const PROXY_CONFIG = [{
    context: [
        "/api",
        "/payment-create.html",
        "/welcome/cas-redirect.html"
    ],
    target: "http://localhost:8180",
    secure: false
}];


module.exports = PROXY_CONFIG;
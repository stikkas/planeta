/**
 * Конфиги для production
 */
export const environment = {
    production: true,

    /**
     * Хост приложения Planeta-web
     */
    mainHost: 'planeta.ru',

    /**
     * Хост приложения магазина
     */
    shopHost: 'shop.planeta.ru',

    /**
     * Хост приложения школы краудфандинга
     */
    schoolHost: 'school.planeta.ru',

    /**
     * Хост приложения благотворительности
     */
    charityHost: 'charity.planeta.ru',

    /**
     * Хост приложения библиородины
     */
    biblioHost: 'biblio.planeta.ru',

    /**
     * Хост приложения административной панели
     */
    adminHost: 'admin.planeta.ru',

    /**
     * Сервера статического контента
     */
    staticNodes: ['s4.planeta.ru', 's3.planeta.ru', 's2.planeta.ru', 's1.planeta.ru'],

    /**
     * Включить логирование в консоль роутинга angular
     */
    traceRoute: false
};

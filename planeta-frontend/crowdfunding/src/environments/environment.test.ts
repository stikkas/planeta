/**
 * Конфиги для test стенда
 */
export const environment = {
    production: true,

    /**
     * Хост приложения Planeta-web
     */
    mainHost: 'test.planeta.ru',

    /**
     * Хост приложения магазина
     */
    shopHost: 'shop.test.planeta.ru',

    /**
     * Хост приложения школы краудфандинга
     */
    schoolHost: 'school.test.planeta.ru',

    /**
     * Хост приложения благотворительности
     */
    charityHost: 'charity.test.planeta.ru',

    /**
     * Хост приложения библиородины
     */
    biblioHost: 'biblio.test.planeta.ru',

    /**
     * Хост приложения административной панели
     */
    adminHost: 'admin.test.planeta.ru',

    /**
     * Сервера статического контента
     */
    staticNodes: ['s2.test.planeta.ru', 's1.test.planeta.ru'],

    /**
     * Включить логирование в консоль роутинга angular
     */
    traceRoute: false
};


/**
 * Конфиги для localhost
 */
export const environment = {
    production: false,

    /**
     * Хост приложения Planeta-web
     */
    mainHost: 'localhost:8180',

    /**
     * Хост приложения магазина
     */
    shopHost: 'shop.dev.planeta.ru',

    /**
     * Хост приложения школы краудфандинга
     */
    schoolHost: 'school.dev.planeta.ru',

    /**
     * Хост приложения благотворительности
     */
    charityHost: 'charity.dev.planeta.ru',

    /**
     * Хост приложения библиородины
     */
    biblioHost: 'biblio.dev.planeta.ru',

    /**
     * Хост приложения административной панели
     */
    adminHost: 'admin.dev.planeta.ru',

    /**
     * Сервера статического контента
     */
    staticNodes: ['s1.dev.planeta.ru', 's2.dev.planeta.ru'],

    /**
     * Включить логирование в консоль роутинга angular
     */
    traceRoute: false
};


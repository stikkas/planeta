/**
 * Конфиги для dev стенда
 */
export const environment = {
    production: false,

    /**
     * Хост приложения Planeta-web
     */
    mainHost: 'dev.planeta.ru',

    /**
     * Хост приложения магазина
     */
    shopHost: 'shop.dev.planeta.ru',

    /**
     * Хост приложения школы краудфандинга
     */
    schoolHost: 'school.dev.planeta.ru',

    /**
     * Хост приложения благотворительности
     */
    charityHost: 'charity.dev.planeta.ru',

    /**
     * Хост приложения библиородины
     */
    biblioHost: 'biblio.dev.planeta.ru',

    /**
     * Хост приложения административной панели
     */
    adminHost: 'admin.dev.planeta.ru',

    /**
     * Сервера статического контента
     */
    staticNodes: ['s2.dev.planeta.ru'],

    /**
     * Включить логирование в консоль роутинга angular
     */
    traceRoute: true
};

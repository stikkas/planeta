import {
    AfterViewInit,
    Component,
    ElementRef,
    forwardRef,
    Input,
    NgZone,
    OnDestroy,
    QueryList,
    ViewChild,
    ViewChildren
} from '@angular/core';
import {ControlValueAccessor, NG_VALUE_ACCESSOR} from '@angular/forms';
import {PlEditorConfig} from './EditorConfig';
import {PluginType} from './PluginType';
import 'tinymce';
import {AbstractPluginComponent} from './plugins/abstract.plugin.component';
import {Language} from 'angular-l10n';

declare let tinymce: any;

tinymce.baseURL = '/assets/tinymce';
tinymce.suffix = '.min';

@Component({
    selector: 'pl-editor',
    templateUrl: './editor.component.html',
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            multi: true,
            useExisting: forwardRef(() => PlEditorComponent),
        }
    ]
})
export class PlEditorComponent implements AfterViewInit, OnDestroy, ControlValueAccessor {
    pluginType = PluginType;

    @Language()
    private lang;
    /**
     * Элемент для вставки TinyMCE
     */
    @ViewChild('tinymce')
    tinymceRef: ElementRef;

    @ViewChildren('plugin')
    pluginList: QueryList<AbstractPluginComponent>;

    /**
     * Конфиг для редактора
     */
    @Input()
    settings = PlEditorConfig.DEFAULT;

    /**
     * Значение из модели до инициализации редактора
     */
    beforeInitValue: string;
    /**
     * Флаг показывающий когда сработало событие изменения из редактора или нет
     */
    fromWriteValue: boolean;
    /**
     * Редактор TinyMCE
     */
    editor: any;
    /**
     * Показывает, что уже один раз инициализировали, и значит плагины добавили
     * Исходим из предположения, что набор плагинов и тулбара не будет меняться втечение всего lifecycle
     */
    initialized = false;
    /**
     * Нужны для того, чтобы работало [(ngModel)] на компоненте
     * @param {string} content
     */
    onModelChange = (content: string) => {
    };

    onModelTouched = () => {
    };

    constructor(private ngZone: NgZone) {
    }

    /**
     * Инициализируем TinyMCE
     */
    ngAfterViewInit(): void {
        if (!this.initialized) {
            this.settings.plugins.forEach((plugin) => {
                const pluginComponent = this.pluginList.find(it => it.type == plugin);
                if (pluginComponent) {
                    tinymce.PluginManager.add(plugin, (editor) => {
                        editor.addButton(plugin, {
                            icon: plugin,
                            onclick: function () {
                                pluginComponent.onClick(editor);
                            }
                        });
                    });
                } else {
                    console.log(`can't find component for plugin ${plugin}`);
                }
            });
            this.initialized = true;
        }

        tinymce.init({
            init_instance_callback: (editor) => {
                if (this.beforeInitValue != null) {
                    editor.setContent(this.beforeInitValue);
                }
                ['KeyUp', 'SetContent', 'change', 'ExecCommand'].forEach(event => {
                    editor.on(event, () => {
                        this.ngZone.run(() => {
                            this.triggerChange();
                        });
                    });
                });
                this.editor = editor;
            },
            language: this.lang,
            target: this.tinymceRef.nativeElement,
            menubar: false,
            skin: 'planeta',
            plugins: `${this.settings.plugins.join(' ')} ${this.settings.mcePlugins || ''}`,
            toolbar: this.settings.toolbar
        });
    }

    registerOnChange(fn: any): void {
        this.onModelChange = fn;
    }

    registerOnTouched(fn: any): void {
        this.onModelTouched = fn;
    }

    /**
     * Записываем данные в редактор
     * @param obj - данные
     */
    writeValue(obj: any): void {
        const val = obj ? obj.toString() : '';
        if (this.editor) {
            this.fromWriteValue = true;
            this.editor.setContent(val);
        } else {
            this.beforeInitValue = val;
        }
    }

    /**
     * Срабатывает на изменение содержимого редактора
     * Изменяем модель, привязанную к компоненте
     */
    triggerChange() {
        if (this.fromWriteValue) {
            this.fromWriteValue = false;
        } else {
            let content = this.editor.getContent();
            if (!content) {
                content = '';
            } else if (content.indexOf('<img class="mceplanetavideo"') >= 0) {
                const domEl = new DOMParser().parseFromString(content, 'text/html');
                Array.from(domEl.querySelectorAll('img.mceplanetavideo')).forEach(el => {
                    const data = JSON.parse(el.getAttribute('data-mce-json'));
                    el.parentElement.innerHTML = `<p:video id="${data.id}" duration="${data.duration}" name="${data.name}" image="${data.image}" owner="${data.owner}"></p:video>`;
                });
                content = domEl.body.innerHTML;
            }
            this.onModelChange(content);
            this.onModelTouched();
        }
    }

    /**
     * Возвращает true - если такой плагин присутствует в настройках
     * @param {PluginType} type - тип планетовского плагина
     * @returns {boolean}
     */
    withPlugin(type: PluginType): boolean {
        return this.settings.plugins.some(it => it == type);
    }

    ngOnDestroy(): void {
        if (this.editor) {
            tinymce.remove(this.editor);
        }
    }
}



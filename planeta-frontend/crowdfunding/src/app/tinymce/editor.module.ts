import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {PlEditorComponent} from './editor.component';
import {PlPhotoPluginComponent} from './plugins/photo/photo.plugin.component';
import {PlExternalPluginComponent} from './plugins/external/external.plugin.component';
import {PlFullscreenCampaignPluginComponent} from './plugins/fullscreen-campaign/fullscreen-campaign.plugin.component';
import {PlFullscreenFaqEditPluginComponent} from './plugins/fullscreen-faq-edit/fullscreen-faq-edit.plugin.component';
import {PlLinkPluginComponent} from './plugins/link/link.plugin.component';
import {PlMusicPluginComponent} from './plugins/music/music.plugin.component';
import {PlVideoPluginComponent} from './plugins/video/video.plugin.component';
import {LocalizationModule} from 'angular-l10n';
import {MceUploaderService} from './services/uploader.service';

@NgModule({
    imports: [
        CommonModule,
        LocalizationModule
    ],
    exports: [PlEditorComponent],
    declarations: [
        PlEditorComponent,
        PlPhotoPluginComponent,
        PlExternalPluginComponent,
        PlFullscreenCampaignPluginComponent,
        PlFullscreenFaqEditPluginComponent,
        PlLinkPluginComponent,
        PlMusicPluginComponent,
        PlVideoPluginComponent
    ],
    providers: [MceUploaderService]
})
export class PlEditorModule {
}


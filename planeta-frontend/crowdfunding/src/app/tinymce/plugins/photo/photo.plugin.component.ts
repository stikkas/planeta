import {Component, ContentChild, Input, OnDestroy, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {AbstractPluginComponent} from '../abstract.plugin.component';
import {PluginType} from '../../PluginType';
import {BsModalRef, BsModalService} from 'ngx-bootstrap';
import {Subject} from 'rxjs/Subject';
import {debounceTime, distinctUntilChanged, map, switchMap} from 'rxjs/operators';
import {PlAuthService, PlImageUtilsService} from '@planeta/planeta-core';
import {MceUploaderService} from '../../services/uploader.service';
import {ToastrService} from 'ngx-toastr';
import {TranslationService} from 'angular-l10n';

const NO_IMAGE = '/images/defaults/upload-pic.png';

@Component({
    selector: 'pl-photo-plugin',
    templateUrl: './photo.plugin.component.html'
})
export class PlPhotoPluginComponent implements AbstractPluginComponent {
    readonly type = PluginType.PHOTO;
    /**
     * Ссылка к модальному окну
     */
    tmplRef: BsModalRef;

    @ViewChild('tmpl')
    tmplWindow: TemplateRef<any>;

    editor: any;
    /**
     * Ссылка к модальному окну для вставки по ссылке
     */
    linkRef: BsModalRef;

    @ViewChild('link')
    linkWindow: TemplateRef<any>;

    /**
     * Ссылка к модальному окну для вставки через загрузку
     */
    uploadRef: BsModalRef;

    @ViewChild('upload')
    uploadWindow: TemplateRef<any>;

    /**
     * Subject ссылки к картинке
     */
    private readonly imageUrl = new Subject<string>();
    /**
     * Флаг показывающий что процесс загрузки по ссылке в деле
     * @type {boolean}
     */
    imageLoading = false;

    /**
     * Последняя загруженная картинка по ссылке
     */
    lastLoadedImageUrl = NO_IMAGE;
    /**
     * Результат загрузки фотографии по ссылке
     */
    imageByLinkResult: string;

    /**
     * id профиля пользователя
     */
    profileId: number;

    /**
     * Фото не добавлено в альбом
     */
    imageNotAdded = true;
    private loadedImageMetaData: any;

    constructor(private modalService: BsModalService, private auth: PlAuthService,
                private uploader: MceUploaderService, private toastr: ToastrService,
                private translation: TranslationService) {
        this.imageUrl.pipe(
                debounceTime(750),
                distinctUntilChanged(),
                map((url: string) => url)
        ).subscribe((url: string) => this.imageUrlChanged(url));
        auth.getUserInfo().subscribe(([userInfo]) => {
            if (userInfo) {
                this.profileId = userInfo.profile.profileId;
            }
        })
    }

    onClick(editor: any) {
        this.editor = editor;
        this.tmplRef = this.modalService.show(this.tmplWindow);
    }

    /**
     * Показывает диалог вставки фото по ссылке
     */
    showPhotoLink() {
        this.tmplRef.hide();
        this.linkRef = this.modalService.show(this.linkWindow);
    }

    /**
     * Показывает диалог вставки через загрузку
     */
    showPhotoUpload() {
        this.tmplRef.hide();
        this.uploadRef = this.modalService.show(this.uploadWindow);
    }

    /**
     * Срабатывает когда изменилась ссылка на картинку из интернета
     * @param {string} url - url ссылки
     */
    onImgUrlChanged(url: string) {
        if (!this.imageLoading) {
            this.imageUrl.next(url);
        }
    }

    /**
     * Загружает картинку по урлу
     * @param {string} url - url ссылки
     */
    imageUrlChanged(url: string) {
        if (url != this.lastLoadedImageUrl) {
            if (url && url.length > 8) {
                this.lastLoadedImageUrl = url;
                this.imageLoading = true;
            }
        }
    }

    onErrorImage() {
        this.imageLoading = false;
        this.lastLoadedImageUrl = NO_IMAGE;
        this.imageByLinkResult = 'mce.plugin.photo-link.noresult';
    }

    onLoadImage() {
        if (this.lastLoadedImageUrl === NO_IMAGE) {
            return;
        }
        this.imageNotAdded = true;
        this.imageLoading = false;
        this.imageByLinkResult = 'mce.plugin.photo-link.result';
        this.uploader.addExternalImage(this.lastLoadedImageUrl, this.profileId)
                .subscribe(([res]) => {
                    if (res == null) {
                        this.toastr.error(
                                this.translation.translate('errors.server-error.message'),
                                this.translation.translate('errors.server-error.title')
                        );
                    } else {
                        this.imageNotAdded = false;
                        this.loadedImageMetaData = res;
                    }
                });
    }

    submitDisabled() {
        return this.imageLoading || this.lastLoadedImageUrl === NO_IMAGE || this.imageNotAdded;
    }

    /**
     * Вставляет картинку в редактор
     */
    insertImage() {
        const imgReplacement = document.createElement('img');
        imgReplacement.setAttribute('class', 'mceplanetaphoto');
        imgReplacement.setAttribute('src', this.loadedImageMetaData.url);
        this.editor.execCommand('mceInsertContent', 0, `${imgReplacement.outerHTML}<p class="planeta-selection">&nbsp;</p>`);
        this.linkRef.hide();
    }
}


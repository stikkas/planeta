import {Component, TemplateRef, ViewChild} from '@angular/core';
import {AbstractPluginComponent} from '../abstract.plugin.component';
import {PluginType} from '../../PluginType';
import {BsModalRef, BsModalService} from 'ngx-bootstrap';

@Component({
    selector: 'pl-video-plugin',
    templateUrl: './video.plugin.component.html'
})
export class PlVideoPluginComponent implements AbstractPluginComponent {
    readonly type = PluginType.VIDEO;
    /**
     * Ссылка к модальному окну
     */
    tmplRef: BsModalRef;

    @ViewChild('tmpl')
    tmplWindow: TemplateRef<any>;

    constructor(private modalService: BsModalService) {

    }

    onClick(editor: any) {
        this.tmplRef = this.modalService.show(this.tmplWindow);
    }
}


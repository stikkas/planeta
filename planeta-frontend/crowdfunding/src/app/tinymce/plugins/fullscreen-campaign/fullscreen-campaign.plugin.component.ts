import {Component, TemplateRef, ViewChild} from '@angular/core';
import {AbstractPluginComponent} from '../abstract.plugin.component';
import {PluginType} from '../../PluginType';
import {BsModalRef, BsModalService} from 'ngx-bootstrap';

@Component({
    selector: 'pl-fullscreen-campaign-plugin',
    templateUrl: './fullscreen-campaign.plugin.component.html'
})
export class PlFullscreenCampaignPluginComponent implements AbstractPluginComponent {
    readonly type = PluginType.FULLSCREEN_CAMPAIGN;
    /**
     * Ссылка к модальному окну
     */
    tmplRef: BsModalRef;

    @ViewChild('tmpl')
    tmplWindow: TemplateRef<any>;

    constructor(private modalService: BsModalService) {

    }

    onClick(editor: any) {
        this.tmplRef = this.modalService.show(this.tmplWindow);
    }
}


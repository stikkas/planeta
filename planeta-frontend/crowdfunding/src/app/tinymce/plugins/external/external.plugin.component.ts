import {Component, TemplateRef, ViewChild} from '@angular/core';
import {AbstractPluginComponent} from '../abstract.plugin.component';
import {PluginType} from '../../PluginType';
import {BsModalRef, BsModalService} from 'ngx-bootstrap';
import {Subject} from 'rxjs/Subject';
import {debounceTime, distinctUntilChanged, map} from 'rxjs/operators';
import {MceUploaderService} from '../../services/uploader.service';
import {Language, TranslationService} from 'angular-l10n';
import {
    HumanDurationPipe,
    ImageType,
    PlAuthService,
    PlImageUtilsService,
    PlStaticNodesService
} from '@planeta/planeta-core';
import {ToastrService} from 'ngx-toastr';

const NO_IMAGE = '/images/defaults/upload-pic.png';

@Component({
    selector: 'pl-external-plugin',
    templateUrl: './external.plugin.component.html'
})
export class PlExternalPluginComponent implements AbstractPluginComponent {
    readonly type = PluginType.EXTERNAL;
    /**
     * Ссылка к модальному окну
     */
    tmplRef: BsModalRef;

    @ViewChild('tmpl')
    tmplWindow: TemplateRef<any>;

    @Language()
    lang;

    /**
     * Последняя загруженный видос
     */
    lastLoadedUrl: string;
    /**
     * Url видео, который вернул нам наш сервис
     */
    validVideoUrl: string;
    /**
     * Картинка последнего загруженного видоса
     */
    lastLoadedImg = NO_IMAGE;

    /**
     * Результат загрузки видео
     */
    videoLoadResult: string;
    /**
     * Название загруженного видоса
     */
    resultParams = <any>{};
    /**
     * Продолжительность видео
     */
    duration: string;
    durationValue: number;

    private videoType: string;
    private videoId: number | undefined;
    editor: any;
    /**
     * Subject ссылки к внешней ссылке
     */
    private readonly externalUrl = new Subject<string>();

    /**
     * В процессе получения информации с youtube
     */
    loading = false;
    /**
     * id профиля пользователя
     */
    profileId: number;

    constructor(private modalService: BsModalService, private uploader: MceUploaderService, private translation: TranslationService,
                private humanDuration: HumanDurationPipe, private imageUtils: PlImageUtilsService,
                private staticNode: PlStaticNodesService, private auth: PlAuthService, private toastr: ToastrService) {
        this.externalUrl.pipe(
                debounceTime(750),
                distinctUntilChanged(),
                map((url: string) => url))
                .subscribe((url: string) => this.urlChanged(url));
        auth.getUserInfo().subscribe(([userInfo]) => {
            if (userInfo) {
                this.profileId = userInfo.profile.profileId;
            }
        })
    }

    onClick(editor: any) {
        this.editor = editor;
        this.tmplRef = this.modalService.show(this.tmplWindow);
    }

    /**
     * Срабатывает когда изменится значение в инпуте для ссылки на youtube
     */
    onUrlChanged(url: string) {
        if (!this.loading) {
            this.externalUrl.next(url);
        }
    }

    /**
     * Получает информацию с youtube
     * @param {string} url - ссылка на видео
     */
    private urlChanged(url: string) {
        if (url != this.lastLoadedUrl && url.trim() != '') {
            this.loading = true;
            this.lastLoadedUrl = url;
            this.uploader.loadExternalVideo(url).subscribe(([res]) => {
                if (res) {
                    this.resultParams = {name: res.name};
                    this.videoLoadResult = 'mce.plugin.external-link.name';
                    this.lastLoadedImg = res.imageUrl;
                    this.validVideoUrl = res.videoUrl;
                    this.duration = this.humanDuration.transform(res.duration);
                    this.durationValue = res.duration;
                    this.videoType = res.videoType;
                    this.videoId = res.cachedVideoId;
                } else {
                    this.videoLoadResult = 'mce.plugin.external-link.noresult';
                    this.resultParams = {};
                    this.lastLoadedUrl = '';
                    this.lastLoadedImg = NO_IMAGE;
                    this.duration = '';
                    this.validVideoUrl = '';
                    this.videoType = '';
                    this.videoId = undefined;
                    this.durationValue = undefined;
                }
                this.loading = false;
            });
        }
    }

    /**
     * Включает или выключает кнопку добавления видео в редактор
     * @returns {boolean}
     */
    submitDisabled() {
        return this.loading || this.lastLoadedImg === NO_IMAGE;
    }

    /**
     * Добавляет видео в редактор
     */
    insertLink() {
        this.uploader.addExternalVideo(this.validVideoUrl, this.profileId).subscribe(([res]) => {
            if (res == null) {
                this.toastr.error(
                        this.translation.translate('errors.server-error.message'),
                        this.translation.translate('errors.server-error.title')
                );
            } else {
                const imgReplacement = document.createElement('p');
                imgReplacement.appendChild(this.parseRichMedia({
                    duration: res.duration,
                    id: res.id || res.objectId,
                    name: res.name,
                    owner: res.ownerId,
                    image: res.imageUrl,
                    url: res.url || res.videoUrl
                }));

                this.editor.execCommand('mceInsertContent', 0, `${imgReplacement.outerHTML}<p class="planeta-selection">&nbsp;</p>`);
                this.tmplRef.hide();
            }
        });

    }

    parseRichMedia(attributes: any) {
        const data = <any>{
            name: attributes.name,
            duration: this.duration
        };
        // if (TinyMcePlaneta.currentConfiguration.campaign) {
        //     data.config = 'campaign';
        // }
        if (attributes.image) {
            data.url = this.imageUtils.getThumbnailUrl(attributes.image, PlImageUtilsService.CAMPAIGN_VIDEO, ImageType.VIDEO);
        } else {
            data.profileId = attributes.owner;
            data.videoId = attributes.id;
        }
        const $imgReplacement = document.createElement('img');
        $imgReplacement.setAttribute('data-mce-json', JSON.stringify(attributes));
        $imgReplacement.setAttribute('class', 'mceplanetavideo');
        $imgReplacement.setAttribute('src', `${document.location.protocol}//${this.staticNode.getStaticNode()}/video-stub.jpg?${this.serializeData(data)}`);
        // $imgReplacement.addClass(attributes['class']);
        /*
        if (!attributes.image) {
            var richTagUpdate = setInterval(function () {
                $.get('/api/public/video.json?' + $.param({
                    profileId: attributes.owner,
                    videoId: attributes.id
                }), {}, function (response) {
                    if (response && response.status == 'ERROR') {
                        clearInterval(richTagUpdate);
                        console.log('Не удалось загрузить видео. Неподдерживаемый кодек');
                    }
                    if (response.imageUrl) {
                        const json = JSON.parse($imgReplacement.getAttribute('data-mce-json'));
                        json.image = response.imageUrl;
                        var $tinyImg = $(tinymce.activeEditor.getBody()).find('img[src="' + src + '"]');
                        $tinyImg.attr('data-mce-json', JSON.stringify(json));
                        $tinyImg.attr("src", src + "&r=" + (new Date()).getTime());
                        clearInterval(richTagUpdate);
                    }
                });
            }, 3000);
        }
        */
        return $imgReplacement;
    }

    serializeData(data: any): string {
        const ret = [];
        Object.keys(data).forEach(key => {
            const value = data[key];
            if (value) {
                ret.push(encodeURIComponent(key) + "=" + encodeURIComponent(value));
            }
        });
        return ret.join("&");
    }
}


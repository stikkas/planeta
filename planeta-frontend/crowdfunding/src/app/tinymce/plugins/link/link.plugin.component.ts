import {Component, TemplateRef, ViewChild} from '@angular/core';
import {AbstractPluginComponent} from '../abstract.plugin.component';
import {PluginType} from '../../PluginType';
import {BsModalRef, BsModalService} from 'ngx-bootstrap';

@Component({
    selector: 'pl-link-plugin',
    templateUrl: './link.plugin.component.html'
})
export class PlLinkPluginComponent implements AbstractPluginComponent {
    readonly type = PluginType.LINK;
    /**
     * Ссылка к модальному окну
     */
    tmplRef: BsModalRef;

    @ViewChild('tmpl')
    tmplWindow: TemplateRef<any>;

    editor: any;

    constructor(private modalService: BsModalService) {

    }

    onClick(editor: any) {
        this.tmplRef = this.modalService.show(this.tmplWindow);
        this.editor = editor;
    }

    /**
     * Вставляет ссылку в редактор
     * @param {string} value
     */
    insertLink(value: string) {
        this.editor.selection.setContent(`<a href="${value}">${this.editor.selection.getContent() || value}</a>`);
        this.tmplRef.hide();
    }

}


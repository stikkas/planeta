import {PluginType} from '../PluginType';

export interface AbstractPluginComponent {
    readonly type: PluginType;
    onClick: (editor: any) => void
}

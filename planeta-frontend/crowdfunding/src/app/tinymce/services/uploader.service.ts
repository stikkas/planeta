import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {HttpClient} from '@angular/common/http';
import {catchError, map} from 'rxjs/operators';
import {ToastrService} from 'ngx-toastr';
import {TranslationService} from 'angular-l10n';
import {ErrorHandlerService} from '../../services/error.handler.service';
import {ErrorableResult} from '@planeta/planeta-core';

/**
 * Сервис для загрузки различных файлов на сервер
 */
@Injectable()
export class MceUploaderService extends ErrorHandlerService {

    constructor(private http: HttpClient, toastr: ToastrService,
                translation: TranslationService) {
        super(toastr, translation);
    }

    /**
     * Wrap external image with proxy and add record to photo db
     * @param {String} externalUrl - external image url
     * @param {Number} profileId - id of profile
     */
    addExternalImage(externalUrl: string, profileId: number): Observable<any> {
        return this.http.post('/api/parse.json', null, {
            params: {
                message: externalUrl,
                profileId: `${profileId}`
            }
        }).pipe(
                map((res: any) => {
                    if (!(res && res.imageAttachments && res.imageAttachments.length)) {
                        return [null, [], null];
                    } else {
                        return [res.imageAttachments[0], [], null];
                    }
                }),
                catchError(this.errorHandler(null))
        );
    }

    /**
     * Wrap external video with proxy and add record to video db
     * @param {String} externalUrl - external video url
     * @param {Number} profileId - id of profile
     */
    addExternalVideo(externalUrl: string, profileId: number): Observable<any> {
        return this.http.post('/api/parse.json', null, {
            params: {
                message: externalUrl,
                profileId: `${profileId}`
            }
        }).pipe(
                map((res: any) => {
                    if (!(res && (res.videoAttachments && res.videoAttachments.length))) {
                        return [null, [], null];
                    } else {
                        return [res.videoAttachments[0], [], null];
                    }
                }),
                catchError(this.errorHandler(null))
        );
    }

    /**
     * Загружает видео с внешнего ресурса к нам на сервер
     * @param {string} externalVideoUrl - url к видео
     * @returns {Observable<any>}
     */
    loadExternalVideo(externalVideoUrl: string): Observable<any> {
        return this.http.get('/api/public/video-external.json', {params: {url: externalVideoUrl}})
                .pipe(
                        map((res: any) => {
                            if (!(res && res.videoUrl && res.imageUrl)) {
                                console.log(res);
                                return [null, [], null];
                            } else {
                                console.log(res);
                                return [res, [], null];
                            }
                        }),
                        catchError(this.errorHandler(null))
                );
    }
}

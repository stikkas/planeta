/**
 * Только наши плагины
 */
export enum PluginType {
    MUSIC = 'pl-music',
    VIDEO = 'pl-video',
    PHOTO = 'pl-photo',
    LINK = 'pl-link',
    EXTERNAL = 'pl-external',
    FULLSCREEN_FAQ = 'pl-fullscreen-faq-edit',
    FULLSCREEN_CAMPAIGN = 'pl-fullscreen-campaign',
}

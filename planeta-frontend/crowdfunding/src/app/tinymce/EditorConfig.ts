import {PluginType} from './PluginType';

/**
 * Наборы конфигов для разных страниц
 */

const DEFAULT = [PluginType.PHOTO, PluginType.MUSIC, PluginType.VIDEO, PluginType.EXTERNAL, PluginType.LINK];
const FAQ = [PluginType.PHOTO, PluginType.MUSIC, PluginType.VIDEO, PluginType.EXTERNAL, PluginType.LINK, PluginType.FULLSCREEN_FAQ];
const CAMPAIGN = [PluginType.PHOTO, PluginType.MUSIC, PluginType.VIDEO, PluginType.EXTERNAL, PluginType.LINK, PluginType.FULLSCREEN_CAMPAIGN];
const SHARE = [PluginType.LINK];
const QUIZ = [PluginType.PHOTO, PluginType.LINK];
const NEWS_POST = [PluginType.PHOTO, PluginType.MUSIC, PluginType.VIDEO, PluginType.EXTERNAL, PluginType.LINK];
const USER_SETTINGS = DEFAULT;

export class PlEditorConfig {
    static readonly DEFAULT: PlEditorConfigItem = {
        plugins: DEFAULT,
        toolbar: DEFAULT.join(' ')
    };
    static readonly FAQ: PlEditorConfigItem = {
        plugins: FAQ,
        toolbar: `bold italic underline | ${FAQ.slice(0, FAQ.length - 1).join(' ')} | ${FAQ[FAQ.length - 1]}`
    };
    static readonly CAMPAIGN: PlEditorConfigItem = {
        plugins: CAMPAIGN,
        toolbar: `bold italic underline | ${CAMPAIGN.slice(0, CAMPAIGN.length - 1).join(' ')} | alignleft aligncenter alignright alignjustify | ${CAMPAIGN[CAMPAIGN.length - 1]}`
    };
    static readonly SHARE: PlEditorConfigItem = {
        plugins: SHARE,
        toolbar: `${SHARE[0]} bullist numlist`
    };
    static readonly QUIZ: PlEditorConfigItem = {
        plugins: QUIZ,
        toolbar: `bold italic underline | ${QUIZ.join(' ')} | alignleft aligncenter alignright alignjustify`
    };
    static readonly NEWS_POST: PlEditorConfigItem = {
        plugins: NEWS_POST,
        toolbar: `formatselect bold italic underline blockquote | ${NEWS_POST.join(' ')} | alignleft aligncenter alignright alignjustify | bullist numlist`,
        mcePlugins: 'lists advlist'
    };
    static readonly USER_SETTINGS: PlEditorConfigItem = {
        plugins: USER_SETTINGS,
        toolbar: USER_SETTINGS.join(' ')
    };
}

export interface PlEditorConfigItem {
    plugins: PluginType[]
    toolbar: string
    mcePlugins?: string
}


import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'pHtmlTransform'
})
export class PHtmlTransform implements PipeTransform {

    transform(value: string): string {
        return value.replace('<p:photo', '<img')
                .replace('</p:photo>', '')
                .replace('image="', 'src="')
                .replace('id="', 'data-id="')
                .replace('owner="', 'data-owner="');
    }
}

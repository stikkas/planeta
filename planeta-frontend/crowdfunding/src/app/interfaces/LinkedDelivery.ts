import {DeliveryType} from '../models/enums/DeliveryType';
import {Address} from '../models/Address';

export interface LinkedDelivery {
    address: Address;
    description: string;
    enabled: boolean;
    location: {};
    name: string;
    price: number;
    publicNote: string;
    serviceId: number;
    serviceType: DeliveryType;
    shareId: number;
    subjectId: number;
    subjectType: string;
}

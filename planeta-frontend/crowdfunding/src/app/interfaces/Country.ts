export interface Country {
    countryId: number;
    countryNameEn: string;
    countryNameRus: string;
    globalRegionId: number;
    locationId: number;
    locationType: string;
    name: string;
    parent: any;
    parentLocationId: number;
    parentLocationType: string;
}

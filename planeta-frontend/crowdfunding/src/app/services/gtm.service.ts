import {Injectable} from '@angular/core';
import * as _ from 'lodash';
import {HttpClient} from '@angular/common/http';
import {catchError, map} from 'rxjs/operators';
import {Observable} from 'rxjs/Observable';
import {of} from 'rxjs/observable/of';
import {PaymentData} from '../models/PaymentData';
import {PurchaseShareInfo} from '../models/PurchaseShareInfo';
import {ActionStatus} from '@planeta/planeta-core';

@Injectable()
export class GtmService {

    constructor(private http: HttpClient) {

    }

    private loadGtmCampaignInfo(campaignIds: string[], startIndex: number = 0) {

        return this.http.get('/api/public/gtm/get-campaign-list-info.json', {params: {ids: campaignIds}}).pipe(
            map((response: ActionStatus<any>) => {
                if (response.success) {
                    const items = response.result;
                    _.each(items, function (item, idx) {
                        item.pos = startIndex + idx;
                    });
                    return items;
                } else {
                    return null;
                }
            }),
            catchError(this.errorHandler( null))
        );
    }

    private errorHandler<T>( result: T) {
        return (error: any): Observable<T> => {
            console.log(error.message);
            return of(result as T);
        };
    }

    private getCampaignInfoFromLocalStorage() {
        let campaignInfo;

        if (window.localStorage) {
            campaignInfo = window.localStorage.getItem('gtmCampaignInfo');
            if (campaignInfo && campaignInfo !== 'undefined') {
                campaignInfo = JSON.parse(campaignInfo);
            }
        }

        return campaignInfo;
    }

    private loadGtmCampaignInfoAndRunMethod(context, methodName: string, campaignId: number, object?, object2?) {
        const campaignInfo = this.getCampaignInfoFromLocalStorage();
        const args = Array.prototype.slice.call(arguments, 3);

        if (!campaignInfo || campaignInfo.campaignId !== campaignId) {
            this.loadGtmCampaignInfo([campaignId.toString()]).subscribe(result => {
                if (result) {
                    args.unshift(result[0]);
                    context[methodName].apply(context, args);
                }
            });
        } else {
            args.unshift(campaignInfo);
            context[methodName].apply(context, args);
        }
    }

    trackChoicePaymentProvider (paymentData: PaymentData, purchaseShareInfo: PurchaseShareInfo) {
        this.loadGtmCampaignInfoAndRunMethod(
            this,
            '_trackChoicePaymentProvider',
            purchaseShareInfo.campaign.campaignId,
            paymentData,
            purchaseShareInfo
        );
    }

    private _trackChoicePaymentProvider (campaignInfo, paymentData: PaymentData, purchaseShareInfo: PurchaseShareInfo) {
        window.dataLayer.push({
            event: 'universalCustomTrigger',
            ecommerce: {
                checkout: {
                    actionField: {
                        step: 1,
                        option: paymentData.paymentMethodId,
                        list: campaignInfo.listName
                    },

                    products: [{
                        name: purchaseShareInfo.campaign.name,
                        id: purchaseShareInfo.campaign.campaignId,
                        price: purchaseShareInfo.shareDetailed.price,
                        brand: purchaseShareInfo.authorName,
                        category: purchaseShareInfo.campaign.mainTag.name,
                        quantity: purchaseShareInfo.quantity
                    }]
                }
            }
        });
    }

    /**
     * Универсальное событие для gtm
     * @param event
     * @param url
     */
    trackUniversalEvent(event: string, url?: string): void {
        if (url) {
            window.dataLayer.push({
                event: event,
                ecommerce: {},
                'customPagePath': url
            });
        } else {
            window.dataLayer.push({
                event: event,
                ecommerce: {}
            });
        }
    }
}

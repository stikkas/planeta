import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {ActionStatus, ErrorableResult} from '@planeta/planeta-core';
import {catchError, map} from 'rxjs/operators';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/observable/throw';
import {ErrorHandlerService} from './error.handler.service';
import {MyCampaignsStatusesCount} from '../models/MyCampaignsStatusesCount';
import {MyPurchasesAndRewardsCount} from '../models/MyPurchasesAndRewardsCount';
import {ProfileSites} from '../models/ProfileSites';
import {ToastrService} from 'ngx-toastr';
import {TranslationService} from 'angular-l10n';
import {ProfileDTO} from '../models/dto/ProfileDTO';
import {PostDTO} from '../models/dto/PostDTO';
import {SubscriptionsDTO} from '../models/dto/SubscriptionsDTO';
import {BalanceOperation} from '../models/BalanceOperation';

@Injectable()
export class ProfileService extends ErrorHandlerService {
    private myBalanceUrl = '/api/profile/my-balance';
    private updatesForMeUrl = '/api/profile/updates-for-me';
    private myCampaignStatusesUrl = '/api/profile/my-campaign-statuses';
    private myPurchasedCampaignStatusesUrl = '/api/profile/my-purchased-campaign-statuses';
    private myPurchasesAndRewardsUrl = '/api/profile/my-purchases-and-rewards';
    private updatePasswordUrl = '/api/profile/update-password';
    private mySitesUrl = '/api/profile/my-sites';
    private saveMySitesUrl = '/api/profile/save-my-sites';
    private saveMyBaseInfoUrl = '/api/profile/profile-save-main-settings';
    private saveSubscriptionsUrl = '/api/profile/save-subscriptions';
    private myBalanceOperationsUrl = '/api/profile/balance-operations';

    /**
     * Баланс пользователя
     * @type {number}
     */
    myBalance = 0;

    constructor(private http: HttpClient,
                toastr: ToastrService,
                translation: TranslationService) {
        super(toastr, translation);
        this.loadMyBalance();
    }

    /**
     * Получает информацию о балансе авторизованного текущего пользователя с сервера при инициализации
     */
    loadMyBalance() {
        this.http.get(this.myBalanceUrl).subscribe((response: ActionStatus<number>) => {
            if (response.success) {
                this.myBalance = response.result;
            }
        });
    }

    /**
     * Получает информацию о балансе авторизованного текущего пользователя
     * @returns number
     */
    getMyBalance(): number {
        return this.myBalance;
    }


    /**
     * Достает с сервера коллекцию обновлений для моего профиля
     * @returns {Observable<ErrorableResult>}
     */
    getUpdatesForMe(offset: number = 0, limit: number = 10): Observable<ErrorableResult> {
        const limitString = limit.toString();
        const offsetString = offset.toString();

        return this.http.get(this.updatesForMeUrl, {params: {offset: offsetString, limit: limitString}}).pipe(
                map((response: ActionStatus<any>) => this.successHandler(response)),
                catchError(this.errorHandler([]))
        );
    }

    /**
     * Достает количество проектов пользователя с группировкой по статусам
     * @returns {Observable<ErrorableResult>}
     */
    getMyCampaignsStatusesCount(): Observable<ErrorableResult> {
        return this.http.get(this.myCampaignStatusesUrl).pipe(
                map((response: ActionStatus<MyCampaignsStatusesCount[]>) => this.successHandler(response)),
                catchError(this.errorHandler([]))
        );
    }

    /**
     * Достает количество поддержанных пользователем проектов с группировкой по статусам
     * @returns {Observable<ErrorableResult>}
     */
    getMyPurchasedCampaignsStatusesCount(): Observable<ErrorableResult> {
        return this.http.get(this.myPurchasedCampaignStatusesUrl).pipe(
                map((response: ActionStatus<MyCampaignsStatusesCount[]>) => this.successHandler(response)),
                catchError(this.errorHandler([]))
        );
    }

    /**
     * Достает количество поддержанных проектов с группировкой по типу "товара"
     * @returns {Observable<ErrorableResult>}
     */
    getMyPurchasesAndRewardsCount(): Observable<ErrorableResult> {
        return this.http.get(this.myPurchasesAndRewardsUrl).pipe(
                map((response: ActionStatus<MyPurchasesAndRewardsCount[]>) => this.successHandler(response)),
                catchError(this.errorHandler([]))
        );
    }

    /**
     * Изменить мой пароль
     * @param data
     * @returns {Observable<ErrorableResult>}
     */
    updatePassword(data: any): Observable<ErrorableResult> {
        return this.http.post(this.updatePasswordUrl, data).pipe(
                map((response: ActionStatus<any>) => this.successHandler(response, true)),
                catchError(this.errorHandler(false))
        );
    }

    /**
     * Достает с сервера контакты профиля (сайты)
     * @returns {Observable<any>}
     */
    getMySites(): Observable<ErrorableResult> {
        return this.http.get(this.mySitesUrl).pipe(
                map((response: ActionStatus<ProfileSites>) => this.successHandler(response)),
                catchError(this.errorHandler(null))
        );
    }

    /**
     * Сохраняет на сервер контакты профиля (сайты)
     * @returns {Observable<any>}
     */
    saveMySites(data: ProfileSites): Observable<any> {
        return this.http.post(this.saveMySitesUrl, data).pipe(
                map((response: ActionStatus<ProfileSites>) => this.successHandler(response)),
                catchError(this.errorHandler(null))
        );
    }

    /**
     * Сохраняет базовую информацию профиля
     * @param {ProfileDTO} data
     * @returns {Observable<any>}
     */
    saveBaseInfo(data: ProfileDTO): Observable<any> {
        return this.http.post(this.saveMyBaseInfoUrl, data).pipe(
                map((response: ActionStatus<ProfileSites>) => this.successHandler(response)),
                catchError(this.errorHandler(null))
        );
    }

    /**
     * Добавляет новость на страницу проекта
     * @param {PostDTO} news - новость
     * @returns {Observable<any>}
     */
    addNewsToCampaign(news: PostDTO): Observable<any> {
        return this.http.post('/api/profile/news/add-post', news).pipe(
                map((response: ActionStatus<any>) => this.successHandler(response, true)),
                catchError(this.errorHandler(null))
        );
    }

    /**
     * Отправляет запрос для заявки менеджеру
     * @param {number} campaignId - id интересующего проекта
     * @param {string} message - сообщение менеджеру
     * @param {string} email - для неавторизованного пользователя
     */
    callToManager(campaignId: number, message: string, email?: string): Observable<ErrorableResult> {
        return this.http.post('/api/public/ask-manager', {campaignId, message, email}).pipe(
                map((response: ActionStatus<any>) => this.successHandler(response, true)),
                catchError(this.errorHandler(null)));
    }

    /**
     * Сохраняет данные подписок на сервер
     * @param {ProfileDTO} data
     * @returns {Observable<any>}
     */
    saveSubscriptions(data: SubscriptionsDTO): Observable<any> {
        return this.http.post(this.saveSubscriptionsUrl, data).pipe(
                map((response: ActionStatus<boolean>) => this.successHandler(response)),
                catchError(this.errorHandler(null))
        );
    }

    /**
     * Достает с сервера информацию об моих операциях по балансу
     * @param offset
     * @param limit
     */
    getMyBalanceOperations(offset: number = 0, limit: number = 10): Observable<any> {
        const limitString = limit.toString();
        const offsetString = offset.toString();

        return this.http.get(this.myBalanceOperationsUrl, {params: {offset: offsetString, limit: limitString}}).pipe(
                map((response: ActionStatus<BalanceOperation>) => this.successHandler(response)),
                catchError(this.errorHandler(null))
        );
    }
}


import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {ActionStatus, ErrorableResult} from '@planeta/planeta-core';
import {catchError, map} from 'rxjs/operators';
import {Observable} from 'rxjs/Observable';
import {ErrorHandlerService} from './error.handler.service';
import {ToastrService} from 'ngx-toastr';
import {TranslationService} from 'angular-l10n';

@Injectable()
export class RewardsService extends ErrorHandlerService {

    constructor(private http: HttpClient,
                toastr: ToastrService,
                translation: TranslationService) {
        super(toastr, translation);
    }

    /**
     * Возвращает информацию об вознаграждении
     * @returns {Observable<ErrorableResult>}
     */
    getRewardById(id: number): Observable<ErrorableResult> {
        return this.http.get(`/api/public/rewards/${id}`).pipe(
                map((response: ActionStatus<any>) => this.successHandler(response)),
                catchError(this.errorHandler(null))
        );
    }

    /**
     * Получает информацию о вознаграждениях (в не благотворительных проектах), которые приобрел пользователь
     * @returns {Observable<ErrorableResult>}
     */
    getMyPurchasedRewards(): Observable<ErrorableResult> {
        return this.http.get('/api/profile/my-purchased-rewards').pipe(
            map((response: ActionStatus<any>) => this.successHandler(response, [])),
            catchError(this.errorHandler([]))
        );
    }

    /**
     * Получает информацию о вознаграждениях (в благотворительных проектах), которые приобрел пользователь
     * @returns {Observable<ErrorableResult>}
     */
    getMyPurchasedCharityRewards(): Observable<ErrorableResult> {
        return this.http.get('/api/profile/my-purchased-срфкшен-rewards').pipe(
            map((response: ActionStatus<any>) => this.successHandler(response, [])),
            catchError(this.errorHandler([]))
        );
    }
}


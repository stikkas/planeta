import {
    ActivatedRouteSnapshot,
    CanActivate,
    Params,
    Router,
    RouterStateSnapshot
} from '@angular/router';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {PlAuthService} from '@planeta/planeta-core';
import {of} from 'rxjs/observable/of';

@Injectable()
export class AccountCanActivate implements CanActivate {

    constructor(private authService: PlAuthService, private router: Router) {
    }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
        if (route.params.authRequired) {
            return of(true);
        } else {
            return this.checkPermission(state.url.split('?')[0], route.queryParams);
        }
    }

    checkPermission(url: string, queryParams: Params): Observable<boolean> {
        return this.authService.getUserInfo()
                .map(([info]) => {
                    if (info == null) {
                        this.router.navigate([url, {authRequired: true}], {queryParams: queryParams});
                        return false;
                    } else {
                        return true;
                    }
                });
    }

}


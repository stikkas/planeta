import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot} from '@angular/router';
import {RewardsService} from '../rewards.service';

@Injectable()
export class RewardCampaignResolver implements Resolve<any> {

    constructor(private rewardsService: RewardsService,
                private router: Router) {
    }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        return this.rewardsService.getRewardById(+route.params.id)
                .map(([res]) => {
                    if (res) {
                        return res;
                    } else {
                        // Возвращаемся к списку вознаграждений
                        let routerLink = route.parent.pathFromRoot
                                .map(s => s.url)
                                .reduce((a, e) => a.concat(e)).map(s => s.path);
                        this.router.navigate(routerLink);
                    }
                });
    }

}


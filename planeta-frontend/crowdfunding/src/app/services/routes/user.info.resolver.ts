import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {ProjectService} from '../project.service';
import {ErrorableResult, PlAuthService} from '@planeta/planeta-core';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class UserInfoResolver implements Resolve<ErrorableResult> {

    constructor(private authService: PlAuthService) {
    }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<ErrorableResult> {
        return this.authService.getUserInfo();
    }

}

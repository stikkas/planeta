import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Params, Route, Router, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs/Observable';
import {ProfileService} from '../profile.service';
import {MyPurchasesAndRewardsCount} from '../../models/MyPurchasesAndRewardsCount';
import {OrderObjectType} from '../../models/enums/OrderObjectType';
import {PlAuthService} from '@planeta/planeta-core';
import {of} from 'rxjs/observable/of';
import {AccountCanActivate} from './account.can.activate';

@Injectable()
export class MyPurchaseTypeActivate implements CanActivate{

    constructor(private accountCanActivate: AccountCanActivate,
                private profileService: ProfileService,
                private authService: PlAuthService,
                private router: Router) {
    }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
        return this.accountCanActivate.canActivate(route, state).flatMap(canActivate => {
            if (canActivate && !route.params.authRequired) {
                return this.checkPurchases();
            } else {
                return of(canActivate);
            }
        });
    }

    checkPurchases(): Observable<boolean> {
        return this.profileService.getMyPurchasesAndRewardsCount().map(([result]) => {
            let rewards = 0;
            let biblio = 0;
            let charity = 0;
            let shop = 0;

            result.forEach(function (item: MyPurchasesAndRewardsCount) {
                switch (item.orderObjectType) {
                    case OrderObjectType.SHARE: {
                        if (item.campaignTagMnemonic === 'CHARITY') {
                            charity += item.count;
                        } else {
                            rewards += item.count;
                        }
                    } break;
                    case OrderObjectType.BIBLIO: biblio += item.count; break;
                    case OrderObjectType.PRODUCT: shop += item.count; break;
                }
            });

            if (rewards > 0) {
                this.router.navigate(['/account/my-rewards']);
                return false;
            }

            if (biblio > 0) {
                this.router.navigate(['/account/my-charity']);
                return false;
            }

            if (charity > 0) {
                this.router.navigate(['/account/my-bibliorodina']);
                return false;
            }

            if (shop > 0) {
                this.router.navigate(['/account/my-purchases']);
                return false;
            }

            return true;
        });
    }
}

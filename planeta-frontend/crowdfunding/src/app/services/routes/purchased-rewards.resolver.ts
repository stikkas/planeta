import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {ErrorableResult} from '@planeta/planeta-core';
import {Observable} from 'rxjs/Observable';
import {RewardsService} from '../rewards.service';

@Injectable()
export class PurchasedRewardsResolver implements Resolve<ErrorableResult> {

    constructor(private rewardsService: RewardsService) {
    }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<ErrorableResult> {
        return this.rewardsService.getMyPurchasedRewards();
    }

}

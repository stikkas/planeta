import {ActionStatus, ErrorableResult, ErrorField} from '@planeta/planeta-core';
import {of} from 'rxjs/observable/of';
import {Observable} from 'rxjs/Observable';
import {ToastrService} from 'ngx-toastr';
import {TranslationService} from 'angular-l10n';

export class ErrorHandlerService {

    constructor(private toastr: ToastrService,
                private translation: TranslationService) {}

    /**
     * Обработчик ошибок сетевых ошибок
     */
    protected errorHandler<T>(result: T, showToastr: boolean = true) {
        return (error: any): Observable<ErrorableResult> => {
            if (showToastr) {
                this.toastr.error(
                    this.translation.translate('errors.server-error.message'),
                    this.translation.translate('errors.server-error.title')
                );
            }
            return of(<ErrorableResult>[result, [], error.message]);
        };
    }

    /**
     * Унифицированный success callback
     * @param {ActionStatus<T>} response
     * @param defaultValue - значение по умолчанию в случае успешного ответа с сервера без результата
     */
    protected successHandler<T>(response: ActionStatus<T>, defaultValue: any = null): ErrorableResult {
        if (response.success) {
            return [response.result == null ? defaultValue : response.result, <ErrorField[]>[], ''];
        } else {
            const errorIsArray = Array.isArray(response.result);
            return [null, <ErrorField[]>(errorIsArray ? response.result : []),
                errorIsArray ? '' : `${response.result}`];
        }
    }
}

import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {catchError, map} from 'rxjs/operators';
import {Observable} from 'rxjs/Observable';
import {of} from 'rxjs/observable/of';

import {ActionStatus} from '@planeta/planeta-core';
import {PurchaseShareInfo} from '../models/PurchaseShareInfo';
import {TranslationService} from 'angular-l10n';
import {ToastrService} from 'ngx-toastr';
import {RechargeBalanceDTO} from '../models/dto/RechargeBalanceDTO';


@Injectable()
export class PaymentService {
    private paymentMethodsListUrl = '/api/public/payment-methods';
    private getPurchaseInfoUrl = '/api/public/purchase-share-info/';
    private rechargeBalanceUrl = '/api/profile/recharge-balance';

    constructor(private http: HttpClient,
                private toastr: ToastrService,
                private translationService: TranslationService) {
    }

    /**
     * Достает список доступных платёжных методов
     */
    getPaymentMethods() {
        return this.http.get(this.paymentMethodsListUrl, {});
    }

    /**
     * Достаёт информацию о покупке
     * @param {string} infoId - Id 'корзины' (сохраненной информации о покупке)
     * @returns {Observable<PurchaseShareInfo>}
     */
    getPurchaseInfo(infoId: string): Observable<PurchaseShareInfo> {
        return this.http.get(`${this.getPurchaseInfoUrl}${infoId}`).pipe(
            map((response: ActionStatus<PurchaseShareInfo>) => {
                if (response.success) {
                    return response.result;
                } else {
                    return null;
                }
            }),
            catchError(this.errorHandler( null))
        );
    }

    /**
     * Обработчик ошибок сетевых ошибок
     */
    private errorHandler<T>( result: T) {
        return (error: any): Observable<T> => {
            this.toastr.error(
                this.translationService.translate('errors.server-error.message'),
                this.translationService.translate('errors.server-error.title')
            );
            console.log(error.message);
            return of(result as T);
        };
    }

    /**
     * Пополнить баланс пользователя
     * @param {RechargeBalanceDTO} data
     * @returns {Observable<any>}
     */
    rechargeBalance(data: RechargeBalanceDTO) {
        return this.http.post(this.rechargeBalanceUrl, data).pipe(
            map((response: ActionStatus<any>) => {
                if (response.success) {
                    return response.result;
                } else {
                    return null;
                }
            }),
            catchError(this.errorHandler( null))
        );
    }
}

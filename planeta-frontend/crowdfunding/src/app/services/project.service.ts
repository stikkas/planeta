import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {ActionStatus, ErrorableResult} from '@planeta/planeta-core';
import {CampaignTargeting} from '../models/CampaignTargeting';
import {catchError, map} from 'rxjs/operators';
import {Observable} from 'rxjs/Observable';
import {ErrorHandlerService} from './error.handler.service';
import {ToastrService} from 'ngx-toastr';
import {TranslationService} from 'angular-l10n';
import {MyCampaignRequestParams} from "../models/MyCampaignRequestParams";

@Injectable()
export class ProjectService extends ErrorHandlerService {
    private campaignTargetingUrl = '/api/public/campaign-targeting';
    private vkPixelUrl = 'https://vk.com/rtrg?p=';
    private fbPixelUrl = 'https://www.facebook.com/tr?id=';
    private fbPixelParams = '&ev=PageView&noscript=1';

    constructor(private http: HttpClient,
                toastr: ToastrService,
                translation: TranslationService) {
        super(toastr, translation);
    }

    /**
     * Получает информацию о проектах пользователя
     * @returns {Observable<ErrorableResult>}
     */
    getMyCampaigns(): Observable<ErrorableResult> {
        return this.http.get('/api/profile/my-campaigns').pipe(
                map((response: ActionStatus<any>) => this.successHandler(response, [])),
                catchError(this.errorHandler([]))
        );
    }

    /**
     * Получает информацию о проектах, которые поддержал пользователь
     * @returns {Observable<ErrorableResult>}
     */
    getMyPurchasedCampaigns(queryParams: any): Observable<ErrorableResult> {
        return this.http.get('/api/profile/my-purchased-campaigns', {params: queryParams}).pipe(
            map((response: ActionStatus<any>) => this.successHandler(response, [])),
            catchError(this.errorHandler([]))
        );
    }


    /**
     * Создаёт пиксели VK и Facebook по айдишникам из настроек проекта
     */
    createCampaignTargetingPixels(campaignId: number): void {
        this.http.get(this.campaignTargetingUrl, {
            params: {
                campaignId: campaignId.toString()
            }
        }).subscribe(
                (response: ActionStatus<CampaignTargeting>) => {
                    if (response.success) {
                        if (response.result) {

                            if (response.result.vkTargetingId > 0) {
                                this.addVkPixel(response.result.vkTargetingId);
                            }

                            if (response.result.fbTargetingId > 0) {
                                this.addFbPixel(response.result.fbTargetingId);
                            }
                        }
                    }
                },
                (error: Error) => {
                    console.log(error.message);
                }
        );
    }

    /**
     * Удаляет черновик пользователя
     */
    removeDraft(campaignId: number): Observable<ErrorableResult> {
        return this.http.post('/api/profile/campaign-remove', campaignId).pipe(
                map((response: ActionStatus<any>) => this.successHandler(response, true)),
                catchError(this.errorHandler(false)));
    }

    /**
     * Создаёт пиксель VK
     */
    private addVkPixel(pixelId: number) {
        const image = document.createElement('img');
        image.src = this.vkPixelUrl + pixelId;
    }

    /**
     * Создаёт пиксель Facebook
     */
    private addFbPixel(pixelId: number) {
        const image = document.createElement('img');
        image.src = this.fbPixelUrl + pixelId + this.fbPixelParams;
    }


    /**
     * Получает статические данные по проектам
     * @param {number[]} campaignIds - идентификаторы проектов
     */
    getMyCampaignsStats(campaignIds: number[]) {
        return this.http.post('/api/profile/my-campaigns-stats', campaignIds).pipe(
                map((response: ActionStatus<any>) => this.successHandler(response)),
                catchError(this.errorHandler(null)));
    }
}


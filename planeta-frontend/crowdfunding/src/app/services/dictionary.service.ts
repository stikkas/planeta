import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {ErrorHandlerService} from './error.handler.service';
import {TranslationService} from 'angular-l10n';
import {ToastrService} from 'ngx-toastr';
import {ActionStatus, ErrorableResult} from '@planeta/planeta-core';
import {Observable} from 'rxjs/Observable';
import {catchError, map} from 'rxjs/operators';
import {City} from '../models/City';
import {Country} from '../interfaces/Country';

@Injectable()
export class DictionaryService extends ErrorHandlerService {
    private countriesListUrl = '/api/util/country-list.json';
    private citiesBySubstringUrl = '/api/util/cities-list-by-substring';
    private cityByIdUrl = '/api/util/city-by-id';

    constructor(private http: HttpClient,
                toastr: ToastrService,
                translation: TranslationService) {
        super(toastr, translation);
    }

    /**
     * Достает список стран
     */
    getCountries() {
        return this.http.get(this.countriesListUrl).pipe(
            map((response: ActionStatus<Country[]>) => this.successHandler(response)),
            catchError(this.errorHandler(null))
        );
    }

    /**
     * Получает список городов по строке подзапроса
     * @returns {Observable<ErrorableResult>}
     */
    getCitiesBySubstring(data): Observable<ErrorableResult> {
        return this.http.get(this.citiesBySubstringUrl, {params: data}).pipe(
            map((response: ActionStatus<City[]>) => this.successHandler(response)),
            catchError(this.errorHandler(null))
        );
    }

    getCityById(cityId: number) {
        return this.http.get(this.cityByIdUrl, {params: {cityId: cityId.toString()}}).pipe(
            map((response: ActionStatus<City>) => this.successHandler(response)),
            catchError(this.errorHandler(null))
        );
    }
}

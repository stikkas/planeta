import {DeliveryAddress} from './DeliveryAddress';

/**
 * Данные о заказе на странице моих покупок
 */

export class OrderInfoRewardsForProfilePage {
    deliveryAddress: DeliveryAddress;
    deliveryType: string;
    deliveryTypeName: string;
    deliveryTypeDescription: string;
    deliveryTypeNameEn: string;
    deliveryTypeDescriptionEn: string;
    orderId: number;
    orderType: string;
    buyerId: number;
    paymentType: string  = 'NOT_SET';
    paymentStatus: string = 'PENDING';
    paymentStatusHuman: string = 'Ожидание';
    errorCode: string;
    orderObjectsCount: number;
    orderObjectPrice: number;
    orderObjectName: string;
    orderObjectImageUrl: string;
    questionToBuyer: string;
    orderObjectComment: string;
    totalPrice : number;
    timeAdded: number;
    timeUpdated: number;
    reserveTimeExpired: Date;

    campaignStatus: string;

    /**
     * Идентификатор проекта
     */
    campaignId: number;

    /**
     * статус проекта
     */
    status: string;

    /**
     * Путь к заглавной картинке проекта
     */
    imageUrl: string;

    /**
     * Название проекта с html разметкой
     */
    nameHtml: string;

    /**
     *  Алиас проекта
     */
    webCampaignAlias: string;

    /**
     * Короткое описание с html разметкой
     */
    shortDescriptionHtml: string;

    /**
     * Конечная сумма, цель проекта
     */
    targetAmount: number;

    /**
     * Собранная сумма на данный момент
     */
    collectedAmount: number;

    /**
     * Статус завершенного проекта - успешный или нет
     */
    targetStatus: string;

    /**
     * время окончания проекта
     */
    timeFinish: number;
}

import {OrderObjectType} from './enums/OrderObjectType';
import {TransactionType} from './enums/TransactionType';
import {PaymentStatus} from './enums/PaymentStatus';

/**
 * Модель данных финансовой операции пользователя
 */
export class BalanceOperation {

    amountFee = 0;
    amountNet = 0;

    /**
     * Комментарий
     * @type {string}
     */
    comment = '';

    /**
     * Номер заказа
     */
    orderId = 0;

    /**
     * Ти ордера
     * @type {null}
     */
    orderObjectType: OrderObjectType = null;

    /**
     * Id профиля пользователя
     */
    profileId = 0;

    /**
     * Время создания записи
     */
    timeAdded = 0;

    /**
     * Id транзакции
     */
    transactionId = 0;

    /**
     * Тип транзакции
     */
    transactionType: TransactionType = null;

    /**
     * Статус платежа
     * @type {PaymentStatus}
     */
    paymentStatus: PaymentStatus = null;

    /**
     *
     */
    topayTransactionId = 0;

    /**
     * Название платежного метода на русском
     * @type {string}
     */
    paymentMethodNameRu = '';

    /**
     * Название платежного метода на английском
     * @type {string}
     */
    paymentMethodNameEn = '';
}

import {DeliveryType} from './enums/DeliveryType';
import {CustomerContacts} from './CustomerContacts';
import {Address} from './Address';

export class DeliveryInfo {

    /**
     * Тип доставки
     * @type {DeliveryType}
     */
    deliveryType: DeliveryType = DeliveryType.NOT_SET;

    /**
     * Id способа доставки. По умолчанию - Доставка почтой( -1 )
     * @type {number}
     */
    serviceId = 0;

    /**
     * Адрес доставки
     * @type {Address}
     */
    address: Address = new Address();

    /**
     * Контакты покупателя
     * @type {CustomerContacts}
     */
    customerContacts: CustomerContacts = new CustomerContacts();


    constructor() {

    }
}


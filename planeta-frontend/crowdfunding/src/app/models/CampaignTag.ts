export class CampaignTag {

    /**
     * Id категории
     * @type {number}
     */
    id = 0;

    /**
     * Название категории
     * @type {string}
     */
    name = '';

    /**
     * Название категории на английском
     * @type {string}
     */
    engName = '';

    /**
     * Алиас категории
     * @type {string}
     */
    mnemonicName = '';

    /**
     * Вес категории
     * @type {number}
     */
    preferredOrder = 0;

    /**
     * Алиас спонсора категории
     * @type {string}
     */
    sponsorAlias = '';

    /**
     * Показывать в редакторе
     * @type {boolean}
     */
    editorVisibility = true;

    /**
     * Показывать в поиске
     * @type {boolean}
     */
    visibleInSearch = true;

    /**
     * Показывать в благотворительности
     * @type {boolean}
     */
    charityVisibility = false;

    /**
     * Является ли специальным проектом
     * @type {boolean}
     */
    specialProject = false;

    constructor() {

    }
}

export class ProfileDashboardLink {

    nameCode = '';
    iconSrc = '';
    href = '';
    queryParams: any = null;

    constructor(nameCode: string, iconSrc: string, href: string, queryParams?: any) {
        this.nameCode = nameCode;
        this.iconSrc = iconSrc;
        this.href = href;

        if (queryParams) {
            this.queryParams = queryParams;
        }
    }
}

export class PaymentMethod {
    alias = '';
    card = true;
    children: PaymentMethod[] = [];
    name = '';
    nameEn = '';
    description = '';
    descriptionEn = '';
    id = 0;
    imageUrl = '';
    internal = false;
    mobile: false;
    needPhone = false;
    parentId = 0;
    promo = false;

    constructor() {

    }
}

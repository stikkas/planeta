import {Gender} from '../enums/Gender';

export class ProfileDTO {

    displayName = '';
    countryId = 0;
    userBirthDate: Date = null;
    userGender: Gender = null;
    alias = '';
    phoneNumber = '';
    cityId = 0;
    summary = '';
}

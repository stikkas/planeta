/**
 * Новость, добавляемая в проект
 */
export class PostDTO {
    profileId: number;
    campaignId: number;
    postText: string;
    title: string;
    timeAdded: number;
}
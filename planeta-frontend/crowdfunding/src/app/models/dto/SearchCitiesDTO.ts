import {SearchTypeahead} from '../../core/modules/pl-forms/interface/SearchTypeahead';

export class SearchCitiesDTO implements SearchTypeahead {

    countryId = 0;
    regionId = 0;
    query = '';
    limit = 20;
}

export class SubscriptionsDTO {

    /**
     * Получать рассылки Планеты
     * @type {boolean}
     */
    receiveNewsletters = false;

    /**
     * Получать письма с информацией из моих проектов
     * @type {boolean}
     */
    receiveMyCampaignNewsletters = false;
}

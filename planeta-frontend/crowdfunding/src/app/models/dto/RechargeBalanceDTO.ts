import {PaymentData} from '../PaymentData';

export class RechargeBalanceDTO {
    amount = 100;
    paymentData = new PaymentData();
}

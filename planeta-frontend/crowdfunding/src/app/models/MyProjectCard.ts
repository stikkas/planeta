/**
 * Данные о компании на странице моих проектов
 */
import {ShareForProjectCard} from './ShareForProjectCard';

export interface MyProjectCard {
    /**
     * Идентификатор проекта
     */
    campaignId: number;

    /**
     * статус проекта
     */
    status: string;

    /**
     * Причина отклонения, для отклоненных проектов
     */
    declineCase: string;

    /**
     * Путь к заглавной картинке проекта
     */
    imageUrl: string;

    /**
     * Название проекта с html разметкой
     */
    nameHtml: string;

    /**
     *  Алиас проекта
     */
    webCampaignAlias: string;

    /**
     * Короткое описание с html разметкой
     */
    shortDescriptionHtml: string;

    /**
     * Конечная сумма, цель проекта
     */
    targetAmount: number;

    /**
     * Собранная сумма на данный момент
     */
    collectedAmount: number;

    /**
     * Сумма, которую пожертвовали сегодня
     */
    todayPurchase: number;

    /**
     * Кол-во посещений страницы проекта
     */
    visits: number;

    /**
     * Кол-во посещений страницы проекта за сегодня
     */
    todayVisits: number;

    /**
     * Кол-во покупок
     */
    purchaseCount: number;

    /**
     * Кол-во покупок за сегодня
     */
    todayPurchaseCount: number;

    /**
     * Кол-во комментариев
     */
    reviews: number;

    /**
     * Кол-во комментариев за сегодня
     */
    todayReviews: number;

    /**
     * Статус завершенного проекта - успешный или нет
     */
    targetStatus: string;

    /**
     * Причина постановки на паузу проекта
     */
    pauseCase: string;

    /**
     * ид последней новости в проекте
     */
    newsId: number;

    /**
     * название новости
     */
    newsTitle: string;

    /**
     * дата публикации новости
     */
    newsTimeAdded: Date;

    /**
     * время окончания проекта
     */
    timeFinish: Date;

    /**
     * покупки пользователя
     */
    shares: ShareForProjectCard[];
}

export class Address {
    addressId = 0;
    countryId = 1;
    city = '';
    zipCode = '';
    street = '';

    // TODO: телефона и имени не должно быть в этой сущности. Надо выносить в customerContacts
    phone = '';
    customerName: string = '';

    constructor() {

    }
}

export class ProfileSites {

    /**
     * Адрес пользователя в Фейсбуке. По умолчанию - пустая строка
     * @type {string}
     */
    facebookUrl = '';

    /**
     * Адрес пользователя в Google+. По умолчанию - пустая строка
     * @type {string}
     */
    googleUrl = '';

    /**
     * Id профиля пользователя
     * @type {number}
     */
    profileId = 0;

    /**
     * Адрес сайта пользователя. По умолчанию - пустая строка
     * @type {string}
     */
    siteUrl = '';

    /**
     * Адрес пользователя в Твиттере. По умолчанию - пустая строка
     * @type {string}
     */
    twitterUrl = '';

    /**
     * Адрес пользователя в Контаче. По умолчанию - пустая строка
     * @type {string}
     */
    vkUrl = '';

    constructor() {}
}

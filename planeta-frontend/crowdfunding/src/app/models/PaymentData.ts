import {DeliveryInfo} from './DeliveryInfo';

export class PaymentData {
    /**
     * Id информации о покупке, сохраненной на серваке
     * @type {number}
     */
    infoId = 0;

    /**
     * Информация о доставке
     * @type {DeliveryInfo}
     */
    deliveryInfo: DeliveryInfo = new DeliveryInfo();

    /**
     * Email покупателя
     * @type {string}
     */
    email = '';

    /**
     * Ответ на вопрос автора
     * @type {string}
     */
    reply = '';

    /**
     * Дополнительный телефон для оплаты (только для мобильных платежей)
     * @type {string}
     */
    paymentPhone = '';

    /**
     * Нужен ли дополнительный телефон для оплаты (только для мобильных платежей)
     * @type {boolean}
     */
    needPhone = false;

    /**
     * Id метода оплаты
     * @type {number}
     */
    paymentMethodId = 0;

    /**
     * Частично оплатить покуку с баланса
     * @type {boolean}
     */
    partialFromBalance: boolean = false;

    /**
     * Полностью оплатить покуку с баланса
     * @type {boolean}
     */
    fullFromBalance: boolean = false;


    constructor() {

    }
}

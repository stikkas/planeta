import {News} from './News';

/**
 * Объект апдейта, который позволяет группировать новости профиля по дням
 */
export class Update {

    /**
     * дата (день) публикации
     */
    date: number;

    /**
     * Коллекция новостей
     * @type {News[]}
     */
    news: News[] = [];

    constructor(date: number, news: News[]) {
        this.date = date;
        this.news = news;
    }
}

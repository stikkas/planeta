import {NewsType} from './enums/NewsType';

/**
 * Новости
 */
export class News {

    /**
     * Время добавления новости
     */
    timeAdded: number;

    /**
     * Id проекта
     */
    campaignId: number;

    /**
     * Название проекта
     */
    campaignName: string;

    /**
     * Процент сборов проекта
     */
    campaignProgress: number;

    /**
     * URL изображения профиля автора проекта
     */
    authorImageUrl: string;

    /**
     * Заголовок новости
     */
    postName: string;

    /**
     * Текст новости
     */
    text: string;

    /**
     * Тип новости
     */
    type: NewsType;
}

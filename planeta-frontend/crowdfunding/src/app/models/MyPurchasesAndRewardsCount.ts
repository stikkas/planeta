import {OrderObjectType} from './enums/OrderObjectType';

export class MyPurchasesAndRewardsCount {
    count: number = 0;
    orderObjectType: OrderObjectType;
    campaignTagMnemonic: string;
}

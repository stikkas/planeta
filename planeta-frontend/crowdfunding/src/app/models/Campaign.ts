import {CampaignStatus} from './enums/CampaignStatus';
import {CampaignTag} from './CampaignTag';

export class Campaign {

    /**
     * Id профиля автора
     * @type {number}
     */
    authorProfileId = 0;

    /**
     * Алиас проекта
     * @type {string}
     */
    campaignAlias = '';

    /**
     * Id проекта
     * @type {number}
     */
    campaignId = 0;

    /**
     * Id страны
     * @type {number}
     */
    countryId = 0;

    /**
     * Id региона
     * @type {number}
     */
    regionId = 0;

    /**
     * Id города
     * @type {number}
     */
    cityId = 0;

    /**
     * Планируемая сумма сбора
     * @type {number}
     */
    targetAmount = 0;

    /**
     * Собранная сумма
     * @type {number}
     */
    collectedAmount = 0;

    /**
     * Id профиля, который создавал проект
     * @type {number}
     */
    creatorProfileId = 0;

    /**
     * Название проекта
     * @type {string}
     */
    name = '';

    /**
     * Описание
     * @type {number}
     */
    description = 0;

    /**
     * Описание в формате html
     * @type {string}
     */
    descriptionHtml = '';

    /**
     * Короткое описание
     * @type {string}
     */
    shortDescription = '';

    /**
     * Короткое описание в формате html
     * @type {string}
     */
    shortDescriptionHtml = '';

    /**
     * Адрес изображения
     * @type {string}
     */
    imageUrl = '';

    /**
     * Главная категори проекта
     * @type {{}}
     */
    mainTag: CampaignTag = null;

    /**
     * Категории проекта
     * @type {Array}
     */
    tags: CampaignTag[] = [];

    /**
     * Статус проекта
     * @type {CampaignStatus}
     */
    status: CampaignStatus = CampaignStatus.ACTIVE;

    constructor(object?: any) {
        if (object) {
            this.authorProfileId = object.authorProfileId;
            this.campaignAlias = object.campaignAlias;
            this.campaignId = object.campaignId;
            this.countryId = object.countryId;
            this.regionId = object.regionId;
            this.cityId = object.cityId;
            this.targetAmount = object.targetAmount;
            this.collectedAmount = object.collectedAmount;
            this.creatorProfileId = object.creatorProfileId;
            this.name = object.name;
            this.description = object.description;
            this.descriptionHtml = object.descriptionHtml;
            this.shortDescription = object.shortDescription;
            this.shortDescriptionHtml = object.shortDescriptionHtml;
            this.imageUrl = object.imageUrl;
            this.mainTag = object.mainTag;
            this.tags = object.tags;
            this.status = object.status;
        }
    }
}

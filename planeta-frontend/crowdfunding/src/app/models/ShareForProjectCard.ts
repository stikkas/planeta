import {PaymentStatus} from './enums/PaymentStatus';

export interface ShareForProjectCard {
    /**
     * Идентификатор вознаграждения
     */
    shareId: number

    /**
     * название
     */
    nameHtml: string

    /**
     * оплаченная сумма
     */
    totalPrice: number

    /**
     * количество купленных вознаграждений
     */
    amount: number

    /**
     * статус покупки
     */
    status: PaymentStatus
}

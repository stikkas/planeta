export class DeliveryAddress {
    id: number;
    address: number;
    city: number;
    country: number;
    zipCode: number;
    phone: number;
    fio: number;
    orderId: number;

    constructor() {}
}

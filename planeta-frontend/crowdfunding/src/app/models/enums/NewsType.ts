/**
 * Типы новостей в ленте профиля
 */
export enum NewsType {
    /**
     * Проект запущен
     */
    CAMPAIGN_STARTED = 'CAMPAIGN_STARTED',

    /**
     * Проект возобновлен
     * @type {string}
     */
    CAMPAIGN_RESUMED = 'CAMPAIGN_RESUMED',

    /**
     * Проект успешно завершен
     * @type {string}
     */
    CAMPAIGN_FINISHED_SUCCESS = 'CAMPAIGN_FINISHED_SUCCESS',

    /**
     * Проект завершен неуспешно
     * @type {string}
     */
    CAMPAIGN_FINISHED_FAIL = 'CAMPAIGN_FINISHED_FAIL',

    /**
     * Проект достиг 50% цели сборов
     * @type {string}
     */
    CAMPAIGN_REACHED_50 = 'CAMPAIGN_REACHED_50',

    /**
     * Проект достиг 100% цели сборов
     * @type {string}
     */
    CAMPAIGN_REACHED_100 = 'CAMPAIGN_REACHED_100',

    /**
     * Новость
     * @type {string}
     */
    POST = 'POST'
}

export enum OrderObjectType {
    SHARE = 'SHARE',
    PRODUCT = 'PRODUCT',
    BIBLIO = 'BIBLIO'
}

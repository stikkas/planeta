export enum CampaignTargetStatus {
    NONE = 'NONE',
    SUCCESS = 'SUCCESS',
    FAIL = 'FAIL'
}

export enum PaymentStatus {

    /**
     * ожидает оплаты
     * @type {string}
     */
    PENDING = 'PENDING',

    /**
     * Завершен
     * @type {string}
     */
    COMPLETED = 'COMPLETED',

    /**
     * Отменен
     * @type {string}
     */
    CANCELLED = 'CANCELLED',

    /**
     * Неуспешный
     * @type {string}
     */
    FAILED = 'FAILED',

    /**
     * Зарезервирован
     * @type {string}
     */
    RESERVED = 'RESERVED',

    /**
     * Просрочен
     * @type {string}
     */
    OVERDUE = 'OVERDUE'
}

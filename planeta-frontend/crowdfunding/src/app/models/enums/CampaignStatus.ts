export enum CampaignStatus {
    /**
     * Все
     */
    ALL = 'ALL',

    /**
     * Черновик
     * @type {string}
     */
    DRAFT = 'DRAFT',

    /**
     * Не запущен
     * @type {string}
     */
    NOT_STARTED = 'NOT_STARTED',

    /**
     * На паузе
     * @type {string}
     */
    PAUSED = 'PAUSED',

    /**
     * Активный
     * @type {string}
     */
    ACTIVE = 'ACTIVE',

    /**
     * Завершенный
     * @type {string}
     */
    FINISHED = 'FINISHED',

    /**
     * Удаленный
     * @type {string}
     */
    DELETED = 'DELETED',

    /**
     * Отклоненный
     * @type {string}
     */
    DECLINED = 'DECLINED',

    /**
     * На доработке
     * @type {string}
     */
    PATCH = 'PATCH',

    /**
     * Одобрен менеджерами
     * @type {string}
     */
    APPROVED = 'APPROVED'
}

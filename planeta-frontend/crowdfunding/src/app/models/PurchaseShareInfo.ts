import {ShareDetailed} from './ShareDetailed';
import {Campaign} from './Campaign';
import {Address} from './Address';

export class PurchaseShareInfo {
    id: number;
    shareDetailed: ShareDetailed = new ShareDetailed();
    campaign: Campaign = new Campaign();
    authorName: string = '';
    donateAmount: number = 0;
    quantity: number = 0;
    storedAddress: Address = new Address();

    constructor() {

    }
}

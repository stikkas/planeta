import {LinkedDelivery} from '../interfaces/LinkedDelivery';

export class ShareDetailed {

    price = 0;
    linkedDeliveries: LinkedDelivery[];
    shareId = 0;
    questionToBuyer = '';
    name = '';

    constructor(object?: any) {
        if (object) {
            this.price = object.price;
            this.linkedDeliveries = object.linkedDeliveries;
            this.shareId = object.shareId;
            this.questionToBuyer = object.questionToBuyer;
            this.name = object.name;
        }
    }
}

import {Gender} from './enums/Gender';

enum ProfileType {
    NOT_SET,
    USER_ACTIVE,
    USER_BOUNCED,
    USER_SPAMMER,
    USER_DELETED,
    GROUP_PENDING,
    GROUP_ACTIVE,
    HIDDEN_GROUP_ACTIVE,
    GROUP_REJECTED,
    GROUP_ACTIVE_REQUEST_OFFICIAL,
    GROUP_ACTIVE_OFFICIAL,
    GROUP_DELETED
}

enum ProfileStatus {
    USER,
    GROUP,
    HIDDEN_GROUP
}

export class Profile {
    profileId: number = 0;
    alias: string = '';
    displayName: string = '';
    profileType: ProfileType;
    status: ProfileStatus;
    imageUrl: string;
    summary: string = '';
    userGender: Gender;
    phoneNumber = '';
    countryId = 0;
    cityId = 0;
    userBirthDate: Date;
    receiveMyCampaignNewsletters: boolean;
    receiveNewsletters: boolean;
}

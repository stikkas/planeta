import {CampaignStatus} from "./enums/CampaignStatus";
import {CampaignTargetStatus} from "./enums/CampaignTargetStatus";

export class MyCampaignRequestParams {
    limit: number;
    offset: number;
    status: CampaignStatus;
    targetStatus: CampaignTargetStatus;
}

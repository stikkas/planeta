export class MyCampaignsStatusesCountDTO {
    active = 0;
    draft = 0;
    onModeration = 0;
    finished = 0;
    declined = 0;

    constructor() {

    }
}

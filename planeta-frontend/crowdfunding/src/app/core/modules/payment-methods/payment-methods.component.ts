import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

import {Language, TranslationService} from 'angular-l10n';
import {ToastrService} from 'ngx-toastr';

import {ActionStatus, PlErrorable} from '@planeta/planeta-core';
import {PaymentData} from '../../../models/PaymentData';
import {PurchaseShareInfo} from '../../../models/PurchaseShareInfo';
import {PaymentService} from '../../../services/payment.service';
import {PaymentMethod} from '../../../models/PaymentMethod';
import {ProfileService} from '../../../services/profile.service';

/**
 * Компонент выбора способов оплаты покупки
 */
@Component({
    selector: 'pl-payment-methods',
    templateUrl: './payment-methods.component.html',
    styleUrls: ['./payment-methods.component.less']
})
export class PaymentMethodsComponent implements OnInit {

    /**
     * Id платёжного метода "С платёжного кошелька Planeta.ru"
     * @type {number}
     */
    private FULL_PAYMENT_FROM_BALANCE_METHOD_ID: number = 10;

    /**
     * Текущий язык
     */
    @Language() lang: string;

    /**
     * Модель, в которой хранится ID выбранного способа оплаты
     */
    @Input() model: PaymentData;

    /**
     * Сохраненная ифна и покупке
     */
    @Input() purchaseInfo: PurchaseShareInfo;

    /**
     * Объект с ошибками
     */
    @Input() errors: PlErrorable;

    /**
     * Эмиттер, который синхронизирует данные в родительской компоненте
     * @type {EventEmitter<any>}
     */
    @Output() modelChange: EventEmitter<PaymentData> = new EventEmitter();

    /**
     * Доступные платёжный методы
     * @type {Array}
     */
    methods: PaymentMethod[] = [];

    /**
     * Тут хранится информация о выбранном методе оплаты первого уровня, для удобства
     * @type {any}
     */
    selectedFirstLevelMethod: PaymentMethod = new PaymentMethod();

    /**
     * Тут хранится информация о выбранном методе оплаты второго уровня, для удобства
     * @type {any}
     */
    selectedSecondLevelMethod: PaymentMethod = new PaymentMethod();

    /**
     * Показывать/не показывать вкладку с другими способами оплаты.
     * Показывактся, когда выбран метод оплаты "Другие способы"
     * @type {boolean}
     */
    showOtherMethods = false;

    /**
     * Доп настройки для инпута с телефоном
     */
    phoneInputSettings = {
        mask: ['+', '7', '(', /[1-9]/, /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/]
    };

    constructor(private paymentService: PaymentService,
                private toastr: ToastrService,
                private translationService: TranslationService,
                private profileService: ProfileService) {
    }

    ngOnInit() {
        this.getPaymentMethods();
    }

    /**
     * Получает список доступных платёжныых методов с сервера
     */
    getPaymentMethods() {
        this.paymentService.getPaymentMethods().subscribe(
                (response: ActionStatus<PaymentMethod[]>) => {
                    if (response.success) {
                        this.methods = response.result;
                        // Выбираем по умолчанию первый метод из списка
                        this.changeFirstLevelMethod(response.result[0]);
                    }
                },
                (error: Error) => {
                    this.toastr.error(
                            this.translationService.translate('errors.server-error.message'),
                            this.translationService.translate('errors.server-error.title')
                    );
                    console.log(error.message);
                }
        );
    }

    /**
     * Устанавливает новое значение выбранного метода оплаты первого уровня
     * @param {PaymentMethod} method
     */
    changeFirstLevelMethod(method: PaymentMethod) {
        if (method.id !== this.model.paymentMethodId) {
            this.selectedFirstLevelMethod = method;
            this.model.paymentMethodId = method.id;
            this.model.needPhone = method.needPhone;
            this.modelChange.emit(this.model);
            this.showOtherMethods = method.alias === 'OTHER';
        }
    }

    /**
     * Проверяет, является метод оплаты первого уровня активным (выбранным)
     * @param {number} methodId
     */
    isActiveFirstLevel(methodId: number) {
        return !!(this.selectedFirstLevelMethod && this.selectedFirstLevelMethod.id === methodId);
    }

    /**
     * Устанавливает новое значение выбранного метода оплаты второго уровня
     * @param {PaymentMethod} method
     */
    changeSecondLevelMethod(method: PaymentMethod) {
        if (method.id !== this.model.paymentMethodId) {
            this.selectedSecondLevelMethod = method;
            this.model.paymentMethodId = method.id;
            this.model.needPhone = method.needPhone;
            this.modelChange.emit(this.model);
        }
    }

    /**
     * Проверяет, является метод оплаты второго уровня активным (выбранным)
     * @param {number} methodId
     */
    isActiveSecondLevel(methodId: number) {
        return !!(this.selectedSecondLevelMethod && this.selectedSecondLevelMethod.id === methodId);
    }

    /**
     * Возвращает название метода с учётом текущего языка
     */
    getMethodName(method: PaymentMethod) {
        return this.lang === 'en' ? method.nameEn : method.name;
    }

    /**
     * Возвращает описание метода с учётом текущего языка
     */
    getMethodDescription(method: PaymentMethod) {
        return this.lang === 'en' ? method.descriptionEn : method.description;
    }

    /**
     * Проверить наличие ошибки по имени филда
     * @param {string} field
     */
    hasError(field: string) {
        return this.errors.hasError(field);
    }

    /**
     * Получить код ошибки по имени филда
     * @param {string} field
     */
    getError(field: string) {
        return this.translationService.translate(`errors.${this.errors.getError(field)}`);
    }

    /**
     * Работает на изменение paymentData.fullFromBalance
     */
    fullFromBalanceChanged() {
        if (this.model.fullFromBalance) {
            this.model.paymentMethodId = this.FULL_PAYMENT_FROM_BALANCE_METHOD_ID;
            this.showOtherMethods = false;
        } else {
            this.model.paymentMethodId = 0;
            this.selectedFirstLevelMethod = new PaymentMethod();
            this.selectedSecondLevelMethod = new PaymentMethod();
        }
    }

    getBalance() {
        return this.profileService.getMyBalance();
    }
}

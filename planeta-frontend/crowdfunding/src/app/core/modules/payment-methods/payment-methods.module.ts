import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import {LocalizationModule} from 'angular-l10n';
import {TextMaskModule} from 'angular2-text-mask';
import {TooltipModule} from 'ngx-bootstrap';
import {PlCoreModule} from '@planeta/planeta-core';
import {PaymentMethodsComponent} from './payment-methods.component';
import {PlFormsModule} from '../pl-forms/pl-forms.module';

@NgModule({
    imports: [
        BrowserModule,
        FormsModule,
        PlFormsModule,
        TextMaskModule,
        LocalizationModule,
        ReactiveFormsModule,
        TooltipModule.forRoot(),
        PlCoreModule,
    ],
    exports: [
        PaymentMethodsComponent
    ],
    declarations: [
        PaymentMethodsComponent
    ]
})
export class PaymentMethodsModule {}


import {Component} from '@angular/core';
import {TranslationService} from 'angular-l10n';
import {BaseInput} from '../models/BaseInput';

/**
 * Компонент базового инпута с подсветкой ошибок через ErrorService.
 * Позволяет настраивать маску вводимых значений в инпут через принимаемый параметр @Input() mask. Документацию
 * по маскам смотрим тут https://github.com/text-mask/text-mask/blob/master/componentDocumentation.md#readme
 */
@Component({
    selector: 'pl-input',
    templateUrl: './pl-input.component.html',
    styleUrls: ['./pl-input.component.less']
})
export class PlInputComponent extends BaseInput {

    constructor(public translationService: TranslationService) {
        super(translationService);
    }
}

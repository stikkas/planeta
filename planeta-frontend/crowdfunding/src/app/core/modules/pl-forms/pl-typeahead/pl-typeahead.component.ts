import {
    Component,
    ElementRef,
    EventEmitter,
    HostListener,
    Input, OnChanges,
    OnInit,
    Output,
    SimpleChanges,
    ViewChild
} from '@angular/core';
import {Language, TranslationService} from 'angular-l10n';
import {SearchTypeahead} from '../interface/SearchTypeahead';
import {DictionaryService} from '../../../../services/dictionary.service';
import {City} from '../../../../models/City';
import {PlInputLabelType} from '../models/enums/PlInputLabelType';
import {PlErrorable} from '@planeta/planeta-core';
import {PlInputSettings} from '../models/PlInputSettings';
import {SearchCitiesDTO} from '../../../../models/dto/SearchCitiesDTO';

/**
 * Компонент базового инпута с typeahead и подсветкой ошибок через ErrorService.
 */
@Component({
    selector: 'pl-typeahead',
    templateUrl: './pl-typeahead.component.html',
    styleUrls: ['./pl-typeahead.component.less']
})
export class PlTypeaheadComponent implements OnInit, OnChanges {

    @ViewChild('inputControl') inputControl: ElementRef;

    /**
     * Текущий язык
     */
    @Language() lang: string;

    /**
     * Филд для ошибок
     */
    @Input() field?: string;

    /**
     * дто с данными для поиска
     */
    @Input() dto: SearchCitiesDTO;

    /**
     * Значение инпута (текстовое отображение)
     * @type {string}
     * @private
     */
    _value = '';

    /**
     * Объект с ошибками
     */
    @Input() errors?: PlErrorable;

    /**
     * Код из файла локализации для заголовка инпута
     */
    @Input() labelCode: string;

    /**
     * Дополнительные настройки
     */
    @Input() settings = new PlInputSettings();

    /**
     * Модель, где хранится строка, введённая в инпут
     */
    @Input() model: any;

    /**
     * Эмиттер, который синхронизирует данные в родительской модели
     * @type {EventEmitter<any>}
     */
    @Output() modelChange: EventEmitter<any> = new EventEmitter<any>();

    /**
     * Типы лейблов
     * @type {PlInputLabelType}
     */
    labelTypes = PlInputLabelType;

    /**
     * Варианты подсказок
     * @type {any[]}
     */
    variants: any[] = [];

    /**
     * Индекс выбранной опции
     * @type {number}
     */
    selectedOptionIndex = 0;

    /**
     * Флаг, который открывает закрывает выпадашку с опциями
     * @type {boolean}
     */
    open = false;

    constructor(private translationService: TranslationService,
                private dictionaryService: DictionaryService) {

    }

    /**
     * Листенер на изменения значений переменных
     * @param {SimpleChanges} changes
     */
    ngOnChanges(changes: SimpleChanges): void {
        if (changes.settings) {
            this.settings = new PlInputSettings(changes.settings.currentValue);
        }
    }

    ngOnInit() {
        if (this.model > 0) {
            this.dictionaryService.getCityById(this.model).subscribe(([result]) => {
                if (result) {
                    if (this.lang === 'ru') {
                        this._value = result.nameRus;
                    } else {
                        this._value = result.nameEn;
                    }
                }
            });
        }
    }

    /**
     * Получить value для инпута
     */
    getValue(): string {
        return this.model > 0 ? this._value : '';
    }

    /**
     * Работает на change инпута, эмитит данные в родителя
     * @param newValue
     */
    changeInput(newValue: any): void {
        if (newValue !== '') {
            this.dto.query = newValue;
            this.dictionaryService.getCitiesBySubstring(this.dto).subscribe(([result]) => {
                this.variants = result;
                this.open = true;
            });
        } else {
            this.variants = [];
        }

        if (this.errors && this.field && this.settings.required) {
            if (this.inputControl.nativeElement.classList.contains('error') && newValue !== '') {
                this.inputControl.nativeElement.classList.remove('error');
                const newErrors = this.errors.errors.filter(item => item.field !== this.field);
                this.errors.reset(newErrors);
            } else if (!this.inputControl.nativeElement.classList.contains('error') && newValue === '') {
                this.inputControl.nativeElement.classList.add('error');
            }
        } else if (newValue === '' && !this.settings.required) {
            if (this.inputControl.nativeElement.classList.contains('error')) {
                this.inputControl.nativeElement.classList.remove('error');
                this.errors.reset();
            }
        }
    }

    isActive(index): boolean {
        return index === this.selectedOptionIndex;
    }

    /**
     * Определяет позицию тултипа
     * @returns {string}
     */
    tooltipPositionTop(): string {
        return window.innerWidth <= 1024 ? 'left' : 'top';
    }

    /**
     * Получает перевод плейсхолдера
     * @returns {string}
     */
    getPlaceholderTranslate(): string {
        let placeholder = '';
        if (this.settings.placeholder !== '') {
            placeholder = this.translationService.translate(this.settings.placeholder);
        }
        return placeholder;
    }

    /**
     * Проверить на наличие ошибки
     * @returns {boolean}
     */
    hasError() {
        let response = null;
        if (this.errors && this.field) {
            response = this.errors.hasError(this.field);
        }

        return response;
    }

    /**
     * Получить код ошибки
     * @returns {string | any}
     */
    getError(): string {
        let response = null;
        if (this.errors && this.field) {
            let code = this.errors.getError(this.field);
            if (!code) {
                code = 'errors.field_required';
            } else {
                code = `errors.${code}`;
            }
            response = this.translationService.translate(code);
        }

        return response;
    }

    /**
     * Закрывает выпадашку с опциями (для click-outside)
     */
    close(): void {
        this.open = false;
    }

    /**
     * Получает имя опции из списка с учётом языка
     * @param option
     * @returns {string}
     */
    getOptionName(option: City): string {
        let name = '';

        if (this.lang === 'ru') {
            name = option.nameRus;
        } else if (this.lang === 'en') {
            name = option.nameEn;
        }

        // подкрашиваем (выделяем жирным) символы, которые мы ввели
        const query = this.dto.query.toLowerCase();
        return name.toLowerCase().replace(query, '<b>' + query + '</b>');
    }

    /**
     * Проставляет значение выбранной из списка опции в модель
     * @param option
     */
    setOption(option: City): void {
        this.model = option.objectId;
        this.open = false;

        if (this.lang === 'ru') {
            this._value = option.nameRus;
        } else if (this.lang === 'en') {
            this._value = option.nameEn;
        }

        this.modelChange.emit(option.objectId);
    }

    /**
     * Эвент листенер по нажатию клавиш для того, чтобы выбирать предложенкие из списка стрелочками и подтверждать
     * выбор по enter
     * @param {KeyboardEvent} event
     */
    @HostListener('window:keydown', ['$event'])
    keyboardInput(event: KeyboardEvent): void {
        if (event.code === 'Enter' && this.open) {
            const option = this.variants[this.selectedOptionIndex];
            if (option) {
                this.setOption(option);
            }
        }

        if (event.code === 'ArrowUp' && this.open) {
            if (this.selectedOptionIndex === 0) {
                this.selectedOptionIndex = this.variants.length - 1;
            } else {
                this.selectedOptionIndex--;
            }
        }

        if (event.code === 'ArrowDown' && this.open) {
            if (this.selectedOptionIndex === this.variants.length - 1) {
                this.selectedOptionIndex = 0;
            } else {
                this.selectedOptionIndex++;
            }
        }
    }
}

import {Component, HostListener, OnInit, ViewChild} from '@angular/core';
import {LocaleService, TranslationService} from 'angular-l10n';
import {BaseInput} from '../models/BaseInput';
import {BsDatepickerDirective, BsLocaleService} from 'ngx-bootstrap';

/**
 * Компонент базового инпута с DatePicker подсветкой ошибок через ErrorService.
 */
@Component({
    selector: 'pl-datepicker',
    templateUrl: './pl-datepicker.component.html',
    styleUrls: ['./pl-datepicker.component.less']
})
export class PlDatepickerComponent extends BaseInput implements OnInit {
    @ViewChild(BsDatepickerDirective) datepicker: BsDatepickerDirective;

    constructor(public translationService: TranslationService,
                private ngxlocaleService: BsLocaleService,
                private localeSService: LocaleService) {
        super(translationService);

        // По умолчанию выставляем локализацию на том языке, который выбран в интерфейсе
        this.ngxlocaleService.use(this.localeSService.getBrowserLanguage());
    }

    /**
     * Внутренняя модель
     */
    _model: Date;

    ngOnInit() {
        // Меняем локализацию ngx-datepicker при смене языка
        this.localeSService.defaultLocaleChanged.subscribe(locale => {
            if (locale === 'ru-RU') {
                this.ngxlocaleService.use('ru');
            } else {
                this.ngxlocaleService.use('en');
            }
        });

        // Датапикер работает с форматом Date.
        if (typeof this.model === 'object') {
            this._model = this.model;
        } else if (typeof this.model === 'number') {
            this._model = new Date(this.model);
        }
    }

    /**
     * Скрываем датапикер при скроллинге
     */
    @HostListener('window:scroll')
    onScrollEvent() {
        this.datepicker.hide();
    }

    /**
     * Срабатывает, когда мы выбираем дату из календаря, но не когда вводим ее руками в инпут
     * @param value: Date
     */
    onValueChange(value: Date) {
        let _value: any;

        if (value) {
            // Отдаем на выход значение такого же типа, какого оно приходило в компонент
            if (typeof this.model === 'object') {
                _value = value;
            } else if (typeof this.model === 'number') {
                _value = value.getTime();
            }

            this.model = _value;
            this.modelChange.emit(_value);
        }
    }
}

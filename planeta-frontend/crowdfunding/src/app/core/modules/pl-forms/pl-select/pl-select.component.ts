import {
    Component,
    DoCheck,
    EventEmitter,
    Input,
    IterableDiffers,
    OnChanges,
    OnInit,
    Output,
    SimpleChanges
} from '@angular/core';
import {Language, TranslationService} from 'angular-l10n';
import {PlInputLabelType} from '../models/enums/PlInputLabelType';
import {PlInputSettings} from '../models/PlInputSettings';

/**
 * Компонент базового select
 */
@Component({
    selector: 'pl-select',
    templateUrl: './pl-select.component.html',
    styleUrls: ['./pl-select.component.less']
})
export class PlSelectComponent implements OnInit, DoCheck, OnChanges {
    /**
     * Коллекция опций для выпадашки
     */
    @Input() options: any[];

    /**
     * Название поля, в котором хранится название опции на английском
     */
    @Input() nameEn: string;

    /**
     * Название поля, в котором хранится название опции на русском
     */
    @Input() nameRu: string;

    /**
     * Название поля, в котором хранится идентификатор опции
     */
    @Input() id: string;

    /**
     * Код из файла локализации для заголовка селекта
     */
    @Input() titleCode: string;

    /**
     * Модель где хранится ID выбранной опции
     */
    @Input() model: any;

    /**
     * Эмиттер, который синхронизирует данные в родительской модели
     * @type {EventEmitter<any>}
     */
    @Output() modelChange: EventEmitter<any> = new EventEmitter<any>();

    /**
     * Флаг, который открывает закрывает выпадашку с опциями
     * @type {boolean}
     */
    open = false;

    /**
     * Текущий язык
     */
    @Language() lang: string;

    /**
     * Данные выбранной опции
     * @type {{}}
     */
    selectedOption = {};

    differ: any;

    /**
     * Дополнительные настройки
     * @type {PlInputSettings}
     */
    @Input() settings = new PlInputSettings();

    /**
     * Типы лейблов
     * @type {PlInputLabelType}
     */
    labelTypes = PlInputLabelType;

    constructor(public translationService: TranslationService,
                differs: IterableDiffers) {

        this.differ = differs.find([]).create(null);
    }

    ngOnInit() {
        if (this.model && this.options) {
            this._setOption(true);
        }
    }

    ngDoCheck() {
        const change = this.differ.diff(this.options);
        if (change) {
            this._setOption();
        }
    }

    private _setOption(firstInitialize: boolean = true) {
        const self = this;
        const option = this.options.filter(opt => self.model === opt[self.id])[0];
        if (option) {
            this.setOption(option, firstInitialize);
        }
    }

    /**
     * Открывает/закрывает выпадашку с опциями селекта
     */
    toggleOpen() {
        this.open = !this.open;
    }

    /**
     * Закрывает выпадашку с опциями селекта (для click-outside)
     */
    close() {
        this.open = false;
    }

    /**
     * Проставляет значение выбранной из списка опции в модель
     * @param option
     * @param firstInitialize
     */
    setOption(option: any, firstInitialize: boolean = true) {
        this.model = option[this.id];
        this.selectedOption = option;

        if (!firstInitialize) {
            this.modelChange.emit(this.model);
        }
    }

    /**
     * Проверяет, является ли текущая опция выбранной (активной)
     * @param option
     * @returns {boolean}
     */
    isActive(option: any) {
        return this.selectedOption[this.id] === option[this.id];
    }

    /**
     * Получает имя опции из списка с учётом языка
     * @param option
     * @returns {string}
     */
    getOptionName(option: any) {
        let name = '';
        if (this.lang === 'ru') {
            name = option[this.nameRu];
        } else if (this.lang === 'en') {
            name = option[this.nameEn];
        }
        return name;
    }

    /**
     * Получает имя выбранной опции с учётом языка, которое подставляется в инпут
     * @returns {string}
     */
    getSelectedName() {
        let name = '';
        if (this.selectedOption[this.nameRu]) {
            if (this.lang === 'ru') {
                name = this.selectedOption[this.nameRu];
            } else if (this.lang === 'en') {
                name = this.selectedOption[this.nameEn];
            }
        } else {
            name = this.translationService.translate('pl-select.not-selected');
        }

        return name;
    }

    ngOnChanges(changes: SimpleChanges): void {
        if (changes.settings) {
            this.settings = new PlInputSettings(changes.settings.currentValue);
        }
    }
}

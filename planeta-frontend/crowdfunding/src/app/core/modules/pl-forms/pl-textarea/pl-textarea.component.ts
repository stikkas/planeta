import {Component} from '@angular/core';
import {TranslationService} from 'angular-l10n';
import {BaseInput} from '../models/BaseInput';

/**
 * Компонент базовой textarea с подсветкой ошибок через ErrorService.
 */
@Component({
    selector: 'pl-textarea',
    templateUrl: './pl-textarea.component.html',
    styleUrls: ['./pl-textarea.component.less']
})
export class PlTextareaComponent extends BaseInput {

    constructor(public translationService: TranslationService) {
        super(translationService);
    }
}

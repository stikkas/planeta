import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {TranslationService} from 'angular-l10n';
import {BaseInput} from '../models/BaseInput';
import {ActionStatus, PlAuthService} from '@planeta/planeta-core';
import {EmailStatus} from '../../../../models/enums/EmailStatus';
import {HttpClient} from '@angular/common/http';
import {ToastrService} from 'ngx-toastr';
import {Subscription} from 'rxjs/Subscription';

/**
 * Компонент базового инпута для email с проверкой на существующий email и возможностью авторизоваться
 */
@Component({
    selector: 'pl-email-with-login',
    templateUrl: './pl-email-with-login.component.html',
    styleUrls: ['./pl-email-with-login.component.less']
})
export class PlEmailWithLoginComponent extends BaseInput implements OnInit {

    @Output() blockVisibilityChanged: EventEmitter<boolean> = new EventEmitter();

    /**
     * Показывать/не показывать блок с авторизацией, если введен существующий email
     * @type {boolean}
     */
    showPass = false;

    /**
     * Валидный/не валидный email
     * @type {boolean}
     */
    validEmail = true;

    /**
     * Код ошибки
     * @type {string}
     */
    emailFieldError = '';

    /**
     * Листенер на логин/разлогин пользователя
     */
    authSubscriber: Subscription;

    constructor(public translationService: TranslationService,
                private authService: PlAuthService,
                private httpClient: HttpClient,
                private toastr: ToastrService) {
        super(translationService);
    }

    ngOnInit() {
        this.authService.getUserInfo().subscribe(([info]) => this.blockVisibilityChanged.emit(info == null));
        this.authSubscriber = this.authService.authChanged.subscribe(res => this.blockVisibilityChanged.emit(res == null));

        this.modelChange
                .debounceTime(300)
                .distinctUntilChanged()
                .subscribe(model => this.checkEmail(model));
    }

    /**
     * Открывает окно авторизации
     */
    showAuthModal() {
        this.authService.showLogin(this.model);
    }

    /**
     * Проверяет на валидность и существование email
     * @param email
     */
    private checkEmail(email: string) {
        this.httpClient.get('/api/public/email-check', {
            params: {
                'email': email
            }
        }).subscribe(
                (response: ActionStatus<EmailStatus>) => {
                    if (response.success) {
                        let disableBlock = true;
                        if (response.result === EmailStatus.ALREADY_EXIST) {
                            this.showPass = true;
                            this.validEmail = true;
                        } else if (response.result === EmailStatus.NEW_VALID_EMAIL || response.result === EmailStatus.MAY_BE_NOT_VALID) {
                            this.showPass = false;
                            this.validEmail = true;
                            disableBlock = false;
                        } else if (response.result === EmailStatus.NOT_VALID) {
                            this.validEmail = false;
                            this.showPass = false;
                            this.emailFieldError = 'errors.wrong-email';
                        }

                        this.blockVisibilityChanged.emit(disableBlock);
                    } else {
                        this.toastr.error(
                                this.translationService.translate('errors.server-error.message'),
                                this.translationService.translate('errors.server-error.title')
                        );
                    }
                },
                (error: Error) => {
                    console.log(error.message);
                    this.toastr.error(
                            this.translationService.translate('errors.server-error.message'),
                            this.translationService.translate('errors.server-error.title')
                    );
                }
        );
    }
}

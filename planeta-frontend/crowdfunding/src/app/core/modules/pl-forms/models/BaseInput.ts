import {Language, TranslationService} from 'angular-l10n';
import {ElementRef, EventEmitter, Input, OnChanges, Output, SimpleChanges, ViewChild} from '@angular/core';
import {PlErrorable} from '@planeta/planeta-core';
import {PlInputLabelType} from './enums/PlInputLabelType';
import {PlInputSettings} from './PlInputSettings';

export class BaseInput implements OnChanges {

    @ViewChild('inputControl') inputControl: ElementRef;

    /**
     * Текущий язык
     */
    @Language() lang: string;

    /**
     * Филд для ошибок
     */
    @Input() field?: string;

    /**
     * Объект с ошибками
     */
    @Input() errors?: PlErrorable;

    /**
     * Код из файла локализации для заголовка инпута
     */
    @Input() labelCode: string;

    /**
     * Дополнительные настройки
     */
    @Input() settings = new PlInputSettings();

    /**
     * Модель, где хранится строка, введённая в инпут
     */
    @Input() model: any;

    /**
     * Эмиттер, который синхронизирует данные в родительской модели
     * @type {EventEmitter<any>}
     */
    @Output() modelChange: EventEmitter<any> = new EventEmitter<any>();

    /**
     * Типы лейблов
     * @type {PlInputLabelType}
     */
    labelTypes = PlInputLabelType;

    constructor(public translationService: TranslationService) {

    }

    tooltipPositionTop() {
        return window.innerWidth <= 1024 ? 'left' : 'top';
    }

    getPlaceholderTranslate() {
        let placeholder = '';
        if (this.settings.placeholder !== '') {
            placeholder = this.translationService.translate(this.settings.placeholder);
        }
        return placeholder;
    }

    /**
     * Проверить на наличие ошибки
     * @returns {boolean}
     */
    hasError() {
        let response = null;
        if (this.errors && this.field) {
            response = this.errors.hasError(this.field);
        }

        return response;
    }

    /**
     * Получить код ошибки
     * @returns {string | any}
     */
    getError() {
        let response = null;
        if (this.errors && this.field) {
            let code = this.errors.getError(this.field);
            if (!code) {
                code = 'errors.field_required';
            } else {
                code = `errors.${code}`;
            }
            response = this.translationService.translate(code);
        }

        return response;
    }

    /**
     * Работает на change инпута, эмитит данные в родителя
     * @param newValue
     * @param type
     */
    changeValue(newValue: any, type?: String) {
        let _value;

        // Костыли
        if (type === 'DATEPICKER') {
            const hack = this.toISO(newValue);
            const date = new Date(hack);
            if (typeof this.model === 'object') {
                _value = date;
            } else if (typeof this.model === 'number') {
                _value = date.getTime();
            }

            if (!_value) {
                _value = '';
            }
        } else {
            _value = newValue;
        }
        // Костыли

        console.log(_value);
        this.modelChange.emit(_value);
        if (this.errors && this.field && this.settings.required) {
            if (this.inputControl.nativeElement.classList.contains('error') && _value !== '') {
                this.inputControl.nativeElement.classList.remove('error');
                const newErrors = this.errors.errors.filter(item => item.field !== this.field);
                this.errors.reset(newErrors);
            } else if (!this.inputControl.nativeElement.classList.contains('error') && _value === '') {
                this.inputControl.nativeElement.classList.add('error');
            }
        } else if (_value === '' && !this.settings.required) {
            if (this.inputControl.nativeElement.classList.contains('error')) {
                this.inputControl.nativeElement.classList.remove('error');
                this.errors.reset();
            }
        }
    }

    /**
     * Аглихак. Date умеет парсить дату только из формата ISO, поэтому приводим строку к такому формату
     * @param {string} row
     */
    toISO(row: string): string {
        return row.split('.').reverse().join('-');
    }

    ngOnChanges(changes: SimpleChanges): void {
        if (changes.settings) {
            this.settings = new PlInputSettings(changes.settings.currentValue);
        }
    }

    /**
     * Получить перевод тултипа для label help
     */
    getLabelHelpTranslate() {
        return this.translationService.translate(this.settings.labelHelp);
    }
}

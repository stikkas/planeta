import {PlInputType} from './enums/PlInputType';
import {PlInputLabelType} from './enums/PlInputLabelType';

export class PlInputSettings {

    /**
     * @type {string}
     */
    placeholder?: string = '';

    /**
     * Тип инпута
     * @type {PlInputType.TEXT}
     */
    type?: PlInputType = PlInputType.TEXT;

    /**
     * Маска для инпутов
     * @type {boolean}
     */
    mask?: Array<string | RegExp> | boolean = false;

    /**
     * Disabled для инпута
     * @type {boolean}
     */
    disabled?: boolean = false;

    /**
     * Обязательное/не обязательное поле
     * @type {boolean}
     */
    required?: boolean = true;

    /**
     * Только на чтение
     * @type {boolean}
     */
    readOnly?: boolean = false;

    /**
     * Тип label
     * @type {PlInputLabelType.INNER}
     */
    labelType?: PlInputLabelType = PlInputLabelType.INNER;

    /**
     * Код текста для приставки к инпуту слева. Если не null, то рисуется аддон
     * @type {null}
     */
    addonCode?: string = null;

    /**
     * Код перевода подсказки рядом с label
     * @type {null}
     */
    labelHelp: string = null;

    /**
     * Количество строк для textarea
     * @type {number}
     */
    rows: number = 1;

    constructor(options?: any) {
        if (options) {
            if (options.type) {
                this.type = options.type;
            }
            if (options.mask) {
                if (options.type && options.type === PlInputType.PASSWORD) {
                    this.mask = false;
                } else {
                    this.mask = options.mask;
                }
            }
            if (options.disabled !== undefined) {
                this.disabled = options.disabled;
            }
            if (options.placeholder) {
                this.placeholder = options.placeholder;
            }
            if (options.required !== undefined) {
                this.required = options.required;
            }
            if (options.readOnly !== undefined) {
                this.readOnly = options.readOnly;
            }
            if (options.labelType) {
                this.labelType = options.labelType;
            }
            if (options.addonCode) {
                if (!options.labelType || options.labelType === PlInputLabelType.OUTER) {
                    this.addonCode = options.addonCode;
                }
            }
            if (options.labelHelp) {
                if (!options.labelType || options.labelType === PlInputLabelType.OUTER) {
                    this.labelHelp = options.labelHelp;
                }
            }
            if (options.rows) {
                this.rows = options.rows;
            }
        }
    }
}

export enum PlInputLabelType {
    /**
     * Рисуется внутри инпута
     */
    INNER = 'INNER',

    /**
     * Рисуется снаружи инпута
     */
    OUTER = 'OUTER'
}

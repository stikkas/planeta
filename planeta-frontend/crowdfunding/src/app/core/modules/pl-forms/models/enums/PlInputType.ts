export enum PlInputType {

    PASSWORD = 'PASSWORD',
    TEXT = 'TEXT',
    NUMBER = 'NUMBER'
}

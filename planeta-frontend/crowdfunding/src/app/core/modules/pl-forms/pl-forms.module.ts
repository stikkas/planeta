import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import {LocalizationModule} from 'angular-l10n';
import {TextMaskModule} from 'angular2-text-mask';
import {ClickOutsideModule} from 'ng-click-outside';
import {BsDatepickerModule, TypeaheadModule, TooltipModule} from 'ngx-bootstrap';

import {StickyModule} from 'ng2-sticky-kit';
import {PlCoreModule} from '@planeta/planeta-core';
import {ElasticModule} from 'angular2-elastic';
import {PlSelectComponent} from './pl-select/pl-select.component';
import {PlEmailWithLoginComponent} from './pl-email-with-login/pl-email-with-login.component';
import {PlInputComponent} from './pl-input/pl-input.component';
import {PlTextareaComponent} from './pl-textarea/pl-textarea.component';
import {PlDatepickerComponent} from './pl-datepicker/pl-datepicker.component';
import {PlTypeaheadComponent} from './pl-typeahead/pl-typeahead.component';

// Дополнительно импортируем русскую локализацию для ngx-datepicker, en - включается по умолчанию
import { defineLocale } from 'ngx-bootstrap/chronos';
import { ruLocale } from 'ngx-bootstrap/locale';
// Назначаем русскую локализацию
defineLocale('ru', ruLocale);

@NgModule({
    imports: [
        BrowserModule,
        FormsModule,
        ClickOutsideModule,
        TextMaskModule,
        LocalizationModule,
        ReactiveFormsModule,
        TooltipModule.forRoot(),
        StickyModule,
        PlCoreModule,
        ElasticModule,
        BsDatepickerModule.forRoot(),
        TypeaheadModule.forRoot()
    ],
    exports: [
        PlInputComponent,
        PlTextareaComponent,
        PlSelectComponent,
        PlEmailWithLoginComponent,
        PlDatepickerComponent,
        PlTypeaheadComponent
    ],
    declarations: [
        PlInputComponent,
        PlTextareaComponent,
        PlSelectComponent,
        PlEmailWithLoginComponent,
        PlDatepickerComponent,
        PlTypeaheadComponent
    ],
    providers: []
})
export class PlFormsModule {}


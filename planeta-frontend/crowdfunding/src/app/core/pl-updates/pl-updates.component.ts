import {Component, OnInit} from '@angular/core';
import {ProfileService} from '../../services/profile.service';
import {DefaultLocale, Language} from 'angular-l10n';
import {Update} from '../../models/Update';
import {NewsType} from '../../models/enums/NewsType';
import {PlAuthService} from '@planeta/planeta-core';

@Component({
    selector: 'pl-updates',
    templateUrl: './pl-updates.component.html',
    styleUrls: ['./pl-updates.component.less']
})
export class PlUpdatesComponent {

    @Language() lang;
    @DefaultLocale() defaultLocale;

    /**
     * Список моих новостей
     * @type {any[]}
     */
    updates: Update[] = [];

    /**
     * Лимит на подгрузку новостей
     * @type {number}
     */
    limit = 10;

    /**
     * Смещение на подгрузку новостей
     * @type {number}
     */
    offset = 0;

    /**
     * Форматы отображения даты для локализатора
     * @type {any}
     */
    updateDateOptions = {day: 'numeric', month: 'long', year: 'numeric'};
    updateTimeOptions = {hour: '2-digit', minute: '2-digit'};

    /**
     * Типы новостей
     * @type {NewsType}
     */
    newsTypes = NewsType;

    /**
     * Идёт/не идёт загрузка новостей
     * @type {boolean}
     */
    loading = true;

    /**
     * Показаны все новости
     */
    last = false;

    /**
     * Первая загрузка или нет
     * @type {boolean}
     */
    firstLoad = true;

    constructor(private profileService: ProfileService, private authService: PlAuthService) {
        this.authService.getUserInfo().subscribe(([info]) => {
            if (info) {
                this.getMyUpdates();
            }
        });

    }

    getMyUpdates() {
        this.loading = true;
        this.profileService.getUpdatesForMe(this.offset, this.limit).subscribe(([result]) => this.prepareUpdates(result));
    }

    /**
     * Загрузить ещё новостей
     */
    loadMoreUpdates() {
        this.offset += this.limit;
        this.getMyUpdates();
    }

    /**
     * Группировка новостей по дням
     * @param result
     */
    private prepareUpdates(result) {
        if (result.length > 0) {
            const self = this;
            result.forEach(function (item) {
                const tempDate = new Date(item.timeAdded).setHours(0, 0, 0, 0);

                if (self.updates.length === 0) {
                    self.updates.push(new Update(tempDate, [item]));
                } else {
                    const existIndex = self.updates.findIndex(it => it.date === tempDate);

                    if (existIndex > -1) {
                        self.updates[existIndex].news.push(item);
                    } else {
                        self.updates.push(new Update(tempDate, [item]));
                    }
                }
            });
        } else {
            this.last = true;
        }

        this.loading = false;
        this.firstLoad = false;
    }
}

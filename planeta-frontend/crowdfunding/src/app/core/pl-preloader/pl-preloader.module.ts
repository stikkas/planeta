import {NgModule} from '@angular/core';
import {PlPreloaderComponent} from './pl-preloader.component';


@NgModule({
    imports: [

    ],
    exports: [
        PlPreloaderComponent
    ],
    declarations: [
        PlPreloaderComponent
    ],
    providers: []
})
export class PlPreloaderModule {
}


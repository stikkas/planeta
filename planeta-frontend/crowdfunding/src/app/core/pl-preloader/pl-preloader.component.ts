import { Component } from '@angular/core';

@Component({
  selector: 'pl-preloader',
  templateUrl: './pl-preloader.component.html',
  styleUrls: ['./pl-preloader.component.less']
})
export class PlPreloaderComponent {

  constructor() { }
}

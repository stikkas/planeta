import {Component, Output, EventEmitter, Input} from '@angular/core';

/**
 * Компонент стандартной кнопки "Показать ещё"
 */
@Component({
    selector: 'pl-load-more-button',
    templateUrl: './pl-load-more-button.component.html',
    styleUrls: ['./pl-load-more-button.component.less']
})
export class PlLoadMoreButtonComponent {

    /**
     * Эмиттер, даёт знать родителю о нажатии на кнопку
     * @type {EventEmitter}
     */
    @Output() load: EventEmitter<any> = new EventEmitter<any>();

    /**
     * Идёт/не идёт загрузка
     */
    @Input() loading: boolean;

    constructor() {

    }

    /**
     * Клик по кнопке
     */
    emit() {
        this.load.emit('click');
    }
}

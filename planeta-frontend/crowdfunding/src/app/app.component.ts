import {Component} from '@angular/core';
import {Title} from '@angular/platform-browser';
import {TranslationService} from 'angular-l10n';
import {NavigationEnd, Router} from '@angular/router';
import {GtmService} from './services/gtm.service';


@Component({
    selector: 'pl-root',
    templateUrl: './app.component.html'
})
export class AppComponent {
    constructor(private titleService: Title,
                private translation: TranslationService,
                private router: Router,
                private gtm: GtmService) {
        titleService.setTitle(translation.translate('titles.main-title'));

        this.trackPageViewOnRouteChange();
    }

    /**
     * Открыт или закрыт бутерброд в шапке
     * @type {boolean}
     */
    isSandwichOpen: boolean = false;

    /**
     * Эвент срабатывает на открытие/закрытие
     * @param {boolean} value
     */
    toggleSandwich(value: boolean) {
        this.isSandwichOpen = value;
    }

    /**
     * Трек показа страницы при переходе по роутерам
     */
    private trackPageViewOnRouteChange(): void {
        this.router.events.subscribe(event => {
            if (event instanceof NavigationEnd) {
                this.gtm.trackUniversalEvent('pageView');
            }
        });
    }
}

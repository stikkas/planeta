import {Routes} from '@angular/router';
import {CampaignsPageComponent} from './campaigns-page.component';
import {CampaignPageComponent} from './campaign/campaign-page.component';

export const campaignsRoutes: Routes = [{
    path: 'campaigns',
    component: CampaignsPageComponent,
    children: [
        {
            path: ':alias',
            component: CampaignPageComponent
            }, {
            path: ':alias/rewards',
            loadChildren: '../rewards/rewards-page.module#RewardsPageModule',
        }
    ]
}];


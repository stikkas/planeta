import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {CommonModule} from '@angular/common';

import {campaignsRoutes} from './routes';
import {CampaignsPageComponent} from './campaigns-page.component';
import {RewardsPageModule} from '../rewards/rewards-page.module';
import {CampaignPageComponent} from './campaign/campaign-page.component';


@NgModule({
    imports: [
        CommonModule,
        RewardsPageModule,
        RouterModule.forChild(campaignsRoutes),
    ],
    exports: [],
    declarations: [
        CampaignsPageComponent,
        CampaignPageComponent
    ],
    providers: []
})
export class CampaignsPageModule {
}


import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {environment} from '../../../../environments/environment';
import {Language, TranslationService} from 'angular-l10n';
import {PlImageUtilsService, PlDateUtilsService, PlStaticNodesService} from '@planeta/planeta-core';
import {OrderInfoRewardsForProfilePage} from '../../../models/OrderInfoRewardsForProfilePage';

@Component({
  selector: '[pl-my-purchases-reward-order-card]',
  templateUrl: './my-purchases-reward-order-card.component.html',
})
export class MyPurchasesRewardOrderCardComponent implements OnInit {
    /**
     * Данные по проекту
     */
    @Input() project: OrderInfoRewardsForProfilePage;

    @Language()
    lang;
    /**
     * Основной сайт (старый) куда перенаправлять не релизованные на ангуляре запросы
     */
    mainHost: string;

    /**
     * Класс для отрисовывания бэйджа
     */
    badgeClass: string;

    /**
     * Класс для отрисовывания статуса заказа
     */
    paymentClass: string;

    /**
     * Строка для бэйджа
     */
    badgeTitle: string;

    /**
     * Собрано в процентах
     */
    collectedPercent = 0;

    constructor(private images: PlImageUtilsService,
                private dateUtils: PlDateUtilsService,
                private translationService: TranslationService) {
        this.mainHost = environment.mainHost;
    }

    ngOnInit() {
        switch (this.project.campaignStatus) {
            case 'ACTIVE':
                this.badgeClass = 'status-badge__info';
                this.badgeTitle = 'core.badge.active';
                break;
            case 'FINISHED':
                if (this.project.targetStatus === 'SUCCESS') {
                    this.badgeClass = 'status-badge__success';
                    this.badgeTitle = 'core.badge.success';
                } else {
                    this.badgeClass = 'status-badge__finish';
                    this.badgeTitle = 'core.badge.failed';
                }
                break;
            case 'PAUSED':
                this.badgeClass = 'status-badge__pause';
                this.badgeTitle = 'core.badge.paused';
                break;
            case 'DELETED':
                this.badgeClass = 'status-badge__canceled';
                this.badgeTitle = 'core.badge.deleted';
                break;
        }

        switch (this.project.paymentStatus) {
            case 'PENDING':
                this.paymentClass = '';
                break;
            case 'COMPLETED':
                this.paymentClass = 'paid';
                break;
            case 'CANCELLED':
                this.paymentClass = 'canceled';
                break;
            case 'FAILED':
                this.paymentClass = 'canceled';
                break;
            case 'RESERVED':
                this.paymentClass = '';
                break;
            case 'OVERDUE':
                this.paymentClass = '';
                break;
        }

        if (this.project.targetAmount) {
            this.collectedPercent = parseInt(`${this.project.collectedAmount / this.project.targetAmount * 100}`);
        }

    }

    /**
     * Возвращает ссылку к картинке проекта
     * @param {string} url - из базы
     * @returns {string} - строка с полным урлом для обработки и получения картинки
     */
    getImageUrl(url: string) {
        return url ? this.images.getThumbnailUrl(url, PlImageUtilsService.PROJECT_ITEM) :
            '/images/project/photo-album-cover.jpg';
    }

    /**
     * Возвращает ссылку к картинке заказа
     * @param {string} url - из базы
     * @returns {string} - строка с полным урлом для обработки и получения картинки
     */
    getOrderImageUrl(url: string) {
        return url ? this.images.getThumbnailUrl(url, PlImageUtilsService.MEDIUM) : '';
    }

    /**
     * Возвращает сколько времени осталось до завершения проекта словами
     * @returns {string} - строка полное предложение со словами
     */
    getTimeLeft() {
        return this.dateUtils.getTimeLeftWithWords(this.project.timeFinish);
    }

    getOrderTimeAdded() {
        return this.dateUtils.formatDate(this.project.timeAdded);
    }

    getOrderDeliveryAddress() {
        let addressElements = [];
        if (this.project.deliveryAddress.country) {
            addressElements.push(this.project.deliveryAddress.country)
        }
        if (this.project.deliveryAddress.city) {
            addressElements.push(this.project.deliveryAddress.city)
        }
        if (this.project.deliveryAddress.address) {
            addressElements.push(this.project.deliveryAddress.address)
        }
        if (this.project.deliveryAddress.zipCode) {
            addressElements.push(this.project.deliveryAddress.zipCode)
        }

        return addressElements.join(', ');
    }
}

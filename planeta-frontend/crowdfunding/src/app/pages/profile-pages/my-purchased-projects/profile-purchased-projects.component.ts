import {Component, TemplateRef, ViewChild} from '@angular/core';
import {CampaignStatus} from "../../../models/enums/CampaignStatus";
import {CampaignTargetStatus} from "../../../models/enums/CampaignTargetStatus";
import {MyProjectCard} from "../../../models/MyProjectCard";
import {environment} from "../../../../environments/environment";
import {ActivatedRoute, Router} from "@angular/router";
import {ProjectService} from "../../../services/project.service";
import {ProfileService} from "../../../services/profile.service";
import {MyCampaignRequestParams} from "../../../models/MyCampaignRequestParams";

import {AbstractProfilePage} from '../abstract-profile-page';
import {PlAuthService} from '@planeta/planeta-core';

import {BsModalRef, BsModalService} from "ngx-bootstrap";

@Component({
    templateUrl: './profile-purchased-projects.component.html'
})
export class ProfilePurchasedProjectsComponent extends AbstractProfilePage {
    private DEFAULT_OFFSET: number = 10;

    navs = [{
        link: '/account/my-purchased-projects',
        params: {status: CampaignStatus.ACTIVE},
        clazz: 'active',
        title: 'core.campaign-status-multiple.ACTIVE',
        imageUrl: '/images/icons/campaign-status-active.svg',
        enabled: false
    }, {
        link: '/account/my-purchased-projects',
        params: {status: CampaignStatus.FINISHED, targetStatus: CampaignTargetStatus.SUCCESS},
        clazz: 'finish',
        title: 'core.campaign-status-multiple.SUCCESSFULLY_FINISHED',
        imageUrl: '/images/icons/campaign-target-status-success.svg',
        enabled: false
    }, {
        link: '/account/my-purchased-projects',
        params: {status: CampaignStatus.FINISHED, targetStatus: CampaignTargetStatus.FAIL},
        clazz: 'fail',
        title: 'core.campaign-status-multiple.UNSUCCESSFULLY_FINISHED',
        imageUrl: '/images/icons/campaign-target-status-fail.svg',
        enabled: false
    }];

    private titles = {
        ACTIVE: {
            bodyTitle: 'core.campaign-status-multiple.ACTIVE',
            bodyDescr: 'profile-page.card.descr-active-purchased'
        },
        SUCCESSFULLY_FINISHED: {
            bodyTitle: 'core.campaign-status-multiple.SUCCESSFULLY_FINISHED',
            bodyDescr: 'profile-page.card.descr-success-finish-purchased'
        },
        UNSUCCESSFULLY_FINISHED: {
            bodyTitle: 'core.campaign-status-multiple.UNSUCCESSFULLY_FINISHED',
            bodyDescr: 'profile-page.card.descr-unsuccess-finish-purchased'
        }
    };


    private campaigns = {
        /**
         * Список активных проектов
         */
        ACTIVE: <MyProjectCard[]>[],

        /**
         * Список успешно завершенных проектов
         */
        SUCCESSFULLY_FINISHED: <MyProjectCard[]>[],

        /**
         * Список неуспешно завершенных проектов
         */
        UNSUCCESSFULLY_FINISHED: <MyProjectCard[]>[],
    };

    /**
     * общее количество прокетов разных типов
     */
    private campaignsCount = {
        ACTIVE: 0,
        SUCCESSFULLY_FINISHED: 0,
        UNSUCCESSFULLY_FINISHED: 0
    };

    /**
     * Проекты для отображения
     */
    myProjects = <MyProjectCard[]>[];

    /**
     * Заголовок
     */
    title: { bodyTitle?: string, bodyDescr?: string } = {};

    /**
     * url основной планеты
     */
    mainHost = environment.mainHost;

    myParams: MyCampaignRequestParams = {
        status: CampaignStatus.ACTIVE,
        targetStatus: null,
        limit: 0,
        offset: this.DEFAULT_OFFSET
    };

    /**
     * Ссылка к модальному окну для связи с менеджером
     */
    supportDialogRef: BsModalRef;
    @ViewChild('supportDialog') sdWindow: TemplateRef<any>;

    constructor(private route: ActivatedRoute, private router: Router,
                private profileService: ProfileService, private projectService: ProjectService,
                private modalService: BsModalService,
                authService: PlAuthService) {
        super(authService, route);
        authService.getUserInfo().subscribe(([info]) => {
            if (info) {
                this.init();
            }
        });

    }

    /**
     * признак того, что инфа по проектам еще подгружается
     */
    projectsLoading = true;
    /**
     * признак того, что инфа по проектам еще подгружается по кнопке "Показать ещё"
     */
    moreProjectsLoading = false;
    /**
     * признак того, что нужно показать кнпоку "загрузить еще"
     */
    canShowMore = false;

    /**
     * Устанавливает заголовок списка проектов
     */
    private setTitle() {
        if (this.myParams.status == CampaignStatus.ACTIVE) {
            this.title = this.titles.ACTIVE;
        } else if (this.myParams.status == CampaignStatus.FINISHED) {
            if (this.myParams.targetStatus == CampaignTargetStatus.FAIL) {
                this.title = this.titles.UNSUCCESSFULLY_FINISHED;
            } else {
                this.title = this.titles.SUCCESSFULLY_FINISHED;
            }
        } else {
            this.title = {};
        }
    }

    private fetchCampaigns(more: boolean) {
        more ? this.moreProjectsLoading = true : this.projectsLoading = true;
        this.projectService.getMyPurchasedCampaigns(this.myParams).subscribe(([data]) => {
            data.forEach(item => {
                this.myProjects.push(item);
            });
            more ? this.moreProjectsLoading = false : this.projectsLoading = false;
            this.canShowMore = (this.myParams.status == CampaignStatus.ACTIVE && this.myProjects.length < this.campaignsCount.ACTIVE) ||
                    (this.myParams.status == CampaignStatus.FINISHED && this.myParams.targetStatus == CampaignTargetStatus.SUCCESS && this.myProjects.length < this.campaignsCount.SUCCESSFULLY_FINISHED) ||
                    (this.myParams.status == CampaignStatus.FINISHED && this.myParams.targetStatus == CampaignTargetStatus.FAIL && this.myProjects.length < this.campaignsCount.UNSUCCESSFULLY_FINISHED);
        });
    }

    init() {
        this.profileService.getMyPurchasedCampaignsStatusesCount().subscribe(([result]) => {
            result.forEach((item) => {
                switch (item.status) {
                    case CampaignStatus.ACTIVE: {
                        if (item.count > 0) {
                            this.navs[0].enabled = true;
                            this.campaignsCount.ACTIVE = item.count;
                        }
                        break;
                    }
                    case CampaignStatus.FINISHED: {
                        if (item.targetStatus === CampaignTargetStatus.SUCCESS && item.count > 0) {
                            this.navs[1].enabled = true;
                            this.campaignsCount.SUCCESSFULLY_FINISHED = item.count;
                        } else if (item.targetStatus === CampaignTargetStatus.FAIL && item.count > 0) {
                            this.navs[2].enabled = true;
                            this.campaignsCount.UNSUCCESSFULLY_FINISHED = item.count;
                        }
                        break;
                    }
                }
            });

            this.route.queryParams.subscribe(params => {
                if (params && params.status) {
                    //this.projectsLoading = true;
                    this.myParams = {
                        status: params.status,
                        targetStatus: params.targetStatus || CampaignTargetStatus.NONE,
                        offset: 0,
                        limit: this.DEFAULT_OFFSET
                    };
                    this.myProjects = [];
                    this.fetchCampaigns(false);
                } else {
                    const first = this.navs.find(it => it.enabled === true);
                    if (first && first['params']) {
                        this.router.navigate(['/account/my-purchased-projects'], {queryParams: first['params']});
                    } else {
                        this.myProjects = this.campaigns.ACTIVE;
                    }
                }
                this.setTitle();
            });
        });
    };

    getNavs() {
        return this.navs.filter(it => it.enabled == true);
    }

    showMore() {
        //this.moreProjectsLoading = true;
        this.myParams.offset = this.myParams.offset + this.myParams.limit;
        this.fetchCampaigns(true);
    }

    showSupportDialog(campaignId: number) {
        this.supportDialogRef = this.modalService.show(this.sdWindow);
    }
}


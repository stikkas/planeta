import {Routes} from '@angular/router';
import {ProfilePageComponent} from './profile-page.component';
import {ProfileDashboardComponent} from './dashboard/profile-dashboard.component';
import {ProfileMyProjectsComponent} from './my-projects/profile-my-projects.component';
import {ProfileBalanceMainComponent} from './balance/profile-balance-main.component';
import {ProfileSettingsComponent} from './settings/profile-settings.component';
import {ProfilePurchasedProjectsComponent} from './my-purchased-projects/profile-purchased-projects.component';
import {ProfilePurchasesAndRewardsComponent} from './my-purchases-and-rewards/profile-purchases-and-rewards.component';
import {AccountCanActivate} from '../../services/routes/account.can.activate';
import {MyPurchaseTypeActivate} from '../../services/routes/my-purchase-type.activate';
import {ProfileSettingsNotificationsComponent} from './settings/notifications/profile-settings-notifications.component';
import {ProfileSettingsMainComponent} from './settings/main/profile-settings-main.component';
import {ProfileSettingsContactsComponent} from './settings/contacts/profile-settings-contacts.component';
import {ProfileSettingsChangePasswordComponent} from './settings/change-password/profile-settings-change-password.component';
import {ProfileBalanceOperationsComponent} from './balance/balance-operations/profile-balance-operations.component';
import {PurchasedRewardsResolver} from '../../services/routes/purchased-rewards.resolver';
import {UserInfoResolver} from '../../services/routes/user.info.resolver';

export const profileRoutes: Routes = [{
    path: 'account',
    component: ProfilePageComponent,
    resolve: {userInfo: UserInfoResolver},
    children: [
        {
            path: '', component: ProfileDashboardComponent,
            canActivate: [AccountCanActivate]
        },
        {
            path: 'my-projects',
            component: ProfileMyProjectsComponent,
            canActivate: [AccountCanActivate]
        },
        {
            path: 'my-purchased-projects',
            component: ProfilePurchasedProjectsComponent,
            canActivate: [AccountCanActivate]
        },
        {
            path: 'my-purchases-and-rewards',
            canActivate: [MyPurchaseTypeActivate],
            component: ProfilePurchasesAndRewardsComponent
        },
        {
            path: 'my-rewards',
            component: ProfilePurchasesAndRewardsComponent
        },
        {
            path: 'my-charity-orders',
            component: ProfilePurchasesAndRewardsComponent
        },
        {
            path: 'my-bibliorodina',
            component: ProfilePurchasesAndRewardsComponent
        },
        {
            path: 'my-purchases',
            component: ProfilePurchasesAndRewardsComponent
        },
        {
            path: 'balance',
            component: ProfileBalanceMainComponent,
            children: [
                {path: '', component: ProfileBalanceOperationsComponent}
            ]
        },
        {
            path: 'settings',
            component: ProfileSettingsComponent,
            children: [
                {
                    path: '',
                    component: ProfileSettingsMainComponent,
                    canActivate: [AccountCanActivate]
                },
                {
                    path: 'notifications', component: ProfileSettingsNotificationsComponent,
                    canActivate: [AccountCanActivate]
                },
                {
                    path: 'contacts', component: ProfileSettingsContactsComponent,
                    canActivate: [AccountCanActivate]
                },
                {
                    path: 'change-password', component: ProfileSettingsChangePasswordComponent,
                    canActivate: [AccountCanActivate]
                }
            ]
        }
    ]
}];


import {Component, Input, ViewChild} from '@angular/core';
import {RouterLinkActive} from '@angular/router';
import {Language} from 'angular-l10n';

@Component({
    selector: '[pl-profile-nav-i]',
    templateUrl: './profile-nav-item.component.html',
    exportAs: 'profileNavIt'
})
export class ProfileNavItemComponent {
    @ViewChild('rla') rla: RouterLinkActive;
    @Input() model: any;
    @Language() lang;

    tooltipPositionLeft() {
        return window.innerWidth < 1400 ? 'right' : 'left';
    }
    tooltipIsDisabled() {
        return window.innerWidth < 768;
    }
}

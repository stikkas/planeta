import {PlAuthService} from '@planeta/planeta-core';
import {ActivatedRoute} from '@angular/router';

export abstract class AbstractProfilePage {

    constructor(authService: PlAuthService,
                activeRoute: ActivatedRoute) {

        authService.authChanged.subscribe(res => {
            window.location.href = window.location.href.replace(';authRequired=true', '')
        });

        activeRoute.params.subscribe(params => {
            if (params.authRequired) {
                authService.showLogin();
            }
        });
    }

}


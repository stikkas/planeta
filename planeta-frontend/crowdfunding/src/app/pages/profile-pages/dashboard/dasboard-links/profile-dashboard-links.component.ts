import {Component, Input} from '@angular/core';
import {Language} from 'angular-l10n';
import {ProfileDashboardLink} from '../../../../models/ProfileDashboardLink';
import {DashboardLinksType} from '../../../../models/enums/DashboardLinksType';

@Component({
    selector: 'pl-profile-dashboard-links',
    templateUrl: './profile-dashboard-links.component.html'
})

export class ProfileDashboardLinksComponent {

    /**
     * Текущий язык
     */
    @Language() lang: string;

    /**
     * Коллекция ссылок
     */
    @Input() links: ProfileDashboardLink[];

    /**
     * Тип блока:
     */
    @Input() type: DashboardLinksType;

    /**
     * Идёт загрузка данных
     */
    @Input() loading: boolean;

    types = DashboardLinksType;

    constructor() {

    }
}

import {Title} from '@angular/platform-browser';
import {Component} from '@angular/core';

import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';

import {Language} from 'angular-l10n';
import {ProfileService} from '../../../services/profile.service';
import {MyCampaignsStatusesCount} from '../../../models/MyCampaignsStatusesCount';
import {MyCampaignsStatusesCountDTO} from '../../../models/MyCampaignsStatusesCountDTO';
import {CampaignStatus} from '../../../models/enums/CampaignStatus';
import {CampaignTargetStatus} from '../../../models/enums/CampaignTargetStatus';
import {ProfileDashboardLink} from '../../../models/ProfileDashboardLink';
import {DashboardLinksType} from '../../../models/enums/DashboardLinksType';
import {MyPurchasesAndRewardsCount} from '../../../models/MyPurchasesAndRewardsCount';
import {OrderObjectType} from '../../../models/enums/OrderObjectType';
import {PlAuthService} from '@planeta/planeta-core';
import {AbstractProfilePage} from '../abstract-profile-page';
import {ActivatedRoute} from '@angular/router';

@Component({
    templateUrl: './profile-dashboard.component.html'
})

export class ProfileDashboardComponent extends AbstractProfilePage {

    /**
     * Текущий язык
     */
    @Language() lang: string;

    /**
     * Информация о количестве проектов в каждом статусе. ВАЖНО! В данном блоке некоторые статусы группируются:
     * АКТИВНЫЕ = ACTIVE + PAUSED; НА МОДЕРАЦИИ = NOT_STARTED + PATCH + APPROVED;
     * @type {ProfileDashboardLink[]}
     */
    myCampaignsStatusesLinks: ProfileDashboardLink[] = [];

    /**
     * Показывать/не показывать прелоадер блока "Мои проекты"
     * @type {boolean}
     */
    myCampaignsStatusesLoading = true;

    /**
     * Поддержанные проекты пользователя, сгруппированные по статусу и таргет статусу проектов
     * @type {MyCampaignsStatusesCount[]}
     */
    myPurchasedCampaignsStatusesLinks: ProfileDashboardLink[] = [];

    /**
     * Показывать/не показывать прелоадер блока "Поддержанные проекты"
     * @type {boolean}
     */
    myPurchasedCampaignsStatusesLoading = true;

    /**
     * Покупки и вознаграждения пользователя, сгруппированные по типу "товара"
     * @type {MyCampaignsStatusesCount[]}
     */
    myPurchasesAndRewardsLinks: ProfileDashboardLink[] = [];

    /**
     * Показывать/не показывать прелоадер блока "Покупки и вознаграждения"
     * @type {boolean}
     */
    myPurchasesAndRewardsLoading = true;

    linkTypes = DashboardLinksType;

    constructor(private titleService: Title,
                private profileService: ProfileService, private authService: PlAuthService,
                activeRoute: ActivatedRoute) {
        super(authService, activeRoute);
        // TODO какой заголовок?
        titleService.setTitle('Выбор способа доставки и оплаты | Проект "Название проекта тут"');
        this.authService.getUserInfo().subscribe(([info]) => {
            if (info) {
                this.getMyCampaignStatusesCount();
                this.getMyPurchasedCampaignStatusesCount();
                this.getMyPurchasesAndRewardsCount();
            }
        });
    }


    /**
     * Достает количество проектов пользователя с группировкой по статусам и дополнительно объединяет некоторые статусы.
     * Читать в описании к myCampaignsStatusesCount
     */
    getMyCampaignStatusesCount() {
        this.profileService.getMyCampaignsStatusesCount().subscribe(([result]) => {
            const counts: MyCampaignsStatusesCountDTO = new MyCampaignsStatusesCountDTO();

            result.forEach(function (item: MyCampaignsStatusesCount) {
                if (item.status === CampaignStatus.ACTIVE || item.status === CampaignStatus.PAUSED) {
                    counts.active += item.count;
                } else if (item.status === CampaignStatus.NOT_STARTED || item.status === CampaignStatus.PATCH ||
                        item.status === CampaignStatus.APPROVED) {
                    counts.onModeration += item.count;
                } else if (item.status === CampaignStatus.DRAFT) {
                    counts.draft += item.count;
                } else if (item.status === CampaignStatus.FINISHED) {
                    counts.finished += item.count;
                } else if (item.status === CampaignStatus.DECLINED) {
                    counts.declined += item.count;
                }
            });

            if (counts.active > 0) {
                this.myCampaignsStatusesLinks.push(new ProfileDashboardLink(
                        'core.campaign-status-multiple.ACTIVE',
                        '/images/icons/campaign-status-active.svg',
                        '/account/my-projects'
                ));
            }

            if (counts.draft > 0) {
                this.myCampaignsStatusesLinks.push(new ProfileDashboardLink(
                        'core.campaign-status-multiple.DRAFT',
                        '/images/icons/campaign-status-draft.svg',
                        '/account/my-projects',
                        {status: CampaignStatus.DRAFT}
                ));
            }

            if (counts.onModeration > 0) {
                this.myCampaignsStatusesLinks.push(new ProfileDashboardLink(
                        'core.campaign-status-multiple.NOT_STARTED',
                        '/images/icons/campaign-status-not-started.svg',
                        '/account/my-projects',
                        {status: CampaignStatus.NOT_STARTED}
                ));
            }

            if (counts.finished > 0) {
                this.myCampaignsStatusesLinks.push(new ProfileDashboardLink(
                        'core.campaign-status-multiple.FINISHED',
                        '/images/icons/campaign-status-finished.svg',
                        '/account/my-projects',
                        {status: CampaignStatus.FINISHED}
                ));
            }

            if (counts.declined > 0) {
                this.myCampaignsStatusesLinks.push(new ProfileDashboardLink(
                    'core.campaign-status-multiple.DECLINED',
                    '/images/icons/campaign-status-declined.svg',
                    '/account/my-projects',
                    {status: CampaignStatus.DECLINED}
                ));
            }

            this.myCampaignsStatusesLoading = false;
        });
    }

    /**
     * Достает количество поддержанных пользователем проектов с группировкой по статусам
     */
    getMyPurchasedCampaignStatusesCount() {
        const self = this;
        this.profileService.getMyPurchasedCampaignsStatusesCount().subscribe(([result]) => {
            result.forEach(function (item) {
                switch (item.status) {
                    case CampaignStatus.ACTIVE:
                        self.myPurchasedCampaignsStatusesLinks.push(new ProfileDashboardLink(
                                'core.campaign-status-multiple.ACTIVE',
                                '/images/icons/campaign-status-active.svg',
                                '/account/my-purchased-projects'
                        ));
                        break;
                    case CampaignStatus.FINISHED: {
                        if (item.targetStatus === CampaignTargetStatus.SUCCESS) {
                            self.myPurchasedCampaignsStatusesLinks.push(new ProfileDashboardLink(
                                    'core.campaign-target-status-multiple.SUCCESS',
                                    '/images/icons/campaign-target-status-success.svg',
                                    '/account/my-purchased-projects',
                                    {status: CampaignStatus.FINISHED, targetStatus: CampaignTargetStatus.SUCCESS}
                            ));
                        } else if (item.targetStatus === CampaignTargetStatus.FAIL) {
                            self.myPurchasedCampaignsStatusesLinks.push(new ProfileDashboardLink(
                                    'core.campaign-target-status-multiple.FAIL',
                                    '/images/icons/campaign-target-status-fail.svg',
                                    '/account/my-purchased-projects',
                                    {status: CampaignStatus.FINISHED, targetStatus: CampaignTargetStatus.FAIL}
                            ));
                        }
                    }

                }
            });

            this.myPurchasedCampaignsStatusesLoading = false;
        });
    }

    /**
     * Достает количество поддержанных проектов с группировкой по типу "товара"
     */
    getMyPurchasesAndRewardsCount() {
        const self = this;
        this.profileService.getMyPurchasesAndRewardsCount().subscribe(([result]) => {
            let rewards = 0;
            let biblio = 0;
            let charity = 0;
            let shop = 0;

            result.forEach(function (item: MyPurchasesAndRewardsCount) {
                switch (item.orderObjectType) {
                    case OrderObjectType.SHARE: {
                        if (item.campaignTagMnemonic === 'CHARITY') {
                            charity += item.count;
                        } else {
                            rewards += item.count;
                        }
                    }
                        break;
                    case OrderObjectType.BIBLIO:
                        biblio += item.count;
                        break;
                    case OrderObjectType.PRODUCT:
                        shop += item.count;
                        break;
                }
            });

            if (rewards > 0) {
                self.myPurchasesAndRewardsLinks.push(new ProfileDashboardLink(
                        'profile-page.rewards',
                        '/images/icons/planeta.svg',
                        '/account/my-purchases-and-rewards',
                        {orderObjectType: OrderObjectType.SHARE, charity: false}
                ));
            }

            if (biblio > 0) {
                self.myPurchasesAndRewardsLinks.push(new ProfileDashboardLink(
                        'core.planeta-projects.biblio',
                        '/images/icons/biblio.svg',
                        '/account/my-purchases-and-rewards',
                        {orderObjectType: OrderObjectType.BIBLIO}
                ));
            }

            if (charity > 0) {
                self.myPurchasesAndRewardsLinks.push(new ProfileDashboardLink(
                        'core.planeta-projects.charity',
                        '/images/icons/charity.svg',
                        '/account/my-purchases-and-rewards',
                        {orderObjectType: OrderObjectType.SHARE, charity: true}
                ));
            }

            if (shop > 0) {
                self.myPurchasesAndRewardsLinks.push(new ProfileDashboardLink(
                        'core.planeta-projects.shop',
                        '/images/icons/shop.svg',
                        '/account/my-purchases-and-rewards',
                        {orderObjectType: OrderObjectType.PRODUCT}
                ));
            }

            this.myPurchasesAndRewardsLoading = false;
        });
    }
}


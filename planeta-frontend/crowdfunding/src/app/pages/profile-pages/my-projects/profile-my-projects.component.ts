import {Component, TemplateRef, ViewChild} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {CampaignStatus} from '../../../models/enums/CampaignStatus';
import {BsModalRef, BsModalService} from 'ngx-bootstrap';
import {PlInputLabelType} from '../../../core/modules/pl-forms/models/enums/PlInputLabelType';
import {ProjectService} from '../../../services/project.service';
import {PlAuthService, PlErrorable, PlErrorsService} from '@planeta/planeta-core';
import {MyProjectCard} from '../../../models/MyProjectCard';
import {PlEditorConfig} from '../../../tinymce/EditorConfig';
import {ProfileService} from '../../../services/profile.service';
import {environment} from '../../../../environments/environment';
import {ToastrService} from 'ngx-toastr';
import {TranslationService} from 'angular-l10n';
import {AbstractProfilePage} from '../abstract-profile-page';
import {PlInputSettings} from '../../../core/modules/pl-forms/models/PlInputSettings';

@Component({
    templateUrl: './profile-my-projects.component.html'
})
export class ProfileMyProjectsComponent extends AbstractProfilePage {
    EditorConfig = PlEditorConfig;
    myProfileId: number;
    /**
     * Ссылка к модальному окну с подтверждением удаления черновика
     */
    deleteDraftRef: BsModalRef;
    @ViewChild('deleteDraft') ddWindow: TemplateRef<any>;

    /**
     * Ссылка к модальному окну для связи с менеджером
     */
    callManagerRef: BsModalRef;
    @ViewChild('callManager') cmWindow: TemplateRef<any>;

    /**
     * Ссылка к модальному окну для добавления новости
     */
    addNewsRef: BsModalRef;
    @ViewChild('addNews') anWindow: TemplateRef<any>;

    /**
     * Текст для вопроса менеджеру проекта
     */
    managerAsk: string;

    /**
     * Id черновика для удаления
     */
    draftRemovedId: number;

    /**
     * Id проекта для добавления новости
     */
    addNewsId: number;

    /**
     * Id проекта по поводу, которого хотят задать вопрос менеджеру
     */
    managerCampaignId: number;

    /**
     * Показывает, что данные еще грузятся
     */
    loading = true;

    /**
     * Дополнительные параметры для textarea вопроса менеджеру
     */
    managerAskSettings = <PlInputSettings>{
        labelType: PlInputLabelType.OUTER,
        placeholder: 'core.modal.manager-ask.placeholder',
        rows: 10
    };

    navs = [{
        link: '/account/my-projects',
        clazz: CampaignStatus.ACTIVE.toLowerCase(),
        title: 'core.campaign-status-multiple.ACTIVE',
        imageUrl: '/images/icons/campaign-status-active.svg',
        enabled: false
    }, {
        link: '/account/my-projects',
        params: {status: CampaignStatus.DRAFT},
        clazz: CampaignStatus.DRAFT.toLowerCase(),
        title: 'core.campaign-status-multiple.DRAFT',
        imageUrl: '/images/icons/campaign-status-draft.svg',
        enabled: false
    }, {
        link: '/account/my-projects',
        params: {status: CampaignStatus.NOT_STARTED},
        clazz: CampaignStatus.NOT_STARTED.toLowerCase(),
        title: 'core.campaign-status-multiple.NOT_STARTED',
        imageUrl: '/images/icons/campaign-status-not-started.svg',
        enabled: false
    }, {
        link: '/account/my-projects',
        params: {status: CampaignStatus.FINISHED},
        clazz: CampaignStatus.FINISHED.toLowerCase(),
        title: 'core.campaign-status-multiple.FINISHED',
        imageUrl: '/images/icons/campaign-status-finished.svg',
        enabled: false
    }, {
        link: '/account/my-projects',
        params: {status: CampaignStatus.DECLINED},
        clazz: CampaignStatus.DECLINED.toLowerCase(),
        title: 'core.campaign-status-multiple.DECLINED',
        imageUrl: '/images/icons/campaign-status-declined.svg',
        enabled: false
    }];

    private titles = {
        ACTIVE: {
            bodyTitle: 'core.campaign-status-multiple.ACTIVE',
            bodyDescr: 'profile-page.card.descr-active'
        },
        DRAFT: {
            bodyTitle: 'core.campaign-status-multiple.DRAFT',
            bodyDescr: 'profile-page.card.descr-draft'
        },
        NOT_STARTED: {
            bodyTitle: 'core.campaign-status-multiple.NOT_STARTED',
            bodyDescr: 'profile-page.card.descr-moderate'
        },
        FINISHED: {
            bodyTitle: 'core.campaign-status-multiple.FINISHED',
            bodyDescr: 'profile-page.card.descr-finish'
        },
        DECLINED: {
            bodyTitle: 'core.campaign-status-multiple.DECLINED',
            bodyDescr: 'profile-page.card.descr-cancel'
        }
    };

    private campaigns = {
        /**
         * Список активных проектов
         */
        ACTIVE: <MyProjectCard[]>[],

        /**
         * Список черновиков
         */
        DRAFT: <MyProjectCard[]>[],

        /**
         * Список проектов на модерации
         */
        NOT_STARTED: <MyProjectCard[]>[],

        /**
         * Список завершенных проектов
         */
        FINISHED: <MyProjectCard[]>[],

        /**
         * Список отклоненных проектов
         */
        DECLINED: <MyProjectCard[]>[]
    };

    /**
     * Проекты для отображения
     */
    myProjects = <MyProjectCard[]>[];

    /**
     * Заголовок
     */
    title: { bodyTitle?: string, bodyDescr?: string } = {};

    /**
     * Текст новости, которую нужно добавить в проект
     */
    addNewsBody: string;
    /**
     * Заголовок новости
     */
    addNewsTitle: string;

    /**
     * Время добавления последней новости
     */
    lastNewTimeadded: number;

    /**
     * Ошибки для формы добавления
     */
    addNewsErrors: PlErrorable;

    /**
     * url школы краудфандинга
     */
    schoolHost = environment.schoolHost;

    /**
     * url основной планеты
     */
    mainHost = environment.mainHost;

    /**
     * Настройки для инпута заголовка новости
     */
    addNewsTitleSettings = {
        placeholder: 'core.modal.type-news-title'
    };

    constructor(private route: ActivatedRoute, private router: Router,
                private modalService: BsModalService, private projectService: ProjectService,
                private profileService: ProfileService, private authService: PlAuthService,
                private errors: PlErrorsService,
                private toastr: ToastrService,
                private translationService: TranslationService) {
        super(authService, route);
        authService.getUserInfo().subscribe(([info]) => {
            if (info) {
                this.myProfileId = info.profile.profileId;
                this.init();
            }
        });

        this.addNewsErrors = errors.getErrorable();
    }

    init() {
        this.projectService.getMyCampaigns().subscribe(([info]) => {
            info.forEach((campaign: MyProjectCard) => {
                switch (campaign.status) {
                    case 'ACTIVE':
                    case 'PAUSED':
                        this.campaigns.ACTIVE.push(campaign);
                        break;
                    case 'NOT_STARTED':
                    case 'APPROVED':
                    case 'PATCH':
                        this.campaigns.NOT_STARTED.push(campaign);
                        break;
                    case 'DRAFT':
                        this.campaigns.DRAFT.push(campaign);
                        break;
                    case 'FINISHED':
                        this.campaigns.FINISHED.push(campaign);
                        break;
                    case 'DECLINED':
                        this.campaigns.DECLINED.push(campaign);
                        break;
                }
            });
            this.navs[0].enabled = !!this.campaigns.ACTIVE.length;
            this.navs[1].enabled = !!this.campaigns.DRAFT.length;
            this.navs[2].enabled = !!this.campaigns.NOT_STARTED.length;
            this.navs[3].enabled = !!this.campaigns.FINISHED.length;
            this.navs[4].enabled = !!this.campaigns.DECLINED.length;

            this.route.queryParams.subscribe(params => {
                if (params.status) {
                    this.myProjects = this.campaigns[params.status];
                    this.loading = false;
                    this.collectStat(this.myProjects);
                    this.setTitle(params.status);
                } else {
                    const first = this.navs.find(it => it.enabled === true);
                    if (first && first['params']) {
                        this.router.navigate(['/account/my-projects'], {queryParams: first['params']});
                    } else {
                        this.myProjects = this.campaigns.ACTIVE;
                        this.loading = false;
                        this.collectStat(this.myProjects);
                        this.setTitle(CampaignStatus.ACTIVE);
                    }
                }
            });
        });

    }

    /**
     * Получает статистику по проектам
     * @param {MyProjectCard[]} projects - список проектов передаем сюда т.к. результаты будут позже и текущие проекты уже могут измениться
     */
    collectStat(projects: MyProjectCard[]) {
        if (projects.length) {
            this.projectService.getMyCampaignsStats(projects.map(it => it.campaignId))
                    .subscribe(([res, errors, error]) => {
                        const campaignStats = <MyProjectCard[]>res;
                        if (campaignStats) {
                            projects.forEach(it => {
                                const stat = campaignStats.find(cmp => cmp.campaignId == it.campaignId);
                                if (stat) {
                                    it.visits = stat.visits;
                                    it.todayVisits = stat.todayVisits;
                                    it.reviews = stat.reviews;
                                    it.todayReviews = stat.todayReviews;
                                    it.todayPurchase = stat.todayPurchase;
                                    it.todayPurchaseCount = stat.todayPurchaseCount;
                                }
                            })
                        } else {
                            console.log(error);
                        }
                    });
        }
    }

    /**
     * Устанавливает заголовок списка проектов
     * @param {string} status - проекты с каким статусом отображаются
     */
    setTitle(status: string) {
        if (this.myProjects.length) {
            this.title = this.titles[status];
        } else {
            this.title = {};
        }
    }

    /**
     * Удаляет черновик из списка черновиков на клиенте
     */
    private projectRemoved(draftId: number) {
        const draftIdx = this.campaigns.DRAFT.findIndex(it => it.campaignId === draftId);
        if (draftIdx > -1) {
            this.campaigns.DRAFT.splice(draftIdx, 1);
        }
    }

    /**
     * Удаляет черновик с сервера
     */
    removeDraft() {
        if (this.draftRemovedId) {
            const draftId = this.draftRemovedId;
            this.projectService.removeDraft(draftId).subscribe(([res]) => {
                if (res) {
                    this.projectRemoved(draftId);
                } else {
                    // TODO что-то надо делать с ошибкой
                }
            })
        }
        this.deleteDraftRef.hide();
    }

    /**
     * Отправка запроса менеджеру
     */
    callToManager() {
        if (this.managerCampaignId) {
            const campaignId = this.managerCampaignId;
            this.profileService.callToManager(campaignId, this.managerAsk)
                    .subscribe(([result]) => {
                        // По идее тут не должно возникнуть ошибок, если будут возникать тогда будем обрабатывать
                        if (result) {
                            // this.askManagerErrors.reset();
                            this.managerAsk = '';
                            this.callManagerRef.hide();
                            this.toastr.success(
                                this.translationService.translate('core.modal.manager-ask.success-message'),
                                this.translationService.translate('core.modal.manager-ask.success-title')
                            );
                        } else {
                            this.toastr.error(
                                this.translationService.translate('errors.server-error.message'),
                                this.translationService.translate('errors.server-error.title')
                            );
                            // this.askManagerErrors.reset(errors);
                        }
                    });
        }
    }

    /**
     * Разрешает или запрещает отправку сообщения, взависимости от введенных данных
     * @returns {boolean | null}
     */
    managerAskDisabled(): boolean | null {
        return this.managerAsk && this.managerAsk.trim() ? null : true;
    }

    /**
     * Открывает диалог удаления черновика
     */
    openDraftDelDialog(campaignId: number) {
        this.draftRemovedId = campaignId;
        this.deleteDraftRef = this.modalService.show(this.ddWindow);
    }

    /**
     * Открывает диалог для вопроса менеджеру
     */
    openCallManagerDialog(campaignId: number) {
        this.managerCampaignId = campaignId;
        this.callManagerRef = this.modalService.show(this.cmWindow);
    }

    /**
     * Открывает диалог добавления новости
     */
    openAddNewsDialog(campaignId: number) {
        this.addNewsErrors.reset();
        this.addNewsId = campaignId;
        this.addNewsRef = this.modalService.show(this.anWindow);
    }

    /**
     * Отправляет запрос на публикацию новости
     */
    publishNews() {
        const now = new Date().getTime();
        if (this.lastNewTimeadded && (now - this.lastNewTimeadded) < 500) {
            // Чтобы быстрые щелчки не публиковали новость дважды
            return;
        }
        this.lastNewTimeadded = now;

        this.profileService.addNewsToCampaign({
            campaignId: this.addNewsId,
            profileId: this.myProfileId,
            postText: this.addNewsBody,
            title: this.addNewsTitle,
            timeAdded: now
        }).subscribe(([result, errors, error]) => {
            if (result) {
                this.addNewsRef.hide();
                this.addNewsTitle = '';
                this.addNewsBody = '';
            } else {
                this.addNewsErrors.reset(errors);
                this.toastr.error(
                        this.translationService.translate('errors.check-data'),
                );
                if (error) {
                    console.log(error);
                }
            }
        });
    }

    /**
     * Возвращает возможные типы проектов, которые есть у пользователя
     */
    getNavs() {
        return this.navs.filter(it => it.enabled == true);
    }
}


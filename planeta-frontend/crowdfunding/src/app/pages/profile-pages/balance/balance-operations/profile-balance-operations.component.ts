import {Component, Input, OnInit, TemplateRef, ViewChild} from '@angular/core';

import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import {ProfileService} from '../../../../services/profile.service';
import {BsModalRef, BsModalService} from 'ngx-bootstrap';
import {PlErrorable, PlErrorsService} from '@planeta/planeta-core';
import {PaymentService} from '../../../../services/payment.service';
import {PlInputSettings} from '../../../../core/modules/pl-forms/models/PlInputSettings';
import {PlInputType} from '../../../../core/modules/pl-forms/models/enums/PlInputType';
import {DefaultLocale, Language, TranslationService} from 'angular-l10n';
import {environment} from '../../../../../environments/environment';
import {RechargeBalanceDTO} from '../../../../models/dto/RechargeBalanceDTO';
import {TransactionType} from '../../../../models/enums/TransactionType';
import {BalanceOperation} from '../../../../models/BalanceOperation';
import {PaymentStatus} from '../../../../models/enums/PaymentStatus';

@Component({
    templateUrl: './profile-balance-operations.component.html'
})

export class ProfileBalanceOperationsComponent implements OnInit {

    @DefaultLocale() defaultLocale;

    limit = 20;
    offset = 0;

    /**
     * Текущий язык
     */
    @Language() lang: string;

    /**
     * Хост приложения
     * @type {string}
     */
    mainHost = environment.mainHost;

    /**
     * финансовые операции пользователя
     * @type {any[]}
     */
    balanceOperations: BalanceOperation[] = [];

    /**
     * Настройки инпута
     */
    totalAmountInputSettings = new PlInputSettings({
        type: PlInputType.NUMBER
    });

    /**
     * ДТО с данными для пополнения баланса
     * @type {RechargeBalanceDTO}
     */
    rechargeBalanceDTO = new RechargeBalanceDTO();

    /**
     * Форматы отображения даты для локализатора
     * @type {any}
     */
    updateDateOptions = { day: 'numeric', month: 'numeric', year: 'numeric'};

    /**
     * Объект с ошибками
     */
    @Input() errors: PlErrorable;

    /**
     * Ссылка к модальному окну для пополнения баланса
     */
    rechargeBalanceRef: BsModalRef;

    /**
     * Идет загрузка
     * @type {boolean}
     */
    loading = false;

    /**
     * Первая загрузка или нет
     * @type {boolean}
     */
    firstLoad = true;

    /**
     * Все данные загрузились
     * @type {boolean}
     */
    last = false;

    @ViewChild('rechargeBalance') anWindow: TemplateRef<any>;

    /**
     * Типы транзакций
     * @type {TransactionType}
     */
    transactionTypes = TransactionType;

    constructor(private profileService: ProfileService,
                private modalService: BsModalService,
                private paymentService: PaymentService,
                private errorService: PlErrorsService,
                private translationService: TranslationService) {
        this.errors = errorService.getErrorable();
    }

    ngOnInit() {
        this.getMyBalanceOperations();
        // this.getPaymentMethods();
    }

    /**
     * Получить баланс пользователя
     * @returns {number}
     */
    getBalance(): number {
        return this.profileService.getMyBalance();
    }

    /**
     * Достать с сервера операции по балансу пользователя
     */
    getMyBalanceOperations() {
        this.loading = true;

        this.profileService.getMyBalanceOperations(this.offset , this.limit).subscribe(([result]) => {
            if (result) {
                result.forEach(item => this.balanceOperations.push(item));
                if (result.length < this.limit) {
                    this.last = true;
                }
            }
            this.loading = false;
            this.firstLoad = false;
        });
    }

    /**
     * Открыть модалку способов оплаты
     */
    openPaymentMethodsModal() {
        this.rechargeBalanceRef = this.modalService.show(this.anWindow);
    }

    /**
     * Пополнить баланс
     */
    pay() {
        this.paymentService.rechargeBalance(this.rechargeBalanceDTO).subscribe(result => {
            window.location.href = result;
            console.log(result);
        });
    }

    /**
     * Трансформировать цену
     * @param {string} amount
     */
    transformAmount(amount: number) {
        return amount.toString().replace('-', '');
    }

    /**
     * Загрузить еще операций по балансу
     */
    loadMoreOperations() {
        this.offset += this.limit;
        this.getMyBalanceOperations();
    }

    /**
     * Получить комментарий к финансовой операции
     * @param {BalanceOperation} item
     */
    getItemComment(item: BalanceOperation) {
        let name = '';
        const regexp = /\[(.*)\]/;

        if (item.comment === 'PLANETA payment.') {
            name = this.translationService.translate('profile-page.balance.bank-transfer');
        } else if (item.comment.indexOf('Order cancellation') >= 0) {
            name = this.translationService.translate('profile-page.balance.order-cancellation') +
                    item.orderId;
        } else if (item.comment.indexOf('Order completion') >= 0) {
            name = this.translationService.translate('profile-page.balance.order-completion') +
                item.orderId;
        } else if (!item.orderId && !item.orderObjectType && !item.topayTransactionId) {
            name = this.translationService.translate('profile-page.balance.increase');
        }

        return name;
    }

    /**
     * Получить статус финансовой операции
     */
    getItemStatus(item: BalanceOperation) {
        let status = '';

        if (item.comment === 'PLANETA payment.') {
            status = this.translationService.translate('core.payment-statuses.COMPLETED');
        } else if (item.comment.indexOf('Order cancellation') >= 0) {
            status = this.translationService.translate('core.payment-statuses.' + item.paymentStatus);
        } else if (item.comment.indexOf('Order completion') >= 0) {
            status = this.translationService.translate('core.payment-statuses.' + item.paymentStatus);
        } else if (!item.orderId && !item.orderObjectType && !item.topayTransactionId) {
            status = this.translationService.translate('core.payment-statuses.COMPLETED');
        }

        return status;
    }

    /**
     * Получить класс финансовой операции
     */
    getItemClass(item: BalanceOperation) {
        let clazz = '';

        if (item.comment.indexOf('Order cancellation') >= 0 || item.paymentStatus === PaymentStatus.CANCELLED ||
            item.paymentStatus === PaymentStatus.FAILED) {
            clazz = 'project-fail';
        } else if (item.paymentStatus === PaymentStatus.OVERDUE || item.paymentStatus === PaymentStatus.RESERVED) {
            clazz = 'done';
            console.log(item);
        } else {
            clazz = 'paid';
        }

        return clazz;
    }
}

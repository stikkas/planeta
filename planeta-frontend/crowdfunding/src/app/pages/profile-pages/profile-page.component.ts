import {Component, OnInit} from '@angular/core';

import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';

import {Language, TranslationService} from 'angular-l10n';
import {PlAuthService} from '@planeta/planeta-core';
import {Profile} from '../../models/Profile';
import {ProfileService} from '../../services/profile.service';
import {ActivatedRoute} from '@angular/router';

@Component({
    selector: 'pl-profile-page',
    templateUrl: './profile-page.component.html'
})
export class ProfilePageComponent {

    /**
     * Текущий язык
     */
    @Language() lang: string;

    /**
     * Профиль пользователя
     */
    profile: Profile = new Profile();

    constructor(private translation: TranslationService,
                private authService: PlAuthService,
                private profileService: ProfileService,
                private activeRoute: ActivatedRoute) {

        this.authService.authChanged.subscribe(res => {
            window.location.href = window.location.href.replace(';authRequired=true', '')
        });

        this.activeRoute.params.subscribe(params => {
            if (params.authRequired) {
                this.authService.showLogin();
            }
        });

        this.activeRoute.data.subscribe(data => {
            if (data.userInfo[0]) {
                this.profile = data.userInfo[0].profile;
            }
        });

    }

    /**
     * Получить баланс пользователя
     * @returns {number}
     */
    getBalance(): number {
        return this.profileService.getMyBalance();
    }
}


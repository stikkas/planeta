import {Component} from '@angular/core';
import {ProfileService} from '../../../../services/profile.service';
import {ProfileSites} from '../../../../models/ProfileSites';
import {PlInputLabelType} from '../../../../core/modules/pl-forms/models/enums/PlInputLabelType';
import {PlAuthService, PlErrorable} from '@planeta/planeta-core';
import {Title} from '@angular/platform-browser';
import {TranslationService} from 'angular-l10n';
import {ToastrService} from 'ngx-toastr';
import {AbstractProfilePage} from '../../abstract-profile-page';
import {ActivatedRoute} from '@angular/router';

const FACEBOOK_URL = 'https://facebook.com/';
const VK_URL = 'https://vk.com/';
const SITE_URL = 'http://';
const TWITTER_URL = 'https://twitter.com/';
const GOOGLE_URL = 'https://plus.google.com/';

@Component({
    templateUrl: './profile-settings-contacts.component.html'
})
export class ProfileSettingsContactsComponent extends AbstractProfilePage {

    /**
     * Сайты пользователя
     * @type {ProfileSites}
     */
    sites: ProfileSites = new ProfileSites();

    /**
     * Объект для хранения ошибок
     */
    errors: PlErrorable;

    inputSettings = {
        labelType: PlInputLabelType.OUTER,
        addonCode: 'http://'
    };

    vkSettings = {
        labelType: PlInputLabelType.OUTER,
        addonCode: 'https://vk.com/'
    };

    fbSettings = {
        labelType: PlInputLabelType.OUTER,
        addonCode: 'https://facebook.com/'
    };

    twitterSettings = {
        labelType: PlInputLabelType.OUTER,
        addonCode: 'https://twitter.com/'
    };

    googleSettings = {
        labelType: PlInputLabelType.OUTER,
        addonCode: 'https://plus.google.com/'
    };

    constructor(private profileService: ProfileService,
                private titleService: Title,
                private translation: TranslationService,
                private toastr: ToastrService,
                authService: PlAuthService, activeRoute: ActivatedRoute) {
        super(authService, activeRoute);
        authService.getUserInfo().subscribe(([info]) => {
            if (info) {
                this.getMySites();
            }
        });
        titleService.setTitle(translation.translate('titles.profile-settings.contacts'));
    }

    /**
     * Достаем контакты профиля с сервера
     */
    getMySites() {
        this.profileService.getMySites().subscribe(([result]) => {
            if (result) {
                // Удаляем схемы
                result.siteUrl = result.siteUrl.replace(SITE_URL, '');
                result.vkUrl = result.vkUrl.replace(VK_URL, '');
                result.facebookUrl = result.facebookUrl.replace(FACEBOOK_URL, '');
                result.twitterUrl = result.twitterUrl.replace(TWITTER_URL, '');
                result.googleUrl = result.googleUrl.replace(GOOGLE_URL, '');

                this.sites = result;
            }
        });
    }

    save() {
        this.profileService.saveMySites(this.sites).subscribe(([result]) => {
            if (result) {
                this.toastr.success(
                        this.translation.translate('core.data-saved'),
                        this.translation.translate('core.data-saved-title')
                );
            }
        });
    }
}

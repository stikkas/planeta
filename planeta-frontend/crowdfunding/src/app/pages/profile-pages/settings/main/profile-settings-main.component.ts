import {Component} from '@angular/core';
import {TranslationService} from 'angular-l10n';
import {Title} from '@angular/platform-browser';
import {PlAuthService, PlErrorable, PlErrorsService} from '@planeta/planeta-core';
import {Profile} from '../../../../models/Profile';
import {ProfileDTO} from '../../../../models/dto/ProfileDTO';
import {PlInputLabelType} from '../../../../core/modules/pl-forms/models/enums/PlInputLabelType';
import {Country} from '../../../../interfaces/Country';
import {DictionaryService} from '../../../../services/dictionary.service';
import {Gender} from '../../../../models/enums/Gender';
import {SearchCitiesDTO} from '../../../../models/dto/SearchCitiesDTO';
import {ProfileService} from '../../../../services/profile.service';
import {PlEditorConfig} from '../../../../tinymce/EditorConfig';
import {ToastrService} from 'ngx-toastr';
import {PlInputSettings} from '../../../../core/modules/pl-forms/models/PlInputSettings';
import {AbstractProfilePage} from '../../abstract-profile-page';
import {ActivatedRoute} from '@angular/router';

@Component({
    templateUrl: './profile-settings-main.component.html'
})
export class ProfileSettingsMainComponent extends AbstractProfilePage {

    /**
     * Профиль пользователя
     */
    profile: Profile = new Profile();

    /**
     * Модель данных профиля
     * @type {ProfileDTO}
     */
    profileDTO = new ProfileDTO();

    /**
     * Конфиг для tinyMCE
     * @type {EditorConfig}
     */
    EditorConfig = PlEditorConfig;

    /**
     * Объект для хранения ошибок
     */
    errors: PlErrorable;

    phoneInputSettings = {
        labelType: PlInputLabelType.OUTER,
        required: false
    };


    birthdayInputSettings = {
        labelType: PlInputLabelType.OUTER,
        placeholder: 'core.date-mask',
        required: false
    };

    cityInputSettings = <PlInputSettings>{
        labelType: PlInputLabelType.OUTER,
        disabled: true,
        placeholder: 'core.begin-to-write'
    };

    nameInputSettings = {
        labelType: PlInputLabelType.OUTER,
        placeholder: 'profile-page.settings-block.fio'
    };

    siteUrlInputSettings = <PlInputSettings>{
        labelType: PlInputLabelType.OUTER,
        addonCode: 'https://planeta.ru/',
        required: false,
        labelHelp: 'profile-page.settings-block.alias-help',
        disabled: false
    };

    selectSettings = {
        labelType: PlInputLabelType.OUTER
    };

    /**
     * Список всех стран
     */
    countries: Country[];

    /**
     * DTO для поиска городов по подзапросу
     * @type {SearchCitiesDTO}
     */
    searchCitiesDTO = new SearchCitiesDTO();

    /**
     * Список для селекта
     */
    genders = [
        {
            code: Gender.MALE,
            valueEn: 'Male',
            valueRu: 'Мужской'
        },
        {
            code: Gender.FEMALE,
            valueEn: 'Female',
            valueRu: 'Женский'
        },
    ];

    constructor(private translationService: TranslationService,
                private authService: PlAuthService,
                private titleService: Title,
                private dictionaryService: DictionaryService,
                private profileServxice: ProfileService,
                private toastr: ToastrService,
                private errorService: PlErrorsService,
                activeRoute: ActivatedRoute) {
        super(authService, activeRoute);

        titleService.setTitle(translationService.translate('titles.profile-settings.main'));
        this.errors = errorService.getErrorable();
        this.getCountriesList();

        this.authService.getUserInfo().subscribe(([info]) => {
            if (info) {
                this.profile = info.profile;
                this.profileDTO.displayName = this.profile.displayName;
                this.profileDTO.alias = this.profile.alias;
                this.profileDTO.userGender = this.profile.userGender;
                this.profileDTO.phoneNumber = this.profile.phoneNumber;
                this.profileDTO.userBirthDate = new Date(this.profile.userBirthDate);
                this.profileDTO.countryId = this.profile.countryId;
                this.profileDTO.cityId = this.profile.cityId;
                this.profileDTO.summary = this.profile.summary;

                if (this.profile.alias) {
                    this.siteUrlInputSettings.disabled = true;
                }

                this.cityInputSettings.disabled = !this.isCountrySelected();

                // Передаем id страны пользователя в дто поиска предложений по городам
                this.searchCitiesDTO.countryId = this.profile.countryId;
            }
        });
    }

    /**
     * Достает список стран
     */
    getCountriesList() {
        this.dictionaryService.getCountries().subscribe(([result]) => {
            if (result) {
                this.countries = result;
            }
        });
    }

    /**
     * Выбрана страна из селекта или нет
     * @returns {boolean}
     */
    private isCountrySelected(): boolean {
        return this.profileDTO.countryId > 0;
    }

    /**
     * Срабатывает при выборе старны в селекте. Разблокирывает инпут с городами
     * @param countryId
     */
    countryChanged(countryId) {
        if (countryId > 0) {
            // Почему в дочернем компоненте не происходит обновления данных, если изменять атрибут, а не целый объект
            this.cityInputSettings = <PlInputSettings>{
                labelType: PlInputLabelType.OUTER,
                disabled: false,
                placeholder: 'core.begin-to-write'
            };

            console.log(this.cityInputSettings.disabled);
            this.searchCitiesDTO.countryId = countryId;
            this.profileDTO.cityId = 0;
        }
    }

    /**
     * Сохранить мои данные
     */
    save() {
        this.profileServxice.saveBaseInfo(this.profileDTO).subscribe(([result, errors]) => {
            if (result) {
                this.errors.reset();
                this.toastr.success(
                        this.translationService.translate('profile-page.settings-block.data-changed'),
                );
            } else {
                this.errors.reset(errors);
                this.toastr.error(
                        this.translationService.translate('errors.check-data'),
                );
            }
        });
    }
}

import {Component} from '@angular/core';
import {Title} from '@angular/platform-browser';
import {TranslationService} from 'angular-l10n';
import {Profile} from '../../../../models/Profile';
import {PlAuthService} from '@planeta/planeta-core';
import {ProfileService} from '../../../../services/profile.service';
import {SubscriptionsDTO} from '../../../../models/dto/SubscriptionsDTO';
import {ToastrService} from 'ngx-toastr';
import {AbstractProfilePage} from '../../abstract-profile-page';
import {ActivatedRoute} from '@angular/router';

@Component({
    templateUrl: './profile-settings-notifications.component.html'
})
export class ProfileSettingsNotificationsComponent extends AbstractProfilePage {

    /**
     * Информация о подписках
     */
    subscriptionsDTO = new SubscriptionsDTO();

    /**
     * Блокирует кнопку во время отправки запроса на сервер
     */
    loading = false;

    constructor(private titleService: Title,
                private translationService: TranslationService,
                private authService: PlAuthService,
                private profileService: ProfileService,
                private toastr: ToastrService,
                activeRoute: ActivatedRoute) {
        super(authService, activeRoute);
        titleService.setTitle(translationService.translate('titles.profile-settings.notifications'));

        this.authService.getUserInfo().subscribe(([info]) => {
            if (info) {
                this.subscriptionsDTO.receiveMyCampaignNewsletters = info.profile.receiveMyCampaignNewsletters;
                this.subscriptionsDTO.receiveNewsletters = info.profile.receiveNewsletters;
            }
        });
    }

    save() {
        this.loading = true;
        this.profileService.saveSubscriptions(this.subscriptionsDTO).subscribe(([result]) => {
            if (result) {
                this.toastr.success(
                        this.translationService.translate('profile-page.settings-block.data-changed')
                );
            }
            this.loading = false;
        });
    }
}

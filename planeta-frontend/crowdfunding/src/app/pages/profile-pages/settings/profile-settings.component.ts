import {Component} from '@angular/core';

@Component({
    templateUrl: './profile-settings.component.html'
})

export class ProfileSettingsComponent {

    /**
     * Вкладки левого меню
     * @type {{}[]}
     */
    navs: any[] = [
        {
            link: '/account/settings',
            imageUrl: '/images/icons/settings.svg',
            title: 'profile-page.settings'
        },
        {
            link: '/account/settings/contacts',
            imageUrl: '/images/icons/settings-share.svg',
            title: 'profile-page.settings-block.contacts'
        },
        {
            link: '/account/settings/notifications',
            imageUrl: '/images/icons/settings-mail.svg',
            title: 'profile-page.settings-block.notify-set'
        },
        {
            link: '/account/settings/change-password',
            imageUrl: '/images/icons/settings-lock.svg',
            title: 'profile-page.settings-block.change-pass'
        }
    ];
}


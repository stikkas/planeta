import {Component, OnInit} from '@angular/core';
import {PlAuthService, PlErrorable, PlErrorsService} from '@planeta/planeta-core';
import {PlInputType} from '../../../../core/modules/pl-forms/models/enums/PlInputType';
import {Language, TranslationService} from 'angular-l10n';
import {ProfileService} from '../../../../services/profile.service';
import {PlInputLabelType} from '../../../../core/modules/pl-forms/models/enums/PlInputLabelType';
import {ToastrService} from 'ngx-toastr';
import {Title} from '@angular/platform-browser';
import {AbstractProfilePage} from '../../abstract-profile-page';
import {ActivatedRoute} from '@angular/router';

@Component({
    templateUrl: './profile-settings-change-password.component.html'
})
export class ProfileSettingsChangePasswordComponent extends AbstractProfilePage {

    /**
     * Текущий язык
     */
    @Language() lang;

    /**
     * Объект для хранения ошибок
     */
    errors: PlErrorable;

    /**
     * Настройки для инпутов
     */
    inputSettings = {
        type: PlInputType.PASSWORD,
        labelType: PlInputLabelType.OUTER
    };

    /**
     * Модель данных
     */
    data = {
        oldPassword: '',
        newPassword: '',
        rePassword: ''
    };

    constructor(private errorService: PlErrorsService,
                private profileService: ProfileService,
                private toastr: ToastrService,
                private titleService: Title,
                private translationService: TranslationService,
                authService: PlAuthService, activeRoute: ActivatedRoute) {
        super(authService, activeRoute);
        this.errors = errorService.getErrorable();
        titleService.setTitle(translationService.translate('titles.profile-settings.change-pass'));
    }

    /**
     * Сохраняет новый пароль на сервер
     */
    updatePassword() {
        this.profileService.updatePassword(this.data).subscribe(([result, errors]) => {
            if (result) {
                this.errors.reset();
                this.toastr.success(
                        this.translationService.translate('profile-page.settings-block.password-changed'),
                );
            } else {
                this.errors.reset(errors);
                this.toastr.error(
                        this.translationService.translate('errors.check-data'),
                );
            }
        });
    }
}

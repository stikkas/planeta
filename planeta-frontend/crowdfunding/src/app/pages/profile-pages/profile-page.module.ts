import {NgModule} from '@angular/core';
import {BrowserModule, Title} from '@angular/platform-browser';
import {RouterModule} from '@angular/router';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';

import {LocalizationModule} from 'angular-l10n';
import {TooltipModule} from 'ngx-bootstrap';
import {PlCoreModule} from '@planeta/planeta-core';

import {profileRoutes} from './routes';
import {ProfilePageComponent} from './profile-page.component';
import {ProfileDashboardComponent} from './dashboard/profile-dashboard.component';
import {ProfileMyProjectsComponent} from './my-projects/profile-my-projects.component';
import {ProfileSettingsComponent} from './settings/profile-settings.component';
import {ProfileBalanceMainComponent} from './balance/profile-balance-main.component';
import {ProfileTabComponent} from './profile-tab/profile-tab.component';
import {ProfilePurchasesAndRewardsComponent} from './my-purchases-and-rewards/profile-purchases-and-rewards.component';
import {ProfilePurchasedProjectsComponent} from './my-purchased-projects/profile-purchased-projects.component';
import {ProfileNavItemComponent} from './profile-nav-item/profile-nav-item.component';
import {PlUpdatesComponent} from '../../core/pl-updates/pl-updates.component';
import {MyProjectCardComponent} from './my-project-card/my-project-card.component';
import {PHtmlTransform} from '../../pipes/p-html-transform.pipe';
import {ProfileDashboardLinksComponent} from './dashboard/dasboard-links/profile-dashboard-links.component';
import {PlPreloaderModule} from '../../core/pl-preloader/pl-preloader.module';
import {ProfileSettingsNotificationsComponent} from './settings/notifications/profile-settings-notifications.component';
import {PlLoadMoreButtonComponent} from '../../core/pl-load-more-button/pl-load-more-button.component';
import {ProfileSettingsMainComponent} from './settings/main/profile-settings-main.component';
import {ProfileSettingsContactsComponent} from './settings/contacts/profile-settings-contacts.component';
import {ProfileSettingsChangePasswordComponent} from './settings/change-password/profile-settings-change-password.component';
import {StickyModule} from 'ng2-sticky-kit';
import {PlFormsModule} from '../../core/modules/pl-forms/pl-forms.module';
import {PlEditorModule} from '../../tinymce/editor.module';
import {ProfileBalanceOperationsComponent} from './balance/balance-operations/profile-balance-operations.component';
import {PaymentMethodsModule} from '../../core/modules/payment-methods/payment-methods.module';
import {PurchasedRewardsResolver} from '../../services/routes/purchased-rewards.resolver';
import {MyPurchasesRewardOrderCardComponent} from './my-purchases-reward-order-card/my-purchases-reward-order-card.component';
import {UserInfoResolver} from '../../services/routes/user.info.resolver';
import {PlSupportPopupModule} from "@planeta/planeta-support-popup";
import {ClickOutsideModule} from 'ng-click-outside';

@NgModule({
    imports: [
        BrowserModule,
        CommonModule,
        RouterModule.forChild(profileRoutes),
        TooltipModule,
        FormsModule,
        PlFormsModule,
        ClickOutsideModule,
        PaymentMethodsModule,
        HttpClientModule,
        ReactiveFormsModule,
        PlCoreModule,
        LocalizationModule,
        StickyModule,
        PlEditorModule,
        PlPreloaderModule,
        PlSupportPopupModule
    ],
    exports: [],
    declarations: [
        ProfilePageComponent,
        ProfileDashboardComponent,
        ProfileDashboardLinksComponent,
        ProfileMyProjectsComponent,
        ProfileTabComponent,
        ProfileSettingsComponent,
        ProfileBalanceMainComponent,
        ProfileBalanceOperationsComponent,
        ProfilePurchasedProjectsComponent,
        ProfilePurchasesAndRewardsComponent,
        ProfileNavItemComponent,
        PlUpdatesComponent,
        MyProjectCardComponent,
        PHtmlTransform,
        ProfileSettingsMainComponent,
        ProfileSettingsContactsComponent,
        ProfileSettingsNotificationsComponent,
        ProfileSettingsChangePasswordComponent,
        PlLoadMoreButtonComponent,
        MyPurchasesRewardOrderCardComponent
    ],
    providers: [
        Title,
        PurchasedRewardsResolver,
        UserInfoResolver
    ]
})
export class ProfilePageModule {
}


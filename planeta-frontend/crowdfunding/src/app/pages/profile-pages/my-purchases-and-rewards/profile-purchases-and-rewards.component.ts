import {Component} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {BsModalService} from 'ngx-bootstrap';
import {ProjectService} from '../../../services/project.service';
import {ErrorableResult, PlAuthService, PlErrorsService} from '@planeta/planeta-core';
import {ProfileService} from '../../../services/profile.service';
import {ToastrService} from 'ngx-toastr';
import {TranslationService} from 'angular-l10n';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import {OrderInfoRewardsForProfilePage} from '../../../models/OrderInfoRewardsForProfilePage';
import {AbstractProfilePage} from '../abstract-profile-page';
import {RewardsService} from '../../../services/rewards.service';
import {OrderObjectType} from '../../../models/enums/OrderObjectType';
import {MyPurchasesAndRewardsCount} from '../../../models/MyPurchasesAndRewardsCount';

@Component({
    selector: 'pl-profile-purchases-and-rewards',
    templateUrl: './profile-purchases-and-rewards.component.html'
})

export class ProfilePurchasesAndRewardsComponent extends AbstractProfilePage {
    myProfileId: number;

    isShowSortDropdown = false;
    sortByDate = true;
    sortByTotalPrice = false;
    sortByItemsCount = false;
    sortByStatus = false;

    navs = [{
        link: '/account/my-rewards',
        clazz: 'share',
        title: 'profile-purchases-and-rewards.categories.shares.name',
        imageUrl: '/images/icons/planeta.svg',
        enabled: false
    }, {
        link: '/account/my-charity-orders',
        clazz: 'charity',
        title: 'profile-purchases-and-rewards.categories.charity.name',
        imageUrl: '/images/icons/charity.svg',
        enabled: false
    }, {
        link: '/account/my-bibliorodina',
        clazz: 'biblio',
        title: 'profile-purchases-and-rewards.categories.biblio.name',
        imageUrl: '/images/icons/biblio.svg',
        enabled: false
    }, {
        link: '/account/my-purchases',
        clazz: 'shop',
        title: 'profile-purchases-and-rewards.categories.shop.name',
        imageUrl: '/images/icons/shop.svg',
        enabled: false
    }];

    private titles = {
        SHARES: {
            bodyTitle: 'profile-purchases-and-rewards.categories.shares.name',
            bodyDescr: 'profile-purchases-and-rewards.categories.shares.description'
        },
        CHARITY: {
            bodyTitle: 'profile-purchases-and-rewards.categories.charity.name',
            bodyDescr: 'profile-purchases-and-rewards.categories.charity.description'
        },
        BIBLIO: {
            bodyTitle: 'profile-purchases-and-rewards.categories.biblio.name',
            bodyDescr: 'profile-purchases-and-rewards.categories.biblio.description'
        },
        SHOP: {
            bodyTitle: 'profile-purchases-and-rewards.categories.shop.name',
            bodyDescr: 'profile-purchases-and-rewards.categories.shop.description'
        }
    };

    /**
     * Заголовок
     */
    title: { bodyTitle?: string, bodyDescr?: string } = {};

    /**
     * Устанавливает заголовок списка заказов
     * @param {string} type - тип отображаемых заказов
     */
    setTitle(type: string) {
        if (this.orders.length) {
            this.title = this.titles[type];
        } else {
            this.title = {};
        }
    }

    orders = <OrderInfoRewardsForProfilePage[]>[];

    constructor(private route: ActivatedRoute, private router: Router,
                private modalService: BsModalService, private projectService: ProjectService,
                private profileService: ProfileService, private rewardsService: RewardsService,
                private errors: PlErrorsService,
                private toastr: ToastrService,
                private translationService: TranslationService,
                authService: PlAuthService) {
        super(authService, route);
        authService.getUserInfo().subscribe(([info]) => {
            if (info) {
                this.init();
            }
        });
    }

    init() {
        this.rewardsService.getMyPurchasedRewards().subscribe(([result]) => {
            const orders = this.orders;
            result.forEach((order: OrderInfoRewardsForProfilePage) => {
                orders.push(order);
            });

            this.route.queryParams.subscribe(params => {
                this.setTitle('SHARES');
            });
        });

        this.profileService.getMyPurchasesAndRewardsCount().subscribe(([result]) => {
            let rewards = 0;
            let biblio = 0;
            let charity = 0;
            let shop = 0;

            result.forEach(function (item: MyPurchasesAndRewardsCount) {
                switch (item.orderObjectType) {
                    case OrderObjectType.SHARE: {
                        if (item.campaignTagMnemonic === 'CHARITY') {
                            charity += item.count;
                        } else {
                            rewards += item.count;
                        }
                    } break;
                    case OrderObjectType.BIBLIO: biblio += item.count; break;
                    case OrderObjectType.PRODUCT: shop += item.count; break;
                }
            });

            this.navs[0].enabled = rewards > 0;
            this.navs[1].enabled = biblio > 0;
            this.navs[2].enabled = charity > 0;
            this.navs[3].enabled = shop > 0;

            console.log(this.navs[0].enabled);
            console.log(this.navs[1].enabled);
            console.log(this.navs[2].enabled);
            console.log(this.navs[3].enabled);
        });
    };

    /**
     * Открыть/закрыть список типов сортировки заказов
     */
    toggleShowSortDropdown() {
        this.isShowSortDropdown = !this.isShowSortDropdown;
        console.log('toggleShowSortDropdown');
    }

    /**
     * Закрыть список сортировки заказов
     */
    closeSortDropdown() {
        this.isShowSortDropdown = false;
        console.log('closeSortDropdown');
    }

    /**
     * Установить тип сортировки
     * @param {number} sortType - тип сортировки
     */
    changeSortBy(sortType: number) {
        this.clearSort();
        switch (sortType) {
            case 1:
                this.sortByDate = true;
                break;
            case 2:
                this.sortByTotalPrice = true;
                break;
            case 3:
                this.sortByItemsCount = true;
                break;
            case 4:
                this.sortByStatus = true;
                break;
            default:
                this.sortByDate = true;
        }
    }

    /**
     * Сбросить тип сортировки и закрыть список сортировки
     */
    clearSort() {
        this.isShowSortDropdown = false;
        this.sortByDate = false;
        this.sortByTotalPrice = false;
        this.sortByItemsCount = false;
        this.sortByStatus = false;
    }

    /**
     * Возвращает возможные типы заказов, которые есть у пользователя
     */
    getNavs() {
        return this.navs.filter(it => it.enabled == true);
    }
}


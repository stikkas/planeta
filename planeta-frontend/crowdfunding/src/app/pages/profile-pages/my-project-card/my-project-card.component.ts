import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {PlImageUtilsService, PlStaticNodesService} from '@planeta/planeta-core';
import {environment} from '../../../../environments/environment';
import {MyProjectCard} from '../../../models/MyProjectCard';
import {Language} from 'angular-l10n';

@Component({
    selector: '[pl-my-project-card]',
    templateUrl: './my-project-card.component.html'
})
export class MyProjectCardComponent implements OnInit {
    /**
     * Данные по проекту
     */
    @Input() project: MyProjectCard;

    @Input() my: boolean;

    @Language()
    lang;
    /**
     * Основной сайт (старый) куда перенаправлять не релизованные на ангуляре запросы
     */
    mainHost: string;

    /**
     * Класс для отрисовывания бэйджа
     */
    badgeClass: string;

    /**
     * Строка для бэйджа
     */
    badgeTitle: string;

    /**
     * Собрано в процентах
     */
    collectedPercent = 0;

    /**
     *  Осталось до 50% в рублях
     */
    toHalfRemained: number;

    /**
     * Флаг переключения меню "Управление проектом" на мобиле
     */
    manproject = false;

    /**
     * Эмиттер для удаления черновика
     */
    @Output("delDraft")
    delDraftEmitter = new EventEmitter<number>();

    /**
     * Эмиттер для связи с менеджером
     */
    @Output("callManager")
    callManagerEmitter = new EventEmitter<number>();

    /**
     * Эмиттер для добавления новости в проект
     */
    @Output("addNews")
    addNewsEmitter = new EventEmitter<number>();

    /**
     * Эмиттер для обращения в саппорт
     */
    @Output("askSupport")
    askSupportEmitter = new EventEmitter<number>();

    constructor(private images: PlImageUtilsService, private staticNodesService: PlStaticNodesService) {
        this.mainHost = environment.mainHost;
    }

    ngOnInit() {
        switch (this.project.status) {
            case 'ACTIVE':
                this.badgeClass = 'status-badge__info';
                this.badgeTitle = 'core.badge.active';
                break;
            case 'NOT_STARTED':
            case 'PATCH':
                this.badgeClass = 'status-badge__moderate';
                this.badgeTitle = 'core.badge.moderate';
                break;
            case 'APPROVED':
                this.badgeClass = 'status-badge__approved';
                this.badgeTitle = 'core.badge.approved';
                break;
            case 'DECLINED':
                this.badgeClass = 'status-badge__canceled';
                this.badgeTitle = 'core.badge.declined';
                break;
            case 'DRAFT':
                this.badgeClass = 'status-badge__finish';
                this.badgeTitle = 'core.badge.draft';
                break;
            case 'FINISHED':
                if (this.project.targetStatus === 'SUCCESS') {
                    this.badgeClass = 'status-badge__success';
                    this.badgeTitle = 'core.badge.success';
                } else {
                    this.badgeClass = 'status-badge__finish';
                    this.badgeTitle = 'core.badge.failed';
                }
                break;
            case 'PAUSED':
                this.badgeClass = 'status-badge__pause';
                this.badgeTitle = 'core.badge.paused';
                break;
        }

        if (this.project.targetAmount) {
            this.collectedPercent = parseInt(`${this.project.collectedAmount / this.project.targetAmount * 100}`);
        }
        this.toHalfRemained = parseInt(`${this.project.targetAmount / 2 - this.project.collectedAmount}`);
    }

    /**
     * Возвращает ссылку к картинке проекта
     * @param {string} url - из базы
     * @returns {string} - строка с полным урлом для обработки и получения картинки
     */
    getImageUrl(url: string) {
        return url ? this.images.getThumbnailUrl(url, PlImageUtilsService.PROJECT_ITEM) :
            '/images/project/photo-album-cover.jpg';
    }

    /**
     * Запускает событие удаления черновика
     */
    removeDraft() {
        this.delDraftEmitter.emit(this.project.campaignId);
    }

    /**
     * Запускает событие для связи с менеджером
     */
    callManager() {
        this.callManagerEmitter.emit(this.project.campaignId);
    }

    /**
     * Запускает событие для добавления новости
     */
    addNews() {
        this.addNewsEmitter.emit(this.project.campaignId);
    }

    askSupport() {
        this.askSupportEmitter.emit(this.project.campaignId);
    }
}



import {Component} from '@angular/core';
import {Title} from '@angular/platform-browser';

@Component({
    templateUrl: './page-not-found.component.html',
    styleUrls: ['./page-not-found.component.less']
})
export class PageNotFoundComponent {

    constructor(serviceTitle: Title) {
        serviceTitle.setTitle('Ошибка | Planeta.ru');
    }
}



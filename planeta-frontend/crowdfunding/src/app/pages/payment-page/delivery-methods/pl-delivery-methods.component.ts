import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {LinkedDelivery} from '../../../interfaces/LinkedDelivery';
import {DeliveryType} from '../../../models/enums/DeliveryType';
import {DictionaryService} from '../../../services/dictionary.service';
import {Country} from '../../../interfaces/Country';
import {Language, TranslationService} from 'angular-l10n';
import {PlErrorable} from '@planeta/planeta-core';
import {DeliveryInfo} from '../../../models/DeliveryInfo';
import {PaymentMethod} from '../../../models/PaymentMethod';

@Component({
    selector: 'pl-delivery-methods',
    templateUrl: './pl-delivery-methods.component.html',
    styleUrls: ['./pl-delivery-methods.component.less']
})
export class DeliveryMethodsComponent implements OnInit {
    /**
     * Текущий язык
     */
    @Language() lang: string;

    /**
     * Список способов доставки
     */
    @Input() methods: LinkedDelivery[];

    /**
     * Модель, в которой будет храниться данные для оплаты
     */
    @Input() model: DeliveryInfo;

    /**
     * Объект с ошибками
     */
    @Input() errors: PlErrorable;

    /**
     * Эмиттер, который синхронизирует данные в родительской модели
     * @type {EventEmitter<any>}
     */
    @Output() modelChange: EventEmitter<any> = new EventEmitter<any>();

    /**
     * Список всех стран
     */
    countries: Country[];

    /**
     * Адрес самовывоза, никуда не передается. Только для информации
     */
    pickUpAddress = '';

    pickupInputSettings = {
        readOnly: true
    };

    constructor(private dictionaryService: DictionaryService,
                public translationService: TranslationService) {

    }

    ngOnInit() {
        this.getCountriesList();
        const method = this.methods.filter((mth: LinkedDelivery) => mth.serviceType === DeliveryType.CUSTOMER_PICKUP)[0];
        if (method) {
            this.setPickUpAddress(method);
        }
    }

    /**
     * Формирует адрес самовывоза
     * @param {LinkedDelivery} method
     */
    setPickUpAddress(method: LinkedDelivery) {
        const address = method.address;
        if (address.zipCode) {
            this.pickUpAddress += address.zipCode + ', ';
        }
        this.pickUpAddress += address.city + ', ' + address.street;
        if (address.phone) {
            this.pickUpAddress += ', ' + address.phone;
        }
    }

    /**
     * Меняет способ доставки
     * @param {LinkedDelivery} method
     */
    changeMethod(method: LinkedDelivery) {
        this.model.deliveryType = method.serviceType;
        this.model.serviceId = method.serviceId;
        this.modelChange.emit(this.model);
    }

    /**
     * Достает список стран
     */
    getCountriesList() {
        this.dictionaryService.getCountries().subscribe(([result]) => {
            if (result) {
                this.countries = result;
            }
        });
    }

    /**
     * Является ли выбранный метод доставкой по почте
     * @returns {boolean}
     */
    isDelivery() {
        return this.model.deliveryType === DeliveryType.DELIVERY;
    }

    /**
     * Является ли выбранный метод самовывозом
     * @returns {boolean}
     */
    isCustomerPickUp() {
        return this.model.deliveryType === DeliveryType.CUSTOMER_PICKUP;
    }

    /**
     * Возвращает название метода с учётом текущего языка
     */
    getMethodName(method: PaymentMethod) {
        return this.lang === 'en' ? method.nameEn : method.name;
    }

    /**
     * Возвращает описание метода с учётом текущего языка
     */
    getMethodDescription(method: PaymentMethod) {
        return this.lang === 'en' ? method.descriptionEn : method.description;
    }
}

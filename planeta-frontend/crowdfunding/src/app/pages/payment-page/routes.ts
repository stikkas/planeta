import {PaymentPageComponent} from './payment-page.component';
import {Routes} from '@angular/router';

export const routes: Routes = [{
    path: 'campaigns/:campaignId/donate/:cartId/payment',
    // http://localhost:4200/campaigns/75372/donate/295562/payment
    component: PaymentPageComponent
}];

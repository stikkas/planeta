import {NgModule} from '@angular/core';
import {BrowserModule, Title} from '@angular/platform-browser';
import {RouterModule} from '@angular/router';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';

import {LocalizationModule} from 'angular-l10n';
import {TextMaskModule} from 'angular2-text-mask';
import {ClickOutsideModule} from 'ng-click-outside';
import {TooltipModule} from 'ngx-bootstrap/tooltip';

import {routes} from './routes';
import {PaymentPageComponent} from './payment-page.component';
import {DeliveryMethodsComponent} from './delivery-methods/pl-delivery-methods.component';
import {PaymentService} from '../../services/payment.service';
import {ProfileService} from '../../services/profile.service';
import {StickyModule} from 'ng2-sticky-kit';
import {PlCoreModule} from '@planeta/planeta-core';
import {ElasticModule} from 'angular2-elastic';
import {PlFormsModule} from '../../core/modules/pl-forms/pl-forms.module';
import {PlPreloaderModule} from '../../core/pl-preloader/pl-preloader.module';
import {PaymentMethodsModule} from '../../core/modules/payment-methods/payment-methods.module';

@NgModule({
    imports: [
        PlFormsModule,
        PaymentMethodsModule,
        BrowserModule,
        RouterModule.forChild(routes),
        FormsModule,
        ClickOutsideModule,
        HttpClientModule,
        TextMaskModule,
        LocalizationModule,
        ReactiveFormsModule,
        TooltipModule.forRoot(),
        StickyModule,
        PlCoreModule,
        ElasticModule,
        PlPreloaderModule
    ],
    exports: [],
    declarations: [
        PaymentPageComponent,
        DeliveryMethodsComponent
    ],
    providers: [
        Title,
        PaymentService,
        ProfileService
    ]
})
export class PaymentPageModule {
}


import {Title} from '@angular/platform-browser';
import {Component, OnDestroy, OnInit, ViewChild, ChangeDetectorRef} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {ActivatedRoute} from '@angular/router';

import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';

import {Language, TranslationService} from 'angular-l10n';
import {ToastrService} from 'ngx-toastr';

import {environment} from '../../../environments/environment';

import {ProjectService} from '../../services/project.service';
import {PaymentService} from '../../services/payment.service';
import {PurchaseShareInfo} from '../../models/PurchaseShareInfo';
import {PaymentData} from '../../models/PaymentData';
import {GtmService} from '../../services/gtm.service';
import {ActionStatus, ErrorField, PlAuthService, PlErrorable, PlErrorsService} from '@planeta/planeta-core';
import {Subscription} from 'rxjs/Subscription';
import {ProfileService} from '../../services/profile.service';


@Component({
    selector: 'pl-payment-page',
    templateUrl: './payment-page.component.html',
    host: {
        '(document:click)': 'onDocumentClick($event)',
        '(document:touchstart)': 'onDocumentClick($event)'
    }
})

export class PaymentPageComponent implements OnInit, OnDestroy {
    @ViewChild('myform') myFormEl;

    /**
     * Хост приложения
     * @type {string}
     */
    mainHost = environment.mainHost;

    /**
     * Информация о покупке
     * @type {null}
     */
    purchaseShareInfo: PurchaseShareInfo = new PurchaseShareInfo();

    /**
     * Текущий язык
     */
    @Language() lang: string;

    /**
     * Нажата ли галочка о согласии на обработку ПД и согласии с условиями предоставления сервиса
     */
    agreement = false;

    /**
     * Ошибка, если не нажата галочка согласия
     */
    agreementError = false;

    /**
     * Тултип ошибки, если не нажата галочка согласия
     */
    agreementErrorTooltip = false;

    /**
     * True, когда вся информация с сервера загрузилась
     */
    infoLoaded = false;

    /**
     * Данные для покупки, которые отправляются на сервер
     * @type {PaymentData}
     */
    paymentData: PaymentData = new PaymentData();

    /**
     * Объект для хранения ошибок
     */
    errors: PlErrorable;

    /**
     * Блокировать/не блокировать все блоки
     * @type {boolean}
     */
    disableAllBlocks = false;

    /**
     * Доступна или недоступна покупателю функция "Перезвоните мне"
     * @type {boolean}
     */
    isHelpBlockAvailable = false;

    /**
     * Пока
     * @type {boolean}
     */
    showHelpDropdown = false;

    /**
     * Модель, где хранятся данные о введенном телефоне в блоке "Сложности с покупкой"
     * @type {string}
     */
    callMePhone = '';

    /**
     * true, когда заявка об обратно звонке отправлена успешно
     * @type {boolean}
     */
    isCallMeSuccess = false;

    /**
     * Профиль пользователя
     */
    profile: any = null;

    /**
     * Листенер на логин/разлогин пользователя
     */
    authSubscriber: Subscription;

    /**
     * Доп настройки для инпута с телефоном
     */
    phoneInputOptions = {
        mask: ['+', '7', '(', /[1-9]/, /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/]
    };

    constructor(private httpClient: HttpClient,
                errorService: PlErrorsService,
                private activeRouter: ActivatedRoute,
                private toastr: ToastrService,
                private translationService: TranslationService,
                private projectService: ProjectService,
                private profileService: ProfileService,
                private paymentService: PaymentService,
                private gtmService: GtmService,
                private authService: PlAuthService,
                private titleService: Title,
                private cdRef: ChangeDetectorRef) {

        this.errors = errorService.getErrorable();
    }

    ngOnInit() {
        this.activeRouter.params.subscribe(params => {
            this.getPurchaseInfo(params['cartId']);
        });

        this.authService.getUserInfo().subscribe(([info]) => this.setProfile(info));
        this.authSubscriber = this.authService.authChanged.subscribe(res => this.setProfile(res));
    }

    ngOnDestroy() {
        this.authSubscriber.unsubscribe();
    }

    /**
     * Устанавливает данные профиля
     * @param profile
     */
    private setProfile(profile) {
        this.profile = profile;
        if (profile != null) {
            this.paymentData.email = profile.username;
        } else {
            this.paymentData.email = '';
        }
    }

    /**
     *
     * @param {string} infoId: Id объекта с информацией о покупке в базе
     */
    getPurchaseInfo(infoId: string) {
        this.paymentService.getPurchaseInfo(infoId).subscribe(result => {
            this.infoLoaded = true;
            if (result) {
                this.purchaseShareInfo = result;
                this.paymentData.infoId = result.id;

                this.checkIsCallMeCallbackAvailable(this.purchaseShareInfo.donateAmount);
                if (this.purchaseShareInfo.shareDetailed.linkedDeliveries.length > 0) {
                    this.paymentData.deliveryInfo.deliveryType = this.purchaseShareInfo.shareDetailed.linkedDeliveries[0].serviceType;
                    this.paymentData.deliveryInfo.serviceId = this.purchaseShareInfo.shareDetailed.linkedDeliveries[0].serviceId;
                }

                if (result.storedAddress) {
                    this.paymentData.deliveryInfo.address = result.storedAddress;
                    this.paymentData.deliveryInfo.customerContacts.customerName = result.storedAddress.customerName;
                    this.paymentData.deliveryInfo.customerContacts.phone = result.storedAddress.phone;
                }

                /**
                 * Проверить наличие пикселей и создать
                 */
                this.projectService.createCampaignTargetingPixels(result.campaign.campaignId);

                /**
                 * Выставляем title страницы
                 */
                this.titleService.setTitle(
                        this.translationService.translate('titles.payment-page-title') +
                        `"${this.purchaseShareInfo.campaign.name}"`
                );
            } else {
                // Отвправить на 404 если в параметрах указан не существующий id и с сервера вернулся болт
                window.location.href = '/404.html';
            }
        });
    }

    /**
     * Проверяет, доступность блока помощи для данной акции
     * @param {number} price
     */
    checkIsCallMeCallbackAvailable(price: number) {
        this.httpClient.get('/api/public/callback/available', {
            params: {
                'amount': price.toString()
            }
        }).subscribe(
                res => {
                    if (res['success'] && res['result']) {
                        this.isHelpBlockAvailable = true;
                    }
                },
                (error: Error) => {
                    console.log(error.message);
                    this.toastr.error(
                            this.translationService.translate('errors.server-error.message'),
                            this.translationService.translate('errors.server-error.title')
                    );
                }
        );
    }


    /**
     * Открывает / закрывает дропдаун "Связаться со мной"
     */
    toggleHelpDropdown() {
        this.showHelpDropdown = !this.showHelpDropdown;
    }

    /**
     * Закрыть дропдаун "Связаться со мной"
     */
    closeHelpDropdown() {
        this.showHelpDropdown = false;
    }

    /**
     * Отправляет на сервер запрос на создание заявки на обратный звонок
     */
    sendCallMeCallBack() {
        const callMeInfo = {
            type: 'ORDERING_PROBLEM',
            phone: this.callMePhone,
            orderType: 'SHARE',
            objectId: this.purchaseShareInfo.shareDetailed.shareId,
            amount: this.purchaseShareInfo.donateAmount
        };

        this.httpClient.post('/api/public/callback/create', callMeInfo).subscribe(
                (response: ActionStatus<any>) => {
                    if (response.success) {
                        this.isCallMeSuccess = true;
                    } else {
                        this.toastr.error(
                                this.translationService.translate('errors.server-error.message'),
                                this.translationService.translate('errors.server-error.title')
                        );
                    }
                },
                (error: Error) => {
                    console.log(error.message);
                    this.toastr.error(
                            this.translationService.translate('errors.server-error.message'),
                            this.translationService.translate('errors.server-error.title')
                    );
                }
        );
    }

    /**
     * Оплата покупки
     */
    purchase(event) {
        if ( !this.agreement ) {
            this.agreementError = true;
            this.agreementErrorTooltip = true;
            event.stopPropagation();
            return;
        }
        const self = this;
        this.httpClient.post('/api/public/campaign/validate-share-purchase', this.paymentData).subscribe(
                (response: ActionStatus<ErrorField[]>) => {
                    if (response.success) {
                        this.gtmService.trackChoicePaymentProvider(this.paymentData, this.purchaseShareInfo);
                        this.myFormEl.nativeElement.submit();
                    } else {
                        self.errors.reset(response.result);
                        this.toastr.error(
                                this.translationService.translate('errors.check-data'),
                        );
                    }
                },
                (error: Error) => {
                    console.log(error.message);
                    this.toastr.error(
                            this.translationService.translate('errors.server-error.message'),
                            this.translationService.translate('errors.server-error.title')
                    );
                }
        );
    }

    /**
     * Показывать/не показывать блок со способами доставки
     * @returns {boolean}
     */
    doRequireDelivery() {
        return this.purchaseShareInfo.shareDetailed.linkedDeliveries ?
                this.purchaseShareInfo.shareDetailed.linkedDeliveries.length > 0 : false;
    }

    /**
     * Возвращает порядковый номер блока выбора способов оплаты
     */
    getPaymentMethodsBlockTitleNumber() {
        let titleNumber = '';
        if (this.purchaseShareInfo.shareDetailed.questionToBuyer && !this.doRequireDelivery() ||
                !this.purchaseShareInfo.shareDetailed.questionToBuyer && this.doRequireDelivery()) {
            titleNumber = '2.';
        } else if (this.purchaseShareInfo.shareDetailed.questionToBuyer !== '' && this.doRequireDelivery()) {
            titleNumber = '3.';
        }
        return titleNumber;
    }

    /**
     * Устанавливает значение флага disableAllBlocks
     * @param value
     */
    visibilityChanged(value) {
        this.disableAllBlocks = value;
        this.cdRef.detectChanges();
    }

    /**
     * Получить баланс пользователя
     */
    getBalance() {
        return this.profileService.getMyBalance();
    }

    /**
     * Проверка галочки соглашения на обработку
     */
    onAgreementChange() {
        if ( this.agreement ) {
            this.agreementError = false;
            this.agreementErrorTooltip = false;
        }
    }

    /**
     * Закрыть тултип для галочки на обработку по клику на документ
     */
    onDocumentClick() {
        if ( this.agreementErrorTooltip ) {
            this.agreementErrorTooltip = false;
        }
    }
}


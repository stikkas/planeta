import {Routes} from '@angular/router';
import {RewardsPageComponent} from './rewards-page.component';
import {RewardPageComponent} from './reward/reward-page.component';
import {RewardCampaignResolver} from '../../services/routes/reward.campaign.resolver';

export const rewardsRoutes: Routes = [{
    path: 'campaigns/:alias/rewards',
    component: RewardsPageComponent,
    children: [
        {path: ':id', component: RewardPageComponent, resolve: {reward: RewardCampaignResolver}}
    ]
}];


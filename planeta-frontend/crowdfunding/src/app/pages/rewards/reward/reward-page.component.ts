import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {ImageType, PlImageUtilsService} from '@planeta/planeta-core';


/**
 * Страница одного вознаграждения, переходим по ссылки со страницы проект
 * или со страницы списка вознаграждений
 */
@Component({
    templateUrl: './reward-page.component.html'
})
export class RewardPageComponent implements OnInit {
    /**
     * Данные о вознаграждении
     */
    reward: any;

    constructor(private route: ActivatedRoute,
                private imageUtils: PlImageUtilsService) {
    }

    ngOnInit(): void {
        this.route.data.subscribe((data: { reward: any }) => {
            this.reward = data.reward;
            console.log(this.reward);
        });
    }
 // fancybox
 // href="{{= ImageUtils.getThumbnailUrl(imageUrl, ImageUtils.CAMPAIGN_VIDEO, ImageType.PHOTO)}}">
    getThumbnailUrl(): string {
        return this.imageUtils.getThumbnailUrl(this.reward.imageUrl, PlImageUtilsService.MEDIUM, ImageType.GROUP);
    }
}



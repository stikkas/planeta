import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {CommonModule} from '@angular/common';

import {rewardsRoutes} from './routes';
import {RewardsPageComponent} from './rewards-page.component';
import {RewardPageComponent} from './reward/reward-page.component';
import {RewardCampaignResolver} from '../../services/routes/reward.campaign.resolver';
import {PlCoreModule} from '@planeta/planeta-core';


@NgModule({
    imports: [
        CommonModule,
        PlCoreModule,
        RouterModule.forChild(rewardsRoutes)
    ],
    exports: [],
    declarations: [
        RewardsPageComponent,
        RewardPageComponent
    ],
    providers: [RewardCampaignResolver]
})
export class RewardsPageModule {
}


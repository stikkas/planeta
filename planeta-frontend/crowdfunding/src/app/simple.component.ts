import {Component} from '@angular/core';


@Component({
    selector: 'pl-simple',
    template: `
        <button (click)="clickMe($event)">Click Me</button>`
})
export class SimpleComponent {
    clickMe(event) {
        console.log(event);
        console.log('Click MEEEEEE');
    }
}


import {Routes} from '@angular/router';
import {PageNotFoundComponent} from './pages/page-not-found/page-not-found.component';

export const routes: Routes = [{
    path: 'account',
    loadChildren: './pages/profile-pages/profile-page.module#ProfilePageModule'
}, {
    path: 'campaigns',
    loadChildren: './pages/campaigns/campaigns-page.module#CampaignsPageModule'
}, {
    path: '404.html',
    component: PageNotFoundComponent
}, {
    path: '**',
    component:
    PageNotFoundComponent
}];


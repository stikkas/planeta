import {BrowserModule, Title} from '@angular/platform-browser';
import {APP_INITIALIZER, NgModule} from '@angular/core';
import {HttpClientModule} from '@angular/common/http';
import {RouterModule} from '@angular/router';
import {L10nConfig, L10nLoader, LocaleService, ProviderType, StorageStrategy, LocalizationModule} from 'angular-l10n';
import {ToastrModule} from 'ngx-toastr';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

import {PlLoginPopupModule} from '@planeta/planeta-login-popup';
import {PlHeaderModule} from '@planeta/planeta-header';
import {PlFooterModule} from '@planeta/planeta-footer';

import {AppComponent} from './app.component';
import {DictionaryService} from './services/dictionary.service';
import {PageNotFoundComponent} from './pages/page-not-found/page-not-found.component';
import {PaymentPageModule} from './pages/payment-page/payment-page.module';
import {routes} from './routes';
import {environment} from '../environments/environment';
import {ProjectService} from './services/project.service';
import {GtmService} from './services/gtm.service';
import {BsDropdownModule, ModalModule, TooltipModule} from 'ngx-bootstrap';
import {PlCoreModule} from '@planeta/planeta-core';
import {ProfilePageModule} from './pages/profile-pages/profile-page.module';

import {AccountCanActivate} from './services/routes/account.can.activate';
import {PHtmlTransform} from './pipes/p-html-transform.pipe';
import {CampaignsPageModule} from './pages/campaigns/campaigns-page.module';
import {RewardsService} from './services/rewards.service';
import {SimpleComponent} from './simple.component';
import {MyPurchaseTypeActivate} from './services/routes/my-purchase-type.activate';
import {PlSupportPopupModule} from '@planeta/planeta-support-popup';

const l10nConfig: L10nConfig = {
    locale: {
        languages: [
            {code: 'ru', dir: 'ltr'},
            {code: 'en', dir: 'ltr'}
        ],
        storage: StorageStrategy.Cookie
    },
    translation: {
        providers: [
            {type: ProviderType.Static, prefix: './assets/i18n/footer/'},
            {type: ProviderType.Static, prefix: './assets/i18n/header/'},
            {type: ProviderType.Static, prefix: './assets/i18n/core/'},
            {type: ProviderType.Static, prefix: './assets/i18n/login-popup/'},
            {type: ProviderType.Static, prefix: './assets/i18n/support-popup/'},
            {type: ProviderType.Static, prefix: './assets/i18n/web/core_'},
            {type: ProviderType.Static, prefix: './assets/i18n/web/editor_'},
            {type: ProviderType.Static, prefix: './assets/i18n/web/errors_'},
            {type: ProviderType.Static, prefix: './assets/i18n/web/pages_'}
        ],
        caching: true,
        composedKeySeparator: '.'
    }
};

export const MY_ID = 'crowdfunding';

export function initL10n(l10nLoader: L10nLoader, locale: LocaleService): Function {
    locale.setDefaultLocale('ru', 'RU');
    return () => l10nLoader.load();
}

@NgModule({
    declarations: [
        AppComponent,
        PageNotFoundComponent,
        SimpleComponent
    ],
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        BsDropdownModule.forRoot(),
        ModalModule.forRoot(),
        RouterModule.forRoot(routes, {enableTracing: environment.traceRoute}),
        HttpClientModule,
        ProfilePageModule,
        CampaignsPageModule,
        PlSupportPopupModule,
        PlHeaderModule.forRoot({
            shopHost: environment.shopHost,
            mainHost: environment.mainHost,
            schoolHost: environment.schoolHost,
            charityHost: environment.charityHost,
            adminHost: environment.adminHost,
            biblioHost: environment.biblioHost
        }),
        PlFooterModule.forRoot(),
        LocalizationModule.forRoot(l10nConfig),
        PaymentPageModule,
        PlLoginPopupModule,
        PlCoreModule.forRoot({
            staticNodes: environment.staticNodes
        }),
        ToastrModule.forRoot({
            closeButton: false,
            timeOut: 10000,
            positionClass: 'toast-top-right',
            preventDuplicates: false,
            progressBar: true
        }),
        TooltipModule.forRoot()
    ],
    providers: [
        ProjectService,
        RewardsService,
        DictionaryService,
        Title,
        AccountCanActivate,
        MyPurchaseTypeActivate,
        GtmService,
        {
            provide: APP_INITIALIZER,
            useFactory: initL10n,
            deps: [L10nLoader, LocaleService],
            multi: true
        },
        PHtmlTransform
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}


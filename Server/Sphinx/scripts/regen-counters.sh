#!/bin/bash

if [ ${#@} -eq 0 ]; then 
    echo Regenerates counters after change of sharding scheme
    echo "usage: $(basename $0) sphinx.conf | psql database"1>&2
    exit 1
fi

grep '^source' $1 | egrep -v 'delta|=' | awk '
BEGIN{
    srand(systime());
    print "DELETE FROM sphinx.sph_counter;"
}
{
    print "INSERT INTO sphinx.sph_counter (source_id, source_name, range_max_id) \
        VALUES (" int(rand()*systime()) ", '\''" $2 "'\'', 0);"
}
'

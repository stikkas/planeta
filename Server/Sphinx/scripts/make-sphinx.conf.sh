#!/bin/bash

CONFIGDIR=${CONFIGDIR:-/etc/sphinxsearch}
VARDIR=${VARDIR:-/var/lib/sphinxsearch}

SPHINX_CONFIGDIR=${SPHINX_CONFIGDIR:-/etc/sphinxsearch}
SPHINX_VARDIR=${SPHINX_VARDIR:-/var/lib/sphinxsearch}

function replacer_src() {
    while read a; do

	a=$(echo $a | sed -e "
	s/@shardName@/$shardName/g;
	s/@sqlHost@/$sqlHost/g;
	s/@sqlPort@/$sqlPort/g;
	s/@sqlPass@/$sqlPass/g;
	s/@sqlUser@/$sqlUser/g;
	s/@sqlDb@/$sqlDb/g
	")

        A=$(echo $a | sed -ne '/source .*#index.*/s/source \(\w\+\).*#index \(\w\+\).*/\1 \2/gp')
        if [ -n "$A" ]; then
            b=($A)
            set -x
            INDEX=${b[1]} 
            SRC=${b[0]}
            INDEX_SRC="${INDEX_SRC} $SRC"
            set +x
            echo $a | sed -e 's/#index.*$//'
        else
            echo $a
        fi
    done
}

function replacer_index() {
    while read a; do
        A=`echo $a | sed -ne '/index .*/s/index \(\w\+\)[^:]*$/\1/gp'`
        if [ -n "$A" ]; then
            B="$A"
        fi
        if echo $a | grep -q @shard_src@; then
            for i in ${INDEX_SRC[$B]}; do
                echo ${a/@shard_src@/$i}
            done
        else
            echo ${a/@vardir@/$SPHINX_VARDIR}
        fi
    done
}

for j in /opt/sphinxsearch/config/*sharded*.src.conf; do
    INDEX_SRC=""
    (
        for i in /opt/sphinxsearch/config/shard*.conf; do
            source "$i"
            replacer_src < "$j"
        done
        replacer_index < "${j/src/index}"
    )
done 
cat /opt/sphinxsearch/config/searchd.conf

#!/bin/bash
set -e
set -x

sudo /usr/bin/indexer --rotate --all --verbose | awk '{if ($0 ~ /ERROR|FATAL/) {print; exit_code = 1; errors = errors "\n" $0} else {print} } END {print errors; exit exit_code}'


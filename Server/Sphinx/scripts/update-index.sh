#!/bin/bash

set -x

#open pipe
exec 101> >(psql -U planeta -d userdb01)
exec 102> >(psql -U planeta -d userdb02)

#new
echo "SELECT MAX(profile_id) AS id INTO TEMPORARY max_profile_id FROM profiledb.profiles;" >&101
echo "SELECT MAX(profile_id) AS id INTO TEMPORARY max_profile_id FROM profiledb.profiles;" >&102
echo "SELECT MAX(campaign_id) AS id INTO TEMPORARY max_campaign_id FROM commmondb.campaigns;" >&101
echo "SELECT MAX(campaign_id) AS id INTO TEMPORARY max_campaign_id FROM commondb.campaigns;" >&102
echo "SELECT MAX(id) AS id INTO TEMPORARY max_post_id FROM profiledb.posts;" >&101
echo "SELECT MAX(id) AS id INTO TEMPORARY max_post_id FROM profiledb.posts;" >&102
echo "SELECT MAX(broadcast_id) AS id INTO TEMPORARY max_broadcast_id FROM profiledb.broadcasts;" >&101
echo "SELECT MAX(broadcast_id) AS id INTO TEMPORARY max_broadcast_id FROM profiledb.broadcasts;" >&102
echo "SELECT MAX(seminar_id) AS id INTO TEMPORARY max_seminar_id FROM commondb.seminar;" >&101
echo "SELECT MAX(seminar_id) AS id INTO TEMPORARY max_seminar_id FROM commondb.seminar;" >&102
indexer --merge user user_delta --rotate
indexer --merge profile profile_delta --rotate
indexer --merge campaign campaign_delta --rotate
indexer --merge post post_delta --rotate
indexer --merge broadcast broadcast_delta --rotate
indexer seminar --rotate
echo "UPDATE sphinx.sph_counter SET range_max_id = (SELECT id FROM max_profile_id) WHERE source_name='user_src';" >&101
echo "UPDATE sphinx.sph_counter SET range_max_id = (SELECT id FROM max_profile_id) WHERE source_name='user_src';" >&102
echo "UPDATE sphinx.sph_counter SET range_max_id = (SELECT id FROM max_campaign_id) WHERE source_name='campaign_src';" >&101
echo "UPDATE sphinx.sph_counter SET range_max_id = (SELECT id FROM max_campaign_id) WHERE source_name='campaign_src';" >&102
echo "UPDATE sphinx.sph_counter SET range_max_id = (SELECT id FROM max_post_id) WHERE source_name='post01_src';" >&101
echo "UPDATE sphinx.sph_counter SET range_max_id = (SELECT id FROM max_post_id) WHERE source_name='post02_src';" >&102
echo "UPDATE sphinx.sph_counter SET range_max_id = (SELECT id FROM max_broadcast_id) WHERE source_name='broadcast01_src';" >&101
echo "UPDATE sphinx.sph_counter SET range_max_id = (SELECT id FROM max_broadcast_id) WHERE source_name='broadcast02_src';" >&102
echo "UPDATE sphinx.sph_counter SET range_max_id = (SELECT id FROM max_seminar_id) WHERE source_name='seminar01_src';" >&101
echo "UPDATE sphinx.sph_counter SET range_max_id = (SELECT id FROM max_seminar_id) WHERE source_name='seminar02_src';" >&102

#close pipe
exec 101>&-;
exec 102>&-;

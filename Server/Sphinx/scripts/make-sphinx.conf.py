#!/usr/bin/env python

import os
import glob
import ConfigParser
import StringIO

vardir = "/var/lib/sphinxsearch"
configdir = '/opt/sphinxsearch/config'

indexsrcs = {}

def build_sources(path, srcs, shards):
  configs = [];
  for source in glob.glob( os.path.join(path, shards) ):
    parser = ConfigParser.SafeConfigParser()
    fd = open(source)
    ifd = StringIO.StringIO()
    ifd.write('[default]\n')
    for line in fd.readlines():
      ifd.write(line)
    fd.close()
    parser.readfp(StringIO.StringIO(ifd.getvalue()), source)
    configs.append(parser)
  for source in glob.glob( os.path.join(path, srcs) ):
    fd = open(source, 'r')
    lines = fd.readlines()
    fd.close()
    for config in configs:
      for line in lines:
        splitline = line.split()
        is_src_line = len(splitline) > 0 and splitline[0].strip() == 'source'
        if is_src_line:
          print line.strip().split('#index')[0] \
            .replace('@shardName@', config.get('default', 'shardName').strip('"'))
        else:
          print line.strip() \
            .replace('@shardName@', config.get('default', 'shardName').strip('"')) \
            .replace('@sqlHost@', config.get('default', 'sqlHost').strip('"')) \
            .replace('@sqlPort@', config.get('default', 'sqlPort').strip('"')) \
            .replace('@sqlPass@', config.get('default', 'sqlPass').strip('"')) \
            .replace('@sqlUser@', config.get('default', 'sqlUser').strip('"')) \
            .replace('@sqlDb@', config.get('default', 'sqlDb').strip('"'))
        if is_src_line:
          srcname = line.split()[1].strip().replace('@shardName@', config.get('default', 'shardName').strip('"'))
          indexname = line.split('#index')
          if len(indexname) > 1:
            indexname = indexname[1].strip()
            if indexsrcs.get(indexname) == None:
              indexsrcs[indexname] = []
            indexsrcs[indexname].append(srcname)

def build_indexes(path, indexes):
  for indexfile in glob.glob( os.path.join(path, indexes) ):
    fd = open(indexfile, 'r')
    lines = fd.readlines()
    fd.close()
    indexname = ''
    for line in lines:
      if indexname == '' and len(line.split()) > 0 and line.split()[0].strip() == 'index':
        indexname = line.split()[1].strip()
      if line.count('@shard_src@') > 0:
        if indexsrcs.get(indexname) != None:
          for src in indexsrcs.get(indexname):
            print line.strip().replace('@shard_src@', src);
      else:
        print line.strip() \
          .replace('@vardir@', vardir)

def build_searchd(path, configs):
  for config in glob.glob( os.path.join(path, configs) ):
    print open(config, 'r').read()

# sharded
build_sources(configdir, '*sharded*.src.conf', 'shard*.conf')
build_indexes(configdir, '*sharded*.index.conf')
# common
build_sources(configdir, '*common.*.src.conf', 'commondb.conf')
build_indexes(configdir, '*common.*.index.conf')
# common_user
build_sources(configdir, '*common_user*.src.conf', 'commondb_user.conf')
build_indexes(configdir, '*common_user*.index.conf')

# searchd
build_searchd(configdir, 'searchd.conf')

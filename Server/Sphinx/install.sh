#!/bin/bash
set -e
set -x

PROFILE=${PROFILE:-Dev}
DESTDIR=/etc/sphinxsearch
VARDIR=/var/lib/sphinxsearch

rm -rf /opt/sphinxsearch/config/
cp -a /opt/sphinxsearch/ALL/config /opt/sphinxsearch
cp -a /opt/sphinxsearch/${PROFILE}/config /opt/sphinxsearch

(
    export CONFIGDIR="${DESTDIR}"
    export VARDIR="${VARDIR}"
    python "/opt/sphinxsearch/scripts/make-sphinx.conf.py" | sudo tee /etc/sphinxsearch/sphinx.conf &>/dev/null
)

cat /opt/sphinxsearch/stopwords.txt      | sudo tee ${VARDIR}/dict/stopwords.txt &>/dev/null
cat /opt/sphinxsearch/ru_wordforms/*.txt | sudo tee ${VARDIR}/dict/ru_wordforms.txt &>/dev/null
cat /opt/sphinxsearch/exceptions.txt     | sudo tee ${VARDIR}/dict/exceptions.txt &>/dev/null

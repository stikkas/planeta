import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.*;
import java.util.regex.Pattern;

public class MakeSphinxConfig {
    private static final List<File> fileList = new ArrayList<File>();
    private static final StringBuilder sb = new StringBuilder();
    private static String varDir = "";
    private static List<String> shardSrcList;

    public static void main(String... args) throws Exception {
        String checkFile = null;
        for (int i = 0; i < args.length; i++) {
            String arg = args[i];
            if (arg.equals("--vardir")) {
                varDir = args[++i];
            } else if (arg.equals("--check")) {
                checkFile = args[++i];
            } else {
                addFiles(arg);
            }

        }

        build("index.sharded.", "shard.");
        build("index.common.", "commondb.");

        System.out.println(sb.toString());

        if (checkFile != null) {
            check(stringFromFile(new File(checkFile)), sb.toString());
        }

    }

    private static void build(String fileConfStartWith, String fileDbConfStartWith) {
        for (String indexName : getIndexNameList(fileConfStartWith)) {
            shardSrcList = new ArrayList<String>();
            buildSources(fileConfStartWith + indexName + "*.src.conf", fileDbConfStartWith);
            buildIndexes(fileConfStartWith + indexName + "*.index.conf");
        }
    }

    private static Set<String> getIndexNameList(String fileConfStartWith) {
        Set<String> result = new HashSet<String>();
        for (File file : fileList) {
            if (file.getName().startsWith(fileConfStartWith)) {
                String name = file.getName().substring(fileConfStartWith.length());
                result.add(name.substring(0, name.indexOf('.')));
            }
        }
        return result;
    }


    private static void buildIndexes(String fileConfMask) {
        for (File fileConf : getFileListByMask(fileConfMask)) {
            buildIndex(fileConf);
        }

    }

    private static void buildIndex(File file) {
        for (String line : stringListFromFile(file)) {
            if (line.indexOf("@shard_src@") > 0) {
                for (String shardSrc : shardSrcList) {
                    sb.append(line.replaceAll("@shard_src@", shardSrc));
                }
            } else {
                sb.append(line.replaceAll("@vardir@", varDir));
            }
            sb.append("\n");
        }
    }


    private static void buildSources(String fileConfMask, String fileDbConfMask) {
        for (File fileDbConf : getFileListByMask(fileDbConfMask)) {
            Properties dbParam = getDbParam(fileDbConf.getAbsolutePath());
            for (File fileConf : getFileListByMask(fileConfMask)) {
                buildSource(fileConf, dbParam);
            }
        }
    }


    private static void buildSource(File file, Properties dbParam) {
        String fileContext = stringFromFile(file);
        for (String propertyName : dbParam.stringPropertyNames()) {
            fileContext = fileContext.replaceAll('@' + propertyName + '@', unquote(dbParam.getProperty(propertyName)));
        }

        int indexCommentPos = 0;
        while (true) {
            indexCommentPos = fileContext.indexOf("#index ", indexCommentPos + 7);
            if (indexCommentPos < 0) {
                break;
            }
            int sourcePos = fileContext.lastIndexOf("source ", indexCommentPos);
            if (sourcePos < 0) {
                break;
            }
            String sourceName = fileContext.substring(sourcePos + 7, indexCommentPos).trim();
            String indexName = fileContext.substring(indexCommentPos + 7, fileContext.indexOf('{', indexCommentPos)).trim();
            shardSrcList.add(sourceName);

        }

        sb.append(fileContext);
    }

    private static List<File> getFileListByMask(String mask) {
        List<File> result = new ArrayList<File>();
        Pattern pattern = Pattern.compile(mask.replace(".", "\\.").replace("*", "[^.]*"));
        for (File file : fileList) {
            if (pattern.matcher(file.getName()).find()) {
                result.add(file);
            }
        }
        return result;
    }

    private static Properties getDbParam(String fileName) {
        Properties properties = new Properties();
        try {
            properties.load(new FileInputStream(fileName));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return properties;
    }


    private static void addFiles(String path) {
        for (File file : (new File(path)).listFiles()) {
            fileList.add(file);
        }
    }

    static String stringFromFile(File file) {
        try {
            Scanner scanner = new Scanner(file).useDelimiter("\\A");
            if (scanner.hasNext()) {
                return scanner.next();
            } else {
                return "";
            }
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

    static List<String> stringListFromFile(File file) {
        List<String> list = new ArrayList<String>();
        try {
            Scanner scanner = new Scanner(file).useDelimiter("\n");
            while (scanner.hasNext()) {
                list.add(scanner.next());
            }
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        }
        return list;
    }


    private static String unquote(String s) {
        s = s.trim();
        return s.startsWith("\"") && s.endsWith("\"") ? s.substring(1, s.length() - 1) : s;
    }

    /**
     * ************************************************
     */
    private static void check(String s1, String s2) {
        Map<String, String> m1 = toMap(s1);
        Map<String, String> m2 = toMap(s2);
        for (String key : m1.keySet()) {
            if (!m2.containsKey(key)) {
                System.out.println("exists in s1 not exists in s2 " + key);
            }
        }

        for (String key : m2.keySet()) {
            if (!m1.containsKey(key)) {
                System.out.println("exists in s2 not exists in s1 " + key);
            }
        }

        for (String key : m1.keySet()) {
            if (m2.containsKey(key) && !m1.get(key).replaceAll("\\s+", " ").equals(m2.get(key).replaceAll("\\s+", " "))) {
                System.out.println("!= " + key);
            }
        }


    }

    private static Map<String, String> toMap(String s) {
        Map<String, String> map = new HashMap<String, String>();
        int i = 0;
        while (true) {
            int st = s.indexOf('{', i);
            int en = s.indexOf('}', i);
            if (st < 0 && en < 0) {
                break;
            }
            String name = s.substring(i, st).trim();
            if (name.indexOf('#') > 0) {
                name = name.substring(0, name.indexOf('#')).trim();
            }
            String context = s.substring(st + 1, en);
            map.put(name, context);
            i = en + 1;
        }
        if (i < s.length() && s.substring(i, s.length()).trim().isEmpty()) {
            System.out.println(s.substring(i, s.length()));
        }
        return map;
    }
}

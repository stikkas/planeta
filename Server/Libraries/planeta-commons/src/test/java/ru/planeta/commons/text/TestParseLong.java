package ru.planeta.commons.text;

import org.apache.commons.lang3.math.NumberUtils;
import org.junit.Ignore;
import org.junit.Test;

import java.util.Scanner;

/**
 * User: a.savanovich
 * Date: 04.10.13
 * Time: 17:20
 */
@Ignore
public class TestParseLong {
    @Test
    public void test() {
        long start = System.currentTimeMillis();
        for(int i = 0; i < 100000; ++i) {
            parseLong(String.valueOf(i));
        }
        long end = System.currentTimeMillis();
        System.out.println("----------------");
        System.out.println(end - start);

        start = System.currentTimeMillis();
        for(int i = 0; i < 100000; ++i) {
            parseScanner(String.valueOf(i));
        }
        end = System.currentTimeMillis();
        System.out.println("----------------");
        System.out.println(end - start);


    }

    private void parseLong(String alias) {
        long profileId = NumberUtils.toLong(alias, 0);
        if (profileId == 0) {
            System.out.print(profileId);
        } else {
            System.out.print(profileId);
        }
    }

    private void parseScanner(String alias) {
        Scanner scanner = new Scanner(alias);
        long profileId = scanner.hasNextLong(10) ? scanner.nextLong(10) : 0;
        if (scanner.hasNext()) {
            System.out.print(profileId);
        } else {
            System.out.print(profileId);
        }
    }
}

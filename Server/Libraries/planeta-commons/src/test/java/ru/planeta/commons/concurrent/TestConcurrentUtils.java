package ru.planeta.commons.concurrent;

import org.apache.log4j.Logger;
import org.junit.Ignore;
import org.junit.Test;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.*;
import java.util.concurrent.locks.Lock;

import static org.junit.Assert.fail;

/**
 * User: a.savanovich
 * Date: 16.02.12
 * Time: 11:07
 */
public class TestConcurrentUtils {
    private static Logger log = Logger.getLogger(TestConcurrentUtils.class);

    private class TestThread extends Thread {
        TestThread(LockManager lockManager, Object lockObject) {
            this.lockManager = lockManager;
            this.lockObject = lockObject;
        }

        LockManager lockManager;
        Object lockObject;
    }

    final Lock lock = new ReentrantLock();
    final Condition sharedLockAcquired = lock.newCondition();
    final Condition exclusiveLockAcquired = lock.newCondition();

    private class SharedTestThread extends TestThread {
        SharedTestThread(LockManager lockManager, Object lockObject, int count) {
            super(lockManager, lockObject);
            this.count = count;
        }

        private int count;

        public void run() {
            lock.lock();
            try {
                lockManager.acquireSharedLock(lockObject);
                log.error("share " + count);
                sharedLockAcquired.signal();
                lockManager.releaseSharedLock(lockObject);
                if (!exclusiveLockAcquired.await(10, TimeUnit.SECONDS)) {
                    log.error("false");
                    fail();
                }
            } catch (InterruptedException e) {
                log.error("qqq", e);
            } finally {
                lock.unlock();
            }
        }
    }


    @Ignore
    @Test
    public void testConcurrent() throws InterruptedException {

        final LockManager lockManager = new LockManager();
        final String lockUrl = "http://google.com";

        log.error(lockUrl);


        Thread firstThread = new SharedTestThread(lockManager, lockUrl, 1);
        Thread secondThread = new TestThread(lockManager, lockUrl) {

            public void run() {
                lock.lock();
                try {
                    sharedLockAcquired.await();
                    log.error("acquire exclusive lock");
                    lockManager.acquireExclusiveLock(lockUrl);
                    log.error("exclusive");
                    lockManager.releaseExclusiveLock(lockUrl);
                    exclusiveLockAcquired.signalAll();
                } catch (InterruptedException e) {
                    log.error("www", e);
                } finally {
                    lock.unlock();
                }
            }
        };

        Thread thirdThread = new SharedTestThread(lockManager, lockUrl, 2);
        Thread forthThread = new SharedTestThread(lockManager, lockUrl, 3);
        secondThread.start();
        firstThread.start();

        thirdThread.start();
        forthThread.start();

        // wait for all threads
        firstThread.join();
        secondThread.join();
        thirdThread.join();
        forthThread.join();

    }


    @Ignore
    @Test
    public void testExclusiveAfterShared() throws InterruptedException {
        final String lockUrl = "http://mail.ru";
        LockManager lockManager = new LockManager();

        log.debug(lockUrl);

        Thread firstThread = new TestThread(lockManager, lockUrl) {

            public void run() {
                lockManager.acquireSharedLock(lockUrl);
                try {

                    log.debug("shared 1");
                    lockManager.acquireExclusiveLock(lockUrl);
                    try {
                        log.error("exclusive 1");
                    } finally {
                        lockManager.releaseExclusiveLock(lockUrl);
                    }
                    log.debug("shared 2");
                } finally {
                    lockManager.releaseSharedLock(lockUrl);
                }


            }
        };

        firstThread.start();
        firstThread.join();
    }


    @Ignore
    @Test
    public void testExclusiveAfterShared2() throws InterruptedException {
        final String lockUrl = "http://mail.ru";
        LockManager lockManager = new LockManager();

        log.debug(lockUrl);

        Thread firstThread = new TestThread(lockManager, lockUrl) {

            public void run() {
                lockManager.acquireSharedLock(lockUrl);
                try {

                    log.debug("shared 1");
                    lockManager.acquireExclusiveLock(lockUrl);
                    try {
                        log.error("exclusive 1");
                    } finally {
                        lockManager.releaseExclusiveLock(lockUrl);
                    }
                    log.debug("shared 2");
                } finally {
                    lockManager.release(lockUrl);
                }

            }
        };

        firstThread.start();
        firstThread.join();
    }

    @Test(expected = IllegalMonitorStateException.class)
    public void testSecondReadUnLock() {
        ReadWriteLock lock = new ReentrantReadWriteLock();
        lock.readLock().lock();
        lock.readLock().unlock();
        lock.readLock().unlock();

    }

    @Test
    @Ignore
    // only fo me. don't read this stupid code.
    public void badTestWithSleep() throws InterruptedException {

        LockManager lockManager = new LockManager();

        final String lockUrl = "http://yahoo.com";

        log.debug(lockUrl);

        Thread firstThread = new TestThread(lockManager, lockUrl) {

            public void run() {

                lockManager.acquireExclusiveLock(lockUrl);
                try {
                    sleep(5000);
                } catch (InterruptedException e) {
                    log.error("time", e);
                }
                log.error("exclusive 1");
                lockManager.releaseExclusiveLock(lockUrl);

            }
        };

        Thread secondThread = new TestThread(lockManager, lockUrl) {

            public void run() {
                try {
                    sleep(1000);
                } catch (InterruptedException e) {
                    log.error("time", e);
                }

                lockManager.acquireExclusiveLock(lockUrl);
                try {
                    sleep(5000);
                } catch (InterruptedException e) {
                    log.error("time", e);
                }
                log.error("exclusive 4");
                lockManager.releaseExclusiveLock(lockUrl);
            }
        };


        Thread thirdThread = new TestThread(lockManager, lockUrl) {

            public void run() {

                lockManager.acquireSharedLock(lockUrl);
                try {
                    sleep(5000);
                } catch (InterruptedException e) {
                    log.error("time", e);
                }
                log.error("shared 3");
                lockManager.releaseSharedLock(lockUrl);
            }
        };


        Thread forthThread = new TestThread(lockManager, lockUrl) {

            public void run() {

                lockManager.acquireSharedLock(lockUrl);
                try {
                    sleep(5000);
                } catch (InterruptedException e) {
                    log.error("time", e);
                }
                log.error("shared 4");
                lockManager.releaseSharedLock(lockUrl);
            }
        };


        firstThread.start();
        thirdThread.start();
        forthThread.start();
        secondThread.start();
        // do ----------
        firstThread.join();
        secondThread.join();
        thirdThread.join();
        forthThread.join();
    }

}

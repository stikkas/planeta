package ru.planeta.commons.web.typograf;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TypografAsyncWebClientTest {

    private static final String input = "<p:photo url=\"http://www.some-url.ru\"/> - Это \"Типограф\"?\n" +
            " — Нет, это «Типограф»!";
    private static final String expected = "<p:photo url=\"http://www.some-url.ru\"/>&nbsp;&mdash; Это &laquo;Типограф&raquo;? " +
            "&mdash;&nbsp;Нет, это &laquo;Типограф&raquo;!";


    @Test
    public void testProcessOnceBlocking() throws Exception {
        try {
            String typografiedTextHtml = TypografAsyncWebClient.processOnceBlocking(input);
            assertEquals(expected, typografiedTextHtml);
        } catch (Exception ignore) {}
    }
}

package ru.planeta.commons.text;

import org.junit.Test;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

import static org.junit.Assert.assertEquals;

/**
 * User: m.shulepov
 * Date: 03.04.12
 * Time: 16:20
 */
public class TestProfanityFilter {

    @Test
    public void testFilter() throws UnsupportedEncodingException {
//       Сука текст бла бла бля
        String text = URLDecoder.decode("%D0%A1%D1%83%D0%BA%D0%B0%20%D1%82%D0%B5%D0%BA%D1%81%D1%82%20%D0%B1%D0%BB%D0%B0%20%D0%B1%D0%BB%D0%B0%20%D0%B1%D0%BB%D1%8F", "utf-8");
        String filteredText = ProfanityFilter.filter(text);
//       **** текст бла бла ***
        String expectedText = URLDecoder.decode("****%20%D1%82%D0%B5%D0%BA%D1%81%D1%82%20%D0%B1%D0%BB%D0%B0%20%D0%B1%D0%BB%D0%B0%20***", "utf-8");
        assertEquals(expectedText, filteredText);

//       Сука бля блядское отродие
        text = URLDecoder.decode("%D0%A1%D1%83%D0%BA%D0%B0%20%D0%B1%D0%BB%D1%8F%20%D0%B1%D0%BB%D1%8F%D0%B4%D1%81%D0%BA%D0%BE%D0%B5%20%D0%BE%D1%82%D1%80%D0%BE%D0%B4%D0%B8%D0%B5", "utf-8");
        filteredText = ProfanityFilter.filter(text);
//       **** *** ******** отродие
        expectedText = URLDecoder.decode("****%20***%20********%20%D0%BE%D1%82%D1%80%D0%BE%D0%B4%D0%B8%D0%B5", "utf-8");
        assertEquals(expectedText, filteredText);

//       Сукабля блядское отродие
        text = URLDecoder.decode("%D0%A1%D1%83%D0%BA%D0%B0%D0%B1%D0%BB%D1%8F%20%D0%B1%D0%BB%D1%8F%D0%B4%D1%81%D0%BA%D0%BE%D0%B5%20%D0%BE%D1%82%D1%80%D0%BE%D0%B4%D0%B8%D0%B5", "utf-8");
        filteredText = ProfanityFilter.filter(text);
//       ******* ******** отродие
        expectedText = URLDecoder.decode("*******%20********%20%D0%BE%D1%82%D1%80%D0%BE%D0%B4%D0%B8%D0%B5", "utf-8");
        assertEquals(expectedText, filteredText);

//       С_у_к_а б_л_я $блядское$ отродие
        text = URLDecoder.decode("%D0%A1_%D1%83_%D0%BA_%D0%B0%20%D0%B1_%D0%BB_%D1%8F%20%24%D0%B1%D0%BB%D1%8F%D0%B4%D1%81%D0%BA%D0%BE%D0%B5%24%20%D0%BE%D1%82%D1%80%D0%BE%D0%B4%D0%B8%D0%B5", "utf-8");
        filteredText = ProfanityFilter.filter(text);
//       ****** ***** ********** отродие
        expectedText = URLDecoder.decode("*******%20*****%20**********%20%D0%BE%D1%82%D1%80%D0%BE%D0%B4%D0%B8%D0%B5", "utf-8");
        assertEquals(expectedText, filteredText);

//      на столе, под столом, у стола, по да на
        text = URLDecoder.decode("%D0%BD%D0%B0+%D1%81%D1%82%D0%BE%D0%BB%D0%B5%2C+%D0%BF%D0%BE%D0%B4+%D1%81%D1%82%D0%BE%D0%BB%D0%BE%D0%BC%2C+%D1%83+%D1%81%D1%82%D0%BE%D0%BB%D0%B0%2C+%D0%BF%D0%BE+%D0%B4%D0%B0+%D0%BD%D0%B0", "utf-8");
        filteredText = ProfanityFilter.filter(text);
//      на столе, под столом, у стола, по да на
        expectedText = URLDecoder.decode("%D0%BD%D0%B0+%D1%81%D1%82%D0%BE%D0%BB%D0%B5%2C+%D0%BF%D0%BE%D0%B4+%D1%81%D1%82%D0%BE%D0%BB%D0%BE%D0%BC%2C+%D1%83+%D1%81%D1%82%D0%BE%D0%BB%D0%B0%2C+%D0%BF%D0%BE+%D0%B4%D0%B0+%D0%BD%D0%B0", "utf-8");
        assertEquals(expectedText, filteredText);

        text = URLDecoder.decode("124 32 456", "utf-8");
        filteredText = ProfanityFilter.filter(text);

        expectedText = URLDecoder.decode("124 32 456", "utf-8");
        assertEquals(expectedText, text);

    }
}

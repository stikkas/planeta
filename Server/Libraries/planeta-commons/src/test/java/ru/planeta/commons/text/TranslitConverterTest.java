package ru.planeta.commons.text;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 *
 * Created by asavan on 16.01.2017.
 */
public class TranslitConverterTest {

    @Test
    public void testLength() throws Exception {
        assertEquals(TranslitConverter.ENGLISH_KEYBOARD_LAYOUT.length(), TranslitConverter.RUSSIAN_KEYBOARD_LAYOUT.length());
    }

    @Test
    public void changeEngToRusLayout() throws Exception {
        assertEquals("№", TranslitConverter.changeEngToRusLayout("#"));
        assertEquals("Ё", TranslitConverter.changeEngToRusLayout("~"));
    }

    @Test
    public void changeRusToEngLayout() throws Exception {
        assertEquals(":", TranslitConverter.changeRusToEngLayout("Ж"));
        assertEquals("\"", TranslitConverter.changeRusToEngLayout("Э"));
        assertEquals(",", TranslitConverter.changeRusToEngLayout("б"));
        assertEquals(".", TranslitConverter.changeRusToEngLayout("ю"));
        assertEquals("<", TranslitConverter.changeRusToEngLayout("Б"));
        assertEquals(">", TranslitConverter.changeRusToEngLayout("Ю"));
    }

}
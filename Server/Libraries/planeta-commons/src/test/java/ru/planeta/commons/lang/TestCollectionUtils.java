package ru.planeta.commons.lang;

import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * User: nzabolotko
 * Date: 12.07.12
 * Time: 12:27
 */
public class TestCollectionUtils {

    @Test
    public void testSubList() {
        int itemsCount = 100;
        int value = 0;

        List<Integer> testList = new ArrayList<Integer>();

        for (int i=0; i < itemsCount; i++) {
            value++;
            testList.add(value);
        }

        List<Integer> testList2 = new ArrayList<Integer>();

        value = 0;
        for (int i=0; i < 20; i++) {
            value++;
            testList2.add(value);
        }

        List<Integer> testList3 = new ArrayList<Integer>();

        value = 80;
        for (int i=80; i < itemsCount; i++) {
            value++;
            testList3.add(value);
        }


        //simple work check
        Assert.assertEquals(CollectionUtils.subList(testList, 0, 20), testList2);

        //java.lang.IllegalArgumentException: fromIndex(300) > toIndex(100) check
        Assert.assertEquals(CollectionUtils.subList(testList, 300, 20), new ArrayList());

        //isEmpty() check
        Assert.assertEquals(CollectionUtils.subList(new ArrayList(),100, 200), new ArrayList());

        // offset < 0 || limit < 0 check
        Assert.assertEquals(CollectionUtils.subList(testList, -11, -1), new ArrayList());
        Assert.assertEquals(CollectionUtils.subList(testList, -1, 20), new ArrayList());
        Assert.assertEquals(CollectionUtils.subList(testList, 1, -20), new ArrayList());

        //testList.size() < limit check
        Assert.assertEquals(CollectionUtils.subList(testList, 80, 30), testList3);
    }
}

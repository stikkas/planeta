package ru.planeta.commons.math;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Tests for size utils helper class
 */
public class TestSizeUtils {

    @Test
    public void testResizeHeight() {

        SizeUtils.Size size = SizeUtils.resizeHeight(1280, 720, 480, 4);
        assertEquals(852, size.getWidth());
        assertEquals(480, size.getHeight());

        size = SizeUtils.resizeHeight(1280, 720, 240, 4);
        assertEquals(424, size.getWidth());
        assertEquals(240, size.getHeight());

        size = SizeUtils.resizeHeight(640, 480, 480, 4);
        assertEquals(640, size.getWidth());
        assertEquals(480, size.getHeight());
    }

    @Test
    public void testResizeWidth() {

        SizeUtils.Size size = SizeUtils.resizeWidth(640, 480, 320, 4);
        assertEquals(320, size.getWidth());
        assertEquals(240, size.getHeight());

        size = SizeUtils.resizeWidth(1280, 720, 640, 4);
        assertEquals(640, size.getWidth());
        assertEquals(360, size.getHeight());
    }

    @Test
    public void testFit() {
        SizeUtils.Size size = SizeUtils.fit(1600, 900, 640, 480);
        assertEquals(360, size.getHeight());
        assertEquals(640, size.getWidth());

        size = SizeUtils.fit(150, 50, 100, 100);
        assertEquals(100, size.getWidth());
        assertEquals(33, size.getHeight());

        size = SizeUtils.fit(50, 150, 100, 100);
        assertEquals(33, size.getWidth());
        assertEquals(100, size.getHeight());
    }
}

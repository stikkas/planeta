package ru.planeta.commons.external.oembed;

import org.junit.Test;
import ru.planeta.commons.external.oembed.provider.VideoType;

import static org.junit.Assert.*;

/**
 * Created with IntelliJ IDEA.
 * User: connanedoile
 * Date: 4/4/12
 * Time: 5:10 PM
 */
public class OembedTest {
    @Test
    public void testGetExternalVideoInfo() throws Exception {
        String urlYouTube = "http://www.youtube.com/watch?v=WRElyiSeH0I&feature=g-logo&context=G21eff32FOAAAAAAAAAA";
        assertEquals(VideoType.YOUTUBE, Oembed.getExternalVideoInfo(urlYouTube).getVideoType());

        String urlVimeo = "http://www.vimeo.com/37412824";
        assertEquals(VideoType.VIMEO, Oembed.getExternalVideoInfo(urlVimeo).getVideoType());

        assertNotEquals(VideoType.YOUTUBE, Oembed.getExternalVideoInfo(urlVimeo).getVideoType());
        assertNotEquals(VideoType.VIMEO, Oembed.getExternalVideoInfo(urlYouTube).getVideoType());
    }
}

package ru.planeta.commons.external;

import org.junit.Test;
import ru.planeta.commons.external.oembed.Oembed;
import ru.planeta.commons.external.oembed.OembedInfo;
import ru.planeta.commons.external.oembed.provider.VideoType;

import static org.junit.Assert.*;

/**
 * Tests for youtube utils
 */
public class TestYoutubeUtils {

    @Test
    public void testIsYoutubeUrl() {

        assertTrue(Oembed.getExternalVideoInfo("http://www.youtube.com/watch?v=WRElyiSeH0I&feature=g-logo&context=G21eff32FOAAAAAAAAAA").getVideoType() == VideoType.YOUTUBE);
        assertNull(Oembed.getExternalVideoInfo("http://www.youtube.com/"));
        assertEquals(Oembed.getExternalVideoInfo("http://youtu.be/WRElyiSeH0I").getVideoType(), VideoType.YOUTUBE);
    }

    @Test
    public void testExtractYoutubeId() {

        assertEquals("WRElyiSeH0I", Oembed.getExternalVideoInfo("http://www.youtube.com/watch?v=WRElyiSeH0I&feature=g-logo&context=G21eff32FOAAAAAAAAAA").getId());
        assertEquals("WRElyiSeH0I", Oembed.getExternalVideoInfo("http://youtu.be/WRElyiSeH0I").getId());
        assertNull(Oembed.getExternalVideoInfo("http://www.youtube.com/"));
    }

    @Test
    public void testExtractYoutubeInfo() {

        assertValidVideo(Oembed.getExternalVideoInfo("http://www.youtube.com/watch?v=WRElyiSeH0I&feature=g-logo&context=G21eff32FOAAAAAAAAAA"));
        assertValidVideo(Oembed.getExternalVideoInfo("http://youtu.be/WRElyiSeH0I"));
    }

    private static void assertValidVideo(OembedInfo youtubeInfo) {
        assertNotNull(youtubeInfo.getId());
        assertNotNull(youtubeInfo.getName());
        assertNotNull(youtubeInfo.getDescription());
        assertNotNull(youtubeInfo.getThumbnailUrl());
        assertTrue(youtubeInfo.getDuration() > 0);
    }
}

package ru.planeta.commons.generator;

import org.apache.log4j.Logger;
import org.junit.Ignore;
import org.junit.Test;

/**
 * User: sshendyapin
 * Date: 03.09.12
 * Time: 18:33
 */

public class TestTldGenerator {
    
    private static final Logger log = Logger.getLogger(TestTldGenerator.class);

    @Test
    @Ignore
    public void testGenerateMethods() {
        Class c = TldGenerator.class;
        String tld = TldGenerator.generateMethods(c);
        log.info(tld);
    }

}

package ru.planeta.commons.archive;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.junit.Ignore;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * @author ds.kolyshev
 * Date: 05.12.11
 */
public class TestZipUtils {
    
    private static final Logger log = Logger.getLogger(TestZipUtils.class);

    @Test
    public void zipUnzipDirectory() throws IOException {

        File testDirectory = null;
        File zippedFile = null;
        File unzipDirectory = null;

        try {
            testDirectory = new File("testZipUtils");
            testDirectory.mkdirs();

            File testFile1 = new File(getClass().getClassLoader().getResource("ziptest1.txt").getFile());
            File dstFile1 = new File(testDirectory, "dstziptest1.txt");

            File testFile2 = new File(getClass().getClassLoader().getResource("ziptest1.txt").getFile());
            File dstFile2 = new File(testDirectory, "dstziptest2.txt");

            //copy files to temp directory
            FileUtils.copyFile(testFile1, dstFile1);
            FileUtils.copyFile(testFile2, dstFile2);

            //zip folder
            zippedFile = new File(testDirectory.getAbsolutePath() + ".zip");
            ZipUtils.zip(testDirectory, zippedFile);
            assertTrue(zippedFile.exists());

            //unzip folder
            unzipDirectory = new File("testZipUtils_Unzip");
            unzipDirectory.mkdirs();
            ZipUtils.unzip(zippedFile, unzipDirectory);
            assertTrue(unzipDirectory.exists());

            //check files
            assertEquals(2, unzipDirectory.listFiles().length);
            File unzippedFile1 = new File(unzipDirectory, dstFile1.getName());
            assertTrue(unzippedFile1.exists());
            assertEquals(dstFile1.length(), unzippedFile1.length());

            File unzippedFile2 = new File(unzipDirectory, dstFile2.getName());
            assertTrue(unzippedFile2.exists());
            assertEquals(dstFile2.length(), unzippedFile2.length());
        } finally {
            //clear
            if (testDirectory != null && testDirectory.exists()) {
                FileUtils.deleteDirectory(testDirectory);
            }

            if (zippedFile != null && zippedFile.exists()) {
                FileUtils.deleteQuietly(zippedFile);
            }

            if (unzipDirectory != null && unzipDirectory.exists()) {
                FileUtils.deleteDirectory(unzipDirectory);
            }
        }
    }

    @Test
    @Ignore
    public void testUnzipStream() throws IOException {
        URL url = new URL("http://ipgeobase.ru/files/db/Main/geo_files.zip");
        InputStream inputStream = url.openStream();
        InputStream  zipArchiveInputStream = ZipUtils.unzipFile(inputStream, "cities.txt");

        InputStreamReader reader = new InputStreamReader(zipArchiveInputStream, "windows-1251");
        StringBuffer buffer = new StringBuffer();
        int ch;
        while ((ch = reader.read()) > -1) {
            buffer.append((char) ch);
        }
        reader.close();
        log.info(buffer.toString().substring(0, 123));
    }
}

package ru.planeta.commons.text;

import org.junit.Assert;
import org.junit.Test;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

/**
 * User: m.shulepov
 * Date: 03.04.12
 * Time: 14:35
 */
public class TestStemmer {

    @Test
    public void testStem() throws UnsupportedEncodingException {
        Stemmer stemmer = new Stemmer();

        // Программирование
        String originalStr = URLDecoder.decode("%D0%9F%D1%80%D0%BE%D0%B3%D1%80%D0%B0%D0%BC%D0%BC%D0%B8%D1%80%D0%BE%D0%B2%D0%B0%D0%BD%D0%B8%D0%B5", "utf-8");
        // программирован
        String expectedStr = URLDecoder.decode("%D0%BF%D1%80%D0%BE%D0%B3%D1%80%D0%B0%D0%BC%D0%BC%D0%B8%D1%80%D0%BE%D0%B2%D0%B0%D0%BD", "utf-8");

        Assert.assertEquals(expectedStr, stemmer.stem(originalStr));
    }
}

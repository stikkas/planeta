package ru.planeta.commons.lang;

import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * @author: ds.kolyshev
 * Date: 21.01.13
 */
public class TestStringUtils {

	private static final Logger log = Logger.getLogger(TestStringUtils.class);

    @Test
    public void testParseFloat() {
        String rusFloat = "33,4";
        String engFloat = "33.4";
        Float number = new Float(33.4);
        Float calculated = StringUtils.parseFloat(engFloat);
        assertEquals(number, calculated);
        calculated = StringUtils.parseFloat(rusFloat);
        assertEquals(number, calculated);
    }

    @Test
    public void testCalcWordCaseNumber() {
        int count = 24;
        int caseNumber = StringUtils.calcWordCaseNumber(count);
        assertEquals(2, caseNumber);
    }

    @Test
    public void testStrip() {
        String test = "watching tv (at home)";
        test = test.replaceAll("(?s)(.*?)", "");

        test = org.apache.commons.lang3.StringUtils.substringBetween(test, "(", ")");
        System.out.print(test);
    }

    @Test
    public void testAscii() throws Exception {
        String utf = StringUtils.nativeToAsciiReverse("\\xc3\\u042d\\u0442\\u043e\n\n \\u043f\\u043e\\u043b\\u0435\n\n \\u043d\\u0435\\u043e\\u0431\\u0445\\u043e\\u0434\\u0438\\u043c\\u043e");
        assertNotNull(utf);

    }

    @Test
    public void testStripLinksRegexp() {
        String stringWithLinksHtml = "<p><a href=\"http://children-happy.ru/\">http://<nobr>children-happy</nobr>.ru/</a></p>";
        String test = "<p>http://<nobr>children-happy</nobr>.ru/</p>";
        assertEquals(test, StringUtils.stripLinksFromHtml(stringWithLinksHtml));
    }

    @Test
    public void testSplitByCommas() {
        List<String> liststr = StringUtils.splitByCommas("aaa@bbb.cc, ddd@eee.ff, ggg@hhh.jj");
        Assert.assertEquals(3, liststr.size());
        Assert.assertEquals("aaa@bbb.cc", liststr.get(0));
        Assert.assertEquals("ddd@eee.ff", liststr.get(1));
        Assert.assertEquals("ggg@hhh.jj", liststr.get(2));

        liststr = StringUtils.splitByCommas("aaa@bbb.cc");
        Assert.assertEquals(1, liststr.size());
        Assert.assertEquals("aaa@bbb.cc", liststr.get(0));
    }

}

package ru.planeta.commons.generator;

import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Ignore;
import org.junit.Test;

import java.util.Random;
import java.util.Set;
import java.util.TreeSet;

/**
 * User: a.savanovich
 * Date: 09.10.13
 * Time: 16:59
 */
@Ignore
public class TestRandomGenerate {
    @Test
    public void testGenerate() {
        Set<Integer> ids = new TreeSet<Integer>();
        Random randomGenerator = new Random();
        while(ids.size() < 200) {
            int randomInt = randomGenerator.nextInt(9999);
            ids.add(randomInt);
        }
        for (int i : ids) {
            System.out.println(String.format("%04d", i));
        }
    }

    @Test
    public void testGenerateLetters() {
        Set<String> ids = new TreeSet<String>();
        while(ids.size() < 400) {
            String randomInt = RandomStringUtils.randomAlphanumeric(8).toUpperCase();
            ids.add(randomInt);
        }
        for (String i : ids) {
            System.out.println(i);
        }
    }

    @Test
    @Ignore
    public void testRandomBoolean() {
        Random rnd = new Random();

        int globalCyclesCount = 15;
        int cyclesCount = 50000;
        for (int i=0; i< globalCyclesCount; i++) {
            int trueCount = 0;
            int falseCount = 0;

            for (int j = 0; j < cyclesCount; j++) {
                boolean chance = rnd.nextBoolean();
                if (chance) {
                    trueCount++;
                } else {
                    falseCount++;
                }
            }
            System.out.println("true  " + trueCount + " " + String.format( "%.2f", (100.0/cyclesCount)*trueCount ) + "%");
            System.out.println("false " + falseCount + " " + String.format( "%.2f", (100.0/cyclesCount)*falseCount) + "%");
            System.out.println();
        }
    }
}

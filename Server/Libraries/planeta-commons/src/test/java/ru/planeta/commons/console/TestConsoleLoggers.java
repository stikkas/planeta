package ru.planeta.commons.console;

import org.apache.log4j.Logger;
import org.junit.Test;

/**
 * @author: ds.kolyshev
 * Date: 13.02.13
 */
public class TestConsoleLoggers {

	private static Logger log = Logger.getLogger(TestConsoleLoggers.class);

	@Test
	public void test() {
		log.trace("Trace must be written to stdout");
		log.debug("Debug must be written to stdout");
		log.info("Info must be written to stdout");

		log.warn("Warn must be written only to stderr");
		log.error("Error must be written only to stderr");
	}
}

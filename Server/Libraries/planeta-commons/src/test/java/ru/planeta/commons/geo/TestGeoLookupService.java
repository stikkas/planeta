package ru.planeta.commons.geo;

import org.apache.log4j.Logger;
import org.junit.Ignore;
import org.junit.Test;

/**
 * User: a.savanovich
 * Date: 12.04.12
 * Time: 18:51
 */
@Ignore
public class TestGeoLookupService {

    private static final Logger log = Logger.getLogger(TestGeoLookupService.class);

    @Test
    public void testGetCountryCode() throws Exception {
        GeoLookupService geoLookupService = new GeoLookupService();
        String ip = "92.241.173.132";
        String str = geoLookupService.getCountryCode(ip);
        log.info("countryCode: " + str);
        long id = geoLookupService.getLocationId(ip);
        log.info("location: " + id);
    }
}

package ru.planeta.commons.text;


import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

import static org.junit.Assert.*;

/**
 * @author ds.kolyshev
 * Date: 24.10.11
 */
public class TestTranslitConverter {
    @Test
    public void testTranlitString() throws UnsupportedEncodingException {

        // Привет, Мир. Это Длинная Строка с Разными символами русского алфавита. 123 EDFRww
        String testString = URLDecoder.decode("%D0%9F%D1%80%D0%B8%D0%B2%D0%B5%D1%82%2C+%D0%9C%D0%B8%D1%80.+%D0%AD%D1%82%D0%BE+%D0%94%D0%BB%D0%B8%D0%BD%D0%BD%D0%B0%D1%8F+%D0%A1%D1%82%D1%80%D0%BE%D0%BA%D0%B0+%D1%81+%D0%A0%D0%B0%D0%B7%D0%BD%D1%8B%D0%BC%D0%B8+%D1%81%D0%B8%D0%BC%D0%B2%D0%BE%D0%BB%D0%B0%D0%BC%D0%B8+%D1%80%D1%83%D1%81%D1%81%D0%BA%D0%BE%D0%B3%D0%BE+%D0%B0%D0%BB%D1%84%D0%B0%D0%B2%D0%B8%D1%82%D0%B0.+123+EDFRww", "utf-8");
        String translitString = TranslitConverter.toTranslit(testString);
        String expectedTranslitString = "Privet, Mir. Eto Dlinnaya Stroka s Raznymi simvolami russkogo alfavita. 123 EDFRww";

        assertEquals(expectedTranslitString, translitString);
    }
}

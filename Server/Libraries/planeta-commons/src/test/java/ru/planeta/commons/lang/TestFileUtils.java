package ru.planeta.commons.lang;

import org.junit.Assert;
import org.junit.Test;

import java.io.File;

/**
 * @author p.vyazankin
 * @since 2/28/13 9:29 PM
 */
public class TestFileUtils {

    @Test
    public void fileContentEquals() throws Exception {
        File[] files = new File[3];
        files[0] = FileUtils.getFileFromClasspath("f0.txt");
        files[1] = FileUtils.getFileFromClasspath("f1.txt");
        files[2] = FileUtils.getFileFromClasspath("f2.txt");

        Assert.assertTrue(org.apache.commons.io.FileUtils.contentEquals(files[0], files[1]));
        Assert.assertFalse(org.apache.commons.io.FileUtils.contentEquals(files[0], files[2]));
    }

    @Test
    public void testLongFilename() throws Exception {
        String longFilename = "%d0%9e%d1%87%d0%b5%d0%bd%d1%8c+%d0%b4%d0%bb%d0%b8%d0%bd%d0%bd%d0%be%d0%b5+%d0%bd%d0%b0%d0%b7%d0%b2%d0%b0%d0%bd%d0%b8%d0%b5+%d0%94%d0%b0+%d0%b5%d1%89%d0%b5+%d0%b8+%d1%82%d0%be%d1%87%d0%ba%d0%b0+%d0%bf%d0%be%d1%81%d0%b5%d1%80%d0%b5%d0%b4%d0%b8%d0%bd%d011.xls";
        String strippedFilename = FileUtils.stripLongFilename(longFilename);
        Assert.assertEquals(FileUtils.MAX_FILE_LENGTH, strippedFilename.length());
    }
}

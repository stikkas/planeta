package ru.planeta.commons.web;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import ru.planeta.commons.onlinekassa.*;

import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * User: m.shulepov
 * Date: 17.04.12
 * Time: 18:09
 */
public class TestWebUtils {

    @Test
    @Ignore
    public void testHttpsHeadRequest() {
        final String httpJpg = "http://forumlocal.ru/user/upload/file570554.jpg";
        final String httpsJpg = "https://forumlocal.ru/user/upload/file570554.jpg";

        Map<String,String> headers = WebUtils.headRequest(httpJpg, 1000, 1000);
        Assert.assertNotNull(headers);
        String contentType = headers.get("Content-Type");
        if (!StringUtils.isEmpty(contentType)) {
            Assert.assertTrue(contentType.startsWith("image/"));
        }

        headers = WebUtils.headRequest(httpsJpg,1000,1000);
        Assert.assertNotNull(headers);
        contentType = headers.get("Content-Type");
        if (!StringUtils.isEmpty(contentType)) {
            Assert.assertTrue(contentType.startsWith("image/"));
        }

        final String yandexUrl = "https://ya.ru";
        headers = WebUtils.headRequest(yandexUrl,1000,1000);
        Assert.assertNotNull(headers);
    }

    @Test
    public void testAppendHttpIfNecessary() {
        String httpUrl ="http://planeta.ru";
        String httpsUrl ="https://planeta.ru";

        String url = httpUrl;
        Assert.assertEquals(httpUrl, WebUtils.appendProtocolIfNecessary(url, false));

        url = "planeta.ru";
        Assert.assertEquals(httpUrl, WebUtils.appendProtocolIfNecessary(url, false));

        url = "planeta.ru";
        Assert.assertEquals(httpsUrl, WebUtils.appendProtocolIfNecessary(url, true));

        url = "http://planeta.ru";
        Assert.assertEquals(httpsUrl, WebUtils.appendProtocolIfNecessary(url, true));

        url = "https://planeta.ru";
        Assert.assertEquals(httpUrl, WebUtils.appendProtocolIfNecessary(url, false));
    }


    @Test
    @Ignore
    public void testExtractParams() throws UnsupportedEncodingException {
        String[] urls = {
                "http://planeta.ru/campaigns?widget=123&som=val",
                "https://planeta.ru/campaigns/share/123",
                "https://planeta.ru/campaigns/share/123?qweqwe=123123&empty=&nonempty=lkjlk",
                "/campaigns/share/123?qweqwe=123123",
                "//planeta.ru/campaigns/share/123?qweqwe=123123",
                "qweqwe=123123&empty=&nonempty=lkjlk"};
        for (String s : urls) {
            System.out.println(WebUtils.getQueryParams(s, "utf-8"));
        }
    }

    @Test
    @Ignore
    public void testDownloadStringWithTimeout() {
        String s1 = WebUtils.downloadStringWithTimeout("http://planeta.ru", 30 * 1000);
        String s2 = WebUtils.downloadStringWithTimeout("http://planeta.ru", 10); // ConnectTimeoutException expected
    }

    @Ignore
    @Test
    public void testCloudPaymentsOnlineKassa() throws JsonProcessingException, UnsupportedEncodingException {
        List<CustomerReceiptItem> items = new ArrayList<>();

        CustomerReceiptItem item01 = new CustomerReceiptItem();
        item01.setLabel("Товар 1");
        item01.setPrice(new BigDecimal(100.0));
        item01.setQuantity(1);
        item01.setAmount(item01.getPrice().multiply(new BigDecimal(item01.getQuantity())));
        item01.setVat(null);
        items.add(item01);

//        CustomerReceiptItem item02 = new CustomerReceiptItem();
//        item02.setLabel("Товар 2");
//        item02.setPrice(new BigDecimal(200.0));
//        item02.setQuantity(2);
//        item02.setAmount(item01.getPrice().multiply(new BigDecimal(item01.getQuantity())));
//        item02.setVat(null);
//        items.add(item02);

        CustomerReceipt customerReceipt = new CustomerReceipt();
        customerReceipt.setItems(items);
        customerReceipt.setTaxationSystem(TaxationSystem.LIGHT_DOHOD_RASHOD);
        customerReceipt.setEmail("michail.michail@gmail.com");

        Receipt receipt = new Receipt();
        receipt.setInn("7722724252");
        receipt.setInvoiceId("100500");
        receipt.setAccountId("191");
        receipt.setType(ReceiptType.INCOME);
        receipt.setCustomerReceipt(customerReceipt);


        ObjectMapper objectMapper = new ObjectMapper();
        String postData = objectMapper.writeValueAsString(receipt);
        System.out.println("postData");
        System.out.println(postData);
        System.out.println("------------------");

        String publicId = "pk_5b44589c6baf4b6f713395d8b9947";
        String secretKey = "c945894af73fa6dae8847c966ed8271b";
        String header = publicId + ":" + secretKey;

        String codedHeader = Base64.encodeBase64String(header.getBytes());
        System.out.println("codedHeader: " + codedHeader);
        

        String answer = WebUtils.uploadStringWithBasicHttpAauth("https://api.cloudpayments.ru/kkt/receipt", postData, "application/json", "utf-8", codedHeader);
        System.out.println("answer");
        System.out.println(answer);
        System.out.println("------------------");

        // successful response is {"Model":{"Id":"hm5LhNv"},"InnerResult":null,"Success":true,"Message":"Queued"}
    }
}

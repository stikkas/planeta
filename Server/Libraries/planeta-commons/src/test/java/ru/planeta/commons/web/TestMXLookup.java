package ru.planeta.commons.web;

import org.junit.Ignore;
import org.junit.Test;
import ru.planeta.commons.web.MxLookup.MXLookup;
import ru.planeta.commons.web.MxLookup.SMTPMXLookup;

import javax.naming.NamingException;

@Ignore
public class TestMXLookup {
    @Test
    public void testLookup() throws NamingException {
        int res = MXLookup.doLookup("mial.ru");
        System.out.println(res);

        res = MXLookup.doLookup("mail.ru");
        System.out.println(res);

    }


    @Test
    public void testSmtpValid2() {
        String[] testData = {
                "real@rgagnon.com",
                "you@acquisto.net",
                "fail.me@nowhere.spam", // Invalid domain name
                "arkham@bigmeanogre.net", // Invalid address
                "nosuchaddress@yahoo.com", // Failure of this method,
                "trafic_test2010@mail.ru",
                "trafic_test20221@mail.ru",
                "trafic_test2010@mial.ru",
                "asavanters@gmail.com",
                "asavanster@gmail.com",
                "vangog1406@yandex.ru"
        };

        for (String aTestData : testData) {
            System.out.println(aTestData + " is valid? " +
                    SMTPMXLookup.isAddressValid(aTestData));
        }
    }
}
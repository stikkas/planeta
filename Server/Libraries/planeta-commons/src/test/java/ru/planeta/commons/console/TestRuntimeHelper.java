package ru.planeta.commons.console;

import org.apache.commons.lang3.SystemUtils;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

import static org.junit.Assert.assertEquals;

/**
 * Runtime helper test class
 *
 * @author ds.kolyshev
 *         Date: 15.05.12
 */
public class TestRuntimeHelper {

    @Test
    public void test() throws IOException, TimeoutException, InterruptedException {
        if (SystemUtils.IS_OS_WINDOWS) {
            return;
        }

        String command = "whoami";
        int exitValue = RuntimeHelper.executeCommandLine(command);
        assertEquals(0, exitValue);
    }

    private OutLineHandler handler = new OutLineHandler() {
        @Override
        public void handleLine(String line) {
            Assert.assertTrue(line.startsWith(System.getProperty("user.home")));
        }
    };

    @Test
    public void testExec() throws IOException {
        if (SystemUtils.IS_OS_WINDOWS) {
            return;
        }

        Assert.assertTrue(RuntimeHelper.isNotWindows());
        int exit = RuntimeHelper.exec(new String[]{"pwd"}, null);
        Assert.assertEquals(0, exit);

        exit = RuntimeHelper.exec(new String[]{"cd", "pwd"}, handler);
        Assert.assertEquals(0, exit);
    }
}

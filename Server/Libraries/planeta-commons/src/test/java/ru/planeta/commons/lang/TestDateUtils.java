package ru.planeta.commons.lang;

import org.apache.commons.lang3.math.NumberUtils;
import org.junit.Test;

import java.util.Calendar;
import java.util.Date;

import static org.junit.Assert.*;

/**
 * Tests for date utils
 */
public class TestDateUtils {

    @Test
    public void testTruncate() {

        Date testDate = DateUtils.parseDate("09.02.2013");
        Date truncated = DateUtils.truncate(testDate, Calendar.WEEK_OF_YEAR);

        assertNotNull(truncated);
        assertEquals(DateUtils.parseDate("04.02.2013"), truncated);
    }

    @Test
    public void testNumberUtils() {
        int duration = NumberUtils.toInt("3:15", 0);
        assertEquals(duration, 0);
        duration = NumberUtils.toInt("05", 0);
        assertEquals(duration, 5);
    }

    @Test
    public void testDateRange() {
        final Date yesterday = org.apache.commons.lang3.time.DateUtils.addDays(new Date(), -1);
        final Date tomorrow = org.apache.commons.lang3.time.DateUtils.addDays(new Date(), 1);

        assertFalse(DateUtils.isBeforeNow(null));
        assertFalse(DateUtils.isBeforeNow(tomorrow));
        assertTrue(DateUtils.isBeforeNow(yesterday));

        assertFalse(DateUtils.isAfterNow(null));
        assertFalse(DateUtils.isAfterNow(yesterday));
        assertTrue(DateUtils.isAfterNow(tomorrow));
    }
}

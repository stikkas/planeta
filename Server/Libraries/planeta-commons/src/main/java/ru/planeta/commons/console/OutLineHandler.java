package ru.planeta.commons.console;

public abstract class OutLineHandler {
        public abstract void handleLine(String line);
}
package ru.planeta.commons.external.oembed.provider;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;
import ru.planeta.commons.external.oembed.OembedInfo;

import java.io.IOException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.text.MessageFormat;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created with IntelliJ IDEA.
 * User: connanedoile
 * Date: 4/4/12
 * Time: 4:49 PM
 */
// TODO:  The XML should include <yt:noembed/> if the video isn't embeddable.
// TODO: Check restricted access videos (some scheduler job?)
public class Youtube implements MediaService {

    private final static Logger log = Logger.getLogger(Youtube.class);
    private static final String IFRAME_HTML = "<iframe src=''https://www.youtube.com/embed/{0}?autoplay=1&wmode=opaque'' frameborder=''0'' allowfullscreen></iframe>";
    protected final static Pattern YOUTUBE_PATTERN = Pattern.compile(".*(youtube.com/.*v[=/]|y(2|out)u.be/)([^?&#]+).*$", Pattern.CASE_INSENSITIVE);

    protected final static String API_URL = "https://www.googleapis.com/youtube/v3/videos?id={id}&key=AIzaSyCa3fe4S2zmsI0-GQiT4EnezJ8CUpEJ0_E&part=snippet,contentDetails";

    @Override
    public OembedInfo getInfo(final String url) {
        try {
            String id = extractYoutubeId(url);
            if (StringUtils.isEmpty(id)) {
                return null;
            }

            return getOembedInfoById(id);
        } catch (Exception ex) {
            log.warn("Exception while parsing ", ex);
            return null;
        }
    }

    public static OembedInfo getOembedInfoById(String id) throws IOException, JSONException {
        String videoInfo = getYoutubeInfo(id);
        if (StringUtils.isEmpty(videoInfo)) {
            return null;
        }
        JSONObject obj = new JSONObject(videoInfo);
        JSONObject item = obj.getJSONArray("items").getJSONObject(0);
        JSONObject snippet = item.getJSONObject("snippet");


        OembedInfo youtubeInfo = new OembedInfo();
        youtubeInfo.setId(id);
        youtubeInfo.setNaturalUrl(getNaturalUrlById(id));
        youtubeInfo.setName(snippet.getString("title"));
        youtubeInfo.setDescription(snippet.getString("description"));

        String thumbnailUrl = getThumbnail(snippet);
        if (thumbnailUrl == null) {
            return null;
        }
        youtubeInfo.setThumbnailUrl(thumbnailUrl);
        youtubeInfo.setDuration(stringToSeconds(item.getJSONObject("contentDetails").getString("duration")));
        youtubeInfo.setVideoType(VideoType.YOUTUBE);

        youtubeInfo.setHtml(MessageFormat.format(IFRAME_HTML, id));

        return youtubeInfo;
    }

    private static int stringToSeconds(String s) {
        try {
            return org.joda.time.format.ISOPeriodFormat.standard().parsePeriod(s).toStandardSeconds().getSeconds();
        } catch (Exception e) {
            return 0;
        }
    }

    private static String[] THUMBNAILS = {"maxres", "medium", "standard", "high", "default"};

    public static String getThumbnail(JSONObject obj) {

        try {
            if (obj.has("thumbnails")) {
                JSONObject thumbnails = obj.getJSONObject("thumbnails");
                for (String thumbnailName : THUMBNAILS) {
                    if (thumbnails.has(thumbnailName)) {
                        return thumbnails.getJSONObject(thumbnailName).getString("url");
                    }
                }
                String[] names = JSONObject.getNames(thumbnails);
                if (names.length > 0) {
                    return thumbnails.getJSONObject(names[0]).getString("url");
                }
            }

        } catch (JSONException ignored) {
        }
        return null;
    }

    private static String getYoutubeInfo(String id) throws IOException {
        return IOUtils.toString(new URL(API_URL.replace("{id}", id)), StandardCharsets.UTF_8);
    }

    /**
     * Checks if the specified url is youtube video url
     */
    @Override
    public boolean isServiceUrl(String url) {
        return StringUtils.isNotEmpty(url) && YOUTUBE_PATTERN.matcher(url).find();
    }

    /**
     * Extracts youtube video identifier from the specified url.
     * If wrong url (cannot extract or non-youtube) -- returns null.
     */
    private static String extractYoutubeId(String url) {
        try {
            Matcher matcher = YOUTUBE_PATTERN.matcher(url);
            if (!matcher.find()) {
                return null;
            }

            return matcher.group(3);
        } catch (Exception ex) {
            log.warn("Cannot extract youtube id from: " + url, ex);
        }

        return null;
    }

    @Override
    public String getNaturalUrl(String url) {
        return getNaturalUrlById(extractYoutubeId(url));
    }

    private static String getNaturalUrlById(String id) {
        return "https://www.youtube.com/v/" + id;
    }
}

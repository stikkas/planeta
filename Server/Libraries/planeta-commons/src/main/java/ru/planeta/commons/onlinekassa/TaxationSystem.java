package ru.planeta.commons.onlinekassa;

import java.util.HashMap;
import java.util.Map;

/**
 * Варианты системы налогообложения:
 * 0 — Общая система налогообложения
 * 1 — Упрощенная система налогообложения (Доход)
 * 2 — Упрощенная система налогообложения (Доход минус Расход)
 * 3 — Единый налог на вмененный доход
 * 4 — Единый сельскохозяйственный налог
 * 5 — Патентная система налогообложения
 * Указанная система налогообложения должна совпадать с одним из вариантов, зарегистрированных в ККТ.
 *
 * https://cloudpayments.ru/docs/api/kassa
 */
public enum TaxationSystem {
    OBSCHAYA_SISTEMA_NALOGOOBLAZHENIYA(0),
    LIGHT_DOHOD(1),
    LIGHT_DOHOD_RASHOD(2),
    EDINYJ_NALOG_NA_VREMENNYJ_DOHOD(3),
    EDINYJ_SELSKOHOZYAJSTVENNYJ_NALOG(4),
    PATENTNAYA_SISTEMA_NALOGOOBLOZHENIYA(5);

    private final int code;

    private TaxationSystem(int code) {
        this.code = code;
    }

    public int getCode() {
        return code;
    }

    private static Map<Integer, TaxationSystem> lookup = new HashMap<>();

    static {
        for (TaxationSystem s : values()) {
            lookup.put(s.getCode(), s);
        }
    }

    public static TaxationSystem getByValue(int code) {
        return lookup.get(code);
    }

    public static TaxationSystem fromString(String json) {
        TaxationSystem type = OBSCHAYA_SISTEMA_NALOGOOBLAZHENIYA;
        for (TaxationSystem t : values()) {
            if (t.name().equals(json)) {
                type = t;
                break;
            }
        }
        return type;
    }
}

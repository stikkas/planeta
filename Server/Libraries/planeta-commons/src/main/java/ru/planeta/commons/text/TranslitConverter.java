package ru.planeta.commons.text;

/**
 * Cyrillic symbols converter
 *
 * @author ds.kolyshev
 * Date: 24.10.11
 */
public class TranslitConverter {
    private static final String[] charTable = new String[81];

    private static final char START_CHAR = '\u0401';

    static {
        charTable['\u0410' - START_CHAR] = "A";
        charTable['\u0411' - START_CHAR] = "B";
        charTable['\u0412' - START_CHAR] = "V";
        charTable['\u0413' - START_CHAR] = "G";
        charTable['\u0414' - START_CHAR] = "D";
        charTable['\u0415' - START_CHAR] = "E";
        charTable['\u0401' - START_CHAR] = "E";
        charTable['\u0416' - START_CHAR] = "ZH";
        charTable['\u0417' - START_CHAR] = "Z";
        charTable['\u0418' - START_CHAR] = "I";
        charTable['\u0419' - START_CHAR] = "I";
        charTable['\u041a' - START_CHAR] = "K";
        charTable['\u041b' - START_CHAR] = "L";
        charTable['\u041c' - START_CHAR] = "M";
        charTable['\u041d' - START_CHAR] = "N";
        charTable['\u041e' - START_CHAR] = "O";
        charTable['\u041f' - START_CHAR] = "P";
        charTable['\u0420' - START_CHAR] = "R";
        charTable['\u0421' - START_CHAR] = "S";
        charTable['\u0422' - START_CHAR] = "T";
        charTable['\u0423' - START_CHAR] = "U";
        charTable['\u0424' - START_CHAR] = "F";
        charTable['\u0425' - START_CHAR] = "H";
        charTable['\u0426' - START_CHAR] = "C";
        charTable['\u0427' - START_CHAR] = "CH";
        charTable['\u0428' - START_CHAR] = "SH";
        charTable['\u0429' - START_CHAR] = "SH";
        charTable['\u042a' - START_CHAR] = "'";
        charTable['\u042b' - START_CHAR] = "Y";
        charTable['\u042c' - START_CHAR] = "'";
        charTable['\u042d' - START_CHAR] = "E";
        charTable['\u042e' - START_CHAR] = "U";
        charTable['\u042f' - START_CHAR] = "YA";

        for (int i = 0; i < charTable.length; i++) {
            char idx = (char) ((char) i + START_CHAR);
            char lower = new String(new char[]{idx}).toLowerCase().charAt(0);
            if (charTable[i] != null) {
                charTable[lower - START_CHAR] = charTable[i].toLowerCase();
            }
        }
    }

    static final String ENGLISH_KEYBOARD_LAYOUT = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ[]{};':\",./<>?@#$^&`~";
    static final String RUSSIAN_KEYBOARD_LAYOUT = "фисвуапршолдьтщзйкыегмцчняФИСВУАПРШОЛДЬТЩЗЙКЫЕГМЦЧНЯхъХЪжэЖЭбю.БЮ,\"№;:?ёЁ";

    /**
     * Replaces cyrillic symbols with their translit
     *
     */
    public static String toTranslit(String text) {
        if (text == null) return null;
        char charBuffer[] = text.toCharArray();
        StringBuilder sb = new StringBuilder(text.length());
        for (char symbol : charBuffer) {
            int i = symbol - START_CHAR;
            if (i >= 0 && i < charTable.length) {
                String replace = charTable[i];
                sb.append(replace == null ? symbol : replace);
            } else {
                sb.append(symbol);
            }
        }

        return sb.toString();
    }

    public static String changeEngToRusLayout(String text) {
        if (text == null) return null;
        char charBuffer[] = text.toCharArray();
        StringBuilder sb = new StringBuilder(text.length());

        for (char symbol : charBuffer) {
            int index = ENGLISH_KEYBOARD_LAYOUT.indexOf(symbol);
            if (index >= 0 && index < RUSSIAN_KEYBOARD_LAYOUT.length()) {
                char replace = RUSSIAN_KEYBOARD_LAYOUT.charAt(index);
                sb.append(replace);
            } else {
                return null;
            }
        }
        return sb.toString();
    }


    public static String changeRusToEngLayout(String text) {
        if (text == null) return null;
        char charBuffer[] = text.toCharArray();
        StringBuilder sb = new StringBuilder(text.length());

        for (char symbol : charBuffer) {
            int index = RUSSIAN_KEYBOARD_LAYOUT.indexOf(symbol);
            if (index >= 0 && index < ENGLISH_KEYBOARD_LAYOUT.length()) {
                char replace = ENGLISH_KEYBOARD_LAYOUT.charAt(index);
                sb.append(replace);
            } else {
                return null;
            }
        }
        return sb.toString();
    }
}

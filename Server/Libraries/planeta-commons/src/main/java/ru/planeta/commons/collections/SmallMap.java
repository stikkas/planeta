package ru.planeta.commons.collections;

import java.util.AbstractMap;
import java.util.AbstractSet;
import java.util.Iterator;
import java.util.Set;

/**
 * Simple map implementation optimized for
 * small memory usage.
 */
public class SmallMap extends AbstractMap {

    private Entry entry = null;

    @Override
    public void clear() {
        entry = null;
    }

    @Override
    public boolean isEmpty() {
        return entry == null;
    }

    @Override
    public int size() {
        int r = 0;
        for (Entry e = entry; e != null; e = e.next) r++;
        return r;
    }

    @Override
    public boolean containsKey(Object key) {
        for (Entry e = entry; e != null; e = e.next) {
            if (e.key.equals(key)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean containsValue(Object value) {
        for (Entry e = entry; e != null; e = e.next) {
            if (e.value == null) {
                if (value == null) return true;
            } else if (e.value.equals(value)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public Object get(Object key) {
        for (Entry e = entry; e != null; e = e.next) {
            if (e.key.equals(key)) {
                return e.value;
            }
        }
        return null;
    }

    @Override
    public Object put(Object key, Object value) {
        for (Entry e = entry; e != null; e = e.next) {
            if (e.key.equals(key)) {
                Object r = e.value;
                e.value = value;
                return r;
            }
        }
        entry = new Entry(key, value, entry);
        return null;
    }

    @Override
    public Object remove(Object key) {
        if (entry != null) {
            if (entry.key.equals(key)) {
                Object r = entry.value;
                entry = entry.next;
                return r;
            }
            for (Entry e = entry; e.next != null; e = e.next) {
                if (key.equals(e.next.key)) {
                    Object r = e.next.value;
                    e.next = e.next.next;
                    return r;
                }
            }
        }
        return null;
    }

    @Override
    public Set entrySet() {
        return new EntrySet();
    }

    class EntrySet extends AbstractSet {

        @Override
        public Iterator iterator() {
            return new Iterator() {

                Entry last = null;
                Entry e = entry;

                @Override
                public boolean hasNext() {
                    return e != null;
                }

                @Override
                public Object next() {
                    last = e;
                    e = e.next;
                    return last;
                }

                @Override
                public void remove() {
                    if (last == null) throw new IllegalStateException();
                    SmallMap.this.remove(last.key);
                }
            };
        }

        @Override
        public int size() {
            return SmallMap.this.size();
        }
    }

    /**
     * Simple entry implementation
     */
    private static class Entry implements java.util.Map.Entry {
        final Object key;
        Object value;
        Entry next;

        Entry(Object key, Object value, Entry next) {
            if (key == null) throw new NullPointerException();
            this.key = key;
            this.value = value;
            this.next = next;
        }

        @Override
        public Object getKey() {
            return key;
        }

        @Override
        public Object getValue() {
            return value;
        }

        @Override
        public Object setValue(Object value) {
            Object r = this.value;
            this.value = value;
            return r;
        }

        @Override
        public int hashCode() {
            return (key == null ? 0 : key.hashCode()) ^
                    (value == null ? 0 : value.hashCode());
        }
    }

}

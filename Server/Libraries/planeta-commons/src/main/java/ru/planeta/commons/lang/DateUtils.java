package ru.planeta.commons.lang;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Helper utils for Dates
 *
 * @author ds.kolyshev
 * Date: 05.09.11
 */
public final class DateUtils {

    private static final String[] DATE_FORMATS = {
            "EEE MMM dd HH:mm:ss ZZZZZ yyyy",
            "dd MM yyyy",
            "dd.MM.yyyy",
            "M/d/y",
            "M-d-y"
    };

    /**
     * Returns number of seconds from 01.01.1970
     *
     * @param date
     * @return
     */
    public static long getUnixTime(Date date) {
        return date.getTime();
    }

    /**
     * Formats duration in human readable format (like hh:mi)
     *
     * @param milliseconds
     * @return
     */
    public static String humanDuration(int milliseconds) {
        int totalSec = milliseconds / 1000;
        int hours = (totalSec / 3600) % 24;
        int minutes = (totalSec / 60) % 60;
        int seconds = totalSec % 60;

        String duration = "";
        if (hours > 0) {
            duration += hours + ":";
        }
        duration += ((hours > 0 && minutes < 10) ? "0" : "") + minutes;
        duration += ':' + (seconds < 10 ? "0" : "") + seconds;
        return duration;
    }

    /**
     * Parses date string
     *
     * @param dateStr
     * @return
     */
    public static Date parseDate(String dateStr) {
        try {
            return org.apache.commons.lang3.time.DateUtils.parseDate(dateStr, DATE_FORMATS);
        } catch (ParseException ex) {
            return null;
        }
    }


    /**
     * Returns a new Date with the hours, milliseconds, seconds and minutes set to 0
     *
     * @param date
     * @return
     */
    public static Date getStartOfDay(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.MILLISECOND, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        return calendar.getTime();
    }

    /**
     * Returns the last millisecond of specified date
     *
     * @param date Can be null
     * @return
     */
    public static Date getEndOfDate(Date date) {
        if (date == null) {
            return null;
        }
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.MILLISECOND, 999);
        calendar.set(Calendar.SECOND, 59);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.HOUR_OF_DAY, 23);
        return calendar.getTime();
    }

    /**
     * Extension for standard apache commons DateUtils.truncate method,
     * that supports truncating by Calendar.WEEK_OF_YEAR.
     *
     * @param date
     * @param field
     * @return
     */
    public static Date truncate(Date date, int field) {

        if (field == Calendar.WEEK_OF_YEAR) {
            Calendar calendar = Calendar.getInstance(new Locale("ru"));
            calendar.setTime(date);
            int year = calendar.get(Calendar.YEAR);
            int week = calendar.get(Calendar.WEEK_OF_YEAR);

            Calendar truncated = Calendar.getInstance(new Locale("ru"));
            truncated.clear();
            truncated.set(Calendar.YEAR, year);
            truncated.set(Calendar.WEEK_OF_YEAR, week);
            return truncated.getTime();
        }

        return org.apache.commons.lang3.time.DateUtils.truncate(date, field);
    }

    /**
     * Checks presented date is before now (<code>new Date()</code>).
     *
     * @param date tested date, may be <code>null</code>;
     * @return true if <i>date</i> is before now and not <code>null</code>, false otherwise.
     */
    public static boolean isBeforeNow(Date date) {
        return (date != null) && date.before(new Date());
    }

    /**
     * Checks presented date is after now (<code>new Date()</code>).
     *
     * @param date tested date, may be <code>null</code>;
     * @return true if <i>date</i> is after now and not <code>null</code>, false otherwise.
     */
    public static boolean isAfterNow(Date date) {
        return (date != null) && date.after(new Date());
    }

    public static Date monthAgo() {
        Calendar c = Calendar.getInstance();
        c.add(Calendar.MONTH, -1);
        return c.getTime();
    }

    public static Date weekAgo() {
        Calendar c = Calendar.getInstance();
        c.add(Calendar.DAY_OF_MONTH, -7);
        return c.getTime();
    }

    public static Date twoWeekAgo() {
        Calendar c = Calendar.getInstance();
        c.add(Calendar.DAY_OF_MONTH, -14);
        return c.getTime();
    }

    public static Date firstDayOfCurrentMonth() {
        Calendar c = Calendar.getInstance();
        c.add(Calendar.DAY_OF_MONTH, -1 * c.get(Calendar.DAY_OF_MONTH));
        return c.getTime();
    }

    public static Date firstDayOfPreviousMonth() {
        Calendar c = Calendar.getInstance();
        c.add(Calendar.MONTH, -1);
        c.add(Calendar.DAY_OF_MONTH, -1 * c.get(Calendar.DAY_OF_MONTH));
        return c.getTime();
    }

    private static final String ISO8601_2004 = "yyyy-MM-dd'T'HH:mm:ss.SZ";

    public static String formatToISO8601(Date date) {
        String result = null;
        if (date != null) {
            String s = new SimpleDateFormat(ISO8601_2004).format(date);
            result = new StringBuilder(s).insert(s.length() - 2, ':').toString();
        }

        return result;
    }

    private static boolean isCalendarBetween(Calendar checked, int field, int from, int to) {
        int day = checked.get(field);
        return (day >= from) && (day <= to);
    }

    public static boolean isHoursBetween(Calendar checked, int fromHour, int toHour) {
        return isCalendarBetween(checked, Calendar.HOUR_OF_DAY, fromHour, toHour);
    }

    public static boolean isDayBetween(Calendar checked, int fromDay, int toDay) {
        return isCalendarBetween(checked, Calendar.DAY_OF_WEEK, fromDay, toDay);
    }

    public static boolean isWeekday(Calendar calendar) {
        return isDayBetween(calendar, Calendar.MONDAY, Calendar.FRIDAY);
    }

    public static boolean isWeekend(Calendar calendar) {
        return isDayBetween(calendar, Calendar.SATURDAY, Calendar.SUNDAY);
    }

    public static boolean isToday(Calendar today, Calendar calendar) {
        return today.get(Calendar.YEAR) == calendar.get(Calendar.YEAR) &&
                today.get(Calendar.MONTH) == calendar.get(Calendar.MONTH) &&
                today.get(Calendar.DATE) == calendar.get(Calendar.DATE);
    }

    public static boolean isDayWeekToday(Calendar calendar, int day) {
        return calendar.get(Calendar.DAY_OF_WEEK) == day;
    }
}


package ru.planeta.commons.external.oembed.provider;

import ru.planeta.commons.external.oembed.OembedInfo;

/**
 * Created by IntelliJ IDEA.
 * User: Andrew
 * Date: 28.04.12
 * Time: 16:22
 */
public interface MediaService {
    OembedInfo getInfo(final String url);

    boolean isServiceUrl(String url);

    String getNaturalUrl(String url);
}

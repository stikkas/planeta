package ru.planeta.commons.web.typograf;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import ru.planeta.commons.web.WebUtils;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ScheduledExecutorService;

/**
 * Client for typografy service <a href="http://www.typograf.ru/">http://www.typograf.ru/</a>
 *
 * @author s.kalmykov
 * @see <a href="http://www.typograf.ru/webservice/about/">http://www.typograf.ru/webservice/about/</a>
 * @since 27.03.2014
 */
public class TypografAsyncWebClient extends AbstractAsyncWebClient<String, String> {

    private static final Logger LOGGER = Logger.getLogger(TypografAsyncWebClient.class);
    private final static String TYPOGRAF_URL = "http://www.typograf.ru/webservice/";
    private static final String defaultXmlPreferences = "<?xml version=\"1.0\" encoding=\"windows-1251\" ?>\n" +
            "<preferences>" +
            "<paragraph insert=\"0\"/>" +
            "<cmsNewLine valid=\"1\"/>" +
            "</preferences>";
    private static final int MAX_TYPOGRAF_TEXT_SIZE = 51200;

    /**
     * Blocking version of http request with default preferences
     *
     * @param textHtml text(html) to typografy
     * @return web-service result
     */
    public static String processOnceBlocking(String textHtml) throws Exception {
        return requestSafe(textHtml, defaultXmlPreferences);
    }


    private String xmlPreferences;
    private ProcessCallback<String> callback;
    private final ScheduledExecutorService executorService;

    public TypografAsyncWebClient(ProcessCallback<String> callback, ScheduledExecutorService executorService) {
        this(defaultXmlPreferences, callback, executorService);

    }

    public TypografAsyncWebClient(String xmlPreferences, ProcessCallback<String> callback, ScheduledExecutorService executorService) {
        setXmlPreferences(xmlPreferences);
        this.callback = callback;
        this.executorService = executorService;
    }

    public String getXmlPreferences() {
        return xmlPreferences;
    }

    /**
     * Set preferences for web-service
     *
     * @param xmlPreferences valid xml string
     * @see <a href="http://www.typograf.ru/webservice/about/">http://www.typograf.ru/webservice/about/</a>
     */
    public void setXmlPreferences(String xmlPreferences) {
        this.xmlPreferences = xmlPreferences;
    }

    /**
     * Set preferences for web-service from stored xml file
     *
     * @param xmlFile valid xml file
     * @see <a href="http://www.typograf.ru/webservice/about/">http://www.typograf.ru/webservice/about/</a>
     */
    public void setXmlPreferences(File xmlFile) {
        try {
            this.xmlPreferences = FileUtils.readFileToString(xmlFile);
        } catch (IOException e) {
            this.xmlPreferences = defaultXmlPreferences;
        }
    }

    // remove it with ApacheHttpClient or HttpURLConnection to use it without planeta-commons
    private static String request(String textHtml, String xmlPreferences) throws IOException {
        Map<String, String> params = new HashMap<String, String>(2);
        params.put("text", textHtml);
        params.put("chr", "UTF-8");
        if (xmlPreferences != null) {
            params.put("xml", xmlPreferences);
        }
        return WebUtils.uploadMapExternal(TYPOGRAF_URL, params);
    }

    private static String requestSafe(String textHtml, String xmlPreferences) throws Exception {
        if(textHtml.length() > MAX_TYPOGRAF_TEXT_SIZE)  {
            throw new Exception("Typograf cannot process text over 50 kB!");
        }
        String result = request(textHtml, xmlPreferences);
        if (StringUtils.isBlank(result)) {
            throw new Exception("Typograf returned null or blank value!");
        }
        return result;
    }


    @Override
    protected String syncProcess(String input) throws Exception {
        if (input.isEmpty()) {
            return "";
        }
        return requestSafe(input, xmlPreferences);
    }

    @Override
    protected ScheduledExecutorService getExecutorService() {
        return executorService;
    }

    @Override
    protected String defaultValue(String input) {
        return input;
    }

    @Override
    protected ProcessCallback<String> getCallback() {
        return callback;
    }
}

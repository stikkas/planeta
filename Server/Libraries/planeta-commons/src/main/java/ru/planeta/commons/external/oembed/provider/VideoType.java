package ru.planeta.commons.external.oembed.provider;

import java.util.HashMap;
import java.util.Map;

/**
 * Enum VideoType
 *
 * @author s.fionov
 */
public enum VideoType {

    PLANETA(1), YOUTUBE(2), VIMEO(4), EXTERNAL(3),
    /**
     * Special video type for videos parsed
     * automatically from external video sources (like youtube or vimeo).
     * Videos of this type are hidden and didn't add to the user feed,
     * but are available to view with direct link.
     */
    EXTERNAL_HIDDEN(5);

    private VideoType(int code) {
        this.code = code;
    }

    private int code;

    public int getCode() {
        return code;
    }

    private static final Map<Integer, VideoType> lookup = new HashMap<Integer, VideoType>();

    static {
        for (VideoType s : values()) {
            lookup.put(s.getCode(), s);
        }
    }

    public static VideoType getByValue(int code) {
        return lookup.get(code);
    }
}

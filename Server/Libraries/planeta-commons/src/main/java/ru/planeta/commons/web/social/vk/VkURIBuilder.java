package ru.planeta.commons.web.social.vk;

import org.apache.http.client.utils.URIBuilder;

import java.io.UnsupportedEncodingException;
import java.net.URISyntaxException;
import java.net.URLEncoder;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Process URL building with CSV parameters like ..&uids=111,222,333&..
 */
public class VkURIBuilder {

    private URIBuilder uriBuilder;
    private Map<String, String> encodedParams = new LinkedHashMap<>();

    public VkURIBuilder() {
        uriBuilder = new URIBuilder();
    }

    public VkURIBuilder(String url) throws URISyntaxException {
        uriBuilder = new URIBuilder(url);
    }

    public VkURIBuilder addParameter(String key, String value) {
        uriBuilder.addParameter(key, value);
        return this;
    }

    public VkURIBuilder addListParameter(String key, List objectsList) {
        StringBuilder value = new StringBuilder();
        String encodedKey = null;
        try {
            encodedKey = URLEncoder.encode(key, "UTF-8");
            Iterator it = objectsList.iterator();
            while (it.hasNext()) {
                String part = URLEncoder.encode(it.next().toString(), "UTF-8");
                value.append(part);
                if (it.hasNext()) {
                    value.append(',');
                }
            }
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return this;
        }

        encodedParams.put(encodedKey, value.toString());
        return this;
    }

    public VkURIBuilder addParameter(String key, Object value) {
        if(value instanceof List) {
            return addListParameter(key, (List) value);
        } else {
            uriBuilder.addParameter(key, value.toString());
        }
        return this;
    }

    public VkURIBuilder addParameterMap(Map<String, Object> parameters) {
        for (Map.Entry<String, Object> entry : parameters.entrySet()) {
            addParameter(entry.getKey(), entry.getValue());
        }
        return this;
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder(uriBuilder.toString());
        if(!encodedParams.isEmpty()) {
            if (uriBuilder.getQueryParams().size() > 0) {
                result.append('&');
            } else {
                result.append('?');
            }
            Iterator<Map.Entry<String, String>> it = encodedParams.entrySet().iterator();
            while (it.hasNext()) {
                Map.Entry<String, String> entry = it.next();
                result.append(entry.getKey());
                result.append('=');
                result.append(entry.getValue());
                if(it.hasNext()) {
                    result.append('&');
                }
            }
        }
        return result.toString();
    }
}

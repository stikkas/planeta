package ru.planeta.commons.web.filters;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;

import static java.nio.charset.StandardCharsets.UTF_8;

/**
 */
public class JsonpCallbackRawdataFilter extends JsonpCallbackFilter {

    @Override
    protected void writeInner(OutputStream out, byte[] data) throws IOException {
        String stringData = new String(data, UTF_8);
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(JsonGenerator.Feature.ESCAPE_NON_ASCII, true);
        Map<String, String> jsonMap = new HashMap<>();
        jsonMap.put("content", StringUtils.trim(stringData));
        String json = objectMapper.writeValueAsString(jsonMap);
        out.write(json.getBytes(UTF_8));
    }
}
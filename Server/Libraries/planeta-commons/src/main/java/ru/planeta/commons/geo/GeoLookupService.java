package ru.planeta.commons.geo;

import com.maxmind.geoip.LookupService;
import ru.planeta.commons.web.IpUtils;
import ru.planeta.geo.updatable.UpdatableLookupService;

/**
 * @author atropnikov
 */
public class GeoLookupService {

    private final ru.planeta.geo.updatable.GeoLookupService lookupService;
    private IpBlockLocation ipBlockLocation;

    /**
     * First update time, default - half a day
     */
    private final static long startSleepTime = 43200000;

    /**
     * Update period, default - 1 day
     */
    private final static long sleepTime = 86400000;

    private IpBlockLocation getIpBlockLocation() {
        return ipBlockLocation;
    }

    public GeoLookupService() {
        try {
            lookupService = new UpdatableLookupService(Thread.currentThread().getContextClassLoader().getResource("geoip/GeoIP.dat").getPath(), LookupService.GEOIP_MEMORY_CACHE, sleepTime, startSleepTime);
            resetBlockLocations(new IpBlockLocation());
        } catch (Exception ex) {
            throw new Error("File not found. Can't initialize geo service", ex);
        }
    }

    public void finish() {
        lookupService.close();
    }

    public void resetBlockLocations(IpBlockLocation ipBlockLocation) {
        this.ipBlockLocation = ipBlockLocation;
    }

    public int getLocationId(String ip) {
        return getIpBlockLocation().getLocationId(IpUtils.ipToLong(ip));
    }

    public String getCountryCode(String ip) {
        return lookupService.getCountryCode(ip);
    }
}

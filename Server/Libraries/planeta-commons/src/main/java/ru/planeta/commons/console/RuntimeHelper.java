package ru.planeta.commons.console;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.SystemUtils;
import org.apache.log4j.Logger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.concurrent.TimeoutException;

import static java.nio.charset.StandardCharsets.UTF_8;

/**
 * Helper class for runtime commands execute
 *
 * @author ds.kolyshev
 * Date: 15.05.12
 */
public class RuntimeHelper {

    private static final Logger log = Logger.getLogger(RuntimeHelper.class);

	private static final long RUNTIME_TIMEOUT = 2000;

    public static boolean isNotWindows() {
        return !SystemUtils.IS_OS_WINDOWS;
    }


    /**
	 * Executes command with default timeout
	 *
	 * @param commandLine
	 * @return
	 * @throws IOException
	 * @throws InterruptedException
	 * @throws TimeoutException
	 */
	public static int executeCommandLine(final String commandLine) throws IOException, InterruptedException, TimeoutException {
		return executeCommandLine(commandLine, RUNTIME_TIMEOUT);
	}

	/**
	 * Executes command with specified timeout
	 *
	 * @param commandLine
	 * @param timeout
	 * @return
	 * @throws IOException
	 * @throws InterruptedException
	 * @throws TimeoutException
	 */
	public static int executeCommandLine(final String commandLine, final long timeout) throws IOException, InterruptedException, TimeoutException {
		Runtime runtime = Runtime.getRuntime();
		Process process = runtime.exec(commandLine);
		Worker worker = new Worker(process);
		worker.start();
		try {
			worker.join(timeout);
			if (worker.exit != null) {
                return worker.exit;
            } else {
                throw new TimeoutException();
            }
		} catch (InterruptedException ex) {
			worker.interrupt();
			Thread.currentThread().interrupt();
			throw ex;
		} finally {
			process.destroy();
		}
	}

	private static class Worker extends Thread {
		private final Process process;
		private Integer exit;

		private Worker(Process process) {
			this.process = process;
		}

		public void run() {
			try {
				exit = process.waitFor();
			} catch (InterruptedException ignore) {
            }
		}
	}


    /**
     * Executes command with command line
     * @param commands array of commands, /bin/bash will be inserted before all commands
     * @param outHandler handles output lines, may be null
     * @return
     * @throws IOException
     */
    public static int exec(String[] commands, OutLineHandler outHandler) throws IOException {
        String[] bash = new String[commands.length + 2];
        bash[0] = "/bin/bash";
        bash[1] = "-c";
        System.arraycopy(commands, 0, bash, 2, commands.length);
        if (log.isDebugEnabled()) {
            log.debug("Resulting commands array: " + Arrays.toString(bash));
        }

        ProcessBuilder builder = new ProcessBuilder(bash);
        builder.redirectErrorStream(true);
        Process process = builder.start();

        readProcess(outHandler, process);
        try {
            process.waitFor();
        } catch (InterruptedException e) {
            throw new RuntimeException("Command execution was interrupted. Resulting command array was: " + Arrays.toString(bash));
        }
        int exit = process.exitValue();
        log.debug("Command executed with exitCode=" + exit);
        return exit;
    }

    private static void readProcess(OutLineHandler outHandler, Process process) throws IOException {
        InputStream stdout = process.getInputStream();

        BufferedReader reader = new BufferedReader(new InputStreamReader(stdout, UTF_8));
        try{
        String line;
        while ((line = reader.readLine()) != null) {
            log.trace("Command out:~$ " + line);
            if (outHandler != null) {
                outHandler.handleLine(line);
            }
        }
        } finally {
            IOUtils.closeQuietly(reader);
        }
    }
}

package ru.planeta.commons.web;

import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.fluent.Form;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.*;
import java.security.*;
import java.security.cert.X509Certificate;
import java.util.*;
import java.util.regex.Pattern;

import static org.apache.http.HttpHeaders.CONNECTION;
import static org.apache.http.HttpHeaders.CONTENT_TYPE;

/**
 * Utility methods for uploading and downloading content through HTTP.
 */
public class WebUtils {

    // tablet support:   note that android.+mobile is mobile device, android without mobile - tablet
    private static final Pattern UA_TABLET = Pattern.compile("(?i).*(android|ipad|playbook|silk).*");
    private static final Pattern UA_MOBILE = Pattern.compile("(?i).*(android.+mobile|avantgo|bada/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|meego.+mobile|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino).*");
    private static final Pattern UA_SUB = Pattern.compile("(?i)1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\\-(n|u)|c55/|capi|ccwa|cdm\\-|cell|chtm|cldc|cmd\\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\\-s|devi|dica|dmob|do(c|p)o|ds(12|\\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\\-|_)|g1 u|g560|gene|gf\\-5|g\\-mo|go(\\.w|od)|gr(ad|un)|haie|hcit|hd\\-(m|p|t)|hei\\-|hi(pt|ta)|hp( i|ip)|hs\\-c|ht(c(\\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\\-(20|go|ma)|i230|iac( |\\-|/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |/)|klon|kpt |kwc\\-|kyo(c|k)|le(no|xi)|lg( g|/(k|l|u)|50|54|\\-[a-w])|libw|lynx|m1\\-w|m3ga|m50/|ma(te|ui|xo)|mc(01|21|ca)|m\\-cr|me(di|rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\\-2|po(ck|rt|se)|prox|psio|pt\\-g|qa\\-a|qc(07|12|21|32|60|\\-[2-7]|i\\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\\-|oo|p\\-)|sdk/|se(c(\\-|0|1)|47|mc|nd|ri)|sgh\\-|shar|sie(\\-|m)|sk\\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\\-|v\\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\\-|tdg\\-|tel(i|m)|tim\\-|t\\-mo|to(pl|sh)|ts(70|m\\-|m3|m5)|tx\\-9|up(\\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\\-|your|zeto|zte\\-");
    private static final Pattern UA_IOS = Pattern.compile(".*(iPad|iPhone|iPod).*");

    // http://www.useragentstring.com/pages/Java/ to prevent redirect of our link-checker bot
    private static final Pattern UA_JAVA_QUIRK = Pattern.compile("(?i)Java/?1\\.\\d+.*");
    private static final Pattern URL_PATTERN = Pattern.compile("^https?://[\\w\\d\\-\\.]+(/.*?)(\\?.*)?$");
    private static final String GRANT_ACCESS_PARAM_NAME = "_accessGranted";
    private static final String GRANT_ACCESS_PARAM_VALUE = "true";

    private static final String UA_HEADER = "User-Agent";
    private static final String REFERER_HEADER = "Referer";
    private static final String BOT_USER_AGENT = "Mozilla/5.0";

    private static Logger log = Logger.getLogger(WebUtils.class);

    public static String createUrl(String existingUrl, Map<String, String> additionalParams) {
        return createUrl(existingUrl, additionalParams, true);
    }

    public static String getFullURL(HttpServletRequest request) {
        StringBuffer requestURL = request.getRequestURL();
        String queryString = request.getQueryString();

        if (queryString == null) {
            return requestURL.toString();
        } else {
            return requestURL.append('?').append(queryString).toString();
        }
    }

    public static String getGtmCidFromCookie(HttpServletRequest request) {
        String cid = CookieUtils.getCookieValue(request.getCookies(), "_ga", "");
        cid = cid.replace("GA1.1.", "");
        cid = cid.replace("GA1.2.", "");
        cid = cid.replace("GA1.3.", "");
        cid = cid.replace("GA1.4.", "");
        cid = cid.replace("GA1.5.", "");
        return cid;
    }

    public static String createUrl(String existingUrl, Map<String, String> additionalParams, boolean overrideWithAdditional) {
        String[] href = existingUrl.split("\\?");
        String page = href[0];
        Map<String, String> allParams = new HashMap<String, String>(additionalParams);
        if (href.length == 2) {
            String query = href[1];
            String[] params = query.split("&");
            for (String param : params) {
                String[] keyValue = param.split("=");
                if (keyValue.length >= 2) {
                    if (!overrideWithAdditional || allParams.get(keyValue[0]) == null) {
                        try {
                            allParams.put(keyValue[0], URLDecoder.decode(keyValue[1], "UTF-8"));
                        } catch (UnsupportedEncodingException e) {
                            log.warn("Error while decoding params: " + keyValue[1]);
                        }
                    }
                } else {
                    log.error("Can't split request param " + param);
                }
            }
        }
        String params = tryUrlEncodeMap(allParams, "UTF-8");
        params = StringUtils.isBlank(params) ? "" : "?" + params;
        return page + params;
    }

    public static boolean isIos(HttpServletRequest request) {
        String ua = request.getHeader(UA_HEADER);
        return !(StringUtils.isEmpty(ua) || StringUtils.length(ua) < 4) && UA_IOS.matcher(ua).matches();
    }

    /**
     * Serializes request to string
     *
     * @return http request data
     */
    public static String requestDataToString(HttpServletRequest request) {
        if (request == null) {
            return "";
        }
        StringBuilder errors = new StringBuilder();

        try {
            // Appending request url
            errors.append(request.getMethod());
            errors.append(" ");
            errors.append(request.getScheme());
            errors.append("://");
            errors.append(request.getHeader("Host"));
            errors.append(request.getRequestURI());
            String query = request.getQueryString();
            if (!StringUtils.isEmpty(query)) {
                errors.append("?");
                errors.append(query);
            }
            errors.append("\n");

            Enumeration headerNames = request.getHeaderNames();

            while (headerNames.hasMoreElements()) {

                String headerName = headerNames.nextElement().toString();
                errors.append(headerName);
                errors.append(": ");
                String headerValue = request.getHeader(headerName);
                errors.append(headerValue);
                errors.append("\n");
            }
            addParamsToSb(errors, request);
        } catch (Exception ex) {
            log.warn(ex);
        }

        return errors.toString();
    }

    static private void addParamsToSb(StringBuilder sb, HttpServletRequest request) {

        if (request.getParameterMap() == null) {
            return;
        }
        sb.append("Parameters:\n");
        for (Object entry : request.getParameterMap().entrySet()) {
            Map.Entry parameter = (Map.Entry) entry;
            sb.append(parameter.getKey());
            sb.append("=");
            sb.append(request.getParameter((String) parameter.getKey()));
            sb.append("\n");
        }
        sb.append("\n");
    }

    /**
     * Sends HEAD request to the specified url.
     * Returns map of response headers (but for status code == 200 only).
     *
     * @param url               url
     * @param connectionTimeout connection timeout (ms)
     * @param readTimeout       read timeout (ms)
     * @return map with http response headers
     */
    public static Map<String, String> headRequest(String url, int connectionTimeout, int readTimeout) {
        try {
            return headRequest(new URL(url), connectionTimeout, readTimeout);
        } catch (Exception ex) {
            log.error("Wrong url has been passed for head request: " + url, ex);
        }
        return null;
    }

    private static HttpURLConnection prepareAllowAllHostsConnection(URL url) throws IOException, NoSuchAlgorithmException, KeyManagementException {
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        if ("https".equals(url.getProtocol())) {
            // hack from stackowerflow
            final TrustManager[] trustAllCertificates = new TrustManager[]{
                new X509TrustManager() {
                    @Override
                    public X509Certificate[] getAcceptedIssuers() {
                        return null;
                    }

                    @Override
                    public void checkClientTrusted(X509Certificate[] certs, String authType) {
                    }

                    @Override
                    public void checkServerTrusted(X509Certificate[] certs, String authType) {
                    }
                }
            };

            SSLContext sc = SSLContext.getInstance("SSL");
            sc.init(null, trustAllCertificates, new SecureRandom());
            HttpsURLConnection sconnection = (HttpsURLConnection) connection;
            sconnection.setSSLSocketFactory(sc.getSocketFactory());
        }
        return connection;
    }

    /**
     * Sends HEAD request to the specified url.
     * Returns map of response headers (but for status code == 200 only)
     *
     * @param url               url
     * @param connectionTimeout connection timeout (ms)
     * @param readTimeout       read timeout (ms)
     * @return map with http response headers
     */
    public static Map<String, String> headRequest(URL url, int connectionTimeout, int readTimeout) throws URISyntaxException, IOException, UnrecoverableKeyException, NoSuchAlgorithmException, KeyStoreException, KeyManagementException {
        HttpURLConnection connection = null;
        try {
            connection = prepareAllowAllHostsConnection(url);
            connection.setRequestMethod("HEAD");
            connection.setReadTimeout(readTimeout);
            connection.setConnectTimeout(connectionTimeout);
            connection.setRequestProperty("User-Agent", BOT_USER_AGENT);
            connection.connect();

            if (connection.getResponseCode() != HttpURLConnection.HTTP_OK) {
                return null;
            }

            Map<String, String> headers = new HashMap<>();

            for (Map.Entry<String, List<String>> entry : connection.getHeaderFields().entrySet()) {
                headers.put(entry.getKey(), StringUtils.lowerCase(StringUtils.join(entry.getValue(), "; ")));
            }

            return headers;
        } catch (Exception ex) {
            log.error("Error while doing head request to " + url, ex);
            return null;
        } finally {
            if (connection != null) {
                connection.disconnect();
            }
        }
    }

    /**
     * deprecated. Use IOUtils.toString instead
     *
     *
     * Downloads string from the specified url.
     * Uses "utf-8" encoding.
     *
     * @param url url to downloaded
     * @return response string in utf-8 encoding
     */
    @Deprecated
    public static String downloadString(String url) {
        return downloadString(url, "utf-8");
    }

    /**
     * Downloads string from the specified url
     *
     * @param url      url to download
     * @param encoding response encoding
     * @return response string
     */
    private static String downloadString(String url, String encoding) {

        byte[] bytes = downloadBytes(url);

        try {
            if (bytes == null) {
                return null;
            }

            return new String(bytes, encoding);
        } catch (Exception ex) {
            log.error("Cannot create string from bytes downloaded from url: " + url, ex);
            return null;
        }
    }

    public static String downloadStringWithTimeout(String url, int timeout) {
        RequestConfig requestConfig = RequestConfig.custom().setConnectTimeout(timeout).build();
        HttpClient httpClient = HttpClientBuilder.create().setDefaultRequestConfig(requestConfig).build();
        HttpGet httpGet = new HttpGet(url);
        try {
            HttpResponse httpResponse = httpClient.execute(httpGet);
            return IOUtils.toString(httpResponse.getEntity().getContent(), "UTF-8");
        } catch (IOException ex) {
            log.error(ex);
            return null;
        }
    }

    /**
     * Downloads bytes from the specified url
     *
     * @param url url to download
     * @return raw response bytes
     */
    public static byte[] downloadBytes(String url) {
        try {
            URL link = new URL(url);
            InputStream inputStream = link.openStream();
            try {
                return IOUtils.toByteArray(inputStream);
            } finally {
                IOUtils.closeQuietly(inputStream);
            }
        } catch (Exception ex) {
            log.error("Error downloading bytes from url: " + url, ex);
            return null;
        }
    }

    /**
     * Uploads (sends a POST request) data from the input stream to the specified url.
     * Returns string response. Returns null if something gone wrong.
     *
     * @param url             url to upload
     * @param postInputStream upload data stream
     * @param contentType     upload data content type
     * @return response string
     */
    public static String uploadStream(String url, InputStream postInputStream, String contentType, boolean useGrandAccess) {
        return uploadStream(url, postInputStream, contentType, "utf-8", useGrandAccess);
    }

    /**
     * Uploads (sends a POST request) data from the input stream to the specified url.
     * Returns string response. Returns null if something gone wrong.
     *
     * @param url             url to upload
     * @param postInputStream upload data stream
     * @param contentType     upload data content type
     * @param encoding        response encoding
     * @return response string
     */
    public static String uploadStream(String url, InputStream postInputStream, String contentType, String encoding, boolean useGrandAccess) {
        return uploadStream(url, postInputStream, contentType, encoding, useGrandAccess, null);
    }

    public static String uploadStream(String url, InputStream postInputStream, String contentType, String encoding, boolean useGrandAccess, String credentials) {
        try {
            byte[] bytes = upload(url, postInputStream, contentType, useGrandAccess, credentials);

            if (bytes == null) {
                return null;
            }

            return IOUtils.toString(bytes, encoding);
        } catch (Exception ex) {
            log.error("Error converting bytes to string", ex);
            return null;
        }
    }

    /**
     * Uploads (sends a POST request) data from the input stream to the specified url.
     * Returns byte array as response.
     *
     * @param url             url to upload
     * @param postInputStream post data stream
     * @param contentType     post data content type
     * @return raw response bytes
     */
    public static byte[] upload(String url, InputStream postInputStream, String contentType, boolean useGranAccess) {
        return upload(url, postInputStream, contentType, useGranAccess, null);
    }

    public static byte[] upload(String url, InputStream postInputStream, String contentType, boolean useGranAccess, String credentials) {
        try {
            if (useGranAccess) {
                url += (url.indexOf('?') > 0 ? "&" : "?") + GRANT_ACCESS_PARAM_NAME + "=" + GRANT_ACCESS_PARAM_VALUE;
            }

            URL link = new URL(url);
            if (!StringUtils.startsWithIgnoreCase(url, "http")) {
                log.error("Url " + url + " is not http");
                return null;
            }

            HttpURLConnection connection = null;
            OutputStream outputStream = null;
            InputStream inputStream = null;
            try {
                connection = (HttpURLConnection) link.openConnection();
                connection.setRequestMethod("POST");
                connection.setDoOutput(true);
                connection.setDoInput(true);
                connection.setUseCaches(false);
                connection.setRequestProperty("Content-Type", contentType);
                if (credentials != null) {
                    connection.setRequestProperty("Authorization", "Basic " + credentials);
                }

                outputStream = connection.getOutputStream();
                IOUtils.copy(postInputStream, outputStream);
                outputStream.flush();
                inputStream = connection.getInputStream();
                return IOUtils.toByteArray(inputStream);
            } finally {
                IOUtils.closeQuietly(inputStream);
                IOUtils.closeQuietly(outputStream);
                if (connection != null) {
                    connection.disconnect();
                }
            }
        } catch (Exception ex) {
            log.error("Error uploading stream to the " + url + " :", ex);
            return null;
        }
    }

    /**
     * Uploads (sends a POST request) to the specified url
     *
     * @param url         url to upload
     * @param postData    post data
     * @param contentType post data content type
     * @return response string
     */
    private static String uploadStringWithBasicHttpAuth(String url, String postData, String contentType, String encoding, String credentials) throws UnsupportedEncodingException {
        return uploadStream(url, new ByteArrayInputStream(postData.getBytes(encoding)), contentType, encoding, false, credentials);
    }


    private static String uploadString(String url, String postData, String contentType, boolean useGrandAccess) {
        return uploadStream(url, IOUtils.toInputStream(postData), contentType, useGrandAccess);
    }

    /**
     * Sends POST request to specified url with generated from map data. Use to planeta hosts
     *
     * @param url      url to POST
     * @param data     key-value dictionary to urlEncode into POST data
     * @return response of posted server
     */
    public static String uploadMap(String url, Map<String, String> data) {
        String stringData = getUploadString(data);
        return uploadString(url, stringData);
    }

    private static String getUploadString(Map<String, String> data) {
        return tryUrlEncodeMap(data, "utf-8");
    }

    // use to external(non planeta) urls
    public static String uploadMapExternal(String url, Map<String, String> data) {
        String stringData = getUploadString(data);
        return uploadStringWithoutParam(url, stringData);
    }

    /*
    Generates urlencoded POST data from key-value dictionary
     */
    private static String urlEncodeMap(Map<String, String> data, String encoding) throws UnsupportedEncodingException {
        StringBuilder postData = new StringBuilder();
        if (data == null) {
            return StringUtils.EMPTY;
        }
        Iterator<Map.Entry<String, String>> it = data.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry<String, String> pair = it.next();
            if (pair.getValue() != null) {
                postData.append(URLEncoder.encode(pair.getKey(), encoding));
                postData.append("=");
                postData.append(URLEncoder.encode(pair.getValue(), encoding));
                if (it.hasNext()) {
                    postData.append("&");
                }
            }
        }
        return postData.toString();
    }

    public static String tryUrlEncodeMap(String encoding, String... params) {
        Map<String, String> map = new HashMap<>();
        for (int i = 0; i < params.length; i += 2) {
            map.put(params[i], params[i + 1]);
        }
        return tryUrlEncodeMap(map, encoding);

    }

    public static String tryUrlEncodeMap(Map<String, String> data, String encoding) {
        String result;
        try {
            result = urlEncodeMap(data, encoding);
        } catch (UnsupportedEncodingException e) {
            Logger.getLogger(WebUtils.class).error("Error encoding", e);

            StringBuilder sb = new StringBuilder();
            Iterator<Map.Entry<String, String>> it = data.entrySet().iterator();
            while (it.hasNext()) {
                Map.Entry<String, String> param = it.next();
                if (param.getValue() != null) {
                    sb.append(String.format("%s=%s", param.getKey(), param.getValue()));
                    if (it.hasNext()) {
                        sb.append("&");
                    }
                }
            }
            result = sb.toString();
        }

        return result;
    }

    /**
     * Uploads postData to the specified url and returns result
     *
     * @param url      url to upload
     * @param postData upload data
     * @return response string
     */
    public static String uploadString(String url, String postData) {
        return uploadString(url, postData, "application/x-www-form-urlencoded", true);
    }

    public static String uploadStringWithoutParam(String url, String postData) {
        return uploadString(url, postData, "application/x-www-form-urlencoded", false);
    }

    public static String uploadStringWithBasicHttpAauth(String url, String postData, String contentType, String encoding, String credentials) throws UnsupportedEncodingException {
        return uploadStringWithBasicHttpAuth(url, postData, contentType, encoding, credentials);
    }

    /**
     * Extracts host from the url
     *
     * @param url url
     * @return host name
     */
    public static String getHost(String url) {
        if (StringUtils.isEmpty(url)) {
            return StringUtils.EMPTY;
        }
        int beginIndex = url.indexOf("://");
        if (beginIndex < 4) {
            return StringUtils.EMPTY;
        }
        beginIndex += 3;
        int endIndex = url.indexOf('/', beginIndex);
        if (endIndex < 0) {
            return url.substring(beginIndex);
        }
        return url.substring(beginIndex, endIndex);
    }

    /**
     * Uploads file with multipart/form-data mime type
     *
     * @param url  url to upload
     * @param file file to upload
     * @return response string
     */
    public static String uploadMultipartStream(String url, File file) throws IOException {
        return uploadMultipartStream(url, file, "utf-8");
    }

    /**
     * Uploads file with multipart/form-data mime type
     *
     * @param url             url to upload
     * @param file            file to upload
     * @param responseCharset response encoding
     * @return response string
     * @throws IOException
     */
    public static String uploadMultipartStream(String url, File file, String responseCharset) throws IOException {
        HttpClient httpClient = new DefaultHttpClient();
        HttpPost httpPost = new HttpPost(url);

        FileBody bin = new FileBody(file);
        StringBody comment = new StringBody("Filename: " + file.getName());

        MultipartEntity reqEntity = new MultipartEntity();
        reqEntity.addPart("userfile1", bin);
        reqEntity.addPart("comment", comment);
        httpPost.setEntity(reqEntity);

        HttpResponse response = httpClient.execute(httpPost);
        HttpEntity resEntity = response.getEntity();

        return EntityUtils.toString(resEntity, responseCharset);
    }

    /**
     * Checks if request is originated from Internet Explorer
     *
     * @param request request
     * @return true if user-agent is IE
     */
    public static boolean isInternetExplorer(HttpServletRequest request) {
        if (request != null) {
            String userAgent = request.getHeader("User-Agent");
            if (userAgent != null) {
                return userAgent.contains("MSIE") && !userAgent.contains("MSIE 10") && !userAgent.contains("ms-office");
            }
        }
        return false;
    }

    public static String removeJSessionFromUrl(String url) {
        return StringUtils.substringBefore(url, ";jsessionid=");
    }

    /**
     * Appends protocol(http:// or https:// if secured=true) to the passed url
     * Replaces http:// with https:// or vice versa if url already contained protocol, but secured parameter indicated to use different one.
     *
     * @param url     url with or without http
     * @param secured if true -- appends https
     * @return combined url
     */
    public static String appendProtocolIfNecessary(String url, boolean secured) {
        if (url == null) {
            return null;
        }
        url = url.replaceFirst("^https?://", "");
        String protocol = !secured || url.startsWith("localhost") ? "http://" : "https://";
        return protocol + url;
    }

    /**
     * Adds <code>no-cache</code> response headers to passed response {@link HttpServletResponse}
     *
     * @param response response
     */
    public static void setNoCacheResponseHeaders(HttpServletResponse response) {
        response.setHeader("Cache-Control", "private, max-age=0, no-cache, no-store, must-revalidate"); // HTTP 1.1.
        response.setHeader("Pragma", "no-cache"); // HTTP 1.0.
        response.setDateHeader("Expires", 0); // Proxies.
    }

    public static String getReferer(HttpServletRequest request) {
        return request.getHeader(REFERER_HEADER);
    }

    public static class Parameters {

        private Map<String, String> params;

        public Parameters() {
            params = new HashMap<String, String>();
        }

        public Parameters(Map<String, String> params) {
            if (params == null) {
                throw new IllegalArgumentException("Argument 'params' could not be null.");
            }
            this.params = params;
        }

        public Parameters(Parameters params) {
            if (params == null) {
                throw new IllegalArgumentException("Argument 'params' could not be null.");
            }
            this.params = params.getParams();
        }

        /**
         * Creates {@link Parameters} from JSON-object <code>json</code> presented as string.<br>
         * If Json-object is not plain, you must use "soft" parsing, otherwise will be thrown exception.<br>
         * When used "soft" parsing, all nested objects/arrays will be present as string parameter in resulted {@link Parameters}.
         *
         * @param json      JSON-object as string;
         * @param softParse "soft" parsing indicator, <code>true</code> enabled;
         * @return filled parameters from JSON-object.
         * @throws IOException JSON-object parsing error.
         */
        public static Parameters parsePlainJSON(String json, boolean softParse) throws IOException {
            Map<String, String> params = new HashMap<String, String>();
            if (StringUtils.isNotEmpty(json)) {
                Map<String, String> subObjects = new HashMap<String, String>();
                String plainJson = softParse ? createPlainJson(json, subObjects) : json;

                try {
                    params = new ObjectMapper().readValue(plainJson, new TypeReference<Map<String, String>>() {
                    });
                } catch (IOException e) {
                    throw new IOException(String.format("Error JSON object parsing: [%s]", json), e);
                }

                for (Map.Entry<String, String> param : params.entrySet()) {
                    if (subObjects.containsKey(param.getValue())) {
                        param.setValue(subObjects.get(param.getValue()));
                    }
                }
            }

            return new Parameters(params);
        }

        /**
         * Creates plain JSON object by extracting sub-objects.<br>
         * All nested objects/arrays are replaces to unique strings.<br>
         * All nested objects/arrays are collects in map, where keys are the same unique strings.
         *
         * @param json       processed plain JSON-object;
         * @param subObjects collected JSON-objects/arrays with result placeholders keys;
         * @return plain JSON-object where nested objects/arrays are replaced to unique key.
         */
        private static String createPlainJson(String json, Map<String, String> subObjects) {
            if (StringUtils.isEmpty(json)) {
                return StringUtils.EMPTY;
            }

            final int ROOT_OBJ_LEVEL = 1;
            final String PLACEHOLDER = "subObjectPlaceholder%d";
            final List<String> START_OBJECT_CHARS = Arrays.asList(JsonToken.START_OBJECT.asString(), JsonToken.START_ARRAY.asString());
            final List<String> END_OBJECT_CHARS = Arrays.asList(JsonToken.END_OBJECT.asString(), JsonToken.END_ARRAY.asString());
            Stack<String> bracketsStack = new Stack<String>();

            StringBuilder plainJson = new StringBuilder();
            StringBuilder subObject = new StringBuilder();
            for (int i = 0; i < json.length(); i++) {
                String currentChar = String.valueOf(json.charAt(i));
                int previousLevel = bracketsStack.size();

                if (START_OBJECT_CHARS.contains(currentChar)) {
                    bracketsStack.push(currentChar);
                } else if (END_OBJECT_CHARS.contains(currentChar)) {
                    bracketsStack.pop();
                }

                boolean isObjectFinished = (previousLevel > bracketsStack.size());
                boolean isSubObjectFinished = isObjectFinished && (bracketsStack.size() == ROOT_OBJ_LEVEL);
                boolean isSubObject = (bracketsStack.size() > ROOT_OBJ_LEVEL) || isSubObjectFinished;

                if (isSubObject) {
                    subObject.append(currentChar);
                    if (isSubObjectFinished) {
                        String placeholder = String.format(PLACEHOLDER, i);
                        plainJson.append(String.format("\"%s\"", placeholder));
                        subObjects.put(placeholder, subObject.toString());
                        subObject = new StringBuilder();
                    }
                } else {
                    plainJson.append(currentChar);
                }
            }

            return plainJson.toString();
        }

        public static Parameters parseServletRequest(ServletRequest request) {
            Map<String, String> params = new HashMap<String, String>();

            Enumeration names = request.getParameterNames();
            while (names.hasMoreElements()) {
                String name = names.nextElement().toString();
                String value = request.getParameter(name);
                params.put(name, value);
            }

            return new Parameters(params);
        }

        public Map<String, String> getParams() {
            return params;
        }

        public Parameters getParams(String... paramNames) {
            Parameters result = new Parameters();
            for (String paramName : paramNames) {
                if (has(paramName)) {
                    result.add(paramName, get(paramName));
                }
            }

            return result;
        }

        public Parameters add(String name, Object value) {
            if (value != null) {
                params.put(name, value.toString());
            }
            return this;
        }

        public Parameters add(String name, int value) {
            return add(name, String.valueOf(value));
        }

        public Parameters add(String name, long value) {
            return add(name, String.valueOf(value));
        }

        public Parameters add(Map.Entry<String, String> param) {
            return (param != null) ? add(param.getKey(), param.getValue()) : this;
        }

        public Parameters addAll(Parameters params) {
            getParams().putAll(params.getParams());
            return this;
        }

        public boolean has(String paramName) {
            return params.containsKey(paramName);
        }

        public String get(String paramName) {
            String result = params.get(paramName);
            return StringUtils.isEmpty(result) ? "" : result;
        }

        public String createUrl(String baseUrl, boolean overrideParamIfExists) {
            return WebUtils.createUrl(baseUrl, getParams(), overrideParamIfExists);
        }

        public Form createForm() {
            Form result = Form.form();
            for (Map.Entry<String, String> entry : params.entrySet()) {
                result.add(entry.getKey(), entry.getValue());
            }

            return result;
        }

        public boolean isEmpty() {
            return getParams().isEmpty();
        }

        @Override
        public String toString() {
            StringBuilder sb = new StringBuilder();
            for (Map.Entry entry : params.entrySet()) {
                sb.append(String.format("%s=%s%n", entry.getKey(), entry.getValue()));
            }

            return sb.toString();
        }
    }

    public static Map<String, String> getQueryParams(String query, String enc) throws UnsupportedEncodingException {
        Map<String, String> data = new HashMap<>();
        int idx = query.indexOf('?');
        int state = 1;
        char c;
        StringBuilder pName = new StringBuilder();
        StringBuilder pVal = new StringBuilder();
        for (int i = idx + 1; i < query.length(); ++i) {
            c = query.charAt(i);
            switch (state) {
                case 1:
                    if (c == '=') {
                        state = 2;
                    } else {
                        pName.append(c);
                    }
                    break;
                case 2:
                    if (c == '&') {
                        state = 1;
                        data.put(pName.toString(), URLDecoder.decode(pVal.toString(), enc));
                        pName = new StringBuilder();
                        pVal = new StringBuilder();
                    } else if (i == query.length() - 1) {
                        pVal.append(c);
                        data.put(pName.toString(), URLDecoder.decode(pVal.toString(), enc));
                    } else {
                        pVal.append(c);
                    }
                    break;
            }
        }
        return data;
    }

    @Deprecated
    public static String urlDecode(String url, String encode) {
        String decodeString = "#invalidUrl";
        try {
            decodeString = URLDecoder.decode(url, encode);
        } catch (UnsupportedEncodingException e) {
            log.warn("Error while decoding params: " + url);
        }

        return decodeString;
    }

    public static String getUriWithQuery(HttpServletRequest request) {
        String requestURL = request.getRequestURI();
        String queryString = request.getQueryString();

        if (queryString == null) {
            return requestURL;
        } else {
            return requestURL + "?" + queryString;
        }
    }

    public static void setDefaultHeaders(HttpServletResponse response) {
        response.setStatus(HttpServletResponse.SC_OK);
        response.setHeader(CONTENT_TYPE, "text/html");
        response.setHeader(CONNECTION, "close");
        setNoCacheResponseHeaders(response);
    }

    public static String encodeUrl(String url) {
        String result = url;
        try {
            result = URLEncoder.encode(url, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            //never happen
        }
        return result;
    }

}

package ru.planeta.commons.web;

import org.apache.commons.lang3.StringUtils;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

/**
 * Helper class for working with ip addresses
 *
 * @author ameshkov
 */
public class IpUtils {

    /**
     * Converts ip address to Long
     *
     * @param addr
     * @return
     */
    public static Long ipToLong(String addr) {
        String[] addrArray = addr.split("\\.");

        long num = 0;
        for (int i = 0; i < addrArray.length; i++) {
            int power = 3 - i;

            num += ((Integer.parseInt(addrArray[i]) % 256 * Math.pow(256, power)));
        }
        return num;
    }

    /**
     * Converts long to ip address
     *
     * @param i
     * @return
     */
    public static String longToIp(Long i) {
        return ((i >> 24) & 0xFF) + "." +
                ((i >> 16) & 0xFF) + "." +
                ((i >> 8) & 0xFF) + "." +
                (i & 0xFF);
    }

    /**
     * Gets user ip address. Checks proxy headers first
     *
     * @param req
     * @return
     */
    public static String getRemoteAddr(HttpServletRequest req) {
        String ip = req.getHeader("X-Real-IP");

        if (StringUtils.isBlank(ip)) {
            ip = req.getHeader("X-Forwarded-For");
        }

        if (StringUtils.isBlank(ip)) {
            ip = req.getRemoteAddr();
        }

        return ip;
    }

    public static List<String> getIpList(HttpServletRequest request) {
        String ip = request.getHeader("X-Forwarded-For");

        String[] ips = StringUtils.split(ip, ",");
        List<String> result = new ArrayList<String>();
        if (ips != null) {
            for (String ip1 : ips) {
                if (StringUtils.isNotBlank(ip1)) {
                    if (!result.contains(ip1.trim())) {
                        result.add(ip1.trim());
                    }
                }
            }
        }
        String realIp = request.getHeader("X-Real-IP");
        if (realIp != null) {
            if (!result.contains(realIp)) {
                result.add(realIp);
            }
        }
        String remote = request.getRemoteAddr();
        if (remote != null) {
            if (!result.contains(remote)) {
                result.add(remote);
            }
        }
        return result;
    }

}

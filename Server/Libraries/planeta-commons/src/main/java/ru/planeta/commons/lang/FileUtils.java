package ru.planeta.commons.lang;

import java.io.File;
import java.io.InputStream;
import java.net.URL;

/**
 * @author p.vyazankin
 * @since 2/28/13 1:50 PM
 */
public class FileUtils {

    static final int MAX_FILE_LENGTH = 255;
    private static final String CUT_SYMBOLS = "_";

    public static URL getResourceFromClasspath(String resourceName) {
        return FileUtils.class.getClassLoader().getResource(resourceName);
    }

    public static InputStream getResourceFromClasspathAsStream(String resourceName) {
        return FileUtils.class.getClassLoader().getResourceAsStream(resourceName);
    }

    public static File getFileFromClasspath(String resourceName) {
        return new File(FileUtils.class.getClassLoader().getResource(resourceName).getFile());
    }

    public static String stripLongFilename(String reqFileName) {
        String extension = reqFileName.substring(reqFileName.lastIndexOf("."));
        if(reqFileName.length() > MAX_FILE_LENGTH) {
            reqFileName = reqFileName.substring(0, MAX_FILE_LENGTH - extension.length() - CUT_SYMBOLS.length()) + CUT_SYMBOLS + extension;
        }
        return reqFileName;
    }

}

package ru.planeta.commons.onlinekassa;

//https://cloudpayments.ru/docs/api/kassa#auth
public class Receipt {
    private String inn;
    private String invoiceId;
    private String accountId;
    private String type;
    private CustomerReceipt customerReceipt;

    public String getInn() {
        return inn;
    }

    public void setInn(String inn) {
        this.inn = inn;
    }

    public String getInvoiceId() {
        return invoiceId;
    }

    public void setInvoiceId(String invoiceId) {
        this.invoiceId = invoiceId;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public String getType() {
        return type;
    }

    public void setType(ReceiptType type) {
        this.type = type.toString();
    }

    public CustomerReceipt getCustomerReceipt() {
        return customerReceipt;
    }

    public void setCustomerReceipt(CustomerReceipt customerReceipt) {
        this.customerReceipt = customerReceipt;
    }
}

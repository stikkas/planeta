package ru.planeta.commons.web.typograf;

import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

/**
 * Stub for async web clients
 *
 * @author s.kalmykov
 * @see ru.planeta.commons.web.typograf.TypografAsyncWebClient
 * @since 28.03.2014
 */
public abstract class AbstractAsyncWebClient<I, O> {

    private static final Logger log = Logger.getLogger(AbstractAsyncWebClient.class);

    protected abstract O syncProcess(I input) throws Exception;

    protected abstract ScheduledExecutorService getExecutorService();

    protected abstract O defaultValue(I input);

    protected abstract ProcessCallback<O> getCallback();

    private List<O> defaultValues(I[] inputs) {
        List<O> outputs = new ArrayList<O>(inputs.length);
        for (I input : inputs) {
            outputs.add(defaultValue(input));
        }
        return outputs;
    }

    /**
     * Callback for async processing, i.e. separate db text field update
     */
    public interface ProcessCallback<O> {

        void afterProcess(List<O> outputs);
    }

    /**
     * Helper class to iterate processed string via next() method
     * @param <O>
     */
    public static abstract class QueueProcessCallback<O> implements ProcessCallback<O> {

        private int counter = 0;
        private List<O> outputs;

        public O next() {
            return outputs.get(counter++);
        }

        public abstract void afterProcessQueue() throws ArrayIndexOutOfBoundsException;

        @Override
        public void afterProcess(List<O> outputs) {
            this.outputs = outputs;
            try {
                afterProcessQueue();
            } catch (ArrayIndexOutOfBoundsException e) {
                log.error(e);
            }
        }
    }

    /**
     * Multithreaded parallel string processing
     */
    private class ProcessTask implements Runnable {

        private ProcessTask(I[] inputStrings) {
            this.inputs = inputStrings;
            this.outputs = new ArrayList<O>(defaultValues(inputs));
        }

        private I[] inputs;
        private List<O> outputs;

        @Override
        public void run() {
            ExecutorService es = getExecutorService();
            CompletionService<O> ecs = new ExecutorCompletionService<>(es);
            List<Future<O>> futures = new ArrayList<>(inputs.length);
            for (I input : inputs) {
                futures.add(ecs.submit(new SingleProcessTask(input)));
            }

            for (int i = 0; i < inputs.length; ++i) {
                try {
                    outputs.set(i, futures.get(i).get(5, TimeUnit.MINUTES));
                } catch (InterruptedException | ExecutionException | TimeoutException e) {
                    log.error(e);
                }
            }

            getCallback().afterProcess(outputs);
        }
    }

    private class SingleProcessTask implements Callable<O> {

        private I input;

        private SingleProcessTask(I input) {
            this.input = input;
        }

        @Override
        public O call() throws Exception {
            return syncProcess(input);
        }
    }

    public void processAsync(I[] inputs) {
        getExecutorService().schedule(new ProcessTask(inputs), 1, TimeUnit.SECONDS);
    }
}

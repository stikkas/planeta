package ru.planeta.commons.concurrent;

/**
 * Lock mode. Could be shared or exclusive
 */
public enum LockMode {

    /**
     * Any number of threads can acquire a lock in shared mode
     * if it is not yet acquired in exclusive mode.
     */
    SHARED,

    /**
     * Only one exclusive lock is possible at the moment
     */
    EXCLUSIVE
}

package ru.planeta.commons.json;

import com.fasterxml.jackson.databind.ser.std.DateSerializer;

import java.text.SimpleDateFormat;

/**
 * Created by alexa_000 on 31.10.2014.
 */
public class ReadableDateFormatter extends DateSerializer {
    private static final String FORMAT = "dd.MM.yyyy";

    public ReadableDateFormatter() {
        super(false, new SimpleDateFormat(FORMAT));
    }
}

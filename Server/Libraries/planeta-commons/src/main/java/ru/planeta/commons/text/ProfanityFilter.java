package ru.planeta.commons.text;

import org.apache.commons.lang3.StringUtils;

import java.util.*;

/**
 * Profanity filter, that excludes most of the profane words from the text.
 *
 * User: m.shulepov
 * Date: 03.04.12
 * Time: 13:10
 */
public class ProfanityFilter {

    private static final String ESCAPE_SYMBOL = "*";

    private static final Set<String> EXCEPTIONS = new HashSet<String>();
    private static final List<String> BLACKLIST = new ArrayList<String>();

    static {
        List<String> blacklist = Arrays.asList("\u0441\u0443\u043a\u0430", "\u0431\u043b\u044f", "\u0431\u0430\u043b\u0434\u0430", "\u0437\u0430\u043b\u0443\u043f\u0430", "\u043c\u0430\u043d\u0434\u0430", "\u0431\u043b\u044f\u0434\u044c", "\u0431\u043b\u044f\u0434\u043e\u043b\u0438\u0437", "\u0431\u043b\u044f\u0434\u0443\u043d", "\u0431\u043b\u044f\u0434\u043e\u0432\u0430\u0442\u044c", "\u0431\u043b\u044f\u0434\u043a\u0438", "\u0431\u043b\u044f\u0445\u0430", "\u0445\u0443\u0439",  "\u043e\u0445\u0443\u0435\u0442\u044c", "\u043e\u0445\u0443\u0438\u0442\u0435\u043b\u044c\u043d\u043e", "\u043e\u0445\u0443\u0435\u043b", "\u0445\u0443\u0435\u0441\u043e\u0441", "\u043f\u0438\u0437\u0434\u0430", "\u043f\u0438\u0437\u0434\u0435\u0446", "\u043f\u0438\u0437\u0434\u0435\u0442\u044c", "\u043f\u0438\u0437\u0434\u043e\u043b\u0438\u0437", "\u043f\u0438\u0437\u0434\u0435\u043d\u044b\u0448", "\u0435\u0431\u0430\u0442\u044c", "\u0432\u044b\u0435\u0431\u043d\u0443\u043b\u0441\u044f", "\u0435\u0431\u044b\u0440\u044c", "\u0435\u0431\u0430\u043d\u043d\u044b\u0439", "\u043d\u0430\u0435- \u0437\u0430\u0435\u0431\u0430\u0442\u044c", "\u043f\u043e\u0434\u044a\u0435\u0431\u043d\u0443\u0442\u044c",  "\u0435\u0431", "\u0438\u043f\u0430\u0442\u044c\u0441\u044f", "\u043d\u0438\u0438\u043f\u0435\u0442", "\u0445\u0435\u0440",  "\u0445\u0435\u0440\u043e\u0432\u044b\u0439", "\u0445\u0435\u0440\u043e\u0432\u0430\u0442\u043e", "\u043f\u0438\u0434\u043e\u0440",  "\u043f\u0435\u0434\u0440\u0438\u043b\u0430", "\u043f\u0438\u0434\u043e\u0440\u0430\u0441", "\u043f\u0438\u0434\u0430\u0440", "\u043f\u0438\u0434\u0430\u0440\u0430\u0437", "\u0433\u0430\u043d\u0434\u043e\u043d", "\u043c\u0443\u0434\u0430\u043a", "\u043c\u0443\u0434\u043e\u0437\u0432\u043e\u043d", "\u043c\u0443\u0434\u0438\u043b\u0430", "\u043e\u0442\u043c\u0443\u0434\u043e\u0445\u0430\u0442\u044c", "\u044e\u043b\u0434\u0430", "\u044e\u043b\u0434\u0430\u043a", "\u044f\u043b\u0434\u0430", "\u044f\u043b\u0434\u043e\u043c\u0435\u0440");
        // get words' bases
        Stemmer stemmer = new Stemmer();
        for (String blacklistWord: blacklist) {
            BLACKLIST.add(stemmer.stem(blacklistWord));
        }

        // над, под, вот, вне, без, при, пре, для,  ото, надо, меж, между, изо
        EXCEPTIONS.addAll(Arrays.asList("\u043d\u0430\u0434", "\u043f\u043e\u0434", "\u0432\u043e\u0442", "\u0432\u043d\u0435", "\u0431\u0435\u0437", "\u043f\u0440\u0438", "\u043f\u0440\u0435", "\u0434\u043b\u044f", "\u043e\u0442\u043e", "\u043d\u0430\u0434\u043e", "\u043c\u0435\u0436", "\u043c\u0435\u0436\u0434\u0443", "\u0438\u0437\u043e"));
    }

    public static String filter(String text) {
        Stemmer stemmer = new Stemmer();

        String[] words = text.split("[ .,;:\r\n]");
        for (String word: words) {
            String wordToTest = word.toLowerCase().replaceAll("[^a-zA-Z\\u0430-\\u044f\\u0410-\\u042f0-9]", ""); //[^a-zA-Zа-яА-Я0-9]
            String wordBase = stemmer.stem(wordToTest);

            if (wordToTest.length() < 3 || EXCEPTIONS.contains(wordToTest)) {
                continue;
            }

            for (String blackListWord: BLACKLIST) {
                if (wordBase.contains(blackListWord) || blackListWord.contains(wordBase)) {
                    text = StringUtils.replaceOnce(text, word, StringUtils.repeat(ESCAPE_SYMBOL, word.length()));
                    break;
                }
            }
        }
        return text;
    }

}

package ru.planeta.commons.collections;

import java.util.Collection;
import java.util.Map;

/**
 * Created by IntelliJ IDEA.
 * User: ameshkov
 * Date: 02.03.12
 * Time: 11:22
 * To change this template use File | Settings | File Templates.
 */
public class MapBuilder<T extends Map> {

    private final T map;

    /**
     * Creates an instance of the map builder
     */
    public MapBuilder(Class<T> clazz) {
        try {
            map = clazz.newInstance();
        } catch (Exception ex) {
            throw new RuntimeException("Wrong class specified in MapBuilder constructor", ex);
        }
    }

    /**
     * Adds new entry to the map
     *
     * @param key
     * @param value
     * @return
     */
    public MapBuilder<T> add(Object key, Object value) {
        map.put(key, value);
        return this;
    }

    /**
     * Adds collection entry to map with empty checking.
     * If collection is empty <code>null</code> will be add.
     *
     * @param key
     * @param collection
     * @return
     */
    public MapBuilder<T> addCollection(Object key, Collection<?> collection) {
        Object value = null;
        if (collection != null && !collection.isEmpty()) {
            value = collection;
        }
        return add(key, value);
    }

    /**
     * Builds map
     *
     * @return
     */
    public T build() {
        return map;
    }
}

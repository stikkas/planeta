package ru.planeta.commons.web.filters;

import javax.servlet.ServletOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import javax.servlet.WriteListener;

/**
 * User: m.shulepov
 * Date: 05.04.12
 * Time: 14:40
 */
@Deprecated
public class FilterServletOutputStream extends ServletOutputStream {

    private OutputStream stream;

    public FilterServletOutputStream(OutputStream output) {
        this.stream = output;
    }

    @Override
    public void write(int b) throws IOException {
        stream.write(b);
    }

    @Override
    public void write(byte[] b) throws IOException {
        stream.write(b);
    }

    @Override
    public void write(byte[] b, int off, int len) throws IOException {
        stream.write(b, off, len);
    }

    @Override
    public boolean isReady() {
        return true;
    }

    @Override
    public void setWriteListener(WriteListener wl) {
    }

}
package ru.planeta.commons.web.social.vk;

import org.apache.log4j.Logger;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Manages vk.com server api requests and responses (<a href="http://vk.com/dev/secure">http://vk.com/dev/secure</a>)
 */

public class VkSecureApiClient extends FetchingTokenVkClient {

    private static final String GRANT_TYPE = "client_credentials";
    private static final String GRANT_TYPE_PARAM_NAME = "grant_type";
    private static final int MAX_NOTIFICATION_LENGTH = 254;

    /**
     * http://vk.com/dev/secure.sendNotification
     * sending notification to one user too frequently is restricted
     * but it is no official info about minimum timeout
     *
     * @param uids    list of vk users ids
     * @param message notification message
     * @return ids of users who successfully recieve send notification
     */
    public List<Long> sendNotification(List<Long> uids, String message) {
        if (uids.size() > 1000) {
            LOGGER.error("Too much recipients for secure.sendNotification: " + uids.size());
            return Collections.emptyList();
        }
        if (uids.isEmpty()) {
            LOGGER.error("No uids secure.sendNotification: " + uids.size());
            return Collections.emptyList();
        }
        Map<String, Object> params = new HashMap<>();
        if (message.length() > MAX_NOTIFICATION_LENGTH) {
            LOGGER.error("Too much symbols in message for secure.sendNotification: " + message);
            return Collections.emptyList();
        }
        params.put("uids", uids);
        params.put("message", message);
        try{
            String successUids = (String) apiRequest("secure.sendNotification", params);
            return parseVkUids(successUids);
        } catch (ClassCastException ex) {
            LOGGER.warn(ex);
            return Collections.emptyList();
        } catch (Exception ex) {
            LOGGER.error(ex);
            return Collections.emptyList();
        }

    }

    protected String getAccessTokenUrl() {
        Map<String, Object> accessTokenRequestParams = new HashMap<>();
        accessTokenRequestParams.put(CLIENT_ID_PARAM_NAME, getClientId());
        accessTokenRequestParams.put(CLIENT_SECRET_PARAM_NAME, getClientSecret());
        accessTokenRequestParams.put(GRANT_TYPE_PARAM_NAME, GRANT_TYPE);
        return getAccessTokenUrl(accessTokenRequestParams);
    }

    private final static Logger LOGGER = Logger.getLogger(VkSecureApiClient.class);

    public VkSecureApiClient(String clientId, String clientSecret) {
        super(clientId, clientSecret);
    }


}
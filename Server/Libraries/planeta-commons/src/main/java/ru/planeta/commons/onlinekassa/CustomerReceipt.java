package ru.planeta.commons.onlinekassa;

import java.util.List;

//https://cloudpayments.ru/docs/api/kassa#auth
public class CustomerReceipt {
    private List<CustomerReceiptItem> items;
    private int taxationSystem;
    private String email;
    private String phone;

    public List<CustomerReceiptItem> getItems() {
        return items;
    }

    public void setItems(List<CustomerReceiptItem> items) {
        this.items = items;
    }

    public int getTaxationSystem() {
        return taxationSystem;
    }

    public void setTaxationSystem(TaxationSystem taxationSystem) {
        this.taxationSystem = taxationSystem.getCode();
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}

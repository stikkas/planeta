package ru.planeta.commons.web.social.fb;

import ru.planeta.commons.web.WebUtils;

/**
 * Created by Alexey on 20.07.2015.
 */
public class FbService {
    public static String refreshFbCache(String url) {
        String str = WebUtils.uploadMapExternal("https://graph.facebook.com/", new WebUtils.Parameters().
                add("id", url).
                add("scrape", "true").getParams());
        return str;
    }
}

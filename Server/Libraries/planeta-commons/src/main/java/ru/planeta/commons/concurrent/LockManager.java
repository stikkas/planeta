package ru.planeta.commons.concurrent;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * deprecated this code is ugly
 * Special class managing locks on different objects
 */
@Deprecated
public class LockManager {

    private final Map<Object, Lock> objectLockMap = new ConcurrentHashMap<Object, Lock>();

    private synchronized Lock getOrCreateObjectLock(Object obj) {

        Lock lock = objectLockMap.get(obj);
        if (lock == null) {
            lock = new Lock();
            objectLockMap.put(obj, lock);
        }

        return lock;
    }

    /**
     * Acquired shared lock on the specified object
     *
     * @param obj
     * @return
     */
    public boolean acquireSharedLock(Object obj) {
        return getOrCreateObjectLock(obj).acquireSharedLock();
    }

    /**
     * Acquires exclusive lock on the specified object
     *
     * @param obj
     * @return
     */
    public boolean acquireExclusiveLock(Object obj) {
        return getOrCreateObjectLock(obj).acquireExclusiveLock();
    }

    /**
     * Releases shared lock on the specified object
     *
     * @param obj
     * @return
     */
    public boolean releaseSharedLock(Object obj) {

        Lock lock = objectLockMap.get(obj);
        if (lock != null) {
            return lock.releaseSharedLock();
        }
        return true;
    }

    /**
     * Releases exclusive lock on the specified object
     *
     * @param obj
     * @return
     */
    public boolean releaseExclusiveLock(Object obj) {
        Lock lock = objectLockMap.get(obj);
        if (lock != null) {
            return lock.releaseExclusiveLock();
        }
        return true;
    }

    /**
     * Releases all locks on the specified object
     *
     * @param obj
     * @return
     */
    public boolean release(Object obj) {
        Lock lock = objectLockMap.get(obj);
        if (lock != null) {
            return lock.release();
        }
        return true;
    }
}

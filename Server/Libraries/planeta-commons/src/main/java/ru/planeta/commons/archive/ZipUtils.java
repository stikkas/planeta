package ru.planeta.commons.archive;

import org.apache.commons.compress.archivers.zip.ZipArchiveEntry;
import org.apache.commons.compress.archivers.zip.ZipArchiveInputStream;
import org.apache.commons.compress.archivers.zip.ZipArchiveOutputStream;
import org.apache.commons.compress.archivers.zip.ZipFile;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;

import java.io.*;
import java.net.URI;
import java.util.Deque;
import java.util.Enumeration;
import java.util.LinkedList;


/**
 * @author ds.kolyshev
 * Date: 05.12.11
 */
public class ZipUtils {

    /**
     * Compress folder
     *
     * @throws IOException
     */
    public static void zip(File directory, File zippedFile) throws IOException {
        URI base = directory.toURI();
        Deque<File> queue = new LinkedList<>();
        queue.push(directory);
        OutputStream out = null;
        ZipArchiveOutputStream zout = null;
        try {
            out = new FileOutputStream(zippedFile);
            zout = new ZipArchiveOutputStream(out);
            while (!queue.isEmpty()) {
                directory = queue.pop();
                if (directory == null) {
                    continue;
                }
                File[] list = directory.listFiles();
                if (list == null) {
                    continue;
                }
                for (File kid : list) {
                    String name = base.relativize(kid.toURI()).getPath();
                    if (kid.isDirectory()) {
                        queue.push(kid);
                        name = name.endsWith("/") ? name : name + "/";
                        zout.putArchiveEntry(new ZipArchiveEntry(name));
                    } else {
                        zout.putArchiveEntry(new ZipArchiveEntry(name));
                        InputStream inputStream = FileUtils.openInputStream(kid);
                        try {
                            IOUtils.copy(inputStream, zout);
                        } finally {
                            IOUtils.closeQuietly(inputStream);
                        }
                        zout.closeArchiveEntry();
                    }
                }
            }
            zout.finish();
        } finally {
            IOUtils.closeQuietly(zout);
            IOUtils.closeQuietly(out);
        }
    }

    /**
     * Decompress folder
     *
     * @throws IOException
     */
    public static void unzip(File zippedFile, File directory) throws IOException {
        ZipFile zfile = new ZipFile(zippedFile);
        try {
            Enumeration<? extends ZipArchiveEntry> entries = zfile.getEntries();
            while (entries.hasMoreElements()) {
                ZipArchiveEntry entry = entries.nextElement();
                File file = new File(directory, entry.getName());
                if (entry.isDirectory()) {
                    file.mkdirs();
                } else {
                    file.getParentFile().mkdirs();
                    InputStream in = zfile.getInputStream(entry);
                    try {
                        FileUtils.copyInputStreamToFile(in, file);
                    } finally {
                        IOUtils.closeQuietly(in);
                    }
                }
            }
        } finally {
            zfile.close();
        }

    }

    public static InputStream unzipFile(InputStream zippedStream, String relativePath) throws IOException {
        ZipArchiveInputStream zipArchiveInputStream = new ZipArchiveInputStream(zippedStream);
        ZipArchiveEntry entry = zipArchiveInputStream.getNextZipEntry();
        while (entry != null) {
            if (entry.getName().equals(relativePath)) {
                return zipArchiveInputStream;
            }
            entry = zipArchiveInputStream.getNextZipEntry();
        }
        throw new IOException("No such file " + relativePath);
    }
}

package ru.planeta.commons.external.oembed;

import ru.planeta.commons.external.oembed.provider.VideoType;

/**
 * Created with IntelliJ IDEA.
 * User: connanedoile
 * Date: 4/4/12
 * Time: 6:00 PM
 */
public class OembedInfo {

	private String id;
	private String name;
	private String description;
	private String thumbnailUrl;
	private int duration;
	private String html;
	private int width = 0;
	private int height = 0;
	private VideoType videoType;
	private String naturalUrl;

	public VideoType getVideoType() {
		return videoType;
	}

	public void setVideoType(VideoType videoType) {
		this.videoType = videoType;
	}

	public void setNaturalUrl(String naturalUrl) {
		this.naturalUrl = naturalUrl;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public String getHtml() {
		return html;
	}

	public void setHtml(String html) {
		this.html = html;
	}


	/**
	 * Youtube video identifier
	 *
	 * @return
	 */
	public String getId() {
		return id;
	}

	/**
	 * Sets youtube video identifier
	 *
	 * @param id
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * Video name
	 *
	 * @return
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets youtube video name
	 *
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Video description
	 *
	 * @return
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Sets video description
	 *
	 * @param description
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * Video thumbnail url
	 *
	 * @return
	 */
	public String getThumbnailUrl() {
		return thumbnailUrl;
	}

	/**
	 * Sets video thumbnail url
	 *
	 * @param thumbnailUrl
	 */
	public void setThumbnailUrl(String thumbnailUrl) {
		this.thumbnailUrl = thumbnailUrl;
	}

	/**
	 * Video duration (ms)
	 *
	 * @return
	 */
	public int getDuration() {
		return duration;
	}

	/**
	 * Sets video duration (ms)
	 *
	 * @param duration
	 */
	public void setDuration(int duration) {
		this.duration = duration;
	}

	public String getNaturalUrl() {
		return this.naturalUrl;
	}
}

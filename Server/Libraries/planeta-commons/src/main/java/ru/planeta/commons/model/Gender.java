package ru.planeta.commons.model;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * Enum Gender
 * @author a.tropnikov
 */
public enum Gender {

    NOT_SET(0), MALE(1), FEMALE(2);
    private int code;

    private Gender(int code) {
        this.code = code;
    }

    public int getCode() {
        return code;
    }
    private static final Map<Integer, Gender> lookup = new HashMap<Integer, Gender>();

    static {
        for (Gender s : EnumSet.allOf(Gender.class)) {
            lookup.put(s.getCode(), s);
        }
    }

    public static Gender getByValue(int code) {
        return lookup.get(code);
    }
}

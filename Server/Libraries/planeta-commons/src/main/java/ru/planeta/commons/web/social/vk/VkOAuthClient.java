package ru.planeta.commons.web.social.vk;

import org.apache.commons.lang3.math.NumberUtils;
import ru.planeta.commons.web.VkRegistration;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * For single user authorized by OAuth
 */
public class VkOAuthClient extends FetchingTokenVkClient {

    private static final String AUTH_CODE_PARAM = "code";
    private static final String AUTH_REDIRECT_PARAM = "redirect_uri";
    public static final String USER_ID_JSON_KEY = "user_id";

    private String code;
    private long uid;
    private String redirectUri;

    public VkOAuthClient(String clientId, String clientSecret, String code, String redirectUri) {
        super(clientId, clientSecret);
        this.code = code;
        this.redirectUri = redirectUri;
        getAccessToken();  // to set uid and cache token
    }

    @Override
    protected String getAccessTokenUrl() {
        Map<String, Object> accessTokenRequestParams = new HashMap<String, Object>();
        accessTokenRequestParams.put(CLIENT_ID_PARAM_NAME, getClientId());
        accessTokenRequestParams.put(CLIENT_SECRET_PARAM_NAME, getClientSecret());
        accessTokenRequestParams.put(AUTH_CODE_PARAM, code);
        accessTokenRequestParams.put(AUTH_REDIRECT_PARAM, redirectUri);
        return getAccessTokenUrl(accessTokenRequestParams);
    }

    @Override
    protected String processAccessTokenResponseObject(Map<String, Object> responseObject) {
        String accessToken = super.processAccessTokenResponseObject(responseObject);
        uid = NumberUtils.createLong(responseObject.get(USER_ID_JSON_KEY).toString());
        return accessToken;
    }

    public VkRegistration authorize() throws IOException {
        return getUser(uid);
    }


}

package ru.planeta.commons.model;

import java.util.Date;

public class OrderShortLinkStat {
    public static final String SHORTLINK_ID_COOKIE_NAME = "plShLID";
    public static final String SHORTLINK_REFERRER_COOKIE_NAME = "plShLRf";

    private long shortLinkStatsId;
    private long shortLinkId;
    private long orderId;
    private String referrer;
    private Date timeAdded;

    public long getShortLinkStatsId() {
        return shortLinkStatsId;
    }

    public void setShortLinkStatsId(long shortLinkStatsId) {
        this.shortLinkStatsId = shortLinkStatsId;
    }

    public long getShortLinkId() {
        return shortLinkId;
    }

    public void setShortLinkId(long shortLinkId) {
        this.shortLinkId = shortLinkId;
    }

    public long getOrderId() {
        return orderId;
    }

    public void setOrderId(long orderId) {
        this.orderId = orderId;
    }

    public String getReferrer() {
        return referrer;
    }

    public void setReferrer(String referrer) {
        this.referrer = referrer;
    }

    public Date getTimeAdded() {
        return timeAdded;
    }

    public void setTimeAdded(Date timeAdded) {
        this.timeAdded = timeAdded;
    }
}

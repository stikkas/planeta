package ru.planeta.commons.web.social.vk;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.log4j.Logger;
import ru.planeta.commons.model.Gender;
import ru.planeta.commons.web.VkRegistration;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import static ru.planeta.commons.lang.CollectionUtils.MapFunction;
import static ru.planeta.commons.lang.CollectionUtils.map;

/**
 * Client class for requesting http://api.vk.com. List of available methods can be found on
 * <a href="http://vk.com/dev/api_requests">http://vk.com/dev/api_requests</a>
 */
public class VkApiClient {

    protected final static Logger LOGGER = Logger.getLogger(VkApiClient.class);
    protected static final String CLIENT_SECRET_PARAM_NAME = "client_secret";
    private static final String METHOD_CALL_URL_STUB = "https://api.vk.com/method/";
    private static final String ACCESS_TOKEN_PARAM_NAME = "access_token";
    public static final String API_VERSION = "5.74"; //https://vk.com/dev/auth_sites

    /**
     * <a href="http://vk.com/dev/fields">list of available user profile fields</a>
     */
    public interface VkParams {
        String UID = "id";
        String FIRST_NAME = "first_name";
        String LAST_NAME = "last_name";
        String SEX = "sex";
        String BDATE = "bdate";
        String PHOTO_BIG = "photo_200";
        String SCREEN_NAME = "screen_name";
    }


    private static final List<String> USER_INFO_FIELDS =  Arrays.asList(VkParams.UID, VkParams.FIRST_NAME, VkParams.LAST_NAME, VkParams.SEX, VkParams.BDATE, VkParams.PHOTO_BIG,
        "city", "country", "timezone", "nickname", VkParams.SCREEN_NAME);  // may be redundant

    /**
     * <a href="http://vk.com/dev/friends.get">http://vk.com/dev/friends.get</a>
     *
     * @param clientVkUid friends "owner"
     */
    public List<VkRegistration> getFriends(long clientVkUid, int offset, int limit) {
        Map<String, Object> params = new HashMap<>();
        params.put("user_id", clientVkUid);
        params.put("fields", USER_INFO_FIELDS);
        params.put("count", limit);
        params.put("offset", offset);


        try {
            List<Map<String, Object>> userInfosJSON = (List<Map<String, Object> >) apiRequestOpen("friends.get", params);
            return map(userInfosJSON, new MapFunction<Map<String, Object>, VkRegistration>() {
                @Override
                public VkRegistration map(Map<String, Object> sourceObject) {
                    return parseJSON(sourceObject);
                }
            });
        } catch (Exception ex) {
            LOGGER.warn("friends.get failed", ex);
            return Collections.emptyList();
        }
    }

    /**
     * <a href="http://vk.com/dev/users.get">http://vk.com/dev/users.get</a>
     * @param uids - users to get info about
     * @return info about each vk user converted in UserCredentialsInfo format
     */
    public final List<VkRegistration> getUsers(List<Long> uids) {
        Map<String, Object> params = new HashMap<>();
        if (uids.size() > 1000) {
            LOGGER.error("Too much recipients for users.get: " + uids.size());
            return Collections.emptyList();
        }
        params.put("user_ids", uids);
        params.put("fields", USER_INFO_FIELDS);
        params.put("v", API_VERSION);
        try {
            Object o = apiRequest("users.get", params);
            if (o == null) {
                LOGGER.error("users.get failed apiRequestOpen has returned null");
                return Collections.emptyList();
            }
            if (!(o instanceof List)) {
                LOGGER.error("users.get failed apiRequestOpen has returned "  + o.getClass().toString());
                return Collections.emptyList();
            }
            List<Map<String, Object>> userInfosJSON = (List<Map<String, Object>>) o;
            return map(userInfosJSON, new MapFunction<Map<String, Object>, VkRegistration>() {
                @Override
                public VkRegistration map(Map<String, Object> sourceObject) {
                    return parseJSON(sourceObject);
                }
            });
        } catch (Exception ex) {
            LOGGER.error("users.get failed", ex);
            return Collections.emptyList();
        }
    }

    /**
     * Lightweight version of {@link #getUsers(java.util.List)} for single user
     * @param uid single user vk id
     * @return user info in our format
     */
    public final VkRegistration getUser(long uid) {
        List<VkRegistration> users = getUsers(Collections.singletonList(uid));
        if(users != null && !users.isEmpty()) {
            return users.get(0);
        }
        return null;
    }

    /**
     * Parse common csv-url Vk format
     *
     * @param vkUidsList comma-separated numbers in string
     * @return list of longs
     */
    public static List<Long> parseVkUids(String vkUidsList) {
        if(StringUtils.isEmpty(vkUidsList)) {
            return Collections.emptyList();
        }
        String[] strings = StringUtils.split(vkUidsList, ',');
        List<Long> result = new ArrayList<>();
        for (String value : strings) {
            Long parsed = NumberUtils.createLong(value);
            if (parsed != null) {
                result.add(parsed);
            }
        }
        return result;
    }



    /**
     * Sends api request to vk.com
     * <a href="http://vk.com/dev/api_requests">http://vk.com/dev/api_requests</a>
     *
     *
     * @param methodName full method name, with dot
     * @param parameters map of method parameters (list values will be converted in needed csv format)
     * @return parsed Json
     */
    public final Object apiRequest(String methodName, Map<String, Object> parameters) {
        if(getAccessToken() == null){
            return null;
        }
        parameters.put(ACCESS_TOKEN_PARAM_NAME, getAccessToken());
        parameters.put(CLIENT_SECRET_PARAM_NAME, getClientSecret());
        try {
            String requestUrl = createUrl(METHOD_CALL_URL_STUB + methodName, parameters);
            String rawResponse = downloadString(requestUrl);
            Map<String, Object> parsedResponse = parseRawResponse(rawResponse);
            if(!parsedResponse.isEmpty() && parsedResponse.get("response") != null) {
                return parsedResponse.get("response");
            }
        } catch (URISyntaxException | IOException e) {
            LOGGER.error(e);
        }
        return null;

    }

    /**
     * Methods that are don't require access_token
     * {@link #apiRequest(String, java.util.Map)}
     */
    public final Object apiRequestOpen(String methodName, Map<String, Object> parameters) throws URISyntaxException, IOException {
        // parameters.put(CLIENT_SECRET_PARAM_NAME, getClientSecret());
        String requestUrl = createUrl(METHOD_CALL_URL_STUB + methodName, parameters);
        String rawResponse = downloadString(requestUrl);
        Map<String, Object> parsedResponse = parseRawResponse(rawResponse);
        if (!parsedResponse.isEmpty() && parsedResponse.get("response") != null) {
            return parsedResponse.get("response");
        }
        throw new IOException("No response");
    }

    /**
     * Process URL building with CSV parameters like ..&uids=111,222,333&..
     * @throws URISyntaxException
     */
    private static String createUrl(String pathName, Map<String, Object> parameters) throws URISyntaxException {
        return new VkURIBuilder(pathName).addParameterMap(parameters).toString();
    }

    protected final Map<String, Object> parseRawResponse(String rawResponse) throws IOException {
        try {
            Map<String, Object> parsedResponse = new ObjectMapper().readValue(rawResponse, HashMap.class);
            if(parsedResponse == null) {
                return Collections.emptyMap();
            }
            Map<String, Object> error = (Map<String, Object>) parsedResponse.get("error");
            if(error != null) {
                int errCode = (Integer) error.get("error_code");
                LOGGER.error("VkException " + rawResponse);
                switch (errCode) {
                    case 15: // too frequently
                        break;
                    case 5: // invalid access token
                        invalidateAccessToken();
                        break;
                    default:
                        break;
                }
                return Collections.emptyMap();
            } else {
                return parsedResponse;
            }
        } catch (JsonProcessingException | ClassCastException processEx) {
            LOGGER.error(rawResponse, processEx);
        }
        return Collections.emptyMap();
    }

    protected static String downloadString(String requestUrl) throws IOException {
        return IOUtils.toString(new URL(requestUrl), StandardCharsets.UTF_8);
    }

    /**
     * Vk Api communication requires authorization http://vk.com/dev/authentication
     * @return apiId or client_id (<a href="http://vk.com/dev/auth_sites">http://vk.com/dev/auth_sites</a>)
     */
    public String getClientId() {
        return clientId;
    }

    /**
     * Vk Api communication requires authorization http://vk.com/dev/authentication
     * @return secret or client_secret (<a href="http://vk.com/dev/auth_server">http://vk.com/dev/auth_server</a>)
     */
    public String getClientSecret() {
        return clientSecret;
    }

    protected void invalidateAccessToken() {
        accessToken = null;
    }

    public String getAccessToken() {
        return accessToken;
    }

    private VkRegistration parseJSON(Map<String, Object> vkUserInfo) {
        String firstName = (String) vkUserInfo.get(VkParams.FIRST_NAME);
        String lastName = (String) vkUserInfo.get(VkParams.LAST_NAME);
        String uid = (vkUserInfo.get(VkParams.UID)).toString();
        String sex = (vkUserInfo.get(VkParams.SEX)).toString();
        String birthdayStr = (String) (vkUserInfo.get(VkParams.BDATE));
        String picture = (String) vkUserInfo.get(VkParams.PHOTO_BIG);
        String alias = (String) vkUserInfo.get(VkParams.SCREEN_NAME);
        boolean deactivated = vkUserInfo.get("deactivated") != null;
        final Date birthDate;

        VkRegistration vkRegistration = new VkRegistration();
        vkRegistration.setUsername(uid);
        vkRegistration.setAlias(alias);
        vkRegistration.setAvatarUrl(picture);
        vkRegistration.setGender(parseVkGender(sex));
        vkRegistration.setFirstName(firstName);
        vkRegistration.setLastName(lastName);
        vkRegistration.setDeactivated(deactivated);

        try {
            SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy");
            if (birthdayStr != null) {
                birthDate = format.parse(birthdayStr);
                vkRegistration.setBirthday(birthDate);
            }
        } catch (ParseException e) {
//                credentialsInfo.setUserBirthDate(new Date());
        }

        return vkRegistration;
    }

    public Gender parseVkGender(String sex) {
        return "2".equals(sex) ? Gender.MALE : Gender.FEMALE;
    }

    private String clientId;
    private String clientSecret;
    private String accessToken;

    public VkApiClient(String clientId, String clientSecret, String accessToken) {
        this.clientId = clientId;
        this.clientSecret = clientSecret;
        this.accessToken = accessToken;
    }

}

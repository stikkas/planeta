package ru.planeta.commons.text;

import org.apache.commons.lang3.StringUtils;
import ru.perm.kefir.bbcode.BBProcessorFactory;
import ru.perm.kefir.bbcode.TextProcessor;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Helper class for parsing bbcode.
 *
 * @author ameshkov
 */
public class Bbcode {

    private static final TextProcessor textProcessor = BBProcessorFactory.getInstance().create();
    private static final Pattern BBCODE_OPEN = Pattern.compile("\\[" + // open bbcode tag
            "([^\\[\\]]+?)" + // bbcode tagName (save it to use \1)
            "(?:(=|\\s).*?)?" + // possible tag attributes - starts with space or = (don't save it)
            "\\]" + // close bbcode
            ".*?" + // text(or bbcode) in tag
            "\\[/\\1\\]" // closing tag
            , Pattern.CASE_INSENSITIVE | Pattern.DOTALL);
    private static final Pattern BBCODE_VOID = Pattern.compile("\\[[^\\[\\]]+/\\]", Pattern.CASE_INSENSITIVE | Pattern.DOTALL);
    private static final Pattern URL_PATTERN = Pattern.compile("(((([A-Za-z]{3,9}\\:(?:\\/\\/)?)(?:[-;:&=\\+\\$,w]+@)?[A-Za-z0-9.-]+)((?:\\/[-\\+~%()\\/.\\u0430-\\u044f\\u0410-\\u042f\\w_]*)?(:?(:?\\?|!)(?:[-\\+=&;%@.\\w_]+))?#?(?:[\\w]*))?))", Pattern.CASE_INSENSITIVE);

    /**
     * Transforms input bbcode to html
     *
     * @param bbcode string containing bbcode
     * @return formatted html
     */
    public static String transform(String bbcode) {
        if (StringUtils.isEmpty(bbcode)) {
            return StringUtils.EMPTY;
        }
        String uniquePlaceholder = String.valueOf((new Date()).getTime());
        Map<String, String> bbcodes = new HashMap<String, String>();
        Map<String, String> emptyBbcodes = new HashMap<String, String>();
        String modified = getString(bbcode, BBCODE_OPEN, uniquePlaceholder, bbcodes, "open");
        String modified1 = getString(modified, BBCODE_VOID, uniquePlaceholder, emptyBbcodes, "void");

        String bbcodeWithParsedLinks = URL_PATTERN.matcher(modified1).replaceAll("[url]$1[/url]");
        for (Map.Entry<String, String> extracted : emptyBbcodes.entrySet()) {
            bbcodeWithParsedLinks = bbcodeWithParsedLinks.replace(extracted.getKey(), extracted.getValue());
        }
        for (Map.Entry<String, String> extracted : bbcodes.entrySet()) {
            bbcodeWithParsedLinks = bbcodeWithParsedLinks.replace(extracted.getKey(), extracted.getValue());
        }
        return textProcessor.process(bbcodeWithParsedLinks);
    }

    private static String getString(String bbcode, Pattern regexp, String uniquePlaceholder, Map<String, String> bbcodes, String prefix) {
        Matcher matcher = regexp.matcher(bbcode);
        StringBuffer sb = new StringBuffer();
        while (matcher.find()) {
            String placeHolder = uniquePlaceholder + prefix + StringUtils.leftPad(Integer.toString(bbcodes.size()), 4, "0");
            bbcodes.put(placeHolder, matcher.group());
            matcher.appendReplacement(sb, placeHolder);
        }
        matcher.appendTail(sb);
        return sb.toString();
    }

    /**
     * Strips all bbcode from the text
     *
     * @param text text to strip bbcode from
     * @return text without bbcode
     */
    public static String stripBbcode(String text) {
        if (StringUtils.isEmpty(text)) {
            return text;
        }

        return BBCODE_VOID.matcher(BBCODE_OPEN.matcher(text).replaceAll("")).replaceAll("");
    }

    /**
     * Checks if specified text contains bbcode
     *
     * @param text text to be inspected
     * @return true if text contains bbcode, false otherwise
     */
    public static boolean containsBbcode(String text) {
        return BBCODE_OPEN.matcher(text).find() || BBCODE_VOID.matcher(text).find();
    }

    public static String transformCampaignDescription(long profileId, String campaignDescription, String oldPhotoId, String newPhotoId) {
        //campaignDescription = campaignDescription.replaceFirst("\\[photo(\\s+([^\\[\\]]*))id=" + oldPhotoId + "(\\D)", "[photo$1id=" + newPhotoId + "$3");
        campaignDescription = campaignDescription.replaceFirst("(?is)\\[photo(\\s+([^\\[\\]]*))id=" + oldPhotoId + "(\\D)([^\\[\\]]*)owner=(\\d+)(\\D)",
                "[photo$1id=" + newPhotoId + "$3$4owner=" + String.valueOf(profileId) + "$6");
        return campaignDescription;
    }

    public static HashMap<String, String> getUrlMap(long currOwnerId, String campaignDescription) {
        Pattern pattern = Pattern.compile("\\[photo[^\\]]*id=(\\d+)\\D[^\\[\\]]*owner=(\\d+)[^\\]]*\\]([^\\[]*)\\[");
        Matcher matcher = pattern.matcher(campaignDescription);

        HashMap<String, String> photosUrlsMap = new HashMap<String, String>();
        while (matcher.find()) {
            String oldPhotoId = matcher.group(1);
            String photoUrl = matcher.group(3);
            String ownerId = matcher.group(2);
            if (currOwnerId != Long.valueOf(ownerId)) {
                photosUrlsMap.put(oldPhotoId, photoUrl);
            }
        }
        return photosUrlsMap;
    }
}

package ru.planeta.commons.lang;

import org.apache.commons.collections4.MapUtils;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * XML style string builder is a utility that creates simple XML documents.<br>
 * This example:
 * <pre>
 *     XmlStyleStringBuilder.create("campaign")
 *           .entity("name").text("Campaign name").up()
 *           .entity("time-start").text(DateUtils.formatToISO8601(new Date())).up()
 *           .entity("shares")
 *               .entity("share").attribute("id", 12L).text("share-1").up()
 *               .entity("share").attribute("id", 14L).text("share-2").up()
 *               .entity("share").attribute("id", 17L).text("share-3").up()
 *           .up()
 *           .entity("status-flags")
 *               .unclosedEntity("ignore-target-amount").up()
 *               .unclosedEntity("finish-on-target-reach").up()
 *           .up()
 *           .unclosedEntity("purchase-opts")
 *               .attribute("amount", 10)
 *               .attribute("purchase-count", 7)
 *               .attribute("purchase-sum", new BigDecimal(14).setScale(2))
 *           .prettyPrint();
 * </pre>
 * will generate following string:
 * <pre>
 * &lt;?xml version="1.0" encoding="UTF-8"?&gt;
 * &lt;campaign&gt;
 *     &lt;name&gt;Campaign name&lt;/name&gt;
 *     &lt;time-stagt&gt;2013-12-03T12:24:17.170+02:00&lt;/time-stagt&gt;
 *     &lt;shares&gt;
 *         &lt;share id="12"&gt;share-1&lt;/share&gt;
 *         &lt;share id="14"&gt;share-2&lt;/share&gt;
 *         &lt;share id="17"&gt;share-3&lt;/share&gt;
 *     &lt;/shares&gt;
 *     &lt;status-flags&gt;
 *         &lt;ignore-target-amount/&gt;
 *         &lt;finish-on-target-reach/&gt;
 *     &lt;/status-flags&gt;
 *     &lt;purchase-opts amount="10" purchase-count="7" purchase-sum="14.00"/&gt;
 * &lt;/campaign&gt;
 * </pre>
 *
 * User: eshevchenko
 */
public class XmlStyleStringBuilder {

    public static class Entity {

        private Entity parent;
        private List<Entity> children;

        private String name;
        private Map<String, Object> attributes;
        private boolean unclosed;
        private String text;

        protected Entity(Entity parent, String name, boolean unclosed) {
            this.parent = parent;
            this.name = name;
            this.unclosed = unclosed;
        }

        public static Entity create(Entity parent, String name) {
            return new Entity(parent, name, false);
        }

        public static Entity createRoot(String name) {
            return create(null, name);
        }

        public static Entity createUnclosed(Entity parent, String name) {
            return new Entity(parent, name, true);
        }

        public static Entity createUnclosedRoot(String name) {
            return createUnclosed(null, name);
        }

        private Entity addChild(Entity entity) {
            if (isUnclosed()) {
                throw new UnsupportedOperationException("Can't add child entity to unclosed element.");
            }
            if (org.apache.commons.lang3.StringUtils.isNotEmpty(text)) {
                throw new UnsupportedOperationException("Can't add child entity to non container element.");
            }

            if (children == null) {
                children = new ArrayList<Entity>();
            }
            children.add(entity);
            return entity;
        }

        public Entity addChild(String name) {
            return addChild(create(this, name));
        }

        public Entity addUnclosedChild(String name) {
            return addChild(createUnclosed(this, name));
        }

        public Entity addAttribute(String name, Object value) {
            if (attributes == null) {
                attributes = new LinkedHashMap<String, Object>();
            }
            attributes.put(name, value);
            return this;
        }

        public void setText(String text) {
            if (isUnclosed()) {
                throw new UnsupportedOperationException("Can't set text entity to unclosed element.");
            }
            if (children != null) {
                throw new UnsupportedOperationException("Can't set text entity value to container element.");
            }
            this.text = text;
        }

        public Entity getParent() {
            return parent;
        }

        public String getName() {
            return name;
        }

        public boolean isUnclosed() {
            return unclosed;
        }

        public boolean isRoot() {
            return (getParent() == null);
        }

        private String formatContent() {
            StringBuilder result = new StringBuilder();
            if (children != null) {
                for (Entity child : children) {
                    result.append(child);
                }
            } else if (org.apache.commons.lang3.StringUtils.isNotEmpty(text)) {
                result.append(text);
            }
            return result.toString();
        }

        private String formatAttributes() {
            StringBuilder builder = new StringBuilder();
            if (MapUtils.isNotEmpty(attributes)) {
                for (String name : attributes.keySet()) {
                    builder.append(String.format(" %s=\"%s\"", name, attributes.get(name)));
                }
            }
            return builder.toString();
        }

        @Override
        public String toString() {
            return isUnclosed()
                    ? String.format("<%s%s />", getName(), formatAttributes())
                    : String.format("<%s%s>%s</%s>", name, formatAttributes(), formatContent(), name);
        }
    }

    protected static final String XML_HEADER = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";

    private Entity root;
    private Entity current;

    protected XmlStyleStringBuilder(Entity root) {
        this.root = root;
        this.current = root;
    }

    public static XmlStyleStringBuilder create(String name) {
        return new XmlStyleStringBuilder(Entity.createRoot(name));
    }

    public static XmlStyleStringBuilder createUnclosed(String name) {
        return new XmlStyleStringBuilder(Entity.createUnclosedRoot(name));
    }

    protected Entity getRoot() {
        return root;
    }

    protected Entity getCurrent() {
        return current;
    }

    private XmlStyleStringBuilder changeCurrent(Entity another) {
        current = another;
        return this;
    }

    public XmlStyleStringBuilder entity(String name) {
        return changeCurrent(getCurrent().addChild(name));
    }

    public XmlStyleStringBuilder unclosedEntity(String name) {
        return changeCurrent(getCurrent().addUnclosedChild(name));
    }

    public XmlStyleStringBuilder attribute(String name, Object value) {
        getCurrent().addAttribute(name, value);
        return this;
    }

    public XmlStyleStringBuilder text(Object text) {
        getCurrent().setText(text.toString());
        return this;
    }

    public XmlStyleStringBuilder up() {
        if (getCurrent().isRoot()) {
            throw new UnsupportedOperationException("Can't go higher than root entity.");
        }
        return changeCurrent(getCurrent().getParent());
    }

    @Override
    public String toString() {
        return XML_HEADER + getRoot();
    }
}
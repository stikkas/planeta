package ru.planeta.commons.math;

/**
 * Utils for calculating video or thumbnail sizes
 */
public class SizeUtils {

    /**
     * Resizes the specified rectangle to the specified height saving aspect.
     *
     * @param width        Source width
     * @param height       Source height
     * @param targetHeight Destination height
     * @param round        Value which the width/height should be divisible by.
     * @return
     */
    public static Size resizeHeight(int width, int height, int targetHeight, int round) {

        if (targetHeight == height) return new Size(width, height);
        int targetWidth = round((int) ((float) width / (float) height * (float) targetHeight), round);
        return new Size(targetWidth, targetHeight);
    }

    /**
     * Resizes the specified rectangle to the specified width saving aspect
     *
     * @param width       Source width
     * @param height      Source height
     * @param targetWidth Destination width
     * @param round       Value which the width/height should be divisible by
     * @return
     */
    public static Size resizeWidth(int width, int height, int targetWidth, int round) {

        if (targetWidth == width) return new Size(width, height);
        int targetHeight = round((int) ((float) height / (float) width * (float) targetWidth), round);
        return new Size(targetWidth, targetHeight);
    }

    /**
     * Fits the specified rectangle into the specified container saving aspect
     *
     * @param width
     * @param height
     * @param containerWidth
     * @param containerHeight
     * @return
     */
    public static Size fit(int width, int height, int containerWidth, int containerHeight) {

        if (width <= containerWidth && height <= containerHeight) return new Size(width, height);
        float containerAspect = (float) containerWidth / (float) containerHeight;
        float aspect = (float) width / (float) height;

        if (Math.abs(containerAspect - aspect) < 1e-6) {
            return width < containerWidth ? new Size(width, height) : new Size(containerWidth, containerHeight);
        } else if (containerAspect > aspect) {
            return resizeHeight(width, height, containerHeight, 1);
        } else {
            return resizeWidth(width, height, containerWidth, 1);
        }
    }

    /**
     * Finds the closest value divisible by round.
     *
     * @param value
     * @param round
     * @return
     */
    private static int round(int value, int round) {
        for (int i = 0; i <= round; i++) {
            if (value - i > 0 && (((value - i) % round) == 0)) {
                return value - i;
            }
        }

        return value;
    }


    public static class Size {
        private final int width;
        private final int height;

        public Size(int width, int height) {
            this.width = width;
            this.height = height;
        }

        public int getWidth() {
            return width;
        }

        public int getHeight() {
            return height;
        }
    }
}

package ru.planeta.commons.generator;

import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

/**
 * User: sshendyapin
 * Date: 03.09.12
 * Time: 17:16
 */
public class TldGenerator {

    private static final String HELPER_FUNCTION_PATTERN = "helper-function-pattern.tld";
    private static Logger log = Logger.getLogger(TldGenerator.class);

    /**
     * Iterates though the given classes and generates functions for all public static methods.
     * Also takes <code>helper-function-pattern.tld</code> with <code>Header</code> and <code>Footer</code> .
     *
     *
     * @param classes
     * @return String   representation of the generated tld file
     */
    public static String generateMethods(Class... classes) {
        StringBuilder sb = new StringBuilder();
        String xmlContent = "";
        try {
            xmlContent = parseXmlToString(HELPER_FUNCTION_PATTERN);
        } catch (IOException e) {
            log.error("IOException", e);
        }

        for (Class c : classes) {
            Method[] methods = c.getMethods();
            for (Method method : methods) {
                int mods = method.getModifiers();
                if (Modifier.isStatic(mods)) {
                    generateMethod(c, method, sb);
                }
            }
        }
        return xmlContent.replace("${content}", sb.toString());
    }

    /**
     * Generates tld function from the given class and method, and appends it to the given StringBuilder
     *
     * @param c
     * @param method
     * @param sb
     */
    private static void generateMethod(Class c, Method method, StringBuilder sb) {
        sb.append("<function>\n\t<name>");
        sb.append(method.getName());
        sb.append("</name>\n\t<function-class>\n\t\t");
        sb.append(c.getName());
        sb.append("\n\t</function-class>\n");
        sb.append("\t<function-signature>\n\t\t");
        // checks if this method returns an Array of Objects
        if (method.getReturnType().isArray()) {
            if (!method.getReturnType().isPrimitive()){
                sb.append(method.getReturnType().getComponentType().getName());
                sb.append("[]");
            } else {
                sb.append(method.getReturnType().getComponentType());
                sb.append("[]");
            }
        } else {
            String[] splittedMethodReturnType = method.getReturnType().toString().split("\\s");
            if (splittedMethodReturnType.length > 1) {
                sb.append(splittedMethodReturnType[1]);
            } else {
                sb.append(method.getReturnType());
            }
        }
        sb.append(" ");
        sb.append(method.getName());
        // generates method's parameters
        Class[] paramTypes = method.getParameterTypes();
        sb.append("(");
        for (int i = 0; i < paramTypes.length; i++) {
            sb.append(paramTypes[i].getName());
            if (i < paramTypes.length - 1){
                sb.append(", ");
            }
        }
        sb.append(")\n");
        sb.append("\t</function-signature>\n");
        sb.append("</function>\n\n");
    }

    /**
     * Parsing given xml file from the given <code>sourcePath</code> to String.
     *
     * @param sourcePath
     * @return
     * @throws IOException
     */
    private static String parseXmlToString(String sourcePath) throws IOException {
        return IOUtils.toString(TldGenerator.class.getClassLoader().getResourceAsStream(sourcePath));
    }
}

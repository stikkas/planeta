package ru.planeta.commons.onlinekassa;

import java.util.HashMap;
import java.util.Map;

/**
 * Типы онлайн-чеков
 *
 * Тип	            Признак расчета	        Применение
 * ----------------------------------------------------------------------------------------------------------------
 * Income	        Приход	                Выдается при получении средств от покупателя (клиента)
 * IncomeReturn	    Возврат прихода	        Выдается при возврате покупателю (клиенту) средств, полученных от него
 * Expense	        Расход	                Выдается при выдаче средств покупателю (клиенту)
 * ExpenseReturn	    Вовзрат расхода	        Выдается при получениеи средств от покупателя (клиента), выданных ему
 *
 * https://cloudpayments.ru/Docs/Directory#receipt-type
 */
public enum ReceiptType {
    INCOME("Income"),
    INCOMERETURN("IncomeReturn"),
    EXPENSE("Expense"),
    EXPENSERETURN("ExpenseReturn");

    private String text;

    ReceiptType(String text) {
        this.text = text;
    }

    @Override
    public String toString() {
        return text;
    }

    private static Map<String, ReceiptType> lookup = new HashMap<>();

    static {
        for (ReceiptType s : values()) {
            lookup.put(s.toString(), s);
        }
    }

    public static ReceiptType getByValue(String text) {
        return lookup.get(text);
    }
}

package ru.planeta.commons.lang;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.collections4.MapUtils;
import org.apache.log4j.Logger;
import org.springframework.util.Assert;

import java.util.*;

/**
 * JSON style string builder.<br>
 * Provides methods for constructing JSON-string.<br>
 * User: eshevchenko
 */
public class JsonStyleStringBuilder {

    private static final Logger LOGGER = Logger.getLogger(JsonStyleStringBuilder.class);

    private final Map<String, Object> params = new LinkedHashMap<String, Object>();

    public JsonStyleStringBuilder addAttribute(String name, Object value) {
        Assert.hasText(name, "Attribute name can't be empty string.");
        params.put(name, value);

        return this;
    }

    public JsonStyleStringBuilder addAttributes(Map<String, ?> attributes) {
        if (!MapUtils.isEmpty(attributes)) {
            params.putAll(attributes);
        }

        return this;
    }

    public JsonStyleStringBuilder addArray(String name, Collection<?> collection) {
        return addAttribute(name, collection);
    }

    public JsonStyleStringBuilder addArray(String name, Enumeration<?> enumeration) {
        Collection<Object> collection = new ArrayList<Object>();
        while (enumeration.hasMoreElements()) {
            Object element = enumeration.nextElement();
            collection.add(element);
        }

        return addArray(name, collection);
    }

    public JsonStyleStringBuilder addNestedObject(String name) {
        JsonStyleStringBuilder result = new JsonStyleStringBuilder();
        addAttribute(name, result);

        return result;
    }

    private static boolean isAssignableFrom(Class<?> clazz, Object object) {
        return (object != null) && clazz.isAssignableFrom(object.getClass());
    }

    private static Object normalize(Object object) {
        Object result = object;
        if (isAssignableFrom(JsonStyleStringBuilder.class, object)) {
            result = normalizeParameters((JsonStyleStringBuilder) object);
        } else if (isAssignableFrom(Collection.class, object)) {
            result = normalizeCollection((Collection) object);
        }

        return result;
    }

    private static Collection<?> normalizeCollection(Collection<?> collection) {
        Collection<Object> result = new ArrayList<Object>();
        for (Object element : collection) {
            result.add(normalize(element));
        }

        return result;
    }

    private static Map<String, Object> normalizeParameters(JsonStyleStringBuilder printer) {
        Map<String, Object> result = new LinkedHashMap<String, Object>();
        for (Map.Entry<String, Object> param : printer.params.entrySet()) {
            result.put(param.getKey(), normalize(param.getValue()));
        }

        return result;
    }

    @Override
    public String toString() {
        try {
            Map<String, Object> normalized = normalizeParameters(this);
            return new ObjectMapper().writer().writeValueAsString(normalized);
        } catch (Exception e) {
            LOGGER.error("Formatting error:", e);
            return printError("formattingError", e);
        }
    }

    public static String printError(String name, Throwable t) {
        return new JsonStyleStringBuilder()
                .addAttribute(name, t.getClass().getName())
                .toString();
    }

    public boolean isEmpty() {
        return params.isEmpty();
    }
}

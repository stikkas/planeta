package ru.planeta.commons.external.oembed.provider;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.log4j.Logger;
import ru.planeta.commons.external.oembed.OembedInfo;
import ru.planeta.commons.web.WebUtils;

import java.io.IOException;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created with IntelliJ IDEA.
 * User: connanedoile
 * Date: 4/4/12
 * Time: 5:19 PM
 */
public class Vimeo implements MediaService{

    private final static Logger log = Logger.getLogger(Vimeo.class);
    private final static Pattern VIMEO_PATTERN = Pattern.compile("(.*(vimeo.com/([0-9]*))).*$", Pattern.CASE_INSENSITIVE);
    private final static String API_URL = "https://vimeo.com/api/oembed.json?autoplay=true&url={url}";
    private final static ObjectMapper MAPPER = new ObjectMapper();

    /**
     * Checks if the specified url is youtube video url
     *
     */
    public boolean isServiceUrl(String url) {
        if (StringUtils.isEmpty(url)) {
            return false;
        }
        Matcher matcher = VIMEO_PATTERN.matcher(url);
        return matcher.find();
    }

	@Override
	public String getNaturalUrl(String url) {
		return "https://vimeo.com/"+extractVimeoId(url);
	}

	@Override
	public OembedInfo getInfo(final String url) {
        OembedInfo vimeoInfo = new OembedInfo();
        String downloadUrl = API_URL.replace("{url}", url);
        String json = WebUtils.downloadString(downloadUrl);

        if (StringUtils.isEmpty(json)) {
            return null;
        }

        try {
            Map messages = MAPPER.readValue(json, Map.class);
            vimeoInfo.setId(messages.get("video_id").toString());
			vimeoInfo.setVideoType(VideoType.VIMEO);
            vimeoInfo.setNaturalUrl(getNaturalUrl(url));
			vimeoInfo.setName(messages.get("title").toString());
            vimeoInfo.setDescription(messages.get("description").toString());
            vimeoInfo.setThumbnailUrl(messages.get("thumbnail_url").toString());
            vimeoInfo.setDuration(NumberUtils.toInt(messages.get("duration").toString()));
            vimeoInfo.setHtml(messages.get("html").toString().trim());
            vimeoInfo.setWidth(NumberUtils.toInt(messages.get("width").toString()));
            vimeoInfo.setHeight(NumberUtils.toInt(messages.get("height").toString()));
            return vimeoInfo;
        } catch (IOException e) {
            log.debug("Exception while parsing " + url + " vimeo url", e);
            return null;
        }
    }

    /**
     * Extracts youtube video identifier from the specified url.
     * If wrong url (cannot extract or non-youtube) -- returns null.
     *
     */
    public static String extractVimeoId(String url) {
        try {
            Matcher matcher = VIMEO_PATTERN.matcher(url);
            if (!matcher.find()) {
                return null;
            }

            String id = matcher.group(3);
            return StringUtils.isEmpty(id) ? null : id;
        } catch (Exception ex) {
            log.warn("Cannot extract vimeo id from: " + url, ex);
        }

        return null;
    }

}

package ru.planeta.commons.lang;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.charset.CharsetEncoder;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/*
* *
 * Created by Alexey on 26.10.2015.
 */

public class RegexProperty implements CharSequence {

    public static String propertieFilePath = "C:\\Project\\Server\\Web\\planeta-static\\src\\main\\resources\\locale\\sourses.properties";
    public static String newFilePath = "C:\\Project\\Server\\Web\\planeta-static\\src\\main\\webapp\\WEB-INF\\jsp\\international-templates\\";
    public static String oldFilePath = "C:\\Project\\Server\\Web\\planeta-static\\src\\main\\webapp\\templates\\campaigns.html";
    public static String newFileNameWithExtension = "campaigns.jsp";
    public static String propertieText;

    public static void main(String[] args) throws IOException {
        File file = new File(oldFilePath);
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new InputStreamReader(new FileInputStream(file)));
        }
        catch (FileNotFoundException e1) {
            System.out.println("File not found!");
            System.exit(0);
        }

        String str;
        long propertieCounter = 1;
        PrintWriter newFile = createNewFile();
        PrintWriter propertieFile = openPropertieFile();

        while((str = reader.readLine()) != null) {
            String propertieName = newFileNameWithExtension + ".propertie." + propertieCounter;
            String newRow = checkWithRegExp(str, propertieName);

            if(newRow != null){
                newFile.println(newRow);

                final CharsetEncoder asciiEncoder = Charset.forName("US-ASCII").newEncoder();
                final StringBuilder result = new StringBuilder();
                for (final Character character : propertieText.toCharArray()) {
                    if (asciiEncoder.canEncode(character)) {
                        result.append(character);
                    } else {
                        result.append("\\u");
                        result.append(Integer.toHexString(0x10000 | character).substring(1).toUpperCase());
                    }
                }

                propertieFile.println(result);
                propertieCounter++;
            }else{
                newFile.println(str);
            }
        }

        reader.close();
        newFile.close();
        propertieFile.close();
    }

    public static String checkWithRegExp(String row, String propertieName){
        Pattern p = Pattern.compile("(\\s[\\u0430-\\u044f\\u0410-\\u042f]|>|\"|'){1}([\\u0430-\\u044f\\u0410-\\u042f\\u0451\\u0401nbsp& :;\\.\\\",«»!?_\\/\\\\-]+)(<|\"|'|$)");
        Matcher m = p.matcher(row);
        if(m.find()) {
            propertieText = propertieName + "=" + m.group(2);
            return m.replaceAll("$1<spring:message code=\"" + propertieName + "\" text=\"default text\"> </spring:message>$3");
        }
        return null;
    }

    public static PrintWriter createNewFile() {
        try {
            newFilePath = newFilePath + newFileNameWithExtension;
            return new PrintWriter(new OutputStreamWriter(new FileOutputStream(newFilePath, true), "UTF-8"));
        } catch(IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static PrintWriter openPropertieFile() {
        try {
            return new PrintWriter(new OutputStreamWriter(new FileOutputStream(propertieFilePath, true), "UTF-8"));
        } catch(IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public int length() {
        return 0;
    }

    @Override
    public char charAt(int index) {
        return 0;
    }

    @Override
    public CharSequence subSequence(int start, int end) {
        return null;
    }
}

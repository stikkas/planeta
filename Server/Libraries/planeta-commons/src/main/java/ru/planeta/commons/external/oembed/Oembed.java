package ru.planeta.commons.external.oembed;

import ru.planeta.commons.external.oembed.provider.MediaService;
import ru.planeta.commons.external.oembed.provider.Vimeo;
import ru.planeta.commons.external.oembed.provider.Youtube;

import java.util.HashSet;
import java.util.Set;

/**
 * User: connanedoile
 * Date: 4/4/12
 * Time: 4:38 PM
 */
public class Oembed {
    private static final Set<MediaService> mediaServices = new HashSet<>();

    static {
        mediaServices.add(new Youtube());
        mediaServices.add(new Vimeo());
    }

    public static OembedInfo getExternalVideoInfo(String url) {
        for (MediaService service : mediaServices) {
            if (service.isServiceUrl(url)) {
                return service.getInfo(service.getNaturalUrl(url));
            }
        }
        return null;
    }


    public static String getNaturalUrl(String url) {
        for (MediaService service : mediaServices) {
            if (service.isServiceUrl(url)) {
                return service.getNaturalUrl(url);
            }
        }
        return null;
    }
}

package ru.planeta.commons.geo;

/**
 *
 * @author atropnikov
 * Class IpBlock
 */
public class IpBlock {

    private long startIp;
    private long endIp;
    private int locationId;

    public void setStartIp(long startIp) {
        this.startIp = startIp;
    }

    public void setEndIp(long endIp) {
        this.endIp = endIp;
    }

    public void setLocationId(int locationId) {
        this.locationId = locationId;
    }

    public IpBlock() {

    }

    public IpBlock(long startIp, long endIp, int locationId) {
        this.startIp = startIp;
        this.endIp = endIp;
        this.locationId = locationId;
    }

    public long getStartIp() {
        return startIp;
    }

    public long getEndIp() {
        return endIp;
    }

    public int getLocationId() {
        return locationId;
    }
}

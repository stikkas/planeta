package ru.planeta.commons.web.filters;

import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import javax.servlet.WriteListener;

/**
 * Class HttpHeadFilter handles HEAD requests by intercepting and wrapping them into GET requests
 * User: m.shulepov
 * Date: 10.07.12
 * Time: 15:41
 */
public class HttpHeadFilter extends OncePerRequestFilter {

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        if ("HEAD".equals(request.getMethod())) {
            NoBodyResponseWrapper noBodyResponseWrapper = new NoBodyResponseWrapper(response);

            filterChain.doFilter(new HttpMethodWrapper("GET", request), noBodyResponseWrapper);
            noBodyResponseWrapper.setContentLength();
        } else {
            filterChain.doFilter(request, response);
        }
    }

    private static class HttpMethodWrapper extends HttpServletRequestWrapper {
        private final String method;

        /**
         * Constructs a request object wrapping the given request.
         * @throws IllegalArgumentException if the request is null
         */
        public HttpMethodWrapper(String method, HttpServletRequest request) {
            super(request);
            this.method = method;
        }

        @Override
        public String getMethod() {
            return method;
        }
    }

    @Deprecated
    private static class NoBodyResponseWrapper extends HttpServletResponseWrapper {
        private final NoBodyOutputStream noBodyOutputStream = new NoBodyOutputStream();
        private PrintWriter writer;

        /**
         * Constructs a response adaptor wrapping the given response.
         * @throws IllegalArgumentException if the response is null
         */
        public NoBodyResponseWrapper(HttpServletResponse response) {
            super(response);
        }

        @Override
        public ServletOutputStream getOutputStream() throws IOException {
            return noBodyOutputStream;
        }

        @Override
        public PrintWriter getWriter() throws IOException {
            if (writer == null) {
                writer = new PrintWriter(new OutputStreamWriter(noBodyOutputStream, getCharacterEncoding()));
            }
            return writer;
        }

        public void setContentLength() {
            super.setContentLength(noBodyOutputStream.getContentLength());
        }
    }

    @Deprecated
    private static class NoBodyOutputStream extends ServletOutputStream {
        private int contentLength = 0;

        public int getContentLength() {
            return contentLength;
        }

        @Override
        public void write(int b) throws IOException {
            contentLength++;
        }

        @Override
        public void write(byte[] b, int off, int len) throws IOException {
            contentLength += len;
        }

        @Override
        public boolean isReady() {
            return true;
        }

        @Override
        public void setWriteListener(WriteListener wl) {
        }

    }

}

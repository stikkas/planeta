package ru.planeta.commons.web.filters;

import org.apache.commons.lang3.StringUtils;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;

/**
 * JSONP Callback filter. Intercepts requests and wraps response in jsonp callback if "callback" parameter is present
 * <p/>
 * User: m.shulepov
 * Date: 05.04.12
 * Time: 13:50
 */
public class JsonpCallbackFilter implements Filter {

    private static final String DEFAULT_CALLBACK_PARAM = "callback";
    private static final byte[] CALLBACK_OPEN = "(".getBytes(StandardCharsets.UTF_8);
    private static final byte[] CALLBACK_CLOSE = ");".getBytes(StandardCharsets.UTF_8);

    private String jsonpCallbackParameterName = DEFAULT_CALLBACK_PARAM;


    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    public void setJsonpCallbackParameterName(String jsonpCallbackParameterName) {
        this.jsonpCallbackParameterName = jsonpCallbackParameterName;
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;

        String callback = request.getParameter(jsonpCallbackParameterName);

        if (StringUtils.isEmpty(callback)) {
            chain.doFilter(servletRequest, servletResponse);
            return;
        }

        OutputStream out = response.getOutputStream();
        GenericResponseWrapper wrapper = new GenericResponseWrapper(response);

        chain.doFilter(request, wrapper);

        response.setContentType("application/json; charset=utf-8");

        out.write(callback.getBytes(StandardCharsets.UTF_8));
        out.write(CALLBACK_OPEN);
        writeInner(out, wrapper.getData());
        out.write(CALLBACK_CLOSE);

        out.close();
    }

    protected void writeInner(OutputStream out, byte[] data) throws IOException {
        out.write(data);
    }

    @Override
    public void destroy() {
    }
}
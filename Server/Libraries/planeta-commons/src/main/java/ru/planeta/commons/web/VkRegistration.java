package ru.planeta.commons.web;

import ru.planeta.commons.model.Gender;

import java.util.Date;


public class VkRegistration {

    public RegistrationData getRegistrationData() {
        return registrationData;
    }

    public void setRegistrationData(RegistrationData registrationData) {
        this.registrationData = registrationData;
    }

    private RegistrationData registrationData = new RegistrationData();

    private String username;
    private String alias;
    private boolean deactivated = false;

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getFirstName() {
        return registrationData.getFirstName();
    }

    public void setFirstName(String firstName) {
        registrationData.setFirstName(firstName);
    }

    public String getLastName() {
        return registrationData.getLastName();
    }

    public void setLastName(String lastName) {
        registrationData.setLastName(lastName);
    }

    public Gender getGender() {
        return registrationData.getGender();
    }

    public void setGender(Gender gender) {
        registrationData.setGender(gender);
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Date getBirthday() {
        return registrationData.getUserBirthDate();
    }

    public void setBirthday(Date birthday) {
        registrationData.setUserBirthDate(birthday);
    }

    public String getAvatarUrl() {
        return registrationData.getPhotoUrl();
    }

    public void setAvatarUrl(String avatarUrl) {
        registrationData.setPhotoUrl(avatarUrl);
    }

    public boolean isDeactivated() {
        return deactivated;
    }

    public void setDeactivated(boolean deactivated) {
        this.deactivated = deactivated;
    }
}

package ru.planeta.commons.concurrent;

import java.util.EnumSet;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * Represents simple lock with lock upgrading support
 */
public class Lock {

    private final ReentrantReadWriteLock readWriteLock = new ReentrantReadWriteLock();
    private Map<Thread, EnumSet<LockMode>> threadLockModes = new ConcurrentHashMap<Thread, EnumSet<LockMode>>();

    /**
     * Acquires this lock in shared mode
     */
    public boolean acquireSharedLock() {

        if (threadLockModes.containsKey(Thread.currentThread())) {
            // Lock is already acquired
            return true;
        }

        readWriteLock.readLock().lock();
        threadLockModes.put(Thread.currentThread(), EnumSet.of(LockMode.SHARED));
        return true;
    }

    /**
     * Acquired this lock in exclusive mode
     */
    public boolean acquireExclusiveLock() {

        EnumSet<LockMode> currentLockModes = threadLockModes.get(Thread.currentThread());
        if (currentLockModes == null) {
            readWriteLock.writeLock().lock();
            threadLockModes.put(Thread.currentThread(), EnumSet.of(LockMode.EXCLUSIVE));
            return true;
        } else if (currentLockModes.contains(LockMode.EXCLUSIVE)) {
            // Lock is already acquired by this thread in exclusive mode
            return true;
        } else {
            // Unlocking and than locking exclusively
            readWriteLock.readLock().unlock();
            readWriteLock.writeLock().lock();
            currentLockModes.add(LockMode.EXCLUSIVE);
            return true;
        }
    }

    /**
     * Releases shared lock
     */
    public boolean releaseSharedLock() {

        EnumSet<LockMode> currentLockModes = threadLockModes.get(Thread.currentThread());
        if (currentLockModes == null ||
                !currentLockModes.contains(LockMode.SHARED) ||
                currentLockModes.contains(LockMode.EXCLUSIVE)) {
            // Doing nothing is currentLockModes contains exclusive lock or does not exist at all
            return true;
        }

        readWriteLock.readLock().unlock();
        currentLockModes.remove(LockMode.SHARED);
        if (currentLockModes.size() == 0) {
            threadLockModes.remove(Thread.currentThread());
        }
        return true;
    }

    /**
     * Releases exclusive lock
     */
    public boolean releaseExclusiveLock() {
        EnumSet<LockMode> currentLockModes = threadLockModes.get(Thread.currentThread());
        if (currentLockModes == null || !currentLockModes.contains(LockMode.EXCLUSIVE)) {
            // No exclusive lock
            return true;
        }

        readWriteLock.readLock().lock();
        readWriteLock.writeLock().unlock();
        currentLockModes.remove(LockMode.EXCLUSIVE);
        if (currentLockModes.size() == 0) {
            readWriteLock.readLock().unlock();
            threadLockModes.remove(Thread.currentThread());
        }
        return true;
    }

    /**
     * Releases lock of the specified lock mode
     *
     * @param lockMode
     */
    public boolean release(LockMode lockMode) {
        if (lockMode == LockMode.SHARED) {
            return releaseSharedLock();
        } else {
            return releaseExclusiveLock();
        }
    }

    /**
     * Releases all locks
     */
    public boolean release() {

        EnumSet<LockMode> currentLockModes = threadLockModes.get(Thread.currentThread());
        if (currentLockModes == null) {
            return true;
        }

        if (currentLockModes.contains(LockMode.EXCLUSIVE)) {
            return releaseExclusiveLock();
        }

        if (currentLockModes.contains(LockMode.SHARED)) {
            return releaseSharedLock();
        }

        return true;
    }

    /**
     * Returns count of threads holding this lock in any mode
     *
     * @return
     */
    public int getCountOfLockHolders() {
        return threadLockModes.size();
    }
}

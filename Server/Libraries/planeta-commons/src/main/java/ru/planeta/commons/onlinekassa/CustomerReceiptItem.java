package ru.planeta.commons.onlinekassa;

import java.math.BigDecimal;

//https://cloudpayments.ru/docs/api/kassa#auth
public class CustomerReceiptItem {
    private String label;
    private BigDecimal price = BigDecimal.ZERO;
    private int quantity;
    private BigDecimal amount = BigDecimal.ZERO;
    private Integer vat;
    private String ean13;

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public Integer getVat() {
        return vat;
    }

    /**
     *
     * @param vat Значения ставки НДС:
     *            null или не указано — НДС не облагается
     *            18 — НДС 18%
     *            10 — НДС 10%
     *            0 — НДС 0%
     *            110 — расчетный НДС 10/110
     *            118 — расчетный НДС 18.118
     *            При указании ставки НДС будьте внимательны: "НДС 0%" и "НДС не облагается" — это не равнозначные варианты.
     */
    public void setVat(Integer vat) {
        this.vat = vat;
    }

    public String getEan13() {
        return ean13;
    }

    public void setEan13(String ean13) {
        this.ean13 = ean13;
    }
}

package ru.planeta.commons.text;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Porter Stemmer algorithm that extracts word base from any given word. This implementation works only for cyrillic symbols
 *
 * User: m.shulepov
 * Date: 03.04.12
 * Time: 14:29
 */
public class Stemmer {

    private static final Pattern PERFECTIVEGROUND = Pattern.compile("((\\u0438\\u0432|\\u0438\\u0432\\u0448\\u0438|\\u0438\\u0432\\u0448\\u0438\\u0441\\u044c|\\u044b\\u0432|\\u044b\\u0432\\u0448\\u0438|\\u044b\\u0432\\u0448\\u0438\\u0441\\u044c)|((?<=[\\u0430\\u044f])(\\u0432|\\u0432\\u0448\\u0438|\\u0432\\u0448\\u0438\\u0441\\u044c)))$");

    private static final Pattern REFLEXIVE = Pattern.compile("(\\u0441[\\u044f\\u044c])$");

    private static final Pattern ADJECTIVE = Pattern.compile("(\\u0435\\u0435|\\u0438\\u0435|\\u044b\\u0435|\\u043e\\u0435|\\u0438\\u043c\\u0438|\\u044b\\u043c\\u0438|\\u0435\\u0439|\\u0438\\u0439|\\u044b\\u0439|\\u043e\\u0439|\\u0435\\u043c|\\u0438\\u043c|\\u044b\\u043c|\\u043e\\u043c|\\u0435\\u0433\\u043e|\\u043e\\u0433\\u043e|\\u0435\\u043c\\u0443|\\u043e\\u043c\\u0443|\\u0438\\u0445|\\u044b\\u0445|\\u0443\\u044e|\\u044e\\u044e|\\u0430\\u044f|\\u044f\\u044f|\\u043e\\u044e|\\u0435\\u044e)$");

    private static final Pattern PARTICIPLE = Pattern.compile("((\\u0438\\u0432\\u0448|\\u044b\\u0432\\u0448|\\u0443\\u044e\\u0449)|((?<=[\\u0430\\u044f])(\\u0435\\u043c|\\u043d\\u043d|\\u0432\\u0448|\\u044e\\u0449|\\u0449)))$");

    private static final Pattern VERB = Pattern.compile("((\\u0438\\u043b\\u0430|\\u044b\\u043b\\u0430|\\u0435\\u043d\\u0430|\\u0435\\u0439\\u0442\\u0435|\\u0443\\u0439\\u0442\\u0435|\\u0438\\u0442\\u0435|\\u0438\\u043b\\u0438|\\u044b\\u043b\\u0438|\\u0435\\u0439|\\u0443\\u0439|\\u0438\\u043b|\\u044b\\u043b|\\u0438\\u043c|\\u044b\\u043c|\\u0435\\u043d|\\u0438\\u043b\\u043e|\\u044b\\u043b\\u043e|\\u0435\\u043d\\u043e|\\u044f\\u0442|\\u0443\\u0435\\u0442|\\u0443\\u044e\\u0442|\\u0438\\u0442|\\u044b\\u0442|\\u0435\\u043d\\u044b|\\u0438\\u0442\\u044c|\\u044b\\u0442\\u044c|\\u0438\\u0448\\u044c|\\u0443\\u044e|\\u044e)|((?<=[\\u0430\\u044f])(\\u043b\\u0430|\\u043d\\u0430|\\u0435\\u0442\\u0435|\\u0439\\u0442\\u0435|\\u043b\\u0438|\\u0439|\\u043b|\\u0435\\u043c|\\u043d|\\u043b\\u043e|\\u043d\\u043e|\\u0435\\u0442|\\u044e\\u0442|\\u043d\\u044b|\\u0442\\u044c|\\u0435\\u0448\\u044c|\\u043d\\u043d\\u043e)))$");

    private static final Pattern NOUN = Pattern.compile("(\\u0430|\\u0435\\u0432|\\u043e\\u0432|\\u0438\\u0435|\\u044c\\u0435|\\u0435|\\u0438\\u044f\\u043c\\u0438|\\u044f\\u043c\\u0438|\\u0430\\u043c\\u0438|\\u0435\\u0438|\\u0438\\u0438|\\u0438|\\u0438\\u0435\\u0439|\\u0435\\u0439|\\u043e\\u0439|\\u0438\\u0439|\\u0439|\\u0438\\u044f\\u043c|\\u044f\\u043c|\\u0438\\u0435\\u043c|\\u0435\\u043c|\\u0430\\u043c|\\u043e\\u043c|\\u043e|\\u0443|\\u0430\\u0445|\\u0438\\u044f\\u0445|\\u044f\\u0445|\\u044b|\\u044c|\\u0438\\u044e|\\u044c\\u044e|\\u044e|\\u0438\\u044f|\\u044c\\u044f|\\u044f)$");

    private static final Pattern RVRE = Pattern.compile("^(.*?[\\u0430\\u0435\\u0438\\u043e\\u0443\\u044b\\u044d\\u044e\\u044f])(.*)$");

    private static final Pattern DERIVATIONAL = Pattern.compile(".*[^\\u0430\\u0435\\u0438\\u043e\\u0443\\u044b\\u044d\\u044e\\u044f]+[\\u0430\\u0435\\u0438\\u043e\\u0443\\u044b\\u044d\\u044e\\u044f].*\\u043e\\u0441\\u0442\\u044c?$");

    private static final Pattern DER = Pattern.compile("\\u043e\\u0441\\u0442\\u044c?$");

    private static final Pattern SUPERLATIVE = Pattern.compile("(\\u0435\\u0439\\u0448\\u0435|\\u0435\\u0439\\u0448)$");

    private static final Pattern I = Pattern.compile("\\u0438$");
    private static final Pattern P = Pattern.compile("\\u044c$");
    private static final Pattern NN = Pattern.compile("\\u043d\\u043d$");

    public String stem(String word) {
        word = word.toLowerCase();
        word = word.replace("\\u0451", "\\u0435");
        Matcher m = RVRE.matcher(word);
        if (m.matches()) {
            String pre = m.group(1);
            String rv = m.group(2);
            String temp = PERFECTIVEGROUND.matcher(rv).replaceFirst("");
            if (temp.equals(rv)) {
                rv = REFLEXIVE.matcher(rv).replaceFirst("");
                temp = ADJECTIVE.matcher(rv).replaceFirst("");
                if (!temp.equals(rv)) {
                    rv = temp;
                    rv = PARTICIPLE.matcher(rv).replaceFirst("");
                } else {
                    temp = VERB.matcher(rv).replaceFirst("");
                    if (temp.equals(rv)) {
                        rv = NOUN.matcher(rv).replaceFirst("");
                    } else {
                        rv = temp;
                    }
                }

            } else {
                rv = temp;
            }

            rv = I.matcher(rv).replaceFirst("");

            if (DERIVATIONAL.matcher(rv).matches()) {
                rv = DER.matcher(rv).replaceFirst("");
            }

            temp = P.matcher(rv).replaceFirst("");
            if (temp.equals(rv)) {
                rv = SUPERLATIVE.matcher(rv).replaceFirst("");
                rv = NN.matcher(rv).replaceFirst("\\u043d");
            } else {
                rv = temp;
            }
            word = pre + rv;

        }

        return word;
    }

}

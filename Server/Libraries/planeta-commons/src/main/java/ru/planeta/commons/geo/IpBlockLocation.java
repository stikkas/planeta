package ru.planeta.commons.geo;

import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class IpBlockLocation {

    private TreeMap<Long, IpBlock> map = new TreeMap<>();

    private void addIpBlock(long startIp, long endIp, int locationId) {
        map.put(startIp, new IpBlock(endIp, locationId));
    }

    public void addLocations(List<ru.planeta.commons.geo.IpBlock> ipBlocks) {
        if (ipBlocks != null) {
            for (ru.planeta.commons.geo.IpBlock ipBlock : ipBlocks) {
                addIpBlock(ipBlock.getStartIp(), ipBlock.getEndIp(), ipBlock.getLocationId());
            }
        }
    }

    public int getLocationId(long ip) {
        Map.Entry<Long, IpBlock> entry = map.floorEntry(ip);
        if (entry != null && entry.getValue().getEndIp() >= ip) {
            return entry.getValue().getLocationId();
        } else {
            return -1;
        }
    }

    private static class IpBlock {

        private final long endIp;
        private final int locationId;

        public IpBlock(long endIp, int locationId) {
            this.endIp = endIp;
            this.locationId = locationId;
        }

        public long getEndIp() {
            return endIp;
        }

        public int getLocationId() {
            return locationId;
        }
    }
}

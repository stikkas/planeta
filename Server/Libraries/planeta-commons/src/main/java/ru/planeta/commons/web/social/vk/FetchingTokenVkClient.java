package ru.planeta.commons.web.social.vk;

import org.apache.commons.lang3.StringUtils;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Map;

public abstract class FetchingTokenVkClient extends VkApiClient {

    private static final String GET_ACCESS_TOKEN_URL_STUB = "https://oauth.vk.com/access_token";
    protected static final String CLIENT_ID_PARAM_NAME = "client_id";
    private static final String ACCESS_TOKEN_JSON_KEY = "access_token";
    private static final String ACCESS_TOKEN_EXPIRES_KEY = "expires_in";

    private String currentAccessToken;

    protected FetchingTokenVkClient(String clientId, String clientSecret) {
        super(clientId, clientSecret, null);
    }

    abstract protected String getAccessTokenUrl();

    protected void invalidateAccessToken() {
        currentAccessToken = null;
    }

    public final String getAccessToken() {
        if(currentAccessToken == null) {
            try {
                currentAccessToken = fetchAccessToken();
            } catch (IOException e) {
                LOGGER.error("cannot fetch access token");
                return null;
            }
        }
        return currentAccessToken;
    }

    private String fetchAccessToken() throws IOException {
        String tokenJSON = downloadString(getAccessTokenUrl());
        Map<String, Object> responseObject = parseRawResponse(tokenJSON);
        return processAccessTokenResponseObject(responseObject);
    }

    protected String processAccessTokenResponseObject(Map<String, Object> responseObject) {
        String accessToken = (String) responseObject.get(ACCESS_TOKEN_JSON_KEY);
        Integer expiresIn = (Integer) responseObject.get(ACCESS_TOKEN_EXPIRES_KEY);
        return StringUtils.stripToEmpty(accessToken);
    }

    protected static String getAccessTokenUrl(Map<String, Object> accessTokenRequestParams) {
        try {
            VkURIBuilder vkURIBuilder = new VkURIBuilder(GET_ACCESS_TOKEN_URL_STUB);
            vkURIBuilder.addParameterMap(accessTokenRequestParams);
            return vkURIBuilder.toString();
        } catch (URISyntaxException e) {
            LOGGER.error(e);
            return GET_ACCESS_TOKEN_URL_STUB;
        }

    }

}

package ru.planeta.commons.web;

import org.apache.commons.lang3.StringUtils;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Helper class for cookie actions
 * <p/>
 * Created by IntelliJ IDEA.
 * User: atropnikov
 * Date: 24.02.12
 * Time: 11:59
 */
public class CookieUtils {

    /**
     * Standart P3P cookie. You can override it if you want
     */
    public static final String COOKIE_P3P_HEADER = "policyref=\"/w3c/p3p.xml\", CP=\"NOI DSP COR NID CUR OUR NOR\"";

    public static final String EXTERNAL_REFERRER = "new_referrer_url";

    public static final String FIRST_ENTER_URL = "new_enter_url";

    public static final String VISITOR_COOKIE_NAME = "vid";

    public static final String SOURCE_COOKIE_NAME = "sourceId";

    public static final int ONE_MONTH = 60 * 60 * 24 * 30;

    /**
     * Gets cookies value if it exists. Otherwise returns {@code defaultValue}.
     *
     * @param cookies
     * @param cookieName
     * @param defaultValue
     * @return
     */
    public static String getCookieValue(Cookie[] cookies, String cookieName, String defaultValue) {
        if (cookies != null) {
            for (Cookie cookie : cookies) {
                if (cookieName.equals(cookie.getName())) {
                    return (cookie.getValue());
                }
            }
        }

        return defaultValue;
    }

    public static boolean hasCookie(HttpServletRequest req, String cookieName) {
        if (cookieName == null || req == null || req.getCookies() == null) {
            return false;
        }
        for (Cookie cookie : req.getCookies()) {
            if (cookieName.equals(cookie.getName())) {
                return true;
            }
        }
        return false;
    }

    public static void setCookie(HttpServletResponse resp, String name, String value, String domain) {
        setCookie(resp, name, value, ONE_MONTH, domain);
    }

    public static void setCookie(HttpServletRequest req, HttpServletResponse resp, String name, String value) {
        setCookie(req, resp, name, value, ONE_MONTH);
    }

    /**
     * Creates cookie with defined parameters
     *
     * @param req
     * @param resp
     * @param name
     * @param value
     * @param maxAge
     */
    public static void setCookie(HttpServletRequest req, HttpServletResponse resp, String name, String value, int maxAge) {
        setCookie(resp, name, value, maxAge, getDomain(req));
    }

    private static String getDomain(HttpServletRequest req) {
        //http://stackoverflow.com/questions/1134290/cookies-on-localhost-with-explicit-domain
        return StringUtils.startsWith(req.getServerName(), "localhost")
                ? ""
                : "." + req.getServerName();
    }

    /**
     * Creates cookie with defined parameters
     *
     * @param resp
     * @param name
     * @param value
     * @param maxAge
     * @param domain
     */
    public static void setCookie(HttpServletResponse resp, String name, String value, int maxAge, String domain) {
        if (domain != null && (domain.startsWith("localhost") || domain.startsWith(".localhost"))) {
            domain = StringUtils.EMPTY;
        }
        Cookie cookie = createCookie(name, value, maxAge, domain);
        resp.addCookie(cookie);
        resp.setHeader("P3P", COOKIE_P3P_HEADER);
    }

    private static Cookie createCookie(String name, String value, int maxAge, String domain) {
        Cookie result = new Cookie(name, value);
        result.setMaxAge(maxAge);
        result.setPath("/");
        result.setDomain(domain);

        return result;
    }

    /**
     * Remove cookie by name
     *
     * @param req
     * @param resp
     * @param name
     */
    public static void removeCookie(HttpServletRequest req, HttpServletResponse resp, String name) {
        Cookie cookie = createCookie(name, "", 0, getDomain(req));
        resp.addCookie(cookie);
    }

    public static void setCookie(HttpServletResponse response, String name, String value, String domain, int maxAge) {
        Cookie cookie = createCookie(name, value, maxAge, domain);
        response.addCookie(cookie);
    }
}

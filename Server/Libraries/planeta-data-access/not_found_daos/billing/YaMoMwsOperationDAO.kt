package ru.planeta.dao.commondb.billing

import org.apache.ibatis.annotations.Mapper

/**
 * Created by eshevchenko on 24.04.14.
 */
@Mapper
interface YaMoMwsOperationDAO {

    val mwsOperationId: Long
}

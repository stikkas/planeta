package ru.planeta.dao.commondb


/**
 * Class OrderDAOImpl
 *
 * @author a.tropnikov
 */
class OrderDAOImpl /*extends BaseCommonDbDAO implements OrderDAO*///
//    private static final String TRANSACTION_ID = "transactionId";
//    private static final String BUYER_ID = "buyerId";
//    private static final String MERCHANT_ID = "merchantId";
//    private static final String PAYMENT_STATUSES = "paymentStatuses";
//    private static final String DELIVERY_STATUSES = "deliveryStatuses";
//    private static final String PAYMENT_PENDING_STATUS_CODE = "paymentPendingStatusCode";
//    private static final String PAYMENT_COMPLETED_STATUS_CODE = "paymentCompletedStatusCode";
//    private static final String HIDE_CANCELED = "hideCanceled";
//    private static final String ORDER_OBJECT_TYPE = "orderObjectType";
//    private static final String ORDER_OBJECT_TYPES = "orderObjectTypes";
//
//    @Override
//    public Order selectCampaignById(long orderId) {
//        return (Order) selectOne(Statements.Order.SELECT, orderId);
//    }
//
//    @Nullable
//    @Override
//    public Order selectForUpdate(long orderId) {
//        return (Order) selectOne(Statements.Order.SELECT_FOR_UPDATE, orderId);
//    }
//
//    private List<Order> selectOrdersByMoneyTransactionIds(String statementName, Collection<Long> transactionIds) {
//        return CollectionUtils.isEmpty(transactionIds)
//                ? new ArrayList<Order>()
//                : selectCampaignById(statementName, getParameters("transactionIds", transactionIds));
//    }
//
//    @Override
//    public List<Order> selectOrdersByBuyerDebitTransactionIds(Collection<Long> transactionIds) {
//        return selectOrdersByMoneyTransactionIds(Statements.Order.SELECT_ORDERS_BY_BUYER_DEBIT_TRANSACTION_IDS, transactionIds);
//    }
//
//    @Override
//    public List<Order> selectOrdersByBuyerCreditTransactionIds(Collection<Long> transactionIds) {
//        return selectOrdersByMoneyTransactionIds(Statements.Order.SELECT_ORDERS_BY_BUYER_CREDIT_TRANSACTION_IDS, transactionIds);
//    }
//
//    @Override
//    public Order insert(Order order) {
//        insert(Statements.Order.INSERT, order);
//        return order;
//    }
//
//    @Override
//    public Order update(Order order) {
//        order.setTimeUpdated(new Date());
//        update(Statements.Order.UPDATE, order);
//        return order;
//    }
//
//    @Override
//    public void deleteByProfileId(long orderId) {
//        deleteByProfileId(Statements.Order.DELETE, orderId);
//    }
//
//    @Override
//    public int selectUserOrdersCount(long buyerId, long objectId, OrderObjectType orderObjectType) {
//        Map<String, Object> params = getParameters(BUYER_ID, buyerId,
//                OBJECT_ID, objectId,
//                CommonDbParameters.ORDER_OBJECT_TYPE, orderObjectType,
//                PAYMENT_PENDING_STATUS_CODE, PaymentStatus.PENDING.getCode(),
//                PAYMENT_COMPLETED_STATUS_CODE, PaymentStatus.COMPLETED.getCode());
//        Integer count = (Integer) selectUnique(Statements.Order.SELECT_USER_ORDERS_COUNT, params);
//        return count == null ? 0 : count;
//    }
//
//    @Override
//    public Order selectBuyerLastBonus(long buyerId, Date date) {
//        List<Order> orders = getMerchantOrdersInfo(getParameters(BUYER_ID, buyerId, HIDE_CANCELED, true, "sortByTimeUpdated", true),
//                EnumSet.of(OrderObjectType.BONUS), EnumSet.of(PaymentStatus.COMPLETED, PaymentStatus.PENDING), null, date, null, 0, 1);
//        if (orders.isEmpty()) {
//            return null;
//        }
//        return orders.get(0);
//    }
//
//    @Override
//    public List<Order> selectOrdersAfterTime(long buyerId, @Nullable Collection<OrderObjectType> objectTypes,
//            PaymentStatus paymentStatus, Date dateFrom, int offset, int limit) {
//        return selectCampaignById(Statements.Order.SELECT_ORDERS_AFTER_TIME, getParameters(BUYER_ID, buyerId, HIDE_CANCELED, true,
//                ORDER_OBJECT_TYPES, objectTypes, PAYMENT_STATUS, paymentStatus, DATE_FROM, dateFrom,
//                OFFSET, offset, LIMIT, limit));
//    }
//
//    @Override
//    public List<Order> getMerchantOrdersInfo(long buyerId, @Nullable Collection<OrderObjectType> objectTypes, PaymentStatus paymentStatus,
//            DeliveryStatus deliveryStatus, Date dateFrom, Date dateTo, int offset, int limit) {
//        return getMerchantOrdersInfo(getParameters(BUYER_ID, buyerId, HIDE_CANCELED, true), objectTypes, paymentStatus, deliveryStatus, dateFrom, dateTo, offset, limit);
//    }
//
//    @Override
//    public List<Order> getMerchantOrdersInfo(long merchantId, OrderObjectType objectType, PaymentStatus paymentStatus, DeliveryStatus deliveryStatus, Date dateFrom, Date dateTo, int offset, int limit) {
//        Map<String, Object> params = getParameters(MERCHANT_ID, merchantId);
//        return getMerchantOrdersInfo(params, objectType, paymentStatus, deliveryStatus, dateFrom, dateTo, offset, limit);
//    }
//
//    @Override
//    public List<Order> getMerchantOrdersInfo(Collection<Long> orderIds, OrderObjectType orderObjectType, PaymentStatus paymentStatus, Date from, Date to, int offset, int limit) {
//        return getMerchantOrdersInfo(getParameters("orderIds", orderIds), orderObjectType, paymentStatus, null, from, to, offset, limit);
//    }
//
//    @Override
//    public List<Order> getMerchantOrdersInfo(String email, OrderObjectType orderObjectType, PaymentStatus paymentStatus, Date from, Date to, int offset, int limit) {
//        if (StringUtils.isEmpty(email)) {
//            email = null;
//        }
//        return getMerchantOrdersInfo(getParameters("email", email), orderObjectType, paymentStatus, null, from, to, offset, limit);
//    }
//
//    @Override
//    public int selectOrdersCount(long buyerId, long merchantId, OrderObjectType objectType, PaymentStatus[] paymentStatuses, DeliveryStatus deliveryStatus, Date dateFrom, Date dateTo) {
//        Map<String, Object> params = getParameters(
//                BUYER_ID, buyerId,
//                TRANSACTION_ID, 0,
//                CommonDbParameters.ORDER_OBJECT_TYPE, objectType,
//                PAYMENT_STATUSES, paymentStatuses,
//                DELIVERY_STATUS, deliveryStatus,
//                DATE_FROM, dateFrom,
//                DATE_TO, dateTo,
//                MERCHANT_ID, merchantId);
//        return (Integer) selectOne(Statements.Order.SELECT_ORDERS_MERCHANT_COUNT, params);
//    }
//
//    @Override
//    public List<Order> selectFilteredOrdersForCampaign(CampaignSearchFilter campaignSearchFilter) {
//        return selectCampaignById(Statements.Order.SELECT_ORDERS_FOR_CAMPAIGN, campaignSearchFilter);
//    }
//
//    @Override
//    public List<OrderInfoForReport> selectFilteredCampaignOrdersForReport(CampaignSearchFilter campaignSearchFilter) {
//        return selectCampaignById(SELECT_CAMPAIGN_ORDERS_FOR_REPORT, campaignSearchFilter);
//    }
//
//    @Override
//    public List<Order> selectOrdersByObject(long orderObjectId, OrderObjectType orderObjectType, DeliveryStatus deliveryStatus, PaymentStatus paymentStatus) {
//        return selectCampaignById(Statements.Order.SELECT_ORDERS_BY_OBJECT, getParameters(OBJECT_ID, orderObjectId,
//                CommonDbParameters.ORDER_OBJECT_TYPE, orderObjectType, DELIVERY_STATUS, deliveryStatus, PAYMENT_STATUS, paymentStatus));
//    }
//
//    @Override
//    public int selectOrdersCountForObject(long objectId, OrderObjectType objectType, EnumSet<PaymentStatus> paymentStatuses, EnumSet<DeliveryStatus> deliveryStatuses) {
//        Map<String, Object> params = getParameters(OBJECT_ID, objectId,
//                CommonDbParameters.ORDER_OBJECT_TYPE, objectType,
//                PAYMENT_STATUSES, paymentStatuses,
//                DELIVERY_STATUSES, deliveryStatuses);
//        Integer count = (Integer) selectOne(Statements.Order.SELECT_ORDERS_COUNT_FOR_OBJECT, params);
//        return count == null ? 0 : count;
//    }
//
//    @Override
//    public long selectMinCompletedOrderId(long buyerId, OrderObjectType objectType) {
//        Map<String, Object> params = getParameters(BUYER_ID, buyerId,
//                CommonDbParameters.ORDER_OBJECT_TYPE, objectType);
//        Long orderId = (Long) selectOne(Statements.Order.SELECT_MIN_COMPLETED_ORDER_ID, params);
//        return orderId == null ? 0 : orderId;
//    }
//
//    private List<Order> getMerchantOrdersInfo(Map<String, Object> specificParams, @Nullable OrderObjectType orderObjectType, PaymentStatus paymentStatus, DeliveryStatus deliveryStatus, Date dateFrom, Date dateTo, int offset, int limit) {
//        EnumSet<OrderObjectType> orderObjectTypes = orderObjectType == null ? EnumSet.noneOf(OrderObjectType.class) : EnumSet.of(orderObjectType);
//        return getMerchantOrdersInfo(specificParams, orderObjectTypes, paymentStatus, deliveryStatus, dateFrom, dateTo, offset, limit);
//    }
//
//    private List<Order> getMerchantOrdersInfo(Map<String, Object> specificParams, Collection<OrderObjectType> orderObjectTypes, PaymentStatus paymentStatus, DeliveryStatus deliveryStatus, Date dateFrom, Date dateTo, int offset, int limit) {
//        Map<String, Object> params = getParameters(
//                BUYER_ID, 0,
//                MERCHANT_ID, 0,
//                TRANSACTION_ID, 0,
//                "orderObjectTypes", orderObjectTypes,
//                PAYMENT_STATUS, paymentStatus,
//                DELIVERY_STATUS, deliveryStatus,
//                DATE_FROM, dateFrom,
//                DATE_TO, dateTo,
//                OFFSET, offset,
//                LIMIT, limit);
//        if (!MapUtils.isEmpty(specificParams)) {
//            params.putAll(specificParams);
//        }
//        return selectCampaignById(Statements.Order.SELECT_ORDERS, params);
//    }
//
//    private List<Order> getMerchantOrdersInfo(Map<String, Object> specificParams, Collection<OrderObjectType> orderObjectTypes, Collection<PaymentStatus> paymentStatuses, DeliveryStatus deliveryStatus, Date dateFrom, Date dateTo, int offset, int limit) {
//        Map<String, Object> params = getParameters(
//                BUYER_ID, 0,
//                MERCHANT_ID, 0,
//                TRANSACTION_ID, 0,
//                "orderObjectTypes", orderObjectTypes,
//                PAYMENT_STATUSES, paymentStatuses,
//                DELIVERY_STATUS, deliveryStatus,
//                DATE_FROM, dateFrom,
//                DATE_TO, dateTo,
//                OFFSET, offset,
//                LIMIT, limit);
//        if (!MapUtils.isEmpty(specificParams)) {
//            params.putAll(specificParams);
//        }
//        return selectCampaignById(Statements.Order.SELECT_ORDERS, params);
//    }
//
//    @Override
//    public List<Order> searchPurchasesOrders(String query, long buyerId, OrderObjectType orderObjectTypes, int offset, int limit) {
//        Map<String, Object> params = getParameters(
//                BUYER_ID, buyerId,
//                ORDER_OBJECT_TYPE, orderObjectTypes,
//                OFFSET, offset,
//                LIMIT, limit);
//        if (StringUtils.isNotBlank(query)) {
//            long id = NumberUtils.toLong(query, 0);
//            if (id > 0) {
//                params.put("orderId", id);
//            } else {
//                params.put("query", query);
//            }
//        }
//        return selectCampaignById(SELECT_SEARCH_PURCHASES_ORDER, params);
//    }
//
//    @Override
//    public OrderInfo selectOrderInfoCommonData(long orderId, long orderObjectId, long deliveryServiceId, SubjectType subjectType) {
//        OrderInfo result;
//
//        int subjectTypeCode = subjectType != null ? subjectType.getCode() : 0;  // may be null for delivery payment
//        if (deliveryServiceId == 0) {
//            result = (OrderInfo) selectOne(SELECT_ORDER_INFO_COMMON_DATA_SIMPLE, getParameters(
//                    "orderId", orderId,
//                    "subjectTypeCode", subjectTypeCode
//            ));
//
//            result.setLinkedDelivery(new LinkedDelivery());
//            result.getLinkedDelivery().setLocation(new UnspecifiedLocation(0, LocationType.getByValue(0)));
//            result.setDeliveryAddress(new DeliveryAddress());
//        } else {
//            result = (OrderInfo) selectOne(SELECT_ORDER_INFO_COMMON_DATA_FULL, getParameters(
//                    "orderId", orderId,
//                    "orderObjectId", orderObjectId,
//                    "deliveryServiceId", deliveryServiceId,
//                    "subjectTypeCode", subjectTypeCode
//            ));
//        }
//
//        return result;
//    }
//
//    @Override
//    public List<Map<String, Object>> selectOrdersInforForOHMPage(int offset, int limit) {
//        return (List<Map<String, Object>>) selectCampaignById(Statements.Order.SELECT_ORDER_INFO_FOR_OHM, getParameters("offset", offset, "limit", limit));
//    }
//
//    @Override
//    public List<Order> selectOrdersByIds(Collection<Long> orderIds) {
//        return CollectionUtils.isEmpty(orderIds)
//                ? Collections.<Order>emptyList()
//                : selectCampaignById(Statements.Order.SELECT_ORDERS_BY_IDS, getParameters("orderIds", orderIds));
//    }
//
//    @Override
//    public List<OrderInfoForProfilePage> selectUserOrdersRewardsInfo(long buyerId, int offset, int limit) {
//        return selectCampaignById(Statements.Order.SELECT_ORDERS_REWARD_INFO, getParameters(BUYER_ID, buyerId,
//                "offset", offset,
//                "limit", limit));
//    }
//
//
//    @Override
//    public boolean isCharityOrder(long orderId) {
//        if (orderId <= 0) {
//            return false;
//        }
//        return (Boolean) selectOne(Statements.Order.IS_CHARITY_ORDER, getParameters("orderId", orderId));
//    }
//
//    @Override
//    public void updateProfileBackedCount(long profileId) {
//        update(Statements.Order.UPDATE_BACKED_COUNT, getParameters("profileId", profileId));
//    }
//
//    @Override
//    public long selectCampaignIdByOrderId(long orderId) {
//        if (orderId > 0) {
//            Object obj = selectOne(Statements.Order.SELECT_CAMPAIGN_ID_BY_ORDER_ID, getParameters("orderId", orderId));
//            if (obj instanceof Long) {
//                return (Long) obj;
//            }
//        }
//        return 0;
//    }

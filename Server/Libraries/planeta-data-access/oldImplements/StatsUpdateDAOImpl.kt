package ru.planeta.dao.statdb

import org.springframework.stereotype.Repository
import ru.planeta.dao.Statements

/**
 * @author a.savanovich
 */
@Repository
class StatsUpdateDAOImpl : BaseStatDbDAO<*>(), StatsUpdateDAO {

    override fun updateCommentsStats() {
        selectOne(Statements.UpdateStats.UPDATE_COMMENTS_STATS, null)
    }
}

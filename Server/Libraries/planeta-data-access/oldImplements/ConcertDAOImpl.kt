package ru.planeta.dao.concertdb


class ConcertDAOImpl /*: BaseDAO<Concert>(), ConcertDAO {

    override val concertsCount: Long
        get() = selectUnique(ConcertStatement.GET_CONCERTS_COUNT, null) as Long

    private interface ConcertStatement {
        companion object {
            val INSERT = Statements.CONCERTDB + ".insertConcert"
            val UPDATE = Statements.CONCERTDB + ".updateConcert"
            val SELECT_ONE = Statements.CONCERTDB + ".selectConcert"
            val SELECT_ONE_BY_EXT_ID = Statements.CONCERTDB + ".selectConcertByExtId"
            val SELECT_LIST = Statements.CONCERTDB + ".selectCampaignById"
            val GET_CONCERTS_COUNT = Statements.CONCERTDB + ".getConcertsCount"
        }
    }

    override fun insert(concert: Concert): Int {
        return insert(ConcertStatement.INSERT, concert)

    }

    override fun update(concert: Concert): Int {
        return update(ConcertStatement.UPDATE, concert)
    }

    override fun selectCampaignById(concertId: Long): Concert {
        return selectOne(ConcertStatement.SELECT_ONE, getParameters("concertId", concertId))
    }

    override fun selectCampaignById(offset: Int, limit: Int): List<Concert> {
        val params = getParameters(OFFSET, offset, LIMIT, limit)
        return selectCampaignById(ConcertStatement.SELECT_LIST.toInt(), params.toInt())
    }

    override fun selectActive(offset: Int, limit: Int): List<Concert> {
        val params = getParameters(STATUSCODE, ConcertStatus.ACTIVE.getCode(), OFFSET, offset, LIMIT, limit)
        return selectCampaignById(ConcertStatement.SELECT_LIST.toInt(), params.toInt())
    }

    override fun selectByExtId(externalConcertId: Long): Concert {
        return selectOne(ConcertStatement.SELECT_ONE_BY_EXT_ID, getParameters("externalConcertId", externalConcertId))
    }

    companion object {

        val STATUSCODE = "statusCode"
        val OFFSET = "offset"
        val LIMIT = "limit"
    }
}*/

package ru.planeta.dao.commondb

/*
 * Created by Alexey on 05.05.2016.
 */
class MailMessageDAOImpl /*: BaseDAO<MailMessage>(), MailMessageDAO */{
//
//    private interface Notification {
//        companion object {
//
//            val SELECT_BY_MSG_ID = Statements.COMMONDB + ".selectMailNotificationByMsgId"
//            val SELECT_SPAMMERS = Statements.COMMONDB + ".selectSpammersEmail"
//            val INSERT = Statements.COMMONDB + ".insertMailNotification"
//            val UPDATE = Statements.COMMONDB + ".updateMailNotification"
//            val UPDATE_STATUS = Statements.COMMONDB + ".updateMailNotificationStatus"
//            val UPDATE_EXTID_AND_SENT = Statements.COMMONDB + ".updateMailNotificationSent"
//            val CLEAN_OLD_NOTIFICATIONS_LOGS = Statements.COMMONDB + ".cleanMailNotificationsLogs"
//            val MOVE_OLD_NOTIFICATIONS = Statements.COMMONDB + ".moveOldMailNotifications"
//            val MAIL_WAS_OPENED = Statements.COMMONDB + ".insertOpenedMail"
//        }
//    }
//
//    override fun getMailNotificationById(id: Long): List<MailMessage> {
//        return selectCampaignById(Notification.SELECT_BY_MSG_ID, getParameters("messageId", id))
//    }
//
//    override fun getSpammers(threshold: Int, offset: Int, limit: Int): List<*> {
//        return selectCampaignById(Notification.SELECT_SPAMMERS, getParameters("threshold", threshold, "offset", offset, "limit", limit))
//    }
//
//    override fun getMailNotificationByExtMsgId(extMessageId: String): List<MailMessage> {
//        return selectCampaignById(Notification.SELECT_BY_MSG_ID, getParameters("extMessageId", extMessageId))
//    }
//
//    override fun insertMailNotification(mailMessage: MailMessage) {
//        mailMessage.timeAdded = Date()
//        insert(Notification.INSERT, mailMessage)
//    }
//
//    override fun updateMailNotification(mailMessage: MailMessage) {
//        mailMessage.timeUpdated = Date()
//        update(Notification.UPDATE, mailMessage)
//    }
//
//    override fun cleanMailNotificationsLogs(deleteBefore: Date) {
//        deleteByProfileId(Notification.CLEAN_OLD_NOTIFICATIONS_LOGS, getParameters("deleteBefore", deleteBefore))
//    }
//
//    override fun moveOldMailNotifications(deleteBefore: Date) {
//        insert(Notification.MOVE_OLD_NOTIFICATIONS, getParameters("deleteBefore", deleteBefore))
//    }
//
//    override fun updateStatus(extMessageId: String, status: MailMessageStatus) {
//        update(Notification.UPDATE_STATUS, getParameters("externalMessageId", extMessageId,
//                "statusCode", status.getCode(),
//                "timeUpdated", Date()))
//    }
//
//    override fun setExternalMessageIdAndSentStatus(intMessageId: Long, extMessageId: String) {
//        update(Notification.UPDATE_EXTID_AND_SENT, getParameters("messageId", intMessageId,
//                "externalMessageId", extMessageId,
//                "timeUpdated", Date()))
//    }
//
//    override fun saveOpenedMailInfo(messageId: Long) {
//        update(Notification.MAIL_WAS_OPENED, getParameters("messageId", messageId, "timeAdded", Date()))
//    }
}

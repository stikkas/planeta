package ru.planeta.dao.msgdb

import org.springframework.stereotype.Repository
import ru.planeta.dao.BaseClusterDAO
import ru.planeta.dao.Statements
import ru.planeta.model.msg.DialogObjectSelectParams
import ru.planeta.model.msg.DialogUser

/**
 * @author ameshkov
 */
@Repository
class DialogUserDAOImpl : BaseClusterDAO<*>(), DialogUserDAO {

    override fun select(dialogId: Long): List<DialogUser> {
        return selectList(Statements.DialogUser.SELECT, DialogObjectSelectParams(dialogId))
    }

    override fun insert(dialogUser: DialogUser) {
        insert(Statements.DialogUser.INSERT, dialogUser)
    }

}

package ru.planeta.dao.statdb

import org.springframework.stereotype.Repository
import ru.planeta.dao.Statements
import ru.planeta.dao.commondb.BaseCommonDbDAO
import ru.planeta.model.stat.OrderGeoStat

import java.util.Date

/**
 * Created with IntelliJ IDEA.
 * User: michail
 * Date: 27.06.16
 * Time: 12:54
 */
@Repository
class OrderGeoStatDAOImpl : BaseCommonDbDAO<OrderGeoStat>(), OrderGeoStatDAO {
    override fun insert(orderGeo: OrderGeoStat) {
        insert(Statements.StatOrderGeo.INSERT, orderGeo)
    }

    override fun update(orderGeo: OrderGeoStat) {
        orderGeo.timeUpdated = Date()
        update(Statements.StatOrderGeo.UPDATE, orderGeo)
    }

    override fun selectById(orderId: Long): OrderGeoStat {
        return selectOne(Statements.StatOrderGeo.SELECT, getParameters("orderId", orderId))
    }

    override fun selectUnresolved(dateFrom: Date, offset: Int, limit: Int): List<OrderGeoStat> {
        return selectList(Statements.StatOrderGeo.SELECT_UNRESOLVED,
                getParameters("dateFrom", dateFrom,
                        "offset", offset,
                        "limit", limit))
    }
}

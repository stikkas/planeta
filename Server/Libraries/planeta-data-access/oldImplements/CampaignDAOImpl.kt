package ru.planeta.dao.commondb.campaign

class CampaignDAOImpl /*extends BaseCommonDbDAO implements CampaignDAO*///    private static final String SHARE_STATUS = "shareStatus";
//
//    @Nonnull
//    @Override
//    public List<Campaign> selectCampaignsByStatus(long profileId, EnumSet<CampaignStatus> statuses, int offset, int limit) {
//        if (statuses != null && statuses.isEmpty()) {
//            return Collections.emptyList();
//        }
//        Map params = getParameters(PROFILE_ID, profileId, "statuses", (statuses == null || statuses.contains(CampaignStatus.ALL)) ? null : statuses, "offset", offset, "limit", limit);
//        return selectCampaignById(SELECT_LIST_BY_STATUS, params);
//    }
//
//    @Nonnull
//    @Override
//    public List<Campaign> selectCampaignsByStatusAndManagerId(EnumSet<CampaignStatus> statuses, long managerId, int offset, int limit) {
//        if (statuses != null && statuses.isEmpty()) {
//            return Collections.emptyList();
//        }
//        Map params = getParameters("statuses", (statuses == null || statuses.contains(CampaignStatus.ALL)) ? null : statuses, "managerId", managerId, "offset", offset, "limit", limit);
//        return selectCampaignById(SELECT_LIST_BY_STATUS_AND_MANAGER, params);
//
//    }
//
//    @Nonnull
//    @Override
//    public List<Campaign> selectCampaignsWithSharesByStatus(long profileId, CampaignStatus status, int offset, int limit) {
//        Map params = getParameters(PROFILE_ID, profileId, "status", status.getCode(), SHARE_STATUS, ShareStatus.ACTIVE.getCode(), "offset", offset, "limit", limit);
//        return selectCampaignById(SELECT_LIST_WITH_SHARES, params);
//    }
//
//    @Override
//    public Campaign selectCampaignById(long campaignId) {
//        if (campaignId <= 0) {
//            return null;
//        }
//        Map params = getParameters(CAMPAIGN_ID, campaignId);
//        return (Campaign) selectOne(SELECT, params);
//    }
//
//    @Override
//    public Campaign selectForUpdate(long campaignId) {
//        Map params = getParameters(CAMPAIGN_ID, campaignId);
//        return (Campaign) selectOne(SELECT_FOR_UPDATE, params);
//    }
//
//    @Override
//    public int updateDraftVisible(long campaignId, boolean isVisible) {
//        Map params = getParameters(CAMPAIGN_ID, campaignId, "draftVisible", isVisible);
//        return update(UPDATE_DRAFT_VISIBLE, params);
//    }
//
//    @Override
//    public Campaign selectByAlias(String campaignAlias) {
//        if (StringUtils.isBlank(campaignAlias)) {
//            return null;
//        }
//        return (Campaign) selectOne(SELECT_BY_ALIAS, getParameters(CAMPAIGN_ALIAS, campaignAlias));
//    }
//
//    @Override
//    public Campaign selectCampaignById(String campaignAlias) {
//        if (StringUtils.isBlank(campaignAlias)) {
//            return null;
//        }
//        long campaignId = NumberUtils.toLong(campaignAlias, -1);
//        if (campaignId == -1) {
//            return (Campaign) selectOne(SELECT, getParameters(CAMPAIGN_ALIAS, campaignAlias));
//        }
//        return selectCampaignById(campaignId);
//    }
//
//    @Override
//    public List<Campaign> selectCampaignsForAdminSearch(String query, int offset, int limit,
//                                                        @Nullable Long managerId,
//                                                        @Nullable Date dateFrom, @Nullable Date dateTo,
//                                                        EnumSet<CampaignStatus> campaignStatuses, Integer campaignTagId, String stringOrderBy) {
//        Map params = getParameters(CAMPAIGN_NAME, query,
//                MANAGER_ID, managerId,
//                CAMPAIGN_TAG_ID, campaignTagId,
//                DATE_FROM, dateFrom,
//                DATE_TO, dateTo,
//                CAMPAIGN_STATUS_CODES, CampaignStatus.getCodes(campaignStatuses),
//                "stringOrderBy", StringUtils.isNotEmpty(stringOrderBy) ? stringOrderBy : null,
//                OFFSET, offset, LIMIT, limit);
//        return selectCampaignById(SELECT_CAMPAIGNS_FOR_ADMIN_SEARCH, params);
//    }
//
//    @Override
//    public int selectCampaignsForAdminSearchCount(String query,
//                                                  @Nullable Long managerId,
//                                                  @Nullable Date dateFrom, @Nullable Date dateTo,
//                                                  EnumSet<CampaignStatus> campaignStatuses, Integer campaignTagId) {
//        Map params = getParameters(CAMPAIGN_NAME, query,
//                MANAGER_ID, managerId,
//                CAMPAIGN_TAG_ID, campaignTagId,
//                DATE_FROM, dateFrom,
//                DATE_TO, dateTo,
//                CAMPAIGN_STATUS_CODES, CampaignStatus.getCodes(campaignStatuses));
//        Integer count = (Integer) selectOne(SELECT_CAMPAIGNS_FOR_ADMIN_SEARCH_COUNT, params);
//        if (count == null) {
//            return 0;
//        }
//        return count;
//    }
//
//    @Override
//    public Campaign selectWithShares(long campaignId, boolean isSharePriceOrderByDesc) {
//        Map params = getParameters(CAMPAIGN_ID, campaignId,
//                SHARE_STATUS, Arrays.asList(ShareStatus.ACTIVE, ShareStatus.INVESTING, ShareStatus.INVESTING_WITHOUT_MODERATION, ShareStatus.INVESTING_ALL_ALLOWED));
//
//        if (isSharePriceOrderByDesc) {
//            return (Campaign) selectOne(SELECT_WITH_SHARES_ORDER_BY_PRICE_DESC, params);
//        }
//        return (Campaign) selectOne(SELECT_WITH_SHARES, params);
//    }
//
//    @Override
//    public void insert(Campaign campaign) {
//        Date now = new Date();
//        campaign.setTimeAdded(now);
//        campaign.setTimeUpdated(now);
//        if (campaign.getTimeLastPurchased() == null) {
//            campaign.setTimeLastPurchased(now);
//        }
//        if (campaign.getCreatorProfileId() == 0) {
//            campaign.setCreatorProfileId(campaign.getProfileId());
//        }
//        insert(INSERT, campaign);
//    }
//
//    @Override
//    public void update(Campaign campaign) {
//        update(UPDATE, campaign);
//    }
//
//    @Override
//    public void deleteByProfileId(long campaignId) {
//        Map params = getParameters(CAMPAIGN_ID, campaignId, CAMPAIGN_STATUS_CODE, CampaignStatus.DELETED.getCode());
//        deleteByProfileId(DELETE, params);
//    }
//
//    @Override
//    public List<CampaignBacker> selectCampaignBackers(long campaignId, EnumSet<OrderObjectType> shareTypes, int offset, int limit) {
//        Map params = getParameters(CAMPAIGN_ID, campaignId, "shareTypeCodes", CodebleToInt.collectCodes(shareTypes), OFFSET, offset, LIMIT, limit);
//        return selectCampaignById(SELECT_BACKERS, params);
//    }
//
//    @Override
//    public List<Campaign> selectStartedCampaignsByTime(Date date, EnumSet<CampaignStatus> statuses, int offset, int limit) {
//        Map params = getParameters("date", date, "statuses", statuses, OFFSET, offset, LIMIT, limit);
//        return selectCampaignById(SELECT_STARTED_BY_TIME, params);
//    }
//
//    @Override
//    public List<Campaign> selectFinishedCampaignsByTime(Date date, EnumSet<CampaignStatus> statuses, int offset, int limit) {
//        Map params = getParameters("date", date, "statuses", statuses, OFFSET, offset, LIMIT, limit);
//        return selectCampaignById(SELECT_FINISHED_BY_TIME, params);
//    }
//
//    @Override
//    public List<Campaign> selectNotRenewedBackground(int offset, int limit) {
//        Map params = getParameters(OFFSET, offset, LIMIT, limit);
//        return selectCampaignById(SELECT_NOT_RENEWED_BACKGROUND, params);
//    }
//
//    @Override
//    public BigDecimal getTotalCollectedAmount() {
//        return (BigDecimal) selectOne(SELECT_TOTAL_COLLECTED_AMOUNT, null);
//    }
//
//    @Override
//    public int getCampaignBackerCount(long campaignId) {
//        Map params = getParameters(CAMPAIGN_ID, campaignId);
//        return (Integer) selectOne(SELECT_BACKER_COUNT, params);
//    }
//
//    @Override
//    public List<Long> selectPurchasedCampaigns(long profileId) {
//        return selectCampaignById(SELECT_PURCHASED_CAMPAIGNS, getParameters(PROFILE_ID, profileId));
//    }
//
//    @Override
//    public List<Campaign> getCampaignsByIds(List<Long> campaignIds) {
//        if (CollectionUtils.isEmpty(campaignIds)) {
//            return new ArrayList<>();
//        }
//        return selectCampaignById(SELECT_CAMPAIGNS_BY_IDS, getParameters(
//                "campaignIds", campaignIds
//        ));
//    }
//
//    @Override
//    public int getCampaignsCount(long profileId, CampaignStatus status) {
//        Integer result = (Integer) selectOne(SELECT_COUNT, getParameters(PROFILE_ID, profileId, "status", status.getCode()));
//        if (result == null) {
//            return 0;
//        }
//        return result;
//    }
//
//    @Override
//    public List<Map<String, Object>> getCampaignsCountPercentagesByCustomTags() {
//        return selectCampaignById(SELECT_CAMPAIGNS_COUNT_PERCENTAGES_BY_CUSTOM_TAGS, "");
//    }
//
//    @Override
//    public int getCampaignsCount(long profileId, EnumSet<CampaignStatus> statuses) {
//        Integer result = (Integer) selectOne(SELECT_COUNT_BY_STATUSES, getParameters(PROFILE_ID, profileId, "statuses", statuses));
//        if (result == null) {
//            return 0;
//        }
//        return result;
//    }
//
//    @Override
//    public List<Campaign> selectCampaignsByDateRange(@Nullable Date timeStartFrom, @Nullable Date timeStartTo, @Nullable Date timeFinishFrom, @Nullable Date timeFinishTo) {
//        Map params = getParameters("timeStartFrom", timeStartFrom, "timeStartTo", timeStartTo,
//                "timeFinishFrom", timeFinishFrom, "timeFinishTo", timeFinishTo);
//        return selectCampaignById(SELECT_CAMPAIGNS_BY_DATE_RANGE, params);  //To change body of implemented methods use File | Settings | File Templates.
//    }
//
//    @Override
//    public List<Campaign> selectLiteDraftCampaignsByOwnerId(List<Long> groupsIds) {
//        if (groupsIds.isEmpty()) {
//            return Collections.emptyList();
//        }
//        return selectCampaignById(SELECT_DRAFT_BY_OWNER_IDS, getParameters(PROFILE_IDS, groupsIds));
//    }
//
//    @Override
//    public List<Campaign> selectCampaignsByContractorId(long contractorId, int offset, int limit) {
//        Map params = getParameters(CONTRACTOR_ID, contractorId, OFFSET, offset, LIMIT, limit);
//        return selectCampaignById(SELECT_CAMPAIGNS_BY_CONTRACTOR_ID, params);
//    }
//
//    @Override
//    public int selectCampaignsCountWithPurchasedOrReservedShares(long campaignId) {
//        Map params = getParameters(CAMPAIGN_ID, campaignId);
//        Integer result = (Integer) selectOne(SELECT_CAMPAIGNS_COUNT_WITH_PURCHASED_OR_RESERVED_SHARES, params);
//        if (result == null) {
//            return 0;
//        }
//        return result;
//    }
//
//    @Override
//    public BigDecimal selectCampaignsTargetAmount(CampaignStatus status) {
//        return (BigDecimal) selectOne(SELECT_TARGET_AMOUNT, getParameters("status", status));
//    }
//
//    @Override
//    public BigDecimal selectCampaignsPurchasedAmount(CampaignStatus status) {
//        return (BigDecimal) selectOne(SELECT_PURCHASED_AMOUNT, getParameters("status", status));
//    }
//
//    @Override
//    public BigDecimal selectPurchasedAmountForAllTime() {
//        return (BigDecimal) selectOne(SELECT_ALL_TIME_PURCHASED_AMOUNT, null);
//    }
//
//    @Override
//    public Integer selectCampaignsCountBetween(Date dateBegin, Date dateEnd, CampaignStatus status) {
//        return (Integer) selectOne(SELECT_CAMPAIGNS_COUNT_BETWEEN,
//                getParameters("dateBegin", dateBegin,
//                        "dateEnd", dateEnd,
//                        "status", status));
//    }
//
//    @Override
//    public List<CampaignContact> selectCurators(long campaignId) {
//        return selectCampaignById(CampaignCurators.SELECT_CURATORS, getParameters(CAMPAIGN_ID, campaignId));
//    }
//
//    @Override
//    public void deleteCampaignCurator(long campaignId, String email) {
//        deleteByProfileId(CampaignCurators.DELETE_CURATOR, getParameters(CAMPAIGN_ID, campaignId, "email", email));
//    }
//
//    @Override
//    public void insertCampaignCurator(long campaignId, String email) {
//        insert(CampaignCurators.INSERT_CURATOR, getParameters(CAMPAIGN_ID, campaignId, "email", email));
//    }
//
//    @Override
//    public List<Campaign> getBackedCampaignList(long profileId, EnumSet<OrderObjectType> shareTypes, int offset, int limit) {
//        return selectCampaignById(SELECT_BACKED_PROJECTS, getParameters("profileId", profileId,
//                "shareTypeCodes", CodebleToInt.collectCodes(shareTypes),
//                "offset", offset, "limit", limit));
//    }
//
//    @Override
//    public List<Map> getUserBackedCampaignTagCountList(long profileId) {
//        return selectCampaignById(SELECT_USER_BACKED_CAMPAIGN_TAG_COUNT_LIST, profileId);
//    }
//
//    @Override
//    public List<CampaignWithNewEventsCount> selectCampaignNewEventsCountList(List<Long> campaignIds, long profileId, Date dateFrom) {
//        if (campaignIds == null || campaignIds.size() == 0) {
//            return new ArrayList<>(0);
//        }
//        return selectCampaignById(SELECT_CAMPAIGN_NEW_EVENTS_COUNT, getParameters("campaignIds", campaignIds, "profileId", profileId, "dateFrom", dateFrom));
//    }
//
//    private static Map getCampaignNewsParam(CampaignEventsParam campaignEventsParam) {
//        campaignEventsParam.setQuery(StringUtils.isEmpty(campaignEventsParam.getQuery()) ? null : campaignEventsParam.getQuery().trim());
//        if (campaignEventsParam.getCampaignId() == null) {
//            if (NumberUtils.toLong(campaignEventsParam.getQuery(), 0) > 0) {
//                campaignEventsParam.setCampaignId(NumberUtils.toLong(campaignEventsParam.getQuery(), 0));
//                campaignEventsParam.setQuery(null);
//            }
//        }
//
//        return getParameters(
//                "param", campaignEventsParam,
//                "offset", campaignEventsParam.getOffset(),
//                "limit", campaignEventsParam.getLimit());
//    }
//
//    public ListWithCount<CampaignNews> selectCampaignNews(CampaignEventsParam campaignEventsParam) {
//        Map parameters = getCampaignNewsParam(campaignEventsParam);
//        //parameters.put("types", types);
//        return new ListWithCount<CampaignNews>(selectCampaignById(SELECT_CAMPAIGN_NEWS, parameters), (Integer) selectOne(SELECT_CAMPAIGN_NEWS_COUNT, parameters));
//    }
//
//    public ListWithCount<CampaignNews> selectCampaignPosts(CampaignEventsParam campaignEventsParam) {
//        Map parameters = getCampaignNewsParam(campaignEventsParam);
//        return new ListWithCount<CampaignNews>(selectCampaignById(SELECT_CAMPAIGN_POSTS, parameters), (Integer) selectOne(SELECT_CAMPAIGN_POSTS_COUNT, parameters));
//    }
//
//    public ListWithCount<CampaignNews> selectCampaignComments(CampaignEventsParam campaignEventsParam) {
//        Map parameters = getCampaignNewsParam(campaignEventsParam);
//        return new ListWithCount<CampaignNews>(selectCampaignById(SELECT_CAMPAIGN_COMMENTS, parameters), (Integer) selectOne(SELECT_CAMPAIGN_COMMENTS_COUNT, parameters));
//    }
//
//    @Override
//    public int selectCampaignCommentsCountByDate(Date date, ObjectType commentsType, long campaignId) {
//        return (int) selectUnique(SELECT_CAMPAIGN_COMMENTS_COUNT_BY_DATE, getParameters("date", date, "commentsType", commentsType.getCode(), "campaignId", campaignId));
//    }
//
//
//    @Override
//    public long getCampaignId(String campaignAlias) {
//        Long campaignId = (Long) selectOne(SELECT_CAMPAIGN_ID_BY_ALIAS, campaignAlias);
//        if (campaignId == null) {
//            return 0;
//        }
//        return campaignId;
//
//    }
//
//    @Override
//    public Campaign selectCampaignByOrder(long orderId) {
//        return (Campaign) selectOne(SELECT_CAMPAIGN_BY_ORDER, getParameters("orderId", orderId));
//    }
//
//    @Override
//    public Campaign getAuthorActiveCampaign(long creatorProfileId) {
//        return (Campaign) selectOne(SELECT_ACTIVE_AUTHOR_CAMPAIGN, getParameters("creatorProfileId", creatorProfileId));
//    }
//
//    @Override
//    public List<Long> selectPurchasedShareIds(long buyerId, long campaignId) {
//        return selectCampaignById(SELECT_PURCHASED_SHARE_IDS,
//                getParameters("buyerId", buyerId, "campaignId", campaignId));
//    }
//
//    @Override
//    public List<Long> selectPurchasedCampaignIds(long buyerId) {
//        return selectCampaignById(SELECT_PURCHASED_CAMPAIGN_IDS,
//                getParameters("buyerId", buyerId));
//    }
//
//    @Override
//    public List<Campaign> selectCampaignBySponsorAlias(String sponsorAlias, int offset, int limit) {
//        return selectCampaignById(SELECT_BY_SPONSOR_ALIAS, getParameters("sponsorAlias", sponsorAlias, "offset", offset, "limit", limit));
//    }
//
//    @Override
//    public List<GtmCampaignInfo> getGtmCampaignInfoList(List<Long> campaignIds) {
//        if (CollectionUtils.isEmpty(campaignIds)) {
//            return new ArrayList<>();
//        }
//        return selectCampaignById(GET_GTM_CAMPAIGN_INFO_LIST, getParameters(
//                "campaignIds", campaignIds
//        ));
//    }
//
//    @Override
//    public BigDecimal getCollectedAmount(long campaignId) {
//        return (BigDecimal) selectOne(SELECT_CAMPAIGN_COLLECTED_AMOUNT, getParameters(
//                "campaignId", campaignId));
//    }
//
//    @Override
//    public  int getSuccessfulCampaignsCountByTagId(@Nullable Integer campaignTagId) {
//        Integer result = (Integer) selectOne(COUNT_SUCCESS_CAMPAIGNS_BY_TAG_ID, getParameters(
//                "campaignTagId", campaignTagId));
//        if(result == null) {
//            result = 0;
//        }
//        return result;
//    }
//
//    @Override
//    public int countOrganizationCharityCampaigns() {
//        Integer result = (Integer) selectOne(COUNT_ORGANIZATION_CHARITY_CAMPAIGNS, null);
//        if(result == null) {
//            result = 0;
//        }
//        return result;
//    }
//
//    @Override
//    public List<CharityCampaignsStats> charityCampaignsStats(Date dateStart) {
//        return selectCampaignById(CHARITY_CAMPAIGN_STATS, getParameters("dateStart", dateStart));
//    }
//
//    @Override
//    public List<Long> selectProfileIdsOfCampaignsWithEventsForDay(int offset, int limit, Date date) {
//        return selectCampaignById(SELECT_PROFILE_IDS_OF_CAMPAIGNS_WITH_EVENTS_FOR_DAY, getParameters("offset", offset, "limit", limit, "date", date));
//    }
//
//    @Override
//    public List<Campaign> selectCampaignsWithEventsForDayByProfileId(long profileId, int offset, int limit, Date date) {
//        return selectCampaignById(CAMPAIGNS_WITH_EVENTS_FOR_DAY_BY_PROFILE_ID, getParameters("profileId", profileId, "offset", offset, "limit", limit, "date", date));
//    }
//
//    @Override
//    public CampaignStat selectAggregationStatsForDayByCampaignId(long campaignId, Date date) {
//        return (CampaignStat) selectOne(SELECT_AGGREGATION_STATS_FOR_DAY_BY_CAMPAIGN_ID, getParameters("campaignId", campaignId, "date", date));
//    }
//
//    @Override
//    public List<MyCampaignsStatusesCount> getUserCampaignsStatuses(long profileId) {
//        return selectCampaignById(SELECT_USER_CAMPAIGNS_STATUSES_COUNT, getParameters("profileId", profileId));
//    }
//
//    @Override
//    public List<MyCampaignsStatusesCount> getUserPurchasedCampaignsStatuses(long profileId) {
//        return selectCampaignById(SELECT_USER_PURCHASED_CAMPAIGNS_STATUSES_COUNT, getParameters("profileId", profileId));
//    }
//
//    @Override
//    public List<MyPurchasesAndRewardsCount> getUserPurchasedAndRewardsCount (long profileId) {
//        return selectCampaignById(SELECT_USER_PURCHASED_AND_REWARDS_COUNT, getParameters("profileId", profileId));
//    }

package ru.planeta.dao.commondb.loyalty


class BonusDAOImpl /*: BaseCommonDbDAO<*>(), BonusDAO {

    override fun selectBonuses(offset: Int, limit: Int): List<Bonus> {
        return selectCampaignById(Statements.Bonus.SELECT_BONUSES, limit, offset)
    }

    override fun selectAllBonuses(offset: Int, limit: Int): List<Bonus> {
        return selectCampaignById(Statements.Bonus.SELECT_ALL_BONUSES, limit, offset)
    }

    private fun selectCampaignById(statement: String, limit: Int, offset: Int): List<Bonus> {
        val params = getParameters(BaseCommonDbDAO.Companion.OFFSET, offset, BaseCommonDbDAO.Companion.LIMIT, limit)
        return selectCampaignById(statement, params)
    }

    override fun selectCampaignById(bonusId: Long): Bonus? {
        val params = getParameters(BONUS_ID, bonusId)
        return selectOne(Statements.Bonus.SELECT, params) as Bonus
    }

    override fun insert(bonus: Bonus) {
        insert(Statements.Bonus.INSERT, bonus)
    }

    override fun update(bonus: Bonus) {
        update(Statements.Bonus.UPDATE, bonus)
    }

    override fun updateBonusesOrder(bonusPriorities: List<BonusPriorityItem>) {
        update(Statements.Bonus.REORDER_BONUSES, getParameters("priorities", bonusPriorities))
    }

    companion object {

        private val BONUS_ID = "bonusId"
    }

}*/

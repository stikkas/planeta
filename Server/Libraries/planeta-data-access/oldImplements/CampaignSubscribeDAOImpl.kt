package ru.planeta.dao.commondb.campaign

class CampaignSubscribeDAOImpl/* : BaseDAO<*>(), CampaignSubscribeDAO {

    override fun getCampaignSubsctiptionsToSend(offset: Long, limit: Int): List<CampaignSubscribe> {
        return selectCampaignById(GET_CAMPAIGN_SUBSCRIPTIONS_TO_SEND, getParameters("offset", offset, "limit", limit))
    }

    override fun isSubscribedOnCampaignEnd(profileId: Long, campaignId: Long): Boolean? {
        return selectOne(SELECT_IS_SUBSCRIBED_ON_END, getParameters("profileId", profileId, "campaignId", campaignId)) as Boolean
    }

    override fun subscribeOnCampaignEnd(profileId: Long, campaignId: Long) {
        insert(INSERT_SUBSCRIBE_ON_END, getParameters("profileId", profileId, "campaignId", campaignId))
    }

    override fun unsubscribeOnCampaignEnd(profileId: Long, campaignId: Long) {
        update(DELETE_SUBSCRIBE_ON_END, getParameters("profileId", profileId, "campaignId", campaignId))
    }
}*/

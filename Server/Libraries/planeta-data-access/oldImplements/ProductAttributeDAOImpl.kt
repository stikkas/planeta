package ru.planeta.dao.shopdb

import org.springframework.stereotype.Repository
import ru.planeta.dao.Statements
import ru.planeta.model.shop.Category
import ru.planeta.model.shop.ProductAttribute

import ru.planeta.dao.parameter.ShopDbParameter.Companion.ATTRIBUTE_ID
import ru.planeta.dao.parameter.ShopDbParameter.Companion.ATTRIBUTE_TYPE_ID

/**
 * User: m.shulepov
 * Date: 20.06.12
 * Time: 18:26
 */
@Repository
class ProductAttributeDAOImpl : BaseShopDbDAO<*>(), ProductAttributeDAO {

    override fun insertCategory(category: Category) {
        insert(Statements.ProductAttribute.INSERT_CATEGORY, category)
    }

    override fun selectAllCategories(): List<Category> {
        return selectList(Statements.ProductAttribute.SELECT_ALL_CATEGORIES)
    }

    override fun select(attributeId: Long): ProductAttribute {
        val params = getParameters(Companion.getATTRIBUTE_ID(), attributeId)
        return selectOne(Statements.ProductAttribute.SELECT, params) as ProductAttribute
    }

    override fun selectByType(attributeTypeId: Long): List<ProductAttribute> {
        val params = getParameters(Companion.getATTRIBUTE_TYPE_ID(), attributeTypeId)
        return selectList(Statements.ProductAttribute.SELECT_BY_TYPE, params)
    }

}

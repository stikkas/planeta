package ru.planeta.dao.shopdb

import org.springframework.stereotype.Repository
import ru.planeta.dao.Statements
import ru.planeta.model.shop.ExtendedPromoCode
import ru.planeta.model.shop.PromoCode

/**
 * User: a.savanovich
 * Date: 09.10.13
 * Time: 18:08
 */
@Repository
class PromoCodesDAOImpl : BaseShopDbDAO<*>(), PromoCodeDAO {
    override fun getPromoCodeByCode(code: String): PromoCode {
        return selectOne(Statements.PromoCode.SELECT_CODE, getParameters("code", code)) as PromoCode
    }

    override fun getPromoCodeById(promoCodeId: Long): PromoCode {
        return selectOne(Statements.PromoCode.SELECT_CODE_BY_ID, getParameters("promoCodeId", promoCodeId)) as PromoCode
    }

    override fun getPromoCodeIdByCode(code: String, alsoDisabled: Boolean): Long? {
        return selectUnique(Statements.PromoCode.SELECT_CODE_ID, getParameters("code", code, "alsoDisabled", alsoDisabled)) as Long
    }

    override fun getExtendedPromoCodeById(promoCodeId: Long): ExtendedPromoCode {
        return selectOne(Statements.PromoCode.SELECT_CODE_BY_ID_EXTENDED, getParameters("promoCodeId", promoCodeId)) as ExtendedPromoCode
    }

    override fun getPromoCodesList(searchString: String, offset: Int, limit: Int): List<PromoCode> {
        return selectList(Statements.PromoCode.SELECT_LIST, getParameters(
                "searchString", searchString,
                "offset", offset,
                "limit", limit
        )) as List<PromoCode>
    }

    override fun getExtendedPromoCodesList(searchString: String, offset: Int, limit: Int): List<ExtendedPromoCode> {
        return selectList(Statements.PromoCode.SELECT_LIST_EXTENDED, getParameters(
                "searchString", searchString,
                "offset", offset,
                "limit", limit
        )) as List<ExtendedPromoCode>
    }

    override fun getExtendedPromoCodesListCount(searchString: String): Long {
        val result = selectUnique(Statements.PromoCode.SELECT_CODE_LIST_COUNT, getParameters("searchString", searchString)) as Long
        return result ?: 0
    }

    override fun insert(promoCode: PromoCode) {
        insert(Statements.PromoCode.INSERT, promoCode)
    }

    override fun update(promoCode: PromoCode) {
        update(Statements.PromoCode.UPDATE, promoCode)
    }

    override fun markDisabled(promoCodeId: Long) {
        update(Statements.PromoCode.MARK_DELETED,
                getParameters("promoCodeId", promoCodeId,
                        "disabled", true))
    }

    override fun markEnabled(promoCodeId: Long) {
        update(Statements.PromoCode.MARK_DELETED,
                getParameters("promoCodeId", promoCodeId,
                        "disabled", false))
    }
}

package ru.planeta.dao.commondb

class OrderObjectDAOImpl /*extends BaseCommonDbDAO implements OrderObjectDAO*///
//    private static final String ORDER_ID = "orderId";
//    private static final String OBJECT_ID = "objectId";
//    private static final String ORDER_OBJECT_ID = "orderObjectId";
//    private static final String ORDER_OBJECT_TYPE = "orderObjectType";
//
//    @Override
//    public List<OrderObject> selectCampaignById(long orderId, long objectId, OrderObjectType orderObjectType) {
//        Map params = getParameters(ORDER_ID, orderId,
//                OBJECT_ID, objectId,
//                ORDER_OBJECT_TYPE, orderObjectType);
//        return selectCampaignById(Statements.OrderObject.SELECT_ORDER_OBJECTS, params);
//    }
//
//    @Override
//    public OrderObject selectCampaignById(long orderObjectId) {
//        return (OrderObject) selectOne(Statements.OrderObject.SELECT_ORDER_OBJECT, orderObjectId);
//    }
//
//    @Override
//    public List<OrderObject> selectOrderObjects(long orderId) {
//        Map params = getParameters("orderIds", Arrays.asList(Long.valueOf(orderId)), "merchantId", 0);
//        return selectCampaignById(Statements.OrderObject.SELECT_ALL_ORDERS_OBJECTS, params);
//    }
//
//    @Override
//    public List<OrderObject> selectOrdersObjects(Collection<Long> orderIds, long merchantId) {
//        Map params = getParameters("orderIds", orderIds.isEmpty() ? null : orderIds, "merchantId", merchantId);
//        return selectCampaignById(Statements.OrderObject.SELECT_ALL_ORDERS_OBJECTS, params);
//    }
//
//    @Override
//    public void insert(OrderObject orderObject) {
//        insert(Statements.OrderObject.INSERT, orderObject);
//    }
//
//    @Override
//    public void update(OrderObject orderObject) {
//        update(Statements.OrderObject.UPDATE, orderObject);
//    }
//
//    @Override
//    public void markOrderObjectsAsGift(long orderId) {
//        update(Statements.OrderObject.MARK_ORDER_OBJECTS_AS_GIFT, orderId);
//    }
//
//    @Override
//    public void updateOrderObjectComment(long orderId, long objectId, String comment) {
//        update(Statements.OrderObject.UPDATE_COMMENT, getParameters(
//                "orderId", orderId,
//                "objectId", objectId,
//                "comment", StringUtils.isBlank(comment) ? StringUtils.EMPTY : comment
//        ));
//    }
//
//    @Override
//    public List<OrderObject> selectMerchantOrderObjects(long merchantId, PaymentStatus paymentStatus, OrderObjectType orderObjectType) {
//        Map parameters = getParameters("merchantId", merchantId, "orderObjectType", orderObjectType, "paymentStatus", paymentStatus);
//        return selectCampaignById(Statements.OrderObject.SELECT_MERCHANT_ORDERS_OBJECTS, parameters);
//    }
//
//    @Override
//    public List<OrderObject> selectOverdueOrdersObjects() {
//        return selectCampaignById(Statements.OrderObject.SELECT_OVERDUE_ORDERS_OBJECTS);
//    }
//
//    @Override
//    public boolean isProfileHasPurchasedObject(long buyerId, long objectId, OrderObjectType orderObjectType) {
//        return (boolean)selectOne(Statements.OrderObject.SELECT_PROFILE_PURCHASE_OBJECT,
//                getParameters("buyerId", buyerId, "objectId", objectId, "objectType", orderObjectType));
//    }

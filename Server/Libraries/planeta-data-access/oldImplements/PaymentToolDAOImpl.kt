package ru.planeta.dao.payment

import org.springframework.stereotype.Repository
import ru.planeta.dao.Statements
import ru.planeta.dao.commondb.BaseCommonDbDAO
import ru.planeta.model.common.PaymentTool
import ru.planeta.model.enums.ProjectType

import java.math.BigDecimal

/**
 * Created with IntelliJ IDEA.
 * User: a.volkov
 * Date: 24.04.14
 * Time: 18:14
 */
@Repository
class PaymentToolDAOImpl : BaseCommonDbDAO<PaymentTool>(), PaymentToolDAO {
    override fun insert(tool: PaymentTool): PaymentTool {
        insert(Statements.PaymentTool.INSERT, tool)
        return tool
    }

    override fun selectAllPaymentTools(): List<PaymentTool> {
        return selectList(Statements.PaymentTool.SELECT_ALL, null)
    }

    override fun update(tool: PaymentTool): PaymentTool {
        insert(Statements.PaymentTool.UPDATE, tool)
        return tool
    }

    override fun getTools(paymentMethodId: Long, projectType: ProjectType, amount: BigDecimal, tlsSupported: Boolean): List<PaymentTool> {
        return selectList(Statements.PaymentTool.SELECT_FOR_METHOD_AND_PROJECT, getParameters(
                "paymentMethodId", paymentMethodId,
                "projectType", projectType,
                "amount", amount,
                "tlsNotSupported", !tlsSupported
        ))
    }

    override fun getToolByToolId(toolId: Long): PaymentTool {
        return selectOne(Statements.PaymentTool.SELECT, getParameters(
                "id", toolId))
    }
}

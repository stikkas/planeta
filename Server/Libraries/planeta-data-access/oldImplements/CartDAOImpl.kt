package ru.planeta.dao.shopdb

import org.springframework.stereotype.Repository
import ru.planeta.dao.Statements
import ru.planeta.model.shop.ShoppingCartShortItem
import java.util.Date

/**
 * @author Andrew Arefyev
 * Date: 03.12.12
 * Time: 19:08
 */
@Repository
class CartDAOImpl : BaseShopDbDAO<*>(), CartDAO {
    override fun insert(buyerId: Long, productId: Long, quantity: Int): Int {
        return insert(Statements.ShoppingCart.INSERT, getParameters(Companion.PROFILE_ID, buyerId, Companion.PRODUCT_ID, productId, Companion.QUANTITY, quantity))

    }

    override fun select(profileId: Long, productId: Long): ShoppingCartShortItem? {
        return selectOne(Statements.ShoppingCart.SELECT_FROM_CART, getParameters(Companion.PROFILE_ID, profileId, Companion.PRODUCT_ID, productId)) as ShoppingCartShortItem
    }

    override fun select(profileId: Long): List<ShoppingCartShortItem> {
        return selectList(Statements.ShoppingCart.SELECT_FROM_CART, getParameters(Companion.PROFILE_ID, profileId))
    }

    override fun update(buyerId: Long, productId: Long, quantity: Int): Int {
        return update(Statements.ShoppingCart.UPDATE_QUANTITY, getParameters(Companion.PROFILE_ID, buyerId, Companion.PRODUCT_ID, productId, Companion.QUANTITY, quantity))
    }

    override fun clearCart(buyerId: Long): Int {
        return delete(Statements.ShoppingCart.DELETE.toLong(), getParameters(Companion.PROFILE_ID, buyerId))
    }

    override fun delete(buyerId: Long, productId: Long): Int {
        return delete(Statements.ShoppingCart.DELETE.toLong(), getParameters(Companion.PROFILE_ID, buyerId, Companion.PRODUCT_ID, productId))
    }

    override fun deleteProduct(productId: Long): Int {
        return delete(Statements.ShoppingCart.DELETE.toLong(), getParameters(Companion.PRODUCT_ID, productId))
    }

    override fun getLastTimeUpdated(profileId: Long): Date {
        return selectOne(Statements.ShoppingCart.GET_LAST_TIME_UPDATED, getParameters(Companion.PROFILE_ID, profileId)) as Date
    }

    override fun selectCartOwnersIds(): List<Long> {
        return selectList(Statements.ShoppingCart.GET_ALL_IDS)
    }
}

package ru.planeta.dao.commondb.campaign


class CampaignTargetingDAOImpl /*: BaseCommonDbDAO<CampaignTargeting>(), CampaignTargetingDAO {

    override fun selectByCampaignId(campaignId: Long): CampaignTargeting {
        return selectOne(Statements.CampaignTargeting.SELECT_BY_CAMPAIGN_ID, campaignId)
    }

    override fun insert(campaignTargeting: CampaignTargeting) {
        insert(Statements.CampaignTargeting.INSERT_CAMPAIGN_TARGETING, campaignTargeting)
    }

    override fun update(campaignTargeting: CampaignTargeting) {
        update(Statements.CampaignTargeting.UPDATE_CAMPAIGN_TARGETING, campaignTargeting)
    }
}*/

package ru.planeta.dao.payment

import org.springframework.stereotype.Repository
import ru.planeta.dao.commondb.BaseCommonDbDAO
import ru.planeta.model.common.PaymentProvider

/**
 * Created with IntelliJ IDEA.
 * User: a.volkov
 * Date: 24.04.14
 * Time: 13:44
 */
@Repository
class PaymentProviderDAOImpl : BaseCommonDbDAO<PaymentProvider>(), PaymentProviderDAO {
    override fun select(type: PaymentProvider.Type): PaymentProvider {
        return selectOne(SELECT_BY_TYPE, getParameters("type", type))
    }

    override fun select(providerId: Long): PaymentProvider {
        return selectOne(SELECT, providerId)
    }

    override fun update(provider: PaymentProvider): PaymentProvider {
        update(UPDATE, provider)
        return provider
    }

    override fun selectAll(): List<PaymentProvider> {
        return selectList(SELECT_ALL)
    }
}

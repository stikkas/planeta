package ru.planeta.dao.commondb.campaign


class CampaignContactDAOImpl /*: BaseCommonDbDAO<*>(), CampaignContactDAO {
    override fun selectCampaignById(campaignId: Long, offset: Int, limit: Int): List<CampaignContact> {
        val params = getParameters("campaignId", campaignId, "offset", offset, "limit", limit)
        return selectCampaignById(Statements.CampaignContact.SELECT_LIST, params)
    }

    override fun insert(campaignContact: CampaignContact) {
        insert(Statements.CampaignContact.INSERT, campaignContact)
    }

    override fun deleteByProfileId(campaignId: Long, email: String) {
        val params = getParameters("campaignId", campaignId, "email", email)
        deleteByProfileId(Statements.CampaignContact.DELETE.toLong(), params)
    }

    override fun getCount(campaignId: Long): Int {
        return selectOne(Statements.CampaignContact.GET_CAMPAIGN_COUNT, getParameters("campaignId", campaignId)) as Int
    }
}*/

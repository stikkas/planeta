package ru.planeta.dao.statdb

import org.springframework.stereotype.Repository
import ru.planeta.dao.Statements
import ru.planeta.model.stat.SharedObjectStats

/**
 * User: m.shulepov
 * Date: 23.10.12
 * Time: 16:17
 */
@Repository
class SharedObjectStatsDAOImpl : BaseStatDbDAO<*>(), SharedObjectStatsDAO {

    override fun insert(sharedObject: SharedObjectStats) {
        insert(Statements.SharedObject.INSERT, sharedObject)
    }

    override fun updateCount(sharedObjectId: Long) {
        update(Statements.SharedObject.UPDATE, sharedObjectId)
    }

    override fun selectById(sharedObjectId: Long): SharedObjectStats? {
        return selectOne(Statements.SharedObject.SELECT_BY_ID, sharedObjectId) as SharedObjectStats
    }

    override fun selectByUrl(url: String): SharedObjectStats? {
        return selectOne(Statements.SharedObject.SELECT_BY_URL, url) as SharedObjectStats
    }

}

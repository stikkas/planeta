package ru.planeta.dao.trashcan

import org.springframework.stereotype.Repository
import ru.planeta.dao.BaseDAO
import ru.planeta.dao.Statements
import ru.planeta.model.trashcan.CampaignDraft

import java.util.Date

@Repository
class CampaignDraftDAOImpl : BaseDAO<CampaignDraft>(), CampaignDraftDAO {

    override fun insert(draft: CampaignDraft) {
        insert(INSERT, draft)
    }

    override fun cleanDrafts(deleteTo: Date) {
        delete(CLEAN_OLD, getParameters("deleteBefore", deleteTo))
    }

    companion object {

        private val INSERT = Statements.TRASHCAN + ".insertCampaignDraft"
        private val CLEAN_OLD = Statements.TRASHCAN + ".cleanOldCampaignDraft"
    }

}

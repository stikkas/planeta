package ru.planeta.dao.trashcan

import org.springframework.stereotype.Repository
import ru.planeta.dao.BaseDAO
import ru.planeta.dao.Statements
import ru.planeta.model.stat.RequestStat

@Repository
class RefererDAOImpl : BaseDAO<RequestStat>(), RefererDAO {
    override fun insert(stat: RequestStat) {
        insert(INSERT, stat)
    }

    companion object {

        private val INSERT = Statements.TRASHCAN + ".insertRedirectStat"
    }

}

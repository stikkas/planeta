package ru.planeta.dao.statdb

import org.springframework.stereotype.Repository
import ru.planeta.dao.Statements
import ru.planeta.model.common.ShareServiceType
import ru.planeta.model.stat.SharedObject

/**
 * User: m.shulepov
 * Date: 23.10.12
 * Time: 16:17
 */
@Repository
class SharedObjectDAOImpl : BaseStatDbDAO<*>(), SharedObjectDAO {

    override fun insertDetails(sharedObject: SharedObject) {
        insert(Statements.SharedObject.INSERT_DETAILS, sharedObject)
    }

    override fun selectLastSharedObjectDetails(sharedObjectId: Long, shareServiceType: ShareServiceType): SharedObject {
        val params = getParameters("sharedObjectId", sharedObjectId, "shareServiceType", shareServiceType)
        return selectOne(Statements.SharedObject.SELECT_LAST_SHARED_OBJECT_DETAILS, params) as SharedObject
    }

    override fun selectSharedObjectDetails(profileId: Long, uuid: String?, sharedObjectId: Long, url: String, shareServiceType: ShareServiceType): SharedObject? {
        val params = getParameters("authorProfileId", profileId, "sharedObjectId", sharedObjectId, "authorUUID", uuid, "sharedObjectUrl", url, "shareServiceType", shareServiceType)
        return selectOne(Statements.SharedObject.SELECT_SHARED_OBJECT_DETAILS_FOR_PROFILE, params) as SharedObject
    }

}

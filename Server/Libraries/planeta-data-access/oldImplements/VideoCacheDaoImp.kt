package ru.planeta.dao.commondb


class VideoCacheDaoImp /*: BaseCommonDbDAO<CachedVideo>(), VideoCacheDAO {

    protected var sequencesDAO: SequencesDAO? = null
        set

    override fun selectByOrderId(id: Long): CachedVideo {
        val params = getParameters("cachedVideoId", id)
        return selectOne(Statements.CachedVideo.SELECT_VIDEO_CACHE_BY_ID, params)
    }

    override fun selectByUrl(url: String): CachedVideo {
        val params = getParameters("url", url)
        return selectOne(Statements.CachedVideo.SELECT_VIDEO_CACHE_BY_URL, params)
    }

    override fun insert(cachedVideo: CachedVideo) {
        if (cachedVideo.cachedVideoId == 0L) {
            cachedVideo.cachedVideoId = sequencesDAO!!.selectNextLong(CachedVideo::class.java)
        }
        insert(Statements.CachedVideo.INSERT_VIDEO_CACHE, cachedVideo)
    }

    override fun update(video: CachedVideo) {
        update(Statements.CachedVideo.UPDATE_VIDEO_CACHE, video)
    }
}*/

package ru.planeta.dao.promo

import org.springframework.stereotype.Repository
import ru.planeta.dao.BaseDAO
import ru.planeta.model.promo.TechnobattleProject

/**
 * Created with IntelliJ IDEA.
 * User: michail
 * Date: 16.01.2017
 * Time: 18:45
 */
@Repository
class TechnobattleProjectDAOImpl : BaseDAO<TechnobattleProject>(), TechnobattleProjectDAO {
    private interface Statements {
        companion object {
            val SELECT = ru.planeta.dao.Statements.PROMODB + ".selectTechnobattleProjects"
            val SELECT_ONE = ru.planeta.dao.Statements.PROMODB + ".selectOneTechnobattleProject"
            val INSERT = ru.planeta.dao.Statements.PROMODB + ".insertProject"
            val UPDATE = ru.planeta.dao.Statements.PROMODB + ".updateProject"
        }
    }

    override fun selectList(offset: Int, limit: Int): List<TechnobattleProject> {
        return selectList(Statements.SELECT.toInt(), getParameters("offset", offset, "limit", limit))
    }

    override fun selectOne(projectId: Long): TechnobattleProject {
        return selectOne(Statements.SELECT_ONE, getParameters("projectId", projectId))
    }

    override fun insert(project: TechnobattleProject) {
        insert(Statements.INSERT, project)
    }

    override fun update(project: TechnobattleProject) {
        update(Statements.UPDATE, project)
    }
}

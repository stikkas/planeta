package ru.planeta.dao.statdb

import org.springframework.stereotype.Repository
import ru.planeta.dao.Statements
import ru.planeta.model.stat.CampaignStat

import java.util.Date

/**
 * Created with IntelliJ IDEA.
 * User: a.volkov
 * Date: 02.09.2014
 * Time: 19:05
 */
@Repository
class CampaignStatDAOImpl : BaseStatDbDAO<CampaignStat>(), CampaignStatDAO {

    override fun aggregate() {
        insert(Statements.CampaignStat.INSERT_GENERAL_STATS, null)
        insert(Statements.CampaignStat.INSERT_REFERER_STATS, null)
        insert(Statements.CampaignStat.INSERT_COUNTRY_STATS, null)
        insert(Statements.CampaignStat.INSERT_CITY_STATS, null)
    }

    override fun selectGeneralStats(campaignId: Long, from: Date, to: Date, offset: Int, limit: Int): List<CampaignStat> {
        return selectList(Statements.CampaignStat.SELECT_GENERAL, getParameters("campaignId", campaignId, "dateFrom", from, "dateTo", to, "offset", offset, "limit", limit))
    }

    override fun selectGeneralTotalStats(campaignId: Long, from: Date, to: Date): CampaignStat {
        return selectOne(Statements.CampaignStat.SELECT_GENERAL_TOTAL_STATS, getParameters("campaignId", campaignId, "dateFrom", from, "dateTo", to))
    }

    override fun selectRefererStats(campaignId: Long, from: Date, to: Date, offset: Int, limit: Int): List<CampaignStat> {
        return selectList(Statements.CampaignStat.SELECT_REFERER, getParameters("campaignId", campaignId, "dateFrom", from, "dateTo", to, "offset", offset, "limit", limit))
    }

    override fun selectCountryStats(campaignId: Long, from: Date, to: Date, offset: Int, limit: Int): List<CampaignStat> {
        return selectList(Statements.CampaignStat.SELECT_COUNTRY, getParameters("campaignId", campaignId, "dateFrom", from, "dateTo", to, "offset", offset, "limit", limit))
    }

    override fun selectCityStats(campaignId: Long, from: Date, to: Date, offset: Int, limit: Int): List<CampaignStat> {
        return selectList(Statements.CampaignStat.SELECT_CITY, getParameters("campaignId", campaignId, "dateFrom", from, "dateTo", to, "offset", offset, "limit", limit))
    }
}

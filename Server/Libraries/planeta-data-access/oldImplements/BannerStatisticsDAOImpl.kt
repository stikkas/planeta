package ru.planeta.dao.trashcan

import org.springframework.stereotype.Component
import ru.planeta.dao.BaseDAO
import ru.planeta.dao.Statements
//import ru.planeta.mybatis.generator.plugins.model.ExTableConfig;

//import java.util.ArrayList;
//import java.util.Collection;
import java.util.Date

/**
 * User: Serge Blagodatskih<stikkas17></stikkas17>@gmail.com><br></br>
 * Date: 12.07.16<br></br>
 * Time: 16:17
 */
@Component
class BannerStatisticsDAOImpl : BaseDAO<*>(), BannerStatisticsDAO {

    override fun insertClicked(profileId: Long, bannerId: Long, referer: String) {
        insert(Statements.BannerStatistics.INSERT_CLICKED, getParameters("profileId",
                profileId, "bannerId", bannerId, "referer", referer, "timeAdded", Date()))
    }

    override fun insertAppeared(profileId: Long, bannerId: Long, referer: String) {
        insert(Statements.BannerStatistics.INSERT_APPEARED, getParameters("profileId",
                profileId, "bannerId", bannerId, "referer", referer, "timeAdded", Date()))
    }
}

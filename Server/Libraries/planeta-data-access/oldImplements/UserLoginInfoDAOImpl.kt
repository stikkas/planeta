package ru.planeta.dao.commondb

class UserLoginInfoDAOImpl /*: BaseCommonDbDAO<*>(), UserLoginInfoDAO {

    override fun insert(userLoginInfo: UserLoginInfo): Int {
        return insert(Statements.UserLoginInfo.INSERT, userLoginInfo)

    }

    override fun update(userLoginInfo: UserLoginInfo): Int {
        return update(Statements.UserLoginInfo.UPDATE, userLoginInfo)
    }

    override fun deleteByProfileId(profileId: Long, userIpAddress: String, userAgent: String): Int {
        val params = getParameters(PROFILE_ID, profileId, USER_IP_ADDRESS, userIpAddress, USER_AGENT, userAgent)
        return deleteByProfileId(Statements.UserLoginInfo.DELETE, params)
    }

    override fun selectCampaignById(profileId: Long, userIpAddress: String, userAgent: String): UserLoginInfo {
        val params = getParameters(PROFILE_ID, profileId, USER_IP_ADDRESS, userIpAddress, USER_AGENT, userAgent)
        return selectOne(Statements.UserLoginInfo.SELECT, params) as UserLoginInfo
    }

    override fun selectLastUserLoginInfo(profileId: Long): UserLoginInfo {
        val params = getParameters(PROFILE_ID, profileId)
        return selectOne(Statements.UserLoginInfo.SELECT_LAST, params) as UserLoginInfo
    }

    companion object {

        private val PROFILE_ID = "profileId"
        private val USER_IP_ADDRESS = "userIpAddress"
        private val USER_AGENT = "userAgent"
        private val DATE_TO = "dateTo"
        private val CHECKING_DATE_INTERVAL = "sendingDateInterval"
        private val AUTO_SUBSCRIBE_GROUPS = "autoSubscribeGroupsIds"
    }
}
*/

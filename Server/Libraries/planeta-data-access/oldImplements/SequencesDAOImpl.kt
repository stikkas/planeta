package ru.planeta.dao.commondb


class SequencesDAOImpl /*: BaseCommonDbDAO<*>(), SequencesDAO*/ {
//
//    /**
//     * Selects next sequence value for the specified class
//     *
//     */
//    override fun selectNextLong(clazz: Class<*>): Long {
//        return selectNext(clazz, Statements.Sequences.SELECT_NEXT_LONG) as Long
//    }
//
//    private fun selectNext(clazz: Class<*>, statementName: String): Any {
//        val sequenceName = SEQUENCES[clazz]
//                ?: throw IllegalArgumentException("No sequence name found for this statement")
//
//        return selectOneDirect(statementName, sequenceName)
//    }
//
//    companion object {
//
//        internal val SEQUENCES: MutableMap<Class<*>, String> = HashMap()
//
//        init {
//            SEQUENCES[Dialog::class.java] = "commondb.seq_dialog_id"
//            SEQUENCES[DialogMessage::class.java] = "commondb.seq_message_id"
//            SEQUENCES[Profile::class.java] = "commondb.seq_profile_id"
//            SEQUENCES[Photo::class.java] = "commondb.seq_photo_id"
//            SEQUENCES[PhotoAlbum::class.java] = "commondb.seq_photo_album_id"
//            SEQUENCES[Video::class.java] = "commondb.seq_video_id"
//            SEQUENCES[AudioTrack::class.java] = "commondb.seq_audio_track_id"
//            SEQUENCES[Comment::class.java] = "commondb.seq_comment_id"
//            SEQUENCES[CommentObject::class.java] = "commondb.seq_comment_object_id"
//            SEQUENCES[Post::class.java] = "profiledb.seq_posts_id"
//            SEQUENCES[Broadcast::class.java] = "commondb.seq_broadcast_id"
//            SEQUENCES[BroadcastStream::class.java] = "commondb.seq_broadcast_stream_id"
//            SEQUENCES[Chat::class.java] = "commondb.seq_chat_id"
//            SEQUENCES[ChatMessage::class.java] = "commondb.seq_chat_message_id"
//            SEQUENCES[CachedVideo::class.java] = "commondb.seq_cached_video_id"
//            SEQUENCES[Order::class.java] = "commondb.seq_order_id"
//            SEQUENCES[OrderObject::class.java] = "commondb.seq_order_object_id"
//            SEQUENCES[Share::class.java] = "commondb.seq_share_id"
//            SEQUENCES[TopayTransaction::class.java] = "commondb.seq_topay_transaction_id"
//            SEQUENCES[Address::class.java] = "commondb.seq_address_id"
//            SEQUENCES[CreditTransaction::class.java] = "commondb.seq_credit_transaction_id"
//            SEQUENCES[DebitTransaction::class.java] = "commondb.seq_debit_transaction_id"
//            SEQUENCES[Product::class.java] = "shopdb.seq_product_id"
//            SEQUENCES[ProfileFile::class.java] = "commondb.seq_profile_file_id"
//            SEQUENCES[PaymentMethod::class.java] = "commondb.seq_payment_method_id"
//            SEQUENCES[PaymentProvider::class.java] = "commondb.seq_payment_provider_id"
//            SEQUENCES[PaymentTool::class.java] = "commondb.seq_payment_tool_id"
//            SEQUENCES[PaymentTool::class.java] = "commondb.seq_project_payment_tool_id"
//        }
//    }
}

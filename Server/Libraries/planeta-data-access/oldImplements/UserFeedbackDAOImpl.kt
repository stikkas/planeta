package ru.planeta.dao.trashcan

import org.springframework.stereotype.Repository
import ru.planeta.dao.BaseDAO
import ru.planeta.dao.Statements
import ru.planeta.model.trashcan.FeedbackPageType
import ru.planeta.model.trashcan.UserFeedback

@Repository
class UserFeedbackDAOImpl : BaseDAO<UserFeedback>(), UserFeedbackDAO {

    override fun insert(userFeedback: UserFeedback) {
        insert(INSERT, userFeedback)
    }

    override fun selectUserFeedbacks(offset: Int, limit: Int): List<UserFeedback> {
        return selectList(SELECT, getParameters(OFFSET, offset, LIMIT, limit))
    }

    override fun selectUserFeedbackByOrderId(orderId: Long): UserFeedback {
        return selectOne(SELECT_BY_ORDER_ID, getParameters(ORDER_ID, orderId))
    }

    override fun selectUserFeedbackByCampaignIdAndPageType(campaignId: Long, feedbackPageType: FeedbackPageType): UserFeedback {
        return selectOne(SELECT_BY_CAMPAIGN_ID_AND_PAGE_TYPE, getParameters(CAMPAIGN_ID, campaignId, PAGE_TYPE_CODE, feedbackPageType.code))
    }

    override fun selectUserFeedbackCount(): Int {
        return selectUnique(SELECT_COUNT, null) as Int
    }

    override fun updateUserFeedback(userFeedback: UserFeedback) {
        update(UPDATE, userFeedback)
    }

    companion object {
        private val SELECT = Statements.TRASHCAN + ".selectUserFeedbacks"
        private val SELECT_COUNT = Statements.TRASHCAN + ".selectUserFeedbackCount"
        private val SELECT_BY_ORDER_ID = Statements.TRASHCAN + ".selectUserFeedbackByOrderId"
        private val SELECT_BY_CAMPAIGN_ID_AND_PAGE_TYPE = Statements.TRASHCAN + ".selectUserFeedbackByCampaignIdAndPageType"
        private val INSERT = Statements.TRASHCAN + ".insertUserFeedback"
        private val UPDATE = Statements.TRASHCAN + ".updateUserFeedback"

        private val OFFSET = "offset"
        private val LIMIT = "limit"
        private val ORDER_ID = "orderId"
        private val USER_ID = "userId"
        private val CAMPAIGN_ID = "campaignId"
        private val PAGE_TYPE_CODE = "pageTypeCode"
    }
}

package ru.planeta.dao.trashcan

import org.springframework.stereotype.Repository
import ru.planeta.dao.BaseDAO
import ru.planeta.dao.Statements
import ru.planeta.model.trashcan.PromoEmail

@Repository
class PromoEmailDAOImpl : BaseDAO<PromoEmail>(), PromoEmailDAO {

    override fun insert(email: PromoEmail) {
        insert(INSERT, email)
    }

    override fun selectSentEmailsCountForUser(email: String, promoId: Long): Int {
        return selectUnique(SELECT_EMAILS_COUNT, getParameters("email", email, "promoId", promoId)) as Int
    }

    companion object {
        private val INSERT = Statements.TRASHCAN + ".insertPromoEmail"
        private val SELECT_EMAILS_COUNT = Statements.TRASHCAN + ".selectSentEmailsCountForUser"
    }
}

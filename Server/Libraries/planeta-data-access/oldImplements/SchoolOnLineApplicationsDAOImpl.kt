package ru.planeta.dao.trashcan

import org.springframework.stereotype.Repository
import ru.planeta.dao.BaseDAO
import ru.planeta.dao.Statements
import ru.planeta.model.trashcan.SchoolOnlineCourseApplication

import java.util.Date

@Repository
class SchoolOnLineApplicationsDAOImpl : BaseDAO<*>(), SchoolOnLineApplicationsDAO {

    override fun insert(onlineCourseApplication: SchoolOnlineCourseApplication) {
        onlineCourseApplication.timeAdded = Date()
        insert(Statements.SchoolOnlineApplications.INSERT_SCHOOL_ONLINE_APPLICATION, onlineCourseApplication)
    }

    override fun select(email: String): SchoolOnlineCourseApplication {
        return selectOne(Statements.SchoolOnlineApplications.SELECT_SCHOOL_ONLINE_APPLICATION_BY_EMAIL, getParameters(EMAIL, email)) as SchoolOnlineCourseApplication
    }

    companion object {
        private val EMAIL = "email"
    }
}

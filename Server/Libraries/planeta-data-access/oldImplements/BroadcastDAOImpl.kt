package ru.planeta.dao.profiledb

class BroadcastDAOImpl /*: BaseClusterDAO<*>(), BroadcastDAO */{
//    override fun selectByProfileId(profileId: Long, offset: Int, limit: Int): List<Broadcast> {
//        return selectCampaignById(Statements.Broadcast.SELECT_BY_PROFILE_ID, createBuilder(profileId, ObjectType.BROADCAST, offset, limit).build())
//    }
//
//    override fun selectByOrderId(broadcastId: Long): Broadcast {
//        return selectOne(Statements.Broadcast.SELECT_BY_ID, BaseDAO.getParameters("objectId", broadcastId)) as Broadcast
//    }
//
//    override fun selectByIdList(idList: List<Long>): List<Broadcast> {
//        return if (CollectionUtils.isEmpty(idList)) {
//            ArrayList()
//        } else selectCampaignById(Statements.Broadcast.SELECT_BY_ID_LIST, BaseDAO.getParameters(OBJECT_TYPE, ObjectType.BROADCAST.code, "idList", idList))
//    }
//
//    private fun getAdditionalSearchParams(searchString: String, params: AllShardsParameters) {
//        if (!StringUtils.isEmpty(searchString)) {
//            val id = NumberUtils.toLong(searchString)
//            if (id > 0) {
//                params.put(BROADCAST_ID, id)
//            } else {
//                params.put(SEARCH_STRING, searchString)
//            }
//        }
//    }
//
//    override fun selectForAdmin(searchString: String, dateFrom: Date, dateTo: Date, offset: Long, limit: Int): List<Broadcast> {
//        val params = createAllShardsBuilder()
//                .add(DATE_FROM, dateFrom)
//                .add(DATE_TO, dateTo)
//                .add(OFFSET, offset)
//                .add(LIMIT, limit).build()
//
//        getAdditionalSearchParams(searchString, params)
//        return selectCampaignById(Statements.Broadcast.SELECT_FOR_ADMIN, params)
//    }
//
//    override fun selectForAdminCount(searchString: String, dateFrom: Date, dateTo: Date): Long {
//        val params = createAllShardsBuilder()
//                .add(DATE_FROM, dateFrom)
//                .add(DATE_TO, dateTo).build()
//        getAdditionalSearchParams(searchString, params)
//        return selectOne(Statements.Broadcast.SELECT_FOR_ADMIN_COUNT, params) as Long
//    }
//
//
//    override fun insert(broadcast: Broadcast) {
//        if (broadcast.broadcastId == 0L) {
//            broadcast.broadcastId = sequencesDAO.selectNextLong(Broadcast::class.java)
//        }
//        insert(Statements.Broadcast.INSERT, broadcast)
//    }
//
//    override fun update(broadcast: Broadcast) {
//        update(Statements.Broadcast.UPDATE, broadcast)
//    }
//
//    override fun updateBroadcastViewsCount(defaultStreamId: Long) {
//        val params = createAllShardsBuilder()
//                .add(DEFAULT_STREAM_ID, defaultStreamId)
//                .build()
//        update(Statements.Broadcast.UPDATE_BROADCAST_VIEWS_COUNT, params)
//    }
//
//    override fun deleteByProfileId(broadcastId: Long) {
//        deleteByProfileId(Statements.Broadcast.DELETE, BaseDAO.getParameters("objectId", broadcastId))
//    }
//
//    override fun selectEndedByTime(time: Long): List<Broadcast> {
//        val params = createAllShardsBuilder()
//                .add(OBJECT_TYPE, ObjectType.BROADCAST.code)
//                .add(DATE_TO, Date(time))
//                .add(BROADCAST_STATUS_CODE, BroadcastStatus.LIVE.code).build()
//        return selectCampaignById(Statements.Broadcast.SELECT_ENDED_BY_TIME_ALL_SHARDS, params)
//    }
//
//    override fun selectStartedByTime(time: Long): List<Broadcast> {
//        val params = createAllShardsBuilder()
//                .add(OBJECT_TYPE, ObjectType.BROADCAST.code)
//                .add(DATE_FROM, Date(time))
//                .add(BROADCAST_STATUS_CODE, BroadcastStatus.NOT_STARTED.code).build()
//        return selectCampaignById(Statements.Broadcast.SELECT_STARTED_BY_TIME_ALL_SHARDS, params)
//    }
//
//    override fun selectUpcomingBroadcasts(dateTo: Date, onlyDashboard: Boolean, offset: Int, limit: Int): List<Broadcast> {
//        val params = createAllShardsBuilder()
//                .add(OBJECT_TYPE, ObjectType.BROADCAST.code)
//                .add(DATE_TO, dateTo)
//                .add("onlyDashboard", onlyDashboard)
//                .add(OFFSET, offset)
//                .add(LIMIT, limit).build()
//        return selectCampaignById(Statements.Broadcast.SELECT_UPCOMING_BROADCASTS, params)
//    }
//
//    override fun selectArchivedBroadcasts(broadcastCategory: BroadcastCategory?, dateTo: Date, isPopularSort: Boolean, offset: Int, limit: Int): List<Broadcast> {
//        val broadcastCategoryCode = broadcastCategory?.code
//        val params = createAllShardsBuilder()
//                .add(OBJECT_TYPE, ObjectType.BROADCAST.code)
//                .add(BROADCAST_CATEGORY_CODE, broadcastCategoryCode)
//                .add(DATE_TO, dateTo)
//                .add("isPopularSort", isPopularSort)
//                .add(OFFSET, offset)
//                .add(LIMIT, limit).build()
//        return selectCampaignById(Statements.Broadcast.SELECT_ARCHIVED_BROADCASTS, params)
//    }

}

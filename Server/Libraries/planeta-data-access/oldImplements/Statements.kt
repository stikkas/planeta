package ru.planeta.dao

/**
 * Contains statements names
 *
 * @author ameshkov
 */
object Statements {

    /**
     * CommonDb mybatis namespace
     */
    val COMMONDB = "cluster.commondb"

    /**
     * promodb mybatis namespace
     */
    val PROMODB = "cluster.promodb"

    /**
     * Biblio mybatis namespace
     */
    private val BIBLIO = "cluster.bibliodb"

    /**
     * Concert mybatis namespace
     */
    val CONCERTDB = "cluster.concertdb"

    val TRASHCAN = "cluster.trashcan"

    /**
     * MsgDb mybatis namespace
     */
    val MSGDB = "cluster.msgdb"

    /**
     * StatDb mybatis namespace
     */
    val STATDB = "cluster.statdb"
    /**
     * ProfileDb mybatis namespace
     */
    val PROFILEDB = "cluster.profiledb"

    /**
     * ShopDb mybatis namespace
     */
    val SHOPDB = "cluster.shopdb"

    /**
     * CharityDB mybatis namespace
     */
    val CHARITYDB = "cluster.charitydb"

    val MAILDB = "cluster.maildb"

    interface InvoiceBill {
        companion object {

            val SELECT = "$COMMONDB.selectInvoiceBills"
        }
    }

    interface Sponsor {
        companion object {

            val SELECT = "$COMMONDB.selectSponsor"
            val SELECT_SPONSORS = "$COMMONDB.selectSponsors"
            val SELECT_ALL = "$COMMONDB.selectAllSponsors"
            val INSERT = "$COMMONDB.insertSponsor"
            val UPDATE = "$COMMONDB.updateSponsor"
            val DELETE = "$COMMONDB.deleteSponsor"
        }
    }

    interface AuthToken {
        companion object {
            val SAVE = "$COMMONDB.insertToken"
            val DELETE = "$COMMONDB.deleteToken"
            val FIND_BY_TOKEN = "$COMMONDB.selectToken"
        }
    }

    interface Campus {
        companion object {

            val SELECT = "$PROMODB.selectCampus"
            val SELECT_ALL = "$PROMODB.selectAllCampuses"
            val INSERT = "$PROMODB.insertCampus"
            val UPDATE = "$PROMODB.updateCampus"
        }
    }

    interface CampaignStat {
        companion object {

            val SELECT_GENERAL = "$STATDB.selectCampaignStatGeneral"
            val SELECT_GENERAL_TOTAL_STATS = "$STATDB.selectCampaignGeneralTotalStats"
            val SELECT_VISITORS_AND_COLLECTED_AMOUNT_ON_DATE = "$STATDB.selectCampaignVisitorAndCollectedAmountOnDate"
            val SELECT_REFERER = "$STATDB.selectCampaignStatReferer"
            val SELECT_COUNTRY = "$STATDB.selectCampaignStatCountry"
            val SELECT_CITY = "$STATDB.selectCampaignStatCity"
            val INSERT_GENERAL_STATS = "$STATDB.insertCampaignStatGeneral"
            val INSERT_REFERER_STATS = "$STATDB.insertCampaignStatReferer"
            val INSERT_COUNTRY_STATS = "$STATDB.insertCampaignStatCountry"
            val INSERT_CITY_STATS = "$STATDB.insertCampaignStatCity"
        }
    }

    interface CampaignEditTime {
        companion object {

            val INSERT = "$COMMONDB.insertCampaignEditTime"
            val UPDATE = "$COMMONDB.updateCampaignEditTime"
            val SELECT_LAST = "$COMMONDB.selectCampaignEditTimeLast"
            val EXISTS = "$COMMONDB.existsCampaignEditTimeWithId"
            val DELETE = "$COMMONDB.deleteCampaignEditTime"
        }
    }

    interface PaymentMethod {
        companion object {

            val SELECT = "$COMMONDB.selectPaymentMethod"
            val INSERT = "$COMMONDB.insertPaymentMethod"
            val UPDATE = "$COMMONDB.updatePaymentMethod"
            val UPDATE_PROJECT_PAYMENT_METHOD = "$COMMONDB.updateProjectPaymentMethodByMethodIdAndProjectType"
            val SELECT_FOR_PROJECT = "$COMMONDB.selectPaymentMethodsForProjectType"
            val SELECT_FOR_PROJECT_OLD = "$COMMONDB.selectPaymentMethodsForProjectTypeOld"
            val SELECT_ALL = "$COMMONDB.selectAllPaymentMethods"
            val SELECT_BY_ALIAS = "$COMMONDB.selectPaymentMethodsByAlias"
        }
    }

    interface PaymentProvider {
        companion object {

            val SELECT = "$COMMONDB.selectPaymentProvider"
            val UPDATE = "$COMMONDB.updatePaymentProvider"
            val INSERT = "$COMMONDB.insertPaymentProvider"
            val SELECT_BY_TYPE = "$COMMONDB.selectProviderByType"
            val SELECT_ALL = "$COMMONDB.selectAllPaymentProviders"
        }
    }

    interface PaymentTool {
        companion object {

            val SELECT = "$COMMONDB.selectPaymentProviderTool"
            val SELECT_ALL = "$COMMONDB.selectAllPaymentTools"
            val UPDATE = "$COMMONDB.updatePaymentProviderTool"
            val INSERT = "$COMMONDB.insertPaymentProviderTool"
            val SELECT_FOR_METHOD_AND_PROJECT = "$COMMONDB.selectPaymentProviderToolsForMethod"
        }
    }

    interface ProjectPaymentTool {
        companion object {

            val SELECT_ALL = "$COMMONDB.selectAllProjectPaymentTools"
            val SELECT_BY_ID = "$COMMONDB.selectProjectPaymentToolById"
            val INSERT = "$COMMONDB.insertProjectPaymentTool"
            val UPDATE = "$COMMONDB.updateProjectPaymentTool"
            val SELECT_BY_PAYMENT_METHOD_ID = "$COMMONDB.selectProjectPaymentToolsByPaymentMethodId"
        }
    }

    interface StatEvent {
        companion object {

            val INSERT = "$COMMONDB.insertStatEvent"
            val CLEAN = "$COMMONDB.cleanStatEvents"
        }
    }

    interface StatOrderGeo {
        companion object {

            val INSERT = "$STATDB.insertStatOrderGeo"
            val UPDATE = "$STATDB.updateStatOrderGeo"
            val SELECT = "$STATDB.selectStatOrderGeoByOrderId"
            val SELECT_UNRESOLVED = "$STATDB.selectStatOrderGeoUnresolved"
        }
    }

    interface OrderShortLinkStat {
        companion object {
            val INSERT = "$STATDB.insertOrderShortLinkStat"
            val SELECT_BY_ID = "$STATDB.selectOrderShortLinkStatById"
        }
    }

    interface CampaignAfterPurchaseMailStat {
        companion object {
            val INSERT = "$STATDB.insertCampaignAfterPurchaseMailStat"
            val UPDATE = "$STATDB.updateCampaignAfterPurchaseMailStat"
            val SELECT_BY_USER_ID = "$STATDB.selectCampaignAfterPurchaseMailStatByUserId"
            val SELECT_USERS_FOR_SENDING_MAIL = "$STATDB.selectUsersForSendingMail"
        }
    }

    interface ProfileNews {
        companion object {

            val INSERT = "$PROFILEDB.insertProfileNews"
            val SELECT_FOR_PROFILE = "$PROFILEDB.selectProfileNewsForProfile"
            val SELECT_COUNT_FOR_PROFILE = "$PROFILEDB.selectProfileNewsCountForProfile"
            val SELECT_PROFILE_NEWS_BY_DATE = "$PROFILEDB.selectProfileNewsByDate"
            val SELECT_FOR_GUEST = "$PROFILEDB.selectProfileNewsForGuest"
            val SELECT_CAMPAIGN_NEWS = "$PROFILEDB.selectCampaignNews"
            val MARK_DELETED = "$PROFILEDB.markProfileNewsDeleted"
            val SELECT_BY_ID = "$PROFILEDB.selectProfileNewsById"
            val SELECT = "$PROFILEDB.selectProfileNews"
        }
    }

    interface Post {
        companion object {

            val INSERT = "$PROFILEDB.insertPost"
            val UPDATE = "$PROFILEDB.updatePost"
            val UPDATE_CHARITY_POST = "$PROFILEDB.updateCharityPost"
            val SELECT = "$PROFILEDB.selectPost"
            val SELECT_LIST = "$PROFILEDB.selectPosts"
            val SELECT_COUNT = "$PROFILEDB.selectPostsCount"
            val SELECT_LIST_BY_IDS = "$PROFILEDB.selectPostsByIds"
            val SELECT_BY_CAMPAIGN_ID = "$PROFILEDB.selectPostsByCampaignId"
            val SELECT_BY_PROFILE_ID = "$PROFILEDB.selectPromoPostsByProfileId"
            val SELECT_CAMPAIGN_POSTS_COUNT = "$PROFILEDB.selectCampaignPostsCount"
            val SELECT_ID = "$PROFILEDB.selectId"
            val SELECT_CAMPAIGN_POSTS_LAST_NOTIFICATION_TIME = "$PROFILEDB.selectCampaignPostsLastNotificationTime"
            val SELECT_CHARITY_NEWS = "$PROFILEDB.selectCharityNews"
            val SELECT_FUTURE_CHARITY_POSTS = "$PROFILEDB.selectFutureCharityPosts"
            val SELECT_CHARITY_POSTS_BY_ID_OR_ALL = "$PROFILEDB.selectCharityPostsById"
            val COUNT_CHARITY_POSTS = "$PROFILEDB.countCharityPosts"
            val DELETE_CHARITY_POST = "$PROFILEDB.deleteCharityPost"
        }
    }

    //TODO: может быть на enum'ы все эти константы заменить или часть хотя бы?
    object UserPrivateInfo {

        val SELECT_BY_EMAIL = "$COMMONDB.selectUserPrivateInfoByEmail"
        val SELECT_BY_USERNAME = "$COMMONDB.selectUserPrivateInfoByUsername"
        val SELECT_LIKE_EMAIL = "$COMMONDB.selectUserPrivateInfoListWhereEmailLike"
        val SELECT_BY_USERID = "$COMMONDB.selectUserPrivateInfo"
        val SELECT_BY_REGCODE = "$COMMONDB.selectUserPrivateInfoByRegCode"
        val INSERT = "$COMMONDB.insertUserPrivateInfo"
        val UPDATE = "$COMMONDB.updateUserPrivateInfo"
        val DELETE = "$COMMONDB.deleteUserPrivateInfo"
        val SELECT_PLANETA_ADMINS = "$COMMONDB.selectPlanetaAdminsList"
        val SELECT_NOT_NULL_REG_CODE = "$COMMONDB.selectUserPrivateInfoNotNullRegCode"
        val SELECT_NULL_REG_CODE = "$COMMONDB.selectUserPrivateInfoNullRegCode"
        val SELECT_BY_TIME_ADDED = "$COMMONDB.selectUserPrivateInfoByTimeAdded"
        val SELECT_BY_USERS_IDS = "$COMMONDB.selectUsersPrivateInfoByIds"
        val SELECT_USERS_COUNT = "$COMMONDB.selectUsersCount"
        val SELECT_SUBSCRIBERS = "$COMMONDB.selectUserPrivateSubscribers"
    }

    interface Advertising {
        companion object {

            val MERGE = "$COMMONDB.mergeAdvertising"
            val DELETE = "$COMMONDB.deleteAdvertising"
            val SELECT = "$COMMONDB.selectAdvertising"
            val SELECT_BY_ACTIVE_DATE = "$COMMONDB.selectAdvertisingByActiveDate"
            val SELECT_BY_INACTIVE_DATE = "$COMMONDB.selectInactiveAdvertisingByActiveDate"
            val SELECT_BY_CAMPAIGN_ID = "$COMMONDB.selectAdvertisingByCampaignId"
        }
    }

    object UserCredentials {

        val INSERT = "$COMMONDB.insertUserCredentials"
        val DELETE_BY_PROFILE_ID = "$COMMONDB.deleteUserCredentialsByProfileId"
        val DELETE_BY_USERNAME = "$COMMONDB.deleteUserCredentialsByUsername"
        val SELECT_BY_USERNAMES = "$COMMONDB.selectUserCredentialsByUsernames"
        val SELECT_BY_USERNAME_AND_TYPE = "$COMMONDB.selectUserCredentialsByUsernameAndType"
        val UPDATE = "$COMMONDB.updateUserCredentials"
        val SELECT = "$COMMONDB.selectUserCredentials"
        val SELECT_BY_PROFILE_ID = "$COMMONDB.selectUserCredentialsByProfileId"
        val SELECT_BY_PROFILE_IDS = "$COMMONDB.selectUserCredentialsByProfileIds"
        val SELECT_REGISTERED_IDS_FROM_TYPE = "$COMMONDB.selectUserCredentialsFromType"
    }

    object GeoLocation {

        val INSERT_NEW_CITY = "$PROFILEDB.insertNewCity"
        val SELECT_COUNTRIES = "$PROFILEDB.selectCountries"
        val SELECT_COUNTRIES_BY_CAMPAIGNS = "$PROFILEDB.selectCountriesByCampaigns"
        val SELECT_COUNTRY_BY_REGION = "$PROFILEDB.selectCountriesByRegion"
        val SELECT_COUNTRIES_BY_IDS = "$PROFILEDB.selectCountriesByIds"
        val SELECT_CITIES_BY_SUBSTRING = "$PROFILEDB.selectCitiesBySubstring"
        val SELECT_REGIONS_BY_SUBSTRING = "$PROFILEDB.selectRegionsBySubstring"
        val SELECT_CITIES_BY_RUS_NAME = "$PROFILEDB.selectCitiesByRusName"
        val SELECT_CITIES_BY_ENG_NAME = "$PROFILEDB.selectCitiesByEngName"
        val SELECT_CITIES_WITH_GEO = "$PROFILEDB.selectCitiesWithGeo"
        val SELECT_CITY = "$PROFILEDB.selectCity"
        val SELECT_REGION = "$PROFILEDB.selectRegion"
        val SELECT_GLOBAL_REGIONS = "$PROFILEDB.selectGlobalRegions"
        val SELECT_GLOBAL_REGION = "$PROFILEDB.selectGlobalRegion"
        val SELECT_REGION_CITIES_BY_SUBSTRING = "$PROFILEDB.selectRegionCitiesBySubstring"
    }

    /**
     * Contains sequences statements
     */
    object Sequences {

        val SELECT_NEXT_INT = "$COMMONDB.selectNextInt"
        val SELECT_NEXT_LONG = "$COMMONDB.selectNextLong"
    }


    object UpdateStats {

        val UPDATE_COMMENTS_STATS = "$STATDB.updateCommentsStats"
    }

    object Contractor {

        val SELECT_LIST = "$COMMONDB.selectContractorList"
        val SELECT_LIST_BY_PROFILE = "$COMMONDB.selectContractorListByProfile"
        val SELECT = "$COMMONDB.selectContractorById"
        val SELECT_BY_NAME = "$COMMONDB.selectContractorByName"
        val SELECT_BY_INN = "$COMMONDB.selectContractorByInn"
        val UPDATE = "$COMMONDB.updateContractor"
        val INSERT = "$COMMONDB.insertContractor"
        val DELETE = "$COMMONDB.deleteContractor"

        val INSERT_RELATION = "$COMMONDB.insertContractorRelation"
        val DELETE_RELATION = "$COMMONDB.deleteContractorRelation"
        val DELETE_ALL_RELATIONS_BY_CONTRACTOR_ID = "$COMMONDB.deleteAllContractorRelationsByContractorId"
        val SELECT_BY_CAMPAIGN_ID = "$COMMONDB.selectContractorByCampaignId"
        val SELECT_CONTRACTORS_SEARCH = "$COMMONDB.selectContractorsSearch"
        val SELECT_CONTRACTORS_SEARCH_TOTAL_COUNT = "$COMMONDB.selectContractorsSearchTotalCount"
        val UPDATE_RELATION = "$COMMONDB.updateContractorRelation"
    }

    object CreditTransaction {

        val SELECT_LIST = "$COMMONDB.selectCreditTransactions"
        val SELECT = "$COMMONDB.selectCreditTransaction"
        val UPDATE = "$COMMONDB.updateCreditTransaction"
        val INSERT = "$COMMONDB.insertCreditTransaction"
        val DELETE = "$COMMONDB.deleteCreditTransaction"
    }

    object DebitTransaction {

        val SELECT_LIST = "$COMMONDB.selectDebitTransactions"
        val SELECT = "$COMMONDB.selectDebitTransaction"
        val UPDATE = "$COMMONDB.updateDebitTransaction"
        val INSERT = "$COMMONDB.insertDebitTransaction"
        val DELETE = "$COMMONDB.deleteDebitTransaction"
    }

    object MoneyTransaction {

        val SELECT_MONEY_TRANSACTION = "$COMMONDB.selectMoneyTransactions"
    }

    object Banner {

        val SELECT = "$COMMONDB.selectBanner"
        val SELECT_LIST = "$COMMONDB.selectBannerList"
        val UPDATE = "$COMMONDB.updateBanner"
        val INSERT = "$COMMONDB.insertBanner"
        val DELETE = "$COMMONDB.deleteBanner"
        val SET_SHOW_COUNTER = "$COMMONDB.setShowCounter"
    }

    object Stats {
        val SELECT_WELCOME_STATS = "$COMMONDB.selectWelcomeStats"
    }

    object ShortLink {

        val SELECT = "$COMMONDB.selectShortLinkById"
        val SELECT_BY_ALIAS = "$COMMONDB.selectShortLinkByAlias"
        val SELECT_LIST = "$COMMONDB.selectShortLinkList"
        val SELECT_LIST_BY_PROFILE_ID = "$COMMONDB.selectShortLinksByProfileId"
        val SELECT_EXISTS_SOCIAL_SHORTLINKS_BY_BASENAME = "$COMMONDB.selectExistsSocialShortLinksByBasename"
        val UPDATE = "$COMMONDB.updateShortLink"
        val INSERT = "$COMMONDB.insertShortLink"
        val DELETE = "$COMMONDB.deleteShortLink"
    }

    object Configuration {

        val SELECT = "$COMMONDB.selectConfiguration"
        val SELECT_LIST = "$COMMONDB.selectConfigurations"
        val INSERT = "$COMMONDB.insertConfiguration"
        val UPDATE = "$COMMONDB.updateConfiguration"
        val DELETE = "$COMMONDB.deleteConfiguration"
    }

    object Order {

        val UPDATE_BACKED_COUNT = "$COMMONDB.updateProfileBackedCount"
        val SELECT_ORDERS = "$COMMONDB.getMerchantOrdersInfo"
        val SELECT_ORDERS_AFTER_TIME = "$COMMONDB.selectOrdersAfterTime"
        val SELECT_SEARCH_PURCHASES_ORDER = "$COMMONDB.selectSearchPurchasesOrder"
        val SELECT_ORDER_INFO_FOR_OHM = "$COMMONDB.selectOrderInfoForOneHundredMillionPage"
        val SELECT_ORDERS_BY_BUYER_CREDIT_TRANSACTION_IDS = "$COMMONDB.selectOrdersByBuyerCreditTransactionIds"
        val SELECT_ORDERS_BY_BUYER_DEBIT_TRANSACTION_IDS = "$COMMONDB.selectOrdersByBuyerDebitTransactionIds"

        val SELECT_USER_ORDERS_COUNT = "$COMMONDB.selectUserOrdersCount"
        val SELECT = "$COMMONDB.selectOrder"
        val SELECT_FOR_UPDATE = "$COMMONDB.selectOrderForUpdate"
        val UPDATE = "$COMMONDB.updateOrder"
        val INSERT = "$COMMONDB.insertOrder"
        val DELETE = "$COMMONDB.deleteOrder"
        val SELECT_ORDERS_BY_OBJECT = "$COMMONDB.selectOrdersByObject"
        val SELECT_ORDERS_FOR_CAMPAIGN = "$COMMONDB.selectOrdersForCampaign"
        val SELECT_CAMPAIGN_ORDERS_FOR_REPORT = "$COMMONDB.selectCampaignOrdersForReport"
        val SELECT_ORDERS_COUNT_FOR_OBJECT = "$COMMONDB.selectOrdersCountForObject"
        val SELECT_MIN_COMPLETED_ORDER_ID = "$COMMONDB.selectMinCompletedOrderId"
        val SELECT_ORDER_INFO_COMMON_DATA_SIMPLE = "$COMMONDB.selectOrderInfoCommonData"
        val SELECT_ORDER_INFO_COMMON_DATA_FULL = "$COMMONDB.selectOrderInfoCommonDataFull"

        val SELECT_ORDERS_MERCHANT_COUNT = "$COMMONDB.selectOrdersMerchantCount"
        val SELECT_ORDERS_BY_IDS = "$COMMONDB.selectOrdersByIds"
        val IS_CHARITY_ORDER = "$COMMONDB.isCharityOrder"

        val SELECT_ORDERS_REWARD_INFO = "$COMMONDB.selectUserOrdersRewardsInfo"
        val SELECT_ORDERS_CHARITY_INFO = "$COMMONDB.selectUserOrdersCharityInfo"

        val SELECT_PURCHASED_BOOKS_COUNT = "$COMMONDB.selectPurchasedBooksCount"
        val SELECT_PURCHASED_BOOKS_SUM = "$COMMONDB.selectPurchasedBooksSum"

        val SELECT_CAMPAIGN_ID_BY_ORDER_ID = "$COMMONDB.selectCampaignIdByOrderId"

    }

    object ProfileBalance {

        val SELECT = "$COMMONDB.selectProfileBalance"
        val GET_LK = "$COMMONDB.getPersonalCabinet"
        val SELECT_FOR_UPDATE = "$COMMONDB.selectForUpdateProfileBalance"
        val UPDATE = "$COMMONDB.updateProfileBalance"
        val SELECT_FROZEN_AMOUNT = "$COMMONDB.selectFrozenAmount"
        val UPDATE_FROZEN_AMOUNT = "$COMMONDB.updateFrozenAmount"
        val INSERT = "$COMMONDB.insertProfileBalance"
        val DELETE = "$COMMONDB.deleteProfileBalance"
    }

    object SuperAdminUtils {

        val DELETE_PROFILE = "$COMMONDB.deleteProfileAndProfileData"
        val DECREASE_BALANCE = "$COMMONDB.decreaseProfileBalance"
        val INCREASE_BALANCE = "$COMMONDB.increaseProfileBalance"
        val DELETE_USER_COMMENTS = "$COMMONDB.deleteUserCommets"
        val DELETE_USER_POSTS = "$COMMONDB.deleteUserPosts"
    }

    object Dialog {

        val SELECT = "$MSGDB.selectDialogById"
        val INSERT = "$MSGDB.insertDialog"
        val UPDATE = "$MSGDB.updateDialog"
    }

    object DialogUser {

        val SELECT = "$MSGDB.selectDialogUsers"
        val INSERT = "$MSGDB.insertDialogUser"
        val DELETE = "$MSGDB.deleteDialogUser"
    }

    object DialogMessage {

        val SELECT = "$MSGDB.selectDialogMessage"
        val SELECT_BY_DIALOG = "$MSGDB.selectMessagesByDialog"
        val SELECT_LAST_BY_DIALOG = "$MSGDB.selectLastMessagesByDialog"
        val SELECT_SPAMMERS = "$MSGDB.selectSpammers"
        val INSERT = "$MSGDB.insertDialogMessage"
        val UPDATE = "$MSGDB.updateDialogMessage"
        val SELECT_LAST_MESSAGE = "$MSGDB.selectLastMessage"
        val SELECT_LAST_MESSAGE_ID_BY_DIALOG = "$MSGDB.selectLastMessageIdByDialog"
    }

    object SharedObject {

        val SELECT_BY_ID = "$STATDB.selectSharedObjectById"
        val SELECT_BY_URL = "$STATDB.selectSharedObjectByUrl"
        val SELECT_SHARED_OBJECT_DETAILS_FOR_PROFILE = "$STATDB.selectSharedObjectDetailsForProfile"
        val SELECT_LAST_SHARED_OBJECT_DETAILS = "$STATDB.selectLastSharedObjectDetails"
        val INSERT = "$STATDB.insertSharedObject"
        val INSERT_DETAILS = "$STATDB.insertSharedObjectDetails"
        val UPDATE = "$STATDB.updateSharedObjectCount"
    }

    object StaticNodeStat {

        val SELECT_ALL = "$STATDB.selectStatStaticNode"
        val INSERT = "$STATDB.insertStatStaticNode"
        val UPDATE_FREE_SPACE_KB = "$STATDB.updateStatStaticNodeFreeSpaceKB"
    }

    object DBLog {

        val SELECT_LOG_RECORDS = "$STATDB.selectLogRecords"
        val SELECT_HTTP_INTERACTION_LOG_BY_RECORD_IDS = "$STATDB.selectHttpInteractionLogRecordByIds"
    }

    object Profile {

        val SEARCH_PROFILES = "$PROFILEDB.searchProfiles"
        val SELECT_BY_ID = "$PROFILEDB.selectProfileById"
        val SELECT_WITH_DELETED_BY_ID = "$PROFILEDB.selectProfileWithDeletedById"
        val SELECT_BY_ALIAS = "$PROFILEDB.selectProfileByAlias"
        val SELECT_BY_ID_LIST = "$PROFILEDB.selectProfilesByIdList"
        val SELECT_BY_STATUS_TYPE_SEARCH_STRING = "$PROFILEDB.selectProfilesByStatusTypeSearchString"
        val INSERT = "$PROFILEDB.insertProfile"
        val UPDATE = "$PROFILEDB.updateProfile"
        val DELETE = "$PROFILEDB.deleteProfile"
        val SELECT_HIDDEN_GROUP = "$PROFILEDB.selectHiddenGroup"
        val SELECT_PROFILES_COUNT = "$PROFILEDB.selectProfilesCount"
        val SELECT_SEARCHED_BY_ID_LIST = "$PROFILEDB.selectProfilesSearchedByIdList"
        val UPDATE_BACKED_COUNT = "$PROFILEDB.updateProfileBackedCount"
        val UPDATE_PROJECTS_COUNT = "$PROFILEDB.updateProfileProjectsCount"
        val UPDATE_HAS_ACTIVE_PROJECTS = "$PROFILEDB.updateProfileHasActiveProjects"
        val UPDATE_SUBSCRIBERS_COUNT = "$PROFILEDB.updateProfileSubscribersCount"
        val UPDATE_NEW_SUBSCRIBERS_COUNT = "$PROFILEDB.updateProfileNewSubscribersCount"
        val SELECT_SHOP_PRODUCTS_PROFILES = "$PROFILEDB.selectShopProductsProfiles"

        val SELECT_SPAMMERS = "$PROFILEDB.selectSpammers"
    }

    object Group {

        val SELECT = "$PROFILEDB.selectGroupByProfileId"
        val INSERT = "$PROFILEDB.insertGroup"
        val UPDATE = "$PROFILEDB.updateGroup"
        val DELETE = "$PROFILEDB.deleteGroup"
    }

    object ProfileUser {

        val SELECT = "$PROFILEDB.selectUser"
        val INSERT = "$PROFILEDB.insertUser"
        val UPDATE = "$PROFILEDB.updateUser"
        val UPDATE_FRIENDS_COUNT = "$PROFILEDB.updateFriendsCount"
        val UPDATE_USERS_REQUEST_IN_COUNT = "$PROFILEDB.updateUsersRequestInCount"
        val UPDATE_USERS_REQUEST_OUT_COUNT = "$PROFILEDB.updateUsersRequestOutCount"
        val UPDATE_FRIENDS_GROUPS_COUNT = "$PROFILEDB.updateFriensGroupsCount"
        val DELETE = "$PROFILEDB.deleteUser"
    }

    object PhotoAlbum {

        val SELECT_PHOTO_ALBUM_BY_ID = "$PROFILEDB.selectPhotoAlbumById"
        val SELECT_PHOTO_ALBUM_BY_ID_WITH_PERMISSION = "$PROFILEDB.selectPhotoAlbumByIdWithPermission"
        val SELECT_PHOTO_ALBUM_BY_ALBUM_TYPE_ID = "$PROFILEDB.selectPhotoAlbumByAlbumTypeId"
        val SELECT_PHOTO_ALBUMS = "$PROFILEDB.selectPhotoAlbums"
        val INSERT = "$PROFILEDB.insertPhotoAlbum"
        val UPDATE = "$PROFILEDB.updatePhotoAlbum"
        val DELETE = "$PROFILEDB.deletePhotoAlbum"
        val UPDATE_PHOTO_ALBUMS_VIEWS_COUNT = "$PROFILEDB.updatePhotoAlbumViewsCount"
    }

    object AudioTrack {

        val SELECT_AUDIO_TRACK_BY_ID = "$PROFILEDB.selectAudioTrackById"
        val SELECT_AUDIO_TRACKS_COUNT = "$PROFILEDB.selectAudioTracksCount"
        val SELECT_AUDIO_TRACKS_BY_ALBUM_ID = "$PROFILEDB.selectAudioTracksByAlbumId"
        val SELECT_AUDIO_TRACKS_BY_PLAYLIST_ID = "$PROFILEDB.selectAudioTracksByPlaylistId"
        val SELECT_AUDIO_TRACK_BY_UPLOADER_TRACK_ID = "$PROFILEDB.selectAudioTrackByUploaderTrackId"
        val SELECT_AUDIO_TRACKS_BY_IDS_LIST = "$PROFILEDB.selectAudioTitlesByIdList"
        val INSERT = "$PROFILEDB.insertAudioTrack"
        val UPDATE = "$PROFILEDB.updateAudioTrack"
        val DELETE = "$PROFILEDB.deleteAudioTrack"
        val SELECT_AUDIO_TRACKS = "$PROFILEDB.selectAudioTracks"
        val UPDATE_AUDIO_TRACK_LISTENINGS_COUNT = "$PROFILEDB.updateAudioTrackListeningsCount"
        val UPDATE_AUDIO_TRACK_DOWNLOADS_COUNT = "$PROFILEDB.updateAudioTrackDownloadsCount"
    }

    object Photo {

        val SELECT_PHOTO_BY_ID = "$PROFILEDB.selectPhotoById"
        val SELECT_PHOTO_BY_ALBUM_ID = "$PROFILEDB.selectPhotoByAlbumId"
        val SELECT_PHOTO_COUNT_BY_ALBUM_ID = "$PROFILEDB.selectPhotoCountByAlbumId"
        val INSERT = "$PROFILEDB.insertPhoto"
        val UPDATE = "$PROFILEDB.updatePhoto"
        val DELETE = "$PROFILEDB.deletePhoto"
        val UPDATE_PHOTO_VIEWS_COUNT = "$PROFILEDB.updatePhotoViewsCount"
    }

    object ProfileBlockSetting {

        val SELECT = "$PROFILEDB.selectProfileBlockSetting"
        val SELECT_BY_PROFILE_ID = "$PROFILEDB.selectProfileBlockSettings"
        val INSERT = "$PROFILEDB.insertProfileBlockSetting"
        val UPDATE = "$PROFILEDB.updateProfileBlockSetting"
        val DELETE = "$PROFILEDB.deleteProfileBlockSetting"
    }

    object Video {

        val SELECT_VIDEO_BY_ID = "$PROFILEDB.selectVideoById"
        val SELECT_VIDEO = "$PROFILEDB.selectVideo"
        val SELECT_VIDEOS_BY_IDS = "$PROFILEDB.selectVideoTitlesByIdList"
        val SELECT_VIDEO_BY_CACHED_VIDEO_ID = "$PROFILEDB.selectVideoByCachedVideoId"
        val INSERT = "$PROFILEDB.insertVideo"
        val UPDATE = "$PROFILEDB.updateVideo"
        val DELETE = "$PROFILEDB.deleteVideo"
        val UPDATE_VIDEO_VIEWS_COUNT = "$PROFILEDB.updateVideoViewsCount"
    }

    object CachedVideo {

        val INSERT_VIDEO_CACHE = "$COMMONDB.insertCachedVideo"
        val UPDATE_VIDEO_CACHE = "$COMMONDB.updateCachedVideo"
        val SELECT_VIDEO_CACHE_BY_ID = "$COMMONDB.selectCachedVideoById"
        val SELECT_VIDEO_CACHE_BY_URL = "$COMMONDB.selectCachedVideoByUrl"
    }

    object ProfileCommunicationChannel {

        val SELECT_LIST = "$PROFILEDB.selectProfileCommunicationChannels"
        val SELECT = "$PROFILEDB.selectProfileCommunicationChannel"
        val INSERT = "$PROFILEDB.insertProfileCommunicationChannel"
        val UPDATE = "$PROFILEDB.updateProfileCommunicationChannel"
        val DELETE = "$PROFILEDB.deleteProfileCommunicationChannel"
        val DELETE_ALL = "$PROFILEDB.deleteProfileCommunicationChannels"
    }

    object Comment {

        val SELECT_COMMENTS = "$PROFILEDB.selectComments"
        val SELECT_COMMENTS2 = "$PROFILEDB.selectComments2"
        val SELECT_COMMENTS_WITH_CHILD_AS_PLAIN = "$PROFILEDB.selectCommentsWithChildAsPlain"
        val SELECT_COMMENT_BY_ID = "$PROFILEDB.selectCommentById"
        val SELECT_COMMENTS_COUNT = "$PROFILEDB.selectCommentsCount"
        val SELECT_COMMENTS_COUNT2 = "$PROFILEDB.selectCommentsCount2"
        val SELECT_COMMENTS_FOR_ADMIN_BY_DATE = "$PROFILEDB.selectProductCommentsForModeratorByDate"
        val SELECT_COMMENTS_FOR_ADMIN_BY_DATE_COUNT = "$PROFILEDB.selectProductCommentsForModeratorByDateCount"
        val INSERT = "$PROFILEDB.insertComment"
        val UPDATE = "$PROFILEDB.updateComment"
        val DELETE = "$PROFILEDB.deleteComment"
        val RESTORE = "$PROFILEDB.restoreComment"
        val GET_LAST_NOT_DELETED_COMMENT_ID = "$PROFILEDB.getLastNotDeletedCommentId"
        val SELECT_LAST_COMMENTS = "$PROFILEDB.selectLastComments"
    }

    object CommentObject {

        val SELECT_BY_ID = "$PROFILEDB.selectCommentObject"
        val SELECT_BY_PROFILE_ID = "$PROFILEDB.selectCommentObjects"
        val INSERT = "$PROFILEDB.insertCommentObject"
        val UPDATE = "$PROFILEDB.updateCommentObject"
        val DELETE = "$PROFILEDB.deleteCommentObject"
        val UPDATE_COMMENTS_COUNT = "$PROFILEDB.updateCommentsCount"
    }

    object PromoCode {

        val SELECT_CODE = "$SHOPDB.selectPromoCodeByCode"
        val SELECT_CODE_ID = "$SHOPDB.selectPromoCodeIdByCode"
        val SELECT_CODE_BY_ID = "$SHOPDB.selectPromoCodeById"
        val SELECT_CODE_BY_ID_EXTENDED = "$SHOPDB.selectExtendedPromoCodeById"
        val SELECT_LIST = "$SHOPDB.selectPromoCodesList"
        val SELECT_LIST_EXTENDED = "$SHOPDB.selectExtendedPromoCodesList"
        val SELECT_CODE_LIST_COUNT = "$SHOPDB.selectExtendedPromoCodesListCount"
        val INSERT = "$SHOPDB.insertPromoCode"
        val UPDATE = "$SHOPDB.updatePromoCode"
        val MARK_DELETED = "$SHOPDB.markPromoCodeDeleted"
        val DELETE = "$SHOPDB.deletePromoCode"
    }

    /**
     * Contains db statements for profile_dialogs table.
     */
    object ProfileDialog {

        val SELECT = "$PROFILEDB.selectProfileDialog"
        val SELECT_LIST = "$PROFILEDB.selectProfileDialogs"
        val SELECT_WITH_UNREAD = "$PROFILEDB.selectProfileDialogsWithUnreadMessages"
        val SELECT_BY_COMPANION = "$PROFILEDB.selectProfileDialogByCompanion"
        val INSERT = "$PROFILEDB.insertProfileDialog"
        val UPDATE = "$PROFILEDB.updateProfileDialog"
        val DELETE = "$PROFILEDB.deleteProfileDialog"
        val SELECT_DIALOG_INFO_ID_LIST = "$PROFILEDB.selectDialogInfoIdList"
        val SELECT_DIALOG_INFO_LIST = "$PROFILEDB.selectDialogInfoList"
    }

    object GeoBase {

        val SELECT_LIST = "$COMMONDB.selectGeoIps"
        val SELECT_ID_BY_ID = "$COMMONDB.selectGeoCityId"
        val SELECT_COUNTRY_ID_BY_CODE = "$COMMONDB.selectCountryIdByCode"
    }

    object TopayTransaction {

        val SELECT_LIST = "$COMMONDB.selectTopayTransactions"
        val SELECT = "$COMMONDB.selectTopayTransaction"
        val SELECT_PROFILE_LAST_TRANSACTION = "$COMMONDB.selectProfileLastTransaction"
        val UPDATE = "$COMMONDB.updateTopayTransaction"
        val INSERT = "$COMMONDB.insertTopayTransaction"
        val DELETE = "$COMMONDB.deleteTopayTransaction"
        val SELECT_ORDER_TRANSACTIONS = "$COMMONDB.selectOrderTransactions"
        val SELECT_FOR_POOLING = "$COMMONDB.selectTransactionsForPooling"
        val SELECT_MONTHLY_PAID_SUM = "$COMMONDB.selectMonthlyPaidSum"
        val SELECT_LAST_PAYMENT_BY_EXTERNAL_ID = "$COMMONDB.selectLastPaymentByExternalId"
        val GET_PREV_FAIL_TRANSACTION = "$COMMONDB.selectPrevFailTransaction"
    }

    object MailTemplate {

        val SELECT_LIST = "$COMMONDB.selectMailTemplates"
        val SELECT_BY_NAME = "$COMMONDB.selectMailTemplateByName"
        val SELECT = "$COMMONDB.selectMailTemplate"
        val UPDATE = "$COMMONDB.updateMailTemplate"
        val INSERT = "$COMMONDB.insertMailTemplate"
        val DELETE = "$COMMONDB.deleteMailTemplate"
    }

    object MailAttachment {

        val SELECT_LIST = "$COMMONDB.selectMailAttachments"
        val SELECT = "$COMMONDB.selectMailAttachment"
        val UPDATE = "$COMMONDB.updateMailAttachment"
        val INSERT = "$COMMONDB.insertMailAttachment"
        val DELETE = "$COMMONDB.deleteMailAttachment"
    }

    object Broadcast {

        val SELECT_FOR_ADMIN = "$PROFILEDB.selectBroadcastsForAdmin"
        val SELECT_FOR_ADMIN_COUNT = "$PROFILEDB.selectBroadcastsForAdminCount"
        val SELECT_BY_PROFILE_ID = "$PROFILEDB.selectBroadcastsByProfileId"
        val SELECT_BY_ID = "$PROFILEDB.selectBroadcastById"
        val SELECT_BY_ID_LIST = "$PROFILEDB.selectBroadcastsByIdList"
        val INSERT = "$PROFILEDB.insertBroadcast"
        val UPDATE = "$PROFILEDB.updateBroadcast"
        val DELETE = "$PROFILEDB.deleteBroadcast"
        val UPDATE_BROADCAST_VIEWS_COUNT = "$PROFILEDB.updateBroadcastViewsCount"
        val SELECT_STARTED_BY_TIME_ALL_SHARDS = "$PROFILEDB.selectBroadcastsToStartToByTime"
        val SELECT_ENDED_BY_TIME_ALL_SHARDS = "$PROFILEDB.selectBroadcastsToStopToByTime"
        val SELECT_UPCOMING_BROADCASTS = "$PROFILEDB.selectUpcomingBroadcasts"
        val SELECT_BROADCASTS_BY_PROFILE_IDS = "$PROFILEDB.selectBroadcastsByProfileIds"
        val SELECT_ARCHIVED_BROADCASTS = "$PROFILEDB.selectArchivedBroadcasts"
    }

    object BroadcastSubscription {

        val SELECT_BY_BROADCAST_ID = "$PROFILEDB.selectBroadcastSubscriptionByBroadcastId"
        val SELECT_BY_BROADCAST_ID_SUBSCRIBER_ID = "$PROFILEDB.selectBroadcastSubscriptionByBroadcastIdSubscriberId"
        val INSERT = "$PROFILEDB.insertBroadcastSubscription"
        val DELETE = "$PROFILEDB.deleteBroadcastSubscription"
    }

    object BroadcastGeoTargeting {

        val SELECT_BROADCAST_GEO_TARGETING = "$PROFILEDB.selectBroadcastGeoTargeting"
        val INSERT = "$PROFILEDB.insertBroadcastGeoTargeting"
        val DELETE = "$PROFILEDB.deleteBroadcastGeoTargeting"
        val DELETE_BY_COUNTRY_ID_CITY_ID = "$PROFILEDB.deleteBroadcastGeoTargetingByCountryIdCityId"
    }

    object BroadcastBackersTargeting {

        val SELECT_BROADCAST_BACKERS_TARGETING = "$PROFILEDB.selectBroadcastBackersTargeting"
        val INSERT = "$PROFILEDB.insertBroadcastBackersTargeting"
        val DELETE = "$PROFILEDB.deleteBroadcastBackersTargeting"
    }

    object BroadcastProductTargeting {
        val SELECT = "$PROFILEDB.selectBroadcastProductTargeting"
        val INSERT = "$PROFILEDB.insertBroadcastProductTargeting"
        val DELETE = "$PROFILEDB.deleteBroadcastProductTargeting"
    }


    object BroadcastPrivateTargeting {

        val SELECT_BROADCAST_PRIVATE_TARGETING = "$PROFILEDB.selectBroadcastPrivateTargeting"
        val SELECT_BROADCAST_PRIVATE_TARGETING_BY_LINK = "$PROFILEDB.selectBroadcastPrivateTargetingByLink"
        val INSERT = "$PROFILEDB.insertBroadcastPrivateTargeting"
        val UPDATE = "$PROFILEDB.updateBroadcastPrivateTargeting"
    }

    object BroadcastStream {

        val SELECT_BY_BROADCAST_ID = "$PROFILEDB.selectStreamsByBroadcastId"
        val SELECT_BY_ID = "$PROFILEDB.selectStreamById"
        val INSERT = "$PROFILEDB.insertStream"
        val UPDATE = "$PROFILEDB.updateStream"
        val DELETE = "$PROFILEDB.deleteStream"
        val DELETE_BY_BROADCAST_ID = "$PROFILEDB.deleteStreamsByBroadcastId"
    }

    object Campaign {

        val SELECT_LIST_WITH_SHARES = "$COMMONDB.selectCampaignsWithShares"
        val SELECT_LIST_BY_STATUS = "$COMMONDB.selectCampaignsByStatus"
        val SELECT_LIST_BY_STATUS_AND_MANAGER = "$COMMONDB.selectCampaignsByStatusAndManager"
        val SELECT_COUNT = "$COMMONDB.selectCampaignsCount"
        val SELECT_COUNT_BY_STATUSES = "$COMMONDB.selectCampaignsCountByStatuses"
        val SELECT_CAMPAIGNS_COUNT_PERCENTAGES_BY_CUSTOM_TAGS = "$COMMONDB.selectCampaignsCountPercentagesByCustomTags"
        val SELECT = "$COMMONDB.selectCampaignById"
        val SELECT_BY_ALIAS = "$COMMONDB.selectCampaignByAlias"
        val SELECT_FOR_UPDATE = "$COMMONDB.selectCampaignForUpdate"
        val SELECT_WITH_SHARES = "$COMMONDB.selectCampaignWithShares"
        val SELECT_WITH_SHARES_ORDER_BY_PRICE_DESC = "$COMMONDB.selectCampaignWithSharesOrderByPriceDesc"
        val INSERT = "$COMMONDB.insertCampaign"
        val UPDATE = "$COMMONDB.updateCampaign"
        val DELETE = "$COMMONDB.deleteCampaign"
        val SELECT_BACKER_COUNT = "$COMMONDB.selectCampaignBackerCount"
        val SELECT_BACKERS = "$COMMONDB.selectCampaignBackers"
        val SELECT_STARTED_BY_TIME = "$COMMONDB.selectStartedCampaignsByTime"
        val SELECT_FINISHED_BY_TIME = "$COMMONDB.selectFinishedCampaignsByTime"
        val SELECT_NOT_RENEWED_BACKGROUND = "$COMMONDB.selectNotRenewedBackground"
        val SELECT_BY_SPONSOR_ALIAS = "$COMMONDB.selectCampaignsBySponsorAlias"
        val SELECT_PURCHASED_CAMPAIGNS = "$COMMONDB.selectPurchasedCampaigns"
        val SELECT_CAMPAIGNS_BY_IDS = "$COMMONDB.selectCampaignsByIds"
        val SELECT_CAMPAIGNS_FOR_ADMIN_SEARCH = "$COMMONDB.selectCampaignForAdminSearch"
        val SELECT_CAMPAIGNS_FOR_ADMIN_SEARCH_COUNT = "$COMMONDB.selectCampaignForAdminSearchCount"

        val GET_GTM_CAMPAIGN_INFO_LIST = "$COMMONDB.getGtmCampaignInfoList"

        // for top stats
        val SELECT_TOTAL_COLLECTED_AMOUNT = "$COMMONDB.selectTotalPurchaseSumCampaigns"
        val SELECT_CAMPAIGNS_BY_DATE_RANGE = "$COMMONDB.selectCampaignsByDateRange"
        val SELECT_DRAFT_BY_OWNER_IDS = "$COMMONDB.selectDraftCampaignsByOwnerIds"

        val SELECT_CAMPAIGNS_BY_CONTRACTOR_ID = "$COMMONDB.selectCampaignsByContractorId"
        val SELECT_CAMPAIGNS_COUNT_WITH_PURCHASED_OR_RESERVED_SHARES = "$COMMONDB.selectCampaignsCountWithPurchasedOrReservedShares"
        val SELECT_BACKED_PROJECTS = "$COMMONDB.selectBackedProjects"
        val SELECT_USER_BACKED_CAMPAIGN_TAG_COUNT_LIST = "$COMMONDB.selectUserBackedCampaignTagCountList"
        val SELECT_TARGET_AMOUNT = "$COMMONDB.selectCampaignsTargetAmount"
        val SELECT_PURCHASED_AMOUNT = "$COMMONDB.selectCampaignsPurchasedAmount"
        val SELECT_ALL_TIME_PURCHASED_AMOUNT = "$COMMONDB.selectAllTimePurchasedAmount"
        val SELECT_CAMPAIGNS_COUNT_BETWEEN = "$COMMONDB.selectCampiagnsCountBetween"
        val SELECT_CAMPAIGN_NEW_EVENTS_COUNT = "$COMMONDB.selectCampaignNewEventsCount"
        val SELECT_CAMPAIGN_NEWS = "$COMMONDB.selectCampaignNews"
        val SELECT_CAMPAIGN_NEWS_COUNT = "$COMMONDB.selectCampaignNewsCount"
        val SELECT_CAMPAIGN_POSTS = "$COMMONDB.selectCampaignPosts"
        val SELECT_CAMPAIGN_POSTS_COUNT = "$COMMONDB.selectCampaignPostsCount"
        val SELECT_CAMPAIGN_COMMENTS = "$COMMONDB.selectCampaignComments"
        val SELECT_CAMPAIGN_COMMENTS_COUNT = "$COMMONDB.selectCampaignCommentsCount"
        val SELECT_CAMPAIGN_COMMENTS_COUNT_BY_DATE = "$COMMONDB.selectCampaignCommentsCountByDate"
        val SELECT_CAMPAIGN_ID_BY_ALIAS = "$COMMONDB.selectCampaignIdByAlias"
        val SELECT_CAMPAIGN_BY_ORDER = "$COMMONDB.selectCampaignIdByOrder"
        val SELECT_ACTIVE_AUTHOR_CAMPAIGN = "$COMMONDB.selectAuthorActiveCampaign"
        val SELECT_PURCHASED_SHARE_IDS = "$COMMONDB.selectPurchasedShareId"
        val SELECT_PURCHASED_CAMPAIGN_IDS = "$COMMONDB.selectPurchasedCampaignIds"
        val SELECT_CAMPAIGN_COLLECTED_AMOUNT = "$COMMONDB.selectCollectedAmount"
        val UPDATE_DRAFT_VISIBLE = "$COMMONDB.updateDraftVisible"
        val COUNT_SUCCESS_CAMPAIGNS_BY_TAG_ID = "$COMMONDB.countSuccessCampaignsByTagId"
        val COUNT_ORGANIZATION_CHARITY_CAMPAIGNS = "$COMMONDB.countOrganizationCharityCampaigns"
        val CHARITY_CAMPAIGN_STATS = "$COMMONDB.charityCampaignStats"
        val CAMPAIGNS_WITH_EVENTS_FOR_DAY_BY_PROFILE_ID = "$COMMONDB.selectCampaignsWithEventsForDayByProfileId"
        val SELECT_PROFILE_IDS_OF_CAMPAIGNS_WITH_EVENTS_FOR_DAY = "$COMMONDB.selectProfileIdsOfCampaignsWithEventsForDay"
        val SELECT_AGGREGATION_STATS_FOR_DAY_BY_CAMPAIGN_ID = "$COMMONDB.selectAggregationStatsForDayByCampaignId"
        val SELECT_USER_CAMPAIGNS_STATUSES_COUNT = "$COMMONDB.selectUserCampaignsStatusesCount"
        val SELECT_USER_PURCHASED_CAMPAIGNS_STATUSES_COUNT = "$COMMONDB.selectUserPurchasedCampaignsStatusesCount"
        val SELECT_USER_PURCHASED_AND_REWARDS_COUNT = "$COMMONDB.selectUserPurchasesAndRewardsCount"

        val GET_MY_PROJECTS = "$COMMONDB.selectMyProjects"
        val GET_MY_PURCHASED_PROJECTS = "$COMMONDB.selectMyPurchasedProjects"

        val GET_STAT_MY_PROJECTS = "$COMMONDB.getMyProjectsStats"

        object CampaignSubscribe {
            val SELECT_IS_SUBSCRIBED_ON_END = "$COMMONDB.selectIsSubscribedOnEnd"
            val INSERT_SUBSCRIBE_ON_END = "$COMMONDB.insertSubscribeOnEnd"
            val DELETE_SUBSCRIBE_ON_END = "$COMMONDB.deleteSubscribeOnEnd"
            val GET_CAMPAIGN_SUBSCRIPTIONS_TO_SEND = "$COMMONDB.getCampaignSubsctiptionsToSend"
        }

        object CampaignCurators {

            val SELECT_CURATORS = "$COMMONDB.selectCampaignCurators"
            val DELETE_CURATOR = "$COMMONDB.deleteCampaignCurator"
            val INSERT_CURATOR = "$COMMONDB.insertCampaignCurator"
        }

    }

    object CampaignStats {

        val SELECT_AGGREGETE_CAMPAIGN_STAT_BY_SPONSOR_ALIAS = "$COMMONDB.selectAggregateCampaignStatBySponsorAlias"
    }

    object CampaignPermission {

        val SELECT_LIST = "$COMMONDB.selectCampaignPermissionList"
        val INSERT = "$COMMONDB.insertCampaignPermission"
        val DELETE = "$COMMONDB.deleteCampaignPermission"

        val SELECT = "$COMMONDB.selectCampaignPermissionById"
        val UPDATE = "$COMMONDB.updateCampaignPermission"
    }

    object InvestingOrderInfo {

        val SELECT_LIST = "$COMMONDB.selectInvestingOrderInfoList"
        val SELECT_IN_INTERVAL = "$COMMONDB.selectInvestingOrderInfoListInInterval"
        val SELECT_BY_IDS = "$COMMONDB.selectInvestingOrderInfoListByIds"
        val COUNT = "$COMMONDB.countInvestingOrderInfo"
        val INSERT = "$COMMONDB.insertInvestingOrderInfo"
        val UPDATE = "$COMMONDB.updateInvestingOrderInfo"
        val DELETE = "$COMMONDB.deleteInvestingOrderInfo"
        val SELECT = "$COMMONDB.selectInvestingOrderInfoById"
        val SELECT_USER_TYPE = "$COMMONDB.selectInvestingOrderInfoByUserType"
    }

    object BillsFor1c {

        val SELECT_LIST = "$COMMONDB.selectBillsFor1cList"
        val INSERT = "$COMMONDB.insertBillsFor1c"
        val DELETE = "$COMMONDB.deleteBillsFor1c"
        val SELECT = "$COMMONDB.selectBillsFor1cById"
        val UPDATE = "$COMMONDB.updateBillsFor1c"
    }

    object CampaignContact {

        val SELECT_LIST = "$COMMONDB.selectCampaignContactList"
        val INSERT = "$COMMONDB.insertCampaignContact"
        val DELETE = "$COMMONDB.deleteCampaignContact"
        val GET_CAMPAIGN_COUNT = "$COMMONDB.getContactsCount"
    }

    interface CampaignTag {
        companion object {

            val SELECT_ALL = "$COMMONDB.selectAllTags"
            val SELECT_BY_CAMPAIGN = "$COMMONDB.selectTagsByCampaign"
            val REWRITE_CAMPAIGN_TAGS = "$COMMONDB.updateCampaignTags"
            val SELECT_CAMPAIGN_TAG_BY_ALIAS = "$COMMONDB.selectCampaignTagByAlias"
            val SELECT_CAMPAIGN_TAG_BY_MNEMONIC_NAME = "$COMMONDB.selectCampaignTagByMnemonicName"
            val SELECT_CAMPAIGN_TAGS_EXISTS_IN_SEMINARS = "$COMMONDB.selectCampaignTagsExistInSeminars"
            val SELECT_CAMPAIGN_TAGS_FOR_CHARITY = "$COMMONDB.selectCampaignTagsForCharity"
            val SELECT_CAMPAIGN_TAG_BY_ID = "$COMMONDB.selectCampaignTagById"
            val INSERT_CAMPAIGN_TAG = "$COMMONDB.insertCampaignTag"
            val SET_TAG_WITH_MANAGER_DEFAULT_RELATION = "$COMMONDB.setTagWithManagerDefaultRelation"
            val UPDATE_CAMPAIGN_TAG = "$COMMONDB.updateCampaignTag"
        }
    }

    interface CampaignTargeting {
        companion object {
            val SELECT_BY_CAMPAIGN_ID = "$COMMONDB.selectCampaignTargetingByCampaignId"
            val INSERT_CAMPAIGN_TARGETING = "$COMMONDB.insertCampaignTargeting"
            val UPDATE_CAMPAIGN_TARGETING = "$COMMONDB.updateCampaignTargeting"
        }
    }

    object PurchaseShareInfo {
        val SELECT_PURCHASE_SHARE_INFO = "$COMMONDB.selectPurchaseShareInfo"
        val INSERT_PURCHASE_SHARE_INFO = "$COMMONDB.insertPurchaseShareInfo"
    }

    object ShareEva {
        val SELECT_SHARE = "$COMMONDB.selectShareEva"
    }

    object Share {

        val SELECT_BY_CAMPAIGN = "$COMMONDB.selectCampaignShares"
        val SELECT = "$COMMONDB.selectShare"
        val SELECT_FOR_UPDATE = "$COMMONDB.selectShareForUpdate"
        val SELECT_DETAILED = "$COMMONDB.selectShareDetailed"
        val INSERT = "$COMMONDB.insertShare"
        val UPDATE = "$COMMONDB.updateShare"
        val DISABLE_SHARE = "$COMMONDB.disableShare"
        val DELETE = "$COMMONDB.deleteShare"
        val DELETE_BY_CAMPAIGN = "$COMMONDB.deleteCampaignShares"
        val SELECT_SALE_INFO = "$COMMONDB.selectSharesSaleInfo"
        val SELECT_SALE_PURSCHASE_INFO_ON_DATE = "$COMMONDB.selectSharesPurchasedInfoOnDate"
        val SELECT_SALE_PURSCHASE_INFO_ON_DATE_BY_CAMPAIGN_ID = "$COMMONDB.selectSharesPurchasedInfoOnDateByCampaignId"
        val SELECT_ORDERS_COUNT_BY_STATUS = "$COMMONDB.selectShareOrdersCountByStatus"
        val SELECT_BY_IDS = "$COMMONDB.selectSearchedByIds"
    }

    object Chat {

        val SELECT = "$PROFILEDB.selectChatById"
        val SELECT_BY_OWNER = "$PROFILEDB.selectChatByOwnerObjectId"
        val INSERT = "$PROFILEDB.insertChat"
    }

    object ChatMessage {

        val SELECT = "$PROFILEDB.selectChatMessage"
        val SELECT_BY_CHAT = "$PROFILEDB.selectMessagesByChat"
        val SELECT_LAST_BY_CHAT = "$PROFILEDB.selectLastMessagesByChat"
        val INSERT = "$PROFILEDB.insertChatMessage"
        val UPDATE = "$PROFILEDB.updateChatMessage"
    }

    object OrderObject {

        val SELECT_ORDER_OBJECT = "$COMMONDB.selectOrderObject"
        val SELECT_ORDER_OBJECTS = "$COMMONDB.selectOrderObjects"
        val SELECT_ALL_ORDERS_OBJECTS = "$COMMONDB.selectAllOrdersObjects"
        val INSERT = "$COMMONDB.insertOrderObject"
        val UPDATE = "$COMMONDB.updateOrderObject"
        val UPDATE_COMMENT = "$COMMONDB.updateOrderObjectComment"
        val SELECT_MERCHANT_ORDERS_OBJECTS = "$COMMONDB.selectMerchantOrdersObjects"
        val SELECT_OVERDUE_ORDERS_OBJECTS = "$COMMONDB.selectOverdueOrdersObjects"
        val MARK_ORDER_OBJECTS_AS_GIFT = "$COMMONDB.markOrderObjectsAsGift"
        val SELECT_PROFILE_PURCHASE_OBJECT = "$COMMONDB.isProfileHasPurchasedObject"
    }

    object Subscribers {

        val INSERT = "$COMMONDB.insertSubscriber"
        val IS_SUBSCRIBED = "$COMMONDB.selectIsSubscribed"
    }

    interface Hall {
        companion object {
            val INSERT = "$CONCERTDB.insertHall"
            val UPDATE = "$CONCERTDB.updateHall"
            val SELECT_ONE = "$CONCERTDB.selectHall"
            val SELECT_ONE_BY_NAME = "$CONCERTDB.selectHallByName"
        }
    }

    interface Section {
        companion object {
            val INSERT = "$CONCERTDB.insertSection"
            val UPDATE = "$CONCERTDB.updateSection"
            val SELECT_ONE = "$CONCERTDB.selectSection"
            val SELECT_LIST_FOR_CONCERT = "$CONCERTDB.selectSectionsForConcert"
        }
    }


    interface Place {
        companion object {
            val INSERT = "$CONCERTDB.insertPlace"
            val UPDATE = "$CONCERTDB.updatePlace"
            val SELECT_ONE = "$CONCERTDB.selectPlace"
            val SELECT_ONE_BY_PARAMS = "$CONCERTDB.selectPlaceByParams"
            val SELECT_LIST_FOR_CONCERT = "$CONCERTDB.selectPlacesForConcert"
            val SELECT_RESERVED_PLACES = "$CONCERTDB.selectReservedPlaces"
        }
    }


    interface Product {
        companion object {

            val INSERT = "$SHOPDB.insertProduct"

            val SELECT_PRODUCTS = "$SHOPDB.selectProducts"
            val SELECT_SIMILAR_PRODUCTS = "$SHOPDB.selectSimilarProducts"
            val SELECT_PRODUCTS_CURRENT_VERSION = "$SHOPDB.selectProductsCurrent"
            val SELECT_PRODUCTS_CURRENT_VERSION_COUNT = "$SHOPDB.selectProductsCurrentCount"
            val SELECT_PRODUCTS_FOR_ADMIN_SEARCH = "$SHOPDB.selectProductsForAdminSearch"

            val SELECT_SORTED_PARENT_PRODUCTS_IDS = "$SHOPDB.selectSortedParentProductsId"
            val SELECT_PRODUCTS_BY_IDS = "$SHOPDB.selectProductsByIds"
            val SELECT_DONATE_BY_REFERRER = "$SHOPDB.selectDonateByReferrerId"

            val SELECT_PRODUCTS_BY_ID_LIST = "$SHOPDB.selectProductsByIdList"
            val SELECT_TOP_READY_FOR_PURCHASE_PRODUCTS = "$SHOPDB.selectReadyForPurchaseTopProducts"
            val SELECT_TOP_READY_FOR_PURCHASE_PRODUCTS_WITH_PRICE_RANGE = "$SHOPDB.selectReadyForPurchaseTopProductsWithPriceRange"
            val SELECT_TOP_READY_FOR_PURCHASE_PRODUCTS_WITH_PRICE_RANGE_COUNT = "$SHOPDB.selectReadyForPurchaseTopProductsWithPriceRangeCount"

            val UPDATE_TOTAL_PURCHASE_COUNT = "$SHOPDB.updateTotalPurchaseCount"
            val LET_THEM_BE_FREE = "$SHOPDB.makeChildsFree"
            val UPDATE_CURRENT_TO_OLD = "$SHOPDB.updateCurrentToOld"
            val UPDATE_PRODUCT_NAME_AND_DESCRIPTION_HTML = "$SHOPDB.updateProductNameAndDescriptionHtml"
            val UPDATE_STATUS_A_TO_B = "$SHOPDB.updateStatusAtoB"
            val UPDATE_STATUS_TO_START_AND_SET_START_DATE = "$SHOPDB.updateStatusToStartAndSetStartDate"

            val UPDATE_TOTAL_QUANTITY_DELTA = "$SHOPDB.updateTotalQuantityDelta"
            val UPDATE_TOTAL_QUANTITY = "$SHOPDB.updateTotalQuantity"
            val UPDATE_DETAILS = "$SHOPDB.updateProductDetails"

            val BIND_TO_PROFILE = "$SHOPDB.bindToProfile"
        }
    }

    interface ProductTag {
        companion object {

            val SELECT_ALL = "$SHOPDB.selectAllTags"
            val SELECT_BY_PRODUCT = "$SHOPDB.selectTagsByProduct"
            val UPDATE_PRODUCT_TAGS = "$SHOPDB.updateProductTags"
            val SELECT_BY_MNEMONIC = "$SHOPDB.selectTagByMnemonicName"
            val SELECT_BY_MNEMONIC_NAME_LIST = "$SHOPDB.selectTagSByMnemonicNameList"
            val COUNT_PRODUCT_TAGS = "$SHOPDB.countProductTags"
            val SELECT_TAG_BY_ID = "$SHOPDB.selectTagById"
            val INSERT_PRODUCT_TAG = "$SHOPDB.insertProductTag"
            val UPDATE_PRODUCT_TAG = "$SHOPDB.updateProductTag"
            val REMOVE_NEW_TAGS = "$SHOPDB.removeNewTags"
        }
    }

    object ProductAttribute {

        val INSERT_CATEGORY = "$SHOPDB.insertCategory"
        val SELECT_ALL_CATEGORIES = "$SHOPDB.selectAllCategories"
        val SELECT = "$SHOPDB.selectAttributeById"
        val SELECT_BY_TYPE = "$SHOPDB.selectAttributesByType"
    }

    object Address {

        val SELECT = "$COMMONDB.selectAddress"
        val INSERT = "$COMMONDB.insertAddress"
        val UPDATE = "$COMMONDB.updateAddress"
        val LAST_POST_ADDRESS = "$COMMONDB.getLastPostAddress"
    }


    object ShoppingCart {

        val INSERT = "$SHOPDB.insertCartItem"
        val SELECT_FROM_CART = "$SHOPDB.selectFromCart"
        val UPDATE_QUANTITY = "$SHOPDB.updateCartItem"
        val DELETE = "$SHOPDB.deleteFromCart"
        val GET_LAST_TIME_UPDATED = "$SHOPDB.getLastTimeUpdated"
        val GET_ALL_IDS = "$SHOPDB.getAllIds"
    }

    object UserLoginInfo {

        val INSERT = "$COMMONDB.insertUserLoginInfo"
        val SELECT = "$COMMONDB.selectUserLoginInfo"
        val DELETE = "$COMMONDB.deleteUserLoginInfo"
        val UPDATE = "$COMMONDB.updateUserLoginInfo"
        val SELECT_LAST = "$COMMONDB.selectLastLoginInfo"
    }

    object UserLastOnlineTime {

        val INSERT = "$COMMONDB.insertUserLastOnlineTime"
        val SELECT = "$COMMONDB.selectUserLastOnlineTime"
        val SELECT_LIST = "$COMMONDB.selectUsersLastOnlineTime"
        val DELETE = "$COMMONDB.deleteUserLastOnlineTime"
        val UPDATE = "$COMMONDB.updateUserLastOnlineTime"
    }

    object PlanetaManagers {

        val SELECT_ALL_MANAGERS = "$COMMONDB.selectPlanetaManagersAll"
        val GET_MANAGER_ID = "$COMMONDB.getManagerIdByProfileId"
        val INSERT_NEW_MANAGER = "$COMMONDB.insertNewManager"
        val UPDATE_MANAGER = "$COMMONDB.updatePlanetaManager"
        val SELECT_CURRENT_MANAGER = "$COMMONDB.selectCurrentManager"
        val SELECT_MANAGERS_BY_CAMPAIGN_IDS = "$COMMONDB.selectManagersByCampaignIds"
        val SELECT_MANAGERS_FOR_CAMPAIGN_TAGS = "$COMMONDB.selectPlanetaManagersForCampaignTags"
        val SELECT_MANAGERS_FOR_GROUP_CATEGORIES = "$COMMONDB.selectPlanetaManagersForGroupCategories"
        val SELECT_MANAGER_BY_PROFILE_ID = "$COMMONDB.selectManagerByProfileId"
        val SELECT_MANAGER_BY_ID = "$COMMONDB.selectManagerById"
    }

    object PlanetaManagersRelations {

        val SELECT_CAMPAIGN_RELATIONS = "$COMMONDB.selectCampaignManagerRelations"
        val SELECT_GROUP_RELATIONS = "$COMMONDB.selectGroupManagerRelations"
        val INSERT_CURRENT_MANAGER = "$COMMONDB.insertCurrentManager"
        val UPDATE_CURRENT_MANAGER = "$COMMONDB.updateCurrentManager"
        val CHANGE_CURRENT_TAG_MANAGER = "$COMMONDB.updateManagerForTag"
    }

    object ProfileFile {

        val SELECT = "$PROFILEDB.selectProfileFile"
        val SELECT_LIST_BY_PROFILE = "$PROFILEDB.selectProfileFilesByProfileId"
        val INSERT = "$PROFILEDB.insertProfileFile"
        val UPDATE_FILE_TITLE_AND_DESCRIPTION = "$PROFILEDB.updateProfileFileTitleAndDescription"
        val DELETE = "$PROFILEDB.deleteProfileFile"
    }

    object ModerationMessage {

        val SELECT_LAST_CAMPAIGN_MODERATION_MESSAGE = "$COMMONDB.selectLastCampaignModerationMessage"
        val SELECT_LAST_CAMPAIGN_MODERATION_MESSAGE_LIST = "$COMMONDB.selectLastCampaignModerationMessageList"
        val SELECT_ALL_CAMPAIGN_MODERATION_MESSAGES = "$COMMONDB.selectAllCampaignModerationMessages"
        val INSERT_CAMPAIGN_MODERATION_MESSAGE = "$COMMONDB.insertCampaignModerationMessage"
    }

    interface DeliveryServices {
        companion object {

            val ADD_DELIVERY_LINK = "$COMMONDB.insertDeliveryLink"
            val ADD_DELIVERY_LINK_OBJECT = "$COMMONDB.insertDeliveryLinkObject"
            val UPDATE_DELIVERY_LINK = "$COMMONDB.updateLinkedDelivery"

            val SELECT_LINKED_DELIVERIES = "$COMMONDB.selectLinkedDeliveries"
            val SELECT_BASE_DELIVERIES = "$COMMONDB.selectBaseDeliveries"
            val DELETE_BASE_DELIVERY = "$COMMONDB.deleteBaseDelivery"
            val CREATE_BASE_DELIVERY = "$COMMONDB.insertBaseDelivery"
            val UPDATE_BASE_DELIVERY = "$COMMONDB.updateBaseDelivery"
            val SELECT_UNLINKED_BASE_DELIVERIES = "$COMMONDB.selectUnlinkedBaseDeliveries"
        }
    }

    interface DeliveryAddress {
        companion object {

            val UPDATE_DELIVERY_ADDRESS = "$COMMONDB.updateDeliveryAddress"
            val UPDATE_ORDER_ID = "$COMMONDB.updateOrderId"
            val INSERT_DELIVERY_ADDRESS = "$COMMONDB.insertDeliveryAddress"
            val DELETE_DELIVERY_ADDRESS = "$COMMONDB.deleteDeliveryAddress"
            val SELECT_DELIVERY_ADDRESS = "$COMMONDB.selectDeliveryAddress"
        }
    }

    object Bonus {

        val SELECT_BONUSES = "$COMMONDB.selectBonuses"
        val SELECT_ALL_BONUSES = "$COMMONDB.selectAllBonuses"
        val SELECT = "$COMMONDB.selectBonus"
        val INSERT = "$COMMONDB.insertBonus"
        val UPDATE = "$COMMONDB.updateBonus"
        val REORDER_BONUSES = "$COMMONDB.updateBonusesOrder"
    }

    object Faq {

        val SEARCH = "$COMMONDB.searchFaqSnippets"
    }

    object ProfileSubscription {

        val SELECT_SUBSCRIPTION_INFO_LIST = "$PROFILEDB.selectSubscriptionInfoList"
        val SELECT_PROFILE_LIST = "$PROFILEDB.selectProfileList"
        val SELECT_OWN_GROUPS_ID = "$PROFILEDB.selectProfileOwnGroupsId"
        val SELECT_SUBSCRIPTION_MAP = "$PROFILEDB.selectSubscriptionMap"
        val SELECT_PROFILE_RELATION_LIST = "$PROFILEDB.selectProfileRelationList"
        val GET_SUBSCRIBERS_INFO_COUNT = "$PROFILEDB.getSubscriptionInfoCount"
        val GET_NEW_SUBSCRIBERS_INFO_COUNT = "$PROFILEDB.getNewSubscriptionInfoCount"
    }

    object VipClub {

        val SELECT_VIP_LIST = "$PROFILEDB.selectVipList"
    }

    object UserCallback {

        val INSERT_USER_CALLBACK = "$COMMONDB.insertUserCallback"
        val UPDATE_CALLBACK_PROCESSED = "$COMMONDB.updateCallbackProcessed"
        val SELECT_USER_CALLBACKS = "$COMMONDB.selectUserCallbacks"
        val SELECT_BY_ID = "$COMMONDB.selectByOrderId"
        val SELECT_CALLBACKS_BY_IDS = "$COMMONDB.selectCallbackByIds"
    }

    object UserCallbackComment {

        val INSERT_USER_CALLBACK_COMMENT = "$COMMONDB.insertUserCallbackComment"
        val SELECT_USER_CALLBACK_COMMENTS_COUNT = "$COMMONDB.selectUserCallbackCommentsCount"
        val SELECT_USER_CALLBACK_COMMENTS = "$COMMONDB.selectUserCallbackComments"
    }

    interface Library {
        companion object {

            val INSERT_LIBRARY = "$BIBLIO.insertLibrary"
            val SELECT_LIBRARY = "$BIBLIO.selectLibrary"
            val SELECT_LIBRARIES = "$BIBLIO.selectLibraries"
            val SELECT_LIBRARIES_BY_IDS = "$BIBLIO.selectLibrariesByIds"
            val UPDATE_LIBRARY = "$BIBLIO.updateLibrary"
            val LIBRARY_WITHOUT_COORDINATES = "$BIBLIO.selectLibraryWithoutCoordinates"
            val LIBRARY_RANDOM = "$BIBLIO.selectRandomLibrary"
            val COUNT_ACTIVE_LIBRARIES = "$BIBLIO.countActiveLibraries"
            val LIBRARY_WITHOUT_REGION = "$BIBLIO.selectLibraryWithoutRegion"
            val SEARCH_CLUSTER_LIBRARIES = "$BIBLIO.searchClusterLibraries"
            val SEARCH_LIBRARIES_IN_AREA = "$BIBLIO.searchLibrariesInArea"
            val LIBRARY_WITHOUT_POST_INDEX = "$BIBLIO.selectLibraryWithoutPostIndex"
            val HAS_REQUESTS = "$BIBLIO.selectHasLibraryRequests"
        }
    }

    interface Book {
        companion object {

            val SELECT_BOOK_FOR_CHANGE_PRICE = "$BIBLIO.selectBookForChangePrice"
            val SEARCH_BOOKS = "$BIBLIO.searchBooks"
            val SELECT_BOOKS = "$BIBLIO.selectBooks"
            val COUNT_ACTIVE_BOOKS = "$BIBLIO.countActiveBooks"
            val SELECT_INDEX_BOOKS = "$BIBLIO.selectBooksForIndexPage"
            val SELECT_BOOK_BY_ID = "$BIBLIO.selectBookById"
            val SELECT_ACTIVE_BOOK_BY_ID = "$BIBLIO.selectActiveBookById"
            val INSERT_BOOK = "$BIBLIO.insertBook"
            val UPDATE_BOOK = "$BIBLIO.updateBook"
            val DELETE_BOOK = "$BIBLIO.deleteBook"
            val SELECT_COUNT_SEARCH_BOOKS = "$BIBLIO.selectCountSearchBooks"
            val HAS_REQUESTS = "$BIBLIO.selectHasRequests"
        }
    }

    interface MagazineInfo {
        companion object {
            val SELECT_MAGAZINE_INFO_BY_PRICE_LIST_ID = "$BIBLIO.selectMagazinesInfoByPriceListId"
            val INSERT_MAGAZINE_INFO = "$BIBLIO.insertMagazineInfo"
            val UPDATE_MAGAZINE_INFO = "$BIBLIO.updateMagazineInfo"
        }
    }

    interface MagazinePriceList {
        companion object {
            val SELECT_MAGAZINE_PRICE_LIST_BY_ID = "$BIBLIO.selectMagazinePriceListById"
            val SELECT_MAGAZINE_PRICE_LISTS = "$BIBLIO.selectMagazinePriceLists"
            val INSERT_MAGAZINE_PRICE_LIST = "$BIBLIO.insertMagazinePriceList"
            val UPDATE_MAGAZINE_PRISE_LIST = "$BIBLIO.updateMagazinePriceList"
        }
    }

    interface PublishingHouse {
        companion object {

            val SELECT_PUBLISHING_HOUSES = "$BIBLIO.selectPublishingHouses"
            val INSERT_PUBLISHING_HOUSE = "$BIBLIO.insertPublishingHouse"
            val UPDATE_PUBLISHING_HOUSE = "$BIBLIO.updatePublishingHouse"
            val SELECT_COUNT_PUBLISHING_HOUSES = "$BIBLIO.countPublishingHouses"
            val SELECT_PUBLISHING_HOUSE_BY_ID = "$BIBLIO.getPublishingHouseById"
        }
    }

    interface BookTag {
        companion object {

            val SELECT_ALL_BOOK_TAGS = "$BIBLIO.selectAllBookTags"
            val EXISTS_TAG_IN_BOOKS = "$BIBLIO.existsTagInBooks"
            val SELECT_TAGS_BY_BOOK_ID = "$BIBLIO.selectTagsByBook"
            val DELETE_BOOK_TAG = "$BIBLIO.deleteBookTags"
            val SELECT_BOOK_TAG_BY_ID = "$BIBLIO.selectBookTagById"
            val SELECT_BOOK_TAG_BY_MNEMONIC_NAME = "$BIBLIO.selectBookTagByMnemonicName"
            val UPDATE_BOOK_TAGS = "$BIBLIO.updateBookTags"
        }
    }

    interface Bin {
        companion object {

            val INSERT = "$BIBLIO.insertBin"
            val UPDATE = "$BIBLIO.updateBin"
            val INSERT_BOOKS = "$BIBLIO.insertBookRelations"
            val INSERT_LIBRARIES = "$BIBLIO.insertLibraryRelations"

            val SELECT = "$BIBLIO.selectByOrderId"
            val SELECT_BY_PID = "$BIBLIO.selectCampaignById"
            val REMOVE_RELATIONS = "$BIBLIO.deleteRelations"
            val REMOVE = "$BIBLIO.deleteBin"
            val REMOVE_ALL = "$BIBLIO.deleteAll"
        }
    }

    interface BannerStatistics {
        companion object {

            val INSERT_CLICKED = "$TRASHCAN.insertClicked"
            val INSERT_APPEARED = "$TRASHCAN.insertAppeared"
        }
    }

    interface AdBlockStatistics {
        companion object {
            val HAS = "$TRASHCAN.hasAdBlock"
        }
    }

    interface GoogleStat {
        companion object {

            val INSERT_GTM_CID = "$TRASHCAN.insertGtmCid"
        }
    }

    interface Member {
        companion object {

            val SELECT_ALL_MEMBERS = "$CHARITYDB.selectAllMembers"
        }
    }

    interface CharityNewsTag {
        companion object {

            val SELECT_ALL_CHARITY_NEWS_TAGS = "$CHARITYDB.selectAllCharityNewsTags"
        }
    }

    interface SchoolOnlineApplications {
        companion object {

            val INSERT_SCHOOL_ONLINE_APPLICATION = "$TRASHCAN.insertSchoolOnlineApplication"
            val SELECT_SCHOOL_ONLINE_APPLICATION_BY_EMAIL = "$TRASHCAN.selectSchoolOnlineApplicationByEmail"
        }
    }

    interface News {
        companion object {
            val SELECT_LAST_CAMPAIGN_NEWS = "$PROFILEDB.selectLastCampaignNews"
        }
    }

    interface Purchase {
        companion object {
            val SELECT_MY_PURCHASES_FOR_CAMPAIGN = "$COMMONDB.selectMyPurchasesByCampaignId"
        }
    }
}

package ru.planeta.dao.commondb.campaign


class ContractorDAOImpl /*: BaseCommonDbDAO<*>(), ContractorDAO {

    override fun selectCampaignById(offset: Int, limit: Int): List<Contractor> {
        val params = getParameters("offset", offset, "limit", limit)
        return selectCampaignById(Statements.Contractor.SELECT_LIST.toInt(), params.toInt())

    }

    override fun selectCampaignById(profileId: Long, offset: Int, limit: Int): List<Contractor> {
        val params = getParameters(BaseCommonDbDAO.Companion.PROFILE_ID, profileId, "offset", offset, "limit", limit)
        return selectCampaignById(Statements.Contractor.SELECT_LIST_BY_PROFILE.toInt(), params.toInt())

    }

    override fun selectCampaignById(contractorId: Long): Contractor {
        val params = getParameters(BaseCommonDbDAO.Companion.CONTRACTOR_ID, contractorId)
        return selectOne(Statements.Contractor.SELECT, params) as Contractor
    }

    override fun selectByInn(inn: String): Contractor {
        val params = getParameters(BaseCommonDbDAO.Companion.CONTRACTOR_INN, inn)
        return selectOne(Statements.Contractor.SELECT_BY_INN, params) as Contractor
    }

    override fun selectCampaignById(name: String): Contractor {
        val params = getParameters(NAME, name)
        return selectOne(Statements.Contractor.SELECT_BY_NAME, params) as Contractor
    }

    override fun selectByCampaignId(campaignId: Long): Contractor {
        val params = getParameters(CAMPAIGN_ID, campaignId)
        return selectOne(Statements.Contractor.SELECT_BY_CAMPAIGN_ID, params) as Contractor
    }

    override fun selectContractorsSearch(searchString: String, offset: Int, limit: Int): ArrayListWithCount<ContractorWithCampaigns> {
        val params = getParameters("searchString", searchString,
                "offset", offset, "limit", limit)
        val contractors = selectCampaignById(Statements.Contractor.SELECT_CONTRACTORS_SEARCH.toInt(), params.toInt())

        val totalCount = selectContractorsSearchTotalCount(params)
        val result = ArrayListWithCount<ContractorWithCampaigns>(contractors)
        result.totalCount = totalCount

        return result
    }

    override fun selectContractorsSearchTotalCount(params: Map<*, *>): Int {
        return selectOne(Statements.Contractor.SELECT_CONTRACTORS_SEARCH_TOTAL_COUNT, params) as Int
    }

    override fun insert(contractor: Contractor) {
        insert(Statements.Contractor.INSERT, contractor)
    }

    override fun insertRelation(contractorId: Long, campaignId: Long) {
        val params = getParameters(BaseCommonDbDAO.Companion.CONTRACTOR_ID, contractorId, "campaignId", campaignId)
        insert(Statements.Contractor.INSERT_RELATION, params)
    }

    override fun deleteRelation(campaignId: Long) {
        val params = getParameters(CAMPAIGN_ID, campaignId)
        deleteByProfileId(Statements.Contractor.DELETE_RELATION, params)
    }

    override fun deleteAllRelationByContractorId(contractorId: Long) {
        val params = getParameters(BaseCommonDbDAO.Companion.CONTRACTOR_ID, contractorId)
        deleteByProfileId(Statements.Contractor.DELETE_ALL_RELATIONS_BY_CONTRACTOR_ID, params)
    }

    override fun updateRelation(contractorId: Long, campaignId: Long) {
        val params = getParameters(BaseCommonDbDAO.Companion.CONTRACTOR_ID, contractorId, "campaignId", campaignId)
        insert(Statements.Contractor.UPDATE_RELATION, params)
    }

    override fun update(contractor: Contractor) {
        update(Statements.Contractor.UPDATE, contractor)
    }

    override fun deleteByProfileId(contractorId: Long) {
        val params = getParameters(BaseCommonDbDAO.Companion.CONTRACTOR_ID, contractorId)
        deleteByProfileId(Statements.Contractor.DELETE, params)
    }

    companion object {

        val CAMPAIGN_ID = "campaignId"
        val NAME = "name"
    }
}*/

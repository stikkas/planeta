package ru.planeta.dao.msgdb

import org.springframework.stereotype.Repository
import ru.planeta.dao.BaseClusterDAO
import ru.planeta.dao.Statements
import ru.planeta.model.msg.DialogMessage
import ru.planeta.model.msg.DialogObjectSelectParams

/**
 * @author ameshkov
 */
@Repository
class DialogMessageDAOImpl : BaseClusterDAO<*>(), DialogMessageDAO {

    override fun selectMessage(dialogId: Long, messageId: Long): DialogMessage {
        return selectOne(Statements.DialogMessage.SELECT, DialogObjectSelectParams(dialogId, messageId)) as DialogMessage
    }

    override fun selectMessages(dialogId: Long, offset: Int, limit: Int): List<DialogMessage> {
        return selectList(Statements.DialogMessage.SELECT_BY_DIALOG, DialogObjectSelectParams(dialogId, offset, limit))
    }

    override fun selectMessages(dialogId: Long, startMessageId: Long): List<DialogMessage> {
        return selectList(Statements.DialogMessage.SELECT_LAST_BY_DIALOG, DialogObjectSelectParams(dialogId, startMessageId))
    }

    override fun selectSpammers(threshold: Long, limit: Int, offset: Int): List<Long> {
        val params = DialogObjectSelectParams(threshold, offset, limit)
        return selectList(Statements.DialogMessage.SELECT_SPAMMERS, params)
    }

    override fun insert(dialogMessage: DialogMessage) {
        if (dialogMessage.messageId == 0L) {
            dialogMessage.messageId = sequencesDAO!!.selectNextLong(DialogMessage::class.java)
        }

        insert(Statements.DialogMessage.INSERT, dialogMessage)
    }

    override fun update(dialogMessage: DialogMessage) {
        update(Statements.DialogMessage.UPDATE, dialogMessage)
    }

    override fun selectLastMessage(dialogId: Long): DialogMessage {
        return selectOne(Statements.DialogMessage.SELECT_LAST_MESSAGE, DialogObjectSelectParams(dialogId)) as DialogMessage
    }

    override fun selectLastMessageId(dialogId: Long): Long {
        val lastMessageId = selectOne(Statements.DialogMessage.SELECT_LAST_MESSAGE_ID_BY_DIALOG, DialogObjectSelectParams(dialogId)) as Long
        return lastMessageId ?: 0
    }
}

package ru.planeta.dao.commondb


/**
 * DAO for UserPrivateInfo
 *
 * @author a.savanovich
 */
class UserPrivateInfoDAOImpl /*extends BaseCommonDbDAO<UserPrivateInfo> implements UserPrivateInfoDAO*///
//    @Override
//    public UserPrivateInfo selectByEmail(final String email) {
//        return selectOne(Statements.UserPrivateInfo.SELECT_BY_EMAIL, email);
//    }
//
//    @Override
//    public UserPrivateInfo selectByUsername(final String username) {
//        return selectOne(Statements.UserPrivateInfo.SELECT_BY_USERNAME, UserPrivateInfo.transformUsername(username));
//    }
//
//    @Override
//    public UserPrivateInfo selectByUserId(final long userId) {
//        return selectOne(Statements.UserPrivateInfo.SELECT_BY_USERID, userId);
//    }
//
//    @Override
//    public long selectUsersCount() {
//        return count(Statements.UserPrivateInfo.SELECT_USERS_COUNT);
//    }
//
//    @Override
//    public UserPrivateInfo selectByRegCode(final String regCode) {
//        return selectOne(Statements.UserPrivateInfo.SELECT_BY_REGCODE, regCode);
//    }
//
//    @Override
//    @SuppressWarnings("unchecked")
//    public List<UserPrivateInfo> selectByStatus(final EnumSet<UserStatus> statuses) {
//        if (statuses == null || statuses.isEmpty()) {
//            return Collections.emptyList();
//        }
//        List<Integer> statusCodes = new ArrayList<Integer>();
//        for (UserStatus status : statuses) {
//            statusCodes.add(status.getCode());
//        }
//        final Map<String, Object> params = getParameters("statusCodes", statusCodes);
//
//        return selectCampaignById(Statements.UserPrivateInfo.SELECT_PLANETA_ADMINS, params);
//    }
//
//    @Override
//    public void insert(final UserPrivateInfo userPrivateInfo) {
//        insert(Statements.UserPrivateInfo.INSERT, userPrivateInfo);
//    }
//
//    @Override
//    public void deleteByProfileId(final long userId) {
//        deleteByProfileId(Statements.UserPrivateInfo.DELETE, userId);
//    }
//
//    @Override
//    public void update(final UserPrivateInfo userPrivateInfo) {
//        update(Statements.UserPrivateInfo.UPDATE, userPrivateInfo);
//    }
//
//    @Override
//    public List<UserPrivateInfo> selectUserPrivateInfoNotNullRegCode(final Date dateFrom, final Date dateTo, final int offset, final int limit) {
//        final Map params = getParameters("dateFrom", dateFrom, "dateTo", dateTo, "offset", offset, "limit", limit);
//        return selectCampaignById(Statements.UserPrivateInfo.SELECT_NOT_NULL_REG_CODE, params);
//    }
//
//    @Override
//    public List<UserPrivateInfo> selectUserPrivateInfo(final Date dateFrom, final Date dateTo, final int offset, final int limit) {
//        final Map params = getParameters("dateFrom", dateFrom, "dateTo", dateTo, "offset", offset, "limit", limit);
//        return selectCampaignById(Statements.UserPrivateInfo.SELECT_BY_TIME_ADDED, params);
//    }
//
//    @Override
//    public List<UserPrivateInfo> selectUsersPrivateInfo(Collection<Long> userIds) {
//        return CollectionUtils.isEmpty(userIds)
//                ? new ArrayList<UserPrivateInfo>()
//                : selectCampaignById(Statements.UserPrivateInfo.SELECT_BY_USERS_IDS, getParameters("userIds", userIds));
//    }
//
//    @Override
//    public List<UserPrivateInfo> selectSubscriberListToSpam(long profileId, int offset, int limit) {
//        final Map params = getParameters("subjectProfileId", profileId, "offset", offset, "limit", limit);
//        return selectCampaignById(Statements.UserPrivateInfo.SELECT_SUBSCRIBERS, params);
//    }

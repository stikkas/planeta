package ru.planeta.dao.commondb.campaign

class SponsorDAOImpl /*: BaseCommonDbDAO<*>(), SponsorDAO {
    override fun selectSponsor(alias: String): Sponsor {
        return selectOne(Statements.Sponsor.SELECT, getParameters("alias", alias)) as Sponsor
    }

    override fun selectAll(): List<Sponsor> {
        return selectCampaignById(Statements.Sponsor.SELECT_ALL, null)
    }

    override fun selectSponsors(aliasList: List<String>): List<Sponsor> {
        return selectCampaignById(Statements.Sponsor.SELECT_SPONSORS, getParameters("aliasList", aliasList))
    }

    override fun insert(sponsor: Sponsor) {
        insert(Statements.Sponsor.INSERT, sponsor)
    }

    override fun update(sponsor: Sponsor) {
        update(Statements.Sponsor.UPDATE, sponsor)
    }

    override fun deleteByProfileId(alias: String) {
        deleteByProfileId(Statements.Sponsor.DELETE, getParameters("alias", alias))
    }
}*/

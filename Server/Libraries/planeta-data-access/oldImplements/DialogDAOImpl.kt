package ru.planeta.dao.msgdb

import org.springframework.stereotype.Repository
import ru.planeta.dao.BaseClusterDAO
import ru.planeta.dao.Statements
import ru.planeta.model.msg.Dialog
import ru.planeta.model.msg.DialogObjectSelectParams

/**
 * @author ameshkov
 */
@Repository
class DialogDAOImpl : BaseClusterDAO<*>(), DialogDAO {

    override fun select(dialogId: Long): Dialog {
        return selectOne(Statements.Dialog.SELECT, DialogObjectSelectParams(dialogId)) as Dialog
    }

    override fun update(dialog: Dialog) {
        update(Statements.Dialog.UPDATE, dialog)
    }

    override fun insert(dialog: Dialog) {
        if (dialog.dialogId == 0L) {
            dialog.dialogId = sequencesDAO!!.selectNextLong(Dialog::class.java)
        }
        insert(Statements.Dialog.INSERT, dialog)
    }
}

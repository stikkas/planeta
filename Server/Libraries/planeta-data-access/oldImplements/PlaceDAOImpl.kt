package ru.planeta.dao.concertdb


class PlaceDAOImpl /*: BaseDAO<Place>(), PlaceDAO {
    override fun insert(place: Place): Int {
        return insert(Statements.Place.INSERT, place)
    }

    override fun update(place: Place): Int {
        return insert(Statements.Place.UPDATE, place)
    }

    override fun selectCampaignById(placeId: Long): Place {
        return selectOne(Statements.Place.SELECT_ONE, getParameters("placeId", placeId))
    }

    override fun selectByExternalUnusedId(externalUnusedId: Long): Place {
        return selectOne(Statements.Place.SELECT_ONE, getParameters("externalUnusedId", externalUnusedId))
    }


    override fun selectByExternalPlaceId(externalPlaceId: Long): Place {
        return selectOne(Statements.Place.SELECT_ONE, getParameters("externalPlaceId", externalPlaceId))
    }

    override fun selectBySectionRowPosition(sectionId: Long, rowId: Long, position: Long): Place {
        return selectOne(Statements.Place.SELECT_ONE_BY_PARAMS,
                getParameters("sectionId", sectionId, "rowId", rowId, "position", position))
    }

    override fun selectByExtConcertId(externalConcertId: Long, externalSectionId: Long?, externalRowId: Long?): List<Place> {
        return selectCampaignById(Statements.Place.SELECT_LIST_FOR_CONCERT,
                getParameters("externalConcertId", externalConcertId,
                        "externalSectionId", externalSectionId,
                        "externalRowId", externalRowId))
    }

    override fun selectReservedPlaces(offset: Int, limit: Int): List<Place> {
        return selectCampaignById(Statements.Place.SELECT_RESERVED_PLACES, getParameters("limit", limit, "offset", offset))
    }
}*/

package ru.planeta.dao.promo

import org.springframework.stereotype.Repository
import ru.planeta.dao.BaseDAO
import ru.planeta.model.promo.TechnobattleRegistration

/**
 * Created with IntelliJ IDEA.
 * User: michail
 * Date: 18.11.2016
 * Time: 11:48
 */

@Repository
class TechnobattleRegistrationDAOImpl : BaseDAO<TechnobattleRegistration>(), TechnobattleRegistrationDAO {
    private interface Statements {
        companion object {
            val INSERT = ru.planeta.dao.Statements.COMMONDB + ".technobattleUserInsert"
            val SELECT = ru.planeta.dao.Statements.COMMONDB + ".technobattleUsersSelect"
        }
    }


    override fun insert(registration: TechnobattleRegistration): Int {
        return insert(Statements.INSERT, registration)
    }

    override fun select(offset: Int, limit: Int): List<TechnobattleRegistration> {
        return selectList(Statements.SELECT, getParameters("offset", offset, "limit", limit))
    }

    override fun selectById(id: Long): TechnobattleRegistration {
        return selectOne(Statements.SELECT, getParameters("registrationId", id))
    }
}

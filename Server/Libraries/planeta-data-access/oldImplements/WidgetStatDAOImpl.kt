package ru.planeta.dao.statdb

import org.springframework.stereotype.Repository
import ru.planeta.dao.Statements

/**
 *
 * Created by alexa_000 on 04.06.2015.
 */
@Repository
class WidgetStatDAOImpl : BaseStatDbDAO<*>(), WidgetStatDAO {

    override fun aggregate() {
        insert(INSERT_STAT, null)
    }

    companion object {

        private val INSERT_STAT = Statements.STATDB + ".insertWidgetStats"
    }
}

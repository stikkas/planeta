package ru.planeta.dao.commondb.campaign

class ShareSearchDAOImpl /*: BaseCommonDbDAO<ShareForSearch>(), ShareSearchDAO {

    override fun selectCampaignById(shareIds: Collection<Long>): List<ShareForSearch> {
        return if (CollectionUtils.isEmpty(shareIds))
            emptyList()
        else
            selectCampaignById(Statements.COMMONDB + ".selectSharesForSearchByIds", getParameters(SHARE_ID_LIST, shareIds))
    }

    override fun selectSharesForWelcome(): List<ShareForSearch> {
        return selectCampaignById(Statements.COMMONDB + ".selectSharesForWelcome", null)
    }

    companion object {

        private val SHARE_ID_LIST = "shareIdList"
    }
}*/

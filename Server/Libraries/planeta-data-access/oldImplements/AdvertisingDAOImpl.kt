package ru.planeta.dao.commondb.advertising

class AdvertisingDAOImpl/*<T : AdvertisingDTO> : BaseCommonDbDAO<T>(), AdvertisingDAO<T> {

    override val activeByDateAdvertising: List<T>
        get() = selectCampaignById(Statements.Advertising.SELECT_BY_ACTIVE_DATE, getParameters(
                "dateTo", Date()
        )
        )

    override val inactiveByDateAdvertising: List<T>
        get() = selectCampaignById(Statements.Advertising.SELECT_BY_INACTIVE_DATE, getParameters(
                "dateFrom", Date()
        )
        )

    override fun getAdvertising(broadcastIdFrom: Long, broadcastIdTo: Long, state: AdvertisingState): List<T> {
        return selectCampaignById(Statements.Advertising.SELECT, getParameters(
                "broadcastIdFrom", broadcastIdFrom,
                "broadcastIdTo", broadcastIdTo,
                "state", state
        )
        )
    }

    override fun saveAdvertising(dto: AdvertisingDTO) {
        insert(Statements.Advertising.MERGE, dto)
    }

    override fun deleteAdvertising(eventId: Long) {
        deleteByProfileId(Statements.Advertising.DELETE, getParameters("broadcastId", eventId))
    }

    override fun getAdvertisingByCampaign(campaignId: Long): List<T> {
        return selectCampaignById(Statements.Advertising.SELECT_BY_CAMPAIGN_ID, getParameters("campaignId", campaignId))
    }
}
*/

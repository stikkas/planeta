package ru.planeta.dao.profiledb


class CommentDAOImpl /*: BaseClusterDAO<*>(), CommentDAO */{
//
//    override fun selectComments(clientId: Long, ownerId: Long, objectId: Long,
//                                objectType: ObjectType, parentCommentId: Long,
//                                offset: Int, limit: Int, sort: String, lastCommentId: Long): List<Comment> {
//        val params = createBuilder(ownerId, objectId, offset, limit).add(PARENT_ID, parentCommentId).add(OBJECT_TYPE, objectType.code).add(CLIENT_ID, clientId).add("lastCommentId", lastCommentId).add(SORT, sort).build()
//        return selectCampaignById(Statements.Comment.SELECT_COMMENTS, params)
//    }
//
//    override fun selectComments(clientId: Long, ownerId: Long, objectId: Long,
//                                objectType: ObjectType, parentCommentId: Long,
//                                offset: Int, limit: Int, sort: String): List<Comment> {
//        val params = createBuilder(ownerId, objectId, offset, limit).add(PARENT_ID, parentCommentId).add(OBJECT_TYPE, objectType.code).add(CLIENT_ID, clientId).add("lastCommentId", 0).add(SORT, sort).build()
//        return selectCampaignById(Statements.Comment.SELECT_COMMENTS2, params)
//    }
//
//    override fun selectCommentsWithChilds(clientId: Long, ownerId: Long, objectId: Long,
//                                          objectType: ObjectType, parentCommentId: Long, countOfChildComments: Int,
//                                          offset: Int, limit: Int, sort: String): List<Comment> {
//        return selectCommentsWithChilds(clientId, ownerId, objectId, objectType, parentCommentId, countOfChildComments, offset, limit, sort, 0)
//    }
//
//    override fun selectCommentsWithChilds(clientId: Long, ownerId: Long, objectId: Long,
//                                          objectType: ObjectType, parentCommentId: Long, countOfChildComments: Int,
//                                          offset: Int, limit: Int, sort: String, lastCommentId: Long): List<Comment> {
//
//
//        val list = selectComments(clientId, ownerId, objectId, objectType, parentCommentId, offset, limit, sort, lastCommentId)
//        for (comment in list) {
//            if (comment.childsCount > 0) {
//                val childList = selectCommentsWithChilds(clientId, ownerId, objectId, objectType, comment.commentId, countOfChildComments, offset, countOfChildComments, sort, lastCommentId)
//                comment.childComments = childList
//            }
//        }
//
//        return list
//
//    }
//
//    override fun selectCommentById(profileId: Long, commentId: Long): Comment? {
//        return selectOne(Statements.Comment.SELECT_COMMENT_BY_ID, createBuilder(profileId, commentId).build()) as Comment
//    }
//
//    override fun insert(comment: Comment) {
//        if (comment.commentId == 0L) {
//            comment.commentId = sequencesDAO.selectNextLong(Comment::class.java)
//        }
//        insert(Statements.Comment.INSERT, comment)
//    }
//
//    override fun deleteByProfileId(profileId: Long, commentId: Long) {
//        deleteByProfileId(Statements.Comment.DELETE, createBuilder(profileId, commentId).build())
//    }
//
//    /**
//     * Restore deleteByProfileId comment
//     *
//     * @param profileId
//     * @param commentId
//     */
//    override fun restore(profileId: Long, commentId: Long) {
//        update(Statements.Comment.RESTORE, createBuilder(profileId, commentId).build())
//    }
//
//    override fun update(comment: Comment) {
//        update(Statements.Comment.UPDATE, comment)
//    }
//
//    /**
//     * Returns last not deleted comment identifier
//     *
//     * @param profileId
//     * @param objectId
//     * @param objectType
//     * @return
//     */
//    override fun getLastNotDeletedCommentId(profileId: Long, objectId: Long, objectType: ObjectType): Long {
//        val params = createBuilder(profileId, objectId, objectType).build()
//        val lastCommentId = selectOne(Statements.Comment.GET_LAST_NOT_DELETED_COMMENT_ID, params) as Long
//        return lastCommentId ?: 0
//    }
//
//    /**
//     * Select comments created after "startCommentId"
//     *
//     * @param profileId
//     * @param objectId
//     * @param objectType
//     * @param startCommentId
//     * @param offset
//     * @param limit
//     * @return
//     */
//    override fun selectLastComments(profileId: Long, objectId: Long, objectType: ObjectType, startCommentId: Long, sort: String,
//                                    offset: Int, limit: Int): List<Comment> {
//        val params = createBuilder(profileId, objectId, objectType).add(COMMENT_ID, startCommentId).add(SORT, sort).add(OFFSET, offset).add(LIMIT, limit).build()
//        return selectCampaignById(Statements.Comment.SELECT_LAST_COMMENTS, params)
//    }
//
//    override fun selectCommentsWithChildAsPlain(clientId: Long, profileId: Long, objectId: Long, objectType: ObjectType, offset: Int, limit: Int): List<Comment> {
//
//        val params = createBuilder(profileId, objectId, offset, limit).add(OBJECT_TYPE, objectType.code).add(CLIENT_ID, clientId).build()
//        return selectCampaignById(Statements.Comment.SELECT_COMMENTS_WITH_CHILD_AS_PLAIN, params)
//    }
//
//    override fun selectCommentsForAdminByDate(query: String?, from: Date, to: Date, limit: Int, offset: Int): ProductCommentDTOListWithFullCount {
//        var query = query
//        var id = 0
//        if (StringUtils.isNotEmpty(query)) {
//            try {
//                id = Integer.valueOf(query!!)
//                query = null
//            } catch (ignored: Exception) {
//
//            }
//
//        }
//        val dto = ProductCommentDTOListWithFullCount()
//
//        val dbParameters = createBuilder(-1)
//                .add("id", id)
//                .add("query", query)
//                .add("from", from)
//                .add("to", to)
//                .add("limit", limit)
//                .add("offset", offset)
//                .build()
//
//        var count: Int? = selectOne(Statements.Comment.SELECT_COMMENTS_FOR_ADMIN_BY_DATE_COUNT, dbParameters) as Int
//        count = if (count != null) count else 0
//        dto.count = count
//        if (count > 0) {
//            dto.list = selectCampaignById(Statements.Comment.SELECT_COMMENTS_FOR_ADMIN_BY_DATE, dbParameters)
//        }
//        return dto
//    }
//
//    override fun getCommentsCount(profileId: Long, objectId: Long, objectType: ObjectType, parentCommentId: Long): Int {
//        return selectOne(Statements.Comment.SELECT_COMMENTS_COUNT, createBuilder(profileId, objectId).add(PARENT_ID, parentCommentId).add(OBJECT_TYPE, objectType.code).build()) as Int
//    }
//
//    override fun getCommentsCount(objectId: Long, objectType: ObjectType): Int {
//        val count = selectOne(Statements.Comment.SELECT_COMMENTS_COUNT2, BaseDAO.getParameters("objectId", objectId, "objectType", objectType)) as Int
//        return count ?: 0
//    }
}

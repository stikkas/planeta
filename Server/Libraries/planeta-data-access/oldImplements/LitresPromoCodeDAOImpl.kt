package ru.planeta.dao.trashcan

import org.springframework.stereotype.Repository
import ru.planeta.dao.BaseDAO
import ru.planeta.dao.Statements
import ru.planeta.model.trashcan.LitresPromoCode

@Repository
class LitresPromoCodeDAOImpl : BaseDAO<LitresPromoCode>(), LitresPromoCodeDAO {

    override fun selectLastNonUsed(): LitresPromoCode {
        return selectOne(SELECT, null)
    }

    override fun usePromoCode(code: LitresPromoCode) {
        update(UPDATE, code)
    }

    override fun selectFreePromoCodesCount(): Int {
        return selectUnique(SELECT_COUNT, getParameters("status", LitresPromoCode.NOT_USED)) as Int
    }

    override fun selectUsedPromoCodesCount(): Int {
        return selectUnique(SELECT_COUNT, getParameters("status", LitresPromoCode.USED)) as Int
    }

    private fun selectCountByStatus(status: Int): Int {
        return selectUnique(SELECT_COUNT, getParameters("status", status)) as Int
    }

    companion object {

        private val SELECT = Statements.TRASHCAN + ".selectLitresPromoCode"
        private val SELECT_COUNT = Statements.TRASHCAN + ".selectLitresPromoCodesCount"
        private val UPDATE = Statements.TRASHCAN + ".updateLitresPromoCode"
    }
}

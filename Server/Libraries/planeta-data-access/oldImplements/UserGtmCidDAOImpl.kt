package ru.planeta.dao.trashcan

import org.springframework.stereotype.Repository
import ru.planeta.dao.BaseDAO
import ru.planeta.dao.Statements
import ru.planeta.model.trashcan.UserGtmCid

/**
 *
 * User: Serge Blagodatskih<stikkas17></stikkas17>@gmail.com><br></br>
 * Date: 27.04.16<br></br>
 * Time: 14:50
 */
@Repository
class UserGtmCidDAOImpl : BaseDAO<UserGtmCid>(), UserGtmCidDAO {

    override fun insert(userId: Long, cid: String) {
        insert(Statements.GoogleStat.INSERT_GTM_CID, getParameters("userId", userId, "cid", cid))
    }

}

package ru.planeta.dao.generated.commondb

import org.apache.ibatis.annotations.Param
import org.springframework.stereotype.Repository
import ru.planeta.dao.commondb.BaseCommonDbDAO
import ru.planeta.model.filters.generated.PaymentErrorsFilter
import ru.planeta.model.commondb.PaymentErrors
import ru.planeta.model.commondb.PaymentErrorsEx
import ru.planeta.model.commondb.PaymentErrorsExample
import ru.planeta.model.commondb.PaymentErrorsWithComments

@Repository
class PaymentErrorsMapperImpl : BaseCommonDbDAO<*>(), PaymentErrorsMapper {

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table commondb.payment_errors
     *
     * @mbggenerated Tue May 29 12:53:11 MSK 2018
     */
    override fun countByExample(example: PaymentErrorsExample): Int {
        return selectOne("ru.planeta.dao.generated.commondb.PaymentErrorsMapper.countByExample", example) as Int
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table commondb.payment_errors
     *
     * @mbggenerated Tue May 29 12:53:11 MSK 2018
     */
    override fun deleteByExample(example: PaymentErrorsExample): Int {
        return delete("ru.planeta.dao.generated.commondb.PaymentErrorsMapper.deleteByExample", example)
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table commondb.payment_errors
     *
     * @mbggenerated Tue May 29 12:53:11 MSK 2018
     */
    override fun deleteByPrimaryKey(paymentErrorId: Long?): Int {
        val params = getParameters(
                "paymentErrorId", paymentErrorId
        )
        return delete("ru.planeta.dao.generated.commondb.PaymentErrorsMapper.deleteByPrimaryKey", params)
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table commondb.payment_errors
     *
     * @mbggenerated Tue May 29 12:53:11 MSK 2018
     */
    override fun insert(record: PaymentErrors): Int {
        return insert("ru.planeta.dao.generated.commondb.PaymentErrorsMapper.insert", record)
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table commondb.payment_errors
     *
     * @mbggenerated Tue May 29 12:53:11 MSK 2018
     */
    override fun insertSelective(record: PaymentErrors): Int {
        return insert("ru.planeta.dao.generated.commondb.PaymentErrorsMapper.insertSelective", record)
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table commondb.payment_errors
     *
     * @mbggenerated Tue May 29 12:53:11 MSK 2018
     */
    override fun selectByExample(example: PaymentErrorsExample): List<PaymentErrors> {
        return selectList("ru.planeta.dao.generated.commondb.PaymentErrorsMapper.selectByExample", example)
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table commondb.payment_errors
     *
     * @mbggenerated Tue May 29 12:53:11 MSK 2018
     */
    override fun selectByPrimaryKey(paymentErrorId: Long?): PaymentErrors {
        return selectOne("ru.planeta.dao.generated.commondb.PaymentErrorsMapper.selectByPrimaryKey", paymentErrorId) as PaymentErrors
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table commondb.payment_errors
     *
     * @mbggenerated Tue May 29 12:53:11 MSK 2018
     */
    override fun updateByExampleSelective(@Param("record") record: PaymentErrors, @Param("example") example: PaymentErrorsExample): Int {
        val params = getParameters(
                "record", record,
                "example", example
        )
        return update("ru.planeta.dao.generated.commondb.PaymentErrorsMapper.updateByExampleSelective", params)
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table commondb.payment_errors
     *
     * @mbggenerated Tue May 29 12:53:11 MSK 2018
     */
    override fun updateByExample(@Param("record") record: PaymentErrors, @Param("example") example: PaymentErrorsExample): Int {
        val params = getParameters(
                "record", record,
                "example", example
        )
        return update("ru.planeta.dao.generated.commondb.PaymentErrorsMapper.updateByExample", params)
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table commondb.payment_errors
     *
     * @mbggenerated Tue May 29 12:53:11 MSK 2018
     */
    override fun updateByPrimaryKeySelective(record: PaymentErrors): Int {
        return update("ru.planeta.dao.generated.commondb.PaymentErrorsMapper.updateByPrimaryKeySelective", record)
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table commondb.payment_errors
     *
     * @mbggenerated Tue May 29 12:53:11 MSK 2018
     */
    override fun updateByPrimaryKey(record: PaymentErrors): Int {
        return update("ru.planeta.dao.generated.commondb.PaymentErrorsMapper.updateByPrimaryKey", record)
    }

    override fun selectByIdList(paymentErrorsIdList: List<Long>): List<PaymentErrors> {
        return selectList("ru.planeta.dao.generated.commondb.PaymentErrorsMapper.selectByIdList", paymentErrorsIdList)
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table commondb.payment_errors
     *
     * @mbggenerated Tue May 29 12:53:11 MSK 2018
     */
    override fun selectPaymentErrorsExByExample(example: PaymentErrorsExample): List<PaymentErrorsEx> {
        return selectList("ru.planeta.dao.generated.commondb.PaymentErrorsMapper.selectPaymentErrorsExByExample", example)
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table commondb.payment_errors
     *
     * @mbggenerated Tue May 29 12:53:11 MSK 2018
     */
    override fun selectPaymentErrorsExByPrimaryKey(paymentErrorId: Long?): PaymentErrorsEx {
        return selectOne("ru.planeta.dao.generated.commondb.PaymentErrorsMapper.selectPaymentErrorsExByPrimaryKey", paymentErrorId) as PaymentErrorsEx
    }

    override fun selectPaymentErrorsExByIdList(paymentErrorsIdList: List<Long>): List<PaymentErrorsEx> {
        return selectList("ru.planeta.dao.generated.commondb.PaymentErrorsMapper.selectPaymentErrorsExByIdList", paymentErrorsIdList)
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table commondb.payment_errors
     *
     * @mbggenerated Tue May 29 12:53:11 MSK 2018
     */
    override fun selectPaymentErrorsWithCommentsByExample(example: PaymentErrorsExample): List<PaymentErrorsWithComments> {
        return selectList("ru.planeta.dao.generated.commondb.PaymentErrorsMapper.selectPaymentErrorsWithCommentsByExample", example)
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table commondb.payment_errors
     *
     * @mbggenerated Tue May 29 12:53:11 MSK 2018
     */
    override fun selectPaymentErrorsWithCommentsByPrimaryKey(paymentErrorId: Long?): PaymentErrorsWithComments {
        return selectOne("ru.planeta.dao.generated.commondb.PaymentErrorsMapper.selectPaymentErrorsWithCommentsByPrimaryKey", paymentErrorId) as PaymentErrorsWithComments
    }

    override fun selectPaymentErrorsWithCommentsByIdList(paymentErrorsIdList: List<Long>): List<PaymentErrorsWithComments> {
        return selectList("ru.planeta.dao.generated.commondb.PaymentErrorsMapper.selectPaymentErrorsWithCommentsByIdList", paymentErrorsIdList)
    }

    override fun selectPaymentErrorsListByPaymentErrorsFilter(@Param("paymentErrorsFilter") paymentErrorsFilter: PaymentErrorsFilter): List<PaymentErrors> {
        return selectList("ru.planeta.dao.generated.commondb.PaymentErrorsMapper.selectPaymentErrorsListByPaymentErrorsFilter", paymentErrorsFilter)
    }

    override fun selectPaymentErrorsCountByFilter(@Param("paymentErrorsFilter") paymentErrorsFilter: PaymentErrorsFilter): Int {
        return selectOne("ru.planeta.dao.generated.commondb.PaymentErrorsMapper.selectPaymentErrorsCountByFilter", paymentErrorsFilter) as Int
    }

    override fun selectPaymentErrorsWithCommentsListByPaymentErrorsFilter(@Param("paymentErrorsFilter") paymentErrorsFilter: PaymentErrorsFilter): List<PaymentErrorsWithComments> {
        return selectList("ru.planeta.dao.generated.commondb.PaymentErrorsMapper.selectPaymentErrorsWithCommentsListByPaymentErrorsFilter", paymentErrorsFilter)
    }

    override fun selectPaymentErrorsExListByPaymentErrorsFilter(@Param("paymentErrorsFilter") paymentErrorsFilter: PaymentErrorsFilter): List<PaymentErrorsEx> {
        return selectList("ru.planeta.dao.generated.commondb.PaymentErrorsMapper.selectPaymentErrorsExListByPaymentErrorsFilter", paymentErrorsFilter)
    }
}

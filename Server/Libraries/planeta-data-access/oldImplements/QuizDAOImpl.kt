package ru.planeta.dao.trashcan

import org.apache.commons.lang3.StringUtils
import org.apache.commons.lang3.math.NumberUtils
import org.springframework.stereotype.Repository
import ru.planeta.dao.BaseDAO
import ru.planeta.dao.Statements
import ru.planeta.dao.profiledb.ArrayListWithCount
import ru.planeta.model.trashcan.*

@Repository
class QuizDAOImpl : BaseDAO<*>(), QuizDAO {

    override fun insert(quiz: Quiz) {
        insert(INSERT_QUIZ, quiz)
    }

    override fun update(quiz: Quiz) {
        update(UPDATE_QUIZ, quiz)
    }

    override fun insert(quizQuestion: QuizQuestion) {
        insert(INSERT_QUIZ_QUESTION, quizQuestion)
    }

    override fun insertQuizQuestionRelation(quizId: Long, quizQuestionId: Long, quizQuestionOrderNum: Long) {
        insert(INSERT_QUIZ_QUESTION_RELATION, getParameters(QUIZ_ID, quizId,
                QUIZ_QUESTION_ID, quizQuestionId,
                QUIZ_QUESTION_ORDER_NUM, quizQuestionOrderNum))
    }

    override fun selectNextQuizQuestionOrderNumInQuiz(quizId: Long): Long {
        return selectUnique(SELECT_NEXT_QUIZ_QUESTION_ORDER_NUM_IN_QUIZ, getParameters(QUIZ_ID, quizId)) as Long
    }

    override fun insertQuizQuestionOption(quizQuestionOption: QuizQuestionOption) {
        insert(INSERT_QUIZ_QUESTION_OPTION, quizQuestionOption)
    }

    override fun selectNextQuizQuestionOptionOrderNumInQuiz(quizQuestionId: Long): Long {
        return selectUnique(SELECT_NEXT_QUIZ_QUESTION_OPTION_ORDER_NUM_IN_QUIZ, getParameters(QUIZ_QUESTION_ID, quizQuestionId)) as Long
    }

    override fun selectQuizzes(searchString: String, offset: Int, limit: Int): ArrayListWithCount<Quiz> {
        val params = getParameters("searchString", searchString,
                OFFSET, offset, LIMIT, limit)
        val quizzes = selectList(SELECT_QUIZZES_SEARCH, params)

        val totalCount = selectQuizzesSearchTotalCount(params)
        val result = ArrayListWithCount<Quiz>(quizzes)
        result.totalCount = totalCount

        return result
    }

    override fun selectQuizzesSearchTotalCount(params: Map<*, *>): Int {
        return selectOne(SELECT_QUIZZES_SEARCH_TOTAL_COUNT, params) as Int
    }

    override fun selectQuizQuestions(offset: Int, limit: Int): List<QuizQuestion> {
        return selectList(SELECT_QUIZ_QUESTIONS, getParameters(OFFSET, offset, LIMIT, limit))
    }

    override fun selectQuizQuestionsByQuizId(quizId: Long, enabled: Boolean?): List<QuizQuestion> {
        return selectList(SELECT_QUIZ_QUESTIONS_BY_QUIZ_ID, getParameters(QUIZ_ID, quizId, ENABLED, enabled))
    }

    override fun selectQuizQuestionById(quizQuestionId: Long): QuizQuestion {
        return selectOne(SELECT_QUIZ_QUESTION_BY_ID, getParameters(QUIZ_QUESTION_ID, quizQuestionId)) as QuizQuestion
    }

    override fun selectQuizAnswerByParams(quizId: Long, quizQuestionId: Long, userId: Long): QuizAnswer {
        return selectOne(SELECT_QUIZ_ANSWER_BY_PARAMS, getParameters(QUIZ_ID, quizId,
                QUIZ_QUESTION_ID, quizQuestionId,
                USER_ID, userId)) as QuizAnswer
    }

    override fun selectQuizAnswersByParams(quizId: Long?, quizQuestionId: Long?, userId: Long?, offset: Long, limit: Long): List<QuizAnswer> {
        val params = getParameters(OFFSET, offset, LIMIT, limit)
        if (quizId != null) {
            params.put(QUIZ_ID, quizId)
        }
        if (quizQuestionId != null) {
            params.put(QUIZ_QUESTION_ID, quizQuestionId)
        }
        if (userId != null) {
            params.put(USER_ID, userId)
        }
        return selectList(SELECT_QUIZ_ANSWERS_BY_PARAMS, params)
    }

    override fun selectQuizAnswersForReportByParams(quizId: Long?, userId: Long?, offset: Long, limit: Long): List<QuizAnswerForReport> {
        val params = getParameters(OFFSET, offset, LIMIT, limit)
        if (quizId != null) {
            params.put(QUIZ_ID, quizId)
        }
        if (userId != null) {
            params.put(USER_ID, userId)
        }
        return selectList(SELECT_QUIZ_ANSWERS_FOR_REPORT_BY_PARAMS, params)
    }

    override fun updateQuizQuestion(quizQuestion: QuizQuestion) {
        update(UPDATE_QUIZ_QUESTION, quizQuestion)
    }

    override fun updateQuizQuestionsOrder(quizId: Long, quizQuestionId: Long, newOrderNum: Int, oldOrderNum: Int) {
        val params = getParameters(QUIZ_ID, quizId,
                QUIZ_QUESTION_ID, quizQuestionId,
                "newOrderNum", newOrderNum,
                "oldOrderNum", oldOrderNum)
        update(UPDATE_QUIZ_QUESTIONS_ORDER, params)
    }

    override fun updateQuizQuestionOptionsOrder(quizQuestionOptionId: Long, quizQuestionId: Long, newOrderNum: Int, oldOrderNum: Int) {
        val params = getParameters(QUIZ_QUESTION_OPTION_ID, quizQuestionOptionId,
                QUIZ_QUESTION_ID, quizQuestionId,
                "newOrderNum", newOrderNum,
                "oldOrderNum", oldOrderNum)
        update(UPDATE_QUIZ_QUESTION_OPTIONS_ORDER, params)
    }

    override fun updateQuizQuestionOption(quizQuestionOption: QuizQuestionOption) {
        update(UPDATE_QUIZ_QUESTION_OPTION, quizQuestionOption)
    }

    override fun updateQuizQuestionEnabling(quizQuestionId: Long, enabled: Boolean) {
        val params = getParameters(QUIZ_QUESTION_ID, quizQuestionId,
                ENABLED, enabled)
        update(UPDATE_QUIZ_QUESTION_ENABLING, params)
    }

    override fun deleteQuizQuestionOption(quizQuestionOptionId: Long) {
        delete(DELETE_QUIZ_QUESTION, getParameters(QUIZ_QUESTION_OPTION_ID, quizQuestionOptionId))
    }

    override fun selectQuiz(quizAlias: String, enabled: Boolean?): Quiz? {
        if (StringUtils.isBlank(quizAlias)) {
            return null
        }
        val quizId = NumberUtils.toLong(quizAlias, -1)
        return if (quizId == -1) {
            selectOne(SELECT_QUIZ_BY_ALIAS, getParameters(QUIZ_ALIAS, quizAlias, ENABLED, enabled)) as Quiz
        } else selectOne(SELECT_QUIZ_BY_ID, getParameters(QUIZ_ID, quizId, ENABLED, enabled)) as Quiz
    }

    override fun insertQuizAnswer(quizAnswer: QuizAnswer) {
        insert(INSERT_QUIZ_ANSWER, quizAnswer)
    }

    companion object {
        private val SELECT_QUIZZES_SEARCH = Statements.TRASHCAN + ".selectQuizzesSearch"
        private val SELECT_QUIZZES_SEARCH_TOTAL_COUNT = Statements.TRASHCAN + ".selectQuizzesSearchTotalCount"
        private val INSERT_QUIZ = Statements.TRASHCAN + ".insertQuiz"
        private val UPDATE_QUIZ = Statements.TRASHCAN + ".updateQuiz"

        private val SELECT_QUIZ_QUESTIONS = Statements.TRASHCAN + ".selectQuizQuestions"
        private val SELECT_QUIZ_QUESTION_BY_ID = Statements.TRASHCAN + ".selectQuizQuestionById"
        private val SELECT_QUIZ_QUESTIONS_BY_QUIZ_ID = Statements.TRASHCAN + ".selectQuizQuestionsByQuizId"
        private val SELECT_NEXT_QUIZ_QUESTION_ORDER_NUM_IN_QUIZ = Statements.TRASHCAN + ".selectNextQuizQuestionOrderNumInQuiz"

        private val INSERT_QUIZ_QUESTION = Statements.TRASHCAN + ".insertQuizQuestion"
        private val INSERT_QUIZ_QUESTION_RELATION = Statements.TRASHCAN + ".insertQuizQuestionRelation"
        private val UPDATE_QUIZ_QUESTION = Statements.TRASHCAN + ".updateQuizQuestionResultMap"
        private val UPDATE_QUIZ_QUESTION_ENABLING = Statements.TRASHCAN + ".updateQuizQuestionEnabling"
        private val UPDATE_QUIZ_QUESTIONS_ORDER = Statements.TRASHCAN + ".updateQuizQuestionsOrder"
        private val DELETE_QUIZ_QUESTION = Statements.TRASHCAN + ".deleteQuizQuestionOption"

        private val INSERT_QUIZ_QUESTION_OPTION = Statements.TRASHCAN + ".insertQuizQuestionOption"
        private val SELECT_NEXT_QUIZ_QUESTION_OPTION_ORDER_NUM_IN_QUIZ = Statements.TRASHCAN + ".selectNextQuizQuestionOptionOrderNumInQuizQuestion"
        private val UPDATE_QUIZ_QUESTION_OPTION = Statements.TRASHCAN + ".updateQuizQuestionOption"
        private val UPDATE_QUIZ_QUESTION_OPTIONS_ORDER = Statements.TRASHCAN + ".updateQuizQuestionOptionsOrder"

        private val INSERT_QUIZ_ANSWER = Statements.TRASHCAN + ".insertQuizAnswer"
        private val SELECT_QUIZ_ANSWER_BY_PARAMS = Statements.TRASHCAN + ".selectQuizAnswerByParams"
        private val SELECT_QUIZ_ANSWERS_BY_PARAMS = Statements.TRASHCAN + ".selectQuizAnswersByParams"
        private val SELECT_QUIZ_ANSWERS_FOR_REPORT_BY_PARAMS = Statements.TRASHCAN + ".selectQuizAnswersForReportByParams"

        private val SELECT_QUIZ_BY_ID = Statements.TRASHCAN + ".selectQuizById"
        private val SELECT_QUIZ_BY_ALIAS = Statements.TRASHCAN + ".selectQuizByAlias"

        private val USER_ID = "userId"

        private val OFFSET = "offset"
        private val LIMIT = "limit"

        private val QUIZ_ID = "quizId"
        private val QUIZ_ALIAS = "alias"
        private val QUIZ_QUESTION_ID = "quizQuestionId"
        private val QUIZ_QUESTION_OPTION_ID = "quizQuestionOptionId"
        private val QUIZ_QUESTION_ORDER_NUM = "quizQuestionOrderNum"
        private val ENABLED = "enabled"
    }
}

package ru.planeta.dao.commondb.campaign

class CampaignTagDAOImpl /*: BaseCommonDbDAO<CampaignTag>(), CampaignTagDAO {

    override fun selectAll(): List<CampaignTag> {
        return selectCampaignById(Statements.CampaignTag.SELECT_ALL, null)
    }

    override fun selectTagsByCampaign(campaignId: Long): List<CampaignTag> {
        val params = getParameters(CAMPAIGN_ID, campaignId)
        return selectCampaignById(Statements.CampaignTag.SELECT_BY_CAMPAIGN, params)
    }

    override fun updateTags(campaignId: Long, newTags: List<CampaignTag>) {
        update(Statements.CampaignTag.REWRITE_CAMPAIGN_TAGS, getParameters(CAMPAIGN_ID, campaignId, "tags", newTags))
    }

    override fun selectCampaignById(campaignTagId: Long): CampaignTag {
        return selectOne(Statements.CampaignTag.SELECT_CAMPAIGN_TAG_BY_ID, campaignTagId)
    }

    override fun insert(campaignTag: CampaignTag) {
        insert(Statements.CampaignTag.INSERT_CAMPAIGN_TAG, campaignTag)
    }

    override fun update(campaignTag: CampaignTag) {
        update(Statements.CampaignTag.UPDATE_CAMPAIGN_TAG, campaignTag)
    }

    override fun selectByAlias(sponsorAlias: String): CampaignTag {
        return selectOne(Statements.CampaignTag.SELECT_CAMPAIGN_TAG_BY_ALIAS, sponsorAlias)
    }

    override fun setTagWithManagerDefaultRelation(campaignTagId: Long) {
        insert(Statements.CampaignTag.SET_TAG_WITH_MANAGER_DEFAULT_RELATION, campaignTagId)
    }

    override fun selectByMnemonicName(name: String): CampaignTag {
        return selectOne(Statements.CampaignTag.SELECT_CAMPAIGN_TAG_BY_MNEMONIC_NAME, name)
    }

    override fun selectTagsExistsInSeminars(): List<CampaignTag> {
        return selectCampaignById(Statements.CampaignTag.SELECT_CAMPAIGN_TAGS_EXISTS_IN_SEMINARS)
    }

    override fun selectTagsForCharity(): List<CampaignTag> {
        return selectCampaignById(Statements.CampaignTag.SELECT_CAMPAIGN_TAGS_FOR_CHARITY)
    }

    companion object {

        private val CAMPAIGN_ID = "campaignId"
    }

}*/

package ru.planeta.dao.commondb.campaign


class CampaignPermissionDAOImpl/* : BaseCommonDbDAO<CampaignPermission>(), CampaignPermissionDAO {

    override fun insert(campaignPermission: CampaignPermission) {
        insert(Statements.CampaignPermission.INSERT, campaignPermission)
    }

    override fun update(campaignPermission: CampaignPermission) {
        update(Statements.CampaignPermission.UPDATE, campaignPermission)
    }

    override fun deleteByProfileId(campaignId: Long) {
        val params = getParameters(CAMPAIGN_ID, campaignId)
        deleteByProfileId(Statements.CampaignPermission.DELETE, params)
    }

    override fun selectCampaignById(campaignId: Long): CampaignPermission? {
        val params = getParameters(CAMPAIGN_ID, campaignId)
        return selectOne(Statements.CampaignPermission.SELECT, params)
    }

    override fun selectCampaignById(offset: Int, limit: Int): List<CampaignPermission> {
        val params = getParameters(BaseCommonDbDAO.Companion.OFFSET, offset, BaseCommonDbDAO.Companion.LIMIT, limit)
        return selectCampaignById(Statements.CampaignPermission.SELECT_LIST.toInt(), params.toInt())
    }

    companion object {

        private val CAMPAIGN_ID = "campaignId"
    }
}*/

package ru.planeta.dao.commondb.campaign


/**
 * Created by IntelliJ IDEA.
 * User: atropnikov
 * Date: 21.03.12
 * Time: 13:08
 */
class ShareDAOImpl /*extends BaseCommonDbDAO<Share> implements ShareDAO*///
//    private static final String SHARE_ID = "shareId";
//    private static final String SHARE_ID_LIST = "shareIdList";
//    private static final String CAMPAIGN_ID = "campaignId";
//    private static final String NONZEROPRICE = "nonZeroPrice";
//    private static final String TO = "to";
//    private static final String FROM = "from";
//    private static final String STATUS = "shareStatus";
//
//    @Override
//    public List<Share> selectCampaignShares(long campaignId, int offset, int limit) {
//        Map params = getParameters(CAMPAIGN_ID, campaignId, OFFSET, offset, LIMIT, limit);
//        return selectCampaignById(Statements.Share.SELECT_BY_CAMPAIGN, params);
//    }
//
//    @Override
//    public List<Share> selectCampaignSharesFiltered(long campaignId, EnumSet<ShareStatus> shareStatuses, boolean nonZeroPrice, int offset, int limit) {
//        Map params = getParameters(CAMPAIGN_ID, campaignId, STATUS, shareStatuses, NONZEROPRICE, nonZeroPrice, OFFSET, offset, LIMIT, limit);
//        return selectCampaignById(Statements.Share.SELECT_BY_CAMPAIGN, params);
//    }
//
//    @Override
//    public List<Share> selectCampaignById(Collection<Long> shareIds) {
//        return CollectionUtils.isEmpty(shareIds)
//                ? Collections.<Share>emptyList()
//                : selectCampaignById(Statements.Share.SELECT_BY_IDS, getParameters(SHARE_ID_LIST, shareIds));
//    }
//    @Override
//    public Share selectCampaignById(long shareId) {
//        Map params = getParameters(SHARE_ID, shareId);
//        return selectOne(Statements.Share.SELECT, params);
//    }
//
//    @Override
//    public Share selectForUpdate(long shareId) {
//        Map params = getParameters(SHARE_ID, shareId);
//        return selectOne(Statements.Share.SELECT, params);
//    }
//
//
//    @Override
//    public ShareDetails selectDetailed(long shareId) {
//        Map params = getParameters(SHARE_ID, shareId);
//        return (ShareDetails) selectOne(Statements.Share.SELECT_DETAILED, params);
//    }
//
//    @Override
//    public List<Share> selectSharesSaleInfo(long campaignId, Date from, Date to, int offset, int limit) {
//        return selectCampaignById(Statements.Share.SELECT_SALE_INFO, getParameters(CAMPAIGN_ID, campaignId, FROM, from, TO, to, OFFSET, offset, LIMIT, limit));
//    }
//
//    @Override
//    public List<Share> selectSharesPurschaseInfoOnDate(Date date) {
//        return selectCampaignById(Statements.Share.SELECT_SALE_PURSCHASE_INFO_ON_DATE, getParameters("date", date));
//    }
//
//    @Override
//    public List<Share> selectSharesPurchasedInfoOnDateByCampaignId(Date date, long campaignId) {
//        return selectCampaignById(Statements.Share.SELECT_SALE_PURSCHASE_INFO_ON_DATE_BY_CAMPAIGN_ID, getParameters("date", date));
//    }
//
//    @Override
//    public void insert(Share share) {
//        share.setTimeAdded(new Date());
//        insert(Statements.Share.INSERT, share);
//    }
//
//    @Override
//    public void update(Share share) {
//        update(Statements.Share.UPDATE, share);
//    }
//
//
//    @Override
//    public void disableShare(long shareId) {
//        Map params = getParameters(SHARE_ID, shareId);
//        update(Statements.Share.DISABLE_SHARE, params);
//    }
//
//    @Override
//    public void deleteByProfileId(long shareId) {
//        Map params = getParameters(SHARE_ID, shareId);
//        deleteByProfileId(Statements.Share.DELETE, params);
//    }
//
//    @Override
//    public void deleteCampaignShares(long campaignId) {
//        Map params = getParameters(CAMPAIGN_ID, campaignId);
//        deleteByProfileId(Statements.Share.DELETE_BY_CAMPAIGN, params);
//    }

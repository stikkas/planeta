package ru.planeta.dao.concertdb


class SectionDAOImpl /*: BaseDAO<Section>(), SectionDAO {
    override fun insert(section: Section): Int {
        return insert(Statements.Section.INSERT, section)
    }

    override fun update(section: Section): Int {
        return update(Statements.Section.UPDATE, section)
    }

    override fun selectCampaignById(sectionId: Long): Section {
        return selectOne(Statements.Section.SELECT_ONE, getParameters("sectionId", sectionId))
    }

    override fun selectByExtId(externalSectionId: Long): Section {
        return selectOne(Statements.Section.SELECT_ONE, getParameters("externalSectionId", externalSectionId))
    }

    override fun selectByExtConcertId(externalConcertId: Long): List<Section> {
        return selectCampaignById(Statements.Section.SELECT_LIST_FOR_CONCERT, getParameters("externalConcertId", externalConcertId))
    }

    override fun selectSectorsByExtConcertId(externalConcertId: Long): List<Section> {
        return selectCampaignById(Statements.Section.SELECT_LIST_FOR_CONCERT, getParameters("externalConcertId", externalConcertId, "onlySectors", true))
    }

    override fun selectRowsByExtConcertId(externalConcertId: Long, externalParentId: Long?): List<Section> {
        return selectCampaignById(Statements.Section.SELECT_LIST_FOR_CONCERT,
                getParameters("externalConcertId", externalConcertId,
                        "externalParentId", externalParentId,
                        "onlyRows", true))
    }
}*/

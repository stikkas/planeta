package ru.planeta.dao.promo

import org.springframework.stereotype.Repository
import ru.planeta.dao.commondb.BaseCommonDbDAO
import ru.planeta.model.promo.Campus

/**
 * Created by alexa_000 on 12.03.2015.
 */
@Repository
class CampusDAOImpl : BaseCommonDbDAO<*>(), CampusDAO {
    override fun select(campusId: Long): Campus {
        return selectOne(SELECT, getParameters("campusId", campusId)) as Campus
    }

    override fun selectAll(): List<Campus> {
        return selectList(SELECT_ALL)
    }

    override fun insert(campus: Campus) {
        insert(INSERT, campus)
    }

    override fun update(campus: Campus) {
        update(UPDATE, campus)
    }
}

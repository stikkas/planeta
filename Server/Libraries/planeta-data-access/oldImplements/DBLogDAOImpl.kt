package ru.planeta.dao.statdb

import org.apache.commons.collections4.CollectionUtils
import org.apache.commons.lang3.StringUtils
import org.apache.log4j.Logger
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.stereotype.Repository
import ru.planeta.dao.Statements
import ru.planeta.model.stat.log.DBLogRecord
import ru.planeta.model.stat.log.HttpInteractionLogRecord
import ru.planeta.model.stat.log.LoggerType

import javax.sql.DataSource
import java.sql.*
import java.util.*

/**
 * Database log DAO implementation.<br></br>
 * User: eshevchenko
 */
@Repository
class DBLogDAOImpl : BaseStatDbDAO<*>(), DBLogDAO {

    @Qualifier("commondbDataSource")
    @Autowired
    private val statdbDataSource: DataSource? = null

    override fun insertLogRecord(message: String, loggerType: LoggerType, objectId: Long, subjectId: Long, requestId: Long, responseId: Long): Long {
        val result = doWithinConnection(object : ConnectionExecutor<Long> {
            @Throws(SQLException::class)
            override fun execute(connection: Connection): Long? {
                return doWithPreparedStatement(connection, INSERT_DB_LOG, object : StatementExecutor<Long> {
                    @Throws(SQLException::class)
                    override fun execute(statement: Statement?, connection: Connection): Long? {
                        val preparedStatement = statement as PreparedStatement?

                        val id = getNextDBLogId(connection)

                        preparedStatement!!.setLong(1, id)                           //id
                        preparedStatement.setString(2, message)                    //message
                        preparedStatement.setInt(3, loggerType.code)          //log_type_id
                        preparedStatement.setLong(4, objectId)                     //object_id
                        preparedStatement.setLong(5, subjectId)                    //subject_id
                        preparedStatement.setLong(6, requestId)                    //request_id
                        preparedStatement.setLong(7, responseId)                   //response_id

                        preparedStatement.executeUpdate()

                        return id
                    }
                })
            }
        })

        return result ?: 0
    }

    override fun insertRequestLogRecord(metaData: String): Long {
        return insertHttpInteractionLogRecord(metaData, 0, true)
    }

    override fun insertResponseLogRecord(requestId: Long, metaData: String): Long {
        return insertHttpInteractionLogRecord(metaData, requestId, false)
    }

    private fun insertHttpInteractionLogRecord(metaData: String, requestId: Long, isRequest: Boolean): Long {
        val result = doWithinConnection(object : ConnectionExecutor<Long> {
            @Throws(SQLException::class)
            override fun execute(connection: Connection): Long? {
                return doWithPreparedStatement(connection, INSERT_HTTP_INTERACTION_LOG, object : StatementExecutor<Long> {
                    @Throws(SQLException::class)
                    override fun execute(statement: Statement?, connection: Connection): Long? {
                        val preparedStatement = statement as PreparedStatement?

                        val id = getNextHttpInteractionLogId(connection)

                        preparedStatement!!.setLong(1, id)                           //id
                        preparedStatement.setString(2, metaData)                   //meta_data
                        preparedStatement.setLong(3, requestId)                    //request_id
                        preparedStatement.setBoolean(4, isRequest)                 //is_request

                        preparedStatement.executeUpdate()

                        return id
                    }
                })
            }
        })

        return result ?: 0
    }

    override fun updateRefInfo(recordId: Long, objectId: Long, subjectId: Long, requestId: Long, responseId: Long) {
        doWithinConnection(object : ConnectionExecutor<Any> {
            @Throws(SQLException::class)
            override fun execute(connection: Connection): Any {
                return doWithPreparedStatement(connection, UPDATE_REF_INFO, object : StatementExecutor<Any> {
                    @Throws(SQLException::class)
                    override fun execute(statement: Statement?, connection: Connection): Any? {
                        val preparedStatement = statement as PreparedStatement?

                        preparedStatement!!.setLong(1, objectId)     //object_id
                        preparedStatement.setLong(2, subjectId)    //subject_id
                        preparedStatement.setLong(3, requestId)    //request_id
                        preparedStatement.setLong(4, responseId)   //response_id
                        preparedStatement.setLong(5, recordId)     //id

                        preparedStatement.executeUpdate()

                        return null
                    }
                })
            }
        })
    }

    override fun deleteTestData() {
        doWithinConnection(object : ConnectionExecutor<Any> {
            @Throws(SQLException::class)
            override fun execute(connection: Connection): Any? {
                val dbLogRecordIds = HashSet<Long>()
                val httpLogRecordIds = HashSet<Long>()

                doWithStatement(connection, object : StatementExecutor<Long> {
                    @Throws(SQLException::class)
                    override fun execute(statement: Statement?, connection: Connection): Long? {
                        var resultSet: ResultSet? = null
                        try {
                            resultSet = statement!!.executeQuery(SELECT_TEST_DATA_IDS)
                            while (resultSet!!.next()) {
                                dbLogRecordIds.add(resultSet.getLong(1))
                                httpLogRecordIds.add(resultSet.getLong(2))
                                httpLogRecordIds.add(resultSet.getLong(3))
                            }

                            return null
                        } finally {
                            silentClose(ResultSet::class.java, resultSet)
                        }
                    }
                })

                deleteFromByIds(connection, "http_interaction_log", httpLogRecordIds)
                deleteFromByIds(connection, "db_log", dbLogRecordIds)

                return null
            }
        })
    }

    private fun <T> doWithinConnection(executor: ConnectionExecutor<T>): T? {
        var result: T? = null

        var connection: Connection? = null
        try {
            connection = statdbDataSource!!.connection
            connection!!.autoCommit = false
            result = executor.execute(connection)
            connection.commit()
        } catch (e: Exception) {
            log.error("Database logging error:", e)
        } finally {
            silentClose(Connection::class.java, connection)
        }

        return result
    }

    private interface ConnectionExecutor<T> {
        @Throws(SQLException::class)
        fun execute(connection: Connection): T
    }

    private interface StatementExecutor<T> {
        @Throws(SQLException::class)
        fun execute(statement: Statement?, connection: Connection): T
    }

    override fun selectLogRecords(loggerType: LoggerType, objectId: Long, subjectId: Long): List<DBLogRecord> {
        return selectList(Statements.DBLog.SELECT_LOG_RECORDS, getParameters(
                "loggerType", loggerType,
                "objectId", objectId,
                "subjectId", subjectId
        ))
    }

    override fun selectHttpInteractionLogByRecordIds(recIds: Collection<Long>, searchString: String): List<HttpInteractionLogRecord> {
        val ids = if (CollectionUtils.isEmpty(recIds)) null else recIds
        val str = if (StringUtils.isBlank(searchString)) null else searchString.trim { it <= ' ' }
        return if (ids == null && str == null) {
            emptyList()
        } else selectList(Statements.DBLog.SELECT_HTTP_INTERACTION_LOG_BY_RECORD_IDS, getParameters(
                "ids", ids,
                "searchString", str
        ))

    }

    companion object {

        private val log = Logger.getLogger(DBLogDAOImpl::class.java)

        private val INSERT_DB_LOG = "INSERT INTO statdb.db_log(" +
                "  id,\n" +
                "  message,\n" +
                "  logger_type_id,\n" +
                "  object_id,\n" +
                "  subject_id,\n" +
                "  request_id,\n" +
                "  response_id,\n" +
                "  time_added " +
                ") VALUES (?,?,?,?,?,?,?, (extract('epoch' from now()) * 1000)::bigint)"

        private val INSERT_HTTP_INTERACTION_LOG = "INSERT INTO statdb.http_interaction_log(" +
                "  id,\n" +
                "  meta_data,\n" +
                "  request_id,\n" +
                "  is_request,\n" +
                "  time_added " +
                ") VALUES (?,?,?,?, (extract('epoch' from now()) * 1000)::bigint)"

        private val UPDATE_REF_INFO = "UPDATE statdb.db_log\n" +
                " SET object_id = ?,\n" +
                "     subject_id = ?,\n" +
                "     request_id = ?,\n" +
                "     response_id = ?\n" +
                " WHERE id = ?"

        private val SELECT_TEST_DATA_IDS = "SELECT id, request_id, response_id \n" +
                " FROM statdb.db_log \n" +
                " WHERE object_id < 0 \n" +
                " OR subject_id < 0"

        private val DELETE_TEMPLATE = "DELETE\n" +
                " FROM statdb.%s\n" +
                " WHERE id = ANY(ARRAY%s)"

        @Throws(SQLException::class)
        private fun deleteFromByIds(connection: Connection, tableName: String, ids: Set<Long>) {
            val sql = String.format(DELETE_TEMPLATE, tableName, ids)
            doWithPreparedStatement(connection, sql, object : StatementExecutor<Int> {
                @Throws(SQLException::class)
                override fun execute(statement: Statement?, connection: Connection): Int? {
                    return (statement as PreparedStatement).executeUpdate()
                }
            })
        }

        @Throws(SQLException::class)
        private fun getNextDBLogId(connection: Connection): Long {
            return seqNextVal("seq_db_log_id", connection)
        }

        @Throws(SQLException::class)
        private fun getNextHttpInteractionLogId(connection: Connection): Long {
            return seqNextVal("seq_http_interaction_log_id", connection)
        }

        @Throws(SQLException::class)
        private fun seqNextVal(seqName: String, connection: Connection): Long {
            val sql = String.format("SELECT nextval('statdb.%s');", seqName)

            return doWithStatement(connection, object : StatementExecutor<Long> {
                @Throws(SQLException::class)
                override fun execute(statement: Statement?, connection: Connection): Long? {
                    var resultSet: ResultSet? = null
                    try {
                        resultSet = statement!!.executeQuery(sql)
                        return if (resultSet!!.next()) resultSet.getLong(1) else 0
                    } finally {
                        silentClose(ResultSet::class.java, resultSet)
                    }
                }
            })
        }

        private fun <T> silentClose(closableClass: Class<T>, closable: T?) {
            if (closable != null) {
                try {
                    if (closableClass.isAssignableFrom(Connection::class.java)) {
                        (closable as Connection).close()
                    } else if (closableClass.isAssignableFrom(Statement::class.java)) {
                        (closable as Statement).close()
                    } else if (closableClass.isAssignableFrom(ResultSet::class.java)) {
                        (closable as ResultSet).close()
                    }
                } catch (e: SQLException) {
                    log.error("Can't close:", e)
                }

            }
        }

        @Throws(SQLException::class)
        private fun <T> doWithStatement(connection: Connection, executor: StatementExecutor<T>): T {
            var statement: Statement? = null
            try {
                statement = connection.createStatement()
                return executor.execute(statement, connection)
            } finally {
                silentClose(Statement::class.java, statement)
            }
        }

        @Throws(SQLException::class)
        private fun <T> doWithPreparedStatement(connection: Connection, sql: String, executor: StatementExecutor<T>): T {
            var statement: Statement? = null
            try {
                statement = connection.prepareStatement(sql)
                return executor.execute(statement, connection)
            } finally {
                silentClose(Statement::class.java, statement)
            }
        }
    }
}

package ru.planeta.dao.statdb

import org.springframework.stereotype.Repository
import ru.planeta.dao.Statements
import ru.planeta.model.stat.StatStaticNode

import java.util.*

/**
 * @author p.vyazankin
 * @since 11/28/12 12:09 PM
 */
@Repository
class StatStaticNodeDAOImpl : BaseStatDbDAO<StatStaticNode>(), StatStaticNodeDAO {

    override fun saveStatStaticNode(statStaticNode: StatStaticNode) {
        val params = getParameters("domain", statStaticNode.domain, "freeSpaceKB", statStaticNode.freeSpaceKB)
        val affected = updateOrInsert(Statements.StaticNodeStat.UPDATE_FREE_SPACE_KB, params, Statements.StaticNodeStat.INSERT, params)
        if (affected == 0) {
            throw RuntimeException("No rows were affected due to static node stats update or insert. It's not normal.")
        }
    }

    override fun getSorted(comparator: Comparator<StatStaticNode>): SortedSet<StatStaticNode> {
        val result = TreeSet(comparator)
        for (node in selectList(Statements.StaticNodeStat.SELECT_ALL, null)) {
            result.add(node)
        }
        return result
    }
}

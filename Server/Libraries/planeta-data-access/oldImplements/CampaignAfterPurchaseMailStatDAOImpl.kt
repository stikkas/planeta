package ru.planeta.dao.statdb

import org.springframework.stereotype.Repository
import ru.planeta.dao.Statements
import ru.planeta.model.stat.CampaignAfterPurchaseMailStat

@Repository
class CampaignAfterPurchaseMailStatDAOImpl : BaseStatDbDAO<CampaignAfterPurchaseMailStat>(), CampaignAfterPurchaseMailStatDAO {

    override fun insert(campaignAfterPurchaseMailStat: CampaignAfterPurchaseMailStat) {
        insert(Statements.CampaignAfterPurchaseMailStat.INSERT, campaignAfterPurchaseMailStat)
    }

    override fun update(campaignAfterPurchaseMailStat: CampaignAfterPurchaseMailStat) {
        insert(Statements.CampaignAfterPurchaseMailStat.UPDATE, campaignAfterPurchaseMailStat)
    }

    override fun selectByUserId(userId: Long): CampaignAfterPurchaseMailStat {
        return selectOne(Statements.CampaignAfterPurchaseMailStat.SELECT_BY_USER_ID, getParameters("userId", userId))
    }

    override fun selectUsersForSendingMail(offset: Long, limit: Long): List<CampaignAfterPurchaseMailStat> {
        val params = getParameters(OFFSET, offset,
                LIMIT, limit)
        return selectList(Statements.CampaignAfterPurchaseMailStat.SELECT_USERS_FOR_SENDING_MAIL, params)
    }

    companion object {
        private val OFFSET = "offset"
        private val LIMIT = "limit"
        private val ORDER_ID = "orderId"
        private val USER_ID = "userId"
    }
}

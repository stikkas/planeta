package ru.planeta.dao.trashcan

import java.util.Date
import org.springframework.stereotype.Component
import ru.planeta.dao.BaseDAO
import ru.planeta.dao.Statements

/**
 *
 * User: Serge Blagodatskih<stikkas17></stikkas17>@gmail.com><br></br>
 * Date: 27.04.16<br></br>
 * Time: 14:48
 */
@Component
class AdBlockStatisticsDAOImpl : BaseDAO<*>(), AdBlockStatisticsDAO {

    override fun hasAdBlock(profileId: Long, cid: String, ip: String) {
        insert(Statements.AdBlockStatistics.HAS, getParameters("profileId",
                profileId, "cid", cid, "ip", ip, "timeAdded", Date()))
    }
}

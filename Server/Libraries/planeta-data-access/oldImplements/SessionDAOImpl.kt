package ru.planeta.dao

import org.apache.log4j.Logger
import org.springframework.context.annotation.Scope
import org.springframework.context.annotation.ScopedProxyMode
import org.springframework.stereotype.Repository
import org.springframework.web.context.WebApplicationContext

import java.io.Serializable
import java.util.HashMap

/**
 * Created with IntelliJ IDEA.
 * User: a.volkov
 * Date: 06.05.14
 * Time: 12:44
 */
@Repository
@Scope(value = WebApplicationContext.SCOPE_SESSION, proxyMode = ScopedProxyMode.TARGET_CLASS)
class SessionDAOImpl internal constructor() : SessionDAO, Serializable {
    private val data = HashMap<String, Any>()

    init {
        ++counter
        if (counter > 50000) {
            log.error("Too many SessionDAOImpl$counter")
        }
    }

    @Throws(Throwable::class)
    protected fun finalize() {
        --counter
    }

    override fun put(key: String, `object`: Any) {
        if (data.size > 10) {
            log.error("Too mush data in session" + data.size)
        }
        data[key] = `object`
    }

    override fun get(key: String): Any? {
        return data[key]
    }

    override fun remove(key: String) {
        data.remove(key)
    }

    companion object {

        private val log = Logger.getLogger(SessionDAOImpl::class.java)
        @Volatile
        private var counter = 0
    }
}

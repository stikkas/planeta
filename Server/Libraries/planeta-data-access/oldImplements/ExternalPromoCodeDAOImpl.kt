package ru.planeta.dao.trashcan

import org.springframework.stereotype.Repository
import ru.planeta.dao.BaseDAO
import ru.planeta.dao.Statements
import ru.planeta.model.trashcan.PromoCode

@Repository
class ExternalPromoCodeDAOImpl : BaseDAO<PromoCode>(), ExternalPromoCodeDAO {

    override fun selectLastNonUsed(configId: Long): PromoCode {
        return selectOne(SELECT, getParameters("configId", configId))
    }

    override fun updatePromoCode(code: PromoCode) {
        update(UPDATE, code)
    }

    override fun usePromoCode(code: PromoCode) {
        code.status = PromoCode.USED
        update(UPDATE, code)
    }

    override fun selectAll(configId: Long): List<PromoCode> {
        return selectList(SELECT_ALL_CODES, getParameters("configId", configId))
    }

    override fun insertCodes(configId: Long, codes: List<String>) {
        insert(INSERT_CODES, getParameters("configId", configId, "codes", codes))
    }

    override fun selectUsedPromoCodesCount(configId: Long): Long {
        return selectUnique(SELECT_USED_COUNT, getParameters("configId", configId)) as Long
    }

    companion object {
        private val SELECT = Statements.TRASHCAN + ".selectExternalPromoCode"
        private val UPDATE = Statements.TRASHCAN + ".updateExternalPromoCode"
        private val SELECT_ALL_CODES = Statements.TRASHCAN + ".selectAllExternalPromoCodes"
        private val INSERT_CODES = Statements.TRASHCAN + ".insertExternalPromoCodes"
        private val SELECT_USED_COUNT = Statements.TRASHCAN + ".selectUsedPromoCodesCount"
    }
}

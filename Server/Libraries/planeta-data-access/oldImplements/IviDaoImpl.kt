package ru.planeta.dao.trashcan

import org.springframework.stereotype.Repository
import ru.planeta.dao.BaseDAO
import ru.planeta.dao.Statements
import ru.planeta.model.common.IviStat

import java.util.Date

/**
 * Created with IntelliJ IDEA.
 * User: michail
 * Date: 26.07.2017
 * Time: 11:08
 */
@Repository
class IviDaoImpl : BaseDAO<*>(), IviDao {

    override fun insert(orderId: Long) {
        insert(INSERT_ORDER, getParameters("orderId", orderId))
    }

    override fun getIviStats(from: Date, to: Date, offset: Long, limit: Int): List<IviStat> {
        return selectList(SELECT_STATS, getParameters("from", from, "to", to, "offset", offset, "limit", limit))
    }

    companion object {
        private val INSERT_ORDER = Statements.TRASHCAN + ".insertIviOrder"
        private val SELECT_STATS = Statements.TRASHCAN + ".getIviStats"
    }
}

package ru.planeta.dao.promo

import org.springframework.stereotype.Repository
import ru.planeta.dao.BaseDAO
import ru.planeta.model.promo.TechnobattleVote
import ru.planeta.model.promo.VoteType

/**
 * Created with IntelliJ IDEA.
 * User: michail
 * Date: 16.01.2017
 * Time: 16:44
 */
@Repository
class TechnobattleVoterDAOImpl : BaseDAO<TechnobattleVote>(), TechnobattleVoterDAO {
    private interface Statements {
        companion object {
            val INSERT = ru.planeta.dao.Statements.PROMODB + ".insertTechnobattleVote"
            val HAS_EXISTING_VOTES = ru.planeta.dao.Statements.PROMODB + ".selectExistingVoteCount"
            val SELECT_VOTE_FOR_PROJECT = ru.planeta.dao.Statements.PROMODB + ".selectVoteForProject"
        }
    }


    override fun insert(vote: TechnobattleVote) {
        insert(Statements.INSERT, vote)
    }

    override fun selectVotesCountForUser(userId: String, voteType: VoteType): Int {
        return selectUnique(Statements.HAS_EXISTING_VOTES, getParameters("userId", userId, "voteType", voteType)) as Int
    }

    override fun selectVotesCountForProject(projectId: Long): Int {
        return selectUnique(Statements.SELECT_VOTE_FOR_PROJECT, getParameters("projectId", projectId)) as Int
    }
}

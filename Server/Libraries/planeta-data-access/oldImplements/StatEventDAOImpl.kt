package ru.planeta.dao.statdb

import org.springframework.stereotype.Repository
import ru.planeta.dao.Statements
import ru.planeta.dao.commondb.BaseCommonDbDAO
import ru.planeta.model.stat.StatEvent

/**
 * Created with IntelliJ IDEA.
 * User: a.volkov
 * Date: 01.08.14
 * Time: 18:38
 */

@Repository
class StatEventDAOImpl : BaseCommonDbDAO<*>(), StatEventDAO {

    override fun insert(event: StatEvent) {
        insert(Statements.StatEvent.INSERT, event)
    }

    override fun cleanAggregatedRecords() {
        delete(Statements.StatEvent.CLEAN, null)
    }

}

package ru.planeta.dao.shopdb


class ProductDAOImpl /*extends BaseShopDbDAO implements ProductDAO*///
//    @Override
//    public void updateStatusCurrentToOld(long productId, boolean withChildren) {
//        Map<String, Object> parameters = getParameters(withChildren ? PARENT_PRODUCT_ID : PRODUCT_ID, productId);
//        parameters.put(TO_STATUS, ProductStatus.OLD.getCode());
//        update(UPDATE_CURRENT_TO_OLD, parameters);
//    }
//
//    @Override
//    public void updateProductNameAndDescriptionHtml(long productId, String name_html, String description_html) {
//        Map<String, Object> parameters = getParameters(PRODUCT_ID, productId,
//                NAME_HTML, name_html,
//                DESCRIPTION_HTML, description_html);
//        update(UPDATE_PRODUCT_NAME_AND_DESCRIPTION_HTML, parameters);
//    }
//
//    @Override
//    public void updateStatusActiveToPause(long productId) {
//        updateStatusAtoB(productId, ProductStatus.ACTIVE, ProductStatus.PAUSE);
//    }
//
//    @Override
//    public void updateStatusPauseToActive(long productId) {
//        updateStatusAtoB(productId, ProductStatus.PAUSE, ProductStatus.ACTIVE);
//    }
//
//    @Override
//    public void updateStatusPauseToActiveAndSetStartDate(long productId) {
//        updateStatusToStartAndSetStartDate(productId);
//    }
//
//    private void updateStatusAtoB(long productId, ProductStatus a, ProductStatus b) {
//        Map<String, Object> parameters = getParameters(FROM_STATUS, a.getCode(), TO_STATUS, b.getCode());
//        parameters.put(PRODUCT_ID, productId);
//        update(UPDATE_STATUS_A_TO_B, parameters);
//    }
//
//    private void updateStatusToStartAndSetStartDate(long productId) {
//        update(UPDATE_STATUS_TO_START_AND_SET_START_DATE, getParameters(PRODUCT_ID, productId, TIME_STARTED, new Date()));
//    }
//
//    @Override
//    public long selectCurrentProductsCount() {
//        return count(SELECT_PRODUCTS_CURRENT_VERSION_COUNT);
//    }
//
//    @Override
//    public List<Product> selectChildrenProductsByStatus(long parentId, ProductStatus status) {
//        return selectCampaignById(SELECT_PRODUCTS, getParameters(PARENT_PRODUCT_ID, parentId,
//                "statusCode", status.getCode()));
//    }
//
//    @Override
//    public void save(Product product) {
//        if (product.getProductId() > 0) {
//            update(UPDATE_DETAILS, product);
//        } else {
//            insert(INSERT, product);
//        }
//    }
//
//    @Override
//    public Product selectProduct(long productId) {
//        return (Product) selectOne(SELECT_PRODUCTS, getParameters(PRODUCT_ID, productId));
//    }
//
//    @Override
//    public List<Product> getSimilarProducts(long productId, int limit) {
//        Map<String, Object> params = getParameters(OFFSET, 0, LIMIT, limit, "productId", productId, "statusCode", ProductStatus.ACTIVE.getCode());
//        return selectCampaignById(SELECT_SIMILAR_PRODUCTS, params);
//    }
//
//    @Override
//    public Product selectCurrent(long productId) {
//        return (Product) selectOne(SELECT_PRODUCTS_CURRENT_VERSION, getParameters(PRODUCT_ID, productId));
//    }
//
//    @Override
//    public void updateTotalQuantityDelta(long productId, long delta) {
//        if (productId == 0) return;
//        update(UPDATE_TOTAL_QUANTITY_DELTA, getParameters(PRODUCT_ID, productId, QUANTITY, delta));
//    }
//
//    @Override
//    public void updateTotalQuantity(long productId, long value) {
//        if (productId == 0) return;
//        update(UPDATE_TOTAL_QUANTITY, getParameters(PRODUCT_ID, productId, QUANTITY, value));
//    }
//
//    @Override
//    public List<ProductInfo> selectActiveProductByIdList(List<Long> productIdList) {
//        if (productIdList == null || productIdList.isEmpty()) {
//            return Collections.emptyList();
//        }
//        return sort((List<ProductInfo>) selectCampaignById(SELECT_PRODUCTS_BY_ID_LIST, getParameters(PRODUCT_IDS, productIdList, "statusCode", ProductStatus.ACTIVE.getCode())), productIdList);
//    }
//
//    private static List<ProductInfo> sort(List<ProductInfo> productInfoList, List<Long> productIdList) {
//        if (productInfoList == null || productInfoList.size() < 2) {
//            return productInfoList;
//        }
//        List<ProductInfo> result = new ArrayList<>();
//        for (Long productId : productIdList) {
//            for (ProductInfo productInfo : productInfoList) {
//                if (productInfo.getProductId() == productId) {
//                    result.add(productInfo);
//                    break;
//                }
//            }
//        }
//        return result;
//    }
//
//    @Override
//    public List<Product> selectChildrenProducts(long productId) {
//        return selectCampaignById(SELECT_PRODUCTS_CURRENT_VERSION, getParameters(PARENT_PRODUCT_ID, productId));
//    }
//
//    @Override
//    public void updateTotalPurchaseCount(long productId, int quantity) {
//        Map<String, Object> params = getParameters(PRODUCT_ID, productId, QUANTITY, quantity, PRODUCT_STATE, ProductState.ATTRIBUTE.getCode());
//        update(UPDATE_TOTAL_PURCHASE_COUNT, params);
//    }
//
//    @Override
//    public void freeChildren(long productId) {
//        if (productId == 0) return;
//        update(LET_THEM_BE_FREE, getParameters(PARENT_PRODUCT_ID, productId, NEW_PARENT_ID, 0));
//    }
//
//
//    @Override
//    public List selectTopLevelProducts(int offset, int limit, String query, int tagId, String status) {
//        Map<String, Object> params = getParameters(OFFSET, offset, LIMIT, limit, PARENT_PRODUCT_ID, 0);
//        params.put(SEARCH_STRING, query);
//        params.put("tagId", tagId);
//        params.put("status", status);
//        return selectCampaignById(SELECT_PRODUCTS_FOR_ADMIN_SEARCH, params);
//    }
//
//
//    @Override
//    public List<Product> selectByIdList(List<Long> productIdList) {
//        if (productIdList == null || productIdList.isEmpty()) {
//            return Collections.emptyList();
//        }
//        return (List<Product>) selectCampaignById(SELECT_PRODUCTS_BY_ID_LIST, getParameters(PRODUCT_IDS, productIdList));
//    }
//
//    @Override
//    public void bindToProfile(long productId, long referrerId, boolean showOnCampaign, long[] campaignIds) {
//        update(BIND_TO_PROFILE, getParameters(PRODUCT_ID, productId, REFERRER_ID, referrerId, SHOW_ON_CAMPAIGN, showOnCampaign, CAMPAIGN_IDS, campaignIds));
//    }
//
//    @Override
//    public List<Product> getProductListForCampaignByReferrer(long referrerId, long campaignId, int offset, int limit) {
//        return selectCampaignById(SELECT_TOP_READY_FOR_PURCHASE_PRODUCTS_WITH_PRICE_RANGE,
//                getParameters(REFERRER_ID, referrerId, CAMPAIGN_ID, campaignId, OFFSET, offset, LIMIT, limit));
//    }
//
//    @Override
//    public int getProductsCountByReferrer(long referrerId) {
//        return (Integer) selectOne(SELECT_TOP_READY_FOR_PURCHASE_PRODUCTS_WITH_PRICE_RANGE_COUNT, getParameters(REFERRER_ID, referrerId));
//    }
//
//    @Override
//    public List<ProductInfo> selectProductsByIds(List<Long> ids) {
//        if (CollectionUtils.isEmpty(ids)) {
//            return new ArrayList<>();
//        }
//        return selectCampaignById(SELECT_PRODUCTS_BY_IDS, getParameters("ids", ids));
//    }
//
//    @Override
//    public long getDonateProductByReferrer(long referrerId) {
//        return (long) selectOne(SELECT_DONATE_BY_REFERRER, getParameters("referrerId", referrerId));
//    }

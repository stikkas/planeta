package ru.planeta.dao.statdb

import org.springframework.stereotype.Repository
import ru.planeta.dao.Statements
import ru.planeta.dao.commondb.BaseCommonDbDAO
import ru.planeta.commons.model.OrderShortLinkStat

@Repository
class OrderShortLinkStatDAOImpl : BaseCommonDbDAO<OrderShortLinkStat>(), OrderShortLinkStatDAO {
    override fun insert(orderShortLinkStat: OrderShortLinkStat) {
        insert(Statements.OrderShortLinkStat.INSERT, orderShortLinkStat)
    }

    override fun selectById(shortLinkStatsId: Long): OrderShortLinkStat {
        return selectOne(Statements.OrderShortLinkStat.SELECT_BY_ID, getParameters("shortLinkStatsId", shortLinkStatsId))
    }
}

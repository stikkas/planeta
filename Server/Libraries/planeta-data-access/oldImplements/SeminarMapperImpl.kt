package ru.planeta.dao.commondb.school

class SeminarMapperImpl /*: BaseCommonDbDAO<*>(), SeminarDAO {

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table commondb.seminar
     *
     * @mbggenerated Mon Feb 01 20:11:43 MSK 2016
     */
    override fun countByExample(example: SeminarExample): Int {
        return selectOne("ru.planeta.dao.commondb.SeminarDAO.countByExample", example) as Int
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table commondb.seminar
     *
     * @mbggenerated Mon Feb 01 20:11:43 MSK 2016
     */
    override fun deleteByExample(example: SeminarExample): Int {
        return deleteByProfileId("ru.planeta.dao.commondb.SeminarDAO.deleteByExample", example)
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table commondb.seminar
     *
     * @mbggenerated Mon Feb 01 20:11:43 MSK 2016
     */
    override fun deleteByPrimaryKey(seminarId: Long?): Int {
        val params = getParameters(
                "seminarId", seminarId
        )
        return deleteByProfileId("ru.planeta.dao.commondb.SeminarDAO.deleteByPrimaryKey", params)
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table commondb.seminar
     *
     * @mbggenerated Mon Feb 01 20:11:43 MSK 2016
     */
    override fun insert(record: Seminar): Int {
        return insert("ru.planeta.dao.commondb.SeminarDAO.insert", record)
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table commondb.seminar
     *
     * @mbggenerated Mon Feb 01 20:11:43 MSK 2016
     */
    override fun insertSelective(record: Seminar): Int {
        return insert("ru.planeta.dao.commondb.SeminarDAO.insertSelective", record)
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table commondb.seminar
     *
     * @mbggenerated Mon Feb 01 20:11:43 MSK 2016
     */
    override fun selectByExample(example: SeminarExample): List<Seminar> {
        return selectCampaignById("ru.planeta.dao.commondb.SeminarDAO.selectByExample", example)
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table commondb.seminar
     *
     * @mbggenerated Mon Feb 01 20:11:43 MSK 2016
     */
    override fun selectByPrimaryKey(seminarId: Long?): Seminar {
        return selectOne("ru.planeta.dao.commondb.SeminarDAO.selectByPrimaryKey", seminarId) as Seminar
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table commondb.seminar
     *
     * @mbggenerated Mon Feb 01 20:11:43 MSK 2016
     */
    override fun updateByExampleSelective(@Param("record") record: Seminar, @Param("example") example: SeminarExample): Int {
        val params = getParameters(
                "record", record,
                "example", example
        )
        return update("ru.planeta.dao.commondb.SeminarDAO.updateByExampleSelective", params)
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table commondb.seminar
     *
     * @mbggenerated Mon Feb 01 20:11:43 MSK 2016
     */
    override fun updateByExample(@Param("record") record: Seminar, @Param("example") example: SeminarExample): Int {
        val params = getParameters(
                "record", record,
                "example", example
        )
        return update("ru.planeta.dao.commondb.SeminarDAO.updateByExample", params)
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table commondb.seminar
     *
     * @mbggenerated Mon Feb 01 20:11:43 MSK 2016
     */
    override fun updateByPrimaryKeySelective(record: Seminar): Int {
        return update("ru.planeta.dao.commondb.SeminarDAO.updateByPrimaryKeySelective", record)
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table commondb.seminar
     *
     * @mbggenerated Mon Feb 01 20:11:43 MSK 2016
     */
    override fun updateByPrimaryKey(record: Seminar): Int {
        return update("ru.planeta.dao.commondb.SeminarDAO.updateByPrimaryKey", record)
    }

    override fun selectByIdList(seminarIdList: List<Long>): List<Seminar> {
        return selectCampaignById("ru.planeta.dao.commondb.SeminarDAO.selectByIdList", seminarIdList)
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table commondb.seminar
     *
     * @mbggenerated Mon Feb 01 20:11:43 MSK 2016
     */
    override fun selectSeminarWithTagNameAndCityNameByExample(example: SeminarExample): List<SeminarWithTagNameAndCityName> {
        return selectCampaignById("ru.planeta.dao.commondb.SeminarDAO.selectSeminarWithTagNameAndCityNameByExample", example)
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table commondb.seminar
     *
     * @mbggenerated Mon Feb 01 20:11:43 MSK 2016
     */
    override fun selectSeminarWithTagNameAndCityNameByPrimaryKey(seminarId: Long?): SeminarWithTagNameAndCityName {
        return selectOne("ru.planeta.dao.commondb.SeminarDAO.selectSeminarWithTagNameAndCityNameByPrimaryKey", seminarId) as SeminarWithTagNameAndCityName
    }

    override fun selectSeminarWithTagNameAndCityNameByIdList(seminarIdList: List<Long>): List<SeminarWithTagNameAndCityName> {
        return selectCampaignById("ru.planeta.dao.commondb.SeminarDAO.selectSeminarWithTagNameAndCityNameByIdList", seminarIdList)
    }
}*/

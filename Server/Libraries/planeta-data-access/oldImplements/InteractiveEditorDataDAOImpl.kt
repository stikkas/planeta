package ru.planeta.dao.trashcan

import org.springframework.stereotype.Repository
import ru.planeta.dao.BaseDAO
import ru.planeta.dao.Statements
import ru.planeta.model.trashcan.InteractiveEditorData

@Repository
class InteractiveEditorDataDAOImpl : BaseDAO<*>(), InteractiveEditorDataDAO {

    override fun insert(interactiveEditorData: InteractiveEditorData) {
        insert(INSERT_INTERACTIVE_EDITOR_DATA, interactiveEditorData)
    }

    override fun selectByUserId(userId: Long): InteractiveEditorData {
        return selectOne(SELECT_INTERACTIVE_EDITOR_DATA_BY_USER_ID, getParameters(USER_ID, userId)) as InteractiveEditorData
    }

    override fun selectByEmail(email: String): InteractiveEditorData {
        return selectOne(SELECT_INTERACTIVE_EDITOR_DATA_BY_USER_ID, getParameters(EMAIL, email)) as InteractiveEditorData
    }

    override fun update(interactiveEditorData: InteractiveEditorData) {
        update(UPDATE_INTERACTIVE_EDITOR_DATA, interactiveEditorData)
    }

    override fun deleteByUserId(userId: Long): Int {
        val params = getParameters(
                USER_ID, userId
        )
        return delete(DELETE_INTERACTIVE_EDITOR_DATA, params)
    }

    companion object {
        private val SELECT_INTERACTIVE_EDITOR_DATA_BY_USER_ID = Statements.TRASHCAN + ".selectInteractiveEditorDataByUserId"
        private val SELECT_INTERACTIVE_EDITOR_DATA_BY_EMAIL = Statements.TRASHCAN + ".selectInteractiveEditorDataByEmail"

        private val INSERT_INTERACTIVE_EDITOR_DATA = Statements.TRASHCAN + ".insertInteractiveEditorData"
        private val UPDATE_INTERACTIVE_EDITOR_DATA = Statements.TRASHCAN + ".updateInteractiveEditorData"
        private val DELETE_INTERACTIVE_EDITOR_DATA = Statements.TRASHCAN + ".deleteInteractiveEditorData"

        private val USER_ID = "userId"
        private val CAMPAIGN_ID = "campaignId"
        private val EMAIL = "email"

        private val OFFSET = "offset"
        private val LIMIT = "limit"
    }
}

package ru.planeta.dao.commondb

import org.springframework.stereotype.Repository
import ru.planeta.dao.Statements
import ru.planeta.model.common.AuthToken

/**
 * Date: 20.06.12
 *
 * @author s.kalmykov
 */
@Repository
class AuthTokenRepositoryImpl : BaseCommonDbDAO<AuthToken>(), AuthTokenRepository {

    override fun findByToken(token: String): AuthToken {
        return selectOne(Statements.AuthToken.FIND_BY_TOKEN, getParameters("token", token))
    }

    override fun delete(token: AuthToken) {
        delete(Statements.AuthToken.DELETE, token)
    }

    override fun save(token: AuthToken) {
        insert(Statements.AuthToken.SAVE, token)
    }
}

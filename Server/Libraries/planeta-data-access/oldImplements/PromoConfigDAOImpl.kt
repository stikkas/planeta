package ru.planeta.dao.trashcan

import org.springframework.stereotype.Repository
import ru.planeta.dao.BaseDAO
import ru.planeta.dao.Statements
import ru.planeta.model.trashcan.PromoConfig

import java.math.BigDecimal

@Repository
class PromoConfigDAOImpl : BaseDAO<PromoConfig>(), PromoConfigDAO {

    override fun selectAll(): List<PromoConfig> {
        return selectList(SELECT_ALL)
    }

    override fun selectCompatible(campaignTags: Collection<Long>, totalPrice: BigDecimal): List<PromoConfig> {
        return selectList(SELECT_COMPATIBLE, getParameters("campaignTags", campaignTags, "totalPrice", totalPrice))
    }

    override fun insert(config: PromoConfig) {
        insert(INSERT, config)
    }

    override fun update(config: PromoConfig) {
        update(UDPATE, config)
    }

    override fun selectById(configId: Long): PromoConfig {
        return selectOne(SELECT_ONE, getParameters("configId", configId))
    }

    override fun finishConfigs() {
        update(FINISH, null)
    }

    companion object {
        private val INSERT = Statements.TRASHCAN + ".insertPromoConfig"
        private val UDPATE = Statements.TRASHCAN + ".updatePromoConfig"
        private val SELECT_ALL = Statements.TRASHCAN + ".selectAllPromoConfigs"
        private val SELECT_COMPATIBLE = Statements.TRASHCAN + ".selectCompatiblePromoConfigs"
        private val SELECT_ONE = Statements.TRASHCAN + ".selectConfigById"
        private val FINISH = Statements.TRASHCAN + ".finishPromoConfigs"
    }
}

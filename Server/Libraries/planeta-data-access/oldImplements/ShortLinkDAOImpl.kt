package ru.planeta.dao.commondb


/**
 * Class BannerDAOImpl
 *
 * @author a.savanovich
 */
class ShortLinkDAOImpl /*extends BaseCommonDbDAO<ShortLink> implements ShortLinkDAO*///
//    @Override
//    public ShortLink selectCampaignById(long linkId) {
//        return selectOne(Statements.ShortLink.SELECT, linkId);
//    }
//
//    @Override
//    public ShortLink selectByAlias(String shortLink) {
//        return selectOne(Statements.ShortLink.SELECT_BY_ALIAS, shortLink);
//    }
//
//    @Override
//    public List<ShortLink> selectListByProfileId(long profileId, int offset, int limit) {
//        return selectCampaignById(Statements.ShortLink.SELECT_LIST_BY_PROFILE_ID,
//                getParameters(OFFSET, offset, LIMIT, limit, "profileId", profileId));
//    }
//
//    @Override
//    public List<ShortLink> selectExistsSocialShortLinksByBasename(String shortlinkBasename,
//                                                                  List<String> socialShortlinkAdditionsList,
//                                                                  int offset, int limit) {
//        return selectCampaignById(Statements.ShortLink.SELECT_EXISTS_SOCIAL_SHORTLINKS_BY_BASENAME,
//                getParameters(OFFSET, offset, LIMIT, limit,
//                        "shortlinkBasename", shortlinkBasename,
//                        "socialShortlinkAdditionsList", socialShortlinkAdditionsList));
//    }
//
//
//    @Override
//    public List<ShortLink> selectCampaignById(EnumSet<ShortLinkStatus> statuses, int offset, int limit) {
//        if (statuses == null || statuses.isEmpty()) {
//            return Collections.emptyList();
//        }
//        int[] statusCodes = getStatusCodes(statuses);
//        Map params = getParameters(OFFSET, offset, LIMIT, limit, "statusCodes", statusCodes);
//        return selectCampaignById(Statements.ShortLink.SELECT_LIST, params);
//    }
//
//
//    @Override
//    public void insert(ShortLink link) {
//        insert(Statements.ShortLink.INSERT, link);
//    }
//
//
//    @Override
//    public void update(ShortLink link) {
//        update(Statements.ShortLink.UPDATE, link);
//    }
//
//    @Override
//    public void deleteByProfileId(long linkId) {
//        deleteByProfileId(Statements.ShortLink.DELETE, linkId);
//    }
//
//    private static int[] getStatusCodes(EnumSet<ShortLinkStatus> statuses) {
//        int[] statusCodes = new int[statuses.size()];
//        int i = 0;
//        for (ShortLinkStatus status : statuses) {
//            statusCodes[i] = status.getCode();
//            i++;
//        }
//        return statusCodes;
//    }

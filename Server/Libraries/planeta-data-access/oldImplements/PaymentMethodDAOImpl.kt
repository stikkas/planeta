package ru.planeta.dao.payment

import org.springframework.stereotype.Repository
import ru.planeta.dao.Statements
import ru.planeta.dao.commondb.BaseCommonDbDAO
import ru.planeta.model.common.PaymentMethod
import ru.planeta.model.enums.ProjectType

/**
 * Created with IntelliJ IDEA.
 * User: a.volkov
 * Date: 24.04.14
 * Time: 13:28
 */
@Repository
class PaymentMethodDAOImpl : BaseCommonDbDAO<PaymentMethod>(), PaymentMethodDAO {

    override val paymentMethods: List<PaymentMethod>
        get() = selectList(Statements.PaymentMethod.SELECT_ALL)

    override fun select(id: Long): PaymentMethod {
        return selectOne(Statements.PaymentMethod.SELECT, id)
    }

    override fun update(method: PaymentMethod): PaymentMethod {
        update(Statements.PaymentMethod.UPDATE, method)
        return method
    }

    override fun updateProjectPaymentMethod(projectType: ProjectType, paymentMethodId: Long, enabled: Boolean) {
        update(Statements.PaymentMethod.UPDATE_PROJECT_PAYMENT_METHOD, getParameters(
                "projectType", projectType,
                "paymentMethodId", paymentMethodId,
                "enabled", enabled
        ))
    }

    override fun getPaymentMethodsForProject(projectType: ProjectType, internal: Boolean?, promo: Boolean?, old: Boolean?): List<PaymentMethod> {
        return selectList(if (old)
            Statements.PaymentMethod.SELECT_FOR_PROJECT_OLD
        else
            Statements.PaymentMethod.SELECT_FOR_PROJECT, getParameters(
                "projectType", projectType,
                "internal", internal,
                "promo", promo
        ))
    }

    override fun getPaymentMethodsForProject(projectType: ProjectType, internal: Boolean?, promo: Boolean?): List<PaymentMethod> {
        return getPaymentMethodsForProject(projectType, internal, promo, projectType !== ProjectType.MAIN)
    }

    override fun getInternalPaymentMethod(projectType: ProjectType): PaymentMethod {
        return selectOne(Statements.PaymentMethod.SELECT_FOR_PROJECT, getParameters(
                "projectType", projectType,
                "internal", true,
                "promo", false
        ))
    }

    override fun getPromoPaymentMethod(projectType: ProjectType): PaymentMethod {
        return selectOne(Statements.PaymentMethod.SELECT_FOR_PROJECT, getParameters(
                "projectType", projectType,
                "internal", true,
                "promo", true
        ))
    }

    override fun selectByAlias(alias: String): PaymentMethod {
        return selectOne(Statements.PaymentMethod.SELECT_BY_ALIAS, getParameters(
                "alias", alias
        ))
    }
}

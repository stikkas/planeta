package ru.planeta.dao.commondb.campaign

class CampaignEditTimeDAOImpl /*: BaseCommonDbDAO<*>(), CampaignEditTimeDAO {

    override fun insert(campaignId: Long, profileId: Long): CampaignEditTime {
        val now = Date()
        insert(INSERT, getParameters(BaseCommonDbDAO.Companion.CAMPAIGN_ID, campaignId, BaseCommonDbDAO.Companion.PROFILE_ID,
                profileId, "lastTimeActive", now))
        return CampaignEditTime(campaignId, profileId, now)
    }

    override fun update(campaignId: Long, profileId: Long): CampaignEditTime {
        val now = Date()
        update(UPDATE, getParameters(BaseCommonDbDAO.Companion.CAMPAIGN_ID, campaignId, BaseCommonDbDAO.Companion.PROFILE_ID,
                profileId, "lastTimeActive", now))
        return CampaignEditTime(campaignId, profileId, now)
    }

    override fun deleteByProfileId(campaignId: Long, profileId: Long) {
        deleteByProfileId(DELETE, getParameters(BaseCommonDbDAO.Companion.CAMPAIGN_ID, campaignId, BaseCommonDbDAO.Companion.PROFILE_ID,
                profileId))
    }

    override fun selectLast(campaignId: Long, profileId: Long): CampaignEditTime {
        return selectOne(SELECT_LAST, getParameters(BaseCommonDbDAO.Companion.CAMPAIGN_ID, campaignId,
                BaseCommonDbDAO.Companion.PROFILE_ID, profileId)) as CampaignEditTime
    }

    override fun checkIfExists(campaignId: Long, profileId: Long): Boolean {
        return selectOne(EXISTS, getParameters(BaseCommonDbDAO.Companion.CAMPAIGN_ID, campaignId,
                BaseCommonDbDAO.Companion.PROFILE_ID, profileId)) as Boolean
    }

}*/

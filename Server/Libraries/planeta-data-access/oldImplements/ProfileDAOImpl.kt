package ru.planeta.dao.profiledb


/**
 * Class ProfileDAOImpl
 *
 * @author a.tropnikov
 */
//@Repository
class ProfileDAOImpl /*extends BaseClusterDAO<Profile> implements ProfileDAO*///
//    @Override
//    public Profile selectByOrderId(long profileId) {
//        return selectOne(Statements.Profile.SELECT_BY_ID, profileId);
//    }
//
//    @Override
//    public Profile selectWithDeletedById(long profileId) {
//        return selectOne(Statements.Profile.SELECT_WITH_DELETED_BY_ID, profileId);
//    }
//
//    @Override
//    public Profile selectByAlias(String alias) {
//        return selectOne(Statements.Profile.SELECT_BY_ALIAS, alias);
//    }
//
//    @Override
//    public List<Profile> selectSearchedByIds(Collection<Long> profileIds, long clientId) {
//        if (CollectionUtils.isEmpty(profileIds)) {
//            return Collections.emptyList();
//        }
//        ProfileDbParameters params = createBuilder(0).
//                add(CLIENT_ID, clientId).
//                add(PROFILE_IDS, profileIds).
//                build();
//        return selectCampaignById(Statements.Profile.SELECT_SEARCHED_BY_ID_LIST, params);
//    }
//
//    @Override
//    public List<Profile> selectSearchedByIds(Collection<Long> profileIds) {
//        if (profileIds == null || profileIds.isEmpty()) {
//            return Collections.emptyList();
//        }
//        final int maxLength = Short.MAX_VALUE - 1;
//
//        int ln = profileIds.size();
//        if (ln <= maxLength) {
//            List<Long> profileIdList = new ArrayList<>(profileIds);
//            return selectCampaignById(Statements.Profile.SELECT_BY_ID_LIST, profileIdList);
//        }
//
//        List<Profile> result = new ArrayList<>(profileIds.size());
//        List<Long> profileIdList = new ArrayList<>(profileIds);
//        int start = 0;
//        int end = maxLength;
//        while (ln < end) {
//            result.addAll(selectCampaignById(Statements.Profile.SELECT_BY_ID_LIST, profileIdList.subList(start, end)));
//            start += maxLength;
//            end += maxLength;
//            if (end > ln) {
//                end = ln;
//            }
//        }
//        return result;
//    }
//
//    @Override
//    public void insert(Profile profile) {
//        if (profile.getProfileId() == 0) {
//            profile.setProfileId(getSequencesDAO().selectNextLong(Profile.class));
//        }
//        insert(Statements.Profile.INSERT, profile);
//    }
//
//    @Override
//    public void update(Profile profile) {
//        update(Statements.Profile.UPDATE, profile);
//    }
//
//    @Override
//    public void deleteByProfileId(long profileId) {
//        deleteByProfileId(Statements.Profile.DELETE, profileId);
//    }
//
//    @Override
//    public Profile getHiddenGroup(long clientId) {
//        return selectOne(Statements.Profile.SELECT_HIDDEN_GROUP, clientId);
//    }
//
//    @Override
//    public List<Profile> searchProfiles(long clientId, int offset, int limit) {
//        return selectCampaignById(Statements.Profile.SEARCH_PROFILES, createBuilder(0, offset, limit).add(CLIENT_ID, clientId).build());
//    }
//
//    @Override
//    public long selectProfilesCount() {
//        return count(Statements.Profile.SELECT_PROFILES_COUNT);
//    }
//
//    @Override
//    public void updateProjectsCount(long profileId) {
//        update(Statements.Profile.UPDATE_PROJECTS_COUNT, getParameters("profileId", profileId));
//    }
//
//    @Override
//    public void updateHasActiveProjects(long profileId) {
//        update(Statements.Profile.UPDATE_HAS_ACTIVE_PROJECTS, getParameters("profileId", profileId));
//    }
//
//    @Override
//    public void updateBackedCount(long profileId) {
//        update(Statements.Profile.UPDATE_BACKED_COUNT, getParameters("profileId", profileId));
//    }
//
//    @Override
//    public void updateSubscribersCount(long profileId) {
//        update(Statements.Profile.UPDATE_SUBSCRIBERS_COUNT, getParameters("profileId", profileId));
//    }
//
//    @Override
//    public void updateNewSubscribersCount(long profileId, long newSubscribersCount) {
//        update(Statements.Profile.UPDATE_NEW_SUBSCRIBERS_COUNT, getParameters("profileId", profileId,
//                "newSubscribersCount", newSubscribersCount));
//    }
//
//    @Override
//    public List getSpammers(Date date, int offset, int limit) {
//        return selectCampaignById(Statements.Profile.SELECT_SPAMMERS, createBuilder(0, offset, limit).add("dateFrom", date).build());
//    }
//
//    @Override
//    public List<Profile> getShopProductsProfiles(long tagId, int limit) {
//        return selectCampaignById(Statements.Profile.SELECT_SHOP_PRODUCTS_PROFILES, getParameters(
//                "tagId", tagId,
//                LIMIT, limit
//        ));
//    }

package ru.planeta.dao.concertdb


class HallDAOImpl /*: BaseDAO<*>(), HallDAO {
    override fun insert(hall: Hall): Int {
        return insert(Statements.Hall.INSERT, hall)
    }

    override fun update(hall: Hall): Int {
        return update(Statements.Hall.UPDATE, hall)
    }

    override fun selectCampaignById(hallId: Long): Hall {
        return selectOne(Statements.Hall.SELECT_ONE, getParameters("hallId", hallId)) as Hall
    }

    override fun selectByTitle(title: String): Hall {
        return selectOne(Statements.Hall.SELECT_ONE_BY_NAME, getParameters("title", title)) as Hall
    }
}*/

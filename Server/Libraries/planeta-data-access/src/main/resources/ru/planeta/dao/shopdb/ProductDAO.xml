<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE mapper
        PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN"
        "http://mybatis.org/dtd/mybatis-3-mapper.dtd">
<mapper namespace="ru.planeta.dao.shopdb.ProductDAO">

    <resultMap id="ProductResultMapWithoutTags" type="ru.planeta.model.shop.Product">
        <result property="productId" column="product_id"/>
        <result property="parentProductId" column="parent_product_id"/>
        <result property="profileId" column="store_id"/>
        <result property="referrerId" column="referrer_id"/>
        <result property="showOnCampaign" column="show_on_campaign"/>
        <result property="campaignIds" column="campaign_ids" jdbcType="ARRAY"
                typeHandler="ru.planeta.dao.mappers.ArrayTypeHandler"/>
        <result property="donateId" column="donate_id"/>

        <result property="price" column="price"/>
        <result property="oldPrice" column="old_price"/>
        <result property="currency" column="currency"/>

        <result property="totalQuantity" column="total_quantity"/>
        <result property="totalOnHoldQuantity" column="total_on_hold_quantity"/>
        <result property="totalPurchaseCount" column="total_purchase_count"/>
        <result property="purchaseLimit" column="purchase_limit"/>

        <result property="productState" column="product_state" javaType="ru.planeta.model.shop.enums.ProductState"
                typeHandler="ru.planeta.dao.mappers.enums.shop.ProductStateHandler"/>
        <result property="productStatus" column="product_status" javaType="ru.planeta.model.shop.enums.ProductStatus"
                typeHandler="ru.planeta.dao.mappers.enums.shop.ProductStatusHandler"/>
        <result property="productCategory" column="product_category" javaType="ru.planeta.model.shop.enums.ProductCategory"
                typeHandler="ru.planeta.dao.mappers.enums.shop.ProductCategoryHandler"/>
        <result property="imageUrls" column="image_urls" javaType="ArrayList" jdbcType="ARRAY"
                typeHandler="ru.planeta.dao.mappers.ArrayTypeHandler"/>
        <result property="coverImageUrl" column="cover_image_url"/>
        <result property="tableImageUrl" column="table_image_url"/>
        <result property="name" column="name"/>
        <result property="nameHtml" column="name_html"/>
        <result property="description" column="description"/>
        <result property="descriptionHtml" column="description_html"/>
        <result property="textForMail" column="text_for_mail"/>

        <result property="timeAdded" column="time_added"/>
        <result property="timeUpdated" column="time_updated"/>

        <result property="startSaleDate" column="start_sale_date"/>
        <result property="timeStarted" column="time_started"/>

        <result property="contentUrls" column="content_urls" javaType="ArrayList" jdbcType="ARRAY"
                typeHandler="ru.planeta.dao.mappers.ArrayTypeHandler"/>

        <result property="freeDelivery" column="free_delivery"/>
        <result property="cashAvailable" column="cash_available"/>

        <association property="tags" javaType="ru.planeta.model.shop.Category">
            <result property="categoryId" column="category_id"/>
            <result property="value" column="value"/>
            <result property="mnemonicName" column="mnemonic_name"/>
            <result property="tagTypeId" column="tag_type"/>
        </association>
        <association property="productAttribute" javaType="ru.planeta.model.shop.ProductAttribute">
            <result property="attributeId" column="product_attribute_id"/>
            <result property="attributeTypeId" column="product_attribute_type"/>
            <result property="value" column="product_attribute_value"/>
            <result property="index" column="product_attribute_index"/>
        </association>
    </resultMap>

    <resultMap id="ProductInfoResultMap" extends="ProductResultMap" type="ru.planeta.model.shop.ProductInfo"/>

    <resultMap id="ProductResultMap" type="ru.planeta.model.shop.Product" extends="ProductResultMapWithoutTags">
        <collection property="tags"
                    column="product_id"
                    select="ru.planeta.dao.shopdb.ProductTagDAO.selectTagsByProduct"/>
    </resultMap>

    <!--used in updates often-->
    <sql id="withParentOrJustProduct">
        <choose>
            <when test="parentProductId!=null">
                ((parent_product_id=#{parentProductId})
                OR (product_id=#{parentProductId}));
            </when>
            <when test="productId!=null">
                product_id=#{productId};
            </when>
        </choose>
    </sql>

    <insert id="insertProduct" parameterType="ru.planeta.model.shop.Product">
        <selectKey keyProperty="productId" resultType="long" order="BEFORE">
            <choose>
                <when test="productId > 0">
                    SELECT #{productId};
                </when>
                <otherwise>
                    SELECT nextval('shopdb.seq_product_id');
                </otherwise>
            </choose>
        </selectKey>
        INSERT INTO shopdb.products (
        product_id,
        merchant_profile_id,
        parent_product_id,
        store_id,
        referrer_id,
        donate_id,
        price,
        currency,
        total_purchase_count,
        total_on_hold_quantity, -- check usage
        total_quantity,
        purchase_limit,
        product_state,
        product_status,
        product_category,
        image_urls,
        cover_image_url,
        table_image_url,
        name,
        name_html,
        description,
        description_html,
        text_for_mail,
        product_attribute_id,
        product_attribute_type,
        product_attribute_value,
        product_attribute_index,
        old_price,
        content_urls,
        campaign_ids,
        free_delivery,
        cash_available,
        show_on_campaign,
        time_started
        <if test="startSaleDate != null">
            ,start_sale_date
        </if>
        ) VALUES (
        #{productId},
        #{profileId},
        #{parentProductId},
        #{profileId},
        #{referrerId},
        #{donateId},
        #{price},
        #{currency},
        COALESCE((select x.total_purchase_count from shopdb.products x where x.product_id = #{productId} offset 0 limit 1),0),
        COALESCE((select x.total_on_hold_quantity from shopdb.products x where x.product_id = #{productId} offset 0 limit 1),0),
        #{totalQuantity},
        #{purchaseLimit},
        #{productState.code},
        2, -- productStatusCode PAUSE
        #{productCategory.code},
        <if test="imageUrls != null">
            #{imageUrls, typeHandler=ru.planeta.dao.mappers.ArrayTypeHandler},
        </if>
        <if test="imageUrls == null">
            null,
        </if>
        #{coverImageUrl},
        #{tableImageUrl},
        #{name},
        #{nameHtml},
        #{description},
        #{descriptionHtml},
        #{textForMail},
        #{productAttribute.attributeId},
        #{productAttribute.attributeTypeId},
        #{productAttribute.value},
        #{productAttribute.index},
        #{oldPrice},
        <if test="contentUrls != null">
            #{contentUrls, typeHandler=ru.planeta.dao.mappers.ArrayTypeHandler},
        </if>
        <if test="contentUrls == null">
            null,
        </if>
        <if test="campaignIds != null">
            #{campaignIds, typeHandler=ru.planeta.dao.mappers.ArrayTypeHandler},
        </if>
        <if test="campaignIds == null">
            null,
        </if>
        #{freeDelivery},
        #{cashAvailable},
        #{showOnCampaign},
        #{timeStarted}
        <if test="startSaleDate != null">
            ,#{startSaleDate}
        </if>
        );

        INSERT INTO shopdb.product_tag_relations (
        tag_id,
        product_id,
        order_num
        )
        SELECT с.category_id, #{productId} as product_id, tags.order_num
        FROM shopdb.product_tags с
        INNER JOIN
        <foreach item="tag" index="index" collection="tags" open="(" separator=" union " close=")">
            (SELECT #{tag.categoryId} AS categoryId, ${index} AS order_num)
        </foreach>
        AS tags ON tags.categoryId = с.category_id;
    </insert>

    <update id="updateCurrentToOld" parameterType="map">
        UPDATE shopdb.products p
           SET product_status = #{b},
               total_quantity = 0,
               time_updated = NOW()
         WHERE p.product_status in (1,2)
           AND <include refid="withParentOrJustProduct"/>
    </update>

    <update id="updateProductDetails" parameterType="ru.planeta.model.shop.Product">
        UPDATE shopdb.products p
        SET
        merchant_profile_id = #{profileId},
        parent_product_id = #{parentProductId},
        store_id = #{profileId},
        referrer_id = #{referrerId},
        donate_id = #{donateId},
        price = #{price},
        old_price = #{oldPrice},
        currency = #{currency},
        total_quantity = #{totalQuantity},
        purchase_limit = #{purchaseLimit},
        start_sale_date = #{startSaleDate},
        <choose>
            <when test="imageUrls != null">
                image_urls = #{imageUrls, typeHandler=ru.planeta.dao.mappers.ArrayTypeHandler},
            </when>
            <otherwise>
                image_urls = null,
            </otherwise>
        </choose>
        cover_image_url = #{coverImageUrl},
        table_image_url = #{tableImageUrl},
        name = #{name},
        name_html = #{nameHtml},
        description = #{description},
        description_html = #{descriptionHtml},
        text_for_mail = #{textForMail},
        product_state = #{productState.code},
        product_category = #{productCategory.code},
        product_attribute_id = #{productAttribute.attributeId},
        product_attribute_type = #{productAttribute.attributeTypeId},
        product_attribute_value = #{productAttribute.value},
        product_attribute_index = #{productAttribute.index},
        <choose>
            <when test="contentUrls != null">
                content_urls = #{contentUrls, typeHandler=ru.planeta.dao.mappers.ArrayTypeHandler},
            </when>
            <otherwise>
                content_urls = null,
            </otherwise>
        </choose>
        <choose>
            <when test="campaignIds != null">
                campaign_ids = #{campaignIds, typeHandler=ru.planeta.dao.mappers.ArrayTypeHandler},
            </when>
            <otherwise>
                campaign_ids = null,
            </otherwise>
        </choose>
        free_delivery = #{freeDelivery},
        cash_available = #{cashAvailable},
        show_on_campaign = #{showOnCampaign},
        time_started = #{timeStarted},
        time_updated = NOW()
        WHERE product_id = #{productId};
    </update>

    <update id="updateProductNameAndDescriptionHtml" parameterType="map">
        UPDATE shopdb.products p
        SET name_html = #{nameHtml},
            description_html = #{descriptionHtml},
            time_updated = NOW()
        WHERE p.product_id = #{productId};
    </update>

    <update id="updateStatusAtoB" parameterType="map">
        UPDATE shopdb.products p
           SET product_status = #{b},
               time_updated = NOW()
         WHERE p.product_status = #{a} --current
           AND <include refid="withParentOrJustProduct"/>
    </update>

    <update id="updateStatusToStartAndSetStartDate" parameterType="map">
        UPDATE shopdb.products p
        SET product_status = 1,
            time_started = #{timeStarted},
            time_updated = NOW()
        WHERE true
        AND <include refid="withParentOrJustProduct"/>
    </update>

    <update id="updateTotalPurchaseCount" parameterType="map">
        UPDATE shopdb.products p
           SET total_purchase_count = p.total_purchase_count + #{quantity}
           <if test="quantity>0">,
               time_last_purchased = now()
           </if>
         FROM shopdb.products c
         WHERE p.product_id = #{productId}
            OR c.product_id = #{productId}
           AND c.parent_product_id = p.product_id
           AND p.product_state = 1;

    </update>

    <update id="updateTotalQuantityDelta" parameterType="map">
        UPDATE shopdb.products
           SET total_quantity = total_quantity + #{quantity}
         WHERE product_id = #{productId} AND product_category = 2;
    </update>

    <update id="updateTotalQuantity" parameterType="map">
        UPDATE shopdb.products
        SET total_quantity = #{quantity}
        WHERE product_id = #{productId} AND product_category = 2;
    </update>

    <update id="makeChildsFree" parameterType="map">
        UPDATE shopdb.products
           SET parent_product_id = 0
         WHERE product_state in (1,3)
           AND parent_product_id = #{parentProductId};
    </update>

    <select id="selectReadyForPurchaseTopProducts" resultMap="ProductInfoResultMap" parameterType="Map">
        SELECT child.*,
        cat.*
        FROM shopdb.products child
        LEFT JOIN shopdb.product_tags cat
        ON child.type = cat.category_id
        WHERE child.product_status = 1 /* active */
        AND child.product_state in (1, 3) /* META or SOLID_PRODUCT */
        AND child.total_quantity &gt; child.total_on_hold_quantity
        <if test="searchString != null">
            AND child.name ILIKE '%' || #{searchString} || '%'
        </if>
        <if test="categoryTypeId > 0">
            AND child.type = #{categoryTypeId}
        </if>
        <choose>
            <when test="sortCriteria.code == 0">
                ORDER BY time_last_purchased DESC NULLS LAST
            </when>
            <when test="sortCriteria.code == 1">
                ORDER BY total_purchase_count DESC
            </when>
            <when test="sortCriteria.code == 2">
                ORDER BY time_added DESC
            </when>
        </choose>
        <include refid="ru.planeta.dao.CommonDAO.offsetLimit"/>
    </select>

    <sql id="selectReadyForPurchaseTopProductsWithPriceRange_Where">
        WHERE child.product_status = 1 /* active */
        AND ( child.product_state in (1, 3)/* META or SOLID_PRODUCT */)
        AND child.product_category != 3
        AND child.total_quantity &gt; 0
        AND (child.show_on_campaign or (#{campaignId} = ANY(child.campaign_ids)))
        <if test="searchString != null">
            AND child.name ILIKE '%' || #{searchString} || '%'
        </if>
        <if test="categoryTypeId > 0">
            AND child.type = #{categoryTypeId}
        </if>
        <if test="priceFrom > 0">
            AND child.price &gt;= #{priceFrom}
        </if>
        <if test="priceTo > 0">
            AND child.price &lt;= #{priceTo}
        </if>
        <if test="referrerId">
            AND child.referrer_id = #{referrerId}
        </if>
        <if test="sortCriteria">
            <choose>
                <!--            <when test="sortCriteria.code == 0">
                                ORDER BY time_last_purchased DESC NULLS LAST
                            </when>-->
                <when test="sortCriteria.code == 1">
                    ORDER BY total_purchase_count DESC
                </when>
                <when test="sortCriteria.code == 2">
                    ORDER BY time_added DESC
                </when>
            </choose>
        </if>
    </sql>
    <select id="selectReadyForPurchaseTopProductsWithPriceRange" resultMap="ProductInfoResultMap" parameterType="Map">
        SELECT child.*,
        cat.*
        FROM shopdb.products child
        LEFT JOIN shopdb.product_tags cat
        ON child.type = cat.category_id
        <include refid="selectReadyForPurchaseTopProductsWithPriceRange_Where"/>
        <include refid="ru.planeta.dao.CommonDAO.offsetLimit"/>
    </select>

    <select id="selectSortedParentProductsId" resultType="java.lang.Long" parameterType="Map">
        select ids.product_id from (
            select
                distinct p.product_id,
                coalesce((
                    select true
                    from shopdb.products p1
                    join shopdb.product_tag_relations r1
                    on r1.product_id = p1.product_id
                    where r1.tag_id in (7,8)
                      and p1.product_id  = p.product_id limit 1
                )::bool, false) as ontop
                <choose>
	                <when test="sortCriteriaCode == 0">
	                    , p.time_last_purchased
	                </when>
	                <when test="sortCriteriaCode == 1">
	                    , p.total_purchase_count
	                </when>
	                <when test="sortCriteriaCode == 2">
	                    , p.time_updated
	                </when>
	            </choose>
            from shopdb.products p
            join shopdb.product_tag_relations r
            on r.product_id = p.product_id
            where
                p.parent_product_id = 0
                and p.total_quantity &gt; 0
                and p.product_status = 1
            <if test="categoryId &gt; 0">
                and r.tag_id = #{categoryId}
            </if>

            <if test="searchString != null">
                AND p.name ILIKE '%' || #{searchString} || '%'
            </if>

            order by ontop desc
            <choose>
	            <when test="sortCriteriaCode == 0">
	                , p.time_last_purchased DESC NULLS LAST
	            </when>
	            <when test="sortCriteriaCode == 1">
	                , p.total_purchase_count DESC
	            </when>
	            <when test="sortCriteriaCode == 2">
	                , p.time_updated DESC
	            </when>
	        </choose>
            <if test="limit &gt; 0">
                OFFSET #{offset}
                LIMIT #{limit}
            </if>
        ) as ids
    </select>

    <select id="selectProductsByIds" resultMap="ProductInfoResultMap" parameterType="Map">
        select *
        from shopdb.products p
        where p.product_id in <foreach item="id" index="index" collection="ids" open="(" separator="," close=")">#{id}</foreach>
        UNION
        SELECT * from shopdb.products c
        where c.parent_product_id in <foreach item="id" index="index" collection="ids" open="(" separator="," close=")">#{id}</foreach>
    </select>

    <select id="selectDonateByReferrerId" resultType="long" parameterType="Map">
        select product_id
        from shopdb.products
        where referrer_id = #{referrerId}
        and product_category = 3
        and product_status = 1
    </select>

    <select id="selectReadyForPurchaseTopProductsWithPriceRangeCount" resultType="Integer" parameterType="Map">
        SELECT count(*)
        FROM shopdb.products child
        <include refid="selectReadyForPurchaseTopProductsWithPriceRange_Where"/>
    </select>

    <select id="selectProducts" resultMap="ProductInfoResultMap" parameterType="Map">
        SELECT
        p.*,
        cat.*
        FROM shopdb.products p
        LEFT JOIN shopdb.product_tags cat
        ON p.type = cat.category_id
        WHERE true
        <if test="productId!=null">
           AND p.product_id = #{productId}
        </if>
        <if test="statusCode!=null">
           AND p.product_status = #{statusCode}
        </if>
        <if test="parentProductId!=null">
           AND p.parent_product_id = #{parentProductId}
        </if>
        <if test="searchString != null">
           AND p.name ILIKE '%' || #{searchString} || '%'
        </if>
        <if test="onlyAvailable">
           AND (p.product_category = 1 OR (p.product_category = 2 AND p.total_quantity > 1))
        </if>
        ORDER BY p.time_updated DESC
        <include refid="ru.planeta.dao.CommonDAO.offsetLimit"/>
    </select>



    <select id="selectSimilarProducts" resultMap="ProductResultMap" parameterType="Map">
       SELECT p.*
         FROM shopdb.products p
        WHERE p.product_id != #{productId}
          AND p.product_status = #{statusCode}
          AND (p.product_category = 1 OR (p.product_category = 2 AND p.total_quantity > 1))
          AND p.parent_product_id = 0
     ORDER BY random()
        <include refid="ru.planeta.dao.CommonDAO.offsetLimit"/>
    </select>

    <sql id="selectProductsCurrentWhere">
        WHERE p.product_status in (1,2)
        <if test="productId!=null">
            AND p.product_id = #{productId}
        </if>
        <if test="(categoryTypeId != null) &amp;&amp; (categoryTypeId > 0)">
            AND p.type = #{categoryTypeId}
        </if>
        <if test="parentProductId!=null">
            AND p.parent_product_id = #{parentProductId}
        </if>
        <if test="searchString != null">
            AND p.name ilike '%'||#{searchString}||'%'
        </if>
        <if test="referrerId">
            AND p.referrer_id = #{referrerId}
        </if>

    </sql>
    <select id="selectProductsCurrent" resultMap="ProductResultMap" parameterType="Map">
        SELECT
               p.*
          FROM shopdb.products p
        <include refid="selectProductsCurrentWhere"/>
      ORDER BY p.name ASC
        <include refid="ru.planeta.dao.CommonDAO.offsetLimit"/>
    </select>

    <select id="selectProductsForAdminSearch" resultMap="ProductResultMap" parameterType="Map">
        SELECT DISTINCT
        p.*
        FROM shopdb.products p
        join shopdb.product_tag_relations r
        on r.product_id = p.product_id
        WHERE
          p.parent_product_id = #{parentProductId}
        <if test="(tagId != null) &amp;&amp; (tagId > 0)">
            AND r.tag_id = #{tagId}
        </if>
        <if test="searchString != null">
            AND p.name ilike '%'||#{searchString}||'%'
        </if>

        <if test="(status != null) &amp;&amp; (status != '')">
            <if test="status == 'PAUSED'">
                AND p.product_status = 2
            </if>
            <if test="status == 'ENDED'">
                AND p.total_quantity = 0
            </if>
            <if test="status == 'OTHER'">
                AND p.total_quantity > 0
                AND p.product_status = 1
            </if>
        </if>

        <if test="!status || status == ''">
            AND p.product_status in (1,2)
        </if>
        ORDER BY p.name ASC
        <include refid="ru.planeta.dao.CommonDAO.offsetLimit"/>
    </select>

    <select id="selectProductsCurrentCount" resultType="java.lang.Long" parameterType="Map">
        SELECT count(*)
        FROM shopdb.products p
        <include refid="selectProductsCurrentWhere"/>
        ;
    </select>

    <select id="selectProductsByIdList" resultMap="ProductInfoResultMap" parameterType="Map">
        SELECT
        p.*,
        cat.*
        FROM shopdb.products p
        LEFT JOIN shopdb.product_tags cat
        ON p.type = cat.category_id
        WHERE p.product_id IN
        <foreach item="item" index="index" collection="productIds" open="(" separator="," close=")">#{item}</foreach>
        <if test="statusCode!=null">
            AND p.product_status = #{statusCode}
        </if>
        ;
    </select>

    <update id="bindToProfile" parameterType="Map">
        UPDATE shopdb.products p
        SET referrer_id = #{referrerId},
            show_on_campaign = #{showOnCampaign},
            <choose>
                <when test="campaignIds != null">
                    campaign_ids = #{campaignIds, typeHandler=ru.planeta.dao.mappers.ArrayTypeHandler}
                </when>
                <otherwise>
                    campaign_ids = null
                </otherwise>
            </choose>
        WHERE product_id = #{productId};
    </update>
</mapper>

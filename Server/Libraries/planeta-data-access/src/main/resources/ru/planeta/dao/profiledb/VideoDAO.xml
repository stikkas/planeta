<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE mapper
PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN"
"http://mybatis.org/dtd/mybatis-3-mapper.dtd">
<mapper namespace="ru.planeta.dao.profiledb.VideoDAO">
	<resultMap id="videoResultMap" type="ru.planeta.model.profile.media.Video">
		<result property="videoId" column="video_id" />
		<result property="profileId" column="owner_profile_id" />
		<result property="authorProfileId" column="author_profile_id" />
		<result property="name" column="name" />
		<result property="description" column="description" />
		<result property="imageUrl" column="image_url" />
		<result property="imageId" column="image_id" />
		<result property="videoUrl" column="video_url" />
		<result property="storyboardUrl" column="storyboard_url" />
		<result property="availableQualityModes" column="available_quality_modes" />
		<result property="statusCode" column="status" />
		<result property="timeAdded" column="time_added" />
		<result property="timeUpdated" column="time_updated" />
		<result property="viewsCount" column="views_count" />
		<result property="downloadsCount" column="downloads_count" />
		<result property="authorName" column="author_name" />
		<result property="authorImageUrl" column="author_image_url" />
        <result property="authorAlias" column="author_alias" />
        <result property="authorGenderCode" column="author_gender" />
        <result property="ownerAlias" column="owner_alias" />
		<result property="ownerName" column="owner_name" />
		<result property="ownerImageUrl" column="owner_image_url" />
		<result property="ownerProfileTypeCode" column="owner_profile_type_id" />
		<result property="commentsCount" column="comments_count" />
		<result property="commentsPermissionCode" column="comments_permission" />
		<result property="duration" column="duration" />
        <result property="videoTypeCode" column="video_type" />
        <result property="viewPermissionCode" column="view_permission" />
        <result property="cachedVideoId" column="cached_video_id" />
	</resultMap>

	<select id="selectVideoById" parameterType="map" resultMap="videoResultMap">
        SELECT t1.*,
		<include refid="ru.planeta.dao.CommonDAO.ownerProfileFields" />
          FROM profiledb.videos t1
		<include refid="ru.planeta.dao.CommonDAO.ownerProfileJoinsCommon" />
		   AND <include refid="ru.planeta.dao.CommonDAO.ownerProfileJoinsCommentObjectId" /> = t1.video_id
         WHERE t1.owner_profile_id = #{profileId}
		   AND t1.video_id = #{objectId};
	</select>

    <select id="selectVideosCount" parameterType="map" resultType="Integer">
        SELECT COUNT(*)
          FROM profiledb.videos t1
         WHERE t1.owner_profile_id = #{profileId};
	</select>

	<select id="selectVideo" parameterType="map" resultMap="videoResultMap">
	    SELECT t1.*,
		<include refid="ru.planeta.dao.CommonDAO.ownerProfileFields" />
          FROM profiledb.videos t1
		<include refid="ru.planeta.dao.CommonDAO.videoTagFilter" />
		<include refid="ru.planeta.dao.CommonDAO.ownerProfileJoinsCommon" />
		   AND <include refid="ru.planeta.dao.CommonDAO.ownerProfileJoinsCommentObjectId" /> = t1.video_id
         WHERE t1.owner_profile_id = #{profileId}
           AND t1.view_permission &lt;= #{permissionLevel}
           AND t1.video_type != 5
         <if test="searchString != null and searchString != ''">
            AND (lower(t1.name) LIKE '%' || lower(#{searchString}) || '%')
        </if>
         ORDER BY t1.time_added DESC NULLS LAST
		<include refid="ru.planeta.dao.CommonDAO.offsetLimit" />
	</select>

    <select id="selectVideosForNotificationByTimeAddedWithPermissions" parameterType="map" resultMap="videoResultMap">
        SELECT t1.*,
        <include refid="ru.planeta.dao.CommonDAO.ownerProfileFields" />
        FROM profiledb.videos t1
        <include refid="ru.planeta.dao.CommonDAO.ownerProfileJoinsCommon" />
        AND <include refid="ru.planeta.dao.CommonDAO.ownerProfileJoinsCommentObjectId" /> = t1.video_id
        WHERE t1.owner_profile_id = #{profileId}
        AND t1.author_profile_id = #{profileId}
        AND t1.view_permission &lt;= #{permissionLevel}
        AND t1.time_added &gt; #{dateFrom}
        AND t1.video_type != 5
        ORDER BY
        t1.time_added DESC
        <include refid="ru.planeta.dao.CommonDAO.offsetLimit" />
    </select>

    <select id="selectAttachedVideos" parameterType="map" resultMap="videoResultMap">
        SELECT t1.*,
        <include refid="ru.planeta.dao.CommonDAO.ownerProfileFields" />
          FROM profiledb.videos t1
        <include refid="ru.planeta.dao.CommonDAO.videoTagFilter" />
        <include refid="ru.planeta.dao.CommonDAO.ownerProfileJoinsCommon" />
            AND <include refid="ru.planeta.dao.CommonDAO.ownerProfileJoinsCommentObjectId" /> = t1.video_id
         WHERE t1.owner_profile_id = #{profileId}
           AND t1.video_type = 5
         ORDER BY t1.time_added DESC NULLS LAST
        <include refid="ru.planeta.dao.CommonDAO.offsetLimit" />
    </select>

    <select id="selectVideoByCachedVideoId" parameterType="map" resultMap="videoResultMap">
        SELECT t1.*,
        <include refid="ru.planeta.dao.CommonDAO.ownerProfileFields" />
        FROM profiledb.videos t1
        <include refid="ru.planeta.dao.CommonDAO.videoTagFilter" />
        <include refid="ru.planeta.dao.CommonDAO.ownerProfileJoinsCommon" />
        AND <include refid="ru.planeta.dao.CommonDAO.ownerProfileJoinsCommentObjectId" /> = t1.video_id
        WHERE t1.owner_profile_id = #{profileId}
        AND t1.author_profile_id = #{authorId}
        AND t1.cached_video_id = #{cachedVideoId}
    </select>

    <select id="selectVideoTitlesByIdList" parameterType="map" resultMap="videoResultMap">
		SELECT t1.*,
         <include refid="ru.planeta.dao.CommonDAO.ownerProfileFields" />
		  FROM profiledb.videos t1
         <include refid="ru.planeta.dao.CommonDAO.ownerProfileJoinsCommon" />
           AND <include refid="ru.planeta.dao.CommonDAO.ownerProfileJoinsCommentObjectId" /> = t1.video_id
		 WHERE t1.owner_profile_id = #{profileId}
        <if test="videos != null">
           AND t1.video_id IN
            <foreach item="item" index="index" collection="videos" open="(" separator="," close=")">#{item}</foreach>
        </if>
        <if test="videos == null">
            AND t1.video_id = 0
        </if>
         ;
    </select>

    <select id="selectTopVideosForGroup" parameterType="map" resultMap="videoResultMap">
		SELECT t1.*,
            <include refid="ru.planeta.dao.CommonDAO.ownerProfileFields" />
        FROM profiledb.videos t1
             <include refid="ru.planeta.dao.CommonDAO.ownerProfileJoinsCommon" />
         AND <include refid="ru.planeta.dao.CommonDAO.ownerProfileJoinsCommentObjectId" /> = t1.video_id
        WHERE t1.owner_profile_id = #{profileId}
          AND t1.view_permission = 1
        ORDER BY t1.views_count DESC
        <include refid="ru.planeta.dao.CommonDAO.offsetLimit" />;
    </select>

	<insert id="insertVideo" parameterType="ru.planeta.model.profile.media.Video">
        INSERT INTO profiledb.videos (
			video_id,
			owner_profile_id,
			author_profile_id,
			name,
			description,
			image_url,
			image_id,
			video_url,
			storyboard_url,
            available_quality_modes,
            status,
            time_added,
			time_updated,
			views_count,
            downloads_count,
            duration,
            video_type,
            view_permission,
            cached_video_id
		) VALUES (
			#{videoId},
			#{profileId},
			#{authorProfileId},
			#{name},
			#{description},
			#{imageUrl},
			#{imageId},
			#{videoUrl},
			#{storyboardUrl},
            #{availableQualityModes},
            #{statusCode},
            #{timeAdded},
			#{timeUpdated},
			#{viewsCount},
            #{downloadsCount},
            #{duration},
            #{videoTypeCode},
            #{viewPermissionCode},
            #{cachedVideoId}
		);
	</insert>

	<update id="updateVideo" parameterType="ru.planeta.model.profile.media.Video">
		UPDATE profiledb.videos
		   SET name = #{name},
               description = #{description},
			   video_url = #{videoUrl},
               available_quality_modes = #{availableQualityModes},
               image_id = #{imageId},
               image_url = #{imageUrl},
               storyboard_url = #{storyboardUrl},
               status = #{statusCode},
               time_updated = #{timeUpdated},
               views_count = #{viewsCount},
               downloads_count = #{downloadsCount},
               duration = #{duration},
               view_permission = #{viewPermissionCode},
               cached_video_id = #{cachedVideoId}
		 WHERE video_id = #{videoId}
		   AND owner_profile_id = #{profileId};
	</update>

	<update id="updateVideoViewsCount" parameterType="map">
        UPDATE profiledb.videos
           SET views_count = views_count + #{viewsCount}
         WHERE video_id = #{objectId}
            AND owner_profile_id = #{profileId};
	</update>

	<update id="updateVideoDownloadsCount" parameterType="map">
        UPDATE profiledb.videos
           SET downloads_count = downloads_count + #{downloadsCount}
         WHERE video_id = #{objectId}
           AND owner_profile_id = #{profileId};
	</update>

	<delete id="deleteVideo" parameterType="map">
		DELETE FROM profiledb.videos
		 WHERE owner_profile_id = #{profileId}
		   AND video_id = #{objectId};
	</delete>

</mapper>
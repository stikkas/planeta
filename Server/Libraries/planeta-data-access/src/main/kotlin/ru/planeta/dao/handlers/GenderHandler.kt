package ru.planeta.dao.handlers

import org.apache.ibatis.type.BaseTypeHandler
import org.apache.ibatis.type.JdbcType
import org.apache.ibatis.type.MappedTypes
import ru.planeta.commons.model.Gender
import java.sql.CallableStatement
import java.sql.PreparedStatement
import java.sql.ResultSet

@MappedTypes(Gender::class)
class GenderHandler : BaseTypeHandler<Gender>() {

    override fun getNullableResult(p0: ResultSet, p1: String): Gender {
        return Gender.getByValue(p0.getInt(p1))
    }

    override fun getNullableResult(p0: ResultSet, p1: Int): Gender {
        return Gender.getByValue(p0.getInt(p1))
    }

    override fun getNullableResult(p0: CallableStatement, p1: Int): Gender {
        return Gender.getByValue(p0.getInt(p1))
    }

    override fun setNonNullParameter(p0: PreparedStatement, p1: Int, p2: Gender, p3: JdbcType?) {
        if (p3 == null) {
            p0.setInt(p1, p2.code)
        } else {
            p0.setObject(p1, p2.code, p3.TYPE_CODE)
        }
    }
}

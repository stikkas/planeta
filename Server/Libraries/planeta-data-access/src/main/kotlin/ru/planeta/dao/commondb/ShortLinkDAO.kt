package ru.planeta.dao.commondb

import org.apache.ibatis.annotations.Mapper
import org.apache.ibatis.annotations.Param
import ru.planeta.model.common.ShortLink
import ru.planeta.model.enums.ShortLinkStatus
import java.util.EnumSet

/**
 * User: a.savanovich
 * Date: 28.09.16
 * Time: 13:49
 */
@Mapper
interface ShortLinkDAO {

    fun insert(shortLink: ShortLink)

    fun select(shortLinkId: Long): ShortLink?

    fun selectByAlias(shortLink: String): ShortLink

    fun selectListByProfileId(@Param("profileId") profileId: Long, @Param("offset") offset: Int, @Param("limit") limit: Int): List<ShortLink>

    fun selectExistsSocialShortLinksByBasename(@Param("shortlinkBasename") shortlinkBasename: String,
                                               @Param("socialShortlinkAdditionsList") socialShortlinkAdditionsList: List<String>,
                                               @Param("offset") offset: Int, @Param("limit") limit: Int): List<ShortLink>

    fun selectList(@Param("params") params: EnumSet<ShortLinkStatus>, @Param("offset") offset: Int, @Param("limit") limit: Int): List<ShortLink>

    fun delete(id: Long)

    fun update(link: ShortLink)
}


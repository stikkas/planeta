package ru.planeta.dao.commondb

import org.apache.ibatis.annotations.Mapper
import org.apache.ibatis.annotations.Param
import ru.planeta.model.common.BillsFor1c
import ru.planeta.model.enums.TopayTransactionStatus

/**
 *
 * User: Serge Blagodatskih<stikkas17></stikkas17>@gmail.com><br></br>
 * Date: 01.02.16<br></br>
 * Time: 14:19
 */
@Mapper
interface BillsFor1cDAO {

    fun insert(bill: BillsFor1c)

    fun update(bill: BillsFor1c)

    fun delete(id: Long)

    fun select(id: Long): BillsFor1c?

    fun selectList(@Param("offset") offset: Int, @Param("limit") limit: Int): List<BillsFor1c>

    fun selectListByStatus(@Param("status") status: TopayTransactionStatus, @Param("offset") offset: Int, @Param("limit") limit: Int): List<BillsFor1c>

}

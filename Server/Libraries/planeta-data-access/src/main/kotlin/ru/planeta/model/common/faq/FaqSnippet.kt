package ru.planeta.model.common.faq

import ru.planeta.model.commondb.FaqArticle
import ru.planeta.model.commondb.FaqCategory
import ru.planeta.model.commondb.FaqParagraph

/**
 * @author s.kalmykov
 * @since 27.08.2014
 */
class FaqSnippet : FaqParagraph() {
    var article: FaqArticle? = null
    var category: FaqCategory? = null
    var highlightedAnnotationHtml: String? = null
    var highlightedArticleTitleHtml: String? = null
    var highlightedCategoryTitleHtml: String? = null
    var highlightedParagraphTitleHtml: String? = null
}

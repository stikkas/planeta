package ru.planeta.dao.commondb

import org.apache.ibatis.annotations.Param
import java.util.Date
import org.springframework.beans.factory.annotation.Autowired
import ru.planeta.model.enums.PaymentErrorType
import ru.planeta.model.filters.PaymentErrorsFilter
import ru.planeta.model.commondb.PaymentErrors
import ru.planeta.model.commondb.PaymentErrorsEx
import ru.planeta.model.commondb.PaymentErrorsExample
import ru.planeta.model.commondb.PaymentErrorsWithComments

abstract class BasePaymentErrorsService {
    @Autowired
    protected var paymentErrorsMapper: PaymentErrorsMapper? = null

    val orderByClause: String
        get() = "payment_error_id"

    //**************************************************************************************************
    //*********************************  Create ** Update ** Delete  *************************************
    //**************************************************************************************************
    fun insertPaymentErrors(paymentErrors: PaymentErrors) {
        paymentErrors.timeAdded = Date()
        paymentErrors.timeUpdated = Date()
        paymentErrorsMapper!!.insertSelective(paymentErrors)
    }

    fun insertPaymentErrorsAllFields(paymentErrors: PaymentErrors) {
        paymentErrors.timeAdded = Date()
        paymentErrors.timeUpdated = Date()
        paymentErrorsMapper!!.insert(paymentErrors)
    }

    fun updatePaymentErrors(paymentErrors: PaymentErrors) {
        paymentErrors.timeUpdated = Date()
        paymentErrorsMapper!!.updateByPrimaryKeySelective(paymentErrors)
    }

    fun updatePaymentErrorsAllFields(paymentErrors: PaymentErrors) {
        paymentErrors.timeUpdated = Date()
        paymentErrorsMapper!!.updateByPrimaryKey(paymentErrors)
    }

    fun insertOrUpdatePaymentErrors(paymentErrors: PaymentErrors) {
        paymentErrors.timeUpdated = Date()
        val selectedPaymentErrors = paymentErrorsMapper!!.selectByPrimaryKey(paymentErrors.paymentErrorId)
        if (selectedPaymentErrors == null) {
            paymentErrors.timeAdded = Date()
            paymentErrorsMapper!!.insertSelective(paymentErrors)
        } else {
            paymentErrorsMapper!!.updateByPrimaryKeySelective(paymentErrors)
        }
    }

    fun insertOrUpdatePaymentErrorsAllFields(paymentErrors: PaymentErrors) {
        paymentErrors.timeUpdated = Date()
        val selectedPaymentErrors = paymentErrorsMapper!!.selectByPrimaryKey(paymentErrors.paymentErrorId)
        if (selectedPaymentErrors == null) {
            paymentErrors.timeAdded = Date()
            paymentErrorsMapper!!.insert(paymentErrors)
        } else {
            paymentErrorsMapper!!.updateByPrimaryKey(paymentErrors)
        }
    }

    fun deletePaymentErrors(id: Long?) {
        paymentErrorsMapper!!.deleteByPrimaryKey(id)
    }

    //**************************************************************************************************
    //**************************************** payment_errors ********************************************
    //************************************** SelectByPrimaryKey ******************************************
    //**************************************************************************************************
    fun selectPaymentErrors(paymentErrorId: Long?): PaymentErrors {
        return paymentErrorsMapper!!.selectByPrimaryKey(paymentErrorId)
    }

    //**************************************************************************************************
    //**************************************** payment_errors ********************************************
    //**************************************************************************************************
    private fun COMMENT_SECTION_PaymentErrors() {

    }

    fun getPaymentErrorsExample(@Param("offset") offset: Int, @Param("limit") limit: Int): PaymentErrorsExample {
        val example = PaymentErrorsExample()
        example.offset = offset
        example.limit = limit
        example.or()
        example.orderByClause = orderByClause
        return example
    }

    fun selectPaymentErrorsCount(): Int {
        return paymentErrorsMapper!!.countByExample(getPaymentErrorsExample(0, 0))
    }

    fun selectPaymentErrorsList(@Param("offset") offset: Int, @Param("limit") limit: Int): List<PaymentErrors> {
        return paymentErrorsMapper!!.selectByExample(getPaymentErrorsExample(offset, limit))
    }

    //**************************************************************************************************
    //**************************************** payment_errors ********************************************
    //********************* Long transactionId,PaymentErrorType paymentErrorType *************************
    //**************************************************************************************************
    private fun COMMENT_SECTION_PaymentErrors(@Param("transactionId") transactionId: Long?, @Param("paymentErrorType") paymentErrorType: PaymentErrorType) {

    }

    fun getPaymentErrorsExample(@Param("transactionId") transactionId: Long?, @Param("paymentErrorType") paymentErrorType: PaymentErrorType, @Param("offset") offset: Int, @Param("limit") limit: Int): PaymentErrorsExample {
        val example = PaymentErrorsExample()
        example.offset = offset
        example.limit = limit
        example.or()
                .andTransactionIdEqualTo(transactionId)
                .andPaymentErrorTypeEqualTo(paymentErrorType)
        example.orderByClause = orderByClause
        return example
    }

    fun selectPaymentErrorsCount(@Param("transactionId") transactionId: Long?, @Param("paymentErrorType") paymentErrorType: PaymentErrorType): Int {
        return paymentErrorsMapper!!.countByExample(getPaymentErrorsExample(transactionId, paymentErrorType, 0, 0))
    }

    fun selectPaymentErrorsList(@Param("transactionId") transactionId: Long?, @Param("paymentErrorType") paymentErrorType: PaymentErrorType, @Param("offset") offset: Int, @Param("limit") limit: Int): List<PaymentErrors> {
        return paymentErrorsMapper!!.selectByExample(getPaymentErrorsExample(transactionId, paymentErrorType, offset, limit))
    }

    fun selectPaymentErrors(@Param("transactionId") transactionId: Long?, @Param("paymentErrorType") paymentErrorType: PaymentErrorType): PaymentErrors? {
        val list = paymentErrorsMapper!!.selectByExample(getPaymentErrorsExample(transactionId, paymentErrorType, 0, 0))
        return if (list.size > 0) list[0] else null
    }

    fun selectPaymentErrorsListByFilter(paymentErrorsFilter: PaymentErrorsFilter): List<PaymentErrors> {
        return paymentErrorsMapper!!.selectPaymentErrorsListByPaymentErrorsFilter(paymentErrorsFilter)
    }

    fun selectPaymentErrorsCountByFilter(paymentErrorsFilter: PaymentErrorsFilter): Int {
        return paymentErrorsMapper!!.selectPaymentErrorsCountByFilter(paymentErrorsFilter)
    }

    //**************************************************************************************************
    //**************************************** payment_errors ********************************************
    //************************************** SelectByPrimaryKey ******************************************
    //**************************************************************************************************
    fun selectPaymentErrorsWithComments(paymentErrorId: Long?): PaymentErrorsWithComments {
        return paymentErrorsMapper!!.selectPaymentErrorsWithCommentsByPrimaryKey(paymentErrorId)
    }

    //**************************************************************************************************
    //********************************* payment_errors_with_comments *************************************
    //************************************** Long paymentErrorId *****************************************
    //**************************************************************************************************
    private fun COMMENT_SECTION_PaymentErrorsWithComments(paymentErrorId: Long?) {

    }

    fun getPaymentErrorsExample(@Param("paymentErrorId") paymentErrorId: Long?, @Param("offset") offset: Int, @Param("limit") limit: Int): PaymentErrorsExample {
        val example = PaymentErrorsExample()
        example.offset = offset
        example.limit = limit
        example.or()
                .andPaymentErrorIdEqualTo(paymentErrorId)
        example.orderByClause = orderByClause
        return example
    }

    fun selectPaymentErrorsWithCommentsList(@Param("paymentErrorId") paymentErrorId: Long?, @Param("offset") offset: Int, @Param("limit") limit: Int): List<PaymentErrorsWithComments> {
        return paymentErrorsMapper!!.selectPaymentErrorsWithCommentsByExample(getPaymentErrorsExample(paymentErrorId, offset, limit))
    }

    fun selectPaymentErrorsWithCommentsListByFilter(paymentErrorsFilter: PaymentErrorsFilter): List<PaymentErrorsWithComments> {
        return paymentErrorsMapper!!.selectPaymentErrorsWithCommentsListByPaymentErrorsFilter(paymentErrorsFilter)
    }

    //**************************************************************************************************
    //**************************************** payment_errors ********************************************
    //************************************** SelectByPrimaryKey ******************************************
    //**************************************************************************************************
    fun selectPaymentErrorsEx(paymentErrorId: Long?): PaymentErrorsEx {
        return paymentErrorsMapper!!.selectPaymentErrorsExByPrimaryKey(paymentErrorId)
    }

    //**************************************************************************************************
    //*************************************** payment_errors_ex ******************************************
    //**************************************************************************************************
    private fun COMMENT_SECTION_PaymentErrorsEx() {

    }

    fun selectPaymentErrorsExList(@Param("offset") offset: Int, @Param("limit") limit: Int): List<PaymentErrorsEx> {
        return paymentErrorsMapper!!.selectPaymentErrorsExByExample(getPaymentErrorsExample(offset, limit))
    }

    fun selectPaymentErrorsExListByFilter(paymentErrorsFilter: PaymentErrorsFilter): List<PaymentErrorsEx> {
        return paymentErrorsMapper!!.selectPaymentErrorsExListByPaymentErrorsFilter(paymentErrorsFilter)
    }
}

package ru.planeta.model.common

/**
 * This class represents manager of objects(Group/Campaign)
 *
 *
 * User: Andrew Arefyev
 * Date: 18.02.13
 * Time: 21:26
 */
open class PlanetaManager {

    var managerId: Long = 0
    var profileId: Long = 0
    var fullName: String? = null
    var name: String? = null
    var email: String? = null
    var isActive: Boolean = false
}

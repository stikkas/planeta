package ru.planeta.model.trashcan

class QuizQuestion {
    var quizQuestionId: Long = 0
    var quizQuestionOrderNum: Long = 0
    var questionText: String? = null
    var questionDescription: String? = null
    var type = QuizQuestionType.TEXT_BIG
    var startValue: Int = 0
    var endValue: Int = 0
    var isHasCustomRadio: Boolean = false
    var customRadioText: String? = null
    var quizQuestionOptionList: List<QuizQuestionOption>? = null
    var isEnabled = true

    var typeCode: Int
        get() = if (type != null) type.code else 0
        set(code) {
            this.type = QuizQuestionType.getByValue(code)
        }
}

package ru.planeta.model.enums

import java.util.*
import java.util.EnumSet
import java.util.HashMap
import ru.planeta.model.enums.ReadableEnum

enum class PaymentErrorStatus (override val code: Int, override val descriptionMessageCode: String, override val fullDescriptionMessageCode: String) : ReadableEnum {
    NEW(1, "enum.payment.error.status.new.description", "enum.payment.error.status.new.full.description"),
    IN_PROGRESS(2, "enum.payment.error.status.in.progress.description", "enum.payment.error.status.in.progress.full.description"),
    RESOLVED(3, "enum.payment.error.status.resolved.description", "enum.payment.error.status.resolved.full.description");

    fun checkTransition(that: PaymentErrorStatus): Boolean {
        val list = transitions[this]
        return list != null && list.contains(that)
    }

    companion object {

        var transitions: MutableMap<PaymentErrorStatus, List<PaymentErrorStatus>> = HashMap()

        init {
            transitions[NEW] = object : ArrayList<PaymentErrorStatus>() {
                init {
                    add(IN_PROGRESS)
                    add(RESOLVED)
                }
            }
            transitions[IN_PROGRESS] = object : ArrayList<PaymentErrorStatus>() {
                init {
                    add(RESOLVED)
                }
            }
            transitions[RESOLVED] = object : ArrayList<PaymentErrorStatus>() {
                init {
                    add(IN_PROGRESS)
                }
            }

        }

        private val lookup = HashMap<Int, PaymentErrorStatus>()

        fun parse(s: String?): PaymentErrorStatus? {
            if (s == null || s.isEmpty()) return null
            try {
                val i = Integer.parseInt(s)
                if (i < values().size) return values()[i]
            } catch (e: NumberFormatException) {
            }

            return null
        }

        fun getByValue(code: Int): PaymentErrorStatus? {
            if (lookup.size == 0) {
                for (s in EnumSet.allOf(PaymentErrorStatus::class.java)) {
                    lookup[s.code] = s
                }
            }
            return lookup[code]
        }
    }
}

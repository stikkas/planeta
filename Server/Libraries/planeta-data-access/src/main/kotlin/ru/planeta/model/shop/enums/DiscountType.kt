package ru.planeta.model.shop.enums

import ru.planeta.model.enums.Codable

import java.util.EnumSet
import java.util.HashMap

/**
 * Created with IntelliJ IDEA.
 * User: michail
 * Date: 25.05.16
 * Time: 16:03
 */
enum class DiscountType private constructor(// total_price = total_price - min(prices of products from cart)

        override val code: Int) : Codable {
    ABSOLUTE(1), // total_price = total_price - discount_amount
    RELATIVE(2), // total_price = total_price * discount_amount/100
    PRODUCTFREE(3);


    companion object {

        private val lookup = HashMap<Int, DiscountType>()

        init {
            for (s in EnumSet.allOf(DiscountType::class.java)) {
                lookup[s.code] = s
            }
        }

        fun getByValue(usageTypeCode: Int): DiscountType? {
            return lookup[usageTypeCode]
        }
    }
}

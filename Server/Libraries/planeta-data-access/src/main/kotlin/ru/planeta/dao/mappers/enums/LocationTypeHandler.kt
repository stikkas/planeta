package ru.planeta.dao.mappers.enums

import org.apache.ibatis.type.JdbcType
import org.apache.ibatis.type.TypeHandler
import ru.planeta.model.profile.location.LocationType

import java.sql.CallableStatement
import java.sql.PreparedStatement
import java.sql.ResultSet
import java.sql.SQLException

/**
 * @author Andrew.Arefyev@gmail.com
 * 30.10.13 20:11
 */
class LocationTypeHandler : TypeHandler<LocationType> {
    @Throws(SQLException::class)
    override fun setParameter(preparedStatement: PreparedStatement, i: Int, o: LocationType, jdbcType: JdbcType) {
        preparedStatement.setInt(i, o.code)
    }

    @Throws(SQLException::class)
    override fun getResult(resultSet: ResultSet, s: String): LocationType? {
        return LocationType.getByValue(resultSet.getInt(s))
    }

    @Throws(SQLException::class)
    override fun getResult(resultSet: ResultSet, i: Int): LocationType? {
        return LocationType.getByValue(resultSet.getInt(i))
    }

    @Throws(SQLException::class)
    override fun getResult(callableStatement: CallableStatement, i: Int): LocationType? {
        return LocationType.getByValue(callableStatement.getInt(i))
    }
}

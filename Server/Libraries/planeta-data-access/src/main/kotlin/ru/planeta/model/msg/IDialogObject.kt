package ru.planeta.model.msg

/**
 * Interface created for us to make sharding simpler.
 * Dialog objects are partitioned by dialogId property value.
 *
 * @author ameshkov
 */
interface IDialogObject {

    var dialogId: Long
}

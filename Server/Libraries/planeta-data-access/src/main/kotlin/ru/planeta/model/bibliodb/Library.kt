package ru.planeta.model.bibliodb

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import ru.planeta.model.bibliodb.enums.BiblioObjectStatus
import ru.planeta.model.bibliodb.enums.LibraryType
import ru.planeta.model.common.Identified

import java.util.Date

@JsonIgnoreProperties(ignoreUnknown = true)
open class Library : Identified {
    var libraryId: Long = -1
    override var id: Long
        get() = libraryId
        set(id) {
            libraryId = id
        }
    var region: String? = null
    var name: String? = null
    var address: String? = null
    var latitude: Double? = null
    var longitude: Double? = null
    var contractor: String? = null
    var email: String? = null
    var comment: String? = null
    var postIndex: String? = null
    var libraryType = LibraryType.PUBLIC
    var site: String? = null
    var status = BiblioObjectStatus.PAUSED
    var timeAdded: Date? = null

    override fun hashCode(): Int {
        var hash = 3
        hash = 53 * hash + (this.libraryId xor this.libraryId.ushr(32)).toInt()
        return hash
    }

    override fun equals(obj: Any?): Boolean {
        if (this === obj) {
            return true
        }
        if (obj == null) {
            return false
        }
        if (javaClass != obj.javaClass) {
            return false
        }
        val other = obj as Library?
        return this.libraryId == other!!.libraryId
    }

}

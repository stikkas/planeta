package ru.planeta.model.common

/**
 * Class DebitTransaction
 *
 * @author a.tropnikov
 */
class DebitTransaction : BaseTransaction() {

    var paymentProviderId: Long? = null
    var externalSystemData: String? = null
}

package ru.planeta.model.stat

import java.util.Date

class CampaignAfterPurchaseMailStat {
    var feedbackLetterId: Long = 0
    var userId: Long = 0
    var orderId: Long = 0
    var timeAdded: Date? = null
    var mailSent: Boolean? = null
    var timeUpdated: Date? = null
}

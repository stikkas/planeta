package ru.planeta.model.profile.media

/**
 * User: a.savanovich
 * Date: 23.03.12
 * Time: 12:01
 */
interface AudioStats {
    var objectId: Long?

    var name: String?

    var ownerAlias: String?

    var artistName: String?

    var authorName: String?
}

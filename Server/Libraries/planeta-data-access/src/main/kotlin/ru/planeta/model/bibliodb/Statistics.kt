package ru.planeta.model.bibliodb

import java.io.Serializable
import java.math.BigDecimal

/**
 * Статистика по библиотекам
 * User: Serge Blagodatskih<stikkas17></stikkas17>@gmail.com><br></br>
 * Date: 11.05.16<br></br>
 * Time: 17:08
 */
class Statistics : Serializable {

    /**
     * Количество изданий в проекте
     */
    var books: Long = 0

    /**
     * Количество библиотек в проекте
     */
    var libraries: Long = 0

    /**
     * Количество подписанных номеров в проекте
     */
    var signedIssues: Long = 0

    /**
     * Рублей собрано на подписку
     */
    var totalSum = BigDecimal.ZERO
}

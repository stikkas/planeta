package ru.planeta.dao.trashcan

import org.apache.ibatis.annotations.Param
import ru.planeta.model.trashcan.PromoEmail

interface PromoEmailDAO {
    fun insert(promoEmail: PromoEmail)

    fun selectSentEmailsCountForUser(@Param("email") email: String, @Param("promoId") promoId: Long): Int
}

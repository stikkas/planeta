package ru.planeta.model.charity

/*
 * Created by Alexey on 06.07.2016.
 */

import java.io.Serializable

class CharityNewsTag : Serializable {
    var tagId: Long = 0
    var nameRus: String? = null
    var nameEng: String? = null
    var mnemonicName: String? = null
    var isForNews: Boolean = false
    var isForPartnerNews: Boolean = false
}

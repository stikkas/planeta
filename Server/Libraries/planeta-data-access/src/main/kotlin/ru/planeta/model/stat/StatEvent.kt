package ru.planeta.model.stat

import java.math.BigDecimal
import java.util.Date

/**
 * Created with IntelliJ IDEA.
 * User: a.volkov
 * Date: 01.08.14
 * Time: 15:41
 */
class StatEvent {
    var id: Long = 0
    var visitorId: Long = 0
    var affiliateId: Long = 0
    var date: Date? = null
    var type: StatEventType? = null
    var uri: String? = null
    var referer: String? = null
    var city: String? = null
    var country: String? = null
    var sourceId: Long = 0
    var objectId: Long = 0
    var amount: BigDecimal? = null
    var affiliateAmount: BigDecimal? = null
}

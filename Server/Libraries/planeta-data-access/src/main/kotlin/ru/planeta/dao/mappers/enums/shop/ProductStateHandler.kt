package ru.planeta.dao.mappers.enums.shop

import org.apache.ibatis.type.JdbcType
import org.apache.ibatis.type.TypeHandler
import ru.planeta.model.shop.enums.ProductState

import java.sql.CallableStatement
import java.sql.PreparedStatement
import java.sql.ResultSet
import java.sql.SQLException

/**
 * Created by IntelliJ IDEA.
 * User: Andrew
 * Date: 13.09.12
 * Time: 23:49
 */
class ProductStateHandler : TypeHandler<ProductState> {

    @Throws(SQLException::class)
    override fun setParameter(preparedStatement: PreparedStatement, i: Int, o: ProductState, jdbcType: JdbcType) {
        preparedStatement.setInt(i, o.code)
    }

    @Throws(SQLException::class)
    override fun getResult(resultSet: ResultSet, s: String): ProductState? {
        return ProductState.getByValue(resultSet.getInt(s))
    }

    @Throws(SQLException::class)
    override fun getResult(rs: ResultSet, columnIndex: Int): ProductState? {
        return ProductState.getByValue(rs.getInt(columnIndex))
    }

    @Throws(SQLException::class)
    override fun getResult(callableStatement: CallableStatement, i: Int): ProductState {
        throw NoSuchMethodError("not implemented for CallableStatement")

    }
}

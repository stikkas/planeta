package ru.planeta.model.bibliodb

import java.util.Objects

/**
 * Книги для корзины
 * User: Serge Blagodatskih<stikkas17></stikkas17>@gmail.com><br></br>
 * Date: 27.04.16<br></br>
 * Time: 13:08
 */
class BookOrder {

    var book: Book? = null

    var count: Int = 0

    constructor() {}

    constructor(book: Book, count: Int) {
        this.book = book
        this.count = count
    }

    override fun hashCode(): Int {
        var hash = 3
        hash = 53 * hash + Objects.hashCode(book)
        hash = 53 * hash + count
        return hash
    }

    override fun equals(obj: Any?): Boolean {
        if (this === obj) {
            return true
        }
        if (obj == null) {
            return false
        }
        if (javaClass != obj.javaClass) {
            return false
        }
        val other = obj as BookOrder?
        return if (count != other!!.count) {
            false
        } else book == other.book
    }

}

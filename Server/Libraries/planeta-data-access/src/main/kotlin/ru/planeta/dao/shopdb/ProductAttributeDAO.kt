package ru.planeta.dao.shopdb

import org.apache.ibatis.annotations.Mapper
import ru.planeta.model.shop.Category
import ru.planeta.model.shop.ProductAttribute

/**
 * User: m.shulepov
 */

@Mapper
interface ProductAttributeDAO {

    /**
     * Insert new category
     *
     * @param category
     */
    fun insertCategory(category: Category)

    /**
     * Select categories
     *
     * @return
     */
    fun selectAllCategories(): List<Category>

    /**
     * Select attribute by id
     *
     * @param attributeId
     * @return
     */
    fun select(attributeId: Long): ProductAttribute

    /**
     * Select attributes by type
     *
     * @return
     */
    fun selectByType(attributeTypeId: Long): List<ProductAttribute>

}

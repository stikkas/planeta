package ru.planeta.dao.commondb

/*
 * Created by Alexey on 05.05.2016.
 */


import org.apache.ibatis.annotations.Mapper
import org.apache.ibatis.annotations.Param
import ru.planeta.model.mail.MailMessage
import ru.planeta.model.mail.MailMessageStatus

import java.util.Date

@Mapper
interface MailMessageDAO {

    fun getMailNotificationById(id: Long): List<MailMessage>

    fun getSpammers(@Param("threshold") threshold: Int, @Param("offset") offset: Int, @Param("limit") limit: Int): List<*>

    fun getMailNotificationByExtMsgId(extMessageId: String): List<MailMessage>

    fun insertMailNotification(mailMessage: MailMessage)

    fun updateMailNotification(mailMessage: MailMessage)

    /*
     deleteByProfileId from email_notifications_logs messages older than deleteBefore
      */
    fun cleanMailNotificationsLogs(deleteBefore: Date)

    /*
     move messages from email_notifications to email_notifications_logs if message has external id and older than deleteBefore
      */
    fun moveOldMailNotifications(deleteBefore: Date)

    fun updateStatus(@Param("extMessageId") extMessageId: String, @Param("status") status: MailMessageStatus)

    fun setExternalMessageIdAndSentStatus(@Param("intMessageId") intMessageId: Long, @Param("extMessageId") extMessageId: String)

    fun saveOpenedMailInfo(messageId: Long)
}

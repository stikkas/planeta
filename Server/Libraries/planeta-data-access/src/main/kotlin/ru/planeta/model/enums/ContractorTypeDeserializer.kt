package ru.planeta.model.enums

import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.core.JsonProcessingException
import com.fasterxml.jackson.databind.DeserializationContext
import com.fasterxml.jackson.databind.JsonDeserializer
import java.io.IOException

/**
 *
 * @author stikkas<stikkas></stikkas>@yandex.ru>
 */
class ContractorTypeDeserializer : JsonDeserializer<ContractorType>() {

    @Throws(IOException::class, JsonProcessingException::class)
    override fun deserialize(jp: JsonParser, dc: DeserializationContext): ContractorType {
        val value = jp.text
        return ContractorType.fromString(value)
    }

}

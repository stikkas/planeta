package ru.planeta.model.common.campaign.enums

import java.util.HashMap

enum class ShareStatus private constructor(// valid share for investing projects

        val code: Int) {

    DRAFT(0), // not valid share
    ACTIVE(1), // valid share in production
    DISABLED(2), // valid share temporary removed from production
    INVESTING(3),
    INVESTING_WITHOUT_MODERATION(4),
    INVESTING_ALL_ALLOWED(5);


    companion object {

        private val lookup = HashMap<Int, ShareStatus>()

        init {
            for (s in values()) {
                lookup[s.code] = s
            }
        }

        fun getByValue(code: Int): ShareStatus? {
            return lookup[code]
        }
    }
}

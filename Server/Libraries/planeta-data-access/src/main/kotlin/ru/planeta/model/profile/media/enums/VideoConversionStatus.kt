package ru.planeta.model.profile.media.enums

import java.util.EnumSet
import java.util.HashMap

/**
 * Video files conversation status
 *
 * @author ds.kolyshev
 * Date: 14.10.11
 */
enum class VideoConversionStatus private constructor(val code: Int) {
    PROCESSING(0), PROCESSED(4), ERROR(8);


    companion object {

        private val lookup = HashMap<Int, VideoConversionStatus>()

        init {
            for (s in EnumSet.allOf(VideoConversionStatus::class.java)) {
                lookup[s.code] = s
            }
        }

        fun getByValue(code: Int): VideoConversionStatus? {
            return lookup[code]
        }
    }
}

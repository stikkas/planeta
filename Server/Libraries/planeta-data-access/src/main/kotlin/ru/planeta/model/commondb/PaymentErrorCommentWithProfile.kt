package ru.planeta.model.commondb

import ru.planeta.model.profile.Profile

class PaymentErrorCommentWithProfile : PaymentErrorComments() {
    var profile: Profile? = null

    var email: String? = null
}

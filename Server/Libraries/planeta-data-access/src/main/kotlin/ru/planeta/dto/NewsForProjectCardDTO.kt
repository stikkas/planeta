package ru.planeta.dto

import java.util.*


class NewsForProjectCardDTO {
    /**
     * идентификатор последней новости
     */
    var newsId: Long? = null

    /**
     * название последней новости
     */
    var newsTitle: String? = null

    /**
     * дата последней новости
     */
    var newsTimeAdded: Date? = null
}

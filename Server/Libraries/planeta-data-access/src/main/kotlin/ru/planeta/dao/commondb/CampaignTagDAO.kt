package ru.planeta.dao.commondb

import org.apache.ibatis.annotations.Mapper
import org.apache.ibatis.annotations.Param
import ru.planeta.model.common.campaign.CampaignTag

/**
 * Campaign tags DAO
 * User: m.shulepov
 */
@Mapper
interface CampaignTagDAO {

    /**
     * Select all campaign tags
     *
     * @return list of campaign tags
     */
    fun selectAll(): List<CampaignTag>

    /**
     * Select tags by campaign
     *
     * @return list of campaign tags for campaign
     */
    fun selectTagsByCampaign(campaignId: Long): List<CampaignTag>

    /**
     * Update tags for campaign(remove tags, which are not in the list and save tags if they not yet in db)
     *
     * @param campaignId campaign id
     * @param newTags    list of new tags
     */
    fun updateTags(@Param("campaignId") campaignId: Long, @Param("newTags") newTags: List<CampaignTag>)

    fun select(campaignTagId: Long): CampaignTag

    fun insert(campaignTag: CampaignTag)

    fun update(campaignTag: CampaignTag)

    fun selectByAlias(SponsorAlias: String): CampaignTag

    fun setTagWithManagerDefaultRelation(campaignTagId: Long)

    fun selectByMnemonicName(name: String): CampaignTag

    fun selectTagsExistsInSeminars(): List<CampaignTag>

    fun selectTagsForCharity(): List<CampaignTag>
}

package ru.planeta.model.common

/**
 * Created with IntelliJ IDEA.
 * User: a.volkov
 * Date: 28.05.14
 * Time: 17:10
 */
interface Identified {
    var id: Long
}

package ru.planeta.model.common

import org.apache.ibatis.type.JdbcType
import org.apache.ibatis.type.TypeHandler

import java.sql.CallableStatement
import java.sql.PreparedStatement
import java.sql.ResultSet
import java.sql.SQLException

/**
 * Type handler for [ShareServiceType] enum<br></br>
 * User: m.shulepov
 * Date: 14.09.12
 * Time: 0:21
 */
class SharedServiceTypeHandler : TypeHandler<ShareServiceType> {

    @Throws(SQLException::class)
    override fun setParameter(preparedStatement: PreparedStatement, i: Int, shareServiceType: ShareServiceType, jdbcType: JdbcType) {
        preparedStatement.setInt(i, shareServiceType.code)
    }

    @Throws(SQLException::class)
    override fun getResult(resultSet: ResultSet, s: String): ShareServiceType? {
        return ShareServiceType.getByValue(resultSet.getInt(s))
    }

    @Throws(SQLException::class)
    override fun getResult(callableStatement: CallableStatement, i: Int): ShareServiceType? {
        return ShareServiceType.getByValue(callableStatement.getInt(i))
    }

    @Throws(SQLException::class)
    override fun getResult(rs: ResultSet, columnIndex: Int): ShareServiceType? {
        return ShareServiceType.getByValue(rs.getInt(columnIndex))
    }
}

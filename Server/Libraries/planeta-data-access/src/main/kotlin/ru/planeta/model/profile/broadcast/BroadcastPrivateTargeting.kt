package ru.planeta.model.profile.broadcast

import ru.planeta.model.profile.ProfileObject

import java.util.Calendar
import java.util.Date

/**
 * @author m.shulepov
 * Date: 22.04.13
 */
class BroadcastPrivateTargeting : ProfileObject() {

    var broadcastId: Long = 0
    var email: String? = null
    var generatedLink: String? = null
    var firstVisitTime: Date? = null
    var validInHours: Int = 0
    var timeAdded: Date? = null

    val expirationTime: Date?
        get() {
            val calendar = Calendar.getInstance()
            if (firstVisitTime == null) {
                return null
            }
            calendar.time = this.firstVisitTime!!
            calendar.add(Calendar.HOUR_OF_DAY, this.validInHours)
            return calendar.time
        }

    val isExpired: Boolean
        get() = expirationTime!!.before(Date())

}

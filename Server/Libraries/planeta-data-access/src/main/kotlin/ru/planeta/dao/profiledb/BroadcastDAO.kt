package ru.planeta.dao.profiledb

import org.apache.ibatis.annotations.Mapper
import org.apache.ibatis.annotations.Param
import ru.planeta.model.profile.broadcast.Broadcast
import ru.planeta.model.profile.broadcast.enums.BroadcastCategory

import java.util.Date

/**
 * Broadcast's DAO
 *
 * @author ds.kolyshev
 * Date: 19.03.12
 */
@Mapper
interface BroadcastDAO {
    /**
     * Selects list of broacasts by event's profile identifier
     *
     */
    @Deprecated("")
    fun selectByProfileId(@Param("profileId") profileId: Long, @Param("offset") offset: Int, @Param("limit") limit: Int): List<Broadcast>

    fun selectById(broadcastId: Long): Broadcast

    fun selectByIdList(idList: List<Long>): List<Broadcast>

    fun selectForAdmin(@Param("searchString") searchString: String, @Param("dateFrom") dateFrom: Date, @Param("dateTo") dateTo: Date, @Param("offset") offset: Long, @Param("limit") limit: Int): List<Broadcast>

    fun selectForAdminCount(@Param("searchString") searchString: String, @Param("dateFrom") dateFrom: Date, @Param("dateTo") dateTo: Date): Long

    fun insert(broadcast: Broadcast)

    fun update(broadcast: Broadcast)

    fun delete(broadcastId: Long)

    /**
     * Selects started broadcasts for specified time from all shards
     *
     */
    fun selectEndedByTime(time: Long): List<Broadcast>

    /**
     * Selects ended broadcasts for specified time from all shards
     *
     */
    fun selectStartedByTime(time: Long): List<Broadcast>

    /**
     * Select list of upcoming broadcasts
     *
     * @param dateTo        finish date
     * @param offset        offset
     * @param limit         limit
     * @return list of upcoming broadcasts
     */
    fun selectUpcomingBroadcasts(@Param("dateTo") dateTo: Date, @Param("onlyDashboard") onlyDashboard: Boolean, @Param("offset") offset: Int, @Param("limit") limit: Int): List<Broadcast>

    /**
     * Select list of archived broadcasts
     *
     * @param dateTo            end date
     * @param offset            offset
     * @param limit             limit   @return  list of archived broadcasts
     */
    fun selectArchivedBroadcasts(@Param("broadcastCategory") broadcastCategory: BroadcastCategory, @Param("dateTo") dateTo: Date, @Param("isPopularSort") isPopularSort: Boolean, @Param("offset") offset: Int, @Param("limit") limit: Int): List<Broadcast>

    fun updateBroadcastViewsCount(defaultStreamId: Long)
}

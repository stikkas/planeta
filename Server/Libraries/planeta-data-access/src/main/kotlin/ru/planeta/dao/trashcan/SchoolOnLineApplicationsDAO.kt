package ru.planeta.dao.trashcan

import ru.planeta.model.trashcan.SchoolOnlineCourseApplication

/**
 *
 * User: Serge Blagodatskih<stikkas17></stikkas17>@gmail.com><br></br>
 * Date: 27.04.16<br></br>
 * Time: 14:48
 */
interface SchoolOnLineApplicationsDAO {
    fun insert(onlineCourseApplication: SchoolOnlineCourseApplication)

    fun select(email: String): SchoolOnlineCourseApplication
}

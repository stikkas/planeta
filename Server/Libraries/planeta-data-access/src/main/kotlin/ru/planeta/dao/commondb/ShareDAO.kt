package ru.planeta.dao.commondb

import org.apache.ibatis.annotations.Mapper
import org.apache.ibatis.annotations.Param
import ru.planeta.dto.ShareDTO
import ru.planeta.model.common.campaign.Share
import ru.planeta.model.common.campaign.ShareDetails
import ru.planeta.model.common.campaign.enums.ShareStatus
import java.util.Date
import java.util.EnumSet

/**
 * Campaign's share DAO
 * Created by IntelliJ IDEA.
 * User: atropnikov
 * Date: 21.03.12
 * Time: 13:05
 */
@Mapper
interface ShareDAO {

    /**
     * Select campaign's shares
     *
     * @param campaignId
     * @param offset
     * @param limit
     * @return
     */
    fun selectCampaignShares(@Param("campaignId") campaignId: Long, @Param("offset") offset: Int, @Param("limit") limit: Int): List<Share>

    fun findShare(shareId: Long): ShareDTO //{

    /**
     * Select campaign's shares
     *
     *
     * @param campaignId
     * @param shareStatuses - list of share statuses
     * @param nonZeroPrice - true if we need shares with non-zero price, false otherwise
     * @param offset
     * @param limit
     * @return
     */
    fun selectCampaignSharesFiltered(@Param("campaignId") campaignId: Long, @Param("shareStatuses") shareStatuses: EnumSet<ShareStatus>, @Param("nonZeroPrice") nonZeroPrice: Boolean, @Param("offset") offset: Int, @Param("limit") limit: Int): List<Share>

    fun select(shareIds: Collection<Long>): List<Share>

    /**
     * Select share by id and profile
     *
     * @param shareId
     * @return
     */
    fun select(shareId: Long): Share?

    fun selectSharesPurchasedInfoOnDateByCampaignId(@Param("date") date: Date, @Param("campaignId") campaignId: Long): List<Share>

    /**
     * Insert new share
     *
     * @param share
     */
    fun insert(share: Share)

    /**
     * Update specified share
     *
     * @param share
     */
    fun update(share: Share)

    /**
     * Delete share by id and profile
     *
     * @param shareId
     */
    fun delete(shareId: Long)

    /**
     * Delete all campaign's shares
     *
     * @param campaignId
     */
    fun deleteCampaignShares(campaignId: Long)

    fun selectForUpdate(shareId: Long): Share

    /**
     * all required data must by linked in api
     *
     * @param shareId
     * @return
     */
    fun selectDetailed(shareId: Long): ShareDetails

    fun selectSharesSaleInfo(campaignId: Long, @Param("from") from: Date?, @Param("to") to: Date?, @Param("offset") offset: Int, @Param("limit") limit: Int): List<Share>

    fun selectSharesPurschaseInfoOnDate(date: Date): List<Share>

    fun disableShare(shareId: Long)
}

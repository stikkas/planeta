package ru.planeta.dao.commondb

import org.apache.ibatis.annotations.Mapper
import org.apache.ibatis.annotations.Param
import ru.planeta.model.common.auth.CredentialType
import ru.planeta.model.common.auth.UserCredentials

/**
 * Date: 12.09.12
 *
 * @author s.kalmykov
 */
@Mapper
interface UserCredentialsDAO {

    /**
     * Inserts UserCredentials instance to commondb.user_credentials
     * no sequence is needed, any user can have many credentials, empty password
     * but no (login, type) duplicates
     *
     * @param userCredentials credentials to insert
     */
    fun insert(userCredentials: UserCredentials)

    /**
     * Removes UserCredentials instance from commondb.user_credentials by profileId and credentialType
     */
    fun deleteByProfileId(@Param("profileId") profileId: Long, @Param("type") credentialType: CredentialType): Int

    /**
     * Removes UserCredentials instance from commondb.user_credentials by username and credentialType
     */
    fun deleteByUsername(@Param("username") username: String, @Param("type") credentialType: CredentialType): Int

    /**
     * Updates UserCredentials instance
     * Used to change status
     *
     * @param userCredentials new credentials to update to
     */
    fun update(userCredentials: UserCredentials)

    /**
     * Return unique record by primary key
     *
     * @param userName login of user
     * @param type     type of authorization
     * @return null if no record found with specified parameters
     */
    fun select(@Param("userName") userName: String, @Param("type") type: CredentialType): UserCredentials

    /**
     * Get all credentials of specified user and type
     *
     * @param userId - profileId of user
     * @param type   - Email, VK, Twitter etc.
     * @return empty list if user has no credentials of specified type
     */
    fun getUserCredentialsOfType(@Param("profileId") userId: Long, @Param("type") type: CredentialType? = null): List<UserCredentials>

    /**
     * Get credentials of the users by type and usernames
     *
     * @param usernames - list of user's profileId
     * @param type      - Email, VK, Twitter etc.
     * @return
     */
    fun getUserCredentialsOfTypeByUsernames(@Param("usernames") usernames: List<String>, @Param("type") type: CredentialType): List<UserCredentials>

}

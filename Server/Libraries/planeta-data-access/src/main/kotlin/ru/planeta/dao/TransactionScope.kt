package ru.planeta.dao

import org.apache.ibatis.session.SqlSession
import org.apache.ibatis.session.SqlSessionFactory

import java.util.HashMap

/**
 * Transaction scope. Current transaction
 *
 * @author ameshkov
 */
class TransactionScope private constructor() {

    private var nestingLevel = 0
    private val currentSessions = HashMap<SqlSessionFactory, SqlSession>()

    /**
     * Gets current sql session (that is used inside this scope).
     * If no sessions yet - opens new session.
     */
    fun getSqlSession(sqlSessionFactory: SqlSessionFactory): SqlSession? {
        var session: SqlSession? = currentSessions[sqlSessionFactory]

        if (session == null) {
            session = sqlSessionFactory.openSession()
            currentSessions[sqlSessionFactory] = session
        }

        return session
    }

    fun commit() {
        if (nestingLevel == 0) {
            for (session in currentSessions.values) {
                session.commit()
            }
        }
    }

    /**
     * Closes all current sessions.
     */
    fun close() {
        if (nestingLevel == 0) {
            for (session in currentSessions.values) {
                session.close()
            }
            current.set(null)
            currentSessions.clear()
        } else {
            nestingLevel--
        }
    }

    companion object {

        private val current = ThreadLocal<TransactionScope>()
        private val isTest = ThreadLocal<Boolean>()

        /**
         * Returns current transaction scope and increments nesting level.
         * If no current transaction - creates new.
         */
        fun createOrGetCurrent(): TransactionScope {

            var transactionScope: TransactionScope? = current.get()

            if (transactionScope == null) {
                transactionScope = TransactionScope()
                current.set(transactionScope)
                isTest.set(false)
            } else {
                transactionScope.nestingLevel++
            }

            return transactionScope
        }

        fun createOrGetCurrentTest(): TransactionScope {

            var transactionScope: TransactionScope? = current.get()

            if (transactionScope == null) {
                transactionScope = TransactionScope()
                current.set(transactionScope)
                isTest.set(true)
            } else {
                transactionScope.nestingLevel++
            }

            return transactionScope
        }

        val isInTestTransactionNesting: Boolean
            get() {
                val transactionScope = current.get()
                val isInTest = isTest.get()
                return if (isInTest == null || !isInTest) {
                    false
                } else transactionScope != null && transactionScope.nestingLevel > 1
            }

        val isInTransaction: Boolean
            get() {
                val transactionScope = current.get()
                return transactionScope != null
            }
    }
}

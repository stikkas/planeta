package ru.planeta.dao.trashcan

import org.apache.ibatis.annotations.Param

/**
 *
 * User: Serge Blagodatskih<stikkas17></stikkas17>@gmail.com><br></br>
 * Date: 27.04.16<br></br>
 * Time: 14:48
 */
interface AdBlockStatisticsDAO {

    fun hasAdBlock(@Param("profileId") profileId: Long, @Param("cid") cid: String, @Param("ip") ip: String)
}

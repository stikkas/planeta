package ru.planeta.model.profile

import ru.planeta.model.enums.ObjectType

/**
 * @author a.savanovich
 */
class CommentObject : ProfileObject() {

    override var objectId: Long? = 0

    var objectTypeIdCode: Int
        get() = objectType!!.code
        set(objectType) {
            this.objectType = ObjectType.Companion.getByCode(objectType) ?: ObjectType.COMMENT
        }

    override fun equals(o: Any?): Boolean {
        if (this === o) return true
        if (javaClass != o?.javaClass) return false
        if (!super.equals(o)) return false

        o as CommentObject

        if (objectId != o.objectId) return false

        return true
    }

    override fun hashCode(): Int {
        var result = super.hashCode()
        result = 31 * result + (objectId?.hashCode() ?: 0)
        return result
    }


}

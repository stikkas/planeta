package ru.planeta.model.common.campaign.param

import ru.planeta.model.enums.ObjectType
import ru.planeta.model.news.ProfileNews

import java.util.*

/**
 *
 * Created by kostiagn on 02.06.2015.
 */
open class CampaignEventsParam {
    var query: String? = null
    var campaignId: Long? = null
    var profileNewsType: ProfileNews.Type? = null
        set(profileNewsType) {
            field = profileNewsType
            this.profileNewsTypeSet = if (profileNewsType == null) null else EnumSet.of(profileNewsType)
        }
    var profileNewsTypeSet: Set<ProfileNews.Type>? = null
    var dateFrom: Date? = null
    var dateTo: Date? = null
    var offset = 0
    var limit = 20
    var commentsType: ObjectType? = null
    var managerId: Long? = null
    var profileId: Long? = null
    var postId: Long? = null

    val commentsTypeCode: Int?
        get() = if (commentsType == null) null else commentsType!!.code

    fun setProfileNewsTypeSet(vararg profileNewsTypeList: ProfileNews.Type) {
        this.profileNewsTypeSet = HashSet(Arrays.asList(*profileNewsTypeList))
    }
}

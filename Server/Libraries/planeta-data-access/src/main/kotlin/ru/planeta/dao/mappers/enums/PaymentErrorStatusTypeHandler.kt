package ru.planeta.dao.mappers.enums

import java.sql.CallableStatement
import java.sql.PreparedStatement
import java.sql.ResultSet
import java.sql.SQLException
import org.apache.ibatis.type.JdbcType
import org.apache.ibatis.type.MappedTypes
import org.apache.ibatis.type.TypeHandler
import ru.planeta.model.enums.PaymentErrorStatus

@MappedTypes(PaymentErrorStatus::class)
class PaymentErrorStatusTypeHandler : TypeHandler<PaymentErrorStatus> {

    @Throws(SQLException::class)
    override fun setParameter(ps: PreparedStatement, i: Int, parameter: PaymentErrorStatus, jdbcType: JdbcType) {
        ps.setInt(i, parameter.code)
    }

    @Throws(SQLException::class)
    override fun getResult(rs: ResultSet, columnName: String): PaymentErrorStatus? {
        return if (rs.getObject(columnName) == null) null else PaymentErrorStatus.getByValue(rs.getInt(columnName))
    }

    @Throws(SQLException::class)
    override fun getResult(rs: ResultSet, columnIndex: Int): PaymentErrorStatus? {
        return if (rs.getObject(columnIndex) == null) null else PaymentErrorStatus.getByValue(rs.getInt(columnIndex))
    }

    @Throws(SQLException::class)
    override fun getResult(cs: CallableStatement, columnIndex: Int): PaymentErrorStatus? {
        return if (cs.getObject(columnIndex) == null) null else PaymentErrorStatus.getByValue(cs.getInt(columnIndex))
    }
}

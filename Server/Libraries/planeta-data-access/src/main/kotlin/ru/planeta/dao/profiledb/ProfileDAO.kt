package ru.planeta.dao.profiledb

import org.apache.ibatis.annotations.Mapper
import org.apache.ibatis.annotations.Param
import ru.planeta.model.profile.Profile
import java.util.Date

/**
 * Interface ProfileDAO
 *
 * @author a.tropnikov
 */
@Mapper
interface ProfileDAO {

    fun selectById(profileId: Long): Profile?

    fun selectByAlias(alias: String): Profile?

    fun selectSearchedByIds(@Param("profileIds") profileIds: Collection<Long>, @Param("clientId") clientId: Long = 0L): List<Profile>

    fun insert(profile: Profile)

    fun update(profile: Profile)

    fun delete(profileId: Long)

    fun getHiddenGroup(clientId: Long): Profile

    fun searchProfiles(@Param("clientId") clientId: Long, @Param("offset") offset: Int, @Param("limit") limit: Int): List<Profile>

    fun selectProfilesCount(): Long

    fun updateProjectsCount(profileId: Long)

    fun updateHasActiveProjects(profileId: Long)

    fun updateBackedCount(profileId: Long)

    fun updateSubscribersCount(profileId: Long)

    fun updateNewSubscribersCount(@Param("profileId") profileId: Long, @Param("newSubscribersCount") newSubscribersCount: Long)

    fun getSpammers(@Param("date") date: Date, @Param("offset") offset: Int, @Param("limit") limit: Int): List<*>

    fun getShopProductsProfiles(@Param("tagId") tagId: Long, @Param("limit") limit: Int): List<Profile>

    fun selectWithDeletedById(profileId: Long): Profile
}

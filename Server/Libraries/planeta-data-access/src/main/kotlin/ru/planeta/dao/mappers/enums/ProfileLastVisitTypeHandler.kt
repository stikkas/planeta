package ru.planeta.dao.mappers.enums

import org.apache.ibatis.type.JdbcType
import org.apache.ibatis.type.TypeHandler
import ru.planeta.model.enums.ProfileLastVisitType

import java.sql.CallableStatement
import java.sql.PreparedStatement
import java.sql.ResultSet
import java.sql.SQLException

class ProfileLastVisitTypeHandler : TypeHandler<ProfileLastVisitType> {

    @Throws(SQLException::class)
    override fun setParameter(ps: PreparedStatement, i: Int, parameter: ProfileLastVisitType, jdbcType: JdbcType) {
        ps.setInt(i, parameter.ordinal)
    }

    @Throws(SQLException::class)
    override fun getResult(rs: ResultSet, columnName: String): ProfileLastVisitType {
        return ProfileLastVisitType.values()[rs.getInt(columnName)]
    }

    @Throws(SQLException::class)
    override fun getResult(cs: CallableStatement, columnIndex: Int): ProfileLastVisitType {
        return ProfileLastVisitType.values()[cs.getInt(columnIndex)]
    }

    @Throws(SQLException::class)
    override fun getResult(rs: ResultSet, columnIndex: Int): ProfileLastVisitType {
        return ProfileLastVisitType.values()[rs.getInt(columnIndex)]
    }

}

package ru.planeta.model.enums

/**
 * Describes post search filter type contants.
 * Created by eshevchenko.
 */
enum class PostFilterType {
    ALL, USERS, CAMPAIGNS
}

package ru.planeta.model.profile

import ru.planeta.model.enums.ObjectType
import java.io.Serializable
import java.util.*

/**
 * @author a.savanovich
 */
class Comment : ProfileObject(), Serializable, ICommentable, IVotable {

    /**
     * zero if there are no parent comments,
     * it can be comments with not-null parent, but with objectType != COMMENT for no-hierarchical comments (campaigns)
     */
    var parentCommentId: Long = 0
    override var objectId: Long? = 0
    /**
     * type of object
     *
     * @see ObjectType
     */
    override var objectType: ObjectType? = null
    /**
     * depth of comment node in commentaries tree
     */
    var level: Int = 0
    var childsCount: Int = 0
    var timeAdded: Date? = null
    var isDeleted: Boolean = false
    var text: String? = null
    var textHtml: String? = null
    var authorFirstName: String? = null

    override var voteObjectType: ObjectType? = ObjectType.COMMENT

    var childComments: List<Comment>? = null

    var commentId: Long
        get() = voteObjectId
        set(commentId) {
            voteObjectId = commentId
        }



    companion object {

        private const val serialVersionUID = 1L
    }

    override fun equals(o: Any?): Boolean {
        if (this === o) return true
        if (javaClass != o?.javaClass) return false
        if (!super.equals(o)) return false

        o as Comment

        if (objectId != o.objectId) return false
        if (objectType != o.objectType) return false

        return true
    }

    override fun hashCode(): Int {
        var result = super.hashCode()
        result = 31 * result + (objectId?.hashCode() ?: 0)
        result = 31 * result + (objectType?.hashCode() ?: 0)
        return result
    }
}

package ru.planeta.dao.commondb

import org.apache.ibatis.annotations.Mapper

/**
 * Helper object that selects next value of the sequence from the commondb.
 * This way we're making sure that all identifiers are unique across the cluster.
 *
 * @author ameshkov
 */
@Mapper
interface SequencesDAO {

    /**
     * Selects next sequence value for the specified class
     *
     * @param clazz
     * @return
     */
    fun selectNextLong(clazz: Class<*>): Long
}

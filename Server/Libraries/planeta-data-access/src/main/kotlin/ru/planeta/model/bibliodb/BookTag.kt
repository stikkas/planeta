package ru.planeta.model.bibliodb

/*
 * Created by Alexey on 07.04.2016.
 */
import java.io.Serializable

class BookTag : Serializable {
    /**
     * Идентификатор категории
     */
    var tagId: Long = 0
    /**
     * Название категории
     */
    var name: String? = null
    /**
     * Порядок сортировки категорий
     */
    var sortOrder: Int = 0
    /**
     * Кодовое название категории
     */
    var mnemonicName: String? = null
    /**
     * Признак основной категории
     */
    var isPrimary: Boolean = false

    constructor() {}

    constructor(categoryId: Long) {
        this.tagId = categoryId
    }

    constructor(name: String, tagId: Long) {
        this.name = name
        this.tagId = tagId
    }

    override fun equals(o: Any?): Boolean {
        if (this === o) return true
        if (o !is BookTag) return false

        val that = o as BookTag?

        if (tagId != that!!.tagId) return false
        if (if (mnemonicName != null) mnemonicName != that.mnemonicName else that.mnemonicName != null) return false
        return if (if (name != null) name != that.name else that.name != null) false else true

    }

    override fun hashCode(): Int {
        var result = tagId.toInt()
        result = 31 * result + if (name != null) name!!.hashCode() else 0
        result = 31 * result + if (mnemonicName != null) mnemonicName!!.hashCode() else 0
        return result
    }

    companion object {

        val RUSSIANPOST = "RUSSIANPOST"
    }

}

package ru.planeta.dao.handlers

import org.apache.ibatis.type.BaseTypeHandler
import org.apache.ibatis.type.JdbcType
import org.apache.ibatis.type.MappedTypes
import ru.planeta.model.stat.log.LoggerType
import java.sql.CallableStatement
import java.sql.PreparedStatement
import java.sql.ResultSet

/**
 * Logger type handler.<br></br>
 * User: eshevchenko
 */
@MappedTypes(LoggerType::class)
class LoggerTypeHandler : BaseTypeHandler<LoggerType>() {

    override fun getNullableResult(p0: ResultSet, p1: String): LoggerType? {
        return LoggerType.getByCode(p0.getInt(p1))
    }

    override fun getNullableResult(p0: ResultSet, p1: Int): LoggerType? {
        return LoggerType.getByCode(p0.getInt(p1))
    }

    override fun getNullableResult(p0: CallableStatement, p1: Int): LoggerType? {
        return LoggerType.getByCode(p0.getInt(p1))
    }

    override fun setNonNullParameter(p0: PreparedStatement, p1: Int, p2: LoggerType, p3: JdbcType?) {
        p0.setInt(p1, p2.code)
    }

}

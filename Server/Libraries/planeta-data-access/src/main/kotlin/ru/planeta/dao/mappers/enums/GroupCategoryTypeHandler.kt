package ru.planeta.dao.mappers.enums

import org.apache.ibatis.type.JdbcType
import org.apache.ibatis.type.TypeHandler
import ru.planeta.model.enums.GroupCategory

import java.sql.CallableStatement
import java.sql.PreparedStatement
import java.sql.ResultSet
import java.sql.SQLException

/**
 * Mybatis mapper for [ru.planeta.model.enums.GroupCategory] class
 *
 * @author m.shulepov
 */
class GroupCategoryTypeHandler : TypeHandler<GroupCategory> {

    @Throws(SQLException::class)
    override fun setParameter(ps: PreparedStatement, i: Int, parameter: GroupCategory, jdbcType: JdbcType) {
        ps.setInt(i, parameter.code)
    }

    @Throws(SQLException::class)
    override fun getResult(rs: ResultSet, columnName: String): GroupCategory? {
        return GroupCategory.getByValue(rs.getInt(columnName))
    }

    @Throws(SQLException::class)
    override fun getResult(cs: CallableStatement, columnIndex: Int): GroupCategory? {
        return GroupCategory.getByValue(cs.getInt(columnIndex))
    }

    @Throws(SQLException::class)
    override fun getResult(rs: ResultSet, columnIndex: Int): GroupCategory? {
        return GroupCategory.getByValue(rs.getInt(columnIndex))
    }
}

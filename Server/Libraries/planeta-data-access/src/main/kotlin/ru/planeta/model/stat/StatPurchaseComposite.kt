package ru.planeta.model.stat

import ru.planeta.model.enums.OrderObjectType

import java.math.BigDecimal
import java.util.Date

/**
 * @author is.kuzminov
 * Date: 16.08.12
 * Time: 15:07
 */
class StatPurchaseComposite {
    var startDate: Date? = null
    var amount: BigDecimal? = null
    var objectType: OrderObjectType? = null
    var name: String? = null
    var projectName: String? = null
    var count: Int = 0
    var sumOfObjectsCount: Int = 0
    var regCount: Int = 0

    fun setOrderObjectCode(code: Int) {
        this.objectType = OrderObjectType.Companion.getByValue(code)
    }
}

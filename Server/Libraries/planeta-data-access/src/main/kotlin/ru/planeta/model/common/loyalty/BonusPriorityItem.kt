package ru.planeta.model.common.loyalty

/**
 * @author Andrew.Arefyev@gmail.com
 * 10.04.2014 20:52
 */
class BonusPriorityItem {
    var bonusId: Long = 0
    var priority: Int = 0

    constructor() {}

    constructor(bonusId: Long, priority: Int) {
        this.bonusId = bonusId.toInt().toLong()
        this.priority = priority
    }
}

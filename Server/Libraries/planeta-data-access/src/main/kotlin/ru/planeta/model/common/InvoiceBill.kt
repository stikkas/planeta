package ru.planeta.model.common

import com.fasterxml.jackson.annotation.JsonFormat
import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import java.io.Serializable
import java.math.BigDecimal
import java.util.Date
import ru.planeta.commons.model.Gender
import ru.planeta.model.enums.ContractorType
import ru.planeta.model.enums.ProjectType
import ru.planeta.model.enums.TopayTransactionStatus

/**
 * Join topay_transactions & investing_order_info
 * User: Serge Blagodatskih<stikkas17></stikkas17>@gmail.com><br></br>
 * Date: 16.02.16<br></br>
 * Time: 11:59
 */
@JsonIgnoreProperties(ignoreUnknown = true)
class InvoiceBill : Serializable {

    /**
     * Bill number
     */
    var transactionId: Long = 0
    /**
     * Order number
     */
    var orderId: Long = 0
    /**
     * User's profile id
     */
    var profileId: Long = 0
    /**
     * total sum of bill
     */
    var amountNet: BigDecimal? = null
    /**
     * status of transaction
     */
    var status: TopayTransactionStatus? = null

    /**
     * insert date of the transaction
     */
    var timeAdded: Date? = null

    var timeUpdated: Date? = null

    var expireDate: Date? = null

    var projectType: ProjectType? = null

    /**
     * тип покупателя<br></br>
     * физ.лицо или юр.лицо
     */
    var userType: ContractorType? = null

    /**
     * телефон (ФЛ, ЮЛ) <br></br>
     * +7 и еще 10 цифр
     */
    var phoneNumber: String? = null

    /**
     * фамилия покупателя (ФЛ) или руководителя (ЮЛ)
     */
    var lastName: String? = null

    /**
     * имя покупателя (ФЛ) или руководителя (ЮЛ)
     */
    var firstName: String? = null

    /**
     * отчество покупателя (ФЛ) или руководителя (ЮЛ)
     */
    var middleName: String? = null

    /**
     * должность руководителя (ЮЛ)
     */
    var bossPosition: String? = null

    /**
     * пол (ФЛ)
     */
    var gender: Gender? = null

    /**
     * полное наименование юрлица (ЮЛ)
     */
    var orgName: String? = null

    /**
     * сокращенное наименование юрлица или бренд (ЮЛ)
     */
    var orgBrand: String? = null

    /**
     * дата рождения (ФЛ) или дата регистрации (ЮЛ)
     */
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd.MM.yyyy")
    var birthDate: Date? = null

    /**
     * место рождения (ФЛ)
     */
    var birthPlace: String? = null

    /**
     * индекс регистрации (ФЛ) или юридический индекс (ЮЛ)
     */
    var regIndex: String? = null

    /**
     * id страна регистрации (ФЛ) или юридический страна (ЮЛ)
     */
    var regCountryId: Long = 0

    /**
     * регион регистрации (ФЛ) или юридический регион (ЮЛ)
     */
    var regRegion: String? = null

    /**
     * населенный пункт регистрации (ФЛ) или юридический населенный пункт (ЮЛ)
     */
    var regLocation: String? = null

    /**
     * адрес регистрации (ФЛ) или юридический адрес (ЮЛ)
     */
    var regAddress: String? = null

    /**
     * индекс проживания (ФЛ) или фактический индекс (ЮЛ)
     */
    var liveIndex: String? = null

    /**
     * id страна проживания (ФЛ) или фактический страна(ЮЛ)
     */
    var liveCountryId: Long = 0

    /**
     * регион проживания (ФЛ) или фактический регион(ЮЛ)
     */
    var liveRegion: String? = null

    /**
     * населенный пункт проживания (ФЛ) или фактический населенный пункт(ЮЛ)
     */
    var liveLocation: String? = null

    /**
     * адрес проживания (ФЛ) или фактический адрес (ЮЛ)
     */
    var liveAddress: String? = null

    /**
     * серия и номер паспорта (ФЛ)
     */
    var passportNumber: String? = null

    /**
     * кем выдан паспорт (ФЛ)
     */
    var passportIssuer: String? = null

    /**
     * когда выдан паспорт (ФЛ)
     */
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd.MM.yyyy")
    var passportIssueDate: Date? = null

    /**
     * код подразделения, выдавшего паспорт (ФЛ)
     */
    var passportIssuerCode: String? = null

    /**
     * инн (ЮЛ)<br></br>
     * 10 или 12 цифр
     */
    var inn: String? = null

    /**
     * сайт (ЮЛ)
     */
    var webSite: String? = null

    /**
     * Project Name (Campaign Name)
     */
    var name: String? = null

    /**
     * Project Id (Campaign Id)
     */
    var campaignId: Long = 0

    /**
     * Image for share
     */
    var imageUrl: String? = null

    /**
     * Name of Author
     */
    var merchantName: String? = null

    /**
     * Id of Author
     */
    var merchantId: Long = 0
    /**
     * Alias of Author
     */
    var merchantAlias: String? = null

    var statusCode: Int
        @JsonIgnore
        get() = if (status != null) status!!.code else 0
        set(code) {
            this.status = TopayTransactionStatus.getByValue(code)
        }

    var projectTypeCode: Int
        @JsonIgnore
        get() = if (projectType != null) projectType!!.code else 0
        set(code) {
            this.projectType = ProjectType.getByCode(code)
        }

    var userTypeCode: Int
        @JsonIgnore
        get() = if (userType != null) userType!!.code else 0
        set(code) {
            this.userType = ContractorType.getByValue(code)
        }

    var genderCode: Int
        @JsonIgnore
        get() = if (gender != null) gender!!.code else 0
        set(gender) {
            this.gender = Gender.getByValue(gender)
        }

    companion object {

        private const val serialVersionUID = 1L
    }

}

package ru.planeta.model.common

import ru.planeta.model.enums.UserStatus
import java.io.Serializable
import java.util.Date
import java.util.EnumSet

/**
 * @author a.savanovich
 */
class UserPrivateInfo : Serializable {

    var userId: Long = 0
    var email: String? = null
    var userStatus: EnumSet<UserStatus>? = null
    var password: String? = null
    var username: String? = null
        get(): String? {
            return field?.toLowerCase()
        }
    var regCode: String? = null
    var timeAdded: Date? = null
    var timeUpdated: Date? = null

    /**
     * For mybatis only
     */
    /**
     * For mybatis only
     */
    var status: Int
        get() = UserStatus.getCodeByEnumSet(userStatus!!)
        set(statusCode) {
            this.userStatus = UserStatus.getEnumSetFromCode(statusCode)
        }

    constructor() {}

    constructor(other: UserPrivateInfo) {
        this.userId = other.userId
        this.email = other.email
        this.userStatus = other.userStatus
        this.password = other.password
        this.username = other.username
        this.regCode = other.regCode
        this.timeAdded = other.timeAdded
        this.timeUpdated = other.timeUpdated
    }


    fun setUsernameAndEmail(email: String) {
        username = email
        this.email = email
    }

}

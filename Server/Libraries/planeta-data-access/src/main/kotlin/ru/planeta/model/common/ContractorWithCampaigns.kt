package ru.planeta.model.common

import ru.planeta.model.common.campaign.Campaign

class ContractorWithCampaigns : Contractor() {
    var campaigns: List<Campaign>? = null
}

package ru.planeta.dao

import org.apache.ibatis.annotations.Mapper
import ru.planeta.entity.Profile

@Mapper
interface ProfileRepositoryDAO {

    fun insert(profile: Profile)

    fun findOne(profileId: Long): Profile?

    fun update(profile: Profile)
}

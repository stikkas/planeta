package ru.planeta.model.mail

/**
 * Created with IntelliJ IDEA.
 * User: s.fionov
 * Date: 13.08.12
 */
class MailAttachment {
    var attachmentId: Long? = null
    var templateId: Int? = null
    var fileName: String? = null
    var mimeType: String? = null
    var content: ByteArray? = null
}

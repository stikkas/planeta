package ru.planeta.model.common.delivery

import java.math.BigDecimal

/**
 * @author Andrew.Arefyev@gmail.com
 * 29.10.13 15:01
 */
class LinkedDelivery : BaseDelivery {

    var subjectId: Long = 0
    var price: BigDecimal? = null
    var publicNote: String = ""
    var subjectType = SubjectType.SHARE

    constructor() : super() {}

    constructor(delivery: BaseDelivery) : super(delivery) {}

    constructor(delivery: LinkedDelivery) : super(delivery) {
        subjectId = delivery.subjectId
        price = delivery.price
        publicNote = delivery.publicNote
        subjectType = delivery.subjectType
    }

    fun getShareId(): Long {
        return subjectId
    }

    fun setShareId(shareId: Long) {
        this.subjectId = shareId
    }

}

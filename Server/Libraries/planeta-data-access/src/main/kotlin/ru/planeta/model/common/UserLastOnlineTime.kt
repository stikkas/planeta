package ru.planeta.model.common

import java.util.Date

class UserLastOnlineTime {
    var profileId: Long = 0
    var lastOnlineTime: Date? = null
    var isOnline: Boolean = false

    constructor() {}

    constructor(profileId: Long, lastOnlineTime: Date) {
        this.profileId = profileId
        this.lastOnlineTime = lastOnlineTime
    }

    constructor(profileId: Long, lastOnlineTime: Date, online: Boolean) {
        this.profileId = profileId
        this.lastOnlineTime = lastOnlineTime
        isOnline = online
    }
}

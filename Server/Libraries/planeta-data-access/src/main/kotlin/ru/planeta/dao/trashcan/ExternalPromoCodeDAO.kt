package ru.planeta.dao.trashcan

import org.apache.ibatis.annotations.Mapper
import org.apache.ibatis.annotations.Param
import ru.planeta.model.trashcan.PromoCode

@Mapper
interface ExternalPromoCodeDAO {
    fun selectLastNonUsed(configId: Long): PromoCode

    fun updatePromoCode(code: PromoCode)

    fun usePromoCode(code: PromoCode)

    fun selectAll(configId: Long): List<PromoCode>

    fun insertCodes(@Param("configId") configId: Long, @Param("codes") codes: List<String>)

    fun selectUsedPromoCodesCount(configId: Long): Long
}

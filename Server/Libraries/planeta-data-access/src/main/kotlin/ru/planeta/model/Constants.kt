package ru.planeta.model

/**
 * @author ds.kolyshev
 * Date: 03.08.11
 */
object Constants {

    // Album types
    val ALBUM_DEFAULT = 1
    val ALBUM_AVATAR = 2
    val ALBUM_USER_HIDDEN = 4
    val ALBUM_GROUP_PRODUCT = 6
    val ALBUM_COMMENT = 7
    val ALBUM_BROADCAST = 9
    val ALBUM_BROADCAST_STREAM = 10
    val ALBUM_CAMPAIGN = 11
    val ALBUM_VIDEO_PREVIEWS = 12
    val ALBUM_VIDEO_STORYBOARDS = 13

    val CONNECTION_TIMEOUT_MILLISECONDS = 50000
    val READ_TIMEOUT_MILLISECONDS = 30000

    // Id for fictive profile. Uses for global planeta's balance definition.
    val PLANETA_PROFILE_ID: Long = -1

}



package ru.planeta.model.common

import java.io.Serializable
import java.math.BigDecimal
import java.util.Date

/**
 * Base transaction class.<br></br>
 * Contains common transaction attributes.
 */
open class BaseTransaction : Serializable {

    var transactionId: Long = 0
    var profileId: Long = 0
    var amountNet: BigDecimal? = null
    var amountFee: BigDecimal? = null
    var comment: String? = null
    var timeAdded: Date? = null
}

package ru.planeta.dao.commondb

import org.apache.ibatis.annotations.Mapper
import ru.planeta.model.common.UserLastOnlineTime

@Mapper
interface UserLastOnlineTimeDAO {
    /**
     * Inserts new userLastOnlineTime Object
     */
    fun insert(userLastOnlineTime: UserLastOnlineTime): Int

    /**
     * Updates userLastOnlineTime Object
     */
    fun update(userLastOnlineTime: UserLastOnlineTime): Int

    /**
     * Delete userLastOnlineTime by profile_id
     */
    fun delete(profileId: Long): Int

    /**
     * Select userLastOnlineTime by profile_id
     */
    fun select(profileId: Long): UserLastOnlineTime

    /**
     * Selects UserLastOnlineTime's list for given profileID's list
     */
    fun selectList(profileIds: List<Long>): List<UserLastOnlineTime>

}

package ru.planeta.model.profile

import ru.planeta.commons.model.Gender
import ru.planeta.model.enums.GroupCategory
import ru.planeta.model.enums.ProfileStatus
import ru.planeta.model.enums.ProfileType
import java.io.Serializable
import java.util.Date

/**
 * Class Profile
 *
 * @author a.tropnikov
 */
open class Profile : Serializable {

    var profileId: Long = 0
    var creatorProfileId: Long = 0
    var profileType: ProfileType? = null
    var imageUrl: String? = null
    var imageId: Long = 0
    var smallImageUrl: String? = null
    var smallImageId: Long = 0
    var alias: String? = null
    var status = ProfileStatus.NOT_SET
    var cityId: Int = 0
    var countryId: Int = 0
    var userBirthDate: Date? = null
    var userGender: Gender? = Gender.NOT_SET
    var groupCategory: GroupCategory? = null
    var cityName: String? = null
    var countryName: String? = null
    var cityNameEng: String? = null
    var countryNameEng: String? = null
    var usersCount: Int = 0
    var displayName: String? = null
    var summary: String? = null
    var isVip: Boolean = false
    var vipObtainDate: Date? = null
    var isLimitMessages: Boolean = false
    var isReceiveNewsletters: Boolean = false
    var isShowBackedCampaigns: Boolean = false
    var backedCount: Int = 0
    var projectsCount: Int = 0
    var authorProjectsCount: Int = 0
    var subscribersCount: Int = 0
    var newSubscribersCount: Int = 0
    var subscriptionsCount: Int = 0
    private var canSendMsg: Boolean = false
    var isReceiveMyCampaignNewsletters: Boolean = false
    var phoneNumber: String? = null
    var timeAdded: Date? = null
    var timeUpdated: Date? = null

    var profileTypeCode: Int
        get() = if (profileType == null) 0 else profileType!!.code
        set(code) {
            profileType = ProfileType.Companion.getByValue(code)
        }

    var userGenderCode: Int
        get() = if (userGender == null) 0 else userGender!!.code
        set(code) {
            userGender = Gender.getByValue(code)
        }

    var groupCategoryCode: Int
        get() = if (groupCategory == null) 0 else groupCategory!!.code
        set(code) {
            groupCategory = GroupCategory.Companion.getByValue(code)
        }

    val userLink: String
        get() = alias ?: profileId.toString()

    val isOfficialGroup: Boolean
        get() = ProfileType.GROUP === profileType && ProfileStatus.GROUP_ACTIVE_OFFICIAL === status

    val webAlias: String
        get() = alias ?: profileId.toString()

    constructor() {}

    constructor(code: Int, profileType: ProfileType) {
        this.profileType = profileType
        this.status = ProfileStatus.Companion.getByValue(code, profileType)
    }

    /**
     * for no duplicates in Set<Profile>
     * generated
    </Profile> */
    override fun equals(o: Any?): Boolean {
        if (this === o) return true
        if (o == null) return false

        if (o !is Profile) return false

        val profile = o as Profile?

        return profileId == profile!!.profileId

    }

    override fun hashCode(): Int {
        return (profileId xor profileId.ushr(32)).toInt()
    }

    fun getcanSendMsg(): Boolean {
        return canSendMsg
    }

    fun setcanSendMsg(canSendMsg: Boolean) {
        this.canSendMsg = canSendMsg
    }

    companion object {

        private const val serialVersionUID = 1L
    }


}

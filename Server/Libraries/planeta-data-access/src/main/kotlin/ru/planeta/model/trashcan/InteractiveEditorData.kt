package ru.planeta.model.trashcan

import java.util.Date

class InteractiveEditorData {
    var dataId: Long = 0
    var userId: Long = 0
    var campaignId: Long = 0
    var contractorTypePercent = 13
    var desiredCollectedAmountPercent = 10
    var reachedStepNum = 0
    var formsSettings = "{}"
    var isAgreeStorePersonalData = true
    var isAgreeReceiveLessonsMaterialsOnEmail = false
    var timeAdded: Date? = null

    fun setShowFormsSettingsBaseOnReachedStepNum(reachedStepNum: Int) {
        var showFormsSettings = "{"
        for (i in 0 until reachedStepNum) {
            if (i > 0) {
                showFormsSettings += ","
            }
            showFormsSettings += "\"$i\":{\"showForm\":true}"
        }
        showFormsSettings += "}"
        this.formsSettings = showFormsSettings
    }
}

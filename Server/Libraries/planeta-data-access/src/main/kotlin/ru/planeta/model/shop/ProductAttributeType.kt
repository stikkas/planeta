package ru.planeta.model.shop

/**
 * User: m.shulepov
 */
class ProductAttributeType {

    var attributeTypeId: Long = 0
    var value: String? = null

    companion object {

        fun getDefault(id: Long): ProductAttributeType {
            val type = ProductAttributeType()
            type.attributeTypeId = id
            type.value = "Параметры"
            return type
        }
    }
}

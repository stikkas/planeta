package ru.planeta.dao.commondb

import org.apache.ibatis.annotations.Mapper
import org.apache.ibatis.annotations.Param
import ru.planeta.model.advertising.AdvertisingDTO
import ru.planeta.model.advertising.AdvertisingState
import java.util.*

/**
 * Created with IntelliJ IDEA.
 * Date: 05.03.14
 * Time: 18:28
 */
@Mapper
interface AdvertisingDAO {

    fun activeByDateAdvertising(dateTo: Date): List<AdvertisingDTO>

    fun inactiveByDateAdvertising(dateFrom: Date): List<AdvertisingDTO>

    fun getAdvertising(@Param("broadcastIdFrom") broadcastIdFrom: Long, @Param("broadcastIdTo") broadcastIdTo: Long,
                       @Param("state") state: AdvertisingState?): List<AdvertisingDTO>

    fun saveAdvertising(dto: AdvertisingDTO)

    fun deleteAdvertising(eventId: Long)

    fun getAdvertisingByCampaign(campaignId: Long): List<AdvertisingDTO>
}

package ru.planeta.entity

import java.util.*

abstract class AbstractTimestampEntity {

    var timeAdded: Date? = null

    var timeUpdated: Date? = null

}

package ru.planeta.model.enums

import java.util.*

/**
 * User roles/statuses enumeration
 *
 * @author ds.kolyshev
 * Date: 21.09.11
 */
enum class UserStatus (val code: Int) {

    USER(1), BLOCKED(2), DELETED(4), MODERATOR(8), MANAGER(16), ADMIN(32), SUPER_ADMIN(64), NO_COMMENT(128);


    companion object {
        private val lookup = HashMap<Int, UserStatus>()

        init {
            for (s in EnumSet.allOf(UserStatus::class.java)) {
                lookup[s.code] = s
            }
        }

        fun getByValue(code: Int): UserStatus? {
            return lookup[code]
        }

        fun getCodeByEnumSet(set: EnumSet<UserStatus>): Int {
            var statusCode = 0
            for (s in set) {
                statusCode = statusCode or s.code
            }

            return statusCode
        }

        fun getEnumSetFromCode(code: Int): EnumSet<UserStatus> {
            val set = EnumSet.noneOf(UserStatus::class.java)

            for (s in UserStatus.values()) {
                if (s.code and code != 0) {
                    set.add(s)
                }
            }

            return set
        }
    }

}

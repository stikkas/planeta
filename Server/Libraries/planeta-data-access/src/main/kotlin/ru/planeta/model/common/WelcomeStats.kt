package ru.planeta.model.common

class WelcomeStats {

    var activeCampaigns: Int? = null
    var users: Int? = null
    var shares: Int? = null
}

package ru.planeta.model.advertising

import java.util.HashMap

/**
 * Created with IntelliJ IDEA.
 * Date: 28.02.14
 * Time: 15:21
 */
enum class AdvertisingState private constructor(val state: Int) {
    THIS(1),
    GLOBAL(2),
    NONE(0);


    companion object {

        private val lookup = HashMap<Int, AdvertisingState>()

        init {
            for (s in values()) {
                lookup[s.state] = s
            }
        }

        fun fromCode(code: Int): AdvertisingState? {
            return lookup[code]
        }
    }
}

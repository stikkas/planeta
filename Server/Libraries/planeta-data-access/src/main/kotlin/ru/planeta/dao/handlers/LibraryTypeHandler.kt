package ru.planeta.dao.handlers

import org.apache.ibatis.type.BaseTypeHandler
import org.apache.ibatis.type.JdbcType
import org.apache.ibatis.type.MappedTypes
import ru.planeta.model.bibliodb.enums.LibraryType
import java.sql.CallableStatement
import java.sql.PreparedStatement
import java.sql.ResultSet

@MappedTypes(LibraryType::class)
class LibraryTypeHandler : BaseTypeHandler<LibraryType>() {
    override fun getNullableResult(p0: ResultSet, p1: String): LibraryType? {
        return LibraryType.getByValue(p0.getInt(p1))
    }

    override fun getNullableResult(p0: ResultSet, p1: Int): LibraryType? {
        return LibraryType.getByValue(p0.getInt(p1))
    }

    override fun getNullableResult(p0: CallableStatement, p1: Int): LibraryType? {
        return LibraryType.getByValue(p0.getInt(p1))
    }

    override fun setNonNullParameter(p0: PreparedStatement, p1: Int, p2: LibraryType, p3: JdbcType?) {
        p0.setInt(p1, p2.code)
    }

}

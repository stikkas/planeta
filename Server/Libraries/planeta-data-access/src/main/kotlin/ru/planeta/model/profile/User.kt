package ru.planeta.model.profile

import ru.planeta.model.enums.MartialStatus

class User : ProfileObject() {
    var friendsCount: Int = 0
    var groupsCount: Int = 0
    var usersRequestInCount: Int = 0
    var usersRequestOutCount: Int = 0
    var martialStatusCode: Int = 0

    var martialStatus: MartialStatus?
        get() = MartialStatus.Companion.getByValue(martialStatusCode)
        set(martialStatus) {
            this.martialStatusCode = if (martialStatus == null) 0 else martialStatus!!.code
        }

}

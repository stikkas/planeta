package ru.planeta.model.common.campaign

/**
 * @author m.shulepov
 * Date: 04.02.14
 */
class CampaignTag {
    var id: Int = 0
    var name: String? = null
    var engName: String? = null
    var mnemonicName: String? = null
    var preferredOrder: Int = 0
    var sponsorAlias: String? = null
    var sponsorSearchPageHtml: String? = null
    var warnText: String? = null
    var editorVisibility: Boolean? = true
    var visibleInSearch: Boolean? = true
    var charityVisibility: Boolean? = false
    var specialProject: Boolean? = false

    constructor() {}

    constructor(id: Int) {
        this.id = id
    }

    constructor(id: Int, name: String, mnemonicName: String) {
        this.id = id
        this.name = name
        this.mnemonicName = mnemonicName
    }

    override fun equals(o: Any?): Boolean {
        if (this === o) return true
        if (o !is CampaignTag) return false

        val that = o as CampaignTag?

        if (id != that!!.id) return false
        if (if (mnemonicName != null) mnemonicName != that.mnemonicName else that.mnemonicName != null) return false
        return if (if (name != null) name != that.name else that.name != null) false else true

    }

    override fun hashCode(): Int {
        var result = id
        result = 31 * result + if (name != null) name!!.hashCode() else 0
        result = 31 * result + if (mnemonicName != null) mnemonicName!!.hashCode() else 0
        return result
    }

    companion object {
        val CHARITY = "CHARITY"
        val ANOTHER = "ANOTHER"
        val INVESTING = "INVESTING"
        val CHARITY_TAG_ID = 11
    }
}

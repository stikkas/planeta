package ru.planeta.dao.commondb

import org.apache.ibatis.annotations.Mapper
import ru.planeta.model.common.campaign.CampaignTargeting

@Mapper
interface CampaignTargetingDAO {
    fun selectByCampaignId(campaignId: Long): CampaignTargeting

    fun insert(campaignTargeting: CampaignTargeting)

    fun update(campaignTargeting: CampaignTargeting)
}

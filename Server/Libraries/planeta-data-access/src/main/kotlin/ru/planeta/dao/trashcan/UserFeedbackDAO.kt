package ru.planeta.dao.trashcan

import org.apache.ibatis.annotations.Param
import ru.planeta.model.trashcan.FeedbackPageType
import ru.planeta.model.trashcan.UserFeedback

interface UserFeedbackDAO {
    fun insert(userFeedback: UserFeedback)

    fun selectUserFeedbacks(@Param("offset") offset: Int, @Param("limit") limit: Int): List<UserFeedback>

    fun selectUserFeedbackByOrderId(orderId: Long): UserFeedback

    fun selectUserFeedbackByCampaignIdAndPageType(@Param("campaignId") campaignId: Long, @Param("feedbackPageType") feedbackPageType: FeedbackPageType): UserFeedback

    fun selectUserFeedbackCount(): Int

    fun updateUserFeedback(userFeedback: UserFeedback)
}

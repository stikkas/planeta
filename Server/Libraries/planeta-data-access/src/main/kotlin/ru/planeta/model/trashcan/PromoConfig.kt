package ru.planeta.model.trashcan

import org.apache.commons.lang3.StringUtils
import ru.planeta.model.enums.PromoConfigStatus

import java.math.BigDecimal
import java.util.Arrays
import java.util.Date

class PromoConfig {
    var configId: Long = 0
    var name: String? = null
    var status = PromoConfigStatus.ACTIVE
    var campaignTags: LongArray? = null
    var timeStart: Date? = null
    var timeFinish: Date? = null
    var priceCondition: BigDecimal? = null
    var mailTemplate: String? = null
    var usageCount: Int = 0
    var isHasPromocode: Boolean = false
    private var promoCodesString: String? = null // only for save promocodes via adminka


    var creatorId: Long = 0
    var updaterId: Long = 0
    var timeAdded: Date? = null
    var timeUpdated: Date? = null

    var promoCodesList: List<String>?
        get() = if (StringUtils.isEmpty(promoCodesString)) {
            null
        } else Arrays.asList(*promoCodesString!!.split("\r\n".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray())
        set(list) {
            if (list != null && !list.isEmpty()) {
                this.promoCodesString = list.joinToString("\r\n")
            }
        }

    constructor() {}

    constructor(name: String, status: PromoConfigStatus, campaignTags: LongArray, timeStart: Date, timeFinish: Date, priceCondition: BigDecimal, mailTemplate: String, usageCount: Int, hasPromocode: Boolean) {
        this.name = name
        this.status = status
        this.campaignTags = campaignTags
        this.timeStart = timeStart
        this.timeFinish = timeFinish
        this.priceCondition = priceCondition
        this.mailTemplate = mailTemplate
        this.usageCount = usageCount
        this.isHasPromocode = hasPromocode
    }

    // we don't want to show old promocodes
    fun getPromoCodesString(): String {
        return ""
    }

    //
    fun setPromoCodesString(promoCodesString: String) {
        this.promoCodesString = promoCodesString
    }
}

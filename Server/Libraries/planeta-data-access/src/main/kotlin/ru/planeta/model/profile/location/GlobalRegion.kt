package ru.planeta.model.profile.location

import java.io.Serializable

/**
 * @author Andrew.Arefyev@gmail.com
 * Date: 04.11.13
 * Time: 23:15
 */
class GlobalRegion : BaseLocation, Serializable {

    override var locationId: Int = 0
    override var name: String? = ""
    var regionNameEn: String? = null

    override var locationType: LocationType? = null
        get() = LocationType.GLOBAL_REGION

    override var parentLocationId: Int? = 0
        get() = IGeoLocation.DEFAULT_LOCATION_ID

    override var parentLocationType: LocationType? = LocationType.GLOBAL_REGION

    constructor() {}

    constructor(regionId: Int, regionNameEn: String, regionNameRus: String) {
        this.locationId = regionId
        this.regionNameEn = regionNameEn
        this.name = regionNameRus
    }

    fun getRegionId(): Int {
        return locationId
    }

    fun setRegionId(regionId: Int) {
        this.locationId = regionId
    }

    fun getRegionNameRus(): String? {
        return name
    }

    fun setRegionNameRus(regionNameRus: String) {
        this.name = regionNameRus
    }

}

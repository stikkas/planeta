package ru.planeta.model.trashcan

import java.util.Date

class PromoCode {

    var codeId: Long = 0
    var promoConfigId: Long = 0
    var timeAdded: Date? = null
    var status: Int = 0
    var secretCode: String? = null

    companion object {

        val USED = 1
        val NOT_USED = 0
    }

}

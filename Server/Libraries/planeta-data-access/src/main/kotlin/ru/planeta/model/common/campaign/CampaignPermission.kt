package ru.planeta.model.common.campaign

import ru.planeta.model.enums.PermissionLevel

import java.util.Date

/**
 * User: a.savanovich
 * Date: 13.12.13
 * Time: 15:28
 */
class CampaignPermission {
    var campaignId: Long = 0
    var viewLevel = PermissionLevel.EVERYBODY
    var writeLevel = PermissionLevel.ADMINS
    var changeStatusLevel = PermissionLevel.ADMINS
    var timeAdded: Date? = null
    var managerId: Long = 0 // manager who set this permission

    var viewLevelCode: Int
        get() = viewLevel.code
        set(viewLevel) {
            this.viewLevel = PermissionLevel.getByValue(viewLevel)
        }

    var writeLevelCode: Int
        get() = writeLevel.code
        set(writeLevel) {
            this.writeLevel = PermissionLevel.getByValue(writeLevel)
        }

    var changeStatusLevelCode: Int
        get() = changeStatusLevel.code
        set(changeStatusLevel) {
            this.changeStatusLevel = PermissionLevel.getByValue(changeStatusLevel)
        }
}

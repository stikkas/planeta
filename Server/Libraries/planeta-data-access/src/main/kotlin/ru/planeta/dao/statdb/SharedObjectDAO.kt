package ru.planeta.dao.statdb

import org.apache.ibatis.annotations.Param
import ru.planeta.model.common.ShareServiceType
import ru.planeta.model.stat.SharedObject

/**
 * User: m.shulepov
 * Date: 23.10.12
 * Time: 16:15
 */
interface SharedObjectDAO {

    /**
     * Inserts shared object
     *
     * @param sharedObject
     */
    fun insertDetails(sharedObject: SharedObject)

    /**
     * Returns last shared object filtered by `sharedObjectId` and `shareServiceTye`
     *
     * @param sharedObjectId
     * @param shareServiceType
     * @return
     */
    fun selectLastSharedObjectDetails(@Param("sharedObjectId") sharedObjectId: Long, @Param("shareServiceType") shareServiceType: ShareServiceType): SharedObject

    /**
     * Returns shared object for `profileId` or `uuid`, filtered by specified params
     *
     * @param profileId
     * @param uuid
     * @param sharedObjectId
     * @param url
     * @param shareServiceType @return
     */
    fun selectSharedObjectDetails(@Param("profileId") profileId: Long, @Param("uuid") uuid: String?, @Param("sharedObjectId") sharedObjectId: Long, @Param("url") url: String, @Param("shareServiceType") shareServiceType: ShareServiceType): SharedObject?

}

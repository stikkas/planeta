package ru.planeta.dao.mappers.enums.shop

import org.apache.ibatis.type.JdbcType
import org.apache.ibatis.type.TypeHandler
import ru.planeta.model.shop.enums.DeliveryStatus

import java.sql.CallableStatement
import java.sql.PreparedStatement
import java.sql.ResultSet
import java.sql.SQLException

/**
 * Created by IntelliJ IDEA.
 * User: Andrew
 * Date: 16.10.12
 * Time: 20:51
 */
class DeliveryStatusHandler : TypeHandler<DeliveryStatus> {
    @Throws(SQLException::class)
    override fun setParameter(preparedStatement: PreparedStatement, i: Int, parameter: DeliveryStatus, jdbcType: JdbcType) {
        preparedStatement.setInt(i, parameter.code)
    }

    @Throws(SQLException::class)
    override fun getResult(rs: ResultSet, columnName: String): DeliveryStatus? {
        return DeliveryStatus.getByValue(rs.getInt(columnName))
    }

    @Throws(SQLException::class)
    override fun getResult(cs: CallableStatement, columnIndex: Int): DeliveryStatus? {
        return DeliveryStatus.getByValue(cs.getInt(columnIndex))
    }

    @Throws(SQLException::class)
    override fun getResult(rs: ResultSet, columnIndex: Int): DeliveryStatus? {
        return DeliveryStatus.getByValue(rs.getInt(columnIndex))
    }
}

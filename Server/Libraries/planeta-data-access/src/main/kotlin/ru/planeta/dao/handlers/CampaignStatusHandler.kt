package ru.planeta.dao.handlers

import org.apache.ibatis.type.BaseTypeHandler
import org.apache.ibatis.type.JdbcType
import org.apache.ibatis.type.MappedTypes
import ru.planeta.model.common.campaign.enums.CampaignStatus
import java.sql.CallableStatement
import java.sql.PreparedStatement
import java.sql.ResultSet

@MappedTypes(CampaignStatus::class)
class CampaignStatusHandler : BaseTypeHandler<CampaignStatus>() {

    override fun getNullableResult(p0: ResultSet, p1: String): CampaignStatus? {
        return CampaignStatus.getByValue(p0.getInt(p1))
    }

    override fun getNullableResult(p0: ResultSet, p1: Int): CampaignStatus? {
        return CampaignStatus.getByValue(p0.getInt(p1))
    }

    override fun getNullableResult(p0: CallableStatement, p1: Int): CampaignStatus? {
        return CampaignStatus.getByValue(p0.getInt(p1))
    }

    override fun setNonNullParameter(p0: PreparedStatement, p1: Int, p2: CampaignStatus, p3: JdbcType?) {
        throw NotImplementedError("not used")
    }
}

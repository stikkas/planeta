package ru.planeta.model.shop

import ru.planeta.model.shop.enums.DiscountType
import ru.planeta.model.shop.enums.PromoCodeUsageType
import java.util.Date

/**
 * User: a.savanovich
 * Date: 09.10.13
 * Time: 18:10
 */
open class PromoCode {

    var promoCodeId: Long = 0
    var title: String? = null
    var code: String? = null
    var discountType = DiscountType.ABSOLUTE        // PromoCode discount calculation type
    var discountAmount: Long = 0
    var isFreeDelivery = false
    var timeBegin: Date? = null                 // PromoCode can be used since timeBegin
    var timeEnd: Date? = null                   // PromoCode can be used till timeEnd
    var minOrderPrice: Int = 0              // PromoCode can be used if order price is greater or equals minOrderPrice
    var minProductCount: Int = 0            // PromoCode can be used if there are minProductCount (or greater) proudcts in the cart
    var codeUsageType = PromoCodeUsageType.UNLIMITED // PromoCode usage restrictions
    var usageCount = 1             // PromoCode can be used usageCount times
    var profileId: Long = 0                 // PromoCode can be used only by profileId
    var productTagId: Long = 0              // PromoCode can be used only for products with this productTag

    var timeAdded: Date? = null

    var codeUsageTypeCode: Int
        get() = codeUsageType.code
        set(code) {
            this.codeUsageType = PromoCodeUsageType.getByValue(code) ?: PromoCodeUsageType.UNLIMITED
        }

    var discountTypeCode: Int
        get() = discountType.code
        set(code) {
            this.discountType = DiscountType.getByValue(code) ?: DiscountType.ABSOLUTE
        }


    constructor() {}

    constructor(title: String) {
        this.title = title
    }

    constructor(title: String, code: String) {
        this.title = title
        this.code = code
    }

    companion object {
        val CODE_LENGTH = 12
    }
}

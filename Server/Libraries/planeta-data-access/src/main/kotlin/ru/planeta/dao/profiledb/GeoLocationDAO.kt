package ru.planeta.dao.profiledb

import org.apache.ibatis.annotations.Mapper
import org.apache.ibatis.annotations.Param
import ru.planeta.model.profile.location.City
import ru.planeta.model.profile.location.Country
import ru.planeta.model.profile.location.GlobalRegion
import ru.planeta.model.profile.location.Region

/**
 * @author a.savanovich
 */
@Mapper
interface GeoLocationDAO {

    fun globalRegions(): List<GlobalRegion>

    fun countries(): List<Country>

    fun countriesByCampaigns(): List<Country>

    fun getCountriesByGlobalRegion(globalRegionId: Int): List<Country>

    fun getCountriesByIds(countryIds: List<Int>): List<Country>

    fun selectCity(@Param("cityId") cityId: Long): City

    fun getCountryCitiesBySubstring(@Param("countryId") countryId: Long, @Param("subName") subName: String,
                                    @Param("offset") offset: Int, @Param("limit") limit: Int): List<City>

    fun getCountryRegionsBySubstring(@Param("countryId") countryId: Long, @Param("subName") subName: String,
                                     @Param("lang") lang: String, @Param("offset") offset: Int, @Param("limit") limit: Int): List<Region>

    fun getRegionCitiesBySubstring(@Param("regionId") regionId: Long, @Param("substring") substring: String,
                                   @Param("offset") offset: Int, @Param("limit") limit: Int): List<City>

    fun getCitiesWithGeo(@Param("offset") offset: Int, @Param("limit") limit: Int): List<City>

    fun selectCity(@Param("regionId") locationId: Int): List<City>

    fun insertNewCity(city: City)

    fun selectRegion(@Param("regionId") regionId: Long): Region

    fun selectRegion(@Param("countryId") countryId: Int): List<Region>

    fun getGlobalRegionById(regionId: Int): GlobalRegion
}

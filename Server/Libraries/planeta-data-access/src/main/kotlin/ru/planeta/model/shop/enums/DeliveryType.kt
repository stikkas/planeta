package ru.planeta.model.shop.enums

import ru.planeta.model.enums.Codable

import java.util.HashMap

/**
 * Enum with delivery types
 * User: a.savanovich
 * Date: 22.06.12
 * Time: 11:54
 */
enum class DeliveryType private constructor(override val code: Int) : Codable {
    NOT_SET(0),
    RUSSIAN_POST(3),
    CUSTOMER_PICKUP(5),
    DELIVERY(7);


    companion object {

        private val lookup = HashMap<Int, DeliveryType>()

        init {
            for (s in values()) {
                lookup[s.code] = s
            }
        }

        fun getByValue(code: Int): DeliveryType? {
            return lookup[code]
        }
    }
}

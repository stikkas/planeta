package ru.planeta.dao.mappers.enums

import org.apache.ibatis.type.JdbcType
import org.apache.ibatis.type.TypeHandler
import ru.planeta.model.enums.CustomMetaTagType

import java.sql.CallableStatement
import java.sql.PreparedStatement
import java.sql.ResultSet
import java.sql.SQLException

class CustomMetaTagTypeHandler : TypeHandler<CustomMetaTagType> {
    @Throws(SQLException::class)
    override fun setParameter(ps: PreparedStatement, i: Int, parameter: CustomMetaTagType, jdbcType: JdbcType?) {
        ps.setInt(i, parameter.code)
    }

    @Throws(SQLException::class)
    override fun getResult(rs: ResultSet, columnName: String): CustomMetaTagType? {
        return CustomMetaTagType.getByCode(rs.getInt(columnName))
    }

    @Throws(SQLException::class)
    override fun getResult(rs: ResultSet, columnIndex: Int): CustomMetaTagType? {
        return CustomMetaTagType.getByCode(rs.getInt(columnIndex))
    }

    @Throws(SQLException::class)
    override fun getResult(cs: CallableStatement, columnIndex: Int): CustomMetaTagType? {
        return CustomMetaTagType.getByCode(cs.getInt(columnIndex))
    }
}

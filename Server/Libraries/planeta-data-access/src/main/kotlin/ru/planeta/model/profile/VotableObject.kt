package ru.planeta.model.profile

import ru.planeta.model.enums.ObjectType

import java.io.Serializable

/**
 * Date: 21.11.12
 *
 * @author s.kalmykov
 */
open class VotableObject : Serializable, IVotable, Identifier {

    override var authorProfileId: Long

    override var voteObjectId: Long

    override var voteObjectType: ObjectType?

    override var profileId: Long? = null

    override var objectId: Long? = null
        get() = voteObjectId

    override var objectType: ObjectType? = null
        get() = voteObjectType

    override val id: Long
        get() = voteObjectId

    constructor() {
        this.profileId = -1
        this.voteObjectType = ObjectType.AUDIOTRACK
        this.voteObjectId = -1
        this.authorProfileId = -1
    }

    constructor(profileId: Long, objectId: Long, objectType: ObjectType) {
        this.profileId = profileId
        this.voteObjectId = objectId
        this.voteObjectType = objectType
        this.authorProfileId = -1
    }

    constructor(obj: IVotable) {
        this.profileId = obj.profileId
        this.voteObjectId = obj.voteObjectId
        this.voteObjectType = obj.voteObjectType
        this.authorProfileId = obj.authorProfileId
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as VotableObject

        if (voteObjectId != other.voteObjectId) return false
        if (profileId != other.profileId) return false

        return true
    }

    override fun hashCode(): Int {
        var result = voteObjectId.hashCode()
        result = 31 * result + (profileId?.hashCode() ?: 0)
        return result
    }


}

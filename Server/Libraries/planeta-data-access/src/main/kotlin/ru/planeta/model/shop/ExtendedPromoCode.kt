package ru.planeta.model.shop

/**
 * Created with IntelliJ IDEA.
 * User: michail
 * Date: 06.04.16
 * Time: 16:22
 * To change this template use File | Settings | File Templates.
 */
// TODO move to base class. Make fields private
class ExtendedPromoCode : PromoCode() {
    internal var productTag = ""
    internal var profileDisplayName = ""
    var isDisabled = false

    fun getProfileDisplayName(): String {
        return profileDisplayName
    }

    fun setProfileDisplayName(profileDisplayName: String?) {
        this.profileDisplayName = profileDisplayName ?: ""
    }

    fun getProductTag(): String {
        return productTag
    }

    fun setProductTag(productTag: String?) {
        this.productTag = productTag ?: ""
    }
}

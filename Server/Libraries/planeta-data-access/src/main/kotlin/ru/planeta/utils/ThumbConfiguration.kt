package ru.planeta.utils

import ru.planeta.model.enums.ThumbnailType

/**
 * Represents thumbnail configuration.
 * ImageUploaderService generates thumbnail for each thumb configuration.
 * TODO: Implement equals and hashCode
 *
 * @author ameshkov
 */
class ThumbConfiguration {

    /**
     * Returns thumbnail's name
     * TODO: Generate name if it is not set
     *
     * @return
     */
    /**
     * Thumbnail's file name.
     */
    var name: String? = null
    /**
     * Returns thumbnail width
     *
     * @return
     */
    /**
     * Sets thumbnail width
     *
     * @param width
     */
    var width: Int = 0
    /**
     * Gets thumbnail height
     *
     * @return
     */
    /**
     * Sets thumbnail height
     *
     * @param height
     */
    var height: Int = 0
    var isCrop = false
    var isDisableAnimatedGif = false
    // set black top and bottom PADdings if video and thumb have different aspect ratios
    // if set, ignores crop, resizes image to have one side equal to thumb, another side padded with black color
    var isPad = false
    var backgroundColor: String? = "black"

    val isBackgroundColorSet: Boolean
        get() = backgroundColor != null && "black" != backgroundColor

    constructor() {}

    constructor(thumbConfiguration: ThumbConfiguration) {
        this.name = thumbConfiguration.name
        this.width = thumbConfiguration.width
        this.height = thumbConfiguration.height
        this.isCrop = thumbConfiguration.isCrop
        this.isDisableAnimatedGif = thumbConfiguration.isDisableAnimatedGif
        this.isPad = thumbConfiguration.isPad
        this.backgroundColor = thumbConfiguration.backgroundColor
    }

    /**
     * Constructs ThumbConfiguration from ThumbnailType
     *
     * @param thumbnailType
     */
    constructor(thumbnailType: ThumbnailType?) {
        if (thumbnailType != null) {
            this.name = thumbnailType.thumbFileName
            this.width = thumbnailType.width
            this.height = thumbnailType.height
            this.isCrop = thumbnailType.isCrop
            this.isDisableAnimatedGif = thumbnailType.isDisableAnimatedGif
        }
    }
}

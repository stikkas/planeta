package ru.planeta.dao.profiledb

import org.apache.ibatis.annotations.Mapper
import org.apache.ibatis.annotations.Param
import ru.planeta.model.enums.BlockSettingType
import ru.planeta.model.profile.ProfileBlockSetting

/**
 * Interface ProfileBlockSettingDAO
 *
 * @author a.tropnikov
 */
@Mapper
interface ProfileBlockSettingDAO {

    /**
     * Selects list of ProfileBlockSetting by profile identifier
     *
     * @param profileId
     * @return
     */
    fun selectByProfileId(profileId: Long): List<ProfileBlockSetting>

    fun select(@Param("profileId") profileId: Long, @Param("blockSettingType") blockSettingType: BlockSettingType): ProfileBlockSetting

    fun insert(setting: ProfileBlockSetting)

    fun update(setting: ProfileBlockSetting)

    fun delete(@Param("profileId") profileId: Long, @Param("blockSettingType") blockSettingType: BlockSettingType)
}

package ru.planeta.model.promo

import java.util.*

/**
 * Created with IntelliJ IDEA.
 * User: michail
 * Date: 18.11.2016
 * Time: 12:15
 */
open class TechnobattleRegistration {

    var registrationId: Long = 0
    var email: String? = null
    var type = Type.LETTER
    var name: String? = null            // only for registration
    var phone: String? = null           // only for registration
    var city: String? = null            // only for registration
    var description: String? = null     // only for registration
    var requestMethod: String? = null   // only for registration
    var timeAdded: Date? = null
    var voteType: VoteType? = null

    enum class Type {
        LETTER, REGISTRAION, SUBSCRIPTION, BONUS
    }

    constructor()

    // for letter registration - need only email, others myst be empty
    constructor(email: String) {
        this.email = email
        this.type = Type.LETTER
        this.name = ""
        this.phone = ""
        this.city = ""
        this.description = ""
    }

    constructor(email: String, type: Type) {
        this.email = email
        this.type = type
        this.name = ""
        this.phone = ""
        this.city = ""
        this.description = ""
    }

    constructor(email: String, type: Type, voteType: VoteType) : this(email, type) {
        this.voteType = voteType
    }
}

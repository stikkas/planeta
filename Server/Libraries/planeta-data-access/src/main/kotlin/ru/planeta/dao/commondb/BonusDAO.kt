package ru.planeta.dao.commondb

import org.apache.ibatis.annotations.Mapper
import org.apache.ibatis.annotations.Param
import ru.planeta.model.common.loyalty.Bonus
import ru.planeta.model.common.loyalty.BonusPriorityItem

/**
 * Bonus DAO
 *
 *
 * User: m.shulepov
 * Date: 03.04.14
 */
@Mapper
interface BonusDAO {

    /**
     * Select bonuses
     *
     * @param offset
     * @param limit
     * @return
     */
    fun selectBonuses(@Param("offset") offset: Int, @Param("limit") limit: Int): List<Bonus>

    fun selectAllBonuses(@Param("offset") offset: Int, @Param("limit") limit: Int): List<Bonus>

    /**
     * Select bonus by id
     *
     * @param bonusId
     * @return
     */
    fun select(bonusId: Long): Bonus?

    /**
     * Insert new bonus
     *
     * @param bonus
     */
    fun insert(bonus: Bonus)

    /**
     * Update specified bonus
     *
     * @param bonus
     */
    fun update(bonus: Bonus)

    fun updateBonusesOrder(bonusPriorityItems: List<BonusPriorityItem>)
}

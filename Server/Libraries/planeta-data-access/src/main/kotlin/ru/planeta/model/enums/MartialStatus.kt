package ru.planeta.model.enums

import java.util.*

/**
 * Enum MartialStatus
 *
 * @author a.tropnikov
 */
enum class MartialStatus private constructor(val code: Int) {

    NOT_SET(0), NOT_MARRIED(1), IN_RELATIONS(2), BUSY(3), MARRIED(4), FREE(5);


    companion object {

        private val lookup = HashMap<Int, MartialStatus>()

        init {
            for (s in EnumSet.allOf(MartialStatus::class.java)) {
                lookup[s.code] = s
            }
        }

        fun getByValue(code: Int): MartialStatus? {
            return lookup[code]
        }
    }
}

package ru.planeta.model.common.campaign

/**
 * This enum represents affiliate stats' aggregation type
 *
 * @author m.shulepov
 * Date: 07.08.13
 */
enum class AggregationType {
    GENERAL, REFERER, CITY, COUNTRY, DATE, SOURCE
}

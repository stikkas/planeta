package ru.planeta.model.common.campaign

import org.apache.commons.collections4.IterableUtils
import org.apache.commons.lang3.StringUtils
import ru.planeta.model.common.Address
import ru.planeta.model.common.delivery.BaseDelivery
import ru.planeta.model.common.delivery.LinkedDelivery

/**
 * User: atropnikov
 * Date: 02.04.12
 * Time: 14:46
 */
open class ShareDetails : Share {

    var storedAddress: Address = Address()

    open var linkedDeliveries: List<LinkedDelivery>? = null

    open var address: Address
        get() {
            val delivery = IterableUtils.find(linkedDeliveries) { delivery -> delivery.serviceId == BaseDelivery.DEFAULT_CUSTOMER_PICKUP_SERVICE_ID }

            return delivery?.address ?: Address()
        }
        set(address) {
            this.storedAddress = address
        }

    var country: String?
        get() = address.country
        set(country) {
            storedAddress.country = StringUtils.trim(country)
        }

    var zipCode: String?
        get() = address.zipCode
        set(zipCode) {
            storedAddress.zipCode = StringUtils.trim(zipCode)
        }

    open var city: String?
        get() = address.city
        set(city) {
            storedAddress.city = StringUtils.trim(city)
        }

    open var street: String?
        get() = address.street
        set(street) {
            storedAddress.street = StringUtils.trim(street)
        }

    open var phone: String?
        get() = address.phone
        set(phone) {
            storedAddress.phone = StringUtils.trim(phone)
        }

    constructor() {}

    constructor(share: Share) : super(share) {}


    fun setCountryId(countryId: Int) {
        storedAddress.countryId = countryId
    }

}

package ru.planeta.model.enums

import java.util.HashMap

/**
 * User: a.savanovich
 * Date: 20.11.13
 * Time: 13:14
 */
enum class ContractorType private constructor(override val code: Int) : Codable {
    INDIVIDUAL(0),
    LEGAL_ENTITY(1),
    INDIVIDUAL_ENTREPRENEUR(2),
    OOO(3),
    CHARITABLE_FOUNDATION(4);


    companion object {

        private val lookup = HashMap<Int, ContractorType>()

        init {
            for (s in values()) {
                lookup[s.code] = s
            }
        }

        fun getByValue(code: Int): ContractorType? {
            return lookup[code]
        }

        fun fromString(json: String): ContractorType {
            var type = INDIVIDUAL
            for (t in values()) {
                if (t.name == json) {
                    type = t
                    break
                }
            }
            return type
        }
    }
}

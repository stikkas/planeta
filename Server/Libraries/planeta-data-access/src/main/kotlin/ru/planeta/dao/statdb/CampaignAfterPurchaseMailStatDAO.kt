package ru.planeta.dao.statdb

import org.apache.ibatis.annotations.Mapper
import org.apache.ibatis.annotations.Param
import ru.planeta.model.stat.CampaignAfterPurchaseMailStat

// TODO none xml files
// TODO to deleteByProfileId
@Mapper
interface CampaignAfterPurchaseMailStatDAO {
    fun insert(campaignAfterPurchaseMailStat: CampaignAfterPurchaseMailStat)

    fun update(campaignAfterPurchaseMailStat: CampaignAfterPurchaseMailStat)

    fun selectByUserId(userId: Long): CampaignAfterPurchaseMailStat

    fun selectUsersForSendingMail(@Param("offset") offset: Long, @Param("limit") limit: Long): List<CampaignAfterPurchaseMailStat>
}

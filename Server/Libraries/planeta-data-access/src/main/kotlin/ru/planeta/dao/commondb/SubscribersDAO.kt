package ru.planeta.dao.commondb

import org.apache.ibatis.annotations.Mapper
import org.apache.ibatis.annotations.Param

/**
 * DAO for commondb.subscribers
 */
@Mapper
interface SubscribersDAO {

    /**
     * Inserts new subscriber
     *
     * @param subscriptionCode
     * @param email
     */
    fun insert(@Param("subscriptionCode") subscriptionCode: String, @Param("email") email: String)

    /**
     * Checks is exist record with presented parameters.
     *
     * @param subscriptionCode subscription code;
     * @param email            user email;
     * @return `true` if record exists, `false` otherwise.
     */
    fun isSubscribed(@Param("subscriptionCode") subscriptionCode: String, @Param("email") email: String): Boolean
}

package ru.planeta.model.shop

import ru.planeta.model.shop.enums.ProductTagType

import java.io.Serializable

/**
 * User: m.shulepov
 */
class Category : Serializable {

    var categoryId: Long = 0
    var value: String? = null
    var mnemonicName: String? = null
    var tagType: ProductTagType? = null
    var preferredOrder: Int = 0

    var tagTypeId: Int
        get() = tagType?.code ?: 0
        set(tagTypeId) {
            this.tagType = ProductTagType.getByValue(tagTypeId)
        }

    constructor() {}

    constructor(categoryId: Long) {
        this.categoryId = categoryId
    }

    constructor(value: String, categoryId: Long) {
        this.value = value
        this.categoryId = categoryId
    }

    constructor(value: String, categoryId: Long, mnemonicName: String) {
        this.value = value
        this.categoryId = categoryId
        this.mnemonicName = mnemonicName
    }

    override fun equals(o: Any?): Boolean {
        if (this === o) return true
        if (o !is Category) return false

        val that = o as Category?

        if (categoryId != that!!.categoryId) return false
        if (if (mnemonicName != null) mnemonicName != that.mnemonicName else that.mnemonicName != null) return false
        return if (if (value != null) value != that.value else that.value != null) false else true

    }

    override fun hashCode(): Int {
        var result = categoryId.toInt()
        result = 31 * result + if (value != null) value!!.hashCode() else 0
        result = 31 * result + if (mnemonicName != null) mnemonicName!!.hashCode() else 0
        return result
    }

    companion object {
        val NO_WORLD_DELIVERY_CATEGORY = "RU_DELIVERY"              // hack for shop
        val NO_RUSSIAN_POST_CATEGORY = "NO_RUSSIAN_POST_DELIVERY"   // hack for shop

        val NO_FREE_DELIVERY = "NO_FREE_DELIVERY"   // hack for shop
    }
}

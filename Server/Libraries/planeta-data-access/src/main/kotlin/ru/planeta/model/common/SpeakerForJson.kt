package ru.planeta.model.common

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import ru.planeta.model.commondb.Speaker

@JsonIgnoreProperties(ignoreUnknown = true)
class SpeakerForJson : Speaker()

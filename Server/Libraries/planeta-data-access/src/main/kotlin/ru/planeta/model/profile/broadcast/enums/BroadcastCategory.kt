package ru.planeta.model.profile.broadcast.enums

import java.util.HashMap

/**
 * User: s.makarov
 * Date: 14.01.14
 * Time: 12:52
 */
enum class BroadcastCategory private constructor(val code: Int) {
    CONCERTS(0), NASHE_360(1), ALIVE(2), SCHOOL(3);

    companion object {

        private val lookup = HashMap<Int, BroadcastCategory>()

        init {
            for (c in values()) {
                lookup[c.code] = c
            }
        }

        fun getByValue(code: Int?): BroadcastCategory? {
            return lookup[code]
        }
    }

}

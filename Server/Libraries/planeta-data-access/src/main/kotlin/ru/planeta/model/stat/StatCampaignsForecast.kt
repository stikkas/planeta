package ru.planeta.model.stat

import java.math.BigDecimal
import java.util.Date

/**
 * Date: 21.08.12
 * Time: 15:30
 */
class StatCampaignsForecast {
    var campaignId: Long = 0
    var purchaseCount: Int = 0
    var timeStart: Date? = null
    var timeFinish: Date? = null
    var name: String? = null
    var amount = BigDecimal.ZERO
    var targetAmount = BigDecimal.ZERO
    var planedAmount = BigDecimal.ZERO
    var neededRevenue = BigDecimal.ZERO
    var revenuePerDay = BigDecimal.ZERO
}

package ru.planeta.dao.bibliodb

import org.apache.ibatis.annotations.Mapper
import org.apache.ibatis.annotations.Param
import ru.planeta.model.bibliodb.MagazinePriceList

@Mapper
interface MagazinePriceListDAO {
    fun insert(magazinePriceList: MagazinePriceList)

    fun update(magazinePriceList: MagazinePriceList)

    fun selectList(@Param("offset") offset: Long, @Param("limit") limit: Long): List<MagazinePriceList>

    fun select(priceListId: Long): MagazinePriceList
}


package ru.planeta.model.trashcan

import java.util.Date

/**
 * Для сохранения данных редактируемых проектов
 * User: Serge Blagodatskih<stikkas17></stikkas17>@gmail.com><br></br>
 * Date: 16.11.16<br></br>
 * Time: 16:03
 */
class CampaignDraft {

    var id: Long = 0
    var campaignId: Long = 0
    var profileId: Long = 0
    var timeAdded: Date? = null
    var data: String? = null

    constructor()

    constructor(campaignId: Long, profileId: Long, timeAdded: Date, data: String) {
        this.campaignId = campaignId
        this.profileId = profileId
        this.timeAdded = timeAdded
        this.data = data
    }

}

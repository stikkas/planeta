package ru.planeta.dao.commondb

import org.apache.ibatis.annotations.Mapper
import org.apache.ibatis.annotations.Param
import ru.planeta.model.common.campaign.CampaignEditTime

/**
 *
 * User: Serge Blagodatskih<stikkas17></stikkas17>@gmail.com><br></br>
 * Date: 22.03.16<br></br>
 * Time: 16:32
 */
@Mapper
interface CampaignEditTimeDAO {

    /**
     * Add record to table
     * @param campaignId ID of campaign
     * @param profileId ID of editor that campaign
     * @return
     */
    fun insert(@Param("campaignId") campaignId: Long, @Param("profileId") profileId: Long): CampaignEditTime

    /**
     * Update record in table
     * @param campaignId ID of campaign
     * @param profileId ID of editor that campaign
     * @return
     */
    fun update(@Param("campaignId") campaignId: Long, @Param("profileId") profileId: Long): CampaignEditTime

    /**
     * Remove record from table
     * @param campaignId ID of campaign
     * @param profileId ID of editor that campaign
     */
    fun delete(@Param("campaignId") campaignId: Long, @Param("profileId") profileId: Long)

    /**
     * Find last record about editing campaign other user
     * @param campaignId ID of campaign
     * @param profileId ID of editor that campaign
     * @return record if exists or null
     */
    fun selectLast(@Param("campaignId") campaignId: Long, @Param("profileId") profileId: Long): CampaignEditTime

    /**
     * Check if record exists in table
     * @param campaignId ID of campaign
     * @param profileId ID of editor that campaign
     * @return true if exists instead false
     */
    fun checkIfExists(@Param("campaignId") campaignId: Long, @Param("profileId") profileId: Long): Boolean

}

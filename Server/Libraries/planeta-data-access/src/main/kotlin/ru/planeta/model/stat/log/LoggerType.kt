package ru.planeta.model.stat.log

import java.util.HashMap

/**
 * Log type.<br></br>
 * User: eshevchenko
 */
enum class LoggerType private constructor(val code: Int) {
    PAYMENT(1),
    ORDER(2),
    ADMIN_AUDIT(3);


    companion object {

        private val lookup = HashMap<Int, LoggerType>()

        init {
            for (logType in LoggerType.values()) {
                lookup[logType.code] = logType
            }
        }

        fun getByCode(code: Int): LoggerType? {
            return lookup[code]
        }
    }
}

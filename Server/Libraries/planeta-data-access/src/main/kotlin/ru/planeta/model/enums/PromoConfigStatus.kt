package ru.planeta.model.enums

import java.util.*

enum class PromoConfigStatus private constructor(override val code: Int) : Codable {
    PAUSED(0),
    ACTIVE(1),
    FINISHED(2);


    companion object {
        private val lookup = HashMap<Int, PromoConfigStatus>()

        init {
            for (s in EnumSet.allOf(PromoConfigStatus::class.java)) {
                lookup[s.code] = s
            }
        }

        fun getByValue(code: Int): PromoConfigStatus? {
            return lookup[code]
        }
    }
}

package ru.planeta.model.stat

import ru.planeta.model.enums.Codable

import java.util.HashMap

/**
 *
 * Created by a.savanovich on 31.10.2016.
 */
enum class RefererStatType private constructor(override val code: Int) : Codable {
    LINK(1), REGISTRATION(2), VOTE(3), CAMPAIGN_CREATE(4);


    companion object {

        private val lookup = HashMap<Int, RefererStatType>()

        init {
            for (s in values()) {
                lookup[s.code] = s
            }
        }

        fun getByValue(code: Int): RefererStatType? {
            return lookup[code]
        }
    }

}

package ru.planeta.dao.commondb

import org.apache.ibatis.annotations.Mapper
import ru.planeta.model.common.AuthToken

/**
 * @see ru.planeta.model.common.AuthToken
 */
@Mapper
interface AuthTokenRepository {

    fun findByToken(token: String): AuthToken?

    fun delete(token: AuthToken)

    fun save(token: AuthToken)

}

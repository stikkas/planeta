package ru.planeta.dao.commondb

import org.apache.ibatis.annotations.Param
import org.springframework.stereotype.Service
import java.util.ArrayList
import ru.planeta.model.commondb.Partner
import ru.planeta.model.commondb.PartnerExample

@Service
class BasePartnerService(private var partnerMapper: PartnerMapper) {

    val orderByClause: String
        get() = "partner_id"

    //**************************************************************************************************
//*********************************  Create ** Update ** Delete  *************************************
//**************************************************************************************************
    fun insertPartner(partner: Partner) {


        partnerMapper.insertSelective(partner)
    }

    fun insertPartnerAllFields(partner: Partner) {


        partnerMapper.insert(partner)
    }

    fun updatePartner(partner: Partner) {

        partnerMapper.updateByPrimaryKeySelective(partner)
    }

    fun updatePartnerAllFields(partner: Partner) {

        partnerMapper.updateByPrimaryKey(partner)
    }

    fun insertOrUpdatePartner(partner: Partner) {

        val selectedPartner = partnerMapper.selectByPrimaryKey(partner.partnerId)
        if (selectedPartner == null) {

            partnerMapper.insertSelective(partner)
        } else {
            partnerMapper.updateByPrimaryKeySelective(partner)
        }
    }

    fun insertOrUpdatePartnerAllFields(partner: Partner) {

        val selectedPartner = partnerMapper.selectByPrimaryKey(partner.partnerId)
        if (selectedPartner == null) {

            partnerMapper.insert(partner)
        } else {
            partnerMapper.updateByPrimaryKey(partner)
        }
    }

    fun deletePartner(id: Long?) {
        partnerMapper.deleteByPrimaryKey(id)
    }

    //**************************************************************************************************
//******************************************** partner ***********************************************
//************************************** SelectByPrimaryKey ******************************************
//**************************************************************************************************
    fun selectPartner(partnerId: Long?): Partner {
        return partnerMapper.selectByPrimaryKey(partnerId)
    }

    //**************************************************************************************************
//******************************************** partner ***********************************************
//**************************************************************************************************
    private fun COMMENT_SECTION_Partner() {

    }

    fun getPartnerExample(@Param("offset") offset: Int, @Param("limit") limit: Int): PartnerExample {
        val example = PartnerExample()
        example.offset = offset
        example.limit = limit
        example.or()
        example.orderByClause = orderByClause
        return example
    }

    fun selectPartnerCount(): Int {
        return partnerMapper.countByExample(getPartnerExample(0, 0))
    }

    fun selectPartnerList(@Param("offset") offset: Int, @Param("limit") limit: Int): List<Partner> {
        return partnerMapper.selectByExample(getPartnerExample(offset, limit))
    }

    fun selectPartnerList(partnerIdList: List<Long>?): List<Partner> {
        return if (partnerIdList == null || partnerIdList.isEmpty()) ArrayList() else partnerMapper.selectByIdList(partnerIdList)
    }
}

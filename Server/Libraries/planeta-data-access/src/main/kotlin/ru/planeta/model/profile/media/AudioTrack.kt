package ru.planeta.model.profile.media

import ru.planeta.model.enums.ObjectType
import ru.planeta.model.profile.ProfileObject

import java.sql.Date

/**
 * @author a.savanovich
 */
class AudioTrack : ProfileObject(), AudioStats {

    var trackId: Long = 0
    var uploaderTrackId: Long = 0
    var uploaderProfileId: Long = 0
    var albumId: Long = 0
    var uploaderAlbumId: Long = 0
    var trackName: String = ""
    override var artistName: String? = null
    var authors: String? = null
    var lyrics: String? = null
    var trackDuration: Int = 0
    var trackUrl: String? = null
    var downloadsCount: Int = 0
    var listeningsCount: Int = 0
    var isHasMp3: Boolean = false
    var albumTrackNumber: Int = 0
    var timeAdded: Date? = null

    override var objectId: Long? = null
        get() = trackId

    override var name: String? = null
        get() = trackName

    override var voteObjectType: ObjectType?
        get() = ObjectType.AUDIOTRACK
        set(value: ObjectType?) {
            super.voteObjectType = value
        }

    companion object {

        private val serialVersionUID = 1L
    }
}

package ru.planeta.model.concert

/**
 * Created with IntelliJ IDEA.
 * User: michail
 * Date: 14.10.16
 * Time: 11:57
 */
class Hall {
    var hallId: Long = 0
    var title: String? = null
    var description: String? = null
    var address: String? = null
    var schemeUrl: String? = null
}

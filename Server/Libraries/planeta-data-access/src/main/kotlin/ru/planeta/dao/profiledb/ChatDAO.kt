package ru.planeta.dao.profiledb

import org.apache.ibatis.annotations.Mapper
import org.apache.ibatis.annotations.Param
import ru.planeta.model.profile.chat.Chat

/**
 * Data access object for working with Chats.
 *
 * @author m.shulepov
 */
@Mapper
interface ChatDAO {

    /**
     * Selects chat by it's identifier
     */
    fun select(@Param("profileId") profileId: Long, @Param("chatId") chatId: Long): Chat

    /**
     * Selects chat by it's ownerObjectId
     */
    fun selectByOwnerObject(@Param("profileId") profileId: Long, @Param("ownerObjectId") ownerObjectId: Long): Chat

    /**
     * Inserts new chat to the database table
     */
    fun insert(chat: Chat)

}

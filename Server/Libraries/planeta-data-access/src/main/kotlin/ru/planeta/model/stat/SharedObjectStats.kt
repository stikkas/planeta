package ru.planeta.model.stat

import java.util.Date

/**
 * This class contains aggregated meta info of shared object
 * User: m.shulepov
 */
class SharedObjectStats {

    var sharedObjectId: Long = 0
    var url: String? = null
    var count: Long = 0
    var timeAdded: Date? = null
    var timeUpdated: Date? = null

    constructor() {}

    constructor(url: String) {
        this.url = url
    }

}

package ru.planeta.model.promo

/**
 * User: michail
 * Date: 16.01.2017
 * Time: 18:22
 */
class TechnobattleProject {
    var projectId: Long = 0
    var title: String? = null
    var smallImageUrl: String? = null
    var imageUrl: String? = null
    var consumers: String? = null
    var team: String? = null
    var city: String? = null
    var description: String? = null
    var votesCount: Int = 0
    var profileId: Long = 0
    var campaignId: Long = 0
    var campaignAlias: String? = null
    var videoSrc: String? = null
    var sharingTitle: String? = null
    var sharingImageUrl: String? = null
    var status: TechnobattleProjectStatus? = null

    var statusCode: Int
        get() = status?.code ?: 0
        set(code) {
            this.status = TechnobattleProjectStatus.getByValue(code)
        }
}

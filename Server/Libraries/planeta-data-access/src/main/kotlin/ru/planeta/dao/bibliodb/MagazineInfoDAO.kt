package ru.planeta.dao.bibliodb

import org.apache.ibatis.annotations.Mapper
import org.apache.ibatis.annotations.Param
import ru.planeta.model.bibliodb.MagazineInfo

@Mapper
interface MagazineInfoDAO {
    fun update(magazineInfo: MagazineInfo)

    fun insert(magazineInfo: MagazineInfo)

    fun selectList(@Param("priceListId") priceListId: Long, @Param("offset") offset: Long,
                   @Param("limit") limit: Long): List<MagazineInfo>
}

package ru.planeta.model.stat

import ru.planeta.model.common.ShareServiceType

import java.util.Date

/**
 * This class represents single instance of shared object
 * <pre>
 * Note: Not all properties of the class are filled properly when returned from methods
 * Some of them might be missing
</pre> *
 * User: m.shulepov
 */
class SharedObject {

    var sharedObjectId: Long = 0
    var authorProfileId: Long = 0 /* id of the user, who shared the object */
    var authorUUID: String? = null    /* generated unique id */
    var url: String? = null
    var shareServiceType: ShareServiceType? = null
    var timeAdded: Date? = null

}

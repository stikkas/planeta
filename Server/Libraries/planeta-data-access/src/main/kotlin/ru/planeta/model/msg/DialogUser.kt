package ru.planeta.model.msg

import ru.planeta.model.enums.ProfileType
import ru.planeta.model.profile.Profile

import java.io.Serializable

/**
 * Represents user that participates in a dialog
 *
 * @author ameshkov
 */
class DialogUser : Profile, IDialogObject, Serializable {

    override var dialogId: Long = 0

    constructor() {}

    constructor(code: Int, profileType: ProfileType) : super(code, profileType) {}
}

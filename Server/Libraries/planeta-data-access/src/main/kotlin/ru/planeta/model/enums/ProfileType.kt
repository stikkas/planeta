package ru.planeta.model.enums

import java.util.EnumSet
import java.util.HashMap

/**
 * Enum ProfileType
 *
 * @author a.tropnikov
 */
enum class ProfileType private constructor(override val code: Int) : Codable {

    USER(1),
    GROUP(2),
    @Deprecated("")
    EVENT(3),
    HIDDEN_GROUP(4);


    companion object {
        private val lookup = HashMap<Int, ProfileType>()

        init {
            for (s in EnumSet.allOf(ProfileType::class.java)) {
                lookup[s.code] = s
            }
        }

        fun getByValue(code: Int): ProfileType {
            return lookup[code] ?: USER
        }
    }
}


package ru.planeta.model.common.auth

import com.fasterxml.jackson.annotation.JsonIgnore

import java.io.Serializable

/**
 * Date: 12.09.12
 * Links existing user profile with some external username (usually unique user id)
 *
 * @author s.kalmykov
 */
open class UserCredentials : Serializable {


    var profileId: Long = 0
    @Transient
    @get:JsonIgnore
    @set:JsonIgnore
    var externalAuthentication = ExternalAuthentication()

    /**
     * Set external login of some credentialType
     */
    var userName: String?
        get() = externalAuthentication.username
        set(userName) {
            externalAuthentication.username = userName
        }

    var credentialType: CredentialType?
        get() = externalAuthentication.credentialType
        set(type) {
            externalAuthentication.credentialType = type
        }
}

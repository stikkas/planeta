package ru.planeta.dao.mappers

import org.apache.ibatis.type.BaseTypeHandler
import org.apache.ibatis.type.JdbcType
import org.apache.ibatis.type.MappedTypes

import java.sql.CallableStatement
import java.sql.PreparedStatement
import java.sql.ResultSet
import java.sql.SQLException

/**
 * Created with IntelliJ IDEA.
 * User: user
 * Date: 12.08.13
 * Time: 18:50
 * To change this template use File | Settings | File Templates.
 */
@MappedTypes(String::class)
class EscapeStringTypeHandler : BaseTypeHandler<String>() {

    @Throws(SQLException::class)
    override fun setNonNullParameter(ps: PreparedStatement, i: Int, parameter: String, jdbcType: JdbcType) {
        var parameter = parameter
        parameter = cutBadCharacters(parameter)
        ps.setString(i, parameter)
    }

    @Throws(SQLException::class)
    override fun getNullableResult(rs: ResultSet, columnName: String): String {
        return rs.getString(columnName)
    }

    @Throws(SQLException::class)
    override fun getNullableResult(rs: ResultSet, columnIndex: Int): String {
        return rs.getString(columnIndex)
    }

    @Throws(SQLException::class)
    override fun getNullableResult(cs: CallableStatement, columnIndex: Int): String {
        return cs.getString(columnIndex)
    }

    private fun cutBadCharacters(inputString: String): String {
        var inputString = inputString
        for (badCharacter in BAD_CHARACTERS_ARRAY) {
            inputString = inputString.replace(badCharacter, "")
        }

        return inputString
    }

    companion object {

        val BAD_CHARACTERS_ARRAY = arrayOf("\u2028", "\u2029")
    }
}

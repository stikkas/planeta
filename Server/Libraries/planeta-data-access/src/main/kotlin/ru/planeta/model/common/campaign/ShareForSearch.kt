package ru.planeta.model.common.campaign

/**
 *
 * Created by asavan on 02.08.2017.
 */
class ShareForSearch : Share() {
    var shareOrCampaignImageUrl: String? = null
    var campaignName: String? = null
    var campaignTags: List<CampaignTag>? = null
}

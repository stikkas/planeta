package ru.planeta.dao.payment

import org.apache.ibatis.annotations.Mapper
import org.apache.ibatis.annotations.Param
import ru.planeta.model.common.PaymentMethod
import ru.planeta.model.enums.ProjectType

/**
 * Created with IntelliJ IDEA.
 * User: a.volkov
 * Date: 24.04.14
 * Time: 12:47
 */
@Mapper
interface PaymentMethodDAO {

    val paymentMethods: List<PaymentMethod>

    fun select(id: Long): PaymentMethod

    fun update(method: PaymentMethod): PaymentMethod

    fun updateProjectPaymentMethod(@Param("projectType") projectType: ProjectType, @Param("paymentMethodId") paymentMethodId: Long, @Param("enabled") enabled: Boolean)

    fun getPaymentMethodsForProject(@Param("projectType") projectType: ProjectType, @Param("internal") internal: Boolean?, @Param("promo") promo: Boolean?): List<PaymentMethod>

    fun getPaymentMethodsForProject(@Param("projectType") projectType: ProjectType, @Param("internal") internal: Boolean?, @Param("promo") promo: Boolean?, @Param("old") old: Boolean?): List<PaymentMethod>

    fun getInternalPaymentMethod(projectType: ProjectType): PaymentMethod

    fun getPromoPaymentMethod(projectType: ProjectType): PaymentMethod

    fun selectByAlias(alias: String): PaymentMethod
}

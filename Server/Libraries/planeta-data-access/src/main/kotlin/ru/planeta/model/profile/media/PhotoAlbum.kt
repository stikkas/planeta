package ru.planeta.model.profile.media

import ru.planeta.model.enums.ObjectType
import ru.planeta.model.enums.PermissionLevel
import ru.planeta.model.profile.ICommentable
import ru.planeta.model.profile.ProfileObject
import java.io.Serializable
import java.util.*

/**
 * @author ds.kolyshev
 * Date: 27.10.11
 */
class PhotoAlbum : ProfileObject(), Serializable, ICommentable {

    override var objectId: Long? = 0

    override var profileId: Long? = 0
    var albumTypeId: Int = 0
    var title: String? = null
    var imageUrl: String? = null
    var imageId: Long = 0
    internal var viewPermissionCode = PermissionLevel.EVERYBODY.code
    var timeAdded: Date? = null
    var timeUpdated: Date? = null
    var photosCount: Int = 0
    var viewsCount: Int = 0

    override var objectType: ObjectType?
        get() = ObjectType.PHOTOALBUM
        set(value: ObjectType?) {
            super.objectType = value
        }

    var viewPermission: PermissionLevel?
        get() = PermissionLevel.getByValue(viewPermissionCode)
        set(viewPermission) {
            this.viewPermissionCode = viewPermission?.code ?: PermissionLevel.EVERYBODY.code
        }

    fun getAlbumId(): Long? {
        return objectId
    }

    fun setAlbumId(albumId: Long) {
        this.objectId = albumId
    }

    companion object {

        private const val serialVersionUID = 1L
    }
}

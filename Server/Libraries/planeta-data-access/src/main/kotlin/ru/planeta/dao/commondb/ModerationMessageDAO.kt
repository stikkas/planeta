package ru.planeta.dao.commondb

import org.apache.ibatis.annotations.Mapper
import org.apache.ibatis.annotations.Param
import ru.planeta.model.common.ModerationMessage
import ru.planeta.model.common.campaign.enums.CampaignStatus

import java.util.EnumSet

/**
 * Created with IntelliJ IDEA.
 * User: user
 * Date: 22.10.13
 * Time: 18:38
 * To change this template use File | Settings | File Templates.
 */
@Mapper
interface ModerationMessageDAO {
    fun selectLastCampaignModerationMessage(campaignId: Long): ModerationMessage

    fun selectLastCampaignModerationMessageList(@Param("profileId") profileId: Long, @Param("campaignStatuses") campaignStatuses: EnumSet<CampaignStatus>): List<Map<*, *>>

    fun selectAllCampaignModerationMessages(@Param("campaignId") campaignId: Long, @Param("offset") offset: Int, @Param("limit") limit: Int): List<ModerationMessage>

    fun insertModerationMessage(moderationMessage: ModerationMessage)
}

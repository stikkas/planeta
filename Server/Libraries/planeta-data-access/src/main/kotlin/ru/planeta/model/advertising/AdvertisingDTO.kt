package ru.planeta.model.advertising

import java.util.Date

/**
 * Created with IntelliJ IDEA.
 * Date: 28.02.14
 * Time: 15:20
 */
class AdvertisingDTO {
    var vastXml: String? = null
    var state = AdvertisingState.GLOBAL
    var buttonText: String? = null
    var campaignId: Long = -1
    var broadcastId: Long = 0
    var dateTimeFrom: Date? = null
    var dateTimeTo: Date? = null
    var isDateChecked: Boolean = false
    var isCampaignChecked: Boolean = false

    var dateTo: Long
        get() = if (dateTimeTo != null) dateTimeTo!!.time else Date().time
        set(time) {
            dateTimeTo = if (time == 0L) null else Date(time)
        }

    var dateFrom: Long
        get() = if (dateTimeFrom != null) dateTimeFrom!!.time else Date().time
        set(time) {
            dateTimeFrom = if (time == 0L) null else Date(time)
        }

    var stateCode: Int
        get() = state.state
        set(code) {
            state = AdvertisingState.fromCode(code) ?: AdvertisingState.GLOBAL
        }
}

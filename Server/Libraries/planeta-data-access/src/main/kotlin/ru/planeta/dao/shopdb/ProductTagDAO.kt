package ru.planeta.dao.shopdb

import org.apache.ibatis.annotations.Mapper
import org.apache.ibatis.annotations.Param
import ru.planeta.model.shop.Category

@Mapper
interface ProductTagDAO {

    val productTagsCount: Int

    /**
     * Select all product tags
     *
     * @return list of product tags
     */
    fun selectAll(@Param("limit") limit: Int, @Param("offset") offset: Int): List<Category>

    /**
     * Select tags by product
     *
     * @return list of product tags for product
     */
    fun selectTagsByProduct(productId: Long): List<Category>

    /**
     * Update tags for product(remove tags, which are not in the list and save tags if they not yet in db)
     *
     * @param productId product id
     * @param newTags   list of new tags
     */
    fun updateTags(@Param("productId") productId: Long, @Param("newTags") newTags: List<Category>)

    fun getTagByMnemonicName(mnemonicName: String): Category

    fun getTagByMnemonicNameList(mnemonicNames: List<String>): List<Category>

    fun getProductTagById(categoryId: Long): Category

    fun insertProductTag(category: Category)

    fun updateProductTag(category: Category)

    fun removeNewTagsFromOldProducts()
}

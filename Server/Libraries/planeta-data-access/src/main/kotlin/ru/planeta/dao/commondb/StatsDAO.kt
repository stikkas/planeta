package ru.planeta.dao.commondb

import org.apache.ibatis.annotations.Mapper
import ru.planeta.model.common.WelcomeStats

@Mapper
interface StatsDAO {

    fun selectWelcomeStats(): WelcomeStats
}

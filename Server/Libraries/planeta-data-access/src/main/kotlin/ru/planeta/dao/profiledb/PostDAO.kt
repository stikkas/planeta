package ru.planeta.dao.profiledb

import org.apache.ibatis.annotations.Mapper
import org.apache.ibatis.annotations.Param
import ru.planeta.model.charity.enums.CharityEventType
import ru.planeta.model.profile.Post
import java.util.Date

/**
 * Created with IntelliJ IDEA.
 * User: a.volkov
 * Date: 14.11.2014
 * Time: 14:21
 */

@Mapper
interface PostDAO {

    fun select(id: Long): Post

    fun selectPosts(postIds: List<Long>): List<Post>

    fun selectPosts(@Param("postId") postId: Long, @Param("limit") limit: Int, @Param("offset") offset: Int): List<Post>

    fun selectPostsCount(): Long

    fun selectCampaignPostsLastNotificationTime(camapign_id: Long): Date

    fun insert(post: Post)

    fun update(post: Post)

    fun updateCharityPost(post: Post)

    fun selectByCampaignId(@Param("campaignId") campaignId: Long, @Param("limit") limit: Int, @Param("offset") offset: Int): List<Post>

    fun selectPromoPostsByProfileId(@Param("profileId") profileId: Long, @Param("offset") offset: Int, @Param("limit") limit: Int): List<Post>

    fun selectCampaignPostsCount(campaignId: Long): Int

    fun getIdByBlogId(blogId: Long): Long

    fun getIdByPostId(postId: Long): Long

    fun selectFutureCharityPosts(@Param("eventType") eventType: CharityEventType, @Param("offset") offset: Int, @Param("limit") limit: Int): List<Post>

    fun selectCharityNewsByTag(@Param("eventType") eventType: CharityEventType, @Param("newsTagId") newsTagId: Long, @Param("offset") offset: Int, @Param("limit") limit: Int): List<Post>

    fun selectCharityPostByIdOrAll(@Param("postId") postId: Long, @Param("limit") limit: Int, @Param("offset") offset: Int): List<Post>

    fun countCharityPosts(): Long

    fun deleteCharityPost(postId: Long): Long
}

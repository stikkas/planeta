package ru.planeta.dao.profiledb

import org.apache.ibatis.annotations.Mapper
import org.apache.ibatis.annotations.Param
import ru.planeta.model.profile.broadcast.BroadcastPrivateTargeting

/**
 * Private Broadcasts Targeting DAO
 *
 * @author m.shulepov
 * Date: 22.04.13
 */
@Mapper
interface BroadcastPrivateTargetingDAO {

    /**
     * Inserts new record into db
     *
     * @param broadcastPrivateTargeting [BroadcastPrivateTargeting] instance
     */
    fun insert(broadcastPrivateTargeting: BroadcastPrivateTargeting)

    /**
     * Updates record in db
     *
     * @param broadcastPrivateTargeting [BroadcastPrivateTargeting]
     */
    fun update(broadcastPrivateTargeting: BroadcastPrivateTargeting)

    /**
     * Returns list of [BroadcastPrivateTargeting]s for specified parameters
     *
     * @param broadcastId broadcast id
     * @return list of [BroadcastPrivateTargeting]s
     */
    fun selectBroadcastPrivateTargetings(broadcastId: Long): List<BroadcastPrivateTargeting>

    /**
     * Returns [BroadcastPrivateTargeting] for specified `generatedLink`
     *
     * @param broadcastId   broadcast id
     * @param generatedLink generated link
     * @return [BroadcastPrivateTargeting]
     */
    fun getBroadcastPrivateTargetingByLink(@Param("broadcastId") broadcastId: Long, @Param("generatedLink") generatedLink: String): BroadcastPrivateTargeting

}

package ru.planeta.dao.profiledb

import org.apache.ibatis.annotations.Mapper
import org.apache.ibatis.annotations.Param
import ru.planeta.model.news.ProfileNews
import java.util.*

/**
 * Created with IntelliJ IDEA.
 * User: a.volkov
 * Date: 13.11.2014
 * Time: 17:42
 */
@Mapper
interface ProfileNewsDAO {

    fun selectById(id: Long): ProfileNews

    fun selectForProfile(@Param("profileId") profileId: Long, @Param("onlyMyNews") onlyMyNews: Boolean, @Param("offset") offset: Int, @Param("limit") limit: Int): List<ProfileNews>

    fun selectForGuest(@Param("profileId") profileId: Long, @Param("offset") offset: Int, @Param("limit") limit: Int): List<ProfileNews>

    fun selectCampaignNews(@Param("campaignId") campaignId: Long, @Param("offset") offset: Int, @Param("limit") limit: Int): List<ProfileNews>

    fun insert(news: ProfileNews)

    fun markDeleted(id: Long)

    fun select(@Param("profileId") profileId: Long, @Param("type") type: ProfileNews.Type, @Param("objectId") objectId: Long): ProfileNews

    fun selectCountForProfile(@Param("profileId") profileId: Long, @Param("dateFrom") dateFrom: Date, @Param("onlyMyNews") onlyMyNews: Boolean): Int

    fun selectNewsByDate(@Param("date") date: Date, @Param("type") type: ProfileNews.Type): List<ProfileNews>
}

package ru.planeta.model.common

import java.math.BigDecimal
import java.util.Date
import ru.planeta.model.enums.TopayTransactionStatus

/**
 *
 * User: Serge Blagodatskih<stikkas17></stikkas17>@gmail.com><br></br>
 * Date: 01.02.16<br></br>
 * Time: 13:50
 */
class BillsFor1c {

    /**
     * Topay transaction id
     */
    var topayTransactionId: Long? = null

    /**
     * Amount of the bill
     */
    var amount: BigDecimal? = null

    /**
     * Time added
     */
    var timeAdded: Date? = null

    /**
     * Time payment of the bill
     */
    var timePurchased: Date? = null

    /**
     * Topay transaction status
     */
    var status: TopayTransactionStatus? = null

    var statusCode: Int
        get() = if (status != null) status!!.code else 0
        set(code) {
            status = TopayTransactionStatus.getByValue(code)
        }

    constructor() {}

    constructor(topayTransactionId: Long, timeAdded: Date) {
        this.topayTransactionId = topayTransactionId
        this.timeAdded = timeAdded
    }

}

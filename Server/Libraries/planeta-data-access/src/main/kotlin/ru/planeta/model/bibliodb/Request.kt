package ru.planeta.model.bibliodb

import java.io.Serializable
import java.util.Date

import ru.planeta.model.bibliodb.enums.ParticipantType
import ru.planeta.model.bibliodb.enums.RequestStatus

/**
 * Заявка на участие в проекте
 * User: Serge Blagodatskih<stikkas17></stikkas17>@gmail.com><br></br>
 * Date: 27.04.16<br></br>
 * Time: 12:11
 */
class Request : Serializable {

    /**
     * Идентификатор
     */
    var id: Long = 0

    /**
     * Статус заявки
     */
    var status = RequestStatus.NEW

    /**
     * Тип заявителя (издание, библиотека)
     */
    var type: ParticipantType? = null

    /**
     * Название организации
     */
    var organization: String? = null

    /**
     * Почтовый адрес с индексом заявителя
     */
    var address: String? = null

    /**
     * Контактное лицо
     */
    var contact: String? = null

    /**
     * Электронная почта заявителя
     */
    var email: String? = null

    /**
     * Сайт организации
     */
    var site: String? = null

    var timeAdded: Date? = null
}

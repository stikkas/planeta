package ru.planeta.model.profile

import ru.planeta.model.enums.ObjectType

/**
 * Date: 16.10.12
 *
 * @author s.kalmykov
 */
interface IVotable : ICommentable {

    var authorProfileId: Long

    var voteObjectId: Long

    var voteObjectType: ObjectType?

}

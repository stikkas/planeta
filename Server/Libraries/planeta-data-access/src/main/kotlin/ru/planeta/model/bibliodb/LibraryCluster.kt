package ru.planeta.model.bibliodb

class LibraryCluster {
    var latitude: Double? = null
    var longitude: Double? = null
    var count: Long = 0
    var name: String? = null
    var address: String? = null
    var libraryId: Long = 0
}

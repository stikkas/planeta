package ru.planeta.dao.bibliodb

/*
 * Created by Alexey on 11.04.2016.
 */

import org.apache.ibatis.annotations.Mapper
import org.apache.ibatis.annotations.Param
import ru.planeta.model.bibliodb.BookTag

@Mapper
interface BookTagDAO {

    fun getBookTags(@Param("limit") limit: Long, @Param("offset") offset: Long): List<BookTag>

    fun setTagRelation(@Param("printingId") printingId: Long, @Param("tags") tags: List<BookTag>)

    fun getBookTagsByBookId(id: Long): List<BookTag>

    fun existsTagInBooks(@Param("bookIds") bookIds: List<Long>, @Param("bookTagName") bookTagName: String): Boolean
}


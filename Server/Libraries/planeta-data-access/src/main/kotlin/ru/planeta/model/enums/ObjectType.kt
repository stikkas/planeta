package ru.planeta.model.enums

import java.util.EnumSet
import java.util.HashMap

/**
 * Enum ObjectType
 *
 * @author a.tropnikov
 */
enum class ObjectType private constructor(val label: String, val code: Int, //Objects can be
                                          /**
                                           * If this property is "true" than CommentObject.commentsCount is equal to the number of root level comments.
                                           * Otherwise it is equal to the number of all level comments.
                                           *
                                           * @return
                                           */
                                          val isOnlyRootLevel: Boolean) {
    // WALL("wall", 1, true),
    // GROUP("group", 2, true),
    // EVENT("event", 3, true),
    // BLOGPOST("blog", 4, false),
    PHOTO("photo", 5, true),
    VIDEO("video", 6, true),
    AUDIOALBUM("audioalbum", 7, true),
    PHOTOALBUM("photoalbum", 8, true),
    AUDIOTRACK("audiotrack", 9, true),
    PRODUCT("product", 10, true),
    BROADCAST("broadcast", 11, true),
    BROADCAST_STREAM("broadcast_stream", 12, true),
    CHAT("chat", 13, true),
    CHAT_MESSAGE("chat_message", 14, true),
    CAMPAIGN("campaign", 15, true),
    COMMENT("comment", 16, true),
    ORDER("order", 17, false),
    PROFILE("profile", 18, false),
    POST("post", 19, false);


    companion object {

        private val lookupByCode = HashMap<Int, ObjectType>()

        init {
            for (objectType in EnumSet.allOf(ObjectType::class.java)) {
                if (!lookupByCode.containsKey(objectType.code)) {
                    lookupByCode[objectType.code] = objectType
                }
            }
        }

        fun getByCode(code: Int): ObjectType? {
            return lookupByCode[code]
        }
    }
    // </editor-fold>
}

package ru.planeta.dao.handlers

import org.apache.ibatis.type.BaseTypeHandler
import org.apache.ibatis.type.JdbcType
import org.apache.ibatis.type.MappedTypes
import ru.planeta.model.common.auth.CredentialType
import java.sql.CallableStatement
import java.sql.PreparedStatement
import java.sql.ResultSet

@MappedTypes(CredentialType::class)
class CredentialTypeHandler : BaseTypeHandler<CredentialType>() {
    override fun getNullableResult(p0: ResultSet, p1: String?): CredentialType? {
        return CredentialType.getByValue(p0.getInt(p1))
    }

    override fun getNullableResult(p0: ResultSet, p1: Int): CredentialType? {
        return CredentialType.getByValue(p0.getInt(p1))
    }

    override fun getNullableResult(p0: CallableStatement, p1: Int): CredentialType? {
        return CredentialType.getByValue(p0.getInt(p1))
    }

    override fun setNonNullParameter(p0: PreparedStatement, p1: Int, p2: CredentialType, p3: JdbcType?) {
        p0.setInt(p1, p2.code)
    }
}
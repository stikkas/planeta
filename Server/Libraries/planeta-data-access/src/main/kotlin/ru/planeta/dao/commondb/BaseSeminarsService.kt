package ru.planeta.dao.commondb

import org.apache.ibatis.annotations.Param
import org.springframework.beans.factory.annotation.Autowired
import ru.planeta.model.commondb.Seminars
import ru.planeta.model.commondb.SeminarsExample

abstract class BaseSeminarsService {
    @Autowired
    protected var seminarsDAO: SeminarsDAO? = null

    val orderByClause: String
        get() = "seminar_id"

    //**************************************************************************************************
    //*********************************  Create ** Update ** Delete  *************************************
    //**************************************************************************************************
    fun insertSeminars(seminars: Seminars) {


        seminarsDAO!!.insertSelective(seminars)
    }

    fun insertSeminarsAllFields(seminars: Seminars) {


        seminarsDAO!!.insert(seminars)
    }

    fun updateSeminars(seminars: Seminars) {

        seminarsDAO!!.updateByPrimaryKeySelective(seminars)
    }

    fun updateSeminarsAllFields(seminars: Seminars) {

        seminarsDAO!!.updateByPrimaryKey(seminars)
    }

    fun insertOrUpdateSeminars(seminars: Seminars) {

        val selectedSeminars = seminarsDAO!!.selectByPrimaryKey(seminars.seminarId)
        if (selectedSeminars == null) {

            seminarsDAO!!.insertSelective(seminars)
        } else {
            seminarsDAO!!.updateByPrimaryKeySelective(seminars)
        }
    }

    fun insertOrUpdateSeminarsAllFields(seminars: Seminars) {

        val selectedSeminars = seminarsDAO!!.selectByPrimaryKey(seminars.seminarId)
        if (selectedSeminars == null) {

            seminarsDAO!!.insert(seminars)
        } else {
            seminarsDAO!!.updateByPrimaryKey(seminars)
        }
    }

    fun deleteSeminars(id: Long?) {
        seminarsDAO!!.deleteByPrimaryKey(id)
    }

    //**************************************************************************************************
    //******************************************* seminars ***********************************************
    //************************************** SelectByPrimaryKey ******************************************
    //**************************************************************************************************
    fun selectSeminars(seminarId: Long?): Seminars {
        return seminarsDAO!!.selectByPrimaryKey(seminarId)
    }

    //**************************************************************************************************
    //******************************************* seminars ***********************************************
    //**************************************************************************************************
    private fun COMMENT_SECTION_Seminars() {

    }

    fun getSeminarsExample(@Param("offset") offset: Int, @Param("limit") limit: Int): SeminarsExample {
        val example = SeminarsExample()
        example.offset = offset
        example.limit = limit
        example.or()
        example.orderByClause = orderByClause
        return example
    }

    fun selectSeminarsCount(): Int {
        return seminarsDAO!!.countByExample(getSeminarsExample(0, 0))
    }

    fun selectSeminarsList(@Param("offset") offset: Int, @Param("limit") limit: Int): List<Seminars> {
        return seminarsDAO!!.selectByExample(getSeminarsExample(offset, limit))
    }
}

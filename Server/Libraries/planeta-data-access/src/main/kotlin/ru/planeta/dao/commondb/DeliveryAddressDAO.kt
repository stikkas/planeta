package ru.planeta.dao.commondb

import org.apache.ibatis.annotations.Mapper
import org.apache.ibatis.annotations.Param
import ru.planeta.model.common.DeliveryAddress

/**
 * Created with IntelliJ IDEA.
 * Date: 27.10.2014
 * Time: 11:32
 * To change this template use File | Settings | File Templates.
 */
@Mapper
interface DeliveryAddressDAO {
    fun insertOrUpdate(deliveryAddress: DeliveryAddress): DeliveryAddress

    fun selectDelivery(orderId: Long): DeliveryAddress

    fun delete(orderId: Long)

    fun updateOderId(@Param("deliveryAdderessId") deliveryAdderessId: Long, @Param("orderId") orderId: Long)

    fun insertList(list: List<DeliveryAddress>)
}

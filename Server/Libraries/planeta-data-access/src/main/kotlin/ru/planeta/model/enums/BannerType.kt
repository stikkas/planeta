package ru.planeta.model.enums

import java.util.HashMap

enum class BannerType(override val code: Int) : Codable {
    TEASER(1),
    TOP_LINE(2),
    CAMPAIGN_LOWLINE(4),
    CAMPAIGN_SIDEBAR(5),
    LOW_LINE(6),
    PROMO_BANNER(8),
    PROFILE(10),
    SHOP(12);


    companion object {

        private val lookup = HashMap<Int, BannerType>()

        init {
            for (s in values()) {
                lookup[s.code] = s
            }
        }

        fun getByValue(code: Int): BannerType? {
            return lookup[code]
        }
    }

}

package ru.planeta.dao.mappers

import java.sql.Types

/**
 * Class LongArray
 *
 * @author a.tropnikov
 */
class LongArray : SqlArrayAdapter {

    companion object {
        private val emptyArray = arrayOf<Long>()
    }

    private var array: Array<Long>? = null

    constructor(array: Array<Long>?) {
        if (array == null) {
            throw IllegalArgumentException("parameter array should not be null")
        }
        this.array = array
    }

    constructor(list: List<Long>?) {
        if (list == null) {
            throw IllegalArgumentException("parameter array should not be null")
        }
        this.array = list.toTypedArray()
    }

    override fun getBaseType(): Int {
        return Types.BIGINT
    }

    override fun getBaseTypeName(): String {
        return "int8"
    }

    override fun toString(): String {
        var result = "{"
        array?.forEachIndexed { index, l ->
            if (index > 0) {
                result += ","
            }
            result += l
        }
        result += "}"
        return result
    }

    override fun free() {
    }
}


package ru.planeta.dao.profiledb

import org.apache.ibatis.annotations.Mapper
import org.apache.ibatis.annotations.Param
import ru.planeta.model.profile.media.Video

/**
 * Profile video dao
 *
 * @author ds.kolyshev
 * Date: 27.10.11
 */
@Mapper
interface VideoDAO {

    /**
     * Selects video by its identifier
     *
     */
    fun selectVideoById(@Param("profileId") profileId: Long, @Param("videoId") videoId: Long): Video?

    /**
     * Inserts video
     *
     */
    fun insert(video: Video)

    /**
     * Updates video
     *
     */
    fun update(video: Video)

    /**
     * Deletes video
     *
     */
    fun delete(@Param("profileId") profileId: Long, @Param("videoId") videoId: Long)

    /**
     * Updates video's views count
     *
     */
    fun updateViewsCount(@Param("profileId") profileId: Long, @Param("videoId") videoId: Long, @Param("viewsCount") viewsCount: Int)

    /**
     * Selects external video by its owner, author and cached video id
     *
     * @param profileId       - owner id
     * @param authorProfileId - author id
     * @param cachedVideoId   - cachedVideo id
     */
    fun selectVideoByCacheId(@Param("profileId") profileId: Long, @Param("authorProfileId") authorProfileId: Long, @Param("cachedVideoId") cachedVideoId: Long): Video?
}

package ru.planeta.model.shop.enums

import ru.planeta.model.enums.Codable

import java.util.HashMap

/**
 * Enum ProductCategory
 *
 * @author m.shulepov
 */
enum class ProductCategory private constructor(override val code: Int) : Codable {
    DIGITAL(1), PHYSICAL(2), DONATE(3);


    companion object {

        private val lookup = HashMap<Int, ProductCategory>()

        init {
            for (s in values()) {
                lookup[s.code] = s
            }
        }

        fun getByValue(code: Int): ProductCategory? {
            return lookup[code]
        }
    }
}

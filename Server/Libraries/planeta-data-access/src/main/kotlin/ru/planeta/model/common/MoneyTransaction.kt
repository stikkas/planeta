package ru.planeta.model.common

import ru.planeta.model.enums.MoneyTransactionType
import ru.planeta.model.enums.OrderObjectType
import ru.planeta.model.shop.enums.PaymentStatus

import java.math.BigDecimal

/**
 * Created by kostiagn on 27.10.2015.
 */
class MoneyTransaction : BaseTransaction() {
    var balance: BigDecimal? = null
    var orderId: Long = 0
    var orderObjectType: OrderObjectType? = null
    var transactionType: MoneyTransactionType? = null
    var paymentStatus: PaymentStatus? = null
    var topayTransactionId: Long = 0
    var paymentMethodNameRu: String? = null
    var paymentMethodNameEn: String? = null
}

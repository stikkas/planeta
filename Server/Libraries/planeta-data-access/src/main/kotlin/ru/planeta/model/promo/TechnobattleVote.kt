package ru.planeta.model.promo


/**
 * Created with IntelliJ IDEA.
 * User: michail
 * Date: 16.01.2017
 * Time: 16:42
 */
class TechnobattleVote {
    var voterId: Long = 0
    var userId: String? = null
    var projectId: Long = 0
    var voteType: VoteType? = null

    constructor() {}

    constructor(userId: String, projectId: Long, voteType: VoteType) {
        this.userId = userId
        this.projectId = projectId
        this.voteType = voteType
        this.voterId = 0
    }
}

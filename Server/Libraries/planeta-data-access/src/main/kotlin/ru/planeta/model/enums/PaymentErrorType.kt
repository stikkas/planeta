package ru.planeta.model.enums

import java.util.*

enum class PaymentErrorType private constructor(override val code: Int, override val descriptionMessageCode: String, override val fullDescriptionMessageCode: String) : ReadableEnum {
    CAMPAIGN_NOT_ACTIVE(1, "enum.payment.error.type.campaign.not.active.description", "enum.payment.error.type.campaign.not.active.full.description"),
    NOT_ENOUGH_SHARE_AMOUNT(2, "enum.payment.error.type.not.enough.share.amount.description", "enum.payment.error.type.not.enough.share.amount.full.description"),
    NOT_ENOUGH_PRODUCT_AMOUNT(3, "enum.payment.error.type.not.enough.product.amount.description", "enum.payment.error.type.not.enough.product.amount.full.description"),
    ORDER_NOT_ENOUGH_MONEY_FOR_PURCHASE(4, "enum.payment.error.type.order.not.enough.money.for.purchase.description", "enum.payment.error.type.order.not.enough.money.for.purchase.full.description"),
    OTHER_ERRORS(5, "enum.payment.error.type.other.errors.description", "enum.payment.error.type.other.errors.full.description"),
    LARGE_SUM_FAIL_PAYMENT(6, "enum.payment.error.type.large.sum.fail.payment.description", "enum.payment.error.type.large.sum.fail.payment.full.description"),
    TWO_FAIL_PAYMENTS(7, "enum.payment.error.type.two.fail.payments.description", "enum.payment.error.type.two.fail.payments.full.description");


    companion object {

        private val lookup = HashMap<Int, PaymentErrorType>()

        fun parse(s: String?): PaymentErrorType? {
            if (s == null || s.isEmpty()) return null
            try {
                val i = Integer.parseInt(s)
                if (i < values().size) return values()[i]
            } catch (e: NumberFormatException) {
            }

            return null
        }

        fun getByValue(code: Int): PaymentErrorType? {
            if (lookup.size == 0) {
                for (s in EnumSet.allOf(PaymentErrorType::class.java)) {
                    lookup[s.code] = s
                }
            }
            return lookup[code]
        }
    }
}

package ru.planeta.model.profile.location

import com.fasterxml.jackson.annotation.JsonIgnoreProperties

/**
 * @author Andrew.Arefyev@gmail.com
 * 30.10.13 19:04
 */
@JsonIgnoreProperties(ignoreUnknown = true)
class UnspecifiedLocation : BaseLocation {

    override var locationType: LocationType? = null
    override var locationId: Int = 0

    override var name: String? = ""
        get() = "Unspecified location"

    override var parentLocationId: Int? = 0
        get() = IGeoLocation.DEFAULT_LOCATION_ID

    override var parentLocationType: LocationType? = null
        get() = null


    constructor() {}

    constructor(locationId: Int?, locationType: LocationType) {
        this.locationId = locationId ?: 0
        this.locationType = locationType
    }
}

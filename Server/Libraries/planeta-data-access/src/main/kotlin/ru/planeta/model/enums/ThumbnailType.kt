package ru.planeta.model.enums

import org.apache.commons.lang3.StringUtils

/**
 * @author ds.kolyshev
 * Date: 20.09.11
 */
enum class ThumbnailType {

    REAL("real", 0, 0), //really uncropped file (copy of uploaded file)
    ORIGINAL("original", 1000, 1000),
    PHOTOPREVIEW("photo_preview", 150, 112),
    USER_AVATAR("user_avatar", 183, 0),
    USER_SMALL_AVATAR("user_small_avatar", 50, 50, true, true),
    SMALL("small", 150, 0),
    MEDIUM("medium", 300, 0),
    BIG("big", 450, 0),
    HUGE("huge", 670, 0),
    ALBUM_COVER("album_cover", 130, 130, true, true),
    TV_COVER("tv_cover", 220, 124, true, true),
    TV_BIG_COVER("tv_big_cover", 460, 259, true, true),
    PRODUCT_COVER("product_cover", 210, 210, true),
    PRODUCT_COVER_NEW("product_cover_new", 280, 280, true),
    SCHOOL_NEWS("original_school", 360, 203, true),
    CAMPAIGN_VIDEO("original_campaign", 600, 338, true),
    SHARE_COVER("share_preview", 220, 150, true, true);

    val thumbFileName: String?
    val width: Int
    val height: Int
    val isCrop: Boolean
    val isDisableAnimatedGif: Boolean


    constructor(thumbFileName: String? = null, width: Int = 0, height: Int = 0, crop: Boolean = false, disableAnimatedGif: Boolean = false) {
        this.thumbFileName = thumbFileName
        this.width = width
        this.height = height
        this.isCrop = crop
        this.isDisableAnimatedGif = disableAnimatedGif
    }

    companion object {

        /**
         * Gets thumbnail url
         *
         * @param imageUrl      url to convert
         * @param thumbnailType convert params
         * @return converted image url
         */
        fun getUrlByThumbnailType(imageUrl: String, thumbnailType: ThumbnailType): String? {
            if (StringUtils.isEmpty(imageUrl)) {
                return null
            }

            val lastSlashIndex = imageUrl.lastIndexOf('/')
            if (lastSlashIndex < 0) {
                return null
            }

            val lastPointIndex = imageUrl.lastIndexOf('.')
            if (lastPointIndex < 0) {
                return null
            }

            var extension = imageUrl.substring(lastPointIndex)
            extension = if (".gif" == extension) extension else ".jpg"

            return imageUrl.substring(0, lastSlashIndex + 1) + thumbnailType.thumbFileName + extension
        }
    }
}

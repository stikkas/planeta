package ru.planeta.model.shop

import org.apache.commons.lang3.ArrayUtils
import org.apache.commons.lang3.StringUtils
import ru.planeta.model.enums.ObjectType
import ru.planeta.model.profile.VotableObject
import ru.planeta.model.shop.enums.ProductCategory
import ru.planeta.model.shop.enums.ProductState
import ru.planeta.model.shop.enums.ProductStatus
import java.math.BigDecimal
import java.util.*

/**
 * This class represents product.
 * User: a.savanovich
 * Date: 18.06.12
 * Time: 12:28
 */
open class Product : VotableObject {

    override var objectId: Long? = null
        get() = productId

    // this field represents product-parent relation
    var parentProductId: Long = 0

    var price: BigDecimal? = BigDecimal.ZERO
    var oldPrice = BigDecimal.ZERO
    var currency = "RUB"

    var totalPurchaseCount: Int = 0
    // purchase limit per user
    var purchaseLimit: Int = 0

    var productState = ProductState.SOLID_PRODUCT
    var productStatus = ProductStatus.PAUSE
    var productCategory = ProductCategory.PHYSICAL

    private var tags: MutableList<Category> = mutableListOf()

    var name: String? = null
    var nameHtml: String? = null
    var description: String? = null
    var descriptionHtml: String? = null
    var textForMail: String? = null
    var imageUrls: List<String>? = null
    var coverImageUrl: String? = null
    var tableImageUrl: String? = null
    var productAttribute = ProductAttribute()

    var timeAdded: Date? = null
    var timeUpdated: Date? = null
    var timeStarted: Date? = null       // time when product become active
    var startSaleDate: Date? = null     // time when preordered product is available for delivery

    //private String contentUrl;
    var contentUrls: List<String>? = null

    var isFreeDelivery = false
    var isCashAvailable = false

    // sum of quantities of a product and of it's direct children(if it has any)
    var totalQuantity: Int = 0
        get() = if (productCategory == ProductCategory.DIGITAL) 1 else field
    var totalOnHoldQuantity: Int = 0

    var referrerId: Long = 0
    var donateId: Long = 0
    var isShowOnCampaign = true
    var campaignIds: LongArray? = null

    var productId: Long
        get() = voteObjectId
        set(productId) {
            voteObjectId = productId
        }

    var merchantProfileId: Long?
        get() = profileId
        set(merchantProfileId) {
            this.profileId = merchantProfileId
        }

    val mainTag: Category?
        get() = if (getTags().isEmpty()) null else getTags()[0]

    var imageUrlsArray: Array<String>?
        get() {
            if (imageUrls == null) return null
            val array = arrayOf<String>()
            for (imageUrl in imageUrls!!) {
                ArrayUtils.add<String>(array, imageUrl)
            }
            return array
        }
        set(o) {}

    var storeId: Long?
        get() = profileId
        set(storeId) {
            profileId = storeId
        }

    var contentUrlsString: String
        get() = if (contentUrls == null || contentUrls!!.isEmpty()) {
            ""
        } else contentUrls!!.joinToString("\r\n")
        set(contentUrlsString) {
            if (StringUtils.isEmpty(contentUrlsString)) {
                return
            }
            contentUrls = ArrayList(Arrays.asList(*contentUrlsString.split("\\s*\\r\\n\\s*".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()))
        }

    var campaignIdsString: String
        get() {
            if (campaignIds == null || campaignIds!!.size == 0) {
                return ""
            }
            val idsString = ArrayList<String>()
            for (l in campaignIds!!) {
                if (l == null) {
                    continue
                }
                idsString.add(l.toString())
            }
            return idsString.joinToString(",")
        }
        set(ids) {
            if (StringUtils.isEmpty(ids)) {
                return
            }
            val idsString = Arrays.asList(*ids.split("\\s*,\\s*".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray())
            val ids = LongArray(idsString.size)
            for (i in idsString.indices) {
                ids[i] = java.lang.Long.parseLong(idsString[i])
            }
            campaignIds = ids
        }

    constructor() {
        voteObjectType = ObjectType.PRODUCT
    }

    constructor(product: Product) {
        copy(product)
    }

    fun copy(product: Product) {
        this.productId = product.productId
        clone(product)
    }

    fun clone(product: Product) {
        this.profileId = product.profileId
        this.merchantProfileId = product.merchantProfileId
        this.voteObjectType = ObjectType.PRODUCT
        this.authorProfileId = product.authorProfileId
        this.parentProductId = product.parentProductId
        this.price = product.price
        this.oldPrice = product.oldPrice
        this.currency = product.currency
        this.totalPurchaseCount = product.totalPurchaseCount
        this.purchaseLimit = product.purchaseLimit
        this.productState = product.productState
        this.tags = product.getTags()
        this.productStatus = product.productStatus
        this.productCategory = product.productCategory
        this.name = product.name
        this.nameHtml = product.nameHtml
        this.description = product.description
        this.descriptionHtml = product.descriptionHtml
        this.textForMail = product.textForMail
        this.imageUrls = product.imageUrls
        this.coverImageUrl = product.coverImageUrl
        this.tableImageUrl = product.tableImageUrl
        this.contentUrls = product.contentUrls
        this.isFreeDelivery = product.isFreeDelivery
        val attribute = ProductAttribute()
        attribute.value = product.productAttribute.value
        attribute.attributeId = product.productAttribute.attributeId
        attribute.attributeTypeId = product.productAttribute.attributeTypeId
        attribute.index = product.productAttribute.index
        this.productAttribute = attribute
        this.tags = product.getTags()
        this.timeAdded = product.timeAdded
        this.timeUpdated = product.timeUpdated
        this.totalQuantity = product.totalQuantity
        this.startSaleDate = product.startSaleDate
        this.referrerId = product.referrerId
        this.donateId = product.donateId
        this.isCashAvailable = product.isCashAvailable
        this.isShowOnCampaign = product.isShowOnCampaign
        this.campaignIds = product.campaignIds
        this.timeStarted = product.timeStarted
    }


    fun getTags(): MutableList<Category> {
        if (tags == null) {
            tags = mutableListOf()
        }
        return tags
    }

    fun setTags(tags: List<Category>) {
        getTags().clear()
        for (tag in tags) addTag(tag)
    }

    fun addTag(tag: Category?) {
        if (tag != null) {
            getTags().add(tag)
        }
    }

    override fun equals(o: Any?): Boolean {
        if (this === o) return true
        if (o !is Product) return false
        if (!super.equals(o)) return false

        val product = o as Product?

        if (productId == 0L || product!!.productId == 0L) {
            if (if (name != null) name != product!!.name else product!!.name != null) return false
            if (if (description != null) description != product.description else product.description != null) {
                return false
            }
            if (if (price != null) price != product.price else product.price != null) return false
            if (if (productAttribute != null) !productAttribute!!.equals(product.productAttribute) else product.productAttribute != null)
                return false
            if (if (coverImageUrl != null) coverImageUrl != product.coverImageUrl else product.coverImageUrl != null) {
                return false
            }
            if (if (imageUrls != null) imageUrls != product.imageUrls else product.imageUrls != null) return false

        }

        return true
    }

    override fun hashCode(): Int {
        var result = super.hashCode()
        if (productId == 0L) {
            result = 31 * result + if (name != null) name!!.hashCode() else 0
            result = 31 * result + (parentProductId xor parentProductId.ushr(32)).toInt()
            result = 31 * result + if (productAttribute != null) productAttribute!!.hashCode() else 0
        } else {
            result = 31 * result + productStatus.code
        }
        return result
    }
}

package ru.planeta.model.shop.enums

import ru.planeta.model.enums.Codable

import java.util.HashMap

/**
 * User: a.savanovich
 * Date: 20.06.12
 * Time: 18:18
 */
enum class PaymentStatus private constructor(override val code: Int) : Codable {
    PENDING(0),
    COMPLETED(1),
    CANCELLED(2),
    FAILED(3),
    RESERVED(4),
    OVERDUE(5);


    companion object {

        private val lookup = HashMap<Int, PaymentStatus>()

        init {
            for (s in values()) {
                lookup[s.code] = s
            }
        }

        fun getByValue(code: Int): PaymentStatus? {
            return lookup[code]
        }
    }

}

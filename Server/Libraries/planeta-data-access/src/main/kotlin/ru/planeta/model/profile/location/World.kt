package ru.planeta.model.profile.location

/**
 * @author Andrew.Arefyev@gmail.com
 * 20.11.13 23:29
 */
class World : BaseLocation() {
    override var locationId: Int
        get() = IGeoLocation.DEFAULT_LOCATION_ID
        set(locationId) {}

    override var locationType: LocationType? = null
        get() = LocationType.WORLD

    override var name: String? = ""
        get() = "Мир"

    override var parentLocationId: Int? = 0
        get() = IGeoLocation.DEFAULT_LOCATION_ID

    override var parentLocationType: LocationType? = null
        get() = null

}

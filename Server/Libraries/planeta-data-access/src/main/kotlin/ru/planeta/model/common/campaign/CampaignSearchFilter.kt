package ru.planeta.model.common.campaign

import ru.planeta.model.shop.enums.DeliveryStatus
import ru.planeta.model.shop.enums.PaymentStatus

import java.util.Date

class CampaignSearchFilter {

    var clientId: Long = 0
    var campaignId: Long = 0
    var shareId: Long = 0
    var dateFrom: Date? = null
    var dateTo: Date? = null
    var paymentStatus: PaymentStatus? = null
    var deliveryStatus: DeliveryStatus? = null
    var searchString: String? = null
    var cityAndCountrySearchString: String? = null
    var offset: Int = 0
    var limit: Int = 0

    constructor() {}

    /**
     * @param clientId       client id
     * @param campaignId     campaign id
     * @param dateFrom       start date
     * @param dateTo         end date
     * @param paymentStatus  payment status
     * @param deliveryStatus delivery status
     * @param searchString   search string
     * @param offset         offset
     * @param limit          limit
     */
    constructor(clientId: Long, campaignId: Long, shareId: Long, dateFrom: Date, dateTo: Date, paymentStatus: PaymentStatus, deliveryStatus: DeliveryStatus, searchString: String, cityAndCountrySearchString: String, offset: Int, limit: Int) {
        this.clientId = clientId
        this.campaignId = campaignId
        this.shareId = shareId
        this.dateFrom = dateFrom
        this.dateTo = dateTo
        this.paymentStatus = paymentStatus
        this.deliveryStatus = deliveryStatus
        this.searchString = searchString
        this.cityAndCountrySearchString = cityAndCountrySearchString
        this.offset = offset
        this.limit = limit
    }
}

package ru.planeta.model.msg

import java.io.Serializable

/**
 * This class unifies all dialog data.
 *
 * @author ameshkov
 */
class DialogInfo : Serializable {

    var dialogId: Long = 0
    var dialog: Dialog? = null
    var lastMessage: DialogMessage? = null

    var dialogUsers: List<DialogUser>? = null
    var unreadMessagesCount: Int = 0
    var firstUnreadMessageId: Long = 0
}

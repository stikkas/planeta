package ru.planeta.dao.concertdb

import org.apache.ibatis.annotations.Mapper
import org.apache.ibatis.annotations.Param
import ru.planeta.model.concert.Place

/**
 * Created with IntelliJ IDEA.
 * User: michail
 * Date: 14.10.16
 * Time: 17:00
 */
@Mapper
interface PlaceDAO {
    fun insert(place: Place): Int

    fun update(place: Place): Int

    fun select(placeId: Long): Place

    fun selectByExternalUnusedId(externalUnusedId: Long): Place

    fun selectByExternalPlaceId(externalPlaceId: Long): Place

    fun selectBySectionRowPosition(@Param("sectionId") sectionId: Long, @Param("rowId") rowId: Long, @Param("position") position: Long): Place

    fun selectByExtConcertId(@Param("externalConcertId") externalConcertId: Long, @Param("externalSectionId") externalSectionId: Long?, @Param("externalRowId") externalRowId: Long?): List<Place>

    fun selectReservedPlaces(@Param("offset") offset: Int, @Param("limit") limit: Int): List<Place>
}

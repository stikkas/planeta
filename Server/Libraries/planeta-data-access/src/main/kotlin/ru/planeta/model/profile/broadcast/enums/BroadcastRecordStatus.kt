package ru.planeta.model.profile.broadcast.enums

import java.util.HashMap

/**
 * Broadcast recording status
 *
 * @author ds.kolyshev
 * Date: 19.03.12
 */
enum class BroadcastRecordStatus private constructor(val code: Int) {
    NO_RECORD(0), WILL_BE_RECORD(1), RECORDING(2), RECORD_PAUSED(3), RECORD_FINISHED(4);


    companion object {

        private val lookup = HashMap<Int, BroadcastRecordStatus>()

        init {
            for (s in values()) {
                lookup[s.code] = s
            }
        }

        fun getByValue(code: Int?): BroadcastRecordStatus? {
            return lookup[code]
        }
    }
}

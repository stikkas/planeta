package ru.planeta.dao.profiledb

import org.apache.ibatis.annotations.Param
import java.util.Date
import org.springframework.beans.factory.annotation.Autowired
import ru.planeta.model.profiledb.ProfileSites
import ru.planeta.model.profiledb.ProfileSitesExample

abstract class BaseProfileSitesService {
    @Autowired
    protected var profileSitesMapper: ProfileSitesMapper? = null

    val orderByClause: String
        get() = "profile_id"

    //**************************************************************************************************
    //*********************************  Create ** Update ** Delete  *************************************
    //**************************************************************************************************
    fun insertProfileSites(profileSites: ProfileSites) {
        profileSites.timeAdded = Date()
        profileSites.timeUpdated = Date()
        profileSitesMapper!!.insertSelective(profileSites)
    }

    fun insertProfileSitesAllFields(profileSites: ProfileSites) {
        profileSites.timeAdded = Date()
        profileSites.timeUpdated = Date()
        profileSitesMapper!!.insert(profileSites)
    }

    fun updateProfileSites(profileSites: ProfileSites) {
        profileSites.timeUpdated = Date()
        profileSitesMapper!!.updateByPrimaryKeySelective(profileSites)
    }

    fun updateProfileSitesAllFields(profileSites: ProfileSites) {
        profileSites.timeUpdated = Date()
        profileSitesMapper!!.updateByPrimaryKey(profileSites)
    }

    fun insertOrUpdateProfileSites(profileSites: ProfileSites) {
        profileSites.timeUpdated = Date()
        val selectedProfileSites = profileSitesMapper!!.selectByPrimaryKey(profileSites.profileId)
        if (selectedProfileSites == null) {
            profileSites.timeAdded = Date()
            profileSitesMapper!!.insertSelective(profileSites)
        } else {
            profileSitesMapper!!.updateByPrimaryKeySelective(profileSites)
        }
    }

    fun insertOrUpdateProfileSitesAllFields(profileSites: ProfileSites) {
        profileSites.timeUpdated = Date()
        val selectedProfileSites = profileSitesMapper!!.selectByPrimaryKey(profileSites.profileId)
        if (selectedProfileSites == null) {
            profileSites.timeAdded = Date()
            profileSitesMapper!!.insert(profileSites)
        } else {
            profileSitesMapper!!.updateByPrimaryKey(profileSites)
        }
    }

    fun deleteProfileSites(id: Long?) {
        profileSitesMapper!!.deleteByPrimaryKey(id)
    }

    //**************************************************************************************************
    //***************************************** profile_sites ********************************************
    //************************************** SelectByPrimaryKey ******************************************
    //**************************************************************************************************
    fun selectProfileSites(profileId: Long?): ProfileSites {
        return profileSitesMapper!!.selectByPrimaryKey(profileId)
    }

    //**************************************************************************************************
    //***************************************** profile_sites ********************************************
    //**************************************************************************************************
    private fun COMMENT_SECTION_ProfileSites() {

    }

    fun getProfileSitesExample(@Param("offset") offset: Int, @Param("limit") limit: Int): ProfileSitesExample {
        val example = ProfileSitesExample()
        example.offset = offset
        example.limit = limit
        example.or()
        example.orderByClause = orderByClause
        return example
    }

    fun selectProfileSitesCount(): Int {
        return profileSitesMapper!!.countByExample(getProfileSitesExample(0, 0))
    }

    fun selectProfileSitesList(@Param("offset") offset: Int, @Param("limit") limit: Int): List<ProfileSites> {
        return profileSitesMapper!!.selectByExample(getProfileSitesExample(offset, limit))
    }
}

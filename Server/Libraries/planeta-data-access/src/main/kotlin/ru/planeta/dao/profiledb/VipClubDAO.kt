package ru.planeta.dao.profiledb

import org.apache.ibatis.annotations.Mapper
import org.apache.ibatis.annotations.Param

/**
 * Created by a.savanovich on 05.08.2015.
 */
@Mapper
interface VipClubDAO {

    fun getVipIds(@Param("offset") offset: Int, @Param("limit") limit: Int): List<Long>
}

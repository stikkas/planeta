package ru.planeta.dao.shopdb


import org.apache.ibatis.annotations.Mapper
import org.apache.ibatis.annotations.Param
import ru.planeta.model.shop.ShoppingCartShortItem
import java.util.*

/**
 * Primitive operation with a single shopping cart
 *
 * @author Andrew Arefyev
 * Date: 03.12.12
 * Time: 18:45
 */
@Mapper
interface CartDAO {
    /**
     * adds new record.
     *
     * @param buyerId   buyer profile id
     * @param productId product id
     * @param quantity  requested quantity
     */
    fun insert(@Param("profileId") buyerId: Long, @Param("productId") productId: Long, @Param("quantity") quantity: Int): Int

    /**
     * request product quantity
     *
     * @param profileId buyer profile id
     * @param productId product id
     * @return cart record in short format
     */
    fun select(@Param("profileId") profileId: Long, @Param("productId") productId: Long): ShoppingCartShortItem?

    /**
     * selectCampaignById shopping cart
     *
     * @param profileId buyer id
     * @return list of product with quantities
     */
    fun select(profileId: Long): List<ShoppingCartShortItem>

    /**
     * update quantity of existing records
     *
     * @param buyerId   buyer profile id
     * @param productId product id
     * @param quantity  new quantity
     * @return count of changed rows
     */
    fun update(@Param("profileId") buyerId: Long, @Param("productId") productId: Long, @Param("quantity") quantity: Int): Int

    /**
     * deleteByProfileId all user cart
     *
     * @param buyerId buyer id
     * @return count deleted rows
     */
    fun clearCart(@Param("profileId") buyerId: Long): Int

    /**
     * deleteByProfileId the product item from cart
     *
     * @param buyerId   buyer Id
     * @param productId product Id
     * @return count deleted rows
     */
    fun delete(@Param("profileId") buyerId: Long, @Param("productId") productId: Long): Int

    /**
     * deleteByProfileId product from all carts
     *
     * @param productId product id
     * @return count deleted rows
     */
    fun deleteProduct(@Param("productId") productId: Long): Int

    /**
     * @return
     */
    fun getLastTimeUpdated(profileId: Long): Date

    /**
     * get all carts profile id
     *
     * @return
     */
    fun selectCartOwnersIds(): List<Long>
}

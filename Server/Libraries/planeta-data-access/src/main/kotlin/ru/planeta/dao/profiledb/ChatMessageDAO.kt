package ru.planeta.dao.profiledb

import org.apache.ibatis.annotations.Mapper
import org.apache.ibatis.annotations.Param
import ru.planeta.model.profile.chat.ChatMessage

/**
 * Data access object for working with chat messages dao
 *
 * @author m.shulepov
 */
@Mapper
interface ChatMessageDAO {

    /**
     * Selects specified chat message
     *
     * @param chatId
     * @param messageId
     * @return
     */
    fun selectMessage(@Param("profileId") profileId: Long, @Param("chatId") chatId: Long, @Param("messageId") messageId: Long): ChatMessage

    /**
     * Selects chat messages for the specified chat (starting from the last message)
     *
     * @param chatId
     * @param offset
     * @param limit
     * @return
     */
    fun selectMessages(@Param("profileId") profileId: Long, @Param("chatId") chatId: Long, @Param("offset") offset: Int, @Param("limit") limit: Int): List<ChatMessage>

    /**
     * Selects chat messages starting from the specified "startMessageId" (exclusive)
     *
     * @param chatId
     * @param startMessageId
     * @return
     */
    fun selectMessages(@Param("profileId") profileId: Long, @Param("chatId") chatId: Long, @Param("startMessageId") startMessageId: Long): List<ChatMessage>

    /**
     * Inserts chat message into the chat
     *
     * @param chatMessage
     */
    fun insert(chatMessage: ChatMessage)

    /**
     * Updates chat message. Updates only deleted flag.
     *
     * @param chatMessage
     */
    fun update(chatMessage: ChatMessage)

}

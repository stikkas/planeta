package ru.planeta.dao.commondb

import org.apache.ibatis.annotations.Mapper
import ru.planeta.model.common.ProjectPaymentTool

/*
 * Project payment tool DAO
 * Created by Alexey on 18.11.2015.
 */

@Mapper
interface ProjectPaymentToolDAO {

    fun selectAll(): List<ProjectPaymentTool>

    fun update(projectPaymentTool: ProjectPaymentTool): ProjectPaymentTool

    fun selectByPaymentMethodId(paymentMethodId: Long): List<ProjectPaymentTool>
}

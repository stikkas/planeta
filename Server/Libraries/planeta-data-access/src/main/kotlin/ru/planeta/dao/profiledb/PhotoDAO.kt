package ru.planeta.dao.profiledb

import org.apache.ibatis.annotations.Mapper
import org.apache.ibatis.annotations.Param
import ru.planeta.model.profile.media.Photo

/**
 * Common photo dao
 *
 * @author ds.kolyshev
 * Date: 27.10.11
 */
@Mapper
interface PhotoDAO {

    /**
     * Selects photo by its identifier
     *
     */
    fun selectPhotoById(@Param("profileId") profileId: Long, @Param("photoId") photoId: Long): Photo?

    /**
     * Selects photo by album's identifier
     *
     */
    fun selectPhotoByAlbumId(@Param("profileId") profileId: Long, @Param("albumId") albumId: Long, @Param("offset") offset: Int, @Param("limit") limit: Int): List<Photo>

    fun selectPhotoCountByAlbumId(@Param("profileId") profileId: Long, @Param("albumId") albumId: Long): Long

    /**
     * Updates views count
     *
     */
    fun updateViewsCount(@Param("profileId") profileId: Long, @Param("photoId") photoId: Long, @Param("viewsCount") viewsCount: Int)

    /**
     * Inserts photo
     *
     */
    fun insert(photo: Photo)

    /**
     * Updates photo album
     * Updated fields: title, tags, timeUpdated, imageUrl
     *
     */
    fun update(photo: Photo)

    /**
     * Deletes specified group's photo
     *
     */
    fun delete(@Param("profileId") profileId: Long, @Param("photoId") photoId: Long)
}

package ru.planeta.model.common

import com.fasterxml.jackson.annotation.JsonIgnore
import ru.planeta.commons.model.Gender
import ru.planeta.model.common.campaign.enums.CampaignStatus
import ru.planeta.model.common.campaign.enums.CampaignTargetStatus
import ru.planeta.model.common.delivery.LinkedDelivery
import ru.planeta.model.enums.OrderObjectType
import ru.planeta.model.enums.ProjectType
import ru.planeta.model.shop.enums.DeliveryStatus
import ru.planeta.model.shop.enums.DeliveryType
import ru.planeta.model.shop.enums.PaymentStatus
import ru.planeta.model.shop.enums.PaymentType
import java.math.BigDecimal
import java.util.ArrayList
import java.util.Date

/**
 * todo add comment
 */
class OrderInfoForProfilePage : TransactionalObject() {

    var orderId: Long = 0
    var orderType: OrderObjectType? = null
    var buyerId: Long = 0
    var paymentType = PaymentType.NOT_SET
    var paymentStatus = PaymentStatus.PENDING
    var errorCode: String? = null
    var totalPrice = BigDecimal.ZERO
    var timeAdded: Date? = null
    var timeUpdated: Date? = null
    var reserveTimeExpired: Date? = null

    var deliveryType = DeliveryType.NOT_SET
    var deliveryStatus = DeliveryStatus.PENDING
    var deliveryPrice = BigDecimal.ZERO
    @get:JsonIgnore
    var deliveryDepartmentId: Long = 0

    var trackingCode: String? = null
        set(trackingCode) {
            field = if (trackingCode != null && trackingCode.length == 0) null else trackingCode
        }
    var cashOnDeliveryCost: BigDecimal? = BigDecimal.ZERO
        set(cashOnDeliveryCost) {
            field = cashOnDeliveryCost ?: BigDecimal.ZERO
        }
    var deliveryComment: String? = null
    var projectType: ProjectType? = null

    var investInfo: InvestingOrderInfo? = null

    var promoCodeId: Long = 0
    var discount = BigDecimal.ZERO

    var buyerGender = Gender.NOT_SET
    var buyerAlias: String? = null
    var buyerName: String? = null
    var buyerImageUrl: String? = null
    var buyerEmail: String? = null
    var orderObjectCampaignId: Long = 0
    var orderObjectCampaignName: String? = null

    var linkedDelivery: LinkedDelivery? = null
    var questionToBuyer: String? = null

    var orderObjectName: String? = null
    var orderObjectsCount: Int = 0
    var orderObjectPrice: BigDecimal? = null
    var orderObjectComment: String? = null

    var campaignId: Long = 0
    var campaignStatus = CampaignStatus.DRAFT
    var imageUrl: String? = null
    var nameHtml: String? = null
    var webCampaignAlias: String? = null
    var shortDescriptionHtml: String? = null
    var targetAmount = BigDecimal.ZERO
    var collectedAmount = BigDecimal.ZERO
    var targetStatus = CampaignTargetStatus.NONE
    var timeFinish: Date? = null


    private var orderObjectsInfo: MutableList<OrderObjectInfo> = ArrayList()

    var deliveryAddress: DeliveryAddress? = null

    fun getOrderObjectsInfo(): List<OrderObjectInfo> {
        return orderObjectsInfo
    }

    fun setOrderObjectsInfo(orderObjectsInfo: MutableList<OrderObjectInfo>) {
        this.orderObjectsInfo = orderObjectsInfo
    }

    fun addOrderObjectInfo(orderObjectInfo: OrderObjectInfo) {
        orderObjectsInfo.add(orderObjectInfo)
    }

    /*public static OrderInfo createFrom(Order order) {
        OrderInfo result = new OrderInfo();

        result.setCreditTransactionId(order.getCreditTransactionId());
        result.setDebitTransactionId(order.getDebitTransactionId());
        result.setCancelCreditTransactionId(order.getCancelCreditTransactionId());
        result.setCancelDebitTransactionId(order.getCancelDebitTransactionId());

        result.setOrderId(order.getOrderId());
        result.setOrderType(order.getOrderType());
        result.setTopayTransactionId(order.getTopayTransactionId());
        result.setBuyerId(order.getBuyerId());
        result.setPaymentType(order.getPaymentType());
        result.setPaymentStatus(order.getPaymentStatus());
        result.setErrorCode(order.getErrorCode());
        result.setTotalPrice(order.getTotalPrice());
        result.setTimeAdded(order.getTimeAdded());
        result.setTimeUpdated(order.getTimeUpdated());
        result.setReserveTimeExpired(order.getReserveTimeExpired());

        result.setDeliveryStatus(order.getDeliveryStatus());
        result.setDeliveryType(order.getDeliveryType());
        result.setDeliveryPrice(order.getDeliveryPrice());
        result.setDeliveryDepartmentId(order.getDeliveryDepartmentId());

        result.setTrackingCode(order.getTrackingCode());
        result.setCashOnDeliveryCost(order.getCashOnDeliveryCost());
        result.setDeliveryComment(order.getDeliveryComment());

        result.setInvestInfo(order.getInvestInfo());
        result.setPromoCodeId(order.getPromoCodeId());
        result.setDiscount(order.getDiscount());

        return result;
    }*/
}

package ru.planeta.dao.profiledb

import org.apache.ibatis.annotations.Mapper
import org.apache.ibatis.annotations.Param
import ru.planeta.model.profile.ProfileForAdminsWithEmail

@Mapper
interface ProfileForAdminsDAO {
    fun selectByIds(@Param("list") profileIds: Collection<Long>): List<ProfileForAdminsWithEmail>

    fun selectList(@Param("offset") offset: Int, @Param("limit") limit: Int): List<ProfileForAdminsWithEmail>

    fun selectVipUsers(@Param("offset") offset: Int, @Param("limit") limit: Int): List<ProfileForAdminsWithEmail>

    fun selectPlanetaWorkersWithoutSuperAdmins(): List<ProfileForAdminsWithEmail>
}

package ru.planeta.dao

import org.apache.ibatis.annotations.Mapper
import org.apache.ibatis.annotations.Param
import ru.planeta.dto.PurchaseForProjectCardDTO

@Mapper
interface PurchaseDAO /*: BaseCommonDbDAO<PurchaseForProjectCardDTO>()*/ {
    fun selectMyPurchasesByCampaignId(@Param("profileId") profileId: Long, @Param("campaignId") campaignId: Long): List<PurchaseForProjectCardDTO>?
//        return selectCampaignById(Statements.Purchase.SELECT_MY_PURCHASES_FOR_CAMPAIGN,
//                getParameters("profileId", profileId, "campaignId", campaignId))
//    }
}

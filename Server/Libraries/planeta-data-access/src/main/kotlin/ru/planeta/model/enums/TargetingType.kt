package ru.planeta.model.enums

import java.util.*

/**
 * Created with IntelliJ IDEA.
 * User: michail
 * Date: 06.09.2017
 * Time: 13:32
 */
enum class TargetingType private constructor(override val code: Int) : Codable {

    ALL(1),
    AUTHORS(2),
    NOT_AUTHORS(4);


    companion object {

        private val lookup = HashMap<Int, TargetingType>()

        init {
            for (s in values()) {
                lookup[s.code] = s
            }
        }

        fun getByValue(code: Int): TargetingType? {
            return lookup[code]
        }

        fun getCodeByEnumSet(set: EnumSet<TargetingType>): Int {
            var statusCode = 0
            for (s in set) {
                statusCode = statusCode or s.code
            }
            return statusCode
        }

        fun getEnumSetFromCode(code: Int): EnumSet<TargetingType> {
            val set = EnumSet.noneOf(TargetingType::class.java)

            for (s in TargetingType.values()) {
                if (s.code and code != 0) {
                    set.add(s)
                }
            }
            return set
        }
    }
}

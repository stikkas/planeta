package ru.planeta.dao.commondb

import org.apache.ibatis.annotations.Mapper
import ru.planeta.model.common.Address

/**
 * @author a.savanovich
 * @see Address
 */
@Mapper
interface AddressDAO {

    /**
     * Selects address by id
     *
     * @param addressId
     * @return
     */
    fun select(addressId: Long): Address

    /**
     * Inserts address without any relation to any entity(User, Shop, etc.)
     *
     * @param address address
     */
    fun insert(address: Address)

    /**
     * Simply updates address
     *
     * @param address
     */
    fun update(address: Address)

    fun getLastPostAddress(buyerId: Long): Address

}

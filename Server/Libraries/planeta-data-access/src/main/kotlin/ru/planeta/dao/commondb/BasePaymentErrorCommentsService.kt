package ru.planeta.dao.commondb

import org.apache.ibatis.annotations.Param
import java.util.Date
import org.springframework.beans.factory.annotation.Autowired
import ru.planeta.model.commondb.PaymentErrorComments
import ru.planeta.model.commondb.PaymentErrorCommentsExample

abstract class BasePaymentErrorCommentsService {
    @Autowired
    protected var paymentErrorCommentsMapper: PaymentErrorCommentsMapper? = null

    val orderByClause: String
        get() = "payment_error_comment_id"

    //**************************************************************************************************
    //*********************************  Create ** Update ** Delete  *************************************
    //**************************************************************************************************
    fun insertPaymentErrorComments(paymentErrorComments: PaymentErrorComments) {
        paymentErrorComments.timeAdded = Date()
        paymentErrorComments.timeUpdated = Date()
        paymentErrorCommentsMapper!!.insertSelective(paymentErrorComments)
    }

    fun insertPaymentErrorCommentsAllFields(paymentErrorComments: PaymentErrorComments) {
        paymentErrorComments.timeAdded = Date()
        paymentErrorComments.timeUpdated = Date()
        paymentErrorCommentsMapper!!.insert(paymentErrorComments)
    }

    fun updatePaymentErrorComments(paymentErrorComments: PaymentErrorComments) {
        paymentErrorComments.timeUpdated = Date()
        paymentErrorCommentsMapper!!.updateByPrimaryKeySelective(paymentErrorComments)
    }

    fun updatePaymentErrorCommentsAllFields(paymentErrorComments: PaymentErrorComments) {
        paymentErrorComments.timeUpdated = Date()
        paymentErrorCommentsMapper!!.updateByPrimaryKey(paymentErrorComments)
    }

    fun insertOrUpdatePaymentErrorComments(paymentErrorComments: PaymentErrorComments) {
        paymentErrorComments.timeUpdated = Date()
        val selectedPaymentErrorComments = paymentErrorCommentsMapper!!.selectByPrimaryKey(paymentErrorComments.paymentErrorCommentId)
        if (selectedPaymentErrorComments == null) {
            paymentErrorComments.timeAdded = Date()
            paymentErrorCommentsMapper!!.insertSelective(paymentErrorComments)
        } else {
            paymentErrorCommentsMapper!!.updateByPrimaryKeySelective(paymentErrorComments)
        }
    }

    fun insertOrUpdatePaymentErrorCommentsAllFields(paymentErrorComments: PaymentErrorComments) {
        paymentErrorComments.timeUpdated = Date()
        val selectedPaymentErrorComments = paymentErrorCommentsMapper!!.selectByPrimaryKey(paymentErrorComments.paymentErrorCommentId)
        if (selectedPaymentErrorComments == null) {
            paymentErrorComments.timeAdded = Date()
            paymentErrorCommentsMapper!!.insert(paymentErrorComments)
        } else {
            paymentErrorCommentsMapper!!.updateByPrimaryKey(paymentErrorComments)
        }
    }

    fun deletePaymentErrorComments(id: Long?) {
        paymentErrorCommentsMapper!!.deleteByPrimaryKey(id)
    }

    //**************************************************************************************************
    //************************************ payment_error_comments ****************************************
    //************************************** SelectByPrimaryKey ******************************************
    //**************************************************************************************************
    fun selectPaymentErrorComments(paymentErrorCommentId: Long?): PaymentErrorComments {
        return paymentErrorCommentsMapper!!.selectByPrimaryKey(paymentErrorCommentId)
    }

    //**************************************************************************************************
    //************************************ payment_error_comments ****************************************
    //**************************************************************************************************
    private fun COMMENT_SECTION_PaymentErrorComments() {

    }

    fun getPaymentErrorCommentsExample(@Param("offset") offset: Int, @Param("limit") limit: Int): PaymentErrorCommentsExample {
        val example = PaymentErrorCommentsExample()
        example.offset = offset
        example.limit = limit
        example.or()
        example.orderByClause = orderByClause
        return example
    }

    fun selectPaymentErrorCommentsCount(): Int {
        return paymentErrorCommentsMapper!!.countByExample(getPaymentErrorCommentsExample(0, 0))
    }

    fun selectPaymentErrorCommentsList(@Param("offset") offset: Int, @Param("limit") limit: Int): List<PaymentErrorComments> {
        return paymentErrorCommentsMapper!!.selectByExample(getPaymentErrorCommentsExample(offset, limit))
    }

    //**************************************************************************************************
    //************************************ payment_error_comments ****************************************
    //************************************** Long paymentErrorId *****************************************
    //**************************************************************************************************
    private fun COMMENT_SECTION_PaymentErrorComments(paymentErrorId: Long?) {

    }

    fun getPaymentErrorCommentsExample(@Param("paymentErrorId") paymentErrorId: Long?, @Param("offset") offset: Int, @Param("limit") limit: Int): PaymentErrorCommentsExample {
        val example = PaymentErrorCommentsExample()
        example.offset = offset
        example.limit = limit
        example.or()
                .andPaymentErrorIdEqualTo(paymentErrorId)
        example.orderByClause = orderByClause
        return example
    }

    fun selectPaymentErrorCommentsCount(paymentErrorId: Long?): Int {
        return paymentErrorCommentsMapper!!.countByExample(getPaymentErrorCommentsExample(paymentErrorId, 0, 0))
    }

    fun selectPaymentErrorCommentsList(@Param("paymentErrorId") paymentErrorId: Long?, @Param("offset") offset: Int, @Param("limit") limit: Int): List<PaymentErrorComments> {
        return paymentErrorCommentsMapper!!.selectByExample(getPaymentErrorCommentsExample(paymentErrorId, offset, limit))
    }
}

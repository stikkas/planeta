package ru.planeta.model.enums

import java.util.EnumSet
import java.util.HashMap

/**
 * Enum ProfileStatus
 *
 * @author a.tropnikov
 */
enum class ProfileRelationStatus(val code: Int) {

    NOT_SET(0),
    SELF(1),
    REQUEST_IN(4),
    REQUEST_OUT(8),
    ACTIVE(16),
    BLOCKED_BY_ME(64),
    BLOCKED_BY_OTHER(128),
    SUBSCRIBER(256),
    MODERATOR(512),
    ADMIN(1024),
    // user is an artist (one of group creators)
    ARTIST(2048),
    // user is a subscriber
    REJECTED(4096),
    SUBSCRIPTION(8192),
    PROFILE_ADMIN(16384),
    NO_COMMENT(32768);


    companion object {

        private val lookup = HashMap<Int, ProfileRelationStatus>()

        init {
            for (s in EnumSet.allOf(ProfileRelationStatus::class.java)) {
                lookup[s.code] = s
            }
        }

        fun getByValue(code: Int): ProfileRelationStatus? {
            return lookup[code]
        }

        fun getCodeByEnumSet(set: EnumSet<ProfileRelationStatus>?): Int {
            if (set == null) {
                return 0
            }
            var statusCode = 0
            for (s in set) {
                statusCode = statusCode or s.code
            }

            return statusCode
        }

        fun getEnumSetFromCode(code: Int): EnumSet<ProfileRelationStatus> {
            val set = EnumSet.noneOf(ProfileRelationStatus::class.java)

            for (s in ProfileRelationStatus.values()) {
                if (s.code and code != 0) {
                    set.add(s)
                }
            }

            return set
        }

        /**
         * WTF is this?
         *
         * @param relationStatus
         * @return
         */
        fun getPermissionLevel(relationStatus: EnumSet<ProfileRelationStatus>): PermissionLevel {
            if (relationStatus.contains(SELF) || relationStatus.contains(ADMIN)) {
                return PermissionLevel.ADMINS
            }

            if (relationStatus.contains(MODERATOR)) {
                return PermissionLevel.MODERATORS
            }

            return if (relationStatus.contains(ACTIVE)
                    || relationStatus.contains(ARTIST)
                    || relationStatus.contains(REQUEST_IN)) {
                PermissionLevel.FRIENDS
            } else PermissionLevel.EVERYBODY

        }
    }
}

package ru.planeta.dto

import java.util.*

/**
 * Использутеся для отображения собственных проектов пользователся в личном кабинете
 */
class MyPurchasedProjectCardDTO : ProjectCardDTO() {
    /**
     * идентификатор последней новости
     */
    var newsId: Long? = null

    /**
     * название последней новости
     */
    var newsTitle: String? = null

    /**
     * дата последней новости
     */
    var newsTimeAdded: Date? = null

    /**
     * время окончания проекта
     */
    var timeFinish: Date? = null

    /**
     * список купленных акций
     */
    var shares: List<PurchaseForProjectCardDTO>? = null

    var timeLastPurchased: Date? = null
}

package ru.planeta.model.profile

/**
 * Created with IntelliJ IDEA.
 * User: michail
 * Date: 14.06.16
 * Time: 13:02
 */
class Phone {
    var phoneNumber: String? = null

    val normalizedPhone: String
        get() = phoneNumber?.replace("[(,),\\-, ]".toRegex(), "") ?: ""

    constructor() {}

    constructor(phoneNumber: String) {
        this.phoneNumber = phoneNumber
    }
}

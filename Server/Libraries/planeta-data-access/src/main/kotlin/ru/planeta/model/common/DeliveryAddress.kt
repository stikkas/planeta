package ru.planeta.model.common

/**
 * Date: 27.10.2014
 * Time: 11:42
 */
class DeliveryAddress {
    var id: Long = 0
    var orderId: Long = 0
    var fio: String? = null
    var phone: String? = null
    var country: String? = null
    var zipCode: String? = null
    var city: String? = null
    var address: String? = null

    override fun toString(): String {
        return "DeliveryAddress{" +
                "id=" + id +
                ", orderId=" + orderId +
                ", fio='" + fio + '\''.toString() +
                ", phone='" + phone + '\''.toString() +
                ", country='" + country + '\''.toString() +
                ", zipCode='" + zipCode + '\''.toString() +
                ", city='" + city + '\''.toString() +
                ", address='" + address + '\''.toString() +
                '}'.toString()
    }
}

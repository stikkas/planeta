package ru.planeta.dao.commondb

import org.apache.ibatis.annotations.Mapper
import org.apache.ibatis.annotations.Param
import ru.planeta.model.common.ProfileBalance

import java.math.BigDecimal

/**
 * Interface ProfileBalanceDAO
 *
 * @author a.tropnikov
 */
@Mapper
interface ProfileBalanceDAO {

    fun select(profileId: Long): BigDecimal

    fun selectForUpdate(profileId: Long): BigDecimal

    fun insert(profileId: Long)

    fun update(@Param("profileId") profileId: Long, @Param("balance") balance: BigDecimal)

    fun selectFrozenAmount(profileId: Long): BigDecimal

    fun updateFrozenAmount(@Param("profileId") profileId: Long, @Param("frozenAmount") frozenAmount: BigDecimal)

    fun delete(profileId: Long)

    fun getPersonalCabinet(profileId: Long): ProfileBalance
}

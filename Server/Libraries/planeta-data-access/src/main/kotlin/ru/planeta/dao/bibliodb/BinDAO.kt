package ru.planeta.dao.bibliodb

import org.apache.ibatis.annotations.Mapper
import ru.planeta.model.bibliodb.Bin
import java.util.*

/**
 *
 * User: Serge Blagodatskih<stikkas17></stikkas17>@gmail.com><br></br>
 * Date: 27.04.16<br></br>
 * Time: 14:48
 */
@Mapper
interface BinDAO {

    fun insertBin(bin: Bin)

    fun updateBin(bin: Bin)

    fun insertBookRelations(bin: Bin)

    fun insertLibraryRelations(bin: Bin)

    fun deleteRelations(bin: Bin)

    fun deleteBin(bin: Bin)

    fun findByProfileId(profileId: Long): Bin

    fun findById(binId: Long): Bin

    fun deleteAllOlderDate(removeBefore: Date)
}

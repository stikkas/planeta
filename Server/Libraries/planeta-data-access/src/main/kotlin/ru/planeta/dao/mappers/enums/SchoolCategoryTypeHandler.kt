package ru.planeta.dao.mappers.enums

import org.apache.ibatis.type.JdbcType
import org.apache.ibatis.type.TypeHandler
import ru.planeta.model.school.SchoolCategory

import java.sql.CallableStatement
import java.sql.PreparedStatement
import java.sql.ResultSet
import java.sql.SQLException

/**
 * Created with IntelliJ IDEA.
 * Date: 27.08.2015
 * Time: 15:53
 * To change this template use File | Settings | File Templates.
 */
class SchoolCategoryTypeHandler : TypeHandler<SchoolCategory> {
    @Throws(SQLException::class)
    override fun setParameter(ps: PreparedStatement, i: Int, parameter: SchoolCategory, jdbcType: JdbcType) {
        ps.setInt(i, parameter.code)
    }

    @Throws(SQLException::class)
    override fun getResult(rs: ResultSet, columnName: String): SchoolCategory? {
        return SchoolCategory.getByCode(rs.getInt(columnName))
    }

    @Throws(SQLException::class)
    override fun getResult(rs: ResultSet, columnIndex: Int): SchoolCategory? {
        return SchoolCategory.getByCode(rs.getInt(columnIndex))
    }

    @Throws(SQLException::class)
    override fun getResult(cs: CallableStatement, columnIndex: Int): SchoolCategory? {
        return SchoolCategory.getByCode(cs.getInt(columnIndex))
    }
}

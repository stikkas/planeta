package ru.planeta.model.common.auth

import java.util.EnumSet
import java.util.TreeMap

/**
 * Date: 02.10.12
 *
 * @author s.kalmykov
 */
enum class CredentialStatus private constructor(// credentials are need to be confirmed (has credentialStatus PENDING before)

        val code: Int) {
    DELETED(0), // credentials were deleted with user, default credentialType, used if code from base is old and enum has no such value
    ACTIVE(1), // credentials are confirmed
    PENDING(2);


    companion object {

        private val lookup = TreeMap<Int, CredentialStatus>()

        init {
            for (s in EnumSet.allOf(CredentialStatus::class.java)) {
                lookup[s.code] = s
            }
        }

        fun getByValue(code: Int): CredentialStatus {
            val mayBeStatus = lookup[code]
            return mayBeStatus ?: DELETED
        }
    }
}

package ru.planeta.model.common.campaign

class CampaignWithNewEventsCount : Campaign {
    var newEventsCount: Int = 0
    var newNewsCount: Int = 0
    var newCommentCount: Int = 0
    var newPurchaseCount: Int = 0
    var newAmount: Int = 0

    constructor(campaignId: Long, newAmount: Int, newNewsCount: Int, newCommentCount: Int, newPurchaseCount: Int) {
        this.campaignId = campaignId
        this.newAmount = newAmount
        this.newNewsCount = newNewsCount
        this.newCommentCount = newCommentCount
        this.newPurchaseCount = newPurchaseCount
        this.newEventsCount = newNewsCount + newCommentCount + newPurchaseCount
    }

    constructor(campaign: Campaign) : super(campaign) {}
}

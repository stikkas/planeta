package ru.planeta.dao.commondb

import org.apache.ibatis.annotations.Mapper
import org.apache.ibatis.annotations.Param
import ru.planeta.model.common.PlanetaCampaignManager
import ru.planeta.model.common.PlanetaManager
import ru.planeta.model.common.PlanetaManagerRelation
import ru.planeta.model.common.campaign.CampaignTag

/**
 * Created with IntelliJ IDEA.
 * User: Андрей Arefyev
 * Date: 19.02.13
 * Time: 3:23
 */
@Mapper
interface PlanetaManagersDAO {

    /**
     * Returns planeta manager for specified `campaignTag` if one exists, otherwise returns null
     *
     * @param campaignTag campaign tag
     * @return planeta manager info
     */
    fun getCampaignManager(campaignTag: CampaignTag): PlanetaManager?

    fun selectPlanetaCampaignTagManagerRelations(): List<PlanetaManagerRelation<*>>

    fun updateManagerForCurrentObject(managerForCurrentObject: PlanetaCampaignManager)

    fun insertManagerForCurrentObject(managerForCurrentObject: PlanetaCampaignManager)

    fun getManagerForCurrentCampaign(campaignId: Long): PlanetaManager

    fun getManagersForCurrentCampaigns(campaignIdList: List<Long>): List<PlanetaManager>

    fun selectManagersByCampaignTags(): List<PlanetaManager>

    fun selectManagersForGroupCategories(): List<PlanetaManager>

    fun selectAllManagers(active: Boolean?): List<PlanetaManager>

    fun getManagerId(profileId: Long?): Long

    fun updateManagerForTag(@Param("managerId") managerId: Long, @Param("tagId") tagId: Long)

    fun insertNewManager(@Param("name") name: String, @Param("fullName") fullName: String, @Param("email") email: String, @Param("profileId") profileId: Long?): PlanetaManager

    fun updateManager(manager: PlanetaManager)

    fun selectManagerByProfileId(profileId: Long): PlanetaManager

    fun selectManagerById(id: Long): PlanetaManager
}

package ru.planeta.model.profile

/**
 * @author Andrew.Arefyev@gmail.com
 * 11.10.13 20:42
 */
interface ISocial : ICommentable, IVotable {

    var commentsCount: Int
}

package ru.planeta.model.common.campaign

import ru.planeta.model.enums.OrderObjectType

class MyPurchasesAndRewardsCount {

    var count: Int = 0
    var orderObjectType: OrderObjectType? = null
        private set
    var campaignTagMnemonic: String? = null

    fun setOrderObjectType(code: Int) {
        this.orderObjectType = OrderObjectType.getByValue(code)
    }
}

package ru.planeta.model.common.campaign

/**
 * Represents fund campaign
 *
 *
 * Created by IntelliJ IDEA.
 * User: atropnikov
 * Date: 21.03.12
 * Time: 12:24
 */
class CampaignSubscribe {
    var profileId: Long = 0
    var campaignId: Long = 0
}

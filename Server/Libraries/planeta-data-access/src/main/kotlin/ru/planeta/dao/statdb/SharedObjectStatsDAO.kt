package ru.planeta.dao.statdb

import ru.planeta.model.stat.SharedObjectStats

/**
 * User: m.shulepov
 * Date: 23.10.12
 * Time: 16:15
 */
interface SharedObjectStatsDAO {

    /**
     * Inserts shared object stats
     *
     * @param sharedObjectStats
     */
    fun insert(sharedObjectStats: SharedObjectStats)

    /**
     * Updates count of shared object
     *
     * @param sharedObjectId
     */
    fun updateCount(sharedObjectId: Long)

    /**
     * Returns shared object by `sharedObjectId`
     *
     * @param sharedObjectId
     * @return
     */
    fun selectById(sharedObjectId: Long): SharedObjectStats?

    /**
     * Returns shared object by url
     *
     * @param url
     * @return
     */
    fun selectByUrl(url: String): SharedObjectStats?

}

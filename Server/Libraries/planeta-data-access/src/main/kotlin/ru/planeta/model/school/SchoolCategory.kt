package ru.planeta.model.school

import ru.planeta.model.enums.Codable
import java.util.*

/**
 * Created with IntelliJ IDEA.
 * Date: 26.08.2015
 * Time: 19:06
 * To change this template use File | Settings | File Templates.
 */
enum class SchoolCategory private constructor(override val code: Int) : Codable {
    SOCIAL(1),
    CITY(2),
    REGION(3),
    BUSINESS(4);


    companion object {

        private val lookup = HashMap<Int, SchoolCategory>()

        init {
            for (customMetaTagType in values()) {
                lookup[customMetaTagType.code] = customMetaTagType
            }
        }

        fun getByCode(code: Int): SchoolCategory? {
            return lookup[code]
        }
    }

}

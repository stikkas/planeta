package ru.planeta.model.common

import com.fasterxml.jackson.annotation.JsonFormat
import ru.planeta.model.shop.enums.PaymentStatus

import java.util.Date

class IviStat {
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    var date: Date? = null
    var email: String? = null
    var `object`: String? = null
    var price: String? = null
    var status: PaymentStatus? = null
}

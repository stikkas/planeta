package ru.planeta.dao.commondb

import org.apache.ibatis.annotations.Mapper
import org.apache.ibatis.annotations.Param
import ru.planeta.model.common.Banner
import ru.planeta.model.enums.BannerStatus
import ru.planeta.model.enums.BannerType
import java.util.Date
import java.util.EnumSet

/**
 * User: a.savanovich
 * Date: 12.05.12
 * Time: 13:49
 */
@Mapper
interface BannerDAO {

    /**
     * Insert banner into db
     */
    fun insert(banner: Banner)

    /**
     * selectCampaignById current banner
     */
    fun select(bannerId: Long): Banner?

    fun selectList(@Param("bannerStatuses") bannerStatuses: EnumSet<BannerStatus>, @Param("offset") offset: Int,
                   @Param("limit") limit: Int): List<Banner>


    fun selectList(@Param("type") type: BannerType?, @Param("bannerStatuses") bannerStatuses: EnumSet<BannerStatus>,
                   @Param("timeStart") timeStart: Date, @Param("timeFinish") timeFinish: Date,
                   @Param("offset") offset: Int, @Param("limit") limit: Int): List<Banner>


    /**
     * deleteByProfileId banner from db
     */
    fun delete(bannerId: Long)

    /**
     * update banner
     */
    fun update(banner: Banner)

    fun setShowCounter(@Param("bannerId") bannerId: Long, @Param("i") i: Int)
}


package ru.planeta.model.profile.location

import java.util.EnumSet
import java.util.HashMap

/**
 * @author Andrew.Arefyev@gmail.com
 * 29.10.13 17:17
 */
enum class LocationType private constructor(val code: Int) {
    WORLD(0), CITY(1), REGION(2), COUNTRY(3), GLOBAL_REGION(4);


    companion object {

        private val lookup = HashMap<Int, LocationType>()

        init {
            for (s in EnumSet.allOf(LocationType::class.java)) {
                lookup[s.code] = s
            }
        }

        fun getByValue(code: Int): LocationType? {
            return lookup[code]
        }
    }

}

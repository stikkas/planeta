package ru.planeta.dao.mappers.enums.bibliodb

import java.sql.CallableStatement
import java.sql.PreparedStatement
import java.sql.ResultSet
import java.sql.SQLException

import org.apache.ibatis.type.JdbcType
import org.apache.ibatis.type.TypeHandler

import ru.planeta.model.bibliodb.enums.BiblioObjectStatus

class BiblioObjectStatusHandler : TypeHandler<BiblioObjectStatus> {

    @Throws(SQLException::class)
    override fun getResult(rs: ResultSet, columnIndex: Int): BiblioObjectStatus? {
        return BiblioObjectStatus.getByValue(rs.getInt(columnIndex))
    }

    @Throws(SQLException::class)
    override fun getResult(cs: CallableStatement, columnIndex: Int): BiblioObjectStatus? {
        return BiblioObjectStatus.getByValue(cs.getInt(columnIndex))
    }

    @Throws(SQLException::class)
    override fun getResult(rs: ResultSet, columnName: String): BiblioObjectStatus? {
        return BiblioObjectStatus.getByValue(rs.getInt(columnName))
    }

    @Throws(SQLException::class)
    override fun setParameter(ps: PreparedStatement, i: Int, parameter: BiblioObjectStatus, jdbcType: JdbcType) {
        ps.setInt(i, parameter.code)
    }

}

package ru.planeta.model.mail

import ru.planeta.model.enums.Codable

import java.util.EnumSet
import java.util.HashMap

/**
 * Created with IntelliJ IDEA.
 * User: michail
 * Date: 19.05.16
 * Time: 15:49
 */
enum class MailMessageStatus private constructor(// email delivered

        override val code: Int) : Codable {
    SENT(1), // email sent
    FAIL(2), // email sending failed
    BOUNCE(3), // bounce notification received; need to unsubscribe
    COMPLAINT(4), // complaint notification received; need to unsubscribe
    DELIVERY(5);


    companion object {

        private val lookup = HashMap<Int, MailMessageStatus>()

        init {
            for (s in EnumSet.allOf(MailMessageStatus::class.java)) {
                lookup[s.code] = s
            }
        }

        fun getByValue(status: Int): MailMessageStatus? {
            return lookup[status]
        }
    }
}

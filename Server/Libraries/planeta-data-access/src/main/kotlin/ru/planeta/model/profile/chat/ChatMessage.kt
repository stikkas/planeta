package ru.planeta.model.profile.chat

import ru.planeta.model.profile.ProfileObject

import java.util.Date

/**
 * This class represents chat message
 *
 * @author m.shulepov
 */
class ChatMessage : ProfileObject() {

    var chatId: Long = 0
    var messageId: Long = 0
    var messageText: String? = null
    var messageTextHtml: String? = null
    var isDeleted: Boolean = false
    var timeAdded: Date? = null
}

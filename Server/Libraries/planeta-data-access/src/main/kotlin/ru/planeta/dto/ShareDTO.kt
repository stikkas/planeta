package ru.planeta.dto

import ru.planeta.model.common.delivery.LinkedDelivery
import java.math.BigDecimal
import java.util.*

/**
 * Для отображения информации об вознаграждении на карточке вознаграждения
 */
class ShareDTO {
    var shareId: Long? = null
    var nameHtml: String? = null
    var descriptionHtml: String? = null
    var imageUrl: String? = null
    var estimatedDeliveryTime: Date? = null
    var price: BigDecimal? = null
    var leftCount: Int = 0
    var purchasedCount: Int = 0
    var linkedDeliveries: List<LinkedDelivery>? = null
}


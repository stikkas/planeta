package ru.planeta.model.common

import ru.planeta.model.enums.ProjectType
import ru.planeta.model.enums.TopayTransactionStatus

import java.io.Serializable
import java.math.BigDecimal
import java.util.Date

/**
 * Created by IntelliJ IDEA.
 * User: atropnikov
 * Date: 22.12.11
 * Time: 16:13
 */
class TopayTransaction : Serializable {

    var transactionId: Long = 0
    var profileId: Long = 0
    var orderId: Long = 0
    var amountNet: BigDecimal? = null
    var amountFee: BigDecimal? = null
    var status = TopayTransactionStatus.NEW
    var debitTransactionId: Long = 0
    var timeAdded: Date? = null
    var timeUpdated: Date? = null
    var expireDate: Date? = null

    var projectType: ProjectType? = null
    var param1: String? = null
    var param2: String? = null
    var comment: String? = null

    var paymentToolCode = ""
    var paymentMethodId: Long = -1
    var paymentProviderId: Long = -1

    var extTransactionId: String? = null
    var extBaseTransactionId: String? = null
    var extErrorCode: Int = 0
    var extErrorMessage: String? = null

    var isTlsSupported = true

    val isExpired: Boolean
        get() = expireDate != null && expireDate!!.time < System.currentTimeMillis()

    companion object {

        private const val serialVersionUID = 1L
    }
}

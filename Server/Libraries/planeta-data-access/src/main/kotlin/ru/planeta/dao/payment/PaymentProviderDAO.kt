package ru.planeta.dao.payment

import org.apache.ibatis.annotations.Mapper
import ru.planeta.model.common.PaymentProvider

/**
 * Created with IntelliJ IDEA.
 * User: a.volkov
 * Date: 24.04.14
 * Time: 12:47
 */
@Mapper
interface PaymentProviderDAO {

    fun select(type: PaymentProvider.Type): PaymentProvider

    fun select(providerId: Long): PaymentProvider

    fun update(provider: PaymentProvider): PaymentProvider

    fun selectAll(): List<PaymentProvider>
}

package ru.planeta.dao.mappers.enums.charitydb

import java.sql.CallableStatement
import java.sql.PreparedStatement
import java.sql.ResultSet
import java.sql.SQLException

import org.apache.ibatis.type.JdbcType
import org.apache.ibatis.type.TypeHandler

import ru.planeta.model.charity.enums.CharityEventType

class CharityEventTypeHandler : TypeHandler<CharityEventType> {

    @Throws(SQLException::class)
    override fun getResult(rs: ResultSet, columnIndex: Int): CharityEventType? {
        return CharityEventType.getByValue(rs.getInt(columnIndex))
    }

    @Throws(SQLException::class)
    override fun getResult(cs: CallableStatement, columnIndex: Int): CharityEventType? {
        return CharityEventType.getByValue(cs.getInt(columnIndex))
    }

    @Throws(SQLException::class)
    override fun setParameter(ps: PreparedStatement, i: Int, parameter: CharityEventType, jdbcType: JdbcType) {
        ps.setInt(i, parameter.code)
    }

    @Throws(SQLException::class)
    override fun getResult(rs: ResultSet, columnName: String): CharityEventType? {
        return CharityEventType.getByValue(rs.getInt(columnName))
    }
}

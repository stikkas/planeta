package ru.planeta.dao

/**
 * Created with IntelliJ IDEA.
 * User: a.volkov
 * Date: 06.05.14
 * Time: 12:43
 */
interface SessionDAO {
    fun put(key: String, `object`: Any)

    operator fun get(key: String): Any?

    fun remove(key: String)
}

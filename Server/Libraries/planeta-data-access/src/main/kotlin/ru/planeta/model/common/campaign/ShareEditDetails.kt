package ru.planeta.model.common.campaign

import ru.planeta.model.common.delivery.BaseDelivery
import ru.planeta.model.common.delivery.LinkedDelivery

/**
 * @author Andrew.Arefyev@gmail.com
 * 05.08.13 19:41
 */
open class ShareEditDetails : ru.planeta.model.common.campaign.ShareDetails {

    var deliverToCustomer: Boolean = false
    var pickupByCustomer: Boolean = false

    override var linkedDeliveries: List<LinkedDelivery>?
        get() = super.linkedDeliveries
        set(linkedDeliveries) {
            super.linkedDeliveries = linkedDeliveries
            if (linkedDeliveries != null) {
                for (linkedDelivery in linkedDeliveries) {
                    if (linkedDelivery.serviceId == BaseDelivery.DEFAULT_DELIVERY_SERVICE_ID) {
                        deliverToCustomer = linkedDelivery.isEnabled
                    } else if (linkedDelivery.serviceId == BaseDelivery.DEFAULT_CUSTOMER_PICKUP_SERVICE_ID) {
                        pickupByCustomer = linkedDelivery.isEnabled
                    }
                }
            }
        }

    constructor(share: Share) : super(share) {
        if (share is ShareEditDetails) {
            deliverToCustomer = share.deliverToCustomer
            pickupByCustomer = share.pickupByCustomer
        }
    }

    constructor() : super() {}

}

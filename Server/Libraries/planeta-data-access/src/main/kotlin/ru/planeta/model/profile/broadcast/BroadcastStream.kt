package ru.planeta.model.profile.broadcast

import ru.planeta.model.enums.ObjectType
import ru.planeta.model.enums.PermissionLevel
import ru.planeta.model.profile.ProfileObject
import ru.planeta.model.profile.broadcast.enums.BroadcastRecordStatus
import ru.planeta.model.profile.broadcast.enums.BroadcastStatus
import ru.planeta.model.profile.broadcast.enums.BroadcastType

import java.util.Date

/**
 * Broadcast stream
 *
 * @author ds.kolyshev
 * Date: 19.03.12
 */
class BroadcastStream : ProfileObject() {
    var streamId: Long = 0
    var broadcastId: Long = 0
    var broadcastUrl: String? = null
    var broadcastType: BroadcastType? = BroadcastType.VIDEO
    var broadcastStatus: BroadcastStatus? = BroadcastStatus.NOT_STARTED
    var broadcastRecordStatus: BroadcastRecordStatus? = BroadcastRecordStatus.WILL_BE_RECORD
    var name: String? = null
    var description: String? = null
    var embedVideoHtml: String? = null
    var imageUrl: String? = null
    var imageId: Long = 0
    var videoUrl: String? = null
    var videoId: Long = 0
    var timeAdded: Date? = null
    var timeUpdated: Date? = null
    var tooltip: String? = null
    @Deprecated("")
    var timeBegin: Date? = null
    @Deprecated("")
    var timeEnd: Date? = null
    var viewPermission = PermissionLevel.EVERYBODY

    internal var broadcastTypeCode: Int?
        get() = broadcastType?.code
        set(code) {
            broadcastType = BroadcastType.getByValue(code)
        }

    internal var broadcastStatusCode: Int?
        get() = broadcastStatus?.code
        set(code) {
            broadcastStatus = BroadcastStatus.getByValue(code)
        }

    internal var broadcastRecordStatusCode: Int?
        get() = broadcastRecordStatus?.code
        set(code) {
            broadcastRecordStatus = BroadcastRecordStatus.getByValue(code)
        }

    internal var viewPermissionCode: Int
        get() = viewPermission.code
        set(code) {
            viewPermission = PermissionLevel.getByValue(code)
        }

    val streamUrlId: String
        get() = "livestream$streamId"

    val dumpFileName: String
        get() = "$streamUrlId.flv"

    val millisecondsToStart: Long
        @Deprecated("")
        get() = timeBegin!!.time - Date().time

    val duration: Long
        @Deprecated("")
        get() = timeEnd!!.time - timeBegin!!.time

    override var voteObjectType: ObjectType?
        get() = ObjectType.BROADCAST_STREAM
        set(value: ObjectType?) {
            super.voteObjectType = value
        }
}

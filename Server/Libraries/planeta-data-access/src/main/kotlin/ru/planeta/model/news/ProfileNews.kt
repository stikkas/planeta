package ru.planeta.model.news

import com.fasterxml.jackson.core.JsonProcessingException
import com.fasterxml.jackson.core.type.TypeReference
import com.fasterxml.jackson.databind.ObjectMapper

import java.io.IOException
import java.util.Date
import java.util.HashMap

/**
 * User: a.volkov
 * Date: 13.11.2014
 * Time: 13:36
 */
open class ProfileNews {

    var id: Long = 0
    var type: Type? = null
    var profileId: Long = 0
    var objectId: Long = 0
    open var campaignId: Long = 0
    var isRead: Boolean = false
    var isDeleted: Boolean = false
    var timeAdded: Date? = null
    private var extraParams: String? = null
    var extraParamsMap: Map<String, String> = HashMap()

    // !!!DANGER!!!. if remove or add type, also change DB. profiledb.profile_news_type
    enum class Type private constructor(val isInternal: Boolean = false) {
        CAMPAIGN_CREATED(true),
        CAMPAIGN_ON_MODERATION(true),
        CAMPAIGN_PAUSED(true),
        CAMPAIGN_ON_REWORK(true),
        CAMPAIGN_DECLINED(true),
        CAMPAIGN_DELETED(true),
        CAMPAIGN_APPROVED(true),
        CAMPAIGN_STARTED,
        CAMPAIGN_FINISHED_SUCCESS,
        CAMPAIGN_FINISHED_FAIL,
        CAMPAIGN_RESUMED,
        CAMPAIGN_REACHED_15(true),
        CAMPAIGN_REACHED_20(true),
        CAMPAIGN_REACHED_25(true),
        CAMPAIGN_REACHED_35(true),
        CAMPAIGN_REACHED_50,
        CAMPAIGN_REACHED_75(true),
        CAMPAIGN_REACHED_100,
        SHARE_CHANGED(true),
        SHARE_DELETED(true),
        SHARE_ADDED(true),
        SHARE_PURCHASED(true),
        POST,
        ADMIN_CHANGE_USER_STATUS(true),
        ADMIN_CHANGE_USER_PASSWORD(true),
        ADMIN_CHANGE_USER_EMAIL(true),
        ADMIN_DECREASE_USER_BALANCE(true),
        ADMIN_FREEZE_USER_AMOUNT(true),
        ADMIN_UNFREEZE_USER_AMOUNT(true),
        ADMIN_DECREASE_USER_FROZEN_AMOUNT(true),
        ADMIN_GIFT_MONEY_FOR_USER(true),
        ADMIN_LOGIN_AS_USER(true),
        ADMIN_CANCEL_PAYMENT(true),
        ADMIN_PROCESS_PAYMENT(true),
        ADMIN_SAVE_BROADCAST_INFO(true),
        ADMIN_REMOVE_BROADCAST(true),
        ADMIN_START_BROADCAST(true),
        ADMIN_STOP_BROADCAST(true),
        ADMIN_PAUSE_BROADCAST(true),
        ADMIN_CREATE_NEW_BONUS(true),
        ADMIN_EDIT_BONUS(true),
        ADMIN_DELETE_BONUS(true),
        ADMIN_SIMPLE_CHANGE_CAMPAIGN_STATUS(true),
        ADMIN_CHANGE_CAMPAIGN_DESCRIPTION_AND_META_DATA(true),
        ADMIN_CHANGE_CAMPAIGN_DATA(true),
        ADMIN_SAVE_PRODUCT(true),
        ADMIN_DELETE_PRODUCT(true),
        ADMIN_CHANGE_ORDER_DELIVERY_STATUS(true),
        ADD_CAMPAIGN_CONTRACTOR(true),
        CHANGE_CAMPAIGN_CONTRACTOR(true),
        SAVE_CAMPAIGN(true),
        SAVE_DRAFT_CAMPAIGN(true),
        CHANGE_CAMPAIGN_POST(true),
        DELETE_CAMPAIGN_POST(true),
        SEND_CAMPAIGN_NEWS_NOTIFICATION(true),
        DELETE_COMMENT(true),
        UPDATE_ORDER_STATUS_COMPLETE(true),
        UPDATE_ORDER_STATUS_IN_TRANSIT(true),
        UPDATE_ORDER_STATUS_CANCEL(true),
        UPDATE_ORDER_STATUS_GIVEN(true),
        PROFILE_SET_ADMIN_SUBSCRIBER(true),
        SCHOOL_DOWNLOAD_PDF(true),
        SUCCESS_PAGE_SHOW(true),
        FAIL_PAGE_SHOW(true),
        PROFILE_SETTINGS_CHANGED(true),
        PROFILE_SUBSCRIPTIONS_CHANGED(true),
        PROFILE_PASSWORD_CHANGED(true),
        ADMIN_SET_ADMIN_SUBSCRIBER(true),
        MAIL_SPAMMER_BLOCKED(true),
        DIALOG_SPAMMER_BLOCKED(true),
        SPAMMER_UNBLOCKED(true),
        CAMPAIGN_INTERACTIVE_STEP_START(true),
        CAMPAIGN_INTERACTIVE_STEP_NAME(true),
        CAMPAIGN_INTERACTIVE_STEP_EMAIL(true),
        CAMPAIGN_INTERACTIVE_STEP_PROCEED(true),
        CAMPAIGN_INTERACTIVE_STEP_INFO(true),
        CAMPAIGN_INTERACTIVE_STEP_DURATION(true),
        CAMPAIGN_INTERACTIVE_STEP_PRICE(true),
        CAMPAIGN_INTERACTIVE_STEP_VIDEO(true),
        CAMPAIGN_INTERACTIVE_STEP_DESCRIPTION(true),
        CAMPAIGN_INTERACTIVE_STEP_REWARD(true),
        CAMPAIGN_INTERACTIVE_STEP_COUNTERPARTY(true),
        CAMPAIGN_INTERACTIVE_STEP_CHECK(true),
        CAMPAIGN_INTERACTIVE_STEP_FINAL(true),
        CAMPAIGN_INTERACTIVE_STEP_FINAL_DRAFT(true),
        SAVE_CAMPAIGN_INTERACTIVE_STEP_INFO(true),
        SAVE_CAMPAIGN_INTERACTIVE_STEP_DURATION(true),
        SAVE_CAMPAIGN_INTERACTIVE_STEP_PRICE(true),
        SAVE_CAMPAIGN_INTERACTIVE_STEP_VIDEO(true),
        SAVE_CAMPAIGN_INTERACTIVE_STEP_DESCRIPTION(true),
        SAVE_CAMPAIGN_INTERACTIVE_STEP_REWARD(true),
        SAVE_CAMPAIGN_INTERACTIVE_STEP_COUNTERPARTY(true),
        CREATE_DRAFT_CAMPAIGN_INTERACTIVE_STEP_INFO(true),
        ADMIN_CHANGE_QUIZ_INFO(true),
        PROFILE_FILE_DELETED(true),
        MANAGER_FOR_CAMPAIGN_UPDATED(true),
        CHANGED_PRODUCT_TAGS(true)
    }

    constructor() {}

    constructor(type: Type, profileId: Long, objectId: Long, campaignId: Long) {
        this.type = type
        this.profileId = profileId
        this.objectId = objectId
        this.campaignId = campaignId
    }

    constructor(type: Type, profileId: Long, objectId: Long) {
        this.type = type
        this.profileId = profileId
        this.objectId = objectId
    }

    constructor(profileNews: ProfileNews) {
        id = profileNews.id
        type = profileNews.type
        profileId = profileNews.profileId
        objectId = profileNews.objectId
        isRead = profileNews.isRead
        timeAdded = profileNews.timeAdded
        extraParams = profileNews.getExtraParams()
    }

    fun getExtraParams(): String? {
        if (extraParams == null) {
            serializeExtraParams()
        }
        return extraParams
    }

    fun setExtraParams(extraParams: String) {
        this.extraParams = extraParams
        parseExtraParams()
    }

    override fun equals(o: Any?): Boolean {
        if (this === o) return true
        if (o == null || javaClass != o.javaClass) return false

        val that = o as ProfileNews?

        return id == that!!.id

    }

    override fun hashCode(): Int {
        return (id xor id.ushr(32)).toInt()
    }

    private fun parseExtraParams() {
        val om = ObjectMapper()
        try {
            this.extraParamsMap = om.readValue(this.extraParams, object : TypeReference<Map<String, String>>() {

            })
        } catch (e: IOException) {
            this.extraParamsMap = HashMap()
        }

    }

    private fun serializeExtraParams() {
        val om = ObjectMapper()
        try {
            this.extraParams = om.writeValueAsString(extraParamsMap)
        } catch (e: JsonProcessingException) {
            this.extraParams = ""
        }

    }
}

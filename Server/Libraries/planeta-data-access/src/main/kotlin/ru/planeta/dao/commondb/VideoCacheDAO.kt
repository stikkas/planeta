package ru.planeta.dao.commondb

import org.apache.ibatis.annotations.Mapper
import ru.planeta.model.common.CachedVideo

/**
 * Created with IntelliJ IDEA.
 * User: connanedoile
 * Date: 4/10/12
 * Time: 11:49 AM
 */
@Mapper
interface VideoCacheDAO {
    /**
     * @param id
     * @return CachedVideo
     */
    fun selectById(id: Long): CachedVideo

    /**
     * @param url
     * @return CachedVideo
     */
    fun selectByUrl(url: String): CachedVideo

    /**
     * @param cachedVideo
     */
    fun insert(cachedVideo: CachedVideo)

    fun update(video: CachedVideo)
}

package ru.planeta.dao.commondb

import org.apache.ibatis.annotations.Mapper
import org.apache.ibatis.annotations.Param
import ru.planeta.model.enums.OrderObjectType
import ru.planeta.model.stat.StatCampaignsForecast
import ru.planeta.model.stat.StatPurchaseComposite
import ru.planeta.model.stat.StatPurchaseProduct
import ru.planeta.model.stat.StatPurchaseShare
import java.util.*

/**
 * @author is.kuzminov
 * Date: 17.08.12
 * Time: 10:05
 */
@Mapper
interface AdminStatPurchaseDAO {
    fun selectStatComposite(@Param("dateFrom") dateFrom: Date, @Param("dateTo") dateTo: Date,
                            @Param("step") step: String): List<StatPurchaseComposite>

    fun selectForecast(): List<StatCampaignsForecast>

    fun selectStatPurchaseShares(@Param("dateFrom") dateFrom: Date, @Param("dateTo") dateTo: Date): List<StatPurchaseShare>

    fun selectStatPurchaseProducts(@Param("dateFrom") dateFrom: Date, @Param("dateTo") dateTo: Date): List<StatPurchaseProduct>

    fun selectStatPurchases(@Param("dateFrom") dateFrom: Date, @Param("dateTo") dateTo: Date,
                            @Param("orderObjectType") orderObjectType: OrderObjectType): Long

    fun selectTotalOrdersCount(@Param("dateFrom") dateFrom: Date, @Param("dateTo") dateTo: Date,
                               @Param("orderObjectType") orderObjectType: OrderObjectType): Long
}

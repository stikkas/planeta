package ru.planeta.dto

class SearchResultsDTO(var results: List<SearchResultItemDTO>?, var campaignsEstimatedCount: Long?, var productsEstimatedCount: Long?, var sharesEstimatedCount: Long?)

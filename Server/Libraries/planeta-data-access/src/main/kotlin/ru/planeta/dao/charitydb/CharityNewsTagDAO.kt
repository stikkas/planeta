package ru.planeta.dao.charitydb

/*
 * Created by Alexey on 06.07.2016.
 */

import org.apache.ibatis.annotations.Mapper
import org.apache.ibatis.annotations.Param
import ru.planeta.model.charity.CharityNewsTag

@Mapper
interface CharityNewsTagDAO {
    fun selectAllCharityNewsTags(@Param("offset") offset: Int, @Param("limit") limit: Int,
                                 @Param("offset") forNews: Boolean, @Param("forPartner") forPartner: Boolean): List<CharityNewsTag>
}

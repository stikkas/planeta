package ru.planeta.model.filters

import org.apache.commons.lang3.StringUtils
import org.apache.commons.lang3.math.NumberUtils
import org.springframework.format.annotation.DateTimeFormat
import ru.planeta.model.enums.PaymentErrorStatus
import ru.planeta.model.enums.PaymentErrorType
import java.util.*

/**
 * Created by kostiagn on 16.07.2015.
 */
class PaymentErrorsFilter {

    var paymentErrorType: PaymentErrorType? = null

    var paymentErrorStatus: PaymentErrorStatus? = null

    @set:DateTimeFormat(pattern = "null")
    var dateFrom: Date? = null

    @set:DateTimeFormat(pattern = "null")
    var dateTo: Date? = null

    var managerId: Long? = null

    var orderId: Long? = null

    var transactionId: Long? = null

    var campaignId: Long? = null

    var offset: Int = 0

    var limit = 20

    var email: String? = null

    var profileId: Long? = null

    var orderBy = "payment_errors.time_added desc"

    var query: String? = ""
        set(query) {
            field = query
            if (StringUtils.isNotBlank(query)) {
                val id = NumberUtils.toLong(query, -1)
                if (id != -1L) {
                    profileId = id
                } else {
                    email = query
                }
            }
        }
}

package ru.planeta.model.concert

import java.util.Date

/**
 * Created with IntelliJ IDEA.
 * User: michail
 * Date: 14.10.16
 * Time: 11:57
 */
class Concert {
    var concertId: Long = 0
    var externalConcertId: Long = 0
    var title: String? = null
    var description: String? = null
    var imageUrl: String? = null        // picture of the concert
    var schemeUrl: String? = null       // hall scheme is attribure of the concert (one hall may have different schemes)
    var hallId: Long = 0
    var concertDate: Date? = null
    var ageRestriction: Int = 0
    var status: ConcertStatus? = null
    var timeAdded: Date? = null
    var timeUpdated: Date? = null

    var statusCode: Int
        get() = status!!.code
        set(statusCode) {
            this.status = ConcertStatus.getByValue(statusCode)
        }
}

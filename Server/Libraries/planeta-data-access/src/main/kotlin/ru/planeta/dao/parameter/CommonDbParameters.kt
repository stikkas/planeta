package ru.planeta.dao.parameter

/**
 * parameters for common db implementations
 * User: Andrew Arefyev
 * Date: 22.11.12
 * Time: 19:18
 */
open class CommonDbParameters {
    companion object {

        val DELIVERY_STATUS = "deliveryStatus"
        val OBJECT_ID = "objectId"
        val PAYMENT_STATUS = "paymentStatus"
        val ORDER_OBJECT_TYPE = "orderObjectType"

        val OFFSET = "offset"
        val LIMIT = "limit"
    }

}

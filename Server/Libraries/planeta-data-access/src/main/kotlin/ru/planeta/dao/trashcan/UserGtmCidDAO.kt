package ru.planeta.dao.trashcan

import org.apache.ibatis.annotations.Param

/**
 *
 * User: Serge Blagodatskih<stikkas17></stikkas17>@gmail.com><br></br>
 * Date: 27.04.16<br></br>
 * Time: 14:48
 */
interface UserGtmCidDAO {
    fun insert(@Param("userId") userId: Long, @Param("cid") cid: String)
}

package ru.planeta.model.enums

/**
 * Created by kostiagn on 16.07.2015.
 */
interface ReadableEnum {
    val code: Int

    val descriptionMessageCode: String

    val fullDescriptionMessageCode: String

    val name: String
}

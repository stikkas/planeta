package ru.planeta.model.enums

/**
 * Created by eshevchenko.
 */
interface Codable {
    val code: Int
}

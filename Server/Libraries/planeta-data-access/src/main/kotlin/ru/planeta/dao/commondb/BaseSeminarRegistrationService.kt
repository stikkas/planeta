package ru.planeta.dao.commondb

import org.apache.ibatis.annotations.Param
import java.util.ArrayList
import java.util.Date
import org.springframework.beans.factory.annotation.Autowired
import ru.planeta.model.commondb.SeminarRegistration
import ru.planeta.model.commondb.SeminarRegistrationExample

abstract class BaseSeminarRegistrationService {
    @Autowired
    protected var seminarRegistrationMapper: SeminarRegistrationMapper? = null

    val orderByClause: String
        get() = "seminar_registration_id"

    //**************************************************************************************************
    //*********************************  Create ** Update ** Delete  *************************************
    //**************************************************************************************************
    fun insertSeminarRegistration(seminarRegistration: SeminarRegistration) {
        seminarRegistration.timeAdded = Date()

        seminarRegistrationMapper!!.insertSelective(seminarRegistration)
    }

    fun insertSeminarRegistrationAllFields(seminarRegistration: SeminarRegistration) {
        seminarRegistration.timeAdded = Date()

        seminarRegistrationMapper!!.insert(seminarRegistration)
    }

    fun updateSeminarRegistration(seminarRegistration: SeminarRegistration) {

        seminarRegistrationMapper!!.updateByPrimaryKeySelective(seminarRegistration)
    }

    fun updateSeminarRegistrationAllFields(seminarRegistration: SeminarRegistration) {

        seminarRegistrationMapper!!.updateByPrimaryKey(seminarRegistration)
    }

    fun insertOrUpdateSeminarRegistration(seminarRegistration: SeminarRegistration) {

        val selectedSeminarRegistration = seminarRegistrationMapper!!.selectByPrimaryKey(seminarRegistration.seminarRegistrationId)
        if (selectedSeminarRegistration == null) {
            seminarRegistration.timeAdded = Date()
            seminarRegistrationMapper!!.insertSelective(seminarRegistration)
        } else {
            seminarRegistrationMapper!!.updateByPrimaryKeySelective(seminarRegistration)
        }
    }

    fun insertOrUpdateSeminarRegistrationAllFields(seminarRegistration: SeminarRegistration) {

        val selectedSeminarRegistration = seminarRegistrationMapper!!.selectByPrimaryKey(seminarRegistration.seminarRegistrationId)
        if (selectedSeminarRegistration == null) {
            seminarRegistration.timeAdded = Date()
            seminarRegistrationMapper!!.insert(seminarRegistration)
        } else {
            seminarRegistrationMapper!!.updateByPrimaryKey(seminarRegistration)
        }
    }

    fun deleteSeminarRegistration(id: Long?) {
        seminarRegistrationMapper!!.deleteByPrimaryKey(id)
    }

    //**************************************************************************************************
    //************************************* seminar_registration *****************************************
    //************************************** SelectByPrimaryKey ******************************************
    //**************************************************************************************************
    fun selectSeminarRegistration(seminarRegistrationId: Long?): SeminarRegistration {
        return seminarRegistrationMapper!!.selectByPrimaryKey(seminarRegistrationId)
    }

    //**************************************************************************************************
    //************************************* seminar_registration *****************************************
    //**************************************************************************************************
    private fun COMMENT_SECTION_SeminarRegistration() {

    }

    fun getSeminarRegistrationExample(@Param("offset") offset: Int, @Param("limit") limit: Int): SeminarRegistrationExample {
        val example = SeminarRegistrationExample()
        example.offset = offset
        example.limit = limit
        example.or()
        example.orderByClause = orderByClause
        return example
    }

    fun selectSeminarRegistrationCount(): Int {
        return seminarRegistrationMapper!!.countByExample(getSeminarRegistrationExample(0, 0))
    }

    fun selectSeminarRegistrationList(@Param("offset") offset: Int, @Param("limit") limit: Int): List<SeminarRegistration> {
        return seminarRegistrationMapper!!.selectByExample(getSeminarRegistrationExample(offset, limit))
    }

    fun selectSeminarRegistrationList(seminarRegistrationIdList: List<Long>?): List<SeminarRegistration> {
        return if (seminarRegistrationIdList == null || seminarRegistrationIdList.isEmpty()) ArrayList() else seminarRegistrationMapper!!.selectByIdList(seminarRegistrationIdList)
    }

    //**************************************************************************************************
    //************************************* seminar_registration *****************************************
    //****************************************** byProfileId *********************************************
    //**************************************** Long profileId ********************************************
    //**************************************************************************************************
    private fun COMMENT_SECTION_SeminarRegistrationByProfileId(profileId: Long?) {

    }

    fun getSeminarRegistrationByProfileIdExample(@Param("profileId") profileId: Long?, @Param("offset") offset: Int, @Param("limit") limit: Int): SeminarRegistrationExample {
        val example = SeminarRegistrationExample()
        example.offset = offset
        example.limit = limit
        example.or()
                .andProfileIdEqualTo(profileId)
        example.orderByClause = orderByClause
        return example
    }

    fun selectSeminarRegistrationCountByProfileId(profileId: Long?): Int {
        return seminarRegistrationMapper!!.countByExample(getSeminarRegistrationByProfileIdExample(profileId, 0, 0))
    }

    fun selectSeminarRegistrationListByProfileId(@Param("profileId") profileId: Long?, @Param("offset") offset: Int, @Param("limit") limit: Int): List<SeminarRegistration> {
        return seminarRegistrationMapper!!.selectByExample(getSeminarRegistrationByProfileIdExample(profileId, offset, limit))
    }

    fun selectSeminarRegistrationListByProfileId(@Param("profileId") profileId: Long?, @Param("seminarRegistrationIdList") seminarRegistrationIdList: List<Long>?): List<SeminarRegistration> {
        return if (seminarRegistrationIdList == null || seminarRegistrationIdList.isEmpty()) ArrayList() else seminarRegistrationMapper!!.selectByIdList(seminarRegistrationIdList)
    }

    //**************************************************************************************************
    //************************************* seminar_registration *****************************************
    //****************************************** bySeminarId *********************************************
    //**************************************** Long seminarId ********************************************
    //**************************************************************************************************
    private fun COMMENT_SECTION_SeminarRegistrationBySeminarId(seminarId: Long?) {

    }

    fun getSeminarRegistrationBySeminarIdExample(@Param("seminarId") seminarId: Long?, @Param("offset") offset: Int, @Param("limit") limit: Int): SeminarRegistrationExample {
        val example = SeminarRegistrationExample()
        example.offset = offset
        example.limit = limit
        example.or()
                .andSeminarIdEqualTo(seminarId)
        example.orderByClause = orderByClause
        return example
    }

    fun selectSeminarRegistrationCountBySeminarId(seminarId: Long?): Int {
        return seminarRegistrationMapper!!.countByExample(getSeminarRegistrationBySeminarIdExample(seminarId, 0, 0))
    }

    fun selectSeminarRegistrationListBySeminarId(seminarId: Long?, @Param("offset") offset: Int, @Param("limit") limit: Int): List<SeminarRegistration> {
        return seminarRegistrationMapper!!.selectByExample(getSeminarRegistrationBySeminarIdExample(seminarId, offset, limit))
    }

    fun selectSeminarRegistrationListBySeminarId(seminarId: Long?, @Param("seminarRegistrationIdList") seminarRegistrationIdList: List<Long>?): List<SeminarRegistration> {
        return if (seminarRegistrationIdList == null || seminarRegistrationIdList.isEmpty()) ArrayList() else seminarRegistrationMapper!!.selectByIdList(seminarRegistrationIdList)
    }
}

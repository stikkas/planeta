package ru.planeta.model.profile

import ru.planeta.commons.model.Gender

import java.util.Date

/**
 * Created with IntelliJ IDEA.
 * User: a.volkov
 * Date: 24.10.12
 * Time: 14:04
 */
class CampaignBacker {
    var profileId: Long = 0
    var displayName: String? = null
    var imageUrl: String? = null
    var lastPurchaseTime: Date? = null
    var alias: String? = null
    var supportedProjectsCount: Int = 0
    var userGender = Gender.NOT_SET
    var cityName: String? = null
    var cityNameEng: String? = null
    var countryName: String? = null
    var countryNameEng: String? = null
}

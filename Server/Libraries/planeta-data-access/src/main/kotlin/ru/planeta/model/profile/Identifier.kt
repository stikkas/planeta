package ru.planeta.model.profile

interface Identifier {
    val id: Long
}

package ru.planeta.model.common.school

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import ru.planeta.model.commondb.SeminarRegistration

@JsonIgnoreProperties(ignoreUnknown = true)
class SeminarRegistrationSolo : SeminarRegistration() {
    var email: String? = null
}

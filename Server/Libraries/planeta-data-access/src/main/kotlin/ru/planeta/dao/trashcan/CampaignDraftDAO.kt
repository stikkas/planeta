package ru.planeta.dao.trashcan

import ru.planeta.model.trashcan.CampaignDraft

import java.util.Date

interface CampaignDraftDAO {
    fun insert(draft: CampaignDraft)

    fun cleanDrafts(deleteTo: Date)
}

package ru.planeta.dao.commondb

import org.apache.ibatis.annotations.Mapper
import org.apache.ibatis.annotations.Param
import ru.planeta.dao.ListWithCount
import ru.planeta.model.charity.CharityCampaignsStats
import ru.planeta.model.common.campaign.*
import ru.planeta.model.common.campaign.enums.CampaignStatus
import ru.planeta.model.common.campaign.param.CampaignEventsParam
import ru.planeta.model.enums.ObjectType
import ru.planeta.model.enums.OrderObjectType
import ru.planeta.model.profile.CampaignBacker
import ru.planeta.model.stat.CampaignStat
import java.math.BigDecimal
import java.util.*

/**
 * Campaign DAO
 *
 *
 * Created by IntelliJ IDEA. User: atropnikov Date: 21.03.12 Time: 12:56
 */
@Mapper
interface CampaignDAO {

    // methods for top page. calculate stats
    fun totalCollectedAmount(): BigDecimal

    fun campaignsCountPercentagesByCustomTags(): List<Map<String, Any>>

    fun selectCampaignsByStatus(@Param("profileId") profileId: Long, @Param("statuses") statuses: EnumSet<CampaignStatus>?,
                                @Param("offset") offset: Int, @Param("limit") limit: Int): List<Campaign>

    fun selectCampaignsByStatusAndManagerId(@Param("statuses") statuses: EnumSet<CampaignStatus>, @Param("managerId") managerId: Long,
                                            @Param("offset") offset: Int, @Param("limit") limit: Int): List<Campaign>

    /**
     * Select profile campaigns by status
     *
     * @param profileId profile id
     * @param status status
     * @param offset offset
     * @param limit limit
     * @return list of campaigns
     */
    @Deprecated("")
    fun selectCampaignsWithSharesByStatus(@Param("profileId") profileId: Long, @Param("status") status: CampaignStatus,
                                          @Param("offset") offset: Int, @Param("limit") limit: Int): List<Campaign>

    /**
     * Select campaign by id
     *
     * @param campaignId campaign id
     * @return campaign or null if campaign not found
     */
    fun selectCampaignById(@Param("campaignId") campaignId: Long): Campaign?

    fun selectForUpdate(campaignId: Long): Campaign?

    fun updateDraftVisible(@Param("campaignId") campaignId: Long, @Param("isVisible") isVisible: Boolean): Int

    /**
     * Select campaign by alias if campaignAlias can convert to long selectCampaignById
     * campaign by id
     *
     * @param campaignAlias campaign alias or id
     * @return campaign or null if campaign not found
     */
    fun selectCampaign(@Param("campaignAlias") campaignAlias: String): Campaign?

    fun selectByAlias(campaignAlias: String): Campaign?

    fun selectCampaignsForAdminSearch(@Param("query") query: String, @Param("offset") offset: Int, @Param("limit") limit: Int,
                                      @Param("managerId") managerId: Long?,
                                      @Param("dateFrom") dateFrom: Date?, @Param("dateTo") dateTo: Date?,
                                      @Param("campaignStatuses") campaignStatuses: EnumSet<CampaignStatus>, @Param("campaignTagId") campaignTagId: Int?,
                                      @Param("stringOrderBy") stringOrderBy: String): List<Campaign>

    fun selectCampaignsForAdminSearchCount(@Param("query") query: String,
                                           @Param("managerId") managerId: Long?,
                                           @Param("dateFrom") dateFrom: Date?, @Param("dateTo") dateTo: Date?,
                                           @Param("campaignStatuses") campaignStatuses: EnumSet<CampaignStatus>, @Param("campaignTagId") campaignTagId: Int?): Int

    /**
     * Select campaign by id with shares collection
     *
     * @param campaignId campaign
     * @return campaign or null if campaign not found
     */
    @Deprecated("")
    fun selectWithShares(@Param("campaignId") campaignId: Long, @Param("isSharePriceOrderByDesc") isSharePriceOrderByDesc: Boolean): Campaign?

    /**
     * Get collected amount money by campaign id
     *
     * @param campaignId campaign
     * @return amount money
     */
    fun getCollectedAmount(campaignId: Long): BigDecimal

    /**
     * Insert new campaign
     *
     * @param campaign campaign to be inserted
     */
    fun insert(campaign: Campaign)

    /**
     * Update specified campaign
     *
     * @param campaign campaign to be updated
     */
    fun update(campaign: Campaign)

    /**
     * Marks campaign as deleted; Doesn't not remove campaign from db
     *
     * @param campaignId campaign id
     */
    fun delete(campaignId: Long)

    /**
     * Select list of campaign backers
     *
     * @param campaignId campaign id
     * @param offset offset
     * @param limit limit
     * @return list of campaign backers
     */
    fun selectCampaignBackers(@Param("campaignId") campaignId: Long, @Param("shareTypes") shareTypes: EnumSet<OrderObjectType>,
                              @Param("offset") offset: Int, @Param("limit") limit: Int): List<CampaignBacker>

    /**
     * Select campaigns which finished by time and time <= date
     *
     * @param date date
     * @return list of campaigns
     */
    fun selectFinishedCampaignsByTime(@Param("date") date: Date, @Param("statuses") statuses: EnumSet<CampaignStatus>,
                                      @Param("offset") offset: Int, @Param("limit") limit: Int): List<Campaign>

    fun selectStartedCampaignsByTime(@Param("date") date: Date, @Param("statuses") statuses: EnumSet<CampaignStatus>,
                                     @Param("offset") offset: Int, @Param("limit") limit: Int): List<Campaign>

    fun selectNotRenewedBackground(@Param("offset") offset: Int, @Param("limit") limit: Int): List<Campaign>

    /**
     * get count of current baсkers
     */
    fun getCampaignBackerCount(campaignId: Long): Int

    /**
     * selects ids of campaigns which has been ordered by user
     *
     * @param profileId profile id
     * @return list of campaign ids
     */
    fun selectPurchasedCampaigns(@Param("profileId") profileId: Long): List<Long>

    /**
     * Selects list of campaigns by ids
     * @param campaignIds campaign ids
     */
    fun getCampaignsByIds(@Param("campaignIds") campaignIds: List<Long>): List<Campaign>

    fun getCampaignsCount(@Param("profileId") profileId: Long, @Param("status") status: CampaignStatus): Int

    fun getCampaignsCountByStatuses(@Param("profileId") profileId: Long, @Param("statuses") statuses: EnumSet<CampaignStatus>): Int

    fun selectCampaignsByDateRange(@Param("timeStartFrom") timeStartFrom: Date?, @Param("timeStartTo") timeStartTo: Date?,
                                   @Param("timeFinishFrom") timeFinishFrom: Date?, @Param("timeFinishTo") timeFinishTo: Date?): List<Campaign>

    fun selectLiteDraftCampaignsByOwnerId(groupsIds: List<Long>): List<Campaign>

    fun selectCampaignsByContractorId(@Param("contractorId") contractorId: Long, @Param("offset") offset: Int, @Param("limit") limit: Int): List<Campaign>

    fun selectCurators(campaignId: Long): MutableList<CampaignContact>

    fun deleteCampaignCurator(@Param("campaignId") campaignId: Long, @Param("email") email: String)

    fun insertCampaignCurator(@Param("campaignId") campaignId: Long, @Param("email") email: String)

    fun selectCampaignsCountWithPurchasedOrReservedShares(campaignId: Long): Int

    fun getBackedCampaignList(@Param("profileId") profileId: Long, @Param("shareTypes") shareTypes: EnumSet<OrderObjectType>, @Param("offset") offset: Int, @Param("limit") limit: Int): List<Campaign>

    fun getUserBackedCampaignTagCountList(@Param("profileId") profileId: Long): List<Map<*, *>>

    fun selectCampaignsTargetAmount(status: CampaignStatus): BigDecimal

    fun selectCampaignsPurchasedAmount(status: CampaignStatus): BigDecimal

    fun selectPurchasedAmountForAllTime(): BigDecimal

    fun selectCampaignsCountBetween(@Param("dateBegin") dateBegin: Date, @Param("dateEnd") dateEnd: Date,
                                    @Param("status") status: CampaignStatus): Int?

    fun selectCampaignNewEventsCountList(@Param("campaignIds") campaignIds: List<Long>, @Param("profileId") profileId: Long,
                                         @Param("dateFrom") dateFrom: Date?): List<CampaignWithNewEventsCount>

    fun selectCampaignNews(campaignEventsParam: CampaignEventsParam): ListWithCount<CampaignNews>

    fun selectCampaignPosts(campaignEventsParam: CampaignEventsParam): ListWithCount<CampaignNews>

    fun selectCampaignComments(campaignEventsParam: CampaignEventsParam): ListWithCount<CampaignNews>

    fun selectCampaignCommentsCountByDate(@Param("date") date: Date, @Param("commentsType") commentsType: ObjectType,
                                          @Param("campaignId") campaignId: Long): Int

    fun getCampaignId(@Param("campaignAlias") campaignAlias: String): Long

    fun selectCampaignByOrder(@Param("orderId") orderId: Long): Campaign

    fun getAuthorActiveCampaign(@Param("creatorProfileId") creatorProfileId: Long): Campaign

    fun selectPurchasedShareIds(@Param("buyerId") buyerId: Long, @Param("campaignId") campaignId: Long): List<Long>

    fun selectPurchasedCampaignIds(@Param("buyerId") buyerId: Long): List<Long>

    fun selectCampaignBySponsorAlias(@Param("sponsorAlias") sponsorAlias: String, @Param("offset") offset: Int,
                                     @Param("limit") limit: Int): List<Campaign>

    fun getGtmCampaignInfoList(@Param("campaignIds") campaignIds: List<Long>): List<GtmCampaignInfo>

    fun getSuccessfulCampaignsCountByTagId(@Param("campaignTagId") campaignTagId: Int?): Int

    fun countOrganizationCharityCampaigns(): Int

    fun charityCampaignsStats(@Param("dateStart") dateStart: Date): List<CharityCampaignsStats>

    fun selectProfileIdsOfCampaignsWithEventsForDay(@Param("offset") offset: Int, @Param("limit") limit: Int,
                                                    @Param("date") date: Date): List<Long>

    fun selectCampaignsWithEventsForDayByProfileId(@Param("profileId") profileId: Long, @Param("offset") offset: Int,
                                                   @Param("limit") limit: Int, @Param("date") date: Date): List<Campaign>

    fun selectAggregationStatsForDayByCampaignId(@Param("campaignId") campaignId: Long, @Param("date") date: Date): CampaignStat

    fun getUserCampaignsStatuses(@Param("profileId") profileId: Long): List<MyCampaignsStatusesCount>

    fun getUserPurchasedCampaignsStatuses(@Param("profileId") profileId: Long): List<MyCampaignsStatusesCount>

    fun getUserPurchasedAndRewardsCount(@Param("profileId") profileId: Long): List<MyPurchasesAndRewardsCount>
}

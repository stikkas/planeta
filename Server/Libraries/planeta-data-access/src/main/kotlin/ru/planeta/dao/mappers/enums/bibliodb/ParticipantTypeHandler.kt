package ru.planeta.dao.mappers.enums.bibliodb

import java.sql.CallableStatement
import java.sql.PreparedStatement
import java.sql.ResultSet
import java.sql.SQLException
import org.apache.ibatis.type.JdbcType
import org.apache.ibatis.type.TypeHandler
import ru.planeta.model.bibliodb.enums.ParticipantType

/**
 *
 * User: Serge Blagodatskih<stikkas17></stikkas17>@gmail.com><br></br>
 * Date: 27.04.16<br></br>
 * Time: 12:42
 */
class ParticipantTypeHandler : TypeHandler<ParticipantType> {

    @Throws(SQLException::class)
    override fun getResult(rs: ResultSet, columnIndex: Int): ParticipantType? {
        return ParticipantType.getByValue(rs.getInt(columnIndex))
    }

    @Throws(SQLException::class)
    override fun getResult(cs: CallableStatement, columnIndex: Int): ParticipantType? {
        return ParticipantType.getByValue(cs.getInt(columnIndex))
    }

    @Throws(SQLException::class)
    override fun getResult(rs: ResultSet, columnName: String): ParticipantType? {
        return ParticipantType.getByValue(rs.getInt(columnName))
    }

    @Throws(SQLException::class)
    override fun setParameter(ps: PreparedStatement, i: Int, parameter: ParticipantType, jdbcType: JdbcType) {
        ps.setInt(i, parameter.code)
    }

}

package ru.planeta.dao.mappers.enums

import org.apache.ibatis.type.JdbcType
import org.apache.ibatis.type.TypeHandler
import ru.planeta.model.enums.ProjectType

import java.sql.CallableStatement
import java.sql.PreparedStatement
import java.sql.ResultSet
import java.sql.SQLException

/**
 * Project type handler
 * User: eshevchenko
 */
class ProjectTypeHandler : TypeHandler<ProjectType> {

    @Throws(SQLException::class)
    override fun setParameter(preparedStatement: PreparedStatement, i: Int, projectType: ProjectType, jdbcType: JdbcType) {
        preparedStatement.setInt(i, projectType.code)
    }

    @Throws(SQLException::class)
    override fun getResult(resultSet: ResultSet, s: String): ProjectType {
        return ProjectType.getByCode(resultSet.getInt(s))
    }

    @Throws(SQLException::class)
    override fun getResult(resultSet: ResultSet, i: Int): ProjectType {
        return ProjectType.getByCode(resultSet.getInt(i))
    }

    @Throws(SQLException::class)
    override fun getResult(callableStatement: CallableStatement, i: Int): ProjectType {
        return ProjectType.getByCode(callableStatement.getInt(i))
    }
}

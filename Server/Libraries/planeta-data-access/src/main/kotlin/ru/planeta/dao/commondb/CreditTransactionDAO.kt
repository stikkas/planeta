package ru.planeta.dao.commondb

import org.apache.ibatis.annotations.Mapper
import org.apache.ibatis.annotations.Param
import ru.planeta.model.common.CreditTransaction

/**
 * Interface CreditTransactionDAO
 *
 * @author a.tropnikov
 */
@Mapper
interface CreditTransactionDAO {

    fun selectList(profileId: Long, @Param("offset") offset: Int, @Param("limit") limit: Int): List<CreditTransaction>

    fun select(transactionId: Long): CreditTransaction

    fun insert(creditTransaction: CreditTransaction): CreditTransaction

    fun update(creditTransaction: CreditTransaction): CreditTransaction

    fun delete(profileId: Long, @Param("transactionId") transactionId: Long)
}

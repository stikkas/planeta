package ru.planeta.model.enums

import java.util.HashMap

enum class CustomMetaTagType (override val code: Int, val label: String) : Codable {
    MAIN(0, "main"),
    SHOP(1, "shop"),
    TV(2, "tv"),
    CONCERT(3, "concert"),
    SCHOOL(4, "school"),
    BIBLIO(5, "biblio"),
    CHARITY(6, "charity"),
    PROMO(7, "promo");


    companion object {

        private val lookup = HashMap<Int, CustomMetaTagType>()

        init {
            for (customMetaTagType in values()) {
                lookup[customMetaTagType.code] = customMetaTagType
            }
        }

        fun getByCode(code: Int): CustomMetaTagType? {
            return lookup[code]
        }
    }

}

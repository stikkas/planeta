package ru.planeta.model.enums

import java.util.*

/**
 * @author ds.kolyshev
 * Date: 27.10.11
 */
enum class PermissionLevel(val code: Int) {

    // FRIENDS and MEMBERS is same thing?
    NONE(0),
    EVERYBODY(1), FRIEND_OF_FRIEND(10), SUBSCRIBERS(20), MEMBERS(30), FRIENDS(32), ARTISTS(34), MODERATORS(36), CUSTOM(38), ADMINS(40),
    GLOBAL_MANAGERS(50), GLOBAL_ADMINS(60), NOBODY(1000);


    companion object {

        private val lookup = HashMap<Int, PermissionLevel>()

        init {
            for (s in EnumSet.allOf(PermissionLevel::class.java)) {
                lookup[s.code] = s
            }
        }

        fun getByValue(code: Int): PermissionLevel {
            return lookup[code] ?: NONE
        }
    }
}

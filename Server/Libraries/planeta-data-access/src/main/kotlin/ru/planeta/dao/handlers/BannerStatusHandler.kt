package ru.planeta.dao.handlers

import org.apache.ibatis.type.BaseTypeHandler
import org.apache.ibatis.type.JdbcType
import org.apache.ibatis.type.MappedTypes
import ru.planeta.model.enums.BannerStatus
import ru.planeta.model.enums.Codable
import java.sql.CallableStatement
import java.sql.PreparedStatement
import java.sql.ResultSet
import java.util.*

@MappedTypes(BannerStatus::class)
class BannerStatusHandler : BaseTypeHandler<BannerStatus>() {
    override fun getNullableResult(p0: ResultSet, p1: String?): BannerStatus? {
        return BannerStatus.getByValue(p0.getInt(p1))
    }

    override fun getNullableResult(p0: ResultSet, p1: Int): BannerStatus? {
        return BannerStatus.getByValue(p0.getInt(p1))
    }

    override fun getNullableResult(p0: CallableStatement, p1: Int): BannerStatus? {
        return BannerStatus.getByValue(p0.getInt(p1))
    }

    override fun setNonNullParameter(p0: PreparedStatement, p1: Int, p2: BannerStatus, p3: JdbcType?) {
        p0.setInt(p1, p2.code)
    }
}
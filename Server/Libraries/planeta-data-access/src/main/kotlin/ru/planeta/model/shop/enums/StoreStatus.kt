package ru.planeta.model.shop.enums

import ru.planeta.model.enums.Codable

import java.util.EnumSet
import java.util.TreeMap

/**
 * User: a.savanovich
 * Date: 20.06.12
 * Time: 18:14
 */
enum class StoreStatus private constructor(override val code: Int) : Codable {
    ACTIVE(1), PAUSED(2), REJECTED(3), MODERATION(4);


    companion object {

        private val lookup = TreeMap<Int, StoreStatus>()

        init {
            for (s in EnumSet.allOf(StoreStatus::class.java)) {
                lookup[s.code] = s
            }
        }

        fun getByValue(code: Int): StoreStatus? {
            return lookup[code]
        }
    }

}

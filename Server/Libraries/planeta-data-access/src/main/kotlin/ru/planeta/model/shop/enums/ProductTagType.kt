package ru.planeta.model.shop.enums

import ru.planeta.model.enums.Codable

import java.util.EnumSet
import java.util.HashMap

/**
 * Created with IntelliJ IDEA.
 * User: michail
 * Date: 19.08.16
 * Time: 12:07
 */
enum class ProductTagType private constructor(override val code: Int) : Codable {
    VISIBLE(1),
    PROMO(2),
    HIDDEN(3);


    companion object {

        private val lookup = HashMap<Int, ProductTagType>()

        init {
            for (s in EnumSet.allOf(ProductTagType::class.java)) {
                lookup[s.code] = s
            }
        }

        fun getByValue(code: Int): ProductTagType? {
            return lookup[code]
        }
    }

}

package ru.planeta.model.trashcan

import ru.planeta.model.promo.VoteType

import java.util.Date

/**
 *
 * Created by asavan on 25.01.2017.
 */
class LitresPromoCode {

    var promoId: Long = 0
    var email: String? = null
    var timeAdded: Date? = null
    var status: Int = 0
    var secretCode: String? = null
    var voteType: VoteType? = null

    val voteTypeCode: Int
        get() = voteType?.code ?: 0

    companion object {

        val USED = 1
        val NOT_USED = 0
    }
}

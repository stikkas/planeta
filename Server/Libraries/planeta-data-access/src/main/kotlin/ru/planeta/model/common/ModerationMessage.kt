package ru.planeta.model.common

import ru.planeta.model.common.campaign.enums.CampaignStatus
import ru.planeta.model.enums.ModerationMessageStatus

import java.util.Date

/**
 * Date: 22.10.13
 * Time: 18:41
 */
class ModerationMessage {
    var messageId: Long = 0
    var campaignId: Long = 0
    var senderId: Long = 0
    var message: String? = null
    var displayName: String? = null
    var timeAdded: Date? = null
    var status: ModerationMessageStatus? = null
    var campaignStatus = CampaignStatus.ALL

    internal var statusCode: Int
        get() = status!!.code
        set(status) {
            this.status = ModerationMessageStatus.getByValue(status)
        }

    internal var campaignStatusCode: Int
        get() = campaignStatus.code
        set(status) {
            this.campaignStatus = CampaignStatus.getByValue(status)
        }
}

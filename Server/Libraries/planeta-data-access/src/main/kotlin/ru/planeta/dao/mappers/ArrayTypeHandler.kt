package ru.planeta.dao.mappers

import org.apache.ibatis.type.JdbcType
import org.apache.ibatis.type.TypeHandler
import java.sql.*

/**
 * @author ds.kolyshev
 * Date: 12.09.11
 */
class ArrayTypeHandler : TypeHandler<Any> {
    private var clazz: Class<*>? = null

    constructor() : super()

    constructor(clazz: Class<*>) : this() {
        this.clazz = clazz
    }

    override fun getResult(rs: ResultSet, columnName: String): Any? {
        val array = rs.getArray(columnName)
        return getResultHelper(array)
    }

    private fun getResultHelper(array: java.sql.Array): Any? {
        if (array == null) {
            return null
        }

        val componentType = array.array.javaClass.componentType
        when (componentType) {
            Int::class.java -> {
                val keys = array.array as Array<Int>
                val result = arrayOfNulls<Int>(keys.size)
                keys.forEachIndexed { index, i ->
                    result[index] = i
                }
                return result
            }
            Long::class.java -> {
                val keys = array.array as Array<Long>
                val result = arrayOfNulls<Long>(keys.size)
                keys.forEachIndexed { index, i ->
                    result[index] = i
                }
                return result
            }
            String::class.java -> {
                val keys = array.array as Array<String>
                val result = mutableListOf<String>()
                keys.forEach {
                    result.add(it)
                }
                return result
            }
            else -> throw SQLException("Array type mapper not supported array of " + componentType.name)
        }
    }

    override fun setParameter(ps: PreparedStatement, i: Int, parameter: Any, jdbcType: JdbcType) {
        if (parameter == null) {
            ps.setNull(i, Types.ARRAY)
        } else {
            val type = parameter.javaClass.componentType
            if (!type.isArray) {
                if (parameter is List<*>) {
                    var array: SqlArrayAdapter
                    when (clazz) {
                        null -> {
                            array = StringArray(parameter as List<String>)
                            ps.setArray(i, array)
                            return
                        }
                        Long::class.java -> {
                            array = LongArray(parameter as List<Long>)
                            ps.setArray(i, array)
                            return
                        }
                        else -> throw SQLException("Parameter type $type of class $clazz is not supported")
                    }
                } else {
                    throw SQLException("Parameter type $type is not array")
                }
            }
            val componentType = type.componentType
            if ("long" == componentType.name) {
                val keys = parameter as Array<Long>
                val longArray = LongArray(keys)
                ps.setArray(i, longArray)
            } else if ("int" == componentType.name) {
                val keys = parameter as Array<Int>
                val intArray = IntArray(keys)
                ps.setArray(i, intArray)
            } else {
                throw SQLException("Sql Array adapter no supported array of " + componentType.name)
            }
        }
    }

    override fun getResult(rs: ResultSet, columnIndex: Int): Any? {
        val array = rs.getArray(columnIndex)
        return getResultHelper(array)
    }

    override fun getResult(cs: CallableStatement, columnIndex: Int): Any? {
        val array = cs.getArray(columnIndex) ?: return null

        val keys = array.array as Array<Int>
        val result = arrayOfNulls<Int>(keys.size)
        keys.forEachIndexed { index, i ->
            result[index] = i
        }

        return result
    }
}

package ru.planeta.model.common

import com.fasterxml.jackson.annotation.JsonIgnoreProperties

import java.math.BigDecimal

/**
 *
 * User: Serge Blagodatskih<stikkas17></stikkas17>@gmail.com><br></br>
 * Date: 25.02.16<br></br>
 * Time: 17:32
 */
@JsonIgnoreProperties(ignoreUnknown = true)
class ProfileBalance {
    var profileId: Long = 0
    var balance: BigDecimal? = null
    var frozenAmount: BigDecimal? = null

    /**
     * Count bills for customer
     */
    var billCount: Long = 0
}

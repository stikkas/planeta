package ru.planeta.dao.commondb

import org.apache.ibatis.annotations.Mapper
import org.apache.ibatis.annotations.Param
import ru.planeta.model.common.campaign.CampaignContact

/**
 * @author: ds.kolyshev
 * Date: 28.07.13
 */
@Mapper
interface CampaignContactDAO {

    fun selectList(@Param("campaignId") campaignId: Long, @Param("offset") offset: Int, @Param("limit") limit: Int): List<CampaignContact>

    fun insert(campaignContact: CampaignContact)

    fun delete(@Param("campaignId") campaignId: Long, @Param("email") email: String)

    fun getCount(campaignId: Long): Int
}

package ru.planeta.dao.trashcan

import ru.planeta.model.trashcan.InteractiveEditorData

interface InteractiveEditorDataDAO {
    fun insert(interactiveEditorData: InteractiveEditorData)

    fun selectByUserId(userId: Long): InteractiveEditorData

    fun selectByEmail(email: String): InteractiveEditorData

    fun update(interactiveEditorData: InteractiveEditorData)

    fun deleteByUserId(userId: Long): Int
}

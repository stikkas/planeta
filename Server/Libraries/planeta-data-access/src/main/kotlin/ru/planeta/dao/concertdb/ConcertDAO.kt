package ru.planeta.dao.concertdb

import org.apache.ibatis.annotations.Mapper
import org.apache.ibatis.annotations.Param
import ru.planeta.model.concert.Concert

/**
 * Created with IntelliJ IDEA.
 * User: michail
 * Date: 14.10.16
 * Time: 12:06
 */
@Mapper
interface ConcertDAO {
    val concertsCount: Long

    fun insert(concert: Concert): Int

    fun update(concert: Concert): Int

    fun select(concertId: Long): Concert

    fun selectList(@Param("offset") offset: Int, @Param("limit") limit: Int): List<Concert>

    fun selectActive(@Param("offset") offset: Int, @Param("limit") limit: Int): List<Concert>

    fun selectByExtId(concertId: Long): Concert
}

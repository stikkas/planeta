package ru.planeta.dao.commondb

import org.apache.ibatis.annotations.Mapper
import org.apache.ibatis.annotations.Param
import ru.planeta.model.common.OrderObject
import ru.planeta.model.enums.OrderObjectType
import ru.planeta.model.shop.enums.PaymentStatus

/**
 * User: atropnikov
 * Date: 06.04.12
 * Time: 16:53
 */
@Mapper
interface OrderObjectDAO {

    /**
     * Inserts order object.
     *
     * @param orderObject object to insert;
     */
    fun insert(orderObject: OrderObject)

    /**
     * Selects all objects for specified order.
     *
     * @param orderId order identifier;
     * @return
     */
    fun selectOrderObjects(orderId: Long): List<OrderObject>

    /**
     * Selects objects for all specified orders with merchant restriction, if `merchantId` is not zero.
     *
     * @param orderIds   order identifiers collection;
     * @param merchantId merchant profile identifier restriction;
     * @return
     */
    fun selectOrdersObjects(@Param("orderIds") orderIds: Collection<Long>, @Param("merchantId") merchantId: Long): List<OrderObject>

    /**
     * Selects object with specified order, object type and object identifier.
     *
     * @param orderId         order identifier;
     * @param objectId        object identifier;
     * @param orderObjectType object type;
     * @return order object if exist, `null` otherwise.
     */
    fun selectOrderObjects(@Param("orderId") orderId: Long, @Param("objectId") objectId: Long, @Param("orderObjectType") orderObjectType: OrderObjectType): List<OrderObject>

    /**
     * Updates order object's properties.
     *
     * @param orderObject object to update;
     */
    fun update(orderObject: OrderObject)

    fun select(orderObjectId: Long): OrderObject

    fun markOrderObjectsAsGift(orderId: Long)

    fun updateOrderObjectComment(@Param("orderId") orderId: Long, @Param("objectId") objectId: Long, @Param("comment") comment: String)

    fun selectMerchantOrderObjects(@Param("merchantId") merchantId: Long, @Param("paymentStatus") paymentStatus: PaymentStatus,
                                   @Param("orderObjectType") orderObjectType: OrderObjectType): List<OrderObject>

    fun selectOverdueOrdersObjects(): List<OrderObject>

    fun isProfileHasPurchasedObject(@Param("buyerId") buyerId: Long, @Param("objectId") objectId: Long,
                                    @Param("orderObjectType") orderObjectType: OrderObjectType): Boolean
}

package ru.planeta.model.enums

/**
 * User: a.savanovich
 * Date: 02.04.13
 * Time: 12:19
 */
enum class EmailStatus {
    ALREADY_EXIST, NOT_VALID, NEW_VALID_EMAIL, MAY_BE_NOT_VALID
}

package ru.planeta.model.profile

import ru.planeta.model.enums.CommunicationChannelType

/**
 * Class ProfileCommunicationChannel
 *
 * @author a.tropnikov
 */
class ProfileCommunicationChannel : IProfileObject {

    override var profileId: Long? = 0
    var channelId: Int = 0
    var channelValue: String? = null

    var communicationChannelType: CommunicationChannelType?
        get() = CommunicationChannelType.Companion.getByValue(channelId)
        set(communicationChannelType) {
            channelId = communicationChannelType?.code ?: 0
        }
}

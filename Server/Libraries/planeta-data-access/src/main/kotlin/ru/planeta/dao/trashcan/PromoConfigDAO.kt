package ru.planeta.dao.trashcan

import org.apache.ibatis.annotations.Param
import ru.planeta.model.trashcan.PromoConfig

import java.math.BigDecimal

interface PromoConfigDAO {
    fun selectAll(): List<PromoConfig>

    fun selectCompatible(@Param("campaignTags") campaignTags: Collection<Long>, @Param("totalPrice") totalPrice: BigDecimal): List<PromoConfig>

    fun insert(config: PromoConfig)

    fun update(config: PromoConfig)

    fun selectById(configId: Long): PromoConfig

    fun finishConfigs()
}

package ru.planeta.model.profile

import ru.planeta.model.enums.ProfileType

/**
 * Class Profile
 *
 * @author a.tropnikov
 */
class ProfileForAdminsWithEmail : Identifier {


    var profileId: Long = 0
    var imageUrl: String? = null
    var alias: String? = null
    var displayName: String? = null
    var phoneNumber: String? = null
    var isVip = false
    var email: String? = null
    var profileType = ProfileType.USER


    override val id: Long
        get() = 0

    var profileTypeCode: Int
        get() = profileType.code
        set(code) {
            this.profileType = ProfileType.Companion.getByValue(code)
        }
}

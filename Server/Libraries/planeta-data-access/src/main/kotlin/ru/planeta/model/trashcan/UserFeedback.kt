package ru.planeta.model.trashcan

import java.util.Date

class UserFeedback {
    var userFeedbackId: Long = 0
    var orderId: Long = 0
    var userId: Long = 0
    var campaignId: Long = 0
    var score: Int = 0
    var isExtraTesting: Boolean = false
    var pageType: FeedbackPageType? = null
    var timeAdded: Date? = null

    var pageTypeCode: Int
        get() = pageType?.code ?: 0
        set(pageTypeCode) {
            this.pageType = FeedbackPageType.getByValue(pageTypeCode)
        }
}

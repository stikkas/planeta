package ru.planeta.model.shop.enums

import java.util.EnumSet
import java.util.HashMap

/**
 * User: a.savanovich
 * Date: 20.06.12
 * Time: 18:10
 */
enum class ProductStatus private constructor(val code: Int) {
    ACTIVE(1),
    PAUSE(2),
    OLD(3);


    companion object {

        private val lookup = HashMap<Int, ProductStatus>()

        init {
            for (s in EnumSet.allOf(ProductStatus::class.java)) {
                lookup[s.code] = s
            }
        }

        fun getByValue(code: Int): ProductStatus? {
            return lookup[code]
        }
    }

}

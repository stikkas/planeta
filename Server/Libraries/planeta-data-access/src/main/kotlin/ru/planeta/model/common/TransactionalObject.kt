package ru.planeta.model.common

/**
 * Base class for transactional objects.<br></br>
 * Contains two pairs of transaction identifiers<br></br>
 * Each of pair contains credit and debit transaction identifier.
 */
open class TransactionalObject {

    var creditTransactionId: Long = 0
    var debitTransactionId: Long = 0
    var cancelCreditTransactionId: Long = 0
    var cancelDebitTransactionId: Long = 0
}

package ru.planeta.model.commondb

import ru.planeta.model.commondb.PaymentErrorCommentWithProfile
import ru.planeta.model.profile.Profile

class PaymentErrorsWithComments : PaymentErrors() {
    var profile: Profile? = null

    var manager: Profile? = null

    var commentList: List<PaymentErrorCommentWithProfile>? = null

    var email: String? = null
}

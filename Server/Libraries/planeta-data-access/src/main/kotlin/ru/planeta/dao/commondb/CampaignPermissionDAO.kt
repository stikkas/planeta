package ru.planeta.dao.commondb

import org.apache.ibatis.annotations.Mapper
import org.apache.ibatis.annotations.Param
import ru.planeta.model.common.campaign.CampaignPermission

/**
 * User: a.savanovich
 * Date: 13.12.13
 * Time: 16:44
 */
@Mapper
interface CampaignPermissionDAO {
    fun insert(campaignPermission: CampaignPermission)

    fun update(campaignPermission: CampaignPermission)

    fun delete(campaignId: Long)

    fun select(campaignId: Long): CampaignPermission?

    fun selectList(offset: Int, @Param("limit") limit: Int): List<CampaignPermission>
}

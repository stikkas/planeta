package ru.planeta.model.common.campaign

import ru.planeta.model.common.campaign.enums.CampaignStatus
import ru.planeta.model.common.campaign.enums.CampaignTargetStatus

class MyCampaignsStatusesCount {

    var count: Int = 0
    var status: CampaignStatus? = null
        private set
    var targetStatus: CampaignTargetStatus? = null
        private set

    fun setStatus(code: Int) {
        this.status = CampaignStatus.getByValue(code)
    }

    fun setTargetStatus(code: Int) {
        this.targetStatus = CampaignTargetStatus.getByValue(code)
    }
}

package ru.planeta.dao.handlers

import org.apache.ibatis.type.BaseTypeHandler
import org.apache.ibatis.type.JdbcType
import ru.planeta.model.enums.OrderObjectType
import java.sql.CallableStatement
import java.sql.PreparedStatement
import java.sql.ResultSet

/**
 * Created by IntelliJ IDEA.
 * User: Andrew
 * Date: 31.10.12
 * Time: 11:02
 */
class OrderObjectTypeHandler : BaseTypeHandler<OrderObjectType>() {
    override fun getNullableResult(p0: ResultSet, p1: String): OrderObjectType? {
        return OrderObjectType.getByValue(p0.getInt(p1))
    }

    override fun getNullableResult(p0: ResultSet, p1: Int): OrderObjectType? {
        return OrderObjectType.getByValue(p0.getInt(p1))
    }

    override fun getNullableResult(p0: CallableStatement, p1: Int): OrderObjectType? {
        return OrderObjectType.getByValue(p0.getInt(p1))
    }

    override fun setNonNullParameter(p0: PreparedStatement, p1: Int, p2: OrderObjectType, p3: JdbcType?) {
        p0.setInt(p1, p2.code)
    }
}

package ru.planeta.model.commondb

import java.util.Date

class QuickstartGetBook {
    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column commondb.quickstart_get_book.email
     *
     * @mbggenerated Tue May 29 12:53:11 MSK 2018
     */
    var email: String? = null
        set(email) {
            field = email?.trim { it <= ' ' }
        }
    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column commondb.quickstart_get_book.book_type
     *
     * @mbggenerated Tue May 29 12:53:11 MSK 2018
     */
    var bookType: String? = null
        set(bookType) {
            field = bookType?.trim { it <= ' ' }
        }
    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column commondb.quickstart_get_book.time_added
     *
     * @mbggenerated Tue May 29 12:53:11 MSK 2018
     */
    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column commondb.quickstart_get_book.time_added
     *
     * @return the value of commondb.quickstart_get_book.time_added
     *
     * @mbggenerated Tue May 29 12:53:11 MSK 2018
     */
    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column commondb.quickstart_get_book.time_added
     *
     * @param timeAdded the value for commondb.quickstart_get_book.time_added
     *
     * @mbggenerated Tue May 29 12:53:11 MSK 2018
     */
    var timeAdded: Date? = null

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column commondb.quickstart_get_book.profile_id
     *
     * @mbggenerated Tue May 29 12:53:11 MSK 2018
     */
    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column commondb.quickstart_get_book.profile_id
     *
     * @return the value of commondb.quickstart_get_book.profile_id
     *
     * @mbggenerated Tue May 29 12:53:11 MSK 2018
     */
    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column commondb.quickstart_get_book.profile_id
     *
     * @param profileId the value for commondb.quickstart_get_book.profile_id
     *
     * @mbggenerated Tue May 29 12:53:11 MSK 2018
     */
    var profileId: Long? = null


    /**
     * This class was generated by MyBatis Generator.
     * This class corresponds to the database table commondb.quickstart_get_book
     *
     * @mbggenerated Tue May 29 12:53:11 MSK 2018
     */
    class Builder {
        private val obj: QuickstartGetBook

        init {
            this.obj = QuickstartGetBook()
        }

        /**
         * This method was generated by MyBatis Generator.
         * This method corresponds to the database table commondb.quickstart_get_book
         *
         * @mbggenerated Tue May 29 12:53:11 MSK 2018
         */
        fun email(email: String): Builder {
            obj.email = email
            return this
        }

        /**
         * This method was generated by MyBatis Generator.
         * This method corresponds to the database table commondb.quickstart_get_book
         *
         * @mbggenerated Tue May 29 12:53:11 MSK 2018
         */
        fun bookType(bookType: String): Builder {
            obj.bookType = bookType
            return this
        }

        /**
         * This method was generated by MyBatis Generator.
         * This method corresponds to the database table commondb.quickstart_get_book
         *
         * @mbggenerated Tue May 29 12:53:11 MSK 2018
         */
        fun timeAdded(timeAdded: Date): Builder {
            obj.timeAdded = timeAdded
            return this
        }

        /**
         * This method was generated by MyBatis Generator.
         * This method corresponds to the database table commondb.quickstart_get_book
         *
         * @mbggenerated Tue May 29 12:53:11 MSK 2018
         */
        fun profileId(profileId: Long?): Builder {
            obj.profileId = profileId
            return this
        }

        /**
         * This method was generated by MyBatis Generator.
         * This method corresponds to the database table commondb.quickstart_get_book
         *
         * @mbggenerated Tue May 29 12:53:11 MSK 2018
         */
        fun build(): QuickstartGetBook {
            return this.obj
        }
    }

    companion object {

        /**
         * This method was generated by MyBatis Generator.
         * This method corresponds to the database table commondb.quickstart_get_book
         *
         * @mbggenerated Tue May 29 12:53:11 MSK 2018
         */
        val builder: Builder
            get() = Builder()
    }
}

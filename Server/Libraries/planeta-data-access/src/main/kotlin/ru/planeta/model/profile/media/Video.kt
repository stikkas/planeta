package ru.planeta.model.profile.media

import ru.planeta.commons.external.oembed.provider.VideoType
import ru.planeta.commons.lang.DateUtils
import ru.planeta.model.enums.ObjectType
import ru.planeta.model.enums.PermissionLevel
import ru.planeta.model.profile.ProfileObject
import ru.planeta.model.profile.media.enums.VideoConversionStatus
import ru.planeta.model.profile.media.enums.VideoQuality

import java.util.Date
import java.util.EnumSet

/**
 * Common profile's video
 *
 * @author ds.kolyshev
 * Date: 27.10.11
 */
class Video : ProfileObject() {

    var name: String? = null
    var description: String? = null
    var imageUrl: String? = null
    var imageId: Long = 0
    var videoUrl: String? = null
    var storyboardUrl: String? = null
    var availableQualities: EnumSet<VideoQuality>? = null
    var status: VideoConversionStatus? = null
    var timeAdded: Date? = null
    var timeUpdated: Date? = null
    var viewsCount: Int = 0
    var downloadsCount: Int = 0
    var duration: Int = 0
    internal var videoTypeCode = VideoType.PLANETA.code
    var cachedVideoId: Long = 0
    internal var viewPermissionCode = PermissionLevel.EVERYBODY.code
    var width: Int = 0
    var height: Int = 0

    val humanDuration: String
        get() = DateUtils.humanDuration(duration * 1000)

    var viewPermission: PermissionLevel?
        get() = PermissionLevel.getByValue(viewPermissionCode)
        set(viewPermission) {
            this.viewPermissionCode = viewPermission?.code ?: PermissionLevel.EVERYBODY.code
        }

    var videoId: Long
        get() = voteObjectId
        set(videoId) {
            voteObjectId = videoId
        }

    internal var availableQualityModes: Int
        get() = VideoQuality.getCodeByEnumSet(availableQualities!!)
        set(availableQualityModes) {
            this.availableQualities = VideoQuality.getEnumSetFromCode(availableQualityModes)
        }

    internal var statusCode: Int
        get() = status!!.code
        set(status) {
            this.status = VideoConversionStatus.getByValue(status)
        }

    val qualityModesUrls: Map<VideoQuality, String>?
        get() = if (availableQualities == null) null else VideoQuality.getUrlsByVideoQualitySet(videoUrl, availableQualities)

    var videoType: VideoType?
        get() = VideoType.getByValue(videoTypeCode)
        set(videoType) {
            this.videoTypeCode = videoType?.code ?: VideoType.PLANETA.code
        }

    override var voteObjectType: ObjectType?
        get() = ObjectType.VIDEO
        set(value: ObjectType?) {
            super.voteObjectType = value
        }

    override var objectId: Long? = null
        get() = videoId

    override var objectType: ObjectType?
        get() = ObjectType.VIDEO
        set(value: ObjectType?) {
            super.objectType = value
        }

    companion object {

        private val serialVersionUID = 1L
    }
}

package ru.planeta.dao.commondb

import org.apache.ibatis.annotations.Mapper
import org.apache.ibatis.annotations.Param
import ru.planeta.model.common.UserLoginInfo

/**
 * User: sshendyapin
 * Date: 28.01.13
 * Time: 16:11
 */
@Mapper
interface UserLoginInfoDAO {

    fun insert(userLoginInfo: UserLoginInfo): Int

    fun update(userLoginInfo: UserLoginInfo): Int

    fun delete(@Param("profileId") profileId: Long, @Param("userIpAddress") userIpAddress: String, @Param("userAgent") userAgent: String): Int

    fun select(@Param("profileId") profileId: Long, @Param("userIpAddress") userIpAddress: String, @Param("userAgent") userAgent: String): UserLoginInfo

    fun selectLastUserLoginInfo(profileId: Long): UserLoginInfo
}

package ru.planeta.dao.bibliodb

import org.apache.ibatis.annotations.Mapper
import org.apache.ibatis.annotations.Param
import ru.planeta.model.bibliodb.Library
import ru.planeta.model.bibliodb.LibraryCluster
import ru.planeta.model.bibliodb.enums.LibraryType

@Mapper
interface LibraryDAO {

    fun countActiveLibraries(): Long

    fun insert(library: Library)

    fun select(libraryId: Long): Library?

    fun selectList(@Param("limit") limit: Long, @Param("offset") offset: Long): List<Library>

    fun selectListByIds(libraryIds: List<Long>): List<Library>

    fun update(library: Library)

    fun selectFirstLibraryWithoutCoordinates(offset: Int): Library

    fun selectFirstLibraryWithoutRegion(offset: Int): Library

    fun randomLibrary(exludedLibraries: Collection<Library>): Library

    fun searchLibraries(@Param("searchStr") searchStr: String, @Param("libraryType") libraryType: LibraryType?,
                        @Param("north") north: Double, @Param("south") south: Double, @Param("east") east: Double,
                        @Param("west") west: Double): List<Library>

    fun selectFirstLibraryWithoutPostIndex(offset: Long): Library

    fun hasLibraryRequests(@Param("name") name: String, @Param("email") email: String): Boolean

    fun searchLibraryClusters(@Param("searchStr") searchStr: String, @Param("libraryType") libraryType: LibraryType?,
                              @Param("north") north: Double, @Param("south") south: Double,
                              @Param("east") east: Double, @Param("west") west: Double): List<LibraryCluster>
}


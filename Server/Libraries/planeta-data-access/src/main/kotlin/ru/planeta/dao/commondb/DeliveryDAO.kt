package ru.planeta.dao.commondb

import org.apache.ibatis.annotations.Mapper
import org.apache.ibatis.annotations.Param
import ru.planeta.model.common.delivery.BaseDelivery
import ru.planeta.model.common.delivery.LinkedDelivery
import ru.planeta.model.common.delivery.SubjectType
import java.math.BigDecimal

/**
 * @author Andrew.Arefyev@gmail.com
 * 30.10.13 13:25
 */
@Mapper
interface DeliveryDAO {

    fun select(@Param("shareId") shareId: Long, @Param("subjectType") subjectType: SubjectType): List<LinkedDelivery>

    fun selectEnabled(@Param("shareId") shareId: Long, @Param("subjectType") subjectType: SubjectType): List<LinkedDelivery>

    fun addDeliveryToShare(@Param("shareId") shareId: Long, @Param("deliveryId") deliveryId: Long, @Param("publicNote") publicNote: String, @Param("price") price: BigDecimal, @Param("subjectType") subjectType: SubjectType)

    fun removeBaseDelivery(deliveryTemplateId: Long)

    fun updateBaseDelivery(delivery: BaseDelivery)

    fun insert(delivery: BaseDelivery)

    fun selectBaseDeliveries(@Param("serviceId") serviceId: Long?, @Param("searchString") searchString: String?): List<BaseDelivery>

    fun updateLinkedDelivery(delivery: LinkedDelivery)

    fun selectBaseDelivery(deliveryId: Long): BaseDelivery

    /**
     * unlincked delivery services except default ones.
     *
     * @param shareId
     * @param subjectType
     * @return
     */
    fun selectUnlinkedBaseDeliveries(@Param("shareId") shareId: Long, @Param("subjectType") subjectType: SubjectType): List<BaseDelivery>

    fun addDeliveryToShare(delivery: LinkedDelivery)

    fun select(@Param("subjectId") subjectId: Long, @Param("serviceId") serviceId: Long, @Param("subjectType") subjectType: SubjectType): LinkedDelivery
}

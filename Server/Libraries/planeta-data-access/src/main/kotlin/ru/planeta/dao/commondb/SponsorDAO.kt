package ru.planeta.dao.commondb

import org.apache.ibatis.annotations.Mapper
import org.apache.ibatis.annotations.Param
import ru.planeta.model.common.campaign.Sponsor

/**
 * Created by alexa_000 on 12.03.2015.
 */
@Mapper
interface SponsorDAO {
    fun selectSponsor(@Param("alias")alias: String): Sponsor

    fun selectAll(): List<Sponsor>

    fun selectSponsors(@Param("aliasList") aliasList: List<String>): List<Sponsor>

    fun insert(@Param("sponsor") sponsor: Sponsor)

    fun update(@Param("sponsor")sponsor: Sponsor)

    fun delete(@Param("alias")alias: String)
}

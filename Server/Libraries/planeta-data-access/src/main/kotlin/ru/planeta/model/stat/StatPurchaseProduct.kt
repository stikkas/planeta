package ru.planeta.model.stat

import ru.planeta.model.profile.Profile
import ru.planeta.model.shop.Product

class StatPurchaseProduct : StatPurchaseCommon() {
    var groupProfile: Profile? = null
        protected set

    var product: Product? = null

    override operator fun compareTo(o: Any): Int {
        val that: StatPurchaseProduct
        try {
            that = o as StatPurchaseProduct
        } catch (e: Exception) {
            return 1
        }

        var compare1 = 0
        var compare2 = 0
        val gp = this.groupProfile?.displayName
        if (gp != null) {
            compare1 = gp.compareTo(that.groupProfile?.displayName ?: "")
        }
        val name = this.name
        if (name != null) {
            compare2 = name.compareTo(that.name ?: "")
        }
        if (compare1 != 0) return compare1
        return if (compare2 != 0) compare2 else 0
    }

    fun setMerchantProfile(groupProfile: Profile) {
        this.groupProfile = groupProfile
    }

    override fun equals(o: Any?): Boolean {
        if (this === o) return true
        if (javaClass != o?.javaClass) return false
        if (!super.equals(o)) return false

        o as StatPurchaseProduct

        if (groupProfile != o.groupProfile) return false

        return true
    }

    override fun hashCode(): Int {
        var result = super.hashCode()
        result = 31 * result + (groupProfile?.hashCode() ?: 0)
        return result
    }

}

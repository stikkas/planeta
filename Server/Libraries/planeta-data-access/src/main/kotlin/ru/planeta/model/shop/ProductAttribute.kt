package ru.planeta.model.shop

import com.fasterxml.jackson.annotation.JsonIgnoreProperties

import java.io.Serializable

/**
 * User: m.shulepov
 */
@JsonIgnoreProperties(ignoreUnknown = true)
class ProductAttribute : Serializable {

    var attributeId: Long = 0
    var attributeTypeId: Long = 0
    var value: String? = null
    var index: Int = 0

    override fun equals(o: Any?): Boolean {
        if (this === o) return true
        if (o == null || javaClass != o.javaClass) return false

        val attribute = o as ProductAttribute?

        if (attributeId != attribute!!.attributeId) return false
        if (attributeTypeId != attribute.attributeTypeId) return false
        return if (if (value != null) value != attribute.value else attribute.value != null) false else true

    }

    override fun hashCode(): Int {
        var result = (attributeId xor attributeId.ushr(32)).toInt()
        result = 31 * result + (attributeTypeId xor attributeTypeId.ushr(32)).toInt()
        result = 31 * result + if (value != null) value!!.hashCode() else 0
        return result
    }
}

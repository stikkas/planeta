package ru.planeta.dao.msgdb

import org.apache.ibatis.annotations.Mapper
import ru.planeta.model.msg.Dialog

/**
 * Data access object for working with Dialogs.
 *
 * @author ameshkov
 */
@Mapper
interface DialogDAO {

    /**
     * Selects dialog by it's identifier
     *
     * @param dialogId
     * @return
     */
    fun select(dialogId: Long): Dialog

    /**
     * Updates dialog object (in the database).
     * This method updates only: name, last_message_id, messages_count
     *
     * @param dialog
     */
    fun update(dialog: Dialog)

    /**
     * Inserts new dialog to the database table
     *
     * @param dialog
     */
    fun insert(dialog: Dialog)
}

package ru.planeta.dao.mappers.enums

import org.apache.ibatis.type.JdbcType
import org.apache.ibatis.type.TypeHandler
import ru.planeta.model.enums.ProfileType

import java.sql.CallableStatement
import java.sql.PreparedStatement
import java.sql.ResultSet
import java.sql.SQLException

/**
 * @author Andrew.Arefyev@gmail.com
 * 11.12.13 22:18
 */
class ProfileTypeHandler : TypeHandler<ProfileType> {

    @Throws(SQLException::class)
    override fun setParameter(ps: PreparedStatement, i: Int, parameter: ProfileType, jdbcType: JdbcType) {
        ps.setInt(i, parameter.code)
    }

    @Throws(SQLException::class)
    override fun getResult(rs: ResultSet, columnName: String): ProfileType {
        return ProfileType.getByValue(rs.getInt(columnName))
    }

    @Throws(SQLException::class)
    override fun getResult(cs: CallableStatement, columnIndex: Int): ProfileType {
        return ProfileType.getByValue(cs.getInt(columnIndex))
    }

    @Throws(SQLException::class)
    override fun getResult(rs: ResultSet, columnIndex: Int): ProfileType {
        return ProfileType.getByValue(rs.getInt(columnIndex))
    }

}

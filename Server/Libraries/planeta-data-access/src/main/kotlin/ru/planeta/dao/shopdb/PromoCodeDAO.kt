package ru.planeta.dao.shopdb

import org.apache.ibatis.annotations.Mapper
import org.apache.ibatis.annotations.Param
import ru.planeta.model.shop.ExtendedPromoCode
import ru.planeta.model.shop.PromoCode

/**
 * User: a.savanovich
 * Date: 09.10.13
 * Time: 18:09
 */
@Mapper
interface PromoCodeDAO {
    fun getPromoCodeByCode(code: String): PromoCode

    fun getPromoCodeById(promoCodeId: Long): PromoCode

    fun getPromoCodeIdByCode(@Param("code") code: String, @Param("alsoDisabled") alsoDisabled: Boolean): Long?

    fun getExtendedPromoCodeById(promoCodeId: Long): ExtendedPromoCode

    fun getPromoCodesList(@Param("searchString") searchString: String, @Param("offset") offset: Int, @Param("limit") limit: Int): List<PromoCode>

    fun getExtendedPromoCodesList(@Param("searchString") searchString: String, @Param("offset") offset: Int, @Param("limit") limit: Int): List<ExtendedPromoCode>

    fun getExtendedPromoCodesListCount(searchString: String): Long

    fun insert(promoCode: PromoCode)

    fun update(promoCode: PromoCode)

    fun markDisabled(promoCodeId: Long)

    fun markEnabled(promoCodeId: Long)
}

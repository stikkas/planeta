package ru.planeta.model.stat

import ru.planeta.model.profile.Profile

import java.util.Date

class StatPurchaseShare : StatPurchaseCommon() {
    //
    var campaignId: Long = 0
    var campaignName: String? = null
    var campaignTimeStart: Date? = null
    var campaignTimeFinish: Date? = null
    //
    var groupId: Long = 0
    var groupProfile: Profile? = null

    override operator fun compareTo(o: Any): Int {
        val that: StatPurchaseShare
        try {
            that = o as StatPurchaseShare
        } catch (e: Exception) {
            return 1
        }

        var compare1 = 0
        var compare2 = 0
        var compare3 = 0
        try {
            compare1 = groupProfile!!.displayName!!.compareTo(that.groupProfile!!.displayName ?: "")
        } catch (npe: NullPointerException) {
            if (groupProfile == null || groupProfile!!.displayName == null) {
                compare1 -= 1
            }
            if (that.groupProfile == null || that.groupProfile!!.displayName == null) {
                compare1 += 1
            }
        }

        try {
            compare2 = campaignName!!.compareTo(that.campaignName!!)
        } catch (npe: NullPointerException) {
            if (campaignName == null) {
                compare1 -= 1
            }
            if (that.campaignName == null) {
                compare1 += 1
            }
        }

        try {
            compare3 = name!!.compareTo(that.name ?: "")
        } catch (npe: NullPointerException) {
            if (name == null) {
                compare1 -= 1
            }
            if (that.name == null) {
                compare1 += 1
            }
        }

        if (compare1 != 0) {
            return compare1
        }
        if (compare2 != 0) {
            return compare2
        }
        return if (compare3 != 0) {
            compare3
        } else 0
    }

    override fun equals(o: Any?): Boolean {
        if (this === o) return true
        if (javaClass != o?.javaClass) return false
        if (!super.equals(o)) return false

        o as StatPurchaseShare

        if (campaignId != o.campaignId) return false
        if (groupId != o.groupId) return false

        return true
    }

    override fun hashCode(): Int {
        var result = super.hashCode()
        result = 31 * result + campaignId.hashCode()
        result = 31 * result + groupId.hashCode()
        return result
    }


}

package ru.planeta.model.shop

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import ru.planeta.model.enums.ObjectType
import ru.planeta.model.profile.Profile
import ru.planeta.model.shop.enums.ProductCategory
import java.util.*

/**
 * This class represents product.
 * User: a.savanovich
 * Date: 18.06.12
 * Time: 12:28
 */
@JsonIgnoreProperties(ignoreUnknown = true)
open class ProductInfo : Product {

    // synthetic fields
    var childrenProducts: MutableList<ProductInfo>? = null
        get() {
            if (field == null) {
                this.childrenProducts = ArrayList()
            }
            return field
        }
    var groupName: String? = null
    var referrer: Profile? = null
    var donate: Product? = null

    // sum of quantities of a product and of it's direct children(if it has any)
    var childrenCount: Int = 0

    override var objectId: Long? = null
        get() = productId

    override var objectType: ObjectType?
        get() = voteObjectType
        set(objectType) {
            this.voteObjectType = ObjectType.PRODUCT
        }

    val readyForPurchaseQuantity: Int
        get() = totalQuantity - totalOnHoldQuantity

    val isAvailable: Boolean
        get() = this.isAvailable(1)

    constructor(product: Product) : super(product) {}

    constructor() : super()

    fun isAvailable(count: Int): Boolean {
        return productCategory === ProductCategory.DIGITAL || readyForPurchaseQuantity >= count
    }

    fun dropChildrenProducts() {
        childrenProducts = null
    }

    fun cloneWithChildren(productInfo: ProductInfo) {
        clone(productInfo)
        this.groupName = productInfo.groupName
        this.referrer = productInfo.referrer
        this.donate = productInfo.donate

        val newChildren = ArrayList<ProductInfo>()
        val children = productInfo.childrenProducts
        children?.forEach {
            val newChild = ProductInfo()
            newChild.clone(it)
            newChildren.add(newChild)
        }
        this.childrenProducts = newChildren

    }
}

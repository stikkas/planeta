package ru.planeta.model.common

import ru.planeta.model.enums.OrderObjectType
import java.util.Date

/**
 * Contains order info.
 * User: m.shulepov
 * Date: 26.04.12
 * Time: 11:48
 */
class OrderObjectInfo : OrderObject() {

    var merchantAlias: String? = null
    var merchantName: String? = null
    var merchantImageUrl: String? = null
    var objectName: String? = null
    var objectImageUrl: String? = null
    var ownerId: Long = 0
    var ownerName: String? = null
    var info: Any? = null
    //private String downloadUrl;
    var downloadUrls: Collection<String>? = null
    var startSaleDate: Date? = null
    override var estimatedDeliveryTime: Date? = null
    /**
     * for usage in Shop orders (merchant orders(out), and ShoppingCartItem(in)) .
     */
    var count = 1
    // TODO: migrate to orderObjectType
    var objectType: OrderObjectType? = null

    companion object {

        fun createFrom(`object`: OrderObject): OrderObjectInfo {
            val result = OrderObjectInfo()

            result.creditTransactionId = `object`.creditTransactionId
            result.debitTransactionId = `object`.debitTransactionId
            result.cancelCreditTransactionId = `object`.cancelCreditTransactionId
            result.cancelDebitTransactionId = `object`.cancelDebitTransactionId

            result.orderObjectId = `object`.orderObjectId
            result.orderId = `object`.orderId
            result.objectId = `object`.objectId
            result.objectType = `object`.orderObjectType
            result.merchantId = `object`.merchantId
            result.price = `object`.price
            result.comment = `object`.comment
            result.estimatedDeliveryTime = `object`.estimatedDeliveryTime

            return result
        }
    }
}

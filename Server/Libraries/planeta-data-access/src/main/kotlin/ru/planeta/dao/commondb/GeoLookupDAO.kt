package ru.planeta.dao.commondb

import org.apache.ibatis.annotations.Mapper
import org.apache.ibatis.annotations.Param
import ru.planeta.commons.geo.IpBlock

/**
 * User: a.savanovich
 * Date: 17.04.12
 * Time: 13:05
 */
@Mapper
interface GeoLookupDAO {
    fun selectBlocks(@Param("offset") offset: Int, @Param("limit") limit: Int): List<IpBlock>

    fun getCityId(cityId: Int): Int?

    fun getCountryByCode(code: String): Int?
}

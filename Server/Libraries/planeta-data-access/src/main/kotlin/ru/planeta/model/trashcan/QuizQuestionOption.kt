package ru.planeta.model.trashcan

class QuizQuestionOption {
    var quizQuestionOptionId: Long = 0
    var quizQuestionId: Long = 0
    var optionText: String? = null
    var optionOrderNum: Int = 0
}

package ru.planeta.model.shop.enums

import ru.planeta.model.enums.Codable

import java.util.EnumSet
import java.util.HashMap

/**
 * Indicates state of a product
 */
enum class ProductState private constructor(override val code: Int) : Codable {

    @Deprecated("")
    GROUP(0),
    META(1), ATTRIBUTE(2), SOLID_PRODUCT(3);


    companion object {

        private val lookup = HashMap<Int, ProductState>()

        init {
            for (s in EnumSet.allOf(ProductState::class.java)) {
                lookup[s.code] = s
            }
        }

        fun getByValue(code: Int): ProductState? {
            return lookup[code]
        }
    }
}

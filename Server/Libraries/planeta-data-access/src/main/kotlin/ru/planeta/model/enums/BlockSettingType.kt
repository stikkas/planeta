package ru.planeta.model.enums

import java.util.EnumSet
import java.util.HashMap

/**
 * Enum BlockSettingType.
 * If new type is added, it also have to be added to DefaultBlockSettings
 *
 * @author a.tropnikov
 */
enum class BlockSettingType (override val code: Int) : Codable {

    /**
     * Group's settings
     */
    GROUP_PROMO(23),
    GROUP_FAN_BLOG(36), GROUP_ARTISTS(37), GROUP_ADMINS(38),
    /**
     * User's settings
     */

    USER_GROUPS(109),
    USER_WRITE_MESSAGES(153),

    /**
     * shared settings
     */
    WALL(301),
    PHOTO(302), VIDEO(303), AUDIO(304), BLOG(305), MEMBERS(306), ABOUT(307), CONTACT_INFO(308), PROFILE_INFO(309),
    STATUS(310), EVENTS(311), SETTINGS(312), BROADCAST(313);


    companion object {

        private val lookup = HashMap<Int, BlockSettingType>()

        init {
            for (s in EnumSet.allOf(BlockSettingType::class.java)) {
                lookup[s.code] = s
            }
        }

        fun getByValue(code: Int): BlockSettingType? {
            return lookup[code]
        }
    }

}

package ru.planeta.model.bibliodb.enums

import java.util.HashMap
import ru.planeta.model.enums.Codable

/**
 * Статус заявки
 * User: Serge Blagodatskih<stikkas17></stikkas17>@gmail.com><br></br>
 * Date: 27.04.16<br></br>
 * Time: 12:33
 */
enum class RequestStatus private constructor(override val code: Int) : Codable {
    NEW(1), IN_PROGRESS(2), DONE(3), REJECTED(4);


    companion object {

        private val lookup = HashMap<Int, RequestStatus>()

        init {
            for (s in values()) {
                lookup[s.code] = s
            }
        }

        fun getByValue(code: Int): RequestStatus {
            return lookup[code] ?: NEW
        }
    }
}

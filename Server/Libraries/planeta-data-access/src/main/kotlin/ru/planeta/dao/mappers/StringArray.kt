package ru.planeta.dao.mappers

import java.sql.Types

/**
 * User: a.savanovich
 * Date: 29.06.12
 * Time: 15:08
 */
class StringArray : SqlArrayAdapter {
    private var array: List<String>? = null

    constructor(array: List<String>) {
        if (array == null) {
            throw IllegalArgumentException("parameter array should not be null")
        }
        this.array = array
    }

    override fun getBaseType(): Int {
        return Types.VARCHAR
    }

    override fun getBaseTypeName(): String {
        return "text"
    }

    override fun toString(): String {
        var result = "{"
        array?.forEachIndexed { index, s ->
            if (index > 0) {
                result += ","
            }
            result += s
        }
        result += "}"
        return result
    }

    override fun free() {
    }
}


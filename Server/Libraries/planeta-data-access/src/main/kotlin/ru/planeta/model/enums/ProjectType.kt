package ru.planeta.model.enums

import java.util.HashMap

/**
 * Enum that contains projects names, codes and payable flags.
 * User: eshevchenko
 */
enum class ProjectType private constructor(val code: Int, val label: String) {

    MAIN(1, "Main"),
    CAMPAIGN(2, "Campaign"),
    CONCERT(3, "Concert"),
    CONCERT_NEW(11, "Concert"),
    SHOP(4, "Shop"),
    TV(5, "TV"),
    MOBILE(6, "Mobile"),
    ADMIN(7, "Admin"),
    AFFILIATE(8, "Affiliate"),
    PAYMENT_GATE(9, "PaymentGate"),
    WIDGETS(10, "Widgets"),
    STATISTIC_SERVICE(12, "StatisticService"),
    SCHOOL(13, "School"),
    INVEST(14, "Invest"),
    BIBLIO(15, "Biblio"),
    INVEST_ALL_ALLOWED(16, "InvestAllAllowed"),
    CHARITY(17, "Charity"),
    NOTIFICATION_SERVICE(20, "Notification Service"),
    STATIC(30, "Static"),
    IM(31, "Im"),
    LINK(32, "ShortLink"),
    PROMO(33, "Promo"),
    DIGITAL_PRODUCT(34, "DigitalProductService"),
    DELIVERY(35, "Delivery");


    companion object {

        private val lookup = HashMap<Int, ProjectType>()

        init {
            for (projectType in values()) {
                lookup[projectType.code] = projectType
            }
        }

        fun getByCode(code: Int): ProjectType {
            return lookup[code] ?: MAIN
        }

        fun getByName(name: String): ProjectType {
            for (type in values()) {
                if (type.label.equals(name, ignoreCase = true))
                    return type
            }
            return ProjectType.MAIN
        }
    }

}

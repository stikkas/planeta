package ru.planeta.dao.msgdb

import org.apache.ibatis.annotations.Mapper
import ru.planeta.model.msg.DialogUser

/**
 * Data access object for dialog participants dao
 *
 * @author ameshkov
 */
@Mapper
interface DialogUserDAO {

    /**
     * Selects list of dialog participants
     *
     * @param dialogId
     * @return
     */
    fun select(dialogId: Long): List<DialogUser>

    /**
     * Inserts new dialog participant
     *
     * @param dialogUser
     */
    fun insert(dialogUser: DialogUser)

}

package ru.planeta.model.commondb

class PaymentErrorsEx : PaymentErrors() {
    var email: String? = null

    var commentsCount: Int = 0

    var paymentMethodId: Long = 0

    var paymentProviderId: Long = 0
}

package ru.planeta.model.common

import ru.planeta.model.enums.OrderObjectType

import java.math.BigDecimal
import java.util.Date

/**
 * Created by eshevchenko.
 */
open class UserCallback {

    var userCallbackId: Long = 0
    var type: Type? = null
    var userId: Long = 0
    var userPhone: String? = null
    var paymentId: Long? = 0
    var orderType: OrderObjectType? = null
    var orderObjectId: Long? = 0
    var amount: BigDecimal? = null
    var managerId: Long = 0
    var isProcessed: Boolean = false
    var timeAdded: Date? = null
    var timeUpdated: Date? = null
    var commentsCount: Int? = null

    enum class Type {
        FAILURE_PAYMENT, ORDERING_PROBLEM
    }

    companion object {


        private fun forUser(userId: Long, userPhone: String, amount: BigDecimal): UserCallback {
            val result = UserCallback()
            result.userId = userId
            result.userPhone = userPhone
            result.amount = amount
            return result
        }

        fun createFailurePayment(userId: Long, userPhone: String, paymentId: Long?, amount: BigDecimal): UserCallback {
            val result = forUser(userId, userPhone, amount)
            result.type = Type.FAILURE_PAYMENT
            result.paymentId = paymentId
            return result
        }

        fun createOrderingProblem(userId: Long, userPhone: String, orderType: OrderObjectType, objectId: Long?, amount: BigDecimal): UserCallback {
            val result = forUser(userId, userPhone, amount)
            result.type = Type.ORDERING_PROBLEM
            result.orderType = orderType
            result.orderObjectId = objectId
            return result
        }
    }
}

package ru.planeta.dao.commondb

import org.apache.ibatis.annotations.Mapper
import org.apache.ibatis.annotations.Param
import ru.planeta.model.commondb.FaqParagraph
import ru.planeta.model.commondb.FaqParagraphExample

@Mapper
interface FaqParagraphMapper {
    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table commondb.faq_paragraph
     *
     * @mbggenerated Tue May 29 12:53:11 MSK 2018
     */
    fun countByExample(example: FaqParagraphExample): Int

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table commondb.faq_paragraph
     *
     * @mbggenerated Tue May 29 12:53:11 MSK 2018
     */
    fun deleteByExample(example: FaqParagraphExample): Int

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table commondb.faq_paragraph
     *
     * @mbggenerated Tue May 29 12:53:11 MSK 2018
     */
    fun deleteByPrimaryKey(faqParagraphId: Long?): Int

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table commondb.faq_paragraph
     *
     * @mbggenerated Tue May 29 12:53:11 MSK 2018
     */
    fun insert(record: FaqParagraph): Int

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table commondb.faq_paragraph
     *
     * @mbggenerated Tue May 29 12:53:11 MSK 2018
     */
    fun insertSelective(record: FaqParagraph): Int

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table commondb.faq_paragraph
     *
     * @mbggenerated Tue May 29 12:53:11 MSK 2018
     */
    fun selectByExample(example: FaqParagraphExample): List<FaqParagraph>

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table commondb.faq_paragraph
     *
     * @mbggenerated Tue May 29 12:53:11 MSK 2018
     */
    fun selectByPrimaryKey(faqParagraphId: Long?): FaqParagraph

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table commondb.faq_paragraph
     *
     * @mbggenerated Tue May 29 12:53:11 MSK 2018
     */
    fun updateByExampleSelective(@Param("record") record: FaqParagraph, @Param("example") example: FaqParagraphExample): Int

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table commondb.faq_paragraph
     *
     * @mbggenerated Tue May 29 12:53:11 MSK 2018
     */
    fun updateByExample(@Param("record") record: FaqParagraph, @Param("example") example: FaqParagraphExample): Int

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table commondb.faq_paragraph
     *
     * @mbggenerated Tue May 29 12:53:11 MSK 2018
     */
    fun updateByPrimaryKeySelective(record: FaqParagraph): Int

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table commondb.faq_paragraph
     *
     * @mbggenerated Tue May 29 12:53:11 MSK 2018
     */
    fun updateByPrimaryKey(record: FaqParagraph): Int

    fun selectByIdList(faqParagraphIdList: List<Long>): List<FaqParagraph>
}

package ru.planeta.model.common

/**
 * User: s.makarov
 * Date: 25.11.13
 * Time: 15:23
 */
class ContractorForCurrentObject : Contractor() {
    var objectId: Long = 0
}

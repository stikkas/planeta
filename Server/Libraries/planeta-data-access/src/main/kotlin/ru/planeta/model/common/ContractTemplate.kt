package ru.planeta.model.common

import ru.planeta.model.enums.ContractorType

class ContractTemplate {
    var contractorType: ContractorType? = null
    var header: String? = null
    var partiesDetails: String? = null
}

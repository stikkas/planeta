package ru.planeta.dao.commondb

import org.apache.ibatis.annotations.Mapper
import org.apache.ibatis.annotations.Param
import java.util.Date
import ru.planeta.model.common.InvestingOrderInfo
import ru.planeta.model.enums.ContractorType
import ru.planeta.model.enums.ModerateStatus

/**
 *
 * User: Serge Blagodatskih<stikkas17></stikkas17>@gmail.com><br></br>
 * Date: 27.01.16<br></br>
 * Time: 15:07
 */
@Mapper
interface InvestingOrderInfoDAO {

    fun update(info: InvestingOrderInfo)

    fun insert(info: InvestingOrderInfo)

    fun delete(id: Long)

    fun select(@Param("userId") userId: Long, @Param("userType") userType: ContractorType): InvestingOrderInfo?

    fun select(id: Long): InvestingOrderInfo?

    fun selectList(@Param("offset") offset: Int, @Param("limit") limit: Int): List<InvestingOrderInfo>

    fun selectList(@Param("status") status: ModerateStatus, @Param("start") start: Date, @Param("end") end: Date, @Param("offset") offset: Int, @Param("limit") limit: Int): List<InvestingOrderInfo>

    fun selectList(ids: List<Long>): List<InvestingOrderInfo>

    fun count(): Long
}

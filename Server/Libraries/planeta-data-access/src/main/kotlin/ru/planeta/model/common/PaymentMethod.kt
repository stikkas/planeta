package ru.planeta.model.common

/**
 * Created with IntelliJ IDEA.
 * User: a.volkov
 * Date: 23.04.14
 * Time: 16:24
 */
class PaymentMethod : Identified {

    override var id: Long = 0
    var parentId: Long = 0
    var alias: Alias? = null
    var imageUrl: String? = null
    var name: String? = null
    var nameEn: String? = null
    var description: String? = null
    var descriptionEn: String? = null
    var isMobile: Boolean = false
    var isNeedPhone: Boolean = false
    var isCard: Boolean = false
    var isPromo: Boolean = false
    var isInternal: Boolean = false

    var children: List<PaymentMethod>? = null

    enum class Alias {
        BEELINE, MEGAFON, MTS, CARD, CARD_ALFA, YANDEX_MONEY, QIWI, WEB_MONEY, OTHER, PLANETA, PLANETA_PROMO,
        CASH, INTERNET_BANKING, MOBILE, SBERBANK_ONLINE, ALFA_CLICK, PSB_RETAIL, OTHER_BANKS, BANK_OFFICE, RS_BANK,
        TINKOFF, PAYMENT_TERMINAL, CELL_POINT, MONEY_TRANSFER, OTHER_CASH, W1, INVOICE, CARD_SBERBANK, ELECTRONIC_MONEY
    }
}

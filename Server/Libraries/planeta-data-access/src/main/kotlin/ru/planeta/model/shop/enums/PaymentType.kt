package ru.planeta.model.shop.enums

import ru.planeta.model.enums.Codable

import java.util.HashMap

/**
 * User: a.savanovich
 * Date: 22.06.12
 * Time: 11:50
 */
enum class PaymentType private constructor(override val code: Int) : Codable {
    NOT_SET(0), CASH(1), PAYMENT_SYSTEM(2);


    companion object {

        private val lookup = HashMap<Int, PaymentType>()

        init {
            for (s in values()) {
                lookup[s.code] = s
            }
        }

        fun getByValue(code: Int): PaymentType? {
            return lookup[code]
        }
    }
}

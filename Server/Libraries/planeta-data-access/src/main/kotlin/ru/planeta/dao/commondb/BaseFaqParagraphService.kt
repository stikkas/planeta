package ru.planeta.dao.commondb

import org.apache.ibatis.annotations.Param
import java.util.ArrayList
import org.springframework.beans.factory.annotation.Autowired
import ru.planeta.model.commondb.FaqParagraph
import ru.planeta.model.commondb.FaqParagraphExample

abstract class BaseFaqParagraphService {
    @Autowired
    protected var faqParagraphMapper: FaqParagraphMapper? = null

    val orderByClause: String
        get() = "faq_paragraph_id"

    //**************************************************************************************************
    //*********************************  Create ** Update ** Delete  *************************************
    //**************************************************************************************************
    open fun insertFaqParagraph(faqParagraph: FaqParagraph) {


        faqParagraphMapper!!.insertSelective(faqParagraph)
    }

    fun insertFaqParagraphAllFields(faqParagraph: FaqParagraph) {


        faqParagraphMapper!!.insert(faqParagraph)
    }

    fun updateFaqParagraph(faqParagraph: FaqParagraph) {

        faqParagraphMapper!!.updateByPrimaryKeySelective(faqParagraph)
    }

    fun updateFaqParagraphAllFields(faqParagraph: FaqParagraph) {

        faqParagraphMapper!!.updateByPrimaryKey(faqParagraph)
    }

    fun insertOrUpdateFaqParagraph(faqParagraph: FaqParagraph) {

        val selectedFaqParagraph = faqParagraphMapper!!.selectByPrimaryKey(faqParagraph.faqParagraphId)
        if (selectedFaqParagraph == null) {

            faqParagraphMapper!!.insertSelective(faqParagraph)
        } else {
            faqParagraphMapper!!.updateByPrimaryKeySelective(faqParagraph)
        }
    }

    fun insertOrUpdateFaqParagraphAllFields(faqParagraph: FaqParagraph) {

        val selectedFaqParagraph = faqParagraphMapper!!.selectByPrimaryKey(faqParagraph.faqParagraphId)
        if (selectedFaqParagraph == null) {

            faqParagraphMapper!!.insert(faqParagraph)
        } else {
            faqParagraphMapper!!.updateByPrimaryKey(faqParagraph)
        }
    }

    fun deleteFaqParagraph(id: Long?) {
        faqParagraphMapper!!.deleteByPrimaryKey(id)
    }

    //**************************************************************************************************
    //***************************************** faq_paragraph ********************************************
    //************************************** SelectByPrimaryKey ******************************************
    //**************************************************************************************************
    fun selectFaqParagraph(faqParagraphId: Long?): FaqParagraph {
        return faqParagraphMapper!!.selectByPrimaryKey(faqParagraphId)
    }

    //**************************************************************************************************
    //***************************************** faq_paragraph ********************************************
    //**************************************************************************************************
    private fun COMMENT_SECTION_FaqParagraph() {

    }

    fun getFaqParagraphExample(@Param("offset") offset: Int, @Param("limit") limit: Int): FaqParagraphExample {
        val example = FaqParagraphExample()
        example.offset = offset
        example.limit = limit
        example.or()
        example.orderByClause = orderByClause
        return example
    }

    fun selectFaqParagraphCount(): Int {
        return faqParagraphMapper!!.countByExample(getFaqParagraphExample(0, 0))
    }

    fun selectFaqParagraphList(@Param("offset") offset: Int, @Param("limit") limit: Int): List<FaqParagraph> {
        return faqParagraphMapper!!.selectByExample(getFaqParagraphExample(offset, limit))
    }

    fun selectFaqParagraphList(faqParagraphIdList: List<Long>?): List<FaqParagraph> {
        return if (faqParagraphIdList == null || faqParagraphIdList.isEmpty()) ArrayList() else faqParagraphMapper!!.selectByIdList(faqParagraphIdList)
    }

    //**************************************************************************************************
    //***************************************** faq_paragraph ********************************************
    //***************************************** byFaqArticle *********************************************
    //*************************************** Long faqArticleId ******************************************
    //**************************************************************************************************
    private fun COMMENT_SECTION_FaqParagraphByFaqArticle(faqArticleId: Long?) {

    }

    fun getFaqParagraphByFaqArticleExample(@Param("faqArticleId") faqArticleId: Long?, @Param("offset") offset: Int, @Param("limit") limit: Int): FaqParagraphExample {
        val example = FaqParagraphExample()
        example.offset = offset
        example.limit = limit
        example.or()
                .andFaqArticleIdEqualTo(faqArticleId)
        example.orderByClause = "order_num"
        return example
    }

    fun selectFaqParagraphCountByFaqArticle(faqArticleId: Long?): Int {
        return faqParagraphMapper!!.countByExample(getFaqParagraphByFaqArticleExample(faqArticleId, 0, 0))
    }

    fun selectFaqParagraphListByFaqArticle(@Param("faqArticleId") faqArticleId: Long?, @Param("offset") offset: Int, @Param("limit") limit: Int): List<FaqParagraph> {
        return faqParagraphMapper!!.selectByExample(getFaqParagraphByFaqArticleExample(faqArticleId, offset, limit))
    }

    fun selectFaqParagraphListByFaqArticle(@Param("faqArticleId") faqArticleId: Long?, @Param("faqParagraphIdList") faqParagraphIdList: List<Long>?): List<FaqParagraph> {
        return if (faqParagraphIdList == null || faqParagraphIdList.isEmpty()) ArrayList() else faqParagraphMapper!!.selectByIdList(faqParagraphIdList)
    }
}

package ru.planeta.model.common.delivery

import ru.planeta.model.enums.OrderObjectType

import java.util.EnumSet
import java.util.HashMap

/**
 * @author Andrew.Arefyev@gmail.com
 * 24.06.2014 11:43
 */
enum class SubjectType private constructor(val code: Int) {
    SHARE(1),
    SHOP(2);


    companion object {

        private val lookup = HashMap<Int, SubjectType>()

        init {
            for (s in EnumSet.allOf(SubjectType::class.java)) {
                lookup[s.code] = s
            }
        }

        fun getByValue(code: Int): SubjectType? {
            return lookup[code]
        }

        fun fromOrderObjectType(orderObjectType: OrderObjectType): SubjectType? {
            when (orderObjectType) {
                OrderObjectType.SHARE -> return SHARE
                OrderObjectType.PRODUCT, OrderObjectType.BONUS -> return SHOP
                else -> return null
            }
        }
    }
}

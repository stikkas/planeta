package ru.planeta.dao.statdb

import org.apache.ibatis.annotations.Mapper
import ru.planeta.model.stat.StatStaticNode

/**
 * @author p.vyazankin
 * @since 11/28/12 1:38 PM
 */
@Mapper
interface StatStaticNodeDAO {
    fun save(statStaticNode: StatStaticNode)
    fun findAll(): List<StatStaticNode>
}

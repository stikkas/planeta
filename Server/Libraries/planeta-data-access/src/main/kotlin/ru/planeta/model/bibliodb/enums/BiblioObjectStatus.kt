package ru.planeta.model.bibliodb.enums

import ru.planeta.model.enums.Codable
import java.util.*

/*
 * Created by Alexey on 07.04.2016.
 */

enum class BiblioObjectStatus private constructor(// now used only for books, not for libs

        override val code: Int) : Codable {
    ACTIVE(1),
    PAUSED(2),
    DELETED(3),
    REQUEST(4);


    companion object {

        private val lookup = HashMap<Int, BiblioObjectStatus>()

        init {
            for (s in values()) {
                lookup[s.code] = s
            }
        }

        fun getByValue(code: Int): BiblioObjectStatus? {
            return lookup[code]
        }
    }
}

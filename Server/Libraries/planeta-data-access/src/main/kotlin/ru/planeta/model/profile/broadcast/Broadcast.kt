package ru.planeta.model.profile.broadcast

import ru.planeta.model.advertising.AdvertisingDTO
import ru.planeta.model.enums.PermissionLevel
import ru.planeta.model.profile.ProfileObject
import ru.planeta.model.profile.broadcast.enums.BroadcastCategory
import ru.planeta.model.profile.broadcast.enums.BroadcastStatus
import ru.planeta.model.profile.broadcast.enums.BroadcastSubscriptionStatus

import java.util.Date

/**
 * Broadcast
 *
 * @author ds.kolyshev
 * Date: 16.03.12
 */
class Broadcast : ProfileObject() {
    var broadcastId: Long = 0
    var broadcastCategory: BroadcastCategory? = BroadcastCategory.CONCERTS
    var broadcastStatus: BroadcastStatus? = BroadcastStatus.NOT_STARTED
    var broadcastSubscriptionStatus: BroadcastSubscriptionStatus? = BroadcastSubscriptionStatus.NOT_STARTED
    var name: String? = null
    var description: String? = null
    var descriptionHtml: String? = null
    var imageUrl: String? = null
    var imageId: Long = 0
    var isImageCustomDescription: Boolean = false
    var imageDescription: String? = null
    var imageDescriptionHtml: String? = null
    var pausedImageUrl: String? = null
    var pausedImageId: Long = 0
    var isPausedCustomDescription: Boolean = false
    var pausedDescription: String? = null
    var pausedDescriptionHtml: String? = null
    var finishedImageUrl: String? = null
    var finishedImageId: Long = 0
    var isFinishedCustomDescription: Boolean = false
    var finishedDescription: String? = null
    var finishedDescriptionHtml: String? = null
    var closedImageUrl: String? = null
    var closedImageId: Long = 0
    var isClosedCustomDescription: Boolean = false
    var closedDescription: String? = null
    var closedDescriptionHtml: String? = null
    var timeAdded: Date? = null
    var timeUpdated: Date? = null
    var viewsCount: Int = 0
    var viewPermission = PermissionLevel.EVERYBODY
    var defaultStreamId: Long = 0
    var timeBegin: Date? = null
    var timeEnd: Date? = null
    var isAutoStart = false
    var isAutoSubscribe = false
    var isArchived = false
    var timeArchived: Date? = null
    var isVisibleOnDashboard: Boolean = false
    var isClosed: Boolean = false
    var advertismentBannerHtml: String? = null
    var advertising: AdvertisingDTO? = null
    var isChatEnabled = true
    var isAnonymousCanView = false
    var chatFakeUsersCount = 0

    internal var broadcastStatusCode: Int?
        get() = broadcastStatus?.code
        set(code) {
            broadcastStatus = BroadcastStatus.getByValue(code)
        }

    internal var broadcastCategoryCode: Int?
        get() = broadcastCategory?.code
        set(code) {
            broadcastCategory = BroadcastCategory.getByValue(code)
        }

    internal var broadcastSubscriptionStatusCode: Int?
        get() = broadcastSubscriptionStatus?.code
        set(code) {
            broadcastSubscriptionStatus = BroadcastSubscriptionStatus.getByValue(code)
        }

    internal var viewPermissionCode: Int
        get() = viewPermission.code
        set(code) {
            viewPermission = PermissionLevel.getByValue(code)
        }

    val millisecondsToStart: Long
        @Deprecated("")
        get() = timeBegin!!.time - Date().time

    val duration: Long
        @Deprecated("")
        get() = timeEnd!!.time - timeBegin!!.time
}

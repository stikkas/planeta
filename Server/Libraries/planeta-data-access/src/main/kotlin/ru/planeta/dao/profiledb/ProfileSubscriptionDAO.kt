package ru.planeta.dao.profiledb

import org.apache.ibatis.annotations.Mapper
import org.apache.ibatis.annotations.Param
import ru.planeta.model.enums.ProfileStatus
import ru.planeta.model.enums.ProfileType
import ru.planeta.model.profile.Profile
import ru.planeta.model.profile.ProfileRelationShort
import ru.planeta.model.profile.ProfileSubscriptionInfo
import java.util.*

@Mapper
interface ProfileSubscriptionDAO {

    fun getSubscriberList(@Param("profileId") profileId: Long, @Param("query") query: String?, @Param("isAdmin") isAdmin: Boolean?, @Param("lastViewSubscribersDate") lastViewSubscribersDate: Date?, @Param("offset") offset: Int, @Param("limit") limit: Int): List<ProfileSubscriptionInfo>

    fun getSubscriptionList(@Param("profileId") profileId: Long, @Param("query") query: String?, @Param("isAdmin") isAdmin: Boolean?, @Param("offset") offset: Int, @Param("limit") limit: Int): List<ProfileSubscriptionInfo>

    fun getProfileList(@Param("profileId") profileId: Long, @Param("query") query: String, @Param("isAdmin") isAdmin: Boolean?, @Param("profileType") profileType: ProfileType, @Param("profileStatuses") profileStatuses: EnumSet<ProfileStatus>, @Param("offset") offset: Int, @Param("limit") limit: Int): List<Profile>

    fun getSubscriberProfileList(@Param("profileId") profileId: Long, @Param("query") query: String?, @Param("isAdmin") isAdmin: Boolean?, @Param("profileType") profileType: ProfileType?, @Param("profileStatuses") profileStatuses: EnumSet<ProfileStatus>?, @Param("offset") offset: Int, @Param("limit") limit: Int): MutableList<Profile>

    fun selectUserOwnGroupsId(clientId: Long): MutableList<Long>

    fun selectSubscriptions(@Param("clientId") clientId: Long, @Param("ids") ids: List<Long>): List<Long>

    fun getProfileRelationShortList(@Param("profileId") profileId: Long, @Param("profileIdList") profileIdList: List<Long>): List<ProfileRelationShort>

    fun getSubscribersCount(profileId: Long): Long

    fun getNewSubscribersCount(@Param("profileId") profileId: Long, @Param("timeLastVisit") timeLastVisit: Date): Long

}

package ru.planeta.model.enums

import java.util.EnumSet
import java.util.HashMap

enum class ModerateStatus (override val code: Int) : Codable {
    NEW(1), APPROVED(2), FAIL(3);


    companion object {
        private val lookup = HashMap<Int, ModerateStatus>()

        init {
            for (s in EnumSet.allOf(ModerateStatus::class.java)) {
                lookup[s.code] = s
            }
        }

        fun getByValue(code: Int): ModerateStatus? {
            return lookup[code]
        }
    }

}

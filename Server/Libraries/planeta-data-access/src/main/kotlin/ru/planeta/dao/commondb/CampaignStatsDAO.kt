package ru.planeta.dao.commondb

import org.apache.ibatis.annotations.Mapper
import ru.planeta.model.stat.AggregateCampaignStatByTag

/**
 * User: m.shulepov
 */
@Mapper
interface CampaignStatsDAO {

    @Deprecated("")
    fun getAggregateCampaignStatBySponsorAlias(sponsorAlias: String): AggregateCampaignStatByTag
}

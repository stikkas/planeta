package ru.planeta.model.profile.broadcast.enums

import java.util.HashMap

/**
 * Broadcast status enumeration
 *
 * @author ds.kolyshev
 * Date: 16.03.12
 */
enum class BroadcastStatus private constructor(val code: Int) {
    NOT_STARTED(0), LIVE(1), PAUSED(2), FINISHED(3);


    companion object {

        private val lookup = HashMap<Int, BroadcastStatus>()

        init {
            for (s in values()) {
                lookup[s.code] = s
            }
        }

        fun getByValue(code: Int?): BroadcastStatus? {
            return lookup[code]
        }
    }
}

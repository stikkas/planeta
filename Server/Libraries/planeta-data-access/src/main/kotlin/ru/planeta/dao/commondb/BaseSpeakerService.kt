package ru.planeta.dao.commondb

import org.apache.ibatis.annotations.Param
import ru.planeta.model.commondb.Speaker
import ru.planeta.model.commondb.SpeakerExample
import java.util.*

class BaseSpeakerService(private var speakerDAO: SpeakerDAO) {
    val orderByClause: String
        get() = "speaker_id"

    //**************************************************************************************************
    //*********************************  Create ** Update ** Delete  *************************************
    //**************************************************************************************************
    fun insertSpeaker(speaker: Speaker) {


        speakerDAO.insertSelective(speaker)
    }

    fun insertSpeakerAllFields(speaker: Speaker) {


        speakerDAO.insert(speaker)
    }

    fun updateSpeaker(speaker: Speaker) {

        speakerDAO.updateByPrimaryKeySelective(speaker)
    }

    fun updateSpeakerAllFields(speaker: Speaker) {

        speakerDAO.updateByPrimaryKey(speaker)
    }

    fun insertOrUpdateSpeaker(speaker: Speaker) {

        val selectedSpeaker = speakerDAO.selectByPrimaryKey(speaker.speakerId)
        if (selectedSpeaker == null) {

            speakerDAO.insertSelective(speaker)
        } else {
            speakerDAO.updateByPrimaryKeySelective(speaker)
        }
    }

    fun insertOrUpdateSpeakerAllFields(speaker: Speaker) {

        val selectedSpeaker = speakerDAO.selectByPrimaryKey(speaker.speakerId)
        if (selectedSpeaker == null) {

            speakerDAO.insert(speaker)
        } else {
            speakerDAO.updateByPrimaryKey(speaker)
        }
    }

    fun deleteSpeaker(id: Long?) {
        speakerDAO.deleteByPrimaryKey(id)
    }

    //**************************************************************************************************
    //******************************************** speaker ***********************************************
    //************************************** SelectByPrimaryKey ******************************************
    //**************************************************************************************************
    fun selectSpeaker(speakerId: Long?): Speaker {
        return speakerDAO.selectByPrimaryKey(speakerId)
    }

    //**************************************************************************************************
    //******************************************** speaker ***********************************************
    //**************************************************************************************************
    private fun COMMENT_SECTION_Speaker() {

    }

    fun getSpeakerExample(@Param("offset") offset: Int, @Param("limit") limit: Int): SpeakerExample {
        val example = SpeakerExample()
        example.offset = offset
        example.limit = limit
        example.or()
        example.orderByClause = orderByClause
        return example
    }

    fun selectSpeakerCount(): Int {
        return speakerDAO.countByExample(getSpeakerExample(0, 0))
    }

    fun selectSpeakerList(@Param("offset") offset: Int, @Param("limit") limit: Int): List<Speaker> {
        return speakerDAO.selectByExample(getSpeakerExample(offset, limit))
    }

    fun selectSpeakerList(speakerIdList: List<Long>?): List<Speaker> {
        return if (speakerIdList == null || speakerIdList.isEmpty()) ArrayList() else speakerDAO.selectByIdList(speakerIdList)
    }
}

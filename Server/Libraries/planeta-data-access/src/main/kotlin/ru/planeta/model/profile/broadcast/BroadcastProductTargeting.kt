package ru.planeta.model.profile.broadcast

/**
 * Broadcast's backers limitings
 *
 * @author: ds.kolyshev
 * Date: 18.04.13
 */
class BroadcastProductTargeting {
    var broadcastId: Long = 0
    var productId: Long = 0

    constructor() {}

    constructor(broadcastId: Long, productId: Long) {
        this.broadcastId = broadcastId
        this.productId = productId
    }
}

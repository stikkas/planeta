package ru.planeta.dao.commondb

import org.apache.ibatis.annotations.Mapper
import ru.planeta.dto.PurchaseShareInfoDTO
import ru.planeta.dto.SaveShareInfoDTO

@Mapper
interface PurchaseShareInfoDAO {

    fun findPurchaseShareInfo(infoId: Long?): PurchaseShareInfoDTO // {
    //        return selectOne(Statements.PurchaseShareInfo.SELECT_PURCHASE_SHARE_INFO, getParameters("infoId", infoId));
    //    }

    fun savePurchaseShareInfo(shareInfo: SaveShareInfoDTO): Int  // {
    //        return insert(Statements.PurchaseShareInfo.INSERT_PURCHASE_SHARE_INFO, getParameters("shareInfo", shareInfo));
    //    }
}


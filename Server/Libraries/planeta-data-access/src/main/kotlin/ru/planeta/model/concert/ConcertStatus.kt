package ru.planeta.model.concert

import ru.planeta.model.enums.Codable

import java.util.EnumSet
import java.util.HashMap

/**
 * Created with IntelliJ IDEA.
 * User: michail
 * Date: 19.10.16
 * Time: 10:55
 */
enum class ConcertStatus private constructor(override val code: Int) : Codable {
    PAUSED(0),
    ACTIVE(1);


    companion object {

        private val lookup = HashMap<Int, ConcertStatus>()

        init {
            for (s in EnumSet.allOf(ConcertStatus::class.java)) {
                lookup[s.code] = s
            }
        }

        fun getByValue(code: Int): ConcertStatus? {
            return lookup[code]
        }
    }
}

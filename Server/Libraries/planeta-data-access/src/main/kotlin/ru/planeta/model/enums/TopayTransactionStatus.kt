package ru.planeta.model.enums

import java.util.HashMap

/**
 * Created by IntelliJ IDEA.
 * User: atropnikov
 * Date: 22.12.11
 * Time: 16:15
 */
enum class TopayTransactionStatus private constructor(val code: Int) {

    NEW(1),
    DONE(2),
    @Deprecated("")
    DECLINE(3),
    ERROR(4),
    CANCELED(5);

    val isNew: Boolean
        get() = this == NEW

    val isDone: Boolean
        get() = this == DONE

    val isError: Boolean
        get() = this == ERROR

    companion object {

        private val lookup = HashMap<Int, TopayTransactionStatus>()

        init {
            for (s in values()) {
                lookup[s.code] = s
            }
        }

        fun getByValue(code: Int): TopayTransactionStatus? {
            return lookup[code]
        }
    }
}

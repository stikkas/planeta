package ru.planeta.model.common.campaign.enums

import ru.planeta.model.enums.Codable

import java.util.*

/**
 * Created by IntelliJ IDEA.
 * User: atropnikov
 * Date: 21.03.12
 * Time: 12:42
 */
enum class CampaignStatus(//campaign is approved and waiting for start by job or by manager act [author cant make any changes]

        override val code: Int) : Codable {

    ALL(0),
    DRAFT(1), //campaign isn't validated
    NOT_STARTED(2), //campaign on moderation
    PAUSED(3), //campaign is paused
    ACTIVE(4), //campaign is active
    FINISHED(5), //campaign is finished
    DELETED(7), //campaign is deleted
    DECLINED(8), //campaign is declined
    PATCH(9), //campaign send to author to rework
    APPROVED(12);

    val isDraft: Boolean
        get() = this == DRAFT || this == NOT_STARTED || this == PATCH || this == APPROVED

    companion object {

        private val lookup = HashMap<Int, CampaignStatus>()

        init {
            for (s in values()) {
                lookup[s.code] = s
            }
        }

        fun getByValue(code: Int): CampaignStatus {
            return lookup[code] ?: ALL
        }

        fun getCodes(enums: EnumSet<CampaignStatus>): List<Int> {
            val codes = LinkedList<Int>()
            for (cs in enums) {
                codes.add(cs.code)
            }
            return codes
        }
    }

}

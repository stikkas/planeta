package ru.planeta.model.common.campaign

import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import ru.planeta.model.common.campaign.enums.ShareStatus

import java.math.BigDecimal
import java.util.Date

/**
 * Contains data about campaigns share
 *
 *
 * User: atropnikov
 * Date: 21.03.12
 * Time: 12:21
 */
@JsonIgnoreProperties(ignoreUnknown = true)
open class Share {

    var shareId: Long = 0
    var campaignId: Long? = 0
    var profileId: Long? = 0
    var name: String? = null
    var nameHtml: String? = null
    var description: String? = null
    var descriptionHtml: String? = null
    var imageId: Long = 0
    var imageUrl: String? = null
    var price: BigDecimal? = null
    var amount: Int = 0
    var purchaseCount: Int = 0
    var purchaseSum = BigDecimal.ZERO
    var timeAdded: Date? = null
    var rewardInstruction: String? = null
    var rewardInstructionHtml: String? = null
    var questionToBuyer: String? = null
    var questionToBuyerHtml: String? = null
    var donePurchaseCount: Int = 0
    var order: Int? = null
    var shareStatus: ShareStatus? = ShareStatus.DRAFT
    var estimatedDeliveryTime: Date? = null

    var shareStatusCode: Int
        @JsonIgnore
        get() = shareStatus!!.code
        set(shareStatusCode) {
            this.shareStatus = ShareStatus.getByValue(shareStatusCode)
        }

    constructor(o: Share) {
        this.shareId = o.shareId
        this.campaignId = o.campaignId
        this.profileId = o.profileId
        this.name = o.name
        this.nameHtml = o.nameHtml
        this.description = o.description
        this.descriptionHtml = o.descriptionHtml
        this.imageId = o.imageId
        this.imageUrl = o.imageUrl
        this.price = o.price
        this.amount = o.amount
        this.purchaseCount = o.purchaseCount
        this.purchaseSum = o.purchaseSum
        this.timeAdded = o.timeAdded
        this.rewardInstruction = o.rewardInstruction
        this.donePurchaseCount = o.donePurchaseCount
        this.questionToBuyer = o.questionToBuyer
        this.order = o.order
        this.shareStatus = o.shareStatus
        this.estimatedDeliveryTime = o.estimatedDeliveryTime
    }

    constructor() {}

    override fun hashCode(): Int {
        var hash = 5
        hash = 53 * hash + (shareId xor shareId.ushr(32)).toInt()
        return hash
    }

    override fun equals(obj: Any?): Boolean {
        if (this === obj) {
            return true
        }
        if (obj == null) {
            return false
        }
        return if (obj is Share) shareId == obj.shareId else false
    }

}



package ru.planeta.dao.commondb

import org.apache.ibatis.annotations.Mapper
import org.apache.ibatis.annotations.Param
import ru.planeta.dao.profiledb.ArrayListWithCount
import ru.planeta.model.common.Contractor
import ru.planeta.model.common.ContractorWithCampaigns

/**
 * User: a.savanovich
 * Date: 22.11.13
 * Time: 13:51
 */
@Mapper
interface ContractorDAO {
    fun select(contractorId: Long): Contractor

    fun select(name: String): Contractor

    fun insert(contractor: Contractor)

    fun selectList(offset: Int, @Param("limit") limit: Int): List<Contractor>

    fun selectList(profileId: Long, @Param("offset") offset: Int, @Param("limit") limit: Int): List<Contractor>

    fun update(contractor: Contractor)

    fun delete(contractorId: Long)

    fun insertRelation(contractorId: Long, @Param("campaignId") campaignId: Long)

    fun selectByCampaignId(campaignId: Long): Contractor

    fun updateRelation(contractorId: Long, @Param("campaignId") campaignId: Long)

    fun deleteRelation(campaignId: Long)

    fun deleteAllRelationByContractorId(contractorId: Long)

    fun selectContractorsSearch(searchString: String, @Param("offset") offset: Int, @Param("limit") limit: Int): ArrayListWithCount<ContractorWithCampaigns>

    fun selectContractorsSearchTotalCount(params: Map<*, *>): Int

    fun selectByInn(inn: String): Contractor
}

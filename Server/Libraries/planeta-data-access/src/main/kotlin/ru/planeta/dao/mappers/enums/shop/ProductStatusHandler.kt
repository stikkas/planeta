package ru.planeta.dao.mappers.enums.shop

import org.apache.ibatis.type.JdbcType
import org.apache.ibatis.type.TypeHandler
import ru.planeta.model.shop.enums.ProductStatus

import java.sql.CallableStatement
import java.sql.PreparedStatement
import java.sql.ResultSet
import java.sql.SQLException

/**
 * Created by IntelliJ IDEA.
 * User: Andrew
 * Date: 13.09.12
 * Time: 23:53
 */
class ProductStatusHandler : TypeHandler<ProductStatus> {

    @Throws(SQLException::class)
    override fun getResult(resultSet: ResultSet, s: String): ProductStatus? {
        return ProductStatus.getByValue(resultSet.getInt(s))
    }

    @Throws(SQLException::class)
    override fun setParameter(preparedStatement: PreparedStatement, i: Int, o: ProductStatus, jdbcType: JdbcType) {
        preparedStatement.setInt(i, o.code)
    }


    @Throws(SQLException::class)
    override fun getResult(callableStatement: CallableStatement, i: Int): ProductStatus {
        throw NoSuchMethodError("not implemented for CallableStatement")
    }

    @Throws(SQLException::class)
    override fun getResult(rs: ResultSet, columnIndex: Int): ProductStatus {
        throw NoSuchMethodError("not implemented for CallableStatement")
    }
}


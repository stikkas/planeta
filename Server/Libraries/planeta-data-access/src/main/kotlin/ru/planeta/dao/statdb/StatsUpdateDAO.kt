package ru.planeta.dao.statdb

/**
 * @author a.savanovich
 */
interface StatsUpdateDAO {

    fun updateCommentsStats()
}

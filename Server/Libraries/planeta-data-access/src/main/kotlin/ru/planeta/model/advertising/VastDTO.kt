package ru.planeta.model.advertising

/**
 * Created with IntelliJ IDEA.
 * Date: 20.01.14
 * Time: 13:44
 */
//todo replace all enumeration string with enum
class VastDTO {
    var adName: String? = null
    var videoUrl: String? = null
    var adId: Long = -1
    var redirectUrl: String? = null
    var mediaType: String? = null
    var duration: Long = 0
    var width: Int = 0
    var height: Int = 0
    var deliveryType = "progressive"
    var description: String? = null
    var isSkippable: Boolean = false
}

package ru.planeta.dao.trashcan

import org.apache.ibatis.annotations.Mapper
import org.apache.ibatis.annotations.Param
import ru.planeta.model.common.IviStat
import java.util.*

@Mapper
interface IviDao {
    fun insert(orderId: Long)
    fun getIviStats(@Param("start") start: Date?, @Param("finish") finish: Date?, @Param("offset") offset: Long, @Param("limit") limit: Int): List<IviStat>
}

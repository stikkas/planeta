package ru.planeta.model.profile.broadcast

import ru.planeta.model.profile.ProfileObject

/**
 * Broadcast subscription
 *
 * @author: ds.kolyshev
 * Date: 15.05.13
 */
class BroadcastSubscription : ProfileObject() {
    var broadcastId: Long = 0
    var subscriberProfileId: Long = 0
    var isOnEmail: Boolean = false
    var isOnSite: Boolean = false
}

package ru.planeta.dao.bibliodb

/*
 * Created by Alexey on 06.06.2016.
 */

import org.apache.ibatis.annotations.Mapper
import org.apache.ibatis.annotations.Param
import ru.planeta.model.bibliodb.PublishingHouse

@Mapper
interface PublishingHouseDAO {

    fun getPublishingHouses(@Param("searchRow") searchRow: String, @Param("offset") offset: Int,
                            @Param("limit") limit: Int): List<PublishingHouse>

    fun getPublishingHouseById(publishingHouseId: Long): PublishingHouse

    fun insertPublishingHouse(publishingHouse: PublishingHouse)

    fun updatePublishingHouse(publishingHouse: PublishingHouse)

    fun selectCountPublishingHouses(searchRow: String): Long
}

package ru.planeta.dao.commondb

import org.apache.ibatis.annotations.Mapper
import org.apache.ibatis.annotations.Param
import ru.planeta.model.common.campaign.ShareForSearch

@Mapper
interface ShareSearchDAO {
    fun select(@Param("shareIds") shareIds: Collection<Long>): List<ShareForSearch>

    fun selectSharesForWelcome(): List<ShareForSearch>
}

package ru.planeta.dao.commondb

import org.apache.ibatis.annotations.Mapper
import org.apache.ibatis.annotations.Param
import ru.planeta.model.common.TopayTransaction
import ru.planeta.model.enums.ProjectType
import ru.planeta.model.enums.TopayTransactionStatus

import java.math.BigDecimal
import java.util.Date

/**
 * Created by IntelliJ IDEA.
 * User: atropnikov
 * Date: 22.12.11
 * Time: 16:20
 */
@Mapper
interface TopayTransactionDAO {

    /**
     * Selects payment transaction by specified identifier.
     *
     * @param transactionId transaction identifier;
     * @return payment transaction `TopayTransaction`, or `null`, if transaction with identifier not exists.
     */
    fun select(transactionId: Long): TopayTransaction

    /**
     * Inserts presented payment transaction.
     *
     * @param topayTransaction transaction to insert;
     */
    fun insert(topayTransaction: TopayTransaction): TopayTransaction

    /**
     * Updates specified payment transaction;
     *
     * @param topayTransaction transaction to update;
     */
    fun update(topayTransaction: TopayTransaction): TopayTransaction

    /**
     * Deletes payment transaction with specified identifier;
     *
     * @param transactionId transaction identifier;
     */
    fun delete(transactionId: Long)

    /**
     * Selects payment transactions for profileId, optionally filtered by specified params
     */
    fun selectTopayTransactions(@Param("query") query: String, @Param("status") status: TopayTransactionStatus, @Param("paymentMethodId") paymentMethodId: Long?, @Param("paymentProviderId") paymentProviderId: Long?, @Param("projectType") projectType: ProjectType, @Param("dateFrom") dateFrom: Date, @Param("dateTo") dateTo: Date, @Param("orderOrTransactionId") orderOrTransactionId: Long, @Param("offset") offset: Int, @Param("limit") limit: Int): List<TopayTransaction>

    fun selectOrderTopayTransactions(orderId: Long): List<TopayTransaction>

    fun selectForPooling(@Param("status") status: TopayTransactionStatus, @Param("offset") offset: Int, @Param("limit") limit: Int): List<TopayTransaction>

    fun selectMonthlyPaidSum(@Param("profileId") profileId: Long, @Param("paymentMethodId") paymentMethodId: Long, @Param("paymentToolCode") paymentToolCode: String): BigDecimal

    fun selectPaymentByExternalId(@Param("paymentProviderId") paymentProviderId: Long, @Param("externalPaymentId") externalPaymentId: String, @Param("isRecurrent") isRecurrent: Boolean): TopayTransaction

    /***
     * возвращает предыдущую плохую транзакцию для того же пользователя
     * в интервале, который задан через параметр payment.errors.two.fail.payments (минуты)
     *
     */
    fun getPrevFailTransaction(transactionId: Long): TopayTransaction
}

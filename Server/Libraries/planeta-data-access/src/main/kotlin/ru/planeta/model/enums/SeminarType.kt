package ru.planeta.model.enums

import java.util.EnumSet
import java.util.HashMap

enum class SeminarType private constructor(override val code: Int, override val descriptionMessageCode: String, override val fullDescriptionMessageCode: String) : ReadableEnum {
    SOLO(1, "enum.seminar.type.solo.description", "enum.seminar.type.solo.full.description"),
    SOLOSIMPLE(2, "enum.seminar.type.solosimple.description", "enum.seminar.type.solosimple.full.description"),
    PAIR(3, "enum.seminar.type.pair.description", "enum.seminar.type.pair.full.description"),
    WEBINAR(4, "enum.seminar.type.webinar.description", "enum.seminar.type.webinar.full.description"),
    COMPANY(5, "enum.seminar.type.company.description", "enum.seminar.type.company.full.description"),
    EXTERNAL(6, "enum.seminar.type.external.description", "enum.seminar.type.external.full.description"),
    DELETED(7, "enum.seminar.type.deleted.description", "enum.seminar.type.deleted.full.description");


    companion object {

        private val lookup = HashMap<Int, SeminarType>()


        fun parse(s: String?): SeminarType? {
            if (s == null || s.isEmpty()) return null
            try {
                val i = Integer.parseInt(s)
                if (i < values().size) return values()[i]
            } catch (e: NumberFormatException) {
            }

            return null
        }

        fun getByValue(code: Int): SeminarType? {
            if (lookup.size == 0) {
                for (s in EnumSet.allOf(SeminarType::class.java)) {
                    lookup[s.code] = s
                }
            }
            return lookup[code]
        }
    }
}

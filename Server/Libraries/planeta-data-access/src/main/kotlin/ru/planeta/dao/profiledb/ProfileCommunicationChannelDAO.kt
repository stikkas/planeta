package ru.planeta.dao.profiledb

import org.apache.ibatis.annotations.Mapper
import org.apache.ibatis.annotations.Param
import ru.planeta.model.profile.ProfileCommunicationChannel

/**
 * Interface ProfileCommunicationChannelDAO
 *
 * @author a.tropnikov
 */
@Mapper
interface ProfileCommunicationChannelDAO {

    fun selectProfileCommunicationChannels(profileId: Long): List<ProfileCommunicationChannel>

    fun select(@Param("profileId") profileId: Long, @Param("channelId") channelId: Int): ProfileCommunicationChannel

    fun insert(channel: ProfileCommunicationChannel)

    fun update(channel: ProfileCommunicationChannel)

    fun delete(@Param("profileId") profileId: Long, @Param("channelId") channelId: Int)

    fun deleteAll(profileId: Long)
}

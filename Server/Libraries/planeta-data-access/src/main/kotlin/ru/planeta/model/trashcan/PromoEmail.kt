package ru.planeta.model.trashcan

import java.util.Date

class PromoEmail {
    var emailId: Long = 0
    var promoId: Long = 0
    var promoCodeId: Long = 0
    var email: String? = null
    var timeSent: Date? = null
    var orderId: Long = 0

    constructor() {}

    constructor(promoId: Long, promoCodeId: Long, email: String, orderId: Long) {
        this.promoId = promoId
        this.promoCodeId = promoCodeId
        this.email = email
        this.orderId = orderId
    }
}

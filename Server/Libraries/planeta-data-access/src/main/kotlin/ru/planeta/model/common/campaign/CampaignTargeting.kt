package ru.planeta.model.common.campaign

class CampaignTargeting {
    var campaignId: Long = 0
    var vkTargetingId: String? = null
    var fbTargetingId: String? = null

    constructor() {}

    constructor(campaignId: Long) {
        this.campaignId = campaignId
    }
}

package ru.planeta.dao.profiledb

import org.apache.ibatis.annotations.Mapper
import org.apache.ibatis.annotations.Param
import ru.planeta.model.profile.User

/**
 * Interface UserDAO
 *
 * @author a.tropnikov
 */
@Mapper
interface UserDAO {

    fun select(profileId: Long): User

    fun insert(user: User)

    fun update(user: User)

    fun addFriendsCount(@Param("profileId") profileId: Long, @Param("count") count: Int)

    fun addUsersRequestInCount(@Param("profileId") profileId: Long, @Param("count") count: Int)

    fun addUsersRequestOutCount(@Param("profileId") profileId: Long, @Param("count") count: Int)

    fun delete(profileId: Long)
}

package ru.planeta.model.common

import ru.planeta.model.enums.ContractorPositionType
import ru.planeta.model.enums.ContractorType

import java.util.Date

/**
 * User: a.savanovich
 * Date: 20.11.13
 * Time: 13:09
 */
open class Contractor {
    var contractorId: Long = 0
    var name: String? = null
    var contractNumber: String? = null
    var countryId: Long = 0
    var countryNameRus: String? = null
    var cityId: Long = 0
    var cityNameRus: String? = null
    var type: ContractorType? = ContractorType.INDIVIDUAL
    var ogrn: String? = null
    var inn: String? = null
    var passportNumber: String? = null
    var otherDetails: String? = null
    var timeAdded: Date? = null
    var timeUpdated: Date? = null
    var phone: String? = null
    var legalAddress: String? = null
    var actualAddress: String? = null
    var authority: String? = null
    var issueDate: Date? = null
    var checkingAccount: String? = null
    var beneficiaryBank: String? = null
    var correspondingAccount: String? = null
    var bic: String? = null
    var unit: String? = null
    var birthDate: Date? = null
    var kpp: String? = null
    var responsiblePerson: String? = null
    var scanPassportUrl: String? = null
    var initialsWithLastName: String? = null
    var position: ContractorPositionType? = ContractorPositionType.OTHER
    var customPosition: String? = null

    var typeCode: Int
        get() = type!!.code
        set(typeCode) {
            this.type = ContractorType.getByValue(typeCode)
        }

    var positionCode: Int
        get() = position!!.code
        set(positionCode) {
            this.position = ContractorPositionType.getByValue(positionCode)
        }
}

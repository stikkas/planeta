package ru.planeta.model.bibliodb

import ru.planeta.model.bibliodb.enums.BiblioObjectStatus
import ru.planeta.model.bibliodb.enums.LibraryType

import java.util.Date
import java.util.EnumSet

/**
 * Created with IntelliJ IDEA.
 * User: michail
 * Date: 28.11.2016
 * Time: 19:35
 */
class BookFilter {
    var searchStr: String? = null
    var statuses: EnumSet<BiblioObjectStatus>? = null
    var tagId: Int? = null
    var dateFrom: Date? = null
    var dateTo: Date? = null

    var limit: Int = 0
    var offset: Int = 0

    fun setStatus(status: BiblioObjectStatus?) {
        if (status != null) {
            this.statuses = EnumSet.of(status)
        } else {
            this.statuses = null
        }
    }
}

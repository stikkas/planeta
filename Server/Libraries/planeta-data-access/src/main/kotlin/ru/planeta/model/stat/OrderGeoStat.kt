package ru.planeta.model.stat

import ru.planeta.commons.web.IpUtils
import java.util.Date

/**
 * Created with IntelliJ IDEA.
 * User: michail
 * Date: 27.06.16
 * Time: 12:49
 */
class OrderGeoStat {
    var orderId: Long = 0
    var ipAddress: Long? = null
    var countryId: Int? = null
    var cityId: Int? = null
    var timeUpdated: Date? = null
    var referrerUrl: String? = null

    var stringIpAddress: String
        get() = IpUtils.longToIp(ipAddress)
        set(ipAddress) {
            this.ipAddress = IpUtils.ipToLong(ipAddress)
        }

    constructor() {}

    constructor(orderId: Long, ipAddress: String, referrerUrl: String) {
        this.orderId = orderId
        this.ipAddress = IpUtils.ipToLong(ipAddress)
        this.referrerUrl = referrerUrl
    }
}

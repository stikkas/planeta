package ru.planeta.model.concert

import java.math.BigDecimal
import java.util.Date

/**
 * Created with IntelliJ IDEA.
 * User: michail
 * Date: 14.10.16
 * Time: 14:15
 */
class Place {
    var placeId: Long = 0
    var externalUnusedId: Long = 0  // MS place id. unused according to documentation, but we can use it for sorting
    var externalPlaceId: Long? = null   // external place id for purchasing on MS. Not the same that externalUnusedPlaceId!
    // if not exists, must be null, not zero! (according to the concerdb.place table constraint)
    var name: String? = null
    var externalConcertId: Long = 0
    var sectionId: Long = 0         // reference to Section object
    var rowId: Long = 0             // reference to Section object
    var position: Int = 0           // place nubmer
    // <sectionId, rowId, position> must be unique
    var price: BigDecimal? = null       // price
    var x: Int = 0                  // coordinates on MS map
    var y: Int = 0
    var ticketPdfUrl: String? = null
    var ticketCode: String? = null
    var state: PlaceState? = null
    var timeAdded: Date? = null
    var timeUpdated: Date? = null

    var isDeleted = false

    var stateCode: Int?
        get() = state?.code
        set(code) {
            this.state = PlaceState.getByValue(code)
        }
}

package ru.planeta.dao.handlers

import org.apache.ibatis.type.BaseTypeHandler
import org.apache.ibatis.type.JdbcType
import org.apache.ibatis.type.MappedTypes
import ru.planeta.model.common.campaign.enums.CampaignTargetStatus
import java.sql.CallableStatement
import java.sql.PreparedStatement
import java.sql.ResultSet

@MappedTypes(CampaignTargetStatus::class)
class CampaignStatusTargetHandler : BaseTypeHandler<CampaignTargetStatus>() {
    override fun getNullableResult(p0: ResultSet, p1: String?): CampaignTargetStatus {
        return CampaignTargetStatus.getByValue(p0.getInt(p1))
    }

    override fun getNullableResult(p0: ResultSet, p1: Int): CampaignTargetStatus {
        return CampaignTargetStatus.getByValue(p0.getInt(p1))
    }

    override fun getNullableResult(p0: CallableStatement, p1: Int): CampaignTargetStatus {
        return CampaignTargetStatus.getByValue(p0.getInt(p1))
    }

    override fun setNonNullParameter(p0: PreparedStatement?, p1: Int, p2: CampaignTargetStatus?, p3: JdbcType?) {
        throw NotImplementedError("not used")
    }
}


package ru.planeta.model.stat

import java.util.Date

/**
 * Date: 12.10.2015
 * Time: 16:39
 */
class AggregateCampaignStatByTag {
    var totalCount: Int = 0
    var collectedAmount: Int = 0
    var minimalTimeStart: Date? = null
}

package ru.planeta.model.common.delivery

import org.apache.ibatis.type.JdbcType
import org.apache.ibatis.type.TypeHandler

import java.sql.CallableStatement
import java.sql.PreparedStatement
import java.sql.ResultSet
import java.sql.SQLException

/**
 * @author Andrew.Arefyev@gmail.com
 * 24.06.2014 11:43
 */
class SubjectTypeHandler : TypeHandler<SubjectType> {

    @Throws(SQLException::class)
    override fun setParameter(ps: PreparedStatement, i: Int, parameter: SubjectType, jdbcType: JdbcType) {
        ps.setInt(i, parameter.code)
    }

    @Throws(SQLException::class)
    override fun getResult(rs: ResultSet, columnName: String): SubjectType? {
        return SubjectType.getByValue(rs.getInt(columnName))
    }

    @Throws(SQLException::class)
    override fun getResult(cs: CallableStatement, columnIndex: Int): SubjectType? {
        return SubjectType.getByValue(cs.getInt(columnIndex))
    }

    @Throws(SQLException::class)
    override fun getResult(rs: ResultSet, columnIndex: Int): SubjectType? {
        return SubjectType.getByValue(rs.getInt(columnIndex))
    }

}

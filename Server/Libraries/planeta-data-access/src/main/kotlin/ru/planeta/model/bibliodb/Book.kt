package ru.planeta.model.bibliodb

import java.io.Serializable
import ru.planeta.model.bibliodb.enums.BiblioObjectStatus
import java.math.BigDecimal
import java.util.ArrayList
import java.util.Date

/*
 * Created by Alexey on 07.04.2016.
 */
open class Book : Serializable {

    /**
     * Идентификатор издания
     */
    var bookId: Long = 0
    /**
     * Название издания
     */
    var title: String? = null
    /**
     * Описание издания
     */
    var description: String? = null
    /**
     * Стоимость одного номера издания
     */
    var price = BigDecimal.ZERO
    /**
     * Стоимость одного номера издания с учетом скидок
     */
    var newPrice = BigDecimal.ZERO
    /**
     * Путь к картинки обложки издания
     */
    var imageUrl: String? = null
    /**
     * Переодичность издания
     */
    var periodicity: String? = null
    /**
     * Статус подписки на издание
     */
    var status = BiblioObjectStatus.PAUSED
    /**
     * Бонус для мецаната
     */
    var bonus: String? = null
    /**
     * Дата изменения стоимости номера издания
     */
    var changePriceDate: Date? = null
    /**
     * Комментарий
     */
    var comment: String? = null

    /**
     * Идентификатор издательства
     */
    var publishingHouseId: Long = 0

    /**
     * Название издательства
     */
    var publishingHouseName: String? = null

    /**
     * Показывать или нет на главной
     */
    var index: Int = 0

    /**
     * Контактное лицо (из заявки)
     */
    var contact: String? = null

    /**
     * Контактный емейл (из заявки)
     */
    var email: String? = null

    /**
     * Сайт издания (из заявки)
     */
    var site: String? = null

    var timeAdded: Date? = null

    /**
     * Категории издания
     */
    var tags = mutableListOf<BookTag>()

    var tagIds: List<Long> = ArrayList()

//    fun setTags(tags: List<BookTag>) {
//        getTags().clear()
//        for (tag in tags) addTag(tag)
//    }

    fun addTag(tag: BookTag?) {
        if (tag != null) {
            tags.add(tag)
        }
    }

//    fun getTags(): MutableList<BookTag> {
//        if (tags == null) {
//            tags = ArrayList()
//        }
//        return tags
//    }

    override fun hashCode(): Int {
        var hash = 3
        hash = 17 * hash + (this.bookId xor this.bookId.ushr(32)).toInt()
        return hash
    }

    override fun equals(obj: Any?): Boolean {
        if (this === obj) {
            return true
        }
        if (obj == null) {
            return false
        }
        if (javaClass != obj.javaClass) {
            return false
        }
        val other = obj as Book?
        return this.bookId == other!!.bookId
    }
}

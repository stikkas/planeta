package ru.planeta.dao.commondb

import org.apache.ibatis.annotations.Mapper
import org.apache.ibatis.annotations.Param
import ru.planeta.model.commondb.Seminars
import ru.planeta.model.commondb.SeminarsExample

@Mapper
interface SeminarsDAO {
    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table commondb.seminars
     *
     * @mbggenerated Tue May 29 12:53:11 MSK 2018
     */
    fun countByExample(example: SeminarsExample): Int

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table commondb.seminars
     *
     * @mbggenerated Tue May 29 12:53:11 MSK 2018
     */
    fun deleteByExample(example: SeminarsExample): Int

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table commondb.seminars
     *
     * @mbggenerated Tue May 29 12:53:11 MSK 2018
     */
    fun deleteByPrimaryKey(seminarId: Long?): Int

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table commondb.seminars
     *
     * @mbggenerated Tue May 29 12:53:11 MSK 2018
     */
    fun insert(record: Seminars): Int

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table commondb.seminars
     *
     * @mbggenerated Tue May 29 12:53:11 MSK 2018
     */
    fun insertSelective(record: Seminars): Int

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table commondb.seminars
     *
     * @mbggenerated Tue May 29 12:53:11 MSK 2018
     */
    fun selectByExample(example: SeminarsExample): List<Seminars>

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table commondb.seminars
     *
     * @mbggenerated Tue May 29 12:53:11 MSK 2018
     */
    fun selectByPrimaryKey(seminarId: Long?): Seminars

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table commondb.seminars
     *
     * @mbggenerated Tue May 29 12:53:11 MSK 2018
     */
    fun updateByExampleSelective(@Param("record") record: Seminars, @Param("example") example: SeminarsExample): Int

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table commondb.seminars
     *
     * @mbggenerated Tue May 29 12:53:11 MSK 2018
     */
    fun updateByExample(@Param("record") record: Seminars, @Param("example") example: SeminarsExample): Int

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table commondb.seminars
     *
     * @mbggenerated Tue May 29 12:53:11 MSK 2018
     */
    fun updateByPrimaryKeySelective(record: Seminars): Int

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table commondb.seminars
     *
     * @mbggenerated Tue May 29 12:53:11 MSK 2018
     */
    fun updateByPrimaryKey(record: Seminars): Int

    fun selectByIdList(seminarsIdList: List<Long>): List<Seminars>
}

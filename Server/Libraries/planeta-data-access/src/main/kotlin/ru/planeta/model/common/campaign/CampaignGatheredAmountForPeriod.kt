package ru.planeta.model.common.campaign

import java.util.Date

/**
 * @author m.shulepov
 * Date: 14.06.13
 */
class CampaignGatheredAmountForPeriod {

    var timePeriod: Date? = null
    var amount: Double = 0.toDouble()

}

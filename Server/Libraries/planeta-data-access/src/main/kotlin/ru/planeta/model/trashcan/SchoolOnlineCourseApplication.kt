package ru.planeta.model.trashcan

import java.util.Date

class SchoolOnlineCourseApplication {
    var applicationId: Long = 0
    var name: String? = null
    var email: String? = null
    var isInviteSent = false
    var timeAdded: Date? = null
}

package ru.planeta.model.common

/**
 * Planeta manager relation, which contains relation between [PlanetaManager] and
 * some type of object(CampaignCategoryType/GroupCategory)
 *
 * @author m.shulepov
 */
class PlanetaManagerRelation<T> {

    var category: T? = null
    var manager: PlanetaManager? = null
}

package ru.planeta.model.stat


import java.util.Date

/**
 * @author p.vyazankin
 * @since 11/28/12 12:11 PM
 */
class StatStaticNode {

    var domain: String? = null
    var freeSpaceKB: Long = 0
    var timeUpdated: Date? = null

    override fun equals(o: Any?): Boolean {
        if (this === o) return true
        if (o == null || javaClass != o.javaClass) return false

        val that = o as StatStaticNode?

        if (freeSpaceKB != that!!.freeSpaceKB) return false
        if (if (domain != null) domain != that.domain else that.domain != null) return false
        return if (if (timeUpdated != null) timeUpdated != that.timeUpdated else that.timeUpdated != null) false else true

    }

    override fun hashCode(): Int {
        var result = if (domain != null) domain!!.hashCode() else 0
        result = 31 * result + (freeSpaceKB xor freeSpaceKB.ushr(32)).toInt()
        result = 31 * result + if (timeUpdated != null) timeUpdated!!.hashCode() else 0
        return result
    }

    override fun toString(): String {
        return "StatStaticNode{" +
                "domain='" + domain + '\''.toString() +
                ", freeSpaceKB=" + freeSpaceKB +
                ", timeUpdated=" + timeUpdated +
                '}'.toString()
    }
}

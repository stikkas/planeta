package ru.planeta.dao

import org.apache.ibatis.annotations.Mapper
import ru.planeta.dto.ProfileSitesDTO

@Mapper
interface ProfileSitesDAO {

    fun insert(record: ProfileSitesDTO): Int

    fun update(record: ProfileSitesDTO): Int

    fun selectByProfileId(profileId: Long): ProfileSitesDTO
}


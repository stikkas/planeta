package ru.planeta.model.trashcan

import java.util.HashMap

enum class QuizQuestionType private constructor(val code: Int) {
    TEXT(1), NUMBER(2), CHECKBOX_SIMPLE(3), CHECKBOX_MULTI(4), RADIO_NUMBER_RANGE(5), RADIO_OPTIONS(6), TEXT_BIG(7);


    companion object {

        private val lookup = HashMap<Int, QuizQuestionType>()

        init {
            for (s in values()) {
                lookup[s.code] = s
            }
        }

        fun getByValue(code: Int): QuizQuestionType {
            return lookup[code] ?: TEXT_BIG
        }
    }
}

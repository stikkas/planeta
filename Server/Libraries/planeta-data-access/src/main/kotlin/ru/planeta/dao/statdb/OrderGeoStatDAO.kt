package ru.planeta.dao.statdb

import org.apache.ibatis.annotations.Mapper
import org.apache.ibatis.annotations.Param
import ru.planeta.model.stat.OrderGeoStat
import java.util.*

/**
 * Created with IntelliJ IDEA.
 * User: michail
 * Date: 27.06.16
 * Time: 12:50
 */
@Mapper
interface OrderGeoStatDAO {
    fun insert(orderGeo: OrderGeoStat)

    fun update(orderGeo: OrderGeoStat)

    fun selectByOrderId(orderId: Long): OrderGeoStat

    fun selectUnresolved(@Param("dateFrom") dateFrom: Date,
                         @Param("offset") offset: Int, @Param("limit") limit: Int): List<OrderGeoStat>
}

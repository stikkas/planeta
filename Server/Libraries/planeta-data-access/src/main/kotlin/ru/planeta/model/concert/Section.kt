package ru.planeta.model.concert

/**
 * Created with IntelliJ IDEA.
 * User: michail
 * Date: 14.10.16
 * Time: 14:26
 */
class Section {
    var sectionId: Long = 0
    var externalSectionId: Long = 0
    var externalParentId: Long = 0
    var externalConcertId: Long = 0
    var hallId: Long = 0
    var name: String? = null

    var isDeleted = false
}

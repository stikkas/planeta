package ru.planeta.dao.commondb

import org.apache.ibatis.annotations.Mapper
import org.apache.ibatis.annotations.Param
import java.math.BigDecimal

/**
 * Author: Stepanov
 * Date: 22.03.13
 * Time: 17:04
 */
@Mapper
interface SuperAdminUtilsDAO {

    fun deleteProfile(profileId: Long)

    fun decreaseBalance(@Param("profileId") profileId: Long, @Param("amount") amount: BigDecimal)

    fun increaseBalance(@Param("profileId") profileId: Long, @Param("amount") amount: BigDecimal)

    fun deleteUserComments(@Param("profileId") profileId: Long, @Param("daysCount") daysCount: Int)

    fun deleteUserPosts(@Param("profileId") profileId: Long, @Param("daysCount") daysCount: Int)
}

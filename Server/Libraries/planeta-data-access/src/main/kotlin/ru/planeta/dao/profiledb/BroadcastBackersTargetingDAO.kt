package ru.planeta.dao.profiledb

import org.apache.ibatis.annotations.Mapper
import org.apache.ibatis.annotations.Param
import ru.planeta.model.profile.broadcast.BroadcastBackersTargeting

/**
 * Broadcast Backers Targeting DAO
 *
 * @author ds.kolyshev
 * Date: 18.04.13
 */
@Mapper
interface BroadcastBackersTargetingDAO {

    fun insert(broadcastBackersTargeting: BroadcastBackersTargeting)

    fun delete(@Param("broadcastId") broadcastId: Long, @Param("campaignId") campaignId: Long)

    fun selectBroadcastBackersTargetings(broadcastId: Long): List<BroadcastBackersTargeting>
}

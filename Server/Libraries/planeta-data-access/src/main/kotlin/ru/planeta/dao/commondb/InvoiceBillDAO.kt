package ru.planeta.dao.commondb

import org.apache.ibatis.annotations.Mapper
import org.apache.ibatis.annotations.Param
import ru.planeta.model.common.InvoiceBill

/**
 *
 * User: Serge Blagodatskih<stikkas17></stikkas17>@gmail.com><br></br>
 * Date: 16.02.16<br></br>
 * Time: 15:59
 */
@Mapper
interface InvoiceBillDAO {

    fun select(@Param("buyerId") buyerId: Long, @Param("transactionId") id: Long): InvoiceBill

    fun select(@Param("buyerId") buyerId: Long, @Param("query") query: String?,
               @Param("offset") offset: Int, @Param("limit") limit: Int): List<InvoiceBill>
}


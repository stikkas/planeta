package ru.planeta.model.bibliodb

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.databind.annotation.JsonDeserialize
import java.io.Serializable
import java.math.BigDecimal
import ru.planeta.model.common.InvestingOrderInfo
import ru.planeta.model.enums.ContractorType
import ru.planeta.model.enums.ContractorTypeDeserializer

@JsonIgnoreProperties(ignoreUnknown = true)
class BiblioPurchase : Serializable {

    var email: String? = null
    var totalSum: BigDecimal? = null
    var price: BigDecimal? = null
    var isFromBalance: Boolean = false
    var isPlanetaPurchase: Boolean = false
    var paymentMethodId: Long? = null
    var payeerPhone: String? = null
    var investInfo: InvestingOrderInfo? = null

    @JsonDeserialize(using = ContractorTypeDeserializer::class)
    var customerType: ContractorType? = null
    var fio: String? = null
}

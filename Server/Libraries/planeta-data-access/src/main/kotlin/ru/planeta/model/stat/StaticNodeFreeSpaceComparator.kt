package ru.planeta.model.stat

import java.io.Serializable
import java.util.Comparator

/**
 * @author p.vyazankin
 * @since 11/23/12 4:54 PM
 */
class StaticNodeFreeSpaceComparator(private val isAsc: Boolean) : Comparator<StatStaticNode>, Serializable {

    override fun compare(o1: StatStaticNode, o2: StatStaticNode): Int {
        val result = if (isAsc) o2.freeSpaceKB - o1.freeSpaceKB else o1.freeSpaceKB - o2.freeSpaceKB
        if (result < 0) {
            return -1
        }
        return if (result > 0) {
            1
        } else {
            0
        }
    }
}

package ru.planeta.model.enums

import java.util.EnumSet
import java.util.HashMap

/**
 * User: sshendyapin
 * Date: 19.07.12
 * Time: 16:05
 */
enum class PermissionType(val code: Int) {

    // what else?
    VIEW(1),
    WRITE(10), COMMENT(20), VOTE(30), ADD(40);


    companion object {

        private val lookup = HashMap<Int, PermissionType>()

        init {
            for (s in EnumSet.allOf(PermissionType::class.java)) {
                lookup[s.code] = s
            }
        }

        fun getByValue(code: Int): PermissionType? {
            return lookup[code]
        }
    }
}

package ru.planeta.model.stat

import ru.planeta.model.common.UserSource
import ru.planeta.model.profile.Profile

import java.util.Date

class StatRegistration {

    var profile: Profile? = null
    var email: String? = null
    var timeAdded: Date? = null
    var userSource: UserSource? = null
}

package ru.planeta.dto

import java.math.BigDecimal

/**
 * Использутеся для отображения собственных проектов пользователся в личном кабинете
 */
class MyProjectCardDTO : ProjectCardDTO() {
    /**
     * Причина отклонения, для отклоненных проектов
     */
    var declineCase: String? = null

    /**
     * Сумма, которую пожертвовали сегодня
     */
    var todayPurchase: BigDecimal? = null

    /**
     * Кол-во посещений страницы проекта
     */
    var visits: Int? = 0

    /**
     * Кол-во посещений страницы проекта за сегодня
     */
    var todayVisits: Int? = 0

    /**
     * Кол-во покупок за сегодня
     */
    var todayPurchaseCount: Long? = null

    /**
     * Кол-во комментариев
     */
    var reviews: Int? = 0

    /**
     * Кол-во комментариев за сегодня
     */
    var todayReviews: Int? = 0

    /**
     * Причина постановки на паузу проекта
     */
    var pauseCase: String? = null
}

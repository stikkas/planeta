package ru.planeta.model.stat

import java.util.Date

/**
 * Created by alexa_000 on 04.06.2015.
 */
class WidgetStat {

    var type: WidgetType? = null
    var campaignId: Long = 0
    var date: Date? = null
    var referer: String? = null
    var count: Int = 0

    enum class WidgetType {
        IMAGE, IFRAME, PURCHASE
    }
}

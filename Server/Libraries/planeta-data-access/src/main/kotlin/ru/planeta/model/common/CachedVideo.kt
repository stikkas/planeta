package ru.planeta.model.common

import ru.planeta.commons.external.oembed.provider.VideoType

import java.util.Date

/**
 * User: connanedoile
 * Date: 4/10/12
 * Time: 11:41 AM
 */
class CachedVideo {
    var cachedVideoId: Long = 0
    var url: String? = null
    var title: String? = null
    var description: String? = null
    var duration: Long = 0
    var thumbnailUrl: String? = null
    var html: String? = null
    var width: Int = 0
    var height: Int = 0
    var videoType = VideoType.PLANETA
    var timeAdded = Date()
    var externalId: String? = null

    internal var videoTypeCode: Int
        get() = videoType.code
        set(videoTypeCode) {
            this.videoType = VideoType.getByValue(videoTypeCode)
        }
}

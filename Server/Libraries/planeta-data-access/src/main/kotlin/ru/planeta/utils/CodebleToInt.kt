package ru.planeta.utils

import org.apache.commons.collections4.CollectionUtils
import ru.planeta.model.enums.Codable

import ru.planeta.commons.lang.CollectionUtils.map

/**
 *
 * Created by asavan on 29.11.2016.
 */
object CodebleToInt {
    fun <T : Codable> collectCodes(codables: Collection<T>?): List<Int>? {
        var result: List<Int>? = null
        if (CollectionUtils.isNotEmpty(codables)) {
            result = map(codables) { sourceObject -> sourceObject.code }
        }
        return result
    }
}


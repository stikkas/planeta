package ru.planeta.model.enums

import java.util.HashMap

/**
 * User: atropnikov
 * Date: 06.04.12
 * Time: 12:09
 */
enum class OrderObjectType private constructor(override val code: Int) : Codable {
    SHARE(1),
    PRODUCT(2),
    @Deprecated("")
    DONATE(3),
    TICKET(5),
    @Deprecated("")
    TICKET_NEW(6),
    BONUS(7),
    DELIVERY(8),
    INVESTING(9),
    INVESTING_WITHOUT_MODERATION(10),
    BIBLIO(11),
    // Valid only for BIBLIO order objects, not for     orders
    BOOK(12),
    LIBRARY(13),
    INVESTING_WITH_ALL_ALLOWED_INVOICE(14);


    companion object {
        private val lookup = HashMap<Int, OrderObjectType>()

        init {
            for (type in values()) {
                lookup[type.code] = type
            }
        }

        fun getByValue(code: Int): OrderObjectType? {
            return lookup[code]
        }
    }
}

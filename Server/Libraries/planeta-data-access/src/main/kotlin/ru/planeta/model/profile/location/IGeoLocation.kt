package ru.planeta.model.profile.location

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonTypeInfo

/**
 * @author Andrew.Arefyev@gmail.com
 * 29.10.13 17:31
 */
@JsonTypeInfo(use = JsonTypeInfo.Id.MINIMAL_CLASS, include = JsonTypeInfo.As.PROPERTY, property = "@class", defaultImpl = UnspecifiedLocation::class)
@JsonIgnoreProperties(ignoreUnknown = true)
interface IGeoLocation {

    var locationId: Int

    var locationType: LocationType?

    var name: String?

    var parentLocationId: Int?

    var parentLocationType: LocationType?

    var parent: IGeoLocation?

    companion object {

        val DEFAULT_LOCATION_ID = 0
    }
}

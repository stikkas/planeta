package ru.planeta.model.common

import ru.planeta.model.enums.BannerStatus
import ru.planeta.model.enums.BannerType
import ru.planeta.model.enums.TargetingType

import java.util.Date
import java.util.EnumSet

/**
 * Class Banner
 *
 * @author a.savanovich
 */
class Banner {

    var bannerId: Long = 0
    var statusCode = BannerStatus.OFF.code
    var name: String? = null // for TEASER header
    var html: String? = null // only for TOP_LINE
    var timeAdded: Date? = null
    var timeUpdated: Date? = null

    var imageUrl: String? = null // image for teaser or topline
    var mobileImageUrl: String? = null // image for mobile view
    var refererUrl: String? = null // url to redirect after click
    var altText: String? = null  // text to be shown on mouse over
    var weight: Int = 0 // affects random banner selection
    var mask: String? = null // mask to match url for this banner to be shown
    var maskView: String? = null // easy to read mask equivalent
    var isTarget: Boolean = false // if opens in a new window
    var utmMarks: String? = null // utm marks
    var viewsCount: Long = 0 // count of views
    var clickCount: Long = 0 // count of clicks

    var timeStart: Date? = null // may not be shown before
    var timeFinish: Date? = null // may not be shown after

    var type: BannerType? = null // topline, teaser, etc.
    var showCounter: Int = 0 // if 0 don't show

    var targetingType = EnumSet.of(TargetingType.ALL)

    var status: BannerStatus?
        get() = BannerStatus.getByValue(statusCode)
        set(status) {
            statusCode = status?.code ?: BannerStatus.OFF.code
        }

    val typeCode: Int
        get() = if (type != null) {
            type!!.code
        } else {
            0
        }

    /**
     * For mybatis only
     */
    var targeting: Int
        get() = TargetingType.getCodeByEnumSet(targetingType)
        set(targeting) {
            this.targetingType = TargetingType.getEnumSetFromCode(targeting)
        }

    fun switchStatus() {
        status = if (status === BannerStatus.ON) BannerStatus.OFF else BannerStatus.ON
    }

    fun setTypeCode(typeCode: Int?) {
        type = BannerType.getByValue(typeCode!!)
    }

    @Synchronized
    fun decreaseShowCounter() {
        --showCounter
    }

}

package ru.planeta.model.common.delivery


import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import ru.planeta.model.common.Address
import ru.planeta.model.profile.location.IGeoLocation
import ru.planeta.model.shop.enums.DeliveryType

/**
 * @author Andrew.Arefyev@gmail.com
 * 29.10.13 19:30
 */
@JsonIgnoreProperties(ignoreUnknown = true)
open class BaseDelivery {

    var serviceId: Long = 0
    var location: IGeoLocation? = null
    var name: String? = null
    var nameEn: String? = null
    var description: String? = null
    var descriptionEn: String? = null
    var address: Address? = null
    var serviceType = DeliveryType.DELIVERY
    var isEnabled: Boolean = false

    constructor() {}

    constructor(delivery: BaseDelivery) {
        serviceId = delivery.serviceId
        location = delivery.location
        name = delivery.name
        description = delivery.description
        address = delivery.address
        serviceType = delivery.serviceType
        isEnabled = delivery.isEnabled
        nameEn = delivery.nameEn
        descriptionEn = delivery.descriptionEn
    }

    companion object {
        val DEFAULT_DELIVERY_SERVICE_ID: Long = -1
        val DEFAULT_CUSTOMER_PICKUP_SERVICE_ID: Long = -2
    }
}

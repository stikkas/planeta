package ru.planeta.model.msg

import java.io.Serializable
import java.util.Date

/**
 * Represents message in a dialog
 *
 * @author ameshkov
 */
class DialogMessage : DialogObject(0), Serializable {

    var messageId: Long = 0
    var userId: Long = 0
    var messageText: String? = null
    var messageTextHtml: String? = null
    var isDeleted: Boolean = false
    var timeAdded: Date? = null
    var userImageUrl: String? = null
    var userDisplayName: String? = null
    var userAlias: String? = null
}

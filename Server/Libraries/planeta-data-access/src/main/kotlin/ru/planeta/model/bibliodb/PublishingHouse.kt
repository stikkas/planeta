package ru.planeta.model.bibliodb

import java.io.Serializable

/**
 *
 * User: Serge Blagodatskih<stikkas17></stikkas17>@gmail.com><br></br>
 * Date: 19.04.16<br></br>
 * Time: 15:35
 */
class PublishingHouse : Serializable {

    /**
     * Идентификатор издательства
     */
    var publishingHouseId: Long = 0
    /**
     * Название издательства
     */
    var name: String? = null
    var contractorId: Long = 0
    var contractorName: String? = null
}

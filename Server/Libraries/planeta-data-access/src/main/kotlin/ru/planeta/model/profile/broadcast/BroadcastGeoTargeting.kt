package ru.planeta.model.profile.broadcast

import ru.planeta.model.profile.ProfileObject

/**
 * @author: ds.kolyshev
 * Date: 08.02.13
 */
class BroadcastGeoTargeting : ProfileObject() {
    var broadcastId: Long = 0
    var cityId: Long = 0
    var countryId: Long = 0
    var isAllowed: Boolean = false
}

package ru.planeta.model.profile.location

/**
 * @author Andrew.Arefyev@gmail.com
 * 05.11.13 14:21
 */
abstract class BaseLocation : IGeoLocation {
    override var parent: IGeoLocation? = null
}

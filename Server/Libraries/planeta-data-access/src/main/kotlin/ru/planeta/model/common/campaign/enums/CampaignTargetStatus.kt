package ru.planeta.model.common.campaign.enums

import java.util.HashMap

/**
 * User: atropnikov
 * Date: 09.04.12
 * Time: 17:13
 */
enum class CampaignTargetStatus private constructor(val code: Int) {

    NONE(0), SUCCESS(1), FAIL(2);


    companion object {

        private val lookup = HashMap<Int, CampaignTargetStatus>()

        init {
            for (s in values()) {
                lookup[s.code] = s
            }
        }

        fun getByValue(code: Int): CampaignTargetStatus {
            return lookup[code] ?: NONE
        }
    }
}

package ru.planeta.model.charity

import java.util.*

class LibraryFileExample {
    var orderByClause: String? = null

    var isDistinct: Boolean = false

    var oredCriteria: MutableList<Criteria>

    var offset: Int = 0

    var limit: Int = 0

    init {
        oredCriteria = ArrayList()
    }


    fun or(criteria: Criteria) {
        oredCriteria.add(criteria)
    }

    fun or(): Criteria {
        val criteria = createCriteriaInternal()
        oredCriteria.add(criteria)
        return criteria
    }

    fun createCriteria(): Criteria {
        val criteria = createCriteriaInternal()
        if (oredCriteria.size == 0) {
            oredCriteria.add(criteria)
        }
        return criteria
    }

    protected fun createCriteriaInternal(): Criteria {
        return Criteria()
    }

    fun clear() {
        oredCriteria.clear()
        orderByClause = null
        isDistinct = false
    }

    abstract class GeneratedCriteria() {
        var criteria: MutableList<Criterion> = ArrayList()

        val isValid: Boolean
            get() = criteria.size > 0

        val allCriteria: List<Criterion>
            get() = criteria


        protected fun addCriterion(condition: String?) {
            if (condition == null) {
                throw RuntimeException("Value for condition cannot be null")
            }
            criteria.add(Criterion(condition))
        }

        protected fun addCriterion(condition: String, value: Any?, property: String) {
            if (value == null) {
                throw RuntimeException("Value for $property cannot be null")
            }
            criteria.add(Criterion(condition, value))
        }

        protected fun addCriterion(condition: String, value1: Any?, value2: Any?, property: String) {
            if (value1 == null || value2 == null) {
                throw RuntimeException("Between values for $property cannot be null")
            }
            criteria.add(Criterion(condition, value1, value2))
        }

        fun andLibraryFileIdIsNull(): Criteria {
            addCriterion("library_file.library_file_id is null")
            return this as Criteria
        }

        fun andLibraryFileIdIsNotNull(): Criteria {
            addCriterion("library_file.library_file_id is not null")
            return this as Criteria
        }

        fun andLibraryFileIdEqualTo(value: Long?): Criteria {
            addCriterion("library_file.library_file_id =", value, "libraryFileId")
            return this as Criteria
        }

        fun andLibraryFileIdNotEqualTo(value: Long?): Criteria {
            addCriterion("library_file.library_file_id <>", value, "libraryFileId")
            return this as Criteria
        }

        fun andLibraryFileIdGreaterThan(value: Long?): Criteria {
            addCriterion("library_file.library_file_id >", value, "libraryFileId")
            return this as Criteria
        }

        fun andLibraryFileIdGreaterThanOrEqualTo(value: Long?): Criteria {
            addCriterion("library_file.library_file_id >=", value, "libraryFileId")
            return this as Criteria
        }

        fun andLibraryFileIdLessThan(value: Long?): Criteria {
            addCriterion("library_file.library_file_id <", value, "libraryFileId")
            return this as Criteria
        }

        fun andLibraryFileIdLessThanOrEqualTo(value: Long?): Criteria {
            addCriterion("library_file.library_file_id <=", value, "libraryFileId")
            return this as Criteria
        }

        fun andLibraryFileIdIn(values: List<Long>): Criteria {
            addCriterion("library_file.library_file_id in", values, "libraryFileId")
            return this as Criteria
        }

        fun andLibraryFileIdNotIn(values: List<Long>): Criteria {
            addCriterion("library_file.library_file_id not in", values, "libraryFileId")
            return this as Criteria
        }

        fun andLibraryFileIdBetween(value1: Long?, value2: Long?): Criteria {
            addCriterion("library_file.library_file_id between", value1, value2, "libraryFileId")
            return this as Criteria
        }

        fun andLibraryFileIdNotBetween(value1: Long?, value2: Long?): Criteria {
            addCriterion("library_file.library_file_id not between", value1, value2, "libraryFileId")
            return this as Criteria
        }

        fun andThemeIdIsNull(): Criteria {
            addCriterion("library_file.theme_id is null")
            return this as Criteria
        }

        fun andThemeIdIsNotNull(): Criteria {
            addCriterion("library_file.theme_id is not null")
            return this as Criteria
        }

        fun andThemeIdEqualTo(value: Long?): Criteria {
            addCriterion("library_file.theme_id =", value, "themeId")
            return this as Criteria
        }

        fun andThemeIdNotEqualTo(value: Long?): Criteria {
            addCriterion("library_file.theme_id <>", value, "themeId")
            return this as Criteria
        }

        fun andThemeIdGreaterThan(value: Long?): Criteria {
            addCriterion("library_file.theme_id >", value, "themeId")
            return this as Criteria
        }

        fun andThemeIdGreaterThanOrEqualTo(value: Long?): Criteria {
            addCriterion("library_file.theme_id >=", value, "themeId")
            return this as Criteria
        }

        fun andThemeIdLessThan(value: Long?): Criteria {
            addCriterion("library_file.theme_id <", value, "themeId")
            return this as Criteria
        }

        fun andThemeIdLessThanOrEqualTo(value: Long?): Criteria {
            addCriterion("library_file.theme_id <=", value, "themeId")
            return this as Criteria
        }

        fun andThemeIdIn(values: List<Long>): Criteria {
            addCriterion("library_file.theme_id in", values, "themeId")
            return this as Criteria
        }

        fun andThemeIdNotIn(values: List<Long>): Criteria {
            addCriterion("library_file.theme_id not in", values, "themeId")
            return this as Criteria
        }

        fun andThemeIdBetween(value1: Long?, value2: Long?): Criteria {
            addCriterion("library_file.theme_id between", value1, value2, "themeId")
            return this as Criteria
        }

        fun andThemeIdNotBetween(value1: Long?, value2: Long?): Criteria {
            addCriterion("library_file.theme_id not between", value1, value2, "themeId")
            return this as Criteria
        }

        fun andHeaderIsNull(): Criteria {
            addCriterion("library_file.header is null")
            return this as Criteria
        }

        fun andHeaderIsNotNull(): Criteria {
            addCriterion("library_file.header is not null")
            return this as Criteria
        }

        fun andHeaderEqualTo(value: String): Criteria {
            addCriterion("library_file.header =", value, "header")
            return this as Criteria
        }

        fun andHeaderNotEqualTo(value: String): Criteria {
            addCriterion("library_file.header <>", value, "header")
            return this as Criteria
        }

        fun andHeaderGreaterThan(value: String): Criteria {
            addCriterion("library_file.header >", value, "header")
            return this as Criteria
        }

        fun andHeaderGreaterThanOrEqualTo(value: String): Criteria {
            addCriterion("library_file.header >=", value, "header")
            return this as Criteria
        }

        fun andHeaderLessThan(value: String): Criteria {
            addCriterion("library_file.header <", value, "header")
            return this as Criteria
        }

        fun andHeaderLessThanOrEqualTo(value: String): Criteria {
            addCriterion("library_file.header <=", value, "header")
            return this as Criteria
        }

        fun andHeaderLike(value: String): Criteria {
            addCriterion("library_file.header like", value, "header")
            return this as Criteria
        }

        fun andHeaderNotLike(value: String): Criteria {
            addCriterion("library_file.header not like", value, "header")
            return this as Criteria
        }

        fun andHeaderILike(value: String): Criteria {
            addCriterion("library_file.header ilike", value, "header")
            return this as Criteria
        }

        fun andHeaderIn(values: List<String>): Criteria {
            addCriterion("library_file.header in", values, "header")
            return this as Criteria
        }

        fun andHeaderNotIn(values: List<String>): Criteria {
            addCriterion("library_file.header not in", values, "header")
            return this as Criteria
        }

        fun andHeaderBetween(value1: String, value2: String): Criteria {
            addCriterion("library_file.header between", value1, value2, "header")
            return this as Criteria
        }

        fun andHeaderNotBetween(value1: String, value2: String): Criteria {
            addCriterion("library_file.header not between", value1, value2, "header")
            return this as Criteria
        }

        fun andUrlIsNull(): Criteria {
            addCriterion("library_file.url is null")
            return this as Criteria
        }

        fun andUrlIsNotNull(): Criteria {
            addCriterion("library_file.url is not null")
            return this as Criteria
        }

        fun andUrlEqualTo(value: String): Criteria {
            addCriterion("library_file.url =", value, "url")
            return this as Criteria
        }

        fun andUrlNotEqualTo(value: String): Criteria {
            addCriterion("library_file.url <>", value, "url")
            return this as Criteria
        }

        fun andUrlGreaterThan(value: String): Criteria {
            addCriterion("library_file.url >", value, "url")
            return this as Criteria
        }

        fun andUrlGreaterThanOrEqualTo(value: String): Criteria {
            addCriterion("library_file.url >=", value, "url")
            return this as Criteria
        }

        fun andUrlLessThan(value: String): Criteria {
            addCriterion("library_file.url <", value, "url")
            return this as Criteria
        }

        fun andUrlLessThanOrEqualTo(value: String): Criteria {
            addCriterion("library_file.url <=", value, "url")
            return this as Criteria
        }

        fun andUrlLike(value: String): Criteria {
            addCriterion("library_file.url like", value, "url")
            return this as Criteria
        }

        fun andUrlNotLike(value: String): Criteria {
            addCriterion("library_file.url not like", value, "url")
            return this as Criteria
        }

        fun andUrlIn(values: List<String>): Criteria {
            addCriterion("library_file.url in", values, "url")
            return this as Criteria
        }

        fun andUrlNotIn(values: List<String>): Criteria {
            addCriterion("library_file.url not in", values, "url")
            return this as Criteria
        }

        fun andUrlBetween(value1: String, value2: String): Criteria {
            addCriterion("library_file.url between", value1, value2, "url")
            return this as Criteria
        }

        fun andUrlNotBetween(value1: String, value2: String): Criteria {
            addCriterion("library_file.url not between", value1, value2, "url")
            return this as Criteria
        }
    }

    class Criteria : GeneratedCriteria()

    class Criterion {
        var condition: String? = null
            private set

        var value: Any? = null
            private set

        var secondValue: Any? = null

        var isNoValue: Boolean = false

        var isSingleValue: Boolean = false
            private set

        var isBetweenValue: Boolean = false

        var isListValue: Boolean = false
            private set

        var typeHandler: String? = null
            private set

        constructor(condition: String) : super() {
            this.condition = condition
            this.typeHandler = null
            this.isNoValue = true
        }

        @JvmOverloads constructor(condition: String, value: Any, typeHandler: String? = null) : super() {
            this.condition = condition
            this.value = value
            this.typeHandler = typeHandler
            if (value is List<*>) {
                this.isListValue = true
            } else {
                this.isSingleValue = true
            }
        }

        @JvmOverloads constructor(condition: String, value: Any, secondValue: Any, typeHandler: String? = null) : super() {
            this.condition = condition
            this.value = value
            this.secondValue = secondValue
            this.typeHandler = typeHandler
            this.isBetweenValue = true
        }
    }
}

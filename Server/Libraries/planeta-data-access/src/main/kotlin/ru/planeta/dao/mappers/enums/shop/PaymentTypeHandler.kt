package ru.planeta.dao.mappers.enums.shop

import org.apache.ibatis.type.JdbcType
import org.apache.ibatis.type.TypeHandler
import ru.planeta.model.shop.enums.PaymentType

import java.sql.CallableStatement
import java.sql.PreparedStatement
import java.sql.ResultSet
import java.sql.SQLException

/**
 * Created by eshevchenko.
 */
class PaymentTypeHandler : TypeHandler<PaymentType> {

    @Throws(SQLException::class)
    override fun setParameter(ps: PreparedStatement, i: Int, parameter: PaymentType, jdbcType: JdbcType) {
        ps.setInt(i, parameter.code)
    }

    @Throws(SQLException::class)
    override fun getResult(rs: ResultSet, columnName: String): PaymentType? {
        return PaymentType.getByValue(rs.getInt(columnName))
    }

    @Throws(SQLException::class)
    override fun getResult(rs: ResultSet, columnIndex: Int): PaymentType? {
        return PaymentType.getByValue(rs.getInt(columnIndex))
    }

    @Throws(SQLException::class)
    override fun getResult(cs: CallableStatement, columnIndex: Int): PaymentType? {
        return PaymentType.getByValue(cs.getInt(columnIndex))
    }
}

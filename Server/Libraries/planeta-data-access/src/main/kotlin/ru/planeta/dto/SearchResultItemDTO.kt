package ru.planeta.dto


/**
 * Используется для отображения результатов поиска из заголовка
 */
class SearchResultItemDTO(
        /**
         * URL картинки
         */
        val imageUrl: String?,
        /**
         * Название найденного результата
         */
        val name: String?,
        /**
         * относительная ссылка (без хоста)
         */
        val link: String,
        /**
         * Идентификатор для проекта
         */
        val campaignId: Long?,
        /**
         * Идентификатор для акции
         */
        val shareId: Long?,
        /**
         * Идентификатор для продукта из магазина
         */
        val productId: Long?)


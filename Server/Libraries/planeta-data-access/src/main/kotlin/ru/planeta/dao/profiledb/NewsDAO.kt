package ru.planeta.dao.profiledb

import org.apache.ibatis.annotations.Mapper
import ru.planeta.dto.NewsForProjectCardDTO

@Mapper
interface NewsDAO {
    fun selectLastCampaignNews(campaignId: Long): NewsForProjectCardDTO?
}

package ru.planeta.model.common

import com.fasterxml.jackson.annotation.JsonIgnore
import ru.planeta.model.enums.OrderObjectType
import ru.planeta.model.enums.ProjectType
import ru.planeta.model.shop.enums.DeliveryStatus
import ru.planeta.model.shop.enums.DeliveryType
import ru.planeta.model.shop.enums.PaymentStatus
import ru.planeta.model.shop.enums.PaymentType
import java.math.BigDecimal
import java.util.Date

/**
 * todo add comment
 */
open class Order : TransactionalObject() {

    var orderId: Long = 0
    var orderType: OrderObjectType? = null
    var topayTransactionId: Long = 0
    var buyerId: Long = 0
    var paymentType = PaymentType.NOT_SET
    var paymentStatus = PaymentStatus.PENDING
    var errorCode: String? = null
    var totalPrice = BigDecimal.ZERO
    var timeAdded: Date? = null
    var timeUpdated: Date? = null
    var reserveTimeExpired: Date? = null

    var deliveryType = DeliveryType.NOT_SET
    var deliveryStatus = DeliveryStatus.PENDING
    var deliveryPrice = BigDecimal.ZERO
    @get:JsonIgnore
    var deliveryDepartmentId: Long = 0

    var trackingCode: String? = null
        set(trackingCode) {
            field = if (trackingCode != null && trackingCode.length == 0) null else trackingCode
        }
    var cashOnDeliveryCost: BigDecimal? = BigDecimal.ZERO
        set(cashOnDeliveryCost) {
            field = cashOnDeliveryCost ?: BigDecimal.ZERO
        }
    var deliveryComment: String? = null
    var projectType: ProjectType? = null

    var investInfo: InvestingOrderInfo? = null

    var promoCodeId: Long = 0
    var discount = BigDecimal.ZERO
}

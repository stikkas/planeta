package ru.planeta.model.stat

import java.math.BigDecimal

abstract class StatPurchaseCommon : Comparable<Any> {
    //
    var typeId: Long = 0
    var name: String? = null
    var price: BigDecimal? = null
    //
    var count: Int? = null
    var countDiff: Int? = null
    //
    var amount: BigDecimal? = null
    var amountDiff: BigDecimal? = null

    abstract override fun compareTo(o: Any): Int

    override fun equals(o: Any?): Boolean {
        if (this === o) return true
        if (o == null || javaClass != o.javaClass) return false

        val that = o as StatPurchaseCommon?

        return if (typeId != that!!.typeId) false else true

    }

    override fun hashCode(): Int {
        return (typeId xor typeId.ushr(32)).toInt()
    }
}

package ru.planeta.model.profile

import ru.planeta.model.enums.ObjectType

/**
 * User: s.fionov
 * Date: 20.01.12
 */
class ObjectCommentRead : IProfileObject {
    override var profileId: Long? = 0
    var objectId: Long = 0
    var objectType: ObjectType? = null
    var userProfileId: Long = 0
    var lastCommentId: Long = 0
    var lastCommentCount: Int = 0

    var objectTypeCode: Int
        get() = objectType?.code ?: 0
        set(objectTypeCode) {
            this.objectType = ObjectType.Companion.getByCode(objectTypeCode)
        }
}

package ru.planeta.model.profile.broadcast

import ru.planeta.model.profile.ProfileObject

import java.math.BigDecimal

/**
 * Broadcast's backers limitings
 *
 * @author: ds.kolyshev
 * Date: 18.04.13
 */
class BroadcastBackersTargeting : ProfileObject() {
    var broadcastId: Long = 0
    var campaignId: Long = 0
    var limitSumm: BigDecimal? = null
}

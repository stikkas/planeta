package ru.planeta.dao.commondb

import org.apache.ibatis.annotations.Mapper
import org.apache.ibatis.annotations.Param
import ru.planeta.model.common.DebitTransaction

/**
 * Interface CredtiTransactionDAO
 *
 * @author a.tropnikov
 */
@Mapper
interface DebitTransactionDAO {

    fun selectList(@Param("profileId") profileId: Long, @Param("offset") offset: Int, @Param("limit") limit: Int): List<DebitTransaction>

    fun select(transactionId: Long): DebitTransaction?

    fun insert(debitTransaction: DebitTransaction): DebitTransaction

    fun update(debitTransaction: DebitTransaction): DebitTransaction

    fun delete(@Param("profileId") profileId: Long, @Param("transactionId") transactionId: Long)
}

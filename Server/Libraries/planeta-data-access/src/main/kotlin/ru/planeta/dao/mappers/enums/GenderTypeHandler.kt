package ru.planeta.dao.mappers.enums

import org.apache.ibatis.type.JdbcType
import org.apache.ibatis.type.TypeHandler
import ru.planeta.commons.model.Gender

import java.sql.CallableStatement
import java.sql.PreparedStatement
import java.sql.ResultSet
import java.sql.SQLException

/**
 * Created with IntelliJ IDEA.
 * Date: 18.01.2016
 * Time: 17:03
 * To change this template use File | Settings | File Templates.
 */
class GenderTypeHandler : TypeHandler<Gender> {
    @Throws(SQLException::class)
    override fun setParameter(ps: PreparedStatement, i: Int, parameter: Gender, jdbcType: JdbcType) {
        ps.setInt(i, parameter.code)
    }

    @Throws(SQLException::class)
    override fun getResult(rs: ResultSet, columnName: String): Gender {
        return Gender.getByValue(rs.getInt(columnName))
    }

    @Throws(SQLException::class)
    override fun getResult(rs: ResultSet, columnIndex: Int): Gender {
        return Gender.getByValue(rs.getInt(columnIndex))
    }

    @Throws(SQLException::class)
    override fun getResult(cs: CallableStatement, columnIndex: Int): Gender {
        return Gender.getByValue(cs.getInt(columnIndex))
    }
}

package ru.planeta.model.concert

import ru.planeta.model.enums.Codable

import java.util.EnumSet
import java.util.HashMap

/**
 * Created with IntelliJ IDEA.
 * User: michail
 * Date: 14.10.16
 * Time: 14:32
 */
enum class PlaceState private constructor(override val code: Int) : Codable {
    FREE(0),
    UNAVAILABLE(1),
    RESERVED(2),
    SOLD(3);


    companion object {

        private val lookup = HashMap<Int, PlaceState>()

        init {
            for (s in EnumSet.allOf(PlaceState::class.java)) {
                lookup[s.code] = s
            }
        }

        fun getByValue(code: Int?): PlaceState? {
            return lookup[code]
        }


        fun fromMoscowShowState(msState: Int): PlaceState {
            // http://handydev.com/moscowshow/api/struct_handydev_1_1_premiera_1_1_core_1_1_web_1_1_structures_1_1_place.html
            // 0 - свободно
            // 1 - недоступно к заказу
            // 3 - забронировано
            // 5 - неоплачено, валидировано 1 раз или более
            // 4 - оплачено, валидировано 1 раз
            // 6 - оплачено, валидировано более 1 раза

            when (msState) {
                0 -> return FREE
                1 -> return UNAVAILABLE
                3, 5 -> return RESERVED
                4, 6 -> return SOLD
                else -> return UNAVAILABLE
            }
        }
    }
}

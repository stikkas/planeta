package ru.planeta.model.shop

/**
 * product with requested quantity
 *
 * @author Andrew Arefyev
 * date: 03.12.12
 * Time: 18:16
 */
class ShoppingCartShortItem {
    var productId: Long = 0
    var quantity: Int = 0

    constructor() {}

    constructor(productId: Long, quantity: Int) {
        this.productId = productId
        this.quantity = quantity
    }
}

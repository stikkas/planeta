package ru.planeta.model.msg

import java.io.Serializable
import java.util.Date

/**
 * Represents a dialog between one or more participants.
 *
 * @author ameshkov
 */
class Dialog : DialogObject(0), Serializable {

    override var dialogId: Long = 0
    var name: String? = null
    var creatorUserId: Long = 0
    var lastMessageId: Long = 0
    var messagesCount: Long = 0
    var timeAdded: Date? = null
}

package ru.planeta.model.shop.enums

import ru.planeta.model.enums.Codable

import java.util.*

/**
 * User: a.savanovich
 * Date: 20.06.12
 * Time: 18:18
 */
enum class DeliveryStatus private constructor(override val code: Int) : Codable {
    PENDING(0),
    IN_TRANSIT(1),
    DELIVERED(2),
    REJECTED(3),
    CANCELLED(4),
    GIVEN(5);


    companion object {

        private val lookup = HashMap<Int, DeliveryStatus>()
        private val validTransitions = LinkedHashMap<DeliveryStatus, Set<DeliveryStatus>>()

        init {
            for (s in values()) {
                lookup[s.code] = s
            }
            validTransitions[PENDING] = EnumSet.of(IN_TRANSIT, CANCELLED, GIVEN)
            validTransitions[IN_TRANSIT] = EnumSet.of(DELIVERED, REJECTED, GIVEN)
            validTransitions[DELIVERED] = EnumSet.of(REJECTED, GIVEN)
            validTransitions[CANCELLED] = EnumSet.noneOf(DeliveryStatus::class.java)
            validTransitions[REJECTED] = EnumSet.noneOf(DeliveryStatus::class.java)
            validTransitions[GIVEN] = EnumSet.of(DELIVERED, CANCELLED, REJECTED)
        }

        fun getValidTransitions(): Map<DeliveryStatus, Set<DeliveryStatus>> {
            return validTransitions
        }

        fun getValidTransitionsByStatus(status: DeliveryStatus): Set<DeliveryStatus>? {
            return validTransitions[status]
        }

        fun getByValue(code: Int): DeliveryStatus? {
            return lookup[code]
        }
    }

}

package ru.planeta.dao.parameter

/**
 * Created by IntelliJ IDEA.
 * User: Andrew
 * Date: 13.11.12
 * Time: 17:42
 */
class ShopDbParameter : CommonDbParameters() {
    companion object {

        val STORE_ID = "storeId"
        val TIME_UPDATED = "timeUpdated"
        val OWNER_PROFILE_ID = "ownerProfileId"
        val PROFILE_ID = "profileId"
        val QUANTITY = "quantity"
        val TIME_STARTED = "timeStarted"
        val PRODUCT_ID = "productId"
        val REFERRER_ID = "referrerId"
        val SHOW_ON_CAMPAIGN = "showOnCampaign"
        val CAMPAIGN_ID = "campaignId"
        val CAMPAIGN_IDS = "campaignIds"
        val PRODUCT_IDS = "productIds"
        val RECORD_ID = "recordId"
        val AUTHOR_PROFILE_ID = "authorProfileId"
        val PARENT_PRODUCT_ID = "parentProductId"
        val NAME_HTML = "nameHtml"
        val DESCRIPTION_HTML = "descriptionHtml"
        val SEARCH_STRING = "searchString"
        val PRODUCT_STATE = "productState"
        val NEW_PARENT_ID = "new_parent_id"
        val SORT_CRITERIA = "sortCriteria"
        val FROM_STATUS = "a"
        val TO_STATUS = "b"
        val FROM_VERSION = "fromVersion"
        val TO_VERSION = "toVersion"
        val ATTRIBUTE_ID = "attributeId"
        //TODO check for logical duplicates
        val CATEGORY_ID = "categoryId"
        val CATEGORY_TYPE_ID = "categoryTypeId"
        val ATTRIBUTE_TYPE_ID = "attributeTypeId"

        val ORDER_ID = "orderId"
        val IDS = "ids"

        val PRICE_FROM = "priceFrom"
        val PRICE_TO = "priceTo"
    }

}

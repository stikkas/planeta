package ru.planeta.model.common.campaign

/**
 * Created by alexa_000 on 11.03.2015.
 */
class Sponsor {

    var alias: String? = null
    var multiplier = 1
    var maxDonate: Long? = null
    var projectCardHtml: String? = null
    var projectPageHtml: String? = null
    var searchPageHtml: String? = null
}

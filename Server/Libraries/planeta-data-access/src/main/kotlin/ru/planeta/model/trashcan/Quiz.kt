package ru.planeta.model.trashcan

import java.util.Date

class Quiz {
    var quizId: Long = 0
    var name: String? = null
    var alias: String? = null
    var description: String? = null
    var isEnabled = false
    var timeAdded: Date? = null
}

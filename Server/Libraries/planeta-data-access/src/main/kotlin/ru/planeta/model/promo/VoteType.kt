package ru.planeta.model.promo

import ru.planeta.model.enums.Codable

import java.util.EnumSet
import java.util.HashMap

/**
 * Created with IntelliJ IDEA.
 * User: michail
 * Date: 11.01.2017
 * Time: 16:24
 */
enum class VoteType private constructor(override val code: Int) : Codable {
    FB(1), VK(2);


    companion object {

        private val lookup = HashMap<Int, VoteType>()

        init {
            for (s in values()) {
                lookup[s.code] = s
            }
        }

        fun getByValue(code: Int): VoteType? {
            return lookup[code]
        }

        fun getCodeByEnumSet(set: EnumSet<VoteType>): Int {
            var statusCode = 0
            for (s in set) {
                statusCode = statusCode or s.code
            }

            return statusCode
        }
    }
}

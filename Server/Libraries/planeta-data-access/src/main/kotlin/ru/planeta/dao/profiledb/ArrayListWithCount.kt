package ru.planeta.dao.profiledb

import org.apache.ibatis.annotations.Param
import java.util.ArrayList

class ArrayListWithCount<T> : ArrayList<T> {
    var totalCount: Int = 0

    constructor(list: Collection<T>) : super(list) {}

    constructor() : super() {}

    constructor(list: List<T>, @Param("totalCount") totalCount: Int) : super(list) {
        this.totalCount = totalCount
    }
}

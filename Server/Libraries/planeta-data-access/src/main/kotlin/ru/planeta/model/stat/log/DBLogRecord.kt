package ru.planeta.model.stat.log

import com.fasterxml.jackson.annotation.JsonIgnore

/**
 * Database log record.<br></br>
 * User: eshevchenko
 */
open class DBLogRecord : BaseDBLogRecord() {

    var message: String? = null
    var loggerType: LoggerType? = null
    var objectId: Long = 0
    var subjectId: Long = 0
    @get:JsonIgnore
    @set:JsonIgnore
    var requestId: Long = 0
    @get:JsonIgnore
    @set:JsonIgnore
    var responseId: Long = 0
}

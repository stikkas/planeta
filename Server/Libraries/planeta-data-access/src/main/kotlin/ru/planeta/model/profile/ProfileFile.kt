package ru.planeta.model.profile

import java.util.Date

/**
 * @author: ds.kolyshev
 * Date: 17.03.13
 */
class ProfileFile : IProfileObject {

    override var profileId: Long? = 0
    var authorId: Long = 0
    var fileId: Long = 0
    var name: String? = null
    var fileUrl: String? = null
    var title: String? = null
    var description: String? = null
    var timeAdded: Date? = null
    var timeUpdated: Date? = null
    var size: Long = 0
    var extension: String? = null
}

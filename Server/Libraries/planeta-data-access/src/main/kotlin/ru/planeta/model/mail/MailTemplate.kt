package ru.planeta.model.mail

/**
 * Mail template class
 *
 * @author ds.kolyshev
 * Date: 12.01.12
 */
class MailTemplate {
    var templateId: Int? = null
    var name: String? = null
        set(name) {
            field = name?.trim { it <= ' ' }
        }
    var subject: String? = null
        set(subject) {
            field = subject?.trim { it <= ' ' }
        }
    var contentBbcode: String? = null
        set(contentBbcode) {
            field = contentBbcode?.trim { it <= ' ' }
        }
    var contentHtml: String? = null
        set(contentHtml) {
            field = contentHtml?.trim { it <= ' ' }
        }
    var isUseFooterAndHeader: Boolean = false
    var fromAddress: String? = null
        set(fromAddress) {
            field = fromAddress?.trim { it <= ' ' }
        }
    var replyToAddress: String? = null
        set(replyToAddress) {
            field = replyToAddress?.trim { it <= ' ' }
        }
    var toAddress: String? = null
        set(toAddress) {
            field = toAddress?.trim { it <= ' ' }
        }
    var attachments: List<MailAttachment>? = null
    var isSkipMessageLogging: Boolean = false
}

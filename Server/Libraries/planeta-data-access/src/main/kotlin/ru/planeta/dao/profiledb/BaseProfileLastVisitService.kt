package ru.planeta.dao.profiledb

import org.apache.ibatis.annotations.Param
import java.util.Date
import org.springframework.beans.factory.annotation.Autowired
import ru.planeta.model.enums.ProfileLastVisitType
import ru.planeta.model.profiledb.ProfileLastVisit
import ru.planeta.model.profiledb.ProfileLastVisitExample

abstract class BaseProfileLastVisitService {
    @Autowired
    protected var profileLastVisitMapper: ProfileLastVisitMapper? = null

    //**************************************************************************************************
    //*********************************  Create ** Update ** Delete  *************************************
    //**************************************************************************************************
    fun insertProfileLastVisit(profileLastVisit: ProfileLastVisit) {

        profileLastVisit.timeUpdated = Date()
        profileLastVisitMapper!!.insertSelective(profileLastVisit)
    }

    fun insertProfileLastVisitAllFields(profileLastVisit: ProfileLastVisit) {

        profileLastVisit.timeUpdated = Date()
        profileLastVisitMapper!!.insert(profileLastVisit)
    }

    fun updateProfileLastVisit(profileLastVisit: ProfileLastVisit) {
        profileLastVisit.timeUpdated = Date()
        profileLastVisitMapper!!.updateByPrimaryKeySelective(profileLastVisit)
    }

    fun updateProfileLastVisitAllFields(profileLastVisit: ProfileLastVisit) {
        profileLastVisit.timeUpdated = Date()
        profileLastVisitMapper!!.updateByPrimaryKey(profileLastVisit)
    }

    fun insertOrUpdateProfileLastVisit(profileLastVisit: ProfileLastVisit) {
        profileLastVisit.timeUpdated = Date()
        val selectedProfileLastVisit = profileLastVisitMapper!!.selectByPrimaryKey(profileLastVisit.profileId,
                profileLastVisit.profileLastVisitType, profileLastVisit.objectId)
        if (selectedProfileLastVisit == null) {

            profileLastVisitMapper!!.insertSelective(profileLastVisit)
        } else {
            profileLastVisitMapper!!.updateByPrimaryKeySelective(profileLastVisit)
        }
    }

    fun insertOrUpdateProfileLastVisitAllFields(profileLastVisit: ProfileLastVisit) {
        profileLastVisit.timeUpdated = Date()
        val selectedProfileLastVisit = profileLastVisitMapper!!.selectByPrimaryKey(profileLastVisit.profileId, profileLastVisit.profileLastVisitType, profileLastVisit.objectId)
        if (selectedProfileLastVisit == null) {

            profileLastVisitMapper!!.insert(profileLastVisit)
        } else {
            profileLastVisitMapper!!.updateByPrimaryKey(profileLastVisit)
        }
    }

    //**************************************************************************************************
    //************************************** profile_last_visit ******************************************
    //************************************** SelectByPrimaryKey ******************************************
    //**************************************************************************************************
    fun selectProfileLastVisit(profileId: Long?, @Param("profileLastVisitType") profileLastVisitType: ProfileLastVisitType, @Param("objectId") objectId: Long?): ProfileLastVisit {
        return profileLastVisitMapper!!.selectByPrimaryKey(profileId, profileLastVisitType, objectId)
    }

    //**************************************************************************************************
    //************************************** profile_last_visit ******************************************
    //**** Long profileId,ProfileLastVisitType profileLastVisitType,Long objectId,Date timeUpdated *******
    //**************************************************************************************************
    private fun COMMENT_SECTION_ProfileLastVisit(profileId: Long?, @Param("profileLastVisitType") profileLastVisitType: ProfileLastVisitType, @Param("objectId") objectId: Long?, @Param("timeUpdated") timeUpdated: Date) {

    }

    fun getProfileLastVisitExample(profileId: Long?, @Param("profileLastVisitType") profileLastVisitType: ProfileLastVisitType, @Param("objectId") objectId: Long?, @Param("timeUpdated") timeUpdated: Date, @Param("offset") offset: Int, @Param("limit") limit: Int): ProfileLastVisitExample {
        val example = ProfileLastVisitExample()
        example.offset = offset
        example.limit = limit
        example.or()
                .andProfileIdEqualTo(profileId)
                .andProfileLastVisitTypeEqualTo(profileLastVisitType)
                .andObjectIdEqualTo(objectId)
                .andTimeUpdatedGreaterThan(timeUpdated)
        return example
    }

    fun selectProfileLastVisitCount(profileId: Long?, @Param("profileLastVisitType") profileLastVisitType: ProfileLastVisitType, @Param("objectId") objectId: Long?, @Param("timeUpdated") timeUpdated: Date): Int {
        return profileLastVisitMapper!!.countByExample(getProfileLastVisitExample(profileId, profileLastVisitType, objectId, timeUpdated, 0, 0))
    }

    fun selectProfileLastVisitList(profileId: Long?, @Param("profileLastVisitType") profileLastVisitType: ProfileLastVisitType, @Param("objectId") objectId: Long?, @Param("timeUpdated") timeUpdated: Date, @Param("offset") offset: Int, @Param("limit") limit: Int): List<ProfileLastVisit> {
        return profileLastVisitMapper!!.selectByExample(getProfileLastVisitExample(profileId, profileLastVisitType, objectId, timeUpdated, offset, limit))
    }
}

package ru.planeta.dao.bibliodb

import org.apache.ibatis.annotations.Mapper
import org.apache.ibatis.annotations.Param
import ru.planeta.model.bibliodb.Book
import ru.planeta.model.bibliodb.BookFilter
import java.util.*

/*
 * Created by Alexey on 08.04.2016.
 */

@Mapper
interface BookDAO {

    fun insertBook(book: Book)

    fun selectBookById(id: Long): Book

    fun selectActiveBookById(id: Long): Book

    fun selectBooks(): List<Book>

    fun searchBooks(filter: BookFilter): List<Book>

    fun selectCountSearchBooks(filter: BookFilter): Long

    fun updateBook(book: Book)

    fun deleteBook(id: Long)

    fun countActiveBooks(): Long

    fun selectBooksForIndexPage(): List<Book>

    fun selectBookForChangePrice(@Param("date") date: Date, @Param("offset") offset: Long,
                                 @Param("limit") limit: Long): List<Book>

    fun selectHasRequests(@Param("title") title: String, @Param("email") email: String): Boolean
}



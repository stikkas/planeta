package ru.planeta.model.common.loyalty

import com.fasterxml.jackson.annotation.JsonIgnoreProperties

/**
 * Represents bonus item
 *
 *
 * User: m.shulepov
 * Date: 03.04.14
 */
@JsonIgnoreProperties(ignoreUnknown = true)
class Bonus {

    var bonusId: Long = 0
    var name: String? = null
    var description: String? = null
    var descriptionHtml: String? = null
    var imageId: Long = 0
    var imageUrl: String? = null
    var link: String? = null
    var buttonText: String? = null
    var linkText: String? = null
    var campaignAlias: String? = null
    var campaignProgress: Int = 0
    var campaignName: String? = null
    var price: Int = 0
    var priority: Int = 0
    var available: Int = 0
    var type: Type? = null
    var isEnabled: Boolean = false
    var isDeleted: Boolean = false

    enum class Type {
        INFO, BONUS
    }
}



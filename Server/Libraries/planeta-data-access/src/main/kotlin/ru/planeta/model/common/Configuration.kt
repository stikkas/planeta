package ru.planeta.model.common

/**
 * Planeta's configuration property
 *
 * @author ds.kolyshev
 * Date: 17.05.12
 */
class Configuration {
    var key: String? = null
    var intValue: Int = 0
    var stringValue: String? = null
    var booleanValue: Boolean = false
    var description: String? = null
}

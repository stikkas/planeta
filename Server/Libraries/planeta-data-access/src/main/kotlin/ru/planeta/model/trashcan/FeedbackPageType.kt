package ru.planeta.model.trashcan

import ru.planeta.model.enums.Codable
import java.util.*

enum class FeedbackPageType (override val code: Int) : Codable {
    PAYMENT_SUCCESS(1), PAYMENT_FAIL(2), CAMPAIGN_CREATED_SUCCESSFUL(3);


    companion object {

        private val lookup = HashMap<Int, FeedbackPageType>()

        init {
            for (s in values()) {
                lookup[s.code] = s
            }
        }

        fun getByValue(code: Int): FeedbackPageType? {
            return lookup[code]
        }
    }
}

package ru.planeta.model.common.auth

import java.util.EnumSet
import java.util.TreeMap

/**
 * Date: 02.10.12
 *
 * @author s.kalmykov
 */
enum class CredentialType private constructor(val code: Int) {
    ERROR(0), // default credentialType, used if code from base is old and enum has no such value
    EMAIL(1), // simple email and password, needed to be confirmed (has credentialStatus PENDING before)
    VK(2), // vkontakte uid(long) and md5(apiId + uid + apiSecret) hash;
    FACEBOOK(3),
    YANDEX(4),
    ODNOKLASSINKI(5),
    MAILRU(6);


    companion object {

        private val lookup = TreeMap<Int, CredentialType>()

        init {
            for (s in EnumSet.allOf(CredentialType::class.java)) {
                lookup[s.code] = s
            }
        }

        fun getByValue(code: Int): CredentialType {
            val mayBeType = lookup[code]
            return mayBeType ?: ERROR
        }
    }
}

package ru.planeta.model.profile.media.enums

import org.apache.commons.lang3.StringUtils

import java.util.EnumSet
import java.util.HashMap

/**
 * Video converting quality modes
 *
 * @author ds.kolyshev
 * Date: 26.09.11
 */
enum class VideoQuality private constructor(val code: Int, val fileName: String?) {
    NOT_SET(0, null), MODE_240P(1, "240p.mp4"), MODE_360P(2, "360p.mp4"), MODE_480P(4, "480p.mp4"), MODE_720P(8, "720p.mp4"), MODE_1080P(16, "1080p.mp4");


    companion object {

        private val lookup = HashMap<Int, VideoQuality>()

        init {
            for (s in EnumSet.allOf(VideoQuality::class.java)) {
                lookup[s.code] = s
            }
        }

        fun getByValue(code: Int): VideoQuality {
            return lookup[code] ?: NOT_SET
        }

        fun getCodeByEnumSet(set: EnumSet<VideoQuality>): Int {
            var statusCode = 0
            for (s in set) {
                statusCode = statusCode or s.code
            }

            return statusCode
        }

        fun getEnumSetFromCode(code: Int): EnumSet<VideoQuality> {
            val set = EnumSet.noneOf(VideoQuality::class.java)

            for (s in VideoQuality.values()) {
                if (s.code and code != 0) {
                    set.add(s)
                }
            }

            return set
        }

        /**
         * Gets map of urls by qualities set
         *
         */
        fun getUrlsByVideoQualitySet(videoUrl: String?, videoQualityModes: EnumSet<VideoQuality>?): Map<VideoQuality, String>? {
            if (StringUtils.isEmpty(videoUrl)) {
                return null
            }

            val lastSlashIndex = videoUrl?.lastIndexOf('/') ?: -1
            if (lastSlashIndex < 0) {
                return null
            }

            val map = HashMap<VideoQuality, String>()
            videoQualityModes?.let {
                for (quality in videoQualityModes) {
                    map[quality] = videoUrl?.substring(0, lastSlashIndex + 1) + quality.fileName
                }
            }

            return map
        }
    }


}

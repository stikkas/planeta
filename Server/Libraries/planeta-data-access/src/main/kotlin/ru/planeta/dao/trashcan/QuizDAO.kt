package ru.planeta.dao.trashcan

import org.apache.ibatis.annotations.Param
import ru.planeta.dao.profiledb.ArrayListWithCount
import ru.planeta.model.trashcan.*

interface QuizDAO {
    fun insert(quiz: Quiz)

    fun update(quiz: Quiz)

    fun insert(quizQuestion: QuizQuestion)

    fun insertQuizAnswer(quizAnswer: QuizAnswer)

    fun insertQuizQuestionRelation(@Param("quizId") quizId: Long, @Param("quizQuestionId") quizQuestionId: Long, @Param("quizQuestionOrderNum") quizQuestionOrderNum: Long)

    fun selectNextQuizQuestionOrderNumInQuiz(quizId: Long): Long

    fun insertQuizQuestionOption(quizQuestionOption: QuizQuestionOption)

    fun selectNextQuizQuestionOptionOrderNumInQuiz(quizQuestionId: Long): Long

    fun selectQuizzes(@Param("searchString") searchString: String, @Param("offset") offset: Int, @Param("limit") limit: Int): ArrayListWithCount<Quiz>

    fun selectQuizzesSearchTotalCount(params: Map<*, *>): Int

    fun selectQuizQuestions(@Param("offset") offset: Int, @Param("limit") limit: Int): List<QuizQuestion>

    fun selectQuizQuestionsByQuizId(@Param("quizId") quizId: Long, @Param("enabled") enabled: Boolean?): List<QuizQuestion>

    fun selectQuizQuestionById(quizQuestionId: Long): QuizQuestion

    fun selectQuizAnswerByParams(@Param("quizId") quizId: Long, @Param("quizQuestionId") quizQuestionId: Long, @Param("userId") userId: Long): QuizAnswer

    fun selectQuizAnswersByParams(quizId: Long?, @Param("quizQuestionId") quizQuestionId: Long?, @Param("userId") userId: Long?, @Param("offset") offset: Long, @Param("limit") limit: Long): List<QuizAnswer>

    fun selectQuizAnswersForReportByParams(quizId: Long?, @Param("userId") userId: Long?, @Param("offset") offset: Long, @Param("limit") limit: Long): List<QuizAnswerForReport>

    fun updateQuizQuestion(quizQuestion: QuizQuestion)

    fun updateQuizQuestionsOrder(@Param("quizId") quizId: Long, @Param("quizQuestionId") quizQuestionId: Long, @Param("newOrderNum") newOrderNum: Int, @Param("oldOrderNum") oldOrderNum: Int)

    fun updateQuizQuestionOptionsOrder(@Param("quizQuestionOptionId") quizQuestionOptionId: Long, @Param("quizQuestionId") quizQuestionId: Long, @Param("newOrderNum") newOrderNum: Int, @Param("oldOrderNum") oldOrderNum: Int)

    fun updateQuizQuestionOption(quizQuestionOption: QuizQuestionOption)

    fun updateQuizQuestionEnabling(@Param("quizQuestionId") quizQuestionId: Long, @Param("enabled") enabled: Boolean)

    fun deleteQuizQuestionOption(quizQuestionOptionId: Long)

    fun selectQuiz(@Param("quizAlias") quizAlias: String, @Param("enabled") enabled: Boolean?): Quiz
}

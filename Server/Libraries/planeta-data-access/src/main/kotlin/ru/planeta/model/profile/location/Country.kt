package ru.planeta.model.profile.location

import java.io.Serializable

/**
 * @author a.savanovich
 */
class Country : BaseLocation(), Serializable {
    override var locationId: Int
        get() = countryId
        set(value) {
            this.countryId = value
        }

    override var locationType: LocationType? = null
        get() = LocationType.COUNTRY

    override var name: String?
        get() = countryNameRus
        set(value) {}
    override var parentLocationId: Int?
        get() = globalRegionId
        set(value) {}
    override var parentLocationType: LocationType? = null
        get() = LocationType.GLOBAL_REGION

    // setters only for ibatis
    var countryId: Int = 0
    var countryNameRus: String? = null
    var countryNameEn: String? = null
    var globalRegionId: Int = 0

    override fun equals(o: Any?): Boolean {
        if (this === o) return true
        if (o !is Country) return false

        val country = o as Country?

        return countryId == country!!.countryId && globalRegionId == country.globalRegionId
    }

    override fun hashCode(): Int {
        var result = countryId
        result = 31 * result + globalRegionId
        return result
    }

    companion object {

        private const val serialVersionUID = 1L
    }

}

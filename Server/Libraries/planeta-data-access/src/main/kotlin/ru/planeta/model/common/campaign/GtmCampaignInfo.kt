package ru.planeta.model.common.campaign

/**
 * Created by kostiagn on 22.12.2015.
 */
class GtmCampaignInfo {
    var campaignId: Long = 0
    var campaignAlias: String? = null
    var campaignName: String? = null
    var firstTagName: String? = null
    var creatorName: String? = null

    val webCampaignAlias: String?
        get() = if (campaignAlias != null) campaignAlias else campaignId.toString()
}

package ru.planeta.dao.charitydb

/*
 * Created by Alexey on 24.06.2016.
 */

import org.apache.ibatis.annotations.Mapper
import org.apache.ibatis.annotations.Param
import ru.planeta.model.charity.Member

@Mapper
interface CharityDAO {
    fun selectAllMembers(@Param("offset") offset: Int, @Param("limit") limit: Int): List<Member>
}

package ru.planeta.model.common

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import ru.planeta.model.commondb.Partner

@JsonIgnoreProperties(ignoreUnknown = true)
class PartnerForJson : Partner()

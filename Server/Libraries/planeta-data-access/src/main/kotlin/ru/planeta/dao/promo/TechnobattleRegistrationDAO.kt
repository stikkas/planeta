package ru.planeta.dao.promo

import org.apache.ibatis.annotations.Param
import ru.planeta.model.promo.TechnobattleRegistration

/**
 * Created with IntelliJ IDEA.
 * User: michail
 * Date: 18.11.2016
 * Time: 11:47
 */
interface TechnobattleRegistrationDAO {
    fun insert(registration: TechnobattleRegistration): Int

    fun select(@Param("offset") offset: Int, @Param("limit") limit: Int): List<TechnobattleRegistration>

    fun selectById(id: Long): TechnobattleRegistration
}

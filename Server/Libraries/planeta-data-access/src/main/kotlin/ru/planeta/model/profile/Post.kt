package ru.planeta.model.profile

import ru.planeta.model.charity.enums.CharityEventType
import ru.planeta.model.common.Identified

import java.util.Date

/**
 * Created with IntelliJ IDEA.
 * User: a.volkov
 * Date: 11.11.2014
 * Time: 18:15
 */
class Post : Identified, Identifier {
    //what
    var postId: Long = 0
        private set
    //who
    var profileId: Long = 0
    //where
    var campaignId: Long = 0
    var tagId: Long = 0
    var title: String? = null
    var headingText: String? = null
    var postText: String? = null
    var commentsCount: Int = 0
    var timeAdded: Date? = null
    var timeUpdated: Date? = null
    var notificationTime: Date? = null
    var eventDate: Date? = null
    var eventLocation: String? = null
    var eventType: CharityEventType? = null
    var imageId: Long = 0
    var imageUrl: String? = null
    var weight: Int = 0

    constructor() {}

    constructor(profileId: Long, campaignId: Long, title: String, headingText: String, postText: String) {
        this.profileId = profileId
        this.campaignId = campaignId
        this.title = title
        this.headingText = headingText
        this.postText = postText
    }

    override var id: Long
        get() = postId
        set(id) {
            postId = id
        }
}

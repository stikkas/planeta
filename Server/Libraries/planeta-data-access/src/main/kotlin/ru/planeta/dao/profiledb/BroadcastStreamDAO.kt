package ru.planeta.dao.profiledb

import org.apache.ibatis.annotations.Mapper
import org.apache.ibatis.annotations.Param
import ru.planeta.model.profile.broadcast.BroadcastStream

/**
 * Broadcast stream dao
 *
 * @author ds.kolyshev
 * Date: 19.03.12
 */
@Mapper
interface BroadcastStreamDAO {
    /**
     * Selects list of streams for specified profile's broadcast
     *
     * @param broadcastId
     * @param offset
     * @param limit
     * @return
     */
    fun selectByBroadcastId(@Param("broadcastId") broadcastId: Long, @Param("offset") offset: Int, @Param("limit") limit: Int): List<BroadcastStream>

    /**
     * Selects stream by identifier
     *
     * @param streamId
     * @return
     */
    fun selectById(streamId: Long): BroadcastStream

    fun insert(broadcastStream: BroadcastStream)

    fun update(broadcastStream: BroadcastStream)

    fun delete(streamId: Long)

    /**
     * Deletes all streams for specified broadcast
     *
     * @param broadcastId
     */
    fun deleteByBroadcastId(broadcastId: Long)
}

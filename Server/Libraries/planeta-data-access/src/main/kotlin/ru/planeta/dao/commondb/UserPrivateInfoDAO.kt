package ru.planeta.dao.commondb

import org.apache.ibatis.annotations.Mapper
import org.apache.ibatis.annotations.Param
import ru.planeta.model.common.UserPrivateInfo
import ru.planeta.model.enums.UserStatus
import java.util.Date
import java.util.EnumSet

/**
 * DAO for UserPrivateInfo
 *
 * @author a.savanovich
 */
@Mapper
interface UserPrivateInfoDAO {

    fun selectByUserId(userId: Long): UserPrivateInfo?

    fun selectByEmail(email: String): UserPrivateInfo?

    fun selectByUsername(username: String): UserPrivateInfo?

    fun selectUsersCount(): Long

    fun selectByRegCode(regCode: String): UserPrivateInfo?

    fun selectByStatus(statuses: EnumSet<UserStatus>): List<UserPrivateInfo>

    fun insert(info: UserPrivateInfo)

    fun delete(userId: Long)

    fun update(info: UserPrivateInfo)

    /**
     * selects list of UserPrivateInfo without RegCode and gt;= dateFrom and lt; dateTo
     *
     * @param dateFrom
     * @param dateTo
     * @return
     */
    fun selectUserPrivateInfoNotNullRegCode(@Param("dateFrom") dateFrom: Date, @Param("dateTo") dateTo: Date, @Param("offset") offset: Int, @Param("limit") limit: Int): List<UserPrivateInfo>

    fun selectUserPrivateInfo(@Param("dateFrom") dateFrom: Date, @Param("dateTo") dateTo: Date, @Param("offset") offset: Int, @Param("limit") limit: Int): List<UserPrivateInfo>

    fun selectUsersPrivateInfo(userIds: Collection<Long>): List<UserPrivateInfo>

    fun selectSubscriberListToSpam(@Param("profileId") profileId: Long, @Param("offset") offset: Int, @Param("limit") limit: Int): List<UserPrivateInfo>
}

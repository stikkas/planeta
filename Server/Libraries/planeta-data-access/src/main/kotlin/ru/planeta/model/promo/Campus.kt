package ru.planeta.model.promo

import ru.planeta.model.common.campaign.Campaign

/**
 *
 * User: Serge Blagodatskih<stikkas17></stikkas17>@gmail.com><br></br>
 * Date: 22.11.16<br></br>
 * Time: 16:29
 */
class Campus {

    var id: Long = 0
    var name: String? = null
    var shortName: String? = null
    var imageUrl: String? = null
    var tagId: Int = 0
    var tagName: String? = null
    // Кол-во проектов
    var count: Long = 0
    var projects: List<Campaign>? = null

}

package ru.planeta.model.charity.enums

/*
 * Created by Alexey on 28.06.2016.
 */


import ru.planeta.model.enums.Codable

import java.util.HashMap

enum class CharityEventType private constructor(override val code: Int) : Codable {
    ABOUT(1), NEWS(2), PARTNERS_NEWS(3), SCHOOL(4);


    companion object {

        private val lookup = HashMap<Int, CharityEventType>()

        init {
            for (s in values()) {
                lookup[s.code] = s
            }
        }

        fun getByValue(code: Int): CharityEventType? {
            return lookup[code]
        }
    }
}

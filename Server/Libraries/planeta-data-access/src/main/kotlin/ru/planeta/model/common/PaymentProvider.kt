package ru.planeta.model.common

import java.math.BigDecimal

/**
 * Created with IntelliJ IDEA.
 * User: a.volkov
 * Date: 23.04.14
 * Time: 16:35
 */
class PaymentProvider : Identified {

    override var id: Long = 0
    var type: Type? = null
    var imageUrl: String? = null
    var name: String? = null
    var min: BigDecimal? = null
    var max: BigDecimal? = null
    var commission: BigDecimal? = null
    var monthlyLimit: BigDecimal? = null
    var url: String? = null
    var isDeferred: Boolean = false
    var isEmulated: Boolean = false
    var isInternal: Boolean = false
    var processorClass: String? = null
    var processorParams: String? = null

    enum class Type {
        PROMO_PLANETA, PLANETA, ROBOKASSA, DENGIONLINE, ALFABANK, QIWI, W1, YANDEX_MONEY, CHRONOPAY,
        BEST2PAY, RBK_MONEY, CLOUD_PAYMENTS, WEBMONEY, INVOICE, SBERBANK
    }
}

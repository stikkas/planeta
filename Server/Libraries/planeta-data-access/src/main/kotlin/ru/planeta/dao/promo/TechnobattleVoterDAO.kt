package ru.planeta.dao.promo

import org.apache.ibatis.annotations.Param
import ru.planeta.model.promo.TechnobattleVote
import ru.planeta.model.promo.VoteType

/**
 * Created with IntelliJ IDEA.
 * User: michail
 * Date: 16.01.2017
 * Time: 16:41
 */
interface TechnobattleVoterDAO {
    fun insert(vote: TechnobattleVote)
    fun selectVotesCountForUser(@Param("userId") userId: String, @Param("voteType") voteType: VoteType): Int
    fun selectVotesCountForProject(projectId: Long): Int
}

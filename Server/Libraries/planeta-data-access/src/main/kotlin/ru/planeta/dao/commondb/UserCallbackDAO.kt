package ru.planeta.dao.commondb

import org.apache.ibatis.annotations.Mapper
import org.apache.ibatis.annotations.Param
import ru.planeta.model.common.UserCallback
import ru.planeta.model.common.UserCallbackComment

import java.util.Date

/**
 * Created by eshevchenko on 30.01.15.
 */
@Mapper
interface UserCallbackDAO {

    /**
     * Inserts [ru.planeta.model.common.UserCallback].
     *
     * @param userCallback user's callback for insert;
     * @return inserted (with id field) [ru.planeta.model.common.UserCallback] instance;
     */
    fun insert(userCallback: UserCallback): Long

    /**
     * Selects user's callback requests according with filtration parameters.
     *
     * @param type      type of request;
     * @param processed is processed flag;
     * @param dateFrom  start of processing date filter;
     * @param dateTo    finish of processing date filter;
     * @param offset    selection offset;
     * @param limit     batch size;
     * @return list of applicable [ru.planeta.model.common.UserCallback];
     */
    fun selectUserCallbacks(@Param("type") type: UserCallback.Type, @Param("processed") processed: Boolean?, @Param("dateFrom") dateFrom: Date, @Param("dateTo") dateTo: Date, @Param("offset") offset: Int, @Param("limit") limit: Int): List<UserCallback>

    /**
     * Updates user's callback request to processed state.
     *
     * @param id        user's callback identifier;
     * @param managerId manager profile identifier;
     */
    fun updateProcessingStatus(@Param("id") id: Long, @Param("managerId") managerId: Long)

    /**
     * Selects user's callback request by identifier.
     *
     * @param id user's callback identifier;
     * @return founded callback, `null` if callback with presented identifier not exists.
     */
    fun selectById(id: Long): UserCallback

    fun selectCallbackComments(callbackId: Long): List<UserCallbackComment>

    fun insertCallbackComment(comment: UserCallbackComment)

    fun selectUserCallbacksByIds(callbackIds: List<Long>): List<UserCallback>
}

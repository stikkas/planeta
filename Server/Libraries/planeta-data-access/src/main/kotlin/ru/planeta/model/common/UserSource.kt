package ru.planeta.model.common

/**
 * Date: 06.09.12
 * On planeta first visit of some user hi gains cookies with its first visit planeta url and referer
 * On this user registration complete this info with its new profileId (named UserSource) is inserted into db to analyze later
 *
 * @author s.kalmykov
 */
class UserSource {

    var profileId: Long? = null
    var referrerUrl: String? = null
    var enterUrl: String? = null

    constructor(profileId: Long, referrerUrl: String, enterUrl: String) {
        this.profileId = profileId
        this.referrerUrl = referrerUrl
        this.enterUrl = enterUrl
    }

    /**
     * for iBatis
     */
    constructor() {
    }
}

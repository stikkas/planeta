package ru.planeta.model.enums

import java.util.EnumSet
import java.util.HashMap

/**
 * Enum GroupCategory
 *
 * @author a.tropnikov
 */
enum class GroupCategory(val code: Int) {

    MUSIC(1), VISUAL_ART(2), MOVIE(3), THEATER(4), PHOTOGRAPHY(5), LITERATURE(6),
    DESIGN(7), DANCE(8), BUSINESS(9), GAMING_INDUSTRY(10),
    TECHNOLOGY(11), SPORT(13), FOOD(14),
    CHARITY(15),
    OTHER(99);


    companion object {

        private val lookup = HashMap<Int, GroupCategory>()

        init {
            for (s in EnumSet.allOf(GroupCategory::class.java)) {
                lookup[s.code] = s
            }
        }

        fun getByValue(code: Int): GroupCategory? {
            return lookup[code]
        }
    }
}

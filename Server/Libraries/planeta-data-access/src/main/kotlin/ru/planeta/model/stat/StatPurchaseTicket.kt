package ru.planeta.model.stat

import ru.planeta.model.profile.Profile

class StatPurchaseTicket : StatPurchaseCommon() {
    //
    var eventId: Long = 0
    var eventProfile: Profile? = null

    override operator fun compareTo(o: Any): Int {
        val that: StatPurchaseTicket
        try {
            that = o as StatPurchaseTicket
        } catch (e: Exception) {
            return 1
        }

        var compare1 = 0
        val compare2 = 0
        var compare3 = 0
        val profile = this.eventProfile
        val otherProfile = that.eventProfile
        if (profile?.displayName == null) compare1 -= 1
        if (otherProfile?.displayName == null) compare1 += 1
        if (profile?.displayName != null && otherProfile?.displayName != null) {
            compare1 = profile.displayName?.compareTo(otherProfile.displayName ?: "") ?: 0
        }

        if (name == null) compare1 -= 1
        if (that.name == null) compare1 += 1
        if (name != null && that.name != null) {
            compare3 = name?.compareTo(that.name ?: "") ?: 0
        }

        if (compare1 != 0) return compare1
        if (compare2 != 0) return compare2
        return if (compare3 != 0) compare3 else 0
    }

    override fun equals(o: Any?): Boolean {
        if (this === o) return true
        if (javaClass != o?.javaClass) return false
        if (!super.equals(o)) return false

        o as StatPurchaseTicket

        if (eventId != o.eventId) return false

        return true
    }

    override fun hashCode(): Int {
        var result = super.hashCode()
        result = 31 * result + eventId.hashCode()
        return result
    }


}

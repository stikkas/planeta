package ru.planeta.model.common.campaign

import ru.planeta.model.news.ProfileNews
import ru.planeta.model.profile.Comment
import ru.planeta.model.profile.Post
import java.util.*

class CampaignNews {
    var profileNews: ProfileNews? = null
    var post: Post? = null
    var campaign: Campaign? = null
    var comment: Comment? = null

    companion object {

        fun getCampaignList(campaignNewsList: List<CampaignNews>): List<Campaign> {
            val campaignList = ArrayList<Campaign>(campaignNewsList.size)
            campaignNewsList.forEach {
                val cmp = it.campaign
                if (cmp != null) {
                    campaignList.add(cmp)
                }
            }
            return campaignList
        }
    }
}

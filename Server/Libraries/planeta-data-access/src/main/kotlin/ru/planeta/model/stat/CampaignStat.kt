package ru.planeta.model.stat

import com.fasterxml.jackson.databind.annotation.JsonSerialize
import ru.planeta.commons.json.ReadableDateFormatter
import ru.planeta.model.common.campaign.Campaign
import ru.planeta.model.common.campaign.Share

import java.math.BigDecimal
import java.util.ArrayList
import java.util.Date

/**
 * Created with IntelliJ IDEA.
 * User: a.volkov
 * Date: 01.09.2014
 * Time: 17:46
 */
class CampaignStat {
    @get:JsonSerialize(using = ReadableDateFormatter::class)
    var date: Date? = null
    var views: Int = 0
    var visitors: Int = 0
    var buyers: Int = 0
    var purchases: Int = 0
    var comments: Int = 0
    var affiliateId: Long = 0
    var affiliateName: String? = null
    var referer: String? = null
    var country: String? = null
    var city: String? = null
    var amount: BigDecimal? = null
    var campaignComments: Int = 0
    var newsComments: Int = 0
    var shares: List<Share> = ArrayList<Share>()
    var campaign: Campaign? = null
    var statisticUrl: String? = null
    var campaignUrl: String? = null
}

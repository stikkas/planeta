package ru.planeta.dao.commondb

import org.apache.ibatis.annotations.Param
import java.util.ArrayList
import org.springframework.beans.factory.annotation.Autowired
import ru.planeta.model.commondb.FaqCategory
import ru.planeta.model.commondb.FaqCategoryExample

abstract class BaseFaqCategoryService {
    @Autowired
    protected var faqCategoryMapper: FaqCategoryMapper? = null

    val orderByClause: String
        get() = "faq_category_id"

    //**************************************************************************************************
    //*********************************  Create ** Update ** Delete  *************************************
    //**************************************************************************************************
    fun insertFaqCategory(faqCategory: FaqCategory) {


        faqCategoryMapper!!.insertSelective(faqCategory)
    }

    fun insertFaqCategoryAllFields(faqCategory: FaqCategory) {


        faqCategoryMapper!!.insert(faqCategory)
    }

    fun updateFaqCategory(faqCategory: FaqCategory) {

        faqCategoryMapper!!.updateByPrimaryKeySelective(faqCategory)
    }

    fun updateFaqCategoryAllFields(faqCategory: FaqCategory) {

        faqCategoryMapper!!.updateByPrimaryKey(faqCategory)
    }

    fun insertOrUpdateFaqCategory(faqCategory: FaqCategory) {

        val selectedFaqCategory = faqCategoryMapper!!.selectByPrimaryKey(faqCategory.faqCategoryId)
        if (selectedFaqCategory == null) {

            faqCategoryMapper!!.insertSelective(faqCategory)
        } else {
            faqCategoryMapper!!.updateByPrimaryKeySelective(faqCategory)
        }
    }

    fun insertOrUpdateFaqCategoryAllFields(faqCategory: FaqCategory) {

        val selectedFaqCategory = faqCategoryMapper!!.selectByPrimaryKey(faqCategory.faqCategoryId)
        if (selectedFaqCategory == null) {

            faqCategoryMapper!!.insert(faqCategory)
        } else {
            faqCategoryMapper!!.updateByPrimaryKey(faqCategory)
        }
    }

    fun deleteFaqCategory(id: Long?) {
        faqCategoryMapper!!.deleteByPrimaryKey(id)
    }

    //**************************************************************************************************
    //***************************************** faq_category *********************************************
    //************************************** SelectByPrimaryKey ******************************************
    //**************************************************************************************************
    fun selectFaqCategory(faqCategoryId: Long?): FaqCategory {
        return faqCategoryMapper!!.selectByPrimaryKey(faqCategoryId)
    }

    //**************************************************************************************************
    //***************************************** faq_category *********************************************
    //**************************************************************************************************
    private fun COMMENT_SECTION_FaqCategory() {

    }

    fun getFaqCategoryExample(@Param("offset") offset: Int, @Param("limit") limit: Int): FaqCategoryExample {
        val example = FaqCategoryExample()
        example.offset = offset
        example.limit = limit
        example.or()
        example.orderByClause = orderByClause
        return example
    }

    fun selectFaqCategoryCount(): Int {
        return faqCategoryMapper!!.countByExample(getFaqCategoryExample(0, 0))
    }

    fun selectFaqCategoryList(@Param("offset") offset: Int, @Param("limit") limit: Int): List<FaqCategory> {
        return faqCategoryMapper!!.selectByExample(getFaqCategoryExample(offset, limit))
    }

    fun selectFaqCategoryList(faqCategoryIdList: List<Long>?): List<FaqCategory> {
        return if (faqCategoryIdList == null || faqCategoryIdList.isEmpty()) ArrayList() else faqCategoryMapper!!.selectByIdList(faqCategoryIdList)
    }
}

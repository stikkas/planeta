package ru.planeta.dto

import ru.planeta.entity.AbstractTimestampEntity

class ProfileSitesDTO : AbstractTimestampEntity() {

    /**
     * Id профиля пользователя, которому принадлежат контакты
     */
    var profileId: Long = 0

    /**
     * Адрес сайта пользователя
     */
    var siteUrl: String = ""
        set(value) {
            value.trim { it <= ' ' }
        }

    /**
     * Адрес Вконтакте пользователя
     */
    var vkUrl: String = ""
        set(value) {
            value.trim { it <= ' ' }
        }

    /**
     * Адрес в Facebook пользователя
     */
    var facebookUrl: String = ""
        set(value) {
            value.trim { it <= ' ' }
        }

    /**
     * Адрес в Twitter пользователя
     */
    var twitterUrl: String = ""
        set(value) {
            value.trim { it <= ' ' }
        }

    /**
     * Адрес в Google+ пользователя
     */
    var googleUrl: String = ""
        set(value) {
            value.trim { it <= ' ' }
        }
}

package ru.planeta.model.common

import java.util.HashMap

/**
 * Enum that represents services, for sharing content with
 *
 *
 * Note: Keep this enum in sync with constants in `share-widgets.js` file
 *
 * User: m.shulepov
 */
enum class ShareServiceType private constructor(val code: Int, val url: String) {

    FACEBOOK(1, "https://www.facebook.com/sharer/sharer.php?u={url}"),
    VKONTAKTE(2, "https://vk.com/share.php?url={url}"),
    TWITTER(3, "https://twitter.com/intent/tweet?url={url}&text={title}"),
    ODNOKLASSNIKI(4, "https://www.odnoklassniki.ru/dk?st.cmd=addShare&st._surl={url}"),
    GOOGLE_PLUS(5, "https://plus.google.com/share?url={url}&hl=ru"),
    YANDEX(6, "https://my.ya.ru/posts_add_link.xml?URL={url}&title={title}"),
    MOIMIR(7, "https://connect.mail.ru/share?url={url}&title={title}"),
    SURFINGBIRD(8, "https://surfingbird.ru/share?url={url}"),
    TELEGRAM(9, "https://t.me/share/url?url={url}");


    companion object {

        private val lookup = HashMap<Int, ShareServiceType>()

        init {
            for (s in values()) {
                lookup[s.code] = s
            }
        }

        fun getByValue(code: Int): ShareServiceType? {
            return lookup[code]
        }
    }

}

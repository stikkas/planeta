package ru.planeta.model.enums

import java.util.EnumSet
import java.util.HashMap

/**
 * Communication channels
 *
 * @author ds.kolyshev
 * Date: 09.11.11
 */
enum class CommunicationChannelType(override val code: Int) : Codable {

    GOOGLE(1),
    YANDEX(2),
    MAILRU(3),
    RAMBLER(4),
    VKONTAKTE(5),
    FACEBOOK(6),
    TWITTER(7),
    LIVEJOURNAL(8),
    BLOGGER(9),
    LASTFM(10),
    FLICKR(11),
    YOUTUBE(12),
    VIMEO(13),
    STEAM(14),
    SKYPE(15),
    ICQ(16),
    JABBER(17),
    PHONE(18),
    TUMBLR(19),
    GOOGLE_PLUS(20),
    SOUNDCLOUD(21),
    WORDPRESS(22),
    MAILRU_BLOGS(23);


    companion object {

        private val lookup = HashMap<Int, CommunicationChannelType>()

        init {
            for (s in EnumSet.allOf(CommunicationChannelType::class.java)) {
                lookup[s.code] = s
            }
        }

        fun getByValue(code: Int): CommunicationChannelType? {
            return lookup[code]
        }
    }
}

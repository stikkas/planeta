package ru.planeta.model.common.auth

/**
 * Specifies user from external system (social network)
 */
class ExternalAuthentication {

    var credentialType: CredentialType? = null
    var username: String? = null

    constructor(username: String, credentialType: CredentialType) {
        this.credentialType = credentialType
        this.username = username
    }

    @Deprecated("")
    constructor() {
    }
}

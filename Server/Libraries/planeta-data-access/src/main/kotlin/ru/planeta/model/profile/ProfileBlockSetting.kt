package ru.planeta.model.profile

import ru.planeta.model.enums.BlockSettingType
import ru.planeta.model.enums.PermissionLevel

/**
 * Class ProfileBlockSetting
 *
 * @author a.tropnikov
 */
class ProfileBlockSetting : IProfileObject {

    override var profileId: Long? = 0
    var blockSettingType: BlockSettingType? = null
    var viewPermission = PermissionLevel.EVERYBODY
    var votePermission = PermissionLevel.EVERYBODY
    var commentPermission = PermissionLevel.ADMINS
    var writePermission = PermissionLevel.ADMINS
    var addPermission = PermissionLevel.ADMINS

    var blockSettingTypeCode: Int
        get() = blockSettingType!!.code
        set(code) {
            this.blockSettingType = BlockSettingType.Companion.getByValue(code)
        }

    var viewPermissionCode: Int
        get() = viewPermission.code
        set(code) {
            this.viewPermission = PermissionLevel.Companion.getByValue(code)
        }

    var commentPermissionCode: Int
        get() = commentPermission.code
        set(code) {
            this.commentPermission = PermissionLevel.Companion.getByValue(code)
        }

    var votePermissionCode: Int
        get() = votePermission.code
        set(code) {
            this.votePermission = PermissionLevel.Companion.getByValue(code)
        }

    var writePermissionCode: Int
        get() = writePermission.code
        set(code) {
            this.writePermission = PermissionLevel.Companion.getByValue(code)
        }

    var addPermissionCode: Int
        get() = addPermission.code
        set(code) {
            this.addPermission = PermissionLevel.Companion.getByValue(code)
        }
}

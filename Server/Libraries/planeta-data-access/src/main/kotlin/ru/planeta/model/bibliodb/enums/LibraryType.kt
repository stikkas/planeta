package ru.planeta.model.bibliodb.enums

import java.util.HashMap
import ru.planeta.model.enums.Codable

/**
 * Тип библиотеки
 * User: Serge Blagodatskih<stikkas17></stikkas17>@gmail.com><br></br>
 * Date: 27.04.16<br></br>
 * Time: 12:33
 */
enum class LibraryType private constructor(override val code: Int) : Codable {
    PUBLIC(1), SCHOLASTIC(2), SCIENTIFIC(3), DOMESTIC(4), SPECIAL(5);


    companion object {

        private val lookup = HashMap<Int, LibraryType>()

        init {
            for (s in values()) {
                lookup[s.code] = s
            }
        }

        fun getByValue(code: Int): LibraryType {
            return lookup[code] ?: PUBLIC
        }
    }
}

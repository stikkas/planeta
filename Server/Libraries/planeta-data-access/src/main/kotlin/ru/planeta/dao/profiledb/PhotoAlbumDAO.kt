package ru.planeta.dao.profiledb

import org.apache.ibatis.annotations.Mapper
import org.apache.ibatis.annotations.Param
import ru.planeta.model.profile.media.PhotoAlbum

/**
 * @author ds.kolyshev
 * Date: 27.10.11
 */
@Mapper
interface PhotoAlbumDAO {

    /**
     * Selects photo album by its identifier
     *
     * @param profileId
     * @param albumId
     * @return
     */
    fun selectAlbumById(@Param("profileId") profileId: Long, @Param("albumId") albumId: Long): PhotoAlbum?

    /**
     * Selects photo hidden album by it's type
     *
     * @param profileId
     * @param albumTypeId
     * @return
     */
    fun selectAlbumByType(@Param("profileId") profileId: Long, @Param("albumTypeId") albumTypeId: Int): PhotoAlbum

    /**
     * Inserts photo album
     *
     * @param photoAlbum
     */
    fun insert(photoAlbum: PhotoAlbum)

    /**
     * Updates photo album
     *
     * @param photoAlbum
     */
    fun update(photoAlbum: PhotoAlbum)

    /**
     * Deletes specified photo album
     *
     * @param profileId
     * @param albumId
     */
    fun delete(@Param("profileId") profileId: Long, @Param("albumId") albumId: Long)

}

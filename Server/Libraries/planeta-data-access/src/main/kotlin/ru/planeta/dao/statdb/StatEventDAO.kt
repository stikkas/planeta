package ru.planeta.dao.statdb

import ru.planeta.model.stat.StatEvent

/**
 * Created with IntelliJ IDEA.
 * User: a.volkov
 * Date: 01.08.14
 * Time: 18:35
 */
interface StatEventDAO {

    /**
     * insert raw event data to events table
     *
     * @param event
     */
    fun insert(event: StatEvent)

    /**
     * deleteByProfileId aggregated records
     */
    fun cleanAggregatedRecords()
}

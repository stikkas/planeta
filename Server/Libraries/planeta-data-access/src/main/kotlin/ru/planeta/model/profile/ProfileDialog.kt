package ru.planeta.model.profile

import java.util.Date

/**
 * Represents a dialog link (from user to dialog).
 * profileId -- identifier of the dialog participant.
 *
 * @author ameshkov
 */
class ProfileDialog : IProfileObject {

    override var profileId: Long? = 0
    var companionProfileId: Long = 0
    var dialogId: Long = 0
    var unreadMessagesCount: Int = 0
    var isHasMessages: Boolean = false
    var timeAdded: Date? = null
    var timeUpdated: Date? = null
    var firstUnreadMessageId: Long = 0
}

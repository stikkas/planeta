package ru.planeta.dao.msgdb

import org.apache.ibatis.annotations.Mapper
import org.apache.ibatis.annotations.Param
import ru.planeta.model.msg.DialogMessage

/**
 * Data access object for working with dialog messages dao
 *
 * @author ameshkov
 */
@Mapper
interface DialogMessageDAO {

    /**
     * Selects specified dialog message
     *
     * @param dialogId
     * @param messageId
     * @return
     */
    fun selectMessage(@Param("dialogId") dialogId: Long, @Param("messageId") messageId: Long): DialogMessage

    /**
     * Selects dialog messages for the specified dialog (starting from the last message)
     *
     * @param dialogId
     * @param offset
     * @param limit
     * @return
     */
    fun selectMessages(@Param("dialogId") dialogId: Long, @Param("offset") offset: Int, @Param("limit") limit: Int): List<DialogMessage>

    /**
     * Selects dialog messages starting from the specified "startMessageId" (exclusive)
     *
     * @param dialogId
     * @param startMessageId
     * @return
     */
    fun selectLastMessages(@Param("dialogId") dialogId: Long, @Param("startMessageId") startMessageId: Long): List<DialogMessage>

    /**
     * Selects dialog messages starting from the specified "startMessageId" (exclusive)
     *
     * @param limit
     * @param offset
     * @return
     */
    fun selectSpammers(@Param("dialogId") threshold: Long, @Param("limit") limit: Int, @Param("offset") offset: Int): List<Long>

    /**
     * Inserts dialog message to the dialog
     *
     * @param dialogMessage
     */
    fun insert(dialogMessage: DialogMessage)

    /**
     * Updates dialog message. Updates only deleted flag.
     *
     * @param dialogMessage
     */
    fun update(dialogMessage: DialogMessage)

    /**
     * Selecting last not deleted dialog message
     *
     * @param dialogId
     * @return
     */
    fun selectLastMessage(dialogId: Long): DialogMessage

    /**
     * Selecting last not deleted dialog messageId
     *
     * @param dialogId
     * @return
     */
    fun selectLastMessageId(dialogId: Long): Long?
}

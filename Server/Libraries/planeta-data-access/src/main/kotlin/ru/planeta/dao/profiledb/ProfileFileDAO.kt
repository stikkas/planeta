package ru.planeta.dao.profiledb

import org.apache.ibatis.annotations.Mapper
import org.apache.ibatis.annotations.Param
import ru.planeta.model.profile.ProfileFile

/**
 * @author: ds.kolyshev
 * Date: 17.03.13
 */
@Mapper
interface ProfileFileDAO {

    /**
     * Selects list of profile files
     *
     * @param profileId
     * @param offset
     * @param limit
     * @return
     */
    fun selectList(@Param("profileId") profileId: Long, @Param("offset") offset: Int, @Param("limit") limit: Int): List<ProfileFile>

    /**
     * Select profile's file by identifier
     *
     * @param profileId
     * @param fileId
     * @return
     */
    fun selectById(@Param("profileId") profileId: Long, @Param("fileId") fileId: Long): ProfileFile

    /**
     * Inserts profile's file
     *
     * @param profileFile
     */
    fun insert(profileFile: ProfileFile): ProfileFile

    fun updateFileTitleAndDescription(profileFile: ProfileFile)

    /**
     * Deletes profile's file
     *
     * @param profileId
     * @param fileId
     */
    fun delete(@Param("profileId") profileId: Long, @Param("fileId") fileId: Long)
}

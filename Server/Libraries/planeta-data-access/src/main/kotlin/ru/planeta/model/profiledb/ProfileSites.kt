package ru.planeta.model.profiledb

import java.util.Date

class ProfileSites {
    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column profiledb.profile_sites.profile_id
     *
     * @mbggenerated Tue May 29 12:53:11 MSK 2018
     */
    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column profiledb.profile_sites.profile_id
     *
     * @return the value of profiledb.profile_sites.profile_id
     *
     * @mbggenerated Tue May 29 12:53:11 MSK 2018
     */
    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column profiledb.profile_sites.profile_id
     *
     * @param profileId the value for profiledb.profile_sites.profile_id
     *
     * @mbggenerated Tue May 29 12:53:11 MSK 2018
     */
    var profileId: Long? = null

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column profiledb.profile_sites.site_url
     *
     * @mbggenerated Tue May 29 12:53:11 MSK 2018
     */
    var siteUrl: String? = null
        set(siteUrl) {
            field = siteUrl?.trim { it <= ' ' }
        }
    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column profiledb.profile_sites.vk_url
     *
     * @mbggenerated Tue May 29 12:53:11 MSK 2018
     */
    var vkUrl: String? = null
        set(vkUrl) {
            field = vkUrl?.trim { it <= ' ' }
        }

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column profiledb.profile_sites.facebook_url
     *
     * @mbggenerated Tue May 29 12:53:11 MSK 2018
     */
    var facebookUrl: String? = null
        set(facebookUrl) {
            field = facebookUrl?.trim { it <= ' ' }
        }


    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column profiledb.profile_sites.twitter_url
     *
     * @mbggenerated Tue May 29 12:53:11 MSK 2018
     */
    var twitterUrl: String? = null
        set(twitterUrl) {
            field = twitterUrl?.trim { it <= ' ' }
        }

    var googleUrl: String? = null
        set(googleUrl) {
            field = googleUrl?.trim { it <= ' ' }
        }

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column profiledb.profile_sites.time_added
     *
     * @mbggenerated Tue May 29 12:53:11 MSK 2018
     */
    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column profiledb.profile_sites.time_added
     *
     * @return the value of profiledb.profile_sites.time_added
     *
     * @mbggenerated Tue May 29 12:53:11 MSK 2018
     */
    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column profiledb.profile_sites.time_added
     *
     * @param timeAdded the value for profiledb.profile_sites.time_added
     *
     * @mbggenerated Tue May 29 12:53:11 MSK 2018
     */
    var timeAdded: Date? = null

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column profiledb.profile_sites.time_updated
     *
     * @mbggenerated Tue May 29 12:53:11 MSK 2018
     */
    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column profiledb.profile_sites.time_updated
     *
     * @return the value of profiledb.profile_sites.time_updated
     *
     * @mbggenerated Tue May 29 12:53:11 MSK 2018
     */
    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column profiledb.profile_sites.time_updated
     *
     * @param timeUpdated the value for profiledb.profile_sites.time_updated
     *
     * @mbggenerated Tue May 29 12:53:11 MSK 2018
     */
    var timeUpdated: Date? = null


    /**
     * This class was generated by MyBatis Generator.
     * This class corresponds to the database table profiledb.profile_sites
     *
     * @mbggenerated Tue May 29 12:53:11 MSK 2018
     */
    class Builder {
        private val obj: ProfileSites

        init {
            this.obj = ProfileSites()
        }

        /**
         * This method was generated by MyBatis Generator.
         * This method corresponds to the database table profiledb.profile_sites
         *
         * @mbggenerated Tue May 29 12:53:11 MSK 2018
         */
        fun profileId(profileId: Long?): Builder {
            obj.profileId = profileId
            return this
        }

        /**
         * This method was generated by MyBatis Generator.
         * This method corresponds to the database table profiledb.profile_sites
         *
         * @mbggenerated Tue May 29 12:53:11 MSK 2018
         */
        fun siteUrl(siteUrl: String): Builder {
            obj.siteUrl = siteUrl
            return this
        }

        /**
         * This method was generated by MyBatis Generator.
         * This method corresponds to the database table profiledb.profile_sites
         *
         * @mbggenerated Tue May 29 12:53:11 MSK 2018
         */
        fun vkUrl(vkUrl: String): Builder {
            obj.vkUrl = vkUrl
            return this
        }

        /**
         * This method was generated by MyBatis Generator.
         * This method corresponds to the database table profiledb.profile_sites
         *
         * @mbggenerated Tue May 29 12:53:11 MSK 2018
         */
        fun facebookUrl(facebookUrl: String): Builder {
            obj.facebookUrl = facebookUrl
            return this
        }

        /**
         * This method was generated by MyBatis Generator.
         * This method corresponds to the database table profiledb.profile_sites
         *
         * @mbggenerated Tue May 29 12:53:11 MSK 2018
         */
        fun twitterUrl(twitterUrl: String): Builder {
            obj.twitterUrl = twitterUrl
            return this
        }

        /**
         * This method was generated by MyBatis Generator.
         * This method corresponds to the database table profiledb.profile_sites
         *
         * @mbggenerated Tue May 29 12:53:11 MSK 2018
         */
        fun googleUrl(googleUrl: String): Builder {
            obj.googleUrl = googleUrl
            return this
        }

        /**
         * This method was generated by MyBatis Generator.
         * This method corresponds to the database table profiledb.profile_sites
         *
         * @mbggenerated Tue May 29 12:53:11 MSK 2018
         */
        fun timeAdded(timeAdded: Date): Builder {
            obj.timeAdded = timeAdded
            return this
        }

        /**
         * This method was generated by MyBatis Generator.
         * This method corresponds to the database table profiledb.profile_sites
         *
         * @mbggenerated Tue May 29 12:53:11 MSK 2018
         */
        fun timeUpdated(timeUpdated: Date): Builder {
            obj.timeUpdated = timeUpdated
            return this
        }

        /**
         * This method was generated by MyBatis Generator.
         * This method corresponds to the database table profiledb.profile_sites
         *
         * @mbggenerated Tue May 29 12:53:11 MSK 2018
         */
        fun build(): ProfileSites {
            return this.obj
        }
    }

    companion object {

        /**
         * This method was generated by MyBatis Generator.
         * This method corresponds to the database table profiledb.profile_sites
         *
         * @mbggenerated Tue May 29 12:53:11 MSK 2018
         */
        val builder: Builder
            get() = Builder()
    }
}

package ru.planeta.dao.concertdb

import org.apache.ibatis.annotations.Mapper
import org.apache.ibatis.annotations.Param
import ru.planeta.model.concert.Section

/**
 * Created with IntelliJ IDEA.
 * User: michail
 * Date: 14.10.16
 * Time: 17:05
 */
@Mapper
interface SectionDAO {
    fun insert(section: Section): Int

    fun update(section: Section): Int

    fun select(sectionId: Long): Section

    fun selectByExtId(externalSectionId: Long): Section

    fun selectByExtConcertId(externalConcertId: Long): List<Section>

    fun selectSectorsByExtConcertId(externalConcertId: Long): List<Section>

    fun selectRowsByExtConcertId(@Param("externalConcertId") externalConcertId: Long, @Param("externalParentId") externalParentId: Long?): List<Section>
}

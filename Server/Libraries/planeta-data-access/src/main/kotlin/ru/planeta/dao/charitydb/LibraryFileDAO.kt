package ru.planeta.dao.charitydb

import org.apache.ibatis.annotations.Mapper
import org.apache.ibatis.annotations.Param
import ru.planeta.model.charity.LibraryFile
import ru.planeta.model.charity.LibraryFileExample


@Mapper
interface LibraryFileDAO {
    fun countByExample(example: LibraryFileExample): Int

    fun deleteByExample(example: LibraryFileExample): Int

    fun deleteByPrimaryKey(libraryFileId: Long?): Int

    fun insert(record: LibraryFile): Int

    fun insertSelective(record: LibraryFile): Int

    fun selectByExample(example: LibraryFileExample): List<LibraryFile>

    fun selectByPrimaryKey(libraryFileId: Long?): LibraryFile

    fun updateByExampleSelective(@Param("record") record: LibraryFile, @Param("example") example: LibraryFileExample): Int

    fun updateByExample(@Param("record") record: LibraryFile, @Param("example") example: LibraryFileExample): Int

    fun updateByPrimaryKeySelective(record: LibraryFile): Int

    fun updateByPrimaryKey(record: LibraryFile): Int

    fun selectByIdList(libraryFileIdList: List<Long>): List<LibraryFile>
}

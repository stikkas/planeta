package ru.planeta.model.common.campaign

import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import ru.planeta.model.common.campaign.enums.CampaignStatus
import ru.planeta.model.common.campaign.enums.CampaignTargetStatus
import ru.planeta.model.enums.ObjectType
import ru.planeta.model.profile.VotableObject
import java.math.BigDecimal
import java.math.RoundingMode
import java.util.*

/**
 * Represents fund campaign
 *
 *
 * Created by IntelliJ IDEA.
 * User: atropnikov
 * Date: 21.03.12
 * Time: 12:24
 */
@JsonIgnoreProperties(ignoreUnknown = true)
open class Campaign : VotableObject {
    @get:JsonIgnore
    var updaterProfileId: Long = 0
    var name: String? = null
    var nameHtml: String? = null
    var albumId: Long = 0
    var imageId: Long = 0
    var imageUrl: String? = null
    var viewImageId: Long = 0
    var viewImageUrl: String? = null
    var videoProfileId: Long = 0
    var videoId: Long = 0
    var videoUrl: String? = null
    var description: String? = null
    var descriptionHtml: String? = null
    var shortDescription: String? = null
        set(shortDescription) {
            field = shortDescription
            shortDescriptionHtml = shortDescription
        }
    var shortDescriptionHtml: String? = null
    var timeStart: Date? = null
    var timeFinish: Date? = null
    var targetAmount: BigDecimal? = BigDecimal.ZERO
        set(targetAmount) {
            if (targetAmount == null) {
                this.targetAmount = BigDecimal.ZERO
            } else {
                field = targetAmount
            }
        }
    // Contains the project collected amount (synchronizes with purchase_sum), and not updated if campaign failed..
    var collectedAmount = BigDecimal.ZERO
    var tags = mutableListOf<CampaignTag>()
        get() {
            if (field == null) {
                field = mutableListOf()
            }
            return field
        }
        set(tags) {
            this.tags.clear()
            for (tag in tags)
                addTag(tag)
        }

    var purchaseSum = BigDecimal.ZERO
    var purchaseCount: Int = 0
    var status: CampaignStatus? = CampaignStatus.DRAFT
    var targetStatus = CampaignTargetStatus.NONE
    var timeAdded: Date? = null
    var timeUpdated: Date? = null
    var shares: MutableList<ShareEditDetails> = ArrayList()
    //fields for comment support
    var commentsCount: Int = 0
    override var objectType: ObjectType? = ObjectType.CAMPAIGN
    var timeLastPurchased: Date? = null
    var isCanMakeVipOffer: Boolean = false

    var timeLastNewsPublished: Date? = null

    var isPlanetaDelivery = true
    var creatorProfileId: Long = 0
        set(creatorProfileId) {
            field = creatorProfileId
            authorProfileId = creatorProfileId
        }
    var campaignAlias: String? = null
    var metaData: String? = null
    var sponsorAlias: String? = null
    var sponsorMultiplier: Int? = null

    var backgroundImageUrl: String? = null
    var isDraftVisible: Boolean = false

    open var videoContent: Any? = null

    var countryId: Long = 0
    var countryNameRus: String? = null
    var countryNameEng: String? = null
    var cityId: Long = 0
    var cityNameRus: String? = null
    var cityNameEng: String? = null
    var regionId: Long = 0
    var regionNameRus: String? = null
    var regionNameEng: String? = null

    var campaignId: Long
        get() = voteObjectId
        set(campaignId) {
            voteObjectId = campaignId
        }

    override var voteObjectType: ObjectType?
        get() = ObjectType.CAMPAIGN
        set(value: ObjectType?) {
            super.voteObjectType = value
        }

    override var objectId: Long? = null
        get() = campaignId

    override var profileId: Long? = null


    val isIgnoreTargetAmount: Boolean
        get() = BigDecimal.ZERO.compareTo(targetAmount!!) == 0

    val isFinishOnTargetReach: Boolean
        get() = timeFinish == null

    val mainTag: CampaignTag?
        get() = if (tags.isEmpty()) null else tags[0]

    var statusCode: Int
        get() = if (status != null) status!!.code else 0
        set(code) {
            this.status = CampaignStatus.getByValue(code)
        }

    var targetStatusCode: Int
        get() = targetStatus.code
        set(code) {
            this.targetStatus = CampaignTargetStatus.getByValue(code)
        }

    /**
     * Calculates completion percentage of campaign;
     * Returns 0, if `targetAmount == null` or `targetAmount == 0`
     *
     * @return completion percentage of campaign
     */
    val completionPercentage: Double
        get() = if (targetAmount == null || targetAmount!!.compareTo(BigDecimal.ZERO) == 0) 0.0 else purchaseSum.divide(targetAmount, 2, RoundingMode.HALF_UP).toDouble() * 100

    val isDraft: Boolean
        get() = status == null || status!!.isDraft

    val webCampaignAlias: String?
        get() = if (campaignAlias != null) campaignAlias else campaignId.toString()

    val isCharity: Boolean
        get() = containsTag(CampaignTag.CHARITY)

    val isInvesting: Boolean
        get() = containsTag(CampaignTag.INVESTING)

    constructor() {
        this.targetAmount = BigDecimal.ZERO
        this.collectedAmount = BigDecimal.ZERO
        this.purchaseSum = BigDecimal.ZERO
        this.status = CampaignStatus.DRAFT
        this.targetStatus = CampaignTargetStatus.NONE
        this.objectType = ObjectType.CAMPAIGN
    }

    constructor(campaign: Campaign) {
        copyAllFields(campaign, this)
    }


    fun addTag(tag: CampaignTag?) {
        if (tag != null) {
            if (tag.mnemonicName == CampaignTag.CHARITY) {
                tags.add(0, tag)
            } else {
                tags.add(tag)
            }
        }
    }

    fun setSpecificFields(campaign: Campaign) {
        this.status = campaign.status
    }

    @JsonIgnore
    @Deprecated("")
    fun addShare(share: ShareEditDetails) {
        if (!shares.contains(share)) {
            shares.add(share)
        }
    }

    private fun containsTag(tagName: String): Boolean {
        if (tags != null) {
            for (tag in tags!!) {
                if (tagName.equals(tag.mnemonicName!!, ignoreCase = true)) {
                    return true
                }
            }
        }
        return false
    }

    fun canChange(): Boolean {
        return status == null || EnumSet.of(CampaignStatus.PATCH, CampaignStatus.NOT_STARTED, CampaignStatus.DRAFT).contains(status)
    }

    companion object {

        fun copyAllFields(campaignFrom: Campaign, campaignTo: Campaign) {
            campaignTo.campaignId = campaignFrom.campaignId
            campaignTo.profileId = campaignFrom.profileId
            campaignTo.name = campaignFrom.name
            campaignTo.nameHtml = campaignFrom.nameHtml
            campaignTo.albumId = campaignFrom.albumId
            campaignTo.imageId = campaignFrom.imageId
            campaignTo.imageUrl = campaignFrom.imageUrl
            campaignTo.viewImageId = campaignFrom.viewImageId
            campaignTo.viewImageUrl = campaignFrom.viewImageUrl
            campaignTo.videoProfileId = campaignFrom.videoProfileId
            campaignTo.videoId = campaignFrom.videoId
            campaignTo.videoUrl = campaignFrom.videoUrl
            campaignTo.description = campaignFrom.description
            campaignTo.descriptionHtml = campaignFrom.descriptionHtml
            campaignTo.shortDescription = campaignFrom.shortDescription
            campaignTo.shortDescriptionHtml = campaignFrom.shortDescriptionHtml
            campaignTo.timeStart = campaignFrom.timeStart
            campaignTo.timeFinish = campaignFrom.timeFinish
            campaignTo.targetAmount = campaignFrom.targetAmount
            campaignTo.collectedAmount = campaignFrom.collectedAmount
            campaignTo.tags = campaignFrom.tags
            campaignTo.purchaseSum = campaignFrom.purchaseSum
            campaignTo.purchaseCount = campaignFrom.purchaseCount
            campaignTo.status = campaignFrom.status
            campaignTo.targetStatus = campaignFrom.targetStatus
            campaignTo.timeAdded = campaignFrom.timeAdded
            campaignTo.timeUpdated = campaignFrom.timeUpdated
            campaignTo.shares = campaignFrom.shares
            campaignTo.commentsCount = campaignFrom.commentsCount
            campaignTo.objectType = campaignFrom.objectType
            campaignTo.timeLastPurchased = campaignFrom.timeLastPurchased
            campaignTo.creatorProfileId = campaignFrom.creatorProfileId
            campaignTo.isCanMakeVipOffer = campaignFrom.isCanMakeVipOffer
            campaignTo.campaignAlias = campaignFrom.campaignAlias
            campaignTo.isPlanetaDelivery = campaignFrom.isPlanetaDelivery
            campaignTo.sponsorAlias = campaignFrom.sponsorAlias
            campaignTo.sponsorMultiplier = campaignFrom.sponsorMultiplier
            campaignTo.metaData = campaignFrom.metaData
            campaignTo.backgroundImageUrl = campaignFrom.backgroundImageUrl
            campaignTo.isDraftVisible = campaignFrom.isDraftVisible
            campaignTo.countryId = campaignFrom.countryId
            campaignTo.countryNameRus = campaignFrom.countryNameRus
            campaignTo.countryNameEng = campaignFrom.countryNameEng
            campaignTo.cityId = campaignFrom.cityId
            campaignTo.cityNameRus = campaignFrom.cityNameRus
            campaignTo.cityNameEng = campaignFrom.cityNameEng
            campaignTo.regionId = campaignFrom.regionId
            campaignTo.regionNameRus = campaignFrom.regionNameRus
            campaignTo.regionNameEng = campaignFrom.regionNameEng
        }

        fun <C : Campaign, T : Campaign> copyAllFields(listFrom: List<C>, listTo: List<T>): List<T> {
            for (campaignFrom in listFrom) {
                for (campaignTo in listTo) {
                    if (campaignFrom.id == campaignTo.id) {
                        Campaign.copyAllFields(campaignFrom, campaignTo)
                        break
                    }
                }
            }

            return listTo
        }
    }
}

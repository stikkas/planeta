package ru.planeta.dao.promo

import ru.planeta.model.promo.Campus

/**
 * Created by alexa_000 on 12.03.2015.
 */
interface CampusDAO {

    fun selectAll(): List<Campus>

    fun insert(campus: Campus)

    fun select(campusId: Long): Campus

    fun update(campus: Campus)
}

package ru.planeta.dao.commondb

import org.apache.ibatis.annotations.Mapper
import java.math.BigDecimal

/**
 * Created with IntelliJ IDEA.
 * User: michail
 * Date: 05.10.16
 * Time: 14:09
 */
@Mapper
interface BiblioOrderStatsDAO {
    fun purchasedBooksCount(): Long

    fun purchasedBooksSum(): BigDecimal
}

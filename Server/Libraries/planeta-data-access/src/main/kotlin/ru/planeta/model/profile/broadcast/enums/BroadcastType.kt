package ru.planeta.model.profile.broadcast.enums

import java.util.HashMap

/**
 * Broadcast type
 *
 * @author ds.kolyshev
 * Date: 16.03.12
 */
enum class BroadcastType private constructor(val code: Int) {
    VIDEO(0), AUDIO(1), EMBED(2);


    companion object {

        private val lookup = HashMap<Int, BroadcastType>()

        init {
            for (s in values()) {
                lookup[s.code] = s
            }
        }

        fun getByValue(code: Int?): BroadcastType? {
            return lookup[code]
        }
    }
}

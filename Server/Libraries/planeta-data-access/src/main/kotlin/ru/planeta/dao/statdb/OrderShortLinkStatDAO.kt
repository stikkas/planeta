package ru.planeta.dao.statdb

import org.apache.ibatis.annotations.Mapper
import ru.planeta.commons.model.OrderShortLinkStat

@Mapper
interface OrderShortLinkStatDAO {
    fun insert(orderShortLinkStat: OrderShortLinkStat)

    fun selectById(shortLinkStatsId: Long): OrderShortLinkStat
}

package ru.planeta.dao.profiledb

import org.apache.ibatis.annotations.Mapper
import ru.planeta.model.profile.broadcast.BroadcastProductTargeting

@Mapper
interface BroadcastProductTargetingDAO {

    fun insert(broadcastProductTargeting: BroadcastProductTargeting)

    fun delete(broadcastProductTargeting: BroadcastProductTargeting)

    fun selectBroadcastProductTargetings(broadcastId: Long): List<BroadcastProductTargeting>
}

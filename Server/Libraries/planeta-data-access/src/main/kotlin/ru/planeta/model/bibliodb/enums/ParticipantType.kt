package ru.planeta.model.bibliodb.enums

import java.util.HashMap
import ru.planeta.model.enums.Codable

/**
 * Тип участника проекта
 * User: Serge Blagodatskih<stikkas17></stikkas17>@gmail.com><br></br>
 * Date: 27.04.16<br></br>
 * Time: 12:28
 */
enum class ParticipantType private constructor(override val code: Int) : Codable {
    LIBRARY(1), BOOK(2);


    companion object {

        private val lookup = HashMap<Int, ParticipantType>()

        init {
            for (t in values()) {
                lookup[t.code] = t
            }
        }

        fun getByValue(code: Int): ParticipantType? {
            return lookup[code]
        }
    }
}

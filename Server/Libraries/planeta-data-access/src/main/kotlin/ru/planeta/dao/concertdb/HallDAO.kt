package ru.planeta.dao.concertdb

import org.apache.ibatis.annotations.Mapper
import ru.planeta.model.concert.Hall

/**
 * Created with IntelliJ IDEA.
 * User: michail
 * Date: 14.10.16
 * Time: 12:36
 */
@Mapper
interface HallDAO {
    fun insert(hall: Hall): Int

    fun update(hall: Hall): Int

    fun select(hallId: Long): Hall

    fun selectByTitle(title: String): Hall
}

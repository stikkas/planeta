package ru.planeta.dao

import org.apache.ibatis.annotations.Mapper
import ru.planeta.dto.MyProjectCardDTO

@Mapper
interface MyProjectCardDAO {

    fun selectByProfileId(profileId: Long): List<MyProjectCardDTO>

    /**
     * Возвращает данные по просмотрам, комментариям за сегодня и за прошлое (без сегодня)
     */
    fun getStatistics(ids: List<Long?>): List<MyProjectCardDTO>
}

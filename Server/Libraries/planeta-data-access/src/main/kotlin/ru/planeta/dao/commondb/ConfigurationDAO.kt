package ru.planeta.dao.commondb

import org.apache.ibatis.annotations.Mapper
import org.apache.ibatis.annotations.Param
import ru.planeta.model.common.Configuration

/**
 * Configuration properties DAO
 *
 * @author ds.kolyshev
 * Date: 17.05.12
 */
@Mapper
interface ConfigurationDAO {

    /**
     * Selects configuration field by key
     *
     * @param key
     * @return
     */
    fun select(key: String): Configuration?

    /**
     * Selects list of configuration fields
     *
     * @param query
     * @param offset
     * @param limit
     * @return
     */
    fun selectList(@Param("query") query: String?, @Param("offset") offset: Int, @Param("limit") limit: Int): List<Configuration>

    fun insert(configuration: Configuration)

    fun update(configuration: Configuration)

    fun delete(key: String)

}

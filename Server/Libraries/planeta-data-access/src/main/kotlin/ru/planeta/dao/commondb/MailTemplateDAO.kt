package ru.planeta.dao.commondb

import org.apache.ibatis.annotations.Mapper
import ru.planeta.model.mail.MailTemplate

/**
 * Mail templates DAO
 *
 * @author ds.kolyshev
 * Date: 12.01.12
 */
@Mapper
interface MailTemplateDAO {
    /**
     * Selects list of mail templates
     *
     * @return
     */
    fun selectMailTemplates(): List<MailTemplate>

    /**
     * Selects mail template by specified id
     *
     * @param templateId
     * @return
     */
    fun select(templateId: Int): MailTemplate

    /**
     * Selects template by it's name
     *
     * @param templateName
     * @return
     */
    fun selectByName(templateName: String): MailTemplate

    fun insert(mailTemplate: MailTemplate)

    fun update(mailTemplate: MailTemplate)

    fun delete(templateId: Int)
}

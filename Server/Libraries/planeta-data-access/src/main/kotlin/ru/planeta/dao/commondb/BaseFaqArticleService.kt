package ru.planeta.dao.commondb

import org.apache.ibatis.annotations.Param
import java.util.ArrayList
import org.springframework.beans.factory.annotation.Autowired
import ru.planeta.model.commondb.FaqArticle
import ru.planeta.model.commondb.FaqArticleExample

abstract class BaseFaqArticleService {
    @Autowired
    protected var faqArticleMapper: FaqArticleMapper? = null

    val orderByClause: String
        get() = "faq_article_id"

    //**************************************************************************************************
    //*********************************  Create ** Update ** Delete  *************************************
    //**************************************************************************************************
    open fun insertFaqArticle(faqArticle: FaqArticle) {


        faqArticleMapper!!.insertSelective(faqArticle)
    }

    fun insertFaqArticleAllFields(faqArticle: FaqArticle) {


        faqArticleMapper!!.insert(faqArticle)
    }

    fun updateFaqArticle(faqArticle: FaqArticle) {

        faqArticleMapper!!.updateByPrimaryKeySelective(faqArticle)
    }

    fun updateFaqArticleAllFields(faqArticle: FaqArticle) {

        faqArticleMapper!!.updateByPrimaryKey(faqArticle)
    }

    fun insertOrUpdateFaqArticle(faqArticle: FaqArticle) {

        val selectedFaqArticle = faqArticleMapper!!.selectByPrimaryKey(faqArticle.faqArticleId)
        if (selectedFaqArticle == null) {

            faqArticleMapper!!.insertSelective(faqArticle)
        } else {
            faqArticleMapper!!.updateByPrimaryKeySelective(faqArticle)
        }
    }

    fun insertOrUpdateFaqArticleAllFields(faqArticle: FaqArticle) {

        val selectedFaqArticle = faqArticleMapper!!.selectByPrimaryKey(faqArticle.faqArticleId)
        if (selectedFaqArticle == null) {

            faqArticleMapper!!.insert(faqArticle)
        } else {
            faqArticleMapper!!.updateByPrimaryKey(faqArticle)
        }
    }

    open fun deleteFaqArticle(id: Long?) {
        faqArticleMapper!!.deleteByPrimaryKey(id)
    }

    //**************************************************************************************************
    //****************************************** faq_article *********************************************
    //************************************** SelectByPrimaryKey ******************************************
    //**************************************************************************************************
    fun selectFaqArticle(faqArticleId: Long?): FaqArticle {
        return faqArticleMapper!!.selectByPrimaryKey(faqArticleId)
    }

    //**************************************************************************************************
    //****************************************** faq_article *********************************************
    //**************************************************************************************************
    private fun COMMENT_SECTION_FaqArticle() {

    }

    fun getFaqArticleExample(offset: Int, @Param("limit") limit: Int): FaqArticleExample {
        val example = FaqArticleExample()
        example.offset = offset
        example.limit = limit
        example.or()
        example.orderByClause = orderByClause
        return example
    }

    fun selectFaqArticleCount(): Int {
        return faqArticleMapper!!.countByExample(getFaqArticleExample(0, 0))
    }

    fun selectFaqArticleList(offset: Int, @Param("limit") limit: Int): List<FaqArticle> {
        return faqArticleMapper!!.selectByExample(getFaqArticleExample(offset, limit))
    }

    fun selectFaqArticleList(faqArticleIdList: List<Long>?): List<FaqArticle> {
        return if (faqArticleIdList == null || faqArticleIdList.isEmpty()) ArrayList() else faqArticleMapper!!.selectByIdList(faqArticleIdList)
    }

    //**************************************************************************************************
    //****************************************** faq_article *********************************************
    //***************************************** byFaqCategory ********************************************
    //************************************** Long faqCategoryId ******************************************
    //**************************************************************************************************
    private fun COMMENT_SECTION_FaqArticleByFaqCategory(faqCategoryId: Long?) {

    }

    fun getFaqArticleByFaqCategoryExample(faqCategoryId: Long?, @Param("offset") offset: Int, @Param("limit") limit: Int): FaqArticleExample {
        val example = FaqArticleExample()
        example.offset = offset
        example.limit = limit
        example.or()
                .andFaqCategoryIdEqualTo(faqCategoryId)
        example.orderByClause = "order_num"
        return example
    }

    fun selectFaqArticleCountByFaqCategory(faqCategoryId: Long?): Int {
        return faqArticleMapper!!.countByExample(getFaqArticleByFaqCategoryExample(faqCategoryId, 0, 0))
    }

    fun selectFaqArticleListByFaqCategory(faqCategoryId: Long?, @Param("offset") offset: Int, @Param("limit") limit: Int): List<FaqArticle> {
        return faqArticleMapper!!.selectByExample(getFaqArticleByFaqCategoryExample(faqCategoryId, offset, limit))
    }

    fun selectFaqArticleListByFaqCategory(faqCategoryId: Long?, @Param("faqArticleIdList") faqArticleIdList: List<Long>?): List<FaqArticle> {
        return if (faqArticleIdList == null || faqArticleIdList.isEmpty()) ArrayList() else faqArticleMapper!!.selectByIdList(faqArticleIdList)
    }
}

package ru.planeta.dao.commondb

import org.apache.ibatis.annotations.Mapper
import org.apache.ibatis.annotations.Param
import ru.planeta.model.common.MoneyTransaction
import ru.planeta.model.enums.MoneyTransactionType
import ru.planeta.model.enums.OrderObjectType

/**
 * Created by kostiagn on 27.10.2015.
 */
@Mapper
interface MoneyTransactionDAO {
    fun selectMoneyTransaction(@Param("profileId") profileId: Long, @Param("transactionType") transactionType: MoneyTransactionType?, @Param("orderObjectType") orderObjectType: OrderObjectType?, @Param("offset") offset: Int, @Param("limit") limit: Int): List<MoneyTransaction>
}

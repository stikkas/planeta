package ru.planeta.dao.mappers.enums.shop

import org.apache.ibatis.type.JdbcType
import org.apache.ibatis.type.TypeHandler
import ru.planeta.model.shop.enums.DeliveryType

import java.sql.CallableStatement
import java.sql.PreparedStatement
import java.sql.ResultSet
import java.sql.SQLException

/**
 * Created by IntelliJ IDEA.
 * User: m.shulepov
 */
class DeliveryTypeHandler : TypeHandler<DeliveryType> {
    @Throws(SQLException::class)
    override fun setParameter(preparedStatement: PreparedStatement, i: Int, parameter: DeliveryType, jdbcType: JdbcType) {
        preparedStatement.setInt(i, parameter.code)
    }

    @Throws(SQLException::class)
    override fun getResult(rs: ResultSet, columnName: String): DeliveryType? {
        return DeliveryType.getByValue(rs.getInt(columnName))
    }

    @Throws(SQLException::class)
    override fun getResult(cs: CallableStatement, columnIndex: Int): DeliveryType? {
        return DeliveryType.getByValue(cs.getInt(columnIndex))
    }

    @Throws(SQLException::class)
    override fun getResult(rs: ResultSet, columnIndex: Int): DeliveryType? {
        return DeliveryType.getByValue(rs.getInt(columnIndex))
    }
}

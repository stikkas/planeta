package ru.planeta.model.commondb

import ru.planeta.model.enums.PaymentErrorStatus
import ru.planeta.model.enums.PaymentErrorType
import java.math.BigDecimal
import java.util.*

class PaymentErrorsExample {
    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database table commondb.payment_errors
     *
     * @mbggenerated Tue May 29 12:53:11 MSK 2018
     */
    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table commondb.payment_errors
     *
     * @mbggenerated Tue May 29 12:53:11 MSK 2018
     */
    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table commondb.payment_errors
     *
     * @mbggenerated Tue May 29 12:53:11 MSK 2018
     */
    var orderByClause: String? = null

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database table commondb.payment_errors
     *
     * @mbggenerated Tue May 29 12:53:11 MSK 2018
     */
    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table commondb.payment_errors
     *
     * @mbggenerated Tue May 29 12:53:11 MSK 2018
     */
    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table commondb.payment_errors
     *
     * @mbggenerated Tue May 29 12:53:11 MSK 2018
     */
    var isDistinct: Boolean = false

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database table commondb.payment_errors
     *
     * @mbggenerated Tue May 29 12:53:11 MSK 2018
     */
    var oredCriteria: MutableList<Criteria>

    var offset: Int = 0

    var limit: Int = 0

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table commondb.payment_errors
     *
     * @mbggenerated Tue May 29 12:53:11 MSK 2018
     */
    init {
        oredCriteria = ArrayList()
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table commondb.payment_errors
     *
     * @mbggenerated Tue May 29 12:53:11 MSK 2018
     */
//    fun getOredCriteria(): List<Criteria> {
//        return oredCriteria
//    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table commondb.payment_errors
     *
     * @mbggenerated Tue May 29 12:53:11 MSK 2018
     */
    fun or(criteria: Criteria) {
        oredCriteria.add(criteria)
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table commondb.payment_errors
     *
     * @mbggenerated Tue May 29 12:53:11 MSK 2018
     */
    fun or(): Criteria {
        val criteria = createCriteriaInternal()
        oredCriteria.add(criteria)
        return criteria
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table commondb.payment_errors
     *
     * @mbggenerated Tue May 29 12:53:11 MSK 2018
     */
    fun createCriteria(): Criteria {
        val criteria = createCriteriaInternal()
        if (oredCriteria.size == 0) {
            oredCriteria.add(criteria)
        }
        return criteria
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table commondb.payment_errors
     *
     * @mbggenerated Tue May 29 12:53:11 MSK 2018
     */
    protected fun createCriteriaInternal(): Criteria {
        return Criteria()
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table commondb.payment_errors
     *
     * @mbggenerated Tue May 29 12:53:11 MSK 2018
     */
    fun clear() {
        oredCriteria.clear()
        orderByClause = null
        isDistinct = false
    }

    /**
     * This class was generated by MyBatis Generator.
     * This class corresponds to the database table commondb.payment_errors
     *
     * @mbggenerated Tue May 29 12:53:11 MSK 2018
     */
    abstract class GeneratedCriteria constructor() {
        var paymentErrorTypeCriteria: MutableList<Criterion>

        var paymentErrorStatusCriteria: MutableList<Criterion>


        var criteria: MutableList<Criterion>

        val isValid: Boolean
            get() = (criteria.size > 0
                    || paymentErrorTypeCriteria.size > 0
                    || paymentErrorStatusCriteria.size > 0)

        init {
            criteria = ArrayList()
            paymentErrorTypeCriteria = ArrayList()
            paymentErrorStatusCriteria = ArrayList()
        }

//        fun getPaymentErrorTypeCriteria(): List<Criterion> {
//            return paymentErrorTypeCriteria
//        }

        protected fun addPaymentErrorTypeCriterion(condition: String, value: Any?, property: String) {
            if (value == null) {
                throw RuntimeException("Value for $property cannot be null")
            }
            paymentErrorTypeCriteria.add(Criterion(condition, value, "ru.planeta.dao.mappers.enums.PaymentErrorTypeTypeHandler"))
            allCriteria = null
        }

        protected fun addPaymentErrorTypeCriterion(condition: String, value1: PaymentErrorType?, value2: PaymentErrorType?, property: String) {
            if (value1 == null || value2 == null) {
                throw RuntimeException("Between values for $property cannot be null")
            }
            paymentErrorTypeCriteria.add(Criterion(condition, value1, "ru.planeta.dao.mappers.enums.PaymentErrorTypeTypeHandler", value2))
            allCriteria = null
        }

//        fun getPaymentErrorStatusCriteria(): List<Criterion> {
//            return paymentErrorStatusCriteria
//        }

        protected fun addPaymentErrorStatusCriterion(condition: String, value: Any?, property: String) {
            if (value == null) {
                throw RuntimeException("Value for $property cannot be null")
            }
            paymentErrorStatusCriteria.add(Criterion(condition, value, "ru.planeta.dao.mappers.enums.PaymentErrorStatusTypeHandler"))
            allCriteria = null
        }

        protected fun addPaymentErrorStatusCriterion(condition: String, value1: PaymentErrorStatus?, value2: PaymentErrorStatus?, property: String) {
            if (value1 == null || value2 == null) {
                throw RuntimeException("Between values for $property cannot be null")
            }
            paymentErrorStatusCriteria.add(Criterion(condition, value1, "ru.planeta.dao.mappers.enums.PaymentErrorStatusTypeHandler", value2))
            allCriteria = null
        }


        var allCriteria: MutableList<Criterion>? = null
            get() {
                if (field == null) {
                    val all = ArrayList<Criterion>()
                    all.addAll(criteria)
                    all.addAll(paymentErrorTypeCriteria)
                    all.addAll(paymentErrorStatusCriteria)
                    field = all
                }
                return field
            }

//        fun getCriteria(): List<Criterion> {
//            return criteria
//        }

        protected fun addCriterion(condition: String?) {
            if (condition == null) {
                throw RuntimeException("Value for condition cannot be null")
            }
            criteria.add(Criterion(condition))
            allCriteria = null
        }

        protected fun addCriterion(condition: String, value: Any?, property: String) {
            if (value == null) {
                throw RuntimeException("Value for $property cannot be null")
            }
            criteria.add(Criterion(condition, value))
            allCriteria = null
        }

        protected fun addCriterion(condition: String, value1: Any?, value2: Any?, property: String) {
            if (value1 == null || value2 == null) {
                throw RuntimeException("Between values for $property cannot be null")
            }
            criteria.add(Criterion(condition, value1, "", value2))
            allCriteria = null
        }

        fun andPaymentErrorIdIsNull(): Criteria {
            addCriterion("payment_errors.payment_error_id is null")
            return this as Criteria
        }

        fun andPaymentErrorIdIsNotNull(): Criteria {
            addCriterion("payment_errors.payment_error_id is not null")
            return this as Criteria
        }

        fun andPaymentErrorIdEqualTo(value: Long?): Criteria {
            addCriterion("payment_errors.payment_error_id =", value, "paymentErrorId")
            return this as Criteria
        }

        fun andPaymentErrorIdNotEqualTo(value: Long?): Criteria {
            addCriterion("payment_errors.payment_error_id <>", value, "paymentErrorId")
            return this as Criteria
        }

        fun andPaymentErrorIdGreaterThan(value: Long?): Criteria {
            addCriterion("payment_errors.payment_error_id >", value, "paymentErrorId")
            return this as Criteria
        }

        fun andPaymentErrorIdGreaterThanOrEqualTo(value: Long?): Criteria {
            addCriterion("payment_errors.payment_error_id >=", value, "paymentErrorId")
            return this as Criteria
        }

        fun andPaymentErrorIdLessThan(value: Long?): Criteria {
            addCriterion("payment_errors.payment_error_id <", value, "paymentErrorId")
            return this as Criteria
        }

        fun andPaymentErrorIdLessThanOrEqualTo(value: Long?): Criteria {
            addCriterion("payment_errors.payment_error_id <=", value, "paymentErrorId")
            return this as Criteria
        }

        fun andPaymentErrorIdIn(values: List<Long>): Criteria {
            addCriterion("payment_errors.payment_error_id in", values, "paymentErrorId")
            return this as Criteria
        }

        fun andPaymentErrorIdNotIn(values: List<Long>): Criteria {
            addCriterion("payment_errors.payment_error_id not in", values, "paymentErrorId")
            return this as Criteria
        }

        fun andPaymentErrorIdBetween(value1: Long?, value2: Long?): Criteria {
            addCriterion("payment_errors.payment_error_id between", value1, value2, "paymentErrorId")
            return this as Criteria
        }

        fun andPaymentErrorIdNotBetween(value1: Long?, value2: Long?): Criteria {
            addCriterion("payment_errors.payment_error_id not between", value1, value2, "paymentErrorId")
            return this as Criteria
        }

        fun andPaymentErrorTypeIsNull(): Criteria {
            addCriterion("payment_errors.payment_error_type_id is null")
            return this as Criteria
        }

        fun andPaymentErrorTypeIsNotNull(): Criteria {
            addCriterion("payment_errors.payment_error_type_id is not null")
            return this as Criteria
        }

        fun andPaymentErrorTypeEqualTo(value: PaymentErrorType): Criteria {
            addPaymentErrorTypeCriterion("payment_errors.payment_error_type_id =", value, "paymentErrorType")
            return this as Criteria
        }

        fun andPaymentErrorTypeNotEqualTo(value: PaymentErrorType): Criteria {
            addPaymentErrorTypeCriterion("payment_errors.payment_error_type_id <>", value, "paymentErrorType")
            return this as Criteria
        }

        fun andPaymentErrorTypeGreaterThan(value: PaymentErrorType): Criteria {
            addPaymentErrorTypeCriterion("payment_errors.payment_error_type_id >", value, "paymentErrorType")
            return this as Criteria
        }

        fun andPaymentErrorTypeGreaterThanOrEqualTo(value: PaymentErrorType): Criteria {
            addPaymentErrorTypeCriterion("payment_errors.payment_error_type_id >=", value, "paymentErrorType")
            return this as Criteria
        }

        fun andPaymentErrorTypeLessThan(value: PaymentErrorType): Criteria {
            addPaymentErrorTypeCriterion("payment_errors.payment_error_type_id <", value, "paymentErrorType")
            return this as Criteria
        }

        fun andPaymentErrorTypeLessThanOrEqualTo(value: PaymentErrorType): Criteria {
            addPaymentErrorTypeCriterion("payment_errors.payment_error_type_id <=", value, "paymentErrorType")
            return this as Criteria
        }

        fun andPaymentErrorTypeIn(values: List<PaymentErrorType>): Criteria {
            addPaymentErrorTypeCriterion("payment_errors.payment_error_type_id in", values, "paymentErrorType")
            return this as Criteria
        }

        fun andPaymentErrorTypeNotIn(values: List<PaymentErrorType>): Criteria {
            addPaymentErrorTypeCriterion("payment_errors.payment_error_type_id not in", values, "paymentErrorType")
            return this as Criteria
        }

        fun andPaymentErrorTypeBetween(value1: PaymentErrorType, value2: PaymentErrorType): Criteria {
            addPaymentErrorTypeCriterion("payment_errors.payment_error_type_id between", value1, value2, "paymentErrorType")
            return this as Criteria
        }

        fun andPaymentErrorTypeNotBetween(value1: PaymentErrorType, value2: PaymentErrorType): Criteria {
            addPaymentErrorTypeCriterion("payment_errors.payment_error_type_id not between", value1, value2, "paymentErrorType")
            return this as Criteria
        }

        fun andProfileIdIsNull(): Criteria {
            addCriterion("payment_errors.profile_id is null")
            return this as Criteria
        }

        fun andProfileIdIsNotNull(): Criteria {
            addCriterion("payment_errors.profile_id is not null")
            return this as Criteria
        }

        fun andProfileIdEqualTo(value: Long?): Criteria {
            addCriterion("payment_errors.profile_id =", value, "profileId")
            return this as Criteria
        }

        fun andProfileIdNotEqualTo(value: Long?): Criteria {
            addCriterion("payment_errors.profile_id <>", value, "profileId")
            return this as Criteria
        }

        fun andProfileIdGreaterThan(value: Long?): Criteria {
            addCriterion("payment_errors.profile_id >", value, "profileId")
            return this as Criteria
        }

        fun andProfileIdGreaterThanOrEqualTo(value: Long?): Criteria {
            addCriterion("payment_errors.profile_id >=", value, "profileId")
            return this as Criteria
        }

        fun andProfileIdLessThan(value: Long?): Criteria {
            addCriterion("payment_errors.profile_id <", value, "profileId")
            return this as Criteria
        }

        fun andProfileIdLessThanOrEqualTo(value: Long?): Criteria {
            addCriterion("payment_errors.profile_id <=", value, "profileId")
            return this as Criteria
        }

        fun andProfileIdIn(values: List<Long>): Criteria {
            addCriterion("payment_errors.profile_id in", values, "profileId")
            return this as Criteria
        }

        fun andProfileIdNotIn(values: List<Long>): Criteria {
            addCriterion("payment_errors.profile_id not in", values, "profileId")
            return this as Criteria
        }

        fun andProfileIdBetween(value1: Long?, value2: Long?): Criteria {
            addCriterion("payment_errors.profile_id between", value1, value2, "profileId")
            return this as Criteria
        }

        fun andProfileIdNotBetween(value1: Long?, value2: Long?): Criteria {
            addCriterion("payment_errors.profile_id not between", value1, value2, "profileId")
            return this as Criteria
        }

        fun andTransactionIdIsNull(): Criteria {
            addCriterion("payment_errors.transaction_id is null")
            return this as Criteria
        }

        fun andTransactionIdIsNotNull(): Criteria {
            addCriterion("payment_errors.transaction_id is not null")
            return this as Criteria
        }

        fun andTransactionIdEqualTo(value: Long?): Criteria {
            addCriterion("payment_errors.transaction_id =", value, "transactionId")
            return this as Criteria
        }

        fun andTransactionIdNotEqualTo(value: Long?): Criteria {
            addCriterion("payment_errors.transaction_id <>", value, "transactionId")
            return this as Criteria
        }

        fun andTransactionIdGreaterThan(value: Long?): Criteria {
            addCriterion("payment_errors.transaction_id >", value, "transactionId")
            return this as Criteria
        }

        fun andTransactionIdGreaterThanOrEqualTo(value: Long?): Criteria {
            addCriterion("payment_errors.transaction_id >=", value, "transactionId")
            return this as Criteria
        }

        fun andTransactionIdLessThan(value: Long?): Criteria {
            addCriterion("payment_errors.transaction_id <", value, "transactionId")
            return this as Criteria
        }

        fun andTransactionIdLessThanOrEqualTo(value: Long?): Criteria {
            addCriterion("payment_errors.transaction_id <=", value, "transactionId")
            return this as Criteria
        }

        fun andTransactionIdIn(values: List<Long>): Criteria {
            addCriterion("payment_errors.transaction_id in", values, "transactionId")
            return this as Criteria
        }

        fun andTransactionIdNotIn(values: List<Long>): Criteria {
            addCriterion("payment_errors.transaction_id not in", values, "transactionId")
            return this as Criteria
        }

        fun andTransactionIdBetween(value1: Long?, value2: Long?): Criteria {
            addCriterion("payment_errors.transaction_id between", value1, value2, "transactionId")
            return this as Criteria
        }

        fun andTransactionIdNotBetween(value1: Long?, value2: Long?): Criteria {
            addCriterion("payment_errors.transaction_id not between", value1, value2, "transactionId")
            return this as Criteria
        }

        fun andOrderIdIsNull(): Criteria {
            addCriterion("payment_errors.order_id is null")
            return this as Criteria
        }

        fun andOrderIdIsNotNull(): Criteria {
            addCriterion("payment_errors.order_id is not null")
            return this as Criteria
        }

        fun andOrderIdEqualTo(value: Long?): Criteria {
            addCriterion("payment_errors.order_id =", value, "orderId")
            return this as Criteria
        }

        fun andOrderIdNotEqualTo(value: Long?): Criteria {
            addCriterion("payment_errors.order_id <>", value, "orderId")
            return this as Criteria
        }

        fun andOrderIdGreaterThan(value: Long?): Criteria {
            addCriterion("payment_errors.order_id >", value, "orderId")
            return this as Criteria
        }

        fun andOrderIdGreaterThanOrEqualTo(value: Long?): Criteria {
            addCriterion("payment_errors.order_id >=", value, "orderId")
            return this as Criteria
        }

        fun andOrderIdLessThan(value: Long?): Criteria {
            addCriterion("payment_errors.order_id <", value, "orderId")
            return this as Criteria
        }

        fun andOrderIdLessThanOrEqualTo(value: Long?): Criteria {
            addCriterion("payment_errors.order_id <=", value, "orderId")
            return this as Criteria
        }

        fun andOrderIdIn(values: List<Long>): Criteria {
            addCriterion("payment_errors.order_id in", values, "orderId")
            return this as Criteria
        }

        fun andOrderIdNotIn(values: List<Long>): Criteria {
            addCriterion("payment_errors.order_id not in", values, "orderId")
            return this as Criteria
        }

        fun andOrderIdBetween(value1: Long?, value2: Long?): Criteria {
            addCriterion("payment_errors.order_id between", value1, value2, "orderId")
            return this as Criteria
        }

        fun andOrderIdNotBetween(value1: Long?, value2: Long?): Criteria {
            addCriterion("payment_errors.order_id not between", value1, value2, "orderId")
            return this as Criteria
        }

        fun andPaymentErrorStatusIsNull(): Criteria {
            addCriterion("payment_errors.payment_error_status_id is null")
            return this as Criteria
        }

        fun andPaymentErrorStatusIsNotNull(): Criteria {
            addCriterion("payment_errors.payment_error_status_id is not null")
            return this as Criteria
        }

        fun andPaymentErrorStatusEqualTo(value: PaymentErrorStatus): Criteria {
            addPaymentErrorStatusCriterion("payment_errors.payment_error_status_id =", value, "paymentErrorStatus")
            return this as Criteria
        }

        fun andPaymentErrorStatusNotEqualTo(value: PaymentErrorStatus): Criteria {
            addPaymentErrorStatusCriterion("payment_errors.payment_error_status_id <>", value, "paymentErrorStatus")
            return this as Criteria
        }

        fun andPaymentErrorStatusGreaterThan(value: PaymentErrorStatus): Criteria {
            addPaymentErrorStatusCriterion("payment_errors.payment_error_status_id >", value, "paymentErrorStatus")
            return this as Criteria
        }

        fun andPaymentErrorStatusGreaterThanOrEqualTo(value: PaymentErrorStatus): Criteria {
            addPaymentErrorStatusCriterion("payment_errors.payment_error_status_id >=", value, "paymentErrorStatus")
            return this as Criteria
        }

        fun andPaymentErrorStatusLessThan(value: PaymentErrorStatus): Criteria {
            addPaymentErrorStatusCriterion("payment_errors.payment_error_status_id <", value, "paymentErrorStatus")
            return this as Criteria
        }

        fun andPaymentErrorStatusLessThanOrEqualTo(value: PaymentErrorStatus): Criteria {
            addPaymentErrorStatusCriterion("payment_errors.payment_error_status_id <=", value, "paymentErrorStatus")
            return this as Criteria
        }

        fun andPaymentErrorStatusIn(values: List<PaymentErrorStatus>): Criteria {
            addPaymentErrorStatusCriterion("payment_errors.payment_error_status_id in", values, "paymentErrorStatus")
            return this as Criteria
        }

        fun andPaymentErrorStatusNotIn(values: List<PaymentErrorStatus>): Criteria {
            addPaymentErrorStatusCriterion("payment_errors.payment_error_status_id not in", values, "paymentErrorStatus")
            return this as Criteria
        }

        fun andPaymentErrorStatusBetween(value1: PaymentErrorStatus, value2: PaymentErrorStatus): Criteria {
            addPaymentErrorStatusCriterion("payment_errors.payment_error_status_id between", value1, value2, "paymentErrorStatus")
            return this as Criteria
        }

        fun andPaymentErrorStatusNotBetween(value1: PaymentErrorStatus, value2: PaymentErrorStatus): Criteria {
            addPaymentErrorStatusCriterion("payment_errors.payment_error_status_id not between", value1, value2, "paymentErrorStatus")
            return this as Criteria
        }

        fun andManagerIdIsNull(): Criteria {
            addCriterion("payment_errors.manager_id is null")
            return this as Criteria
        }

        fun andManagerIdIsNotNull(): Criteria {
            addCriterion("payment_errors.manager_id is not null")
            return this as Criteria
        }

        fun andManagerIdEqualTo(value: Long?): Criteria {
            addCriterion("payment_errors.manager_id =", value, "managerId")
            return this as Criteria
        }

        fun andManagerIdNotEqualTo(value: Long?): Criteria {
            addCriterion("payment_errors.manager_id <>", value, "managerId")
            return this as Criteria
        }

        fun andManagerIdGreaterThan(value: Long?): Criteria {
            addCriterion("payment_errors.manager_id >", value, "managerId")
            return this as Criteria
        }

        fun andManagerIdGreaterThanOrEqualTo(value: Long?): Criteria {
            addCriterion("payment_errors.manager_id >=", value, "managerId")
            return this as Criteria
        }

        fun andManagerIdLessThan(value: Long?): Criteria {
            addCriterion("payment_errors.manager_id <", value, "managerId")
            return this as Criteria
        }

        fun andManagerIdLessThanOrEqualTo(value: Long?): Criteria {
            addCriterion("payment_errors.manager_id <=", value, "managerId")
            return this as Criteria
        }

        fun andManagerIdIn(values: List<Long>): Criteria {
            addCriterion("payment_errors.manager_id in", values, "managerId")
            return this as Criteria
        }

        fun andManagerIdNotIn(values: List<Long>): Criteria {
            addCriterion("payment_errors.manager_id not in", values, "managerId")
            return this as Criteria
        }

        fun andManagerIdBetween(value1: Long?, value2: Long?): Criteria {
            addCriterion("payment_errors.manager_id between", value1, value2, "managerId")
            return this as Criteria
        }

        fun andManagerIdNotBetween(value1: Long?, value2: Long?): Criteria {
            addCriterion("payment_errors.manager_id not between", value1, value2, "managerId")
            return this as Criteria
        }

        fun andErrorTextIsNull(): Criteria {
            addCriterion("payment_errors.error_text is null")
            return this as Criteria
        }

        fun andErrorTextIsNotNull(): Criteria {
            addCriterion("payment_errors.error_text is not null")
            return this as Criteria
        }

        fun andErrorTextEqualTo(value: String): Criteria {
            addCriterion("payment_errors.error_text =", value, "errorText")
            return this as Criteria
        }

        fun andErrorTextNotEqualTo(value: String): Criteria {
            addCriterion("payment_errors.error_text <>", value, "errorText")
            return this as Criteria
        }

        fun andErrorTextGreaterThan(value: String): Criteria {
            addCriterion("payment_errors.error_text >", value, "errorText")
            return this as Criteria
        }

        fun andErrorTextGreaterThanOrEqualTo(value: String): Criteria {
            addCriterion("payment_errors.error_text >=", value, "errorText")
            return this as Criteria
        }

        fun andErrorTextLessThan(value: String): Criteria {
            addCriterion("payment_errors.error_text <", value, "errorText")
            return this as Criteria
        }

        fun andErrorTextLessThanOrEqualTo(value: String): Criteria {
            addCriterion("payment_errors.error_text <=", value, "errorText")
            return this as Criteria
        }

        fun andErrorTextLike(value: String): Criteria {
            addCriterion("payment_errors.error_text like", value, "errorText")
            return this as Criteria
        }

        fun andErrorTextNotLike(value: String): Criteria {
            addCriterion("payment_errors.error_text not like", value, "errorText")
            return this as Criteria
        }

        fun andErrorTextIn(values: List<String>): Criteria {
            addCriterion("payment_errors.error_text in", values, "errorText")
            return this as Criteria
        }

        fun andErrorTextNotIn(values: List<String>): Criteria {
            addCriterion("payment_errors.error_text not in", values, "errorText")
            return this as Criteria
        }

        fun andErrorTextBetween(value1: String, value2: String): Criteria {
            addCriterion("payment_errors.error_text between", value1, value2, "errorText")
            return this as Criteria
        }

        fun andErrorTextNotBetween(value1: String, value2: String): Criteria {
            addCriterion("payment_errors.error_text not between", value1, value2, "errorText")
            return this as Criteria
        }

        fun andCampaignIdIsNull(): Criteria {
            addCriterion("payment_errors.campaign_id is null")
            return this as Criteria
        }

        fun andCampaignIdIsNotNull(): Criteria {
            addCriterion("payment_errors.campaign_id is not null")
            return this as Criteria
        }

        fun andCampaignIdEqualTo(value: Long?): Criteria {
            addCriterion("payment_errors.campaign_id =", value, "campaignId")
            return this as Criteria
        }

        fun andCampaignIdNotEqualTo(value: Long?): Criteria {
            addCriterion("payment_errors.campaign_id <>", value, "campaignId")
            return this as Criteria
        }

        fun andCampaignIdGreaterThan(value: Long?): Criteria {
            addCriterion("payment_errors.campaign_id >", value, "campaignId")
            return this as Criteria
        }

        fun andCampaignIdGreaterThanOrEqualTo(value: Long?): Criteria {
            addCriterion("payment_errors.campaign_id >=", value, "campaignId")
            return this as Criteria
        }

        fun andCampaignIdLessThan(value: Long?): Criteria {
            addCriterion("payment_errors.campaign_id <", value, "campaignId")
            return this as Criteria
        }

        fun andCampaignIdLessThanOrEqualTo(value: Long?): Criteria {
            addCriterion("payment_errors.campaign_id <=", value, "campaignId")
            return this as Criteria
        }

        fun andCampaignIdIn(values: List<Long>): Criteria {
            addCriterion("payment_errors.campaign_id in", values, "campaignId")
            return this as Criteria
        }

        fun andCampaignIdNotIn(values: List<Long>): Criteria {
            addCriterion("payment_errors.campaign_id not in", values, "campaignId")
            return this as Criteria
        }

        fun andCampaignIdBetween(value1: Long?, value2: Long?): Criteria {
            addCriterion("payment_errors.campaign_id between", value1, value2, "campaignId")
            return this as Criteria
        }

        fun andCampaignIdNotBetween(value1: Long?, value2: Long?): Criteria {
            addCriterion("payment_errors.campaign_id not between", value1, value2, "campaignId")
            return this as Criteria
        }

        fun andAmountIsNull(): Criteria {
            addCriterion("payment_errors.amount is null")
            return this as Criteria
        }

        fun andAmountIsNotNull(): Criteria {
            addCriterion("payment_errors.amount is not null")
            return this as Criteria
        }

        fun andAmountEqualTo(value: BigDecimal): Criteria {
            addCriterion("payment_errors.amount =", value, "amount")
            return this as Criteria
        }

        fun andAmountNotEqualTo(value: BigDecimal): Criteria {
            addCriterion("payment_errors.amount <>", value, "amount")
            return this as Criteria
        }

        fun andAmountGreaterThan(value: BigDecimal): Criteria {
            addCriterion("payment_errors.amount >", value, "amount")
            return this as Criteria
        }

        fun andAmountGreaterThanOrEqualTo(value: BigDecimal): Criteria {
            addCriterion("payment_errors.amount >=", value, "amount")
            return this as Criteria
        }

        fun andAmountLessThan(value: BigDecimal): Criteria {
            addCriterion("payment_errors.amount <", value, "amount")
            return this as Criteria
        }

        fun andAmountLessThanOrEqualTo(value: BigDecimal): Criteria {
            addCriterion("payment_errors.amount <=", value, "amount")
            return this as Criteria
        }

        fun andAmountIn(values: List<BigDecimal>): Criteria {
            addCriterion("payment_errors.amount in", values, "amount")
            return this as Criteria
        }

        fun andAmountNotIn(values: List<BigDecimal>): Criteria {
            addCriterion("payment_errors.amount not in", values, "amount")
            return this as Criteria
        }

        fun andAmountBetween(value1: BigDecimal, value2: BigDecimal): Criteria {
            addCriterion("payment_errors.amount between", value1, value2, "amount")
            return this as Criteria
        }

        fun andAmountNotBetween(value1: BigDecimal, value2: BigDecimal): Criteria {
            addCriterion("payment_errors.amount not between", value1, value2, "amount")
            return this as Criteria
        }

        fun andTimeAddedIsNull(): Criteria {
            addCriterion("payment_errors.time_added is null")
            return this as Criteria
        }

        fun andTimeAddedIsNotNull(): Criteria {
            addCriterion("payment_errors.time_added is not null")
            return this as Criteria
        }

        fun andTimeAddedEqualTo(value: Date): Criteria {
            addCriterion("payment_errors.time_added =", value, "timeAdded")
            return this as Criteria
        }

        fun andTimeAddedNotEqualTo(value: Date): Criteria {
            addCriterion("payment_errors.time_added <>", value, "timeAdded")
            return this as Criteria
        }

        fun andTimeAddedGreaterThan(value: Date): Criteria {
            addCriterion("payment_errors.time_added >", value, "timeAdded")
            return this as Criteria
        }

        fun andTimeAddedGreaterThanOrEqualTo(value: Date): Criteria {
            addCriterion("payment_errors.time_added >=", value, "timeAdded")
            return this as Criteria
        }

        fun andTimeAddedLessThan(value: Date): Criteria {
            addCriterion("payment_errors.time_added <", value, "timeAdded")
            return this as Criteria
        }

        fun andTimeAddedLessThanOrEqualTo(value: Date): Criteria {
            addCriterion("payment_errors.time_added <=", value, "timeAdded")
            return this as Criteria
        }

        fun andTimeAddedIn(values: List<Date>): Criteria {
            addCriterion("payment_errors.time_added in", values, "timeAdded")
            return this as Criteria
        }

        fun andTimeAddedNotIn(values: List<Date>): Criteria {
            addCriterion("payment_errors.time_added not in", values, "timeAdded")
            return this as Criteria
        }

        fun andTimeAddedBetween(value1: Date, value2: Date): Criteria {
            addCriterion("payment_errors.time_added between", value1, value2, "timeAdded")
            return this as Criteria
        }

        fun andTimeAddedNotBetween(value1: Date, value2: Date): Criteria {
            addCriterion("payment_errors.time_added not between", value1, value2, "timeAdded")
            return this as Criteria
        }

        fun andTimeUpdatedIsNull(): Criteria {
            addCriterion("payment_errors.time_updated is null")
            return this as Criteria
        }

        fun andTimeUpdatedIsNotNull(): Criteria {
            addCriterion("payment_errors.time_updated is not null")
            return this as Criteria
        }

        fun andTimeUpdatedEqualTo(value: Date): Criteria {
            addCriterion("payment_errors.time_updated =", value, "timeUpdated")
            return this as Criteria
        }

        fun andTimeUpdatedNotEqualTo(value: Date): Criteria {
            addCriterion("payment_errors.time_updated <>", value, "timeUpdated")
            return this as Criteria
        }

        fun andTimeUpdatedGreaterThan(value: Date): Criteria {
            addCriterion("payment_errors.time_updated >", value, "timeUpdated")
            return this as Criteria
        }

        fun andTimeUpdatedGreaterThanOrEqualTo(value: Date): Criteria {
            addCriterion("payment_errors.time_updated >=", value, "timeUpdated")
            return this as Criteria
        }

        fun andTimeUpdatedLessThan(value: Date): Criteria {
            addCriterion("payment_errors.time_updated <", value, "timeUpdated")
            return this as Criteria
        }

        fun andTimeUpdatedLessThanOrEqualTo(value: Date): Criteria {
            addCriterion("payment_errors.time_updated <=", value, "timeUpdated")
            return this as Criteria
        }

        fun andTimeUpdatedIn(values: List<Date>): Criteria {
            addCriterion("payment_errors.time_updated in", values, "timeUpdated")
            return this as Criteria
        }

        fun andTimeUpdatedNotIn(values: List<Date>): Criteria {
            addCriterion("payment_errors.time_updated not in", values, "timeUpdated")
            return this as Criteria
        }

        fun andTimeUpdatedBetween(value1: Date, value2: Date): Criteria {
            addCriterion("payment_errors.time_updated between", value1, value2, "timeUpdated")
            return this as Criteria
        }

        fun andTimeUpdatedNotBetween(value1: Date, value2: Date): Criteria {
            addCriterion("payment_errors.time_updated not between", value1, value2, "timeUpdated")
            return this as Criteria
        }
    }

    /**
     * This class was generated by MyBatis Generator.
     * This class corresponds to the database table commondb.payment_errors
     *
     * @mbggenerated do_not_delete_during_merge Tue May 29 12:53:11 MSK 2018
     */
    class Criteria : GeneratedCriteria()

    /**
     * This class was generated by MyBatis Generator.
     * This class corresponds to the database table commondb.payment_errors
     *
     * @mbggenerated Tue May 29 12:53:11 MSK 2018
     */
    class Criterion {
        var condition: String? = null
            private set

        var value: Any? = null
            private set

        var secondValue: Any? = null

        var isNoValue: Boolean = false

        var isSingleValue: Boolean = false
            private set

        var isBetweenValue: Boolean = false

        var isListValue: Boolean = false
            private set

        var typeHandler: String? = null
            private set

        constructor(condition: String) : super() {
            this.condition = condition
            this.typeHandler = null
            this.isNoValue = true
        }

        @JvmOverloads constructor(condition: String, value: Any, typeHandler: String? = null, secondValue: Any? = null) : super() {
            this.condition = condition
            this.value = value
            this.typeHandler = typeHandler
            if (secondValue != null) {
                this.isBetweenValue = true
                this.secondValue = secondValue
            } else {
                if (value is List<*>) {
                    this.isListValue = true
                } else {
                    this.isSingleValue = true
                }
            }
        }

//        @JvmOverloads constructor(condition: String, value: Any, secondValue: Any, typeHandler: String? = null) : super() {
//            this.condition = condition
//            this.value = value
//            this.secondValue = secondValue
//            this.typeHandler = typeHandler
//            this.isBetweenValue = true
//        }
    }
}

package ru.planeta.model.bibliodb

import java.util.Date

class MagazinePriceList {
    var priceListId: Long = 0
    var name: String? = null
    var timeAdded: Date? = null

    override fun toString(): String {
        return "MagazinePriceList{" +
                "priceListId=" + priceListId +
                ", name='" + name + '\''.toString() +
                ", timeAdded='" + timeAdded!!.time + '\''.toString() +
                '}'.toString()
    }
}

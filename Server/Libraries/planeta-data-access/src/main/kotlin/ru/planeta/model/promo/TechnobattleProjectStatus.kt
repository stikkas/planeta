package ru.planeta.model.promo

import ru.planeta.model.enums.Codable
import java.util.*

/**
 * Created with IntelliJ IDEA.
 * User: michail
 * Date: 11.01.2017
 * Time: 16:24
 */
enum class TechnobattleProjectStatus private constructor(override val code: Int) : Codable {
    ACTIVE(1), DELETED(2), EXPERT_COUNCIL_WINNER(3), POPULAR_VOTE_WINNER(4);


    companion object {

        private val lookup = HashMap<Int, TechnobattleProjectStatus>()

        init {
            for (s in values()) {
                lookup[s.code] = s
            }
        }

        fun getByValue(code: Int): TechnobattleProjectStatus? {
            return lookup[code]
        }

        fun getCodeByEnumSet(set: EnumSet<TechnobattleProjectStatus>): Int {
            var statusCode = 0
            for (s in set) {
                statusCode = statusCode or s.code
            }

            return statusCode
        }
    }
}

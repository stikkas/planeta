package ru.planeta.model.msg

/**
 * Abstract class created for us to make sharding simpler.
 * Dialog objects are partitioned by dialogId property value.
 *
 * @author ameshkov
 */
abstract class DialogObject(dialogId: Long) : IDialogObject {

    /**
     * Gets dialog identifier. This field is used for data partitioning.
     *
     * @return
     */
    /**
     * Sets dialog identifier.s
     *
     * @param dialogId
     */
    override var dialogId: Long = dialogId
}

package ru.planeta.dao.profiledb

import org.apache.ibatis.annotations.Mapper
import org.apache.ibatis.annotations.Param
import ru.planeta.model.profile.broadcast.BroadcastSubscription

/**
 * @author: ds.kolyshev
 * Date: 15.05.13
 */
@Mapper
interface BroadcastSubscriptionDAO {

    /**
     * Selects list of subscribers by broadcast
     *
     * @param profileId
     * @param broadcastId
     * @param offset
     * @param limit
     * @return
     */
    fun selectByBroadcastId(@Param("profileId") profileId: Long, @Param("broadcastId") broadcastId: Long, @Param("offset") offset: Int, @Param("limit") limit: Int): List<BroadcastSubscription>

    /**
     * Selects subscription for specified broadcast and subscriber
     *
     * @param profileId
     * @param broadcastId
     * @param subscriberId
     * @return
     */
    fun selectByBroadcastSubscriberId(@Param("profileId") profileId: Long, @Param("broadcastId") broadcastId: Long, @Param("subscriberId") subscriberId: Long): BroadcastSubscription

    /**
     * Inserts broadcast's subscriber
     *
     * @param broadcastSubscription
     */
    fun insert(broadcastSubscription: BroadcastSubscription)

    /**
     * Deletes broadcast subscriber
     *
     * @param profileId
     * @param broadcastId
     * @param subscriberId
     */
    fun delete(@Param("profileId") profileId: Long, @Param("broadcastId") broadcastId: Long, @Param("subscriberId") subscriberId: Long)

}

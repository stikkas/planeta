package ru.planeta.model.common.campaign

import ru.planeta.model.common.delivery.BaseDelivery
import ru.planeta.model.common.delivery.LinkedDelivery


// TODO: remove forever
class DraftShareEditDetails : ShareEditDetails() {

    override var city: String? = null
    override var street: String? = null
    override var phone: String? = null

    override var linkedDeliveries: List<LinkedDelivery>?
        get() = super.linkedDeliveries
        set(linkedDeliveries) {
            super.linkedDeliveries = linkedDeliveries
            if (linkedDeliveries != null) {
                for (linkedDelivery in linkedDeliveries) {
                    if (linkedDelivery.serviceId == BaseDelivery.DEFAULT_CUSTOMER_PICKUP_SERVICE_ID) {
                        if (linkedDelivery.address != null) {
                            val address = linkedDelivery.address
                            city = address?.city
                            street = address?.street
                            phone = address?.phone
                        }
                    }
                }
            }
        }


}

package ru.planeta.dao

import org.apache.ibatis.annotations.Mapper
import org.apache.ibatis.annotations.Param
import ru.planeta.dto.MyPurchasedProjectCardDTO
import ru.planeta.model.common.campaign.enums.CampaignStatus
import ru.planeta.model.common.campaign.enums.CampaignTargetStatus

@Mapper
interface MyPurchasedProjectCardDAO {
    fun select(@Param("profileId") profileId: Long, @Param("status") status: CampaignStatus,
               @Param("targetStatus") targetStatus: CampaignTargetStatus?, @Param("offset") offset: Long,
               @Param("limit") limit: Int): List<MyPurchasedProjectCardDTO>
}


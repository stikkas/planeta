package ru.planeta.model.enums

/**
 * just for to understand it.
 * [MoneyTransactionType.CREDIT]
 * [MoneyTransactionType.DEBIT]
 */
enum class MoneyTransactionType {
    DEBIT,
    CREDIT
}

package ru.planeta.model.enums

import java.util.HashMap

enum class ContractorPositionType private constructor(override val code: Int) : Codable {
    NOT_SET(-1),
    OTHER(0),
    GEN_DIRECTOR(1),
    DIRECTOR(2),
    PREDSEDATEL(3),
    PRESIDENT(4);


    companion object {

        private val lookup = HashMap<Int, ContractorPositionType>()

        init {
            for (s in values()) {
                lookup[s.code] = s
            }
        }

        fun getByValue(code: Int): ContractorPositionType? {
            return lookup[code]
        }

        fun fromString(json: String): ContractorPositionType {
            var type = OTHER
            for (t in values()) {
                if (t.name == json) {
                    type = t
                    break
                }
            }
            return type
        }
    }
}

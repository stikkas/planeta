package ru.planeta.dao.trashcan

import ru.planeta.model.trashcan.LitresPromoCode

/**
 * Created by asavan on 25.01.2017.
 */
interface LitresPromoCodeDAO {
    fun selectLastNonUsed(): LitresPromoCode

    fun usePromoCode(code: LitresPromoCode)

    fun selectFreePromoCodesCount(): Int

    fun selectUsedPromoCodesCount(): Int
}

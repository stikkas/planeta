package ru.planeta.model.trashcan

import java.util.Date

open class QuizAnswer {
    var quizAnswerId: Long = 0
    var quizId: Long = 0
    var quizQuestionId: Long = 0
    var quizQuestionOptionId: Long = 0
    var userId: Long = 0
    var answerCustomText: String? = null
    var answerInteger: Long = 0
    var isAnswerBoolean: Boolean? = null
    var timeAdded: Date? = null
}

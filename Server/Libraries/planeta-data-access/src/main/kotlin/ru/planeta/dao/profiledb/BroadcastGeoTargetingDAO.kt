package ru.planeta.dao.profiledb

import org.apache.ibatis.annotations.Mapper
import org.apache.ibatis.annotations.Param
import ru.planeta.model.profile.broadcast.BroadcastGeoTargeting

/**
 * Broadcast Geo Targeting DAO
 *
 * @author ds.kolyshev
 * Date: 14.04.13
 */
@Mapper
interface BroadcastGeoTargetingDAO {

    fun insert(broadcastGeoTargeting: BroadcastGeoTargeting)

    fun delete(broadcastId: Long)

    fun deleteByCountryIdCityId(@Param("broadcastId") broadcastId: Long, @Param("countryId") countryId: Int, @Param("cityId") cityId: Int, @Param("allowed") allowed: Boolean)

    /**
     * Selects list of broadcast geo restrictions
     *
     * @param broadcastId
     * @param allowed
     * @return
     */
    fun selectBroadcastGeoTargeting(@Param("broadcastId") broadcastId: Long, @Param("allowed") allowed: Boolean?): List<BroadcastGeoTargeting>
}

package ru.planeta.dto

import ru.planeta.model.shop.enums.PaymentStatus
import java.math.BigDecimal

/**
 * Использутеся для отображения списка купленных вознаграждений в проекте в списке поддержанных преоктов
 */
class PurchaseForProjectCardDTO {
    /**
     * Идентификатор вознаграждения
     */
    var shareId: Long? = null

    /**
     * название
     */
    var nameHtml: String? = null

    /**
     * оплаченная сумма
     */
    var totalPrice: BigDecimal? = null

    /**
     * количество купленных вознаграждений
     */
    var amount: Int? = null

    /**
     * статус покупки
     */
    var status: PaymentStatus? = null
}

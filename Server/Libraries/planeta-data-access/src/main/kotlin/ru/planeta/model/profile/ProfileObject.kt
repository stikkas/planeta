package ru.planeta.model.profile

import ru.planeta.commons.model.Gender
import ru.planeta.model.enums.PermissionLevel
import ru.planeta.model.enums.ProfileType

/**
 * Class ProfileObject
 *
 * @author a.tropnikov
 */
open class ProfileObject : VotableObject(), IProfileObject {

    var tagIds: List<Long>? = null
    var authorName: String? = null
    var authorAlias: String? = null
    var authorImageUrl: String? = null
    var authorGender = Gender.NOT_SET
    var ownerProfileTypeCode: Int = 0
    var ownerName: String? = null
    var ownerAlias: String? = null
    var ownerImageUrl: String? = null
    var commentsCount: Int = 0
    var commentsPermission = PermissionLevel.NONE


    var ownerProfileType: ProfileType?
        get() = ProfileType.Companion.getByValue(ownerProfileTypeCode)
        set(ownerProfileType) {
            this.ownerProfileTypeCode = ownerProfileType?.code ?: 0
        }

    var commentsPermissionCode: Int
        get() = commentsPermission.code
        set(commentsPermissionCode) {
            this.commentsPermission = PermissionLevel.Companion.getByValue(commentsPermissionCode)
        }

    private var authorGenderCode: Int
        get() = authorGender.code
        set(authorGenderId) {
            this.authorGender = Gender.getByValue(authorGenderId)
        }

    val ownerWebAlias: String
        get() = if (ownerAlias == null) profileId.toString() else ownerAlias ?: ""

}

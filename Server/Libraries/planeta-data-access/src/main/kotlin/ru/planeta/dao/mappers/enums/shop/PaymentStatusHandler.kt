package ru.planeta.dao.mappers.enums.shop

import org.apache.ibatis.type.JdbcType
import org.apache.ibatis.type.TypeHandler
import ru.planeta.model.shop.enums.PaymentStatus

import java.sql.CallableStatement
import java.sql.PreparedStatement
import java.sql.ResultSet
import java.sql.SQLException

/**
 * @author Andrew Arefyev
 * Date: 17.10.12
 * Time: 4:12
 */
class PaymentStatusHandler : TypeHandler<PaymentStatus> {

    @Throws(SQLException::class)
    override fun setParameter(ps: PreparedStatement, i: Int, parameter: PaymentStatus, jdbcType: JdbcType) {
        ps.setInt(i, parameter.code)
    }

    @Throws(SQLException::class)
    override fun getResult(rs: ResultSet, columnName: String): PaymentStatus? {
        return PaymentStatus.getByValue(rs.getInt(columnName))
    }

    @Throws(SQLException::class)
    override fun getResult(cs: CallableStatement, columnIndex: Int): PaymentStatus? {
        return PaymentStatus.getByValue(cs.getInt(columnIndex))
    }

    @Throws(SQLException::class)
    override fun getResult(rs: ResultSet, columnIndex: Int): PaymentStatus? {
        return PaymentStatus.getByValue(rs.getInt(columnIndex))
    }
}


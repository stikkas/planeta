package ru.planeta.dao.trashcan

import ru.planeta.model.stat.RequestStat

interface RefererDAO {
    fun insert(requestStat: RequestStat)
}

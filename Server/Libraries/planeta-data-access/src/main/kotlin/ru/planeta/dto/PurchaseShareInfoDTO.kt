package ru.planeta.dto

import ru.planeta.model.common.Address
import ru.planeta.model.common.campaign.Campaign
import ru.planeta.model.common.campaign.ShareDetails

import java.math.BigDecimal

/**
 * Использутеся для отображения данных о вознаграждении на странице оплаты
 */
class PurchaseShareInfoDTO {
    var id: Long? = null
    var shareDetailed: ShareDetails? = null
    var campaign: Campaign? = null
    var authorName: String? = null
    var donateAmount: BigDecimal? = null
    var quantity: Int? = null
    var storedAddress: Address? = null

    constructor()

    constructor(shareDetailed: ShareDetails, campaign: Campaign, authorName: String, donateAmount: BigDecimal, quantity: Int?) {
        this.shareDetailed = shareDetailed
        this.campaign = campaign
        this.authorName = authorName
        this.donateAmount = donateAmount
        this.quantity = quantity
    }
}


package ru.planeta.model.commondb

import java.util.*

class SpeakerExample {
    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database table commondb.speaker
     *
     * @mbggenerated Tue May 29 12:53:11 MSK 2018
     */
    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table commondb.speaker
     *
     * @mbggenerated Tue May 29 12:53:11 MSK 2018
     */
    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table commondb.speaker
     *
     * @mbggenerated Tue May 29 12:53:11 MSK 2018
     */
    var orderByClause: String? = null

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database table commondb.speaker
     *
     * @mbggenerated Tue May 29 12:53:11 MSK 2018
     */
    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table commondb.speaker
     *
     * @mbggenerated Tue May 29 12:53:11 MSK 2018
     */
    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table commondb.speaker
     *
     * @mbggenerated Tue May 29 12:53:11 MSK 2018
     */
    var isDistinct: Boolean = false

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database table commondb.speaker
     *
     * @mbggenerated Tue May 29 12:53:11 MSK 2018
     */
    protected var oredCriteria: MutableList<Criteria>

    var offset: Int = 0

    var limit: Int = 0

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table commondb.speaker
     *
     * @mbggenerated Tue May 29 12:53:11 MSK 2018
     */
    init {
        oredCriteria = ArrayList()
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table commondb.speaker
     *
     * @mbggenerated Tue May 29 12:53:11 MSK 2018
     */


    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table commondb.speaker
     *
     * @mbggenerated Tue May 29 12:53:11 MSK 2018
     */
    fun or(criteria: Criteria) {
        oredCriteria.add(criteria)
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table commondb.speaker
     *
     * @mbggenerated Tue May 29 12:53:11 MSK 2018
     */
    fun or(): Criteria {
        val criteria = createCriteriaInternal()
        oredCriteria.add(criteria)
        return criteria
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table commondb.speaker
     *
     * @mbggenerated Tue May 29 12:53:11 MSK 2018
     */
    fun createCriteria(): Criteria {
        val criteria = createCriteriaInternal()
        if (oredCriteria.size == 0) {
            oredCriteria.add(criteria)
        }
        return criteria
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table commondb.speaker
     *
     * @mbggenerated Tue May 29 12:53:11 MSK 2018
     */
    protected fun createCriteriaInternal(): Criteria {
        return Criteria()
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table commondb.speaker
     *
     * @mbggenerated Tue May 29 12:53:11 MSK 2018
     */
    fun clear() {
        oredCriteria.clear()
        orderByClause = null
        isDistinct = false
    }

    /**
     * This class was generated by MyBatis Generator.
     * This class corresponds to the database table commondb.speaker
     *
     * @mbggenerated Tue May 29 12:53:11 MSK 2018
     */
    abstract class GeneratedCriteria () {
        var criteria: MutableList<Criterion>

        val isValid: Boolean
            get() = criteria.size > 0

        val allCriteria: List<Criterion>
            get() = criteria

        init {
            criteria = ArrayList()
        }


        protected fun addCriterion(condition: String?) {
            if (condition == null) {
                throw RuntimeException("Value for condition cannot be null")
            }
            criteria.add(Criterion(condition))
        }

        protected fun addCriterion(condition: String, value: Any?, property: String) {
            if (value == null) {
                throw RuntimeException("Value for $property cannot be null")
            }
            criteria.add(Criterion(condition, value))
        }

        protected fun addCriterion(condition: String, value1: Any?, value2: Any?, property: String) {
            if (value1 == null || value2 == null) {
                throw RuntimeException("Between values for $property cannot be null")
            }
            criteria.add(Criterion(condition, value1, "", value2))
        }

        fun andSpeakerIdIsNull(): Criteria {
            addCriterion("speaker.speaker_id is null")
            return this as Criteria
        }

        fun andSpeakerIdIsNotNull(): Criteria {
            addCriterion("speaker.speaker_id is not null")
            return this as Criteria
        }

        fun andSpeakerIdEqualTo(value: Long?): Criteria {
            addCriterion("speaker.speaker_id =", value, "speakerId")
            return this as Criteria
        }

        fun andSpeakerIdNotEqualTo(value: Long?): Criteria {
            addCriterion("speaker.speaker_id <>", value, "speakerId")
            return this as Criteria
        }

        fun andSpeakerIdGreaterThan(value: Long?): Criteria {
            addCriterion("speaker.speaker_id >", value, "speakerId")
            return this as Criteria
        }

        fun andSpeakerIdGreaterThanOrEqualTo(value: Long?): Criteria {
            addCriterion("speaker.speaker_id >=", value, "speakerId")
            return this as Criteria
        }

        fun andSpeakerIdLessThan(value: Long?): Criteria {
            addCriterion("speaker.speaker_id <", value, "speakerId")
            return this as Criteria
        }

        fun andSpeakerIdLessThanOrEqualTo(value: Long?): Criteria {
            addCriterion("speaker.speaker_id <=", value, "speakerId")
            return this as Criteria
        }

        fun andSpeakerIdIn(values: List<Long>): Criteria {
            addCriterion("speaker.speaker_id in", values, "speakerId")
            return this as Criteria
        }

        fun andSpeakerIdNotIn(values: List<Long>): Criteria {
            addCriterion("speaker.speaker_id not in", values, "speakerId")
            return this as Criteria
        }

        fun andSpeakerIdBetween(value1: Long?, value2: Long?): Criteria {
            addCriterion("speaker.speaker_id between", value1, value2, "speakerId")
            return this as Criteria
        }

        fun andSpeakerIdNotBetween(value1: Long?, value2: Long?): Criteria {
            addCriterion("speaker.speaker_id not between", value1, value2, "speakerId")
            return this as Criteria
        }

        fun andNameIsNull(): Criteria {
            addCriterion("speaker.name is null")
            return this as Criteria
        }

        fun andNameIsNotNull(): Criteria {
            addCriterion("speaker.name is not null")
            return this as Criteria
        }

        fun andNameEqualTo(value: String): Criteria {
            addCriterion("speaker.name =", value, "name")
            return this as Criteria
        }

        fun andNameNotEqualTo(value: String): Criteria {
            addCriterion("speaker.name <>", value, "name")
            return this as Criteria
        }

        fun andNameGreaterThan(value: String): Criteria {
            addCriterion("speaker.name >", value, "name")
            return this as Criteria
        }

        fun andNameGreaterThanOrEqualTo(value: String): Criteria {
            addCriterion("speaker.name >=", value, "name")
            return this as Criteria
        }

        fun andNameLessThan(value: String): Criteria {
            addCriterion("speaker.name <", value, "name")
            return this as Criteria
        }

        fun andNameLessThanOrEqualTo(value: String): Criteria {
            addCriterion("speaker.name <=", value, "name")
            return this as Criteria
        }

        fun andNameLike(value: String): Criteria {
            addCriterion("speaker.name like", value, "name")
            return this as Criteria
        }

        fun andNameNotLike(value: String): Criteria {
            addCriterion("speaker.name not like", value, "name")
            return this as Criteria
        }

        fun andNameIn(values: List<String>): Criteria {
            addCriterion("speaker.name in", values, "name")
            return this as Criteria
        }

        fun andNameNotIn(values: List<String>): Criteria {
            addCriterion("speaker.name not in", values, "name")
            return this as Criteria
        }

        fun andNameBetween(value1: String, value2: String): Criteria {
            addCriterion("speaker.name between", value1, value2, "name")
            return this as Criteria
        }

        fun andNameNotBetween(value1: String, value2: String): Criteria {
            addCriterion("speaker.name not between", value1, value2, "name")
            return this as Criteria
        }

        fun andDescriptionIsNull(): Criteria {
            addCriterion("speaker.description is null")
            return this as Criteria
        }

        fun andDescriptionIsNotNull(): Criteria {
            addCriterion("speaker.description is not null")
            return this as Criteria
        }

        fun andDescriptionEqualTo(value: String): Criteria {
            addCriterion("speaker.description =", value, "description")
            return this as Criteria
        }

        fun andDescriptionNotEqualTo(value: String): Criteria {
            addCriterion("speaker.description <>", value, "description")
            return this as Criteria
        }

        fun andDescriptionGreaterThan(value: String): Criteria {
            addCriterion("speaker.description >", value, "description")
            return this as Criteria
        }

        fun andDescriptionGreaterThanOrEqualTo(value: String): Criteria {
            addCriterion("speaker.description >=", value, "description")
            return this as Criteria
        }

        fun andDescriptionLessThan(value: String): Criteria {
            addCriterion("speaker.description <", value, "description")
            return this as Criteria
        }

        fun andDescriptionLessThanOrEqualTo(value: String): Criteria {
            addCriterion("speaker.description <=", value, "description")
            return this as Criteria
        }

        fun andDescriptionLike(value: String): Criteria {
            addCriterion("speaker.description like", value, "description")
            return this as Criteria
        }

        fun andDescriptionNotLike(value: String): Criteria {
            addCriterion("speaker.description not like", value, "description")
            return this as Criteria
        }

        fun andDescriptionIn(values: List<String>): Criteria {
            addCriterion("speaker.description in", values, "description")
            return this as Criteria
        }

        fun andDescriptionNotIn(values: List<String>): Criteria {
            addCriterion("speaker.description not in", values, "description")
            return this as Criteria
        }

        fun andDescriptionBetween(value1: String, value2: String): Criteria {
            addCriterion("speaker.description between", value1, value2, "description")
            return this as Criteria
        }

        fun andDescriptionNotBetween(value1: String, value2: String): Criteria {
            addCriterion("speaker.description not between", value1, value2, "description")
            return this as Criteria
        }

        fun andPhotoUrlIsNull(): Criteria {
            addCriterion("speaker.photo_url is null")
            return this as Criteria
        }

        fun andPhotoUrlIsNotNull(): Criteria {
            addCriterion("speaker.photo_url is not null")
            return this as Criteria
        }

        fun andPhotoUrlEqualTo(value: String): Criteria {
            addCriterion("speaker.photo_url =", value, "photoUrl")
            return this as Criteria
        }

        fun andPhotoUrlNotEqualTo(value: String): Criteria {
            addCriterion("speaker.photo_url <>", value, "photoUrl")
            return this as Criteria
        }

        fun andPhotoUrlGreaterThan(value: String): Criteria {
            addCriterion("speaker.photo_url >", value, "photoUrl")
            return this as Criteria
        }

        fun andPhotoUrlGreaterThanOrEqualTo(value: String): Criteria {
            addCriterion("speaker.photo_url >=", value, "photoUrl")
            return this as Criteria
        }

        fun andPhotoUrlLessThan(value: String): Criteria {
            addCriterion("speaker.photo_url <", value, "photoUrl")
            return this as Criteria
        }

        fun andPhotoUrlLessThanOrEqualTo(value: String): Criteria {
            addCriterion("speaker.photo_url <=", value, "photoUrl")
            return this as Criteria
        }

        fun andPhotoUrlLike(value: String): Criteria {
            addCriterion("speaker.photo_url like", value, "photoUrl")
            return this as Criteria
        }

        fun andPhotoUrlNotLike(value: String): Criteria {
            addCriterion("speaker.photo_url not like", value, "photoUrl")
            return this as Criteria
        }

        fun andPhotoUrlIn(values: List<String>): Criteria {
            addCriterion("speaker.photo_url in", values, "photoUrl")
            return this as Criteria
        }

        fun andPhotoUrlNotIn(values: List<String>): Criteria {
            addCriterion("speaker.photo_url not in", values, "photoUrl")
            return this as Criteria
        }

        fun andPhotoUrlBetween(value1: String, value2: String): Criteria {
            addCriterion("speaker.photo_url between", value1, value2, "photoUrl")
            return this as Criteria
        }

        fun andPhotoUrlNotBetween(value1: String, value2: String): Criteria {
            addCriterion("speaker.photo_url not between", value1, value2, "photoUrl")
            return this as Criteria
        }
    }

    /**
     * This class was generated by MyBatis Generator.
     * This class corresponds to the database table commondb.speaker
     *
     * @mbggenerated do_not_delete_during_merge Tue May 29 12:53:11 MSK 2018
     */
    class Criteria : GeneratedCriteria()

    /**
     * This class was generated by MyBatis Generator.
     * This class corresponds to the database table commondb.speaker
     *
     * @mbggenerated Tue May 29 12:53:11 MSK 2018
     */
    class Criterion {
        var condition: String? = null
            private set

        var value: Any? = null
            private set

        var secondValue: Any? = null

        var isNoValue: Boolean = false

        var isSingleValue: Boolean = false
            private set

        var isBetweenValue: Boolean = false

        var isListValue: Boolean = false
            private set

        var typeHandler: String? = null
            private set

        constructor(condition: String) : super() {
            this.condition = condition
            this.typeHandler = null
            this.isNoValue = true
        }

        @JvmOverloads constructor(condition: String, value: Any, typeHandler: String? = null, secondValue: Any? = null) : super() {
            this.condition = condition
            this.value = value
            this.typeHandler = typeHandler
            if (secondValue != null) {
                this.secondValue = secondValue
                this.isBetweenValue = true
            } else {
                if (value is List<*>) {
                    this.isListValue = true
                } else {
                    this.isSingleValue = true
                }
            }
        }

    }
}

package ru.planeta.model.bibliodb

import java.io.Serializable
import java.util.Date
import java.util.HashSet

/**
 * Корзина
 * User: Serge Blagodatskih<stikkas17></stikkas17>@gmail.com><br></br>
 * Date: 27.04.16<br></br>
 * Time: 12:11
 */
class Bin : Serializable {

    /**
     * Идентификатор
     */
    var id: Long = 0

    /**
     * Идентификатор пользователя
     */
    var profileId: Long = 0

    var timeUpdated: Date? = null

    /**
     * Книги
     */
    var books: Set<BookOrder> = HashSet()

    /**
     * Библиотеки
     */
    var libraries: Set<Library> = HashSet()

    val isEmpty: Boolean
        get() = books.isEmpty() && libraries.isEmpty()

    fun hasChanges(another: Bin): Boolean {
        if (libraries.size != another.libraries.size) {
            return true
        }
        if (books.size != another.books.size) {
            return true
        }
        return if (!libraries.containsAll(another.libraries)) {
            true
        } else !books.containsAll(another.books)
    }
}

package ru.planeta.model.stat

import ru.planeta.model.enums.ProjectType

import java.util.Date

/**
 * Created by a.savanovich on 31.10.2016.
 */
class RequestStat {
    var externalId: Long = 0
    var ip: String? = null
    var referer: String? = null
    var cid: String? = null
    var type: RefererStatType? = null
    var timeAdded: Date? = null
    var projectType = ProjectType.MAIN
    var shortLinkId: Long? = null

    var typeCode: Int
        get() = type?.code ?: 0
        set(typeCode) {
            this.type = RefererStatType.getByValue(typeCode)
        }

    val projectTypeCode: Int
        get() = projectType.code

    fun setProjectType(projectTypeCode: Int) {
        this.projectType = ProjectType.getByCode(projectTypeCode)
    }
}

package ru.planeta.model.enums

import java.util.EnumSet
import java.util.HashMap

enum class ShortLinkStatus(override val code: Int) : Codable {
    ON(1), OFF(0);


    companion object {

        private val lookup = HashMap<Int, ShortLinkStatus>()

        init {
            for (s in values()) {
                lookup[s.code] = s
            }
        }

        fun getByValue(code: Int): ShortLinkStatus? {
            return lookup[code]
        }

        fun getCodeByEnumSet(set: EnumSet<ShortLinkStatus>): Int {
            var statusCode = 0
            for (s in set) {
                statusCode = statusCode or s.code
            }

            return statusCode
        }
    }

}

package ru.planeta.model.common

import java.util.Date

class AuthToken {
    var id: Long? = null
    var token: String = ""
    var authentication: ByteArray? = null
    var timeAdded: Date? = null

    constructor() {}

    constructor(token: String, authentication: ByteArray, timeAdded: Date) {
        this.token = token
        this.authentication = authentication
        this.timeAdded = timeAdded
    }

    constructor(token: String) {
        this.token = token
    }
}

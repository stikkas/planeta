package ru.planeta.dao.profiledb

import org.apache.ibatis.annotations.Mapper
import org.apache.ibatis.annotations.Param
import ru.planeta.model.enums.ObjectType
import ru.planeta.model.profile.Comment
import ru.planeta.model.profile.CommentObject

/**
 * @author a.savanovich
 */
@Mapper
interface CommentObjectDAO {

    fun insert(`object`: CommentObject)

    fun update(`object`: CommentObject)

    fun select(@Param("ownerId") ownerId: Long, @Param("type") type: ObjectType, @Param("objectId") objectId: Long): CommentObject

    fun select(old: CommentObject): CommentObject

    fun select(comment: Comment): CommentObject

    fun select(@Param("ownerId") ownerId: Long, @Param("type") type: ObjectType, @Param("offset") offset: Int, @Param("limit") limit: Int): List<CommentObject>
    // TODO may be not need

    fun addCommentsCount(@Param("`object`") `object`: CommentObject, @Param("commentCount") commentCount: Int)

    fun delete(@Param("ownerId") ownerId: Long, @Param("type") type: ObjectType, @Param("objectId") objectId: Long)
}

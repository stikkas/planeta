package ru.planeta.dao.mappers.enums.trashcan

import org.apache.ibatis.type.JdbcType
import org.apache.ibatis.type.TypeHandler
import ru.planeta.model.enums.PromoConfigStatus

import java.sql.CallableStatement
import java.sql.PreparedStatement
import java.sql.ResultSet
import java.sql.SQLException

/**
 * Created by IntelliJ IDEA.
 * User: Andrew
 * Date: 16.10.12
 * Time: 20:51
 */
class PromoConfigStatusHandler : TypeHandler<PromoConfigStatus> {
    @Throws(SQLException::class)
    override fun setParameter(preparedStatement: PreparedStatement, i: Int, parameter: PromoConfigStatus, jdbcType: JdbcType) {
        preparedStatement.setInt(i, parameter.code)
    }

    @Throws(SQLException::class)
    override fun getResult(rs: ResultSet, columnName: String): PromoConfigStatus? {
        return PromoConfigStatus.getByValue(rs.getInt(columnName))
    }

    @Throws(SQLException::class)
    override fun getResult(cs: CallableStatement, columnIndex: Int): PromoConfigStatus? {
        return PromoConfigStatus.getByValue(cs.getInt(columnIndex))
    }

    @Throws(SQLException::class)
    override fun getResult(rs: ResultSet, columnIndex: Int): PromoConfigStatus? {
        return PromoConfigStatus.getByValue(rs.getInt(columnIndex))
    }
}

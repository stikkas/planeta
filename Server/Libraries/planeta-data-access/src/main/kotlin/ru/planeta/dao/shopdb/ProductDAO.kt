package ru.planeta.dao.shopdb

import org.apache.ibatis.annotations.Mapper
import org.apache.ibatis.annotations.Param
import ru.planeta.model.shop.Product
import ru.planeta.model.shop.ProductInfo
import ru.planeta.model.shop.enums.ProductStatus

/**
 * Product's DAO
 * User: a.savanovich
 * Date: 18.06.12
 * Time: 16:43
 */
@Mapper
interface ProductDAO {

    fun save(product: Product)

    /**
     * CURRENT=ACTIVE,PAUSE. CURRENT to OLD.
     *
     * @param productId    product
     * @param withChildren use "false" to GROUP, and "true" for META and ATTRIBUTE
     */
    fun updateStatusCurrentToOld(@Param("productId") productId: Long, @Param("withChildren") withChildren: Boolean)

    /**
     * Selects current (ACTIVE,PAUSE) version of product
     *
     * @param productId product id
     * @return product
     */
    fun selectCurrent(productId: Long): Product?

    /**
     * ACTIVE->PAUSE
     *
     * @param productId product
     */
    fun updateStatusActiveToPause(productId: Long)

    /**
     * PAUSE->ACTIVE
     *
     * @param productId product
     */
    fun updateStatusPauseToActive(productId: Long)

    fun updateStatusPauseToActiveAndSetStartDate(productId: Long)

    /**
     * returns product include if one is OLD
     *
     * @param productId product
     * @return product or NULL
     */
    fun selectProduct(productId: Long): Product?

    fun selectCurrentProductsCount(): Long


    /**
     * selectCampaignById children products by in curtain status
     *
     * @param parentId parent product
     * @param status   status of children
     * @return list of products
     */
    fun selectChildrenProductsByStatus(@Param("parentId") parentId: Long, @Param("status") status: ProductStatus): List<Product>

    /**
     * Updates product's purchase count
     * Sets time last purchased to current time, for specified products if `count` is positive.
     *
     * @param productId product
     * @param count     quantity of purchased products
     */
    fun updateTotalPurchaseCount(@Param("productId") productId: Long, @Param("count") count: Int)

    fun getSimilarProducts(@Param("productId") productId: Long, @Param("limit") limit: Int): List<Product>

    /**
     * change remove link to parent from children.
     *
     * @param productId - parent id
     */
    fun freeChildren(productId: Long)

    /**
     * selectCampaignById ACTIVE,PAUSE or just created products(on moderation) with parent 0
     *
     */
    fun selectTopLevelProducts(@Param("offset") offset: Int, @Param("limit") limit: Int, @Param("query") query: String?, @Param("tagId") tagId: Int, @Param("status") status: String?): List<*>

    /**
     * returns children with current version
     *
     * @param productId parent
     * @return children products list
     */
    fun selectChildrenProducts(productId: Long): List<Product>

    /**
     * changes product totalQuantity by delta.
     *
     * @param productId - product to change
     * @param delta     new value will be x = x+delta
     */
    fun updateTotalQuantityDelta(@Param("productId") productId: Long, @Param("delta") delta: Long)

    /**
     * changes product totalQuantity
     *
     * @param productId - product to change
     * @param value     new value
     */
    fun updateTotalQuantity(@Param("productId") productId: Long, @Param("value") value: Long)

    fun selectActiveProductByIdList(productIdList: List<Long>): List<ProductInfo>

    fun selectByIdList(productIdList: List<Long>): List<Product>

    fun updateProductNameAndDescriptionHtml(@Param("productId") productId: Long, @Param("name_html") name_html: String, @Param("description_html") description_html: String)

    fun bindToProfile(@Param("productId") productId: Long, @Param("referrerId") referrerId: Long, @Param("showOnCampaign") showOnCampaign: Boolean, @Param("campaignIds") campaignIds: LongArray)

    fun getProductListForCampaignByReferrer(@Param("referrerId") referrerId: Long, @Param("campaignId") campaignId: Long, @Param("offset") offset: Int, @Param("limit") limit: Int): List<Product>

    fun getProductsCountByReferrer(referrerId: Long): Int

    fun selectProductsByIds(ids: List<Long>): List<ProductInfo>

    fun getDonateProductByReferrer(referrerId: Long): Long
}

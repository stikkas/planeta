package ru.planeta.dto

import java.math.BigDecimal

class SaveShareInfoDTO {
    var id: Long = 0
    var shareId: Long = 0
    var quantity: Int = 0
    var donateAmount: BigDecimal? = null

    constructor()

    constructor(shareId: Long, quantity: Int, donateAmount: BigDecimal) {
        this.shareId = shareId
        this.quantity = quantity
        this.donateAmount = donateAmount
    }
}

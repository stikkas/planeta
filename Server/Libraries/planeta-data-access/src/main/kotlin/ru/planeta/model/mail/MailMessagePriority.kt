package ru.planeta.model.mail

import ru.planeta.model.enums.Codable

import java.util.EnumSet
import java.util.HashMap

/**
 * Created with IntelliJ IDEA.
 * User: michail
 * Date: 06.12.2016
 * Time: 17:35
 */
enum class MailMessagePriority private constructor(override val code: Int) : Codable {
    HIGH(1000), DEFAULT(500), LOW(100);


    companion object {

        private val lookup = HashMap<Int, MailMessagePriority>()

        init {
            for (s in values()) {
                lookup[s.code] = s
            }
        }

        fun getByValue(code: Int): MailMessagePriority? {
            return lookup[code]
        }

        fun getCodeByEnumSet(set: EnumSet<MailMessagePriority>): Int {
            var statusCode = 0
            for (s in set) {
                statusCode = statusCode or s.code
            }

            return statusCode
        }
    }
}

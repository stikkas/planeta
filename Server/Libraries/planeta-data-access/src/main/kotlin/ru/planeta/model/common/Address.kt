package ru.planeta.model.common

import com.fasterxml.jackson.annotation.JsonIgnoreProperties

import java.io.Serializable

/**
 * User: a.savanovich
 * Date: 18.06.12
 * Time: 12:49
 */
@JsonIgnoreProperties(ignoreUnknown = true)
class Address : Serializable {

    private var addressId: Long? = null
    var street: String? = null
    var city: String? = null
    var country: String? = null
    var zipCode: String? = null

    var cityId: Int = 0
    var countryId: Int = 0

    /**
     * search ValidateUtils.rejectIfNotPhone
     */
    var phone: String? = null

    var isLastUsed = true

    var customerName: String? = null    // this class also used for store customer data (WHYYY???), so this field is for storing customer name

    constructor() {}

    constructor(address: Address) {
        this.addressId = address.getAddressId()
        this.street = address.street
        this.city = address.city
        this.country = address.country
        this.zipCode = address.zipCode
        this.cityId = address.cityId
        this.countryId = address.countryId
        this.phone = address.phone
    }

    @Deprecated("")
    constructor(zipCode: String, country: String, city: String, street: String, phone: String) {
        this.zipCode = zipCode
        this.country = country
        this.city = city
        this.street = street
        this.phone = phone
    }

    fun getAddressId(): Long {
        if (addressId == null) {
            this.addressId = 0L
        }
        return addressId!!
    }

    fun setAddressId(addressId: Long?) {
        if (addressId == null) {
            this.addressId = 0L
        } else {
            this.addressId = addressId
        }
    }


    override fun equals(o: Any?): Boolean {
        if (this === o) return true
        if (o == null || javaClass != o.javaClass) return false

        val address = o as Address?

        if (cityId != address!!.cityId) return false
        if (countryId != address.countryId) return false
        if (isLastUsed != address.isLastUsed) return false
        if (if (addressId != null) addressId != address.addressId else address.addressId != null) return false
        if (if (street != null) street != address.street else address.street != null) return false
        if (if (city != null) city != address.city else address.city != null) return false
        if (if (country != null) country != address.country else address.country != null) return false
        if (if (zipCode != null) zipCode != address.zipCode else address.zipCode != null) return false
        if (if (customerName != null) customerName != address.customerName else address.customerName != null) return false
        return if (phone != null) phone == address.phone else address.phone == null

    }

    override fun hashCode(): Int {
        var result = if (addressId != null) addressId!!.hashCode() else 0
        result = 31 * result + if (street != null) street!!.hashCode() else 0
        result = 31 * result + if (city != null) city!!.hashCode() else 0
        result = 31 * result + if (country != null) country!!.hashCode() else 0
        result = 31 * result + if (zipCode != null) zipCode!!.hashCode() else 0
        result = 31 * result + cityId
        result = 31 * result + countryId
        result = 31 * result + if (phone != null) phone!!.hashCode() else 0
        result = 31 * result + if (customerName != null) customerName!!.hashCode() else 0
        result = 31 * result + if (isLastUsed) 1 else 0
        return result
    }

    override fun toString(): String {
        return "$zipCode, $country, $city, $street"
    }
}

package ru.planeta.model.common.school

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import ru.planeta.model.commondb.SeminarRegistration

@JsonIgnoreProperties(ignoreUnknown = true)
class SeminarRegistrationPair : SeminarRegistration() {
    var email: String? = null
    var email2: String? = null
}

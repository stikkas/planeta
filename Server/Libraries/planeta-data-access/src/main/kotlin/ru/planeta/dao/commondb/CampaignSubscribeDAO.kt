package ru.planeta.dao.commondb

import org.apache.ibatis.annotations.Mapper
import org.apache.ibatis.annotations.Param
import ru.planeta.model.common.campaign.CampaignSubscribe

/**
 * Created with IntelliJ IDEA.
 * User: michail
 * Date: 16.11.2017
 * Time: 16:04
 */
@Mapper
interface CampaignSubscribeDAO {
    fun getCampaignSubsctiptionsToSend(@Param("offset") offset: Long, @Param("limit") limit: Int): List<CampaignSubscribe>

    fun isSubscribedOnCampaignEnd(@Param("profileId") profileId: Long, @Param("campaignId") campaignId: Long): Boolean?

    fun subscribeOnCampaignEnd(@Param("profileId") profileId: Long, @Param("campaignId") campaignId: Long)

    fun unsubscribeOnCampaignEnd(@Param("profileId") profileId: Long, @Param("campaignId") campaignId: Long)
}

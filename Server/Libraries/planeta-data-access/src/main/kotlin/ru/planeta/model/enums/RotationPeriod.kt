package ru.planeta.model.enums

import java.util.EnumSet
import java.util.HashMap

/**
 * @author ds.kolyshev
 * Date: 30.09.11
 */
enum class RotationPeriod private constructor(override val code: Int, val milliseconds: Long) : Codable {
    MONTH(0, 30.toLong() * 24 * 60 * 60 * 1000),
    WEEK(1, 7 * 24 * 60 * 60 * 1000),
    DAY(2, 24 * 60 * 60 * 1000),
    HOUR(4, 60 * 60 * 1000);


    companion object {

        private val lookup = HashMap<Int, RotationPeriod>()

        init {
            for (s in EnumSet.allOf(RotationPeriod::class.java)) {
                lookup[s.code] = s
            }
        }

        fun getByValue(code: Int): RotationPeriod? {
            return lookup[code]
        }
    }
}

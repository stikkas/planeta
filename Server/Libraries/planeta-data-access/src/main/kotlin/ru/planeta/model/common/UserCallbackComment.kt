package ru.planeta.model.common

import java.util.Date

/**
 * Date: 03.06.2015
 * Time: 16:38
 */
class UserCallbackComment {
    var commentId: Long = 0
    var callbackId: Long = 0
    var authorId: Long = 0
    var authorName: String? = null
    var text: String? = null
    var timeAdded: Date? = null
}

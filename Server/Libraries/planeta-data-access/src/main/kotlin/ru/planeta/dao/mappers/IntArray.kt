package ru.planeta.dao.mappers

import java.sql.SQLException
import java.sql.Types

/**
 * @author ds.kolyshev
 * Date: 13.09.11
 */
class IntArray : SqlArrayAdapter {

    companion object {
        val emptyArray = arrayOf<Int>()
    }

    private val array: Array<Int>

    constructor(array: Array<Int>?) {
        if (array == null) {
            throw IllegalArgumentException("parameter array should not be null")
        }
        this.array = array
    }


    @Throws(SQLException::class)
    override fun getBaseType(): Int {
        return Types.INTEGER
    }

    @Throws(SQLException::class)
    override fun getBaseTypeName(): String? {
        return "int4"
    }

    override fun toString(): String {
        var result = "{"
        array?.let {
            for (i in it.indices) {
                if (i > 0) {
                    result += ","
                }
                result += it[i]
            }
        }
        result += "}"
        return result
    }

    @Throws(SQLException::class)
    override fun free() {
    }
}


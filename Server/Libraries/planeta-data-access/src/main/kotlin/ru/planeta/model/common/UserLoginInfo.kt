package ru.planeta.model.common

import java.util.Date

/**
 * User: sshendyapin
 * Date: 28.01.13
 * Time: 16:13
 */
class UserLoginInfo {

    var profileId: Long = 0
    var userIpAddress: String? = null
    var userAgent: String? = null
    var timeLogin: Date? = null
}

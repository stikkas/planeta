package ru.planeta.model.mail

import org.apache.commons.lang3.StringUtils

import javax.activation.DataSource
import java.util.Date

/**
 * Created with IntelliJ IDEA.
 * User: michail
 * Date: 06.12.2016
 * Time: 17:26
 */
class MailMessage {
    var messageId: Long = 0
    var from: String? = null
    var replyTo: String? = null
    var to: List<String>? = null
    var subject: String? = null
    var contentHtml: String? = null
    var multipartFiles: Map<String, DataSource>? = null
    var isSkipBodyLogging: Boolean = false
    var attemptsCount: Int = 0
    var status: MailMessageStatus? = null
    var priority = MailMessagePriority.DEFAULT
    var timeAdded: Date? = null
    var timeUpdated: Date? = null
    var externalMessageId: String? = null

    /*
        stringify addressee list
     */
    var toList: String?
        get() = StringUtils.join(to, ", ")
        set(toList) {
            this.to = ru.planeta.commons.lang.StringUtils.splitByCommas(toList)
        }

    /*
        get contentHtml for insering to db
     */
    val contentHtmlSafe: String?
        get() = if (isSkipBodyLogging) "" else contentHtml

    val statusCode: Int?
        get() = if (status != null) status!!.code else null

    // for mybatis
    constructor() {}

    constructor(from: String, replyTo: String, to: List<String>, subject: String, contentHtml: String) {
        this.from = from
        this.replyTo = replyTo
        this.to = to
        this.subject = subject
        this.contentHtml = contentHtml
    }

    constructor(from: String, replyTo: String, to: List<String>, subject: String, contentHtml: String, multipartFiles: Map<String, DataSource>?,
                skipLogging: Boolean, priority: MailMessagePriority) {
        this.from = from
        this.replyTo = replyTo
        this.to = to
        this.subject = subject
        this.contentHtml = contentHtml
        this.multipartFiles = multipartFiles
        this.priority = priority
        this.isSkipBodyLogging = skipLogging
        this.attemptsCount = 0
    }

    constructor(messageId: Long, from: String, replyTo: String, to: List<String>, subject: String, contentHtml: String, multipartFiles: Map<String, DataSource>, skipLogging: Boolean, priority: MailMessagePriority) {
        this.messageId = messageId
        this.from = from
        this.replyTo = replyTo
        this.to = to
        this.subject = subject
        this.contentHtml = contentHtml
        this.multipartFiles = multipartFiles
        this.priority = priority
        this.isSkipBodyLogging = skipLogging
        this.attemptsCount = 0
    }

    fun increaseAttemptsCount() {
        attemptsCount++
    }

    fun setStatusCode(code: Int) {
        this.status = MailMessageStatus.getByValue(code)
    }

}


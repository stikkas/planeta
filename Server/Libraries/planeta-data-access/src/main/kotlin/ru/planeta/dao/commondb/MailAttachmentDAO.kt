package ru.planeta.dao.commondb

import org.apache.ibatis.annotations.Mapper
import ru.planeta.model.mail.MailAttachment

/**
 * Mail attachments DAO
 */
@Mapper
interface MailAttachmentDAO {
    /**
     * Selects list of mail attachments
     *
     * @return
     */
    fun selectMailAttachments(templateId: Int): List<MailAttachment>

    /**
     * Selects mail attachment by specified id
     *
     * @param attachmentId
     * @return
     */
    fun select(attachmentId: Long): MailAttachment

    fun insert(mailAttachment: MailAttachment)

    fun update(mailAttachment: MailAttachment)

    fun delete(attachmentId: Long)
}

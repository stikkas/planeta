package ru.planeta.dao.handlers

import org.apache.ibatis.type.BaseTypeHandler
import org.apache.ibatis.type.JdbcType
import org.apache.ibatis.type.MappedTypes
import ru.planeta.model.enums.ShortLinkStatus
import java.sql.CallableStatement
import java.sql.PreparedStatement
import java.sql.ResultSet

@MappedTypes(ShortLinkStatus::class)
class ShortLinkStatusHandler : BaseTypeHandler<ShortLinkStatus>() {
    override fun getNullableResult(p0: ResultSet, p1: String?): ShortLinkStatus? {
        return ShortLinkStatus.getByValue(p0.getInt(p1))
    }

    override fun getNullableResult(p0: ResultSet, p1: Int): ShortLinkStatus? {
        return ShortLinkStatus.getByValue(p0.getInt(p1))
    }

    override fun getNullableResult(p0: CallableStatement, p1: Int): ShortLinkStatus? {
        return ShortLinkStatus.getByValue(p0.getInt(p1))
    }

    override fun setNonNullParameter(p0: PreparedStatement, p1: Int, p2: ShortLinkStatus, p3: JdbcType?) {
        p0.setInt(p1, p2.code)
    }
}
package ru.planeta.model.stat.log

/**
 * Base database log record.<br></br>
 * User: eshevchenko
 */
abstract class BaseDBLogRecord {

    var id: Long = 0
    var timeAdded: Long = 0
}

package ru.planeta.dao.statdb

import org.apache.ibatis.annotations.Mapper
import org.apache.ibatis.annotations.Param
import ru.planeta.model.stat.log.DBLogRecord
import ru.planeta.model.stat.log.HttpInteractionLogRecord
import ru.planeta.model.stat.log.LoggerType

/**
 * Database log data access object.<br></br>
 * User: eshevchenko
 */
@Mapper
interface DBLogDAO {

    /**
     * Inserts request log record using directly JDBC connection.
     *
     * @param metaData request meta data;
     * @return created record identifier.
     */
    fun insertRequestLogRecord(@Param("metaData") metaData: String): Long

    /**
     * Inserts response log record using directly JDBC connection.
     *
     * @param requestId related request log record identifier;
     * @param metaData  request meta data;
     * @return created record identifier.
     */
    fun insertResponseLogRecord(@Param("requestId") requestId: Long,
                                @Param("metaData") metaData: String): Long

    /**
     * @return created record identifier.
     */
    fun insertLogRecord(@Param("message") message: String, @Param("loggerType") loggerType: LoggerType,
                        @Param("objectId") objectId: Long, @Param("subjectId") subjectId: Long,
                        @Param("requestId") requestId: Long, @Param("responseId") responseId: Long): Long

    /**
     * Updates specific log record reference info.
     *
     * @param recordId   record identifier;
     * @param objectId   logged object identifier;
     * @param subjectId  logged subject identifier;
     * @param requestId  HTTP request log record identifier;
     * @param responseId HTTP response log record identifier;
     */
    fun updateRefInfo(@Param("recordId") recordId: Long, @Param("objectId") objectId: Long,
                      @Param("subjectId") subjectId: Long, @Param("requestId") requestId: Long,
                      @Param("responseId") responseId: Long)

    /**
     * Selects log records by specific logger type, object and subject identifiers.
     *
     * @param loggerType logger type;
     * @param objectId   logged object identifier;
     * @param subjectId  logged subject identifier;
     * @return satisfying log records.
     */
    fun selectLogRecords(@Param("loggerType") loggerType: LoggerType, @Param("objectId") objectId: Long,
                         @Param("subjectId") subjectId: Long): List<DBLogRecord>

    /**
     * Selects HTTP-interaction (response and request) log records.
     *
     * @param ids          request/response identifiers, can be `null`;
     * @param searchString search substring of meta data, can be `null` or empty string;
     * @return HTTP-interaction log records.
     */
    fun selectHttpInteractionLogByRecordIds(@Param("ids") ids: Collection<Long>,
                                            @Param("searchString") searchString: String): List<HttpInteractionLogRecord>

    /**
     * Deletes test data (records created while tests executing).<br></br>
     * May be used within testing only.<br></br>
     * Testing records has minus value object and subject identifiers.
     */
    fun deleteTestData()
}

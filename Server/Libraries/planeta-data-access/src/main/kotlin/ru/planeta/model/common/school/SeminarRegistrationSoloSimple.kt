package ru.planeta.model.common.school

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import ru.planeta.model.commondb.SeminarRegistration

@JsonIgnoreProperties(ignoreUnknown = true)
class SeminarRegistrationSoloSimple : SeminarRegistration() {
    var email: String? = null
}

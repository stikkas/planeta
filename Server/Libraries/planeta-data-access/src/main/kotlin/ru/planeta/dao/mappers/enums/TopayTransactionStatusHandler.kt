package ru.planeta.dao.mappers.enums

import org.apache.ibatis.type.JdbcType
import org.apache.ibatis.type.TypeHandler
import ru.planeta.model.enums.TopayTransactionStatus

import java.sql.CallableStatement
import java.sql.PreparedStatement
import java.sql.ResultSet
import java.sql.SQLException

/**
 * Payment transaction status handler.
 * User: eshevchenko
 */
class TopayTransactionStatusHandler : TypeHandler<TopayTransactionStatus> {

    @Throws(SQLException::class)
    override fun setParameter(preparedStatement: PreparedStatement, i: Int, status: TopayTransactionStatus, jdbcType: JdbcType) {
        preparedStatement.setInt(i, status.code)
    }

    @Throws(SQLException::class)
    override fun getResult(resultSet: ResultSet, s: String): TopayTransactionStatus? {
        return TopayTransactionStatus.getByValue(resultSet.getInt(s))
    }

    @Throws(SQLException::class)
    override fun getResult(resultSet: ResultSet, i: Int): TopayTransactionStatus? {
        return TopayTransactionStatus.getByValue(resultSet.getInt(i))
    }

    @Throws(SQLException::class)
    override fun getResult(callableStatement: CallableStatement, i: Int): TopayTransactionStatus? {
        return TopayTransactionStatus.getByValue(callableStatement.getInt(i))
    }
}

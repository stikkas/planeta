package ru.planeta.dao.profiledb

import org.apache.ibatis.annotations.Mapper
import org.apache.ibatis.annotations.Param
import ru.planeta.dto.AudioDTO
import ru.planeta.model.profile.media.AudioTrack

/**
 * @author a.savanovich
 * Date: 27.10.11
 */
@Mapper
interface AudioTrackDAO {

    /**
     * Selects list of owner audio tracks
     *
     */
    fun selectAudioTracks(@Param("profileId") profileId: Long, @Param("offset") offset: Int,
                          @Param("limit") limit: Int): List<AudioTrack>

    /**
     * Selects list of audio tracks of the specified profile (satisfy to the search string)
     *
     */
    fun selectAudioTracks(@Param("profileId") profileId: Long, @Param("offset") offset: Int, @Param("limit") limit: Int,
                          @Param("searchString") searchString: String?): List<AudioTrack>

    /**
     * Selects owner audio track by identifier
     *
     */
    fun selectAudioTrackById(@Param("profileId") profileId: Long, @Param("trackId") trackId: Long): AudioTrack

    /**
     * @param profileId owner profile Id
     * @return audios count of this profile
     */
    fun getAudiosCount(profileId: Long): Int

    /**
     * Selects list of owner audio album's tracks
     *
     */
    fun selectAudioTracksByAlbumId(@Param("profileId") profileId: Long, @Param("albumId") albumId: Long,
                                   @Param("offset") offset: Int, @Param("limit") limit: Int): List<AudioTrack>

    /**
     * Selects list of owner audio playlist's tracks (satisfy to the search string)
     *
     */
    fun selectAudioTracksByPlaylistId(@Param("profileId") profileId: Long, @Param("playlistId") playlistId: Int,
                                      @Param("searchString") searchString: String): List<AudioTrack>

    /**
     * Selects audio track by original track identifier
     *
     */
    fun selectAudioTrackByUploaderTrackId(@Param("profileId") profileId: Long, @Param("trackId") trackId: Long): List<AudioTrack>


    fun selectAudioByListTrackIds(@Param("profileId") profileId: Long, @Param("ids") ids: List<Long>): List<AudioTrack>

    /**
     * Inserts new track
     *
     */
    fun insert(ownerAudioTrack: AudioTrack)

    /**
     * Updates audio track
     *
     */
    fun update(ownerAudioTrack: AudioTrack)

    /**
     * Deletes track
     *
     */
    fun delete(@Param("profileId") profileId: Long, @Param("trackId") trackId: Long)

    /**
     * Updates user audio's views count
     *
     */
    fun updateListeningsCount(@Param("profileId") profileId: Long, @Param("trackId") trackId: Long,
                              @Param("listeningsCount") listeningsCount: Int)

    /**
     * Updates user audio's downloads count
     *
     */
    fun updateDownloadsCount(@Param("profileId") profileId: Long, @Param("trackId") trackId: Long,
                             @Param("downloadsCount") downloadsCount: Int)

    fun selectAudio(trackId: Long): AudioDTO?
}


package ru.planeta.entity

import ru.planeta.commons.model.Gender
import ru.planeta.model.enums.ProfileStatus
import ru.planeta.model.enums.ProfileType
import java.util.*

//@Entity
//@Table(name = "profiledb.profiles")
class Profile : AbstractTimestampEntity {

    /**
     * Идентификатор профиля пользователя (равен ID юзера из таблицы commondb.users_private_info)
     */
//    @Id
//    @Column(name = "profile_id", nullable = false)
    var profileId: Long = 0

    /**
     * Имя пользователя, которое будет отображаться везде на сервисе
     */
//    @Column(name = "display_name", nullable = false)
    var displayName: String = ""

    /**
     * Идентификатор профиля пользователя создавшего данный профиль, если это сообщество
     */
//    @Column(name = "creator_profile_id", nullable = false)
    var creatorProfileId: Long = 0

    /**
     * Тип профиля пользователя
     */
//    @Column(name = "profile_type_id", nullable = false)
    var profileType: ProfileType? = null

    /**
     * URL изображения профиля
     */
//    @Column(name = "image_url", nullable = true)
    var imageUrl: String? = null

    /**
     * Id записи в таблице в profiledb.photos
     */
//    @Column(name = "image_id", nullable = false)
    var imageId: Long = 0

    /**
     * Маленькая картинка профиля
     */
//    @Column(name = "small_image_url", nullable = true)
    var smallImageUrl: String? = null

    /**
     * Id записи в таблице в profiledb.photos
     */
//    @Column(name = "small_image_id", nullable = false)
    var smallImageId: Long = 0

    /**
     * Алиас пользователя. Для формирования уникального URL на страницу профиля, нпр. https://planeta.ru/ivasyuta
     */
//    @Column(name = "alias", nullable = true)
    var alias: String? = null

    /**
     * Статус пользователя
     */
//    @Column(name = "status", nullable = false)
    var status = ProfileStatus.NOT_SET

    /**
     * Идентификатор страны проживания пользователя из profiledb.country
     */
//    @Column(name = "country_id", nullable = true)
    var countryId: Int = 0

    /**
     * Идентификатор города проживания пользователя из profiledb.city
     */
//    @Column(name = "city_id", nullable = true)
    var cityId: Int = 0

    /**
     * Дата рождения пользователя
     */
//    @Column(name = "user_birth_date", nullable = true)
    var userBirthDate: Date? = null

    /**
     * Пол пользователя
     */
//    @Column(name = "user_gender", nullable = true)
    var userGender: Gender? = Gender.NOT_SET

    /**
     * Описание "О себе" пользователя
     */
//    @Column(name = "summary", nullable = true)
    var summary: String? = null

    /**
     * Контактный номер пользователя
     */
//    @Column(name = "phone_number", nullable = true)
    var phoneNumber: String? = null

    /**
     * Является ли пользователь VIP клиентом
     */
//    @Column(name = "vip", nullable = false)
    var vip: Boolean = false

    /**
     * Дата окончания действия vip
     */
//    @Column(name = "vip_obtain_date", nullable = true)
    var vipObtainDate: Date? = null

    /**
     * Получает пользователь или нет письма с информацией из его проектов
     */
//    @Column(name = "receive_my_campaign_newsletters", nullable = false)
    var receiveMyCampaignNewsletters: Boolean = false

    /**
     * Получает пользователь или нет рассылки планеты
     */
//    @Column(name = "is_receive_newsletters", nullable = false)
    var isReceiveNewsletters: Boolean = false

    /**
     * Количество пользователей, подписанных на этог профиль
     */
//    @Column(name = "subscribers_count", nullable = true)
    var subscribersCount: Int = 0

    /**
     * Количество новых подписчиков
     */
//    @Column(name = "new_subscribers_count", nullable = false)
    var newSubscribersCount: Int = 0

    /**
     * Количество пользователей, на которых подписан данный профиль
     */
//    @Column(name = "subscriptions_count", nullable = false)
    var subscriptionsCount: Int = 0

    /**
     * Количество запущенных (статусы 3, 4, 5) проектов профиля
     */
//    @Column(name = "projects_count", nullable = true)
    var projectsCount: Int = 0

    /**
     * Количество всех проектов автора
     */
//    @Column(name = "author_projects_count", nullable = true)
    var authorProjectsCount: Int = 0

    /**
     * Количество участников в сообществе
     */
//    @Column(name = "users_count", nullable = false)
    var usersCount: Int = 0

    /**
     * Показывать поддержанные проекты
     */
//    @Column(name = "is_show_backed_campaigns", nullable = false)
    var isShowBackedCampaigns: Boolean = false

    /**
     * Количество поддержанных проектов
     */
//    @Column(name = "backed_count", nullable = false)
    var backedCount: Int = 0

    constructor(): super()

    constructor(code: Int, profileType: ProfileType): super() {
        this.profileType = profileType
        this.status = ProfileStatus.getByValue(code, profileType)
    }
}

package ru.planeta.model.common

import ru.planeta.model.enums.ProjectType

/**
 * Created with IntelliJ IDEA.
 * User: a.volkov
 * Date: 20.05.14
 * Time: 17:10
 */
class ProjectPaymentTool {
    var id: Long = 0
    var projectType: ProjectType? = null
    var paymentMethodId: Long = 0
    var paymentMethodName: String? = null
    var paymentToolId: Long = 0
    var isEnabled: Boolean = false
    var priority: Int = 0
}

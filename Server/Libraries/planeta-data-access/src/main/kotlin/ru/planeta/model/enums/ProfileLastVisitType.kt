package ru.planeta.model.enums

enum class ProfileLastVisitType {
    COMMENTS_TAB, //0
    NEWS_TAB, //1
    CAMPAIGN_PAGE, //2
    SUBSCRIBERS_PAGE  //3
}

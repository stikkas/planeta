package ru.planeta.dto

import ru.planeta.model.common.campaign.enums.CampaignStatus
import ru.planeta.model.common.campaign.enums.CampaignTargetStatus

import java.math.BigDecimal

/**
 * Использутеся для отображения собственных проектов пользователся в личном кабинете
 */
open class ProjectCardDTO {
    /**
     * Идентификатор проекта
     */
    var campaignId: Long? = null

    /**
     * статус проекта
     */
    var status: CampaignStatus? = null

    /**
     * Путь к заглавной картинке проекта
     */
    var imageUrl: String? = null

    /**
     * Название проекта с html разметкой
     */
    var nameHtml: String? = null

    /**
     * Алиас проекта
     */
    var webCampaignAlias: String? = null

    /**
     * Короткое описание с html разметкой
     */
    var shortDescriptionHtml: String? = null

    /**
     * Конечная сумма, цель проекта
     */
    var targetAmount: BigDecimal? = null

    /**
     * Собранная сумма на данный момент
     */
    var collectedAmount: BigDecimal? = null

    /**
     * Кол-во покупок
     */
    var purchaseCount: Long? = null

    /**
     * Статус завершенного проекта - успешный или нет
     */
    var targetStatus: CampaignTargetStatus? = null

}

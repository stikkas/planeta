package ru.planeta.model.common

import java.math.BigDecimal

/**
 * Date: 21.01.2016
 * Time: 15:24
 */
class OrderInfoForReport : OrderInfo() {
    var orderObjectsTotalPrice: BigDecimal? = null
    var orderObjectName: String? = null
    var orderObjectsCount: Int = 0
    var orderObjectPrice: BigDecimal? = null
    var orderObjectComment: String? = null
}

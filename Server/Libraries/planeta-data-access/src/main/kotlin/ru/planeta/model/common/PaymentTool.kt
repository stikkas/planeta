package ru.planeta.model.common

import com.fasterxml.jackson.annotation.JsonAutoDetect
import com.fasterxml.jackson.annotation.JsonIgnore
import ru.planeta.model.enums.ProjectType

import java.io.Serializable
import java.math.BigDecimal

/**
 * Created with IntelliJ IDEA.
 * User: a.volkov
 * Date: 24.04.14
 * Time: 17:46
 */
@JsonAutoDetect
class PaymentTool : Identified, Serializable {
    @get:JsonIgnore
    override var id: Long = 0
    var name: String? = null
    @get:JsonIgnore
    var code: String? = null
    @get:JsonIgnore
    var paymentMethodId: Long = 0
    @get:JsonIgnore
    var paymentProviderId: Long = 0
    @get:JsonIgnore
    var projectType: ProjectType? = null
    var min: BigDecimal? = null
    var max: BigDecimal? = null
    @get:JsonIgnore
    var commission: BigDecimal? = null
    @get:JsonIgnore
    var monthlyLimit: BigDecimal? = null
    @get:JsonIgnore
    var isEnabled: Boolean = false
    var isTlsSupported = true
    var comment: String? = null
}

package ru.planeta.model.enums

import java.util.*

/**
 * @author s.makarov
 */
enum class ModerationMessageStatus(val code: Int) {
    MODERATOR_MESSAGE(1), AUTHOR_MESSAGE(2), INTERNAL_MESSAGE(3);


    companion object {
        private val lookup = HashMap<Int, ModerationMessageStatus>()

        init {
            for (s in EnumSet.allOf(ModerationMessageStatus::class.java)) {
                lookup[s.code] = s
            }
        }

        fun getByValue(code: Int): ModerationMessageStatus? {
            return lookup[code]
        }

        fun getCodeByEnumSet(set: EnumSet<ModerationMessageStatus>): Int {
            var statusCode = 0
            for (s in set) {
                statusCode = statusCode or s.code
            }

            return statusCode
        }

        fun getEnumSetFromCode(code: Int): EnumSet<ModerationMessageStatus> {
            val set = EnumSet.noneOf(ModerationMessageStatus::class.java)

            for (s in ModerationMessageStatus.values()) {
                if (s.code and code != 0) {
                    set.add(s)
                }
            }

            return set
        }
    }
}


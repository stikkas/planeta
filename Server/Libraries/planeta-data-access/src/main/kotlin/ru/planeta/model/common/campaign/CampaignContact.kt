package ru.planeta.model.common.campaign

/**
 * Campaign's public contact class
 *
 * @author: ds.kolyshev
 * Date: 28.07.13
 */
class CampaignContact {
    var campaignId: Long = 0
    var email: String? = null
    var contactProfileId: Long = 0
    var contactName: String? = null
    var imageUrl: String? = null
}

package ru.planeta.model.profile

import ru.planeta.model.enums.ObjectType

/**
 * Date: 11.10.12
 *
 * @author s.kalmykov
 */
interface ICommentable {


    var profileId: Long?

    val objectId: Long?

    var objectType: ObjectType?
}

package ru.planeta.model.common.charity

class CharityEventOrder {
    var eventOrderId: Long = 0
    var profileId: Long = 0
    var fio: String? = null
    var email: String? = null
    var phone: String? = null
    var organizationName: String? = null
    var eventGoal: String? = null
    var eventResult: String? = null
    var eventExpectedTime: String? = null
    var eventAudience: String? = null
    var participantsCount: Int = 0
    var eventFormat: String? = null
}

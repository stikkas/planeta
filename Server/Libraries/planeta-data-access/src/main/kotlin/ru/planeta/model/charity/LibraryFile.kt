package ru.planeta.model.charity

class LibraryFile {
    var libraryFileId: Long? = null

    var themeId: Long? = null

    var themeHeader: String? = null

    private var header: String? = null

    private var url: String? = null

    fun getHeader(): String? {
        return header
    }

    fun setHeader(header: String?) {
        this.header = header?.trim { it <= ' ' }
    }

    fun getUrl(): String? {
        return url
    }

    fun setUrl(url: String?) {
        this.url = url?.trim { it <= ' ' }
    }

    class Builder {
        private val obj: LibraryFile

        init {
            this.obj = LibraryFile()
        }

        fun libraryFileId(libraryFileId: Long?): Builder {
            obj.libraryFileId = libraryFileId
            return this
        }

        fun themeId(themeId: Long?): Builder {
            obj.themeId = themeId
            return this
        }

        fun themeHeader(themeHeader: String): Builder {
            obj.themeHeader = themeHeader
            return this
        }

        fun header(header: String): Builder {
            obj.header = header
            return this
        }

        fun url(url: String): Builder {
            obj.url = url
            return this
        }

        fun build(): LibraryFile {
            return this.obj
        }
    }

    companion object {

        val builder: Builder
            get() = Builder()
    }
}

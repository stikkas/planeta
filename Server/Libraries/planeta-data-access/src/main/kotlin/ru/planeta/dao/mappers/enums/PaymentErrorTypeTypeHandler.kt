package ru.planeta.dao.mappers.enums

import java.sql.CallableStatement
import java.sql.PreparedStatement
import java.sql.ResultSet
import java.sql.SQLException
import org.apache.ibatis.type.JdbcType
import org.apache.ibatis.type.MappedTypes
import org.apache.ibatis.type.TypeHandler
import ru.planeta.model.enums.PaymentErrorType

@MappedTypes(PaymentErrorType::class)
class PaymentErrorTypeTypeHandler : TypeHandler<PaymentErrorType> {

    @Throws(SQLException::class)
    override fun setParameter(ps: PreparedStatement, i: Int, parameter: PaymentErrorType, jdbcType: JdbcType) {
        ps.setInt(i, parameter.code)
    }

    @Throws(SQLException::class)
    override fun getResult(rs: ResultSet, columnName: String): PaymentErrorType? {
        return if (rs.getObject(columnName) == null) null else PaymentErrorType.getByValue(rs.getInt(columnName))
    }

    @Throws(SQLException::class)
    override fun getResult(rs: ResultSet, columnIndex: Int): PaymentErrorType? {
        return if (rs.getObject(columnIndex) == null) null else PaymentErrorType.getByValue(rs.getInt(columnIndex))
    }

    @Throws(SQLException::class)
    override fun getResult(cs: CallableStatement, columnIndex: Int): PaymentErrorType? {
        return if (cs.getObject(columnIndex) == null) null else PaymentErrorType.getByValue(cs.getInt(columnIndex))
    }
}

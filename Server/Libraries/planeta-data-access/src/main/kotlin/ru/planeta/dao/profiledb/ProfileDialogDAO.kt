package ru.planeta.dao.profiledb

import org.apache.ibatis.annotations.Mapper
import org.apache.ibatis.annotations.Param
import ru.planeta.model.msg.DialogInfo
import ru.planeta.model.profile.ProfileDialog

/**
 * Data access object for working with profile_dialogs table
 *
 * @author ameshkov
 */
@Mapper
interface ProfileDialogDAO {

    /**
     * Selects specified profile dialog object
     *
     * @param profileId
     * @param dialogId
     * @return
     */
    fun select(@Param("profileId") profileId: Long, @Param("dialogId") dialogId: Long): ProfileDialog?

    /**
     * Selects list of user's dialogs links.
     * List is ordered by timeAdded desc.
     * This list includes only user's dialogs with more than one message.
     *
     * @param profileId
     * @param offset
     * @param limit
     * @return
     */
    fun select(@Param("profileId") profileId: Long, @Param("offset") offset: Int, @Param("limit") limit: Int): List<DialogInfo>

    fun select(@Param("profileId") profileId: Long, @Param("query") query: String, @Param("offset") offset: Int, @Param("limit") limit: Int): List<DialogInfo>

    /**
     * Selects list of user's dialogs links to dialogs with unread messages.
     *
     * @param profileId
     * @param offset
     * @param limit
     * @return
     */
    fun selectWithUnread(@Param("profileId") profileId: Long, @Param("offset") offset: Int, @Param("limit") limit: Int): List<ProfileDialog>

    /**
     * Selects users dialog link by the companion user identifier.
     *
     * @param profileId
     * @param companionUserId
     * @return
     */
    fun selectByCompanion(@Param("profileId") profileId: Long, @Param("companionUserId") companionUserId: Long): ProfileDialog

    /**
     * Inserts specified user's dialog link
     *
     * @param profileDialog
     */
    fun insert(profileDialog: ProfileDialog)

    /**
     * Updates user dialog link (only timeUpdated field is updated)
     *
     * @param profileDialog
     */
    fun update(profileDialog: ProfileDialog)

}

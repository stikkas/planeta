package ru.planeta.model.common.campaign

import java.util.Date

/**
 *
 * User: Serge Blagodatskih<stikkas17></stikkas17>@gmail.com><br></br>
 * Date: 22.03.16<br></br>
 * Time: 16:15
 */
class CampaignEditTime {

    var campaignId: Long = 0
    var profileId: Long = 0
    var lastTimeActive: Date? = null

    constructor() {}

    constructor(campaignId: Long, profileId: Long, lastTimeActive: Date) {
        this.campaignId = campaignId
        this.profileId = profileId
        this.lastTimeActive = lastTimeActive
    }
    /*
    public static void main(String[] args) {
        System.out.println(
                IbatisGenerator.generateAll(CampaignEditTime.class,
                        "commondb.campaign_edit_time"));
    }
*/
}

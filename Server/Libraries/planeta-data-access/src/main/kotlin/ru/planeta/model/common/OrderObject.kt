package ru.planeta.model.common

import ru.planeta.model.enums.OrderObjectType

import java.math.BigDecimal
import java.util.Date

/**
 * User: atropnikov
 * Date: 06.04.12
 * Time: 16:43
 */
open class OrderObject : TransactionalObject() {

    var orderObjectId: Long = 0
    var orderId: Long = 0
    var objectId: Long = 0
    var orderObjectType: OrderObjectType? = null
    var merchantId: Long = 0
    var price = BigDecimal.ZERO
    var comment: String? = null
    var textForMail: String? = null
    open var estimatedDeliveryTime: Date? = null
}

package ru.planeta.model.profile.location

import java.io.Serializable

/**
 * @author a.savanovich
 */
class City : BaseLocation(), Serializable {
    override var locationId: Int
        get() = objectId
        set(value) {
            this.objectId = value
        }

    override var name: String?
        get() = nameRus
        set(value) {}

    var objectId: Int = 0
    var regionId: Int = 0
    // setters only for ibatis
    var countryId: Int = 0
    var lat: Double? = null
    var lng: Double? = null
    var nameRus: String? = null
    var nameEn: String? = null
    var regionNameRus: String? = null
    var regionNameEn: String? = null

    override var locationType: LocationType? = null
        get() = LocationType.CITY

    override var parentLocationId: Int? = null
        get() = if (regionId != IGeoLocation.DEFAULT_LOCATION_ID) regionId else countryId

    override var parentLocationType: LocationType? = null
        get() = if (regionId != IGeoLocation.DEFAULT_LOCATION_ID) LocationType.REGION else LocationType.COUNTRY

    override fun equals(o: Any?): Boolean {
        if (this === o) return true
        if (o !is City) return false

        val city = o as City?

        return countryId == city!!.countryId && objectId == city.objectId && regionId == city.regionId
    }

    override fun hashCode(): Int {
        var result = objectId
        result = 31 * result + regionId
        result = 31 * result + countryId
        return result
    }

    companion object {

        private const val serialVersionUID = 1L
    }
}

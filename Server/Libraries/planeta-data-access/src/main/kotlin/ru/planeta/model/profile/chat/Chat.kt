package ru.planeta.model.profile.chat

import ru.planeta.model.enums.ObjectType
import ru.planeta.model.profile.ProfileObject

import java.util.Date

/**
 * This class represents chat
 *
 * @author m.shulepov
 */
class Chat : ProfileObject() {

    var chatId: Long = 0
    var ownerObjectId: Long = 0
    private var ownerObjectTypeCode: Int = 0
    var timeAdded: Date? = null

    override var objectType: ObjectType?
        get() = ObjectType.getByCode(ownerObjectTypeCode)
        set(value: ObjectType?) {
            super.objectType = value
        }

    fun setOwnerObjectTypeCode(ownerObjectTypeCode: Int) {
        this.ownerObjectTypeCode = ownerObjectTypeCode
    }

}

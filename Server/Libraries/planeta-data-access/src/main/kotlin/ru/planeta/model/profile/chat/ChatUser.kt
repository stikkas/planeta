package ru.planeta.model.profile.chat


import ru.planeta.model.profile.Profile

import java.util.Date

/**
 * Class that represents users participating in chat
 *
 * @author m.shulepov
 */
class ChatUser {

    var profileId: Long = 0
    var chatId: Long = 0
    var smallImageUrl: String? = null
    var name: String? = null
    var alias: String? = null
    var displayName: String? = null
    @get:Synchronized
    @set:Synchronized
    var lastActivityTime = Date()

    constructor() {}

    constructor(profileId: Long, chatId: Long) {
        this.profileId = profileId
        this.chatId = chatId
    }

    constructor(profile: Profile, chatId: Long) {
        this.chatId = chatId
        this.profileId = profile.profileId
        this.smallImageUrl = profile.smallImageUrl
        this.alias = profile.alias
        this.displayName = profile.displayName
    }

    override fun equals(o: Any?): Boolean {
        if (this === o) return true
        if (o == null || javaClass != o.javaClass) return false

        val chatUser = o as ChatUser?

        if (chatId != chatUser!!.chatId) return false
        return if (profileId != chatUser.profileId) false else true

    }

    override fun hashCode(): Int {
        var result = (profileId xor profileId.ushr(32)).toInt()
        result = 31 * result + (chatId xor chatId.ushr(32)).toInt()
        return result
    }

}

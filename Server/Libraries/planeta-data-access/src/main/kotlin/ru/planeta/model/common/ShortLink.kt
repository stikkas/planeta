package ru.planeta.model.common

import ru.planeta.model.enums.ShortLinkStatus

/**
 *
 * Created by a.savanovich on 28.09.2016.
 */
class ShortLink {
    var shortLinkId: Long = 0
    var profileId: Long = 0

    //TODO новая админка: переделать вот эту хуету
    var shortLinkStatus: ShortLinkStatus? = ShortLinkStatus.OFF
    var shortLink: String? = null
    var redirectAddressUrl: String? = null
    var cookieName: String? = null
    var cookieValue: String? = null

    fun switchStatus() {
        shortLinkStatus = if (shortLinkStatus === ShortLinkStatus.ON) ShortLinkStatus.OFF else ShortLinkStatus.ON
    }

}

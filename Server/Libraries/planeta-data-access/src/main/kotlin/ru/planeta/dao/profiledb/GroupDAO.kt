package ru.planeta.dao.profiledb

import org.apache.ibatis.annotations.Mapper
import ru.planeta.model.profile.Group

/**
 * @author ds.kolyshev
 * Date: 27.10.11
 */
@Mapper
interface GroupDAO {

    /**
     * Selects group by profileId
     *
     * @param profileId
     * @return
     */
    fun selectByProfileId(profileId: Long): Group?

    /**
     * Inserts group
     *
     * @param group
     */
    fun insert(group: Group)

    /**
     * Updates group
     *
     * @param group
     */
    fun update(group: Group)

    /**
     * Deletes group by profileId
     *
     * @param profileId
     */
    fun delete(profileId: Long)

}

package ru.planeta.model.stat

/**
 * Created with IntelliJ IDEA.
 * User: a.volkov
 * Date: 01.08.14
 * Time: 15:42
 */
enum class StatEventType {
    VIEW,
    CAMPAIGN_VIEW,
    CAMPAIGN_PURCHASE,
    CAMPAIGN_COMMENT,
    REGISTER,
    WIDGET_IMAGE_SHOW,
    WIDGET_IFRAME_SHOW,
    WIDGET_PURCHASE_SHOW,
    LANG_CHANGE
}

package ru.planeta.model.enums

import java.util.*

/**
 * Enum ProfileStatus
 *
 * @author a.tropnikov
 */
enum class ProfileStatus private constructor(override val code: Int, val profileType: ProfileType) : Codable {

    NOT_SET(0, ProfileType.USER),
    USER_ACTIVE(1, ProfileType.USER),
    USER_BOUNCED(2, ProfileType.USER),
    USER_SPAMMER(4, ProfileType.USER),
    USER_DELETED(8, ProfileType.USER),
    GROUP_PENDING(1, ProfileType.GROUP),
    GROUP_ACTIVE(2, ProfileType.GROUP),
    HIDDEN_GROUP_ACTIVE(2, ProfileType.HIDDEN_GROUP),
    GROUP_REJECTED(4, ProfileType.GROUP),
    GROUP_ACTIVE_REQUEST_OFFICIAL(5, ProfileType.GROUP),
    GROUP_ACTIVE_OFFICIAL(6, ProfileType.GROUP),
    GROUP_DELETED(8, ProfileType.GROUP),
    @Deprecated("")
    EVENT_CREATED(1, ProfileType.EVENT),
    @Deprecated("")
    EVENT_CANCELED(2, ProfileType.EVENT),
    @Deprecated("")
    EVENT_ORDERED(4, ProfileType.EVENT),
    @Deprecated("")
    EVENT_PLANED(8, ProfileType.EVENT);

    private class LookupKey(private val code: Int, private val profileType: ProfileType?) {

        override fun equals(obj: Any?): Boolean {
            if (obj == null) {
                return false
            }
            if (javaClass != obj.javaClass) {
                return false
            }
            val other = obj as LookupKey?
            if (code != other!!.code) {
                return false
            }
            return this.profileType === other.profileType
        }

        override fun hashCode(): Int {
            var result = code
            result *= 31
            if (profileType != null) {
                result += profileType.hashCode()
            }
            return result
        }
    }

    companion object {
        private val lookup = HashMap<LookupKey, ProfileStatus>()

        init {
            for (s in EnumSet.allOf(ProfileStatus::class.java)) {
                lookup[LookupKey(s.code, s.profileType)] = s
            }
        }

        fun getCodes(enums: EnumSet<ProfileStatus>): List<Int> {
            val codes = LinkedList<Int>()
            for (ps in enums) {
                codes.add(ps.code)
            }
            return codes
        }

        fun getByValue(code: Int, profileType: ProfileType): ProfileStatus {
            return lookup[LookupKey(code, profileType)] ?: NOT_SET
        }

        fun getCodeByEnumSet(set: EnumSet<ProfileStatus>): Int {
            var statusCode = 0
            for (s in set) {
                statusCode = statusCode or s.code
            }

            return statusCode
        }

        fun getEnumSetFromCode(code: Int): EnumSet<ProfileStatus> {
            val set = EnumSet.noneOf(ProfileStatus::class.java)

            for (s in ProfileStatus.values()) {
                if (s.code and code != 0) {
                    set.add(s)
                }
            }

            return set
        }
    }
}

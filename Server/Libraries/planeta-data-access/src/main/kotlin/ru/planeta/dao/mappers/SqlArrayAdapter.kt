package ru.planeta.dao.mappers

import java.sql.Array
import java.sql.ResultSet
import java.sql.SQLException

/**
 * @author ds.kolyshev
 * Date: 13.09.11
 */
abstract class SqlArrayAdapter : Array {
    @Throws(SQLException::class)
    override fun getBaseTypeName(): String? {
        return null
    }

    @Throws(SQLException::class)
    override fun getBaseType(): Int {
        return 0
    }

    @Throws(SQLException::class)
    override fun getArray(): Any? {
        return null
    }

    @Throws(SQLException::class)
    override fun getArray(map: Map<String, Class<*>>): Any? {
        return null
    }

    @Throws(SQLException::class)
    override fun getArray(index: Long, count: Int): Any? {
        return null
    }

    @Throws(SQLException::class)
    override fun getArray(index: Long, count: Int, map: Map<String, Class<*>>): Any? {
        return null
    }

    @Throws(SQLException::class)
    override fun getResultSet(): ResultSet? {
        return null
    }

    @Throws(SQLException::class)
    override fun getResultSet(map: Map<String, Class<*>>): ResultSet? {
        return null
    }

    @Throws(SQLException::class)
    override fun getResultSet(index: Long, count: Int): ResultSet? {
        return null
    }

    @Throws(SQLException::class)
    override fun getResultSet(index: Long, count: Int, map: Map<String, Class<*>>): ResultSet? {
        return null
    }

    @Throws(SQLException::class)
    override fun free() {

    }
}

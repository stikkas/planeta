package ru.planeta.dao.mappers.enums


import org.apache.ibatis.type.JdbcType
import org.apache.ibatis.type.TypeHandler
import ru.planeta.model.enums.ObjectType

import java.sql.CallableStatement
import java.sql.PreparedStatement
import java.sql.ResultSet
import java.sql.SQLException

/**
 * MyBatis type handler for enum
 *
 * @author Andrew.Arefyev
 * @version 0.1
 * @see ru.planeta.model.enums.ObjectType
 *
 * @see TypeHandler
 *
 * @since 31.10.2012
 */
class ObjectTypeHandler : TypeHandler<ObjectType> {
    @Throws(SQLException::class)
    override fun setParameter(ps: PreparedStatement, i: Int, parameter: ObjectType, jdbcType: JdbcType) {
        ps.setInt(i, parameter.code)
    }

    @Throws(SQLException::class)
    override fun getResult(rs: ResultSet, columnName: String): ObjectType? {
        return ObjectType.getByCode(rs.getInt(columnName))
    }

    @Throws(SQLException::class)
    override fun getResult(cs: CallableStatement, columnIndex: Int): ObjectType? {
        return ObjectType.getByCode(cs.getInt(columnIndex))
    }

    @Throws(SQLException::class)
    override fun getResult(rs: ResultSet, columnIndex: Int): ObjectType? {
        return ObjectType.getByCode(rs.getInt(columnIndex))
    }
}

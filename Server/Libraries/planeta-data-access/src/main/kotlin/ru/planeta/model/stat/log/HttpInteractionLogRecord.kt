package ru.planeta.model.stat.log

/**
 * Created with IntelliJ IDEA.
 * User: eshevchenko
 */
class HttpInteractionLogRecord : BaseDBLogRecord() {

    var metaData: String? = null
    var requestId: Long = 0
    var isRequest: Boolean = false
}

package ru.planeta.dao.maildb

import org.apache.ibatis.annotations.Mapper

@Mapper
interface MailUserDAO {
    fun unsubscribeUserByEmail(email: String)

    fun forceSubscribeUserByEmail(email: String)

    fun addNewSubscriber(email: String)

    fun checkSubscriber(email: String): Boolean
}

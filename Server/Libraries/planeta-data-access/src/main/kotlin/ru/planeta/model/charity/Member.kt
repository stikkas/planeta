package ru.planeta.model.charity

/*
 * Created by Alexey on 24.06.2016.
 */

import java.io.Serializable

class Member : Serializable {
    var memberId: Long = 0
    var memberName: String? = null
    var description: String? = null
    var phoneNumber: String? = null
    var email: String? = null
    var imageUrl: String? = null
}

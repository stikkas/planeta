package ru.planeta.model.common

import ru.planeta.commons.model.Gender
import ru.planeta.model.common.delivery.LinkedDelivery

import java.util.ArrayList

/**
 * User: atropnikov
 * Date: 06.04.12
 * Time: 18:26
 */
open class OrderInfo : Order() {

    var buyerGender = Gender.NOT_SET
    var buyerAlias: String? = null
    var buyerName: String? = null
    var buyerImageUrl: String? = null
    var buyerEmail: String? = null
    var orderObjectCampaignId: Long = 0
    var orderObjectCampaignName: String? = null

    var linkedDelivery: LinkedDelivery? = null
    var questionToBuyer: String? = null

    var orderObjectsInfo: MutableList<OrderObjectInfo> = ArrayList()

    var deliveryAddress: DeliveryAddress? = null


    fun addOrderObjectInfo(orderObjectInfo: OrderObjectInfo) {
        orderObjectsInfo.add(orderObjectInfo)
    }

    companion object {

        fun createFrom(order: Order): OrderInfo {
            val result = OrderInfo()

            result.creditTransactionId = order.creditTransactionId
            result.debitTransactionId = order.debitTransactionId
            result.cancelCreditTransactionId = order.cancelCreditTransactionId
            result.cancelDebitTransactionId = order.cancelDebitTransactionId

            result.orderId = order.orderId
            result.orderType = order.orderType
            result.topayTransactionId = order.topayTransactionId
            result.buyerId = order.buyerId
            result.paymentType = order.paymentType
            result.paymentStatus = order.paymentStatus
            result.errorCode = order.errorCode
            result.totalPrice = order.totalPrice
            result.timeAdded = order.timeAdded
            result.timeUpdated = order.timeUpdated
            result.reserveTimeExpired = order.reserveTimeExpired

            result.deliveryStatus = order.deliveryStatus
            result.deliveryType = order.deliveryType
            result.deliveryPrice = order.deliveryPrice
            result.deliveryDepartmentId = order.deliveryDepartmentId

            result.trackingCode = order.trackingCode
            result.cashOnDeliveryCost = order.cashOnDeliveryCost
            result.deliveryComment = order.deliveryComment

            result.investInfo = order.investInfo
            result.promoCodeId = order.promoCodeId
            result.discount = order.discount

            return result
        }
    }
}

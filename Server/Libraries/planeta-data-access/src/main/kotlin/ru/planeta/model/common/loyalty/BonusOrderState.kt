package ru.planeta.model.common.loyalty

/**
 * Created with IntelliJ IDEA.
 * User: a.volkov
 * Date: 07.04.14
 * Time: 16:27
 */
enum class BonusOrderState {
    NEW, COMPLETED, CANCELED
}

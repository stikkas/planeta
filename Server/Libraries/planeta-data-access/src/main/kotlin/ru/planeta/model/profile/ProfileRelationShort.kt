package ru.planeta.model.profile

import ru.planeta.model.enums.ProfileRelationStatus

import java.util.EnumSet

/**
 * Created by kostiagn on 31.03.2015.
 */
class ProfileRelationShort(var profileId: Long, relationStatusCode: Int) {
    var relationStatuses: EnumSet<ProfileRelationStatus>? = null

    init {
        this.relationStatuses = ProfileRelationStatus.Companion.getEnumSetFromCode(relationStatusCode)
    }
}

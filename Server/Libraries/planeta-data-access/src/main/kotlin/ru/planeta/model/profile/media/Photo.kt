package ru.planeta.model.profile.media

import ru.planeta.model.enums.ObjectType
import ru.planeta.model.profile.ICommentable
import ru.planeta.model.profile.IVotable
import ru.planeta.model.profile.ProfileObject

import java.util.Date

/**
 * Common profile photo
 *
 * @author ds.kolyshev
 * Date: 27.10.11
 */
class Photo : ProfileObject(), ICommentable, IVotable {

    var albumId: Long = 0
    var description: String? = null
    var imageUrl: String? = null
    var viewsCount: Int = 0
    var timeAdded: Date? = null
    var photoAlbumName: String? = null
    var photoAlbumTypeId: Int = 0

    var photoId: Long
        get() = voteObjectId
        set(photoId) {
            voteObjectId = photoId
        }

    override var voteObjectType: ObjectType?
        get() = ObjectType.PHOTO
        set(value: ObjectType?) {
            super.voteObjectType = value
        }

    override var objectId: Long? = null
        get() = photoId

    override var objectType: ObjectType?
        get() = ObjectType.PHOTO
        set(value: ObjectType?) {
            super.objectType = value
        }

    companion object {

        private val serialVersionUID = 1L
    }
}

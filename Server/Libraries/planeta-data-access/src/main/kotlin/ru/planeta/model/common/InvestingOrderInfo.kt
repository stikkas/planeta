package ru.planeta.model.common

import com.fasterxml.jackson.annotation.JsonCreator
import com.fasterxml.jackson.annotation.JsonFormat
import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.databind.ObjectMapper

import java.io.IOException
import java.io.Serializable
import java.util.ArrayList
import java.util.Arrays
import java.util.Date

import ru.planeta.commons.model.Gender
import ru.planeta.model.enums.ContractorType
import ru.planeta.model.enums.ModerateStatus

/**
 * Дополнительная информация о покупках на инвестпроекте.
 * <h4>PLANETA-15487</h4>
 *
 *
 * User: Serge Blagodatskih<stikkas17></stikkas17>@gmail.com><br></br>
 * Date: 27.01.16<br></br>
 * Time: 13:00
 */
@JsonIgnoreProperties(ignoreUnknown = true)
class InvestingOrderInfo : Serializable {

    /**
     * id заказа<br></br>
     * ссылка на соответствующий orders.order_id
     */
    @JsonIgnore
    var investingOrderInfoId: Long? = null

    /**
     * Дата добавления записи
     */
    @JsonIgnore
    var timeAdded: Date? = null

    /**
     * тип покупателя<br></br>
     * физ.лицо или юр.лицо
     */
    var userType: ContractorType? = null

    /**
     * телефон (ФЛ, ЮЛ) <br></br>
     * +7 и еще 10 цифр
     */
    var phoneNumber: String? = null

    /**
     * код подтверждения телефона (ФЛ, ЮЛ) <br></br>
     * в базу не кладется
     */
    var confirmationCode: String? = null

    /**
     * фамилия покупателя (ФЛ) или руководителя (ЮЛ)
     */
    var lastName: String? = null

    /**
     * имя покупателя (ФЛ) или руководителя (ЮЛ)
     */
    var firstName: String? = null

    /**
     * отчество покупателя (ФЛ) или руководителя (ЮЛ)
     */
    var middleName: String? = null

    /**
     * должность руководителя (ЮЛ)
     */
    var bossPosition: String? = null

    /**
     * пол (ФЛ)
     */
    var gender: Gender? = Gender.NOT_SET

    /**
     * полное наименование юрлица (ЮЛ)
     */
    var orgName: String? = null

    /**
     * сокращенное наименование юрлица или бренд (ЮЛ)
     */
    var orgBrand: String? = null

    /**
     * дата рождения (ФЛ) или дата регистрации (ЮЛ)
     */
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd.MM.yyyy")
    var birthDate: Date? = null

    /**
     * место рождения (ФЛ)
     */
    var birthPlace: String? = null

    /**
     * индекс регистрации (ФЛ) или юридический индекс (ЮЛ)
     */
    var regIndex: String? = null

    /**
     * страна регистрации (ФЛ) или юридический страна (ЮЛ)
     */
    var regCountry: String? = null

    /**
     * id страна регистрации (ФЛ) или юридический страна (ЮЛ)
     */
    var regCountryId: Long = 0

    /**
     * регион регистрации (ФЛ) или юридический регион (ЮЛ)
     */
    var regRegion: String? = null

    /**
     * id регион регистрации (ФЛ) или юридический регион (ЮЛ)
     */
    var regRegionId: Long = 0

    /**
     * населенный пункт регистрации (ФЛ) или юридический населенный пункт (ЮЛ)
     */
    var regLocation: String? = null

    /**
     * адрес регистрации (ФЛ) или юридический адрес (ЮЛ)
     */
    var regAddress: String? = null

    /**
     * индекс проживания (ФЛ) или фактический индекс (ЮЛ)
     */
    var liveIndex: String? = null

    /**
     * страна проживания (ФЛ) или фактический страна(ЮЛ)
     */
    var liveCountry: String? = null

    /**
     * id страна проживания (ФЛ) или фактический страна(ЮЛ)
     */
    var liveCountryId: Long = 0

    /**
     * регион проживания (ФЛ) или фактический регион(ЮЛ)
     */
    var liveRegion: String? = null

    /**
     * id регион проживания (ФЛ) или фактический регион(ЮЛ)
     */
    var liveRegionId: Long = 0

    /**
     * населенный пункт проживания (ФЛ) или фактический населенный пункт(ЮЛ)
     */
    var liveLocation: String? = null

    /**
     * адрес проживания (ФЛ) или фактический адрес (ЮЛ)
     */
    var liveAddress: String? = null

    /**
     * серия и номер паспорта (ФЛ)
     */
    var passportNumber: String? = null

    /**
     * кем выдан паспорт (ФЛ)
     */
    var passportIssuer: String? = null

    /**
     * когда выдан паспорт (ФЛ)
     */
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd.MM.yyyy")
    var passportIssueDate: Date? = null

    /**
     * код подразделения, выдавшего паспорт (ФЛ)
     */
    var passportIssuerCode: String? = null

    /**
     * инн (ЮЛ)<br></br>
     * 10 или 12 цифр
     */
    var inn: String? = null

    /**
     * сайт (ЮЛ)
     */
    var webSite: String? = null

    /**
     * статус модерации
     */
    var moderateStatus: ModerateStatus? = ModerateStatus.NEW

    var smsCode: String? = null

    var userTypeCode: Int
        @JsonIgnore
        get() = if (userType != null) userType!!.code else 0
        set(code) {
            this.userType = ContractorType.getByValue(code)
        }

    var genderCode: Int
        @JsonIgnore
        get() = if (gender != null) gender!!.code else 0
        set(code) {
            this.gender = Gender.getByValue(code)
        }

    var moderateStatusCode: Int
        @JsonIgnore
        get() = if (moderateStatus != null) moderateStatus!!.code else 0
        set(code) {
            this.moderateStatus = ModerateStatus.getByValue(code)
        }

    constructor() {}

    constructor(userType: ContractorType) {
        this.userType = userType
    }

    constructor(id: Long?, timeAdded: Date) {
        investingOrderInfoId = id
        this.timeAdded = timeAdded
    }

    companion object {

        @JsonCreator
        @Throws(IOException::class)
        fun create(json: String): InvestingOrderInfo {
            return ObjectMapper().readValue(json, InvestingOrderInfo::class.java)
        }

        fun getRequiredFields(userType: ContractorType): List<String> {
            val fields = ArrayList<String>()
            fields.addAll(Arrays.asList(
                    "phoneNumber",
                    "firstName",
                    "lastName",
                    "middleName",
                    "birthDate",
                    "regRegion",
                    "regLocation",
                    "regAddress",
                    "regIndex",
                    "liveRegion",
                    "liveLocation",
                    "liveAddress",
                    "liveIndex"
            ))
            when (userType) {
                ContractorType.INDIVIDUAL -> fields.addAll(Arrays.asList("birthPlace",
                        "passportNumber", "passportIssuer",
                        "passportIssueDate", "passportIssuerCode"))
                ContractorType.LEGAL_ENTITY -> fields.addAll(Arrays.asList("orgName", "inn", "bossPosition"))
            }
            return fields
        }
    }
}

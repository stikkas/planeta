package ru.planeta.model.profile.broadcast.enums

import java.util.HashMap

/**
 * Broadcast subscription status enumeration
 *
 * @author ds.kolyshev
 * Date: 15.05.13
 */
enum class BroadcastSubscriptionStatus private constructor(val code: Int) {
    NOT_STARTED(0), PROGRESS(1), SENT(2);


    companion object {

        private val lookup = HashMap<Int, BroadcastSubscriptionStatus>()

        init {
            for (s in values()) {
                lookup[s.code] = s
            }
        }

        fun getByValue(code: Int?): BroadcastSubscriptionStatus? {
            return lookup[code]
        }
    }
}

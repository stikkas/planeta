package ru.planeta.model.enums

import java.util.HashMap

/**
 * deprecated
 *
 * @author m.shulepov
 */
@Deprecated("")
enum class EventType private constructor(val code: Int) {

    NOT_SET(0),
    CONCERT(1),
    EXHIBITION(2),
    PERFORMANCE(3),
    FESTIVAL(4),
    SHOW(5),
    MOVIE(6),
    LECTURE(7),
    MEETING(8),
    OTHER(9);


    companion object {

        private val lookup = HashMap<Int, EventType>()

        init {
            for (s in values()) {
                lookup[s.code] = s
            }
        }

        fun getByValue(code: Int): EventType? {
            return lookup[code]
        }
    }
}

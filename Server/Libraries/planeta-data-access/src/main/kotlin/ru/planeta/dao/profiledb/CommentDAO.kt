package ru.planeta.dao.profiledb

import org.apache.ibatis.annotations.Mapper
import org.apache.ibatis.annotations.Param
import ru.planeta.model.enums.ObjectType
import ru.planeta.model.profile.Comment
import ru.planeta.model.shop.ProductCommentDTOListWithFullCount
import java.util.Date

/**
 * DAO for comments
 *
 * @author a.savanovich
 */
@Mapper
interface CommentDAO {

    /**
     * Selects list of child blog post's comments
     */
    fun selectComments(@Param("clientId") clientId: Long, @Param("profileId") profileId: Long, @Param("objectId") objectId: Long, @Param("objectType") objectType: ObjectType, @Param("parentCommentId") parentCommentId: Long, @Param("offset") offset: Int, @Param("limit") limit: Int, @Param("sort") sort: String): List<Comment>

    /**
     * Selects list of child blog post's comments with id > lastCommentId
     */
    fun selectComments(@Param("clientId") clientId: Long, @Param("profileId") profileId: Long, @Param("objectId") objectId: Long, @Param("objectType") objectType: ObjectType, @Param("parentCommentId") parentCommentId: Long, @Param("offset") offset: Int, @Param("limit") limit: Int, @Param("sort") sort: String, @Param("lastCommentId") lastCommentId: Long): List<Comment>

    /**
     * Selects list of blog post's comments with child comments
     */
    fun selectCommentsWithChilds(@Param("clientId") clientId: Long, @Param("ownerId") ownerId: Long, @Param("objectId") objectId: Long, @Param("objectType") objectType: ObjectType, @Param("parentCommentId") parentCommentId: Long, @Param("countOfChildComments") countOfChildComments: Int, @Param("offset") offset: Int, @Param("limit") limit: Int, @Param("sort") sort: String): List<Comment>

    /**
     * Selects list of child blog post's comments with id > lastCommentId
     */
    fun selectCommentsWithChilds(@Param("clientId") clientId: Long, @Param("ownerId") ownerId: Long, @Param("objectId") objectId: Long, @Param("objectType") objectType: ObjectType, @Param("parentCommentId") parentCommentId: Long, @Param("countOfChildComments") countOfChildComments: Int, @Param("offset") offset: Int, @Param("limit") limit: Int, @Param("sort") sort: String, @Param("lastCommentId") lastCommentId: Long): List<Comment>

    /**
     * Selected specified comment
     */
    fun selectCommentById(@Param("ownerId") ownerId: Long, @Param("commentId") commentId: Long): Comment?

    /**
     * Selects list of comments created after "startCommentId"
     */
    fun selectLastComments(@Param("profileId") profileId: Long, @Param("objectId") objectId: Long, @Param("objectType") objectType: ObjectType, @Param("startCommentId") startCommentId: Long, @Param("sort") sort: String, @Param("offset") offset: Int, @Param("limit") limit: Int): List<Comment>

    /**
     * Inserts new group object comment
     */
    fun insert(comment: Comment)

    /**
     * Updates specified comment
     */
    fun update(comment: Comment)

    /**
     * Set that comment is deleted, not removed from db
     */
    fun delete(@Param("profileId") profileId: Long, @Param("commentId") commentId: Long)

    /**
     * Restore deleteByProfileId comment
     */
    fun restore(@Param("profileId") profileId: Long, @Param("commentId") commentId: Long)

    /**
     * Returns last not deleted comment identifier
     */
    fun getLastNotDeletedCommentId(@Param("profileId") profileId: Long, @Param("objectId") objectId: Long, @Param("objectType") objectType: ObjectType): Long

    /**
     * returns the number of comments
     *
     * @param profileId       - id of the user who is the object's owner
     * @param objectId        - object id
     * @param objectType      - object type
     * @param parentCommentId - parent comment id
     */
    fun getCommentsCount(@Param("profileId") profileId: Long, @Param("objectId") objectId: Long, @Param("objectType") objectType: ObjectType, @Param("parentCommentId") parentCommentId: Long): Int

    fun getCommentsCount(@Param("objectId") objectId: Long, @Param("objectType") objectType: ObjectType): Int

    fun selectCommentsWithChildAsPlain(@Param("clientId") clientId: Long, @Param("profileId") profileId: Long, @Param("objectId") objectId: Long, @Param("objectType") objectType: ObjectType, @Param("offset") offset: Int, @Param("limit") limit: Int): List<Comment>

    fun selectCommentsForAdminByDate(@Param("query") query: String, @Param("from") from: Date, @Param("to") to: Date, @Param("limit") limit: Int, @Param("offset") offset: Int): ProductCommentDTOListWithFullCount
}

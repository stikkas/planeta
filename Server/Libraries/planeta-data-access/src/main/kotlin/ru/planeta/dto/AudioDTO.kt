package ru.planeta.dto

class AudioDTO {
    /*
    audio track identifier
     */
    var trackId : Long? = null
    /*
    audio track url
     */
    var trackUrl : String? = null
}

package ru.planeta.model.shop

import java.util.Date

/**
 * Created with IntelliJ IDEA.
 * Date: 15.09.2015
 * Time: 15:51
 * To change this template use File | Settings | File Templates.
 */
class ProductCommentDTO {
    var productId: Long = 0
    var commentText: String? = null
    var timeAdded: Date? = null
    var productName: String? = null
    var productNameHtml: String? = null
}

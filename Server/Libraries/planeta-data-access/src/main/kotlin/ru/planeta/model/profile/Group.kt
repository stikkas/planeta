package ru.planeta.model.profile

import com.fasterxml.jackson.annotation.JsonIgnoreProperties

import java.io.Serializable
import java.util.Date

/**
 * Group info
 *
 * @author ds.kolyshev
 * Date: 27.10.11
 */
@JsonIgnoreProperties("contactInfo")
class Group : ProfileObject(), Serializable {

    var genre: String? = null
    var biography: String? = null
    var biographyHTML: String? = null
    var contactInfo: String? = null
    var siteUrl: String? = null
    var timeAdded: Date? = null
    var imageId: Long = 0
    var imageUrl: String? = null
    var backgroundUrl: String? = null

    companion object {

        private const val serialVersionUID = 1L
    }
}

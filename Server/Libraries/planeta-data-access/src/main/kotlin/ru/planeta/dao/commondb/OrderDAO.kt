package ru.planeta.dao.commondb

import org.apache.ibatis.annotations.Mapper
import org.apache.ibatis.annotations.Param
import ru.planeta.model.common.Order
import ru.planeta.model.common.OrderInfo
import ru.planeta.model.common.OrderInfoForProfilePage
import ru.planeta.model.common.OrderInfoForReport
import ru.planeta.model.common.campaign.CampaignSearchFilter
import ru.planeta.model.common.delivery.SubjectType
import ru.planeta.model.enums.OrderObjectType
import ru.planeta.model.shop.enums.DeliveryStatus
import ru.planeta.model.shop.enums.PaymentStatus
import java.util.*

/**
 * Interface OrderDAO
 *
 * @author a.tropnikov
 */
@Mapper
interface OrderDAO {

    /**
     * Selects order by specified identifier.
     *
     * @param orderId
     * @return
     */
    fun select(orderId: Long): Order?


    fun selectForUpdate(orderId: Long): Order?

    /**
     * Selects orders by cancel debit transaction identifiers.
     *
     * @param transactionIds transaction identifiers;
     * @return
     */
    fun selectOrdersByBuyerDebitTransactionIds(@Param("transactionIds") transactionIds: Collection<Long>): List<Order>

    /**
     * Selects orders by credit transaction identifiers.
     *
     * @param transactionIds transaction identifiers;
     * @return
     */
    fun selectOrdersByBuyerCreditTransactionIds(@Param("transactionIds") transactionIds: Collection<Long>): List<Order>

    /**
     * Inserts specified order.
     *
     * @param order
     * @return
     */
    fun insert(order: Order)

    /**
     * Updates specified order.
     *
     * @param order
     */
    fun update(order: Order)

    /**
     * Deletes order by identifier.
     *
     * @param orderId
     */
    fun delete(orderId: Long)

    /**
     * @param buyerId
     * @param objectId
     * @param objectType
     * @return
     */
    fun selectUserOrdersCount(@Param("buyerId") buyerId: Long, @Param("objectId") objectId: Long,
                              @Param("objectType") objectType: OrderObjectType): Int

    /**
     * Selects list of orders for selected buyer with specified order object type for order objects
     *
     * @param buyerId
     * @param objectTypes
     * @param paymentStatus
     * @param deliveryStatus
     * @param dateFrom
     * @param dateTo
     * @param offset
     * @param limit
     * @return
     */
    fun selectBuyerOrders(@Param("buyerId") buyerId: Long, @Param("objectTypes") objectTypes: Collection<OrderObjectType>,
                          @Param("paymentStatus") paymentStatus: PaymentStatus?,
                          @Param("deliveryStatus") deliveryStatus: DeliveryStatus?, @Param("dateFrom") dateFrom: Date?,
                          @Param("dateTo") dateTo: Date?, @Param("offset") offset: Int, @Param("limit") limit: Int): List<Order>

    fun selectOrdersAfterTime(@Param("buyerId") buyerId: Long, @Param("objectTypes") objectTypes: Collection<OrderObjectType>,
                              @Param("paymentStatus") paymentStatus: PaymentStatus,
                              @Param("dateFrom") dateFrom: Date, @Param("offset") offset: Int, @Param("limit") limit: Int): List<Order>

    /**
     * Selects list of orders for selected merchant with specified order object type for order objects
     *
     * @param merchantId
     * @param objectType
     * @param paymentStatus
     * @param deliveryStatus
     * @param dateFrom
     * @param dateTo
     * @param offset
     * @param limit
     * @return
     */
    fun getMerchantOrdersInfo(@Param("merchantId") merchantId: Long, @Param("objectType") objectType: OrderObjectType?,
                              @Param("paymentStatus") paymentStatus: PaymentStatus?,
                              @Param("deliveryStatus") deliveryStatus: DeliveryStatus?,
                              @Param("dateFrom") dateFrom: Date?, @Param("dateTo") dateTo: Date?,
                              @Param("offset") offset: Int, @Param("limit") limit: Int): List<Order>

    fun selectOrders(@Param("orderIds") orderIds: Collection<Long>?, @Param("orderObjectType") orderObjectType: OrderObjectType, @Param("paymentStatus") paymentStatus: PaymentStatus, @Param("from") from: Date, @Param("to") to: Date, @Param("offset") offset: Int, @Param("limit") limit: Int): List<Order>

    fun selectOrders(@Param("email") email: String, @Param("orderObjectType") orderObjectType: OrderObjectType, @Param("paymentStatus") paymentStatus: PaymentStatus, @Param("from") from: Date, @Param("to") to: Date, @Param("offset") offset: Int, @Param("limit") limit: Int): List<Order>

    fun selectOrdersCount(@Param("buyerId") buyerId: Long, @Param("merchantId") merchantId: Long, @Param("objectType") objectType: OrderObjectType, @Param("paymentStatuses") paymentStatuses: Array<PaymentStatus>, @Param("deliveryStatus") deliveryStatus: DeliveryStatus, @Param("dateFrom") dateFrom: Date, @Param("dateTo") dateTo: Date): Int

    /**
     * Selects list of filtered orders for campaign
     *
     * @param campaignSearchFilter@return Orders list
     */
    fun selectFilteredOrdersForCampaign(campaignSearchFilter: CampaignSearchFilter): List<Order>

    fun selectFilteredCampaignOrdersForReport(campaignSearchFilter: CampaignSearchFilter): List<OrderInfoForReport>

    /**
     * list of orders for object
     *
     * @param orderObjectId  object id
     * @param objectType     object type
     * @param deliveryStatus delivery status to filter or null
     * @param paymentStatus  payment status to filter or null
     * @return Orders list
     */
    fun selectOrdersByObject(@Param("objectId") orderObjectId: Long, @Param("objectType") objectType: OrderObjectType, @Param("deliveryStatus") deliveryStatus: DeliveryStatus?, @Param("paymentStatus") paymentStatus: PaymentStatus?): List<Order>

    fun selectOrdersCountForObject(@Param("objectId") objectId: Long, @Param("objectType") objectType: OrderObjectType, @Param("paymentStatuses") paymentStatuses: EnumSet<PaymentStatus>, @Param("deliveryStatuses") deliveryStatuses: EnumSet<DeliveryStatus>?): Int

    fun selectOrdersInforForOHMPage(@Param("offset") offset: Int, @Param("limit") limit: Int): List<Map<String, Any>>

    fun selectOrdersByIds(orderIds: Collection<Long>): List<Order>

    fun selectUserOrdersRewardsInfo(@Param("buyerId") buyerId: Long, @Param("offset") offset: Int, @Param("limit") limit: Int): List<OrderInfoForProfilePage>

    fun isCharityOrder(orderId: Long): Boolean

    fun updateProfileBackedCount(buyerId: Long)

    fun selectCampaignIdByOrderId(orderId: Long): Long?

    fun selectMinCompletedOrderId(@Param("buyerId") buyerId: Long, @Param("objectType") objectType: OrderObjectType): Long

    fun searchPurchasesOrders(@Param("query") query: String?, @Param("buyerId") buyerId: Long, @Param("objectTypes") orderObjectTypes: OrderObjectType?, @Param("offset") offset: Int, @Param("limit") limit: Int): List<Order>

    fun selectOrderInfoCommonData(@Param("orderId") orderId: Long, @Param("orderObjectId") orderObjectId: Long,
                                  @Param("deliveryServiceId") deliveryServiceId: Long, @Param("subjectType") subjectType: SubjectType): OrderInfo

    fun selectBuyerLastBonus(@Param("buyerId") buyerId: Long, @Param("date") date: Date): Order
}

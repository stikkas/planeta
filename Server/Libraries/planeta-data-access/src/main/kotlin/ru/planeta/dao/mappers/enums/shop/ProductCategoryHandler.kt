package ru.planeta.dao.mappers.enums.shop

import org.apache.ibatis.type.JdbcType
import org.apache.ibatis.type.TypeHandler
import ru.planeta.model.shop.enums.ProductCategory

import java.sql.CallableStatement
import java.sql.PreparedStatement
import java.sql.ResultSet
import java.sql.SQLException

/**
 * Created by IntelliJ IDEA.
 * User: Andrew
 * Date: 14.09.12
 * Time: 0:21
 */
class ProductCategoryHandler : TypeHandler<ProductCategory> {

    @Throws(SQLException::class)
    override fun setParameter(preparedStatement: PreparedStatement, i: Int, productCategory: ProductCategory, jdbcType: JdbcType) {
        preparedStatement.setInt(i, productCategory.code)
    }

    @Throws(SQLException::class)
    override fun getResult(resultSet: ResultSet, s: String): ProductCategory? {
        return ProductCategory.getByValue(resultSet.getInt(s))
    }

    @Throws(SQLException::class)
    override fun getResult(callableStatement: CallableStatement, i: Int): ProductCategory? {
        return ProductCategory.getByValue(callableStatement.getInt(i))  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Throws(SQLException::class)
    override fun getResult(rs: ResultSet, columnIndex: Int): ProductCategory? {
        return ProductCategory.getByValue(rs.getInt(columnIndex))
    }
}

package ru.planeta.model.profile.location

import java.io.Serializable

/**
 * @author Andrew.Arefyev@gmail.com
 * 29.10.13 15:02
 */
class Region : BaseLocation(), Serializable {

    override var locationId: Int = 0
    override var name: String? = null
    var nameEn: String? = null
    var countryId: Int = 0

    override var locationType: LocationType? = null
        get() = LocationType.REGION

    val regionId: Long
        get() = locationId.toLong()

    override var parentLocationType: LocationType? = null
        get() = LocationType.COUNTRY

    fun setRegionId(regionId: Int) {
        this.locationId = regionId
    }

    override var parentLocationId: Int? = countryId
        get() = countryId
}

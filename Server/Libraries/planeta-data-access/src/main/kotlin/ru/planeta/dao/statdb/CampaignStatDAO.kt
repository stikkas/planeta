package ru.planeta.dao.statdb

import org.apache.ibatis.annotations.Param
import ru.planeta.model.stat.CampaignStat

import java.util.Date

/**
 * Created with IntelliJ IDEA.
 * User: a.volkov
 * Date: 02.09.2014
 * Time: 19:04
 */
interface CampaignStatDAO {

    fun aggregate()

    fun selectGeneralStats(@Param("campaignId") campaignId: Long, @Param("from") from: Date, @Param("to") to: Date, @Param("offset") offset: Int, @Param("limit") limit: Int): List<CampaignStat>

    fun selectGeneralTotalStats(@Param("campaignId") campaignId: Long, @Param("from") from: Date, @Param("to") to: Date): CampaignStat

    fun selectRefererStats(@Param("campaignId") campaignId: Long, @Param("from") from: Date, @Param("to") to: Date, @Param("offset") offset: Int, @Param("limit") limit: Int): List<CampaignStat>

    fun selectCountryStats(@Param("campaignId") campaignId: Long, @Param("from") from: Date, @Param("to") to: Date, @Param("offset") offset: Int, @Param("limit") limit: Int): List<CampaignStat>

    fun selectCityStats(@Param("campaignId") campaignId: Long, @Param("from") from: Date, @Param("to") to: Date, @Param("offset") offset: Int, @Param("limit") limit: Int): List<CampaignStat>
}

package ru.planeta.dao.promo

import org.apache.ibatis.annotations.Param
import ru.planeta.model.promo.TechnobattleProject

/**
 * User: michail
 * Date: 16.01.2017
 * Time: 18:44
 */
interface TechnobattleProjectDAO {
    fun selectList(@Param("offset") offset: Int, @Param("limit") limit: Int): List<TechnobattleProject>

    fun selectOne(projectId: Long): TechnobattleProject

    fun insert(project: TechnobattleProject)

    fun update(project: TechnobattleProject)
}

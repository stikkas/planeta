package ru.planeta.model.bibliodb

import java.math.BigDecimal

class MagazineInfo {
    var magazineInfoId: Long = 0
    var priceListId: Long = 0
    var magazineName: String? = null
    private var pIndex: String? = null
    var msp: Int = 0
    var periodicity: Int = 0
    var partPriceWithoutTax = BigDecimal.ZERO
    var taxPercentage = BigDecimal.ZERO
    var partPriceWithTax = BigDecimal.ZERO
    var commentNoMagazineMonth: String? = null

    init {
        this.partPriceWithoutTax = BigDecimal.ZERO
        this.taxPercentage = BigDecimal.ZERO
        this.partPriceWithTax = BigDecimal.ZERO
    }

    fun getpIndex(): String? {
        return pIndex
    }

    fun setpIndex(pIndex: String) {
        this.pIndex = pIndex
    }

    override fun toString(): String {
        return "DeliveryAddress{" +
                "magazineInfoId=" + magazineInfoId +
                ", priceListId=" + priceListId +
                ", magazineName='" + magazineName + '\''.toString() +
                ", pIndex=" + pIndex +
                ", msp=" + msp +
                ", periodicity=" + periodicity +
                ", partPriceWithoutTax=" + partPriceWithoutTax +
                ", taxPercentage=" + taxPercentage +
                ", partPriceWithTax=" + partPriceWithTax +
                ", commentNoMagazineMonth='" + commentNoMagazineMonth + '\''.toString() +
                '}'.toString()
    }
}

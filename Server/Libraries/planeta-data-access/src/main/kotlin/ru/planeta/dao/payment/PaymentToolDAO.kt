package ru.planeta.dao.payment

import org.apache.ibatis.annotations.Mapper
import org.apache.ibatis.annotations.Param
import ru.planeta.model.common.PaymentTool
import ru.planeta.model.enums.ProjectType

import java.math.BigDecimal

/**
 * Created with IntelliJ IDEA.
 * User: a.volkov
 * Date: 24.04.14
 * Time: 18:13
 */
@Mapper
interface PaymentToolDAO {

    fun insert(tool: PaymentTool): PaymentTool

    fun selectAllPaymentTools(): List<PaymentTool>

    fun update(tool: PaymentTool): PaymentTool

    fun getTools(@Param("paymentMethodId") paymentMethodId: Long, @Param("projectType") projectType: ProjectType, @Param("amount") amount: BigDecimal, @Param("tlsSupported") tlsSupported: Boolean): List<PaymentTool>

    fun getToolByToolId(toolId: Long): PaymentTool
}

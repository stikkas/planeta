package ru.planeta.model.trashcan

/**
 * https://planeta.atlassian.net/browse/PLANETA-16072
 * User: Serge Blagodatskih<stikkas17></stikkas17>@gmail.com><br></br>
 * Date: 16.05.16<br></br>
 * Time: 14:27
 */
class UserGtmCid {

    var userId: Long = 0
    var googleCid: String? = null
}

package ru.planeta.model.profile

import ru.planeta.model.enums.ProfileType

class ProfileSubscriptionInfo : Profile {
    var isAdmin: Boolean = false
    var isNew: Boolean = false


    constructor(code: Int, profileType: ProfileType) : super(code, profileType) {}

    constructor() : super() {}
}

package ru.planeta.dao.commondb


import org.apache.ibatis.annotations.Mapper
import org.apache.ibatis.annotations.Param
import ru.planeta.model.common.faq.FaqSnippet

/**
 * Faq with full-text search
 * Atomic view element is paragraph (can be displayed in search as a snippet)
 * Next view element is article = concatenated paragraphs on one page
 * Upper-level view is categorized articles list with paragraphs names
 *
 * @author s.kalmykov
 * @since 27.08.2014
 */
@Mapper
interface FaqDAO {
    fun search(@Param("query") query: String, @Param("offset") offset: Int, @Param("limit") limit: Int): List<FaqSnippet>
}

--liquibase formatted sql

--changeset stikkas:1

SET search_path = charitydb, pg_catalog;

CREATE TABLE library_file (
    library_file_id bigint NOT NULL,
    theme_id bigint NOT NULL,
    header text NOT NULL,
    url text NOT NULL
);

CREATE TABLE library_file_theme (
    theme_id bigint NOT NULL,
    theme_header text NOT NULL
);

CREATE TABLE member (
    member_id bigint NOT NULL,
    member_name text,
    description text,
    phone_number text,
    email text,
    image_url text
);

CREATE TABLE news_tag (
    tag_id bigint NOT NULL,
    name_rus text,
    name_eng text,
    mnemonic_name text,
    for_news boolean,
    for_partner_news boolean
);

CREATE SEQUENCE seq_library_file_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE SEQUENCE seq_library_file_theme_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

ALTER TABLE ONLY library_file
    ADD CONSTRAINT library_file_pkey PRIMARY KEY (library_file_id);

ALTER TABLE ONLY library_file_theme
    ADD CONSTRAINT library_file_theme_pkey PRIMARY KEY (theme_id);



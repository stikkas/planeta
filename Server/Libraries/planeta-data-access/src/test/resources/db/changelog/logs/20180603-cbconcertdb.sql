--liquibase formatted sql

--changeset stikkas:1

SET search_path = concertdb, pg_catalog;
CREATE TABLE tickets (
    ticket_id bigint NOT NULL,
    concrete_concert_id bigint NOT NULL,
    sector_id bigint NOT NULL,
    "row" integer DEFAULT 0 NOT NULL,
    seat integer DEFAULT 0 NOT NULL,
    date date NOT NULL,
    create_date date DEFAULT now() NOT NULL,
    price integer NOT NULL
);

CREATE TABLE categories (
    category_id bigint NOT NULL,
    title character varying(100) NOT NULL,
    description character varying(8192) NOT NULL,
    priority integer DEFAULT 0 NOT NULL
);

CREATE TABLE concert (
    concert_id bigint NOT NULL,
    external_concert_id bigint,
    title text NOT NULL,
    image_url text,
    scheme_url text,
    description text,
    hall_id bigint,
    concert_date timestamp without time zone,
    age_restriction integer,
    status integer,
    time_added timestamp without time zone,
    time_updated timestamp without time zone
);

CREATE TABLE concerts (
    concert_id bigint NOT NULL,
    profile_id bigint NOT NULL,
    alias character varying(100) NOT NULL,
    title character varying(100) NOT NULL,
    image_url character varying(100) NOT NULL,
    min_price integer NOT NULL,
    max_price integer NOT NULL,
    max_date date NOT NULL,
    create_date date DEFAULT now() NOT NULL,
    short_descr character varying(1000) NOT NULL,
    descr character varying(8192) NOT NULL,
    age_limit integer DEFAULT 0 NOT NULL,
    featured boolean DEFAULT false
);

CREATE TABLE concrete_concerts (
    concrete_concert_id bigint NOT NULL,
    concert_id bigint NOT NULL,
    artist_id bigint NOT NULL,
    ticket_count integer NOT NULL,
    min_price integer NOT NULL,
    max_price integer NOT NULL,
    date timestamp(0) without time zone NOT NULL,
    etickets_possible boolean DEFAULT false,
    venue_id bigint NOT NULL,
    available_count integer DEFAULT (-1),
    create_date date,
    special_offer boolean DEFAULT false,
    contest boolean DEFAULT false,
    place_id bigint DEFAULT 0 NOT NULL,
    start_selling_date timestamp(0) without time zone NOT NULL,
    end_selling_date timestamp(0) without time zone NOT NULL
);

CREATE TABLE etickets (
    eticket_id bigint NOT NULL,
    concreteconcertid bigint NOT NULL,
    "row" integer DEFAULT 0 NOT NULL,
    seat integer DEFAULT 0 NOT NULL,
    date date NOT NULL,
    create_date date DEFAULT now() NOT NULL,
    price integer NOT NULL
);


CREATE TABLE external_categories (
    external_id bigint NOT NULL,
    external_alias text,
    category_id bigint NOT NULL,
    ticket_provider_id bigint NOT NULL
);

CREATE TABLE external_concerts (
    external_id bigint NOT NULL,
    external_alias text,
    concert_id bigint NOT NULL,
    ticket_provider_id bigint NOT NULL,
    max_price integer DEFAULT 0 NOT NULL,
    min_price integer DEFAULT 0 NOT NULL
);

CREATE TABLE external_concrete_concerts (
    external_id bigint NOT NULL,
    external_alias text,
    concrete_concert_id bigint NOT NULL,
    ticket_provider_id bigint NOT NULL,
    ticket_count integer DEFAULT 0 NOT NULL,
    etickets boolean DEFAULT false NOT NULL,
    max_price integer DEFAULT 0 NOT NULL,
    min_price integer DEFAULT 0 NOT NULL
);

CREATE TABLE external_tickets (
    external_id bigint NOT NULL,
    external_alias text,
    ticket_provider_id bigint NOT NULL,
    ticket_id bigint NOT NULL
);

CREATE TABLE hall (
    hall_id bigint NOT NULL,
    title text,
    description text,
    address text,
    scheme_url text
);


CREATE TABLE place (
    place_id bigint NOT NULL,
    external_place_id bigint,
    name text,
    external_concert_id bigint,
    section_id bigint,
    row_id bigint,
    "position" integer,
    price numeric(10,2),
    x integer,
    y integer,
    ticket_pdf_url text,
    ticket_code text,
    state integer,
    time_added timestamp without time zone,
    time_updated timestamp without time zone,
    external_unused_id integer,
    deleted boolean DEFAULT false
);


CREATE TABLE places (
    place_id bigint NOT NULL,
    row_number integer DEFAULT 0 NOT NULL,
    seat_number integer DEFAULT 0 NOT NULL,
    sector_id bigint NOT NULL
);

CREATE TABLE section (
    section_id bigint NOT NULL,
    external_section_id bigint,
    external_parent_id bigint,
    external_concert_id bigint,
    hall_id bigint,
    name text,
    deleted boolean DEFAULT false
);

CREATE TABLE sectors (
    sector_id bigint NOT NULL,
    name character varying(255),
    admission boolean DEFAULT false NOT NULL,
    place_count integer NOT NULL,
    venue_id bigint NOT NULL
);

CREATE SEQUENCE seq_category_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


CREATE SEQUENCE seq_concert_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


CREATE SEQUENCE seq_concrete_concert_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


CREATE SEQUENCE seq_eticket_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE SEQUENCE seq_hall_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE SEQUENCE seq_place_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE SEQUENCE seq_section_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE SEQUENCE seq_sector_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE SEQUENCE seq_tag_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE SEQUENCE seq_ticket_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE SEQUENCE seq_ticket_provider_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


CREATE SEQUENCE seq_venue_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE TABLE tags (
    tag_id bigint NOT NULL,
    title character varying(50) NOT NULL
);


CREATE TABLE venues (
    venue_id bigint NOT NULL,
    address_id bigint NOT NULL,
    image_url text,
    description text,
    longitude text,
    latitude text,
    profile_id bigint,
    name character varying(100),
    rating real DEFAULT 0,
    votes_count integer DEFAULT 0,
    capacity integer DEFAULT 0,
    phones character varying(100)
);

ALTER TABLE ONLY categories
    ADD CONSTRAINT categories_pkey PRIMARY KEY (category_id);

ALTER TABLE ONLY concert
    ADD CONSTRAINT concert_pkey PRIMARY KEY (concert_id);


ALTER TABLE ONLY concerts
    ADD CONSTRAINT concerts_pkey PRIMARY KEY (concert_id);

ALTER TABLE ONLY concrete_concerts
    ADD CONSTRAINT concrete_concerts_pkey PRIMARY KEY (concrete_concert_id);

ALTER TABLE ONLY etickets
    ADD CONSTRAINT etickets_pkey PRIMARY KEY (eticket_id);

ALTER TABLE ONLY external_categories
    ADD CONSTRAINT external_categories_pkey PRIMARY KEY (ticket_provider_id, category_id);

ALTER TABLE ONLY external_concerts
    ADD CONSTRAINT external_concerts_pkey PRIMARY KEY (concert_id, ticket_provider_id);

ALTER TABLE ONLY external_concrete_concerts
    ADD CONSTRAINT external_concrete_concerts_pkey PRIMARY KEY (ticket_provider_id, concrete_concert_id);

ALTER TABLE ONLY external_tickets
    ADD CONSTRAINT external_tickets_pkey PRIMARY KEY (ticket_provider_id, ticket_id);

ALTER TABLE ONLY hall
    ADD CONSTRAINT hall_pkey PRIMARY KEY (hall_id);

ALTER TABLE ONLY place
    ADD CONSTRAINT place_pkey PRIMARY KEY (place_id);

ALTER TABLE ONLY places
    ADD CONSTRAINT places_pkey PRIMARY KEY (place_id);

ALTER TABLE ONLY section
    ADD CONSTRAINT section_pkey PRIMARY KEY (section_id);

ALTER TABLE ONLY sectors
    ADD CONSTRAINT sectors_pkey PRIMARY KEY (sector_id);

ALTER TABLE ONLY tags
    ADD CONSTRAINT tags_pkey PRIMARY KEY (tag_id);

ALTER TABLE ONLY tickets
    ADD CONSTRAINT tickets_pkey PRIMARY KEY (ticket_id);

ALTER TABLE ONLY venues
    ADD CONSTRAINT venues_pkey PRIMARY KEY (venue_id);

CREATE UNIQUE INDEX concert_external_concert_id_idx ON concert USING btree (external_concert_id);

CREATE UNIQUE INDEX place_external_place_id_idx ON place USING btree (external_place_id);

CREATE UNIQUE INDEX place_external_section_id_row_id_position_idx ON place USING btree (section_id, row_id, "position");


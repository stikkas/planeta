--liquibase formatted sql

--changeset stikkas:1 endDelimiter:\nGO;
SET search_path = statdb, pg_catalog;

CREATE FUNCTION update_all_top_stats() RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN
PERFORM statdb.update_group_top_stats();
PERFORM statdb.update_audio_top_stats();
PERFORM statdb.update_video_top_stats();
PERFORM statdb.update_blog_top_stats();
PERFORM statdb.update_campaign_top_stats();
PERFORM statdb.update_audio_albums_top_stats();
PERFORM statdb.update_top_comments_stats();
RETURN;
END;
$$
GO;

CREATE FUNCTION update_audio_albums_top_stats() RETURNS void
    LANGUAGE plpgsql
    AS $$
DECLARE
  rec record;
  v_position INTEGER;
BEGIN
  DELETE FROM statdb.top_stats
  WHERE object_type_id = 7;
  v_position = 1;
  FOR rec IN (
  	SELECT profile_id, album_id AS object_id
    FROM statdb.audio_albums_general
    ORDER BY report_date DESC, hour DESC, listenings_count DESC
  )
  LOOP
  	IF v_position > 100 THEN 
    	EXIT;
    END IF;
    IF (SELECT profile_id
            FROM statdb.top_stats
           WHERE profile_id = rec.profile_id
             AND object_id = rec.object_id
             AND object_type_id = 7
        ) IS NULL THEN
        INSERT INTO statdb.top_stats (
			profile_id,
			object_id,
			object_type_id,
			position
		) VALUES (
			rec.profile_id,
			rec.object_id,
			7,
			v_position
		);
        v_position = v_position + 1;
    END IF;
  END LOOP;
  
  
RETURN;
END;
$$
GO;

CREATE FUNCTION update_audio_top_stats() RETURNS void
    LANGUAGE plpgsql
    AS $$
DECLARE
    rec record;
    v_position INTEGER;
BEGIN

DELETE FROM statdb.top_stats
      WHERE object_type_id = 9;

v_position = 0;
FOR rec IN ( SELECT profile_id,
             track_id AS object_id
        FROM statdb.audio_general
    ORDER BY report_date DESC, hour DESC, listenings_count DESC
    ) LOOP
    	v_position = v_position + 1;
        
      IF v_position > 100 THEN
        EXIT;  -- exit loop
	 END IF;
        
        
        IF (SELECT profile_id
            FROM statdb.top_stats
           WHERE profile_id = rec.profile_id
             AND object_id = rec.object_id
             AND object_type_id = 9
        ) IS NULL THEN
        INSERT INTO statdb.top_stats (
			profile_id,
			object_id,
			object_type_id,
			position
		) VALUES (
			rec.profile_id,
			rec.object_id,
			9,
			v_position
		);
        END IF;
    END LOOP;
     
RETURN;
END;
$$
GO;

CREATE FUNCTION update_blog_top_stats() RETURNS void
    LANGUAGE plpgsql
    AS $$
DECLARE
    rec record;
    v_position INTEGER;
	v_object_type_code INTEGER;
BEGIN

v_object_type_code = 4;

DELETE FROM statdb.top_stats
      WHERE object_type_id = v_object_type_code;

v_position = 0;
FOR rec IN ( SELECT profile_id,
             blog_post_id AS object_id
        FROM statdb.blog_general
    ORDER BY report_date DESC, hour DESC, views_count DESC
    ) LOOP
    	v_position = v_position + 1;
        
      IF v_position > 100 THEN
        EXIT;  -- exit loop
	 END IF;
        
        
        IF (SELECT profile_id
            FROM statdb.top_stats
           WHERE profile_id = rec.profile_id
             AND object_id = rec.object_id
             AND object_type_id = v_object_type_code
        ) IS NULL THEN
        INSERT INTO statdb.top_stats (
			profile_id,
			object_id,
			object_type_id,
			position
		) VALUES (
			rec.profile_id,
			rec.object_id,
			v_object_type_code,
			v_position
		);
        END IF;
    END LOOP;
     
RETURN;
END;
$$
GO;

CREATE FUNCTION update_campaign_top_stats() RETURNS void
    LANGUAGE plpgsql
    AS $$
DECLARE
    rec record;
    v_position INTEGER;
	v_object_type_code INTEGER;
BEGIN

v_object_type_code = 15;

DELETE FROM statdb.top_stats
      WHERE object_type_id = v_object_type_code;

v_position = 1;
FOR rec IN ( SELECT * FROM statdb.get_campaigns_stats()
    ) LOOP

        
      IF v_position > 100 THEN
        EXIT;  -- exit loop
	 END IF;
        
        
        IF (SELECT profile_id
            FROM statdb.top_stats
           WHERE profile_id = rec.profile_id
             AND object_id = rec.object_id
             AND object_type_id = v_object_type_code
        ) IS NULL THEN
        INSERT INTO statdb.top_stats (
			profile_id,
			object_id,
			object_type_id,
			position
		) VALUES (
			rec.profile_id,
			rec.object_id,
			v_object_type_code,
			v_position
		);
       	v_position = v_position + 1;
        END IF;
    END LOOP;
     
RETURN;
END;
$$
GO;

CREATE FUNCTION update_comments_stats() RETURNS void
    LANGUAGE plpgsql
    AS $$
DECLARE
    rec record;
    rec2 record;
    v_time TIMESTAMP;
    v_report_date DATE;
    v_hour INTEGER;
BEGIN

    v_time = now();
    SELECT report_date,
           hour
      FROM statdb.completed_jobs
     WHERE job_name = 'update_comments'
  ORDER BY report_date DESC, hour DESC
     LIMIT 1
      INTO v_report_date, v_hour;


    IF ( v_report_date >= v_time::DATE AND  v_hour >= EXTRACT(hour from v_time))
    THEN RETURN;
    END IF;

    FOR rec IN (SELECT * FROM statdb.get_comments_stats((v_report_date || ' ' || v_hour || ':00:00')::TIMESTAMP)
    ) LOOP

      IF (SELECT profile_id
            FROM statdb.blog_general
           WHERE profile_id = rec.owner_profile_id
             AND report_date = rec.time_added::DATE
             AND hour = EXTRACT(hour from rec.time_added)
             AND blog_post_id = rec.object_id
        ) IS NULL THEN
             INSERT INTO statdb.blog_general(
                  profile_id,
                  blog_post_id,
                  report_date,
                  hour,
                  views_count,
                  comments_count,
                  unique_commentators
           ) VALUES (
                  rec.owner_profile_id,
                  rec.object_id,
                  rec.time_added,
                  EXTRACT(hour from rec.time_added),
                  rec.comment_count,
                  rec.comment_count,
                  rec.commentators
           );
     ELSE
         UPDATE statdb.blog_general
            SET
                comments_count = rec.comment_count,
                unique_commentators = rec.commentators
          WHERE profile_id = rec.owner_profile_id
            AND report_date = rec.time_added
            AND hour = EXTRACT(hour from rec.time_added)
            AND blog_post_id = rec.object_id
              ;
   END IF;
 END LOOP;

INSERT INTO statdb.completed_jobs(
	job_name,
	report_date,
    hour
  ) VALUES (
  	'update_comments',
     v_time,
     EXTRACT(hour from v_time)     
  );

RETURN;
END;
$$
GO;

CREATE FUNCTION update_group_top_stats() RETURNS void
    LANGUAGE plpgsql
    AS $$
DECLARE
    rec record;
    v_position INTEGER;
	v_object_type_code INTEGER;
BEGIN

v_object_type_code = 1;

DELETE FROM statdb.top_stats
      WHERE object_type_id = v_object_type_code;

v_position = 1;
FOR rec IN ( SELECT t1.profile_id,
             0 AS object_id
        FROM statdb.group_general t1

	    JOIN ( SELECT * FROM statdb.get_top_groups_stats()) t2 ON t1.profile_id = t2.profile_id
   	   WHERE category_id = 1
    ORDER BY report_date DESC, hour DESC, views_count DESC
    ) LOOP
        
      IF v_position > 100 THEN
        EXIT;  -- exit loop
	 END IF;
        
        
        IF (SELECT profile_id
            FROM statdb.top_stats
           WHERE profile_id = rec.profile_id
             AND object_id = rec.object_id
             AND object_type_id = v_object_type_code
        ) IS NULL THEN
        INSERT INTO statdb.top_stats (
			profile_id,
			object_id,
			object_type_id,
			position
		) VALUES (
			rec.profile_id,
			rec.object_id,
			v_object_type_code,
			v_position
		);
        
       	v_position = v_position + 1;
        END IF;
    END LOOP;
     
RETURN;
END;
$$
GO;

CREATE FUNCTION update_top_comments_stats() RETURNS void
    LANGUAGE plpgsql
    AS $$
DECLARE
	v_object_type_id INTEGER := 16;
BEGIN
	DELETE FROM statdb.top_stats WHERE object_type_id = v_object_type_id;
    
    INSERT INTO statdb.top_stats (profile_id, object_id, object_type_id, position)
		 SELECT t1.owner_profile_id AS profile_id,
	   			t1.comment_id AS object_id,
       			16 AS object_type_id,
       			row_number() OVER (ORDER BY rating) AS position
  		   FROM statdb.get_top_comments_stats() t1;
END;
$$
GO;

CREATE FUNCTION update_video_top_stats() RETURNS void
    LANGUAGE plpgsql
    AS $$
DECLARE
    rec record;
    v_position INTEGER;
BEGIN

DELETE FROM statdb.top_stats
      WHERE object_type_id = 6;

v_position = 0;
FOR rec IN ( SELECT profile_id,
             video_id AS object_id
        FROM statdb.video_general
    ORDER BY report_date DESC, hour DESC, views_count DESC
    ) LOOP
    	v_position = v_position + 1;
        
      IF v_position > 100 THEN
        EXIT;  -- exit loop
	 END IF;
        
        
        IF (SELECT profile_id
            FROM statdb.top_stats
           WHERE profile_id = rec.profile_id
             AND object_id = rec.object_id
             AND object_type_id = 6
        ) IS NULL THEN
        INSERT INTO statdb.top_stats (
			profile_id,
			object_id,
			object_type_id,
			position
		) VALUES (
			rec.profile_id,
			rec.object_id,
			6,
			v_position
		);
        END IF;
    END LOOP;
     
RETURN;
END;
$$
GO;

CREATE FUNCTION update_votes_stats() RETURNS void
    LANGUAGE plpgsql
    AS $$
DECLARE
    rec record;
    rec2 record;
    v_time TIMESTAMP;
    v_report_date DATE;
    v_hour INTEGER;
BEGIN

    v_time = now();
    SELECT report_date,
           hour
      FROM statdb.completed_jobs
     WHERE job_name = 'update_votes'
  ORDER BY report_date DESC, hour DESC
     LIMIT 1
      INTO v_report_date , v_hour ;

    IF ( v_report_date >= v_time::DATE AND  v_hour >= EXTRACT(hour from v_time))
    THEN RETURN;
    END IF;

    FOR rec IN (SELECT * FROM statdb.get_votes_stats((v_report_date || ' ' || v_hour || ':00:00')::TIMESTAMP)
    ) LOOP

      IF (SELECT profile_id
            FROM statdb.blog_general
           WHERE profile_id = rec.owner_profile_id
             AND report_date = rec.time_added::DATE
             AND hour = EXTRACT(hour from rec.time_added)
             AND blog_post_id = rec.blog_post_id
        ) IS NULL THEN
             INSERT INTO statdb.blog_general(
                  profile_id,
                  blog_post_id,
                  report_date,
                  hour,
                  views_count,
                  unique_voters_count,
                  rating
           ) VALUES (
                  rec.owner_profile_id,
                  rec.blog_post_id,
                  rec.time_added,
                  EXTRACT(hour from rec.time_added),
                  abs(rec.rating),
                  abs(rec.rating),
                  rec.rating
           );
     ELSE
         UPDATE statdb.blog_general
            SET
                rating = rec.rating,
                unique_voters_count = rec.voters_count
          WHERE profile_id = rec.owner_profile_id
            AND report_date = rec.time_added::DATE
            AND hour = EXTRACT(hour from rec.time_added)
            AND blog_post_id = rec.blog_post_id
              ;
   END IF;
 END LOOP;

-- remember already counted in special row
INSERT INTO statdb.completed_jobs(
	job_name,
	report_date,
    hour
  ) VALUES (
  	'update_votes',
     v_time,
     EXTRACT(hour from v_time)     
  );

RETURN;
END;
$$
GO;

CREATE TABLE order_geo_stats (
    order_id bigint NOT NULL,
    ip_address bigint,
    country_id integer,
    city_id integer,
    time_updated timestamp without time zone,
    referrer_url text DEFAULT ''::text
)
GO;


CREATE TABLE affiliate_stats_campaign (
    date date NOT NULL,
    affiliate_id bigint NOT NULL,
    campaign_id bigint NOT NULL,
    visitors integer NOT NULL,
    views integer NOT NULL,
    purchases integer NOT NULL,
    affiliate_amount numeric DEFAULT 0 NOT NULL
)
GO;


CREATE TABLE affiliate_stats_general (
    date date NOT NULL,
    affiliate_id bigint NOT NULL,
    visitors integer NOT NULL,
    views integer NOT NULL,
    purchases integer NOT NULL,
    affiliate_amount numeric DEFAULT 0 NOT NULL
)
GO;


CREATE TABLE affiliate_stats_referer (
    date date NOT NULL,
    affiliate_id bigint NOT NULL,
    referer character varying(100) NOT NULL,
    visitors integer NOT NULL,
    views integer NOT NULL,
    purchases integer NOT NULL,
    affiliate_amount numeric DEFAULT 0 NOT NULL
)
GO;

CREATE TABLE affiliate_stats_source (
    date date NOT NULL,
    affiliate_id bigint NOT NULL,
    source_id bigint NOT NULL,
    visitors integer NOT NULL,
    views integer NOT NULL,
    purchases integer NOT NULL,
    affiliate_amount numeric DEFAULT 0 NOT NULL
)
GO;

CREATE TABLE boomstarter_stats (
    report_date timestamp(0) without time zone NOT NULL,
    project_url text NOT NULL,
    project_name text NOT NULL,
    project_category character varying(255) NOT NULL,
    project_backers integer NOT NULL,
    project_pledgesum integer NOT NULL,
    project_targetsum integer NOT NULL,
    project_daysleft character varying(255) NOT NULL,
    project_average numeric(10,2) NOT NULL,
    project_success integer NOT NULL,
    project_purchase_rate numeric(10,2) NOT NULL
)
GO;


CREATE TABLE campaign_after_purchase_feedback_letter (
    feedback_letter_id bigint NOT NULL,
    user_id bigint NOT NULL,
    order_id bigint NOT NULL,
    time_added timestamp without time zone DEFAULT now() NOT NULL,
    is_mail_sent boolean DEFAULT false NOT NULL,
    time_updated timestamp without time zone
)
GO;


CREATE TABLE campaign_stats_affiliate (
    date date NOT NULL,
    campaign_id bigint NOT NULL,
    affiliate_id bigint NOT NULL,
    visitors integer NOT NULL,
    views integer NOT NULL,
    purchases integer NOT NULL,
    affiliate_amount numeric DEFAULT 0 NOT NULL
)
GO;

CREATE TABLE campaign_stats_city (
    date date NOT NULL,
    campaign_id bigint NOT NULL,
    city character varying(100) NOT NULL,
    visitors integer NOT NULL,
    views integer NOT NULL,
    purchases integer NOT NULL,
    affiliate_amount numeric DEFAULT 0 NOT NULL
)
GO;

CREATE TABLE campaign_stats_country (
    date date NOT NULL,
    campaign_id bigint NOT NULL,
    country character varying(100) NOT NULL,
    visitors integer NOT NULL,
    views integer NOT NULL,
    purchases integer NOT NULL,
    affiliate_amount numeric DEFAULT 0 NOT NULL
)
GO;

CREATE TABLE campaign_stats_general (
    date date NOT NULL,
    campaign_id bigint NOT NULL,
    visitors integer NOT NULL,
    views integer NOT NULL,
    purchases integer NOT NULL,
    buyers integer NOT NULL,
    comments integer NOT NULL,
    amount numeric DEFAULT 0 NOT NULL
)
GO;

CREATE TABLE campaign_stats_ref (
    date date NOT NULL,
    campaign_id bigint NOT NULL,
    referer text NOT NULL,
    visitors integer NOT NULL,
    views integer NOT NULL,
    purchases integer NOT NULL,
    affiliate_amount numeric DEFAULT 0 NOT NULL
)
GO;

CREATE TABLE completed_jobs (
    job_name character varying(128) NOT NULL,
    report_date date NOT NULL,
    hour integer NOT NULL,
    comment text
)
GO;


CREATE TABLE db_log (
    id bigint NOT NULL,
    message text NOT NULL,
    logger_type_id integer DEFAULT 0 NOT NULL,
    object_id bigint DEFAULT 0 NOT NULL,
    subject_id bigint DEFAULT 0 NOT NULL,
    request_id bigint DEFAULT 0 NOT NULL,
    response_id bigint DEFAULT 0 NOT NULL,
    time_added bigint NOT NULL
)
GO;


CREATE TABLE http_interaction_log (
    id bigint NOT NULL,
    meta_data text NOT NULL,
    request_id bigint DEFAULT 0 NOT NULL,
    is_request boolean DEFAULT true NOT NULL,
    time_added bigint NOT NULL
)
GO;

CREATE TABLE order_shortlink_stats (
    shortlink_stats_id bigint NOT NULL,
    shortlink_id bigint NOT NULL,
    order_id bigint NOT NULL,
    referrer text,
    time_added timestamp without time zone DEFAULT now() NOT NULL
)
GO;

CREATE SEQUENCE seq_campaign_after_purchase_feedback_letter_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
GO;

CREATE SEQUENCE seq_db_log_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
GO;

CREATE SEQUENCE seq_http_interaction_log_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
GO;

CREATE SEQUENCE seq_order_shortlink_stats_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
GO;

CREATE SEQUENCE seq_shared_object_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
GO;

CREATE TABLE shared_object_detail_stats (
    shared_object_id bigint NOT NULL,
    author_profile_id bigint DEFAULT (-1) NOT NULL,
    share_service_type integer DEFAULT 0 NOT NULL,
    time_added timestamp without time zone DEFAULT now() NOT NULL,
    author_uuid character varying(100) DEFAULT NULL::character varying
)
GO;

CREATE TABLE shared_object_stats (
    shared_object_id bigint NOT NULL,
    url character varying(256) NOT NULL,
    total_shares integer DEFAULT 0 NOT NULL,
    time_added timestamp without time zone DEFAULT now() NOT NULL,
    time_updated timestamp without time zone DEFAULT now() NOT NULL
)
GO;


CREATE TABLE static_node (
    domain character varying NOT NULL,
    free_space_kb bigint NOT NULL,
    time_updated timestamp without time zone DEFAULT now() NOT NULL
)
GO;

CREATE TABLE top_stats (
    profile_id bigint NOT NULL,
    object_id bigint NOT NULL,
    object_type_id bigint NOT NULL,
    "position" integer NOT NULL
)
GO;

CREATE TABLE widgets_stats (
    type character varying(20) NOT NULL,
    campaign_id bigint NOT NULL,
    referer text NOT NULL,
    date date NOT NULL,
    count integer NOT NULL
)
GO;


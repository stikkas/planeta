--liquibase formatted sql

--changeset stikkas:1
CREATE SCHEMA bibliodb;
CREATE SCHEMA charitydb;
CREATE SCHEMA concertdb;
CREATE SCHEMA promodb;
CREATE SCHEMA shopdb;
CREATE SCHEMA trashcan;
CREATE SCHEMA statdb;
CREATE SCHEMA commondb;
CREATE SCHEMA maildb;
CREATE SCHEMA profiledb;

--changeset stikkas:2
SET search_path = bibliodb, pg_catalog;

CREATE SEQUENCE advertise_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE TABLE bin (
    bin_id bigint NOT NULL,
    bin_profile_id bigint,
    time_updated timestamp without time zone DEFAULT now() NOT NULL
);

CREATE TABLE bin_book_relation (
    book_id bigint NOT NULL,
    bin_id bigint NOT NULL,
    book_count integer NOT NULL,
    CONSTRAINT count_positive CHECK ((book_count > 0))
);

CREATE SEQUENCE bin_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE TABLE bin_library_relation (
    library_id bigint NOT NULL,
    bin_id bigint NOT NULL
);

CREATE TABLE book (
    book_id bigint NOT NULL,
    book_title text,
    book_description text,
    book_price numeric(10,2) NOT NULL,
    book_new_price numeric(10,2),
    book_image_url text,
    book_periodicity text,
    book_status integer NOT NULL,
    book_bonus text,
    book_change_price_date timestamp without time zone,
    book_comment text,
    book_publishing_house_id bigint NOT NULL,
    book_index integer DEFAULT 0 NOT NULL,
    book_email text,
    book_site text,
    book_contact text,
    time_added timestamp without time zone
);

CREATE SEQUENCE book_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE TABLE book_tag (
    book_tag_id bigint NOT NULL,
    book_tag_name character varying(255),
    book_tag_sort_order integer DEFAULT 0 NOT NULL,
    book_tag_mnemonic_name character varying(100) DEFAULT 'TEMP'::text NOT NULL,
    book_tag_primary boolean DEFAULT true NOT NULL
);

CREATE SEQUENCE book_tag_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE TABLE book_tag_relation (
    book_id bigint NOT NULL,
    book_tag_id bigint NOT NULL,
    tag_priority integer DEFAULT 0 NOT NULL
);

CREATE TABLE library (
    library_id bigint NOT NULL,
    library_status integer NOT NULL,
    library_region character varying NOT NULL,
    library_name character varying NOT NULL,
    library_address character varying,
    library_latitude numeric(18,15),
    library_longitude numeric(18,15),
    library_contractor character varying,
    library_email character varying,
    library_comment character varying,
    library_post_index character varying,
    library_type integer DEFAULT 1 NOT NULL,
    time_added timestamp without time zone,
    site text
);

CREATE SEQUENCE library_id_seq
    START WITH 49
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE TABLE magazine_info (
    magazine_info_id bigint NOT NULL,
    price_list_id bigint NOT NULL,
    magazine_name text NOT NULL,
    p_index text NOT NULL,
    msp integer NOT NULL,
    periodicity integer NOT NULL,
    part_price_without_tax numeric(10,2) NOT NULL,
    tax_percentage numeric(5,4) NOT NULL,
    part_price_with_tax numeric(10,2) NOT NULL,
    comment_no_magazine_month text
);

CREATE SEQUENCE magazine_info_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE TABLE magazine_price_list (
    price_list_id bigint NOT NULL,
    name text NOT NULL,
    time_added timestamp without time zone NOT NULL
);

CREATE SEQUENCE magazine_price_list_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE SEQUENCE partner_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE TABLE publishing_house (
    publishing_house_id bigint NOT NULL,
    publishing_house_name character varying(255),
    contractor_id integer
);

CREATE SEQUENCE publishing_house_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE TABLE request (
    request_id bigint NOT NULL,
    request_status integer,
    request_type integer,
    request_organization text,
    request_address text,
    request_contact text,
    request_email character varying(100),
    request_time_added timestamp without time zone
);

CREATE SEQUENCE request_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;



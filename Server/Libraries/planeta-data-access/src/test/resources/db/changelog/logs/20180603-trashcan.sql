--liquibase formatted sql
--changeset stikkas:1


SET search_path = trashcan, pg_catalog;

CREATE TABLE ab_shares_sort (
    ab_shares_sort_test_id bigint NOT NULL,
    sort_order integer NOT NULL,
    order_id bigint
);


CREATE TABLE ab_shares_sort_shows (
    show_id bigint NOT NULL,
    sort_order integer NOT NULL,
    show_count bigint NOT NULL
);


CREATE TABLE adblock (
    profile_id bigint NOT NULL,
    cid text NOT NULL,
    ip text NOT NULL,
    time_added timestamp without time zone DEFAULT now() NOT NULL
);



CREATE TABLE appeared_banner (
    id bigint NOT NULL,
    banner_id bigint NOT NULL,
    profile_id bigint NOT NULL,
    time_added timestamp without time zone DEFAULT now() NOT NULL,
    referer text
);



CREATE VIEW appear_banners_last_12_months AS
 SELECT count(cb.banner_id) AS cnt,
    b.name,
    cb.banner_id
   FROM (appeared_banner cb
     LEFT JOIN commondb.banners b ON ((cb.banner_id = b.banner_id)))
  WHERE ((cb.time_added >= ((now())::date - '1 year'::interval)) AND (cb.time_added <= ((now())::date + '1 day'::interval)))
  GROUP BY b.name, cb.banner_id;



CREATE VIEW appear_banners_last_6_months AS
 SELECT count(cb.banner_id) AS cnt,
    b.name,
    cb.banner_id
   FROM (appeared_banner cb
     LEFT JOIN commondb.banners b ON ((cb.banner_id = b.banner_id)))
  WHERE ((cb.time_added >= ((now())::date - '6 mons'::interval)) AND (cb.time_added <= ((now())::date + '1 day'::interval)))
  GROUP BY b.name, cb.banner_id;



CREATE VIEW appear_banners_last_7_days AS
 SELECT count(cb.banner_id) AS cnt,
    b.name,
    cb.banner_id
   FROM (appeared_banner cb
     LEFT JOIN commondb.banners b ON ((cb.banner_id = b.banner_id)))
  WHERE ((cb.time_added >= ((now())::date - '6 days'::interval)) AND (cb.time_added <= ((now())::date + '1 day'::interval)))
  GROUP BY b.name, cb.banner_id;



CREATE VIEW appear_banners_last_month AS
 SELECT count(cb.banner_id) AS cnt,
    b.name,
    cb.banner_id
   FROM (appeared_banner cb
     LEFT JOIN commondb.banners b ON ((cb.banner_id = b.banner_id)))
  WHERE ((cb.time_added >= ((now())::date - '1 mon'::interval)) AND (cb.time_added <= ((now())::date + '1 day'::interval)))
  GROUP BY b.name, cb.banner_id;



CREATE VIEW appear_banners_today AS
 SELECT count(cb.banner_id) AS cnt,
    b.name,
    cb.banner_id
   FROM (appeared_banner cb
     LEFT JOIN commondb.banners b ON ((cb.banner_id = b.banner_id)))
  WHERE ((cb.time_added >= (now())::date) AND (cb.time_added <= ((now())::date + '1 day'::interval)))
  GROUP BY b.name, cb.banner_id;



CREATE SEQUENCE appeared_banner_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;



CREATE TABLE campaign_draft (
    campaign_draft_id bigint NOT NULL,
    campaign_draft_time_added timestamp without time zone NOT NULL,
    campaign_draft_cid bigint NOT NULL,
    campaign_draft_pid bigint NOT NULL,
    campaign_draft_data text
);





CREATE SEQUENCE campaign_draft_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;



CREATE TABLE clicked_banner (
    id bigint NOT NULL,
    banner_id bigint NOT NULL,
    profile_id bigint NOT NULL,
    time_added timestamp without time zone DEFAULT now() NOT NULL,
    referer text
);



CREATE VIEW click_banners_last_12_months AS
 SELECT count(cb.banner_id) AS cnt,
    b.name,
    cb.banner_id
   FROM (clicked_banner cb
     LEFT JOIN commondb.banners b ON ((cb.banner_id = b.banner_id)))
  WHERE ((cb.time_added >= ((now())::date - '1 year'::interval)) AND (cb.time_added <= ((now())::date + '1 day'::interval)))
  GROUP BY b.name, cb.banner_id;



CREATE VIEW click_banners_last_6_months AS
 SELECT count(cb.banner_id) AS cnt,
    b.name,
    cb.banner_id
   FROM (clicked_banner cb
     LEFT JOIN commondb.banners b ON ((cb.banner_id = b.banner_id)))
  WHERE ((cb.time_added >= ((now())::date - '6 mons'::interval)) AND (cb.time_added <= ((now())::date + '1 day'::interval)))
  GROUP BY b.name, cb.banner_id;



CREATE VIEW click_banners_last_7_days AS
 SELECT count(cb.banner_id) AS cnt,
    b.name,
    cb.banner_id
   FROM (clicked_banner cb
     LEFT JOIN commondb.banners b ON ((cb.banner_id = b.banner_id)))
  WHERE ((cb.time_added >= ((now())::date - '6 days'::interval)) AND (cb.time_added <= ((now())::date + '1 day'::interval)))
  GROUP BY b.name, cb.banner_id;



CREATE VIEW click_banners_last_month AS
 SELECT count(cb.banner_id) AS cnt,
    b.name,
    cb.banner_id
   FROM (clicked_banner cb
     LEFT JOIN commondb.banners b ON ((cb.banner_id = b.banner_id)))
  WHERE ((cb.time_added >= ((now())::date - '1 mon'::interval)) AND (cb.time_added <= ((now())::date + '1 day'::interval)))
  GROUP BY b.name, cb.banner_id;



CREATE VIEW click_banners_today AS
 SELECT count(cb.banner_id) AS cnt,
    b.name,
    cb.banner_id
   FROM (clicked_banner cb
     LEFT JOIN commondb.banners b ON ((cb.banner_id = b.banner_id)))
  WHERE ((cb.time_added >= (now())::date) AND (cb.time_added <= ((now())::date + '1 day'::interval)))
  GROUP BY b.name, cb.banner_id;



CREATE SEQUENCE clicked_banner_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;





CREATE TABLE compromat (
    email text,
    time_added timestamp without time zone DEFAULT now()
);



CREATE TABLE external_promo_code (
    code_id bigint NOT NULL,
    time_added timestamp without time zone,
    promo_config_id integer NOT NULL,
    status integer DEFAULT 0 NOT NULL,
    secret_code text NOT NULL
);



CREATE SEQUENCE external_promo_code_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;



CREATE TABLE interactive_editor_data (
    data_id bigint NOT NULL,
    user_id bigint NOT NULL,
    campaign_id bigint NOT NULL,
    contractor_type_percent integer NOT NULL,
    desired_collected_amount_percent integer,
    reached_step_num integer NOT NULL,
    forms_settings text,
    agree_store_personal_data boolean DEFAULT false NOT NULL,
    agree_receive_lessons_materials_on_email boolean DEFAULT false NOT NULL,
    time_added timestamp without time zone NOT NULL
);

CREATE TABLE ivi_order (
    order_id bigint NOT NULL
);



CREATE SEQUENCE ivi_promo_code_promo_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;



CREATE TABLE litres_promo_code (
    promo_id integer NOT NULL,
    time_added timestamp without time zone,
    status integer DEFAULT 0 NOT NULL,
    email text,
    secret_code text NOT NULL,
    vote_type integer
);



CREATE SEQUENCE litres_promo_code_promo_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;



ALTER SEQUENCE litres_promo_code_promo_id_seq OWNED BY litres_promo_code.promo_id;


CREATE TABLE opened_emails (
    message_id bigint NOT NULL,
    time_added timestamp without time zone NOT NULL
);



CREATE TABLE order_from (
    order_id bigint NOT NULL,
    order_from integer NOT NULL
);




CREATE TABLE promo_config (
    promo_id bigint NOT NULL,
    name character varying(250) NOT NULL,
    status integer DEFAULT 0 NOT NULL,
    campaign_tag bigint[],
    mail_template character varying(250),
    time_start timestamp without time zone,
    time_finish timestamp without time zone,
    has_promocode boolean DEFAULT false,
    price_condition numeric(10,2),
    usage_count integer DEFAULT 0,
    creator_profile_id bigint,
    updater_profile_id bigint,
    time_added timestamp without time zone,
    time_updated timestamp without time zone
);



CREATE SEQUENCE promo_config_id_seq
    START WITH 5
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;



CREATE SEQUENCE promo_email_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;



CREATE TABLE promo_emails (
    email_id bigint NOT NULL,
    promo_id bigint NOT NULL,
    promo_code_id bigint,
    email text NOT NULL,
    time_sent timestamp without time zone,
    order_id bigint
);



CREATE TABLE quiz (
    quiz_id bigint NOT NULL,
    alias text,
    name text NOT NULL,
    description text,
    enabled boolean DEFAULT false NOT NULL,
    time_added timestamp without time zone DEFAULT now() NOT NULL
);
ALTER TABLE ONLY quiz ALTER COLUMN quiz_id SET STATISTICS 0;
ALTER TABLE ONLY quiz ALTER COLUMN name SET STATISTICS 0;
ALTER TABLE ONLY quiz ALTER COLUMN description SET STATISTICS 0;
ALTER TABLE ONLY quiz ALTER COLUMN enabled SET STATISTICS 0;
ALTER TABLE ONLY quiz ALTER COLUMN time_added SET STATISTICS 0;



CREATE TABLE quiz_answer (
    quiz_answer_id bigint NOT NULL,
    quiz_id bigint NOT NULL,
    quiz_question_id bigint NOT NULL,
    quiz_question_option_id bigint DEFAULT 0 NOT NULL,
    answer_custom_text text,
    answer_integer integer,
    answer_boolean boolean,
    user_id bigint NOT NULL,
    time_added timestamp(6) without time zone NOT NULL
);
ALTER TABLE ONLY quiz_answer ALTER COLUMN quiz_answer_id SET STATISTICS 0;
ALTER TABLE ONLY quiz_answer ALTER COLUMN quiz_id SET STATISTICS 0;
ALTER TABLE ONLY quiz_answer ALTER COLUMN quiz_question_id SET STATISTICS 0;
ALTER TABLE ONLY quiz_answer ALTER COLUMN quiz_question_option_id SET STATISTICS 0;
ALTER TABLE ONLY quiz_answer ALTER COLUMN answer_custom_text SET STATISTICS 0;
ALTER TABLE ONLY quiz_answer ALTER COLUMN user_id SET STATISTICS 0;
ALTER TABLE ONLY quiz_answer ALTER COLUMN time_added SET STATISTICS 0;



CREATE TABLE quiz_invite_mail (
    quiz_invite_mail_id bigint NOT NULL,
    quiz_id bigint NOT NULL,
    user_id bigint NOT NULL,
    time_added timestamp without time zone NOT NULL
);
ALTER TABLE ONLY quiz_invite_mail ALTER COLUMN quiz_invite_mail_id SET STATISTICS 0;
ALTER TABLE ONLY quiz_invite_mail ALTER COLUMN quiz_id SET STATISTICS 0;
ALTER TABLE ONLY quiz_invite_mail ALTER COLUMN user_id SET STATISTICS 0;
ALTER TABLE ONLY quiz_invite_mail ALTER COLUMN time_added SET STATISTICS 0;



CREATE SEQUENCE quiz_invite_mail_quiz_invite_mail_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;



CREATE TABLE quiz_question (
    quiz_question_id bigint NOT NULL,
    question_text text NOT NULL,
    type integer NOT NULL,
    start_value integer DEFAULT 0 NOT NULL,
    end_value integer DEFAULT 0 NOT NULL,
    has_custom_radio boolean DEFAULT false NOT NULL,
    custom_radio_text text,
    question_description text,
    enabled boolean DEFAULT true NOT NULL
);
ALTER TABLE ONLY quiz_question ALTER COLUMN quiz_question_id SET STATISTICS 0;
ALTER TABLE ONLY quiz_question ALTER COLUMN question_text SET STATISTICS 0;
ALTER TABLE ONLY quiz_question ALTER COLUMN type SET STATISTICS 0;



CREATE TABLE quiz_question_option (
    quiz_question_option_id bigint NOT NULL,
    quiz_question_id bigint NOT NULL,
    option_text text NOT NULL,
    option_order_num integer
);
ALTER TABLE ONLY quiz_question_option ALTER COLUMN quiz_question_option_id SET STATISTICS 0;
ALTER TABLE ONLY quiz_question_option ALTER COLUMN quiz_question_id SET STATISTICS 0;
ALTER TABLE ONLY quiz_question_option ALTER COLUMN option_text SET STATISTICS 0;



CREATE TABLE quiz_question_relation (
    quiz_question_relation_id bigint NOT NULL,
    quiz_id bigint NOT NULL,
    quiz_question_id bigint NOT NULL,
    quiz_question_order_num integer
);
ALTER TABLE ONLY quiz_question_relation ALTER COLUMN quiz_question_relation_id SET STATISTICS 0;
ALTER TABLE ONLY quiz_question_relation ALTER COLUMN quiz_id SET STATISTICS 0;
ALTER TABLE ONLY quiz_question_relation ALTER COLUMN quiz_question_id SET STATISTICS 0;
ALTER TABLE ONLY quiz_question_relation ALTER COLUMN quiz_question_order_num SET STATISTICS 0;



CREATE TABLE referer (
    external_id bigint,
    ip text,
    referer text,
    cid text,
    type integer,
    time_added timestamp without time zone,
    project_type integer DEFAULT 1,
    short_link_id bigint
);



CREATE TABLE school_online_applications (
    application_id bigint NOT NULL,
    name text NOT NULL,
    email text NOT NULL,
    time_added timestamp without time zone NOT NULL,
    invite_sent boolean DEFAULT false NOT NULL
);
ALTER TABLE ONLY school_online_applications ALTER COLUMN application_id SET STATISTICS 0;
ALTER TABLE ONLY school_online_applications ALTER COLUMN name SET STATISTICS 0;
ALTER TABLE ONLY school_online_applications ALTER COLUMN email SET STATISTICS 0;
ALTER TABLE ONLY school_online_applications ALTER COLUMN time_added SET STATISTICS 0;
ALTER TABLE ONLY school_online_applications ALTER COLUMN invite_sent SET STATISTICS 0;



CREATE SEQUENCE seq_ab_shares_sort_shows_show_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;



CREATE SEQUENCE seq_ab_shares_sort_test_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;



CREATE SEQUENCE seq_interactive_editor_data_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;



CREATE SEQUENCE seq_quiz_answer_id
    START WITH 10
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;



CREATE SEQUENCE seq_quiz_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;



CREATE SEQUENCE seq_quiz_question_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;



CREATE SEQUENCE seq_quiz_question_option_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;



CREATE SEQUENCE seq_quiz_question_relation_id
    START WITH 10
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;



CREATE SEQUENCE seq_school_online_application_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;



CREATE SEQUENCE seq_user_feedback_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;



CREATE SEQUENCE shown_banner_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;





CREATE TABLE temp_balanse (
    profile_id bigint,
    new_balanse numeric
);



CREATE TABLE user_feedback (
    user_feedback_id bigint NOT NULL,
    order_id bigint,
    user_id bigint NOT NULL,
    campaign_id bigint,
    score integer NOT NULL,
    extra_testing boolean DEFAULT false NOT NULL,
    page_type bigint NOT NULL,
    time_added timestamp without time zone DEFAULT now() NOT NULL
);

CREATE TABLE user_google_relation (
    user_id bigint NOT NULL,
    google_cid text NOT NULL
);




CREATE SEQUENCE yandex_music_promo_code_promo_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;



ALTER TABLE ONLY litres_promo_code ALTER COLUMN promo_id SET DEFAULT nextval('litres_promo_code_promo_id_seq'::regclass);


ALTER TABLE ONLY ab_shares_sort
    ADD CONSTRAINT ab_shares_sort_pkey PRIMARY KEY (ab_shares_sort_test_id);


ALTER TABLE ONLY ab_shares_sort_shows
    ADD CONSTRAINT ab_shares_sort_shows_pkey PRIMARY KEY (show_id);


ALTER TABLE ONLY ab_shares_sort_shows
    ADD CONSTRAINT ab_shares_sort_shows_sort_order_key UNIQUE (sort_order);


ALTER TABLE ONLY adblock
    ADD CONSTRAINT adblock_pkey PRIMARY KEY (profile_id, cid, ip);


ALTER TABLE ONLY clicked_banner
    ADD CONSTRAINT banner_clicked_pk PRIMARY KEY (id);


ALTER TABLE ONLY campaign_draft
    ADD CONSTRAINT campaign_draft_pkey PRIMARY KEY (campaign_draft_id);


ALTER TABLE ONLY external_promo_code
    ADD CONSTRAINT external_promo_code_pkey PRIMARY KEY (code_id);


ALTER TABLE ONLY interactive_editor_data
    ADD CONSTRAINT interactive_editor_data_pkey PRIMARY KEY (data_id);


ALTER TABLE ONLY interactive_editor_data
    ADD CONSTRAINT interactive_editor_data_user_id_key UNIQUE (user_id);


ALTER TABLE ONLY ivi_order
    ADD CONSTRAINT ivi_order_pkey PRIMARY KEY (order_id);


ALTER TABLE ONLY litres_promo_code
    ADD CONSTRAINT litres_promo_code_pkey PRIMARY KEY (promo_id);


ALTER TABLE ONLY opened_emails
    ADD CONSTRAINT opened_emails_pkey PRIMARY KEY (message_id);


ALTER TABLE ONLY order_from
    ADD CONSTRAINT order_from_pkey PRIMARY KEY (order_id);


ALTER TABLE ONLY promo_config
    ADD CONSTRAINT promo_config_pkey PRIMARY KEY (promo_id);


ALTER TABLE ONLY promo_emails
    ADD CONSTRAINT promo_emails_pkey PRIMARY KEY (email_id);


ALTER TABLE ONLY quiz
    ADD CONSTRAINT quiz_alias_key UNIQUE (alias);


ALTER TABLE ONLY quiz_answer
    ADD CONSTRAINT quiz_answer_pkey PRIMARY KEY (quiz_answer_id);


ALTER TABLE ONLY quiz_invite_mail
    ADD CONSTRAINT quiz_invite_mail_pkey PRIMARY KEY (quiz_invite_mail_id);


ALTER TABLE ONLY quiz
    ADD CONSTRAINT quiz_pkey PRIMARY KEY (quiz_id);


ALTER TABLE ONLY quiz_question_option
    ADD CONSTRAINT quiz_question_option_pkey PRIMARY KEY (quiz_question_option_id);


ALTER TABLE ONLY quiz_question
    ADD CONSTRAINT quiz_question_pkey PRIMARY KEY (quiz_question_id);


ALTER TABLE ONLY quiz_question_relation
    ADD CONSTRAINT quiz_question_relation_pkey PRIMARY KEY (quiz_question_relation_id);


ALTER TABLE ONLY school_online_applications
    ADD CONSTRAINT school_online_applications_email_key UNIQUE (email);


ALTER TABLE ONLY school_online_applications
    ADD CONSTRAINT school_online_applications_pkey PRIMARY KEY (application_id);


ALTER TABLE ONLY user_feedback
    ADD CONSTRAINT user_feedback_pkey PRIMARY KEY (user_feedback_id);


ALTER TABLE ONLY user_google_relation
    ADD CONSTRAINT user_google_relation_pkey PRIMARY KEY (user_id, google_cid);


CREATE INDEX campaign_draft_cid_index ON campaign_draft USING btree (campaign_draft_cid);


CREATE INDEX campaign_draft_pid_index ON campaign_draft USING btree (campaign_draft_pid);


CREATE UNIQUE INDEX external_promo_code_secret_code_status_idx ON external_promo_code USING btree (secret_code, promo_config_id);


CREATE UNIQUE INDEX litres_promo_code_email_and_vote_type_idx ON litres_promo_code USING btree (email, vote_type);


CREATE UNIQUE INDEX litres_promo_code_index ON litres_promo_code USING btree (secret_code);


CREATE INDEX promo_config_conditions_idx ON promo_config USING btree (campaign_tag, price_condition, time_start, time_finish);


CREATE INDEX promo_emails_email_idx ON promo_emails USING btree (email);


CREATE UNIQUE INDEX quiz_question_relation_idx ON quiz_question_relation USING btree (quiz_id, quiz_question_id);


CREATE INDEX user_feedback_campaign_id_idx ON user_feedback USING btree (campaign_id);


CREATE INDEX user_feedback_order_id_idx ON user_feedback USING btree (order_id);


CREATE INDEX user_feedback_user_id_idx ON user_feedback USING btree (user_id);


--liquibase formatted sql
--changeset stikkas:1
SET search_path = shopdb, pg_catalog;
CREATE TABLE products (
    product_id bigint NOT NULL,
    current_version boolean DEFAULT false NOT NULL,
    merchant_profile_id bigint NOT NULL,
    store_id bigint NOT NULL,
    price numeric(16,2),
    currency text,
    total_purchase_count integer NOT NULL,
    purchase_limit integer NOT NULL,
    product_state integer NOT NULL,
    type integer,
    product_status integer NOT NULL,
    product_category integer NOT NULL,
    image_urls text[],
    cover_image_url text,
    name character varying(256),
    description text,
    description_html text,
    product_attribute_id integer,
    time_added timestamp without time zone DEFAULT now() NOT NULL,
    time_updated timestamp without time zone DEFAULT now() NOT NULL,
    parent_product_id bigint,
    product_attribute_type integer,
    product_attribute_value character varying(25),
    total_on_hold_quantity bigint DEFAULT 0 NOT NULL,
    total_quantity bigint DEFAULT 0 NOT NULL,
    time_last_purchased timestamp without time zone,
    table_image_url text,
    product_attribute_index integer DEFAULT 0,
    name_html text,
    referrer_id bigint DEFAULT 21765 NOT NULL,
    start_sale_date timestamp without time zone DEFAULT ('now'::text)::date NOT NULL,
    old_price numeric(16,2),
    free_delivery boolean DEFAULT false NOT NULL,
    text_for_mail text,
    cash_available boolean DEFAULT true,
    donate_id bigint DEFAULT 0,
    content_urls text[],
    show_on_campaign boolean DEFAULT true NOT NULL,
    campaign_ids bigint[],
    time_started timestamp without time zone
);

CREATE TABLE attribute (
    attribute_id bigint NOT NULL,
    value character varying(256),
    type integer NOT NULL
);

CREATE TABLE attribute_types (
    attribute_type_id bigint NOT NULL,
    value character varying(256)
);



CREATE TABLE attributes (
    attribute_id bigint NOT NULL,
    value character varying(256),
    type integer NOT NULL,
    index integer DEFAULT 0
);





CREATE TABLE categories_backup (
    category_id bigint,
    value text,
    preferred_order integer,
    mnemonic_name text,
    is_primary_cat boolean
);

CREATE TABLE category_attribute_type (
    category_id bigint NOT NULL,
    attribute_type_id bigint NOT NULL
);



CREATE TABLE product_counts (
    product_id bigint NOT NULL,
    store_id bigint NOT NULL,
    product_count integer NOT NULL
);

CREATE TABLE product_tag_relations (
    product_id bigint NOT NULL,
    tag_id bigint NOT NULL,
    order_num integer DEFAULT 0 NOT NULL
);

CREATE TABLE product_tags (
    category_id bigint NOT NULL,
    value text,
    preferred_order integer DEFAULT 0 NOT NULL,
    mnemonic_name text DEFAULT 'TEMP'::text NOT NULL,
    tag_type integer DEFAULT 1
);



CREATE TABLE products_bkp (
    product_id bigint,
    version integer,
    current_version boolean,
    merchant_profile_id bigint,
    store_id bigint,
    price numeric(16,2),
    currency text,
    total_purchase_count integer,
    purchase_limit integer,
    product_state integer,
    type integer,
    product_status integer,
    product_category integer,
    image_urls text[],
    cover_image_url text,
    name character varying(256),
    description text,
    description_html text,
    product_attribute_id integer,
    time_added timestamp without time zone,
    time_updated timestamp without time zone,
    parent_product_id bigint,
    product_attribute_type integer,
    product_attribute_value character varying(25),
    total_on_hold_quantity bigint,
    total_quantity bigint,
    time_last_purchased timestamp without time zone,
    table_image_url text,
    product_attribute_index integer
);

CREATE TABLE promo_code (
    promo_code_id bigint NOT NULL,
    title character varying NOT NULL,
    code character varying NOT NULL,
    discount_amount integer DEFAULT 0 NOT NULL,
    free_delivery boolean DEFAULT false NOT NULL,
    time_begin timestamp without time zone,
    time_end timestamp without time zone,
    min_order_price integer DEFAULT 0 NOT NULL,
    code_usage_type integer DEFAULT 1 NOT NULL,
    usage_count integer DEFAULT 0 NOT NULL,
    profile_id bigint,
    product_tag_id integer,
    time_added timestamp without time zone NOT NULL,
    disabled boolean DEFAULT false NOT NULL,
    discount_type integer DEFAULT 1 NOT NULL,
    min_product_count integer DEFAULT 0 NOT NULL
);

CREATE SEQUENCE seq_attribute_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE SEQUENCE seq_attribute_type_id
    START WITH 3
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE SEQUENCE seq_category_id
    START WITH 3
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE SEQUENCE seq_delivery_department_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE SEQUENCE seq_delivery_zone_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE SEQUENCE seq_product_history_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE SEQUENCE seq_product_history_record_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE SEQUENCE seq_product_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE SEQUENCE seq_promo_code_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE SEQUENCE seq_warehouse_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE TABLE shopping_cart (
    profile_id bigint NOT NULL,
    product_id bigint NOT NULL,
    quantity integer DEFAULT 1 NOT NULL,
    time_updated timestamp(0) without time zone DEFAULT now()
);

ALTER TABLE ONLY attribute
    ADD CONSTRAINT attribute_keyword_pkey PRIMARY KEY (attribute_id);

ALTER TABLE ONLY attribute_types
    ADD CONSTRAINT attribute_types_pkey PRIMARY KEY (attribute_type_id);

ALTER TABLE ONLY attributes
    ADD CONSTRAINT attributes_pkey PRIMARY KEY (attribute_id);

ALTER TABLE ONLY product_tags
    ADD CONSTRAINT categories_pkey PRIMARY KEY (category_id);

ALTER TABLE ONLY category_attribute_type
    ADD CONSTRAINT category_attribute_type_pkey PRIMARY KEY (category_id, attribute_type_id);

ALTER TABLE ONLY product_counts
    ADD CONSTRAINT product_count_keyword_pkey PRIMARY KEY (product_id, store_id);

ALTER TABLE ONLY product_tag_relations
    ADD CONSTRAINT product_ord_num_unique UNIQUE (product_id, order_num);

ALTER TABLE ONLY product_tag_relations
    ADD CONSTRAINT product_tag_relations_pkey PRIMARY KEY (product_id, tag_id);

ALTER TABLE ONLY products
    ADD CONSTRAINT products_pk PRIMARY KEY (product_id, product_status);

ALTER TABLE ONLY promo_code
    ADD CONSTRAINT promo_code_pkey PRIMARY KEY (promo_code_id);

ALTER TABLE ONLY shopping_cart
    ADD CONSTRAINT shopping_cart_pkey PRIMARY KEY (profile_id, product_id);

CREATE UNIQUE INDEX promo_codes_code_idx ON promo_code USING btree (code);


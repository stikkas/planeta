--liquibase formatted sql

--changeset stikkas:1 endDelimiter:\nGO;
SET search_path = profiledb, pg_catalog
GO;

CREATE TYPE profile_news_type AS ENUM (
    'CAMPAIGN_CREATED',
    'CAMPAIGN_ON_MODERATION',
    'CAMPAIGN_PAUSED',
    'CAMPAIGN_ON_REWORK',
    'CAMPAIGN_DECLINED',
    'CAMPAIGN_DELETED',
    'CAMPAIGN_STARTED',
    'CAMPAIGN_FINISHED_SUCCESS',
    'CAMPAIGN_FINISHED_FAIL',
    'CAMPAIGN_RESUMED',
    'CAMPAIGN_REACHED_15',
    'CAMPAIGN_REACHED_20',
    'CAMPAIGN_REACHED_25',
    'CAMPAIGN_REACHED_50',
    'CAMPAIGN_REACHED_75',
    'CAMPAIGN_REACHED_100',
    'SHARE_CHANGED',
    'SHARE_DELETED',
    'SHARE_ADDED',
    'SHARE_PURCHASED',
    'POST',
    'ADMIN_CHANGE_USER_STATUS',
    'ADMIN_CHANGE_USER_PASSWORD',
    'ADMIN_CHANGE_USER_EMAIL',
    'ADMIN_DECREASE_USER_BALANCE',
    'ADMIN_FREEZE_USER_AMOUNT',
    'ADMIN_UNFREEZE_USER_AMOUNT',
    'ADMIN_DECREASE_USER_FROZEN_AMOUNT',
    'ADMIN_GIFT_MONEY_FOR_USER',
    'ADMIN_LOGIN_AS_USER',
    'ADMIN_CANCEL_PAYMENT',
    'ADMIN_PROCESS_PAYMENT',
    'ADMIN_SAVE_BROADCAST_INFO',
    'ADMIN_REMOVE_BROADCAST',
    'ADMIN_START_BROADCAST',
    'ADMIN_STOP_BROADCAST',
    'ADMIN_PAUSE_BROADCAST',
    'ADMIN_CREATE_NEW_BONUS',
    'ADMIN_EDIT_BONUS',
    'ADMIN_DELETE_BONUS',
    'ADMIN_SIMPLE_CHANGE_CAMPAIGN_STATUS',
    'ADMIN_CHANGE_CAMPAIGN_DESCRIPTION_AND_META_DATA',
    'ADMIN_CHANGE_CAMPAIGN_DATA',
    'ADMIN_SAVE_PRODUCT',
    'ADMIN_DELETE_PRODUCT',
    'ADMIN_CHANGE_ORDER_DELIVERY_STATUS',
    'ADD_CAMPAIGN_CONTRACTOR',
    'CHANGE_CAMPAIGN_CONTRACTOR',
    'SAVE_CAMPAIGN',
    'SAVE_DRAFT_CAMPAIGN',
    'CHANGE_CAMPAIGN_POST',
    'DELETE_CAMPAIGN_POST',
    'SEND_CAMPAIGN_NEWS_NOTIFICATION',
    'DELETE_COMMENT',
    'UPDATE_ORDER_STATUS_COMPLETE',
    'UPDATE_ORDER_STATUS_CANCEL',
    'UPDATE_ORDER_STATUS_GIVEN',
    'PROFILE_SET_ADMIN_SUBSCRIBER',
    'SCHOOL_DOWNLOAD_PDF',
    'SUCCESS_PAGE_SHOW',
    'FAIL_PAGE_SHOW',
    'PROFILE_SETTINGS_CHANGED',
    'PROFILE_PASSWORD_CHANGED',
    'ADMIN_SET_ADMIN_SUBSCRIBER',
    'MAIL_SPAMMER_BLOCKED',
    'DIALOG_SPAMMER_BLOCKED',
    'SPAMMER_UNBLOCKED',
    'CAMPAIGN_INTERACTIVE_STEP_START',
    'CAMPAIGN_INTERACTIVE_STEP_NAME',
    'CAMPAIGN_INTERACTIVE_STEP_EMAIL',
    'CAMPAIGN_INTERACTIVE_STEP_PROCEED',
    'CAMPAIGN_INTERACTIVE_STEP_INFO',
    'CAMPAIGN_INTERACTIVE_STEP_DURATION',
    'CAMPAIGN_INTERACTIVE_STEP_PRICE',
    'CAMPAIGN_INTERACTIVE_STEP_VIDEO',
    'CAMPAIGN_INTERACTIVE_STEP_DESCRIPTION',
    'CAMPAIGN_INTERACTIVE_STEP_REWARD',
    'CAMPAIGN_INTERACTIVE_STEP_COUNTERPARTY',
    'CAMPAIGN_INTERACTIVE_STEP_CHECK',
    'CAMPAIGN_INTERACTIVE_STEP_FINAL',
    'CAMPAIGN_INTERACTIVE_STEP_FINAL_DRAFT',
    'SAVE_CAMPAIGN_INTERACTIVE_STEP_INFO',
    'SAVE_CAMPAIGN_INTERACTIVE_STEP_DURATION',
    'SAVE_CAMPAIGN_INTERACTIVE_STEP_PRICE',
    'SAVE_CAMPAIGN_INTERACTIVE_STEP_VIDEO',
    'SAVE_CAMPAIGN_INTERACTIVE_STEP_DESCRIPTION',
    'SAVE_CAMPAIGN_INTERACTIVE_STEP_REWARD',
    'SAVE_CAMPAIGN_INTERACTIVE_STEP_COUNTERPARTY',
    'CREATE_DRAFT_CAMPAIGN_INTERACTIVE_STEP_INFO',
    'ADMIN_CHANGE_QUIZ_INFO',
    'PROFILE_FILE_DELETED',
    'CAMPAIGN_REACHED_35',
    'CAMPAIGN_APPROVED',
    'CHANGED_PRODUCT_TAGS',
    'MANAGER_FOR_CAMPAIGN_UPDATED',
    'PROFILE_SUBSCRIPTIONS_CHANGED',
    'UPDATE_ORDER_STATUS_IN_TRANSIT'
)
GO;

CREATE FUNCTION check_permission(p_object_id bigint, p_object_type integer, p_profile_id bigint, p_client_id bigint, p_client_permission_level integer, p_object_permission_level integer, p_permission_type integer) RETURNS boolean
    LANGUAGE plpgsql
    AS $$
BEGIN
    -- If client is ADMIN to object owner profile -- ignoring custom_permission setting
    -- If object_permission is not custom -- just comparing object and client permission levels
    IF (p_object_permission_level != 38)
    OR (p_client_permission_level >= 40) THEN
    	RETURN (p_object_permission_level <= p_client_permission_level);
    	
    -- Checking if custom permission is set for the specified client
    ELSE 
    	RETURN (EXISTS(
       		SELECT 1
         	  FROM profiledb.custom_permission t1
      		 WHERE t1.object_id = p_object_id
          	   AND t1.object_type = p_object_type
               AND t1.owner_profile_id = p_profile_id
               AND t1.client_profile_id = p_client_id 
         	   AND t1.permission_type = p_permission_type
    	));
    END IF;    
END;
$$
GO;

CREATE FUNCTION check_relation_status(p_status integer, p_relation_status text) RETURNS boolean
    LANGUAGE sql IMMUTABLE
    AS $_$
        SELECT $1 & t1.relation_status = t1.relation_status
      FROM (
        SELECT CASE WHEN lower($2) = 'not_set' THEN 0
                        WHEN lower($2) = 'self' THEN 1
                WHEN lower($2) = 'no_relation' THEN 2
                WHEN lower($2) = 'request_in' THEN 4
                WHEN lower($2) = 'request_out' THEN 8
                WHEN lower($2) = 'active' THEN 16
                WHEN lower($2) = 'blocked_by_me' THEN 64
                WHEN lower($2) = 'blocked_by_other' THEN 128
                WHEN lower($2) = 'admin' THEN 1024
                WHEN lower($2) = 'artist' THEN 2048
                END AS relation_status
    ) t1
$_$
GO;

CREATE FUNCTION delete_profile(p_id bigint) RETURNS void
    LANGUAGE plpgsql
    AS $$
DECLARE 
	user_type INT;
BEGIN

	IF EXISTS (SELECT 1 FROM commondb.credit_transactions
					   WHERE profile_id = p_id) THEN
		RAISE EXCEPTION 'Cannot delete - there are money transactions';
	END IF;
	
	IF EXISTS (SELECT 1 FROM commondb.debit_transactions
					   WHERE profile_id = p_id) THEN
		RAISE EXCEPTION 'Cannot delete - there are money transactions';
	END IF;
	
	
	SELECT INTO user_type profile_type_id 
		   FROM profiledb.profiles 
		  WHERE profile_id = p_id;

	IF user_type = 1 THEN
		UPDATE profiledb.profiles
           SET 
               image_url = null,
               image_id = 0,
               small_image_url = null,
               small_image_id = 0,
               name = null,
               alias = null,
               status = 0,
               city_id = 0,
               country_id = 0,
               rating = 0,
               user_birth_date = null,
               user_gender = 0,
               user_first_name = null,
               user_last_name = null,
               display_name = 'Пользователь удален',
               display_name_format = 0,
               summary = null
         WHERE profile_id = p_id;
         
        UPDATE profiledb.users
           SET description = '',
           	   description_html = ''
         WHERE profile_id = p_id;
		 
		UPDATE commondb.users_private_info
   	       SET status = 4 -- status deleted
		 WHERE user_id = p_id;
		
        UPDATE profiledb.wall_posts
           SET text_html = 'Запись удалена'
         WHERE owner_profile_id = p_id
		    OR author_profile_id = p_id;
          
        UPDATE profiledb.comments
           SET text = 'Комментарий удален',
               text_html = 'Комментарий удален'
         WHERE owner_profile_id = p_id
		    OR author_profile_id = p_id;
         
        UPDATE profiledb.blog_posts
           SET post_text = '',
               post_text_html = '',
               heading_text = '',
               heading_text_html = '',
               title = 'Запись удалена'
         WHERE owner_profile_id = p_id
		    OR author_profile_id = p_id;
         
		DELETE FROM profiledb.profile_status
         WHERE profile_id = p_id;
        
        DELETE FROM profiledb.profile_status_history
         WHERE profile_id = p_id;
         
        DELETE FROM profiledb.profile_block_settings
         WHERE profile_id = p_id;
        
        RETURN;
	ELSE
		DELETE FROM profiledb.profiles
		WHERE profile_id = p_id;
	END IF;	

	DELETE FROM profiledb.audio_albums
	      WHERE owner_profile_id = p_id;

	DELETE FROM profiledb.audio_tracks
		  WHERE owner_profile_id = p_id;

	DELETE FROM profiledb.blog_categories
		  WHERE profile_id = p_id;

	DELETE FROM profiledb.blog_comment_reads
		  WHERE owner_profile_id = p_id;

	DELETE FROM profiledb.blog_posts
		  WHERE owner_profile_id = p_id;

	DELETE FROM profiledb.comment_objects
		  WHERE profile_id = p_id;
		  
	DELETE FROM profiledb.comments
		  WHERE owner_profile_id = p_id;
          
    DELETE FROM profiledb.vote_objects
          WHERE profile_id = p_id;
          
	DELETE FROM profiledb.votes
          WHERE profile_id = p_id;

	DELETE FROM profiledb.daily_features
		  WHERE profile_id = p_id;

	DELETE FROM profiledb.events
		  WHERE profile_id = p_id;

	DELETE FROM profiledb.feed
		  WHERE profile_id = p_id;

	DELETE FROM profiledb.groups
		  WHERE profile_id = p_id;

	DELETE FROM profiledb.notifications
		  WHERE profile_id = p_id;

	DELETE FROM profiledb.photo_albums
		  WHERE owner_profile_id = p_id;

	DELETE FROM profiledb.photos
		  WHERE owner_profile_id = p_id;

	DELETE FROM profiledb.playlist_tracks
		  WHERE owner_profile_id = p_id;  

	DELETE FROM profiledb.playlists
		  WHERE owner_profile_id = p_id;

	DELETE FROM profiledb.profile_backgrounds
		  WHERE profile_id = p_id;

	DELETE FROM profiledb.profile_block_settings
		  WHERE profile_id = p_id;

	DELETE FROM profiledb.profile_communication_channels
		  WHERE profile_id = p_id;

	DELETE FROM profiledb.profile_dialogs
		  WHERE profile_id = p_id 
		     OR companion_profile_id = p_id;

	DELETE FROM profiledb.profile_news
		  WHERE profile_id = p_id;

	DELETE FROM profiledb.profile_relations
		  WHERE profile_id = p_id 
		     OR subject_profile_id = p_id;

	DELETE FROM profiledb.profile_settings
		  WHERE profile_id = p_id;  

	DELETE FROM profiledb.profile_status
		  WHERE profile_id = p_id;

	DELETE FROM profiledb.tag_objects
		  WHERE profile_id = p_id;

	DELETE FROM profiledb.tags
		  WHERE profile_id = p_id;

	DELETE FROM profiledb.videos
		  WHERE owner_profile_id = p_id;

	DELETE FROM profiledb.wall_posts
		  WHERE owner_profile_id = p_id; 

END;
$$
GO;

CREATE FUNCTION get_database_node(p_id bigint) RETURNS text
    LANGUAGE sql
    AS $_$
        SELECT CASE WHEN $1 % 2 = 0 
                        THEN 'host=127.0.0.1 dbname=userdb01'
                        ELSE 'host=127.0.0.1 dbname=userdb02'
                END AS connstr;
$_$
GO;

CREATE FUNCTION get_profile_admins(my_profile_id bigint) RETURNS TABLE(profile_id bigint)
    LANGUAGE plpgsql
    AS $$
BEGIN
  RETURN QUERY
    select my_profile_id
      union
    select subject_profile_id from profiledb.profile_subscription ps
    where ps.profile_id = my_profile_id and ps.is_admin;
END;
$$
GO;

CREATE FUNCTION get_profile_type_id(v_profile_type text) RETURNS integer
    LANGUAGE sql IMMUTABLE
    AS $_$
        SELECT CASE WHEN lower($1) = 'user' THEN 1
                    WHEN lower($1) = 'group' THEN 2
                WHEN lower($1) = 'event' THEN 3
            END AS profile_type_id;
$_$
GO;

CREATE FUNCTION update_friends_groups_count(p_id bigint) RETURNS integer
    LANGUAGE plpgsql
    AS $$
DECLARE
BEGIN
   UPDATE profiledb.users u
   SET friends_count = (SELECT count(*) 
                          FROM profiledb.profile_relations t1
                          JOIN profiledb.profiles t2
                            ON t1.subject_profile_id = t2.profile_id
                         WHERE u.profile_id = t1.profile_id
                           AND t1.status & 16 = 16
                           AND t2.profile_type_id = 1),
       groups_count = (SELECT count(*) 
                          FROM profiledb.profile_relations t1
                          JOIN profiledb.profiles t2
                            ON t1.subject_profile_id = t2.profile_id
                         WHERE u.profile_id = t1.profile_id
                           AND t1.status & 16 = 16
                           AND t2.profile_type_id = 2)
   WHERE u.profile_id = p_id;
   RETURN 0;	
END;
$$
GO;


CREATE TABLE city (
    city_id integer NOT NULL,
    region_id integer NOT NULL,
    country_id integer NOT NULL,
    city_name_ru character varying(255),
    city_name_en character varying(255) NOT NULL,
    lat numeric(18,15),
    lng numeric(18,15)
)
GO;

CREATE TABLE country (
    country_id integer NOT NULL,
    country_name_ru character varying(50) NOT NULL,
    country_name_en character varying(50) NOT NULL,
    global_region_id integer DEFAULT 0 NOT NULL
)
GO;


CREATE TABLE profiles (
    profile_id bigint NOT NULL,
    profile_type_id integer NOT NULL,
    image_url character varying(256),
    image_id bigint DEFAULT 0 NOT NULL,
    name character varying(256),
    alias character varying(64),
    status integer DEFAULT 0 NOT NULL,
    city_id integer DEFAULT 0 NOT NULL,
    country_id integer DEFAULT 0 NOT NULL,
    rating integer DEFAULT 0 NOT NULL,
    user_birth_date timestamp without time zone,
    user_gender integer DEFAULT 0 NOT NULL,
    user_first_name character varying(128),
    user_last_name character varying(128),
    group_category_id integer DEFAULT 0 NOT NULL,
    creator_profile_id bigint DEFAULT 0 NOT NULL,
    artists_count integer DEFAULT 0 NOT NULL,
    users_count integer DEFAULT 0 NOT NULL,
    display_name_format integer DEFAULT 0,
    display_name character varying(256),
    small_image_id bigint DEFAULT 0 NOT NULL,
    small_image_url character varying(256),
    summary text,
    vip boolean DEFAULT false NOT NULL,
    vip_obtain_date timestamp without time zone,
    is_limit_messages boolean DEFAULT true,
    is_receive_newsletters boolean DEFAULT true,
    is_show_backed_campaigns boolean DEFAULT true,
    projects_count integer DEFAULT 0,
    backed_count integer DEFAULT 0,
    subscribers_count integer DEFAULT 0,
    receive_my_campaign_newsletters boolean DEFAULT true NOT NULL,
    phone_number character varying(32) DEFAULT NULL::character varying,
    has_active_projects boolean DEFAULT false NOT NULL,
    author_projects_count integer DEFAULT 0,
    new_subscribers_count integer DEFAULT 0 NOT NULL,
    subscriptions_count integer DEFAULT 0 NOT NULL,
    time_updated timestamp without time zone,
    time_added timestamp without time zone DEFAULT now()
)
GO;


CREATE TABLE audio_albums (
    album_id bigint NOT NULL,
    owner_profile_id bigint NOT NULL,
    author_profile_id bigint NOT NULL,
    album_name character varying(256) NOT NULL,
    artist_name character varying(256) NOT NULL,
    year integer DEFAULT 0 NOT NULL,
    record_label character varying(128),
    status integer DEFAULT 0 NOT NULL,
    image_url character varying(256),
    image_id bigint DEFAULT 0 NOT NULL,
    description text,
    tracks_count integer DEFAULT 0 NOT NULL,
    downloads_count integer DEFAULT 0 NOT NULL,
    listenings_count integer DEFAULT 0 NOT NULL,
    time_added timestamp without time zone DEFAULT now() NOT NULL
)
GO;


CREATE TABLE audio_tracks (
    track_id bigint NOT NULL,
    owner_profile_id bigint NOT NULL,
    author_profile_id bigint NOT NULL,
    album_id bigint,
    track_name character varying(250) NOT NULL,
    artist_name character varying(250),
    authors text,
    lyrics text,
    track_duration integer NOT NULL,
    track_url character varying(500) NOT NULL,
    listenings_count integer DEFAULT 0 NOT NULL,
    downloads_count integer DEFAULT 0 NOT NULL,
    uploader_profile_id bigint DEFAULT 0 NOT NULL,
    uploader_track_id bigint DEFAULT 0 NOT NULL,
    uploader_album_id bigint,
    has_mp3 boolean DEFAULT false,
    album_track_number integer DEFAULT 0,
    time_added timestamp without time zone DEFAULT now() NOT NULL
)
GO;


CREATE TABLE blog_categories (
    profile_id bigint NOT NULL,
    category_id integer NOT NULL,
    parent_category_id integer DEFAULT 0,
    name character varying(256),
    time_added timestamp without time zone DEFAULT now() NOT NULL,
    time_updated timestamp without time zone DEFAULT now() NOT NULL
)
GO;


CREATE TABLE blog_posts (
    blog_post_id bigint NOT NULL,
    owner_profile_id bigint NOT NULL,
    author_profile_id bigint,
    title character varying(256) NOT NULL,
    heading_text text,
    heading_text_html text,
    post_text text,
    post_text_html text,
    tags character varying(256),
    status integer NOT NULL,
    view_permission integer NOT NULL,
    time_added timestamp without time zone DEFAULT now() NOT NULL,
    time_updated timestamp without time zone DEFAULT now() NOT NULL,
    is_public_blog boolean DEFAULT false,
    on_behalf_of_group boolean DEFAULT false NOT NULL,
    category_id integer,
    campaign_id bigint DEFAULT 0,
    time_last_notification timestamp without time zone
)
GO;


CREATE TABLE broadcast_backers_targeting (
    broadcast_id bigint NOT NULL,
    profile_id bigint NOT NULL,
    campaign_id bigint NOT NULL,
    limit_summ numeric(10,2) DEFAULT 0 NOT NULL
)
GO;


CREATE TABLE broadcast_geo_targeting (
    broadcast_id bigint NOT NULL,
    profile_id bigint NOT NULL,
    city_id bigint,
    country_id bigint,
    allowed boolean
)
GO;


CREATE TABLE broadcast_private_targeting (
    broadcast_id bigint NOT NULL,
    profile_id bigint NOT NULL,
    email character varying(256) NOT NULL,
    generated_link character varying(256) NOT NULL,
    first_visit_time timestamp without time zone,
    valid_in_hours integer,
    time_added timestamp without time zone DEFAULT now() NOT NULL
)
GO;


CREATE TABLE broadcast_product_targeting (
    broadcast_id bigint NOT NULL,
    product_id bigint NOT NULL
)
GO;


CREATE TABLE broadcast_streams (
    stream_id bigint NOT NULL,
    broadcast_id bigint NOT NULL,
    owner_profile_id bigint NOT NULL,
    author_profile_id bigint NOT NULL,
    broadcast_url character varying(256) NOT NULL,
    broadcast_type integer DEFAULT 0 NOT NULL,
    broadcast_status integer DEFAULT 0 NOT NULL,
    broadcast_record_status integer DEFAULT 0 NOT NULL,
    name character varying(256) DEFAULT NULL::character varying,
    description text DEFAULT NULL::character varying,
    image_url character varying(256) DEFAULT NULL::character varying,
    image_id bigint,
    video_url character varying(256) DEFAULT NULL::character varying,
    video_id bigint,
    time_added timestamp without time zone DEFAULT now() NOT NULL,
    time_updated timestamp without time zone DEFAULT now() NOT NULL,
    views_count integer DEFAULT 0 NOT NULL,
    view_permission integer DEFAULT 0 NOT NULL,
    time_begin timestamp(0) without time zone DEFAULT now(),
    time_end timestamp(0) without time zone DEFAULT now(),
    embed_video_html text,
    tooltip character varying(100)
)
GO;


CREATE TABLE broadcast_stubs (
    broadcast_id bigint NOT NULL,
    owner_profile_id bigint NOT NULL,
    image_url character varying(256) DEFAULT NULL::character varying,
    image_id bigint,
    image_custom_description boolean DEFAULT false NOT NULL,
    image_description text,
    image_description_html text,
    paused_image_url character varying(256) DEFAULT NULL::character varying,
    paused_image_id bigint,
    paused_custom_description boolean DEFAULT false NOT NULL,
    paused_description text,
    paused_description_html text,
    finished_image_url character varying(256) DEFAULT NULL::character varying,
    finished_image_id bigint,
    finished_custom_description boolean DEFAULT false NOT NULL,
    finished_description text,
    finished_description_html text,
    closed_image_url character varying(256) DEFAULT NULL::character varying,
    closed_image_id bigint,
    closed_custom_description boolean DEFAULT false NOT NULL,
    closed_description text,
    closed_description_html text
)
GO;


CREATE TABLE broadcast_subscriptions (
    broadcast_id bigint NOT NULL,
    owner_profile_id bigint NOT NULL,
    subscriber_profile_id bigint NOT NULL,
    on_email boolean DEFAULT false NOT NULL,
    on_site boolean DEFAULT false NOT NULL
)
GO;


CREATE TABLE broadcasts (
    broadcast_id bigint NOT NULL,
    owner_profile_id bigint NOT NULL,
    author_profile_id bigint NOT NULL,
    tags character varying(256) DEFAULT NULL::character varying,
    name character varying(256) DEFAULT NULL::character varying,
    description text,
    time_added timestamp without time zone DEFAULT now() NOT NULL,
    time_updated timestamp without time zone DEFAULT now() NOT NULL,
    views_count integer DEFAULT 0 NOT NULL,
    view_permission integer DEFAULT 0 NOT NULL,
    broadcast_status integer DEFAULT 0,
    default_stream_id bigint DEFAULT 0,
    description_html text,
    time_begin timestamp without time zone DEFAULT now() NOT NULL,
    time_end timestamp without time zone DEFAULT now() NOT NULL,
    auto_start boolean DEFAULT false,
    auto_subscribe boolean DEFAULT false,
    archived boolean DEFAULT false,
    adv_banner_html text,
    visible_on_dashboard boolean DEFAULT false,
    closed boolean DEFAULT false NOT NULL,
    broadcast_subscription_status integer DEFAULT 0 NOT NULL,
    time_archived timestamp without time zone,
    order_num integer DEFAULT 0 NOT NULL,
    broadcast_category integer DEFAULT 0 NOT NULL,
    advertising text,
    chat_enabled boolean DEFAULT true NOT NULL,
    anonymous_can_view boolean DEFAULT false,
    chat_fake_users_count integer DEFAULT 0 NOT NULL
)
GO;


CREATE TABLE chat_messages (
    message_id bigint NOT NULL,
    chat_id bigint NOT NULL,
    owner_profile_id bigint NOT NULL,
    author_profile_id bigint NOT NULL,
    message_text text NOT NULL,
    message_text_html text NOT NULL,
    is_deleted boolean DEFAULT false NOT NULL,
    time_added timestamp without time zone DEFAULT now() NOT NULL
)
GO;


CREATE TABLE chats (
    chat_id bigint NOT NULL,
    owner_profile_id bigint NOT NULL,
    owner_object_id bigint NOT NULL,
    owner_object_type_code bigint NOT NULL,
    time_added timestamp without time zone DEFAULT now() NOT NULL
)
GO;


CREATE TABLE comment_objects (
    profile_id bigint NOT NULL,
    object_type_id integer NOT NULL,
    object_id bigint NOT NULL,
    comments_count integer DEFAULT 0 NOT NULL,
    comments_permission integer NOT NULL,
    time_updated timestamp without time zone DEFAULT now()
)
GO;


CREATE TABLE comments (
    comment_id bigint NOT NULL,
    parent_comment_id bigint NOT NULL,
    owner_profile_id bigint NOT NULL,
    object_id integer NOT NULL,
    object_type_id integer NOT NULL,
    level integer NOT NULL,
    childs_count integer DEFAULT 0 NOT NULL,
    author_profile_id bigint NOT NULL,
    time_added timestamp without time zone DEFAULT now() NOT NULL,
    is_deleted boolean DEFAULT false NOT NULL,
    text text NOT NULL,
    text_html text NOT NULL
)
GO;


CREATE TABLE custom_permission (
    object_id bigint NOT NULL,
    object_type bigint NOT NULL,
    owner_profile_id bigint NOT NULL,
    client_profile_id bigint NOT NULL,
    permission_type integer NOT NULL
)
GO;


CREATE TABLE daily_features (
    profile_id bigint NOT NULL,
    post_text text,
    post_text_html text,
    feature_id bigint NOT NULL
)
GO;


CREATE TABLE events (
    profile_id bigint NOT NULL,
    description text NOT NULL,
    place character varying(512) NOT NULL,
    description_html text,
    user_join_permission integer NOT NULL,
    group_join_permission integer NOT NULL,
    is_merchant boolean DEFAULT false,
    special_offer character varying(256) DEFAULT NULL::character varying,
    age_limit character varying(256) DEFAULT NULL::character varying,
    event_ticket_type integer DEFAULT 0 NOT NULL
)
GO;


CREATE TABLE favorites (
    owner_profile_id bigint NOT NULL,
    object_id bigint NOT NULL,
    object_type_id integer NOT NULL,
    client_id bigint NOT NULL
)
GO;


CREATE TABLE feed (
    item_id bigint NOT NULL,
    type integer NOT NULL,
    profile_id bigint NOT NULL,
    object_id bigint NOT NULL,
    group_id bigint NOT NULL,
    time_added timestamp without time zone DEFAULT now() NOT NULL,
    time_updated timestamp without time zone DEFAULT now() NOT NULL
)
GO;


CREATE TABLE global_region (
    region_id integer NOT NULL,
    region_name_ru character varying(50) NOT NULL,
    region_name_en character varying(50) NOT NULL
)
GO;


CREATE TABLE groups (
    profile_id bigint NOT NULL,
    description text,
    description_html text,
    biography text,
    biography_html text,
    contact_info text,
    site_url character varying(512),
    avatar_rotation_style integer,
    avatar_rotation_period integer,
    join_permission integer,
    download_content_permission integer,
    events_create_permission integer,
    events_order_permission integer,
    members_notification_enabled boolean,
    is_merchant boolean DEFAULT false NOT NULL,
    genre text,
    time_added timestamp without time zone DEFAULT now() NOT NULL,
    is_artist boolean DEFAULT false NOT NULL,
    image_id bigint,
    image_url character varying(256),
    background_url text
)
GO;


CREATE TABLE notifications (
    notification_id bigint NOT NULL,
    profile_id bigint,
    notification_type_id integer,
    text_html text,
    is_read boolean,
    time_added timestamp without time zone DEFAULT now(),
    source_id bigint DEFAULT 1 NOT NULL,
    group_id bigint DEFAULT 1 NOT NULL,
    group_size bigint DEFAULT 0 NOT NULL,
    object_id bigint,
    owner_id bigint,
    params_json text
)
GO;


CREATE TABLE photo_albums (
    album_id bigint NOT NULL,
    owner_profile_id bigint NOT NULL,
    author_profile_id bigint NOT NULL,
    album_type_id integer DEFAULT 1 NOT NULL,
    title character varying(512),
    image_url character varying(256),
    image_id bigint,
    tags character varying(256),
    view_permission integer NOT NULL,
    time_added timestamp without time zone DEFAULT now() NOT NULL,
    time_updated timestamp without time zone DEFAULT now() NOT NULL,
    photos_count integer DEFAULT 0 NOT NULL,
    views_count integer DEFAULT 0 NOT NULL
)
GO;


CREATE TABLE photos (
    photo_id bigint NOT NULL,
    album_id bigint NOT NULL,
    owner_profile_id bigint NOT NULL,
    author_profile_id bigint NOT NULL,
    description character varying(512),
    tags character varying(256),
    image_url character varying(512) NOT NULL,
    views_count integer DEFAULT 0 NOT NULL,
    time_added timestamp without time zone DEFAULT now()
)
GO;


CREATE TABLE playlist_tracks (
    owner_profile_id bigint NOT NULL,
    playlist_id integer NOT NULL,
    order_num bigint NOT NULL,
    track_id bigint NOT NULL
)
GO;


CREATE SEQUENCE playlist_tracks_order_num_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
GO;


CREATE TABLE playlists (
    playlist_id integer NOT NULL,
    owner_profile_id bigint NOT NULL,
    name character varying(250) NOT NULL,
    tracks_count integer DEFAULT 0 NOT NULL
)
GO;


CREATE TABLE posts (
    id bigint NOT NULL,
    profile_id bigint NOT NULL,
    campaign_id bigint,
    title character varying(256),
    heading_text text NOT NULL,
    post_text text NOT NULL,
    time_added timestamp without time zone NOT NULL,
    time_updated timestamp without time zone NOT NULL,
    notification_time timestamp without time zone,
    wall_post_id bigint,
    blog_post_id bigint,
    event_date timestamp without time zone,
    event_location text,
    tag_id bigint,
    image_url text,
    category_id bigint,
    event_type bigint,
    weight integer,
    image_id numeric
)
GO;


CREATE TABLE profile_backgrounds (
    profile_id bigint,
    background_id bigint,
    background_image_id bigint,
    background_color character varying(50)
)
GO;


CREATE TABLE profile_block_settings (
    profile_id bigint NOT NULL,
    block_type_id integer NOT NULL,
    view_permission integer DEFAULT 0 NOT NULL,
    comment_permission integer DEFAULT 0 NOT NULL,
    write_permission integer DEFAULT 0 NOT NULL,
    vote_permission integer DEFAULT 0 NOT NULL,
    block_name character varying(32) DEFAULT NULL::character varying,
    add_permission integer NOT NULL
)
GO;


CREATE TABLE profile_communication_channels (
    profile_id bigint NOT NULL,
    channel_id integer NOT NULL,
    channel_value character varying(250)
)
GO;


CREATE TABLE profile_dialogs (
    profile_id bigint NOT NULL,
    companion_profile_id integer,
    dialog_id bigint NOT NULL,
    unread_messages_count integer DEFAULT 0 NOT NULL,
    has_messages boolean DEFAULT false NOT NULL,
    time_added timestamp without time zone DEFAULT now() NOT NULL,
    time_updated timestamp without time zone DEFAULT now() NOT NULL,
    first_unread_message_id bigint DEFAULT 0 NOT NULL
)
GO;


CREATE TABLE profile_feed_settings (
    profile_id bigint NOT NULL,
    subject_profile_id bigint NOT NULL,
    item_id bigint,
    type bigint
)
GO;


CREATE TABLE profile_files (
    file_id bigint NOT NULL,
    owner_profile_id bigint NOT NULL,
    author_profile_id bigint NOT NULL,
    file_name text NOT NULL,
    file_url text NOT NULL,
    description text NOT NULL,
    time_added timestamp without time zone DEFAULT now() NOT NULL,
    time_updated timestamp without time zone DEFAULT now() NOT NULL,
    file_size bigint,
    extension character varying(32),
    title text
)
GO;


CREATE TABLE profile_last_visit (
    profile_id bigint NOT NULL,
    profile_last_visit_type_id integer NOT NULL,
    object_id bigint NOT NULL,
    time_updated timestamp without time zone DEFAULT now() NOT NULL
)
GO;


CREATE TABLE profile_news (
    id bigint NOT NULL,
    profile_id bigint NOT NULL,
    type profile_news_type NOT NULL,
    time_added timestamp without time zone NOT NULL,
    object_id bigint NOT NULL,
    is_deleted boolean DEFAULT false NOT NULL,
    extra_params json,
    internal boolean DEFAULT false NOT NULL,
    campaign_id bigint
)
GO;


CREATE TABLE profile_relations (
    profile_id bigint NOT NULL,
    subject_profile_id bigint NOT NULL,
    status integer NOT NULL,
    time_updated timestamp without time zone DEFAULT now() NOT NULL,
    source_profile_id bigint NOT NULL
)
GO;


CREATE TABLE profile_settings (
    profile_id bigint NOT NULL,
    default_tab character varying(32),
    vote_permission integer,
    background_rotation_style integer,
    background_rotation_period integer,
    daily_rotation_style integer,
    daily_rotation_period integer,
    daily_feature_id integer,
    background_id bigint,
    play_sound_when_new_message_arrives boolean DEFAULT true NOT NULL
)
GO;


CREATE TABLE profile_sites (
    profile_id bigint NOT NULL,
    site_url text,
    vk_url text,
    facebook_url text,
    twitter_url text,
    google_url text,
    time_added timestamp without time zone,
    time_updated timestamp without time zone
)
GO;


CREATE TABLE profile_status (
    profile_id bigint NOT NULL,
    text_status character varying(160) NOT NULL,
    time_added timestamp without time zone DEFAULT now() NOT NULL
)
GO;


CREATE TABLE profile_status_history (
    profile_id bigint NOT NULL,
    text_status character varying(160) NOT NULL,
    time_added timestamp without time zone DEFAULT now() NOT NULL,
    status_id bigint
)
GO;


CREATE TABLE profile_subscription (
    profile_id bigint NOT NULL,
    subject_profile_id bigint NOT NULL,
    time_updated timestamp without time zone DEFAULT now(),
    is_admin boolean DEFAULT false
)
GO;


CREATE TABLE profile_timeline (
    item_id bigint NOT NULL,
    profile_id bigint NOT NULL,
    title text NOT NULL,
    title_html text NOT NULL,
    item_type_id integer NOT NULL,
    description text NOT NULL,
    description_html text NOT NULL,
    item_date timestamp without time zone NOT NULL,
    time_added timestamp without time zone DEFAULT now() NOT NULL,
    time_updated timestamp without time zone DEFAULT now() NOT NULL
)
GO;


CREATE TABLE region (
    region_id integer NOT NULL,
    country_id integer NOT NULL,
    region_name_ru character varying(255),
    region_name_en character varying(255)
)
GO;


CREATE SEQUENCE seq_posts_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
GO;


CREATE SEQUENCE seq_profile_news_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
GO;


CREATE SEQUENCE seq_vote_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
GO;


CREATE TABLE tag_objects (
    tag_id bigint NOT NULL,
    profile_id bigint NOT NULL,
    object_type_id integer DEFAULT 0 NOT NULL,
    object_id bigint NOT NULL
)
GO;


CREATE TABLE tags (
    tag_id bigint NOT NULL,
    profile_id bigint NOT NULL,
    object_type_id integer DEFAULT 0 NOT NULL,
    tag_name character varying(128) NOT NULL,
    objects_count integer NOT NULL
)
GO;


CREATE TABLE users (
    profile_id bigint NOT NULL,
    martial_status integer DEFAULT 0 NOT NULL,
    description text,
    description_html text,
    friends_count integer DEFAULT 0 NOT NULL,
    groups_count integer DEFAULT 0 NOT NULL,
    users_request_in_count integer DEFAULT 0 NOT NULL,
    users_request_out_count integer DEFAULT 0 NOT NULL,
    notification_email character varying(64)
)
GO;


CREATE TABLE videos (
    video_id bigint NOT NULL,
    owner_profile_id bigint NOT NULL,
    author_profile_id bigint NOT NULL,
    name character varying(250) NOT NULL,
    description text,
    duration integer DEFAULT 0 NOT NULL,
    image_url character varying(256),
    image_id bigint,
    video_url character varying(512) NOT NULL,
    available_quality_modes integer DEFAULT 0 NOT NULL,
    status smallint DEFAULT 1 NOT NULL,
    time_added timestamp without time zone DEFAULT now(),
    time_updated timestamp without time zone DEFAULT now(),
    views_count integer DEFAULT 0 NOT NULL,
    downloads_count integer DEFAULT 0 NOT NULL,
    video_type smallint DEFAULT 1 NOT NULL,
    view_permission integer DEFAULT 1 NOT NULL,
    cached_video_id bigint DEFAULT 0 NOT NULL,
    storyboard_url character varying(256) DEFAULT NULL::character varying
)
GO;


CREATE TABLE wall_posts (
    wall_post_id bigint NOT NULL,
    owner_profile_id bigint NOT NULL,
    author_profile_id bigint NOT NULL,
    view_permission integer NOT NULL,
    text_html text,
    post_type integer NOT NULL,
    repost_profile_id bigint DEFAULT 0 NOT NULL,
    repost_wall_post_id bigint DEFAULT 0 NOT NULL,
    share_url character varying(512),
    time_added timestamp without time zone DEFAULT now()
)
GO;


ALTER TABLE ONLY playlist_tracks ALTER COLUMN order_num SET DEFAULT nextval('playlist_tracks_order_num_seq'::regclass)
GO;

ALTER TABLE ONLY audio_albums
    ADD CONSTRAINT audio_albums_pkey PRIMARY KEY (owner_profile_id, album_id)
GO;

ALTER TABLE ONLY blog_categories
    ADD CONSTRAINT blog_categories_pkey PRIMARY KEY (profile_id, category_id)
GO;

ALTER TABLE ONLY blog_posts
    ADD CONSTRAINT blog_posts_pkey PRIMARY KEY (owner_profile_id, blog_post_id)
GO;

ALTER TABLE ONLY broadcast_product_targeting
    ADD CONSTRAINT broadcast_product_targeting_pkey PRIMARY KEY (broadcast_id, product_id)
GO;

ALTER TABLE ONLY broadcast_streams
    ADD CONSTRAINT broadcast_streams_pkey PRIMARY KEY (stream_id)
GO;

ALTER TABLE ONLY broadcast_stubs
    ADD CONSTRAINT broadcast_stubs_pkey PRIMARY KEY (broadcast_id)
GO;

ALTER TABLE ONLY broadcasts
    ADD CONSTRAINT broadcasts_pkey PRIMARY KEY (broadcast_id)
GO;

ALTER TABLE ONLY chats
    ADD CONSTRAINT chats_pkey PRIMARY KEY (owner_profile_id, chat_id)
GO;

ALTER TABLE ONLY city
    ADD CONSTRAINT city_pkey PRIMARY KEY (city_id)
GO;

ALTER TABLE ONLY comments
    ADD CONSTRAINT comments_pkey PRIMARY KEY (owner_profile_id, comment_id)
GO;

ALTER TABLE ONLY country
    ADD CONSTRAINT country_pkey PRIMARY KEY (country_id)
GO;

ALTER TABLE ONLY events
    ADD CONSTRAINT events_pkey PRIMARY KEY (profile_id)
GO;

ALTER TABLE ONLY favorites
    ADD CONSTRAINT favorites_pkey PRIMARY KEY (owner_profile_id, object_id, object_type_id, client_id)
GO;

ALTER TABLE ONLY feed
    ADD CONSTRAINT feed_pkey PRIMARY KEY (item_id)
GO;

ALTER TABLE ONLY global_region
    ADD CONSTRAINT global_region_pkey PRIMARY KEY (region_id)
GO;

ALTER TABLE ONLY audio_tracks
    ADD CONSTRAINT group_audio_tracks_pkey PRIMARY KEY (owner_profile_id, track_id)
GO;

ALTER TABLE ONLY groups
    ADD CONSTRAINT groups_pkey PRIMARY KEY (profile_id)
GO;

ALTER TABLE ONLY chat_messages
    ADD CONSTRAINT messages_pkey PRIMARY KEY (owner_profile_id, message_id)
GO;

ALTER TABLE ONLY notifications
    ADD CONSTRAINT notifications_pkey PRIMARY KEY (notification_id)
GO;

ALTER TABLE ONLY photo_albums
    ADD CONSTRAINT photo_albums_pkey PRIMARY KEY (owner_profile_id, album_id)
GO;

ALTER TABLE ONLY photos
    ADD CONSTRAINT photos_pkey PRIMARY KEY (owner_profile_id, album_id, photo_id)
GO;

ALTER TABLE ONLY playlist_tracks
    ADD CONSTRAINT playlist_tracks_pkey PRIMARY KEY (owner_profile_id, playlist_id, order_num)
GO;

ALTER TABLE ONLY playlists
    ADD CONSTRAINT playlists_pkey PRIMARY KEY (owner_profile_id, playlist_id)
GO;

ALTER TABLE ONLY posts
    ADD CONSTRAINT posts_pkey PRIMARY KEY (id)
GO;

ALTER TABLE ONLY profile_block_settings
    ADD CONSTRAINT profile_block_settings_pkey PRIMARY KEY (profile_id, block_type_id)
GO;

ALTER TABLE ONLY comment_objects
    ADD CONSTRAINT profile_comments_pkey PRIMARY KEY (profile_id, object_type_id, object_id)
GO;

ALTER TABLE ONLY profile_communication_channels
    ADD CONSTRAINT profile_communication_channels_pkey PRIMARY KEY (profile_id, channel_id)
GO;

ALTER TABLE ONLY profile_dialogs
    ADD CONSTRAINT profile_dialogs_companion_profile_id_idx UNIQUE (profile_id, companion_profile_id)
GO;

ALTER TABLE ONLY profile_dialogs
    ADD CONSTRAINT profile_dialogs_pkey PRIMARY KEY (profile_id, dialog_id)
GO;

ALTER TABLE ONLY profile_files
    ADD CONSTRAINT profile_files_pkey PRIMARY KEY (owner_profile_id, file_id)
GO;

ALTER TABLE ONLY profile_last_visit
    ADD CONSTRAINT profile_last_visit_pkey PRIMARY KEY (profile_id, profile_last_visit_type_id, object_id)
GO;

ALTER TABLE ONLY profile_news
    ADD CONSTRAINT profile_news_pkey PRIMARY KEY (id)
GO;

ALTER TABLE ONLY profile_relations
    ADD CONSTRAINT profile_relations_pkey PRIMARY KEY (profile_id, subject_profile_id)
GO;

ALTER TABLE ONLY profile_settings
    ADD CONSTRAINT profile_settings_pkey PRIMARY KEY (profile_id)
GO;

ALTER TABLE ONLY profile_sites
    ADD CONSTRAINT profile_sites_pkey PRIMARY KEY (profile_id)
GO;

ALTER TABLE ONLY profile_status
    ADD CONSTRAINT profile_status_pkey PRIMARY KEY (profile_id)
GO;

ALTER TABLE ONLY profile_subscription
    ADD CONSTRAINT profile_subscription_pkey PRIMARY KEY (profile_id, subject_profile_id)
GO;

ALTER TABLE ONLY profile_timeline
    ADD CONSTRAINT profile_timeline_pkey PRIMARY KEY (item_id)
GO;

ALTER TABLE ONLY profiles
    ADD CONSTRAINT profiles_pkey PRIMARY KEY (profile_id)
GO;

ALTER TABLE ONLY region
    ADD CONSTRAINT region_pkey PRIMARY KEY (region_id)
GO;

ALTER TABLE ONLY tag_objects
    ADD CONSTRAINT tag_objects_pkey PRIMARY KEY (profile_id, tag_id, object_id, object_type_id)
GO;

ALTER TABLE ONLY tags
    ADD CONSTRAINT tags_pkey PRIMARY KEY (profile_id, tag_id)
GO;

ALTER TABLE ONLY users
    ADD CONSTRAINT users_pkey PRIMARY KEY (profile_id)
GO;

ALTER TABLE ONLY videos
    ADD CONSTRAINT videos_pkey PRIMARY KEY (owner_profile_id, video_id)
GO;

ALTER TABLE ONLY wall_posts
    ADD CONSTRAINT wall_posts_pkey PRIMARY KEY (owner_profile_id, wall_post_id)
GO;

CREATE INDEX broadcast_private_targeting_idx ON broadcast_private_targeting USING btree (profile_id, broadcast_id)
GO;

CREATE INDEX chat_messages_idx ON chat_messages USING btree (owner_profile_id, message_id)
GO;

CREATE INDEX city_city_name_en_idx ON city USING btree (city_name_en varchar_ops)
GO;

CREATE INDEX city_city_name_ru_idx ON city USING btree (city_name_ru varchar_pattern_ops)
GO;

CREATE INDEX city_country_id_idx ON city USING btree (country_id)
GO;

CREATE INDEX city_region_id_idx ON city USING btree (region_id)
GO;

CREATE INDEX comments_author_profile_id_idx ON comments USING btree (author_profile_id)
GO;

CREATE INDEX country_global_region_id_idx ON country USING btree (global_region_id)
GO;

CREATE INDEX favorites_idx ON favorites USING btree (client_id)
GO;

CREATE INDEX feed_profile_id_time_updated_idx ON feed USING btree (profile_id, time_updated)
GO;

CREATE UNIQUE INDEX feed_profile_id_type_object_id_idx ON feed USING btree (profile_id, type, object_id)
GO;

CREATE INDEX feed_time_updated_idx ON feed USING btree (time_updated)
GO;

CREATE INDEX photos_idx ON photos USING btree (photo_id)
GO;

CREATE INDEX posts_campaign_id_idx ON posts USING btree (campaign_id)
GO;

CREATE INDEX posts_profile_id_idx ON posts USING btree (profile_id)
GO;

CREATE INDEX posts_time_added_idx ON posts USING btree (time_added)
GO;

CREATE INDEX profile_backgrounds_idx ON profile_backgrounds USING btree (profile_id)
GO;

CREATE INDEX profile_feed_settings_idx ON profile_feed_settings USING btree (profile_id)
GO;

CREATE INDEX profile_has_active_projects_subscribers_count_idx ON profiles USING btree (has_active_projects, subscribers_count)
GO;

CREATE INDEX profile_news_object_id_type_idx ON profile_news USING btree (object_id, type)
GO;

CREATE INDEX profile_news_profile_id_idx ON profile_news USING btree (profile_id)
GO;

CREATE INDEX profile_news_time_added_idx ON profile_news USING btree (time_added)
GO;

CREATE INDEX profile_projects_count_backed_count_idx ON profiles USING btree (projects_count DESC, backed_count DESC)
GO;

CREATE INDEX profile_subscription_profile_id_time_updated_idx ON profile_subscription USING btree (profile_id, time_updated)
GO;

CREATE INDEX profile_subscription_subject_profile_id_profile_id_idx ON profile_subscription USING btree (subject_profile_id, profile_id)
GO;

CREATE INDEX profile_subscription_subject_profile_id_time_updated_idx ON profile_subscription USING btree (subject_profile_id, time_updated)
GO;

CREATE UNIQUE INDEX profiles_alias_uq ON profiles USING btree (alias)
GO;

CREATE INDEX profiles_creator_profile_id_idx ON profiles USING btree (creator_profile_id)
GO;

CREATE INDEX profiles_profile_id_receive_my_campaign_newsletters_idx ON profiles USING btree (profile_id, receive_my_campaign_newsletters)
GO;

CREATE INDEX region_country_id_idx ON region USING btree (country_id)
GO;


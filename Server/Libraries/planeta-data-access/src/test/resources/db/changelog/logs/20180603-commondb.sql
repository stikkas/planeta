--liquibase formatted sql

--changeset stikkas:1 endDelimiter:\nGO;

SET search_path = commondb, pg_catalog
GO;

CREATE TYPE t_payment_system AS (
	payment_systems_id integer,
	name text
)
GO;

CREATE FUNCTION after_restore_from_backup() RETURNS void
    LANGUAGE plpgsql
    AS $_$
BEGIN
  DELETE FROM statdb.static_node;

  UPDATE profiledb.users             SET notification_email = 'noreply@planeta.ru';
  UPDATE commondb.campaign_contacts  SET email = 'noreply@planeta.ru';
  UPDATE commondb.mail_templates     SET to_address = 'noreply@planeta.ru' WHERE to_address LIKE '%planeta.ru%' AND name != 'error.report';
  UPDATE commondb.mail_templates     SET from_address = 'noreply@planeta.ru' WHERE name != 'error.report';
  UPDATE commondb.mail_templates     SET subject =
  '(' || COALESCE ((select str_value FROM commondb.configuration WHERE key = 'stand.name'), 'TEST')
  || ') В приложении ${applicationName} произошло ${errorsCount} ошибок'
  WHERE name = 'error.report';

  UPDATE commondb.planeta_managers   SET email = 'noreply@planeta.ru';
  UPDATE commondb.configuration      SET boolean_value = false where key = 'megaplan.enabled';
  UPDATE commondb.users_private_info SET email = replace(email,'@','_TEST_')
    , username = replace(username,'@','_TEST_')
  WHERE status & 32 != 32 and status & 64 != 64;
  UPDATE maildb.users SET email = replace(email,'@','_TEST_');
  UPDATE maildb."values" SET email = replace(email,'@','_TEST_');
  UPDATE maildb.campaigns SET time_to_send = NULL WHERE time_to_send > now() - interval '1 hour';
  UPDATE commondb.payment_providers SET  processor_params  =  processor_params_for_test;
  UPDATE bibliodb.library SET library_email = replace(library_email,'@','_TEST_');
  UPDATE bibliodb.book SET book_email = replace(book_email,'@','_TEST_');
END;
$_$
GO;

CREATE FUNCTION campaign_stats_city_interval(date_from timestamp without time zone, date_to timestamp without time zone) RETURNS TABLE(date date, campaign_id bigint, city character varying, visitors bigint, views bigint, purchases bigint, affiliate_amount numeric)
    LANGUAGE plpgsql
    AS $$
BEGIN
  RETURN QUERY
  select
    c.date,
    c.campaign_id,
    c.city,
    c.visitors,
    c.views,
    coalesce(b.purchases,0) as purchases,
    0::numeric as affiliate_amount
  from
    (
      SELECT a.date,
        a.campaign_id,
        a.city,
        count(DISTINCT a.visitor_id) AS visitors,
        sum(a.views) AS views,
        sum(a.affiliate_amount) AS affiliate_amount
      FROM ( SELECT (date_trunc('DAY'::text, e.date))::date AS date,
                    e.object_id AS campaign_id,
               e.city,
               visitor_id,
               e.affiliate_amount,
                    1 AS views
             FROM commondb.stat_events e
             WHERE e.date between date_from and date_to and type = 'CAMPAIGN_VIEW') a
      GROUP BY a.date, a.campaign_id, a.city
    ) c
    LEFT JOIN
    (
      SELECT
        s.campaign_id as campaign_id,
        coalesce(pc.city_name_ru, '') as city,
        count(distinct ogs.order_id) as purchases
      FROM commondb.shares s
        JOIN commondb.order_objects oo on oo.object_id = s.share_id
        JOIN commondb.orders o on o.order_id = oo.order_id
        JOIN statdb.order_geo_stats ogs on ogs.order_id = o.order_id
        LEFT JOIN profiledb.city pc on pc.city_id = ogs.city_id
      WHERE o.time_added between date_from and date_to
            AND o.payment_status in (1,2)
            AND o.order_type = 1
      GROUP BY s.campaign_id, pc.city_name_ru
    ) b
      on c.campaign_id = b.campaign_id
         and c.city = b.city;
END;
$$
GO;


CREATE FUNCTION campaign_stats_country_interval(date_from timestamp without time zone, date_to timestamp without time zone) RETURNS TABLE(date date, campaign_id bigint, country character varying, visitors bigint, views bigint, purchases bigint, affiliate_amount numeric)
    LANGUAGE plpgsql
    AS $$
BEGIN
  RETURN QUERY
  select
    c.date,
    c.campaign_id,
    c.country,
    c.visitors,
    c.views,
    coalesce(b.purchases,0) as purchases,
    0::numeric as affiliate_amount
  from
    (
      SELECT a.date,
        a.campaign_id,
        a.country,
        count(DISTINCT a.visitor_id) AS visitors,
        sum(a.views) AS views,
        sum(a.affiliate_amount) AS affiliate_amount
      FROM ( SELECT (date_trunc('DAY'::text, e.date))::date AS date,
                    e.object_id AS campaign_id,
               e.country,
               visitor_id,
               e.affiliate_amount,
                    1 as views
             FROM commondb.stat_events e
             WHERE e.date between date_from and date_to and type = 'CAMPAIGN_VIEW') a
      GROUP BY a.date, a.campaign_id, a.country
    ) c
    LEFT JOIN
    (
      SELECT
        s.campaign_id as campaign_id,
        coalesce(pc.country_name_ru, '') as country,
        count(distinct ogs.order_id) as purchases
      FROM commondb.shares s
        JOIN commondb.order_objects oo on oo.object_id = s.share_id
        JOIN commondb.orders o on o.order_id = oo.order_id
        JOIN statdb.order_geo_stats ogs on ogs.order_id = o.order_id
        LEFT JOIN profiledb.country pc on pc.country_id = ogs.country_id
      WHERE o.time_added between date_from and date_to
            AND o.payment_status in (1,2)
            AND o.order_type = 1
      GROUP BY s.campaign_id, pc.country_name_ru
    ) b
      on c.campaign_id = b.campaign_id
         and c.country = b.country;
END;
$$
GO;


CREATE FUNCTION campaign_stats_general_interval(date_from timestamp without time zone, date_to timestamp without time zone) RETURNS TABLE(date date, campaign_id bigint, visitors bigint, views bigint, purchases bigint, buyers bigint, amount numeric, affiliate_amount numeric, comments bigint)
    LANGUAGE plpgsql
    AS $$
BEGIN
  RETURN QUERY

  select
    c.stat_date as date,
    c.campaign_id,
    c.visitors,
    c.views,
    coalesce(b.purchases, 0),
    coalesce(b.buyers, 0),
    coalesce(b.amount, 0),
    0::numeric as affiliate_amount,
    c.comments
  from
    (SELECT a.stat_date,
       a.campaign_id,
       count(DISTINCT a.visitor_id) AS visitors,
       sum(a.views) AS views,
       sum(a.comments) AS comments
     FROM ( SELECT (date_trunc('DAY'::text, e.date))::date AS stat_date,
                   e.object_id AS campaign_id,
                   CASE e.type
                   WHEN 'CAMPAIGN_VIEW'::text THEN e.visitor_id
                   ELSE NULL::bigint
                   END AS visitor_id,
                   CASE e.type
                   WHEN 'CAMPAIGN_VIEW'::text THEN 1
                   ELSE 0
                   END AS views,
                   CASE e.type
                   WHEN 'CAMPAIGN_COMMENT'::text THEN 1
                   ELSE 0
                   END AS comments
            FROM commondb.stat_events e
            WHERE e.date between date_from and date_to) a
     GROUP BY a.stat_date, a.campaign_id) c
    LEFT JOIN
    (
      SELECT
        t.campaign_id,
        t.stat_date,
        count(distinct t.order_id) as purchases,
        count(distinct t.buyer_id) as buyers,
        sum(t.amount_net) as amount
      FROM
        (
          select distinct s.campaign_id,
            date_trunc('DAY'::text, dt.time_added)::date AS stat_date,
            o.order_id,
            o.buyer_id,
            dt.amount_net from
            commondb.shares s
            JOIN commondb.order_objects oo on oo.object_id = s.share_id
            JOIN commondb.orders o on o.order_id = oo.order_id
            JOIN commondb.debit_transactions dt ON dt.transaction_id = o.debit_transaction_id
          WHERE dt.time_added between date_from and date_to
                AND o.payment_status = 1
                AND o.order_type = 1
        ) t
      GROUP BY t.campaign_id, stat_date
    ) b
      on c.campaign_id = b.campaign_id
         and c.stat_date = b.stat_date;
END;
$$
GO;


CREATE FUNCTION campaign_stats_ref_interval(date_from timestamp without time zone, date_to timestamp without time zone) RETURNS TABLE(date date, campaign_id bigint, referer character varying, visitors bigint, views bigint, purchases bigint, affiliate_amount numeric)
    LANGUAGE plpgsql
    AS $$
BEGIN
  RETURN QUERY
  select
    c.stat_date as date,
    c.campaign_id,
    c.referer,
    c.visitors,
    c.views,
    coalesce(b.purchases,0) as purchases,
    0::numeric as affiliate_amount
  from
    (
      SELECT a.stat_date,
        a.campaign_id,
        a.referer,
        count(DISTINCT a.visitor_id) AS visitors,
        sum(a.views) AS views
      FROM ( SELECT (date_trunc('DAY'::text, e.date))::date AS stat_date,
                    e.object_id AS campaign_id,
               e.referer,
               e.visitor_id,
                    1 AS views
             FROM commondb.stat_events e
             WHERE e.date between date_from and date_to AND type in ('CAMPAIGN_VIEW')) a
      GROUP BY a.stat_date, a.campaign_id, a.referer
    ) c
    LEFT JOIN
    (
      SELECT s.campaign_id,
        count(distinct o.order_id) as purchases,
        coalesce(coalesce(substring(ogs.referrer_url from '.*://([^/]*)'), ogs.referrer_url),'') as referer
      from
        commondb.shares s
        JOIN commondb.order_objects oo on oo.object_id = s.share_id
        JOIN commondb.orders o on o.order_id = oo.order_id
        JOIN statdb.order_geo_stats ogs on ogs.order_id = o.order_id
      WHERE o.time_added between date_from and date_to
            AND o.payment_status in (1,2)
            AND o.order_type = 1
      GROUP BY 1, 3
    ) b
      on c.campaign_id = b.campaign_id
         and c.referer = b.referer;
END;
$$
GO;

CREATE FUNCTION can_be_vip(profile_id bigint, limit_count integer, amount integer, second_amount integer) RETURNS boolean
    LANGUAGE plpgsql
    AS $$
DECLARE
  last_time_bonus timestamp;
  bonuses_count integer;
  orders_count integer;
  last_amount numeric;
  year_start timestamp;
BEGIN
  select date_trunc('year', now()) into year_start;
  SELECT o.time_added into last_time_bonus FROM commondb.orders o
  WHERE o.order_type = 7 AND (o.payment_status = 0 OR o.payment_status = 1) AND o.buyer_id = profile_id ORDER BY o.time_added DESC LIMIT 1;
  SELECT count(o.order_id) into bonuses_count FROM commondb.orders o
  WHERE o.order_type = 7 AND (o.payment_status = 0 OR o.payment_status = 1) AND o.buyer_id = profile_id AND o.time_added > year_start;
  if bonuses_count >= 12 THEN
    RETURN false;
  END IF;
  IF  last_time_bonus IS NULL THEN
    SELECT COUNT(o.order_id), SUM(o.total_price) into orders_count, last_amount
    FROM commondb.orders o
    WHERE o.order_type IN (1, 2, 11) AND o.payment_status = 1
          AND o.buyer_id = profile_id AND o.time_added > year_start GROUP BY o.buyer_id;
    RETURN orders_count >= limit_count AND last_amount >= amount;
  ELSE
    SELECT SUM(o.total_price) into last_amount
    FROM commondb.orders o
    WHERE o.order_type IN (1, 2, 11) AND o.payment_status = 1
          AND o.buyer_id = profile_id AND o.time_added > last_time_bonus AND o.time_added > year_start GROUP BY o.buyer_id;
    RETURN last_amount >= second_amount;
  END IF;
END;
$$
GO;

CREATE FUNCTION decrease_balance(p_profile_id bigint, p_amount numeric) RETURNS void
    LANGUAGE plpgsql
    AS $$
declare
  v_can_decrease boolean := false;
begin
  select (balance >= p_amount) into v_can_decrease 
    from commondb.profile_balances
    where profile_id = p_profile_id;
  
  if (v_can_decrease) then
    -- credit transaction creation
    insert into commondb.credit_transactions(transaction_id, profile_id, amount_net, amount_fee, comment)
      values (nextval('commondb.seq_credit_transaction_id'), p_profile_id, p_amount, 0, 'money returning');
    -- balance increasing
    update commondb.profile_balances
      set balance = (balance - p_amount)
      where profile_id = p_profile_id;
  end if;
end;
$$
GO;

CREATE FUNCTION delete_profile(p_id bigint) RETURNS integer
    LANGUAGE plpgsql
    AS $$
  DECLARE
  user_type INT;
  result TEXT;
BEGIN

  SELECT INTO user_type profile_type_id
  FROM profiledb.profiles
  WHERE profile_id = p_id;
  IF user_type = 1 THEN
    UPDATE profiledb.profiles
    SET
      image_url = null,
      image_id = 0,
      small_image_url = null,
      small_image_id = 0,
      name = null,
      alias = null,
      status = 8,
      city_id = 0,
      country_id = 0,
      rating = 0,
      user_birth_date = null,
      user_gender = 0,
      user_first_name = null,
      user_last_name = null,
      display_name = 'Пользователь удален',
      display_name_format = 0,
      summary = null
    WHERE profile_id = p_id;

    UPDATE commondb.users_private_info
    SET status = 4
    WHERE user_id = p_id;

    UPDATE  maildb.users
    SET unsubscribed = true
    WHERE email = (SELECT email FROM commondb.users_private_info  WHERE user_id = p_id);

    UPDATE commondb.users_private_info
    SET username = '_DELETED_'||user_id||'_'||username||'_'
      ,email = '_DELETED_'||user_id||'_'||email||'_',
      time_updated = now()
    WHERE user_id = p_id;

    UPDATE commondb.user_credentials
    SET username = '_DELETED_'||username
    WHERE profile_id = p_id;
    
    UPDATE profiledb.users
    SET notification_email = '_DELETED_'||profile_id||'_'||notification_email||'_'
    WHERE profile_id = p_id;
  END IF;

  IF user_type = 2 THEN
    UPDATE profiledb.profiles
    SET
      name = '_DELETED_'|| name,
      status = 8
    WHERE profile_id = p_id;
  END IF;

  DELETE FROM profiledb.audio_albums
  WHERE owner_profile_id = p_id;

  DELETE FROM profiledb.audio_tracks
  WHERE owner_profile_id = p_id;

  DELETE FROM profiledb.blog_categories
  WHERE profile_id = p_id;

  DELETE FROM profiledb.blog_posts
  WHERE owner_profile_id = p_id;

  DELETE FROM profiledb.comment_objects
  WHERE profile_id = p_id;

  DELETE FROM profiledb.daily_features
  WHERE profile_id = p_id;

  DELETE FROM profiledb.events
  WHERE profile_id = p_id;

  DELETE FROM profiledb.groups
  WHERE profile_id = p_id;

  DELETE FROM profiledb.notifications
  WHERE profile_id = p_id;

  DELETE FROM profiledb.photo_albums
  WHERE owner_profile_id = p_id;

  DELETE FROM profiledb.photos
  WHERE owner_profile_id = p_id;

  DELETE FROM profiledb.playlist_tracks
  WHERE owner_profile_id = p_id;

  DELETE FROM profiledb.playlists
  WHERE owner_profile_id = p_id;

  DELETE FROM profiledb.profile_backgrounds
  WHERE profile_id = p_id;

  DELETE FROM profiledb.profile_block_settings
  WHERE profile_id = p_id;

  DELETE FROM profiledb.profile_communication_channels
  WHERE profile_id = p_id;

  DELETE FROM profiledb.profile_dialogs
  WHERE profile_id = p_id
        OR companion_profile_id = p_id;

  DELETE FROM profiledb.profile_news
  WHERE profile_id = p_id;

  DELETE FROM profiledb.profile_subscription
  WHERE profile_id = p_id or subject_profile_id = p_id;

  DELETE FROM profiledb.profile_relations
  WHERE profile_id = p_id
        OR subject_profile_id = p_id;

  DELETE FROM profiledb.profile_settings
  WHERE profile_id = p_id;

  DELETE FROM profiledb.profile_status
  WHERE profile_id = p_id;

  DELETE FROM profiledb.tag_objects
  WHERE profile_id = p_id;

  DELETE FROM profiledb.tags
  WHERE profile_id = p_id;

  DELETE FROM profiledb.videos
  WHERE owner_profile_id = p_id;

  DELETE FROM profiledb.wall_posts
  WHERE owner_profile_id = p_id;

  SELECT commondb.delete_profile_feed(p_id) into result;
  RETURN user_type;
END;
$$
GO;

CREATE FUNCTION delete_profile_feed(p_id bigint) RETURNS text
    LANGUAGE plpgsql
    AS $$
  DECLARE
    result TEXT;
BEGIN

  SELECT
    PUBLIC.upload_page('http://feed.planeta.ru/delete-feed-item-with-type-code.json'
    || '?profileId=' || profile_id
    || '&objectId=' || object_id
    || '&feedItemTypeCode=' || TYPE, '') into result
  FROM profiledb.feed
  WHERE profile_id = p_id;
  RETURN result;
END;
$$
GO;

CREATE FUNCTION delete_user_posts(p_profile_id bigint, p_days_count numeric) RETURNS bigint
    LANGUAGE plpgsql
    AS $$
DECLARE
BEGIN
DELETE FROM profiledb.blog_posts bp
WHERE bp.author_profile_id = p_profile_id AND date_part('day', now() - bp.time_added) < p_days_count;
DELETE FROM profiledb.wall_posts wp
WHERE wp.author_profile_id = p_profile_id AND date_part('day', now() - wp.time_added) < p_days_count;
RETURN 0;
END;
$$
GO;

CREATE FUNCTION faq_split(cat_id integer) RETURNS TABLE(id integer, article text, article_id integer, paragraph_title text, paragraph_id integer, text_html text, annotation text)
    LANGUAGE plpgsql
    AS $$
  DECLARE
    arrHtml             text [];
    arrParts            text [];
    result              text [];
    arrTmp              text [];
    articleTitle        text = 'Без названия';
    articleTitleIndex   integer = 0;
    paragraphTitle      text = 'Без названия';
    paragraphTitleIndex integer = 0;
    currHtml            text;
    html                text;
    currAnnotation      text;
  BEGIN
    html := (select
               category_content_html
             from commondb.faq_draft_category fdc
             where fdc.category_id = cat_id);
    DROP TABLE IF EXISTS temp1;
    CREATE TEMP TABLE temp1 (id integer, article text, article_id integer, paragraph_title text, paragraph_id integer, text_html text, annotation text);
    arrHtml := regexp_split_to_array(html, E'(<p>&nbsp;</p>\\s*)+', 'is');
    FOR I IN array_lower(arrHtml, 1)..array_upper(arrHtml, 1) LOOP
      currHtml := arrHtml [I];
      arrTmp := regexp_matches(currHtml, E'<span class="mce-underline">\\s*<strong>(.*)</strong>\\s*</span>', 'igs');
      IF arrTmp != '{}'
      THEN articleTitle = arrTmp [1];
        articleTitleIndex := articleTitleIndex + 1; END IF;
      currHtml := regexp_replace(currHtml, E'<span class="mce-underline">\\s*<strong>(.*)</strong>\\s*</span>', '',
                                 'igs');
      arrTmp := regexp_matches(currHtml, E'<strong>(.*?)</strong>');
      IF arrTmp != '{}'
      THEN paragraphTitle = arrTmp [1];
        paragraphTitleIndex := paragraphTitleIndex + 1; END IF;
      currHtml := regexp_replace(currHtml, E'<strong>(.*?)</strong>', '', 'igs');
      currAnnotation := htmlunescape(regexp_replace(currHtml, E'<[^>]*>', '', 'igs'));
      insert into temp1
      values (I, articleTitle, articleTitleIndex, paragraphTitle, paragraphTitleIndex, currHtml, currAnnotation);
    END LOOP;
    RETURN QUERY select
                   *
                 from temp1;
  END;
  $$
GO;


CREATE FUNCTION generate_promo_codes(code_length integer, number integer, amount numeric, comments text) RETURNS void
    LANGUAGE plpgsql
    AS $$
 DECLARE
   result text := '';
   numDone integer := 0;
 BEGIN
   IF code_length < 0 THEN
     RAISE EXCEPTION 'Given length cannot be less than 0';
   END IF;

   WHILE numDone < number LOOP
    BEGIN
    result := commondb.random_string(code_length);
      INSERT INTO commondb.promo_codes (code, amount, comment) VALUES (result, amount, comments);
         numDone := numDone + 1;
     EXCEPTION WHEN unique_violation THEN
      RAISE NOTICE 'Duplicate code. Retrying...';
     END;
   END LOOP;
 END;
 $$
GO;


CREATE FUNCTION get_planeta_start_sitemap(host text DEFAULT 'planeta.ru'::text, results_offset bigint DEFAULT 0, results_limit bigint DEFAULT 50000) RETURNS text
    LANGUAGE plpgsql
    AS $$
DECLARE
  result TEXT;
BEGIN
SELECT E'<?xml version="1.0" encoding="UTF-8"?>\n' ||
xmlelement(name "urlset", XMLATTRIBUTES(E'http://www.sitemaps.org/schemas/sitemap/0.9' as "xmlns"),
       xmlagg(xmlelement(name "url",
		xmlelement(name "loc", loc::xml)) ORDER BY x.campaign_id ASC))
  INTO result
  FROM
(
 SELECT c.campaign_id,
   	    E'http://start.'||host||E'/campoaigns/'||c.campaign_id  AS loc
   FROM commondb.campaigns c
  WHERE c.status IN (4,5)
  UNION
  SELECT 0 AS campaign_id, E'http://start.'||host||E'/' AS loc
  UNION
  SELECT 0 AS campaign_id, E'http://start.'||host||E'/crowdfunding' AS loc
  UNION
  SELECT 0 AS campaign_id, E'http://start.'||host||E'/funding-rules' AS loc
) x
OFFSET results_offset LIMIT results_limit;
result := REGEXP_REPLACE(result, E'><',E'>\n<','ig');
result := REGEXP_REPLACE(result, E'\n<(/?url>|loc)',E'\n\t<\\1','ig');
result := REGEXP_REPLACE(result, E'\t<(/?loc)',E'\t\t<\\1','ig');
RETURN result;
END;
$$
GO;

CREATE FUNCTION increase_profile_balance(p_profile_id bigint, p_amount numeric, p_amount_fee numeric, p_comment text) RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN
   
  INSERT INTO commondb.debit_transactions(transaction_id, profile_id, amount_net, amount_fee, comment)
    VALUES (nextval('commondb.seq_debit_transaction_id'), p_profile_id, p_amount, p_amount_fee, p_comment);
    
  UPDATE commondb.profile_balances
    SET balance = (balance + p_amount)
    WHERE profile_id = p_profile_id;

END;
$$
GO;


CREATE FUNCTION insert_or_update_old_login_user_mail_send_info(send_user_id bigint) RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN
    LOOP
        -- first try to update the key
        UPDATE commondb.email_rich_notifications
        SET last_sending_date = now(),
            count_of_sent = count_of_sent + 1
        WHERE user_id = send_user_id;
        IF found THEN
            RETURN;
        END IF;
        -- not there, so try to insert the key
        -- if someone else inserts the same key concurrently,
        -- we could get a unique-key failure
        BEGIN
            INSERT INTO commondb.email_rich_notifications(user_id) VALUES (send_user_id);
            RETURN;
        EXCEPTION WHEN unique_violation THEN
            -- Do nothing, and loop to try the UPDATE again.
        END;
    END LOOP;
END;
$$
GO;

CREATE FUNCTION random_string(length integer) RETURNS text
    LANGUAGE plpgsql
    AS $$
 declare
   chars text[] := '{0,1,2,3,4,5,6,7,8,9,A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z}';
   result text := '';
   i integer := 0;
 begin
   if length < 0 then
     raise exception 'Given length cannot be less than 0';
   end if;
   for i in 1..length loop
     result := result || chars[1+random()*(array_length(chars, 1)-1)];
   end loop;
   return result;
 end;
 $$
GO;


CREATE FUNCTION renew_faq(faq_category_id integer) RETURNS void
    LANGUAGE plpgsql
    AS $$
  BEGIN
    insert into commondb.faq_articles (
      select distinct(article_id) + (select count(*) from commondb.faq_articles), article as article_title_html, faq_category_id
      from (select * from public.faq_split(faq_category_id) order by id) t1);


    insert into commondb.faq_paragraphs (
      select paragraph_id + (select count(*) from commondb.faq_paragraphs),
                                    article_id + (select count(*) from commondb.faq_articles where foreign_category_id < faq_category_id) as foreign_article_id,
                                    paragraph_title as paragraph_title_html,
                                    string_agg(text_html, '<br>') as paragraph_text_html,
                                    string_agg(annotation, '<br><br>') as paragraph_annotation_html
      from (select * from public.faq_split(faq_category_id) order by id) t2 group by paragraph_id, article_id, paragraph_title
    );
    update commondb.faq_draft_category set time_created = now() where category_id = faq_category_id;
    update commondb.faq_categories fc
    set category_title_html = (select category_title_html from commondb.faq_draft_category fdc where fc.category_id = fdc.category_id)
    where category_id = faq_category_id;
  END;
  $$
GO;


CREATE FUNCTION renew_faq(blog_post_id integer, faq_category_id integer) RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN
  insert into commondb.faq_articles (
    select distinct(article_id) + (select count(*) from commondb.faq_articles), article as article_title_html, faq_category_id 
    from (select * from public.faq_split(blog_post_id) order by id) t1);


  insert into commondb.faq_paragraphs (
    select paragraph_id + (select count(*) from commondb.faq_paragraphs),
                                  article_id + (select count(*) from commondb.faq_articles where foreign_category_id < faq_category_id) as foreign_article_id,
                                  paragraph_title as paragraph_title_html,
                                  string_agg(text_html, '<br>') as paragraph_text_html,
                                  string_agg(annotation, '<br><br>') as paragraph_annotation_html
    from (select * from public.faq_split(blog_post_id) order by id) t2 group by paragraph_id, article_id, paragraph_title);
END;
$$
GO;

CREATE TABLE address_relations (
    address_id bigint NOT NULL,
    addressee_id bigint NOT NULL,
    addressee_type_code smallint NOT NULL,
    last_used boolean DEFAULT false,
    time_added timestamp without time zone DEFAULT now()
)
GO;

CREATE TABLE addressees (
    addressee_id bigint NOT NULL,
    address_id bigint NOT NULL,
    name text,
    surname text,
    patronymic text,
    phone text,
    comment text,
    time_added timestamp without time zone DEFAULT now() NOT NULL
)
GO;

CREATE TABLE addressees_backup (
    addressee_id bigint,
    address_id bigint,
    name text,
    surname text,
    patronymic text,
    phone text,
    comment text
)
GO;

CREATE TABLE addresses (
    address_id bigint NOT NULL,
    street text NOT NULL,
    city text NOT NULL,
    country text,
    zip_code text,
    phone text,
    time_added timestamp without time zone DEFAULT now() NOT NULL
)
GO;


CREATE TABLE addresses_backup (
    address_id bigint,
    street text,
    city text,
    country text,
    zip_code text,
    phone text
)
GO;

CREATE UNLOGGED TABLE stat_events (
    id bigint NOT NULL,
    visitor_id bigint NOT NULL,
    date timestamp without time zone NOT NULL,
    uri character varying(255) NOT NULL,
    referer character varying(1000),
    type character varying(50) NOT NULL,
    city character varying(100),
    country character varying(100),
    source_id bigint NOT NULL,
    object_id bigint NOT NULL,
    affiliate_id bigint DEFAULT 0 NOT NULL,
    amount numeric DEFAULT 0 NOT NULL,
    affiliate_amount numeric DEFAULT 0 NOT NULL,
    busy boolean DEFAULT false,
    version bigint
)
GO;

CREATE VIEW affiliate_stats_campaign AS
 SELECT a.date,
    a.affiliate_id,
    a.campaign_id,
    count(DISTINCT a.visitor_id) AS visitors,
    sum(a.views) AS views,
    sum(a.purchases) AS purchases,
    sum(a.affiliate_amount) AS affiliate_amount
   FROM ( SELECT (date_trunc('DAY'::text, e.date))::date AS date,
            e.affiliate_id,
            e.object_id AS campaign_id,
            e.affiliate_amount,
                CASE e.type
                    WHEN 'CAMPAIGN_VIEW'::text THEN e.visitor_id
                    ELSE NULL::bigint
                END AS visitor_id,
                CASE e.type
                    WHEN 'CAMPAIGN_VIEW'::text THEN 1
                    ELSE 0
                END AS views,
                CASE e.type
                    WHEN 'CAMPAIGN_PURCHASE'::text THEN 1
                    ELSE 0
                END AS purchases
           FROM stat_events e) a
  GROUP BY a.date, a.affiliate_id, a.campaign_id
GO;

CREATE VIEW affiliate_stats_general AS
 SELECT a.date,
    a.affiliate_id,
    count(DISTINCT a.visitor_id) AS visitors,
    sum(a.views) AS views,
    sum(a.purchases) AS purchases,
    sum(a.affiliate_amount) AS affiliate_amount
   FROM ( SELECT (date_trunc('DAY'::text, e.date))::date AS date,
            e.affiliate_id,
            e.affiliate_amount,
                CASE e.type
                    WHEN 'CAMPAIGN_VIEW'::text THEN e.visitor_id
                    ELSE NULL::bigint
                END AS visitor_id,
                CASE e.type
                    WHEN 'CAMPAIGN_VIEW'::text THEN 1
                    ELSE 0
                END AS views,
                CASE e.type
                    WHEN 'CAMPAIGN_PURCHASE'::text THEN 1
                    ELSE 0
                END AS purchases
           FROM stat_events e) a
  GROUP BY a.date, a.affiliate_id
GO;

CREATE VIEW affiliate_stats_referer AS
 SELECT a.date,
    a.affiliate_id,
    a.referer,
    count(DISTINCT a.visitor_id) AS visitors,
    sum(a.views) AS views,
    sum(a.purchases) AS purchases,
    sum(a.affiliate_amount) AS affiliate_amount
   FROM ( SELECT (date_trunc('DAY'::text, e.date))::date AS date,
            e.affiliate_id,
            e.referer,
            e.affiliate_amount,
                CASE e.type
                    WHEN 'CAMPAIGN_VIEW'::text THEN e.visitor_id
                    ELSE NULL::bigint
                END AS visitor_id,
                CASE e.type
                    WHEN 'CAMPAIGN_VIEW'::text THEN 1
                    ELSE 0
                END AS views,
                CASE e.type
                    WHEN 'CAMPAIGN_PURCHASE'::text THEN 1
                    ELSE 0
                END AS purchases
           FROM stat_events e) a
  GROUP BY a.date, a.affiliate_id, a.referer
GO;

CREATE VIEW affiliate_stats_source AS
 SELECT a.date,
    a.affiliate_id,
    a.source_id,
    count(DISTINCT a.visitor_id) AS visitors,
    sum(a.views) AS views,
    sum(a.purchases) AS purchases,
    sum(a.affiliate_amount) AS affiliate_amount
   FROM ( SELECT (date_trunc('DAY'::text, e.date))::date AS date,
            e.affiliate_id,
            e.source_id,
            e.affiliate_amount,
                CASE e.type
                    WHEN 'CAMPAIGN_VIEW'::text THEN e.visitor_id
                    ELSE NULL::bigint
                END AS visitor_id,
                CASE e.type
                    WHEN 'CAMPAIGN_VIEW'::text THEN 1
                    ELSE 0
                END AS views,
                CASE e.type
                    WHEN 'CAMPAIGN_PURCHASE'::text THEN 1
                    ELSE 0
                END AS purchases
           FROM stat_events e) a
  GROUP BY a.date, a.affiliate_id, a.source_id
GO;

CREATE TABLE auth (
    auth_id bigint NOT NULL,
    auth_token character varying(256) NOT NULL,
    auth_authentication bytea NOT NULL,
    time_added timestamp without time zone NOT NULL
)
GO;

CREATE TABLE banner_keywords (
    banner_id bigint NOT NULL,
    keywords text NOT NULL
)
GO;

CREATE TABLE banners (
    banner_id bigint NOT NULL,
    name character varying(256) NOT NULL,
    html text,
    time_added timestamp without time zone DEFAULT now() NOT NULL,
    time_updated timestamp without time zone DEFAULT now() NOT NULL,
    status integer DEFAULT 0 NOT NULL,
    image_url character varying(256),
    mobile_image_url character varying(256),
    referer_url character varying(256),
    alt_text character varying(256),
    weight integer,
    mask character varying(256),
    mask_view character varying(256),
    target boolean,
    utm_marks character varying(256),
    views_count integer,
    click_count integer,
    type integer,
    time_start timestamp without time zone,
    time_finish timestamp without time zone,
    show_counter integer,
    targeting_type integer DEFAULT 1 NOT NULL
)
GO;

CREATE TABLE base_delivery_service (
    service_id bigint DEFAULT 0 NOT NULL,
    location_id bigint NOT NULL,
    name character varying(100) NOT NULL,
    description text,
    address_id bigint DEFAULT 0,
    is_enabled boolean DEFAULT true,
    type integer DEFAULT 0 NOT NULL,
    location_type integer DEFAULT 0 NOT NULL,
    name_en character varying(100) DEFAULT ''::character varying NOT NULL,
    description_en text
)
GO;

CREATE TABLE bills_for_1c (
    topay_transaction_id bigint NOT NULL,
    amount numeric(10,2),
    time_added timestamp without time zone DEFAULT now() NOT NULL,
    time_purchased timestamp without time zone,
    status integer
)
GO;

CREATE TABLE bonus_orders (
    bonus_order_id bigint NOT NULL,
    bonus_id bigint NOT NULL,
    profile_id bigint NOT NULL,
    date timestamp without time zone NOT NULL,
    state character varying(10) NOT NULL,
    delivery_type character varying(50),
    address character varying(500),
    recipient_name character varying(250),
    recipient_phone character varying(20)
)
GO;

CREATE TABLE bonuses (
    bonus_id bigint NOT NULL,
    name text NOT NULL,
    description text,
    description_html text,
    image_id bigint,
    image_url text,
    price integer DEFAULT 0 NOT NULL,
    priority integer DEFAULT 0 NOT NULL,
    available integer DEFAULT 0 NOT NULL,
    type text DEFAULT 'BONUS'::text,
    enabled boolean DEFAULT true NOT NULL,
    deleted boolean DEFAULT false NOT NULL,
    campaign_alias text,
    link character varying(500),
    link_text character varying(500),
    button_text character varying(128),
    CONSTRAINT bonuses_count_chk CHECK ((available >= 0))
)
GO;
CREATE SEQUENCE broadcast_advertising_id_seq
    START WITH -1
    INCREMENT BY -1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
GO;

CREATE TABLE broadcast_advertising (
    broadcast_id bigint DEFAULT nextval('broadcast_advertising_id_seq'::regclass) NOT NULL,
    status integer DEFAULT 0 NOT NULL,
    button_text text DEFAULT 'Подробнее'::text NOT NULL,
    campaign_id bigint,
    vast_xml text,
    date_from timestamp without time zone,
    date_to timestamp without time zone
)
GO;




CREATE TABLE cached_video (
    cached_video_id bigint NOT NULL,
    url character varying(255) NOT NULL,
    title text,
    description text,
    html text,
    duration bigint NOT NULL,
    width integer,
    height integer,
    thumbnail_url character varying(255) NOT NULL,
    time_added timestamp without time zone DEFAULT now(),
    video_type smallint NOT NULL,
    external_id character varying(100)
)
GO;



CREATE TABLE callbacks_comments (
    comment_id bigint NOT NULL,
    callback_id bigint NOT NULL,
    author_id bigint NOT NULL,
    text text NOT NULL,
    time_added timestamp without time zone DEFAULT now() NOT NULL
)
GO;



CREATE TABLE campaign_category_types (
    campaign_category_type integer,
    category_value text,
    category_name text
)
GO;



CREATE TABLE campaign_contacts (
    campaign_id bigint NOT NULL,
    email text NOT NULL,
    contact_name text,
    contact_profile_id bigint
)
GO;

CREATE TABLE campaign_curators (
    campaign_id bigint NOT NULL,
    email character varying NOT NULL
)
GO;

CREATE TABLE campaign_edit_time (
    campaign_id bigint NOT NULL,
    profile_id bigint NOT NULL,
    last_time_active timestamp without time zone DEFAULT now() NOT NULL
)
GO;

CREATE TABLE campaign_faq (
    campaign_faq_id bigint NOT NULL,
    campaign_id bigint NOT NULL,
    order_num integer NOT NULL,
    question text NOT NULL,
    answer text NOT NULL
)
GO;



CREATE TABLE campaign_median_price (
    campaign_id bigint NOT NULL,
    median_price integer
)
GO;



CREATE TABLE order_objects (
    order_id bigint NOT NULL,
    object_id bigint NOT NULL,
    merchant_id bigint NOT NULL,
    credit_transaction_id bigint DEFAULT 0 NOT NULL,
    debit_transaction_id bigint DEFAULT 0 NOT NULL,
    cancel_credit_transaction_id bigint DEFAULT 0 NOT NULL,
    cancel_debit_transaction_id bigint DEFAULT 0 NOT NULL,
    order_object_type integer NOT NULL,
    price numeric(10,2) DEFAULT 0 NOT NULL,
    ticket_purchase_count bigint,
    order_object_id bigint NOT NULL,
    comment text,
    text_for_mail text
)
GO;


CREATE TABLE orders (
    order_id bigint NOT NULL,
    buyer_id bigint NOT NULL,
    topay_transaction_id bigint DEFAULT 0 NOT NULL,
    credit_transaction_id bigint DEFAULT 0 NOT NULL,
    debit_transaction_id bigint DEFAULT 0 NOT NULL,
    cancel_credit_transaction_id bigint DEFAULT 0 NOT NULL,
    cancel_debit_transaction_id bigint DEFAULT 0 NOT NULL,
    payment_status integer NOT NULL,
    payment_type integer DEFAULT 0 NOT NULL,
    delivery_status integer NOT NULL,
    delivery_type integer NOT NULL,
    delivery_price numeric(10,2) DEFAULT 0 NOT NULL,
    delivery_service_id integer DEFAULT 0,
    time_added timestamp without time zone DEFAULT now() NOT NULL,
    time_updated timestamp without time zone DEFAULT now() NOT NULL,
    total_price numeric(10,2) DEFAULT 0 NOT NULL,
    order_type integer DEFAULT 0 NOT NULL,
    error_code text,
    reserve_time_expired timestamp without time zone,
    tracking_code character varying(50),
    cod_cost numeric(19,0),
    delivery_comment text,
    project_type integer,
    promo_code_id bigint,
    discount numeric(10,2) DEFAULT 0 NOT NULL
)
GO;





CREATE SEQUENCE seq_share_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
GO;



CREATE TABLE shares (
    share_id bigint DEFAULT nextval('seq_share_id'::regclass) NOT NULL,
    campaign_id bigint NOT NULL,
    profile_id bigint NOT NULL,
    name text NOT NULL,
    description text NOT NULL,
    image_id bigint,
    image_url text,
    price numeric(10,2) NOT NULL,
    amount integer NOT NULL,
    time_added timestamp without time zone DEFAULT now() NOT NULL,
    purchase_count integer DEFAULT 0 NOT NULL,
    purchase_sum numeric(10,2) DEFAULT 0 NOT NULL,
    description_html text,
    reward_instruction text,
    order_num integer,
    question_to_buyer text,
    name_html text,
    reward_instruction_html text,
    question_to_buyer_html text,
    share_status integer DEFAULT 0 NOT NULL,
    estimated_delivery_time timestamp without time zone
)
GO;




CREATE MATERIALIZED VIEW campaign_orders AS
 SELECT o.order_id,
    oo.order_object_id,
    s.campaign_id,
    s.share_id
   FROM ((orders o
     JOIN order_objects oo ON ((o.order_id = oo.order_id)))
     JOIN shares s ON ((oo.object_id = s.share_id)))
  WHERE (o.order_type = 1)
  WITH NO DATA
GO;



CREATE TABLE campaign_permissions (
    campaign_id bigint NOT NULL,
    view_level integer NOT NULL,
    write_level integer NOT NULL,
    change_status_level integer NOT NULL,
    manager_id bigint NOT NULL,
    time_added timestamp without time zone
)
GO;


CREATE VIEW campaign_stats_affiliate AS
 SELECT a.date,
    a.campaign_id,
    a.affiliate_id,
    count(DISTINCT a.visitor_id) AS visitors,
    sum(a.views) AS views,
    sum(a.purchases) AS purchases,
    sum(a.affiliate_amount) AS affiliate_amount
   FROM ( SELECT (date_trunc('DAY'::text, stat_events.date))::date AS date,
            stat_events.object_id AS campaign_id,
            stat_events.affiliate_id,
                CASE stat_events.type
                    WHEN 'CAMPAIGN_VIEW'::text THEN stat_events.visitor_id
                    ELSE NULL::bigint
                END AS visitor_id,
            stat_events.affiliate_amount,
                CASE stat_events.type
                    WHEN 'CAMPAIGN_VIEW'::text THEN 1
                    ELSE 0
                END AS views,
                CASE stat_events.type
                    WHEN 'CAMPAIGN_PURCHASE'::text THEN 1
                    ELSE 0
                END AS purchases
           FROM stat_events) a
  GROUP BY a.date, a.campaign_id, a.affiliate_id
GO;



CREATE VIEW campaign_stats_city AS
 SELECT c.date,
    c.campaign_id,
    c.city,
    c.visitors,
    c.views,
    COALESCE(b.purchases, (0)::bigint) AS purchases,
    (0)::numeric AS affiliate_amount
   FROM (( SELECT a.date,
            a.campaign_id,
            a.city,
            count(DISTINCT a.visitor_id) AS visitors,
            sum(a.views) AS views,
            sum(a.affiliate_amount) AS affiliate_amount
           FROM ( SELECT (date_trunc('DAY'::text, stat_events.date))::date AS date,
                    stat_events.object_id AS campaign_id,
                    stat_events.city,
                        CASE stat_events.type
                            WHEN 'CAMPAIGN_VIEW'::text THEN stat_events.visitor_id
                            ELSE NULL::bigint
                        END AS visitor_id,
                    stat_events.affiliate_amount,
                        CASE stat_events.type
                            WHEN 'CAMPAIGN_VIEW'::text THEN 1
                            ELSE 0
                        END AS views
                   FROM stat_events) a
          GROUP BY a.date, a.campaign_id, a.city) c
     LEFT JOIN ( SELECT s.campaign_id,
            pc.city_name_ru AS city,
            count(ogs.order_id) AS purchases
           FROM ((((shares s
             JOIN order_objects oo ON ((oo.object_id = s.share_id)))
             JOIN orders o ON ((o.order_id = oo.order_id)))
             JOIN statdb.order_geo_stats ogs ON ((ogs.order_id = o.order_id)))
             LEFT JOIN profiledb.city pc ON ((pc.city_id = ogs.city_id)))
          WHERE ((((o.time_added >= date_trunc('day'::text, (now() - '1 day'::interval))) AND (o.time_added <= date_trunc('day'::text, now()))) AND (o.payment_status = ANY (ARRAY[1, 2]))) AND (o.order_type = 1))
          GROUP BY s.campaign_id, pc.city_name_ru) b ON (((c.campaign_id = b.campaign_id) AND ((c.city)::text = (b.city)::text))))
GO;


CREATE VIEW campaign_stats_general AS
 SELECT a.date,
    a.campaign_id,
    count(DISTINCT a.visitor_id) AS visitors,
    sum(a.views) AS views,
    sum(a.purchases) AS purchases,
    count(DISTINCT a.buyers) AS buyers,
    sum(a.amount) AS amount,
    sum(a.affiliate_amount) AS affiliate_amount,
    sum(a.comments) AS comments
   FROM ( SELECT (date_trunc('DAY'::text, e.date))::date AS date,
            e.object_id AS campaign_id,
                CASE e.type
                    WHEN 'CAMPAIGN_VIEW'::text THEN e.visitor_id
                    ELSE NULL::bigint
                END AS visitor_id,
            e.amount,
            e.affiliate_amount,
                CASE e.type
                    WHEN 'CAMPAIGN_PURCHASE'::text THEN e.visitor_id
                    ELSE NULL::bigint
                END AS buyers,
                CASE e.type
                    WHEN 'CAMPAIGN_VIEW'::text THEN 1
                    ELSE 0
                END AS views,
                CASE e.type
                    WHEN 'CAMPAIGN_PURCHASE'::text THEN 1
                    ELSE 0
                END AS purchases,
                CASE e.type
                    WHEN 'CAMPAIGN_COMMENT'::text THEN 1
                    ELSE 0
                END AS comments
           FROM stat_events e) a
  GROUP BY a.date, a.campaign_id
GO;



CREATE VIEW campaign_stats_ref AS
 SELECT a.date,
    a.campaign_id,
    a.referer,
    count(DISTINCT a.visitor_id) AS visitors,
    sum(a.views) AS views,
    sum(a.purchases) AS purchases,
    sum(a.affiliate_amount) AS affiliate_amount
   FROM ( SELECT (date_trunc('DAY'::text, stat_events.date))::date AS date,
            stat_events.object_id AS campaign_id,
            stat_events.referer,
                CASE stat_events.type
                    WHEN 'CAMPAIGN_VIEW'::text THEN stat_events.visitor_id
                    ELSE NULL::bigint
                END AS visitor_id,
            stat_events.affiliate_amount,
                CASE stat_events.type
                    WHEN 'CAMPAIGN_VIEW'::text THEN 1
                    ELSE 0
                END AS views,
                CASE stat_events.type
                    WHEN 'CAMPAIGN_PURCHASE'::text THEN 1
                    ELSE 0
                END AS purchases
           FROM stat_events) a
  GROUP BY a.date, a.campaign_id, a.referer
GO;



CREATE TABLE campaign_subscribe (
    profile_id bigint NOT NULL,
    campaign_id bigint NOT NULL
)
GO;



CREATE TABLE campaign_tag_relations (
    campaign_id bigint NOT NULL,
    tag_id bigint NOT NULL,
    order_num integer DEFAULT 0 NOT NULL
)
GO;



CREATE TABLE campaign_tags (
    tag_id bigint NOT NULL,
    tag_name text NOT NULL,
    mnemonic_name text NOT NULL,
    preferred_order integer DEFAULT 0 NOT NULL,
    sponsor_alias character varying(50),
    warn_text character varying(255),
    tag_visibility boolean DEFAULT true,
    eng_tag_name text DEFAULT ''::text NOT NULL,
    visibile_in_search boolean DEFAULT true,
    charity_visibility boolean DEFAULT false NOT NULL,
    editor_visibility boolean DEFAULT true NOT NULL,
    special_project boolean DEFAULT false NOT NULL
)
GO;



CREATE TABLE campaign_targeting (
    campaign_id bigint NOT NULL,
    vk_targeting_id text,
    fb_targeting_id text
)
GO;

CREATE TABLE campaigns (
    campaign_id bigint NOT NULL,
    profile_id bigint NOT NULL,
    name text NOT NULL,
    description text NOT NULL,
    description_html text NOT NULL,
    short_description text,
    image_id bigint,
    image_url text,
    time_start timestamp without time zone,
    time_finish timestamp without time zone,
    target_amount numeric(16,2) DEFAULT 0 NOT NULL,
    ignore_target_amount boolean,
    notification_method integer DEFAULT 0 NOT NULL,
    status integer NOT NULL,
    time_added timestamp without time zone DEFAULT now() NOT NULL,
    time_updated timestamp without time zone DEFAULT now() NOT NULL,
    album_id bigint,
    purchase_sum numeric(16,2) DEFAULT 0 NOT NULL,
    purchase_count integer DEFAULT 0 NOT NULL,
    target_status integer NOT NULL,
    finish_on_target_reach boolean,
    view_image_id bigint,
    view_image_url text,
    video_id bigint,
    video_url text,
    video_profile_id bigint,
    time_last_purchased timestamp without time zone DEFAULT now() NOT NULL,
    collected_amount numeric(16,2) DEFAULT 0 NOT NULL,
    affiliate_program_enabled boolean DEFAULT false NOT NULL,
    time_last_news_published timestamp without time zone,
    short_description_html text,
    name_html text,
    can_make_vip_offer boolean DEFAULT false NOT NULL,
    planeta_delivery boolean DEFAULT true NOT NULL,
    creator_profile_id bigint NOT NULL,
    campaign_alias text,
    sponsored boolean DEFAULT false NOT NULL,
    sponsor_alias character varying(50),
    meta_data character varying(1024),
    background_url text,
    draft_visible boolean DEFAULT false,
    updater_profile_id bigint NOT NULL,
    country_id bigint,
    region_id bigint,
    city_id bigint,
    vk_targeting_code text,
    fb_targeting_code text
)
GO;





CREATE VIEW cb_orders_view AS
 SELECT DISTINCT o.order_id,
    o.buyer_id,
    o.total_price,
    s.campaign_id,
    o.topay_transaction_id,
    o.time_added
   FROM ((orders o
     JOIN order_objects oo ON ((o.order_id = oo.order_id)))
     JOIN shares s ON ((s.share_id = oo.object_id)))
  WHERE ((o.order_type = 1) AND (o.payment_status = ANY (ARRAY[1, 2])))
GO;



CREATE TABLE topay_transactions (
    transaction_id bigint NOT NULL,
    profile_id bigint NOT NULL,
    amount_net numeric(10,2) NOT NULL,
    amount_fee numeric(10,2) NOT NULL,
    status integer NOT NULL,
    debit_transaction_id bigint DEFAULT 0 NOT NULL,
    time_added timestamp without time zone DEFAULT now() NOT NULL,
    time_updated timestamp without time zone,
    payment_system_id integer,
    param_1 text,
    param_2 text,
    project_id integer DEFAULT 1 NOT NULL,
    order_id bigint DEFAULT 0 NOT NULL,
    payment_type_id integer,
    ext_transaction_id character varying(50),
    payment_tool_code character varying(20),
    payment_method_id bigint,
    payment_provider_id bigint,
    ext_error_code integer,
    ext_error_message character varying(500),
    ext_base_transaction_id character varying(50),
    expire_date timestamp without time zone,
    card_binding_id bigint,
    tls_supported boolean DEFAULT true
)
GO;



CREATE VIEW cb_transactions_view AS
 SELECT DISTINCT t.transaction_id,
    t.profile_id,
    t.time_added
   FROM (topay_transactions t
     JOIN orders o ON ((((t.transaction_id = o.topay_transaction_id) AND (o.order_type = 1)) AND (t.status = 2))))
GO;



CREATE VIEW cone_books AS
 SELECT b.book_id,
    b.book_title,
    b.book_price,
    ph.contractor_id
   FROM (bibliodb.book b
     LEFT JOIN bibliodb.publishing_house ph ON ((b.book_publishing_house_id = ph.publishing_house_id)))
GO;



CREATE TABLE debit_transactions (
    transaction_id bigint NOT NULL,
    profile_id bigint NOT NULL,
    amount_net numeric(10,2) NOT NULL,
    amount_fee numeric(10,2) NOT NULL,
    comment text,
    time_added timestamp without time zone DEFAULT now() NOT NULL,
    payment_system_id integer DEFAULT 0 NOT NULL,
    external_system_data text,
    payment_provider_id bigint
)
GO;





CREATE TABLE investing_order_info (
    investing_order_info_id bigint NOT NULL,
    time_added timestamp without time zone DEFAULT now() NOT NULL,
    user_type integer,
    phone_number text,
    last_name text,
    first_name text,
    middle_name text,
    boss_position text,
    gender integer,
    org_name text,
    org_brand text,
    birth_date timestamp without time zone,
    birth_place text,
    reg_index text,
    reg_country_id bigint,
    reg_region text,
    reg_location text,
    reg_address text,
    live_index text,
    live_country_id bigint,
    live_region text,
    live_location text,
    live_address text,
    passport_number text,
    passport_issuer text,
    passport_issue_date timestamp without time zone,
    passport_issuer_code text,
    inn text,
    web_site text,
    moderate_status integer
)
GO;



CREATE VIEW cone_books_purchases AS
 SELECT debit_transactions.transaction_id,
    t.user_id,
    book.book_id,
    tbookcount.book_count,
    tlibcount.lib_count AS library_count,
    t.price,
    'RUR'::character varying(3) AS currency,
    debit_transactions.amount_net AS amount,
    t.direction,
    debit_transactions.time_added AS creation_time,
        CASE oi.user_type
            WHEN 0 THEN 'USER'::text
            WHEN 1 THEN 'ORGANIZATION'::text
            ELSE NULL::text
        END AS user_type,
    oi.inn,
    oi.passport_number
   FROM (((((( SELECT DISTINCT orders.order_id,
            orders.debit_transaction_id AS transaction_id,
            orders.buyer_id AS user_id,
            order_objects.object_id AS book_id,
            order_objects.price,
            'IN'::text AS direction
           FROM (orders
             LEFT JOIN order_objects ON ((orders.order_id = order_objects.order_id)))
          WHERE (((orders.order_type = 11) AND (order_objects.order_object_type = 12)) AND (orders.payment_status = ANY (ARRAY[1, 2])))
        UNION
         SELECT DISTINCT orders.order_id,
            orders.cancel_debit_transaction_id AS transaction_id,
            orders.buyer_id AS user_id,
            order_objects.object_id AS book_id,
            order_objects.price,
            'OUT'::text AS direction
           FROM (orders
             LEFT JOIN order_objects ON ((orders.order_id = order_objects.order_id)))
          WHERE ((orders.order_type = 11) AND (orders.payment_status = 2))) t
     JOIN ( SELECT order_objects.order_id,
            order_objects.object_id,
            count(DISTINCT order_objects.order_object_id) AS book_count
           FROM order_objects
          WHERE (order_objects.order_object_type = 12)
          GROUP BY order_objects.order_id, order_objects.object_id) tbookcount ON (((t.order_id = tbookcount.order_id) AND (t.book_id = tbookcount.object_id))))
     JOIN ( SELECT order_objects.order_id,
            count(DISTINCT order_objects.object_id) AS lib_count
           FROM order_objects
          WHERE (order_objects.order_object_type = 13)
          GROUP BY order_objects.order_id) tlibcount ON ((t.order_id = tlibcount.order_id)))
     LEFT JOIN debit_transactions ON ((t.transaction_id = debit_transactions.transaction_id)))
     LEFT JOIN bibliodb.book ON ((t.book_id = book.book_id)))
     LEFT JOIN investing_order_info oi ON ((t.order_id = oi.investing_order_info_id)))
GO;



CREATE VIEW cone_campaign_purchases AS
 SELECT debit_transactions.transaction_id,
    t.user_id,
    shares.campaign_id,
    'RUR'::character varying(3) AS currency,
    debit_transactions.amount_net AS amount,
    t.direction,
    debit_transactions.time_added AS creation_time,
        CASE oi.user_type
            WHEN 0 THEN 'USER'::text
            WHEN 1 THEN 'ORGANIZATION'::text
            ELSE NULL::text
        END AS user_type,
    oi.inn,
    oi.passport_number
   FROM (((( SELECT DISTINCT orders.order_id,
            orders.debit_transaction_id AS transaction_id,
            orders.buyer_id AS user_id,
            order_objects.object_id AS share_id,
            'IN'::text AS direction
           FROM (orders
             LEFT JOIN order_objects ON ((orders.order_id = order_objects.order_id)))
          WHERE ((orders.order_type = ANY (ARRAY[1, 9, 10, 14])) AND (orders.payment_status = ANY (ARRAY[1, 2])))
        UNION
         SELECT DISTINCT orders.order_id,
            orders.cancel_debit_transaction_id AS transaction_id,
            orders.buyer_id AS user_id,
            order_objects.object_id AS share_id,
            'OUT'::text AS direction
           FROM (orders
             LEFT JOIN order_objects ON ((orders.order_id = order_objects.order_id)))
          WHERE ((orders.order_type = ANY (ARRAY[1, 9, 10, 14])) AND (orders.payment_status = 2))) t
     LEFT JOIN debit_transactions ON ((t.transaction_id = debit_transactions.transaction_id)))
     LEFT JOIN shares ON ((t.share_id = shares.share_id)))
     LEFT JOIN investing_order_info oi ON ((t.order_id = oi.investing_order_info_id)))
GO;



CREATE VIEW cone_campaign_purchases_investing_bak AS
 SELECT debit_transactions.transaction_id,
    t.user_id,
    shares.campaign_id,
    'RUR'::character varying(3) AS currency,
    debit_transactions.amount_net AS amount,
    t.direction,
    debit_transactions.time_added AS creation_time
   FROM ((( SELECT DISTINCT orders.order_id,
            orders.debit_transaction_id AS transaction_id,
            orders.buyer_id AS user_id,
            order_objects.object_id AS share_id,
            'IN'::text AS direction
           FROM (orders
             LEFT JOIN order_objects ON ((orders.order_id = order_objects.order_id)))
          WHERE ((orders.order_type = 1) AND (orders.payment_status = ANY (ARRAY[1, 2])))
        UNION
         SELECT DISTINCT orders.order_id,
            orders.cancel_debit_transaction_id AS transaction_id,
            orders.buyer_id AS user_id,
            order_objects.object_id AS share_id,
            'OUT'::text AS direction
           FROM (orders
             LEFT JOIN order_objects ON ((orders.order_id = order_objects.order_id)))
          WHERE ((orders.order_type = 1) AND (orders.payment_status = 2))) t
     LEFT JOIN debit_transactions ON ((t.transaction_id = debit_transactions.transaction_id)))
     LEFT JOIN shares ON ((t.share_id = shares.share_id)))
GO;



CREATE TABLE contractor_relations (
    contractor_id bigint NOT NULL,
    campaign_id bigint NOT NULL
)
GO;



CREATE VIEW cone_campaigns AS
 SELECT campaigns.campaign_id,
    contractor_relations.contractor_id,
    campaigns.name,
    campaigns.time_start,
    campaigns.time_finish,
    campaigns.creator_profile_id
   FROM (campaigns
     LEFT JOIN contractor_relations ON ((campaigns.campaign_id = contractor_relations.campaign_id)))
  WHERE ((campaigns.status = ANY (ARRAY[3, 4, 5, 7])) OR (campaigns.campaign_id = ANY (ARRAY[(18)::bigint, (184)::bigint, (1302)::bigint, (1945)::bigint, (1576)::bigint])))
GO;



CREATE VIEW cone_concert_purchases AS
 SELECT debit_transactions.transaction_id,
    t.user_id,
    shares.campaign_id AS event_id,
    'RUR'::character varying(3) AS currency,
    debit_transactions.amount_net AS amount,
    t.direction,
    debit_transactions.time_added AS creation_time
   FROM ((( SELECT DISTINCT orders.order_id,
            orders.debit_transaction_id AS transaction_id,
            orders.buyer_id AS user_id,
            order_objects.object_id AS share_id,
            'IN'::text AS direction
           FROM (orders
             LEFT JOIN order_objects ON ((orders.order_id = order_objects.order_id)))
          WHERE ((orders.order_type = 5) AND (orders.payment_status = ANY (ARRAY[1, 2])))
        UNION
         SELECT DISTINCT orders.order_id,
            orders.cancel_debit_transaction_id AS transaction_id,
            orders.buyer_id AS user_id,
            order_objects.object_id AS share_id,
            'OUT'::text AS direction
           FROM (orders
             LEFT JOIN order_objects ON ((orders.order_id = order_objects.order_id)))
          WHERE ((orders.order_type = 5) AND (orders.payment_status = 2))) t
     LEFT JOIN debit_transactions ON ((t.transaction_id = debit_transactions.transaction_id)))
     LEFT JOIN shares ON ((t.share_id = shares.share_id)))
GO;



CREATE TABLE contractors (
    contractor_id bigint NOT NULL,
    name text NOT NULL,
    contract_number text,
    country_id bigint NOT NULL,
    type integer NOT NULL,
    inn text,
    time_added timestamp without time zone,
    time_updated timestamp without time zone,
    passport_number text,
    other_details text,
    phone text,
    city_id bigint,
    ogrn text,
    legal_address text,
    actual_address text,
    authority text,
    issue_date timestamp without time zone,
    checking_account text,
    beneficiary_bank text,
    corresponding_account text,
    bic text,
    unit text,
    birth_date timestamp(0) without time zone DEFAULT NULL::timestamp without time zone,
    kpp text,
    responsible_person text,
    scan_passport_url text,
    initials_with_last_name text,
    "position" integer DEFAULT 0 NOT NULL,
    custom_position text
)
GO;

CREATE VIEW cone_contractors AS
 SELECT contractors.contractor_id,
    contractors.name,
    country.country_name_ru AS country,
        CASE contractors.type
            WHEN 0 THEN 'ФЛ'::text
            WHEN 1 THEN 'ЮЛ'::text
            WHEN 2 THEN 'ИП'::text
            WHEN 3 THEN 'ООО'::text
            WHEN 4 THEN 'БФ'::text
            ELSE NULL::text
        END AS type,
    contractors.inn,
    contractors.passport_number
   FROM (contractors
     LEFT JOIN profiledb.country ON ((contractors.country_id = country.country_id)))
GO;



CREATE TABLE users_private_info (
    user_id bigint NOT NULL,
    email character varying(64),
    status integer DEFAULT 1 NOT NULL,
    password character varying(64) NOT NULL,
    reg_code character varying(64),
    time_added timestamp without time zone DEFAULT now() NOT NULL,
    time_updated timestamp without time zone DEFAULT now() NOT NULL,
    username character varying(64) DEFAULT ''::character varying NOT NULL
)
GO;



CREATE VIEW cone_delivery_payments AS
 SELECT o.topay_transaction_id AS transaction_id,
    o.buyer_id,
    o.total_price,
    o.time_updated AS processing_time
   FROM (orders o
     LEFT JOIN users_private_info up ON ((o.buyer_id = up.user_id)))
  WHERE ((o.order_type = 8) AND (o.payment_status = 1))
GO;



CREATE VIEW cone_investors AS
 SELECT o.topay_transaction_id,
    o.debit_transaction_id AS transaction_id,
        CASE oi.user_type
            WHEN 0 THEN 'USER'::text
            WHEN 1 THEN 'ORGANIZATION'::text
            ELSE NULL::text
        END AS user_type,
    oi.last_name,
    oi.first_name,
    oi.middle_name,
    oi.inn,
    oi.org_name,
    oi.org_brand,
    oi.birth_date,
    oi.birth_place,
    c.country_name_ru AS reg_country,
    oi.reg_region,
    oi.reg_location,
    oi.reg_address,
    oi.reg_index,
    oi.passport_number
   FROM ((investing_order_info oi
     JOIN orders o ON ((o.order_id = oi.investing_order_info_id)))
     LEFT JOIN profiledb.country c ON ((oi.reg_country_id = c.country_id)))
  ORDER BY o.time_added DESC
GO;



CREATE TABLE payment_providers (
    id bigint NOT NULL,
    type character varying(20) NOT NULL,
    name character varying(50) NOT NULL,
    url character varying(100),
    emulated boolean DEFAULT false NOT NULL,
    internal boolean DEFAULT false NOT NULL,
    processor_class character varying(255) NOT NULL,
    processor_params character varying(1000),
    processor_params_for_test text
)
GO;



CREATE VIEW cone_payment_systems AS
 SELECT payment_providers.id AS payment_systems_id,
    payment_providers.type AS name
   FROM payment_providers
GO;



CREATE VIEW cone_payments AS
 SELECT tp.debit_transaction_id AS transaction_id,
    tp.profile_id AS user_id,
    'RUR'::character varying(3) AS currency,
    tp.amount_net AS amount,
    tp.amount_fee,
    tp.payment_provider_id AS payment_system_id,
    tp.time_updated AS processing_time,
    tp.transaction_id AS topay_transaction_id,
        CASE oi.user_type
            WHEN 0 THEN 'USER'::text
            WHEN 1 THEN 'ORGANIZATION'::text
            ELSE NULL::text
        END AS user_type,
    oi.inn,
    oi.passport_number
   FROM ((topay_transactions tp
     LEFT JOIN orders o ON ((o.order_id = tp.order_id)))
     LEFT JOIN investing_order_info oi ON ((oi.investing_order_info_id = o.order_id)))
  WHERE ((tp.status = 2) AND (((tp.payment_provider_id > 0) OR ((tp.payment_provider_id = 0) AND (tp.payment_method_id = 11))) OR (tp.payment_system_id = (-1))))
GO;



CREATE VIEW cone_payments_investing_bak AS
 SELECT tp.debit_transaction_id AS transaction_id,
    tp.profile_id AS user_id,
    'RUR'::character varying(3) AS currency,
    tp.amount_net AS amount,
    tp.amount_fee,
    tp.payment_provider_id AS payment_system_id,
    tp.time_updated AS processing_time
   FROM topay_transactions tp
  WHERE ((tp.status = 2) AND (((tp.payment_provider_id > 0) OR ((tp.payment_provider_id = 0) AND (tp.payment_method_id = 11))) OR (tp.payment_system_id = (-1))))
GO;



CREATE TABLE credit_transactions (
    transaction_id bigint NOT NULL,
    profile_id bigint NOT NULL,
    amount_net numeric(10,2) NOT NULL,
    amount_fee numeric(10,2) NOT NULL,
    comment text,
    time_added timestamp without time zone DEFAULT now() NOT NULL
)
GO;





CREATE VIEW cone_payments_return AS
 SELECT credit_transactions.transaction_id,
    credit_transactions.profile_id AS user_id,
    'RUR'::character varying(3) AS currency,
    credit_transactions.amount_net AS amount,
    credit_transactions.time_added AS creation_time
   FROM credit_transactions
  WHERE (credit_transactions.comment = 'money returning'::text)
GO;



CREATE VIEW cone_product_purchases AS
 SELECT DISTINCT debit_transactions.transaction_id,
    t.user_id,
    t.order_id,
    'RUR'::character varying(3) AS currency,
    debit_transactions.amount_net AS amount,
    (
        CASE t.payment_type
            WHEN 1 THEN 'CASH'::text
            ELSE 'CASHLESS'::text
        END)::character varying(10) AS payment_type,
    t.direction,
    debit_transactions.time_added AS creation_time
   FROM ((( SELECT DISTINCT orders.order_id,
            orders.debit_transaction_id AS transaction_id,
            orders.buyer_id AS user_id,
            orders.payment_type,
            order_objects.object_id AS product_id,
            'IN'::text AS direction
           FROM (orders
             LEFT JOIN order_objects ON ((orders.order_id = order_objects.order_id)))
          WHERE ((orders.order_type = 2) AND (orders.payment_status = ANY (ARRAY[1, 2])))
        UNION
         SELECT DISTINCT orders.order_id,
            orders.cancel_debit_transaction_id AS transaction_id,
            orders.buyer_id AS user_id,
            orders.payment_type,
            order_objects.object_id AS product_id,
            'OUT'::text AS direction
           FROM (orders
             LEFT JOIN order_objects ON ((orders.order_id = order_objects.order_id)))
          WHERE ((orders.order_type = 2) AND (orders.payment_status = 2))) t
     LEFT JOIN debit_transactions ON ((t.transaction_id = debit_transactions.transaction_id)))
     LEFT JOIN shopdb.products ON ((t.product_id = products.product_id)))
  WHERE (t.transaction_id > 0)
GO;



CREATE TABLE promo_codes (
    code character varying(256) NOT NULL,
    email character varying(256),
    time_added timestamp without time zone,
    confirmcode text,
    amount numeric(10,2) DEFAULT 0 NOT NULL,
    comment character varying(256) DEFAULT 'PROMO'::character varying NOT NULL
)
GO;



CREATE TABLE promo_codes_2013 (
    code character varying(256) NOT NULL,
    email character varying(256),
    time_added timestamp without time zone,
    confirmcode text
)
GO;



CREATE VIEW cone_promo_payments AS
 SELECT up.user_id,
    t1.processing_time,
    t1.currency,
    t1.amount
   FROM (( SELECT p2014.email,
            p2014.time_added AS processing_time,
            'RUR'::text AS currency,
            p2014.amount
           FROM promo_codes p2014
          WHERE (p2014.email IS NOT NULL)
        UNION ALL
         SELECT p2013.email,
            p2013.time_added AS processing_time,
            'RUR'::text AS currency,
            500 AS amount
           FROM promo_codes_2013 p2013
          WHERE (p2013.email IS NOT NULL)) t1
     JOIN users_private_info up ON (((t1.email)::text ~~ (up.email)::text)))
GO;



CREATE VIEW cone_shop_orders AS
 SELECT o.order_id,
    o.time_added,
    o.time_updated,
    p.name AS product_name,
        CASE p.product_category
            WHEN 1 THEN 'Товар цифровой'::text
            WHEN 2 THEN 'Товар'::text
            WHEN 3 THEN 'Поддержка'::text
            ELSE NULL::text
        END AS product_category,
    oo.price AS product_price,
    1 AS product_amount,
    pp.profile_id,
    pp.display_name,
    up.email,
    o.total_price AS order_price,
        CASE o.payment_status
            WHEN 0 THEN 'Не оплачен'::text
            WHEN 1 THEN 'Оплачен'::text
            WHEN 2 THEN 'Аннулирован'::text
            ELSE 'ERROR'::text
        END AS payment_status,
        CASE o.delivery_status
            WHEN 0 THEN 'Новый заказ'::text
            WHEN 1 THEN 'Отправлен'::text
            WHEN 2 THEN 'Доставлен'::text
            WHEN 3 THEN 'Возврат'::text
            WHEN 4 THEN 'Отменен'::text
            WHEN 5 THEN 'Выдан'::text
            ELSE 'ERROR'::text
        END AS delivery_status,
    bds.name AS delivery_name,
    o.delivery_price
   FROM (((((orders o
     LEFT JOIN order_objects oo ON ((o.order_id = oo.order_id)))
     LEFT JOIN shopdb.products p ON ((p.product_id = oo.object_id)))
     JOIN users_private_info up ON ((up.user_id = o.buyer_id)))
     JOIN profiledb.profiles pp ON ((pp.profile_id = o.buyer_id)))
     LEFT JOIN base_delivery_service bds ON ((bds.service_id = o.delivery_service_id)))
  WHERE ((o.order_type = 2) AND ((o.payment_type = 1) OR ((o.payment_type = 2) AND (o.payment_status = ANY (ARRAY[1, 2])))))
GO;



CREATE VIEW cone_ticket_purchases AS
 SELECT DISTINCT debit_transactions.transaction_id,
    t.user_id,
    'RUR'::text AS currency,
    debit_transactions.amount_net AS amount,
    t.direction,
    debit_transactions.time_added AS creation_time
   FROM ((( SELECT DISTINCT orders.order_id,
            orders.debit_transaction_id AS transaction_id,
            orders.buyer_id AS user_id,
            order_objects.object_id AS ticket_id,
            'IN'::text AS direction
           FROM (orders
             LEFT JOIN order_objects ON ((orders.order_id = order_objects.order_id)))
          WHERE ((orders.order_type = 6) AND (orders.payment_status = ANY (ARRAY[1, 2])))
        UNION
         SELECT DISTINCT orders.order_id,
            orders.cancel_debit_transaction_id AS transaction_id,
            orders.buyer_id AS user_id,
            order_objects.object_id AS ticket_id,
            'OUT'::text AS direction
           FROM (orders
             LEFT JOIN order_objects ON ((orders.order_id = order_objects.order_id)))
          WHERE ((orders.order_type = 6) AND (orders.payment_status = 2))) t
     LEFT JOIN debit_transactions ON ((t.transaction_id = debit_transactions.transaction_id)))
     LEFT JOIN concertdb.tickets tickets ON ((t.ticket_id = tickets.ticket_id)))
GO;



CREATE VIEW cone_transactions AS
 SELECT t1.transaction_id,
    t1.user_id,
    t1.amount,
    t1.time_added,
    t1.order_id,
        CASE oi.user_type
            WHEN 0 THEN 'USER'::text
            WHEN 1 THEN 'ORGANIZATION'::text
            ELSE NULL::text
        END AS user_type,
    oi.inn,
    oi.passport_number
   FROM (( SELECT DISTINCT d.transaction_id,
            d.profile_id AS user_id,
            (d.amount_net - d.amount_fee) AS amount,
            d.time_added,
            o.order_id
           FROM ((debit_transactions d
             LEFT JOIN topay_transactions t ON ((d.transaction_id = t.debit_transaction_id)))
             LEFT JOIN orders o ON ((o.topay_transaction_id = t.transaction_id)))
          WHERE (d.profile_id > 0)
        UNION
         SELECT DISTINCT c.transaction_id,
            c.profile_id AS user_id,
            (- (c.amount_net - c.amount_fee)) AS amount,
            c.time_added,
            NULL::bigint AS order_id
           FROM (credit_transactions c
             LEFT JOIN orders o ON ((o.cancel_credit_transaction_id = c.transaction_id)))
          WHERE (c.profile_id > 0)) t1
     LEFT JOIN investing_order_info oi ON ((oi.investing_order_info_id = t1.order_id)))
GO;



CREATE VIEW cone_transactions2 AS
 SELECT t1.transaction_id,
    t1.user_id,
    t1.amount,
    t1.time_added,
    t1.order_id,
        CASE oi.user_type
            WHEN 0 THEN 'USER'::text
            WHEN 1 THEN 'ORGANIZATION'::text
            ELSE NULL::text
        END AS user_type,
    oi.inn,
    oi.passport_number
   FROM (( SELECT DISTINCT d.transaction_id,
            d.profile_id AS user_id,
            (d.amount_net - d.amount_fee) AS amount,
            d.time_added,
            o.order_id
           FROM ((debit_transactions d
             LEFT JOIN topay_transactions t ON ((d.transaction_id = t.debit_transaction_id)))
             LEFT JOIN orders o ON ((o.topay_transaction_id = t.transaction_id)))
          WHERE (d.profile_id > 0)
        UNION
         SELECT DISTINCT c.transaction_id,
            c.profile_id AS user_id,
            (- (c.amount_net - c.amount_fee)) AS amount,
            c.time_added,
            o.order_id
           FROM (credit_transactions c
             LEFT JOIN orders o ON ((o.cancel_credit_transaction_id = c.transaction_id)))
          WHERE (c.profile_id > 0)) t1
     LEFT JOIN investing_order_info oi ON ((oi.investing_order_info_id = t1.order_id)))
GO;



CREATE VIEW cone_transactions3 AS
 SELECT t1.transaction_id,
    t1.user_id,
    t1.amount,
    t1.time_added,
    t1.order_id,
        CASE oi.user_type
            WHEN 0 THEN 'USER'::text
            WHEN 1 THEN 'ORGANIZATION'::text
            ELSE NULL::text
        END AS user_type,
    oi.inn,
    oi.passport_number
   FROM (( SELECT DISTINCT d.transaction_id,
            d.profile_id AS user_id,
            (d.amount_net - d.amount_fee) AS amount,
            d.time_added,
            o.order_id
           FROM ((debit_transactions d
             LEFT JOIN topay_transactions t ON ((d.transaction_id = t.debit_transaction_id)))
             LEFT JOIN orders o ON ((o.topay_transaction_id = t.transaction_id)))
          WHERE (d.profile_id > 0)
        UNION
         SELECT DISTINCT c.transaction_id,
            c.profile_id AS user_id,
            (- (c.amount_net - c.amount_fee)) AS amount,
            c.time_added,
            NULL::bigint AS order_id
           FROM credit_transactions c
          WHERE (c.profile_id > 0)) t1
     LEFT JOIN investing_order_info oi ON ((oi.investing_order_info_id = t1.order_id)))
GO;



CREATE VIEW cone_transactions_bak AS
 SELECT t1.transaction_id,
    t1.user_id,
    t1.amount,
    t1.time_added,
        CASE oi.user_type
            WHEN 0 THEN 'USER'::text
            WHEN 1 THEN 'ORGANIZATION'::text
            ELSE NULL::text
        END AS user_type,
    oi.inn,
    oi.passport_number
   FROM (( SELECT d.transaction_id,
            d.profile_id AS user_id,
            (d.amount_net - d.amount_fee) AS amount,
            d.time_added,
            o.order_id
           FROM ((debit_transactions d
             JOIN topay_transactions t ON ((t.debit_transaction_id = d.transaction_id)))
             LEFT JOIN orders o ON ((t.order_id = o.order_id)))
          WHERE (d.profile_id > 0)
        UNION
         SELECT d.transaction_id,
            d.profile_id AS user_id,
            (d.amount_net - d.amount_fee) AS amount,
            d.time_added,
            o.order_id
           FROM ((debit_transactions d
             JOIN orders o ON ((o.cancel_debit_transaction_id = d.transaction_id)))
             LEFT JOIN investing_order_info oi_1 ON ((oi_1.investing_order_info_id = o.order_id)))
          WHERE (d.profile_id > 0)
        UNION
         SELECT c.transaction_id,
            c.profile_id AS user_id,
            (- (c.amount_net - c.amount_fee)) AS amount,
            c.time_added,
            o.order_id
           FROM ((credit_transactions c
             LEFT JOIN orders o ON ((o.credit_transaction_id = c.transaction_id)))
             LEFT JOIN investing_order_info oi_1 ON ((oi_1.investing_order_info_id = o.order_id)))
          WHERE (c.profile_id > 0)) t1
     LEFT JOIN investing_order_info oi ON ((oi.investing_order_info_id = t1.order_id)))
GO;



CREATE VIEW cone_transactions_investing_bak AS
 SELECT t1.transaction_id,
    t1.user_id,
    t1.amount,
    t1.time_added
   FROM ( SELECT credit_transactions.transaction_id,
            credit_transactions.profile_id AS user_id,
            (- (credit_transactions.amount_net - credit_transactions.amount_fee)) AS amount,
            credit_transactions.time_added
           FROM credit_transactions
          WHERE (credit_transactions.profile_id > 0)
        UNION
         SELECT debit_transactions.transaction_id,
            debit_transactions.profile_id AS user_id,
            (debit_transactions.amount_net - debit_transactions.amount_fee) AS amount,
            debit_transactions.time_added
           FROM debit_transactions
          WHERE (debit_transactions.profile_id > 0)) t1
  ORDER BY t1.time_added
GO;



CREATE TABLE profile_balances (
    profile_id bigint NOT NULL,
    balance numeric(16,4) DEFAULT 0 NOT NULL,
    frozen_amount numeric(16,4) DEFAULT 0
)
GO;

CREATE TABLE config (
    appid character varying NOT NULL,
    ip character varying(15) NOT NULL,
    port integer NOT NULL,
    machid character varying NOT NULL,
    key character varying NOT NULL,
    value character varying
)
GO;



CREATE TABLE configuration (
    key text NOT NULL,
    int_value integer,
    str_value text,
    boolean_value boolean,
    description text
)
GO;





CREATE TABLE contest (
    id bigint NOT NULL,
    fio text NOT NULL,
    email text NOT NULL,
    file_url text NOT NULL,
    time_added timestamp(0) without time zone DEFAULT now() NOT NULL,
    profile_id bigint NOT NULL
)
GO;



CREATE SEQUENCE contest_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
GO;

CREATE TABLE custom_meta_tag (
    custom_meta_tag_id bigint NOT NULL,
    custom_meta_tag_type_id integer DEFAULT 0 NOT NULL,
    tag_path text NOT NULL,
    title text,
    description text,
    keywords text,
    image text,
    og_title text,
    og_description text,
    og_url text
)
GO;



CREATE TABLE delivery_address (
    delivery_address_id bigint,
    order_id bigint,
    address text,
    city text,
    country text,
    zip_code text,
    phone text,
    fio text
)
GO;



CREATE TABLE draft_campaigns (
    draft_campaign_id bigint NOT NULL,
    campaign_id bigint NOT NULL,
    profile_id bigint NOT NULL,
    json_campaign jsonb NOT NULL,
    is_edited boolean DEFAULT false NOT NULL,
    updated_profile_id bigint,
    time_added timestamp without time zone DEFAULT now() NOT NULL,
    time_updated timestamp without time zone DEFAULT now() NOT NULL
)
GO;



CREATE TABLE email_notifications (
    message_id bigint NOT NULL,
    sender character varying(64),
    addressee character varying(64),
    subject text,
    message_body text,
    time_added timestamp without time zone DEFAULT now(),
    time_updated timestamp without time zone,
    external_message_id character varying(64),
    status integer,
    reply_to character varying(64)
)
GO;



CREATE TABLE email_notifications_logs (
    message_id bigint NOT NULL,
    sender text,
    addressee text,
    subject text,
    message_body text,
    time_added timestamp without time zone DEFAULT now(),
    time_updated timestamp without time zone,
    external_message_id text,
    status integer,
    reply_to text
)
GO;



CREATE TABLE faq_article (
    faq_article_id bigint NOT NULL,
    faq_category_id bigint NOT NULL,
    order_num integer NOT NULL,
    title text NOT NULL
)
GO;



CREATE TABLE faq_category (
    faq_category_id bigint NOT NULL,
    title text NOT NULL,
    icon_name text NOT NULL
)
GO;



CREATE TABLE faq_paragraph (
    faq_paragraph_id bigint NOT NULL,
    faq_article_id bigint NOT NULL,
    order_num integer NOT NULL,
    title text NOT NULL,
    text_html text NOT NULL,
    annotation text
)
GO;



CREATE TABLE iframe_app (
    iframe_app_id bigint NOT NULL,
    iframe_app_type_id integer NOT NULL,
    iframe_app_external_id text NOT NULL,
    iframe_app_secret_key text NOT NULL,
    iframe_app_html text,
    iframe_app_name text,
    campaign_id bigint NOT NULL
)
GO;



CREATE TABLE ipgeobase_cities_codes (
    city_id_geo integer NOT NULL,
    city_id_planeta integer NOT NULL
)
GO;



CREATE TABLE ipgeobase_countries_codes (
    country_code character varying(2) NOT NULL,
    country_name character varying(255) NOT NULL,
    country_id bigint
)
GO;



CREATE TABLE ipgeobase_ips (
    begin_ip bigint NOT NULL,
    end_ip bigint NOT NULL,
    ip_begin_end character varying(255) NOT NULL,
    country_id character varying(255) NOT NULL,
    city_id integer
)
GO;



CREATE TABLE ipgeobase_names (
    city_id integer NOT NULL,
    city_name character varying(255) NOT NULL,
    region_name character varying(255) NOT NULL,
    region_name_other character varying(255) NOT NULL,
    latitude character varying(255) NOT NULL,
    longitude character varying(255) NOT NULL
)
GO;



CREATE VIEW jasper_aid_payments AS
 SELECT DISTINCT o.order_id,
    o.buyer_id,
    o.credit_transaction_id,
    o.debit_transaction_id,
    o.cancel_credit_transaction_id,
    o.cancel_debit_transaction_id,
    o.payment_status,
    o.time_added,
    c.campaign_id,
    c.time_finish,
    c.status,
    c.name,
    c.target_amount,
    c.target_status
   FROM (((orders o
     JOIN order_objects oo USING (order_id))
     JOIN shares s ON ((s.share_id = oo.object_id)))
     JOIN campaigns c USING (campaign_id))
  WHERE ((o.order_type = 1) AND (o.payment_status = ANY (ARRAY[1, 2])))
GO;
CREATE TABLE jasper_aid_view (
    time_added date,
    order_id bigint,
    buyer_id bigint,
    payment_status integer,
    total_price numeric(10,2),
    object_id bigint
)
GO;

CREATE TABLE linked_delivery_service (
    service_id bigint DEFAULT 0 NOT NULL,
    price numeric(11,2) DEFAULT 0 NOT NULL,
    note text,
    is_enabled boolean DEFAULT true,
    custom_address_id bigint,
    subject_type integer DEFAULT 1,
    subject_id bigint
)
GO;

CREATE TABLE linked_delivery_service_backup (
    share_id bigint,
    service_id bigint,
    price numeric(11,2),
    note text,
    is_enabled boolean,
    custom_address_id bigint
)
GO;



CREATE SEQUENCE seq_mail_attachment_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
GO;



CREATE TABLE mail_attachments (
    attachment_id bigint DEFAULT nextval('seq_mail_attachment_id'::regclass) NOT NULL,
    template_id integer,
    file_name text,
    mime_type text,
    content bytea
)
GO;



CREATE SEQUENCE seq_mail_template_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
GO;



CREATE TABLE mail_templates (
    template_id integer DEFAULT nextval('seq_mail_template_id'::regclass) NOT NULL,
    name character varying(250) NOT NULL,
    subject text,
    footer text,
    content_bbcode text,
    from_address character varying(500),
    content_html text,
    reply_to_address text DEFAULT 'support@planeta.ru'::text,
    to_address text DEFAULT '${userEmail}'::text,
    use_footer_and_header boolean,
    skip_logging boolean DEFAULT false
)
GO;





CREATE TABLE moderation_messages (
    message_id bigint DEFAULT nextval(('commondb.seq_moderation_messages_message_id'::text)::regclass) NOT NULL,
    campaign_id bigint NOT NULL,
    sender_id bigint NOT NULL,
    message text NOT NULL,
    time_added timestamp(0) without time zone DEFAULT now() NOT NULL,
    status integer NOT NULL,
    campaign_status integer DEFAULT 0 NOT NULL
)
GO;



CREATE TABLE muztorg (
    muztorg_id bigint NOT NULL,
    email text NOT NULL,
    image_id bigint,
    video_id bigint,
    time_added timestamp without time zone
)
GO;



CREATE TABLE news (
    news_id integer NOT NULL,
    time_added timestamp without time zone DEFAULT now() NOT NULL,
    title character varying(256) NOT NULL,
    text_html text NOT NULL,
    link character varying(256) NOT NULL
)
GO;



CREATE TABLE news_broadcast (
    news_broadcast_id bigint NOT NULL,
    profile_id bigint NOT NULL,
    post_id bigint NOT NULL,
    time_added timestamp(0) without time zone NOT NULL
)
GO;





CREATE SEQUENCE news_news_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
GO;



CREATE TABLE objects_stats (
    date timestamp without time zone NOT NULL,
    object_type character varying(50) NOT NULL,
    object_id bigint NOT NULL,
    visitors integer DEFAULT 0 NOT NULL,
    views integer DEFAULT 0 NOT NULL,
    buys integer DEFAULT 0 NOT NULL,
    buyers integer DEFAULT 0 NOT NULL,
    collected numeric DEFAULT 0 NOT NULL,
    affiliate_amount numeric DEFAULT 0 NOT NULL
)
GO;



CREATE TABLE partner (
    partner_id bigint NOT NULL,
    logo_url text,
    site_url text
)
GO;

CREATE TABLE payment_card_bindings (
    binding_id bigint NOT NULL,
    user_id bigint NOT NULL,
    card_number_mask character varying(20) NOT NULL,
    card_cvv character varying(3),
    payment_system_id integer,
    access_token text NOT NULL,
    time_added timestamp without time zone DEFAULT now() NOT NULL,
    payment_provider_id bigint,
    payment_provider_account text,
    deleted boolean DEFAULT false NOT NULL,
    last_time_used timestamp without time zone DEFAULT now() NOT NULL
)
GO;


CREATE TABLE payment_error_comments (
    payment_error_comment_id bigint NOT NULL,
    payment_error_id bigint NOT NULL,
    profile_id bigint NOT NULL,
    text text,
    time_added timestamp without time zone NOT NULL,
    time_updated timestamp without time zone NOT NULL
)
GO;



CREATE TABLE payment_error_status (
    payment_error_status_id integer NOT NULL,
    payment_error_status_name text NOT NULL,
    description text NOT NULL,
    full_description text NOT NULL
)
GO;



CREATE TABLE payment_error_type (
    payment_error_type_id integer NOT NULL,
    payment_error_type_name text NOT NULL,
    description text NOT NULL,
    full_description text NOT NULL
)
GO;



CREATE TABLE payment_errors (
    payment_error_id bigint NOT NULL,
    payment_error_type_id integer NOT NULL,
    profile_id bigint NOT NULL,
    transaction_id bigint NOT NULL,
    order_id bigint,
    payment_error_status_id integer NOT NULL,
    manager_id bigint,
    error_text text,
    campaign_id bigint,
    amount numeric(10,2),
    time_added timestamp without time zone NOT NULL,
    time_updated timestamp without time zone NOT NULL
)
GO;



CREATE TABLE payment_methods (
    id bigint NOT NULL,
    alias character varying(20) NOT NULL,
    image_url character varying(100) NOT NULL,
    name character varying(50) NOT NULL,
    description character varying(255),
    mobile boolean DEFAULT false NOT NULL,
    need_phone boolean DEFAULT false NOT NULL,
    deferred boolean DEFAULT false NOT NULL,
    card boolean DEFAULT false NOT NULL,
    internal boolean DEFAULT false NOT NULL,
    promo boolean DEFAULT false NOT NULL,
    parent_id bigint DEFAULT 0 NOT NULL,
    name_en character varying(50) DEFAULT ''::character varying NOT NULL,
    description_en character varying(255) DEFAULT ''::character varying NOT NULL,
    parent_id_old bigint,
    image_url_old character varying(100) DEFAULT NULL::character varying,
    name_old character varying(50) DEFAULT NULL::character varying
)
GO;



CREATE TABLE payment_tools (
    id bigint NOT NULL,
    name character varying(100),
    code character varying(20) NOT NULL,
    payment_provider_id bigint NOT NULL,
    min integer DEFAULT 0 NOT NULL,
    max integer DEFAULT 0 NOT NULL,
    commission numeric NOT NULL,
    monthly_limit integer DEFAULT 0 NOT NULL,
    enabled boolean DEFAULT true NOT NULL,
    tls_supported boolean DEFAULT true
)
GO;



CREATE TABLE planeta_campaign_managers (
    manager_id bigint NOT NULL,
    campaign_id bigint NOT NULL
)
GO;



CREATE SEQUENCE seq_manager_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
GO;



CREATE TABLE planeta_managers (
    manager_id bigint DEFAULT nextval('seq_manager_id'::regclass) NOT NULL,
    name text NOT NULL,
    full_name text NOT NULL,
    email text NOT NULL,
    profile_id bigint DEFAULT 0 NOT NULL,
    active boolean DEFAULT true NOT NULL
)
GO;




CREATE TABLE planeta_managers_relations (
    manager_id bigint NOT NULL,
    category integer NOT NULL,
    managed_object_type integer NOT NULL
)
GO;


CREATE TABLE printing_tag_relations (
    printing_id bigint NOT NULL,
    tag_id bigint NOT NULL,
    order_num integer DEFAULT 0 NOT NULL
)
GO;



CREATE TABLE printing_tags (
    category_id bigint NOT NULL,
    value text,
    preferred_order integer DEFAULT 0 NOT NULL,
    mnemonic_name text DEFAULT 'TEMP'::text NOT NULL,
    is_primary_cat boolean DEFAULT true NOT NULL
)
GO;



CREATE TABLE printings (
    printing_id bigint NOT NULL,
    name text,
    description text,
    subscription integer NOT NULL,
    new_price integer NOT NULL,
    image_url text,
    publishing_house text,
    periodicity text,
    status integer NOT NULL,
    personal_bonus text,
    change_cost_date date,
    comments text
)
GO;



CREATE TABLE private_resources (
    hash character varying(50) NOT NULL,
    path character varying(300) NOT NULL,
    create_date timestamp without time zone DEFAULT now() NOT NULL,
    profile_id bigint
)
GO;



CREATE TABLE product_photos (
    product_id bigint NOT NULL,
    profile_id bigint NOT NULL,
    image_id bigint NOT NULL,
    image_profile_id bigint NOT NULL,
    image_url character varying(256)
)
GO;



CREATE VIEW profile_balances_delta AS
 SELECT t1.profile_id,
    t1.display_name,
    t1.profile_type_id,
    t1.balance,
    t1.balance_agregated,
    t1.delta
   FROM ( SELECT p.profile_id,
            p.display_name,
            p.profile_type_id,
            b.balance,
            sum(transactions.amount) AS balance_agregated,
            (b.balance - sum(transactions.amount)) AS delta
           FROM ((( SELECT credit_transactions.transaction_id,
                    credit_transactions.profile_id,
                    (- (credit_transactions.amount_net - credit_transactions.amount_fee)) AS amount,
                    credit_transactions.time_added,
                    credit_transactions.comment
                   FROM credit_transactions
                UNION
                 SELECT debit_transactions.transaction_id,
                    debit_transactions.profile_id,
                    (debit_transactions.amount_net - debit_transactions.amount_fee) AS amount,
                    debit_transactions.time_added,
                    debit_transactions.comment
                   FROM debit_transactions) transactions
             JOIN profiledb.profiles p ON ((transactions.profile_id = p.profile_id)))
             JOIN profile_balances b ON ((b.profile_id = p.profile_id)))
          GROUP BY p.profile_id, p.display_name, p.profile_type_id, b.balance
          ORDER BY b.balance DESC, p.display_name) t1
  WHERE (t1.delta <> (0)::numeric)
GO;



CREATE TABLE project_payment_methods (
    project_type character varying(20) NOT NULL,
    payment_method_id bigint NOT NULL,
    priority integer DEFAULT 0 NOT NULL,
    enabled boolean DEFAULT true NOT NULL
)
GO;

CREATE TABLE project_payment_tools (
    id bigint NOT NULL,
    project_type character varying(50),
    payment_method_id bigint NOT NULL,
    payment_tool_id bigint NOT NULL,
    enabled boolean DEFAULT true NOT NULL,
    priority integer DEFAULT 1 NOT NULL
)
GO;

CREATE TABLE purchase_share_info (
    id bigint NOT NULL,
    share_id bigint NOT NULL,
    quantity integer NOT NULL,
    donate_amount numeric(10,2) NOT NULL
)
GO;

CREATE SEQUENCE purchase_share_info_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
GO;

CREATE TABLE push_tokens (
    profile_id bigint NOT NULL,
    token_type integer NOT NULL,
    token character(80) NOT NULL,
    create_date date
)
GO;


CREATE TABLE quickstart_get_book (
    email text NOT NULL,
    book_type text NOT NULL,
    profile_id bigint,
    time_added timestamp without time zone DEFAULT now() NOT NULL
)
GO;



CREATE TABLE rewards (
    reward_id bigint NOT NULL,
    share_id bigint NOT NULL,
    profile_id bigint NOT NULL,
    name text NOT NULL,
    description text NOT NULL,
    image_id bigint,
    image_url text,
    time_added timestamp without time zone DEFAULT now() NOT NULL
)
GO;




CREATE TABLE scheduled_blog_posts (
    profile_id bigint NOT NULL,
    blog_post_id bigint NOT NULL,
    scheduled_time timestamp without time zone DEFAULT now() NOT NULL
)
GO;



CREATE TABLE school_nko (
    school_nko_id bigint NOT NULL,
    profile_id bigint,
    email text NOT NULL,
    organization_name text,
    organization_type text,
    "position" text,
    city_name text,
    registration_date timestamp without time zone,
    fio text,
    mobile_phone text,
    work_phone text,
    web_site text,
    time_added timestamp without time zone DEFAULT now() NOT NULL
)
GO;




CREATE TABLE seminar (
    seminar_id bigint NOT NULL,
    name text NOT NULL,
    time_start timestamp without time zone NOT NULL,
    time_added timestamp without time zone NOT NULL,
    city_id bigint DEFAULT 0 NOT NULL,
    tag_id bigint DEFAULT 0 NOT NULL,
    url text,
    participants_limit integer,
    background_image text,
    short_description text,
    description text,
    conditions text,
    results text,
    seminar_type integer,
    format_selection boolean,
    speakers_ids bigint[],
    partners_ids bigint[],
    time_registration_end timestamp without time zone NOT NULL,
    venue text,
    is_shown_on_main_page boolean DEFAULT false NOT NULL
)
GO;





CREATE TABLE seminar_registration (
    seminar_registration_id bigint NOT NULL,
    profile_id bigint NOT NULL,
    seminar_id bigint NOT NULL,
    seminar_format boolean DEFAULT true NOT NULL,
    time_added timestamp without time zone NOT NULL,
    school_category_id integer,
    profile_id2 bigint,
    city text,
    campaign_goal text,
    campaign_target_sum text,
    phone text,
    phone2 text,
    fio text DEFAULT ' '::text NOT NULL,
    fio2 text DEFAULT ' '::text NOT NULL,
    campaign_short_description text,
    organization_name text,
    organization_line_of_ativity text,
    "position" text,
    work_phone text,
    organization_registration_date timestamp without time zone,
    organization_site text
)
GO;






CREATE TABLE seminar_type (
    seminar_type_id integer NOT NULL,
    seminar_type_name text NOT NULL,
    description text NOT NULL,
    full_description text NOT NULL
)
GO;

CREATE TABLE seminars (
    seminar_id bigint NOT NULL,
    title text,
    seminar_date timestamp without time zone NOT NULL
)
GO;


CREATE SEQUENCE seq_address_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
GO;



CREATE SEQUENCE seq_addressee_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
GO;



CREATE SEQUENCE seq_audio_album_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
GO;



CREATE SEQUENCE seq_audio_playlist_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
GO;



CREATE SEQUENCE seq_audio_track_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
GO;



CREATE SEQUENCE seq_auth_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
GO;



CREATE SEQUENCE seq_background_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
GO;



CREATE SEQUENCE seq_banner_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
GO;



CREATE SEQUENCE seq_blog_category_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
GO;



CREATE SEQUENCE seq_blog_post_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
GO;



CREATE SEQUENCE seq_bonus_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
GO;



CREATE SEQUENCE seq_bonus_order_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
GO;



CREATE SEQUENCE seq_broadcast_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
GO;



CREATE SEQUENCE seq_broadcast_stream_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
GO;



CREATE SEQUENCE seq_cached_video_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
GO;



CREATE SEQUENCE seq_campaign_faq_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
GO;



CREATE SEQUENCE seq_campaign_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
GO;



CREATE SEQUENCE seq_campaign_tag_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
GO;



CREATE SEQUENCE seq_chat_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
GO;



CREATE SEQUENCE seq_chat_message_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
GO;



CREATE SEQUENCE seq_comment_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
GO;



CREATE SEQUENCE seq_comment_object_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
GO;



CREATE SEQUENCE seq_commondb_visitor_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
GO;



CREATE SEQUENCE seq_commondb_visitor_session_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
GO;



CREATE SEQUENCE seq_contractor_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
GO;



CREATE SEQUENCE seq_credit_transaction_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
GO;



CREATE SEQUENCE seq_custom_meta_tag
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
GO;



CREATE SEQUENCE seq_daily_feature_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
GO;



CREATE SEQUENCE seq_debit_transaction_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
GO;



CREATE SEQUENCE seq_delivery_address_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
GO;



CREATE SEQUENCE seq_delivery_service_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
GO;



CREATE SEQUENCE seq_dialog_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
GO;



CREATE SEQUENCE seq_draft_campaign_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
GO;


CREATE SEQUENCE seq_faq_article_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
GO;



CREATE SEQUENCE seq_faq_category_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
GO;



CREATE SEQUENCE seq_faq_paragraph_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
GO;



CREATE SEQUENCE seq_feed_item_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
GO;



CREATE SEQUENCE seq_iframe_app_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
GO;



CREATE SEQUENCE seq_iframe_feed_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
GO;



CREATE SEQUENCE seq_library_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
GO;



CREATE SEQUENCE seq_message_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
GO;



CREATE SEQUENCE seq_moderation_messages_message_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
GO;



CREATE SEQUENCE seq_muztorg_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
GO;



CREATE SEQUENCE seq_news_broadcast_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
GO;



CREATE SEQUENCE seq_ny_bonus_mail_props_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
GO;



CREATE SEQUENCE seq_order_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
GO;



CREATE SEQUENCE seq_order_object_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
GO;



CREATE SEQUENCE seq_partner_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
GO;



CREATE SEQUENCE seq_payment_card_binding_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
GO;



CREATE SEQUENCE seq_payment_error_comment_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
GO;



CREATE SEQUENCE seq_payment_error_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
GO;



CREATE SEQUENCE seq_payment_method_id
    START WITH 100
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
GO;



CREATE SEQUENCE seq_payment_provider_id
    START WITH 100
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
GO;



CREATE SEQUENCE seq_payment_tool_id
    START WITH 100
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
GO;



CREATE SEQUENCE seq_photo_album_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
GO;



CREATE SEQUENCE seq_photo_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
GO;



CREATE SEQUENCE seq_printing_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
GO;


CREATE SEQUENCE seq_product_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
GO;



CREATE SEQUENCE seq_profile_file_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
GO;



CREATE SEQUENCE seq_profile_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
GO;



CREATE SEQUENCE seq_project_payment_tool_id
    START WITH 100
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
GO;



CREATE SEQUENCE seq_purchase_share_info_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
GO;



CREATE SEQUENCE seq_reward_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
GO;



CREATE SEQUENCE seq_school_nko_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
GO;



CREATE SEQUENCE seq_seminar_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
GO;



CREATE SEQUENCE seq_seminar_registration_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
GO;



CREATE SEQUENCE seq_short_link_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
GO;



CREATE SEQUENCE seq_speaker_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
GO;



CREATE SEQUENCE seq_stat_event_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
GO;



CREATE SEQUENCE seq_status_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
GO;



CREATE SEQUENCE seq_tag_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
GO;



CREATE SEQUENCE seq_ticket_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
GO;



CREATE SEQUENCE seq_timeline_item_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
GO;



CREATE SEQUENCE seq_topay_transaction_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
GO;



CREATE SEQUENCE seq_user_address_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
GO;



CREATE SEQUENCE seq_user_callback_comment_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
GO;



CREATE SEQUENCE seq_user_callback_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
GO;



CREATE SEQUENCE seq_user_notification_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
GO;



CREATE SEQUENCE seq_user_notification_settings_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
GO;



CREATE SEQUENCE seq_user_proposal_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
GO;



CREATE SEQUENCE seq_video_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
GO;



CREATE SEQUENCE seq_wall_post_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
GO;



CREATE SEQUENCE seq_yamo_mws_operation_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
GO;



CREATE TABLE share_delivery_settings (
    share_id bigint NOT NULL,
    deliver_to_customer boolean DEFAULT false NOT NULL,
    pickup_by_customer boolean DEFAULT false NOT NULL,
    addressee_id bigint
)
GO;

CREATE TABLE share_delivery_settings_backup (
    share_id bigint,
    deliver_to_customer boolean,
    pickup_by_customer boolean,
    addressee_id bigint
)
GO;



CREATE TABLE short_links (
    short_link_id bigint NOT NULL,
    short_link_status integer,
    short_link text,
    redirect_address_url text,
    cookie_name text,
    cookie_value text,
    profile_id bigint DEFAULT 0 NOT NULL
)
GO;



CREATE TABLE speaker (
    speaker_id bigint NOT NULL,
    name text NOT NULL,
    description text NOT NULL,
    photo_url text NOT NULL
)
GO;


CREATE TABLE sponsors (
    alias character varying(50) NOT NULL,
    multiplier integer DEFAULT 1 NOT NULL,
    project_card_html text,
    project_page_html text,
    search_page_html text,
    max_donate bigint
)
GO;



CREATE TABLE subscribers (
    subscription_code text NOT NULL,
    email text NOT NULL
)
GO;


CREATE TABLE sync_info_1c (
    object_id numeric(10,0) NOT NULL,
    id_1c numeric(10,0) NOT NULL,
    object_type numeric(10,0) NOT NULL
)
GO;



CREATE TABLE technobattle_registration (
    registration_id bigint NOT NULL,
    email character varying(64) NOT NULL,
    type character varying(20) NOT NULL,
    name text,
    phone text,
    city text,
    description text,
    time_added timestamp without time zone,
    request_method character varying(20)
)
GO;

CREATE SEQUENCE technobattle_registration_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
GO;



CREATE TABLE tickets (
    ticket_id bigint NOT NULL,
    profile_id bigint NOT NULL,
    name text NOT NULL,
    ticket_category integer NOT NULL,
    time_added timestamp without time zone DEFAULT now() NOT NULL,
    time_updated timestamp without time zone DEFAULT now() NOT NULL,
    price numeric(10,2) NOT NULL,
    amount integer DEFAULT 0 NOT NULL,
    purchase_count integer DEFAULT 0 NOT NULL,
    time_sales_start timestamp without time zone DEFAULT now() NOT NULL,
    time_sales_finish timestamp without time zone DEFAULT now(),
    in_process_count integer DEFAULT 0 NOT NULL,
    description text
)
GO;


CREATE TABLE tickets_checkout (
    order_id bigint NOT NULL,
    object_id bigint DEFAULT 0 NOT NULL,
    seq_num integer DEFAULT 0 NOT NULL,
    time_checkout timestamp without time zone DEFAULT now() NOT NULL,
    order_object_id bigint NOT NULL
)
GO;






CREATE TABLE top_special (
    top_special_id bigint NOT NULL,
    owner_profile_id bigint NOT NULL,
    object_type_id integer NOT NULL,
    top_position integer NOT NULL,
    top_special_url text NOT NULL
)
GO;



CREATE TABLE two_can_payment (
    two_can_id bigint NOT NULL,
    email character varying(64) NOT NULL,
    status integer NOT NULL,
    time_added timestamp without time zone DEFAULT now() NOT NULL,
    target text,
    amount text
)
GO;



CREATE TABLE user_addresses (
    profile_id bigint NOT NULL,
    address_id bigint NOT NULL,
    last_used boolean NOT NULL,
    zip_code text NOT NULL,
    city text NOT NULL,
    address text NOT NULL,
    phone text
)
GO;


CREATE TABLE user_callbacks (
    user_callback_id bigint NOT NULL,
    type text NOT NULL,
    user_id bigint DEFAULT 0 NOT NULL,
    user_phone character varying(16) NOT NULL,
    order_type integer,
    order_object_id bigint DEFAULT 0 NOT NULL,
    payment_id bigint DEFAULT 0 NOT NULL,
    amount numeric(10,2) NOT NULL,
    manager_id bigint DEFAULT 0 NOT NULL,
    processed boolean DEFAULT false NOT NULL,
    time_added timestamp without time zone DEFAULT now() NOT NULL,
    time_updated timestamp without time zone
)
GO;


CREATE TABLE user_credentials (
    profile_id bigint NOT NULL,
    username text NOT NULL,
    credential_type_code integer NOT NULL,
    credential_status_code integer DEFAULT 0 NOT NULL
)
GO;






CREATE TABLE user_last_online_time (
    profile_id bigint NOT NULL,
    last_online_time timestamp without time zone DEFAULT now() NOT NULL
)
GO;



CREATE TABLE user_login_info (
    profile_id bigint NOT NULL,
    user_ip_address character varying(255) NOT NULL,
    user_agent text NOT NULL,
    time_login timestamp without time zone DEFAULT now() NOT NULL
)
GO;



CREATE TABLE user_sources (
    profile_id bigint NOT NULL,
    enter_url text NOT NULL,
    referrer_url text
)
GO;




CREATE TABLE users_proposal (
    id bigint NOT NULL,
    profile_id bigint NOT NULL,
    campaign_id bigint DEFAULT 0 NOT NULL,
    content character varying(1000),
    time_added timestamp without time zone DEFAULT now() NOT NULL
)
GO;



CREATE VIEW view_order_campaign AS
 SELECT DISTINCT o.order_id,
    o.buyer_id,
    o.payment_status,
    o.time_added,
    o.total_price,
    s.campaign_id,
    s.share_id
   FROM ((orders o
     JOIN order_objects oo USING (order_id))
     JOIN shares s ON ((s.share_id = oo.object_id)))
  WHERE (o.order_type = 1)
GO;



CREATE UNLOGGED TABLE visitor_sessions (
    id bigint NOT NULL,
    visitor_id bigint NOT NULL,
    create_date timestamp without time zone NOT NULL,
    referer character varying(1000),
    city character varying(100),
    country character varying(100),
    source_id bigint NOT NULL,
    affiliate_id bigint DEFAULT 0 NOT NULL
)
GO;



CREATE UNLOGGED TABLE visitors (
    id bigint NOT NULL,
    create_date timestamp without time zone NOT NULL,
    update_date timestamp without time zone NOT NULL,
    profile_id bigint NOT NULL
)
GO;



CREATE VIEW widgets_stats AS
 SELECT a.type,
    a.object_id AS campaign_id,
    a.referer,
    a.date,
    count(*) AS count
   FROM ( SELECT
                CASE e.type
                    WHEN 'WIDGET_IMAGE_SHOW'::text THEN 'IMAGE'::text
                    WHEN 'WIDGET_IFRAME_SHOW'::text THEN 'IFRAME'::text
                    WHEN 'WIDGET_PURCHASE_SHOW'::text THEN 'PURCHASE'::text
                    ELSE NULL::text
                END AS type,
            e.object_id,
            (date_trunc('DAY'::text, e.date))::date AS date,
            e.referer
           FROM stat_events e
          WHERE (((e.type)::text = ANY (ARRAY[('WIDGET_IMAGE_SHOW'::character varying)::text, ('WIDGET_IFRAME_SHOW'::character varying)::text, ('WIDGET_PURCHASE_SHOW'::character varying)::text])) AND ((e.referer)::text <> ''::text))) a
  GROUP BY a.type, a.object_id, a.date, a.referer
GO;



ALTER TABLE ONLY contest ALTER COLUMN id SET DEFAULT nextval('contest_id_seq'::regclass)
GO;


ALTER TABLE ONLY news ALTER COLUMN news_id SET DEFAULT nextval('news_news_id_seq'::regclass)
GO;


ALTER TABLE ONLY addressees
    ADD CONSTRAINT addressees_pkey PRIMARY KEY (addressee_id)
GO;


ALTER TABLE ONLY addresses
    ADD CONSTRAINT addresses_pkey PRIMARY KEY (address_id)
GO;


ALTER TABLE ONLY auth
    ADD CONSTRAINT auth_pkey PRIMARY KEY (auth_id)
GO;


ALTER TABLE ONLY banner_keywords
    ADD CONSTRAINT banners_keyword_pkey PRIMARY KEY (banner_id, keywords)
GO;


ALTER TABLE ONLY banners
    ADD CONSTRAINT banners_pkey PRIMARY KEY (banner_id)
GO;


ALTER TABLE ONLY bonus_orders
    ADD CONSTRAINT bonus_orders_pkey PRIMARY KEY (bonus_order_id)
GO;


ALTER TABLE ONLY bonuses
    ADD CONSTRAINT bonuses_pkey PRIMARY KEY (bonus_id)
GO;


ALTER TABLE ONLY broadcast_advertising
    ADD CONSTRAINT broadcast_advertising_pkey PRIMARY KEY (broadcast_id)
GO;


ALTER TABLE ONLY callbacks_comments
    ADD CONSTRAINT callbacks_comments_pkey PRIMARY KEY (comment_id)
GO;


ALTER TABLE ONLY campaign_edit_time
    ADD CONSTRAINT campaign_edit_time_pkey PRIMARY KEY (campaign_id, profile_id)
GO;


ALTER TABLE ONLY campaign_faq
    ADD CONSTRAINT campaign_faq_pkey PRIMARY KEY (campaign_faq_id)
GO;


ALTER TABLE ONLY planeta_managers
    ADD CONSTRAINT campaign_managers_pkey PRIMARY KEY (manager_id)
GO;


ALTER TABLE ONLY planeta_managers_relations
    ADD CONSTRAINT campaign_managers_relations_pkey PRIMARY KEY (manager_id, category, managed_object_type)
GO;


ALTER TABLE ONLY campaign_median_price
    ADD CONSTRAINT campaign_median_price_pkey PRIMARY KEY (campaign_id)
GO;


ALTER TABLE ONLY campaign_tag_relations
    ADD CONSTRAINT campaign_ord_num_unique UNIQUE (campaign_id, order_num)
GO;


ALTER TABLE ONLY campaign_permissions
    ADD CONSTRAINT campaign_permissions_pkey PRIMARY KEY (campaign_id)
GO;


ALTER TABLE ONLY campaign_subscribe
    ADD CONSTRAINT campaign_subscribe_pkey PRIMARY KEY (profile_id, campaign_id)
GO;


ALTER TABLE ONLY campaign_tag_relations
    ADD CONSTRAINT campaign_tag_relations_pkey PRIMARY KEY (campaign_id, tag_id)
GO;


ALTER TABLE ONLY campaign_tags
    ADD CONSTRAINT campaign_tags_pkey PRIMARY KEY (tag_id)
GO;


ALTER TABLE ONLY campaign_targeting
    ADD CONSTRAINT campaign_targeting_pkey PRIMARY KEY (campaign_id)
GO;


ALTER TABLE ONLY campaigns
    ADD CONSTRAINT campaigns_pkey PRIMARY KEY (campaign_id)
GO;


ALTER TABLE ONLY ipgeobase_cities_codes
    ADD CONSTRAINT city_id_geo_pkey PRIMARY KEY (city_id_geo)
GO;


ALTER TABLE ONLY ipgeobase_cities_codes
    ADD CONSTRAINT city_id_planeta UNIQUE (city_id_planeta)
GO;


ALTER TABLE ONLY config
    ADD CONSTRAINT config_pk PRIMARY KEY (appid, ip, port, machid, key)
GO;


ALTER TABLE ONLY configuration
    ADD CONSTRAINT configuration_key_key UNIQUE (key)
GO;


ALTER TABLE ONLY contest
    ADD CONSTRAINT contest_pkey PRIMARY KEY (id)
GO;


ALTER TABLE ONLY contractor_relations
    ADD CONSTRAINT contractor_relation_pkey PRIMARY KEY (contractor_id, campaign_id)
GO;


ALTER TABLE ONLY contractors
    ADD CONSTRAINT contractors_pkey PRIMARY KEY (contractor_id)
GO;


ALTER TABLE ONLY credit_transactions
    ADD CONSTRAINT credit_transactions_pkey PRIMARY KEY (transaction_id)
GO;


ALTER TABLE ONLY custom_meta_tag
    ADD CONSTRAINT custom_meta_tag_custom_meta_tag_type_id_tag_path_key UNIQUE (custom_meta_tag_type_id, tag_path)
GO;


ALTER TABLE ONLY custom_meta_tag
    ADD CONSTRAINT custom_meta_tag_pkey PRIMARY KEY (custom_meta_tag_id)
GO;


ALTER TABLE ONLY debit_transactions
    ADD CONSTRAINT debit_transactions_pkey PRIMARY KEY (transaction_id)
GO;


ALTER TABLE ONLY draft_campaigns
    ADD CONSTRAINT draft_campaigns_pkey PRIMARY KEY (draft_campaign_id)
GO;


ALTER TABLE ONLY email_notifications_logs
    ADD CONSTRAINT email_notifications_logs_pkey PRIMARY KEY (message_id)
GO;


ALTER TABLE ONLY email_notifications
    ADD CONSTRAINT email_notifications_pkey PRIMARY KEY (message_id)
GO;


ALTER TABLE ONLY faq_article
    ADD CONSTRAINT faq_article_pkey PRIMARY KEY (faq_article_id)
GO;


ALTER TABLE ONLY faq_category
    ADD CONSTRAINT faq_category_pkey PRIMARY KEY (faq_category_id)
GO;


ALTER TABLE ONLY faq_paragraph
    ADD CONSTRAINT faq_paragraph_pkey PRIMARY KEY (faq_paragraph_id)
GO;


ALTER TABLE ONLY iframe_app
    ADD CONSTRAINT iframe_app_pkey PRIMARY KEY (iframe_app_id)
GO;


ALTER TABLE ONLY investing_order_info
    ADD CONSTRAINT investing_order_info_pkey PRIMARY KEY (investing_order_info_id)
GO;


ALTER TABLE ONLY ipgeobase_ips
    ADD CONSTRAINT ip_pkey PRIMARY KEY (begin_ip)
GO;


ALTER TABLE ONLY ipgeobase_names
    ADD CONSTRAINT ipgeobase_city_pkey PRIMARY KEY (city_id)
GO;


ALTER TABLE ONLY ipgeobase_countries_codes
    ADD CONSTRAINT ipgeobase_countries_codes_pkey PRIMARY KEY (country_code)
GO;


ALTER TABLE ONLY linked_delivery_service
    ADD CONSTRAINT linked_delivery_service_unique_check UNIQUE (subject_id, service_id, subject_type)
GO;


ALTER TABLE ONLY mail_attachments
    ADD CONSTRAINT mail_attachments_pkey PRIMARY KEY (attachment_id)
GO;


ALTER TABLE ONLY mail_templates
    ADD CONSTRAINT mail_templates_pkey PRIMARY KEY (template_id)
GO;


ALTER TABLE ONLY moderation_messages
    ADD CONSTRAINT moderation_messages_pkey PRIMARY KEY (message_id)
GO;


ALTER TABLE ONLY muztorg
    ADD CONSTRAINT muztorg_pkey PRIMARY KEY (muztorg_id)
GO;


ALTER TABLE ONLY news_broadcast
    ADD CONSTRAINT news_broadcast_pkey PRIMARY KEY (news_broadcast_id)
GO;


ALTER TABLE ONLY news
    ADD CONSTRAINT news_pkey PRIMARY KEY (news_id)
GO;


ALTER TABLE ONLY objects_stats
    ADD CONSTRAINT objects_stats_pkey PRIMARY KEY (date, object_type, object_id)
GO;


ALTER TABLE ONLY order_objects
    ADD CONSTRAINT order_objects_pkey PRIMARY KEY (order_object_id)
GO;


ALTER TABLE ONLY orders
    ADD CONSTRAINT orders_items_pkey PRIMARY KEY (order_id)
GO;


ALTER TABLE ONLY partner
    ADD CONSTRAINT partner_pkey PRIMARY KEY (partner_id)
GO;


ALTER TABLE ONLY payment_error_comments
    ADD CONSTRAINT payment_error_comments_pkey PRIMARY KEY (payment_error_comment_id)
GO;


ALTER TABLE ONLY payment_error_status
    ADD CONSTRAINT payment_error_status_pkey PRIMARY KEY (payment_error_status_id)
GO;


ALTER TABLE ONLY payment_error_type
    ADD CONSTRAINT payment_error_type_pkey PRIMARY KEY (payment_error_type_id)
GO;


ALTER TABLE ONLY payment_errors
    ADD CONSTRAINT payment_errors_pkey PRIMARY KEY (payment_error_id)
GO;


ALTER TABLE ONLY payment_methods
    ADD CONSTRAINT payment_methods_pkey PRIMARY KEY (id)
GO;


ALTER TABLE ONLY payment_providers
    ADD CONSTRAINT payment_providers_pkey PRIMARY KEY (id)
GO;


ALTER TABLE ONLY payment_tools
    ADD CONSTRAINT payment_tools_pkey PRIMARY KEY (id)
GO;


ALTER TABLE ONLY planeta_campaign_managers
    ADD CONSTRAINT planeta_managers_for_campaign_or_group_pkey PRIMARY KEY (manager_id, campaign_id)
GO;


ALTER TABLE ONLY printing_tag_relations
    ADD CONSTRAINT printing_tag_relations_pkey PRIMARY KEY (printing_id, tag_id)
GO;


ALTER TABLE ONLY printing_tags
    ADD CONSTRAINT printing_tags_pkey PRIMARY KEY (category_id)
GO;


ALTER TABLE ONLY printings
    ADD CONSTRAINT printings_pkey PRIMARY KEY (printing_id)
GO;


ALTER TABLE ONLY private_resources
    ADD CONSTRAINT private_resources_pkey PRIMARY KEY (hash)
GO;


ALTER TABLE ONLY product_photos
    ADD CONSTRAINT product_photos_pkey PRIMARY KEY (product_id, image_id)
GO;


ALTER TABLE ONLY profile_balances
    ADD CONSTRAINT profile_balances_pkey PRIMARY KEY (profile_id)
GO;


ALTER TABLE ONLY project_payment_methods
    ADD CONSTRAINT project_payment_methods_pkey PRIMARY KEY (project_type, payment_method_id)
GO;


ALTER TABLE ONLY project_payment_tools
    ADD CONSTRAINT project_payment_tools_pkey PRIMARY KEY (id)
GO;


ALTER TABLE ONLY promo_codes
    ADD CONSTRAINT promo_codes_2013_pkey PRIMARY KEY (code)
GO;


ALTER TABLE ONLY promo_codes_2013
    ADD CONSTRAINT promo_codes_pkey PRIMARY KEY (code)
GO;


ALTER TABLE ONLY purchase_share_info
    ADD CONSTRAINT purchase_share_info_pkey PRIMARY KEY (id)
GO;


ALTER TABLE ONLY push_tokens
    ADD CONSTRAINT push_tokens_pkey PRIMARY KEY (token)
GO;


ALTER TABLE ONLY quickstart_get_book
    ADD CONSTRAINT quickstart_get_book_pkey PRIMARY KEY (email, book_type, time_added)
GO;


ALTER TABLE ONLY rewards
    ADD CONSTRAINT rewards_pkey PRIMARY KEY (reward_id)
GO;


ALTER TABLE ONLY scheduled_blog_posts
    ADD CONSTRAINT scheduled_blog_posts_pkey PRIMARY KEY (profile_id, blog_post_id)
GO;


ALTER TABLE ONLY school_nko
    ADD CONSTRAINT school_nko_pkey PRIMARY KEY (school_nko_id)
GO;


ALTER TABLE ONLY seminar
    ADD CONSTRAINT seminar_pkey PRIMARY KEY (seminar_id)
GO;


ALTER TABLE ONLY seminar_registration
    ADD CONSTRAINT seminar_registration_pkey PRIMARY KEY (seminar_registration_id)
GO;


ALTER TABLE ONLY seminar_type
    ADD CONSTRAINT seminar_type_pkey PRIMARY KEY (seminar_type_id)
GO;


ALTER TABLE ONLY seminars
    ADD CONSTRAINT seminars_pkey PRIMARY KEY (seminar_id)
GO;


ALTER TABLE ONLY share_delivery_settings
    ADD CONSTRAINT share_delivery_settings_pkey PRIMARY KEY (share_id)
GO;


ALTER TABLE ONLY shares
    ADD CONSTRAINT shares_pkey PRIMARY KEY (share_id)
GO;


ALTER TABLE ONLY short_links
    ADD CONSTRAINT short_link_id PRIMARY KEY (short_link_id)
GO;


ALTER TABLE ONLY speaker
    ADD CONSTRAINT speaker_pkey PRIMARY KEY (speaker_id)
GO;


ALTER TABLE ONLY sponsors
    ADD CONSTRAINT sponsors_pkey PRIMARY KEY (alias)
GO;


ALTER TABLE ONLY stat_events
    ADD CONSTRAINT stat_events_pkey PRIMARY KEY (id)
GO;


ALTER TABLE ONLY subscribers
    ADD CONSTRAINT subscribers_pkey PRIMARY KEY (subscription_code, email)
GO;


ALTER TABLE ONLY sync_info_1c
    ADD CONSTRAINT sync_info_1c_pkey PRIMARY KEY (object_id, id_1c, object_type)
GO;


ALTER TABLE ONLY technobattle_registration
    ADD CONSTRAINT technobattle_registration_pkey PRIMARY KEY (registration_id)
GO;


ALTER TABLE ONLY tickets
    ADD CONSTRAINT tickets_pkey PRIMARY KEY (ticket_id)
GO;


ALTER TABLE ONLY top_special
    ADD CONSTRAINT top_special_pkey PRIMARY KEY (top_special_id, owner_profile_id, object_type_id)
GO;


ALTER TABLE ONLY topay_transactions
    ADD CONSTRAINT topay_transactions_pkey PRIMARY KEY (transaction_id)
GO;


ALTER TABLE ONLY bills_for_1c
    ADD CONSTRAINT topay_transcation_pkey PRIMARY KEY (topay_transaction_id)
GO;


ALTER TABLE ONLY two_can_payment
    ADD CONSTRAINT two_can_payment_pkey PRIMARY KEY (two_can_id)
GO;


ALTER TABLE ONLY bonus_orders
    ADD CONSTRAINT unique_bonus_order_id UNIQUE (bonus_order_id)
GO;


ALTER TABLE ONLY stat_events
    ADD CONSTRAINT unique_id UNIQUE (id)
GO;


ALTER TABLE ONLY user_addresses
    ADD CONSTRAINT user_addresses_pkey PRIMARY KEY (address_id)
GO;


ALTER TABLE ONLY user_callbacks
    ADD CONSTRAINT user_callbacks_pkey PRIMARY KEY (user_callback_id)
GO;


ALTER TABLE ONLY payment_card_bindings
    ADD CONSTRAINT user_card_bindings_pkey PRIMARY KEY (binding_id)
GO;


ALTER TABLE ONLY user_credentials
    ADD CONSTRAINT user_credentials_pk PRIMARY KEY (username, credential_type_code)
GO;


ALTER TABLE ONLY user_last_online_time
    ADD CONSTRAINT user_last_online_time_pkey PRIMARY KEY (profile_id)
GO;


ALTER TABLE ONLY user_login_info
    ADD CONSTRAINT user_login_info_pkey PRIMARY KEY (profile_id, user_ip_address, user_agent)
GO;


ALTER TABLE ONLY user_sources
    ADD CONSTRAINT user_sources_pkey PRIMARY KEY (profile_id)
GO;


ALTER TABLE ONLY users_private_info
    ADD CONSTRAINT users_private_info_email_key UNIQUE (email)
GO;


ALTER TABLE ONLY users_private_info
    ADD CONSTRAINT users_private_info_pkey PRIMARY KEY (user_id)
GO;


ALTER TABLE ONLY users_proposal
    ADD CONSTRAINT users_proposal_pkey PRIMARY KEY (id)
GO;


ALTER TABLE ONLY visitor_sessions
    ADD CONSTRAINT visitor_sessions_pkey PRIMARY KEY (id)
GO;


ALTER TABLE ONLY visitors
    ADD CONSTRAINT visitors_pkey PRIMARY KEY (id)
GO;


ALTER TABLE ONLY cached_video
    ADD CONSTRAINT "сached_video_pkey" PRIMARY KEY (cached_video_id)
GO;


CREATE INDEX auth_token_idx ON auth USING btree (auth_token)
GO;


CREATE INDEX bills_for_1c_status_idx ON bills_for_1c USING btree (status)
GO;


CREATE UNIQUE INDEX cached_video_url_idx ON cached_video USING btree (url)
GO;


CREATE UNIQUE INDEX campaign_alias_lower_idx ON campaigns USING btree (lower(campaign_alias))
GO;


CREATE INDEX campaign_orders_order_id_idx ON campaign_orders USING btree (order_id)
GO;


CREATE INDEX campaign_subscribe_campaign_id_idx ON campaign_subscribe USING btree (campaign_id)
GO;


CREATE INDEX campaign_subscribe_profile_id_campaign_id_idx ON campaign_subscribe USING btree (profile_id, campaign_id)
GO;


CREATE UNIQUE INDEX campaigns_campaign_alias ON campaigns USING btree (campaign_alias)
GO;


CREATE INDEX campaigns_creator_profile_id_idx ON campaigns USING btree (creator_profile_id)
GO;


CREATE INDEX campaigns_profile_id_idx ON campaigns USING btree (profile_id)
GO;


CREATE INDEX campaigns_sponsor_alias_idx ON campaigns USING btree (sponsor_alias)
GO;


CREATE INDEX card_number_mask_index ON payment_card_bindings USING btree (card_number_mask)
GO;


CREATE UNIQUE INDEX contractor_relations_idx ON contractor_relations USING btree (campaign_id)
GO;


CREATE INDEX contractors_inn_idx ON contractors USING btree (inn)
GO;


CREATE INDEX contractors_name_idx ON contractors USING btree (name)
GO;

CREATE INDEX deleted_index ON payment_card_bindings USING btree (deleted)
GO;

CREATE INDEX delivery_address_country_id ON delivery_address USING btree (country)
GO;

CREATE INDEX delivery_address_order_id ON delivery_address USING btree (order_id)
GO;

CREATE UNIQUE INDEX email_idx ON promo_codes_2013 USING btree (lower((email)::text))
GO;

CREATE UNIQUE INDEX email_notifications_external_message_id ON email_notifications USING btree (external_message_id)
GO;

CREATE INDEX investing_order_info_inn_idx ON investing_order_info USING btree (inn)
GO;

CREATE INDEX last_time_used_index ON payment_card_bindings USING btree (last_time_used)
GO;

CREATE INDEX linked_delivery_service_subject_type_subject_id_service_id_idx ON linked_delivery_service USING btree (subject_type, subject_id, service_id)
GO;

CREATE INDEX order_objects_object_id_idx ON order_objects USING btree (object_id)
GO;

CREATE INDEX order_objects_order_id_idx ON order_objects USING btree (order_id)
GO;

CREATE INDEX orders_buyer_id_payment_status_idx ON orders USING btree (buyer_id, payment_status)
GO;

CREATE INDEX orders_cancel_debit_transaction_id_idx ON orders USING btree (cancel_debit_transaction_id)
GO;

CREATE INDEX orders_credit_transaction_id_idx ON orders USING btree (credit_transaction_id)
GO;

CREATE UNIQUE INDEX orders_order_id_idx ON orders USING btree (order_id)
GO;

CREATE INDEX orders_payment_status_order_type_buyer_id_idx ON orders USING btree (payment_status, order_type, buyer_id)
GO;

CREATE INDEX orders_time_added_idx ON orders USING btree (time_added)
GO;

CREATE INDEX orders_topay_transaction_id ON orders USING btree (topay_transaction_id)
GO;

CREATE INDEX payment_errors_payment_error_type_id_time_added ON payment_errors USING btree (payment_error_type_id, time_added)
GO;

CREATE INDEX payment_errors_profile_id ON payment_errors USING btree (profile_id)
GO;

CREATE INDEX payment_provider_id_index ON payment_card_bindings USING btree (payment_provider_id)
GO;

CREATE INDEX payment_status_idx ON orders USING btree (payment_status)
GO;

CREATE INDEX ppt_payment_method_id_index ON project_payment_tools USING btree (payment_method_id)
GO;

CREATE INDEX ppt_project_type_index ON project_payment_tools USING btree (project_type)
GO;

CREATE UNIQUE INDEX printing_ord_num_unique ON printing_tag_relations USING btree (printing_id, order_num)
GO;

CREATE INDEX priority_index ON bonuses USING btree (priority)
GO;

CREATE UNIQUE INDEX promo_codes_2013_email_idx ON promo_codes USING btree (lower((email)::text))
GO;

CREATE INDEX push_tokens_profile_id_idx ON push_tokens USING btree (profile_id)
GO;

CREATE INDEX share_status_index ON shares USING btree (share_status)
GO;

CREATE INDEX shares_campaign_id_idx ON shares USING btree (campaign_id)
GO;

CREATE INDEX shares_share_status_idx ON shares USING btree (share_status)
GO;

CREATE UNIQUE INDEX short_link_idx ON short_links USING btree (short_link)
GO;

CREATE INDEX stat_events_affiliate_id_index ON stat_events USING btree (affiliate_id)
GO;

CREATE INDEX stat_events_city_index ON stat_events USING btree (city)
GO;

CREATE INDEX stat_events_country_index ON stat_events USING btree (country)
GO;

CREATE INDEX stat_events_object_id_index ON stat_events USING btree (object_id)
GO;

CREATE INDEX stat_events_referer_index ON stat_events USING btree (referer)
GO;

CREATE INDEX stat_events_source_id_index ON stat_events USING btree (source_id)
GO;

CREATE INDEX stat_events_type_index ON stat_events USING btree (type)
GO;

CREATE INDEX stat_events_visitor_id_index ON stat_events USING btree (visitor_id)
GO;

CREATE INDEX topay_transactions_order_id ON topay_transactions USING btree (order_id)
GO;

CREATE INDEX topay_transactions_profile_id_time_added ON topay_transactions USING btree (profile_id, time_added)
GO;

CREATE UNIQUE INDEX unique_campaign_tags_mnemonic_name ON campaign_tags USING btree (mnemonic_name)
GO;

CREATE UNIQUE INDEX unique_campaign_tags_name ON campaign_tags USING btree (tag_name)
GO;

CREATE INDEX unique_campaign_tags_sponsor_alias ON campaign_tags USING btree (sponsor_alias)
GO;

CREATE UNIQUE INDEX users_private_info_reg_code_uq ON users_private_info USING btree (reg_code)
GO;

CREATE UNIQUE INDEX users_private_info_username_key ON users_private_info USING btree (username)
GO;

CREATE INDEX visitor_sessions_create_date ON visitor_sessions USING btree (create_date)
GO;

CREATE INDEX visitor_sessions_visitor_id_create_date ON visitor_sessions USING btree (visitor_id, create_date)
GO;

CREATE RULE "_RETURN" AS
    ON SELECT TO jasper_aid_view DO INSTEAD  SELECT (date_trunc('day'::text, o.time_added))::date AS time_added,
    o.order_id,
    o.buyer_id,
    o.payment_status,
    o.total_price,
    oo.object_id
   FROM (orders o
     JOIN order_objects oo USING (order_id))
  WHERE ((o.payment_status = ANY (ARRAY[1, 2])) AND (o.order_type = 1))
  GROUP BY o.order_id, oo.object_id
  ORDER BY (date_trunc('day'::text, o.time_added))::date
GO;

CREATE RULE db_table_ignore_duplicate_inserts_login AS
    ON INSERT TO user_login_info
   WHERE (EXISTS ( SELECT 1
           FROM user_login_info
          WHERE (((user_login_info.profile_id = new.profile_id) AND ((user_login_info.user_ip_address)::text = (new.user_ip_address)::text)) AND (user_login_info.user_agent = new.user_agent)))) DO INSTEAD NOTHING
GO;

ALTER TABLE ONLY share_delivery_settings
    ADD CONSTRAINT share_delivery_settings_fk FOREIGN KEY (share_id) REFERENCES shares(share_id) ON UPDATE CASCADE ON DELETE CASCADE
GO;


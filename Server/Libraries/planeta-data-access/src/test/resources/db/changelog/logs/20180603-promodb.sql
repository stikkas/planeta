--liquibase formatted sql

--changeset stikkas:1

SET search_path = promodb, pg_catalog;

CREATE TABLE campus (
    campus_id bigint NOT NULL,
    campus_name character varying(256) NOT NULL,
    campus_short_name character varying(64) NOT NULL,
    campus_image_url text,
    campus_tag_id bigint NOT NULL
);

CREATE SEQUENCE seq_campus_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


CREATE SEQUENCE seq_techbattle_project_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


CREATE SEQUENCE seq_techbattle_voter_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


CREATE TABLE techbattle_project (
    project_id bigint NOT NULL,
    title character varying(256),
    small_image_url text,
    image_url text,
    consumers text,
    team text,
    city character varying(256),
    description text,
    profile_id bigint,
    video_src text,
    sharing_title text DEFAULT ''::text,
    sharing_image_url text DEFAULT ''::text,
    status integer DEFAULT 1,
    campaign_id bigint,
    campaign_alias text
);


CREATE TABLE techbattle_voters (
    voter_id bigint NOT NULL,
    user_id text,
    project_id bigint,
    vote_type character varying(2)
);


ALTER TABLE ONLY campus
    ADD CONSTRAINT campus_pkey PRIMARY KEY (campus_id);


ALTER TABLE ONLY techbattle_project
    ADD CONSTRAINT techbattle_project_campaign_alias_key UNIQUE (campaign_alias);


ALTER TABLE ONLY techbattle_project
    ADD CONSTRAINT techbattle_project_campaign_id_key UNIQUE (campaign_id);


ALTER TABLE ONLY techbattle_project
    ADD CONSTRAINT techbattle_project_pkey PRIMARY KEY (project_id);


ALTER TABLE ONLY techbattle_voters
    ADD CONSTRAINT techbattle_voters_pkey PRIMARY KEY (voter_id);


CREATE UNIQUE INDEX techbattle_project_idx ON techbattle_project USING btree (title);


CREATE INDEX techbattle_voters_project_id_idx ON techbattle_voters USING btree (project_id);


CREATE UNIQUE INDEX techbattle_voters_user_id_vote_type_idx ON techbattle_voters USING btree (user_id, vote_type);


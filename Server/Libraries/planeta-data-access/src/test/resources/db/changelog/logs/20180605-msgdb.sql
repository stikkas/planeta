--liquibase formatted sql
--changeset stikkas:1
CREATE SCHEMA msgdb;

SET search_path = msgdb, pg_catalog;

CREATE TABLE dialog_users (
    dialog_id bigint NOT NULL,
    user_id bigint NOT NULL,
    time_added timestamp without time zone DEFAULT now() NOT NULL
);

CREATE TABLE dialogs (
    dialog_id bigint NOT NULL,
    name character varying(256),
    creator_user_id bigint NOT NULL,
    last_message_id bigint NOT NULL,
    messages_count integer DEFAULT 0 NOT NULL,
    time_added timestamp without time zone DEFAULT now() NOT NULL
);

CREATE TABLE messages (
    dialog_id bigint NOT NULL,
    message_id bigint NOT NULL,
    user_id bigint NOT NULL,
    message_text text NOT NULL,
    message_text_html text NOT NULL,
    is_deleted boolean DEFAULT false NOT NULL,
    time_added timestamp without time zone DEFAULT now() NOT NULL
);

ALTER TABLE ONLY dialog_users
    ADD CONSTRAINT dialog_users_pkey PRIMARY KEY (dialog_id, user_id);

ALTER TABLE ONLY dialogs
    ADD CONSTRAINT dialogs_pkey PRIMARY KEY (dialog_id);

ALTER TABLE ONLY messages
    ADD CONSTRAINT messages_pkey PRIMARY KEY (dialog_id, message_id);


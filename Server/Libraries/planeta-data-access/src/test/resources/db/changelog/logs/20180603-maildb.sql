--liquibase formatted sql

--changeset stikkas:1 endDelimiter:\nGO;

SET search_path = maildb, pg_catalog
GO;

CREATE FUNCTION build_enums() RETURNS void
    LANGUAGE plpgsql
    AS $$
DECLARE
    attr RECORD;
    rec RECORD;
  BEGIN
    DELETE FROM maildb.enum_values;
    ALTER SEQUENCE maildb.seq_enum_value_id RESTART WITH 1;
    FOR attr IN (
      SELECT *
      FROM maildb.attributes
    ) LOOP
    IF attr.name = 'city' THEN
	  FOR rec IN (
		SELECT *
		FROM maildb.get_cities()
	  ) LOOP
	  INSERT INTO maildb.enum_values (
		enum_value_id,
		attribute_id,
		"value",
		text_value
	  ) VALUES (
		nextval('maildb.seq_enum_value_id'),
		attr.attribute_id,
		text(rec.city_id),
		rec.city_name_ru
	  );
      END LOOP;
	ELSIF attr.name = 'region' THEN
	  FOR rec IN (
		SELECT *
		FROM maildb.get_regions()
	  ) LOOP
	  INSERT INTO maildb.enum_values (
		enum_value_id,
		attribute_id,
		"value",
		text_value
	  ) VALUES (
		nextval('maildb.seq_enum_value_id'),
		attr.attribute_id,
		text(rec.region_id),
		rec.region_name_ru
	  );
      END LOOP;
    ELSIF attr.name = 'country' THEN
      FOR rec IN (
        SELECT *
        FROM maildb.get_countries()
      ) LOOP
      INSERT INTO maildb.enum_values (
        enum_value_id,
        attribute_id,
        "value",
        text_value
      ) VALUES (
        nextval('maildb.seq_enum_value_id'),
        attr.attribute_id,
        text(rec.country_id),
        rec.country_name_ru
      );
      END LOOP;
    ELSIF attr.name = 'campaign_ids' THEN
      FOR rec IN (
        SELECT t1.campaign_id,
          concat(case when t2.display_name != '' then t2.display_name else '<Без названия>' end, ' / ', case when t1.name != '' then t1.name else '<Без названия>' end) as name
        FROM commondb.campaigns t1
          LEFT JOIN maildb.get_profiles() t2 ON t2.profile_id = t1.creator_profile_id
      ) LOOP
      INSERT INTO maildb.enum_values (
        enum_value_id,
        attribute_id,
        "value",
        text_value
      ) VALUES (
        nextval('maildb.seq_enum_value_id'),
        attr.attribute_id,
        text(rec.campaign_id),
        rec.name
      );
      END LOOP;
    ELSIF attr.name = 'share_ids' THEN
      FOR rec IN (
        SELECT t1.share_id,
          concat(case when t3.display_name != '' then t3.display_name else '<Без названия>' end, ' / ', case when t2.name != '' then t2.name else '<Без названия>' end, ' / ', t1.name) as name
        FROM commondb.shares t1
          LEFT JOIN commondb.campaigns t2
            ON t1.campaign_id = t2.campaign_id
          LEFT JOIN maildb.get_profiles() t3 ON t3.profile_id = t2.creator_profile_id
      ) LOOP
      INSERT INTO maildb.enum_values (
        enum_value_id,
        attribute_id,
        "value",
        text_value
      ) VALUES (
        nextval('maildb.seq_enum_value_id'),
        attr.attribute_id,
        text(rec.share_id),
        rec.name
      );
      END LOOP;
    ELSIF attr.name = 'campaign_category_types' THEN
      FOR rec IN (
        SELECT *
        FROM commondb.campaign_tags ct
      ) LOOP
      INSERT INTO maildb.enum_values (
        enum_value_id,
        attribute_id,
        "value",
        text_value
      ) VALUES (
        nextval('maildb.seq_enum_value_id'),
        attr.attribute_id,
        text(rec.tag_id),
        concat(rec.tag_name)
      );
      END LOOP;
    ELSIF attr.name = 'subscribers' THEN
      FOR rec IN (
        SELECT *
        FROM maildb.get_profiles()
      ) LOOP
      INSERT INTO maildb.enum_values (
        enum_value_id,
        attribute_id,
        "value",
        text_value
      ) VALUES (
        nextval('maildb.seq_enum_value_id'),
        attr.attribute_id,
        text(rec.profile_id),
        concat(rec.display_name)
      );
      END LOOP;
    ELSIF attr.name = 'registration_source' THEN
      INSERT INTO maildb.enum_values (
        enum_value_id,
        attribute_id,
        "value",
        text_value
      ) VALUES (
                 nextval('maildb.seq_enum_value_id'),
                 attr.attribute_id,
                 '1',
                 'Вконтакте'
               ), (
                 nextval('maildb.seq_enum_value_id'),
                 attr.attribute_id,
                 '2',
                 'Поиск'
               ), (
                 nextval('maildb.seq_enum_value_id'),
                 attr.attribute_id,
                 '3',
                 'Facebook'
               ), (
                 nextval('maildb.seq_enum_value_id'),
                 attr.attribute_id,
                 '4',
                 'Другой сайт'
               ), (
                 nextval('maildb.seq_enum_value_id'),
                 attr.attribute_id,
                 '5',
                 'Прямые'
               ), (
                 nextval('maildb.seq_enum_value_id'),
                 attr.attribute_id,
                 '6',
                 'Офферная реклама VK'
               ), (
                 nextval('maildb.seq_enum_value_id'),
                 attr.attribute_id,
                 '7',
                 'MixUni'
               );
    END IF;
    END LOOP;
  END;
$$
GO;


CREATE FUNCTION collect_info() RETURNS void
    LANGUAGE plpgsql
    AS $$
DECLARE
  rec RECORD;
  rec2 RECORD;
BEGIN
  drop table if exists tmp_values;
  create temp table tmp_values(
    profile_id bigint,

    email TEXT NOT NULL,
    display_name TEXT,
    last_active TIMESTAMP(0) WITHOUT TIME ZONE,

    country TEXT,
    region TEXT,
    city TEXT,
    user_birth_date TIMESTAMP(0) WITHOUT TIME ZONE,
    projects_count INTEGER,
    balance NUMERIC,
    days_registered BIGINT,
    registration_source TEXT,
    CONSTRAINT tmp_values_pkey PRIMARY KEY(profile_id)
  );
  DROP INDEX IF EXISTS tmp_values_email;
  CREATE UNIQUE INDEX tmp_values_email ON tmp_values(email);

  insert into tmp_values
    SELECT profile.profile_id,
      private_info.email,
      profile.display_name,
      now() as last_active,
      profile.country_id,
      COALESCE((SELECT region_id FROM profiledb.city city WHERE city.city_id = profile.city_id),0) AS region_id,
      profile.city_id,
      profile.user_birth_date,
      profile.projects_count,
      balance.balance,
      date_part('days', now() - private_info.time_added) as days_registered,
      (case
       when enter_url ~ 'utm_source=mixuni' then '7'
       when referrer_url ~ 'vk.com' and enter_url !~ 'offers_adv' then '1'
       when referrer_url ~ 'vkontakte.ru' and enter_url !~ 'offers_adv' then '1'
       when referrer_url ~ 'vk.com' and enter_url ~ 'offers_adv' then '6'
       when referrer_url ~ 'vkontakte.ru' and enter_url ~ 'offers_adv' then '6'
       when referrer_url ~ 'google.com' then '2'
       when referrer_url ~ 'yandex.com' then '2'
       when referrer_url ~ 'go.mail.ru' then '2'
       when referrer_url ~ 'mail.ru' then '2'
       when referrer_url ~ 'facebook.com' then '3'
       when referrer_url = '' then '4'
       when referrer_url is NULL then '4'
       when referrer_url ~ 'planeta.ru' then '3'
       else '5' end) as registration_source
    FROM profiledb.profiles profile
      JOIN commondb.users_private_info  private_info ON private_info.user_id = profile.profile_id
      LEFT JOIN commondb.profile_balances balance ON balance.profile_id = profile.profile_id
      LEFT JOIN commondb.user_sources src ON src.profile_id = profile.profile_id
    WHERE profile_type_id = 1 --active user (email confirm)
          AND profile.status = 1 --user
          AND profile.is_receive_newsletters
          AND private_info.email IS NOT NULL
          AND private_info.email != ''
          AND EXISTS(select 1 from profiledb.users usr WHERE usr.profile_id = profile.profile_id)
  ;






  drop table if exists tmp_shares1;
  create temp table tmp_shares1(
    profile_id bigint,
    share_last_order TIMESTAMP WITHOUT TIME ZONE,
    share_max_price NUMERIC,
    share_count BIGINT,
    CONSTRAINT tmp_shares1_pkey PRIMARY KEY(profile_id)
  );

  insert into tmp_shares1
    SELECT t3.buyer_id, max(t3.time_added) as share_last_order, max(t4.price) as share_max_price, count(t4.price) as share_count
    FROM commondb.orders t3
      LEFT JOIN commondb.order_objects t4
        ON t3.order_id = t4.order_id
    WHERE t4.order_object_type = 1
          AND t3.payment_status = 1
    GROUP BY buyer_id;





  drop table if exists tmp_shares ;
  create temp table tmp_shares(
    profile_id bigint,
    share_ids TEXT[],
    campaign_category_types TEXT[],
    CONSTRAINT tmp_shares_pkey PRIMARY KEY(profile_id)
  );

  insert into tmp_shares
    SELECT ord.buyer_id ,array_agg(share.share_id)
    FROM commondb.orders ord
      JOIN commondb.order_objects object ON ord.order_id = object.order_id
      JOIN commondb.shares share ON share.share_id = object.object_id
    WHERE ord.payment_status = 1
    group by ord.buyer_id;


  update tmp_shares
  set campaign_category_types =
  (
    select array_agg(tag_id) campaign_category_types
    from(
          SELECT distinct ord.buyer_id , (select tag_id from commondb.campaign_tag_relations tr where tr.campaign_id = campaign.campaign_id order by order_num asc limit 1) as tag_id
          FROM commondb.orders ord
            JOIN commondb.order_objects object ON ord.order_id = object.order_id
            JOIN commondb.shares share ON share.share_id = object.object_id
            JOIN commondb.campaigns campaign ON campaign.campaign_id = share.campaign_id
          WHERE ord.payment_status = 1
                and ord.buyer_id = tmp_shares.profile_id
        ) as t1
    group by buyer_id);





  drop table if exists tmp_subscribers;
  create temp table tmp_subscribers(
    profile_id bigint,
    subscribers TEXT[],
    CONSTRAINT tmp_subscribers_pkey PRIMARY KEY(profile_id)
  );
  insert into tmp_subscribers
    SELECT profile_id, array_agg(subject_profile_id)
    from profiledb.profile_subscription
    GROUP BY profile_id;





  UPDATE maildb.values
  SET
    email                   = tmp_values.email,
    display_name            = tmp_values.display_name,
    last_active             = tmp_values.last_active,
    country                 = tmp_values.country,
    region                  = tmp_values.region,
    city                    = tmp_values.city,
    user_birth_date         = tmp_values.user_birth_date,
    projects_count          = tmp_values.projects_count,
    balance                 = tmp_values.balance,
    days_registered         = tmp_values.days_registered,
    registration_source     = tmp_values.registration_source,

    share_last_order        = tmp_shares1.share_last_order,
    share_max_price         = tmp_shares1.share_max_price,
    share_count             = coalesce(tmp_shares1.share_count,0),

    share_ids               = tmp_shares.share_ids,
    campaign_category_types = tmp_shares.campaign_category_types,

    subscribers             = tmp_subscribers.subscribers
  FROM tmp_values
    LEFT JOIN tmp_shares       on tmp_shares.profile_id = tmp_values.profile_id
    LEFT JOIN tmp_shares1      on tmp_shares1.profile_id = tmp_values.profile_id
    LEFT JOIN tmp_subscribers  on tmp_subscribers.profile_id = tmp_values.profile_id
  WHERE tmp_values.email = maildb.values.email
  ;

END;
$$
GO;

CREATE FUNCTION sources(OUT profile_id bigint, OUT registration_source text) RETURNS SETOF record
    LANGUAGE sql
    AS $$
  select profile_id,
(case
when enter_url ~ 'utm_source=mixuni' then '7'
when referrer_url ~ 'vk.com' and enter_url !~ 'offers_adv' then '1'
when referrer_url ~ 'vkontakte.ru' and enter_url !~ 'offers_adv' then '1'
when referrer_url ~ 'vk.com' and enter_url ~ 'offers_adv' then '6'
when referrer_url ~ 'vkontakte.ru' and enter_url ~ 'offers_adv' then '6'
when referrer_url ~ 'google.com' then '2'
when referrer_url ~ 'yandex.com' then '2'
when referrer_url ~ 'go.mail.ru' then '2'
when referrer_url ~ 'mail.ru' then '2'
when referrer_url ~ 'facebook.com' then '3'
when referrer_url = '' then '4'
when referrer_url IS NULL then '4'
when referrer_url ~ 'planeta.ru' then '3'
else '5' end) as registration_source
from commondb.user_sources
$$
GO;

CREATE TABLE attributes (
    attribute_id integer NOT NULL,
    name text,
    type integer,
    text_name text
)
GO;


CREATE TABLE campaign_category_types (
    value integer NOT NULL,
    name text,
    short_name text
)
GO;


CREATE TABLE campaign_stats (
    campaign_id bigint NOT NULL,
    addresses bigint,
    sent bigint,
    opened bigint,
    failed bigint,
    unsubscribed bigint,
    abuse bigint,
    domain_name character varying(128) DEFAULT ''::character varying NOT NULL
)
GO;


CREATE TABLE campaigns (
    campaign_id bigint NOT NULL,
    name text,
    template_id bigint,
    filter_list_id bigint,
    text text,
    message_from text,
    subject text,
    date_confirmed timestamp without time zone,
    send_to_all boolean DEFAULT false,
    time_to_send timestamp without time zone,
    date_sent timestamp without time zone
)
GO;

CREATE TABLE enum_values (
    enum_value_id integer NOT NULL,
    attribute_id integer NOT NULL,
    value text,
    text_value text
)
GO;


CREATE TABLE filter_lists (
    filter_list_id bigint NOT NULL,
    name text,
    data bytea
)
GO;

CREATE TABLE loaded_values (
    email text NOT NULL,
    filter_list_id bigint,
    loaded_params_string text
)
GO;

CREATE TABLE messages (
    mailer_message_id bigint NOT NULL,
    message_id text,
    user_id bigint,
    campaign_id bigint,
    date_sent timestamp without time zone,
    is_open boolean,
    is_failed boolean,
    is_unsubscribed boolean DEFAULT false,
    is_abuse boolean DEFAULT false,
    attempt integer DEFAULT 0
)
GO;

CREATE SEQUENCE seq_attribute_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
GO;

CREATE SEQUENCE seq_campaign_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
GO;

CREATE SEQUENCE seq_enum_value_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
GO;


CREATE SEQUENCE seq_filter_list_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
GO;


CREATE SEQUENCE seq_mailer_message_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
GO;



CREATE SEQUENCE seq_template_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
GO;


CREATE SEQUENCE seq_user_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
GO;


CREATE TABLE templates (
    template_id bigint NOT NULL,
    name text,
    text text
)
GO;

CREATE TABLE users (
    user_id bigint NOT NULL,
    email text,
    display_name text,
    statistics text,
    unsubscribed boolean DEFAULT false NOT NULL,
    hash text
)
GO;

CREATE TABLE users_bak_20160518 (
    user_id bigint,
    email text,
    display_name text,
    statistics text,
    unsubscribed boolean,
    hash text
)
GO;

CREATE TABLE "values" (
    email text NOT NULL,
    display_name text,
    last_active timestamp(0) without time zone,
    country text,
    region text,
    city text,
    user_birth_date timestamp(0) without time zone,
    balance numeric,
    subscribers text[],
    share_last_order timestamp without time zone,
    share_max_price numeric,
    share_count bigint DEFAULT 0 NOT NULL,
    campaign_category_types text[],
    campaign_ids text[],
    share_ids text[],
    days_registered bigint,
    registration_source text,
    projects_count integer DEFAULT 0
)
GO;

ALTER TABLE ONLY attributes
    ADD CONSTRAINT attributes_pkey PRIMARY KEY (attribute_id)
GO;
ALTER TABLE ONLY campaign_category_types
    ADD CONSTRAINT campaign_category_types_pkey PRIMARY KEY (value)
GO;

ALTER TABLE ONLY campaign_stats
    ADD CONSTRAINT campaign_stats_pkey PRIMARY KEY (campaign_id, domain_name)
GO;

ALTER TABLE ONLY campaigns
    ADD CONSTRAINT campaigns_pkey PRIMARY KEY (campaign_id)
GO;

ALTER TABLE ONLY enum_values
    ADD CONSTRAINT enum_values_pkey PRIMARY KEY (enum_value_id)
GO;

ALTER TABLE ONLY messages
    ADD CONSTRAINT messages_pkey PRIMARY KEY (mailer_message_id)
GO;

ALTER TABLE ONLY filter_lists
    ADD CONSTRAINT saved_filters_pkey PRIMARY KEY (filter_list_id)
GO;

ALTER TABLE ONLY templates
    ADD CONSTRAINT templates_pkey PRIMARY KEY (template_id)
GO;

ALTER TABLE ONLY users
    ADD CONSTRAINT users_pkey PRIMARY KEY (user_id)
GO;

ALTER TABLE ONLY "values"
    ADD CONSTRAINT values_pkey PRIMARY KEY (email)
GO;

CREATE INDEX campaigns_date_sent_idx ON campaigns USING btree (date_sent)
GO;

CREATE INDEX campaigns_time_to_send_idx ON campaigns USING btree (time_to_send)
GO;

CREATE INDEX enum_values_attribute_id_idx ON enum_values USING btree (attribute_id)
GO;

CREATE UNIQUE INDEX messages_idx ON messages USING btree (user_id, campaign_id)
GO;

CREATE INDEX messages_idx1 ON messages USING btree (campaign_id, message_id)
GO;

CREATE INDEX messages_idx2 ON messages USING btree (date_sent)
GO;

CREATE INDEX messages_idx3 ON messages USING btree (message_id)
GO;

CREATE INDEX users_email_idx ON users USING btree (email)
GO;



package ru.planeta.dao.commondb

import org.junit.Test
import org.springframework.beans.factory.annotation.Autowired
import ru.planeta.model.common.school.Seminar
import ru.planeta.model.enums.SeminarType

import java.util.Date

import org.junit.Assert.*
import org.junit.runner.RunWith
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.transaction.annotation.Transactional

@RunWith(SpringRunner::class)
@Transactional
@ContextConfiguration(locations = ["classpath:applicationContext-common-test.xml"])
class TestSeminarDao {

    @Autowired
    lateinit var seminarDAO: SeminarDAO

    @Test
    fun testPartner() {
        val speakersIds = longArrayOf(1, 2)
        val partnersIds = longArrayOf(1, 2, 3)

        val seminar = getCustomSeminar("Лепим пельмеши")
        seminar.speakersIds = speakersIds
        seminar.partnersIds = partnersIds

        seminarDAO.insert(seminar)
        val selectedSeminar = seminarDAO.selectByPrimaryKey(seminar.seminarId)
        assertNotNull(selectedSeminar)

        isSeminarOk(seminar, selectedSeminar)

        val selectedSpeakersIds = selectedSeminar.speakersIds
        assertEquals(selectedSpeakersIds!!.size.toLong(), speakersIds.size.toLong())

        val selectedPartnersIds = selectedSeminar.partnersIds
        assertEquals(selectedPartnersIds!!.size.toLong(), partnersIds.size.toLong())
    }

    @Test
    fun testSeminar() {
//
//        val seminar = getCustomSeminar("Семинар 1")
//        val seminar2 = getCustomSeminar("Семинар 2")
//        val seminar3 = getCustomSeminar("Семинар 3")
//
//        seminarDAO!!.insert(seminar)
//        seminarDAO.insert(seminar2)
//        seminarDAO.insert(seminar3)
//
//        val selectedSeminar = seminarDAO.selectByPrimaryKey(seminar.seminarId)
//        assertNotNull(selectedSeminar)
//        isSeminarOk(seminar, selectedSeminar)
//
//        selectedSeminar.setIsShownOnMainPage(true)
//        seminarDAO.updateByPrimaryKey(selectedSeminar)
//        val selectedSeminar2 = seminarDAO.selectByPrimaryKey(selectedSeminar.seminarId)
//        isSeminarOk(selectedSeminar, selectedSeminar2)
//
//        val seminarIds = arrayOf<Long>(seminar2.seminarId, seminar3.seminarId)
//        val seminars = seminarDAO.selectByIdList(Arrays.asList(*seminarIds))
//        for (tempSeminar in seminars) {
//            assertFalse(tempSeminar.getIsShownOnMainPage())
//        }
//
//        val emptySeminar = SeminarExample()
//        val falseMainPageSeminar = Seminar()
//        falseMainPageSeminar.setIsShownOnMainPage(false)
//        seminarDAO.updateByExampleSelective(falseMainPageSeminar, emptySeminar)
//
//        val seminarIds2 = arrayOf<Long>(seminar.seminarId, seminar2.seminarId, seminar3.seminarId)
//        val seminars2 = seminarDAO.selectByIdList(Arrays.asList(*seminarIds2))
//        for (tempSeminar in seminars2) {
//            assertFalse(tempSeminar.getIsShownOnMainPage())
//        }
    }

    private fun isSeminarOk(seminar: Seminar, selectedSeminar: Seminar) {
//        assertEquals(seminar.getName(), selectedSeminar.getName())
//        assertEquals(seminar.timeStart, selectedSeminar.timeStart)
//        assertEquals(seminar.timeAdded, selectedSeminar.timeAdded)
//        assertEquals(seminar.cityId, selectedSeminar.cityId)
//        assertEquals(seminar.tagId, selectedSeminar.tagId)
//        assertEquals(seminar.getUrl(), selectedSeminar.getUrl())
//        assertEquals(seminar.participantsLimit, selectedSeminar.participantsLimit)
//        assertEquals(seminar.getBackgroundImage(), selectedSeminar.getBackgroundImage())
//        assertEquals(seminar.getShortDescription(), selectedSeminar.getShortDescription())
//        assertEquals(seminar.getDescription(), selectedSeminar.getDescription())
//        assertEquals(seminar.getConditions(), selectedSeminar.getConditions())
//        assertEquals(seminar.getResults(), selectedSeminar.getResults())
//        assertEquals(seminar.seminarType, selectedSeminar.seminarType)
//        assertEquals(seminar.formatSelection, selectedSeminar.formatSelection)
//        assertEquals(seminar.getIsShownOnMainPage(), selectedSeminar.getIsShownOnMainPage())
    }

    private fun getCustomSeminar(name: String): Seminar {
        return Seminar.builder
                .name(name)
                .timeStart(Date())
                .timeAdded(Date())
                .timeRegistrationEnd(Date())
                .cityId(1.toLong())
                .tagId(1.toLong())
                .url("http://google.ru")
                .participantsLimit(5)
                .backgroundImage("https://static.planeta.ru/images/project/bg_gradient.jpg")
                .shortDescription("Краткое описание")
                .description("Полное описание")
                .conditions("12+ лет")
                .results("Победила дружба")
                .seminarType(SeminarType.SOLO)
                .formatSelection(true)
                .isShownOnMainPage(false)
                .build()
    }
}

package ru.planeta.dao.profiledb


import org.junit.Assert.*
import org.junit.Ignore
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.transaction.annotation.Transactional
import ru.planeta.TestHelper
import ru.planeta.TestHelper.TEST_PROFILE_ID
import ru.planeta.model.profile.media.AudioTrack
import java.util.*

/**
 * @author a.savanovich
 * Date: 27.10.11
 */
@RunWith(SpringRunner::class)
@Transactional
@ContextConfiguration(locations = ["classpath:applicationContext-common-test.xml"])
class TestAudioTrackDAO {

    @Autowired
    lateinit var audioTrackDAO: AudioTrackDAO

    @Test
    fun testAudioTrackDAO() {


        var artistName = "artist name"
        var trackName = "track name"
        var trackUrl = "track url"

        val audioTrack = AudioTrack()
        audioTrack.profileId = TEST_PROFILE_ID
        audioTrack.uploaderProfileId = TEST_PROFILE_ID
        audioTrack.artistName = artistName
        audioTrack.authors = "authors"
        audioTrack.lyrics = "lyrics"
        //			audioTrack.setTags("tags");
        audioTrack.trackDuration = 1991
        audioTrack.trackName = trackName
        audioTrack.trackUrl = trackUrl
        audioTrack.listeningsCount = 0
        audioTrack.downloadsCount = 0
        audioTrack.albumTrackNumber = 1
        //			audioTrack.setLists(new int[]{0, 10, 29});

        audioTrackDAO!!.insert(audioTrack)


        var list = audioTrackDAO.selectAudioTracks(TEST_PROFILE_ID, 0, 10, "artis")
        assertEquals(list.size.toLong(), 1)

        list = audioTrackDAO.selectAudioTracks(TEST_PROFILE_ID, 0, 10, "blablabla")
        assertEquals(list.size.toLong(), 0)

        list = audioTrackDAO.selectAudioTracks(TEST_PROFILE_ID, 0, 10, null)
        assertEquals(list.size.toLong(), 1)

        val inserted = audioTrackDAO.selectAudioTracks(TEST_PROFILE_ID, 0, 10)[0]
        assertEquals(TEST_PROFILE_ID, inserted.profileId)
        assertEquals(TEST_PROFILE_ID, inserted.uploaderProfileId)
        assertEquals(artistName, inserted.artistName)
        assertEquals(audioTrack.albumTrackNumber.toLong(), inserted.albumTrackNumber.toLong())
        //check directory counts

        //Check updating
        artistName = "test artist"
        trackName = "test track"
        trackUrl = "test track url"

        inserted.artistName = artistName
        inserted.trackName = trackName
        inserted.trackUrl = trackUrl
        audioTrackDAO.update(inserted)

        var updated = audioTrackDAO.selectAudioTrackById(TEST_PROFILE_ID, inserted.trackId)
        assertNotNull(updated)
        assertEquals(TEST_PROFILE_ID, updated.profileId)
        assertEquals(artistName, updated.artistName)
        assertEquals(trackName, updated.trackName)
        assertEquals(trackUrl, updated.trackUrl)


        //update lists
        updated = audioTrackDAO.selectAudioTrackById(TEST_PROFILE_ID, inserted.trackId)
        assertNotNull(updated)
        assertEquals(TEST_PROFILE_ID, updated.profileId)

        //updating listenings and downloads count
        val listeningsCount = 5
        val downloadsCount = 6
        audioTrackDAO.updateListeningsCount(audioTrack.profileId!!, audioTrack.trackId, listeningsCount)
        audioTrackDAO.updateDownloadsCount(audioTrack.profileId!!, audioTrack.trackId, downloadsCount)
        val updatedCountsAudioTrack = audioTrackDAO.selectAudioTrackById(audioTrack.profileId!!, audioTrack.trackId)
        assertTrue(updatedCountsAudioTrack.listeningsCount == audioTrack.listeningsCount + listeningsCount)
        assertTrue(updatedCountsAudioTrack.downloadsCount == audioTrack.downloadsCount + downloadsCount)

        //selectin by audio album
        var tracks = audioTrackDAO.selectAudioTracksByAlbumId(TEST_PROFILE_ID, 0, 0, 10)
        assertTrue(tracks.size == 1)

        tracks = audioTrackDAO.selectAudioTrackByUploaderTrackId(audioTrack.profileId!!, audioTrack.uploaderTrackId)
        assertTrue(tracks.size > 0)


        tracks = audioTrackDAO.selectAudioByListTrackIds(audioTrack.profileId!!, createLongListFromAudioList(tracks))
        assertEquals(1, tracks.size.toLong())

        //deleting
        audioTrackDAO.delete(TEST_PROFILE_ID, updated.trackId)
        assertNull(audioTrackDAO.selectAudioTrackById(audioTrack.profileId!!, audioTrack.trackId))


    }

    private fun createLongListFromAudioList(track: List<AudioTrack>): List<Long> {
        val list = ArrayList<Long>()
        for (post in track) {
            list.add(post.trackId)
        }
        return list
    }

    @Ignore
    @Test
    fun authorInfoTest() {
        val profile = TestHelper.newProfile()

        val audioTrack = AudioTrack()
        audioTrack.artistName = "artist"
        audioTrack.authors = "authors"
        audioTrack.lyrics = "lyrics"
        //			audioTrack.setTags("tags");
        audioTrack.trackDuration = 1991
        audioTrack.trackName = "trackname"
        audioTrack.trackUrl = "track url"
        audioTrack.listeningsCount = 0
        audioTrack.downloadsCount = 0
        //			audioTrack.setLists(new int[]{0, 10, 29});
        audioTrack.profileId = profile.profileId
        audioTrack.authorProfileId = profile.profileId

        audioTrackDAO!!.insert(audioTrack)
        val inserted = audioTrackDAO.selectAudioTrackById(audioTrack.profileId!!, audioTrack.trackId)
        assertNotNull(inserted)
    }
}

package ru.planeta.dao.commondb


import org.assertj.core.api.Assertions.assertThat
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.transaction.PlatformTransactionManager
import org.springframework.transaction.TransactionDefinition
import org.springframework.transaction.annotation.Transactional
import org.springframework.transaction.support.DefaultTransactionDefinition
import java.math.BigDecimal
import kotlin.concurrent.thread


/**
 *
 * Created by kostiagn on 21.08.2015.
 */
@RunWith(SpringRunner::class)
@Transactional
@ContextConfiguration(locations = ["classpath:applicationContext-common-test.xml"])
class TestProfileBalanceSelectForUpdate {
    @Autowired
    lateinit var profileBalanceDAO: ProfileBalanceDAO

    @Autowired
    lateinit var txManager: PlatformTransactionManager

    private val firstTransactionStart = Object()
    private val secondTransactionStart = Object()
    private val PROFILE_ID = -12345L

    @Test
    fun simple() {
        profileBalanceDAO.delete(PROFILE_ID)
        profileBalanceDAO.insert(PROFILE_ID)
        assertThat(profileBalanceDAO.select(PROFILE_ID)).isEqualTo(BigDecimal(0).setScale(4))
    }

    @Test
    fun testSelectForUpdate1() {
        val def = DefaultTransactionDefinition()
        def.name = "SomeTxName"
        def.propagationBehavior = TransactionDefinition.PROPAGATION_REQUIRED

        val thread1 = thread(start = false) {
            val status = txManager.getTransaction(def)
            try {
//                synchronized(firstTransactionStart) {
//                    firstTransactionStart.notify()
//                }
//                synchronized(secondTransactionStart) {
//                    secondTransactionStart.wait()
//                }
                var balance = profileBalanceDAO.selectForUpdate(PROFILE_ID)
                assertThat(balance).isNotNull()
                Thread.sleep(500)
                balance = balance.add(BigDecimal.TEN)
                profileBalanceDAO.update(PROFILE_ID, balance)
                txManager.commit(status)
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

        val thread2 = thread(start = false) {
            val status = txManager.getTransaction(def)
            try {
//                synchronized(secondTransactionStart) {
//                    secondTransactionStart.notify()
//                }
                var balance = profileBalanceDAO.selectForUpdate(PROFILE_ID)
                Thread.sleep(500)
                balance = balance.add(BigDecimal(100))
                profileBalanceDAO.update(PROFILE_ID, balance)
                txManager.commit(status)
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

//        synchronized(firstTransactionStart) {
            thread1.start()
//        }

        thread2.start()

        thread1.join()
        thread2.join()

        val status = txManager.getTransaction(def)
        assertThat(profileBalanceDAO.select(PROFILE_ID)).isEqualTo(110)
        profileBalanceDAO.delete(PROFILE_ID)
        txManager.commit(status)

    }
}


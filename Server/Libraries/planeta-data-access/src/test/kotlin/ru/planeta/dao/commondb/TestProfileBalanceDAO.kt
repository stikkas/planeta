package ru.planeta.dao.commondb

import org.junit.Test
import org.springframework.beans.factory.annotation.Autowired


import java.math.BigDecimal

import org.junit.Assert.assertEquals
import org.junit.Assert.assertNull
import org.junit.runner.RunWith
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.transaction.annotation.Transactional

/**
 * Class TestProfileBalanceDAO
 *
 * @author a.tropnikov
 */
@RunWith(SpringRunner::class)
@Transactional
@ContextConfiguration(locations = ["classpath:applicationContext-common-test.xml"])
class TestProfileBalanceDAO {

    @Autowired
    lateinit var profileBalanceDAO: ProfileBalanceDAO

    @Test
    fun test() {
        var balance = BigDecimal.ZERO
        profileBalanceDAO!!.insert(TEST_PROFILE_ID)
        assertEquals(profileBalanceDAO.select(TEST_PROFILE_ID).compareTo(balance).toLong(), 0)

        balance = BigDecimal.TEN
        profileBalanceDAO.update(TEST_PROFILE_ID, balance)
        assertEquals(profileBalanceDAO.select(TEST_PROFILE_ID).compareTo(balance).toLong(), 0)
        assertEquals(profileBalanceDAO.selectForUpdate(TEST_PROFILE_ID).compareTo(balance).toLong(), 0)

        profileBalanceDAO.delete(TEST_PROFILE_ID)
        assertNull(profileBalanceDAO.select(TEST_PROFILE_ID))

    }

    companion object {

        private val TEST_PROFILE_ID: Long = 1000000000
    }
}

package ru.planeta.dao.profiledb


import org.junit.Assert.*
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.transaction.annotation.Transactional
import ru.planeta.TestHelper.TEST_PROFILE_ID
import ru.planeta.model.enums.BlockSettingType
import ru.planeta.model.enums.PermissionLevel
import ru.planeta.model.profile.ProfileBlockSetting

/**
 * Class TestProfileBlockSettingDAO
 *
 * @author a.tropnikov
 */
@RunWith(SpringRunner::class)
@Transactional
@ContextConfiguration(locations = ["classpath:applicationContext-common-test.xml"])
class TestProfileBlockSettingDAO {

    @Autowired
    lateinit var profileBlockSettingDAO: ProfileBlockSettingDAO

    @Test
    fun test() {


        val setting = ProfileBlockSetting()
        setting.profileId = TEST_PROFILE_ID
        setting.blockSettingType = BlockSettingType.VIDEO
        setting.viewPermission = PermissionLevel.NONE
        setting.commentPermission = PermissionLevel.NONE
        profileBlockSettingDAO!!.insert(setting)

        val selected = profileBlockSettingDAO.select(TEST_PROFILE_ID, setting.blockSettingType!!)
        assertNotNull(selected)
        assertSettingEquals(selected, setting)

        setting.viewPermission = PermissionLevel.FRIENDS
        setting.commentPermission = PermissionLevel.EVERYBODY
        profileBlockSettingDAO.update(setting)

        val updated = profileBlockSettingDAO.select(TEST_PROFILE_ID, setting.blockSettingType!!)
        assertSettingEquals(updated, setting)

        val blockSettingList = profileBlockSettingDAO.selectByProfileId(TEST_PROFILE_ID)
        assertNotNull(blockSettingList)
        assertEquals(1, blockSettingList.size.toLong())

        profileBlockSettingDAO.delete(TEST_PROFILE_ID, setting.blockSettingType!!)
        assertNull(profileBlockSettingDAO.select(TEST_PROFILE_ID, setting.blockSettingType!!))

    }

    private fun assertSettingEquals(expected: ProfileBlockSetting, actual: ProfileBlockSetting) {
        assertEquals(expected.profileId, actual.profileId)
        assertEquals(expected.blockSettingType, actual.blockSettingType)
        assertEquals(expected.viewPermission, actual.viewPermission)
        assertEquals(expected.commentPermission, actual.commentPermission)
    }
}

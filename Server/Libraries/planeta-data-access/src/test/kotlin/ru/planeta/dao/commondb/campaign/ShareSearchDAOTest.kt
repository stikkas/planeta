package ru.planeta.dao.commondb.campaign

import org.junit.Test
import org.springframework.beans.factory.annotation.Autowired


import java.util.Arrays

import org.junit.Assert.*
import org.junit.runner.RunWith
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.transaction.annotation.Transactional
import ru.planeta.dao.commondb.ShareSearchDAO

/**
 *
 * Created by asavan on 02.08.2017.
 */
@RunWith(SpringRunner::class)
@Transactional
@ContextConfiguration(locations = ["classpath:applicationContext-common-test.xml"])
class ShareSearchDAOTest {
    @Autowired
    lateinit var shareSearchDAO: ShareSearchDAO

    @Test
    @Throws(Exception::class)
    fun select() {
        val res = shareSearchDAO.select(Arrays.asList(1L, 2L, 666L))
        assertNotNull(res)
    }
}

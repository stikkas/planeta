package ru.planeta.dao.commondb.campaign

import org.junit.Test
import org.springframework.beans.factory.annotation.Autowired
import ru.planeta.model.common.campaign.Campaign
import ru.planeta.model.common.campaign.CampaignTag

import org.junit.Assert.*
import org.junit.runner.RunWith
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.transaction.annotation.Transactional
import ru.planeta.TestHelper
import ru.planeta.TestHelper.TEST_PROFILE_ID
import ru.planeta.dao.commondb.CampaignDAO
import ru.planeta.dao.commondb.CampaignTagDAO

/**
 * User: m.shulepov
 */
@RunWith(SpringRunner::class)
@Transactional
@ContextConfiguration(locations = ["classpath:applicationContext-common-test.xml"])
class TestCampaignTagDAO {

    @Autowired
    lateinit var campaignDAO: CampaignDAO

    @Autowired
    lateinit var campaignTagDAO: CampaignTagDAO

    @Test
    fun testSelectAll() {
        val allTags = campaignTagDAO.selectAll()
        assertNotNull(allTags)
        assertTrue(allTags.size > 0)
    }

    @Test
    fun testSelectUpdateTagsByCampaign() {
        val testProfileId = TEST_PROFILE_ID

        val campaign = TestHelper.createCampaign(testProfileId)
        assertTrue(campaign.campaignId > 0)

        var selectedTags = campaignTagDAO.selectTagsByCampaign(campaign.campaignId)
        assertNotNull(selectedTags)
        assertEquals(0, selectedTags.size.toLong())

        val allTags = campaignTagDAO.selectAll()

        val tagsSize = allTags.size / 2
        val newTags = allTags.subList(0, tagsSize)
        campaignTagDAO.updateTags(campaign.campaignId, newTags)

        selectedTags = campaignTagDAO.selectTagsByCampaign(campaign.campaignId)
        assertNotNull(selectedTags)
        assertEquals(tagsSize.toLong(), selectedTags.size.toLong())
        for (i in selectedTags.indices) {
            assertEquals(selectedTags[i].id.toLong(), newTags[i].id.toLong())
        }
    }

    @Test
    fun testCampaignTagsRelation() {
        val testProfileId = TEST_PROFILE_ID

        var campaign: Campaign? = TestHelper.createCampaign(testProfileId)
        assertTrue(campaign!!.campaignId > 0)
        assertNotNull(campaign.tags)
        assertEquals(0, campaign.tags.size.toLong())

        val allTags = campaignTagDAO!!.selectAll()

        val tagsSize = allTags.size / 2
        campaignTagDAO.updateTags(campaign.campaignId, allTags.subList(0, tagsSize))

        campaign = campaignDAO!!.selectCampaignById(campaign.campaignId)
        assertNotNull(campaign!!.tags)
        assertEquals(tagsSize.toLong(), campaign.tags.size.toLong())

    }

    @Test
    fun testInsertUpdateCampaignTags() {
        val campaignTag = CampaignTag()

        campaignTag.mnemonicName = "TEstMnemName"
        campaignTag.name = "TestName"
        campaignTag.engName = "TestName"
        campaignTag.preferredOrder = 213
        campaignTag.sponsorAlias = "TestSponsorAlias"
        campaignTag.warnText = "TestWarnText"

        campaignTagDAO!!.insert(campaignTag)

        assertNotEquals(0, campaignTag.id.toLong())

        var selected = campaignTagDAO.select(campaignTag.id.toLong())
        assertNotNull(selected)

        assertEquals(campaignTag.name, selected.name)
        assertEquals(campaignTag.mnemonicName, selected.mnemonicName)
        assertEquals(campaignTag.sponsorAlias, selected.sponsorAlias)
        assertEquals(campaignTag.warnText, selected.warnText)
        assertEquals(campaignTag.preferredOrder.toLong(), selected.preferredOrder.toLong())

        campaignTag.mnemonicName = "TEstMnemName2"
        campaignTag.name = "TestName2"
        campaignTag.engName = "TestName2"
        campaignTag.preferredOrder = 214
        campaignTag.sponsorAlias = "TestSponsorAlias2"
        campaignTag.warnText = "TestWarnText2"

        campaignTagDAO.update(campaignTag)

        selected = campaignTagDAO.select(campaignTag.id.toLong())
        assertNotNull(selected)

        assertEquals(campaignTag.name, selected.name)
        assertEquals(campaignTag.mnemonicName, selected.mnemonicName)
        assertEquals(campaignTag.sponsorAlias, selected.sponsorAlias)
        assertEquals(campaignTag.warnText, selected.warnText)
        assertEquals(campaignTag.preferredOrder.toLong(), selected.preferredOrder.toLong())
    }
}

package ru.planeta.dao.concertdb

import org.junit.Test
import org.springframework.beans.factory.annotation.Autowired
import ru.planeta.model.concert.Hall

import org.junit.Assert.assertEquals
import org.junit.Ignore
import org.junit.runner.RunWith
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.transaction.annotation.Transactional

/**
 * Created with IntelliJ IDEA.
 * User: michail
 * Date: 14.10.16
 * Time: 13:24
 */
@Ignore
@RunWith(SpringRunner::class)
@Transactional
@ContextConfiguration(locations = ["classpath:applicationContext-common-test.xml"])
class HallDAOTest {

    @Autowired
    lateinit var hallDAO: HallDAO

    private fun createHall(): Hall {
        val hall = Hall()
        hall.title = "testTitle"
        hall.description = "testDescr"
        hall.address = "testAddr"
        hall.schemeUrl = "http://ya.ru"
        return hall
    }

    private fun cloneHall(hall: Hall): Hall {
        val clone = Hall()
        clone.hallId = hall.hallId
        clone.title = hall.title
        clone.description = hall.description
        clone.address = hall.address
        clone.schemeUrl = hall.schemeUrl
        return clone
    }

    private fun assertHallEquals(actual: Hall, expected: Hall) {
        assertEquals(actual.hallId, expected.hallId)
        assertEquals(actual.title, expected.title)
        assertEquals(actual.description, expected.description)
        assertEquals(actual.schemeUrl, expected.schemeUrl)
        assertEquals(actual.address, expected.address)
    }

    @Test
    fun testInsertAndSelect() {
        var actual = createHall()
        hallDAO.insert(actual)
        val expected = cloneHall(actual)
        actual = hallDAO.select(actual.hallId)
        assertHallEquals(actual, expected)
    }

    @Test
    fun testInsertAndUpdate() {
        var actual = createHall()
        hallDAO.insert(actual)
        actual.title = "test2Title"
        actual.description = "test2Descr"
        actual.address = "test2Addr"
        actual.schemeUrl = "http://r0.ru"
        val expected = cloneHall(actual)
        hallDAO.update(actual)
        actual = hallDAO.select(actual.hallId)
        assertHallEquals(actual, expected)
    }
}

package ru.planeta.dao.profiledb

import org.junit.Ignore
import org.junit.Test
import org.springframework.beans.factory.annotation.Autowired
import ru.planeta.model.profile.broadcast.Broadcast
import ru.planeta.model.profile.broadcast.BroadcastBackersTargeting
import ru.planeta.model.profile.broadcast.BroadcastGeoTargeting
import ru.planeta.model.profile.broadcast.BroadcastStream
import ru.planeta.model.profile.broadcast.enums.BroadcastRecordStatus
import ru.planeta.model.profile.broadcast.enums.BroadcastStatus
import ru.planeta.model.profile.broadcast.enums.BroadcastSubscriptionStatus
import ru.planeta.model.profile.broadcast.enums.BroadcastType


import java.math.BigDecimal
import java.util.Calendar
import java.util.Date

import org.junit.Assert.*
import org.junit.runner.RunWith
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.transaction.annotation.Transactional

/**
 * @author ds.kolyshev
 * Date: 19.03.12
 */
@RunWith(SpringRunner::class)
@Transactional
@ContextConfiguration(locations = ["classpath:applicationContext-common-test.xml"])
class TestBroadcastsDAO {
    private val testProfileId: Long = 1000000000

    @Autowired
    lateinit var broadcastDAO: BroadcastDAO
    @Autowired
    lateinit var broadcastStreamDAO: BroadcastStreamDAO
    @Autowired
    lateinit var broadcastGeoTargetingDAO: BroadcastGeoTargetingDAO
    @Autowired
    lateinit var broadcastBackersTargetingDAO: BroadcastBackersTargetingDAO

    @Test
    fun testBroadcasts() {

        val broadcast = createBroadcast()
        broadcastDAO!!.insert(broadcast)
        var selected = broadcastDAO.selectById(broadcast.broadcastId)
        assertBroadcastsEquals(broadcast, selected)

        var list = broadcastDAO.selectByProfileId(broadcast.profileId!!, 0, 10)
        assertNotNull(list)
        assertEquals(1, list.size.toLong())

        broadcast.name = broadcast.name!! + " 2"
        broadcast.description = broadcast.description!! + " 2"
        broadcast.broadcastStatus = BroadcastStatus.LIVE
        broadcast.defaultStreamId = broadcast.defaultStreamId + 1
        broadcast.timeBegin = Date(broadcast.timeBegin!!.time + 1000)
        broadcast.timeEnd = Date(broadcast.timeEnd!!.time + 1000)
        broadcast.isArchived = !broadcast.isArchived
        broadcast.isVisibleOnDashboard = true
        broadcast.isAutoStart = !broadcast.isAutoStart
        broadcast.isAutoSubscribe = !broadcast.isAutoSubscribe
        broadcast.pausedImageId = broadcast.pausedImageId + 1
        broadcast.pausedImageUrl = "1"
        broadcast.finishedImageId = broadcast.finishedImageId + 1
        broadcast.finishedImageUrl = "2"

        broadcastDAO.update(broadcast)
        selected = broadcastDAO.selectById(broadcast.broadcastId)
        assertBroadcastsEquals(broadcast, selected)

        broadcastDAO.delete(broadcast.broadcastId)
        list = broadcastDAO.selectByProfileId(broadcast.profileId!!, 0, 10)
        assertNotNull(list)
        assertEquals(0, list.size.toLong())

    }

    @Test
    fun testBroadcastsSelect() {

        val broadcatsSize = 5
        val calendar = Calendar.getInstance()
        calendar.add(Calendar.HOUR, 1)
        for (i in 0 until broadcatsSize) {
            val broadcast = createBroadcast()
            broadcast.timeBegin = calendar.time
            broadcastDAO!!.insert(broadcast)
        }

        val upcomingBroadcasts = broadcastDAO!!.selectUpcomingBroadcasts(Date(), true, 0, 0)
        assertNotNull(upcomingBroadcasts)
        assertTrue(upcomingBroadcasts.size >= broadcatsSize)

        // emulate passing of time
        calendar.add(Calendar.HOUR, 3)
        val archivedBroadcasts = broadcastDAO.selectArchivedBroadcasts(null!!, calendar.time, false, 0, 0)
        assertNotNull(archivedBroadcasts)
        assertTrue(archivedBroadcasts.size >= broadcatsSize)

    }

    @Test
    fun testBroadcastsGeoTargetingDAO() {

        val broadcast = createBroadcast()
        broadcastDAO!!.insert(broadcast)
        val selected = broadcastDAO.selectById(broadcast.broadcastId)
        assertBroadcastsEquals(broadcast, selected)

        var geoTargetingList = broadcastGeoTargetingDAO!!.selectBroadcastGeoTargeting(broadcast.broadcastId, null)
        assertNotNull(geoTargetingList)
        assertEquals(0, geoTargetingList.size.toLong())
        geoTargetingList = broadcastGeoTargetingDAO.selectBroadcastGeoTargeting(broadcast.broadcastId, true)
        assertNotNull(geoTargetingList)
        assertEquals(0, geoTargetingList.size.toLong())

        var broadcastGeoTargeting = createBroadcastGeoTargeting(broadcast, 1, 2, true)
        broadcastGeoTargetingDAO.insert(broadcastGeoTargeting)

        broadcastGeoTargeting = createBroadcastGeoTargeting(broadcast, 2, 2, true)
        broadcastGeoTargetingDAO.insert(broadcastGeoTargeting)
        broadcastGeoTargeting = createBroadcastGeoTargeting(broadcast, 3, 2, true)
        broadcastGeoTargetingDAO.insert(broadcastGeoTargeting)
        broadcastGeoTargeting = createBroadcastGeoTargeting(broadcast, 0, 1, false)
        broadcastGeoTargetingDAO.insert(broadcastGeoTargeting)
        broadcastGeoTargeting = createBroadcastGeoTargeting(broadcast, 34, 1, false)
        broadcastGeoTargetingDAO.insert(broadcastGeoTargeting)

        geoTargetingList = broadcastGeoTargetingDAO.selectBroadcastGeoTargeting(broadcast.broadcastId, false)
        assertNotNull(geoTargetingList)
        assertEquals(2, geoTargetingList.size.toLong())
        geoTargetingList = broadcastGeoTargetingDAO.selectBroadcastGeoTargeting(broadcast.broadcastId, true)
        assertNotNull(geoTargetingList)
        assertEquals(3, geoTargetingList.size.toLong())

        broadcastGeoTargetingDAO.deleteByCountryIdCityId(broadcast.broadcastId, 2, 1, true)
        geoTargetingList = broadcastGeoTargetingDAO.selectBroadcastGeoTargeting(broadcast.broadcastId, true)
        assertNotNull(geoTargetingList)
        assertEquals(2, geoTargetingList.size.toLong())

        broadcastGeoTargetingDAO.deleteByCountryIdCityId(broadcast.broadcastId, 2, 0, true)
        geoTargetingList = broadcastGeoTargetingDAO.selectBroadcastGeoTargeting(broadcast.broadcastId, true)
        assertNotNull(geoTargetingList)
        assertEquals(2, geoTargetingList.size.toLong())

        broadcastGeoTargetingDAO.delete(broadcast.broadcastId)
        geoTargetingList = broadcastGeoTargetingDAO.selectBroadcastGeoTargeting(broadcast.broadcastId, null)
        assertNotNull(geoTargetingList)
        assertEquals(0, geoTargetingList.size.toLong())

    }

    @Test
    fun testBroadcastBackersTargetingDAO() {

        val broadcast = createBroadcast()
        broadcastDAO!!.insert(broadcast)
        val selected = broadcastDAO.selectById(broadcast.broadcastId)
        assertBroadcastsEquals(broadcast, selected)

        var backersTargetings = broadcastBackersTargetingDAO!!.selectBroadcastBackersTargetings(broadcast.broadcastId)
        assertNotNull(backersTargetings)
        assertEquals(0, backersTargetings.size.toLong())

        val targeting = BroadcastBackersTargeting()
        targeting.profileId = broadcast.profileId
        targeting.broadcastId = broadcast.broadcastId
        targeting.campaignId = 11
        targeting.limitSumm = BigDecimal.TEN
        broadcastBackersTargetingDAO.insert(targeting)

        backersTargetings = broadcastBackersTargetingDAO.selectBroadcastBackersTargetings(broadcast.broadcastId)
        assertNotNull(backersTargetings)
        assertEquals(1, backersTargetings.size.toLong())
        val selectedTargeting = backersTargetings[0]
        assertEquals(targeting.profileId, selectedTargeting.profileId)
        assertEquals(targeting.broadcastId, selectedTargeting.broadcastId)
        assertEquals(targeting.campaignId, selectedTargeting.campaignId)
        assertTrue(targeting.limitSumm!!.compareTo(selectedTargeting.limitSumm!!) == 0)

        broadcastBackersTargetingDAO.delete(broadcast.broadcastId, targeting.campaignId)
        backersTargetings = broadcastBackersTargetingDAO.selectBroadcastBackersTargetings(broadcast.broadcastId)
        assertNotNull(backersTargetings)
        assertEquals(0, backersTargetings.size.toLong())

    }

    @Test
    @Ignore
    fun testBroadcastStreams() {

        var broadcastStream = createBroadcastStream()
        broadcastStreamDAO!!.insert(broadcastStream)
        var selected = broadcastStreamDAO.selectById(broadcastStream.streamId)
        assertBroadcastStreamsEquals(broadcastStream, selected)

        val list = broadcastStreamDAO.selectByBroadcastId(broadcastStream.broadcastId, 0, 0)
        assertNotNull(list)
        assertEquals(1, list.size.toLong())

        //		List<BroadcastStream> broadcastsToStart = broadcastStreamDAO.selectStartedByTime(broadcastStream.getTimeBegin().getTime() + 1000);
        //		assertNotNull(broadcastsToStart);
        //		assertEquals(1, broadcastsToStart.size());

        broadcastStream.name = broadcastStream.name!! + " 2"
        broadcastStream.description = broadcastStream.description!! + " 2"
        broadcastStream.broadcastUrl = broadcastStream.broadcastUrl!! + " 2"
        broadcastStream.embedVideoHtml = "<embed>test 2</embed>"
        broadcastStream.broadcastStatus = BroadcastStatus.LIVE
        broadcastStreamDAO.update(broadcastStream)
        selected = broadcastStreamDAO.selectById(broadcastStream.streamId)
        assertBroadcastStreamsEquals(broadcastStream, selected)

        //		List<BroadcastStream> broadcastsToEnd = broadcastStreamDAO.selectEndedByTime(broadcastStream.getTimeEnd().getTime() + 10000);
        //		assertNotNull(broadcastsToEnd);
        //		assertEquals(1, broadcastsToEnd.size());

        broadcastStreamDAO.deleteByBroadcastId(broadcastStream.broadcastId)
        assertNull(broadcastStreamDAO.selectById(broadcastStream.streamId))

        broadcastStream = createBroadcastStream()
        broadcastStreamDAO.insert(broadcastStream)
        broadcastStreamDAO.delete(broadcastStream.streamId)
        assertNull(broadcastStreamDAO.selectById(broadcastStream.streamId))


    }

    @Test
    fun testUpdateBroadcastViewsCount() {
        val broadcast = createBroadcast()
        broadcastDAO!!.insert(broadcast)
        var selected = broadcastDAO.selectById(broadcast.broadcastId)
        assertEquals(0, selected.viewsCount.toLong())

        broadcastDAO.updateBroadcastViewsCount(broadcast.defaultStreamId)
        selected = broadcastDAO.selectById(broadcast.broadcastId)
        assertEquals(1, selected.viewsCount.toLong())
    }

    private fun createBroadcast(): Broadcast {
        val broadcast = Broadcast()
        broadcast.description = "test description"
        broadcast.descriptionHtml = "test description html"
        broadcast.broadcastStatus = BroadcastStatus.NOT_STARTED
        broadcast.broadcastSubscriptionStatus = BroadcastSubscriptionStatus.PROGRESS
        broadcast.profileId = testProfileId
        broadcast.name = "test name"
        broadcast.imageId = 123123
        broadcast.defaultStreamId = 1111111
        broadcast.timeBegin = Date()
        broadcast.timeEnd = Date(broadcast.timeBegin!!.time + 20000)
        broadcast.isAutoStart = true
        broadcast.isAutoSubscribe = true
        broadcast.isArchived = true
        broadcast.isVisibleOnDashboard = true
        broadcast.pausedImageId = 1231233
        broadcast.pausedImageUrl = "1231233"
        broadcast.isImageCustomDescription = true
        broadcast.imageDescription = "img descr not started"
        broadcast.imageDescriptionHtml = "img descr not started html"
        broadcast.finishedImageId = 1232332
        broadcast.finishedImageUrl = "1232332"
        broadcast.isFinishedCustomDescription = true
        broadcast.finishedDescription = "true"
        broadcast.finishedDescriptionHtml = "true html"
        broadcast.advertismentBannerHtml = "<html />"

        return broadcast
    }

    private fun createBroadcastStream(): BroadcastStream {
        val broadcastStream = BroadcastStream()
        broadcastStream.description = "test description"
        broadcastStream.embedVideoHtml = "<embed>test</embed>"
        broadcastStream.profileId = testProfileId
        broadcastStream.name = "test name"
        broadcastStream.broadcastId = 10000000
        broadcastStream.broadcastRecordStatus = BroadcastRecordStatus.WILL_BE_RECORD
        broadcastStream.broadcastStatus = BroadcastStatus.NOT_STARTED
        broadcastStream.broadcastType = BroadcastType.VIDEO
        broadcastStream.broadcastUrl = "test url"
        broadcastStream.timeBegin = Date()
        broadcastStream.timeEnd = Date(broadcastStream.timeBegin!!.time + 20000)

        return broadcastStream
    }

    private fun createBroadcastGeoTargeting(broadcast: Broadcast, cityId: Int, countryId: Int, allowed: Boolean): BroadcastGeoTargeting {
        val broadcastGeoTargeting = BroadcastGeoTargeting()
        broadcastGeoTargeting.profileId = broadcast.profileId
        broadcastGeoTargeting.broadcastId = broadcast.broadcastId
        broadcastGeoTargeting.cityId = cityId.toLong()
        broadcastGeoTargeting.countryId = countryId.toLong()
        broadcastGeoTargeting.isAllowed = allowed
        return broadcastGeoTargeting
    }

    private fun assertBroadcastsEquals(expected: Broadcast, actual: Broadcast) {
        assertEquals(expected.broadcastId, actual.broadcastId)
        assertEquals(expected.profileId, actual.profileId)
        assertEquals(expected.name, actual.name)
        assertEquals(expected.description, actual.description)
        assertEquals(expected.descriptionHtml, actual.descriptionHtml)
        assertEquals(expected.imageId, actual.imageId)
        assertEquals(expected.imageUrl, actual.imageUrl)
        assertEquals(expected.isImageCustomDescription, actual.isImageCustomDescription)
        assertEquals(expected.imageDescription, actual.imageDescription)
        assertEquals(expected.imageDescriptionHtml, actual.imageDescriptionHtml)
        assertEquals(expected.authorProfileId, actual.authorProfileId)
        assertEquals(expected.broadcastStatus, actual.broadcastStatus)
        assertEquals(expected.broadcastSubscriptionStatus, actual.broadcastSubscriptionStatus)
        assertEquals(expected.defaultStreamId, actual.defaultStreamId)
        assertEquals(expected.timeBegin, actual.timeBegin)
        assertEquals(expected.timeEnd, actual.timeEnd)
        assertEquals(expected.isAutoStart, actual.isAutoStart)
        assertEquals(expected.isAutoSubscribe, actual.isAutoSubscribe)
        assertEquals(expected.isArchived, actual.isArchived)
        assertEquals(expected.pausedImageId, actual.pausedImageId)
        assertEquals(expected.pausedImageUrl, actual.pausedImageUrl)
        assertEquals(expected.isPausedCustomDescription, actual.isPausedCustomDescription)
        assertEquals(expected.pausedDescription, actual.pausedDescription)
        assertEquals(expected.pausedDescriptionHtml, actual.pausedDescriptionHtml)
        assertEquals(expected.finishedImageId, actual.finishedImageId)
        assertEquals(expected.finishedImageUrl, actual.finishedImageUrl)
        assertEquals(expected.isFinishedCustomDescription, actual.isFinishedCustomDescription)
        assertEquals(expected.finishedDescription, actual.finishedDescription)
        assertEquals(expected.finishedDescriptionHtml, actual.finishedDescriptionHtml)
        assertEquals(expected.advertismentBannerHtml, actual.advertismentBannerHtml)
    }

    private fun assertBroadcastStreamsEquals(expected: BroadcastStream, actual: BroadcastStream) {
        assertEquals(expected.broadcastId, actual.broadcastId)
        assertEquals(expected.streamId, actual.streamId)
        assertEquals(expected.profileId, actual.profileId)
        assertEquals(expected.name, actual.name)
        assertEquals(expected.description, actual.description)
        assertEquals(expected.embedVideoHtml, actual.embedVideoHtml)
        assertEquals(expected.imageId, actual.imageId)
        assertEquals(expected.imageUrl, actual.imageUrl)
        assertEquals(expected.broadcastUrl, actual.broadcastUrl)
        assertEquals(expected.broadcastRecordStatus, actual.broadcastRecordStatus)
        assertEquals(expected.broadcastStatus, actual.broadcastStatus)
        assertEquals(expected.broadcastType, actual.broadcastType)
        assertEquals(expected.viewPermission, expected.viewPermission)
        assertEquals(expected.videoId, expected.videoId)
        assertEquals(expected.videoUrl, expected.videoUrl)
    }
}

package ru.planeta.dao.commondb

import org.junit.Assert.*
import org.junit.Ignore
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.transaction.annotation.Transactional
import ru.planeta.model.common.PlanetaCampaignManager

/**
 * @author m.shulepov
 * Date: 21.03.13
 */
@RunWith(SpringRunner::class)
@Transactional
@ContextConfiguration(locations = ["classpath:applicationContext-common-test.xml"])
class TestPlanetaManagersDAOImpl {

    @Autowired
    lateinit var planetaManagersDAO: PlanetaManagersDAO

    @Test
    @Throws(Exception::class)
    fun testSelectManagersByType() {
        val campaignManagers = planetaManagersDAO!!.selectManagersByCampaignTags()
        assertNotNull(campaignManagers)
        assertTrue(campaignManagers.size > 0)
        val groupManagers = planetaManagersDAO.selectManagersForGroupCategories()
        assertNotNull(groupManagers)
        assertTrue(groupManagers.size > 0)
    }


    @Test
    @Ignore
    @Throws(Exception::class)
    fun testCampaignManager() {
        val campaignId: Long = 1
        val managerId: Long = 3

        val planetaCampaignManager = PlanetaCampaignManager()
        planetaCampaignManager.campaignId = campaignId
        planetaCampaignManager.managerId = managerId

        planetaManagersDAO!!.insertManagerForCurrentObject(planetaCampaignManager)

        val planetaManager = planetaManagersDAO.getManagerForCurrentCampaign(campaignId)
        assertNotNull(planetaManager)
        assertEquals(managerId, planetaManager.managerId)

        val newManagerId: Long = 1
        planetaCampaignManager.managerId = newManagerId
        planetaManagersDAO.updateManagerForCurrentObject(planetaCampaignManager)

        val planetaManagerUpdated = planetaManagersDAO.getManagerForCurrentCampaign(campaignId)
        assertNotNull(planetaManagerUpdated)
        assertEquals(newManagerId, planetaManagerUpdated.managerId)
    }

}

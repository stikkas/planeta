package ru.planeta.dao.profiledb


import org.junit.Assert.*
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.transaction.annotation.Transactional
import ru.planeta.TestHelper
import ru.planeta.model.profile.ProfileFile
import java.util.*

@RunWith(SpringRunner::class)
@Transactional
@ContextConfiguration(locations = ["classpath:applicationContext-common-test.xml"])
class TestProfileFileDAO {

    @Autowired
    lateinit var profileFileDAO: ProfileFileDAO

    @Test
    fun testProfileFileDAO() {

        val profile = TestHelper.createUser(TestHelper.TEST_PROFILE_ID)
        val ownerId = profile.profileId

        val profileFile = createProfileFile(ownerId, ownerId)
        profileFileDAO!!.insert(profileFile)
        assertTrue(profileFile.fileId > 0)

        val selected = profileFileDAO.selectById(ownerId, profileFile.fileId)
        assertFilesEquals(profileFile, selected)

        var list = profileFileDAO.selectList(ownerId, 0, 10)
        assertNotNull(list)
        assertEquals(1, list.size.toLong())
        val inserted = list[0]
        assertFilesEquals(profileFile, inserted)

        profileFileDAO.delete(ownerId, profileFile.fileId)
        list = profileFileDAO.selectList(ownerId, 0, 0)
        assertNotNull(list)
        assertEquals(0, list.size.toLong())
    }

    private fun assertFilesEquals(expected: ProfileFile, actual: ProfileFile) {
        assertNotNull(actual)
        assertEquals(expected.fileId, actual.fileId)
        assertEquals(expected.authorId, actual.authorId)
        assertEquals(expected.description, actual.description)
        assertEquals(expected.fileUrl, actual.fileUrl)
        assertEquals(expected.name, actual.name)
        assertEquals(expected.profileId, actual.profileId)
        assertEquals(expected.timeAdded, actual.timeAdded)
        assertEquals(expected.timeUpdated, actual.timeUpdated)
        assertEquals(expected.size, actual.size)
        assertEquals(expected.extension, actual.extension)
    }

    private fun createProfileFile(ownerId: Long, authorId: Long): ProfileFile {
        val profileFile = ProfileFile()
        profileFile.profileId = ownerId
        profileFile.authorId = authorId
        profileFile.timeAdded = Date()
        profileFile.timeUpdated = Date()
        profileFile.description = "Description"
        profileFile.name = "Name"
        profileFile.fileUrl = "http://files.com/file.ext"
        profileFile.size = 112323
        profileFile.extension = "ext"

        return profileFile
    }
}

package ru.planeta.dao.commondb

import org.assertj.core.api.Assertions.assertThat
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.transaction.annotation.Transactional
import ru.planeta.model.common.DebitTransaction
import java.math.BigDecimal
import java.util.*

/**
 * Class TestDebitTransactionDAO
 *
 * @author a.tropnikov
 */
@RunWith(SpringRunner::class)
@Transactional
@ContextConfiguration(locations = ["classpath:applicationContext-common-test.xml"])
class TestDebitTransactionDAO {

    @Autowired
    lateinit var debitTransactionDAO: DebitTransactionDAO

    private val testProfileId: Long = 1000000000

    @Test
    fun test() {

        val debitTransaction = DebitTransaction()
        debitTransaction.profileId = testProfileId
        debitTransaction.paymentProviderId = 10L
        debitTransaction.externalSystemData = ""
        debitTransaction.amountNet = BigDecimal.ONE
        debitTransaction.amountFee = BigDecimal.TEN
        debitTransaction.comment = "comment"
        debitTransaction.timeAdded = Date()

        debitTransactionDAO.insert(debitTransaction)
        assertThat(debitTransaction.transactionId).isGreaterThan(0)

        val selectedList = debitTransactionDAO.selectList(testProfileId, 0, 0)
        assertThat(selectedList.size).isEqualTo(1)

        val selected = debitTransactionDAO.select(debitTransaction.transactionId)
        assertThat(selected).isNotNull
        selected?.let {
            assertDebitTransactionEquals(it, debitTransaction)
        }

        debitTransaction.amountNet = BigDecimal.TEN
        debitTransaction.amountFee = BigDecimal.ONE
        debitTransaction.comment = "new comment"
        debitTransactionDAO.update(debitTransaction)

        val updated = debitTransactionDAO.select(debitTransaction.transactionId)
        assertThat(updated).isNotNull
        updated?.let {
            assertDebitTransactionEquals(it, debitTransaction)
        }

        debitTransactionDAO.delete(debitTransaction.profileId, debitTransaction.transactionId)
        assertThat(debitTransactionDAO.select(debitTransaction.transactionId)).isNull()

    }

    private fun assertDebitTransactionEquals(expected: DebitTransaction, actual: DebitTransaction) {
        assertThat(expected).isNotNull
        assertThat(expected.transactionId).isEqualTo(actual.transactionId)
        assertThat(expected.amountNet).isEqualTo(actual.amountNet)
        assertThat(expected.amountFee).isEqualTo(actual.amountFee)
        assertThat(expected.comment).isEqualTo(actual.comment)
        assertThat(expected.timeAdded).isEqualTo(actual.timeAdded)
    }
}

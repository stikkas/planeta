package ru.planeta.dao.commondb.campaign

import org.junit.Test
import org.springframework.beans.factory.annotation.Autowired
import ru.planeta.model.common.campaign.enums.CampaignStatus

import java.util.EnumSet

import org.junit.Assert.assertNotNull
import org.junit.Assert.assertTrue
import org.junit.runner.RunWith
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.transaction.annotation.Transactional
import ru.planeta.dao.commondb.CampaignDAO

/**
 *
 * @author Serge Blagodatskih<stikkas17></stikkas17>@gmail.com>
 */
@RunWith(SpringRunner::class)
@Transactional
@ContextConfiguration(locations = ["classpath:applicationContext-common-test.xml"])
class CampaignDAOImplTest {

    @Autowired
    lateinit var campaignDAO: CampaignDAO

    @Test
    fun testSelectCampaignsByStatus() {
        var campaings = campaignDAO.selectCampaignsByStatus(-1, null, 0, 10)
        assertNotNull(campaings)
        assertTrue(!campaings.isEmpty())
        campaings = campaignDAO.selectCampaignsByStatus(-1, EnumSet.of(CampaignStatus.ACTIVE, CampaignStatus.FINISHED), 0, 10)
        assertNotNull(campaings)
    }

    @Test
    fun testCustomTagsPercentage() {
        val map = campaignDAO.campaignsCountPercentagesByCustomTags()
        assertNotNull(map)
    }

}

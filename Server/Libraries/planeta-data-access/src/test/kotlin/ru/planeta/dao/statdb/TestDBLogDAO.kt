package ru.planeta.dao.statdb


import org.apache.commons.collections4.IterableUtils
import org.junit.Assert.assertNotNull
import org.junit.Ignore
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.transaction.annotation.Transactional
import ru.planeta.model.stat.log.LoggerType

/**
 * Test database log data access object.<br></br>
 * User: eshevchenko
 */
@RunWith(SpringRunner::class)
@Transactional
@ContextConfiguration(locations = ["classpath:applicationContext-common-test.xml"])
class TestDBLogDAO {

    @Autowired
    lateinit var dbLogDAO: DBLogDAO

    @Test
    @Throws(Exception::class)
    fun test() {
        val reqId = dbLogDAO.insertRequestLogRecord("metaData")
        val respId = dbLogDAO.insertResponseLogRecord(reqId, "metaData")
        val recId = dbLogDAO.insertLogRecord("comment", LoggerType.ORDER, 1L, 1L, reqId, respId)

        val records = dbLogDAO.selectLogRecords(LoggerType.ORDER, 1L, 1L)
        assertNotNull(records)
        records.forEach{ println(it)}
        assertNotNull(IterableUtils.find(records) { `object` -> `object`.id == recId })

        dbLogDAO.updateRefInfo(recId, 0L, 0L, 0L, 0L)
    }

    @Ignore
    @Test
    @Throws(Exception::class)
    fun testHighLoad() {
        for (i in 0..999) {
            test()
        }
    }

}

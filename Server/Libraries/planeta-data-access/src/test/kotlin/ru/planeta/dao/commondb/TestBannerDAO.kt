package ru.planeta.dao.commondb

import org.apache.log4j.Logger
import org.junit.Assert.*
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.transaction.annotation.Transactional
import ru.planeta.model.common.Banner
import ru.planeta.model.enums.BannerStatus
import java.util.*

/**
 * Class TestBannerDAO
 *
 * @author a.savanovich
 */
@RunWith(SpringRunner::class)
@Transactional
@ContextConfiguration(locations = ["classpath:applicationContext-common-test.xml"])
class TestBannerDAO {

    @Autowired
    lateinit var bannerDAO: BannerDAO

    @Test
    fun testSelectUpdateDelete() {
        val banner = insertBanner(bannerDAO)
        assertTrue(banner.bannerId > 0)


        val selected = bannerDAO.select(banner.bannerId)
        assertNotNull(selected)
        selected?.let {
            assertBannerEquals(it, banner)
        }

        var banners = bannerDAO.selectList(EnumSet.allOf(BannerStatus::class.java), 0, 0)
        assertTrue(!banners.isEmpty())

        banners = bannerDAO.selectList(EnumSet.of(BannerStatus.ON), 0, 0)
        assertTrue(!banners.isEmpty())

        banner.name = "new name"
        banner.html = "<html>"
        banner.status = BannerStatus.OFF
        bannerDAO.update(banner)

        val updated = bannerDAO.select(banner.bannerId)
        assertNotNull(updated)
        updated?.let {
            assertBannerEquals(it, banner)
        }

        banners = bannerDAO.selectList(EnumSet.of(BannerStatus.OFF), 0, 0)
        assertTrue(!banners.isEmpty())

        bannerDAO.delete(banner.bannerId)
        assertNull(bannerDAO.select(banner.bannerId))

    }

    private fun assertBannerEquals(expected: Banner, actual: Banner) {
        assertEquals(expected.bannerId, actual.bannerId)
        assertEquals(expected.name, actual.name)
        assertEquals(expected.html, actual.html)
        assertEquals(expected.timeAdded, actual.timeAdded)
        assertEquals(expected.timeUpdated, actual.timeUpdated)
    }

    private fun insertBanner(bannerDAO: BannerDAO): Banner {
        val banner = Banner()
        banner.name = "name"
        banner.html = "html"
        banner.timeAdded = Date()
        banner.timeUpdated = banner.timeAdded
        banner.status = BannerStatus.ON
        bannerDAO.insert(banner)
        return banner
    }

    companion object {

        private val log = Logger.getLogger(TestBannerDAO::class.java)
    }

}

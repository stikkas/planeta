package ru.planeta.dao.trashcan


import org.junit.Assert.*
import org.junit.Ignore
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.transaction.annotation.Transactional
import ru.planeta.model.trashcan.FeedbackPageType
import ru.planeta.model.trashcan.UserFeedback
import java.util.*

@RunWith(SpringRunner::class)
@Transactional
@ContextConfiguration(locations = ["classpath:applicationContext-common-test.xml"])
class UserFeedbackDAOImplTest {
    @Autowired
    lateinit var userFeedbackDAO: UserFeedbackDAO

    @Ignore
    @Test
    @Throws(Exception::class)
    fun insertSelectUpdate() {
        val userFeedback = UserFeedback()
        userFeedback.orderId = 111
        userFeedback.userId = 111
        userFeedback.campaignId = 111
        userFeedback.score = 10
        userFeedback.isExtraTesting = true
        userFeedback.pageType = FeedbackPageType.PAYMENT_SUCCESS
        userFeedback.timeAdded = Date()

        var userFeedbacksList = userFeedbackDAO!!.selectUserFeedbacks(0, 500)
        assertNotNull(userFeedbacksList)
        val userFeedbackCount = userFeedbackDAO.selectUserFeedbackCount()
        assertEquals(userFeedbackCount.toLong(), userFeedbacksList.size.toLong())

        userFeedbackDAO.insert(userFeedback)
        userFeedbacksList = userFeedbackDAO.selectUserFeedbacks(0, 500)
        val newUserFeedbackCount = userFeedbackDAO.selectUserFeedbackCount()
        assertNotNull(userFeedbacksList)
        assertEquals(newUserFeedbackCount.toLong(), userFeedbacksList.size.toLong())
        assertTrue(newUserFeedbackCount - userFeedbackCount == 1)
    }

}

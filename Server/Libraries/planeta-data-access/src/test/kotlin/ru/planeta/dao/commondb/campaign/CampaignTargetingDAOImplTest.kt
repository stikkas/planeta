package ru.planeta.dao.commondb.campaign

import org.junit.Assert.*
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.transaction.annotation.Transactional
import ru.planeta.TestHelper
import ru.planeta.TestHelper.TEST_PROFILE_ID
import ru.planeta.dao.commondb.CampaignTargetingDAO
import ru.planeta.model.common.campaign.CampaignTargeting

@RunWith(SpringRunner::class)
@Transactional
@ContextConfiguration(locations = ["classpath:applicationContext-common-test.xml"])
class CampaignTargetingDAOImplTest {
    @Autowired
    lateinit var campaignTargetingDAO: CampaignTargetingDAO

    @Test
    fun testCampaignTargetingBaseMethods() {
        val testProfileId = TEST_PROFILE_ID
        val campaign = TestHelper.createCampaign(testProfileId)

        val campaignId = campaign.campaignId
        val vkTargetingId = "123"

        val campaignTargeting = CampaignTargeting()
        campaignTargeting.campaignId = campaignId
        campaignTargeting.vkTargetingId = vkTargetingId

        campaignTargetingDAO!!.insert(campaignTargeting)

        val selectedCampaignTargeting = campaignTargetingDAO.selectByCampaignId(campaignId)
        assertNotNull(selectedCampaignTargeting)
        assertEquals(campaignId, selectedCampaignTargeting.campaignId)
        assertEquals(vkTargetingId, selectedCampaignTargeting.vkTargetingId)
        assertNull(selectedCampaignTargeting.fbTargetingId)

        val newVkTargetingId = "321"
        val newFbTargetingId = "654"

        selectedCampaignTargeting.vkTargetingId = newVkTargetingId
        selectedCampaignTargeting.fbTargetingId = newFbTargetingId

        campaignTargetingDAO.update(selectedCampaignTargeting)

        val updatedCampaignTargeting = campaignTargetingDAO.selectByCampaignId(selectedCampaignTargeting.campaignId)
        assertEquals(selectedCampaignTargeting.campaignId, updatedCampaignTargeting.campaignId)
        assertEquals(newVkTargetingId, updatedCampaignTargeting.vkTargetingId)
        assertEquals(newFbTargetingId, updatedCampaignTargeting.fbTargetingId)
    }
}

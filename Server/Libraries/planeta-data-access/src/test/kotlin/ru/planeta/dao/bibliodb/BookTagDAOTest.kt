package ru.planeta.dao.bibliodb

import org.junit.Assert.assertTrue
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.transaction.annotation.Transactional
import ru.planeta.model.bibliodb.Book
import ru.planeta.model.bibliodb.BookTag
import java.util.*

@RunWith(SpringRunner::class)
@Transactional
@ContextConfiguration(locations = ["classpath:applicationContext-common-test.xml"])
class BookTagDAOTest {
    @Autowired
    lateinit var bookDAO: BookDAO
    @Autowired
    lateinit var bookTagDAO: BookTagDAO

    private fun createBookRequest(): Book {
        val b = Book()
        b.title = "title"
        b.email = "email"
        b.contact = "contact"
        b.site = "site"
        return b
    }

    @Test
    @Throws(Exception::class)
    fun existsTagInBooks() {
        val book = createBookRequest()
        bookDAO.insertBook(book)

        val bookTags = ArrayList<BookTag>()
        val bookTag = BookTag(7) // other type tag
        val bookTag2 = BookTag(8) // RUSSIANPOST tag
        bookTags.add(bookTag)
        bookTags.add(bookTag2)

        bookTagDAO.setTagRelation(book.bookId, bookTags)

        val bookIds = ArrayList<Long>()
        bookIds.add(book.bookId)
        assertTrue(bookTagDAO.existsTagInBooks(bookIds, BookTag.RUSSIANPOST))
    }
}

package ru.planeta.dao.statdb

import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.transaction.annotation.Transactional
import ru.planeta.model.stat.OrderGeoStat


import java.net.UnknownHostException

/**
 * Created with IntelliJ IDEA.
 * User: michail
 * Date: 27.06.16
 * Time: 13:10
 */
@RunWith(SpringRunner::class)
@Transactional
@ContextConfiguration(locations = ["classpath:applicationContext-common-test.xml"])
class TestOrderGeoStatDAO {
    @Autowired
    lateinit var orderGeoStatDAO: OrderGeoStatDAO

    @Throws(UnknownHostException::class)
    private fun createTestOrderGeo(): OrderGeoStat {
        val orderGeo = OrderGeoStat()
        orderGeo.orderId = 123
        orderGeo.stringIpAddress = "127.0.0.1"
        orderGeo.countryId = 1
        orderGeo.cityId = 1
        return orderGeo
    }

    private fun assertOrderGeoEquals(actual: OrderGeoStat, expected: OrderGeoStat) {
        Assert.assertEquals(actual.orderId, expected.orderId)
        Assert.assertEquals(actual.cityId, expected.cityId)
        Assert.assertEquals(actual.countryId, expected.countryId)
        Assert.assertEquals(actual.ipAddress, expected.ipAddress)
        Assert.assertEquals(actual.timeUpdated, expected.timeUpdated)
    }

    @Test
    @Throws(UnknownHostException::class)
    fun testInsert() {
        val orderGeo = createTestOrderGeo()
        orderGeoStatDAO!!.insert(orderGeo)
        val orderGeoActual = orderGeoStatDAO!!.selectByOrderId(orderGeo.orderId)
        assertOrderGeoEquals(orderGeoActual, orderGeo)
    }

    @Test
    @Throws(UnknownHostException::class)
    fun testUpdate() {
        val orderGeo = createTestOrderGeo()
        orderGeoStatDAO.insert(orderGeo)
        orderGeo.cityId = 2
        orderGeo.countryId = 2
        orderGeoStatDAO.update(orderGeo)
        val orderGeoActual = orderGeoStatDAO.selectByOrderId(orderGeo.orderId)
        assertOrderGeoEquals(orderGeoActual, orderGeo)
    }

}

package ru.planeta.dao.bibliodb

import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.transaction.annotation.Transactional
import ru.planeta.model.bibliodb.Book

/**
 * Created with IntelliJ IDEA.
 * User: michail
 * Date: 16.11.2016
 * Time: 19:19
 */
@RunWith(SpringRunner::class)
@Transactional
@ContextConfiguration(locations = ["classpath:applicationContext-common-test.xml"])
class BookDAOTest {

    @Autowired
    lateinit var bookDAO: BookDAO

    private fun createBookRequest(): Book {
        val b = Book()
        b.title = "title"
        b.email = "email"
        b.contact = "contact"
        b.site = "site"
        return b
    }

    @Test
    fun testHasRequests() {
        val b = createBookRequest()
        bookDAO.insertBook(b)
        Assert.assertEquals(bookDAO.selectHasRequests(b.title!!, b.email!!), true)
    }
}

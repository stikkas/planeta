package ru.planeta.dao.commondb

import org.junit.Test
import org.springframework.beans.factory.annotation.Autowired
import ru.planeta.model.common.TopayTransaction
import ru.planeta.model.enums.ProjectType
import ru.planeta.model.enums.TopayTransactionStatus


import java.math.BigDecimal
import java.util.Date

import org.junit.Assert.*
import org.junit.runner.RunWith
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.transaction.annotation.Transactional

/**
 * Created by IntelliJ IDEA.
 * User: atropnikov
 * Date: 22.12.11
 * Time: 16:30
 */
@RunWith(SpringRunner::class)
@Transactional
@ContextConfiguration(locations = ["classpath:applicationContext-common-test.xml"])
class TestTopayTransactionDAO {

    @Autowired
    lateinit var transactionDAO: TopayTransactionDAO


    @Autowired
    lateinit var configurationDAO: ConfigurationDAO

    @Test
    fun testCRUD() {
        val profileId = Long.MAX_VALUE

        val transaction = createTransactionAndInsert(profileId, TopayTransactionStatus.NEW, EXTRA_PARAM)

        val selected = transactionDAO.select(transaction.transactionId)
        assertTransactionEquals(selected, transaction)

        transaction.amountNet = BigDecimal.TEN
        transaction.amountFee = BigDecimal.ZERO
        transaction.status = TopayTransactionStatus.DONE
        transaction.debitTransactionId = 100L
        transaction.timeUpdated = Date()
        transaction.param1 = "updatedParam1"
        transaction.param2 = "updatedParam2"
        transaction.paymentMethodId = ID_STUB
        transaction.paymentProviderId = ID_STUB
        transaction.paymentToolCode = "stub"
        transactionDAO.update(transaction)

        val updated = transactionDAO.select(transaction.transactionId)
        assertTransactionEquals(updated, transaction)

        transactionDAO.delete(transaction.transactionId)
        assertNull(transactionDAO.select(transaction.transactionId))
    }

    private fun createTransactionAndInsert(profileId: Long, status: TopayTransactionStatus, extraParam: String): TopayTransaction {
        return createTransactionAndInsert(profileId, Long.MAX_VALUE, status, extraParam)
    }

    private fun createTransactionAndInsert(profileId: Long, orderId: Long, status: TopayTransactionStatus, extraParam: String): TopayTransaction {
        val transaction = createTransaction(profileId, orderId, status, extraParam)
        transactionDAO!!.insert(transaction)
        assertTrue(transaction.transactionId > 0)
        return transaction
    }

    private fun createTransaction(profileId: Long, orderId: Long, status: TopayTransactionStatus, extraParam: String): TopayTransaction {
        val transaction = TopayTransaction()
        transaction.profileId = profileId
        transaction.orderId = orderId
        transaction.amountNet = BigDecimal.ONE
        transaction.amountFee = BigDecimal.TEN
        transaction.status = status
        transaction.timeAdded = Date()
        transaction.projectType = ProjectType.MAIN
        transaction.param1 = extraParam
        transaction.param2 = "Param2"
        return transaction
    }

    private fun assertTransactionEquals(expected: TopayTransaction, actual: TopayTransaction) {
        assertNotNull(expected)
        assertEquals(expected.transactionId, actual.transactionId)
        assertEquals(expected.profileId, actual.profileId)
        assertEquals(expected.orderId, actual.orderId)
        assertEquals(expected.amountNet!!.compareTo(actual.amountNet!!).toLong(), 0)
        assertEquals(expected.amountFee!!.compareTo(actual.amountFee!!).toLong(), 0)
        assertEquals(expected.status, actual.status)
        assertEquals(expected.debitTransactionId, actual.debitTransactionId)
        assertEquals(expected.timeAdded, actual.timeAdded)
        assertEquals(expected.timeUpdated, actual.timeUpdated)
        assertEquals(expected.projectType, actual.projectType)
        assertEquals(expected.param1, actual.param1)
        assertEquals(expected.param2, actual.param2)

        assertEquals(expected.paymentMethodId, actual.paymentMethodId)
        assertEquals(expected.paymentProviderId, actual.paymentProviderId)
        assertEquals(expected.paymentToolCode, actual.paymentToolCode)
    }

    @Test
    fun testCheckOtherFailTransaction() {
//        val profileId = getId(Profile::class.java)
//        var transaction1 = createTransactionAndInsert(profileId, TopayTransactionStatus.ERROR, "")
//
//        assertNull(transactionDAO!!.getPrevFailTransaction(transaction1.transactionId))
//
//        val transaction2 = createTransactionAndInsert(profileId, TopayTransactionStatus.ERROR, "")
//        var selected = transactionDAO.getPrevFailTransaction(transaction2.transactionId)
//        assertNotNull(selected)
//        assertEquals(transaction1.transactionId, selected.transactionId)
//
//
//        val failInterval = configurationDAO!!.selectCampaignById("payment.errors.two.fail.payments").intValue
//
//        transactionDAO.deleteByProfileId(transaction1.transactionId)
//        transaction1 = createTransaction(profileId, getId(Order::class.java), TopayTransactionStatus.ERROR, "")
//        transaction1.timeAdded = DateUtils.addMinutes(transaction2.timeAdded!!, -failInterval)
//        transactionDAO.insert(transaction1)
//        selected = transactionDAO.getPrevFailTransaction(transaction2.transactionId)
//        assertNotNull(selected)
//        assertEquals(transaction1.transactionId, selected.transactionId)
//
//
//        transactionDAO.deleteByProfileId(transaction1.transactionId)
//        transaction1 = createTransaction(profileId, getId(Order::class.java), TopayTransactionStatus.ERROR, "")
//        transaction1.timeAdded = Date(transaction1.timeAdded!!.time - 1)
//        transactionDAO.insert(transaction1)
//        selected = transactionDAO.getPrevFailTransaction(transaction2.transactionId)
//        assertNull(selected)
//
//        transactionDAO.deleteByProfileId(transaction1.transactionId)
//        transaction1 = createTransaction(profileId, getId(Order::class.java), TopayTransactionStatus.ERROR, "")
//        transaction1.timeAdded = Date(transaction2.timeAdded!!.time + 1)
//        transactionDAO.insert(transaction1)
//        selected = transactionDAO.getPrevFailTransaction(transaction2.transactionId)
//        assertNull(selected)
//
//
//        transactionDAO.deleteByProfileId(transaction1.transactionId)
//
//        val transaction3 = createTransactionAndInsert(profileId, TopayTransactionStatus.ERROR, "")
//        selected = transactionDAO.getPrevFailTransaction(transaction3.transactionId)
//        assertNotNull(selected)
//        assertEquals(transaction2.transactionId, selected.transactionId)
//
//        val transaction4 = createTransactionAndInsert(profileId, TopayTransactionStatus.ERROR, "")
//        selected = transactionDAO.getPrevFailTransaction(transaction4.transactionId)
//        assertNotNull(selected)
//        assertEquals(transaction3.transactionId, selected.transactionId)


    }

    companion object {

        val EXTRA_PARAM = "extra_param"
        val ID_STUB = 1L
    }
}

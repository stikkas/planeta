package ru.planeta.dao.commondb

import org.junit.Assert.assertEquals
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.transaction.annotation.Transactional
import ru.planeta.dao.promo.TechnobattleRegistrationDAO
import ru.planeta.model.promo.TechnobattleRegistration

/**
 * Created with IntelliJ IDEA.
 * User: michail
 * Date: 18.11.2016
 * Time: 12:12
 */
@RunWith(SpringRunner::class)
@Transactional
@ContextConfiguration(locations = ["classpath:applicationContext-common-test.xml"])
class TechnobattleDAOTest {
    @Autowired
    lateinit var technobattleRegistrationDAO: TechnobattleRegistrationDAO

    private fun assertTechnobattleRegistrationEquals(actual: TechnobattleRegistration, expected: TechnobattleRegistration) {
        assertEquals(actual.city, expected.city)
        assertEquals(actual.description, expected.description)
        assertEquals(actual.email, expected.email)
        assertEquals(actual.phone, expected.phone)
        assertEquals(actual.name, expected.name)
        assertEquals(actual.type, expected.type)
    }

    @Test
    fun testInsert() {
        val registration = TechnobattleRegistration()
        registration.city = "city"
        registration.description = "desc"
        registration.name = "name"
        registration.phone = "phone"
        registration.email = "email"
        registration.type = TechnobattleRegistration.Type.LETTER

        technobattleRegistrationDAO.insert(registration)
        val actual = technobattleRegistrationDAO.selectById(registration.registrationId)
        assertTechnobattleRegistrationEquals(actual, registration)
    }

}

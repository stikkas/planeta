package ru.planeta.dao.profiledb


import org.junit.Assert.*
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.transaction.annotation.Transactional
import ru.planeta.TestHelper.TEST_PROFILE_ID
import ru.planeta.model.profile.ProfileCommunicationChannel

/**
 * Class TestProfileCommunicationChannelDAO
 *
 * @author a.tropnikov
 */
@RunWith(SpringRunner::class)
@Transactional
@ContextConfiguration(locations = ["classpath:applicationContext-common-test.xml"])
class TestProfileCommunicationChannelDAO {

    @Autowired
    lateinit var profileCommunicationChannelDAO: ProfileCommunicationChannelDAO

    @Test
    fun test() {


        val channel = ProfileCommunicationChannel()
        channel.profileId = TEST_PROFILE_ID
        channel.channelId = 1
        channel.channelValue = "value"

        profileCommunicationChannelDAO.insert(channel)

        val channels = profileCommunicationChannelDAO.selectProfileCommunicationChannels(TEST_PROFILE_ID)
        assertTrue(channels.size == 1)

        val selected = profileCommunicationChannelDAO.select(TEST_PROFILE_ID, channel.channelId)
        assertNotNull(selected)
        assertChannelEquals(selected, channel)

        channel.channelValue = "new value"
        profileCommunicationChannelDAO.update(channel)

        val updated = profileCommunicationChannelDAO.select(TEST_PROFILE_ID, channel.channelId)
        assertChannelEquals(updated, channel)

        profileCommunicationChannelDAO.deleteAll(TEST_PROFILE_ID)
        assertNull(profileCommunicationChannelDAO.select(TEST_PROFILE_ID, channel.channelId))

        profileCommunicationChannelDAO.insert(channel)
        profileCommunicationChannelDAO.delete(TEST_PROFILE_ID, channel.channelId)
        assertNull(profileCommunicationChannelDAO.select(TEST_PROFILE_ID, channel.channelId))


    }

    private fun assertChannelEquals(expected: ProfileCommunicationChannel, actual: ProfileCommunicationChannel) {
        assertEquals(expected.profileId, actual.profileId)
        assertEquals(expected.channelId.toLong(), actual.channelId.toLong())
        assertEquals(expected.channelValue, actual.channelValue)
    }
}

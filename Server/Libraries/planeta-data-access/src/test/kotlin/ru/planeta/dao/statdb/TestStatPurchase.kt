package ru.planeta.dao.statdb


import org.apache.commons.lang3.time.DateUtils
import org.junit.Assert.assertNotNull
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.transaction.annotation.Transactional
import ru.planeta.dao.commondb.AdminStatPurchaseDAO
import java.util.*

/**
 * User: Kuzminov
 * Date: 17.08.12
 * Time: 10:42
 */
@RunWith(SpringRunner::class)
@Transactional
@ContextConfiguration(locations = ["classpath:applicationContext-common-test.xml"])
class TestStatPurchase {
    @Autowired
    lateinit var purchaseDAO: AdminStatPurchaseDAO

    @Test
    fun testSelectPurchase() {

        val calendar = Calendar.getInstance()
        calendar.set(2017, 1, 1)
        val result = purchaseDAO!!.selectStatComposite(Date(), calendar.time, "month")
        assertNotNull(result)

    }

    @Test
    fun testSelectCampaignsForecast() {


        val result = purchaseDAO!!.selectForecast()
        assertNotNull(result)

    }


    @Test
    fun testSelectPurchaseShare() {


        val result = purchaseDAO!!.selectStatPurchaseShares(DateUtils.addMonths(Date(), -1), Date())

    }

    @Test
    fun testSelectPurchaseProduct() {


        val result = purchaseDAO!!.selectStatPurchaseProducts(DateUtils.addMonths(Date(), -1), Date())

    }
}

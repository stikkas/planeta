package ru.planeta.dao.commondb


import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.transaction.annotation.Transactional

/**
 * Created with IntelliJ IDEA.
 * User: michail
 * Date: 05.10.16
 * Time: 14:25
 */

@RunWith(SpringRunner::class)
@Transactional
@ContextConfiguration(locations = ["classpath:applicationContext-common-test.xml"])
class TestBiblioStats {
    @Autowired
    lateinit var biblioOrderStatsDAO: BiblioOrderStatsDAO

    @Test
    fun testBookStats() {
        val bc = biblioOrderStatsDAO.purchasedBooksCount()
        val bs = biblioOrderStatsDAO.purchasedBooksSum()
    }
}

package ru.planeta.dao.msgdb

import org.assertj.core.api.Assertions.assertThat
import org.junit.Assert.assertEquals
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.transaction.annotation.Transactional
import ru.planeta.TestHelper
import ru.planeta.dao.profiledb.ProfileDAO
import ru.planeta.model.msg.DialogMessage
import ru.planeta.model.profile.Profile
import java.util.*

/**
 * @author ameshkov
 */
@RunWith(SpringRunner::class)
@Transactional
@ContextConfiguration(locations = ["classpath:applicationContext-common-test.xml"])
class TestDialogMessageDAO {

    @Autowired
    lateinit var dialogMessageDAO: DialogMessageDAO

    @Autowired
    lateinit var profileDAO: ProfileDAO

    @Test
    fun testDialogMessageAddRemove() {
        val dialogId: Long = 1
        val profile = TestHelper.createUserProfile()
        profileDAO.insert(profile)

        val dialogMessage = DialogMessage()
        dialogMessage.dialogId = dialogId
        dialogMessage.messageText = "Testtest"
        dialogMessage.messageTextHtml = "Testtest"
        dialogMessage.timeAdded = Date()
        dialogMessage.userId = profile.profileId
        dialogMessage.isDeleted = false
        dialogMessageDAO.insert(dialogMessage)
        assertThat(dialogMessage.messageId).isGreaterThan(0)

        val selectedMessage = dialogMessageDAO.selectMessage(dialogId, dialogMessage.messageId)
        assertThat(selectedMessage).isNotNull
        assertThat(dialogMessage.messageText).isEqualTo(selectedMessage.messageText)

        var messages = dialogMessageDAO.selectMessages(dialogId, 0, 100)
        assertThat(messages).isNotNull
        assertThat(messages.size).isEqualTo(1)

        messages = dialogMessageDAO.selectMessages(dialogId, 0, 0)
        assertThat(messages).isNotNull
        assertThat(messages.size).isEqualTo(1)

        val dialogMessage2 = DialogMessage()
        dialogMessage2.dialogId = dialogId
        dialogMessage2.messageText = "123123"
        dialogMessage2.messageTextHtml = "123123"
        dialogMessage2.timeAdded = Date()
        dialogMessage2.userId = profile.profileId
        dialogMessageDAO.insert(dialogMessage2)

        messages = dialogMessageDAO.selectMessages(dialogId, 0, 10)
        assertThat(messages).isNotNull
        assertThat(messages.size).isEqualTo(2)

        messages = dialogMessageDAO.selectLastMessages(dialogId, dialogMessage.messageId)
        assertThat(messages).isNotNull
        assertThat(messages.size).isEqualTo(1)

        var lastMessage = dialogMessageDAO.selectLastMessage(dialogId)
        assertThat(lastMessage).isNotNull
        assertThat(lastMessage.messageId).isEqualTo(messages[0].messageId)

        dialogMessage.isDeleted = true
        dialogMessageDAO.update(dialogMessage)
        messages = dialogMessageDAO.selectMessages(dialogId, 0, 10)
        assertThat(messages.size).isEqualTo(1)

        lastMessage = dialogMessageDAO.selectLastMessage(dialogId)
        assertThat(lastMessage).isNotNull
        assertThat(lastMessage.messageId).isEqualTo(messages[0].messageId)

    }

    @Test
    fun testSelectLastDialogMessageId() {

        val dialogId: Long = 1

        assertEquals("Последнее сообщение у несуществующего диалога должно быть 0", 0L, dialogMessageDAO.selectLastMessageId(dialogId))

        val countUser = 3
        val messageCount = 3
        val users = ArrayList<Profile>(countUser)
        for (i in 0 until countUser) {
            val profile = TestHelper.createUserProfile()
            profileDAO.insert(profile)
            users.add(profile)
        }


        var lastMessageId: Long = 0
        for (messNum in 0 until messageCount) {
            for (user in users) {
                val dialogMessage = insertNewDialogMessage(dialogId, user, false)
                lastMessageId = dialogMessage.messageId

                assertEquals("Ид последнего сообщения должно быть равно ид сообщения, которое было вставленно последним",
                        lastMessageId, dialogMessageDAO.selectLastMessageId(dialogId))
            }
        }

        for (user in users) {
            val dialogMessage = insertNewDialogMessage(dialogId, user, true)

            assertEquals("Deleted messages have not to be count", lastMessageId, dialogMessageDAO.selectLastMessageId(dialogId))
        }

    }

    private fun insertNewDialogMessage(dialogId: Long, profile: Profile, isDeleted: Boolean): DialogMessage {
        val dialogMessage = DialogMessage()
        dialogMessage.dialogId = dialogId
        dialogMessage.messageText = "Testtest\u2028\u2029"
        dialogMessage.messageTextHtml = "Testtest\u2028\u2029"
        dialogMessage.timeAdded = Date()
        dialogMessage.userId = profile.profileId
        dialogMessage.isDeleted = isDeleted
        dialogMessageDAO.insert(dialogMessage)

        assertThat(dialogMessage.messageId).isGreaterThan(0)

        val selectedMessage = dialogMessageDAO.selectMessage(dialogId, dialogMessage.messageId)
        assertThat(selectedMessage).isNotNull

        val messages = dialogMessageDAO.selectMessages(dialogId, 0, 100)
        assertThat(messages).isNotNull
        for (message in messages) {
            assertThat(message.messageText!!.contains("\u2028")).isTrue()
            assertThat(message.messageText!!.contains("\u2029")).isTrue()
        }

        return dialogMessage
    }
}

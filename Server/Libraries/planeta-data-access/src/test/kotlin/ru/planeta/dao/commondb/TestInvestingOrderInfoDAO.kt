package ru.planeta.dao.commondb

import java.util.ArrayList
import java.util.Calendar
import java.util.Date
import java.util.GregorianCalendar
import org.junit.Assert
import org.junit.Ignore

import org.junit.Assert.assertEquals
import org.junit.Assert.assertNotNull
import org.junit.Assert.assertNull
import org.junit.Test
import org.springframework.beans.factory.annotation.Autowired
import ru.planeta.model.common.InvestingOrderInfo


/**
 *
 * User: Serge Blagodatskih<stikkas17></stikkas17>@gmail.com><br></br>
 * Date: 27.01.16<br></br>
 * Time: 15:29
 */
class TestInvestingOrderInfoDAO /*{

    @Autowired
    private val dao: InvestingOrderInfoDAO? = null

    private val info = InvestingOrderInfo(1L, Date())

    @Test
    fun testInsert() {
        dao!!.insert(info)
    }

    @Test
    fun testUpdate() {
        dao!!.insert(info)
        var orderInfo = dao.selectCampaignById(1L)
        assertNull(orderInfo!!.lastName)
        assertNull(orderInfo.firstName)
        orderInfo.lastName = "Новый"
        orderInfo.firstName = "Покупатель"
        dao.update(orderInfo)
        orderInfo = dao.selectCampaignById(1L)
        Assert.assertEquals("Новый", orderInfo!!.lastName)
        Assert.assertEquals("Покупатель", orderInfo.firstName)
    }

    @Test
    fun testDelete() {
        dao!!.deleteByProfileId(1L)
        dao.insert(info)
        var orderInfo = dao.selectCampaignById(1L)
        assertNotNull(orderInfo)
        dao.deleteByProfileId(1L)
        orderInfo = dao.selectCampaignById(1L)
        assertNull(orderInfo)
    }

    @Test
    fun testSelect() {
        dao!!.insert(info)
        var orderInfo = dao.selectCampaignById(1L)
        assertNotNull(orderInfo)
        orderInfo = dao.selectCampaignById(10L)
        assertNull(orderInfo)
    }

    @Test
    fun testSelectInInterval() {
        val cal = GregorianCalendar.getInstance()
        cal.set(2016, 1, 26)
        val start = cal.time
        cal.add(Calendar.DATE, 5)
        val end = cal.time

        for (info in dao!!.selectCampaignById(null!!, start, null!!, 0, 20)) {
        }

        for (info in dao.selectCampaignById(null!!, start, end, 0, 20)) {
        }

        for (info in dao.selectCampaignById(null!!, null!!, end, 0, 20)) {
        }

        for (info in dao.selectCampaignById(null!!, null!!, null!!, 0, 20)) {
        }

    }

    @Test
    fun testSelectByIds() {
        val ids = ArrayList<Long>()
        dao!!.selectCampaignById(null!!)
        dao.selectCampaignById(ids)
        for (info in dao.selectCampaignById(0, 5)) {
            ids.add(info.investingOrderInfoId)
        }

        val infos = dao.selectCampaignById(ids)
        if (!ids.isEmpty()) {
            assertEquals(ids.size.toLong(), infos.size.toLong())
            for (info in infos) {
                ids.contains(info.investingOrderInfoId)
            }
        } else {
            assertEquals(true, infos.isEmpty())
        }
    }

    @Test
    fun testCount() {
        dao!!.count()
    }

    @Ignore
    @Test
    fun selectCampaignById() {
        var infos = dao!!.selectCampaignById(0, 2)
        assertNotNull(infos)
        assertEquals(infos.isEmpty(), true)
        dao.insert(info)
        infos = dao.selectCampaignById(0, 2)
        assertNotNull(infos)
        assertEquals(infos.size.toLong(), 1)
    }
}*/

package ru.planeta.dao.profiledb

import org.junit.Assert.*
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.transaction.annotation.Transactional
import ru.planeta.model.profile.Group

/**
 * @author ds.kolyshev
 * Date: 27.10.11
 */
@RunWith(SpringRunner::class)
@Transactional
@ContextConfiguration(locations = ["classpath:applicationContext-common-test.xml"])
class TestGroupDAO {

    @Autowired
    lateinit var groupDAO: GroupDAO

    @Test
    fun testAddUpdateRemove() {
        val group = createGroup()


        groupDAO!!.insert(group)

        val inserted = groupDAO.selectByProfileId(group.profileId!!)
        checkGroup(group, inserted)

        inserted!!.biography = group.biography!! + " 2"
        inserted.biographyHTML = group.biographyHTML!! + " 2"
        inserted.contactInfo = group.contactInfo!! + " 2"
        inserted.siteUrl = group.siteUrl!! + " 2"
        groupDAO.update(inserted)

        val updated = groupDAO.selectByProfileId(group.profileId!!)
        checkGroup(inserted, updated)

        groupDAO.delete(group.profileId!!)
        val deleted = groupDAO.selectByProfileId(group.profileId!!)
        assertNull(deleted)


    }

    @Test
    fun testRotationSelectsAllShards() {
        val group = createGroup()


        groupDAO!!.insert(group)

        val inserted = groupDAO.selectByProfileId(group.profileId!!)
        checkGroup(group, inserted)

        groupDAO.update(inserted!!)

        val updated = groupDAO.selectByProfileId(group.profileId!!)
        checkGroup(inserted, updated)

        groupDAO.delete(group.profileId!!)
        assertNull(groupDAO.selectByProfileId(group.profileId!!))


    }

    private fun createGroup(): Group {
        val group = Group()
        group.profileId = 99999
        group.biography = "test biography"
        group.biographyHTML = "test biography html"
        group.contactInfo = "test contactInfo"
        group.siteUrl = "http://site.url/"
        return group
    }

    private fun checkGroup(expected: Group, actual: Group?) {
        assertNotNull(actual)
        assertEquals(expected.profileId, actual!!.profileId)
        assertEquals(expected.biography, actual.biography)
        assertEquals(expected.biographyHTML, actual.biographyHTML)
        assertEquals(expected.contactInfo, actual.contactInfo)
        assertEquals(expected.siteUrl, actual.siteUrl)
    }
}

package ru.planeta.dao.concertdb

import org.junit.Test
import org.springframework.beans.factory.annotation.Autowired
import ru.planeta.model.concert.Place
import ru.planeta.model.concert.PlaceState


import java.math.BigDecimal

import org.junit.Assert.assertEquals
import org.junit.Ignore
import org.junit.runner.RunWith
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.transaction.annotation.Transactional

/**
 * Created with IntelliJ IDEA.
 * User: michail
 * Date: 14.10.16
 * Time: 17:37
 */
@Ignore
@RunWith(SpringRunner::class)
@Transactional
@ContextConfiguration(locations = ["classpath:applicationContext-common-test.xml"])
class PlaceDAOTest {

    @Autowired
    lateinit var placeDAO: PlaceDAO

    private fun create(): Place {
        val `object` = Place()
        `object`.name = "test"
        `object`.externalUnusedId = 100500
        `object`.externalPlaceId = 100500L
        `object`.sectionId = 1
        `object`.rowId = 2
        `object`.position = 3
        `object`.price = BigDecimal.ONE
        `object`.state = PlaceState.FREE

        return `object`
    }


    private fun clone(`object`: Place): Place {
        val clone = Place()
        clone.placeId = `object`.placeId
        clone.externalPlaceId = `object`.externalPlaceId
        clone.name = `object`.name
        clone.sectionId = `object`.sectionId
        clone.rowId = `object`.rowId
        clone.position = `object`.position
        clone.price = `object`.price
        clone.state = `object`.state
        return clone
    }

    private fun assertPlaceEquals(actual: Place, expected: Place) {
        assertEquals(actual.placeId, expected.placeId)
        assertEquals(actual.externalPlaceId, expected.externalPlaceId)
        assertEquals(actual.name, expected.name)
        assertEquals(actual.sectionId, expected.sectionId)
        assertEquals(actual.rowId, expected.rowId)
        assertEquals(actual.position.toLong(), expected.position.toLong())
        assertEquals(actual.price!!.compareTo(expected.price!!).toLong(), 0)
        assertEquals(actual.state, expected.state)
    }

    @Test
    fun testPlaceCRU() {
        var actual = create()
        placeDAO!!.insert(actual)
        var expected = clone(actual)
        actual = placeDAO!!.select(actual.placeId)
        assertPlaceEquals(actual, expected)

        actual.name = "test2"
        actual.price = BigDecimal.TEN
        actual.state = PlaceState.SOLD
        expected = clone(actual)
        placeDAO!!.update(actual)
        actual = placeDAO!!.select(actual.placeId)
        assertPlaceEquals(actual, expected)
    }


}

package ru.planeta.dao.commondb


import org.assertj.core.api.Assertions.assertThat
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.transaction.annotation.Transactional
import ru.planeta.model.mail.MailAttachment
import ru.planeta.model.mail.MailTemplate

/**
 * Mail templates DAO tests
 *
 * @author ds.kolyshev
 * Date: 12.01.12
 */
@RunWith(SpringRunner::class)
@Transactional
@ContextConfiguration(locations = ["classpath:applicationContext-common-test.xml"])
class TestMailTemplateDAO {

    @Autowired
    lateinit var mailTemplateDAO: MailTemplateDAO

    @Autowired
    lateinit var mailAttachmentDAO: MailAttachmentDAO

    @Test
    fun testAddUpdateDelete() {


        val mailTemplate = createMailTemplate()
        mailTemplateDAO.insert(mailTemplate)
        assertThat(mailTemplate.templateId).isNotNull().isGreaterThan(0)

        val mailAttachment = createMailAttachment()
        mailAttachment.templateId = mailTemplate.templateId
        mailAttachmentDAO.insert(mailAttachment)
        assertThat(mailAttachment.attachmentId).isNotNull().isGreaterThan(0)

        val list = mailTemplateDAO.selectMailTemplates()
        assertThat(list).isNotNull.isNotEmpty

        val selected = mailTemplateDAO.select(mailTemplate.templateId!!)
        assertTemplateEquals(selected, mailTemplate)
        assertThat(selected.attachments).isNotNull.isNotEmpty

        val selectedByName = mailTemplateDAO.selectByName(mailTemplate.name!!)
        assertTemplateEquals(selectedByName, mailTemplate)

        mailTemplate.contentBbcode = mailTemplate.contentBbcode + " 2"
        mailTemplate.fromAddress = mailTemplate.fromAddress + " 2"
        mailTemplate.name = mailTemplate.name + " 2"
        mailTemplate.subject = mailTemplate.subject + " 2"

        mailTemplateDAO.update(mailTemplate)

        val updated = mailTemplateDAO.select(mailTemplate.templateId!!)
        assertTemplateEquals(updated, mailTemplate)

        mailTemplateDAO.delete(mailTemplate.templateId!!)
        assertThat(mailTemplateDAO.select(mailTemplate.templateId!!)).isNull()
    }

    private fun assertTemplateEquals(actual: MailTemplate, expected: MailTemplate) {
        assertThat(expected.templateId).isEqualTo(actual.templateId)
        assertThat(expected.contentBbcode).isEqualTo(actual.contentBbcode)
        assertThat(expected.fromAddress).isEqualTo(actual.fromAddress)
        assertThat(expected.name).isEqualTo(actual.name)
        assertThat(expected.subject).isEqualTo(actual.subject)
    }

    private fun createMailTemplate(): MailTemplate {
        val mailTemplate = MailTemplate()
        mailTemplate.contentBbcode = "content bbcode test[]"
        mailTemplate.fromAddress = "testfromaddress@mail.ru"
        mailTemplate.name = "test template name"
        mailTemplate.subject = "test mail subject"
        return mailTemplate
    }

    private fun createMailAttachment(): MailAttachment {
        val mailAttachment = MailAttachment()
        mailAttachment.fileName = "test.html"
        mailAttachment.mimeType = "text/html"
        mailAttachment.content = "content".toByteArray()
        return mailAttachment
    }
}

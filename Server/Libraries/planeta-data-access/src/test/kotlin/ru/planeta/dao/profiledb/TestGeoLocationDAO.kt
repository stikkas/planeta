package ru.planeta.dao.profiledb


import org.junit.Assert.*
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.transaction.annotation.Transactional
import ru.planeta.model.profile.location.GeoConstants

@RunWith(SpringRunner::class)
@Transactional
@ContextConfiguration(locations = ["classpath:applicationContext-common-test.xml"])
class TestGeoLocationDAO {

    @Autowired
    lateinit var geoLocationDAO: GeoLocationDAO

    @Test
    fun testCitiesList() {
        val countries = geoLocationDAO.countries()
        assertFalse(countries.isEmpty())
        assertEquals(GeoConstants.TOTAL_COUNTRIES_COUNT.toLong(), countries.size.toLong())
        assertNotNull(countries[0])

        val cities = geoLocationDAO.getCountryCitiesBySubstring(GeoConstants.RUSSIA.toLong(), "Москва", 0, 100)
        assertFalse(cities.isEmpty())
        assertNotNull(cities[0])
    }
}

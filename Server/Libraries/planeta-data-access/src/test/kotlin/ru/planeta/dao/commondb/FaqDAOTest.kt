package ru.planeta.dao.commondb

import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.transaction.annotation.Transactional
import ru.planeta.model.common.faq.FaqSnippet


@RunWith(SpringRunner::class)
@Transactional
@ContextConfiguration(locations = ["classpath:applicationContext-common-test.xml"])
class FaqDAOTest {
    @Autowired
    lateinit var faqDAO: FaqDAO

    @Test
    @Throws(Exception::class)
    fun testSearchParagraphs() {
        val snippets = faqDAO.search("акция", 0, 0)
        Assert.assertNotNull(snippets)
        Assert.assertFalse(snippets.isEmpty())
        val snippet = snippets[0]
        Assert.assertNotNull(snippet)
        Assert.assertNotNull(snippet.article)
        Assert.assertNotNull(snippet.category)
        Assert.assertNotNull(snippet.getAnnotation())
    }

}

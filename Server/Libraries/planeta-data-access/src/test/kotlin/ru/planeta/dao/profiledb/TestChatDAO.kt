package ru.planeta.dao.profiledb


import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.transaction.annotation.Transactional
import ru.planeta.TestHelper
import ru.planeta.TestHelper.TEST_PROFILE_ID
import ru.planeta.model.enums.ObjectType
import ru.planeta.model.profile.chat.Chat
import java.util.*

/**
 * @author m.shulepov
 */
@RunWith(SpringRunner::class)
@Transactional
@ContextConfiguration(locations = ["classpath:applicationContext-common-test.xml"])
class TestChatDAO {

    @Autowired
    lateinit var chatDAO: ChatDAO

    @Test
    fun testChatAddUpdateRemove() {
        val profile = TestHelper.createUser(TEST_PROFILE_ID)

        val chat = Chat()
        chat.timeAdded = Date()
        chat.profileId = profile.profileId
        chat.ownerObjectId = 100000
        chat.setOwnerObjectTypeCode(ObjectType.BROADCAST.code)
        chatDAO!!.insert(chat)
        assertTrue(chat.chatId > 0)

        var selected = chatDAO.select(profile.profileId, chat.chatId)

        assertCorrectChat(chat, selected)

        selected = chatDAO.selectByOwnerObject(profile.profileId, chat.ownerObjectId)

        assertCorrectChat(chat, selected)


    }

    private fun assertCorrectChat(chat: Chat, selected: Chat) {
        assertEquals(chat.ownerObjectId, selected.ownerObjectId)
        assertEquals(chat.objectType, selected.objectType)
        assertEquals(chat.chatId, selected.chatId)
        assertEquals(chat.profileId, selected.profileId)
    }

}

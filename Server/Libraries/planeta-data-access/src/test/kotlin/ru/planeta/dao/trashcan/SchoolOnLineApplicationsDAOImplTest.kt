package ru.planeta.dao.trashcan

import org.junit.Assert.assertEquals
import org.junit.Assert.assertNotNull
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.transaction.annotation.Transactional
import ru.planeta.model.trashcan.SchoolOnlineCourseApplication

@RunWith(SpringRunner::class)
@Transactional
@ContextConfiguration(locations = ["classpath:applicationContext-common-test.xml"])
class SchoolOnLineApplicationsDAOImplTest {
    @Autowired
    lateinit var schoolOnLineApplicationsDAO: SchoolOnLineApplicationsDAO

    @Test
    @Throws(Exception::class)
    fun insertSelect() {
        val name = "Name01"
        val email = "adshfajdnflaenflaewbffb@sfasdklfjnadklfnakgn.qw"
        val inviteSent = false

        val application = SchoolOnlineCourseApplication()
        application.name = name
        application.email = email
        application.isInviteSent = inviteSent

        schoolOnLineApplicationsDAO.insert(application)

        val selectedOnlineCourseApplication = schoolOnLineApplicationsDAO.select(email)
        assertNotNull(selectedOnlineCourseApplication)
        assertEquals(name, selectedOnlineCourseApplication.name)
        assertEquals(inviteSent, selectedOnlineCourseApplication.isInviteSent)
        assertEquals(application.applicationId, selectedOnlineCourseApplication.applicationId)
    }

}

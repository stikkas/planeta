package ru.planeta.dao.commondb

import org.junit.Ignore
import org.junit.Test
import org.springframework.beans.factory.annotation.Autowired
import ru.planeta.model.common.*
import ru.planeta.model.common.campaign.CampaignSearchFilter
import ru.planeta.model.common.campaign.Share
import ru.planeta.model.common.delivery.SubjectType
import ru.planeta.model.enums.OrderObjectType
import ru.planeta.model.enums.OrderObjectType.*
import ru.planeta.model.shop.Product
import ru.planeta.model.shop.enums.DeliveryStatus
import ru.planeta.model.shop.enums.DeliveryType
import ru.planeta.model.shop.enums.PaymentStatus
import ru.planeta.model.shop.enums.PaymentType


import java.math.BigDecimal
import java.util.*

import org.junit.Assert.*
import org.junit.runner.RunWith
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.transaction.annotation.Transactional
import ru.planeta.TestHelper
import ru.planeta.dao.profiledb.ProfileDAO

/**
 * Class TestOrderDAO
 *
 * @author a.tropnikov
 */
@RunWith(SpringRunner::class)
@Transactional
@ContextConfiguration(locations = ["classpath:applicationContext-common-test.xml"])
class TestOrderDAO {

    private var ID_STUB = 1L
    @Autowired
    lateinit var orderDAO: OrderDAO

    @Autowired
    lateinit var orderObjectDAO: OrderObjectDAO

    @Autowired
    lateinit var profileDAO: ProfileDAO

    @Autowired
    lateinit var campaignDAO: CampaignDAO

    @Autowired
    lateinit var shareDAO: ShareDAO

    private val nextIdStub: Long
        get() = ++ID_STUB

    @Test
    fun testCRUD() {
        val inserted = insertOrder(Random().nextLong(), OrderObjectType.TICKET, PaymentStatus.PENDING, PaymentType.NOT_SET, DeliveryStatus.PENDING, DeliveryType.NOT_SET)
        assertTrue(inserted.orderId != 0L)

        var selected = orderDAO.select(inserted.orderId)!!
        assertOrderEquals(inserted, selected)

        inserted.buyerId = inserted.buyerId + 1
        inserted.orderType = OrderObjectType.SHARE
        inserted.topayTransactionId = Random().nextLong()
        inserted.debitTransactionId = Random().nextLong()
        inserted.creditTransactionId = Random().nextLong()
        inserted.cancelDebitTransactionId = Random().nextLong()
        inserted.cancelCreditTransactionId = Random().nextLong()
        inserted.paymentStatus = PaymentStatus.COMPLETED
        inserted.paymentType = PaymentType.CASH
        inserted.deliveryStatus = DeliveryStatus.DELIVERED
        inserted.deliveryType = DeliveryType.DELIVERY
        inserted.deliveryPrice = inserted.deliveryPrice.add(BigDecimal.ONE)
        inserted.deliveryDepartmentId = nextIdStub
        inserted.timeAdded = Date()
        inserted.timeUpdated = Date()
        inserted.totalPrice = BigDecimal.TEN

        orderDAO.update(cloneOrder(inserted))
        selected = orderDAO.select(inserted.orderId)!!
        assertOrderEquals(inserted, selected)

        orderDAO.delete(inserted.orderId)
        assertNull(orderDAO.select(inserted.orderId))
    }

    @Test
    fun testCharityOrder() {
        val buyerId = Random().nextLong()
        val order = insertOrder(buyerId, SHARE, PaymentStatus.COMPLETED, PaymentType.NOT_SET, DeliveryStatus.PENDING, DeliveryType.NOT_SET)
        orderDAO.isCharityOrder(order.orderId)
    }

    @Test
    fun testCampaignOrders() {
        val buyerId = Random().nextLong()
        val profile = TestHelper.createUserProfile()
        profileDAO.insert(profile)
        val campaign = TestHelper.createCampaign(profile.profileId)
        campaignDAO.insert(campaign)
        val share = TestHelper.createShare(profile.profileId, campaign.campaignId)
        shareDAO.insert(share)
        val order = insertOrder(buyerId, OrderObjectType.SHARE, PaymentStatus.CANCELLED, PaymentType.PAYMENT_SYSTEM, DeliveryStatus.CANCELLED, DeliveryType.CUSTOMER_PICKUP)
        val orderObject = OrderObject()
        orderObject.orderId = order.orderId
        orderObject.merchantId = profile.profileId
        orderObject.objectId = share.shareId
        orderObject.orderObjectType = OrderObjectType.SHARE
        orderObject.price = share.price
        orderObjectDAO.insert(orderObject)

        val campaignSearchFilter = CampaignSearchFilter()
        campaignSearchFilter.campaignId = campaign.campaignId

        assertEquals(campaign.campaignId, orderDAO.selectCampaignIdByOrderId(order.orderId))
    }

    @Test
    fun testSelects() {
        val buyerId = Math.abs(Random().nextLong())
        val merchantId = buyerId + 11

        val buyerCounter = Counter()
        val merchantCounter = Counter()


        val shareOrder = insertOrder(buyerId, SHARE, PaymentStatus.PENDING, PaymentType.NOT_SET, DeliveryStatus.PENDING, DeliveryType.NOT_SET)
        val `object` = insertOrderObject(shareOrder.orderId, merchantId)
        val firstOrderCreatedDate = shareOrder.timeAdded

        assertEquals(1, orderDAO.selectUserOrdersCount(buyerId, `object`.objectId, SHARE).toLong())

        val allMerchantReceivedOrders = orderDAO.getMerchantOrdersInfo(merchantId, null, null, null, null, null, 0, 0)
        assertEquals(0, allMerchantReceivedOrders.size.toLong())

        val allBuyerOrders = orderDAO.selectBuyerOrders(buyerId, emptySet(), null, null, null, null, 0, 0)
        assertEquals(0, allBuyerOrders.size.toLong())


        val shopOrder = insertOrder(buyerId, PRODUCT, PaymentStatus.PENDING, PaymentType.CASH, DeliveryStatus.PENDING, DeliveryType.DELIVERY)
        insertOrderObject(shopOrder.orderId, merchantId)
        buyerCounter.inc(PRODUCT)
        merchantCounter.inc(PRODUCT)

        assertEquals(merchantCounter.totalCount.toLong(), orderDAO.getMerchantOrdersInfo(merchantId, null, null, null, null, null, 0, 0).size.toLong())
        assertEquals(buyerCounter.totalCount.toLong(), orderDAO.selectBuyerOrders(buyerId, emptyList(), null, null, null, null, 0, 0).size.toLong())


        val shareOrder2 = insertOrder(buyerId, SHARE, PaymentStatus.COMPLETED, PaymentType.PAYMENT_SYSTEM, DeliveryStatus.DELIVERED, DeliveryType.NOT_SET)
        val share1 = insertOrderObject(shareOrder2.orderId, merchantId)
        val share1Id = share1.objectId
        buyerCounter.inc(SHARE)
        merchantCounter.inc(SHARE)

        assertEquals(1, orderDAO.selectUserOrdersCount(buyerId, share1Id, SHARE).toLong())
        assertEquals(merchantCounter[SHARE].toLong(), orderDAO.getMerchantOrdersInfo(merchantId, SHARE, PaymentStatus.COMPLETED, DeliveryStatus.DELIVERED, null, null, 0, 0).size.toLong())
        assertEquals(buyerCounter[SHARE].toLong(), orderDAO.selectBuyerOrders(buyerId, EnumSet.of<OrderObjectType>(SHARE), null, null, null, null, 0, 0).size.toLong())
        assertEquals(1, orderDAO.selectBuyerOrders(buyerId, emptySet<OrderObjectType>(), PaymentStatus.COMPLETED,
                DeliveryStatus.DELIVERED, null, null, 0, 0).size.toLong())


        val futureShareOrder = insertOrder(buyerId, SHARE, PaymentStatus.CANCELLED, PaymentType.NOT_SET, DeliveryStatus.REJECTED, DeliveryType.NOT_SET)
        val futureDateAdded = updateOrderToFuture(futureShareOrder)

        val share2 = insertOrderObject(futureShareOrder.orderId, merchantId)
        val share2Id = share2.objectId
        buyerCounter.inc(SHARE)
        merchantCounter.inc(SHARE)

        val prevDay = getDaysShiftedDate(futureDateAdded, -1)
        val nextDay = getDaysShiftedDate(futureDateAdded, 1)
        assertEquals(0, orderDAO.selectUserOrdersCount(buyerId, share2Id, SHARE).toLong())
        assertEquals(1, orderDAO.getMerchantOrdersInfo(merchantId, null, null, null, prevDay, nextDay, 0, 0).size.toLong())
        assertEquals(1, orderDAO.selectBuyerOrders(buyerId, emptySet<OrderObjectType>(), null, null, prevDay, nextDay, 0, 0).size.toLong())


        val cancelledShopOrder = insertOrder(buyerId, PRODUCT, PaymentStatus.CANCELLED, PaymentType.CASH, DeliveryStatus.CANCELLED, DeliveryType.DELIVERY)
        insertOrderObject(cancelledShopOrder.orderId, merchantId)
        insertOrderObject(cancelledShopOrder.orderId, merchantId)
//        assertEquals(buyerCounter[PRODUCT].toLong(), orderDAO.selectBuyerOrders(buyerId, EnumSet.of<OrderObjectType>(PRODUCT), null, null, null, null, 0, 0).size.toLong())

        val ordersInAdminInterface = orderDAO.getMerchantOrdersInfo(merchantId, null, null, null, firstOrderCreatedDate, null, 0, 0)
        assertEquals(4, ordersInAdminInterface.size.toLong())

        val orderByDebitTransaction = orderDAO.selectOrdersByBuyerDebitTransactionIds(listOf(futureShareOrder.cancelDebitTransactionId))
        assertNotNull(orderByDebitTransaction)
        assertEquals(1, orderByDebitTransaction.size.toLong())
        assertOrderEquals(futureShareOrder, orderByDebitTransaction[0])

        val orderByCreditTransaction = orderDAO.selectOrdersByBuyerCreditTransactionIds(listOf(futureShareOrder.creditTransactionId))
        assertNotNull(orderByCreditTransaction)
        assertEquals(1, orderByCreditTransaction.size.toLong())
        assertOrderEquals(futureShareOrder, orderByCreditTransaction[0])

        val allSharesOrders = orderDAO.selectOrdersByObject(share2Id, SHARE, null, null)
        assertNotNull(allSharesOrders)
        assertEquals(1, allSharesOrders.size.toLong())

        val shareCancelledOrders = orderDAO.selectOrdersByObject(share2Id, SHARE, null, PaymentStatus.CANCELLED)
        assertNotNull(shareCancelledOrders)
        assertEquals(1, shareCancelledOrders.size.toLong())

        val shareDeliveredOrders = orderDAO.selectOrdersByObject(share2Id, SHARE, DeliveryStatus.DELIVERED, null)
        assertEquals(0, shareDeliveredOrders.size.toLong())

        var count = orderDAO.selectOrdersCountForObject(share2Id, SHARE, EnumSet.of(PaymentStatus.CANCELLED), null)
        assertEquals(1, count.toLong())
        count = orderDAO.selectOrdersCountForObject(share1Id, SHARE, EnumSet.allOf(PaymentStatus::class.java), EnumSet.allOf(DeliveryStatus::class.java))
        assertEquals(1, count.toLong())
    }

    private fun updateOrderToFuture(futureShareOrder: Order): Date {
        val dateAdded = getDaysShiftedDate(Date(), 2)
        futureShareOrder.timeAdded = dateAdded
        orderDAO.update(futureShareOrder)
        return dateAdded
    }

    private fun insertOrder(buyerId: Long, orderType: OrderObjectType,
                            paymentStatus: PaymentStatus, paymentType: PaymentType,
                            deliveryStatus: DeliveryStatus, deliveryType: DeliveryType): Order {
        val order = Order()
        val now = Date()
        order.buyerId = buyerId
        order.orderType = orderType
        order.topayTransactionId = Random().nextLong()
        order.debitTransactionId = Random().nextLong()
        order.creditTransactionId = Random().nextLong()
        order.cancelDebitTransactionId = Random().nextLong()
        order.cancelCreditTransactionId = Random().nextLong()
        order.paymentStatus = paymentStatus
        order.paymentType = paymentType
        order.deliveryStatus = deliveryStatus
        order.deliveryType = deliveryType
        order.deliveryPrice = BigDecimal.ONE
        order.deliveryDepartmentId = nextIdStub
        order.timeAdded = now
        order.timeUpdated = now
        order.totalPrice = BigDecimal.ONE
        orderDAO.insert(order)
        return order
    }

    private fun insertOrderObject(orderId: Long, merchantId: Long): OrderObject {
        val order = orderDAO.select(orderId)
        assertNotNull(order)
        val orderType = order?.orderType

        val orderObject = OrderObject()
        orderObject.orderId = orderId
        orderObject.merchantId = merchantId
        orderObject.objectId = Random().nextLong()
        orderObject.orderObjectType = orderType
        orderObject.price = BigDecimal.ONE

        orderObjectDAO.insert(orderObject)
        return orderObjectDAO.select(orderObject.orderObjectId)
    }

    private class Counter internal constructor() {
        private val counters: MutableMap<OrderObjectType, Int>
        internal var totalCount: Int = 0
            private set

        init {
            counters = HashMap()
            for (type in EnumSet.allOf(OrderObjectType::class.java)) {
                counters[type] = 0
            }
        }

        internal operator fun get(type: OrderObjectType): Int {
            return counters[type] ?: 0
        }

        internal fun inc(type: OrderObjectType) {
            counters[type] = get(type) + 1
            totalCount++
        }
    }

    @Ignore
    @Test
    fun testSelectCommonData() {
        val data = orderDAO.selectOrderInfoCommonData(870879, 1133467, 0, SubjectType.SHARE)
    }

    @Test
    fun selectMinCompletedOrderId() {
        val buyerId = Random().nextLong()

        val firstOrder = insertOrder(buyerId, OrderObjectType.SHARE, PaymentStatus.COMPLETED, PaymentType.PAYMENT_SYSTEM, DeliveryStatus.CANCELLED, DeliveryType.CUSTOMER_PICKUP)
        val secondOrder = insertOrder(buyerId, OrderObjectType.SHARE, PaymentStatus.COMPLETED, PaymentType.PAYMENT_SYSTEM, DeliveryStatus.CANCELLED, DeliveryType.CUSTOMER_PICKUP)

        val minCompletedOrderId = orderDAO.selectMinCompletedOrderId(buyerId, OrderObjectType.SHARE)
        assertTrue(firstOrder.orderId == minCompletedOrderId)
    }

    companion object {

        private val clazzByType = object : HashMap<OrderObjectType, Class<*>>() {
            init {
                put(SHARE, Share::class.java)
                put(PRODUCT, Product::class.java)
                put(TICKET, Product::class.java)
            }
        }

        private fun cloneOrder(original: Order): Order {
            val result = Order()
            result.orderId = original.orderId
            result.orderType = original.orderType
            result.buyerId = original.buyerId
            result.topayTransactionId = original.topayTransactionId
            result.debitTransactionId = original.debitTransactionId
            result.creditTransactionId = original.creditTransactionId
            result.cancelDebitTransactionId = original.cancelDebitTransactionId
            result.cancelCreditTransactionId = original.cancelCreditTransactionId
            result.paymentStatus = original.paymentStatus
            result.paymentType = original.paymentType
            result.deliveryStatus = original.deliveryStatus
            result.deliveryType = original.deliveryType
            result.deliveryPrice = original.deliveryPrice
            result.deliveryDepartmentId = original.deliveryDepartmentId
            result.timeAdded = original.timeAdded
            result.timeUpdated = original.timeUpdated
            result.totalPrice = original.totalPrice

            return result
        }

        private fun assertOrderEquals(expected: Order, actual: Order) {
            assertNotNull(expected)
            assertNotNull(actual)
            assertEquals(expected.orderId, actual.orderId)
            assertEquals(expected.orderType, actual.orderType)
            assertEquals(expected.buyerId, actual.buyerId)
            assertEquals(expected.topayTransactionId, actual.topayTransactionId)
            assertEquals(expected.debitTransactionId, actual.debitTransactionId)
            assertEquals(expected.creditTransactionId, actual.creditTransactionId)
            assertEquals(expected.cancelDebitTransactionId, actual.cancelDebitTransactionId)
            assertEquals(expected.cancelCreditTransactionId, actual.cancelCreditTransactionId)
            assertEquals(expected.paymentStatus, actual.paymentStatus)
            assertEquals(expected.paymentType, actual.paymentType)
            assertEquals(expected.deliveryStatus, actual.deliveryStatus)
            assertEquals(expected.deliveryType, actual.deliveryType)
            assertEquals(expected.deliveryPrice.compareTo(actual.deliveryPrice).toLong(), 0)
            assertEquals(expected.deliveryDepartmentId, actual.deliveryDepartmentId)
            assertEquals(expected.timeAdded, actual.timeAdded)
            assertEquals(expected.totalPrice.setScale(2, BigDecimal.ROUND_HALF_DOWN), actual.totalPrice.setScale(2, BigDecimal.ROUND_HALF_DOWN))
        }

        fun getDaysShiftedDate(date: Date, dayShift: Int): Date {
            val calendar = Calendar.getInstance()
            calendar.time = date
            calendar.set(Calendar.DAY_OF_MONTH, calendar.get(Calendar.DAY_OF_MONTH) + dayShift)

            return calendar.time
        }
    }
}

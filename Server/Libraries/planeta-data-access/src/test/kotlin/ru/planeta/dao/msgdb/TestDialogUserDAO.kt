package ru.planeta.dao.msgdb

import org.junit.Assert.assertEquals
import org.junit.Assert.assertNotNull
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.transaction.annotation.Transactional
import ru.planeta.TestHelper
import ru.planeta.dao.profiledb.ProfileDAO
import ru.planeta.model.msg.DialogUser
import java.util.*

/**
 * @author ameshkov
 */

@RunWith(SpringRunner::class)
@Transactional
@ContextConfiguration(locations = ["classpath:applicationContext-common-test.xml"])
class TestDialogUserDAO {

    @Autowired
    lateinit var dialogUserDAO: DialogUserDAO

    @Autowired
    lateinit var profileDAO: ProfileDAO

    @Test
    fun testDialogUserDAOAddRemove() {
        val dialogId = Random().nextLong()

        for (i in 0..9) {
            val profile = TestHelper.createUserProfile()
            profileDAO.insert(profile)
            val dialogUser = DialogUser()
            dialogUser.dialogId = dialogId
            dialogUser.profileId = profile.profileId
            dialogUserDAO.insert(dialogUser)
        }

        val dialogUsers = dialogUserDAO.select(dialogId)
        assertNotNull(dialogUsers)
        assertEquals(10, dialogUsers.size.toLong())
        assertNotNull(dialogUsers[0].displayName)

    }
}

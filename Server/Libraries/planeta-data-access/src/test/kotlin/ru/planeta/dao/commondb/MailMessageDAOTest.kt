package ru.planeta.dao.commondb

import org.apache.commons.lang3.time.DateUtils
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.transaction.annotation.Transactional

import java.util.Date

/**
 * Created with IntelliJ IDEA.
 * User: michail
 * Date: 14.12.2016
 * Time: 13:25
 */
@RunWith(SpringRunner::class)
@Transactional
@ContextConfiguration(locations = ["classpath:applicationContext-common-test.xml"])
class MailMessageDAOTest {
    @Autowired
    lateinit var mailMessageDAO: MailMessageDAO

    @Test
    fun testCleanLogs() {
        mailMessageDAO.cleanMailNotificationsLogs(Date())
    }

    @Test
    fun testMoveLogs() {
        mailMessageDAO.moveOldMailNotifications(DateUtils.addDays(Date(), -1))
    }

}

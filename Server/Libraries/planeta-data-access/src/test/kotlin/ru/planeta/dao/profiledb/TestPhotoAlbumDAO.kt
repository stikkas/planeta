package ru.planeta.dao.profiledb


import org.junit.Assert.*
import org.junit.Ignore
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.transaction.annotation.Transactional
import ru.planeta.TestHelper
import ru.planeta.model.enums.PermissionLevel
import ru.planeta.model.profile.media.PhotoAlbum
import java.util.*

/**
 * @author ds.kolyshev
 * Date: 27.10.11
 */
@Ignore
@RunWith(SpringRunner::class)
@Transactional
@ContextConfiguration(locations = ["classpath:applicationContext-common-test.xml"])
class TestPhotoAlbumDAO {

    @Autowired
    lateinit var photoAlbumDAO: PhotoAlbumDAO

    @Test
    fun testAddUpdateRemove() {


        val photoAlbum = createAlbum()
        photoAlbumDAO!!.insert(photoAlbum)

        val inserted = photoAlbumDAO.selectAlbumById(photoAlbum.profileId!!, photoAlbum.getAlbumId()!!)
        checkPhotoAlbums(photoAlbum, inserted)

        assertNotNull(photoAlbumDAO.selectAlbumByType(photoAlbum.profileId!!, photoAlbum.albumTypeId))

        photoAlbum.timeUpdated = Date()
        photoAlbum.imageId = 121
        photoAlbum.imageUrl = "Blahblahblah"
        photoAlbumDAO.update(photoAlbum)

        val updated = photoAlbumDAO.selectAlbumById(photoAlbum.profileId!!, photoAlbum.getAlbumId()!!)

        checkPhotoAlbums(photoAlbum, updated)


        photoAlbum.viewPermission = PermissionLevel.FRIENDS
        photoAlbumDAO.update(photoAlbum)

        photoAlbumDAO.delete(photoAlbum.profileId!!, photoAlbum.getAlbumId()!!)
        assertNull(photoAlbumDAO.selectAlbumById(photoAlbum.profileId!!, photoAlbum.getAlbumId()!!))


    }

    private fun checkPhotoAlbums(expected: PhotoAlbum, actual: PhotoAlbum?) {
        assertNotNull(actual)
        assertEquals(expected.getAlbumId(), actual!!.getAlbumId())
        assertEquals(expected.profileId, actual.profileId)
        assertEquals(expected.authorProfileId, actual.authorProfileId)
        assertEquals(expected.photosCount.toLong(), actual.photosCount.toLong())
        assertEquals(expected.tagIds, actual.tagIds)
        assertEquals(expected.imageId, actual.imageId)
        assertEquals(expected.imageUrl, actual.imageUrl)
        assertEquals(expected.albumTypeId.toLong(), actual.albumTypeId.toLong())
        assertEquals(expected.viewsCount.toLong(), actual.viewsCount.toLong())
        assertEquals(expected.viewPermission, actual.viewPermission)
    }

    private fun createAlbum(): PhotoAlbum {
        val photoAlbum = PhotoAlbum()
        photoAlbum.setAlbumId(0)
        photoAlbum.profileId = 999999
        photoAlbum.authorProfileId = 999991
        photoAlbum.photosCount = 0
        photoAlbum.timeAdded = Date()
        photoAlbum.timeUpdated = Date()
        photoAlbum.viewPermission = PermissionLevel.EVERYBODY
        photoAlbum.viewsCount = 0
        photoAlbum.albumTypeId = 1
        photoAlbum.title = "Album title"
        return photoAlbum
    }

    @Ignore
    @Test
    fun authorInfoTest() {
        val profile = TestHelper.newProfile()
        val photoAlbum = createAlbum()
        photoAlbum.profileId = profile.profileId
        photoAlbum.authorProfileId = profile.profileId
        photoAlbumDAO!!.insert(photoAlbum)
        val inserted = photoAlbumDAO.selectAlbumById(photoAlbum.profileId!!, photoAlbum.getAlbumId()!!)
        assertNotNull(inserted)
        assertEquals("display name", inserted!!.authorName)
        assertEquals("display name", inserted.ownerName)

    }
}

package ru.planeta.dao.profiledb

import org.assertj.core.api.Assertions.assertThat
import org.junit.Ignore
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.transaction.annotation.Transactional
import ru.planeta.commons.model.Gender
import ru.planeta.model.enums.GroupCategory
import ru.planeta.model.enums.ProfileStatus
import ru.planeta.model.enums.ProfileType
import ru.planeta.model.profile.Profile
import ru.planeta.model.profiledb.ProfileSubscription
import java.util.*

/**
 * Class TestProfileRelationDAO
 *
 * @author a.tropnikov
 */
@RunWith(SpringRunner::class)
@Transactional
@ContextConfiguration(locations = ["classpath:applicationContext-common-test.xml"])
class TestProfileRelationDAO {

    @Autowired
    lateinit var profileDAO: ProfileDAO

    @Autowired
    lateinit var profileSubscriptionMapper: ProfileSubscriptionMapperDAO

    @Autowired
    lateinit var profileSubscriptionDAO: ProfileSubscriptionDAO

    @Ignore
    @Test
    fun testGroupRelationSelection() {
        val profile = createProfile(0, 0)
        profileDAO.insert(profile)

        val secondProfile = createProfile(0, profile.profileId)
        secondProfile.alias = "alias2"
        secondProfile.status = ProfileStatus.GROUP_ACTIVE
        secondProfile.profileType = ProfileType.GROUP
        profileDAO.insert(secondProfile)

        profileSubscriptionMapper.insertSelective(ProfileSubscription.builder
                .profileId(profile.profileId)
                .subjectProfileId(secondProfile.profileId)
                .isAdmin(true)
                .build())

        val groupIds = profileSubscriptionDAO.selectUserOwnGroupsId(profile.profileId)
        assertThat(groupIds.size).isEqualTo(1)

        val profile3 = createProfile(0, profile.profileId)
        profile3.alias = "alias3"
        profile3.profileType = ProfileType.GROUP
        profile3.status = ProfileStatus.GROUP_DELETED
        profileDAO.insert(profile3)

        profileSubscriptionMapper.insertSelective(ProfileSubscription.builder.profileId(profile3.profileId).subjectProfileId(profile.profileId).isAdmin(true).build())


        val groupIds2 = profileSubscriptionDAO.selectUserOwnGroupsId(profile.profileId)
        assertThat(groupIds2.size).isEqualTo(1)
    }


    private fun createProfile(profileId: Long, creatorId: Long): Profile {
        val profile = Profile()
        profile.profileId = profileId
        profile.creatorProfileId = creatorId
        profile.profileType = ProfileType.USER
        profile.imageUrl = "/imageurl"
        profile.imageId = 1
        profile.displayName = "display name"
        profile.alias = "alias"
        profile.status = ProfileStatus.NOT_SET
        profile.cityId = 1
        profile.countryId = 1
        profile.userBirthDate = Date()
        profile.userGender = Gender.NOT_SET


        profile.groupCategory = GroupCategory.MUSIC
        return profile
    }


}

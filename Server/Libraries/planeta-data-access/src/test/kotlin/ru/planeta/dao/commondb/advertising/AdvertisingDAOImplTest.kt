package ru.planeta.dao.commondb.advertising

import org.junit.Test
import org.springframework.beans.factory.annotation.Autowired
import ru.planeta.model.advertising.AdvertisingDTO
import ru.planeta.model.advertising.AdvertisingState

import org.junit.Assert.assertTrue
import org.junit.runner.RunWith
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.transaction.annotation.Transactional
import ru.planeta.dao.commondb.AdvertisingDAO


/**
 * Created with IntelliJ IDEA.
 * Date: 07.03.14
 * Time: 13:11
 */
@RunWith(SpringRunner::class)
@Transactional
@ContextConfiguration(locations = ["classpath:applicationContext-common-test.xml"])
class AdvertisingDAOImplTest {
    @Autowired
    lateinit var advertisingDAO: AdvertisingDAO

    @Test
    @Throws(Exception::class)
    fun testSaveAdvertising() {
        var dto = AdvertisingDTO()
        dto.broadcastId = 1
        dto.buttonText = "test name"
        dto.campaignId = 0
        dto.state = AdvertisingState.THIS
        dto.vastXml = "some VAST xml"

        advertisingDAO!!.saveAdvertising(dto)

        dto = advertisingDAO.getAdvertising(1, 0, AdvertisingState.THIS)[0]

        assertTrue(dto.broadcastId == 1L)

        val newText = "newText"

        dto.vastXml = newText

        advertisingDAO.saveAdvertising(dto)

        dto = advertisingDAO.getAdvertising(1, 0, null!!)[0]
        assertTrue(newText == dto.vastXml)
    }
}

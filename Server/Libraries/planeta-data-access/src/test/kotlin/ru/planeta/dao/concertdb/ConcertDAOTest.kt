package ru.planeta.dao.concertdb

import org.junit.Test
import org.springframework.beans.factory.annotation.Autowired
import ru.planeta.model.concert.Concert
import ru.planeta.model.concert.ConcertStatus


import org.junit.Assert.*
import org.junit.Ignore
import org.junit.runner.RunWith
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.transaction.annotation.Transactional

import java.util.Date

/**
 * Created with IntelliJ IDEA.
 * User: michail
 * Date: 14.10.16
 * Time: 12:51
 */
@Ignore
@RunWith(SpringRunner::class)
@Transactional
@ContextConfiguration(locations = ["classpath:applicationContext-common-test.xml"])
class ConcertDAOTest {

    @Autowired
    lateinit var concertDAO: ConcertDAO

    private fun createConcert(): Concert {
        val concert = Concert()
        concert.title = "testTitle"
        concert.description = "testDescr"
        concert.externalConcertId = 100500
        concert.concertDate = Date()
        concert.status = ConcertStatus.PAUSED
        concert.hallId = 11
        return concert
    }

    private fun cloneConcert(concert: Concert): Concert {
        val clone = Concert()
        clone.concertId = concert.concertId
        clone.externalConcertId = concert.externalConcertId
        clone.title = concert.title
        clone.description = concert.description
        clone.timeAdded = concert.timeAdded
        clone.timeUpdated = concert.timeUpdated
        clone.concertDate = concert.concertDate
        clone.status = concert.status
        clone.hallId = concert.hallId
        return clone
    }


    private fun assertConcertEquals(actual: Concert, expected: Concert) {
        assertEquals(actual.concertId, expected.concertId)
        assertEquals(actual.externalConcertId, expected.externalConcertId)
        assertEquals(actual.concertDate, expected.concertDate)
        assertEquals(actual.title, expected.title)
        assertEquals(actual.description, expected.description)
        assertEquals(actual.hallId, expected.hallId)
        assertEquals(actual.status, expected.status)
        assertEquals(actual.statusCode.toLong(), expected.statusCode.toLong())
    }


    @Test
    fun testInsertAndSelectConcert() {
        var actual = createConcert()
        concertDAO.insert(actual)
        val expected = cloneConcert(actual)
        actual = concertDAO.select(actual.concertId)
        assertConcertEquals(actual, expected)

        actual = concertDAO!!.selectByExtId(actual.externalConcertId)
        assertConcertEquals(actual, expected)
    }

    @Test
    fun testInsertAndUpdateConcert() {
        var actual = createConcert()
        concertDAO.insert(actual)
        actual.externalConcertId = 1985
        actual.title = ""
        actual.description = ""
        actual.concertDate = Date()
        actual.hallId = 12
        actual.status = ConcertStatus.ACTIVE
        val expected = cloneConcert(actual)
        concertDAO.update(actual)
        actual = concertDAO.select(actual.concertId)
        assertConcertEquals(actual, expected)
    }
}

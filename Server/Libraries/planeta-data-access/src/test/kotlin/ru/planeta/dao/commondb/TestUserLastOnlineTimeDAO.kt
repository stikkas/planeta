package ru.planeta.dao.commondb

import org.junit.Assert.*
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.transaction.annotation.Transactional
import ru.planeta.TestHelper
import ru.planeta.TestHelper.TEST_PROFILE_ID
import ru.planeta.model.common.UserLastOnlineTime
import java.util.*

@RunWith(SpringRunner::class)
@Transactional
@ContextConfiguration(locations = ["classpath:applicationContext-common-test.xml"])
class TestUserLastOnlineTimeDAO {

    @Autowired
    lateinit var userLastOnlineTimeDAO: UserLastOnlineTimeDAO

    @Test
    fun testInsertSelectUpdateDelete() {
        val profileId = TEST_PROFILE_ID

        val userLastOnlineTime = UserLastOnlineTime(profileId, Date())

        val inserted = userLastOnlineTimeDAO!!.insert(userLastOnlineTime)
        assertEquals(1, inserted.toLong())

        var selectedUserLastOnlineTime = userLastOnlineTimeDAO.select(profileId)
        assertNotNull(selectedUserLastOnlineTime)
        assertEquals(userLastOnlineTime.profileId, selectedUserLastOnlineTime.profileId)
        assertEquals(userLastOnlineTime.lastOnlineTime, selectedUserLastOnlineTime.lastOnlineTime)

        selectedUserLastOnlineTime.lastOnlineTime = Date()
        val updated = userLastOnlineTimeDAO.update(selectedUserLastOnlineTime)
        assertEquals(1, updated.toLong())
        selectedUserLastOnlineTime = userLastOnlineTimeDAO.select(profileId)
        assertTrue(userLastOnlineTime.lastOnlineTime !== selectedUserLastOnlineTime.lastOnlineTime)

        val deleted = userLastOnlineTimeDAO.delete(userLastOnlineTime.profileId)
        assertEquals(1, deleted.toLong())
        selectedUserLastOnlineTime = userLastOnlineTimeDAO.select(profileId)
        assertNull(selectedUserLastOnlineTime)
    }

    @Test
    fun testSelectList() {
        val allUsers = ArrayList<Long>()
        val onlineUsers = ArrayList<Long>()
        for (i in 0..4) {
            val profile = TestHelper.createUserProfile()
            allUsers.add(profile.profileId)
            if (i % 2 == 0) {
                userLastOnlineTimeDAO!!.insert(UserLastOnlineTime(profile.profileId, Date()))
                onlineUsers.add(profile.profileId)
            }
            val selectedUsers = userLastOnlineTimeDAO!!.selectList(allUsers)
            assertTrue(compareList(selectedUsers, onlineUsers))
        }
    }

    private fun compareList(selectedUsers: List<UserLastOnlineTime>, users: List<Long>): Boolean {
        if (selectedUsers.size != users.size) {
            return false
        }
        for (userId in users) {
            var isFound = false
            for (userLastOnlineTime in selectedUsers) {
                if (userLastOnlineTime.profileId == userId) {
                    isFound = true
                    break
                }
            }
            if (!isFound) {
                return false
            }
        }
        return true
    }
}

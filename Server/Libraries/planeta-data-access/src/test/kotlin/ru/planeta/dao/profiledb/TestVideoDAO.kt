package ru.planeta.dao.profiledb


import org.apache.log4j.Logger
import org.junit.Assert.*
import org.junit.Ignore
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.transaction.annotation.Transactional
import ru.planeta.TestHelper
import ru.planeta.commons.external.oembed.provider.VideoType
import ru.planeta.model.enums.PermissionLevel
import ru.planeta.model.profile.media.Video
import ru.planeta.model.profile.media.enums.VideoConversionStatus
import ru.planeta.model.profile.media.enums.VideoQuality
import java.util.*

/**
 * @author ds.kolyshev
 * Date: 27.10.11
 */
@RunWith(SpringRunner::class)
@Transactional
@ContextConfiguration(locations = ["classpath:applicationContext-common-test.xml"])
class TestVideoDAO {

    @Autowired
    lateinit var videoDAO: VideoDAO

    @Test
    fun testAddUpdateRemove() {

        val video = createNewVideo()

        videoDAO.insert(video)

        val inserted = videoDAO.selectVideoById(video.profileId!!, video.videoId)
        checkVideos(video, inserted)

        video.videoUrl = "http://tratatata"
        video.imageId = 33
        video.imageUrl = "http://2"
        video.description = "test, tratata, blahblah"
        video.status = VideoConversionStatus.PROCESSED
        video.duration = 22
        video.viewPermission = PermissionLevel.EVERYBODY
        video.storyboardUrl = "http://storyboard_url"

        videoDAO.update(video)

        val updated = videoDAO.selectVideoById(video.profileId!!, video.videoId)
        assertNotNull(updated!!)

        checkVideos(video, updated)
        assertNotNull(updated.timeAdded)
        assertNotNull(updated.timeUpdated)

        //updating listenings and downloads count
        val viewsCount = 5
        videoDAO.updateViewsCount(video.profileId!!, video.videoId, viewsCount)
        val updatedCountsVideo = videoDAO.selectVideoById(video.profileId!!, video.videoId)
        assertNotNull(updatedCountsVideo)
        assertTrue(updatedCountsVideo?.viewsCount == video.viewsCount + viewsCount)

        videoDAO.delete(video.profileId!!, video.videoId)
        assertNull(videoDAO.selectVideoById(video.profileId!!, video.videoId))
    }

    private fun checkVideos(expected: Video, actual: Video?) {
        assertNotNull(actual!!)
        assertEquals(expected.profileId, actual.profileId)
        assertEquals(expected.videoId, actual.videoId)
        assertEquals(expected.authorProfileId, actual.authorProfileId)
        assertEquals(expected.description, actual.description)
        assertEquals(expected.downloadsCount.toLong(), actual.downloadsCount.toLong())
        assertEquals(expected.viewsCount.toLong(), actual.viewsCount.toLong())
        assertEquals(expected.videoUrl, actual.videoUrl)
        assertEquals(expected.name, actual.name)
        assertEquals(expected.status, actual.status)
        assertEquals(expected.imageUrl, actual.imageUrl)
        assertEquals(expected.imageId, actual.imageId)
        assertEquals(expected.cachedVideoId, actual.cachedVideoId)
        assertEquals(expected.storyboardUrl, actual.storyboardUrl)
        assertNotNull(expected.timeAdded)
        assertNotNull(expected.timeUpdated)
    }

    private fun createNewVideo(): Video {
        val video = Video()
        video.profileId = 99988
        video.authorProfileId = 99987
        video.viewsCount = 0
        video.description = "description"
        video.name = "test video name"
        video.availableQualities = EnumSet.of(VideoQuality.MODE_480P)
        video.videoUrl = "video url"
        video.imageId = 199999
        video.imageUrl = "http://test_video/url"
        video.imageId = 199997
        video.imageUrl = "http://test_video/url1"
        video.status = VideoConversionStatus.PROCESSING
        video.timeAdded = Date()
        video.timeUpdated = Date()
        video.duration = 112
        video.videoType = VideoType.PLANETA
        video.viewPermission = PermissionLevel.EVERYBODY
        video.cachedVideoId = 1111
        video.storyboardUrl = "http://test_video/storyboard_url"
        return video
    }

    @Test
    fun testAuthorInfo() {
        val profile = TestHelper.createUser(TestHelper.TEST_PROFILE_ID)
        val video = createNewVideo()
        video.profileId = profile.profileId
        video.authorProfileId = profile.profileId

        videoDAO.insert(video)

        val inserted = videoDAO.selectVideoById(video.profileId!!, video.videoId)
        assertNotNull(inserted)
        assertEquals(profile.displayName, inserted?.authorName)
        assertEquals(profile.displayName, inserted?.ownerName)
    }

    @Test
    @Ignore
    fun testRealLongVideo() {
        val video = videoDAO.selectVideoById(280513, 11904)
        assertNotNull(video)
        log.info(video?.humanDuration)
    }

    @Test
    @Throws(Exception::class)
    fun testCachedVideo() {
        val video = createNewVideo()
        videoDAO.insert(video)
        assertNotEquals(video.videoId, 0)
        val selectedVideo = videoDAO.selectVideoByCacheId(video.profileId!!, video.authorProfileId, video.cachedVideoId)
        assertNotNull(selectedVideo)
        assertEquals(video.videoId, selectedVideo?.videoId)
    }

    companion object {

        private val log = Logger.getLogger(TestVideoDAO::class.java)
    }
}

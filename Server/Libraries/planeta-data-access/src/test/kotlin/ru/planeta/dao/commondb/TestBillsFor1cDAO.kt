package ru.planeta.dao.commondb


import org.junit.Assert.*
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.transaction.annotation.Transactional
import ru.planeta.model.common.BillsFor1c
import ru.planeta.model.enums.TopayTransactionStatus
import java.math.BigDecimal
import java.util.*

/**
 *
 * User: Serge Blagodatskih<stikkas17></stikkas17>@gmail.com><br></br>
 * Date: 01.02.16<br></br>
 * Time: 15:00
 */

@RunWith(SpringRunner::class)
@Transactional
@ContextConfiguration(locations = ["classpath:applicationContext-common-test.xml"])
class TestBillsFor1cDAO {

    @Autowired
    lateinit var dao: BillsFor1cDAO

    private val bill = BillsFor1c(1L, Date())

    @Test
    fun testInsert() {
        dao.insert(bill)
    }

    @Test
    fun testUpdate() {
        dao.insert(bill)
        var foundBill = dao.select(1L)
        assertNull(foundBill!!.amount)
        assertNull(foundBill.status)
        val date = foundBill.timeAdded
        assertNotNull(date)
        foundBill.amount = BigDecimal.valueOf(1000)
        foundBill.status = TopayTransactionStatus.NEW
        dao.update(foundBill)
        foundBill = dao.select(1L)
        assertEquals(BigDecimal.valueOf(1000).toInt().toLong(), foundBill!!.amount!!.toInt().toLong())
        assertEquals(TopayTransactionStatus.NEW, foundBill.status)
        assertEquals(date, foundBill.timeAdded)
    }

    @Test
    fun testDelete() {
        dao.delete(1L)
        dao.insert(bill)
        var found = dao.select(1L)
        assertNotNull(found)
        dao.delete(1L)
        found = dao.select(1L)
        assertNull(found)
    }

    @Test
    fun testSelect() {
        dao.insert(bill)
        var found = dao.select(1L)
        assertNotNull(found)
        found = dao.select(10L)
        assertNull(found)
    }

    @Test
    fun testSelectList() {
        var bills = dao!!.selectList(0, 2)
        assertNotNull(bills)
        dao.insert(bill)
        bills = dao.selectList(0, 2)
        assertNotNull(bills)
        assertFalse(bills.isEmpty())
    }

    @Test
    fun testSelectListByStatus() {
        val newBill = BillsFor1c(2L, Date())
        newBill.status = TopayTransactionStatus.NEW

        dao.insert(bill)
        dao.insert(newBill)

        val bills = dao.selectListByStatus(TopayTransactionStatus.NEW, 0, 3)
        assertNotNull(bills)
        assertTrue(bills.size >= 1)
        assertTrue(bills.size <= 3)
        assertEquals(java.lang.Long.valueOf(2), bills[0].topayTransactionId)
    }

}

package ru.planeta.dao.trashcan


import org.junit.Assert.*
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.transaction.annotation.Transactional
import ru.planeta.TestHelper
import ru.planeta.model.trashcan.Quiz
import ru.planeta.model.trashcan.QuizAnswer
import ru.planeta.model.trashcan.QuizQuestion
import ru.planeta.model.trashcan.QuizQuestionType
import java.util.*

@RunWith(SpringRunner::class)
@Transactional
@ContextConfiguration(locations = ["classpath:applicationContext-common-test.xml"])
class QuizDAOImplTest {
    @Autowired
    lateinit var quizDAO: QuizDAO

    @Test
    fun insertSelectUpdateQuizTest() {
        val currentMillsString = Date().time.toString()

        // ----- adding quiz -------
        val quiz = Quiz()
        quiz.name = "quizName_$currentMillsString"
        quiz.alias = "quizAlias_$currentMillsString"
        quiz.description = "quizDescription_$currentMillsString"
        quiz.timeAdded = Date()
        // by default quiz is not visible for users: enabled is false

        quizDAO.insert(quiz)
        // -------------------------

        // ----- checking quiz -----
        var selectedQuiz = quizDAO.selectQuiz(quiz.quizId.toString(), null)
        assertNotNull(selectedQuiz)
        assertEquals(quiz.name, selectedQuiz.name)
        assertEquals(quiz.alias, selectedQuiz.alias)
        assertEquals(quiz.description, selectedQuiz.description)

        selectedQuiz = quizDAO.selectQuiz(quiz.quizId.toString(), true)
        assertNull(selectedQuiz)

        selectedQuiz = quizDAO.selectQuiz(quiz.quizId.toString(), false)
        assertNotNull(selectedQuiz)
        // -------------------------

        // ------ update quiz ------
        selectedQuiz.name = selectedQuiz.name!! + selectedQuiz.name!!
        selectedQuiz.alias = selectedQuiz.alias!! + selectedQuiz.alias!!
        selectedQuiz.description = selectedQuiz.description!! + selectedQuiz.description!!
        quizDAO.update(selectedQuiz)
        // -------------------------

        // ----- checking quiz -----
        selectedQuiz = quizDAO.selectQuiz(quiz.quizId.toString(), false)
        assertNotNull(selectedQuiz)
        assertEquals(quiz.name!! + quiz.name!!, selectedQuiz.name)
        assertEquals(quiz.alias!! + quiz.alias!!, selectedQuiz.alias)
        assertEquals(quiz.description!! + quiz.description!!, selectedQuiz.description)
        // -------------------------

        // ------ update quiz / making quiz is enabling for users ------
        selectedQuiz.isEnabled = true
        quizDAO.update(selectedQuiz)
        // -------------------------------------------------------------

        // ----- checking quiz -----
        selectedQuiz = quizDAO.selectQuiz(quiz.quizId.toString(), false)
        assertNull(selectedQuiz)

        selectedQuiz = quizDAO.selectQuiz(quiz.quizId.toString(), true)
        assertNotNull(selectedQuiz)
        assertEquals(true, selectedQuiz.isEnabled)
        // -------------------------
    }

    @Test
    @Throws(Exception::class)
    fun insertSelectUpdateQuizQuestionTest() {
        val currentMillsString = Date().time.toString()

        // ----- adding quizQuestion -------
        val quizQuestion = QuizQuestion()
        quizQuestion.questionText = "quizQuestionText$currentMillsString"
        quizQuestion.questionDescription = "quizQuestionDescription$currentMillsString"
        quizQuestion.type = QuizQuestionType.TEXT
        quizQuestion.startValue = 0
        quizQuestion.endValue = 0
        quizQuestion.isHasCustomRadio = true
        quizQuestion.customRadioText = "quizQuestionCustomRadioText$currentMillsString"
        quizQuestion.isEnabled = true
        //        private List<QuizQuestionOption> quizQuestionOptionList;

        quizDAO!!.insert(quizQuestion)
        // ---------------------------------

        // --- checking quizQuestion ----
        val selectedQuizQuestion = quizDAO.selectQuizQuestionById(quizQuestion.quizQuestionId)
        assertNotNull(selectedQuizQuestion)
        assertEquals(quizQuestion.questionText, selectedQuizQuestion.questionText)
        assertEquals(quizQuestion.questionDescription, selectedQuizQuestion.questionDescription)
        assertTrue(quizQuestion.type === selectedQuizQuestion.type)
        assertEquals(quizQuestion.startValue.toLong(), selectedQuizQuestion.startValue.toLong())
        assertEquals(quizQuestion.endValue.toLong(), selectedQuizQuestion.endValue.toLong())
        assertEquals(quizQuestion.isHasCustomRadio, selectedQuizQuestion.isHasCustomRadio)
        assertEquals(quizQuestion.customRadioText, selectedQuizQuestion.customRadioText)
        assertEquals(quizQuestion.isEnabled, selectedQuizQuestion.isEnabled)
        // ------------------------------

        // ---- update quizQuestion -----
        selectedQuizQuestion.questionText = quizQuestion.questionText!! + quizQuestion.questionText!!
        selectedQuizQuestion.questionDescription = quizQuestion.questionDescription!! + quizQuestion.questionDescription!!
        selectedQuizQuestion.type = QuizQuestionType.NUMBER
        selectedQuizQuestion.startValue = quizQuestion.startValue + 1
        selectedQuizQuestion.endValue = quizQuestion.endValue + 1
        selectedQuizQuestion.isHasCustomRadio = !quizQuestion.isHasCustomRadio
        selectedQuizQuestion.customRadioText = quizQuestion.customRadioText!! + quizQuestion.customRadioText!!
        selectedQuizQuestion.isEnabled = !quizQuestion.isEnabled

        quizDAO.updateQuizQuestion(selectedQuizQuestion)
        // ------------------------------

        // --- checking quizQuestion ----
        val selectedQuizQuestion2 = quizDAO.selectQuizQuestionById(selectedQuizQuestion.quizQuestionId)
        assertNotNull(selectedQuizQuestion2)
        assertEquals(selectedQuizQuestion.questionText, selectedQuizQuestion2.questionText)
        assertEquals(selectedQuizQuestion.questionDescription, selectedQuizQuestion2.questionDescription)
        assertTrue(selectedQuizQuestion.type === selectedQuizQuestion2.type)
        assertEquals(selectedQuizQuestion.startValue.toLong(), selectedQuizQuestion2.startValue.toLong())
        assertEquals(selectedQuizQuestion.endValue.toLong(), selectedQuizQuestion2.endValue.toLong())
        assertEquals(selectedQuizQuestion.isHasCustomRadio, selectedQuizQuestion2.isHasCustomRadio)
        assertEquals(selectedQuizQuestion.customRadioText, selectedQuizQuestion2.customRadioText)
        assertEquals(selectedQuizQuestion.isEnabled, selectedQuizQuestion2.isEnabled)
        // ------------------------------
    }

    @Test
    fun addAndDisableQuestionToQuizTest() {
        val currentMillsString = Date().time.toString()

        // ----- adding quiz -------
        val quiz = Quiz()
        quiz.name = "quizName_$currentMillsString"
        quiz.alias = "quizAlias_$currentMillsString"
        quiz.description = "quizDescription_$currentMillsString"
        quiz.timeAdded = Date()

        quizDAO!!.insert(quiz)
        // --------------------------

        // --- checking quizQuestions count in quiz is 0 -----
        var quizQuestionList = quizDAO.selectQuizQuestionsByQuizId(quiz.quizId, null)
        assertEquals(0, quizQuestionList.size.toLong())
        // ---------------------------------------------------

        // ----- adding quizQuestion to quiz -------
        val quizQuestion = QuizQuestion()
        quizQuestion.questionText = "quizQuestionText$currentMillsString"
        quizQuestion.questionDescription = "quizQuestionDescription$currentMillsString"
        quizQuestion.type = QuizQuestionType.TEXT
        quizQuestion.startValue = 0
        quizQuestion.endValue = 0
        // by default quizQuestion is enabled

        quizDAO.insert(quizQuestion)
        quizDAO.insertQuizQuestionRelation(quiz.quizId, quizQuestion.quizQuestionId,
                quizDAO.selectNextQuizQuestionOrderNumInQuiz(quiz.quizId))
        // -----------------------------------------

        // --- checking quizQuestions count in quiz is 1 -----
        quizQuestionList = quizDAO.selectQuizQuestionsByQuizId(quiz.quizId, null)
        assertEquals(1, quizQuestionList.size.toLong())
        // ---------------------------------------------------

        // ----- adding disabled quizQuestion to quiz -------
        val quizQuestion2 = QuizQuestion()
        quizQuestion2.questionText = "quizQuestionText$currentMillsString"
        quizQuestion2.questionDescription = "quizQuestionDescription$currentMillsString"
        quizQuestion2.type = QuizQuestionType.TEXT
        quizQuestion2.startValue = 0
        quizQuestion2.endValue = 0
        quizQuestion2.isEnabled = false

        quizDAO.insert(quizQuestion2)
        quizDAO.insertQuizQuestionRelation(quiz.quizId, quizQuestion2.quizQuestionId,
                quizDAO.selectNextQuizQuestionOrderNumInQuiz(quiz.quizId))
        // -------------------------------------------------

        // --- checking quizQuestions count -----
        quizQuestionList = quizDAO.selectQuizQuestionsByQuizId(quiz.quizId, null)
        assertEquals(2, quizQuestionList.size.toLong())

        quizQuestionList = quizDAO.selectQuizQuestionsByQuizId(quiz.quizId, true)
        assertEquals(1, quizQuestionList.size.toLong())

        quizQuestionList = quizDAO.selectQuizQuestionsByQuizId(quiz.quizId, false)
        assertEquals(1, quizQuestionList.size.toLong())
        // --------------------------------------

        // --- disable second quizQuestion in quiz -------
        val selectedQuizQuestion = quizDAO.selectQuizQuestionById(quizQuestion.quizQuestionId)
        selectedQuizQuestion.isEnabled = false

        quizDAO.updateQuizQuestion(selectedQuizQuestion)
        // -----------------------------------------------

        // --- checking quizQuestions count -----
        quizQuestionList = quizDAO.selectQuizQuestionsByQuizId(quiz.quizId, null)
        assertEquals(2, quizQuestionList.size.toLong())

        quizQuestionList = quizDAO.selectQuizQuestionsByQuizId(quiz.quizId, true)
        assertEquals(0, quizQuestionList.size.toLong())

        quizQuestionList = quizDAO.selectQuizQuestionsByQuizId(quiz.quizId, false)
        assertEquals(2, quizQuestionList.size.toLong())
        // --------------------------------------
    }

    @Test
    @Throws(Exception::class)
    fun insertQuizAnswerTest() {
        val userId = TestHelper.TEST_PROFILE_ID
        val currentMillsString = Date().time.toString()

        // ----- adding quiz -------
        val quiz = Quiz()
        quiz.name = "quizName_$currentMillsString"
        quiz.alias = "quizAlias_$currentMillsString"
        quiz.description = "quizDescription_$currentMillsString"
        quiz.timeAdded = Date()

        quizDAO!!.insert(quiz)
        // --------------------------

        // ----- adding quizQuestion to quiz -------
        val quizQuestion = QuizQuestion()
        quizQuestion.questionText = "quizQuestionText$currentMillsString"
        quizQuestion.questionDescription = "quizQuestionDescription$currentMillsString"
        quizQuestion.type = QuizQuestionType.TEXT
        quizQuestion.startValue = 0
        quizQuestion.endValue = 0
        // by default quizQuestion is enabled

        quizDAO.insert(quizQuestion)
        quizDAO.insertQuizQuestionRelation(quiz.quizId, quizQuestion.quizQuestionId,
                quizDAO.selectNextQuizQuestionOrderNumInQuiz(quiz.quizId))
        // -----------------------------------------

        val quizQuestionId01 = quizQuestion.quizQuestionId
        val quizAnswer = QuizAnswer()
        quizAnswer.quizId = quiz.quizId
        quizAnswer.quizQuestionId = quizQuestionId01
        quizAnswer.quizQuestionOptionId = 3
        quizAnswer.userId = userId
        quizAnswer.answerCustomText = "customText"
        quizAnswer.answerInteger = 123
        quizAnswer.isAnswerBoolean = true
        quizAnswer.timeAdded = Date()

        quizDAO.insertQuizAnswer(quizAnswer)
        val selectedQuizAnswer = quizDAO.selectQuizAnswerByParams(quiz.quizId, quizQuestionId01, userId)
        assertNotNull(selectedQuizAnswer)
        assertEquals(quizAnswer.quizQuestionOptionId, selectedQuizAnswer.quizQuestionOptionId)
        assertEquals(quizAnswer.userId, selectedQuizAnswer.userId)
        assertEquals(quizAnswer.answerCustomText, selectedQuizAnswer.answerCustomText)
        assertEquals(quizAnswer.answerInteger, selectedQuizAnswer.answerInteger)
        assertEquals(quizAnswer.isAnswerBoolean, selectedQuizAnswer.isAnswerBoolean)
    }
}

package ru.planeta.dao.profiledb

import org.junit.Ignore
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.transaction.annotation.Transactional
import ru.planeta.dao.profiledb.ProfileNewsDAO

@RunWith(SpringRunner::class)
@Transactional
@ContextConfiguration(locations = ["classpath:applicationContext-common-test.xml"])
class TestProfileNewsDAO {
    @Autowired
    lateinit var profileNewsDAO: ProfileNewsDAO

    @Ignore
    @Test
    fun testProfileNewsDAO() {
        // List<ProfileNews> profileNews = profileNewsDAO.selectForProfile(191, true, 0, 1000);
        val profileNews = profileNewsDAO.selectForProfile(191, true, 0, 1000)
        println(profileNews.size)
    }
}

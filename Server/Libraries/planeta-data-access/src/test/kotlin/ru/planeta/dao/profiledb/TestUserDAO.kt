package ru.planeta.dao.profiledb

import org.assertj.core.api.Assertions.assertThat
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.transaction.annotation.Transactional
import ru.planeta.TestHelper.TEST_PROFILE_ID
import ru.planeta.model.enums.MartialStatus
import ru.planeta.model.profile.User

@RunWith(SpringRunner::class)
@Transactional
@ContextConfiguration(locations = ["classpath:applicationContext-common-test.xml"])
class TestUserDAO {

    @Autowired
    lateinit var userDAO: UserDAO

    private fun assertUserEquals(expected: User, actual: User) {
        assertThat(expected.profileId).isEqualTo(actual.profileId)
        assertThat(expected.martialStatus).isEqualTo(actual.martialStatus)
    }

    @Test
    fun test() {
        val user = User()
        user.profileId = TEST_PROFILE_ID
        user.martialStatus = MartialStatus.BUSY

        userDAO.insert(user)

        var selected = userDAO.select(TEST_PROFILE_ID)
        assertThat(selected).isNotNull
        assertUserEquals(selected, user)

        user.martialStatus = MartialStatus.FREE

        userDAO.addFriendsCount(TEST_PROFILE_ID, 1)
        userDAO.addUsersRequestInCount(TEST_PROFILE_ID, 3)
        userDAO.addUsersRequestOutCount(TEST_PROFILE_ID, 4)

        selected = userDAO.select(TEST_PROFILE_ID)
        assertThat(selected.friendsCount).isEqualTo(1)
        assertThat(selected.usersRequestInCount).isEqualTo(3)
        assertThat(selected.usersRequestOutCount).isEqualTo(4)

        userDAO.delete(TEST_PROFILE_ID)
        assertThat(userDAO.select(TEST_PROFILE_ID)).isNull()

    }
}


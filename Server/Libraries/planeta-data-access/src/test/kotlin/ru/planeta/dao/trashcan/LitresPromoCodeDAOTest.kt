package ru.planeta.dao.trashcan


import org.junit.Assert.assertNotNull
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.transaction.annotation.Transactional

/**
 * Created by asavan on 25.01.2017.
 */
@RunWith(SpringRunner::class)
@Transactional
@ContextConfiguration(locations = ["classpath:applicationContext-common-test.xml"])
class LitresPromoCodeDAOTest {
    @Autowired
    lateinit var litresPromoCodeDAO: LitresPromoCodeDAO

    @Test
    @Throws(Exception::class)
    fun selectLastNonUsed() {
        val code = litresPromoCodeDAO.selectLastNonUsed()
        assertNotNull(code)
    }

}

package ru.planeta.dao.profiledb


import org.assertj.core.api.Assertions.assertThat
import org.junit.Ignore
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.transaction.annotation.Transactional
import ru.planeta.commons.model.Gender
import ru.planeta.model.enums.GroupCategory
import ru.planeta.model.enums.ProfileStatus
import ru.planeta.model.enums.ProfileType
import ru.planeta.model.profile.Profile
import java.io.UnsupportedEncodingException
import java.net.URLDecoder
import java.util.*

/**
 * Class TestProfileDAO
 *
 * @author a.tropnikov
 */
@Ignore
@RunWith(SpringRunner::class)
@Transactional
@ContextConfiguration(locations = ["classpath:applicationContext-common-test.xml"])
class TestProfileDAO {

    @Autowired
    lateinit var profileDAO: ProfileDAO

    @Test
    @Throws(UnsupportedEncodingException::class)
    fun test() {
        val profile = Profile()
        profile.profileType = ProfileType.USER
        profile.imageUrl = "/imageurl"
        profile.imageId = 1
        profile.smallImageUrl = "/imageurl1"
        profile.smallImageId = 11
        profile.displayName = "display name"
        profile.alias = "alias"
        profile.status = ProfileStatus.NOT_SET
        profile.cityId = 1
        profile.countryId = 1
        profile.userBirthDate = Date()
        profile.userGender = Gender.NOT_SET

        profile.groupCategory = GroupCategory.MUSIC

        profile.isLimitMessages = true
        profile.isReceiveNewsletters = false

        profileDAO.insert(profile)
        assertThat(profile.profileId).isGreaterThan(0)
        val selected = profileDAO.selectById(profile.profileId)
        val russiaName = URLDecoder.decode("%D0%A0%D0%BE%D1%81%D1%81%D0%B8%D1%8F", "utf-8") // Россия
        val moscowName = URLDecoder.decode("%D0%9C%D0%BE%D1%81%D0%BA%D0%B2%D0%B0", "utf-8") // Москва
        assertThat(selected).isNotNull
        selected?.let {
            assertThat(it.cityName).isEqualTo(moscowName)
            assertThat(it.countryName).isEqualTo(russiaName)
            assertProfileEquals(it, profile)
        }

        profile.profileType = ProfileType.EVENT
        profile.imageUrl = "/newimageurl"
        profile.imageId = 2
        profile.smallImageUrl = "/newimageurl2"
        profile.smallImageId = 22
        profile.displayName = "display name"
        profile.alias = "new alias"
        profile.status = ProfileStatus.EVENT_CREATED
        profile.cityId = 2
        profile.countryId = 3
        profile.userBirthDate = Date()
        profile.userGender = Gender.FEMALE
        profile.groupCategory = GroupCategory.THEATER
        profile.isLimitMessages = false
        profile.isReceiveNewsletters = true

        profileDAO.update(profile)

        val updated = profileDAO.selectById(profile.profileId)
        assertThat(updated).isNotNull
        updated?.let {
            assertProfileEquals(updated, profile)
        }
        profileDAO.delete(profile.profileId)
        assertThat(profileDAO.selectById(profile.profileId)).isNull()
    }

    @Test
    fun testNewSubscribersCount() {
        val profile = Profile()
        profile.profileType = ProfileType.USER
        profile.imageUrl = "/imageurl"
        profile.imageId = 1
        profile.smallImageUrl = "/imageurl1"
        profile.smallImageId = 11
        profile.displayName = "display name"
        profile.alias = "alias"
        profile.status = ProfileStatus.NOT_SET
        profile.cityId = 1
        profile.countryId = 1
        profile.userBirthDate = Date()
        profile.userGender = Gender.NOT_SET


        profile.groupCategory = GroupCategory.MUSIC
        profile.isLimitMessages = true
        profile.isReceiveNewsletters = false

        profileDAO.insert(profile)
        profileDAO.updateNewSubscribersCount(profile.profileId, 100)

        val selected = profileDAO.selectById(profile.profileId)
        assertThat(selected).isNotNull
        selected?.let {
            assertThat(it.newSubscribersCount).isEqualTo(100)
        }
    }

    private fun assertProfileEquals(expected: Profile, actual: Profile) {
        assertThat(expected.profileType).isEqualTo(actual.profileType)
        assertThat(expected.imageUrl).isEqualTo(actual.imageUrl)
        assertThat(expected.imageId).isEqualTo(actual.imageId)
        assertThat(expected.smallImageUrl).isEqualTo(actual.smallImageUrl)
        assertThat(expected.smallImageId).isEqualTo(actual.smallImageId)
        assertThat(expected.displayName).isEqualTo(actual.displayName)
        assertThat(expected.alias).isEqualTo(actual.alias)
        assertThat(expected.status.code.toLong()).isEqualTo(actual.status.code.toLong())
        assertThat(expected.cityId.toLong()).isEqualTo(actual.cityId.toLong())
        assertThat(expected.countryId.toLong()).isEqualTo(actual.countryId.toLong())
        assertThat(expected.userBirthDate).isEqualTo(actual.userBirthDate)
        assertThat(expected.userGender).isEqualTo(actual.userGender)
        assertThat(expected.groupCategory).isEqualTo(actual.groupCategory)
        assertThat(expected.displayName).isEqualTo(actual.displayName)
        assertThat(expected.isLimitMessages).isEqualTo(actual.isLimitMessages)
        assertThat(expected.isReceiveNewsletters).isEqualTo(actual.isReceiveNewsletters)
    }
}

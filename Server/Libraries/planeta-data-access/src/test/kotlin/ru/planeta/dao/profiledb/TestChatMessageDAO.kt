package ru.planeta.dao.profiledb


import org.junit.Assert.*
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.transaction.annotation.Transactional
import ru.planeta.TestHelper
import ru.planeta.model.profile.chat.ChatMessage
import java.util.*

/**
 * @author m.shulepov
 */
@RunWith(SpringRunner::class)
@Transactional
@ContextConfiguration(locations = ["classpath:applicationContext-common-test.xml"])
class TestChatMessageDAO {

    @Autowired
    lateinit var chatMessageDAO: ChatMessageDAO

    @Test
    fun testChatMessageAddRemove() {


        val chatId: Long = 1
        val profile = TestHelper.createUser(TestHelper.TEST_PROFILE_ID)

        val chatMessage = ChatMessage()
        chatMessage.chatId = chatId
        chatMessage.messageText = "Test message"
        chatMessage.messageTextHtml = "Test message"
        chatMessage.timeAdded = Date()
        chatMessage.authorProfileId = profile.profileId
        chatMessage.profileId = profile.profileId
        chatMessage.isDeleted = false
        chatMessageDAO!!.insert(chatMessage)
        assertTrue(chatMessage.messageId > 0)

        val selectedMessage = chatMessageDAO.selectMessage(profile.profileId, chatId, chatMessage.messageId)
        assertNotNull(selectedMessage)
        assertEquals(chatMessage.messageText, selectedMessage.messageText)

        var messages = chatMessageDAO.selectMessages(profile.profileId, chatId, 0, 100)
        assertNotNull(messages)
        assertEquals(1, messages.size.toLong())

        val chatMessage2 = ChatMessage()
        chatMessage2.chatId = chatId
        chatMessage2.messageText = "123123"
        chatMessage2.messageTextHtml = "123123"
        chatMessage2.timeAdded = Date()
        chatMessage2.authorProfileId = profile.profileId
        chatMessage2.profileId = profile.profileId
        chatMessageDAO.insert(chatMessage2)

        messages = chatMessageDAO.selectMessages(profile.profileId, chatId, 0, 10)
        assertNotNull(messages)
        assertEquals(2, messages.size.toLong())

        messages = chatMessageDAO.selectMessages(profile.profileId, chatId, chatMessage.messageId)
        assertNotNull(messages)
        assertEquals(1, messages.size.toLong())

        chatMessage.isDeleted = true
        chatMessageDAO.update(chatMessage)
        messages = chatMessageDAO.selectMessages(profile.profileId, chatId, 0, 10)
        assertEquals(1, messages.size.toLong())


    }
}

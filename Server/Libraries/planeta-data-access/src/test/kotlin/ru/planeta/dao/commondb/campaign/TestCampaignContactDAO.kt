package ru.planeta.dao.commondb.campaign

import org.junit.Test
import org.springframework.beans.factory.annotation.Autowired
import ru.planeta.model.common.campaign.CampaignContact


import org.junit.Assert.assertEquals
import org.junit.Assert.assertNotNull
import org.junit.runner.RunWith
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.transaction.annotation.Transactional
import ru.planeta.TestHelper
import ru.planeta.TestHelper.TEST_PROFILE_ID
import ru.planeta.dao.commondb.CampaignContactDAO

/**
 * @author: ds.kolyshev
 * Date: 28.07.13
 */
@RunWith(SpringRunner::class)
@Transactional
@ContextConfiguration(locations = ["classpath:applicationContext-common-test.xml"])
class TestCampaignContactDAO {

    @Autowired
    lateinit var campaignContactDAO: CampaignContactDAO

    @Test
    fun testCRUD() {
        val testProfileId = TEST_PROFILE_ID
        val campaignId = TEST_PROFILE_ID
        val contactProfile = TestHelper.createUser(TEST_PROFILE_ID)
        val contactEmail = "testmail123@test.ee"
        val contactEmail2 = "testmail1234@test.ee"

        var campaignContact = CampaignContact()
        campaignContact.campaignId = campaignId
        campaignContact.contactProfileId = contactProfile.profileId
        campaignContact.contactName = contactProfile.displayName
        campaignContact.email = contactEmail
        campaignContactDAO!!.insert(campaignContact)

        var list = campaignContactDAO.selectList(campaignId, 0, 0)
        assertNotNull(list)
        assertEquals(1, list.size.toLong())
        assertEquals(campaignId, list[0].campaignId)
        assertEquals(contactEmail, list[0].email)
        assertEquals(contactProfile.profileId, list[0].contactProfileId)
        assertEquals(contactProfile.displayName, list[0].contactName)

        campaignContact = CampaignContact()
        campaignContact.campaignId = campaignId
        campaignContact.email = contactEmail2
        campaignContactDAO.insert(campaignContact)

        list = campaignContactDAO.selectList(campaignId, 0, 0)
        assertNotNull(list)
        assertEquals(2, list.size.toLong())

        assertEquals(campaignId, list[0].campaignId)
        assertEquals(contactEmail2, list[0].email)

        assertEquals(campaignId, list[1].campaignId)
        assertEquals(contactEmail, list[1].email)
        assertEquals(contactProfile.profileId, list[1].contactProfileId)
        assertEquals(contactProfile.displayName, list[1].contactName)


        campaignContactDAO.delete(campaignId, contactEmail)

        list = campaignContactDAO.selectList(campaignId, 0, 0)
        assertNotNull(list)
        assertEquals(1, list.size.toLong())

        assertEquals(campaignId, list[0].campaignId)
        assertEquals(contactEmail2, list[0].email)
    }

}

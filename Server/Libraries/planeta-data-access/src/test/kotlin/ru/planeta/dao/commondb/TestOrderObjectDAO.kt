package ru.planeta.dao.commondb

import org.apache.commons.collections4.CollectionUtils
import org.junit.Test
import org.springframework.beans.factory.annotation.Autowired
import ru.planeta.model.common.CreditTransaction
import ru.planeta.model.common.DebitTransaction
import ru.planeta.model.common.Order
import ru.planeta.model.common.OrderObject
import ru.planeta.model.common.campaign.Share
import ru.planeta.model.enums.OrderObjectType
import ru.planeta.model.profile.Profile


import java.math.BigDecimal
import java.util.HashSet

import org.junit.Assert.*
import org.junit.runner.RunWith
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.transaction.annotation.Transactional

/**
 * User: atropnikov
 * Date: 06.04.12
 * Time: 17:00
 */
@RunWith(SpringRunner::class)
@Transactional
@ContextConfiguration(locations = ["classpath:applicationContext-common-test.xml"])
class TestOrderObjectDAO {

    @Autowired
    private val orderObjectDAO: OrderObjectDAO? = null

    @Test
    fun testCRUD() {
//        val merchantId = getId(Profile::class.java)
//        var shareId = getId(Share::class.java)
//        val version = 0
//
//        val original = createOrderObject(shareId, merchantId)
//        val orderId = original.orderId
//
//        var orderObjects = orderObjectDAO!!.selectOrderObjects(orderId)
//        assertNotNull(orderObjects)
//        assertTrue(orderObjects.isEmpty())
//
//        orderObjectDAO.insert(cloneOrderObject(original))
//        var selected = selectCampaignById(shareId, orderId, OrderObjectType.SHARE)
//        assertTrue(selected.orderObjectId != 0L)
//        original.orderObjectId = selected.orderObjectId
//        assertOrderObjectEquals(original, selected)
//
//        original.price = BigDecimal.ONE
//        original.creditTransactionId = getId(CreditTransaction::class.java)
//        original.debitTransactionId = getId(DebitTransaction::class.java)
//        original.cancelDebitTransactionId = getId(DebitTransaction::class.java)
//        original.cancelCreditTransactionId = getId(CreditTransaction::class.java)
//
//        orderObjectDAO.update(cloneOrderObject(original))
//        selected = selectCampaignById(shareId, orderId, OrderObjectType.SHARE)
//        assertOrderObjectEquals(original, selected)
//
//        original.objectId = ++shareId
//        orderObjectDAO.insert(cloneOrderObject(original))
//        original.objectId = ++shareId
//        orderObjectDAO.insert(cloneOrderObject(original))
//
//        orderObjects = orderObjectDAO.selectOrderObjects(orderId)
//        assertNotNull(orderObjects)
//        assertEquals(3, orderObjects.size.toLong())
//
//        orderObjectDAO.updateOrderObjectComment(orderId, shareId, "comment")
//        orderObjects = orderObjectDAO.selectCampaignById(orderId, shareId, OrderObjectType.SHARE)
//        assertNotNull(orderObjects)
//        assertEquals(1, orderObjects.size.toLong())
    }

//    private fun selectCampaignById(shareId: Long, orderId: Long, orderObjectType: OrderObjectType): OrderObject {
//        val orderObjects = orderObjectDAO!!.selectCampaignById(orderId, shareId, orderObjectType)
//        assertTrue(CollectionUtils.isNotEmpty(orderObjects))
//        return orderObjects.iterator().next()
//    }

    @Test
    fun testSelects() {
//        val objectId = getId(Share::class.java)
//        val merchantId = getId(Profile::class.java)
//
//        val orderIds = HashSet<Long>()
//        var `object` = createOrderObject(objectId, merchantId)
//        orderIds.add(`object`.orderId)
//        orderObjectDAO!!.insert(`object`)
//
//        `object` = createOrderObject(objectId, merchantId)
//        orderIds.add(`object`.orderId)
//        orderObjectDAO.insert(`object`)
//
//        `object` = cloneOrderObject(`object`)
//        `object`.objectId = `object`.objectId + 1
//        `object`.merchantId = `object`.merchantId + 1
//        orderIds.add(`object`.orderId)
//        orderObjectDAO.insert(`object`)
//
//        assertNotNull(selectCampaignById(objectId, orderIds.iterator().next(), OrderObjectType.SHARE))
//        assertEquals(2, orderObjectDAO.selectOrderObjects(`object`.orderId).size.toLong())
//        assertEquals(3, orderObjectDAO.selectOrdersObjects(orderIds, 0L).size.toLong())
//        assertEquals(2, orderObjectDAO.selectOrdersObjects(orderIds, merchantId).size.toLong())
    }

    fun assertOrderObjectEquals(expected: OrderObject, actual: OrderObject) {
        assertNotNull(expected)
        assertEquals(expected.orderId, actual.orderId)
        assertEquals(expected.orderObjectId, actual.orderObjectId)
        assertEquals(expected.objectId, actual.objectId)
        assertEquals(expected.merchantId, actual.merchantId)
        assertEquals(expected.creditTransactionId, actual.creditTransactionId)
        assertEquals(expected.debitTransactionId, actual.debitTransactionId)
        assertEquals(expected.cancelDebitTransactionId, actual.cancelDebitTransactionId)
        assertEquals(expected.cancelCreditTransactionId, actual.cancelCreditTransactionId)
        assertEquals(expected.orderObjectType, actual.orderObjectType)
        assertEquals(expected.price.compareTo(actual.price).toLong(), 0)
    }

    private fun createOrderObject(objectId: Long, merchantId: Long): OrderObject {
        val result = OrderObject()
//        result.orderId = getId(Order::class.java)
//        result.objectId = objectId
//        result.merchantId = merchantId
//        result.creditTransactionId = getId(CreditTransaction::class.java)
//        result.debitTransactionId = getId(DebitTransaction::class.java)
//        result.cancelCreditTransactionId = getId(CreditTransaction::class.java)
//        result.cancelDebitTransactionId = getId(DebitTransaction::class.java)
//        result.orderObjectType = OrderObjectType.SHARE
//        result.price = BigDecimal.TEN

        return result
    }

    private fun cloneOrderObject(original: OrderObject): OrderObject {
        val result = OrderObject()
        result.orderObjectId = original.orderObjectId
        result.orderId = original.orderId
        result.objectId = original.objectId
        result.merchantId = original.merchantId
        result.creditTransactionId = original.creditTransactionId
        result.debitTransactionId = original.debitTransactionId
        result.cancelDebitTransactionId = original.cancelDebitTransactionId
        result.cancelCreditTransactionId = original.cancelCreditTransactionId
        result.orderObjectType = original.orderObjectType
        result.price = original.price

        return result
    }
}

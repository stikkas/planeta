package ru.planeta.dao.statdb

import org.junit.Assert.assertEquals
import org.junit.Assert.assertNotNull
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.transaction.annotation.Transactional
import ru.planeta.commons.model.OrderShortLinkStat

@RunWith(SpringRunner::class)
@Transactional
@ContextConfiguration(locations = ["classpath:applicationContext-common-test.xml"])
class OrderShortLinkStatDAOImplTest {
    @Autowired
    lateinit var orderShortLinkStatDAO: OrderShortLinkStatDAO

    @Test
    fun insertSelect() {
        val orderShortLinkStat = OrderShortLinkStat()
        orderShortLinkStat.orderId = 123
        orderShortLinkStat.shortLinkId = 321
        orderShortLinkStat.referrer = "referrer ololoferrer"

        orderShortLinkStatDAO!!.insert(orderShortLinkStat)
        val selectedOrderShortLinkStat = orderShortLinkStatDAO!!.selectById(orderShortLinkStat.shortLinkStatsId)
        assertNotNull(selectedOrderShortLinkStat)
        assertEquals(orderShortLinkStat.orderId, selectedOrderShortLinkStat.orderId)
        assertEquals(orderShortLinkStat.shortLinkId, selectedOrderShortLinkStat.shortLinkId)
        assertEquals(orderShortLinkStat.referrer, selectedOrderShortLinkStat.referrer)
    }
}

package ru.planeta.dao.commondb

import org.apache.commons.lang3.time.DateUtils
import org.junit.Assert.*
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.transaction.annotation.Transactional
import ru.planeta.TestHelper
import ru.planeta.model.common.UserLoginInfo
import java.util.*

/**
 * User: sshendyapin
 * Date: 28.01.13
 * Time: 18:19
 */
@RunWith(SpringRunner::class)
@Transactional
@ContextConfiguration(locations = ["classpath:applicationContext-common-test.xml"])
class TestUserLoginInfoDAO {

    @Autowired
    lateinit var userLoginInfoDAO: UserLoginInfoDAO

    @Test
    fun testInsertSelectUpdateDelete() {
        val profile = TestHelper.createUserProfile()

        val userLoginInfo = UserLoginInfo()
        userLoginInfo.profileId = profile.profileId
        userLoginInfo.userIpAddress = "127.0.0.1"
        userLoginInfo.userAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:18.0) Gecko/20100101 Firefox/18.0"
        userLoginInfo.timeLogin = DateUtils.addHours(Date(), -2)

        val inserted = userLoginInfoDAO!!.insert(userLoginInfo)
        assertEquals(1, inserted.toLong())

        var selectedUserLoginInfo = userLoginInfoDAO.select(profile.profileId, userLoginInfo.userIpAddress!!, userLoginInfo.userAgent!!)
        assertNotNull(selectedUserLoginInfo)
        assertEquals(userLoginInfo.profileId, selectedUserLoginInfo.profileId)
        assertEquals(userLoginInfo.userAgent, selectedUserLoginInfo.userAgent)
        assertEquals(userLoginInfo.userIpAddress, selectedUserLoginInfo.userIpAddress)
        assertEquals(userLoginInfo.timeLogin, selectedUserLoginInfo.timeLogin)

        selectedUserLoginInfo.timeLogin = Date()
        val updated = userLoginInfoDAO.update(selectedUserLoginInfo)
        assertEquals(1, updated.toLong())
        selectedUserLoginInfo = userLoginInfoDAO.select(profile.profileId, userLoginInfo.userIpAddress!!, userLoginInfo.userAgent!!)
        assertTrue(userLoginInfo.timeLogin !== selectedUserLoginInfo.timeLogin)

        val deleted = userLoginInfoDAO.delete(userLoginInfo.profileId, userLoginInfo.userIpAddress!!, userLoginInfo.userAgent!!)
        assertEquals(1, deleted.toLong())
        selectedUserLoginInfo = userLoginInfoDAO.select(profile.profileId, userLoginInfo.userIpAddress!!, userLoginInfo.userAgent!!)
        assertNull(selectedUserLoginInfo)
    }
}

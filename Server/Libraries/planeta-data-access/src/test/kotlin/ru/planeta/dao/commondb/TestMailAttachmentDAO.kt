package ru.planeta.dao.commondb

import org.assertj.core.api.Assertions.assertThat
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.transaction.annotation.Transactional
import ru.planeta.model.mail.MailAttachment


/**
 * Mail attachments DAO tests
 *
 * @author ds.kolyshev
 * Date: 12.01.12
 */
@RunWith(SpringRunner::class)
@Transactional
@ContextConfiguration(locations = ["classpath:applicationContext-common-test.xml"])
class TestMailAttachmentDAO {

    @Autowired
    lateinit var mailAttachmentDAO: MailAttachmentDAO

    @Test
    fun testAddUpdateDelete() {


        val templateId = 999998

        val mailAttachment = createMailAttachment()
        mailAttachment.templateId = templateId
        mailAttachmentDAO!!.insert(mailAttachment)
        assertThat(mailAttachment.attachmentId).isNotNull().isNotEqualTo(0)

        val list = mailAttachmentDAO.selectMailAttachments(templateId)
        assertThat(list).isNotEmpty

        val selected = mailAttachmentDAO.select(mailAttachment.attachmentId!!)
        assertAttachmentEquals(selected, mailAttachment)

        mailAttachment.content = (String(mailAttachment.content!!) + " 2").toByteArray()
        mailAttachment.fileName = mailAttachment.fileName!! + ".txt"
        mailAttachment.mimeType = "text/plain"
        mailAttachment.fileName = mailAttachment.fileName!! + " 2"
        mailAttachment.templateId = mailAttachment.templateId!! + 1

        mailAttachmentDAO.update(mailAttachment)

        val updated = mailAttachmentDAO.select(mailAttachment.attachmentId!!)
        assertAttachmentEquals(updated, mailAttachment)

        mailAttachmentDAO.delete(mailAttachment.attachmentId!!)
        assertThat(mailAttachmentDAO.select(mailAttachment.attachmentId!!)).isNull()


    }

    private fun assertAttachmentEquals(actual: MailAttachment, expected: MailAttachment) {
        assertThat(expected.attachmentId).isEqualTo(actual.attachmentId)
        assertThat(String(expected.content!!)).isEqualTo(String(actual.content!!))
        assertThat(expected.fileName).isEqualTo(actual.fileName)
        assertThat(expected.mimeType).isEqualTo(actual.mimeType)
    }

    private fun createMailAttachment(): MailAttachment {
        val mailAttachment = MailAttachment()
        mailAttachment.fileName = "test.html"
        mailAttachment.mimeType = "text/html"
        mailAttachment.content = "content".toByteArray()
        return mailAttachment
    }
}

package ru.planeta.dao.commondb

import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.transaction.annotation.Transactional


/**
 *
 * User: Serge Blagodatskih<stikkas17></stikkas17>@gmail.com><br></br>
 * Date: 16.02.16<br></br>
 * Time: 16:10
 */
@RunWith(SpringRunner::class)
@Transactional
@ContextConfiguration(locations = ["classpath:applicationContext-common-test.xml"])
class TestInvoiceBillDAO {

    @Autowired
    lateinit var dao: InvoiceBillDAO

    @Test
    fun testSelectList() {
        val bills = dao.select(3, null, 0, 10)
        if (bills.isNotEmpty()) {
            Assert.assertTrue(bills.size <= 10)
        } else {
            println("Data not found.")
        }
    }

    @Test
    fun testSelectOne() {
        val bills = dao.select(3, null, 0, 20)
        Assert.assertNotNull(bills)
        if (bills.isNotEmpty()) {
            val id = bills[bills.size - 1].transactionId
            val bill = dao.select(3, id)
            Assert.assertNotNull(bill)
            Assert.assertEquals(id, bill.transactionId)
        } else {
            println("CAN'T CHECK SELECTONE - EMPTY TABLE.")
        }
    }

}

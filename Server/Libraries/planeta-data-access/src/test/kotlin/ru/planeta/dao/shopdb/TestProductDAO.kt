package ru.planeta.dao.shopdb

import junit.framework.Assert.assertEquals
import org.assertj.core.api.Assertions.assertThat
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.transaction.annotation.Transactional
import ru.planeta.TestHelper
import ru.planeta.model.shop.Product
import ru.planeta.model.shop.enums.ProductState
import ru.planeta.model.shop.enums.ProductStatus
import java.util.*

/**
 * User: a.savanovich
 * Date: 21.06.12
 * Time: 12:07
 */
@RunWith(SpringRunner::class)
@Transactional
@ContextConfiguration(locations = ["classpath:applicationContext-common-test.xml"])
class TestProductDAO {
    @Autowired
    lateinit var productDAO: ProductDAO

    @Test
    @Throws(Exception::class)
    fun testCommonOnAttribute() {

        //test insert
        val product = TestHelper.generateProductTemplate(TestHelper.TEST_PROFILE_ID, 0, ProductState.SOLID_PRODUCT)
        productDAO.save(product)
        assertThat(product.productId).isNotNull().isGreaterThan(0)

        //test update
        val productId = product.productId
        val title1 = product.productState.toString() + TestHelper.rndAttach()
        product.name = title1
        productDAO.save(product)
        var zombie = productDAO.selectCurrent(productId)
        assertThat(zombie).isNotNull
        assertThat(title1).isEqualTo(zombie?.name)
        assertThat(ProductStatus.PAUSE).isEqualTo(zombie?.productStatus)

        val testNameHtml = "testNameHtml" + TestHelper.rndAttach()
        val testDescriptionHtml = "testDescriptionHtml" + TestHelper.rndAttach()
        productDAO.updateProductNameAndDescriptionHtml(product.productId, testNameHtml, testDescriptionHtml)
        val testLast = productDAO.selectCurrent(productId)
        assertThat(testLast).isNotNull
        assertEquals(testNameHtml, testLast?.nameHtml)
        assertEquals(testDescriptionHtml, testLast?.descriptionHtml)
        assertEquals(zombie?.productStatus, testLast?.productStatus)

        var current = productDAO.selectCurrent(productId)
        assertThat(current).isNotNull
        assertEquals(current?.productId, testLast?.productId)
        assertEquals(testNameHtml, current?.nameHtml)

        //common
        val title2 = product.productState.toString() + TestHelper.rndAttach()
        product.name = title2
        productDAO.save(product)


        //--common

        productDAO.updateStatusCurrentToOld(productId, false)

        current = productDAO.selectCurrent(productId)
        assertThat(current).isNull()

        zombie = productDAO.selectProduct(productId)
        assertThat(zombie).isNotNull
        assertEquals(ProductStatus.OLD, zombie?.productStatus)
        productDAO.save(product)

        productDAO.updateStatusPauseToActive(productId)
        current = productDAO.selectCurrent(productId)
        assertThat(current).isNull()

    }

    @Test
    @Throws(Exception::class)
    fun testChildrensMeta() {

        //test insert
        var parent: Product? = TestHelper.createProductTopMETA(TestHelper.TEST_PROFILE_ID)
        assertThat(parent).isNotNull
        parent?.let {
            assertThat(it.productId).isNotNull().isGreaterThan(0)
        }
        val parentId = parent?.productId
        val children = ArrayList<Product>()
        children.add(TestHelper.insertChildProduct(parent!!))
        children.add(TestHelper.insertChildProduct(parent))
        children.add(TestHelper.insertChildProduct(parent))

        var childrenCheck: List<Product>

        childrenCheck = productDAO.selectChildrenProducts(parentId!!)
        assertThat(childrenCheck).isNotNull
        assertEquals(3, childrenCheck.size.toLong())

        for (child in childrenCheck) {
            assertEquals(parentId, child.parentProductId)
            assertEquals(ProductStatus.PAUSE, child.productStatus)
        }


        childrenCheck = productDAO.selectChildrenProducts(parentId)
        for (child in childrenCheck) {
            assertEquals(parentId, child.parentProductId)
            assertEquals(ProductStatus.PAUSE, child.productStatus)

            child.productAttribute.value = "attr" + TestHelper.rndAttach()
            productDAO.save(child)
        }
        parent = productDAO.selectCurrent(parentId)
        assertThat(parent).isNotNull
        assertEquals(ProductStatus.PAUSE, parent?.productStatus)

        childrenCheck = productDAO.selectChildrenProducts(parentId)
        for (child in childrenCheck) {
            assertEquals(parentId, child.parentProductId)
            assertEquals(ProductStatus.PAUSE, child.productStatus)
        }

        productDAO.updateStatusPauseToActive(parentId)

        childrenCheck = productDAO.selectChildrenProducts(parentId)
        assertEquals(3, childrenCheck.size.toLong())
        for (child in childrenCheck) {
            assertEquals(ProductStatus.PAUSE, child.productStatus)
        }
        parent = productDAO.selectCurrent(parentId)
        assertThat(parent).isNotNull
        assertEquals(ProductStatus.ACTIVE, parent?.productStatus)

        productDAO.updateStatusActiveToPause(parentId)
        parent = productDAO.selectCurrent(parentId)
        assertThat(parent).isNotNull
        assertEquals(ProductStatus.PAUSE, parent?.productStatus)

        productDAO.updateStatusPauseToActive(parentId)
        parent = productDAO.selectCurrent(parentId)
        assertThat(parent).isNotNull
        assertEquals(ProductStatus.ACTIVE, parent?.productStatus)

        productDAO.updateStatusActiveToPause(parentId)

        //START:check update count with parent
        productDAO.updateStatusPauseToActive(parentId)
        val count1 = Math.round(Math.random().toFloat() * 100)
        val chuld1Id = childrenCheck[0].productId
        productDAO.updateTotalPurchaseCount(chuld1Id, count1)

        var current: Product?
        current = productDAO.selectCurrent(chuld1Id)!!
        assertThat(current).isNotNull
        assertEquals(count1.toLong(), current.totalPurchaseCount.toLong())
        current = productDAO.selectCurrent(parentId)!!
        assertThat(current).isNotNull
        assertEquals(count1.toLong(), current.totalPurchaseCount.toLong())

        val count2 = Math.round(Math.random().toFloat() * 100) + count1
        val chuld2Id = childrenCheck[1].productId
        productDAO.updateTotalPurchaseCount(chuld2Id, count2)
        current = productDAO.selectCurrent(chuld1Id)!!
        assertThat(current).isNotNull
        assertEquals(count1.toLong(), current.totalPurchaseCount.toLong())
        current = productDAO.selectCurrent(chuld2Id)
        assertThat(current).isNotNull
        assertEquals(count2.toLong(), current?.totalPurchaseCount?.toLong())
        current = productDAO.selectCurrent(parentId)
        assertThat(current).isNotNull
        assertEquals((count1 + count2).toLong(), current?.totalPurchaseCount?.toLong())

        //END:check update count with parent
        childrenCheck = productDAO.selectChildrenProducts(parentId)
        for (child in childrenCheck) {
            assertEquals(ProductStatus.PAUSE, child.productStatus)
        }
        //        productDAO.save(parent);
        TestHelper.insertChildProduct(parent!!)
        TestHelper.insertChildProduct(parent)
        TestHelper.insertChildProduct(parent)
        TestHelper.insertChildProduct(parent)

        var productList = productDAO.selectTopLevelProducts(0, 0, null, 0, null)
        assertThat(productList).isNotNull.isNotEmpty
        productDAO.freeChildren(parentId)


        productList = productDAO.selectTopLevelProducts(0, 0, null, 0, null)
        assertThat(productList).isNotNull.isNotEmpty

        //todo create test for free from group
        //        ---------------------

        productDAO.updateStatusCurrentToOld(parentId, true)
        childrenCheck = productDAO.selectChildrenProductsByStatus(parentId, ProductStatus.OLD)
        for (child in childrenCheck) {
            assertThat(ProductStatus.OLD).isEqualTo(child.productStatus)
        }

    }

    @Test
    @Throws(Exception::class)
    fun testFilterSelect() {
        val product = TestHelper.createProductTopMETA(TestHelper.TEST_PROFILE_ID)
        val productId = product.productId
        productDAO.updateStatusPauseToActive(productId)
        val found = productDAO.selectCurrent(productId)
        assertThat(found).isNotNull
    }

}

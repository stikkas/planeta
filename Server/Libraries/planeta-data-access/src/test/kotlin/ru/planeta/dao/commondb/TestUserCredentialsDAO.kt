package ru.planeta.dao.commondb


import org.apache.ibatis.exceptions.PersistenceException
import org.junit.Assert.*
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.dao.DuplicateKeyException
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.transaction.annotation.Transactional
import ru.planeta.TestHelper
import ru.planeta.dao.profiledb.ProfileDAO
import ru.planeta.model.common.auth.CredentialType
import ru.planeta.model.common.auth.UserCredentials
import ru.planeta.model.profile.Profile
import java.util.*

/**
 * Date: 12.09.12
 *
 * @author s.kalmykov
 */
@RunWith(SpringRunner::class)
@Transactional
@ContextConfiguration(locations = ["classpath:applicationContext-common-test.xml"])
class TestUserCredentialsDAO {

    @Autowired
    lateinit var userCredentialsDAO: UserCredentialsDAO

    @Autowired
    lateinit var profileDAO: ProfileDAO

    private fun insertUserAndGetId(): Long {
        return insertUser().profileId
    }

    private fun insertUser(): Profile {
        val profile = TestHelper.createUserProfile()
        profileDAO.insert(profile)
        return profile
    }

    @Test(expected = DuplicateKeyException::class)
    fun testInsert() {
        val profileId = insertUserAndGetId()
        val userCredentials = createUserCredentials(profileId)
        userCredentialsDAO.insert(userCredentials)
        val insertedCredentials = userCredentialsDAO.getUserCredentialsOfType(profileId)
        assertNotNull(insertedCredentials)
        assertEquals(1, insertedCredentials.size.toLong())
        val insertedUserCredentials = insertedCredentials[0]
        assertNotNull(insertedUserCredentials)
        assertEquals(profileId, insertedUserCredentials.profileId)
        assertEquals(userCredentials.userName, insertedUserCredentials.userName)
        assertEquals(userCredentials.credentialType, insertedUserCredentials.credentialType)

        // check unique username-type constrain
        userCredentials.profileId = 111
        userCredentialsDAO.insert(userCredentials)
    }

    @Test
    fun testDeleteByProfileId() {
        val profileId = insertUserAndGetId()
        val userCredentials = createUserCredentials(profileId)
        userCredentialsDAO.insert(userCredentials)
        val insertedCredentials = userCredentialsDAO.getUserCredentialsOfType(profileId)
        assertNotNull(insertedCredentials)
        assertEquals(1, insertedCredentials.size.toLong())
        val insertedUserCredentials = insertedCredentials[0]
        assertNotNull(insertedUserCredentials)
        assertEquals(profileId, insertedUserCredentials.profileId)
        assertEquals(userCredentials.userName, insertedUserCredentials.userName)
        assertEquals(userCredentials.credentialType, insertedUserCredentials.credentialType)

        val affectedRows = userCredentialsDAO.deleteByProfileId(insertedUserCredentials.profileId, insertedUserCredentials.credentialType!!)
        assertEquals(1, affectedRows.toLong())
    }

    @Test
    @Throws(Exception::class)
    fun testDeleteByUsername() {
        val profileId = insertUserAndGetId()
        val userCredentials = createUserCredentials(profileId)
        userCredentialsDAO.insert(userCredentials)
        val insertedCredentials = userCredentialsDAO.getUserCredentialsOfType(profileId)
        assertNotNull(insertedCredentials)
        assertEquals(1, insertedCredentials.size.toLong())
        val insertedUserCredentials = insertedCredentials[0]
        assertNotNull(insertedUserCredentials)
        assertEquals(profileId, insertedUserCredentials.profileId)
        assertEquals(userCredentials.userName, insertedUserCredentials.userName)
        assertEquals(userCredentials.credentialType, insertedUserCredentials.credentialType)

        val affectedRows = userCredentialsDAO.deleteByUsername(insertedUserCredentials.userName!!, insertedUserCredentials.credentialType!!)
        assertEquals(1, affectedRows.toLong())
    }

    @Test
    @Throws(Exception::class)
    fun testSelect() {
        val profileId = insertUserAndGetId()
        val userCredentials = createUserCredentials(profileId)
        userCredentialsDAO.insert(userCredentials)
        val insertedUserCredentials = userCredentialsDAO.select(userCredentials.userName!!, userCredentials.credentialType!!)
        assertNotNull(insertedUserCredentials)
        val iHopeNoOneHaveThisEmail = "111111111111111111111111111111"
        assertNull(userCredentialsDAO.select(iHopeNoOneHaveThisEmail, CredentialType.EMAIL))
    }

    @Test
    @Throws(Exception::class)
    fun testGetUserCredentialsList() {
        val profileId = insertUserAndGetId()
        // empty list
        assertNotNull(userCredentialsDAO.getUserCredentialsOfType(profileId))
        val userCredentials = createUserCredentials(profileId)
        userCredentialsDAO.insert(userCredentials)
        userCredentials.credentialType = CredentialType.VK
        userCredentialsDAO.insert(userCredentials)
        userCredentials.credentialType = CredentialType.FACEBOOK
        userCredentialsDAO.insert(userCredentials)

        val insertedCredentials = userCredentialsDAO.getUserCredentialsOfType(profileId)
        assertEquals(3, insertedCredentials.size.toLong())
        // check list order == insert order
        assertEquals(CredentialType.VK, insertedCredentials[1].credentialType)
    }

    @Test
    @Throws(Exception::class)
    fun testGetUserCredentialsOfType() {
        val profileId = insertUserAndGetId()
        val userCredentials = createUserCredentials(profileId)
        userCredentialsDAO.insert(userCredentials)
        userCredentials.credentialType = CredentialType.VK
        userCredentialsDAO.insert(userCredentials)

        var credentials = userCredentialsDAO.getUserCredentialsOfType(profileId, CredentialType.VK)
        assertNotNull(credentials)
        credentials = userCredentialsDAO.getUserCredentialsOfType(profileId, CredentialType.FACEBOOK)
        assertNotNull(credentials)
    }

    @Test
    @Throws(Exception::class)
    fun testGetUserCredentialsByVkUids() {
        val profiles = ArrayList<Profile>()
        val vkUids = ArrayList<String>()
        for (i in 0..4) {
            val profile = insertUser()
            profiles.add(profile)
            val userCredentials = createUserCredentials(profile.profileId)
            val vkUid = java.lang.Long.toString(i.toLong())
            vkUids.add(vkUid)
            userCredentials.userName = vkUid
            userCredentials.credentialType = CredentialType.VK
            userCredentialsDAO.insert(userCredentials)

        }


        val credentials = userCredentialsDAO!!.getUserCredentialsOfTypeByUsernames(vkUids, CredentialType.VK)
        assertNotNull(credentials)
        assertEquals(5, credentials.size.toLong())
        assertTrue(containsAllVkuids(credentials, vkUids))
    }

    private fun containsAllVkuids(credentials: List<UserCredentials>, vkUids: List<String>): Boolean {
        if (credentials.size != vkUids.size) {
            return false
        }
        for (userCredentials in credentials) {
            val vkUid = userCredentials.userName
            var isFound = false
            for (uid in vkUids) {
                if (vkUid == uid) {
                    isFound = true
                    break
                }
            }
            if (!isFound) {
                return false
            }
        }
        return true
    }

    private fun createUserCredentials(profileId: Long): UserCredentials {
        val userCredentials = UserCredentials()
        userCredentials.profileId = profileId
        userCredentials.userName = "login"
        userCredentials.credentialType = CredentialType.EMAIL
        return userCredentials
    }
}


package ru.planeta.dao.statdb

import org.junit.Test
import org.springframework.beans.factory.annotation.Autowired
import ru.planeta.model.common.ShareServiceType
import ru.planeta.model.stat.SharedObject
import ru.planeta.model.stat.SharedObjectStats


import org.junit.Assert.assertEquals
import org.junit.Assert.assertNotNull
import org.junit.runner.RunWith
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.transaction.annotation.Transactional
import ru.planeta.TestHelper

/**
 * @author m.shulepov
 */
@RunWith(SpringRunner::class)
@Transactional
@ContextConfiguration(locations = ["classpath:applicationContext-common-test.xml"])
class TestSharedObjectDAO {

    @Autowired
    lateinit var sharedObjectDAO: SharedObjectDAO

    @Autowired
    lateinit var sharedObjectStatsDAO: SharedObjectStatsDAO

    @Test
    fun testInsertUpdateSelect() {

        val url = "http://planeta.ru/{profileId}/blogs/{blogId}"

        val profileId = TestHelper.TEST_PROFILE_ID

        val urlProfileIdOne: Long = 1000
        val urlProfileIdTwo: Long = 2000
        val blogId: Long = 1

        // create and share first url
        val urlOne = createSharedUrl(url, urlProfileIdOne, blogId)
        var sharedObjectStats = SharedObjectStats(urlOne)
        sharedObjectStatsDAO!!.insert(sharedObjectStats)

        var sharedObject = createSharedObject(profileId, sharedObjectStats.sharedObjectId, ShareServiceType.FACEBOOK)
        sharedObjectDAO!!.insertDetails(sharedObject)

        // create and share second url
        val urlTwo = createSharedUrl(url, urlProfileIdTwo, blogId)
        sharedObjectStats = SharedObjectStats(urlTwo)
        sharedObjectStatsDAO.insert(sharedObjectStats)

        sharedObject = createSharedObject(profileId, sharedObjectStats.sharedObjectId, ShareServiceType.VKONTAKTE)
        sharedObjectDAO.insertDetails(sharedObject)

        var selected = sharedObjectStatsDAO.selectByUrl(urlOne)
        assertNotNull(selected)
        assertEquals(urlOne, selected!!.url)
        assertEquals(1, selected.count)

        selected = sharedObjectStatsDAO.selectByUrl(urlTwo)
        assertNotNull(selected)
        assertEquals(urlTwo, selected!!.url)
        assertEquals(1, selected.count)

        // selectCampaignById details
        val seletedSharedObject = sharedObjectDAO.selectLastSharedObjectDetails(sharedObject.sharedObjectId, ShareServiceType.VKONTAKTE)
        assertNotNull(seletedSharedObject)
        assertEquals(profileId, sharedObject.authorProfileId)

        // share second url 2 more times
        sharedObjectStatsDAO.updateCount(sharedObject.sharedObjectId)
        sharedObjectDAO.insertDetails(seletedSharedObject)
        sharedObjectStatsDAO.updateCount(sharedObject.sharedObjectId)
        sharedObjectDAO.insertDetails(seletedSharedObject)

        selected = sharedObjectStatsDAO.selectByUrl(urlTwo)
        assertNotNull(selected)
        assertEquals(3, selected!!.count)
    }

    private fun createSharedObject(clientId: Long, sharedObjectId: Long, shareServiceType: ShareServiceType): SharedObject {
        val sharedObject = SharedObject()
        sharedObject.sharedObjectId = sharedObjectId
        sharedObject.authorProfileId = clientId
        sharedObject.shareServiceType = shareServiceType
        return sharedObject
    }

    private fun createSharedUrl(url: String, profileId: Long, blogId: Long): String {
        return url.replace("{profileId}", profileId.toString()).replace("{blogId}", blogId.toString())
    }
}

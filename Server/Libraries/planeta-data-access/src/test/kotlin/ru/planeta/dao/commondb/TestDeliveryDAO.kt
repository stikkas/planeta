package ru.planeta.dao.commondb


import com.fasterxml.jackson.databind.ObjectMapper
import org.junit.Assert.*
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.transaction.annotation.Transactional
import ru.planeta.TestHelper
import ru.planeta.model.common.Address
import ru.planeta.model.common.delivery.BaseDelivery
import ru.planeta.model.common.delivery.SubjectType
import ru.planeta.model.profile.location.Country
import ru.planeta.model.shop.enums.DeliveryType
import java.io.IOException
import java.io.StringWriter
import java.math.BigDecimal

/**
 * @author Andrew.Arefyev@gmail.com
 * 31.10.13 15:18
 */
@RunWith(SpringRunner::class)
@Transactional
@ContextConfiguration(locations = ["classpath:applicationContext-common-test.xml"])
class TestDeliveryDAO {

    @Autowired
    lateinit var deliveryDAO: DeliveryDAO

    @Autowired
    lateinit var campaignDAO: CampaignDAO

    @Autowired
    lateinit var shareDAO: ShareDAO

    @Test
    @Throws(Exception::class)
    fun testBaseDeliveryServices() {
        var deliveryService = createBaseDeliveryService()
        deliveryDAO.insert(deliveryService)

        var deliveryServices = deliveryDAO.selectBaseDeliveries(null, deliveryService.name)
        assertTrue(deliveryServices.size > 0)

        assertServicesEqual(deliveryService, findServiceById(deliveryService, deliveryServices)!!)

        deliveryService = createBaseDeliveryService()
        deliveryDAO.insert(deliveryService)

        deliveryServices = deliveryDAO.selectBaseDeliveries(deliveryService.serviceId, null)
        assertTrue(deliveryServices.size == 1)

        deliveryService.name = "Changed Name"
        deliveryService.description = "Changed Description"
        deliveryDAO.updateBaseDelivery(deliveryService)

        deliveryServices = deliveryDAO.selectBaseDeliveries(null, null)
        assertServicesEqual(deliveryService, findServiceById(deliveryService, deliveryServices)!!)

        deliveryDAO.removeBaseDelivery(deliveryService.serviceId)

        deliveryServices = deliveryDAO.selectBaseDeliveries(null, null)
        assertNull(findServiceById(deliveryService, deliveryServices))

        assertNotNull(deliveryDAO.selectBaseDelivery(BaseDelivery.DEFAULT_DELIVERY_SERVICE_ID))
        assertNotNull(deliveryDAO.selectBaseDelivery(BaseDelivery.DEFAULT_CUSTOMER_PICKUP_SERVICE_ID))
        assertEquals(1, deliveryDAO.selectBaseDeliveries(BaseDelivery.DEFAULT_CUSTOMER_PICKUP_SERVICE_ID, null).size.toLong())
        assertEquals(1, deliveryDAO.selectBaseDeliveries(BaseDelivery.DEFAULT_DELIVERY_SERVICE_ID, null).size.toLong())
    }

    @Test
    @Throws(Exception::class)
    fun testDeliveryServiceItems() {
        var deliveryService = createBaseDeliveryService()
        deliveryDAO.insert(deliveryService)

        val campaign = TestHelper.createCampaign(TestHelper.TEST_PROFILE_ID)
        campaignDAO.insert(campaign)
        val share = TestHelper.createShare(TestHelper.TEST_PROFILE_ID, campaign.campaignId)
        shareDAO.insert(share)

        deliveryService = createBaseDeliveryService()
        deliveryDAO.insert(deliveryService)

        deliveryDAO.addDeliveryToShare(share.shareId, deliveryService.serviceId, "Test Note", BigDecimal.TEN, SubjectType.SHARE)

        var serviceItems = deliveryDAO.select(share.shareId, SubjectType.SHARE)


        assertNotNull(serviceItems)
        assertEquals(1, serviceItems.size.toLong())

        val linkedDeliveryService = deliveryDAO.select(share.shareId, deliveryService.serviceId, SubjectType.SHARE)
        linkedDeliveryService.isEnabled = false
        deliveryDAO.updateLinkedDelivery(linkedDeliveryService)

        serviceItems = deliveryDAO.select(share.shareId, SubjectType.SHARE)

        assertNotNull(serviceItems)
        assertEquals(1, serviceItems.size.toLong())

        serviceItems = deliveryDAO.selectEnabled(share.shareId, SubjectType.SHARE)
        assertNotNull(serviceItems)
        assertEquals(0, serviceItems.size.toLong())

    }

    @Test
    @Throws(IOException::class)
    fun testCustomSerialization() {
        val initialDeliveryService = createBaseDeliveryService()
        val initialClass = initialDeliveryService.location?.javaClass

        val objectMapper = ObjectMapper()
        val stringWriter = StringWriter()
        objectMapper.writeValue(stringWriter, initialDeliveryService)
        val json = stringWriter.buffer.toString()
        assertNotNull(json)
        println(json)

        val actualDeliveryService = objectMapper.readValue(json, BaseDelivery::class.java)
        assertNotNull(actualDeliveryService)
        assertTrue(actualDeliveryService.location?.javaClass?.isAssignableFrom(initialClass)!!)
    }

    private fun createBaseDeliveryService(): BaseDelivery {
        val deliveryService = BaseDelivery()
        val address = Address()
        val addressId = Math.round(1000000L * Math.random())
        address.setAddressId(addressId)
        deliveryService.address = address

        val country = Country()
        val countryId = 1
        country.countryId = countryId
        deliveryService.location = country

        deliveryService.serviceType = DeliveryType.DELIVERY
        deliveryService.name = "Test Service Name" + TestHelper.rnd.nextInt(1000)
        deliveryService.description = "Test Service Description" + TestHelper.rnd.nextInt(1000)
        return deliveryService
    }

    private fun assertServicesEqual(expected: BaseDelivery, actual: BaseDelivery) {
        assertEquals(expected.serviceId, actual.serviceId)
        assertEquals(expected.serviceType, actual.serviceType)
        assertEquals(expected.name, actual.name)
        assertEquals(expected.description, actual.description)
    }

    private fun findServiceById(deliveryService: BaseDelivery, deliveryServices: List<BaseDelivery>): BaseDelivery? {
        for (service in deliveryServices) {
            if (service.serviceId == deliveryService.serviceId) {
                return service
            }
        }
        return null
    }
}


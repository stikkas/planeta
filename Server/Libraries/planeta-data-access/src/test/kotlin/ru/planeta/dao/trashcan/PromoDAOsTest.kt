package ru.planeta.dao.trashcan


import org.junit.Assert
import org.junit.Ignore
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.transaction.annotation.Transactional
import ru.planeta.model.enums.PromoConfigStatus
import ru.planeta.model.trashcan.PromoConfig
import java.math.BigDecimal
import java.util.*

/**
 * Created by asavan on 25.01.2017.
 */
@RunWith(SpringRunner::class)
@Transactional
@ContextConfiguration(locations = ["classpath:applicationContext-common-test.xml"])
class PromoDAOsTest {
    @Autowired
    lateinit var promoConfigDAO: PromoConfigDAO

    @Test
    @Ignore
    fun selectLastNonUsed() {
        val configsOld = promoConfigDAO!!.selectAll()

        val tags = longArrayOf(1500L, 1501L, 1502L)
        val config = PromoConfig("test", PromoConfigStatus.ACTIVE, tags,
                GregorianCalendar(2110, 1, 1).time,
                GregorianCalendar(2120, 1, 1).time,
                BigDecimal.ZERO, "litres.promo.a", 0, false)
        promoConfigDAO.insert(config)

        var configsNew = promoConfigDAO.selectAll()

        Assert.assertEquals(configsNew.size.toLong(), (configsOld.size + 1).toLong())

        promoConfigDAO.insert(config)

        val tagsNew = longArrayOf(1600L, 1601L, 1602L)
        config.name = "test2"
        config.campaignTags = tagsNew
        config.timeStart = GregorianCalendar(2015, 1, 1).time
        config.timeFinish = GregorianCalendar(2025, 1, 1).time
        config.isHasPromocode = true
        config.mailTemplate = "litres.promo.b"
        config.priceCondition = BigDecimal.TEN
        config.usageCount = 1

        promoConfigDAO.update(config)
        configsNew = promoConfigDAO.selectAll()

        val compTags = object : ArrayList<Long>() {
            init {
                add(1500L)
                add(1600L)
            }
        }

        val compatibleConfigs = promoConfigDAO.selectCompatible(compTags, BigDecimal.ONE)
    }

}

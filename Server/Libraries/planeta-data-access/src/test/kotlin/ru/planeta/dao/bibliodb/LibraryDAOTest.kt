package ru.planeta.dao.bibliodb

import org.junit.Assert
import org.junit.Assert.assertEquals
import org.junit.Assert.assertFalse
import org.junit.Ignore
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.transaction.annotation.Transactional
import ru.planeta.model.bibliodb.Library

@RunWith(SpringRunner::class)
@Transactional
@ContextConfiguration(locations = ["classpath:applicationContext-common-test.xml"])
class LibraryDAOTest {
    @Autowired
    lateinit var libraryDAO: LibraryDAO

    private fun assertLibrary(expected: Library, actual: Library) {
        assertEquals(expected.address, actual.address)
        assertEquals(expected.latitude, actual.latitude)
        assertEquals(expected.longitude, actual.longitude)
        assertEquals(expected.name, actual.name)
        assertEquals(expected.region, actual.region)
        assertEquals(expected.comment, actual.comment)
        assertEquals(expected.contractor, actual.contractor)
        assertEquals(expected.email, actual.email)
    }

    private fun createLibrary(): Library {
        val lib1 = Library()
        lib1.address = "addr1"
        lib1.latitude = 90.0
        lib1.longitude = 0.0
        lib1.name = "test1"
        lib1.region = "reg1"
        return lib1
    }

    @Test
    fun testInsertLibrary() {
        val lib1 = createLibrary()
        libraryDAO.insert(lib1)
    }

    @Test
    fun testSelectLibrary() {
        val lib1 = createLibrary()

        libraryDAO.insert(lib1)
        val lib2 = libraryDAO.select(lib1.libraryId)!!

        assertLibrary(lib1, lib2)
    }

    private fun createLibraryRequest(): Library {
        val l = Library()
        l.name = "title"
        l.email = "email"
        l.contractor = "contact"
        l.site = "site"
        l.address = "address"
        l.postIndex = "zipcode"
        l.region = "region"
        return l
    }

    @Test
    fun testHasRequests() {
        val l = createLibraryRequest()
        libraryDAO.insert(l)
        Assert.assertEquals(libraryDAO.hasLibraryRequests(l.name ?: "", l.email!!), true)
    }

    @Test
    @Ignore
    fun searchLibrary() {
        val list = libraryDAO.searchLibraries("мос алтуф", null, 90.0, 0.0, 90.0, 0.0)
        assertFalse(list.isEmpty())

        val list2 = libraryDAO.searchLibraries("", null, 90.0, 0.0, 90.0, 0.0)
        assertFalse(list2.isEmpty())
    }

    @Test
    @Ignore
    fun searchCluster() {
        val list = libraryDAO.searchLibraryClusters("мос алтуф", null, 90.0, 0.0, 90.0, 0.0)
        assertFalse(list.isEmpty())

        val list2 = libraryDAO.searchLibraryClusters("", null, 90.0, 0.0, 90.0, 0.0)
        assertFalse(list2.isEmpty())
    }
}


package ru.planeta.dao.shopdb

import org.junit.Assert.*
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.transaction.annotation.Transactional
import ru.planeta.TestHelper
import ru.planeta.model.shop.Product
import ru.planeta.model.shop.enums.ProductState

@RunWith(SpringRunner::class)
@Transactional
@ContextConfiguration(locations = ["classpath:applicationContext-common-test.xml"])
class TestProductTagDAO {

    @Autowired
    lateinit var productDAO: ProductDAO

    @Autowired
    lateinit var productTagDAO: ProductTagDAO

    @Test
    fun testSelectAll() {
        val allTags = productTagDAO!!.selectAll(0, 0)
        assertNotNull(allTags)
        assertTrue(allTags.size > 0)
    }

    @Test
    fun testSelectUpdateTagsByCampaign() {
        val product = TestHelper.generateProductTemplate(TestHelper.TEST_PROFILE_ID, 0, ProductState.SOLID_PRODUCT)
        productDAO.save(product)
        assertNotNull(product)
        assertTrue(product.productId > 0)

        var selectedTags = productTagDAO.selectTagsByProduct(product.productId)
        assertNotNull(selectedTags)
        assertEquals(0, selectedTags.size.toLong())

        val allTags = productTagDAO.selectAll(0, 0)

        val tagsSize = allTags.size / 2
        val newTags = allTags.subList(0, tagsSize)
        productTagDAO.updateTags(product.productId, newTags)

        selectedTags = productTagDAO.selectTagsByProduct(product.productId)
        assertNotNull(selectedTags)
        assertEquals(tagsSize.toLong(), selectedTags.size.toLong())
        for (i in selectedTags.indices) {
            assertEquals(selectedTags[i].categoryId, newTags[i].categoryId)
        }
    }

    @Test
    fun testCampaignTagsRelation() {
        var product: Product? = TestHelper.generateProductTemplate(TestHelper.TEST_PROFILE_ID, 0, ProductState.SOLID_PRODUCT)
        productDAO.save(product!!)
        assertNotNull(product)
        assertTrue(product.productId > 0)

        assertNotNull(product.getTags())
        assertEquals(1, product.getTags().size.toLong())

        val allTags = productTagDAO!!.selectAll(0, 0)

        val tagsSize = allTags.size / 2
        productTagDAO.updateTags(product.productId, allTags.subList(0, tagsSize))

        product = productDAO.selectProduct(product.productId)
        assertNotNull(product!!.getTags())
        assertEquals(tagsSize.toLong(), product.getTags().size.toLong())
    }
}

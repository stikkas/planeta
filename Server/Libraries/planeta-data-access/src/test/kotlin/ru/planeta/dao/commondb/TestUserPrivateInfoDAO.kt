package ru.planeta.dao.commondb


import org.junit.Assert.*
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.transaction.annotation.Transactional
import ru.planeta.TestHelper.TEST_PROFILE_ID
import ru.planeta.model.common.UserPrivateInfo
import ru.planeta.model.enums.UserStatus
import java.util.*

@RunWith(SpringRunner::class)
@Transactional
@ContextConfiguration(locations = ["classpath:applicationContext-common-test.xml"])
class TestUserPrivateInfoDAO {

    @Autowired
    lateinit var userPrivateInfoDAO: UserPrivateInfoDAO

    @Test
    fun testUserPrivateInfoDAO() {


        // Inserting user general object
        val userPrivateInfo = insertUserPrivateInfo("tratatat@test.ee", "123123")

        // Checking selecting by user identifier
        assertNotNull(userPrivateInfoDAO!!.selectByUserId(userPrivateInfo.userId))

        // Checking selecting by email
        assertNotNull(userPrivateInfoDAO.selectByEmail(userPrivateInfo.email!!))
        assertNotNull(userPrivateInfoDAO.selectByUsername(userPrivateInfo.username!!))

        // Checking selecting by reg code
        assertNotNull(userPrivateInfoDAO.selectByRegCode(userPrivateInfo.regCode!!))

        // Checking update method
        userPrivateInfo.email = userPrivateInfo.email!! + ".ru"
        userPrivateInfo.username = userPrivateInfo.email
        userPrivateInfo.userStatus = EnumSet.of(UserStatus.USER, UserStatus.ADMIN)

        // Updating userPrivateInfo and than checking updated object
        userPrivateInfoDAO.update(userPrivateInfo)
        val updated = userPrivateInfoDAO.selectByUserId(userPrivateInfo.userId)
        assertTrue(userPrivateInfo.email == updated!!.email)
        assertTrue(userPrivateInfo.username == updated.username)
        assertTrue(updated.userStatus!!.containsAll(userPrivateInfo.userStatus!!))
        assertTrue(userPrivateInfo.userStatus!!.containsAll(updated.userStatus!!))

        val infos = userPrivateInfoDAO.selectByStatus(EnumSet.of(UserStatus.MANAGER, UserStatus.ADMIN, UserStatus.SUPER_ADMIN))
        val infoIds = ArrayList<Long>()
        for (info in infos) {
            infoIds.add(info.userId)
        }
        assertTrue(infoIds.contains(userPrivateInfo.userId))

        // Deleting userPrivateInfo
        userPrivateInfoDAO.delete(userPrivateInfo.userId)

        // Checking deletion is complete
        assertNull(userPrivateInfoDAO.selectByUserId(userPrivateInfo.userId))
        //
        val userPrivateInfo1 = insertUserPrivateInfo("email@test.ee", "121212")
        assertNotNull(userPrivateInfo1.regCode)
        //        int notification_email_delayed = 1000 * 60 * 60; //one hour
        //        Date oneHourAgo = DateUtils.addMilliseconds(new Date(), -notification_email_delayed);
        //            Date now = DateUtils.addMilliseconds(oneHourAgo, notification_email_delayed);
        //            assertEquals(new Date(), now);
        //        List<UserPrivateInfo> userPrivateInfoList = userPrivateInfoDAO.selectUserPrivateInfoNotNullRegCode(oneHourAgo, null, 0, 0);
        //todo uncomment assertEquals(true, !CollectionUtils.isEmpty(userPrivateInfoList));

    }

    private fun insertUserPrivateInfo(email: String, password: String): UserPrivateInfo {
        val userPrivateInfo = UserPrivateInfo()
        userPrivateInfo.userId = TEST_PROFILE_ID
        userPrivateInfo.email = email.trim { it <= ' ' }.toLowerCase()
        userPrivateInfo.username = userPrivateInfo.email
        userPrivateInfo.password = password
        userPrivateInfo.userStatus = EnumSet.of(UserStatus.USER)
        userPrivateInfo.regCode = (Math.random() * 10000).toInt().toString()

        userPrivateInfoDAO!!.insert(userPrivateInfo)
        return userPrivateInfo
    }

    @Test
    fun testBigLetters() {
        val email = "Devoiter1234@gmail.com"
        val info = userPrivateInfoDAO!!.selectByEmail(email)
        assertNotNull(info)
    }
}

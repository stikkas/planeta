package ru.planeta.dao.commondb

import org.junit.Assert.assertNull
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.transaction.annotation.Transactional
import java.util.*

/**
 * Created by a.savanovich on 28.09.2016.
 */
@RunWith(SpringRunner::class)
@Transactional
@ContextConfiguration(locations = ["classpath:applicationContext-common-test.xml"])
class ShortLinkDAOImplTest {

    @Autowired
    lateinit var shortLinkDAO: ShortLinkDAO

    @Autowired
    lateinit var configurationDAO: ConfigurationDAO

    @Test
    fun testSelectByAlias() {
        val selected = shortLinkDAO.selectByAlias("/")
        assertNull(selected)
    }

    @Test
    fun testSelectExistsSocialShortLinksByBasename() {
        configurationDAO.select(
                "shortlink.extra.social.links")?.stringValue?.replace(" ".toRegex(), "")?.split(",".toRegex())?.dropLastWhile { it.isEmpty() }?.toList()?.let {

            val selected = shortLinkDAO.selectExistsSocialShortLinksByBasename("barcelona",
                    it, 0, 100)
            for (shortl in selected) {
                println(shortl.shortLink)
            }
        }
    }
}

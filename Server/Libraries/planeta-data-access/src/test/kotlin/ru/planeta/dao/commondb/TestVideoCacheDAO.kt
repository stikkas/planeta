package ru.planeta.dao.commondb

import org.junit.Test
import org.springframework.beans.factory.annotation.Autowired
import ru.planeta.model.common.CachedVideo

import org.junit.Assert.assertNotNull
import org.junit.Assert.assertTrue
import org.junit.runner.RunWith
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.transaction.annotation.Transactional

/**
 * Created with IntelliJ IDEA.
 * User: connanedoile
 * Date: 4/10/12
 * Time: 12:37 PM
 */
@RunWith(SpringRunner::class)
@Transactional
@ContextConfiguration(locations = ["classpath:applicationContext-common-test.xml"])
class TestVideoCacheDAO {

    @Autowired
    lateinit var videoCacheDAO: VideoCacheDAO

    private val cachedVideo: CachedVideo
        get() {
            val cachedVideo = CachedVideo()

            cachedVideo.description = "Brad finally gets the attention he deserves."
            cachedVideo.duration = 118
            cachedVideo.height = 720
            cachedVideo.width = 1280
            cachedVideo.title = "Brad!"
            cachedVideo.html = "<iframe src=\"http://player.vimeo.com/video/7100569\" width=\"1280\" height=\"720\" frameborder=\"0\" webkitAllowFullScreen allowFullScreen></iframe>"
            cachedVideo.thumbnailUrl = "http://b.vimeocdn.com/ts/294/128/29412830_1280.jpg"
            cachedVideo.url = "http://vimeo.com/7100569"

            return cachedVideo
        }

    @Test
    @Throws(Exception::class)
    fun testVideoCacheDao() {
        val cachedVideo = cachedVideo


        videoCacheDAO.insert(cachedVideo)

        val findById = videoCacheDAO.selectById(cachedVideo.cachedVideoId)
        assertNotNull(findById)
        assertTrue(findById.cachedVideoId == cachedVideo.cachedVideoId)

        val findByUrl = videoCacheDAO.selectByUrl(cachedVideo.url!!)
        assertNotNull(findByUrl)
        assertTrue(findByUrl.url!!.equals(cachedVideo.url!!, ignoreCase = true))


    }
}

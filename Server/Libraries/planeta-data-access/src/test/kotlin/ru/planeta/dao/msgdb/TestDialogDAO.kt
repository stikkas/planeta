package ru.planeta.dao.msgdb

import org.assertj.core.api.Assertions.assertThat
import org.junit.Test
import org.springframework.beans.factory.annotation.Autowired
import ru.planeta.model.msg.Dialog


import java.util.Date

import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.runner.RunWith
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.transaction.annotation.Transactional

/**
 * @author ameshkov
 */
@RunWith(SpringRunner::class)
@Transactional
@ContextConfiguration(locations = ["classpath:applicationContext-common-test.xml"])
class TestDialogDAO {

    @Autowired
    lateinit var dialogDAO: DialogDAO

    @Test
    fun testDialogAddUpdateRemove() {


        val dialog = Dialog()
        dialog.creatorUserId = 1000
        dialog.lastMessageId = 1
        dialog.messagesCount = 5
        dialog.name = null
        dialog.timeAdded = Date()

        dialogDAO.insert(dialog)
        assertThat(dialog.dialogId).isGreaterThan(0)

        var selected = dialogDAO.select(dialog.dialogId)
        assertEquals(dialog.creatorUserId, selected.creatorUserId)
        assertEquals(dialog.lastMessageId, selected.lastMessageId)
        assertEquals(dialog.messagesCount, selected.messagesCount)

        dialog.lastMessageId = dialog.lastMessageId + 1
        dialog.messagesCount = dialog.messagesCount + 1
        dialog.name = "Ololo"
        dialogDAO.update(dialog)

        selected = dialogDAO.select(dialog.dialogId)
        assertEquals(dialog.creatorUserId, selected.creatorUserId)
        assertEquals(dialog.lastMessageId, selected.lastMessageId)
        assertEquals(dialog.messagesCount, selected.messagesCount)
        assertEquals(dialog.name, selected.name)


    }
}

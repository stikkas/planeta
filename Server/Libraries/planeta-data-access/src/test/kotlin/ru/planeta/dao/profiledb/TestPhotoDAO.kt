package ru.planeta.dao.profiledb


import org.junit.Assert.*
import org.junit.Ignore
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.transaction.annotation.Transactional
import ru.planeta.TestHelper
import ru.planeta.model.enums.PermissionLevel
import ru.planeta.model.profile.media.Photo
import ru.planeta.model.profile.media.PhotoAlbum
import java.util.*

/**
 * @author ds.kolyshev
 * Date: 27.10.11
 */
@Ignore
@RunWith(SpringRunner::class)
@Transactional
@ContextConfiguration(locations = ["classpath:applicationContext-common-test.xml"])
class TestPhotoDAO {

    private val profileId: Long = 9999988
    @Autowired
    lateinit var photoAlbumDAO: PhotoAlbumDAO
    @Autowired
    lateinit var photoDAO: PhotoDAO

    @Test
    fun testAddUpdateRemove() {
        val photoAlbum = createAlbum()
        photoAlbum.viewPermission = PermissionLevel.FRIENDS
        photoAlbumDAO!!.insert(photoAlbum)


        val photo = getNewGroupPhoto(photoAlbum.getAlbumId()!!)
        val photos = photoDAO!!.selectPhotoByAlbumId(photo.profileId!!, photo.albumId, 0, 10)
        photo.photoAlbumName = photoAlbum.title

        photoDAO.insert(photo)

        val updatedUserPhotos = photoDAO.selectPhotoByAlbumId(photo.profileId!!, photo.albumId, 0, 10)
        assertNotNull(photoDAO.selectPhotoById(photo.profileId!!, photo.photoId))
        assertTrue(photos.size + 1 == updatedUserPhotos.size)

        photo.imageUrl = "http://tratatata"
        photo.description = "test, tratata, blahblah"
        photoDAO.update(photo)

        val updated = photoDAO.selectPhotoById(photo.profileId!!, photo.photoId)
        checkPhotos(photo, updated)

        //Photo's views count update
        val viewsCount = 5
        photoDAO.updateViewsCount(photo.profileId!!, photo.photoId, viewsCount)
        val viewsCountUpdatedPhoto = photoDAO.selectPhotoById(photo.profileId!!, photo.photoId)
        assertNotNull(viewsCountUpdatedPhoto)
        assertTrue(viewsCountUpdatedPhoto!!.viewsCount == photo.viewsCount + viewsCount)

        photoAlbum.viewPermission = PermissionLevel.EVERYBODY
        photoAlbumDAO!!.update(photoAlbum)

        //deleting
        photoDAO.delete(photo.profileId!!, photo.photoId)
        assertNull(photoDAO.selectPhotoById(photo.profileId!!, photo.photoId))
    }

    private fun checkPhotos(expected: Photo, actual: Photo?) {
        assertNotNull(actual)
        assertEquals(expected.photoId, actual!!.photoId)
        assertEquals(expected.profileId, actual.profileId)
        assertEquals(expected.albumId, actual.albumId)
        assertEquals(expected.authorProfileId, actual.authorProfileId)
        assertEquals(expected.viewsCount.toLong(), actual.viewsCount.toLong())
        assertEquals(expected.imageUrl, actual.imageUrl)
        assertEquals(expected.tagIds, actual.tagIds)
        assertEquals(expected.description, actual.description)
        assertEquals(expected.photoAlbumName, actual.photoAlbumName)
        assertNotNull(actual.timeAdded)
    }

    private fun createAlbum(): PhotoAlbum {
        val photoAlbum = PhotoAlbum()
        photoAlbum.setAlbumId(0)
        photoAlbum.profileId = profileId
        photoAlbum.authorProfileId = profileId
        photoAlbum.photosCount = 0
        photoAlbum.timeAdded = Date()
        photoAlbum.timeUpdated = Date()
        photoAlbum.viewPermission = PermissionLevel.EVERYBODY
        photoAlbum.viewsCount = 0
        photoAlbum.albumTypeId = 1
        photoAlbum.title = "Album title"
        return photoAlbum
    }

    private fun getNewGroupPhoto(albumId: Long): Photo {
        val photo = Photo()
        photo.albumId = albumId
        photo.profileId = profileId
        photo.authorProfileId = profileId
        photo.description = "test description"
        photo.imageUrl = "http://tratatata"
        photo.viewsCount = 0
        photo.timeAdded = Date()
        return photo
    }

    @Ignore
    @Test
    fun authorInfoTest() {
        val profile = TestHelper.newProfile()
        val photoAlbum = createAlbum()
        photoAlbum.authorProfileId = profile.profileId
        photoAlbum.profileId = profile.profileId
        photoAlbumDAO!!.insert(photoAlbum)
        val photo = getNewGroupPhoto(photoAlbum.getAlbumId()!!)
        photo.profileId = profile.profileId
        photo.authorProfileId = profile.profileId
        photoDAO!!.insert(photo)
        val inserted = photoDAO.selectPhotoById(photo.profileId!!, photo.photoId)
        assertNotNull(inserted)
        assertEquals("display name", inserted!!.authorName)
        assertEquals("display name", inserted.ownerName)
    }
}

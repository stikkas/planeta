package ru.planeta.dao.trashcan


import org.junit.Assert.*
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.transaction.annotation.Transactional
import ru.planeta.TestHelper
import ru.planeta.TestHelper.TEST_PROFILE_ID
import ru.planeta.model.trashcan.InteractiveEditorData
import java.util.*

@RunWith(SpringRunner::class)
@Transactional
@ContextConfiguration(locations = ["classpath:applicationContext-common-test.xml"])
class InteractiveEditorDataDAOImplTest {

    @Autowired
    lateinit var interactiveEditorDataDAO: InteractiveEditorDataDAO

    @Test
    @Throws(Exception::class)
    fun insertSelectUpdate() {
        val interactiveEditorData = InteractiveEditorData()

        val profileId = TEST_PROFILE_ID
        val userPrivateInfo = TestHelper.createUserPrivateInfo(profileId)
        val campaign = TestHelper.createCampaign(profileId)

        interactiveEditorData.userId = profileId
        interactiveEditorData.campaignId = campaign.campaignId
        interactiveEditorData.contractorTypePercent = 13
        interactiveEditorData.desiredCollectedAmountPercent = 10
        interactiveEditorData.reachedStepNum = 0
        interactiveEditorData.isAgreeStorePersonalData = true
        interactiveEditorData.isAgreeReceiveLessonsMaterialsOnEmail = true
        interactiveEditorData.formsSettings = "1111"
        interactiveEditorData.timeAdded = Date()

        interactiveEditorDataDAO!!.insert(interactiveEditorData)

        var selectedEditorData = interactiveEditorDataDAO.selectByUserId(profileId)
        assertNotNull(selectedEditorData)
        assertEquals(interactiveEditorData.userId, selectedEditorData.userId)
        assertEquals(interactiveEditorData.campaignId, selectedEditorData.campaignId)
        assertEquals(interactiveEditorData.desiredCollectedAmountPercent.toLong(), selectedEditorData.desiredCollectedAmountPercent.toLong())
        assertEquals(interactiveEditorData.reachedStepNum.toLong(), selectedEditorData.reachedStepNum.toLong())
        assertEquals(interactiveEditorData.isAgreeStorePersonalData, selectedEditorData.isAgreeStorePersonalData)
        assertEquals(interactiveEditorData.isAgreeReceiveLessonsMaterialsOnEmail, selectedEditorData.isAgreeReceiveLessonsMaterialsOnEmail)

        interactiveEditorData.contractorTypePercent = 6
        interactiveEditorData.desiredCollectedAmountPercent = 15
        interactiveEditorData.reachedStepNum = 4
        interactiveEditorData.isAgreeStorePersonalData = false
        interactiveEditorData.isAgreeReceiveLessonsMaterialsOnEmail = false
        interactiveEditorData.formsSettings = "2222"

        interactiveEditorDataDAO.update(interactiveEditorData)

        selectedEditorData = interactiveEditorDataDAO.selectByUserId(profileId)
        assertNotNull(selectedEditorData)
        assertEquals(interactiveEditorData.userId, selectedEditorData.userId)
        assertEquals(interactiveEditorData.campaignId, selectedEditorData.campaignId)
        assertEquals(interactiveEditorData.desiredCollectedAmountPercent.toLong(), selectedEditorData.desiredCollectedAmountPercent.toLong())
        assertEquals(interactiveEditorData.reachedStepNum.toLong(), selectedEditorData.reachedStepNum.toLong())
        assertEquals(interactiveEditorData.isAgreeStorePersonalData, selectedEditorData.isAgreeStorePersonalData)
        assertEquals(interactiveEditorData.isAgreeReceiveLessonsMaterialsOnEmail, selectedEditorData.isAgreeReceiveLessonsMaterialsOnEmail)

        interactiveEditorDataDAO.deleteByUserId(profileId)
        selectedEditorData = interactiveEditorDataDAO.selectByUserId(profileId)
        assertNull(selectedEditorData)

    }

}

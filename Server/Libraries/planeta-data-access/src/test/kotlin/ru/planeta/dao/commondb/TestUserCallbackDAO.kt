package ru.planeta.dao.commondb


import org.junit.Assert.*
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.transaction.annotation.Transactional
import ru.planeta.model.common.TopayTransaction
import ru.planeta.model.common.UserCallback
import ru.planeta.model.common.campaign.Share
import ru.planeta.model.enums.OrderObjectType
import ru.planeta.model.profile.Profile
import java.math.BigDecimal
import java.util.*

@RunWith(SpringRunner::class)
@Transactional
@ContextConfiguration(locations = ["classpath:applicationContext-common-test.xml"])
class TestUserCallbackDAO {
//
//    @Autowired
//    private val dao: UserCallbackDAO? = null
//
//    @Test
//    fun testCRUD() {
//        val testStartTime = Date()
//
//        val userId = getId(Profile::class.java)
//        val phone = "+76851128564"
//        val orderType = OrderObjectType.SHARE
//        val shareId = getId(Share::class.java)
//        val paymentId = getId(TopayTransaction::class.java)
//        val amount = BigDecimal.TEN
//        val managerId = getId(Profile::class.java)
//
//        val failurePayment = insertUserCallback(UserCallback.Type.FAILURE_PAYMENT, userId, phone, orderType, shareId, paymentId, amount)
//        val orderingProblem = insertUserCallback(UserCallback.Type.ORDERING_PROBLEM, userId, phone, orderType, shareId, paymentId, amount)
//
//        var callbacks = dao!!.selectUserCallbacks(UserCallback.Type.FAILURE_PAYMENT, false, testStartTime, null!!, 0, 0)
//        checkSingleCallbackListSelection(failurePayment, callbacks)
//        callbacks = dao.selectUserCallbacks(UserCallback.Type.ORDERING_PROBLEM, false, testStartTime, null!!, 0, 0)
//        checkSingleCallbackListSelection(orderingProblem, callbacks)
//
//        dao.updateProcessingStatus(failurePayment.userCallbackId, managerId)
//        dao.updateProcessingStatus(orderingProblem.userCallbackId, managerId)
//
//        var selected = dao.selectByOrderId(failurePayment.userCallbackId)
//        assertProcessed(selected)
//        selected = dao.selectByOrderId(orderingProblem.userCallbackId)
//        assertProcessed(selected)
//
//        val idList = Arrays.asList(failurePayment.userCallbackId, orderingProblem.userCallbackId)
//        callbacks = dao.selectUserCallbacksByIds(idList)
//        assertCallbackList(idList, callbacks)
//    }
//
//    private fun insertUserCallback(type: UserCallback.Type, userId: Long, phone: String, orderType: OrderObjectType, shareId: Long, paymentId: Long, amount: BigDecimal): UserCallback {
//        val id = dao!!.insert(createUserCallback(type, userId, phone, paymentId, shareId, orderType, amount))
//
//        val result = dao.selectByOrderId(id)
//        assertNotNull(result)
//        assertNotEquals(result.userCallbackId, EMPTY_ID)
//        assertNotNull(result.timeAdded)
//        assertUnprocessed(result)
//
//        return result
//    }
//
//    companion object {
//
//        private val EMPTY_ID = 0L
//
//        private fun checkSingleCallbackListSelection(expected: UserCallback, callbacks: List<UserCallback>) {
//            assertNotNull(callbacks)
//            assertFalse(callbacks.isEmpty())
//            assertUserCallbackEquals(expected, callbacks[0])
//        }
//
//        private fun createUserCallback(type: UserCallback.Type, userId: Long, phone: String, paymentId: Long, orderObjectId: Long, orderType: OrderObjectType, amount: BigDecimal): UserCallback {
//            val result = UserCallback()
//            result.type = type
//            result.paymentId = paymentId
//            result.orderType = orderType
//            result.orderObjectId = orderObjectId
//            result.userId = userId
//            result.userPhone = phone
//            result.amount = amount
//
//            return result
//        }
//
//        private fun assertUserCallbackEquals(expected: UserCallback, actual: UserCallback) {
//            assertNotNull(expected)
//            assertNotNull(actual)
//            assertEquals(expected.userCallbackId, actual.userCallbackId)
//            assertEquals(expected.userId, actual.userId)
//            assertEquals(expected.userPhone, actual.userPhone)
//            assertEquals(expected.orderObjectId, actual.orderObjectId)
//            assertEquals(expected.orderType, actual.orderType)
//            assertEquals(expected.paymentId, actual.paymentId)
//            assertBigDecimalEquals(expected.amount, actual.amount)
//            assertEquals(expected.isProcessed, actual.isProcessed)
//            assertEquals(expected.managerId, actual.managerId)
//            assertEquals(expected.timeAdded, actual.timeAdded)
//            assertEquals(expected.timeUpdated, actual.timeUpdated)
//        }
//
//        private fun checkProcessing(callback: UserCallback, isProcessed: Boolean) {
//            assertEquals(callback.isProcessed, isProcessed)
//            assertEquals(callback.managerId != EMPTY_ID, isProcessed)
//            assertEquals(callback.timeUpdated != null, isProcessed)
//        }
//
//        private fun assertProcessed(callback: UserCallback) {
//            checkProcessing(callback, true)
//        }
//
//        private fun assertUnprocessed(callback: UserCallback) {
//            checkProcessing(callback, false)
//        }
//
//        private fun assertCallbackList(expected: List<Long>, callbacks: List<UserCallback>) {
//            val callbackIds = ArrayList<Long>()
//            for (callback in callbacks) {
//                callbackIds.add(callback.userCallbackId)
//            }
//            assertArrayEquals(expected.toTypedArray(), callbackIds.toTypedArray())
//        }
//    }
}

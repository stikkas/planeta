package ru.planeta.dao.commondb

import org.junit.Test
import org.springframework.beans.factory.annotation.Autowired
import ru.planeta.model.common.Configuration

import org.junit.Assert.*
import org.junit.runner.RunWith
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.transaction.annotation.Transactional

/**
 * @author ds.kolyshev
 * Date: 17.05.12
 */

@RunWith(SpringRunner::class)
@Transactional
@ContextConfiguration(locations = ["classpath:applicationContext-common-test.xml"])
class TestConfigurationDAO {

    @Autowired
    lateinit var configurationDAO: ConfigurationDAO

    @Test
    fun testSelectUpdateDelete() {
        val configuration = Configuration()
        configuration.key = "planeta.test.key"
        configuration.intValue = 123
        configuration.stringValue = "test string value"

        configurationDAO.insert(configuration)

        var selected = configurationDAO.select(configuration.key!!)
        assertConfigurationsEquals(configuration, selected)

        val list = configurationDAO.selectList(null, 0, 1)
        assertEquals(1, list.size.toLong())

        configuration.booleanValue = true
        configuration.intValue = configuration.intValue + 1
        configuration.stringValue = null

        configurationDAO.update(configuration)
        selected = configurationDAO.select(configuration.key!!)
        assertConfigurationsEquals(configuration, selected)

        configurationDAO.delete(configuration.key!!)
        selected = configurationDAO.select(configuration.key!!)
        assertNull(selected)


    }

    private fun assertConfigurationsEquals(expected: Configuration, actual: Configuration?) {
        assertNotNull(actual)
        assertEquals(expected.key, actual?.key)
        assertEquals(expected.stringValue, actual?.stringValue)
        assertEquals(expected.intValue.toLong(), actual?.intValue?.toLong())
        assertEquals(expected.booleanValue, actual?.booleanValue)
    }
}

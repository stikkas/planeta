package ru.planeta.repository.promodb

import org.junit.Ignore
import org.junit.Test
import org.springframework.beans.factory.annotation.Autowired
import ru.planeta.dao.promo.TechnobattleProjectDAO
import ru.planeta.model.promo.TechnobattleProject

import org.junit.Assert.*
import org.junit.runner.RunWith
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.transaction.annotation.Transactional

/**
 * Created with IntelliJ IDEA.
 * User: michail
 * Date: 16.01.2017
 * Time: 18:59
 */

@Ignore
@RunWith(SpringRunner::class)
@Transactional
@ContextConfiguration(locations = ["classpath:applicationContext-common-test.xml"])
class TechnobattleProjectDAOTest {
    @Autowired
    lateinit var technobattleProjectDAO: TechnobattleProjectDAO

    @Test
    fun testSelect() {
        val list = technobattleProjectDAO!!.selectList(0, 10)
        assertNotNull(list)
        val project = technobattleProjectDAO.selectOne(1)
        assertNotNull(project)
        assertTrue(project.profileId > 0)
    }
}

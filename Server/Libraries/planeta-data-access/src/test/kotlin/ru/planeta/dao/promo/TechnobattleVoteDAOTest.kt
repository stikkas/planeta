package ru.planeta.repository.promodb

import junit.framework.Assert.assertEquals
import org.apache.ibatis.exceptions.PersistenceException
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.transaction.annotation.Transactional
import ru.planeta.dao.promo.TechnobattleVoterDAO
import ru.planeta.model.promo.TechnobattleVote
import ru.planeta.model.promo.VoteType


/**
 * Created with IntelliJ IDEA.
 * User: michail
 * Date: 16.01.2017
 * Time: 17:12
 */
@RunWith(SpringRunner::class)
@Transactional
@ContextConfiguration(locations = ["classpath:applicationContext-common-test.xml"])
class TechnobattleVoteDAOTest {

    @Autowired
    lateinit var technobattleVoterDAO: TechnobattleVoterDAO

    @Test
    fun testInsertAndSelect() {
        assertEquals(0, technobattleVoterDAO.selectVotesCountForUser("123", VoteType.VK))

        technobattleVoterDAO.insert(TechnobattleVote("123", -1, VoteType.VK))
        assertEquals(1, technobattleVoterDAO.selectVotesCountForUser("123", VoteType.VK))

        assertEquals(0, technobattleVoterDAO.selectVotesCountForUser("123", VoteType.FB))
    }

    @Test(expected = PersistenceException::class)
    fun testUnique() {
        technobattleVoterDAO.insert(TechnobattleVote("123", -1, VoteType.VK))
        technobattleVoterDAO.insert(TechnobattleVote("123", -1, VoteType.VK))
    }

    @Test
    fun testVotesCount() {
        technobattleVoterDAO.insert(TechnobattleVote("123", -1, VoteType.VK))
        technobattleVoterDAO.insert(TechnobattleVote("456", -1, VoteType.VK))
        technobattleVoterDAO.insert(TechnobattleVote("789", -1, VoteType.VK))
        technobattleVoterDAO.insert(TechnobattleVote("123", -1, VoteType.FB))

        assertEquals(4, technobattleVoterDAO.selectVotesCountForProject(-1))
    }
}


package ru.planeta.dao.statdb

import org.junit.Assert.*
import org.junit.Ignore
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.transaction.annotation.Transactional
import ru.planeta.TestHelper.TEST_PROFILE_ID
import ru.planeta.model.stat.CampaignAfterPurchaseMailStat

// TODO none xml files
@Ignore
@RunWith(SpringRunner::class)
@Transactional
@ContextConfiguration(locations = ["classpath:applicationContext-common-test.xml"])
class CampaignAfterPurchaseMailStatDAOImplTest {

    @Autowired
    lateinit var campaignAfterPurchaseMailStatDAO: CampaignAfterPurchaseMailStatDAO

    @Test
    fun insertSelectUpdate() {
        val userId = TEST_PROFILE_ID

        val campaignAfterPurchaseMailStat = CampaignAfterPurchaseMailStat()
        campaignAfterPurchaseMailStat.userId = userId
        campaignAfterPurchaseMailStat.orderId = 123
        campaignAfterPurchaseMailStat.mailSent = false

        campaignAfterPurchaseMailStatDAO!!.insert(campaignAfterPurchaseMailStat)
        val selectedCampaignAfterPurchaseMailStat = campaignAfterPurchaseMailStatDAO!!.selectByUserId(campaignAfterPurchaseMailStat.userId)
        assertNotNull(selectedCampaignAfterPurchaseMailStat)
        assertEquals(campaignAfterPurchaseMailStat.feedbackLetterId, selectedCampaignAfterPurchaseMailStat.feedbackLetterId)
        assertEquals(campaignAfterPurchaseMailStat.userId, selectedCampaignAfterPurchaseMailStat.userId)
        assertNotNull(selectedCampaignAfterPurchaseMailStat.timeAdded)
        assertEquals(campaignAfterPurchaseMailStat.mailSent, selectedCampaignAfterPurchaseMailStat.mailSent)
        assertTrue(campaignAfterPurchaseMailStat.orderId == selectedCampaignAfterPurchaseMailStat.orderId)
        assertNull(selectedCampaignAfterPurchaseMailStat.timeUpdated)

        selectedCampaignAfterPurchaseMailStat.mailSent = true
        campaignAfterPurchaseMailStatDAO!!.update(selectedCampaignAfterPurchaseMailStat)

        val selectedCampaignAfterPurchaseMailStat2 = campaignAfterPurchaseMailStatDAO!!.selectByUserId(selectedCampaignAfterPurchaseMailStat.userId)
        assertTrue(selectedCampaignAfterPurchaseMailStat.mailSent === selectedCampaignAfterPurchaseMailStat2.mailSent)
        assertNotNull(selectedCampaignAfterPurchaseMailStat2.timeUpdated)
    }
}

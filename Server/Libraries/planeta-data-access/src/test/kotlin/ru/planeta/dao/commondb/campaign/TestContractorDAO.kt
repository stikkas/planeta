package ru.planeta.dao.commondb.campaign

import org.junit.Test
import org.springframework.beans.factory.annotation.Autowired
import ru.planeta.model.common.Contractor
import ru.planeta.model.enums.ContractorPositionType


import java.util.Date

import org.junit.Assert.*
import org.junit.runner.RunWith
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.transaction.annotation.Transactional
import ru.planeta.TestHelper
import ru.planeta.TestHelper.TEST_PROFILE_ID
import ru.planeta.dao.commondb.ContractorDAO

@RunWith(SpringRunner::class)
@Transactional
@ContextConfiguration(locations = ["classpath:applicationContext-common-test.xml"])
class TestContractorDAO {

    @Autowired
    lateinit var contractorDAO: ContractorDAO

    @Test
    fun test() {
        val testProfileId: Long = 1000000000

        val contractor = Contractor()
        contractor.contractorId = testProfileId
        contractor.name = "Petrov"
        contractor.contractNumber = "100 500"
        contractor.timeAdded = Date()
        contractor.position = ContractorPositionType.GEN_DIRECTOR

        contractorDAO!!.insert(contractor)
        assertTrue(contractor.contractorId > 0)

        val selectedList = contractorDAO.selectList(0, 10)
        assertTrue(selectedList.size > 0)

        val selected = contractorDAO.select(contractor.contractorId)
        assertCreditTransactionEquals(selected, contractor)
        assertEquals(selected.position, ContractorPositionType.GEN_DIRECTOR)

        contractor.contractNumber = "100 600"
        contractor.position = ContractorPositionType.PRESIDENT
        contractorDAO.update(contractor)

        val updated = contractorDAO.select(contractor.contractorId)
        assertCreditTransactionEquals(updated, contractor)
        assertEquals(updated.position, ContractorPositionType.PRESIDENT)

        contractorDAO.delete(contractor.contractorId)
        assertNull(contractorDAO.select(contractor.contractorId))
    }

    @Test
    fun testSelectByInn() {
        val testProfileId: Long = 1000000000

        val contractor = Contractor()
        contractor.contractorId = testProfileId
        contractor.name = "Petrov"
        contractor.contractNumber = "100 500"
        contractor.timeAdded = Date()
        contractor.inn = "HanFfOskXc5"

        contractorDAO!!.insert(contractor)
        assertTrue(contractor.contractorId > 0)

        val testContractor = contractorDAO.selectByInn("HanFfOskXc5")
        assertNotNull(testContractor)
    }

    private fun assertCreditTransactionEquals(expected: Contractor, actual: Contractor) {
        assertNotNull(expected)
        assertEquals(expected.contractorId, actual.contractorId)
        assertEquals(expected.contractNumber, actual.contractNumber)
        assertEquals(expected.timeAdded, actual.timeAdded)
        assertEquals(expected.inn, actual.inn)
    }

    @Test
    fun testSearch() {
        val testContractorId: Long = 1000000001
        val contractor = Contractor()
        contractor.contractorId = testContractorId
        contractor.name = "Gosha Karetnyj"
        contractor.inn = "322335584"
        contractor.contractNumber = "86756675647"
        contractor.timeAdded = Date()

        contractorDAO!!.insert(contractor)
        val selected = contractorDAO.select(contractor.contractorId)
        assertCreditTransactionEquals(selected, contractor)

        val testProfileId = TEST_PROFILE_ID
        val campaign = TestHelper.createCampaign(testProfileId)
        contractorDAO.insertRelation(contractor.contractorId, campaign.campaignId)

        var foundContractors = contractorDAO.selectContractorsSearch("Gosha", 0, 0)
        assertEquals(1, foundContractors.size.toLong())
        assertEquals(1, foundContractors.totalCount.toLong())
        assertCreditTransactionEquals(foundContractors[0], contractor)

        foundContractors = contractorDAO.selectContractorsSearch(contractor.inn!!, 0, 10)
        assertEquals(1, foundContractors.size.toLong())
        assertEquals(1, foundContractors.totalCount.toLong())

        foundContractors = contractorDAO.selectContractorsSearch(campaign.name!!, 0, 10)
        assertEquals(1, foundContractors.size.toLong())
        assertEquals(1, foundContractors.totalCount.toLong())
    }
}

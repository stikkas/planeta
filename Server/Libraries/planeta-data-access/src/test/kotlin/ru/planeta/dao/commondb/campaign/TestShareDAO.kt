package ru.planeta.dao.commondb.campaign


import org.junit.Assert.*
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.transaction.annotation.Transactional
import ru.planeta.TestHelper
import ru.planeta.TestHelper.TEST_PROFILE_ID
import ru.planeta.dao.commondb.ShareDAO
import ru.planeta.model.common.campaign.Share
import java.math.BigDecimal

/**
 * Created by IntelliJ IDEA.
 * User: atropnikov
 * Date: 21.03.12
 * Time: 14:38
 */
@RunWith(SpringRunner::class)
@Transactional
@ContextConfiguration(locations = ["classpath:applicationContext-common-test.xml"])
class TestShareDAO {

    @Autowired
    lateinit var shareDAO: ShareDAO

    @Test
    fun testInsertUpdate() {


        val campaignId = TestHelper.createCampaign(TEST_PROFILE_ID).campaignId

        val share = TestHelper.createShare(TEST_PROFILE_ID, campaignId)
        assertTrue(share.shareId > 0)

        share.name = "new name"
        share.description = "new desc"
        share.imageId = 100000
        share.imageUrl = "new image url"
        share.price = BigDecimal.TEN
        share.amount = 12
        share.purchaseCount = 5
        shareDAO.update(share)

        val selected = shareDAO.select(share.shareId)
        assertShareEquals(selected, share)

        val shares = shareDAO.selectCampaignShares(share.campaignId!!, 0, 0)
        assertTrue(shares.size == 1)
        assertShareEquals(shares[0], share)

        shareDAO.delete(share.shareId)
        assertNull(shareDAO.select(share.shareId))

        shareDAO.insert(share)
        shareDAO.deleteCampaignShares(share.campaignId!!)
        assertNull(shareDAO.select(share.shareId))

    }

    private fun assertShareEquals(expected: Share?, actual: Share) {
        assertNotNull(expected)
        assertEquals(expected!!.shareId, actual.shareId)
        assertEquals(expected.campaignId, actual.campaignId)
        assertEquals(expected.profileId, actual.profileId)
        assertEquals(expected.name, actual.name)
        assertEquals(expected.description, actual.description)
        assertEquals(expected.imageId, actual.imageId)
        assertEquals(expected.imageUrl, actual.imageUrl)
        assertEquals(expected.price!!.compareTo(actual.price!!).toLong(), 0)
        assertEquals(expected.amount.toLong(), actual.amount.toLong())
        assertEquals(expected.timeAdded, actual.timeAdded)
        assertEquals(expected.shareStatus, actual.shareStatus)
    }
}


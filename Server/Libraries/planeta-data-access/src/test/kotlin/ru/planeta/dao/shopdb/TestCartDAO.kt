package ru.planeta.dao.shopdb


import org.junit.Assert.*
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.transaction.annotation.Transactional
import ru.planeta.TestHelper
import ru.planeta.TestHelper.TEST_PROFILE_ID
import ru.planeta.TestHelper.createActiveSimpleProduct
import ru.planeta.TestHelper.random

/**
 * @author Andrew Arefyev
 * Date: 03.12.12
 * Time: 19:17
 */
@RunWith(SpringRunner::class)
@Transactional
@ContextConfiguration(locations = ["classpath:applicationContext-common-test.xml"])
class TestCartDAO {
    @Autowired
    lateinit var cartDAO: CartDAO

    @Test
    @Throws(Exception::class)
    fun testInsertSelect() {

        val quantity1 = TestHelper.random(1000) as Int
        val productId1 = TestHelper.createActiveSimpleProduct(Long.MAX_VALUE, quantity1)

        val quantity2 = TestHelper.random(1000) as Int
        val productId2 = TestHelper.createActiveSimpleProduct(Long.MAX_VALUE)
        //=========================================

        val checkNull = cartDAO.select(TEST_PROFILE_ID, productId1)
        assertNull(checkNull)
        //insert
        try {
            cartDAO.insert(TEST_PROFILE_ID, productId1, quantity1)
        } catch (e: Exception) {
            fail()
        }

        //selectCampaignById count
        val checkQuantityEqual = cartDAO.select(TEST_PROFILE_ID, productId1)
        assertEquals(quantity1.toLong(), checkQuantityEqual?.quantity?.toLong())

        //selectCampaignById records
        val oneProducts = cartDAO.select(TEST_PROFILE_ID)
        assertNotNull(oneProducts)
        assertEquals(1, oneProducts.size.toLong())

        cartDAO.insert(TEST_PROFILE_ID, productId2, quantity2)
        val severalProducts = cartDAO.select(TEST_PROFILE_ID)
        assertNotNull(severalProducts)
        assertEquals(2, severalProducts.size.toLong())

        val profileIds = cartDAO.selectCartOwnersIds()
        assertTrue(profileIds.contains(TEST_PROFILE_ID))
    }

    @Test
    @Throws(Exception::class)
    fun testUpdate() {
        val buyerId = TEST_PROFILE_ID

        val quantity11 = random(1000) as Int
        val quantity12 = quantity11 + random(1000) as Int
        val productId1 = createActiveSimpleProduct(TEST_PROFILE_ID, quantity12)

        val quantity2 = random(1000) as Int
        val productId2 = createActiveSimpleProduct(TEST_PROFILE_ID, quantity2 + quantity12)
        //================================================
        cartDAO.insert(buyerId, productId1, quantity11)
        cartDAO.insert(buyerId, productId2, quantity2)
        //-----------------------------
        val rowsUpdatedCount = cartDAO.update(buyerId, productId1, quantity12)
        assertEquals(1, rowsUpdatedCount.toLong())
        val checkUpdatedQuantity = cartDAO.select(buyerId, productId1)
        assertNotNull(checkUpdatedQuantity)
        assertEquals(quantity12.toLong(), checkUpdatedQuantity?.quantity?.toLong())
        val checkRemainsTheSame = cartDAO.select(buyerId, productId2)
        assertNotNull(checkRemainsTheSame)
        assertEquals(quantity2.toLong(), checkRemainsTheSame?.quantity?.toLong())
    }

    @Test
    @Throws(Exception::class)
    fun testDeleteItem() {
        val buyerId = TEST_PROFILE_ID

        val productDieSecondId = createActiveSimpleProduct(TEST_PROFILE_ID, random(1000) as Int)

        val mcLaudQuantity = random(1000) as Int
        val mcLaudProductId = createActiveSimpleProduct(TEST_PROFILE_ID, mcLaudQuantity)

        val productDieFirstId = createActiveSimpleProduct(TEST_PROFILE_ID, random(1000) as Int)

        cartDAO.insert(buyerId, productDieSecondId, random(1000) as Int)
        cartDAO.insert(buyerId, mcLaudProductId, mcLaudQuantity)
        cartDAO.insert(buyerId, productDieFirstId, random(1000) as Int)

        var deletedRowsCount = cartDAO.delete(buyerId, productDieFirstId)
        assertEquals(1, deletedRowsCount.toLong())
        val itemsInCart = cartDAO.select(buyerId)
        assertEquals(2, itemsInCart.size.toLong())

        deletedRowsCount = cartDAO.delete(buyerId, productDieSecondId)
        assertEquals(1, deletedRowsCount.toLong())
        val itemsInFinalCart = cartDAO.select(buyerId)
        assertEquals(1, itemsInFinalCart.size.toLong())

        val quantityRemained = cartDAO.select(buyerId, mcLaudProductId)
        assertNotNull(quantityRemained)
        assertEquals(mcLaudQuantity.toLong(), quantityRemained?.quantity?.toLong())

        val zeroQuantity = cartDAO.select(buyerId, productDieFirstId)
        assertNull(zeroQuantity)
    }

    @Test
    @Throws(Exception::class)
    fun testDeleteCart() {
        val kennyId = TEST_PROFILE_ID
        val mcLaudId = TEST_PROFILE_ID
        val storeId = TEST_PROFILE_ID

        val kennyProductId1 = createActiveSimpleProduct(storeId, random(1000) as Int)

        val productId2 = createActiveSimpleProduct(storeId, random(1000) as Int)

        val productId3 = createActiveSimpleProduct(storeId, random(1000) as Int)

        cartDAO.insert(kennyId, kennyProductId1, random(1000) as Int)
        cartDAO.insert(kennyId, productId2, random(1000) as Int)
        cartDAO.insert(kennyId, productId3, random(1000) as Int)

        cartDAO.insert(mcLaudId, productId2, random(1000) as Int)
        cartDAO.insert(mcLaudId, productId3, random(1000) as Int)

        //kill Kenny cart
        val deletedRowsCount = cartDAO.clearCart(kennyId)
        assertEquals(3, deletedRowsCount.toLong())
        val itemsInKennyCart = cartDAO.select(kennyId)
        assertEquals(0, itemsInKennyCart.size.toLong())
        //McLaud cart remains the same
        val itemsInMcLaudCart = cartDAO.select(mcLaudId)
        assertEquals(2, itemsInMcLaudCart.size.toLong())

        val profileIds = cartDAO.selectCartOwnersIds()
        assertFalse(profileIds.contains(kennyId))
    }

    @Test
    @Throws(Exception::class)
    fun testDeleteProductFromCarts() {
        val samId = TEST_PROFILE_ID
        val rodneyId = TEST_PROFILE_ID
        val storeId = TEST_PROFILE_ID

        val kennyProductId = TestHelper.createActiveSimpleProduct(storeId, TestHelper.random(1000) as Int)

        val productId2 = TestHelper.createActiveSimpleProduct(storeId, TestHelper.random(1000) as Int)

        val productId3 = TestHelper.createActiveSimpleProduct(storeId, TestHelper.random(1000) as Int)

        cartDAO.insert(samId, kennyProductId, TestHelper.random(1000) as Int)
        cartDAO.insert(samId, productId2, TestHelper.random(1000) as Int)
        cartDAO.insert(samId, productId3, TestHelper.random(1000) as Int)

        cartDAO.insert(rodneyId, productId2, TestHelper.random(1000) as Int)
        cartDAO.insert(rodneyId, productId3, TestHelper.random(1000) as Int)

        val deletedRowsCount = cartDAO.deleteProduct(kennyProductId)
        assertEquals(1, deletedRowsCount.toLong())
        val itemsInSamCart = cartDAO.select(samId)
        assertEquals(2, itemsInSamCart.size.toLong())
        //McLaud cart remains the same
        val itemsInRodneyCart = cartDAO.select(rodneyId)
        assertEquals(2, itemsInRodneyCart.size.toLong())
    }


}

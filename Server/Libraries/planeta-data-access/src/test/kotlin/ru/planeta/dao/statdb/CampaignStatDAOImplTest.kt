package ru.planeta.dao.statdb


import org.junit.Ignore
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.transaction.annotation.Transactional
import ru.planeta.commons.lang.DateUtils
import java.util.*

@RunWith(SpringRunner::class)
@Transactional
@ContextConfiguration(locations = ["classpath:applicationContext-common-test.xml"])
class CampaignStatDAOImplTest {
    @Autowired
    lateinit var campaignStatDAO: CampaignStatDAO

    @Ignore
    @Test
    fun selectGeneralTotalStats() {
        val campaignId: Long = 61750

        val c = Calendar.getInstance()
        c.time = Date()
        c.add(Calendar.DATE, -1)

        val yesterdayBegin = DateUtils.getStartOfDay(c.time)
        val yesterdayEnd = DateUtils.getEndOfDate(c.time)

        val campaignStat = campaignStatDAO.selectGeneralTotalStats(campaignId, yesterdayBegin, yesterdayEnd)
        println("done.")
    }
}

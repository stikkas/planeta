package ru.planeta.dao.profiledb

import org.junit.Test
import org.springframework.beans.factory.annotation.Autowired
import ru.planeta.model.enums.ObjectType
import ru.planeta.model.enums.PermissionLevel
import ru.planeta.model.profile.Comment
import ru.planeta.model.profile.CommentObject


import java.util.Date

import org.junit.Assert.*
import org.junit.runner.RunWith
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.transaction.annotation.Transactional
import ru.planeta.TestHelper.TEST_PROFILE_ID

/**
 * @author a.savanovich
 */
@RunWith(SpringRunner::class)
@Transactional
@ContextConfiguration(locations = ["classpath:applicationContext-common-test.xml"])
class TestCommentObjectDAO {

    @Autowired
    lateinit var commentObjectDAO: CommentObjectDAO

    @Test
    @Throws(Exception::class)
    fun testCommentObjectDAO() {


        // Creating new commentable object
        val commentObject = createCommentObject(TEST_PROFILE_ID)
        commentObjectDAO!!.insert(commentObject)
        var selected = commentObjectDAO.select(commentObject)
        check(commentObject, selected)

        commentObject.commentsPermission = PermissionLevel.ADMINS
        commentObject.commentsCount = 400
        commentObjectDAO.update(commentObject)

        selected = commentObjectDAO.select(commentObject)
        check(commentObject, selected)

        val commentCount = 5
        commentObjectDAO.addCommentsCount(commentObject, commentCount)
        selected = commentObjectDAO.select(commentObject)
        assertTrue(selected.commentsCount == commentObject.commentsCount + commentCount)

        commentObjectDAO.delete(commentObject.profileId!!, commentObject.objectType!!, commentObject.objectId!!)
        selected = commentObjectDAO.select(commentObject)
        assertNull(selected)


    }

    private fun check(first: CommentObject, second: CommentObject) {
        assertEquals(first.objectId, second.objectId)
        assertEquals(first.objectType, second.objectType)
        assertEquals(first.profileId, second.profileId)
        assertEquals(first.commentsCount.toLong(), second.commentsCount.toLong())
        assertEquals(first.commentsPermission, second.commentsPermission)
    }

    private fun createCommentObject(ownerId: Long): CommentObject {
        // Creating new commentObject
        val commentObject = CommentObject()
        commentObject.profileId = ownerId
        commentObject.objectType = ObjectType.PHOTO
        return commentObject
    }

    private fun createComment(commentObject: CommentObject, timeAdded: Date): Comment {
        val comment = Comment()
        comment.profileId = commentObject.profileId
        comment.objectId = commentObject.objectId
        comment.parentCommentId = 0
        comment.objectType = commentObject.objectType
        comment.level = 0
        comment.authorProfileId = commentObject.profileId!!
        comment.timeAdded = timeAdded
        comment.text = "123"
        comment.textHtml = "123"
        return comment
    }
}

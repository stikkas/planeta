package ru.planeta.dao.commondb

import org.junit.Test
import org.springframework.beans.factory.annotation.Autowired
import ru.planeta.model.common.DeliveryAddress


import org.junit.Assert.assertNotNull
import org.junit.Assert.assertTrue
import org.junit.runner.RunWith
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.transaction.annotation.Transactional

@RunWith(SpringRunner::class)
@Transactional
@ContextConfiguration(locations = ["classpath:applicationContext-common-test.xml"])
class TestDeliveryAddressDAO {

    @Autowired
    lateinit var dao: DeliveryAddressDAO

    @Test
    fun testCRUD() {
        val address = DeliveryAddress()
        address.address = "address"
        address.city = "city"
        address.country = "country"
        address.orderId = 1L
        address.fio = "fio"
        address.phone = "phone"
        address.zipCode = "zipCode"

        val inserted = dao.insertOrUpdate(address)
        assertNotNull(inserted)
        assertTrue(inserted.id > 0)
    }
}

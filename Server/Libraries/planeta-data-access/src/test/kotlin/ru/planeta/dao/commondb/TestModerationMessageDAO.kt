package ru.planeta.dao.commondb


import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.Ignore
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.transaction.annotation.Transactional
import ru.planeta.TestHelper
import ru.planeta.model.common.ModerationMessage
import ru.planeta.model.common.campaign.enums.CampaignStatus
import ru.planeta.model.enums.ModerationMessageStatus

/**
 * Date: 22.10.13
 * Time: 20:14
 */
@RunWith(SpringRunner::class)
@Transactional
@ContextConfiguration(locations = ["classpath:applicationContext-common-test.xml"])
class TestModerationMessageDAO {

    @Autowired
    lateinit var moderationMessageDAO: ModerationMessageDAO

    @Autowired
    lateinit var campaignDAO: CampaignDAO

    @Test
    @Throws(Exception::class)
    fun testInsertSelect() {
        val user = TestHelper.createUser(TestHelper.TEST_PROFILE_ID)
        val moderator = TestHelper.createUser(TestHelper.TEST_PROFILE_ID + 1)

        val campaign = TestHelper.createCampaign(TestHelper.TEST_PROFILE_ID)
        campaignDAO.insert(campaign)
        val campaignId = campaign.campaignId
        assertTrue(campaignId > 0)

        val message = "Ja podumaju ob etom. Ok."

        val moderationMessage = ModerationMessage()
        moderationMessage.campaignId = campaignId
        moderationMessage.senderId = TestHelper.TEST_PROFILE_ID + 1
        moderationMessage.message = message
        moderationMessage.status = ModerationMessageStatus.MODERATOR_MESSAGE

        moderationMessageDAO!!.insertModerationMessage(moderationMessage)
        val lastMessageText = "Last moderation message"

        moderationMessage.message = lastMessageText
        moderationMessageDAO.insertModerationMessage(moderationMessage)

        val lastModerationMessage = moderationMessageDAO.selectLastCampaignModerationMessage(campaignId)
        assertEquals(lastModerationMessage.message, lastMessageText)
        assertEquals(lastModerationMessage.campaignId, campaignId)
        assertEquals(lastModerationMessage.senderId, TestHelper.TEST_PROFILE_ID + 1)
        assertEquals(lastModerationMessage.status, ModerationMessageStatus.MODERATOR_MESSAGE)

        val moderationMessageList = moderationMessageDAO.selectAllCampaignModerationMessages(campaignId, 0, 0)
        assertEquals(2, moderationMessageList.size.toLong())
    }

    @Test
    @Ignore
    @Throws(Exception::class)
    fun testSelectLastCampaignModerationMessageList() {

//        val testProfileId = registerUser().getProfileId()
//        val moderatorProfileId = registerUser().getProfileId()
//
//        val campaign1 = insertCampaign(testProfileId)
//        val msg11 = createModerationMessage(campaign1.campaignId, moderatorProfileId, CampaignStatus.DECLINED)
//        val msg12 = createModerationMessage(campaign1.campaignId, moderatorProfileId, CampaignStatus.ACTIVE)
//        val campaign2 = insertCampaign(testProfileId)
//        val msg21 = createModerationMessage(campaign2.campaignId, moderatorProfileId, CampaignStatus.ACTIVE)
//        val msg22 = createModerationMessage(campaign2.campaignId, moderatorProfileId, CampaignStatus.DECLINED)
//        val campaign3 = insertCampaign(testProfileId)
//        val msg31 = createModerationMessage(campaign3.campaignId, moderatorProfileId, CampaignStatus.PATCH)
//        val msg32 = createModerationMessage(campaign3.campaignId, moderatorProfileId, CampaignStatus.PAUSED)
//        val campaign4 = insertCampaign(testProfileId)
//        val msg41 = createModerationMessage(campaign4.campaignId, moderatorProfileId, CampaignStatus.PAUSED)
//        val msg42 = createModerationMessage(campaign4.campaignId, moderatorProfileId, CampaignStatus.PATCH)
//
//
//        var list = moderationMessageDAO!!.selectLastCampaignModerationMessageList(testProfileId, EnumSet.of(CampaignStatus.PATCH, CampaignStatus.DECLINED))
//        assertEquals(2, list.size.toLong())
//        assertEquals(msg42.message, list[0]["message"])
//        assertEquals(msg22.message, list[1]["message"])
//
//        list = moderationMessageDAO.selectLastCampaignModerationMessageList(testProfileId, EnumSet.of(CampaignStatus.ACTIVE, CampaignStatus.PAUSED))
//        assertEquals(2, list.size.toLong())
//        assertEquals(msg32.message, list[0]["message"])
//        assertEquals(msg12.message, list[1]["message"])

    }

    private fun createModerationMessage(campaignId: Long, moderatorProfileId: Long, campaignStatus: CampaignStatus): ModerationMessage {
        val moderationMessage = ModerationMessage()
        moderationMessage.campaignId = campaignId
        moderationMessage.senderId = moderatorProfileId
        moderationMessage.message = "message" + Math.random()
        moderationMessage.campaignStatus = campaignStatus
        moderationMessage.status = ModerationMessageStatus.MODERATOR_MESSAGE
        moderationMessageDAO.insertModerationMessage(moderationMessage)
        return moderationMessage
    }
}

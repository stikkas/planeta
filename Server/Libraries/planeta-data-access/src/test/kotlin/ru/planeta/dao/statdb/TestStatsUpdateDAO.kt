package ru.planeta.dao.statdb

import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.transaction.annotation.Transactional


/**
 * User: a.savanovich
 * Date: 04.04.12
 * Time: 17:03
 */
@RunWith(SpringRunner::class)
@Transactional
@ContextConfiguration(locations = ["classpath:applicationContext-common-test.xml"])
class TestStatsUpdateDAO {
    @Autowired
    lateinit var statsUpdateDAO: StatsUpdateDAO

    @Test
    fun testUpdateComments() {
        statsUpdateDAO!!.updateCommentsStats()
    }
}

package ru.planeta.dao.profiledb

import org.junit.Test
import org.springframework.beans.factory.annotation.Autowired
import ru.planeta.model.profile.broadcast.BroadcastSubscription


import org.junit.Assert.assertEquals
import org.junit.Assert.assertNotNull
import org.junit.runner.RunWith
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.transaction.annotation.Transactional

/**
 * @author: ds.kolyshev
 * Date: 15.05.13
 */
@RunWith(SpringRunner::class)
@Transactional
@ContextConfiguration(locations = ["classpath:applicationContext-common-test.xml"])
class TestBroadcastSubscriptionDAO {

    @Autowired
    lateinit var broadcastSubscriptionDAO: BroadcastSubscriptionDAO

    @Test
    fun testBroadcastSubscriptionDAO() {

        val broadcastSubscription = BroadcastSubscription()
        broadcastSubscription.profileId = 1000099991
        broadcastSubscription.broadcastId = 1000099992
        broadcastSubscription.subscriberProfileId = 1000099993
        broadcastSubscription.isOnSite = true

        broadcastSubscriptionDAO!!.insert(broadcastSubscription)

        var list = broadcastSubscriptionDAO.selectByBroadcastId(broadcastSubscription.profileId!!, broadcastSubscription.broadcastId, 0, 0)
        assertNotNull(list)
        assertEquals(1, list.size.toLong())
        var selected = list[0]
        assertBroadcastSubscriptions(broadcastSubscription, selected)

        selected = broadcastSubscriptionDAO.selectByBroadcastSubscriberId(broadcastSubscription.profileId!!, broadcastSubscription.broadcastId, broadcastSubscription.subscriberProfileId)
        assertBroadcastSubscriptions(broadcastSubscription, selected)

        broadcastSubscriptionDAO.delete(broadcastSubscription.profileId!!, broadcastSubscription.broadcastId, broadcastSubscription.subscriberProfileId)
        list = broadcastSubscriptionDAO.selectByBroadcastId(broadcastSubscription.profileId!!, broadcastSubscription.broadcastId, 0, 0)
        assertNotNull(list)
        assertEquals(0, list.size.toLong())
    }

    private fun assertBroadcastSubscriptions(broadcastSubscription: BroadcastSubscription, selected: BroadcastSubscription) {
        assertEquals(selected.subscriberProfileId, broadcastSubscription.subscriberProfileId)
        assertEquals(selected.profileId, broadcastSubscription.profileId)
        assertEquals(selected.broadcastId, broadcastSubscription.broadcastId)
        assertEquals(selected.isOnEmail, broadcastSubscription.isOnEmail)
        assertEquals(selected.isOnSite, broadcastSubscription.isOnSite)
    }
}


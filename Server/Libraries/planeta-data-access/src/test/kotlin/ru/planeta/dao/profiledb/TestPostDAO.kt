package ru.planeta.dao.profiledb

import org.junit.Ignore
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.transaction.annotation.Transactional

@RunWith(SpringRunner::class)
@Transactional
@ContextConfiguration(locations = ["classpath:applicationContext-common-test.xml"])
class TestPostDAO {
    @Autowired
    lateinit var postDAO: PostDAO

    @Ignore
    @Test
    fun testPostDAO() {
        val posts = postDAO.selectPosts(0, 0, 10)
        println(posts.size)
        val count = postDAO.selectPostsCount()
        println(count)
    }
}

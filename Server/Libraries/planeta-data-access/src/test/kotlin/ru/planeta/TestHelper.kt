package ru.planeta

import org.apache.commons.io.FileUtils
import org.apache.commons.lang3.time.DateUtils
import org.apache.log4j.Logger
import org.junit.Assert.assertEquals
import org.junit.Assert.assertFalse
import ru.planeta.commons.model.Gender
import ru.planeta.model.common.UserPrivateInfo
import ru.planeta.model.common.campaign.Campaign
import ru.planeta.model.common.campaign.CampaignTag
import ru.planeta.model.common.campaign.Share
import ru.planeta.model.common.campaign.enums.CampaignStatus
import ru.planeta.model.common.campaign.enums.ShareStatus
import ru.planeta.model.enums.GroupCategory
import ru.planeta.model.enums.ProfileStatus
import ru.planeta.model.enums.ProfileType
import ru.planeta.model.enums.UserStatus
import ru.planeta.model.profile.Profile
import ru.planeta.model.shop.Category
import ru.planeta.model.shop.Product
import ru.planeta.model.shop.ProductAttribute
import ru.planeta.model.shop.enums.ProductState
import java.io.File
import java.math.BigDecimal
import java.util.*

/**
 * @author pvyazankin
 * @since 17.10.12 14:30
 */

object TestHelper {

    fun createUserProfile(): Profile {
        val profile = Profile()
        profile.profileType = ProfileType.USER
        profile.imageUrl = "/imageurl"
        profile.imageId = 1
        profile.displayName = "name"
        profile.alias = "alias" + Math.random().toString()
        profile.status = ProfileStatus.NOT_SET
        profile.cityId = 1
        profile.countryId = 1
        profile.userBirthDate = Date()
        profile.userGender = Gender.NOT_SET
        return profile
    }

    fun createUser(profileId: Long): Profile {
        val profile = Profile()
        profile.profileId = profileId
        profile.alias = java.lang.Long.toString(profileId)
        profile.imageId = 1
        profile.imageUrl = "http://image.com"
        profile.displayName = "name#" + random(0xFFFF)
        profile.profileType = ProfileType.USER
        profile.status = ProfileStatus.NOT_SET
        profile.cityId = 1
        profile.countryId = 1
        profile.userGender = Gender.NOT_SET
        return profile
    }

    fun createUserPrivateInfo(profileId: Long): UserPrivateInfo {
        val userPrivateInfo = UserPrivateInfo()
        userPrivateInfo.userId = profileId
        userPrivateInfo.userStatus = EnumSet.of(UserStatus.USER)
        userPrivateInfo.timeAdded = Date()
        userPrivateInfo.timeUpdated = Date()
        userPrivateInfo.password = "123"
        userPrivateInfo.setUsernameAndEmail("__test__$profileId@test.ru")
        return userPrivateInfo
    }

    fun newProfile(): Profile {
        val profile = Profile()
        profile.profileType = ProfileType.USER
        profile.imageUrl = "/imageurl"
        profile.imageId = 1
        profile.alias = "alias"
        profile.status = ProfileStatus.NOT_SET
        profile.cityId = 1
        profile.countryId = 1
        profile.userBirthDate = Date()
        profile.userGender = Gender.NOT_SET
        profile.groupCategory = GroupCategory.MUSIC
        profile.displayName = "display name"
        return profile
    }

    fun createGroupProfile(): Profile {
        val profile = Profile()
        profile.profileType = ProfileType.GROUP
        profile.groupCategory = GroupCategory.OTHER
        profile.imageUrl = "/imageurl"
        profile.imageId = 1
        profile.displayName = "name"
        profile.alias = "alias"
        profile.status = ProfileStatus.NOT_SET
        profile.cityId = 1
        profile.countryId = 1
        return profile
    }

    fun createGroupProfile(creatorProfileId: Long): Profile {
        val profile = Profile()
        profile.profileType = ProfileType.GROUP
        profile.groupCategory = GroupCategory.OTHER
        profile.imageUrl = "/imageurl"
        profile.imageId = 1
        profile.displayName = "name"
        profile.alias = "group alias - $creatorProfileId"
        profile.status = ProfileStatus.NOT_SET
        profile.cityId = 1
        profile.countryId = 1
        profile.creatorProfileId = creatorProfileId
        return profile
    }


    fun createCampaign(profileId: Long): Campaign {
        val campaign = Campaign()
        val now = Date()
        campaign.profileId = profileId
        campaign.creatorProfileId = profileId
        campaign.targetAmount = BigDecimal.ONE
        campaign.timeStart = now
        campaign.timeFinish = DateUtils.addDays(Date(), 1)
        campaign.tags = mutableListOf()
        campaign.description = "desc"
        campaign.descriptionHtml = "desc html"
        campaign.purchaseSum = BigDecimal.TEN
        campaign.name = UUID.randomUUID().toString().replace("-".toRegex(), "").substring(0, 12)
        campaign.shortDescription = "short desc"
        campaign.status = CampaignStatus.ACTIVE
        campaign.albumId = 1000L
        campaign.imageId = 10L
        campaign.imageUrl = "image url"
        campaign.viewImageId = 10L
        campaign.viewImageUrl = "view image url"
        campaign.videoProfileId = 10L
        campaign.videoId = 10L
        campaign.videoUrl = "video url"
        campaign.timeAdded = now
        campaign.timeUpdated = now
        campaign.timeLastPurchased = now
        return campaign
    }

    fun createShare(profileId: Long, campaignId: Long): Share {
        val share = Share()
        share.profileId = profileId
        share.campaignId = campaignId
        share.name = "name"
        share.description = "desc"
        share.imageId = 100
        share.imageUrl = "image url"
        share.price = BigDecimal.ONE
        share.amount = 10
        share.shareStatus = ShareStatus.ACTIVE
        val now = Date()
        share.timeAdded = now
        return share
    }

    fun createProduct(merchantId: Long, productState: ProductState): Product {
        return createProduct(merchantId, null, productState)
    }

    fun createProduct(merchantId: Long, parentProduct: Product?, productState: ProductState): Product {

        val product = if (parentProduct != null) {
            generateProductTemplate(parentProduct.merchantProfileId!!, parentProduct.productId, productState)
        } else {
            generateProductTemplate(merchantId, 0, productState)
        }
        return product
    }

    fun createProduct(parentProduct: Product, productState: ProductState): Product {
        return createProduct(0, parentProduct, productState)
    }

    fun createProductAttribute(value: String): ProductAttribute {
        val attribute = ProductAttribute()
        attribute.attributeTypeId = 1
        attribute.value = value
        return attribute
    }

    fun insertChildProduct(parent: Product): Product {
        return createProduct(parent, ProductState.ATTRIBUTE)
    }

    fun createProductTopMETA(merchantId: Long): Product {
        return createProduct(merchantId, ProductState.META)
    }

    fun generateProductTemplate(merchantId: Long, parentProductId: Long, productState: ProductState): Product {
        val product = Product()
        product.productId = 0
        product.parentProductId = parentProductId
        product.name = productState.name + rndAttach()
        product.merchantProfileId = merchantId
        product.timeAdded = Date()
        product.productState = productState
        product.startSaleDate = Date()
        val imageUrls = ArrayList<String>()
        imageUrls.add("http://image1" + rndAttach() + ".jpg")
        imageUrls.add("http://image2" + rndAttach() + ".jpg")
        product.imageUrls = imageUrls
        //        product.setTags(insertCategory("Test Category"));
        //        product.setProductAttribute(createProductAttribute("Test Attribute"));
        product.isFreeDelivery = false
        product.textForMail = "Test text for mail"
        return product
    }

    fun createActiveSimpleProduct(storeId: Long): Long {
        val product1 = createProduct(storeId, ProductState.ATTRIBUTE)
        val productId1 = product1.productId
        return productId1
    }

    fun createActiveSimpleProduct(storeId: Long, quantity: Int): Long {
        val productId1 = createActiveSimpleProduct(storeId)
        return productId1
    }


    var rnd = Random()

    val TEST_PROFILE_ID: Long = 1000000000

    private val PLANETA_ALIAS = "planeta"

    private val log = Logger.getLogger(TestHelper::class.java)

    fun random(max: Int): Long {
        return Math.round(Math.random() * max) + 1
    }

    /**
     * Cleans directories used while testing
     */
    @Throws(Exception::class)
    fun cleanUp() {
        val staticDir = File(".\\planeta_static")
        val tmpDir = File(".\\tmp")

        if (staticDir.exists()) {
            log.info("Cleaning 'planeta_static' directory")
            FileUtils.deleteDirectory(staticDir)
            log.info("Directory 'planeta_static' has been cleaned")
        }

        if (tmpDir.exists()) {
            log.info("Cleaning 'tmp' directory")
            FileUtils.deleteDirectory(tmpDir)
            log.info("Directory 'tmp' has been cleaned")
        }
    }

    fun rndAttach(): String {
        return "_" + random(0xFFFF)
    }

    fun insertCategory(categoryName: String): List<Category> {
        val category = Category()
        category.value = categoryName
        return listOf(category)
    }

    fun assertBigDecimalEquals(expected: BigDecimal?, actual: BigDecimal?) {
        assertFalse((expected == null) xor (actual == null))
        if (expected != null) {
            assertEquals(expected.compareTo(actual!!).toLong(), 0)
        }
    }
}

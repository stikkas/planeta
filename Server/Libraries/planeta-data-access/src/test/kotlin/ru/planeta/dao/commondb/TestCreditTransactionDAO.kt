package ru.planeta.dao.commondb

import org.junit.Test
import org.springframework.beans.factory.annotation.Autowired
import ru.planeta.model.common.CreditTransaction


import java.math.BigDecimal
import java.util.Date

import org.junit.Assert.*
import org.junit.runner.RunWith
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.transaction.annotation.Transactional

/**
 * Class TestCreditTransactionDAO
 *
 * @author a.tropnikov
 */
@RunWith(SpringRunner::class)
@Transactional
@ContextConfiguration(locations = ["classpath:applicationContext-common-test.xml"])
class TestCreditTransactionDAO {

    @Autowired
    lateinit var creditTransactionDAO: CreditTransactionDAO

    @Test
    fun test() {
        val testProfileId: Long = 1000000000

        val creditTransaction = CreditTransaction()
        creditTransaction.profileId = testProfileId
        creditTransaction.amountNet = BigDecimal.ONE
        creditTransaction.amountFee = BigDecimal.TEN
        creditTransaction.comment = "comment"
        creditTransaction.timeAdded = Date()

        creditTransactionDAO.insert(creditTransaction)
        assertTrue(creditTransaction.transactionId > 0)

        val selectedList = creditTransactionDAO.selectList(testProfileId, 0, 0)
        assertEquals(selectedList.size.toLong(), 1)

        val selected = creditTransactionDAO.select(creditTransaction.transactionId)
        assertCreditTransactionEquals(selected, creditTransaction)

        creditTransaction.amountNet = BigDecimal.TEN
        creditTransaction.amountFee = BigDecimal.ONE
        creditTransaction.comment = "new comment"
        creditTransactionDAO.update(creditTransaction)

        val updated = creditTransactionDAO.select(creditTransaction.transactionId)
        assertCreditTransactionEquals(updated, creditTransaction)

        creditTransactionDAO.delete(creditTransaction.profileId, creditTransaction.transactionId)
        assertNull(creditTransactionDAO.select(creditTransaction.transactionId))
    }

    private fun assertCreditTransactionEquals(expected: CreditTransaction, actual: CreditTransaction) {
        assertNotNull(expected)
        assertEquals(expected.transactionId, actual.transactionId)
        assertEquals(expected.amountNet!!.compareTo(actual.amountNet!!).toLong(), 0)
        assertEquals(expected.amountFee!!.compareTo(actual.amountFee!!).toLong(), 0)
        assertEquals(expected.comment, actual.comment)
        assertEquals(expected.timeAdded, actual.timeAdded)
    }
}

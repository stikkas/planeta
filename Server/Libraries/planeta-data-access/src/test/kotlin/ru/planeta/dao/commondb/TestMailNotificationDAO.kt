package ru.planeta.dao.commondb

import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.transaction.annotation.Transactional
import ru.planeta.model.mail.MailMessage
import ru.planeta.model.mail.MailMessagePriority
import ru.planeta.model.mail.MailMessageStatus


import java.util.Collections

/**
 * User: michail
 * Date: 19.05.16
 * Time: 14:10
 */
@RunWith(SpringRunner::class)
@Transactional
@ContextConfiguration(locations = ["classpath:applicationContext-common-test.xml"])
class TestMailNotificationDAO {

    @Autowired
    lateinit var mailMessageDAO: MailMessageDAO

    @Test
    fun insertEmailNotification() {
        val sender = "testSender"
        val addressee = "testAddressee1, testAddressee2"
        val subject = "testSubject"
        val replyTo = "test@planeta.ru"
        val messageBody = "testBody"
        val expected = MailMessage(sender, replyTo, listOf(addressee), subject, messageBody)
        mailMessageDAO.insertMailNotification(expected)
        val actual = mailMessageDAO.getMailNotificationById(expected.messageId)[0]
        assertMessageEquals(actual, expected)
    }

    @Test
    fun insertEmailNotificationWithSkipLogging() {
        val sender = "testSender"
        val addressee = "testAddressee"
        val subject = "testSubject"
        val replyTo = "test@planeta.ru"
        val messageBody = "testBody"
        val expected = MailMessage(sender, replyTo, listOf(addressee), subject, messageBody, null, true, MailMessagePriority.DEFAULT)
        mailMessageDAO.insertMailNotification(expected)
        val actual = mailMessageDAO.getMailNotificationById(expected.messageId)[0]
        assertMessageEqualsWithoutBody(actual, expected)
        Assert.assertEquals("", actual.contentHtml)
    }


    @Test
    fun updateEmailNotification() {
        val externalMessageId = "test456"
        val sender = "testSender"
        val addressee = "testAddressee"
        val subject = "testSubject"
        val replyTo = "test@planeta.ru"
        val messageBody = "testBody"
        val status = MailMessageStatus.SENT
        val expected = MailMessage(sender, replyTo, listOf(addressee), subject, messageBody)
        mailMessageDAO.insertMailNotification(expected)

        expected.externalMessageId = externalMessageId
        expected.status = status
        mailMessageDAO.updateMailNotification(expected)
        val actual = mailMessageDAO.getMailNotificationById(expected.messageId)[0]
        assertMessageEquals(actual, expected)
    }

    private fun assertMessageEquals(actual: MailMessage, expected: MailMessage) {
        assertMessageEqualsWithoutBody(actual, expected)
        Assert.assertEquals(actual.contentHtml, expected.contentHtml)
    }

    private fun assertMessageEqualsWithoutBody(actual: MailMessage, expected: MailMessage) {
        Assert.assertNotNull(actual)
        Assert.assertEquals(actual.messageId, expected.messageId)
        Assert.assertEquals(actual.subject, expected.subject)
        Assert.assertEquals(actual.toList, expected.toList)
        Assert.assertEquals(actual.from, expected.from)
        Assert.assertEquals(actual.status, expected.status)
        Assert.assertEquals(actual.externalMessageId, expected.externalMessageId)
    }
}

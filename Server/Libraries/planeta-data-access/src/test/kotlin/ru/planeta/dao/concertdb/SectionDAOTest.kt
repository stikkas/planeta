package ru.planeta.dao.concertdb

import org.junit.Assert.assertEquals
import org.junit.Ignore
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.transaction.annotation.Transactional
import ru.planeta.model.concert.Section

/**
 * Created with IntelliJ IDEA.
 * User: michail
 * Date: 14.10.16
 * Time: 17:37
 */
@Ignore
@RunWith(SpringRunner::class)
@Transactional
@ContextConfiguration(locations = ["classpath:applicationContext-common-test.xml"])
class SectionDAOTest {

    @Autowired
    lateinit var sectionDAO: SectionDAO

    @Test
    fun testSectionCRU() {
        var actual = create()
        sectionDAO.insert(actual)
        var expected = clone(actual)
        actual = sectionDAO.select(actual.sectionId)
        assertSectionEquals(actual, expected)

        actual.name = "test2"
        actual.externalSectionId = 5
        actual.externalParentId = 6
        expected = clone(actual)
        sectionDAO.update(actual)
        actual = sectionDAO.select(actual.sectionId)
        assertSectionEquals(actual, expected)
    }

    @Ignore
    @Test
    fun testSelectParams() {
        val s1 = sectionDAO.selectByExtConcertId(concertId)
        val s2 = sectionDAO.selectSectorsByExtConcertId(concertId)
        val s3 = sectionDAO.selectRowsByExtConcertId(concertId, null)
        val s4 = sectionDAO.selectRowsByExtConcertId(concertId, sectorId)

    }

    companion object {

        private fun create(): Section {
            val `object` = Section()
            `object`.hallId = 0
            `object`.externalSectionId = 1
            `object`.externalParentId = 2
            `object`.name = "test"
            return `object`
        }


        private fun clone(`object`: Section): Section {
            val clone = Section()
            clone.sectionId = `object`.sectionId
            clone.hallId = `object`.hallId
            clone.externalSectionId = `object`.externalSectionId
            clone.externalParentId = `object`.externalParentId
            clone.name = `object`.name
            return clone
        }

        private fun assertSectionEquals(actual: Section, expected: Section) {
            assertEquals(actual.sectionId, expected.sectionId)
            assertEquals(actual.hallId, expected.hallId)
            assertEquals(actual.externalSectionId, expected.externalSectionId)
            assertEquals(actual.externalParentId, expected.externalParentId)
            assertEquals(actual.name, expected.name)
        }

        private val concertId: Long = 954
        private val sectorId: Long = 2344075
    }
}

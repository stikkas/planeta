package ru.planeta.dao.bibliodb

import org.junit.Assert.*
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.transaction.annotation.Transactional
import ru.planeta.model.bibliodb.MagazineInfo
import ru.planeta.model.bibliodb.MagazinePriceList
import java.math.BigDecimal

@RunWith(SpringRunner::class)
@Transactional
@ContextConfiguration(locations = ["classpath:applicationContext-common-test.xml"])
class MagazineInfoDAOTest {

    @Autowired
    lateinit var magazineInfoDAO: MagazineInfoDAO

    @Autowired
    lateinit var magazinePriceListDAO: MagazinePriceListDAO


    @Test
    fun testInsertSelectUpdate() {
        val mpl = MagazinePriceList()
        mpl.name = "testName"

        magazinePriceListDAO.insert(mpl)
        assertTrue(mpl.priceListId > 0)
        val selectedMpl = magazinePriceListDAO.select(mpl.priceListId)
        assertEquals(mpl.name, selectedMpl.name)
        assertNotNull(selectedMpl.timeAdded)

        val mi = MagazineInfo()
        mi.priceListId = mpl.priceListId
        mi.magazineName = "testMagazineName01"
        mi.setpIndex("p001")
        mi.periodicity = 1
        mi.partPriceWithoutTax = BigDecimal(89.11)
        mi.taxPercentage = BigDecimal(0.10)
        mi.partPriceWithTax = BigDecimal(98.02)
        mi.commentNoMagazineMonth = "March"

        magazineInfoDAO.insert(mi)
        assertTrue(mi.magazineInfoId > 0)

        val mi2 = MagazineInfo()
        mi2.priceListId = mpl.priceListId
        mi2.magazineName = "testMagazineName02"
        mi2.setpIndex("p002")
        mi2.periodicity = 1
        mi2.partPriceWithoutTax = BigDecimal(154.75)
        mi2.taxPercentage = BigDecimal(0.10)
        mi2.partPriceWithTax = BigDecimal(170.23)
        mi2.commentNoMagazineMonth = "October, November"

        magazineInfoDAO.insert(mi2)
        assertTrue(mi2.magazineInfoId > 0)

        val magazineInfoList = magazineInfoDAO.selectList(mpl.priceListId, 0, 200)
        assertNotNull(magazineInfoList)

    }
}

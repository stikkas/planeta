package ru.planeta.dao.commondb


import org.junit.Assert.*
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.transaction.annotation.Transactional
import ru.planeta.model.common.loyalty.Bonus
import ru.planeta.model.common.loyalty.BonusPriorityItem
import java.lang.Math.random
import java.util.*

/**
 * @author Andrew.Arefyev@gmail.com
 * 09.04.2014 15:43
 */
@RunWith(SpringRunner::class)
@Transactional
@ContextConfiguration(locations = ["classpath:applicationContext-common-test.xml"])
class TestBonusDAO {

    @Autowired
    lateinit var bonusDAO: BonusDAO

    @Throws(Exception::class)
    fun testSelectBonuses() {

    }

    @Throws(Exception::class)
    fun testSelect() {

    }

    @Test
    @Throws(Exception::class)
    fun testInsert() {
        val bonus = createBonus()
        bonusDAO.insert(bonus)
        assertNotEquals(0, bonus.bonusId)
        assertNotNull(bonusDAO.select(bonus.bonusId))
    }

    @Test
    @Throws(Exception::class)
    fun testUpdate() {
        val bonuses = arrayOf<Bonus>()
        val priorityItems = arrayOf<BonusPriorityItem>()
        for (i in 0..2) {
            bonuses[i] = createBonus()
            bonusDAO.insert(bonuses[i])
            priorityItems[i] = BonusPriorityItem(bonuses[i].bonusId, Math.round(random()).toInt())
        }
        bonusDAO.updateBonusesOrder(Arrays.asList<BonusPriorityItem>(*priorityItems))
        for (i in 0..2) {
            assertEquals(bonusDAO.select(bonuses[i].bonusId)?.priority?.toLong(), priorityItems[i].priority.toLong())
        }

    }

    @Test
    fun testBonusConstrains() {

        val bonus = createBonus()
        try {
            bonus.available = -2
            bonusDAO.insert(bonus)
            fail()
        } catch (e: Exception) {
            println(e.message)
        }


    }

    fun createBonus(): Bonus {
        val bonus = Bonus()
        bonus.price = 100
        bonus.name = "bonus" + Math.random() * 0xFFFF
        bonus.description = "It's a bonus" + Math.random() * 0xFFFF
        bonus.available = 1
        bonus.type = Bonus.Type.BONUS
        return bonus
    }
}

package ru.planeta.repository

import org.assertj.core.api.Assertions.assertThat
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.transaction.annotation.Transactional
import ru.planeta.commons.model.Gender
import ru.planeta.entity.Profile
import ru.planeta.model.enums.ProfileStatus
import ru.planeta.model.enums.ProfileType
import ru.planeta.dao.ProfileRepositoryDAO
import java.util.*


@RunWith(SpringRunner::class)
@ContextConfiguration(locations = ["classpath:applicationContext-common-test.xml"])
@Transactional
class ProfileRepositoryDAOTest {

    @Autowired
    lateinit var profileRepository: ProfileRepositoryDAO

    @Test
    fun testProfileService() {
        val profile = Profile()

        // Тестим добавление профиля
        profile.creatorProfileId = 1
        profile.profileType = ProfileType.USER
        profile.imageUrl = "test_url"
        profile.imageId = 12
        profile.smallImageUrl = "small_image_url"
        profile.smallImageId = 13
        profile.alias = "alias1"
        profile.status = ProfileStatus.USER_ACTIVE
        profile.cityId = 1
        profile.countryId = 1
        profile.userBirthDate = Date()
        profile.userGender = Gender.MALE
        profile.usersCount = 1
        profile.displayName = "user_test"
        profile.summary = "test_user"
        profile.vip = true
        profile.vipObtainDate = Date()
        profile.isReceiveNewsletters = true
        profile.isShowBackedCampaigns = true
        profile.backedCount = 1
        profile.projectsCount = 1
        profile.authorProjectsCount = 1
        profile.subscribersCount = 1
        profile.newSubscribersCount = 1
        profile.subscriptionsCount = 1
        profile.receiveMyCampaignNewsletters = true
        profile.phoneNumber = "123"
        val now = Date()
        profile.timeUpdated = now
        profile.timeAdded = now

        profileRepository.insert(profile)

        assertThat(profile.timeAdded).isNotNull()
        assertThat(profile.timeUpdated).isNotNull()
        assertThat(profile.timeUpdated).isEqualTo(profile.timeAdded)

        var existProfile = profileRepository.findOne(profile.profileId)

        assertThat(existProfile).isNotNull
        assertThat(existProfile?.timeAdded).isNotNull()
        assertThat(existProfile?.timeUpdated).isNotNull()
        assertThat(existProfile?.timeUpdated).isEqualTo(existProfile?.timeAdded)

        // Тестим обновление профиля
        profile.creatorProfileId = 2
        profile.imageUrl = "test_url_2"
        profile.imageId = 123
        profile.smallImageUrl = "small_image_url2"
        profile.smallImageId = 133
        profile.cityId = 2
        profile.countryId = 3
        profile.userBirthDate = Date()
        profile.userGender = Gender.FEMALE
        profile.usersCount = 2
        profile.displayName = "user_test2"
        profile.summary = "test_user2"
        profile.vip = false
        profile.vipObtainDate = Date()
        profile.isReceiveNewsletters = false
        profile.isShowBackedCampaigns = false
        profile.backedCount = 12
        profile.projectsCount = 12
        profile.authorProjectsCount = 12
        profile.subscribersCount = 12
        profile.newSubscribersCount = 12
        profile.subscriptionsCount = 122
        profile.receiveMyCampaignNewsletters = false
        profile.phoneNumber = "123222"

        val timeCreated = profile.timeAdded
        val lastTimeUpdated = profile.timeUpdated

        profile.timeUpdated = Date()
        profileRepository.update(profile)

        existProfile = profileRepository.findOne(profile.profileId)
        assertThat(existProfile?.timeAdded).isNotNull()
        assertThat(existProfile?.timeUpdated).isNotNull()
        assertThat(existProfile?.timeUpdated).isAfter(existProfile?.timeAdded)
        assertThat(timeCreated).isEqualTo(existProfile?.timeAdded)
        assertThat(existProfile?.timeUpdated).isAfter(lastTimeUpdated)
    }
}


package ru.planeta.dao.commondb

import org.junit.Assert
import org.junit.Test
import org.springframework.beans.factory.annotation.Autowired
import ru.planeta.model.common.Address


import org.junit.Assert.assertNotNull
import org.junit.runner.RunWith
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.transaction.annotation.Transactional

/**
 * Date: 20.06.12
 *
 * @author s.kalmykov
 */

@RunWith(SpringRunner::class)
@Transactional
@ContextConfiguration(locations = ["classpath:applicationContext-common-test.xml"])
class TestAddressDAO {

    @Autowired
    lateinit var addressDAO: AddressDAO

    private fun newAddress(): Address {
        val address = Address()
        address.street = "Sadovaja-Chernogriazskaya"
        address.city = "Moscow"
        address.country = "Russia"
        address.phone = "77777777"
        address.zipCode = "100100"
        return address
    }

    @Test
    @Throws(Exception::class)
    fun testInsertSelect() {
        val inserted = newAddress()
        addressDAO!!.insert(inserted)
        val selected = addressDAO.select(inserted.getAddressId())
        assertNotNull(selected)
        assertAddressesEquals(selected, inserted)
    }

    private fun assertAddressesEquals(expected: Address, actual: Address) {
        Assert.assertEquals(expected.getAddressId(), actual.getAddressId())
        Assert.assertEquals(expected.street, actual.street)
        Assert.assertEquals(expected.city, actual.city)
        Assert.assertEquals(expected.phone, actual.phone)
        Assert.assertEquals(expected.zipCode, actual.zipCode)
    }

}

package ru.planeta.dao.commondb.campaign

import org.apache.commons.lang3.time.DateUtils
import org.junit.Ignore
import org.junit.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.util.Assert
import ru.planeta.model.common.campaign.Campaign
import ru.planeta.model.common.campaign.CampaignTag
import ru.planeta.model.common.campaign.enums.CampaignStatus
import ru.planeta.model.common.campaign.enums.ShareStatus

import java.math.BigDecimal
import java.net.URLDecoder
import java.util.*

import org.junit.Assert.*
import org.junit.runner.RunWith
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.transaction.annotation.Transactional
import ru.planeta.TestHelper
import ru.planeta.dao.commondb.CampaignDAO
import ru.planeta.dao.commondb.ShareDAO

/**
 * Created by IntelliJ IDEA.
 * User: atropnikov
 * Date: 21.03.12
 * Time: 14:12
 */
@RunWith(SpringRunner::class)
@Transactional
@ContextConfiguration(locations = ["classpath:applicationContext-common-test.xml"])
class TestCampaignDAO {

    @Autowired
    lateinit var campaignDAO: CampaignDAO

    @Autowired
    lateinit var shareDAO: ShareDAO

    @Test
    fun testInsertUpdateDelete() {
        val testProfileId = TestHelper.TEST_PROFILE_ID

        val campaign = TestHelper.createCampaign(testProfileId)
        assertTrue(campaign.campaignId > 0)

        var campaigns = campaignDAO.selectCampaignsByStatus(-1, null, 0, 10)
        assertTrue(!campaigns.isEmpty())

        updateCampaignFields(campaign)
        campaignDAO.update(campaign)

        var selected = campaignDAO.selectCampaignById(campaign.campaignId)
        assertCampaignEquals(campaign, selected!!)

        campaigns = campaignDAO.selectCampaignsByStatus(campaign.profileId!!, null, 0, 100)
        assertTrue(campaigns.size == 1)
        assertCampaignEquals(campaigns[0], campaign)

        campaigns = campaignDAO.selectCampaignsByStatus(campaign.profileId!!, EnumSet.of(CampaignStatus.PAUSED), 0, 10)
        assertTrue(campaigns.size == 1)
        assertCampaignEquals(campaigns[0], campaign)

        campaigns = campaignDAO.selectCampaignsByStatus(-1, EnumSet.of(CampaignStatus.PAUSED), 0, 10)
        assertTrue(!campaigns.isEmpty())

        campaigns = campaignDAO.selectCampaignsByStatus(campaign.profileId!!, EnumSet.of(CampaignStatus.PAUSED, CampaignStatus.ACTIVE),
                0, 10)
        assertTrue(!campaigns.isEmpty())


        campaignDAO.delete(campaign.campaignId)
        selected = campaignDAO.selectCampaignById(campaign.campaignId)
        assertNull(selected)

        campaigns = campaignDAO.selectCampaignsByStatus(campaign.profileId!!, EnumSet.of(CampaignStatus.ALL), 0, 10)
        assertEquals(campaigns.size.toLong(), 0)
    }

    @Ignore
    @Test
    fun testSelectWithShares() {
        val profileId = TestHelper.TEST_PROFILE_ID
        val campaignId = TestHelper.TEST_PROFILE_ID
        shareDAO.insert(TestHelper.createShare(profileId, campaignId))
        val shareId1 = TestHelper.createShare(profileId, campaignId).shareId
        val savedShare = shareDAO.select(shareId1)!!
        Assert.notNull(savedShare)
        savedShare.shareStatus = ShareStatus.DISABLED
        shareDAO.update(savedShare)
        val shareDAOShares = shareDAO.selectCampaignShares(campaignId, 0, 0)
        assertEquals(2, shareDAOShares.size.toLong())

        var campaign = campaignDAO.selectCampaignById(campaignId)!!
        assertNotNull(campaign)
        assertNull(campaign.shares)

        campaign = campaignDAO.selectWithShares(campaignId, false)!!
        assertNotNull(campaign)
        assertNotNull(campaign.shares)
        assertEquals(2, campaign.shares.size.toLong())

        val campaigns = campaignDAO.selectCampaignsWithSharesByStatus(profileId, CampaignStatus.ACTIVE, 0, 10)
        assertEquals(campaigns.size.toLong(), 1)
        campaign = campaigns[0]
        assertNotNull(campaign.shares)
        assertEquals(2, campaign.shares.size.toLong())
    }

    @Test
    fun testBackersCount() {
        val count = campaignDAO.getCampaignBackerCount(304)
        print(count)
    }

    @Test
    fun testSelectStartedByTime() {
        val profileId = TestHelper.TEST_PROFILE_ID
        val campaign = TestHelper.createCampaign(profileId)
        // In the beginning we test a cause when the campaign start time is null
        campaign.timeStart = null
        campaign.status = CampaignStatus.APPROVED
        campaignDAO.update(campaign)

        var campaigns = campaignDAO.selectStartedCampaignsByTime(Date(), EnumSet.of(CampaignStatus.APPROVED), 0, 0)

        var found = false
        for (started in campaigns) {
            if (!found) {
                found = started.profileId == profileId
            }
        }
        // It shouldn't be any campaigns created by our profileId
        assertFalse(found)

        // ---------------------------------------

        // Then we test a cause when the campaign start time is specified

        campaign.timeStart = DateUtils.addDays(Date(), -1)
        campaign.status = CampaignStatus.APPROVED
        campaignDAO.update(campaign)

        campaigns = campaignDAO.selectStartedCampaignsByTime(Date(), EnumSet.of(CampaignStatus.APPROVED), 0, 0)
        assertFalse(campaigns.isEmpty())

        found = false
        for (started in campaigns) {
            if (!found) {
                found = started.profileId == profileId
            }
        }
        assertTrue(found)
    }

    @Test
    fun testSelectFinishedByTime() {
        val profileId = TestHelper.TEST_PROFILE_ID
        val campaign = TestHelper.createCampaign(profileId)
        campaign.timeFinish = DateUtils.addDays(Date(), -1)
        campaign.status = CampaignStatus.ACTIVE
        campaignDAO.update(campaign)

        val campaigns = campaignDAO.selectFinishedCampaignsByTime(Date(), EnumSet.of(CampaignStatus.ACTIVE, CampaignStatus.PAUSED), 0, 0)
        assertFalse(campaigns.isEmpty())

        var found = false
        for (finished in campaigns) {
            if (!found) {
                found = finished.profileId == profileId
            }
        }
        assertTrue(found)
    }

    @Test
    @Throws(Exception::class)
    fun testQuerySearch() {
        // Программирование
        val programming = URLDecoder.decode("%D0%9F%D1%80%D0%BE%D0%B3%D1%80%D0%B0%D0%BC%D0%BC%D0%B8%D1%80%D0%BE%D0%B2%D0%B0%D0%BD%D0%B8%D0%B5", "utf-8")
        // программист
        val programmer = URLDecoder.decode("%D0%BF%D1%80%D0%BE%D0%B3%D1%80%D0%B0%D0%BC%D0%BC%D0%B8%D1%81%D1%82", "utf-8")
        // программа
        val programm = URLDecoder.decode("%D0%BF%D1%80%D0%BE%D0%B3%D1%80%D0%B0%D0%BC%D0%BC%D0%B0", "utf-8")

        val profileId = TestHelper.TEST_PROFILE_ID
        val groupId = TestHelper.TEST_PROFILE_ID
        val campaign = TestHelper.createCampaign(groupId)
        val id0 = campaign.campaignId
        campaign.name = programming
        campaign.descriptionHtml = "$programming $programm"
        campaign.campaignId = 0
        campaignDAO.insert(campaign)
        val id1 = campaign.campaignId

        campaign.name = programm
        campaign.descriptionHtml = "$programm $programming"
        campaign.campaignId = 0
        campaignDAO.insert(campaign)
        val id2 = campaign.campaignId

        campaign.name = "$programming $programm"
        campaign.descriptionHtml = "$programming $programmer $programm"
        campaign.campaignId = 0
        campaignDAO.insert(campaign)
        val id3 = campaign.campaignId

        campaign.name = "some name"
        campaign.descriptionHtml = "some description"
        campaign.campaignId = 0
        campaignDAO.insert(campaign)
        val id4 = campaign.campaignId

        val id5 = id4 + 1

        val ids = ArrayList<Long>()
        ids.add(id5)
        ids.add(id4)
        ids.add(id3)
        ids.add(id2)
        ids.add(id1)
        ids.add(id0)

        val campaigns = campaignDAO.getCampaignsByIds(ids)
        assertEquals(5, campaigns.size.toLong())

    }

    private fun updateCampaignFields(campaign: Campaign) {
        campaign.targetAmount = BigDecimal.TEN
        campaign.timeStart = DateUtils.addDays(Date(), 2)
        campaign.timeFinish = DateUtils.addDays(Date(), 3)
        campaign.tags = mutableListOf<CampaignTag>()
        campaign.description = "new desc"
        campaign.descriptionHtml = "new desc html"
        campaign.purchaseSum = BigDecimal.ONE
        campaign.purchaseCount = 2
        campaign.name = "new name"
        campaign.shortDescription = "new short desc"
        campaign.status = CampaignStatus.PAUSED
        campaign.albumId = 1011204L
        campaign.imageId = 11L
        campaign.imageUrl = "new image url"
        campaign.timeUpdated = Date()
        campaign.targetAmount = null
        campaign.timeFinish = null
        campaign.timeLastPurchased = Date()
    }

    private fun assertCampaignEquals(expected: Campaign, actual: Campaign) {
        assertNotNull(expected)
        assertEquals(expected.campaignId, actual.campaignId)
        assertEquals(expected.profileId, actual.profileId)

        assertEquals(expected.tags.size.toLong(), actual.tags.size.toLong())
        for (tag in actual.tags) {
            assertTrue(expected.tags.contains(tag))
        }
        assertEquals(expected.timeStart, actual.timeStart)
        assertEquals(expected.timeFinish, actual.timeFinish)
        assertEquals(expected.timeFinish, actual.timeFinish)
        assertEquals(expected.description, actual.description)
        assertEquals(expected.descriptionHtml, actual.descriptionHtml)
        assertEquals(expected.purchaseSum.compareTo(actual.purchaseSum).toLong(), 0)
        assertEquals(expected.purchaseCount.toLong(), actual.purchaseCount.toLong())
        assertEquals(expected.albumId, actual.albumId)
        assertEquals(expected.imageId, actual.imageId)
        assertEquals(expected.imageUrl, actual.imageUrl)
        assertEquals(expected.viewImageId, actual.viewImageId)
        assertEquals(expected.viewImageUrl, actual.viewImageUrl)
        assertEquals(expected.videoProfileId, actual.videoProfileId)
        assertEquals(expected.videoId, actual.videoId)
        assertEquals(expected.videoUrl, actual.videoUrl)
        assertEquals(expected.name, actual.name)
        assertEquals(expected.shortDescription, actual.shortDescription)
        assertEquals(expected.status, actual.status)
        assertEquals(expected.targetAmount?.compareTo(actual.targetAmount)?.toLong(), 0)
        assertEquals(expected.timeAdded, actual.timeAdded)
        assertEquals(expected.timeUpdated, actual.timeUpdated)
        assertEquals(expected.isFinishOnTargetReach, actual.isFinishOnTargetReach)
        assertEquals(expected.isIgnoreTargetAmount, actual.isIgnoreTargetAmount)
        assertEquals(expected.timeLastPurchased, actual.timeLastPurchased)
    }

    @Test
    fun testGetCampaignIdByAlias() {

        val testProfileId = TestHelper.TEST_PROFILE_ID
        val campaign = TestHelper.createCampaign(testProfileId)
        campaign.campaignAlias = "campaign_alias" + campaign.campaignId
        campaignDAO.update(campaign)

        val selectedCampaignId = campaignDAO.getCampaignId(campaign.webCampaignAlias ?: "")
        assertEquals(selectedCampaignId, campaign.campaignId)

    }

    @Test
    fun testSelectCampaignNewEventsCount() {

        campaignDAO.selectCampaignNewEventsCountList(listOf(19320.toLong()), 53871, null)
        campaignDAO.selectCampaignNewEventsCountList(listOf(19320.toLong()), -1, null)
    }

}

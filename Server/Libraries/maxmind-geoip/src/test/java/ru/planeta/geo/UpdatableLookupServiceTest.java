package ru.planeta.geo;

import org.apache.commons.compress.archivers.ArchiveException;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import ru.planeta.geo.updatable.UpdatableLookupService;

import java.io.IOException;

import static org.junit.Assert.assertEquals;


/**
 * @author a.vovchenko
 * 10.02.2012
 * 17:09:58
 */

public class UpdatableLookupServiceTest  {
	private UpdatableLookupService lookupService = null;
	
	@Before
	public void initTestCase() throws IOException, ArchiveException {
		this.lookupService = new UpdatableLookupService("src/main/resources/geoip/GeoIP.dat", 1, 40000, 10000);
	}
	
	@Test
	@Ignore
	public void testUpdatableLookupServiceInMultiThreadApplication() throws InterruptedException
	{
		Thread t1 = new Thread(new LookupServiceReader(this.lookupService));
		Thread t2 = new Thread(new LookupServiceReader(this.lookupService));
		Thread t3 = new Thread(new LookupServiceReader(this.lookupService));
		Thread t4 = new Thread(new LookupServiceReader(this.lookupService));
		t1.start();
		t2.start();
		t3.start();
		t4.start();
		Thread.sleep(90 * 1000);
		t1.interrupt();
		t2.interrupt();
		t3.interrupt();
		t4.interrupt();
	}


	@Test
	@Ignore
	public void testUSA() throws InterruptedException, IOException, ArchiveException {
		UpdatableLookupService lookupService = new UpdatableLookupService("src/main/resources/geoip/GeoIP.dat", 1, 15000, 1000);
		String code = lookupService.getCountryCode("107.167.112.142");
		assertEquals("AP", code);
		Thread t1 = new Thread(new LookupServiceReader(lookupService));
		t1.start();

		Thread.sleep(20 * 1000);
		code = lookupService.getCountryCode("107.167.112.142");
		assertEquals("AP", code);
		Thread.sleep(90 * 1000);
		code = lookupService.getCountryCode("107.167.112.142");
		assertEquals("AP", code);
		t1.interrupt();
	}
	
	@After
	public void tearDown()
	{
		this.lookupService.close();
	}
	

}

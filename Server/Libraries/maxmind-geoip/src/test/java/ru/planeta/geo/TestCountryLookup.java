package ru.planeta.geo;

import com.maxmind.geoip.LookupService;
import org.apache.log4j.Logger;
import org.junit.Test;

import java.io.IOException;


/**
 * @author ameshkov
 */
public class TestCountryLookup {

    private static final Logger log = Logger.getLogger(TestCountryLookup.class);

    @Test
    public void testCountryLookup() {
        try {
            String path = Thread.currentThread().getContextClassLoader().getResource("geoip/GeoIP.dat").getFile();
            // You should only call LookupService once, especially if you use
            // GEOIP_MEMORY_CACHE mode, since the LookupService constructor takes up
            // resources to load the GeoIP.dat file into memory
            //LookupService cl = new LookupService(dbfile,LookupService.GEOIP_STANDARD);
            LookupService cl = new LookupService(path, LookupService.GEOIP_MEMORY_CACHE);

            log.info(cl.getCountry("127.0.0.1").getCode());
            log.info(cl.getCountry("151.38.39.114").getCode());
            log.info(cl.getCountry("151.38.39.114").getName());
            log.info(cl.getCountry("12.25.205.51").getName());
            log.info(cl.getCountry("64.81.104.131").getName());
            log.info(cl.getCountry("200.21.225.82").getName());
            log.info(cl.getCountry("124.157.249.100").getCode());
            log.info(cl.getCountry("124.157.249.100").getName());

            cl.close();
        } catch (IOException e) {
            log.warn(e);
        }
    }

}
package ru.planeta.geo;

import ru.planeta.geo.updatable.UpdatableLookupService;
import org.apache.log4j.Logger;

import java.util.Random;

/**
 * @author a.vovchenko
 * 10.02.2012
 * 17:20:20
 */
public class LookupServiceReader implements Runnable {

    private static final Logger log = Logger.getLogger(LookupServiceReader.class);

	private final UpdatableLookupService lookupService;
	
	LookupServiceReader(UpdatableLookupService lookupService) {
		this.lookupService = lookupService;
	}

	@Override
	public void run() {
		boolean doWork = true;
		while (doWork)
		{
	        log.info(this.lookupService.getCountry("127.0.0.1").getCode());
		    log.info(this.lookupService.getCountry("151.38.39.114").getCode());
		    log.info(this.lookupService.getCountry("151.38.39.114").getName());
		    log.info(this.lookupService.getCountry("12.25.205.51").getName());
		    log.info(this.lookupService.getCountry("64.81.104.131").getName());
		    log.info(this.lookupService.getCountry("200.21.225.82").getName());
	        log.info(this.lookupService.getCountry("124.157.249.100").getCode());
	        log.info(this.lookupService.getCountry("124.157.249.100").getName());
	        Random random = new Random();
	        int sleepTime = random.nextInt(30);
	        try {
				Thread.sleep(sleepTime * 1000);
			} catch (InterruptedException e) {
				doWork = false;
			}
			
		}

	}

}

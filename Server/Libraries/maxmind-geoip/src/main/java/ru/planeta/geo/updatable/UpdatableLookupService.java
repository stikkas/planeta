package ru.planeta.geo.updatable;

import com.maxmind.geoip.Country;
import com.maxmind.geoip.LookupService;
import org.apache.commons.compress.archivers.ArchiveException;
import org.apache.commons.compress.compressors.gzip.GzipCompressorInputStream;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Timer;

/**
 * @author a.vovchenko
 *         09.02.2012
 *         21:44:36
 */
public class UpdatableLookupService implements GeoLookupService {

    private static final Logger log = Logger.getLogger(UpdatableLookupService.class);

    /**
     * Main MaxMind LookupService, used for all methods implementation
     */
    private LookupService lookupService;

    /**
     * timer for updating geoDatabaseFile
     */
    private Timer geoDataUpdateTimer;

    /**
     * Update period, default - 1 day
     */
    private final long sleepTime;
    /**
     * First update time, default - half a day
     */
    private final long startSleepTime;

    /**
     * URI to geoIP Update Data
     */
    private static final String UPDATE_GEOIPDATA_URI = "http://www.maxmind.com/app/update?license_key=atsK5eZ4Iw5l";

    /**
     * Constructor parameter databaseFile
     */
    private final String databaseFile;
    /**
     * Constructor parameter options
     */
    private final int options;

    public UpdatableLookupService(String databaseFile, int options, long sleepTime, long startSleepTime)
            throws IOException, ArchiveException {
        this.sleepTime = sleepTime;
        this.startSleepTime = startSleepTime;
        this.databaseFile = databaseFile;
        this.options = options;
        initUpdateProcess();
        updataDatabaseFile();
    }

    /**
     */
    private void initUpdateProcess() {
        geoDataUpdateTimer = new Timer();
        geoDataUpdateTimer.schedule(new GeoDataUpdateTask(this), startSleepTime, sleepTime);
    }

    /**
     * Method stop timer for update process
     */
    public void stopUpdateProcess() {
        geoDataUpdateTimer.cancel();
    }

    /**
     * Method used in Timer, for DB file update
     *
     * @throws IOException
     */
    void updataDatabaseFile() throws IOException {
        boolean isNewFile = downloadFile(UpdatableLookupService.UPDATE_GEOIPDATA_URI, databaseFile + ".tmp", "gz");
        if (isNewFile) {
            synchronized (this) {
                if (lookupService != null) {
                    lookupService.close();
                }
                File source = new File(databaseFile);
                File tmp = new File(databaseFile + ".tmp");
                File target = new File(databaseFile + ".old");

                target.delete();

                try {
                    source.renameTo(target);
                    try {
                        tmp.renameTo(source);
                    } catch (Exception e) {
                        log.warn(e);
                        //DB File renamed, but new file can't be copied
                        target.renameTo(source); // So return initia DB file
                    }
                } catch (Exception e) {
                    //Cant copy file
                    log.warn(e);
                } finally {
                    //We close geoLookupService, so we need to reinit it
                    lookupService = new LookupService(databaseFile, options);
                }
            }
        }
    }

    /**
     * download geoIPData from Web
     */
    private boolean downloadFile(String downloadUri, String filePath, String extension) {
        try {
            FileUtils.copyURLToFile(new URL(downloadUri), new File(filePath + "." + extension));
            return decompress(filePath, extension);
        } catch (MalformedURLException e) {
            // bad URL, impossible
            log.warn(e);
        } catch (IOException e) {
            // Some problem, nothing important, file will not be updated
            log.warn(e);
        }
        return false;
    }

    /**
     * decompress downloaded data
     */
    private static boolean decompress(String filePath, String extension) {
        FileInputStream fin = null;
        BufferedInputStream in = null;
        GzipCompressorInputStream gzIn = null;
        try {
            fin = new FileInputStream(filePath + "." + extension);
            in = new BufferedInputStream(fin);
            gzIn = new GzipCompressorInputStream(in);
            FileUtils.copyInputStreamToFile(gzIn, new File(filePath));
        } catch (IOException e) {
            log.debug(e);
            return false;
        } finally {
            IOUtils.closeQuietly(fin);
            IOUtils.closeQuietly(in);
            IOUtils.closeQuietly(gzIn);
            File zip = new File(filePath + "." + extension);
            FileUtils.deleteQuietly(zip);
        }
        return true;
    }

    /**
     * Gets country code by request
     */
    @Override
    public String getCountryCode(String ipAddress) {
        if (ipAddress == null) {
            return null;
        }


        return getCountry(ipAddress).getCode();

    }


    /* (non-Javadoc)
      * @see com.maxmind.geoip.LookupService#close()
      */
    @Override
    public void close() {
        if (lookupService != null) {
            lookupService.close();
            geoDataUpdateTimer.cancel();
        }
    }

    /* (non-Javadoc)
      * @see com.maxmind.geoip.LookupService#getCountry(java.lang.String)
      */
    public Country getCountry(String ipAddress) {
        if (lookupService != null) {
            return lookupService.getCountry(ipAddress);
        } else {
            return null;
        }
    }


}

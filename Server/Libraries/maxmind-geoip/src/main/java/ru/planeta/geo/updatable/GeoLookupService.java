package ru.planeta.geo.updatable;

/**
 * @author делл
 */
public interface GeoLookupService {

    /**
     * Gets country code by request
     *
     * @param ip ip address
     */
    String getCountryCode(String ip);

    void close();
}

package ru.planeta.geo.updatable;

import java.io.IOException;
import java.util.TimerTask;

/**
 * @author a.vovchenko
 * 09.02.2012
 * 18:39:02
 */
public class GeoDataUpdateTask extends TimerTask {
	/**
	 * updateService
	 */	
	private final UpdatableLookupService updateService;
	
	public GeoDataUpdateTask(UpdatableLookupService updateService) {
		super();
		this.updateService = updateService;
	}

	@Override
	public void run() {
		try {
			updateService.updataDatabaseFile();
		} catch (IOException e) {
			// Can't update File
			e.printStackTrace();
		}
	}

}



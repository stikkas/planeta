package ru.planeta.mail;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import ru.planeta.test.AbstractTest;

import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author ds.kolyshev
 * Date: 18.01.12
 */
public class MailServiceTest extends AbstractTest {

	@Autowired
	private MailService mailService;

//    @Test
//    public void sendMail() throws MessagingException {
//        mailService.send("testmail11@dev.planeta.ru", null, "testmail11@mail.ru", "test subject", "test content", new HashMap<String, DataSource>());
//    }

    @Test
    public void testInternetAddress() throws Exception {
        InternetAddress address = new InternetAddress("support@planeta.ru, olga@planeta.ru", "olga");
        address.getAddress();
    }

    @Test
    public void testSplit() {
        String mailTo = "asavan@mail.ru";
        List<String> toList = new ArrayList<String>();
        Collections.addAll(toList, mailTo.split("\\s*,\\s*"));
        System.out.println(toList);
    }

    @Test
    @Ignore
    public void testAmazon() throws MessagingException {
        String mailTo = "michail.michail@gmail.com";
        String mailFrom = "dev@planeta.ru";
        List<String> toList = new ArrayList<>();
        toList.add(mailTo);
        mailService.openConnection();
        String ext_message_id = mailService.send(0,mailFrom,mailFrom,toList,"test","test amazon mail", null);
        mailService.closeConnection();
        Assert.assertNotNull(ext_message_id);
    }

    @Test
    public void testMailServiceConnection() {
        mailService.openConnection();
        Assert.assertTrue(mailService.isConnected());
        mailService.openConnection();
        Assert.assertTrue(mailService.isConnected());
        mailService.closeConnection();
        Assert.assertFalse(mailService.isConnected());
        mailService.closeConnection();
        Assert.assertFalse(mailService.isConnected());
    }
}

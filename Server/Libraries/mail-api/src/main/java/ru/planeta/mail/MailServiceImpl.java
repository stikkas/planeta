package ru.planeta.mail;


import com.sun.mail.smtp.SMTPTransport;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.mail.*;
import javax.mail.internet.*;
import java.io.UnsupportedEncodingException;
import java.util.*;

/**
 * @author ds.kolyshev
 *         Date: 12.01.12
 */
@Service
public class MailServiceImpl implements MailService {

    private static final Logger log = Logger.getLogger(MailServiceImpl.class);
    private String host;
    private int port;
    private String user;
    private String password;
    private String mailSenderName;

    @Value("${mail.host}")
    public void setHost(String host) {
        this.host = host;
    }

    @Value("${mail.port}")
    public void setPort(int port) {
        this.port = port;
    }

    @Value("${mail.user}")
    public void setUser(String user) {
        this.user = user;
    }

    @Value("${mail.password}")
    public void setPassword(String password) {
        this.password = password;
    }

    @Value("${mail.sender.name}")
    public void setMailSenderName(String mailSenderName) {
        this.mailSenderName = mailSenderName;
    }

    private boolean isAuthenticated() {
        return !StringUtils.isEmpty(user) && !StringUtils.isEmpty(password);
    }


    private volatile Session session = null;
    private volatile Transport transport = null;

    @Override
    public synchronized boolean openConnection() {
        Properties props = new Properties();
        props.put("mail.smtp.host", host);
        props.put("mail.smtp.port", port);

        log.info("mail.smtp.host is " + host);

        if (isAuthenticated()) {
            props.put("mail.smtp.user", user);
            props.put("mail.smtp.auth", "true");

            Authenticator authenticator = new SMTPAuthenticator(user, password);
            session = Session.getInstance(props, authenticator);
        } else {
            session = Session.getInstance(props);
        }

        try {
            transport = session.getTransport("smtp");
            if (isAuthenticated()) {
                transport.connect();
            } else {
                transport.connect(host, port, user, password);
            }
            return true;
        } catch (MessagingException ex) {
            log.error(ex);
            return false;
        }
    }

    @Override
    public synchronized void closeConnection() {
        if (transport != null) {
            try {
                transport.close();
                transport = null;
            } catch (MessagingException ex) {
                log.error(ex);
            }
        }
    }

    @Override
    public synchronized boolean isConnected() {
        return transport != null && transport.isConnected();
    }


    @Override
    public String send(long id, String from, String replyTo, List<String> toList, String subject, String contentHtml, Map<String, DataSource> multipartFiles) throws MessagingException {
        String sb = "try to send email" +
                " subject " + subject +
                " from " + from +
                " replyTo " + replyTo +
                " to " + StringUtils.join(toList, "; ");
        logDebug(id, sb);
        return sendInner(id, from, replyTo, toList, subject, contentHtml, multipartFiles);
    }

    private synchronized String sendInner(long id, String from, String replyTo, List<String> toList, String subject, String contentHtml, Map<String, DataSource> multipartFiles) throws MessagingException {
        MimeMultipart multipart = createMimeMultipart(contentHtml, multipartFiles);

        Message message = new MimeMessage(session);

        InternetAddress fromAddress = null;
        try {
            fromAddress = new InternetAddress(from, mailSenderName);
        } catch (Exception ex) {
            logError(id, ex);
        }

        for (String to : toList) {
            InternetAddress toAddress = new InternetAddress(to);
            message.addRecipient(Message.RecipientType.TO, toAddress);
        }
        InternetAddress[] replyToAddress = StringUtils.isEmpty(replyTo) ? null : new InternetAddress[]{new InternetAddress(replyTo)};
        if (replyToAddress != null) {
            message.setReplyTo(replyToAddress);
        }

        message.setFrom(fromAddress);
        message.setSubject(subject);
        message.setContent(multipart);
        message.saveChanges();

        log.info(id + " sending mail message..");

        String ext_message_id = null;
        transport.sendMessage(message, message.getAllRecipients());
        if (transport instanceof SMTPTransport) {
            String resp = ((SMTPTransport) transport).getLastServerResponse();
            log.info(id + " SMTP server response: " + resp);
            ext_message_id = getExtMessageId(resp);
        } else {
            log.error("Transport is not instance of SMTPTransport");
        }
        logDebug(id, "Mail message has been sent");

        return ext_message_id;
    }

    /*
        Amazon SMTP server response example "250 Ok 01020154c88f2469-801e89a5-56b7-4ecf-896a-4821563b05b3-000000\n"
        Need to return 01020154c88f2469-801e89a5-56b7-4ecf-896a-4821563b05b3-000000
    */
    private static String getExtMessageId(String serverResponse) throws ParseException {
        String extMessageId = null;
        final String SMTPSuccessStatus = "250";
        if (serverResponse.startsWith(SMTPSuccessStatus)) {
            extMessageId = serverResponse.substring(serverResponse.lastIndexOf(" ")).trim();
        } else {
            log.warn(serverResponse + " not equals SMTP success status.");
        }

        if (extMessageId == null) {
            log.error("extMessageId is null for SMTP response " + serverResponse);
            throw new ParseException();
        }
        return extMessageId;
    }

    private static void logError(long id, Exception ex) {
        log.error("Mail message send " + id + ". Exception: ", ex);
    }


    private static void logDebug(long id, String message) {
        log.debug("Mail message send " + id + " " + message);
    }

    private static MimeMultipart createMimeMultipart(String contentHtml, Map<String, DataSource> multipartFiles) throws MessagingException {
        // Create a related multi-part to combine the parts
        MimeMultipart multipart = new MimeMultipart("related");
        // Create your new message part
        multipart.addBodyPart(createContentBodyPart(contentHtml));
        for (Map.Entry<String, DataSource> entry : multipartFiles.entrySet()) {
            multipart.addBodyPart(createFileBodyPart(entry.getKey(), entry.getValue()));
        }
        return multipart;
    }

    private static BodyPart createContentBodyPart(String contentHtml) throws MessagingException {
        // Create your new message part
        BodyPart messageBodyPart = new MimeBodyPart();
        messageBodyPart.setContent(contentHtml, "text/html; charset=\"utf-8\"");
        return messageBodyPart;
    }

    private static BodyPart createFileBodyPart(String contentId, DataSource fds) throws MessagingException {

        BodyPart bodyPart = new MimeBodyPart();
        bodyPart.setDataHandler(new DataHandler(fds));
        bodyPart.setDisposition(Part.INLINE);
        bodyPart.setHeader("Content-ID", contentId);
        try {
            bodyPart.setFileName(MimeUtility.encodeText(fds.getName()));
        } catch (UnsupportedEncodingException e) {
            throw new MessagingException("filename cannot be encoded", e);
        }
        return bodyPart;
    }

    private static class SMTPAuthenticator extends Authenticator {

        private final PasswordAuthentication passwordAuthentication;

        SMTPAuthenticator(String username, String password) {
            passwordAuthentication = new PasswordAuthentication(username, password);
        }

        @Override
        protected PasswordAuthentication getPasswordAuthentication() {
            return passwordAuthentication;
        }
    }
}

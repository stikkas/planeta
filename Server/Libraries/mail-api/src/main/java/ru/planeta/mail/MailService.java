package ru.planeta.mail;

import javax.activation.DataSource;
import javax.mail.MessagingException;
import java.util.List;
import java.util.Map;

/**
 * @author ds.kolyshev
 *         Date: 12.01.12
 */
public interface MailService {
    boolean openConnection();

    void closeConnection();

    boolean isConnected();

    String send(long id, String from, String replyTo, List<String> toList, String subject, String contentHtml, Map<String, DataSource> multipartFiles) throws MessagingException;
}

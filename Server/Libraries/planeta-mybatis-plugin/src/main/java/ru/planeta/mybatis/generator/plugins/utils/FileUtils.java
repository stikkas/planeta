package ru.planeta.mybatis.generator.plugins.utils;

import java.io.*;
import java.nio.charset.CharsetEncoder;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

public class FileUtils {

    public static File getTargetDir(String targetProject, String targetPackage) {
        File dir = new File(targetProject);
        StringTokenizer st = new StringTokenizer(targetPackage, ".");
        while (st.hasMoreTokens()) {
            dir = new File(dir, st.nextToken());
        }

        if (!dir.exists() && !dir.mkdirs()) {
            throw new RuntimeException("can't create dir " + dir.getAbsoluteFile());
        }

        return dir;
    }

    public static void writeToFile(File file, List<String> list) {
        if (file == null) return;
        try {
            if (file.exists()) {
                if (!file.delete()) {
                    throw new RuntimeException("can't delete file " + file.getAbsoluteFile());
                }
            }
            if (!file.createNewFile()) {
                throw new RuntimeException("can't create file " + file.getAbsoluteFile());
            }

            Writer out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file)));
            try {
                for (String s : list) {
                    out.write(s);
                    out.write("\r\n");
                }
            } finally {
                out.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static String encodingToAcii(final String txt) {
        final CharsetEncoder asciiEncoder = StandardCharsets.US_ASCII.newEncoder();
        final StringBuilder result = new StringBuilder();
        for (final Character character : txt.toCharArray()) {
            if (asciiEncoder.canEncode(character)) {
                result.append(character);
            } else {
                result.append("\\u");
                result.append(Integer.toHexString(0x10000 | character).substring(1).toUpperCase());
            }
        }
        return result.toString();
    }

    public static List<String> encodingToAcii(final List<String> list) {
        List<String> result = new ArrayList<String>(list.size());
        for (String tx : list) {
            result.add(encodingToAcii(tx));
        }
        return result;
    }

    public static File getFile(File dir, String fullTableName, String extention) {

        StringTokenizer st = new StringTokenizer(fullTableName, ".");
        File file = null;
        while (st.hasMoreTokens()) {
            String s = st.nextToken().toLowerCase();
            if (st.hasMoreTokens()) {
                dir = new File(dir, s);
                if (!dir.exists() && !dir.mkdir()) {
                    throw new RuntimeException("can't create dir " + dir.getAbsoluteFile());
                }
            } else {
                file = new File(dir, s + extention);
            }
        }

        return file;
    }

    public static boolean isFileExists(String targetProject, String targetPackage, String fileName) {
        File file = getFile(targetProject, targetPackage, fileName);
        return file != null && file.exists();

    }

    public static File getFile(String targetProject, String targetPackage, String fileName) {
        File dir = getTargetDir(targetProject, targetPackage);
        File file = new File(dir, fileName);
        return file;
    }


}

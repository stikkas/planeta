package ru.planeta.mybatis.generator.plugins.model;

import org.apache.commons.lang3.StringUtils;
import org.mybatis.generator.api.IntrospectedColumn;
import org.mybatis.generator.api.IntrospectedTable;
import ru.planeta.mybatis.generator.plugins.utils.IntrospectedTableCreator;
import ru.planeta.mybatis.generator.plugins.utils.PluginUtils;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by kostiagn on 15.07.2015.
 */
@XmlRootElement(name = "exTableConfig")
@XmlAccessorType(XmlAccessType.NONE)
public class ExTableConfig {
    @XmlElement(name = "table", required = true)
    private List<ExTable> tableList = new ArrayList<>();

    public List<ExTable> getInheritedTableList(String tableName) {
        List<ExTable> result = new ArrayList<>();
        if (!StringUtils.isBlank(tableName)) {
            for (ExTable table : tableList) {
                if (tableName.equals(table.getParent())) {
                    result.add(table);
                }
            }
        }
        return result;
    }

    public ExTable findTable(String tableName) {
        if (!StringUtils.isBlank(tableName)) {
            for (ExTable table : tableList) {
                if (tableName.equals(table.getName())) {
                    return table;
                }
            }
        }
        return null;
    }

    @XmlAccessorType(XmlAccessType.NONE)
    public static class ExTable {
        @XmlAttribute
        private String name;
        @XmlAttribute
        private String schema;
        @XmlAttribute(name = "extends")
        private String parent;
        @XmlElement(name = "association")
        private List<Association> associationList = new ArrayList<>();
        @XmlElement(name = "collection")
        private List<Collection> collectionList = new ArrayList<>();
        @XmlElement(name = "field")
        private List<Field> fieldList = new ArrayList<>();

        @XmlElement(name = "joinTable")
        private List<JoinTable> joinTableList = new ArrayList<>();

        private String fullTableName;
        private String fullTypeName;
        private String typeName;
        private String mainTypeName;
        private String fullMainTypeName;
        private IntrospectedTable introspectedTable;
        private String baseColumnListName;
        private String resultMapId;

        public String getName() {
            return name;
        }

        public String getSchema() {
            return schema;
        }

        public void setName(String name) {
            this.name = name;
        }

        public void setSchema(String schema) {
            this.schema = schema;
        }

        public String getParent() {
            return parent;
        }

        public List<Association> getAssociationList() {
            return associationList;
        }

        public List<Collection> getCollectionList() {
            return collectionList;
        }

        public List<Relationship> getRelationshipList() {
            List<Relationship> result = new ArrayList<>(getCollectionList().size() + getAssociationList().size());
            result.addAll(getAssociationList());
            result.addAll(getCollectionList());
            return result;
        }

        public List<Field> getFieldList() {
            return fieldList;
        }

        public String getFullTableName() {
            return fullTableName;
        }

        public String getFullTypeName() {
            return fullTypeName;
        }

        public String getTypeName() {
            return typeName;
        }

        public String getMainTypeName() {
            return mainTypeName;
        }

        public String getFullMainTypeName() {
            return fullMainTypeName;
        }

        public IntrospectedTable getIntrospectedTable() {
            return introspectedTable;
        }

        public String getBaseColumnListId() {
            return "Base_Column_List_" + getName();
        }

        public String getResultMapId() {
            return resultMapId;
        }

        public String getJoinTableSqlRefId() {
            return "join_" + getName();
        }

        public List<JoinTable> getJoinTableList() {
            return joinTableList;
        }

        public void setIntrospectedTable(IntrospectedTable introspectedTable) {
            this.introspectedTable = introspectedTable;
            this.introspectedTable = introspectedTable;

            fullTableName = introspectedTable.getTableConfiguration().getSchema() + "." + name;

            typeName = PluginUtils.toCamelCase(name, true);
            fullTypeName = PluginUtils.getTypePackage(introspectedTable.getBaseRecordType()) + "." + typeName;

            fullMainTypeName = introspectedTable.getBaseRecordType();
            mainTypeName = PluginUtils.getTypeName(fullMainTypeName);

            resultMapId = typeName + "ResultMap";

            for (Field field : getFieldList()) {
                final JoinTable joinTable = findJoinTableByAliasSafe(field.getJoinTableAlias());
                if (StringUtils.isBlank(field.type)) {
                    final IntrospectedColumn column = PluginUtils.findColumnSafe(IntrospectedTableCreator.getSafe(introspectedTable.getContext(), joinTable.getTableName()), field.getColumn());
                    field.type = column.getFullyQualifiedJavaType().getShortName();
                }
            }
        }

        public JoinTable findJoinTableByAlias(List<JoinTable> joinTableList, String joinTableAlias) {
            if (StringUtils.isBlank(joinTableAlias)) {
                throw new RuntimeException("joinTableAlias is blank");
            }
            for (JoinTable joinTable : joinTableList) {
                if (joinTableAlias.equals(joinTable.getAlias())) {
                    return joinTable;
                }
                final JoinTable joinTableByAlias = findJoinTableByAlias(joinTable.getJoinTableList(), joinTableAlias);
                if (joinTableByAlias != null) {
                    return joinTableByAlias;
                }
            }
            return null;
        }

        public JoinTable findJoinTableByAlias(String joinTableAlias) {
            return findJoinTableByAlias(getJoinTableList(), joinTableAlias);
        }

        public JoinTable findJoinTableByAliasSafe(String joinTableAlias) {
            final JoinTable result = findJoinTableByAlias(joinTableAlias);
            if (result == null) {
                throw new RuntimeException("cannot found joinTable with alias '" + joinTableAlias + "'");
            }
            return result;
        }
    }


    public abstract static class Relationship {
        @XmlAttribute
        private String name = "";
        @XmlAttribute(required = true)
        private String table = "";
        @XmlAttribute(required = true)
        private String column = "";
        @XmlAttribute(required = true)
        private String selectId = "";
        @XmlAttribute(required = true)
        private String type = "";


        public String getName() {
            return StringUtils.isNotEmpty(name) ? name : table;
        }

        public String getTable() {
            return table;
        }

        public String getColumn() {
            return column;
        }

        public String getSelectId() {
            return selectId;
        }

        public String getType() {
            return type;
        }

        public abstract String getRelationshipName();

        public String getFullTableName() {
            return table;
        }

        public String getTableName() {
            return PluginUtils.extractTableName(table);
        }


    }

    @XmlAccessorType(XmlAccessType.FIELD)
    public static class Association extends Relationship {
        public String getRelationshipName() {
            return "association";
        }
    }

    @XmlAccessorType(XmlAccessType.FIELD)
    public static class Collection extends Relationship {
        @XmlAttribute
        private String orderBy;

        public String getOrderBy() {
            return orderBy;
        }

        public String getRelationshipName() {
            return "collection";
        }
    }

    @XmlAccessorType(XmlAccessType.FIELD)
    public static class Field {
        @XmlAttribute
        private String name;
        @XmlAttribute
        private String type;
        @XmlAttribute
        private String joinTableAlias;
        @XmlAttribute(required = true)
        private String column;


        public String getName() {
            if (name == null) {
                name = PluginUtils.toCamelCase(column, false);
            }
            return name;
        }

        public String getColumn() {
            return column;
        }

        public String getType() {
            return type;
        }

        public String getColumnAlias() {
            return joinTableAlias + "_" + (isCount() ? "count" : column);
        }

        public String getJoinTableAlias() {
            return joinTableAlias;
        }

        public String getFullColumnName() {
            return joinTableAlias + "." + column;
        }

        public boolean isCount() {
            return type.equals("count");
        }

        public String getJavaType() {
            return isCount() ? "int" : type;
        }
    }

    @XmlAccessorType(XmlAccessType.FIELD)
    public static class JoinTable {
        @XmlAttribute
        private String tableName;
        @XmlAttribute
        private boolean leftJoin;
        @XmlAttribute
        private String joinOn;

        @XmlElement(name = "joinTable")
        private List<JoinTable> joinTableList = new ArrayList<>();
        private String alias;


        public List<JoinTable> getJoinTableList() {
            return joinTableList;
        }

        public String getTableName() {
            return tableName;
        }

        public boolean isLeftJoin() {
            return leftJoin;
        }

        public String getJoinOn() {
            return joinOn;
        }

        public String getAlias() {
            if (alias == null) {
                alias = PluginUtils.extractTableName(getTableName());
            }
            return alias;
        }
    }
}

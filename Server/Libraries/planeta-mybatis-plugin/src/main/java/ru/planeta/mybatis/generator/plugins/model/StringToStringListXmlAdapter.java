package ru.planeta.mybatis.generator.plugins.model;

import org.apache.commons.lang3.StringUtils;

import javax.xml.bind.annotation.adapters.XmlAdapter;
import java.util.Arrays;
import java.util.List;

/**
 * Created by kostiagn on 15.07.2015.
 */
public class StringToStringListXmlAdapter extends XmlAdapter<String, List<String>> {

    public String marshal(List<String> list) {
        return StringUtils.join(list, ',');
    }

    public List<String> unmarshal(String val) {
        if (val == null) {
            return null;
        }
        return Arrays.asList(val.split(","));
    }
}
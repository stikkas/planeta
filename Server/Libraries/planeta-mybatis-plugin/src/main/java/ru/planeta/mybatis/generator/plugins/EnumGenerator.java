package ru.planeta.mybatis.generator.plugins;

import org.apache.commons.lang3.StringUtils;
import ru.planeta.mybatis.generator.plugins.annotation.Property;
import ru.planeta.mybatis.generator.plugins.utils.FileUtils;
import org.mybatis.generator.api.GeneratedJavaFile;
import org.mybatis.generator.api.IntrospectedColumn;
import org.mybatis.generator.api.IntrospectedTable;
import org.mybatis.generator.api.dom.java.*;
import org.mybatis.generator.config.ColumnOverride;
import org.mybatis.generator.internal.db.ConnectionFactory;
import ru.planeta.mybatis.generator.plugins.utils.PluginUtils;

import java.io.File;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.*;

import static ru.planeta.mybatis.generator.plugins.utils.FileUtils.*;


public class EnumGenerator extends MyPluginAdapter {
    @Property
    private String targetPackage;
    @Property
    private String targetProject;
    @Property
    private String typeHandlerTargetPackage;
    @Property
    private String typeHandlerTargetProject;
    @Property
    private String messagePropertiesTargetPackage;
    @Property
    private String messagePropertiesFileName;
    @Property
    private String messagePropertiesTargetProject;
    @Property
    private String superInterface;

    private File targetDir;
    private List<String> tableNames = new ArrayList<String>();
    private Map<String, String> enumColumns = new HashMap<>();
    private Connection connection;
    private List<String> properties = new ArrayList<>();
    private Map<String, Map<String, List<String>>> transitions = new HashMap<>();


    protected void init() {
        super.init();

        final Properties properties = getProperties();
        for (String tableName : properties.getProperty("tables", "").split(",")) {
            tableName = tableName.trim();
            if (!tableName.isEmpty()) {
                tableNames.add(tableName);
                enumColumns.put(PluginUtils.extractTableName(tableName) + "_id", PluginUtils.toCamelCase(PluginUtils.extractTableName(tableName), true));
            }
        }

        for (IntrospectedTable it : PluginUtils.getIntrospectedTables(this.getContext())) {
            for (IntrospectedColumn cl : it.getAllColumns()) {
                if (enumColumns.containsKey(cl.getActualColumnName())) {
                    ColumnOverride columnOverride = new ColumnOverride(cl.getActualColumnName());
                    columnOverride.setJavaProperty(PluginUtils.toObjectName(enumColumns.get(cl.getActualColumnName())));
                    cl.setJavaProperty(columnOverride.getJavaProperty());
                    columnOverride.setJavaType(targetPackage + "." + PluginUtils.toClassName(cl.getJavaProperty()));
                    cl.setFullyQualifiedJavaType(new FullyQualifiedJavaType(columnOverride.getJavaType()));
                    cl.setTypeHandler(getTypeHandlerName(cl.getJavaProperty()));
                }
            }
        }

        for (String propName : properties.stringPropertyNames()) {
            if (propName.startsWith("transitions ")) {
                String[] words = propName.substring("transitions ".length()).split(" ");
                if (words.length != 2) {
                    continue;
                }
                String tableName = words[0];
                String val = words[1];

                Map<String, List<String>> valMap = transitions.get(tableName);
                if (valMap == null) {
                    valMap = new HashMap<>();
                    transitions.put(tableName, valMap);
                }
                valMap.put(val, Arrays.asList(properties.getProperty(propName, "").split(" ")));
            }
        }

    }


    @Override
    public List<GeneratedJavaFile> contextGenerateAdditionalJavaFiles() {
        List<GeneratedJavaFile> result = new ArrayList<GeneratedJavaFile>();
        try {
            connection = ConnectionFactory.getInstance().getConnection(this.context.getJdbcConnectionConfiguration());
            for (String tableName : tableNames) {
                result.add(generateEnumFile(tableName));
                result.add(generateTypeHandlerFile(PluginUtils.extractTableName(tableName)));
            }
            writeToPropertiesFile(messagePropertiesFileName, properties);

        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            connection = null;
        }

        return result;
    }


    private String toDotted(String name) {
        return name.toLowerCase().replaceAll("_", ".");
    }

    private String getCode(String tableName, String val, String suf) {
        return "enum." + toDotted(tableName) + "." + toDotted(val) + "." + suf;
    }

    private void addPrivateFieldAndGetter(TopLevelEnumeration enumeration, String fieldName, FullyQualifiedJavaType type) {
        Field field = new Field(fieldName, type);
        field.setVisibility(JavaVisibility.PRIVATE);
        enumeration.addField(field);


        Method method = new Method("get" + PluginUtils.toClassName(fieldName));
        method.setVisibility(JavaVisibility.PUBLIC);
        method.setReturnType(type);
        method.addBodyLine("return " + fieldName + ";");
        enumeration.addMethod(method);
    }

    private String getPrimaryKey(String tableName) throws SQLException {
        ResultSet rs = connection.createStatement().executeQuery("SELECT               \n" +
                "  pg_attribute.attname, \n" +
                "  format_type(pg_attribute.atttypid, pg_attribute.atttypmod) \n" +
                "FROM pg_index, pg_class, pg_attribute \n" +
                "WHERE \n" +
                "  pg_class.oid = '" + tableName + "'::regclass AND\n" +
                "  indrelid = pg_class.oid AND\n" +
                "  pg_attribute.attrelid = pg_class.oid AND \n" +
                "  pg_attribute.attnum = any(pg_index.indkey)\n" +
                "  AND indisprimary");
        StringBuilder sb = new StringBuilder(" ");
        while (rs.next()) {
            sb.append(rs.getString(1)).append(",");
        }

        sb.setLength(sb.length() - 1);
        return sb.toString();
    }

    private GeneratedJavaFile generateEnumFile(String fullTableName) throws SQLException {
        String tableName = PluginUtils.extractTableName(fullTableName);
        final FullyQualifiedJavaType enumType = new FullyQualifiedJavaType(targetPackage + "." + PluginUtils.toCamelCase(tableName, true));
        TopLevelEnumeration enumeration = new TopLevelEnumeration(enumType);
        enumeration.setVisibility(JavaVisibility.PUBLIC);
        if (StringUtils.isNotBlank(superInterface)) {
            FullyQualifiedJavaType superInterfaceType = new FullyQualifiedJavaType(superInterface);
            enumeration.addImportedType(superInterfaceType);
            enumeration.addSuperInterface(superInterfaceType);
        }


        String pk = getPrimaryKey(fullTableName);
        if (pk.isEmpty()) {
            throw new RuntimeException("table " + fullTableName + " has not primary key");
        }
        properties.add("# " + tableName);
        ResultSet rs = connection.createStatement().executeQuery("select * from " + fullTableName + " order by " + pk);
        checkColumnNames(tableName, rs.getMetaData());
        while (rs.next()) {
            int enumCode = rs.getInt(getColumnIdName(tableName));
            String enumName = rs.getString(getColumnNameName(tableName)).toUpperCase();
            final String descriptionMessageCode = getCode(tableName, enumName, "description");
            final String fullDescriptionMessageCode = getCode(tableName, enumName, "full.description");
            enumeration.addEnumConstant(enumName + String.format("(%d, \"%s\", \"%s\")", enumCode, descriptionMessageCode, fullDescriptionMessageCode));

            properties.add(descriptionMessageCode + "=" + rs.getString("description"));
            properties.add(fullDescriptionMessageCode + "=" + rs.getString("full_description"));
        }
        properties.add("");


        addPrivateFieldAndGetter(enumeration, "code", FullyQualifiedJavaType.getIntInstance());
        addPrivateFieldAndGetter(enumeration, "descriptionMessageCode", FullyQualifiedJavaType.getStringInstance());
        addPrivateFieldAndGetter(enumeration, "fullDescriptionMessageCode", FullyQualifiedJavaType.getStringInstance());

        enumeration.addMethod(getConstructor(PluginUtils.toCamelCase(tableName, true), enumType));

        Method method = PluginUtils.createMethod("getName", "public String");
        method.addBodyLine("return name();");
        enumeration.addMethod(method);

        addCheckTransition(fullTableName, enumeration, enumType);
        addParseMethod(fullTableName, enumeration);

        addLookup(fullTableName, enumeration);

        String targetProject = this.getContext().getJavaClientGeneratorConfiguration().getTargetProject();

        return new GeneratedJavaFile(enumeration, targetProject, this.getContext().getJavaFormatter());
    }

    private void checkColumnNames(String tableName, ResultSetMetaData metaData) throws SQLException {
        boolean isExistsColumnId = false;
        boolean isExistsColumnName = false;
        boolean isExistsColumnDescription = false;
        boolean isExistsColumnFullDescription = false;

        String columnIdName = getColumnIdName(tableName);
        String columnNameName = getColumnNameName(tableName);
        for (int i = 1; i <= metaData.getColumnCount(); i++) {
            final String name = metaData.getColumnName(i);
            if ("full_description".equals(name)) {
                isExistsColumnFullDescription = true;
            } else if ("description".equals(name)) {
                isExistsColumnDescription = true;
            } else if (columnIdName.equals(name)) {
                isExistsColumnId = true;
            } else if (columnNameName.equals(name)) {
                isExistsColumnName = true;
            }
        }

        throwExceptionIfNotExists(isExistsColumnId, tableName, columnIdName);
        throwExceptionIfNotExists(isExistsColumnName, tableName, columnNameName);
        throwExceptionIfNotExists(isExistsColumnDescription, tableName, "description");
        throwExceptionIfNotExists(isExistsColumnFullDescription, tableName, "full_description");
    }

    private String getColumnIdName(String tableName) {
        return tableName + "_id";
    }

    private String getColumnNameName(String tableName) {
        return tableName + "_name";
    }

    private void throwExceptionIfNotExists(boolean isExists, String tableName, String columnName) throws SQLException {
        if (!isExists) {
            throw new RuntimeException(String.format("EnumGenerator: not found column %s for table %s", columnName, tableName));
        }
    }

    private void addLookup(String fullTableName, TopLevelEnumeration enumeration) {
        String type = PluginUtils.toCamelCase(PluginUtils.extractTableName(fullTableName), true);

        //private static final Map<Integer, ProfileType> lookup = new HashMap<Integer, ProfileType>();
        Field field = new Field("lookup", new FullyQualifiedJavaType("Map<Integer, " + type + ">"));
        field.setVisibility(JavaVisibility.PRIVATE);
        field.setFinal(true);
        field.setStatic(true);
        field.setInitializationString("new HashMap<>()");
        enumeration.addField(field);
        enumeration.addImportedType(new FullyQualifiedJavaType("java.util.Map"));
        enumeration.addImportedType(new FullyQualifiedJavaType("java.util.HashMap"));

//        public static ProfileType getByValue(int code) {
//              if (lookup == null){
//                  lookup = new HashMap<>();
//                  for (EnumType s : EnumSet.allOf(EnumType.class)) {
//                     lookup.put(s.getCode(), s);
//                }
//              }
//            return lookup.get(code);
//        }
        Method method = PluginUtils.createMethod("getByValue", new FullyQualifiedJavaType(type), "public static", "int code");

        method.addBodyLine("if (lookup.size() == 0){");
        method.addBodyLine("for (" + type + " s : EnumSet.allOf(" + type + ".class)) {");
        method.addBodyLine("lookup.put(s.getCode(), s);");
        method.addBodyLine("}");
        method.addBodyLine("}");
        method.addBodyLine("return lookup.get(code);");
        enumeration.addImportedType(new FullyQualifiedJavaType("java.util.EnumSet"));

        enumeration.addMethod(method);
    }

    private void addParseMethod(String fullTableName, TopLevelEnumeration enumeration) {
        String type = PluginUtils.toCamelCase(PluginUtils.extractTableName(fullTableName), true);
        Method method = PluginUtils.createMethod("parse", new FullyQualifiedJavaType(type), "public static", "String s");
        method.addBodyLine("if (s == null || s.isEmpty()) return null;");
        method.addBodyLine("try {");
        method.addBodyLine("int i = Integer.parseInt(s);");
        method.addBodyLine("if (i < values().length) return values()[i];");
        method.addBodyLine("} catch (NumberFormatException e) {");
        method.addBodyLine("}");
        method.addBodyLine("return null;");
        enumeration.addMethod(method);
    }


    private void addCheckTransition(String fullTableName, TopLevelEnumeration enumeration, FullyQualifiedJavaType enumType) {
        Map<String, List<String>> transitionMap = transitions.get(fullTableName);
        if (transitionMap == null) {
            return;
        }
        Field field = new Field("transitions", new FullyQualifiedJavaType("java.util.Map<" + enumType.getShortName() + ", List<" + enumType.getShortName() + ">>"));
        field.setVisibility(JavaVisibility.PUBLIC);
        field.setStatic(true);

        enumeration.addImportedType(new FullyQualifiedJavaType("java.util.*"));

        StringBuilder sb = new StringBuilder();
        sb.append("new HashMap<>();\nstatic{\n");
        for (String key : transitionMap.keySet()) {
            sb.append("transitions.put(").append(key.toUpperCase()).append(", new ArrayList<" + enumType.getShortName() + ">() {{");
            for (String val : transitionMap.get(key)) {
                sb.append("add(").append(val.toUpperCase()).append(");");
            }
            sb.append("}});\n");
        }
        sb.append("\n}");
        field.setInitializationString(sb.toString());
        enumeration.addField(field);

        String paramType = PluginUtils.toCamelCase(PluginUtils.extractTableName(fullTableName), true);
        Method method = PluginUtils.createMethod("checkTransition", "public boolean", paramType + " that");
        method.addBodyLine("List<" + paramType + "> list = transitions.get(this);");
        method.addBodyLine("return list != null && list.contains(that);");
        enumeration.addMethod(method);
    }

    private void writeToPropertiesFile(String fileName, List<String> properties) {
        writeToFile(getPropertiesFile(fileName), encodingToAcii(properties));
    }

    private File getTargetDir() {
        if (this.targetDir == null) {
            this.targetDir = FileUtils.getTargetDir(messagePropertiesTargetProject, messagePropertiesTargetPackage);
        }

        return targetDir;
    }

    private File getPropertiesFile(String fileName) {
        return new File(getTargetDir(), fileName);
    }

    private Method getConstructor(String name, FullyQualifiedJavaType enumType) {
        Method constructor = new Method(name);
        constructor.setConstructor(true);
        constructor.setVisibility(JavaVisibility.PRIVATE);
        constructor.setReturnType(enumType);
        constructor.addParameter(new Parameter(FullyQualifiedJavaType.getIntInstance(), "code"));
        constructor.addParameter(new Parameter(FullyQualifiedJavaType.getStringInstance(), "descriptionMessageCode"));
        constructor.addParameter(new Parameter(FullyQualifiedJavaType.getStringInstance(), "fullDescriptionMessageCode"));
        constructor.addBodyLine("this.code = code;");
        constructor.addBodyLine("this.descriptionMessageCode = descriptionMessageCode;");
        constructor.addBodyLine("this.fullDescriptionMessageCode = fullDescriptionMessageCode;");

        return constructor;
    }


    private String getTypeHandlerName(String name) {
        return typeHandlerTargetPackage + '.' + PluginUtils.toClassName(name) + "TypeHandler";
    }

    private GeneratedJavaFile generateTypeHandlerFile(String tableName) {
        String typeName = PluginUtils.toCamelCase(tableName, true);
        FullyQualifiedJavaType type = new FullyQualifiedJavaType(targetPackage + "." + typeName);
        TopLevelClass topLevelClass = new TopLevelClass(new FullyQualifiedJavaType(getTypeHandlerName(typeName)));
        topLevelClass.setVisibility(JavaVisibility.PUBLIC);

        topLevelClass.addSuperInterface(new FullyQualifiedJavaType("org.apache.ibatis.type.TypeHandler<" + typeName + ">"));
        topLevelClass.addImportedType(type);

        topLevelClass.addAnnotation("@MappedTypes({" + typeName + ".class})");
        topLevelClass.addImportedType("org.apache.ibatis.type.MappedTypes");

        FullyQualifiedJavaType sqlExceptionType = new FullyQualifiedJavaType("java.sql.SQLException");
        Method method = PluginUtils.createMethod("setParameter", "@Override public", "java.sql.PreparedStatement ps", "int i", typeName + " parameter", "org.apache.ibatis.type.JdbcType jdbcType");
        method.addException(sqlExceptionType);
        method.addBodyLine("ps.setInt(i, parameter.getCode());");
        topLevelClass.addMethod(method);

        method = PluginUtils.createMethod("getResult", type, "@Override public", "java.sql.ResultSet rs", "String columnName");
        method.addException(sqlExceptionType);
        method.addBodyLine("return rs.getObject(columnName) == null ? null : " + typeName + ".getByValue(rs.getInt(columnName));");
        topLevelClass.addMethod(method);

        method = PluginUtils.createMethod("getResult", type, "@Override public", "java.sql.ResultSet rs", "int columnIndex");
        method.addException(sqlExceptionType);
        method.addBodyLine("return rs.getObject(columnIndex) == null ? null : " + typeName + ".getByValue(rs.getInt(columnIndex));");
        topLevelClass.addMethod(method);


        method = PluginUtils.createMethod("getResult", type, "@Override public", "java.sql.CallableStatement cs", "int columnIndex");
        method.addException(sqlExceptionType);
        method.addBodyLine("return cs.getObject(columnIndex) == null ? null : " + typeName + ".getByValue(cs.getInt(columnIndex));");
        topLevelClass.addMethod(method);

        PluginUtils.addImportedType(topLevelClass);

        return PluginUtils.createGeneratedJavaFile(topLevelClass, this.getContext());
    }
}

package ru.planeta.mybatis.generator.plugins.enums;

import ru.planeta.mybatis.generator.plugins.utils.PluginUtils;

import javax.xml.bind.annotation.adapters.XmlAdapter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by kostiagn on 23.06.2015.
 */
public enum ComparisonOperator {
    EQUAL("=", "EQ"),
    NOT_EQUAL("<>", "!=", "NE"),
    GREATER_THEN(">", "GT"),
    LESS_THEN("<", "LT"),
    GREATER_THEN_OR_EQUAL_TO(">=", "GE"),
    LESS_THEN_OR_EQUAL_TO(">=", "LE"),
    BETWEEN,
    LIKE, ILIKE, IN, CONTAINS, ENDS_WITH, STARTS_WITH;

    private List<String> aliasList = new ArrayList<>();
    private String sqlOperator;

    ComparisonOperator(String... strings) {
        aliasList = Arrays.asList(strings);
        if (aliasList.size() > 0) {
            sqlOperator = aliasList.get(0);
        }
    }

    public String getSqlOperator() {
        if (sqlOperator == null) {
            sqlOperator = PluginUtils.toCamelCase(name(), false);
        }
        return sqlOperator;
    }

    public static class ComparisonOperatorAdapter extends XmlAdapter<String, ComparisonOperator> {

        public String marshal(ComparisonOperator comparisonOperation) {
            return comparisonOperation.aliasList.get(0);
        }

        public ComparisonOperator unmarshal(String val) {
            return ComparisonOperator.fromValue(val);
        }
    }

    public static ComparisonOperator fromValue(String v) {
        for (ComparisonOperator op : ComparisonOperator.values()) {
            if (op.name().equalsIgnoreCase(v)) {
                return op;
            }
            for (String alias : op.aliasList) {
                if (alias.equalsIgnoreCase(v)) {
                    return op;
                }
            }
        }
        return valueOf(v);
    }

}

package ru.planeta.mybatis.generator.plugins;

import org.mybatis.generator.api.IntrospectedTable;
import org.mybatis.generator.api.dom.xml.Element;
import org.mybatis.generator.api.dom.xml.TextElement;
import org.mybatis.generator.api.dom.xml.XmlElement;
import org.mybatis.generator.internal.db.ConnectionFactory;
import ru.planeta.mybatis.generator.plugins.utils.PluginUtils;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

/**
 * Created by kostiagn on 15.01.2016.
 */
public class AddJsonbCastInSelectAndUpdate extends MyPluginAdapter {
    private Connection connection;

    public void init() {
        super.init();
        try {
            connection = ConnectionFactory.getInstance().getConnection(this.context.getJdbcConnectionConfiguration());
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }


    private Map<IntrospectedTable, List<String>> jsonbColumnMap = new HashMap<>();

    @Override
    public void initialized(IntrospectedTable introspectedTable) {
        List<String> columnList = new ArrayList<>();
        String query = "select column_name " +
                "from information_schema.columns " +
                "where table_name = '" + introspectedTable.getTableConfiguration().getTableName() + "' " +
                "  and table_schema = '" + introspectedTable.getTableConfiguration().getSchema() + "' " +
                "  and data_type = 'jsonb';";
        try {
            ResultSet rs = connection.createStatement().executeQuery(query);

            while (rs.next()) {
                columnList.add(rs.getString(1));
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        if (columnList.size() > 0) {
            jsonbColumnMap.put(introspectedTable, columnList);
        }

    }

    @Override
    public boolean sqlMapInsertElementGenerated(XmlElement element, IntrospectedTable introspectedTable) {
        replace(element, introspectedTable);
        return true;
    }


    @Override
    public boolean sqlMapInsertSelectiveElementGenerated(XmlElement element, IntrospectedTable introspectedTable) {
        replace(element, introspectedTable);
        return true;
    }

    public boolean sqlMapUpdateByExampleSelectiveElementGenerated(XmlElement element, IntrospectedTable introspectedTable) {
        replace(element, introspectedTable);
        return true;
    }


    public boolean sqlMapUpdateByPrimaryKeySelectiveElementGenerated(XmlElement element, IntrospectedTable introspectedTable) {
        replace(element, introspectedTable);
        return true;
    }

    private void replace(XmlElement element, IntrospectedTable introspectedTable) {
        if (!jsonbColumnMap.containsKey(introspectedTable)) {
            return;
        }

        final List<String> columnList = jsonbColumnMap.get(introspectedTable);


        List<Element> elements = element.getElements();
        for (int i = 0; i < elements.size(); i++) {
            Element el = elements.get(i);
            if (el instanceof TextElement) {
                TextElement textElement = (TextElement) el;
                for (String columnName : columnList) {
                    if (textElement.getContent().contains("#{" + columnName)) {
                        element.getElements().remove(i);
                        element.getElements().add(i, new TextElement(textElement.getContent().replaceAll("(#\\{" + columnName + "\\b[^}]*\\})", "$1::jsonb")));
                    }
                }
            } else if (el instanceof XmlElement) {
                replace((XmlElement) el, introspectedTable);
            }
        }

    }
}

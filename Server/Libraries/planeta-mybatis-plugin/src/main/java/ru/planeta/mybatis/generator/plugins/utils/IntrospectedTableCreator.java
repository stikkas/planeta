package ru.planeta.mybatis.generator.plugins.utils;

import org.mybatis.generator.api.IntrospectedTable;
import org.mybatis.generator.api.JavaTypeResolver;
import org.mybatis.generator.config.Context;
import org.mybatis.generator.config.TableConfiguration;
import org.mybatis.generator.internal.ObjectFactory;
import org.mybatis.generator.internal.db.ConnectionFactory;
import org.mybatis.generator.internal.db.DatabaseIntrospector;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by kostiagn on 15.07.2015.
 */
public class IntrospectedTableCreator {
    private static Map<String, IntrospectedTable> introspectedTableMap = new HashMap<>();

    public static IntrospectedTable get(Context context, String fullTableName) {
        IntrospectedTable introspectedTable = introspectedTableMap.get(fullTableName);
        if (introspectedTable == null) {
            introspectedTable = create(context, fullTableName);
            introspectedTableMap.put(fullTableName, introspectedTable);
        }
        return introspectedTable;
    }

    public static IntrospectedTable getSafe(Context context, String fullTableName) {
        return getSafe(context, fullTableName, "");
    }

    public static IntrospectedTable getSafe(Context context, String fullTableName, String errorMsg) {
        final IntrospectedTable introspectedTable = get(context, fullTableName);
        if (introspectedTable == null) {
            throw new RuntimeException("cannot get table '" + fullTableName + "' from db. " + errorMsg);
        }

        return introspectedTable;
    }

    public static IntrospectedTable get(Context context, String schema, String tableName) {
        return get(context, schema + "." + tableName);
    }

    public static IntrospectedTable create(Context context, String fullTableName) {
        return IntrospectedTableCreator.create(context, PluginUtils.extractSchemaName(fullTableName), PluginUtils.extractTableName(fullTableName));
    }

    public static IntrospectedTable create(Context context, String schema, String tableName) {

        try {
            TableConfiguration tc = new TableConfiguration(context);

            tc.setTableName(tableName);
            tc.setSchema(schema);
            tc.setAlias(tableName);

            List<String> warnings = new ArrayList<>();
            JavaTypeResolver javaTypeResolver = ObjectFactory.createJavaTypeResolver(context, warnings);
            Connection connection = ConnectionFactory.getInstance().getConnection(context.getJdbcConnectionConfiguration());

            DatabaseIntrospector databaseIntrospector = new DatabaseIntrospector(context, connection.getMetaData(), javaTypeResolver, warnings);
            List<IntrospectedTable> tables = databaseIntrospector.introspectTables(tc);
            if (tables.size() > 0) {
                return tables.get(0);
            }
        } catch (Exception e) {
            throw new RuntimeException(String.format("Cannot create IntrospectedTable for table %s.%s", schema, tableName), e);
        }
        return null;

    }
}

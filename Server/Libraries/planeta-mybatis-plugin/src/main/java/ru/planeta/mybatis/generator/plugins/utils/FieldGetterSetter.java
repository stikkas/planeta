package ru.planeta.mybatis.generator.plugins.utils;

import org.mybatis.generator.api.dom.java.*;

import static ru.planeta.mybatis.generator.plugins.utils.PluginUtils.createMethod;
import static ru.planeta.mybatis.generator.plugins.utils.PluginUtils.toClassName;
import static ru.planeta.mybatis.generator.plugins.utils.PluginUtils.toObjectName;

/**
 * Created by kostiagn on 15.07.2015.
 */
public class FieldGetterSetter {
    private Field field;
    private Method setter;
    private Method getter;

    public FieldGetterSetter(Field field, Method setter, Method getter) {
        this.field = field;
        this.setter = setter;
        this.getter = getter;
    }

    public static FieldGetterSetter create(FullyQualifiedJavaType type, String fieldName) {
        Field field = new Field(fieldName, type);
        field.setVisibility(JavaVisibility.PRIVATE);
        return new FieldGetterSetter(field, createGetter(type, fieldName), createSetter(type, fieldName));
    }

    public static FieldGetterSetter addToClass(TopLevelClass topLevelClass, FullyQualifiedJavaType type, String fieldName) {
        FieldGetterSetter fieldGetterSetter = create(type, fieldName);
        topLevelClass.addField(fieldGetterSetter.field);
        topLevelClass.addMethod(fieldGetterSetter.getter);
        topLevelClass.addMethod(fieldGetterSetter.setter);
        topLevelClass.addImportedType(type);
        return fieldGetterSetter;
    }

    public static Method createGetter(FullyQualifiedJavaType type) {
        return createGetter(type, toObjectName(type.getShortName()));
    }

    public static Method createGetter(FullyQualifiedJavaType type, String fieldName) {
        Method method = createMethod("get" + toClassName(fieldName), type, "public");
        method.setReturnType(type);
        method.addBodyLine("return " + fieldName + ";");
        return method;
    }


    public static Method createSetter(FullyQualifiedJavaType type) {
        return createSetter(type, toObjectName(type.getShortName()));
    }

    public static Method createSetter(FullyQualifiedJavaType type, String fieldName) {
        Method method = createMethod("set" + toClassName(fieldName), "public");
        method.addParameter(new Parameter(type, fieldName));
        method.addBodyLine("this." + fieldName + " = " + fieldName + ";");
        return method;
    }

    public Field getField() {
        return field;
    }

    public Method getGetter() {
        return getter;
    }

    public void setGetter(Method getter) {
        this.getter = getter;
    }
}

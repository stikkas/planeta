package ru.planeta.mybatis.generator.plugins;

import org.mybatis.generator.api.GeneratedJavaFile;
import org.mybatis.generator.api.IntrospectedTable;
import org.mybatis.generator.api.dom.java.*;
import ru.planeta.mybatis.generator.plugins.utils.PluginUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static ru.planeta.mybatis.generator.plugins.utils.PluginUtils.getTypeName;

public class AddMapperImpl extends MyPluginAdapter {
    private Map<IntrospectedTable, Interface> interfaceMap = new HashMap<IntrospectedTable, Interface>();

    @Override
    public boolean clientGenerated(Interface interfaze, TopLevelClass topLevelClass, IntrospectedTable introspectedTable) {
        interfaceMap.put(introspectedTable, interfaze);
        return true;
    }

    @Override
    public List<GeneratedJavaFile> contextGenerateAdditionalJavaFiles(IntrospectedTable introspectedTable) {
        List<GeneratedJavaFile> result = new ArrayList<>();
        GeneratedJavaFile generatedJavaFile = createMapperImpl(introspectedTable);
        if (generatedJavaFile != null) {
            result.add(generatedJavaFile);
        }
        return result;
    }

    private GeneratedJavaFile createMapperImpl(IntrospectedTable introspectedTable) {
        //if(!introspectedTable.getTableConfiguration().getTableName().equals("payment_errors")){
        //    return null;
        //}
        String fullTypeName = introspectedTable.getBaseRecordType();
        String typeName = getTypeName(fullTypeName);
        Interface interfaze = interfaceMap.get(introspectedTable);

        TopLevelClass clazz = new TopLevelClass(interfaze.getType().toString() + "Impl");

        clazz.addImportedTypes(interfaze.getImportedTypes());

        clazz.addSuperInterface(interfaze.getType());

        FullyQualifiedJavaType superClassType;
        if (introspectedTable.getTableConfiguration().getSchema().equals("commondb")) {
            superClassType = new FullyQualifiedJavaType("ru.planeta.dao.commondb.BaseCommonDbDAO"); //<" + typeName + ">");
        } else if (introspectedTable.getTableConfiguration().getSchema().equals("profiledb")) {
            superClassType = new FullyQualifiedJavaType("ru.planeta.eva.dao.BaseClusterDAO");
        } else if (introspectedTable.getTableConfiguration().getSchema().equals("charitydb")) {
            superClassType = new FullyQualifiedJavaType("ru.planeta.dao.charitydb.BaseCharityDbDAO");
        } else {
            throw new RuntimeException("You need to add code to the scheme " + introspectedTable.getTableConfiguration().getSchema());
        }
        clazz.setSuperClass(superClassType);
        clazz.addImportedType(superClassType);
        clazz.addImportedType(FullyQualifiedJavaType.getNewMapInstance());
        clazz.addAnnotation("@Repository");
        clazz.addImportedType(new FullyQualifiedJavaType("org.springframework.stereotype.Repository"));


        for (Method method : interfaze.getMethods()) {
            Method m = new Method(method);
            m.setVisibility(JavaVisibility.PUBLIC);
            String sqlId = "\"" + introspectedTable.getMyBatis3SqlMapNamespace() + "." + m.getName() + "\"";

            if (m.getName().startsWith("insert")) {
                m.addBodyLine("return insert(" + sqlId + ", record);");
            } else if (m.getName().startsWith("update")) {
                if (m.getParameters().size() == 1 && m.getName().contains("ByPrimaryKey")) {
                    m.addBodyLine("return update(" + sqlId + ", record);");
                } else {
                    m.addBodyLines(getParamList(m, true));
                    m.addBodyLine("return update(" + sqlId + ", params);");
                }
            } else if (m.getName().startsWith("delete")) {
                List<String> paramList = getParamList(m, false);
                if (paramList.size() > 1) {
                    m.addBodyLines(paramList);
                    m.addBodyLine("return delete(" + sqlId + ", params);");
                } else {
                    m.addBodyLine("return delete(" + sqlId + ", " + m.getParameters().get(0).getName() + ");");
                }
            } else if (m.getName().equals("countByExample")) {
                m.addBodyLines(getParamList(m, false));
                m.addBodyLine("return (Integer) selectOne(" + sqlId + ", example);");
                //return (Integer) selectOne(Statements.CampaignContact.GET_CAMPAIGN_COUNT, getParameters("campaignId", campaignId));
            } else {
                String selectMethod = m.getReturnType().toString().startsWith("java.util.List") || m.getReturnType().toString().startsWith("List") ? "selectList" : "(" + m.getReturnType().getShortName() + ") selectOne";
                if (getParamListSize(m,false) > 1) {
                    m.addBodyLines(getParamList(m, false));
                    m.addBodyLine("return " + selectMethod + "(" + sqlId + ", params);");
                } else {
                    m.addBodyLine("return " + selectMethod + "(" + sqlId + ", " + m.getParameters().get(0).getName() + (isExistsRowBoundsParam(m) ? ", rowBounds" : "") + ");");
                }
            }
            clazz.addMethod(m);
        }
        clazz.setVisibility(JavaVisibility.PUBLIC);
        return PluginUtils.createGeneratedJavaFile(clazz, this.getContext());
    }

    private boolean isExistsRowBoundsParam(Method m) {
        for (Parameter parameter : m.getParameters()) {
            if (parameter.getName().equalsIgnoreCase("RowBounds")) {
                return true;
            }
        }
        return false;
    }

    private int getParamListSize(Method m, boolean withExample) {
        int size = 0;
        for (Parameter parameter : m.getParameters()) {
            String name = parameter.getName();
            if (!name.equalsIgnoreCase("RowBounds")) {
                if (!name.equalsIgnoreCase("example") || withExample) {
                    size ++;
                }
            }
        }
        return size;
    }
    private List<String> getParamList(Method m, boolean withExample) {
        List<String> paramList = new ArrayList<String>();
        for (Parameter parameter : m.getParameters()) {
            String name = parameter.getName();
            if (!name.equalsIgnoreCase("RowBounds")) {
                if (!name.equalsIgnoreCase("example") || withExample) {
                    String paramName = name;
                    if (parameter.getAnnotations().size() > 0) {
                        for (String annotation : parameter.getAnnotations()) {
                            if (annotation.startsWith("@Param(\"")) {
                                paramName = annotation.substring(8, annotation.length() - 2);
                                break;
                            }
                        }
                    }
                    paramList.add("    \"" + paramName + "\", " + name + ",");
                }
            }
        }
        boolean isParam = paramList.size() > 0;
        if (isParam) {
            //remove comma from last parameter
            int lastIdx = paramList.size() - 1;
            String last = paramList.get(lastIdx);
            paramList.remove(lastIdx);
            paramList.add(lastIdx, last.substring(0, last.length() - 1));
            paramList.add(0, "Map params = getParameters(");
            paramList.add(");");
        }
        return paramList;
    }
}

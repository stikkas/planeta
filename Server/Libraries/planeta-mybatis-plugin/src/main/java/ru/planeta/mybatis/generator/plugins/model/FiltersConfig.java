package ru.planeta.mybatis.generator.plugins.model;

import org.apache.commons.lang3.StringUtils;
import org.mybatis.generator.api.IntrospectedColumn;
import org.mybatis.generator.api.IntrospectedTable;
import org.mybatis.generator.config.Context;
import org.springframework.util.CollectionUtils;
import ru.planeta.mybatis.generator.plugins.enums.ComparisonOperator;
import ru.planeta.mybatis.generator.plugins.utils.IntrospectedTableCreator;
import ru.planeta.mybatis.generator.plugins.utils.PluginUtils;

import javax.xml.bind.annotation.*;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by kostiagn on 15.07.2015.
 */
@XmlRootElement(name = "filtersConfig")
@XmlAccessorType(XmlAccessType.NONE)
public class FiltersConfig {
    @XmlAttribute
    private String dateFormat;

    @XmlElement(name = "filter", required = true)
    private List<Filter> filterList = new ArrayList<>();

    public String getDateFormat() {
        return dateFormat;
    }

    public List<Filter> getFilterList() {
        return filterList;
    }

    @XmlAccessorType(XmlAccessType.NONE)
    public static class Filter {
        @XmlElement(name = "var", required = true)
        private List<Var> varList = new ArrayList<>();
        @XmlElement(name = "group", required = true)
        private List<Group> groupList = new ArrayList<>();
        @XmlAttribute
        private String name;
        @XmlAttribute(required = true)
        private String tableName;
        @XmlAttribute
        private String defaultOrderBy;
        @XmlAttribute
        private String lastOrderBy;


        private IntrospectedTable introspectedTable;


        public String getTableName() {
            return tableName;
        }

        public String getDefaultOrderBy() {
            return defaultOrderBy;
        }

        public String getLastOrderBy() {
            return lastOrderBy;
        }

        public List<Var> getVarList() {
            return varList;
        }

        public String getName() {
            if (StringUtils.isEmpty(name)) {
                name = PluginUtils.toCamelCase(tableName, false) + "Filter";
            }
            return name;
        }

        public List<Var> getAllVars() {
            List<Var> list = new ArrayList<>();
            list.addAll(varList);
            for (Group group : groupList) {
                list.addAll(group.varList);
            }
            return list;
        }

        public List<Group> getGroupList() {
            return groupList;
        }

        private IntrospectedTable findOrCreateIntrospectedTable(List<IntrospectedTable> introspectedTables, String fullTableName) {
            String tableName = PluginUtils.extractTableName(fullTableName);
            IntrospectedTable introspectedTable = PluginUtils.findIntrospectedTable(introspectedTables, tableName);
            if (introspectedTable == null) {
                introspectedTable =  IntrospectedTableCreator.get(introspectedTables.get(0).getContext(), fullTableName);
            }
            return introspectedTable;
        }

        public void initIntrospectedTablesAndColumns(List<IntrospectedTable> introspectedTables) {

            introspectedTable = PluginUtils.findIntrospectedTableSafe(introspectedTables, getTableName(), String.format("not found table %s for filter %s", getTableName(), getName()));


            for (FiltersConfig.Var var : getAllVars()) {

                if (StringUtils.isNotBlank(var.getJoinTable())) {
                    var.introspectedJoinTable = findOrCreateIntrospectedTable(introspectedTables, var.getJoinTable());
                }


                IntrospectedColumn introspectedColumn = PluginUtils.findColumn(var.introspectedJoinTable, var.getColumn());
                if (introspectedColumn == null) {
                    introspectedColumn = PluginUtils.findColumnByJavaProperty(var.introspectedJoinTable, var.getName());
                }
                if (introspectedColumn == null) {
                    introspectedColumn = PluginUtils.findColumn(introspectedTable, var.getColumn());
                }
                if (introspectedColumn == null) {
                    introspectedColumn = PluginUtils.findColumnByJavaProperty(introspectedTable, var.getName());
                }


                if (!CollectionUtils.isEmpty(var.getColumnList())) {
                    ArrayList<IntrospectedColumn> introspectedColumnList = new ArrayList<>(var.getColumnList().size());
                    var.introspectedColumnList = introspectedColumnList;
                    for (String columnName : var.getColumnList()) {
                        IntrospectedColumn ic = PluginUtils.findColumn(introspectedTable, columnName);
                        if (ic == null) {
                            ic = PluginUtils.findColumnByJavaProperty(introspectedTable, columnName);
                        }
                        if (ic == null) {
                            throw new RuntimeException(String.format("cannot find column for var '%s'. tableName = '%s'", var.getName(), getTableName()));
                        }
                        introspectedColumnList.add(ic);
                    }
                    introspectedColumn = introspectedColumnList.get(0);
                }
                var.introspectedColumn = introspectedColumn;

                if (StringUtils.isBlank(var.getType())) {
                    if (introspectedColumn == null) {
                        throw new RuntimeException(String.format("cannot find column for var '%s'. tableName = '%s'", var.getName(), getTableName()));
                    }
                    var.setType(introspectedColumn.getFullyQualifiedJavaType().getFullyQualifiedName());
                } else if (var.getType().equalsIgnoreCase("Date")) {
                    var.setType("java.util.Date");
                } else if (var.getType().equalsIgnoreCase("List")) {
                    if (introspectedColumn == null) {
                        throw new RuntimeException(String.format("cannot find column for var '%s'. tableName = '%s'", var.getName(), getTableName()));
                    }
                    var.setType("java.util.List<" + introspectedColumn.getFullyQualifiedJavaType().getShortName() + ">");
                }
            }
        }


        public IntrospectedTable getIntrospectedTable() {
            return introspectedTable;
        }
    }

    @XmlAccessorType(XmlAccessType.NONE)
    public static class Group {
        @XmlElement(name = "var", required = true)
        private List<Var> varList = new ArrayList<>();
        private String operation = "and";

        public List<Var> getVarList() {
            return varList;
        }

        public String getOperation() {
            return operation;
        }
    }

    @XmlAccessorType(XmlAccessType.NONE)
    public static class Var {

        private String name;
        @XmlAttribute(required = true)
        private void setName(String name){
            this.name = name;
            if("limit".equals(name) || "offset".equals(name)){
                type = "int";
                noWhere = true;
            }
        }
        @XmlAttribute
        private String type;
        @XmlAttribute
        private String column;
        @XmlJavaTypeAdapter(ComparisonOperator.ComparisonOperatorAdapter.class)
        @XmlAttribute
        private ComparisonOperator operator;
        @XmlAttribute
        private boolean noWhere;
        @XmlAttribute
        private String where;
        @XmlAttribute
        private String defaultValue;
        @XmlAttribute
        private String joinTable;
        @XmlJavaTypeAdapter(StringToStringListXmlAdapter.class)
        @XmlAttribute
        private List<String> joinTableOnFields;
        @XmlAttribute
        private String joinTableOn;
        @XmlJavaTypeAdapter(StringToStringListXmlAdapter.class)
        @XmlAttribute(name = "columnList")
        private List<String> columnList;
        @XmlAttribute
        private String columnListOperator = "or";
        @XmlAttribute
        private String whereOperator = "and";

        private IntrospectedTable introspectedTable;
        private IntrospectedTable introspectedJoinTable;
        private IntrospectedColumn introspectedColumn;
        private ArrayList<IntrospectedColumn> introspectedColumnList;
        @XmlAttribute(name = "ignoreCase")
        private boolean isIgnoreCase = true;

        public String getName() {
            return name;
        }

        public String getType() {
            return type;
        }

        public String getColumn() {
            return column;
        }

        public ComparisonOperator getOperator() {
            if (operator == null) {
                if (type != null && (type.equals("String") || type.equals("java.lang.String"))) {
                    operator = ComparisonOperator.CONTAINS;
                } else {
                    operator = ComparisonOperator.EQUAL;
                }
            }
            return operator;
        }

        public boolean isNoWhere() {
            return noWhere;
        }

        public String getDefaultValue() {
            return defaultValue;
        }

        public String getJoinTable() {
            return joinTable;
        }

        public List<String> getJoinTableOnFields() {
            return joinTableOnFields;
        }

        public String getJoinTableOn() {
            return joinTableOn;
        }


        public List<String> getColumnList() {
            return columnList;
        }

        public void setType(String type) {
            this.type = type;
        }

        public boolean isListType() {
            return type.startsWith("java.util.List<");
        }

        public String getWhere() {
            return where;
        }

        public String getColumnListOperator() {
            return columnListOperator;
        }

        public boolean isIgnoreCase() {
            return isIgnoreCase;
        }

        public IntrospectedColumn getIntrospectedColumn() {
            return introspectedColumn;
        }

        public List<IntrospectedColumn> getIntrospectedColumnList() {
            return introspectedColumnList;
        }

        public IntrospectedTable getIntrospectedJoinTable() {
            return introspectedJoinTable;
        }

        public String getWhereOperator() {
            return whereOperator;
        }
    }
}

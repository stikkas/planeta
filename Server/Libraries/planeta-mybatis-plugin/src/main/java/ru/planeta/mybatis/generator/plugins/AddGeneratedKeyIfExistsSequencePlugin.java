package ru.planeta.mybatis.generator.plugins;

import org.apache.commons.lang3.StringUtils;
import org.mybatis.generator.api.IntrospectedColumn;
import org.mybatis.generator.api.IntrospectedTable;
import org.mybatis.generator.api.dom.java.FullyQualifiedJavaType;
import org.mybatis.generator.config.ColumnOverride;
import org.mybatis.generator.config.GeneratedKey;
import org.mybatis.generator.internal.db.ConnectionFactory;
import ru.planeta.mybatis.generator.plugins.utils.PluginUtils;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by kostiagn on 10.07.2015.
 */
public class AddGeneratedKeyIfExistsSequencePlugin extends MyPluginAdapter {
    protected void init() {
        super.init();

        List<String> sequenceList = getSequenceFromDB();
        for (IntrospectedTable it : PluginUtils.getIntrospectedTables(this.getContext())) {
            if (it.getPrimaryKeyColumns().size() == 1) {
                String columnName = it.getPrimaryKeyColumns().get(0).getActualColumnName();
//                System.out.println("table " + it.getTableConfiguration().getTableName() + " column " + columnName);
                if (sequenceList.contains("seq_" + columnName) && checkTableName(it.getTableConfiguration().getTableName(), columnName) && it.getGeneratedKey() == null) {
                    String query = "SELECT nextval('commondb.seq_" + columnName + "');";
                    it.getTableConfiguration().setGeneratedKey(new GeneratedKey(columnName, query, false, null));
                    System.out.println("add generated key. table " + it.getTableConfiguration().getTableName() + " column " + columnName + " query '" + query + "'");
                }
            }
        }

    }

    private boolean checkTableName(String tableName, String columnName) {
        if (StringUtils.isBlank(columnName) || StringUtils.isBlank(tableName) || !columnName.endsWith("_id")) {
            return false;
        }
        String columnNameWithoutId = columnName.substring(0, columnName.length() - 3);
        return tableName.equals(columnNameWithoutId) || tableName.equals(columnNameWithoutId + "s");
    }

    private List<String> getSequenceFromDB() {
        Connection connection = null;
        try {
            connection = ConnectionFactory.getInstance().getConnection(this.context.getJdbcConnectionConfiguration());
            List<String> result = new ArrayList<>();
            ResultSet rs = connection.createStatement().executeQuery(
                    "SELECT c.relname \n" +
                            "FROM pg_catalog.pg_class c\n" +
                            "  JOIN pg_catalog.pg_roles r ON r.oid = c.relowner\n" +
                            "  LEFT JOIN pg_catalog.pg_namespace n ON n.oid = c.relnamespace\n" +
                            "WHERE relkind = 'S' and relname like 'seq_%_id' and nspname = 'commondb';");
            while (rs.next()) {
                result.add(rs.getString(1));
            }
            return result;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            try {
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

    }
}

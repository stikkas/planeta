package ru.planeta.mybatis.generator.plugins.model;

import org.apache.commons.lang3.StringUtils;
import ru.planeta.mybatis.generator.plugins.enums.ComparisonOperator;

import javax.xml.bind.annotation.*;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by kostiagn on 23.06.2015.
 */
@XmlRootElement(name = "baseServicesConfig")
@XmlAccessorType(XmlAccessType.FIELD)
public class BaseServicesConfig {
    @XmlElement(name = "table", required = true)
    private List<Table> tableList = new ArrayList<>();

    public Table findTable(String tableName) {
        if (!StringUtils.isBlank(tableName)) {
            for (Table table : tableList) {
                if (tableName.equals(table.getName())) {
                    return table;
                }
            }
        }
        return null;
    }

    @XmlAccessorType(XmlAccessType.FIELD)
    public static class Table {
        @XmlAttribute
        private String name;
        @XmlAttribute
        private String schema;
        @XmlElement(name = "method")
        List<Method> methodList = new ArrayList<>();

        public String getName() {
            return name;
        }

        public List<Method> getMethodList() {
            return methodList;
        }

        public String getSchema() {
            return schema;
        }

        public void setName(String name) {
            this.name = name;
        }

        public void setSchema(String schema) {
            this.schema = schema;
        }
    }

    @XmlAccessorType(XmlAccessType.FIELD)
    public static class Method {
        @XmlAttribute
        private String name = "";
        @XmlElement(name = "param")
        private List<Param> paramList = new ArrayList<>();
        @XmlAttribute
        private boolean selectOne = false;
        @XmlAttribute
        private String orderBy = null;

        public String getName() {
            return name;
        }

        public List<Param> getParamList() {
            return paramList;
        }

        public boolean isSelectOne() {
            return selectOne;
        }

        public String getOrderBy() {
            return orderBy;
        }
    }

    @XmlAccessorType(XmlAccessType.FIELD)
    public static class Param {
        @XmlAttribute(required = true)
        private String name = "";
        @XmlAttribute
        @XmlJavaTypeAdapter(ComparisonOperator.ComparisonOperatorAdapter.class)
        private ComparisonOperator operation = ComparisonOperator.EQUAL;
        @XmlAttribute
        private boolean isList;

        public String getName() {
            return name;
        }

        public ComparisonOperator getOperation() {
            return operation;
        }

        public boolean isList() {
            return isList;
        }
    }

    public List<Table> getTableList() {
        return tableList;
    }

}
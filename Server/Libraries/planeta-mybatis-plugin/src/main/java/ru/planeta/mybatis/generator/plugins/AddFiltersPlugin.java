package ru.planeta.mybatis.generator.plugins;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;
import org.mybatis.generator.api.GeneratedJavaFile;
import org.mybatis.generator.api.IntrospectedColumn;
import org.mybatis.generator.api.IntrospectedTable;
import org.mybatis.generator.api.dom.java.*;
import org.mybatis.generator.api.dom.xml.*;
import ru.planeta.mybatis.generator.plugins.annotation.Config;
import ru.planeta.mybatis.generator.plugins.annotation.Init;
import ru.planeta.mybatis.generator.plugins.annotation.Property;
import ru.planeta.mybatis.generator.plugins.enums.ComparisonOperator;
import ru.planeta.mybatis.generator.plugins.model.ExTableConfig;
import ru.planeta.mybatis.generator.plugins.model.FiltersConfig;
import ru.planeta.mybatis.generator.plugins.utils.FieldGetterSetter;
import ru.planeta.mybatis.generator.plugins.utils.PluginUtils;

import java.util.ArrayList;
import java.util.List;

import static ru.planeta.mybatis.generator.plugins.utils.PluginUtils.*;


public class AddFiltersPlugin extends MyPluginAdapter {
    @Property
    private String filtersPackage;

    @Init
    private List<IntrospectedTable> introspectedTables;

    @Config("configFile")
    private FiltersConfig filtersConfig;

    private String dateFormat;

    protected void init() {
        super.init();

        for (FiltersConfig.Filter filter : filtersConfig.getFilterList()) {
            PluginUtils.filterTableList.add(filter);
            filter.initIntrospectedTablesAndColumns(introspectedTables);
        }
        System.out.println();
    }

    @Override
    public List<GeneratedJavaFile> contextGenerateAdditionalJavaFiles() {
        List<GeneratedJavaFile> result = new ArrayList<GeneratedJavaFile>();
        for (FiltersConfig.Filter filter : filtersConfig.getFilterList()) {
            GeneratedJavaFile javaFile = generateFilterFile(filter);
            if (javaFile != null) {
                result.add(javaFile);
            }
        }
        return result;
    }

    private GeneratedJavaFile generateFilterFile(FiltersConfig.Filter filter) {
        TopLevelClass clazz = new TopLevelClass(filtersPackage + "." + PluginUtils.toClassName(filter.getName()));
        clazz.setVisibility(JavaVisibility.PUBLIC);
        for (FiltersConfig.Var var : filter.getAllVars()) {
            FullyQualifiedJavaType type = new FullyQualifiedJavaType(var.getType());

            FieldGetterSetter fieldGetterSetter = FieldGetterSetter.addToClass(clazz, type, var.getName());

            if (var.getType().equals("java.util.Date")) {
                clazz.addImportedType("org.springframework.format.annotation.DateTimeFormat");
                fieldGetterSetter.getGetter().addAnnotation("@DateTimeFormat(pattern = \"" + dateFormat + "\")");
            }
            if (var.getDefaultValue() != null) {
                fieldGetterSetter.getField().setInitializationString(var.getDefaultValue());
            }
        }
        PluginUtils.addImportedType(clazz);

        FieldGetterSetter fieldGetterSetter = FieldGetterSetter.addToClass(clazz, FullyQualifiedJavaType.getStringInstance(), "orderBy");
        if (StringUtils.isNotBlank(filter.getDefaultOrderBy())) {
            fieldGetterSetter.getField().setInitializationString("\"" + filter.getDefaultOrderBy() + "\"");
        }

        return PluginUtils.createGeneratedJavaFile(clazz, this.getContext());
    }


    @Override
    public boolean sqlMapDocumentGenerated(org.mybatis.generator.api.dom.xml.Document document,
                                           IntrospectedTable introspectedTable) {
        String tableName = introspectedTable.getFullyQualifiedTable().getIntrospectedTableName();
        XmlElement root = document.getRootElement();
        FiltersConfig.Filter filter = findFilter(tableName);
        if (filter != null) {
            generateMapper(root, filter, null);
            for (ExTableConfig.ExTable exTable : PluginUtils.exTable.keySet()) {
                if (PluginUtils.exTable.get(exTable).equals(tableName)) {
                    generateMapper(root, filter, exTable);
                }
            }
        }
        return true;
    }


    private FiltersConfig.Filter findFilter(String tableName) {
        for (FiltersConfig.Filter filter : filtersConfig.getFilterList()) {
            if (filter.getTableName().equals(tableName)) {
                return filter;
            }
        }
        return null;
    }


    public void generateMapper(XmlElement parent, FiltersConfig.Filter filter, ExTableConfig.ExTable exTable) {
        generateSelectByFilter(parent, filter, exTable);
        if (exTable == null) {
            generateSqlSelectByFilter(parent, filter);
            generateSelectCountByFilter(parent, filter);
        }

    }

    public void generateSqlSelectByFilter(XmlElement parent, FiltersConfig.Filter filter) {
        XmlElement selectSqlByFilter = newXmlElementWithCommentAddTo(context, parent, "sql", "id", getSqlSelectByFilterName(filter));
        XmlElement where = newXmlElementAddTo(selectSqlByFilter, "where");

        addAnd(filter, filter.getVarList(), where);
        for (FiltersConfig.Group group : filter.getGroupList()) {
            XmlElement xmlElement = newXmlElementAddTo(where, "trim", "prefix", group.getOperation() + " (", "suffix", ")", "prefixOverrides", "AND|OR ");
            addAnd(filter, group.getVarList(), xmlElement);
        }
    }

    private void addAnd(FiltersConfig.Filter filter, List<FiltersConfig.Var> varList, XmlElement xmlElement) {
        for (FiltersConfig.Var var : varList) {
            if (var.isNoWhere()) {
                continue;
            }
            String varName = /*filter.getName() + "." +*/ var.getName();
            XmlElement ifElement = new XmlElement("if");
            if (var.isListType()) {
                ifElement.addAttribute(new Attribute("test", varName + " != null and " + varName + ".size() > 0"));
            } else {
                ifElement.addAttribute(new Attribute("test", varName + " != null"));
            }

            xmlElement.addElement(ifElement);
            if (var.getWhere() != null) {
                ifElement.addElement(new TextElement(var.getOperator().getSqlOperator() + " " + var.getWhere()));
            } else {
                if (var.getIntrospectedColumn() == null && var.getIntrospectedColumnList() == null) {
                    throw new RuntimeException(String.format("для переменной %s.%s не задано: column или columnList", filter.getTableName(), var.getName()));
                }
                StringBuilder sb = new StringBuilder(" ");
                sb.append(var.getWhereOperator()).append(' ');
                if (var.getJoinTable() != null) {
                    sb.append("exists ( select 1 from ")
                            .append(PluginUtils.getAliasedFullyQualifiedTableName(var.getIntrospectedJoinTable()))
                            .append(" where ");
                    if (var.getJoinTableOn() != null) {
                        sb.append(var.getJoinTableOn());
                    } else if (var.getJoinTableOnFields() != null && var.getJoinTableOnFields().size() > 0) {
                        boolean isFirst = true;
                        for (String fieldName : var.getJoinTableOnFields()) {
                            if (!isFirst) {
                                sb.append(" and ");
                            }
                            isFirst = false;

                            sb.append(var.getIntrospectedJoinTable().getTableConfiguration().getAlias()).append('.').append(fieldName)
                                    .append(" = ")
                                    .append(filter.getIntrospectedTable().getTableConfiguration().getAlias()).append('.').append(fieldName);
                        }
                    } else {
                        sb.append(joinTablesByPrimaryKeyGetWhere(filter.getIntrospectedTable(), var.getIntrospectedJoinTable(), true));
                    }
                    //todo NONE
//                    if (!"NONE".equals(var.getOperator())) {
                    sb.append(" and ");
//                    }
                }

                ifElement.addElement(new TextElement(StringEscapeUtils.escapeXml(sb.toString())));
                //todo NONE
//                if (!"NONE".equals(var.operator)) {
                if (var.getIntrospectedColumn() != null) {
                    addColumnToAnd(filter, var, var.getIntrospectedColumn().getActualColumnName(), ifElement);
                } else if (var.getIntrospectedColumnList() != null) {
                    ifElement.addElement(new TextElement("("));
                    boolean isFirst = true;
                    for (IntrospectedColumn ic : var.getIntrospectedColumnList()) {
                        if (isFirst) {
                            isFirst = false;
                        } else {
                            ifElement.addElement(new TextElement(var.getColumnListOperator()));
                        }
                        addColumnToAnd(filter, var, ic.getActualColumnName(), ifElement);
                    }
                    ifElement.addElement(new TextElement(")"));
                }
//                }

                if (var.getJoinTable() != null) {
                    ifElement.addElement(new TextElement(")"));
                }
            }
        }
    }

    private void addColumnToAnd(FiltersConfig.Filter filter, FiltersConfig.Var var, String columnName, XmlElement ifElement) {
        String fullColumnName = (var.getJoinTable() == null ? filter.getTableName() : PluginUtils.extractTableName(var.getJoinTable())) + "." + columnName;
        String varName = /*filter.getName() + "." + */var.getName();
        if (var.isListType()) {
            ifElement.addElement(new TextElement(StringEscapeUtils.escapeXml(fullColumnName + " in")));
            XmlElement forEachElement = new XmlElement("foreach");
            PluginUtils.addAttributes(forEachElement, "close", ")", "item", "listItem", "open", "(", "separator", ",", "collection", varName);
            ifElement.addElement(forEachElement);
            forEachElement.addElement(new TextElement("#{listItem}"));

        } else {
            ComparisonOperator operator = var.getOperator();
            StringBuilder sb = new StringBuilder();
            if ("java.lang.String".equalsIgnoreCase(var.getType()) || "String".equalsIgnoreCase(var.getType())) {
                sb.append(fullColumnName).append(' ');
                if (operator == ComparisonOperator.CONTAINS || operator == ComparisonOperator.ENDS_WITH) {
                    sb.append(var.isIgnoreCase() ? "ILIKE" : "LIKE").append(" '%' ||");
                } else if (operator == ComparisonOperator.LIKE || operator == ComparisonOperator.STARTS_WITH) {
                    sb.append(var.isIgnoreCase() ? "ILIKE" : "LIKE");
                } else {
                    sb.append(operator.getSqlOperator());
                }
                sb.append(' ');
                sb.append("#{").append(varName).append("}");
                if (operator == ComparisonOperator.CONTAINS || operator == ComparisonOperator.STARTS_WITH) {
                    sb.append(" || '%'");
                }

            } else {
                boolean isDate = "java.util.Date".equalsIgnoreCase(var.getType()) || "Date".equalsIgnoreCase(var.getType());
                boolean isLessAndEqual = operator == ComparisonOperator.LESS_THEN_OR_EQUAL_TO;

                if (isDate && isLessAndEqual) {
                    operator = ComparisonOperator.LESS_THEN;
                }

                sb.append(fullColumnName).append(" ").append(operator.getSqlOperator()).append(" #{").append(varName);
                if (var.getIntrospectedColumn().getTypeHandler() != null) {
                    sb.append(",typeHandler=").append(var.getIntrospectedColumn().getTypeHandler());
                }
                sb.append("}");

                if (isDate) {
                    sb.append("::DATE");
                    if (isLessAndEqual) {
                        sb.append(" + INTERVAL '1 day'");
                    }
                }
            }
            ifElement.addElement(new TextElement(StringEscapeUtils.escapeXml(sb.toString())));
        }
    }

    public void generateSelectByFilter(XmlElement parent, FiltersConfig.Filter filter, ExTableConfig.ExTable exTable) {
        XmlElement selectByFilter = newXmlElementWithCommentAddTo(context, parent, "select"
                , "id", getSelectListByFilterName(filter, exTable)
                , "parameterType", filtersPackage + "." + PluginUtils.toClassName(filter.getName())
                , "resultMap", exTable == null ? "BaseResultMap" : exTable.getResultMapId());

        selectByFilter.addElement(new TextElement("select "));
        newXmlElementAddTo(selectByFilter, "include", "refid", exTable == null ? "Base_Column_List" : exTable.getBaseColumnListId());

        selectByFilter.addElement(new TextElement("from " + filter.getIntrospectedTable().getAliasedFullyQualifiedTableNameAtRuntime()));
        if (exTable != null) {
            newXmlElementAddTo(selectByFilter, "include", "refid", exTable.getJoinTableSqlRefId());
        }

        selectByFilter.addElement(getIncludeElement(filter));


        addOrderBy(filter, selectByFilter);
        addOffsetLimit(selectByFilter);

    }

    private void addOrderBy(FiltersConfig.Filter filter, XmlElement selectByFilter) {
        XmlElement ifElement = newXmlElementAddTo(selectByFilter, "if", "test", "orderBy");

        ifElement.addElement(new TextElement("order by ${" + "orderBy}" + (StringUtils.isEmpty(filter.getLastOrderBy()) ? "" : ", " + filter.getLastOrderBy())));
        if (!StringUtils.isEmpty(filter.getLastOrderBy())) {
            ifElement = new XmlElement("if");
            PluginUtils.addAttribute(ifElement, "test", "!" + "orderBy");
            selectByFilter.addElement(ifElement);
            ifElement.addElement(new TextElement("order by " + filter.getLastOrderBy()));
        }
    }

    private XmlElement getIncludeElement(FiltersConfig.Filter filter) {
        return newXmlElement("include", "refid", getSqlSelectByFilterName(filter));
    }

    private String getSqlSelectByFilterName(FiltersConfig.Filter filter) {
        return "select_by_" + filter.getName();

    }

    private void generateSelectCountByFilter(XmlElement parent, FiltersConfig.Filter filter) {
        XmlElement selectByFilter = newXmlElementWithCommentAddTo(context, parent, "select"
                , "id", "select" + PluginUtils.toCamelCase(filter.getTableName(), true) + "CountByFilter"
                , "parameterType", filtersPackage + "." + PluginUtils.toClassName(filter.getName())
                , "resultType", "java.lang.Integer");

        selectByFilter.addElement(new TextElement("select count(*)"));
        selectByFilter.addElement(new TextElement("from " + filter.getIntrospectedTable().getAliasedFullyQualifiedTableNameAtRuntime()));
        selectByFilter.addElement(getIncludeElement(filter));
    }

    @Override
    public boolean clientGenerated(Interface interfaze, TopLevelClass topLevelClass, IntrospectedTable introspectedTable) {
        String tableName = introspectedTable.getFullyQualifiedTable().getIntrospectedTableName();
        FiltersConfig.Filter filter = findFilter(tableName);
        if (filter != null) {
            generateInterfaceMethod(interfaze, filter, null);
            for (ExTableConfig.ExTable exTable : PluginUtils.exTable.keySet()) {
                if (PluginUtils.exTable.get(exTable).equals(tableName)) {
                    generateInterfaceMethod(interfaze, filter, exTable);
                }
            }
        }
        return true;
    }

    private void generateInterfaceMethod(Interface interfaze, FiltersConfig.Filter filter, ExTableConfig.ExTable exTable) {
        String type = exTable == null ? filter.getIntrospectedTable().getBaseRecordType() : exTable.getTypeName();
        String className = PluginUtils.toClassName(filter.getName());
        String objectName = PluginUtils.toObjectName(className);

        interfaze.addImportedType(new FullyQualifiedJavaType("org.apache.ibatis.annotations.Param"));
        interfaze.addImportedType(new FullyQualifiedJavaType(filtersPackage + "." + className));

        String param = "@Param(\"" + objectName + "\") " + className;

        interfaze.addMethod(PluginUtils.createMethod(getSelectListByFilterName(filter, exTable), PluginUtils.createGenericType("List", type), "", param));
        if (exTable == null) {
            interfaze.addMethod(PluginUtils.createMethod("select" + PluginUtils.toCamelCase(filter.getTableName(), true) + "CountByFilter", FullyQualifiedJavaType.getIntInstance(), "", param));
        }
    }

    private String getSelectListByFilterName(FiltersConfig.Filter filter, ExTableConfig.ExTable exTable) {
        String listName = PluginUtils.toCamelCase(exTable == null ? filter.getTableName() : exTable.getName(), true) + "List";
        return "select" + listName + "By" + PluginUtils.toClassName(filter.getName());
    }
}

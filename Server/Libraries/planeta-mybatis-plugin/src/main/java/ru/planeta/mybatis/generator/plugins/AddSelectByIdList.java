package ru.planeta.mybatis.generator.plugins;

import org.mybatis.generator.api.IntrospectedTable;
import org.mybatis.generator.api.dom.java.FullyQualifiedJavaType;
import org.mybatis.generator.api.dom.java.Interface;
import org.mybatis.generator.api.dom.java.Method;
import org.mybatis.generator.api.dom.java.TopLevelClass;
import org.mybatis.generator.api.dom.xml.Document;
import org.mybatis.generator.api.dom.xml.TextElement;
import org.mybatis.generator.api.dom.xml.XmlElement;

import static ru.planeta.mybatis.generator.plugins.utils.PluginUtils.*;

public class AddSelectByIdList extends MyPluginAdapter {
    private static final String SELECT_BY_ID_LIST = "selectByIdList";

    @Override
    public boolean sqlMapDocumentGenerated(Document document, IntrospectedTable introspectedTable) {
//        SELECT *
//                FROM commondb.orders
//        WHERE order_id in
//                <foreach item="orderId" index="index" collection="orderIds" open="(" separator="," close=")">#{orderId}</foreach>
//                ORDER BY sort_in_this_order(array[<foreach item="orderId" index="index" collection="orderIds" separator="," >#{orderId}</foreach>]::bigint[],order_id);


        if (introspectedTable.getPrimaryKeyColumns().size() != 1 || !introspectedTable.getPrimaryKeyColumns().get(0).getFullyQualifiedJavaType().getShortName().equals("Long")) {
            return true;
        }
        String fullTypeName = introspectedTable.getBaseRecordType();
        String typeName = getTypeName(fullTypeName);
        String primaryKeyName = introspectedTable.getPrimaryKeyColumns().get(0).getActualColumnName();
        String javaPrimaryKeyName = introspectedTable.getPrimaryKeyColumns().get(0).getJavaProperty();

        XmlElement root = document.getRootElement();
        XmlElement select = newXmlElementWithCommentAddTo(context, root
                , "select"
                , "id", SELECT_BY_ID_LIST
                , "parameterType", "list"
                , "resultMap", introspectedTable.getBaseResultMapId()
        );

        select.addElement(new TextElement("SELECT"));
        select.addElement(newXmlElement("include", "refid", introspectedTable.getBaseColumnListId()));
        select.addElement(new TextElement("FROM " + introspectedTable.getFullyQualifiedTableNameAtRuntime()));
        select.addElement(new TextElement("WHERE " + primaryKeyName + " in"));


        XmlElement forEach = newXmlElementAddTo(select, "foreach"
                , "item", "id"
                , "index", "index"
                , "collection", "list"
                , "separator", ","
                , "open", "("
                , "close", ")"
        );
        forEach.addElement(new TextElement("#{id}"));

        select.addElement(new TextElement("ORDER BY sort_in_this_order(array["));
        forEach = newXmlElementAddTo(select, "foreach"
                , "item", "id"
                , "index", "index"
                , "collection", "list"
                , "separator", ","
        );
        forEach.addElement(new TextElement("#{id}"));

        select.addElement(new TextElement("]::bigint[]," + primaryKeyName + ");"));
        return true;
    }

    private String getTypeNameIdList(String typeName) {
        return toObjectName(typeName) + "IdList";
    }

    @Override
    public boolean clientGenerated(Interface interfaze, TopLevelClass topLevelClass, IntrospectedTable introspectedTable) {
        String fullTypeName = introspectedTable.getBaseRecordType();
        String typeName = getTypeName(fullTypeName);
        Method method = createMethod(SELECT_BY_ID_LIST, "public", "List<Long> " + getTypeNameIdList(typeName));
        method.setReturnType(new FullyQualifiedJavaType("List<" + fullTypeName + ">"));
        interfaze.addImportedType(FullyQualifiedJavaType.getNewListInstance());
        interfaze.addMethod(method);
        return true;
    }
}

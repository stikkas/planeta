package ru.planeta.mybatis.generator.plugins;


import org.mybatis.generator.api.PluginAdapter;
import org.mybatis.generator.internal.db.ConnectionFactory;
import ru.planeta.mybatis.generator.plugins.annotation.Init;
import ru.planeta.mybatis.generator.plugins.utils.PluginUtils;

import java.sql.SQLException;
import java.util.List;

public class MyPluginAdapter extends PluginAdapter {
    @Override
    public boolean validate(List<String> warnings) {
        init();
        return true;
    }

    protected void init() {
        try {
            for (java.lang.reflect.Field f : this.getClass().getDeclaredFields()) {
                f.setAccessible(true);
                if (f.getAnnotation(Init.class) != null) {
                    if ("connection".equals(f.getName())) {
                        try {
                            f.set(this, ConnectionFactory.getInstance().getConnection(this.context.getJdbcConnectionConfiguration()));
                        } catch (SQLException e) {
                            throw new RuntimeException(e);
                        }
                    }
                    if ("introspectedTables".equals(f.getName())) {
                        f.set(this, PluginUtils.getIntrospectedTables(this.getContext()));
                    }
                }
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        PluginUtils.initProperty(this, this.properties);
    }
}

package ru.planeta.mybatis.generator.plugins.utils;

import org.mybatis.generator.api.dom.java.Method;
import org.mybatis.generator.api.dom.java.TopLevelClass;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class VarReplacer {
    private char delimiter = '$';
    Map<String, String> vars = new HashMap<String, String>();

    public VarReplacer() {

    }

    public VarReplacer(char delimiter) {
        this.delimiter = delimiter;
    }

    public void put(String key, String val) {
        vars.put(key, val);
    }


    public void replace(TopLevelClass clazz) {
        for (Method method : clazz.getMethods()) {
            replace(method);
        }
    }

    public void replace(Method method) {
        method.setName(replace(method.getName()));
        List<String> lines = new ArrayList<String>();
        for (String line : method.getBodyLines()) {
            lines.add(replace(line));
        }
        method.getBodyLines().clear();
        method.addBodyLines(lines);
    }

    public String replace(String str) {
        String newStr = str;
        while (true) {
            int i = newStr.indexOf(delimiter);
            if (i < 0) {
                break;
            }
            int j = newStr.indexOf(delimiter, i + 1);
            if (j < 0) {
                break;
            }
            String vr = newStr.substring(i + 1, j);
            String val = vars.get(PluginUtils.toObjectName(vr));
            if (val == null) {
                val = "__NOT__FOUND__";
            } else {
                char firstChar = vr.charAt(0);
                if (firstChar >= 'A' && firstChar < 'Z') {
                    val = PluginUtils.toClassName(val);
                } else {
                    val = PluginUtils.toObjectName(val);
                }
            }
            newStr = newStr.substring(0, i) + val + newStr.substring(j + 1);
        }
        return newStr;
    }

    public List<String> replace(List<String> lines) {
        List<String> result = new ArrayList<String>(lines.size());
        for (String line : lines) {
            result.add(replace(line));
        }
        return result;
    }

    public void addBodyLine(Method method, String line) {
        method.addBodyLine(replace(line));
    }
}

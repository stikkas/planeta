package ru.planeta.mybatis.generator.plugins.utils;

import org.apache.commons.lang3.StringUtils;
import org.mybatis.generator.api.GeneratedJavaFile;
import org.mybatis.generator.api.IntrospectedColumn;
import org.mybatis.generator.api.IntrospectedTable;
import org.mybatis.generator.api.dom.java.*;
import org.mybatis.generator.api.dom.xml.Attribute;
import org.mybatis.generator.api.dom.xml.Element;
import org.mybatis.generator.api.dom.xml.TextElement;
import org.mybatis.generator.api.dom.xml.XmlElement;
import org.mybatis.generator.config.Context;
import org.mybatis.generator.config.TableConfiguration;
import ru.planeta.mybatis.generator.plugins.annotation.Config;
import ru.planeta.mybatis.generator.plugins.annotation.Property;
import ru.planeta.mybatis.generator.plugins.model.ExTableConfig;
import ru.planeta.mybatis.generator.plugins.model.FiltersConfig;

import javax.xml.bind.*;
import java.io.File;
import java.sql.SQLException;
import java.util.*;

public class PluginUtils {
    public static Map<ExTableConfig.ExTable, String> exTable = new HashMap<>();
    public static List<FiltersConfig.Filter> filterTableList = new ArrayList<>();

    public static Method createMethod(String name, String str, String... params) {
        Method method = new Method(name);
        for (String word : str.split(" ")) {
            if (word.trim().isEmpty()) continue;
            if (word.equals("static")) {
                method.setStatic(true);
            } else if (getVisibility(word) != null) {
                method.setVisibility(getVisibility(word));
            } else if (word.startsWith("@")) {
                method.addAnnotation(word);
            } else {
                FullyQualifiedJavaType type = findType(word);
                if (type != null) {
                    method.setReturnType(type);
                    continue;
                }
            }
        }


        for (String param : params) {
            if (!param.trim().isEmpty()) {
                for (String p : splitParam(param)) {
                    if (!p.isEmpty()) {
                        method.addParameter(createParam(p));
                    }
                }
            }
        }

        return method;
    }

    public static int findCloseBracket(String tx, int pos, char openBracket, char closeBracket) {
        int count = 0;
        while (pos < tx.length()) {
            if (tx.charAt(pos) == openBracket) {
                count++;
            } else if (tx.charAt(pos) == closeBracket) {
                count--;
                if (count <= 0) {
                    break;
                }
            }
            pos++;
        }
        return pos;
    }

    private static String[] splitParam(String tx) {

        if (tx.indexOf('<') < 0) {
            return tx.split(",");
        }
        StringBuilder sb = new StringBuilder(tx);

        while (true) {
            int st = sb.indexOf("<");
            if (st < 0) {
                break;
            }
            int en = findCloseBracket(tx, st, '<', '>');
            while (st <= en) {
                sb.setCharAt(st++, ' ');
            }
        }

        List<String> paramList = new ArrayList<String>();
        int st = 0;
        while (st < tx.length()) {
            int en = sb.indexOf(",", st);
            if (en < 0) {
                en = tx.length();
            }
            paramList.add(tx.substring(st, en));
            st = en + 1;
        }

        return paramList.toArray(new String[0]);
    }

    public static JavaVisibility getVisibility(String str) {
        try {
            return JavaVisibility.valueOf(str.toUpperCase());
        } catch (Exception e) {
            return null;
        }
    }

    public static Method createMethod(String name, FullyQualifiedJavaType type, String str, String... params) {
        Method method = createMethod(name, str, params);
        method.setReturnType(type);
        return method;
    }

    public static Method createConstruct(FullyQualifiedJavaType clazzType, String str, String... params) {

        Method construct = createMethod(clazzType.getShortName(), str + " " + clazzType.getFullyQualifiedName(), params);
        construct.setConstructor(true);
        return construct;
    }

    public static FullyQualifiedJavaType findType(String word) {

        if (word.equals("String")) {
            return FullyQualifiedJavaType.getStringInstance();
        } else if (word.equals("int")) {
            return FullyQualifiedJavaType.getIntInstance();
        } else if (word.equals("boolean")) {
            return FullyQualifiedJavaType.getBooleanPrimitiveInstance();
        } else if (word.equals("Object")) {
            return FullyQualifiedJavaType.getObjectInstance();
        } else if (word.equals("Long")) {
            return new FullyQualifiedJavaType("java.lang.Long");
        } else if (word.equals("ServletRequest")) {
            return new FullyQualifiedJavaType("javax.servlet.ServletRequest");
        } else if (word.equals("Properties")) {
            return new FullyQualifiedJavaType("java.util.Properties");
        }
        return null;

    }

    public static FullyQualifiedJavaType getType(String word) {
        FullyQualifiedJavaType type = findType(word);
        if (type != null) return type;
        throw new RuntimeException("type " + word + " not found");

    }

    private static Object[] createVar(String var) {
        String[] words = var.split(" ");
        FullyQualifiedJavaType type = findType(words[0]);
        if (type == null) {
            type = new FullyQualifiedJavaType(words[0]);
        }
        String name = null;
        if (words.length > 1) {
            name = words[1];
        } else {
            name = words[0];
            int i = name.lastIndexOf('.');
            if (i > 0) {
                name = name.substring(i + 1);
            }
            name = name.substring(0, 1).toLowerCase() + name.substring(1);
        }

        return new Object[]{type, name};
    }

    public static Parameter createParam(String param) {
        String annotation = null;
        param = param.trim();
        if (param.startsWith("@")) {
            int i = param.indexOf(" ");
            annotation = param.substring(0, i);
            param = param.substring(i + 1);
        }
        Object[] var = createVar(param);

        Parameter parameter = new Parameter((FullyQualifiedJavaType) var[0], (String) var[1]);
        if (annotation != null) {
            parameter.addAnnotation(annotation);
        }
        return parameter;
    }

    public static Field createField(String str) {
        Object[] var = createVar(str);
        return new Field((String) var[1], (FullyQualifiedJavaType) var[0]);

    }

    public static Field createField(String annotation, String str) {
        Field field = createField(str);
        for (String word : annotation.split(" ")) {
            if (getVisibility(word) != null) {
                field.setVisibility(getVisibility(word));
            } else if (word.startsWith("@")) {
                field.addAnnotation(word);
            }
        }
        return field;
    }

    public static String extractTableName(String name) {
        int i = name.lastIndexOf('.');
        if (i >= 0) {
            name = name.substring(i + 1);
        }
        return name;
    }

    public static String extractSchemaName(String name) {
        int i = name.indexOf('.');
        return i < 0 ? "" : name.substring(0, i);
    }

    public static String extractTableName(IntrospectedTable table) {
        return extractTableName(table.getFullyQualifiedTableNameAtRuntime());
    }

    public static String toObjectName(String s) {
        if (s == null || s.isEmpty()) {
            return s;
        }
        if (s.length() == 1) {
            return s.toLowerCase();
        }
        return s.substring(0, 1).toLowerCase() + s.substring(1);
    }

    public static String toClassName(String s) {
        if (s == null || s.isEmpty()) {
            return s;
        }
        if (s.length() == 1) {
            return s.toUpperCase();
        }
        return s.substring(0, 1).toUpperCase() + s.substring(1);
    }

    public static String toCamelCase(String name, boolean isClassName) {
        StringBuilder sb = new StringBuilder(name.length());
        boolean isFirst = true;
        for (String word : name.split("_")) {
            if (!word.isEmpty()) {
                if (isFirst && !isClassName) {
                    sb.append(word.substring(0, 1).toLowerCase());
                } else {
                    sb.append(word.substring(0, 1).toUpperCase());
                }
                sb.append(word.substring(1).toLowerCase());
            }
            isFirst = false;
        }
        return sb.toString();
    }

    public static List<String> bodyLines(StringBuilder sb, String className) {
        return Arrays.asList(sb.toString().replace("$Class$", className).replace("$obj$", toObjectName(className)).split("\n"));
    }

    public static void setAttribute(XmlElement element, String attributeName, String value) {
        Attribute attribute = getAttribute(element, attributeName);

        if (attribute != null) {
            element.getAttributes().remove(attribute);
            element.addAttribute(new Attribute(attribute.getName(), value));
        }
    }

    public static Attribute getAttribute(XmlElement element, String attributeName) {
        if (attributeName == null) return null;
        for (Attribute attribute : element.getAttributes()) {
            if (attributeName.equals(attribute.getName())) {
                return attribute;
            }
        }
        return null;
    }

    public static Attribute addAttribute(XmlElement element, String attributeName, String attributeValue) {
        Attribute attribute = new Attribute(attributeName, attributeValue);
        element.addAttribute(attribute);
        return attribute;
    }

    public static boolean attributeValueEquals(XmlElement element, String attributeName, String attributeValue) {
        if (element != null && attributeName != null && attributeValue != null) {
            Attribute attribute = getAttribute(element, attributeName);
            if (attribute != null) {
                return attributeValue.equals(attribute.getValue());
            }
        }
        return false;
    }

    public static boolean attributeValueContains(XmlElement element, String attributeName, String attributeValue) {
        if (element != null && attributeName != null && attributeValue != null) {
            Attribute attribute = getAttribute(element, attributeName);
            if (attribute != null) {
                return attribute.getValue().contains(attributeValue);
            }
        }
        return false;
    }

    public static void addAttributes(XmlElement element, String... strings) {
        for (int i = 0; i < strings.length; i += 2) {
            element.addAttribute(new Attribute(strings[i], strings[i + 1]));
        }
    }

    public static XmlElement newXmlElement(String elementName, String... strings) {
        XmlElement xmlElement = new XmlElement(elementName);
        addAttributes(xmlElement, strings);
        return xmlElement;
    }

    public static XmlElement newXmlElementWithComment(Context context, String elementName, String... strings) {
        XmlElement xmlElement = newXmlElement(elementName, strings);
        context.getCommentGenerator().addComment(xmlElement);
        return xmlElement;
    }

    public static XmlElement newXmlElementAddTo(XmlElement parent, String elementName, String... strings) {
        XmlElement xmlElement = newXmlElement(elementName, strings);
        parent.addElement(xmlElement);
        return xmlElement;
    }

    public static XmlElement newXmlElementWithCommentAddTo(Context context, XmlElement parent, String elementName, String... strings) {
        XmlElement xmlElement = newXmlElementWithComment(context, elementName, strings);
        parent.addElement(xmlElement);
        return xmlElement;
    }

    public static void addElements(XmlElement root, List<? extends Element> list) {
        for (Element element : list) {
            root.addElement(element);
        }
    }

    public static List<XmlElement> getXmlElements(XmlElement parentElement) {
        List<XmlElement> result = new ArrayList<>();
        for (Element element : parentElement.getElements()) {
            if (element instanceof XmlElement) {
                result.add((XmlElement) element);
            }
        }
        return result;
    }

    public static XmlElement findXmlElement(XmlElement parentElement, String attributeName, String attributeValue) {
        int i = findXmlElementIndex(parentElement, attributeName, attributeValue);
        return i < 0 ? null : (XmlElement) parentElement.getElements().get(i);

    }

    public static int findXmlElementIndex(XmlElement parentElement, String attributeName, String attributeValue) {
        List<Element> elements = parentElement.getElements();
        for (int i = 0; i < elements.size(); i++) {
            Element element = elements.get(i);
            if (element instanceof XmlElement) {
                XmlElement xmlElement = (XmlElement) element;
                Attribute attribute = getAttribute(xmlElement, attributeName);
                if (attribute != null && StringUtils.equals(attributeValue, attribute.getValue())) {
                    return i;
                }
            }
        }
        return -1;
    }

    public static Field findField(TopLevelClass topLevelClass, String fieldName) {
        for (Field field : topLevelClass.getFields()) {
            if (field.getName().equals(fieldName)) return field;
        }
        return null;
    }


    public static GeneratedJavaFile createGeneratedJavaFile(CompilationUnit compilationUnit, Context context) {
        return new GeneratedJavaFile(compilationUnit, context.getJavaClientGeneratorConfiguration().getTargetProject(), context.getJavaFormatter());
    }

    public static boolean containsColumn(IntrospectedTable table, String columnName) {
        return containsColumn(table.getAllColumns(), columnName);
    }

    public static boolean containsColumn(List<IntrospectedColumn> columns, String columnName) {
        IntrospectedColumn cl = findColumn(columns, columnName);
        return cl != null;
    }

    public static IntrospectedColumn findColumn(IntrospectedTable table, String columnName) {
        if (table == null) {
            return null;
        }
        return findColumn(table.getAllColumns(), columnName);
    }

    public static IntrospectedColumn findColumnSafe(IntrospectedTable table, String columnName, String errorMsg) {
        final IntrospectedColumn column = findColumn(table.getAllColumns(), columnName);
        if (column == null) {
            throw new RuntimeException("cannot find column " + columnName + " in table " + table.getTableConfiguration().getTableName() + errorMsg);
        }
        return column;
    }

    public static IntrospectedColumn findColumnSafe(IntrospectedTable table, String columnName) {
        return findColumnSafe(table, columnName, "");
    }

    public static IntrospectedColumn findColumn(List<IntrospectedColumn> columns, String columnName) {
        if (columnName != null) {
            for (IntrospectedColumn column : columns) {
                if (column.getActualColumnName().equals(columnName)) {
                    return column;
                }
            }
        }
        return null;
    }

    public static IntrospectedColumn findColumnByJavaProperty(IntrospectedTable table, String javaProperty) {
        if (table == null) {
            return null;
        }
        return findColumnByJavaProperty(table.getAllColumns(), javaProperty);
    }

    public static IntrospectedColumn findColumnByJavaProperty(List<IntrospectedColumn> columns, String javaProperty) {
        if (javaProperty != null) {
            for (IntrospectedColumn column : columns) {
                if (column.getJavaProperty().equals(javaProperty)) {
                    return column;
                }
            }
        }
        return null;
    }

    public static boolean containsColumns(IntrospectedTable table, List<IntrospectedColumn> columns) {
        for (IntrospectedColumn column : columns) {
            if (!containsColumn(table, column.getActualColumnName())) {
                return false;
            }
        }
        return true;
    }

    public static boolean containsField(TopLevelClass cl, String fieldName) {
        for (Field filed : cl.getFields()) {
            if (fieldName.equals(filed.getName())) {
                return true;
            }
        }
        return false;
    }

    public static IntrospectedTable findIntrospectedTable(List<IntrospectedTable> introspectedTables, String name) {
        for (IntrospectedTable it : introspectedTables) {
            if (it.getFullyQualifiedTable().getIntrospectedTableName().equals(name)) {
                return it;
            }
        }
        return null;
    }

    public static IntrospectedTable findIntrospectedTableSafe(List<IntrospectedTable> introspectedTables, String name) {
        return findIntrospectedTableSafe(introspectedTables, name, null);
    }

    public static IntrospectedTable findIntrospectedTableSafe(List<IntrospectedTable> introspectedTables, String name, String errorMessage) {
        for (IntrospectedTable it : introspectedTables) {
            if (it.getFullyQualifiedTable().getIntrospectedTableName().equals(name)) {
                return it;
            }
        }
        throw new RuntimeException(errorMessage != null ? errorMessage : "cannot find table " + name);
    }


    public static void addImportedType(TopLevelClass clazz) {
        clazz.addImportedType(clazz.getType());

        for (Method m : clazz.getMethods()) {
            for (Parameter parameter : m.getParameters()) {
                clazz.addImportedType(parameter.getType());
            }

            clazz.addImportedType(m.getReturnType());

            for (FullyQualifiedJavaType exceptionType : m.getExceptions()) {
                clazz.addImportedType(exceptionType);
            }
        }

        for (Field field : clazz.getFields()) {
            clazz.addImportedType(field.getType());
        }

        for (FullyQualifiedJavaType interfaceType : clazz.getSuperInterfaceTypes()) {
            clazz.addImportedType(interfaceType);
        }

        if (clazz.getSuperClass() != null) {
            clazz.addImportedType(clazz.getSuperClass().getFullyQualifiedName());
        }
    }

    public static List<IntrospectedTable> getIntrospectedTables(Context context) {
        Object obj = context;
        java.lang.reflect.Field f = null;
        try {
            f = obj.getClass().getDeclaredField("introspectedTables");
            f.setAccessible(true);
            return (List<IntrospectedTable>) f.get(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

    }


    public static void initProperty(Object obj, Properties properties) {
        try {
            for (java.lang.reflect.Field f : obj.getClass().getDeclaredFields()) {
                f.setAccessible(true);
                if (f.getAnnotation(Property.class) != null) {
                    String propertyValue = properties.getProperty(f.getName());
                    if (propertyValue == null) {
                        propertyValue = f.getAnnotation(Property.class).value();
                        if (propertyValue == null) {
                            propertyValue = "";
                        }
                    }
                    if (f.getType().equals(boolean.class)) {
                        f.setBoolean(obj, "true".equalsIgnoreCase(propertyValue));
                    } else if (f.getType().isAssignableFrom(String.class)) {
                        f.set(obj, propertyValue);
                    } else {
                        throw new RuntimeException("Unknown type");
                    }
                } else if (f.getAnnotation(Config.class) != null) {
                    String propertyName = f.getAnnotation(Config.class).value();
                    String fileName = properties.getProperty(propertyName);


                    if (StringUtils.isBlank(fileName)) {
                        throw new RuntimeException("property '" + propertyName + "' is not defined");
                    }
                    File configFile = new File(fileName);
                    System.out.println("Load file config for class " + f.getType() + " from file " + fileName);

                    try {
                        JAXBContext context = JAXBContext.newInstance(f.getType());
                        Unmarshaller unmarshaller = context.createUnmarshaller();
                        unmarshaller.setEventHandler(new ValidationEventHandler() {
                            @Override
                            public boolean handleEvent(ValidationEvent event) {
                                return false;
                            }
                        });
                        f.set(obj, unmarshaller.unmarshal(configFile));
                    } catch (JAXBException e) {
                        throw new RuntimeException("Error while parsing file '" + fileName + "'", e);
                    }

                }
            }


        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static FullyQualifiedJavaType createGenericType(String type, String... types) {
        FullyQualifiedJavaType fullyType = new FullyQualifiedJavaType(type);
        for (String tp : types) {
            fullyType.addTypeArgument(new FullyQualifiedJavaType(tp));
        }
        return fullyType;
    }

    public static void closeConnection(java.sql.Connection connection) {
        try {
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static String joinTablesByPrimaryKeyGetWhere(IntrospectedTable t1, IntrospectedTable t2, boolean doesThrowException) {
        List<String> fieldList = joinTablesByPrimaryKey(t1, t2);
        if (fieldList == null && doesThrowException) {
            throw new RuntimeException(String.format("You can't link tables %s and %s for primary key", t1.getFullyQualifiedTableNameAtRuntime(), t2.getFullyQualifiedTableNameAtRuntime()));
        }
        StringBuilder sb = new StringBuilder();
        for (String fieldName : fieldList) {
            if (sb.length() > 0) {
                sb.append(" and ");
            }
            sb.append(t1.getTableConfiguration().getAlias()).append('.').append(fieldName)
                    .append(" = ")
                    .append(t2.getTableConfiguration().getAlias()).append('.').append(fieldName)
            ;
        }
        return sb.toString();
    }

    public static String joinTablesByPrimaryKeyGetWhere(IntrospectedTable t1, IntrospectedTable t2) {
        //поля первичного ключа из t2 должны быть в таблице t1 (поля должны совпадать по имени и типу (jdbcType))
        //или наоборот
        return joinTablesByPrimaryKeyGetWhere(t1, t2, false);
    }

    public static List<String> joinTablesByPrimaryKey(IntrospectedTable t1, IntrospectedTable t2) {
        //поля первичного ключа из t2 должны быть в таблице t1 (поля должны совпадать по имени и типу (jdbcType))
        //или наоборот

        List<String> result = joinTableToOtherTableByPrimaryKey(t1, t2);
        return result != null ? result : joinTableToOtherTableByPrimaryKey(t1, t1);
    }

    public static List<String> joinTableToOtherTableByPrimaryKey(IntrospectedTable t1, IntrospectedTable t2) {
        //поля первичного ключа из t2 должны быть в таблице t1 (поля должны совпадать по имени и типу (jdbcType))
        if (t2.getPrimaryKeyColumns().size() == 0) {
            return null;
        }
        List<String> list = new ArrayList<String>(t2.getPrimaryKeyColumns().size());
        for (IntrospectedColumn c2 : t2.getPrimaryKeyColumns()) {
            IntrospectedColumn c1 = findColumn(t1, c2.getActualColumnName());
            if (c1 == null || c1.getJdbcType() != c2.getJdbcType()) {
                return null;
            }
            list.add(c2.getActualColumnName());
        }
        return list;
    }

    public static String getAliasedFullyQualifiedTableName(IntrospectedTable introspectedTable) {
        TableConfiguration tableConfiguration = introspectedTable.getTableConfiguration();
        return tableConfiguration.getSchema() + "." + tableConfiguration.getTableName() + " " + tableConfiguration.getAlias();
    }

    public static String getTypeName(String fullTypeName) {
        int i = fullTypeName.lastIndexOf('.');
        return i < 0 ? fullTypeName : fullTypeName.substring(i + 1);
    }

    public static String getTypePackage(String fullTypeName) {
        int i = fullTypeName.lastIndexOf('.');
        return i < 0 ? fullTypeName : fullTypeName.substring(0, i);
    }

    public static String getPrimaryKeyAsDeclaredParam(IntrospectedTable introspectedTable) {
        StringBuilder sb = new StringBuilder();
        for (IntrospectedColumn column : introspectedTable.getPrimaryKeyColumns()) {
            if (sb.length() > 0) {
                sb.append(", ");
            }
            sb.append(column.getFullyQualifiedJavaType().getShortName()).append(' ').append(column.getJavaProperty());
        }
        return sb.toString();
    }

    public static String getPrimaryKeyAsParam(IntrospectedTable introspectedTable, boolean isWithComma) {
        StringBuilder sb = new StringBuilder();
        for (IntrospectedColumn column : introspectedTable.getPrimaryKeyColumns()) {
            if (sb.length() > 0 || isWithComma) {
                sb.append(", ");
            }
            sb.append(column.getJavaProperty());
        }
        return sb.toString();
    }

    public static String getPrimaryKeyAsParamByGetter(IntrospectedTable introspectedTable, String objectName, boolean isWithComma) {
        StringBuilder sb = new StringBuilder();
        for (IntrospectedColumn column : introspectedTable.getPrimaryKeyColumns()) {
            if (sb.length() > 0 || isWithComma) {
                sb.append(", ");
            }
            sb.append(objectName).append(".get").append(PluginUtils.toClassName(column.getJavaProperty())).append("()");
        }
        return sb.toString();
    }

    public static Element fullCopyElement(Element original) {
        if (original instanceof XmlElement) {
            XmlElement xmlElementOriginal = (XmlElement) original;
            XmlElement copy = new XmlElement(xmlElementOriginal.getName());
            for (Element item : xmlElementOriginal.getElements()) {
                copy.addElement(fullCopyElement(item));
            }
            for (Attribute attribute : xmlElementOriginal.getAttributes()) {
                copy.addAttribute(new Attribute(attribute.getName(), attribute.getValue()));
            }
            return copy;
        }

        return new TextElement(((TextElement) original).getContent());
    }

    public static XmlElement fullCopyXmlElement(XmlElement original) {
        return (XmlElement) fullCopyElement(original);
    }


    public static void addOffsetLimit(XmlElement element) {
        XmlElement ifElement = newXmlElementAddTo(element, "if", "test", "offset > 0");
        ifElement.addElement(new TextElement("offset #{offset}"));
        ifElement = newXmlElementAddTo(element, "if", "test", "limit > 0");
        ifElement.addElement(new TextElement("limit #{limit}"));
    }
}


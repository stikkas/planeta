package ru.planeta.mybatis.generator.plugins;

import org.mybatis.generator.api.IntrospectedTable;
import org.mybatis.generator.api.dom.java.FullyQualifiedJavaType;
import org.mybatis.generator.api.dom.java.TopLevelClass;
import org.mybatis.generator.api.dom.xml.TextElement;
import org.mybatis.generator.api.dom.xml.XmlElement;
import ru.planeta.mybatis.generator.plugins.utils.FieldGetterSetter;
import ru.planeta.mybatis.generator.plugins.utils.PluginUtils;

import static ru.planeta.mybatis.generator.plugins.utils.PluginUtils.*;

/**
 * Created by kostiagn on 20.07.2015.
 */
public class AddOffsetLimitToExamplePlugin extends MyPluginAdapter {
    @Override
    public boolean modelExampleClassGenerated(TopLevelClass topLevelClass, IntrospectedTable introspectedTable) {
        FieldGetterSetter.addToClass(topLevelClass, FullyQualifiedJavaType.getIntInstance(), "offset");
        FieldGetterSetter.addToClass(topLevelClass, FullyQualifiedJavaType.getIntInstance(), "limit");
        return true;
    }

    @Override
    public boolean sqlMapSelectByExampleWithoutBLOBsElementGenerated(XmlElement element, IntrospectedTable introspectedTable) {
        addOffsetLimit(element);
        return true;
    }

    @Override
    public boolean sqlMapSelectByExampleWithBLOBsElementGenerated(XmlElement element, IntrospectedTable introspectedTable) {
        addOffsetLimit(element);
        return true;
    }
}

package ru.planeta.mybatis.generator.plugins;

import org.apache.commons.lang3.StringUtils;
import org.mybatis.generator.api.GeneratedJavaFile;
import org.mybatis.generator.api.GeneratedXmlFile;
import org.mybatis.generator.api.IntrospectedColumn;
import org.mybatis.generator.api.IntrospectedTable;
import org.mybatis.generator.api.dom.java.*;
import org.mybatis.generator.api.dom.xml.Document;
import org.mybatis.generator.api.dom.xml.Element;
import org.mybatis.generator.api.dom.xml.TextElement;
import org.mybatis.generator.api.dom.xml.XmlElement;
import ru.planeta.mybatis.generator.plugins.annotation.Config;
import ru.planeta.mybatis.generator.plugins.annotation.Init;
import ru.planeta.mybatis.generator.plugins.annotation.Property;
import ru.planeta.mybatis.generator.plugins.model.ExTableConfig;
import ru.planeta.mybatis.generator.plugins.model.ExTableConfig.ExTable;
import ru.planeta.mybatis.generator.plugins.utils.FieldGetterSetter;
import ru.planeta.mybatis.generator.plugins.utils.IntrospectedTableCreator;
import ru.planeta.mybatis.generator.plugins.utils.PluginUtils;

import java.util.*;

import static ru.planeta.mybatis.generator.plugins.utils.PluginUtils.*;
import static ru.planeta.mybatis.generator.plugins.utils.PluginUtils.attributeValueEquals;

/**
 * Created by kostiagn on 15.07.2015.
 */

public class AddExTablePlugin extends MyPluginAdapter {
    @Init
    private List<IntrospectedTable> introspectedTables;

    @Property
    private String modelPackage;
    @Property
    private boolean enableSubPackages;
    @Config("configFile")
    private ExTableConfig exTableConfig;


    private Map<IntrospectedTable, XmlElement> baseColumnListElementList = new HashMap<>();
    private Map<IntrospectedTable, Document> sqlMapDocumentMap = new HashMap<>();

    @Override
    public boolean sqlMapBaseColumnListElementGenerated(XmlElement element, IntrospectedTable introspectedTable) {
        baseColumnListElementList.put(introspectedTable, element);
        return true;
    }

    @Override
    public boolean sqlMapDocumentGenerated(Document document, IntrospectedTable introspectedTable) {
        sqlMapDocumentMap.put(introspectedTable, document);
        return true;
    }

    public List<GeneratedXmlFile> contextGenerateAdditionalXmlFiles() {
        for (IntrospectedTable introspectedTable : sqlMapDocumentMap.keySet()) {
            String tableName = PluginUtils.extractTableName(introspectedTable);
            for (ExTableConfig.ExTable inheritedTable : exTableConfig.getInheritedTableList(tableName)) {
                addToMapperXml(sqlMapDocumentMap.get(introspectedTable), inheritedTable);
            }
        }
        return null;
    }

    @Override
    public List<GeneratedJavaFile> contextGenerateAdditionalJavaFiles() {
        List<GeneratedJavaFile> result = new ArrayList<>();

        for (IntrospectedTable introspectedTable : introspectedTables) {
            String tableName = PluginUtils.extractTableName(introspectedTable);

            for (ExTableConfig.ExTable inheritedTable : exTableConfig.getInheritedTableList(tableName)) {
                result.add(createModelClass(inheritedTable));
            }
        }
        return result;
    }

    private void addFieldToModelClass(TopLevelClass clazz, ExTableConfig.ExTable exTable) {
        for (ExTableConfig.Field field : exTable.getFieldList()) {
            FieldGetterSetter.addToClass(clazz, new FullyQualifiedJavaType(field.getJavaType()), field.getName());
        }
    }

    private GeneratedJavaFile createModelClass(ExTableConfig.ExTable exTable) {
        TopLevelClass clazz = new TopLevelClass(getPackage(modelPackage, exTable.getFullTableName()) + "." + exTable.getTypeName());
        clazz.setVisibility(JavaVisibility.PUBLIC);
        clazz.setSuperClass(exTable.getIntrospectedTable().getBaseRecordType());

        for (ExTableConfig.Relationship relationship : exTable.getRelationshipList()) {


            String type = relationship.getType();
            if (StringUtils.isBlank(relationship.getType())) {
                final IntrospectedTable relationshipIntrospectedTable = PluginUtils.findIntrospectedTable(introspectedTables, relationship.getTableName());
                boolean isGeneratedTable = relationshipIntrospectedTable != null;
                if (isGeneratedTable) {
                    type = relationshipIntrospectedTable.getBaseRecordType();
                } else {
                    final ExTable relationshipExTable = exTableConfig.findTable(relationship.getTableName());
                    if (relationshipExTable != null) {
                        type = relationshipExTable.getFullTypeName();
                    } else {
                        throw new RuntimeException("type is blank. exTable.name = " + exTable.getName() + " relationship.table = " + relationship.getTable());
                    }
                }
            }
            FullyQualifiedJavaType fullyQualifiedJavaType = new FullyQualifiedJavaType(type);
            if (relationship instanceof ExTableConfig.Collection) {
                fullyQualifiedJavaType = FullyQualifiedJavaType.getNewListInstance();
                fullyQualifiedJavaType.addTypeArgument(new FullyQualifiedJavaType(type));
            }
            FieldGetterSetter.addToClass(clazz, fullyQualifiedJavaType, relationship.getName());
        }
        addFieldToModelClass(clazz, exTable);

        return PluginUtils.createGeneratedJavaFile(clazz, this.getContext());

    }


    private String getPackage(String pck, String fullTableName) {
        if (enableSubPackages) {
            pck += '.' + PluginUtils.extractSchemaName(fullTableName);
        }
        return pck;
    }


    private void addToMapperXml(Document document, ExTableConfig.ExTable exTable) {
        XmlElement rootElement = document.getRootElement();

        XmlElement resultMap = createResultMap(exTable);
        rootElement.addElement(resultMap);
        addBaseColumnListToMapperXml(rootElement, exTable);
        addJoinToMapperXml(rootElement, exTable);

        addFieldToResultMap(resultMap, exTable);

        addRelationshipToResultMap(rootElement, resultMap, exTable);


        List<XmlElement> newElementList = copyAllTagSelect(rootElement, exTable);

        addElements(rootElement, newElementList);
    }

    private void addBaseColumnListToMapperXml(XmlElement rootElement, ExTable exTable) {
        XmlElement baseColumnListElement = new XmlElement(baseColumnListElementList.get(exTable.getIntrospectedTable()));

        setAttribute(baseColumnListElement, "id", exTable.getBaseColumnListId());

        StringBuilder sb = new StringBuilder();
        for (ExTableConfig.Field field : exTable.getFieldList()) {
            sb.append("\n, ");
            if (field.isCount()) {
                sb.append("(SELECT COUNT(*) FROM " + getInnerSelect(exTable, field) + ")");
            } else {
                sb.append(field.getFullColumnName());
            }
            sb.append(" ").append(field.getColumnAlias());
        }
        baseColumnListElement.addElement(new TextElement(sb.toString()));
        rootElement.addElement(baseColumnListElement);
        context.getCommentGenerator().addComment(baseColumnListElement);
    }

    private String getInnerSelect(ExTable exTable, ExTableConfig.Field field) {
        final ExTableConfig.JoinTable joinTable = exTable.findJoinTableByAliasSafe(field.getJoinTableAlias());
        final IntrospectedTable introspectedTable = IntrospectedTableCreator.getSafe(context, joinTable.getTableName());
        final String[] columnNames = getJoinOnColumnNames(exTable.getIntrospectedTable(), introspectedTable, joinTable.getJoinOn());
        return joinTable.getTableName() + " " + joinTable.getAlias()
                + " WHERE "
                + joinTable.getAlias() + '.' + columnNames[0]
                + " = "
                + exTable.getIntrospectedTable().getTableConfiguration().getAlias() + '.' + columnNames[1];
    }


    private void addFieldToResultMap(XmlElement resultMap, ExTableConfig.ExTable exTable) {
        //<result column="COLUMN_NAME" property="FIELD_NAME" />
        for (ExTableConfig.Field field : exTable.getFieldList()) {
            if (PluginUtils.findColumnByJavaProperty(exTable.getIntrospectedTable(), field.getName()) != null) {
                throw new RuntimeException("Field " + field.getName() + " already exist. exTable " + exTable.getName());
            }
            final ExTableConfig.JoinTable joinTable = exTable.findJoinTableByAliasSafe(field.getJoinTableAlias());
            if (!field.isCount()) {
                PluginUtils.findColumnSafe(IntrospectedTableCreator.getSafe(context, joinTable.getTableName()), field.getColumn());
            }
            XmlElement resultElement = newXmlElement("result", "column", field.getColumnAlias(), "property", field.getName());
            resultMap.addElement(resultElement);
        }
    }

    private void addRelationshipToResultMap(XmlElement rootElement, XmlElement resultMap, ExTable exTable) {
        for (ExTableConfig.Relationship relationship : exTable.getRelationshipList()) {
            //<association property="profile" column="profile_id" select="cluster.profiledb.selectProfileById"></association>
            XmlElement relationshipElement = new XmlElement(relationship.getRelationshipName());


            IntrospectedColumn column = findColumnSafe(exTable.getIntrospectedTable(), relationship.getColumn());

            String columnName = column.getActualColumnName();
            if (StringUtils.isNotBlank(column.getTableAlias())) {
                columnName = column.getTableAlias() + "_" + column.getActualColumnName();
            }


            String selectId = relationship.getSelectId();
            if (StringUtils.isBlank(selectId)) {
                if (StringUtils.isNotBlank(relationship.getTable())) {
                    selectId = addSelectForRelationship(exTable, relationship);
                } else {
                    throw new RuntimeException("addRelationshipToResultMap: selectId is blank. exTable.name = " + exTable.getName() + " relationship.tableName = " + relationship.getName());
                }
            }
            addAttributes(relationshipElement
                    , "property", relationship.getName()
                    , "column", columnName
                    , "select", selectId);
            resultMap.addElement(relationshipElement);
        }
    }

    private String addSelectForRelationship(ExTableConfig.ExTable exTable, ExTableConfig.Relationship relationship) {
        IntrospectedTable introspectedTable = PluginUtils.findIntrospectedTable(introspectedTables, relationship.getTableName());
        ExTable relationshipExTable = exTableConfig.findTable(relationship.getTableName());
        boolean isExTable = relationshipExTable != null;
        if (introspectedTable == null && relationshipExTable == null) {
            throw new RuntimeException("addSelectForRelationship: cannot find table or exTable '" + relationship.getTable() + "'");
        }

        if (isExTable) {
            introspectedTable = relationshipExTable.getIntrospectedTable();
        }

        XmlElement rootElement = sqlMapDocumentMap.get(introspectedTable).getRootElement();

        String selectId = String.format("select%sFor%sBy%s", PluginUtils.toCamelCase(relationship.getTableName(), true), exTable.getTypeName(), PluginUtils.toCamelCase(relationship.getColumn(), true));
        XmlElement selectElement = newXmlElementWithCommentAddTo(getContext(), rootElement, "select"
                , "id", selectId
                , "resultMap", isExTable ? relationshipExTable.getResultMapId() : introspectedTable.getBaseResultMapId()
        );

        selectElement.addElement(new TextElement("SELECT"));
        newXmlElementAddTo(selectElement, "include", "refid", isExTable ? relationshipExTable.getBaseColumnListId() : introspectedTable.getBaseColumnListId());
        selectElement.addElement(new TextElement("FROM " + introspectedTable.getFullyQualifiedTableNameAtRuntime()));
        if (isExTable) {
            newXmlElementAddTo(selectElement, "include", "refid", relationshipExTable.getJoinTableSqlRefId());
        }
        selectElement.addElement(new TextElement(String.format("WHERE %s = #{%s}", relationship.getColumn(), relationship.getColumn())));
        if (relationship instanceof ExTableConfig.Collection) {
            ExTableConfig.Collection collection = (ExTableConfig.Collection) relationship;
            if (StringUtils.isNotBlank(collection.getOrderBy())) {
                String orderBy = checkOrderByAndAddTableAlias(introspectedTable, collection.getOrderBy());
                selectElement.addElement(new TextElement("ORDER BY " + orderBy));
            }
        }

        return introspectedTable.getMyBatis3SqlMapNamespace() + "." + selectId;
    }

    private String checkOrderByAndAddTableAlias(IntrospectedTable introspectedTable, String orderBy) {
        StringBuilder sb = new StringBuilder();
        for (String item : orderBy.split(",")) {
            String[] parts = item.trim().split(" ");
            if (parts.length != 1 && parts.length != 2) {
                throw new RuntimeException("Bad orderBy '" + orderBy + "'");
            }
            if (parts.length == 2 && !(parts[1].equalsIgnoreCase("asc") || parts[1].equalsIgnoreCase("desc"))) {
                throw new RuntimeException("Bad orderBy '" + orderBy + "'. Unknown word " + parts[1]);
            }
            PluginUtils.findColumnSafe(introspectedTable, parts[0], ". Bad orderBy '" + orderBy + "'");
            if (sb.length() > 0) {
                sb.append(", ");
            }
            sb.append(introspectedTable.getTableConfiguration().getAlias()).append('.').append(parts[0]);
            if (parts.length == 2) {
                sb.append(' ').append(parts[1]);
            }
        }
        return sb.toString();
    }

    private XmlElement createResultMap(ExTableConfig.ExTable exTable) {
        //<resultMap id="PaymentErrorsExResultMap" type="ru.planeta.model.common.PaymentErrorsEx" extends="ru.planeta.dao.generated.commondb.PaymentErrorsMapper.BaseResultMap">
        XmlElement resultMap = newXmlElementWithComment(context, "resultMap"
                , "id", exTable.getResultMapId()
                , "type", exTable.getFullTypeName()
                , "extends", exTable.getIntrospectedTable().getMyBatis3JavaMapperType() + "." + exTable.getIntrospectedTable().getBaseResultMapId());
        return resultMap;

    }

    private List<XmlElement> copyAllTagSelect(XmlElement root, ExTableConfig.ExTable exTable) {
        List<XmlElement> newElementList = new ArrayList<>();
        for (Element element : root.getElements()) {
            if (!(element instanceof XmlElement)) {
                continue;
            }

            XmlElement xmlElement = (XmlElement) element;
            if (xmlElement.getName().equals("select") && !attributeValueContains(xmlElement, "id", "Rowbounds") && attributeValueEquals(xmlElement, "resultMap", exTable.getIntrospectedTable().getBaseResultMapId())) {
                XmlElement copy = PluginUtils.fullCopyXmlElement(xmlElement);
                context.getCommentGenerator().addComment(copy);
                setAttribute(copy, "id", getSelectMethodName(getAttribute(copy, "id").getValue(), exTable.getTypeName()));
                setAttribute(copy, "resultMap", exTable.getResultMapId());
                changeBaseColumnListName(copy, exTable);

                addJoinTableAfterFrom(copy, exTable);
                newElementList.add(copy);
            }

        }
        return newElementList;
    }

    private void changeBaseColumnListName(XmlElement selectElement, ExTableConfig.ExTable exTable) {
        XmlElement baseColumnListElement = findXmlElement(selectElement, "refid", "Base_Column_List");
        if (baseColumnListElement != null) {
            setAttribute(baseColumnListElement, "refid", exTable.getBaseColumnListId());
        }
    }

    private int getIndexOfFromTextElement(XmlElement parentElement) {
        List<Element> elements = parentElement.getElements();
        for (int i = 0; i < elements.size(); i++) {
            Element element = elements.get(i);
            if (element instanceof TextElement) {
                TextElement textElement = (TextElement) element;
                if (textElement.getContent().trim().toLowerCase().startsWith("from ")) {
                    return i;
                }
            }
        }
        return -1;
    }


    private String[] getJoinOnColumnNames(IntrospectedTable parentIntrospectedTable, IntrospectedTable introspectedTable, String joinOn) {
        if (StringUtils.isBlank(joinOn)) {
            String columnName = getFieldNameToJoinByOneColumnPrimaryKey(parentIntrospectedTable, introspectedTable);
            if (columnName == null) {
                columnName = getFieldNameToJoinByOneColumnPrimaryKey(introspectedTable, parentIntrospectedTable);
                if (columnName == null) {
                    throw new RuntimeException("cannot join tables " + parentIntrospectedTable.getTableConfiguration().getTableName() + " and " + introspectedTable.getTableConfiguration().getTableName()
                            + ". One table must have a primary key consisting of one column and other table must have a column with the same name and type");
                }
            }
            return new String[]{columnName, columnName};
        }
        if (!joinOn.contains("=")) {
            String columnName = joinOn.trim();
            PluginUtils.findColumnSafe(parentIntrospectedTable, columnName);
            PluginUtils.findColumnSafe(introspectedTable, columnName);
            return new String[]{columnName, columnName};
        }

        final String[] parts = joinOn.split("=");
        if (parts.length != 2) {
            throw new RuntimeException("joinOn expression '" + joinOn + "' must have on symbol '='");
        }
        String firstColumnName = parts[0].trim();
        String secondColumnName = parts[1].trim();
        PluginUtils.findColumnSafe(introspectedTable, firstColumnName);
        PluginUtils.findColumnSafe(parentIntrospectedTable, secondColumnName);
        return new String[]{firstColumnName, secondColumnName};
    }

    private String getFieldNameToJoinByOneColumnPrimaryKey(IntrospectedTable it1, IntrospectedTable it2) {
        if (it1.getPrimaryKeyColumns().size() == 1) {
            final IntrospectedColumn primaryKeyColumn = it1.getPrimaryKeyColumns().get(0);
            IntrospectedColumn column = PluginUtils.findColumn(it2, primaryKeyColumn.getActualColumnName());
            if (column != null && column.getJdbcType() == primaryKeyColumn.getJdbcType()) {
                return column.getActualColumnName();
            }
        }
        return null;
    }

    private void join(IntrospectedTable parentIntrospectedTable, String parentAlias, ExTableConfig.JoinTable joinTable, Map<String, String> joinMap) {

        IntrospectedTable introspectedTable = IntrospectedTableCreator.getSafe(context, joinTable.getTableName());
        String[] columnNames = getJoinOnColumnNames(parentIntrospectedTable, introspectedTable, joinTable.getJoinOn());

//        if (joinMap.containsKey(joinTable.getAlias())) {
//            throw new RuntimeException("joinList contains yet table with alias '" + joinTable.getAlias() + "'");
//        }
        if (!joinMap.containsKey(joinTable.getAlias())) {
            joinMap.put(joinTable.getAlias(), (joinTable.isLeftJoin() ? "LEFT " : "") + "JOIN " + joinTable.getTableName() + " " + joinTable.getAlias() + " ON " + joinTable.getAlias() + '.' + columnNames[0] + " = " + parentAlias + '.' + columnNames[1]);
        }

        for (ExTableConfig.JoinTable jt : joinTable.getJoinTableList()) {
            join(introspectedTable, joinTable.getAlias(), jt, joinMap);
        }

    }

    private void addJoinToMapperXml(XmlElement root, ExTableConfig.ExTable exTable) {
        Map<String, String> joinMap = new LinkedHashMap<>();
        for (ExTableConfig.Field field : exTable.getFieldList()) {
            if (!field.isCount()) {
                join(exTable.getIntrospectedTable(), exTable.getIntrospectedTable().getTableConfiguration().getAlias(), exTable.findJoinTableByAliasSafe(field.getJoinTableAlias()), joinMap);
            }
        }


        StringBuilder sb = new StringBuilder();
        for (String s : joinMap.values()) {
            sb.append(s).append("\r\n");
        }
        if (sb.length() > 0) {
            XmlElement sqlJoin = newXmlElementWithCommentAddTo(context, root, "sql", "id", exTable.getJoinTableSqlRefId());
            sqlJoin.addElement(new TextElement(sb.toString()));

        }

    }

    private void addJoinTableAfterFrom(XmlElement selectElement, ExTableConfig.ExTable exTable) {
        if (StringUtils.isNotBlank(exTable.getJoinTableSqlRefId())) {
            if (selectElement.getName().startsWith("select") && exTable.getFieldList().size() > 0) {
                int i = getIndexOfFromTextElement(selectElement);
                if (i >= 0) {
                    selectElement.addElement(i + 1, newXmlElement("include", "refid", exTable.getJoinTableSqlRefId()));
                }
            }
        }
    }


    private String getSelectMethodName(String baseSelectMethodName, String typeName) {
        if (baseSelectMethodName.startsWith("select")) {
            return "select" + typeName + baseSelectMethodName.substring(6);
        }
        return baseSelectMethodName + typeName;
    }

    @Override
    public void initialized(IntrospectedTable introspectedTable) {
        String tableName = PluginUtils.extractTableName(introspectedTable);
        for (ExTableConfig.ExTable inheritedTable : exTableConfig.getInheritedTableList(tableName)) {
            inheritedTable.setIntrospectedTable(introspectedTable);
            PluginUtils.exTable.put(inheritedTable, tableName);
        }
    }

    @Override
    public boolean clientGenerated(Interface interfaze, TopLevelClass topLevelClass, IntrospectedTable introspectedTable) {
        String tableName = PluginUtils.extractTableName(introspectedTable);
        List<Method> methodListToCopy = new ArrayList<>();
        for (Method method : interfaze.getMethods()) {
            if (method.getName().startsWith("select") && !method.getName().contains("Rowbounds")) {
                methodListToCopy.add(method);
            }
        }
        for (ExTableConfig.ExTable inheritedTable : exTableConfig.getInheritedTableList(tableName)) {
            addToMapperJava(interfaze, inheritedTable, methodListToCopy);
        }

        return true;
    }

    private void addToMapperJava(Interface interfaze, ExTableConfig.ExTable exTable, List<Method> methodListToCopy) {
        for (Method method : methodListToCopy) {
            Method copy = new Method(method);
            copy.setName(getSelectMethodName(copy.getName(), exTable.getTypeName()));
            copy.setReturnType(new FullyQualifiedJavaType(method.getReturnType().getFullyQualifiedName().replace(exTable.getMainTypeName(), exTable.getFullTypeName())));
            interfaze.addImportedType(new FullyQualifiedJavaType(exTable.getFullTypeName()));
            interfaze.addMethod(copy);
            System.out.println(copy.getName());
        }

    }


}

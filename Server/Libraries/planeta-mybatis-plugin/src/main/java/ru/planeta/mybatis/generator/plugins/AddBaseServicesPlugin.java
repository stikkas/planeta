package ru.planeta.mybatis.generator.plugins;

import org.apache.commons.lang3.StringUtils;
import org.mybatis.generator.api.GeneratedJavaFile;
import org.mybatis.generator.api.IntrospectedColumn;
import org.mybatis.generator.api.IntrospectedTable;
import org.mybatis.generator.api.dom.java.*;
import org.springframework.util.CollectionUtils;
import ru.planeta.mybatis.generator.plugins.annotation.Config;
import ru.planeta.mybatis.generator.plugins.annotation.Init;
import ru.planeta.mybatis.generator.plugins.annotation.Property;
import ru.planeta.mybatis.generator.plugins.enums.ComparisonOperator;
import ru.planeta.mybatis.generator.plugins.model.BaseServicesConfig;
import ru.planeta.mybatis.generator.plugins.model.ExTableConfig;
import ru.planeta.mybatis.generator.plugins.model.FiltersConfig;
import ru.planeta.mybatis.generator.plugins.utils.PluginUtils;
import ru.planeta.mybatis.generator.plugins.utils.VarReplacer;

import javax.xml.stream.events.Comment;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static ru.planeta.mybatis.generator.plugins.utils.PluginUtils.getTypeName;

public class AddBaseServicesPlugin extends MyPluginAdapter {
    @Init
    private List<IntrospectedTable> introspectedTables;
    private Map<String, TopLevelClass> services = new HashMap<String, TopLevelClass>();
    @Property
    private String targetPackage;
    @Property
    private String filtersPackage;
    @Property
    private String superClass;
    @Property
    private boolean enableSubPackages;
    @Property("Base")
    private String serviceNamePrefix;
    @Property("Service")
    private String serviceNameSuffix;
    @Property
    private String annotation;
    @Config("configFile")
    private BaseServicesConfig baseServicesConfig;

    @Override
    public List<GeneratedJavaFile> contextGenerateAdditionalJavaFiles(IntrospectedTable introspectedTable) {
        List<GeneratedJavaFile> result = new ArrayList<>();

        String tableName = PluginUtils.extractTableName(introspectedTable);
        BaseServicesConfig.Table configTable = baseServicesConfig.findTable(tableName);

        if (configTable == null) {
            return result;
        }


        TopLevelClass clazz = createService(introspectedTable, configTable);
        if (clazz != null) {
            result.add(PluginUtils.createGeneratedJavaFile(clazz, this.getContext()));
            for (ExTableConfig.ExTable exTable : PluginUtils.exTable.keySet()) {
                if (PluginUtils.exTable.get(exTable).equals(tableName)) {
                    final BaseServicesConfig.Table baseServicesConfigTable = baseServicesConfig.findTable(exTable.getName());
                    if (baseServicesConfigTable != null) {
                        addToService(introspectedTable, baseServicesConfigTable, clazz, false);
                    }
                }
            }
        }
        return result;
    }

    private TopLevelClass createService(IntrospectedTable introspectedTable, BaseServicesConfig.Table configTable) {
        String fullTableName = introspectedTable.getFullyQualifiedTableNameAtRuntime();
        String fullTypeName = introspectedTable.getBaseRecordType();
        String typeName = getTypeName(fullTypeName);

        TopLevelClass clazz = new TopLevelClass(getPackage(targetPackage, fullTableName) + "." + serviceNamePrefix + typeName + serviceNameSuffix);
        clazz.setVisibility(JavaVisibility.PUBLIC);
        clazz.setAbstract(true);
        if (!superClass.isEmpty()) {
            clazz.setSuperClass(new FullyQualifiedJavaType(superClass));
        }
        return addToService(introspectedTable, configTable, clazz, true);
    }

    private TopLevelClass addToService(IntrospectedTable introspectedTable, BaseServicesConfig.Table configTable, TopLevelClass clazz, boolean isMainTable) {
        String fullTableName = introspectedTable.getFullyQualifiedTableNameAtRuntime();
        String tableName = PluginUtils.extractTableName(fullTableName);

        String fullTypeName = introspectedTable.getBaseRecordType();
        String typeName = getTypeName(fullTypeName);

        String mainTableName = tableName;

        String mainTypeName = typeName;
        String fullMainTypeName = fullTypeName;

        if (!isMainTable) {

            tableName = configTable.getName();
            fullTableName = introspectedTable.getTableConfiguration().getSchema() + "." + tableName;

            typeName = PluginUtils.toCamelCase(tableName, true);
            fullTypeName = PluginUtils.getTypePackage(introspectedTable.getBaseRecordType()) + "." + typeName;

        }


        String objectName = PluginUtils.toObjectName(typeName);
        String fullMapperName = introspectedTable.getMyBatis3JavaMapperType();
        String mapperName = getTypeName(fullMapperName);
        String inParam = fullTypeName + " " + objectName;
        String exampleFullTypeName = introspectedTable.getExampleType();


        Method method;

        String pkField = "";
        if (introspectedTable.getPrimaryKeyColumns().size() == 1) {
            pkField = introspectedTable.getPrimaryKeyColumns().get(0).getActualColumnName();
        }

        FullyQualifiedJavaType typeListBase = PluginUtils.createGenericType("java.util.List", fullTypeName);

        VarReplacer vars = new VarReplacer();
        vars.put("mapper", mapperName);
        vars.put("fullMapperName", fullMapperName);
        vars.put("obj", objectName);
        vars.put("mainObj", mainTypeName);
        vars.put("childObj", isMainTable ? "" : typeName);
        vars.put("pkField", pkField);

        if (isMainTable) {
            addCreateUpdateDeleteMethods(clazz, introspectedTable, inParam);

            clazz.addField(PluginUtils.createField("@Autowired protected", vars.replace("$fullMapperName$ $mapper$")));
            clazz.addImportedType("org.springframework.beans.factory.annotation.Autowired");
            if (!pkField.isEmpty()) {
                method = PluginUtils.createMethod("getOrderByClause", "public String");
                method.addBodyLine("return \"$pkField$\";");
                clazz.addMethod(method);
            }
        }

        addSelectByPrimaryKey(clazz, introspectedTable, fullTypeName);

        List<BaseServicesConfig.Method> configMethodList = configTable.getMethodList();
        if (CollectionUtils.isEmpty(configMethodList)) {
            configMethodList = new ArrayList<>();
            configMethodList.add(new BaseServicesConfig.Method());
        }
        for (BaseServicesConfig.Method configMethod : configMethodList) {
            InputParams inputParams = new InputParams(configMethod, introspectedTable);
            inputParams.addImportTypes(clazz);
            boolean isExistsParam = inputParams.inputParamList.size() > 0;
            //String ExampleClassName = PluginUtils.toClassName(ExampleName);

            vars.put("param", inputParams.getParamList());
            vars.put("param,", isExistsParam ? inputParams.getParamList() + ',' : "");
            vars.put("by", PluginUtils.toObjectName(inputParams.getSuffixFuncName()));
            vars.put("getExample", "get$MainObj$$By$Example");
            vars.put("selectList", "select" + typeName + "List" + PluginUtils.toClassName(inputParams.getSuffixFuncName()));

            String paramDeclaration = inputParams.getParamDeclaration();

            /*****/
            method = PluginUtils.createMethod("COMMENT_SECTION_$Obj$$By$", "private", paramDeclaration);
            addCommentSeparator(method, tableName, configMethod.getName(), paramDeclaration);
            method.addBodyLine("");
            clazz.addMethod(method);

            /*** getExample ***/
            method = PluginUtils.createMethod("$getExample$", new FullyQualifiedJavaType(exampleFullTypeName), "public", paramDeclaration, "int offset", "int limit");
            method.addBodyLine("$MainObj$Example example = new $MainObj$Example();");
            method.addBodyLine("example.setOffset(offset);");
            method.addBodyLine("example.setLimit(limit);");
            method.addBodyLine("example.or()");
            if (isExistsParam) {
                inputParams.addBodyLine(vars, method, ".and$OtherId$$Operation$($otherId$)");
            }
            method.addBodyLine(";");

            if(StringUtils.isNotBlank(configMethod.getOrderBy())){
                //todo check existing fields in getOrderBy
                method.addBodyLine("example.setOrderByClause(\"" + configMethod.getOrderBy() + "\");");
            }else if (!pkField.isEmpty()) {
                method.addBodyLine("example.setOrderByClause(getOrderByClause());");
            }
            method.addBodyLine("return example;");
            vars.replace(method);
            addMethodIfNotExists(clazz, method);


            if (isMainTable) {

                method = PluginUtils.createMethod("select$Obj$Count$By$", "public int", paramDeclaration);
                method.addBodyLine("return $mapper$.countByExample($getExample$($param,$ 0, 0));");
                clazz.addMethod(method);
            }


            method = PluginUtils.createMethod("$selectList$", typeListBase, "public", paramDeclaration, "int offset", "int limit");
            method.addBodyLine("return $mapper$.select$ChildObj$ByExample($getExample$($param,$ offset, limit));");

            clazz.addMethod(method);

            if (configMethod.isSelectOne()) {
                method = PluginUtils.createMethod("select$Obj$$By$", new FullyQualifiedJavaType(fullTypeName), "public", paramDeclaration);
                method.addBodyLine("List<$Obj$> list = $mapper$.select$ChildObj$ByExample($getExample$($param$, 0, 0));");
                method.addBodyLine("return list.size()>0 ? list.get(0) : null;");
                clazz.addMethod(method);
            }


            /***selectByIdList***/
            clazz.addImportedType("java.util.ArrayList");
            if (introspectedTable.getPrimaryKeyColumns().size() == 1 && introspectedTable.getPrimaryKeyColumns().get(0).getActualColumnName().equals(mainTableName + "_id")) {
                String pkJavaName = introspectedTable.getPrimaryKeyColumns().get(0).getJavaProperty("");
                vars.put("pkJavaName", pkJavaName);
                method = PluginUtils.createMethod("$selectList$", typeListBase, "public", paramDeclaration, "java.util.List<Long> " + pkJavaName + "List");
                method.addBodyLine("if ($pkJavaName$List == null || $pkJavaName$List.isEmpty()) return new ArrayList<$Obj$>();");
                method.addBodyLine("return $mapper$.select$ChildObj$ByIdList($pkJavaName$List);");
                clazz.addMethod(method);
            }

            vars.replace(clazz);
        }


        for (FiltersConfig.Filter filter : PluginUtils.filterTableList) {
            if (filter.getTableName().equals(mainTableName)) {
                /***selectListByFilter (  ) ****/
                String filterType = PluginUtils.toClassName(filter.getName());

                clazz.addImportedType(filtersPackage + "." + filterType);
                vars.put("filterName", filter.getName());
                method = PluginUtils.createMethod("select$Obj$ListByFilter", typeListBase, "public", filterType);
                method.addBodyLine("return $mapper$.select$Obj$ListBy$FilterName$($filterName$);");
                clazz.addMethod(method);

                if (isMainTable) {
                    /*** selectCountByFilter ***/
                    method = PluginUtils.createMethod("select$MainObj$CountByFilter", FullyQualifiedJavaType.getIntInstance(), "public", filterType);
                    method.addBodyLine("return $mapper$.select$MainObj$CountByFilter($mainObj$Filter);");
                    clazz.addMethod(method);
                }
                vars.replace(clazz);
            }
        }

        if (StringUtils.isNotBlank(annotation)) {
            clazz.addAnnotation("@" + annotation);
        }
        services.put(tableName, clazz);
        PluginUtils.addImportedType(clazz);

        return clazz;

    }

    private void addSelectByPrimaryKey(TopLevelClass clazz, IntrospectedTable introspectedTable, String fullTypeName) {
        if (introspectedTable.getPrimaryKeyColumns().size() == 0) {
            return;
        }

        Method method = PluginUtils.createMethod("select$Obj$", new FullyQualifiedJavaType(fullTypeName), "public");
        StringBuilder sb = new StringBuilder();
        for (IntrospectedColumn column : introspectedTable.getPrimaryKeyColumns()) {
            method.addParameter(new Parameter(column.getFullyQualifiedJavaType(), column.getJavaProperty()));
            if (sb.length() > 0) {
                sb.append(", ");
            }
            sb.append(column.getJavaProperty());
        }
        method.addBodyLine("return $mapper$.select$ChildObj$ByPrimaryKey(" + sb.toString() + ");");
        addCommentSeparator(method, PluginUtils.extractTableName(introspectedTable.getFullyQualifiedTableNameAtRuntime()), "SelectByPrimaryKey");
        clazz.addMethod(method);
    }

    private void addMethodIfNotExists(TopLevelClass clazz, Method method) {
        boolean isExists = false;
        for (Method m : clazz.getMethods()) {
            if (m.getName().equals(method.getName()) && m.getParameters().size() == method.getParameters().size()) {
                isExists = true;
                for (int i = 0; i < m.getParameters().size(); i++) {
                    if (!m.getParameters().get(i).getType().equals(method.getParameters().get(i).getType())) {
                        isExists = false;
                        break;
                    }
                }
                if (isExists) {
                    break;
                }
            }
        }
        if (!isExists) {
            clazz.addMethod(method);
        }
    }

    private void addCreateUpdateDeleteMethods(TopLevelClass clazz, IntrospectedTable introspectedTable, String inParam) {


        Method method;
        String setAddedTimeString = "";
        String setUpdatedTimeString = "";
        for (IntrospectedColumn introspectedColumn : introspectedTable.getAllColumns()) {
            if (introspectedColumn.getJavaProperty().equals("timeAdded")) {
                setAddedTimeString = "$obj$.setTimeAdded(new Date());";
            } else if (introspectedColumn.getJavaProperty().equals("timeUpdated")) {
                setUpdatedTimeString = "$obj$.setTimeUpdated(new Date());";
            }
        }
        if (setAddedTimeString.length() > 0 || setUpdatedTimeString.length() > 0) {
            clazz.addImportedType("java.util.Date");
        }

        method = PluginUtils.createMethod("insert$Obj$", "public", inParam);
        method.addBodyLine(setAddedTimeString);
        method.addBodyLine(setUpdatedTimeString);
        method.addBodyLine("$mapper$.insertSelective($obj$);");
        clazz.addMethod(method);
        addCommentSeparator(method, " Create ** Update ** Delete ");

        method = PluginUtils.createMethod("insert$Obj$AllFields", "public", inParam);
        method.addBodyLine(setAddedTimeString);
        method.addBodyLine(setUpdatedTimeString);
        method.addBodyLine("$mapper$.insert($obj$);");
        clazz.addMethod(method);


        method = PluginUtils.createMethod("update$Obj$", "public", inParam);
        method.addBodyLine(setUpdatedTimeString);
        method.addBodyLine("$mapper$.updateByPrimaryKeySelective($obj$);");
        clazz.addMethod(method);


        method = PluginUtils.createMethod("update$Obj$AllFields", "public", inParam);
        method.addBodyLine(setUpdatedTimeString);
        method.addBodyLine("$mapper$.updateByPrimaryKey($obj$);");
        clazz.addMethod(method);


        method = PluginUtils.createMethod("insertOrUpdate$Obj$", "public", inParam);
        method.addBodyLine(setUpdatedTimeString);
        method.addBodyLine("$Obj$ selected$Obj$ = $mapper$.selectByPrimaryKey( " + PluginUtils.getPrimaryKeyAsParamByGetter(introspectedTable, "$obj$", false) + ");");
        method.addBodyLine("if (selected$Obj$ == null) {");
        method.addBodyLine(setAddedTimeString);
        method.addBodyLine("$mapper$.insertSelective($obj$);");
        method.addBodyLine("} else {");
        method.addBodyLine("$mapper$.updateByPrimaryKeySelective($obj$);");
        method.addBodyLine("}");
        clazz.addMethod(method);


        method = PluginUtils.createMethod("insertOrUpdate$Obj$AllFields", "public", inParam);
        method.addBodyLine(setUpdatedTimeString);
        method.addBodyLine("$Obj$ selected$Obj$ = $mapper$.selectByPrimaryKey( " + PluginUtils.getPrimaryKeyAsParamByGetter(introspectedTable, "$obj$", false) + ");");
        method.addBodyLine("if (selected$Obj$ == null) {");
        method.addBodyLine(setAddedTimeString);
        method.addBodyLine("$mapper$.insert($obj$);");
        method.addBodyLine("} else {");
        method.addBodyLine("$mapper$.updateByPrimaryKey($obj$);");
        method.addBodyLine("}");
        clazz.addMethod(method);


        if (introspectedTable.getPrimaryKeyColumns().size() == 1) {
            method = PluginUtils.createMethod("delete$Obj$", "public", "Long id");
            method.addBodyLine("$mapper$.deleteByPrimaryKey(id);");
            clazz.addMethod(method);
        }
    }

    private static final String COMMENT = "//***********************************************************************************************************************************************************************";

    private void addCommentSeparator(Method method, String... strings) {
        int ln = 100;
        method.addJavaDocLine(COMMENT.substring(0, ln));
        for (String s : strings) {
            if (StringUtils.isNotBlank(s)) {
                int i = ln / 2 - s.length() / 2 - 1;
                method.addJavaDocLine(COMMENT.substring(0, i) + ' ' + s + ' ' + COMMENT.substring(i + s.length(), ln));
            }
        }
        method.addJavaDocLine(COMMENT.substring(0, ln));
    }

    private String getPackage(String pck, String fullTableName) {
        if (enableSubPackages) {
            pck += '.' + PluginUtils.extractSchemaName(fullTableName);
        }
        return pck;
    }


    static class InputParams {
        String suffixFuncName = "";
        List<InputParam> inputParamList = new ArrayList<>();
        IntrospectedTable introspectedTable;

        public String getParamList() {
            StringBuilder sb = new StringBuilder();
            for (InputParam in : inputParamList) {
                if (sb.length() > 0) {
                    sb.append(',');
                }
                sb.append(in.paramName);
            }
            return sb.toString();
        }

        public String getParamDeclaration() {
            StringBuilder sb = new StringBuilder();
            for (InputParam in : inputParamList) {
                if (sb.length() > 0) {
                    sb.append(',');
                }
                sb.append(in.paramType).append(' ').append(in.paramName);
            }
            return sb.toString();
        }

        private boolean containsParam(String paramName) {
            for (InputParam param : inputParamList) {
                if (param.paramName.equals(paramName)) {
                    return true;
                }
            }
            return false;
        }

        public String getPrimaryKeyAsDeclaredParam() {
            StringBuilder sb = new StringBuilder();
            for (IntrospectedColumn column : introspectedTable.getPrimaryKeyColumns()) {
                if (!containsParam(column.getJavaProperty())) {
                    if (sb.length() > 0) {
                        sb.append(", ");
                    }
                    sb.append(column.getFullyQualifiedJavaType().getShortName()).append(' ').append(column.getJavaProperty());
                }
            }
            return sb.toString();
        }

        public String getPrimaryKeyAsParam(boolean isWithComma) {
            StringBuilder sb = new StringBuilder();
            for (IntrospectedColumn column : introspectedTable.getPrimaryKeyColumns()) {
                if (!containsParam(column.getJavaProperty())) {
                    if (sb.length() > 0 || isWithComma && !inputParamList.isEmpty()) {
                        sb.append(", ");
                    }
                    sb.append(column.getJavaProperty());
                }
            }
            return sb.toString();
        }

        public boolean isAllPrimaryKeyInParam() {
            for (IntrospectedColumn column : introspectedTable.getPrimaryKeyColumns()) {
                boolean contains = containsParam(column.getJavaProperty());
                if (!contains) {
                    return false;
                }
            }
            return true;
        }

        static class InputParam {
            String paramName = "";
            String paramType = "";
            ComparisonOperator operator = ComparisonOperator.EQUAL;
            FullyQualifiedJavaType importType;
            boolean isList;
        }

        public InputParams(BaseServicesConfig.Method configMethod, IntrospectedTable introspectedTable) {
            this.introspectedTable = introspectedTable;
            if (StringUtils.isNotBlank(configMethod.getName())) {
                suffixFuncName = configMethod.getName();
            }

            for (BaseServicesConfig.Param configParam : configMethod.getParamList()) {
                createParam(configParam);
            }
        }

        private void createParam(BaseServicesConfig.Param configParam) {
            String param = configParam.getName().trim();
            InputParam inputParam = new InputParam();
            inputParam.isList = configParam.isList();

            inputParam.operator = configParam.getOperation();

            IntrospectedColumn cl = PluginUtils.findColumn(introspectedTable.getAllColumns(), param);
            if (cl == null) {
                cl = PluginUtils.findColumnByJavaProperty(introspectedTable.getAllColumns(), param);
            }

            if (cl == null) {
                throw new RuntimeException("not found param with name " + configParam.getName());
            }
            inputParamList.add(inputParam);
            inputParam.importType = cl.getFullyQualifiedJavaType();
            if (!inputParam.isList) {
                inputParam.paramName = cl.getJavaProperty();
                inputParam.paramType = cl.getFullyQualifiedJavaType().getShortName();
            } else {
                inputParam.paramName = cl.getJavaProperty();
                inputParam.paramType = "List<" + cl.getFullyQualifiedJavaType().getShortName() + ">";
            }
        }

        public String getSuffixFuncName() {
            return suffixFuncName;
        }

        private void addBodyLine(VarReplacer vars, Method method, String str) {
            for (InputParam in : inputParamList) {
                vars.put("otherId", in.paramName);
                if (in.isList) {
                    vars.put("operation", "In");
                } else {
                    String methodName = "EqualTo";
                    if (in.operator == ComparisonOperator.GREATER_THEN) {
                        methodName = "GreaterThan";
                    } else if (in.operator == ComparisonOperator.GREATER_THEN_OR_EQUAL_TO) {
                        methodName = "GreaterThanOrEqualTo";
                    } else if (in.operator == ComparisonOperator.LESS_THEN) {
                        methodName = "LessThan";
                    } else if (in.operator == ComparisonOperator.LESS_THEN_OR_EQUAL_TO) {
                        methodName = "LessThanOrEqualTo";
                    } else if (in.operator == ComparisonOperator.LIKE) {
                        methodName = "Like";
                    }

                    vars.put("operation", methodName);
                }
                vars.addBodyLine(method, str);
            }
        }

        public void addImportTypes(TopLevelClass clazz) {
            for (InputParam inputParam : inputParamList) {
                clazz.addImportedType(inputParam.importType);
                if (inputParam.isList) {
                    clazz.addImportedType("java.util.List");
                }
            }
            for (IntrospectedColumn column : introspectedTable.getPrimaryKeyColumns()) {
                clazz.addImportedType(column.getFullyQualifiedJavaType());
            }
        }
    }
}

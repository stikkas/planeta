<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<div class="m-header hidden">
    <div class="m-header_cont">
        <a class="m-header_logo" href="https://${properties['application.host']}/"></a>
        <div class="m-header_menu-toggle"><span class="m-header_menu-toggle-ico"></span></div>
    </div>
</div>

<div class="m-nav hidden">
    <div class="m-menu">
        <div class="m-menu_i"><a class="m-menu_link" href="https://${properties['application.host']}/search/projects"><spring:message code="header.projects" text="default text"> </spring:message></a></div>
        <div class="m-menu_i"><a class="m-menu_link" href="https://${properties['school.application.host']}"><spring:message code="header.school" text="default text"> </spring:message></a></div>
        <div class="m-menu_i"><a class="m-menu_link" href="https://${properties['shop.application.host']}"><spring:message code="header.shop" text="default text"> </spring:message></a></div>
        <div class="m-menu_i"><a class="m-menu_link" href="https://${properties['application.host']}/about"><spring:message code="header.about" text="default text"/></a></div>
        <div class="m-menu_i"><a class="m-menu_link" href="https://${properties['charity.application.host']}"><spring:message code="header.charity" text="default text"/></a></div>
        <div class="m-menu_i"><a class="m-menu_link" href="https://${properties['biblio.application.host']}"><spring:message code="header.biblio" text="default text"/></a></div>
        <div class="m-menu_i"><a class="m-menu_link" href="https://${properties['application.host']}/welcome/bonuses.html"><spring:message code="header.vip.club" text="default text"/></a></div>
    </div>
    <c:choose>
        <c:when test="${!isAuthorized}">
            <div class="m-menu m-menu__action">
                <div class="m-menu_i">
                    <a href="javascript:void(0)" class="m-menu_link js-signup-link"><spring:message code="header.signin" text="default text"> </spring:message></a>
                    </div>
                </div>
        </c:when>
        <c:otherwise>
            <div class="m-menu-user">
                <a class="m-menu-user_link"  href="javascript:void(0)">
                    <span class="m-menu-user_ava"><img src="${hf:getUserAvatarUrl(profile.smallImageUrl, "USER_SMALL_AVATAR", profile.userGender)}" alt="${hf:escapeHtml4(profile.displayName)}"></span>
                    <span class="m-menu-user_name">${profile.displayName}</span>
                </a>
            </div>

            <div class="m-menu m-menu__action">
                <div class="m-menu_i">
                    <a href="https://${properties['application.host']}/account/my-projects" class="m-menu_link"><spring:message code="my.campaigns" text="default text"> </spring:message></a>
                    </div>
                    <div class="m-menu_i">
                        <a href="https://${properties['application.host']}/${aliasOrProfileId}/news" class="m-menu_link"><spring:message code="news.feed" text="default text"> </spring:message></a>
                    </div>
                    <div class="m-menu_i">
                        <a href="https://${properties['application.host']}/account/my-purchases-and-rewards" class="m-menu_link">
                        <c:if test="${myBalance > 0}"><span class="m-menu_notif m-menu_notif__bl">${myBalance} <span class="b-rub">Р</span></span></c:if>
                        <spring:message code="profile" text="default text"> </spring:message>
                        </a>
                    </div>
                <c:if test="${isAdmin}">
                    <div class="m-menu_i">
                        <a class="m-menu_link" href="https://${properties['admin.application.host']}/"><spring:message code="administration" text="default text"> </spring:message></a>
                        </div>
                </c:if>
                <div class="m-menu_i">
                    <a class="m-menu_link" href="https://${properties['application.host']}/account/settings"><spring:message code="profile.settings" text="default text"> </spring:message></a>
                    </div>
                    <div class="m-menu_i">
                        <a class="m-menu_link fic-mistake-a" href="javascript:void(0)"><spring:message code="footer-new.jsp.propertie.1" text="default text"> </spring:message></a>
                    </div>
                    <div class="m-menu_i">
                        <a class="m-menu_link" href="javascript:void(0)" id="m-header-exit"><spring:message code="logout" text="default text"> </spring:message></a>
                    </div>
                </div>
        </c:otherwise>
    </c:choose>
    <div class="m-links">
        <div class="m-links_list">
            <div class="m-links_i">
                <a href="https://${properties["application.host"]}/about" class="m-links_link"><spring:message code="footer-new.jsp.propertie.5" text="default text"> </spring:message></a>
            </div>
            <div class="m-links_i">
                <a href="https://${properties["application.host"]}/advertising" class="m-links_link"><spring:message code="header.advertising" text="default text"> </spring:message></a>
            </div>
            <div class="m-links_i">
                <a href="https://${properties["application.host"]}/faq" class="m-links_link">F.A.Q.</a>
            </div>
            <div class="m-links_i">
                <a href="https://${properties["application.host"]}/welcome/projects-agreement.html" class="m-links_link"><spring:message code="footer-new.jsp.propertie.23" text="default text"> </spring:message></a>
            </div>
            <div class="m-links_i">
                <a href="https://${properties["application.host"]}/contacts" class="m-links_link"><spring:message code="footer-new.jsp.propertie.2" text="default text"> </spring:message></a>
            </div>
        </div>
    </div>
</div>                

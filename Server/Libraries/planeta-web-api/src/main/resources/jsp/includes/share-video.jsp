<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<link rel="image_src" href="${hf:getThumbnailUrl(video.imageUrl, "ORIGINAL", "VIDEO")}"/>
<meta property="og:image" content="${hf:getThumbnailUrl(video.imageUrl, "ORIGINAL", "VIDEO")}" />

<c:set var="defaultVideoUrl" value="https://${properties['tv.application.host']}/video-frame?profileId=${video.profileId}&videoId=${video.videoId}&autostart=true"/>

<c:choose>
    <c:when test="${video.videoType == \"PLANETA\"}">
        <meta property="og:video" content="https://${properties['tv.application.host']}/flash/player-min.swf?json=https://${properties['tv.application.host']}/api/public/video-extended.json&videoId=${video.videoId}&profileId=${video.profileId}&autostart=true" />
    </c:when>
    <c:when test="${video.videoType == \"YOUTUBE\" && video.cachedVideo != null}">
        <meta property="og:video" content="https://youtube.com/v/${video.cachedVideo.externalId}?version=3&autoplay=1" />
    </c:when>
    <c:when test="${video.videoType == \"VIMEO\" && video.cachedVideo != null}">
        <meta property="og:video" content="https://vimeo.com/moogaloop.swf?clip_id=${video.cachedVideo.externalId}" />
    </c:when>
    <c:otherwise>
        <meta property="og:video" content="${defaultVideoUrl}" />
    </c:otherwise>
</c:choose>
<%--<meta property="og:video:secure_url" content="https://secure.example.com/movie.swf" />--%>
<meta property="og:video:type" content="application/x-shockwave-flash" />
<meta property="og:video:width" content="720" />
<meta property="og:video:height" content="480" />

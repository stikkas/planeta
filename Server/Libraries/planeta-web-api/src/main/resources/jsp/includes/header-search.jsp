<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<div class="h-search js-header-search-button">
    <div class="h-search_control-wrap">
        <div class="h-search_control-block<c:if test="${not isAuthorized}"> h-search__narrow</c:if>">
            <input class="h-search_control" type="text" data-emoji_font="true" style="font-family: Tahoma, Arial, sans-serif, 'Segoe UI Emoji', 'Segoe UI Symbol', Symbola, EmojiSymbols !important;">
            <span class="s-icon s-icon-search"></span>
        </div>
    </div>
    <div class="suggest-popup js-large-header-search-view"></div>
</div>

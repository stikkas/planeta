<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ include file="screen-width.jsp" %>
<%@ include file="back-to-top.jsp" %>
<c:if test="${param['no-counters'] != true && header['User-Agent'] != 'planeta-snapshot-bot' && properties['google.tag.manager.id'] != 'false'}">
    <p:script src="planeta-gtm.js" async="true" />
</c:if>

<%--Start header reg confirm --%>
<%@ include file="header-reg-confirm.jsp"%>
<%--End header reg confirm--%>
<%-- ============================ desktop header begins ========================================= --%>
<%@ include file="special-project.jsp"%>
<div class="header hidden">
    <div class="wrap">
        <div class="h-container">
            <c:set var="profile" value="${myProfile.profile}"/>
            <!-- Start header services menu -->
            <%@ include file="header-menu.jsp" %>
            <!-- End header services menu -->

            <!-- Start header info -->
            <%@ include file="header-inner.jsp" %>
            <!-- End header info -->
        </div>
    </div>
</div>
<%-- ============================ desktop header ends ========================================= --%>
<c:if test="${not interactiveCampaignZone}">
<%@ include file="mobile-header.jsp"%>
</c:if>
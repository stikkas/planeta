<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ include file="/WEB-INF/jsp/includes/taglibs.jsp" %>

<%-- This is for bots(search engine crawlers), so they can parse ajax content: https://developers.google.com/webmasters/ajax-crawling/docs/getting-started  --%>
<%-- We suppose any page contains this js-init has js-generated content, so we have to notice it for google-bot --%>
<c:if test="${param['no-counters'] != true && header['User-Agent'] != 'planeta-snapshot-bot'}">
<meta name="fragment" content="!">
</c:if>
<meta name="referrer" content="always"/>

<script type="text/javascript">

    var headerInitParameters = {
        auth: {
            passwordRecovery: '${passwordRecovery}',
            passwordRecoveryRedirectUrl: '${passwordRecoveryRedirectUrl}',
            errorCode: '${errorCode}',
            regCode: '${regCode}'
        }
    };

    var workspaceInitParameters = {
        myProfile: {
            profile: ${hf:toJson(myProfile)}, // mandatory
            myBalance: ${myBalance},
            <c:if test="${not empty email}">
            email: '${email}',
            </c:if>
            isAdmin: ${isAdmin},
            isAuthorized: ${isAuthorized},
            isAuthor: ${isAuthor}
        },
        profile: ${hf:toJson(profileModel)},
        currentLanguage: '${pageContext.response.locale}',
        needStartRouter: !!${hf:toJson(needStartRouter)},
        configuration: {
            mainHost: '${properties["application.host"]}',
            staticNode: '${staticNode}',
            staticNodes: ${hf:toJson(staticNodes)},
            statServiceUrl: '${properties["statistics.service.url"]}',
            statusServiceUrl: '//${properties["status.service.url"]}',
            tvAppUrl: '${properties["tv.application.host"]}',
            adminHost: '${properties["admin.application.host"]}',
            shopAppUrl: '${properties["shop.application.host"]}',
            mobileAppUrl: '${properties["mobile.application.host"]}',
            concertsAppUrl: '${properties["concerts.application.host"]}',
            concertsNewAppUrl: '${properties["concerts.application.host"]}',
            widgetsAppUrl: '${properties["widgets.application.host"]}',
            resourcesHost: '${hf:getStaticBaseUrl("")}',
            casHost: '//${properties["cas.app.host"]}',
            jsBaseUrl: '${hf:getStaticBaseUrl("/res")}',
            flushCache: ${properties['static.flushCache']},
            compress: ${properties['static.compress']},
            projectType: '${projectType}',
            serverTime:<%= System.currentTimeMillis() %>,
            isBotRequest: ${header['User-Agent'] == 'planeta-snapshot-bot'},
            supportPhoneNumber: '${properties["support.phone.number"]}',
            supportEmail: '${properties["support.email"]}',
            schoolUrl: '${properties["school.application.host"]}',
            libraryUrl: '${properties["library.application.host"]}',
            charityUrl: '${properties["charity.application.host"]}',
            promoUrl: '${properties["promo.application.host"]}'
        }
    };
    var workspace;
</script>

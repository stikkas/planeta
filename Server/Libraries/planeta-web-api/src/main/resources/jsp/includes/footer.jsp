<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div class="footer js-footer hidden">
    <div class="footer-info">
        <div class="wrap footer-info_container">
            <div class="col-3 footer-info_col">
                <div class="f-menu"> 
                    <div class="f-menu_header"> <spring:message code="footer-new.jsp.propertie.4" text="default text"> </spring:message> </div>
                    <div class="f-menu_list">
                        <div class="f-menu_i"><a class="f-menu_link" href="https://${properties["application.host"]}/about"> <spring:message code="footer-new.jsp.propertie.5" text="default text"> </spring:message> </a></div>
                        <div class="f-menu_i"><a class="f-menu_link" href="https://${properties["application.host"]}/logo"> <spring:message code="footer-new.jsp.propertie.24" text="default text"> </spring:message> </a></div>
                        <div class="f-menu_i"> <a class="f-menu_link" href="https://${properties["application.host"]}/contacts"> <spring:message code="footer-new.jsp.propertie.2" text="default text"> </spring:message> </a></div>
                        <div class="f-menu_i"> <a class="f-menu_link" href="https://${properties["application.host"]}/advertising"> <spring:message code="header.advertising" text="default text"> </spring:message> </a></div>
                        <div class="f-menu_i"> <a class="f-menu_link" href="https://${properties["application.host"]}/partners"> <spring:message code="footer-new.jsp.propertie.9" text="default text"> </spring:message> </a></div>
                    </div>
                </div>
            </div>
            <div class="col-3 footer-info_col">
                <div class="f-menu">
                    <div class="f-menu_header"> <spring:message code="footer-new.jsp.propertie.10" text="default text"> </spring:message> </div>
                    <div class="f-menu_list">
                        <div class="f-menu_i"> <a class="f-menu_link" href="https://${properties["application.host"]}/search/projects"> <spring:message code="footer-new.jsp.propertie.11" text="default text"> </spring:message> </a></div>
                        <div class="f-menu_i"> <a class="f-menu_link" href="https://${properties["application.host"]}/search/projects?query=&status=NEW"> <spring:message code="footer-new.jsp.propertie.12" text="default text"> </spring:message> </a></div>
                        <div class="f-menu_i"> <a class="f-menu_link" href="https://${properties["application.host"]}/faq/article/8!paragraph54"> <spring:message code="footer-new.jsp.propertie.26" text="default text"> </spring:message> </a></div>
                        <div class="f-menu_i"> <a class="f-menu_link" href="https://${properties["application.host"]}/faq/article/10!paragraph80"> <spring:message code="footer-new.jsp.propertie.13" text="default text"> </spring:message> </a></div>
                    </div>
                </div>
            </div>
            <div class="col-3 footer-info_col">
                <div class="f-menu">
                    <div class="f-menu_header"> <spring:message code="footer-new.jsp.propertie.15" text="default text"> </spring:message> </div>
                    <div class="f-menu_list">
                        <div class="f-menu_i"> <a class="f-menu_link" href="https://${properties["application.host"]}/funding-rules"> <spring:message code="footer-new.jsp.propertie.16" text="default text"> </spring:message>  </a></div>
                        <div class="f-menu_i"> <a class="f-menu_link" href="https://${properties["application.host"]}/stories"> <spring:message code="footer-new.jsp.propertie.17" text="default text"> </spring:message> </a></div>
                        <div class="f-menu_i"> <a class="f-menu_link" href="https://${properties["application.host"]}/faq/article/11"> <spring:message code="footer-new.jsp.propertie.18" text="default text"> </spring:message> </a></div>
                        <div class="f-menu_i"> <a class="f-menu_link" href="https://${properties["application.host"]}/faq/article/12"> <spring:message code="footer-new.jsp.propertie.19" text="default text"> </spring:message> </a></div>
                        <div class="f-menu_i"> <a class="f-menu_link" href="https://${properties["application.host"]}/faq/article/9!paragraph80"> <spring:message code="footer-new.jsp.propertie.20" text="default text"> </spring:message> </a></div>
                    </div>
                </div>
            </div>
            <div class="col-3 footer-info_col">
                <div class="f-menu">
                    <div class="f-menu_header"> <spring:message code="footer-new.jsp.propertie.21" text="default text"> </spring:message> </div>
                    <div class="f-menu_list">
                        <div class="f-menu_i"> <a class="f-menu_link" href="https://${properties["application.host"]}/faq"> F.A.Q. </a> </div>
                        <div class="f-menu_i"> <a class="f-menu_link" href="https://${properties["application.host"]}/welcome/projects-agreement.html"> <spring:message code="footer-new.jsp.propertie.23" text="default text"> </spring:message> </a></div>

                        <c:if test="${param['no-counters'] != true && header['User-Agent'] != 'planeta-snapshot-bot'}">
                            <div class="f-menu_i"><a class="f-menu_link fic-mistake-a" href="javascript:void(0)"><spring:message code="footer-new.jsp.propertie.1" text="default text"> </spring:message></a></div>
                        </c:if>
                    </div>
                </div>
            </div>
            <a class="f-runet" title="<spring:message code="footer-new.jsp.propertie.25" text="default text"> </spring:message>" href="http://premiaruneta.ru/press/26/"></a>
        </div>
    </div>
    <div class="footer-common">
        <div class="wrap">
            <div class="col-3">
                <a class="f-logo" href="https://${properties['application.host']}/"><img src="//${hf:getStaticBaseUrl("")}/images/pln-logo.svg" alt="planeta.ru" width="124" height="32"></a>
            </div>
            <div class="col-6">
                <div class="f-social js-social-anchor">
                </div>
            </div>

            <div class="col-3">
                <div class="f-lang">
                    <span  id="switch-language" class="f-lang_link">
                        <span class="f-lang_ico"><span></span></span>
                        <span class="f-lang_lbl"><spring:message code="switch.locale" text="default text"/></span>
                    </span>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="m-footer js-mobile-footer hidden">&copy; Planeta.ru</div>

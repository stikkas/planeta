<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<div class="main-nav">
    <div class="main-nav_cont">
        <div class="main-nav_wrap">
            <div class="main-nav_heads">
                <div class="main-nav-heads">
                    <div class="main-nav-heads_i">
                        <div class="main-nav-heads_link" data-id="0">
                            <spring:message code="main.nav.campaigns" text="default text"> </spring:message>
                        </div>
                    </div>
                    <div class="main-nav-heads_i">
                        <div class="main-nav-heads_link" data-id="1">
                            <spring:message code="main.nav.shares" text="default text"> </spring:message>
                        </div>
                    </div>
                    <div class="main-nav-heads_i">
                        <div class="main-nav-heads_link" data-id="2">
                            <spring:message code="main.nav.about" text="default text"> </spring:message>
                        </div>
                    </div>
                    <div class="main-nav-heads_i">
                        <div class="main-nav-heads_link" data-id="3">
                            <spring:message code="main.nav.authors" text="default text"> </spring:message>
                        </div>
                    </div>
                    <div class="main-nav-heads_i">
                        <div class="main-nav-heads_link" data-id="4">
                            <spring:message code="main.nav.help" text="default text"> </spring:message>
                        </div>
                    </div>
                </div>
            </div>


            <div class="main-nav_nav">
                <div class="main-nav_nav-list">
                    <div class="main-nav_nav-i" data-id="0">
                        <div class="main-nav-menu">
                            <div class="main-nav-menu_head">
                                <spring:message code="main.nav.campaigns" text="default text"> </spring:message>
                            </div>

                            <div class="main-nav-menu_list">
                                <div class="main-nav-menu_i">
                                    <a href="https://${properties['application.host']}/search/projects?status=ACTIVE" class="main-nav-menu_link">
                                        <spring:message code="main.nav.projects.active" text="default text"> </spring:message>
                                    </a>
                                </div>
                                <div class="main-nav-menu_i">
                                    <a href="https://${properties['application.host']}/search/projects?status=DISCUSSED" class="main-nav-menu_link">
                                        <spring:message code="main.nav.projects.discussed" text="default text"> </spring:message>
                                    </a>
                                </div>
                                <div class="main-nav-menu_i">
                                    <a href="https://${properties['application.host']}/search/projects?status=LAST_UPDATED" class="main-nav-menu_link">
                                        <spring:message code="main.nav.projects.updated" text="default text"> </spring:message>
                                    </a>
                                </div>
                                <div class="main-nav-menu_i">
                                    <a href="https://${properties['application.host']}/search/projects?status=CLOSE_TO_FINISH" class="main-nav-menu_link">
                                        <spring:message code="main.nav.projects.close_to_finish" text="default text"> </spring:message>
                                    </a>
                                </div>
                                <div class="main-nav-menu_i">
                                    <a href="https://${properties['application.host']}/search/projects?status=NEW" class="main-nav-menu_link">
                                        <spring:message code="main.nav.projects.new" text="default text"> </spring:message>
                                    </a>
                                </div>
                            </div>
                            <div class="main-nav-menu_more">
                                <a href="https://${properties['application.host']}/search/projects" class="main-nav-menu_more-link">
                                    <spring:message code="main.nav.show_all" text="default text"> </spring:message>
                                </a>
                            </div>
                        </div>
                    </div>

                    <div class="main-nav_nav-i" data-id="1">
                        <div class="main-nav-menu">
                            <div class="main-nav-menu_head">
                                <spring:message code="main.nav.shares" text="default text"> </spring:message>
                            </div>

                            <div class="main-nav-menu_list">
                                <div class="main-nav-menu_i">
                                    <a href="https://${properties['application.host']}/search/shares?sortOrderList=SORT_BY_PURCHASE_COUNT_DESC" class="main-nav-menu_link">
                                        <spring:message code="main.nav.shares.popular" text="default text"> </spring:message>
                                    </a>
                                </div>
                                <div class="main-nav-menu_i">
                                    <a href="https://${properties['application.host']}/search/shares?sortOrderList=SORT_FEW" class="main-nav-menu_link">
                                        <spring:message code="main.nav.shares.fewer" text="default text"> </spring:message>
                                    </a>
                                </div>
                                <div class="main-nav-menu_i">
                                    <a href="https://${properties['application.host']}/search/shares?sortOrderList=SORT_BY_TIME_ADDED_DESC" class="main-nav-menu_link">
                                        <spring:message code="main.nav.shares.new" text="default text"> </spring:message>
                                    </a>
                                </div>
                                <div class="main-nav-menu_i">
                                    <a href="https://${properties['application.host']}/search/shares?sortOrderList=SORT_BY_PRICE_DESC" class="main-nav-menu_link">
                                        <spring:message code="main.nav.shares.expensive" text="default text"> </spring:message>
                                    </a>
                                </div>
                                <div class="main-nav-menu_i">
                                    <a href="https://${properties['application.host']}/search/shares?sortOrderList=SORT_RARE" class="main-nav-menu_link">
                                        <spring:message code="main.nav.shares.rare" text="default text"> </spring:message>
                                    </a>
                                </div>
                            </div>
                            <div class="main-nav-menu_more">
                                <a href="https://${properties['application.host']}/search/shares" class="main-nav-menu_more-link">
                                    <spring:message code="main.nav.show_all" text="default text"> </spring:message>
                                </a>
                            </div>
                        </div>
                    </div>

                    <div class="main-nav_nav-i" data-id="2">
                        <div class="main-nav-menu">
                            <div class="main-nav-menu_head">
                                <spring:message code="main.nav.about" text="default text"> </spring:message>
                            </div>

                            <div class="main-nav-menu_list">
                                <div class="main-nav-menu_i">
                                    <a href="https://${properties["application.host"]}/about" class="main-nav-menu_link">
                                        <spring:message code="footer-new.jsp.propertie.5" text="default text"> </spring:message>
                                    </a>
                                </div>
                                <div class="main-nav-menu_i">
                                    <a href="https://${properties["application.host"]}/advertising" class="main-nav-menu_link">
                                        <spring:message code="header.advertising" text="default text"> </spring:message>
                                    </a>
                                </div>
                                <div class="main-nav-menu_i">
                                    <a href="https://${properties["application.host"]}/partners" class="main-nav-menu_link">
                                        <spring:message code="footer-new.jsp.propertie.9" text="default text"> </spring:message>
                                    </a>
                                </div>

                                <div class="main-nav-menu_i">
                                    <a href="https://${properties["application.host"]}/about?#about-contacts" class="main-nav-menu_link">
                                        <spring:message code="footer-new.jsp.propertie.2" text="default text"> </spring:message>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="main-nav_nav-i" data-id="3">
                        <div class="main-nav-menu">
                            <div class="main-nav-menu_head">
                                <spring:message code="main.nav.for_authors" text="default text"> </spring:message>
                            </div>

                            <div class="main-nav-menu_list">
                                <div class="main-nav-menu_i">
                                    <a href="https://${properties["application.host"]}/funding-rules" class="main-nav-menu_link">
                                        <spring:message code="main.nav.create_project" text="default text"> </spring:message>
                                    </a>
                                </div>
                                <div class="main-nav-menu_i">
                                    <a href="https://${properties["application.host"]}/stories" class="main-nav-menu_link">
                                        <spring:message code="main.nav.success_stories" text="default text"> </spring:message>
                                    </a>
                                </div>
                                <div class="main-nav-menu_i">
                                    <a href="https://${properties["school.application.host"]}" class="main-nav-menu_link">
                                        <spring:message code="main.nav.school_classes" text="default text"> </spring:message>
                                    </a>
                                </div>
                                <div class="main-nav-menu_i">
                                    <a href="https://${properties["application.host"]}/faq/article/12" class="main-nav-menu_link">
                                        <spring:message code="main.nav.campaign_promotion" text="default text"> </spring:message>
                                    </a>
                                </div>
                                <div class="main-nav-menu_i">
                                    <a href="https://${properties["application.host"]}/faq/article/9!paragraph80" class="main-nav-menu_link">
                                        <spring:message code="main.nav.exec" text="default text"> </spring:message>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="main-nav_nav-i" data-id="4">
                        <div class="main-nav-menu">
                            <div class="main-nav-menu_head">
                                <spring:message code="main.nav.help" text="default text"> </spring:message>
                            </div>

                            <div class="main-nav-menu_list">
                                <div class="main-nav-menu_i">
                                    <a href="https://${properties["application.host"]}/faq" class="main-nav-menu_link">
                                        F.A.Q.
                                    </a>
                                </div>
                                <div class="main-nav-menu_i">
                                    <a href="https://${properties["application.host"]}/welcome/projects-agreement.html" class="main-nav-menu_link">
                                        <spring:message code="main.nav.rules" text="default text"> </spring:message>
                                    </a>
                                </div>
                                <c:if test="${param['no-counters'] != true && header['User-Agent'] != 'planeta-snapshot-bot'}">
                                    <div class="main-nav-menu_i">
                                        <a href="javascript:void(0)" class="main-nav-menu_link fic-mistake-a">
                                            <spring:message code="main.nav.ask_question" text="default text"> </spring:message>
                                        </a>
                                    </div>
                                </c:if>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="main-nav_sections">
            <div class="main-nav_wrap">
                <div class="main-nav-sections">
                    <div class="main-nav-sections_i">
                        <a href="https://${properties["school.application.host"]}" class="main-nav-sections_link">
                            <span class="main-nav-sections_ico">
                                <svg width="32" height="32" viewBox="0 0 32 32"><path d="M18.266 12.096a1.667 1.667 0 1 1 0 3.333 1.667 1.667 0 0 1 0-3.333m-5.158 1.633c-1.729.105-3.457.861-4.474 2.285-.138.195-.338.472-.474.933.285.035.687.112.923.152.622.105 1.352.185 2.047.596a19.143 19.143 0 0 0-.34 1.527.689.689 0 0 0-.009.119c.345.173.802.367 1.157.721.354.354.548.812.72 1.157.04 0 .08-.002.12-.009a19.143 19.143 0 0 0 1.526-.341c.412.696.491 1.425.596 2.048.04.235.118.637.153.922.46-.135.738-.335.932-.474 1.425-1.016 2.18-2.745 2.286-4.473 1.086-.714 2.177-1.48 2.871-2.545 1.317-2.022 1.425-4.425 1.425-6.915-2.49 0-4.893.109-6.914 1.425-1.066.694-1.831 1.786-2.545 2.872m12.7 12.076c-4.003 4.003-9.845 5.038-14.808 3.122-1.183-.531-4.242-2.467.291-7.001a.87.87 0 1 0-1.229-1.23c-4.437 4.437-6.387 1.6-6.966.368-1.95-4.977-.921-10.852 3.099-14.873C10.911 1.475 18.18.87 23.556 4.369a3.192 3.192 0 0 0 .743 3.331 3.195 3.195 0 0 0 3.332.744c3.499 5.375 2.894 12.644-1.823 17.361m3.484-18.714a3.195 3.195 0 0 0-.466-3.917 3.196 3.196 0 0 0-3.917-.467C18.688-1.47 10.184-.815 4.686 4.683c-6.248 6.248-6.248 16.382 0 22.63 6.248 6.249 16.383 6.249 22.631 0 5.498-5.498 6.153-14.001 1.975-20.222" fill="#F2AD0C"/></svg>
                            </span>
                            <span class="main-nav-sections_name">
                                <spring:message code="main.nav.school" text="default text" />
                            </span>
                        </a>
                    </div>
                    <div class="main-nav-sections_i">
                        <a href="https://${properties["shop.application.host"]}" class="main-nav-sections_link">
                            <span class="main-nav-sections_ico">
                                <svg width="32" height="32" viewBox="0 0 32 32"><path d="M15.184 19.655c.033-.033.084-.091.148-.183v3.809h-3.727a2.886 2.886 0 0 1-2.885-2.886v-3.828h3.859a1.59 1.59 0 0 0-.275.208 2.039 2.039 0 0 0 0 2.88c.385.385.896.596 1.44.596.544 0 1.055-.211 1.44-.596m1.808-3.088h6.289v3.828a2.886 2.886 0 0 1-2.885 2.886h-3.948v-5.686c.29.259.629.505 1.019.738a8.737 8.737 0 0 0 1.546.725l.351-1.06c-.504-.167-1.647-.678-2.372-1.431M11.605 8.72h3.727v5.37a6.624 6.624 0 0 0-1.019-.738 8.737 8.737 0 0 0-1.546-.725l-.352 1.06c.006.002.65.219 1.345.635.438.263.963.648 1.308 1.129H8.72v-3.846a2.885 2.885 0 0 1 2.885-2.885m11.676 2.885v3.846h-3.959a1.57 1.57 0 0 0 .274-.208c.385-.385.597-.896.597-1.44 0-.544-.212-1.055-.597-1.44a2.02 2.02 0 0 0-1.439-.596 2.02 2.02 0 0 0-1.44.596 1.863 1.863 0 0 0-.269.374V8.72h3.948a2.885 2.885 0 0 1 2.885 2.885m-4.474 2.848c-.178.178-.985.502-2.099.798.296-1.113.621-1.921.798-2.099a.918.918 0 0 1 1.301.001.91.91 0 0 1 .269.65.913.913 0 0 1-.269.65m-5.063 4.682a.92.92 0 0 1-.65-1.57c.178-.178.985-.502 2.099-.798-.297 1.114-.621 1.921-.799 2.098a.91.91 0 0 1-.65.27m12.062 6.671c-5.415 5.414-14.197 5.414-19.611 0C.78 20.391.78 11.609 6.195 6.194 10.908 1.481 18.172.871 23.547 4.363a3.199 3.199 0 0 0 4.09 4.091c3.492 5.374 2.883 12.639-1.831 17.352m3.492-18.709a3.2 3.2 0 0 0-4.395-4.395C18.684-1.471 10.182-.81 4.686 4.686c-6.248 6.248-6.248 16.381 0 22.628 6.248 6.248 16.381 6.248 22.629 0 5.496-5.496 6.156-13.997 1.983-20.217" fill="#0098D9"/></svg>
                            </span>
                            <span class="main-nav-sections_name">
                                <spring:message code="main.nav.shop" text="default text" />
                            </span>
                        </a>
                    </div>
                    <div class="main-nav-sections_i">
                        <a href="https://${properties["application.host"]}/welcome/bonuses.html" class="main-nav-sections_link">
                            <span class="main-nav-sections_ico">
                                <svg width="32" height="32" viewBox="0 0 32 32"><g fill="#36F"><path d="M25.78 25.782c-5.41 5.41-14.185 5.41-19.594 0-5.41-5.409-5.41-14.184 0-19.594C10.894 1.479 18.152.87 23.522 4.359a3.198 3.198 0 0 0 4.087 4.087c3.489 5.369 2.88 12.627-1.829 17.336m3.488-18.691a3.198 3.198 0 0 0-4.391-4.392C18.663-1.47 10.169-.81 4.678 4.681c-6.242 6.242-6.242 16.366 0 22.609 6.242 6.242 16.367 6.242 22.609 0 5.491-5.491 6.151-13.985 1.981-20.199"/><path d="M21.807 22.001H10.005A2.408 2.408 0 0 1 7.6 19.596a.697.697 0 0 1 1.391 0c0 .559.455 1.014 1.014 1.014h11.802c.558 0 1.013-.455 1.013-1.014a.697.697 0 0 1 1.391 0 2.407 2.407 0 0 1-2.404 2.405"/><path d="M20.691 10.533H11.12a.72.72 0 0 0-.717.717l.759 9.436c0 .394-.436.717-.042.717h9.571c.395 0-.041-.323-.041-.717l.758-9.436a.719.719 0 0 0-.717-.717"/></g></svg>
                            </span>
                            <span class="main-nav-sections_name">
                                <spring:message code="main.nav.vip" text="default text" />
                            </span>
                        </a>
                    </div>
                    <div class="main-nav-sections_i">
                        <a href="https://${properties["charity.application.host"]}" class="main-nav-sections_link">
                            <span class="main-nav-sections_ico">
                                <svg width="32" height="32" viewBox="0 0 32 32"><path d="M17.129 13.138c-.626-.341-1.358-.445-2.298-.323a.128.128 0 0 1-.142-.109.127.127 0 0 1 .11-.142c.921-.12 1.66-.033 2.301.273a12.56 12.56 0 0 0-.413-2.139c-.001-.002-.004-.002-.005-.003-.564-.423-2.014-.38-2.03-.38a.128.128 0 0 1-.13-.123.126.126 0 0 1 .122-.13c.056-.002 1.226-.036 1.935.284a8.868 8.868 0 0 0-1.084-2.252.126.126 0 0 1 .033-.176.126.126 0 0 1 .176.033c.568.834.966 1.747 1.245 2.65.42-.763 1.275-1.151 1.316-1.169a.126.126 0 0 1 .167.064.126.126 0 0 1-.064.167c-.01.005-1.012.462-1.309 1.314.198.724.322 1.433.399 2.084.621-.737 1.574-1.269 1.619-1.294a.127.127 0 0 1 .122.222c-.011.006-1.138.635-1.703 1.437.145 1.557.023 2.696.02 2.719-.009.063-.06.148-.123.148l-.218-.011c-.069-.01-.096-.087-.086-.157.003-.024.186-1.3.04-2.987m8.677 12.667a13.804 13.804 0 0 1-5.163 3.264c-1.326.385-3.223.445-3.852-1.996a29.634 29.634 0 0 0-.211-2.063c.003-.301.017-.62.044-.962.085-.612.275-1.252.673-1.705.349-.397.797-.607 1.39-.684.325-.033.446.06.492.263.171.748.717 1.241 1.561 1.311 1.001.084 2.095-1.564 3.834-2.034-.918-1.172-2.432-1.695-3.434-1.778-.843-.07-1.804.475-1.919 1.203-.026.112-.072.405-.589.446-.01.001-.005.001.01.001-.866.07-1.547.334-2.042.898a2.996 2.996 0 0 0-.427.637c-.265-1.542-.088-2.021.303-2.74.57-1.047.882-2.011 1.028-2.874l.004-.013c.197-.908.489-.86 1.064-1.264 1.227-.862 2.019-3.049 1.281-4.756-.875-2.027-3.943-5.307-6.463-5.408 1.507 3.904-.314 4.894-.021 7.082.283 2.115 1.493 3.457 2.75 3.445.554-.012.836.162.811.834a5.124 5.124 0 0 1-.177.689c-.243.721-.763 1.369-.992 1.813-.187.36-.373.627-.516.927l-.041-.103c-.54-1.288-1.503-1.834-2.568-2.041l-.25-.047c-.228-.07-.263-.407-.267-.422-.098-.65-.376-.947-.983-1.08-.719-.157-2.42.642-2.696.999.957.162 1.541 1.504 2.261 1.662.606.132.917-.133 1.278-.683.079-.12.325-.109.409-.111l.162.031c1.969.382 2.412 3.111 2.482 3.656l.01.089.001.005c.029.229.07.482.126.767.07.357.122.702.161 1.034.464 5.492-2.316 5.466-4.043 4.946a13.807 13.807 0 0 1-5.092-3.238C.78 20.39.78 11.608 6.195 6.193 10.908 1.48 18.172.87 23.546 4.362a3.201 3.201 0 0 0 4.091 4.091c3.492 5.374 2.883 12.639-1.831 17.352m3.492-18.709a3.2 3.2 0 0 0-4.395-4.395C18.684-1.471 10.182-.811 4.686 4.685c-6.248 6.248-6.248 16.381 0 22.629 6.248 6.248 16.381 6.248 22.629 0 5.496-5.496 6.156-13.998 1.983-20.218" fill="#D60B52"/></svg>
                            </span>
                            <span class="main-nav-sections_name">
                                <spring:message code="main.nav.charity" text="default text" />
                            </span>
                        </a>
                    </div>
                    <div class="main-nav-sections_i">
                        <a href="https://${properties["biblio.application.host"]}" class="main-nav-sections_link">
                            <span class="main-nav-sections_ico">
                                <svg width="32" height="32" viewBox="0 0 32 32"><path d="M24.351 8.142H10.079c-.724 0-1.256-.242-1.63-.736-.327-.435-.519-1.055-.548-1.754v-.11c0-.028 0-.053-.003-.082.003-.846.252-1.583.689-2.024.359-.362.86-.547 1.492-.547h14.269a.943.943 0 1 0 0-1.889H10.079c-1.139 0-2.118.381-2.833 1.105-.812.821-1.252 2.043-1.237 3.437 0 .047 0 .095.004.142v19.468c0 1.171.333 2.159.966 2.865.718.799 1.763 1.205 3.1 1.205h14.272a.943.943 0 0 0 .944-.944V9.086a.943.943 0 0 0-.944-.944zm-.944 19.188H10.079c-.78 0-1.35-.195-1.696-.579-.4-.444-.482-1.114-.482-1.599V9.436c.614.393 1.351.595 2.178.595h13.328V27.33zM10.02 4.62a.943.943 0 1 0 0 1.888h13.311a.942.942 0 0 0 .944-.944.943.943 0 0 0-.944-.944H10.02z" fill="#1A8CFF"/><path d="M18.589 18.278c-1.165-.598-2.537-.819-4.47-.819h-1.857v-3.982c0-.368.207-.459.783-.459h4.511l.431 1.322c.038.126.117.182.236.182h.954c.17 0 .249-.069.249-.207.038-1.231.053-2.153.053-2.773 0-.174-.104-.252-.314-.252h-7.268c-1.373 0-2.053.437-2.053 1.485v4.687H8.399s-.188.066-.188.189v.976s.047.085.157.126c.104.041 1.388.472 1.473.5v6.352c0 .026.003.051.006.076v6.229l1.165-1.164 1.228 1.164v-5.958h1.328c2.04 0 3.425-.151 4.171-.45 1.766-.702 2.641-2.024 2.641-3.948 0-1.599-.602-2.691-1.791-3.276zm-3.897 6.005h-2.43V19.14h2.392c2.077 0 3.189.853 3.189 2.474 0 1.69-1.178 2.669-3.151 2.669z"/></svg>
                            </span>
                            <span class="main-nav-sections_name">
                                <spring:message code="page.about.us.about.49" text="default text" />
                            </span>
                        </a>
                    </div>
                    <div class="main-nav-sections_i">
                        <a href="//${properties["promo.application.host"]}/campus" class="main-nav-sections_link">
                            <span class="main-nav-sections_ico">
                                <svg width="32" height="32" viewBox="0 0 32 32"><g fill="#163870"><path d="M29.86 5.564a3.323 3.323 0 1 1-6.646 0 3.323 3.323 0 0 1 6.646 0z"/><path d="M15.98 31.959C7.154 31.959 0 24.805 0 15.98 0 7.154 7.154 0 15.98 0c8.825 0 15.979 7.154 15.979 15.98 0 8.825-7.154 15.979-15.979 15.979zm0-2.038c7.699 0 13.941-6.242 13.941-13.941 0-7.7-6.242-13.942-13.941-13.942-7.7 0-13.942 6.242-13.942 13.942 0 7.699 6.242 13.941 13.942 13.941z"/><path d="M24.132 11.394a1.316 1.316 0 0 0-.652-.469c.021.265 0 .469-.061.632l-3.281 10.782a.76.76 0 0 1-.388.448 1.142 1.142 0 0 1-.611.163H9.07c-.876 0-1.406-.244-1.569-.754-.082-.204-.062-.346.02-.469.082-.102.224-.163.408-.163h9.477c.673 0 1.142-.122 1.407-.367.265-.244.509-.815.774-1.671l2.996-9.885c.163-.53.102-1.019-.203-1.427-.286-.408-.714-.611-1.244-.611H12.82c-.102 0-.285.04-.55.101l.02-.04a2.593 2.593 0 0 0-.509-.061.665.665 0 0 0-.387.122.888.888 0 0 0-.286.265 1.137 1.137 0 0 0-.204.346c-.061.143-.122.265-.183.388-.041.122-.102.244-.163.387a1.548 1.548 0 0 1-.183.326 1.462 1.462 0 0 1-.184.224c-.081.102-.143.184-.204.245-.061.081-.081.142-.102.204-.02.061 0 .163.021.285.02.122.04.224.04.285-.02.286-.122.632-.305 1.06-.163.428-.326.734-.469.917-.02.041-.102.123-.245.245a1.33 1.33 0 0 0-.244.326c-.021.041-.041.143 0 .306.02.163.041.285.02.346-.02.245-.122.571-.265.979-.163.407-.306.733-.469.998-.02.041-.081.123-.183.245a.838.838 0 0 0-.183.306c-.021.061-.021.163 0 .305.02.143.02.245 0 .326a5.67 5.67 0 0 1-.327.999 9.17 9.17 0 0 1-.489.999 1.637 1.637 0 0 1-.183.265 1.549 1.549 0 0 0-.184.265c-.04.081-.081.143-.081.224 0 .041 0 .122.041.204.02.102.04.183.04.244 0 .102-.02.245-.04.408-.021.163-.041.265-.041.285-.163.429-.163.897.02 1.386.204.571.571 1.04 1.08 1.427.53.387 1.06.591 1.631.591h10.089c.469 0 .917-.163 1.345-.468a2.28 2.28 0 0 0 .836-1.183l2.996-9.885c.163-.53.082-.999-.204-1.406zm-11.617.02l.224-.693a.364.364 0 0 1 .183-.245.49.49 0 0 1 .286-.101h6.644c.102 0 .184.04.224.101.041.062.041.143.021.245l-.224.693a.369.369 0 0 1-.184.245.486.486 0 0 1-.285.102h-6.645c-.102 0-.183-.041-.224-.102-.041-.062-.041-.143-.02-.245zm-.897 2.792l.224-.693a.363.363 0 0 1 .183-.244.49.49 0 0 1 .286-.102h6.644c.102 0 .184.041.225.102.04.061.04.142.02.244l-.224.693a.366.366 0 0 1-.184.245.486.486 0 0 1-.285.102h-6.645c-.101 0-.183-.041-.224-.102a.25.25 0 0 1-.02-.245z"/></g></svg>
                            </span>
                            <span class="main-nav-sections_name">
                                <spring:message code="planeta-welcome.jsp.planeta-online-campus" text="default text" />
                            </span>
                        </a>
                    </div>
                    <%--<div class="main-nav-sections_i">
                        <a href="https://${properties["application.host"]}/advertising" class="main-nav-sections_link">
                            <span class="main-nav-sections_ico">
                                <svg width="32" height="32" viewBox="0 0 32 32"><defs><linearGradient x1="50%" y1="0%" x2="50%" y2="100%" id="a"><stop stop-color="#FBB03B" offset="0%"/><stop stop-color="#D4145A" offset="100%"/></linearGradient></defs><path d="M27.638 8.454c3.491 5.374 2.88 12.638-1.832 17.351-5.415 5.415-14.197 5.415-19.612 0C.78 20.391.78 11.61 6.194 6.195 10.907 1.48 18.172.87 23.546 4.361a3.2 3.2 0 0 0 4.092 4.092zm1.66-1.357c4.173 6.22 3.512 14.721-1.984 20.217-6.248 6.248-16.38 6.248-22.628 0-6.248-6.248-6.248-16.38 0-22.628C10.18-.81 18.683-1.472 24.903 2.7a3.202 3.202 0 0 1 4.396 4.396zM16.079 21.33l6.175 3.247-1.18-6.877 4.997-4.87-6.905-1.004-3.088-6.257-3.088 6.257-6.905 1.003 4.997 4.87-1.18 6.878 6.176-3.247z" fill="url(#a)" fill-rule="evenodd"/></svg>
                            </span>
                            <span class="main-nav-sections_name">
                                <spring:message code="header.advertising" text="default text" />
                            </span>
                        </a>
                    </div>--%>
                </div>
            </div>
        </div>
    </div>
</div>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@taglib prefix="p" uri="http://planeta.ru/tags" %>
<script type="text/javascript">
    backToTopDisabled = false;
    <p:if test="${properties['back.to.top.disasbled']}">
    backToTopDisabled = true;
    </p:if>
</script>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<div class="header_search">
    <div class="h-search">
        <div class="h-search_ico">
            <svg width="20" height="20" viewBox="0 0 18 18"><path d="M17.486 15.107a1.681 1.681 0 0 1-.743 2.838 1.681 1.681 0 0 1-1.636-.459l-4.932-4.931a6.745 6.745 0 1 1 2.38-2.379l4.931 4.931zM6.75 1.8a4.95 4.95 0 1 0 0 9.902 4.95 4.95 0 0 0 0-9.902z" fill="#454553"></path></svg>
        </div>
        <button class="h-search_close js-header-search-button-close">
            <svg width="18" height="18" viewBox="0 0 18 18"><g stroke="#454553" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" fill="none"><path d="M1 1l16 16M17 1L1 17"></path></g></svg>
        </button>
        <input type="text" class="form-control h-search_input js-h-search_input" placeholder="<spring:message code="header.search" text="default text"> </spring:message>">
    </div>
</div>
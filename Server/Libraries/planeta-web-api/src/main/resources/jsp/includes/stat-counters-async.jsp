<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="hf" uri="http://planeta.ru/taglibs/HelperFunctions" %>
<%@taglib prefix="p" uri="http://planeta.ru/tags" %>

<c:if test="${param['no-counters'] != true && header['User-Agent'] != 'planeta-snapshot-bot' && properties['google.tag.manager.id'] != 'false'}">
    <!-- Google Tag Manager -->

    <script>
        (function (w, d, s, l, i) {
            w[l] = w[l] || [];
            w[l].push({
                'gtm.start': new Date().getTime(), event: 'gtm.js'
            });
            var f = d.getElementsByTagName(s)[0],
                    j = d.createElement(s), dl = l !== 'dataLayer' ? '&l=' + l : '';
            j.async = true;
            j.src = 'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
            f.parentNode.insertBefore(j, f);
        })(window, document, 'script', 'dataLayer', '${properties['google.tag.manager.id']}');
    </script>

    <!-- End Google Tag Manager -->
    <p:script src="planeta-gtm.js" async="true" />
</c:if>

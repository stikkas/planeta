<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div class="footer">
    <div class="footer_cont">
        <div class="footer_wrap wrap">
            <div class="wrap-row">
                <div class="m-col-tp-9 m-col-tp-push-3">
                    <div class="wrap-row">
                        <div class="m-col-ld-8 col-12">
                            <div class="f-menu-list wrap-row">
                                <div class="f-menu-list_i col-4">
                                    <div class="f-menu">
                                        <div class="f-menu_header">
                                            <spring:message code="footer-new.jsp.propertie.10" text="default text"> </spring:message>
                                        </div>

                                        <div class="f-menu_list">
                                            <div class="f-menu_i">
                                                <a class="f-menu_link" href="https://${properties["school.application.host"]}">
                                                    <spring:message code="footer-new.jsp.propertie.11" text="default text"> </spring:message>
                                                </a>
                                            </div>
                                            <div class="f-menu_i">
                                                <a class="f-menu_link" href="https://${properties["shop.application.host"]}">
                                                    <spring:message code="footer-new.jsp.propertie.12" text="default text"> </spring:message>
                                                </a>
                                            </div>
                                            <div class="f-menu_i">
                                                <a class="f-menu_link" href="https://${properties["application.host"]}/welcome/bonuses.html">
                                                    <spring:message code="footer-new.jsp.propertie.27" text="default text"> </spring:message>
                                                </a>
                                            </div>
                                            <div class="f-menu_i">
                                                <a class="f-menu_link" href="https://${properties["charity.application.host"]}">
                                                    <spring:message code="footer-new.jsp.propertie.26" text="default text"> </spring:message>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="f-menu-list_i col-4">
                                    <div class="f-menu">
                                        <div class="f-menu_header">
                                            <spring:message code="footer-new.jsp.propertie.4" text="default text"> </spring:message>
                                        </div>

                                        <div class="f-menu_list">
                                            <div class="f-menu_i">
                                                <a class="f-menu_link" href="https://${properties["application.host"]}/about">
                                                    <spring:message code="footer-new.jsp.propertie.5" text="default text"> </spring:message>
                                                </a>
                                            </div>
                                            <div class="f-menu_i">
                                                <a class="f-menu_link" href="https://${properties["application.host"]}/advertising">
                                                    <spring:message code="header.advertising" text="default text"> </spring:message>
                                                </a>
                                            </div>
                                            <div class="f-menu_i">
                                                <a class="f-menu_link" href="https://${properties["application.host"]}/partners">
                                                    <spring:message code="footer-new.jsp.propertie.9" text="default text"> </spring:message>
                                                </a>
                                            </div>
                                            <div class="f-menu_i">
                                                <a class="f-menu_link" href="https://${properties["application.host"]}/faq/article/12!paragraph198">
                                                    <spring:message code="footer-new.jsp.propertie.24" text="default text"> </spring:message>
                                                </a>
                                            </div>
                                            <div class="f-menu_i">
                                                <a class="f-menu_link" href="https://${properties["application.host"]}/about?#about-contacts">
                                                    <spring:message code="footer-new.jsp.propertie.2" text="default text"> </spring:message>
                                                </a>
                                            </div>

                                        </div>
                                    </div>
                                </div>

                                <div class="f-menu-list_i col-4">
                                    <div class="f-menu">
                                        <div class="f-menu_header">
                                            <spring:message code="footer-new.jsp.propertie.21" text="default text"> </spring:message>
                                        </div>

                                        <div class="f-menu_list">
                                            <div class="f-menu_i">
                                                <a class="f-menu_link" href="https://${properties["application.host"]}/faq">
                                                    <spring:message code="footer-new.jsp.propertie.34" text="default text"> </spring:message>
                                                </a>
                                            </div>
                                            <div class="f-menu_i">
                                                <a class="f-menu_link" href="https://${properties["application.host"]}/welcome/projects-agreement.html">
                                                    <spring:message code="footer-new.jsp.propertie.23" text="default text"> </spring:message>
                                                </a>
                                            </div>
                                            <div class="f-menu_i">
                                                <a class="f-menu_link" href="https://${properties["application.host"]}/faq/article/8!paragraph54">
                                                    <spring:message code="footer-new.jsp.propertie.35" text="default text"> </spring:message>
                                                </a>
                                            </div>
                                            <div class="f-menu_i">
                                                <a class="f-menu_link" href="https://${properties["application.host"]}/faq/article/10!paragraph80">
                                                    <spring:message code="footer-new.jsp.propertie.36" text="default text"> </spring:message>
                                                </a>
                                            </div>

                                            <c:if test="${param['no-counters'] != true && header['User-Agent'] != 'planeta-snapshot-bot'}">
                                                <div class="f-menu_i">
                                                    <a class="f-menu_link fic-mistake-a" href="javascript:void(0)">
                                                        <spring:message code="footer-new.jsp.propertie.1" text="default text"> </spring:message>
                                                    </a>
                                                </div>
                                            </c:if>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="m-col-ld-4 m-col-tp-6 col-12 js-footer-digest">
                        </div>

                    </div>
                </div>


                <div class="m-col-tp-3 m-col-tp-pull-9 col-10">
                    <div class="f-info">
                        <div class="f-info_logo">
                            <a href="https://planeta.ru" class="logo">
                                <svg width="116" height="30" viewBox="0 0 116 30">
                                    <g fill="#00B2E2">
                                        <path d="M25.942 4.39c5.859 5.859 5.859 15.346 0 21.204-5.858 5.859-15.345 5.859-21.204 0-5.858-5.858-5.858-15.345 0-21.204 5.859-5.843 15.361-5.843 21.204 0zm-1.413 1.414C19.455.73 11.225.73 6.152 5.804c-5.073 5.073-5.073 13.303 0 18.377 5.073 5.073 13.303 5.073 18.377 0 5.073-5.074 5.073-13.304 0-18.377z"></path>
                                        <path d="M28.236 5.097a3 3 0 1 1-6 0 3 3 0 0 1 6 0zM96.639 14.27a3 3 0 0 0-3 3c0 1.649 1.351 3 3 3a3 3 0 0 0 3-3c0-1.65-1.351-3-3-3zM21.702 8.631a8.987 8.987 0 0 1 0 12.722 8.986 8.986 0 0 1-12.723 0 8.987 8.987 0 0 1 0-12.722c3.518-3.503 9.22-3.503 12.723 0z"></path>
                                    </g>
                                    <path d="M39.796 11.882c1.712 0 2.136 1.539 2.136 3.047 0 1.414-.613 2.733-2.199 2.733-1.602 0-2.058-1.743-2.058-2.874-.015-1.508.534-2.906 2.121-2.906zM37.66 9.966h-2.403v13.288h2.481v-5.042l.047.016c.299.518.911 1.46 2.655 1.46 2.827 0 4.021-2.465 4.021-5.057 0-3.283-1.869-4.916-3.974-4.916-1.681 0-2.356.879-2.827 1.617h-.032V9.966zm8.01-3.314h2.482v12.801H45.67zm6.361 6.393c.063-.55.283-1.32 1.54-1.32.628 0 1.633.047 1.633 1.115 0 .456-.392.739-1.445.833-2.387.188-4.429 1.099-4.429 3.282 0 1.964 1.445 2.765 2.874 2.765 1.697 0 2.498-.801 3.063-1.382.047.534.094.675.236 1.131h2.685v-.393c-.298-.11-.549-.346-.549-1.461v-5.136c0-2.34-2.435-2.749-3.77-2.749-1.932 0-3.942.409-4.225 3.315h2.387zm3.173 1.79v1.21c0 1.146-1.131 1.869-2.214 1.869-.487 0-1.163-.267-1.163-1.147 0-1.021.754-1.272 1.697-1.429 1.005-.142 1.429-.314 1.68-.503zm4.068-4.869v9.487h2.482v-5.419c0-1.445.848-2.199 1.963-2.199 1.587 0 1.618 1.178 1.618 1.869v5.765h2.482v-6.566c0-2.073-1.43-3.173-3.283-3.173-1.791 0-2.513 1.068-2.859 1.634h-.031V9.966h-2.372zm16.053 6.707c-.189.675-.896 1.036-1.602 1.036-2.011 0-2.168-1.602-2.168-2.324h6.44v-.927c0-3.267-1.775-4.728-4.445-4.728-4.147 0-4.555 3.644-4.555 4.791 0 3.895 2.057 5.183 4.728 5.183 1.617 0 3.502-.754 4.162-3.031h-2.56zm-3.77-2.906c.141-1.241.77-1.995 1.963-1.995.801 0 1.775.471 1.917 1.995h-3.88zm8.01-6.33v2.545h-1.272v1.775h1.272v5.764c0 1.241.393 2.042 2.875 2.042.392 0 .848-.016 1.162-.047v-1.869a3.864 3.864 0 0 1-.518.031c-1.005 0-1.037-.204-1.037-.754v-5.167h1.555V9.966h-1.555V7.437h-2.482zm7.304 5.608c.063-.55.283-1.32 1.539-1.32.629 0 1.634.047 1.634 1.115 0 .456-.393.739-1.445.833-2.372.188-4.429 1.099-4.429 3.282 0 1.964 1.445 2.765 2.874 2.765 1.696 0 2.497-.801 3.063-1.382.047.534.094.675.235 1.131h2.686v-.393c-.298-.11-.55-.346-.55-1.461v-5.136c0-2.34-2.434-2.749-3.769-2.749-1.932 0-3.943.409-4.225 3.315h2.387zm3.173 1.79v1.21c0 1.146-1.131 1.869-2.215 1.869-.487 0-1.162-.267-1.162-1.147 0-1.021.754-1.272 1.696-1.429 1.005-.142 1.43-.314 1.681-.503zm10.728-4.869v9.487h2.481V14.49c0-1.053.377-2.247 2.309-2.247.236 0 .487.016.723.048V9.762a4.976 4.976 0 0 0-.487-.032c-1.524 0-2.073.927-2.592 1.885h-.031V9.966h-2.403zm6.361 0v6.236c0 2.418 1.335 3.486 3.204 3.486 1.319 0 2.325-.581 2.89-1.586h.032v1.335h2.387V9.95h-2.482v5.466c0 1.697-1.115 2.168-1.916 2.168-.77 0-1.618-.299-1.618-1.744V9.966h-2.497z"
                                          fill="#000000"></path>
                                </svg>
                            </a>
                        </div>

                        <div class="f-info_tel">
                            +7 (495) 181-05-05
                        </div>

                        <div class="f-info_address">
                            <spring:message code="footer-new.jsp.propertie.28" text="default text"> </spring:message>
                            <br>
                            <spring:message code="footer-new.jsp.propertie.29" text="default text"> </spring:message>
                            <br>
                            <spring:message code="footer-new.jsp.propertie.30" text="default text"> </spring:message>
                        </div>

                        <div class="f-info_time">
                            <spring:message code="footer-new.jsp.propertie.31" text="default text"> </spring:message>
                            <br>
                            <spring:message code="footer-new.jsp.propertie.32" text="default text"> </spring:message>
                            <br>
                            <spring:message code="footer-new.jsp.propertie.33" text="default text"> </spring:message>
                        </div>
                    </div>
                </div>

                <a class="f-runet" title="Мы получили премию Рунета!" href="http://premiaruneta.ru/press/26/"></a>
            </div>
        </div>
    </div>

    <div class="footer_bottom">
        <div class="wrap">
            <div class="wrap-row">
                <div class="m-col-tp-9 m-col-tp-push-3 col-12">
                    <div class="wrap-row">
                        <div class="m-col-ld-8 m-col-pl-5 col-12">
                            <div id="switch-language" class="f-lang">
                                <span class="f-lang_link">
                                    <span class="f-lang_ico"><span class="flag-icon-gb"></span></span>
                                    <span class="f-lang_lbl"><spring:message code="switch.locale" text="default text"/></span>
                                </span>
                            </div>
                        </div>

                        <div class="m-col-ld-4 m-col-pl-7 col-12">
                            <div class="f-social js-social-anchor"></div>
                        </div>
                    </div>
                </div>

                <div class="m-col-tp-3 m-col-tp-pull-9 col-12">
                    <div class="footer_copy">
                        Planeta.ru &copy; 2012-2018
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="to-top">
    <div class="to-top_block">
        <div class="to-top_text">
            Наверх
        </div>
    </div>
</div>
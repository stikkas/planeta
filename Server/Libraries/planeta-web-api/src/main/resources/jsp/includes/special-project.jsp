<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<div id="js-header-special-project" class="h-special hidden">
    <div class="wrap">
        <div class="col-12">
            <!-- Start header logo -->
            <%@ include file="header-logo.jsp" %>
            <!-- End header logo -->

            <div class="h-special_create">
                <div class="pwa-create js-create-button-block" >
                    <a href="//${properties['application.host']}/funding-rules" class="pwa-create-link js-create-button"><spring:message code="special-project.jsp.propertie.1" text="default text"> </spring:message></a>
                </div>
            </div>
            <div class="spec-projects">
                <div class="spec-projects_lbl">
                    <spring:message code="special-project.jsp.propertie.2" text="default text"> </spring:message>
                </div>
                <div class="spec-projects_val">
                    <a href="//${properties['library.application.host']}/" class="spec-projects_i spec-projects-biblio-button" target="_self">
                        <img src="//${hf:getStaticBaseUrl("")}/images/biblio/logo-center-white.svg" width="127" height="27" alt='<spring:message code="special-project.jsp.biblio" text="default text"> </spring:message>'>
                    </a>
                    <a href="//${properties['promo.application.host']}/campus" class="spec-projects_i">
                        <img src="//${hf:getStaticBaseUrl("")}/images/biblio/logo-center-white.svg/images/campus/campus-logo-text.svg" width="82" height="25">
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>

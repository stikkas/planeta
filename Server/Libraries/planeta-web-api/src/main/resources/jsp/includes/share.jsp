<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<!--ShareBar.net BEGINS-->
<script type="text/javascript">
    $(document).ready(function() {
        window.hideShareBar || $('.share-cont-vertical').share({orientation: 'vertical', className: 'vertical', counterEnabled: true, hidden: false, parseMetaTags: true});
    });
</script>
<div class="share-cont-vertical share-cont"></div>
<!--ShareBar.net ENDS-->

<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ include file="/WEB-INF/jsp/includes/taglibs.jsp" %>
<link rel="apple-touch-icon" sizes="180x180" href="//${hf:getStaticBaseUrl("")}/images/favicons/apple-touch-icon.png">
<link rel="icon" type="image/png" href="//${hf:getStaticBaseUrl("")}/images/favicons/favicon-32x32.png" sizes="32x32">
<link rel="icon" type="image/png" href="//${hf:getStaticBaseUrl("")}/images/favicons/favicon-16x16.png" sizes="16x16">
<link rel="icon" type="image/png" href="//${hf:getStaticBaseUrl("")}/images/favicons/apple-touch-icon-120x120.png" sizes="128x128">
<link rel="icon" type="image/png" href="//${hf:getStaticBaseUrl("")}/images/favicons/android-chrome-192x192.png" sizes="196x196">
<link rel="shortcut icon" href="//${hf:getStaticBaseUrl("")}/images/favicons/favicon.ico">

<link rel="mask-icon" href="//${hf:getStaticBaseUrl("")}/images/favicons/safari-pinned-tab.svg" color="#5bbad5">
<link rel="apple-touch-icon" href="//${hf:getStaticBaseUrl('')}/images/planeta-retina.png"/>
<link rel="apple-touch-icon-precomposed" href="//${hf:getStaticBaseUrl('')}/images/planeta-retina.png"/>
<link rel="apple-touch-startup-image" href="//${hf:getStaticBaseUrl("")}/images/startup.png">

<meta name="theme-color" content="#00BFDF">

<link rel="manifest" href="/api/util/manifest.json">
<meta name="msapplication-config" content="/api/util/browserconfig.xml">

<meta name="apple-mobile-web-app-capable" content="yes">

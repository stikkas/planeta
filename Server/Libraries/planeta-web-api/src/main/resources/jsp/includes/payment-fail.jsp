<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ include file="/WEB-INF/jsp/includes/generated/doctype-header.jsp" %>
<head>
    <%@include file="/WEB-INF/jsp/includes/generated/stat-counters-async.jsp"%>
    <%@ include file="/WEB-INF/jsp/includes/common-css.jsp" %>
    <%@ include file="/WEB-INF/jsp/includes/generated/common-templates.jsp" %>
    <%@ include file="/WEB-INF/jsp/includes/planeta-js.jsp" %>

    <p:script src="payment.js" />
    <%@include file="/WEB-INF/jsp/includes/generated/common-favicon.jsp" %>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">

    <!-- Sharing meta data: start -->
    <link rel="image_src" href=""/>
    <meta name="viewport" content="width=device-width"/>
    <meta property="og:site_name" content="Planeta.ru"/>
    <c:if test="${campaign == null}">
        <title><spring:message code="payment.fail.jsp.propertie.3" text="default text"> </spring:message></title>
        <meta property="og:title" content=" <fmt:formatDate value="${event.profile.displayName}" pattern="dd.MM.yy"/>" />
    </c:if>

    <c:if test="${campaign != null}">
        <title><spring:message code="payment-success.jsp.propertie.35" text="default text"></spring:message> &laquo;${hf:escapeHtml4(campaign.name)}&raquo;</title>
        <meta property="og:title" content="<spring:message code='payment-success.jsp.propertie.35' text='default text'></spring:message> &laquo;${hf:escapeHtml4(campaign.name)}&raquo;" />
        <meta property="og:description" content="<spring:message code='payment-success.jsp.propertie.36' text='default text'></spring:message>"/>
        <meta name="description" content="<spring:message code='payment-success.jsp.propertie.36' text='default text'></spring:message>"/>
    </c:if>

    <meta property="og:image" content="" />
    <!-- Sharing meta data: end -->


    <script type="text/javascript">
        var PaymentFailCallbackView = UserCallback.Views.BaseView.extend({
            template: '#payment-fail-callback-template'
        });

        var PaymentFailFeedbackView = BaseView.extend({
            template: '#payment-fail-feedback-template',
            className: 'pln-payment-error-loyal_head-action',
            events: {
                'click .fic-mistake-a': 'onFeedbackClicked'
            },
            onFeedbackClicked: function (e) {
                if (e) e.preventDefault();
                FeedbackHelper.showFeedBackDialog({theme: 'payment'});
            }
        });

        var PaymentFailFeedbackSupportContactsView = BaseView.extend({
            template: '#payment-fail-feedback-template-support-contacts'
        });

        var PaymentFailView = UserPayment.Views.Payment.extend({
            template: '#payment-fail-template'
        });

        $(document).ready(function(){
            $('.close-alert').click(function(){
                $('.content-alert-error').hide();
            });

            var model = new UserPayment.Models.BasePayment({
                transactionId: ${payment.transactionId},
                redirectUrl: '/payment-recreate.html',
                amount: ${payment.amountNet},
                oldPayment: true
            });

            var view = new PaymentFailView({
                model: model,
                customParams: ['transactionId'],
                el:'.js-payment-block'
            });

            model.fetch({
                success: function () {
                    try {
                        view.render();
                    } catch (e) {
                        console.log(e);
                    }
                },
                error: function (res) {
                }
            });

            var callbackModel = UserCallback.Models.createFailurePayment(${payment.transactionId}, ${payment.amountNet});
            callbackModel.fetch().done(function() {
                if (callbackModel.get('available')) {
                    var callbackView = new PaymentFailCallbackView({
                        model: callbackModel,
                        el: '.js-callback-container'
                    });
                    callbackView.render();
                } else {
                    var feedbackView = new PaymentFailFeedbackView({
                        model: callbackModel,
                        el: '.js-feedback'
                    });
                    feedbackView.render();

                    var feedbacSupportCantactsView = new PaymentFailFeedbackSupportContactsView({
                        model: callbackModel,
                        el: '.js-callback'
                    });
                    feedbacSupportCantactsView.render();
                }
            });

            $('.js-send-feedback-form').on('click', function(e) {
                e.preventDefault();
                var form = $("#user-feedback-form").serialize();
                if (form.indexOf('score=') == -1) {
                    return;
                }
                $.post('/api/profile/add-user-feedback.json', form).done(function (response) {
                    if (response && response.success) {
                        if (response.result) {
                            $('.js-feedback-message-container').html('<div class="quality-polling_head">Спасибо! Ваш отзыв отправлен!</div>');
                        } else {
                            $('.js-feedback-message-container').html('<div class="quality-polling_head">Вы уже оставили отзыв ранее!</div>');
                        }
                    } else {
                        $('.js-feedback-message-container').html('<div class="quality-polling_head">При отправке отзыва поизошла ошибка!</div>');
                    }
                });
            });
            <c:if test="${not empty order}">
            $.post('/api/profile/check-user-feedback-exists.json', {orderId: ${order.orderId}}).done(function (response) {
                if (response && !response.result) {
                    $('.js-feedback-form').css('display', 'block');
                }
            });
            </c:if>


        });
    </script>

    <script id="payment-fail-feedback-template" type="text/x-jquery-template">
        <span class="btn btn-primary btn-lg  fic-mistake-a">
            <spring:message code="payment.fail.jsp.propertie.4" text="default text"> </spring:message>
        </span>
    </script>

    <script id="payment-fail-feedback-template-support-contacts" type="text/x-jquery-template">
            <div class="pln-payment-error-loyal_mail-report">
                <spring:message code="payment.fail.jsp.propertie.5" text="default text"> </spring:message>
                <b>${properties['support.phone.number']}</b>
                <spring:message code="payment.fail.jsp.propertie.6" text="default text"> </spring:message>
                <a href="mailto:${properties['support.email']}"><b>${properties['support.email']}</b></a>
            </div>
    </script>

    <script id="payment-fail-callback-template" type="text/x-jquery-template">
    <div class="pln-payment-error-loyal_row pln-payment-error-loyal_row__phone">

            <div class="pln-payment-error-loyal_cell">
                <div class="pln-payment-error-loyal_form">
                    <div class="breadcrumbs-campaign-page-templatepln-payment-error-loyal_form-head">
                        <spring:message code="payment.fail.jsp.propertie.7" text="default text"> </spring:message>
                    </div>
                    <div class="pln-payment-error-loyal_form-input">
                        <input class="form-control control-xlg js-phone" type="text">
                    </div>
                    <span class="js-callback-state1">
                    <div class="pln-payment-error-loyal_form-btn">
                        <button class="btn btn-primary btn-block btn-xlg js-callback">
                            <spring:message code="payment.fail.jsp.propertie.8" text="default text"> </spring:message>
                        </button>
                    </div>
                    </span>
                    <span class="js-callback-state2 hidden">
                        <spring:message code="payment.fail.jsp.propertie.9" text="default text"> </spring:message>
                    </span>
                </div>
            </div>


            <div class="pln-payment-error-loyal_cell">
                <div class="pln-payment-error-loyal_or">
                    <spring:message code="payment.fail.jsp.propertie.10" text="default text"> </spring:message>
                </div>

                <div class="pln-payment-error-loyal_form">
                    <div class="pln-payment-error-loyal_form-head">
                        <spring:message code="payment.fail.jsp.propertie.11" text="default text"> </spring:message>
                    </div>
                    <div class="pln-payment-error-loyal_form-btn">
                        <button class="btn btn-xlg fic-mistake-a">
                            <spring:message code="payment.fail.jsp.propertie.12" text="default text"> </spring:message>
                        </button>
                    </div>
                </div>
            </div>

                        </div>

            <div class="pln-payment-error-loyal_row">
                <div class="pln-payment-error-loyal_mail-report">
                    <spring:message code="payment.fail.jsp.propertie.5" text="default text"> </spring:message>
                    <b>${properties['support.phone.number']}</b>
                    <spring:message code="payment.fail.jsp.propertie.6" text="default text"> </spring:message>
                    <a href="mailto:${properties['support.email']}"><b>${properties['support.email']}</b></a>
                </div>
            </div>
    </script>
</head>

<body>
<%@include file="/WEB-INF/jsp/includes/generated/irma/header.jsp" %>
<%@include file="/WEB-INF/jsp/includes/generated/irma/main-nav.jsp" %>
<%@include file="/WEB-INF/jsp/includes/generated/mobile-header.jsp"%>

<div id="global-container" class="wrap-container">
    <div id="main-container" class="wrap main-container">
        <div class="col-12" id="center-container">
            <div class="pln-content-box">
                <div class="pln-content-box_cont">

                    <div class="pln-payment-error-loyal">
                        <div class="pln-payment-error-loyal_row">
                            <div class="pln-payment-error-loyal_head">
                                <span class="hl">
                                    <spring:message code="payment.fail.jsp.propertie.13" text="default text"/></span><spring:message code="payment.fail.jsp.propertie.14" text="default text"/>
                            </div>

                            <div class="pln-payment-error-loyal_head-text">
                                <spring:message code="payment.fail.jsp.propertie.15" text="default text"> </spring:message>
                            </div>

                            <div class="js-feedback"></div>
                        </div>

                        <div class="pln-payment-error-loyal_row js-callback"></div>
                        <div class="js-callback-container"></div>
                    </div>
                </div>
            </div>

            <div class="pln-payment-box js-feedback-form" style="display: none;">
                <form id="user-feedback-form" class="quality-polling js-feedback-message-container">
                    <div class="quality-polling_head">
                        Оцените качество сервиса
                    </div>
                    <div class="quality-polling_tip">
                        1 — очень плохо, 5 — хорошо, 10 — отлично!
                    </div>
                    <div class="quality-polling_value">
                        <div class="quality-polling-score">
                            <c:forEach var = "i" begin = "1" end = "10">
                                <div class="quality-polling-score_i">
                                    <div class="form-ui form-ui-default">
                                        <input class="form-ui-control" id="score-${i}" type="radio" name="score" value="${i}">
                                        <label class="form-ui-label" for="score-${i}">
                                                <span class="form-ui-txt">
                                                    <span class="form-ui-val">${i}</span>
                                                </span>
                                        </label>
                                    </div>
                                </div>
                            </c:forEach>

                        </div>
                    </div>

                    <input type="hidden" name="orderId" value="${order.orderId}"/>
                    <input type="hidden" name="userId" value="${order.buyerId}"/>
                    <input type="hidden" name="pageType" value="PAYMENT_FAIL"/>
                    <c:if test="${campaign != null}">
                        <input type="hidden" name="campaignId" value="${campaign.campaignId}"/>
                    </c:if>

                    <div class="quality-polling_action">
                        <div class="quality-polling_extra">
                            <div class="form-ui form-ui-default">
                                <input class="form-ui-control" id="extraTesting" type="checkbox" name="extraTesting" value="true">
                                <label class="form-ui-label" for="extraTesting">
                                    <span class="form-ui-txt">я хочу принять участие в детальном опросе</span>
                                </label>
                            </div>
                        </div>
                        <div class="quality-polling_btn">
                            <button type="submit" class="btn btn-primary js-send-feedback-form">Отправить</button>
                        </div>
                    </div>
                </form>
            </div>

            <div class="js-payment-block"></div>
        </div>
    </div>
</div>

<%@ include file="/WEB-INF/jsp/includes/generated/irma/footer.jsp" %>
</body>
</html>

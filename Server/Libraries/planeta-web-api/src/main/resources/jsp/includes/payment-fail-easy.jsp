<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ include file="/WEB-INF/jsp/includes/generated/doctype-header.jsp" %>


<head>
    <link rel="stylesheet" type="text/css" href="/errorpages/errorpages.css">
    <%@include file="/WEB-INF/jsp/includes/generated/common-favicon.jsp" %>
</head>
<body id="js-body">
    <div class="error-page">
        <header class="header">
            <div class="wrap">
                <a class="header-logo" href="https://planeta.ru/"><img src="/errorpages/header-logo.png"></a>

                <nav class="header-menu">

                    <a class="header-menu-link" href="https://planeta.ru/search/projects">
                        <spring:message code="payment-fail-easy.jsp.propertie.2" text="default text"> </spring:message>
                    </a>

                    <a class="header-menu-link" href="https://shop.planeta.ru/">
                        <spring:message code="payment-fail-easy.jsp.propertie.4" text="default text"> </spring:message>
                    </a>
                </nav>
            </div>
        </header>

        <div class="wrap">
            <div class="page-404">
                <div class="page-404-descr">
                    <spring:message code="payment-fail-easy.jsp.propertie.5" text="default text"> </spring:message>
                </div>

                <div class="page-404-recommend">
                    <c:choose>
                        <c:when test="${not empty errorMessage}">
                            <p>${errorMessage}</p>
                        </c:when>

                        <c:otherwise>
                            <p><spring:message code="payment-fail-easy.jsp.propertie.6" text="default text"> </spring:message></p>
                        </c:otherwise>
                    </c:choose>
                </div>

                <div class="page-404-recommend">
                    <spring:message code="payment-fail-easy.jsp.propertie.7" text="default text"> </spring:message>
                    <a class="p-404-recommend-link" href="/payment/shopping-cart" >
                        <spring:message code="payment-fail-easy.jsp.propertie.8" text="default text"> </spring:message>
                    </a>.
                </div>
                <br />
                <div class="page-404-recommend"><spring:message code="payment-fail-easy.jsp.propertie.9" text="default text"> </spring:message> <a class="p-404-recommend-link" href="mailto:payment.support@planeta.ru">payment.support@planeta.ru</a></div>
            </div>
        </div>
    </div>

    <footer class="footer">
        <div class="wrap">
            <div class="footer-copy">&copy; 2018 Planeta.ru</div>
            <a class="footer-link" href="https://planeta.ru/about">
                <spring:message code="payment-fail-easy.jsp.propertie.10" text="default text"> </spring:message>
            </a>
        </div>
    </footer>
</body>
</html>

<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<c:if test="${not interactiveCampaignZone}">
    <div class="h-signin">
        <a href="javascript:void(0)" class="h-signin_link js-signup-link" data-toggle="modal">
            <spring:message code="header.signin" text="default text"> </spring:message>
        </a>
    </div>
</c:if>

<%@ include file="header-info-auth-forms.jsp" %>


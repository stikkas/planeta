<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<a class="logo" href="//${properties['application.host']}/">
    <div style="width:150px; height:40px">
        <%@ include file="logo-black-svg.jsp" %>
    </div>
</a>

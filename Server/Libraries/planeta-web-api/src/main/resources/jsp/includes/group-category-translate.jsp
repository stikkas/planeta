<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ include file="/WEB-INF/jsp/includes/taglibs.jsp" %>

<c:if test="${group_category_type =='MUSIC'}">
    Музыка
</c:if>
<c:if test="${group_category_type=='VISUAL_ART'}">
    Изобразительное искусство
</c:if>
<c:if test="${group_category_type=='MOVIE'}">
    Кино
</c:if>
<c:if test="${group_category_type=='THEATER'}">
    Театр
</c:if>
<c:if test="${group_category_type=='PHOTOGRAPHY'}">
    Фотография
</c:if>
<c:if test="${group_category_type=='LITERATURE'}">
    Издания
</c:if>
<c:if test="${group_category_type=='GAMING_INDUSTRY'}">
    Игровая индустрия
</c:if>
<c:if test="${group_category_type=='CHARITY'}">
    Благотворительность
</c:if>
<c:if test="${group_category_type=='DESIGN'}">
    Дизайн
</c:if>
<c:if test="${group_category_type=='DANCE'}">
    Танцевальное искусство
</c:if>
<c:if test="${group_category_type=='BUSINESS'}">
    Бизнес
</c:if>
<c:if test="${group_category_type=='TECHNOLOGY'}">
    Технологии
</c:if>
<c:if test="${group_category_type=='SPORT'}">
    Спорт
</c:if>
<c:if test="${group_category_type=='FOOD'}">
	Еда
</c:if>
<c:if test="${group_category_type=='OTHER'}">
    Другое
</c:if>

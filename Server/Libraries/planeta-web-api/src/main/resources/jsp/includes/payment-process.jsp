<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<div class="content-alert-warning mrg-b-30 mrg-t-20">
    <i class="three-types-alerts-warning"></i>
    <p class="big">Вам выставлен счет для оплаты</p>
    <p>Для совершения платежа вам нужно будет подтвердить выставленный вам счет.</p>
</div>
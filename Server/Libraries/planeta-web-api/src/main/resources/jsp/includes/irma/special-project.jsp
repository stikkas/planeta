<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<div class="h-special">
    <div class="h-special_cont">
        <div class="h-quick-links">
            <a href="https://${properties['application.host']}/welcome/one-hundred-millions.html" class="h-quick-links_i">
                <span class="h-quick-links_ico">
                    <svg width="19" height="19" viewBox="0 0 19 19"><path d="M8.866 2c.745.01.77.476.955 1.046.102.311.172.64.274.969.413 1.353.738 2.78 1.184 4.122.031.107.025.19.057.292.197.596.318 1.251.535 1.846.065-.108.105-.206.133-.308.083-.184.147-.393.242-.585.3-.61.554-1.282.821-1.91.204-.48.287-.886.847-1.014a.573.573 0 0 1 .274.03c.329.105.574.383.636.723.178.481.382.986.573 1.464.102.247.178.49.274.723.017.075.043.147.076.215.175.029.352.034.528.016h1.382c.064.016.13.027.197.03a.88.88 0 0 1 .452.308c.065.105.112.22.14.339a.891.891 0 0 1-.427.83 6.787 6.787 0 0 1-1.922.093 4.08 4.08 0 0 1-1.095-.047c-.61-.205-.764-1.23-1.03-1.815-.058.1-.094.187-.122.277-.076.19-.153.382-.242.585-.299.696-.573 1.442-.904 2.123-.216.435-.267.897-.744 1.06a.77.77 0 0 1-.681-.138c-.408-.298-.427-.992-.605-1.523-.503-1.483-.808-3.071-1.305-4.553-.178-.533-.248-1.082-.44-1.6a2.183 2.183 0 0 0-.196.616c-.14.394-.255.836-.395 1.23-.057.166-.064.314-.121.478-.166.458-.3.969-.452 1.43-.038.102-.025.177-.064.277-.152.42-.235.873-.394 1.307-.306.855-.535 1.782-.815 2.646-.153.47-.267.95-.426 1.4-.05.287-.17.556-.35.785a.73.73 0 0 1-.694.215c-.516-.139-.637-.647-.834-1.107-.427-.992-.904-1.991-1.337-2.984-.108-.255-.223-.51-.33-.754a2.291 2.291 0 0 0-.135-.338H.796a.916.916 0 0 0-.198-.03.883.883 0 0 1-.44-.294 1.098 1.098 0 0 1-.14-.292.753.753 0 0 1 .109-.584c.343-.53 1.088-.4 1.922-.4.393-.04.79-.008 1.171.092.338.266.572.641.662 1.061.16.372.357.753.516 1.123.108.255.184.493.305.739.131.236.242.483.331.738.047-.101.077-.2.121-.292.077-.204.115-.412.185-.6.057-.163.064-.314.12-.477.14-.393.262-.835.395-1.23.102-.298.134-.583.242-.878.535-1.479.86-3.072 1.394-4.553.045-.127.045-.24.09-.369.152-.429.273-.91.426-1.337.063-.315.17-.62.318-.907a.928.928 0 0 1 .344-.261c.067-.011.133-.027.197-.048z"></path></svg>
                </span>
                <span class="h-quick-links_lbl">
                    <spring:message code="special-project.jsp.pulse" text="default text"> </spring:message>
                </span>
            </a>

            <a href="https://${properties['application.host']}/faq/article/7!paragraph47" class="h-quick-links_i">
                <span class="h-quick-links_ico">
                    <svg width="19" height="19" viewBox="0 0 19 19"><path d="M10.013 4.365c-.765-.254-1.657 0-2.294.51S6.7 6.15 6.7 7.041c0 .383.256.637.638.637s.637-.254.637-.637c0-.382.128-.892.51-1.146.382-.255.764-.383 1.274-.255.637.127 1.02.637 1.147 1.146.127.638 0 1.147-.51 1.53a3.8 3.8 0 0 0-1.529 3.058c0 .382.255.637.637.637s.637-.255.51-.637c0-.765.51-1.53 1.147-2.04.892-.764 1.274-1.783 1.019-2.802-.255-1.147-1.147-1.912-2.166-2.167zM8.615 12.925c-.22.22-.22.66-.22.88 0 .22 0 .441.22.661.22.22.66.44.88.44.22 0 .66-.22.66-.44.221 0 .441-.44.441-.66 0-.44-.22-.66-.44-.88-.44-.441-1.1-.441-1.541 0z"></path><path d="M9.5 2A7.494 7.494 0 0 0 2 9.5C2 13.647 5.353 17 9.5 17S17 13.647 17 9.5 13.647 2 9.5 2zm0 13.5c-3.28 0-6-2.72-6-6s2.72-6 6-6 6 2.72 6 6-2.72 6-6 6z"></path></svg>
                </span>
                <span class="h-quick-links_lbl">
                    <spring:message code="special-project.jsp.how" text="default text"> </spring:message>
                </span>
            </a>

            <a href="https://${properties['application.host']}/faq/article/2!paragraph9" class="h-quick-links_i">
                <span class="h-quick-links_ico">
                    <svg width="19" height="19" viewBox="0 0 19 19"><path d="M16.297 10.203h-1.875a.703.703 0 0 1 0-1.406h1.875a.703.703 0 0 1 0 1.406zm-1.378-5.128l-1.406 1.406a.703.703 0 1 1-.995-.994l1.407-1.406a.703.703 0 1 1 .994.994zM9.5 17a.703.703 0 0 1-.703-.703v-1.875a.703.703 0 0 1 1.406 0v1.875A.703.703 0 0 1 9.5 17zm0-11.719a.703.703 0 0 1-.703-.703V2.703a.703.703 0 0 1 1.406 0v1.875a.703.703 0 0 1-.703.703zm-3.018 8.232l-1.407 1.406a.703.703 0 1 1-.994-.994l1.406-1.406a.703.703 0 1 1 .995.993zm-.498-6.825a.703.703 0 0 1-.497-.207L4.081 5.075a.703.703 0 1 1 .994-.994l1.407 1.407a.703.703 0 0 1-.498 1.2zM5.281 9.5a.703.703 0 0 1-.703.703H2.703a.703.703 0 0 1 0-1.406h1.875c.388 0 .703.315.703.703zm7.735 2.813c.186 0 .365.074.497.206l1.406 1.406a.703.703 0 1 1-.994.994l-1.407-1.407a.703.703 0 0 1 .498-1.2z"></path></svg>
                </span>
                <span class="h-quick-links_lbl">
                    <spring:message code="special-project.jsp.what_is" text="default text"> </spring:message>
                </span>
            </a>
        </div>


        <div class="spec-projects">
            <div class="spec-projects_lbl">
                <spring:message code="special-project.jsp.propertie.2" text="default text"> </spring:message>
            </div>
            <div class="spec-projects_val">
                <a href="//${properties['library.application.host']}/" class="spec-projects_i" target="_self">
                    <img src="//${hf:getStaticBaseUrl("")}/images/biblio/logo-center-white.svg" width="117" height="25" alt='<spring:message code="special-project.jsp.biblio" text="default text"> </spring:message>'>
                </a>
                <a href="//${properties['promo.application.host']}/campus" class="spec-projects_i">
                    <img src="//${hf:getStaticBaseUrl("")}/images/campus/campus-logo-text.svg" width="82" height="25">
                </a>
            </div>
        </div>
    </div>
</div>
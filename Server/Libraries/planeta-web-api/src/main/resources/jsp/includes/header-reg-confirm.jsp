<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<c:if test="${isAuthorized}">
<div class="h-reg-confirm hidden">
    <div class="wrap">
        <div class="h-reg-confirm-close"><span class="js-h-reg-close-link h-reg-confirm-link">Напомнить позже<span class="h-reg-close-icon">×</span></span></div>
        <div class="h-reg-confirm-text">Вы не подтвердили регистрацию. Проверьте почту и перейдите по ссылке в
            письме от Planeta.ru. <span class="h-reg-confirm-link js-h-reg-confirm-link">Отправить письмо заново</span>
        </div>
    </div>
</div>
</c:if>
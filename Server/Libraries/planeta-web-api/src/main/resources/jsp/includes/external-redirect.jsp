<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ include file="/WEB-INF/jsp/includes/generated/doctype-header.jsp" %>
<head>
<meta charset="UTF-8">
<title>Planeta</title>
<script type="text/javascript">
    function externalRedirect() {
        window.opener = null;
        window.location.href = "${targetUrl}";
    }
</script>
</head>
<body onload="externalRedirect()">
<noscript>
    <h1>Для корректной работы сайта необходимо включить в настройках браузера Javascript.</h1>
</noscript>
</body>
</html>

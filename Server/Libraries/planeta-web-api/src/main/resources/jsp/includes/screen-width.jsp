<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@taglib prefix="p" uri="http://planeta.ru/tags" %>
<script>
<c:choose>
    <c:when test="${not empty properties['response.layout.use']}">
        isMobileDev = window.screen.width < 768 && ${properties['response.layout.use']};
    </c:when>
    <c:otherwise>
        isMobileDev = false;
    </c:otherwise>
</c:choose>
</script>

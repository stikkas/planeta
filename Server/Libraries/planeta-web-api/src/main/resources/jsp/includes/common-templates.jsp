<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

    <%-----------------------------------------------------------------------------------------------%>
    <%------------------------------- Profile logins and avatars ------------------------------------%>
    <%-----------------------------------------------------------------------------------------------%>


    <script id="profile-author-display-name" type="text/x-jquery-template">
        <a itemprop="url" href="{{= ProfileUtils.getAbsoluteUserLink(authorProfileId, authorAlias)}}" class="profile-link" profile-link-data="{{= authorProfileId}}"><span itemprop="name">{{= authorName}}</span></a>
    </script>

    <script id="profile-small-avatar" type="text/x-jquery-template">
        <a href="/{{= ProfileUtils.getUserLink(profileId, alias)}}">
            {{if profileType == "USER"}}
            <span style="background-image: url({{= ImageUtils.getUserAvatarUrl(smallImageUrl, ImageUtils.SMALL_AVATAR, userGender)}});"
                 alt="{{= displayName}}"
                 class="profile-link user-avatar"
                 profile-link-data="{{= profileId}}">

                {{if authorProjectsCount > 1 && PrivacyUtils.hasAdministrativeRole()}}
                    <span class="projects-count-badge new-badge">{{= authorProjectsCount}}</span>
                {{/if}}
            </span>
            {{else}}
            <div style="background-image: url({{= ImageUtils.getThumbnailUrl(smallImageUrl, ImageUtils.SMALL_AVATAR, ImageType.GROUP)}});"
                 alt="{{= displayName}}"
                 class="profile-link user-avatar"
                 profile-link-data="{{= profileId}}">

                {{if authorProjectsCount > 1 && PrivacyUtils.hasAdministrativeRole()}}
                    <span class="projects-count-badge new-badge">{{= authorProjectsCount}}</span>
                {{/if}}
            </div>
            {{/if}}
        </a>
    </script>

    <script id="profile-author-small-avatar" type="text/x-jquery-template">
        <a href="{{= ProfileUtils.getAbsoluteUserLink(authorProfileId, authorAlias)}}">
            <img src="{{= ImageUtils.getUserAvatarUrl(authorImageUrl, ImageUtils.SMALL_AVATAR, authorGender) }}" alt="{{= authorName}}" class="profile-link" profile-link-data="{{= authorProfileId}}"/>
        </a>
    </script>

    <%-----------------------------------------------------------------------------------------------%>
    <%--------------------------------- Misc templates ----------------------------------------------%>
    <%-----------------------------------------------------------------------------------------------%>

    <script id="dialog-loader-template" type="text/x-jquery-template">
        <div class="loader">
            <div class="loader-imgs"><img src="//${hf:getStaticBaseUrl("")}/images/loader.gif" alt="loader.gif"></div>
            <div class="loader-message"><span><spring:message code="wait.a.while" text="default text"> </spring:message></span></div>
        </div>
    </script>


    <!-- Online status -->
    <script id="online-status-template" type="text/x-jquery-template">
        {{if typeof(onlineStatus) !== 'undefined'}}
        <div class="status-online">{{if onlineStatus == true}}Online{{else}}
            {{if lastOnline}}
            <spring:message code="was" text="default text"> </spring:message>{{if userGender=='FEMALE'}}<spring:message code="female.ending" text="default text"> </spring:message>{{/if}} {{= DateUtils.formatUserLoginInfo(lastOnline)}}
            {{else userLoginInfo}}
            <spring:message code="was2" text="default text"> </spring:message>{{if userGender=='FEMALE'}}<spring:message code="female.ending" text="default text"> </spring:message>{{/if}} {{= DateUtils.formatUserLoginInfo(userLoginInfo.timeLogin)}}
            {{/if}}
            {{/if}}</div>
        {{else}}

        {{/if}}
    </script>


<%--Templates for media attachments: audio, photo, video--%>
<script id="common-audio-template" type="text/x-jquery-template">
    <div class="track-list">
        <div class="track-list-item row">
            <div class="track-list-item-count">
                <a class="play-track" href="javascript:void(0);"></a>
            </div>
            <div class="track-list-item-name">
                <div class="track-list-item-name-title">{{= $data.name}}</div>
                <div class="track-list-item-name-author">
                    <a href="/search/audios/query={{= StringUtils.hashEscape(encodeURI($data.artist))}}">{{= $data.artist}}</a>
                </div>
            </div>
            <div class="track-list-item-option">
                <div class="track-list-item-option-time">
                    {{= StringUtils.humanDuration($data.duration)}}
                </div>
            </div>
            <div class="js-slider"></div>
        </div>
    </div>
</script>

<script id="common-large-video-template" type="text/x-jquery-template">
    <div class="bb-video-player-cover-big">
        <a class="bb-video-link">
            <img class="bb-video-preview" src="{{= ImageUtils.getThumbnailUrl(image, ImageUtils.CAMPAIGN_VIDEO, ImageType.VIDEO)}}" alt="{{= $data.name }}">
        </a>
        <div class="bb-video-time">{{= StringUtils.humanDuration(duration) }}</div>
        <span class="bb-video-icon">&nbsp;&nbsp;</span>
    </div>
</script>

<script id="common-video-template" type="text/x-jquery-template">
    <div class="bb-video-player-cover">
        <a class="bb-video-link">
            <img class="bb-video-preview" src="{{= ImageUtils.getThumbnailUrl(image, ImageUtils.VIDEO_MIDDLE, ImageType.VIDEO)}}" alt="{{= $data.name }}">
        </a>
        <div class="bb-video-time">{{= StringUtils.humanDuration(duration) }}</div>
        <span class="bb-video-icon">&nbsp;&nbsp;</span>
    </div>
</script>

<script id="common-small-video-template" type="text/x-jquery-template">
    <div class="bb-video-cover">
        <a class="bb-video-link" href="javascript:void(0)">
            <img class="bb-video-preview" src="{{= ImageUtils.getThumbnailUrl(image, ImageUtils.PHOTO_PREVIEW, ImageType.VIDEO) }}">
        </a>
        <div class="bb-video-time">{{= StringUtils.humanDuration(duration) }}</div>
        <span class="bb-video-icon"></span>
    </div>
</script>

<script id="campaign-rich-photo-template" type="text/x-jquery-template">
    <a href="${image}">
        <img src="{{= ImageUtils.getThumbnailUrl(image, ImageUtils.ORIGINAL, ImageType.PHOTO)}}"
        {{if clazz}} class="{{= clazz}}"{{/if}}{{if width}} width="{{= width}}"{{/if}} >
    </a>
</script>

<script id="common-photo-template" type="text/x-jquery-template">
    <div class="image-big-preview">
        <a href="{{= RichMediaUtils.wrapExternalUrl(image)}}" >
            <img src="{{= ImageUtils.getThumbnailUrl(image, ImageUtils.HUGE, ImageType.PHOTO)}}">
        </a>
    </div>
</script>
<script id="common-big-photo-template" type="text/x-jquery-template">
    <div class="image-big-preview">
        <a href="{{= RichMediaUtils.wrapExternalUrl(image)}}">
            <img src="{{= ImageUtils.getThumbnailUrl(image, ImageUtils.BIG, ImageType.PHOTO)}}">
        </a>
    </div>
</script>


    <%-----------------------------------------------------------------------------------------------%>
    <%------------------------------------- pager-template ------------------------------------------%>
    <%-----------------------------------------------------------------------------------------------%>

<script id="pager-template" type="text/x-jquery-template">
    <div class="list-loader">
        <a href="javascript:void(0)"><i class="icon-load-cont"></i> <spring:message code="load.more" text="default text"> </spring:message></a>
    </div>
</script>

<script id="comment-pager-template" type="text/x-jquery-template">
    <div class="show-all"><spring:message code="phrase" text="default text"> </spring:message></div>
</script>

<script id="comment-pager-loading-template" type="text/x-jquery-template">
    <div class="show-all"><i class="icon-load"></i> <spring:message code="loading.dots" text="default text"> </spring:message></div>
</script>

<script id="pager-loading-template" type="text/x-jquery-template">
    <div class="list-loader">
        <a class="load" href="javascript:void(0)"><i class="icon-load"></i> <spring:message code="loading.dots" text="default text"> </spring:message></a>
    </div>
</script>

<script id="charity-news-loader-template" type="text/x-jquery-template">
    <div class="charity-show-more">
        <a class="load charity-show-more_link" href="javascript:void(0)"><i class="icon-load"></i> <spring:message code="loading.dots" text="default text"> </spring:message></a>
    </div>
</script>

<script id="pager-loaded-template" type="text/x-jquery-template">
    <div class="list-loader"><a href="javascript:void(0)"><spring:message code="all.uploaded" text="default text"> </spring:message></a></div>
</script>

<script id="scrollable-list-loader-template" type="text/x-jquery-template">
    <div class="list-loader">
        <a class="load" href="javascript:void(0)"><i class="icon-load"></i> <spring:message code="loading.dots" text="default text"> </spring:message></a>
    </div>
</script>

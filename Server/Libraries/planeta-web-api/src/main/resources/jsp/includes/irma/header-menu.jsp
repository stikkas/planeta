<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<div class="header_menu">
    <div class="h-menu">
        <div class="h-menu_i">
            <a href="//${properties['application.host']}/search/projects" class="h-menu_link">
                <spring:message code="header.projects" text="default text"> </spring:message>
            </a>
        </div>
        <div class="h-menu_i">
            <a href="//${properties['application.host']}/search/shares" class="h-menu_link">
                <spring:message code="header.shares" text="default text"> </spring:message>
            </a>
        </div>
        <div class="h-menu_i">
            <a href="//${properties['application.host']}/contacts" class="h-menu_link">
                <spring:message code="header.contacts" text="default text"> </spring:message>
            </a>
        </div>
    </div>
</div>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<div class="h-menu-container">
    <ul class="h-menu">
        <li class="h-menu_i"><a class="h-menu_link" href="https://${properties['application.host']}/search/projects"><spring:message code="header.projects" text="default text"/></a></li>
        <li class="h-menu_i"><a class="h-menu_link" href="https://${properties['school.application.host']}"><spring:message code="header.school" text="default text"/></a></li>
        <li class="h-menu_i"><a class="h-menu_link" href="https://${properties['shop.application.host']}"> <spring:message code="header.shop" text="default text"/></a></li>
        <li class="h-menu_i h-menu_i__lg"><a class="h-menu_link" href="https://${properties['application.host']}/welcome/bonuses.html"><spring:message code="header.vip.club" text="default text"/></a></li>
        <li class="h-menu_i h-menu_i__lg"><a class="h-menu_link" href="https://${properties['charity.application.host']}"> <spring:message code="header.charity" text="default text"/></a></li>
        <li class="h-menu_i"><a class="h-menu_link" href="https://${properties['application.host']}/about"><spring:message code="header.about" text="default text"/></a></li>

        <li class="h-menu_i h-menu_i__sm pln-dropdown">
            <span class="h-menu_link pln-d-switch"><spring:message code="header.more" text="default text"/><span class="h-menu_arr"></span>
            </span>
            <div class="h-menu-more-popup pln-d-popup">
                <div class="h-menu-more">
                    <div class="h-menu-more_i">
                        <a class="h-menu-more_link" href="https://${properties['charity.application.host']}"><spring:message code="header.charity" text="default text"/></a>
                    </div>
                    <div class="h-menu-more_i">
                        <a class="h-menu-more_link" href="https://${properties['application.host']}/welcome/bonuses.html"><spring:message code="header.vip.club" text="default text"/></a>
                    </div>
                </div>
            </div>
        </li>
    </ul>
</div>

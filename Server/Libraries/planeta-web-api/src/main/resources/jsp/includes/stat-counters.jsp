<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="hf" uri="http://planeta.ru/taglibs/HelperFunctions" %>
<%@taglib prefix="p" uri="http://planeta.ru/tags" %>
<c:if test="${param['no-counters'] != true && header['User-Agent'] != 'planeta-snapshot-bot' && properties['google.tag.manager.id'] != 'false'}">
    <!-- Google Tag Manager -->
    <noscript><iframe src="//www.googletagmanager.com/ns.html?id=${properties['google.tag.manager.id']}" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <script>
        dataLayer = [];
        (function (w, d, s, l, i) {
            w[l] = w[l] || [];
            w[l].push({'gtm.start': new Date().getTime(), event: 'gtm.js'});
            var f = d.getElementsByTagName(s)[0],
                    j = d.createElement(s);
            j.async = true;
            j.src = '//www.googletagmanager.com/gtm.js?id=' + i;
            f.parentNode.insertBefore(j, f);
        })(window, document, 'script', 'dataLayer', '${properties['google.tag.manager.id']}')
    </script>
    <!-- End Google Tag Manager -->
    <p:script src="planeta-gtm.js" />
</c:if>

<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<div class="aboute-content">
    <h1>Контакты</h1>
    <p>
        Если вы хотите предложить информационное партнерство, обратиться по поводу проведения онлайн-трансляции или
        задать вопрос службе поддержки, все необходимые для этого контакты можно найти на этой странице.
    </p>

    <h3>Работа с авторами</h3>
    <p>
        Если вы хотите завести проект и нуждаетесь в оценке идеи, напишите на авторскую почту <a href="mailto:authors@planeta.ru">authors@planeta.ru</a>
    </p>

    <h3>Служба поддержки пользователей</h3>
    <p>
        Если у вас возник вопрос по работе сервиса/сайта, вы можете написать на
        <a href="mailto:support@planeta.ru">support@planeta.ru</a>
        <br/>
        По вопросам оплаты и возврата денежных средств обращайтесь на
        <a href="mailto:payment.support@planeta.ru">payment.support@planeta.ru</a>
    </p>

    <h3>PR-отдел</h3>
    <p>
        По вопросам сотрудничества со СМИ и информационной поддержки вы можете написать Ирине Борисовой <a href="mailto:irina@planeta.ru">irina@planeta.ru</a><br/>
        По вопросам PR благотворительных проектов обращайтесь к Наталье Игнатенко <a href="mailto:pr@planeta.ru">charitypr@planeta.ru</a> <br/>
        По вопросам организации мероприятий вы можете написать Василине Горовой <a href="mailto:vasilina@planeta.ru">vasilina@planeta.ru</a> <br/>
    </p>

    <h3>Онлайн-трансляции</h3>
    <p>
        Если вы хотите организовать онлайн-трансляцию концерта на нашем портале (<a href="http://tv.planeta.ru">http://tv.planeta.ru</a>), вы можете
        написать на <a href="mailto:tv@planeta.ru">tv@planeta.ru</a><br/>
    </p>



    <h3>Рекламодателям</h3>
    <p>
        По поводу размещения рекламы на портале вы можете написать на
        <a href="mailto:sale@planeta.ru">sale@planeta.ru</a>
    </p>

    <div itemscope itemtype="http://www.data-vocabulary.org/Organization">
        <h3>Наши реквизиты</h3>
        <p><span itemprop="name">Общество с ограниченной  ответственностью «Глобал Нетворкс»</span><br />
            ОГРН 1107746618385<br />
            ИНН 7722724252<br />
            Юридический/почтовый адрес:  ${properties["juridical.address"]}</p>

        <div itemprop="address" itemscope itemtype="http://data-vocabulary.org/Address">
            Мы находимся по адресу:
            <span itemprop="street-address">${properties["office.address"]}</span>
        </div>
        <span itemprop="tel">Наш телефон: +7 (495) 181-05-05</span><br/>
        <span itemprop="tel">Мы принимаем звонки с 11:00 до 20:00 в будние дни.</span>
        <p></p>
        <div itemprop="address" itemscope itemtype="http://data-vocabulary.org/Address">
            Адрес для отправки документов и корреспонденции:
            <span itemprop="street-address">${properties["documents.and.correspondence.address"]}</span>
        </div>
    </div>

    <%--<a name="map"></a>--%>
    <h3>Схема проезда</h3>
    <p>
        <script type="text/javascript" charset="utf-8" src="//api-maps.yandex.ru/services/constructor/1.0/js/?sid=Jqt7im0n_hwLlXGGSdAQYl30VtPJtVrN&width=600&height=450"></script>
    </p>

</div>
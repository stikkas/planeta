<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:if test="${param['no-counters'] != true && header['User-Agent'] != 'planeta-snapshot-bot'}">
    <div id="auth-forms" class="hidden">

        <form id="signup-form" method="POST" action="https://${properties['cas.app.host']}/login">
            <div class="modal-login-form">
                <div class="mlf-item fieldset js-error-able<c:if test="${param.error && param.errorCode == 101}"> error</c:if>">
                    <span><spring:message code="header-info-auth-forms.jsp.propertie.3" text="default text"> </spring:message></span>
                    <input class="form-control control-lg" type="text" name="username" value="${username}" data-autofocus/>
                </div>

                <div class="mlf-item fieldset js-error-able<c:if test="${param.error && param.errorCode == 101}"> error</c:if>">
                    <div class="mlf-rec">
                        <a class="remember-link" href="javascript:void(0)" tabindex="-1">
                            <spring:message code="header-info-auth-forms.jsp.propertie.4" text="default text"> </spring:message>
                        </a>
                    </div>
                    <span>
                        <spring:message code="header-info-auth-forms.jsp.propertie.5" text="default text"> </spring:message>
                    </span>
                    <input class="form-control control-lg" type="password" name="password"/>
                    <span class="help-inline">
                        <span class="js-error-password">
                        <c:if test="${param.error && param.errorCode == 101}">
                        <c:if test="${param.error}">
                            ${hf:getCASMessageForCode(param.errorCode)}
                        </c:if>
                        </c:if>
                        </span>
                    </span>
                </div>

                <div class="mlf-item">
                    <a class="btn btn-primary btn-lg" href="javascript:void(0)">
                        <spring:message code="header-info-auth-forms.jsp.propertie.6" text="default text"> </spring:message>
                    </a>

                    <p><spring:message code="header-info-auth-forms.jsp.propertie.1" text="default text"> </spring:message> <a class="registration-link" href="javascript:void(0)"><spring:message code="header-info-auth-forms.jsp.propertie.2" text="default text"> </spring:message></a></p>
                </div>
                    <%-- TODO: use CasAuthenticationEntryPoint.createRedirectUrl instead--%>
                <c:set var="springSecurityCheckDomain" value="${properties['application.host']}"/>
                <c:choose>
                    <c:when test="${projectType == 'MAIN'}">
                        <c:set var="springSecurityCheckDomain" value="${properties['application.host']}"/>
                    </c:when>
                    <c:when test="${projectType == 'CAMPAIGN'}">
                        <c:set var="springSecurityCheckDomain" value="${properties['projects.application.host']}"/>
                    </c:when>
                    <c:when test="${projectType == 'SHOP'}">
                        <c:set var="springSecurityCheckDomain" value="${properties['shop.application.host']}"/>
                    </c:when>
                    <c:when test="${projectType == 'CONCERT'}">
                        <c:set var="springSecurityCheckDomain" value="${properties['concerts.application.host']}"/>
                    </c:when>
                    <c:when test="${projectType == 'TV'}">
                        <c:set var="springSecurityCheckDomain" value="${properties['tv.application.host']}"/>
                    </c:when>
                    <c:when test="${projectType == 'CONCERT_NEW'}">
                        <c:set var="springSecurityCheckDomain" value="${properties['concerts.application.host']}"/>
                    </c:when>
                    <c:otherwise>
                        <c:set var="springSecurityCheckDomain" value="${properties['application.host']}"/>
                    </c:otherwise>
                </c:choose>
                <input type="hidden" name="service"
                       value="https://${springSecurityCheckDomain}/login/cas"/>
                <input type="hidden" name="failureUrl" value=""/>
                <c:if test="${not empty afterAuthorizationRedirectUrl}">
                    <input type="hidden" name="successUrl" value="${afterAuthorizationRedirectUrl}"/>
                </c:if>
                <c:if test="${empty afterAuthorizationRedirectUrl}">
                    <input type="hidden" name="successUrl" value=""/>
                </c:if>

                <input type="hidden" name="sendRenew" value="false"/>
                <input type="hidden" name="rememberMe" value="true"/>
            </div>
        </form>

        <form id="remember-form" action="/api/public/password-recover.json" method="POST">
            <div class="modal-login-form">
                <div class="mlf-item fieldset js-error-able">
                    <span><spring:message code="header-info-auth-forms.jsp.propertie.7" text="default text"> </spring:message></span>
                    <input class="form-control control-lg" type="text" name="email" data-autofocus/>
                    <span class="help-inline">
                        <span class="js-error-email">
                        </span>
                    </span>
                </div>
                <div class="mlf-item">
                    <a href="javascript:void(0)" class="btn btn-primary btn-lg submit-remember"><spring:message code="header-info-auth-forms.jsp.propertie.8" text="default text"> </spring:message></a>
                    <a href="javascript:void(0)" class="btn btn-lg back-from-remember"><spring:message code="header-info-auth-forms.jsp.propertie.9" text="default text"> </spring:message></a>
                </div>
            </div>
        </form>

        <form id="registration-form">

            <div class="modal-login-form">
                <div class="mlf-item fieldset js-error-able-email">
                    <span><spring:message code="header-info-auth-forms.jsp.propertie.3" text="default text"> </spring:message></span>
                        <input class="form-control control-lg" type="text" autocomplete="off" name="email" data-autofocus/>
                        <span class="help-inline">
                            <span class="js-error-email">
                            </span>
                        </span>
                    <span class="help-inline"><span id="unregistered-email" style="display: none;"></span></span>
                    <span class="warning-message"><span id="hint" style="display: none;"></span></span>
                </div>
                <div class="mlf-item fieldset js-error-able-password">
                    <span><spring:message code="header-info-auth-forms.jsp.propertie.5" text="default text"> </spring:message></span>
                    <input class="form-control control-lg" type="password" name="password"/>
                    <span class="help-inline">
                        <span class="js-error-password">
                        </span>
                    </span>
                </div>
                <div class="mlf-item">
                    <a class="btn btn-primary btn-lg" href="javascript:void(0)"><spring:message code="header-info-auth-forms.jsp.propertie.10" text="default text"> </spring:message></a>

                    <p><spring:message code="header-info-auth-forms.jsp.propertie.11" text="default text"> </spring:message> <a class="signup-link" href="javascript:void(0)"><spring:message code="header-info-auth-forms.jsp.propertie.12" text="default text"> </spring:message></a></p>
                </div>
            </div>
            <c:if test="${not empty afterAuthorizationRedirectUrl}">
                <input type="hidden" name="redirectUrl" value="${afterAuthorizationRedirectUrl}">
            </c:if>
            <c:if test="${empty afterAuthorizationRedirectUrl}">
                <input type="hidden" name="redirectUrl" value="/">
            </c:if>

        </form>

    </div>
</c:if>
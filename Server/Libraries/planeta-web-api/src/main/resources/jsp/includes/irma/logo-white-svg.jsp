<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">

<svg xmlns="http://www.w3.org/2000/svg" version="1.1" x="0" y="0" width="100%" height="100%" viewBox="0 0 750 200">
    <title>planeta.ru</title>
    <style>
        .a{fill:#00b8e5;}
        .b{fill:none;stroke:#00b8e5;stroke-width:13px;}
        .c{fill:#fff;}
        .d{fill:#fff;}
        .moon {
            transform-origin: -120% 200%;
            animation-name: rotation;
            animation-duration: 120s;
            animation-iteration-count: infinite;
            animation-timing-function: linear;
        }


        .ball {
            animation: jump 120s cubic-bezier(.17,.67,.78,1.26) 120s infinite;
        }

        @keyframes rotation {
            0% {transform:rotate(0deg);}
            2.1% {transform:rotate(360deg);}
            100% {transform:rotate(360deg);}
        }

        @keyframes jump {
            0% {transform:translateY(-400px);}
            0.3% {transform:translateY(0);}
            0.6% {transform:translateY(-100px);}
            0.9% {transform:translateY(0);}
            1.2% {transform:translateY(-40px);}
            1.5% {transform:translateY(0);}
            1.8% {transform:translateY(-20px);}
            2.1% {transform:translateY(0);}
            100% {transform:translateY(0);}
        }
    </style>
    <circle cx="158.4" cy="32.4" r="20" class="moon1 a"/>
    <circle class="a" cx="95" cy="95" r="60"/>
    <circle cx="95" cy="95" r="90" class="b"/>
    <!--<text x="213" y="123" -->
    <!--font-family="Helvetica" -->
    <!--font-size="108"-->
    <!--style="font-weight: bold;">-->
    <!--planeta-->
    <!--</text>-->
    <g class="c">
        <path d="M251.1 75.6c10.9 0 13.6 9.8 13.6 19.4 0 9-3.9 17.4-14 17.4 -10.2 0-13.1-11.1-13.1-18.3C237.5 84.5 241 75.6 251.1 75.6L251.1 75.6zM237.5 63.4h-15.3v84.6h15.8v-32.1l0.3 0.1c1.9 3.3 5.8 9.3 16.9 9.3 18 0 25.6-15.7 25.6-32.2 0-20.9-11.9-31.3-25.3-31.3 -10.7 0-15 5.6-18 10.3h-0.2V63.4z"/>
        <rect x="288.5" y="42.3" width="15.8" height="81.5"/>
        <path d="M329 83c0.4-3.5 1.8-8.4 9.8-8.4 4 0 10.4 0.3 10.4 7.1 0 2.9-2.5 4.7-9.2 5.3 -15.2 1.2-28.2 7-28.2 20.9 0 12.5 9.2 17.6 18.3 17.6 10.8 0 15.9-5.1 19.5-8.8 0.3 3.4 0.6 4.3 1.5 7.2h17.1v-2.5c-1.9-0.7-3.5-2.2-3.5-9.3V79.4c0-14.9-15.5-17.5-24-17.5 -12.3 0-25.1 2.6-26.9 21.1H329L329 83zM349.2 94.4v7.7c0 7.3-7.2 11.9-14.1 11.9 -3.1 0-7.4-1.7-7.4-7.3 0-6.5 4.8-8.1 10.8-9.1C344.9 96.7 347.6 95.6 349.2 94.4z"/>
        <path d="M375.1 63.4v60.4h15.8V89.3c0-9.2 5.4-14 12.5-14 10.1 0 10.3 7.5 10.3 11.9v36.7h15.8V82.1c0-13.2-9.1-20.2-20.9-20.2 -11.4 0-16 6.8-18.2 10.4h-0.2v-8.9H375.1z"/>
        <path d="M477.3 106.1c-1.2 4.3-5.7 6.6-10.2 6.6 -12.8 0-13.8-10.2-13.8-14.8h41v-5.9c0-20.8-11.3-30.1-28.3-30.1 -26.4 0-29 23.2-29 30.5 0 24.8 13.1 33 30.1 33 10.3 0 22.3-4.8 26.5-19.3H477.3L477.3 106.1zM453.3 87.6c0.9-7.9 4.9-12.7 12.5-12.7 5.1 0 11.3 3 12.2 12.7H453.3z"/>
        <path d="M504.3 47.3v16.2h-8.1v11.3h8.1v36.7c0 7.9 2.5 13 18.3 13 2.5 0 5.4-0.1 7.4-0.3v-11.9c-0.3 0.1-2.2 0.2-3.3 0.2 -6.4 0-6.6-1.3-6.6-4.8V74.8h9.9V63.4h-9.9V47.3H504.3z"/>
        <path d="M550.8 83c0.4-3.5 1.8-8.4 9.8-8.4 4 0 10.4 0.3 10.4 7.1 0 2.9-2.5 4.7-9.2 5.3 -15.1 1.2-28.2 7-28.2 20.9 0 12.5 9.2 17.6 18.3 17.6 10.8 0 15.9-5.1 19.5-8.8 0.3 3.4 0.6 4.3 1.5 7.2h17.1v-2.5c-1.9-0.7-3.5-2.2-3.5-9.3V79.4c0-14.9-15.5-17.5-24-17.5 -12.3 0-25.1 2.6-26.9 21.1H550.8L550.8 83zM571 94.4v7.7c0 7.3-7.2 11.9-14.1 11.9 -3.1 0-7.4-1.7-7.4-7.3 0-6.5 4.8-8.1 10.8-9.1C566.7 96.7 569.4 95.6 571 94.4z"/>
        <path d="M639.3 63.4v60.4h15.8v-31.6c0-6.7 2.4-14.3 14.7-14.3 1.5 0 3.1 0.1 4.6 0.3V62.1c-0.7-0.1-2.4-0.2-3.1-0.2 -9.7 0-13.2 5.9-16.5 12h-0.2V63.4H639.3z"/>
        <path d="M679.8 63.4v39.7c0 15.4 8.5 22.2 20.4 22.2 8.4 0 14.8-3.7 18.4-10.1h0.2v8.5h15.2v-60.4h-15.8v34.8c0 10.8-7.1 13.8-12.2 13.8 -4.9 0-10.3-1.9-10.3-11.1V63.4H679.8z"/>
    </g>
    <!--<text x="632" y="123" -->
    <!--font-family="Helvetica" -->
    <!--font-size="108"-->
    <!--style="font-weight: bold;">-->
    <!--ru-->
    <!--</text>-->
    <circle cx="613" cy="111" r="20" class="ball a" <c:if test="${aliasOrProfileId == 'asavan'}">
            onclick="javascript:$('#section-container').empty();
                    $('head').append($('<style></style>').text('.floatLeft{display:inline-block; vertical-align:top; width:190px}'));
                    $.get('//${properties["status.service.url"]}/count/get-online-for-admins').done(function(resp){
                        var url = '/api/util/profile-list.json?&apiKey=asavanSecretKey&profiles[]=' + resp.profiles;
                        $.get(url).done(function(resp) {
                            console.log(resp);
                            var view = new BaseListView({
                                collection: new BaseCollection(resp),
                                el: '#section-container',
                                itemViewType: ProfileInfoHover.Views.ForAdmin
                            });
                            view.render();
                        });
                    });
                    return false;" </c:if>
        />
</svg>

<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ include file="../screen-width.jsp" %>
<%@ include file="../back-to-top.jsp" %>
<c:if test="${param['no-counters'] != true && header['User-Agent'] != 'planeta-snapshot-bot' && properties['google.tag.manager.id'] != 'false'}">
    <p:script src="planeta-gtm.js" async="true" />
</c:if>

<%--Start header reg confirm --%>
<%@ include file="../header-reg-confirm.jsp"%>
<%--End header reg confirm--%>

<%-- ============================ desktop header begins ========================================= --%>

<%@ include file="special-project.jsp"%>

<div class="header">
    <div class="header_cont js-header">
        <div class="header_toggle">
            <div class="menu-toggle js-open-main-nav">
                <div class="menu-toggle_ico"></div>
                <div class="menu-toggle_text">
                    <spring:message code="header.close" text="default text" />
                </div>
            </div>
        </div>

        <div class="header_wrap">
            <div class="header_main">
                <!-- Start header logo -->
                <div class="header_logo">
                    <%@ include file="header-logo.jsp" %>
                </div>
                <!-- End header logo -->

                <c:set var="profile" value="${myProfile.profile}"/>
                <div class="header_in">
                    <!-- Start header services menu -->
                    <%@ include file="header-menu.jsp" %>
                    <!-- End header services menu -->

                    <!-- Start header search ico -->
                    <div class="header_search-ico js-header-search-button-open">
                        <button class="header_search-btn">
                            <svg width="18" height="18" viewBox="0 0 18 18"><path d="M17.486 15.107a1.681 1.681 0 0 1-.743 2.838 1.681 1.681 0 0 1-1.636-.459l-4.932-4.931a6.745 6.745 0 1 1 2.38-2.379l4.931 4.931zM6.75 1.8a4.95 4.95 0 1 0 0 9.902 4.95 4.95 0 0 0 0-9.902z" fill="#454553"></path></svg>
                        </button>
                    </div>
                    <!-- End header search ico -->

                    <!-- Start header user -->
                    <div class="header_user">
                        <%@ include file="header-user.jsp" %>
                    </div>
                    <!-- End header user -->

                    <c:if test="${projectType eq 'MAIN'}">
                        <div class="header_create">
                            <div class="h-create">
                                <a class="h-create_link js-create-button" href="//${properties['application.host']}/funding-rules">
                                    <spring:message code="header.create" text="default text" />
                                </a>
                            </div>
                        </div>
                    </c:if>
                </div>
            </div>

            <!-- Start header search -->
            <%@ include file="header-search.jsp" %>
            <!-- End header search -->
        </div>

        <div class="header_search-results js-header-result-block"></div>
    </div>

    <div class="header_search-results-overlay"></div>
</div>
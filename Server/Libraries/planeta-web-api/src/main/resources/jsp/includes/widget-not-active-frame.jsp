<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ include file="doctype-header.jsp" %>

<head>
</head>

<body class="frame-video-player-page">
<a href="https://planeta.ru">
    <svg xmlns="http://www.w3.org/2000/svg" version="1.1" x="0" y="0" width="100%" height="100%" viewBox="0 0 200 200">
        <title>planeta.ru-ico</title>
        <style>
            @keyframes rotation {
                0% {transform:rotate(0deg);}
                50% {transform:rotate(0deg);}
                100% {transform:rotate(360deg);}
            }

            .a{fill:#00b8e5;}
            .b{fill:none; stroke:#00b8e5; stroke-width:16;}
            .c{fill:#fff;}
            .d{fill:#000;}
            .moon {
                transform-origin: -120% 200%;
                animation-name: rotation;
                animation-duration: 5s;
                animation-iteration-count: infinite;
                animation-timing-function: linear;
            }

        </style>
        <circle cx="165" cy="35" r="20" class="moon a"></circle>
        <circle cx="100" cy="100" r="60" class="a"></circle>
        <circle cx="100" cy="100" r="92" class="b"></circle>

    </svg>
</a>
</body>
</html>
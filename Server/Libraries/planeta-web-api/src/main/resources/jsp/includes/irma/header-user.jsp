<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<script id="modal-change-password-template" type="text/x-jquery-template">
    <div class="modal-dialog change-password-modal">
        <div class="modal modal-login">
            <div class="modal-header">
                <a class="close" data-dismiss="modal">×</a>
                <div class="modal-title">Смена пароля</div>
                <div class="modal-header-shadow"></div>
            </div>
            <div class="modal-body">
                <div class="modal-login-form">
                    <p>Вы были зарегистрированы на Планете. Ваш пароль был сгенерирован автоматически. Если хотите, Вы можете сменить его прямо сейчас:</p>
                    <div class="mlf-item">
                        <span>Новый пароль:</span>
                        <input class="form-control control-lg" type="password" id="password" name="password">
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="modal-footer-cont">
                    <div class="mlf-item">
                        <button class="btn btn-primary btn-lg" type="submit">Сменить</button>
                        <button class="btn btn-lg" type="reset">Пропустить</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</script>

<c:if test="${isAuthorized}">
    <script>
        <c:set var="oldPassword" value="${param['oldPassword']}"/>
        <c:if test="${not empty oldPassword}">
        $(function() {
            var oldPassword = '${oldPassword}';
            var myProfile = workspace.appModel;

            moduleLoader.loadModule('settings').done(function () {
                var settingsModel = new Settings.Models.Password(myProfile);
                var ChangePasswordModal = Modal.OverlappedView.extend({
                    template: '#modal-change-password-template',
                    save: function () {
                        var jEl = $(".modal-dialog");
                        var password = jEl.find('[name=password]').val();
                        var data = {
                            password: password,
                            currentPassword: oldPassword,
                            confirmPassword: password
                        };
                        settingsModel.savePrivateInfo(data, function () {
                            workspace.appView.showSuccessMessage('Пароль успешно изменен');
                            view.dispose();
                        });
                    },
                    noAutomaticDispose: true
                });
                var view = new ChangePasswordModal({
                    model: myProfile
                });
                view.render();
            });

        });
        </c:if>

    </script>
</c:if>


<c:choose>
    <c:when test="${!isAuthorized}">
        <%@ include file="header-user-unauthorized.jsp" %>
    </c:when>

    <c:otherwise>
        <c:if test="${not interactiveCampaignZone}">
            <%@ include file="header-user-info.jsp" %>
        </c:if>
    </c:otherwise>
</c:choose>

<%--<c:if test="${not interactiveCampaignZone}">--%>
    <%--<!-- Start search -->--%>
    <%--<%@ include file="header-search.jsp" %>--%>
    <%--<!-- End search -->--%>
<%--</c:if>--%>

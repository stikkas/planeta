<?xml version="1.0" encoding="utf-8"?>
<%@ page language="java" contentType="application/xml; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="/WEB-INF/jsp/includes/taglibs.jsp" %>
<browserconfig>
    <msapplication>
        <tile>
            <square150x150logo src="//${hf:getStaticBaseUrl("")}/images/favicons/mstile-150x150.png"/>
            <TileColor>#da532c</TileColor>
        </tile>
    </msapplication>
</browserconfig>

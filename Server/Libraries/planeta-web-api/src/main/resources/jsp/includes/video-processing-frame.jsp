<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ include file="doctype-header.jsp" %>

<head>
	<link type="text/css" rel="stylesheet" href="//${hf:getStaticBaseUrl("")}/css-generated/common.css" />
    <link type="text/css" rel="stylesheet" href="//${hf:getStaticBaseUrl("")}/css-generated/tv.css" />
</head>

<body class="frame-video-player-page">
<div id="frame-video-player" class="frame-video-player">
    <div class="broadcast-video">
        <div class="broadcast-header">
        </div>
        <div class="broadcast-cover">
            <div class="noise">
                <div class="overlay-message">Видеозапись обрабатывается.</div>
            </div>
        </div>

    </div>
</div>
</body>
</html>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
    <div class="h-user h-btn js-h-user-dropdown pln-dropdown">
        <a class="h-user_link h-btn_link pln-d-switch" href="javascript:void(0)">
            <span class="h-btn_arr"></span>
            <img src="${hf:getUserAvatarUrl(myProfileProfile.smallImageUrl, "USER_SMALL_AVATAR", myProfileProfile.userGender)}"
                 alt="${hf:escapeHtml4(myProfileProfile.displayName)}" class="h-user_link-img">
            <span class="h-btn_count">
                <span class="h-btn_count_val js-h-btn_count"></span>
            </span>
        </a>
        <div class="h-user_popup pln-d-popup pln-d-popup pln-d-popup_right">
            <div class="h-user_popup-cont">
                <ul class="h-user_menu">
                    <li class="h-user_menu_i">
                        <a href="https://${properties['application.host']}/account/my-projects" class="h-user_menu_link"><spring:message code="my.campaigns" text="default text"> </spring:message></a>
                    </li>
                    <li class="h-user_menu_i">
                        <a href="https://${properties['application.host']}/${aliasOrProfileId}/news" class="h-user_menu_link"><spring:message code="news.feed" text="default text"> </spring:message></a>
                    </li>
                    <li class="h-user_menu_i">
                        <a href="https://${properties['application.host']}/account/my-purchases-and-rewards" class="h-user_menu_link">
                            <c:if test="${myBalance > 0}">
                            <span class="h-user_menu_notif h-user_menu_notif__bl">${myBalance} <span class="b-rub">Р</span></span>
                            </c:if>
                            <spring:message code="profile" text="default text"> </spring:message>
                        </a>
                    </li>
                    <li class="h-user_menu_i js-messages-container">
                        <a class="h-user_menu_link js-messages-link" href="javascript:void(0)">
                            <span class="h-user_menu_notif" style="display: none;"></span>
                            <spring:message code="messages" text="default text"> </spring:message>
                        </a>
                    </li>
                    <c:if test="${isAdmin}">
                    <li class="h-user_menu_i">
                        <a class="h-user_menu_link" href="https://${properties['admin.application.host']}/"><spring:message code="administration" text="default text"> </spring:message></a>
                    </li>
                    </c:if>
                    <li class="h-user_menu_i">
                        <a class="h-user_menu_link" href="https://${properties['application.host']}/account/settings"><spring:message code="profile.settings" text="default text"> </spring:message></a>
                    </li>
                    <li class="h-user_menu_i">
                        <a class="h-user_menu_link fic-mistake-a" href="javascript:void(0)"><spring:message code="footer-new.jsp.propertie.1" text="default text"> </spring:message></a>
                    </li>
                    <li class="h-user_menu_i">
                        <a class="h-user_menu_link" href="javascript:void(0)" id="header-exit"><spring:message code="logout" text="default text"> </spring:message></a>
                    </li>
                </ul>
            </div>
        </div>


    </div>

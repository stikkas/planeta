<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<c:if test="${not interactiveCampaignZone}">
<div class="h-no-user">
        <a href="javascript:void(0)" class="signup-link h-no-user_link js-signup-link"><spring:message code="header.signin" text="default text"> </spring:message></a>
    <c:if test="${param['no-counters'] != true && header['User-Agent'] != 'planeta-snapshot-bot'}">
        <div class="signup">
            <div class="signup-popup span7">
                <div class="caret"></div>
            </div>
        </div>

        <div class="registration">
            <div class="signup-popup span7">
                <div class="caret"></div>
            </div>
        </div>
    </c:if>
</div>
</c:if>
<%@ include file="header-info-auth-forms.jsp" %>


<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ include file="/WEB-INF/jsp/includes/generated/doctype-header.jsp" %>
<head>
<meta charset="UTF-8">
<title>Planeta</title>
<script type="text/javascript">
    function authorizeThroughCAS() {
        var loginForm = document.forms[0];
        loginForm.submit();
    }
</script>
</head>
<body onload="authorizeThroughCAS()">
    <noscript>
        <h1>Для корректной работы сайта необходимо включить в настройках браузера Javascript.</h1>
    </noscript>
    <form action="https://${properties['cas.app.host']}/login" method="post" id="login-form" style="display: none;">
        <div class="clearfix">

            <input class="span4" type="text" name="username" placeholder='Ваш e-mail' value="${userInfo.username}"/>
        </div>
        <div class="clearfix">
            <input class="span4" type="password" name="password" placeholder="Ваш пароль" value="${userInfo.password}"/>
        </div>
        <div class="clearfix">
            <input id="remember_me" name="rememberMe" type="checkbox" class="checkbox" value="true" checked="1"/>
        </div>
        <c:if test="${not empty successUrl}">
        <div class="clearfix">
            <input id="successUrl" name="successUrl" type="text" value="${successUrl}"/>
        </div>
        </c:if>
        <c:if test="${not empty auditedAdminId && (auditedAdminId > 0)}">
        <div class="clearfix">
            <input id="auditedAdminId" name="auditedAdminId" type="text" value="${auditedAdminId}"/>
        </div>
        </c:if>

        <div class="clearfix submit">
            <input type="hidden" name="service" value="https://${host}/login/cas" />
            <input class="btn primary" type="submit" value="Войти"/>
        </div>
    </form>
</body>
</html>

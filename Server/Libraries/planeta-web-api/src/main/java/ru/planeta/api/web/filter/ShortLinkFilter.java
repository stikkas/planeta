package ru.planeta.api.web.filter;

import org.apache.commons.lang3.math.NumberUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.filter.OncePerRequestFilter;
import ru.planeta.commons.model.OrderShortLinkStat;
import ru.planeta.commons.web.CookieUtils;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class ShortLinkFilter extends OncePerRequestFilter {
    private static final Logger logger = Logger.getLogger(ShortLinkFilter.class);

    @Value("${cookie.domain}")
    private String cookieDomain;


    @Override
    protected void doFilterInternal(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, FilterChain filterChain) throws ServletException, IOException {
        try {
            long shortLinkId = NumberUtils.toLong(httpServletRequest.getParameter(OrderShortLinkStat.SHORTLINK_ID_COOKIE_NAME), 0);
            if (shortLinkId > 0) {
                CookieUtils.setCookie(httpServletResponse,
                        OrderShortLinkStat.SHORTLINK_ID_COOKIE_NAME,
                        String.valueOf(shortLinkId),
                        cookieDomain);
                CookieUtils.setCookie(httpServletResponse,
                        OrderShortLinkStat.SHORTLINK_REFERRER_COOKIE_NAME,
                        httpServletRequest.getParameter(OrderShortLinkStat.SHORTLINK_REFERRER_COOKIE_NAME),
                        cookieDomain);
            }
        } catch (Exception ex) {
            logger.error("ERROR during setting SHORTLINK_ID_COOKIE_NAME or SHORTLINK_REFERRER_COOKIE_NAME", ex);
        }
        filterChain.doFilter(httpServletRequest, httpServletResponse);
    }
}

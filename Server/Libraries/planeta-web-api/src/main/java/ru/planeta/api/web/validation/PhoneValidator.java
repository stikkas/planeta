package ru.planeta.api.web.validation;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import ru.planeta.api.utils.ValidateUtils;
import ru.planeta.model.profile.Phone;

/**
 * User: michail
 * Date: 14.06.16
 * Time: 13:06
 */
@Component
public class PhoneValidator implements Validator {
    @Override
    public boolean supports(Class<?> clazz) {
        return Phone.class.isAssignableFrom(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "phoneNumber", "wrong.userNotificationSettings.notificationPhone");
        ValidateUtils.rejectIfNotPhoneExt(errors, "phoneNumber", "wrong.userNotificationSettings.notificationPhone");
    }
}

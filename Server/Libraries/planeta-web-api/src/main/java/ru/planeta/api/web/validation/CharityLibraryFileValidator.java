package ru.planeta.api.web.validation;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import ru.planeta.api.utils.ValidateUtils;
import ru.planeta.model.charity.LibraryFile;

@Component
public class CharityLibraryFileValidator implements Validator {

    @Override
    public boolean supports(Class<?> aClass)  {
        return aClass.isAssignableFrom(LibraryFile.class);
    }

    @Override
    public void validate(Object o, Errors errors) {
        ValidateUtils.rejectIfContainsHtmlTags(errors, "url", "contains.wrong.strings");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "url", "field.required");

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "header", "field.required");

        LibraryFile libraryFile = (LibraryFile) o;

        if (libraryFile.getThemeId() == null || libraryFile.getThemeId() < 1) {
            errors.rejectValue("themeId", "field.required");
        }
    }
}

package ru.planeta.api.web.validation;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import ru.planeta.api.utils.ValidateUtils;
import ru.planeta.model.trashcan.SchoolOnlineCourseApplication;

@Component
public class SchoolOnlineCourseApplicationValidator implements Validator {
    private static final Logger log = Logger.getLogger(SchoolOnlineCourseApplicationValidator.class);

    @Override
    public boolean supports(Class<?> aClass) {
        return aClass.isAssignableFrom(SchoolOnlineCourseApplication.class);
    }

    @Override
    public void validate(Object o, Errors errors) {
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "name", "field.required");
        ValidateUtils.rejectIfContainsNotValidString(errors, "name", "wrong.chars");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "email", "field.required");
        ValidateUtils.rejectIfNotEmail(errors, "email", "wrong.email");

        SchoolOnlineCourseApplication onlineCourseApplication = (SchoolOnlineCourseApplication) o;
        log.info("SchoolOnlineCourseApplication email: " + onlineCourseApplication.getEmail());
        log.info("SchoolOnlineCourseApplication has text: " + StringUtils.hasText(onlineCourseApplication.getEmail()));
    }
}

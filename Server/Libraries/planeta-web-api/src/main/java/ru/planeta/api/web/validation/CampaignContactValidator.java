package ru.planeta.api.web.validation;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import ru.planeta.api.utils.ValidateUtils;
import ru.planeta.model.common.campaign.CampaignContact;

/**
 * @author Andrew.Arefyev@gmail.com
 *         14.02.14 22:03
 */
@Component
public class CampaignContactValidator implements Validator {

    @Override
    public boolean supports(Class<?> type) {
        return type.isAssignableFrom(CampaignContact.class);
    }

    @Override
    public void validate(Object o, Errors errors) {
        ValidationUtils.rejectIfEmpty(errors, "campaignId", "field.required");
        ValidateUtils.rejectIfContainsNotValidString(errors, "contactName", "field.required");
        CampaignContact contact = (CampaignContact) o;
        if (contact.getCampaignId() == 0) {
            errors.rejectValue("campaignId", "field.required");
        }
        ValidationUtils.rejectIfEmpty(errors, "email", "field.required");
        ValidateUtils.rejectIfNotEmail(errors, "email","wrong.email");
    }
}

package ru.planeta.api.web.controllers;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;
import ru.planeta.api.aspect.logging.DBLoggerTypeInfoHolder;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by eshevchenko.
 */
public class LoggableRequestHoldInterceptor extends HandlerInterceptorAdapter {

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        DBLoggerTypeInfoHolder.setLoggableRequestInfo(request, response);
        return super.preHandle(request, response, handler);
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
        DBLoggerTypeInfoHolder.removeLoggableRequestInfo();
    }
}

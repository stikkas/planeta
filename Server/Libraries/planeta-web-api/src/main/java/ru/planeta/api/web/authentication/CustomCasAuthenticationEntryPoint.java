package ru.planeta.api.web.authentication;

import org.apache.log4j.Logger;
import org.springframework.security.cas.web.CasAuthenticationEntryPoint;
import org.springframework.util.StringUtils;
import ru.planeta.api.web.utils.SessionUtils;
import ru.planeta.commons.web.WebUtils;

import javax.servlet.http.HttpServletRequest;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

/**
 * This class delegates it's functionality to CasAuthenticationEntryPoint.
 * It constructs failure url(from current request) for CAS to redirect in case of failure.
 *
 * User: m.shulepov
 * Date: 17.04.12
 * Time: 12:09
 */
public class CustomCasAuthenticationEntryPoint extends CasAuthenticationEntryPoint {

    private static final Logger logger = Logger.getLogger(CustomCasAuthenticationEntryPoint.class);

    @Override
    protected String createRedirectUrl(String serviceUrl) {
        String redirectUrl = super.createRedirectUrl(serviceUrl);
        HttpServletRequest request = AuthUtils.INSTANCE.getRequest();

        if (request.getAttribute(AuthUtils.CUSTOM_FAILURE_URL) == null) {
            return redirectUrl;
        }

        StringBuilder failureUrlBuilder = new StringBuilder();
        failureUrlBuilder.append(request.getRequestURL().toString());
        String queryString = request.getQueryString();
        if (queryString != null) {
            failureUrlBuilder.append("?").append(queryString);
        }
        if (StringUtils.hasText(failureUrlBuilder.toString())) {
            try {
                redirectUrl += "&failureUrl=" + URLEncoder.encode(WebUtils.appendProtocolIfNecessary(failureUrlBuilder.toString(), true), "UTF-8");
                SessionUtils.INSTANCE.addToRedirects(request.getRequestURI());
            } catch (UnsupportedEncodingException e) {
                logger.error("Couldn't construct failure url", e);
            }
        }
        return redirectUrl;
    }

}

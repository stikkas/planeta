package ru.planeta.api.web.validation;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import ru.planeta.api.utils.ValidateUtils;
import ru.planeta.model.common.school.Seminar;
import ru.planeta.model.enums.SeminarType;

@Component
public class SeminarValidator implements Validator{
    @Autowired
    private MessageSource messageSource;
    @Override
    public boolean supports(Class<?> aClass) {
        return aClass.isAssignableFrom(Seminar.class);
    }

    @Override
    public void validate(Object o, Errors errors) {
        ValidateUtils.rejectIfContainsHtmlTags(errors, "name", "contains.wrong.strings");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "name", "field.required.seminar.name");

		Seminar seminar = (Seminar) o;

		if (seminar.getTimeStart() == null) {
			errors.rejectValue("timeStart", "field.required");
		}

        if (seminar.getTimeRegistrationEnd() == null) {
            errors.rejectValue("timeRegistrationEnd", "field.required");
        }

        if(seminar.getSeminarType() == SeminarType.EXTERNAL) {
            ValidateUtils.rejectIfNotUrl(errors, "url", "wrong.url");
            ValidateUtils.rejectIfSizeIsTooLarge(errors, "url", 512, messageSource);            
        } else {        
            if (seminar.getSpeakersIds() == null || seminar.getSpeakersIds().length == 0) {
                errors.rejectValue("speakersIds", "field.required.seminar.speakers.ids");                
            }
            if (seminar.getPartnersIds() == null || seminar.getPartnersIds().length == 0) {
                errors.rejectValue("partnersIds", "field.required.seminar.partners.ids");                
            }
        
            ValidationUtils.rejectIfEmptyOrWhitespace(errors, "backgroundImage", "field.required.seminar.background.image");
            ValidateUtils.rejectIfNotUrl(errors, "backgroundImage", "wrong.chars");
    
            if (StringUtils.isEmpty(seminar.getShortDescription())) {
                errors.rejectValue("shortDescription", "field.required");                
            }
    
            if (StringUtils.isEmpty(seminar.getDescription())) {
                errors.rejectValue("description", "field.required");                
            }
    
            if (StringUtils.isEmpty(seminar.getConditions())) {
                errors.rejectValue("conditions", "field.required");                
            }

            if (StringUtils.isEmpty(seminar.getResults())) {
                errors.rejectValue("results", "field.required");                
            }
        }
    }
}

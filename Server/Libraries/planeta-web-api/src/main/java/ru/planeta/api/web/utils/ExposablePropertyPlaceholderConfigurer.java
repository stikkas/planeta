package ru.planeta.api.web.utils;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.config.PropertyPlaceholderConfigurer;

import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 * Created with IntelliJ IDEA.
 * User: a.volkov
 * Date: 30.11.12
 * Time: 14:09
 */
public class ExposablePropertyPlaceholderConfigurer extends PropertyPlaceholderConfigurer {
    private Map<String, String> properties;

    public ExposablePropertyPlaceholderConfigurer() {
        properties = new HashMap<String, String>(256);
    }

    @Override
    protected void processProperties(ConfigurableListableBeanFactory beanFactoryToProcess, Properties props) throws BeansException {
        super.processProperties(beanFactoryToProcess, props);
        Enumeration names = props.propertyNames();
        while (names.hasMoreElements()) {
            String name = (String) names.nextElement();
            String value = props.getProperty(name);
            properties.put(name, value);
        }
    }

    public String getProperty(String name) {
        return properties.get(name);
    }
}

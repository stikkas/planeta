package ru.planeta.api.web.validation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import ru.planeta.api.exceptions.MessageCode;
import ru.planeta.api.model.UserCredentialsInfo;
import ru.planeta.api.service.profile.AuthorizationService;
import ru.planeta.api.utils.ValidateUtils;
import ru.planeta.model.common.UserPrivateInfo;

/**
 * Date: 17.09.12
 *
 * @author s.kalmykov
 */
@Component
public class UserCredentialsInfoValidator implements Validator {

    @Autowired
    private AuthorizationService authorizationService;

    @Override
    public boolean supports(Class<?> clazz) {
        return UserCredentialsInfo.class.equals(clazz);
    }

    @Override
    public void validate(Object o, Errors errors) {
        UserCredentialsInfo info = (UserCredentialsInfo) o;
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "email", "field.required");
        ValidateUtils.rejectIfNotEmail(errors, "email", "wrong.userNotificationSettings.notificationEmail");
        ValidateUtils.rejectIfNotUserName(errors, "firstName", "wrong.user.firstname");
        ValidateUtils.rejectIfNotUserName(errors, "lastName", "wrong.user.lastname");
        ValidateUtils.rejectIfNotUserName(errors, "nickName", "wrong.user.nickname");
        if (!errors.hasErrors()) {
            UserPrivateInfo userPrivateInfo = authorizationService.getUserPrivateInfoByUsername(info.getEmail());
            // user thinks he has email login, must add password
            if (info.hasEmail()) {
                ValidationUtils.rejectIfEmptyOrWhitespace(errors, "emailPassword", "field.required");
                if (userPrivateInfo == null) {
                    errors.rejectValue("emailPassword", "wrong.userNotificationSettings.currentPassword", "User not found");
                } else if (!authorizationService.checkPassword(userPrivateInfo, info.getEmailPassword())) {
                    errors.rejectValue("emailPassword", "wrong.userNotificationSettings.currentPassword");
                }
                // otherwise return success and redirects to confirmationSuccess
            } else // user thinks he has no email login, ignore password
            // if email already assigned
            if (userPrivateInfo != null) {
//                    String uuid =  profileSettingsService.confirmRegistrationEmail(info);
                errors.rejectValue("email", MessageCode.REGISTRATION_CURRENT_EMAIL_ALREADY_EXISTS.getErrorPropertyName());
            } // otherwise return success and submit to registerByOAuth
        }
    }
}

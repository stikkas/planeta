package ru.planeta.api.web.validation;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import ru.planeta.api.model.FeedbackMessage;
import ru.planeta.api.utils.ValidateUtils;

@Component
public class FeedbackMessageValidator implements Validator{

    public boolean supports(Class<?> type) {
        return type.isAssignableFrom(FeedbackMessage.class);
    }

    public void validate(Object o, Errors errors) {
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "email", "field.required");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "message", "field.required");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "theme", "field.required");

        ValidateUtils.rejectIfNotEmail(errors, "email", "wrong.email");
    }
}

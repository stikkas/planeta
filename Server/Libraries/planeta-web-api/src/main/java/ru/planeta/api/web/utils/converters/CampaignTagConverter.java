package ru.planeta.api.web.utils.converters;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import ru.planeta.api.service.campaign.CampaignService;
import ru.planeta.model.common.campaign.CampaignTag;

/**
 * @author Andrew.Arefyev@gmail.com
 *         06.02.14 13:44
 */
public class CampaignTagConverter implements  Converter<String, CampaignTag> {

    @Autowired
    private CampaignService campaignService;

    @Override
    public CampaignTag convert(String s) {
        return campaignService.getCampaignTagByMnemonic(s);
    }
}

package ru.planeta.api.web.validation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import ru.planeta.api.utils.ValidateUtils;
import ru.planeta.model.profile.media.PhotoAlbum;

/**
 *
 * @author ameshkov
 */
@Component
public class PhotoAlbumValidator implements Validator {

    @Autowired
    private MessageSource messageSource;

    @Override
    public boolean supports(Class<?> type) {
        return type.isAssignableFrom(PhotoAlbum.class);
    }

    @Override
    public void validate(Object o, Errors errors) {
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "title", "field.required");
        ValidateUtils.rejectIfContainsHtmlTags(errors, "title", "contains.wrong.strings");
        ValidateUtils.rejectIfContainsNotValidString(errors, "title", "contains.wrong.strings");
        ValidateUtils.rejectIfSizeIsTooLarge(errors, "title", 512, messageSource);
    }
}

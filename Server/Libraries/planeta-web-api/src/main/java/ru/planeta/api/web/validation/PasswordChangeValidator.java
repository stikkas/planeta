package ru.planeta.api.web.validation;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import ru.planeta.api.model.PasswordChange;
import ru.planeta.api.service.profile.AuthorizationService;
import ru.planeta.api.utils.ValidateUtils;
import ru.planeta.dao.commondb.UserPrivateInfoDAO;
import ru.planeta.model.common.UserPrivateInfo;

/**
 *
 * Class PasswordChangeValidator
 * @author a.tropnikov
 */
@Component
public class PasswordChangeValidator implements Validator {

    @Autowired
    private UserPrivateInfoDAO userPrivateInfoDAO;
    @Autowired
    private AuthorizationService authorizationService;

    public boolean supports(Class<?> clazz) {
        return clazz.isAssignableFrom(PasswordChange.class);
    }

    public void validate(Object target, Errors errors) {
        PasswordChange change = (PasswordChange) target;
        UserPrivateInfo info = userPrivateInfoDAO.selectByUserId(change.getProfileId());
        if (info != null) {
            ValidationUtils.rejectIfEmptyOrWhitespace(errors, "currentPassword", "field.required");
            if (!authorizationService.checkPassword(info, change.getCurrentPassword())) {
                errors.rejectValue("currentPassword", "wrong.passwordChange.currentPassword");
                return;
            }
            if (StringUtils.isNotBlank(change.getCurrentPassword())) {
                ValidationUtils.rejectIfEmptyOrWhitespace(errors, "password", "field.required");
                ValidateUtils.rejectIfNotPassword(errors, "password", "wrong.chars");
                ValidateUtils.rejectIfSizeIsTooSmall(errors, "password", "passwordRecovery.length.6", 6, false);
                if (StringUtils.isNotEmpty(change.getPassword()) && !change.getPassword().equals(change.getConfirmPassword())) {
                    errors.rejectValue("confirmPassword", "mismatch.passwordChange.confirmPassword");
                }
            }
        }
    }
}

package ru.planeta.api.web.utils;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.collections4.IterableUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.joda.time.Days;
import org.joda.time.Period;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.springframework.context.ApplicationContext;
import org.springframework.context.MessageSource;
import org.springframework.web.context.ContextLoader;
import org.springframework.web.context.WebApplicationContext;
import ru.planeta.api.Utils;
import ru.planeta.api.model.enums.image.ImageType;
import ru.planeta.api.service.configurations.StaticNodesService;
import ru.planeta.api.service.content.BroadcastService;
import ru.planeta.api.utils.CASErrorUtils;
import ru.planeta.api.utils.OrderUtils;
import ru.planeta.model.common.OrderObjectInfo;
import ru.planeta.model.common.campaign.Share;
import ru.planeta.model.enums.ThumbnailType;

import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.net.URLEncoder;
import java.text.DateFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Class JstlHelperFunctions
 *
 * @author a.tropnikov
 */
public class JstlHelperFunctions {

    private static final Logger LOGGER = Logger.getLogger(JstlHelperFunctions.class);

    private static MessageSource messageSource;
    private static Properties properties;

    private static String getMessage(String messageCode, String defaultMessage) {
        if (messageSource == null) {
            messageSource = getWebAppContext().getBean(MessageSource.class);
        }

        return messageSource.getMessage(messageCode, null, defaultMessage, Locale.getDefault());
    }

    public static String getMessage(String messageCode) {
        return getMessage(messageCode, "");
    }

    private static String[] getSplittedMessages(String messageCode, String separator) {
        return getMessage(messageCode).split(separator);
    }

    // Dates
    public static Date getDateByTime(long time) {
        return new Date(time);
    }

    public static String dateFormat(Date time) {
        Calendar now = Calendar.getInstance();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(time);

        final String result;

        if (now.get(Calendar.YEAR) != calendar.get(Calendar.YEAR)) {
            result = dateFormatByTemplate(time, "dd.MM.yyyy HH:mm");
        } else {
            final int dayDiff = now.get(Calendar.DAY_OF_YEAR) - calendar.get(Calendar.DAY_OF_YEAR);
            switch (dayDiff) {
                case 0:
                    result = getMessage("days.today") + ", " + dateFormatByTemplate(time, "HH:mm");
                    break;
                case 1:
                    result = getMessage("days.yesterday") + ", " + dateFormatByTemplate(time, "HH:mm");
                    break;
                case -1:
                    result = getMessage("days.tomorrow") + ", " + dateFormatByTemplate(time, "HH:mm");
                    break;
                default:
                    result = dateFormatByTemplate(time, "dd.MM HH:mm");
            }
        }
        return result;
    }

    public static String dateFormatByTemplate(Date date, String formatTemplate) {
        String result = "";
        DateFormatSymbols myDateFormatSymbols = new DateFormatSymbols() {

            @Override
            public String[] getMonths() {
                return getMessage("list.months").split("\\|");
            }

            @Override
            public String[] getShortMonths() {
                return getMessage("list.months.short").split("\\|");
            }
        };
        try {
            return (date == null) || (formatTemplate == null) ? result : new SimpleDateFormat(formatTemplate, myDateFormatSymbols).format(date);
        } catch (Exception e) {
            LOGGER.error("Error calling dateFormat function", e);
            return result;
        }
    }

    public static String dateFormatByTemplateLocativeCase(Date date, String formatTemplate) {
        String result = "";
        DateFormatSymbols myDateFormatSymbols = new DateFormatSymbols() {

            @Override
            public String[] getMonths() {
                return getMessage("list.months.locative.case").split("\\|");
            }
        };
        try {
            return (date == null) || (formatTemplate == null) ? result : new SimpleDateFormat(formatTemplate, myDateFormatSymbols).format(date);
        } catch (Exception e) {
            Logger.getLogger(JstlHelperFunctions.class).error("Error calling dateFormat function", e);
            return result;
        }
    }

    public static String formatDateInMilliseconds(Long time, String formatTemplate) {
        return (time == null) || (formatTemplate == null) ? "" : dateFormatByTemplate(new Date(time), formatTemplate);
    }

    /**
     * Calculate difference in days between two dates
     */
    public static int getDateDifferenceInDays(Date start, Date end) {
        final long day = 1000 * 60 * 60 * 24;
        return getIntervalsCountToDate(start, end, day);
    }

    /**
     * @param dateFrom date from what measured
     * @param dateTo   date to measured
     * @param interval cut period
     * @return intervals counted
     */
    private static int getIntervalsCountToDate(Date dateFrom, Date dateTo, long interval) {
        if (dateFrom.compareTo(dateTo) >= 0) {
            return 0;
        }
        long startTime = dateFrom.getTime();
        long endTime = dateTo.getTime();

        startTime -= startTime % interval;
        endTime -= endTime % interval;
        return (int) ((endTime - startTime) / interval);
    }
    // Images

    public static int mathAbs(int val) {
        return Math.abs(val);
    }

    public static String getFileSize(long sizeInBytes) {
        return FileUtils.byteCountToDisplaySize(sizeInBytes);
    }

    public static String getMyListAsString(List list) {
        return StringUtils.join(list, ',');
    }

    public static String getGroupAvatarUrl(String imageUrl, ThumbnailType thumbnailType) {
        StaticNodesService staticNodesService = getWebAppContext().getBean(StaticNodesService.class);
        return staticNodesService.getThumbnailUrl(imageUrl, thumbnailType, ImageType.GROUP);
    }

    public static int random(int range) {
        Random rand = new Random();
        return rand.nextInt(range);
    }

    // Profile
    /**
     * Return profile alias
     */
    public static String getProfileAlias(long profileId, String alias) {
        return StringUtils.isEmpty(alias) ? String.valueOf(profileId) : alias;
    }

    public static String toJson(Object o) throws Exception {
        try {
            ObjectMapper mapper = new ObjectMapper();
            String serialized = mapper.writeValueAsString(o);
            if (serialized != null) {
                // TODO: Dirty, just for now
                serialized = serialized.replace("script>", "sc ript>");
            }
            return serialized;
        } catch (Exception ex) {
            Logger.getLogger(JstlHelperFunctions.class).error("Error calling toJson function", ex);
            throw ex;
        }
    }

    public static Object[] plurals(int n) {
        return new Object[]{n, ru.planeta.commons.lang.StringUtils.calcWordCaseNumber(n)};
    }

    public static Object[] objects(int n) {
        return new Object[]{n, n};
    }

    /**
     * Shrinks string to specified length
     */
    public static String getShrinkedString(String string, int length) {
        if (StringUtils.isBlank(string)) {
            return StringUtils.EMPTY;
        }
        if (length == -1) {
            return string;
        }
        if (length == 0) {
            length = 64;
        }

        if (string.length() > length) {
            string = string.substring(0, length - 1);
            string += "…";
        }

        return string;
    }

    public static String abbreviate(String text, int length) {
        return Utils.INSTANCE.abbreviate(text, length);
    }

    // Central Authentication Service
    public static String getCASMessageForCode(String errorCode) {
        String messageCode = "";
        String defaultMessage = "Bad credentials";

        if (StringUtils.isNotBlank(errorCode)) {
            CASErrorUtils.CASError casError = CASErrorUtils.getCASErrorByErrorCode(errorCode);
            if (casError != null) {
                messageCode = casError.getMessageCode();
            }
        }

        return StringUtils.isEmpty(messageCode) ? defaultMessage : getMessage(messageCode, defaultMessage);
    }

    public static String getDatePeriodAsString(Date start, Date end) {
        if (start == null || end == null || start.after(end)) {
            return null;
        }

        DateTime startDateTime = new DateTime(start.getTime());
        DateTime endDateTime = new DateTime(end.getTime());
        Period period = new Period(startDateTime, endDateTime);

        int daysLeft = Days.daysBetween(startDateTime, endDateTime).getDays();
        int hoursLeft = period.getHours();
        int minutesLeft = period.getMinutes();

        String result = "";
        if (daysLeft > 0) {
            result += daysLeft + " " + ru.planeta.commons.lang.StringUtils.declOfNum(daysLeft, getSplittedMessages("time.cases.day", ",")) + " ";
        }

        if (hoursLeft > 0) {
            result += hoursLeft + " " + ru.planeta.commons.lang.StringUtils.declOfNum(hoursLeft, getSplittedMessages("time.cases.hour", ",")) + " ";
        }

        if (minutesLeft > 0) {
            result += minutesLeft + " " + ru.planeta.commons.lang.StringUtils.declOfNum(minutesLeft, getSplittedMessages("time.cases.minute", ","));
        }

        return result;
    }

    public static String csvEscapeString(String value) {
        value = StringEscapeUtils.escapeCsv(ru.planeta.commons.lang.StringUtils.unescapeHtml(value.replaceAll("[\r\n]+", " ")));
        if (StringUtils.contains(value, ";")) { //fucking MS Excel
            value = value.replaceAll(";", ",");
        }
        return value;
    }

    public static String toLower(String value) {
        return StringUtils.isEmpty(value) ? StringUtils.EMPTY : StringUtils.lowerCase(value);
    }

    // Broadcast specific method
    public static String constructPrivateBroadcastLink(long profileId, String generatedLink) {
        return getWebAppContext().getBean(BroadcastService.class).constructPrivateBroadcastLink(profileId, generatedLink);
    }

    // Private helper methods
    private static WebApplicationContext getWebAppContext() {
        return ContextLoader.getCurrentWebApplicationContext();
    }

    private static ApplicationContext getContext() {
        return ContextLoader.getCurrentWebApplicationContext();
    }

    public static String getShareDisplayName(Share share) {
        return StringUtils.isNotBlank(share.getName()) ? share.getName() : String.format(getMessage("templates.share.price"), share.getPrice());
    }

    public static BigDecimal round(int scale, BigDecimal bigDecimal) {
        return bigDecimal.setScale(scale, BigDecimal.ROUND_DOWN);
    }

    public static synchronized String getPropertyValue(String property) {
        if (properties == null) {
            properties = (Properties) getWebAppContext().getBean("properties");
        }
        String result = properties.getProperty(property);
        return StringUtils.isNotBlank(result) ? result : StringUtils.EMPTY;
    }

    /**
     * Returns host and base URL for static server
     *
     * @param param will be inserted between host and base url
     * @return 
     */
    public static String getStaticBaseUrl(String param) {
        return getPropertyValue("static.host") + param + getPropertyValue("static.base.url");
    }

    /**
     * Resolves what number format need (old <i>orderId-objectId-seqNumber</i> or new <i>orderId-orderObjectId</i>),
     * and generates order number.
     *
     * @return formatted order number;
     */
    public static String generateOrderNumber(OrderObjectInfo object) {
        return String.format("%d-%d", object.getOrderId(), object.getOrderObjectId());
    }

    public static Map<Long, List<OrderObjectInfo>> groupOrderObjectsByObjectId(Collection<OrderObjectInfo> orderObjects) {
        return OrderUtils.groupOrderObjectsByObjectId(orderObjects, null);
    }

    public static String urlEncode(String value, String charset) throws UnsupportedEncodingException {
        return URLEncoder.encode(value, charset);
    }

    public static <E> boolean containsObjectInList(Iterable<E> iterable, Object object) {
        return IterableUtils.contains(iterable, object);
    }

    public static <E extends Enum<E>> boolean containsStringInEnumSet(EnumSet<E> iterable, String object) {
        E first = iterable.iterator().next();
        Enum<E> toFind = E.valueOf(first.getClass(), object);
        return iterable.contains(toFind);
    }

    public static void setProperties(Properties properties) {
        JstlHelperFunctions.properties = properties;
    }

    public static void setMessageSource(MessageSource messageSource) {
        JstlHelperFunctions.messageSource = messageSource;
    }

    public static String generatePlainPostAnnotation(String annotation) {
        return ru.planeta.commons.lang.StringUtils.stripHtmlTags(annotation);
    }

    public static String getTextWithoutTags(String htmlString) {
        if (htmlString == null) {
            return "";
        }
        Document doc = Jsoup.parse(htmlString);
        return doc.body().text();
    }

}

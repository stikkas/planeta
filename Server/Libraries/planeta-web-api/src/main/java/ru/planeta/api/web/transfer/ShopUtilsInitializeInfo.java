package ru.planeta.api.web.transfer;

import ru.planeta.model.shop.Category;
import ru.planeta.model.shop.enums.ProductCategory;

import java.util.List;

/**
 * User: sshendyapin
 * Date: 07.02.13
 * Time: 16:58
 */
public class ShopUtilsInitializeInfo {
    private List<Category> productTags;
    private ProductCategory[] productCategories;

    public List<Category> getProductTags() {
        return productTags;
    }

    public void setProductTags(List<Category> productTags) {
        this.productTags = productTags;
    }

    public ProductCategory[] getProductCategories() {
        return productCategories;
    }

    public void setProductCategories(ProductCategory[] productCategories) {
        this.productCategories = productCategories;
    }

}

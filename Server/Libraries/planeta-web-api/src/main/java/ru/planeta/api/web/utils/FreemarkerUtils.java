package ru.planeta.api.web.utils;

import ru.planeta.api.model.enums.image.ImageType;
import ru.planeta.api.service.configurations.ImageHelperFunctions;
import ru.planeta.model.enums.ThumbnailType;

/**
 * Created with IntelliJ IDEA.
 * User: a.volkov
 * Date: 05.12.12
 * Time: 19:19
 */
public class FreemarkerUtils {

    public static String getThumbnailUrl(String imageUrl, String thumbnailType, String imageType) {
        return ImageHelperFunctions.getThumbnailUrl(imageUrl, ThumbnailType.valueOf(thumbnailType), ImageType.valueOf(imageType));
    }
}

package ru.planeta.api.web.utils;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.stereotype.Component;

/**
 * User: Sergei
 * Date: 27.05.13
 * Time: 11:03
 */
//@Component("jsonpObjectMapper")
public class JsonpObjectMapper extends ObjectMapper {
    public JsonpObjectMapper() {
        super();
        this.configure(JsonGenerator.Feature.ESCAPE_NON_ASCII, true);
        this.configure(DeserializationFeature.FAIL_ON_NULL_FOR_PRIMITIVES, false);
        this.configure(DeserializationFeature.FAIL_ON_NUMBERS_FOR_ENUMS, false);
        this.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    }
}

package ru.planeta.api.web.dto;

import ru.planeta.commons.lang.CollectionUtils;
import ru.planeta.model.common.campaign.Campaign;
import ru.planeta.model.common.campaign.enums.CampaignStatus;
import ru.planeta.model.common.campaign.enums.CampaignTargetStatus;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import ru.planeta.model.common.campaign.CampaignTag;

public class TopCampaignDTO {


    public long getCampaignId() {
        return campaignId;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public String getName() {
        return name;
    }

    public String getNameHtml() {
        return nameHtml;
    }

    public String getShortDescriptionHtml() {
        return shortDescriptionHtml;
    }

    public CampaignStatus getStatus() {
        return status;
    }

    public CampaignTargetStatus getTargetStatus() {
        return targetStatus;
    }

    public BigDecimal getTargetAmount() {
        return targetAmount;
    }

    public BigDecimal getCollectedAmount() {
        return collectedAmount;
    }

    public Date getTimeFinish() {
        return timeFinish;
    }

    public String getWebCampaignAlias() {
        return webCampaignAlias;
    }

    public String getSponsorAlias() {
        return sponsorAlias;
    }

    public BigDecimal getPurchaseSum() {
        return purchaseSum;
    }

    public void setPurchaseSum(BigDecimal purchaseSum) {
        this.purchaseSum = purchaseSum;
    }

    public String getMetaData() {
        return metaData;
    }

    public List<String> getTags() {
        return tags;
    }

    public List<CampaignTag> getTagsFull() {
        return tagsFull;
    }

    public int getProgress() {
        return progress;
    }

    public void setProgress(int progress) {
        this.progress = progress;
    }

    private final long campaignId;
    private final String imageUrl;
    private final String name;
    private final String nameHtml;
    private final String shortDescriptionHtml;
    private final CampaignStatus status;
    private final CampaignTargetStatus targetStatus;
    private final BigDecimal targetAmount;
    private final BigDecimal collectedAmount;
    private final Date timeFinish;
    private final String webCampaignAlias;
    private final String sponsorAlias;
    private final String metaData;
    private final List<String> tags;
    private final List<CampaignTag> tagsFull;
    private BigDecimal purchaseSum = BigDecimal.ZERO;
    private int progress;

    public TopCampaignDTO(Campaign campaign) {
        this.campaignId = campaign.getCampaignId();
        this.webCampaignAlias = campaign.getWebCampaignAlias();
        this.imageUrl = campaign.getImageUrl();
        this.name = campaign.getName();
        this.nameHtml = campaign.getNameHtml(); 
        this.shortDescriptionHtml = campaign.getShortDescriptionHtml() != null? campaign.getShortDescriptionHtml() : campaign.getShortDescription();
        this.status = campaign.getStatus();
        this.targetStatus = campaign.getTargetStatus();
        this.targetAmount = campaign.getTargetAmount();
        this.collectedAmount = campaign.getCollectedAmount();
        this.timeFinish = campaign.getTimeFinish();
        this.sponsorAlias = campaign.getSponsorAlias();
        this.purchaseSum = campaign.getPurchaseSum();
        this.metaData = campaign.getMetaData();
        this.tagsFull = campaign.getTags();
        tags = new ArrayList<>();
        for (CampaignTag tag : campaign.getTags()) {
            tags.add(tag.getMnemonicName());
        }
        this.progress = (int)((double)campaign.getCollectedAmount().longValue() / (double)campaign.getTargetAmount().longValue() * 100);
    }

    public static List<TopCampaignDTO> convert(Collection<Campaign> campaignList){
        return CollectionUtils.map(campaignList, new CollectionUtils.MapFunction<Campaign, TopCampaignDTO>() {
            @Override
            public TopCampaignDTO map(Campaign sourceObject) {
                return new TopCampaignDTO(sourceObject);
            }
        });
    }
}

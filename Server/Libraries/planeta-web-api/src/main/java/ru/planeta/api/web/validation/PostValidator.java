package ru.planeta.api.web.validation;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import ru.planeta.api.utils.ValidateUtils;
import ru.planeta.model.profile.Post;

/**
 * News post validator
 */
@Component
public class PostValidator implements Validator {

    @Autowired
    private MessageSource messageSource;

    @Override
    public boolean supports(Class<?> clazz) {
        return clazz.isAssignableFrom(Post.class);
    }

    @Override
    public void validate(Object target, Errors errors) {
        Post post = (Post) target;
        if (post.getCampaignId() != 0) {
            ValidationUtils.rejectIfEmptyOrWhitespace(errors, "title", "field.required.blogPost.title");
            ValidationUtils.rejectIfEmptyOrWhitespace(errors, "postText", "field.required.blogPost.headingTextHtml");
        } else {
            if (StringUtils.isBlank(post.getPostText()) && StringUtils.isBlank(post.getTitle())) {
                errors.rejectValue("postText", "one.text.field.is.required");
            }
        }
        ValidateUtils.rejectIfContainsHtmlTags(errors, "title", "contains.wrong.strings");
        ValidateUtils.rejectIfSizeIsTooLarge(errors, "title", 256, messageSource);
        // TODO: check postText with length
    }
}

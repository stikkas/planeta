package ru.planeta.api.web.validation;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import ru.planeta.api.exceptions.MessageCode;
import ru.planeta.api.model.json.Registration;
import ru.planeta.api.service.profile.AuthorizationService;
import ru.planeta.api.utils.ValidateUtils;

@Component
public class RegistrationValidator implements Validator {

    private static final Logger log = Logger.getLogger(RegistrationValidator.class);

    private final AuthorizationService authorizationService;

    @Autowired
    public RegistrationValidator(AuthorizationService authorizationService) {
        this.authorizationService = authorizationService;
    }

    public boolean supports(Class<?> type) {
        return Registration.class.equals(type);
    }

    @Override
    public void validate(Object o, Errors errors) {
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "email", "field.required.registration.email");

        ValidateUtils.rejectIfNotEmail(errors, "email", "wrong.email");
        ValidateUtils.rejectIfSizeIsTooSmall(errors, "password", "passwordRecovery.length.6", 6, false);
        ValidateUtils.rejectIfNotPassword(errors, "password", "wrong.chars");
        ValidateUtils.rejectIfNotUserName(errors, "firstName", "wrong.user.firstname");
        ValidateUtils.rejectIfNotUserName(errors, "lastName", "wrong.user.lastname");
        Registration registration = (Registration) o;
        log.info("email: " + registration.getEmail());
        log.info("has text: " + StringUtils.hasText(registration.getEmail()));
        if (authorizationService.isEmailExists(registration.getEmail())) {
            errors.rejectValue("email", MessageCode.REGISTRATION_CURRENT_EMAIL_ALREADY_EXISTS.getErrorPropertyName());
        }
    }
}

package ru.planeta.api.web.tags;

import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.servlet.tags.RequestContextAwareTag;

/**
 * Created with IntelliJ IDEA.
 * User: a.volkov
 * Date: 10.10.13
 * Time: 15:04
 */
public abstract class BaseTag extends RequestContextAwareTag {

    private WebApplicationContext getContext() {
        return getRequestContext().getWebApplicationContext();
    }

    protected <T> T getBean(Class<T> clazz) {
        return getContext().getBean(clazz);
    }

    protected Object getBean(String name) {
        return getContext().getBean(name);
    }
}

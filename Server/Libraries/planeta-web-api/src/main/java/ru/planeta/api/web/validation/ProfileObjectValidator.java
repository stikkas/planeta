package ru.planeta.api.web.validation;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import ru.planeta.model.profile.ProfileObject;

/**
 * Validator for all profile objects
 * 
 * @author ameshkov
 */
@Component
public class ProfileObjectValidator implements Validator {

	@Override
	public boolean supports(Class<?> type) {
		return type.isAssignableFrom(ProfileObject.class);
	}

	@Override
	public void validate(Object o, Errors errors) {
		ProfileObject profileObject = (ProfileObject) o;

		if (profileObject.getProfileId() <= 0) {
			errors.rejectValue("profileId", "field.required");
		}
	}
}

package ru.planeta.api.web.validation;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import ru.planeta.api.model.json.ProfileGeneralSettings;
import ru.planeta.api.service.profile.ProfileService;
import ru.planeta.api.utils.ValidateUtils;
import ru.planeta.model.profile.Profile;

import java.util.Date;

@Component
public class ProfileGeneralSettingsValidator implements Validator {
    @Autowired
    private ProfileService profileService;

    @Autowired
    private MessageSource messageSource;

    @Override
    public boolean supports(Class<?> type) {
        return type.isAssignableFrom(ProfileGeneralSettings.class);
    }

    @Override
    public void validate(Object o, Errors errors) {
        ValidateUtils.rejectIfContainsHtmlTags(errors, "displayName", "contains.wrong.strings");
        ValidateUtils.rejectIfContainsNotValidString(errors, "displayName", "contains.wrong.strings");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "displayName", "field.required");
        ValidateUtils.rejectIfContainsNotValidStringWysiwyg(errors, "summary", "wrong.chars");
        ValidateUtils.rejectIfSizeIsTooLarge(errors, "summary", 5000, messageSource);

        ValidateUtils.rejectIfNotProfileAlias(errors, "alias", "wrong.chars");
        ValidateUtils.rejectIfNotProfileAliasHasLetter(errors, "alias", "field.error.alias");
        ValidateUtils.rejectIfReservedWord(errors, "alias", "field.error.exists");
        ValidateUtils.rejectIfSizeIsTooLarge(errors, "alias", 64, messageSource);
        ValidateUtils.rejectIfSizeIsTooLarge(errors, "phoneNumber", 32, messageSource);
        ValidateUtils.rejectIfSizeIsTooSmall(errors, "alias", "field.length.short.4", 4, true);
        ProfileGeneralSettings profileGeneralSettings = (ProfileGeneralSettings)o;
        if (StringUtils.isNotBlank(profileGeneralSettings.getAlias())) {
            Profile profile = profileService.getProfile(profileGeneralSettings.getProfileId());
            if (profile != null && profile.getAlias() != null) {
                if (!profile.getAlias().equals(profileGeneralSettings.getAlias())) {
                    errors.rejectValue("alias", "alias.already.set");
                }
            } else {
                profile = profileService.getProfile(profileGeneralSettings.getAlias());
                if (profile != null && profile.getProfileId() != profileGeneralSettings.getProfileId()) {
                    errors.rejectValue("alias", "field.error.exists");
                }
            }
        }
        ProfileGeneralSettings ProfileGeneralSettings = (ProfileGeneralSettings) o;
        if (!ValidateUtils.isValidPhoneWithSpecialCharacters(ProfileGeneralSettings.getPhoneNumber())) {
            errors.rejectValue("phoneNumber", "wrong.chars");
        }
        if (ProfileGeneralSettings.getUserBirthDate() != null) {
            Date now = new Date();
            long years = ((now.getTime() - ProfileGeneralSettings.getUserBirthDate())/1000/60/60/24/365);
            if (years < 13) {
                errors.rejectValue("userBirthDate", "wrong.date.userBirthDate");
            }
            if(years > 150){
                errors.rejectValue("userBirthDate", "too.long.date.userBirthDate");
            }
        }
    }
}

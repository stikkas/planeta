package ru.planeta.api.web.utils;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.stereotype.Component;

/**
 * User: Sergei
 * Date: 27.05.13
 * Time: 11:03
 */
//@Component("customObjectMapper")
public class CustomObjectMapper extends ObjectMapper {
    public CustomObjectMapper() {
        super();
        this.configure(DeserializationFeature.FAIL_ON_NULL_FOR_PRIMITIVES, false);
        this.configure(DeserializationFeature.FAIL_ON_NUMBERS_FOR_ENUMS, false);
        this.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    }
}

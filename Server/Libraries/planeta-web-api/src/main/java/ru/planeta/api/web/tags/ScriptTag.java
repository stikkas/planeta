package ru.planeta.api.web.tags;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import ru.planeta.api.helper.ProjectService;
import ru.planeta.commons.web.WebUtils;

import java.util.Properties;

/**
 * User: a.volkov
 * Date: 10.10.13
 * Time: 13:38
 */
@Configurable
@Component
public class ScriptTag extends BaseTag {
    private static final boolean productionCompress = true;
    private static final boolean productionFlushCache = false;

    private static final String COMPRESS_PROPERTY = "static.compress";
    private static final String FLUSH_CACHE_PROPERTY = "static.flushCache";

    private static final Logger log = Logger.getLogger(ScriptTag.class);


    private String src;
    private String lang;

    private String async;


    @Value("${static.compress:true}")
    private boolean compressJs;

    @Value("${static.flushCache:false}")
    private boolean flushCache;

    private ProjectService projectService;

    @Autowired
    public void setProjectService(ProjectService projectService) {
        this.projectService = projectService;
    }

    public String getSrc() {
        return src;
    }

    public void setSrc(String src) {
        this.src = src;
    }

    public String getLang() {
        return lang;
    }

    public void setLang(String lang) {
        this.lang = lang;
    }

    public String getAsync() {
        return async;
    }

    public void setAsync(String async) {
        this.async = async;
    }

    @Override
    protected int doStartTagInternal() throws Exception {

        if (projectService == null) {
            projectService = getBean(ProjectService.class);
            Properties properties = (Properties) getBean("properties");
            compressJs = "true".equals(properties.getProperty(COMPRESS_PROPERTY));
            flushCache = "true".equals(properties.getProperty(FLUSH_CACHE_PROPERTY));
        }


        String langFromRequest = pageContext.getResponse().getLocale().getLanguage();
        if (langFromRequest != null && lang != null) {
            if(!langFromRequest.equals(lang)) {
                log.error("Different lang" + langFromRequest + " " + lang);
            }
        }

        StringBuilder sb = new StringBuilder();
        sb.append("<script type=\"text/javascript\" src=\"");

        if (projectService != null) {
            String baseUrl = projectService.getStaticJsUrl("");
            sb.append(baseUrl);
        }


        WebUtils.Parameters params = new WebUtils.Parameters();
        if (compressJs != productionCompress) {
            params.add("compress", compressJs);
        }

        if (flushCache != productionFlushCache) {
            params.add("flushCache", flushCache);
        }

        if (langFromRequest != null && !langFromRequest.equals("ru")) {
            params.add("lang", langFromRequest);
        }

        sb.append(params.createUrl("/js/" + src, false));
        sb.append("\"");
        if (async != null ) {
            sb.append(" async ");
        }
        sb.append("></script>");
        this.pageContext.getOut().print(sb.toString());
        return EVAL_PAGE;
    }
}

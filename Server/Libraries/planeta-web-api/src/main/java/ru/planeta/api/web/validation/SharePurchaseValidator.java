package ru.planeta.api.web.validation;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import ru.planeta.api.exceptions.NotFoundException;
import ru.planeta.api.service.campaign.CampaignService;
import ru.planeta.api.service.campaign.DeliveryService;
import ru.planeta.api.utils.ValidateUtils;
import ru.planeta.api.web.model.SharePurchase;
import ru.planeta.model.common.campaign.Share;
import ru.planeta.model.common.campaign.enums.ShareStatus;
import ru.planeta.model.common.delivery.LinkedDelivery;
import ru.planeta.model.common.delivery.SubjectType;
import ru.planeta.model.shop.enums.DeliveryType;

/**
 * @author Andrew.Arefyev@gmail.com
 *         30.07.13 18:36
 */
@Component
public class SharePurchaseValidator implements Validator {

    @Autowired
    private CampaignService campaignService;

    @Autowired
    private DeliveryService deliveryService;

    @Autowired
    private AddressValidator addressValidator;

    @Autowired
    private MessageSource messageSource;

    @Override
    public boolean supports(Class<?> clazz) {
        return SharePurchase.class.isAssignableFrom(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        SharePurchase purchase = (SharePurchase) target;

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "shareId", "field.required");
        if (purchase.getShareId() == 0) {
            return;
        }
        Share storedShare = campaignService.getShare(purchase.getShareId());

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "email", "wrong.email");
        ValidateUtils.rejectIfNotEmail(errors, "email", "wrong.email");

        if (storedShare == null) {
            errors.reject("not.found.exception");
            errors.rejectValue("shareId", "not.found.exception");
            return;
        }
        boolean isInvestingShare = (purchase.getShareStatus() == ShareStatus.INVESTING || purchase.getShareStatus() == ShareStatus.INVESTING_WITHOUT_MODERATION || purchase.getShareStatus() == ShareStatus.INVESTING_ALL_ALLOWED);
        SharePurchase.Section section = purchase.getSection();
        if (section == SharePurchase.Section.TAB1 || section == SharePurchase.Section.ALL && !isInvestingShare) {
            if (!StringUtils.isEmpty(storedShare.getQuestionToBuyer())) {
                ValidationUtils.rejectIfEmptyOrWhitespace(errors, "reply", "field.required");
                ValidateUtils.rejectIfSizeIsTooLarge(errors, "reply", 400, messageSource);
                ValidateUtils.rejectIfContainsNotValidString(errors, "reply", "wrong.chars");
            }
        }
        if (section == SharePurchase.Section.TAB2 || section == SharePurchase.Section.ALL) {
            if (purchase.getPaymentMethodId() == 0) {
                errors.rejectValue("paymentMethodId", "payment.required");
            }
            if (purchase.isNeedPhone()) {
                ValidationUtils.rejectIfEmptyOrWhitespace(errors, "paymentPhone", "phone.required");
                if (!ValidateUtils.isValidPhone(purchase.getPaymentPhone())) {
                    errors.rejectValue("paymentPhone", "phone.not.supported");
                } else if (StringUtils.isNotEmpty(purchase.getPaymentPhone()) && '7' != purchase.getPaymentPhone().charAt(0)) {
                    errors.rejectValue("paymentPhone", "phone.not.supported");
                }
            }
            if (!isInvestingShare) {
                if (purchase.getServiceId() == 0) {
                    for (LinkedDelivery ld :deliveryService.getLinkedDeliveries(purchase.getShareId(), SubjectType.SHARE)) {
                        if (ld.isEnabled()) {
                            errors.rejectValue("serviceId", "delivery.required");
                        }
                    }
                    return;
                }
                try {
                    LinkedDelivery linkedDeliveryService = deliveryService.getLinkedDelivery(purchase.getShareId(), purchase.getServiceId(), SubjectType.SHARE);
                    if ((linkedDeliveryService.getServiceType() == DeliveryType.DELIVERY || linkedDeliveryService.getServiceType() == DeliveryType.RUSSIAN_POST)
                            && linkedDeliveryService.isEnabled()) {
                        errors.pushNestedPath("customerContacts");
                        addressValidator.validate(purchase.getAddress(), errors);
                        if ((purchase.getCity() == null) || purchase.getCity().isEmpty()) {
                            ValidationUtils.rejectIfEmptyOrWhitespace(errors, "cityId", "field.required");
                        }
                        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "countryId", "field.required");
                        if (purchase.getAddress().getCountryId() == 0) {
                            errors.rejectValue("countryId", "field.required");
                        }
                        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "phone", "field.required");
                        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "zipCode", "field.required");
                        errors.popNestedPath();
                        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "customerName", "field.required");
                    } else if (linkedDeliveryService.getServiceType() == DeliveryType.CUSTOMER_PICKUP && linkedDeliveryService.isEnabled()) {
                        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "customerName", "field.required");
                        errors.pushNestedPath("customerContacts");
                        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "phone", "field.required");
                        errors.popNestedPath();
                    }
                } catch (NotFoundException e) {
                    errors.rejectValue("serviceId", "delivery.type.not.assigned.to.share");
                }
            }
        }
    }
}

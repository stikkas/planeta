package ru.planeta.api.web.validation;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import ru.planeta.api.model.json.PasswordRecoveryChange;
import ru.planeta.api.utils.ValidateUtils;

@Component
public class PasswordRecoveryChangeValidator implements Validator {

    public boolean supports(Class<?> type) {
        return PasswordRecoveryChange.class.equals(type);
    }

    public void validate(Object o, Errors errors) {

        PasswordRecoveryChange passwordRecoveryChange = (PasswordRecoveryChange) o;

        ValidateUtils.rejectIfSizeIsTooSmall(errors, "password", "passwordRecovery.length.6", 6, false);

        if (!passwordRecoveryChange.getPassword().equals(passwordRecoveryChange.getConfirmation())) {
            errors.rejectValue("confirmation", "passwordRecovery.confirmation.wrong");
        }

    }
}

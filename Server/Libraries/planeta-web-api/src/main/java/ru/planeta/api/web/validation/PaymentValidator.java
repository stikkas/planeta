package ru.planeta.api.web.validation;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import ru.planeta.api.model.Payment;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * Class PaymentValidator
 *
 * @author a.tropnikov
 */
@Component
public class PaymentValidator implements Validator {

    public boolean supports(Class<?> type) {
        return Payment.class.equals(type);
    }

    public void validate(Object o, Errors errors) {
        Payment payment = (Payment) o;

        if (payment.getAmount() == null || payment.getAmount().setScale(2, RoundingMode.HALF_UP).compareTo(BigDecimal.ZERO) <= 0) {
            errors.rejectValue("amount", "wrong.number");
        }
        if (StringUtils.isEmpty(payment.getCurrency())) {
            errors.rejectValue("currency", "payment.currency.required");
        }
    }
}

package ru.planeta.api.web.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import ru.planeta.model.shop.enums.DeliveryStatus;

import java.math.BigDecimal;

/**
 * Created with IntelliJ IDEA.
 * Date: 10.12.2014
 * Time: 19:10
 * To change this template use File | Settings | File Templates.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ChangeShopOrderStatusDTO {
    private long orderId;
    private DeliveryStatus deliveryStatus;
    private String reason = "";
    private boolean sendMessageToUser = false;
    private String trackingCode;
    private BigDecimal cashOnDeliveryCost;
    private String deliveryComment;

    public long getOrderId() {
        return orderId;
    }

    public void setOrderId(long orderId) {
        this.orderId = orderId;
    }

    public DeliveryStatus getDeliveryStatus() {
        return deliveryStatus;
    }

    public void setDeliveryStatus(DeliveryStatus deliveryStatus) {
        this.deliveryStatus = deliveryStatus;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public boolean isSendMessageToUser() {
        return sendMessageToUser;
    }

    public void setSendMessageToUser(boolean sendMessageToUser) {
        this.sendMessageToUser = sendMessageToUser;
    }

    public String getTrackingCode() {
        return trackingCode;
    }

    public void setTrackingCode(String trackingCode) {
        this.trackingCode = trackingCode;
    }

    public BigDecimal getCashOnDeliveryCost() {
        return cashOnDeliveryCost;
    }

    public void setCashOnDeliveryCost(BigDecimal cashOnDeliveryCost) {
        this.cashOnDeliveryCost = cashOnDeliveryCost;
    }

    public String getDeliveryComment() {
        return deliveryComment;
    }

    public void setDeliveryComment(String deliveryComment) {
        this.deliveryComment = deliveryComment;
    }
}

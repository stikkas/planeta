package ru.planeta.api.web.validation;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import ru.planeta.api.utils.ValidateUtils;
import ru.planeta.model.common.Configuration;

/**
 * Configuration property validator
 *
 * @author ds.kolyshev
 * Date: 17.05.12
 */
@Component
public class ConfigurationValidator implements Validator {
	@Override
	public boolean supports(Class<?> type) {
		return type.isAssignableFrom(Configuration.class);
	}

	@Override
	public void validate(Object o, Errors errors) {
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "key", "field.required.configuration.key");
		ValidateUtils.rejectIfContainsNotValidString(errors, "intValue", "contains.wrong.strings");
		ValidateUtils.rejectIfContainsNotValidString(errors, "booleanValue", "contains.wrong.strings");
	}
}

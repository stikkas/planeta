package ru.planeta.api.web.validation;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import ru.planeta.api.utils.ValidateUtils;
import ru.planeta.model.common.DeliveryAddress;

/**
 * Created by denus on 11.01.2016.
 */
@Component
public class DeliveryAddressValidator implements Validator {
    @Override
    public boolean supports(Class<?> aClass) {
        return DeliveryAddress.class.isAssignableFrom(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {

        ValidationUtils.rejectIfEmpty(errors, "fio", "field.required");
        ValidationUtils.rejectIfEmpty(errors, "phone", "field.required");

        ValidateUtils.rejectIfContainsNotValidString(errors, "zipCode", "wrong.chars");

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "city", "field.required");
        ValidateUtils.rejectIfContainsNotValidString(errors, "city", "wrong.chars");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "address", "field.required");
        ValidateUtils.rejectIfContainsNotValidString(errors, "address", "wrong.chars");
    }
}

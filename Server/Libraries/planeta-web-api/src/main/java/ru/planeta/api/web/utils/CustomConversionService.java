package ru.planeta.api.web.utils;

import org.springframework.format.number.NumberFormatAnnotationFormatterFactory;
import org.springframework.format.support.DefaultFormattingConversionService;
import org.springframework.stereotype.Component;
import ru.planeta.api.web.utils.converters.CampaignTagConverter;
import ru.planeta.api.web.utils.converters.DateConverter;

/**
 *
 * Class CustomConversionService
 * @author a.tropnikov
 */
@Component("conversionService")
public class CustomConversionService extends DefaultFormattingConversionService {

    public CustomConversionService() {
        super(false);
        addFormatterForFieldAnnotation(new NumberFormatAnnotationFormatterFactory());
        addConverter(new CampaignTagConverter());
        addConverter(new DateConverter());
    }
}

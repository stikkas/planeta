package ru.planeta.api.web.validation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import ru.planeta.api.service.shop.PromoCodeService;
import ru.planeta.api.utils.ValidateUtils;
import ru.planeta.model.shop.ExtendedPromoCode;
import ru.planeta.model.shop.PromoCode;

/**
 * Created with IntelliJ IDEA.
 * User: michail
 * Date: 08.04.16
 * Time: 12:40
 */
@Component
public class PromoCodeValidator implements Validator {
    private final PromoCodeService promoCodeService;

    @Autowired
    public PromoCodeValidator(PromoCodeService promoCodeService) {
        this.promoCodeService = promoCodeService;
    }

    @Override
    public boolean supports(Class<?> aClass) {
        return aClass.isAssignableFrom(PromoCode.class) || aClass.isAssignableFrom(ExtendedPromoCode.class);
    }

    @Override
    public void validate(Object o, Errors errors) {
        ValidateUtils.rejectIfContainsHtmlTags(errors, "title", "contains.wrong.strings");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "title", "field.required");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "code", "field.required");

        PromoCode promoCode = (PromoCode) o;
        long extId = promoCodeService.getPromoCodeIdByCode(promoCode.getCode(),true);
        if(extId > 0 && extId != promoCode.getPromoCodeId()) {
            errors.rejectValue("code", "promocode.exists");
        }

        if (promoCode.getTimeBegin() == null) {
            errors.rejectValue("timeBegin", "field.required");
        }

        if (promoCode.getTimeEnd() == null ) {
            errors.rejectValue("timeEnd", "field.required");
        }
    }
}

package ru.planeta.api.web.model;


import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.ObjectMapper;
import ru.planeta.model.common.Address;
import ru.planeta.model.common.campaign.ShareDetails;
import ru.planeta.model.shop.enums.DeliveryType;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.math.BigDecimal;

/**
 * @author Andrew.Arefyev@gmail.com
 *         30.07.13 18:33
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class SharePurchase extends ShareDetails {

    public enum Section {
        ALL, TAB1, TAB2, TAB3
    }

    private String reply;
    private DeliveryType deliveryType = DeliveryType.NOT_SET;
    private int quantity = 0;
    private Section section = Section.ALL;
    private BigDecimal donateAmount = BigDecimal.ZERO;
    private String customerName;
    private long serviceId;
    private String email;
    private boolean authorized;
    private long paymentMethodId;
    private boolean needPhone;
    private String paymentPhone;

    public String getPaymentPhone() {
        return paymentPhone;
    }

    public void setPaymentPhone(String paymentPhone) {
        this.paymentPhone = paymentPhone;
    }

    public boolean isNeedPhone() {
        return needPhone;
    }

    public void setNeedPhone(boolean needPhone) {
        this.needPhone = needPhone;
    }

    public boolean isAuthorized() {
        return authorized;
    }
    public void setAuthorized(boolean authorized) {
        this.authorized = authorized;
    }

    public long getPaymentMethodId() {
        return paymentMethodId;
    }

    public void setPaymentMethodId(long paymentMethodId) {
        this.paymentMethodId = paymentMethodId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public BigDecimal getDonateAmount() {
        return donateAmount;
    }

    public void setDonateAmount(BigDecimal donateAmount) {
        this.donateAmount = donateAmount;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public Section getSection() {
        return section;
    }

    public void setSection(Section section) {
        this.section = section;
    }

    public DeliveryType getDeliveryType() {
        return deliveryType;
    }

    public void setDeliveryType(DeliveryType deliveryType) {
        this.deliveryType = deliveryType;
    }

    public String getReply() {
        return reply;
    }

    public void setReply(String reply) {
        this.reply = reply;
    }

    public long getServiceId() {
        return serviceId;
    }

    public void setServiceId(long serviceId) {
        this.serviceId = serviceId;
    }

    @Nonnull
    @Override
    public Address getAddress() {
        return getStoredAddress();
    }

    public Address getCustomerContacts() {
        return getStoredAddress();
    }

    @JsonCreator
    public static SharePurchase create(String json) throws IOException {
        return new ObjectMapper().readValue(json, SharePurchase.class);
    }
}

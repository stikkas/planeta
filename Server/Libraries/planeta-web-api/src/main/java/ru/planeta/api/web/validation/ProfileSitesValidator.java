package ru.planeta.api.web.validation;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import ru.planeta.api.utils.ValidateUtils;
import ru.planeta.model.profiledb.ProfileSites;

import java.util.HashMap;
import java.util.Map;

/**
 * @author ds.kolyshev
 *         Date: 22.09.11
 */
@Component
public class ProfileSitesValidator implements Validator {
    @Autowired
    private MessageSource messageSource;

    private final static Map<String, String> MAP_URLS = new HashMap<String, String>() {{
        put("siteUrl", "http://");
        put("vkUrl", "https://vk.com/");
        put("facebookUrl", "https://facebook.com/");
        put("twitterUrl", "https://twitter.com/");
        put("googleUrl", "https://plus.google.com/+");
    }};

    @Override
    public boolean supports(Class<?> aClass) {
        return ProfileSites.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        for (Map.Entry<String, String> url : MAP_URLS.entrySet()) {
            String urlType = url.getKey();
            String text = url.getValue();
            if (!StringUtils.equalsIgnoreCase((String) errors.getFieldValue(urlType), text)) {
                ValidateUtils.rejectIfNotUrl(errors, urlType, "field.error");
                ValidateUtils.rejectIfSizeIsTooLarge(errors, urlType, 512, messageSource);
                ValidateUtils.rejectIfContainsNotValidString(errors, urlType, "field.error");
            }
        }
    }
}

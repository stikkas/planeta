package ru.planeta.api.web.utils.converters;

import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.core.convert.converter.Converter;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * Class DateConverter
 *
 * @author a.tropnikov
 */
public class DateConverter implements Converter<String, Date> {
    private static final String PATTERN = "yyyy-MM-dd";
    private static final DateTimeFormatter FORMATTER = DateTimeFormat.forPattern(PATTERN);

    private List<Parser<String, Date>> parsers = Arrays.asList(
            new Parser<String, Date>() {
                @Override
                public Date parse(String input) {
                    return new Date(Long.valueOf(input));
                }
            },
            new Parser<String, Date>() {
                @Override
                public Date parse(String input) throws Exception {
                    return FORMATTER.parseLocalDateTime(input).toDate();
                }
            }
    );

    private interface Parser<I, O> {
        O parse(I input) throws Exception;
    }

    @Override
    public Date convert(String s) {
        if (s == null || s.trim().length() == 0 || s.equals("null")) {
            return null;
        }
        Exception lastException = null;
        for (Parser<String, Date> p : parsers) {
            try {
                return p.parse(s);
            } catch (Exception e) {
                lastException = e;
            }
        }
        throw new IllegalArgumentException("can't parse date param for input string " + s, lastException);
    }
}

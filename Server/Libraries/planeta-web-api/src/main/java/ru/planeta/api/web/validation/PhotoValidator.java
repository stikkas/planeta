package ru.planeta.api.web.validation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import ru.planeta.api.utils.ValidateUtils;
import ru.planeta.model.profile.media.Photo;

/**
 *
 * @author ameshkov
 */
@Component
public class PhotoValidator implements Validator {

    @Autowired
    private MessageSource messageSource;

    public boolean supports(Class<?> type) {
        return type.isAssignableFrom(Photo.class);
    }

    public void validate(Object o, Errors errors) {
        ValidateUtils.rejectIfContainsHtmlTags(errors, "description", "contains.wrong.strings");
        ValidateUtils.rejectIfSizeIsTooLarge(errors, "description", 512, messageSource);
        ValidateUtils.rejectIfContainsNotValidString(errors, "description", "contains.wrong.strings");
    }
}

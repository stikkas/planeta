package ru.planeta.api.web.validation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import ru.planeta.api.utils.ValidateUtils;
import ru.planeta.model.profile.media.AudioTrack;

/**
 *
 * @author ameshkov
 */
@Component
public class AudioTrackValidator implements Validator {

	@Autowired
	private MessageSource messageSource;

	public boolean supports(Class<?> type) {
		return type.isAssignableFrom(AudioTrack.class);
	}

	public void validate(Object o, Errors errors) {
		ValidationUtils.rejectIfEmpty(errors, "trackName", "field.required.audioTrack.trackName");
		ValidationUtils.rejectIfEmpty(errors, "artistName", "field.required.audioTrack.artistName");
        
        ValidateUtils.rejectIfContainsHtmlTags(errors, "trackName", "contains.wrong.strings");
        ValidateUtils.rejectIfSizeIsTooLarge(errors, "trackName", 250, messageSource);
        ValidateUtils.rejectIfContainsNotValidString(errors, "trackName", "contains.wrong.strings");
        

        ValidateUtils.rejectIfContainsHtmlTags(errors, "artistName", "contains.wrong.strings");
        ValidateUtils.rejectIfSizeIsTooLarge(errors, "artistName", 250, messageSource);
        ValidateUtils.rejectIfContainsNotValidString(errors, "artistName", "contains.wrong.strings");
        
	}
}

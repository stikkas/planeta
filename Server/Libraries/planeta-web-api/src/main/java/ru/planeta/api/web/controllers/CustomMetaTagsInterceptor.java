package ru.planeta.api.web.controllers;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;
import ru.planeta.api.service.common.CustomMetaTagService;
import ru.planeta.commons.web.WebUtils;
import ru.planeta.model.enums.CustomMetaTagType;
import ru.planeta.model.commondb.CustomMetaTag;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class CustomMetaTagsInterceptor extends HandlerInterceptorAdapter {

    private static final Logger log = Logger.getLogger(CustomMetaTagsInterceptor.class);

    private CustomMetaTagType customMetaTagType;

    public void setProjectType(String projectTypeName) {
        this.customMetaTagType = CustomMetaTagType.valueOf(projectTypeName.toUpperCase());
    }

    @Autowired
    private CustomMetaTagService customMetaTagService;

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
        String uri = WebUtils.getUriWithQuery(request);
        if (uri != null &&
                !StringUtils.contains(uri, ".json")
                && !StringUtils.contains(uri, "/api/util")
                && !StringUtils.contains(uri, "live-check")) {
            if (StringUtils.contains(uri, ".html")) {
                uri = StringUtils.remove(uri, ".html");
            }
            CustomMetaTag customMetaTag = customMetaTagService.selectCustomMetaTag(customMetaTagType, uri);
            if (customMetaTag != null) {
                request.setAttribute("customMetaTag", customMetaTag);
                if (modelAndView != null) {
                    modelAndView.addObject("customMetaTag", customMetaTag);
                }
            }
        }
    }

}

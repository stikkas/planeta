package ru.planeta.api.web.validation;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import ru.planeta.api.utils.ValidateUtils;
import ru.planeta.model.enums.ObjectType;
import ru.planeta.model.profile.Comment;

/**
 * Validator for comment
 *
 * @author a.savanovich
 */
@Component
public class CommentValidator implements Validator {

    @Autowired
    private MessageSource messageSource;

    @Override
    public boolean supports(Class<?> type) {
        return Comment.class.isAssignableFrom(type);
    }

    @Override
    public void validate(Object o, Errors errors) {
        Comment comment = (Comment) o;

        ValidateUtils.rejectIfContainsNotValidString(errors, "textHtml", "wrong.chars");

        if (StringUtils.isEmpty(comment.getText()) && StringUtils.isEmpty(comment.getTextHtml())) {
            errors.rejectValue("text", "field.required");
        }
        if (comment.getObjectType() == null) {
            errors.rejectValue("objectType", "field.required");
        }
        if (comment.getObjectId() <= 0) {
            errors.rejectValue("objectId", "field.required");
        }
        if (comment.getText() != null && comment.getObjectType() != ObjectType.POST && comment.getObjectType() != ObjectType.CAMPAIGN) {
            // Length condition is for everything except blog post
            ValidateUtils.rejectIfSizeIsTooLarge(errors, "text", comment.getText().trim(), 5000, messageSource);
        }
    }
}

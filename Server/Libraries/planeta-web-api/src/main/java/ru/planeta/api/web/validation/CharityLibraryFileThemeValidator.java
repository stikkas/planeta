package ru.planeta.api.web.validation;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import ru.planeta.api.utils.ValidateUtils;
import ru.planeta.model.charity.LibraryFile;
import ru.planeta.model.charitydb.LibraryFileTheme;

@Component
public class CharityLibraryFileThemeValidator implements Validator {

    @Override
    public boolean supports(Class<?> aClass)  {
        return aClass.isAssignableFrom(LibraryFileTheme.class);
    }

    @Override
    public void validate(Object o, Errors errors) {
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "themeHeader", "field.required");
    }
}

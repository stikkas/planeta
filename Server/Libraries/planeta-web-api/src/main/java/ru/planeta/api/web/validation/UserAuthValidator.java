package ru.planeta.api.web.validation;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import ru.planeta.api.model.authentication.AuthenticatedUserDetails;
import ru.planeta.api.service.profile.AuthorizationService;
import ru.planeta.api.web.model.SharePurchase;
import ru.planeta.api.web.utils.SessionUtils;
import ru.planeta.model.enums.EmailStatus;

/**
 * Date: 17.12.2014
 * Time: 18:35
 */
@Component
public class UserAuthValidator implements Validator {

    @Autowired
    private AuthorizationService authorizationService;

    @Override
    public boolean supports(Class<?> clazz) {
        //only for inner call from SharePurchaseValidator
        return false;
    }

    @Override
    public void validate(Object target, Errors errors) {
        SharePurchase purchase = (SharePurchase) target;

        EmailStatus emailStatus = authorizationService.checkEmail(purchase.getEmail());
        if (!purchase.isAuthorized()) {
            if (EmailStatus.ALREADY_EXIST == emailStatus) {
                errors.rejectValue("email", "registration.current.email.already.exists");
            }
        } else {
            SecurityContext securityContext = (SecurityContext) SessionUtils.INSTANCE.getSession().getAttribute("SPRING_SECURITY_CONTEXT");
            if (securityContext == null){
                securityContext = SecurityContextHolder.getContext();
            }
            if (securityContext.getAuthentication().getPrincipal() instanceof String){
                errors.rejectValue("email", "session.expired");
            } else {
                AuthenticatedUserDetails authenticatedUserDetails = (AuthenticatedUserDetails)securityContext.getAuthentication().getPrincipal() ;
                String email = authenticatedUserDetails.getUserAuthorizationInfo().getUserPrivateInfo().getEmail();
                if (StringUtils.isEmpty(email)) {
                    if (EmailStatus.ALREADY_EXIST == emailStatus) {
                        errors.rejectValue("email", "registration.current.email.already.exists");
                    }
                } else if (!email.equalsIgnoreCase(purchase.getEmail())) {
                    errors.rejectValue("email", "wrong.email");
                }
            }
        }
    }
}

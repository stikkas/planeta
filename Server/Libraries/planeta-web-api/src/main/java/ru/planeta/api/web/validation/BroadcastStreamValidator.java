package ru.planeta.api.web.validation;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import ru.planeta.model.profile.broadcast.BroadcastStream;

/**
 * @author ds.kolyshev
 * Date: 03.04.12
 */
@Component
public class BroadcastStreamValidator implements Validator {
    @Override
    public boolean supports(Class<?> aClass) {
        return aClass.isAssignableFrom(BroadcastStream.class);
    }

    @Override
    public void validate(Object o, Errors errors) {

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "name", "field.required.broadcast.stream.name");
//		BroadcastStream stream = (BroadcastStream) o;
//        if (stream.getTimeBegin() == null) {
//			errors.rejectValue("timeBegin", "field.required");
//		}
//
//        if (stream.getTimeEnd() != null && stream.getTimeBegin().after(stream.getTimeEnd())) {
//            errors.rejectValue("timeEnd", "wrong.date.broadcastTimeEnd");
//        }
    }
}

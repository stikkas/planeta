package ru.planeta.api.web.validation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import ru.planeta.api.utils.ValidateUtils;
import ru.planeta.model.profile.media.Video;

/**
 *
 * @author ameshkov
 */
@Component
public class VideoValidator implements Validator {

	@Autowired
	private MessageSource messageSource;

	public boolean supports(Class<?> type) {
		return type.isAssignableFrom(Video.class);
	}

	public void validate(Object o, Errors errors) {
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "name", "field.required.video.name");
		Video video = (Video) o;

		if (video.getVideoId() <= 0) {
			errors.rejectValue("videoId", "field.required");
		}
        
        ValidateUtils.rejectIfContainsHtmlTags(errors, "name", "contains.wrong.strings");
        ValidateUtils.rejectIfSizeIsTooLarge(errors, "name", 250, messageSource);
        ValidateUtils.rejectIfContainsNotValidString(errors, "name", "contains.wrong.strings");
        
        ValidateUtils.rejectIfContainsNotValidString(errors, "description", "contains.wrong.strings");
	}
}

package ru.planeta.api.web.controllers;

import org.apache.commons.lang3.math.NumberUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import ru.planeta.api.web.authentication.AuthUtils;
import ru.planeta.commons.web.WebUtils;
import ru.planeta.dao.profiledb.PostDAO;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PostRedirectInterceptor extends HandlerInterceptorAdapter {
    private static final Logger log = Logger.getLogger(PostRedirectInterceptor.class);

    @Autowired
    private PostDAO postDAO;

    private final static Pattern BLOG_URL_PATTERN = Pattern.compile("[^/]*/([^/]*)/(fanblog|blog|posts)/(\\d+)");


    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        if (AuthUtils.INSTANCE.isAjaxRequest(request)) {
            return true;
        }
        try {

            String uri = request.getRequestURI();
            Matcher matcher = BLOG_URL_PATTERN.matcher(uri);
            if (!matcher.find()) {
                return true;
            }

            String alias = matcher.group(1);
            String postType = matcher.group(2);

            long postId = 0;
            long id = NumberUtils.toLong(matcher.group(3), 0);
            if (id != 0) {
                if (postType.equalsIgnoreCase("blog") || postType.equalsIgnoreCase("fanblog")) {
                    postId = postDAO.getIdByBlogId(id);
                } else if (postType.equalsIgnoreCase("posts")) {
                    postId = postDAO.getIdByPostId(id);
                } else {
                    return true;
                }
            }
            String newUri;

            if (postId == 0) {
                newUri = ServletUriComponentsBuilder
                        .fromRequest(request)
                        .replacePath("/" + alias + "/news")
                        .build().encode().toString();
            } else {
                newUri = ServletUriComponentsBuilder
                        .fromRequest(request)
                        .replacePath("/" + alias + "/news!post" + postId)
                        .build().encode().toString();
            }
            log.info("request uri: " + uri);
            log.info("redirecting user to news: " + newUri);
            WebUtils.setNoCacheResponseHeaders(response);
            response.setStatus(HttpServletResponse.SC_MOVED_PERMANENTLY);
            response.setHeader("Location", newUri);

            return false;
        } catch (Exception e) {
            log.error("fail!", e);
            return true;
        }
    }


}

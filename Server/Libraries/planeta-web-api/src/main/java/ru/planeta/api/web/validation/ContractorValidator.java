package ru.planeta.api.web.validation;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import ru.planeta.api.service.security.PermissionService;
import ru.planeta.api.text.HtmlValidator;
import ru.planeta.api.utils.ValidateUtils;
import ru.planeta.commons.lang.DateUtils;
import ru.planeta.model.common.Contractor;
import ru.planeta.model.enums.ContractorType;
import java.util.Date;

/**
 * User: s.makarov
 * Date: 29.11.13
 * Time: 16:09
 */
@Component
public class ContractorValidator implements Validator {

    @Autowired
    private PermissionService permissionService;

    public boolean supports(Class<?> type) {
        return type.isAssignableFrom(Contractor.class);
    }

    public void validate(Object o, Errors errors) {
        Contractor contractor = (Contractor) o;
        if (StringUtils.isBlank(contractor.getInn())) {
            contractor.setInn(null);
        }

        if (contractor.getCountryId() == 1) {
            if (contractor.getCityId() == 0) {
                errors.rejectValue("cityId", "field.required");
            }
        }

        if (contractor.getCountryId() == 0) {
            contractor.setCityId(0);
        }

        if (contractor.getInn() != null) {
            String trimInn = contractor.getInn().replaceAll("\\s+", "");
            contractor.setInn(trimInn);
        }

        if (contractor.getCountryId() == 1) {
            validateOgrn(errors, contractor);
            validateInn(errors, contractor);
            validatePassportNumber(errors, contractor);
        }

        if (contractor.getType() == ContractorType.INDIVIDUAL
                || contractor.getType() == ContractorType.INDIVIDUAL_ENTREPRENEUR
                || contractor.getType() == ContractorType.OOO
                || contractor.getType() == ContractorType.LEGAL_ENTITY
                || contractor.getType() == ContractorType.CHARITABLE_FOUNDATION) {
            if (contractor.getCountryId() == 1) {
                validateCorrespondingAccount(errors, contractor);
                validateBic(errors, contractor);
                validateCheckingAccount(errors, contractor);
            }
        }

        if (contractor.getType() == ContractorType.INDIVIDUAL) {
            if (contractor.getBirthDate() != null) {
                validateBirthDate(errors, contractor);
            }
        }

        ValidationUtils.rejectIfEmpty(errors, "name", "field.required");

        if (!HtmlValidator.validateHtml(contractor.getName())) {
            errors.rejectValue("name", "wrong.chars");
        }

        if (!ValidateUtils.isItValidForeignContractorInn(contractor.getInn())) {
            errors.rejectValue("inn", "wrong.chars");
        }

        if (!HtmlValidator.validateHtml(contractor.getPhone())) {
            errors.rejectValue("phone", "wrong.chars");
        }
        if (!HtmlValidator.validateHtml(contractor.getScanPassportUrl())) {
            errors.rejectValue("scanPassportUrl", "wrong.chars");
        }
        if (!HtmlValidator.validateHtml(contractor.getInitialsWithLastName())) {
            errors.rejectValue("initialsWithLastName", "wrong.chars");
        }
        if (!HtmlValidator.validateHtml(contractor.getCorrespondingAccount())) {
            errors.rejectValue("correspondingAccount", "wrong.chars");
        }
        if (!HtmlValidator.validateHtml(contractor.getLegalAddress())) {
            errors.rejectValue("legalAddress", "wrong.chars");
        }
        if (!HtmlValidator.validateHtml(contractor.getBic())) {
            errors.rejectValue("bic", "wrong.chars");
        }
        if (!HtmlValidator.validateHtml(contractor.getBeneficiaryBank())) {
            errors.rejectValue("beneficiaryBank", "wrong.chars");
        }
        if (!HtmlValidator.validateHtml(contractor.getCheckingAccount())) {
            errors.rejectValue("checkingAccount", "wrong.chars");
        }
        if (!HtmlValidator.validateHtml(contractor.getUnit())) {
            errors.rejectValue("unit", "wrong.chars");
        }
        if (!HtmlValidator.validateHtml(contractor.getAuthority())) {
            errors.rejectValue("authority", "wrong.chars");
        }
        if (!HtmlValidator.validateHtml(contractor.getActualAddress())) {
            errors.rejectValue("actualAddress", "wrong.chars");
        }
        if (!HtmlValidator.validateHtml(contractor.getOgrn())) {
            errors.rejectValue("ogrn", "wrong.chars");
        }
        if (!HtmlValidator.validateHtml(contractor.getResponsiblePerson())) {
            errors.rejectValue("responsiblePerson", "wrong.chars");
        }
        if (!HtmlValidator.validateHtml(contractor.getKpp())) {
            errors.rejectValue("kpp", "wrong.chars");
        }
    }

    public static void validatePassportNumber(Errors errors, Contractor contractor) {
        if (contractor.getType() != ContractorType.INDIVIDUAL || 
                StringUtils.isEmpty(contractor.getPassportNumber())) {
            return;
        }
        String trimPassportNumber = contractor.getPassportNumber().replaceAll("\\D+", "");
        if (trimPassportNumber.length() != 10) {
            errors.rejectValue("passportNumber", "contractor.passport.error");
        } else {
            contractor.setPassportNumber(formatPassportNumber(trimPassportNumber));
        }
    }

    private static String formatPassportNumber(String x) {
        return x.substring(0, 4) + " " + x.substring(4, x.length());
    }

    private void validateOgrn(Errors errors, Contractor contractor) {
        if (contractor.getType() == ContractorType.INDIVIDUAL) {
            return;
        }

        if (StringUtils.isBlank(contractor.getOgrn())) {
            //errors.rejectValue("ogrn", "field.required");
        } else {
            String trimOgrn = contractor.getOgrn().replaceAll("\\s+", "");
            if ((trimOgrn.length() != 13 && trimOgrn.length() != 15) || !trimOgrn.matches("\\d+")) {
                errors.rejectValue("ogrn", "contractor.ogrn.error");
            } else {
                contractor.setOgrn(trimOgrn);
            }
        }
    }

    private void validateInn(Errors errors, Contractor contractor) {
        if (contractor.getInn() != null) {
            if ((contractor.getInn().length() != 12 && contractor.getInn().length() != 10) || !contractor.getInn().matches("\\d+")) {
                errors.rejectValue("inn", "contractor.inn.error");
            }
        }
    }

    private void validateCheckingAccount(Errors errors, Contractor contractor) {
        if (!StringUtils.isBlank(contractor.getCheckingAccount())) {
            String trimCheckingAccount = contractor.getCheckingAccount().replaceAll("\\s+", "");
            if (trimCheckingAccount.length() != 20 || !trimCheckingAccount.matches("\\d+")) {
                errors.rejectValue("checkingAccount", "contractor.checking.account.error");
            } else {
                contractor.setCheckingAccount(trimCheckingAccount);
            }
        }
    }

    private void validateCorrespondingAccount(Errors errors, Contractor contractor) {
        if (!StringUtils.isBlank(contractor.getCorrespondingAccount())) {
            String trimCorrespondingAccount = contractor.getCorrespondingAccount().replaceAll("\\s+", "");
            if (trimCorrespondingAccount.length() != 20 || !trimCorrespondingAccount.matches("\\d+")) {
                errors.rejectValue("correspondingAccount", "contractor.corresponding.account.error");
            } else {
                contractor.setCorrespondingAccount(trimCorrespondingAccount);
            }
        }
    }

    private void validateBic(Errors errors, Contractor contractor) {
        if (!StringUtils.isBlank(contractor.getBic())) {
            String trimBic = contractor.getBic().replaceAll("\\s+", "");
            if (trimBic.length() != 9 || !trimBic.matches("\\d+")) {
                errors.rejectValue("bic", "contractor.bic.error");
            } else {
                contractor.setBic(trimBic);
            }
        }
    }

    private void validateBirthDate(Errors errors, Contractor contractor) {
        Date now = new Date();
        now = DateUtils.getEndOfDate(now);
        Date contractorAdulthood = org.apache.commons.lang3.time.DateUtils.addYears(DateUtils.getEndOfDate(contractor.getBirthDate()), 18);
        if (contractorAdulthood.after(now)) {
            errors.rejectValue("birthDate", "contractor.birthday");
        }
    }
}

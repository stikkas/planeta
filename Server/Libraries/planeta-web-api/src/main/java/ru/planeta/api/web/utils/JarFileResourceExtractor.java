package ru.planeta.api.web.utils;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.context.ServletContextAware;
import ru.planeta.api.service.campaign.CampaignStatusUtils;
import ru.planeta.api.service.configurations.ImageHelperFunctions;
import ru.planeta.commons.generator.TldGenerator;
import ru.planeta.commons.lang.DateUtils;
import ru.planeta.commons.text.Bbcode;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.security.CodeSource;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

/**
 * Helper class that extracts JSPs from JARs and adds them to web application (JSP_DESTINATION_PATH property points to exact location)
 * Provide full path to files, that need to be extracted.
 * <p/>
 * User: m.shulepov
 * Date: 10.04.12
 * Time: 10:19
 */
public class JarFileResourceExtractor implements ServletContextAware {

    private final Logger log = Logger.getLogger(JarFileResourceExtractor.class);
    private ServletContext servletContext;
    private String destinationPath;
    private String sourcePath;
    private String destinationPathTld;
    @Value("${jar.jsp.source.path:tlds}")
    private String sourcePathTld;

    @Value("${jar.jsp.destination.path:/WEB-INF/jsp/includes/generated}")
    public void setDestinationPath(String destinationPath) {
        this.destinationPath = destinationPath;
    }

    @Value("${jar.jsp.source.path:jsp/includes}")
    public void setSourcePath(String sourcePath) {
        this.sourcePath = sourcePath;
    }

    @Value("${jar.tld.destination.path:/WEB-INF/tlds/}")
    public void setDestinationPathTld(String destinationPathTld) {
        this.destinationPathTld = destinationPathTld;
    }

    @Override
    public void setServletContext(ServletContext servletContext) {
        this.servletContext = servletContext;
    }

    @PostConstruct
    public void extractFiles() throws IOException {
        extractFiles(destinationPath, sourcePath);
        extractTlds(destinationPathTld);
        extractFiles(destinationPathTld, sourcePathTld);
    }

    private void extractFiles(String destinationPath, String sourcePath) throws IOException {
        try {
            if (StringUtils.isEmpty(destinationPath) || StringUtils.isEmpty(sourcePath) || servletContext == null) {
                return;
            }

            File destinationFolder = new File(servletContext.getRealPath(destinationPath));
            if (!destinationFolder.exists()) {
                destinationFolder.mkdirs();
            }

            CodeSource src = this.getClass().getProtectionDomain().getCodeSource();

            if (src != null) {
                URL jar = src.getLocation();

                File path = new File(jar.getPath() + "/" + sourcePath);
                if(path.isDirectory()) {
                    List<File> filesList = new ArrayList<>();
                    File[] filesPath = path.listFiles();

                    assert filesPath != null;
                    for (File file : filesPath) {
                        filesList.add(file);
                    }

                    for (File file : filesList) {
                        File destinationFile = new File(destinationFolder, file.getName());
                        FileUtils.copyFile(file, destinationFile);
                    }
                }else {
                    List<String> files = new ArrayList<String>();
                    ZipInputStream zip = new ZipInputStream(jar.openStream());

                    try {
                        ZipEntry ze;
                        while ((ze = zip.getNextEntry()) != null) {
                            String entryName = ze.getName();
                            if (entryName.startsWith(sourcePath + "/") && !entryName.endsWith("/")) {
                                files.add(entryName);
                            }
                        }
                    } finally {
                        IOUtils.closeQuietly(zip);
                    }

                    for (String file : files) {
                        File destinationFile;
                        if (file.endsWith(".jsp") && file.contains("irma")) {
                            destinationFile = new File(destinationFolder + "/irma", new File(file).getName());
                        } else {
                            destinationFile = new File(destinationFolder, new File(file).getName());
                        }
                        InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream(file);
                        FileUtils.copyInputStreamToFile(inputStream, destinationFile);
                    }
                }
            }

        } catch (IOException ex) {
            log.error("Error extracting resources from jar", ex);
            throw ex;
        }
    }

    private void extractTlds(String destinationPathTld) throws IOException {
        if (servletContext == null) {
            return;
        }
        String generatedString = "";
        generatedString = TldGenerator.generateMethods(JstlHelperFunctions.class,
                DateUtils.class, Bbcode.class,  StringEscapeUtils.class, ImageHelperFunctions.class,
                CampaignStatusUtils.class);

        File destinationFolder = new File(servletContext.getRealPath(destinationPathTld));
        if (!destinationFolder.exists()) {
            destinationFolder.mkdirs();
        }

        FileUtils.writeStringToFile(new File(servletContext.getRealPath(destinationPathTld) + "/helper-functions.tld"), generatedString );
    }
}

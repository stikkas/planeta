package ru.planeta.api.web.validation;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import ru.planeta.api.utils.ValidateUtils;
import ru.planeta.model.common.school.SeminarRegistrationCompany;
import ru.planeta.model.commondb.SeminarRegistration;

@Component
public class SeminarRegistrationCompanyValidator implements Validator{
    @Override
    public boolean supports(Class<?> aClass) {
        return aClass.isAssignableFrom(SeminarRegistrationCompany.class);
    }

    @Override
    public void validate(Object o, Errors errors) {
        ValidateUtils.rejectIfContainsHtmlTags(errors, "fio", "contains.wrong.strings");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "fio", "field.required.seminar.name");

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "email", "field.required");
        ValidateUtils.rejectIfNotEmail(errors, "email", "check.email");

        SeminarRegistration seminarRegistration = (SeminarRegistration) o;

		if (StringUtils.isEmpty(seminarRegistration.getPhone())) {
			errors.rejectValue("phone", "field.required");
		}

        if (StringUtils.isEmpty(seminarRegistration.getOrganizationName())) {
            errors.rejectValue("organizationName", "field.required");
        }

        if (StringUtils.isEmpty(seminarRegistration.getOrganizationLineOfAtivity())) {
            errors.rejectValue("organizationLineOfAtivity", "field.required");
        }

        if (StringUtils.isEmpty(seminarRegistration.getPosition())) {
            errors.rejectValue("position", "field.required");
        }

        if (StringUtils.isEmpty(seminarRegistration.getCity())) {
            errors.rejectValue("city", "field.required");
        }

        if (StringUtils.isEmpty(seminarRegistration.getWorkPhone())) {
            errors.rejectValue("workPhone", "field.required");
        }

        if (seminarRegistration.getOrganizationRegistrationDate() == null) {
            errors.rejectValue("organizationRegistrationDate", "field.required");
        }

        if (StringUtils.isEmpty(seminarRegistration.getOrganizationSite())) {
            errors.rejectValue("organizationSite", "field.required");
        }

        if (seminarRegistration.getSeminarFormat() == null) {
            errors.rejectValue("seminarFormat", "field.required");
        }
    }
}

package ru.planeta.api.web.validation;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import ru.planeta.api.utils.ValidateUtils;
import ru.planeta.model.bibliodb.BiblioPurchase;
import ru.planeta.model.common.InvestingOrderInfo;
import ru.planeta.model.enums.ContractorType;

import java.math.BigDecimal;

@Component
public class BiblioPurchaseValidator implements Validator {

    private static final BigDecimal MAX_AMOUNT = new BigDecimal(1000000);

    @Autowired
    private InvestingOrderInfoValidator investingOrderInfoValidator;

    @Override
    public boolean supports(Class<?> clazz) {
        return BiblioPurchase.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        BiblioPurchase purchase = (BiblioPurchase) target;

        if (StringUtils.isEmpty(purchase.getEmail())) {
            errors.rejectValue("email", "email.required");
        } else {
            ValidateUtils.rejectIfNotEmail(errors, "email", "wrong.email");
        }
        BigDecimal sum = purchase.getTotalSum();
        if (sum == null || sum.compareTo(BigDecimal.ZERO) <= 0 || sum.compareTo(MAX_AMOUNT) > 0) {
            errors.rejectValue("totalSum", "amount.not.in.range");
        }
        Long paymentMethodId = purchase.getPaymentMethodId();
        if (paymentMethodId == null || paymentMethodId.compareTo(0L) <= 0) {
            errors.rejectValue("paymentMethodId", "payment.required");
        }

        InvestingOrderInfo investInfo = purchase.getInvestInfo();
        if (purchase.getCustomerType() != ContractorType.INDIVIDUAL) {
            if (investInfo == null) {
                errors.rejectValue("investInfo", "info.required");
            } else {
                try {
                    errors.pushNestedPath("investInfo");
                    investingOrderInfoValidator.validate(investInfo, errors);
                } finally {
                    errors.popNestedPath();
                }
            }
        } else {
            ValidationUtils.rejectIfEmptyOrWhitespace(errors, "fio", "field.required");
            ValidateUtils.rejectIfContainsNotValidString(errors, "fio", "wrong.chars");
        }
    }

}

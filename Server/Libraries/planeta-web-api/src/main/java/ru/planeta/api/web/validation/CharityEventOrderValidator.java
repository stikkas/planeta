package ru.planeta.api.web.validation;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import ru.planeta.api.utils.ValidateUtils;
import ru.planeta.model.common.charity.CharityEventOrder;

@Component
public class CharityEventOrderValidator implements Validator {

    @Override
    public boolean supports(Class<?> aClass)  {
        return aClass.isAssignableFrom(CharityEventOrder.class);
    }

    @Override
    public void validate(Object o, Errors errors) {
        ValidateUtils.rejectIfContainsHtmlTags(errors, "fio", "contains.wrong.strings");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "fio", "field.required");

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "email", "field.required");
        ValidateUtils.rejectIfNotEmail(errors, "email", "check.email");

        CharityEventOrder charityEventOrder = (CharityEventOrder) o;

        if (StringUtils.isEmpty(charityEventOrder.getPhone())) {
            errors.rejectValue("phone", "field.required");
        }

        if (StringUtils.isEmpty(charityEventOrder.getOrganizationName())) {
            errors.rejectValue("organizationName", "field.required");
        }

        if (StringUtils.isEmpty(charityEventOrder.getEventGoal())) {
            errors.rejectValue("eventGoal", "field.required");
        }

        if (StringUtils.isEmpty(charityEventOrder.getEventResult())) {
            errors.rejectValue("eventResult", "field.required");
        }

        if (StringUtils.isEmpty(charityEventOrder.getEventExpectedTime())) {
            errors.rejectValue("eventExpectedTime", "field.required");
        }

        if (StringUtils.isEmpty(charityEventOrder.getEventAudience())) {
            errors.rejectValue("eventAudience", "field.required");
        }

        if (charityEventOrder.getParticipantsCount() < 1) {
            errors.rejectValue("participantsCount", "field.required");
        }

        if (StringUtils.isEmpty(charityEventOrder.getEventFormat())) {
            errors.rejectValue("eventFormat", "field.required");
        }
    }
}

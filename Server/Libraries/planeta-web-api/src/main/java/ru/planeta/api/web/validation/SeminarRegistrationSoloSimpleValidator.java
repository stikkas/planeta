package ru.planeta.api.web.validation;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import ru.planeta.api.utils.ValidateUtils;
import ru.planeta.model.common.school.SeminarRegistrationSoloSimple;
import ru.planeta.model.commondb.SeminarRegistration;

@Component
public class SeminarRegistrationSoloSimpleValidator implements Validator{
    @Override
    public boolean supports(Class<?> aClass) {
        return aClass.isAssignableFrom(SeminarRegistrationSoloSimple.class);
    }

    @Override
    public void validate(Object o, Errors errors) {
        ValidateUtils.rejectIfContainsHtmlTags(errors, "fio", "contains.wrong.strings");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "fio", "field.required.seminar.name");

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "email", "field.required");
        ValidateUtils.rejectIfNotEmail(errors, "email", "check.email");

		SeminarRegistration seminarRegistration = (SeminarRegistration) o;

		if (StringUtils.isEmpty(seminarRegistration.getPhone())) {
			errors.rejectValue("phone", "field.required");
		}

        if (StringUtils.isEmpty(seminarRegistration.getCity())) {
            errors.rejectValue("city", "field.required");
        }

        if (seminarRegistration.getSeminarFormat() == null) {
            errors.rejectValue("seminarFormat", "field.required");
        }
    }
}

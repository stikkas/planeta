/*
 * Copyright 2007 The JA-SIG Collaborative. All rights reserved. See license
 * distributed with this file and available online at
 * http://www.ja-sig.org/products/cas/overview/license/index.html
 */
package ru.planeta.api.web.authentication;

import java.net.UnknownHostException;
import org.jasig.cas.client.validation.Cas20ProxyTicketValidator;

/**
 * Implementation of the TicketValidator that will validate Service Tickets in compliance with the CAS 4.
 *
 * @author Unknown
 * @version $Revision$ $Date$
 */
public class CustomCas20ServiceTicketValidator extends Cas20ProxyTicketValidator {

    public CustomCas20ServiceTicketValidator(String casServerUrlPrefix, String proxyCallback) throws UnknownHostException {
        super(casServerUrlPrefix);
        setProxyCallbackUrl(proxyCallback);

    }

//    @Override
//    protected String getUrlSuffix() {
//        return "planetaProxyValidate";
//    }

}

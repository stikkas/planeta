package ru.planeta.api.web.dto;

import ru.planeta.model.common.campaign.Campaign;
import ru.planeta.model.common.campaign.enums.CampaignStatus;

/**
 * Created by kostiagn on 11.09.2015.
 */
public class CampaignShortInfo {
    private final long campaignId;
    private final CampaignStatus status;
    private final String webCampaignAlias;
    private final String campaignAlias;
    private final boolean canChangeCampaign;
    private final long creatorProfileId;

    public CampaignShortInfo(Campaign campaign, boolean canChangeCampaign){
        campaignId = campaign.getCampaignId();
        status = campaign.getStatus();
        campaignAlias = campaign.getCampaignAlias();
        webCampaignAlias = campaign.getWebCampaignAlias();
        this.canChangeCampaign = canChangeCampaign;
        creatorProfileId = campaign.getCreatorProfileId();

    }

    public long getCampaignId() {
        return campaignId;
    }

    public CampaignStatus getStatus() {
        return status;
    }

    public String getWebCampaignAlias() {
        return webCampaignAlias;
    }

    public String getCampaignAlias() {
        return campaignAlias;
    }

    public boolean isCanChangeCampaign() {
        return canChangeCampaign;
    }

    public long getCreatorProfileId() {
        return creatorProfileId;
    }
}

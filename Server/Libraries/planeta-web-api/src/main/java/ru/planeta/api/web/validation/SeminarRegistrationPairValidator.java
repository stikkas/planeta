package ru.planeta.api.web.validation;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import ru.planeta.api.utils.ValidateUtils;
import ru.planeta.model.common.school.SeminarRegistrationPair;
import ru.planeta.model.commondb.SeminarRegistration;

@Component
public class SeminarRegistrationPairValidator implements Validator{
    @Override
    public boolean supports(Class<?> aClass) {
        return aClass.isAssignableFrom(SeminarRegistrationPair.class);
    }

    @Override
    public void validate(Object o, Errors errors) {
        ValidateUtils.rejectIfContainsHtmlTags(errors, "fio", "contains.wrong.strings");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "fio", "field.required.seminar.name");

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "email", "field.required");
        ValidateUtils.rejectIfNotEmail(errors, "email", "check.email");

        SeminarRegistration seminarRegistration = (SeminarRegistration) o;

		if (StringUtils.isEmpty(seminarRegistration.getPhone())) {
			errors.rejectValue("phone", "field.required");
		}

        ValidateUtils.rejectIfContainsHtmlTags(errors, "fio2", "contains.wrong.strings");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "fio2", "field.required.seminar.name");

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "email2", "field.required");
        ValidateUtils.rejectIfNotEmail(errors, "email2", "check.email");

        if (StringUtils.isEmpty(seminarRegistration.getPhone())) {
            errors.rejectValue("phone2", "field.required");
        }

        if (StringUtils.isEmpty(seminarRegistration.getCity())) {
            errors.rejectValue("city", "field.required");
        }

        if (seminarRegistration.getSeminarFormat() == null) {
            errors.rejectValue("seminarFormat", "field.required");
        }

        if (StringUtils.isEmpty(seminarRegistration.getCampaignShortDescription())) {
            errors.rejectValue("campaignShortDescription", "field.required");
        }

        if (seminarRegistration.getCampaignTargetSum() == null) {
            errors.rejectValue("campaignTargetSum", "field.required");
        }
    }
}

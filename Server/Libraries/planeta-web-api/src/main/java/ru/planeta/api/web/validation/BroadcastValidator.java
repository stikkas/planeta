package ru.planeta.api.web.validation;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import ru.planeta.api.utils.ValidateUtils;
import ru.planeta.model.profile.broadcast.Broadcast;

/**
 * Broadcast's validator
 *
 * @author ds.kolyshev
 * Date: 27.03.12
 */
@Component
public class BroadcastValidator implements Validator{
    @Override
    public boolean supports(Class<?> aClass) {
        return aClass.isAssignableFrom(Broadcast.class);
    }

    @Override
    public void validate(Object o, Errors errors) {
        ValidateUtils.rejectIfContainsHtmlTags(errors, "name", "contains.wrong.strings");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "name", "field.required.broadcast.name");

		Broadcast broadcast = (Broadcast) o;
		if (broadcast.getTimeBegin() == null) {
			errors.rejectValue("timeBegin", "field.required");
            return;
		}

		if (broadcast.getTimeEnd() == null) {
			errors.rejectValue("timeEnd", "field.required");
		}

		if (broadcast.getTimeEnd() != null && broadcast.getTimeBegin().after(broadcast.getTimeEnd())) {
			errors.rejectValue("timeEnd", "wrong.date.broadcastTimeEnd");
		}
    }
}

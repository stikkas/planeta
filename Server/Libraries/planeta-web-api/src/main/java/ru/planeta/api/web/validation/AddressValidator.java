package ru.planeta.api.web.validation;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import ru.planeta.api.utils.ValidateUtils;
import ru.planeta.model.common.Address;

/**
 * Address validator.<br>
 * User: eshevchenko
 */
@Component
public class AddressValidator implements Validator {

    @Override
    public boolean supports(Class<?> clazz) {
        return Address.class.isAssignableFrom(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        // ValidationUtils.rejectIfEmptyOrWhitespace(errors, "zipCode", "field.required");
        ValidateUtils.rejectIfContainsNotValidString(errors, "zipCode", "wrong.chars");
        Address address = (Address) target;
        if (address.getCityId() == 0) {
            ValidationUtils.rejectIfEmptyOrWhitespace(errors, "city", "field.required");
        }
        ValidateUtils.rejectIfContainsNotValidString(errors, "city", "wrong.chars");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "street", "field.required");
        ValidateUtils.rejectIfContainsNotValidString(errors, "street", "wrong.chars");
/*
  ValidationUtils.rejectIfEmptyOrWhitespace(errors, "phone", "field.required");
  ValidateUtils.rejectIfContainsNotValidString(errors, "phone", "wrong.chars");
*/
    }
}

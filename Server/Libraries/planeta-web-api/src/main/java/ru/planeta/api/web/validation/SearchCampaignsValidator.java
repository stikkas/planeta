package ru.planeta.api.web.validation;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import ru.planeta.api.search.SearchCampaigns;

import org.springframework.validation.Validator;
/**
 *
 * Created by asavan on 14.12.2016.
 */
@Component
public class SearchCampaignsValidator implements Validator {
    @Override
    public boolean supports(Class<?> aClass) {
        return aClass.isAssignableFrom(SearchCampaigns.class);
    }

    @Override
    public void validate(Object o, Errors errors) {
        SearchCampaigns searchCampaigns = (SearchCampaigns) o;
        if (searchCampaigns.getLimit() == 0 || searchCampaigns.getLimit() > 150) {
            errors.rejectValue("limit", "field.error");
        }
    }

}

package ru.planeta.api.web.validation;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import ru.planeta.api.utils.ValidateUtils;
import ru.planeta.model.profile.chat.ChatMessage;

/**
 * Validator for chat message
 *
 * @author ameshkov
 */
@Component
public class ChatMessageValidator implements Validator {
    @Autowired
    private MessageSource messageSource;

    public boolean supports(Class<?> type) {
        return type.isAssignableFrom(ChatMessage.class);
    }

    public void validate(Object o, Errors errors) {
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "messageText", "messageText", "field.required");

        ChatMessage chatMessage = (ChatMessage) o;

        if (chatMessage.getChatId() <= 0) {
            errors.rejectValue("chatId", "field.required");
        }

        String messageText = StringUtils.trim(chatMessage.getMessageText());

        ValidateUtils.rejectIfSizeIsTooLarge(errors, "messageText", messageText, 160, messageSource);

        if (StringUtils.length(messageText) < 4) {
            errors.rejectValue("messageText", "field.length.short.4");
        }
    }
}

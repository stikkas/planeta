package ru.planeta.api.web.validation;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import ru.planeta.api.service.profile.ConfirmationService;
import ru.planeta.api.utils.ValidateUtils;
import ru.planeta.commons.model.Gender;
import ru.planeta.model.common.InvestingOrderInfo;
import ru.planeta.model.enums.ContractorType;
import ru.planeta.model.profile.Phone;

/**
 * Check field for additional information of the order by investing purchasement.
 *
 * User: Serge Blagodatskih<stikkas17@gmail.com><br>
 * Date: 10.02.16<br>
 * Time: 11:40
 */
@Component
public class InvestingOrderInfoValidator implements Validator {
    private final ConfirmationService confirmationService;

    @Autowired
    public InvestingOrderInfoValidator(ConfirmationService confirmationService) {
        this.confirmationService = confirmationService;
    }

    @Override
    public boolean supports(Class<?> clazz) {
        return InvestingOrderInfo.class.isAssignableFrom(clazz);
    }

    @Value("${sms.verification.enabled:true}")
    private boolean smsVerificationEnabled;


    @Override
    public void validate(Object target, Errors errors) {
        InvestingOrderInfo info = (InvestingOrderInfo) target;
        ValidationUtils.rejectIfEmpty(errors, "userType", "field.required");
        ContractorType userType = info.getUserType();
        if (userType == null) {
            return;
        }

        // mandatory fields
        for (String field : InvestingOrderInfo.Companion.getRequiredFields(userType)) {
            ValidationUtils.rejectIfEmptyOrWhitespace(errors, field, "field.required");
            ValidateUtils.rejectIfContainsNotValidString(errors, field, "wrong.chars");
        }

        // optional fields
        ValidateUtils.rejectIfContainsNotValidString(errors, "orgBrand", "wrong.chars");
        ValidateUtils.rejectIfContainsNotValidString(errors, "webSite", "wrong.chars");

        if (userType == ContractorType.INDIVIDUAL && info.getGender() == Gender.NOT_SET) {
            errors.rejectValue("gender", "field.required.gender");
        }
        if (info.getRegCountryId() == 0) {
            errors.rejectValue("regCountryId", "field.required.country");
        }
        if (info.getLiveCountryId() == 0) {
            errors.rejectValue("liveCountryId", "field.required.country");
        }

        if (!errors.hasErrors()) {
            if (StringUtils.isNotBlank(info.getPhoneNumber()) && smsVerificationEnabled) {
                Phone phone = new Phone(info.getPhoneNumber());
                if (StringUtils.isBlank(info.getConfirmationCode())) {
                    if (userType == ContractorType.INDIVIDUAL) {
                        errors.rejectValue("phoneNumber", "wrong.userNotificationSettings.phoneCode");
                        errors.rejectValue("confirmationCode", "field.required");
                    }
                } else {
                    ConfirmationService.Status confirmStatus = confirmationService.confirm(phone.getNormalizedPhone(), info.getConfirmationCode());
                    switch (confirmStatus) {
                        case CONFIRMED:
                            break;
                        case ATTEMPTS_EXCEEDED:
                            errors.rejectValue("confirmationCode", "wrong.userNotificationSettings.phoneCode.attempts");
                            break;
                        default:
                            errors.rejectValue("confirmationCode", "wrong.userNotificationSettings.phoneCode");
                    }
                }
            }
        }
    }
}

package ru.planeta.api.web.utils;

import com.google.common.net.HttpHeaders;
import freemarker.template.Configuration;
import freemarker.template.DefaultObjectWrapper;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import org.apache.log4j.Logger;
import ru.planeta.api.model.stat.PurchaseReport;
import ru.planeta.model.bibliodb.Book;
import ru.planeta.model.bibliodb.Library;
import ru.planeta.model.bibliodb.PublishingHouse;
import ru.planeta.model.common.campaign.Share;

import javax.annotation.Nullable;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.Writer;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Class for generating Excel reports
 * User: m.shulepov
 * Date: 08.12.12
 * Time: 9:45
 */
public class ExcelReportUtils {

    public enum ReportType {
        SALES, SALES_PRODUCTS, REGISTRATIONS, SHARES_SALE, BOOKS, LIBRARIES, PUB_HOUSES
    }

    private static final String TEMPLATE_FILE = "excel-report-templates.ftl";
    private static final String DATE_FORMAT_PATTERN = "dd.MM.yyyy H:mm:ss";

    private static final Configuration configuration;
    private static final Logger log = Logger.getLogger(ExcelReportUtils.class);

    static {
        /**
         * remove logging of freemarker.cache
         * don't call it after Configuration creation, it will have
         * <a href="http://freemarker.sourceforge.net/docs/pgui_misc_logging.html">no effect</a>
         */
        try {
            freemarker.log.Logger.selectLoggerLibrary(freemarker.log.Logger.LIBRARY_NONE);
        } catch (ClassNotFoundException e) {
            log.warn("FreeMarker console wasn't disabled", e);
        }

        configuration = new Configuration();
        configuration.setLocale(new Locale("ru", "RU"));
        configuration.setEncoding(configuration.getLocale(), "UTF-8");
        // no data-model transform before use it in template
        configuration.setObjectWrapper(new DefaultObjectWrapper());
        // to have 11111111 id not like "11 111 111"
        configuration.setNumberFormat("0.######");
        configuration.setDateTimeFormat(DATE_FORMAT_PATTERN);
        // define loader from jar using Class.getResource
        configuration.setClassForTemplateLoading(ExcelReportUtils.class, "/ru/planeta/templates");
    }

    public static void generateCampaignSharesSaleReport(List<Share> shares, Writer writer) {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("hf", new ru.planeta.api.web.utils.JstlHelperFunctions());
        generateReport(shares, ReportType.SHARES_SALE, params, writer);
    }

    public static void generateSalesReport(NavigableMap<Date, PurchaseReport> reportData, BigDecimal totalAmount, Writer writer) {
        Map<String, PurchaseReport> formattedData = new LinkedHashMap<String, PurchaseReport>();
        DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
        for (Date date : reportData.navigableKeySet()) {
            formattedData.put(dateFormat.format(date), reportData.get(date));
        }
        Map<String, Object> additionalParams = new HashMap<String, Object>();
        additionalParams.put("totalAmount", totalAmount);
        generateReport(formattedData, ReportType.SALES, additionalParams, writer);
    }

    public static void generateStatisticReport(List reportData, BigDecimal totalAmount, Integer totalCount, Writer writer, ReportType reportType) {
        Map<String, Object> param = new HashMap<String, Object>();
        param.put("totalAmount", totalAmount);
        param.put("totalCount", totalCount);
        generateReport(reportData, reportType, param, writer);
    }

    public static void generateBookReport(List<Book> books, Writer writer) {
        Map<String, Object> params = new HashMap<String, Object>();
        generateReport(books, ReportType.BOOKS, params, writer);
    }

    public static void generateLibraryReport(List<Library> libs, Writer writer) {
        Map<String, Object> params = new HashMap<String, Object>();
        generateReport(libs, ReportType.LIBRARIES, params, writer);
    }

    public static void generatePublishingHouseReport(List<PublishingHouse> phs, Writer writer) {
        Map<String, Object> params = new HashMap<String, Object>();
        generateReport(phs, ReportType.PUB_HOUSES, params, writer);
    }

    private static void generateReport(Object reportData, ReportType reportType, @Nullable Map<String, Object> additionalParams, Writer writer) {
        try {
            Template template = configuration.getTemplate(TEMPLATE_FILE);

            Map<String, Object> map = new HashMap<String, Object>();
            map.put("items", reportData);
            map.put("StringUtils", new ru.planeta.commons.lang.StringUtils());
            map.put("reportType", reportType);
            if (additionalParams != null) {
                map.putAll(additionalParams);
            }
            template.process(map, writer);
        } catch (TemplateException e) {
            log.warn("Excel report templates: Bad parameters", e);
        } catch (IOException e) {
            log.error("Excel templates IO", e);
        } catch (Exception e) {
            log.error("Unknown exception", e);
        } finally {
            try {
                writer.close();
            } catch (IOException e) {
                // Nothing we can do here
            }
        }
    }

    public static void setReportHeaders(HttpServletResponse response) {
        response.setStatus(HttpServletResponse.SC_OK);
        String reportFileName = "Report_" + UUID.randomUUID() + "_.xls";
        response.setHeader(HttpHeaders.CONTENT_DISPOSITION, "attachment;filename=" + reportFileName);
        response.setHeader(HttpHeaders.CONTENT_TYPE, "application/vnd.ms-excel; charset=UTF-8");
    }

}

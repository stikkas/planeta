package ru.planeta.api.web.filter.logging;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.filter.GenericFilterBean;
import ru.planeta.api.helper.ProjectService;
import ru.planeta.api.web.authentication.AuthUtils;
import ru.planeta.commons.web.CookieUtils;
import ru.planeta.commons.web.WebUtils;
import ru.planeta.model.enums.ProjectType;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Random;

/**
 * User: a.volkov
 * Date: 04.08.14
 * Time: 18:08
 */
public class VisitorFilter extends GenericFilterBean {

    private static final Logger log = Logger.getLogger(VisitorFilter.class);

    private final String cookieDomain;

    private final static int maxVisitor = Integer.MAX_VALUE - 100;

    private static final String[] EXCLUDES = {"help.yahoo.com", "bot", "prerender", "domainappender", "mailruconnect", "skypeuripreview", "okhttp", "metauri", "bing", "facebook", "python-urllib"};

    private static final String[] DEPRECATED_UA = {"Python-urllib/2.7"};

    @Autowired
    public VisitorFilter(ProjectService projectService) {
        this.cookieDomain = "." + projectService.getHost(ProjectType.MAIN);
    }

    @Override
    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain fc) throws ServletException, IOException {
        HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response = (HttpServletResponse) resp;

        if (AuthUtils.INSTANCE.isAjaxRequest(request) || "POST".equals(request.getMethod())) {
            fc.doFilter(request, response);
            return;
        }
        final String userAgent = request.getHeader("User-Agent");
        if (userAgent != null) {

            for (String deprecated : DEPRECATED_UA) {
                if (deprecated.equals(userAgent)) {
                    throw new ServletException("Too many requests from ip");
                }
            }

            for (String ignoreAgent : EXCLUDES) {
                if (StringUtils.containsIgnoreCase(userAgent, ignoreAgent)) {
                    fc.doFilter(request, response);
                    return;
                }
            }
        } else {
            fc.doFilter(request, response);
            return;
        }

        String visitorId = CookieUtils.getCookieValue(request.getCookies(), CookieUtils.VISITOR_COOKIE_NAME, null);
        if (StringUtils.isEmpty(visitorId)) {
            int vid = new Random().nextInt(maxVisitor) + 1;
            Cookie myCookie = new Cookie(CookieUtils.VISITOR_COOKIE_NAME, String.valueOf(vid));
            myCookie.setDomain(cookieDomain);
            response.addCookie(myCookie);
        }

        String old_referrer = CookieUtils.getCookieValue(request.getCookies(), CookieUtils.EXTERNAL_REFERRER, null);
        if (StringUtils.isNotEmpty(old_referrer)) {
            fc.doFilter(request, response);
            return;
        }

        CookieUtils.removeCookie(request, response, "referrer_url");
        CookieUtils.removeCookie(request, response, "enter_url");

        String referer = WebUtils.getHost(WebUtils.getReferer(request));
        if (referer != null && (referer.contains("localhost") || "planeta.ru".equals(referer) || "pg.planeta.ru".equals(referer))) {
            referer = "";
        }

        if (StringUtils.isNotEmpty(referer)) {
            Cookie myCookie = new Cookie(CookieUtils.EXTERNAL_REFERRER, referer);
            myCookie.setDomain(cookieDomain);
            myCookie.setPath("/");
            response.addCookie(myCookie);
        }

        String enterUrl = request.getRequestURI();
        Cookie enterCookie = new Cookie(CookieUtils.FIRST_ENTER_URL, enterUrl);
        enterCookie.setDomain(cookieDomain);
        enterCookie.setPath("/");
        response.addCookie(enterCookie);
        fc.doFilter(request, response);
    }

}

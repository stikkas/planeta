package ru.planeta.api.web.authentication;

import org.apache.commons.collections4.IterableUtils;
import org.apache.commons.collections4.Predicate;
import org.springframework.security.access.AccessDecisionVoter;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.access.ConfigAttribute;
import org.springframework.security.access.vote.AffirmativeBased;
import org.springframework.security.authentication.AuthenticationTrustResolver;
import org.springframework.security.authentication.AuthenticationTrustResolverImpl;
import org.springframework.security.core.Authentication;
import ru.planeta.api.web.utils.SessionUtils;

import java.util.Collection;
import java.util.List;

/**
 * Custom access decision manager, that either makes decision to redirect client to CAS for authentication
 * or delegates request to the parent class, which in this case is AffirmativeBased and lets it decide.
 * <p/>
 * User: m.shulepov
 * Date: 17.04.12
 * Time: 10:18
 */
public class CustomAccessDecisionManager extends AffirmativeBased {

    private static final String ROLE_ANONYMOUS = "ROLE_ANONYMOUS";
    private static final String ROLE_ADMIN = "ROLE_ADMIN";

    private boolean allowUnauthorizedAccessAjaxRequest = true;

    private AuthenticationTrustResolver authenticationTrustResolver = new AuthenticationTrustResolverImpl();

    public CustomAccessDecisionManager(List<AccessDecisionVoter<? extends Object>> decisionVoters) {
        super(decisionVoters);
    }

    @Override
    public void decide(Authentication authentication, Object object, Collection<ConfigAttribute> configAttributes) throws AccessDeniedException {
        if (containsRole(configAttributes, ROLE_ADMIN)) {
            super.decide(authentication, object, configAttributes);
            return;
        }

        // allow access if request does not contain auth cookie
        if (isAnonymousAccessAllowed(configAttributes) && !AuthUtils.INSTANCE.containsAuthCookie()) {
            return;
        }

        // TODO danger! need to remove this section
        if (allowUnauthorizedAccessAjaxRequest) {
            // allow access if AJAX request
            if (AuthUtils.INSTANCE.isAjaxRequest()) {
                return;
            }
        }

        if (needAuthorization(authentication, configAttributes)) {
            throw new AccessDeniedException(messages.getMessage("AbstractAccessDecisionManager.accessDenied", "Access is denied"));
        } else {
            super.decide(authentication, object, configAttributes);
        }
    }

    private static boolean isAnonymousAccessAllowed(Collection<ConfigAttribute> configAttributes) {
        return configAttributes.size() > 1 && containsRole(configAttributes, ROLE_ANONYMOUS);
    }

    private boolean needAuthorization(Authentication authentication, Collection<ConfigAttribute> configAttributes) {
        boolean anonymousAccessAllowed = isAnonymousAccessAllowed(configAttributes);
        boolean isCasRedirect = SessionUtils.INSTANCE.isRedirectFromCAS(AuthUtils.INSTANCE.getRequest().getRequestURI());
        if (anonymousAccessAllowed && !isCasRedirect && authenticationTrustResolver.isAnonymous(authentication)) {
            AuthUtils.INSTANCE.getRequest().setAttribute(AuthUtils.CUSTOM_FAILURE_URL, "true");
            return true;
        }
        return false;
    }

    private static boolean containsRole(Collection<ConfigAttribute> configAttributes, final String role) {

        return IterableUtils.matchesAny(configAttributes, new Predicate<ConfigAttribute>() {
            @Override
            public boolean evaluate(ConfigAttribute object) {
                return role.equals(object.getAttribute());
            }
        });
    }

    public void setAllowUnauthorizedAccessAjaxRequest(boolean allowAccessAjaxRequest) {
        allowUnauthorizedAccessAjaxRequest = allowAccessAjaxRequest;
    }
}

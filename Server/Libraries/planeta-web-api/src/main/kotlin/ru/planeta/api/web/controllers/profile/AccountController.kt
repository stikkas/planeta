package ru.planeta.api.web.controllers.profile

import org.springframework.validation.BindingResult
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import ru.planeta.api.exceptions.PermissionException
import ru.planeta.api.model.ActionStatus
import ru.planeta.api.model.UserCredentialsInfo
import ru.planeta.api.service.billing.order.MoneyTransactionService
import ru.planeta.api.service.billing.order.OrderService
import ru.planeta.api.service.common.InvoiceBillService
import ru.planeta.api.service.registration.RegistrationService
import ru.planeta.api.web.authentication.isAnonymous
import ru.planeta.api.web.authentication.myProfileId
import ru.planeta.api.web.authentication.userAuthorizationInfo
import ru.planeta.api.web.controllers.Urls
import ru.planeta.api.web.controllers.services.BaseControllerService
import ru.planeta.dao.commondb.UserPrivateInfoDAO
import ru.planeta.model.common.InvoiceBill
import ru.planeta.model.common.MoneyTransaction
import ru.planeta.model.common.OrderInfo
import ru.planeta.model.enums.MoneyTransactionType
import ru.planeta.model.enums.OrderObjectType
import ru.planeta.model.profile.User
import javax.validation.Valid

/**
 * @author ds.kolyshev
 * Date: 09.04.12
 */
@RestController
class AccountController(private val orderService: OrderService,
                        private val invoiceBillService: InvoiceBillService,
                        private val moneyTransactionService: MoneyTransactionService,
                        private val userPrivateInfoDAO: UserPrivateInfoDAO,
                        private val registrationService: RegistrationService,
                        private val baseControllerService: BaseControllerService) {

    @GetMapping(Urls.PROFILE_SEARCH_PURCHASES)
    fun getPurchasesWithSearch(@RequestParam(defaultValue = "0") offset: Int,
                               @RequestParam(defaultValue = "20") limit: Int,
                               @RequestParam(required = false) type: OrderObjectType?,
                               @RequestParam(required = false) searchString: String?): Collection<OrderInfo> {
        val myProfileId = myProfileId()
        return orderService.getProfilePurchaseOrders(myProfileId, searchString, myProfileId, type, offset, limit)
    }

    @GetMapping(Urls.PROFILE_SEARCH_BILLINGS)
    fun getBillsWithSearch(@RequestParam(defaultValue = "0") offset: Int,
                           @RequestParam(defaultValue = "20") limit: Int,
                           @RequestParam(required = false) searchString: String?): List<InvoiceBill> =
            invoiceBillService.getProfileBills(myProfileId(), searchString ?: "", offset, limit)

    @GetMapping(Urls.PROFILE_TRANSACTIONS)
    fun getTransactions(@RequestParam(defaultValue = "0") offset: Int,
                        @RequestParam(defaultValue = "20") limit: Int,
                        @RequestParam(required = false) transactionType: MoneyTransactionType?,
                        @RequestParam(required = false) orderObjectType: OrderObjectType?): List<MoneyTransaction> {
        val myProfileId = myProfileId()
        return moneyTransactionService.getTransactions(myProfileId, myProfileId, transactionType, orderObjectType, offset, limit)
    }

    @PostMapping(Urls.USER_SAVE_REGISTRATION_CREDENTIALS)
    fun saveUserEmail(@Valid credentials: UserCredentialsInfo, result: BindingResult): ActionStatus<*> {
        if (result.hasErrors()) {
            return baseControllerService.createErrorStatus<Any>(result)
        }
        val userPrivateInfo = userPrivateInfoDAO.selectByUsername(credentials.email)
        if (userPrivateInfo != null) {
            if (isAnonymous()) {
                credentials.profileId = userPrivateInfo.userId
                try {
                    baseControllerService.authorizationService.addCredentials(credentials)
                } catch (ex: PermissionException) {
                    // confirmation start
                    val status = ActionStatus<User>()
                    status.isSuccess = true
                    status.addFieldError("email", "Duplicate credentials")
                    return status
                }
            } else {
                registrationService.mergeAccounts(userPrivateInfo, userAuthorizationInfo()?.userPrivateInfo)
            }
        }
        return ActionStatus.createSuccessStatus<Any>()
    }
}


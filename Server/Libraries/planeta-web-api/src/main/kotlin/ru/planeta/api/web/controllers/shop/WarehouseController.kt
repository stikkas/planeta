package ru.planeta.api.web.controllers.shop

import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import ru.planeta.api.model.ActionStatus
import ru.planeta.api.service.shop.ProductTagService
import ru.planeta.api.service.shop.ProductUsersService
import ru.planeta.api.web.authentication.myProfileId
import ru.planeta.api.web.controllers.Urls
import ru.planeta.api.web.transfer.ShopUtilsInitializeInfo
import ru.planeta.model.shop.Product
import ru.planeta.model.shop.enums.ProductCategory

/**
 * Created by IntelliJ IDEA.
 * User: Andrew
 * Date: 19.09.12
 * Time: 14:24
 */
@RestController
class WarehouseController(private val productUsersService: ProductUsersService,
                          private val productTagService: ProductTagService) {

    @GetMapping(Urls.Shop.GET_PRODUCT)
    fun getProduct(@RequestParam productId: Long): Product = productUsersService.getProductCurrent(myProfileId(), productId)


    @GetMapping(Urls.Shop.INITIALIZE_SHOP_UTILS)
    fun initializeShopUtils(): ActionStatus<ShopUtilsInitializeInfo> {
        val shopUtilsInitializeInfo = ShopUtilsInitializeInfo()
        //needs for product search
        shopUtilsInitializeInfo.productTags = productTagService.getProductTags(0, 0)
        shopUtilsInitializeInfo.productCategories = ProductCategory.values()
        return ActionStatus.createSuccessStatus(shopUtilsInitializeInfo)
    }

}

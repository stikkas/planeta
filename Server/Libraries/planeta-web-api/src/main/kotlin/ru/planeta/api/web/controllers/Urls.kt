package ru.planeta.api.web.controllers

import ru.planeta.api.text.FormattingService

/**
 * Class containing common urls used in web app
 *
 * @author atropnikov
 */
object Urls {
    const val CAMPAIGN = "/api/campaign/campaign.json"
    const val CAMPAIGN_COLLECTED_AMOUNT = "/api/campaign/campaign-collected-amount.json"
    const val CAMPAIGN_PURCHASED_SHARE_IDS = "/api/campaign/campaign-purchased-share-ids.json"
    const val CAMPAIGN_PURCHASED_CAMPAIGN_IDS = "/api/campaign/campaign-purchased-campaign-ids.json"
    const val CAMPAIGNS = "/api/campaign/campaigns.json"
    const val CAMPAIGNS_DTO = "/api/campaign/campaigns_dto.json"
    const val GET_SIMPLE_CAMPAIGN = "/api/campaign/get-campaign.json"
    const val CAMPAIGN_SHORT_INFO = "/api/public/campaign/campaign-short-info.json"
    const val CAMPAIGN_VIDEO_INFO = "/api/campaign/campaign-video-info.json"
    const val GET_LAST_CAMPAIGN_MODERATION_MESSAGE = "/api/campaign/last-campaign-moderation-message.json"
    const val GET_LAST_CAMPAIGN_MODERATION_MESSAGE_LIST = "/api/campaign/last-campaign-moderation-message-list.json"
    const val ADD_CAMPAIGN_MODERATION_AUTHOR_RESPONSE_MESSAGE = "/api/campaign/add-campaign-moderation-author-response-message.json"
    const val CAMPAIGN_BACKERS = "/api/campaign/backers.json"
    const val CAMPAIGN_TOTAL_PURCHASE = "/api/util/campaign/total-purchase-sum.json"
    const val CAMPAIGN_GET_UPDATES_JSON = "/api/campaign/campaign-updates.json"
    const val PROFILE_NEWS_POST = "/api/campaign/profile-post.json"
    const val CAMPAIGN_RELATED_LIST = "/api/profile/related-campaigns.json"
    const val CAMPAIGN_LIST_WITH_COUNT = "/api/campaign/get-campaign-list-with-count.json"

    const val SHORT_CAMPAIGN_STAT = "/api/campaign/get-short-campaign-stat.json"
    const val BACKED_CAMPAIGN_LIST = "/api/campaign/get-backed-campaign-list.json"
    const val USER_BACKED_CAMPAIGN_TAG_COUNT_LIST = "/api/campaign/get-user-backed-campaign-tag-count-list.json"
    const val CAMPAIGN_NEW_EVENTS_COUNT = "/api/campaign/get-campaign-new-events-count.json"

    const val CAMPAIGN_END_SUBSCRIBE = "/api/profile/subscribe-campaign-end.json"
    const val CAMPAIGN_END_UNSUBSCRIBE = "/api/profile/unsubscribe-campaign-end.json"
    const val CAMPAIGN_END_IS_SUBSCRIBED = "/api/profile/is-subscribed-campaign-end.json"

    const val VALIDATE_SHARE_PURCHASE = "/api/public/campaign/validate-share-purchase.json"
    const val VALIDATE_INVEST_PURCHASE = "/campaign/validate-invest-purchase.json"
    const val CAMPAIGN_CONTACTS = "/api/profile/campaign/contacts-list.json"
    const val DOES_CAMPAIGN_HAVE_CONTACT_EMAIL_TO_SEND_QUESTION_TO_AUTHOR = "/campaign/is-it-possible-to-send-mail-to-author.json"
    const val CAMPAIGN_CONTACTS_COUNT = "/campaign/contacts-count.json"
    const val CAMPAIGN_CURATORS = "/campaign/curators-list.json"
    const val GET_SHARE_DELIVERY_SERVICES = "/api/campaign/get-share-delivery-services.json"

    const val MODERATOR_CAMPAIGN_GET_CURRENT_MANAGER = "/api/campaigns/campaign-get-current-manager.json"


    const val CAMPAIGN_SAVE_CONTACT_INFO = "/api/campaigns/campaign-save-contact-info.json"

    const val FINISH_CAMPAIGN_TRAFFIC_INFO = "/api/campaigns/finish-campaign-traffic-info.json"


    //<editor-fold defaultstate="collapsed" desc="Search controller urls">
    const val SEARCH_USERS = "/search-users.json"
    const val SEARCH_POSTS = "/search-posts.json"
    const val SEARCH_BROADCASTS = "/search-broadcasts.json"
    const val SEARCH_PROJECTS = "/search-projects.json"
    const val SEARCH_CHARITY = "/search-charity.json"
    const val SEARCH_LOCATION_FILTERS = "/api/search/search-location-filter-names"

    const val SEARCH_PRODUCTS_JSON = "/search-products.json"

    const val SEARCH_PRODUCTS_JSON_NEW = "/products/search-products.json"
    const val SEARCH_CAMPAIGN_SPONSOR_ALIAS = "/api/util/search-campaign-sponsor.json"

    // Utility public methods
    const val BARCODE = "/api/profile/barcode.html"
    const val BARCODE_EAN13 = "/api/profile/barcode-ean13.html"
    const val QRCODE = "/api/profile/qrcode.html"
    const val PARSE_MESSAGE = "/api/parse.json"

    const val MANIFEST = "/api/util/manifest.json"
    const val BROWSERCONFIG = "/api/util/browserconfig.xml"
    const val COUNTRIES_LIST_JSON = "/api/util/country-list.json"
    const val COUNTRIES_LIST_IN_CAMPAIGNS_JSON = "/api/util/country-list-in-campaigns.json"
    const val CITIES_LIST_BY_SUBSTRING_JSON = "/api/util/cities-list-by-substring.json"
    const val REGIONS_LIST_BY_SUBSTRING_JSON = "/api/util/regions-list-by-substring.json"
    const val CITY_BY_ID_JSON = "/api/util/city-by-id.json"
    const val HTTP_CHECK = "/api/util/http-check.json"
    const val JETTY_PORT = "/api/util/jetty-port.json"
    const val JETTY_VERSION = "/api/util/jetty-version.json"
    const val STATIC_VERSION = "/api/util/static-version.json"
    const val HTML_CLEAN = "/api/util/html-clean.json"
    const val ADD_SUBSCRIBER = "/api/util/add-subscriber.json"
    const val CUSTOM_META_TAG_FOR_PAGE = "/api/util/custom-meta-tag-for-page.json"


    const val PROFILE_LIST = "/api/util/profile-list.json"
    // Investing info URLs
    const val GET_INVEST_ORDER_INFO = "/api/orders/invest-order-info.json"
    const val DEL_INVEST_ORDER_INFO = "/api/orders/del-invest-order-info.json"

    // find the same url in FormattingService
    const val AWAY_LINK = FormattingService.AWAY_URL

    const val AWAY_LINK_MAIL = FormattingService.AWAY_MAIL


    // Confirmations
    const val CONFIRM = "/api/confirm.json"
    const val CONFIRMATION_RESEND = "/api/confirm-resend.json"
    const val CONFIRM_PHONE = "/api/confirm-phone.json"

    // Photo controller
    const val PHOTO = "/api/public/photo.json"
    const val PHOTO_DELETE = "/api/profile/deleteByProfileId-photo.json"

    // Audio controller
    const val AUDIO_TRACKS = "/api/public/audio-tracks.json"
    const val AUDIO_TRACK = "/api/public/audio-track.json"
    const val AUDIO_SEVERAL_TRACKS = "/api/public/several-audio-tracks.json"

    // Video controller
    const val VIDEO = "/api/public/video.json"
    const val VIDEO_EXTENDED = "/api/public/video-extended.json"
    const val VIDEO_DELETE = "/api/profile/deleteByProfileId-video.json"
    const val VIDEO_EXTERNAL = "/api/public/video-external.json"
    // </editor-fold>

    // Payment controller
    const val PAYMENT_METHODS = "/api/public/payment-methods"
    const val PAYMENT_METHODS_OLD = "/api/profile/payment-methods.json"
    const val USER_SAVE_REGISTRATION_CREDENTIALS = "/api/profile/save-new-user-email.json"

    // Profile news
    const val PROFILE_NEWS = "/api/public/news.json"
    const val PROFILE_TECH_STATS = "/api/profile/tech-stats.json"
    const val PROFILE_TECH_PROJECT = "/api/profile/tech-project.json"
    const val PROFILE_NEWS_ADD_POST = "/api/profile/news/add-post.json"
    const val PROFILE_NEWS_CHANGE_POST = "/api/profile/news/change-post.json"
    const val PROFILE_NEWS_DELETE_POST = "/api/profile/news/deleteByProfileId-post.json"

    // Member zone, common pages
    const val CROP_IMAGE = "/api/profile/crop-image.html"
    const val UPLOAD_AVATAR = "/api/profile/upload-avatar.html"

    // Comment controller
    const val COMMENTS = "/api/public/comments.json"
    const val COMMENTS_WITH_CHILD_AND_RELATION_STATUSES = "/api/public/comments-with-child-and-profile-relation.json"
    const val COMMENTS_LAST = "/api/public/last-comments.json"

    //Secured api urls
    const val COMMENT_ADD = "/api/profile/add-comment.json"
    const val COMMENT_DELETE = "/api/profile/deleteByProfileId-comment.json"
    const val COMMENT_RESTORE = "/api/profile/restore-comment.json"

    // Account controller
    const val PROFILE_SEARCH_PURCHASES = "/profile/search-purchases.json"
    const val PROFILE_SEARCH_BILLINGS = "/profile/search-billings.json"
    const val PROFILE_TRANSACTIONS = "/profile/transactions.json"
    const val PROFILE_LK = "/api/profile/lk.json"
    const val ACCOUNT_LOGIN = "/api/public/login.json"
    const val ACCOUNT_SIGNUP = "/api/public/signup.json"

    // Registration
    const val RECOVER_PASSWORD = "/api/public/password-recover.json"
    const val RESEND_REGISTRATION_COMPLETE_EMAIL = "/api/profile/resend-email.json"

    // Feedback
    const val FEEDBACK = "/api/public/feedback.json"

    // Profile
    const val PROFILE_INFO = "/api/public/profile-info.json"

    // Sitemap
    const val HTML_SITE_MAP = "/welcome/sitemap.html"

    // <editor-fold defaultstate="collapsed" desc="Profile controller">
    const val PROFILE = "/api/public/profile.json"
    const val PROFILE_SAVE_AVATAR = "/api/profile/avatar-save.json"
    const val PROFILE_SAVE_SMALL_AVATAR = "/api/profile/avatar-small-save.json"
    const val PROFILE_SET_IS_SHOW_BACKED_CAMPAIGNS = "/api/profile/set-is-show-backed-campaigns.json"
    // </editor-fold>

    const val LIVE_CHECK = "/live-check"
    const val EMAIL_CHECK = "/api/public/email-check"

    //notifications
    const val UNSUBSCRIBE_AUTHOR = "/author-unsubscribe"


    //sharing urls
    const val SHARE_URL = "/api/share/share-url.html"
    const val GET_SHARED_OBJECT_COUNTER = "/api/share/get-counter.json"

    // TODO rename /api/...
    const val SEND_CAMPAIGN_NEWS_NOTIFICATION = "/api/profile/send-campaign-news-notification.json"
    const val PROMO_NEWS_POSTS = "/api/public/promo-news-posts.json"

    const val GET_PROXY_TICKET = "/api/profile/proxy-ticket.json"

    const val GET_BROADCAST_ADVERTISING_XML = "/api/advertising/get-xml"

    const val TOGGLE_CAMPAIGN_SUBSCRIPTION = "/api/profile/toggle-campaign-subscription.json"
    //welcome
    const val WELCOME_GET_CHARITY_CAMPAIGNS = "/api/welcome/get-charity-campaigns-list.json"
    const val WELCOME_GET_PROMO_CAMPAIGNS = "/api/welcome/get-promo-campaigns-list.json"
    const val WELCOME_GET_CAMPAIGN_TAGS = "/api/welcome/get-campaign-tags.json"
    const val WELCOME_GET_SPONSORS = "/api/welcome/get-sponsors.json"
    const val WELCOME_GET_DRAFT_CAMPAIGNS_LIST = "/api/profile/get-draft-campaigns.json"
    const val WELCOME_GET_LOW_BANNER_HTML = "/api/welcome/get-low-banner-html.json"
    const val WELCOME_GET_PARTNERS = "/api/welcome/get-partners.json"
    const val WELCOME_GET_CURATORS = "/api/welcome/get-curators.json"
    const val WELCOME_GET_DASHBOARD_FILTERED_CAMPAIGNS = "/api/welcome/dashboard-filtered-campaigns.json"

    const val PROFILE_CHECK_SUBSCRIBE = "/api/profile/check-subscribe.json"
    const val PROFILE_SUBSCRIBE = "/api/profile/subscribe.json"
    const val PROFILE_UNSUBSCRIBE = "/api/profile/unsubscribe.json"
    const val PROFILE_GET_SUBSCRIBER_LIST = "/api/profile/get-subscriber-list.json"
    const val PROFILE_GET_SUBSCRIPTION_LIST = "/api/profile/get-subscription-list.json"
    const val PROFILE_GET_SUBSCRIPTION_INFO = "/api/profile/get-subscription-info.json"
    const val PROFILE_GET_SUBSCRIBER_INFO = "/api/profile/get-subscriber-info.json"
    const val PROFILE_SET_ADMIN_SUBSCRIBER = "/api/profile/set-admin-subscriber.json"
    const val PROFILE_LAST_VISIT = "/api/profile/profile-last-visit.json"
    const val PROFILE_GET_TABS_COUNT_INFO = "/api/profile/get-tabs-count-info.json"
    //</editor-fold>

    object Search {
        const val SEARCH_SHARES_JSON = "/api/search/shares"
    }

    object Shop {

        const val GET_PRODUCT = "/product.json"
        const val INITIALIZE_SHOP_UTILS = "/api/public/initialize-shop-utils.json"
    }

    object Member {

        const val ACCOUNT_PURCHASES = "/account/purchases"
        const val ACCOUNT_PAYMENTS = "/account/payments"
    }


    object Payment {
        const val DELIVERY_PAYMENT_CREATE = "/api/public/delivery-payment-create.json"
        const val PAYMENT_CREATE = "/payment-create.html"
        const val PAYMENT_RECREATE = "/payment-recreate.html"
        const val PAYMENT_SUCCESS = "/payment-success.html"
        const val PAYMENT_FAIL = "/payment-fail.html"
        const val PAYMENT_FAIL_SIMPLE = "/payment-fail-simple.html"
    }

    object Contractor {
        const val CONTRACTOR_BY_CAMPAIGN = "/admin/contractor-get-campaign.json"
        const val LAST_UPLOADED_FILES = "/admin/last-uploaded-files.json"
        const val DELETE_UPLOADED_PROFILE_FILE = "/admin/deleteByProfileId-uploaded-profile-file.json"
        const val CONTRACTOR_DELETE = "/admin/contractor-deleteByProfileId.json"
        const val CONTRACTOR_ADD_AND_CHANGE_CAMPAIGN_STATUS = "/admin/contractor-add-and-change-campaign-status.json"
        const val CONTRACTOR_ADD_EXTRA_VALIDATE_AND_CHANGE_CAMPAIGN_STATUS = "/admin/contractor-add-extra-validate-and-change-campaign-status.json"
        const val CONTRACTOR_BIND_AND_CHANGE_CAMPAIGN_STATUS = "/admin/contractor-bind-and-change-campaign-status.json"
        const val CONTRACTOR_LIST = "/admin/contractor-list.json"
        const val CONTRACTOR_LIST_BY_PROFILE = "/admin/contractor-list-by-profile.json"

    }

    object Callback {
        const val IS_AVAILABLE = "/api/public/callback/available"
        const val CREATE = "/api/public/callback/create"
        const val PROCESS = "/api/callback/process.json"
    }

    object Faq {
        const val FAQ_CATEGORY_LIST = "/api/public/faq-category-list.json"
        const val FAQ_ARTICLE_LIST = "/api/public/faq-article-list.json"
        const val FAQ_PARAGRAPH_LIST = "/api/public/faq-paragraph-list.json"
        const val FAQ_PARAGRAPH_LIST_WITH_ARTICLE = "/api/public/faq-paragraph-list-with-article.json"

        const val FAQ_EDIT_EDIT_ARTICLE = "/admin/faq-edit/edit-article.json"
        const val FAQ_EDIT_RESORT_ARTICLE = "/admin/faq-edit/resort-article.json"
        const val FAQ_EDIT_DELETE_ARTICLE = "/admin/faq-edit/deleteByProfileId-article.json"

        const val FAQ_EDIT_EDIT_PARAGRAPH = "/admin/faq-edit/edit-paragraph.json"
        const val FAQ_EDIT_RESORT_PARAGRAPH = "/admin/faq-edit/resort-paragraph.json"
        const val FAQ_EDIT_DELETE_PARAGRAPH = "/admin/faq-edit/deleteByProfileId-paragraph.json"

        const val SEARCH = "/api/public/faq-search.json"
    }

    object Quiz {
        const val QUIZ = "/api/quiz/get-quiz.json"
        const val QUIZ_QUESTIONS = "/api/quiz/quiz-questions.json"
        const val ADD_QUIZ_ANSWERS = "/api/quiz/add-quiz-answers.json"
        const val CHECK_USER_HAS_ANSWERED_QUIZ = "/api/quiz/check-user-answered-quiz.json"
    }

    object SchoolWayCampaign {
        const val CREATE_USER = "/api/school-way/create-user.json"
    }

    interface Gtm {
        companion object {
            const val GET_CAMPAIGN_LIST_INFO = "/api/public/gtm/get-campaign-list-info.json"
        }
    }
}

package ru.planeta.api.web.controllers.profile

import org.springframework.validation.BindingResult
import org.springframework.web.bind.annotation.*
import ru.planeta.api.model.ActionStatus
import ru.planeta.api.service.content.PhotoService
import ru.planeta.api.service.profile.ProfileService
import ru.planeta.api.web.authentication.myProfileId
import ru.planeta.api.web.controllers.Urls
import ru.planeta.api.web.controllers.services.BaseControllerService
import ru.planeta.model.profile.media.Photo
import javax.validation.Valid

/**
 * Controller for user photos
 *
 * @author ameshkov
 */
@RestController
class PhotoController(private val photoService: PhotoService,
                      private val profileService: ProfileService,
                      private val baseControllerService: BaseControllerService) {

    @GetMapping(Urls.PHOTO)
    fun getPhoto(@RequestParam(value = "profileId") alias: String,
                 @RequestParam photoId: Long): Photo? {
        val profileId = profileService.getProfileSafe(alias).profileId
        return photoService.getPhoto(myProfileId(), profileId, photoId)
    }

    @PostMapping(Urls.PHOTO)
    fun savePhoto(@Valid photo: Photo, result: BindingResult): ActionStatus<Photo> {
        return if (result.hasErrors()) {
            baseControllerService.createErrorStatus(result)
        } else ActionStatus.createSuccessStatus(photoService.savePhoto(myProfileId(), photo))

    }

    @PostMapping(Urls.PHOTO_DELETE)
    fun deletePhoto(@RequestParam profileId: Long,
                    @RequestParam photoId: Long): ActionStatus<*> {
        photoService.deletePhoto(myProfileId(), profileId, photoId)
        return ActionStatus.createSuccessStatus<Any>()
    }

}

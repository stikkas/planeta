package ru.planeta.eva.api.web.models

import ru.planeta.eva.api.web.dto.AddressDTO
import ru.planeta.eva.api.web.dto.CustomerContactDTO
import ru.planeta.model.shop.enums.DeliveryType

class DeliveryInfo {

    var address: AddressDTO? = null
    var serviceId: Long = 0
    var deliveryType: DeliveryType? = null
    var customerContacts: CustomerContactDTO? = null

}

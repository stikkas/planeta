package ru.planeta.eva.api.web

object Urls {
    const val SHARE_KEY = "shareId"

    const val VALIDATE_SHARE_PURCHASE = "/api/public/campaign/validate-share-purchase"
    const val PURCHASE_SHARE_INFO = "/api/public/purchase-share-info/{$SHARE_KEY}"

    const val PURCHASE_SHARE = "/api/public/purchase-share"
    const val LOGIN = "/api/public/signin"
    const val LOGOUT = "/api/public/logout"
    const val SIGNUP = "/api/public/signup"
    const val RECOVER = "/api/public/recover"
    const val USER_INFO = "/api/public/user-info"
    const val HEADER_SEARCH = "/api/public/header-search"
    const val MY_BALANCE = "/api/profile/my-balance"

    const val MAILER_SUBSCRIBE = "/api/public/mailer-subscribe"

    const val GET_REWARD_INFO = "/api/public/rewards/{id}"

    // Контакты любого пользователя
    const val PROFILE_SITES = "/api/public/profile-sites"

    const val AUDIO = "/api/public/audio"

    const val CITIES_LIST_BY_SUBSTRING = "/api/util/cities-list-by-substring"
    const val CITY_BY_ID = "/api/util/city-by-id"

    const val FEEDBACK = "/api/public/feedback"
}

object ProfileUrls {
    const val UPDATES_FOR_ME = "/api/profile/updates-for-me"
    const val MY_CAMPAIGNS = "/api/profile/my-campaigns"
    const val MY_PURCHASED_CAMPAIGNS = "/api/profile/my-purchased-campaigns"
    const val MY_PURCHASED_REWARDS = "/api/profile/my-purchased-rewards"
    const val MY_CAMPAIGNS_STATS = "/api/profile/my-campaigns-stats"
    const val MY_CAMPAIGNS_STATUSES = "/api/profile/my-campaign-statuses"
    const val MY_PURCHASED_CAMPAIGNS_STATUSES = "/api/profile/my-purchased-campaign-statuses"
    const val MY_PURCHASES_AND_REWARDS = "/api/profile/my-purchases-and-rewards"
    const val PROFILE_NEWS_ADD_POST = "/api/profile/news/add-post"

    const val REMOVE_DRAFT = "/api/profile/campaign-remove"
    const val MY_SITES = "/api/profile/my-sites"
    const val SAVE_MY_SITES = "/api/profile/save-my-sites"
}

package ru.planeta.api.web.authentication

import org.apache.commons.lang3.math.NumberUtils
import org.springframework.security.core.Authentication
import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.web.context.request.RequestContextHolder
import org.springframework.web.context.request.ServletRequestAttributes
import ru.planeta.api.model.UserAuthorizationInfo
import ru.planeta.api.model.authentication.AuthenticatedUserDetails
import ru.planeta.api.service.security.PermissionService
import ru.planeta.api.web.utils.SessionUtils
import ru.planeta.commons.web.CookieUtils
import javax.servlet.http.Cookie
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

/**
 * Authentication Utils
 *
 *
 * User: m.shulepov
 * Date: 17.04.12
 * Time: 10:18
 */
object AuthUtils {
    const val CUSTOM_FAILURE_URL = "custom_failure_url"
    const val SUCCESS_CAS_REDIRECT_KEY = "successUrl"
    const val AUTH_ENABLED_COOKIE_NAME = "auth_enabled"
    const val AUDITED_ADMIN_ID = "auditedAdminId"

    val request: HttpServletRequest
        get() {
            val attr = RequestContextHolder.currentRequestAttributes() as ServletRequestAttributes
            return attr.request
        }

    /**
     * Checks if current request is ajax
     *
     * @return true if it's an ajax request and false otherwise
     */
    val isAjaxRequest: Boolean
        get() = isAjaxRequest(request)

    /**
     * Checks if specified request is ajax.
     *
     * @return true if it's an ajax request and false otherwise
     */
    fun isAjaxRequest(request: HttpServletRequest): Boolean {
        return "XMLHttpRequest" == request.getHeader("X-Requested-With") || request.requestURI.contains(".json")
    }

    /**
     * Checks if request has an authorization cookie, which is set after client's successful authorization.
     *
     */
    fun containsAuthCookie(): Boolean {
        return CookieUtils.getCookieValue(request.cookies, AUTH_ENABLED_COOKIE_NAME, null) != null
    }

    /**
     * Sets authorization cookie, so that when client makes request next time, it would be redirected to CAS
     *
     */
    fun setAuthCookie(request: HttpServletRequest, response: HttpServletResponse, domain: String?) {
        val cookie = Cookie(AUTH_ENABLED_COOKIE_NAME, "true")
        cookie.maxAge = Integer.MAX_VALUE
        cookie.path = "/"
        var cookieDomain = domain ?: request.serverName
        if (!cookieDomain.startsWith(".")) {
            cookieDomain = "." + cookieDomain
        }
        cookie.domain = cookieDomain
        response.addCookie(cookie)
    }


    fun saveAuditedAdminId(request: HttpServletRequest, authentication: Authentication) {
        val userAuthInfo = if (authentication.principal is AuthenticatedUserDetails)
            (authentication.principal as AuthenticatedUserDetails).userAuthorizationInfo
        else
            null

        val adminId = NumberUtils.toLong(request.getParameter(AUDITED_ADMIN_ID))
        val user = userAuthInfo?.user

        if (adminId > 0 && user != null) {
            SessionUtils.setAdminAuditInfo(adminId, user.profileId)
        }
        "".isNullOrEmpty()
    }
}




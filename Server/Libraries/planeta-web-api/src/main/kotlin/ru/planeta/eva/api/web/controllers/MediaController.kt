package ru.planeta.eva.api.web.controllers

import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import ru.planeta.eva.api.services.MediaService
import ru.planeta.eva.api.web.ActionStatus
import ru.planeta.eva.api.web.Urls

@RestController
class MediaController(private val mediaService: MediaService) {

    @GetMapping(Urls.AUDIO)
    fun getAudio(@RequestParam("trackId") trackId: Long): ActionStatus =
        ActionStatus(true, mediaService.getAudio(trackId))
}

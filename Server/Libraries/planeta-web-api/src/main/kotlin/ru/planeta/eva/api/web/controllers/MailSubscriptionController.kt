package ru.planeta.eva.api.web.controllers

import org.apache.log4j.Logger
import org.springframework.validation.FieldError
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController
import ru.planeta.api.utils.ValidateUtils
import ru.planeta.eva.api.services.MailerSubscriptionService
import ru.planeta.eva.api.web.ActionStatus
import ru.planeta.eva.api.web.Urls
import ru.planeta.eva.api.web.dto.MailerSubscribeDTO

@RestController
class MailSubscriptionController(private val mailerSubscriptionService: MailerSubscriptionService) {

    private val logger = Logger.getLogger(AuthController::class.java)

    @PostMapping(Urls.MAILER_SUBSCRIBE)
    fun subscribe(@RequestBody dto: MailerSubscribeDTO?) : ActionStatus {
        if (dto == null) {
            return ActionStatus(FieldError("subscription", "email", "unexpexted_error"))
        }

        val email: String? = dto.email

        if (email == null) {
            return ActionStatus(FieldError("subscription", "email", "wrong_email"))
        }

        if (!ValidateUtils.isValidEmail(email)) {
            return ActionStatus(FieldError("subscription", "email", "wrong_email"))
        }

        try {
            mailerSubscriptionService.subscribeNewUser(email)
        } catch (ex: Exception) {
            logger.error(ex)
            return ActionStatus(FieldError("subscription", "email", "unexpexted_error"))
        }
        return ActionStatus(true)
    }
}

package ru.planeta.api.web.controllers.profile

import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.servlet.ModelAndView
import ru.planeta.api.service.profile.AuthorNotificationService
import ru.planeta.api.service.profile.ProfileService
import ru.planeta.api.web.controllers.Urls
import ru.planeta.api.web.controllers.services.BaseControllerService
import ru.planeta.model.enums.ProjectType

/**
 * Controller for view notifications
 * Class NotificationController
 *
 * @author a.tropnikov
 */
@Controller
class NotificationController(private val profileService: ProfileService,
                             private val baseControllerService: BaseControllerService) {

    @RequestMapping(Urls.UNSUBSCRIBE_AUTHOR)
    fun unsubscribeAuthor(@RequestParam id: Long,
                          @RequestParam hash: String): ModelAndView {
        val validHash = AuthorNotificationService.getSubscriptionsKey(id)
        if (validHash == hash) {
            profileService.unsubscribeProfileFromAuthorSubscription(id)
        }
        return baseControllerService.createDefaultModelAndView("/static/unsubscribe-author", ProjectType.MAIN)
    }
}

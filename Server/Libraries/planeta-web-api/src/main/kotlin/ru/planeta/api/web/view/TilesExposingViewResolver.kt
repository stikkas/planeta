package ru.planeta.api.web.view

import org.springframework.web.servlet.view.AbstractUrlBasedView
import org.springframework.web.servlet.view.UrlBasedViewResolver

/**
 * Created with IntelliJ IDEA.
 * User: a.volkov
 * Date: 09.07.13
 * Time: 16:39
 */
class TilesExposingViewResolver : UrlBasedViewResolver() {

    override fun buildView(viewName: String?): AbstractUrlBasedView {
        val superView = super.buildView(viewName)
        if (superView is TilesExposingView) {
            if (this.exposeContextBeansAsAttributes != null) {
                superView.setExposeContextBeansAsAttributes(this.exposeContextBeansAsAttributes)
            }
            if (this.exposedContextBeanNames != null) {
                superView.setExposedContextBeanNames(*this.exposedContextBeanNames)
            }
        }
        return superView
    }

}

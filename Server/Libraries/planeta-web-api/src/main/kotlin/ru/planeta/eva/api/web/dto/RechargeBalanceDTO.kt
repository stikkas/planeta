package ru.planeta.eva.api.web.dto

import ru.planeta.eva.api.web.models.PaymentData
import java.math.BigDecimal

class RechargeBalanceDTO {
    var amount = BigDecimal.ZERO
    var paymentData = PaymentData()
}

package ru.planeta.api.web.controllers.services

import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.stereotype.Service
import org.springframework.web.servlet.ModelAndView
import ru.planeta.api.exceptions.NotFoundException
import ru.planeta.api.web.authentication.AuthUtils
import ru.planeta.api.web.authentication.getAuthentication
import ru.planeta.api.web.controllers.Urls
import ru.planeta.api.web.utils.SessionUtils
import ru.planeta.eva.api.web.cas.handlers.CustomAuthenticationSuccessHandler
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

/**
 * @author Andrew.Arefyev@gmail.com
 * 27.01.14 21:13
 */
@Service
class ControllerAutoLoginWrapService(val baseControllerService: BaseControllerService,
                                     private val authenticationSuccessHandler: CustomAuthenticationSuccessHandler,
                                     @Qualifier("userService")
                                     private val userDetailsService: UserDetailsService) {
    /**
     * Automatically logs user in
     *
     * @param username логин
     * @param request запрос
     * @throws ru.planeta.api.exceptions.NotFoundException if no such user
     */
    fun autoLoginUser(username: String, request: HttpServletRequest? = null) {
        val userDetails = userDetailsService.loadUserByUsername(username) ?: throw NotFoundException("Cannot find user details for \"$username\"")
        updateAuthenticationToken(userDetails, userDetails.password, userDetails.authorities)
        if (request != null) {
            val successUrl = request.getParameter(AuthUtils.SUCCESS_CAS_REDIRECT_KEY)
            SessionUtils.setRegistrationRedirectUrl(successUrl)
        }
    }

    fun autoLoginAndCasRedirectView(username: String, redirectUrl: String,
                                    request: HttpServletRequest,
                                    response: HttpServletResponse): ModelAndView {
        autoLoginUser(username)
        authenticationSuccessHandler.onAuthenticationSuccess(request, response, getAuthentication())

        return baseControllerService.createRedirectModelAndView(redirectUrl)
    }

    fun updateAuthenticationToken(userDetails: Any, password: String, authorities: Collection<GrantedAuthority>) {
        SessionUtils.setProfileAuthentication(getAuthentication())
        val authentication = UsernamePasswordAuthenticationToken(userDetails, password, authorities)
        SecurityContextHolder.getContext().authentication = authentication
    }
}

package ru.planeta.eva.api.web.cas.handlers

import org.apache.commons.lang3.StringUtils
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.core.Authentication
import org.springframework.security.web.authentication.logout.SimpleUrlLogoutSuccessHandler
import ru.planeta.api.web.authentication.addCookie
import ru.planeta.api.web.utils.SessionUtils
import ru.planeta.commons.web.CookieUtils
import ru.planeta.eva.api.web.cas.services.AuthTokenService
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

class CustomLogoutSuccessHandler(private val authCookieDomain: String,
                                 private val authToken: String) : SimpleUrlLogoutSuccessHandler() {

    @Autowired
    private lateinit var authTokenService: AuthTokenService

    override fun onLogoutSuccess(request: HttpServletRequest, response: HttpServletResponse, authentication: Authentication?) {
        var service = request.getParameter("service")
        if (StringUtils.isEmpty(service)) {
            service = "/"
        }
        removeAuthCookie(request, response)
        response.sendRedirect(service)
    }

    fun removeAuthCookie(request: HttpServletRequest, response: HttpServletResponse) {
        val token = CookieUtils.getCookieValue(request.cookies, authToken, null)
        if (token != null) {
            authTokenService.removeToken(token)
            addCookie(request, response, authToken, null, authCookieDomain, 0)
        }
        SessionUtils.session.removeAttribute(authToken)
    }
}


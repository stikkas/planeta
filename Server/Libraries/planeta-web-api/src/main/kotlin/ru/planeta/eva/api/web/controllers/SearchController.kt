package ru.planeta.eva.api.web.controllers

import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import ru.planeta.api.search.*
import ru.planeta.api.service.campaign.CampaignService
import ru.planeta.api.service.shop.ProductTagService
import ru.planeta.eva.api.web.ActionStatus
import ru.planeta.eva.api.web.Urls
import ru.planeta.dto.SearchResultItemDTO
import ru.planeta.dto.SearchResultsDTO
import java.net.URLDecoder
import java.util.ArrayList

@RestController("EvaWebApiSearchController")
class SearchController(private val campaignService: CampaignService,
                       private val searchService: SearchService,
                       private val productTagService: ProductTagService) {

    /**
     * Поиск продуктов, вознаграждений и проектов из заголовка
     */
    @GetMapping(Urls.HEADER_SEARCH)
    fun findAll(@RequestParam query: String): ActionStatus {
        val searchCampaigns = SearchCampaigns()
        searchCampaigns.query = query
        searchCampaigns.limit = 5
        searchCampaigns.offset = 0

        val query = searchCampaigns.query
        searchCampaigns.sortOrderList = listOf(SortOrder.getCampaignSortOrder(query, searchCampaigns.status))
        searchCampaigns.campaignTags = campaignService.getCampaignTagsByMnemonic(searchCampaigns.categories?: listOf()).toMutableList()

        val campaigns = searchService.getTopFilteredCampaignsSearchResult(searchCampaigns)

        val productsSearch = ProductsSearch()
        productsSearch.query = query
        productsSearch.limit = 5
        productsSearch.offset = 0

        productsSearch.productTags = productTagService.getTagByMnemonicNameList(productsSearch.productTagsMnemonics ?: listOf())
        val products = searchService.searchForProducts(productsSearch)

        val searchShares = SearchShares()
        searchShares.query = URLDecoder.decode(query, "UTF-8")
        searchShares.limit = 5
        searchShares.offset = 0
        searchShares.sortOrderList = listOf(SortOrder.SORT_BY_PURCHASE_COUNT_DESC)
        val shares = searchService.searchForShares(searchShares)

        val results = ArrayList<SearchResultItemDTO>()

        campaigns.searchResultRecords?.mapTo(results) { it ->
            SearchResultItemDTO(it.imageUrl, it.name, "/campaigns/${it.campaignId}", it.campaignId,null, null)
        }
        val cec = campaigns.estimatedCount
        val pec = products.estimatedCount
        val sec = shares.estimatedCount

        val searchResult = SearchResultsDTO(results, cec, pec, sec)
        return ActionStatus(result = searchResult)
    }
}


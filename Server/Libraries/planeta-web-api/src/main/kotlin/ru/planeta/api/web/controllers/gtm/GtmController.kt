package ru.planeta.api.web.controllers.gtm

import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import ru.planeta.api.model.ActionStatus
import ru.planeta.api.service.campaign.CampaignService
import ru.planeta.api.web.controllers.Urls
import ru.planeta.model.common.campaign.GtmCampaignInfo

/**
 *
 * Created by kostiagn on 22.12.2015.
 */
@RestController
class GtmController(private val campaignService: CampaignService) {

    @GetMapping(Urls.Gtm.GET_CAMPAIGN_LIST_INFO)
    fun getCampaignListInfo(@RequestParam(value = "ids[]") campaignIds: List<Long>): ActionStatus<List<GtmCampaignInfo>> =
            ActionStatus.createSuccessStatus(campaignService.getGtmCampaignInfoList(campaignIds))

}

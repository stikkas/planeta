package ru.planeta.api.web.controllers.advertising

import org.apache.commons.io.IOUtils
import org.apache.commons.lang3.StringUtils
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.GetMapping
import ru.planeta.api.advertising.AdvertisingService
import ru.planeta.api.exceptions.NotFoundException
import ru.planeta.api.web.controllers.Urls
import ru.planeta.model.advertising.AdvertisingDTO
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse


/**
 * Created with IntelliJ IDEA.
 * Date: 09.01.14
 * Time: 19:25
 */
@Controller
class AdvertisingController(private val advertisingService: AdvertisingService) {

    @GetMapping(Urls.GET_BROADCAST_ADVERTISING_XML)
    fun getAdvertisingXml(profileId: Long,
                          broadcastId: Long,
                          request: HttpServletRequest,
                          response: HttpServletResponse) {
        var dto: AdvertisingDTO? = try {
            advertisingService.loadLocalAdvertising(profileId)
        } catch (e: NotFoundException) {
            try {
                advertisingService.loadActiveGlobalAdvertising()
            } catch (ignored: NotFoundException) {
                null
            }

        }

        if (dto != null && StringUtils.isNotEmpty(dto.vastXml)) {
            response.contentType = "text/xml"
            response.setHeader("Access-Control-Allow-Origin", "http://s0.2mdn.net")
            response.setHeader("Access-Control-Allow-Credentials", "true")
            IOUtils.write(dto.vastXml, response.outputStream)
        } else {
            response.sendError(404)
        }
    }

}

package ru.planeta.api.web.controllers

import org.springframework.beans.factory.annotation.Value
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController
import ru.planeta.api.model.ActionStatus
import ru.planeta.commons.web.WebUtils
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

/**
 *
 * Created by a.savanovich on 07.07.2015.
 */
@RestController
class LiveCheckController {
    @Value("\${static.base.url}")
    private lateinit var staticBaseUrl: String

    @GetMapping(Urls.JETTY_VERSION)
    fun jettyVersion(): ActionStatus<*> = ActionStatus.createSuccessStatus(System.getProperty("jetty.version"))

    @GetMapping(Urls.STATIC_VERSION)
    fun staticVersion(): ActionStatus<*> = ActionStatus.createSuccessStatus<String>(staticBaseUrl)

    @GetMapping(Urls.HTTP_CHECK)
    fun httpCheck(): ActionStatus<*> = ActionStatus.createSuccessStatus<Any>()

    @GetMapping(Urls.JETTY_PORT)
    fun getJettyPort(request: HttpServletRequest): ActionStatus<*> = ActionStatus.createSuccessStatus(request.localPort)

    @GetMapping(Urls.LIVE_CHECK)
    fun liveCheck(request: HttpServletRequest, resp: HttpServletResponse): String {
        WebUtils.setDefaultHeaders(resp)
        return "OK"
    }

}

package ru.planeta.eva.api.web.dto

class SearchCitiesDTO {

    var countryId: Long = 0
    var regionId: Long = 0
    var query: String = ""
    var limit: Int = 0
}

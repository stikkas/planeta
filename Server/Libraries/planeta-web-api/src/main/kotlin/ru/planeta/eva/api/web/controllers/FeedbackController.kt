package ru.planeta.eva.api.web.controllers

import org.apache.commons.lang3.StringUtils
import org.springframework.validation.BindingResult
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController
import ru.planeta.api.mail.MailClient
import ru.planeta.api.model.FeedbackMessage
import ru.planeta.api.web.authentication.myProfileId
import ru.planeta.eva.api.web.ActionStatus
import ru.planeta.eva.api.web.Urls
import javax.validation.Valid

@RestController("EvaWebApiFeedbackController")
class FeedbackController (private val mailClient: MailClient) {

    @PostMapping(Urls.FEEDBACK)
    fun feedback(@Valid @RequestBody feedbackMessage: FeedbackMessage, result: BindingResult): ActionStatus {
        if (result.hasErrors()) {
            return ActionStatus(false, result.allErrors)
        }
        if (!StringUtils.isEmpty(feedbackMessage.additionalInfo)) {
            feedbackMessage.message = feedbackMessage.message + feedbackMessage.additionalInfo;
        }
        mailClient.sendFeedbackEmail(feedbackMessage.email, feedbackMessage.message, feedbackMessage.theme, myProfileId())
        return ActionStatus(result = true)

    }
}

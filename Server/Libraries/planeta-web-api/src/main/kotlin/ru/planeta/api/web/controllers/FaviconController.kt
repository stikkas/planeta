package ru.planeta.api.web.controllers

import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.servlet.ModelAndView
import ru.planeta.api.web.controllers.services.BaseControllerService
import ru.planeta.model.enums.ProjectType
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

/**
 * User: a.savanovich
 * Date: 18.05.12
 * Time: 13:39
 */
@Controller
class FaviconController(private val baseControllerService: BaseControllerService) {

    @GetMapping(Urls.MANIFEST)
    fun getManifest(request: HttpServletRequest, response: HttpServletResponse): ModelAndView =
            baseControllerService.createDefaultModelAndView("includes/generated/manifest", ProjectType.MAIN)

    @GetMapping(Urls.BROWSERCONFIG)
    fun getBrowserconfig(request: HttpServletRequest, response: HttpServletResponse): ModelAndView =
            baseControllerService.createDefaultModelAndView("includes/generated/browserconfig-xml", ProjectType.MAIN)
}


package ru.planeta.eva.api.web.validation.validators

import ru.planeta.eva.api.web.validation.annotations.NotBlank
import javax.validation.ConstraintValidator
import javax.validation.ConstraintValidatorContext

class NotBlankValidator : ConstraintValidator<NotBlank, String?> {

    override fun isValid(value: String?, context: ConstraintValidatorContext?) = !value.isNullOrBlank()

    override fun initialize(anno: NotBlank?) {
    }
}

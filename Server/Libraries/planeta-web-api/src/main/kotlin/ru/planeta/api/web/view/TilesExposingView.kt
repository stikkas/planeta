package ru.planeta.api.web.view

import org.springframework.web.context.support.ContextExposingHttpServletRequest
import org.springframework.web.servlet.view.tiles2.TilesView

import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse
import java.util.Arrays
import java.util.HashSet

/**
 * Created with IntelliJ IDEA.
 * User: a.volkov
 * Date: 09.07.13
 * Time: 16:40
 */
class TilesExposingView : TilesView() {

    private var exposeContextBeansAsAttributes = false
    private var exposedContextBeanNames: Set<String>? = null

    override fun setExposeContextBeansAsAttributes(exposeContextBeansAsAttributes: Boolean) {
        this.exposeContextBeansAsAttributes = exposeContextBeansAsAttributes
    }

    override fun setExposedContextBeanNames(exposedContextBeanNames: Array<String>) {
        this.exposedContextBeanNames = HashSet(Arrays.asList(*exposedContextBeanNames))
    }

    override fun getRequestToExpose(originalRequest: HttpServletRequest): HttpServletRequest {
        return if (this.exposeContextBeansAsAttributes || this.exposedContextBeanNames != null) {
            ContextExposingHttpServletRequest(originalRequest, webApplicationContext, this.exposedContextBeanNames)
        } else originalRequest
    }

    override fun renderMergedOutputModel(model: Map<String, Any>, request: HttpServletRequest, response: HttpServletResponse) {
        val requestToExpose = getRequestToExpose(request)
        exposeModelAsRequestAttributes(model, requestToExpose)
        super.renderMergedOutputModel(model, requestToExpose, response)
    }

}


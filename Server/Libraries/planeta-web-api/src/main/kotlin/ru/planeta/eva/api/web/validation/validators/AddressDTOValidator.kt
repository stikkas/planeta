package ru.planeta.eva.api.web.validation.validators

import org.springframework.stereotype.Component
import org.springframework.validation.Errors
import org.springframework.validation.ValidationUtils
import org.springframework.validation.Validator
import ru.planeta.api.utils.ValidateUtils
import ru.planeta.eva.api.web.dto.AddressDTO

@Component
class AddressDTOValidator : Validator {

    override fun supports(clazz: Class<*>): Boolean {
        return AddressDTO::class.java.isAssignableFrom(clazz)
    }

    override fun validate(target: Any?, errors: Errors) {
        ValidateUtils.rejectIfContainsNotValidString(errors, "zipCode", "wrong_chars")
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "zipCode", "field_required")

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "city", "field_required")
        ValidateUtils.rejectIfContainsNotValidString(errors, "city", "wrong_chars")

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "street", "field_required")
        ValidateUtils.rejectIfContainsNotValidString(errors, "street", "wrong_chars")

        val address = target as? AddressDTO
        if (address?.countryId == 0L) {
            errors.rejectValue("countryId", "field_required")
        }
    }
}

package ru.planeta.eva.api.web.validation.validators

import org.springframework.stereotype.Component
import org.springframework.validation.Errors
import ru.planeta.api.service.campaign.DeliveryService
import org.springframework.validation.ValidationUtils
import org.springframework.validation.Validator
import ru.planeta.api.utils.ValidateUtils
import ru.planeta.api.web.authentication.myProfileId
import ru.planeta.eva.api.services.ShareService
import ru.planeta.eva.api.web.models.PaymentData
import ru.planeta.dto.PurchaseShareInfoDTO
import ru.planeta.model.common.campaign.enums.ShareStatus
import ru.planeta.model.common.delivery.SubjectType
import ru.planeta.model.shop.enums.DeliveryType

@Component
class ShareDTOValidator(private val deliveryService: DeliveryService,
                        private val addressValidator: AddressDTOValidator,
                        private val customerContactDTOValidator: CustomerContactDTOValidator,
                        private val shareService: ShareService) : Validator {

    override fun supports(clazz: Class<*>): Boolean {
        return PaymentData::class.java.isAssignableFrom(clazz)
    }

    override fun validate(target: Any, errors: Errors) {
        val purchase = target as PaymentData

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "infoId", "field_required")
        val infoId = purchase.infoId
        if (infoId == 0L) {
            return
        }

        val purchaseInfo: PurchaseShareInfoDTO? = shareService.getPurchaseShareInfo(infoId, myProfileId())

        if (purchaseInfo != null) {
            ValidationUtils.rejectIfEmptyOrWhitespace(errors, "email", "wrong_email")
            ValidateUtils.rejectIfNotEmail(errors, "email", "wrong_email")

            val shareStatus = purchaseInfo.shareDetailed?.shareStatus

            val isInvestingShare = (shareStatus == ShareStatus.INVESTING || shareStatus == ShareStatus.INVESTING_WITHOUT_MODERATION ||
                    shareStatus == ShareStatus.INVESTING_ALL_ALLOWED)

            if (!isInvestingShare) {
                if (purchaseInfo.shareDetailed?.questionToBuyer?.isNotEmpty() == true) {
                    ValidationUtils.rejectIfEmptyOrWhitespace(errors, "reply", "field_required")
                    purchase.reply?.let {
                        if (it.length > REPLY_MAX_LENGTH) {
                            errors.rejectValue("reply", "field_length_max_400")
                        }
                    }
                    ValidateUtils.rejectIfContainsNotValidString(errors, "reply", "wrong_chars")
                }
            }

            if (purchase.paymentMethodId == METHOD_NOT_SET || purchase.paymentMethodId == METHOD_OTHRES) {
                errors.rejectValue("paymentMethodId", "payment_required")
            }

            if (purchase.needPhone) {
                ValidationUtils.rejectIfEmptyOrWhitespace(errors, "paymentPhone", "field_required")
                val phone = purchase.paymentPhone

                if (phone != null && !phone.isEmpty()) {
                    ValidateUtils.rejectIfNotPhoneHuman(errors, "paymentPhone", "wrong_type")
                }
            }

            if (!isInvestingShare) {
                if (purchase.deliveryInfo.serviceId == DELIVERY_NOT_SET) {
                    val shareId = purchaseInfo.shareDetailed?.shareId
                    if (shareId != null) {
                        if (deliveryService.getLinkedDeliveries(shareId, SubjectType.SHARE).any { it.isEnabled }) {
                            errors.pushNestedPath("deliveryInfo")
                            errors.rejectValue("serviceId", "delivery_required")
                            errors.popNestedPath()
                        }
                    }
                    return
                }

                val shareId = purchaseInfo.shareDetailed?.shareId ?: return
                val linkedDeliveryService = deliveryService.getLinkedDelivery(shareId, purchase.deliveryInfo.serviceId, SubjectType.SHARE)
                val serviceType = linkedDeliveryService.serviceType
                if (linkedDeliveryService.isEnabled) {
                    errors.pushNestedPath("deliveryInfo")
                    if (serviceType == DeliveryType.DELIVERY || serviceType == DeliveryType.RUSSIAN_POST) {
                        errors.pushNestedPath("address")
                        addressValidator.validate(purchase.deliveryInfo.address, errors)
                        errors.popNestedPath()
                    }
                    errors.pushNestedPath("customerContacts")
                    customerContactDTOValidator.validate(purchase.deliveryInfo.customerContacts, errors)
                    errors.popNestedPath()
                    errors.popNestedPath()
                }
            }
        } else {
            return
        }
    }

    companion object {
        private const val METHOD_OTHRES = 9L
        private const val METHOD_NOT_SET = 0L
        private const val DELIVERY_NOT_SET = 0L
        private const val REPLY_MAX_LENGTH = 400
    }
}


package ru.planeta.api.web.controllers.profile

import org.springframework.web.bind.annotation.*
import ru.planeta.api.exceptions.PermissionException
import ru.planeta.api.model.ActionStatus
import ru.planeta.api.model.json.ProfileInfo
import ru.planeta.api.news.ProfileNewsService
import ru.planeta.api.service.profile.ProfileLastVisitService
import ru.planeta.api.service.profile.ProfileService
import ru.planeta.api.service.profile.UserService
import ru.planeta.api.service.security.PermissionService
import ru.planeta.api.web.authentication.isAnonymous
import ru.planeta.api.web.authentication.myProfileId
import ru.planeta.api.web.controllers.Urls
import ru.planeta.model.common.ProfileBalance
import ru.planeta.model.enums.ProfileLastVisitType
import ru.planeta.model.profile.Profile
import java.util.*

/**
 * User: atropnikov
 * Date: 16.04.12
 * Time: 14:24
 */
@RestController
class CommonProfileController(private val profileLastVisitService: ProfileLastVisitService,
                              private val profileNewsService: ProfileNewsService,
                              private val profileService: ProfileService,
                              private val userService: UserService) {

    @GetMapping(Urls.PROFILE_LK)
    fun profileBalance(): ProfileBalance = userService.getPersonalCabinet(myProfileId())

    @GetMapping(Urls.PROFILE_GET_TABS_COUNT_INFO)
    fun campaignNewEventsCount(): ActionStatus<Map<String, Any>> {
        val map = HashMap<String, Any>()
        if (!isAnonymous()) {
            map.put("newsCount", profileNewsService.selectCountForProfile(myProfileId(), null, false))
            val lastVisit = profileLastVisitService.selectProfileLastVisit(myProfileId(), ProfileLastVisitType.NEWS_TAB)
            map.put("newNewsCount", profileNewsService.selectCountForProfile(myProfileId(), lastVisit, false))
        }
        return ActionStatus.createSuccessStatus(map)
    }

    @GetMapping(Urls.PROFILE_INFO)
    fun getProfileInfo(@RequestParam profileId: Long): ProfileInfo = profileService.getProfileInfo(myProfileId(), profileId)

    @GetMapping(Urls.PROFILE)
    fun getProfile(@RequestParam(value = "profileId", defaultValue = "-1") alias: String): ProfileInfo =
            profileService.getProfile(myProfileId(), alias)

    @PostMapping(Urls.PROFILE_SAVE_AVATAR)
    fun saveAvatarId(@RequestParam(value = "avatarImageId", defaultValue = "0") imageId: Long,
                     @RequestParam profileId: Long): ActionStatus<Profile> =
            ActionStatus.createSuccessStatus(profileService.setProfileAvatar(myProfileId(), profileId, imageId))

    @PostMapping(Urls.PROFILE_SAVE_SMALL_AVATAR)
    fun saveSmallAvatarId(@RequestParam(value = "avatarImageId", defaultValue = "0") imageId: Long,
                          @RequestParam(required = false) profileId: Long?): ActionStatus<Profile> {
        val myProfileId = myProfileId()
        val profileId = profileId ?: myProfileId
        return ActionStatus.createSuccessStatus(profileService.setProfileSmallAvatar(myProfileId, profileId, imageId))
    }

    @PostMapping(Urls.PROFILE_SET_IS_SHOW_BACKED_CAMPAIGNS)
    fun setIsShowBackedCampaigns(@RequestParam isShowBackedCampaigns: Boolean): ActionStatus<*> {
        if (isAnonymous()) {
            throw PermissionException()
        }
        profileService.setIsShowBackedCampaigns(myProfileId(), isShowBackedCampaigns)
        return ActionStatus.createSuccessStatus<Any>()
    }

    @PostMapping(Urls.PROFILE_LAST_VISIT)
    fun trackProfileLastVisit(@RequestParam profileLastVisitType: ProfileLastVisitType): ActionStatus<*> {
        if (isAnonymous()) {
            throw PermissionException()
        }
        profileLastVisitService.insertOrUpdateProfileLastVisit(myProfileId(), profileLastVisitType)
        return ActionStatus.createSuccessStatus<Any>()
    }
}


package ru.planeta.api.web.controllers.profile

import org.springframework.validation.BindingResult
import org.springframework.web.bind.annotation.*
import ru.planeta.api.exceptions.VideoNotFoundException
import ru.planeta.api.exceptions.VideoPermissionException
import ru.planeta.api.model.ActionStatus
import ru.planeta.api.model.json.VideoInfo
import ru.planeta.api.model.json.VideoInfoForFlash
import ru.planeta.api.service.content.VideoService
import ru.planeta.api.service.profile.ProfileService
import ru.planeta.api.web.authentication.myProfileId
import ru.planeta.api.web.controllers.Urls
import ru.planeta.api.web.controllers.services.BaseControllerService
import ru.planeta.model.profile.media.Video
import javax.validation.Valid

/**
 * Controller for profile's video
 *
 * @author dkolyshev
 */
@RestController
class VideoController(private val videoService: VideoService,
                      private val profileService: ProfileService,
                      private val baseControllerService: BaseControllerService) {


    @GetMapping(Urls.VIDEO)
    fun getVideo(@RequestParam(value = "profileId") alias: String,
                 @RequestParam videoId: Long): VideoInfo {

        // show tv iframe anyway (tv service throws permission and notfound exceptions itsels)
        val profile = profileService.getProfileSafe(alias)
        try {
            return videoService.getVideo(profile.profileId, profile.profileId, videoId)
        } catch (e: VideoPermissionException) {
            return VideoInfo(profile.profileId, videoId)
        } catch (e: VideoNotFoundException) {
            return VideoInfo(profile.profileId, videoId)
        }

    }

    @GetMapping(Urls.VIDEO_EXTENDED)
    fun getVideoExtendedInfo(@RequestParam(value = "profileId") alias: String,
                             @RequestParam videoId: Long): VideoInfoForFlash {

        val profile = profileService.getProfileSafe(alias)
        return videoService.getVideoInfoExtended(myProfileId(), profile.profileId, videoId)
    }

    @PostMapping(Urls.VIDEO)
    fun saveVideo(@Valid video: Video, result: BindingResult): ActionStatus<Video> {
        return if (result.hasErrors()) {
            baseControllerService.createErrorStatus(result)
        } else ActionStatus.createSuccessStatus(videoService.saveVideo(myProfileId(), video))
    }

    @PostMapping(Urls.VIDEO_DELETE)
    fun deleteVideo(@RequestParam profileId: Long, @RequestParam videoId: Long): ActionStatus<*> {
        videoService.deleteVideo(myProfileId(), profileId, videoId)
        return ActionStatus.createSuccessStatus<Any>()
    }


    @PostMapping(Urls.VIDEO_EXTERNAL)
    fun addExternalVideo(@RequestParam profileId: Long,
                         @RequestParam(value = "url") externalUrl: String): ActionStatus<VideoInfo> {
        return ActionStatus.createSuccessStatus(videoService.addExternalVideo(myProfileId(), profileId, externalUrl, null))
    }

    @GetMapping(Urls.VIDEO_EXTERNAL)
    fun getExternalVideoDetails(@RequestParam url: String): VideoInfo {
        return videoService.getExternalVideoDetails(url)
    }
}

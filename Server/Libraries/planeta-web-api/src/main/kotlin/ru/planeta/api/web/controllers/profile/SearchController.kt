package ru.planeta.api.web.controllers.profile

import org.apache.commons.collections4.CollectionUtils
import org.apache.log4j.Logger
import org.springframework.validation.BindingResult
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import ru.planeta.api.exceptions.MessageCode
import ru.planeta.api.exceptions.PermissionException
import ru.planeta.api.model.ActionStatus
import ru.planeta.api.news.ProfileNewsDTO
import ru.planeta.api.search.*
import ru.planeta.api.service.campaign.CampaignService
import ru.planeta.api.service.shop.ProductTagService
import ru.planeta.api.web.authentication.myProfileId
import ru.planeta.api.web.controllers.Urls
import ru.planeta.api.web.dto.TopCampaignDTO
import ru.planeta.dao.profiledb.GeoLocationDAO
import ru.planeta.model.common.campaign.CampaignTag
import ru.planeta.model.enums.PostFilterType
import ru.planeta.model.profile.broadcast.Broadcast
import ru.planeta.model.profile.broadcast.enums.BroadcastCategory
import ru.planeta.model.shop.ProductInfo
import java.net.URLDecoder
import java.util.*
import javax.validation.Valid

/**
 * @author s.fionov
 */
@RestController
class SearchController(private val productTagService: ProductTagService,
                       private val geoLocationDAO: GeoLocationDAO,
                       private val searchService: SearchService,
                       private val campaignService: CampaignService) {

    private val logger = Logger.getLogger(SearchController::class.java)
    /**
     * Searches for users.
     * query, offset and limit are mandatory parameters.
     * Other parameters are optional -- just not set them and we will not use them for searching.
     *
     * @param query     Search query
     * @return JSON object with search result
     */
    @GetMapping(Urls.SEARCH_USERS)
    fun searchForUsers(@RequestParam query: String,
                       @RequestParam(defaultValue = "0") offset: Int,
                       @RequestParam(defaultValue = "20") limit: Int): SearchResult<ProfileSearchResult> {

        var result: SearchResult<ProfileSearchResult> = try {
            searchService.searchForUsers(myProfileId(), query, offset, limit)
        } catch (e: Exception) {
            logger.error("Search failed", e)
            SearchResult()
        }
        return result
    }

    @GetMapping(Urls.SEARCH_POSTS)
    fun searchForNewsPosts(@RequestParam query: String,
                           @RequestParam(defaultValue = "0") offset: Int,
                           @RequestParam(defaultValue = "20") limit: Int,
                           @RequestParam(required = false) profileId: Long?,
                           @RequestParam(defaultValue = "ALL") filter: PostFilterType): SearchResult<ProfileNewsDTO> =
            try {
                searchService.searchForNewsPosts(myProfileId(), query, profileId, filter, offset, limit)
            } catch (e: Exception) {
                logger.error("Search failed", e)
                SearchResult()
            }

    @GetMapping(Urls.SEARCH_BROADCASTS)
    fun searchForBroadcasts(@RequestParam(defaultValue = "") query: String,
                            @RequestParam(defaultValue = "0") offset: Int,
                            @RequestParam(defaultValue = "20") limit: Int,
                            @RequestParam(required = false) profileId: Int?,
                            @RequestParam(defaultValue = "0") sortBy: Int,
                            @RequestParam(required = false) broadcastCategory: BroadcastCategory?,
                            @RequestParam(required = false) filterField: String?): SearchResult<Broadcast> =
            try {
                searchService.searchForBroadcasts(myProfileId(), sortBy, query, profileId, filterField, offset, limit, broadcastCategory
                        ?: BroadcastCategory.ALIVE)
            } catch (e: Exception) {
                logger.error("Search failed", e)
                SearchResult()
            }


    @GetMapping(Urls.SEARCH_PROJECTS)
    fun searchForProjects(@Valid searchCampaigns: SearchCampaigns, bindingResult: BindingResult): SearchResult<TopCampaignDTO> {
        if (bindingResult.hasErrors()) {
            throw PermissionException(MessageCode.FIELD_ERROR)
        }
        val query = searchCampaigns.query
        searchCampaigns.sortOrderList = listOf(SortOrder.getCampaignSortOrder(query, searchCampaigns.status))
        searchCampaigns.campaignTags = campaignService.getCampaignTagsByMnemonic(searchCampaigns.categories
                ?: listOf()).toMutableList()
        if (query == null || query.trim { it <= ' ' }.isEmpty()) {
            searchCampaigns.isDontShowUnsuccessfulProject = java.lang.Boolean.TRUE
        }
        val result = searchService.getTopFilteredCampaignsSearchResult(searchCampaigns)
        val campaigns = result.searchResultRecords
        return SearchResult(result.searchTime, result.estimatedCount, TopCampaignDTO.convert(campaigns))
    }

    @GetMapping(Urls.SEARCH_CHARITY)
    fun searchForCharityProjects(@Valid searchCampaigns: SearchCampaigns, bindingResult: BindingResult): SearchResult<TopCampaignDTO> {
        searchCampaigns.status = CampaignStatusFilter.CHARITY
        return searchForProjects(searchCampaigns, bindingResult)
    }

    @GetMapping(Urls.SEARCH_LOCATION_FILTERS)
    fun searchLocationFilterNames(@Valid searchCampaigns: SearchCampaigns, bindingResult: BindingResult): SearchCampaigns {
        if (bindingResult.hasErrors()) {
            throw PermissionException(MessageCode.FIELD_ERROR)
        }
        if (searchCampaigns.cityId != 0L) {
            val city = geoLocationDAO.selectCity(searchCampaigns.cityId)
            searchCampaigns.cityNameRus = city.name
            searchCampaigns.cityNameEng = city.nameEn
            searchCampaigns.cityId = city.objectId.toLong()
            searchCampaigns.regionNameRus = city.regionNameRus
            searchCampaigns.regionNameEng = city.regionNameEn
            searchCampaigns.regionId = city.regionId.toLong()
            searchCampaigns.countryId = city.countryId.toLong()
        } else if (searchCampaigns.regionId != 0L) {
            val region = geoLocationDAO.selectRegion(searchCampaigns.regionId)
            searchCampaigns.regionNameRus = region.name
            searchCampaigns.regionNameEng = region.nameEn
            searchCampaigns.regionId = region.regionId
            searchCampaigns.countryId = region.countryId.toLong()
        }
        return searchCampaigns
    }

    @GetMapping(Urls.SEARCH_PRODUCTS_JSON_NEW)
    fun searchFilteredProductsNew(productsSearch: ProductsSearch): List<ProductInfo> = searchFilteredProducts(productsSearch).searchResultRecords
            ?: listOf()

    @GetMapping(Urls.SEARCH_PRODUCTS_JSON)
    fun searchFilteredProducts(productsSearch: ProductsSearch): SearchResult<ProductInfo> {
        productsSearch.productTags = productTagService.getTagByMnemonicNameList(productsSearch.productTagsMnemonics
                ?: listOf())
        return searchService.searchForProducts(productsSearch)
    }

    @GetMapping(Urls.Search.SEARCH_SHARES_JSON)
    fun searchShares(searchShares: SearchShares): ActionStatus<SearchResult<*>> {
        searchShares.query?.let {
            if (it.isNotBlank()) {
                searchShares.query = URLDecoder.decode(it, "UTF-8");
            }
        }

        if (searchShares.sortOrderList == null) {
            val sortOrder = SortOrder.SORT_BY_PURCHASE_COUNT_DESC
            searchShares.sortOrderList = listOf(sortOrder)
        }

        if (!CollectionUtils.isEmpty(searchShares.campaignTagsIds)) {
            val list = ArrayList<CampaignTag>()
            searchShares.campaignTagsIds?.forEach { id -> list.add(CampaignTag(id)) }
            searchShares.campaignTags = list
        }

        return ActionStatus.createSuccessStatus(searchService.searchForShares(searchShares))
    }
}

package ru.planeta.api.web.controllers.profile

import org.apache.log4j.Logger
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.*
import ru.planeta.api.exceptions.PermissionException
import ru.planeta.api.model.ActionStatus
import ru.planeta.api.news.LoggerService
import ru.planeta.api.service.profile.ProfileLastVisitService
import ru.planeta.api.service.profile.ProfileService
import ru.planeta.api.service.profile.ProfileSubscriptionService
import ru.planeta.api.service.security.PermissionService
import ru.planeta.api.web.authentication.isAnonymous
import ru.planeta.api.web.authentication.isProfileIdMine
import ru.planeta.api.web.authentication.myProfileId
import ru.planeta.api.web.controllers.Urls
import ru.planeta.model.enums.ProfileLastVisitType
import ru.planeta.model.enums.ProfileType
import ru.planeta.model.news.ProfileNews
import ru.planeta.model.profile.ProfileSubscriptionInfo
import java.util.*

@RestController
class ProfileSubscriptionController(private val profileSubscriptionService: ProfileSubscriptionService,
                                    private val profileLastVisitService: ProfileLastVisitService,
                                    private val permissionService: PermissionService,
                                    private val profileNewsService: LoggerService,
                                    private val profileService: ProfileService) {

    private val logger = Logger.getLogger(ProfileSubscriptionController::class.java)

    private fun amIAdmin(profileId: Long): Boolean {
        return permissionService.isAdmin(myProfileId(), profileId)
    }

    private fun checkAmIAdmin(profileId: Long) {
        permissionService.checkIsAdmin(myProfileId(), profileId)
    }

    @GetMapping(Urls.PROFILE_CHECK_SUBSCRIBE)
    fun checkSubscribe(@RequestParam profileId: Long): ActionStatus<*> {
        if (isAnonymous()) {
            return ActionStatus.createSuccessStatus(false)
        }
        val profile = profileService.getProfileSafe(profileId)
        val checkProfileId = if (profile.profileType == ProfileType.HIDDEN_GROUP) profile.creatorProfileId else profile.profileId
        val isSubscribed = profileSubscriptionService.doISubscribeYou(myProfileId(), checkProfileId)
        return ActionStatus.createSuccessStatus(isSubscribed)
    }

    @RequestMapping(value = Urls.PROFILE_SUBSCRIBE, method = [RequestMethod.POST, RequestMethod.GET])
    fun subscribe(@RequestParam profileId: Long): ActionStatus<*> {
        if (isAnonymous()) {
            throw PermissionException()
        }

        profileSubscriptionService.subscribe(myProfileId(), profileId)
        return ActionStatus.createSuccessStatus<Any>()
    }

    @RequestMapping(value = Urls.PROFILE_UNSUBSCRIBE, method = [RequestMethod.POST, RequestMethod.GET])
    fun unsubscribe(@RequestParam profileId: Long): ActionStatus<*> {
        if (isAnonymous()) {
            throw PermissionException()
        }
        val myProfileId = myProfileId()
        logger.info("Unsubscribe from user, user: $myProfileId profile:$profileId")
        profileSubscriptionService.unsubscribe(myProfileId, profileId)
        return ActionStatus.createSuccessStatus<Any>()
    }

    @GetMapping(Urls.PROFILE_GET_SUBSCRIBER_LIST)
    fun getSubscriberList(@RequestParam profileId: Long,
                          @RequestParam(required = false) query: String?,
                          @RequestParam(required = false) lastViewSubscribersDate: Date?,
                          @RequestParam(required = false) isAdmin: Boolean?,
                          @RequestParam(defaultValue = "0") offset: Int,
                          @RequestParam(defaultValue = "18") limit: Int): ActionStatus<List<ProfileSubscriptionInfo>> {
        if (limit == 0 || limit > 100) {
            throw PermissionException()
        }
        if (isAdmin == true) {
            checkAmIAdmin(profileId)
        }
        if (isProfileIdMine(profileId)) {
            profileLastVisitService.insertOrUpdateProfileLastVisit(profileId, ProfileLastVisitType.SUBSCRIBERS_PAGE)
        }
        return ActionStatus.createSuccessStatus(profileSubscriptionService.getSubscriberList(profileId, query, isAdmin, lastViewSubscribersDate, offset, limit))
    }

    @GetMapping(Urls.PROFILE_GET_SUBSCRIPTION_LIST)
    fun getSubscriptionList(@RequestParam profileId: Long,
                            @RequestParam(required = false) query: String?,
                            @RequestParam(required = false) isAdmin: Boolean?,
                            @RequestParam(defaultValue = "0") offset: Int,
                            @RequestParam(defaultValue = "18") limit: Int): ActionStatus<List<ProfileSubscriptionInfo>> {
        if (isAdmin == true) {
            checkAmIAdmin(profileId)
        }
        return ActionStatus.createSuccessStatus(profileSubscriptionService.getSubscriptionList(profileId, query, isAdmin, offset, limit))
    }

    @GetMapping(Urls.PROFILE_GET_SUBSCRIPTION_INFO)
    fun getSubscriptionInfo(@RequestParam profileId: Long): ActionStatus<Map<String, Any>> {
        val map = HashMap<String, Any>()
        map.put("count", profileSubscriptionService.getSubscriptionsCount(profileId))
        if (amIAdmin(profileId)) {
            map.put("adminsCount", profileSubscriptionService.getAdminSubscriptionsCount(profileId))
        }
        return ActionStatus.createSuccessStatus(map)
    }

    @GetMapping(Urls.PROFILE_GET_SUBSCRIBER_INFO)
    fun getSubscribersInfo(@RequestParam profileId: Long): ActionStatus<Map<String, Any>> {
        val map = HashMap<String, Any>()
        map.put("count", profileSubscriptionService.getSubscribersCount(profileId))
        if (amIAdmin(profileId)) {
            map.put("adminsCount", profileSubscriptionService.getAdminSubscribersCount(profileId))
            val dt = profileLastVisitService.selectProfileLastVisit(profileId, ProfileLastVisitType.SUBSCRIBERS_PAGE)
            map.put("lastViewSubscribersDate", dt ?: Date(0))
        }
        return ActionStatus.createSuccessStatus(map)
    }

    @PostMapping(Urls.PROFILE_SET_ADMIN_SUBSCRIBER)
    fun getSubscribersInfo(@RequestParam profileId: Long,
                           @RequestParam subjectProfileId: Long,
                           @RequestParam isAdmin: Boolean): ActionStatus<*> {
        val myProfileId = myProfileId()
        logger.info("""Toggle admin: actor is $myProfileId,
            profileId is $profileId, subjectProfileId is $subjectProfileId, isAdmin flag is $isAdmin""")
        permissionService.toggleAdminBySubscription(myProfileId, profileId, subjectProfileId, isAdmin)
        profileNewsService.addProfileNews(ProfileNews.Type.ADMIN_SET_ADMIN_SUBSCRIBER, myProfileId, profileId)
        profileNewsService.addProfileNews(ProfileNews.Type.PROFILE_SET_ADMIN_SUBSCRIBER, myProfileId, subjectProfileId)
        return ActionStatus.createSuccessStatus<Any>()
    }
}


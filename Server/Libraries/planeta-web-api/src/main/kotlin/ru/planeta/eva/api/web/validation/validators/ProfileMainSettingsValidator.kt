package ru.planeta.eva.api.web.validation.validators

import org.apache.commons.lang3.StringUtils
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.MessageSource
import org.springframework.stereotype.Component
import org.springframework.validation.Errors
import org.springframework.validation.ValidationUtils
import org.springframework.validation.Validator
import ru.planeta.api.model.json.ProfileGeneralSettings
import ru.planeta.api.service.profile.ProfileService
import ru.planeta.api.utils.ValidateUtils
import ru.planeta.api.web.authentication.myProfileId
import ru.planeta.eva.api.models.ProfileMainSettingsDTO
import java.util.Date

@Component
class ProfileMainSettingsValidator(private val profileService: ProfileService,
                                   private val messageSource: MessageSource) : Validator {

    override fun supports(type: Class<*>): Boolean {
        return type.isAssignableFrom(ProfileMainSettingsDTO::class.java)
    }

    override fun validate(o: Any, errors: Errors) {
        ValidateUtils.rejectIfContainsHtmlTags(errors, "displayName", "contains_wrong_chars")
        ValidateUtils.rejectIfContainsNotValidString(errors, "displayName", "contains_wrong_chars")
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "displayName", "field_required")

        ValidateUtils.rejectIfContainsNotValidStringWysiwyg(errors, "summary", "contains_wrong_chars")

        ValidateUtils.rejectIfSizeIsTooLarge(errors, "summary", 5000, messageSource)

        ValidateUtils.rejectIfNotProfileAlias(errors, "alias", "contains_wrong_chars")
        ValidateUtils.rejectIfNotProfileAliasHasLetter(errors, "alias", "only-latin-ang-digits")
        ValidateUtils.rejectIfReservedWord(errors, "alias", "alias-exist")
        ValidateUtils.rejectIfSizeIsTooLarge(errors, "alias", 64, messageSource)
        ValidateUtils.rejectIfSizeIsTooSmall(errors, "alias", "length-short-4", 4, true)

        ValidateUtils.rejectIfSizeIsTooLarge(errors, "phoneNumber", 32, messageSource)
        val profileGeneralSettings = o as ProfileMainSettingsDTO

        if (StringUtils.isNotBlank(profileGeneralSettings.alias)) {
            var profile = profileService.getProfile(myProfileId())

            if (profile != null && profile.alias != null) {
                if (profile.alias != profileGeneralSettings.alias) {
                    errors.rejectValue("alias", "alias-already-set")
                }
            } else {
                profile = profileService.getProfile(profileGeneralSettings.alias ?: "")
                if (profile != null && profile.profileId != myProfileId()) {
                    errors.rejectValue("alias", "alias-exist")
                }
            }
        }
        if (!ValidateUtils.isValidPhoneWithSpecialCharacters(o.phoneNumber)) {
            errors.rejectValue("phoneNumber", "contains_wrong_chars")
        }

        if (o.userBirthDate != null) {
            val now = Date()
            val years = (now.time - o.userBirthDate!!.time) / 1000 / 60 / 60 / 24 / 365
            if (years < 13) {
                errors.rejectValue("userBirthDate", "too-young")
            }
            if (years > 150) {
                errors.rejectValue("userBirthDate", "too-old")
            }
        }
    }
}

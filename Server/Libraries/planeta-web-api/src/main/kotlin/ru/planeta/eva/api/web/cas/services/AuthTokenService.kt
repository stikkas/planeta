package ru.planeta.eva.api.web.cas.services

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.stereotype.Service
import org.springframework.util.SerializationUtils
import ru.planeta.api.model.authentication.AuthenticatedUserDetails
import ru.planeta.dao.commondb.AuthTokenRepository
import ru.planeta.model.common.AuthToken
import java.util.*

/**
 * Сервис для управления токенами SSO
 */
@Service
class AuthTokenService(private val authTokenRepository: AuthTokenRepository,
                       private val authTokenEncoder: BCryptPasswordEncoder) {

    /**
     * Возвращает строку токена для использования в куке для авторизованных пользователей
     */
    fun createToken(authentication: UsernamePasswordAuthenticationToken, userAgent: String?): String {
        val privateInfo = (authentication.principal as AuthenticatedUserDetails).userAuthorizationInfo.userPrivateInfo
        val email = privateInfo.email ?: ""
        val now = Date()
        //@formatter:off
        val authToken = AuthToken(
                authTokenEncoder.encode("""$email:${privateInfo.password}:${privateInfo.username}:${now.time}:${userAgent ?: "unknown-null"}"""),
                SerializationUtils.serialize(authentication), now)
        //@formatter:on
        authTokenRepository.save(authToken)
        return authToken.token
    }

    /**
     * Возвращает аутентификацию из базы если у пользователя правильный токен и тот же user-agent что и при создании токена
     * в противном случае - null
     */
    fun getAuthentication(token: String, userAgent: String?): UsernamePasswordAuthenticationToken? {
        val authToken = authTokenRepository.findByToken(token) ?: return null
        val authentication = SerializationUtils.deserialize(authToken.authentication) as? UsernamePasswordAuthenticationToken
        return if (authentication == null) {// По какой-то причине в базе находится сломанная запись, удаляем ее
            authTokenRepository.delete(authToken)
            null
        } else {
            val privateInfo = (authentication.principal as AuthenticatedUserDetails).userAuthorizationInfo.userPrivateInfo
            val email = privateInfo.email ?: ""
            //@formatter:off
            if (authTokenEncoder.matches(
                            """$email:${privateInfo.password}:${privateInfo.username}:${authToken.timeAdded?.time}:${userAgent ?: "unknown-null"}""",
                            token)) {
            //@formatter:on
                authentication
            } else { // Вполне возможно кто-то пытается правильный токен использовать с другого userAgent
                null
            }
        }
    }

    /**
     * Удаляет запись токена из базы
     */
    fun removeToken(token: String) {
        authTokenRepository.delete(AuthToken(token))
    }
}


package ru.planeta.api.web.controllers

import org.apache.log4j.Logger
import org.springframework.web.bind.annotation.*
import ru.planeta.api.exceptions.PermissionException
import ru.planeta.api.model.ActionStatus
import ru.planeta.api.service.profile.ProfileService
import ru.planeta.api.service.profile.ProfileSubscriptionService
import ru.planeta.api.web.authentication.isAnonymous
import ru.planeta.api.web.authentication.myProfileId
import ru.planeta.model.enums.ProfileType

/**
 * Date: 24.10.12
 * Time: 10:33
 */

@RestController
class SubscriptionController(private val profileSubscriptionService: ProfileSubscriptionService,
                             private val profileService: ProfileService) {
    private val logger = Logger.getLogger(SubscriptionController::class.java)

    @PostMapping(Urls.TOGGLE_CAMPAIGN_SUBSCRIPTION)
    fun toggleCampaignEventSubscriptions(@RequestParam profileId: Long, @RequestParam isSubscribed: Boolean): ActionStatus<Boolean> {
        var profileId = profileId
        if (isAnonymous()) {
            throw PermissionException()
        }
        val profile = profileService.getProfileSafe(profileId)
        profileId = if (profile.profileType == ProfileType.HIDDEN_GROUP) profile.creatorProfileId else profile.profileId
        val clientId = myProfileId()
        if (isSubscribed) {
            logger.info("Unsubscribe from campaign, user: $clientId profile:$profileId")
            profileSubscriptionService.unsubscribe(clientId, profileId)
            return ActionStatus.createSuccessStatus(false)
        } else {
            profileSubscriptionService.subscribe(clientId, profileId)
            return ActionStatus.createSuccessStatus(true)
        }
    }
}

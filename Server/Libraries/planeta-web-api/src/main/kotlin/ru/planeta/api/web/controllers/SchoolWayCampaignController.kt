package ru.planeta.api.web.controllers

import org.apache.commons.lang3.StringUtils
import org.apache.log4j.Logger
import org.springframework.security.web.authentication.AuthenticationSuccessHandler
import org.springframework.validation.BindingResult
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RestController
import ru.planeta.api.exceptions.MessageCode
import ru.planeta.api.exceptions.PermissionException
import ru.planeta.api.exceptions.RegistrationException
import ru.planeta.api.model.ActionStatus
import ru.planeta.api.model.json.Registration
import ru.planeta.api.service.interactive.InteractiveEditorDataService
import ru.planeta.api.service.profile.AuthorizationService
import ru.planeta.api.service.registration.RegistrationService
import ru.planeta.api.service.security.PermissionService
import ru.planeta.api.utils.ValidateUtils
import ru.planeta.api.web.authentication.getAuthentication
import ru.planeta.api.web.authentication.isAnonymous
import ru.planeta.api.web.authentication.myProfileId
import ru.planeta.api.web.authentication.userAuthorizationInfo
import ru.planeta.api.web.controllers.services.BaseControllerService
import ru.planeta.api.web.controllers.services.ControllerAutoLoginWrapService
import ru.planeta.eva.api.web.cas.handlers.CustomAuthenticationSuccessHandler
import ru.planeta.model.enums.ProjectType
import ru.planeta.model.stat.RefererStatType
import ru.planeta.model.trashcan.InteractiveEditorData
import java.net.URLDecoder
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse
import javax.validation.Valid

@RestController
class SchoolWayCampaignController(private val permissionService: PermissionService,
                                  private val registrationService: RegistrationService,
                                  private val interactiveEditorDataService: InteractiveEditorDataService,
                                  private val authorizationService: AuthorizationService,
                                  private val autoLoginWrapService: ControllerAutoLoginWrapService,
                                  private val authenticationSuccessHandler: CustomAuthenticationSuccessHandler,
                                  private val baseControllerService: BaseControllerService) {

    private val logger = Logger.getLogger(SchoolWayCampaignController::class.java)

    @PostMapping(Urls.SchoolWayCampaign.CREATE_USER)
    fun createUser(@Valid registration: Registration,
                   bindingResult: BindingResult,
                   request: HttpServletRequest, response: HttpServletResponse): ActionStatus<Long> {
        if (bindingResult.hasErrors()) {
            return baseControllerService.createErrorStatus(bindingResult)
        }

        val myProfileId = myProfileId()
        if (StringUtils.isBlank(registration.email)) {
            return if (permissionService.isAnonymous(myProfileId)) {
                throw RegistrationException(MessageCode.WRONG_EMAIL)
            } else {
                ActionStatus.createSuccessStatus(myProfileId)
            }
        }
        val userPrivateInfoByEmail = authorizationService.getUserPrivateInfoByUsername(registration.email ?: "")

        if (permissionService.isAnonymous(myProfileId)) {
            if (userPrivateInfoByEmail == null) {
                if (isValidEmail(registration.email ?: "")) {
                    val newUserId = createNewUserAndGetUserIdAndLogin(registration.email ?: "", registration.registrationData.firstName, request, response)
                    setReachedStepNumInteractiveEditorData(3)
                    return ActionStatus.createSuccessStatus(newUserId)
                } else {
                    throw RegistrationException(MessageCode.WRONG_EMAIL)
                }
            }
        } else if (userPrivateInfoByEmail == null) {
            val userInfo = registrationService.addEmailToExternalUser(registration.email, userAuthorizationInfo())
            if (userInfo != null) {
                return ActionStatus.createSuccessStatus(userInfo.profile.profileId)
            }
        } else if (userPrivateInfoByEmail.userId == myProfileId) {
            return ActionStatus.createSuccessStatus(myProfileId)
        }
        throw PermissionException(MessageCode.REGISTRATION_CURRENT_EMAIL_ALREADY_EXISTS)
    }

    protected fun createNewUserAndGetUserIdAndLogin(email: String, displayName: String, request: HttpServletRequest, response: HttpServletResponse): Long {
        try {
            val userAuthorizationInfo = registrationService.autoRegisterByEmail(email, displayName,
                    baseControllerService.getRequestStat(request, response, RefererStatType.REGISTRATION, ProjectType.MAIN))
            if (userAuthorizationInfo.profile != null) {
                autoLoginWrapService.autoLoginUser(email, request)
                authenticationSuccessHandler.onAuthenticationSuccess(request, response, getAuthentication())
                return userAuthorizationInfo.profile.profileId
            } else {
                logger.warn("Profile null after autoregistration " + email)
                throw RegistrationException(MessageCode.UNEXPECTED_EXCEPTION)
            }
        } catch (ex: IllegalArgumentException) {
            logger.error(ex)
            throw RegistrationException(MessageCode.WRONG_EMAIL)
        }

    }

    private fun setReachedStepNumInteractiveEditorData(reachedStepNum: Int) {
        if (isAnonymous()) {
            return
        }
        var interactiveEditorData: InteractiveEditorData? = interactiveEditorDataService.getByUserId(myProfileId())
        if (interactiveEditorData == null) {
            interactiveEditorData = InteractiveEditorData()
            interactiveEditorData.userId = myProfileId()
        }

        if (reachedStepNum > interactiveEditorData.reachedStepNum) {
            interactiveEditorData.reachedStepNum = reachedStepNum
            interactiveEditorData.setShowFormsSettingsBaseOnReachedStepNum(reachedStepNum)
        }

        if (interactiveEditorData.dataId > 0) {
            interactiveEditorDataService.update(interactiveEditorData)
        } else {
            interactiveEditorDataService.insert(interactiveEditorData)
        }
    }

    companion object {

        protected fun isValidEmail(email: String): Boolean {
            var email = email
            var result = ValidateUtils.isValidEmail(email)
            if (!result) {
                email = URLDecoder.decode(email, "UTF-8")
                result = ValidateUtils.isValidEmail(email)
            }

            return result
        }
    }
}

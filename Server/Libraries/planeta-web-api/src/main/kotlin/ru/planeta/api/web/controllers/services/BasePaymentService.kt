package ru.planeta.api.web.controllers.services

import ru.planeta.model.common.Order
import ru.planeta.model.common.TopayTransaction

/**
 * Every application to use BasePaymentController should implement Bean of BasePaymentService
 */
interface BasePaymentService {

    fun getPaymentSuccessActionName(order: Order?): String

    fun getPaymentSourceUrl(transaction: TopayTransaction): String?
}

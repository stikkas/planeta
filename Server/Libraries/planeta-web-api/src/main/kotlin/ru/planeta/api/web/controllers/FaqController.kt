package ru.planeta.api.web.controllers


import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.*
import ru.planeta.api.exceptions.PermissionException
import ru.planeta.api.model.ActionStatus
import ru.planeta.api.service.faq.FaqArticleService
import ru.planeta.api.service.faq.FaqCategoryService
import ru.planeta.api.service.faq.FaqParagraphService
import ru.planeta.api.service.security.PermissionService
import ru.planeta.api.web.authentication.myProfileId
import ru.planeta.dao.commondb.FaqDAO
import ru.planeta.model.common.faq.FaqSnippet
import ru.planeta.model.commondb.FaqArticle
import ru.planeta.model.commondb.FaqCategory
import ru.planeta.model.commondb.FaqParagraph
import java.util.*

/**
 * @author s.kalmykov
 * @since 28.08.2014
 */
@RestController
class FaqController(private val faqArticleService: FaqArticleService,
                    private val faqCategoryService: FaqCategoryService,
                    private val faqParagraphService: FaqParagraphService,
                    private val faqDAO: FaqDAO,
                    private val permissionService: PermissionService) {

    @RequestMapping(Urls.Faq.FAQ_CATEGORY_LIST)
    fun faqCategoryList(): List<FaqCategory> = faqCategoryService.selectFaqCategoryList(0, 0)


    @RequestMapping(Urls.Faq.SEARCH)
    fun search(@RequestParam query: String): List<FaqSnippet> {
        return faqDAO.search(query, 0, 1000)
    }

    @RequestMapping(Urls.Faq.FAQ_ARTICLE_LIST)
    fun getFaqArticleList(@RequestParam(value = "faqCategoryId") faqCategoryId: Long): List<FaqArticle> {
        return faqArticleService.selectFaqArticleListByFaqCategory(faqCategoryId, 0, 0)
    }

    @RequestMapping(Urls.Faq.FAQ_PARAGRAPH_LIST_WITH_ARTICLE)
    fun getFaqParagraphListWithArticle(@RequestParam(value = "faqArticleId") faqArticleId: Long): Map<String, Any> {
        val faqParagraphList = faqParagraphService.selectFaqParagraphListByFaqArticle(faqArticleId, 0, 0)
        val faqArticle = faqArticleService.selectFaqArticle(faqArticleId)
        return object : HashMap<String, Any>() {
            init {
                put("faqParagraphList", faqParagraphList)
                put("faqArticle", faqArticle)
            }
        }
    }

    @RequestMapping(Urls.Faq.FAQ_PARAGRAPH_LIST)
    fun getFaqParagraphList(@RequestParam(value = "faqArticleId") faqArticleId: Long): List<FaqParagraph> {
        return faqParagraphService.selectFaqParagraphListByFaqArticle(faqArticleId, 0, 0)
    }

    @PostMapping(Urls.Faq.FAQ_EDIT_EDIT_ARTICLE)
    fun faqEditEditArticle(@RequestParam faqArticleId: Long,
                           @RequestParam faqCategoryId: Long,
                           faqArticle: FaqArticle): ActionStatus<*> {
        permissionService.checkAdministrativeRole(myProfileId())
        if (faqArticleId == 0L) {
            faqArticleService.insertFaqArticle(faqArticle)
        } else {
            faqArticleService.updateFaqArticle(faqArticle)
        }
        return ActionStatus.createSuccessStatus(faqArticle)
    }


    @PostMapping(Urls.Faq.FAQ_EDIT_RESORT_ARTICLE)
    fun faqEditResortArticle(@RequestParam idFrom: Int,
                             @RequestParam idTo: Int,
                             @RequestParam faqCategoryId: Long): ActionStatus<*> {
        permissionService.checkAdministrativeRole(myProfileId())
        faqArticleService.resort(idFrom.toLong(), idTo.toLong(), faqCategoryId)
        return ActionStatus.createSuccessStatus<Any>()
    }

    @PostMapping(Urls.Faq.FAQ_EDIT_DELETE_ARTICLE)
    fun faqEditDeleteArticle(@RequestParam(value = "faqArticleId") faqArticleId: Long): ActionStatus<*> {
        permissionService.checkAdministrativeRole(myProfileId())
        faqArticleService.deleteFaqArticle(faqArticleId)
        return ActionStatus.createSuccessStatus<Any>()
    }

    @PostMapping(Urls.Faq.FAQ_EDIT_EDIT_PARAGRAPH)
    fun faqEditEditParagraph(@RequestParam faqArticleId: Long,
                             @RequestParam faqParagraphId: Long, faqParagraph: FaqParagraph): ActionStatus<*> {
        permissionService.checkAdministrativeRole(myProfileId())
        if (faqParagraphId == 0L) {
            faqParagraphService.insertFaqParagraph(faqParagraph)
        } else {
            faqParagraphService.updateFaqParagraph(faqParagraph)
        }
        return ActionStatus.createSuccessStatus(faqParagraph)
    }


    @PostMapping(Urls.Faq.FAQ_EDIT_RESORT_PARAGRAPH)
    fun faqEditResortParagraph(@RequestParam idFrom: Int,
                               @RequestParam idTo: Int,
                               @RequestParam faqArticleId: Long): ActionStatus<*> {
        permissionService.checkAdministrativeRole(myProfileId())
        faqParagraphService.resort(idFrom.toLong(), idTo.toLong(), faqArticleId)
        return ActionStatus.createSuccessStatus<Any>()
    }

    @PostMapping(Urls.Faq.FAQ_EDIT_DELETE_PARAGRAPH)
    fun faqEditDeleteParagraph(@RequestParam faqParagraphId: Long): ActionStatus<*> {
        permissionService.checkAdministrativeRole(myProfileId())
        faqParagraphService.deleteFaqParagraph(faqParagraphId)
        return ActionStatus.createSuccessStatus<Any>()
    }
}

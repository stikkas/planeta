package ru.planeta.eva.api.web.dto

class CustomerContactDTO {
    var phone: String? = null
    var customerName: String? = null
}


package ru.planeta.api.web.controllers

import org.springframework.context.MessageSource
import org.springframework.validation.BindingResult
import org.springframework.web.bind.annotation.*
import ru.planeta.api.model.ActionStatus
import ru.planeta.api.service.profile.ConfirmationService
import ru.planeta.api.web.authentication.myProfileId
import ru.planeta.model.profile.Phone
import javax.servlet.http.HttpServletResponse
import javax.validation.Valid

/**
 * Date: 16.07.12
 *
 * @author s.kalmykov
 */
@RestController
class ConfirmationController(private val confirmationService: ConfirmationService,
                             private val messageSource: MessageSource) {

    @PostMapping(Urls.CONFIRM)
    fun confirm(@RequestParam(value = "code") secureCode: String,
                @RequestParam confirmationId: String,
                response: HttpServletResponse): ConfirmationService.Status =
            confirmationService.confirm(confirmationId, secureCode)

    @GetMapping(Urls.CONFIRMATION_RESEND)
    fun reSend(@RequestParam confirmationId: String,
               response: HttpServletResponse): ActionStatus<String> =
            ActionStatus.createSuccessStatus(confirmationService.reSendCode(confirmationId))

    @PostMapping(Urls.CONFIRM_PHONE)
    fun confirmPhone(@Valid @RequestBody phone: Phone, result: BindingResult): ActionStatus<*> {
        if (result.hasErrors()) {
            return ActionStatus.createErrorStatus<Any>(result, messageSource)
        }

        val phoneNumber = phone.normalizedPhone
        return if (confirmationService.createPhoneConfirmation(myProfileId(), phoneNumber, null) == null) {
            ActionStatus.createErrorStatus<Any>("wrong.userNotificationSettings.phoneCode.attempts", messageSource)
        } else ActionStatus.createSuccessStatus<Any>()
    }

}


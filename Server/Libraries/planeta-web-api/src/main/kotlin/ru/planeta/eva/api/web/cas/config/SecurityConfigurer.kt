package ru.planeta.eva.api.web.cas.config

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.builders.WebSecurity
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter
import org.springframework.security.config.annotation.web.configurers.ExpressionUrlAuthorizationConfigurer
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.security.web.authentication.AuthenticationFailureHandler
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter
import ru.planeta.api.helper.ProjectService
import ru.planeta.api.service.authentication.AuthenticatedUserDetailsService
import ru.planeta.eva.api.web.cas.handlers.CustomAuthenticationFailureHandler
import ru.planeta.eva.api.web.cas.handlers.CustomAuthenticationSuccessHandler
import ru.planeta.eva.api.web.cas.handlers.CustomLogoutSuccessHandler
import ru.planeta.eva.api.web.cas.filter.SsoFilter
import ru.planeta.api.web.filter.logging.VisitorFilter

@EnableWebSecurity
@EnableGlobalMethodSecurity
class SecurityConfigurer : WebSecurityConfigurerAdapter() {

    @Value("\${app.ignoring.urls:}")
    lateinit var urls: Array<String>

    @Value("\${auth.cookie.domain:}")
    lateinit var authCookieDomain: String

    @Value("\${app.member.urls:}")
    lateinit var memberUrls: Array<String>

    @Value("\${app.admin.urls:}")
    lateinit var adminUrls: Array<String>

    @Value("\${app.anonymous.urls:}")
    lateinit var anonymousUrls: Array<String>

    @Value("\${app.admin.manager.urls:}")
    lateinit var adminOrManagerUrls: Array<String>

    // Отключаем единый вход на локальной машине
    @Value("\${sso.enabled:true}")
    var ssoEnabled: Boolean = true

    // Названия токена аутентификации
    @Value("\${app.auth.token:oapl}")
    lateinit var authToken: String

    // Доступ по умолчанию, если надо для какого-то приложения, как в админке, запретить,
    // то в application.properties надо прописать false для этого свойства
    // также надо не забыть указать кому доступны все урлы не вошедшие в предыдущие правила "/**"
    @Value("\${default.allow.access:true}")
    var defaultAccess: Boolean = true

    @Autowired
    lateinit var projectService: ProjectService

    override fun configure(web: WebSecurity) {
        web.ignoring().antMatchers(*urls)
    }

    override fun configure(http: HttpSecurity) {
        // @formatter:off
        val configurer = http.authorizeRequests()
        addAccessRoles(configurer, anonymousUrls, "MEMBER", "ANONYMOUS")
        addAccessRoles(configurer, memberUrls, "MEMBER")
        addAccessRoles(configurer, adminOrManagerUrls, "ADMIN", "MANAGER")
        addAccessRoles(configurer, adminUrls, "ADMIN")
        if (defaultAccess) {
            configurer.antMatchers("/**").permitAll()
        }
        val abstractConfigurer = configurer.and()
            .logout()
                .logoutSuccessHandler(logoutSuccessHandler())
                .clearAuthentication(true)
                .invalidateHttpSession(true)
            .and()
            .headers().disable()
            .csrf().disable()
            .addFilterAfter(visitorFilter(), BasicAuthenticationFilter::class.java)
        // @formatter:on
        if (ssoEnabled) {
            abstractConfigurer.addFilterBefore(ssoFilter(), BasicAuthenticationFilter::class.java)
        }
    }

    private fun addAccessRoles(configurer: ExpressionUrlAuthorizationConfigurer<HttpSecurity>.ExpressionInterceptUrlRegistry,
                               urls: Array<String>, vararg roles: String) {
        if (!urls.isEmpty()) {
            if (roles.size > 1) {
                configurer.antMatchers(*urls).hasAnyRole(*roles)
            } else {
                configurer.antMatchers(*urls).hasRole(roles[0])
            }
        }
    }

    @Bean
    fun authenticationFailureHandler(): AuthenticationFailureHandler {
        val handler = CustomAuthenticationFailureHandler()
        handler.setDefaultFailureUrl("/")
        return handler
    }

    @Bean
    fun authenticationSuccessHandler() = CustomAuthenticationSuccessHandler(authCookieDomain, authToken)

    @Bean
    fun userService() = AuthenticatedUserDetailsService()

    @Bean
    fun logoutSuccessHandler() = CustomLogoutSuccessHandler(authCookieDomain, authToken)

    @Bean
    fun visitorFilter() = VisitorFilter(projectService)

    @Bean
    fun ssoFilter() = SsoFilter(authToken)

    @Bean
    fun authTokenEncoder() = BCryptPasswordEncoder()
}


package ru.planeta.api.web.controllers

/**
 * @author Andrew.Arefyev@gmail.com
 * 10.01.14 21:23
 */
interface IAction {
    val path: String

    val actionName: String
}

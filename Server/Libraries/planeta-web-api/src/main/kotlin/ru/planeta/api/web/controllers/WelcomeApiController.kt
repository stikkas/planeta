package ru.planeta.api.web.controllers

import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import ru.planeta.api.exceptions.PermissionException
import ru.planeta.api.model.ActionStatus
import ru.planeta.api.search.CampaignStatusFilter
import ru.planeta.api.search.SearchCampaigns
import ru.planeta.api.search.SearchService
import ru.planeta.api.search.SortOrder
import ru.planeta.api.service.campaign.CampaignService
import ru.planeta.api.service.configurations.ConfigurationService
import ru.planeta.api.web.authentication.myProfileId
import ru.planeta.api.web.dto.TopCampaignDTO
import ru.planeta.model.common.campaign.Campaign
import ru.planeta.model.common.campaign.CampaignTag
import ru.planeta.model.common.campaign.Sponsor

/**
 * User: sshendyapin
 * Date: 21.10.13
 * Time: 11:30
 */
@RestController
class WelcomeApiController(private val searchService: SearchService,
                           private val campaignService: CampaignService,
                           private val configurationService: ConfigurationService) {

    @GetMapping(Urls.WELCOME_GET_CHARITY_CAMPAIGNS)
    fun charityCampaigns(): List<TopCampaignDTO> {
        val campaigns = searchService.getTopFilteredCampaignsSearchResult(
                SearchCampaigns(listOf(SortOrder.SORT_BY_MEDIAN_PRICE), "", null,
                        CampaignStatusFilter.CHARITY, null,
                        null, null, null, null,
                        null, null, false,
                        0, 12)).searchResultRecords
        return TopCampaignDTO.convert(campaigns)
    }

    @GetMapping(Urls.WELCOME_GET_CAMPAIGN_TAGS)
    fun campaignCategoryTypes(): List<CampaignTag> = campaignService.allTags

    @GetMapping(Urls.WELCOME_GET_DRAFT_CAMPAIGNS_LIST)
    fun draftCampaigns(): List<Campaign> = campaignService.getMyDraftCampaigns(myProfileId())

    @GetMapping(Urls.WELCOME_GET_LOW_BANNER_HTML)
    fun lowBannerHtml(): ActionStatus<List<String>> = ActionStatus.createSuccessStatus(configurationService.campaignWelcomeLowBanner)

    @GetMapping(Urls.WELCOME_GET_SPONSORS)
    fun getSponsors(@RequestParam(value = "alias") aliasList: List<String>): ActionStatus<List<Sponsor>> =
            ActionStatus.createSuccessStatus(campaignService.getSponsors(aliasList))

    @GetMapping(Urls.WELCOME_GET_DASHBOARD_FILTERED_CAMPAIGNS)
    fun getCampaignsForDashboard(@RequestParam(value = "status", defaultValue = "ACTIVE") statusFilter: CampaignStatusFilter,
                                 @RequestParam(defaultValue = "0") offset: Int,
                                 @RequestParam(defaultValue = "20") limit: Int): List<TopCampaignDTO> {
        var offset = offset
        var sortOrder = SortOrder.SORT_DEFAULT
        when (statusFilter) {
            CampaignStatusFilter.RECOMMENDED -> {
                sortOrder = SortOrder.SORT_BY_MEDIAN_PRICE
                offset += limit     // ugly hack
            }
            CampaignStatusFilter.ACTIVE -> sortOrder = SortOrder.SORT_BY_MEDIAN_PRICE
        }
        if (limit > 100) {
            throw PermissionException()
        }
        val searchResult = searchService.getTopFilteredCampaignsSearchResult(SearchCampaigns(listOf(sortOrder),
                "", null, statusFilter, null, null,
                null, null, null, null, null,
                true, offset, limit))
        val campaigns = searchResult.searchResultRecords
        return TopCampaignDTO.convert(campaigns)
    }

}

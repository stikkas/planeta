package ru.planeta.eva.api.web.dto

class AddressDTO {
    var addressId: Long? = null
    var street: String? = null
    var city: String? = null
    var zipCode: String? = null
    var countryId: Long = 0
}

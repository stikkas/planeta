package ru.planeta.api.web.controllers.profile

import org.springframework.context.MessageSource
import org.springframework.web.bind.annotation.*
import ru.planeta.api.exceptions.NotFoundException
import ru.planeta.api.exceptions.PermissionException
import ru.planeta.api.helper.ProjectService
import ru.planeta.api.model.ActionStatus
import ru.planeta.api.service.profile.AuthorizationService
import ru.planeta.api.service.registration.RegistrationService
import ru.planeta.api.utils.ValidateUtils
import ru.planeta.api.web.authentication.myProfileId
import ru.planeta.api.web.controllers.Urls
import ru.planeta.model.enums.ProjectType

/**
 * User: m.shulepov
 * Date: 12.04.12
 * Time: 19:10
 */

@RestController
class BaseRegistrationController(private val registrationService: RegistrationService,
                                 private val authorizationService: AuthorizationService,
                                 private val messageSource: MessageSource,
                                 private val projectService: ProjectService) {

    /**
     * DON'T MOVE THIS METHOD TO PLANETA-WEB
     *
     * @throws NotFoundException
     * @throws PermissionException
     */
    @GetMapping(Urls.RESEND_REGISTRATION_COMPLETE_EMAIL)
    fun sendRegistrationCompleteEmail(): ActionStatus<*> {
        registrationService.resendRegistrationCompleteEmail(myProfileId())
        return ActionStatus.createSuccessStatus<Any>()
    }

    @PostMapping(Urls.RECOVER_PASSWORD)
    fun recover(@RequestParam email: String,
                @RequestParam(defaultValue = "") redirectUrl: String): ActionStatus<*> {
        var email = email
        try {
            if (!isValidEmail(email)) {
                return ActionStatus.createErrorStatus<Any>("wrong.email", messageSource)
            }

            email = email.toLowerCase()
            val userPrivateInfo = authorizationService.getUserPrivateInfoByUsername(email) ?:
                    return ActionStatus.createErrorStatus<Any>("passwordRecovery.current.email.not.exists", messageSource)
            registrationService.recoverPassword(email, projectService.getUrl(ProjectType.MAIN), redirectUrl)
        } catch (e: Exception) {
            return ActionStatus.createErrorStatus<Any>(e.message, messageSource)
        }

        return ActionStatus.createSuccessStatus<Any>()
    }

    private fun isValidEmail(email: String?): Boolean {
        return email != null && ValidateUtils.isValidEmail(email)
    }

}


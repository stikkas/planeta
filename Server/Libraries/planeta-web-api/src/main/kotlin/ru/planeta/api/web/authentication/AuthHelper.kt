@file:JvmName("AuthHelper")

package ru.planeta.api.web.authentication

import org.springframework.security.core.Authentication
import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.context.SecurityContextHolder
import ru.planeta.api.model.UserAuthorizationInfo
import ru.planeta.api.model.authentication.AuthenticatedUserDetails
import ru.planeta.api.model.authentication.Authority
import ru.planeta.api.service.security.PermissionService
import ru.planeta.entity.Profile
import javax.servlet.http.Cookie
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

fun getAuthentication(): Authentication? = SecurityContextHolder.getContext().authentication

fun hasAuthority(authority: GrantedAuthority): Boolean = getAuthentication()?.authorities?.contains(authority) == true

fun userAuthorizationInfo(): UserAuthorizationInfo? =
       (getAuthentication()?.principal as? AuthenticatedUserDetails)?.userAuthorizationInfo


/**
 * Обновляет в памяти данные профиля
 */
fun updateCurrentProfile(profile: Profile) {
    val _profile = ru.planeta.model.profile.Profile()

    _profile.profileId = profile.profileId
    _profile.creatorProfileId = profile.creatorProfileId
    _profile.profileType = profile.profileType
    _profile.imageUrl = profile.imageUrl
    _profile.imageId = profile.imageId
    _profile.smallImageUrl = profile.smallImageUrl
    _profile.smallImageId = profile.smallImageId
    _profile.alias = profile.alias
    _profile.status = profile.status
    _profile.cityId = profile.cityId
    _profile.countryId = profile.countryId
    _profile.userBirthDate = profile.userBirthDate
    _profile.userGender = profile.userGender
    _profile.usersCount = profile.usersCount
    _profile.displayName = profile.displayName
    _profile.summary = profile.summary
    _profile.isVip = profile.vip
    _profile.vipObtainDate = profile.vipObtainDate
    _profile.isReceiveNewsletters = profile.isReceiveNewsletters
    _profile.isShowBackedCampaigns = profile.isShowBackedCampaigns
    _profile.backedCount = profile.backedCount
    _profile.projectsCount = profile.projectsCount
    _profile.authorProjectsCount = profile.authorProjectsCount
    _profile.subscribersCount = profile.subscribersCount
    _profile.newSubscribersCount = profile.newSubscribersCount
    _profile.subscriptionsCount = profile.subscriptionsCount
    _profile.isReceiveMyCampaignNewsletters = profile.receiveMyCampaignNewsletters
    _profile.phoneNumber = profile.phoneNumber

    (getAuthentication()?.principal as? AuthenticatedUserDetails)?.userAuthorizationInfo!!.profile = _profile
}

fun isAuthorized(): Boolean = userAuthorizationInfo() != null

// TODO проверить это утверждение, есть подозрение что у анонима тоже есть AuthorizationInfo
fun isAnonymous(): Boolean = !isAuthorized()

fun myProfileId(): Long = userAuthorizationInfo()?.profile?.profileId ?: PermissionService.ANONYMOUS_USER_PROFILE_ID

/**
 * Добавляет cookie к ответу для основного домена и всех его поддоменов
 */
fun addCookie(request: HttpServletRequest, response: HttpServletResponse, name: String, value: String? = null,
              domain: String? = null,
              maxAge: Int = Integer.MAX_VALUE) {
    val cookie = Cookie(name, value)
    cookie.maxAge = maxAge
    cookie.path = "/"
    var cookieDomain: String = if (domain.isNullOrEmpty()) request.serverName else domain!!
    if (cookieDomain[0] != '.') {
        cookieDomain = '.' + cookieDomain
    }
    cookie.domain = cookieDomain
    response.addCookie(cookie)
}

fun isProfileIdMine(profileId: Long): Boolean = !isAnonymous() && myProfileId() == profileId

fun hasEmail(): Boolean {
    val userPrivateInfo = userAuthorizationInfo()?.userPrivateInfo
    return userPrivateInfo != null && !userPrivateInfo.email.isNullOrBlank()
}

fun isAdmin(): Boolean = hasAuthority(Authority.ADMIN) || hasAuthority(Authority.SUPER_ADMIN)




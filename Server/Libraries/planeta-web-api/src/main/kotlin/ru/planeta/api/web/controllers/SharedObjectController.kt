package ru.planeta.api.web.controllers

import org.apache.commons.lang3.StringUtils
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.MessageSource
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.*
import org.springframework.web.servlet.ModelAndView
import ru.planeta.api.exceptions.MessageCode
import ru.planeta.api.exceptions.PermissionException
import ru.planeta.api.helper.ProjectService
import ru.planeta.api.model.ActionStatus
import ru.planeta.api.service.common.CustomMetaTagService
import ru.planeta.api.service.configurations.StaticNodesService
import ru.planeta.api.service.profile.AuthorizationService
import ru.planeta.api.service.profile.ProfileService
import ru.planeta.api.service.profile.UserService
import ru.planeta.api.service.social.SharedObjectService
import ru.planeta.api.web.authentication.myProfileId
import ru.planeta.api.web.controllers.services.BaseControllerService
import ru.planeta.commons.web.CookieUtils
import ru.planeta.mail.MailService
import ru.planeta.model.enums.ProjectType
import ru.planeta.model.stat.SharedObject
import java.io.UnsupportedEncodingException
import java.net.URLEncoder
import java.util.*
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

/**
 * Controller for shared objects
 * User: m.shulepov
 */
@Controller
class SharedObjectController(private val sharedObjectService: SharedObjectService,
                             private val baseControllerService: BaseControllerService) {

    private val cookieDomain: String = ".${baseControllerService.projectService.getHost(ProjectType.MAIN)}"

    @GetMapping(Urls.SHARE_URL)
    fun redirectToShareService(sharedObject: SharedObject?,
                               @RequestParam(required = false) title: String?,
                               request: HttpServletRequest, response: HttpServletResponse): ModelAndView {
        if (sharedObject == null) {
            throw PermissionException(MessageCode.SHARED_OBJECT_NOT_PRESENT)
        }

        val serviceRedirectUrl = createSharedServiceRedirectUrl(sharedObject, title)

        var cookieValue: String? = CookieUtils.getCookieValue(request.cookies, PLANETA_UID_COOKIE, null)
        if (cookieValue == null) {
            cookieValue = generateUUID()
            CookieUtils.setCookie(response, PLANETA_UID_COOKIE, cookieValue, cookieDomain)
        }

        sharedObject.authorUUID = cookieValue
        sharedObjectService.processSharedObject(myProfileId(), cookieValue, sharedObject)

        return baseControllerService.createRedirectModelAndView(serviceRedirectUrl)
    }

    private fun generateUUID(): String {
        return UUID.randomUUID().toString().replace("-", "")
    }

    @GetMapping(Urls.GET_SHARED_OBJECT_COUNTER)
    @ResponseBody
    fun getSharedObjectCounter(@RequestParam url: String): ActionStatus<Long> {
        val sharedObjectStats = sharedObjectService.getSharedObjectByUrl(url) ?: return ActionStatus.createErrorStatus("SharedObject for url not found " + url)
        return ActionStatus.createSuccessStatus(sharedObjectStats.count)
    }

    companion object {
        private val PLANETA_UID_COOKIE = "planeta_uid"

        /**
         * Constructs url of share service for specified s`sharedObject`
         */
        private fun createSharedServiceRedirectUrl(sharedObject: SharedObject, title: String?): String {
            var serviceUrl: String = sharedObject.shareServiceType?.url ?: ""
            serviceUrl = StringUtils.replaceOnce(serviceUrl, "{url}", safeEncodeUTF8(sharedObject.url))
            serviceUrl = StringUtils.replaceOnce(serviceUrl, "{title}", safeEncodeUTF8(title))
            return serviceUrl
        }

        /**
         * Safely encodes string into UTF-8 encoding, without throwing NullPointerException
         *
         * @return string encoded in UTF-8 encoding or passed string if null or empty
         */
        private fun safeEncodeUTF8(str: String?): String? {
            return if (StringUtils.isEmpty(str)) {
                str
            } else URLEncoder.encode(str, "UTF-8")
        }
    }
}


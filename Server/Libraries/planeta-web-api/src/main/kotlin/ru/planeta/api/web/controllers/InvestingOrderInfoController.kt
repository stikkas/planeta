package ru.planeta.api.web.controllers

import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import ru.planeta.api.model.ActionStatus
import ru.planeta.api.service.common.InvestingOrderInfoService
import ru.planeta.api.web.authentication.myProfileId
import ru.planeta.commons.model.Gender
import ru.planeta.model.common.InvestingOrderInfo
import ru.planeta.model.enums.ContractorType

/**
 *
 * User: Serge Blagodatskih<stikkas17></stikkas17>@gmail.com><br></br>
 * Date: 28.01.16<br></br>
 * Time: 11:54
 */
@RestController
class InvestingOrderInfoController(private val infoService: InvestingOrderInfoService) {

    @GetMapping(Urls.GET_INVEST_ORDER_INFO)
    fun getOrderInfo(@RequestParam userType: ContractorType): InvestingOrderInfo =
            infoService.select(myProfileId(), userType) ?: InvestingOrderInfo(userType)

    @RequestMapping(Urls.DEL_INVEST_ORDER_INFO)
    fun removeOrderInfo(@RequestParam orderId: Long): ActionStatus<*> {
        infoService.delete(orderId)
        return ActionStatus.createSuccessStatus<Any>()
    }
}

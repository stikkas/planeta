package ru.planeta.eva.api.web.validation.validators

import org.apache.commons.lang3.StringUtils
import org.springframework.stereotype.Component
import org.springframework.validation.Errors
import org.springframework.validation.ValidationUtils
import org.springframework.validation.Validator
import ru.planeta.api.service.profile.AuthorizationService
import ru.planeta.api.utils.ValidateUtils
import ru.planeta.api.web.authentication.myProfileId
import ru.planeta.dao.commondb.UserPrivateInfoDAO
import ru.planeta.eva.api.models.ChangePasswordDTO

@Component
internal class ChangePasswordDTOValidator(private val userPrivateInfoDAO: UserPrivateInfoDAO, private val authorizationService: AuthorizationService) : Validator {

    companion object {
        const val MINIMAL_PASS_LENGTH = 6
    }

    override fun supports(clazz: Class<*>): Boolean {
        return ChangePasswordDTO::class.java.isAssignableFrom(clazz)
    }

    override fun validate(target: Any, errors: Errors) {

        val change = target as ChangePasswordDTO
        val info = userPrivateInfoDAO.selectByUserId(myProfileId())

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "oldPassword", "field_required")
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "newPassword", "field_required")
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "rePassword", "field_required")

        if (info != null) {
            if (!authorizationService.checkPassword(info, change.oldPassword)) {
                errors.rejectValue("oldPassword", "wrong_password")
                return
            }

            if (StringUtils.isNotBlank(change.oldPassword)) {
                ValidateUtils.rejectIfNotPassword(errors, "newPassword", "wrong_chars")
                ValidateUtils.rejectIfSizeIsTooSmall(errors, "newPassword", "too_short", MINIMAL_PASS_LENGTH, false)

                if (StringUtils.isNotEmpty(change.newPassword) && change.newPassword != change.rePassword) {
                    errors.rejectValue("rePassword", "do_not_match")
                }
            }
        } else {
            errors.rejectValue("user_info", "not_found")
        }
    }
}

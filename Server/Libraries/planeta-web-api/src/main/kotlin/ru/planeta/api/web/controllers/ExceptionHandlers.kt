package ru.planeta.api.web.controllers

import com.fasterxml.jackson.databind.ObjectMapper
import org.apache.log4j.Logger
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.MessageSource
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import ru.planeta.api.exceptions.*
import ru.planeta.api.model.ActionStatus
import ru.planeta.api.web.authentication.AuthUtils
import ru.planeta.api.web.controllers.services.BaseControllerService
import ru.planeta.commons.text.TranslitConverter
import java.io.OutputStream
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

@ControllerAdvice
class ExceptionHandlers {

    @Autowired
    lateinit var baseControllerService: BaseControllerService

    @Autowired
    lateinit var messageSource: MessageSource

    @ExceptionHandler(VideoPermissionException::class)
    fun handleVideoPermissionException(request: HttpServletRequest, response: HttpServletResponse, ex: VideoPermissionException) {
        log.info(ex.message)
        request.setAttribute("exception", ex)
        request.getRequestDispatcher(VIDEO_PERMISSION_FRAME).forward(request, response)
    }

    @ExceptionHandler(VideoProcessingException::class)
    fun handleVideoProcessingException(request: HttpServletRequest, response: HttpServletResponse, ex: VideoProcessingException) {
        log.info(ex.message)
        request.setAttribute("exception", ex)
        request.getRequestDispatcher(VIDEO_PROCESSING_FRAME).forward(request, response)
    }


    @ExceptionHandler(WidgetNotActiveException::class)
    fun handleWidgetNotActiveException(request: HttpServletRequest, response: HttpServletResponse, ex: WidgetNotActiveException) {
        log.info("Not active share in widget " + ex.shareId)
        request.setAttribute("exception", ex)
        request.getRequestDispatcher(WIDGET_NOT_ACTIVE_FRAME).forward(request, response)
    }

    @ExceptionHandler(VideoNotFoundException::class)
    fun handleVideoNotFoundException(request: HttpServletRequest, response: HttpServletResponse, ex: VideoNotFoundException) {
        log.info(ex.message)
        request.getRequestDispatcher(VIDEO_NOT_FOUND_FRAME).forward(request, response)
    }

    @ExceptionHandler(PaymentException::class)
    fun handlePaymentException(request: HttpServletRequest, response: HttpServletResponse, ex: PaymentException) {

        log.info(ex.message)
        if (AuthUtils.isAjaxRequest(request)) {
            ajaxHandler(response, baseControllerService.getErrorMessage(ex.message, PERMISSION_DEFAULT_CODE), ex)
            return
        }
        request.setAttribute("isCardPayment", false)
        request.setAttribute("hasPreviouslyFailed", false)
        request.setAttribute("errorMessage", baseControllerService.getErrorMessage(ex.message, ex.message))
        request.getRequestDispatcher(PAYMENT_ERROR).forward(request, response)
    }

    @ExceptionHandler(NoContentException::class)
    fun handleNoContentException(request: HttpServletRequest, response: HttpServletResponse, ex: NoContentException) {
        log.info(ex.message)
        request.setAttribute("exception", ex)
        response.status = HttpServletResponse.SC_NO_CONTENT
    }

    @ExceptionHandler(PermissionException::class)
    fun handlePermissionException(request: HttpServletRequest, response: HttpServletResponse, ex: PermissionException) {
        log.info(ex.message + " Request URL: " + request.requestURL.toString())
        handleException(request, response, HttpServletResponse.SC_FORBIDDEN,
                baseControllerService.getErrorMessage(ex.messageCode?.errorPropertyName, PERMISSION_DEFAULT_CODE, arrayOf(ex.arguments as Any)), ex)
    }

    @ExceptionHandler(NotFoundException::class)
    fun handleNotFoundException(request: HttpServletRequest, response: HttpServletResponse, ex: NotFoundException) {
        log.info(ex.message + " Request URL: " + request.requestURL.toString())
        handleException(request, response, HttpServletResponse.SC_NOT_FOUND, baseControllerService.getErrorMessage(ex.message, NOT_FOUND_DEFAULT_CODE), ex)
    }

    @ExceptionHandler(MoscowShowInteractionException::class)
    fun handleMoscowShowInteractionException(request: HttpServletRequest, response: HttpServletResponse, ex: NotFoundException) {
        log.info(ex.message + " Request URL: " + request.requestURL.toString())
        handleException(request, response, HttpServletResponse.SC_BAD_REQUEST, baseControllerService.getErrorMessage(ex.message, EXCEPTION_DEFAULT_CODE), ex)
    }


    @ExceptionHandler(Exception::class)
    fun handleCommonException(request: HttpServletRequest, response: HttpServletResponse, ex: Exception) {
        log.error("Exception", ex)
        handleException(request, response, HttpServletResponse.SC_INTERNAL_SERVER_ERROR, baseControllerService.getErrorMessage(ex.message, EXCEPTION_DEFAULT_CODE), ex)
    }

    protected fun handleException(request: HttpServletRequest, response: HttpServletResponse, statusCode: Int, message: String?, ex: Exception) {
        if (AuthUtils.isAjaxRequest(request)) {
            ajaxHandler(response, message, ex)
        } else {
            response.sendError(statusCode, TranslitConverter.toTranslit(message))
        }
    }

    protected fun ajaxHandler(response: HttpServletResponse, message: String?, ex: Exception) {
        val objectMapper = ObjectMapper()
        val actionStatus = ActionStatus.createErrorStatus<Any>(message, messageSource, ex.javaClass)
        if (log.isDebugEnabled) {
            log.error("handle exception", ex)
        }
        try {
            response.outputStream?.use {
                response.status = HttpServletResponse.SC_OK
                response.contentType = "application/json"
                objectMapper.writeValue(it, actionStatus)
            }
        } finally {
            response.flushBuffer()
        }
    }

    companion object {
        const val PERMISSION_DEFAULT_CODE = "permission.exception"
        const val NOT_FOUND_DEFAULT_CODE = "not.found.exception"
        const val EXCEPTION_DEFAULT_CODE = "unexpected.exception"

        private val log = Logger.getLogger(ExceptionHandlers::class.java)

        const val VIDEO_PERMISSION_FRAME = "/WEB-INF/jsp/includes/generated/video-permission-frame.jsp"
        const val VIDEO_NOT_FOUND_FRAME = "/WEB-INF/jsp/includes/generated/video-not-found-frame.jsp"
        const val VIDEO_PROCESSING_FRAME = "/WEB-INF/jsp/includes/generated/video-processing-frame.jsp"
        const val WIDGET_NOT_ACTIVE_FRAME = "/WEB-INF/jsp/includes/generated/widget-not-active-frame.jsp"
        const val PAYMENT_ERROR = "/WEB-INF/jsp/includes/generated/payment-fail-easy.jsp"
    }
}


package ru.planeta.api.web.controllers.profile

import org.apache.log4j.Logger
import org.springframework.stereotype.Controller
import org.springframework.validation.BindingResult
import org.springframework.web.bind.annotation.*
import ru.planeta.api.exceptions.NotFoundException
import ru.planeta.api.exceptions.PermissionException
import ru.planeta.api.model.ActionStatus
import ru.planeta.api.news.LoggerService
import ru.planeta.api.service.comments.CommentsService
import ru.planeta.api.service.profile.ProfileService
import ru.planeta.api.service.profile.ProfileSubscriptionService
import ru.planeta.api.service.statistic.StatisticsService
import ru.planeta.api.web.authentication.myProfileId
import ru.planeta.api.web.controllers.Urls
import ru.planeta.api.web.controllers.services.BaseControllerService
import ru.planeta.model.enums.ObjectType
import ru.planeta.model.enums.ProfileRelationStatus
import ru.planeta.model.news.ProfileNews
import ru.planeta.model.profile.Comment
import ru.planeta.model.stat.StatEventType
import java.util.*
import javax.validation.Valid

/**
 * Controller for comments
 *
 * @author a.savanovich
 * Class CommentsController
 */
@RestController
class CommentsController(private val statisticsService: StatisticsService,
                         private val profileSubscriptionService: ProfileSubscriptionService,
                         private val commentsService: CommentsService,
                         private val profileNewsService: LoggerService,
                         private val profileService: ProfileService,
                         private val baseControllerService: BaseControllerService) {

    /**
     * Gets list of photo comments (child to the specified root comment).
     * If parentCommentId is 0 or not set, returns list of root comments.
     *
     * @return JSON object with list of comments
     */
    @GetMapping(Urls.COMMENTS)
    fun getComments(@RequestParam objectId: Long,
                    @RequestParam(defaultValue = "0") parentCommentId: Long,
                    @RequestParam(defaultValue = "0") includeChildComments: Int,
                    @RequestParam objectType: ObjectType,
                    @RequestParam(value = "profileId") alias: String,
                    @RequestParam(defaultValue = "0") offset: Int,
                    @RequestParam(defaultValue = "10") limit: Int,
                    @RequestParam(defaultValue = "desc") sort: String): List<Comment> {
        val profileId = profileService.getProfileSafe(alias).profileId
        return commentsService.getComments(myProfileId(), profileId, objectId, objectType, parentCommentId, includeChildComments, offset, limit, sort)
    }

    class CommentWithRelationStatuses {
        var comment: Comment? = null
        var relationStatuses: EnumSet<ProfileRelationStatus>? = null
    }

    @GetMapping(Urls.COMMENTS_WITH_CHILD_AND_RELATION_STATUSES)
    fun getCommentsWithChildAndRelationStatusesAsPlain(@RequestParam objectId: Long,
                                                       @RequestParam(defaultValue = "0") objectOwnerProfileId: Long,
                                                       @RequestParam objectType: ObjectType,
                                                       @RequestParam(value = "profileId") alias: String,
                                                       @RequestParam(defaultValue = "0") offset: Int,
                                                       @RequestParam(defaultValue = "10") limit: Int): List<CommentWithRelationStatuses> {
        var limit = limit
        if (limit <= 0 || limit > 100) {
            log.info("limit was changed from $limit to $UPPER_LIMIT")
            limit = UPPER_LIMIT
        }
        val profileId = profileService.getProfileSafe(alias).profileId
        val commentList = commentsService.getCommentsWithChildAsPlain(myProfileId(), profileId, objectId, objectType, offset, limit)

        val profileIdList = commentList.map { it.authorProfileId }

        val profileRelationShortList = profileSubscriptionService.getProfileRelationShortList(objectOwnerProfileId, profileIdList)

        val commentWithRelationStatusesList = ArrayList<CommentWithRelationStatuses>(commentList.size)
        for (comment in commentList) {
            val commentWithRelationStatuses = CommentWithRelationStatuses()
            commentWithRelationStatuses.comment = comment
            commentWithRelationStatusesList.add(commentWithRelationStatuses)
            for (profileRelationShort in profileRelationShortList) {
                if (profileRelationShort.profileId == comment.authorProfileId) {
                    commentWithRelationStatuses.relationStatuses = profileRelationShort.relationStatuses
                    break
                }
            }
        }
        return commentWithRelationStatusesList
    }

    /**
     * Adds new comment.
     * Author identifier is set to current user's id.
     *
     * @return JSON object (representing added comment). If something gone wrong - HTTP status 500 is set.
     */
    @PostMapping(Urls.COMMENT_ADD)
    fun addComment(@Valid comment: Comment, result: BindingResult,
                   @RequestParam objectType: ObjectType,
                   @RequestHeader(value = "Referer", required = false) referer: String?): ActionStatus<Comment> {

        if (result.hasErrors()) {
            return baseControllerService.createErrorStatus(result)
        }
        comment.authorProfileId = myProfileId()
        comment.objectType = objectType
        val addedComment = commentsService.addComment(comment)

        if (comment.objectType == ObjectType.CAMPAIGN) {
            statisticsService.trackEvent(StatEventType.CAMPAIGN_COMMENT, comment.objectId, referer, myProfileId())
        }

        return ActionStatus.createSuccessStatus(addedComment)
    }

    @PostMapping(Urls.COMMENT_DELETE)
    fun deleteComment(@RequestParam commentId: Long,
                      @RequestParam profileId: Long): ActionStatus<Comment> {
        val myProfileId = myProfileId()
        commentsService.deleteComment(myProfileId, profileId, commentId)
        profileNewsService.addProfileNews(ProfileNews.Type.DELETE_COMMENT, myProfileId, commentId)
        return ActionStatus.createSuccessStatus()
    }

    /**
     * Restore deleteByProfileId comment
     */
    @PostMapping(Urls.COMMENT_RESTORE)
    fun restoreComment(@RequestParam profileId: Long,
                       @RequestParam commentId: Long): ActionStatus<*> {
        commentsService.restoreComment(myProfileId(), profileId, commentId)
        return ActionStatus.createSuccessStatus<Any>()
    }

    @GetMapping(Urls.COMMENTS_LAST)
    fun getLastComments(@RequestParam profileId: Long,
                        @RequestParam objectId: Long,
                        @RequestParam objectType: ObjectType,
                        @RequestParam(defaultValue = "0") startCommentId: Long,
                        @RequestParam(defaultValue = "desc") sort: String,
                        @RequestParam(defaultValue = "0") offset: Int,
                        @RequestParam(defaultValue = "0") limit: Int): List<Comment> =
            commentsService.getLastComments(profileId, objectId, objectType, startCommentId, sort, offset, limit)

    companion object {
        private val log = Logger.getLogger(CommentsController::class.java)
        private val UPPER_LIMIT = 100
    }
}

package ru.planeta.eva.api.web.controllers

import org.springframework.validation.BindingResult
import org.springframework.validation.FieldError
import org.springframework.web.bind.annotation.*
import ru.planeta.api.web.authentication.myProfileId
import ru.planeta.eva.api.services.ShareService
import ru.planeta.eva.api.web.ActionStatus
import ru.planeta.eva.api.web.Urls
import ru.planeta.eva.api.web.models.PaymentData
import ru.planeta.eva.api.web.validation.validators.AuthShareDTOValidator
import ru.planeta.dto.SaveShareInfoDTO
import javax.validation.Valid

@RestController("EvaWebApiCampaignController")
class WebApiCampaignController(val authShareDTOValidator: AuthShareDTOValidator,
                               val shareService: ShareService) {

    @PostMapping(Urls.VALIDATE_SHARE_PURCHASE)
    fun validateShare(@Valid @RequestBody paymentData: PaymentData, errors: BindingResult): ActionStatus {
        authShareDTOValidator.validate(paymentData, errors)

        return if (errors.hasErrors()) {
            ActionStatus(errors)
        } else {
            ActionStatus(result = "/redirect_url")
        }
    }

    @GetMapping(Urls.PURCHASE_SHARE_INFO)
    fun purchaseShareInfo(@PathVariable(Urls.SHARE_KEY) infoId: Long): ActionStatus {
        val result = shareService.getPurchaseShareInfo(infoId, myProfileId())
        return ActionStatus(result = result)
    }

    /**
     * Переход со страницы выбора акции
     */
    @PostMapping(Urls.PURCHASE_SHARE)
    fun purchaseShare(@RequestBody shareDTO: SaveShareInfoDTO): ActionStatus {
        return if (shareService.checkPurchaseShare(shareDTO)) {
            shareService.savePurchaseShareInfo(shareDTO)
            ActionStatus(result = shareDTO.id)
        } else {
            ActionStatus(FieldError("shareInfoDTO", "donateAmount", "wrong_amount_or_quantity"))
        }
    }
}


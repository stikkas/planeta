package ru.planeta.eva.api.web

import org.springframework.validation.BindingResult
import org.springframework.validation.FieldError

/**
 * Результаты ответа от сервера
 */
class ActionStatus {
    val success: Boolean
    val result: Any?

    /**
     * По умолчанию результат положительный
     */
    constructor(success: Boolean = true, result: Any? = null) {
        this.success = success
        this.result = result
    }

    /**
     * Ответ с ошибками
     */
    constructor(errors: BindingResult) {
        success = false
        result = errors.allErrors
    }

    /**
     * Ответ c одной ошибкой
     */
    constructor(error: FieldError): this(listOf(error))

    constructor(error: List<FieldError>) {
        success = false
        result = error
    }
}


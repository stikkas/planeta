package ru.planeta.eva.api.web.controllers

import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController
import ru.planeta.api.service.profile.ProfileBalanceService
import ru.planeta.api.web.authentication.myProfileId
import ru.planeta.eva.api.web.ActionStatus
import ru.planeta.eva.api.web.Urls

@RestController("EvaWebApiBalanceController")
class BalanceController(private val balanceService: ProfileBalanceService) {
    @GetMapping(Urls.MY_BALANCE)
    fun getBalance() : ActionStatus {
        val balance = balanceService.getBalance(myProfileId())
        return ActionStatus(true, balance)
    }
}

package ru.planeta.eva.api.web.models

import ru.planeta.eva.api.web.models.DeliveryInfo
import java.math.BigDecimal

class PaymentData {

    var deliveryInfo: DeliveryInfo = DeliveryInfo()

    // @NotBlank
    var infoId: Long = 0

    // @NotBlank(message="wrong.email")
    // @Pattern(regexp = "^[_A-Za-z0-9-+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})\$", message = "wrong.email")
    var email: String? = null

    var paymentPhone: String? = null
    var needPhone: Boolean = false
    var reply: String? = null
    var paymentMethodId: Long = 0
}

package ru.planeta.eva.api.web.controllers

import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import ru.planeta.api.service.geo.GeoService
import ru.planeta.eva.api.web.ActionStatus
import ru.planeta.eva.api.web.Urls
import ru.planeta.eva.api.web.dto.SearchCitiesDTO
import ru.planeta.model.profile.location.City

/**
 * Controller for utility methods.
 */
@RestController
class UtilitiesController(private val geoService: GeoService) {

    companion object {
        const val DEFAULT_LIMIT = 20
    }

    @GetMapping(Urls.CITIES_LIST_BY_SUBSTRING)
    fun getCitiesListBySubstring(searchCitiesDTO: SearchCitiesDTO): ActionStatus {
        var limitValue = searchCitiesDTO.limit
        if (limitValue > DEFAULT_LIMIT) {
            limitValue = DEFAULT_LIMIT
        }

        val cities: List<City> = if (searchCitiesDTO.regionId > 0) {
            geoService.getRegionCitiesBySubstring(searchCitiesDTO.regionId, searchCitiesDTO.query, 0, limitValue)
        } else {
            geoService.getCountryCitiesBySubstring(searchCitiesDTO.countryId, searchCitiesDTO.query, 0, limitValue)
        }

        return ActionStatus(result = cities)
    }

    @GetMapping(Urls.CITY_BY_ID)
    fun getCityById(@RequestParam cityId: Int): ActionStatus {
        return ActionStatus(result = geoService.getCityById(cityId))
    }
}

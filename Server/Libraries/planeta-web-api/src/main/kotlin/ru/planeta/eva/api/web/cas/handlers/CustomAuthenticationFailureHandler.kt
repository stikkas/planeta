package ru.planeta.eva.api.web.cas.handlers

import org.apache.log4j.Logger
import org.springframework.security.core.AuthenticationException
import org.springframework.security.web.authentication.AuthenticationFailureHandler
import org.springframework.security.web.util.UrlUtils
import org.springframework.util.Assert
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

/**
 * Created by IntelliJ IDEA.
 * User: m.shulepov
 * Date: 15.03.12
 * Time: 13:04
 */
class CustomAuthenticationFailureHandler : AuthenticationFailureHandler {
    val logger = Logger.getLogger(CustomAuthenticationFailureHandler::class.java)

    private var defaultFailureUrl: String? = null
    /**
     * If set to <tt>true</tt>, performs a forward to the failure destination URL instead of a redirect. Defaults to
     * <tt>false</tt>.
     */
    var isUseForward = false

    override fun onAuthenticationFailure(request: HttpServletRequest, response: HttpServletResponse,
                                         exception: AuthenticationException) {
        if (defaultFailureUrl == null) {
            logger.debug("No failure URL set, sending 401 Unauthorized error")
            response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Authentication Failed: " + exception.message)
        } else {
            val destinationUrl = createDestinationUrl(request)
            if (isUseForward) {
                logger.debug("Forwarding to " + destinationUrl)
                request.getRequestDispatcher(destinationUrl).forward(request, response)
            } else {
                logger.debug("Redirecting to " + destinationUrl)
                response.sendRedirect(destinationUrl)
            }
        }
    }

    private fun createDestinationUrl(request: HttpServletRequest): String? {
        var destinationUrl = this.defaultFailureUrl
        if (request.queryString != null) {
            destinationUrl += "?" + request.queryString
        }
        return destinationUrl
    }

    /**
     * The URL which will be used as the failure destination.
     */
    fun setDefaultFailureUrl(defaultFailureUrl: String) {
        Assert.isTrue(UrlUtils.isValidRedirectUrl(defaultFailureUrl), "'$defaultFailureUrl' is not a valid redirect URL")
        this.defaultFailureUrl = defaultFailureUrl
    }
}


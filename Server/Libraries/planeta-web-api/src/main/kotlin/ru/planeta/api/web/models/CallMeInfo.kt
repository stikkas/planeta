package ru.planeta.api.web.models

import ru.planeta.model.common.UserCallback
import ru.planeta.model.enums.OrderObjectType
import java.math.BigDecimal

class CallMeInfo {
    var type: UserCallback.Type = UserCallback.Type.ORDERING_PROBLEM
    var phone: String = ""
    var paymentId: Long? = null
    var orderType: OrderObjectType? = null
    var objectId: Long? = null
    var amount: BigDecimal = BigDecimal.ZERO
}

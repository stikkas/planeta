package ru.planeta.api.web.utils

import org.springframework.security.core.Authentication
import org.springframework.web.context.request.RequestContextHolder
import org.springframework.web.context.request.ServletRequestAttributes
import ru.planeta.api.aspect.logging.AdminAuditLoggerInfo
import ru.planeta.api.model.ExternalUserInfo
import ru.planeta.api.web.utils.SessionUtils.CREDENTIALS_INFO
import ru.planeta.api.web.utils.SessionUtils.session
import ru.planeta.model.profile.Comment

import javax.servlet.http.HttpSession
import java.util.HashSet

object SessionUtils {

    private const val REDIRECTS_TO_CAS = "cas_redirects"
    private const val REDIRECT_AFTER_OAUTH = "oauth_redirect"
    private const val CREDENTIALS_INFO = "userCredentialsInfo"
    private const val REDIRECT_AFTER_REGISTRATION_SESSION_ATTR = "redirectAfterRegistrationUrl"
    private const val PURCHASE_ORDER_ID = "purchase_order_id"
    private const val DEFERRED_COMMENTS = "deferred_comments"
    private const val PROFILE_AUTHENTICATION = "profileAuthentication"
    private const val PRIVATE_BROADCAST_GENERATE_LINK = "privateBroadcastGeneratedLink"

    private const val ADMIN_AUDIT_INFO = "adminAuditInfo"

    @JvmStatic
    val session: HttpSession
        get() {
            val attr = RequestContextHolder.currentRequestAttributes() as ServletRequestAttributes
            return attr.request.session
        }

    @JvmStatic
    var redirectAfterOAuth: String?
        get() = session.getAttribute(REDIRECT_AFTER_OAUTH) as? String
        set(redirectUrl) = session.setAttribute(REDIRECT_AFTER_OAUTH, redirectUrl)

    // don't remove it (for page refresh
    @JvmStatic
    val externalUserInfo: ExternalUserInfo?
        get() = session.getAttribute(CREDENTIALS_INFO) as? ExternalUserInfo

    @JvmStatic
    val redirectAfterRegistrationUrl: String?
        get() = session.getAttribute(REDIRECT_AFTER_REGISTRATION_SESSION_ATTR) as? String

    @JvmStatic
    var deferredComments: Map<Long, Comment>?
        get() = session.getAttribute(DEFERRED_COMMENTS) as? Map<Long, Comment>
        set(result) = session.setAttribute(DEFERRED_COMMENTS, result)

    @JvmStatic
    var orderId: Long?
        get() {
            val orderId = session.getAttribute(PURCHASE_ORDER_ID) as? Long
            session.removeAttribute(PURCHASE_ORDER_ID)
            return orderId
        }
        set(orderId) = session.setAttribute(PURCHASE_ORDER_ID, orderId)

    @JvmStatic
    var privateBroadcastGeneratedLink: String?
        get() = session.getAttribute(PRIVATE_BROADCAST_GENERATE_LINK) as? String
        set(generatedLink) = session.setAttribute(PRIVATE_BROADCAST_GENERATE_LINK, generatedLink)
    /**
     * Determines if provided url has been already redirected to CAS
     *
     */
    @JvmStatic
    fun isRedirectFromCAS(url: String): Boolean {
        val redirectsToCAS = session.getAttribute(REDIRECTS_TO_CAS) as? MutableSet<String>
        return redirectsToCAS?.remove(url) ?: false
    }

    @JvmStatic
    fun addToRedirects(url: String) {
        val redirectsToCAS = session.getAttribute(REDIRECTS_TO_CAS) as? MutableSet<String> ?: HashSet()
        redirectsToCAS.add(url)
        session.setAttribute(REDIRECTS_TO_CAS, redirectsToCAS)
    }

    @JvmStatic
    fun setUserCredentials(userInfo: ExternalUserInfo) {
        session.setAttribute(CREDENTIALS_INFO, userInfo)
    }

    /**
     * Used only for multi-step registration:
     * 1. On page A anonymous stars registration (save A using this method)
     * 2. He must be redirected to cas
     * 3. He is redirected to page B for some info fulfill
     * 4. After fulfill complete he must be redirected back to page A (retrieve A with get method)
     * Note that CAS provided successUrl must be used for single-step registrations
     * @see .getRedirectAfterRegistrationUrl
     *
     * @param redirectUrl - can contain proto and host or be just relative url
     */
    @JvmStatic
    fun setRegistrationRedirectUrl(redirectUrl: String?) =
            if (redirectUrl == null) {
                session.removeAttribute(REDIRECT_AFTER_REGISTRATION_SESSION_ATTR)
            } else {
                session.setAttribute(REDIRECT_AFTER_REGISTRATION_SESSION_ATTR, redirectUrl)
            }

    @JvmStatic
    fun setProfileAuthentication(authentication: Authentication?) = session.setAttribute(PROFILE_AUTHENTICATION, authentication)

    @JvmStatic
    fun removePrivateBroadcastGeneratedLink() = session.removeAttribute(PRIVATE_BROADCAST_GENERATE_LINK)

    @JvmStatic
    fun getAdminAuditInfo(session: HttpSession?): AdminAuditLoggerInfo? =
            session?.getAttribute(ADMIN_AUDIT_INFO) as? AdminAuditLoggerInfo

    @JvmStatic
    fun setAdminAuditInfo(adminId: Long?, userId: Long?) {
        if (adminId != null && userId != null) {
            session.setAttribute(ADMIN_AUDIT_INFO, AdminAuditLoggerInfo(adminId, userId))
        }
    }

}


package ru.planeta.api.web.controllers

import org.springframework.validation.BindingResult
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RestController
import ru.planeta.api.mail.MailClient
import ru.planeta.api.model.ActionStatus
import ru.planeta.api.model.FeedbackMessage
import ru.planeta.api.web.authentication.myProfileId
import ru.planeta.api.web.controllers.services.BaseControllerService
import javax.validation.Valid

@Deprecated("use ru.planeta.eva.api.web.controllers.FeedbackController")
@RestController
class StaticCommonController(private val mailClient: MailClient,
                             private val baseControllerService: BaseControllerService) {

    @PostMapping(Urls.FEEDBACK)
    fun sendFeedbackMessage(@Valid feedbackMessage: FeedbackMessage,
                            result: BindingResult): ActionStatus<*> {
        if (result.hasErrors()) {
            return baseControllerService.createErrorStatus<Any>(result)
        }
        feedbackMessage.message = feedbackMessage.message + feedbackMessage.additionalInfo
        mailClient.sendFeedbackEmail(feedbackMessage.email, feedbackMessage.message, feedbackMessage.theme, myProfileId())
        return ActionStatus.createSuccessStatus<Any>()
    }

}

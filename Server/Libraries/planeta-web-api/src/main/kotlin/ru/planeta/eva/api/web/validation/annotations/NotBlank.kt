package ru.planeta.eva.api.web.validation.annotations

import ru.planeta.eva.api.web.validation.validators.NotBlankValidator
import java.lang.annotation.Documented
import javax.validation.Constraint
import javax.validation.Payload
import kotlin.reflect.KClass


@Documented
@Constraint(validatedBy = [(NotBlankValidator::class)])
@Target(AnnotationTarget.FIELD)
@Retention(AnnotationRetention.RUNTIME)
annotation class NotBlank(val message: String = "field.required",
                          val groups: Array<KClass<*>> = [],
                          val payload: Array<KClass<out Payload>> = [])

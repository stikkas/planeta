package ru.planeta.api.web.controllers

import org.apache.commons.collections4.IterableUtils
import org.apache.commons.lang3.StringUtils
import org.springframework.context.MessageSource
import org.springframework.context.i18n.LocaleContextHolder
import org.springframework.validation.BindingResult
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import ru.planeta.api.aspect.transaction.Transactional
import ru.planeta.api.exceptions.MessageCode
import ru.planeta.api.exceptions.MessageCode.*
import ru.planeta.api.exceptions.NotFoundException
import ru.planeta.api.exceptions.PermissionException
import ru.planeta.api.model.ActionStatus
import ru.planeta.api.news.LoggerService
import ru.planeta.api.service.campaign.CampaignPermissionService
import ru.planeta.api.service.campaign.CampaignService
import ru.planeta.api.service.campaign.ContractorService
import ru.planeta.api.service.content.ProfileFileService
import ru.planeta.api.service.security.PermissionService
import ru.planeta.api.web.authentication.myProfileId
import ru.planeta.api.web.controllers.services.BaseControllerService
import ru.planeta.model.common.Contractor
import ru.planeta.model.common.campaign.Campaign
import ru.planeta.model.enums.ContractorType
import ru.planeta.model.news.ProfileNews
import ru.planeta.model.profile.ProfileFile
import java.util.*
import javax.validation.Valid

/**
 * User: a.savanovich
 * Date: 22.11.13
 * Time: 17:09
 */
@RestController
class ContractorController(private val contractorService: ContractorService,
                           private val permissionService: PermissionService,
                           private val campaignPermissionService: CampaignPermissionService,
                           private val profileFileService: ProfileFileService,
                           private val campaignService: CampaignService,
                           private val profileNewsService: LoggerService,
                           private val messageSource: MessageSource,
                           private val baseControllerService: BaseControllerService) {

    @GetMapping(Urls.Contractor.CONTRACTOR_LIST)
    fun selectList(@RequestParam offset: Int, @RequestParam limit: Int): List<Contractor> =
            if (!permissionService.hasAdministrativeRole(myProfileId()))
                throw PermissionException()
            else
                contractorService.selectList(offset, limit)

    @GetMapping(Urls.Contractor.CONTRACTOR_LIST_BY_PROFILE)
    fun selectListByProfile(@RequestParam profileId: Long,
                            @RequestParam offset: Int,
                            @RequestParam limit: Int): ActionStatus<List<Contractor>> =
            if (!permissionService.isAdmin(myProfileId(), profileId))
                throw PermissionException()
            else
                ActionStatus.createSuccessStatus(contractorService.selectList(profileId, offset, limit))

    @PostMapping(Urls.Contractor.CONTRACTOR_ADD_AND_CHANGE_CAMPAIGN_STATUS)
    @Transactional
    fun contractorAddAndChangeCampaignStatus(@Valid contractor: Contractor, bindingResult: BindingResult,
                                             @RequestParam(required = false) campaignId: Long?): ActionStatus<Contractor> {
        val actionStatus = checkContractorExists(contractor)
        if (actionStatus != null) return actionStatus

        if (bindingResult.hasErrors()) {
            return baseControllerService.createErrorStatus(bindingResult)
        }

        val campaign = campaignService.getCampaign(campaignId)
        if (campaign?.tags != null && campaignHasTag(campaign, "CHARITY") && contractor.type != ContractorType.CHARITABLE_FOUNDATION) {
            val status = ActionStatus.createErrorStatus<Contractor>(CONTRACTOR_TYPE_CHARITY_ERROR.errorPropertyName, messageSource)
            status.addFieldError("type", messageSource.getMessage(CONTRACTOR_TYPE_CHARITY_ERROR.errorPropertyName, null, LocaleContextHolder.getLocale()))
            return status
        }

        contractorService.add(contractor, myProfileId(), campaignId)

        if (campaignId != null && campaignId > 0) {
            checkBindPermission(campaignId)
            contractorService.insertRelation(contractor.contractorId, campaignId)
        }
        return ActionStatus.createSuccessStatus(contractor)
    }

    private fun checkContractorExists(@Valid contractor: Contractor): ActionStatus<Contractor>? {
        if (StringUtils.isNotBlank(contractor.inn)) {
            val existingContractor = contractorService.selectByInn(contractor.inn ?: "")
            if (existingContractor != null && contractor.contractorId != existingContractor.contractorId) {
                val actionStatus = ActionStatus<Contractor>()
                if (permissionService.hasAdministrativeRole(myProfileId())) {
                    actionStatus.result = existingContractor
                }
                actionStatus.isSuccess = false
                actionStatus.errorMessage = "contractor exists"
                return actionStatus
            }
        }
        return null
    }

    @PostMapping(Urls.Contractor.CONTRACTOR_ADD_EXTRA_VALIDATE_AND_CHANGE_CAMPAIGN_STATUS)
    @Transactional
    fun contractorAddExtraValidateAndChangeCampaignStatus(
            @Valid contractor: Contractor, bindingResult: BindingResult,
            @RequestParam(required = false) campaignId: Long?): ActionStatus<Contractor> {
        val actionStatus = checkContractorExists(contractor)
        if (actionStatus != null) return actionStatus

        if (bindingResult.hasErrors()) {
            return baseControllerService.createErrorStatus(bindingResult)
        }

        val campaign = campaignService.getCampaign(campaignId)
        if (campaign?.tags != null && campaignHasTag(campaign, "CHARITY") && contractor.type != ContractorType.CHARITABLE_FOUNDATION) {
            val status = ActionStatus.createErrorStatus<Contractor>(CONTRACTOR_TYPE_CHARITY_ERROR.errorPropertyName, messageSource)
            status.addFieldError("type", messageSource.getMessage(CONTRACTOR_TYPE_CHARITY_ERROR.errorPropertyName, null, LocaleContextHolder.getLocale()))
            return status
        }

        contractorService.validateContractor(bindingResult, campaignId, contractor)

        if (bindingResult.hasErrors()) {
            return baseControllerService.createErrorStatus(bindingResult)
        }

        if (campaign != null) {
            val files = profileFileService.getProfileFiles(myProfileId(), campaign.profileId ?: -1, 0, 1)
            if (files.isEmpty()) {
                val status = ActionStatus.createErrorStatus<Contractor>(FIELD_REQUIRED.errorPropertyName, messageSource)
                status.addFieldError("scanPassportUrl", messageSource.getMessage(FIELD_REQUIRED.errorPropertyName, null, LocaleContextHolder.getLocale()))
                return status
            }
        }

        contractorService.add(contractor, myProfileId(), campaignId)

        if (campaignId != null && campaignId > 0) {
            checkBindPermission(campaignId)
            contractorService.insertRelation(contractor.contractorId, campaignId)
        }

        return ActionStatus.createSuccessStatus(contractor)
    }

    @PostMapping(Urls.Contractor.CONTRACTOR_BIND_AND_CHANGE_CAMPAIGN_STATUS)
    @Transactional
    fun contractorBindAndChangeCampaignStatus(@RequestParam contractorId: Long, @RequestParam campaignId: Long): ActionStatus<Contractor> {
        val contractor = contractorService.select(contractorId) ?: throw NotFoundException("contractor with id $contractorId not found")
        checkBindPermission(campaignId)
        contractorService.insertRelation(contractorId, campaignId)
        return ActionStatus.createSuccessStatus(contractor)
    }

    private fun checkBindPermission(campaignId: Long?) {
        if (!campaignPermissionService.isEditable(myProfileId(), campaignService.getCampaignSafe(campaignId)))
            throw PermissionException(MessageCode.PERMISSION_ADMIN)
    }

    private fun checkAdminPermission() {
        if (!permissionService.hasAdministrativeRole(myProfileId()))
            throw PermissionException()
    }

    @PostMapping(Urls.Contractor.CONTRACTOR_DELETE)
    fun delete(@RequestParam contractorId: Long) {
        checkAdminPermission()
        contractorService.delete(contractorId)
    }

    @GetMapping(Urls.Contractor.CONTRACTOR_BY_CAMPAIGN)
    fun selectByCampaignId(@RequestParam campaignId: Long): Contractor? {
        campaignService.getEditableCampaignSafe(myProfileId(), campaignId)
        return contractorService.selectByCampaignId(campaignId)
    }

    @GetMapping(Urls.Contractor.LAST_UPLOADED_FILES)
    fun getLastUploadedFiles(@RequestParam profileId: Long,
                             @RequestParam(defaultValue = "0") offset: Int,
                             @RequestParam(defaultValue = "10") limit: Int): List<ProfileFile> =
            profileFileService.getProfileFiles(myProfileId(), profileId, offset, limit)

    @PostMapping(Urls.Contractor.DELETE_UPLOADED_PROFILE_FILE)
    fun deleteProfileFile(@RequestParam profileId: Long,
                          @RequestParam fileId: Long): ActionStatus<*> {
        val profileFile = profileFileService.getProfileFile(myProfileId(), profileId, fileId)

        if (!permissionService.hasAdministrativeRole(myProfileId()) && getDateDiffInMinutes(profileFile.timeAdded ?: Date(), Date()) > MINUTES_FOR_BE_ABLE_TO_DELETE_UPLOADED_FILE) {
            return ActionStatus.createErrorStatus<Any>(PERMISSION_DENIED_FOR_FILE_DELETING_BY_USER.errorPropertyName, messageSource)
        }
        profileFileService.deleteProfileFile(myProfileId(), profileId, fileId)

        val extraParamsMap = HashMap<String, String>()
        extraParamsMap.put("fileUrl", profileFile.fileUrl ?: "")
        profileNewsService.addFileDeletedNews(ProfileNews.Type.PROFILE_FILE_DELETED, myProfileId(), profileFile.profileId ?: -1, extraParamsMap)

        return ActionStatus.createSuccessStatus<Any>()
    }

    private fun getDateDiffInMinutes(from: Date, to: Date): Long {
        val diff = to.time - from.time
        return diff / (60 * 1000)
    }

    companion object {
        val MINUTES_FOR_BE_ABLE_TO_DELETE_UPLOADED_FILE: Long = 60

        private fun campaignHasTag(campaign: Campaign, mnemonicName: String): Boolean {
            return IterableUtils.matchesAny(campaign.tags) { `object` -> mnemonicName == `object`.mnemonicName }
        }
    }
}

package ru.planeta.api.web.controllers.services

import org.apache.commons.io.FileUtils
import org.apache.commons.io.FilenameUtils
import org.apache.commons.lang3.StringUtils
import org.apache.log4j.Logger
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.MessageSource
import org.springframework.context.NoSuchMessageException
import org.springframework.context.i18n.LocaleContextHolder
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Service
import org.springframework.validation.BindingResult
import org.springframework.validation.DataBinder
import org.springframework.validation.Validator
import org.springframework.web.servlet.ModelAndView
import ru.planeta.api.exceptions.NotFoundException
import ru.planeta.api.exceptions.PermissionException
import ru.planeta.api.helper.ProjectService
import ru.planeta.api.model.ActionStatus
import ru.planeta.api.model.authentication.Authority
import ru.planeta.api.service.common.CustomMetaTagService
import ru.planeta.api.service.configurations.StaticNodesService
import ru.planeta.api.service.profile.AuthorizationService
import ru.planeta.api.service.profile.ProfileBalanceService
import ru.planeta.api.service.profile.ProfileService
import ru.planeta.api.service.profile.UserService
import ru.planeta.api.web.authentication.*
import ru.planeta.api.web.controllers.IAction
import ru.planeta.api.web.utils.ProperRedirectView
import ru.planeta.commons.model.OrderShortLinkStat
import ru.planeta.commons.web.CookieUtils
import ru.planeta.commons.web.IpUtils
import ru.planeta.commons.web.WebUtils
import ru.planeta.mail.MailService
import ru.planeta.model.enums.CustomMetaTagType
import ru.planeta.model.enums.ProjectType
import ru.planeta.model.stat.RefererStatType
import ru.planeta.model.stat.RequestStat
import ru.planeta.reports.SimpleReport
import java.io.ByteArrayInputStream
import java.io.File
import java.util.*
import java.util.concurrent.Executors
import java.util.concurrent.TimeUnit
import javax.activation.DataSource
import javax.mail.util.ByteArrayDataSource
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

/**
 * Base controller for all controllers
 *
 * @author ameshkov
 */
@Service
class BaseControllerService(@Value("\${noreply.email}")
                            private val noReplyEmail: String,
                            val staticNodesService: StaticNodesService,
                            val profileService: ProfileService,
                            val userService: UserService,
                            val projectService: ProjectService,
                            val authorizationService: AuthorizationService,
                            val customMetaTagService: CustomMetaTagService,
                            private val mailService: MailService,
                            val messageSource: MessageSource,
                            private val balanceService: ProfileBalanceService) {

    fun delay(task: Runnable, delay: Long, timeUnit: TimeUnit) {
        DELAYED_TASKS_EXECUTOR.schedule(task, delay, timeUnit)
    }

    fun <T> createErrorStatus(result: BindingResult): ActionStatus<T> {
        return ActionStatus.createErrorStatus(result, messageSource)
    }

    val logger: Logger = Logger.getLogger(BaseControllerService::class.java)

    /**
     * Creates default model and view object. Populates model with common attributes.
     *
     * @param action      Action name
     * @param actionName  Action name for modelAndView object
     * @param projectType Project type (MAIN for planeta.ru, PROJECTS for projects.planeta.ru, etc)
     * @return Model and view
     * @throws PermissionException Thrown when user does not have access to this action
     * @throws NotFoundException   User is not found
     */
    fun createDefaultModelAndView(action: String, actionName: String, projectType: ProjectType?): ModelAndView {
        val modelAndView = ModelAndView(action)
        modelAndView.addObject("action", actionName)

        val myProfileId = myProfileId()
        val userAuthorizationInfo = userAuthorizationInfo()
        val myProfile = if (userAuthorizationInfo != null) profileService.getProfileInfo(myProfileId, myProfileId) else profileService.anonymousProfile
        modelAndView.addAllObjects(mapOf(
                "myProfile" to myProfile,
                "myProfileProfile" to myProfile.profile,
                "aliasOrProfileId" to myProfile.profile?.webAlias,
                "staticNode" to staticNodesService.staticNode,
                "mainAppUrl" to projectService.getUrl(ProjectType.MAIN),
                "staticNodes" to staticNodesService.staticNodes,
                "projectType" to (projectType ?: ProjectType.MAIN),
                "isAuthorized" to isAuthorized(),
                "isAuthor" to ((myProfile.profile?.authorProjectsCount ?: 0) > 0),
                "hasEmail" to hasEmail(),
                "isSuperAdmin" to hasAuthority(Authority.SUPER_ADMIN),
                "userBalance" to balanceService.getBalance(myProfileId()),
                "isAdmin" to isAdmin()))

        if (userAuthorizationInfo != null) {
            modelAndView.addAllObjects(mapOf("userAuthorizationInfo" to userAuthorizationInfo,
                    "myBalance" to userService.getUserProfileBalance(myProfileId),
                    // TODO deleteByProfileId this
                    "email" to authorizationService.getUserPrivateEmailById(myProfile.profile?.profileId ?: -1)))
        } else {
            // TODO deleteByProfileId this
            modelAndView.addAllObjects(mapOf("email" to "", "myBalance" to 0.0, "frozenAmount" to 0.0))
        }


        if (projectType != null && INTERCEPTABLE_PROJECTS.contains(projectType)) {
            var uri = WebUtils.getUriWithQuery(AuthUtils.request)
            if (org.apache.commons.lang3.StringUtils.contains(uri, ".html")) {
                uri = org.apache.commons.lang3.StringUtils.remove(uri, ".html")
            }
            if (org.apache.commons.lang3.StringUtils.startsWith(uri, "/interactive")) {
                modelAndView.addObject("interactiveCampaignZone", true)
            } else {
                modelAndView.addObject("interactiveCampaignZone", false)
            }
            val customMetaTagType = CustomMetaTagType.valueOf(projectType.toString())
            val customMetaTag = customMetaTagService.selectCustomMetaTag(customMetaTagType, uri)
            if (customMetaTag != null) {
                modelAndView.addObject("customMetaTag", customMetaTag)
            }
        }
        return modelAndView
    }

    /**
     * Creates default model and view object. Populates model with common attributes.
     *
     * @throws PermissionException
     * @throws NotFoundException
     */
    fun createDefaultModelAndView(action: String, projectType: ProjectType): ModelAndView =
            createDefaultModelAndView(action, action, projectType)

    fun createDefaultModelAndView(action: IAction, projectType: ProjectType): ModelAndView =
            createDefaultModelAndView(action.path, action.actionName, projectType)

    fun createRedirectModelAndView(url: String): ModelAndView = ModelAndView(ProperRedirectView(url))

    fun sendReport(clientId: Long, report: SimpleReport) {
        val dir = tmpDirName
        val fileName = report.save(dir)
        val files = getStringDataSourceMap(fileName)
        val email = authorizationService.getUserPrivateEmailById(clientId)
        try {
            if (!mailService.isConnected) {
                mailService.openConnection()
                logger.debug("mailService connection opened")
            }
            val locale = LocaleContextHolder.getLocale()
            mailService.send(0, noReplyEmail, noReplyEmail, listOf(email),
                    messageSource.getMessage("report.campaign.report", null, locale),
                    messageSource.getMessage("report.campaign.report.inside", null, locale), files)
        } finally {
            mailService.closeConnection()
            logger.debug("mailService connection closed")
        }
    }

    fun getRequestStat(request: HttpServletRequest, response: HttpServletResponse, type: RefererStatType, projectType: ProjectType): RequestStat {
        val stat = RequestStat()
        stat.timeAdded = Date()
        stat.type = type
        stat.projectType = projectType
        stat.ip = IpUtils.getRemoteAddr(request)
        stat.cid = WebUtils.getGtmCidFromCookie(request)

        val shortLinkId = CookieUtils.getCookieValue(request.getCookies(), OrderShortLinkStat.SHORTLINK_ID_COOKIE_NAME, "0").toLong()
        if (shortLinkId != 0L) {
            stat.shortLinkId = shortLinkId
        }

        val sourceCookie = CookieUtils.getCookieValue(request.cookies, CookieUtils.SOURCE_COOKIE_NAME, null)
        if (sourceCookie != null) {
            stat.referer = sourceCookie
            CookieUtils.removeCookie(request, response, CookieUtils.SOURCE_COOKIE_NAME)
        } else {
            stat.referer = WebUtils.getReferer(request)
        }
        return stat
    }

    fun toCamelCase(list: List<Map<*, *>>): List<Map<*, *>> {
        val newList = ArrayList<Map<*, *>>()
        val names = HashMap<String, String>()
        for (map in list) {
            val newMap = HashMap<String, Any?>()
            newList.add(newMap)
            for (obj in map.keys) {
                if (obj is String) {
                    var newName: String? = names[obj]
                    if (newName == null) {
                        newName = toCamelCase(obj)
                        names.put(obj, newName)
                    }
                    newMap.put(newName, map[obj])
                } else {
                    newMap.put(obj as String, map[obj])
                }
            }
        }
        return newList
    }

    fun getErrorMessage(errorCode: String?, defaultErrorCode: String?, args: Array<Any>?): String? {
        if (errorCode != null) {
            try {
                return getParametrizedMessage(errorCode, args)
            } catch (ex: NoSuchMessageException) {
                if (logger.isDebugEnabled) {
                    logger.debug("Error code $errorCode not found")
                }
            }
        }

        return try {
            getParametrizedMessage(defaultErrorCode)
        } catch (ex: NoSuchMessageException) {
            if (logger.isDebugEnabled) {
                logger.debug("Default error code $errorCode not found")
            }
            defaultErrorCode
        }

    }

    fun getErrorMessage(errorCode: String?, defaultErrorCode: String?): String? =
            getErrorMessage(errorCode, defaultErrorCode, null)

    fun getParametrizedMessage(errorCode: String?): String =
            getParametrizedMessage(errorCode, null)

    fun getParametrizedMessage(errorCode: String?, args: Array<Any>?): String =
            messageSource.getMessage(errorCode, args, LocaleContextHolder.getLocale())


    fun createRedirectModelAndView(url: String, additionalParams: Map<String, *>): ModelAndView = ModelAndView(ProperRedirectView(url), additionalParams)

    fun createPermanentRedirectModelAndView(url: String): ModelAndView {
        val redirectView = ProperRedirectView(url)
        redirectView.setStatusCode(HttpStatus.MOVED_PERMANENTLY)
        return ModelAndView(redirectView)
    }

    fun createRedirectModelAndView(url: String, vararg objects: Any): ModelAndView {
        val map = HashMap<String, Any>()
        for (i in 0 until objects.size / 2) {
            if (objects[i * 2] !is String) {
                throw IllegalArgumentException()
            }
            map.put(objects[i * 2] as String, objects[i * 2 + 1])
        }
        return ModelAndView(ProperRedirectView(url), map)
    }

    fun validate(obj: Any?, validator: Validator?): BindingResult {
        if (obj == null || validator == null) {
            throw IllegalArgumentException()
        }
        if (!validator.supports(obj.javaClass)) {
            throw IllegalArgumentException("validator can't validate class " + obj.javaClass)
        }
        val binder = DataBinder(obj)
        binder.validator = validator
        binder.validate()
        return binder.bindingResult
    }

    companion object {
        private val DELAYED_TASKS_EXECUTOR = Executors.newSingleThreadScheduledExecutor()
        private val INTERCEPTABLE_PROJECTS = EnumSet.of(ProjectType.MAIN, ProjectType.SHOP,
                ProjectType.TV, ProjectType.CONCERT, ProjectType.SCHOOL, ProjectType.BIBLIO, ProjectType.CHARITY,
                ProjectType.PROMO)

        private fun toCamelCase(s: String): String {
            if (!s.contains("_")) {
                return s
            }
            val sb = StringBuilder(s.length)
            var isFirst = true
            for (w in s.split("_".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()) {
                if (isFirst) {
                    sb.append(w)
                } else {
                    sb.append(w.substring(0, 1).toUpperCase()).append(w.substring(1))
                }
                isFirst = false
            }
            return sb.toString()
        }

        private val tmpDirName: String
            get() {
                var dir = System.getProperty("java.io.tmpdir")
                if (StringUtils.isBlank(dir)) {
                    dir = "/tmp"
                }
                return dir
            }

        private fun getStringDataSourceMap(fileName: String): Map<String, DataSource> {
            val files = HashMap<String, DataSource>()
            val file = File(fileName)
            val ist = ByteArrayInputStream(FileUtils.readFileToByteArray(file))
            val ds = ByteArrayDataSource(ist, "application/octet-stream")
            ds.name = FilenameUtils.getName(fileName)
            files.put(FilenameUtils.getBaseName(fileName), ds)
            return files
        }
    }
}


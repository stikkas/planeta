package ru.planeta.eva.api.web.controllers

import org.apache.log4j.Logger
import org.springframework.beans.factory.annotation.Value
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler
import org.springframework.validation.BindingResult
import org.springframework.validation.FieldError
import org.springframework.web.bind.annotation.*
import ru.planeta.api.exceptions.PermissionException
import ru.planeta.api.helper.ProjectService
import ru.planeta.api.model.json.Registration
import ru.planeta.api.service.profile.AuthorizationService
import ru.planeta.api.service.registration.RegistrationService
import ru.planeta.api.utils.ValidateUtils
import ru.planeta.api.web.authentication.getAuthentication
import ru.planeta.api.web.authentication.isAnonymous
import ru.planeta.api.web.authentication.userAuthorizationInfo
import ru.planeta.api.web.controllers.services.ControllerAutoLoginWrapService
import ru.planeta.eva.api.web.ActionStatus
import ru.planeta.eva.api.web.Urls
import ru.planeta.eva.api.web.cas.handlers.CustomAuthenticationSuccessHandler
import ru.planeta.eva.api.web.cas.handlers.CustomLogoutSuccessHandler
import ru.planeta.model.enums.ProjectType
import ru.planeta.model.stat.RefererStatType
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse
import javax.validation.Valid


/**
 * User: s.makarov
 * Date: 22.09.14
 * Time: 13:38
 */

class UserLoginData {
    var email: String? = null
    var password: String? = null
}

@RestController("EvaWebApiAuthController")
class AuthController(private val registrationService: RegistrationService,
                     private val authorizationService: AuthorizationService,
                     private val controllerAutoLoginWrapService: ControllerAutoLoginWrapService,
                     @Value("\${project.type:MAIN}")
                     private val projectType: ProjectType,
                     private val projectService: ProjectService,
                     private val authenticationSuccessHandler: CustomAuthenticationSuccessHandler,
                     private val logoutSuccessHandler: CustomLogoutSuccessHandler) {

    @GetMapping(Urls.USER_INFO)
    fun userInfo() = ActionStatus(result = userAuthorizationInfo())

    @PostMapping(Urls.LOGOUT)
    fun logout(req: HttpServletRequest, resp: HttpServletResponse): ActionStatus {
        getAuthentication()?.let {
            SecurityContextLogoutHandler().logout(req, resp, null)
            logoutSuccessHandler.removeAuthCookie(req, resp)
        }
        return ActionStatus()
    }

    @PostMapping(Urls.LOGIN)
    fun login(@RequestBody userLoginData: UserLoginData,
              request: HttpServletRequest, response: HttpServletResponse): ActionStatus {
        val username = userLoginData.email ?: ""
        val password = userLoginData.password

        val registration = Registration()
        registration.email = username
        registration.password = password

        var ipAddress = request.getHeader("X-FORWARDED-FOR")
        if (ipAddress == null) {
            ipAddress = request.remoteAddr
        }
        logger.info("Try to login from [" + (ipAddress ?: "unknown IP") + "] as [" + username + "]")

        if (isAnonymous()) {
            val userPrivateInfo = authorizationService.getUserPrivateInfoByUsername(username)
            if (!authorizationService.checkPassword(userPrivateInfo, password ?: "")) {
                return ActionStatus(FieldError("signin", "password", "login.error.signin"))
            } else {
                controllerAutoLoginWrapService.autoLoginUser(username, request)
                authenticationSuccessHandler.onAuthenticationSuccess(request, response, getAuthentication())
            }
        }
        return userInfo()
    }

    @PostMapping(Urls.SIGNUP)
    fun signup(@Valid @RequestBody registration: Registration, result: BindingResult,
               request: HttpServletRequest, response: HttpServletResponse): ActionStatus {
        if (!isAnonymous()) {
            return userInfo()
        }
        return if (result.hasErrors()) {
            ActionStatus(result)
        } else {
            try {
                val userAuthorizationInfo = registrationService.registerByEmail(registration.email, registration.password,
                        controllerAutoLoginWrapService.baseControllerService.getRequestStat(request, response, RefererStatType.REGISTRATION, projectType))
                controllerAutoLoginWrapService.autoLoginUser(userAuthorizationInfo.username, request)
                authenticationSuccessHandler.onAuthenticationSuccess(request, response, getAuthentication())
                userInfo()
            } catch (ex: PermissionException) {
                logger.error("Cannot register user", ex)
                ActionStatus(FieldError("registration", "username", ex.message))
            } catch (ex: Exception) {
                logger.error("Cannot register user", ex)
                ActionStatus(FieldError("registration", "password", "login.error.signup"))
            }
        }
    }

    @PostMapping(Urls.RECOVER)
    fun recover(@RequestParam email: String,
                @RequestParam(defaultValue = "") redirectUrl: String): ActionStatus {
        var email = email
        try {
            if (!isValidEmail(email)) {
                return ActionStatus(FieldError("recover", "email", "wrong.email"))
            }

            email = email.toLowerCase()
            authorizationService.getUserPrivateInfoByUsername(email)
                    ?: return ActionStatus(FieldError("recover", "email", "login.error.email_not_exists"))
            registrationService.recoverPassword(email, projectService.getUrl(ProjectType.MAIN), redirectUrl)
        } catch (ex: Exception) {
            logger.error("Cannon recover email", ex)
            return ActionStatus(FieldError("recover", "email", "login.error.unexpected_error"))
        }

        return ActionStatus()
    }

    private fun isValidEmail(email: String?): Boolean {
        return email != null && ValidateUtils.isValidEmail(email)
    }

    private val logger = Logger.getLogger(AuthController::class.java)
}




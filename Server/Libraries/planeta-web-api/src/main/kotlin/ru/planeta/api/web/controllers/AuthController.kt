package ru.planeta.api.web.controllers

import org.apache.log4j.Logger
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.MessageSource
import org.springframework.context.i18n.LocaleContextHolder
import org.springframework.validation.BindingResult
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RestController
import ru.planeta.api.exceptions.MessageCode
import ru.planeta.api.exceptions.PermissionException
import ru.planeta.api.model.ActionStatus
import ru.planeta.api.model.json.Registration
import ru.planeta.api.service.profile.AuthorizationService
import ru.planeta.api.service.registration.RegistrationService
import ru.planeta.eva.api.web.cas.handlers.CustomAuthenticationSuccessHandler
import ru.planeta.api.web.authentication.getAuthentication
import ru.planeta.api.web.authentication.isAnonymous
import ru.planeta.api.web.controllers.services.BaseControllerService
import ru.planeta.api.web.controllers.services.ControllerAutoLoginWrapService
import ru.planeta.model.enums.ProjectType
import ru.planeta.model.stat.RefererStatType
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse
import javax.validation.Valid


/**
 * User: s.makarov
 * Date: 22.09.14
 * Time: 13:38
 */

class UserLoginData {
    var username: String? = null
    var password: String? = null
}

@RestController
class AuthController(private val registrationService: RegistrationService,
                     private val authorizationService: AuthorizationService,
                     private val controllerAutoLoginWrapService: ControllerAutoLoginWrapService,
                     private val baseControllerService: BaseControllerService,
                     private val messageSource: MessageSource,
                     @Value("\${project.type:MAIN}")
                     private val projectType: ProjectType,
                     private val authenticationSuccessHandler: CustomAuthenticationSuccessHandler) {

    @PostMapping(Urls.ACCOUNT_LOGIN)
    fun login(userLoginData: UserLoginData,
              request: HttpServletRequest, response: HttpServletResponse): ActionStatus<*> {
        val username = userLoginData.username ?: ""
        val password = userLoginData.password

        val registration = Registration()
        registration.email = username
        registration.password = password

        var ipAddress = request.getHeader("X-FORWARDED-FOR")
        if (ipAddress == null) {
            ipAddress = request.remoteAddr
        }
        logger.info("Try to login from [" + (ipAddress ?: "unknown IP") + "] as [" + username + "]")

        if (isAnonymous()) {
            val userPrivateInfo = authorizationService.getUserPrivateInfoByUsername(username)
            return if (authorizationService.checkPassword(userPrivateInfo, password?: "")) {
                controllerAutoLoginWrapService.autoLoginUser(username, request)
                authenticationSuccessHandler.onAuthenticationSuccess(request, response, getAuthentication())
                ActionStatus.createSuccessStatus(registration)
            } else {
                val errorMessage = messageSource.getMessage(
                        MessageCode.ERROR_AUTHENTICATION_CREDENTIALS_BAD_USERNAMEORPASSWORD.errorPropertyName,
                        null, LocaleContextHolder.getLocale())
                ActionStatus.createErrorStatus<Any>(errorMessage)
                        .addFieldError("username", errorMessage)
                        .addFieldError("password", errorMessage)
            }
        }
        return ActionStatus.createSuccessStatus(registration)
    }

    @PostMapping(Urls.ACCOUNT_SIGNUP)
    fun signup(@Valid registration: Registration, result: BindingResult,
               request: HttpServletRequest, response: HttpServletResponse): ActionStatus<*> {
        if (!isAnonymous()) {
            return ActionStatus.createSuccessStatus(registration)
        }

        return if (result.hasErrors()) {
            ActionStatus.createErrorStatus<Any>(result, messageSource)
        } else {
            try {
                val userAuthorizationInfo = registrationService.registerByEmail(registration.email, registration.password,
                        baseControllerService.getRequestStat(request, response, RefererStatType.REGISTRATION, projectType))
                controllerAutoLoginWrapService.autoLoginUser(userAuthorizationInfo.username, request)
                authenticationSuccessHandler.onAuthenticationSuccess(request, response, getAuthentication())
                ActionStatus.createSuccessStatus(registration)
            } catch (ex: PermissionException) {
                logger.error("Cannot register user", ex)
                ActionStatus.createErrorStatus<Any>(ex.message, messageSource)
            } catch (ex: Exception) {
                logger.error("Cannot register user", ex)
                ActionStatus.createErrorStatus<Any>("registration.error.unexpected", messageSource)
            }

        }
    }

    private val logger = Logger.getLogger(AuthController::class.java)
}




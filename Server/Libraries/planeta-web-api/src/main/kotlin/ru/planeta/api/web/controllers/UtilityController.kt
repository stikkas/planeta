package ru.planeta.api.web.controllers

import org.apache.log4j.Logger
import org.springframework.context.i18n.LocaleContextHolder
import org.springframework.web.bind.annotation.*
import org.springframework.web.servlet.ModelAndView
import ru.planeta.api.model.ActionStatus
import ru.planeta.api.service.common.SubscribersService
import ru.planeta.api.service.content.SmartFormatterService
import ru.planeta.api.service.geo.GeoService
import ru.planeta.api.service.statistic.UniversalAnalyticsService
import ru.planeta.api.text.FormattingService
import ru.planeta.api.text.HtmlValidator
import ru.planeta.api.text.Message
import ru.planeta.api.utils.ValidateUtils
import ru.planeta.api.web.authentication.myProfileId
import ru.planeta.api.web.controllers.services.BaseControllerService
import ru.planeta.api.web.utils.BarcodeUtils
import ru.planeta.commons.web.WebUtils
import ru.planeta.model.Constants
import ru.planeta.model.enums.CustomMetaTagType
import ru.planeta.model.enums.EmailStatus
import ru.planeta.model.commondb.CustomMetaTag
import ru.planeta.model.profile.location.City
import ru.planeta.model.profile.location.Region
import java.io.IOException
import java.io.UnsupportedEncodingException
import java.net.MalformedURLException
import java.net.URL
import java.net.URLEncoder
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

/**
 * Controller for utility methods.
 * For now it is just bbcode-to-html transformation
 *
 * @author ameshkov
 */
@RestController
class UtilityController(private val geoService: GeoService,
                        private val subscribersService: SubscribersService,
                        private val smartFormatterService: SmartFormatterService,
                        private val universalAnalyticsService: UniversalAnalyticsService,
                        private val baseControllerService: BaseControllerService) {

    @GetMapping(Urls.COUNTRIES_LIST_JSON)
    fun countriesListBySubstring(): ActionStatus<*> = ActionStatus.createSuccessStatus(geoService.getCountriesNew(false))

    @GetMapping(Urls.COUNTRIES_LIST_IN_CAMPAIGNS_JSON)
    fun countriesInCampaignListBySubstring(): ActionStatus<*> = ActionStatus.createSuccessStatus(geoService.getCountriesNew(true))

    @PostMapping(Urls.ADD_SUBSCRIBER)
    fun addSubscriber(@RequestParam subscriptionCode: String, @RequestParam email: String): ActionStatus<*> {
        subscribersService.subscribe(subscriptionCode, email)
        return ActionStatus.createSuccessStatus<Any>()
    }

    @GetMapping(Urls.BARCODE)
    fun generateBarcode(@RequestParam orderId: Long, @RequestParam(defaultValue = "0") orderObjectId: Long,
                        response: HttpServletResponse) {
        response.status = HttpServletResponse.SC_OK
        response.contentType = PNG_CONTENT_TYPE
        BarcodeUtils.generatePicture(response.outputStream, orderId, orderObjectId)
    }

    @GetMapping(Urls.BARCODE_EAN13)
    fun generateEAN13Barcode(@RequestParam code: String, response: HttpServletResponse) {
        response.status = HttpServletResponse.SC_OK
        response.contentType = PNG_CONTENT_TYPE
        BarcodeUtils.generateBarcode(response.outputStream, code, true)
    }

    @GetMapping(Urls.QRCODE)
    fun generateQRcode(@RequestParam ticketUrl: String, @RequestParam width: Int,
                       @RequestParam height: Int, request: HttpServletRequest, response: HttpServletResponse) {
        response.status = HttpServletResponse.SC_OK
        response.contentType = PNG_CONTENT_TYPE
        BarcodeUtils.generateQRPicture(response.outputStream, ticketUrl, height, width)
    }

    @GetMapping(Urls.CITIES_LIST_BY_SUBSTRING_JSON)
    fun getCitiesListBySubstring(@RequestParam countryId: Int,
                                 @RequestParam regionId: Int,
                                 @RequestParam(value = "q") citySubstr: String,
                                 @RequestParam(defaultValue = "20") limit: Int,
                                 request: HttpServletRequest, response: HttpServletResponse): List<City> =
            if (regionId != 0) {
                geoService.getRegionCitiesBySubstring(regionId.toLong(), citySubstr, 0, limit)
            } else {
                geoService.getCountryCitiesBySubstring(countryId.toLong(), citySubstr, 0, limit)
            }

    @GetMapping(Urls.REGIONS_LIST_BY_SUBSTRING_JSON)
    fun getRegionsListBySubstring(@RequestParam countryId: Int,
                                  @RequestParam(value = "q") regionSubstr: String,
                                  @RequestParam(defaultValue = "20") limit: Int,
                                  request: HttpServletRequest, response: HttpServletResponse): List<Region> =
            geoService.getCountryRegionsBySubstring(countryId, regionSubstr,
                    LocaleContextHolder.getLocale().language.toLowerCase(), 0, limit)

    @GetMapping(Urls.CITY_BY_ID_JSON)
    fun getCityById(@RequestParam cityId: Int, request: HttpServletRequest, response: HttpServletResponse): City =
            geoService.getCityById(cityId)

    @GetMapping(Urls.CROP_IMAGE)
    fun cropImage(@RequestParam url: String, request: HttpServletRequest, response: HttpServletResponse): String {
        var staticNode = baseControllerService.staticNodesService.getStaticNodeOrCurrent(url)
        staticNode = WebUtils.appendProtocolIfNecessary(staticNode, false)
        val urlString = staticNode + "/cropImage.html?" + request.queryString
        return WebUtils.downloadString(urlString)
    }

    @PostMapping(Urls.UPLOAD_AVATAR)
    fun uploadAvatar(request: HttpServletRequest, response: HttpServletResponse): String {
        try {
            val profileId = myProfileId()
            var staticNode = baseControllerService.staticNodesService.staticNode
            staticNode = WebUtils.appendProtocolIfNecessary(staticNode, false)
            val urlString = "$staticNode/uploadImage?clientId=$profileId&ownerId=$profileId&albumId=0&albumTypeId=${Constants.ALBUM_AVATAR}&userfile1=webcam.jpg"

            return WebUtils.uploadStream(urlString, request.inputStream, request.contentType, true)

        } catch (ex: Exception) {
            log.error("Error occurred", ex)
            throw IOException("avatar.error")
        }
    }

    @PostMapping(Urls.PARSE_MESSAGE)
    fun parseMessage(@RequestParam message: String, @RequestParam(value = "profileId", defaultValue = "0") ownerProfileId: Long,
                     request: HttpServletRequest): Message {
        var ownerProfileId = ownerProfileId

        if (ownerProfileId == 0L) {
            ownerProfileId = myProfileId()
        }
        return smartFormatterService.extractAttachments(myProfileId(), ownerProfileId, message, request.serverName)
    }

    @PostMapping(Urls.HTML_CLEAN)
    fun htmlClean(@RequestParam(value = "html") dirtyHtml: String): ActionStatus<String> =
            ActionStatus.createSuccessStatus(HtmlValidator.cleanHtml(dirtyHtml))

    @GetMapping(Urls.EMAIL_CHECK)
    fun emailCheck(@RequestParam email: String, request: HttpServletRequest,
                   response: HttpServletResponse): ActionStatus<EmailStatus> =
            if (!ValidateUtils.isValidEmail(email)) {
                ActionStatus.createSuccessStatus(EmailStatus.NOT_VALID)
            } else ActionStatus.createSuccessStatus(baseControllerService.authorizationService.checkEmail(email))

    @GetMapping(Urls.AWAY_LINK)
    fun externalLink(@RequestParam(value = FormattingService.AWAY_PARAMETER) url: String,
                     request: HttpServletRequest, response: HttpServletResponse): ModelAndView {
        var url = url
        val googleSearchTemplate = "http://google.com/#q="
        url = WebUtils.appendProtocolIfNecessary(url, false)
        try {
            val parsedUrl = URL(url)
        } catch (e: MalformedURLException) {
            return try {
                baseControllerService.createRedirectModelAndView(googleSearchTemplate + URLEncoder.encode(url, "UTF-8"))
            } catch (ignored: UnsupportedEncodingException) {
                baseControllerService.createRedirectModelAndView("/")
            }

        }
        universalAnalyticsService.trackExternalLink(myProfileId(), url)
        return ModelAndView("includes/generated/external-redirect")
                .addObject("targetUrl", url)
    }

    @GetMapping(Urls.AWAY_LINK_MAIL)
    fun externalMailLink(@RequestParam(value = FormattingService.AWAY_PARAMETER) url: String,
                         request: HttpServletRequest, response: HttpServletResponse): ModelAndView {
        var url = url
        url = WebUtils.appendProtocolIfNecessary(url, false)
        try {
            universalAnalyticsService.trackExternalLink(myProfileId(), url)
        } catch (e: MalformedURLException) {
            return baseControllerService.createRedirectModelAndView("/")
        }

        return baseControllerService.createRedirectModelAndView(url)
    }

    @GetMapping(Urls.CUSTOM_META_TAG_FOR_PAGE)
    fun getCustomMetaTagForPage(@RequestParam customMetaTagType: CustomMetaTagType,
                                @RequestParam tagPath: String): ActionStatus<*> =
            ActionStatus.createSuccessStatus<CustomMetaTag>(baseControllerService.customMetaTagService.selectCustomMetaTag(customMetaTagType, tagPath))

    companion object {
        private val log = Logger.getLogger(UtilityController::class.java)
        private val PNG_CONTENT_TYPE = "image/png"
    }
}

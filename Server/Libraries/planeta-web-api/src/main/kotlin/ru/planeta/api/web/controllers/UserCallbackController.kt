package ru.planeta.api.web.controllers

import org.springframework.web.bind.annotation.*
import ru.planeta.api.model.ActionStatus
import ru.planeta.api.service.common.UserCallbackService
import ru.planeta.api.web.authentication.myProfileId
import ru.planeta.api.web.models.CallMeInfo
import ru.planeta.model.common.UserCallback
import ru.planeta.model.enums.OrderObjectType
import java.math.BigDecimal

/**
 * Created by eshevchenko.
 */
@RestController
class UserCallbackController(private val callbackService: UserCallbackService) {

    @GetMapping(Urls.Callback.IS_AVAILABLE)
    fun isCallbacksAvailable(@RequestParam amount: BigDecimal): ActionStatus<Boolean> {
        return ActionStatus.createSuccessStatus(callbackService.isCallbacksAvailable(amount))
    }

    @PostMapping(Urls.Callback.CREATE)
    fun createCallback(@RequestBody callMeInfo: CallMeInfo): ActionStatus<UserCallback> {
        val userId = myProfileId()
        var res: UserCallback? = null
        val orderId: Long?
        when (callMeInfo.type) {
            UserCallback.Type.FAILURE_PAYMENT -> {
                res = callbackService.createFailurePaymentCallback(userId, callMeInfo.phone, callMeInfo.paymentId, callMeInfo.amount)
                orderId = callbackService.getOrderIdByPaymentId(callMeInfo.paymentId)
            }
            UserCallback.Type.ORDERING_PROBLEM -> {
                res = callbackService.createOrderingProblemCallback(userId, callMeInfo.phone, callMeInfo.orderType ?: OrderObjectType.BOOK, callMeInfo.objectId, callMeInfo.amount)
                orderId = callMeInfo.objectId
            }
            else -> orderId = null
        }
        if (res != null) {
            callbackService.sendSmsToManager(callMeInfo.phone, callMeInfo.amount.toString(), orderId)
        }

        return ActionStatus.createSuccessStatus(res)
    }

    @PostMapping(Urls.Callback.PROCESS)
    fun processCallback(@RequestParam userCallbackId: Long): ActionStatus<UserCallback> {
        val callback = callbackService.processUserCallback(myProfileId(), userCallbackId)
        return ActionStatus.createSuccessStatus(callback)
    }
}

package ru.planeta.eva.api.web.validation.validators

import org.springframework.stereotype.Component
import org.springframework.validation.Errors
import org.springframework.validation.ValidationUtils
import org.springframework.validation.Validator
import ru.planeta.api.utils.ValidateUtils
import ru.planeta.eva.api.web.dto.CustomerContactDTO

@Component
class CustomerContactDTOValidator : Validator {

    override fun supports(clazz: Class<*>): Boolean {
        return CustomerContactDTO::class.java.isAssignableFrom(clazz)
    }

    override fun validate(target: Any?, errors: Errors) {
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "customerName", "field_required")
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "phone", "field_required")
    }
}


package ru.planeta.eva.api.web.validation.validators

import org.springframework.security.core.context.SecurityContext
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.stereotype.Component
import org.springframework.validation.Errors
import org.springframework.validation.Validator
import ru.planeta.api.model.authentication.AuthenticatedUserDetails
import ru.planeta.api.service.profile.AuthorizationService
import ru.planeta.api.service.security.PermissionService
import ru.planeta.api.web.authentication.myProfileId
import ru.planeta.api.web.utils.SessionUtils
import ru.planeta.eva.api.web.models.PaymentData
import ru.planeta.model.enums.EmailStatus

@Component
class AuthShareDTOValidator(private val authorizationService: AuthorizationService) : Validator {

    override fun supports(clazz: Class<*>) = false

    override fun validate(target: Any, errors: Errors) {

        val purchase = target as PaymentData

        val emailStatus = authorizationService.checkEmail(purchase.email ?: "")
        if (myProfileId() == PermissionService.ANONYMOUS_USER_PROFILE_ID) {
            if (EmailStatus.ALREADY_EXIST == emailStatus) {
                errors.rejectValue("email", "registration_current_email_already_exists")
            }
        } else {
            var securityContext: SecurityContext? = SessionUtils.session.getAttribute("SPRING_SECURITY_CONTEXT") as SecurityContext
            if (securityContext == null) {
                securityContext = SecurityContextHolder.getContext()
            }
            val principal = securityContext?.authentication?.principal
            if (principal == null || principal is String) {
                errors.rejectValue("email", "session_expired")
            } else {
                val authenticatedUserDetails = principal as AuthenticatedUserDetails
                val email = authenticatedUserDetails.userAuthorizationInfo.userPrivateInfo.email
                if (email.isNullOrEmpty()) {
                    if (EmailStatus.ALREADY_EXIST == emailStatus) {
                        errors.rejectValue("email", "registration_current_email_already_exists")
                    }
                } else if (!email.equals(purchase.email, ignoreCase = true)) {
                    errors.rejectValue("email", "wrong_email")
                }
            }
        }
    }
}


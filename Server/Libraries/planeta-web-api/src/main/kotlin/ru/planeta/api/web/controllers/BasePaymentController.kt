package ru.planeta.api.web.controllers

import org.apache.commons.lang3.StringUtils
import org.apache.log4j.Logger
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.*
import org.springframework.web.servlet.ModelAndView
import ru.planeta.api.Utils.empty
import ru.planeta.api.aspect.logging.BillingLoggableRequest
import ru.planeta.api.exceptions.NotFoundException
import ru.planeta.api.helper.ProjectService
import ru.planeta.api.log.DBLogger
import ru.planeta.api.log.LoggerListProxy
import ru.planeta.api.log.service.DBLogService
import ru.planeta.api.model.ActionStatus
import ru.planeta.api.news.LoggerService
import ru.planeta.api.remote.PaymentGateService
import ru.planeta.api.service.billing.order.OrderService
import ru.planeta.api.service.billing.payment.PaymentService
import ru.planeta.api.service.billing.payment.settings.PaymentSettingsService
import ru.planeta.api.service.campaign.CampaignService
import ru.planeta.api.service.campaign.CampaignStatsService
import ru.planeta.api.service.profile.ProfileService
import ru.planeta.api.service.profile.ProfileSubscriptionService
import ru.planeta.api.service.security.PermissionService
import ru.planeta.api.service.shop.ProductUsersService
import ru.planeta.api.utils.OrderUtils
import ru.planeta.api.web.authentication.isAnonymous
import ru.planeta.api.web.authentication.myProfileId
import ru.planeta.api.web.controllers.services.BaseControllerService
import ru.planeta.api.web.controllers.services.BasePaymentControllerService
import ru.planeta.api.web.controllers.services.BasePaymentService
import ru.planeta.api.web.utils.ProperRedirectView
import ru.planeta.commons.web.WebUtils
import ru.planeta.model.common.Order
import ru.planeta.model.common.OrderObjectInfo
import ru.planeta.model.common.PaymentMethod
import ru.planeta.model.common.TopayTransaction
import ru.planeta.model.common.campaign.GtmCampaignInfo
import ru.planeta.model.common.campaign.enums.ShareStatus
import ru.planeta.model.enums.OrderObjectType
import ru.planeta.model.enums.ProjectType
import ru.planeta.model.news.ProfileNews
import ru.planeta.model.stat.log.LoggerType
import java.util.*
import javax.servlet.http.HttpServletResponse


/**
 * Base payment controller.<br></br>
 * Implements common logic for payments creating.<br></br>
 * Processes payment fail and success requests.<br></br>
 * User: eshevchenko
 */
@Controller
class BasePaymentController(@Value("\${payment.fail.action.name:includes/generated/payment-fail}")
                            private val paymentFailActionName: String,
                            @Value("\${project.type:MAIN}")
                            private val projectType: ProjectType,
                            private val campaignService: CampaignService,
                            private val paymentSettingsService: PaymentSettingsService,
                            private val paymentGateService: PaymentGateService,
                            private val paymentService: PaymentService,
                            private val profileNewsService: LoggerService,
                            private val orderService: OrderService,
                            private val campaignStatService: CampaignStatsService,
                            private val profileSubscriptionService: ProfileSubscriptionService,
                            private val productUsersService: ProductUsersService,
                            private val permissionService: PermissionService,
                            private val projectService: ProjectService,
                            private val profileService: ProfileService,
                            private val baseControllerService: BaseControllerService,
                            @Autowired(required = false) // not all projects use this controller
                            private val basePaymentService: BasePaymentService?,
                            private val basePaymentControllerService: BasePaymentControllerService,
                            dbLogService: DBLogService) {

    val logger: Logger = LoggerListProxy("paymentControllerLogger", baseControllerService.logger, DBLogger.getLogger(LoggerType.PAYMENT, dbLogService))

    protected object Params {
        const val TRANSACTION_ID = "transactionId"
        const val SIGN = "s"
        const val PHONE = "phone"
        const val ORDER_ID = "orderId"
    }

    @GetMapping(Urls.PAYMENT_METHODS)
    @ResponseBody
    fun getPaymentMethods(@RequestParam(defaultValue = "false") internal: Boolean,
                          @RequestParam(required = false) shareId: Long?): ActionStatus<List<PaymentMethod>> {
        if (shareId != null) {
            val share = campaignService.getShare(shareId)
            if (share != null) {
                when (share.shareStatus) {
                    ShareStatus.INVESTING,
                    ShareStatus.INVESTING_WITHOUT_MODERATION ->
                        return ActionStatus.createSuccessStatus(paymentSettingsService.getPaymentMethods(ProjectType.INVEST, internal, false))
                    ShareStatus.INVESTING_ALL_ALLOWED ->
                        return ActionStatus.createSuccessStatus(paymentSettingsService.getPaymentMethods(ProjectType.INVEST_ALL_ALLOWED, internal, false))
                    else -> {
                    }
                }
            }
        }

        return ActionStatus.createSuccessStatus(paymentSettingsService.getPaymentMethods(projectType, internal, false))
    }

    @GetMapping(Urls.PAYMENT_METHODS_OLD) // После перехода на новую платежку всех проектов это можно убрать.
    @ResponseBody
    fun getPaymentMethodsOld(@RequestParam(defaultValue = "false") internal: Boolean,
                             @RequestParam(required = false) shareId: Long?): ActionStatus<List<PaymentMethod>> {
        if (shareId != null) {
            val share = campaignService.getShare(shareId)
            if (share != null) {
                when (share.shareStatus) {
                    ShareStatus.INVESTING,
                    ShareStatus.INVESTING_WITHOUT_MODERATION ->
                        return ActionStatus.createSuccessStatus(paymentSettingsService.getPaymentMethods(ProjectType.INVEST, internal, false))
                    ShareStatus.INVESTING_ALL_ALLOWED ->
                        return ActionStatus.createSuccessStatus(paymentSettingsService.getPaymentMethods(ProjectType.INVEST_ALL_ALLOWED, internal, false))
                    else -> {
                    }
                }
            }
        }
        return ActionStatus.createSuccessStatus(paymentSettingsService.getPaymentMethodsOld(projectType, internal, false))
    }

    @BillingLoggableRequest
    @RequestMapping(value = Urls.Payment.PAYMENT_RECREATE, method = [RequestMethod.GET, RequestMethod.POST])
    fun createPaymentByOrder(@RequestParam transactionId: Long,
                             @RequestParam paymentMethodId: Long,
                             @RequestParam(required = false) phone: String?): ModelAndView =
            basePaymentControllerService.createPaymentRedirectModelAndView(paymentService.createPaymentFrom(transactionId, paymentMethodId, phone ?: ""))

    @BillingLoggableRequest
    @RequestMapping(value = Urls.Payment.PAYMENT_SUCCESS, method = [RequestMethod.GET, RequestMethod.POST])
    fun paymentSuccess(@RequestParam(value = Params.TRANSACTION_ID, defaultValue = "0") transactionId: Long,
                       @RequestParam(value = Params.ORDER_ID, defaultValue = "0") orderId: Long,
                       response: HttpServletResponse): ModelAndView {
        WebUtils.setNoCacheResponseHeaders(response)
        val result: ModelAndView
        val myProfileId = myProfileId()
        val isInternalPayment = orderId > 0 && transactionId == 0L
        if (isInternalPayment) {
            result = createInternalPaymentSuccessModelAndView(myProfileId, orderId)
        } else {
            val transaction = paymentService.getPayment(transactionId)
            if (isAccessiblePayment(myProfileId, transaction)) {
                result = createPaymentSuccessModelAndView(transaction)
                profileNewsService.addProfileNews(ProfileNews.Type.SUCCESS_PAGE_SHOW, myProfileId, transactionId)
            } else {
                result = ModelAndView(ProperRedirectView("/"))
            }
        }
        return result
    }

    private fun createPaymentSuccessModelAndView(transaction: TopayTransaction): ModelAndView {
        val order = if (transaction.orderId == 0L) null else orderService.getOrder(transaction.orderId)
        val result = baseControllerService.createDefaultModelAndView(basePaymentService?.getPaymentSuccessActionName(order)
                ?: "", projectType)
        val autoRedirectUri = if (order != null) Urls.Member.ACCOUNT_PURCHASES else Urls.Member.ACCOUNT_PAYMENTS
        val paymentMethod = paymentService.getPaymentMethod(transaction.paymentMethodId)
        val provider = paymentService.getPaymentProvider(transaction.paymentProviderId)
        if (paymentMethod == null || provider == null) {
            throw NotFoundException("payment method or payment provider not found")
        }
        if (order != null) {
            fillOrderInfo(result, order)
            basePaymentControllerService.addDeferredComment(transaction.orderId, paymentMethod.isMobile)
        }
        val profile = profileService.getProfile(myProfileId())
        return result.addAllObjects(mapOf("profile" to profile,
                "transaction" to transaction,
                "myProfileId" to (profile?.profileId ?: -1),
                "redirectUrl" to projectService.getUrl(ProjectType.MAIN, autoRedirectUri),
                "isMobilePayment" to paymentMethod.isMobile,
                "isDeferred" to provider.isDeferred))
    }

    private fun createInternalPaymentSuccessModelAndView(profileId: Long, orderId: Long): ModelAndView {
        val order = orderService.getOrderSafe(profileId, orderId)
        val result = baseControllerService.createDefaultModelAndView(basePaymentService?.getPaymentSuccessActionName(order)
                ?: "", projectType)
        val autoRedirectUri = Urls.Member.ACCOUNT_PURCHASES
        fillOrderInfo(result, order)
        basePaymentControllerService.addDeferredComment(orderId, false)
        return result.addAllObjects(mapOf("myProfileId" to profileId,
                "redirectUrl" to projectService.getUrl(ProjectType.MAIN, autoRedirectUri),
                "isDeferred" to false,
                "isMobilePayment" to false))
    }

    @BillingLoggableRequest
    @RequestMapping(value = Urls.Payment.PAYMENT_FAIL, method = [RequestMethod.GET, RequestMethod.POST])
    fun paymentFail(@RequestParam(value = Params.TRANSACTION_ID) transactionId: Long,
                    response: HttpServletResponse): ModelAndView {
        WebUtils.setNoCacheResponseHeaders(response)
        val result = baseControllerService.createDefaultModelAndView(paymentFailActionName, projectType)
        val transaction = paymentService.getPayment(transactionId)
        val paymentMethod = paymentService.getPaymentMethod(transaction.paymentMethodId)
        if (transaction.orderId > 0) {
            fillOrderInfo(result, transaction.profileId, transaction.orderId)
            basePaymentControllerService.removeDeferredComment(transaction.orderId)
        }

        profileNewsService.addProfileNews(ProfileNews.Type.FAIL_PAGE_SHOW, myProfileId(), transactionId)
        return result.addAllObjects(mapOf("payment" to transaction,
                "paymentFailureUrl" to basePaymentService?.getPaymentSourceUrl(transaction),
                "isCardPayment" to paymentMethod.isCard))
    }

    @BillingLoggableRequest
    @RequestMapping(value = Urls.Payment.PAYMENT_FAIL_SIMPLE, method = [RequestMethod.GET, RequestMethod.POST])
    fun paymentFailEasy(response: HttpServletResponse): ModelAndView {
        WebUtils.setNoCacheResponseHeaders(response)
        return baseControllerService.createDefaultModelAndView("includes/generated/payment-fail-easy", projectType)
    }

    private fun fillOrderInfo(modelAndView: ModelAndView, order: Order) {
        val gtmCampaignInfoList: List<GtmCampaignInfo>
        val clientId = order.buyerId
        val orderInfo = orderService.getOrderInfo(order)
        val orderType = orderInfo.orderType
        val orderObjects = orderService.getOrderObjectsInfo(clientId, orderInfo.orderId)
        val firstObject = if (!empty(orderObjects)) orderObjects?.iterator()?.next() else null

        modelAndView.addAllObjects(mapOf("fromShop" to (orderType == OrderObjectType.PRODUCT),
                "fromConcert" to (orderType == OrderObjectType.TICKET),
                "fromStart" to (orderType == OrderObjectType.SHARE),
                "order" to orderInfo,
                "objects" to OrderUtils.joinOrderObjectsByObjectId(orderObjects),
                "firstObject" to firstObject,
                "sharingDescription" to getSharingDescription(firstObject),
                "paymentType" to orderInfo.paymentType))

        when (orderType) {
            OrderObjectType.INVESTING -> {
                gtmCampaignInfoList = campaignService.getGtmCampaignInfoList(if (firstObject == null) listOf() else listOf(firstObject.ownerId))
                modelAndView.addObject("gtmCampaignInfo", if (gtmCampaignInfoList.isNotEmpty()) gtmCampaignInfoList[0] else null)
            }
            OrderObjectType.INVESTING_WITH_ALL_ALLOWED_INVOICE,
            OrderObjectType.INVESTING_WITHOUT_MODERATION -> {
                modelAndView.addAllObjects(mapOf("campaign" to campaignService.getCampaignSafe(firstObject?.ownerId
                        ?: -1),
                        "isSubscribeded" to profileSubscriptionService.doISubscribeYou(order.buyerId,
                                campaignService.getCampaignSafe(firstObject?.ownerId ?: -1).creatorProfileId),
                        "autoLoadDelay" to 3))
                gtmCampaignInfoList = campaignService.getGtmCampaignInfoList(if (firstObject == null) listOf() else listOf(firstObject.ownerId))
                modelAndView.addObject("gtmCampaignInfo", if (gtmCampaignInfoList.isNotEmpty()) gtmCampaignInfoList[0] else null)
            }
            OrderObjectType.SHARE -> {
                modelAndView.addAllObjects(mapOf("similarShares" to campaignStatService.getSimilarShares(firstObject?.objectId
                        ?: -1, 2),
                        "campaign" to campaignService.getCampaign(firstObject?.ownerId ?: -1),
                        "isSubscribeded" to profileSubscriptionService.doISubscribeYou(order.buyerId,
                                campaignService.getCampaign(firstObject?.ownerId ?: -1)?.creatorProfileId ?: -1)))
                gtmCampaignInfoList = campaignService.getGtmCampaignInfoList(if (firstObject == null) listOf() else listOf(firstObject.ownerId))
                modelAndView.addObject("gtmCampaignInfo", if (gtmCampaignInfoList.isNotEmpty()) gtmCampaignInfoList[0] else null)
            }
            OrderObjectType.PRODUCT -> {
                modelAndView.addObject("relatedProducts", productUsersService.getSimilarProducts(firstObject?.objectId
                        ?: -1, 4))
                val cartItemList = orderObjects?.mapTo(ArrayList<Map<String, Any>>()) { productUsersService.cartItemToDTO(it.objectId, it.count) }
                modelAndView.addObject("cartItemList", cartItemList)
            }
        }
    }

    private fun fillOrderInfo(modelAndView: ModelAndView, clientId: Long, orderId: Long) =
            fillOrderInfo(modelAndView, orderService.getOrderSafe(clientId, orderId))

    private fun getSharingDescription(orderObject: OrderObjectInfo?): String? {
        if (orderObject == null) {
            return null
        }
        var result = StringUtils.EMPTY
        try {
            when (orderObject.objectType) {
                OrderObjectType.SHARE -> result = campaignService.getCampaignSafe(orderObject.ownerId).shortDescription ?: ""
            }
        } catch (ex: Exception) {
            logger.error("Error getting sharing description:", ex)
        }

        return result
    }


    /**
     * Anonymous user can't see payment.
     *
     * @param clientId client profile identifier;
     * @param transaction payment transaction;
     * @return `true` if payment transaction not `null`
     * and client has access to it.
     */
    private fun isAccessiblePayment(clientId: Long, transaction: TopayTransaction?): Boolean =
            transaction != null && permissionService.isAdmin(clientId, transaction.profileId)

}

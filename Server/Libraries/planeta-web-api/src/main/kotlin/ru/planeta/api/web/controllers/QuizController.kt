package ru.planeta.api.web.controllers

import org.springframework.context.MessageSource
import org.springframework.validation.BindingResult
import org.springframework.web.bind.annotation.*
import ru.planeta.api.exceptions.NotFoundException
import ru.planeta.api.exceptions.PermissionException
import ru.planeta.api.model.ActionStatus
import ru.planeta.api.service.quiz.QuizService
import ru.planeta.api.text.HtmlValidator
import ru.planeta.api.web.authentication.isAnonymous
import ru.planeta.api.web.authentication.myProfileId
import ru.planeta.model.trashcan.Quiz
import ru.planeta.model.trashcan.QuizAnswer
import ru.planeta.model.trashcan.QuizQuestion
import java.util.*

@RestController
class QuizController(private val quizService: QuizService,
                     private val messageSource: MessageSource) {

    @GetMapping(Urls.Quiz.QUIZ_QUESTIONS)
    fun getQuizQuestions(@RequestParam quizAlias: String): ActionStatus<List<QuizQuestion>> {
        val quiz = quizService.getQuiz(quizAlias, true) ?: throw NotFoundException()
        val quizQuestions = quizService.getQuizQuestionsByQuizId(quiz.quizId, true)
        return ActionStatus.createSuccessStatus(quizQuestions)
    }

    @PostMapping(Urls.Quiz.ADD_QUIZ_ANSWERS)
    fun addUserFeedback(@RequestBody quizAnswers: ArrayList<QuizAnswer>?, bindingResult: BindingResult): ActionStatus<*> {
        if (isAnonymous()) {
            throw PermissionException()
        }
        val myProfileId = myProfileId()
        if (quizAnswers != null) {
            val foundQuizAnswerList = quizService.getQuizAnswersByParams(quizAnswers[0].quizId, null, myProfileId, 0, 1)
            if (foundQuizAnswerList.size == 0) {
                for (quizAnswer in quizAnswers) {
                    if (quizAnswers != null && !HtmlValidator.validateHtml(quizAnswer.answerCustomText)) {
                        bindingResult.rejectValue("answer" + quizAnswer.quizQuestionId, "wrong.chars")
                        return ActionStatus.createErrorStatus<Any>("quiz.check.error", messageSource)
                    }
                    if (quizAnswer.quizId <= 0) {
                        return ActionStatus.createErrorStatus<Any>("quiz.unknown.error", messageSource)
                    }
                }
                quizService.addQuizAnswers(quizAnswers, myProfileId)
            }
            return ActionStatus.createSuccessStatus(true)
        } else {
            return ActionStatus.createErrorStatus<Any>("quiz.unknown.error", messageSource)
        }
    }

    @GetMapping(Urls.Quiz.QUIZ)
    fun getQuiz(@RequestParam(value = "quizAlias") quizAlias: String): ActionStatus<*> {
        val quiz = quizService.getQuiz(quizAlias, null) ?: throw NotFoundException(Quiz::class.java, "alias", quizAlias)
        return ActionStatus.createSuccessStatus(quiz)
    }

    @PostMapping(Urls.Quiz.CHECK_USER_HAS_ANSWERED_QUIZ)
    fun checkUserHasAnsweredQuiz(@RequestParam quizId: Long, @RequestParam(defaultValue = "0") offset: Long,
                                 @RequestParam(defaultValue = "100") limit: Long): ActionStatus<*> {
        val quizAnswers = quizService.getQuizAnswersByParams(quizId, null, myProfileId(), offset, limit)
        return if (quizAnswers != null && quizAnswers.size > 0) {
            ActionStatus.createSuccessStatus(true)
        } else {
            ActionStatus.createSuccessStatus(false)
        }
    }
}

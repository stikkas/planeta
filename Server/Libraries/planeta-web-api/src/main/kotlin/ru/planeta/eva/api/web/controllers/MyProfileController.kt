package ru.planeta.eva.api.web.controllers

import org.springframework.validation.BindingResult
import org.springframework.web.bind.annotation.*
import ru.planeta.api.exceptions.NotFoundException
import ru.planeta.api.exceptions.PermissionException
import ru.planeta.api.news.ProfileNewsService
import ru.planeta.api.service.billing.order.OrderService
import ru.planeta.api.service.campaign.CampaignService
import ru.planeta.api.web.authentication.myProfileId
import ru.planeta.eva.api.services.UserProfileService
import ru.planeta.eva.api.web.ActionStatus
import ru.planeta.eva.api.web.ProfileUrls
import ru.planeta.model.common.campaign.enums.CampaignStatus
import ru.planeta.model.common.campaign.enums.CampaignTargetStatus
import ru.planeta.model.profile.Post
import javax.validation.Valid

@RestController("EvaWebApiMyProfileController")
class MyProfileController(private val newsService: ProfileNewsService,
                          private val campaignService: CampaignService,
                          private val userProfileService: UserProfileService,
                          private val orderService: OrderService) {

    companion object {
        const val DEFAULT_LIMIT = 10
        const val DEFAULT_OFFSET = 0
    }

    /**
     * Список новостей (обновлений) для профиля
     */
    @GetMapping(ProfileUrls.UPDATES_FOR_ME)
    fun getUpdatesForMe(@RequestParam(defaultValue = DEFAULT_OFFSET.toString()) offset: Int,
                        @RequestParam(defaultValue = DEFAULT_LIMIT.toString()) limit: Int): ActionStatus {

        var limitValue = limit
        if (limitValue > DEFAULT_LIMIT) {
            limitValue = DEFAULT_LIMIT
        }

        val news = newsService.getProfileNews(myProfileId(), myProfileId(), false, offset, limitValue)

        return ActionStatus(result = news)
    }

    @GetMapping(ProfileUrls.MY_CAMPAIGNS_STATUSES)
    fun getMyCampaignsStatuses(): ActionStatus {
        return ActionStatus(result = campaignService.getUserCampaignsStatuses(myProfileId()))
    }

    @GetMapping(ProfileUrls.MY_PURCHASED_CAMPAIGNS_STATUSES)
    fun getMyPurchasedCampaignsStatuses(): ActionStatus {
        return ActionStatus(result = campaignService.getUserPurchasedCampaignsStatuses(myProfileId()))
    }

    @GetMapping(ProfileUrls.MY_PURCHASES_AND_REWARDS)
    fun getMyPurchasesAndRewards(): ActionStatus {
        return ActionStatus(result = campaignService.getUserPurchasedAndRewardsCount(myProfileId()))
    }

    @GetMapping(ProfileUrls.MY_CAMPAIGNS)
    fun getMyCampaigns(@RequestParam(defaultValue = "0") offset: Int,
                       @RequestParam(defaultValue = "10") limit: Int): ActionStatus =
            ActionStatus(result = userProfileService.getMyProjects(myProfileId()))

    @GetMapping(ProfileUrls.MY_PURCHASED_CAMPAIGNS)
    fun getMyPurchasedCampaigns(@RequestParam(required = false, defaultValue = "0") offset: Long,
                       @RequestParam(required = false, defaultValue = "10") limit: Int,
                       @RequestParam(required = false, defaultValue = "ACTIVE") status: CampaignStatus?,
                       @RequestParam(required = false) targetStatus: CampaignTargetStatus?
    ): ActionStatus {
        if (status != CampaignStatus.ACTIVE && status != CampaignStatus.FINISHED) {
            return ActionStatus(false)
        }
        return ActionStatus(result = userProfileService.getMyPurchasedProjects(myProfileId(), status, targetStatus, offset, limit))
    }

    @GetMapping(ProfileUrls.MY_PURCHASED_REWARDS)
    fun getMyPurchasedRewards(@RequestParam(required = false, defaultValue = "0") offset: Int,
                                @RequestParam(required = false, defaultValue = "100") limit: Int
    ): ActionStatus {
        return ActionStatus(result = orderService.getUserOrdersRewardsInfo(myProfileId(), offset, limit))
    }

    @PostMapping(ProfileUrls.MY_CAMPAIGNS_STATS)
    fun getMyCampaignsStats(@RequestBody campaignIds: List<Long>): ActionStatus =
            ActionStatus(result = userProfileService.getMyProjectsStats(campaignIds))

    @PostMapping(ProfileUrls.REMOVE_DRAFT)
    @Throws(PermissionException::class, NotFoundException::class)
    fun removeDraft(@RequestBody campaignId: Long): ActionStatus {
        campaignService.removeCampaign(myProfileId(), campaignId)
        return ActionStatus(result = true)
    }

    @PostMapping(ProfileUrls.PROFILE_NEWS_ADD_POST)
    fun addPost(@Valid @RequestBody post: Post, result: BindingResult): ActionStatus {
        if (result.hasErrors()) {
            return ActionStatus(result)
        }
        newsService.addProfileNewsPost(myProfileId(), post)
        return ActionStatus()
    }
}



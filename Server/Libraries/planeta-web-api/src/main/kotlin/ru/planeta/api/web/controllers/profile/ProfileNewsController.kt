package ru.planeta.api.web.controllers.profile

import org.springframework.context.MessageSource
import org.springframework.validation.BindingResult
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import ru.planeta.api.model.ActionStatus
import ru.planeta.api.news.LoggerService
import ru.planeta.api.news.ProfileNewsDTO
import ru.planeta.api.news.ProfileNewsService
import ru.planeta.api.service.comments.CommentsService
import ru.planeta.api.service.notifications.NotificationService
import ru.planeta.api.web.authentication.myProfileId
import ru.planeta.api.web.controllers.Urls
import ru.planeta.model.enums.ObjectType
import ru.planeta.model.news.ProfileNews
import ru.planeta.model.profile.Post
import java.util.*
import javax.validation.Valid

/**
 * Created with IntelliJ IDEA.
 * User: a.volkov
 * Date: 20.11.2014
 * Time: 14:13
 */
@RestController
class ProfileNewsController(private val newsService: ProfileNewsService,
                            private val notificationService: NotificationService,
                            private val commentsService: CommentsService,
                            private val messageSource: MessageSource,
                            private val profileNewsService: LoggerService) {

    @GetMapping(Urls.PROFILE_NEWS)
    fun getNews(@RequestParam profileId: Long,
                @RequestParam(defaultValue = "false") onlyMyNews: Boolean,
                @RequestParam(defaultValue = "0") campaignId: Long,
                @RequestParam(defaultValue = "0") offset: Int,
                @RequestParam(defaultValue = "20") limit: Int): ActionStatus<List<ProfileNewsDTO>> {
        val news = if (campaignId > 0)
            newsService.getCampaignNews(campaignId, limit, offset)
        else
            newsService.getProfileNews(myProfileId(), profileId, onlyMyNews, offset, limit)

        return ActionStatus.createSuccessStatus(news)
    }

    @GetMapping(Urls.PROFILE_TECH_STATS)
    fun getTechNews(@RequestParam profileId: Long,
                    @RequestParam(defaultValue = "false") onlyMyNews: Boolean,
                    @RequestParam(defaultValue = "0") offset: Int,
                    @RequestParam(defaultValue = "20") limit: Int): ActionStatus<Map<String, Any>> {
        val map = HashMap<String, Any>()
        map.put("commentsCount", commentsService.getCommentsCount(profileId, ObjectType.PROFILE))
        map.put("newsCount", newsService.selectCountForProfile(profileId, null, true))
        return ActionStatus.createSuccessStatus(map)
    }

    @PostMapping(value = Urls.PROFILE_NEWS_ADD_POST)
    fun addNewsPost(@Valid post: Post, result: BindingResult): ActionStatus<ProfileNewsDTO> {
        if (result.hasErrors()) {
            return ActionStatus.createErrorStatus(result, messageSource)
        }

        val dto = newsService.addProfileNewsPost(myProfileId(), post)
        return ActionStatus.createSuccessStatus(dto)
    }

    @PostMapping(Urls.PROFILE_NEWS_CHANGE_POST)
    fun changeNewsPost(@Valid post: Post, result: BindingResult): ActionStatus<ProfileNewsDTO> {
        if (result.hasErrors()) {
            return ActionStatus.createErrorStatus(result, messageSource)
        }

        val dto = newsService.changeProfileNewsPost(myProfileId(), post)
        profileNewsService.addProfileNews(ProfileNews.Type.CHANGE_CAMPAIGN_POST, myProfileId(), post.id, post.campaignId)
        return ActionStatus.createSuccessStatus(dto)
    }

    @PostMapping(Urls.PROFILE_NEWS_DELETE_POST)
    fun deleteProfileNews(@RequestParam id: Long): ActionStatus<*> {
        val myProfileId = myProfileId()
        newsService.markDeleted(myProfileId, id)
        profileNewsService.addProfileNews(ProfileNews.Type.DELETE_CAMPAIGN_POST, myProfileId, id)
        return ActionStatus.createSuccessStatus<Any>()
    }

    @PostMapping(Urls.SEND_CAMPAIGN_NEWS_NOTIFICATION)
    fun sendNotificationBlogPost(@RequestParam profileId: Long,
                                 @RequestParam postId: Long,
                                 @RequestParam campaignId: Long): ActionStatus<*> {
        val myProfileId = myProfileId()
        profileNewsService.addProfileNews(ProfileNews.Type.SEND_CAMPAIGN_NEWS_NOTIFICATION, myProfileId, postId, campaignId)
        return notificationService.sendCampaignNewsAddedNotificationSecure(myProfileId, campaignId, profileId, postId)
    }
}



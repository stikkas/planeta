package ru.planeta.api.web.controllers.services

import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service
import ru.planeta.api.web.controllers.services.BaseControllerService
import ru.planeta.model.enums.ProjectType
import ru.planeta.model.stat.RefererStatType
import ru.planeta.model.stat.RequestStat
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

/**
 * Created by asavan on 02.02.2017.
 */
@Service
class BaseProjectSpecificControllerService(@Value("\${project.type:MAIN}")
                                           private val projectType: ProjectType,
                                           val baseControllerService: BaseControllerService) {

    fun getRequestStat(request: HttpServletRequest, response: HttpServletResponse, type: RefererStatType): RequestStat =
            baseControllerService.getRequestStat(request, response, type, projectType)
}

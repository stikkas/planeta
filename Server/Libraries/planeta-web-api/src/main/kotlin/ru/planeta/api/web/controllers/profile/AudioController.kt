package ru.planeta.api.web.controllers.profile

import org.apache.log4j.Logger
import org.springframework.validation.BindingResult
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import ru.planeta.api.model.ActionStatus
import ru.planeta.api.model.json.TrackObjectsList
import ru.planeta.api.service.content.AudioService
import ru.planeta.api.web.authentication.myProfileId
import ru.planeta.api.web.controllers.Urls
import ru.planeta.api.web.controllers.services.BaseControllerService
import ru.planeta.model.profile.media.AudioTrack
import javax.validation.Valid

/**
 * Base Controller for profile's audios
 * User: m.shulepov
 * Date: 16.05.12
 * Time: 11:14
 */

@RestController
class AudioController(private val audioService: AudioService,
                      private val baseControllerService: BaseControllerService) {

    private val logger = Logger.getLogger(AudioController::class.java)

    @GetMapping(Urls.AUDIO_TRACKS)
    fun getTracks(@RequestParam(defaultValue = "0") profileId: Long,
                  @RequestParam(defaultValue = "0") playlistId: Int,
                  @RequestParam(defaultValue = "0") albumId: Long,
                  @RequestParam(required = false) searchString: String?,
                  @RequestParam(defaultValue = "0") offset: Int,
                  @RequestParam(defaultValue = "0") limit: Int): List<AudioTrack> {
        val myProfileId = myProfileId()
        val profileId = if (profileId == 0L) myProfileId else profileId

        val result = when {
            profileId <= 0 -> {
                logger.error("Anonymous track own audio list")
                null
            }
            playlistId > 0 -> audioService.getPlaylistTracks(myProfileId, profileId, playlistId, searchString ?: "")
            albumId > 0 -> audioService.getAudioAlbumTracks(myProfileId, profileId, albumId, offset, limit)
            else -> audioService.getAudioTracks(myProfileId, profileId, offset, limit, searchString)
        }
        return result ?: emptyList()

    }

    @GetMapping(Urls.AUDIO_TRACK)
    fun getTrack(@RequestParam(defaultValue = "0") profileId: Long,
                 @RequestParam(defaultValue = "0") trackId: Long): ActionStatus<AudioTrack> =
            ActionStatus.createSuccessStatus(audioService.getAudioTrack(myProfileId(), profileId, trackId))

    @PostMapping(Urls.AUDIO_TRACK)
    fun saveTrack(@Valid audioTrack: AudioTrack, result: BindingResult): ActionStatus<AudioTrack> {
        if (result.hasErrors()) {
            return baseControllerService.createErrorStatus(result)
        }
        try {
            return ActionStatus.createSuccessStatus(audioService.saveAudioTrack(myProfileId(), audioTrack))
        } catch (ex: Exception) {
            logger.error("Error saving audio track", ex)
            throw ex
        }

    }

    @GetMapping(Urls.AUDIO_SEVERAL_TRACKS)
    fun getTrack(trackObjectsList: TrackObjectsList): ActionStatus<List<AudioTrack>> =
            ActionStatus.createSuccessStatus(trackObjectsList.trackObjects.map { audioService.getAudioTrack(myProfileId(), it.profileId, it.trackId) })

}


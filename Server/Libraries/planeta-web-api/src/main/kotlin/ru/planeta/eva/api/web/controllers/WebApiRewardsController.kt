package ru.planeta.eva.api.web.controllers

import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RestController
import ru.planeta.eva.api.services.ShareService
import ru.planeta.eva.api.web.ActionStatus
import ru.planeta.eva.api.web.Urls

@RestController
class WebApiRewardsController(private val shareService: ShareService) {

    @GetMapping(Urls.GET_REWARD_INFO)
    fun validateShare(@PathVariable id: Long): ActionStatus {
        return ActionStatus(result = shareService.shareInfo(id))
    }
}



package ru.planeta.eva.api.web.cas.filter

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler
import org.springframework.web.filter.GenericFilterBean
import ru.planeta.api.web.authentication.isAnonymous
import ru.planeta.api.web.utils.SessionUtils
import ru.planeta.commons.web.CookieUtils
import ru.planeta.eva.api.web.cas.handlers.CustomAuthenticationSuccessHandler
import ru.planeta.eva.api.web.cas.handlers.CustomLogoutSuccessHandler
import ru.planeta.eva.api.web.cas.services.AuthTokenService
import javax.servlet.FilterChain
import javax.servlet.ServletRequest
import javax.servlet.ServletResponse
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

class SsoFilter(private val authToken: String) : GenericFilterBean() {

    @Autowired
    private lateinit var authTokenService: AuthTokenService

    @Autowired
    private lateinit var logoutSuccessHandler: CustomLogoutSuccessHandler

    @Autowired
    private lateinit var authenticationSuccessHandler: CustomAuthenticationSuccessHandler

    override fun doFilter(servletRequest: ServletRequest, servletResponse: ServletResponse, filterChain: FilterChain) {
        val request = servletRequest as HttpServletRequest
        val response = servletResponse as HttpServletResponse
        val token = CookieUtils.getCookieValue(request.cookies, authToken, null)
        if (isAnonymous()) {
            if (token != null) {
                login(token, request, response)
            }
        } else {
            if (token == null) { // Авторизованный пользователь должен всегда иметь token, разлогиниваем
                logout(request, response)
            } else {
                val savedToken = SessionUtils.session.getAttribute(authToken)
                if (savedToken != token) { // Пытаемся залогинить пользователя под токеном из запроса
                    // если не получается, то разлогиниваем
                    login(token, request, response, true)
                }
            }
        }
        filterChain.doFilter(servletRequest, servletResponse)
    }

    private fun login(token: String, request: HttpServletRequest, response: HttpServletResponse, logout: Boolean = false) {
        val authentication = authTokenService.getAuthentication(token, request.getHeader("User-Agent"))
        if (authentication != null) {
            SecurityContextHolder.getContext().authentication = authentication
            authenticationSuccessHandler.setCookie(authentication, request, response)
        } else if (logout) {
            logout(request, response)
        }
    }

    fun logout(request: HttpServletRequest, response: HttpServletResponse) {
        SecurityContextLogoutHandler().logout(request, response, null)
        logoutSuccessHandler.removeAuthCookie(request, response)
    }
}


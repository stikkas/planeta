package ru.planeta.api.web.controllers.services

import org.apache.commons.lang3.StringUtils
import org.apache.commons.lang3.tuple.Pair
import org.apache.log4j.Logger
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service
import org.springframework.validation.BindingResult
import org.springframework.web.servlet.ModelAndView
import ru.planeta.api.exceptions.MessageCode
import ru.planeta.api.exceptions.NotFoundException
import ru.planeta.api.exceptions.PermissionException
import ru.planeta.api.exceptions.RegistrationException
import ru.planeta.api.helper.ProjectService
import ru.planeta.api.model.json.DeliveryInfo
import ru.planeta.api.service.campaign.DeliveryService
import ru.planeta.api.service.comments.CommentsService
import ru.planeta.api.service.profile.AuthorizationService
import ru.planeta.api.service.registration.RegistrationService
import ru.planeta.api.service.security.PermissionService
import ru.planeta.api.utils.ValidateUtils
import ru.planeta.api.web.authentication.*
import ru.planeta.api.web.utils.ProperRedirectView
import ru.planeta.api.web.utils.SessionUtils
import ru.planeta.eva.api.web.cas.handlers.CustomAuthenticationSuccessHandler
import ru.planeta.model.common.TopayTransaction
import ru.planeta.model.common.delivery.SubjectType
import ru.planeta.model.enums.ProjectType
import ru.planeta.model.profile.Comment
import ru.planeta.model.stat.RefererStatType
import java.io.UnsupportedEncodingException
import java.math.BigDecimal
import java.net.URLDecoder
import java.util.*
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

@Service
class BasePaymentControllerService(@Value("\${project.type:MAIN}")
                                   private val projectType: ProjectType,
                                   private val projectService: ProjectService,
                                   private val deliveryService: DeliveryService,
                                   private val permissionService: PermissionService,
                                   private val authorizationService: AuthorizationService,
                                   private val baseControllerService: BaseControllerService,
                                   private val controllerAutoLoginWrapService: ControllerAutoLoginWrapService,
                                   private val authenticationSuccessHandler: CustomAuthenticationSuccessHandler,
                                   private val commentsService: CommentsService,
                                   private val registrationService: RegistrationService) {

    private val logger = Logger.getLogger(BasePaymentControllerService::class.java)

    /**
     * Saves deferred comment in user session.
     *
     * @param orderId order identifier which associated with comment;
     * @param comment deferred comment;
     * @throws NotFoundException
     */
    fun saveDeferredComment(orderId: Long, comment: Comment?) {
        if (comment == null) {
            return
        }
        val comments = deferredComments()
        comments.put(orderId, comment)
        SessionUtils.deferredComments = comments
    }

    /**
     * Persists deferred comment (if exists), and removes it from user session.
     *
     * @param orderId order identifier which associated with comment;
     * @param isMobilePayment mobile payment flag;
     */
    fun addDeferredComment(orderId: Long, isMobilePayment: Boolean) {
        if (orderId == 0L) {
            return
        }
        try {
            val commentByOrderId = deferredComments()
            if (!commentByOrderId.containsKey(orderId)) {
                return
            }

            // TODO: ignore if payment was mobile
            if (!isMobilePayment) {
                commentByOrderId[orderId]?.let {
                    commentsService.addComment(it)
                }
                commentByOrderId.remove(orderId)
            }
        } catch (ex: Exception) {
            logger.warn("Error sending deferred comments", ex)
        }

    }

    /**
     * Removes deferred comment (if exists) from user session.
     *
     * @param orderId order identifier which associated with comment;
     */
    fun removeDeferredComment(orderId: Long) {
        if (orderId == 0L) {
            return
        }

        val commentByOrderId = deferredComments()
        if (commentByOrderId.containsKey(orderId)) {
            commentByOrderId.remove(orderId)
        }
    }

    fun createPaymentRedirectModelAndView(transaction: TopayTransaction): ModelAndView =
            ModelAndView(ProperRedirectView(projectService.getPaymentGateRedirectUrl(transaction)))

    /**
     * Returns *DeliveryInfo* may be *null*<br></br>
     *
     * @throws NotFoundException
     */
    fun getDeliveryInfo(shareId: Long, deliveryServiceId: Long): DeliveryInfo? {
        if (deliveryServiceId == 0L) {
            return null
        }
        val linkedDelivery = deliveryService.getLinkedDelivery(shareId, deliveryServiceId, SubjectType.SHARE)

        return DeliveryInfo(linkedDelivery.serviceId, linkedDelivery.price ?: BigDecimal.ZERO, linkedDelivery.serviceType)
    }

    /**
     * Gets user profile identifier.<br></br>
     * If user is anonymous, tries authorize him by presented email.<br></br>
     * If user isn't registered, tries register him by presented email and
     * ip.<br></br>
     *
     * @param email user email;
     * @return user profile identifier, if user
     * identification/authorization/registration was successfully, anonymous
     * identifier (-1) otherwise.
     * @throws java.io.UnsupportedEncodingException
     * @throws NotFoundException
     * @throws PermissionException
     */
    @Throws(RegistrationException::class, PermissionException::class)
    fun getRegisteredOrCreateProfileId(email: String?, request: HttpServletRequest, response: HttpServletResponse): Long {
        val myProfileId = myProfileId()
        if (StringUtils.isBlank(email)) {
            return if (permissionService.isAnonymous(myProfileId)) {
                throw RegistrationException(MessageCode.WRONG_EMAIL)
            } else {
                myProfileId
            }
        }
        val email = email as String
        val userPrivateInfoByEmail = authorizationService.getUserPrivateInfoByUsername(email)

        if (permissionService.isAnonymous(myProfileId)) {
            if (userPrivateInfoByEmail == null) {
                return if (isValidEmail(email)) {
                    createNewUserAndGetUserIdAndLogin(email, request, response)
                } else {
                    throw RegistrationException(MessageCode.WRONG_EMAIL)
                }
            }
        } else if (userPrivateInfoByEmail == null) {
            val userInfo = registrationService.addEmailToExternalUser(email, userAuthorizationInfo())
            if (userInfo != null) {
                return userInfo.profile.profileId
            }
        } else if (userPrivateInfoByEmail.userId == myProfileId) {
            return myProfileId
        }
        throw PermissionException(MessageCode.REGISTRATION_CURRENT_EMAIL_ALREADY_EXISTS)
    }

    fun createNewUserAndGetUserIdAndLogin(email: String, request: HttpServletRequest, response: HttpServletResponse): Long {
        try {
            val userAuthorizationInfo = registrationService.autoRegisterByEmail(email, null,
                    baseControllerService.getRequestStat(request, response, RefererStatType.REGISTRATION, projectType))
            if (userAuthorizationInfo.profile != null) {
                controllerAutoLoginWrapService.autoLoginUser(email)
                authenticationSuccessHandler.onAuthenticationSuccess(request, response, getAuthentication())
                return userAuthorizationInfo.profile.profileId
            } else {
                logger.warn("Profile null after autoregistration $email")
                throw RegistrationException(MessageCode.UNEXPECTED_EXCEPTION)
            }
        } catch (ex: IllegalArgumentException) {
            logger.error(ex)
            throw RegistrationException(MessageCode.WRONG_EMAIL)
        }

    }

    fun checkEmailAndGetProfileId(email: String, result: BindingResult, request: HttpServletRequest, response: HttpServletResponse): Pair<BindingResult, Long> {
        //current user is authorized and has email:
        if (hasEmail()) {
            return Pair.of(result, myProfileId())
        }

        if (StringUtils.isEmpty(email)) {
            result.rejectValue("email", "email.required")
            return Pair.of(result, -1L)
        }
        if (!ValidateUtils.isValidEmail(email)) {
            result.rejectValue("email", "wrong.email")
            return Pair.of(result, -1L)
        }
        val userPrivateInfo = authorizationService.getUserPrivateInfoByUsername(email.toLowerCase())
        //email - ok
        //email is new - password will be generated, confirmation email will be send
        if (userPrivateInfo == null) {
            //new user.
            if (isAnonymous()) {
                val newUserId = createNewUserAndGetUserIdAndLogin(email, request, response)
                return Pair.of(result, newUserId)
            }
            registrationService.addEmailToExternalUser(email, userAuthorizationInfo())
            return Pair.of(result, myProfileId())
        }

        // https://planeta.atlassian.net/browse/PLANETA-15291
        result.rejectValue("email", "email.widgets.exist")
        return Pair.of(result, -1L)
    }

    /**
     * Indicates is email valid.<br></br>
     * Performs double check validation.<br></br>
     * Second validation phase performs with email charset decoding, if first
     * phase was failed.<br></br>
     *
     * @param email checked email;
     * @return true if email is valid, false otherwise.
     * @throws UnsupportedEncodingException
     */
    private fun isValidEmail(email: String): Boolean {
        var email = email
        var result = ValidateUtils.isValidEmail(email)
        if (!result) {
            email = URLDecoder.decode(email, "UTF-8")
            result = ValidateUtils.isValidEmail(email)
        }

        return result
    }

    /**
     * Gets deferred comments map from the user session.
     */
    private fun deferredComments(): MutableMap<Long, Comment> {
        var result = SessionUtils.deferredComments?.toMutableMap()

        if (result == null) {
            result = HashMap()
            SessionUtils.deferredComments = result
        }
        return result
    }
}

package ru.planeta.api.web.utils

import com.google.zxing.BarcodeFormat
import com.google.zxing.client.j2se.MatrixToImageWriter
import com.google.zxing.qrcode.QRCodeWriter
import org.apache.log4j.Logger
import org.krysalis.barcode4j.BarcodeGenerator
import org.krysalis.barcode4j.impl.code128.EAN128Bean
import org.krysalis.barcode4j.impl.upcean.EAN13Bean
import org.krysalis.barcode4j.output.bitmap.BitmapCanvasProvider
import java.awt.image.BufferedImage
import java.io.OutputStream

/**
 * Class for generating barcodes
 */
object BarcodeUtils {

    private val log = Logger.getLogger(BarcodeUtils::class.java)
    /**
     * Company internal information, (pattern: n2+an..30)
     * check http://www.databar-barcode.info/application-identifiers for more information on AI numbers
     * NOTE: If you change this code check the corresponding format for this code
     */
    private val AI_CODE = "99"
    private val SEPARATOR = "-"


    /**
     * Utility method that generates barcode image and writes it into output stream<br></br>
     *
     * The format of the barcode is: 01-{orderId}-{orderObjectId}
     * @param out [java.io.OutputStream] to which image will be written
     * @param orderId order identifier
     * @param orderObjectId order object identifier (pass 0 in order to omit this part of barcode from being generated)
     */
    fun generatePicture(out: OutputStream, orderId: Long, orderObjectId: Long) {
        val barcode = generateBarcodeString(orderId, orderObjectId)
        generateBarcode(out, barcode, false)
    }

    /**
     * Utility method that generates QR code image and writes it into output stream<br></br>
     * @param out [java.io.OutputStream] to which image will be written
     * @param url url
     */
    fun generateQRPicture(out: OutputStream, url: String, height: Int, width: Int) {
        try {
            val writer = QRCodeWriter()
            out.use {
                val bitMatrix = writer.encode(url, BarcodeFormat.QR_CODE, width, height)
                MatrixToImageWriter.writeToStream(bitMatrix, "png", it)
            }
        } catch (e: Exception) {
            log.error("Error occurred", e)
        }

    }

    private fun generateBarcodeString(partOne: Long, partTwo: Long): String {
        val barcode = StringBuilder(AI_CODE).append(SEPARATOR).append(partOne)
        if (partTwo > 0) {
            barcode.append(SEPARATOR).append(partTwo)
        }
        return barcode.toString()
    }

    // encapsulate barcode lib here
    fun generateBarcode(out: OutputStream, code: String, useEAN13: Boolean) {
        try {
            if (code.length < 3) {
                throw IllegalArgumentException("Code must contain at least 3 characters")
            }
            // Create the barcode generator
            val barcodeGenerator: BarcodeGenerator
            if (useEAN13) {
                barcodeGenerator = EAN13Bean()
            } else {
                barcodeGenerator = EAN128Bean()
                barcodeGenerator.isOmitBrackets = true
            }

            val dpi = 150
            out.use {
                // Set up the canvas provider for monochrome PNG output
                val canvas = BitmapCanvasProvider(
                        it, "image/png", dpi, BufferedImage.TYPE_BYTE_BINARY, false, 0)

                // Generate the barcode
                barcodeGenerator.generateBarcode(canvas, code)

                // Signal end of generation
                canvas.finish()
            }
        } catch (e: Exception) {
            log.error("Error occurred", e)
        }

    }
}


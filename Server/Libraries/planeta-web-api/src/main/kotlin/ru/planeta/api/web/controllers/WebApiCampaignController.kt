package ru.planeta.api.web.controllers;

import org.apache.commons.lang3.StringUtils
import org.apache.commons.lang3.time.DateUtils
import org.springframework.validation.BindingResult
import org.springframework.validation.DataBinder
import org.springframework.web.bind.annotation.*
import ru.planeta.api.Utils
import ru.planeta.api.exceptions.NotFoundException
import ru.planeta.api.model.ActionStatus
import ru.planeta.api.model.json.VideoInfo
import ru.planeta.api.news.ProfileNewsDTO
import ru.planeta.api.news.ProfileNewsService
import ru.planeta.api.search.SearchService
import ru.planeta.api.service.campaign.*
import ru.planeta.api.service.comments.CommentsService
import ru.planeta.api.service.common.PlanetaManagersService
import ru.planeta.api.service.configurations.ConfigurationService
import ru.planeta.api.service.content.VideoService
import ru.planeta.api.service.profile.ProfileLastVisitService
import ru.planeta.api.service.profile.ProfileSitesService
import ru.planeta.api.service.security.PermissionService
import ru.planeta.api.service.shop.ProductUsersService
import ru.planeta.api.web.authentication.isAnonymous
import ru.planeta.api.web.authentication.myProfileId
import ru.planeta.api.web.controllers.services.BaseControllerService
import ru.planeta.api.web.dto.CampaignShortInfo
import ru.planeta.api.web.dto.TopCampaignDTO
import ru.planeta.api.web.model.SharePurchase
import ru.planeta.api.web.validation.ProfileSitesValidator
import ru.planeta.api.web.validation.UserAuthValidator
import ru.planeta.commons.web.WebUtils
import ru.planeta.dao.ListWithCount
import ru.planeta.dao.commondb.SponsorDAO
import ru.planeta.dao.profiledb.PostDAO
import ru.planeta.model.common.InvestingOrderInfo
import ru.planeta.model.common.ModerationMessage
import ru.planeta.model.common.PlanetaManager
import ru.planeta.model.common.campaign.Campaign
import ru.planeta.model.common.campaign.CampaignContact
import ru.planeta.model.common.campaign.CampaignWithNewEventsCount
import ru.planeta.model.common.campaign.enums.CampaignStatus
import ru.planeta.model.common.delivery.LinkedDelivery
import ru.planeta.model.common.delivery.SubjectType
import ru.planeta.model.enums.ModerationMessageStatus
import ru.planeta.model.enums.ObjectType
import ru.planeta.model.enums.ProfileLastVisitType
import ru.planeta.model.enums.ProfileType
import ru.planeta.model.profiledb.ProfileSites
import ru.planeta.model.profile.CampaignBacker
import java.math.BigDecimal
import java.util.*
import javax.validation.Valid

@Deprecated("Используем ru.planeta.eva.api.web.controllers.WebApiCampaignController" +
        "по необходимости переносим все методы отсюда туда")
@RestController
class WebApiCampaignController(private val campaignService: CampaignService,
                               private val newsService: ProfileNewsService,
                               private val configurationService: ConfigurationService,
                               private val permissionService: PermissionService,
                               private val userAuthValidator: UserAuthValidator,
                               private val postDAO: PostDAO,
                               private val campaignPermissionService: CampaignPermissionService,
                               private val commentsService: CommentsService,
                               private val campaignFaqService: CampaignFaqService,
                               private val profileLastVisitService: ProfileLastVisitService,
                               private val videoService: VideoService,
                               private val sponsorDAO: SponsorDAO,
                               private val campaignStatsService: CampaignStatsService,
                               private val moderationMessageService: ModerationMessageService,
                               private val productUsersService: ProductUsersService,
                               private val searchService: SearchService,
                               private val campaignSubscribeService: CampaignSubscribeService,
                               private val profileSitesService: ProfileSitesService,
                               private val deliveryService: DeliveryService,
                               private val planetaManagersService: PlanetaManagersService,
                               private val profileSitesValidator: ProfileSitesValidator,
                               private val baseControllerService: BaseControllerService) {

    @GetMapping(Urls.CAMPAIGN_TOTAL_PURCHASE)
    fun totalPurchaseSum(): BigDecimal = campaignService.totalCollectedAmount

    @GetMapping(Urls.CAMPAIGN_BACKERS)
    fun getCampaignBackers(@RequestParam campaignId: Long, @RequestParam(defaultValue = "0") offset: Int,
                           @RequestParam(defaultValue = "10") limit: Int): List<CampaignBacker> {
        val limit = if (limit == 0 || limit > 100) 100 else limit
        return campaignService.getCampaignBackersNew(campaignId, offset, limit)
    }

    @GetMapping(Urls.CAMPAIGN_GET_UPDATES_JSON)
    fun campaignUpdatesJson(@RequestParam campaignId: Long, @RequestParam(defaultValue = "0") offset: Int,
                            @RequestParam(defaultValue = "10") limit: Int): Collection<*> =
            newsService.getCampaignNews(campaignId, limit, offset)

    @GetMapping(Urls.PROFILE_NEWS_POST)
    fun profileNewsPost(@RequestParam postId: Long): ActionStatus<Map<String, Any>> {
        val myProfileId = myProfileId()
        val newsPost = newsService.getProfileNewsPost(postId)
        val notificationDaysLimit = configurationService.newsNotificationDaysLimit
        val isPostAdmin = permissionService.isAdmin(myProfileId, newsPost.profileId)

        return ActionStatus.createSuccessStatus(mapOf("newsPost" to newsPost,
                "canEdit" to isPostAdmin,
                "canSendSpam" to canSendSpam(myProfileId, isPostAdmin, newsPost, notificationDaysLimit),
                "notificationDaysLimit" to notificationDaysLimit))
    }


    @PostMapping(Urls.VALIDATE_SHARE_PURCHASE)
    fun validateShare(@Valid @RequestBody sharePurchase: SharePurchase, bindingResult: BindingResult): ActionStatus<*> {
        userAuthValidator.validate(sharePurchase, bindingResult)

        return if (bindingResult.hasFieldErrors()) {
            ActionStatus.createErrorStatus<Any>(bindingResult, baseControllerService.messageSource)
        } else ActionStatus.createSuccessStatus<Any>()

    }

    @PostMapping(Urls.VALIDATE_INVEST_PURCHASE)
    fun validateInvest(@Valid @RequestBody info: InvestingOrderInfo, investErrors: BindingResult): ActionStatus<*> =
            if (investErrors.hasFieldErrors()) {
                ActionStatus.createErrorStatus<Any>(investErrors, baseControllerService.messageSource)
            } else ActionStatus.createSuccessStatus<Any>()

    private fun canSendSpam(clientId: Long, isPostAdmin: Boolean, post: ProfileNewsDTO, notificationDaysLimit: Int): Boolean {
        var canSendSpam = false
        if (post.campaignId > 0) {
            canSendSpam = permissionService.hasAdministrativeRole(clientId)
            if (isPostAdmin && !canSendSpam) {
                val lastNotifyTime = postDAO.selectCampaignPostsLastNotificationTime(post.campaignId)
                canSendSpam = lastNotifyTime == null || Date().after(DateUtils.addDays(lastNotifyTime, notificationDaysLimit))
            }
        }
        return canSendSpam
    }

    @GetMapping(Urls.CAMPAIGN)
    fun getCampaign(@RequestParam(value = "objectId") campaignAlias: String): CampaignInfo =
            getCampaignInfo(myProfileId(), campaignAlias, true)

    @GetMapping(Urls.CAMPAIGN_COLLECTED_AMOUNT)
    fun getCampaignCollectedAmount(@RequestParam campaignId: Long): BigDecimal = campaignService.getCollectedAmount(campaignId)

    @GetMapping(Urls.CAMPAIGN_SHORT_INFO)
    fun getCampaignShortInfo(@RequestParam(value = "webCampaignAlias") webCampaignAlias: String): ActionStatus<CampaignShortInfo> {
        val myProfileId = myProfileId()
        val campaign = campaignService.getCampaignSafe(myProfileId, webCampaignAlias)
        val canChangeCampaign = campaignPermissionService.isEditable(myProfileId, campaign)
        return ActionStatus.createSuccessStatus(CampaignShortInfo(campaign, canChangeCampaign))
    }

    @GetMapping(Urls.CAMPAIGNS)
    fun getCampaign(@RequestParam(value = "id") ids: List<Long>): List<CampaignInfo> = getCampaignsInfos(campaignService.getCampaigns(ids))

    @GetMapping(Urls.CAMPAIGNS_DTO)
    fun getCampaignsDto(@RequestParam(value = "ids[]") ids: List<Long>): List<TopCampaignDTO> =
            TopCampaignDTO.convert(campaignService.getCampaigns(ids))

    @GetMapping(Urls.SEARCH_CAMPAIGN_SPONSOR_ALIAS)
    fun searchCampaignSponsorAlias(@RequestParam sponsorAlias: String,
                                   @RequestParam(defaultValue = "0") offset: Int,
                                   @RequestParam(defaultValue = "10") limit: Int): List<TopCampaignDTO> =
            TopCampaignDTO.convert(campaignService.getCampaignBySponsorAlias(sponsorAlias, offset, limit))

    @GetMapping(Urls.CAMPAIGN_NEW_EVENTS_COUNT)
    fun getCampaignNewEventsCount(@RequestParam campaignId: Long): ActionStatus<Map<String, Any>> {
        val map = mutableMapOf<String, Any>("backersCount" to campaignService.getCampaignBackersCount(campaignId),
                "commentsCount" to commentsService.getCommentsCount(campaignId, ObjectType.CAMPAIGN),
                "updatesCount" to postDAO.selectCampaignPostsCount(campaignId),
                "faqCount" to campaignFaqService.selectCampaignFaqCountByCampaign(campaignId))
        if (!isAnonymous()) {
            val myProfileId = myProfileId()
            val c = getCampaignStatisticElement(campaignId, myProfileId)
            if (c != null) {
                map.put("newCommentCount", c.newCommentCount)
                map.put("newNewsCount", c.newNewsCount)
                map.put("newPurchaseCount", c.newPurchaseCount)
            }
            profileLastVisitService.insertOrUpdateProfileLastVisit(myProfileId, ProfileLastVisitType.CAMPAIGN_PAGE, campaignId)
        }
        return ActionStatus.createSuccessStatus(map)
    }

    @GetMapping(Urls.GET_SIMPLE_CAMPAIGN)
    fun getSimpleCampaign(@RequestParam campaignAlias: String): ActionStatus<CampaignInfo> =
            ActionStatus.createSuccessStatus(getCampaignInfo(myProfileId(), campaignAlias, false))

    private fun getCampaignInfo(clientId: Long, campaignAlias: String, withShares: Boolean): CampaignInfo {
        val campaign: Campaign = if (withShares) {
            campaignService.getCampaignWithShares(clientId, campaignService.getCampaignId(campaignAlias), false)
        } else {
            campaignService.getCampaignSafe(clientId, campaignAlias)
        }
        if (campaign.sponsorAlias != null) {
            val sponsor = sponsorDAO.selectSponsor(campaign.sponsorAlias ?: "")
            campaign.sponsorMultiplier = sponsor?.multiplier
        }

        campaign.shares?.forEach {
            it.linkedDeliveries = deliveryService.getLinkedDeliveries(it.shareId, SubjectType.SHARE)
        }

        val isEditable = campaignPermissionService.isEditable(clientId, campaign)
        return CampaignInfo(campaign, isEditable)
    }

    @GetMapping(Urls.CAMPAIGN_VIDEO_INFO)
    fun getVideoInfo(@RequestParam videoProfileId: Long, @RequestParam videoId: Long): VideoInfo? =
            videoService.getVideoSafely(videoProfileId, videoProfileId, videoId)

    @GetMapping(Urls.DOES_CAMPAIGN_HAVE_CONTACT_EMAIL_TO_SEND_QUESTION_TO_AUTHOR)
    fun doesCampaignHaveContactEmailToSendQuestionToAuthor(@RequestParam campaignId: Long): ActionStatus<Boolean> {
        val campaignContacts = campaignService.getCampaignContacts(myProfileId(), campaignId, 0, 0)
        return if (campaignContacts.none { StringUtils.isEmpty(it.email) }) ActionStatus.createSuccessStatus(true)
        else ActionStatus.createSuccessStatus(false)
    }

    @GetMapping(Urls.CAMPAIGN_CONTACTS)
    fun getCampaignContacts(@RequestParam campaignId: Long): List<CampaignContact> =
            try {
                val myProfileId = myProfileId()
                val campaign = campaignService.getCampaignSafe(campaignId)
                permissionService.checkIsAdmin(myProfileId, campaign.profileId ?: -1)
                campaignService.getCampaignContacts(myProfileId, campaignId, 0, 0)
            } catch (e: NotFoundException) {
                emptyList()
            }

    @GetMapping(Urls.CAMPAIGN_CONTACTS_COUNT)
    fun getCampaignContactsCount(@RequestParam campaignId: Long): Int = campaignService.getCampaignContactsCount(campaignId)

    @GetMapping(Urls.CAMPAIGN_CURATORS)
    fun getCampaignCurators(@RequestParam campaignId: Long): List<CampaignContact> = campaignService.getCampaignCurators(campaignId)

    @GetMapping(Urls.CAMPAIGN_RELATED_LIST)
    fun getRelatedCampaigns(@RequestParam(required = false) objectId: Long): Collection<Campaign> =
            campaignStatsService.getRelatedCampaigns(objectId)


    @GetMapping(Urls.CAMPAIGN_LIST_WITH_COUNT)
    fun getCampaignList(@RequestParam profileId: Long, @RequestParam(defaultValue = "0") offset: Int,
                        @RequestParam(defaultValue = "10") limit: Int): ListWithCount<*> {
        val profile = baseControllerService.profileService.getProfileSafe(profileId)
        val myProfileId = myProfileId()
        val campaignStatuses: EnumSet<CampaignStatus> = if (permissionService.isAdmin(myProfileId, profileId)) {
            EnumSet.of(CampaignStatus.ALL)
        } else {
            EnumSet.of(CampaignStatus.ACTIVE, CampaignStatus.FINISHED)
        }
        val campaignList = campaignService.getCampaignsByStatus(profile.profileId, campaignStatuses, offset, limit)
        val count = campaignService.getCampaignsCount(profile.profileId, campaignStatuses)

        return if (campaignPermissionService.isAuthorOrAuthorAdmin(myProfileId, profileId)) {
            ListWithCount(addNewEventsCount(myProfileId, campaignList), count)
        } else ListWithCount(campaignList, count)
    }


    @GetMapping(Urls.BACKED_CAMPAIGN_LIST)
    fun getBackedCampaignList(@RequestParam profileId: Long, @RequestParam(defaultValue = "0") offset: Int,
                              @RequestParam(defaultValue = "10") limit: Int): List<*> =
            campaignService.getBackedCampaignList(profileId, offset, limit)

    @GetMapping(Urls.SHORT_CAMPAIGN_STAT)
    fun getShortCampaignStat(@RequestParam campaignId: Long): Map<String, Any?> {
        val allStats = getCampaignStatisticElement(campaignId, PermissionService.ANONYMOUS_USER_PROFILE_ID)
        val map = mutableMapOf<String, Any?>(
                "allCommentCount" to allStats?.newCommentCount,
                "allNewsCount" to allStats?.newNewsCount,
                "allPurchaseCount" to allStats?.newPurchaseCount,
                "allAmount" to allStats?.newAmount)
        if (!isAnonymous()) {
            map.put("isAnonim", false)
            val newStats = getCampaignStatisticElement(campaignId, myProfileId())
            map.put("newCommentCount", newStats?.newCommentCount ?: 0)
            map.put("newNewsCount", newStats?.newNewsCount ?: 0)
            map.put("newPurchaseCount", newStats?.newPurchaseCount ?: 0)
            map.put("newAmount", newStats?.newAmount ?: 0)
        } else {
            map.put("isAnonim", true)
            map.put("newCommentCount", 0)
            map.put("newNewsCount", 0)
            map.put("newPurchaseCount", 0)
            map.put("newAmount", 0)
        }
        return map
    }

    @GetMapping(Urls.USER_BACKED_CAMPAIGN_TAG_COUNT_LIST)
    fun getUserBackedCampaignTagCountList(@RequestParam profileId: Long): List<Map<*, *>> =
            baseControllerService.toCamelCase(campaignService.getUserBackedCampaignTagCountList(profileId))

    @GetMapping(Urls.GET_LAST_CAMPAIGN_MODERATION_MESSAGE)
    fun getLastCampaignModerationMessage(@RequestParam campaignId: Long): ActionStatus<String> {
        val moderationMessage = moderationMessageService.getLastCampaignModerationMessage(campaignId)
        var moderationMessageText = ""
        if (moderationMessage != null) {
            moderationMessageText = moderationMessage.message ?: ""
        }
        return ActionStatus.createSuccessStatus(moderationMessageText)
    }

    private fun getCampaignStatisticElement(campaignId: Long, profileId: Long): CampaignWithNewEventsCount? {
        val list = campaignService.selectCampaignNewEventsCountList(listOf(campaignId), profileId, null)
        return if (list != null && !list.isEmpty()) list[0] else null
    }

    private fun addNewEventsCount(profileId: Long, campaignList: List<Campaign>): List<CampaignWithNewEventsCount> {
        return Campaign.copyAllFields(campaignList, campaignService.selectCampaignNewEventsCountList(Utils.getIdList(campaignList), profileId, null))
    }

    private fun getCampaignsInfos(campaigns: List<Campaign>): List<CampaignInfo> {
        val campaignInfos = ArrayList<CampaignInfo>(campaigns.size)
        campaigns.mapTo(campaignInfos) { CampaignInfo(it, it.status?.isDraft ?: false) }
        return campaignInfos
    }

    @GetMapping(Urls.GET_LAST_CAMPAIGN_MODERATION_MESSAGE_LIST)
    fun getLastCampaignModerationMessageList(@RequestParam profileId: Long,
                                             @RequestParam campaignStatuses: List<CampaignStatus>): ActionStatus<List<Map<*, *>>> =
            ActionStatus.createSuccessStatus(baseControllerService.toCamelCase(moderationMessageService.getLastCampaignModerationMessageList(profileId,
                    EnumSet.copyOf(campaignStatuses))))

    @GetMapping(Urls.GET_SHARE_DELIVERY_SERVICES)
    fun getShareDeliveryServices(@RequestParam shareId: Long): List<LinkedDelivery> =
            deliveryService.getLinkedDeliveries(shareId, SubjectType.SHARE)

    @PostMapping(Urls.ADD_CAMPAIGN_MODERATION_AUTHOR_RESPONSE_MESSAGE)
    fun addCampaignModerationMessage(moderationMessage: ModerationMessage): ActionStatus<*> {
        moderationMessage.status = ModerationMessageStatus.AUTHOR_MESSAGE
        moderationMessage.senderId = myProfileId()
        moderationMessageService.addModerationMessage(moderationMessage)
        return ActionStatus.createSuccessStatus<Any>()
    }


    @GetMapping(Urls.MODERATOR_CAMPAIGN_GET_CURRENT_MANAGER)
    fun getCurretManagerOfCampaign(@RequestParam campaignId: Long): ActionStatus<PlanetaManager> {
        campaignService.getEditableCampaignSafe(myProfileId(), campaignId)
        return ActionStatus.createSuccessStatus(planetaManagersService.getCampaignManager(campaignId))
    }

    class CampaignInfo(campaign: Campaign, val isCanChangeCampaign: Boolean) : Campaign(campaign) {
        val moderationMap: Map<CampaignStatus, EnumSet<CampaignStatus>>
            get() = CampaignStatusUtils.getModerationMap()
    }

    @PostMapping(Urls.CAMPAIGN_SAVE_CONTACT_INFO)
    fun campaignSaveContactInfo(@RequestParam campaignId: Long, @RequestParam vkUrl: String, @RequestParam facebookUrl: String,
                                @RequestParam siteUrl: String, @RequestParam twitterUrl: String, @RequestParam emailList: String,
                                @RequestParam displayName: String, @RequestParam imageId: Long): ActionStatus<Map<String, String>> {
        val campaign = campaignService.getCampaignSafe(campaignId)
        campaignService.updateCampaignContactList(myProfileId(), campaignId,
                Arrays.asList(*emailList.split(",".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()))

        val profile = baseControllerService.profileService.getProfileSafe(campaign.profileId ?: -1)
        val profileId = if (profile.profileType == ProfileType.HIDDEN_GROUP) profile.creatorProfileId else profile.profileId

        val profileSites = ProfileSites.builder
                .profileId(profileId)
                .vkUrl(vkUrl)
                .facebookUrl(facebookUrl)
                .siteUrl(siteUrl)
                .twitterUrl(twitterUrl)
                .build()

        val resultMap: MutableMap<String, String>

        val binder = DataBinder(profileSites)
        binder.validator = profileSitesValidator
        binder.validate()
        val result = binder.bindingResult
        if (result.hasErrors()) {
            return baseControllerService.createErrorStatus(binder.bindingResult)
        } else {
            resultMap = HashMap()
            prepareProfileSites(profileSites, resultMap)
        }
        insertOrUpdateProfileSites(profileSites)
        baseControllerService.profileService.setGroupDisplayNameAndAvatar(myProfileId(), profileId, displayName, imageId)

        return ActionStatus.createSuccessStatus(resultMap)
    }

    private fun insertOrUpdateProfileSites(profileSites: ProfileSites) {
        if ("https://facebook.com/".equals(profileSites.facebookUrl, ignoreCase = true) || profileSites.facebookUrl == null) {
            profileSites.facebookUrl = ""
        }
        if ("https://vk.com/".equals(profileSites.vkUrl, ignoreCase = true) || profileSites.vkUrl == null) {
            profileSites.vkUrl = ""
        }
        if ("http://".equals(profileSites.siteUrl, ignoreCase = true) || profileSites.siteUrl == null) {
            profileSites.siteUrl = ""
        }
        if ("https://twitter.com/".equals(profileSites.twitterUrl, ignoreCase = true) || profileSites.twitterUrl == null) {
            profileSites.twitterUrl = ""
        }
        if ("https://plus.google.com/".equals(profileSites.googleUrl, ignoreCase = true) || profileSites.googleUrl == null) {
            profileSites.googleUrl = ""
        }
        profileSitesService.insertOrUpdateProfileSites(profileSites)
    }

    @GetMapping(Urls.FINISH_CAMPAIGN_TRAFFIC_INFO)
    fun getFinishCampaignTrafficInfo(
            @RequestParam(value = "campaignId") campaignId: Long): ActionStatus<Map<String, Any>> {
        val campaign = campaignService.getCampaignSafe(campaignId)
        val map = HashMap<String, Any>()

        val authorActiveCampaign = campaignService.getAuthorActiveCampaign(campaign.creatorProfileId)
        if (authorActiveCampaign != null) {
            map.put("campaign", authorActiveCampaign)
        }
        val productList = productUsersService.getProductListForCampaignByReferrer(campaign.creatorProfileId, campaignId, 0, 6)
        if (productList != null && productList.size > 0) {
            map.put("productList", productList)
            map.put("productsCount", productUsersService.getProductsCountByReferrer(campaign.creatorProfileId))
        }

        if (authorActiveCampaign == null && (productList == null || productList.size == 0)) {
            val campaignList = searchService.searchForCampaignRelatedForFinishedCampaign(campaign)
            map.put("campaignList", campaignList)
        }

        return ActionStatus.createSuccessStatus(map)
    }

    @GetMapping(Urls.CAMPAIGN_PURCHASED_SHARE_IDS)
    fun getPurchasedShareIds(@RequestParam(value = "campaignId") campaignId: Long): ActionStatus<List<Long>> =
            if (isAnonymous()) {
                ActionStatus.createSuccessStatus(null)
            } else ActionStatus.createSuccessStatus(campaignService.selectPurchasedShareIds(myProfileId(), campaignId))

    @GetMapping(Urls.CAMPAIGN_PURCHASED_CAMPAIGN_IDS)
    fun getPurchasedCampaignIds(): ActionStatus<List<Long>> =
            if (isAnonymous()) {
                ActionStatus.createSuccessStatus(null)
            } else ActionStatus.createSuccessStatus(campaignService.selectPurchasedCampaignIds(myProfileId()))

    @GetMapping(Urls.CAMPAIGN_END_IS_SUBSCRIBED)
    fun isSubscribedOnCampainEnd(@RequestParam(value = "campaignId") campaignId: Long): ActionStatus<Boolean> =
            if (isAnonymous()) {
                ActionStatus.createSuccessStatus(false)
            } else ActionStatus.createSuccessStatus(campaignSubscribeService.isSubscribedOnCampaignEnd(myProfileId(), campaignId))

    @PostMapping(Urls.CAMPAIGN_END_SUBSCRIBE)
    fun subscribeOnCampaignEnd(@RequestParam campaignId: Long): ActionStatus<*> {
        if (isAnonymous()) {
            return ActionStatus.createSuccessStatus<Any>()
        }
        if (campaignId == 0L) {
            return ActionStatus.createErrorStatus<Any>(null)
        }
        campaignSubscribeService.subscribeOnCampaignEnd(myProfileId(), campaignId)
        return ActionStatus.createSuccessStatus<Any>()
    }

    @PostMapping(Urls.CAMPAIGN_END_UNSUBSCRIBE)
    fun unsubscribeOnCampaignEnd(@RequestParam campaignId: Long): ActionStatus<*> {
        if (isAnonymous()) {
            return ActionStatus.createSuccessStatus<Any>()
        }
        if (campaignId == 0L) {
            return ActionStatus.createErrorStatus<Any>(null)
        }
        campaignSubscribeService.unsubscribeOnCampaignEnd(myProfileId(), campaignId)
        return ActionStatus.createSuccessStatus<Any>()
    }

    private fun prepareProfileSites(profileSites: ProfileSites, map: MutableMap<String, String>) {
        if (profileSites.siteUrl == null) {
            profileSites.siteUrl = ""
        } else {
            profileSites.siteUrl?.let {
                if (it.isNotBlank()) {
                    val url = WebUtils.appendProtocolIfNecessary(it, false)
                    map["siteUrl"] = url
                    profileSites.siteUrl = url
                }
            }
        }

        if (profileSites.vkUrl == null) {
            profileSites.vkUrl = ""
        } else {
            profileSites.vkUrl?.let {
                if (it.isNotBlank()) {
                    val url = WebUtils.appendProtocolIfNecessary(it, true)
                    map["vkUrl"] = url
                    profileSites.vkUrl = url
                }
            }
        }

        if (profileSites.facebookUrl == null) {
            profileSites.facebookUrl = ""
        } else {
            profileSites.facebookUrl?.let {
                if (it.isNotBlank()) {
                    val url = WebUtils.appendProtocolIfNecessary(it, true)
                    map["facebookUrl"] = url
                    profileSites.facebookUrl = url
                }
            }
        }

        if (profileSites.twitterUrl == null) {
            profileSites.twitterUrl = ""
        } else {
            profileSites.twitterUrl?.let {
                if (it.isNotBlank()) {
                    val url = WebUtils.appendProtocolIfNecessary(it, true)
                    map["twitterUrl"] = url
                    profileSites.twitterUrl = url
                }
            }
        }

        if (profileSites.googleUrl == null) {
            profileSites.googleUrl = ""
        } else {
            profileSites.googleUrl?.let {
                if (it.isNotBlank()) {
                    val url = WebUtils.appendProtocolIfNecessary(it, true)
                    map["googleUrl"] = url
                    profileSites.googleUrl = url
                }
            }
        }
    }

}

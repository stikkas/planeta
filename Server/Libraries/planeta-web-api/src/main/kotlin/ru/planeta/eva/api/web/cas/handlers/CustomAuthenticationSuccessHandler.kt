package ru.planeta.eva.api.web.cas.handlers

import org.apache.log4j.Logger
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.Authentication
import ru.planeta.api.model.authentication.AuthenticatedUserDetails
import ru.planeta.api.service.common.UserLastOnlineTimeService
import ru.planeta.api.service.common.UserLoginInfoService
import ru.planeta.api.web.authentication.AuthUtils
import ru.planeta.api.web.authentication.addCookie
import ru.planeta.api.web.utils.SessionUtils
import ru.planeta.commons.web.CookieUtils
import ru.planeta.commons.web.IpUtils
import ru.planeta.commons.web.WebUtils
import ru.planeta.dao.trashcan.UserGtmCidDAO
import ru.planeta.eva.api.web.cas.services.AuthTokenService
import ru.planeta.model.common.UserLastOnlineTime
import ru.planeta.model.common.UserLoginInfo
import java.util.*
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

/**
 * User: m.shulepov
 * Date: 21.03.12
 * Time: 17:04
 */
class CustomAuthenticationSuccessHandler(private val authCookieDomain: String,
                                         private val authToken: String) {

    @Autowired
    private lateinit var userLoginInfoService: UserLoginInfoService

    @Autowired
    private lateinit var userGtmCidDAO: UserGtmCidDAO

    @Autowired
    private lateinit var authTokenService: AuthTokenService

    @Autowired
    private lateinit var userLastOnlineTimeService: UserLastOnlineTimeService


    fun onAuthenticationSuccess(request: HttpServletRequest, response: HttpServletResponse, authentication: Authentication?) {
        if (!request.serverName.startsWith("localhost")) {
            setCookie(authentication, request, response)
        }
        try {
            val userDetails = authentication?.principal as? AuthenticatedUserDetails
            val uai = userDetails?.userAuthorizationInfo
            if (uai != null) {
                val userLoginInfo = getUserLoginInfoFromRequest(request, uai.profile.profileId)
                userLoginInfoService.addOrUpdateUserLoginInfo(userLoginInfo)
                userLastOnlineTimeService.addOrUpdateUserLastOnlineTime(UserLastOnlineTime(userLoginInfo.profileId, userLoginInfo.timeLogin ?: Date()))
                try {
                    userGtmCidDAO.insert(uai.profile.profileId, WebUtils.getGtmCidFromCookie(request))
                } catch (e: Exception) {
                    log.warn(e)
                }

            }
        } catch (ex: Exception) {
            log.error(ex)
        }

        if (authentication != null) {
            AuthUtils.saveAuditedAdminId(request, authentication)
        }
    }

    fun setCookie(authentication: Authentication?, request: HttpServletRequest, response: HttpServletResponse) {
        val token = authTokenService.createToken(authentication as UsernamePasswordAuthenticationToken, request.getHeader("User-Agent"))
        addCookie(request, response, authToken, token, authCookieDomain, CookieUtils.ONE_MONTH)
        SessionUtils.session.setAttribute(authToken, token)
    }

    companion object {
        private val log = Logger.getLogger(CustomAuthenticationSuccessHandler::class.java)

        /**
         * Creates UserLoginInfo from HttpServletRequest
         *
         * @param request   request to be parsed
         * @param profileId user's profile id
         */
        private fun getUserLoginInfoFromRequest(request: HttpServletRequest, profileId: Long): UserLoginInfo {
            val userLoginInfo = UserLoginInfo()
            try {
                userLoginInfo.profileId = profileId
                userLoginInfo.timeLogin = Date()
                userLoginInfo.userAgent = request.getHeader("User-Agent")
                userLoginInfo.userIpAddress = IpUtils.getRemoteAddr(request)
            } catch (ex: Exception) {
                log.error("Error occured", ex)
            }

            return userLoginInfo
        }
    }
}


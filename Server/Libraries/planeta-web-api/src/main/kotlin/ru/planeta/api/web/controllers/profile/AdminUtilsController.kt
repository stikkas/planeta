package ru.planeta.api.web.controllers.profile

import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import ru.planeta.api.exceptions.PermissionException
import ru.planeta.api.service.profile.ProfileService
import ru.planeta.api.web.controllers.Urls
import ru.planeta.model.profile.Profile

/**
 * Created by asavan on 16.12.2016.
 */
@RestController
class AdminUtilsController(private val profileService: ProfileService) {

    @GetMapping(Urls.PROFILE_LIST)
    fun searchForUsers(@RequestParam(value = "profiles[]") profiles: List<Long>,
                       @RequestParam apiKey: String): List<Profile> {
        if ("asavanSecretKey" != apiKey) {
            throw PermissionException()
        }
        return profileService.getProfiles(profiles)
    }
}


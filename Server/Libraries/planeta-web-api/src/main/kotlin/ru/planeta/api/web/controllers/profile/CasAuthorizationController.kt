package ru.planeta.api.web.controllers.profile

import org.apache.log4j.Logger
import org.springframework.security.cas.authentication.CasAuthenticationToken
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import ru.planeta.api.web.controllers.Urls
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

/**
 * Controller for getting service tickets for specified service
 * After you have service ticket, you can authorize with it at specified service
 * Service ticket should be provided as parameter "ticket"
 */
@RestController("planetaCasAuthorizationController")
class CasAuthorizationController {

    @GetMapping(Urls.GET_PROXY_TICKET)
    fun getServiceTicket(@RequestParam service: String,
                         @RequestParam(defaultValue = "json") type: String,
                         request: HttpServletRequest, response: HttpServletResponse): String? {
        val principal = request.userPrincipal
        var ticket: String? = null

        if (principal != null && principal is CasAuthenticationToken) {
            ticket = principal.assertion.principal.getProxyTicketFor(service)
            log.info("auth token: " + principal)
            log.info("ticket for service \"$service\": $ticket")
        }

        var responseText: String? = null
        if (ticket == null) {
            response.status = HttpServletResponse.SC_UNAUTHORIZED
        } else {
            when {
                type.equals("json", ignoreCase = true) -> responseText = String.format("{\"ticket\":\"%s\"}", ticket)
                type.equals("text", ignoreCase = true) -> responseText = ticket
                else -> log.error("unknown response type: " + type)
            }
        }

        return responseText
    }

    companion object {
        private val log = Logger.getLogger(CasAuthorizationController::class.java)
    }
}

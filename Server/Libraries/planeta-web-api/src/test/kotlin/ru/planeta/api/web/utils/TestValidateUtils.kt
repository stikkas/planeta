package ru.planeta.api.web.utils

import org.junit.Test
import ru.planeta.api.utils.ValidateUtils

import org.junit.Assert.*
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit4.SpringRunner

/**
 * author: a.savanovich
 * Date: 15.04.12
 * Time: 1:17
 */
@RunWith(SpringRunner::class)
@ContextConfiguration(locations = ["classpath*:spring/applicationContext-*.xml",
    "classpath*:/spring/applicationContext-dao.xml",
    "classpath*:/spring/applicationContext-geo.xml"])
@SpringBootTest
class TestValidateUtils {

    @Autowired
    private lateinit var validateUtils: ValidateUtils

    @Test
    fun testIsValid() {
        assertTrue(ValidateUtils.isValidRussians("Rodrigo_Lavos"))
    }

    @Test
    fun testValidEmail() {
        assertTrue(ValidateUtils.isValidEmail("michail.michail@gmail.com"))
        assertFalse(ValidateUtils.isValidEmail("_DELETED_304841_detector5@yandex.ru_"))
        assertTrue(ValidateUtils.isValidEmail("dima@koulikoff.ru"))
    }
}


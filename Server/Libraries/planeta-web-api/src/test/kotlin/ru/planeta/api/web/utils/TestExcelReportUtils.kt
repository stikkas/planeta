package ru.planeta.api.web.utils

import org.apache.commons.lang3.time.DateUtils
import org.apache.poi.ss.usermodel.Cell
import org.apache.poi.ss.usermodel.WorkbookFactory
import org.junit.After
import org.junit.Assert.assertNotNull
import org.junit.Before
import org.junit.Ignore
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit4.SpringRunner
import ru.planeta.api.model.stat.PurchaseReport
import ru.planeta.api.service.statistic.AdminStatisticService
import ru.planeta.commons.model.Gender
import ru.planeta.dao.TransactionScope
import ru.planeta.dao.commondb.UserPrivateInfoDAO
import ru.planeta.dao.profiledb.ProfileDAO
import ru.planeta.model.common.UserPrivateInfo
import ru.planeta.model.enums.ProfileStatus
import ru.planeta.model.enums.ProfileType
import ru.planeta.model.enums.UserStatus
import ru.planeta.model.profile.Profile
import java.io.FileInputStream
import java.io.StringWriter
import java.math.BigDecimal
import java.math.RoundingMode
import java.util.*

/**
 * User: m.shulepov
 * Date: 08.12.12
 * Time: 9:55
 */
@RunWith(SpringRunner::class)
@ContextConfiguration(locations = ["classpath*:spring/applicationContext-*.xml",
    "classpath*:/spring/applicationContext-dao.xml",
    "classpath*:/spring/applicationContext-geo.xml"])
@SpringBootTest
class TestExcelReportUtils {

    @Autowired
    private lateinit var adminStatisticService: AdminStatisticService

    @Autowired
    private lateinit var userPrivateInfoDAO: UserPrivateInfoDAO

    @Autowired
    private lateinit var profileDAO: ProfileDAO

    private lateinit var transactionScope: TransactionScope

    @Before
    fun setUp() {
        transactionScope = TransactionScope.createOrGetCurrentTest()
    }

    @After
    fun tearDown() {
        transactionScope.close()
    }

    @Test
    @Ignore
    fun testDashboard() {
        val now = Date()
        val prevMonthStart = DateUtils.truncate(DateUtils.addMonths(now, -1), Calendar.MONTH)
        val thisMonthEnd = DateUtils.truncate(DateUtils.addMonths(now, 1), Calendar.MONTH)

        val purchaseReportsMap = adminStatisticService.getStatPurchaseCompositeReport(
                6L,
                prevMonthStart,
                thisMonthEnd,
                "month")

        val firstEntryValue = if (purchaseReportsMap.firstEntry() == null) null else purchaseReportsMap.firstEntry().value
        val lastEntryValue = if (purchaseReportsMap.lastEntry() == null) null else purchaseReportsMap.lastEntry().value
        val b = 0

    }

    @Test
    @Ignore
    fun testGenerateSalesReport() {
        val clientId = insertPlanetaAdminPrivateInfo("test@tratatata.com", "123").userId
        val reportData1 = adminStatisticService.getStatPurchaseCompositeReport(clientId, null, null, "day")
        val sampleReportData = TreeMap<Date, PurchaseReport?>()
        var count = 0
        for (date in reportData1.keys) {
            sampleReportData.put(date, reportData1[date])
            count++
            if (count > 10) {
                break
            }
        }
        val sw = StringWriter()
        ExcelReportUtils.generateSalesReport(sampleReportData, BigDecimal.ZERO, sw)
        assertNotNull(sw.toString())
        //
        val reportData2 = adminStatisticService.getStatPurchaseShareReport(clientId, DateUtils.addMonths(Date(), -1), Date())
        ExcelReportUtils.generateStatisticReport(reportData2.subList(0, Math.min(reportData2.size, 10)), BigDecimal(0), 0, sw, ExcelReportUtils.ReportType.SHARES_SALE)
        assertNotNull(sw.toString())
        //
        val reportData4 = adminStatisticService.getStatPurchaseProductReport(clientId, DateUtils.addMonths(Date(), -1), Date())
        ExcelReportUtils.generateStatisticReport(reportData4.subList(0, Math.min(reportData4.size, 10)), BigDecimal(0), 0, sw, ExcelReportUtils.ReportType.SALES_PRODUCTS)
        assertNotNull(sw.toString())
        //
    }

    @Ignore
    @Test
    fun testReadDocument() {
        FileInputStream("E:/orders.xls").use {

            val wb = WorkbookFactory.create(it)
            val sheet = wb.getSheetAt(2)

            val rowsCount = sheet.lastRowNum
            //System.out.println("Total Number of Rows: " + (rowsCount + 1));
            for (i in 0..rowsCount) {
                val row = sheet.getRow(i)
                val colCounts = row.lastCellNum.toInt()
                //System.out.println("Total Number of Cols: " + colCounts);
                for (j in 0 until colCounts) {
                    val cell = row.getCell(j)
                    cell.cellType = Cell.CELL_TYPE_STRING
                    println(cell.stringCellValue)
                }

                println("-------------------------------")
            }

        }
    }

    @Ignore
    @Test
    fun testReadDocumentBiblio() {
        FileInputStream("E:/test/biblio.xlsx").use {
            val wb = WorkbookFactory.create(it)
            val sheet = wb.getSheetAt(0)
            val rowStart = 5
            val columnStart = 0

            val rowsCount = sheet.lastRowNum
            //System.out.println("Total Number of Rows: " + (rowsCount + 1));
            for (i in rowStart..rowsCount) {
                val row = sheet.getRow(i)
                val colCounts = row.lastCellNum.toInt()
                //System.out.println("Total Number of Cols: " + colCounts);
                for (j in columnStart until colCounts) {
                    val cell = row.getCell(j)
                    if (j == 8) {
                        cell.cellType = Cell.CELL_TYPE_NUMERIC

                        //                        System.out.println("CEILING " + BigDecimal.valueOf(cell.getNumericCellValue()).setScale(4, RoundingMode.CEILING));
                        //                        System.out.println("DOWN " + BigDecimal.valueOf(cell.getNumericCellValue()).setScale(4, RoundingMode.DOWN));
                        //                        System.out.println("FLOOR " + BigDecimal.valueOf(cell.getNumericCellValue()).setScale(4, RoundingMode.FLOOR));
                        //                        System.out.println("HALF_DOWN " + BigDecimal.valueOf(cell.getNumericCellValue()).setScale(4, RoundingMode.HALF_DOWN));
                        //                        System.out.println("HALF_EVEN " + BigDecimal.valueOf(cell.getNumericCellValue()).setScale(4, RoundingMode.HALF_EVEN));
                        println("HALF_UP " + BigDecimal.valueOf(cell.numericCellValue).setScale(4, RoundingMode.HALF_UP))
                        //                        System.out.println("UNNECESSARY " + BigDecimal.valueOf(cell.getNumericCellValue()).setScale(4, RoundingMode.UNNECESSARY));
                        //                        System.out.println("UP " + BigDecimal.valueOf(cell.getNumericCellValue()).setScale(4, RoundingMode.UP));
                    } else {
                        cell.cellType = Cell.CELL_TYPE_STRING
                        println(cell.stringCellValue)
                    }
                }

                println("-------------------------------")
            }
        }
    }

    private fun insertPlanetaAdminPrivateInfo(email: String, password: String): UserPrivateInfo {
        val info = insertUserPrivateInfo(email, password)
        info.userStatus = EnumSet.of(UserStatus.ADMIN)
        userPrivateInfoDAO.update(info)
        return info
    }

    private fun insertUserPrivateInfo(email: String, password: String): UserPrivateInfo {
        val userPrivateInfo = UserPrivateInfo()
        userPrivateInfo.userId = insertUserProfile().profileId
        userPrivateInfo.email = email.trim { it <= ' ' }.toLowerCase()
        userPrivateInfo.username = userPrivateInfo.email
        userPrivateInfo.password = password
        userPrivateInfo.userStatus = EnumSet.of(UserStatus.USER)

        userPrivateInfoDAO.insert(userPrivateInfo)
        return userPrivateInfo
    }

    private fun insertUserProfile(): Profile {
        val profile = Profile()
        profile.profileType = ProfileType.USER
        profile.imageUrl = "/imageurl"
        profile.imageId = 1
        profile.displayName = "displayName"
        profile.alias = "alias"
        profile.status = ProfileStatus.NOT_SET
        profile.cityId = 1
        profile.countryId = 1
        profile.userBirthDate = Date()
        profile.userGender = Gender.NOT_SET
        profileDAO.insert(profile)
        return profile
    }
}

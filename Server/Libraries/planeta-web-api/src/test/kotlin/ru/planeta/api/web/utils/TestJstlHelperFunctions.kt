package ru.planeta.api.web.utils

import org.junit.Ignore
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.mock.web.MockServletContext
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.web.context.ContextLoader
import org.springframework.web.context.ContextLoaderListener

import javax.servlet.ServletContextEvent

/**
 * @author ds.kolyshev
 * Date: 03.11.11
 */
@RunWith(SpringRunner::class)
@ContextConfiguration(locations = ["classpath*:spring/applicationContext-*.xml",
    "classpath*:/spring/applicationContext-dao.xml",
    "classpath*:/spring/applicationContext-geo.xml"])
@SpringBootTest
class TestJstlHelperFunctions {


    @Test
    @Ignore
    fun testGetProperty() {

        val sc = MockServletContext("")
        sc.addInitParameter(ContextLoader.CONFIG_LOCATION_PARAM,
                "classpath*:spring/applicationContext-*.xml classpath*:/spring/applicationContext-dao.xml classpath*:/spring/applicationContext-geo.xml") // <== Customize with your paths
        val listener = ContextLoaderListener()
        val event = ServletContextEvent(sc)
        listener.contextInitialized(event)

        val str = JstlHelperFunctions.getPropertyValue("application.host")
        println(str)
    }
}


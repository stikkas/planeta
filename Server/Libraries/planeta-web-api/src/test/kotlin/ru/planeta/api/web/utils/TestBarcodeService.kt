package ru.planeta.api.web.utils

/**
 *
 * @author a.savanovich
 */

import org.apache.commons.io.FileUtils
import org.junit.Test
import java.io.*

/**
 * Tests for barcode lib
 *
 * @author a.savanovich
 */
class TestBarcodeService {

    @Test
    fun testBarcodeGenerators() {
        try {
            generateQRPicture("qr_out1.png", "http://testqr1.planeta?p1=a1&p2=a2")
            generateQRPicture("qr_out2.png", "http://testqr2.planeta?p1=a3&p2=a4")
        } finally {
            FileUtils.deleteQuietly(File("qr_out1.png"))
            FileUtils.deleteQuietly(File("qr_out2.png"))
        }
    }

    private fun generateQRPicture(filename: String, url: String) {
        val stream = getStreamFromName(filename)
        BarcodeUtils.generateQRPicture(stream, url, 100, 100)
        stream.close()
    }

    private fun getStreamFromName(filename: String): OutputStream {
        val outputFile = File(filename)
        return FileOutputStream(outputFile)
    }
}


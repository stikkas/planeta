package ru.planeta.api.web

import com.brsanthu.googleanalytics.GoogleAnalytics
import com.brsanthu.googleanalytics.ItemHit
import com.brsanthu.googleanalytics.TransactionHit
import org.apache.log4j.Logger
import org.junit.Ignore
import org.junit.Test

/**
 * User: s.makarov
 * Date: 10.12.13
 * Time: 15:16
*/

class TestUniversalAnalytics {

    @Test
    @Ignore
    fun testAnalytics() {
        val gaId = "UA-69745567-1"
        val ga = GoogleAnalytics(gaId)
        //        ga.getConfig().setGatherStats(true);
        //        ga.getConfig().setEnabled(true);
        //        ga.getConfig().setUseHttps(false);

        var transactionHit = TransactionHit()
        transactionHit.trackingId(ga.defaultRequest.trackingId())
        transactionHit.txId("1500008")
        transactionHit.txAffiliation("testShopName")
        transactionHit.txShipping(1.38)
        transactionHit.txTax(0.0)
        transactionHit.currencyCode("RUB")


        var transactionTotalPrice: Double? = 0.0

        var itemHit = ItemHit()
        itemHit.itemName("testItemName1")
        itemHit.txId(transactionHit.txId())
        itemHit.itemCategory("share")
        itemHit.itemPrice(2.5)
        itemHit.itemQuantity(4)
        itemHit.currencyCode("RUB")
        itemHit.itemCode("123")

        transactionTotalPrice = transactionTotalPrice?.plus(itemHit.itemPrice() * itemHit.itemQuantity()!!)
        ga.post(itemHit)

        itemHit = ItemHit()
        itemHit.itemName("testItemName2")
        itemHit.txId(transactionHit.txId())
        itemHit.itemCategory("share")
        itemHit.itemPrice(1.0)
        itemHit.itemQuantity(3)
        itemHit.currencyCode("RUB")
        itemHit.itemCode("1234")

        transactionTotalPrice = transactionTotalPrice?.plus(itemHit.itemPrice() * itemHit.itemQuantity()!!)
        ga.post(itemHit)

        itemHit = ItemHit()
        itemHit.itemName("testItemName3")
        itemHit.txId(transactionHit.txId())
        itemHit.itemCategory("share")
        itemHit.itemPrice(5.0)
        itemHit.itemQuantity(2)
        itemHit.currencyCode("RUB")
        itemHit.itemCode("1235")

        transactionTotalPrice = transactionTotalPrice?.plus(itemHit.itemPrice() * itemHit.itemQuantity()!!)
        ga.post(itemHit)


        transactionHit.txRevenue(transactionTotalPrice)
        ga.post(transactionHit)

        transactionTotalPrice = 0.0

        transactionHit = TransactionHit()
        transactionHit.trackingId(ga.defaultRequest.trackingId())
        transactionHit.txId("11003")
        transactionHit.txAffiliation("testShopName")
        transactionHit.txShipping(1.0)
        transactionHit.txTax(0.0)
        transactionHit.currencyCode("RUB")

        itemHit = ItemHit()
        itemHit.itemName("testItemName1")
        itemHit.txId(transactionHit.txId())
        itemHit.itemCategory("share")
        itemHit.itemPrice(3.50)
        itemHit.itemQuantity(7)
        itemHit.currencyCode("RUB")
        itemHit.itemCode("123")

        transactionTotalPrice += itemHit.itemPrice() * itemHit.itemQuantity()!!
        ga.post(itemHit)


        transactionHit.txRevenue(transactionTotalPrice)
        ga.post(transactionHit)


        //        PageViewHit pageViewHit = new PageViewHit("http://www.trackSomething.go?myParam=lalala&utm_source=java", "Measurement Protocol Test");
        //        pageViewHit.documentHostName("test.test");
        //        pageViewHit.documentPath("/test");
        //        pageViewHit.clientId("7777777");
        //        pageViewHit.documentUrl("testDocumentUrl");
        //        pageViewHit.documentTitle("testDocumentTitle");
        //
        //        GoogleAnalyticsResponse googleAnalyticsResponse = ga.post(pageViewHit);
        //        LOG.info("[DEBUG] googleAnalyticsResponse: " + googleAnalyticsResponse.toString());

        //        ga.post(new SocialHit("some social network", "some social action", "some social target"));
        //        EventHit eventHit = new EventHit();
        //        eventHit.eventCategory("video");
        //        eventHit.eventAction("play");
        //        eventHit.eventLabel("holiday");
        //        eventHit.eventValue(300);
        //        ga.post(eventHit);

        //ga.postAsync(new PageViewHit("http://www.trackSomething.go?myParam=lalala", "Measurement Protocol Test"));

        /*ga.postAsync(new RequestProvider() {
            public GoogleAnalyticsRequest getRequest() {
                return new PageViewHit("http://www.trackSomething.go?myParam=lalala", "Measurement Protocol Test");
            }
        });*/

        /*ga.getConfig().setGatherStats(false);
        ga.resetStats();
        ga.post(new PageViewHit());
        ga.post(new PageViewHit());
        ga.post(new PageViewHit());
        ga.post(new AppViewHit());
        ga.post(new AppViewHit());
        ga.post(new ItemHit());

        assertEquals(0, ga.getStats().getPageViewHits());
        assertEquals(0, ga.getStats().getAppViewHits());
        assertEquals(0, ga.getStats().getItemHits());

        ga.getConfig().setGatherStats(true);
        ga.resetStats();
        ga.post(new PageViewHit());
        ga.post(new PageViewHit());
        ga.post(new PageViewHit());
        ga.post(new AppViewHit());
        ga.post(new AppViewHit());
        ga.post(new ItemHit());

        assertEquals(3, ga.getStats().getPageViewHits());
        assertEquals(2, ga.getStats().getAppViewHits());
        assertEquals(1, ga.getStats().getItemHits());*/
    }

    companion object {
        private val LOG = Logger.getLogger(TestUniversalAnalytics::class.java)
    }

}

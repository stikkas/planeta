package ru.planeta.reports;

public enum ReportType {
    CSV, EXCEL, EXCEL2007;

    public SimpleReport createReport(String fileName) {
        if (this == ReportType.CSV) {
            return new CsvReport(fileName);
        } else if (this == ReportType.EXCEL) {
            return new SimpleExcelReport(fileName);
        } else if (this == ReportType.EXCEL2007) {
            return new SimpleExcel2007Report(fileName);
        }
        throw new RuntimeException("bad report Type");
    }
}

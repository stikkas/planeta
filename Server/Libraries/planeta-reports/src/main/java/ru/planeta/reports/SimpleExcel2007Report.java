package ru.planeta.reports;

import org.apache.poi.hssf.usermodel.HSSFHyperlink;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.usermodel.charts.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class SimpleExcel2007Report extends AbstractSimpleExcelReport {
    public SimpleExcel2007Report() {
        super();
    }

    public SimpleExcel2007Report(String fileName) {
        super(fileName);
    }

    @Override
    protected Workbook createWorkbook() {
        return new XSSFWorkbook();
    }

    @Override
    protected String getExtension() {
        return ".xlsx";
    }

    public void addChartForTable(int chartCol, int chartRow, int chartWidth, int chartHeight,int tableCol, int tableRow, int tableWidth, int tableHeight) {

        Drawing drawing = sheet.createDrawingPatriarch();
        ClientAnchor anchor = drawing.createAnchor(0, 0, 0, 0, chartCol, chartRow, chartCol + chartWidth - 1, chartRow + chartHeight);


        Chart chart = drawing.createChart(anchor);

        ChartLegend legend = chart.getOrCreateLegend();
        legend.setPosition(LegendPosition.TOP_RIGHT);

        ScatterChartData data = chart.getChartDataFactory().createScatterChartData();


        ValueAxis bottomAxis = chart.getChartAxisFactory().createValueAxis(AxisPosition.BOTTOM);

        ValueAxis leftAxis = chart.getChartAxisFactory().createValueAxis(AxisPosition.LEFT);
        leftAxis.setCrosses(AxisCrosses.AUTO_ZERO);



        ChartDataSource<Number> xs = DataSources.fromNumericCellRange(sheet, new CellRangeAddress(tableRow + 1, tableRow + tableHeight - 1, 0, 0));

        for (int i = tableCol + 1 ; i < tableCol + tableWidth; i++) {
            data.addSerie(xs, DataSources.fromNumericCellRange(sheet, new CellRangeAddress(tableRow + 1, tableRow + tableHeight - 1, i, i)));
        }

        chart.plot(data, bottomAxis, leftAxis);

    }
}


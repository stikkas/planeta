package ru.planeta.reports;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Workbook;

public class SimpleExcelReport extends AbstractSimpleExcelReport {

    public SimpleExcelReport() {
        super();
    }

    public SimpleExcelReport(String fileName) {
        super(fileName);
    }

    @Override
    protected Workbook createWorkbook() {
        return new HSSFWorkbook();
    }

    @Override
    protected String getExtension() {
        return ".xls";
    }
}

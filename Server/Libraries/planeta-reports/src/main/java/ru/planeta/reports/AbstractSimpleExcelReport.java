package ru.planeta.reports;

import org.apache.poi.hssf.usermodel.HSSFDataFormat;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.util.CellRangeAddress;

import javax.servlet.http.HttpServletResponse;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Date;

public abstract class AbstractSimpleExcelReport extends AbstractSimpleReport {
    protected Sheet sheet;
    protected Row row;
    protected Workbook workbook;
    protected CellStyle dataCellStyle;
    protected CellStyle intCellStyle;
    protected CellStyle longWithoutThousandsSeparatorCellStyle;
    protected CellStyle floatCellStyle;
    protected CellStyle hyperlinkCellStyle;
    protected int rowNum;
    protected int celNum;
    protected String fullFileName;

    public AbstractSimpleExcelReport() {
        workbook = createWorkbook();

        dataCellStyle = workbook.createCellStyle();
        dataCellStyle.setDataFormat(HSSFDataFormat.getBuiltinFormat("m/d/yy h:mm"));

        intCellStyle = workbook.createCellStyle();
        intCellStyle.setDataFormat(HSSFDataFormat.getBuiltinFormat("#,##0"));

        longWithoutThousandsSeparatorCellStyle = workbook.createCellStyle();
        longWithoutThousandsSeparatorCellStyle.setDataFormat(HSSFDataFormat.getBuiltinFormat("0"));

        floatCellStyle = workbook.createCellStyle();
        floatCellStyle.setDataFormat(HSSFDataFormat.getBuiltinFormat("#,##0.00"));

        hyperlinkCellStyle = workbook.createCellStyle();
        Font hyperlinkFont = workbook.createFont();
        hyperlinkFont.setUnderline(Font.U_SINGLE);
        hyperlinkFont.setColor(IndexedColors.BLUE.getIndex());
        hyperlinkCellStyle.setFont(hyperlinkFont);
    }

    protected abstract Workbook createWorkbook();

    protected abstract String getExtension();

    public AbstractSimpleExcelReport(String fileName) {
        this();
        this.fileName = fileName + getExtension();
        fullFileName = this.fileName;
    }


    public String normalizeSheetName(String name) {
        StringBuffer sb = new StringBuffer(name.length());

        for (int i = 0; i < name.length(); i++) {
            char ch = name.charAt(i);
            if ("/\\?*][:".indexOf(ch) < 0) {
                sb.append(ch);
            }
        }
        String result = sb.toString();
        while (result.startsWith("'")) {
            result = result.substring(1);
        }
        while (result.endsWith("'")) {
            result = result.substring(0, result.length() - 1);
        }
        return result;
    }

    public SimpleReport addSheet(String name) {
        rowNum = 0;
        if (name.length() > 31) {
            name = name.substring(0, 31);
        }
        if (workbook.getSheet(name) != null) {
            name = name.substring(0, 29) + "_";
            for (int i = 1; i < 10; i++) {
                if (workbook.getSheet(name + i) == null) {
                    name = name + i;
                    break;
                }
            }
        }

        sheet = workbook.createSheet(name);
        return this;
    }

    public Sheet getSheet() {
        if (sheet == null) {
            addSheet("Sheet");
        }
        return sheet;
    }

    @Override
    public SimpleReport addRow() {
        row = getSheet().createRow(rowNum++);
        celNum = 0;
        return this;
    }

    @Override
    public SimpleReport addCaptionRow(Object... cells) {
        super.addCaptionRow(cells);
        sheet.createFreezePane(0, 1);
        sheet.setAutoFilter(new CellRangeAddress(0, 0, 0, cells.length - 1));
        return this;
    }

    @Override
    public SimpleReport skipRow() {
        rowNum++;
        return this;
    }

    @Override
    public SimpleReport addCell(String s) {
        row.createCell(celNum++).setCellValue(s);
        return this;
    }

    @Override
    public SimpleReport skipCell() {
        celNum++;
        return this;
    }

    @Override
    public void addToResponse(HttpServletResponse response) throws IOException {
        response.setContentType("application/vnd.ms-excel; charset=UTF-8");
        response.addHeader("Content-Disposition", "attachment; filename=\"" + this.fileName + "\"");
        workbook.write(response.getOutputStream());
    }


    @Override
    public SimpleReport addCell(Date dt) {
        Cell cell = row.createCell(celNum++);
        if (dt != null) {
            cell.setCellValue(dt);
        }
        cell.setCellStyle(dataCellStyle);
        return this;
    }

    @Override
    public SimpleReport addCell(Object obj) {
        if (obj instanceof Date) {
            return addCell((Date) obj);
        }
        if (obj instanceof String) {
            return addCell((String) obj);
        }
        if (obj instanceof Hyperlink) {
            return addCell((Hyperlink) obj);
        }
        Cell cell = row.createCell(celNum++);
        if (obj == null) {
            return this;
        }

        if (obj instanceof Number) {
            CellStyle cellStyle;
            if (obj instanceof Double || obj instanceof Float) {
                cellStyle = floatCellStyle;
            } else if (obj instanceof Long) {
                cellStyle = longWithoutThousandsSeparatorCellStyle;
            } else {
                cellStyle = intCellStyle;
            }
            cell.setCellStyle(cellStyle);
            cell.setCellType(Cell.CELL_TYPE_NUMERIC);
            cell.setCellValue(((Number) obj).doubleValue());
        } else {
            cell.setCellValue(obj.toString());
        }


        return this;
    }

    @Override
    public String save(String dir) {
        try {
            if (!dir.endsWith("/") && !dir.endsWith("\\")) {
                dir = dir + '/';
            }
            fullFileName = dir + fileName;
            FileOutputStream fileOut = new FileOutputStream(fullFileName);
            workbook.write(fileOut);
            fileOut.close();
            return fullFileName;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void saveToStream(OutputStream out) throws IOException {
        workbook.write(out);
    }

    public void openInExcel() {
        try {
            String[] cmdarray = new String[]{"cmd.exe", "/c", fullFileName};
            Runtime.getRuntime().exec(cmdarray);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Override
    public SimpleReport addCell(Hyperlink hyperlink) {

        Cell cell = row.createCell(celNum++);
        if (hyperlink.getText() != null) {
            org.apache.poi.ss.usermodel.Hyperlink hl = workbook.getCreationHelper().createHyperlink(org.apache.poi.common.usermodel.Hyperlink.LINK_URL);

            hl.setLabel(hyperlink.getText());
            hl.setAddress(hyperlink.getUrl());
            cell.setCellValue(hyperlink.getText());
            cell.setHyperlink(hl);
        }
        cell.setCellStyle(hyperlinkCellStyle);
        return this;
    }
}

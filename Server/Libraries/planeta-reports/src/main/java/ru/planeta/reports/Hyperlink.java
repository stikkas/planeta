package ru.planeta.reports;

/**
 * Created by kostiagn on 25.05.2015.
 */
public class Hyperlink {
    private String url;
    private String text;

    public Hyperlink() {
    }

    public Hyperlink(String text, String url) {
        this.url = url;
        this.text = text;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}

package ru.planeta.reports;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Date;

public interface SimpleReport {
    SimpleReport addRow();

    SimpleReport addRow(Object... cells);

    SimpleReport addCaptionRow(Object... cells);

    SimpleReport skipRow();

    SimpleReport skipRow(int countRow);

    SimpleReport addCell(String s);

    SimpleReport addCell(Object o);

    SimpleReport addCell(Date dt);

    SimpleReport addCell(Hyperlink hyperlink);

    SimpleReport addCells(Object... cells);

    SimpleReport skipCell();

    SimpleReport skipCell(int countCell);

    SimpleReport addSheet(String sheetName);

    void addToResponse(HttpServletResponse response) throws IOException;

    /**
     * @return fullFilename to saved report
     */
    String save(String dirName);

    /**
     * @return fullFilename to saved report
     */
    void saveToStream(OutputStream out) throws IOException;

    String toString();

}

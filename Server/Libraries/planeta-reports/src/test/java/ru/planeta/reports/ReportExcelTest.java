package ru.planeta.reports;

import org.junit.Ignore;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.Date;

public class ReportExcelTest {
    @Test
    @Ignore
    public void createChart() throws IOException {
        long ms = (new Date()).getTime() % 1000000;
        String fileName = "report" + ms;
        int chartHeight = 20;
        SimpleExcel2007Report report = new SimpleExcel2007Report(fileName);
        report.addSheet("Chart").skipRow(chartHeight);
        report.addRow("Дата,Новые,В работе,Решенные,Отклоненные,Жалобы,Вопросы,Благодарности".split(","));
        report.addRow(new Date("1/1/2014"), 10, 5, 2, 3, 5, 2, 6);
        report.addRow(new Date("1/2/2014"), 20, 12, 5, 5, 10, 10, 10);
        report.addRow(new Date("1/3/2014"), 30, 20, 15, 9, 20, 20, 12);
        report.addRow(new Date("1/4/2014"), 40, 27, 25, 10, 25, 30, 13);
        report.addRow(new Date("1/5/2014"), 50, 39, 45, 15, 30, 40, 14);
        report.addRow(new Date("1/6/2014"), 60, 45, 45, 20, 35, 50, 15);
        report.addRow(new Date("1/7/2014"), 30, 55, 46, 23, 40, 60, 16);
        report.addRow(new Date("1/8/2014"), 20, 20, 50, 27, 45, 70, 17);
        report.addRow(new Date("1/9/2014"), 10, 15, 70, 33, 50, 70, 18);
        report.addRow(new Date("1/10/2014"), 0, 0, 100, 40, 60, 70, 20);

        report.addChartForTable(0, 0, 15, chartHeight, 0, chartHeight, 7, chartHeight + 10);

        String path = "c:/temp/";
        File dir = new File(path);
        if(!dir.exists() || !dir.isDirectory()) {
            return;
        }
        report.save(path);
        System.out.println(path + fileName);
        report.openInExcel();


    }

    @Test
    @Ignore
    public void test() throws IOException {
        for (ReportType reportType : ReportType.values()) {

            SimpleReport report = reportType.createReport("test_"+reportType.name());
            report.addCaptionRow(",Дата,Текст,Long,Float,BigDecimal".split(","));
            report.addRow();
            report.addCell("ряд 1");
            report.addCell(new Date());
            report.addCell("2");
            report.addCell(1324568L);
            report.addCell(4.5);
            report.addCell(new BigDecimal(135800));

            report.addRow().addCell("ряд 2").skipCell().addCells("2222", 333).addCell(44.44).addCell(new BigDecimal(589652));
            report.skipRow();
            report.addRow("ряд 4", null, "1234", 2345, 3.456);
            report.addRow(new Hyperlink("yandex.ru","http://yandex.ru"), new Hyperlink("Планета","http://planeta.ru"), new Hyperlink("Вася", "http://planeta.ru/vasl"));
            report.save("d:\\temp");
        }


    }
}

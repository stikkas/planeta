<#include "email-template-header.ftl">

<a href="https://biblio.planeta.ru"><img src="https://s3.planeta.ru/i/159c9b/1479383744597_renamed.jpg"></a>

<div style="margin-top: 5px;"><strong>В издательство:</strong> ${bookOrder.book.publishingHouseName}</div>

<div style="margin-top: 5px;"><strong>Дата:</strong> ${date?string("dd MMMMM yyyy")}</div>

<div style="color: #00b8e5; font-size: 25px; margin-top: 15px;">Добрый день!</div>

<div style="margin-top: 15px;">В проекте "Библиородина" (<a href="https://biblio.planeta.ru">biblio.planeta.ru</a>) была оформлена подписка на ваше издание:</div>

<div style="margin-top: 5px;"><strong>Название:</strong> ${bookOrder.book.title}</div>
<div style="margin-top: 5px;"><strong>Количество экземпляров/подписок:</strong> ${bookOrder.count}</div>
<div style="margin-top: 5px;"><strong>Бонус мецената:</strong> ${bookOrder.book.bonus}</div>
<div style="margin-top: 5px;"><strong>ФИО мецената:</strong> ${buyerName}</div>
<div style="margin-top: 5px;"><strong>E-mail мецената:</strong> ${buyerEmail}</div>

<hr>

<div>
    <strong>ВАЖНО!</strong>  Указанные данные направлены вам исключительно для в качестве справочной информации.
    Доставка подписки на ваше издание осуществляется по подписному каталогу ФГУП «Почта России» за счет средств мецената, оформившего подписку в дар библиотеке в рамках федерального проекта «БиблиоРодина». Бонус мецената предоставляется либо партнером проекта, либо самим издательством по собственному усмотрению и направляется меценату по электронной почте!
</div>

<div style="margin-top: 10px; font-style: italic;">
    «БиблиоРодина» представляет собой интернет-портал с функционалом для оформления подписок
    на российские научно-популярные и другие периодические издания в дар российским библиотекам.
    Это новый формат подписки, основанный на принципах краудфандинга или другими словами - коллективного финансирования.
    Запуск проекта инициирован краудфандинговой платформой Planeta.ru при поддержке Государственной Думы РФ.
    Партнерами проекта стали Российская библиотечная ассоциация,
    Русская школьная библиотечная ассоциация (РШБА), Почта России и различные тематические издания.
</div>

<div style="margin-top: 10px;">По всем вопросам вы можете связаться по электронной почте
    <a href="mailto:biblio@planeta.ru">biblio@planeta.ru</a>,
    а также прислать свои отзывы и предложения по работе проекта «БиблиоРодина».
</div>

<div style="margin-top: 10px;">С уважением, <br>
    Служба поддержки проекта «БиблиоРодина» <br>
    Biblio.planeta.ru<br>
    Библиородина.рф <br>
</div>

<#include "email-template-footer.ftl">
<#macro comments_plurals count>
    <#if count % 10 == 1 && count % 100 != 11>
    новый</span> комментарий
    <#elseif count % 100 &lt; 5>
    новых</span> комментария
    <#elseif count % 10 &lt; 5 && count % 100 &gt; 20>
    новых</span> комментария
    <#else>
    новых</span> комментариев
    </#if>
</#macro>

<#macro comments_count commentsCount section>
    <#if commentsCount == 0>
    <td valign="middle" align="center" width="50" style="padding-top: 8px;padding-bottom: 8px;">
        <img src="https://s2.planeta.ru/i/9bae0/1429019169583_renamed.jpg">
    </td>
    <td valign="middle" style="padding-top: 8px;padding-bottom: 8px;">
        <span style="color:#afadaa;font-weight: 700;">нет</span> новых комментариев ${section}
    </td>
    <#else>
    <td valign="middle" align="center" width="50" style="padding-top: 8px;padding-bottom: 8px;">
        <img src="https://s2.planeta.ru/i/9badd/1429019168927_renamed.jpg">
    </td>
    <td valign="middle" style="padding-top: 8px;padding-bottom: 8px;">
            <span style="color:#3498db;font-weight: 700;">${commentsCount} <@comments_plurals commentsCount/> ${section}
    </td>
    </#if>
</#macro>

<#include "email-template-header.ftl">

<#list statsList as stats>
<div style="color:#000;">

    <div style="margin-bottom: 5px;font-size: 15px;line-height: 22px;font-weight:700;letter-spacing:0.21em;text-align:center;">
        СТАТИСТИКА ПРОЕКТА <a href="${stats.campaignUrl}">«${stats.campaign.name}»</a> ПО СБОРАМ
        ЗА ${date?string("dd MMMMM yyyy")}</div>

    <div style="margin-bottom: 6px;font-size: 60px;line-height: 64px;letter-spacing: -.03em;font-weight: 700;">
        Собрано <span style="color:#3498db;">${stats.amount}</span>
        <img src="https://s2.planeta.ru/i/9bae2/1429019170504_renamed.jpg">
    </div>
    <div style="margin-bottom: 6px;font-size: 60px;line-height: 64px;letter-spacing: -.03em;font-weight: 700;">
        Вознаграждений куплено <span style="color:#3498db;">${stats.purchases}</span>
    </div>
    <div style="margin-bottom: 6px;font-size: 60px;line-height: 64px;letter-spacing: -.03em;font-weight: 700;">
        Посетителей <span style="color:#3498db;">${stats.visitors}</span>
    </div>
    <div style="margin-bottom: 6px;font-size: 60px;line-height: 64px;letter-spacing: -.03em;font-weight: 700;">
        Просмотров <span style="color:#3498db;">${stats.views}</span>
    </div>
    <div style="margin-bottom: 6px;font-size: 60px;line-height: 64px;letter-spacing: -.03em;font-weight: 700;">
        <span style="color:#d0cdc5;">Всего</span> <span style="color:#3498db;">${stats.campaign.collectedAmount}</span>
        <img src="https://s2.planeta.ru/i/9bae2/1429019170504_renamed.jpg">
    </div>

    <#if stats.shares?size &gt; 0>
        <hr style="margin-top: 45px;margin-bottom: 30px;border: 0;border-bottom:1px solid #e4e2db;">

        <div style="margin-bottom: 9px;font-weight: 700;letter-spacing:.2em;text-align:center;">КУПЛЕННЫЕ СЕГОДНЯ
            ВОЗНАГРАЖДЕНИЯ
        </div>

        <table width="100%" border="0" cellpadding="0" cellspacing="0"
               style="margin: 0;margin-bottom: 27px;font-size: 13px;line-height: 16px;color:#1a1a1a;">
            <thead style="font-size: 11px;">
            <tr>
                <td style="padding: 10px;">ВОЗНАГРАЖДЕНИЕ</td>
                <td style="width: 65px;padding: 10px;text-align:center;">КУПЛЕНО</td>
                <td style="width: 65px;padding: 10px;text-align:right;">СУММА</td>
            </tr>
            </thead>
            <tbody>
                <#list stats.shares as share>
                <tr>
                    <td style="padding: 10px;background:#f0eee8;">${share.name}</td>
                    <td style="padding: 10px;background:#f0eee8;text-align:center;">${share.purchaseCount}</td>
                    <td style="padding: 10px;background:#f0eee8;text-align:right;">${share.purchaseSum} руб</td>
                </tr>
                </#list>
            </tbody>
        </table>
    </#if>

    <hr style="margin: 0;margin-bottom: 18px;border: 0;border-bottom:1px solid #e4e2db;">


    <table width="100%" border="0" cellpadding="0" cellspacing="0"
           style="margin: 0;margin-bottom: 19px;font-size: 18px;line-height: 22px;">
        <tbody>
        <tr>
            <@comments_count stats.campaignComments 'к проекту'/>
        </tr>
        </tbody>
    </table>


    <table width="100%" border="0" cellpadding="0" cellspacing="0"
           style="margin: 0;margin-bottom: 19px;font-size: 18px;line-height: 22px;">
        <tbody>
        <tr>
            <@comments_count stats.newsComments 'в новостях'/>
        </tr>
        </tbody>
    </table>

    <hr style="margin: 0;margin-bottom: 50px;border: 0;border-bottom:1px solid #e4e2db;">


    <div style="margin-top: 28px;margin-bottom: 28px;text-align:center;">
        <a href="${stats.statisticUrl}" style="font-size: 15px;line-height: 20px;font-weight:
           700;text-decoration:none;color: #ffffff;background: #3498db">
        <span style="text-decoration:none;color: #ffffff;background: #3498db; padding: 16px 34px;">
                Открыть подробную статистику
            </span>
        </a>
    </div>

    <div style="text-align:center;font-size: 15px;font-weight: 700;">или <a style="color:#3498db;" href="${stats.campaignUrl}">перейти в проект</a></div>

</div>
</#list>



<#include "email-template-footer.ftl">

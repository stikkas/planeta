<#compress>
<#macro campaign_affiliate_stats_row_referer stat aggregation showAffiliateStats>
<@compress single_line=true>
<Row>
    <Cell>
        <Data ss:Type="String">
            <#if stat.referer != "">
            ${stat.referer}
            <#else>
            Без источника
            </#if>
        </Data>
    </Cell>
    <Cell>
        <Data ss:Type="Number">
            ${stat.visitors}
        </Data>
    </Cell>
    <Cell>
        <Data ss:Type="Number">
            ${stat.views}
        </Data>
    </Cell>
    <Cell>
        <Data ss:Type="Number">
            ${stat.purchases}
        </Data>
    </Cell>
    <#if showAffiliateStats>
    <Cell>
        <Data ss:Type="Number">
            ${stat.affiliateAmount}
        </Data>
    </Cell>
    <Cell>
        <Data ss:Type="Number">
            ${stat.affiliateAmount}
        </Data>
    </Cell>
    </#if>
</Row>
</@compress>
</#macro>
<#macro campaign_affiliate_stats_row stat aggregation showAffiliateStats>
<@compress single_line=true>
<Row>
    <Cell>
        <Data <#if aggregation == "DATE">ss:Type="Date"<#else>ss:Type="String"</#if>>
        <#if aggregation == "DATE">
            ${stat.date?date}
        <#elseif aggregation == "AFFILIATE">
            <#if stat.affiliateName??>
                ${stat.affiliateName}
            <#else>
                Планета
            </#if>
        <#elseif aggregation == "COUNTRY">
            <#if stat.country != "">${stat.country}<#else>Не определена</#if>
        <#elseif aggregation == "CITY">
            <#if stat.city != "">${stat.city}<#else>Не определен</#if>
        </#if>
        </Data>
    </Cell>
    <Cell>
        <Data ss:Type="Number">
            ${stat.visitors}
        </Data>
    </Cell>
    <Cell>
        <Data ss:Type="Number">
            ${stat.views}
        </Data>
    </Cell>
    <Cell>
        <Data ss:Type="Number">
            ${stat.purchases}
        </Data>
    </Cell>
    <#if aggregation == "DATE">
    <Cell>
        <Data ss:Type="Number">
        ${stat.buyers}
        </Data>
    </Cell>
    <Cell>
        <Data ss:Type="Number">
        ${stat.comments}
        </Data>
    </Cell>
    <Cell>
        <Data ss:Type="Number">
        ${stat.amount}
        </Data>
    </Cell>
    </#if>
    <#if showAffiliateStats>
    <Cell>
        <Data ss:Type="Number">
            ${stat.affiliateAmount}
        </Data>
    </Cell>
    <Cell>
        <Data ss:Type="Number">
            ${stat.affiliateAmount}
        </Data>
    </Cell>
    </#if>
</Row>
</@compress>
</#macro>

<#macro concert_orders_row order orderObject isBarcodeEAN13>
    <Row>
        <Cell>
          <Data ss:Type="String">${StringUtils.escapeHtml(order.buyerName)}</Data>
        </Cell>
        <Cell>
          <Data ss:Type="String">${StringUtils.escapeHtml(order.buyerEmail)}</Data>
        </Cell>
        <Cell>
          <Data ss:Type="String">${StringUtils.escapeHtml(orderObject.objectName)}</Data>
        </Cell>
        <Cell>
          <Data ss:Type="String">${orderObject.price}</Data>
        </Cell>
        <Cell>
          <Data ss:Type="String">${order.timeAdded?datetime}</Data>
        </Cell>
    </Row>
</#macro>

<#macro sales_row key item>
    <Row>
        <Cell>
          <Data ss:Type="String">${key?date}</Data>
        </Cell>
        <Cell>
          <Data ss:Type="String">${item.regCount}</Data>
        </Cell>
        <Cell>
            <Data ss:Type="String">${item.sharesAmount} / ${item.sharesSumOfObjectsCount}</Data>
        </Cell>
        <Cell>
            <Data ss:Type="String">${item.ticketsAmount} / ${item.ticketsSumOfObjectsCount}</Data>
        </Cell>
        <Cell>
            <Data ss:Type="String">${item.productsAmount} / ${item.productsSumOfObjectsCount}</Data>
        </Cell>
        <Cell>
            <Data ss:Type="String">${item.biblioPurchasesAmount} / ${item.biblioPurchasesCount}</Data>
        </Cell>
        <Cell>
            <Data ss:Type="String">${item.sharesAmount + item.productsAmount + item.ticketsAmount + item.biblioPurchasesAmount}</Data>
        </Cell>
    </Row>
</#macro>

<#macro sales_shares_row item>
    <Row>
        <Cell>
            <Data ss:Type="String">${item.groupProfile.displayName}</Data>
        </Cell>
        <Cell>
            <Data ss:Type="String">${item.campaignName}</Data>
        </Cell>
        <Cell>
            <Data ss:Type="String">${item.name}, ${item.price}</Data>
        </Cell>
        <Cell>
            <Data ss:Type="String">${item.count}</Data>
        </Cell>
        <Cell>
            <Data ss:Type="String">${item.amount}</Data>
        </Cell>
    </Row>
</#macro>

<#macro sales_tickets_row item>
    <Row>
        <Cell>
            <Data ss:Type="String">${item.eventProfile.displayName}</Data>
        </Cell>
        <Cell>
            <Data ss:Type="String">${item.name}, ${item.price}</Data>
        </Cell>
        <Cell>
            <Data ss:Type="String">${item.count}</Data>
        </Cell>
        <Cell>
            <Data ss:Type="String">${item.amount}</Data>
        </Cell>
    </Row>
</#macro>

<#macro sales_products_row item>
    <Row>
        <Cell>
            <Data ss:Type="String">${item.groupProfile.displayName}</Data>
        </Cell>
        <Cell>
            <Data ss:Type="String">${item.product.name}, ${item.price}</Data>
        </Cell>
        <Cell>
            <Data ss:Type="String">${item.count}</Data>
        </Cell>
        <Cell>
            <Data ss:Type="String">${item.amount}</Data>
        </Cell>
    </Row>
</#macro>

<#macro registrations_row item>
    <Row>
        <Cell>
            <Data ss:Type="String">${item.timeAdded?date}</Data>
        </Cell>
        <Cell>
            <Data ss:Type="String">${item.profile.name!""}</Data>
        </Cell>
        <Cell>
            <Data ss:Type="String">${item.email!""}</Data>
        </Cell>
        <Cell>
            <Data ss:Type="String">${item.userSource.enterUrl!""}</Data>
        </Cell>
        <Cell>
            <Data ss:Type="String">${item.userSource.referrerUrl!""}</Data>
        </Cell>
    </Row>
</#macro>

<#macro audio_row item>
    <Row>
        <Cell>
          <Data ss:Type="String">${StringUtils.escapeHtml(item.alias)}/audio/${item.album_id}</Data>
        </Cell>
        <Cell>
          <Data ss:Type="String">${StringUtils.escapeHtml(item.track_name)}</Data>
        </Cell>
        <Cell>
          <Data ss:Type="String">${StringUtils.escapeHtml(item.album_name)}</Data>
        </Cell>
        <Cell>
          <Data ss:Type="String">${StringUtils.escapeHtml(item.artist_name)}</Data>
        </Cell>
        <Cell>
          <Data ss:Type="String">${StringUtils.escapeHtml(item.description)}</Data>
        </Cell>
        <Cell>
          <Data ss:Type="String">${StringUtils.escapeHtml(item.group_name)}</Data>
        </Cell>
        <Cell>
          <Data ss:Type="String">${item.genre!""}</Data>
        </Cell>
        <Cell>
          <Data ss:Type="String">${item.group_category_id}</Data>
        </Cell>
        <Cell>
          <Data ss:Type="String">https://planeta.ru/${StringUtils.escapeHtml(item.alias)}/audio/${item.album_id}</Data>
        </Cell>
    </Row>
</#macro>

<#macro video_row item>
    <Row>
        <Cell>
          <Data ss:Type="String">${StringUtils.escapeHtml(item.alias)}/video/${item.video_id}</Data>
        </Cell>
        <Cell>
          <Data ss:Type="String">${StringUtils.escapeHtml(item.video_name)}</Data>
        </Cell>
        <Cell>
          <Data ss:Type="String">${StringUtils.escapeHtml(item.description)}</Data>
        </Cell>
        <Cell>
          <Data ss:Type="String">${StringUtils.escapeHtml(item.group_name)}</Data>
        </Cell>
        <Cell>
          <Data ss:Type="String">${item.genre!""}</Data>
        </Cell>
        <Cell>
          <Data ss:Type="String">${item.group_category_id}</Data>
        </Cell>
        <Cell>
          <Data ss:Type="String">http://tv.planeta.ru/video/${StringUtils.escapeHtml(item.alias)}/${item.video_id}</Data>
        </Cell>
    </Row>
</#macro>

<#macro blog_row item>
    <Row>
        <Cell>
          <Data ss:Type="String">${StringUtils.escapeHtml(item.alias)}/blog/${item.blog_post_id}</Data>
        </Cell>
        <Cell>
          <Data ss:Type="String">${StringUtils.escapeHtml(item.title)}</Data>
        </Cell>
        <Cell>
          <Data ss:Type="String">${item.tags!""}</Data>
        </Cell>
        <Cell>
          <Data ss:Type="String">${StringUtils.escapeHtml(item.group_name)}</Data>
        </Cell>
        <Cell>
          <Data ss:Type="String">${item.genre!""}</Data>
        </Cell>
        <Cell>
          <Data ss:Type="String">${item.group_category_id}</Data>
        </Cell>
        <Cell>
          <Data ss:Type="String">https://planeta.ru/${StringUtils.escapeHtml(item.alias)}/blog/${item.blog_post_id}</Data>
        </Cell>
    </Row>
</#macro>

<#macro photo_row item>
    <Row>
        <Cell>
          <Data ss:Type="String">${StringUtils.escapeHtml(item.alias)}/photo/${item.album_id}</Data>
        </Cell>
        <Cell>
          <Data ss:Type="String">${item.photo_id}</Data>
        </Cell>
        <Cell>
          <Data ss:Type="String">${StringUtils.escapeHtml(item.title)}</Data>
        </Cell>
        <Cell>
          <Data ss:Type="String">${StringUtils.escapeHtml(item.description)}</Data>
        </Cell>
        <Cell>
          <Data ss:Type="String">${StringUtils.escapeHtml(item.group_name)}</Data>
        </Cell>
        <Cell>
          <Data ss:Type="String">${item.genre!""}</Data>
        </Cell>
        <Cell>
          <Data ss:Type="String">${item.group_category_id}</Data>
        </Cell>
        <Cell>
          <Data ss:Type="String">https://planeta.ru/${StringUtils.escapeHtml(item.alias)}/albums/${item.album_id}</Data>
        </Cell>
    </Row>
</#macro>

<#macro group_row item>
    <Row>
        <Cell>
          <Data ss:Type="String">${StringUtils.escapeHtml(item.alias)}</Data>
        </Cell>
        <Cell>
          <Data ss:Type="String">${StringUtils.escapeHtml(item.name)}</Data>
        </Cell>
        <Cell>
          <Data ss:Type="String">${StringUtils.escapeHtml(item.summary!"")}</Data>
        </Cell>
        <Cell>
          <Data ss:Type="String">${StringUtils.escapeHtml(item.artists!"")}</Data>
        </Cell>
        <Cell>
          <Data ss:Type="String">https://planeta.ru/${StringUtils.escapeHtml(item.alias)}</Data>
        </Cell>
    </Row>
</#macro>

<#macro project_row item>
    <Row>
        <Cell>
          <Data ss:Type="String">/campaigns/${item.campaign_id}/${item.share_id}</Data>
        </Cell>
        <Cell>
          <Data ss:Type="String">${StringUtils.escapeHtml(item.project_name)}</Data>
        </Cell>
        <Cell>
          <Data ss:Type="String">${StringUtils.escapeHtml(item.share_name)}</Data>
        </Cell>
        <Cell>
          <Data ss:Type="String">${item.tags!""}</Data>
        </Cell>
        <Cell>
          <Data ss:Type="String">${StringUtils.escapeHtml(item.description)}</Data>
        </Cell>
        <Cell>
          <Data ss:Type="String">${StringUtils.escapeHtml(item.group_name)}</Data>
        </Cell>
        <Cell>
          <Data ss:Type="String">${item.genre!""}</Data>
        </Cell>
        <Cell>
          <Data ss:Type="String">${item.group_category_id}</Data>
        </Cell>
        <Cell>
          <Data ss:Type="String">https://planeta.ru/campaigns/${item.campaign_id}</Data>
        </Cell>
    </Row>
</#macro>

<#macro concert_actual_row item>
<Row>
    <Cell>
        <Data ss:Type="String">${item.event_name}</Data>
    </Cell>
    <Cell>
        <Data ss:Type="String">${item.name}</Data>
    </Cell>
    <Cell>
        <Data ss:Type="String">${item.place}</Data>
    </Cell>
    <Cell>
        <Data ss:Type="String">${item.time_sales_start}</Data>
    </Cell>
    <Cell>
        <Data ss:Type="String">${item.time_sales_finish}</Data>
    </Cell>
    <Cell>
        <Data ss:Type="String">${item.amount?number - item.purchase_count?number}</Data>
    </Cell>
</Row>
</#macro>

<#macro table_header titles>
    <Row>
        <#list titles as title>
            <Cell ss:StyleID="s21">
                <Data ss:Type="String">${StringUtils.escapeHtml(title)}</Data>
            </Cell>
        </#list>
    </Row>
</#macro>

<#macro audio_table items>
    <Table>
        <#assign titles = ["идентификатор", "название контента", "название альбома", "название исполнителя", "описание контента", "название сообщества", "жанр", "категория", "ссылка"] >
        <@table_header titles/>
        <#list items as item>
            <@audio_row item/>
        </#list>
    </Table>
</#macro>

<#macro video_table items>
    <Table>
        <#assign titles = ["идентификатор", "название контента", "описание контента", "назание сообщества", "жанр", "категория", "ссылка"] >
        <@table_header titles/>
        <#list items as item>
            <@video_row item/>
        </#list>
    </Table>
</#macro>

<#macro blog_table items>
    <Table>
        <#assign titles = ["идентификатор", "название", "описание", "назание сообщества", "жанр", "категория", "ссылка"] >
        <@table_header titles/>
        <#list items as item>
            <@blog_row item/>
        </#list>
    </Table>
</#macro>

<#macro photo_table items>
    <Table>
        <#assign titles = ["идентификатор", "название контента", "назание альбома", "описание контента", "назание сообщества", "жанр", "категория", "ссылка"] >
        <@table_header titles/>
        <#list items as item>
            <@photo_row item/>
        </#list>
    </Table>
</#macro>

<#macro group_table items>
    <Table>
        <#assign titles = ["идентификатор", "название", "описание", "артисты", "ссылка"] >
        <@table_header titles/>
        <#list items as item>
            <@group_row item/>
        </#list>
    </Table>
</#macro>

<#macro project_table items>
    <Table>
        <#assign titles = ["идентификатор", "название проекта", "название вознаграждения", "категория проекта", "описание", "название сообщества", "жанр сообщества", "категория сообщества", "ссылка"] >
        <@table_header titles/>
        <#list items as item>
            <@project_row item/>
        </#list>
    </Table>
</#macro>

<#macro sales_table items>
    <Table>
        <#assign titles = ["дата", "регистрации", "вознаграждения", "билеты", "товары", "библиородина", "всего"] >
        <@table_header titles/>
        <#list items?keys as key>
            <@sales_row key=key item=items[key]/>
        </#list>
    </Table>
</#macro>

<#macro sales_shares_table items>
    <Table>
        <#assign titles = ["сообщество", "проект", "вознаграждение, цена", "количество", "сумма"] >
        <@table_header titles/>
        <#list items as item>
            <@sales_shares_row item=item/>
        </#list>
        <Row>
            <Cell>
                <Data ss:Type="String">Итого:</Data>
            </Cell>
            <Cell>
                <Data ss:Type="String"></Data>
            </Cell>
            <Cell>
                <Data ss:Type="String"></Data>
            </Cell>
            <Cell>
                <Data ss:Type="Number">${totalCount}</Data>
            </Cell>
            <Cell>
                <Data ss:Type="Number">${totalAmount}</Data>
            </Cell>
        </Row>
    </Table>
</#macro>

<#macro sales_tickets_table items>
    <Table>
        <#assign titles = ["Событие", "время, место", "билет, цена", "билетов", "сумма"] >
        <@table_header titles/>
        <#list items as item>
            <@sales_tickets_row item=item/>
        </#list>
        <Row>
            <Cell>
                <Data ss:Type="String">Итого:</Data>
            </Cell>
            <Cell>
                <Data ss:Type="String"></Data>
            </Cell>
            <Cell>
                <Data ss:Type="String"></Data>
            </Cell>
            <Cell>
                <Data ss:Type="Number">${totalCount}</Data>
            </Cell>
            <Cell>
                <Data ss:Type="Number">${totalAmount}</Data>
            </Cell>
        </Row>
    </Table>
</#macro>

<#macro sales_products_table items>
    <Table>
        <#assign titles = ["Сообщество", "товар, цена", "количество", "сумма"] >
        <@table_header titles/>
        <#list items as item>
            <@sales_products_row item=item/>
        </#list>
        <Row>
            <Cell>
                <Data ss:Type="String">Итого:</Data>
            </Cell>
            <Cell>
                <Data ss:Type="String"></Data>
            </Cell>
            <Cell>
                <Data ss:Type="String">${totalCount}</Data>
            </Cell>
            <Cell>
                <Data ss:Type="String">${totalAmount}</Data>
            </Cell>
        </Row>
    </Table>
</#macro>

<#macro registrations_table items>
    <Table>
        <#assign titles = ["дата", "имя", "почта", "первое посещение", "источник"] >
        <@table_header titles/>
        <#list items as item>
            <@registrations_row item=item/>
        </#list>
    </Table>
</#macro>

<#macro concert_orders_table items isBarcodeEAN13>
    <Table>
        <#assign titles = ["Имя", "Email", "Номер заказа", "Название билета", "Стоимость", "Дата покупки"] >
        <@table_header titles/>
        <#list items as order>
            <#list order.orderObjectsInfo as orderObject>
                <@concert_orders_row order=order orderObject=orderObject isBarcodeEAN13=isBarcodeEAN13/>
            </#list>
        </#list>
    </Table>
</#macro>

<#macro delivery_status_dic status>
<#compress >
    <#assign ddic={
    "PENDING": "Новый",
    "IN_TRANSIT": "Отправлен",
    "DELIVERED": "Доставлен",
    "REJECTED": "Возвращен",
    "CANCELLED": "Отменен"
    }>
    ${StringUtils.escapeHtml(ddic[status])}
</#compress>
</#macro>

<#macro payment_status_dic status>
<#compress>
    <#assign pdic={
    "PENDING": "Не оплачен",
    "COMPLETED": "Оплачен",
    "CANCELLED": "Отменен"
    }>${StringUtils.escapeHtml(pdic[status])}
</#compress>
</#macro>
<#macro shop_orders_styles>
<Style ss:ID="CenteredDate">
    <NumberFormat ss:Format="Long Date"/>
    <Alignment ss:Vertical="Center" ss:Horizontal="Center"/>
</Style>
</#macro>
<#macro shares_sale_table items>
    <Table>
        <#assign titles = ["Название вознаграждения", "Куплено (шт.)", "Собрано (руб.)"] >
        <@table_header titles/>
        <#list items as i >
            <Row>
                <Cell>
                    <Data ss:Type="String">${i.name}</Data>
                </Cell>
                <Cell>
                    <Data ss:Type="Number">${i.purchaseCount}</Data>
                </Cell>
                <Cell>
                    <Data ss:Type="Number">${i.purchaseSum}</Data>
                </Cell>
            </Row>
        </#list>
    </Table>
</#macro>

<#macro pub_houses_table items>
    <Table>
    <#assign titles = ["Название", "Контрагент"] >
    <@table_header titles/>
    <#list items as i >
            <Row>
                <Cell>
                    <Data ss:Type="String"><#if i.name??>${i.name}</#if></Data>
                </Cell>
                <Cell>
                    <Data ss:Type="String">
                        <#if i.contractorName??>${i.contractorName}</#if>
                    </Data>
                </Cell>
            </Row>
    </#list>
    </Table>
</#macro>


<#macro libraries_table items>
    <Table>
    <#assign titles = ["Название", "Статус", "Индекс", "Регион", "Адрес", "Контактное лицо", "Email",
    "Сайт", "Комментарий", "Долгота", "Широта", "Тип"] >
    <@table_header titles/>
    <#list items as i >
            <Row>
                <Cell>
                    <Data ss:Type="String"><#if i.name??>${i.name}</#if></Data>
                </Cell>
                <Cell>
                    <Data ss:Type="String"><#if i.status??>${i.status}</#if></Data>
                </Cell>
                <Cell>
                    <Data ss:Type="String"><#if i.postIndex??>${i.postIndex}</#if></Data>
                </Cell>
                <Cell>
                    <Data ss:Type="String"><#if i.region??>${i.region}</#if></Data>
                </Cell>
                <Cell>
                    <Data ss:Type="String"><#if i.address??>${i.address}</#if></Data>
                </Cell>
                <Cell>
                    <Data ss:Type="String"><#if i.contractor??>${i.contractor}</#if></Data>
                </Cell>
                <Cell>
                    <Data ss:Type="String"><#if i.email??>${i.email}</#if></Data>
                </Cell>
                <Cell>
                    <Data ss:Type="String"><#if i.site??>${i.site}</#if></Data>
                </Cell>
                <Cell>
                    <Data ss:Type="String"><#if i.comment??>${i.comment}</#if></Data>
                </Cell>
                <Cell>
                    <Data ss:Type="String"><#if i.longitude??>${i.longitude}</#if></Data>
                </Cell>
                <Cell>
                    <Data ss:Type="String"><#if i.latitude??>${i.latitude}</#if></Data>
                </Cell>
                <Cell>
                    <Data ss:Type="String"><#if i.libraryType??>${i.libraryType}</#if></Data>
                </Cell>
            </Row>
    </#list>
    </Table>
</#macro>


<#macro books_table items>
    <Table>
        <#assign titles = ["Название", "Описание", "Обложка", "Статус", "Дата добавления", "Издательство", "Цена",
        "Старая цена", "Дата смены цены", "Периодичность", "Бонус", "Комментарий", "Контактное лицо", "Контактный email", "Сайт"] >
        <@table_header titles/>
        <#list items as i >
            <Row>
                <Cell>
                    <Data ss:Type="String"><#if i.title??>${i.title}</#if></Data>
                </Cell>
                <Cell>
                    <Data ss:Type="String"><#if i.description??>${i.description}</#if></Data>
                </Cell>
                <Cell>
                    <Data ss:Type="String"><#if i.imageUrl??>${i.imageUrl}</#if></Data>
                </Cell>
                <Cell>
                    <Data ss:Type="String"><#if i.status??>${i.status}</#if></Data>
                </Cell>
                <Cell>
                    <Data ss:Type="String"><#if i.timeAdded??>${i.timeAdded?datetime}</#if></Data>
                </Cell>
                <Cell>
                    <Data ss:Type="String"><#if i.publishingHouseName??>${i.publishingHouseName}</#if></Data>
                </Cell>
                <Cell>
                    <Data ss:Type="Number"><#if i.price??>${i.price}</#if></Data>
                </Cell>
                <Cell>
                    <Data ss:Type="Number"><#if i.newPrice??>${i.newPrice}</#if></Data>
                </Cell>
                <Cell>
                    <#if i.changePriceDate??><Data ss:Type="String">${i.changePriceDate?datetime}</Data></#if>
                </Cell>
                <Cell>
                    <Data ss:Type="String"><#if i.periodicity??>${i.periodicity}</#if></Data>
                </Cell>
                <Cell>
                    <Data ss:Type="String"><#if i.bonus??>${i.bonus}</#if></Data>
                </Cell>
                <Cell>
                    <Data ss:Type="String"><#if i.comment??>${i.comment}</#if></Data>
                </Cell>
                <Cell>
                    <Data ss:Type="String"><#if i.contact??>${i.contact}</#if></Data>
                </Cell>
                <Cell>
                    <Data ss:Type="String"><#if i.email??>${i.email}</#if></Data>
                </Cell>
                <Cell>
                    <Data ss:Type="String"><#if i.site??>${i.site}</#if></Data>
                </Cell>
            </Row>
        </#list>
    </Table>
</#macro>


<#macro campaign_affiliate_stats aggregationType items showAffiliateStats>
    <Table>
        <#assign titles = [] />
        <#if aggregationType == "DATE">
            <#assign titles = titles + ["Дата"] />
        </#if>
        <#if aggregationType == "REFERER">
            <#assign titles = titles + ["Сайт-источник"] />
        </#if>
        <#if aggregationType == "AFFILIATE">
            <#assign titles = titles + ["Имя"] />
        </#if>
        <#if aggregationType == "COUNTRY">
            <#assign titles = titles + ["Страна"] />
        </#if>
        <#if aggregationType == "CITY">
            <#assign titles = titles + ["Город"] />
        </#if>
        <#assign titles = titles + ["Уникальные посетители", "Просмотры", "Покупок"] >
        <#if aggregationType == "DATE"><#assign titles = titles + ["Покупателей", "Комментариев", "Собрано средств"] ></#if>
        <#if showAffiliateStats>
            <#assign titles = titles + ["Партнерские отчисления", "Реферальные отчисления"] >
        </#if>
        <@table_header titles/>
        <#list items as stat>
            <#if aggregationType == "REFERER">
                <@campaign_affiliate_stats_row_referer stat=stat aggregation=aggregationType showAffiliateStats=showAffiliateStats/>
            <#else>
                <@campaign_affiliate_stats_row stat=stat aggregation=aggregationType showAffiliateStats=showAffiliateStats/>
            </#if>
        </#list>
    </Table>
</#macro>


<#macro concert_actual_table items>
<Table>
    <#assign titles = ["Концерт", "Билет", "Место", "Начало продаж", "Конец продаж", "Количество билетов"] >
    <@table_header titles/>
    <#list items as item>
        <@concert_actual_row item=item/>
    </#list>
</Table>
</#macro>
</#compress>
<@compress single_line=false><?xml version="1.0" encoding="UTF-8" ?>
<?mso-application progid="Excel.Sheet"?>
<Workbook
        xmlns="urn:schemas-microsoft-com:office:spreadsheet"
        xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet"
>
  <DocumentProperties xmlns="urn:schemas-microsoft-com:office:office">
    <Author></Author>
    <LastAuthor></LastAuthor>
    <Created></Created>
    <Company></Company>
    <Version>1.0</Version>
  </DocumentProperties>
  <ExcelWorkbook xmlns="urn:schemas-microsoft-com:office:excel">
    <WindowHeight>6795</WindowHeight>
    <WindowWidth>8460</WindowWidth>
    <WindowTopX>120</WindowTopX>
    <WindowTopY>15</WindowTopY>
    <ProtectStructure>False</ProtectStructure>
    <ProtectWindows>False</ProtectWindows>
  </ExcelWorkbook>
  <Styles>
    <Style ss:ID="Default" ss:Name="Normal">
      <Alignment ss:Vertical="Bottom" />
      <Borders />
      <Font />
      <Interior />
      <NumberFormat />
      <Protection />
    </Style>
    <Style ss:ID="s21">
      <Font x:Family="Swiss" ss:Bold="1" />
    </Style>
    <Style ss:ID="DateLiteral">
       <NumberFormat ss:Format="Short Date"/>
    </Style>
    <Style ss:ID="Centered">
        <Alignment ss:Vertical="Center" ss:Horizontal="Center"/>
    </Style>
  <#switch reportType>
      <#case "SHOP_ORDERS">
          <@shop_orders_styles/>
          <#break>
  </#switch>
  </Styles>
  <Worksheet ss:Name="Sheet1">
    <#switch reportType>
        <#case "SEO_BY_CONTENT">
            <#switch contentType>
                <#case "AUDIO">
                    <@audio_table items/>
                    <#break>
                <#case "VIDEO">
                    <@video_table items/>
                    <#break>
                <#case "PHOTO">
                    <@photo_table items/>
                    <#break>
                <#case "BLOG">
                    <@blog_table items/>
                    <#break>
                <#case "PROJECT">
                    <@project_table items/>
                    <#break>
                <#case "GROUP">
                    <@group_table items/>
                    <#break>
            </#switch>
            <#break>
        <#case "SALES">
            <@sales_table items/>
            <#break>
        <#case "REGISTRATIONS">
            <@registrations_table items/>
            <#break>
        <#case "SALES_SHARES">
            <@sales_shares_table items/>
            <#break>
        <#case "SALES_TICKETS">
            <@sales_tickets_table items/>
            <#break>
        <#case "SALES_PRODUCTS">
            <@sales_products_table items/>
            <#break>
        <#case "CONCERT_ORDERS">
            <@concert_orders_table items=items isBarcodeEAN13=isBarcodeEAN13/>
            <#break>
        <#case "CAMPAIGN_AFFILIATE_STATS">
            <@campaign_affiliate_stats aggregationType items showAffiliateStats/>
            <#break>
        <#case "CONCERTS_ACTUAL">
            <@concert_actual_table items/>
            <#break>
        <#case "SHOP_ORDERS">
            <@shop_orders_table items/>
            <#break>
        <#case "SHARES_SALE">
            <@shares_sale_table items/>
            <#break>
        <#case "BOOKS">
            <@books_table items/>
            <#break>
        <#case "LIBRARIES">
            <@libraries_table items/>
            <#break>
        <#case "PUB_HOUSES">
            <@pub_houses_table items/>
            <#break>
    </#switch>
    <WorksheetOptions xmlns="urn:schemas-microsoft-com:office:excel">
      <Print>
        <ValidPrinterInfo />
        <HorizontalResolution>600</HorizontalResolution>
        <VerticalResolution>600</VerticalResolution>
      </Print>
      <Selected />
      <Panes>
        <Pane>
          <Number>3</Number>
          <ActiveRow>5</ActiveRow>
          <ActiveCol>1</ActiveCol>
        </Pane>
      </Panes>
      <ProtectObjects>False</ProtectObjects>
      <ProtectScenarios>False</ProtectScenarios>
    </WorksheetOptions>
  </Worksheet>
</Workbook>
</@compress>
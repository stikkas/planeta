<#include "email-template-header.ftl">
<@compress single_line=true>

<h3 style="margin: 0 0 1em;">${dearByGender} ${order.buyerName}!</h3>
<p>Ваш заказ принят!</p>
<p>Номер вашего заказа <a style="color: #3498db;" href="${baseHostUrl}/profile/print-order.html?orderId=${order.orderId}">${order.orderId}</a></p>
Список товаров:
<table>
    <tr>
        <th><i>№</i></th>
        <th><i>Наименование</i></th>
        <th><i>Количество</i></th>
        <th><i>Стоимость</i></th>
    </tr>
    <#assign pvals = orderObjects?values>
    <#list pvals as products>
        <tr>
            <td>${products_index + 1}</td>
            <td>${products?first.objectName}</td>
            <td>${products?size}</td>
            <td>${products?first['price']}</td>
        </tr>
    </#list>
</table>
<ul>
    <li>Общая cтоимость заказа: <b>${order.totalPrice}</b> руб.;</li>
    <#if order.deliveryPrice &gt; 0 >
        <li>Стоимость доставки: <b>${order.deliveryPrice}</b> руб.;</li>
    </#if>
    <li>Выбранный способ оплаты: <b>
        <#switch order.paymentType>
            <#case "NOT_SET">с баланса планеты
                <#break>
            <#case "CASH">наличными
                <#break>
            <#case "PAYMENT_SYSTEM">через платежные системы
                <#break>
        </#switch>
    </b>;</li>
    <li>Статус: <b>
        <#switch order.paymentType>
            <#case "NOT_SET">оплачен
                <#break>
            <#case "CASH">ожидает оплаты
                <#break>
            <#case "PAYMENT_SYSTEM">оплачен
                <#break>
        </#switch>
    </b>;</li>
    <#if order.deliveryType != "NOT_SET">
        <li>Выбранный способ доставки: <b>${order.linkedDelivery.name}</b>;</li>
        <#if order.deliveryType == "DELIVERY" >
            <#assign address = order.linkedDelivery.address />
            <li>Адрес доставки: <#if address.zipCode??>${address.zipCode}, </#if>${address.city}, ${address.street}</li>
        </#if>
    </#if>
</ul>
<#list pvals as products>
    <#if products?first.textForMail?? >
        <br>
        ${products?first.textForMail}
    </#if>
</#list>
<p>
    <#if isDigital>
        Вы можете скачать купленные вами цифровые товары в разделе <a style="color: #3498db;" href="${baseHostUrl}/account">«Мои заказы»</a>.
    <#else>
        Вы можете ознакомиться со списком ваших покупок на Планете в разделе <a style="color: #3498db;" href="${baseHostUrl}/account">«Мои заказы»</a>.
    </#if>

</p>
<br/>
<p>Напоминаем, что наш магазин работает только в будние дни, суббота, воскресенье – выходные дни. Пункт самовывоза работает строго с 11:00 до 20:00 в будние дни и с 11:00 до 18:00 по субботам.</p>
<p>В случае необходимости, с Вами свяжется наш менеджер, чтобы обсудить детали заказа.</p>
<br/>
Если у Вас возникли вопросы по поводу оформления заказа и покупки товаров, Вы можете связаться с нами по телефону +7 (495) 181-06-06 или написать на наш
<a style="color: #3498db;" href="mailto:shop@planeta.ru">e-mail</a>.
<br/>
<br/>
С наилучшими пожеланиями, <a style="color: #3498db;" href="${baseHostUrl}">Planeta.ru</a>

</@compress>
<#include "email-template-footer.ftl">

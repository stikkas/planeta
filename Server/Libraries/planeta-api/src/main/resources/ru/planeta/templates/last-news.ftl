<@compress single_line=true>
        <table width="801" border="0" cellpadding="0" cellspacing="0" style="font-family:Tahoma, Geneva, sans-serif; font-size:12px; line-height:18px; color:#303030;">
            <tr>
                <td style="vertical-align:bottom;" width="232" height="73" colspan="3">
                    <a href="https://planeta.ru/"><img src="https://${staticUrl}/images/mailer/newsletter_1.png" width="232" height="73" border="0" alt="Planeta.ru" /></a></td>
                <td style="vertical-align:top;" width="568">
                    <p style="margin-top:34px; margin-bottom:0; margin-right:40px; font-family:Georgia, 'Times New Roman', Times, serif; font-style:italic; line-height:20px; font-size:16px; text-align:right; color:#808080;">Привет с Планеты!</p>
                </td>
                <td style="vertical-align:bottom;" width="1" height="73">
                    <img src="https://${staticUrl}/images/mailer/spacer.gif" width="1" height="73" alt="" /></td>
            </tr>
            <tr>
                <td style="vertical-align:bottom;" width="800" height="25" colspan="4">
                    <img src="https://${staticUrl}/images/mailer/newsletter_3.png" width="800" height="25" alt="" /></td>
                <td style="vertical-align:bottom;" width="1" height="25">
                    <img src="https://${staticUrl}/images/mailer/spacer.gif" width="1" height="25" alt="" /></td>
            </tr>
            <tr>
                <td style="vertical-align:bottom;" width="40">
                    <img src="https://${staticUrl}/images/mailer/spacer.gif" width="40" alt="" /></td>
                <td style="vertical-align:middle;" width="766" colspan="4">
                    <div style="background: #232c2e; color: white; padding: 25px 10px; margin: 0 0 20px 0; font-family: Arial, sans-serif;">
                        <p style="margin-top:0; margin-bottom:0px; line-height:28px; font-size:28px; text-align: center; font-weight: 700; letter-spacing: -1px; color: white;">Привет с Планеты, ${StringUtils.unescapeHtml(recipient.displayName)}!</p>
                        <p style="text-align: center; color: #ccc; font-size: 15px; margin-top: 5px; margin-bottom: 20px;">Вы давно не заглядывали на свою страницу, а тем временем там появилось что-то новенькое!</p>
                        <hr style="height: 0px; border-bottom: none; border-left: none; border-right: none; border-top: 1px solid #454545;" />
                        <div style="text-align: center;">
                            <table style="margin: 30px auto 0; font-family: Georgia, 'Times New Roman', serif; font-style: italic;">
                                <tr>
                                    <#if totalNewPostCount &gt;= 1>
                                        <td style="padding: 0 20px;">
                                            <div style="font-family: Arial, sans-serif; font-size: 24px; font-weight: 700; font-style: normal; margin-bottom: 5px">${totalNewPostCount}</div>
                                            ${totalNewPostCountMessage}
                                        </td>
                                    </#if>

                                    <#if totalNewWallPostCount &gt;= 1>
                                        <td style="padding: 0 20px;"><div style="font-family: Arial, sans-serif; font-size: 24px; font-weight: 700; font-style: normal; margin-bottom: 5px">
                                            ${totalNewWallPostCount}</div>${totalNewWallPostCountMessage} к постам
                                        </td>
                                    </#if>

                                    <#if totalFriendsUpdatesCount &gt;= 1>
                                        <td style="padding: 0 20px;"><div style="font-family: Arial, sans-serif; font-size: 24px; font-weight: 700; font-style: normal; margin-bottom: 5px">
                                        ${totalFriendsUpdatesCount}</div>${totalFriendsUpdatesCountMessage} у друзей
                                        </td>
                                    </#if>
                                </tr>
                            </table>
                        </div>
                    </div>
                </td>
                <td style="vertical-align:bottom;" width="1">
                    <img src="https://${staticUrl}/images/mailer/spacer.gif" width="1" alt="" /></td>
            </tr>
            <#if groupNotificationInfoList?size &gt; 0>
                <tr>
                    <td style="vertical-align:bottom;" width="800" colspan="4">
                        <div style="font-family:Arial,sans-serif;margin-left:40px; margin-right:40px;">

                            <p style="margin:0 10px 10px;text-transform:uppercase;"><b>НОВОЕ В БЛОГАХ СООБЩЕСТВ</b></p>

                            <table  border="0" cellpadding="0" cellspacing="0" style="width:100%;margin:0 0 40px;border-top:1px solid #d9d9d9;">
                                <tbody>
                                    <#list groupNotificationInfoList as groupNotificationInfo>
                                        <#if groupNotificationInfo.lastPostId &gt; 0>
                                            <tr>
                                                <td colspan="5" height="20"></td>
                                            </tr>
                                            <tr>
                                                <td width="10"></td>
                                                <td valign="middle" width="50">
                                                    <a href="${StringUtils.unescapeHtml(baseHostUrl)}${groupNotificationInfo.authorProfileId}"><img src="${StringUtils.unescapeHtml(groupNotificationInfo.lastPostAuthorImageUrl)}" width="50" height="50" border="0"></a>
                                                </td>
                                                <td width="10"></td>
                                                <td valign="middle">
                                                    <div style="font-size:15px;line-height:18px;">
                                                        <a href="${StringUtils.unescapeHtml(baseHostUrl)}${StringUtils.unescapeHtml(groupNotificationInfo.profileId)}/blog/${StringUtils.unescapeHtml(groupNotificationInfo.lastPostId)}" style="color:#26aedd;text-decoration:none;">
                                                            <b>${StringUtils.unescapeHtml(groupNotificationInfo.lastPostTitle)}</b>
                                                        </a>
                                                        <span style="color:#737373;">
                                                            <#if groupNotificationInfo.blogPostCount &gt; 1>
                                                                и еще <a href="${StringUtils.unescapeHtml(baseHostUrl)}${StringUtils.unescapeHtml(groupNotificationInfo.profileId)}/blog" style="color:#737373;">${groupNotificationInfo.blogPostCount-1} ${groupNotificationInfo.blogPostCountMessage}</a>
                                                            </#if>
                                                            в сообществе
                                                        </span>
                                                        <a href="${StringUtils.unescapeHtml(baseHostUrl)}${StringUtils.unescapeHtml(groupNotificationInfo.profileId)}" style="color:#303030;text-decoration:none;">
                                                            <b>${StringUtils.unescapeHtml(groupNotificationInfo.displayName)}</b>
                                                        </a>
                                                    </div>
                                                </td>
                                                <td width="10"></td>
                                            </tr>
                                        </#if>
                                    </#list>
                                </tbody>
                            </table>

                            <#if newFriendsOfFriendsNotificationList?size &gt; 0>
                                <p style="margin:0 10px 10px;text-transform:uppercase;"><b>НОВОЕ У ДРУЗЕЙ</b></p>

                                <table border="0" cellpadding="0" cellspacing="0" style="width:100%;margin:0 0 20px;border-top:1px solid #d9d9d9;">
                                    <tbody>

                                            <#list newFriendsOfFriendsNotificationList as friendAndHisNewFriends>

                                                <#assign friendProfile = friendAndHisNewFriends.friendProfile>
                                                <#assign newFriendsOfFriend = friendAndHisNewFriends.newFriendsOfFriend>
                                                <#assign newFriendsCount = friendAndHisNewFriends.newFriendsCount>
                                                <#assign newFriendsCountMessage = friendAndHisNewFriends.newFriendsCountMessage>

                                                <tr>
                                                    <td colspan="5" height="15"></td>
                                                </tr>

                                                <tr>
                                                    <td width="10"></td>
                                                    <td valign="top" width="50">
                                                        <a href="${StringUtils.unescapeHtml(baseHostUrl)}${StringUtils.unescapeHtml(friendProfile.profileId)}"><img src="${ImageHelperFunctions.getUserAvatarUrlSmallNonStatic(friendProfile)}" width="50" height="50" border="0"></a>
                                                    </td>
                                                    <td width="10"></td>
                                                    <td valign="top">
                                                        <div style="font-size:15px;margin:0 0 10px;"><b><a href="${StringUtils.unescapeHtml(baseHostUrl)}${StringUtils.unescapeHtml(friendProfile.profileId)}" style="color:#303030;text-decoration:none;">${friendProfile.displayName}</a></b> <span style="color:#838383;"><@gender_verb friendProfile "добавил"/> ${newFriendsCount} ${newFriendsCountMessage}</span></div>

                                                        <table border="0" cellpadding="0" cellspacing="0">
                                                            <tbody>
                                                            <tr>
                                                                <#list newFriendsOfFriend as friendOfFriends>
                                                                    <td valign="top" align="center" width="50">
                                                                        <div><a href="${StringUtils.unescapeHtml(baseHostUrl)}${StringUtils.unescapeHtml(friendOfFriends.subjectProfileId)}"><img src="${ImageHelperFunctions.getUserAvatarUrlSmallNonStatic(friendOfFriends)}" width="50" height="50" border="0"></a></div>
                                                                        <div style="width:50px;overflow:hidden;font-size:11px;line-height:13px;"><a href="${StringUtils.unescapeHtml(baseHostUrl)}${StringUtils.unescapeHtml(friendOfFriends.subjectProfileId)}" style="color:#26aedd;text-decoration:none;"><b><nobr>${StringUtils.unescapeHtml(friendOfFriends.displayName)}</nobr></b></a></div>
                                                                    </td>
                                                                    <td width="10"></td>
                                                                </#list>
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                    <td width="10"></td>
                                                </tr>
                                            </#list>
                                    </tbody>
                                </table>
                            </#if>

                            <#if photoNotificationList?size &gt; 0 || videoNotificationList?size &gt; 0>
                                <p style="margin:0 10px 10px;text-transform:uppercase;"><b>НОВЫЕ ФОТО И ВИДЕО</b></p>

                                <table border="0" cellpadding="0" cellspacing="0" style="margin:0 0 20px;border-top:1px solid #d9d9d9;">
                                    <tbody>
                                    <tr>
                                        <td colspan="3" height="15"></td>
                                    </tr>
                                    <tr>
                                        <td width="10"></td>
                                        <td>
                                            <#list photoNotificationList as photo>
                                                <a href="${StringUtils.unescapeHtml(baseHostUrl)}photo/${photo.profileId}/${photo.voteObjectId}"><img src="${ImageHelperFunctions.getPhotoPreviewNonStatic(photo.imageUrl)}" style="margin:0 10px 10px 0;vertical-align:top;" width="125"></a>
                                            </#list>
                                            <#list videoNotificationList as video>
                                                <a href="${StringUtils.unescapeHtml(baseHostUrl)}video/${video.profileId}/${video.voteObjectId}" title="${StringUtils.unescapeHtml(video.name)}"><img src="${video.imageUrl}" style="margin:0 10px 10px 0;vertical-align:top;" width="125"></a>
                                            </#list>
                                        </td>
                                        <td width="10"></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </#if>
                        </div>
                    </td>
                    <td style="vertical-align:bottom;" width="1"></td>
                </tr>
            </#if>
            <tr>
                <td style="vertical-align:bottom;" width="800" height="25" colspan="4">
                    <img src="https://${staticUrl}/images/mailer/newsletter_8.png" width="800" height="25" alt="" /></td>
                <td style="vertical-align:bottom;" width="1" height="25">
                    <img src="https://${staticUrl}/images/mailer/spacer.gif" width="1" height="25" alt="" /></td>
            </tr>
            <tr>
                <td style="vertical-align:bottom;" width="800" colspan="4">
                    <div style="margin-left:40px; margin-right:40px; font-size:11px; line-height:16px; color:#808080">
                        Вы получили данное письмо, потому что в настройках Вашего профиля есть отметка об отправке
                        уведомлений этого типа на электронную почту. Чтобы настроить способы уведомлений, нажмите
                        <a href="${StringUtils.unescapeHtml(baseHostUrl)}settings/notifications" style="color:#399cbf;"><font color="#399cbf">здесь</font></a>.
                    </div>
                </td>
                <td style="vertical-align:bottom;" width="1">
                    <img src="https://${staticUrl}/images/mailer/spacer.gif" width="1" height="40" alt="" /></td>
            </tr>
            <tr>
                <td style="vertical-align:bottom;" width="34" height="1">
                    <img src="https://${staticUrl}/images/mailer/spacer.gif" width="34" height="1" alt="" /></td>
                <td style="vertical-align:bottom;" width="61" height="1">
                    <img src="https://${staticUrl}/images/mailer/spacer.gif" width="61" height="1" alt="" /></td>
                <td style="vertical-align:bottom;" width="137" height="1">
                    <img src="https://${staticUrl}/images/mailer/spacer.gif" width="137" height="1" alt="" /></td>
                <td style="vertical-align:bottom;" width="568" height="1">
                    <img src="https://${staticUrl}/images/mailer/spacer.gif" width="568" height="1" alt="" /></td>
                <td style="vertical-align:bottom;" width="1" height="1"></td>
            </tr>
        </table>
</@compress>

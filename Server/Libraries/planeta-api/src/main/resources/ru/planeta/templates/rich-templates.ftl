<@compress single_line=true>
    <#switch templateName>
        <#case 'PREMAILER_TRANSFORM'>
            <#include "premailer-no-inline-style.ftl">
            <#break>
        <#case 'NEW_BLOG_POST_IN_CAMPAIGN'>
            <#include "new-blog-post-in-campaign.ftl">
            <#break>
        <#default>
    </#switch>
</@compress>

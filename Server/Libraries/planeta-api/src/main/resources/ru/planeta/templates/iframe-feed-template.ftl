<#macro gender_verb verb>
${verb}<#if gender?? && gender == "FEMALE">а</#if>
</#macro>

<#macro gender_verb_forms male_verb female_verb>
<#if gender?? && gender == "FEMALE">${female_verb}<#else>${male_verb}</#if>
</#macro>

<@compress single_line=true>
<li class="vk-fa-item">
    <div class="vk-fa-ava"><a href="${externalLink}" target="_blank" class="profile-link" profile-link-data="${profileId}"><img src="${profileImageUrl}"></a></div>
    <div class="vk-fa-cont">
        <div class="vk-fa-name">
            <#switch iframeFeedType>
                <#case "PURCHASE_SHARE">
                <#case "LIKE_CAMPAIGN">
                <#case "SUBSCRIBE_CAMPAIGN">
                <#case "INVITE_TO_PARTICIPATE_IN_CAMPAIGN">
                <#case "INSTALL_APPLICATION">
                <#case "INVITE_TO_INSTALL_APPLICATION">
                <#case "ADDED_COMMENT_TO_CAMPAIGN">
                    <a href="${externalLink}" target="_blank" class="profile-link" profile-link-data="${profileId}">${profileDisplayName}</a>
                    <#break >
            </#switch>
            <#switch iframeFeedType>
                <#case "PURCHASE_SHARE">
                    <@gender_verb verb="купил"/> вознаграждение <strong>&quot;${shareName}&quot;</strong> проекта &quot;<a href="/vkapp/campaigns/${campaignId}">${campaignName}</a>&quot;.
                    <#break >
                <#case "START_CAMPAIGN">
                    Проект &quot;<a href="/vkapp/campaigns/${campaignId}">${campaignName}</a>&quot;
                    запущен в сообществе &quot;<a href="${externalLink}" target="_blank" class="profile-link" profile-link-data="${profileId}">${profileDisplayName}</a>&quot;
                    <#break >
                <#case "LIKE_CAMPAIGN">
                    <@gender_verb verb="оценил"/> проект &quot;<a href="/vkapp/campaigns/${campaignId}">${campaignName}</a>&quot;.
                    <#break >
                <#case "SUBSCRIBE_CAMPAIGN">
                    <@gender_verb_forms male_verb="подписался" female_verb="подписалась"/> на проект &quot;<a href="/vkapp/campaigns/${campaignId}">${campaignName}</a>&quot;.
                    <#break >
                <#case "INVITE_TO_PARTICIPATE_IN_CAMPAIGN">
                    <@gender_verb verb="пригласил"/> меня участвовать в проекте &quot;<a href="/vkapp/campaigns/${campaignId}">${campaignName}</a>&quot;.
                    <#break >
                <#case "INSTALL_APPLICATION">
                    <@gender_verb verb="установил"/> приложение.
                    <#break >
                <#case "INVITE_TO_INSTALL_APPLICATION">
                    <@gender_verb verb="пригласил"/> меня установить приложение.
                    <#break >
                <#case "FINISH_CAMPAIGN">
                    Проект &quot;<a href="/vkapp/campaigns/${campaignId}">${campaignName}</a>&quot; сообщества
                    &quot;<a href="${externalLink}" target="_blank" class="profile-link" profile-link-data="${profileId}">${profileDisplayName}</a>&quot;
                    <#if campaign.targetStatus == "SUCCESS">
                        окончен успешно.
                    <#elseif campaign.targetStatus == "FAIL">
                        окончен. Финансовая цель не достигнута.
                    <#elseif campaign.targetStatus == "NONE">
                        окончен.
                    </#if>
                    <#break >
                <#case "ADDED_SHARE_TO_CAMPAIGN">
                    В проект &quot;<a href="/vkapp/campaigns/${campaignId}">${campaignName}</a>&quot; сообщества
                    &quot;<a href="${externalLink}" target="_blank" class="profile-link" profile-link-data="${profileId}">${profileDisplayName}</a>&quot;
                    добавлено новое вознаграждение <strong>&quot;${shareName}&quot;</strong>.
                    <#break >
                <#case "ADDED_BLOG_POST_TO_CAMPAIGN">
                    В проект &quot;<a href="/vkapp/campaigns/${campaignId}">${campaignName}</a>&quot; добавлен новый пост &quot;<strong>${blogPostTitle}</strong>&quot;.
                    <#break >
                <#case "CAMPAIGN_REACHED_AMOUNT">
                    <#if targetAmountPercent == 100>
                        Проект &quot;<a href="/vkapp/campaigns/${campaignId}">${campaignName}</a>&quot; собрал необходимую сумму.
                    <#else>
                        Проект &quot;<a href="/vkapp/campaigns/${campaignId}">${campaignName}</a>&quot;
                        собрал ${targetAmountPercent}% от необходимой суммы.
                    </#if>
                    <#break >
                <#case "ADDED_COMMENT_TO_CAMPAIGN">
                    <@gender_verb verb="добавил"/> комментарий к проекту &quot;<a href="/vkapp/campaigns/${campaignId}">${campaignName}</a>&quot;.
                    <#break >
                <#case "PAUSE_CAMPAIGN">
                    Проект &quot;<a href="/vkapp/campaigns/${campaignId}">${campaignName}</a>&quot; сообщества
                    &quot;<a href="${externalLink}" target="_blank" class="profile-link" profile-link-data="${profileId}">${profileDisplayName}</a>&quot; приостановлен.
                    <#break >
                <#case "RESUME_CAMPAIGN">
                    Проект &quot;<a href="/vkapp/campaigns/${campaignId}">${campaignName}</a>&quot; сообщества
                    &quot;<a href="${externalLink}" target="_blank" class="profile-link" profile-link-data="${profileId}">${profileDisplayName}</a>&quot; возобновлен.
                    <#break >
            </#switch>
        </div>
        <div class="vk-fa-date">${timeAdded}</div>
    </div>
</li>
</@compress>


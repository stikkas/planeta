<@compress single_line=true>
    <#switch emailNode>
        <#case "SUBJECT">
        Ваш заказ №${order.orderId} отправлен
            <#break>
        <#case "BODY">
            <#include "email-template-header.ftl">
        <h3 style="margin: 0 0 1em;">Здравствуйте, ${StringUtils.unescapeHtml(userName)}!</h3>
        Заказ №<a href="${appHost}/account">${order.orderId}</a> передан в почтовое отделение.
        <#if address.id?? && address.id!=0>
            <br/>Заказ будет доставлен по адресу:
            <#if address.zipCode?? && address.zipCode!="">${address.zipCode}, </#if>
            <#if address.country?? && address.country!="">${address.country}, </#if>
            <#if address.city?? && address.city!="">${address.city}, </#if>
            <#if address.address?? && address.address!="">${address.address} </#if>
            <br/>
        </#if>
            <#if order.trackingCode??>
            <br>
            Почтовый идентификатор вашей посылки: ${order.trackingCode?xhtml}. Вы можете узнать её текущее местоположение, введя этот номер на сайте
            <a style="color: #3498db;" href="https://www.pochta.ru/tracking">"Почты России"</a><br/>
            </#if>
            <#if (order.cashOnDeliveryCost > 0)>
            <br>
            Наложеный платёж: ${order.cashOnDeliveryCost?string.number}&nbsp;руб.<br/>
            </#if>
            <#if order.deliveryComment?? && order.deliveryComment?trim != "">
            <br>
            Комментарий: ${StringUtils.unescapeHtml(order.deliveryComment?xhtml)} <br>
            </#if>
            <br/>
            С наилучшими пожеланиями, <a href="${appHost}">Planeta.ru</a>
             <#include "email-template-footer.ftl">
            <#break>
    </#switch>
</@compress>


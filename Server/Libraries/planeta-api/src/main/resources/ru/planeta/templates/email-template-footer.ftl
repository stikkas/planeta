<@compress single_line=true>

                                                            </td>
                                                        </tr>
                                                        <tr style="line-height: inherit;">
                                                            <td style="line-height: inherit; vertical-align: top; height: 40px; padding: 0;" valign="top"><img src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" height="40" width="1" style="display:block"></td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td style="line-height: inherit; vertical-align: top; width: 40px; padding: 0;" valign="top"></td>
                                            </tr>
                                        </table>


                                    </td>
                                </tr>
                            </table>


                            <table style="line-height: 13px; border-spacing: 0; border-collapse: collapse; width: 100%; font-size: 11px; color: #8e8e8e; text-align: center;">
                                <tr style="line-height: inherit;">
                                    <td style="line-height: inherit; vertical-align: top; height: 30px; padding: 0;" valign="top"><img src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" height="30" width="1" style="display:block"></td>
                                </tr>
                                <tr style="line-height: inherit;">
                                    <td style="line-height: inherit; vertical-align: top; padding: 0;" valign="top">
                                        <span style="line-height: inherit; color: #000;">&copy; 2018 Planeta.ru</span>
                                        <span style="line-height: inherit; padding-left: 5px; padding-right: 5px; color: #c0c0c0;">&bull;</span>
                                        <a href="mailto:support@planeta.ru" style="line-height: inherit; color: #3498db; text-decoration: underline;">support@planeta.ru</a>
                                    </td>
                                </tr>



                                <tr style="line-height: inherit;">
                                    <td style="line-height: inherit; vertical-align: top; height: 10px; padding: 0;" valign="top"><img src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" height="10" width="1" style="display:block"></td>
                                </tr>
                                <tr style="line-height: inherit;">
                                    <td style="line-height: inherit; vertical-align: top; padding: 0;" valign="top">

                                        <div style="line-height: 16px; font-size: 11px; color: #808080;">
                                            Это письмо было отправлено на почтовый ящик
                                            <a style="font-weight: 700;line-height: inherit;color:#808080;" href="mailto:${mailTo}">${mailTo}</a>,
                                            <br style="line-height: inherit;" /> потому что он был использован при регистрации на сайте <a href="${baseHostUrl}" style="line-height: inherit; color: #808080; text-decoration: underline;">planeta.ru</a>.
                                            <br style="line-height: inherit;" /> Вы можете изменить свои настройки перейдя по <a href="${baseHostUrl}/settings" style="line-height: inherit; color: #808080; text-decoration: underline;">ссылке</a>.
                                        </div>

                                    </td>
                                </tr>



                                <tr style="line-height: inherit;">
                                    <td style="line-height: inherit; vertical-align: top; height: 30px; padding: 0;" valign="top"><img src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" height="30" width="1" style="display:block"></td>
                                </tr>
                            </table>



                        </td>
                    </tr>
                </table>
            </center>
        </td>
    </tr>
</table>
</@compress>
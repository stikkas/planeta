<body style="-webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; width: 100% !important; min-width: 100%; color: #515054; font-family: 'Arial', sans-serif; font-weight: normal; text-align: left; line-height: 1.3; margin: 0; padding: 0;">


<table style="border-spacing: 0; border-collapse: collapse; height: 100%; width: 100%; -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; color: #9d9d9d; font-family: 'Open Sans', -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif; font-weight: 300; text-align: left; line-height: 17px; font-size: 12px; background: #161f26; margin: 0; padding: 0;"
       bgcolor="#161f26">
    <tbody>
    <tr>
        <td style="vertical-align: top; font-size: 0; line-height: 0; padding: 25px 0 0;" valign="top">&nbsp;</td>
    </tr>
    <tr>
        <td style="vertical-align: top; padding: 0;" valign="top">

            <center style="width: 100%; min-width: 480px;">
                <table style="border-spacing: 0; border-collapse: collapse; width: 480px; text-align: left; margin: auto;">

                    <tbody>
                    <tr>
                        <td style="vertical-align: top; text-align: center; padding: 0 0 25px;" align="center"
                            valign="top">
                            <a href="https://school.planeta.ru/" utm-content="headerlogo" style="color: #01c8fe; text-decoration: underline;">
                                <img src="http://files.planeta.ru/mailer/interactive/i/logo.png" width="136" height="31"
                                     style="outline: none; text-decoration: none; -ms-interpolation-mode: bicubic; display: inline; vertical-align: middle; border: none;">
                            </a>
                        </td>
                    </tr>


                    <tr>
                        <td style="vertical-align: top; padding: 0;" valign="top">
                            <img src="http://files.planeta.ru/mailer/interactive/i/main-img.jpg" width="480"
                                 height="300"
                                 style="outline: none; text-decoration: none; -ms-interpolation-mode: bicubic; display: block; vertical-align: middle;">
                        </td>
                    </tr>


                    <tr>
                        <td style="vertical-align: top; padding: 0;" valign="top">

                            <table style="border-spacing: 0; border-collapse: collapse; width: 100%;">
                                <tbody>
                                <tr>
                                    <td style="vertical-align: top; font-size: 24px; line-height: 33px; color: #000; text-align: center; background: #fff; padding: 29px 48px 4px;"
                                        align="center" valign="top" bgcolor="#fff">

                                        ${userName},
                                        <br> поздравляем с&nbsp;окончанием
                                        <nobr>онлайн-курса</nobr>
                                        «Школы краудфандинга»!

                                    </td>
                                </tr>
                                <tr>
                                    <td style="vertical-align: top; font-size: 16px; line-height: 24px; color: #454553; background: #fff; padding: 10px 48px 32px;"
                                        valign="top" bgcolor="#fff">

                                        <p style="font-size: inherit; line-height: inherit; color: inherit; margin: 1.5em 0;">
                                        Вы&nbsp;уже отправили свой проект на&nbsp;модерацию? Или решили взять паузу
                                        и&nbsp;ещё раз всё как следует обдумать? Так или иначе, вы&nbsp;молодец, и&nbsp;вот документальное тому подтверждение:</p>
                                        <table style="border-spacing: 0; border-collapse: collapse; width: 100%;">
                                            <tbody><tr>
                                                <td style="vertical-align: top; padding: 8px 0 14px;" valign="top">
                                                    <a href="${certUrl}" target="_blank" style="color: #1a8cff; text-decoration: underline;"><img src="${certUrl}" width="384" style="outline: none; text-decoration: none; -ms-interpolation-mode: bicubic; display: block; vertical-align: middle; margin: auto; border: none;"></a>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                        <table style="border-spacing: 0; border-collapse: collapse; width: 100%;">
                                            <tbody><tr>
                                                <td style="vertical-align: top; font-style: italic; font-size: 12px; line-height: 14px; text-align: center; padding: 0;" align="center" valign="top">
                                                    <a href="${certUrl}"target="_blank" style="color: #1a8cff; text-decoration: underline;">Скачать сертификат</a>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>


                                        <table style="border-spacing: 0; border-collapse: collapse; width: 100%;">
                                            <tbody><tr>
                                                <td style="vertical-align: top; font-size: 14px; line-height: 24px; text-align: center; padding: 36px 0 10px;" align="center" valign="top">
                                                    Поделиться
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center" style="vertical-align: top; padding: 0 0 16px;" valign="top">
                                                    <table style="border-spacing: 0; border-collapse: collapse; margin: auto;">
                                                        <tbody><tr>
                                                            <td style="vertical-align: top; padding: 0 12px;" valign="top">
                                                                <a href="https://www.facebook.com/sharer/sharer.php?u=${shareUrl}" style="color: #1a8cff; text-decoration: underline;">
                                                                    <img src="http://files.planeta.ru/mailer/interactive/i/share-ico-fb.png" width="43" height="43" style="outline: none; text-decoration: none; -ms-interpolation-mode: bicubic; display: block; vertical-align: middle; border: none;">
                                                                </a>
                                                            </td>
                                                            <td style="vertical-align: top; padding: 0 12px;" valign="top">
                                                                <a href="https://vk.com/share.php?url=${shareUrl}&image=${shareImageUrl}" style="color: #1a8cff; text-decoration: underline;">
                                                                    <img src="http://files.planeta.ru/mailer/interactive/i/share-ico-vk.png" width="43" height="43" style="outline: none; text-decoration: none; -ms-interpolation-mode: bicubic; display: block; vertical-align: middle; border: none;">
                                                                </a>
                                                            </td>
                                                            <td style="vertical-align: top; padding: 0 12px;" valign="top">
                                                                <a href="http://www.odnoklassniki.ru/dk?st.cmd=addShare&st.s=1&st._surl=${shareUrl}" style="color: #1a8cff; text-decoration: underline;">
                                                                    <img src="http://files.planeta.ru/mailer/interactive/i/share-ico-ok.png" width="43" height="43" style="outline: none; text-decoration: none; -ms-interpolation-mode: bicubic; display: block; vertical-align: middle; border: none;">
                                                                </a>
                                                            </td>
                                                            <td style="vertical-align: top; padding: 0 12px;" valign="top">
                                                                <a href="https://telegram.me/share/url?url=${shareUrl}" style="color: #1a8cff; text-decoration: underline;">
                                                                    <img src="http://files.planeta.ru/mailer/interactive/i/share-ico-tg.png" width="43" height="43" style="outline: none; text-decoration: none; -ms-interpolation-mode: bicubic; display: block; vertical-align: middle; border: none;">
                                                                </a>
                                                            </td>
                                                            <td style="vertical-align: top; padding: 0 12px;" valign="top">
                                                                <a href="https://twitter.com/home?status=${shareUrl}" style="color: #1a8cff; text-decoration: underline;">
                                                                    <img src="http://files.planeta.ru/mailer/interactive/i/share-ico-tw.png" width="43" height="43" style="outline: none; text-decoration: none; -ms-interpolation-mode: bicubic; display: block; vertical-align: middle; border: none;">
                                                                </a>
                                                            </td>
                                                        </tr>
                                                        </tbody></table>

                                                </td>
                                            </tr>
                                            </tbody></table>



                                        <p style="font-size: inherit; line-height: inherit; color: inherit; margin: 1.5em 0;">
                                            На&nbsp;случай, если вы&nbsp;захотите закрепить полученные знания или узнать
                                            новые подробности для работы над своим крауд-проектом, держите
                                            дополнительные материалы к&nbsp;урокам:</p>

                                        <ul style="font-size: inherit; line-height: inherit; color: inherit; padding-left: 36px; margin: 1.5em 0;">
                                            <li style="font-size: 14px; line-height: 21px; color: inherit; margin: 0 0 7px;">
                                                Урок 1. <a href="https://s4.planeta.ru/f/32b7/1505804437510_renamed.pdf" style="color: #1a8cff; text-decoration: underline;">Введение:
                                                что такое
                                                <nobr>онлайн-курс</nobr>
                                                «Крауд-проект за&nbsp;60&nbsp;минут»</a></li>
                                            <li style="font-size: 14px; line-height: 21px; color: inherit; margin: 0 0 7px;">
                                                Урок 2. <a href="https://s4.planeta.ru/f/32b8/1505804472892_renamed.pdf" style="color: #1a8cff; text-decoration: underline;">Словарь
                                                краудфандинга</a></li>
                                            <li style="font-size: 14px; line-height: 21px; color: inherit; margin: 0 0 7px;">
                                                Урок 3. <a href="https://s4.planeta.ru/f/32b9/1505804490849_renamed.pdf" style="color: #1a8cff; text-decoration: underline;">О&nbsp;дополнительных
                                                материалах</a></li>
                                            <li style="font-size: 14px; line-height: 21px; color: inherit; margin: 0 0 7px;">
                                                Урок 4. <a href="https://s4.planeta.ru/f/32ba/1505804517280_renamed.pdf" style="color: #1a8cff; text-decoration: underline;">Перед
                                                стартом: готовим «визитку» проекта</a></li>
                                            <li style="font-size: 14px; line-height: 21px; color: inherit; margin: 0 0 7px;">
                                                Урок 5. <a href="https://s4.planeta.ru/f/32bb/1505804541369_renamed.pdf" style="color: #1a8cff; text-decoration: underline;">Оформление
                                                черновика: название и&nbsp;краткая информация о&nbsp;проекте</a></li>
                                            <li style="font-size: 14px; line-height: 21px; color: inherit; margin: 0 0 7px;">
                                                Урок 6. <a href="https://s4.planeta.ru/f/32bc/1505804562072_renamed.pdf" style="color: #1a8cff; text-decoration: underline;">Как
                                                рассчитать оптимальный срок проекта?</a></li>
                                            <li style="font-size: 14px; line-height: 21px; color: inherit; margin: 0 0 7px;">
                                                Урок 7. <a href="https://s4.planeta.ru/f/32bd/1505804681320_renamed.pdf" style="color: #1a8cff; text-decoration: underline;">Финансовая
                                                цель: калькулятор
                                                <nobr>крауд-проекта</nobr>
                                            </a></li>
                                            <li style="font-size: 14px; line-height: 21px; color: inherit; margin: 0 0 7px;">
                                                Урок 8. <a href="https://s4.planeta.ru/f/32be/1505804701983_renamed.pdf" style="color: #1a8cff; text-decoration: underline;">Видеообращение:
                                                технические нюансы и&nbsp;смысловая нагрузка</a></li>
                                            <li style="font-size: 14px; line-height: 21px; color: inherit; margin: 0 0 7px;">
                                                Урок 9. <a href="https://s4.planeta.ru/f/32bf/1505804720715_renamed.pdf" style="color: #1a8cff; text-decoration: underline;">Текстовое
                                                описание и&nbsp;его лайфхаки</a></li>
                                            <li style="font-size: 14px; line-height: 21px; color: inherit; margin: 0 0 7px;">
                                                Урок 10. <a href="https://s4.planeta.ru/f/32c0/1505804734788_renamed.pdf"
                                                            style="color: #1a8cff; text-decoration: underline;">Вознаграждения:
                                                как конвертировать «спасибо» в&nbsp;рубли</a></li>
                                            <li style="font-size: 14px; line-height: 21px; color: inherit; margin: 0 0 7px;">
                                                Урок 11. <a href="https://s4.planeta.ru/f/32c1/1505804756369_renamed.pdf"
                                                            style="color: #1a8cff; text-decoration: underline;">Модерация
                                                проекта на&nbsp;Planeta.ru</a></li>
                                            <li style="font-size: 14px; line-height: 21px; color: inherit; margin: 0 0 7px;">
                                                Урок 12. <a href="https://s4.planeta.ru/f/32c2/1505804776459_renamed.pdf"
                                                            style="color: #1a8cff; text-decoration: underline;">Финал:
                                                мотивация наше всё!</a></li>
                                            <li style="font-size: 14px; line-height: 21px; color: inherit; margin: 0 0 7px;">
                                                Приложение: <a href="https://s4.planeta.ru/f/32c3/1505804794656_renamed.pdf"
                                                               style="color: #1a8cff; text-decoration: underline;">таблица
                                                для составления вознаграждений</a></li>
                                        </ul>

                                        <p style="font-size: inherit; line-height: inherit; color: inherit; margin: 1.5em 0;">
                                            В&nbsp;этих материалах вы&nbsp;найдете самые важные подробности и&nbsp;конкретные
                                            предложения по&nbsp;улучшению вашего проекта.</p>

                                        <p style="font-size: inherit; line-height: inherit; color: inherit; margin: 1.5em 0;">
                                            Смотрите видео курс, читайте наши материалы&nbsp;— и&nbsp;запускайте
                                            собственную
                                            <nobr>крауд-кампанию</nobr>
                                            !
                                        </p>

                                        <p style="font-size: inherit; line-height: inherit; color: inherit; margin: 1.5em 0;">
                                            Не&nbsp;стесняйтесь возвращаться к&nbsp;уже пройденным урокам&nbsp;— это
                                            поможет вам сделать вашу кампанию намного эффективней.</p>

                                        <p style="font-size: inherit; line-height: inherit; color: inherit; margin: 1.5em 0;">
                                            Удачи вам в&nbsp;сборе денег и&nbsp;до&nbsp;встречи на&nbsp;Planeta.ru!</p>

                                        <div style="font-family: Georgia, 'Times New Roman', Times, serif; font-style: italic; font-size: 14px; line-height: 24px; text-align: right; color: inherit;"
                                             align="right">
                                            С&nbsp;уважением<span style="margin-right: -3.77px;">,</span>
                                            <br>команда Planeta.ru
                                        </div>

                                    </td>
                                </tr>
                                </tbody>
                            </table>

                        </td>
                    </tr>


                    <tr>
                        <td style="vertical-align: top; font-size: 0; line-height: 0; padding: 4px 0 0;" valign="top">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td style="vertical-align: top; padding: 0;" valign="top">
                            <table style="border-spacing: 0; border-collapse: collapse; width: 100%; background: #fff;"
                                   bgcolor="#fff">
                                <tbody>
                                <tr>
                                    <td style="vertical-align: top; font-size: 16px; line-height: 24px; font-weight: 300; text-align: center; color: #454553; padding: 32px 45px 12px;"
                                        align="center" valign="top">
                                        <nobr>P. S.</nobr>
                                        Чтобы материалы всегда были под рукой, вы&nbsp;можете скачать их&nbsp;все одним
                                        архивом:
                                    </td>
                                </tr>
                                <tr>
                                    <td style="vertical-align: top; font-size: 14px; line-height: 21px; text-align: center; color: #454553; padding: 0 0 29px;"
                                        align="center" valign="top">
                                        <a href="https://s4.planeta.ru/f/32a9/1505804264196_renamed.zip" style="color: #1a8cff; text-decoration: underline;">Скачать
                                            архив</a>
                                        <br> (.zip, 16,7 mb)
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>


                    <tr>
                        <td style="vertical-align: top; font-size: 0; line-height: 0; padding: 4px 0 0;" valign="top">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td style="vertical-align: top; padding: 0;" valign="top">
                            <table style="border-spacing: 0; border-collapse: collapse; width: 100%; background: #fff;"
                                   bgcolor="#fff">
                                <tbody>
                                <tr>
                                    <td style="vertical-align: top; font-size: 15px; line-height: 18px; text-align: center; font-weight: 200; color: #73747e; padding: 21px 0;"
                                        align="center" valign="top">
                                        Присоединяйтесь к&nbsp;Planeta.ru в&nbsp;социальных сетях
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" style="vertical-align: top; padding: 0 0 29px;" valign="top">
                                        <table style="border-spacing: 0; border-collapse: collapse; margin: auto;">
                                            <tbody>
                                            <tr>
                                                <td style="vertical-align: top; padding: 0 8px;" valign="top">
                                                    <a href="https://l.planeta.ru/vk.com"
                                                       style="color: #01c8fe; text-decoration: underline;">
                                                        <img src="http://files.planeta.ru/mailer/interactive/i/ico-vk.png"
                                                             width="33" height="33"
                                                             style="outline: none; text-decoration: none; -ms-interpolation-mode: bicubic; display: block; vertical-align: middle; border: none;">
                                                    </a>
                                                </td>
                                                <td style="vertical-align: top; padding: 0 8px;" valign="top">
                                                    <a href="https://l.planeta.ru/facebook.com"
                                                       style="color: #01c8fe; text-decoration: underline;">
                                                        <img src="http://files.planeta.ru/mailer/interactive/i/ico-fb.png"
                                                             width="33" height="33"
                                                             style="outline: none; text-decoration: none; -ms-interpolation-mode: bicubic; display: block; vertical-align: middle; border: none;">
                                                    </a>
                                                </td>
                                                <td style="vertical-align: top; padding: 0 8px;" valign="top">
                                                    <a href="https://l.planeta.ru/ok.ru"
                                                       style="color: #01c8fe; text-decoration: underline;">
                                                        <img src="http://files.planeta.ru/mailer/interactive/i/ico-ok.png"
                                                             width="33" height="33"
                                                             style="outline: none; text-decoration: none; -ms-interpolation-mode: bicubic; display: block; vertical-align: middle; border: none;">
                                                    </a>
                                                </td>
                                                <td style="vertical-align: top; padding: 0 8px;" valign="top">
                                                    <a href="https://l.planeta.ru/instagram.com"
                                                       style="color: #01c8fe; text-decoration: underline;">
                                                        <img src="http://files.planeta.ru/mailer/interactive/i/ico-ig.png"
                                                             width="33" height="33"
                                                             style="outline: none; text-decoration: none; -ms-interpolation-mode: bicubic; display: block; vertical-align: middle; border: none;">
                                                    </a>
                                                </td>
                                                <td style="vertical-align: top; padding: 0 8px;" valign="top">
                                                    <a href="https://l.planeta.ru/twitter.com"
                                                       style="color: #01c8fe; text-decoration: underline;">
                                                        <img src="http://files.planeta.ru/mailer/interactive/i/ico-tw.png"
                                                             width="33" height="33"
                                                             style="outline: none; text-decoration: none; -ms-interpolation-mode: bicubic; display: block; vertical-align: middle; border: none;">
                                                    </a>
                                                </td>
                                                <td style="vertical-align: top; padding: 0 8px;" valign="top">
                                                    <a href="https://l.planeta.ru/youtube.com"
                                                       style="color: #01c8fe; text-decoration: underline;">
                                                        <img src="http://files.planeta.ru/mailer/interactive/i/ico-yt.png"
                                                             width="33" height="33"
                                                             style="outline: none; text-decoration: none; -ms-interpolation-mode: bicubic; display: block; vertical-align: middle; border: none;">
                                                    </a>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>


                    <tr>
                        <td style="vertical-align: top; padding: 0;" valign="top">
                            <table style="border-spacing: 0; border-collapse: collapse; width: 100%;">
                                <tbody>
                                <tr>
                                    <td style="vertical-align: top; font-size: 0; line-height: 0; padding: 14px 0 0;"
                                        valign="top">&nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td style="vertical-align: top; font-size: 12px; line-height: 17px; text-align: left; padding: 18px 19px 0;"
                                        align="left" valign="top">
                                        Отзывы и&nbsp;пожелания по&nbsp;поводу курса вы&nbsp;можете направлять напрямую
                                        его ведущему и&nbsp;куратору образовательного направления Planeta.ru Егору
                                        Ельчину на&nbsp;адрес <a href="mailto:school@planeta.ru"
                                                                 style="color: #01c8fe; text-decoration: underline;">school@planeta.ru</a>.
                                    </td>
                                </tr>
                                <tr>
                                    <td style="vertical-align: top; font-size: 12px; line-height: 17px; text-align: left; padding: 18px 19px 0;"
                                        align="left" valign="top">
                                        Вы&nbsp;получили это письмо, так как ввели
                                        <nobr>e-mail</nobr>
                                        <a href="mailto:test@test.com"
                                           style="color: #01c8fe; text-decoration: underline;">${mailTo}</a> при
                                        регистрации на&nbsp;странице
                                        <nobr>онлайн-курса</nobr>
                                        «Краудфандинговый проект за&nbsp;60&nbsp;минут»
                                        <nobr>крауд-платформы</nobr>
                                        Planeta.ru.
                                    </td>
                                </tr>
                                <tr>
                                    <td style="vertical-align: top; font-size: 0; line-height: 0; padding: 14px 0 0;"
                                        valign="top">&nbsp;
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>


                    </tbody>
                </table>
            </center>
        </td>
    </tr>
    <tr>
        <td style="vertical-align: top; font-size: 0; line-height: 0; padding: 25px 0 0;" valign="top">&nbsp;</td>
    </tr>
    </tbody>
</table>


</body>
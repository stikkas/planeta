<#include "email-template-header.ftl">

<a href="https://biblio.planeta.ru"><img src="https://s3.planeta.ru/i/159c9b/1479383744597_renamed.jpg"></a>

<div style="margin-top: 5px;"><strong>В библиотеку:</strong> ${library.name}</div>

<div style="margin-top: 5px;"><strong>Адрес:</strong> <#if library.postIndex??>${library.postIndex}, </#if> ${library.address}</div>

<div style="margin-top: 5px;"><strong>Дата:</strong> ${date?string("dd MMMMM yyyy")}</div>

<div style="color: #00b8e5; font-size: 25px; margin-top: 15px;">Добрый день!</div>

<div style="margin-top: 15px;">В проекте "Библиородина" (biblio.planeta.ru) на адрес вашей библиотеки оформлена
    подписка на
</div>
<#list bookOrders as bookOrder>
<div style="margin-top: 10px; margin-left: 30px;">
    <div><strong>Название:</strong> ${bookOrder.book.title}</div>
    <div style="margin-top: 5px;"><strong>Издательство:</strong> ${bookOrder.book.publishingHouseName}</div>
    <div style="margin-top: 5px;"><strong>Количество:</strong> ${bookOrder.count}</div>
    <div style="margin-top: 5px;"><strong>Подписка:</strong> ${bookOrder.book.comment}</div>
    <div style="margin-top: 5px;"><strong>ФИО менцената:</strong> ${buyerName}</div>
</div>
</#list>
</div>

<hr>

<div><strong>Внимание!</strong> Срок поступления печатных изданий в библиотеку зависит от периода подписки, внутренних правил и процедур
    издательства и службы доставки.
</div>

<div style="margin-top: 10px; font-style: italic">«БиблиоРодина» представляет собой интернет-портал с функционалом для оформления подписок на российские научные и
    научно-популярные периодические издания в дар региональным библиотекам. Это новый формат подписки, основанный на
    принципах краудфандинга или другими словами - народного финансирования. Запуск проекта инициирован краудфандинговой
    платформой Planeta.ru при поддержке Государственной Думы РФ. Партнерами проекта стали Российская библиотечная
    ассоциация, Русская школьная библиотечная ассоциация (РШБА), Почта России и различные тематические издания.
</div>

<div style="margin-top: 10px;">По всем вопросам вы можете связаться с краудфандинговой платформой Planeta.ru по
    электронной почте
    biblio@planeta.ru, а также прислать свои отзывы и предложения по продвижению проекта «БиблиоРодина» в вашей
    организации.
</div>

<div style="margin-top: 10px;">С уважением, Planeta.ru</div>

<#include "email-template-footer.ftl">
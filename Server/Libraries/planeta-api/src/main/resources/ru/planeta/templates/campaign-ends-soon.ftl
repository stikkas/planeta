<#include "email-template-header.ftl">
<@compress single_line=true>

<h2 style="
        letter-spacing: -1px;
        font: 700 30px/40px Arial,sans-serif;
        margin: 0;
        color: #1a1a1a;
        text-decoration:none;">
        <span style="color: #1a1a1a;">Проект &laquo;${campaignName}&raquo; завершается через два дня</span>
</h2>

<div class="post-blog-content">
    <p>Добрый день!</p>
    <p>Planeta.ru напоминает вам о&nbsp;завершении крауд-проекта &laquo;${campaignName}&raquo; &ndash; до&nbsp;финала кампании осталось два&nbsp;дня.</p>
    <p>Возможно, вы&nbsp;планировали покупки в&nbsp;проекте или&nbsp;хотели задать вопрос автору? Или&nbsp;же&nbsp;вам важно узнать, что&nbsp;нового произошло, пока вас не&nbsp;было? Проверьте!</p>

<a href="${StringUtils.unescapeHtml(baseHostUrl)}/campaigns/${campaignId}" style="line-height: 20px; color: #fff; text-decoration: none; font-size: 17px; font-weight: 700; white-space: nowrap; text-align: center;">
        <span style="line-height: inherit; display: inline-block; vertical-align: middle; text-decoration: none; color: #fff; border-radius: 5px; background: #63a7fb; padding: 20px 24px;" bgcolor="#63a7fb">
            Проверить
        </span>
</a>


</@compress>
<#include "email-template-footer.ftl">

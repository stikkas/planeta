<#macro video video index>
<div class="bb-video-player-cover" onclick="workspace.showVideoPlayer(${video.ownerId}, ${video.objectId}, this);return false;">
    <a class="bb-video-link">
        <img class="bb-video-preview" src="${video.imageUrl}">
    </a>
    <div class="bb-video-time">${utils.humanDuration(video.duration)}</div>
    <span class="bb-video-icon"></span>
</div>
</#macro>


<#macro image image index>
<a class="bb-image-link" href="<#if (image.ownerId > 0 && image.objectId > 0)>javascript:workspace.showPhoto(${image.ownerId}, ${image.objectId})<#else>${image.url}</#if>">
    <img class="bb-image-preview" src="${image.thumbnailUrl}">
</a>
</#macro>

<#macro audio audio>
<div class="track-list-item row">
    <div class="track-list-item-count">
        <a class="play-track"
           href="javascript:workspace.playTrack(${audio.ownerId}, ${audio.objectId});"></a>
    </div>
    <div class="track-list-item-name">
        <div class="track-list-item-name-title">${audio.name}</div>
        <div class="track-list-item-name-author">
            <a href="/search/audios/query=${audio.artist}">${audio.artist}</a>
        </div>
    </div>
    <div class="track-list-item-option">
        <div class="track-list-item-option-time">${utils.humanDuration(audio.duration)}
        </div>
    </div>
</div>
</#macro>


<@compress single-line=true>
    <#if (images?? && images?size > 0) >
    <div class="bb-image-cover">
        <#list images as img>
            <@image img img_index />
        </#list>
    </div>
    </#if>

    <#if (videos?? && videos?size > 0)>
    <div class="video-attachments clearfix">
        <#list videos as v>
            <@video v v_index />
        </#list>
    </div>
    </#if>

    <#if (audios?? && audios?size > 0)>
    <div class="track-list">
        <#list audios as a>
            <@audio a />
        </#list>
    </div>
    </#if>
</@compress>

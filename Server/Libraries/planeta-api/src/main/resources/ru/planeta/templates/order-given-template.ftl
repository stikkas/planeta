<@compress single_line=true>
    <#switch emailNode>
        <#case "SUBJECT">
                Ваш заказ №${order.orderId} выдан
            <#break>
        <#case "BODY">
            <#include "email-template-header.ftl">
            <h3 style="margin: 0 0 1em;">Здравствуйте, ${StringUtils.unescapeHtml(userName)}!</h3>
            <p>Заказ №<a href="${appHost}/account">${order.orderId}</a> был выдан.</p>

            <#if order.deliveryComment?? && order.deliveryComment?trim != "">
                <p>Комментарий: ${StringUtils.unescapeHtml(order.deliveryComment?xhtml)}</p>
            </#if>

            <p>Вы можете ознакомиться со списком ваших покупок в разделе <a style="color: #3498db;" href="${baseHostUrl}/account">«Мои заказы»</a>.
            <p>С наилучшими пожеланиями, <a href="${appHost}">Planeta.ru</a></p>
            <#include "email-template-footer.ftl">
            <#break>
    </#switch>
</@compress>


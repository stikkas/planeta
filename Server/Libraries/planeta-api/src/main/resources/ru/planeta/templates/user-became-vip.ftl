<@compress single_line=true>
<table width="700" border="0" cellpadding="0" cellspacing="0" style="font-family:Arial, sans-serif; font-size:16px; line-height:24px; color:#1f1f1f; margin: 0 auto;background:#ffffff;">
    <tbody>
    <tr>
        <td>

            <table width="100%" border="0" cellpadding="0" cellspacing="0" style="margin: 0">
                <tbody>
                <tr>
                    <td valign="top">
                        <a href="${baseHostUrl}/welcome/bonuses.html"><img src="https://s2.planeta.ru/i/77b14/1410347697218_renamed.jpg" width="341" height="177" border="0" alt="Планета Club"></a>
                    </td>
                    <td valign="middle">
                        <a href="${baseHostUrl}"><img src="https://s2.planeta.ru/i/77b13/1410347694886_renamed.jpg" width="99" height="27" border="0" alt="Planeta.ru"></a>
                    </td>
                    <td width="40"></td>
                </tr>
                </tbody>
            </table>


            <table width="100%" border="0" cellpadding="0" cellspacing="0" style="margin: 0">
                <tbody>
                <tr>
                    <td width="40"></td>
                    <td>

                        <p>
                            <img src="https://s2.planeta.ru/i/77b12/1410347691945_renamed.jpg" width="557" height="80" border="0" alt="Выберите и получите уникальный подарок">
                        </p>

                        <p>У нас для вас хорошие новости!</p>
                        <p>За пять лет, что мы растем и развиваемся вместе с вами, авторами проектов на Planeta.ru удалось достичь замечательных результатов. Более семисот млн. рублей уже собрано на реализацию более 3500 проектов.</p>
                        <p>Особенно приятно, что именно вы помогли нашим авторам в достижении этих целей!</p>
                        <p>В качестве «спасибо» за вашу активную поддержку, мы хотим пригласить вас в закрытый «Vip Club». Членами закрытого клуба становятся пользователи «Планеты», совершившие ${count} и более покупок как в проектах, нашем интернет-магазине, так и в проекте БиблиоРодина  на общую сумму не меньше ${amount} рублей.</p>

                    </td>
                    <td width="40"></td>
                </tr>
                <tr>
                    <td colspan="3" height="28"></td>
                </tr>
                </tbody>
            </table>


            <table width="100%" border="0" cellpadding="0" cellspacing="0" style="margin: 0;background:#d12f2b;background-image: url(https://s2.planeta.ru/i/77b11/1410347691033_renamed.jpg);background-position:50% 0;">
                <tbody>
                <tr>
                    <td valign="middle" align="center" height="90">
                        <a href="${baseHostUrl}/welcome/bonuses.html" style="padding: 17px 50px;background:#fff;color:#d9332f;font-size: 14px;line-height:16px;font-weight: 700;letter-spacing:0.2em;">ВЫБРАТЬ ПОДАРОК</a>
                    </td>
                </tr>
                </tbody>
            </table>


            <table width="100%" border="0" cellpadding="0" cellspacing="0" style="margin: 0;font-size: 14px;line-height: 1.5;">
                <tbody>
                <tr>
                    <td colspan="3" height="28"></td>
                </tr>
                <tr>
                    <td width="40"></td>
                    <td>
                        <p>Член клуба после вступления может выбрать только один подарок. Количество бонусов ограничено, не откладывайте время выбора! Переходите по ссылке, выбирайте и заказывайте.</p>
                        <p>Вы остаетесь членом клуба до тех пор, пока принимаете активное участие в жизни проектов! После выбора подарка для получения следующего необходимо совершить покупки как в проектах, так и в нашем интернет-магазине на общую сумму не менее ${second} рублей.</p>
                        <p>В&nbsp;случае возникновения вопросов, обращайтесь к&nbsp;куратору Клуба Катерине&nbsp;Лукиной&nbsp;— <a href="mailto:vipclub@planeta.ru" style="color: #00bfdf">vipclub@planeta.ru</a></p>

                    </td>
                    <td width="40"></td>
                </tr>
                <tr>
                    <td colspan="3" height="28"></td>
                </tr>
                </tbody>
            </table>


        </td>
    </tr>
    </tbody>
</table>
</@compress>

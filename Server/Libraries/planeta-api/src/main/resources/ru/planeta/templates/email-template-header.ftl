<@compress single_line=true>
<table style="font-size: 18px; line-height: 27px; border-spacing: 0; border-collapse: collapse; height: 100%; width: 100%; -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; color: #585754; font-family: 'Arial', sans-serif; font-weight: normal; text-align: left; background: #f0eee8; margin: 0; padding: 0;" bgcolor="#f0eee8">
    <tr style="line-height: inherit;">
        <td style="line-height: inherit; vertical-align: top; height: 40px; padding: 0;" valign="top"><img src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" height="40" width="1" style="display:block"></td>
    </tr>
    <tr style="line-height: inherit;">
        <td style="line-height: inherit; vertical-align: top; padding: 0;" valign="top">

            <center style="line-height: inherit; width: 100%; min-width: 700px;">
                <table style="line-height: inherit; border-spacing: 0; border-collapse: collapse; width: 700px; text-align: left; margin: auto;">
                    <tr style="line-height: inherit;">
                        <td style="line-height: inherit; vertical-align: top; padding: 0;" valign="top">



                            <table style="line-height: inherit; border-spacing: 0; border-collapse: collapse; width: 100%; color: #fff; background: #63a7fb url(http://files.planeta.ru/mailer/nl/i/header-wrap-bg.png);" background="http://files.planeta.ru/mailer/nl/i/header-wrap-bg.png" bgcolor="#63a7fb">
                                <tr style="line-height: inherit;">
                                    <td style="line-height: inherit; vertical-align: top; padding: 0;" valign="top">

                                        <table style="line-height: inherit; border-spacing: 0; border-collapse: collapse; width: 100%;">
                                            <tr style="line-height: inherit;">
                                                <td style="line-height: inherit; vertical-align: top; padding: 0;" valign="top">


                                                    <table style="line-height: inherit; border-spacing: 0; border-collapse: collapse; width: 100%;">
                                                        <tr style="line-height: inherit;">
                                                            <td style="line-height: inherit; vertical-align: top; width: 40px; padding: 0;" valign="top"></td>
                                                            <td style="line-height: inherit; vertical-align: top; padding: 0;" valign="top">
                                                                <table style="line-height: inherit; border-spacing: 0; border-collapse: collapse; width: 100%;">
                                                                    <tr style="line-height: inherit;">
                                                                        <td colspan="2" style="line-height: inherit; vertical-align: top; height: 24px; padding: 0;" valign="top"><img src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" height="24" width="1" style="display:block"></td>
                                                                    </tr>
                                                                    <tr style="line-height: inherit;">
                                                                        <td style="line-height: inherit; vertical-align: top; padding: 0;" valign="top">
                                                                            <a href="${baseHostUrl}" style="line-height: inherit; color: #3498db; text-decoration: underline;">

                                                                                <img src="http://files.planeta.ru/mailer/nl/i/logo.png" width="118" height="31" style="line-height: inherit; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic; display: block; border: none;" />

                                                                            </a>
                                                                        </td>
                                                                        <td align="right" style="line-height: inherit; vertical-align: middle !important; padding: 0;" valign="middle">
                                                                            <table style="line-height: 18px; border-spacing: 0; border-collapse: collapse; font-size: 16px; color: #fff;">
                                                                                <tr style="line-height: inherit;">
                                                                                    <td style="line-height: inherit; vertical-align: top; padding: 0 0 0 48px;" valign="top">
                                                                                        <a href="${baseHostUrl}/search/projects" style="line-height: inherit; color: #fff; text-decoration: none;">Проекты</a>
                                                                                    </td>
                                                                                    <td style="line-height: inherit; vertical-align: top; padding: 0 0 0 48px;" valign="top">
                                                                                        <a href="${shopHostUrl}" style="line-height: inherit; color: #fff; text-decoration: none;">Магазин</a>
                                                                                    </td>
                                                                                    <td style="line-height: inherit; vertical-align: top; padding: 0 0 0 48px;" valign="top">
                                                                                        <a href="${baseHostUrl}/about" style="line-height: inherit; color: #fff; text-decoration: none;">О нас</a>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr style="line-height: inherit;">
                                                                        <td colspan="2" style="line-height: inherit; vertical-align: top; height: 24px; padding: 0;" valign="top"><img src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" height="24" width="1" style="display:block"></td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                            <td style="line-height: inherit; vertical-align: top; width: 40px; padding: 0;" valign="top"></td>
                                                        </tr>
                                                    </table>


                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>


                            <table style="line-height: inherit; border-spacing: 0; border-collapse: collapse; width: 100%; background: #fff;" bgcolor="#fff">
                                <tr style="line-height: inherit;">
                                    <td style="line-height: inherit; vertical-align: top; padding: 0;" valign="top">

                                        <table style="line-height: inherit; border-spacing: 0; border-collapse: collapse; width: 100%;">
                                            <tr style="line-height: inherit;">
                                                <td style="line-height: inherit; vertical-align: top; width: 40px; padding: 0;" valign="top"></td>
                                                <td style="line-height: inherit; vertical-align: top; padding: 0;" valign="top">
                                                    <table style="line-height: inherit; border-spacing: 0; border-collapse: collapse; width: 100%;">
                                                        <tr style="line-height: inherit;">
                                                            <td style="line-height: inherit; vertical-align: top; height: 40px; padding: 0;" valign="top"><img src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" height="40" width="1" style="display:block"></td>
                                                        </tr>
                                                        <tr style="line-height: inherit;">
                                                            <td style="line-height: inherit; vertical-align: top; padding: 0;" valign="top">


</@compress>
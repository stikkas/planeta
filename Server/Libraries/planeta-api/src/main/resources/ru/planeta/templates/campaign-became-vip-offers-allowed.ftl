<@compress single_line=true>
<table width="660" border="0" cellpadding="0" cellspacing="0" style="font-family:Arial, Helvetica, sans-serif;font-size:18px;line-height:29px;color:#111111;margin:0 auto">
    <tbody>
    <tr>
        <td height="20"></td>
    </tr>
    <tr>
        <td>
            <table width="100%" border="0" cellpadding="0" cellspacing="0" style="font-size: 16px;line-height: 27px;margin: 0;background:#1d242a;-webkit-border-radius: 10px;-moz-border-radius: 10px;border-radius: 10px;">
                <tbody>
                <tr>
                    <td width="14" height="60"></td>
                    <td width="140" align="left" valign="middle">
                        <a href="${StringUtils.unescapeHtml(baseHostUrl)}" target="_blank"><img src="https://s2.planeta.ru/i/53d0a/1387361332768_renamed.jpg" alt="Planeta.ru" width="120" height="31" border="0" style="display: block;"></a>
                    </td>
                    <td style="color: #ffffff" align="right" valign="middle">
                        <a href="${StringUtils.unescapeHtml(baseHostUrl)}/search/projects" target="_blank" style="text-decoration:none;color: #ffffff;margin: 0 20px;">
                            <span style="text-decoration:none;color: #ffffff">Проекты</span>
                        </a>
                        <a href="${StringUtils.unescapeHtml(baseHostUrl)}/search/shares" target="_blank" style="text-decoration:none;color: #ffffff;margin: 0 20px;">
                            <span style="text-decoration:none;color: #ffffff">Вознаграждения</span>
                        </a>
                        <a href="${StringUtils.unescapeHtml(tvHostUrl)}" target="_blank" style="text-decoration:none;color: #ffffff;margin: 0 20px;">
                            <span style="text-decoration:none;color: #ffffff">Трансляции</span>
                        </a>
                        <a href="${StringUtils.unescapeHtml(shopHostUrl)}" target="_blank" style="text-decoration:none;color: #ffffff;margin: 0 20px;">
                            <span style="text-decoration:none;color: #ffffff">Магазин</span>
                        </a>
                    </td>
                    <td width="17"></td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    <tr>
        <td height="40"></td>
    </tr>
    <tr>
        <td>

            <table width="100%" border="0" cellpadding="0" cellspacing="0" style="margin: 0;">
                <tbody>
                <tr>
                    <td width="30"></td>
                    <td align="left" valign="top">

                        <h1 style="margin: 0 0 27px;font-size: 32px;line-height: 38px;font-weight: 700;letter-spacing: -1px;">Здравствуйте!</h1>

                        <p style="margin: 0 0 25px;">
                            Команда портала Planeta.ru поздравляет вас с&nbsp;достижением 50% от&nbsp;заявленной цели и&nbsp;желает дальнейшего успешного сбора проекту!
                            <br>
                            Мы&nbsp;хотим рассказать о&nbsp;сервисах, которые наверняка могут быть полезны для&nbsp;вашего проекта как&nbsp;сейчас, так&nbsp;и&nbsp;после его успешного завершения.
                        </p>



                    </td>
                    <td width="30"></td>
                </tr>
                </tbody>
            </table>

            <img style="margin: 0 0 25px;" src="https://s2.planeta.ru/i/7e7ab/1414412059828_renamed.jpg">

            <table width="100%" border="0" cellpadding="0" cellspacing="0" style="margin: 0;">
                <tbody>
                <tr>
                    <td width="30"></td>
                    <td align="left" valign="top">
                        <p style="margin: 0 0 25px"><b>&laquo;Планета Club&raquo;</b>

                        <p style="margin: 0 0 25px"><a href="https://vip.planeta.ru" style="color:#3498db;">&laquo;Планета Club&raquo;</a>&nbsp;&mdash; это клуб привилегированных спонсоров портала. Членами этого клуба по&nbsp;персональному приглашению становятся пользователи, совершившие не менее ${count} покупок на&nbsp;общую сумму не&nbsp;менее ${amount} рублей. Участие в&nbsp;&laquo;Планета Club&raquo; дает акционерам возможность выбирать эксклюзивные подарки, эмоции и&nbsp;переживания, предложенные авторами и&nbsp;партнерами &laquo;Планеты&raquo;.</p>

                        <p style="margin: 0 0 25px">На&nbsp;данный момент членами клуба являются самые активные спонсоры, которые ежедневно посещают наш сайт, поддерживают интересные проекты из&nbsp;различных категорий и&nbsp;следят за&nbsp;новыми. В&nbsp;среднем пользователь поддерживает 13 проектов на&nbsp;сумму 4500&ndash;5000&nbsp;рублей каждый, и&nbsp;одним из&nbsp;авторов, чью идею могут поддержать наши акционеры, можете стать вы!</p>

                        <p style="margin: 0 0 25px;padding: 20px;border-left:2px solid #b34a3a;font-family:Georgia, sans-serif;font-size: 23px;line-height: 34px;background:#f7f6f4;">Мы&nbsp;с&nbsp;удовольствием приглашаем вас стать частью &laquo;Планета Club&raquo; и&nbsp;предложить свои товары в&nbsp;качестве подарков.</p>

                        <p style="margin: 0 0 25px">Подробную информацию о&nbsp;размещении товаров в &laquo;Планета Club&raquo; вы&nbsp;можете узнать у его куратора <a href="mailto:vipclub@planeta.ru" style="color:#3498db;">Катерины Лукиной</a>.</p>

                        <p style="margin: 43px 0">
                            <a href="mailto:vipclub@planeta.ru" style="font-size: 14px;line-height: 18px;font-weight: 700;text-decoration:none;color: #ffffff;background: #3498db">
                                <span style="text-decoration:none;color: #ffffff;background: #3498db; padding: 12px 26px;">Cвязаться с куратором &laquo;Планета Club&raquo;</span>
                            </a>
                        </p>

                        <p style="margin: 0 0 25px"><b>Магазин &laquo;Планета&raquo;</b>

                        <p style="margin: 0 0 25px">Также вы можете воспользоваться еще одним активно развивающимся сервисом – <a href="https://shop.planeta.ru" style="color:#3498db;">интернет-магазином «Планеты»</a>. В нем продаются эксклюзивные или выпущенные ограниченным тиражом товары: диски и книги с автографами, артефакты из личных коллекций и многое другое. С помощью Магазина «Планета» автор может дополнительно монетизировать свой успешный проект, выставив на продажу вещи, созданные по итогам крауд-кампании.</p>

                        <p style="margin: 0 0 25px">Подробную информацию о&nbsp;магазине &laquo;Планета&raquo; вы&nbsp;можете узнать, написав письмо на&nbsp;адрес <a href="mailto:shop@planeta.ru" style="color:#3498db;">shop@planeta.ru</a>.</p>

                        <p style="margin: 43px 0">
                            <a href="mailto:shop@planeta.ru" style="font-size: 14px;line-height: 18px;font-weight: 700;text-decoration:none;color: #ffffff;background: #3498db">
                                <span style="text-decoration:none;color: #ffffff;background: #3498db; padding: 12px 26px;">Узнать о магазине &laquo;Планета&raquo;</span>
                            </a>
                        </p>
                    </td>
                    <td width="30"></td>
                </tr>
                </tbody>
            </table>

        </td>
    </tr>
    <tr>
        <td height="20"></td>
    </tr>
    <tr>
        <td>


            <table width="100%" border="0" cellpadding="0" cellspacing="0" style="border-top:1px solid #d4d6d9;">
                <tbody>
                <tr>
                    <td width="40" height="66" valign="middle">
                        <a href="http://vk.com/planetaru" target="_blank"><img src="https://s2.planeta.ru/i/758b5/1409051425924_renamed.jpg" width="30" height="30" style="display:block"></a>
                    </td>
                    <td width="40" valign="middle">
                        <a href="http://www.facebook.com/planetaru" target="_blank"><img src="https://s2.planeta.ru/i/758b3/1409051425329_renamed.jpg" width="30" height="30" style="display:block"></a>
                    </td>
                    <td width="40" valign="middle">
                        <a href="http://twitter.com/PlanetaPortal" target="_blank"><img src="https://s2.planeta.ru/i/758b4/1409051425633_renamed.jpg" width="30" height="30" style="display:block"></a>
                    </td>
                    <td width="40" valign="middle">
                        <a href="http://www.youtube.com/user/planetarutv" target="_blank"><img src="https://s2.planeta.ru/i/758b6/1409051426200_renamed.jpg" width="30" height="30" style="display:block"></a>
                    </td>
                    <td width="10"></td>
                    <td valign="middle" style="font-size: 11px">
                        &copy; 2018 Planeta.ru
                        <span style="color:#c0c0c0;margin:0 4px;">&bull;</span>
                        <a href="mailto:support@planeta.ru" style="color: #3498db">support@planeta.ru</a>
                        <span style="color:#c0c0c0;margin:0 4px;">&bull;</span>
                        Не хотите получать рассылку? <a href="${StringUtils.unescapeHtml(baseHostUrl)}/settings/notifications" style="color: #3498db" target="_blank">Отписаться</a>
                    </td>
                </tr>
                </tbody>
            </table>

        </td>
    </tr>
    <tr>
        <td height="20"></td>
    </tr>
    </tbody>
</table>
</@compress>
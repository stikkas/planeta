<@compress single_line=true>
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

    <style type="text/css">

        h2.hn{
            letter-spacing: -1px;
            font: 700 30px/40px Arial,sans-serif;
            color: #000;
            margin: 0;
        }

        .post-blog-content{
            font-family: Arial, Tahoma, sans-serif;
            font-size: 14px;
            line-height: 22px;
            word-wrap: break-word;
        }

        a{
            color: #26aedd;
        }
        p, li{
            font: 14px/22px Arial,sans-serif;
            color: #333;
            margin: 0;
        }
        ul, ol{
            padding: 0;
            margin: 0 0 0 35px;
        }
        img{
            max-width: 100%;
            border: 0;
        }
        pre{
            font-family: Menlo,Monaco,"Courier New",monospace;
            color: #333333;
            display: block;
            padding: 8.5px;
            margin: 0;
            font-size: 12px;
            line-height: 18px;
            background-color: #f5f5f5;
            border: 1px solid #ccc;
            border: 1px solid rgba(0, 0, 0, 0.15);
            -webkit-border-radius: 4px;
            -moz-border-radius: 4px;
            border-radius: 4px;
            white-space: pre-wrap;
            word-break: break-all;
        }
        .mce-underline{
            text-decoration: underline;
        }
        .mce-strikethrough{
            text-decoration: line-through;
        }
        .mce-text-center{
            text-align: center;
        }
        .mce-text-right{
            text-align: right;
        }
        .mce-text-justify{
            text-align: justify;
        }
        h1, h2, h3, h4, h5, h6{
            font-weight: 400;
            color: #333;
        }
        h1{
            font-size: 24px;
            line-height: 36px;
            margin: 0 0 15px;
        }
        h2{
            font-weight: 700;
            font-size: 21px;
            line-height: 31px;
            letter-spacing: -1px;
            color: #1a1a1a;
            margin: 0 0 15px;
        }
        h3{
            font-size: 18px;
            line-height: 27px;
            color: #808080;
            margin: 0 0 15px;
        }
        h4{
            font-size: 14px;
            line-height: 23px;
            font-weight: 700;
            color: #4D4D4D;
            margin: 0 0 15px;
        }
        h5{
            font-family: Georgia, serif;
            font-size: 18px;
            line-height: 27px;
            font-style: italic;
            font-weight: 700;
            margin: 0 0 15px;
        }


    </style>
</head>
<body>
    ${noInlineStyleHtml}
</body>
</html>
</@compress>

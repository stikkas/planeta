<#include "email-template-header.ftl">
<@compress single_line=true>

<h3 style="margin: 0 0 1em;">${dearByGender} ${order.buyerName}!</h3>
<p>Вы&nbsp;оформили подписку на&nbsp;печатные издания в&nbsp;поддержку библиотек&nbsp;России в&nbsp;проекте «БиблиоРодина».</p>

<p>Издания:
<ul>
    <#list books as bookEntry>
        <#assign book = bookEntry.getLeft()>
        <#assign count = bookEntry.getRight()>
        <li><b>${book.title}</b><br/>
        Цена подписки: ${book.price}<br/>
        Количество подписок: ${count}<br/>
        Стоимость: ${count*book.price}<br/>
        Бонус для мецената: ${book.bonus}<br/></li>
    </#list>
</ul>
</p>

<p>Издания будут доставлены в библиотеки:
<ul>
    <#list libraries as library>
        <li><b>${library.name}</b><br/>
            ${library.address}<br/>
    </#list>
</ul>
</p>

<p>Номер вашего заказа: ${order.orderId}</p>
<p>Общая стоимость заказа: ${order.totalPrice}</p>

<p>Вы можете ознакомиться со списком ваших покупок в разделе <a style="color: #3498db;" href="${baseHostUrl}/account">«Мои заказы»</a>.
В случае возникновения вопросов вы можете обратиться в службу поддержки по адресу <a style="color: #3498db;" href="mailto:support@planeta.ru">support@planeta.ru</a>.
<br/>
<br/>
Будем рады новым встречам!
</p>

<a href="${StringUtils.unescapeHtml(baseHostUrl)}/api/welcome/biblio/cert-pdf.html?transactionId=${order.topayTransactionId}" style="line-height: 20px; color: #fff; text-decoration: none; font-size: 17px; font-weight: 700; white-space: nowrap; text-align: center;">
    <span style="line-height: inherit; display: inline-block; vertical-align: middle; text-decoration: none; color: #fff; border-radius: 5px; background: #63a7fb; padding: 20px 24px;" bgcolor="#63a7fb">
        Скачать сертификат
    </span>
</a>


</@compress>
<#include "email-template-footer.ftl">

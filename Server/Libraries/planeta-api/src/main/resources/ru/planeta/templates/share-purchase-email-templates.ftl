<#include "email-template-header.ftl">
<@compress single_line=true>
<h3 style="margin: 0 0 1em;">
    <#switch order.buyerGender>
        <#case "NOT_SET">Уважаемый(-ая)<#break>
        <#case "MALE">Уважаемый<#break>
        <#case "FEMALE">Уважаемая<#break>
        <#default>Уважаемый(-ая)
    </#switch> ${StringUtils.unescapeHtml(order.buyerName)}!
</h3>
<p>Вы приобрели на сайте ${appHost} вознаграждение <i>${StringUtils.unescapeHtml(share.objectName)}</i> проекта
    <a style="color: #3498db;" href="${baseHostUrl}/campaigns/${share.ownerId}">${StringUtils.unescapeHtml(share.getOwnerName())}</a> от
    автора <i>${StringUtils.unescapeHtml(share.merchantName)}</i>.
</p>
<ul>
    <li>Номер заказа:<b> ${order.orderId}</b></li>
    <li>Стоимость заказа:<b> ${order.totalPrice?string("0.##")} руб.</b></li>
    <li>Цена одного вознаграждения:<b> ${share.price?string("0.##")} руб.</b></li>
    <li>Количество:<b> ${order.orderObjectsInfo?size}</b></li>
</ul>
    <#if delivery??>
    <p><b>${delivery.name}</b></p>
        <#if delivery.price?number &gt; 0>
        <ul>
            <#switch delivery.serviceType>
                <#case "DELIVERY">
                    <li>Стоимость доставки:<b> ${delivery.price?string("0.##")} руб.</b></li>
                    <#break>
                <#case "CUSTOMER_PICKUP">
                    <li>Стоимость доставки в пункт самовывоза:<b> ${delivery.price} руб.</b></li>
                    <#break>
            </#switch>
        </ul>
        </#if>
        <#if delivery.publicNote?? >
        <p>${delivery.publicNote}</p>
        </#if>
        <#if address??>
        <p>
            <#switch delivery.serviceType>
                <#case "DELIVERY">
                    Заказ будет доставлен по указанному Вами адресу:
                    <#break>
                <#case "CUSTOMER_PICKUP">
                    После объявления о начале самовывоза заказ можно будет забрать по адресу:
                    <#break>
            </#switch>
        ${address.city}, ${address.address} <#if address.phone??>, тел.:${address.phone}.</#if></p>
        </#if>
    </#if>
<p>Вы можете ознакомиться со <a style="color: #3498db;" href="${baseHostUrl}/account">списком ваших покупок в в разделе «Мои заказы» на Планете</a>.</p>
    <#if instruction??>
    <p>Как получить вознаграждение:</p>
    <p>${instruction}</p>
    </#if>

<p>
    Если у Вас возникли какие-то трудности, то Вы всегда можете сообщить о них в нашу
    <a style="color: #3498db;" href="mailto:${supportEmail}">службу поддержки</a>.
</p>
</@compress>
<#include "email-template-footer.ftl">

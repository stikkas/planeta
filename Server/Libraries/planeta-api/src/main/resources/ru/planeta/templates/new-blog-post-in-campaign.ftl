<#include "email-template-header.ftl">
<@compress single_line=true>

    <h2 style="
        letter-spacing: -1px;
        font: 700 30px/40px Arial,sans-serif;
        margin: 0;
        color: #1a1a1a;
        text-decoration:none;">
        <a href="${blogPostLink}" target="_blank"
           style="text-decoration:none;color: #1a1a1a;">
            <span style="color: #1a1a1a;">${blogPostTitle}</span>
        </a>
    </h2>

    <div style="margin:0 0 20px;font-size:11px;color:#808080;">${blogPostDate}</div>

    <div class="post-blog-content">
        <p>${blogPostHtml}</p>
    </div>

    <a href="${StringUtils.unescapeHtml(baseHostUrl)}/campaigns/${campaignId}" style="line-height: 20px; color: #fff; text-decoration: none; font-size: 17px; font-weight: 700; white-space: nowrap; text-align: center;">
        <span style="line-height: inherit; display: inline-block; vertical-align: middle; text-decoration: none; color: #fff; border-radius: 5px; background: #63a7fb; padding: 20px 24px;" bgcolor="#63a7fb">
            Перейти к проекту
        </span>
    </a>


</@compress>
<#include "email-template-footer.ftl">

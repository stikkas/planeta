<#include "email-template-header.ftl">

<a href="https://biblio.planeta.ru"><img src="https://s3.planeta.ru/i/159c9b/1479383744597_renamed.jpg"></a>

<div style="margin-top: 5px;"><strong>В издательство:</strong> ${bookOrder.book.publishingHouseName}</div>

<div style="margin-top: 5px;"><strong>Дата:</strong> ${date?string("dd MMMMM yyyy")}</div>

<div style="color: #00b8e5; font-size: 25px; margin-top: 15px;">Добрый день!</div>

<div style="margin-top: 15px;">В проекте "Библиородина" (<a href="https://biblio.planeta.ru">biblio.planeta.ru</a>) была оформлена подписка на ваше издание:</div>

<div style="margin-top: 5px;"><strong>Название:</strong> ${bookOrder.book.title}</div>
<div style="margin-top: 5px;"><strong>Количество экземпляров/подписок:</strong> ${bookOrder.count}</div>
<div style="margin-top: 5px;"><strong>Бонус мецената:</strong> ${bookOrder.book.bonus}</div>
<div style="margin-top: 5px;"><strong>ФИО мецената:</strong> ${buyerName}</div>
<div style="margin-top: 5px;"><strong>E-mail мецената:</strong> ${buyerEmail}</div>

<hr>

<div>
    <strong>Напоминание!</strong> Издательство самостоятельно направляет бонус каждому меценату на его адрес электронной
    почты. Срок направления бонуса не должен превышать 5 рабочих дней (рекомендация).
</div>

<div style="margin-top: 10px;">
    Формирование отчета, перечисление средств и отправка изданий в библиотеки осуществляется в соответствии с
    Агентским договором.
</div>

<div style="margin-top: 10px;">По всем вопросам вы можете связаться с краудфандинговой платформой Planeta.ru по
    электронной почте
    biblio@planeta.ru, а также прислать свои отзывы и предложения по продвижению проекта «БиблиоРодина» в вашей
    организации.
</div>

<div style="margin-top: 10px;">С уважением, Planeta.ru</div>

<#include "email-template-footer.ftl">
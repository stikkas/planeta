package ru.planeta.api.service.billing.payment.system.yamoney.mws.models;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

/**
 * Created by eshevchenko on 25.04.14.
 */
@XmlAccessorType(XmlAccessType.FIELD)
public abstract class BaseRequest {
}

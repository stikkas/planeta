package ru.planeta.api.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.apache.commons.lang3.StringUtils;
import ru.planeta.commons.model.Gender;
import ru.planeta.commons.web.RegistrationData;
import ru.planeta.model.common.auth.CredentialType;
import ru.planeta.model.common.auth.ExternalAuthentication;
import ru.planeta.model.common.auth.UserCredentials;

import javax.annotation.Nullable;
import java.io.Serializable;
import java.util.Date;

/**
 * Adapter for various data from external social networks
 */
public class ExternalUserInfo implements Serializable {

    public String getPhotoUrl() {
        return getRegistrationData().getPhotoUrl();
    }

    public void setGender(Gender gender) {
        getRegistrationData().setGender(gender);
    }

    public String getFirstName() {
        return getRegistrationData().getFirstName();
    }

    public Date getUserBirthDate() {
        return getRegistrationData().getUserBirthDate();
    }

    public void setFirstName(String firstName) {
        getRegistrationData().setFirstName(firstName);
    }

    public void setPhotoUrl(String photoUrl) {
        getRegistrationData().setPhotoUrl(photoUrl);
    }

    public Gender getGender() {
        return getRegistrationData().getGender();
    }

    public void setNickName(String nickName) {
        getRegistrationData().setNickName(nickName);
    }

    public String getLastName() {
        return getRegistrationData().getLastName();
    }

    public void setUserBirthDate(Date userBirthDate) {
        getRegistrationData().setUserBirthDate(userBirthDate);
    }

    public String getNickName() {
        return getRegistrationData().getNickName();
    }

    public void setLastName(String lastName) {
        getRegistrationData().setLastName(lastName);
    }

    private RegistrationData registrationData = new RegistrationData();

    @JsonIgnore
    public void setRegistrationData(RegistrationData registrationData) {
        this.registrationData = registrationData;
    }

    @JsonIgnore
    public RegistrationData getRegistrationData() {
        return registrationData;

    }

    public CredentialType getCredentialType() {
        return externalAuthentication.getCredentialType();
    }

    public void setCredentialType(CredentialType credentialType) {
        externalAuthentication.setCredentialType(credentialType);
    }

    public String getExternalUsername() {
        return externalAuthentication.getUsername();
    }

    public void setExternalUsername(String externalUsername) {
        externalAuthentication.setUsername(externalUsername);
    }

    @Nullable
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
    @JsonIgnore
    public boolean hasEmail() {
        return StringUtils.isNotBlank(email);
    }

    public UserCredentials createUserCredentials(long profileId) {
        UserCredentials userCredentials = new UserCredentials();
        userCredentials.setUserName(getExternalUsername());
        userCredentials.setProfileId(profileId);
        userCredentials.setCredentialType(getCredentialType());
        return userCredentials;
    }

    @JsonIgnore
    public ExternalAuthentication getExternalAuthentication() {
        return externalAuthentication;
    }

    @JsonIgnore
    public void setExternalAuthentication(ExternalAuthentication externalAuthentication) {
        this.externalAuthentication = externalAuthentication;
    }


    private transient ExternalAuthentication externalAuthentication = new ExternalAuthentication();

    private String email;

}

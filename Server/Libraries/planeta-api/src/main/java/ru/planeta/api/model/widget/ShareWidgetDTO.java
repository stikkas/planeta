package ru.planeta.api.model.widget;

/**
 * Date: 07.08.13
 * Time: 17:38
 */
public class ShareWidgetDTO extends CampaignWidgetDTO {
    private String shareName;
    private int sharePrice;

    public ShareWidgetDTO(String campaignName, WidgetColorTheme theme, String campaignUrl, String campaignImgUrl, int moneyCollected, int moneyTarget, String shareName, int sharePrice) {
        super(campaignName, theme, campaignUrl, campaignImgUrl, moneyCollected, moneyTarget);
        this.shareName = shareName;
        this.sharePrice = sharePrice;
    }

    public String getShareName() {
        return shareName;
    }

    public void setShareName(String shareName) {
        this.shareName = shareName;
    }

    public int getSharePrice() {
        return sharePrice;
    }

    public void setSharePrice(int sharePrice) {
        this.sharePrice = sharePrice;
    }
}

package ru.planeta.api.text;


import com.fasterxml.jackson.annotation.JsonIgnore;
import ru.planeta.api.Utils;

import java.util.ArrayList;
import java.util.List;

import static ru.planeta.api.Utils.nvl;

/**
 * Represents a message with attachments.
 * messageText is parsed for attachments, which are added to list of video or image attachments.
 * audioAttachments are added from client side only.
 * image and video attachments also could be added from the client side.
 */
public class Message {
    private String text;
    private List<MessagePart> parts = new ArrayList<MessagePart>();
    private List<VideoAttachment> videoAttachments = new ArrayList<VideoAttachment>();
    private List<ImageAttachment> imageAttachments = new ArrayList<ImageAttachment>();
    private List<AudioAttachment> audioAttachments = new ArrayList<AudioAttachment>();

    /**
     * Gets message text (this text is parsed for attachments)
     *
     * @return
     */
    public String getText() {
        return text;
    }

    /**
     * Sets message text
     *
     * @param text
     */
    public void setText(String text) {
        this.text = Utils.nvl(text, "");
    }

    /**
     * Gets message parts
     *
     * @return
     */
    @JsonIgnore
    public List<MessagePart> getParts() {
        return parts;
    }

    /**
     * Sets message parts
     *
     * @param parts
     */
    public void setParts(List<MessagePart> parts) {
        this.parts = parts;
    }

    /**
     * Gets list of video attachments
     *
     * @return
     */
    public List<VideoAttachment> getVideoAttachments() {
        return videoAttachments;
    }

    /**
     * Sets list of video attachments
     *
     * @param videoAttachments
     */
    public void setVideoAttachments(List<VideoAttachment> videoAttachments) {
        this.videoAttachments = videoAttachments;
    }

    /**
     * Gets list of image attachments
     *
     * @return
     */
    public List<ImageAttachment> getImageAttachments() {
        return imageAttachments;
    }

    /**
     * Sets list of image attachments
     *
     * @param imageAttachments
     */
    public void setImageAttachments(List<ImageAttachment> imageAttachments) {
        this.imageAttachments = imageAttachments;
    }

    /**
     * Gets list of audio attachments
     *
     * @return
     */
    public List<AudioAttachment> getAudioAttachments() {
        return audioAttachments;
    }

    /**
     * Sets list of audio attachments
     *
     * @param audioAttachments
     */
    public void setAudioAttachments(List<AudioAttachment> audioAttachments) {
        this.audioAttachments = audioAttachments;
    }
}

package ru.planeta.api.model;

public class DialogLastMessageObject {
    private long dialogId;
    private long lastMessageId;

    public DialogLastMessageObject() {

    }

    public DialogLastMessageObject(long dialogId, long lastMessageId) {
        this.dialogId = dialogId;
        this.lastMessageId = lastMessageId;
    }

    public long getDialogId() {
        return dialogId;
    }

    public void setDialogId(long dialogId) {
        this.dialogId = dialogId;
    }

    public long getLastMessageId() {
        return lastMessageId;
    }

    public void setLastMessageId(long lastMessageId) {
        this.lastMessageId = lastMessageId;
    }
}

package ru.planeta.api.helper;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import ru.planeta.api.utils.PaymentUtils;
import ru.planeta.commons.web.WebUtils;
import ru.planeta.model.common.InvestingOrderInfo;
import ru.planeta.model.common.TopayTransaction;
import ru.planeta.model.common.campaign.Campaign;
import ru.planeta.model.enums.ProjectType;
import ru.planeta.model.profile.Profile;

import javax.annotation.Nonnull;
import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: a.volkov
 * Date: 06.05.14
 * Time: 13:53
 */

@Service
public class ProjectServiceImpl implements ProjectService {

    private static final String TRANSACTION_PARAM = "transactionId";
    private static final String ORDER_PARAM = "orderId";
    private static final String SIGN_PARAM = "s";
    private static final String PG_REDIRECT_URI = "/redirect.html";
    private static final String PAYMENT_SUCCESS_HTML = "/payment-success.html";
    private static final String PAYMENT_FAIL_HTML = "/payment-fail.html";

    @Value("${application.host}")
    private String planetaHost;

    @Value("${projects.application.host}")
    private String projectsHost;

    @Value("${shop.application.host}")
    private String shopHost;

    @Value("${admin.application.host}")
    private String adminHost;

    @Value("${tv.application.host}")
    private String tvHost;

    @Value("${widgets.application.host}")
    private String widgetsHost;

    @Value("${payment.gate2.host}")
    private String paymentGateHost;

    @Value("${notification.server.url}")
    private String notificationServiceHost;

    @Value("${im.server.url}")
    private String imServiceHost;

    @Value("${statistics.application.host}")
    private String statisticHost;

    @Value("${static.host}")
    private String staticAppHost;

    @Value("${school.application.host}")
    private String schoolHost;
    
    @Value("${biblio.application.host}")
    private String biblioHost;

    @Value("${shortlink.application.host}")
    private String shortLinkHost;

    @Value("${static.base.url}")
    private String staticBaseUrl;

    @Value("${promo.application.host}")
    private String promoHost;

    @Value("${dp.application.host}")
    private String dpHost;

    private final Map<ProjectType, String> hosts = new HashMap<>();

    @PostConstruct
    private void init() {
        hosts.put(ProjectType.MAIN, planetaHost);
        hosts.put(ProjectType.ADMIN, adminHost);
        hosts.put(ProjectType.PAYMENT_GATE, paymentGateHost);
        hosts.put(ProjectType.SHOP, shopHost);
        hosts.put(ProjectType.TV, tvHost);
        hosts.put(ProjectType.WIDGETS, widgetsHost);
        hosts.put(ProjectType.STATISTIC_SERVICE, statisticHost);
        hosts.put(ProjectType.NOTIFICATION_SERVICE, notificationServiceHost);
        hosts.put(ProjectType.STATIC, staticAppHost);
        hosts.put(ProjectType.SCHOOL, schoolHost);
        hosts.put(ProjectType.INVEST, planetaHost);
        hosts.put(ProjectType.INVEST_ALL_ALLOWED, planetaHost);
        hosts.put(ProjectType.BIBLIO, biblioHost);
        hosts.put(ProjectType.LINK, shortLinkHost);
        hosts.put(ProjectType.PROMO, promoHost);
        hosts.put(ProjectType.DIGITAL_PRODUCT, dpHost);
        hosts.put(ProjectType.DELIVERY, planetaHost);
    }

    @Override
    public String getPaymentFailUrl(ProjectType projectType) {
        return getUrl(projectType, "/payment-fail-simple.html");
    }

    @Override
    public String getPaymentFailUrl(ProjectType projectType, long transactionId) {
        return getUrl(projectType, PAYMENT_FAIL_HTML, new WebUtils.Parameters().add(TRANSACTION_PARAM, transactionId));
    }

    @Override
    public String getPaymentFailUrl(@Nonnull TopayTransaction transaction) {
        return getPaymentFailUrl(transaction.getProjectType(), transaction.getTransactionId());
    }

    @Override
    public String getPaymentSuccessUrl(ProjectType projectType, long transactionId) {
        return getUrl(projectType, PAYMENT_SUCCESS_HTML, new WebUtils.Parameters().add(TRANSACTION_PARAM, transactionId));
    }

    @Override
    public String getPaymentSuccessUrl(TopayTransaction transaction) {
        return getPaymentSuccessUrl(transaction.getProjectType(), transaction.getTransactionId());
    }

    @Override
    public String getPaymentSuccessUrlByOrder(ProjectType projectType, long orderId) {
        return getUrl(projectType, PAYMENT_SUCCESS_HTML, new WebUtils.Parameters().add(ORDER_PARAM, orderId));
    }

    @Override
    public String getPaymentGateRedirectUrl(TopayTransaction transaction) {
        WebUtils.Parameters params = new WebUtils.Parameters()
                .add(TRANSACTION_PARAM, transaction.getTransactionId())
                .add(SIGN_PARAM, PaymentUtils.sign(transaction));
        return getUrl(ProjectType.PAYMENT_GATE, PG_REDIRECT_URI, params);
    }

    @Override
    public String getHost(ProjectType projectType) {
        return hosts.get(projectType);
    }

    @Nonnull
    @Override
    public String getUrl(ProjectType projectType, @Nonnull String uri) {
        return getUrl(projectType) + (uri.startsWith("/") ? uri : ("/" + uri));
    }

    @Override
    public String getUrlHttp(ProjectType projectType, @Nonnull String uri) {
        return getUrl(projectType, false) + (uri.startsWith("/") ? uri : ("/" + uri));
    }

    @Override
    public String getUrl(ProjectType projectType, String uri, WebUtils.Parameters params) {
        return params.createUrl(getUrl(projectType, uri), false);
    }

    public String getUrl(ProjectType projectType, String uri, Map<String, String> params) {
        return new WebUtils.Parameters(params).createUrl(getUrl(projectType, uri), false);
    }
    
    @Override
    public String getCampaignUrl(@Nonnull Campaign campaign) {
        return getUrl(ProjectType.MAIN, "/campaigns/" + campaign.getWebCampaignAlias());
    }
    
    @Override
    public String getUserUrl(@Nonnull Profile profile) {
        return getUrl(ProjectType.MAIN, "/" + profile.getWebAlias());
    }
    
    @Override
    public String getInvestingOrderURL(@Nonnull InvestingOrderInfo investingOrderInfo) {
        return new WebUtils.Parameters().add("orderId", investingOrderInfo.getInvestingOrderInfoId()).createUrl(getUrl(ProjectType.ADMIN, "/admin/investing-order-info.html"), false);
    }
    
    @Override
    public String getInvoicePrintUrl(long transactionId) {
        return new WebUtils.Parameters().add("transactionId", transactionId).createUrl(getUrl(ProjectType.MAIN, "/welcome/invoice/pdf.html"), false);
    }
    
    @Override
    public String getBiblioSertPrintUrl(long transactionId) {
        return new WebUtils.Parameters().add("transactionId", transactionId).createUrl(getUrl(ProjectType.MAIN, "/api/welcome/biblio/cert-pdf.html"), false);
    }

    @Override
    @Nonnull
    public String getUrl(ProjectType projectType) {
//        return getUrl(projectType, false);
        return getUrl(projectType, true);
    }


    @Override
    @Nonnull
    public String getUrl(ProjectType projectType, boolean secured) {
        String host = getHost(projectType);
        if (host == null) {
            throw new IllegalStateException("host isn't configured for project: " + projectType);
        }
        return WebUtils.appendProtocolIfNecessary(host, secured);
    }

    @Override
    public String getStaticBaseUrl(String param) {
        return getUrl(ProjectType.STATIC) + param + staticBaseUrl;
    }

    @Override
    public String getStaticJsUrl(String url) {
        return getStaticBaseUrl("/res") + url;
    }
}

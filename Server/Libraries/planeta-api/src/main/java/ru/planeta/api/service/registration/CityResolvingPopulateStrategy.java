package ru.planeta.api.service.registration;

import org.apache.commons.lang3.StringUtils;
import ru.planeta.api.geo.GeoResolverWebService;
import ru.planeta.model.profile.Profile;
import ru.planeta.model.profile.location.City;

public abstract class CityResolvingPopulateStrategy implements PopulateUserRegistrationInfoStrategy {

    private final String ip;
    private final GeoResolverWebService geoResolverWebService;

    CityResolvingPopulateStrategy(GeoResolverWebService geoResolverWebService, String ip) {
        this.geoResolverWebService = geoResolverWebService;
        this.ip = ip;
    }

    @Override
    public void delayedProfileUpdate(Profile profile) {
        try {
            if (StringUtils.isNotBlank(ip)) {
                City city = geoResolverWebService.resolveCity(ip);
                if (city != null) {
                    profile.setCityId(city.getObjectId());
                    profile.setCountryId(city.getCountryId());
                }
            }
        } catch (Exception ignored) {
        }
    }
}

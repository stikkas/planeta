package ru.planeta.api.model.stat;

import ru.planeta.model.enums.OrderObjectType;
import ru.planeta.model.stat.StatPurchaseComposite;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * @author is.kuzminov
 *         Date: 17.08.12
 *         Time: 11:28
 */
public class PurchaseReport {
    private int regCount;
    private int ticketsCountDiff;
    private int sharesCountDiff;
    private int productsCountDiff;
    private int regCountDiff;
    private int biblioPurchasesCountDiff;
    private BigDecimal ticketsAmountDiff = BigDecimal.ZERO;
    private BigDecimal sharesAmountDiff = BigDecimal.ZERO;
    private BigDecimal productsAmountDiff = BigDecimal.ZERO;
    private BigDecimal biblioPurchasesAmountDiff = BigDecimal.ZERO;
    private List<StatPurchaseComposite> shares;
    private List<StatPurchaseComposite> tickets;
    private List<StatPurchaseComposite> products;
    private List<StatPurchaseComposite> biblioPurchases;
    //
    private BigDecimal totalAmountDiff = BigDecimal.ZERO;

    public PurchaseReport() {
    }

    public PurchaseReport(StatPurchaseComposite seoPurchase) {
        shares = new ArrayList<StatPurchaseComposite>();
        tickets = new ArrayList<StatPurchaseComposite>();
        products = new ArrayList<StatPurchaseComposite>();
        biblioPurchases = new ArrayList<StatPurchaseComposite>();
        this.addPurchase(seoPurchase);
        this.regCount = seoPurchase.getRegCount();
    }

    public BigDecimal getTicketsAmount() {
        return getAmount(OrderObjectType.TICKET);
    }

    public BigDecimal getSharesAmount() {
        return getAmount(OrderObjectType.SHARE);
    }

    public BigDecimal getProductsAmount() {
        return getAmount(OrderObjectType.PRODUCT);
    }

    public BigDecimal getBiblioPurchasesAmount() {
        return getAmount(OrderObjectType.BIBLIO);
    }

    public BigDecimal getTotalAmount() {
        return getAmount(OrderObjectType.PRODUCT).add(getAmount(OrderObjectType.SHARE)).add(getAmount(OrderObjectType.TICKET)).add(getAmount(OrderObjectType.BIBLIO));
    }

    public int getRegCount() {
        return regCount;
    }

    public void setRegCount(int regCount) {
        this.regCount = regCount;
    }

    public List<StatPurchaseComposite> getShares() {
        return shares;
    }

    public List<StatPurchaseComposite> getProducts() {
        return products;
    }

    public List<StatPurchaseComposite> getBiblioPurchases() {
        return biblioPurchases;
    }

    public int getTicketsSumOfObjectsCount() {
        return getSumOfObjectsCount(OrderObjectType.TICKET);
    }

    public int getSharesSumOfObjectsCount() {
        return getSumOfObjectsCount(OrderObjectType.SHARE);
    }

    public int getProductsSumOfObjectsCount() {
        return getSumOfObjectsCount(OrderObjectType.PRODUCT);
    }

    public int getTicketsCount() {
        return getCount(OrderObjectType.TICKET);
    }

    public int getSharesCount() {
        return getCount(OrderObjectType.SHARE);
    }

    public int getProductsCount() {
        return getCount(OrderObjectType.PRODUCT);
    }

    public int getBiblioPurchasesCount() {
        return getCount(OrderObjectType.BIBLIO);
    }

    public int getSize() {
        return shares.size() + tickets.size() + products.size() + biblioPurchases.size();
    }

    public void addPurchase(StatPurchaseComposite purchase) {
        switch (purchase.getObjectType()) {
            case SHARE:
                shares.add(purchase);
                break;
            case TICKET:
                tickets.add(purchase);
                break;
            case PRODUCT:
                products.add(purchase);
                break;
            case BIBLIO:
                biblioPurchases.add(purchase);
                break;
        }
    }

    private BigDecimal getAmount(List<StatPurchaseComposite> seoPurchases) {
        if (seoPurchases == null || seoPurchases.isEmpty()) {
            return BigDecimal.ZERO;
        }
        BigDecimal amount = BigDecimal.ZERO;
        for (StatPurchaseComposite purchase : seoPurchases) {
            amount = amount.add(purchase.getAmount());
        }
        return amount;
    }

    private int getSumOfObjectsCount(List<StatPurchaseComposite> purchases) {
        if (purchases == null || purchases.isEmpty()) {
            return 0;
        }
        int count = 0;
        for (StatPurchaseComposite purchase : purchases) {
            count += purchase.getSumOfObjectsCount();
        }
        return count;
    }

    private int getCount(List<StatPurchaseComposite> purchases) {
        if (purchases == null || purchases.isEmpty()) {
            return 0;
        }
        int count = 0;
        for (StatPurchaseComposite purchase : purchases) {
            count += purchase.getCount();
        }
        return count;
    }

    private int getSumOfObjectsCount(OrderObjectType objectType) {
        switch (objectType) {
            case SHARE:
                return getSumOfObjectsCount(shares);
            case TICKET:
                return getSumOfObjectsCount(tickets);
            case PRODUCT:
                return getSumOfObjectsCount(products);
            case BIBLIO:
                return getSumOfObjectsCount(biblioPurchases);
            default:
                return 0;
        }
    }

    private int getCount(OrderObjectType objectType) {
        switch (objectType) {
            case SHARE:
                return getCount(shares);
            case TICKET:
                return getCount(tickets);
            case PRODUCT:
                return getCount(products);
            case BIBLIO:
                return getCount(biblioPurchases);
            default:
                return 0;
        }
    }

    private BigDecimal getAmount(OrderObjectType objectType) {
        switch (objectType) {
            case SHARE:
                return getAmount(shares);
            case TICKET:
                return getAmount(tickets);
            case PRODUCT:
                return getAmount(products);
            case BIBLIO:
                return getAmount(biblioPurchases);
            default:
                return BigDecimal.ZERO;
        }
    }


    public void setDiff(PurchaseReport report) {
        this.ticketsCountDiff = this.getTicketsCount() - report.getTicketsCount();
        this.ticketsAmountDiff = this.getTicketsAmount().add(report.getTicketsAmount().negate());
        this.sharesCountDiff = this.getSharesCount() - report.getSharesCount();
        this.sharesAmountDiff = this.getSharesAmount().add(report.getSharesAmount().negate());
        this.productsCountDiff = this.getProductsCount() - report.getProductsCount();
        this.productsAmountDiff = this.getProductsAmount().add(report.getProductsAmount().negate());
        this.biblioPurchasesCountDiff = this.getBiblioPurchasesCount() - report.getBiblioPurchasesCount();
        this.biblioPurchasesAmountDiff = this.getBiblioPurchasesAmount().add(report.getBiblioPurchasesAmount().negate());
        this.regCountDiff = this.regCount - report.getRegCount();
        this.totalAmountDiff = this.totalAmountDiff.add(this.ticketsAmountDiff).add(sharesAmountDiff).add(productsAmountDiff).add(biblioPurchasesAmountDiff);
    }

    public int getTicketsCountDiff() {
        return ticketsCountDiff;
    }

    public void setTicketsCountDiff(int ticketsCountDiff) {
        this.ticketsCountDiff = ticketsCountDiff;
    }

    public int getSharesCountDiff() {
        return sharesCountDiff;
    }

    public void setSharesCountDiff(int sharesCountDiff) {
        this.sharesCountDiff = sharesCountDiff;
    }

    public int getProductsCountDiff() {
        return productsCountDiff;
    }

    public void setProductsCountDiff(int productsCountDiff) {
        this.productsCountDiff = productsCountDiff;
    }

    public int getBiblioPurchasesCountDiff() {
        return biblioPurchasesCountDiff;
    }

    public void setBiblioPurchasesCountDiff(int biblioPurchasesCountDiff) {
        this.biblioPurchasesCountDiff = biblioPurchasesCountDiff;
    }

    public BigDecimal getTicketsAmountDiff() {
        return ticketsAmountDiff;
    }

    public void setTicketsAmountDiff(BigDecimal ticketsAmountDiff) {
        this.ticketsAmountDiff = ticketsAmountDiff;
    }

    public BigDecimal getSharesAmountDiff() {
        return sharesAmountDiff;
    }

    public void setSharesAmountDiff(BigDecimal sharesAmountDiff) {
        this.sharesAmountDiff = sharesAmountDiff;
    }

    public BigDecimal getProductsAmountDiff() {
        return productsAmountDiff;
    }

    public void setProductsAmountDiff(BigDecimal productsAmountDiff) {
        this.productsAmountDiff = productsAmountDiff;
    }

    public BigDecimal getBiblioPurchasesAmountDiff() {
        return biblioPurchasesAmountDiff;
    }

    public void setBiblioPurchasesAmountDiff(BigDecimal biblioPurchasesAmountDiff) {
        this.biblioPurchasesAmountDiff = biblioPurchasesAmountDiff;
    }

    public BigDecimal getTotalAmountDiff() {
        return totalAmountDiff;
    }

    public void setTotalAmountDiff(BigDecimal totalAmountDiff) {
        this.totalAmountDiff = totalAmountDiff;
    }

    public int getRegCountDiff() {
        return regCountDiff;
    }

    public void setRegCountDiff(int regCountDiff) {
        this.regCountDiff = regCountDiff;
    }
}

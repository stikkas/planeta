package ru.planeta.api.news;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonInclude;
import ru.planeta.commons.model.Gender;
import ru.planeta.model.common.campaign.Campaign;
import ru.planeta.model.news.ProfileNews;
import ru.planeta.model.profile.Post;
import ru.planeta.model.profile.Profile;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.MathContext;
import java.util.Date;

import static org.apache.commons.lang3.StringUtils.defaultString;

/**
 * Created with IntelliJ IDEA.
 * User: a.volkov
 * Date: 13.11.2014
 * Time: 20:03
 */
@JsonAutoDetect
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ProfileNewsDTO extends ProfileNews implements Serializable {
    //who
    private long authorProfileId;
    private String authorAlias;
    private Gender authorGender;
    private String authorName;
    private String authorImageUrl;
    //what
    private long postId;
    private String postName;
    private String headingText;
    private String text;
    private int commentsCount;
    //where
    private long campaignId;
    private boolean isCharity;
    private String webCampaignAlias;
    private String campaignName;
    private int campaignProgress;
    private String campaignImageUrl;
    private Boolean campaignSucceed;

    private Date notificationTime;

    public ProfileNewsDTO(ProfileNews news, Profile author) {
        super(news);

        authorProfileId = author.getProfileId();
        authorAlias = defaultString(author.getAlias());
        authorGender = author.getUserGender();
        authorName = author.getDisplayName();
        authorImageUrl = author.getSmallImageUrl();
    }

    public ProfileNewsDTO(ProfileNews news, Profile author, Campaign campaign) {
        this(news, author);
        if (campaign != null) {
            campaignId = campaign.getCampaignId();
            isCharity = campaign.isCharity();
            webCampaignAlias = campaign.getWebCampaignAlias();
            campaignName = campaign.getName();
            campaignImageUrl = campaign.getImageUrl();
            switch (news.getType()) {
                case CAMPAIGN_RESUMED:
                    if (campaign.getTargetAmount().compareTo(BigDecimal.ZERO) == 0) {
                        campaignProgress = 0;
                    } else {
                        campaignProgress = campaign.getCollectedAmount().divide(campaign.getTargetAmount(), MathContext.DECIMAL64).multiply(new BigDecimal(100)).intValue();
                    }
                    break;
                case CAMPAIGN_FINISHED_SUCCESS:
                case CAMPAIGN_FINISHED_FAIL:
                    if (campaign.getTargetAmount().compareTo(BigDecimal.ZERO) == 0) {
                        campaignProgress = campaign.getCollectedAmount().compareTo(BigDecimal.ZERO) == 0 ? 0 : 100;
                        campaignSucceed = campaign.getCollectedAmount().compareTo(BigDecimal.ZERO) > 0;
                    } else {
                        campaignProgress = campaign.getCollectedAmount().divide(campaign.getTargetAmount(), MathContext.DECIMAL64).multiply(new BigDecimal(100)).intValue();
                        campaignSucceed = campaignProgress >= 50;
                    }
                    break;
                case CAMPAIGN_REACHED_25:
                    campaignProgress = 25;
                    break;
                case CAMPAIGN_REACHED_50:
                    campaignProgress = 50;
                    break;
                case CAMPAIGN_REACHED_75:
                    campaignProgress = 75;
                    break;
                default:
                    campaignProgress = 0;
            }
        }
    }

    public Boolean getCampaignSucceed() {
        return campaignSucceed;
    }

    public ProfileNewsDTO(ProfileNews news, Profile author, Post post) {
        this(news, author);
        if (post != null) {
            postId = post.getId();
            postName = post.getTitle();
            headingText = post.getHeadingText();
            text = post.getPostText();
            commentsCount = post.getCommentsCount();
            notificationTime = post.getNotificationTime();
            if (getTimeAdded() == null) {
                setTimeAdded(post.getTimeAdded());
            }
        }
    }

    public ProfileNewsDTO(ProfileNews news, Profile author, Campaign campaign, Post post) {
        this(news, author, post);
        if (campaign != null) {
            campaignId = campaign.getCampaignId();
            webCampaignAlias = campaign.getWebCampaignAlias();
            campaignName = campaign.getName();
            campaignImageUrl = campaign.getImageUrl();
            isCharity = campaign.isCharity();
        }
    }


    public int getCampaignProgress() {
        return campaignProgress;
    }

    public String getCampaignImageUrl() {
        return campaignImageUrl;
    }

    public String getAuthorName() {
        return authorName;
    }

    public void setAuthorName(String authorName) {
        this.authorName = authorName;
    }

    public String getAuthorImageUrl() {
        return authorImageUrl;
    }

    public void setAuthorImageUrl(String authorImageUrl) {
        this.authorImageUrl = authorImageUrl;
    }

    public long getPostId() {
        return postId;
    }

    public void setPostId(long postId) {
        this.postId = postId;
    }

    public String getPostName() {
        return postName;
    }

    public void setPostName(String postName) {
        this.postName = postName;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public int getCommentsCount() {
        return commentsCount;
    }

    public void setCommentsCount(int commentsCount) {
        this.commentsCount = commentsCount;
    }

    public long getCampaignId() {
        return campaignId;
    }

    public void setCampaignId(long campaignId) {
        this.campaignId = campaignId;
    }

    public String getCampaignName() {
        return campaignName;
    }

    public void setCampaignName(String campaignName) {
        this.campaignName = campaignName;
    }

    public long getAuthorProfileId() {
        return authorProfileId;
    }

    public void setAuthorProfileId(long authorProfileId) {
        this.authorProfileId = authorProfileId;
    }

    public String getAuthorAlias() {
        return authorAlias;
    }

    public void setAuthorAlias(String authorAlias) {
        this.authorAlias = authorAlias;
    }

    public Gender getAuthorGender() {
        return authorGender;
    }

    public void setAuthorGender(Gender authorGender) {
        this.authorGender = authorGender;
    }

    public String getHeadingText() {
        return headingText;
    }

    public void setHeadingText(String headingText) {
        this.headingText = headingText;
    }

    public String getWebCampaignAlias() {
        return webCampaignAlias;
    }

    public void setWebCampaignAlias(String webCampaignAlias) {
        this.webCampaignAlias = webCampaignAlias;
    }

    public Date getNotificationTime() {
        return notificationTime;
    }

    public void setNotificationTime(Date notificationTime) {
        this.notificationTime = notificationTime;
    }

    public boolean isCharity() {
        return isCharity;
    }

    public void setCharity(boolean isCharity) {
        this.isCharity = isCharity;
    }
}

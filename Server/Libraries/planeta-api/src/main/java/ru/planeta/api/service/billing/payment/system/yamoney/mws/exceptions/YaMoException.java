package ru.planeta.api.service.billing.payment.system.yamoney.mws.exceptions;

/**
 * Base YandexMoney exception.<br>
 * Created by eshevchenko.
 */
public class YaMoException extends Exception {

    public YaMoException(Throwable cause) {
        super(cause);
    }

    public YaMoException() {
        super();
    }

    public YaMoException(String message, Object... args) {
        super(String.format(message, args));
    }
}

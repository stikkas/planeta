package ru.planeta.api.service.billing.payment.system.yamoney.mws.models;

import ru.planeta.api.service.billing.payment.system.yamoney.mws.models.enums.CurrencyCode;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by eshevchenko on 25.04.14.
 */
@XmlRootElement
public class ConfirmPaymentRequest extends BaseRequest {
    @XmlAttribute
    public Date requestDT;
    @XmlAttribute
    public long orderId;
    @XmlAttribute
    public BigDecimal amount;
    @XmlAttribute
    public CurrencyCode currency;

    public static ConfirmPaymentRequest create(long orderId, BigDecimal amount, CurrencyCode currency) {
        ConfirmPaymentRequest result = new ConfirmPaymentRequest();
        result.orderId = orderId;
        result.amount = amount;
        result.currency = currency;

        return result;
    }

}

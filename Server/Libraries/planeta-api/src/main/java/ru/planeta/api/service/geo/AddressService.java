package ru.planeta.api.service.geo;


import ru.planeta.model.common.Address;

/**
 * Service for working with addresses
 *
 * @author ds.kolyshev
 *         Date: 22.05.12
 */
public interface AddressService {

    /**
     * Returns address by {@code addressId}
     *
     * @param addressId address identifier
     * @return
     */
    Address getAddress(long addressId);

    /**
     * Updates address.
     *
     * @param address
     */
    void changeAddress(Address address);

    void saveAddress(Address address);

    Address getLastPostAddress(long buyerId);
}

package ru.planeta.api.model.biblio;

import com.fasterxml.jackson.annotation.JsonIgnore;
import ru.planeta.model.common.Identified;
import ru.planeta.model.profile.Identifier;

public class Advertise implements Identified, Identifier {

    /**
     * Идентификатор
     */
    @JsonIgnore
    private long id;

    /**
     * Заголовок
     */
    private String title;

    /**
     * Краткое описание
     */
    private String annonce;

    /**
     * Подпись для ссылки на видео
     */
    private String videoTitle;

    /**
     * Подпись к ссылке на полное объявление
     */
    private String textTitle;

    /**
     * Ссылка на видео
     */
    private String videoLink;

    /**
     * Ссылка на текст объявления
     */
    private String textLink;

    /**
     * Ссылка к изображению
     */
    private String imageUrl;

    @Override
    public long getId() {
        return id;
    }

    @Override
    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAnnonce() {
        return annonce;
    }

    public void setAnnonce(String annonce) {
        this.annonce = annonce;
    }

    public String getVideoTitle() {
        return videoTitle;
    }

    public void setVideoTitle(String videoTitle) {
        this.videoTitle = videoTitle;
    }

    public String getTextTitle() {
        return textTitle;
    }

    public void setTextTitle(String textTitle) {
        this.textTitle = textTitle;
    }

    public String getVideoLink() {
        return videoLink;
    }

    public void setVideoLink(String videoLink) {
        this.videoLink = videoLink;
    }

    public String getTextLink() {
        return textLink;
    }

    public void setTextLink(String textLink) {
        this.textLink = textLink;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }
}

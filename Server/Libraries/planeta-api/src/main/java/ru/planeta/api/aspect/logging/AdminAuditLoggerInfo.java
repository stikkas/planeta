package ru.planeta.api.aspect.logging;

import org.apache.commons.collections4.keyvalue.AbstractKeyValue;

/**
 * Created with IntelliJ IDEA.
 * User: eshevchenko
 */
public class AdminAuditLoggerInfo extends AbstractKeyValue {

    public AdminAuditLoggerInfo(Object adminId, Object userId) {
        super(adminId, userId);
    }

    public long getAdminId() {
        return getLong(getKey());
    }

    public long getUserId() {
        return getLong(getValue());
    }

    private static long getLong(Object obj) {
        return (obj == null) ? 0L : (Long) obj;
    }
}

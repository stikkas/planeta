package ru.planeta.api.service.billing.payment.system.yamoney.mws.models;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import java.math.BigDecimal;

/**
 * Created by eshevchenko on 25.04.14.
 */
@XmlRootElement
public class RepeatCardPaymentRequest extends BaseRequest {
    @XmlAttribute
    public long clientOrderId;
    @XmlAttribute
    public long invoiceId;
    @XmlAttribute
    public BigDecimal amount;
    @XmlAttribute
    public String orderNumber;
    @XmlAttribute
    public String cvv;

    public static RepeatCardPaymentRequest create(long clientOrderId, long invoiceId, BigDecimal amount, String orderNumber, String cvv) {
        RepeatCardPaymentRequest result = new RepeatCardPaymentRequest();
        result.clientOrderId = clientOrderId;
        result.invoiceId = invoiceId;
        result.amount = amount;
        result.orderNumber = orderNumber;
        result.cvv = cvv;

        return result;
    }
}

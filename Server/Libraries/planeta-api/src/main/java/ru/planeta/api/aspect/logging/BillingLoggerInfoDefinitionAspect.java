package ru.planeta.api.aspect.logging;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.aspectj.lang.ProceedingJoinPoint;
import org.springframework.beans.factory.annotation.Value;
import ru.planeta.commons.web.CookieUtils;
import ru.planeta.model.common.Order;
import ru.planeta.model.common.TopayTransaction;
import ru.planeta.model.stat.log.LoggerType;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Database logger type info definition aspect.<br>
 * Sets specific logger info to {@link DBLoggerTypeInfoHolder}.<br>
 * User: eshevchenko
 */
public class BillingLoggerInfoDefinitionAspect {

    private static final int COOKIE_LIFE_TIME = 60 * 60 * 24; // 24 hours

    private String cookieDomain;

    @Value("${application.host}")
    public void setCookieDomain(String cookieDomain) {
        this.cookieDomain = StringUtils.startsWith(cookieDomain, "localhost") ? "" : "." + cookieDomain;
    }

    public void afterPaymentModification(Object payment) {
        final long paymentId = (payment != null) ? ((TopayTransaction) payment).getTransactionId() : 0L;
        DBLoggerTypeInfoHolder.setLoggerTypeInfo(LoggerType.PAYMENT, paymentId);
    }

    public void afterOrderModification(Object order) {
        final long orderId = (order != null) ? ((Order) order).getOrderId() : 0L;
        DBLoggerTypeInfoHolder.setLoggerTypeInfo(LoggerType.ORDER, orderId);
    }

    public Object aroundRequest(ProceedingJoinPoint pjp, BillingLoggableRequest billingLoggableRequest) throws Throwable {
        if (!billingLoggableRequest.initial()) {
            restoreBillingLoggerInfoFromCookies(LoggerType.ORDER);
            restoreBillingLoggerInfoFromCookies(LoggerType.PAYMENT);
        }

        Object result = pjp.proceed();

        storeBillingLoggerInfoToCookies(LoggerType.ORDER);
        storeBillingLoggerInfoToCookies(LoggerType.PAYMENT);

        return result;
    }

    private void storeBillingLoggerInfoToCookies(LoggerType loggerType) {
        HttpServletResponse response = DBLoggerTypeInfoHolder.getLoggableResponse();
        if (response != null) {
            Long info = DBLoggerTypeInfoHolder.getInfoByLoggerType(Long.class, loggerType);
            String value = (info != null) ? info.toString() : "";
            CookieUtils.setCookie(response, getCookieName(loggerType), value, COOKIE_LIFE_TIME, cookieDomain);
        }
    }

    private void restoreBillingLoggerInfoFromCookies(LoggerType loggerType) {
        HttpServletRequest request = DBLoggerTypeInfoHolder.getLoggableRequest();
        if (request != null) {
            String info = CookieUtils.getCookieValue(request.getCookies(), getCookieName(loggerType), "");
            DBLoggerTypeInfoHolder.setLoggerTypeInfo(loggerType, NumberUtils.toLong(info, 0L));
        }
    }

    private static String getCookieName(LoggerType loggerType) {
        return "logger_" + loggerType.name().toLowerCase();
    }
}

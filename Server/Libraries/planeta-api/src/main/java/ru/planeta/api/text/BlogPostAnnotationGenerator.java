package ru.planeta.api.text;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.ccil.cowan.tagsoup.Parser;
import org.xml.sax.InputSource;
import org.xml.sax.helpers.DefaultHandler;

import java.io.StringReader;

/**
 * Created with IntelliJ IDEA.
 * User: a.volkov
 * Date: 18.04.13
 * Time: 12:11
 */
public class BlogPostAnnotationGenerator extends DefaultHandler {

    private static final Logger log = Logger.getLogger(BlogPostAnnotationGenerator.class);

    public static String generate(final String html) {
        if (StringUtils.isBlank(html)) {
            return StringUtils.EMPTY;
        }
        String annotation;
        final Parser parser = new Parser();
        final BlogPostContentHandler handler = new BlogPostContentHandler();
        parser.setContentHandler(handler);
        try {
            parser.parse(new InputSource(new StringReader(html)));
            annotation = handler.getText();
        } catch (Exception e) {
            log.error("can't parse blog post text html", e);
            annotation = html;
        }

        return annotation;
    }


}

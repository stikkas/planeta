package ru.planeta.api.service.configurations;

import ru.planeta.api.exceptions.NotFoundException;
import ru.planeta.api.model.json.UserCallbackAvailabilityCondition;
import ru.planeta.api.model.json.VipClubJoinCondition;
import ru.planeta.model.common.Configuration;

import javax.annotation.Nullable;
import java.util.List;
import java.util.Set;

/**
 * Configuration properties service
 *
 * @author ds.kolyshev
 *         Date: 17.05.12
 */
public interface ConfigurationService {
    /**
     * Gets configuration property by key
     *
     */
    @Nullable
    Configuration getConfigurationByKey(String key);

    /**
     * Get configuration properties list
     *
     */
    List<Configuration> getConfigurationList(String query, int offset, int limit);

    List<Configuration> getConfigurationList(int offset, int limit);

    /**
     * Saves configuration property
     *
     */
    void saveConfiguration(Configuration configuration);

    /**
     * Deletes configuration property
     *
     */
    void deleteConfigurationByKey(String key);

    /**
     * Gets campaign's success threshold configuration value which has no targetAmount
     *
     */
    int getCampaignSuccessThreshold();

    /**
     * Returns promo campaigns identifiers
     *
     */
    List<Long> getPromoCampaignIds();

    List<Long> getIds(String configKey);

    Set<Long> getIdsSet(String configKey);

    @Nullable
    String getStringConfig(String configKey) throws NotFoundException;

    <T> List<T> getJsonArrayConfig(Class<T> elementClass, String key);

    void saveConfigAsJson(Object object, String configurationKey) throws NotFoundException;

    void saveStrValue(String strValue, String configurationKey) throws NotFoundException;

    void setPromoCampaignIds(String idList);

    List<Long> getShopFundsIds();

    List<String> getRelatedCampaignTagMnemonics();

    String getFundingRulesCustomHtml() throws NotFoundException;

    long getPromoNewsSourceProfileId();

    List<Long> getPromoNewsSliderIds();

    List<String> getCampaignWelcomeLowBanner() throws NotFoundException;

    String getPartnersWelcome() throws NotFoundException;

    List<Long> getPromoAboutUsCampaignIds();

    void setPromoAboutUsCampaignIds(String idList);

    List<Long> getCharityPromoCampaignIds();

    List<Long> getCharityRecentWebinarsIds();

    List<Long> getCharityTopCampaignIds();

    List<Long> getPromoProfilesIdsOfBroadcasts();

    void setCharityRecentWebinarsIds(String idList);

    void setCharityPromoCampaignIds(String idList);

    void setCharityTopCampaignIds(String idList);

    void setPromoProfilesIdsOfBroadcasts(String profileIdsList);

    int getStartupSeminarId();

    boolean needNotifyVipClubMember();

    VipClubJoinCondition getVipClubJoinCondition();

    UserCallbackAvailabilityCondition getCallbackAvailabilityCondition();

    int getNewsNotificationDaysLimit();

    int getPaymentErrorsLargeSum();

    Set<Long> getCommercialCampaigns();

    Set<Long> getCommercialShares();
}

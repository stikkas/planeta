package ru.planeta.api.service.billing.payment.system.yamoney.mws.exceptions;

/**
 * Describes MWS response parsing exception.<br>
 * Created by eshevchenko.
 */
public class ResponseParsingException extends RuntimeException {

    public ResponseParsingException(Exception e) {
        super(e);
    }
}

package ru.planeta.api.service.billing;

import ru.planeta.api.exceptions.NotFoundException;
import ru.planeta.api.exceptions.PaymentException;
import ru.planeta.api.exceptions.PermissionException;

import java.math.BigDecimal;

/**
 * Gift service encapsulates Planeta's gift logic.<br>
 * Created by eshevchenko on 25.02.14.
 */
public interface GiftService {
    /**
     * Gifts money to user by creating and processing PLANETA-payment.<br/>
     * After gifting sends to user email and notification.
     *
     * @param clientId  client identifier, should have Planeta's ADMIN-permission;
     * @param profileId profile identifier;
     * @param amount    gift amount;
     * @param message   message for email and notification formatting;
     * @throws PaymentException
     * @throws PermissionException
     * @throws NotFoundException
     */
    void giftMoney(long clientId, long profileId, BigDecimal amount, String message) throws PaymentException, PermissionException, NotFoundException;
}

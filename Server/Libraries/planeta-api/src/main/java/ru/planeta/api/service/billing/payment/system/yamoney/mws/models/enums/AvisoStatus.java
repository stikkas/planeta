package ru.planeta.api.service.billing.payment.system.yamoney.mws.models.enums;

import javax.xml.bind.annotation.XmlEnumValue;

/**
 * Yandex.Money payment notification statuses.
 */
public enum AvisoStatus {
    @XmlEnumValue(value = "-1")
    NOT_CREATED_YET,
    @XmlEnumValue(value = "81")
    EXPECT_SENDING,
    @XmlEnumValue(value = "100")
    DELIVERED_BUT_CONTRACTOR_REJECT,
    @XmlEnumValue(value = "103")
    DELIVERY_REJECTED,
    @XmlEnumValue(value = "1000")
    SUCCESSFULLY_NOTIFIED,
    @XmlEnumValue(value = "1010")
    NOT_DELIVERED_BUT_SUCCESSFULLY_PROCESSED,
    @XmlEnumValue(value = "1020")
    DELIVERED_AND_PAYMENT_RETURNED,
    @XmlEnumValue(value = "1021")
    DELIVERED_AND_PAYMENT_RETURNED1,
    @XmlEnumValue(value = "1022")
    DELIVERED_AND_PAYMENT_PARTIAL_RETURNED;
}

package ru.planeta.api.text;

/**
 * Message part type
 */
public enum MessagePartType {

    TEXT,
    LINK
}

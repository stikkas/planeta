package ru.planeta.api.remote;

import ru.planeta.api.exceptions.NotFoundException;
import ru.planeta.api.exceptions.PermissionException;
import ru.planeta.api.model.DialogLastMessageObject;
import ru.planeta.model.msg.DialogInfo;
import ru.planeta.api.model.json.DialogMessageInfo;
import ru.planeta.api.model.json.DialogShortInfo;
import ru.planeta.model.msg.DialogMessage;

import java.util.List;

/**
 * Service for working with dialogs
 *
 * @author ameshkov
 */
public interface DialogService {

    /**
     * Selects user dialogs list.
     *
     * @param profileId profile id
     * @param offset    offset
     * @param limit     limit
     * @return list of {@link DialogInfo}s
     */
    List<DialogInfo> getUserDialogs(long profileId, int offset, int limit);

    List<DialogShortInfo> getUserDialogs(long profileId, String query, int offset, int limit);

    /**
     * Selects list of user's dialogs with unread messages
     *
     * @param profileId profile id
     * @param offset    offset
     * @param limit     limit
     * @return list of {@link DialogInfo}s
     */
    List<DialogInfo> getUserDialogsWithUnread(long profileId, int offset, int limit);

    /**
     * Gets dialog info by identifier
     *
     * @param profileId profile id
     * @param dialogId  dialog id
     * @return {@link DialogInfo} object
     * @throws NotFoundException
     */
    DialogInfo getDialog(long profileId, long dialogId) throws NotFoundException;


    /**
     * Gets dialog info for the dialog between specified users.
     *
     * @param profileId profile id
     * @param companionProfileId companion profile id
     * @return {@link DialogInfo} object
     */
    DialogInfo getUserDialog(long profileId, long companionProfileId);



    /**
     * Sends a message to the list of specified users.
     * If dialog between them does not exist yet -- creates new dialog.
     * <p/>
     * <p>This method ignores invalid user ids in <code>companionProfileIds</code> array</p>
     *
     * @param profileId           profile identifier of user
     * @param companionProfileIds array that contains profile identifiers of recipients
     * @param text                message to be sent
     * @return list of {@link DialogMessageInfo}s
     * @throws PermissionException      if user with specified profileId doesn't exist
     * @throws IllegalArgumentException if provided text is empty
     */
    List<DialogMessageInfo> sendMessage(long profileId, long[] companionProfileIds, String text) throws PermissionException, IllegalArgumentException, NotFoundException;

    /**
     * Sends new dialog message
     *
     * @param dialogMessage dialog message
     * @return Created dialog message
     * @throws IllegalArgumentException Thrown if message text is empty
     */
    DialogMessageInfo sendDialogMessage(DialogMessage dialogMessage) throws PermissionException, NotFoundException;

    /**
     * Gets dialog messages (sorted by timeAdded DESC).
     * Also it updates UserDialog object (sets unreadMessagesCount to 0 and firstUnreadMessageIs to 0)
     *
     * @param profileId Id of the user that requests messages
     * @param dialogId  dialog id
     * @param offset    offset
     * @param limit     limit
     * @return  list of {@link DialogMessageInfo}s
     */
    List<DialogMessageInfo> getLastMessages(long profileId, long dialogId, int offset, int limit) throws NotFoundException;

    /**
     * Gets list of dialog messages starting from "messageId" (exclusive)
     * Also it updates UserDialog object (sets unreadMessagesCount to 0 and firstUnreadMessageIs to 0)
     *
     * @param profileId      Id of the user that requests messages
     * @param dialogId       dialog id
     * @param startMessageId id of first dialog message
     * @return all records with specified parameters
     */
    List<DialogMessageInfo> getMessages(long profileId, long dialogId, long startMessageId) throws NotFoundException;



    /**
     * Gets all last message for all dialogs in list
     * (for each dialog call method getLastMessages)
     *
     * @param profileId                Id of the user that requests messages
     * @param dialogLastMessageObjects list of pairs dialogId & messageId
     * @return all new message for dialogs from list
     */
    List<DialogMessageInfo> getLastMessagesForSeveralDialogs(long profileId, List<DialogLastMessageObject> dialogLastMessageObjects) throws NotFoundException;

    /**
     * Remove a message from a dialog
     *
     * @param profileId - id of the user whose deletes message
     * @param dialogId  - dialog id
     * @param messageId - id of the message to deleteByProfileId
     * @throws NotFoundException
     * @throws PermissionException
     */
    void removeDialogMessage(long profileId, long dialogId, long messageId) throws NotFoundException, PermissionException;

    /**
     * Returns an array of dialogs ids which have new messages
     *
     * @param profileId - profile identifier of user
     */
}

package ru.planeta.api.service.admin;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.log4j.Logger;
import org.sphx.api.SphinxException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.planeta.api.BaseSearchableService;
import ru.planeta.api.aspect.transaction.NonTransactional;
import ru.planeta.api.exceptions.AlreadyExistException;
import ru.planeta.api.exceptions.MessageCode;
import ru.planeta.api.exceptions.NotFoundException;
import ru.planeta.api.exceptions.PermissionException;
import ru.planeta.api.search.SearchResult;
import ru.planeta.api.service.profile.PasswordEncoder;
import ru.planeta.api.service.profile.ProfileService;
import ru.planeta.api.service.registration.RegistrationService;
import ru.planeta.api.utils.ValidateUtils;
import ru.planeta.dao.commondb.ProfileBalanceDAO;
import ru.planeta.dao.commondb.SuperAdminUtilsDAO;
import ru.planeta.dao.commondb.UserPrivateInfoDAO;
import ru.planeta.dao.maildb.MailUserDAO;
import ru.planeta.dao.profiledb.ProfileForAdminsDAO;
import ru.planeta.model.common.UserPrivateInfo;
import ru.planeta.model.enums.ProfileStatus;
import ru.planeta.model.enums.ProfileType;
import ru.planeta.model.enums.UserStatus;
import ru.planeta.model.profile.Profile;
import ru.planeta.model.profile.ProfileForAdminsWithEmail;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.EnumSet;
import java.util.List;

/**
 * author: ds.kolyshev
 * Date: 17.11.11
 */
@Service
public class AdminServiceImpl extends BaseSearchableService implements AdminService {

    private final SuperAdminUtilsDAO superAdminUtilsDAO;
    private final ProfileService profileService;
    private final RegistrationService registrationService;
    private final PasswordEncoder passwordEncoder;
    private final ProfileBalanceDAO profileBalanceDAO;
    private final MailUserDAO mailUserDAO;
    private final UserPrivateInfoDAO userPrivateInfoDAO;
    private final ProfileForAdminsDAO profileForAdminsDAO;

    private static final long BIG_USER_NUMBER = 100500;

    private static final Logger log = Logger.getLogger(AdminServiceImpl.class);

    @Autowired
    public AdminServiceImpl(SuperAdminUtilsDAO superAdminUtilsDAO, ProfileService profileService, RegistrationService registrationService, PasswordEncoder passwordEncoder, ProfileBalanceDAO profileBalanceDAO, MailUserDAO mailUserDAO, UserPrivateInfoDAO userPrivateInfoDAO, ProfileForAdminsDAO profileForAdminsDAO) {
        this.superAdminUtilsDAO = superAdminUtilsDAO;
        this.profileService = profileService;
        this.registrationService = registrationService;
        this.passwordEncoder = passwordEncoder;
        this.profileBalanceDAO = profileBalanceDAO;
        this.mailUserDAO = mailUserDAO;
        this.userPrivateInfoDAO = userPrivateInfoDAO;
        this.profileForAdminsDAO = profileForAdminsDAO;
    }

    @Override
    public void setProfileStatus(long clientId, long profileId, ProfileStatus profileStatus) throws NotFoundException, PermissionException {
        getPermissionService().checkAdministrativeRole(clientId);

        Profile profile = profileService.getProfileSafe(profileId);

        if (profileStatus == profile.getStatus()) {
            return;
        }

        profile.setStatus(profileStatus);
        getProfileDAO().update(profile);
    }

    @Override
    public SearchResult<ProfileForAdminsWithEmail> getVipUsers(long clientId, int offset, int limit) throws PermissionException {
        getPermissionService().checkAdministrativeRole(clientId);
        List<ProfileForAdminsWithEmail> profiles = profileForAdminsDAO.selectVipUsers(offset, limit);
        return new SearchResult<>(0, BIG_USER_NUMBER, profiles);
    }

    @Override
    public SearchResult<ProfileForAdminsWithEmail> getProfiles(long clientId, String query, ProfileType type,
                                             int offset, int limit) throws PermissionException {
        SearchResult<ProfileForAdminsWithEmail> result;

        getPermissionService().checkAdministrativeRole(clientId);

        if (StringUtils.isBlank(query)) {
            List<ProfileForAdminsWithEmail> profiles = profileForAdminsDAO.selectList(offset, limit);
            return new SearchResult<>(0, BIG_USER_NUMBER, profiles);
        } else {
            try {
                result = getSearchService().adminSearchForProfiles(query, type, offset, limit);
            } catch (SphinxException e) {
                log.warn("Profile admin search error: ", e);
                result = new SearchResult<>(0, 0, new ArrayList<ProfileForAdminsWithEmail>());
            }
        }

        if (offset == 0) {
            long profileId = NumberUtils.toLong(query);
            if (profileId > 0) {
                List<ProfileForAdminsWithEmail> profiles = profileForAdminsDAO.selectByIds(Collections.singletonList(profileId));
                if (!profiles.isEmpty()) {
                    if (result.isEmpty()) {
                        result.setSearchResultRecords(profiles);
                    } else {
                        final List<ProfileForAdminsWithEmail> profilesFound = result.getSearchResultRecords();
                        profilesFound.addAll(0, profiles);
                    }
                }
            }
        }

        return result;
    }

    @Override
    public List<ProfileForAdminsWithEmail> getPlanetaWorkersWithoutSuperAdmins() {
        return profileForAdminsDAO.selectPlanetaWorkersWithoutSuperAdmins();
    }


    @Override
    public void setUserStatus(long clientId, long profileId, EnumSet<UserStatus> roles) throws NotFoundException, PermissionException {

        // Only SUPER_ADMIN can grant roles
        if (!getPermissionService().isSuperAdmin(clientId)) {
            throw new PermissionException();
        }

        UserPrivateInfo privateInfo = getUserPrivateInfoSafe(profileId);

        if (!roles.contains(UserStatus.USER)) {
            roles.add(UserStatus.USER);
        }

        privateInfo.setUserStatus(roles);
        userPrivateInfoDAO.update(privateInfo);
    }

    private UserPrivateInfo getUserPrivateInfoSafe(long profileId) throws NotFoundException {
        UserPrivateInfo privateInfo = userPrivateInfoDAO.selectByUserId(profileId);
        if (privateInfo == null) {
            throw new NotFoundException(UserPrivateInfo.class, profileId);
        }
        return privateInfo;
    }

    @Override
    public String getUserEmail(long clientId, long profileId) throws NotFoundException, PermissionException {
        if (!getPermissionService().hasAdministrativeRole(clientId)) {
            throw new PermissionException();
        }
        UserPrivateInfo privateInfo = userPrivateInfoDAO.selectByUserId(profileId);
        if (privateInfo == null) {
            throw new NotFoundException(UserPrivateInfo.class, profileId);
        }
        return privateInfo.getEmail();
    }

    @Override
    public void setUserPassword(long clientId, long profileId, String password) throws PermissionException, NotFoundException {

        if (!getPermissionService().hasAdministrativeRole(clientId)) {
            throw new PermissionException();
        }
        UserPrivateInfo privateInfo = getUserPrivateInfoSafe(profileId);

        privateInfo.setPassword(passwordEncoder.encode(password));

        userPrivateInfoDAO.update(privateInfo);

    }

    @Override
    public void setUserEmail(long clientId, long profileId, String email) throws PermissionException, NotFoundException {

        if (!ValidateUtils.isValidEmail(email)) {
            throw new PermissionException(MessageCode.WRONG_EMAIL);
        }

        getPermissionService().checkAdministrativeRole(clientId);

        UserPrivateInfo userPrivateInfo = userPrivateInfoDAO.selectByEmail(email);
        if (userPrivateInfo != null) {
            throw new PermissionException(MessageCode.REGISTRATION_CURRENT_EMAIL_ALREADY_EXISTS);
        }

        UserPrivateInfo privateInfo = getUserPrivateInfoSafe(profileId);
        mailUserDAO.unsubscribeUserByEmail(privateInfo.getEmail());

        privateInfo.setUsernameAndEmail(email.trim().toLowerCase());
        userPrivateInfoDAO.update(privateInfo);

    }

    @Override
    public void confirmRegistration(long clientId, long profileId) throws PermissionException, NotFoundException {

        getPermissionService().checkAdministrativeRole(clientId);

        UserPrivateInfo privateInfo = getUserPrivateInfoSafe(profileId);
        if (privateInfo.getRegCode() == null) {
            throw new NotFoundException(MessageCode.REGISTRATION_CURRENT_REGCODE_NOT_EXISTS);
        }
        privateInfo.setRegCode(null);
        userPrivateInfoDAO.update(privateInfo);

        Profile profile = profileService.getProfileSafe(profileId);
        profile.setStatus(ProfileStatus.USER_ACTIVE);
        getProfileDAO().update(profile);

    }

    @Override
    public void sendRegistrationCompleteEmail(long clientId, long profileId) throws PermissionException, NotFoundException {
        getPermissionService().checkAdministrativeRole(clientId);
        registrationService.resendRegistrationCompleteEmail(profileId);
    }

    @Override
    public void changeAlias(long profileId, String alias) throws AlreadyExistException, NotFoundException {
        alias = StringUtils.trim(alias);
        alias = StringUtils.lowerCase(alias);

        if (!StringUtils.isEmpty(alias)) {
            alias = alias.replace("?", "").replace("/", "").replace("#", "");


            Profile profile = profileService.getProfileSafe(profileId);
            Profile current = profileService.getProfile(alias);

            if (current != null && profile.getProfileId() != current.getProfileId()) {
                throw new AlreadyExistException();
            } else {
                profile.setAlias(alias);
                getProfileDAO().update(profile);

            }

        }
    }

    @Override
    @NonTransactional
    public void deleteProfile(long clientId, long profileId) throws PermissionException {
        if (!getPermissionService().isPlanetaAdmin(clientId)) {
            throw new PermissionException();
        }

        superAdminUtilsDAO.deleteProfile(profileId);
    }

    @Override
    public void decreaseBalance(long clientId, long profileId, BigDecimal amount) throws PermissionException {
        if (!getPermissionService().isSuperAdmin(clientId)) {
            throw new PermissionException();
        }

        superAdminUtilsDAO.decreaseBalance(profileId, amount);
    }

    @Override
    public void freezeAmount(long clientId, long profileId, BigDecimal amount) throws PermissionException {
        if (!getPermissionService().isSuperAdmin(clientId)) {
            throw new PermissionException();
        }

        superAdminUtilsDAO.decreaseBalance(profileId, amount);

        BigDecimal frozenAmount = profileBalanceDAO.selectFrozenAmount(profileId);
        profileBalanceDAO.updateFrozenAmount(profileId, frozenAmount == null ? amount : frozenAmount.add(amount));
    }

    @Override
    public void unfreezeAmount(long clientId, long profileId, BigDecimal amount) throws PermissionException {
        if (!getPermissionService().isSuperAdmin(clientId)) {
            throw new PermissionException();
        }

        superAdminUtilsDAO.increaseBalance(profileId, amount);

        BigDecimal frozenAmount = profileBalanceDAO.selectFrozenAmount(profileId);
        profileBalanceDAO.updateFrozenAmount(profileId, frozenAmount == null ? amount : frozenAmount.subtract(amount));
    }

    @Override
    public void decreaseFrozenAmount(long clientId, long profileId, BigDecimal amount) throws PermissionException {
        if (!getPermissionService().isSuperAdmin(clientId)) {
            throw new PermissionException();
        }

        BigDecimal frozenAmount = profileBalanceDAO.selectFrozenAmount(profileId);
        profileBalanceDAO.updateFrozenAmount(profileId, frozenAmount == null ? amount : frozenAmount.subtract(amount));
    }

    @Override
    public void deleteUserPosts(long clientId, long profileId, int daysCount) throws PermissionException {
        if (!getPermissionService().isSuperAdmin(clientId)) {
            throw new PermissionException();
        }

        superAdminUtilsDAO.deleteUserPosts(profileId, daysCount);
    }

    @Override
    public void deleteUserComments(long clientId, long profileId, int daysCount) throws PermissionException {
        if (!getPermissionService().isSuperAdmin(clientId)) {
            throw new PermissionException();
        }

        superAdminUtilsDAO.deleteUserComments(profileId, daysCount);
    }
}

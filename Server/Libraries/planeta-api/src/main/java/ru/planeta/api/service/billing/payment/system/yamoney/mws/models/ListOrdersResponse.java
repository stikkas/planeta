package ru.planeta.api.service.billing.payment.system.yamoney.mws.models;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by eshevchenko on 25.04.14.
 */
@XmlRootElement
public class ListOrdersResponse extends BaseResponse {
    @XmlElement(name = "order")
    public List<Order> orders = new ArrayList<>();
}

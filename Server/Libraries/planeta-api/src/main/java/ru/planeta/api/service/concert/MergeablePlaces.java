package ru.planeta.api.service.concert;

import ru.planeta.api.exceptions.MoscowShowInteractionException;
import ru.planeta.api.exceptions.NotFoundException;
import ru.planeta.model.concert.Place;
import ru.planeta.model.concert.PlaceState;


import javax.annotation.Nonnull;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: michail
 * Date: 31.10.2016
 * Time: 13:36
 */
public class MergeablePlaces implements Mergeable<Place, ru.planeta.moscowshow.model.Place> {
    private PlaceService placeService;
    private List<Place> planetaList;
    private List<ru.planeta.moscowshow.model.Place> msList;
    private long externalConcertId;

    @Override
    public int compare(Place planetaPlace, ru.planeta.moscowshow.model.Place msPlace) {
        return Long.compare(planetaPlace.getExternalUnusedId(), msPlace.getId());
    }

    @Override
    public void insert(ru.planeta.moscowshow.model.Place msPlace) throws NotFoundException, MoscowShowInteractionException {
        Place newPlace = getPlace(msPlace);
        newPlace.setExternalConcertId(externalConcertId);
        placeService.insertOrUpdatePlace(newPlace);
    }

    @Override
    public void update(Place planetaPlace, ru.planeta.moscowshow.model.Place msPlace) throws NotFoundException, MoscowShowInteractionException {
        planetaPlace = updatePlace(msPlace, planetaPlace);
        placeService.insertOrUpdatePlace(planetaPlace);
    }

    @Override
    public void delete(Place planetaPlace) {
        placeService.markPlaceDeleted(planetaPlace);
    }

    @Override
    public List<Place> getInternalList() {
        return planetaList;
    }

    @Override
    public List<ru.planeta.moscowshow.model.Place> getExternalList() {
        return msList;
    }

    public MergeablePlaces(List<Place> planetaList, List<ru.planeta.moscowshow.model.Place> msList, PlaceService placeService, long externalConcertId) {
        this.planetaList = planetaList;
        this.msList = msList;
        this.placeService = placeService;
        this.externalConcertId = externalConcertId;
    }

    private static Place updatePlace(@Nonnull ru.planeta.moscowshow.model.Place moscowShowPlace, @Nonnull Place place) {
        place.setExternalUnusedId(moscowShowPlace.getId());
        place.setName(moscowShowPlace.getName());
        place.setSectionId(moscowShowPlace.getSectionId());
        place.setRowId(moscowShowPlace.getRowId());
        place.setPosition(moscowShowPlace.getPosition());
        place.setPrice(moscowShowPlace.getPrice());
        place.setX(moscowShowPlace.getX());
        place.setY(moscowShowPlace.getY());
        place.setState(PlaceState.Companion.fromMoscowShowState(moscowShowPlace.getState()));
        return place;
    }

    private static Place getPlace(@Nonnull ru.planeta.moscowshow.model.Place moscowShowPlace) {
        return updatePlace(moscowShowPlace, new Place());
    }
}

package ru.planeta.api.service.faq;

import ru.planeta.model.commondb.FaqArticle;

import java.util.List;

/**
 * Created by kostiagn on 01.09.2015.
 */
public interface FaqArticleService {
    void insertFaqArticle(FaqArticle faqArticle);

    void updateFaqArticle(FaqArticle faqArticle);

    void deleteFaqArticle(Long faqArticleId);

    List<FaqArticle> selectFaqArticleListByFaqCategory(Long faqCategoryId, int offset, int limit);

    List<FaqArticle> selectFaqArticleList(int offset, int limit);


    FaqArticle selectFaqArticle(Long faqArticleId);

    void resort(long idFrom, long idTo, long faqCategoryId);
}

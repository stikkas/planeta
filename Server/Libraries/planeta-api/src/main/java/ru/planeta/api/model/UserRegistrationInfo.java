package ru.planeta.api.model;

/**
 * Additional registration info used for statistics and analytics
 */
public class UserRegistrationInfo {
    private String ip;


    public UserRegistrationInfo(String ip) {
        this.ip = ip;
    }

    public String getIp() {
        return ip;
    }
}

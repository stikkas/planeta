package ru.planeta.api.service.concert;

import ru.planeta.api.exceptions.MoscowShowInteractionException;
import ru.planeta.api.exceptions.NotFoundException;
import ru.planeta.model.concert.Concert;


/**
 * Created with IntelliJ IDEA.
 * User: michail
 * Date: 27.10.16
 * Time: 15:21
 */
public interface ConcertImportService {
    void importMoscowShowConcerts() throws NotFoundException, MoscowShowInteractionException;

    void importMoscowShowConcertScheme(Concert concert) throws NotFoundException, MoscowShowInteractionException;
}

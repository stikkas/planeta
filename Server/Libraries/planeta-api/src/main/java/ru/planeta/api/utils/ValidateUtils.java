package ru.planeta.api.utils;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.validator.routines.UrlValidator;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;

import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Component
@Lazy(false)
public class ValidateUtils {

    private static Pattern tagRegexp;
    private static Pattern validUserName;
    private static Pattern validPassword;
    private static Pattern validEmail;
    private static Pattern validPhone;
    private static Pattern validPhoneWithSpecialCharacters;
    private static Pattern validPhoneExt;
    private static Pattern validPhoneHuman;
    private static Pattern validPlainText;
    private static Pattern validCampaignAlias;
    private static Pattern validCampaignAliasLetter;
    private static Pattern validProfileAlias;
    private static Pattern validProfileAliasLetter;
    private static Pattern validReservedWord;
    private static Pattern validDescription;
    private static Pattern validOnlyDigits;
    private static Pattern validVkTargetingId;
    private static Pattern validForeignContractorInn;

    private static final String[] schemes = {"http","https"};
    private static final UrlValidator urlValidator = new UrlValidator(schemes);

    private static final String[] STOP_WORDS_WYSIWYG = {
            "<script",
            "iframe",
            "<frame",
            "<img",
            "<object",
            "javascript:",
            "vbscript:",
            "&#60;&#115&#99&#114&#105&#112&#116&#62;",
            "&amp;#60;&amp;#115&amp;#99&amp;#114&amp;#105&amp;#112&amp;#116&amp;#62;",
            "'})"
    };


    private static final String[] STOP_WORDS = {
            "<script",
            "iframe",
            "<frame",
            "<img",
            "<a",
            "<object",
            "javascript:",
            "vbscript:",
            "&#60;&#115&#99&#114&#105&#112&#116&#62;",
            "&amp;#60;&amp;#115&amp;#99&amp;#114&amp;#105&amp;#112&amp;#116&amp;#62;",
            "'})"
    };

    @Value("${tag.regexp}")
    public void setTagRegexp(String pattern) {
        tagRegexp = Pattern.compile(pattern);
    }

    @Value("${valid.user.name}")
    public void setValidUserName(String pattern) {
        validUserName = Pattern.compile(pattern);
    }

    @Value("${valid.description}")
    public void setValidDescription(String pattern) {
        validDescription = Pattern.compile(pattern);
    }

    @Value("${valid.only.digits}")
    public void setValidOnlyDigits(String pattern) {
        validOnlyDigits = Pattern.compile(pattern);
    }

    @Value("${valid.foreign.contractor.inn}")
    public void setValidForeignContractorInn(String pattern) {
        validForeignContractorInn = Pattern.compile(pattern);
    }

    @Value("${valid.campaign.vk.targeting.id}")
    public void setValidVkTargetingId(String pattern) {
        validVkTargetingId = Pattern.compile(pattern);
    }

    @Value("${valid.password}")
    public void setValidPassword(String pattern) {
        validPassword = Pattern.compile(pattern);
    }

    @Value("${valid.email}")
    public void setValidEmail(String pattern) {
        validEmail = Pattern.compile(pattern);
    }

    @Value("${valid.phone}")
    public void setValidPhone(String pattern) {
        validPhone = Pattern.compile(pattern);
    }

    @Value("${valid.phone.special.characters}")
    public void setValidPhoneWithSpecialCharacters(String pattern) {
        validPhoneWithSpecialCharacters = Pattern.compile(pattern);
    }

    @Value("${valid.phone.ext}")
    public void setValidPhoneExt(String pattern) {
        validPhoneExt = Pattern.compile(pattern);
    }

    @Value("${valid.phone.human}")
    public void setValidPhoneHuman(String pattern) {
        validPhoneHuman = Pattern.compile(pattern);
    }

    @Value("${valid.plain.text}")
    public void setValidPlainText(String pattern) {
        validPlainText = Pattern.compile(pattern);
    }

    @Value("${valid.campaign.alias}")
    public void setValidCampaignAlias(String pattern) {
        validCampaignAlias = Pattern.compile(pattern);
    }

    @Value("${valid.campaign.alias.letter}")
    public void setValidCampaignAliasLetter(String pattern) {
        validCampaignAliasLetter = Pattern.compile(pattern);
    }

    @Value("${valid.profile.alias}")
    public void setValidProfileAlias(String pattern) {
        validProfileAlias = Pattern.compile(pattern);
    }

    @Value("${valid.profile.alias.letter}")
    public void setValidProfileAliasLetter(String pattern) {
        validProfileAliasLetter = Pattern.compile(pattern);
    }

    @Value("${valid.reserved.word}")
    public void setValidReservedWord(String pattern) {
        validReservedWord = Pattern.compile(pattern);
    }

    public static void rejectIfContainsHtmlTags(Errors errors, String fieldName, String errorCode) {
        String value = getStringValue(errors, fieldName);
        if (StringUtils.isEmpty(value)) {
            return;
        }
        Matcher matcher = tagRegexp.matcher(value);
        if (matcher.find()) {
            errors.rejectValue(fieldName, errorCode);
        }
    }

    public static void rejectIfNotUserName(Errors errors, String fieldName, String errorCode) {
        rejectIfNotMatches(errors, fieldName, errorCode, validUserName);
    }

    public static void rejectIfNotUrl(Errors errors, String fieldName, String errorCode) {
        String value = getStringValue(errors, fieldName);
        if (StringUtils.isEmpty(value)) {
            return;
        }
        if (!urlValidator.isValid(value)) {
            errors.rejectValue(fieldName, errorCode);
        }
    }

    public static void rejectIfNotPassword(Errors errors, String fieldName, String errorCode) {
        rejectIfNotMatches(errors, fieldName, errorCode, validPassword);
    }

    public static void rejectIfNotEmail(Errors errors, String fieldName, String errorCode) {
        rejectIfNotMatches(errors, fieldName, errorCode, validEmail);
    }

    public static void rejectIfNotPhone(Errors errors, String fieldName, String errorCode) {
        rejectIfNotMatches(errors, fieldName, errorCode, validPhone);
    }

    public static void rejectIfNotPhoneExt(Errors errors, String fieldName, String errorCode) {
        rejectIfNotMatches(errors, fieldName, errorCode, validPhoneExt);
    }

    public static void rejectIfNotPhoneHuman(Errors errors, String fieldName, String errorCode) {
        rejectIfNotMatches(errors, fieldName, errorCode, validPhoneHuman);
    }

    public static void rejectIfNotPlainText(Errors errors, String fieldName, String errorCode) {
        rejectIfNotMatches(errors, fieldName, errorCode, validPlainText);
    }

    public static void rejectIfNotCampaignAlias(Errors errors, String fieldName, String errorCode) {
        rejectIfNotMatches(errors, fieldName, errorCode, validCampaignAlias);
    }

    public static void rejectIfNotCampaignAliasHasLetter(Errors errors, String fieldName, String errorCode) {
        rejectIfNotMatches(errors, fieldName, errorCode, validCampaignAliasLetter);
    }

    public static void rejectIfNotProfileAlias(Errors errors, String fieldName, String errorCode) {
        rejectIfNotMatches(errors, fieldName, errorCode, validProfileAlias);
    }

    public static void rejectIfNotProfileAliasHasLetter(Errors errors, String fieldName, String errorCode) {
        rejectIfNotMatches(errors, fieldName, errorCode, validProfileAliasLetter);
    }

    public static void rejectIfReservedWord(Errors errors, String fieldName, String errorCode) {
        rejectIfMatches(errors, fieldName, errorCode, validReservedWord);
    }

    public static void rejectIfNotMatches(Errors errors, String fieldName, String errorCode, Pattern pattern) {
        String value = getStringValue(errors, fieldName);
        if (StringUtils.isEmpty(value)) {
            return;
        }
        if (!match(pattern, value)) {
            errors.rejectValue(fieldName, errorCode);
        }
    }

    public static void rejectIfMatches(Errors errors, String fieldName, String errorCode, Pattern pattern) {
        String value = getStringValue(errors, fieldName);
        if (StringUtils.isEmpty(value)) {
            value = "";
        }
        if (match(pattern, value)) {
            errors.rejectValue(fieldName, errorCode);
        }
    }

    //</editor-fold>
    private static boolean match(Pattern pattern, String input) {
        if (input == null) {
            return true;
        }
        Matcher matcher = pattern.matcher(input);
        return matcher.matches();
    }

    private static String getStringValue(Errors errors, String fieldName) {
        Object obj = errors.getFieldValue(fieldName);
        if (obj != null && String.class.equals(obj.getClass())) {
            return (String) obj;
        }

        return null;
    }

    /**
     * reject with default error message.
     * checks given value
     * @param errors {@link org.springframework.validation.Errors} item
     * @param fieldName field bind to
     * @param length border length
     * @param value value to check
     */
    public static void rejectIfSizeIsTooLarge(Errors errors, String fieldName, String value, int length, MessageSource messageSource) {
        rejectIfSizeIsTooLarge(errors, fieldName, value, "field.length.variable", length, messageSource.getMessage("length.field.is.greater.than.permitted", null, Locale.getDefault()));
    }

    /**
     * @param errorCode static error codes
     */
    public static void rejectIfSizeIsTooLarge(Errors errors, String fieldName, String value, String errorCode, int length, MessageSource messageSource) {
        rejectIfSizeIsTooLarge(errors, fieldName, value, errorCode, length, messageSource.getMessage("length.field.is.greater.than.permitted", null, Locale.getDefault()));
    }

    /**
     * reject with default error message.
     * checks stored value
     * @param errors {@link org.springframework.validation.Errors} item
     * @param fieldName field bind to
     * @param length border length
     */
    public static void rejectIfSizeIsTooLarge(Errors errors, String fieldName, int length, MessageSource messageSource) {
        rejectIfSizeIsTooLarge(errors, fieldName, "field.length.variable", length, messageSource.getMessage("length.field.is.greater.than.permitted", null, Locale.getDefault()));
    }

    /**
     * reject with given error message.
     * checks stored value
     * @param errors {@link org.springframework.validation.Errors} item
     * @param fieldName field bind to
     * @param length border length
     * @param errorCode error code template
     * @param defaultMessage message will be if  something wrong with errorCode
     */
    public static void rejectIfSizeIsTooLarge(Errors errors, String fieldName, String errorCode, int length, String defaultMessage) {
        rejectIfSizeIsTooLarge(errors, fieldName, getStringValue(errors, fieldName), errorCode, length, defaultMessage);
    }

    /**
     * reject with given error message.
     * checks given value
     * @param errors {@link org.springframework.validation.Errors} item
     * @param fieldName field bind to
     * @param length border length
     * @param value value to check
     * @param errorCode error code template
     * @param defaultMessage message will be if  something wrong with errorCode
     */
    public static void rejectIfSizeIsTooLarge(Errors errors, String fieldName, String value, String errorCode, int length, String defaultMessage) {
        if (StringUtils.isEmpty(value)) {
            return;
        }
        if (value.length() > length) {
            errors.rejectValue(fieldName, errorCode, new Object[]{ length }, defaultMessage);
        }
    }

    public static void rejectIfSizeIsTooLargeNotField(Errors errors, String value, String errorCode, int length, MessageSource messageSource) {
        if (StringUtils.isEmpty(value)) {
            return;
        }
        if (value.length() > length) {
            errors.reject(errorCode, new Object[]{length}, messageSource.getMessage("length.field.is.greater.than.permitted", null, Locale.getDefault()));
        }
    }

    public static void rejectIfSizeIsTooSmall(Errors errors, String fieldName, String errorCode, int length, boolean allowEmpty) {
        String value = getStringValue(errors, fieldName);
        rejectIfSizeIsTooSmall(errors, fieldName, value, errorCode, length, allowEmpty);
    }


    public static void rejectIfSizeIsTooSmall(Errors errors, String fieldName, String value, String errorCode, int length, boolean allowEmpty) {
        if (allowEmpty && StringUtils.isEmpty(value)) {
            return;
        }
        if (StringUtils.isEmpty(value) || value.length() < length) {
            errors.rejectValue(fieldName, errorCode);
        }
    }

    public static boolean isItOnlyDigitsString(String value) {
        return isValidTemplate(validOnlyDigits, value);
    }

    public static boolean isItValidForeignContractorInn(String value) {
        return isValidTemplate(validForeignContractorInn, value);
    }

    public static boolean isItValidVkTargetingId(String value) {
        return isValidTemplate(validVkTargetingId, value);
    }

    public static void rejectIfContainsNotValidStringWysiwyg(Errors errors, String fieldName, String errorCode) {
        String value = getStringValue(errors, fieldName);
        rejectIfContainsNotValidStringWysiwyg(errors, fieldName, value, errorCode);
    }


    public static void rejectIfContainsNotValidString(Errors errors, String fieldName, String errorCode) {
        String value = getStringValue(errors, fieldName);
        rejectIfContainsNotValidString(errors, fieldName, value, errorCode);
    }

    public static void rejectIfContainsNotValidStringWysiwyg(Errors errors, String fieldName, String value, String errorCode) {
        if (isContainNotValidStringWysiwyg(value)) {
            errors.rejectValue(fieldName, errorCode);
        }
    }

    public static void rejectIfContainsNotValidString(Errors errors, String fieldName, String value, String errorCode) {
        if (isContainNotValidString(value)) {
            errors.rejectValue(fieldName, errorCode);
        }
    }

    public static boolean isContainNotValidStringWysiwyg(String value) {
        if (StringUtils.isEmpty(value)) {
            return false;
        }
        for (String stopWord: STOP_WORDS_WYSIWYG) {
            if (StringUtils.containsIgnoreCase(value, stopWord)) {
                return true;
            }
        }
        return value.matches(".*[\\u2028-\\u2029]+.*");
    }

    public static boolean isContainNotValidString(String value) {
        if (StringUtils.isEmpty(value)) {
            return false;
        }
        for (String stopWord: STOP_WORDS) {
            if (StringUtils.containsIgnoreCase(value, stopWord)) {
                return true;
            }
        }
        return value.matches(".*[\\u2028-\\u2029]+.*");
    }

    private static boolean isValidTemplate(Pattern pattern, String value) {
        return match(pattern, value);
    }

    public static boolean isValidPhone(String value) {
        return isValidTemplate(validPhone, value);
    }

    public static boolean isValidPhoneWithSpecialCharacters(String value) {
        return isValidTemplate(validPhoneWithSpecialCharacters, value);
    }

    public static boolean isValidEmail(String value) {
        return isValidTemplate(validEmail, value);
    }

    public static boolean isPlanetaEmail(String value) {
        return value != null && value.endsWith("@planeta.ru");
    }

    public static boolean isValidRussians(String value) {
        return isValidTemplate(validDescription, value);
    }

    public static boolean isReserved(String value) {
        return isValidTemplate(validReservedWord, value);
    }
}

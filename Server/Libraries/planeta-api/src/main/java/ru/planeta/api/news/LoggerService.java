package ru.planeta.api.news;

import ru.planeta.model.common.campaign.Share;
import ru.planeta.model.news.ProfileNews;

import java.util.Map;

/**
 * Created by asavan on 27.12.2016.
 */
public interface LoggerService {
    ProfileNews addProfileNews(ProfileNews.Type type, long profileId, long objectId, long campaignId, Map<String, String> extraParamsMap);

    ProfileNews addProfileNews(ProfileNews.Type type, long profileId, long objectId, long campaignId);

    ProfileNews addProfileNews(ProfileNews.Type type, long profileId, long objectId);

    ProfileNews addFileDeletedNews(ProfileNews.Type type, long profileId, long objectId, Map<String, String> extraParamsMap);

    ProfileNews addShareNews(ProfileNews.Type type, long clientId, Share share, long campaignId);
}

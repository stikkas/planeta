package ru.planeta.api.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import ru.planeta.commons.web.RegistrationData;
import ru.planeta.model.common.auth.UserCredentials;
import ru.planeta.commons.model.Gender;

import java.util.Date;

/**
 * Info for view during registration and authorization (like name, email, photo etc.)
 * Any external info (from VK or FB) can be parsed into this class instance
 * See welcome/start.html jsp
 * Date: 17.09.12
 *
 * @author s.kalmykov
 */
public class UserCredentialsInfo extends UserCredentials {

    @JsonIgnore
    public void setRegistrationData(RegistrationData registrationData) {
        this.registrationData = registrationData;
    }

    @JsonIgnore
    public RegistrationData getRegistrationData() {
        return registrationData;

    }

    private RegistrationData registrationData = new RegistrationData();

    private String email;

    public String getNickName() {
        return registrationData.getNickName();
    }

    public void setNickName(String nickName) {
        registrationData.setNickName(nickName);
    }

    public String getExternalPhotoUrl() {
        return registrationData.getPhotoUrl();
    }

    public void setExternalPhotoUrl(String externalPhotoUrl) {
        registrationData.setPhotoUrl(externalPhotoUrl);
    }

    public String getFirstName() {
        return registrationData.getFirstName();
    }

    public void setFirstName(String firstName) {
        registrationData.setFirstName(firstName);
    }

    public String getLastName() {
        return registrationData.getLastName();
    }

    public void setLastName(String lastName) {
        registrationData.setLastName(lastName);
    }

    private boolean hasEmail;
    private String emailPassword;

    public String getEmailPassword() {
        return emailPassword;
    }

    public void setEmailPassword(String emailPassword) {
        this.emailPassword = emailPassword;
    }

    public Gender getGender() {
        return registrationData.getGender();
    }

    public void setGender(Gender gender) {
        registrationData.setGender(gender);
    }

    public Date getUserBirthDate() {
        return registrationData.getUserBirthDate();
    }

    public void setUserBirthDate(Date userBirthDate) {
        registrationData.setUserBirthDate(userBirthDate);
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }


    public boolean hasEmail() {
        return hasEmail;
    }

    public void setHasEmail(boolean hasEmail) {
        this.hasEmail = hasEmail;
    }

}

package ru.planeta.api.text;


import com.fasterxml.jackson.databind.annotation.JsonSerialize;

/**
 * Represents an audio track attachment
 */
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class AudioAttachment extends Attachment {
    private int duration;
    private String name;
    private String artist;

    /**
     * Audio track duration
     *
     * @return
     */
    public int getDuration() {
        return duration;
    }

    /**
     * Sets audio track duration
     *
     * @param duration
     */
    public void setDuration(int duration) {
        this.duration = duration;
    }

    /**
     * Audio track name
     *
     * @return
     */
    public String getName() {
        return name;
    }

    /**
     * Sets audio track name
     *
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Gets artist name
     *
     * @return
     */
    public String getArtist() {
        return artist;
    }

    /**
     * Sets artist name
     *
     * @param artist
     */
    public void setArtist(String artist) {
        this.artist = artist;
    }

    public String toString() {
        return toString("audio", "id", getObjectId(), "owner", getOwnerId(),
                "duration", getDuration(), "artist", getArtist(), "name", getName());
    }

}

package ru.planeta.api.service.billing.payment.settings;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import ru.planeta.api.BaseService;
import ru.planeta.dao.payment.PaymentMethodDAO;
import ru.planeta.dao.payment.PaymentProviderDAO;
import ru.planeta.model.common.PaymentMethod;
import ru.planeta.model.common.PaymentProvider;
import ru.planeta.model.enums.ProjectType;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Implements {@link ru.planeta.api.service.billing.payment.settings.PaymentSettingsService} interface.
 * Created by eshevchenko.
 */
@Service
@Lazy(true)
public class PaymentSettingsServiceImpl extends BaseService implements PaymentSettingsService {

    private static final Logger log = Logger.getLogger(PaymentSettingsService.class);

    @Autowired
    private PaymentMethodDAO paymentMethodDAO;

    @Autowired
    private PaymentProviderDAO paymentProviderDAO;

    private List<PaymentMethod> sortPaymentMethods(List<PaymentMethod> methods, ProjectType projectType) {
        Map<Long, PaymentMethod> methodMap = new HashMap<>();
        for (PaymentMethod m : methods) {
            methodMap.put(m.getId(), m);
        }
        List<PaymentMethod> result = new ArrayList<>();
        for (PaymentMethod m : methods) {
            if (m.getParentId() > 0) {
                PaymentMethod parent = methodMap.get(m.getParentId());
                if (parent != null) {
                    if (parent.getChildren() == null) {
                        parent.setChildren(new ArrayList<>());
                    }
                    parent.getChildren().add(m);
                } else {
                    log.warn("can't find parent payment method with id " + m.getParentId());
                }
            } else {
                if (projectType == ProjectType.BIBLIO && m.getAlias() == PaymentMethod.Alias.INVOICE) {
                    continue;
                }
                result.add(m);
            }
        }
        return result;
    }

    @Override
    public List<PaymentMethod> getPaymentMethodsOld(ProjectType projectType, Boolean internal, Boolean promo) {
        List<PaymentMethod> methods = paymentMethodDAO.getPaymentMethodsForProject(projectType, internal, promo, true);
        return sortPaymentMethods(methods.stream()
                .filter(m -> PaymentMethod.Alias.ELECTRONIC_MONEY != m.getAlias()).collect(Collectors.toList()), projectType);
    }

    @Override
    public List<PaymentMethod> getPaymentMethods(ProjectType projectType, Boolean internal, Boolean promo) {
        List<PaymentMethod> methods = paymentMethodDAO.getPaymentMethodsForProject(projectType, internal, promo);
        return sortPaymentMethods(methods, projectType);
    }

    @Override
    public List<PaymentMethod> getPaymentMethodsPlain(ProjectType projectType, Boolean internal, Boolean promo) {
        return paymentMethodDAO.getPaymentMethodsForProject(projectType, internal, promo);
    }

    @Override
    public PaymentMethod getInternalPaymentMethod() {
        return paymentMethodDAO.getInternalPaymentMethod(ProjectType.MAIN);
    }

    @Override
    public PaymentMethod getPromoPaymentMethod() {
        return paymentMethodDAO.getPromoPaymentMethod(ProjectType.MAIN);
    }

    @Override
    public List<PaymentMethod> getAllPaymentMethods() {
        return paymentMethodDAO.getPaymentMethods();
    }

    public List<PaymentProvider> getAllProviders() {
        return paymentProviderDAO.selectAll();
    }

}

package ru.planeta.api.service.billing.payment.system.yamoney.mws.operations;

import ru.planeta.api.service.billing.payment.system.yamoney.mws.MwsAccount;
import ru.planeta.api.service.billing.payment.system.yamoney.mws.exceptions.CommunicationException;
import ru.planeta.api.service.billing.payment.system.yamoney.mws.exceptions.ResponseParsingException;
import ru.planeta.api.service.billing.payment.system.yamoney.mws.exceptions.UnexpectedStatusCodeException;
import ru.planeta.api.service.billing.payment.system.yamoney.mws.exceptions.YaMoException;
import ru.planeta.api.service.billing.payment.system.yamoney.mws.models.BaseResponse;
import ru.planeta.api.service.billing.payment.system.yamoney.mws.utils.MarshallingUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.apache.http.HttpEntity;
import org.apache.http.HttpStatus;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;

import static java.nio.charset.StandardCharsets.UTF_8;

/**
 * YandexMoney operation. Provides common operation logic.<br>
 * Created by eshevchenko.
 */
public abstract class BaseOperation<Q, S extends BaseResponse> {

    private final MwsAccount account;
    private final OperationName operationName;
    private final Q request;
    private final Class<S> responseClass;

    protected BaseOperation(OperationName operationName, Q request, Class<S> responseClass, MwsAccount account) {
        this.operationName = operationName;
        this.request = request;
        this.responseClass = responseClass;
        this.account = account;
    }

    protected Q getRequest() {
        return request;
    }

    protected OperationName getOperationName() {
        return operationName;
    }

    protected Class<S> getResponseClass() {
        return responseClass;
    }

    protected MwsAccount getAccount() {
        return account;
    }

    public S execute(String serviceUrl, int communicationTimeout, Logger logger) throws YaMoException {
        HttpPost request = new HttpPost(getOperationName().formatUrl(serviceUrl));
        request.setEntity(createRequestEntity(getRequest()));

        StringBuilder builder = new StringBuilder(getOperationName().name()).append(" operation log:\n")
                .append("Request:\n")
                .append(request).append("\n")
                .append(ReflectionToStringBuilder.toString(getRequest(), ToStringStyle.MULTI_LINE_STYLE));
        try {
            CloseableHttpClient httpClient = createHttpClient(communicationTimeout);
            try {
                CloseableHttpResponse response = httpClient.execute(request);
                builder.append("\nResponse:\n").append(response);
                try {
                    final int statusCode = response.getStatusLine().getStatusCode();
                    if (statusCode != HttpStatus.SC_OK) {
                        throw new UnexpectedStatusCodeException(statusCode);
                    }

                    return parseResponse(builder, response);
                } finally {
                    IOUtils.closeQuietly(response);
                }
            } catch (Exception e) {
                throw new CommunicationException(e);
            } finally {
                IOUtils.closeQuietly(httpClient);
            }
        } catch (YaMoException e) {
            builder.append("\nRequest execution error: ").append(e.getMessage());
            throw e;
        } catch (Exception e) {
            builder.append("\nRequest execution error: ").append(e.getMessage());
            throw new YaMoException(e);
        } finally {
            logger.info(builder);
        }
    }

    private CloseableHttpClient createHttpClient(int communicationTimeout) {
        SSLConnectionSocketFactory sslSocketFactory = getAccount().getSslConnectionSocketFactory();
        RequestConfig requestConfig = RequestConfig.custom()
                .setSocketTimeout(communicationTimeout)
                .setConnectTimeout(communicationTimeout)
                .build();

        return HttpClients.custom()
                .setSSLSocketFactory(sslSocketFactory)
                .setDefaultRequestConfig(requestConfig)
                .build();
    }

    protected S parseResponse(StringBuilder builder, CloseableHttpResponse response) throws ResponseParsingException {
        HttpEntity entity = response.getEntity();
        try {
            byte[] asBytes = EntityUtils.toByteArray(entity);
            builder.append(String.format("%nContent %d bytes:%n", asBytes.length)).append(new String(asBytes, UTF_8));

            return MarshallingUtils.unmarshal(getResponseClass(), asBytes);
        } catch (Exception e) {
            throw new ResponseParsingException(e);
        } finally {
            EntityUtils.consumeQuietly(entity);
        }
    }

    protected abstract HttpEntity createRequestEntity(Q request) throws YaMoException;
}

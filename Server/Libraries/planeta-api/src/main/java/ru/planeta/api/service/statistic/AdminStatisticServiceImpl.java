package ru.planeta.api.service.statistic;

import org.apache.commons.lang3.time.DateUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.planeta.api.BaseDaoService;
import ru.planeta.api.Utils;
import ru.planeta.api.exceptions.PermissionException;
import ru.planeta.api.model.DateRange;
import ru.planeta.api.model.stat.PurchaseReport;
import ru.planeta.api.service.security.PermissionService;
import ru.planeta.dao.commondb.AdminStatPurchaseDAO;
import ru.planeta.model.common.UserPrivateInfo;
import ru.planeta.model.enums.RotationPeriod;
import ru.planeta.model.profile.Profile;
import ru.planeta.model.shop.Product;
import ru.planeta.model.stat.*;

import java.math.BigDecimal;
import java.util.*;

import static ru.planeta.api.Utils.empty;

/**
 * @author is.kuzminov
 *         Date: 19.07.12
 */
@Service
public class AdminStatisticServiceImpl extends BaseDaoService implements AdminStatisticService {

    private static final Logger log = Logger.getLogger(AdminStatisticServiceImpl.class);

    private final PermissionService permissionService;
    private final AdminStatPurchaseDAO purchaseDAO;

    @Autowired
    public AdminStatisticServiceImpl(PermissionService permissionService, AdminStatPurchaseDAO purchaseDAO) {
        this.permissionService = permissionService;
        this.purchaseDAO = purchaseDAO;
    }

    @Override
    public NavigableMap<Date, PurchaseReport> getStatPurchaseCompositeReport(long clientId, Date dateFrom, Date dateTo, String step)
            throws PermissionException {
        if (!permissionService.hasAdministrativeRole(clientId)) {
            throw new PermissionException();
        }
        NavigableMap<Date, PurchaseReport> result = new TreeMap<>();
        List<StatPurchaseComposite> purchases = purchaseDAO.selectStatComposite(dateFrom, dateTo, step);
        if (Utils.empty(purchases)) {
            return result;
        }
        for (StatPurchaseComposite p : purchases) {
            if (!result.containsKey(p.getStartDate())) {
                result.put(p.getStartDate(), new PurchaseReport(p));
            } else {
                result.get(p.getStartDate()).addPurchase(p);
            }
        }
        List<PurchaseReport> values = new ArrayList<>(result.values());
        for (int i = 0; i < values.size() - 1; ++i) {
            values.get(i + 1).setDiff(values.get(i));
        }

        return result.descendingMap();
    }

    public Date getDateTo(Date dateFrom, RotationPeriod step) throws PermissionException {
        final Date dateTo;
        switch(step) {
            case DAY:
                dateTo = DateUtils.addDays(dateFrom, 1);
                break;
            case WEEK:
                dateTo = DateUtils.addWeeks(dateFrom, 1);
                break;
            case MONTH:
                dateTo = DateUtils.addMonths(dateFrom, 1);
                break;
            default:
                throw new PermissionException();
        }
        return dateTo;
    }

    @Override
    public List<StatRegistration> getRegistrationsReport(long clientId, Date dateFrom, Date dateTo) throws PermissionException {
        if (!permissionService.hasAdministrativeRole(clientId)) {
            throw new PermissionException();
        }
        List<UserPrivateInfo> userPrivateInfoList = getUserPrivateInfoDAO().selectUserPrivateInfo(dateFrom, dateTo, 0, 0);
        //
        Map<Long, UserPrivateInfo> userIdToProfile = new HashMap<>();
        for (UserPrivateInfo userPrivateInfo : userPrivateInfoList) {
            userIdToProfile.put(userPrivateInfo.getUserId(), userPrivateInfo);
        }
        List<StatRegistration> statRegistrationList = new LinkedList<>();
        List<Profile> profileList = getProfileDAO().selectSearchedByIds(userIdToProfile.keySet(), 0L);
        for(Profile profile : profileList) {
            UserPrivateInfo userPrivateInfo =  userIdToProfile.get(profile.getProfileId());
            StatRegistration statRegistration = getNewStatRegistration(profile, userPrivateInfo);
            statRegistrationList.add(statRegistration);
        }
        //
        Collections.sort(statRegistrationList, new Comparator<StatRegistration>() {
            @Override
            public int compare(StatRegistration o1, StatRegistration o2) {
                Date date1 = o1.getTimeAdded();
                Date date2 = o2.getTimeAdded();
                return date1.compareTo(date2);
            }
        });
        return statRegistrationList;
    }

    private StatRegistration getNewStatRegistration(Profile profile, UserPrivateInfo userPrivateInfo) {
        StatRegistration statRegistration = new StatRegistration();
        //
        if(profile == null) {  //dev possible
            profile = new Profile();
            profile.setProfileId(userPrivateInfo.getUserId());
            log.error("profile not found " + userPrivateInfo.getUserId());
        }
        statRegistration.setProfile(profile);
        statRegistration.setTimeAdded(userPrivateInfo.getTimeAdded());
        statRegistration.setEmail(userPrivateInfo.getEmail());
        return statRegistration;
    }

    @Override
    public List<StatPurchaseShare> getStatPurchaseShareReport(long clientId, Date dateFrom, Date dateTo)
            throws PermissionException {
        if (!permissionService.hasAdministrativeRole(clientId)) {
            throw new PermissionException();
        }
        //
        List<StatPurchaseShare> statPurchaseShareList = purchaseDAO.selectStatPurchaseShares(dateFrom, dateTo);
        //
        Date previousDateFrom = new Date(dateFrom.getTime() - (dateTo.getTime() - dateFrom.getTime()));
        List<StatPurchaseShare> previousStatPurchaseShareList = purchaseDAO.selectStatPurchaseShares(previousDateFrom, dateFrom);
        //
        setDiff(statPurchaseShareList, previousStatPurchaseShareList);
        //
        Map<Long, List<StatPurchaseShare>> groupIdToSharesList = getLongListMap(statPurchaseShareList);
        List<Profile> profileList = getProfileDAO().selectSearchedByIds(groupIdToSharesList.keySet(), 0L);
        for(Profile p : profileList) {
            List<StatPurchaseShare> sharesList = groupIdToSharesList.get(p.getProfileId());
            for(StatPurchaseShare statPurchaseShare : sharesList) {
                statPurchaseShare.setGroupProfile(p);
            }
        }
        Collections.sort(statPurchaseShareList);
        //
        return statPurchaseShareList;
    }

    @Override
    public List<StatPurchaseShare> getStatPurchaseShareReportWithoutDiff(long clientId, Date dateFrom, Date dateTo)
            throws PermissionException {
        if (!permissionService.hasAdministrativeRole(clientId)) {
            throw new PermissionException();
        }
        //
        final List<StatPurchaseShare> statPurchaseShareList = purchaseDAO.selectStatPurchaseShares(dateFrom, dateTo);
        //
        Map<Long, List<StatPurchaseShare>> groupIdToSharesList = getLongListMap(statPurchaseShareList);
        List<Profile> profileList = getProfileDAO().selectSearchedByIds(groupIdToSharesList.keySet(), 0L);
        for(Profile p : profileList) {
            List<StatPurchaseShare> sharesList = groupIdToSharesList.get(p.getProfileId());
            for(StatPurchaseShare statPurchaseShare : sharesList) {
                statPurchaseShare.setGroupProfile(p);
            }
        }
        Collections.sort(statPurchaseShareList);
        //
        return statPurchaseShareList;
    }

    private static Map<Long, List<StatPurchaseShare>> getLongListMap(List<StatPurchaseShare> statPurchaseShareList) {
        Map<Long, List<StatPurchaseShare>> groupIdToSharesList = new HashMap<>();
        for (StatPurchaseShare statPurchaseShare : statPurchaseShareList) {
            long groupId = statPurchaseShare.getGroupId();
            List<StatPurchaseShare> sharesList = groupIdToSharesList.get(groupId);
            if(sharesList == null) {
                sharesList = new LinkedList<>();
                groupIdToSharesList.put(groupId, sharesList);
            }
            sharesList.add(statPurchaseShare);
        }
        return groupIdToSharesList;
    }

    @Override
    public List<StatPurchaseProduct> getStatPurchaseProductReport(long clientId, Date dateFrom, Date dateTo)
            throws PermissionException {
        if (!permissionService.hasAdministrativeRole(clientId)) {
            throw new PermissionException();
        }
        //get products ids
        List<StatPurchaseProduct> statPurchaseProductList = purchaseDAO.selectStatPurchaseProducts(dateFrom, dateTo);
        List<Long> productIdList = new LinkedList<>();
        for(StatPurchaseProduct statPurchaseProduct : statPurchaseProductList) {
            Long productId = statPurchaseProduct.getTypeId();
            productIdList.add(productId);
        }
        //get products
        List<Product> productList = getProductDAO().selectByIdList(productIdList);
        Map<Long, Product> productIdToProduct = new TreeMap<>();
        for(Product product : productList) {
            productIdToProduct.put(product.getProductId(), product);
        }
        //set products
        for(StatPurchaseProduct statPurchaseProduct : statPurchaseProductList) {
            Long productId = statPurchaseProduct.getTypeId();
            statPurchaseProduct.setProduct(productIdToProduct.get(productId));
        }
        //get merchants
        Map<Long, List<StatPurchaseProduct>> merchantIdToStatPurchaseProductList = new HashMap<>();
        for (StatPurchaseProduct statPurchaseProduct : statPurchaseProductList) {
            long merchantProfileId = statPurchaseProduct.getProduct().getMerchantProfileId();
            List<StatPurchaseProduct> merchantStatPurchaseProductList = merchantIdToStatPurchaseProductList.get(merchantProfileId);
            if(merchantStatPurchaseProductList == null) {
                merchantStatPurchaseProductList = new LinkedList<>();
                merchantIdToStatPurchaseProductList.put(merchantProfileId, merchantStatPurchaseProductList);
            }
            merchantStatPurchaseProductList.add(statPurchaseProduct);
        }
        //set merchants
        List<Profile> merchantList = getProfileDAO().selectSearchedByIds(merchantIdToStatPurchaseProductList.keySet(), 0L);
        for(Profile merchantProfile : merchantList) {
            List<StatPurchaseProduct> merchantStatPurchaseProductList = merchantIdToStatPurchaseProductList.get(merchantProfile.getProfileId());
            for(StatPurchaseProduct statPurchaseProduct : merchantStatPurchaseProductList) {
                statPurchaseProduct.setMerchantProfile(merchantProfile);
            }
        }
        //get amount and count for previous period
        Date previousDateTo = dateFrom;
        Date previousDateFrom = new Date(dateFrom.getTime() - (dateTo.getTime() - dateFrom.getTime()));
        List<StatPurchaseProduct> previousStatPurchaseProductList = purchaseDAO.selectStatPurchaseProducts(previousDateFrom, previousDateTo);
        //set difference with current period
        setDiff(statPurchaseProductList, previousStatPurchaseProductList);
        //
        Collections.sort(statPurchaseProductList);
        //
        return statPurchaseProductList;
    }

    public DateRange getPreviousRange(Date dateFrom, Date dateTo) {
        Date previousDateFrom = new Date(dateFrom.getTime() - (dateTo.getTime() - dateFrom.getTime()));
        return new DateRange(previousDateFrom, dateFrom);
    }

    @Override
    public <T extends StatPurchaseCommon> void setDiff(List<T> current, List<T> previous) {
        for(StatPurchaseCommon statPurchase : current) {
            int previousIndex = previous.indexOf(statPurchase);
            if(previousIndex >= 0) {
                StatPurchaseCommon previousStatPurchase = previous.get(previousIndex);
                BigDecimal amountDiff = statPurchase.getAmount().add(previousStatPurchase.getAmount().negate());
                statPurchase.setAmountDiff(amountDiff);
                Integer countDiff = statPurchase.getCount() - previousStatPurchase.getCount();
                statPurchase.setCountDiff(countDiff);
            } else {
                statPurchase.setAmountDiff(null);
                statPurchase.setCountDiff(null);
            }
        }
    }

    @Override
    public List<StatCampaignsForecast> getCampaignsForecast(long clientId) throws PermissionException {
        if (!permissionService.hasAdministrativeRole(clientId)) {
            throw new PermissionException();
        }
        List<StatCampaignsForecast> result = purchaseDAO.selectForecast();
        if (result != null && !result.isEmpty()) {
            Collections.sort(result, new Comparator<StatCampaignsForecast>() {
                @Override
                public int compare(StatCampaignsForecast o1, StatCampaignsForecast o2) {
                    BigDecimal diff1 = o1.getPlanedAmount().add(o1.getTargetAmount().negate());
                    BigDecimal diff2 = o2.getPlanedAmount().add(o2.getTargetAmount().negate());
                    return diff1.compareTo(diff2);
                }
            });
        }
        return result;
    }

}

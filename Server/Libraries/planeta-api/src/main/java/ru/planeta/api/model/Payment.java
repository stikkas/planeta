package ru.planeta.api.model;

import java.math.BigDecimal;

/**
 * Class Payment
 *
 * @author a.tropnikov
 */
public class Payment {

    private BigDecimal amount;
    private String currency;
    private String merchantUrl;

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getMerchantUrl() {
        return merchantUrl;
    }

    public void setMerchantUrl(String merchantUrl) {
        this.merchantUrl = merchantUrl;
    }
}

package ru.planeta.api.service.billing.payment.settings;

import ru.planeta.model.common.PaymentMethod;
import ru.planeta.model.common.PaymentProvider;
import ru.planeta.model.enums.ProjectType;

import java.util.List;

/**
 * Service loads, stores and provides access to payment settings.<br>
 * Created by eshevchenko.
 */
public interface PaymentSettingsService {


    List<PaymentMethod> getPaymentMethods(ProjectType projectType, Boolean internal, Boolean promo);
// После перехода на новую платежку всех проектов это можно убрать.
    List<PaymentMethod> getPaymentMethodsOld(ProjectType projectType, Boolean internal, Boolean promo);

    List<PaymentMethod> getPaymentMethodsPlain(ProjectType projectType, Boolean internal, Boolean promo);

    PaymentMethod getInternalPaymentMethod();

    PaymentMethod getPromoPaymentMethod();

    List<PaymentMethod> getAllPaymentMethods();

    List<PaymentProvider> getAllProviders();
}

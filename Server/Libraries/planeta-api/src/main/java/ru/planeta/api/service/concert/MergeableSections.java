package ru.planeta.api.service.concert;

import ru.planeta.api.exceptions.MoscowShowInteractionException;
import ru.planeta.api.exceptions.NotFoundException;
import ru.planeta.model.concert.Section;
import ru.planeta.moscowshow.model.StructElement;

import javax.annotation.Nonnull;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: michail
 * Date: 31.10.2016
 * Time: 13:35
 */

public class MergeableSections implements Mergeable<Section, StructElement> {
    private SectionService sectionService;
    private List<Section> planetaList;
    private List<StructElement> msList;
    private long externalConcertId;


    @Override
    public int compare(Section section, StructElement structElement) {
        return Long.compare(section.getExternalSectionId(), structElement.getId());
    }

    @Override
    public void insert(StructElement structElement) throws NotFoundException, MoscowShowInteractionException {
        Section section = getSection(structElement);
        section.setExternalConcertId(externalConcertId);
        sectionService.insertOrUpdateSection(section);
    }

    @Override
    public void update(Section section, StructElement structElement) throws NotFoundException, MoscowShowInteractionException {
        section = updateSection(structElement, section);
        sectionService.insertOrUpdateSection(section);
    }

    @Override
    public void delete(Section section) {
        sectionService.markSectionDeleted(section);
    }

    @Override
    public List<Section> getInternalList() {
        return planetaList;
    }

    @Override
    public List<StructElement> getExternalList() {
        return msList;
    }

    private static Section updateSection(@Nonnull StructElement structElement, @Nonnull Section section) {
        section.setName(structElement.getName());
        section.setExternalSectionId(structElement.getId());
        section.setExternalParentId(structElement.getParentId());
        return section;
    }

    private static Section getSection(@Nonnull StructElement structElement) {
        return updateSection(structElement, new Section());
    }


    public MergeableSections(List<Section> planetaList, List<StructElement> msList, SectionService sectionService, long externalConcertId) {
        this.planetaList = planetaList;
        this.msList = msList;
        this.sectionService = sectionService;
        this.externalConcertId = externalConcertId;
    }
}

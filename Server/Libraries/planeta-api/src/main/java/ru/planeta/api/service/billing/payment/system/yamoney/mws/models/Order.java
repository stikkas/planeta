package ru.planeta.api.service.billing.payment.system.yamoney.mws.models;

import ru.planeta.api.service.billing.payment.system.yamoney.mws.models.enums.AvisoStatus;
import ru.planeta.api.service.billing.payment.system.yamoney.mws.models.enums.CurrencyBank;
import ru.planeta.api.service.billing.payment.system.yamoney.mws.models.enums.CurrencyCode;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by eshevchenko.
 */
@XmlType(name = "order")
@XmlAccessorType(XmlAccessType.FIELD)
public class Order {

    @XmlAttribute
    public Long shopId;
    @XmlAttribute
    public String shopName;
    @XmlAttribute
    public Long articleId;
    @XmlAttribute
    public String articleName;
    @XmlAttribute
    public Long invoiceId;
    @XmlAttribute
    public String orderNumber;
    @XmlAttribute
    public String paymentSystemOrderNumber;
    @XmlAttribute
    public String customerNumber;
    @XmlAttribute
    public Date createdDatetime;
    @XmlAttribute
    public Boolean paid;
    @XmlAttribute
    public BigDecimal orderSumAmount;
    @XmlAttribute
    public CurrencyCode orderSumCurrencyPaycash;
    @XmlAttribute
    public CurrencyBank orderSumBankPaycash;
    @XmlAttribute
    public BigDecimal paidSumAmount;
    @XmlAttribute
    public CurrencyCode paidSumCurrencyPaycash;
    @XmlAttribute
    public CurrencyBank paidSumBankPaycash;
    @XmlAttribute
    public BigDecimal receivedSumAmount;
    @XmlAttribute
    public CurrencyCode receivedSumCurrencyPaycash;
    @XmlAttribute
    public CurrencyBank receivedSumBankPaycash;
    @XmlAttribute
    public BigDecimal shopSumAmount;
    @XmlAttribute
    public CurrencyCode shopSumCurrencyPaycash;
    @XmlAttribute
    public CurrencyBank shopSumBankPaycash;
    @XmlAttribute
    public Date paymentDatetime;
    @XmlAttribute
    public Date paymentAuthorizationTime;
    @XmlAttribute
    public String payerCode;
    @XmlAttribute
    public String payerAddress;
    @XmlAttribute
    public String payeeCode;
    @XmlAttribute
    public Date paymentSystemDatetime;
    @XmlAttribute
    public Date avisoReceivedDatetime;
    @XmlAttribute
    public AvisoStatus avisoStatus;
}

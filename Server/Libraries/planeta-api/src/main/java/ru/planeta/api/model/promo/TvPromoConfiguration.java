package ru.planeta.api.model.promo;

/**
 * User: sshendyapin
 * Date: 04.02.13
 * Time: 16:24
 */
public class TvPromoConfiguration extends BasePromoConfiguration {

    private String imageUrl;
    private String title;
    private String text;
    private String btnLink;

    private String html;

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getHtml() {
        return html;
    }

    public void setHtml(String html) {
        this.html = html;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getBtnLink() {
        return btnLink;
    }

    public void setBtnLink(String btnLink) {
        this.btnLink = btnLink;
    }
}

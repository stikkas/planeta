package ru.planeta.api.text;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.planeta.api.service.content.VideoCacheService;
import ru.planeta.model.common.CachedVideo;

/**
 * Created with IntelliJ IDEA.
 * User: a.volkov
 * Date: 18.02.13
 * Time: 13:15
 */
@Component
public class VideoAttachmentExtractor implements AttachmentExtractor {

    @Autowired
    private VideoCacheService videoCacheService;

    @Override
    public Attachment extract(String url) {
        CachedVideo cachedVideo = videoCacheService.getVideo(url);

        if (cachedVideo == null) {
            return null;
        }

        VideoAttachment videoAttachment = new VideoAttachment();
        videoAttachment.setImageUrl(cachedVideo.getThumbnailUrl());
        videoAttachment.setDuration((int) cachedVideo.getDuration());
        videoAttachment.setName(cachedVideo.getTitle());
        videoAttachment.setDescription(cachedVideo.getDescription());
        videoAttachment.setVideoUrl(cachedVideo.getUrl());
        videoAttachment.setUrl(url);
        videoAttachment.setVideoType(cachedVideo.getVideoType());
        return videoAttachment;
    }
}

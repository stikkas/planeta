package ru.planeta.api.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.apache.commons.lang3.StringUtils;
import ru.planeta.model.common.UserPrivateInfo;
import ru.planeta.model.enums.UserStatus;
import ru.planeta.model.profile.Profile;
import ru.planeta.model.profile.User;

import java.io.Serializable;
import java.util.EnumSet;

/**
 * Class that contains all user's authorization info
 *
 * @author ameshkov
 */
public class UserAuthorizationInfo implements Serializable {

    private static final long serialVersionUID = 1L;

    public UserAuthorizationInfo() {
    }

    public UserAuthorizationInfo(Profile profile, User user, UserPrivateInfo userPrivateInfo) {
        this.profile = profile;
        this.user = user;
        this.userPrivateInfo = userPrivateInfo;
    }

    private Profile profile;
    @JsonIgnore
    private transient User user;
    @JsonIgnore
    private UserPrivateInfo userPrivateInfo;

    public Profile getProfile() {
        return profile;
    }

    public void setProfile(Profile profile) {
        this.profile = profile;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public UserPrivateInfo getUserPrivateInfo() {
        return userPrivateInfo;
    }

    public void setUserPrivateInfo(UserPrivateInfo userPrivateInfo) {
        this.userPrivateInfo = userPrivateInfo;
    }

    @JsonIgnore
    public String getPassword() {
        return userPrivateInfo == null ? null : userPrivateInfo.getPassword();
    }

    public String getUsername() {
        return userPrivateInfo == null ? null : (StringUtils.isBlank(userPrivateInfo.getUsername()) ? userPrivateInfo.getEmail().toLowerCase() : userPrivateInfo.getUsername());
    }

    public EnumSet<UserStatus> getUserStatus() {
        return userPrivateInfo == null ? null : userPrivateInfo.getUserStatus();
    }
}

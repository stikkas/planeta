package ru.planeta.api.service.billing.payment.system.yamoney.mws.operations;

import ru.planeta.api.service.billing.payment.system.yamoney.mws.MwsAccount;
import ru.planeta.api.service.billing.payment.system.yamoney.mws.exceptions.YaMoException;
import ru.planeta.api.service.billing.payment.system.yamoney.mws.models.BaseResponse;
import ru.planeta.api.service.billing.payment.system.yamoney.mws.models.ListOrdersResponse;
import ru.planeta.api.service.billing.payment.system.yamoney.mws.models.ListReturnsResponse;
import ru.planeta.api.service.billing.payment.system.yamoney.mws.models.RepeatCardPaymentResponse;
import ru.planeta.api.service.billing.payment.system.yamoney.mws.utils.MarshallingUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.Consts;
import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.message.BasicNameValuePair;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * YandexMoney MWS informational operation.<br>
 * Provides methods for creating following operations:
 * <ul>
 *     <li>listOrders;</li>
 *     <li>listReturns;</li>
 * </ul>
 * Created by eshevchenko.
 */
public final class InfoOperation<S extends BaseResponse> extends BaseOperation<InfoOperation.YaMoneyForm, S> {

    protected InfoOperation(OperationName operationName, Class<S> responseClass, MwsAccount account, YaMoneyForm form) {
        super(operationName, form, responseClass, account);
    }

    @Override
    protected HttpEntity createRequestEntity(YaMoneyForm form) throws YaMoException {
        return new UrlEncodedFormEntity(form.build(), Consts.UTF_8);
    }

    static class YaMoneyForm {
        private final List<NameValuePair> params = new ArrayList<NameValuePair>();

        public static YaMoneyForm common(MwsAccount account) {
            return new YaMoneyForm()
                    .add("requestDT", MarshallingUtils.formatToISO8601(new Date()))
                    .add("shopId", account.getShopId().toString());
        }

        public YaMoneyForm add(String name, Object value) {
            if (StringUtils.isNotBlank(name) && (value != null)) {
                params.add(new BasicNameValuePair(name, value.toString()));
            }
            return this;
        }

        public List<NameValuePair> build() {
            return new ArrayList<NameValuePair>(params);
        }
    }

    public static InfoOperation<ListOrdersResponse> listOrders(MwsAccount account, Long invoiceId, Long orderNumber, Date orderCreatedDatetimeGreaterOrEqual, Date orderCreatedDatetimeLessOrEqual, Boolean paid, Date paymentDatetimeGreaterOrEqual, Date paymentDatetimeLessOrEqual) {
        return new InfoOperation<ListOrdersResponse>(OperationName.listOrders, ListOrdersResponse.class, account, YaMoneyForm.common(account)
                .add("orderCreatedDatetimeGreaterOrEqual", MarshallingUtils.formatToISO8601(orderCreatedDatetimeGreaterOrEqual))
                .add("orderCreatedDatetimeLessOrEqual", MarshallingUtils.formatToISO8601(orderCreatedDatetimeLessOrEqual))
                .add("paid", paid)
                .add("paymentDatetimeGreaterOrEqual", MarshallingUtils.formatToISO8601(paymentDatetimeGreaterOrEqual))
                .add("paymentDatetimeLessOrEqual", MarshallingUtils.formatToISO8601(paymentDatetimeLessOrEqual))
                .add("invoiceId", invoiceId)
                .add("orderNumber", orderNumber));
    }

    public static InfoOperation<ListReturnsResponse> listReturns(MwsAccount account, Integer status, Boolean partial, Date from, Date till) {
        return new InfoOperation<ListReturnsResponse>(OperationName.listReturns, ListReturnsResponse.class, account, YaMoneyForm.common(account)
                .add("from", MarshallingUtils.formatToISO8601(from))
                .add("till", MarshallingUtils.formatToISO8601(till))
                .add("status", status)
                .add("partial", partial));
    }

    public static InfoOperation<RepeatCardPaymentResponse> repeatCardPayment(MwsAccount account, long clientOrderId, long invoiceId, BigDecimal amount, String orderNumber, String cvv) {
        return new InfoOperation<RepeatCardPaymentResponse>(OperationName.repeatCardPayment, RepeatCardPaymentResponse.class, account, new YaMoneyForm()
                .add("clientOrderId", clientOrderId)
                .add("invoiceId", invoiceId)
                .add("amount", amount)
                .add("cvv", cvv)
                .add("orderNumber", orderNumber));
    }
}

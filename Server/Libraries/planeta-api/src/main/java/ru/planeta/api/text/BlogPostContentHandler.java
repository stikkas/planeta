package ru.planeta.api.text;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;
import ru.planeta.api.Utils;

import java.util.*;

import static ru.planeta.api.Utils.empty;
import static ru.planeta.api.text.RichMediaObject.Type;

/**
 * Created with IntelliJ IDEA.
 * User: a.volkov
 * Date: 18.04.13
 * Time: 12:31
 */
class BlogPostContentHandler extends DefaultHandler {
    private static final char DOTS = '\u2026';

    private static final String LEADING_PATTERN = "^((\\s|&nbsp;)*<br/>(\\s|&nbsp;)*)+";
    private static final String TAIL_PATTERN = "((\\s|&nbsp;)*<br/>(\\s|&nbsp;)*)+$";

    private static final int ANNOTATION_LENGTH = 400;

    private static final char OPEN_TAG_START = '<';
    private static final char OPEN_TAG_END = '>';
    private static final String CLOSE_TAG_START = "</";
    private static final char CLOSE_TAG_END = '>';
    private static final String EMPTY_TAG_END = "/>";
    private static final char NBSP = '\u00A0';

    private static final String BR_TAG = "br";

    private static final Logger log = Logger.getLogger(BlogPostContentHandler.class);

    private static final Collection<String> SUPPORTED_TAGS = Arrays.asList("b", "u", "i", "s", "a", "br", "strong", "em", "span");
    private static final Collection<String> MEDIA_TAGS = Arrays.asList(Type.PHOTO.getValue(), Type.VIDEO.getValue(), Type.AUDIO.getValue(), "img");
    private static final Collection<String> IGNORED_ATTRS = Arrays.asList("shape", "clear", "style");
    private static final Collection<String> FORCE_BR_BEFORE_TAGS = Arrays.asList("p", "ul", "ol", "div", Type.PHOTO.getValue(), Type.VIDEO.getValue(), Type.AUDIO.getValue(), "img");
    private static final Collection<String> FORCE_BR_AFTER_TAGS = FORCE_BR_BEFORE_TAGS;

    private int position;
    private StringBuilder text;
    private boolean processed;
    private Deque<TagInfo> tags;
    private StringBuilder tagBody = null;
    private int mediaTagsCount = 0;
    private boolean hasText = false;
    private boolean hasTags = false;
    private String lastTagName = StringUtils.EMPTY;
    private String lastText = StringUtils.EMPTY;

    private static class TagInfo {
        public String name;
        public boolean hasChild = false;
        public Attributes attributes;
        public boolean ignored = false;

        private TagInfo(String name, Attributes attributes) {
            this.name = name;
            this.attributes = attributes;
        }
    }

    @Override
    public void startDocument() throws SAXException {
        text = new StringBuilder();
        tagBody = new StringBuilder();
        processed = false;
        tags = new ArrayDeque<>();
    }

    @Override
    public void endDocument() throws SAXException {
        if (Utils.empty(tags) && !Utils.empty(tagBody)) {
            writeTagBody();
        }

        while (!Utils.empty(tags)) {
            onTagEnd(tags.getLast());
        }

        if (hasText || hasTags) {
            text = new StringBuilder(trim(text.toString()));
        }
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
//        log.info("start element: " + qName);
        if (processed) {
            return;
        }
        if (FORCE_BR_BEFORE_TAGS.contains(qName.toLowerCase()) && (hasTags || hasText)) {
            TagInfo br = new TagInfo(BR_TAG, null);
            onTagStart(br);
            onTagEnd(br);
        }
        if (isTagSupported(qName)) {
            onTagStart(new TagInfo(qName, attributes));
        }
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
//        log.info("end element: " + qName);
        if (processed) {
            return;
        }
        if (isTagSupported(qName)) {
            onTagEnd(tags.getLast());
        }
        if (FORCE_BR_AFTER_TAGS.contains(qName.toLowerCase())) {
            TagInfo br = new TagInfo(BR_TAG, null);
            onTagStart(br);
            onTagEnd(br);
        }
    }

    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        if (processed) {
            return;
        }
        // TODO rewrite this shit
        String text;
        if (position + length < ANNOTATION_LENGTH) {
            text = new String(ch, start, length);
        } else {
            processed = true;
            text = new String(ch, start, Math.min(length, ANNOTATION_LENGTH - position));
            int whitespace = lastIndexOfWhitespace(text);
            if (whitespace > 0) {
                text = text.substring(0, whitespace);
            } else {
                text = StringUtils.EMPTY;
            }
            text += DOTS;
        }

        if (!isBlank(text) || ((hasText || hasTags) && !isBlank(lastText))) {
            String plainText = simplify(text);
            String escaped = StringEscapeUtils.escapeHtml4(plainText);
            position += plainText.length();
            tagBody.append(escaped);
            hasText = true;
            lastText = plainText;
            if (!isBlank(text)) {
                lastTagName = StringUtils.EMPTY;
            }
        }
    }

    public String getText() {
        return text.toString();
    }

    private void onTagStart(TagInfo tag) {
        log.debug("tag start: " + tag.name);

        tag.ignored = lastTagName.equalsIgnoreCase(BR_TAG) && tag.name.equalsIgnoreCase(BR_TAG);

        boolean isMediaTag = isMediaTag(tag.name);
        if (isMediaTag && mediaTagsCount > 0) {
            writeTagBody();
            processed = true;
        } else {
            writeTagStart(tag);
            if (isMediaTag) {
                ++mediaTagsCount;
            }
        }
        lastTagName = tag.name;
        hasTags = true;
    }

    private void onTagEnd(TagInfo tag) {
        log.debug("tag end: " + tag.name);
        TagInfo current = tags.removeLast();
        writeTagEnd(current);
    }

    private static boolean isTagSupported(String qName) {
        return SUPPORTED_TAGS.contains(qName) || MEDIA_TAGS.contains(qName);
    }

    private static boolean isMediaTag(String qName) {
        return MEDIA_TAGS.contains(qName);
    }

    private static String simplify(CharSequence s) {
        if (Utils.empty(s)) {
            return StringUtils.EMPTY;
        }
        StringBuilder res = new StringBuilder(s.length());
        int state = 0;
        for (int i = 0; i < s.length(); ++i) {
            char c = s.charAt(i);
            switch (state) {
                case 0:
                    if (isWhitespace(c)) {
                        state = 1;
                    }
                    res.append(c);
                    break;
                case 1:
                    if (!isWhitespace(c)) {
                        state = 0;
                        res.append(c);
                    }
                    break;
            }
        }
        return res.toString();
    }

    private static int lastIndexOfWhitespace(CharSequence in) {
        if (Utils.empty(in)) {
            return -1;
        }
        int index = -1;
        for (int i = in.length() - 1; i >= 0; --i) {
            if (isWhitespace(in.charAt(i))) {
                index = i;
                break;
            }
        }
        return index;
    }

    private void writeTagStart(TagInfo tag) {
        if (!Utils.empty(tags) && getLastChar(text) != OPEN_TAG_END) {
            TagInfo parent = tags.getLast();
            parent.hasChild = true;
            text.append(OPEN_TAG_END);
        }
        writeTagBody();
        tags.add(tag);
        if (!tag.ignored) {
            text.append(OPEN_TAG_START).append(tag.name);
            writeAttributes(tag.attributes);
        }
    }

    private void writeTagEnd(TagInfo tag) {
        if (tag.ignored) {
            return;
        }
        boolean emptyBody = Utils.empty(tagBody);
        if (!emptyBody || tag.hasChild) {
            if (!emptyBody) {
                if(getLastChar(text) != CLOSE_TAG_END){
                    text.append(CLOSE_TAG_END);
                }
                writeTagBody();
            }
            text.append(CLOSE_TAG_START).append(tag.name).append(CLOSE_TAG_END);
        } else {
            text.append(EMPTY_TAG_END);
        }
    }

    private static char getLastChar(CharSequence text) {
        return text.charAt(text.length() - 1);
    }

    private void writeTagBody() {
        text.append(tagBody);
        tagBody = new StringBuilder();
    }

    private void writeAttributes(Attributes attributes) {
        if (attributes == null || attributes.getLength() == 0) {
            return;
        }
        for (int i = 0; i < attributes.getLength(); ++i) {
            if (!IGNORED_ATTRS.contains(attributes.getQName(i))) {
                text.append(' ').append(attributes.getQName(i)).append("=\"").append(StringEscapeUtils.escapeHtml4(attributes.getValue(i))).append('"');
            }
        }
    }

    private static String trim(String in) {
        return in.trim().replaceAll(LEADING_PATTERN, StringUtils.EMPTY).replaceAll(TAIL_PATTERN, StringUtils.EMPTY);
    }

    private static boolean isWhitespace(char c) {
        return Character.isWhitespace(c) || c == NBSP;
    }

    private static boolean isBlank(CharSequence s) {
        if (Utils.empty(s)) {
            return true;
        }
        boolean blank = true;
        for (int i = 0; blank && i < s.length(); ++i) {
            blank = isWhitespace(s.charAt(i));
        }
        return blank;
    }
}

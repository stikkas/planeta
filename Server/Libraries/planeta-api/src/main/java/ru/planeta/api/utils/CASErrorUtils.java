package ru.planeta.api.utils;

/**
 * Created by IntelliJ IDEA.
 * User: m.shulepov
 * Date: 05.03.12
 * Time: 16:39
 */
public class CASErrorUtils {

    public static enum CASError {

        BAD_CREDENTIALS("error.authentication.credentials.bad", "101"),

        BAD_PASSWORD("error.authentication.credentials.bad.usernameorpassword.password", "102"),

        BAD_USERNAME_OR_PASSWORD("error.authentication.credentials.bad.usernameorpassword", "103"),

        BLOCKED_CREDENTIALS("error.authentication.credentials.blocked", "104"),

        UNKNOWN_USERNAME("error.authentication.credentials.bad.usernameorpassword.username", "105"),

        UNSUPPORTED_CREDENTIALS("error.authentication.credentials.unsupported", "106");

        private String messageCode;
        private String errorCode;

        CASError(String messageCode, String errorCode) {
            this.messageCode = messageCode;
            this.errorCode = errorCode;
        }

        public String getMessageCode() {
            return messageCode;
        }

        public String getErrorCode() {
            return errorCode;
        }
    }

    public static CASError getCASErrorByErrorCode(String errorCode) {
        for (CASError error: CASError.values()) {
            if (error.getErrorCode().equals(errorCode)) {
                return error;
            }
        }
        return null;
    }

    public static CASError getCASErrorByMessageCode(String messageCode) {
        for (CASError error: CASError.values()) {
            if (error.getMessageCode().equals(messageCode)) {
                return error;
            }
        }
        return null;
    }

}

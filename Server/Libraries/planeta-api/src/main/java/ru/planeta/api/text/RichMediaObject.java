package ru.planeta.api.text;

import java.util.HashMap;
import java.util.Map;

public class RichMediaObject {

    public static enum Type {
        PHOTO("p:photo"), VIDEO("p:video"), AUDIO("p:audio");

        private final String value;

        Type(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }

        private static final Map<String, Type> lookup = new HashMap<String, Type>();

        static {
            for (Type type : values()) {
                lookup.put(type.value, type);
            }
        }

        public static Type getTypeByValue(String value) {
            return lookup.get(value);
        }
    }

    private Type type;
    private long id;
    private long owner;
    private Map<String, String> params;

    public RichMediaObject(Type type, long id, long owner, Map<String, String> params) {
        this.type = type;
        this.id = id;
        this.owner = owner;
        this.params = params;
    }

    public Type getType() {
        return type;
    }

    public long getId() {
        return id;
    }

    public long getOwner() {
        return owner;
    }

    public String getParam(String key) {
        return params.get(key);
    }
}

package ru.planeta.api.service.quiz;

import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.planeta.api.exceptions.NotFoundException;
import ru.planeta.dao.profiledb.ArrayListWithCount;
import ru.planeta.dao.trashcan.QuizDAO;
import ru.planeta.model.trashcan.*;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;

@Service
public class QuizServiceImpl implements QuizService{
    @Autowired
    private QuizDAO quizDAO;

    @Override
    public void addOrUpdate(Quiz quiz) {
        Quiz selectedQuiz = quizDAO.selectQuiz(String.valueOf(quiz.getQuizId()), null);
        if (selectedQuiz == null) {
            quiz.setTimeAdded(new Date());
            quizDAO.insert(quiz);
        } else {
            quizDAO.update(quiz);
        }
    }

    @Override
    public void addQuizQuestion(QuizQuestion quizQuestion) {
        quizDAO.insert(quizQuestion);
    }

    @Override
    public void addQuizQuestionRelation(long quizId, long quizQuestionId, long quizQuestionOrderNum) {
        quizDAO.insertQuizQuestionRelation(quizId, quizQuestionId, quizQuestionOrderNum);
    }

    @Override
    public void addQuizQuestionRelation(long quizId, long quizQuestionId) {
        long quizQuestionOrderNum = quizDAO.selectNextQuizQuestionOrderNumInQuiz(quizId);
        addQuizQuestionRelation(quizId, quizQuestionId, quizQuestionOrderNum);
    }

    @Override
    public void addOrUpdateQuizQuestionOption(@NotNull QuizQuestionOption quizQuestionOption) {
        if (quizQuestionOption.getQuizQuestionOptionId() == 0) {
            long quizQuestionOptionOrderNum = quizDAO.selectNextQuizQuestionOptionOrderNumInQuiz(quizQuestionOption.getQuizQuestionId());
            quizQuestionOption.setOptionOrderNum((int)quizQuestionOptionOrderNum);
            quizDAO.insertQuizQuestionOption(quizQuestionOption);
        } else {
            quizDAO.updateQuizQuestionOption(quizQuestionOption);
        }
    }

    @Override
    public void setQuizQuestionEnabling(long quizQuestionId, boolean enabled) {
        quizDAO.updateQuizQuestionEnabling(quizQuestionId, enabled);
    }

    @Override
    public void deleteQuizQuestionOption(long quizQuestionOptionId) {
        quizDAO.deleteQuizQuestionOption(quizQuestionOptionId);
    }

    @Override
    public ArrayListWithCount<Quiz> getQuizzesSearch(String searchString, int offset, int limit) {
        return quizDAO.selectQuizzes(searchString, offset, limit);
    }

    @Override
    public List<QuizQuestion> getQuizQuestions(int offset, int limit) {
        return quizDAO.selectQuizQuestions(offset, limit);
    }

    @Override
    public List<QuizQuestion> getQuizQuestionsByQuizId(long quizId, Boolean enabled) {
        return quizDAO.selectQuizQuestionsByQuizId(quizId, enabled);
    }

    @Override
    public QuizQuestion getQuizQuestionById(long quizQuestionId) {
        return quizDAO.selectQuizQuestionById(quizQuestionId);
    }

    @Override
    @Nonnull
    public QuizQuestion getQuizQuestionSafe(long quizQuestionId) throws NotFoundException {
        QuizQuestion quizQuestion = getQuizQuestionById(quizQuestionId);
        if (quizQuestion == null) {
            throw new NotFoundException(QuizQuestion.class, quizQuestionId);
        }
        return quizQuestion;
    }

    @Override
    public void updateQuizQuestion(QuizQuestion quizQuestion) {
        quizDAO.updateQuizQuestion(quizQuestion);
    }

    @Override
    public void updateQuizQuestionsOrder(long quizId, long quizQuestionId, int newOrderNum, int oldOrderNum) {
        quizDAO.updateQuizQuestionsOrder(quizId, quizQuestionId, newOrderNum, oldOrderNum);
    }

    @Override
    public void updateQuizQuestionOptionsOrder(long quizQuestionOptionId, long quizQuestionId, int newOrderNum, int oldOrderNum) {
        quizDAO.updateQuizQuestionOptionsOrder(quizQuestionOptionId, quizQuestionId, newOrderNum, oldOrderNum);
    }

    @Override
    public void updateQuizQuestionOption(QuizQuestionOption quizQuestionOption) {
        quizDAO.updateQuizQuestionOption(quizQuestionOption);
    }

    @Override
    public Quiz getQuiz(String quizAlias, @Nullable Boolean enabled) {
        return quizDAO.selectQuiz(quizAlias, enabled);
    }

    @Override
    @Nonnull
    public Quiz getQuizSafe(String quizAlias, @Nullable Boolean enabled) throws NotFoundException {
        Quiz quiz = getQuiz(quizAlias, enabled);
        if (quiz == null) {
            long quizId = NumberUtils.toLong(quizAlias, -1);
            if (quizId == -1) {
                throw new NotFoundException(Quiz.class, "alias", quizAlias);
            } else {
                throw new NotFoundException(Quiz.class, quizId);
            }
        }
        return quiz;
    }

    @Override
    public void addQuizAnswers(List<QuizAnswer> quizAnswers, long userId) {
        for(QuizAnswer quizAnswer : quizAnswers) {
            if (!isQuizAnswerExists(quizAnswer.getQuizId(), quizAnswer.getQuizQuestionId(), userId)) {
                quizAnswer.setUserId(userId);
                quizAnswer.setTimeAdded(new Date());
                quizDAO.insertQuizAnswer(quizAnswer);
            }
        }

        return;
    }

    @Override
    public List<QuizAnswer> getQuizAnswersByParams(@Nullable Long quizId,
                                                   @Nullable Long quizQuestionId,
                                                   @Nullable Long userId,
                                                   long offset, long limit) {
        return quizDAO.selectQuizAnswersByParams(quizId, quizQuestionId, userId, offset, limit);
    }

    @Override
    public List<QuizAnswerForReport> getQuizAnswersForReportByParams(@Nullable Long quizId,
                                                                     @Nullable Long userId,
                                                                     long offset, long limit) {
        return quizDAO.selectQuizAnswersForReportByParams(quizId, userId, offset, limit);
    }

    private boolean isQuizAnswerExists(long quizId, long quizQuestionId, long userId) {
        QuizAnswer quizAnswer = quizDAO.selectQuizAnswerByParams(quizId, quizQuestionId, userId);
        return quizAnswer != null ? true : false;
    }
}

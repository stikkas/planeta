package ru.planeta.api.service.billing.payment.system.yamoney.mws.operations;

import ru.planeta.api.service.billing.payment.system.yamoney.mws.MwsAccount;
import ru.planeta.api.service.billing.payment.system.yamoney.mws.exceptions.YaMoException;
import ru.planeta.api.service.billing.payment.system.yamoney.mws.models.*;
import ru.planeta.api.service.billing.payment.system.yamoney.mws.models.enums.CurrencyCode;
import ru.planeta.api.service.billing.payment.system.yamoney.mws.utils.MarshallingUtils;
import ru.planeta.api.service.billing.payment.system.yamoney.mws.utils.SecurityUtils;
import org.apache.http.Consts;
import org.apache.http.HttpEntity;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.bouncycastle.cms.CMSProcessableByteArray;
import org.bouncycastle.cms.CMSSignedData;

import java.math.BigDecimal;

/**
 * YandexMoney MWS financial operation.<br>
 * Provides methods for creating following operations:
 * <ul>
 *     <li>cancelPayment;</li>
 *     <li>confirmPayment;</li>
 *     <li>repeatCardPayment;</li>
 *     <li>returnPayment;</li>
 * </ul>
 * Created by eshevchenko.
 */
public final class FinancialOperation<Q extends BaseRequest, S extends BaseResponse> extends BaseOperation<Q, S> {

    private static final ContentType PKCS7_CONTENT_TYPE = ContentType.create("application/pkcs7-mime", Consts.UTF_8);

    protected FinancialOperation(OperationName operationName, Q request, Class<S> responseClass, MwsAccount account) {
        super(operationName, request, responseClass, account);
    }

    @Override
    protected HttpEntity createRequestEntity(Q request) throws YaMoException {
        try {
            byte[] xml = MarshallingUtils.marshall(request);
            CMSSignedData signedData = SecurityUtils.signData(new CMSProcessableByteArray(xml), getAccount().getPrivateKey(), getAccount().getCertificate());
            String body = SecurityUtils.pemEncode(signedData.getSignedContent().getContent());
            return new StringEntity(body, PKCS7_CONTENT_TYPE);
        } catch (Exception e) {
            throw new YaMoException(e);
        }
    }

    public static FinancialOperation<CancelPaymentRequest, CancelPaymentResponse> cancelPayment(MwsAccount account, long orderId) {
        CancelPaymentRequest request = CancelPaymentRequest.create(orderId);
        return new FinancialOperation<CancelPaymentRequest, CancelPaymentResponse>(OperationName.cancelPayment, request, CancelPaymentResponse.class, account);
    }

    public static FinancialOperation<ConfirmPaymentRequest, ConfirmPaymentResponse> confirmPayment(MwsAccount account, long orderId, BigDecimal amount, CurrencyCode currency) {
        ConfirmPaymentRequest request = ConfirmPaymentRequest.create(orderId, amount, currency);
        return new FinancialOperation<ConfirmPaymentRequest, ConfirmPaymentResponse>(OperationName.confirmPayment, request, ConfirmPaymentResponse.class, account);
    }

    public static FinancialOperation<RepeatCardPaymentRequest, RepeatCardPaymentResponse> repeatCardPayment(MwsAccount account, long clientOrderId, long invoiceId, BigDecimal amount, String orderNumber, String cvv) {
        RepeatCardPaymentRequest request = RepeatCardPaymentRequest.create(clientOrderId, invoiceId, amount, orderNumber, cvv);
        return new FinancialOperation<RepeatCardPaymentRequest, RepeatCardPaymentResponse>(OperationName.repeatCardPayment, request, RepeatCardPaymentResponse.class, account);
    }

    public static FinancialOperation<ReturnPaymentRequest, ReturnPaymentResponse> returnPayment(MwsAccount account, long clientOrderId, long invoiceId, BigDecimal amount, CurrencyCode currency, String сause) {
        ReturnPaymentRequest request = ReturnPaymentRequest.create(clientOrderId, invoiceId, account.getShopId(), amount, currency, сause);
        return new FinancialOperation<ReturnPaymentRequest, ReturnPaymentResponse>(OperationName.returnPayment, request, ReturnPaymentResponse.class, account);
    }

}

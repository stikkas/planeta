package ru.planeta.api.service.billing.payment.system.yamoney.mws.exceptions;

/**
 * Describes MWS network communication exception.<br>
 * Created by eshevchenko.
 */
public class CommunicationException extends YaMoException {

    public CommunicationException(Exception e) {
        super(e);
    }
}

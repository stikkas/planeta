package ru.planeta.api.helper;

import ru.planeta.commons.web.WebUtils;
import ru.planeta.model.common.InvestingOrderInfo;
import ru.planeta.model.common.TopayTransaction;
import ru.planeta.model.common.campaign.Campaign;
import ru.planeta.model.enums.ProjectType;
import ru.planeta.model.profile.Profile;

import javax.annotation.Nonnull;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: a.volkov
 * Date: 06.05.14
 * Time: 13:46
 */
public interface ProjectService {

    // use next method
    String getPaymentFailUrl(ProjectType projectType);

    String getPaymentFailUrl(ProjectType projectType, long transactionId);

    String getPaymentFailUrl(@Nonnull TopayTransaction transaction);

    String getPaymentSuccessUrl(ProjectType projectType, long transactionId);

    String getPaymentSuccessUrl(TopayTransaction transaction);

    String getPaymentSuccessUrlByOrder(ProjectType projectType, long orderId);

    String getPaymentGateRedirectUrl(TopayTransaction transaction);

    String getHost(ProjectType projectType);

    @Nonnull
    String getUrl(ProjectType projectType, @Nonnull String uri);

    String getUrlHttp(ProjectType projectType, @Nonnull String uri);

    String getUrl(ProjectType projectType, String uri, WebUtils.Parameters params);

    /**
     * deprecated Use previous method
     */
    @Deprecated
    String getUrl(ProjectType projectType, String uri, Map<String, String> params);

    @Nonnull
    String getUrl(ProjectType projectType);

    @Nonnull
    String getUrl(ProjectType projectType, boolean secured);

    String getCampaignUrl(Campaign campaign);

    String getInvoicePrintUrl(long transactionId);
    
    String getBiblioSertPrintUrl(long transactionId);

    String getUserUrl(Profile profile);

    String getInvestingOrderURL(InvestingOrderInfo investingOrderInfo);

    String getStaticBaseUrl(String param);

    String getStaticJsUrl(String url);
}

package ru.planeta.api.service.geo;

import ru.planeta.api.model.shop.ShoppingCartContactsDTO;
import ru.planeta.model.common.Address;
import ru.planeta.model.common.DeliveryAddress;

/**
 * Date: 31.10.2014
 * Time: 14:26
 */
public interface DeliveryAddressService {
    DeliveryAddress saveDeliveryAddress(DeliveryAddress deliveryAddress);

    DeliveryAddress getDeliveryAddress(long orderId);

    void updateOderId(long deliveryAdderessId, long orderId);

    DeliveryAddress createFromShopOrderInfoDTO(ShoppingCartContactsDTO dto);

    DeliveryAddress getDeliveryAddress(Address address, String customerName);
}

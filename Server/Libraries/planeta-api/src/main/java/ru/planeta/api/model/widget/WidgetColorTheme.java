package ru.planeta.api.model.widget;

import ru.planeta.api.model.enums.widget.WidgetBackgroundColor;
import ru.planeta.api.model.enums.widget.WidgetFontColor;

/**
 * Date: 07.08.13
 * Time: 16:47
 */
public class WidgetColorTheme {
    private WidgetBackgroundColor background;
    private WidgetFontColor font;

    public WidgetColorTheme(WidgetBackgroundColor background, WidgetFontColor font) {
        this.background = background;
        this.font = font;
    }

    public WidgetBackgroundColor getBackground() {
        return background;
    }

    public void setBackground(WidgetBackgroundColor background) {
        this.background = background;
    }

    public WidgetFontColor getFont() {
        return font;
    }

    public void setFont(WidgetFontColor font) {
        this.font = font;
    }

    @Override
    public String toString() {
        return background.toString() + " " + font.toString();
    }
}

package ru.planeta.api.model.enums.widget;

/**
 * Created with IntelliJ IDEA.
 * Date: 07.08.13
 * Time: 16:48
 */
public enum WidgetBackgroundColor {
    WHITE("bg-white"),
    BLACK("bg-black");

    private String style;

    private WidgetBackgroundColor(String style) {
        this.style = style;
    }

    @Override
    public String toString() {
        return style;
    }

    public String getAsString(){
        return toString();
    }
}

package ru.planeta.api.text;

/**
 * Represents an image attachment
 */
public class ImageAttachment extends VisualAttachment {
    private String thumbnailUrl;

    public String getThumbnailUrl() {
        return thumbnailUrl;
    }

    public void setThumbnailUrl(String thumbnailUrl) {
        this.thumbnailUrl = thumbnailUrl;
    }

    public String toString() {
        return toString("photo", "id", getObjectId(), "owner", getOwnerId(),
                "image", getUrl(), "width", getWidth(), "height", getHeight());
    }

}

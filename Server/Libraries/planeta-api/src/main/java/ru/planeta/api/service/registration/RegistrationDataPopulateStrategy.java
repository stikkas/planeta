package ru.planeta.api.service.registration;

import org.apache.commons.lang3.StringUtils;
import ru.planeta.api.geo.GeoResolverWebService;
import ru.planeta.commons.web.RegistrationData;
import ru.planeta.commons.model.Gender;
import ru.planeta.model.common.UserPrivateInfo;
import ru.planeta.model.profile.Profile;
import ru.planeta.model.stat.RequestStat;

public class RegistrationDataPopulateStrategy extends CityResolvingPopulateStrategy implements PopulateUserRegistrationInfoStrategy {
    private RegistrationData registrationData;
    private RequestStat requestStat;


    RegistrationDataPopulateStrategy(RegistrationData registrationData, GeoResolverWebService geoResolverWebService, RequestStat requestStat) {
        super(geoResolverWebService, requestStat == null ? null : requestStat.getIp());
        this.registrationData = registrationData;
        this.requestStat = requestStat;
    }

    @Override
    public void populateProfile(Profile profile) {
        if (registrationData != null) {
            profile.setUserGender(registrationData.getGender() == null ? Gender.NOT_SET : registrationData.getGender());
            profile.setUserBirthDate(registrationData.getUserBirthDate());
            profile.setDisplayName(StringUtils.stripToEmpty(registrationData.getFirstName()) + " " + StringUtils.stripToEmpty(registrationData.getLastName()));
        }

    }

    @Override
    public void populatePrivateInfo(UserPrivateInfo userPrivateInfo) {}

    @Override
    public RequestStat getRequestStat() {
        return requestStat;
    }
}

package ru.planeta.api.service.concert;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.planeta.dao.concertdb.SectionDAO;
import ru.planeta.model.concert.Section;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: michail
 * Date: 18.10.16
 * Time: 12:12
 */
@Service
public class SectionServiceImpl implements SectionService {
    @Autowired
    private SectionDAO sectionDAO;

    @Override
    public Section getSectionByExtId(long externalSectionId) {
        return sectionDAO.selectByExtId(externalSectionId);
    }

    @Override
    public int insertOrUpdateSection(Section section) {
        return section.getSectionId() > 0 ? updateSection(section) : insertSection(section);
    }

    @Override
    public List<Section> getSectionsByExternalConcertId(long extConcertId) {
        return sectionDAO.selectByExtConcertId(extConcertId);
    }

    @Override
    public List<Section> getSectorsByExternalConcertId(long extConcertId) {
        // sector has externalParentId = 0;
        return sectionDAO.selectSectorsByExtConcertId(extConcertId);
    }

    @Override
    public List<Section> getRowsByExternalConcertId(long extConcertId, Long extSectorId) {
        return sectionDAO.selectRowsByExtConcertId(extConcertId, extSectorId);
    }

    @Override
    public int markSectionDeleted(Section section) {
        section.setDeleted(true);
        return updateSection(section);
    }

    private int updateSection(Section section) {
        return sectionDAO.update(section);
    }

    private int insertSection(Section section) {
        return sectionDAO.insert(section);
    }
}

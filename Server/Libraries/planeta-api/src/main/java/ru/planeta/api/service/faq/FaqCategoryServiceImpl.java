package ru.planeta.api.service.faq;

import org.springframework.stereotype.Service;
import ru.planeta.dao.commondb.BaseFaqCategoryService;

/**
 * Created by kostiagn on 01.09.2015.
 */
@Service
public class FaqCategoryServiceImpl extends BaseFaqCategoryService implements FaqCategoryService {
}

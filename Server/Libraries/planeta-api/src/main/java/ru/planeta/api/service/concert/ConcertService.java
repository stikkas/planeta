package ru.planeta.api.service.concert;

import ru.planeta.api.exceptions.NotFoundException;
import ru.planeta.model.concert.Concert;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: michail
 * Date: 14.10.16
 * Time: 13:48
 */
public interface ConcertService {
    long getConcertsCount();

    int insertOrUpdateConcert(Concert concert);

    List<Concert> selectList(int offset, int limit) throws NotFoundException;

    List<Concert> getActiveConcerts(int offset, int limit) throws NotFoundException;

    Concert getConcertSafe(long concertId) throws NotFoundException;

    Concert getConcertByExternalId(long externalConcertId) throws NotFoundException;
}

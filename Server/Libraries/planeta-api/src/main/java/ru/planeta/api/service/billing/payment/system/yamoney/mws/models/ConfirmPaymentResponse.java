package ru.planeta.api.service.billing.payment.system.yamoney.mws.models;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by eshevchenko on 25.04.14.
 */
@XmlRootElement
public class ConfirmPaymentResponse extends BaseResponse {
}

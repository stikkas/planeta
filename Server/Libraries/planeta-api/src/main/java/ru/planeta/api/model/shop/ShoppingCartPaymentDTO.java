package ru.planeta.api.model.shop;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import ru.planeta.model.shop.enums.PaymentType;

import java.math.BigDecimal;

/**
 * Created with IntelliJ IDEA.
 * Date: 27.08.2014
 * Time: 21:17
 * To change this template use File | Settings | File Templates.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
//extend require to get two DTO in one @RequestBody
public class ShoppingCartPaymentDTO extends PaymentDTO {
    private PaymentType paymentType;
    private long promoCodeId;
    private String promoCode;
    private long discountAmount;
    private BigDecimal donateAmount;
    private BigDecimal deliveryPrice;
    private boolean planetaPurchase;

    private boolean needPhone;
    private String paymentPhone;

    public boolean isNeedPhone() {
        return needPhone;
    }

    public void setNeedPhone(boolean needPhone) {
        this.needPhone = needPhone;
    }

    public String getPaymentPhone() {
        return paymentPhone;
    }

    public void setPaymentPhone(String paymentPhone) {
        this.paymentPhone = paymentPhone;
    }

    public PaymentType getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(PaymentType paymentType) {
        this.paymentType = paymentType;
    }

    public long getPromoCodeId() {
        return promoCodeId;
    }

    public void setPromoCodeId(long promoCodeId) {
        this.promoCodeId = promoCodeId;
    }

    public String getPromoCode() {
        return promoCode;
    }

    public void setPromoCode(String promoCode) {
        this.promoCode = promoCode;
    }

    public long getDiscountAmount() {
        return discountAmount;
    }

    public void setDiscountAmount(long discountAmount) {
        this.discountAmount = discountAmount;
    }

    public BigDecimal getDeliveryPrice() {
        return deliveryPrice;
    }

    public void setDeliveryPrice(BigDecimal deliveryPrice) {
        this.deliveryPrice = deliveryPrice;
    }

    public boolean isPlanetaPurchase() {
        return planetaPurchase;
    }

    public void setPlanetaPurchase(boolean planetaPurchase) {
        this.planetaPurchase = planetaPurchase;
    }

    public BigDecimal getDonateAmount() {
        return donateAmount;
    }

    public void setDonateAmount(BigDecimal donateAmount) {
        this.donateAmount = donateAmount;
    }
}

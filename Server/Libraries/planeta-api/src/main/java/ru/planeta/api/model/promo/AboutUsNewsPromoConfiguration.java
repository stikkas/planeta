package ru.planeta.api.model.promo;

public class AboutUsNewsPromoConfiguration extends BasePromoConfiguration {

    private String date;
    private String name;
    private String promoNewsUrl;
    private String anchor;

    public AboutUsNewsPromoConfiguration() {
    }

    public AboutUsNewsPromoConfiguration(String date, String name, String promoNewsUrl, String anchor) {
        this.date = date;
        this.name = name;
        this.promoNewsUrl = promoNewsUrl;
        this.anchor = anchor;
    }

    public String getAnchor() {
        return anchor;
    }

    public void setAnchor(String anchor) {
        this.anchor = anchor;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPromoNewsUrl() {
        return promoNewsUrl;
    }

    public void setPromoNewsUrl(String promoNewsUrl) {
        this.promoNewsUrl = promoNewsUrl;
    }

}

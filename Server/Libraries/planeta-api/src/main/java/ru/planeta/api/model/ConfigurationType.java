package ru.planeta.api.model;

/**
 * This class contains constants for configuration types
 * User: m.shulepov
 */
public interface ConfigurationType {

    String CALLBACK_RECIEVER_PHONE_KEY = "callback.phone.number";

    String SHOP_TOP_PRODUCTS_LIST = "shop.top.products.list";
    String PLANETA_PROMO_CAMPAIGN_CONFIGURATION_KEY = "planeta.start.hot.campaign";
    String PLANETA_PROMO_CABOUT_US_CAMPAAIGN_CONFIGURATION_KEY = "planeta.about.us.campaign";
    String PLANETA_CHARITY_PROMO_CAMPAIGN_CONFIGURATION_KEY = "planeta.charity.start.campaigns";
    String PLANETA_CHARITY_TOP_CAMPAIGN_CONFIGURATION_KEY = "planeta.charity.top.campaigns";
    String PLANETA_CHARITY_RECENT_WEBINARS_CONFIGURATION_KEY = "planeta.charity.recent.webinars";
    String CAMPAIGN_SUCCESS_THRESHOLD_CONFIGURATION = "planeta.campaign.success.threshold";

    String PLANETA_SHOP_FUNDS = "planeta.shop.funds";

    String PLANETA_RELATED_CAMPAIGN_TAGS_CONFIGURATION_KEY = "planeta.related.campaign.tags.configuration.list";
    String PLANETA_CAMPAIGN_LOW_BANNER_CONFIGURATION_KEY = "planeta.campaign.low.banner.html";
    String PLANETA_WELCOME_PARTNERS_CONFIGURATION_KEY = "planeta.welcome.partners.html";
    String PLANETA_CAMPAIGN_FUNDING_RULES_CONFIGURATION_KEY = "planeta.campaign.funding.rules.html";

    //
    String PLANETA_SHOP_PROMO_CONFIGURATION_LIST = "planeta.shop.promo.configuration.list";
    String PLANETA_BROADCAST_PROMO_CONFIGURATION_LIST_NEW = "planeta.broadcast.promo.configuration.list.new";
    String PLANETA_PARTNERS_PROMO_CONFIGURATION_LIST = "planeta.partners.promo.configuration.list";
    String PLANETA_ABOUTS_US_NEWS_CONFIGURATION_LIST = "planeta.about.us.news.promo.configuration.list";
    //

    String VIP_CLUB_JOIN_CONDITION = "vip.join.condition";
    String VIP_CLUB_MEMBER_NOTIFY = "vip.need.notify.member";

    String USER_CALLBACK_AVAILABILITY_CONDITION = "user.callback.availability.condition";

    String ADFOX_BANNER_CONFIG = "adfox.banner.config";

    String SHORTLINK_EXTRA_SOCIAL_LINKS = "shortlink.extra.social.links";

    String NEWS_NOTIFICATION_DAYS_LIMIT = "news.notification.days.limit";

    String PAYMENT_ERRORS_LARGE_SUM = "payment.errors.large.sum";

    String BIBLIO_PARTNERS_CONFIGURATION_LIST = "biblio.partners.configuration.list";
    String BIBLIO_ADVERTISE_CONFIGURATION_LIST = "biblio.advertise.configuration.list";

    String CHARITY_PARTNERS_CONFIGURATION_LIST = "charity.partners.configuration.list";
    String SCHOOL_STARTUP_SEMINAR = "school.startup.seminar";

    String TECHBATTLE_EXPERTS_LIST = "techbattle.experts.list";
    String TECHBATTLE_VIDEOS_LIST = "techbattle.videos.list";
    String TECHBATTLE_WINNERS_VIDEOS_LIST = "techbattle.winners.videos.list";
    String TECHBATTLE_PARTNERS_LIST = "techbattle.partners.list";
    String TECHBATTLE_NEWS_SOURCE_PROFILE_ID = "techbattle.new.source.profile.id";
    String TECHBATTLE_NEWS_SLIDER_IDS = "techbattle.new.slider.ids";

    String COMMERICAL_CAMPAIGNS = "commercial.campaigns";
    String COMMERCIAL_SHARES = "commercial.shares";

}

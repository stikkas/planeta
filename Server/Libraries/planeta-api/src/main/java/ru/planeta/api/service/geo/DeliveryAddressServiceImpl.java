package ru.planeta.api.service.geo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.planeta.api.model.shop.ShoppingCartContactsDTO;
import ru.planeta.dao.commondb.DeliveryAddressDAO;
import ru.planeta.model.common.Address;
import ru.planeta.model.common.DeliveryAddress;
import ru.planeta.model.profile.location.Country;

/**
 * Date: 31.10.2014
 * Time: 14:28
 */
@Service
public class DeliveryAddressServiceImpl implements DeliveryAddressService {

    @Autowired
    private DeliveryAddressDAO deliveryAddressDAO;

    @Autowired
    private GeoService geoService;

    @Override
    public DeliveryAddress saveDeliveryAddress(DeliveryAddress deliveryAddress) {
        return deliveryAddressDAO.insertOrUpdate(deliveryAddress);
    }

    @Override
    public DeliveryAddress getDeliveryAddress(long orderId) {
        return deliveryAddressDAO.selectDelivery(orderId);
    }

    @Override
    public void updateOderId(long deliveryAdderessId, long orderId) {
        deliveryAddressDAO.updateOderId(deliveryAdderessId, orderId);
    }

    @Override
    public DeliveryAddress createFromShopOrderInfoDTO(ShoppingCartContactsDTO dto) {
        Address customerContacts = dto.getCustomerContacts();
        DeliveryAddress deliveryAddress = new DeliveryAddress();
        deliveryAddress.setAddress(customerContacts.getStreet());
        deliveryAddress.setCity(customerContacts.getCity());
        Country c = geoService.getCountryById(customerContacts.getCountryId());
        if (c == null) {
            deliveryAddress.setCountry("");
        } else {
            deliveryAddress.setCountry(c.getCountryNameRus());
        }
        deliveryAddress.setZipCode(customerContacts.getZipCode());
        deliveryAddress.setFio(dto.getCustomerName());
        deliveryAddress.setPhone(dto.getPhoneNumber());
        return deliveryAddress;
    }

    @Override
    public DeliveryAddress getDeliveryAddress(Address address, String customerName) {

        if (address.getCountry() == null) {
            Country country = geoService.getCountryById(address.getCountryId());
            if (country != null) {
                address.setCountry(country.getName());
            } else {
                address.setCountry("");
            }
        }

        DeliveryAddress deliveryAddress = new DeliveryAddress();
        deliveryAddress.setZipCode(address.getZipCode());
        deliveryAddress.setPhone(address.getPhone());
        deliveryAddress.setFio(customerName);
        deliveryAddress.setCountry(address.getCountry());
        deliveryAddress.setAddress(address.getStreet());
        deliveryAddress.setCity(address.getCity());

        return deliveryAddress;
    }
}

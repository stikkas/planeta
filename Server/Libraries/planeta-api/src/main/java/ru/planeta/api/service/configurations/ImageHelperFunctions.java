package ru.planeta.api.service.configurations;

import org.springframework.web.context.ContextLoader;
import org.springframework.web.context.WebApplicationContext;
import ru.planeta.api.model.enums.image.ImageType;
import ru.planeta.commons.model.Gender;
import ru.planeta.model.enums.ThumbnailType;
import ru.planeta.model.profile.Profile;
import ru.planeta.utils.ThumbConfiguration;

/**
 * Created with IntelliJ IDEA.
 * User: user
 * Date: 02.10.13
 * Time: 16:16
 */
public class ImageHelperFunctions {

    public void setStaticNodesService(StaticNodesService staticNodesService) {
        this.staticNodesService = staticNodesService;
    }

    public ImageHelperFunctions(StaticNodesService staticNodesService) {
        this.staticNodesService = staticNodesService;
    }

    public ImageHelperFunctions() {
    }

    private  StaticNodesService staticNodesService;

    /**
     * Returns thumb url for initial image url
     *
     */
    public static String getThumbnailUrl(String imageUrl, ThumbnailType thumbnailType, ImageType imageType) {
        StaticNodesService staticNodesService = getWebAppContext().getBean(StaticNodesService.class);
        return getThumbnailUrlStatic(imageUrl, thumbnailType, imageType, staticNodesService);
    }

    public static String getThumbnailUrlStatic(String imageUrl, ThumbnailType thumbnailType, ImageType imageType, StaticNodesService staticNodesService) {
        return staticNodesService.getThumbnailUrl(imageUrl, thumbnailType, imageType);
    }


    private static WebApplicationContext getWebAppContext() {
        return ContextLoader.getCurrentWebApplicationContext();
    }

    /**
     * Returns thumb for user avatar url
     *
     */
    public static String getUserAvatarUrl(String imageUrl, ThumbnailType thumbnailType, Gender gender) {
        StaticNodesService staticNodesService = getWebAppContext().getBean(StaticNodesService.class);

        return getUserAvatarUrl(imageUrl, thumbnailType, gender, staticNodesService);
    }

    public static String getUserAvatarUrlByProfileAndThumbnailType(Profile profile, ThumbnailType thumbnailType) {
        return getUserAvatarUrl(profile.getImageUrl(), thumbnailType, profile.getUserGender());
    }

    public static String getUserAvatarUrlSmall(Profile profile) {
        return  getUserAvatarUrl(profile.getImageUrl(), ThumbnailType.USER_SMALL_AVATAR, profile.getUserGender());
    }

    public String getUserAvatarUrlSmallNonStatic(Profile profile) {
        return  getUserAvatarUrlNonStatic(profile.getImageUrl(), ThumbnailType.USER_SMALL_AVATAR, profile.getUserGender());
    }

    public String getUserAvatarUrlNonStatic(String imageUrl, ThumbnailType thumbnailType, Gender gender) {
        return getUserAvatarUrl(imageUrl, thumbnailType, gender, staticNodesService);
    }

    private static String getUserAvatarUrl(String imageUrl, ThumbnailType thumbnailType, Gender gender, StaticNodesService staticNodesService) {
        ImageType imageType = gender == Gender.FEMALE ? ImageType.USER_FEMALE : ImageType.USER;
        return staticNodesService.getThumbnailUrl(imageUrl, thumbnailType, imageType);
    }

    public String getPhotoPreviewNonStatic(String imageUrl) {
        return getThumbnailUrlStatic(imageUrl, ThumbnailType.ALBUM_COVER, ImageType.PHOTO, staticNodesService);
    }

    public static String getProxyThumbnailUrl(String imageUrl, ThumbnailType thumbnailType, ImageType imageType, int width, int height, boolean crop, boolean pad) {
        StaticNodesService staticNodesService = getWebAppContext().getBean(StaticNodesService.class);
        return getProxyThumbnailUrlStatic(staticNodesService, imageUrl, thumbnailType, imageType, width, height, crop, pad);
    }

    public static String getProxyThumbnailUrlStatic(StaticNodesService staticNodesService, String imageUrl, ThumbnailType thumbnailType, ImageType imageType, int width, int height, boolean crop, boolean pad) {
        String url = staticNodesService.getThumbnailUrl(imageUrl, thumbnailType, imageType);
        ThumbConfiguration configuration = new ThumbConfiguration();
        configuration.setHeight(height);
        configuration.setWidth(width);
        configuration.setCrop(crop);
        configuration.setPad(pad);
        return staticNodesService.wrapImageUrl(url, configuration, true);
    }
}

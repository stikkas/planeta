package ru.planeta.api.service.billing.payment.system.yamoney.mws.models;

import ru.planeta.api.service.billing.payment.system.yamoney.mws.models.enums.CurrencyCode;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by eshevchenko on 25.04.14.
 */
@XmlRootElement
public class ReturnPaymentRequest extends BaseRequest {
    @XmlAttribute
    public Date requestDT;
    @XmlAttribute
    public long clientOrderId;
    @XmlAttribute
    public long invoiceId;
    @XmlAttribute
    public long shopId;
    @XmlAttribute
    public BigDecimal amount;
    @XmlAttribute
    public CurrencyCode currency;
    @XmlAttribute
    public String cause;

    public static ReturnPaymentRequest create(long clientOrderId, long invoiceId, long shopId, BigDecimal amount, CurrencyCode currency, String сause) {
        ReturnPaymentRequest result = new ReturnPaymentRequest();
        result.requestDT = new Date();
        result.clientOrderId = clientOrderId;
        result.shopId = shopId;
        result.invoiceId = invoiceId;
        result.amount = amount;
        result.currency = currency;
        result.cause = сause;

        return result;
    }
}

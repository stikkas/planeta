package ru.planeta.api.service.security;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import ru.planeta.api.exceptions.MessageCode;
import ru.planeta.api.exceptions.NotFoundException;
import ru.planeta.api.exceptions.PermissionException;
import ru.planeta.api.model.UserAuthorizationInfo;
import ru.planeta.api.model.authentication.AuthenticatedUserDetails;
import ru.planeta.api.service.configurations.DefaultBlockSettings;
import ru.planeta.api.service.profile.ProfileSubscriptionService;
import ru.planeta.dao.commondb.UserPrivateInfoDAO;
import ru.planeta.dao.profiledb.*;
import ru.planeta.model.common.UserPrivateInfo;
import ru.planeta.model.enums.*;
import ru.planeta.model.profile.*;
import ru.planeta.model.profile.media.Video;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.EnumSet;

@Service
public class PermissionServiceImpl implements PermissionService {

    private static final Logger log = Logger.getLogger(PermissionServiceImpl.class);

    private final ProfileDAO profileDAO;
    private final ProfileBlockSettingDAO profileBlockSettingDAO;
    private final UserPrivateInfoDAO userPrivateInfoDAO;
    private final DefaultBlockSettings defaultBlockSettings;
    @Autowired
    private ProfileSubscriptionService profileSubscriptionService;

    @Autowired
    public PermissionServiceImpl(ProfileDAO profileDAO, ProfileBlockSettingDAO profileBlockSettingDAO, UserPrivateInfoDAO userPrivateInfoDAO, DefaultBlockSettings defaultBlockSettings) {
        this.profileDAO = profileDAO;
        this.profileBlockSettingDAO = profileBlockSettingDAO;
        this.userPrivateInfoDAO = userPrivateInfoDAO;
        this.defaultBlockSettings = defaultBlockSettings;
    }

    @Override
    public boolean isAdmin(long clientId, long profileId) {
        if (clientId < 0) {
            return false;
        }
        return clientId == profileId || hasAdministrativeRole(clientId) || isProfileAdmin(clientId, profileId);
    }

    @Override
    public void checkIsAdmin(long clientId, long profileId) throws PermissionException {
        if (!isAdmin(clientId, profileId)) {
            throw new PermissionException(MessageCode.PERMISSION_ADMIN);
        }
    }

    @Override
    public boolean isProfileAdmin(long profileId, long groupProfileId) {
        if (profileId < 0) {
            return false;
        }
        Profile profile = profileDAO.selectById(groupProfileId);
        if (profile == null) {
            return false;
        }
        if (profile.getProfileType() == ProfileType.HIDDEN_GROUP) {
            groupProfileId = profile.getCreatorProfileId();
        }
        return profileSubscriptionService.isAdmin(profileId, groupProfileId);
    }

    @Override
    public boolean checkViewPermission(long clientId, long profileId, BlockSettingType blockSettingType) {
        return checkPermission(clientId, profileId, blockSettingType, PermissionType.VIEW);
    }


    @Override
    public boolean checkWritePermission(long clientId, long profileId, BlockSettingType blockSettingType) {
        return checkPermission(clientId, profileId, blockSettingType, PermissionType.WRITE);
    }

    @Override
    public boolean checkAddPermission(long clientId, long profileId, BlockSettingType blockSettingType) {
        if (isAnonymous(profileId)) {
            log.warn("try check permission to anonymous profile");
            return false;
        }
        return checkPermission(clientId, profileId, blockSettingType, PermissionType.ADD);
    }

    @Override
    public boolean isObjectOwnerOrAuthor(long clientId, @Nonnull ProfileObject profileObject) {
        return isAdmin(clientId, profileObject.getProfileId()) || clientId == profileObject.getAuthorProfileId();
    }

    private static boolean permissionCompare(PermissionLevel first, PermissionLevel second) {
        return first.getCode() <= second.getCode();
    }

    private boolean checkPermissionLevel(long clientId, long profileId, PermissionLevel permissionLevel) {

        if (permissionCompare(PermissionLevel.NOBODY, permissionLevel)) {
            return false;
        }

        if (permissionCompare(permissionLevel, PermissionLevel.EVERYBODY)) {
            return true;
        }

        if (permissionCompare(permissionLevel, PermissionLevel.ADMINS)) {
            if (isAdmin(clientId, profileId)) {
                return true;
            }
        }

        if (permissionCompare(permissionLevel, PermissionLevel.MODERATORS)) {
            if (isGroupModerator(clientId, profileId)) {
                return true;
            }
        }

        return false;

    }

    @Override
    public boolean isGroupModerator(long profileId, long groupProfileId) {
        return isModerator(profileId) || hasAdministrativeRole(profileId);
    }

    @Override
    public boolean checkSendMessagePermission(long clientId, long profileId) {
        Profile profile = profileDAO.selectById(profileId);
        return hasAdministrativeRole(clientId) || profileSubscriptionService.doISubscribeYou(profileId, clientId) || profile != null && !profile.isLimitMessages();
    }

    @Override
    public void checkSendMessagePermission(long clientId) throws PermissionException {
        if (isAnonymous(clientId)) {
            throw new PermissionException(MessageCode.ANONYMOUS_CANT_CREATE_DIALOG);
        }
        Profile profile = profileDAO.selectById(clientId);
        if (profile == null) {
            throw new PermissionException(MessageCode.NOT_FOUND_EXCEPTION);
        }
        if (profile.getStatus() == ProfileStatus.NOT_SET) {
            throw new PermissionException(MessageCode.ACCOUNT_NOT_VERIFIED_MESSAGE_USER_DENIED);
        }
        if (profile.getStatus() == ProfileStatus.USER_SPAMMER) {
            throw new PermissionException(MessageCode.ACCOUNT_BANNED_MESSAGE_USER_DENIED);
        }
    }

    private boolean checkPermission(long clientId, long profileId, BlockSettingType blockSettingType, PermissionType type) {
        if (type != PermissionType.VIEW && isAnonymous(clientId)) {
            return false;
        }
        try {
            PermissionLevel level = getPermissionLevel(profileId, blockSettingType, type);
            return checkPermissionLevel(clientId, profileId, level);
        } catch (NotFoundException ex) {
            log.error("Error occured", ex);
            return false;
        }
    }


    @Override
    public boolean isModerator(long clientId) {
        return isUserInRole(clientId, UserStatus.MODERATOR);
    }

    @Override
    public EnumSet<ProfileRelationStatus> getProfileRelationStatus(long clientId, long profileId) {
        EnumSet<ProfileRelationStatus> statuses = EnumSet.noneOf(ProfileRelationStatus.class);
        if (clientId == profileId) {
            return EnumSet.of(ProfileRelationStatus.SELF);
        }
        if (hasAdministrativeRole(clientId) || profileSubscriptionService.isAdmin(clientId, profileId)) {
            statuses.add(ProfileRelationStatus.ADMIN);
        }

        if (statuses.isEmpty()) {
            statuses.add(ProfileRelationStatus.NOT_SET);
        }

        return statuses;
    }

    /**
     * This implementation first tries to load user info from Security context, and if there is no security context,
     * it tries to load user info from database.
     * After acquiring user info it checks if user is in specific role
     *
     * @param clientId user, whose role needs to be checked
     * @param status   to check against
     */
    @Override
    public boolean isUserInRole(long clientId, UserStatus status) {
        EnumSet<UserStatus> userStatus = getUserStatuses(clientId);
        return userStatus != null && userStatus.contains(status);
    }

    @Nullable
    private EnumSet<UserStatus> getUserStatuses(long clientId) {
        if (isAnonymous(clientId)) {
            return null;
        }
        UserAuthorizationInfo userAuthorizationInfo = getAuthorizationInfo();
        EnumSet<UserStatus> userStatus = null;
        if (userAuthorizationInfo != null
                && userAuthorizationInfo.getUserPrivateInfo() != null
                && clientId == userAuthorizationInfo.getUserPrivateInfo().getUserId()) {
            userStatus = userAuthorizationInfo.getUserPrivateInfo().getUserStatus();
        } else {
            UserPrivateInfo userPrivateInfo = userPrivateInfoDAO.selectByUserId(clientId);
            if (userPrivateInfo != null) {
                userStatus = userPrivateInfo.getUserStatus();
            }
        }
        return userStatus;
    }

    @Override
    public boolean hasAdministrativeRole(long clientId) {
        EnumSet<UserStatus> userStatuses = getUserStatuses(clientId);
        return (userStatuses != null) && (userStatuses.contains(UserStatus.ADMIN) || userStatuses.contains(UserStatus.SUPER_ADMIN) || userStatuses.contains(UserStatus.MANAGER));
    }

    @Override
    public void checkAdministrativeRole(long clientId) throws PermissionException {
        if (!hasAdministrativeRole(clientId)) {
            throw new PermissionException(MessageCode.USER_NOT_ADMIN);
        }
    }

    @Override
    public boolean isPlanetaAdmin(long clientId) {
        EnumSet<UserStatus> userStatuses = getUserStatuses(clientId);
        return (userStatuses != null) && (userStatuses.contains(UserStatus.ADMIN) || userStatuses.contains(UserStatus.SUPER_ADMIN));
    }

    @Override
    public boolean isSuperAdmin(long clientId) {
        return isUserInRole(clientId, UserStatus.SUPER_ADMIN);
    }

    @Override
    public boolean hasNoComment(long clientId) {
        return isUserInRole(clientId, UserStatus.NO_COMMENT);
    }

    private UserAuthorizationInfo getAuthorizationInfo() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication == null) {
            return null;
        }
        Object principal = authentication.getPrincipal();
        return principal instanceof AuthenticatedUserDetails ? ((AuthenticatedUserDetails) principal).getUserAuthorizationInfo() : null;
    }

    private static PermissionLevel getPermissionLevel(@Nullable ProfileBlockSetting profileBlockSetting, PermissionType type) {
        if (profileBlockSetting == null) {
            return PermissionType.VIEW == type ? PermissionLevel.EVERYBODY : PermissionLevel.ADMINS;
        }
        final PermissionLevel level;
        switch (type) {
            case VIEW:
                level = profileBlockSetting.getViewPermission();
                break;
            case WRITE:
                level = profileBlockSetting.getWritePermission();
                break;
            case VOTE:
                level = profileBlockSetting.getVotePermission();
                break;
            case COMMENT:
                level = profileBlockSetting.getCommentPermission();
                break;
            case ADD:
                level = profileBlockSetting.getAddPermission();
                break;
            default:
                level = null;
        }
        return level;
    }


    @Override
    public boolean checkVideoViewPermission(long clientId, long profileId, Video video) {
        if (video == null) {
            log.error("Null video");
            return false; //that should never happen
        }
        return checkPermissionLevel(clientId, profileId, video.getViewPermission());
    }

    private PermissionLevel getPermissionLevel(long profileId, BlockSettingType blockSettingType, PermissionType type) throws NotFoundException {
        ProfileBlockSetting profileBlockSetting = getProfileBlockSetting(profileId, blockSettingType);
        return getPermissionLevel(profileBlockSetting, type);
    }

    @Nullable
    private ProfileBlockSetting getProfileBlockSetting(long profileId, BlockSettingType blockSettingType) throws NotFoundException {
        ProfileBlockSetting profileBlockSetting = profileBlockSettingDAO.select(profileId, blockSettingType);
        if (profileBlockSetting == null) {
            Profile profile = profileDAO.selectById(profileId);
            if (profile == null) {
                throw new NotFoundException(Profile.class, profileId);
            }
            profileBlockSetting = defaultBlockSettings.getDefaultPermissionLevels(profile.getProfileType()).get(blockSettingType);
        }
        return profileBlockSetting;
    }

    @Override
    public boolean isAnonymous(long myProfileId) {
        return myProfileId == PermissionService.ANONYMOUS_USER_PROFILE_ID;
    }

    @Override
    public void toggleAdminBySubscription(long clientId, long profileId, long subjectProfileId, boolean isAdmin) throws PermissionException {
        // check client permissions: it can be global admin or "admin by subscription"
        if (hasAdministrativeRole(clientId) || profileSubscriptionService.isAdmin(clientId, subjectProfileId)) {
            profileSubscriptionService.toggleAdmin(profileId, subjectProfileId, isAdmin);
        } else {
            throw new PermissionException();
        }
    }
}

package ru.planeta.api.text;

import javax.annotation.Nullable;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: a.volkov
 * Date: 13.02.13
 * Time: 16:07
 */
public interface MessageParserService {

    /**
     * Extracts list of attachments from the specified message text.
     * Ignores links to hosts from "ignoreHosts" parameter.
     *
     * @param messageText
     * @param ignoreHosts
     * @return
     */
    Message extractAttachments(String messageText, String... ignoreHosts);

    /**
     * Extracts attachment from the specified url
     *
     * @param url the specified url
     * @return
     */
    @Nullable
    Attachment extractAttachment(String url);

    /**
     * Divides message into list of message parts (links and text parts).
     *
     * @param text
     * @return
     */
    List<MessagePart> extractMessageParts(String text);

}

package ru.planeta.api.service.concert;

import ru.planeta.model.concert.Section;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: michail
 * Date: 18.10.16
 * Time: 12:11
 */
public interface SectionService {
    Section getSectionByExtId(long externalSectionId);

    int insertOrUpdateSection(Section section);

    /**
     * Select all sections for concert both sectors and rows
     */
    List<Section> getSectionsByExternalConcertId(long extConcertId);

    /**
     * Select only sectors, not rows for concert
     */
    List<Section> getSectorsByExternalConcertId(long extConcertId);

    /**
     * Select only rows for concert
     */
    List<Section> getRowsByExternalConcertId(long extConcertId, Long extSectorId);

    int markSectionDeleted(Section section);
}

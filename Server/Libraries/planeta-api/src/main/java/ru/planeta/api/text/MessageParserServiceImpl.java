package ru.planeta.api.text;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.planeta.api.Utils;

import javax.annotation.Nullable;
import javax.annotation.PreDestroy;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;
import java.util.regex.Pattern;

import static ru.planeta.api.Utils.empty;

/**
 * Parser for the text messages. Extracts all links from the text,
 * checks if it is an image or video link and adds it to the specified attachments collection.
 */
@Service
public final class MessageParserServiceImpl implements MessageParserService {

    private static final Logger log = Logger.getLogger(MessageParserServiceImpl.class);
    private static final Pattern URL_PATTERN = Pattern.compile("(((([A-Za-z]{3,9}\\:(?:\\/\\/)?)(?:[-;:&=\\+\\$,\\w]+@)?[A-Za-z0-9.-]+)((?:\\/[-\\+~%()\\/.\\u0430-\\u044f\\u0410-\\u042f\\w_]*)?(:?(:?\\?|!)(?:[-\\+=&;%@.\\w_]+))?#?(?:[\\w]*))?))", Pattern.CASE_INSENSITIVE);

    @Autowired
    private List<AttachmentExtractor> attachmentExtractors;

    private ExecutorService executorService = Executors.newCachedThreadPool();


    @PreDestroy
    public void shutdown() {
        this.executorService.shutdown();
    }


    @Override
    public Message extractAttachments(String messageText, String... ignoreHosts) {
        Message message = new Message();
        message.setParts(extractMessageParts(messageText));


        List<MessagePart> urls = new ArrayList<>();
        for (MessagePart part : message.getParts()) {
            if (part.getMessagePartType() == MessagePartType.LINK && !Utils.empty(part.getText()) && !isUrlIgnored(part.getText(), ignoreHosts)) {
                urls.add(part);
            }
        }

        List<Future<Attachment>> awaitedAttachments = new ArrayList<>();
        for (MessagePart messagePart : urls) {

            final String url = messagePart.getText();

            Future<Attachment> future = executorService.submit(new Callable<Attachment>() {
                @Override
                public Attachment call() throws Exception {
                    return extractAttachment(url);
                }
            });
            awaitedAttachments.add(future);
        }
        try {
            for (Future<Attachment> future : awaitedAttachments) {
                Attachment attachment = future.get();
                if (attachment != null) {
                    if (attachment instanceof VideoAttachment) {
                        message.getVideoAttachments().add((VideoAttachment) attachment);
                    } else if (attachment instanceof ImageAttachment) {
                        message.getImageAttachments().add((ImageAttachment) attachment);
                    }
                }
            }
        } catch (InterruptedException e) {
            log.error("Error while waiting for attachments", e);
        } catch (ExecutionException e) {
            log.error("Error while extracting attachment", e);
        }

        return message;
    }

    /**
     * Extracts attachment from the specified url
     *
     * @param url the specified url
     */
    @Nullable
    public Attachment extractAttachment(String url) {

        for (AttachmentExtractor extractor : attachmentExtractors) {
            Attachment attachment = extractor.extract(url);
            if (attachment != null) {
                return attachment;
            }
        }

        return null;
    }

    /**
     * Divides message into list of message parts (links and text parts).
     *
     */
    @Override
    public List<MessagePart> extractMessageParts(String text) {
        List<MessagePart> messageParts = new ArrayList<>();

        if (StringUtils.isEmpty(text)) {
            return messageParts;
        }

        List<String> splitted = ru.planeta.commons.lang.StringUtils.splitPreserveAllTokens(text, URL_PATTERN);

        for (String part : splitted) {
            if (!StringUtils.isEmpty(part) && URL_PATTERN.matcher(part).find()) {
                messageParts.add(new MessagePart(part, MessagePartType.LINK));
            } else {
                messageParts.add(new MessagePart(part, MessagePartType.TEXT));
            }
        }

        return messageParts;
    }

    /**
     * Checks if the specified url targets host from "ignoredHosts" array
     *
     */

    private static boolean isUrlIgnored(String url, String[] ignoredHosts) {
        if (Utils.empty(ignoredHosts)) {
            return false;
        }

        for (String host : ignoredHosts) {
            if (StringUtils.containsIgnoreCase(url, "://" + host)) {
                return true;
            }
        }

        return false;
    }
}

package ru.planeta.api.service.authentication;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import ru.planeta.api.exceptions.NotFoundException;
import ru.planeta.api.model.UserAuthorizationInfo;
import ru.planeta.api.model.authentication.AuthenticatedUserDetails;
import ru.planeta.api.service.profile.AuthorizationService;

/**
 * User: atropnikov
 * Date: 21.03.12
 * Time: 17:19
 */
public class AuthenticatedUserDetailsService implements UserDetailsService {

    @Autowired
    private AuthorizationService authorizationService;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException, DataAccessException {
        final UserAuthorizationInfo userAuthorizationInfo;
        try {
            userAuthorizationInfo = authorizationService.getUserInfoByUsername(username);
        } catch (NotFoundException e) {
            throw new DataIntegrityViolationException(username, e);
        }

        if (userAuthorizationInfo == null) {
            throw new UsernameNotFoundException(username);
        }

        return new AuthenticatedUserDetails(userAuthorizationInfo);
    }
}

package ru.planeta.api.news;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.planeta.api.BaseService;
import ru.planeta.api.aspect.transaction.NonTransactional;
import ru.planeta.api.aspect.transaction.Transactional;
import ru.planeta.api.exceptions.NotFoundException;
import ru.planeta.api.exceptions.PermissionException;
import ru.planeta.api.text.FormattingService;
import ru.planeta.dao.profiledb.ProfileNewsDAO;
import ru.planeta.dao.profiledb.CommentDAO;
import ru.planeta.dao.profiledb.PostDAO;
import ru.planeta.model.common.campaign.Campaign;
import ru.planeta.model.enums.ObjectType;
import ru.planeta.model.news.ProfileNews;
import ru.planeta.model.profile.Post;
import ru.planeta.model.profile.Profile;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: a.volkov
 * Date: 13.11.2014
 * Time: 19:59
 */

@Service
public class ProfileNewsService extends BaseService {

    @Autowired
    private PostDAO postDAO;
    @Autowired
    private ProfileNewsDAO profileNewsDAO;
    @Autowired
    private CommentDAO commentDAO;
    @Autowired
    private FormattingService formattingService;

    @Autowired
    private LoggerService loggerService;

    private Logger logger = Logger.getLogger(ProfileNewsService.class);

    @Transactional
    public ProfileNewsDTO addProfileNewsPost(long clientId, Post post) throws PermissionException {
        verifyAdminPermission(clientId, post.getProfileId());

        formatPostText(post);

        postDAO.insert(post);

        ProfileNews profileNews = loggerService.addProfileNews(ProfileNews.Type.POST, post.getProfileId(), post.getId(), post.getCampaignId());

        if (post.getCampaignId() > 0) {
            updateTimePublished(post);
        }

        return assembleDto(profileNews);
    }

    private void updateTimePublished(Post post) {
        Campaign campaign = getCampaignDAO().selectForUpdate(post.getCampaignId());
        if (campaign != null) {
            campaign.setTimeLastNewsPublished(new Date());
            getCampaignDAO().update(campaign);
        } else {
            logger.error("No campaign " + post.getCampaignId());
        }
    }

    private void formatPostText(Post post) {
        String postText = formattingService.cleanHtml(post.getPostText());
        String annotation = formattingService.createAnnotation(postText);
        post.setPostText(postText);
        post.setHeadingText(annotation);
    }

    private void verifyAdminPermission(long clientId, long profileId) throws PermissionException {
        getPermissionService().checkIsAdmin(clientId, profileId);
    }

    @Transactional
    public void markDeleted(long clientId, long newsId) throws PermissionException, NotFoundException {
        ProfileNews news = profileNewsDAO.selectById(newsId);
        if (news == null) {
            throw new NotFoundException(ProfileNews.class, newsId);
        }
        verifyAdminPermission(clientId, news.getProfileId());

        if (!news.isDeleted()) {
            profileNewsDAO.markDeleted(newsId);
        }
    }

    @Transactional
    public ProfileNewsDTO changeProfileNewsPost(long clientId, Post post) throws PermissionException {
        verifyAdminPermission(clientId, post.getProfileId());

        formatPostText(post);
        postDAO.update(post);

        ProfileNews profileNews = profileNewsDAO.select(post.getProfileId(), ProfileNews.Type.POST, post.getId());
        return assembleDto(profileNews);
    }

    @NonTransactional
    public List<ProfileNewsDTO> getProfileNews(long clientId, long profileId, boolean onlyMyNews, int offset, int limit) {
        List<ProfileNews> news;
        if (clientId == profileId) {
            news = profileNewsDAO.selectForProfile(profileId, onlyMyNews, offset, limit);
        } else {
            news = profileNewsDAO.selectForGuest(profileId, offset, limit);
        }
        return assembleDtos(news);
    }

    @Transactional
    public List<ProfileNewsDTO> getCampaignNews(long campaignId, int limit, int offset) {
        List<ProfileNews> campaignNews = profileNewsDAO.selectCampaignNews(campaignId, offset, limit);
        return assembleDtos(campaignNews);
    }

    private ProfileNewsDTO assembleDto(@Nonnull Post post) throws NotFoundException {
        ProfileNews news = profileNewsDAO.select(post.getProfileId(), ProfileNews.Type.POST, post.getId());
        if (news == null) {
            throw new NotFoundException(ProfileNews.class, "objectId", String.valueOf(post.getId()));
        }
        return getProfileNewsDTO(post, news);
    }

    @Nonnull
    private ProfileNewsDTO getProfileNewsDTO(@Nonnull Post post, @Nonnull ProfileNews news) {
        Profile author = getProfileDAO().selectById(post.getProfileId());
        Campaign campaign = getCampaign(post.getCampaignId());

        ProfileNewsDTO result = new ProfileNewsDTO(news, author, campaign, post);
        result.setCommentsCount(commentDAO.getCommentsCount(news.getProfileId(), post.getId(), ObjectType.POST, 0));

        return result;
    }

    private ProfileNewsDTO assembleDto(ProfileNews profileNews) {
        ProfileNewsDTO result;

        if (profileNews.getType() == ProfileNews.Type.POST) {
            Post post = postDAO.select(profileNews.getObjectId());
            result = getProfileNewsDTO(post, profileNews);
        } else {
            Profile author = getProfileDAO().selectById(profileNews.getProfileId());
            result = new ProfileNewsDTO(profileNews, author, getCampaign(profileNews.getObjectId()));
        }

        return result;
    }

    private List<ProfileNewsDTO> assembleDtos(List<ProfileNews> newsList) {
        List<ProfileNewsDTO> dtos = new ArrayList<>(newsList.size());
        for (ProfileNews news : newsList) {
            dtos.add(assembleDto(news));
        }

        return dtos;
    }

    @Nullable
    private Campaign getCampaign(long campaignId) {
        return campaignId > 0 ? getCampaignDAO().selectCampaignById(campaignId) : null;
    }

    public ProfileNewsDTO getProfileNewsPost(long postId) throws NotFoundException {
        Post post = postDAO.select(postId);
        if (post == null) {
            throw new NotFoundException(Post.class, postId);
        }
        return assembleDto(post);
    }

    public List<ProfileNewsDTO> getProfileNewsPost(List<Long> postIds) {
        List<ProfileNewsDTO> result = new ArrayList<>();
        for (Post post : postDAO.selectPosts(postIds)) {
            try {
                result.add(assembleDto(post));
            } catch (NotFoundException e) {
                logger.warn("Cannot assemble news post #" + post.getId(), e);
            }
        }

        return result;
    }

    public int selectCountForProfile(long profileId, Date dateFrom, boolean onlyMyNews) {
        return profileNewsDAO.selectCountForProfile(profileId, dateFrom, onlyMyNews);
    }
}

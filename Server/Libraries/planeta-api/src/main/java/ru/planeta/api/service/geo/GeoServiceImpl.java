package ru.planeta.api.service.geo;


import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.IterableUtils;
import org.apache.commons.collections4.Predicate;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import ru.planeta.commons.text.TranslitConverter;
import ru.planeta.dao.profiledb.GeoLocationDAO;
import ru.planeta.model.profile.location.*;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.*;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * User: eshevchenko
 * Date: 09.06.12
 * Time: 15:33
 */
@Service
@Lazy
public class GeoServiceImpl implements GeoService {

    @Autowired
    private GeoLocationDAO geoLocationDAO;

    private static final long CACHE_UPDATE_DELAY = 1000 * 60 * 60 * 24; // 1 day

    private List<IGeoLocation> topLocations = new ArrayList<>();
    private Map<Integer, Country> countries = new TreeMap<>();
    private Map<Integer, Country> countriesInCampaigns = new TreeMap<>();

    private final ReadWriteLock countriesLock = new ReentrantReadWriteLock();
    private final ReadWriteLock locationsLock = new ReentrantReadWriteLock();
    private final ReadWriteLock countriesInCampaignsLock = new ReentrantReadWriteLock();

    // @PostConstruct
    @Scheduled(fixedDelay = CACHE_UPDATE_DELAY, initialDelay = 0)
    public void updateCache() {
        final List<Country> countryList = geoLocationDAO.countries();
        Map<Integer, Country> tempCountries = new TreeMap<>();
        for (Country country: countryList) {
            tempCountries.put(country.getCountryId(), country);
        }
        countriesLock.writeLock().lock();
        try {
            this.countries.clear();
            this.countries.putAll(tempCountries);
        } finally {
            countriesLock.writeLock().unlock();
        }

        fillTopLocations();
    }

    @Scheduled(fixedDelay = CACHE_UPDATE_DELAY, initialDelay = 0)
    public void updateCacheCountriesInCampaigns() {
        final List<Country> countryList = geoLocationDAO.countriesByCampaigns();
        Map<Integer, Country> tempCountries = new TreeMap<>();
        for (Country country: countryList) {
            tempCountries.put(country.getCountryId(), country);
        }
        countriesInCampaignsLock.writeLock().lock();
        try {
            this.countriesInCampaigns.clear();
            this.countriesInCampaigns.putAll(tempCountries);
        } finally {
            countriesInCampaignsLock.writeLock().unlock();
        }
    }

    private void fillTopLocations() {
        locationsLock.writeLock().lock();
        try {
            topLocations.clear();
            topLocations.addAll(geoLocationDAO.globalRegions());
            topLocations.addAll(geoLocationDAO.getCountriesByGlobalRegion(IGeoLocation.Companion.getDEFAULT_LOCATION_ID()));
            topLocations.add(new World());
        } finally {
            locationsLock.writeLock().unlock();
        }
    }

    @Override
    public Collection<Country> getCountries() {
        countriesLock.readLock().lock();
        try {
            if (countries.isEmpty()) {
                updateCache();
            }
            return countries.values();
        } finally {
            countriesLock.readLock().unlock();
        }
    }

    @Override
    public Collection<Country> getCountriesInCampaigns() {
        countriesInCampaignsLock.readLock().lock();
        try {
            if (countriesInCampaigns.isEmpty()) {
                updateCacheCountriesInCampaigns();
            }
            return countriesInCampaigns.values();
        } finally {
            countriesInCampaignsLock.readLock().unlock();
        }
    }

    @Override
    public Collection<Country> getCountriesNew(boolean onlyInCampaigns) {
        List<Country> c;
        if(!onlyInCampaigns) {
            c = new ArrayList<>(getCountries());
        } else {
            c = new ArrayList<>(getCountriesInCampaigns());
        }

        CollectionUtils.filter(c, country -> country.getCountryId() != 220 && country.getCountryId() != 219);

        Collections.sort(c, (o1, o2) -> {
            if (o1.getCountryId() == 1) return -1;
            if (o2.getCountryId() == 1) return 1;

            return o1.getCountryNameRus().compareTo(o2.getCountryNameRus());
        });
        return c;
    }

    @Override
    @Nullable
    public Country getCountryById(final int countryId) {
        countriesLock.readLock().lock();
        try {
            if (countries.isEmpty()) {
                countriesLock.readLock().unlock();
                updateCache();
                countriesLock.readLock().lock();
            }
            return countries.get(countryId);
        } finally {
            countriesLock.readLock().unlock();
        }
    }

    @Override
    @Nullable
    public Country getCountryByName(final String name) {
        if (StringUtils.isEmpty(name)) {
            return null;
        }
        countriesLock.readLock().lock();
        try {
            return IterableUtils.find(getCountries(), new Predicate<Country>() {
                @Override
                public boolean evaluate(Country country) {
                    return country.getCountryNameRus().equalsIgnoreCase(name)
                            || country.getCountryNameEn().equalsIgnoreCase(name)
                            || country.getCountryNameRus().contains(name);
                }
            });
        } finally {
            countriesLock.readLock().unlock();
        }
    }

    @Override
    public City getCityById(int cityId) {
        return geoLocationDAO.selectCity(Long.valueOf(cityId));
    }

    @Override
    public void addNewCity(String nameRu, int regionId) {
        City city = new City();
        city.setNameRus(nameRu);
        city.setNameEn(TranslitConverter.toTranslit(nameRu));
        city.setRegionId(regionId);
        city.setCountryId(getLocation(regionId, LocationType.REGION).getParentLocationId());
        geoLocationDAO.insertNewCity(city);
    }

    @Override
    public List<City> getCountryCitiesBySubstring(long countryId, final String subName, int offset, int limit) {
        return geoLocationDAO.getCountryCitiesBySubstring(countryId, subName, offset, limit);
    }

    @Override
    public List<Region> getCountryRegionsBySubstring(int countryId, final String subName, String lang, int offset, int limit) {
        return geoLocationDAO.getCountryRegionsBySubstring(countryId, subName, lang, offset, limit);
    }

    @Override
    public List<City> getRegionCitiesBySubstring(long locationId, String substring, int offset, int limit) {
        return geoLocationDAO.getRegionCitiesBySubstring(locationId, substring, offset, limit);
    }

    @Override
    public List<City> getCitiesWithGeo(int offset, int limit) {
        return geoLocationDAO.getCitiesWithGeo(offset, limit);
    }

    @Override
    public IGeoLocation getLocation(IGeoLocation location) {
        return getLocation(location.getLocationId(), location.getLocationType());
    }

    @Override
    public IGeoLocation getLocation(int locationId, @Nonnull LocationType type) {
        switch (type) {
            case CITY:
                return getCityById(locationId);
            case REGION:
                return geoLocationDAO.selectRegion(Long.valueOf(locationId));
            case COUNTRY:
                return getCountryById(locationId);
            case GLOBAL_REGION:
                return getGlobalRegion(locationId);
            case WORLD:
                return new World();
            default:
                return null;
    }
    }

    @Override
    public Collection<IGeoLocation> getTopLocations() {
        locationsLock.readLock().lock();
        try {
            return topLocations;
        } finally {
            locationsLock.readLock().unlock();
        }
    }

    @Override
    public List<? extends IGeoLocation> getNestedLocations(final int locationId, LocationType type) {
        switch (type) {
            case REGION:
                return geoLocationDAO.selectCity(locationId);
            case COUNTRY:
                return geoLocationDAO.selectRegion(locationId);
            case GLOBAL_REGION:
                return geoLocationDAO.getCountriesByGlobalRegion(locationId);
            case WORLD:
                return new ArrayList<>(getTopLocations());
            case CITY:
            default:
                return Collections.emptyList();
        }
    }

    @Override
    public IGeoLocation getLocationWithParents(IGeoLocation location) {
        IGeoLocation typedLocation = location;
        if (UnspecifiedLocation.class.isAssignableFrom(location.getClass())) {
            typedLocation = getLocation(location);
        }
        IGeoLocation curLoc = typedLocation;
        while (curLoc != null && curLoc.getParentLocationId() != IGeoLocation.Companion.getDEFAULT_LOCATION_ID()) {
            IGeoLocation parent = getLocation(curLoc.getParentLocationId(), curLoc.getParentLocationType());
            curLoc.setParent(parent);
            curLoc = curLoc.getParent();
        }
        return typedLocation;
    }

    private GlobalRegion getGlobalRegion(int locationId) {
        return geoLocationDAO.getGlobalRegionById(locationId);
    }

}

package ru.planeta.api.service.billing;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import ru.planeta.api.exceptions.NotFoundException;
import ru.planeta.api.exceptions.PaymentException;
import ru.planeta.api.exceptions.PermissionException;
import ru.planeta.api.service.billing.payment.PaymentService;
import ru.planeta.api.service.billing.payment.settings.PaymentSettingsService;
import ru.planeta.api.service.profile.ProfileBalanceService;
import ru.planeta.api.service.security.PermissionService;
import ru.planeta.model.Constants;
import ru.planeta.model.common.PaymentMethod;
import ru.planeta.model.common.TopayTransaction;

import java.math.BigDecimal;

/**
 * Implements {@link GiftService} interface.<br>
 * Created by eshevchenko.
 */
@Service
public class GiftServiceImpl implements GiftService {

    private final PaymentService paymentService;
    private final PaymentSettingsService settingsService;
    private final PermissionService permissionService;
    private final ProfileBalanceService profileBalanceService;

    @Autowired
    public GiftServiceImpl(PaymentService paymentService, PaymentSettingsService settingsService, PermissionService permissionService, ProfileBalanceService profileBalanceService) {
        this.paymentService = paymentService;
        this.settingsService = settingsService;
        this.permissionService = permissionService;
        this.profileBalanceService = profileBalanceService;
    }

    @Override
    public void giftMoney(long clientId, long profileId, BigDecimal amount, String message) throws PaymentException, PermissionException, NotFoundException {
        permissionService.checkAdministrativeRole(clientId);
        String comment = message == null || message.isEmpty() ? "Money gift from Planeta." : message;
        giftMoney(profileId, amount, settingsService.getPromoPaymentMethod(), comment);
    }


    private void giftMoney(long profileId, BigDecimal amount, PaymentMethod paymentMethod, String comment) throws PaymentException {
        Assert.isTrue(amount.intValue() > 0, "Amount can not be less than zero.");
        BigDecimal planetaBalance = profileBalanceService.getBalance(Constants.INSTANCE.getPLANETA_PROFILE_ID());
        Assert.isTrue(planetaBalance.compareTo(amount) >= 0, "Amount can not be greater than Planeta balance.");
        TopayTransaction payment = paymentService.createInternalPayment(profileId, amount, paymentMethod);
        payment.setComment(comment);
        paymentService.processPayment(payment, null);
    }

}

package ru.planeta.api.service.registration;

import org.apache.commons.lang3.tuple.Pair;
import ru.planeta.api.exceptions.NotFoundException;
import ru.planeta.api.exceptions.PermissionException;
import ru.planeta.api.model.UserAuthorizationInfo;
import ru.planeta.commons.web.RegistrationData;
import ru.planeta.model.common.UserPrivateInfo;
import ru.planeta.model.common.auth.ExternalAuthentication;
import ru.planeta.model.stat.RequestStat;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

/**
 * Top-level service method to call from controller.
 */
public interface RegistrationService {

    /**
     * Register user by its email and password only
     *
     * @param email valid email
     * @param password raw password
     * @return private info, profile, user
     * @throws PermissionException if email exists
     * @throws IllegalArgumentException if email is invalid
     */
    @Nonnull
    UserAuthorizationInfo registerByEmail(String email, String password, RequestStat requestStat) throws PermissionException, IllegalArgumentException;

    /**
     * Register user by its email only with generated password
     *
     * @param email valid email
     * @throws PermissionException if email exists
     * @throws IllegalArgumentException if email is invalid
     */
    @Nonnull
    UserAuthorizationInfo autoRegisterByEmail(String email, String displayName, RequestStat requestStat) throws PermissionException, IllegalArgumentException;

    /**
     * Register user by its email only with generated password
     *
     * @param email email for auto registration
     * @throws PermissionException if email exists
     * @throws IllegalArgumentException if email is invalid
     */
    Pair<UserAuthorizationInfo, String> autoRegisterGetPassword(String email, String displayName, RequestStat requestStat) throws PermissionException, IllegalArgumentException;

    void setAsynchronous(boolean asynchronous);

    /**
     * Register user by data provided by social network
     *
     * @param email email provided by user
     * @param externalAuthentication external login
     * @param registrationData user info
     * @return private info, user and profile
     * @throws PermissionException if authentication is already used
     * @throws IllegalArgumentException if email is invalid
     */
    @Nonnull
    UserAuthorizationInfo registerFromSocialNetworkWithEmail(String email, ExternalAuthentication externalAuthentication, RegistrationData registrationData, RequestStat requestStat) throws PermissionException, IllegalArgumentException;

    /**
     * Add email authentication (username changes to email) to registered user, after
     *
     * @param email valid email
     * @param userAuthorizationInfo existing private info of registered user
     * @return modified private info
     * @throws NotFoundException if userPrivateInfo is null
     * @throws PermissionException if REGISTRATION_CURRENT_EMAIL_ALREADY_EXISTS
     * @throws IllegalArgumentException if email is invalid
     */
    UserAuthorizationInfo addEmailToExternalUser(String email, UserAuthorizationInfo userAuthorizationInfo) throws NotFoundException, PermissionException, IllegalArgumentException;

    // dangerous. not request password
    @Deprecated
    long autoRegisterIfNotExists(String email, String displayName, RequestStat requestStat) throws PermissionException;

    /**
     * Removes oddPrivateInfo (change username) and moves all credentials to masterPrivateInfo
     * @param masterUserPrivateInfo profile to merge in
     * @param oddPrivateInfo profile that will be merged into
     * @throws NotFoundException if no credentials connected with oddPrivateInfo
     */
    void mergeAccounts(UserPrivateInfo masterUserPrivateInfo, UserPrivateInfo oddPrivateInfo) throws NotFoundException;

    /**
     * Send registration complete email
     * @param profileId user to send
     * @throws NotFoundException if no such user or user have no email
     */
    void resendRegistrationCompleteEmail(long profileId) throws NotFoundException;

    /**
     * User's password recovery request
     */
    @Nullable
    UserPrivateInfo recoverPassword(String email, String url, String redirectUrl) throws Exception;

}

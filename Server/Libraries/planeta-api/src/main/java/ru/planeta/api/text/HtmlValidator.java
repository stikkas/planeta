package ru.planeta.api.text;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.ccil.cowan.tagsoup.Parser;
import org.xml.sax.InputSource;

import java.io.StringReader;
import java.util.regex.Pattern;

/**
 * Simple html validator.
 * Checks for restricted html tags and attributes.
 *
 * @author m.shulepov
 */
public final class HtmlValidator {

    private static final Pattern RESTRICTED_PATTERN = Pattern.compile("(<script|<iframe|<object|javascript:|vbscript:)", Pattern.CASE_INSENSITIVE);
    private static final Logger log = Logger.getLogger(HtmlValidator.class);

    /**
     * Helper method that validates html
     *
     * @param html html to be validated
     * @return true if html is valid, false otherwise
     */
    public static boolean validateHtml(String html) {
        if (StringUtils.isEmpty((html))) {
            return true;
        }
        try {
            if (RESTRICTED_PATTERN.matcher(html).find()) {
                return false;
            }

            Parser parser = new HtmlValidatingParser(false);
            parser.parse(new InputSource(new StringReader(html)));
            return true;
        } catch (Exception e) {
            log.warn(e.getMessage(), e);
        }
        return false;
    }

    /**
     * Helper method that cleans html
     *
     * @param html html to be cleaned
     * @return clean html
     */
    public static String cleanHtml(String html) {
        if (StringUtils.isEmpty(html)) {
            return StringUtils.EMPTY;
        }
        try {
            //if there are not any html tag in the string parser will ignore first end tag
            //for example "<p:photo>111</p:photo> 222 <p:photo>333</p:photo> 444" after CleanHtml will be "<p:photo>111 222 <p:photo>333</p:photo> 444</p:photo>"
            html = "<html>" + html;
            Parser parser = new HtmlValidatingParser(true);
            parser.parse(new InputSource(new StringReader(html)));
            return ((HtmlValidatingParser) parser).getHtml();
        } catch (Exception e) {
            log.warn(e.getMessage(), e);
        }
        return StringUtils.EMPTY;
    }

}

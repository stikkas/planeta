package ru.planeta.api.service.registration;

import ru.planeta.api.mail.MailClient;

public class AutoRegistrationConfirmationStrategy extends EmailConfirmationStrategy{
    public AutoRegistrationConfirmationStrategy(MailClient mailClient, String email) {
        super(mailClient, email);
    }

    @Override
    protected void sendEmail(String password, String regCode) {
        getMailClient().sendAutoRegistrationCompleteEmail(getEmail(), password, regCode);
    }
}

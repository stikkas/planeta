package ru.planeta.api.model.campaign;

import ru.planeta.model.common.campaign.Campaign;

import java.util.List;

/**
 * User: atropnikov
 * Date: 10.04.12
 * Time: 13:54
 */
public class CampaignEdit extends Campaign {
    public static enum Section {
        COMMON, DETAILS, SHARES, ALL
    }

    private Section section;
    private Object videoContent;

    @Override
    public Object getVideoContent() {
        return videoContent;
    }

    @Override
    public void setVideoContent(Object videoContent) {
        this.videoContent = videoContent;
    }

    public CampaignEdit() {
        super();
        this.section = Section.ALL;
    }

    public CampaignEdit(Campaign campaign) {
        super(campaign);
        this.section = Section.ALL;
    }

    public Section getSection() {
        return section;
    }

    public void setSection(Section section) {
        this.section = section;
    }
}

package ru.planeta.api.service.billing.payment.system.webmoney.rest.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * User: Serge Blagodatskih<stikkas17@gmail.com><br>
 * Date: 14.06.16<br>
 * Time: 17:50
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class PaymentStateResponse {

    private String state;

    @JsonProperty("State")
    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

}

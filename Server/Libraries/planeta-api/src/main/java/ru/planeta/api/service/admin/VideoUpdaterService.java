package ru.planeta.api.service.admin;


import org.json.JSONException;
import org.xml.sax.SAXException;

import ru.planeta.model.profile.media.Video;

import java.io.IOException;

/**
 *
 * Created by a.savanovich on 08.12.2015.
 */
public interface VideoUpdaterService {

    Video updateYoutubeAndCachedVideo(long videoId, long profileId) throws IOException, JSONException;

    void updateCampaignDescription(long campaignId, VideoUpdaterService videoUpdaterService) throws IOException, SAXException;

    void updateCampaign(long campaignId) throws IOException, SAXException;
}

package ru.planeta.api.model.campaign;

import ru.planeta.api.Utils;
import ru.planeta.model.common.campaign.Campaign;

import java.util.Date;

import static ru.planeta.api.Utils.nvl;

/**
 * Created with IntelliJ IDEA.
 * Date: 09.10.2015
 * Time: 20:06
 * To change this template use File | Settings | File Templates.
 */
public class PromoCampaignDTO {
    private String url;
    private String image;
    private String name;
    private int progressProc;
    private long collected;
    private long target;
    private Date timeFinish;
    private String shortDesc;

    private PromoCampaignDTO(String url, String image, String name, long collected, long target, Date timeFinish, String shortDesc) {
        this.url = url;
        this.image = image;
        this.name = name;
        this.collected = collected;
        this.target = target;
        this.timeFinish = timeFinish;
        this.progressProc = (int) ((double)collected / (double)target * 100);
        this.shortDesc = shortDesc;
    }

    public static PromoCampaignDTO createFromCampaign(Campaign campaign){
         return new PromoCampaignDTO(
                 Utils.nvl(campaign.getCampaignAlias(),String.valueOf(campaign.getCampaignId())),
                 campaign.getImageUrl(),
                 campaign.getName(),
                 campaign.getCollectedAmount().longValue(),
                 campaign.getTargetAmount().longValue(),
                 campaign.getTimeFinish(),
                 campaign.getShortDescription()
         );
    }

    public String getShortDesc() {
        return shortDesc;
    }

    public void setShortDesc(String shortDesc) {
        this.shortDesc = shortDesc;
    }

    public Date getTimeFinish() {
        return timeFinish;
    }

    public void setTimeFinish(Date timeFinish) {
        this.timeFinish = timeFinish;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getProgressProc() {
        return progressProc;
    }

    public void setProgressProc(int progressProc) {
        this.progressProc = progressProc;
    }

    public long getCollected() {
        return collected;
    }

    public void setCollected(long collected) {
        this.collected = collected;
    }

    public long getTarget() {
        return target;
    }

    public void setTarget(long target) {
        this.target = target;
    }
}

package ru.planeta.api.geo;

import ru.planeta.model.profile.location.City;
import ru.planeta.model.profile.location.Country;

import javax.annotation.Nonnull;

/**
 * Http Invoker Geo webservice
 *
 * User: d.kolyshev
 * Date: 20.02.2013
 */
public interface GeoResolverWebService {

	/**
	 * Returns city object resolved from ip with geo resolver
	 *
	 * @param ip
	 * @return
	 */
	City resolveCity(@Nonnull String ip);

	/**
	 * Returns country object resolved from ip with geo resolver
	 *
	 * @param ip
	 * @return
	 */
	Country resolveCountry(@Nonnull String ip);
}

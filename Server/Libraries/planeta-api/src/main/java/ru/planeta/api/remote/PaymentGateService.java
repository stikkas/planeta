package ru.planeta.api.remote;

import ru.planeta.api.exceptions.NotFoundException;
import ru.planeta.api.exceptions.OrderException;
import ru.planeta.api.exceptions.PaymentException;
import ru.planeta.api.exceptions.PermissionException;
import ru.planeta.model.common.PaymentTool;
import ru.planeta.model.enums.ProjectType;

/**
 * Created with IntelliJ IDEA.
 * User: a.volkov
 * Date: 13.05.14
 * Time: 11:42
 */
public interface PaymentGateService {

    boolean cancelPayment(long transactionId) throws NotFoundException;

    boolean validate(long transactionId) throws PaymentException, NotFoundException;

    void purchaseOrder(long orderId) throws PermissionException, NotFoundException, OrderException;
}

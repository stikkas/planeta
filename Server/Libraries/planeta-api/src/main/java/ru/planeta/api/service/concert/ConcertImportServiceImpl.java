package ru.planeta.api.service.concert;

/**
 * Created with IntelliJ IDEA.
 * User: michail
 * Date: 27.10.16
 * Time: 15:22
 */

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.planeta.api.exceptions.MoscowShowInteractionException;
import ru.planeta.api.exceptions.NotFoundException;
import ru.planeta.model.concert.*;
import ru.planeta.moscowshow.client.MoscowShowClient;
import ru.planeta.moscowshow.model.ActionStructure;
import ru.planeta.moscowshow.model.StructElement;
import ru.planeta.moscowshow.model.response.ResponseActionList;
import ru.planeta.moscowshow.model.response.ResponseActionSchema;

import javax.annotation.Nonnull;
import java.util.List;
import org.springframework.context.annotation.Lazy;

@Service
@Lazy
public class ConcertImportServiceImpl implements ConcertImportService {
    private static final Logger logger = Logger.getLogger(ConcertServiceImpl.class);

    @Autowired
    private ConcertService concertService;

    @Autowired
    private HallService hallService;

    @Autowired
    private SectionService sectionService;

    @Autowired
    private PlaceService placeService;

    @Autowired
    private MoscowShowClient moscowShowClient;


    private static <T1, T2> void doMerge(Mergeable<T1, T2> mergeableObject) {
        logger.info("doMerge started");
        int planetaStep = 0;
        int msStep = 0;

        List<T1> planetaList = mergeableObject.getInternalList();
        List<T2> msList = mergeableObject.getExternalList();

        for (; planetaStep < planetaList.size() && msStep < msList.size(); ) {
            logger.info("doMerge iteration. planeta step is " + planetaStep + ", ms step is " + msStep);

            T1 planetaObject = planetaList.get(planetaStep);
            T2 msObject = msList.get(msStep);
            switch (mergeableObject.compare(planetaObject, msObject)) {
                case 1:                                                     // INSERT: planeta id is greater than MS id
                    try {
                        mergeableObject.insert(msObject);
                    } catch (Exception ex) {
                        logger.error(ex);
                    }
                    msStep++;
                    break;
                case 0:                                                      // UPDATE: object are equals
                    try {
                        mergeableObject.update(planetaObject, msObject);
                    } catch (Exception ex) {
                        logger.error(ex);
                    }
                    planetaStep++;
                    msStep++;
                    break;
                case -1:                                                    // DELETE: planeta id is lower than MS id
                    try {
                        mergeableObject.delete(planetaObject);
                    } catch (Exception ex) {
                        logger.error(ex);
                    }
                    planetaStep++;
                    break;
                default:
                    logger.error("doMerge: Long.compare error wtf? " +
                            "planetaStep is " + planetaStep +
                            "msStep is " + msStep);
                    planetaStep++;  // prevent endless loop
                    msStep++;
            }
        }

        for (; planetaStep < planetaList.size(); planetaStep++) {
            logger.info("Import concerts. planeta step is " + planetaStep + ", ms step is " + msStep);

            T1 planetaObject = planetaList.get(planetaStep);
            try {
                mergeableObject.delete(planetaObject);
            } catch (Exception ex) {
                logger.error(ex);
            }
        }

        for (; msStep < msList.size(); msStep++) {
            logger.info("Import concerts. planeta step is " + planetaStep + ", ms step is " + msStep);
            try {
                mergeableObject.insert(msList.get(msStep));
            } catch (Exception ex) {
                logger.error(ex);
            }
        }
        logger.info("doMerge stopped");
    }


    @Override
    public void importMoscowShowConcerts() throws NotFoundException, MoscowShowInteractionException {
        ResponseActionList responseActionList = moscowShowClient.listActions();
        if (responseActionList == null) {
            throw new NotFoundException(ResponseActionList.class);
        }

        if (responseActionList.hasErrors()) {
            throw new MoscowShowInteractionException(responseActionList.getErrorMessage());
        }

        List<Concert> concertList = concertService.selectList(0, 0);
        List<ActionStructure> msConcertList = responseActionList.getResult().getActions();

        MergeableConcerts mergeableConcerts = new MergeableConcerts(concertList, msConcertList, concertService, hallService, this);
        doMerge(mergeableConcerts);

    }


    @Override
    public void importMoscowShowConcertScheme(@Nonnull Concert concert) throws NotFoundException, MoscowShowInteractionException {
        ResponseActionSchema responseActionSchema = moscowShowClient.actionSchema(concert.getExternalConcertId());
        if (responseActionSchema == null) {
            throw new NotFoundException(ResponseActionSchema.class, concert.getExternalConcertId());
        }

        if (responseActionSchema.hasErrors()) {
            throw new MoscowShowInteractionException(responseActionSchema.getErrorMessage());
        }

        // update concert hall scheme
        concert.setSchemeUrl(responseActionSchema.getResult().getImageUrl());
        try {
            concertService.insertOrUpdateConcert(concert);
        } catch (Exception ex) {
            logger.error(ex);
        }

        try {
            importSections(concert, responseActionSchema);
        } catch (Exception ex) {
            logger.error(ex);
        }

        try {
            importPlaces(concert, responseActionSchema);
        } catch (Exception ex) {
            logger.error(ex);
        }
    }

    private void importSections(@Nonnull Concert concert, @Nonnull ResponseActionSchema responseActionSchema) {
        List<Section> sectionList = sectionService.getSectionsByExternalConcertId(concert.getExternalConcertId());
        List<StructElement> moscowShowSectionList = responseActionSchema.getResult().getSectionsAndRows();

        MergeableSections mergeableSections = new MergeableSections(sectionList, moscowShowSectionList, sectionService, concert.getExternalConcertId());
        doMerge(mergeableSections);
    }

    private void importPlaces(@Nonnull Concert concert, @Nonnull ResponseActionSchema responseActionSchema) {
        List<Place> placeList = placeService.getPlacesByExternalConcertId(concert.getExternalConcertId());
        List<ru.planeta.moscowshow.model.Place> moscowShowPlaceList = responseActionSchema.getResult().getPlaces();

        MergeablePlaces mergeablePlaces = new MergeablePlaces(placeList, moscowShowPlaceList, placeService, concert.getExternalConcertId());
        doMerge(mergeablePlaces);
    }

}



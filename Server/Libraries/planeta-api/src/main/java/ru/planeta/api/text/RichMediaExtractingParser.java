package ru.planeta.api.text;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class RichMediaExtractingParser extends HtmlValidatingParser {

    private static final String ID = "id";
    private static final String OWNER = "owner";
    private final ArrayList<RichMediaObject> richMediaTags;

    public RichMediaExtractingParser(boolean cleanHtml) {
        super(cleanHtml);
        this.richMediaTags = new ArrayList<>();
    }

    public ArrayList<RichMediaObject> getRichMediaTags() {
        return richMediaTags;
    }

    @Override
    public void startElement(String uri, String localName, String element, Attributes attributes) throws SAXException {
        super.startElement(uri, localName, element, attributes);
        String tagName = element.toLowerCase();
        try {
            if (CUSTOM_TAGS.contains(tagName)) {
                RichMediaObject richMediaObject = extractRichObject(attributes, tagName);
                richMediaTags.add(richMediaObject);
            }
        } catch (RuntimeException ignored) {
        }
    }

    private RichMediaObject extractRichObject(Attributes attributes, String tagName) {
        RichMediaObject.Type type = RichMediaObject.Type.getTypeByValue(tagName);
        long id = Long.parseLong(attributes.getValue(ID));
        long owner = Long.parseLong(attributes.getValue(OWNER));

        Map<String, String> attrs = new HashMap<>();
        for (int i=0; i<attributes.getLength(); i++) {
            String key = attributes.getLocalName(i);
            if(!key.equals(ID) && !key.equals(OWNER)) {
                attrs.put(key, attributes.getValue(i));
            }
        }

        return new RichMediaObject(type, id, owner, attrs);
    }

}

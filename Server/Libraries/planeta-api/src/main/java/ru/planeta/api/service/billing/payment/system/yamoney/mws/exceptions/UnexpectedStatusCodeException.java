package ru.planeta.api.service.billing.payment.system.yamoney.mws.exceptions;

/**
 * Describes MWS unexpected response status code exception.<br>
 * Created by eshevchenko.
 */
public class UnexpectedStatusCodeException extends YaMoException {

    public UnexpectedStatusCodeException(int statusCode) {
        super("Unexpected response status code %d", statusCode);
    }
}

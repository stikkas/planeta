package ru.planeta.api.service.configurations;

import org.springframework.stereotype.Service;
import ru.planeta.model.enums.BlockSettingType;
import ru.planeta.model.enums.PermissionLevel;
import ru.planeta.model.enums.ProfileType;
import ru.planeta.model.profile.ProfileBlockSetting;

import javax.annotation.Nonnull;
import javax.annotation.PostConstruct;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.Map;


/**
 * @author a.savanovich
 */
@Service
public class DefaultBlockSettingsImpl implements DefaultBlockSettings {

    private Map<ProfileType, Map<BlockSettingType, ProfileBlockSetting>> defaultBlockSettings = new HashMap<>();

    @PostConstruct
    void init() {
        for (ProfileType type : ProfileType.values()) {
            defaultBlockSettings.put(type, getBlockSettingTypeProfileBlockSettingMapInner(type));
        }
    }

    @Override
    @Nonnull
    public Map<BlockSettingType, ProfileBlockSetting> getDefaultPermissionLevels(ProfileType type) {
        return defaultBlockSettings.get(type);
    }

    @Nonnull
    private static Map<BlockSettingType, ProfileBlockSetting> getBlockSettingTypeProfileBlockSettingMapInner(ProfileType type) {
        Map<BlockSettingType, ProfileBlockSetting> profileBlockSettings = new EnumMap<>(BlockSettingType.class);
        switch (type) {
            case EVENT:
                createDefaultEventPrivacySettings(profileBlockSettings);
                break;
            case USER:
                createDefaultUserPrivacySettings(profileBlockSettings);
                break;
            case GROUP:
                createDefaultGroupPrivacySettings(profileBlockSettings);
                break;
            default:
                createDefaultPrivacySettings(profileBlockSettings);
        }
        return profileBlockSettings;
    }

    private static void insertOneSetting(BlockSettingType type, SetLevels setter, Map<BlockSettingType, ProfileBlockSetting> profileBlockSettings) {
        ProfileBlockSetting setting = new ProfileBlockSetting();
        setting.setBlockSettingType(type);
        setter.setLevels(setting);
        profileBlockSettings.put(type, setting);
    }

    private static void createDefaultPrivacySettings(Map<BlockSettingType, ProfileBlockSetting> profileBlockSettings) {

        SettingInserter settingInserter = new SettingInserter(profileBlockSettings);
        settingInserter.setDefaultSettings(BlockSettingType.VIDEO);
        settingInserter.setDefaultSettings(BlockSettingType.AUDIO);
        settingInserter.setDefaultSettings(BlockSettingType.PHOTO);
        settingInserter.setDefaultSettings(BlockSettingType.STATUS);
        settingInserter.setDefaultSettings(BlockSettingType.CONTACT_INFO);

        settingInserter.insertSeting(BlockSettingType.WALL, new MembersCommentVoteAddSetLevels());
        settingInserter.setDefaultSettings(BlockSettingType.BLOG);
        settingInserter.insertSeting(BlockSettingType.MEMBERS, new MembersSetLevels());
        settingInserter.insertSeting(BlockSettingType.EVENTS, new MembersCommentVoteAddSetLevels());
        settingInserter.insertSeting(BlockSettingType.BROADCAST, new AdminsWriteAddSetLevels());
    }

    private static void createDefaultUserPrivacySettings(Map<BlockSettingType, ProfileBlockSetting> profileBlockSettings) {
        SettingInserter settingInserter = new SettingInserter(profileBlockSettings);
        SetLevels adminLevel = new AdminsWriteAddSetLevels();
        SetLevels userFriendSetLevels = new UserFriendsSetLevels();
        settingInserter.insertSeting(BlockSettingType.VIDEO, adminLevel);
        settingInserter.insertSeting(BlockSettingType.AUDIO, adminLevel);
        settingInserter.insertSeting(BlockSettingType.PHOTO, adminLevel);
        settingInserter.insertSeting(BlockSettingType.STATUS, adminLevel);

        settingInserter.insertSeting(BlockSettingType.BROADCAST, adminLevel);

        settingInserter.insertSeting(BlockSettingType.WALL, userFriendSetLevels);
        settingInserter.insertSeting(BlockSettingType.BLOG, adminLevel);
        settingInserter.insertSeting(BlockSettingType.EVENTS, userFriendSetLevels);
        settingInserter.insertSeting(BlockSettingType.MEMBERS, new FriendInviteSetLevels());
        settingInserter.insertSeting(BlockSettingType.USER_GROUPS, adminLevel);
        settingInserter.insertSeting(BlockSettingType.USER_WRITE_MESSAGES, new UserMessagesSetLevels());
        settingInserter.insertSeting(BlockSettingType.ABOUT, adminLevel);
        settingInserter.insertSeting(BlockSettingType.CONTACT_INFO, adminLevel);
        settingInserter.insertSeting(BlockSettingType.PROFILE_INFO, adminLevel);
        settingInserter.insertSeting(BlockSettingType.SETTINGS, new AdminSetLevels());
    }

    private static void createDefaultEventPrivacySettings(Map<BlockSettingType, ProfileBlockSetting> profileBlockSettings) {
        SettingInserter settingInserter = new SettingInserter(profileBlockSettings);
        SetLevels adminLevel = new AdminsWriteAddSetLevels();
        settingInserter.insertSeting(BlockSettingType.VIDEO, adminLevel);
        settingInserter.insertSeting(BlockSettingType.AUDIO, adminLevel);
        settingInserter.insertSeting(BlockSettingType.PHOTO, adminLevel);
        settingInserter.insertSeting(BlockSettingType.STATUS, adminLevel);

        settingInserter.insertSeting(BlockSettingType.WALL, new SubscribersSetLevels());
        settingInserter.insertSeting(BlockSettingType.MEMBERS, adminLevel);
        settingInserter.insertSeting(BlockSettingType.BLOG, adminLevel);
        settingInserter.insertSeting(BlockSettingType.EVENTS, adminLevel);
        settingInserter.insertSeting(BlockSettingType.BROADCAST, adminLevel);

        settingInserter.insertSeting(BlockSettingType.ABOUT, adminLevel);
        settingInserter.insertSeting(BlockSettingType.SETTINGS, new AdminSetLevels());
    }

    private static void createDefaultGroupPrivacySettings(Map<BlockSettingType, ProfileBlockSetting> profileBlockSettings) {

        createDefaultPrivacySettings(profileBlockSettings);
        insertOneSetting(BlockSettingType.WALL, new MembersSetLevels(), profileBlockSettings);
        insertOneSetting(BlockSettingType.GROUP_FAN_BLOG, new MembersSetLevels(), profileBlockSettings);
        SetLevels defaultSetter = new SetLevels();
        insertOneSetting(BlockSettingType.ABOUT, defaultSetter, profileBlockSettings);
        insertOneSetting(BlockSettingType.GROUP_ADMINS, defaultSetter, profileBlockSettings);
        insertOneSetting(BlockSettingType.GROUP_ARTISTS, defaultSetter, profileBlockSettings);
        insertOneSetting(BlockSettingType.GROUP_PROMO, defaultSetter, profileBlockSettings);
        insertOneSetting(BlockSettingType.SETTINGS, new ModeratorSetLevels(), profileBlockSettings);
    }

    private static class SettingInserter {
        final SetLevels defaultSetter = new SetLevels();
        Map<BlockSettingType, ProfileBlockSetting> profileBlockSettings;

        public SettingInserter(Map<BlockSettingType, ProfileBlockSetting> profileBlockSettings) {
            this.profileBlockSettings = profileBlockSettings;
        }

        public void insertSeting(BlockSettingType type, SetLevels setter) {
            insertOneSetting(type, setter, profileBlockSettings);
        }

        public void setDefaultSettings(BlockSettingType type) {
            insertSeting(type, defaultSetter);
        }
    }

    private static class SetLevels {

        public void setLevels(ProfileBlockSetting setting) {
            setting.setViewPermission(PermissionLevel.EVERYBODY);
            setting.setCommentPermission(PermissionLevel.EVERYBODY);
            setting.setVotePermission(PermissionLevel.EVERYBODY);
            setting.setWritePermission(PermissionLevel.MODERATORS);
            setting.setAddPermission(PermissionLevel.MODERATORS);
        }
    }

    private static class AdminsWriteAddSetLevels extends SetLevels {

        @Override
        public void setLevels(ProfileBlockSetting setting) {
            super.setLevels(setting);
            setting.setWritePermission(PermissionLevel.ADMINS);
            setting.setAddPermission(PermissionLevel.ADMINS);
        }
    }

    private static class UserFriendsSetLevels extends SetLevels {

        @Override
        public void setLevels(ProfileBlockSetting setting) {
            super.setLevels(setting);
            setting.setWritePermission(PermissionLevel.FRIENDS);
            setting.setAddPermission(PermissionLevel.FRIENDS);
        }
    }

    private static class UserMessagesSetLevels extends SetLevels {

        @Override
        public void setLevels(ProfileBlockSetting setting) {
            super.setLevels(setting);
            setting.setWritePermission(PermissionLevel.EVERYBODY);
            setting.setAddPermission(PermissionLevel.EVERYBODY);
        }
    }

    private static class FriendInviteSetLevels extends SetLevels {

        @Override
        public void setLevels(ProfileBlockSetting setting) {
            super.setLevels(setting);
            setting.setWritePermission(PermissionLevel.EVERYBODY);
            setting.setAddPermission(PermissionLevel.ADMINS);
        }
    }


    private static class MembersCommentVoteAddSetLevels extends SetLevels {

        @Override
        public void setLevels(ProfileBlockSetting setting) {
            super.setLevels(setting);
            setting.setWritePermission(PermissionLevel.ADMINS);
            setting.setCommentPermission(PermissionLevel.MEMBERS);
            setting.setVotePermission(PermissionLevel.MEMBERS);
            setting.setAddPermission(PermissionLevel.MEMBERS);
        }
    }

    private static class MembersSetLevels extends SetLevels {

        @Override
        public void setLevels(ProfileBlockSetting setting) {
            super.setLevels(setting);
            setting.setWritePermission(PermissionLevel.MODERATORS);
            setting.setAddPermission(PermissionLevel.MEMBERS);
            setting.setCommentPermission(PermissionLevel.MEMBERS);
            setting.setVotePermission(PermissionLevel.MEMBERS);
        }
    }

    private static class SubscribersSetLevels extends SetLevels {

        @Override
        public void setLevels(ProfileBlockSetting setting) {
            super.setLevels(setting);
            setting.setWritePermission(PermissionLevel.MODERATORS);
            setting.setAddPermission(PermissionLevel.SUBSCRIBERS);
            setting.setCommentPermission(PermissionLevel.SUBSCRIBERS);
            setting.setVotePermission(PermissionLevel.SUBSCRIBERS);
        }
    }


    private static class ModeratorSetLevels extends SetLevels {

        @Override
        public void setLevels(ProfileBlockSetting setting) {
            super.setLevels(setting);
            setting.setWritePermission(PermissionLevel.MODERATORS);
            setting.setAddPermission(PermissionLevel.MODERATORS);
            setting.setCommentPermission(PermissionLevel.MODERATORS);
            setting.setVotePermission(PermissionLevel.MODERATORS);
            setting.setViewPermission(PermissionLevel.MODERATORS);
        }
    }

    private static class AdminSetLevels extends SetLevels {

        @Override
        public void setLevels(ProfileBlockSetting setting) {
            super.setLevels(setting);
            setting.setWritePermission(PermissionLevel.ADMINS);
            setting.setAddPermission(PermissionLevel.ADMINS);
            setting.setCommentPermission(PermissionLevel.ADMINS);
            setting.setVotePermission(PermissionLevel.ADMINS);
            setting.setViewPermission(PermissionLevel.ADMINS);
        }
    }

}

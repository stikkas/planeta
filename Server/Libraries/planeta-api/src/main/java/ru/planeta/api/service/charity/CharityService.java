package ru.planeta.api.service.charity;

/*
 * Created by Alexey on 24.06.2016.
 */

import ru.planeta.model.charity.CharityCampaignsStats;
import ru.planeta.model.charity.Member;

import java.util.Date;
import java.util.List;

public interface CharityService {

    List<Member> getAllMembers(int offset, int limit);
}

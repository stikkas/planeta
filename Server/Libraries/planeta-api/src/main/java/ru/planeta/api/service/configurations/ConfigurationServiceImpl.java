package ru.planeta.api.service.configurations;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.CollectionType;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import ru.planeta.api.exceptions.NotFoundException;
import ru.planeta.api.model.ConfigurationType;
import ru.planeta.api.model.json.UserCallbackAvailabilityCondition;
import ru.planeta.api.model.json.VipClubJoinCondition;
import ru.planeta.dao.commondb.ConfigurationDAO;
import ru.planeta.model.common.Configuration;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.io.IOException;
import java.util.*;

import static java.nio.charset.StandardCharsets.UTF_8;

/**
 * @author ds.kolyshev
 *         Date: 17.05.12
 */
@Service
public class ConfigurationServiceImpl implements ConfigurationService {
    //default configuration values
    private static final String DEFAULT_STRING_VALUE = "";
    private static final int DEFAULT_THRESHOLD_VALUE = 100000;

    private static final Logger logger = Logger.getLogger(ConfigurationService.class);

    private final ConfigurationDAO configurationDAO;

    @Autowired
    public ConfigurationServiceImpl(ConfigurationDAO configurationDAO) {
        this.configurationDAO = configurationDAO;
    }

    @Nullable
    @Override
    public Configuration getConfigurationByKey(String key) {
        return configurationDAO.select(key);
    }

    @Override
    public List<Configuration> getConfigurationList(int offset, int limit) {
        return configurationDAO.selectList(null, offset, limit);
    }

    @Override
    public List<Configuration> getConfigurationList(String query, int offset, int limit) {
        return configurationDAO.selectList(query, offset, limit);
    }

    @Override
    public void saveConfiguration(Configuration configuration) {
        Configuration current = configurationDAO.select(configuration.getKey());
        if (current == null) {
            configurationDAO.insert(configuration);
        } else {
            configurationDAO.update(configuration);
        }
    }

    @Override
    public void deleteConfigurationByKey(String key) {
        configurationDAO.delete(key);
    }

    @Override
    public int getCampaignSuccessThreshold() {
        try {
            return getIntConfig(ConfigurationType.CAMPAIGN_SUCCESS_THRESHOLD_CONFIGURATION);
        } catch (NotFoundException e) {
            return DEFAULT_THRESHOLD_VALUE;
        }
    }

    @Override
    public List<Long> getPromoCampaignIds() {
        return getIds(ConfigurationType.PLANETA_PROMO_CAMPAIGN_CONFIGURATION_KEY);
    }

    @Override
    public List<Long> getPromoAboutUsCampaignIds() {
        return getIds(ConfigurationType.PLANETA_PROMO_CABOUT_US_CAMPAAIGN_CONFIGURATION_KEY);
    }

    @Override
    public void setPromoAboutUsCampaignIds(String idList) {
        Configuration configuration = getConfigurationSafe(ConfigurationType.PLANETA_PROMO_CABOUT_US_CAMPAAIGN_CONFIGURATION_KEY);
        configuration.setStringValue(idList);
        saveConfiguration(configuration);
    }

    @Override
    public List<Long> getCharityPromoCampaignIds() {
        return getIds(ConfigurationType.PLANETA_CHARITY_PROMO_CAMPAIGN_CONFIGURATION_KEY);
    }

    @Override
    public List<Long> getCharityRecentWebinarsIds() {
        return getIds(ConfigurationType.PLANETA_CHARITY_RECENT_WEBINARS_CONFIGURATION_KEY);
    }

    public List<Long> getCharityTopCampaignIds() {
        return getIds(ConfigurationType.PLANETA_CHARITY_TOP_CAMPAIGN_CONFIGURATION_KEY);
    }

    @Override
    public List<Long> getPromoProfilesIdsOfBroadcasts() {
        return getIds(ConfigurationType.PLANETA_BROADCAST_PROMO_CONFIGURATION_LIST_NEW);
    }

    @Override
    public List<Long> getShopFundsIds() {
        return getIds(ConfigurationType.PLANETA_SHOP_FUNDS);
    }

    @Override
    public List<String> getRelatedCampaignTagMnemonics() {
        try {
            String stringValues = getStringConfigSafe(ConfigurationType.PLANETA_RELATED_CAMPAIGN_TAGS_CONFIGURATION_KEY);
            return Arrays.asList(stringValues.replaceAll(" ", "").split(","));
        } catch (Exception e) {
            return Collections.emptyList();
        }
    }

    @Override
    public int getStartupSeminarId() {
        try {
            return getIntConfig(ConfigurationType.SCHOOL_STARTUP_SEMINAR);
        } catch (NotFoundException e) {
            logger.error(e.getMessageCode(), e);
            return 0;
        }
    }

    @Override
    public List<Long> getIds(String configKey) {
        try {
            String stringValues = getStringConfig(configKey);
            return convertToList(stringValues);
        } catch (NotFoundException e) {
            return Collections.emptyList();
        }
    }

    @Override
    public Set<Long> getIdsSet(String configKey) {
        try {
            String stringValues = getStringConfig(configKey);
            return convertToSet(stringValues);
        } catch (NotFoundException e) {
            return Collections.emptySet();
        }
    }

    @Nonnull
    private Configuration getConfiguration(String key) throws NotFoundException {
        Configuration result = getConfigurationByKey(key);
        if (result == null) {
            throw new NotFoundException("Configuration property not found: " + key);
        }

        return result;
    }

    private List<Configuration> getConfigurationList(String key) throws NotFoundException {
        List<Configuration> result = getConfigurationList(key, 0, 0);
        if (CollectionUtils.isEmpty(result)) {
            throw new NotFoundException("Configuration property not found: " + key);
        }

        return result;
    }

    @Nonnull
    private Configuration getConfigurationSafe(String key) {
        Configuration result = getConfigurationByKey(key);
        if (result == null) {
            result = new Configuration();
            result.setKey(key);
        }
        return result;
    }

    @Nullable
    @Override
    public String getStringConfig(String configKey) throws NotFoundException {
        return getConfiguration(configKey).getStringValue();
    }

    @Nonnull
    private String getStringConfigSafe(String configKey) throws NotFoundException {
        String result = getStringConfig(configKey);
        if (result == null) {
            return StringUtils.EMPTY;
        }
        return result;
    }

    private List<String> getStringListConfig(String configKey) throws NotFoundException {
        List<String> result = new ArrayList<>();
        for (Configuration configuration : getConfigurationList(configKey)) {
            result.add(configuration.getStringValue());
        }
        return result;
    }

    private int getIntConfig(String configKey) throws NotFoundException {
        return getConfiguration(configKey).getIntValue();
    }

    private long getLongConfig(String configKey, long defaultValue) throws NotFoundException {
        return NumberUtils.toLong(getConfiguration(configKey).getStringValue(), defaultValue);
    }

    private boolean getBoolConfig(String key, boolean defaultValue) {
        try {
            return getConfiguration(key).getBooleanValue();
        } catch (Exception e) {
            return defaultValue;
        }
    }

    @Override
    public <T> List<T> getJsonArrayConfig(Class<T> elementClass, String key) {
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            String configString = getStringConfig(key);
            CollectionType type = objectMapper.getTypeFactory().constructCollectionType(LinkedList.class, elementClass);
            return objectMapper.readValue(configString, type);
        } catch (Exception e) {
            return new LinkedList<>();
        }
    }

    private <T> T getJsonConfig(String key, Class<T> resultClass) throws NotFoundException {
        String json = getStringConfigSafe(key);
        try {
            return new ObjectMapper().readValue(json.getBytes(UTF_8), resultClass);
        } catch (IOException e) {
            logger.error(String.format("Can't parse JSON config '%s':%n%s", key, json), e);
            throw new NotFoundException("Configuration property not found: " + key);
        }
    }

    @Override
    public void saveConfigAsJson(Object object, String configurationKey) throws NotFoundException {
        ObjectMapper objectMapper = new ObjectMapper();
        String json;
        try {
            json = objectMapper.writeValueAsString(object);
        } catch (Exception e) {
            throw new NotFoundException("saveConfigurationAsJsonStrValue Fail");
        }
        Configuration configuration = getConfigurationSafe(configurationKey);
        configuration.setStringValue(json);
        saveConfiguration(configuration);
    }

    @Override
    public void saveStrValue(String strValue, String configurationKey) {
        Configuration configuration = getConfigurationSafe(configurationKey);
        configuration.setStringValue(strValue);
        saveConfiguration(configuration);
    }

    @Override
    public void setPromoCampaignIds(String idList) {
        Configuration configuration = getConfigurationSafe(ConfigurationType.PLANETA_PROMO_CAMPAIGN_CONFIGURATION_KEY);
        configuration.setStringValue(idList);
        saveConfiguration(configuration);
    }

    @Override
    public void setCharityRecentWebinarsIds(String idList) {
        Configuration configuration = getConfigurationSafe(ConfigurationType.PLANETA_CHARITY_RECENT_WEBINARS_CONFIGURATION_KEY);
        configuration.setStringValue(idList);
        saveConfiguration(configuration);
    }

    @Override
    public void setCharityPromoCampaignIds(String idList) {
        Configuration configuration = getConfigurationSafe(ConfigurationType.PLANETA_CHARITY_PROMO_CAMPAIGN_CONFIGURATION_KEY);
        configuration.setStringValue(idList);
        saveConfiguration(configuration);
    }

    @Override
    public void setCharityTopCampaignIds(String idList) {
        Configuration configuration = getConfigurationSafe(ConfigurationType.PLANETA_CHARITY_TOP_CAMPAIGN_CONFIGURATION_KEY);
        configuration.setStringValue(idList);
        saveConfiguration(configuration);
    }

    @Override
    public void setPromoProfilesIdsOfBroadcasts(String profileIdsList) {
        Configuration configuration = getConfigurationByKey(ConfigurationType.PLANETA_BROADCAST_PROMO_CONFIGURATION_LIST_NEW);
        if (configuration != null) {
            configuration.setStringValue(profileIdsList);
        } else {
            configuration = new Configuration();
            configuration.setKey(ConfigurationType.PLANETA_BROADCAST_PROMO_CONFIGURATION_LIST_NEW);
            configuration.setStringValue(profileIdsList);
        }
        saveConfiguration(configuration);
    }

    @Override
    public long getPromoNewsSourceProfileId() {
        try {
            return getIntConfig(ConfigurationType.TECHBATTLE_NEWS_SOURCE_PROFILE_ID);
        } catch (Exception ex) {
            return 0;
        }
    }

    @Override
    public List<Long> getPromoNewsSliderIds() {
        return getIds(ConfigurationType.TECHBATTLE_NEWS_SLIDER_IDS);
    }

    @Override
    public List<String> getCampaignWelcomeLowBanner() throws NotFoundException {
        return getStringListConfig(ConfigurationType.PLANETA_CAMPAIGN_LOW_BANNER_CONFIGURATION_KEY);
    }

    @Override
    public String getPartnersWelcome() throws NotFoundException {
        return getStringConfig(ConfigurationType.PLANETA_WELCOME_PARTNERS_CONFIGURATION_KEY);
    }


    @Override
    public String getFundingRulesCustomHtml() throws NotFoundException {
        return getStringConfig(ConfigurationType.PLANETA_CAMPAIGN_FUNDING_RULES_CONFIGURATION_KEY);
    }

    // Helper Methods


    private static List<Long> convertToList(String stringValues) {
        List<Long> ids = new ArrayList<>();
        if (stringValues != null) {
            String[] idStrings = stringValues.replaceAll(" ", "").split(",");
            try {
                for (String idString : idStrings) {
                    ids.add(NumberUtils.toLong(idString.trim()));
                }
            } catch (Exception ex) {
                logger.error(ex.getMessage());
            }
        }
        return ids;
    }

    private static Set<Long> convertToSet(String stringValues) {
        Set<Long> ids = new HashSet<>();
        if (stringValues != null) {
            String[] idStrings = stringValues.replaceAll(" ", "").split(",");
            try {
                for (String idString : idStrings) {
                    ids.add(NumberUtils.toLong(idString.trim()));
                }
            } catch (Exception ex) {
                logger.error(ex.getMessage());
            }
        }
        return ids;
    }

    @Override
    public boolean needNotifyVipClubMember() {
        return getBoolConfig(ConfigurationType.VIP_CLUB_MEMBER_NOTIFY, true);
    }

    @Override
    public VipClubJoinCondition getVipClubJoinCondition() {
        try {
            return getJsonConfig(ConfigurationType.VIP_CLUB_JOIN_CONDITION, VipClubJoinCondition.class);
        } catch (NotFoundException e) {
            return new VipClubJoinCondition();
        }
    }

    @Override
    public UserCallbackAvailabilityCondition getCallbackAvailabilityCondition() {
        try {
            return getJsonConfig(ConfigurationType.USER_CALLBACK_AVAILABILITY_CONDITION, UserCallbackAvailabilityCondition.class);
        } catch (NotFoundException e) {
            return new UserCallbackAvailabilityCondition();
        }
    }

    @Override
    public int getNewsNotificationDaysLimit() {
        try {
            return getIntConfig(ConfigurationType.NEWS_NOTIFICATION_DAYS_LIMIT);
        } catch (NotFoundException e) {
            return 0;
        }
    }

    @Override
    public int getPaymentErrorsLargeSum() {
        try {
            return getIntConfig(ConfigurationType.PAYMENT_ERRORS_LARGE_SUM);
        } catch (NotFoundException e) {
            return 5000;
        }
    }

    @Cacheable("commercialCampaigns")
    @Override
    public Set<Long> getCommercialCampaigns() {
        return getIdsSet(ConfigurationType.COMMERICAL_CAMPAIGNS);
    }

    @Cacheable("commercialShares")
    @Override
    public Set<Long> getCommercialShares() {
        return getIdsSet(ConfigurationType.COMMERCIAL_SHARES);
    }

}

package ru.planeta.api.service.social;

import ru.planeta.api.aspect.transaction.NonTransactional;
import ru.planeta.api.model.ExternalUserInfo;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.io.IOException;

/**
 * Date: 21.09.12
 *
 * @author s.kalmykov
 */
public interface OAuthClientService {

    @Nullable
    String getRedirectUrl(String saveRedirectUrl);

    // may be long
    @Nullable
    @NonTransactional
    ExternalUserInfo authorizeExternal(String code, String redirectUrl) throws IOException;

    boolean isMyUrl(@Nonnull String url);

}

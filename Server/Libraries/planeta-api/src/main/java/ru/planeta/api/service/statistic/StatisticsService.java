package ru.planeta.api.service.statistic;

import ru.planeta.api.aspect.transaction.NonTransactional;
import ru.planeta.model.stat.CampaignStat;
import ru.planeta.model.stat.StatEventType;

import java.util.Date;
import java.util.List;

/**
 * Interface StatisticsService
 *
 * @author a.tropnikov
 */
public interface StatisticsService {

    void trackEvent(StatEventType type, Long objectId, String referer, final long visitorId);

    @NonTransactional
    List<CampaignStat> getCampaignGeneralStats(long campaignId, Date from, Date to, int offset, int limit);

    @NonTransactional
    CampaignStat getCampaignGeneralTotalStats(long campaignId, Date from, Date to);

    @NonTransactional
    List<CampaignStat> getCampaignRefererStats(long campaignId, Date from, Date to, int offset, int limit);

    @NonTransactional
    List<CampaignStat> getCampaignCountryStats(long campaignId, Date from, Date to, int offset, int limit);

    @NonTransactional
    List<CampaignStat> getCampaignCityStats(long campaignId, Date from, Date to, int offset, int limit);

}

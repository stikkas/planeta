package ru.planeta.api.service.interactive;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.planeta.api.exceptions.MessageCode;
import ru.planeta.api.exceptions.PermissionException;
import ru.planeta.api.service.security.PermissionService;
import ru.planeta.dao.trashcan.InteractiveEditorDataDAO;
import ru.planeta.model.trashcan.InteractiveEditorData;

import java.util.Date;

@Service
public class InteractiveEditorDataServiceImpl implements InteractiveEditorDataService {
    @Autowired
    private PermissionService permissionService;

    @Autowired
    private InteractiveEditorDataDAO interactiveEditorDataDAO;

    @Override
    public void insert(InteractiveEditorData interactiveEditorData) {
        interactiveEditorData.setTimeAdded(new Date());
        interactiveEditorDataDAO.insert(interactiveEditorData);
    }

    @Override
    public InteractiveEditorData getByUserId(long userId) {
        return interactiveEditorDataDAO.selectByUserId(userId);
    }

    @Override
    public InteractiveEditorData getByEmail(String email) {
        return interactiveEditorDataDAO.selectByEmail(email);
    }

    @Override
    public void update(InteractiveEditorData interactiveEditorData) {
        interactiveEditorDataDAO.update(interactiveEditorData);
    }

    @Override
    public void deleteByUserId(long clientId, long userId) throws PermissionException {
        if (clientId != userId && !permissionService.hasAdministrativeRole(clientId)) {
            throw new PermissionException(MessageCode.USER_NOT_ADMIN);
        }
        interactiveEditorDataDAO.deleteByUserId(userId);
    }
}

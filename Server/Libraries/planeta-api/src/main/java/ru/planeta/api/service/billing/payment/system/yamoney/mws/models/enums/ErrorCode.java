package ru.planeta.api.service.billing.payment.system.yamoney.mws.models.enums;

import javax.xml.bind.annotation.XmlEnumValue;

/**
 * Yandex.Money MWS (Merchant Web Service) errors codes.
 */
public enum ErrorCode {

    /**
     * Common errors
     */
    @XmlEnumValue(value = "0")
    NO_ERRORS,
    @XmlEnumValue(value = "10")
    XML_PARSING,
    @XmlEnumValue(value = "50")
    PKCS7_DATA_INTEGRITY,
    @XmlEnumValue(value = "51")
    PKCS7_SIGNATURE,
    @XmlEnumValue(value = "53")
    UNKNOWN_CERTIFICATE,
    @XmlEnumValue(value = "55")
    CERTIFICATE_HAS_EXPIRED,
    @XmlEnumValue(value = "110")
    OPERATION_PERMISSION,

    /**
     * Common parameters errors
     */
    @XmlEnumValue(value = "111")
    INVALID_PARAM_REQUEST_DT,
    @XmlEnumValue(value = "112")
    INVALID_PARAM_INVOICE_ID,
    @XmlEnumValue(value = "113")
    INVALID_PARAM_SHOP_ID,
    @XmlEnumValue(value = "114")
    INVALID_PARAM_ORDER_NUMBER,
    @XmlEnumValue(value = "115")
    INVALID_PARAM_CLIENT_ORDER_ID,
    @XmlEnumValue(value = "117")
    INVALID_PARAM_CLIENT_STATUS,
    @XmlEnumValue(value = "118")
    INVALID_PARAM_CLIENT_FROM,
    @XmlEnumValue(value = "119")
    INVALID_PARAM_CLIENT_TILL,

    /**
     * Information requests errors
     */
    @XmlEnumValue(value = "200")
    INVALID_PARAM_OUTPUT_FORMAT,
    @XmlEnumValue(value = "201")
    INVALID_PARAM_CSV_DELIMITER,
    @XmlEnumValue(value = "202")
    INVALID_PARAM_ORDER_CREATED_DATETIME_GREATER_OR_EQUAL,
    @XmlEnumValue(value = "203")
    INVALID_PARAM_ORDER_CREATED_DATETIME_LESS_OR_EQUAL,
    @XmlEnumValue(value = "204")
    INVALID_PARAM_PAID,
    @XmlEnumValue(value = "205")
    INVALID_PARAM_PAYMENT_DATETIME_GREATER_OR_EQUAL,
    @XmlEnumValue(value = "206")
    INVALID_PARAM_PAYMENT_DATETIME_LESS_OR_EQUAL,
    @XmlEnumValue(value = "207")
    INVALID_PARAM_OUTPUT_FIELDS,
    @XmlEnumValue(value = "208")
    EMPTY_CREATION_DATE_RANGE,
    @XmlEnumValue(value = "209")
    EXCEEDED_CREATION_DATE_RANGE,
    @XmlEnumValue(value = "210")
    EMPTY_PAYMENT_DATE_RANGE,
    @XmlEnumValue(value = "211")
    EXCEEDED_PAYMENT_DATE_RANGE,
    @XmlEnumValue(value = "212")
    SELECTION_PARAMS_CONFLICT,
    @XmlEnumValue(value = "213")
    NO_SELECTION_CRITERIA,
    @XmlEnumValue(value = "214")
    NOT_SET_SHOP_ID_PARAM_AT_ORDER_NUMBER_SELECTION,
    @XmlEnumValue(value = "215")
    NOT_SET_SHOP_ID_PARAM_AT_INVOICE_ID_SELECTION,
    @XmlEnumValue(value = "216")
    TOO_MANY_SELECTION_RESULTS,
    @XmlEnumValue(value = "217")
    INVALID_PARAM_PARTIAL,

    /**
     * Financial transaction requests errors
     */
    @XmlEnumValue(value = "402")
    INVALID_PARAM_AMOUNT,
    @XmlEnumValue(value = "403")
    INVALID_PARAM_CURRENCY,
    @XmlEnumValue(value = "404")
    INVALID_PARAM_CAUSE,
    @XmlEnumValue(value = "405")
    NON_UNIQUE_OPERATION_ID,
    @XmlEnumValue(value = "410")
    CANT_RETURN_NON_PAYED_ORDER,
    @XmlEnumValue(value = "411")
    CANT_DELIVER_PAYMENT_NOTIFY,
    @XmlEnumValue(value = "412")
    REQUEST_CURRENCY_MISMATCH,
    @XmlEnumValue(value = "413")
    RETURN_AMOUNT_MORE_THAN_PAYMENT_AMOUNT,
    @XmlEnumValue(value = "414")
    PAYMENT_AMOUNT_ALREADY_RETURNED,
    @XmlEnumValue(value = "415")
    NOT_FOUND_ORDER_BY_INVOICE_ID,
    @XmlEnumValue(value = "416")
    CANT_RETURN_NOT_ENOUGH_MONEY,
    @XmlEnumValue(value = "417")
    CANT_RETURN_BUYER_ACCOUNT_CLOSED,
    @XmlEnumValue(value = "418")
    CANT_RETURN_BUYER_ACCOUNT_BLOCKED,
    @XmlEnumValue(value = "601")
    CANT_REPEAT_CARD_PAYMENT,
    @XmlEnumValue(value = "602")
    CANT_REPEAT_CURRENT_PAYMENT,
    @XmlEnumValue(value = "604")
    INVALID_PARAM_CVV,
    @XmlEnumValue(value = "603")
    REQUIRED_PARAM_ORDER_NUMBER,

    /**
     * Other errors
     */
    @XmlEnumValue(value = "1000")
    TECH;
}

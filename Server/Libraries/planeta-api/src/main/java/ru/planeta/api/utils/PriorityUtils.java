package ru.planeta.api.utils;

import org.jetbrains.annotations.NotNull;

import java.util.concurrent.*;

public class PriorityUtils {
    static class PriorityTask<T> extends FutureTask<T> implements Comparable<PriorityTask>  {
        private int priority;

        public int getPriority() {
            return priority;
        }

        public void setPriority(int priority) {
            this.priority = priority;
        }

        PriorityTask(Runnable runnable, T result, int priority) {
            super(runnable, result);
            this.priority = priority;
        }

        public PriorityTask(Callable<T> callable, int priority) {
            super(callable);
            this.priority = priority;
        }

        @Override
        public int compareTo(@NotNull PriorityTask o) {
            return Integer.compare(o.getPriority(), this.getPriority());
        }
    }

    public static class PriorityExecutor extends ThreadPoolExecutor {

        PriorityExecutor(int corePoolSize, int maximumPoolSize,
                         long keepAliveTime, TimeUnit unit, BlockingQueue<Runnable> workQueue) {
            super(corePoolSize, maximumPoolSize, keepAliveTime, unit, workQueue);
        }

        public Future<?> submit(Runnable task, int priority) {
            return super.submit(new PriorityTask(task, null, priority));
        }

        public void execute(Runnable command, int priority) {
            super.execute(new PriorityTask(command, null, priority));
        }

        @Override
        protected <T> RunnableFuture<T> newTaskFor(Callable<T> callable) {
            return (RunnableFuture<T>) callable;
        }

        @Override
        protected <T> RunnableFuture<T> newTaskFor(Runnable runnable, T value) {
            return (RunnableFuture<T>) runnable;
        }
    }

    public static PriorityExecutor newFixedThreadPoolPriorityExecutor(int nThreads) {
        return new PriorityExecutor(nThreads, nThreads, 0L,
                TimeUnit.MILLISECONDS, new PriorityBlockingQueue<Runnable>());
    }
}

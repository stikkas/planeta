package ru.planeta.api.text;

/**
 * Created with IntelliJ IDEA.
 * User: a.volkov
 * Date: 13.02.13
 * Time: 16:29
 */
public abstract class VisualAttachment extends Attachment {

    private Integer width;
    private Integer height;

    public Integer getWidth() {
        return width;
    }

    public void setWidth(Integer width) {
        this.width = width;
    }

    public Integer getHeight() {
        return height;
    }

    public void setHeight(Integer height) {
        this.height = height;
    }
}

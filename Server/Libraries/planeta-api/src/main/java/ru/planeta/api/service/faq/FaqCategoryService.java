package ru.planeta.api.service.faq;

import ru.planeta.model.commondb.FaqArticle;
import ru.planeta.model.commondb.FaqCategory;

import java.util.List;

/**
 * Created by kostiagn on 01.09.2015.
 */
public interface FaqCategoryService {
    List<FaqCategory> selectFaqCategoryList(int offset, int limit);
}

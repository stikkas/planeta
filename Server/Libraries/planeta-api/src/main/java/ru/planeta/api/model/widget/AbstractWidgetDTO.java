package ru.planeta.api.model.widget;

/**
 * Created with IntelliJ IDEA.
 * Date: 07.08.13
 * Time: 17:42
 */
public abstract class AbstractWidgetDTO {
    private int percentCollected;
    private int moneyCollected;
    private int moneyTarget;
    private WidgetColorTheme theme;

    protected AbstractWidgetDTO(int moneyCollected, int moneyTarget, WidgetColorTheme theme) {
        this.moneyCollected = moneyCollected;
        this.moneyTarget = moneyTarget;
        this.theme = theme;
        if (moneyTarget > 0){
            this.percentCollected = Math.min(moneyCollected * 100 / moneyTarget, 100);
        }
    }

    public int getPercentCollected() {
        return percentCollected;
    }

    public void setPercentCollected(int percentCollected) {
        this.percentCollected = percentCollected;
    }

    public int getMoneyCollected() {
        return moneyCollected;
    }

    public void setMoneyCollected(int moneyCollected) {
        this.moneyCollected = moneyCollected;
    }

    public int getMoneyTarget() {
        return moneyTarget;
    }

    public void setMoneyTarget(int moneyTarget) {
        this.moneyTarget = moneyTarget;
    }

    public WidgetColorTheme getTheme() {
        return theme;
    }

    public void setTheme(WidgetColorTheme theme) {
        this.theme = theme;
    }
}

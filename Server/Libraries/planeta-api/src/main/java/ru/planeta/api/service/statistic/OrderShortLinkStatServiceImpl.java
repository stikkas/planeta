package ru.planeta.api.service.statistic;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.planeta.dao.statdb.OrderShortLinkStatDAO;
import ru.planeta.commons.model.OrderShortLinkStat;

@Service
public class OrderShortLinkStatServiceImpl implements OrderShortLinkStatService {
    private final OrderShortLinkStatDAO orderShortLinkStatDAO;

    @Autowired
    public OrderShortLinkStatServiceImpl(OrderShortLinkStatDAO orderShortLinkStatDAO) {
        this.orderShortLinkStatDAO = orderShortLinkStatDAO;
    }

    @Override
    public void add(OrderShortLinkStat orderShortLinkStat) {
        orderShortLinkStatDAO.insert(orderShortLinkStat);
    }

    @Override
    public OrderShortLinkStat getById(long shortLinkStatsId) {
        return orderShortLinkStatDAO.selectById(shortLinkStatsId);
    }
}

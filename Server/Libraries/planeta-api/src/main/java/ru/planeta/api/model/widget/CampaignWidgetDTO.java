package ru.planeta.api.model.widget;

/**
 * Date: 07.08.13
 * Time: 16:44
 */
public class CampaignWidgetDTO extends AbstractWidgetDTO {

    private String campaignUrl;
    private String campaignImgUrl;
    private String campaignName;


    public CampaignWidgetDTO(String campaignName, WidgetColorTheme theme, String campaignUrl, String campaignImgUrl, int moneyCollected, int moneyTarget) {
        super(moneyCollected, moneyTarget, theme);
        this.campaignName = campaignName;
        this.campaignUrl = campaignUrl;
        this.campaignImgUrl = campaignImgUrl;
    }

    public String getCampaignUrl() {
        return campaignUrl;
    }

    public void setCampaignUrl(String campaignUrl) {
        this.campaignUrl = campaignUrl;
    }

    public String getCampaignImgUrl() {
        return campaignImgUrl;
    }

    public void setCampaignImgUrl(String campaignImgUrl) {
        this.campaignImgUrl = campaignImgUrl;
    }

    public String getCampaignName() {
        return campaignName;
    }

    public void setCampaignName(String campaignName) {
        this.campaignName = campaignName;
    }
}

package ru.planeta.api.service.geo;

import ru.planeta.model.profile.location.*;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Collection;
import java.util.List;

/**
 * User: eshevchenko
 * Date: 09.06.12
 * Time: 15:33
 */
public interface GeoService {

    Collection<Country> getCountries();

    @Nullable
    Country getCountryByName(String name);

    Collection<Country> getCountriesInCampaigns();

    Collection<Country> getCountriesNew(boolean onlyInCampaigns);

    @Nullable
	Country getCountryById(int countryId);

    List<City> getCountryCitiesBySubstring(long countryId, String subName, int offset, int limit);

    List<Region> getCountryRegionsBySubstring(int countryId, String subName, String lang, int offset, int limit);

    List<City> getRegionCitiesBySubstring(long locationId, String substring, int offset, int limit);

    List<City> getCitiesWithGeo(int offset, int limit);

	City getCityById(int cityId);

    void addNewCity(String nameRu, int regionId);

    IGeoLocation getLocation(IGeoLocation location);

    IGeoLocation getLocation(int locationId, @Nonnull LocationType type);

    Collection<IGeoLocation> getTopLocations();

    List<? extends IGeoLocation> getNestedLocations(int locationId, LocationType type);

    IGeoLocation getLocationWithParents(IGeoLocation location);

}

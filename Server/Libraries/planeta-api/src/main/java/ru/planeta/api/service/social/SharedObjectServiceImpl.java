package ru.planeta.api.service.social;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.planeta.dao.statdb.SharedObjectDAO;
import ru.planeta.dao.statdb.SharedObjectStatsDAO;
import ru.planeta.model.stat.SharedObject;
import ru.planeta.model.stat.SharedObjectStats;

import javax.annotation.Nullable;

/**
 * User: m.shulepov
 * Date: 23.10.12
 * Time: 17:05
 */
@Service
public class SharedObjectServiceImpl implements SharedObjectService {

    private final SharedObjectDAO sharedObjectDAO;
    private final SharedObjectStatsDAO sharedObjectStatsDAO;

    @Autowired
    public SharedObjectServiceImpl(SharedObjectDAO sharedObjectDAO, SharedObjectStatsDAO sharedObjectStatsDAO) {
        this.sharedObjectDAO = sharedObjectDAO;
        this.sharedObjectStatsDAO = sharedObjectStatsDAO;
    }

    @Override
    public void processSharedObject(long clientId, @Nullable String uuid, SharedObject sharedObject) {

        sharedObject.setUrl(getCroppedUrl(sharedObject.getUrl()));
        sharedObject.setAuthorProfileId(clientId);

        // try to get shared object by id or url
        SharedObjectStats existingSharedObjectStats;
        if (sharedObject.getSharedObjectId() != 0) {
            existingSharedObjectStats = sharedObjectStatsDAO.selectById(sharedObject.getSharedObjectId());
        } else {
            existingSharedObjectStats = sharedObjectStatsDAO.selectByUrl(sharedObject.getUrl());
        }

        // try to get shared object for specific service, if profile is not anonymous
        SharedObject sharedObjectDetails = null;
        if (existingSharedObjectStats != null) {
            sharedObjectDetails = sharedObjectDAO.selectSharedObjectDetails(clientId, uuid, existingSharedObjectStats.getSharedObjectId(), existingSharedObjectStats.getUrl(), sharedObject.getShareServiceType());
        }

        // if profile has already shared this object on specific service exit
        if (sharedObjectDetails != null) {
            return;
        }

        // insert new record for shared object or update total shares count of existing one
        if (existingSharedObjectStats != null) {
            sharedObjectStatsDAO.updateCount(existingSharedObjectStats.getSharedObjectId());
        } else {
            existingSharedObjectStats = new SharedObjectStats(sharedObject.getUrl());
            sharedObjectStatsDAO.insert(existingSharedObjectStats);
        }

        sharedObject.setSharedObjectId(existingSharedObjectStats.getSharedObjectId());
        sharedObjectDAO.insertDetails(sharedObject);
    }

    @Nullable
    @Override
    public SharedObjectStats getSharedObjectByUrl(String url){
        return sharedObjectStatsDAO.selectByUrl(getCroppedUrl(url));
    }

    private static String getCroppedUrl(String url) {
        return StringUtils.substring(url, 0, 256);
    }

}

package ru.planeta.api.log.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.planeta.api.aspect.transaction.NonTransactional;
import ru.planeta.api.model.json.DBLogRecordInfo;
import ru.planeta.dao.statdb.DBLogDAO;
import ru.planeta.model.stat.log.LoggerType;
import ru.planeta.model.stat.log.DBLogRecord;
import ru.planeta.model.stat.log.HttpInteractionLogRecord;

import java.util.*;

/**
 * Database log service implementation.<br>
 * User: eshevchenko
 */
@Service
public class DBLogServiceImpl implements DBLogService {

    @Autowired
    private DBLogDAO dbLogDAO;

    @Override
    @NonTransactional
    public List<DBLogRecordInfo> getLogRecords(LoggerType loggerType, long objectId, long subjectId) {
        List<DBLogRecord> records = dbLogDAO.selectLogRecords(loggerType, objectId, subjectId);
        return collectDbLogRecordsInfo(records);
    }

    private List<DBLogRecordInfo> collectDbLogRecordsInfo(List<DBLogRecord> records) {
        List<DBLogRecordInfo> result = new ArrayList<>();

        Map<Long, HttpInteractionLogRecord> interactionById = new HashMap<>();
        for (DBLogRecord record : records) {
            interactionById.put(record.getRequestId(), null);
            interactionById.put(record.getResponseId(), null);
        }

        for (HttpInteractionLogRecord request : dbLogDAO.selectHttpInteractionLogByRecordIds(interactionById.keySet(), null)) {
            interactionById.put(request.getId(), request);
        }

        for (DBLogRecord record : records) {
            HttpInteractionLogRecord request = interactionById.get(record.getRequestId());
            HttpInteractionLogRecord response = interactionById.get(record.getResponseId());

            result.add(DBLogRecordInfo.Companion.create(record, request, response));
        }

        return result;
    }

    @Override
    @NonTransactional
    public long addRecordToLog(String message, LoggerType loggerType, Long objectId, Long subjectId, Long requestId, Long responseId) {
        return dbLogDAO.insertLogRecord(message, loggerType, getSafeId(objectId), getSafeId(subjectId), getSafeId(requestId), getSafeId(responseId));
    }

    @Override
    @NonTransactional
    public long addRecordToLog(String message, LoggerType loggerType) {
        return addRecordToLog(message, loggerType, 0L, 0L, 0L, 0L);
    }

    @Override
    @NonTransactional
    public void attachRefInfo(long recordId, Long objectId, Long subjectId, Long requestId, Long responseId) {
        dbLogDAO.updateRefInfo(recordId, getSafeId(objectId), getSafeId(subjectId), getSafeId(requestId), getSafeId(responseId));
    }

    @Override
    @NonTransactional
    public long addRequestToLog(String metaData) {
        return dbLogDAO.insertRequestLogRecord(metaData);
    }

    @Override
    @NonTransactional
    public long addResponseToLog(long requestId, String metaData) {
        return dbLogDAO.insertResponseLogRecord(requestId, metaData);
    }

    private static long getSafeId(Long id) {
        return (id != null) ? id : 0L;
    }
}

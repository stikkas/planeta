package ru.planeta.api.service.admin;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.log4j.Logger;
import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.xml.sax.SAXException;
import ru.planeta.api.service.configurations.StaticNodesService;
import ru.planeta.commons.external.oembed.OembedInfo;
import ru.planeta.commons.external.oembed.provider.Youtube;
import ru.planeta.dao.commondb.VideoCacheDAO;
import ru.planeta.dao.commondb.CampaignDAO;
import ru.planeta.dao.profiledb.VideoDAO;
import ru.planeta.model.common.CachedVideo;
import ru.planeta.model.common.campaign.Campaign;
import ru.planeta.model.profile.media.Video;
import ru.planeta.utils.ThumbConfiguration;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.net.URL;
import java.nio.charset.StandardCharsets;

/**
 *
 * Created by a.savanovich on 08.12.2015.
 */
@Service
public class VideoUpdaterServiceImpl implements VideoUpdaterService {

    private static final Logger log = Logger.getLogger(VideoUpdaterServiceImpl.class);

    @Autowired
    private VideoDAO videoDAO;

    @Autowired
    private VideoCacheDAO videoCacheDAO;

    @Autowired
    private StaticNodesService staticNodesService;

    @Autowired
    private CampaignDAO campaignDAO;

    private void updateCachedVideo(@Nonnull Video video) {
        CachedVideo cachedVideo = videoCacheDAO.selectById(video.getCachedVideoId());
        cachedVideo.setThumbnailUrl(video.getImageUrl());
        videoCacheDAO.update(cachedVideo);
    }

    private Video updateYoutubeVideo(long videoId, long profileId) throws IOException, JSONException {

        Video video = videoDAO.selectVideoById(profileId, videoId);
        if (video != null) {
            OembedInfo info = Youtube.getOembedInfoById(video.getVideoUrl());

            ThumbConfiguration commentThumb = new ThumbConfiguration();
            // TODO: Move to constants
            commentThumb.setWidth(640);
            commentThumb.setHeight(360);
            commentThumb.setCrop(true);
            commentThumb.setPad(false);

            // https://planeta.atlassian.net/browse/PLANETA-15355 cache prevent
            String imagePath = staticNodesService.wrapImageUrl(info.getThumbnailUrl() + "?rnd=" + RandomStringUtils.randomAlphanumeric(4), commentThumb, true);
            video.setImageUrl(imagePath);
            videoDAO.update(video);
        }
        return video;
    }

    @Override
    public Video updateYoutubeAndCachedVideo(long videoId, long profileId) throws IOException, JSONException {
        Video video = updateYoutubeVideo(videoId, profileId);
        if (video != null) {
            // flushUploaderCache(video);
            updateCachedVideo(video);
        }
        return video;
    }

    private void flushUploaderCache(String imageUrl) {
        try {
            String url = imageUrl + "&flushCache=true";
            log.info(url);
            String done = IOUtils.toString(new URL(url), StandardCharsets.UTF_8);
            log.debug(done);
        } catch (Exception ex) {
            log.warn(ex);
        }
    }

    private void updateCampaignViewUrl(long campaignId) throws IOException, JSONException {
        log.info("Start update view url");
        Campaign campaign = campaignDAO.selectForUpdate(campaignId);
        Video video = updateYoutubeAndCachedVideo(campaign.getVideoId(), campaign.getVideoProfileId());
        if (video != null) {
            campaign.setViewImageUrl(video.getImageUrl());
            campaignDAO.update(campaign);
        } else {
            log.warn("Video id #" + campaign.getVideoId() + " not found!");
        }
        log.info("End update view url");
    }

    @Override
    public void updateCampaignDescription(long campaignId, VideoUpdaterService videoUpdaterService) throws IOException, SAXException {
        Campaign campaign = campaignDAO.selectCampaignById(campaignId);
        if (campaign != null) {
            VideoParser handler = new VideoParser(videoDAO);
            handler.setVideoUpdater(videoUpdaterService);
            String result = handler.replace(campaign.getDescriptionHtml());
            Campaign updated = campaignDAO.selectForUpdate(campaignId);
            updated.setDescriptionHtml(result);
            campaignDAO.update(updated);
        }
    }

    @Override
    public void updateCampaign(long campaignId) throws IOException, SAXException {
        try {
            updateCampaignViewUrl(campaignId);
        } catch (JSONException e) {
            log.warn(e);
        }
        updateCampaignDescription(campaignId, this);
    }
}

package ru.planeta.api.service.statistic;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.planeta.api.aspect.transaction.NonTransactional;
import ru.planeta.api.helper.ProjectService;
import ru.planeta.commons.web.WebUtils;
import ru.planeta.dao.statdb.CampaignStatDAO;
import ru.planeta.model.enums.ProjectType;
import ru.planeta.model.stat.CampaignStat;
import ru.planeta.model.stat.StatEventType;

import javax.annotation.PreDestroy;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Class StatisticsServiceImpl
 *
 * @author a.tropnikov
 */
@Service
public class StatisticsServiceImpl implements StatisticsService {

    private static final Logger log = Logger.getLogger(StatisticsServiceImpl.class);

    private static final String STAT_EVENT_URI = "/stat-event";

    private final ProjectService projectService;
    private final CampaignStatDAO campaignStatDAO;

    private final ExecutorService executor = Executors.newFixedThreadPool(5);

    @Autowired
    public StatisticsServiceImpl(ProjectService projectService, CampaignStatDAO campaignStatDAO) {
        this.projectService = projectService;
        this.campaignStatDAO = campaignStatDAO;
    }

    @Override
    public void trackEvent(final StatEventType type, final Long objectId, final String referer, final long visitorId) {
        try {
            executor.submit(new Runnable() {
                @Override
                public void run() {
                    WebUtils.Parameters params = new WebUtils.Parameters()
                            .add("type", type)
                            .add("visitor", visitorId)
                            .add("object", objectId)
                            .add("ref", referer);
                    String url = projectService.getUrl(ProjectType.STATISTIC_SERVICE, STAT_EVENT_URI, params);
                    WebUtils.downloadString(url);
                }
            });
        } catch(Exception e) {
            log.warn(e);
        }
    }

    @Override
    @NonTransactional
    public List<CampaignStat> getCampaignGeneralStats(long campaignId, Date from, Date to, int offset, int limit) {
        List<CampaignStat> result = campaignStatDAO.selectGeneralStats(campaignId, from, to, offset, limit);
        if (result.size() > 1000) {
            log.error("too big getCampaignGeneralStats " + campaignId);
            try {
                throw new Exception("Too big getCampaignGeneralStats");
            } catch (Exception ex) {
                log.error(ex);
            }
        }
        return result;
    }

    @Override
    @NonTransactional
    public CampaignStat getCampaignGeneralTotalStats(long campaignId, Date from, Date to) {
        return campaignStatDAO.selectGeneralTotalStats(campaignId, from, to);
    }

    @NonTransactional
    @Override
    public List<CampaignStat> getCampaignRefererStats(long campaignId, Date from, Date to, int offset, int limit) {
        return campaignStatDAO.selectRefererStats(campaignId, from, to, offset, limit);
    }

    @Override
    @NonTransactional
    public List<CampaignStat> getCampaignCountryStats(long campaignId, Date from, Date to, int offset, int limit) {
        return campaignStatDAO.selectCountryStats(campaignId, from, to, offset, limit);
    }

    @Override
    @NonTransactional
    public List<CampaignStat> getCampaignCityStats(long campaignId, Date from, Date to, int offset, int limit) {
        return campaignStatDAO.selectCityStats(campaignId, from, to, offset, limit);
    }


    @PreDestroy
    private void shutdown() {
        executor.shutdown();
    }

}

package ru.planeta.api.service.concert;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.planeta.api.exceptions.NotFoundException;
import ru.planeta.dao.concertdb.ConcertDAO;
import ru.planeta.model.concert.Concert;
import ru.planeta.model.concert.ConcertStatus;

import java.util.Date;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: michail
 * Date: 14.10.16
 * Time: 13:50
 */

@Service
public class ConcertServiceImpl implements ConcertService {
    @Autowired
    private ConcertDAO concertDAO;

    @Override
    public long getConcertsCount() {
        return concertDAO.getConcertsCount();
    }

    private int insertConcert(Concert concert) {
        concert.setTimeAdded(new Date());
        concert.setTimeUpdated(concert.getTimeAdded());
        if (concert.getStatus() == null) {
            concert.setStatus(ConcertStatus.PAUSED);
        }
        return concertDAO.insert(concert);
    }

    private int updateConcert(Concert concert) {
        concert.setTimeUpdated(new Date());
        return concertDAO.update(concert);
    }

    @Override
    public int insertOrUpdateConcert(Concert concert) {
        return concert.getConcertId() > 0 ? updateConcert(concert) : insertConcert(concert);
    }


    @Override
    public Concert getConcertSafe(long concertId) throws NotFoundException {
        Concert concert = concertDAO.select(concertId);
        if (concert == null) {
            throw new NotFoundException(Concert.class, concertId);
        }
        return concert;
    }

    @Override
    public Concert getConcertByExternalId(long externalConcertId) throws NotFoundException {
        return concertDAO.selectByExtId(externalConcertId);
    }


    @Override
    public List<Concert> selectList(int offset, int limit) throws NotFoundException {
        return concertDAO.selectList(offset, limit);
    }

    @Override
    public List<Concert> getActiveConcerts(int offset, int limit) throws NotFoundException {
        return concertDAO.selectActive(offset, limit);
    }


}


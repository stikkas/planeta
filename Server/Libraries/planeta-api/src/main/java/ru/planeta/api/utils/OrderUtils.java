package ru.planeta.api.utils;

import ru.planeta.api.Utils;
import ru.planeta.model.common.Order;
import ru.planeta.model.common.OrderObject;
import ru.planeta.model.common.OrderObjectInfo;

import javax.annotation.Nullable;
import java.math.BigDecimal;
import java.util.*;

import static ru.planeta.api.Utils.empty;

/**
 * Contains utility order processing methods
 *
 * @author Andrew.Arefyev@gmail.com
 *         16.05.13 17:07
 */
public final class OrderUtils {

    /**
     * Groups order objects by object identifier.
     *
     * @param orderObjects {@link OrderObjectInfo} items list;
     * @param comparator   result sorting comparator, if <code>null</code> then sorting will be ignored;
     * @return map of grouped order objects, where key is object identifier, and value is list of order object with same object identifier.
     */
    public static Map<Long, List<OrderObjectInfo>> groupOrderObjectsByObjectId(Collection<OrderObjectInfo> orderObjects, @Nullable Comparator<OrderObjectInfo> comparator) {
        Map<Long, List<OrderObjectInfo>> result = new HashMap<>();
        for (OrderObjectInfo orderObject : orderObjects) {
            List<OrderObjectInfo> orderObjectsGroup = result.get(orderObject.getObjectId());
            if (orderObjectsGroup == null) {
                orderObjectsGroup = new ArrayList<>();
                result.put(orderObject.getObjectId(), orderObjectsGroup);
            }
            orderObjectsGroup.add(orderObject);
        }

        if (comparator != null) {
            for (List<OrderObjectInfo> objects : result.values()) {
                Collections.sort(objects, comparator);
            }
        }

        return result;
    }

    /**
     * Joins same type (equals objectId) order objects by increasing <code>count</code> field.<br>
     * Duplicated objects (equals objectId) removes from result list.
     *
     * @return list of joined order objects.
     */
    public static List<OrderObjectInfo> joinOrderObjectsByObjectId(Collection<OrderObjectInfo> orderObjects) {
        if (Utils.empty(orderObjects)) {
            return Collections.emptyList();
        }
        List<OrderObjectInfo> result = new ArrayList<>();

        Map<Long, List<OrderObjectInfo>> groupedOrderObjectsByObjectId = groupOrderObjectsByObjectId(orderObjects, null);
        for (List<OrderObjectInfo> groupedOrderObjects : groupedOrderObjectsByObjectId.values()) {
            OrderObjectInfo orderObject = groupedOrderObjects.iterator().next();
            orderObject.setCount(groupedOrderObjects.size());

            result.add(orderObject);
        }

        return result;
    }

    /**
     * Indicates that specified order is gift.
     *
     * @param order - checked order;
     * @return <code>true</code> if order is gift, <code>false</code> otherwise.
     */
    public static boolean isGiftOrder(Order order) {
        return (order.getTotalPrice().compareTo(BigDecimal.ZERO) == 0);
    }

    public static BigDecimal calculateOrderObjectsPrice(Collection<OrderObject> orderObjects) {
        BigDecimal result = BigDecimal.ZERO;
        for (OrderObject orderObject : orderObjects) {
            result = result.add(orderObject.getPrice());
        }

        return result;
    }
}

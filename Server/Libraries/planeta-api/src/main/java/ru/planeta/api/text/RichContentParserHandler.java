package ru.planeta.api.text;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.log4j.Logger;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;
import org.xml.sax.helpers.DefaultHandler;
import ru.planeta.api.model.enums.image.ImageType;
import ru.planeta.api.service.configurations.StaticNodesService;
import ru.planeta.model.enums.ThumbnailType;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.apache.commons.lang3.StringUtils.isEmpty;
import static ru.planeta.api.text.RichMediaObject.*;

/**
 * Created with IntelliJ IDEA.
 * User: Александр
 * Date: 25.11.12
 * Time: 13:00
 */
public class RichContentParserHandler extends DefaultHandler {
    private static final Logger log = Logger.getLogger(RichContentParserHandler.class);

    private enum State {
        DEFAULT, LINK, IMAGE, LINE_BREAK
    }

    private int level = 0;
    private State state = State.DEFAULT;
    private Message message;
    private StaticNodesService staticNodesService;

    private StringBuilder text;
    private List<ImageAttachment> images;
    private List<VideoAttachment> videos;
    private List<AudioAttachment> audios;

    private Map<String, Object> currentData = new HashMap<>();


    public RichContentParserHandler(Message message, StaticNodesService staticNodesService) {
        this.message = message;
        this.staticNodesService = staticNodesService;
    }

    @Override
    public void startDocument() throws SAXException {
        text = new StringBuilder();
        images = new ArrayList<>();
        videos = new ArrayList<>();
        audios = new ArrayList<>();
    }

    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        if (length == 0) {
            return;
        }
        String safeText = StringEscapeUtils.escapeHtml4(new String(ch, start, length));
        switch (state) {
            case DEFAULT:
                text.append(safeText);
                break;
            case LINK:
                String value = (String) currentData.get("value");
                currentData.put("value", value == null ? safeText : value + safeText);
                break;
        }
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {

        switch (qName) {
            case "br":
                level = 0;
                state = State.LINE_BREAK;
                break;
            case "a":
                level = 0;
                state = State.LINK;
                currentData.clear();
                currentData.put("href", attributes.getValue("href"));
                break;
            case "img":
                if (state == State.LINK) {
                    ImageAttachment image = new ImageAttachment();
                    image.setObjectId(0);
                    image.setOwnerId(0);
                    image.setUrl((String) currentData.get("href"));
                    images.add(image);
                    state = State.DEFAULT;
                }
                break;
            default:
                try {
                    if (qName.equals(Type.PHOTO.getValue())) {
                        images.add(createImageAttachment(attributes));
                    } else if (qName.equals(Type.VIDEO.getValue())) {
                        videos.add(createVideoAttachment(attributes));
                    } else if (qName.equals(Type.AUDIO.getValue())) {
                        audios.add(createAudioAttachment(attributes));
                    }
                } catch (IllegalArgumentException e) {
                    log.error("invalid custom tag", e);
                }
                break;
        }
        level++;
    }


    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        level--;
        if (level == 0) {
            switch (state) {
                case LINK:
                    text.append("<a href=\"").append(currentData.get("href")).append("\">");
                    text.append(currentData.get("value")).append("</a>");
                    break;
                case LINE_BREAK:
                    text.append('\n');
                    break;
            }
        }

        if (level == 0) {
            state = State.DEFAULT;
        }
    }

    @Override
    public void warning(SAXParseException e) throws SAXException {
        log.warn(e);
    }

    @Override
    public void endDocument() throws SAXException {
        message.setText(trimWhitespaces(text));
        message.setAudioAttachments(audios);
        message.setImageAttachments(images);
        message.setVideoAttachments(videos);
    }

    @Override
    public void error(SAXParseException e) throws SAXException {
        log.error("Error occured", e);
    }

    @Override
    public void fatalError(SAXParseException e) throws SAXException {
        log.error("Error occured", e);
    }

    private ImageAttachment createImageAttachment(Attributes attributes) {
        String id = attributes.getValue("id");
        String owner = attributes.getValue("owner");
        String image = attributes.getValue("image");
        if (isEmpty(id)) {
            throw new IllegalArgumentException("id attribute is empty");
        }
        if (isEmpty(owner)) {
            throw new IllegalArgumentException("owner attribute is empty");
        }
        if (isEmpty(image)) {
            throw new IllegalArgumentException("image attribute is empty");
        }
        ImageAttachment attachment = new ImageAttachment();
        attachment.setObjectId(NumberUtils.createLong(id));
        attachment.setOwnerId(NumberUtils.createLong(owner));
        attachment.setUrl(image);
        attachment.setThumbnailUrl(staticNodesService.getThumbnailUrl(attachment.getUrl(), ThumbnailType.HUGE, ImageType.PHOTO));
        return attachment;
    }

    private AudioAttachment createAudioAttachment(Attributes attributes) {
        String id = attributes.getValue("id");
        String owner = attributes.getValue("owner");
        String name = attributes.getValue("name");
        String artist = attributes.getValue("artist");
        String duration = attributes.getValue("duration");

        if (isEmpty(id)) {
            throw new IllegalArgumentException("id attribute is empty");
        }
        if (isEmpty(owner)) {
            throw new IllegalArgumentException("owner attribute is empty");
        }
        if (isEmpty(name)) {
            throw new IllegalArgumentException("name attribute is empty");
        }
        if (isEmpty(artist)) {
            throw new IllegalArgumentException("artist attribute is empty");
        }
        if (isEmpty(duration)) {
            throw new IllegalArgumentException("duration attribute is empty");
        }
        AudioAttachment audio = new AudioAttachment();
        audio.setObjectId(NumberUtils.createLong(id));
        audio.setOwnerId(NumberUtils.createLong(owner));
        audio.setName(name);
        audio.setArtist(artist);
        audio.setDuration(NumberUtils.toInt(duration));
        return audio;
    }

    private static VideoAttachment createVideoAttachment(Attributes attributes) {
        String id = attributes.getValue("id");
        String owner = attributes.getValue("owner");
        String name = attributes.getValue("name");
        String url = attributes.getValue("url");
        String image = attributes.getValue("image");
        String artist = attributes.getValue("artist");
        String duration = attributes.getValue("duration");

        if (isEmpty(id)) {
            throw new IllegalArgumentException("id attribute is empty");
        }
        if (isEmpty(owner)) {
            throw new IllegalArgumentException("owner attribute is empty");
        }
        if (isEmpty(name)) {
            throw new IllegalArgumentException("name attribute is empty");
        }
        if (isEmpty(url)) {
            throw new IllegalArgumentException("url attribute is empty");
        }
        if (isEmpty(image)) {
            throw new IllegalArgumentException("image attribute is empty");
        }
        if (isEmpty(artist)) {
            throw new IllegalArgumentException("artist attribute is empty");
        }
        if (isEmpty(duration)) {
            throw new IllegalArgumentException("duration attribute is empty");
        }
        VideoAttachment video = new VideoAttachment();
        video.setObjectId(NumberUtils.createLong(id));
        video.setOwnerId(NumberUtils.createLong(owner));
        video.setName(name);
        video.setUrl(url);
        video.setImageUrl(image);
        video.setDuration(NumberUtils.toInt(duration));
        return video;
    }


    private static String trimWhitespaces(final StringBuilder text) {
        if (StringUtils.isEmpty(text)) {
            return StringUtils.EMPTY;
        }
        StringBuilder builder = new StringBuilder(text.length());
        int state = 0;
        for (int i = 0; i < text.length(); ++i) {
            char c = text.charAt(i);
            switch (state) {
                case 0:
                    if (c != ' ') {
                        state = 1;
                        builder.append(c);
                    }
                    break;
                case 1:
                    if (c == ' ') {
                        builder.append(' ');
                        state = 0;
                    } else {
                        builder.append(c);
                    }
                    break;
            }
        }
        return builder.toString();
    }
}

package ru.planeta.api.utils;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Cell;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class DocumentUtils {
    public static String getCellValueString(Cell cell) {
        if (cell == null) {
            return "";
        } else {
            cell.setCellType(Cell.CELL_TYPE_STRING);
            return cell.getStringCellValue();
        }
    }

    public static int getCellValueInt(Cell cell) {
        String strValue = getCellValueString(cell).replaceAll("[\\D]", "");
        return StringUtils.isNotBlank(strValue) ? Integer.parseInt(strValue) : 0;
    }

    public static BigDecimal getCellValueBigDecimal(Cell cell, int newScale) {
        if (cell == null) {
            return BigDecimal.ZERO;
        } else {
            cell.setCellType(Cell.CELL_TYPE_NUMERIC);
            return BigDecimal.valueOf(cell.getNumericCellValue()).setScale(newScale, RoundingMode.HALF_UP);
        }
    }
}

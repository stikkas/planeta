package ru.planeta.api.service.widget;

import ru.planeta.api.exceptions.NotFoundException;
import ru.planeta.api.exceptions.WidgetNotActiveException;
import ru.planeta.api.model.widget.AbstractWidgetDTO;
import ru.planeta.api.model.widget.WidgetColorTheme;
import ru.planeta.api.model.enums.widget.WidgetNames;

/**
 * Created with IntelliJ IDEA.
 * Date: 08.08.13
 * Time: 12:58
 */
public interface WidgetService {
    AbstractWidgetDTO getCampaignWidgetDTO(WidgetNames name, long offerId, long shareId, WidgetColorTheme colorTheme) throws NotFoundException, WidgetNotActiveException;
}

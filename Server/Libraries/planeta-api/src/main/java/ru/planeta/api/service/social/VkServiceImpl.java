package ru.planeta.api.service.social;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import ru.planeta.api.aspect.transaction.NonTransactional;
import ru.planeta.api.model.ExternalUserInfo;
import ru.planeta.commons.web.VkRegistration;
import ru.planeta.commons.web.social.vk.VkApiClient;
import ru.planeta.commons.web.social.vk.VkOAuthClient;
import ru.planeta.model.common.auth.CredentialType;
import ru.planeta.model.common.auth.ExternalAuthentication;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

@Service
public class VkServiceImpl implements OAuthClientService {

    private String oauthAppId;

    @Value("${oauth.vk.appId}")
    public void setOAuthAppId(String oauthAppId) {
        this.oauthAppId = oauthAppId;
    }

    private String oauthSecret;

    @Value("${oauth.vk.secret}")
    public void setOAuthSecret(String oauthSecret) {
        this.oauthSecret = oauthSecret;
    }


    @Override
    @Nullable
    public String getRedirectUrl(String saveRedirectUrl) {
        try {
            return "https://oauth.vk.com/authorize?client_id=" + oauthAppId + "&scope=&redirect_uri="
                    + URLEncoder.encode(saveRedirectUrl, "UTF-8") + "&response_type=code" + "&v=" + VkApiClient.API_VERSION;
        } catch (UnsupportedEncodingException e) {
            return null;
        }
    }

    // may be long
    @Nullable
    @Override
    @NonTransactional
    public ExternalUserInfo authorizeExternal(String code, String redirectUrl) throws IOException {
        VkOAuthClient vkOAuthClient = new VkOAuthClient(oauthAppId, oauthSecret, code, redirectUrl);
        return getExternalUserInfo(vkOAuthClient.authorize());
    }

    @Override
    public boolean isMyUrl(@Nonnull String url) {
        return url.contains("vk");
    }


    private static ExternalUserInfo getExternalUserInfo(@Nullable VkRegistration vkRegistration) {
        if (vkRegistration == null) {
            return null;
        }
        ExternalUserInfo externalUserInfo = new ExternalUserInfo();
        externalUserInfo.setRegistrationData(vkRegistration.getRegistrationData());
        ExternalAuthentication externalAuthentication = new ExternalAuthentication(vkRegistration.getUsername(), CredentialType.VK);
        externalUserInfo.setExternalAuthentication(externalAuthentication);
        return externalUserInfo;
    }

}

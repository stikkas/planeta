package ru.planeta.api.service.billing.payment.system.yamoney.mws.models.enums;

import javax.xml.bind.annotation.XmlEnumValue;

/**
 * Currency codes constants.<br>
 * Created by eshevchenko.
 */
public enum CurrencyCode {
    @XmlEnumValue(value = "643")
    RUBLE,
    @XmlEnumValue(value = "10643")
    DEMO_RUBLE;
}

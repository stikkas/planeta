package ru.planeta.api.service.registration;

import org.apache.commons.lang3.StringUtils;
import ru.planeta.api.geo.GeoResolverWebService;
import ru.planeta.commons.web.RegistrationData;
import ru.planeta.model.common.UserPrivateInfo;
import ru.planeta.model.profile.Profile;
import ru.planeta.model.stat.RequestStat;

public class EmailRegistrationPopulateStrategy extends RegistrationDataPopulateStrategy {
    private final String email;

    EmailRegistrationPopulateStrategy(String email, RegistrationData registrationData, GeoResolverWebService geoResolverWebService, RequestStat requestStat) {
        super(registrationData, geoResolverWebService, requestStat);
        this.email = email;
    }

    @Override
    public void populateProfile(Profile profile) {
        super.populateProfile(profile);
        if (StringUtils.isBlank(profile.getDisplayName())) {
            String name = email.substring(0, email.indexOf("@"));
            profile.setDisplayName(StringUtils.stripToEmpty(name));
        }
    }

    @Override
    public void populatePrivateInfo(UserPrivateInfo userPrivateInfo) {
        userPrivateInfo.setEmail(email);
    }
}

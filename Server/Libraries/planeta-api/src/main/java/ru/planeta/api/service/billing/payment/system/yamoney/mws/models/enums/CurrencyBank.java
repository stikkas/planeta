package ru.planeta.api.service.billing.payment.system.yamoney.mws.models.enums;

import javax.xml.bind.annotation.XmlEnumValue;

/**
 * Banks constants.<br>
 * Created by eshevchenko.
 */
public enum CurrencyBank {
    @XmlEnumValue(value = "1001")
    ECOMBANK,
    @XmlEnumValue(value = "1003")
    DEMO_BANK;
}

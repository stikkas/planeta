package ru.planeta.api.service.billing.payment.system.yamoney.mws;

import ru.planeta.api.service.billing.payment.system.yamoney.mws.exceptions.InitializationException;
import ru.planeta.api.service.billing.payment.system.yamoney.mws.utils.SecurityUtils;
import org.apache.commons.net.util.SSLContextUtils;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.springframework.core.io.Resource;

import javax.net.ssl.KeyManager;
import javax.net.ssl.TrustManager;
import java.io.IOException;
import java.security.KeyStore;
import java.security.PrivateKey;
import java.security.cert.X509Certificate;

/**
 * Stores YandexMoney account information necessary for operation execution.<br>
 * Created by eshevchenko.
 */
public class MwsAccount {

    private final Long shopId;
    private final X509Certificate certificate;
    private final PrivateKey privateKey;
    private final SSLConnectionSocketFactory sslConnectionSocketFactory;

    public MwsAccount(Long shopId, Resource clientKeyStoreFile, String clientKeyStorePassword, KeyStore trustStore) throws InitializationException {
        try {
            char[] passwordAsChars = clientKeyStorePassword.toCharArray();
            KeyStore clientKeyStore = SecurityUtils.loadKeyStore("PKCS12", clientKeyStoreFile.getInputStream(), passwordAsChars);
            String alias = SecurityUtils.findAlias(clientKeyStore);

            this.shopId = shopId;
            this.certificate = (X509Certificate) clientKeyStore.getCertificate(alias);
            this.privateKey = (PrivateKey) clientKeyStore.getKey(alias, passwordAsChars);
            this.sslConnectionSocketFactory = createSSLConnectionSocketFactory(trustStore, clientKeyStore, clientKeyStorePassword);
        } catch (Exception e) {
            throw new InitializationException(e);
        }
    }

    public Long getShopId() {
        return shopId;
    }

    public X509Certificate getCertificate() {
        return certificate;
    }

    public PrivateKey getPrivateKey() {
        return privateKey;
    }

    public SSLConnectionSocketFactory getSslConnectionSocketFactory() {
        return sslConnectionSocketFactory;
    }

    @Override
    public String toString() {
        return new StringBuilder()
                .append("shopId[").append(shopId).append("] ")
                .append("certLoaded[").append(certificate != null).append("] ")
                .append("keyLoaded[").append(privateKey != null).append("] ")
                .toString();
    }

    private static SSLConnectionSocketFactory createSSLConnectionSocketFactory(KeyStore trustStore, KeyStore clientKeyStore, String clientKeyStorePassword) throws IOException {
        TrustManager[] trustManager = SecurityUtils.createTrustManager("PKIX", trustStore);
        KeyManager[] keyManager = SecurityUtils.createKeyManager("SunX509", clientKeyStore, clientKeyStorePassword.toCharArray());

        return new SSLConnectionSocketFactory(
                SSLContextUtils.createSSLContext("TLS", keyManager, trustManager),
                new String[]{"TLSv1"},
                null,
                SSLConnectionSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
    }
}

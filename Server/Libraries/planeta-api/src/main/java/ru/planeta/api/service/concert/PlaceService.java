package ru.planeta.api.service.concert;

import ru.planeta.api.exceptions.MoscowShowInteractionException;
import ru.planeta.api.exceptions.NotFoundException;
import ru.planeta.model.concert.Place;
import ru.planeta.moscowshow.model.OrderItem;
import ru.planeta.moscowshow.model.response.ResponsePlaceOrder;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: michail
 * Date: 17.10.16
 * Time: 12:08
 */
public interface PlaceService {
    Place getPlaceByExtId(long externalPlaceId);

    Place getPlaceBySectionRowPosition(long sectionId, long rowId, long position);

    int updatePlace(Place place);

    int insertPlace(Place place);

    int insertOrUpdatePlace(Place place);

    List<Place> prepareMoscowShowOrder(List<OrderItem> places) throws NotFoundException, MoscowShowInteractionException;

    ResponsePlaceOrder hold(List<OrderItem> orderItemList) throws NotFoundException, MoscowShowInteractionException;

    List<Place> getPlacesByExternalConcertId(long extConcertId);

    List<Place> getPlacesByExternalConcertId(long extConcertId, Long extSectionId, Long extRowId);

    int markPlaceDeleted(Place place);

    List<Place> selectReservedPlaces(int offset, int limit);
}

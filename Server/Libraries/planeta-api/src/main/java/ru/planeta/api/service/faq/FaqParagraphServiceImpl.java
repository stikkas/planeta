package ru.planeta.api.service.faq;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.stereotype.Service;
import ru.planeta.dao.commondb.BaseFaqParagraphService;
import ru.planeta.model.commondb.FaqParagraph;
import ru.planeta.model.commondb.FaqParagraphExample;

import java.util.List;

/**
 * Created by kostiagn on 01.09.2015.
 */
@Service
public class FaqParagraphServiceImpl extends BaseFaqParagraphService implements FaqParagraphService {

    @Override
    public void delete(Long faqArticleId) {
        FaqParagraphExample faqParagraphExample = new FaqParagraphExample();
        faqParagraphExample.or().andFaqArticleIdEqualTo(faqArticleId);
        getFaqParagraphMapper().deleteByExample(faqParagraphExample);
    }

    @Override
    public void insertFaqParagraph(FaqParagraph faqParagraph) {
        if (faqParagraph.getOrderNum() == null) {
            FaqParagraphExample faqParagraphExample = new FaqParagraphExample();
            faqParagraphExample.or().andFaqArticleIdEqualTo(faqParagraph.getFaqArticleId());
            faqParagraphExample.setOrderByClause("order_num desc");
            faqParagraphExample.setOffset(0);
            faqParagraphExample.setLimit(1);
            final List<FaqParagraph> list = getFaqParagraphMapper().selectByExample(faqParagraphExample);

            faqParagraph.setOrderNum(CollectionUtils.isEmpty(list) ? 1 : list.get(0).getOrderNum() + 1);
        }
        super.insertFaqParagraph(faqParagraph);
    }


    @Override
    public void resort(long idFrom, long idTo, long faqArticleId) {


        int orderNumFrom = -1;
        int orderNumTo = -1;

        final List<FaqParagraph> faqParagraphs = selectFaqParagraphListByFaqArticle(faqArticleId, 0, 0);
        for (FaqParagraph faqParagraph : faqParagraphs) {
            if (faqParagraph.getFaqParagraphId() == idFrom) {
                orderNumFrom = faqParagraph.getOrderNum();
            }
            if (faqParagraph.getFaqParagraphId() == idTo) {
                orderNumTo = faqParagraph.getOrderNum();
            }
        }

        if (orderNumFrom == orderNumTo || orderNumFrom < 0 || orderNumTo < 0) {
            return;
        }
        if (orderNumFrom < orderNumTo) {
            for (FaqParagraph faqParagraph : faqParagraphs) {
                if (faqParagraph.getOrderNum() == orderNumFrom) {
                    faqParagraph.setOrderNum(orderNumTo);
                    getFaqParagraphMapper().updateByPrimaryKey(faqParagraph);
                } else if (faqParagraph.getOrderNum() > orderNumFrom && faqParagraph.getOrderNum() <= orderNumTo) {
                    faqParagraph.setOrderNum(faqParagraph.getOrderNum() - 1);
                    getFaqParagraphMapper().updateByPrimaryKey(faqParagraph);
                }
            }
        } else {
            for (FaqParagraph faqParagraph : faqParagraphs) {
                if (faqParagraph.getOrderNum() == orderNumFrom) {
                    faqParagraph.setOrderNum(orderNumTo);
                    getFaqParagraphMapper().updateByPrimaryKey(faqParagraph);
                } else if (faqParagraph.getOrderNum() >= orderNumTo && faqParagraph.getOrderNum() < orderNumFrom) {
                    faqParagraph.setOrderNum(faqParagraph.getOrderNum() + 1);
                    getFaqParagraphMapper().updateByPrimaryKey(faqParagraph);
                }
            }
        }
    }
}

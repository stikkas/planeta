package ru.planeta.api.model.authentication;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import ru.planeta.api.model.UserAuthorizationInfo;
import ru.planeta.model.enums.UserStatus;

import java.util.Collection;
import java.util.EnumSet;

/**
 * User: atropnikov
 * Date: 21.03.12
 * Time: 17:18
 */
public class AuthenticatedUserDetails implements UserDetails {

    private final UserAuthorizationInfo userAuthorizationInfo;

    public AuthenticatedUserDetails(UserAuthorizationInfo userAuthorizationInfo) {
        this.userAuthorizationInfo = userAuthorizationInfo;
    }

    @Override
    public Collection<GrantedAuthority> getAuthorities() {
        EnumSet<UserStatus> userStatuses = userAuthorizationInfo.getUserStatus();
        return Authority.getUserAuthorities(userStatuses);
    }


    @Override
    public String getPassword() {
        return userAuthorizationInfo.getPassword();
    }

    @Override
    public String getUsername() {
        return userAuthorizationInfo.getUsername();
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        EnumSet<UserStatus> statuses = userAuthorizationInfo.getUserStatus();
        return statuses != null && !statuses.contains(UserStatus.BLOCKED) && !statuses.contains(UserStatus.DELETED);
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    public UserAuthorizationInfo getUserAuthorizationInfo() {
        return userAuthorizationInfo;
    }
}

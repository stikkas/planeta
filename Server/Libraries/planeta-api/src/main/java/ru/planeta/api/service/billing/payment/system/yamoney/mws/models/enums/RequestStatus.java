package ru.planeta.api.service.billing.payment.system.yamoney.mws.models.enums;

import javax.xml.bind.annotation.XmlEnumValue;

/**
 * Yandex.Money MWS (Merchant Web Service) request statuses constants.
 */
public enum RequestStatus {
    @XmlEnumValue(value = "0")
    SUCCESS,
    @XmlEnumValue(value = "1")
    IN_PROCESS,
    @XmlEnumValue(value = "3")
    REJECTED;
}

package ru.planeta.api.text;

import org.apache.commons.lang3.StringEscapeUtils;
import org.ccil.cowan.tagsoup.Parser;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.regex.Pattern;

import static ru.planeta.api.text.RichMediaObject.*;

/**
 * Parser implementation that is used for html validation
 *
 * @author m.shulepov
 */
public class HtmlValidatingParser extends Parser {

    private static final Set<String> VALID_TAGS = new HashSet<>(Arrays.asList(
            "html", "body", "head",
            "div", "p", "br", "a", "span", "img", "li", "ul", "ol",
            "h1", "h2", "h3", "h4", "h5", "pre", "address",
            "strong", "b", "i", "u", "em", "blockquote"));

    protected static final Set<String> CUSTOM_TAGS = new HashSet<>(Arrays.asList(
        Type.PHOTO.getValue(), Type.VIDEO.getValue(), Type.AUDIO.getValue()
    ));

    private static final Set<String> IGNORED_TAGS = new HashSet<>(Arrays.asList("html", "head"));

    private static final Set<String> IGNORED_ATTRIBUTES = new HashSet<>(Arrays.asList("shape", "clear"));

    private static final Set<String> RESTRICTED_ATTRIBUTES = new HashSet<>(Arrays.asList(
            "onblur", "onchange", "onclick", "dblclick", "onfocus", "onkeydown", "onkeypress",
            "onkeyup", "onload", "onmousedown", "onmousemove", "onmouseout", "onmouseover", "onmouseup",
            "onreset", "onselect", "onsubmit", "onunload", "onerror", "onabort", "style",
            "align"));

    private static final Pattern JAVASCRIPT_REGEXP = Pattern.compile("(javascript|vbscript):");

    private ErrorMessageHandler errorMessageHandler;
    private StringBuilder contentBuilder = new StringBuilder();
    private boolean cleanHtml;
    private boolean appendEnabled;

    public HtmlValidatingParser(boolean cleanHtml) {
        super();
        this.cleanHtml = cleanHtml;
        errorMessageHandler = new ErrorMessageHandler(cleanHtml);
    }

    public String getHtml() {
        return contentBuilder.toString().replaceAll("</?body[^>]*>", "");
    }

    @Override
    public void startElement(String uri, String localName, String element, Attributes attributes) throws SAXException {
        String tagName = element.toLowerCase();
        setAppendEnabled(tagName);

        if (!VALID_TAGS.contains(tagName) && !CUSTOM_TAGS.contains(tagName)) {
            errorMessageHandler.handleErrorMessage("Invalid open tag: " + tagName);
            return;
        }

        if (CUSTOM_TAGS.contains(tagName)) {
            if (attributes.getValue("id") == null) {
                errorMessageHandler.handleErrorMessage(tagName + " without Id attribute");
                return;
            }
        }

        if (appendEnabled) {
            contentBuilder.append("<").append(tagName);
        }

        for (int i = 0; i < attributes.getLength(); i++) {
            handleAttribute(attributes.getQName(i), attributes.getValue(i));
        }

        if (appendEnabled) {
            contentBuilder.append(">");
        }
    }

    private void setAppendEnabled(String tagName) {
        this.appendEnabled = this.cleanHtml && !IGNORED_TAGS.contains(tagName);
    }

    private void handleAttribute(String name, String value) throws SAXException {
        if (RESTRICTED_ATTRIBUTES.contains(name.toLowerCase())) {
            errorMessageHandler.handleErrorMessage("Invalid attribute: " + name);
            return;
        }
        if (JAVASCRIPT_REGEXP.matcher(value.toLowerCase()).find()) {
            errorMessageHandler.handleErrorMessage("Invalid attribute value: " + value);
            return;
        }

        // ignore attribute with name "shape", it's added by parses and contains housekeeping info
        if (appendEnabled && !IGNORED_ATTRIBUTES.contains(name.toLowerCase())) {
            contentBuilder.append(" ").append(name).append("=").append("\"").append(StringEscapeUtils.escapeHtml4(value)).append("\"");
        }
    }

    @Override
    public void endElement(String uri, String localName, String element) throws SAXException {
        String tagName = element.toLowerCase();
        setAppendEnabled(tagName);

        if (!VALID_TAGS.contains(tagName) && !CUSTOM_TAGS.contains(tagName)) {
            errorMessageHandler.handleErrorMessage("Invalid closing tag: " + element);
            return;
        }

        if (appendEnabled) {
            contentBuilder.append("</").append(tagName).append(">");
        }
    }

    @Override
    public void startCDATA() throws SAXException {
        errorMessageHandler.handleErrorMessage("CDATA not allowed");
    }

    @Override
    public void endCDATA() throws SAXException {
        errorMessageHandler.handleErrorMessage("CDATA not allowed");
    }

    @Override
    public void comment(char[] ch, int start, int length) throws SAXException {
        errorMessageHandler.handleErrorMessage("Comments not allowed");
    }

    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        if (appendEnabled) {
            contentBuilder.append(StringEscapeUtils.escapeHtml4(new String(ch, start, length)));
        }
    }

    /**
     * Helper class for handling error messages
     * If <code>ignoreError=true</code> it's no op, otherwise it throws {@link SAXException}
     */
    private static class ErrorMessageHandler {

        private boolean ignoreError;

        ErrorMessageHandler(boolean ignoreError) {
            this.ignoreError = ignoreError;
        }

        void handleErrorMessage(String message) throws SAXException {
            if (ignoreError) {
                return;
            }
            throw new SAXException(message);
        }
    }


}

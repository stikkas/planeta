package ru.planeta.api.model.enums.widget;

/**
 * Created with IntelliJ IDEA.
 * Date: 07.08.13
 * Time: 18:05
 */
public enum WidgetNames {
    CAMPAIGN_240X400_WITH_SHARE(240, 400),
    CAMPAIGN_300X250(300, 250),
    CAMPAIGN_240X240(240, 240),
    CAMPAIGN_192X240(192, 240),
    CAMPAIGN_200X200(200, 200),
    CAMPAIGN_728X90(728, 90),
    CAMPAIGN_600X120(600, 120),
    CAMPAIGN_600X120_WITH_MONEY_TARGET(600, 120),
    CAMPAIGN_468X80(468, 80);

    private int width;
    private int height;

    WidgetNames(int width, int height) {
        this.width = width;
        this.height = height;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    @Override
    public String toString() {
        return super.toString().toLowerCase();
    }
}

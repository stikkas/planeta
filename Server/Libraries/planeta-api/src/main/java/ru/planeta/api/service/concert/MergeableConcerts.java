package ru.planeta.api.service.concert;

import org.apache.commons.lang3.StringUtils;
import ru.planeta.api.exceptions.MoscowShowInteractionException;
import ru.planeta.api.exceptions.NotFoundException;
import ru.planeta.model.concert.Concert;
import ru.planeta.model.concert.ConcertStatus;
import ru.planeta.model.concert.Hall;
import ru.planeta.moscowshow.model.ActionStructure;

import javax.annotation.Nonnull;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: michail
 * Date: 31.10.2016
 * Time: 13:34
 */
public class MergeableConcerts implements Mergeable<Concert, ActionStructure> {

    private ConcertService concertService;
    private ConcertImportService concertImportService;
    private HallService hallService;

    private List<Concert> planetaList;
    private List<ActionStructure> msList;

    @Override
    public int compare(Concert concert, ActionStructure actionStructure) {
        return Long.compare(concert.getExternalConcertId(), actionStructure.getId());
    }

    @Override
    public void insert(ActionStructure actionStructure) throws NotFoundException, MoscowShowInteractionException {
        Concert newConcert = getConcert(actionStructure);
        importConcert(newConcert, actionStructure);
    }

    @Override
    public void update(Concert concert, ActionStructure actionStructure) throws NotFoundException, MoscowShowInteractionException {
        concert = updateConcert(actionStructure, concert);
        importConcert(concert, actionStructure);
    }

    private void importConcert(Concert concert, ActionStructure actionStructure) throws NotFoundException, MoscowShowInteractionException {
        concertService.insertOrUpdateConcert(concert);
        concertImportService.importMoscowShowConcertScheme(concert);
        importHall(concert, actionStructure);
    }

    @Override
    public void delete(Concert concert) {
        concert.setStatus(ConcertStatus.PAUSED);
        concertService.insertOrUpdateConcert(concert);
    }

    @Override
    public List<Concert> getInternalList() {
        return planetaList;
    }

    @Override
    public List<ActionStructure> getExternalList() {
        return msList;
    }

    public MergeableConcerts(List<Concert> planetaList, List<ActionStructure> msList, ConcertService concertService, HallService hallService, ConcertImportService concertImportService) {
        this.planetaList = planetaList;
        this.msList = msList;
        this.concertService = concertService;
        this.hallService = hallService;
        this.concertImportService = concertImportService;
    }

    private static Concert updateConcert(@Nonnull ActionStructure actionStructure, @Nonnull Concert concert) {
        concert.setExternalConcertId(actionStructure.getId());
        concert.setTitle(actionStructure.getName());
        concert.setImageUrl(actionStructure.getRawAction().getImageUrl());
        concert.setConcertDate(actionStructure.getActionDate());
        return concert;
    }

    private static Concert getConcert(@Nonnull ActionStructure actionStructure) {
        return updateConcert(actionStructure, new Concert());
    }

    private void importHall(@Nonnull Concert planetaConcert, @Nonnull ActionStructure msConcert) {
        Hall hall = null;

        if (StringUtils.isNotBlank(msConcert.getHall())) {
            hall = hallService.getHallByTitle(msConcert.getHall());
        }

        if (hall == null) {
            hall = getHall(msConcert);
        } else {
            hall = updateHall(msConcert, hall);
        }
        hallService.insertOrUpdateHall(hall);

        planetaConcert.setHallId(hall.getHallId());
        concertService.insertOrUpdateConcert(planetaConcert);
    }

    private static Hall updateHall(@Nonnull ActionStructure actionStructure, @Nonnull Hall hall) {
        hall.setTitle(actionStructure.getHall());
        hall.setAddress(actionStructure.getLocation());
        return hall;
    }

    private static Hall getHall(@Nonnull ActionStructure actionStructure) {
        return updateHall(actionStructure, new Hall());
    }
}

package ru.planeta.api.service.charity;

import org.springframework.stereotype.Service;
import ru.planeta.model.charity.LibraryFile;
import ru.planeta.model.charity.LibraryFileExample;

import java.util.List;

@Service
public class CharityLibraryFileServiceImpl extends BaseLibraryFileService implements CharityLibraryFileService {

    @Override
    public List<LibraryFile> selectLibraryFileByHeaderList(String headerPart, int offset, int limit) {
        LibraryFileExample example = new LibraryFileExample();
        example.setOffset(offset);
        example.setLimit(limit);
        example.or()
            .andHeaderILike("%" + headerPart + "%")
        ;
        example.setOrderByClause(getOrderByClause());

        return libraryFileDAO.selectByExample(example);
    }
}

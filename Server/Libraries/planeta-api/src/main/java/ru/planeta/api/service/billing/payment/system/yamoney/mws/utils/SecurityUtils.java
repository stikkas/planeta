package ru.planeta.api.service.billing.payment.system.yamoney.mws.utils;

import org.apache.commons.io.IOUtils;
import org.bouncycastle.cms.*;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.openssl.PEMReader;
import org.bouncycastle.openssl.PEMWriter;

import javax.net.ssl.KeyManager;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import java.io.*;
import java.security.*;
import java.security.cert.CertStore;
import java.security.cert.CertStoreException;
import java.security.cert.CollectionCertStoreParameters;
import java.security.cert.X509Certificate;
import java.util.Arrays;
import java.util.Collection;
import java.util.Enumeration;
import java.util.Iterator;

/**
 * Provides common methods for:
 * <ul>
 * <li>PKCS#7 data singing/verifying;</li>
 * <li>PEM format encoding/decoding;</li>
 * <li>{@link KeyStore}, {@link TrustManager} and {@link KeyManager} initialization;</li>
 * </ul>
 * Created by eshevchenko.
 */
public final class SecurityUtils {

    public static final String BC = BouncyCastleProvider.PROVIDER_NAME;

    static {
        Security.addProvider(new BouncyCastleProvider());
    }

    private SecurityUtils() {
    }

    /**
     * Signs presented data using PKCS#7 cryptographic standart.
     *
     * @param data       singing data;
     * @param privateKey private key;
     * @param cert       certificate;
     * @return signed data.
     * @throws CertStoreException
     * @throws CMSException
     * @throws InvalidAlgorithmParameterException
     * @throws NoSuchAlgorithmException
     * @throws NoSuchProviderException
     */
    public static CMSSignedData signData(CMSProcessableByteArray data, PrivateKey privateKey, X509Certificate cert) throws CertStoreException, CMSException, InvalidAlgorithmParameterException, NoSuchAlgorithmException, NoSuchProviderException {
        CollectionCertStoreParameters parameters = new CollectionCertStoreParameters(Arrays.asList(cert));
        CertStore certStore = CertStore.getInstance("Collection", parameters, BC);

        CMSSignedDataGenerator generator = new CMSSignedDataGenerator();
        generator.addSigner(privateKey, cert, CMSSignedDataGenerator.DIGEST_SHA1);
        generator.addCertificatesAndCRLs(certStore);
        return generator.generate(data, true, BC);
    }

    /**
     * Verifies signed data.
     *
     * @param signedData signed data;
     * @throws Exception when verification failed.
     */
    public static void verifySign(CMSSignedData signedData) throws Exception {
        CertStore certStore = signedData.getCertificatesAndCRLs("Collection", BC);

        SignerInformationStore signersInfo = signedData.getSignerInfos();
        Collection signers = signersInfo.getSigners();
        Iterator signersIt = signers.iterator();
        while (signersIt.hasNext()) {
            SignerInformation signer = (SignerInformation) signersIt.next();
            SignerId signerId = signer.getSID();

            X509Certificate cert = (X509Certificate) certStore.getCertificates(signerId).iterator().next();
            if (!signer.verify(cert.getPublicKey(), BC)) {
                throw new Exception("Signer information verification failed.");
            }
        }
    }

    /**
     * Utility for converting a security artifact to a string
     *
     * @param obj Object The security artifact: key, certificate, etc.
     * @return String
     * @throws Exception
     */
    public final static String pemEncode(Object obj) throws IOException {
        StringWriter writer = new StringWriter();
        PEMWriter pemWriter = new PEMWriter(writer);
        try {
            pemWriter.writeObject(obj);
            pemWriter.flush();
            return writer.toString();
        } finally {
            IOUtils.closeQuietly(pemWriter);
        }
    }

    /**
     * Utility for converting a string to a security artifact
     *
     * @param input String The security artifact encoded in a string
     * @return Object
     * @throws Exception
     */
    public final static Object pemDecode(String input) throws IOException {
        StringReader reader = new StringReader(input);
        PEMReader pemReader = new PEMReader(reader);
        try {
            return pemReader.readObject();
        } finally {
            IOUtils.closeQuietly(pemReader);
        }
    }

    /**
     * Creates {@link TrustManager}'s through initialized by keyStore {@link TrustManagerFactory}.
     *
     * @param algorithm the standard name of the requested trust management algorithm;
     * @param keyStore  initialized {@link java.security.KeyStore};
     * @return initialized {@link TrustManager} arrays.
     */
    public static TrustManager[] createTrustManager(String algorithm, KeyStore keyStore) {
        try {
            TrustManagerFactory trustManagerFactory = TrustManagerFactory.getInstance(algorithm);
            trustManagerFactory.init(keyStore);
            return trustManagerFactory.getTrustManagers();
        } catch (GeneralSecurityException e) {
            throw new IllegalStateException("Can't init JSSE:" + e.getMessage(), e);
        }
    }

    /**
     * Creates {@link KeyManager}'s through initialized by keyStore {@link KeyManagerFactory}.
     *
     * @param algorithm the standard name of the requested trust management algorithm;
     * @param keyStore  initialized {@link java.security.KeyStore};
     * @param keyPass   presented keyStore's password;
     * @return initialized {@link KeyManager} arrays.
     */
    public static KeyManager[] createKeyManager(String algorithm, KeyStore keyStore, char[] keyPass) {
        try {
            KeyManagerFactory keyManagerFactory = KeyManagerFactory.getInstance(algorithm);
            keyManagerFactory.init(keyStore, keyPass);
            return keyManagerFactory.getKeyManagers();
        } catch (GeneralSecurityException e) {
            throw new IllegalStateException("Can't init JSSE:" + e.getMessage(), e);
        }
    }

    /**
     * Creates and initialize particular type {@link KeyStore} from presented <tt>keyStore</tt> as in-stream.
     * @param keyStoreType the type of keystore @see KeyStore.getInstance(String);
     * @param keyStore key store;
     * @param keyStorePassword key store password;
     * @return initialized key store;
     * @throws IOException
     * @throws GeneralSecurityException
     */
    public static KeyStore loadKeyStore(String keyStoreType, InputStream keyStore, char[] keyStorePassword) throws IOException, GeneralSecurityException {
        KeyStore result = KeyStore.getInstance(keyStoreType);
        try {
            result.load(keyStore, keyStorePassword);
        } finally {
            IOUtils.closeQuietly(keyStore);
        }
        return result;
    }

    /**
     * Creates and initialize particular type {@link KeyStore} from presented <tt>keyStorePath</tt> file.
     * @param keyStoreType the type of keystore @see KeyStore.getInstance(String);
     * @param keyStorePath path to key store file;
     * @param keyStorePassword key store password;
     * @return initialized key store;
     * @throws IOException
     * @throws GeneralSecurityException
     */

    public static KeyStore loadKeyStore(String keyStoreType, String keyStorePath, char[] keyStorePassword) throws IOException, GeneralSecurityException {
        return loadKeyStore(keyStoreType, new FileInputStream(keyStorePath), keyStorePassword);
    }


    /**
     * Finds alias in keyStore.
     *
     * @param keyStore key store;
     * @return founded alias.
     * @throws KeyStoreException when alias not found.
     */
    public static String findAlias(KeyStore keyStore) throws KeyStoreException {
        Enumeration<String> e = keyStore.aliases();
        while (e.hasMoreElements()) {
            String entry = e.nextElement();
            if (keyStore.isKeyEntry(entry)) {
                return entry;
            }
        }
        throw new KeyStoreException("Cannot find a private key entry");
    }
}

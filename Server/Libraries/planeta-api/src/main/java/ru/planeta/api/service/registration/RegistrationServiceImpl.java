package ru.planeta.api.service.registration;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import ru.planeta.api.BaseService;
import ru.planeta.api.exceptions.MessageCode;
import ru.planeta.api.exceptions.NotFoundException;
import ru.planeta.api.exceptions.PermissionException;
import ru.planeta.api.geo.GeoResolverWebService;
import ru.planeta.api.mail.UploaderService;
import ru.planeta.api.model.UserAuthorizationInfo;
import ru.planeta.api.service.profile.AuthorizationService;
import ru.planeta.api.service.profile.PasswordEncoder;
import ru.planeta.api.service.profile.ProfileService;
import ru.planeta.api.service.profile.UsernameTransformer;
import ru.planeta.api.service.statistic.UniversalAnalyticsService;
import ru.planeta.commons.web.RegistrationData;
import ru.planeta.dao.commondb.ProfileBalanceDAO;
import ru.planeta.dao.commondb.UserCredentialsDAO;
import ru.planeta.dao.commondb.UserPrivateInfoDAO;
import ru.planeta.dao.profiledb.ProfileCommunicationChannelDAO;
import ru.planeta.dao.profiledb.ProfileDAO;
import ru.planeta.dao.profiledb.UserDAO;
import ru.planeta.dao.trashcan.RefererDAO;
import ru.planeta.model.common.UserPrivateInfo;
import ru.planeta.model.common.auth.ExternalAuthentication;
import ru.planeta.model.common.auth.UserCredentials;
import ru.planeta.model.enums.CommunicationChannelType;
import ru.planeta.model.enums.ProfileType;
import ru.planeta.model.enums.UserStatus;
import ru.planeta.model.profile.Profile;
import ru.planeta.model.profile.ProfileCommunicationChannel;
import ru.planeta.model.profile.User;
import ru.planeta.model.profile.media.Photo;
import ru.planeta.model.stat.RequestStat;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.io.IOException;
import java.util.Date;
import java.util.EnumSet;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

@Service
public class RegistrationServiceImpl extends BaseService implements RegistrationService {
    private static final int IP_RESOLUTION_DELAY = 5;
    private static final int SET_PHOTO_DELAY = 3;
    private static final int SEND_EMAIL_DELAY = 3;

    private static final Logger log = Logger.getLogger(RegistrationServiceImpl.class);

    @Value("${registration.autosubscribe.profiles}")
    private long[] autoSubscribeProfiles;

    // This variable is used for tests purposes only
    private boolean isAsynchronous = true;

    @Override
    public void setAsynchronous(boolean asynchronous) {
        isAsynchronous = asynchronous;
    }

    @Autowired
    private AuthorizationService authorizationService;
    @Autowired
    private UniversalAnalyticsService universalAnalyticsService;
    @Autowired
    private UserCredentialsDAO userCredentialsDAO;
    @Autowired
    private UserPrivateInfoDAO userPrivateInfoDAO;
    @Autowired
    private GeoResolverWebService geoResolverWebService;
    @Autowired
    private ProfileDAO profileDAO;
    @Autowired
    private UserDAO userDAO;
    @Autowired
    private PasswordEncoder passwordEncoder;
    @Autowired
    private ProfileBalanceDAO profileBalanceDAO;
    @Autowired
    private ProfileCommunicationChannelDAO profileCommunicationChannelDAO;
    @Autowired
    private ProfileService profileService;
    @Autowired
    private UploaderService uploaderService;
    @Autowired
    private UsernameTransformer usernameTransformer;

    @Autowired
    private RefererDAO refererDAO;


    @Nonnull
    private UserAuthorizationInfo registerFromSocialNetworkInner(ExternalAuthentication externalAuthentication,
                                                                 RegistrationData registrationData, RequestStat requestStat) throws PermissionException {

        String planetaUsername = externalAuthentication.getCredentialType().name() + externalAuthentication.getUsername();
        String password = generatePassword();

        UserAuthorizationInfo userAuthorizationInfo = insertUser(planetaUsername, null, password, new RegistrationDataPopulateStrategy(registrationData, geoResolverWebService, requestStat));
        final long profileId = userAuthorizationInfo.getProfile().getProfileId();

        addCommunicationChannel(externalAuthentication, profileId);

        authorizationService.addSocialNetworkCredentials(profileId, externalAuthentication);

        autoSubscribe(profileId);

        profileBalanceDAO.insert(profileId);
        setProfilePhotoAfterTransactionCommit(profileId, registrationData.getPhotoUrl());

        universalAnalyticsService.trackRegistrationBySocial(profileId);

        return userAuthorizationInfo;
    }

    @Override
    public void resendRegistrationCompleteEmail(long profileId) throws NotFoundException {
        UserPrivateInfo privateInfo = getUserPrivateInfoDAO().selectByUserId(profileId);
        if (privateInfo == null) {
            throw new NotFoundException(UserPrivateInfo.class, profileId);
        }
        String regCode = generateRegCode();
        privateInfo.setRegCode(regCode);
        getUserPrivateInfoDAO().update(privateInfo);
        getMailClient().sendRegistrationCompleteEmail(privateInfo.getEmail(), privateInfo.getRegCode());
    }


    @Override
    @Nullable
    public UserPrivateInfo recoverPassword(String email, String url, String redirectUrl) {
        String regCode = UUID.randomUUID().toString().replace("-", "");

        UserPrivateInfo userPrivateInfo = authorizationService.getUserPrivateInfoByUsername(email);
        if (userPrivateInfo != null) {
            userPrivateInfo.setRegCode(regCode);
            getUserPrivateInfoDAO().update(userPrivateInfo);

            //send registration email
            getMailClient().sendPasswordRecoveryEmail(email, regCode, url, redirectUrl);
        }
        return userPrivateInfo;
    }


    private UserPrivateInfo startEmailConfirmation(String username, String email, final String rawPassword, final EmailConfirmationStrategy confirmationStrategy) {
        final String regCode = generateRegCode();
        UserPrivateInfo userPrivateInfo = userPrivateInfoDAO.selectByUsername(username);
        if (userPrivateInfo == null) {
            return null;
        }
        userPrivateInfo.setUsernameAndEmail(email);
        userPrivateInfo.setRegCode(regCode);
        userPrivateInfoDAO.update(userPrivateInfo);

        delay(new Runnable() {
            @Override
            public void run() {
                try {
                    confirmationStrategy.sendEmail(rawPassword, regCode);

                } catch (Exception ex) {
                    log.warn("Email not sended", ex);
                }
            }
        }, SEND_EMAIL_DELAY, TimeUnit.SECONDS);
        return userPrivateInfo;
    }


    @Nonnull
    @Override
    public UserAuthorizationInfo registerByEmail(String email, String password, RequestStat requestStat) throws PermissionException, IllegalArgumentException {
        String username = usernameTransformer.normalize(email);

        UserAuthorizationInfo userAuthorizationInfo = insertUser(username, null, password,
                new EmailRegistrationPopulateStrategy(email, null, geoResolverWebService, requestStat));

        final long profileId = userAuthorizationInfo.getProfile().getProfileId();

        UserPrivateInfo userPrivateInfo = startEmailConfirmation(username, email, password, new EmailConfirmationStrategy(getMailClient(), email));
        userAuthorizationInfo.setUserPrivateInfo(userPrivateInfo);
        profileBalanceDAO.insert(profileId);

        final Runnable task = new Runnable() {
            @Override
            public void run() {
                try {
                    autoSubscribe(profileId);
                } catch (Exception ex) {
                    log.warn("Additional registration process failed", ex);
                }
            }
        };
        if (isAsynchronous) {
            delay(task, 3, TimeUnit.SECONDS);
        } else {
            task.run();
        }

        return userAuthorizationInfo;
    }


    @Override
    @Nonnull
    public UserAuthorizationInfo registerFromSocialNetworkWithEmail(String email,
                                                                    ExternalAuthentication externalAuthentication,
                                                                    RegistrationData registrationData, RequestStat requestStat
    ) throws PermissionException, IllegalArgumentException {

        UserAuthorizationInfo userAuthorizationInfo = registerFromSocialNetworkInner(externalAuthentication, registrationData, requestStat);
        addEmailToExternalUserSafe(email, userAuthorizationInfo);
        return userAuthorizationInfo;
    }


    @Override
    public Pair<UserAuthorizationInfo, String> autoRegisterGetPassword(String email, String displayName, RequestStat requestStat) throws PermissionException, IllegalArgumentException {
        String generatedPassword = generatePassword();
        UserAuthorizationInfo userAuthorizationInfo = autoRegisterByEmail(email, displayName, generatedPassword, new AutoRegistrationConfirmationStrategy(getMailClient(), email), requestStat);
        return ImmutablePair.of(userAuthorizationInfo, generatedPassword);
    }

    @Override
    @Nonnull
    public UserAuthorizationInfo autoRegisterByEmail(String email, String displayName, RequestStat requestStat) throws PermissionException, IllegalArgumentException {
        return autoRegisterGetPassword(email, displayName, requestStat).getLeft();
    }

    @Nonnull
    private UserAuthorizationInfo autoRegisterByEmail(String email, String displayName,
                                                      String generatedPassword,
                                                      EmailConfirmationStrategy confirmationStrategy, RequestStat requestStat) throws PermissionException, IllegalArgumentException {
        String username = usernameTransformer.normalize(email);
        UserAuthorizationInfo userAuthorizationInfo = insertUser(username, displayName, generatedPassword,
                new EmailRegistrationPopulateStrategy(username, null, geoResolverWebService, requestStat));
        final long profileId = userAuthorizationInfo.getProfile().getProfileId();

        UserPrivateInfo userPrivateInfo = startEmailConfirmation(username, email, generatedPassword, confirmationStrategy);
        userAuthorizationInfo.setUserPrivateInfo(userPrivateInfo);

        autoSubscribe(profileId);

        profileBalanceDAO.insert(profileId);

        return userAuthorizationInfo;
    }

    @Override
    public long autoRegisterIfNotExists(String email, String displayName, RequestStat requestStat) throws PermissionException {
        UserPrivateInfo userPrivateInfo = authorizationService.getUserPrivateInfoByUsername(email);
        long profileId;
        if (userPrivateInfo != null) {
            profileId = userPrivateInfo.getUserId();
        } else {
            UserAuthorizationInfo userAuthorizationInfo = autoRegisterByEmail(email, displayName, requestStat);
            profileId = userAuthorizationInfo.getProfile().getProfileId();
        }
        return profileId;
    }

    @Override
    public void mergeAccounts(UserPrivateInfo masterUserPrivateInfo, UserPrivateInfo oddPrivateInfo) throws NotFoundException {
        if (oddPrivateInfo.getUserId() == masterUserPrivateInfo.getUserId()) {
            // nothing to merge, it is the same accounts
            return;
        }

        List<UserCredentials> credentials = userCredentialsDAO.getUserCredentialsOfType(oddPrivateInfo.getUserId(), null);
        if (credentials.isEmpty()) {
            throw new NotFoundException(UserCredentials.class, oddPrivateInfo.getUserId());
        }
        for (UserCredentials credential : credentials) {
            credential.setProfileId(masterUserPrivateInfo.getUserId());
            userCredentialsDAO.update(credential);
        }

        oddPrivateInfo.setEmail(null);
        oddPrivateInfo.setUsername("from_" + oddPrivateInfo.getUsername() + "_to_" + masterUserPrivateInfo.getUsername());
        userPrivateInfoDAO.update(oddPrivateInfo);
    }

    @Override
    public UserAuthorizationInfo addEmailToExternalUser(String email, UserAuthorizationInfo userAuthorizationInfo) throws NotFoundException, PermissionException, IllegalArgumentException {
        if (userAuthorizationInfo == null) {
            throw new NotFoundException(UserAuthorizationInfo.class);
        }
        if (userAuthorizationInfo.getUserPrivateInfo() == null || userAuthorizationInfo.getUser() == null) {
            throw new NotFoundException(UserPrivateInfo.class);
        }

        addEmailToExternalUserSafe(email, userAuthorizationInfo);
        return userAuthorizationInfo;
    }

    private void addEmailToExternalUserSafe(String email, UserAuthorizationInfo userAuthorizationInfo) throws IllegalArgumentException, PermissionException {
        UserPrivateInfo userPrivateInfo = userAuthorizationInfo.getUserPrivateInfo();
        if (StringUtils.isNotBlank(userPrivateInfo.getEmail())) {
            throw new IllegalArgumentException("Can't add email to user with email");
        }

        String validEmail = usernameTransformer.normalize(email);

        // generate new password to send with email
        String generatedPassword = generatePassword();
        userPrivateInfo.setPassword(passwordEncoder.encode(generatedPassword));
        addEmailToExternalUserNoConfirm(validEmail, userPrivateInfo);

        UserPrivateInfo modifiedUserPrivateInfo = startEmailConfirmation(validEmail, validEmail, generatedPassword,
                new AutoRegistrationConfirmationStrategy(getMailClient(), validEmail));


        userAuthorizationInfo.setUserPrivateInfo(modifiedUserPrivateInfo);
    }


    private static String generatePassword() {
        return UUID.randomUUID().toString().replace("-", "").substring(0, 6);
    }

    private static String generateRegCode() {
        return UUID.randomUUID().toString().replace("-", "");
    }


    private void addCommunicationChannel(ExternalAuthentication externalAuthentication, long profileId) {
        // create VK link
        ProfileCommunicationChannel profileCommunicationChannel = new ProfileCommunicationChannel();
        profileCommunicationChannel.setProfileId(profileId);
        updateCommunicationChannel(externalAuthentication, profileCommunicationChannel);
        profileCommunicationChannelDAO.insert(profileCommunicationChannel);
    }

    private static void updateCommunicationChannel(ExternalAuthentication externalAuthentication, ProfileCommunicationChannel profileCommunicationChannel) {
        String externalUsername = externalAuthentication.getUsername();
        switch (externalAuthentication.getCredentialType()) {
            case VK:
                profileCommunicationChannel.setCommunicationChannelType(CommunicationChannelType.VKONTAKTE);
                profileCommunicationChannel.setChannelValue("https://vk.com/id" + externalUsername);
                break;
            case FACEBOOK:
                profileCommunicationChannel.setCommunicationChannelType(CommunicationChannelType.FACEBOOK);
                profileCommunicationChannel.setChannelValue("https://www.facebook.com/" + externalUsername);
                break;
            case MAILRU:
                profileCommunicationChannel.setCommunicationChannelType(CommunicationChannelType.MAILRU);
                profileCommunicationChannel.setChannelValue(externalUsername);
                break;
            case YANDEX:
                profileCommunicationChannel.setCommunicationChannelType(CommunicationChannelType.YANDEX);
                profileCommunicationChannel.setChannelValue(externalUsername);
                break;
        }
    }

    private void addEmailToExternalUserNoConfirm(String email, UserPrivateInfo userPrivateInfo) throws PermissionException {
        UserPrivateInfo selectedUserPrivateInfo = userPrivateInfoDAO.selectByUsername(email);
        if (selectedUserPrivateInfo != null) {
            throw new PermissionException(MessageCode.REGISTRATION_CURRENT_EMAIL_ALREADY_EXISTS);
        }

        userPrivateInfo.setUsernameAndEmail(email);
        userPrivateInfoDAO.update(userPrivateInfo);
    }

    /**
     * @param username         username of user
     * @param rawPassword      password of user
     * @param populateStrategy how to fill userPrivateInfo, user and profile objects   @return private info, user and profile
     * @throws PermissionException if username exists
     */
    @Nonnull
    private UserAuthorizationInfo insertUser(String username, String displayName, String rawPassword, final PopulateUserRegistrationInfoStrategy populateStrategy) throws PermissionException {
        UserPrivateInfo selectedUserPrivateInfo = userPrivateInfoDAO.selectByUsername(username);
        if (selectedUserPrivateInfo != null) {
            throw new PermissionException(MessageCode.REGISTRATION_CURRENT_EMAIL_ALREADY_EXISTS);
        }

        // Adding profile object
        final Profile profile = new Profile();
        profile.setProfileType(ProfileType.USER);
        if (StringUtils.isBlank(profile.getImageUrl())) {
            String imageUrl = profileService.generateRandomAvatarUrl();
            profile.setImageUrl(imageUrl);
            profile.setSmallImageUrl(imageUrl);
        }
        profile.setShowBackedCampaigns(true);
        profile.setReceiveNewsletters(true);
        profile.setDisplayName(displayName);
        profile.setReceiveMyCampaignNewsletters(true);
        populateStrategy.populateProfile(profile);
        profileDAO.insert(profile);

        delay(new Runnable() {
            @Override
            public void run() {
                try {
                    Profile profile1 = profileDAO.selectById(profile.getProfileId());
                    populateStrategy.delayedProfileUpdate(profile1);
                    profileDAO.update(profile1);
                } catch (Exception ex) {
                    log.warn("delayProfileUpdate failed", ex);
                }
            }
        }, IP_RESOLUTION_DELAY, TimeUnit.SECONDS);


        // Adding user object
        User user = new User();
        user.setProfileId(profile.getProfileId());
        userDAO.insert(user);

        // Adding user private info object
        UserPrivateInfo userPrivateInfo = new UserPrivateInfo();
        userPrivateInfo.setUserId(profile.getProfileId());
        //noinspection deprecation
        userPrivateInfo.setUsername(username);
        String encodedPassword = passwordEncoder.encode(rawPassword);
        userPrivateInfo.setPassword(encodedPassword);
        userPrivateInfo.setTimeAdded(new Date());
        userPrivateInfo.setUserStatus(EnumSet.of(UserStatus.USER));
        populateStrategy.populatePrivateInfo(userPrivateInfo);
        userPrivateInfoDAO.insert(userPrivateInfo);

        try {
            RequestStat r = populateStrategy.getRequestStat();
            if (r != null) {
                r.setExternalId(userPrivateInfo.getUserId());
                refererDAO.insert(r);
            }
        } catch (Exception ex) {
            log.error(ex);
        }

        return new UserAuthorizationInfo(profile, user, userPrivateInfo);
    }

    private void autoSubscribe(long clientId) {
        for (long profileId : autoSubscribeProfiles) {
            try {
                profileService.relationAddActive(clientId, profileId);
            } catch (Exception ex) {
                log.warn(String.format("Cannot subscribe user %s to %s", clientId, profileId), ex);
            }
        }
    }

    private void setProfilePhotoAfterTransactionCommit(final long profileId, final String externalPhotoUrl) {
        delay(new Runnable() {
            @Override
            public void run() {
                try {
                    setProfilePhoto(externalPhotoUrl, profileId);
                } catch (Exception ex) {
                    log.warn("No photo", ex);
                }
            }
        }, SET_PHOTO_DELAY, TimeUnit.SECONDS);
    }

    private void setProfilePhoto(String externalPhotoUrl, long profileId) throws IOException, NotFoundException, PermissionException {
        if (StringUtils.isEmpty(externalPhotoUrl)) {
            log.debug("No external photo");
            externalPhotoUrl = profileService.generateRandomAvatarUrl();
        }
        Photo photo = uploaderService.transferProfilePhoto(externalPhotoUrl, profileId);
        if (photo == null || photo.getPhotoId() == 0) {
            log.debug("No photo " + externalPhotoUrl);
            return;
        }
        profileService.setProfileAvatar(profileId, profileId, photo.getPhotoId());
    }


}

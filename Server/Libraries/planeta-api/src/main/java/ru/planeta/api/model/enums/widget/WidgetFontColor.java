package ru.planeta.api.model.enums.widget;

/**
 * Created with IntelliJ IDEA.
 * Date: 07.08.13
 * Time: 16:55
 */
public enum WidgetFontColor {
    WHITE("text-white"),
    BLACK("text-black");

    private String style;

    private WidgetFontColor(String style) {
        this.style = style;
    }

    @Override
    public String toString() {
        return style;
    }

    public String getAsString(){
        return toString();
    }
}

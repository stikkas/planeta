package ru.planeta.api.model;

import java.util.Date;

/**
 * Represents a date range. We need this class for setting advanced search filters like on birth date
 * @author ameshkov
 */
public class DateRange {

	private final static long MIN_UTC = -2208997800000L;
	private final static long MAX_UTC = 32503680000000L;
	private Date fromDate;
	private Date toDate;

    public DateRange(Date from, Date to) {
		if (from == null) {
			from = new Date(MIN_UTC);
		}
		if (to == null) {
			to = new Date(MAX_UTC);
		}
		this.fromDate = from;
		this.toDate = to;
	}

    public Date getFrom() {
		return fromDate;
    }

    public Date getTo() {
		return toDate;
    }

}

package ru.planeta.api.model;

import org.springframework.context.MessageSource;
import org.springframework.context.NoSuchMessageException;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;

import java.util.HashMap;
import java.util.Map;

/**
 * Represents any action status
 *
 * @author ameshkov
 */
public class ActionStatus<T> {

    public static <T> ActionStatus<T> createSuccessStatus() {
        ActionStatus<T> status = new ActionStatus<>();
        status.setSuccess(true);
        return status;
    }

    public static <T> ActionStatus<T> createSuccessStatus(T result) {
        ActionStatus<T> status = createSuccessStatus();
        status.setResult(result);
        return status;
    }

    public static <T> ActionStatus<T> createErrorStatus(BindingResult bindingResult, MessageSource messageSource) {
        ActionStatus<T> status = new ActionStatus<>();
        status.setSuccess(false);
        if (bindingResult.getFieldErrorCount() == 1) {
            status.setErrorMessage(messageSource.getMessage(bindingResult.getFieldErrors().get(0).getCode(), bindingResult.getFieldErrors().get(0).getArguments(), LocaleContextHolder.getLocale()));
        } else if (bindingResult.getFieldErrorCount() > 0) {
            status.setErrorMessage(messageSource.getMessage("field.error", null, LocaleContextHolder.getLocale()));
        } else if (!bindingResult.getGlobalErrors().isEmpty()) {
            for (ObjectError error : bindingResult.getGlobalErrors()) {
                status.setErrorMessage(messageSource.getMessage(error, LocaleContextHolder.getLocale()));
            }
        } else {
            status.setErrorMessage("Invalid object");
        }

        for (FieldError fieldError : bindingResult.getFieldErrors()) {
            status.addFieldError(fieldError.getField(), messageSource.getMessage(fieldError, LocaleContextHolder.getLocale()));
        }
        return status;
    }

    public static <T> ActionStatus<T> createErrorStatus(String messageCode, MessageSource messageSource) {
        return createErrorStatus(messageCode, messageSource, null);
    }

    public static <T> ActionStatus<T> createErrorStatus(String messageCode, MessageSource messageSource, Class<? extends Exception> errorClass) {
        ActionStatus<T> status = new ActionStatus<>();
        status.setSuccess(false);
        try {
            status.setErrorMessage(messageSource.getMessage(messageCode, null, LocaleContextHolder.getLocale()));
        } catch (NoSuchMessageException ex) {
            status.setErrorMessage(messageCode);
        }
        if (errorClass != null) {
            status.setErrorClass(errorClass.getSimpleName());
        }

        return status;
    }

    public static <T> ActionStatus<T> createErrorStatus(String message) {
        ActionStatus<T> status = new ActionStatus<>();
        status.setSuccess(false);
        status.setErrorMessage(message);
        return status;
    }

    private boolean success;
    private String errorMessage;
    private String errorClass;
    private T result;
    private Map<String, String> fieldErrors;

    public String getErrorClass() {
        return errorClass;
    }

    public void setErrorClass(String errorClass) {
        this.errorClass = errorClass;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public T getResult() {
        return result;
    }

    public void setResult(T result) {
        this.result = result;
    }

    public Map<String, String> getFieldErrors() {
        return fieldErrors;
    }

    public void setFieldErrors(Map<String, String> fieldErrors) {
        this.fieldErrors = fieldErrors;
    }

    public ActionStatus<T> addFieldError(String fieldName, String error) {
        if (fieldErrors == null) {
            fieldErrors = new HashMap<>();
        }
        fieldErrors.put(fieldName, error);
        return this;
    }
}

package ru.planeta.api.service.social;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.binary.StringUtils;
import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.springframework.security.access.AuthorizationServiceException;
import org.springframework.stereotype.Service;
import ru.planeta.api.aspect.transaction.NonTransactional;
import ru.planeta.api.model.ExternalUserInfo;
import ru.planeta.commons.model.Gender;
import ru.planeta.commons.web.RegistrationData;
import ru.planeta.commons.web.WebUtils;
import ru.planeta.model.common.auth.CredentialType;
import ru.planeta.model.common.auth.ExternalAuthentication;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLEncoder;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Date: 18.09.12
 *
 * @author s.kalmykov
 */
@Service
public class FacebookServiceImpl implements OAuthClientService {

    private static final String APP_ID = "283941085052946";
    //private static final String APP_ID = "1407572939277057";
    private static final String SECRET = "c3181474330be1669bc81c33b0167b0d";
    //private static final String SECRET = "7512f3e36802f8774237351c34e96b7f";
    private static final String SIGN_ALGORITHM = "HMACSHA256";

    private static final Logger logger = Logger.getLogger(FacebookServiceImpl.class);



    private static String secretGetUrl(String code, String redirectUrl) {
        Map<String, String> query = new HashMap<String, String>();
        query.put("client_id", APP_ID);
        query.put("redirect_uri", redirectUrl);
        query.put("client_secret", SECRET);
        query.put("code", code);
        String queryString = WebUtils.tryUrlEncodeMap(query, "UTF-8");
        if (org.apache.commons.lang3.StringUtils.isNotEmpty(queryString)) {
            return "https://graph.facebook.com/oauth/access_token?" + queryString;
        }

        logger.error("queryString is null");
        throw new RuntimeException("queryString is null");
    }


    private static ExternalUserInfo parseJSON(String userInfoJSON) {
        try {
            Map<String, Object> response = new ObjectMapper().readValue(userInfoJSON, HashMap.class);
            String firstName = (String) response.get("first_name");
            String lastName = (String) response.get("last_name");
            String email = (String) response.get("email");
            String uid = (response.get("id")).toString();
            String gender = (String) (response.get("gender"));
            String birthday = (String) (response.get("birthday"));
            RegistrationData registrationData = new RegistrationData();
            registrationData.setFirstName(firstName);
            registrationData.setLastName(lastName);
            registrationData.setGender("female".equals(gender) ? Gender.FEMALE : Gender.MALE);
            registrationData.setPhotoUrl("https://graph.facebook.com/" + uid + "/picture?type=large");
            try {
                SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy");
                if (birthday != null) {
                    Date birthDate = format.parse(birthday);
                    registrationData.setUserBirthDate(birthDate);
                }
            } catch (ParseException e) {
                logger.error("birthday: " + birthday);
            }
            ExternalAuthentication externalAuthentication = new ExternalAuthentication(uid, CredentialType.FACEBOOK);

            ExternalUserInfo externalUserInfo = new ExternalUserInfo();
            externalUserInfo.setEmail(email);
            externalUserInfo.setRegistrationData(registrationData);
            externalUserInfo.setExternalAuthentication(externalAuthentication);
            return externalUserInfo;
        } catch (IOException e) {
            logger.error("Malformed json in Facebook response "+ userInfoJSON, e);
        }
        return null;
    }

    private static Map<String, Object> parseSignedRequest(String signedRequest) {
        try {
            int idx = signedRequest.indexOf(".");
            // it's important to use exactly this decoder with urlSafe mode on. Other gives bad result.
            Base64 decoder = new Base64(true);
            byte[] sig = decoder.decode(signedRequest.substring(0, idx));
            String rawPayload = signedRequest.substring(idx + 1);
            String payload = StringUtils.newStringUtf8(decoder.decode(rawPayload));

            Map<String, Object> response = new ObjectMapper().readValue(payload, HashMap.class);
            String algorithm = (String) (response.get("algorithm"));
            if (!"HMAC-SHA256".equals(algorithm)) {
                logger.error("Bad algorithm for signed_request");
                throw new AuthorizationServiceException("Bad algorithm for signed_request");
            }

            SecretKeySpec secret = new SecretKeySpec(SECRET.getBytes("UTF-8"), SIGN_ALGORITHM);
            Mac mac = Mac.getInstance(SIGN_ALGORITHM);
            mac.init(secret);
            byte[] expectedDigestSignature = mac.doFinal(rawPayload.getBytes("UTF-8"));
            if (!Arrays.equals(expectedDigestSignature, sig)) {
                logger.error("Bad signature for signed_request");
                throw new AuthorizationServiceException("Bad signature for signed_request");
            }


            return response;
        } catch (NoSuchAlgorithmException e) {
            logger.error("No algorithm to decode Facebook data");
        } catch (InvalidKeyException e) {
            logger.error("Invalid key while decoding Facebook data");
        } catch (JsonMappingException e) {
            logger.error("Invalid json mapping in Facebook signed request");
        } catch (JsonParseException e) {
            logger.error("Invalid json parsing in Facebook signed request");
        } catch (UnsupportedEncodingException e) {
            logger.error("No UTF-8 encoding");
        } catch (IOException e) {
            logger.error("Malformed json in Facebook signed request");
        }
        return null;
    }

    @Override
    public String getRedirectUrl(String saveRedirectUrl) {
        try {
            return "https://www.facebook.com/dialog/oauth?client_id=" + APP_ID + "&response_type=code&redirect_uri=" + URLEncoder.encode(saveRedirectUrl, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            return null;
        }
    }

    @Nullable
    @Override
    @NonTransactional
    public ExternalUserInfo authorizeExternal(String code, String redirectUrl) throws IOException {
        String getUrl = secretGetUrl(code, redirectUrl);
        String fbParams = IOUtils.toString(new URL(getUrl));
        logger.debug(fbParams);
        Map<String, Object> response = new ObjectMapper().readValue(fbParams, HashMap.class);
        String accessToken = (String) response.get("access_token");
        String infoUrl = "https://graph.facebook.com/v2.12/me?access_token=" + accessToken + "&fields=id,first_name,last_name,gender,birthday";
        String userInfoJSON = IOUtils.toString(new URL(infoUrl));
        logger.debug(userInfoJSON);
        return parseJSON(userInfoJSON);
    }

    @Override
    public boolean isMyUrl(@Nonnull String url) {
        return url.contains("fb");
    }
}

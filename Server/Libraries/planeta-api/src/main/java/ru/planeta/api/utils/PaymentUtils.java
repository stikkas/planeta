package ru.planeta.api.utils;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.log4j.Logger;
import ru.planeta.model.common.TopayTransaction;

import javax.annotation.Nonnull;

/**
 * Created with IntelliJ IDEA.
 * User: a.volkov
 * Date: 20.05.14
 * Time: 15:33
 */
public final class PaymentUtils {

    private static final String SALT = "yuYgk3JYt75ghCF34rtbgt";

    private static final Logger log = Logger.getLogger(PaymentUtils.class);

    private PaymentUtils() {}

    @Nonnull
    public static String sign(@Nonnull TopayTransaction transaction) {
        StringBuilder sb = new StringBuilder();
        sb.append(transaction.getProfileId())
                .append(transaction.getTimeAdded().getTime())
                .append(transaction.getTransactionId())
                .append(transaction.getAmountNet().intValue());
        log.debug("transaction id: " + transaction.getTransactionId() + " string: " + sb);
        return DigestUtils.md5Hex(sb.toString());
    }
}

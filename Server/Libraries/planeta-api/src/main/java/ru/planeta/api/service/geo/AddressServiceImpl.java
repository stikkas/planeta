package ru.planeta.api.service.geo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.planeta.dao.commondb.AddressDAO;
import ru.planeta.model.common.Address;

/**
 * @author ds.kolyshev
 *         Date: 22.05.12
 */
@Service
public class AddressServiceImpl implements AddressService {

    private final AddressDAO addressDAO;

    @Autowired
    public AddressServiceImpl(AddressDAO addressDAO) {
        this.addressDAO = addressDAO;
    }

    @Override
    public Address getAddress(long addressId) {
        return addressDAO.select(addressId);
    }

    @Override
    public void changeAddress(Address address) {
        addressDAO.update(address);
    }

    @Override
    public void saveAddress(Address address) {
        if (address.getAddressId() > 0) {
            changeAddress(address);
        } else {
            addressDAO.insert(address);
        }
    }

    @Override
    public Address getLastPostAddress(long buyerId) {
        return addressDAO.getLastPostAddress(buyerId);
    }
}

package ru.planeta.api.model.authentication;

import org.springframework.security.core.GrantedAuthority;
import ru.planeta.model.enums.UserStatus;

import java.util.ArrayList;
import java.util.Collection;
import java.util.EnumSet;

/**
 * User: atropnikov
 * Date: 21.03.12
 * Time: 17:28
 */
public enum Authority implements GrantedAuthority {

    MEMBER("ROLE_MEMBER", UserStatus.USER),
    MODERATOR("ROLE_MODERATOR", UserStatus.MODERATOR),
    MANAGER("ROLE_MANAGER", UserStatus.MANAGER),
    ADMIN("ROLE_ADMIN", UserStatus.ADMIN),
    SUPER_ADMIN("ROLE_ADMIN", UserStatus.SUPER_ADMIN);

    private String authority;
    private UserStatus status;

    private Authority(String name, UserStatus status) {
        this.authority = name;
        this.status = status;
    }

    @Override
    public String getAuthority() {
        return authority;
    }

    public static Collection<GrantedAuthority> getUserAuthorities(EnumSet<UserStatus> userStatuses) {

        Collection<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();

        if (userStatuses != null && !userStatuses.isEmpty()) {
            for (Authority authority : values()) {
                if (userStatuses.contains(authority.status)) {
                    authorities.add(authority);
                }
            }
        }
        return authorities;
    }

    @Override
    public String toString() {
        return authority;
    }
}

package ru.planeta.api.helper;

import ru.planeta.model.enums.OrderObjectType;

import java.util.EnumSet;

import static ru.planeta.model.enums.OrderObjectType.*;

/**
 * Created by a.savanovich on 22.11.2016.
 */
public class ShareUtils {

    private static final EnumSet<OrderObjectType> ORDER_OBJECT_TYPES = EnumSet.of(SHARE, INVESTING, INVESTING_WITH_ALL_ALLOWED_INVOICE, INVESTING_WITHOUT_MODERATION);

    public static boolean isShareType(OrderObjectType type) {
        return ORDER_OBJECT_TYPES.contains(type);
    }

    public static EnumSet<OrderObjectType> getOrderObjectTypes() {
        return ORDER_OBJECT_TYPES;
    }
}

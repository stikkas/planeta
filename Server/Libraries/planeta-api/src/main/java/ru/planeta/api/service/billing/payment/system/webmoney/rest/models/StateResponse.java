package ru.planeta.api.service.billing.payment.system.webmoney.rest.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * User: Serge Blagodatskih<stikkas17@gmail.com><br>
 * Date: 14.06.16<br>
 * Time: 17:47
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class StateResponse {

    private int errorCode;

    private PaymentStateResponse payment;

    @JsonProperty("ErrorCode")
    public int getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }

    @JsonProperty("Payment")
    public PaymentStateResponse getPayment() {
        return payment;
    }

    public void setPayment(PaymentStateResponse payment) {
        this.payment = payment;
    }

}

package ru.planeta.api.service.billing.payment.system.yamoney.mws.operations;

/**
 * List of supported MWS operation names.<br>
 * Created by eshevchenko.
 */
public enum OperationName {
    listOrders,
    listReturns,
    returnPayment,
    confirmPayment,
    cancelPayment,
    repeatCardPayment;

    /**
     * Formats MWS operation URL.
     * @param baseUrl service base URL;
     * @return formatted operation URL.
     */
    public String formatUrl(String baseUrl) {
        return baseUrl + "/" + name();
    }
}

package ru.planeta.api.service.social;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import ru.planeta.api.aspect.transaction.NonTransactional;
import ru.planeta.api.model.ExternalUserInfo;
import ru.planeta.commons.model.Gender;
import ru.planeta.commons.web.RegistrationData;
import ru.planeta.commons.web.WebUtils;
import ru.planeta.model.common.auth.CredentialType;
import ru.planeta.model.common.auth.ExternalAuthentication;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLEncoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Date: 19.09.12
 *
 * @author s.kalmykov
 */
@Service
public class MailRuServiceImpl implements OAuthClientService {

    // ru.planeta@mail.ru : zLVN2kS3bHocQOR  -> http://api.mail.ru/sites/my

    //    private static final String APP_ID = "687562";
//    private static final String SECRET = "167e46ca32678a04b1a3efc220e6f2f0";
    @SuppressWarnings("UnusedDeclaration")
    private static final String PUBLIC_KEY = "4abbcf7d0b209f358d61a7ee2a48cdd1";
    private static final Logger logger = Logger.getLogger(MailRuServiceImpl.class);


    @Value("${oauth.mailru.appId}")
    private String APP_ID;

    @Value("${oauth.mailru.secret}")
    private String SECRET;

    @Override
    public String getRedirectUrl(String saveRedirectUrl) {
        try {
            return "https://connect.mail.ru/oauth/authorize?client_id=" + APP_ID + "&response_type=code&redirect_uri="
                    + URLEncoder.encode(saveRedirectUrl, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            return null;
        }
    }

    private String fetchUserInfoJson(String code, String redirectUrl) throws IOException {
        Map<String, String> postData = secretPostData(code, redirectUrl, APP_ID, SECRET);
        String tokenJSON = WebUtils.uploadMapExternal("https://connect.mail.ru/oauth/token", postData);
        logger.debug(tokenJSON);
        if (tokenJSON == null) {
            throw new IOException("No connect to connect.mail.ru");
        }
        HashMap<String, Object> object = (new ObjectMapper()).readValue(tokenJSON, HashMap.class);
        String accessToken = (String) object.get("access_token");
        String dataUrl = generateDataUrl(accessToken, SECRET, APP_ID, "users.getInfo");
        logger.debug(dataUrl);
        return IOUtils.toString(new URL(dataUrl));
    }

    @Nullable
    @Override
    @NonTransactional
    public ExternalUserInfo authorizeExternal(String code, String redirectUrl) throws IOException {
        String userInfoJSON = fetchUserInfoJson(code, redirectUrl);
        return parseJSON(userInfoJSON);
    }

    @Override
    public boolean isMyUrl(@Nonnull String url) {
        return url.contains("mailru");
    }

    /**
     * [{"pic_50":"http://avt.appsmail.ru/mail/s.a.kalmykov/_avatar50","friends_count":0,"pic_22":"http://avt.appsmail.ru/mail/s.a.kalmykov/_avatar22","nick":"Сергей Калмыков","is_verified":0,"is_online":0,"pic_big":"http://avt.appsmail.ru/mail/s.a.kalmykov/_avatarbig","last_name":"Калмыков","has_pic":0,"email":"s.a.kalmykov@mail.ru","pic_190":"http://avt.appsmail.ru/mail/s.a.kalmykov/_avatar190","referer_id":"","vip":0,"pic_32":"http://avt.appsmail.ru/mail/s.a.kalmykov/_avatar32","birthday":"30.12.1985","referer_type":"","link":"http://my.mail.ru/mail/s.a.kalmykov/","location":{"country":{"name":"Россия","id":"24"},"city":{"name":"Москва","id":"25"},"region":{"name":"Москва","id":"999999"}},"uid":"4584638109898584213","app_installed":1,"pic_128":"http://avt.appsmail.ru/mail/s.a.kalmykov/_avatar128","sex":0,"pic":"http://avt.appsmail.ru/mail/s.a.kalmykov/_avatar","pic_small":"http://avt.appsmail.ru/mail/s.a.kalmykov/_avatarsmall","pic_180":"http://avt.appsmail.ru/mail/s.a.kalmykov/_avatar180","first_name":"Сергей","pic_40":"http://avt.appsmail.ru/mail/s.a.kalmykov/_avatar40"}]
     */
    public static ExternalUserInfo parseJSON(String userInfoJSON) {
        try {
            HashMap<String, Object> response = (HashMap<String, Object>) (new ObjectMapper().readValue(userInfoJSON, List.class)).get(0);
            String firstName = (String) response.get("first_name");
            String lastName = (String) response.get("last_name");
            String uid = (String) (response.get("uid"));
            String sex = (response.get("sex")).toString();
            String birthday = (String) (response.get("birthday"));
            String picture = (String) response.get("pic_190");
            String email = (String) response.get("email");
            RegistrationData registrationData = new RegistrationData();
            registrationData.setFirstName(firstName);
            registrationData.setLastName(lastName);
            try {
                SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy");
                if (birthday != null) {
                    Date birthDate = format.parse(birthday);
                    registrationData.setUserBirthDate(birthDate);
                }
            } catch (ParseException e) {
//                credentialsInfo.setUserBirthDate(new Date());
            }
            registrationData.setGender("0".equals(sex) ? Gender.MALE : Gender.FEMALE);
            registrationData.setPhotoUrl(picture);

            ExternalAuthentication externalAuthentication = new ExternalAuthentication(uid, CredentialType.MAILRU);

            ExternalUserInfo externalUserInfo = new ExternalUserInfo();
            externalUserInfo.setEmail(email);
            externalUserInfo.setRegistrationData(registrationData);
            externalUserInfo.setExternalAuthentication(externalAuthentication);
            return externalUserInfo;
        } catch (IOException e) {
            logger.error("Malformed json in MailRU response " + userInfoJSON, e);
        }
        return null;
    }

    static String generateDataUrl(String accessToken, String secret, String appId, String method) {
        Map<String, String> query = new TreeMap<>();
        query.put("app_id", appId);
        query.put("session_key", accessToken);
        query.put("method", method);
        query.put("secure", "1");

        StringBuilder signatureParams = new StringBuilder();
        StringBuilder queryString = new StringBuilder();
        for (Map.Entry<String, String> entry : query.entrySet()) {
            signatureParams.append(entry.getKey()).append("=").append(entry.getValue());
            queryString.append(entry.getKey()).append("=").append(entry.getValue()).append("&");
        }

        String signature = DigestUtils.md5Hex(signatureParams + secret);
        queryString.append("sig=").append(signature);

        return "http://www.appsmail.ru/platform/api?" + queryString;
    }

    private static Map<String, String> secretPostData(String code, String redirectUrl, String app_id, String secret) {
        Map<String, String> query = new HashMap<String, String>();
        query.put("grant_type", "authorization_code");
        query.put("redirect_uri", redirectUrl);
        query.put("client_id", app_id);
        query.put("client_secret", secret);
        query.put("code", code);
        return query;
    }
}

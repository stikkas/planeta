package ru.planeta.api.service.quiz;

import ru.planeta.api.exceptions.NotFoundException;
import ru.planeta.dao.profiledb.ArrayListWithCount;
import ru.planeta.model.trashcan.*;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.List;

public interface QuizService {
    void addOrUpdate(Quiz quiz);

    void addQuizQuestion(QuizQuestion quizQuestion);

    void addQuizQuestionRelation(long quizId, long quizQuestionId, long quizQuestionOrderNum);

    void addQuizQuestionRelation(long quizId, long quizQuestionId);

    void addOrUpdateQuizQuestionOption(QuizQuestionOption quizQuestionOption);

    void setQuizQuestionEnabling(long quizQuestionId, boolean enabled);

    void deleteQuizQuestionOption(long quizQuestionOptionId);

    ArrayListWithCount<Quiz> getQuizzesSearch(String searchString, int offset, int limit);

    List<QuizQuestion> getQuizQuestions(int offset, int limit);

    List<QuizQuestion> getQuizQuestionsByQuizId(long quizId, Boolean enabled);

    QuizQuestion getQuizQuestionById(long quizQuestionId);

    @Nonnull
    QuizQuestion getQuizQuestionSafe(long quizQuestionId) throws NotFoundException;

    void updateQuizQuestion(QuizQuestion quizQuestion);

    void updateQuizQuestionsOrder(long quizId, long quizQuestionId, int newOrderNum, int oldOrderNum);

    void updateQuizQuestionOptionsOrder(long quizQuestionOptionId, long quizQuestionId, int newOrderNum, int oldOrderNum);

    void updateQuizQuestionOption(QuizQuestionOption quizQuestionOption);

    Quiz getQuiz(String quizAlias, Boolean enabled);

    @Nonnull
    Quiz getQuizSafe(String quizAlias, @Nullable Boolean enabled) throws NotFoundException;

    void addQuizAnswers(List<QuizAnswer> quizAnswers, long userId);

    List<QuizAnswer> getQuizAnswersByParams(@Nullable Long quizId, @Nullable Long quizQuestionId, @Nullable Long userId, long offset, long limit);

    List<QuizAnswerForReport> getQuizAnswersForReportByParams(@Nullable Long quizId,
                                                              @Nullable Long userId,
                                                              long offset, long limit);
}

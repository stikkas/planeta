package ru.planeta.api.service.billing.payment.system.yamoney.mws.models;

import ru.planeta.api.service.billing.payment.system.yamoney.mws.models.enums.CurrencyCode;
import ru.planeta.api.service.billing.payment.system.yamoney.mws.models.enums.ErrorCode;
import ru.planeta.api.service.billing.payment.system.yamoney.mws.models.enums.RequestStatus;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by eshevchenko.
 */
@XmlType(name = "returnPayment")
@XmlAccessorType(XmlAccessType.FIELD)
public class ReturnPayment {

    @XmlAttribute
    public Long returnId;
    @XmlAttribute
    public Long invoiceId;
    @XmlAttribute
    public Long shopId;
    @XmlAttribute
    public BigDecimal amount;
    @XmlAttribute
    public CurrencyCode currency;
    @XmlAttribute
    public String cause;
    @XmlAttribute
    public RequestStatus status;
    @XmlAttribute
    public ErrorCode error;
    @XmlAttribute
    public Date createdDT;
    @XmlAttribute
    public Date processedDT;
    @XmlAttribute
    public String sender;
    @XmlAttribute
    public BigDecimal articleAmount;
    @XmlAttribute
    public CurrencyCode articleCurrency;
    @XmlAttribute
    public String orderNumber;
}

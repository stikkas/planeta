package ru.planeta.api.service.configurations;

import ru.planeta.model.enums.BlockSettingType;
import ru.planeta.model.enums.ProfileType;
import ru.planeta.model.profile.ProfileBlockSetting;

import javax.annotation.Nonnull;
import java.util.Map;

/**
 * @author a.savanovich
 */
public interface DefaultBlockSettings {
    @Nonnull
    Map<BlockSettingType, ProfileBlockSetting> getDefaultPermissionLevels(ProfileType type);
}

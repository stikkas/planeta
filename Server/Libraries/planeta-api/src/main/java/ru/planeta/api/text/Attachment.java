package ru.planeta.api.text;

import org.apache.commons.lang3.StringUtils;

import static ru.planeta.commons.lang.StringUtils.escapeHtml;

/**
 * Base class for all attachments
 */
public abstract class Attachment {

    private String url;
    private long ownerId;
    private long objectId;

    /**
     * Attachment's source url
     *
     * @return
     */
    public String getUrl() {
        return url;
    }

    /**
     * Sets attachment's source url
     *
     * @param url
     */
    public void setUrl(String url) {
        this.url = url;
    }

    /**
     * Video owner identifier (optional field, set after comment has been added only)
     *
     * @return
     */
    public long getOwnerId() {
        return ownerId;
    }

    /**
     * Sets video owner identifier
     *
     * @param ownerId
     */
    public void setOwnerId(long ownerId) {
        this.ownerId = ownerId;
    }

    /**
     * Returns unique attached object identifier.
     *
     * @return
     */
    public long getObjectId() {
        return objectId;
    }

    /**
     * Sets unique attached object identifier.
     *
     * @param objectId
     */
    public void setObjectId(long objectId) {
        this.objectId = objectId;
    }

    protected String toString(String tagName, Object... attributes) {
        StringBuilder builder = new StringBuilder();
        builder.append("<p:").append(tagName);
        for (int i = 0; i < attributes.length - 1; i += 2) {
            String attrName = String.valueOf(attributes[i]);
            String attrValue = String.valueOf(attributes[i + 1]);
            if (!StringUtils.isEmpty(attrValue) && !attrValue.equals("0") && !attrValue.equals("null")) {
                builder.append(" ").append(attrName).append("=\"").append(escapeHtml(attrValue)).append("\"");
            }
        }
        builder.append("/>");
        return builder.toString();
    }

}

package ru.planeta.api.aspect.logging;

import org.apache.log4j.Logger;
import ru.planeta.model.stat.log.LoggerType;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;

/**
 * Database logger type info holder.<br>
 * Stores logger specific data in ThreadLocal.<br>
 * User: eshevchenko
 */
public final class DBLoggerTypeInfoHolder {

    private static final Logger LOGGER = Logger.getLogger(DBLoggerTypeInfoHolder.class);

    private static final ThreadLocal<Map<LoggerType, Object>> loggerTypeInfoHolder = new ThreadLocal<Map<LoggerType, Object>>() {

        @Override
        protected Map<LoggerType, Object> initialValue() {
            return new HashMap<>();
        }
    };

    private static final ThreadLocal<HttpServletRequest> requestHolder = new ThreadLocal<HttpServletRequest>();
    private static final ThreadLocal<HttpServletResponse> responseHolder = new ThreadLocal<HttpServletResponse>();

    public static void setLoggerTypeInfo(LoggerType loggerType, Object info) {
        loggerTypeInfoHolder.get().put(loggerType, info);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(String.format("%s-logger information installed: %s", loggerType, info));
        }
    }

    public static <T> T getInfoByLoggerType(Class<T> infoClass, LoggerType loggerType) {
        return (T) loggerTypeInfoHolder.get().get(loggerType);
    }

    public static void clear() {
        loggerTypeInfoHolder.remove();
    }

    public static long getAuditedAdminId() {
        AdminAuditLoggerInfo info = getInfoByLoggerType(AdminAuditLoggerInfo.class, LoggerType.ADMIN_AUDIT);
        return (info == null) ? 0L : info.getAdminId();
    }

    public static void removeLoggableRequestInfo() {
        requestHolder.remove();
        responseHolder.remove();
    }

    public static void setLoggableRequestInfo(HttpServletRequest request, HttpServletResponse response) {
        requestHolder.set(request);
        responseHolder.set(response);
    }

    public static final HttpServletRequest getLoggableRequest() {
        return requestHolder.get();
    }

    public static final HttpServletResponse getLoggableResponse() {
        return responseHolder.get();
    }
}

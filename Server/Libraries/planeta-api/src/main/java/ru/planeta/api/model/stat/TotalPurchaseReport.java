package ru.planeta.api.model.stat;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Date: 08.11.12
 * Time: 13:41
 */
public class TotalPurchaseReport {
    private int regCount = 0;
    private int regCountDiff = 0;

    private int sharesCount = 0;
    private int sharesCountDiff = 0;
    private int sharesAmount = 0;
    private int sharesAmountDiff = 0;

    private int ticketsCount = 0;
    private int ticketsCountDiff = 0;
    private int ticketAmount = 0;
    private int ticketAmountDiff = 0;

    private int biblioPurchasesCount = 0;
    private int biblioPurchasesCountDiff = 0;
    private int productsAmount = 0;
    private int productsAmountDiff = 0;

    private int productsCount = 0;
    private int productsCountDiff = 0;
    private int biblioPurchasesAmount = 0;
    private int biblioPurchasesAmountDiff = 0;

    private int totalAmount = 0;       
    private int totalAmountDiff = 0;

    public TotalPurchaseReport(List<PurchaseReport> purchaseReports) {
        addPurchaseReportList(purchaseReports);
    }

    public TotalPurchaseReport() {
    }

    public void addPurchaseReport(PurchaseReport purchaseReport) {
        this.regCount += purchaseReport.getRegCount();
        //
        this.sharesCount += purchaseReport.getSharesCount();
        this.ticketsCount += purchaseReport.getTicketsCount();
        this.productsCount += purchaseReport.getProductsCount();
        this.biblioPurchasesCount += purchaseReport.getBiblioPurchasesCount();
        //
        this.sharesAmount += purchaseReport.getSharesAmount().intValue();
        this.ticketAmount += purchaseReport.getTicketsAmount().intValue();
        this.productsAmount += purchaseReport.getProductsAmount().intValue();
        this.biblioPurchasesAmount += purchaseReport.getBiblioPurchasesAmount().intValue();
        //
        this.totalAmount = this.productsAmount + this.sharesAmount + this.ticketAmount + this.biblioPurchasesAmount;
    }

    public void addPurchaseReportList(List<PurchaseReport> purchaseReports) {
        for (PurchaseReport report : purchaseReports) {
            addPurchaseReport(report);
        }
    }

    public int getRegCount() {
        return regCount;
    }

    public int getRegCountDiff() {
        return regCountDiff;
    }

    public int getSharesCount() {
        return sharesCount;
    }

    public int getSharesCountDiff() {
        return sharesCountDiff;
    }

    public int getTicketsCount() {
        return ticketsCount;
    }

    public int getTicketsCountDiff() {
        return ticketsCountDiff;
    }

    public int getProductsCount() {
        return productsCount;
    }

    public int getProductsCountDiff() {
        return productsCountDiff;
    }

    public void setRegCountDiff(int regCountDiff) {
        this.regCountDiff = regCountDiff;
    }

    public void setSharesCountDiff(int sharesCountDiff) {
        this.sharesCountDiff = sharesCountDiff;
    }

    public void setTicketsCountDiff(int ticketsCountDiff) {
        this.ticketsCountDiff = ticketsCountDiff;
    }

    public void setProductsCountDiff(int productsCountDiff) {
        this.productsCountDiff = productsCountDiff;
    }

    public int getSharesAmount() {
        return sharesAmount;
    }

    public void setSharesAmount(int sharesAmount) {
        this.sharesAmount = sharesAmount;
    }

    public int getSharesAmountDiff() {
        return sharesAmountDiff;
    }

    public void setSharesAmountDiff(int sharesAmountDiff) {
        this.sharesAmountDiff = sharesAmountDiff;
    }

    public int getTicketAmount() {
        return ticketAmount;
    }

    public void setTicketAmount(int ticketAmount) {
        this.ticketAmount = ticketAmount;
    }

    public int getTicketAmountDiff() {
        return ticketAmountDiff;
    }

    public void setTicketAmountDiff(int ticketAmountDiff) {
        this.ticketAmountDiff = ticketAmountDiff;
    }

    public int getProductsAmount() {
        return productsAmount;
    }

    public void setProductsAmount(int productsAmount) {
        this.productsAmount = productsAmount;
    }

    public int getProductsAmountDiff() {
        return productsAmountDiff;
    }

    public void setProductsAmountDiff(int productsAmountDiff) {
        this.productsAmountDiff = productsAmountDiff;
    }

    public int getBiblioPurchasesCount() {
        return biblioPurchasesCount;
    }

    public int getBiblioPurchasesCountDiff() {
        return biblioPurchasesCountDiff;
    }

    public void setBiblioPurchasesCountDiff(int biblioPurchasesCountDiff) {
        this.biblioPurchasesCountDiff = biblioPurchasesCountDiff;
    }

    public int getBiblioPurchasesAmount() {
        return biblioPurchasesAmount;
    }

    public void setBiblioPurchasesAmount(int biblioPurchasesAmount) {
        this.biblioPurchasesAmount = biblioPurchasesAmount;
    }

    public int getBiblioPurchasesAmountDiff() {
        return biblioPurchasesAmountDiff;
    }

    public void setBiblioPurchasesAmountDiff(int biblioPurchasesAmountDiff) {
        this.biblioPurchasesAmountDiff = biblioPurchasesAmountDiff;
    }

    public int getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(int totalAmount) {
        this.totalAmount = totalAmount;
    }

    public int getTotalAmountDiff() {
        return totalAmountDiff;
    }

    public void setTotalAmountDiff(int totalAmountDiff) {
        this.totalAmountDiff = totalAmountDiff;
    }
}

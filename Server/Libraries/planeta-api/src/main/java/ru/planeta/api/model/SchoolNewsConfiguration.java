package ru.planeta.api.model;

import ru.planeta.api.model.promo.BasePromoConfiguration;

/**
 * Date: 07.07.2015
 * Time: 13:25
 */
public class SchoolNewsConfiguration extends BasePromoConfiguration {

    private String title;
    private String imageUrl;
    private String description;
    private String originalUrl;
    private long postId;
    private boolean archive;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getOriginalUrl() {
        return originalUrl;
    }

    public void setOriginalUrl(String originalUrl) {
        this.originalUrl = originalUrl;
    }

    public long getPostId() {
        return postId;
    }

    public void setPostId(long postId) {
        this.postId = postId;
    }

    public boolean isArchive() {
        return archive;
    }

    public void setIsArchive(boolean isArchive) {
        this.archive = isArchive;
    }
}

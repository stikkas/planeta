package ru.planeta.api.service.faq;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.planeta.dao.commondb.BaseFaqArticleService;
import ru.planeta.model.commondb.FaqArticle;
import ru.planeta.model.commondb.FaqArticleExample;

import java.util.List;

/**
 * Created by kostiagn on 01.09.2015.
 */
@Service
public class FaqArticleServiceImpl extends BaseFaqArticleService implements FaqArticleService {
    @Autowired
    private FaqParagraphService faqParagraphService;

    public void deleteFaqArticle(Long faqArticleId) {
        super.deleteFaqArticle(faqArticleId);
        faqParagraphService.delete(faqArticleId);
    }

    @Override
    public void insertFaqArticle(FaqArticle faqArticle) {
        if (faqArticle.getOrderNum() == null) {
            FaqArticleExample faqArticleExample = new FaqArticleExample();
            faqArticleExample.or().andFaqCategoryIdEqualTo(faqArticle.getFaqCategoryId());
            faqArticleExample.setOrderByClause("order_num desc");
            faqArticleExample.setOffset(0);
            faqArticleExample.setLimit(1);
            final List<FaqArticle> list = getFaqArticleMapper().selectByExample(faqArticleExample);

            faqArticle.setOrderNum(CollectionUtils.isEmpty(list) ? 1 : list.get(0).getOrderNum() + 1);
        }
        super.insertFaqArticle(faqArticle);
    }

    @Override
    public void resort(long idFrom, long idTo, long faqCategoryId) {


        int orderNumFrom = -1;
        int orderNumTo = -1;

        final List<FaqArticle> faqArticles = selectFaqArticleListByFaqCategory(faqCategoryId, 0, 0);
        for (FaqArticle faqArticle : faqArticles) {
            if (faqArticle.getFaqArticleId() == idFrom) {
                orderNumFrom = faqArticle.getOrderNum();
            }
            if (faqArticle.getFaqArticleId() == idTo) {
                orderNumTo = faqArticle.getOrderNum();
            }
        }

        if (orderNumFrom == orderNumTo || orderNumFrom < 0 || orderNumTo < 0) {
            return;
        }
        if (orderNumFrom < orderNumTo) {
            for (FaqArticle faqArticle : faqArticles) {
                if (faqArticle.getOrderNum() == orderNumFrom) {
                    faqArticle.setOrderNum(orderNumTo);
                    getFaqArticleMapper().updateByPrimaryKey(faqArticle);
                } else if (faqArticle.getOrderNum() > orderNumFrom && faqArticle.getOrderNum() <= orderNumTo) {
                    faqArticle.setOrderNum(faqArticle.getOrderNum() - 1);
                    getFaqArticleMapper().updateByPrimaryKey(faqArticle);
                }
            }
        } else {
            for (FaqArticle faqArticle : faqArticles) {
                if (faqArticle.getOrderNum() == orderNumFrom) {
                    faqArticle.setOrderNum(orderNumTo);
                    getFaqArticleMapper().updateByPrimaryKey(faqArticle);
                } else if (faqArticle.getOrderNum() >= orderNumTo && faqArticle.getOrderNum() < orderNumFrom) {
                    faqArticle.setOrderNum(faqArticle.getOrderNum() + 1);
                    getFaqArticleMapper().updateByPrimaryKey(faqArticle);
                }
            }
        }
    }
}

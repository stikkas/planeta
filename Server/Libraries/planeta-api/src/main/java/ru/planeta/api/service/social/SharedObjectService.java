package ru.planeta.api.service.social;

import ru.planeta.api.exceptions.NotFoundException;
import ru.planeta.model.stat.SharedObject;
import ru.planeta.model.stat.SharedObjectStats;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

/**
 * Shared object service
 * User: m.shulepov
 * Date: 23.10.12
 * Time: 17:04
 */
public interface SharedObjectService {

    /**
     * Processes shared object, saving different stats about it to the database
     * <p>
     * If profile with specified <code>clientId</code> or <code>uuid</code> has already shared <code>sharedObject</code> on specific service,
     * no record would be stored in the database
     * </p>
     * <p>
     * Note: in default implementation we store only first 256 symbols of url
     * </p>
     *
     * @param clientId
     * @param uuid
     * @param sharedObject
     */
    void processSharedObject(long clientId, String uuid, SharedObject sharedObject);

    /**
     * Returns shared object stats by url
     *
     */
    @Nullable
    SharedObjectStats getSharedObjectByUrl(String url);

}

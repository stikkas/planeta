package ru.planeta.api.text;

import ru.planeta.commons.external.oembed.provider.VideoType;

/**
 * Represents a video attachment
 */
public class VideoAttachment extends VisualAttachment {

    private String videoUrl;
    private String name;
    private String description;
    private int duration;
    private String imageUrl;
    private VideoType videoType;

    /**
     * Video url (for youtube -- equal to youtube's video id)
     *
     * @return
     */
    public String getVideoUrl() {
        return videoUrl;
    }

    /**
     * Sets video urls
     *
     * @param videoUrl
     */
    public void setVideoUrl(String videoUrl) {
        this.videoUrl = videoUrl;
    }

    /**
     * Gets video name
     *
     * @return
     */
    public String getName() {
        return name;
    }

    /**
     * Sets video name
     *
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Gets video description
     *
     * @return
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets video description
     *
     * @param description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Gets video duration
     *
     * @return
     */
    public int getDuration() {
        return duration;
    }

    /**
     * Sets video duration
     *
     * @param duration
     */
    public void setDuration(int duration) {
        this.duration = duration;
    }

    /**
     * Gets image url
     *
     * @return
     */
    public String getImageUrl() {
        return imageUrl;
    }

    /**
     * Sets image url
     *
     * @param imageUrl
     */
    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    /**
     * Gets video type
     *
     * @return
     */
    public VideoType getVideoType() {
        return videoType;
    }

    /**
     * Sets video type
     *
     * @param videoType
     */
    public void setVideoType(VideoType videoType) {
        this.videoType = videoType;
    }

    public String toString() {
        return toString("video", "id", getObjectId(), "owner", getOwnerId(),
                "duration", getDuration(), "image", getImageUrl(), "name", getName(),
                "type", getVideoType(), "width", getWidth(), "height", getHeight());
    }

}

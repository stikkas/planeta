package ru.planeta.api.service.charity;

import ru.planeta.model.charity.LibraryFile;
import ru.planeta.model.charity.LibraryFileExample;

import java.util.List;

public interface CharityLibraryFileService {
    void insertLibraryFile(LibraryFile libraryFile);
    void insertLibraryFileAllFields(LibraryFile libraryFile);
    void updateLibraryFile(LibraryFile libraryFile);
    void updateLibraryFileAllFields(LibraryFile libraryFile);
    void insertOrUpdateLibraryFile(LibraryFile libraryFile);
    void insertOrUpdateLibraryFileAllFields(LibraryFile libraryFile);
    void deleteLibraryFile(Long id);
    String getOrderByClause();
    LibraryFile selectLibraryFile(Long libraryFileId);
    LibraryFileExample getLibraryFileByHeaderExample(String header, int offset, int limit);
    int selectLibraryFileCountByHeader(String header);
    List<LibraryFile> selectLibraryFileListByHeader(String header, int offset, int limit);
    List<LibraryFile> selectLibraryFileListByHeader(String header, List<Long> libraryFileIdList);

    List<LibraryFile> selectLibraryFileByHeaderList(String headerPart, int offset, int limit);
}

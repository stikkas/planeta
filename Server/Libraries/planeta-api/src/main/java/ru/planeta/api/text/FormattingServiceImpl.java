package ru.planeta.api.text;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.xml.sax.InputSource;
import ru.planeta.api.Utils;
import ru.planeta.api.model.json.VideoInfo;
import ru.planeta.api.service.content.VideoService;
import ru.planeta.commons.text.Bbcode;
import ru.planeta.commons.web.WebUtils;
import ru.planeta.dao.profiledb.PhotoAlbumDAO;
import ru.planeta.dao.profiledb.PhotoDAO;
import ru.planeta.model.Constants;
import ru.planeta.model.enums.PermissionLevel;
import ru.planeta.model.profile.media.Photo;
import ru.planeta.model.profile.media.PhotoAlbum;

import javax.annotation.Nullable;
import java.io.StringReader;
import java.util.*;
import java.util.regex.Pattern;

import static ru.planeta.commons.external.oembed.provider.VideoType.EXTERNAL;

/**
 * FormattingService implementation for formatting html text of comments, posts, etc.
 *
 * @author m.shulepov
 */
@Service
public class FormattingServiceImpl implements FormattingService {
    private static final Logger log = Logger.getLogger(FormattingServiceImpl.class);

    private final MessageParserService messageParserService;
    private final PhotoAlbumDAO photoAlbumDAO;
    private final PhotoDAO photoDAO;
    private final VideoService videoService;

    private Map<Class, AttachmentSaver> attachmentSavers = new HashMap<>();

    private List<Pattern> validPatterns = new ArrayList<>();

    @Value("${valid.external.url.patterns:}")
    public void setValidPatterns(String validPatternsString) {
        String[] validPatterns = validPatternsString.split("\\|");
        for (String pattern : validPatterns) {
            this.validPatterns.add(Pattern.compile(WebUtils.appendProtocolIfNecessary(pattern, false)));
            this.validPatterns.add(Pattern.compile(WebUtils.appendProtocolIfNecessary("www." + pattern, false)));
        }
    }

    @Autowired
    public FormattingServiceImpl(MessageParserService messageParserService, PhotoAlbumDAO photoAlbumDAO, PhotoDAO photoDAO, VideoService videoService) {
        attachmentSavers.put(ImageAttachment.class, new ImageAttachmentSaver());
        attachmentSavers.put(VideoAttachment.class, new VideoAttachmentSaver());
        this.messageParserService = messageParserService;
        this.photoAlbumDAO = photoAlbumDAO;
        this.photoDAO = photoDAO;
        this.videoService = videoService;
    }

    @Override
    public String formatBbcode(String bbcode) {
        if (StringUtils.isEmpty(bbcode)) {
            return StringUtils.EMPTY;
        }
        return Bbcode.transform(bbcode);
    }

    @Nullable
    @Override
    public String formatPlainText(String text, long profileId, boolean extractLinksOnly) {
        if (StringUtils.isBlank(text)) {
            return text;
        }
        final String formatted;
        if (Bbcode.containsBbcode(text)) {
            formatted = formatBbcode(text);
        } else {
            formatted = formatMessage(text, profileId, extractLinksOnly);
        }
        return replaceExternalLinks(formatted);
    }

    @Override
    public String formatMessage(String text, long profileId, boolean extractLinksOnly) {
        if (StringUtils.isBlank(text)) {
            return StringUtils.EMPTY;
        }
        StringBuilder sb = new StringBuilder();
        text = StringEscapeUtils.escapeHtml4(StringUtils.trim(text));

        List<MessagePart> messageParts = messageParserService.extractMessageParts(text);

        for (MessagePart messagePart : messageParts) {

            switch (messagePart.getMessagePartType()) {
                case TEXT:
                    if (!StringUtils.isEmpty(messagePart.getText())) {
                        String textPart = messagePart.getText();
                        sb.append(StringUtils.replace(textPart, "\n", "<br>"));
                    }
                    break;
                case LINK:

                    final Attachment attachment;
                    if (!extractLinksOnly) {
                        attachment = messageParserService.extractAttachment(messagePart.getText());
                    } else {
                        attachment = null;
                    }

                    if (attachment == null) {
                        String urlToInsertIntoHtml = messagePart.getText();
                        if (Utils.INSTANCE.isExternalUrlToRedirect(urlToInsertIntoHtml, validPatterns)) {
                            Map<String, String> params = new HashMap<>();
                            params.put(AWAY_PARAMETER, urlToInsertIntoHtml);
                            urlToInsertIntoHtml = WebUtils.createUrl(AWAY_URL, params, true);
                        }
                        sb.append("<a href=\"").append(urlToInsertIntoHtml).append("\" target=\"_blank\" rel=\"nofollow noopener\">").append(messagePart.getText()).append("</a>");
                    } else {
                        attachment.setOwnerId(profileId);
                        saveAttachment(attachment);
                        sb.append(attachment);
                    }
                    break;
            }
        }

        return sb.toString();
    }

    @Override
    public boolean validateHtml(String html) {
        return HtmlValidator.validateHtml(html);
    }

    @Override
    public String replaceExternalLinks(String html) {
        return Utils.INSTANCE.replaceExternalUrls(html, AWAY_URL, AWAY_PARAMETER, validPatterns);
    }

    @Override
    public Map<String, String> getRichPhotosOfWrongOwner(long rightOwnerId, String richHtml) {
        RichMediaExtractingParser parser = new RichMediaExtractingParser(false);
        try {
            Map<String, String> photoToUrlMap = new HashMap<>();
            parser.parse(new InputSource(new StringReader(richHtml)));
            for (RichMediaObject richMediaObject : parser.getRichMediaTags()) {
                if (richMediaObject.getOwner() != rightOwnerId && richMediaObject.getType() == RichMediaObject.Type.PHOTO) {
                    // TODO still need this? How image can be wrong
                    // log.error("Wrong photo author " + rightOwnerId + " " + richMediaObject.getOwner());
                    photoToUrlMap.put(String.valueOf(richMediaObject.getId()), richMediaObject.getParam("image"));
                }
            }
            return photoToUrlMap;
        } catch (Exception e) {
            log.error(e);
            return Collections.emptyMap();
        }
    }

    @Override
    public String cleanHtml(String html) {
        if (StringUtils.isEmpty(html)) {
            return StringUtils.EMPTY;
        }
        html = replaceExternalLinks(html);
        return HtmlValidator.cleanHtml(html);
    }

    @Override
    public String createAnnotation(String html) {
        if (StringUtils.isBlank(html)) {
            return html;
        }
        return BlogPostAnnotationGenerator.generate(html);
    }


    private void saveAttachment(Attachment attachment) {
        AttachmentSaver saver = attachmentSavers.get(attachment.getClass());
        if (saver != null) {
            saver.save(attachment);
        } else {
            throw new IllegalStateException("there is no attachment saver for class: " + attachment.getClass());
        }
    }

    // ~ Attachment Savers =============================================================================================

    interface AttachmentSaver {
        void save(Attachment attachment);
    }

    private class ImageAttachmentSaver implements AttachmentSaver {

        @Override
        public void save(Attachment attachment) {
            Photo photo = new Photo();
            photo.setProfileId(attachment.getOwnerId());
            photo.setAuthorProfileId(attachment.getOwnerId());
            photo.setAlbumId(0);
            photo.setPhotoAlbumTypeId(Constants.INSTANCE.getALBUM_USER_HIDDEN());
            photo.setImageUrl(attachment.getUrl());
            photo.setTimeAdded(new Date());
            photo.setDescription("");
            PhotoAlbum album = photoAlbumDAO.selectAlbumByType(photo.getProfileId(), photo.getPhotoAlbumTypeId());
            if (album == null) {
                album = new PhotoAlbum();
                album.setProfileId(photo.getProfileId());
                album.setAuthorProfileId(photo.getProfileId());
                album.setAlbumTypeId(photo.getPhotoAlbumTypeId());
                // TODO: check if existing album (of HIDDEN type) has the same permission level
                album.setViewPermission(PermissionLevel.EVERYBODY);
                album.setTimeAdded(new Date());
                album.setTimeUpdated(new Date());
                album.setPhotosCount(0);
                album.setViewsCount(0);
                photoAlbumDAO.insert(album);
            }
            photo.setAlbumId(album.getAlbumId());
            photoDAO.insert(photo);
            attachment.setObjectId(photo.getPhotoId());
        }
    }

    private class VideoAttachmentSaver implements AttachmentSaver {

        @Override
        public void save(Attachment attachment) {
            VideoAttachment videoAttachment = (VideoAttachment) attachment;
            long ownerId = attachment.getOwnerId();
            try {
                VideoInfo videoInfo = videoService.addExternalVideo(ownerId, ownerId, videoAttachment.getVideoUrl(), EXTERNAL);
                attachment.setObjectId(videoInfo.getVideoId());
            } catch (Exception ignored) {
                log.error(ignored);
            }
        }
    }

}

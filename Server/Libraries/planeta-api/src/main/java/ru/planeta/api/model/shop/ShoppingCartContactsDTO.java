package ru.planeta.api.model.shop;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import ru.planeta.model.common.Address;
import ru.planeta.model.shop.enums.DeliveryType;

import java.math.BigDecimal;

/**
 * Created with IntelliJ IDEA.
 * User: michail
 * Date: 25.08.16
 * Time: 16:30
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class ShoppingCartContactsDTO {

    private String customerName;
    private long customerId = -1;
    private String email;
    private String phoneNumber;
    private long serviceId;
    private DeliveryType deliveryType;
    private BigDecimal deliveryPrice;
    private boolean onlyDigitalProducts;
    private String comment;


    private Address customerContacts = new Address();

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public long getServiceId() {
        return serviceId;
    }

    public void setServiceId(long serviceId) {
        this.serviceId = serviceId;
    }

    public Address getCustomerContacts() {
        return customerContacts;
    }

    public void setCustomerContacts(Address customerContacts) {
        this.customerContacts = customerContacts;
    }

    public BigDecimal getDeliveryPrice() {
        return deliveryPrice;
    }

    public void setDeliveryPrice(BigDecimal deliveryPrice) {
        this.deliveryPrice = deliveryPrice;
    }

    public long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(long customerId) {
        this.customerId = customerId;
    }

    public DeliveryType getDeliveryType() {
        return deliveryType;
    }

    public void setDeliveryType(DeliveryType deliveryType) {
        this.deliveryType = deliveryType;
    }

    public Boolean getOnlyDigitalProducts() {
        return onlyDigitalProducts;
    }

    public void setOnlyDigitalProducts(Boolean onlyDigitalProducts) {
        this.onlyDigitalProducts = onlyDigitalProducts;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
}

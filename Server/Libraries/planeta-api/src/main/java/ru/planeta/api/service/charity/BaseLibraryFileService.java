package ru.planeta.api.service.charity;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import ru.planeta.dao.charitydb.LibraryFileDAO;
import ru.planeta.model.charity.LibraryFile;
import ru.planeta.model.charity.LibraryFileExample;

public abstract class BaseLibraryFileService {
    @Autowired
    protected LibraryFileDAO libraryFileDAO;

    //**************************************************************************************************
    //*********************************  Create ** Update ** Delete  *************************************
    //**************************************************************************************************
    public void insertLibraryFile(LibraryFile libraryFile) {
        
        
        libraryFileDAO.insertSelective(libraryFile);
    }

    public void insertLibraryFileAllFields(LibraryFile libraryFile) {
        
        
        libraryFileDAO.insert(libraryFile);
    }

    public void updateLibraryFile(LibraryFile libraryFile) {
        
        libraryFileDAO.updateByPrimaryKeySelective(libraryFile);
    }

    public void updateLibraryFileAllFields(LibraryFile libraryFile) {
        
        libraryFileDAO.updateByPrimaryKey(libraryFile);
    }

    public void insertOrUpdateLibraryFile(LibraryFile libraryFile) {
        
        LibraryFile selectedLibraryFile = libraryFileDAO.selectByPrimaryKey( libraryFile.getLibraryFileId());
        if (selectedLibraryFile == null) {
            
            libraryFileDAO.insertSelective(libraryFile);
        } else {
            libraryFileDAO.updateByPrimaryKeySelective(libraryFile);
        }
    }

    public void insertOrUpdateLibraryFileAllFields(LibraryFile libraryFile) {
        
        LibraryFile selectedLibraryFile = libraryFileDAO.selectByPrimaryKey( libraryFile.getLibraryFileId());
        if (selectedLibraryFile == null) {
            
            libraryFileDAO.insert(libraryFile);
        } else {
            libraryFileDAO.updateByPrimaryKey(libraryFile);
        }
    }

    public void deleteLibraryFile(Long id) {
        libraryFileDAO.deleteByPrimaryKey(id);
    }

    public String getOrderByClause() {
        return "library_file_id";
    }

    //**************************************************************************************************
    //***************************************** library_file *********************************************
    //************************************** SelectByPrimaryKey ******************************************
    //**************************************************************************************************
    public LibraryFile selectLibraryFile(Long libraryFileId) {
        return libraryFileDAO.selectByPrimaryKey(libraryFileId);
    }

    //**************************************************************************************************
    //***************************************** library_file *********************************************
    //******************************************* byHeader ***********************************************
    //***************************************** String header ********************************************
    //**************************************************************************************************
    private void COMMENT_SECTION_LibraryFileByHeader(String header) {
        
    }

    public LibraryFileExample getLibraryFileByHeaderExample(String header, int offset, int limit) {
        LibraryFileExample example = new LibraryFileExample();
        example.setOffset(offset);
        example.setLimit(limit);
        example.or()
        .andHeaderLike(header)
        ;
        example.setOrderByClause(getOrderByClause());
        return example;
    }

    public int selectLibraryFileCountByHeader(String header) {
        return libraryFileDAO.countByExample(getLibraryFileByHeaderExample(header, 0, 0));
    }

    public List<LibraryFile> selectLibraryFileListByHeader(String header, int offset, int limit) {
        return libraryFileDAO.selectByExample(getLibraryFileByHeaderExample(header, offset, limit));
    }

    public List<LibraryFile> selectLibraryFileListByHeader(String header, List<Long> libraryFileIdList) {
        if (libraryFileIdList == null || libraryFileIdList.isEmpty()) return new ArrayList<LibraryFile>();
        return libraryFileDAO.selectByIdList(libraryFileIdList);
    }
}

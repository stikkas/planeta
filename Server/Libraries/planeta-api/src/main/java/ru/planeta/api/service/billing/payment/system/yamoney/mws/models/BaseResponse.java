package ru.planeta.api.service.billing.payment.system.yamoney.mws.models;

import ru.planeta.api.service.billing.payment.system.yamoney.mws.models.enums.ErrorCode;
import ru.planeta.api.service.billing.payment.system.yamoney.mws.models.enums.RequestStatus;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import java.util.Date;

/**
 * Created by eshevchenko on 25.04.14.
 */
@XmlAccessorType(XmlAccessType.FIELD)
public abstract class BaseResponse {
    @XmlAttribute
    public Long clientOrderId;
    @XmlAttribute
    public RequestStatus status;
    @XmlAttribute
    public ErrorCode error;
    @XmlAttribute
    public Date processedDT;
    @XmlAttribute
    public String techMessage;
}

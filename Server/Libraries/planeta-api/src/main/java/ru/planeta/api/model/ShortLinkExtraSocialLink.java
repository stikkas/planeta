package ru.planeta.api.model;

public class ShortLinkExtraSocialLink {

    private String name;
    private String urlParams;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrlParams() {
        return urlParams;
    }

    public void setUrlParams(String urlParams) {
        this.urlParams = urlParams;
    }
}

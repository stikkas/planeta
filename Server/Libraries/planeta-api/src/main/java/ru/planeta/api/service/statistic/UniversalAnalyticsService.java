package ru.planeta.api.service.statistic;

import ru.planeta.model.common.campaign.enums.CampaignStatus;
import ru.planeta.model.common.campaign.enums.CampaignTargetStatus;

import javax.annotation.Nonnull;

/**
 * User: s.makarov
 * Date: 19.12.13
 * Time: 17:15
 */
public interface UniversalAnalyticsService {

    /**
     * Only for tests
     */
    void setUgauid(String ugauid);

    void trackRegistrationBySocial(long clientId);

    void trackChangeCampaignStatus(long clientId, CampaignStatus oldStatus, CampaignStatus newStatus, String campaignName, CampaignTargetStatus targetStatus);

    void trackExternalLink(long clientId, @Nonnull String url);
}

package ru.planeta.api.model.biblio;

import com.fasterxml.jackson.annotation.JsonIgnore;
import ru.planeta.model.common.Identified;
import ru.planeta.model.profile.Identifier;

public class Partner implements Identified, Identifier {

    @JsonIgnore
    private long id;

    private String originalUrl;

    private String imageUrl;
    private String name;

    public String getOriginalUrl() {
        return originalUrl;
    }

    public void setOriginalUrl(String originalUrl) {
        this.originalUrl = originalUrl;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public long getId() {
        return id;
    }

    @Override
    public void setId(long id) {
        this.id = id;
    }
}

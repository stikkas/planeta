package ru.planeta.api.service.concert;

import org.apache.commons.collections4.IterableUtils;
import org.apache.commons.collections4.Predicate;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.planeta.api.exceptions.MoscowShowInteractionException;
import ru.planeta.api.exceptions.NotFoundException;
import ru.planeta.dao.concertdb.PlaceDAO;
import ru.planeta.model.concert.Place;
import ru.planeta.model.concert.PlaceState;
import ru.planeta.moscowshow.client.MoscowShowClient;
import ru.planeta.moscowshow.model.OrderItem;
import ru.planeta.moscowshow.model.OrderPlaceResult;
import ru.planeta.moscowshow.model.response.ResponsePlaceOrder;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.springframework.context.annotation.Lazy;

/**
 * Created with IntelliJ IDEA.
 * User: michail
 * Date: 17.10.16
 * Time: 12:11
 */

@Service
@Lazy
public class PlaceServiceImpl implements PlaceService {
    private static final Logger logger = Logger.getLogger(PlaceServiceImpl.class);

    @Autowired
    private PlaceDAO placeDAO;

    @Autowired
    private MoscowShowClient moscowShowClient;

    @Override
    public Place getPlaceByExtId(long externalPlaceId) {
        return placeDAO.selectByExternalPlaceId(externalPlaceId);
    }

    @Override
    public Place getPlaceBySectionRowPosition(long sectionId, long rowId, long position) {
        return placeDAO.selectBySectionRowPosition(sectionId, rowId, position);
    }

    @Override
    public int updatePlace(Place place) {
        place.setTimeUpdated(new Date());
        return placeDAO.update(place);
    }

    @Override
    public int insertPlace(Place place) {
        place.setTimeAdded(new Date());
        place.setTimeUpdated(place.getTimeAdded());
        return placeDAO.insert(place);
    }

    @Override
    public int insertOrUpdatePlace(Place place) {
        return place.getPlaceId() > 0 ? updatePlace(place) : insertPlace(place);
    }

    @Override
    public List<Place>  prepareMoscowShowOrder(List<OrderItem> orderItemList) throws NotFoundException, MoscowShowInteractionException {

        ResponsePlaceOrder response = hold(orderItemList);

        List<Place> tickets = new ArrayList<>(orderItemList.size());
        for (OrderPlaceResult orderPlaceResult : response.getResult().getPlaces()) {
            Place place = placeDAO.selectByExternalPlaceId(orderPlaceResult.getPlaceID());
            if (place == null) {
                place = new Place();
                place.setExternalPlaceId(orderPlaceResult.getPlaceID());
            }

            place.setState(PlaceState.RESERVED);
            place.setExternalConcertId(orderPlaceResult.getActionID());
            place.setSectionId(orderPlaceResult.getSectionID());
            place.setRowId(orderPlaceResult.getRowID());
            place.setPosition(orderPlaceResult.getPosition());
            place.setName(orderPlaceResult.getTicketName());
            place.setTicketCode(orderPlaceResult.getTicketCode());
            place.setTicketPdfUrl(orderPlaceResult.getUrl());


            // need to set price for order object - we can find it only in items in input array
            final Place placeForFind = place;
            OrderItem foundOrderItem = IterableUtils.find(orderItemList, new Predicate<OrderItem>() {
                @Override
                public boolean evaluate(OrderItem orderItem) {
                    return (orderItem.getActionID() == placeForFind.getExternalConcertId()
                    && orderItem.getSectionID() == placeForFind.getSectionId()
                    && orderItem.getRowID() == placeForFind.getRowId()
                    && orderItem.getPosition() == placeForFind.getPosition());
                }
            });

            if (foundOrderItem == null) {
                throw  new NotFoundException(OrderItem.class, placeForFind.getPlaceId());
            }

            place.setPrice(foundOrderItem.getPrice());

            if (place.getPlaceId() > 0) {
                updatePlace(place);
            } else {
                insertPlace(place);
            }

            tickets.add(place);
        }
        return tickets;
    }

    @Override
    public ResponsePlaceOrder hold(List<OrderItem> orderItemList) throws NotFoundException, MoscowShowInteractionException {
        ResponsePlaceOrder response = moscowShowClient.placeOrder(orderItemList);
        if (response == null) {
            throw new NotFoundException(ResponsePlaceOrder.class);
        }

        if (response.hasErrors()) {
            throw new MoscowShowInteractionException(response.getErrorMessage());
        }

        return response;
    }

    @Override
    public List<Place> getPlacesByExternalConcertId(long extConcertId) {
        return getPlacesByExternalConcertId(extConcertId, null, null);
    }

    @Override
    public List<Place> getPlacesByExternalConcertId(long extConcertId, Long extSectionId, Long extRowId) {
        return placeDAO.selectByExtConcertId(extConcertId, extSectionId, extRowId);
    }

    @Override
    public int markPlaceDeleted(Place place) {
        place.setDeleted(true);
        return updatePlace(place);
    }

    @Override
    public List<Place> selectReservedPlaces(int offset, int limit) {
        return placeDAO.selectReservedPlaces(offset, limit);
    }
}

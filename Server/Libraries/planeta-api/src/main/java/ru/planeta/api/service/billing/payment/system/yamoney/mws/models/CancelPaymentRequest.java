package ru.planeta.api.service.billing.payment.system.yamoney.mws.models;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Date;

/**
 * Created by eshevchenko on 25.04.14.
 */
@XmlRootElement
public class CancelPaymentRequest extends BaseRequest {
    @XmlAttribute
    public Date requestDT;
    @XmlAttribute
    public Long orderId;

    public static CancelPaymentRequest create(long orderId) {
        CancelPaymentRequest result = new CancelPaymentRequest();
        result.requestDT = new Date();
        result.orderId = orderId;

        return result;
    }
}

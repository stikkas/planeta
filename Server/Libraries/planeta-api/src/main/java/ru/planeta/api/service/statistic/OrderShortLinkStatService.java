package ru.planeta.api.service.statistic;

import ru.planeta.commons.model.OrderShortLinkStat;

public interface OrderShortLinkStatService {
    void add(OrderShortLinkStat orderShortLinkStat);

    OrderShortLinkStat getById(long shortLinkStatsId);
}

package ru.planeta.api.text;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;
import ru.planeta.commons.web.WebUtils;

import java.util.Map;

import static org.springframework.http.HttpHeaders.CONTENT_TYPE;

/**
 * Class that extracts image from url (or null if no image found)
 */
@Component
public class ImageAttachmentExtractor implements AttachmentExtractor {

    private static final int CONNECTION_TIMEOUT = 60000;
    private static final int READ_TIMEOUT = 60000;

    @Override
    public Attachment extract(String url) {
        Attachment attachment = null;
        Map<String, String> headers = WebUtils.headRequest(url, CONNECTION_TIMEOUT, READ_TIMEOUT);

        if (headers != null) {
            String contentType = headers.get(CONTENT_TYPE);
            if (!StringUtils.isEmpty(contentType) && contentType.startsWith("image/")) {
                ImageAttachment imageAttachment = new ImageAttachment();
                imageAttachment.setUrl(url);
                attachment = imageAttachment;
            }
        }
        return attachment;
    }
}

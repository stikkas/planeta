package ru.planeta.api.service.registration;

import ru.planeta.api.mail.MailClient;

public class EmailConfirmationStrategy {

    public EmailConfirmationStrategy(MailClient mailClient, String email) {
        this.mailClient = mailClient;
        this.email = email;
    }

    private MailClient mailClient;
    private String email;

    public String getEmail() {
        return email;
    }

    protected void sendEmail(String password, String regCode) {
        getMailClient().sendRegistrationCompleteEmail(getEmail(), password, regCode);
    }

    public MailClient getMailClient() {
        return mailClient;
    }
}

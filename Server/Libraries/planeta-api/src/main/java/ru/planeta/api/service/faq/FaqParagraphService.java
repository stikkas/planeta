package ru.planeta.api.service.faq;

import ru.planeta.model.commondb.FaqParagraph;

import java.util.List;

/**
 * Created by kostiagn on 01.09.2015.
 */
public interface FaqParagraphService {
    void delete(Long faqArticleId);

    List<FaqParagraph> selectFaqParagraphListByFaqArticle(Long faqArticleId, int offset, int limit);
    List<FaqParagraph> selectFaqParagraphList(int offset, int limit);

    void insertFaqParagraph(FaqParagraph faqParagraph);
    void updateFaqParagraph(FaqParagraph faqParagraph);

    void deleteFaqParagraph(Long faqParagraphId);

    void resort(long idFrom, long idTo, long faqArticleId);
}

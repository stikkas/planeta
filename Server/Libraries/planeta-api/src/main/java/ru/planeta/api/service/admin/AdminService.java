package ru.planeta.api.service.admin;

import ru.planeta.api.aspect.transaction.NonTransactional;
import ru.planeta.api.exceptions.AlreadyExistException;
import ru.planeta.api.exceptions.NotFoundException;
import ru.planeta.api.exceptions.PermissionException;
import ru.planeta.api.search.SearchResult;
import ru.planeta.model.enums.ProfileStatus;
import ru.planeta.model.enums.ProfileType;
import ru.planeta.model.enums.UserStatus;
import ru.planeta.model.profile.ProfileForAdminsWithEmail;

import java.math.BigDecimal;
import java.util.EnumSet;
import java.util.List;

/**
 * Service from admin restriction methods
 *
 * @author ds.kolyshev
 * Date: 17.11.11
 */
public interface AdminService {

    /**
     * Sets specified status for group
     *
     * @param clientId      admin profile identifier
     * @param profileId     group profile identifier
     */
    void setProfileStatus(long clientId, long profileId, ProfileStatus profileStatus) throws NotFoundException, PermissionException;

    SearchResult<ProfileForAdminsWithEmail> getVipUsers(long clientId, int offset, int limit) throws PermissionException;

    SearchResult<ProfileForAdminsWithEmail> getProfiles(long clientId, String searchString, ProfileType type, int offset, int limit) throws PermissionException;

    List<ProfileForAdminsWithEmail> getPlanetaWorkersWithoutSuperAdmins();

    void setUserStatus(long clientId, long profileId, EnumSet<UserStatus> roles) throws NotFoundException, PermissionException;

    String getUserEmail(long clientId, long profileId) throws NotFoundException, PermissionException;

    /**
     * Sets new password for specified user
     *
     */
    void setUserPassword(long clientId, long profileId, String password) throws PermissionException, NotFoundException;

	/**
	 * Sets new email for specified user
	 *
	 */
	void setUserEmail(long clientId, long profileId, String email) throws PermissionException, NotFoundException;

    /**
     * Confirms registration for specified user
     *
     */
    void confirmRegistration(long clientId, long profileId) throws PermissionException, NotFoundException;

    /**
     * Sends new registration complete email for specified user.
     *
     */
    void sendRegistrationCompleteEmail(long clientId, long profileId) throws PermissionException, NotFoundException;

    /**
     * change alias at specific profile without permission check
     *
     */
    void changeAlias(long profileId, String alias) throws AlreadyExistException, NotFoundException;

    // very long method
    @NonTransactional
    void deleteProfile(long clientId, long profileId) throws PermissionException;

    void decreaseBalance(long clientId, long profileId, BigDecimal amount) throws PermissionException;

    void freezeAmount(long clientId, long profileId, BigDecimal amount) throws PermissionException;

    void unfreezeAmount(long clientId, long profileId, BigDecimal amount) throws PermissionException;

    void decreaseFrozenAmount(long clientId, long profileId, BigDecimal amount) throws PermissionException;

    void deleteUserPosts(long clientId, long profileId, int daysCount) throws PermissionException;

    void deleteUserComments(long clientId, long profileId, int daysCount) throws PermissionException;

}

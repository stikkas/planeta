package ru.planeta.api.service.widget;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import ru.planeta.api.exceptions.NotFoundException;
import ru.planeta.api.exceptions.WidgetNotActiveException;
import ru.planeta.api.helper.ProjectService;
import ru.planeta.api.model.widget.AbstractWidgetDTO;
import ru.planeta.api.model.widget.CampaignWidgetDTO;
import ru.planeta.api.model.widget.ShareWidgetDTO;
import ru.planeta.api.model.widget.WidgetColorTheme;
import ru.planeta.api.model.enums.widget.WidgetNames;
import ru.planeta.dao.commondb.CampaignDAO;
import ru.planeta.dao.commondb.ShareDAO;
import ru.planeta.model.common.campaign.Campaign;
import ru.planeta.model.common.campaign.Share;
import ru.planeta.model.common.campaign.enums.ShareStatus;
import ru.planeta.model.enums.ProjectType;

/**
 * Date: 08.08.13
 * Time: 13:01
 */
@Service
public class WidgetServiceImpl implements WidgetService {

    private static final Logger log = Logger.getLogger(WidgetService.class);

    @Value("${static.host}")
    private String staticAppHost;
    private final CampaignDAO campaignDAO;
    private final ShareDAO shareDAO;
    private final ProjectService projectService;

    @Autowired
    public WidgetServiceImpl(CampaignDAO campaignDAO, ShareDAO shareDAO, ProjectService projectsService) {
        this.campaignDAO = campaignDAO;
        this.shareDAO = shareDAO;
        this.projectService = projectsService;
    }

    @Override
    public AbstractWidgetDTO getCampaignWidgetDTO(WidgetNames name, long offerId, long shareId, WidgetColorTheme colorTheme) throws NotFoundException, WidgetNotActiveException {
        final Campaign campaign = campaignDAO.selectCampaignById(offerId);
        if (campaign == null) {
            throw new NotFoundException(Campaign.class, offerId);
        }

        String url = projectService.getUrl(ProjectType.MAIN, "/campaigns/") + campaign.getWebCampaignAlias();


        final AbstractWidgetDTO abstractWidgetDTO;
        if (name == WidgetNames.CAMPAIGN_240X400_WITH_SHARE) {
            final Share share = shareDAO.select(shareId);
            if (share == null) {
                throw new NotFoundException(Share.class, shareId);
            }
            if (share.getCampaignId() != offerId) {
                log.error("Share from another campaign");
                throw new NotFoundException(Share.class, shareId);
            }
            if (share.getShareStatus() != ShareStatus.ACTIVE) {
                throw new WidgetNotActiveException(shareId);
            }
            abstractWidgetDTO = new ShareWidgetDTO(campaign.getName(),
                    colorTheme,
                    url,
                    campaign.getImageUrl(),
                    campaign.getCollectedAmount().intValue(),
                    campaign.getTargetAmount().intValue(),
                    share.getName(),
                    share.getPrice().intValue());
        } else {
            abstractWidgetDTO = new CampaignWidgetDTO(campaign.getName(),
                    colorTheme,
                    url,
                    campaign.getImageUrl(),
                    campaign.getCollectedAmount().intValue(),
                    campaign.getTargetAmount().intValue());
        }

        return abstractWidgetDTO;
    }
}

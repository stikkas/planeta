package ru.planeta.api.service.configurations;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import ru.planeta.api.model.enums.image.ImageType;
import ru.planeta.commons.web.WebUtils;
import ru.planeta.dao.statdb.StatStaticNodeDAO;
import ru.planeta.model.enums.ThumbnailType;
import ru.planeta.model.stat.StatStaticNode;
import ru.planeta.model.stat.StaticNodeFreeSpaceComparator;
import ru.planeta.utils.ThumbConfiguration;

import javax.annotation.Nonnull;
import javax.annotation.PostConstruct;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.*;

/**
 * @author ds.kolyshev
 * Date: 15.09.11
 */
@Service
@Lazy(false)
public class StaticNodesServiceImpl implements StaticNodesService {

    @Autowired
    public StaticNodesServiceImpl(StatStaticNodeDAO statStaticNodeDAO) {
        this.statStaticNodeDAO = statStaticNodeDAO;
    }

    public static Logger getLogger() {
        return logger;
    }

    private static final long STATIC_NODES_UPDATE_DELAY = 60 * 60 * 1000; // once an hour
    private static final Logger logger = Logger.getLogger(StaticNodesServiceImpl.class);
    private static final String PROXY_PARAM = "/p?url=";
    private static final String DEFAULT_IMAGE_DIR = "/images/defaults/${fileName}";
    private static final Map<ThumbnailType, String> DEFAULT_IMAGES = new HashMap<>(5);
    private static final Comparator<StatStaticNode> FREESPACE_COMPARATOR = new StaticNodeFreeSpaceComparator(true);

    private static final String URL_STR = "url=";


    private Set<StatStaticNode> staticNodes;

    private final StatStaticNodeDAO statStaticNodeDAO;

    static {
        DEFAULT_IMAGES.put(ThumbnailType.USER_SMALL_AVATAR, DEFAULT_IMAGE_DIR + "-small-avatar.jpg");
        DEFAULT_IMAGES.put(ThumbnailType.USER_AVATAR, DEFAULT_IMAGE_DIR + "-avatar.jpg");
        DEFAULT_IMAGES.put(ThumbnailType.PHOTOPREVIEW, DEFAULT_IMAGE_DIR + "-photo-preview.jpg");
        DEFAULT_IMAGES.put(ThumbnailType.ALBUM_COVER, DEFAULT_IMAGE_DIR + "-album-cover.jpg");
        DEFAULT_IMAGES.put(ThumbnailType.PRODUCT_COVER_NEW, DEFAULT_IMAGE_DIR + "-product-cover-new.jpg");
        DEFAULT_IMAGES.put(ThumbnailType.SMALL, DEFAULT_IMAGE_DIR + "-small.jpg");
        DEFAULT_IMAGES.put(ThumbnailType.HUGE, DEFAULT_IMAGE_DIR + "-huge.jpg");
        DEFAULT_IMAGES.put(ThumbnailType.ORIGINAL, DEFAULT_IMAGE_DIR + "-original.jpg");
        DEFAULT_IMAGES.put(ThumbnailType.SHARE_COVER, DEFAULT_IMAGE_DIR + "-small.jpg");
    }

    @Value("${static.host}")
    private String staticHost;

    @Value("${server.path}")
    private String defaultStaticNode;

    /**
     * Updates registered static nodes from db
     */
    @PostConstruct
    synchronized void init() {
        this.staticNodes = Collections.singleton(getDefaultStatStaticNode());
    }

    @Scheduled(fixedRate = STATIC_NODES_UPDATE_DELAY)
    synchronized public void updateStaticNodes() {
        TreeSet<StatStaticNode> staticNodeSet = new TreeSet(FREESPACE_COMPARATOR);
        statStaticNodeDAO.findAll().forEach(node -> staticNodeSet.add(node));
        // DB is empty, using default node instead
        if (staticNodeSet.isEmpty()) {
            addDefaultNode(staticNodeSet);
        }

        this.staticNodes = staticNodeSet;
    }

    private void addDefaultNode(Set<StatStaticNode> staticNodeSet) {
        StatStaticNode staticNode = getDefaultStatStaticNode();
        staticNodeSet.add(staticNode);
    }

    @Nonnull
    private StatStaticNode getDefaultStatStaticNode() {
        StatStaticNode staticNode = new StatStaticNode();
        staticNode.setDomain(defaultStaticNode);
        staticNode.setTimeUpdated(new Date());
        staticNode.setFreeSpaceKB(Integer.MAX_VALUE);
        return staticNode;
    }

    /**
     * @return random static node
     */
    @Override
    public synchronized String getStaticNode() {
        return staticNodes.iterator().next().getDomain();
    }

    @Override
    public synchronized List<String> getStaticNodes() {
        Set<StatStaticNode> staticNodesSet = staticNodes;
        List<String> result = new ArrayList<>(staticNodesSet.size());
        for (StatStaticNode statStaticNode : staticNodesSet) {
            result.add(statStaticNode.getDomain());
        }
        return result;
    }

    @Override
    public String getThumbnailUrl(String url, ThumbnailType thumbnailType, ImageType imageType) {
        if (StringUtils.isEmpty(url)) {
            return getDefaultThumbnailUrl(thumbnailType, imageType);
        }

        if (isProxyServerPath(url)) {
            return wrapImageUrl(getUrlFromProxy(url), new ThumbConfiguration(thumbnailType));
        } else if (isStaticServerPath(url)) {
            return WebUtils.appendProtocolIfNecessary(ThumbnailType.Companion.getUrlByThumbnailType(url, thumbnailType), true);
        } else {
            return wrapImageUrl(url, new ThumbConfiguration(thumbnailType));
        }
    }

    private String getUrlFromProxy(String proxiedUrl) {
        if (!isProxyServerPath(proxiedUrl)) {
            return proxiedUrl;  // URL is not proxied
        }
        int urlStartPos = proxiedUrl.indexOf(URL_STR) + URL_STR.length();
        if (urlStartPos < 0) {
            throw new RuntimeException("Bad proxied url=" + proxiedUrl + " query string parameter url=... wasn't found.");
        }
        int urlEndPos = proxiedUrl.indexOf("&", urlStartPos);
        final String url;
        if (urlEndPos > 0) {
            url = proxiedUrl.substring(urlStartPos, urlEndPos);
        } else {
            url = proxiedUrl;
        }

        final String decodedUrl;
        try {
            decodedUrl = URLDecoder.decode(url, "UTF-8");
            return decodedUrl;
        } catch (UnsupportedEncodingException e) {
            // should never happen
            logger.error(e);
            return url;
        }
    }

    /**
     * Checks url to be on static node
     */
    @Override
    public boolean isStaticServerPath(String url) {
        return extractStaticNode(url) != null;
    }

    private synchronized StatStaticNode extractStaticNode(String url) {
        for (StatStaticNode node : staticNodes) {
            int serverPathIndex = WebUtils.getHost(url).indexOf(node.getDomain());
            if (serverPathIndex >= 0) {
                return node;
            }
        }
        return null;
    }


    @Override
    public String getStaticNodeOrCurrent(String url) {
        final StatStaticNode statStaticNode = extractStaticNode(url);
        if (statStaticNode == null) {
            return getStaticNode();
        }
        return statStaticNode.getDomain();
    }

    /**
     * Checks url to be under the proxy server
     */
    private synchronized boolean isProxyServerPath(String url) {
        if (StringUtils.isEmpty(url)) {
            return false;
        }
        for (StatStaticNode staticNode : staticNodes) {
            if (url.contains(staticNode.getDomain() + PROXY_PARAM)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public String wrapImageUrl(String url, ThumbConfiguration config) {
        return wrapImageUrl(url, config, false);
    }

    @Override
    public String wrapImageUrl(String url, ThumbConfiguration config, boolean force) {

        if (!force && isStaticServerPath(url)) {
            return url;
        }

        StringBuilder sb = new StringBuilder();
        sb.append(getStaticNode());

        sb.append(PROXY_PARAM);
        try {
            sb.append(URLEncoder.encode(url, "UTF-8"));
        } catch (UnsupportedEncodingException e) {
            // should never happen
            getLogger().error(e);
        }
        sb.append("&width=");
        sb.append(config.getWidth());
        sb.append("&height=");
        sb.append(config.getHeight());
        if (config.isCrop()) {
            sb.append("&crop=true");
        }
        if (config.isPad()) {
            sb.append("&pad=true");
        }
        if (config.isDisableAnimatedGif()) {
            sb.append("&disableAnimatedGif=true");
        }
        return WebUtils.appendProtocolIfNecessary(sb.toString(), true);
    }

    @Override
    public String getResourceHost() {
        return staticHost;
    }

    private String getDefaultThumbnailUrl(ThumbnailType thumbnailType, ImageType imageType) {
        String defaultImagePath = DEFAULT_IMAGES.get(thumbnailType);
        if (defaultImagePath == null) {
            logger.error("Ask default image for " + thumbnailType + " " + ExceptionUtils.getStackTrace(new Throwable()));
            defaultImagePath = DEFAULT_IMAGES.get(ThumbnailType.USER_AVATAR);
        }
        return WebUtils.appendProtocolIfNecessary(staticHost, true) + defaultImagePath.replace("${fileName}", imageType.getName());
    }

}

package ru.planeta.api.service.statistic;

import ru.planeta.api.exceptions.PermissionException;
import ru.planeta.api.model.stat.PurchaseReport;
import ru.planeta.api.model.DateRange;
import ru.planeta.model.enums.RotationPeriod;
import ru.planeta.model.stat.*;

import java.util.Date;
import java.util.List;
import java.util.NavigableMap;

/**
 * Service from admin SEO
 *
 * @author is.kuzminov
 * Date: 19.07.12
 */


public interface AdminStatisticService {

    NavigableMap<Date, PurchaseReport> getStatPurchaseCompositeReport(long clientId, Date dateFrom, Date dateTo, String step) throws PermissionException;

    List<StatRegistration> getRegistrationsReport(long clientId, Date dateFrom, Date dateTo) throws PermissionException;

    List<StatPurchaseShare> getStatPurchaseShareReport(long clientId, Date dateFrom, Date dateTo) throws PermissionException;

    List<StatPurchaseShare> getStatPurchaseShareReportWithoutDiff(long clientId, Date dateFrom, Date dateTo) throws PermissionException;

    List<StatPurchaseProduct> getStatPurchaseProductReport(long clientId, Date dateFrom, Date dateTo) throws PermissionException;

    List<StatCampaignsForecast> getCampaignsForecast(long clientId) throws PermissionException;

    <T extends StatPurchaseCommon> void setDiff(List<T> current, List<T> previous);

    DateRange getPreviousRange(Date dateFrom, Date dateTo);

    Date getDateTo(Date dateFrom, RotationPeriod step) throws PermissionException;

}

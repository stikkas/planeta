package ru.planeta.api.service.concert;

import ru.planeta.model.concert.Hall;
import ru.planeta.model.concert.Place;

/**
 * Created with IntelliJ IDEA.
 * User: michail
 * Date: 18.10.16
 * Time: 12:15
 */
public interface HallService {
    Hall getHall(long hallId);

    Hall getHallByTitle(String title);

    int insertOrUpdateHall(Hall hall);
}

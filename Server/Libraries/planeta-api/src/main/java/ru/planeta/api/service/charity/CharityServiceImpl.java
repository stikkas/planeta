package ru.planeta.api.service.charity;

/*
 * Created by Alexey on 24.06.2016.
 */

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.planeta.dao.charitydb.CharityDAO;
import ru.planeta.model.charity.Member;

import java.util.List;

@Service
public class CharityServiceImpl implements CharityService {
    private final CharityDAO charityDAO;

    @Autowired
    public CharityServiceImpl(CharityDAO charityDAO) {
        this.charityDAO = charityDAO;
    }

    @Override
    public List<Member> getAllMembers(int offset, int limit) {
        return charityDAO.selectAllMembers(offset, limit);
    }
}

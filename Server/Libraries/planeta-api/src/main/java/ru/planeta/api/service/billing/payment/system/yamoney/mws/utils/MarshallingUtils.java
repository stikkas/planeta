package ru.planeta.api.service.billing.payment.system.yamoney.mws.utils;

import ru.planeta.api.service.billing.payment.system.yamoney.mws.exceptions.ResponseParsingException;
import org.apache.http.Consts;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.stream.XMLStreamException;
import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * XML marshalling utility class.<br>
 * Created by eshevchenko.
 */
public final class MarshallingUtils {

    private static final String XML_HEADER = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";

    private MarshallingUtils() {
    }

    public static byte[] marshall(Object source) throws JAXBException, XMLStreamException, IOException {
        JAXBContext context = JAXBContext.newInstance(source.getClass());
        Marshaller marshaller = context.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FRAGMENT, true);
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        Writer writer = new OutputStreamWriter(stream, Consts.UTF_8);
        try {
            // xml header <?xml ... standalone="yes"?> workaround
            writer.write(XML_HEADER);
            marshaller.marshal(source, writer);

            return stream.toByteArray();
        } finally {
            writer.close();
        }
    }

    public static <T> T unmarshal(Class<T> resultClass, byte[] xmlAsBytes) throws ResponseParsingException {
        try {
            JAXBContext context = JAXBContext.newInstance(resultClass);
            Unmarshaller unmarshaller = context.createUnmarshaller();
            return (T) unmarshaller.unmarshal(new ByteArrayInputStream(xmlAsBytes));
        } catch (Exception e) {
            throw new ResponseParsingException(e);
        }
    }

    public static <T> T unmarshal(Class<T> resultClass, String xmlString) throws ResponseParsingException {
        try {
            JAXBContext context = JAXBContext.newInstance(resultClass);
            Unmarshaller unmarshaller = context.createUnmarshaller();
            Reader reader = new StringReader(xmlString);
            return (T) unmarshaller.unmarshal(reader);
        } catch (Exception e) {
            throw new ResponseParsingException(e);
        }
    }



    private static final String ISO8601_2004 = "yyyy-MM-dd'T'HH:mm:ss.SZ";

    public static String formatToISO8601(Date date) {
        String result = null;
        if (date != null) {
            String s = new SimpleDateFormat(ISO8601_2004).format(date);
            result = new StringBuilder(s).insert(s.length() - 2, ':').toString();
        }

        return result;
    }
}

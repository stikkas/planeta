package ru.planeta.api.service.concert;

import ru.planeta.api.exceptions.MoscowShowInteractionException;
import ru.planeta.api.exceptions.NotFoundException;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: michail
 * Date: 31.10.2016
 * Time: 13:31
 */
public interface Mergeable<TInternal, TExternal> {
    int compare(TInternal internal, TExternal external);
    void insert(TExternal external) throws NotFoundException, MoscowShowInteractionException;
    void update(TInternal internal, TExternal external) throws NotFoundException, MoscowShowInteractionException;
    void delete(TInternal internal);
    List<TInternal> getInternalList();
    List<TExternal> getExternalList();
}

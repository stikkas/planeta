package ru.planeta.api.model.promo;

/**
 * User: sshendyapin
 * Date: 04.02.13
 * Time: 16:24
 */
public class ShopPromoConfiguration extends BasePromoConfiguration {


    private String imageUrl;
    private String link;
    private String html;

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getHtml() {
        return html;
    }

    public void setHtml(String html) {
        this.html = html;
    }
}

package ru.planeta.api.service.statistic;

import com.brsanthu.googleanalytics.DefaultRequest;
import com.brsanthu.googleanalytics.EventHit;
import com.brsanthu.googleanalytics.GoogleAnalytics;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import ru.planeta.model.common.campaign.enums.CampaignStatus;
import ru.planeta.model.common.campaign.enums.CampaignTargetStatus;

import javax.annotation.Nonnull;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * User: s.makarov
 * Date: 19.12.13
 * Time: 17:15
 */
@Service
public class UniversalAnalyticsServiceImpl implements UniversalAnalyticsService {
    @Value("${universal.analytics.ugauid}")
    private String ugauid;

    private ExecutorService executorService = Executors.newSingleThreadExecutor();

    private static final Logger LOG = Logger.getLogger(UniversalAnalyticsServiceImpl.class);


    public enum GAEventCategory {
        //use toString() method for tracking event to google analytics
        USERS_ACTIONS("users_actions");

        public final String name;

        private GAEventCategory(String name) {
            this.name = name;
        }

        public String toString() {
            return name().toLowerCase();
        }
    }

    private enum GAEventAction {
        //use toString() method for tracking event to google analytics
        SUCCESSFUL_REGISTRATION("successful_registration");

        public final String name;

        private GAEventAction(String name) {
            this.name = name;
        }

        public String toString() {
            return name().toLowerCase();
        }
    }

    private enum GAEventLabel {
        //use toString() method for tracking event to google analytics
        SOCIAL_REGISTRATION("social_registration");

        public final String name;

        private GAEventLabel(String name) {
            this.name = name;
        }

        public String toString() {
            return name().toLowerCase();
        }
    }

    private GoogleAnalytics getUniversalAnalytics(final String userId) {
        if (StringUtils.isEmpty(ugauid)) {
            return null;
        }

        GoogleAnalytics ga = new GoogleAnalytics(ugauid);
        DefaultRequest request = ga.getDefaultRequest();
        request.userId(userId);
        ga.setDefaultRequest(request);

        return ga;
    }

    /**
     * Only for tests
     *
     */
    @Override
    public void setUgauid(String ugauid) {
        this.ugauid = ugauid;
    }

    private void trackEvent(GoogleAnalytics ga,
                            @Nonnull String category,
                            @Nonnull String action,
                            @Nonnull String label) {
        EventHit eventHit = new EventHit();

        eventHit.trackingId(ugauid);
        eventHit.eventCategory(category);
        eventHit.eventAction(action);
        eventHit.eventLabel(label);

        ga.post(eventHit);
    }

    @Override
    public void trackRegistrationBySocial(long clientId) {
        final String userId = String.valueOf(clientId);
        Runnable trackRegistrationBySocialTask = new Runnable() {
            @Override
            public void run() {
                GoogleAnalytics universalAnalytics = getUniversalAnalytics(userId);

                if (universalAnalytics != null) {
                    try {
                        trackEvent(universalAnalytics,
                                GAEventCategory.USERS_ACTIONS.toString(),
                                GAEventAction.SUCCESSFUL_REGISTRATION.toString(),
                                GAEventLabel.SOCIAL_REGISTRATION.toString());
                    } catch (Exception ex) {
                        LOG.error("Failed to track registration by social event to UniversalAnalytics: ", ex);
                    }
                } else {
                    LOG.error("UniversalAnalytics object is null.");
                }
            }
        };
        executorService.execute(trackRegistrationBySocialTask);
    }

    private void trackEventAsync(long clientId, final String category, final String action, final String label) {
        final String userId = String.valueOf(clientId);
        Runnable task = new Runnable() {
            @Override
            public void run() {
                GoogleAnalytics universalAnalytics = getUniversalAnalytics(userId);

                if (universalAnalytics != null) {
                    try {
                        trackEvent(universalAnalytics, category, action, label);
                    } catch (Exception ex) {
                        LOG.error(String.format("Failed to track event (category='%s', action='%s', label='%s') to UniversalAnalytics: ", category, action, label), ex);
                    }
                } else {
                    LOG.error("UniversalAnalytics object is null.");
                }
            }
        };
        executorService.execute(task);
    }

    @Override
    public void trackChangeCampaignStatus(long clientId, CampaignStatus oldStatus, CampaignStatus newStatus, String campaignName, CampaignTargetStatus targetStatus) {
        if (newStatus == CampaignStatus.NOT_STARTED && oldStatus == CampaignStatus.DRAFT) {
            trackEventAsync(clientId, "project-creation", "campaign_on_moderation", campaignName);
        } else if (newStatus == CampaignStatus.DECLINED) {
            trackEventAsync(clientId, "project-creation", "campaign_declined", campaignName);
        } else if (newStatus == CampaignStatus.ACTIVE && oldStatus != CampaignStatus.PAUSED && oldStatus != CampaignStatus.FINISHED) {
            trackEventAsync(clientId, "project-creation", "campaign_active", campaignName);
        } else if (newStatus == CampaignStatus.FINISHED && targetStatus == CampaignTargetStatus.FAIL) {
            trackEventAsync(clientId, "project-creation", "campaign_finished_fail", campaignName);
        } else if (newStatus == CampaignStatus.FINISHED && targetStatus == CampaignTargetStatus.SUCCESS) {
            trackEventAsync(clientId, "project-creation", "campaign_finished_success", campaignName);
        }
    }
    
    @Override
    public void trackExternalLink(long clientId, @Nonnull String url) {
        trackEventAsync(clientId, "External link", "click", url);
    }
}

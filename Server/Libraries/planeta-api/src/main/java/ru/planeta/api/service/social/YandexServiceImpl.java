package ru.planeta.api.service.social;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import ru.planeta.api.aspect.transaction.NonTransactional;
import ru.planeta.api.model.ExternalUserInfo;
import ru.planeta.commons.model.Gender;
import ru.planeta.commons.web.RegistrationData;
import ru.planeta.commons.web.WebUtils;
import ru.planeta.model.common.auth.CredentialType;
import ru.planeta.model.common.auth.ExternalAuthentication;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Date: 19.09.12
 *
 * @author s.kalmykov
 */
@Service
public class YandexServiceImpl implements OAuthClientService {

    private String APP_ID;
    @Value("${oauth.yandex.appId}")
    public void setAppId(String appId) {
        this.APP_ID = appId;
    }

    private String SECRET;
    @Value("${oauth.yandex.secret}")
    public void setSecret(String secret) {
        this.SECRET = secret;
    }

    private static final Logger logger = Logger.getLogger(YandexServiceImpl.class);

    private String secretPostData(String code) {
        Map<String, String> query = new HashMap<String, String>();
        query.put("grant_type", "authorization_code");
        query.put("client_id", APP_ID);
        query.put("client_secret", SECRET);
        query.put("code", code);
        return WebUtils.tryUrlEncodeMap(query, "UTF-8");
    }


    private static String getDataUrl(String accessToken) {
        Map<String, String> query = new HashMap<String, String>();
        query.put("oauth_token", accessToken);
        query.put("format", "json");
        String queryString = WebUtils.tryUrlEncodeMap(query, "UTF-8");
        return "https://login.yandex.ru/info?" + queryString;
    }


    private static ExternalUserInfo parseJSON(String userInfoJSON) {
        try {
            Map<String, Object> response = new ObjectMapper().readValue(userInfoJSON, HashMap.class);
            String firstName = (String) response.get("display_name");
            String lastName = "";
            String email = (String) response.get("default_email");
            String uid = (String) (response.get("id"));
            String gender = (String) (response.get("sex"));
            String birthday = (String) (response.get("birthday"));
            RegistrationData registrationData = new RegistrationData();
            registrationData.setFirstName(firstName);
            registrationData.setLastName(lastName);
            registrationData.setGender("female".equals(gender) ? Gender.FEMALE : Gender.MALE);
            try {
                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
                if (birthday != null) {
                    Date birthDate = format.parse(birthday);
                    registrationData.setUserBirthDate(birthDate);
                }
            } catch (ParseException e) {}

            ExternalAuthentication externalAuthentication = new ExternalAuthentication(uid, CredentialType.YANDEX);

            ExternalUserInfo externalUserInfo = new ExternalUserInfo();
            externalUserInfo.setEmail(email);
            externalUserInfo.setExternalAuthentication(externalAuthentication);
            externalUserInfo.setRegistrationData(registrationData);
            return externalUserInfo;
        } catch (IOException e) {
            logger.error("Malformed json in Yandex response " + userInfoJSON, e);
        }
        return null;
    }


    @Override
    public String getRedirectUrl(String saveRedirectUrl) {
        return "https://oauth.yandex.ru/authorize?response_type=code&client_id=" + APP_ID;
    }

    @Nullable
    @Override
    @NonTransactional
    public ExternalUserInfo authorizeExternal(String code, String redirectUrl) throws IOException {
        String postData = secretPostData(code);
        String yandexJSON = WebUtils.uploadString("https://oauth.yandex.ru/token", postData);
        HashMap<String, Object> object = (new ObjectMapper()).readValue(yandexJSON, HashMap.class);
        String accessToken = (String) object.get("access_token");
        String userInfoJSON = WebUtils.downloadString(this.getDataUrl(accessToken));
        return parseJSON(userInfoJSON);
    }

    @Override
    public boolean isMyUrl(@Nonnull String url) {
        return url.contains("ya");
    }

}


package ru.planeta.api.utils;

import org.apache.log4j.Logger;

import javax.annotation.Nonnull;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Created with IntelliJ IDEA.
 * User: a.volkov
 * Date: 22.08.14
 * Time: 15:17
 */
public final class Locker<T> {

    private static final Logger log = Logger.getLogger(Locker.class);

    private final Map<T, ReentrantLock> locks = new HashMap<T, ReentrantLock>();
    private final ReentrantLock sync = new ReentrantLock();

    private String name = "object";

    public Locker() {
    }

    public Locker(String name) {
        this.name = name;
    }

    public void lock(@Nonnull T id) {
        ReentrantLock lock;
        sync.lock();
        try {
            lock = locks.get(id);
            if (lock == null) {
                lock = new ReentrantLock();
                locks.put(id, lock);

            }
        } finally {
            sync.unlock();
        }
        lock.lock();
        log.debug(name + " #" + id + " locked");
    }

    public void unlock(@Nonnull T id) {
        log.debug(name + " #" + id + " try unlock");
        ReentrantLock lock;
        sync.lock();
        try {
            lock = locks.get(id);
            if (lock != null && !lock.hasQueuedThreads()) {
                locks.remove(id);
            }
        } finally {
            sync.unlock();
        }

        if (lock != null) {
            lock.unlock();
            log.debug(name + " #" + id + " unlocked");
        }
    }
}

package ru.planeta.api.service.security;

import ru.planeta.api.exceptions.PermissionException;
import ru.planeta.model.enums.BlockSettingType;
import ru.planeta.model.enums.ProfileRelationStatus;
import ru.planeta.model.enums.UserStatus;
import ru.planeta.model.profile.ProfileObject;
import ru.planeta.model.profile.media.Video;

import javax.annotation.Nonnull;
import java.util.EnumSet;

/**
 * Service contains methods for checking permission to perform different actions.
 * Some methods, whose names start with 'check' have almost similar methods, whose names start with 'verify'.
 * 'check' methods return true/false, whereas 'verify' methods throw PermissionException
 * 'verify' methods also perform some additional permission checks, so see implementations for more details
 */
public interface PermissionService {

    long ANONYMOUS_USER_PROFILE_ID = -1;

    /**
     * Checks if the specified client is
     * admin of profile
     * or admin of application
     * or clientId == profileId.
     */
    boolean isAdmin(long clientId, long profileId);

    void checkIsAdmin(long clientId, long profileId) throws PermissionException;

    /**
     * Checks if profile (profileId) -- administrator of the group (groupProfileId)
     */
    boolean isProfileAdmin(long profileId, long groupProfileId);

    /**
     * Checks if profile (profileId) -- moderator of the group (groupProfileId)
     */
    boolean isGroupModerator(long profileId, long groupProfileId);

    /**
     * Checks if client profile can view the objects of the specified block type
     */
    boolean checkViewPermission(long clientId, long profileId, BlockSettingType blockSettingType);

    /**
     * Checks if client has write permission for the specified block type owned by profile(profileId)
     */
    boolean checkWritePermission(long clientId, long profileId, BlockSettingType blockSettingType);

    /**
     * Checks if client has add permission for the specified block type owned by profile(profileId)
     */
    boolean checkAddPermission(long clientId, long profileId, BlockSettingType blockSettingType);


    /**
     * Checks if client (clientId) has permission to view video on profile (profileId)
     */
    boolean checkVideoViewPermission(long clientId, long profileId, Video video);

    /**
     * Checks if client (clientId) has permission to comment
     */
    boolean hasNoComment(long clientId);

    /**
     * Checks if client profile (clientId) can deleteByProfileId the specified comment
     */
    boolean isObjectOwnerOrAuthor(long clientId, @Nonnull ProfileObject comment);

    /**
     * Checks if client has permission to send message to specified profile
     */
    boolean checkSendMessagePermission(long clientId, long profileId);

    void checkSendMessagePermission(long clientId) throws PermissionException;

    /**
     * Returns relation status between two profiles
     */
    EnumSet<ProfileRelationStatus> getProfileRelationStatus(long clientId, long profileId);

    /**
     * Checks if user is in specific role.
     *
     * @param clientId user, whose role needs to be checked
     * @param status   to check against
     */
    boolean isUserInRole(long clientId, UserStatus status);

    /**
     * Check if user is in one of roles: SUPER_ADMIN, ADMIN or MANAGER
     *
     * @param clientId client id
     * @return true/false, depending on whether client with specified id is in role
     */
    boolean hasAdministrativeRole(long clientId);

    void checkAdministrativeRole(long clientId) throws PermissionException;

    /**
     * check if user global admin
     *
     * @param clientId client id
     */
    boolean isPlanetaAdmin(long clientId);

    /**
     * check if user is super admin
     *
     * @param clientId client id
     */
    boolean isSuperAdmin(long clientId);

    boolean isAnonymous(long profileId);

    boolean isModerator(long clientId);

    /**
     * Toggles existing subscription (from profileId to subjectProfileId) admin key
     *
     * @param clientId         - who change relationship
     * @param profileId        - admin
     * @param subjectProfileId - profile under administration
     * @param isAdmin          - admin key
     */
    void toggleAdminBySubscription(long clientId, long profileId, long subjectProfileId, boolean isAdmin) throws PermissionException;
}

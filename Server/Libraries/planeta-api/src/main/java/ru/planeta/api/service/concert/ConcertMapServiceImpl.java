package ru.planeta.api.service.concert;

import org.apache.commons.lang3.StringUtils;
import org.apache.http.client.utils.URIBuilder;
import org.apache.log4j.Logger;
import ru.planeta.model.common.Address;

import java.util.Arrays;

/**
 * User: s.makarov
 * Date: 03.02.14
 * Time: 12:04
 */
public class ConcertMapServiceImpl {

    private static final Logger log = Logger.getLogger(ConcertMapServiceImpl.class);

    public static String generateYandexMapUrl(Address address, double longitude, double latitude) {
        try {
            String text = StringUtils.join(Arrays.asList(address.getZipCode(), address.getCountry(), address.getCity(), address.getStreet()), ", ");
            String ll = longitude + "," + latitude;
            URIBuilder uriBuilder = new URIBuilder("http://maps.yandex.ru/?kind=street&ol=geo&z=17&l=map");
            uriBuilder.addParameter("spn", ll);
            uriBuilder.addParameter("sll", ll);
            uriBuilder.addParameter("ll", ll);
            uriBuilder.addParameter("text", text);
            return uriBuilder.toString();
        } catch (Exception e) {
            log.error("YandexMapUrl generation failed: ", e);
            return "";
        }
    }

}

package ru.planeta.api.utils;

import org.apache.log4j.Logger;
import org.springframework.context.MessageSource;
import org.springframework.context.NoSuchMessageException;
import java.text.DateFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created with IntelliJ IDEA.
 * User: user
 * Date: 18.10.13
 * Time: 13:04
 * To change this template use File | Settings | File Templates.
 */
public class DateUtilsRus {

    private MessageSource messageSource;
    private static final Logger LOG = Logger.getLogger(DateUtilsRus.class);

    public DateUtilsRus(MessageSource messageSource) {
        this.messageSource = messageSource;
    }

    public String dateFormat(Date date) {
        return dateFormat(date, "d MMMM yyyy " + getCustomRusSeparator() + " H:mm");
    }

    public String dateFormat(Date date, String pattern) {
        String result = "";
        DateFormatSymbols myDateFormatSymbols = new DateFormatSymbols() {

            @Override
            public String[] getMonths() {
                return getMessage("list.months").split("\\|");
            }

            @Override
            public String[] getShortMonths() {
                return getMessage("list.months.short").split("\\|");
            }
        };
        try {
            return (date == null) || (pattern == null) ? result : new SimpleDateFormat(pattern, myDateFormatSymbols).format(date);
        } catch (Exception e) {
            LOG.error("Error calling dateFormat function", e);
            return result;
        }
    }

    String getCustomRusSeparator() {
        String customSeparator = "";
        try {
            customSeparator = getMessage("date.format.separator.year") + " " +  getMessage("date.format.separator");
        } catch (NoSuchMessageException ex) {
            LOG.error(ex);
        }
        return customSeparator;
    }

    private String getMessage(String messageCode) {
        return messageSource.getMessage(messageCode, null, Locale.getDefault());
    }
}

package ru.planeta.api.model;

/**
 *
 * Created by a.savanovich on 12.08.2015.
 */
public class CleverConfigurationType {
    public static final String CLEVER_CAMPAIGNS = "clever.campaigns.numbers";
    public static final String CLEVER_PRODUCTS = "clever.products.numbers";
    public static final String CLEVER_PROJECT_OF_MONTH = "clever.project.of.month";
    public static final String CLEVER_BOOK_OF_MONTH = "clever.book.of.month";
    public static final String CLEVER_EVENT_OF_MONTH = "clever.event.of.month";
}

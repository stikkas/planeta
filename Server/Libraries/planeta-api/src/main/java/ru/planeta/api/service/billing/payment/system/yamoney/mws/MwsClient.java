package ru.planeta.api.service.billing.payment.system.yamoney.mws;

import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.core.io.Resource;
import ru.planeta.api.service.billing.payment.system.yamoney.mws.exceptions.InitializationException;
import ru.planeta.api.service.billing.payment.system.yamoney.mws.exceptions.YaMoException;
import ru.planeta.api.service.billing.payment.system.yamoney.mws.models.ListOrdersResponse;
import ru.planeta.api.service.billing.payment.system.yamoney.mws.models.RepeatCardPaymentResponse;
import ru.planeta.api.service.billing.payment.system.yamoney.mws.operations.InfoOperation;
import ru.planeta.api.service.billing.payment.system.yamoney.mws.utils.SecurityUtils;

import java.math.BigDecimal;
import java.security.KeyStore;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by eshevchenko.
 */
public class MwsClient {

    public static class Builder {

        private final String serviceUrl;
        private final Integer communicationTimeout;
        private final Map<Long, MwsAccount> accounts = new HashMap<>();
        private final KeyStore trustStore;
        private Logger logger;

        Builder(String serviceUrl, Integer communicationTimeout, KeyStore trustStore) throws InitializationException {
            if (StringUtils.isBlank(serviceUrl)) {
                throw new InitializationException("Can't build Merchant WS client with empty service URL.");
            }
            if (trustStore == null) {
                throw new InitializationException("Can't build Merchant WS client with empty trust store.");
            }

            this.trustStore = trustStore;
            this.serviceUrl = serviceUrl;
            this.communicationTimeout = communicationTimeout;
        }

        public Builder registerAccount(Long shopId, Resource keyStore, String password) throws YaMoException {
            accounts.put(shopId, new MwsAccount(shopId, keyStore, password, trustStore));
            return this;
        }

        public Builder setLogger(Logger logger) {
            this.logger = logger;
            return this;
        }

        public MwsClient build() throws YaMoException {
            MwsClient mwsClient = new MwsClient(serviceUrl, communicationTimeout, accounts);
            mwsClient.logger.info("created " + mwsClient);
            return mwsClient;
        }
    }

    private final Map<Long, MwsAccount> accounts;
    private final String serviceUrl;
    private final int communicationTimeout;
    private final Logger logger = Logger.getLogger(MwsClient.class);

    private MwsClient(String serviceUrl, Integer communicationTimeout, Map<Long, MwsAccount> accounts) throws InitializationException {
        if (StringUtils.isBlank(serviceUrl)) {
            throw new InitializationException("Undefined MWS service URL.");
        }
        if (MapUtils.isEmpty(accounts)) {
            throw new InitializationException("No any account.");
        }

        this.accounts = Collections.unmodifiableMap(accounts);
        this.serviceUrl = serviceUrl;
        if (communicationTimeout == null) {
            this.communicationTimeout = 5000;
        } else {
            this.communicationTimeout = communicationTimeout;
        }
    }

    private MwsAccount getAccount(Long shopId) throws YaMoException {
        MwsAccount result = accounts.get(shopId);
        if (result == null) {
            throw new YaMoException("Unregistered account shopId[%s].", shopId);
        }
        return result;
    }

    public boolean isAccountRegistered(Long shopId) {
        return accounts.containsKey(shopId);
    }

    public static Builder createFor(String serviceUrl, Integer communicationTimeout, String trustStorePath, String trustStorePassword) throws YaMoException {
        try {
            KeyStore trustStore = SecurityUtils.loadKeyStore("JKS", trustStorePath, trustStorePassword.toCharArray());
            return new Builder(serviceUrl, communicationTimeout, trustStore);
        } catch (Exception e) {
            throw new InitializationException("Can't create Merchant WS client: %s", e.getMessage());
        }
    }

    public ListOrdersResponse listOrdersByInvoiceId(Long shopId, long invoiceId) throws YaMoException {
        return InfoOperation.listOrders(getAccount(shopId), invoiceId, null, null, null, null, null, null)
                .execute(serviceUrl, communicationTimeout, logger);
    }

    public RepeatCardPaymentResponse repeatCardPayment(long shopId, long clientOrderId, long invoiceId, BigDecimal amount, String orderNumber, String cvv) throws YaMoException {
        return InfoOperation.repeatCardPayment(getAccount(shopId), clientOrderId, invoiceId, amount, orderNumber, cvv)
                .execute(serviceUrl, communicationTimeout, logger);
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder("YandexMoney Merchant Web Service client")
                .append("\n\tMerchant WS URL: ").append(serviceUrl)
                .append("\n\tRegistered accounts:");

        for (MwsAccount account : accounts.values()) {
            builder.append("\n\t\t").append(account);
        }

        return builder.toString();
    }
}

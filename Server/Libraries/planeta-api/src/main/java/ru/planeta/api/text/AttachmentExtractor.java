package ru.planeta.api.text;

import javax.annotation.Nullable;

/**
 * Interface for classes that extracts attachments from the specified url.
 * It could extract youtube video or image or audio.
 */
public interface AttachmentExtractor {

    @Nullable
    Attachment extract(String url);

}

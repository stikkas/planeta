package ru.planeta.api.service.interactive;


import ru.planeta.api.exceptions.PermissionException;
import ru.planeta.model.trashcan.InteractiveEditorData;

public interface InteractiveEditorDataService {
    void insert(InteractiveEditorData interactiveEditorData);

    InteractiveEditorData getByUserId(long userId);

    InteractiveEditorData getByEmail(String email);

    void update(InteractiveEditorData interactiveEditorData);

    void deleteByUserId(long clientId, long userId) throws PermissionException;
}

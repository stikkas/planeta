package ru.planeta.api.model.enums.image;

/**
 * User: atropnikov
 * Date: 05.04.12
 * Time: 13:49
 */
public enum ImageType {

    USER("user-male"),
    USER_FEMALE("user-female"),
    GROUP("group"),
    EVENT("event"),
    AUDIO("audio"),
    VIDEO("video"),
    PHOTO("photo"),
    PRODUCT("product"),
    BROADCAST("broadcast"),
    DONATE("donate");

    private ImageType(String name) {
        this.name = name;
    }

    private String name;

    public String getName() {
        return name;
    }
}

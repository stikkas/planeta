package ru.planeta.api.news;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.planeta.dao.profiledb.ProfileNewsDAO;
import ru.planeta.model.common.campaign.Share;
import ru.planeta.model.news.ProfileNews;

import java.util.Map;

/**
 *
 * Created by asavan on 27.12.2016.
 */
@Service
public class LoggerServiceImpl implements LoggerService {
    private final ProfileNewsDAO profileNewsDAO;

    @Autowired
    public LoggerServiceImpl(ProfileNewsDAO profileNewsDAO) {
        this.profileNewsDAO = profileNewsDAO;
    }

    @Override
    public ProfileNews addProfileNews(ProfileNews.Type type, long profileId, long objectId, long campaignId, Map<String, String> extraParamsMap) {
        ProfileNews news = new ProfileNews(type, profileId, objectId, campaignId);
        news.setExtraParamsMap(extraParamsMap);
        return addProfileNews(news);
    }

    @Override
    public ProfileNews addProfileNews(ProfileNews.Type type, long profileId, long objectId, long campaignId) {
        return addProfileNews(new ProfileNews(type, profileId, objectId, campaignId));
    }

    @Override
    public ProfileNews addProfileNews(ProfileNews.Type type, long profileId, long objectId) {
        return addProfileNews(new ProfileNews(type, profileId, objectId));
    }

    @Override
    public ProfileNews addFileDeletedNews(ProfileNews.Type type, long profileId, long objectId, Map<String, String> extraParamsMap) {
        ProfileNews news = new ProfileNews(type, profileId, objectId);
        news.setExtraParamsMap(extraParamsMap);
        return addProfileNews(news);
    }

    private ProfileNews addProfileNews(ProfileNews profileNews) {
        profileNewsDAO.insert(profileNews);
        return profileNews;
    }

    @Override
    public ProfileNews addShareNews(ProfileNews.Type type, long clientId, Share share, long campaignId) {
        ProfileNews news = new ProfileNews(type, clientId, share.getShareId(), campaignId);
        news.getExtraParamsMap().put("shareName", share.getName());
        news.getExtraParamsMap().put("purchaseCount", String.valueOf(share.getPurchaseCount()));
        if (type == ProfileNews.Type.SHARE_PURCHASED) {
            news.getExtraParamsMap().put("purchaseSum", String.valueOf(share.getPurchaseSum().longValue()));
        }
        return addProfileNews(news);
    }

}

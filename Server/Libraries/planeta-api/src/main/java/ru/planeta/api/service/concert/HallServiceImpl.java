package ru.planeta.api.service.concert;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.planeta.dao.concertdb.HallDAO;
import ru.planeta.model.concert.Hall;

/**
 * Created with IntelliJ IDEA.
 * User: michail
 * Date: 18.10.16
 * Time: 12:17
 */
@Service
public class HallServiceImpl implements HallService {
    @Autowired
    private HallDAO hallDAO;

    @Override
    public Hall getHall(long hallId) {
        return hallDAO.select(hallId);
    }

    @Override
    public Hall getHallByTitle(String title) {
        return hallDAO.selectByTitle(title);
    }

    private int updateHall(Hall hall) {
        return hallDAO.update(hall);
    }

    private int insertHall(Hall hall) {
        return hallDAO.insert(hall);
    }

    @Override
    public int insertOrUpdateHall(Hall hall) {
        return hall.getHallId() > 0 ? updateHall(hall) : insertHall(hall);
    }
}

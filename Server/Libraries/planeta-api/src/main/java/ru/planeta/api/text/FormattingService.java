package ru.planeta.api.text;

import java.util.Map;

/**
 * Service for formatting html text of comments, posts, etc.
 *
 * @author  m.shulepov
 */
public interface FormattingService {

    String AWAY_URL = "/api/util/away.html";

    String AWAY_MAIL = "/api/util/away_mail.html";

    String AWAY_PARAMETER = "to";

    /**
     * Formats passed text according to the following rules:
     * <pre>
     *  - If text contains bbcode, then format it by calling {@link #formatBbcode(String)} method;
     *  - Otherwise format it by calling {@link #formatMessage(String, long, boolean)}} method
     * </pre>
     *
     *
     * @param text text to be formatted
     * @param profileId profileId
     * @param extractLinksOnly whether to extract links
     * @return formatted text
     */
    String formatPlainText(String text, long profileId, boolean extractLinksOnly);

    /**
     * Converts bbcode into html using rules listed in kefirbb.xml
     *
     * @param bbcode text with bbcode
     * @return formatted html string
     */
    String formatBbcode(String bbcode);

    /**
     * Formats passed <code>text</code> by extracting urls and forming attachments from them;
     * if attachment can not be created from one of the exctracted urls and <code>extractLinksOnly</code>=true,
     * it forms link from it
     *
     *
     *
     *
     * @param text text to be formatted
     * @param profileId profile id
     * @param extractLinksOnly indicates whether links should be created for extracted urls, if attachments couldn't not be created
     * @return formatted text
     */
    String formatMessage(String text, long profileId, boolean extractLinksOnly);

    /**
     * Validates passed html string
     *
     * @param html html
     * @return true if passed html contains valid html, false otherwise
     */
    boolean validateHtml(String html);

    /**
     * Cleans passed html string, deleting restricted tags, attributes and other hacking stuff in specified html
     * @param html html
     * @return cleaned html
     */
    String cleanHtml(String html);

    /**
     * Creates annotation for text html with max text length 400 characters and maximum one photo or video
     * @param html blogpost html text
     * @return annotation
     */
    String createAnnotation(String html);

    String replaceExternalLinks(String html);

    Map<String, String> getRichPhotosOfWrongOwner(long profileId, String richHtml);
}

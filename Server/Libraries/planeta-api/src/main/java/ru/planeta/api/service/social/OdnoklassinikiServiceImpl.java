package ru.planeta.api.service.social;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;
import ru.planeta.api.aspect.transaction.NonTransactional;
import ru.planeta.api.model.ExternalUserInfo;
import ru.planeta.commons.model.Gender;
import ru.planeta.commons.web.RegistrationData;
import ru.planeta.commons.web.WebUtils;
import ru.planeta.model.common.auth.CredentialType;
import ru.planeta.model.common.auth.ExternalAuthentication;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Date: 20.09.12
 *
 * @author s.kalmykov
 */
@Service
public class OdnoklassinikiServiceImpl implements OAuthClientService {

    private static final String APP_ID = "92876800";
    private static final String PUBLIC_KEY = "CBABDKIGABABABABA";
    private static final String SECRET = "257C839320358BCF1AAF75F8";

    private static final Logger logger = Logger.getLogger(OdnoklassinikiServiceImpl.class);



    private static ExternalUserInfo parseJSON(String userInfoJSON) {
        try {
            Map<String, Object> response = getStringObjectMap(userInfoJSON);
            String firstName = (String) response.get("first_name");
            String lastName = (String) response.get("last_name");
            String uid = (String) (response.get("uid"));
            String gender = (String) (response.get("gender"));
            String birthday = (String) (response.get("birthday"));
            String picture = ((String) response.get("pic_1"));
            if (StringUtils.isNotBlank(picture)) {
                picture = picture.replaceAll("&photoType=\\d", "");
            }

            RegistrationData registrationData = new RegistrationData();
            registrationData.setFirstName(firstName);
            registrationData.setLastName(lastName);
            try {
                if (birthday != null) {
                    SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
                    Date birthDate = format.parse(birthday);
                    registrationData.setUserBirthDate(birthDate);
                }
            } catch (ParseException e) {
                logger.warn("json parse error: ", e);
            }
            registrationData.setGender("female".equals(gender) ? Gender.FEMALE : Gender.MALE);
            if (StringUtils.isNotBlank(picture)) {
                registrationData.setPhotoUrl(picture);
            }

            ExternalAuthentication externalAuthentication = new ExternalAuthentication(uid, CredentialType.ODNOKLASSINKI);

            ExternalUserInfo externalUserInfo = new ExternalUserInfo();
            externalUserInfo.setRegistrationData(registrationData);
            externalUserInfo.setExternalAuthentication(externalAuthentication);
            return externalUserInfo;
        } catch (IOException e) {
            logger.error("Malformed json in Odnoklassniki response "+ userInfoJSON, e);
        }
        return null;
    }


    private static String secretPostData(String code, String redirectUri) {
        Map<String, String> query = new HashMap<String, String>();
        query.put("grant_type", "authorization_code");
        query.put("redirect_uri", redirectUri);
        query.put("client_id", APP_ID);
        query.put("client_secret", SECRET);
        query.put("code", code);
        return WebUtils.tryUrlEncodeMap(query, "UTF-8");
    }


    private static String getDataUrl(String accessToken) {
        Map<String, String> query = new HashMap<String, String>();
        String method = "users.getCurrentUser";
        String signature = DigestUtils.md5Hex(accessToken + SECRET);
        signature = DigestUtils.md5Hex("application_key="+PUBLIC_KEY+"method="+method+signature);

        query.put("access_token", accessToken);
        query.put("method", method);
        query.put("sig", signature);
        query.put("application_key", PUBLIC_KEY);
        String queryString = WebUtils.tryUrlEncodeMap(query, "UTF-8");
        return "http://api.odnoklassniki.ru/fb.do?" + queryString;
    }

    @Override
    public String getRedirectUrl(String saveRedirectUrl) {
        try {
            return "http://www.odnoklassniki.ru/oauth/authorize?client_id=" + APP_ID + "&response_type=code&redirect_uri="
                    + URLEncoder.encode(saveRedirectUrl, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            return null;
        }
    }

    @Nullable
    @Override
    @NonTransactional
    public ExternalUserInfo authorizeExternal(String code, String redirectUrl) throws IOException {
        String postData = secretPostData(code, redirectUrl);
        String tokenJSON = WebUtils.uploadString("http://api.odnoklassniki.ru/oauth/token.do", postData);
        Map<String, Object> object = getStringObjectMap(tokenJSON);
        String accessToken = (String) object.get("access_token");
        String dataUrl = getDataUrl(accessToken);
        String userInfoJSON = WebUtils.downloadString(dataUrl);
        return parseJSON(userInfoJSON);
    }

    @Override
    public boolean isMyUrl(@Nonnull String url) {
        return url.contains("ok");
    }

    private static Map<String, Object> getStringObjectMap(String tokenJSON) throws IOException {
        return (new ObjectMapper()).readValue(tokenJSON, HashMap.class);
    }
}

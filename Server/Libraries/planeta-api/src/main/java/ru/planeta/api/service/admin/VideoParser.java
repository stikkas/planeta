package ru.planeta.api.service.admin;

import org.apache.commons.lang3.math.NumberUtils;
import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.log4j.Logger;
import org.ccil.cowan.tagsoup.Parser;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;
import ru.planeta.api.text.RichMediaObject;
import ru.planeta.api.text.VideoAttachment;
import ru.planeta.dao.profiledb.VideoDAO;
import ru.planeta.model.profile.media.Video;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.apache.commons.lang3.StringUtils.isEmpty;

/**
 *
 * Created by a.savanovich on 02.12.2015.
 */
public class VideoParser extends DefaultHandler {

    private static final Logger log = Logger.getLogger(VideoParser.class);


    public VideoParser(VideoDAO videoDAO) {
        this.videoDAO = videoDAO;
    }

    public void setVideoUpdater(VideoUpdaterService videoUpdater) {
        videoUpdaterService = videoUpdater;
    }

    private List<Video> videos;

    private VideoDAO videoDAO;

    private VideoUpdaterService videoUpdaterService = null;

    @Override
    public void startDocument() throws SAXException {
        videos = new ArrayList<>();
    }

    private static VideoAttachment createVideoAttachment(Attributes attributes) {
        String id = attributes.getValue("id");
        String owner = attributes.getValue("owner");
        String name = attributes.getValue("name");
        String image = attributes.getValue("image");
        String duration = attributes.getValue("artist");

        if (isEmpty(id)) {
            throw new IllegalArgumentException("id attribute is empty");
        }
        if (isEmpty(owner)) {
            throw new IllegalArgumentException("owner attribute is empty");
        }
        if (isEmpty(image)) {
            throw new IllegalArgumentException("image attribute is empty");
        }
        VideoAttachment video = new VideoAttachment();
        video.setObjectId(NumberUtils.createLong(id));
        video.setOwnerId(NumberUtils.createLong(owner));
        video.setName(name);
        video.setImageUrl(image);
        video.setDuration(NumberUtils.toInt(duration, 0));
        return video;
    }


    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) {
        try {
            if (qName.equals(RichMediaObject.Type.VIDEO.getValue())) {
                VideoAttachment attachment = createVideoAttachment(attributes);
                Video video = videoDAO.selectVideoById(attachment.getOwnerId(), attachment.getObjectId());
                if (video != null) {
                    videos.add(video);
                }
            }
        } catch (IllegalArgumentException e) {

        }
    }

    @Override
    public void endDocument() {
        System.out.print(videos);
    }

    private String getPhotoString(String paramsString) throws IOException, SAXException {
        Parser parser = new Parser();
        parser.setContentHandler(this);
        parser.parse(new InputSource(new StringReader(paramsString)));
        Video video = getVideo();
        if (video == null) {
            return null;
        }
        if (videoUpdaterService != null) {
            try {
                video = videoUpdaterService.updateYoutubeAndCachedVideo(video.getVideoId(), video.getProfileId());
            } catch (Exception e) {
                log.error("Video " + video.getVideoId() + " not parsed", e);
                return null;
            }
        }
        StringBuilder sb = new StringBuilder();
        appendValue(sb, "duration", String.valueOf(video.getDuration()));
        appendValue(sb, "id", String.valueOf(video.getVideoId()));
        appendValue(sb, "name", String.valueOf(video.getName()));
        appendValue(sb, "owner", String.valueOf(video.getProfileId()));
        appendValue(sb, "image", String.valueOf(video.getImageUrl()));
        return sb.toString();
    }

    private static void appendValue(StringBuilder sb, String name, String value) {
        sb.append(" ").append(name).append("=").append("\"").append(StringEscapeUtils.escapeHtml4(value)).append("\"");
    }

    public String replace(String html) throws IOException, SAXException {
        Pattern p1 = Pattern.compile("(<"+ RichMediaObject.Type.VIDEO.getValue() + " ([^>]*)>\\s*</" + RichMediaObject.Type.VIDEO.getValue() + ">)");
        // Pattern p2 = Pattern.compile("<p:" + getTag() + " ([^>]*)/>");
        String result = replace(p1, html);
        // return replace(p2, result, blogPostUrl, staticNode);
        return result;
    }

    private String replace(Pattern pattern, String tx) throws IOException, SAXException {
        Matcher m = pattern.matcher(tx);
        StringBuilder sb = new StringBuilder();
        int copyFrom = 0;
        while (m.find()) {

            String in = getPhotoString(m.group(1));
            if (in != null) {
                sb.append(tx.substring(copyFrom, m.start()));
                sb.append("<").append(RichMediaObject.Type.VIDEO.getValue())
                        .append(in)
                        .append(">").append("</").append(RichMediaObject.Type.VIDEO.getValue()).append(">");;
            } else {
                sb.append(m.group(1));
            }
            copyFrom = m.end();
        }
        sb.append(tx.substring(copyFrom));
        return sb.toString();

    }

    public Video getVideo() {
        return (videos == null || videos.isEmpty()) ? null : videos.get(0);
    }
}

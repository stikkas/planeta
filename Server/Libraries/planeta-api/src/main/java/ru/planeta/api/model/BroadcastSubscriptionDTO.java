package ru.planeta.api.model;

/**
 * Date: 22.10.2015
 * Time: 16:26
 */
public class BroadcastSubscriptionDTO {
    private String email;
    private long profileId;
    private long broadcastId;
    private long broadcastProfileId;

    public long getBroadcastProfileId() {
        return broadcastProfileId;
    }

    public void setBroadcastProfileId(long broadcastProfileId) {
        this.broadcastProfileId = broadcastProfileId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public long getProfileId() {
        return profileId;
    }

    public void setProfileId(long profileId) {
        this.profileId = profileId;
    }

    public long getBroadcastId() {
        return broadcastId;
    }

    public void setBroadcastId(long broadcastId) {
        this.broadcastId = broadcastId;
    }
}

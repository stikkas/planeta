package ru.planeta.api.service.billing.payment.system.yamoney.mws.models;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by eshevchenko on 25.04.14.
 */
@XmlRootElement
public class ListReturnsResponse extends BaseResponse {

    @XmlElement(name = "returnPayment")
    public List<ReturnPayment> returnPayments = new ArrayList<ReturnPayment>();
}

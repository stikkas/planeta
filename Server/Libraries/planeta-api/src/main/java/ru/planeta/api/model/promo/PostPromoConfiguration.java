package ru.planeta.api.model.promo;

/**
 * Created by eshevchenko on 24.12.14.
 */
public class PostPromoConfiguration extends BasePromoConfiguration {

    private String title;
    private String imageUrl;
    private String description;
    private String originalUrl;
    private long postId;
    private boolean main;
    private boolean hot;

    public PostPromoConfiguration() {
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getOriginalUrl() {
        return originalUrl;
    }

    public void setOriginalUrl(String originalUrl) {
        this.originalUrl = originalUrl;
    }

    public long getPostId() {
        return postId;
    }

    public void setPostId(long postId) {
        this.postId = postId;
    }

    public boolean isMain() {
        return main;
    }

    public void setMain(boolean isMain) {
        this.main = isMain;
    }

    public boolean isHot() {
        return hot;
    }

    public void setHot(boolean isHot) {
        this.hot = isHot;
    }
}

package ru.planeta.api.model.shop;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.math.BigDecimal;

/**
 * Created with IntelliJ IDEA.
 * Date: 20.10.2014
 * Time: 13:37
 * To change this template use File | Settings | File Templates.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class PaymentDTO {
    private BigDecimal amount;
    private long paymentMethodId;
    private boolean fromBalance;

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public long getPaymentMethodId() {
        return paymentMethodId;
    }

    public void setPaymentMethodId(long paymentMethodId) {
        this.paymentMethodId = paymentMethodId;
    }

    public boolean isFromBalance() {
        return fromBalance;
    }

    public void setFromBalance(boolean fromBalance) {
        this.fromBalance = fromBalance;
    }

}

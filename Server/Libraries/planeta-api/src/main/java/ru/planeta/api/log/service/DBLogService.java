package ru.planeta.api.log.service;

import ru.planeta.api.aspect.transaction.NonTransactional;
import ru.planeta.api.model.json.DBLogRecordInfo;
import ru.planeta.model.stat.log.LoggerType;

import java.util.List;

/**
 * Database logging service.<br>
 * Provides methods for database log manipulations.<br>
 * User: eshevchenko
 */
public interface DBLogService {

    /**
     * Gets log records for specified <code>logType</code>, <code>objectId</code> and <code>subjectId</code>
     * @param logType logger type;
     * @param objectId object identifier;
     * @param subjectId subject identifier, can be 0, if not used for current logger type;
     * @return list of log records which responds filtering clause.
     */
    @NonTransactional
    List<DBLogRecordInfo> getLogRecords(LoggerType logType, long objectId, long subjectId);

    /**
     * Adds log record with all related info.
     *
     * @param message log message;
     * @param logType logger type;
     * @param objectId logged object identifier;
     * @param subjectId logged subject identifier;
     * @param requestId HTTP request log record identifier (if logs HTTP request);
     * @param responseId HTTP response log record identifier (if logs HTTP request);
     * @return added record identifier.
     */
    @NonTransactional
    long addRecordToLog(String message, LoggerType logType, Long objectId, Long subjectId, Long requestId, Long responseId);

    /**
     * Adds log record with message for specific logger type. All related info identifiers are sets to 0.
     * @param message log message;
     * @param loggerType logger type;
     * @return added record identifier.
     */
    @Deprecated
    @NonTransactional
    long addRecordToLog(String message, LoggerType loggerType);

    /**
     * Attaches related info identifiers to already existing log record.
     * @param recordId log record identifier;
     * @param objectId logged object identifier;
     * @param subjectId logged subject identifier;
     * @param requestId HTTP request log record identifier (if logs HTTP request);
     * @param responseId HTTP response log record identifier (if logs HTTP request);
     */
    @NonTransactional
    void attachRefInfo(long recordId, Long objectId, Long subjectId, Long requestId, Long responseId);

    /**
     * Adds request meta data to HTTP-communications log.
     * @param metaData request meta data;
     * @return added record identifier.
     */
    @NonTransactional
    long addRequestToLog(String metaData);

    /**
     * Adds request meta data to HTTP-communications log.
     * @param requestId related request indentifier;
     * @param metaData response meta data;
     * @return added record identifier.
     */
    @NonTransactional
    long addResponseToLog(long requestId, String metaData);
}

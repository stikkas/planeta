package ru.planeta.api.service.registration;

import ru.planeta.model.common.UserPrivateInfo;
import ru.planeta.model.profile.Profile;
import ru.planeta.model.stat.RequestStat;

public interface PopulateUserRegistrationInfoStrategy {

    void populateProfile(Profile profile);

    void populatePrivateInfo(UserPrivateInfo userPrivateInfo);

    void delayedProfileUpdate(Profile profile);

    RequestStat getRequestStat();
}

package ru.planeta.api.text;

/**
 * Represents a message part.
 */
public class MessagePart {

    private final String text;
    private final MessagePartType messagePartType;

    public MessagePart(String text, MessagePartType messagePartType) {
        this.text = text;
        this.messagePartType = messagePartType;
    }

    public String getText() {
        return text;
    }

    public MessagePartType getMessagePartType() {
        return messagePartType;
    }
}

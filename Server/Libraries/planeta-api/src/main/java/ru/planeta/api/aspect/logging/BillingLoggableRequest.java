package ru.planeta.api.aspect.logging;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Marks billing request processing methods.
 * User: eshevchenko
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface BillingLoggableRequest {

    /**
     * Indicates is request initial in payment processing redirect chain.
     * @return
     */
    boolean initial() default false;
}

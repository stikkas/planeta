package ru.planeta.api.service.configurations;


import ru.planeta.api.model.enums.image.ImageType;
import ru.planeta.model.enums.ThumbnailType;
import ru.planeta.utils.ThumbConfiguration;

import java.util.List;

/**
 * @author ds.kolyshev
 * Date: 15.09.11
 */
public interface StaticNodesService {

	/**
	 * Returns most common static node
	 * @return
	 */
    String getStaticNode();

	/**
	 * Returns thumbnail url with static node for specified image's url
	 * @param url
	 * @param thumbnailType
	 * @param imageType
	 * @return
	 */
    String getThumbnailUrl(String url, ThumbnailType thumbnailType, ImageType imageType);

    String getStaticNodeOrCurrent(String url);

    /**
	 * Wraps image on specified url. Generates thumbs for external url.
	 * @param url
	 * @param config
	 * @return
	 */
    String wrapImageUrl(String url, ThumbConfiguration config);

    /**
     * Wraps image on specified url. Generates thumbs for external url.
     * @param url
     * @param config
     * @return
     */
    String wrapImageUrl(String url, ThumbConfiguration config, boolean force);

    /**
	 * Gets resources host url
	 * @return
	 */
    String getResourceHost();

	/**
	 * Gets list of registered static node servers
	 * @return
	 */
    List<String> getStaticNodes();

    boolean isStaticServerPath(String url);
}

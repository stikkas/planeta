package ru.planeta.api.model.shop;

import ru.planeta.model.profile.Profile;
import ru.planeta.model.shop.Product;

/**
 * @author ds.kolyshev
 * Date: 02.08.12
 */
public class ProductChangesetInfo {
	private Product product;
	private Profile ownerProfile;

    public ProductChangesetInfo(Product product, Profile ownerProfile) {
		this.product = product;
		this.ownerProfile = ownerProfile;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public Profile getOwnerProfile() {
		return ownerProfile;
	}

	public void setOwnerProfile(Profile ownerProfile) {
		this.ownerProfile = ownerProfile;
	}

}

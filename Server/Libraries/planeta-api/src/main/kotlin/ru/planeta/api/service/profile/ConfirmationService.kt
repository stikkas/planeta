package ru.planeta.api.service.profile

/**
 * Works with any confirmations of some actions.
 * Confirmation can be send by 2 ways - sms and e-mail (2 methods)
 * Processes with time-restriction (and with any status) becomes NOT_FOUND
 * after some expiration time
 */
interface ConfirmationService {

    /**
     * Tries to confirm notification process. If process doesn't exist returns NOT_FOUND.
     * If process confirmed successfully calls confirmationCallback. Exceptions during
     * confirmationCallback generate ERROR status.
     * @param processId UUID.toString id of confirmation process
     * @param confirmationCode secure code of confirmation
     * @return confirmation process status
     */
    fun confirm(processId: String, confirmationCode: String): Status

    /**
     * Creates expiring (5 min to confirm) confirmation process. Sends email with
     * templateName = callback.getClass().getName()
     *
     *
     * @param phone phone number to send confirmation sms
     * @param callback Object that defines name for sms-template and callback to exec just after successful confirm
     * @return UUID.toString id of confirmation process
     */
    fun createPhoneConfirmation(clientId: Long, phone: String, callback: Runnable?): String

    /**
     * Replaces spoiled (with missed sms or email) confirmation process with new one
     * @param confirmationProcessID UUID.toString id of spoiled confirmation process
     * @return UUID.toString id of new confirmation process
     */
    fun reSendCode(confirmationProcessID: String): String


    /**
     * Someone can ask service for process status:
     * process can be CONFIRMED, confirmed processes can be safety finished and get NOT_FOUND status
     * process can be NOT_FOUND if there are no process of such type for specified clientId
     * process can have WRONG_CODE status if attempts to confirm was found, but without success
     * process can have ATTEMPTS_EXCEEDED status if no attempts left
     * process can have ERROR status if there appears an error during callback execution.
     */
    enum class Status {
        CONFIRMED,
        NOT_FOUND,
        WRONG_CODE,
        ATTEMPTS_EXCEEDED,
        ERROR
    }
}

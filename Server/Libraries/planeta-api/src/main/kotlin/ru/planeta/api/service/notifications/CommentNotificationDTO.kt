package ru.planeta.api.service.notifications

import ru.planeta.model.profile.Comment
import ru.planeta.model.profile.Profile

/**
 * @author s.kalmykov
 * @since 09.07.2014
 */
class CommentNotificationDTO(val comment: Comment, val owner: Profile, val author: Profile, val commentableObject: Any)

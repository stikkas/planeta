package ru.planeta.api.service.profile

import org.apache.commons.lang3.StringUtils
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import ru.planeta.api.exceptions.NotFoundException
import ru.planeta.api.exceptions.PermissionException
import ru.planeta.api.model.json.ProfileGeneralSettings
import ru.planeta.api.service.security.PermissionService
import ru.planeta.dao.commondb.UserPrivateInfoDAO
import ru.planeta.dao.maildb.MailUserDAO
import ru.planeta.dao.profiledb.ProfileDAO
import ru.planeta.model.common.UserPrivateInfo
import ru.planeta.model.profile.Profile

import java.util.Date

/**
 * Class ProfileSettingsServiceImpl
 *
 * @author a.tropnikov
 */
@Service
class ProfileSettingsServiceImpl @Autowired
constructor(private val profileDAO: ProfileDAO, private val permissionService: PermissionService, private val userPrivateInfoDAO: UserPrivateInfoDAO, private val mailUserDAO: MailUserDAO) : ProfileSettingsService {

    @Throws(NotFoundException::class, PermissionException::class)
    override fun saveProfileGeneralSettings(clientId: Long, profileGeneralSettings: ProfileGeneralSettings): Profile {

        permissionService.checkIsAdmin(clientId, profileGeneralSettings.profileId)

        val profile = getProfileSafe(profileGeneralSettings.profileId)

        profile.displayName = profileGeneralSettings.displayName
        profile.userBirthDate = if (profileGeneralSettings.userBirthDate == null) null else Date(profileGeneralSettings.userBirthDate!!)

        profile.userGender = profileGeneralSettings.gender
        profile.cityId = profileGeneralSettings.cityId
        profile.countryId = profileGeneralSettings.countryId
        profile.phoneNumber = profileGeneralSettings.phoneNumber

        if (StringUtils.isNotEmpty(profileGeneralSettings.alias)) {
            profile.alias = profileGeneralSettings.alias!!.toLowerCase()
        }

        profile.summary = profileGeneralSettings.summary
        profile.isLimitMessages = profileGeneralSettings.isLimitMessages
        profile.isReceiveMyCampaignNewsletters = profileGeneralSettings.isReceiveMyCampaignNewsletters
        profileDAO.update(profile)
        return profile
    }


    @Throws(NotFoundException::class, PermissionException::class)
    override fun unsubscribeUserFromSpam(clientId: Long, profileId: Long, unsubscribe: Boolean) {
        permissionService.checkIsAdmin(clientId, profileId)
        val profile = getProfileSafe(profileId)
        profile.isReceiveNewsletters = !unsubscribe
        profileDAO.update(profile)
        val privateInfo = userPrivateInfoDAO.selectByUserId(profileId)
                ?: throw NotFoundException(UserPrivateInfo::class.java, profileId)
        if (unsubscribe) {
            mailUserDAO.unsubscribeUserByEmail(privateInfo.email!!)
        } else {
            mailUserDAO.forceSubscribeUserByEmail(privateInfo.email!!)
        }
    }


    @Throws(PermissionException::class, NotFoundException::class)
    override fun unSubscribeUserFromAllMailNotifications(clientId: Long, profileId: Long) {
        permissionService.checkIsAdmin(clientId, profileId)
        unSubscribeUserFromAllMailNotificationsUnsafe(profileId)
    }

    @Throws(PermissionException::class, NotFoundException::class)
    override fun unSubscribeUserFromAllMailNotificationsUnsafe(profileId: Long) {
        val profile = getProfileSafe(profileId)
        profile.isReceiveMyCampaignNewsletters = false
        profile.isReceiveNewsletters = false
        profileDAO.update(profile)
    }

    @Throws(NotFoundException::class)
    private fun getProfileSafe(profileId: Long): Profile {
        return profileDAO.selectById(profileId) ?: throw NotFoundException(Profile::class.java, profileId)
    }


}

package ru.planeta.api.service.campaign

import ru.planeta.model.common.campaign.Campaign
import ru.planeta.model.common.campaign.Share

/**
 * @author s.kalmykov
 * @since 13.07.2014
 */
class ShareWithCampaign(o: Share, var campaign: Campaign?) : Share(o)

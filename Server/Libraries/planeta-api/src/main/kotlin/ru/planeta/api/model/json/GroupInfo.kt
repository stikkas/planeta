package ru.planeta.api.model.json

import ru.planeta.model.profile.Group
import ru.planeta.model.profile.Profile

import java.math.BigDecimal

/**
 *
 * Class GroupInfo
 * @author a.tropnikov
 */
class GroupInfo : ProfileInfo {

    var group: Group? = null
    var balance: BigDecimal? = null

    constructor() : super() {
        group = Group()
    }

    constructor(group: Group) {
        this.group = group
    }

    constructor(group: Group, profile: Profile) : super(profile) {
        this.group = group
    }

}

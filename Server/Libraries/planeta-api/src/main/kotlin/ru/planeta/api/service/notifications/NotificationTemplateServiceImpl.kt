package ru.planeta.api.service.notifications

import freemarker.template.Configuration
import freemarker.template.DefaultObjectWrapper
import freemarker.template.TemplateException
import org.apache.commons.lang3.StringUtils
import org.apache.log4j.Logger
import org.springframework.stereotype.Service
import ru.planeta.api.Utils
import ru.planeta.api.helper.ProjectService
import ru.planeta.api.service.configurations.ImageHelperFunctions
import ru.planeta.api.service.configurations.StaticNodesService
import ru.planeta.model.enums.ProjectType
import java.io.IOException
import java.io.StringWriter
import java.util.*

@Service
class NotificationTemplateServiceImpl(private val projectService: ProjectService,
                                      private val staticNodeService: StaticNodesService) : NotificationTemplateService {
    private val configuration: Configuration

    init {
        /**
         * remove logging of freemarker.cache
         * don't call it after Configuration creation, it will have
         * [no effect](http://freemarker.sourceforge.net/docs/pgui_misc_logging.html)
         */
        try {
            freemarker.log.Logger.selectLoggerLibrary(freemarker.log.Logger.LIBRARY_NONE)
        } catch (e: ClassNotFoundException) {
            log.warn("FreeMarker console wasn't disabled", e)
        }

        configuration = Configuration()
        configuration.locale = Locale("ru", "RU")
        configuration.setEncoding(configuration.locale, "UTF-8")
        // no data-model transform before use it in template
        configuration.objectWrapper = DefaultObjectWrapper()
        // to have 11111111 id not like "11 111 111"
        configuration.numberFormat = "0.######"
        // define loader from jar using Class.getResource
        configuration.setClassForTemplateLoading(NotificationTemplateService::class.java, "/ru/planeta/templates")
    }

    override fun applyCustomTemplate(templateFileName: String, objects: MutableMap<String, Any>): String {
        return applyTemplate(fillHosts(objects), templateFileName)
    }

    private fun fillHosts(objects: MutableMap<String, Any>): MutableMap<String, Any> {
        objects["baseHost"] = projectService.getHost(ProjectType.MAIN)
        objects["appHost"] = projectService.getHost(ProjectType.MAIN)
        objects["baseHostUrl"] = projectService.getUrl(ProjectType.MAIN)
        objects["shopHost"] = projectService.getHost(ProjectType.SHOP)
        objects["shopHostUrl"] = projectService.getUrl(ProjectType.SHOP)
        objects["adminHost"] = projectService.getHost(ProjectType.ADMIN)
        objects["adminHostUrl"] = projectService.getUrl(ProjectType.ADMIN)
        objects["tvHost"] = projectService.getHost(ProjectType.TV)
        objects["tvHostUrl"] = projectService.getUrl(ProjectType.TV)
        return objects
    }

    private fun addHelperFunctions(objects: MutableMap<String, Any>) {
        objects["StringUtils"] = ru.planeta.commons.lang.StringUtils()
        objects["ImageHelperFunctions"] = ImageHelperFunctions(staticNodeService)
        objects["Utils"] = Utils
    }

    override fun applyRichTemplate(objectMap: MutableMap<String, Any>): String {
        return applyCustomTemplate(RICH_TEMPLATE_FILE, objectMap)
    }


    // use applyCustomTemplate instead
    private fun applyTemplate(objectMap: MutableMap<String, Any>, templateFile: String): String {
        addHelperFunctions(objectMap)
        val sw = StringWriter()
        try {
            try {
                val template = configuration.getTemplate(templateFile)
                template.process(objectMap, sw)
            } catch (e: TemplateException) {
                log.error("Notification templates: Bad parameters", e)
                return ""
            } finally {
                sw.close()
            }
        } catch (e: IOException) {
            log.error("Notification templates IO", e)
        } catch (e: Exception) {
            log.error("Unknown exception", e)
        }

        return StringUtils.trimToEmpty(sw.toString())
    }

    companion object {

        private val RICH_TEMPLATE_FILE = "rich-templates.ftl"
        private val log = Logger.getLogger(NotificationTemplateServiceImpl::class.java)
    }
}

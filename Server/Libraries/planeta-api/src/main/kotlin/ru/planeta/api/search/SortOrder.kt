package ru.planeta.api.search

import org.apache.commons.lang3.StringUtils
import java.util.HashMap
import java.util.EnumSet

/**
 * Fields of the search index
 *
 * @author ameshkov
 */
enum class SortOrder private constructor(// <editor-fold defaultstate="collapsed" desc="Constructor and properties">
        val code: Int) {

    SORT_DEFAULT(0),
    SORT_BY_DATE(1),
    SORT_BY_COUNT(2),
    SORT_BY_RATING(3),
    SORT_BY_DURATION(4),
    SORT_BY_SUM(5),
    SORT_BY_TIME_FINISHED_ASC(6),
    SORT_BY_TIME_FINISHED_DESC(15),
    SORT_BY_TIME_ADDED_DESC(8),
    SORT_BY_TIME_ADDED_ASC(18),
    SORT_BY_TIME_STARTED_THAN_CREATED(9),
    SORT_BY_TIME_STARTED_ASC(19),
    SORT_BY_TIME_STARTED_DESC(20),
    SORT_BY_MEDIAN_PRICE(10),
    SORT_BY_COLLECTION_RATE(11),
    SORT_BY_TIME_LAST_NEWS_PUBLISHED(12),
    SORT_BY_COMPLETE_PERCENTAGE(13),
    SORT_RELEVANCE(14),
    SORT_BY_COMMENTS_COUNT(15),
    SORT_BY_PRICE_ASC(16),
    SORT_BY_PRICE_DESC(17),
    SORT_BY_TIME_UPDATED_DESC(21),
    SORT_BY_TIME_UPDATED_ASC(22),
    SORT_BY_RARITY(23),
    SORT_BY_PURCHASE_COUNT_DESC(24),
    SORT_BY_COUNT_DESC(25),
    SORT_BY_COUNT_ASC(26),
    SORT_BY_NAME_ASC(27),
    SORT_BY_NAME_DESC(28),
    SORT_BY_PURCHASE_COUNT_ASC(29),
    SORT_BY_PURCHASE_DATE_DESC(30),
    SORT_BY_PURCHASE_DATE_ASC(31),
    SORT_RARE(32),
    SORT_FEW(33);


    companion object {
        private val lookupByCode = HashMap<Int, SortOrder>()

        init {
            for (SortOrder in EnumSet.allOf(SortOrder::class.java)) {
                if (!lookupByCode.containsKey(SortOrder.code)) {
                    lookupByCode[SortOrder.code] = SortOrder
                }
            }
        }

        fun getByCode(code: Int): SortOrder {
            var order: SortOrder? = lookupByCode[code]
            if (order == null) {
                order = SORT_DEFAULT
            }
            return order
        }

        fun getCampaignSortOrder(query: String?, status: CampaignStatusFilter?): SortOrder {
            val sort: SortOrder
            if (StringUtils.isBlank(query)) {
                when (status) {
                    CampaignStatusFilter.ACTIVE_AND_FINISHED, CampaignStatusFilter.ACTIVE -> sort = SortOrder.SORT_BY_COLLECTION_RATE
                    CampaignStatusFilter.LAST_UPDATED -> sort = SortOrder.SORT_BY_TIME_LAST_NEWS_PUBLISHED
                    CampaignStatusFilter.RECORD_HOLDERS -> sort = SortOrder.SORT_BY_SUM
                    CampaignStatusFilter.NEW -> sort = SortOrder.SORT_BY_TIME_STARTED_THAN_CREATED
                    CampaignStatusFilter.CLOSE_TO_FINISH -> sort = SortOrder.SORT_BY_TIME_FINISHED_ASC
                    CampaignStatusFilter.SUCCESSFUL -> sort = SortOrder.SORT_BY_COMPLETE_PERCENTAGE
                    CampaignStatusFilter.DISCUSSED -> sort = SORT_BY_COMMENTS_COUNT
                    else -> sort = SortOrder.SORT_DEFAULT
                }
            } else {
                sort = SortOrder.SORT_RELEVANCE
            }
            return sort
        }
    }

    // </editor-fold>
}

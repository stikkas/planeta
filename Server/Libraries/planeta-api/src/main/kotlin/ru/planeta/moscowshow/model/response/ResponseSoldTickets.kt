package ru.planeta.moscowshow.model.response

import ru.planeta.moscowshow.model.result.ResultSoldTickets
import javax.xml.bind.annotation.XmlAccessType
import javax.xml.bind.annotation.XmlAccessorType
import javax.xml.bind.annotation.XmlRootElement

/**
 *
 * User: Serge Blagodatskih<stikkas17></stikkas17>@gmail.com><br></br>
 * Date: 13.10.16<br></br>
 * Time: 10:12
 */
@XmlRootElement(name = "getSoldTicketsResponse")
@XmlAccessorType(XmlAccessType.FIELD)
class ResponseSoldTickets : Response<ResultSoldTickets>() {

    override var result: ResultSoldTickets? = null

}

package ru.planeta.api.service.promo

import ru.planeta.model.trashcan.PromoCode

interface ExternalPromoCodeService {
    fun getLastNonUsed(type: Long): PromoCode

    fun updatePromoCode(code: PromoCode)

    fun usePromoCode(code: PromoCode)

    fun insertCodes(type: Long, codes: List<String>)

    fun getAllCodes(type: Long): List<String>?

    fun getUsedPromoCodesCount(configId: Long): Long
}

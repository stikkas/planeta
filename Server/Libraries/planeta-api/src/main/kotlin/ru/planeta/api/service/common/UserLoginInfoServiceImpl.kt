package ru.planeta.api.service.common

import org.apache.log4j.Logger
import org.springframework.stereotype.Service
import ru.planeta.api.service.security.PermissionService
import ru.planeta.dao.commondb.UserLoginInfoDAO
import ru.planeta.model.common.UserLoginInfo

/**
 * User: sshendyapin
 * Date: 28.01.13
 * Time: 20:12
 */
@Service
class UserLoginInfoServiceImpl(private var getUserLoginInfoDAO: UserLoginInfoDAO) : UserLoginInfoService {
    override fun addOrUpdateUserLoginInfo(userLoginInfo: UserLoginInfo): Int {
        try {
            if (PermissionService.ANONYMOUS_USER_PROFILE_ID == userLoginInfo.profileId) {
                log.error("anonymous profile logged in")
                return 0
            }
            val selectedUserLoginInfo = getUserLoginInfoDAO!!.select(userLoginInfo.profileId, userLoginInfo.userIpAddress!!, userLoginInfo.userAgent!!)
            val result: Int
            if (selectedUserLoginInfo == null) {
                //add new userLoginInfo
                result = getUserLoginInfoDAO.insert(userLoginInfo)
            } else {
                //update userLoginInfo
                result = getUserLoginInfoDAO.update(userLoginInfo)
            }
            return result
        } catch (ex: Exception) {
            log.warn("Error occured", ex)
            return 0
        }

    }

    override fun getUserLoginInfo(profileId: Long, userIpAddress: String, userAgent: String): UserLoginInfo {
        return getUserLoginInfoDAO.select(profileId, userIpAddress, userAgent)
    }

    override fun getLastUserLoginInfo(profileId: Long): UserLoginInfo {
        return getUserLoginInfoDAO.selectLastUserLoginInfo(profileId)
    }

    companion object {

        private val log = Logger.getLogger(UserLoginInfoServiceImpl::class.java)
    }
}

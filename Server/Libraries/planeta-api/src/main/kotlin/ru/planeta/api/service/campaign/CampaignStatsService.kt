package ru.planeta.api.service.campaign

import org.sphx.api.SphinxException
import ru.planeta.api.exceptions.NotFoundException
import ru.planeta.api.exceptions.PermissionException
import ru.planeta.model.common.campaign.Campaign
import ru.planeta.model.common.campaign.CampaignSearchFilter
import ru.planeta.model.common.campaign.Share
import ru.planeta.reports.ReportType
import ru.planeta.reports.SimpleReport
import java.util.Date

/**
 * @author m.shulepov
 * Date: 14.06.13
 */
interface CampaignStatsService {

    /**
     * Returns Set with four campaigns: <br></br>
     * 1 campaign - from star campaigns (if there is no star, actual will be returned)<br></br>
     * 2 campaign - from top shared campaigns (if there is no top shared, actual will be returned)<br></br>
     * 3 campaign - from related campaigns (if there is no related campaigns by CampaignTag) ,
     * then CampaignTag.CHARITY will be returned, otherwise actual campaign will be returned<br></br>
     * 4 campaign - from actual campaigns (we think, that actual campaigns are ever available)<br></br>
     *
     *
     * @param ignoreCampaignId       current Campaign
     * @return                      set of campaigns with 4 elements
     */
    @Throws(SphinxException::class)
    fun getRelatedCampaigns(ignoreCampaignId: Long): Collection<Campaign>

    fun getSharesSaleInfo(campaignId: Long, from: Date?, to: Date?, offset: Int, limit: Int): List<Share>

    @Throws(NotFoundException::class)
    fun getSimilarShares(shareId: Long, limit: Int): List<ShareWithCampaign>

    @Throws(NotFoundException::class, PermissionException::class)
    fun getReport(clientId: Long, reportType: ReportType, campaignSearchFilter: CampaignSearchFilter): SimpleReport

}

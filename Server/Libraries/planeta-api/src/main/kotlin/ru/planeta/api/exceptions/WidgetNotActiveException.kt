package ru.planeta.api.exceptions

import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ResponseStatus

/**
 *
 * Created by asavan on 29.03.2017.
 */
@ResponseStatus(value = HttpStatus.OK, reason = "Access denied")
class WidgetNotActiveException(val shareId: Long) : PermissionException()

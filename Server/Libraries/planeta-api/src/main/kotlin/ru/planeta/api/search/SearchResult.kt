package ru.planeta.api.search

/**
 * Represents search results
 *
 * @author ameshkov
 */
class SearchResult<T> {

    /**
     * Time (ms) spent on this search
     */
    var searchTime: Long = 0
    /**
     * Estimation for all documents that could be found
     */
    var estimatedCount: Long = 0
    /**
     * Search results itself
     */
    var searchResultRecords: List<T>? = null

    val size: Int
        get() = if (searchResultRecords == null) 0 else searchResultRecords!!.size

    val isEmpty: Boolean
        get() = searchResultRecords == null || searchResultRecords!!.isEmpty()

    /**
     * Creates an empty search result object.
     */
    constructor() {}

    /**
     * Creates search result object.
     */
    constructor(searchTime: Long, estimatedCount: Long, searchResultRecords: List<T>) {
        this.searchTime = searchTime
        this.estimatedCount = estimatedCount
        this.searchResultRecords = searchResultRecords
    }
}

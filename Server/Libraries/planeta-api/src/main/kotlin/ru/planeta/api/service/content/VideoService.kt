package ru.planeta.api.service.content

import org.sphx.api.SphinxException
import ru.planeta.api.exceptions.*
import ru.planeta.api.model.json.VideoInfo
import ru.planeta.api.model.json.VideoInfoForFlash
import ru.planeta.api.text.VideoAttachment
import ru.planeta.commons.external.oembed.provider.VideoType
import ru.planeta.model.profile.media.Video

/**
 * Video service
 *
 * @author ds.kolyshev
 * Date: 27.10.11
 */
interface VideoService {

    /**
     * Gets video by identifier
     *
     * @param clientId  Viewer profile id
     * @param profileId Owner profile id
     * @param videoId   Video id
     * @return Selected video info
     */
    @Throws(VideoPermissionException::class, VideoNotFoundException::class)
    fun getVideo(clientId: Long, profileId: Long, videoId: Long): VideoInfo


    /**
     * Gets video by identifier with VideoProcessingException
     *
     * @param clientId  Viewer profile id
     * @param profileId Owner profile id
     * @param videoId   Video id
     * @return Selected video info
     */
    @Throws(VideoPermissionException::class, VideoNotFoundException::class, VideoProcessingException::class)
    fun getVideoToSee(clientId: Long, profileId: Long, videoId: Long): VideoInfo


    /**
     * This method is equivalent to the `getVideo()` method, in fact it internally it makes call to `getVideo()` method
     * the difference between two is that current method doesn't throw any exceptions.
     * If any exception occurs during its execution it simply return null.
     *
     * @param clientId  Viewer profile id
     * @param profileId Owner profile id
     * @param videoId   Video identifier
     * @return Selected video id
     */
    @Deprecated("")
    fun getVideoSafely(clientId: Long, profileId: Long, videoId: Long): VideoInfo?

    /**
     * Saves video
     *
     * @param clientId Saver profile id
     * @param video    Video to be saved
     * @return Saved video
     */
    @Throws(NotFoundException::class, PermissionException::class)
    fun saveVideo(clientId: Long, video: Video): Video

    /**
     * Deletes video
     *
     * @param clientId  Deleter profile id
     * @param profileId Video owner profile id
     * @param videoId   Video identifier
     */
    @Throws(NotFoundException::class, PermissionException::class)
    fun deleteVideo(clientId: Long, profileId: Long, videoId: Long)

    /**
     * Adds external video to user video (parses it, then adds to CachedVideo and to videos table).
     * Throws NotFoundException if could not parse it.
     *
     * @param clientId    Video editor profile id
     * @param profileId   Video owner profile id (could be the same as clientId or another -- for groups and events videos)
     * @param externalUrl Youtube url
     * @param videoType   use this to specify VideoType.EXTERNAL_HIDDEN or null if any other videoTypes
     * @return Video info of the added external video
     */
    @Throws(NotFoundException::class, PermissionException::class)
    fun addExternalVideo(clientId: Long, profileId: Long, externalUrl: String, videoType: VideoType?): VideoInfo

    /**
     * Parses video url from popular video hosting, extracts video info,
     * saves video on server and returns cashed video url
     */
    @Throws(NotFoundException::class)
    fun getExternalVideoDetails(url: String): VideoInfo

    /**
     * Gets extended videoInfo by identifier.
     * Extended info is needed for flash player functionality.
     *
     * @param clientId  Viewer profile id
     * @param profileId Video owner profile id
     * @param videoId   Video id
     * @return Video info extended with url to tv application page and related videos set.
     */
    @Throws(PermissionException::class, SphinxException::class, VideoNotFoundException::class)
    fun getVideoInfoExtended(clientId: Long, profileId: Long, videoId: Long): VideoInfoForFlash

    /**
     * Adds video attachments to the user videos
     *
     * @param clientId         Video editor profile id
     * @param ownerProfileId   Video owner profile id
     * @param videoAttachments List of attachments to be added
     * @throws NotFoundException
     * @throws PermissionException
     */
    @Throws(NotFoundException::class, PermissionException::class)
    fun addExternalVideoAttachments(clientId: Long, ownerProfileId: Long, videoAttachments: List<VideoAttachment>)

}

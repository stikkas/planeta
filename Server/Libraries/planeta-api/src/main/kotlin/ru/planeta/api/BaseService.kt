package ru.planeta.api

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.MessageSource
import org.springframework.context.i18n.LocaleContextHolder
import ru.planeta.api.exceptions.MessageCode
import ru.planeta.api.exceptions.NotFoundException
import ru.planeta.api.mail.MailClient
import ru.planeta.api.service.security.PermissionService
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.Executors
import java.util.concurrent.TimeUnit

/**
 * Base service for all services
 *
 * @author ds.kolyshev
 * Date: 13.09.11
 */
abstract class BaseService : BaseDaoService() {

    @Autowired
    open lateinit var mailClient: MailClient

    @Autowired
    open lateinit var permissionService: PermissionService

    @Autowired
    open lateinit var messageSource: MessageSource

    /**
     * Delays task execution
     *
     * @param task  Task
     * @param delay Delay
     * @param timeUnit Time unit for delay
     */
    protected open fun delay(task: Runnable, delay: Long, timeUnit: TimeUnit) {
        DELAYED_TASKS_EXECUTOR.schedule(task, delay, timeUnit)
    }


    interface Getter<T> {
        @Throws(NotFoundException::class)
        operator fun get(objectId: Long): T
    }

    protected open fun getLocalizeMessage(messageCode: MessageCode): String {
        return getMessage(messageCode, messageSource)
    }

    protected open fun format(date: Date, format: String): String {
        return SimpleDateFormat(format).format(date)
    }

    companion object {

        private val DELAYED_TASKS_EXECUTOR = Executors.newSingleThreadScheduledExecutor()

        fun getMessage(messageCode: MessageCode, messageSource: MessageSource?, vararg args: Any): String {
            val code = messageCode.errorPropertyName
            return if (messageSource == null) code else messageSource.getMessage(code, args, code, LocaleContextHolder.getLocale())
        }
    }
}

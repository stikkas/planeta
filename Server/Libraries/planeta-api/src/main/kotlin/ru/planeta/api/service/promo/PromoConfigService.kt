package ru.planeta.api.service.promo

import ru.planeta.model.trashcan.PromoConfig

import java.math.BigDecimal

interface PromoConfigService {
    fun selectAll(): List<PromoConfig>

    fun getCompatibleConfigs(campaignTags: Collection<Long>, totalPrice: BigDecimal, email: String): List<PromoConfig>

    fun insert(clientId: Long, promoConfig: PromoConfig)

    fun update(clientId: Long, promoConfig: PromoConfig)

    fun selectById(configId: Long): PromoConfig

    fun switchStatus(clientId: Long, configId: Long)

    fun finishConfigs()
}


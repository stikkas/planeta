package ru.planeta.api.service.campaign

import org.apache.commons.collections4.IterableUtils
import org.apache.commons.lang3.StringUtils
import org.apache.commons.lang3.StringUtils.isNotBlank
import org.apache.commons.lang3.math.NumberUtils
import org.apache.commons.lang3.tuple.Pair
import org.apache.log4j.Logger
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.MessageSource
import org.springframework.context.annotation.Lazy
import org.springframework.context.i18n.LocaleContextHolder
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Service
import ru.planeta.api.Utils
import ru.planeta.api.aspect.transaction.NonTransactional
import ru.planeta.api.aspect.transaction.Transactional
import ru.planeta.api.exceptions.MessageCode
import ru.planeta.api.exceptions.NotFoundException
import ru.planeta.api.exceptions.PermissionException
import ru.planeta.api.helper.ProjectService
import ru.planeta.api.helper.ShareUtils
import ru.planeta.api.mail.MailClient
import ru.planeta.api.mail.UploaderService
import ru.planeta.api.news.LoggerService
import ru.planeta.api.service.billing.order.OrderService
import ru.planeta.api.service.common.PlanetaManagersService
import ru.planeta.api.service.configurations.ConfigurationService
import ru.planeta.api.service.content.PhotoService
import ru.planeta.api.service.geo.AddressService
import ru.planeta.api.service.notifications.NotificationService
import ru.planeta.api.service.profile.ProfileService
import ru.planeta.api.service.profile.ProfileSubscriptionService
import ru.planeta.api.service.security.PermissionService
import ru.planeta.api.service.statistic.UniversalAnalyticsService
import ru.planeta.api.text.FormattingService
import ru.planeta.commons.lang.CollectionUtils
import ru.planeta.commons.lang.DateUtils
import ru.planeta.commons.model.Gender
import ru.planeta.commons.text.Bbcode
import ru.planeta.commons.web.social.fb.FbService
import ru.planeta.dao.ListWithCount
import ru.planeta.dao.commondb.*
import ru.planeta.dao.profiledb.PhotoAlbumDAO
import ru.planeta.dao.profiledb.ProfileDAO
import ru.planeta.dao.profiledb.ProfileSubscriptionDAO
import ru.planeta.model.Constants
import ru.planeta.model.charity.CharityCampaignsStats
import ru.planeta.model.common.PlanetaCampaignManager
import ru.planeta.model.common.UserPrivateInfo
import ru.planeta.model.common.campaign.*
import ru.planeta.model.common.campaign.enums.CampaignStatus
import ru.planeta.model.common.campaign.enums.CampaignStatus.*
import ru.planeta.model.common.campaign.enums.CampaignTargetStatus
import ru.planeta.model.common.campaign.enums.ShareStatus
import ru.planeta.model.common.campaign.param.CampaignEventsParam
import ru.planeta.model.common.delivery.SubjectType
import ru.planeta.model.enums.*
import ru.planeta.model.news.ProfileNews
import ru.planeta.model.news.ProfileNews.Type.CAMPAIGN_REACHED_35
import ru.planeta.model.news.ProfileNews.Type.CAMPAIGN_STARTED
import ru.planeta.model.profile.CampaignBacker
import ru.planeta.model.profile.Comment
import ru.planeta.model.profile.Profile
import ru.planeta.model.profile.media.Photo
import ru.planeta.model.profile.media.PhotoAlbum
import ru.planeta.model.shop.enums.PaymentStatus
import ru.planeta.model.stat.CampaignStat
import java.io.IOException
import java.math.BigDecimal
import java.math.RoundingMode
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.Executors
import java.util.concurrent.TimeUnit
import java.util.concurrent.locks.ReentrantReadWriteLock
import java.util.regex.Pattern

/**
 * User: atropnikov Date: 21.03.12 Time: 16:11
 */
@Service
@Lazy(false)
class CampaignServiceImpl(
        @Value("\${default.campaign.feedback.email}")
        private var defaultCampaignFeedbackEmail: String,
        private val formattingService: FormattingService,
        private val configurationService: ConfigurationService,
        private val campaignContactDAO: CampaignContactDAO,
        private val photoService: PhotoService,
        private val deliveryService: DeliveryService,
        private val planetaManagersService: PlanetaManagersService,
        private val contractorDAO: ContractorDAO,
        private val campaignTagDAO: CampaignTagDAO,
        private var profileNewsService: LoggerService,
        private var profileSubscriptionService: ProfileSubscriptionService,
        private var profileSubscriptionDAO: ProfileSubscriptionDAO,
        private var photoAlbumDAO: PhotoAlbumDAO,
        private var universalAnalyticsService: UniversalAnalyticsService,
        private var addressService: AddressService,
        private var profileService: ProfileService,
        private var campaignPermissionService: CampaignPermissionService,
        private var sponsorDAO: SponsorDAO,
        private var profileDAO: ProfileDAO,
        private var shareDAO: ShareDAO,
        private var campaignDAO: CampaignDAO,
        private var uploaderService: UploaderService,
        private var projectService: ProjectService,
        private var userPrivateInfoDAO: UserPrivateInfoDAO,
        private var permissionService: PermissionService,
        private var mailClient: MailClient,
        private var messageSource: MessageSource,
        private var notificationService: NotificationService) : CampaignService {


    private val logger = Logger.getLogger(CampaignServiceImpl::class.java)
    private val tagsMapLock = ReentrantReadWriteLock()
    private var orderService: OrderService? = null

    override val campaignsCategories: List<CampaignTag>
        get() = campaignTagDAO.selectAll()

    override val totalCollectedAmount: BigDecimal
        get() = campaignDAO.totalCollectedAmount()


    override val charityPromoCampaigns: List<Campaign>
        get() {
            val promoCampaignIds = configurationService.charityPromoCampaignIds

            return if (promoCampaignIds != null) {
                sortCampaigns(campaignDAO.getCampaignsByIds(promoCampaignIds), promoCampaignIds)
            } else emptyList()
        }

    override val charityTopCampaigns: List<Campaign>
        get() {
            val topCampaignIds = configurationService.charityTopCampaignIds

            return if (topCampaignIds != null) {
                sortCampaigns(campaignDAO.getCampaignsByIds(topCampaignIds), topCampaignIds)
            } else emptyList()
        }

    override val purchasedForAllTime: BigDecimal
        get() = campaignDAO.selectPurchasedAmountForAllTime()

    override val allTags: List<CampaignTag>
        get() {
            tagsMapLock.readLock().lock()
            try {
                if (TAGS_MAP.isEmpty()) {
                    tagsMapLock.readLock().unlock()
                    updateTagsMap()
                    tagsMapLock.readLock().lock()
                }
                return ArrayList(TAGS_MAP.values)
            } finally {
                tagsMapLock.readLock().unlock()
            }
        }

    override val sponsors: List<Sponsor>
        get() = sponsorDAO.selectAll()

    override val successfulCharityCampaignsCount: Int
        get() = campaignDAO.getSuccessfulCampaignsCountByTagId(CampaignTag.CHARITY_TAG_ID)

    override val successfulCampaignsCount: Int
        get() = campaignDAO.getSuccessfulCampaignsCountByTagId(null)

    override val campaignsCountPercentagesByCustomTags: List<Pair<String, Long>>
        get() {
            val resultList = ArrayList<Pair<String, Long>>()
            val mapList = campaignDAO.campaignsCountPercentagesByCustomTags()
            for (tempMap in mapList) {
                resultList.add(Pair.of(tempMap["tag_name"] as String, tempMap["percentage"] as Long))
            }
            return resultList
        }

    override fun setOrderService(orderService: OrderService) {
        this.orderService = orderService
    }

    // @PostConstruct
    @Scheduled(fixedDelay = TAGS_UPDATE_DELAY.toLong(), initialDelay = 0)
    fun updateTagsMap() {
        val tags = campaignTagDAO.selectAll()

        tagsMapLock.writeLock().lock()
        try {
            TAGS_MAP.clear()
            for (tag in tags) {
                tag.mnemonicName?.toUpperCase()?.let {
                    TAGS_MAP[it] = tag
                }
            }
        } finally {
            tagsMapLock.writeLock().unlock()
        }
    }

    override fun getCampaignsCategorieById(campaignTagId: Long): CampaignTag {
        return campaignTagDAO.select(campaignTagId)
    }

    @Throws(PermissionException::class)
    override fun save(campaignTag: CampaignTag) {
        if (campaignTag.id == 0) {
            if (StringUtils.isEmpty(campaignTag.sponsorAlias) || campaignTagDAO.selectByAlias(campaignTag.sponsorAlias
                            ?: "") == null) {
                campaignTag.mnemonicName = StringUtils.upperCase(campaignTag.mnemonicName)

                if (StringUtils.isEmpty(campaignTag.sponsorAlias)) {
                    campaignTag.sponsorAlias = null
                }

                if (StringUtils.isEmpty(campaignTag.warnText)) {
                    campaignTag.warnText = null
                }

                campaignTagDAO.insert(campaignTag)
                campaignTagDAO.setTagWithManagerDefaultRelation(campaignTag.id.toLong())

            } else {
                throw PermissionException(MessageCode.PERMISSON_SPONSOR_NOT_UNIQUE)
            }
        } else {
            if (!StringUtils.isEmpty(campaignTag.sponsorAlias)) {
                val tag = campaignTagDAO.selectByAlias(campaignTag.sponsorAlias ?: "")
                if (tag != null && tag.id != campaignTag.id) {
                    throw PermissionException(MessageCode.PERMISSON_SPONSOR_NOT_UNIQUE)
                }
            }

            campaignTag.mnemonicName = StringUtils.upperCase(campaignTag.mnemonicName)

            if (StringUtils.isEmpty(campaignTag.sponsorAlias)) {
                campaignTag.sponsorAlias = null
            }

            if (StringUtils.isEmpty(campaignTag.warnText)) {
                campaignTag.warnText = null
            }

            campaignTagDAO.update(campaignTag)
        }
    }

    override fun getCampaignsByStatus(profileId: Long, statuses: EnumSet<CampaignStatus>, offset: Int, limit: Int): List<Campaign> {
        return campaignDAO.selectCampaignsByStatus(profileId, statuses, offset, limit)
    }


    override fun getFinishedCampaignsByTime(offset: Int, limit: Int): List<Campaign> {
        return campaignDAO.selectFinishedCampaignsByTime(Date(), EnumSet.of(ACTIVE, PAUSED), offset, limit)
    }

    override fun getStartedCampaignsByTime(offset: Int, limit: Int): List<Campaign> {
        return campaignDAO.selectStartedCampaignsByTime(Date(), EnumSet.of(APPROVED), offset, limit)
    }

    override fun getCampaign(campaignId: Long?): Campaign? {
        if (campaignId == null)
            return null
        return campaignDAO.selectCampaignById(campaignId)
    }

    override fun getCampaign(campaignAlias: String): Campaign? {
        return campaignDAO.selectCampaign(campaignAlias)
    }


    override fun getCampaignsForAdminSearch(clientId: Long, query: String,
                                            offset: Int, limit: Int,
                                            managerId: Long?,
                                            dateFrom: Date?, dateTo: Date?,
                                            campaignStatuses: EnumSet<CampaignStatus>, campaignTagId: Int?, stringOrderBy: String): List<Campaign> {

        return campaignDAO.selectCampaignsForAdminSearch(query, offset, limit, managerId,
                dateFrom, dateTo,
                campaignStatuses, campaignTagId, stringOrderBy)
    }

    override fun getCampaignsForAdminSearchCount(clientId: Long, query: String,
                                                 managerId: Long?,
                                                 dateFrom: Date?, dateTo: Date?,
                                                 campaignStatuses: EnumSet<CampaignStatus>, campaignTagId: Int?): Int {

        return campaignDAO.selectCampaignsForAdminSearchCount(query,
                managerId,
                dateFrom, dateTo,
                campaignStatuses, campaignTagId)
    }

    @Throws(PermissionException::class, NotFoundException::class)
    override fun getDraftCampaign(clientId: Long, campaignId: Long): Campaign {
        val campaign = getEditableCampaignSafe(clientId, campaignId)

        for (share in getCampaignDetailedSharesForEdit(campaignId)) {
            campaign.addShare(share)
        }
        return campaign
    }

    @Throws(PermissionException::class, NotFoundException::class)
    override fun undoChanges(clientId: Long, campaignId: Long): Campaign {
        val campaign = getEditableCampaignSafe(clientId, campaignId)

        for (share in getCampaignDetailedSharesForEdit(campaignId)) {
            campaign.addShare(share)
        }
        return campaign
    }

    @Throws(NotFoundException::class)
    override fun getCampaignSafe(campaignId: Long?): Campaign {
        return getCampaign(campaignId) ?: throw NotFoundException(Campaign::class.java, campaignId)
    }

    @Throws(NotFoundException::class, PermissionException::class)
    override fun updateDescriptionAndMetaData(clientId: Long, campaignId: Long, descriptionHtml: String, metaData: String, sponsorAlias: String) {
        if (!permissionService.isPlanetaAdmin(clientId)) {
            throw PermissionException(MessageCode.USER_NOT_ADMIN)
        }
        val campaign = getCampaignForUpdateSafe(campaignId)
        campaign.descriptionHtml = descriptionHtml
        campaign.metaData = metaData
        campaign.sponsorAlias = sponsorAlias
        updateCampaign(campaign)
    }

    @Throws(NotFoundException::class, PermissionException::class)
    override fun getEditableCampaignSafe(clientId: Long, campaignId: Long): Campaign {
        val campaign = getCampaignSafe(campaignId)
        campaignPermissionService.checkIsEditable(clientId, campaign)
        return campaign
    }

    @Throws(NotFoundException::class, PermissionException::class)
    private fun getEditableCampaignForUpdateSafe(clientId: Long, campaignId: Long): Campaign {
        val campaign = getCampaignForUpdateSafe(campaignId)
        campaignPermissionService.checkIsEditable(clientId, campaign)
        return campaign
    }

    @Throws(NotFoundException::class, PermissionException::class)
    override fun getCampaignSafe(clientId: Long, alias: String): Campaign {
        val campaign = getCampaign(alias) ?: throw NotFoundException("Campaign with alias '$alias' not found!")

        checkViewPermission(clientId, campaign)
        return campaign
    }

    @Throws(NotFoundException::class, PermissionException::class)
    override fun getCampaignWithShares(clientId: Long, campaignId: Long, isSharePriceOrderByDesc: Boolean): Campaign {
        val campaign = campaignDAO.selectWithShares(campaignId, isSharePriceOrderByDesc)
                ?: throw NotFoundException(Campaign::class.java, campaignId)
        checkViewPermission(clientId, campaign)
        return campaign
    }

    @Throws(PermissionException::class)
    private fun checkViewPermission(clientId: Long, campaign: Campaign) {
        if (campaign.isDraft && !campaign.isDraftVisible) {
            if (!permissionService.isAdmin(clientId, campaign.creatorProfileId)) {
                throw PermissionException(MessageCode.DRAFT_UNAVAILABLE)
            }
        }

        if (PROTECTED_STATUSES.contains(campaign.status)) {
            if (!permissionService.isAdmin(clientId, campaign.creatorProfileId)) {
                throw PermissionException(MessageCode.USER_NOT_ADMIN)
            }
        }
    }

    @Throws(PermissionException::class, NotFoundException::class)
    override fun insertCampaign(clientId: Long, creatorProfileId: Long, profileId: Long): Campaign {
        campaignPermissionService.checkCanCreate(clientId, creatorProfileId)

        val profile = profileService.getProfileSafe(profileId)
        profileService.getProfileSafe(creatorProfileId)
        if (profile.profileType !== ProfileType.GROUP && profile.profileType !== ProfileType.HIDDEN_GROUP) {
            throw NotFoundException(Profile::class.java, profileId)
        }

        //create default album
        val albumId = insertCampaignAlbum(clientId, profileId)

        val campaign = createDefaultCampaign(creatorProfileId, profileId, albumId)
        insertCampaign(campaign)

        val share = saveShareWithoutCheckPermission(clientId, createDonateShare(campaign))
        campaign.addShare(ShareEditDetails(share))

        val userPrivateInfo = userPrivateInfoDAO.selectByUserId(clientId)
        if (userPrivateInfo?.email != null) {
            val campaignContact = CampaignContact()
            campaignContact.campaignId = campaign.campaignId
            campaignContact.email = userPrivateInfo.email
            campaignContact.contactProfileId = clientId
            campaignContact.contactName = userPrivateInfo.username
            campaignContactDAO.insert(campaignContact)
        }

        profileNewsService.addProfileNews(ProfileNews.Type.CAMPAIGN_CREATED, clientId, campaign.campaignId, campaign.campaignId)
        return campaign

    }

    @Throws(NotFoundException::class, PermissionException::class)
    override fun saveCampaign(clientId: Long, campaign: Campaign, force: Boolean) {
        val campaignId = campaign.campaignId
        val currentCampaign = getEditableCampaignForUpdateSafe(clientId, campaignId)
        val lastTimeUpdated = currentCampaign.timeUpdated

        if (!force && campaign.timeUpdated != lastTimeUpdated) {
            val updaterId = currentCampaign.updaterProfileId
            if (updaterId != clientId) {
                throw PermissionException(MessageCode.CAMPAIGN_CHANGED,
                        arrayOf<Any>(projectService.getUrl(ProjectType.MAIN, updaterId.toString()),
                                profileService.getProfileSafe(updaterId)?.displayName ?: "",
                                SimpleDateFormat("dd.MM.yyyy").format(lastTimeUpdated
                                        ?: Date()), SimpleDateFormat("HH:mm:ss").format(lastTimeUpdated
                                ?: Date())))
            }
        }
        updateCampaignFields(clientId, currentCampaign, campaign)
        setSponsor(currentCampaign)

        updateCampaign(currentCampaign)
        campaignTagDAO.updateTags(campaignId, currentCampaign.tags)
    }

    @Throws(PermissionException::class, NotFoundException::class)
    override fun publishCampaign(clientId: Long, campaignId: Long) {

        val currentCampaign = getEditableCampaignForUpdateSafe(clientId, campaignId)
        val isCampaignJustCreated = currentCampaign.status === CampaignStatus.DRAFT

        // notify about first update after create only (= as campaign creation notification)
        if (!isCampaignJustCreated) {
            return
        }

        val mgr = planetaManagersService.getCampaignManager(currentCampaign.campaignId)
        if (mgr == null || mgr.managerId == 0L) {
            val planetaCampaignManager = PlanetaCampaignManager()
            planetaCampaignManager.campaignId = currentCampaign.campaignId
            val manager = planetaManagersService.getCampaignManagerByTag(currentCampaign.mainTag)
            if (manager == null) {
                val tagName = if (currentCampaign.mainTag == null) " null" else currentCampaign?.mainTag?.mnemonicName
                logger.error("Default manager for category $tagName not set")
            } else {
                planetaCampaignManager.managerId = manager.managerId
                planetaManagersService.setManagerForCurrentObject(planetaCampaignManager, clientId)
            }
        }

        changeCampaignStatusByAuthor(clientId, currentCampaign, NOT_STARTED)
        updateCampaign(currentCampaign)
        notificationService.campaignCreated(clientId, currentCampaign)

    }

    private fun setSponsor(campaign: Campaign) {
        if (campaign.sponsorAlias == null) {
            val s = getCampaignSponsorByTags(campaign.campaignId)
            if (s != null) {
                campaign.sponsorAlias = s.alias
            }
        }
    }

    @Throws(NotFoundException::class)
    override fun updateCampaignStatusSpecialForTheNeedsOfAdmins(clientId: Long, campaignId: Long): Boolean {
        val currentCampaign = getCampaignForUpdateSafe(campaignId)
        if (!campaignPermissionService.isEditable(clientId, currentCampaign)) {
            return false
        }

        when (currentCampaign.status) {
            FINISHED -> currentCampaign.status = CampaignStatus.ACTIVE
            ACTIVE -> currentCampaign.status = CampaignStatus.FINISHED
            else -> return false
        }
        updateCampaign(currentCampaign)

        return true
    }

    override fun getCampaigns(ids: List<Long>): List<Campaign> {
        return campaignDAO.getCampaignsByIds(ids)
    }

    @Throws(PermissionException::class, NotFoundException::class)
    override fun removeCampaign(clientId: Long, campaignId: Long) {
        val currentCampaign = getEditableCampaignForUpdateSafe(clientId, campaignId)
        changeCampaignStatusByAuthor(clientId, currentCampaign, DELETED)
        updateCampaign(currentCampaign)
    }

    @Throws(PermissionException::class, NotFoundException::class)
    override fun toggleDraftVisible(clientId: Long, campaignId: Long) {
        val campaign = getEditableCampaignSafe(clientId, campaignId)
        campaignDAO.updateDraftVisible(campaignId, !campaign.isDraftVisible)
    }

    @Throws(NotFoundException::class, PermissionException::class)
    override fun startCampaign(clientId: Long, campaignId: Long): Campaign {
        return changeCampaignStatus(clientId, campaignId, CampaignStatus.ACTIVE)
    }

    @Throws(NotFoundException::class, PermissionException::class)
    override fun pauseCampaign(clientId: Long, campaignId: Long): Campaign {
        return changeCampaignStatus(clientId, campaignId, CampaignStatus.PAUSED)
    }

    fun getCampaignShares(campaignId: Long): List<Share> {
        return shareDAO.selectCampaignShares(campaignId, 0, 0)
    }

    @Deprecated("")
    private fun getActiveCampaignShares(campaignId: Long): List<Share> {
        return shareDAO.selectCampaignSharesFiltered(campaignId, EnumSet.of(ShareStatus.ACTIVE, ShareStatus.INVESTING,
                ShareStatus.INVESTING_WITHOUT_MODERATION,
                ShareStatus.INVESTING_ALL_ALLOWED), false, 0, 0)
    }

    override fun getCampaignDetailedShares(campaignId: Long): List<ShareDetails> {
        val shares = getActiveCampaignShares(campaignId)
        val result = ArrayList<ShareDetails>(shares.size)
        for (share in shares) {
            val details = ShareDetails(share)
            details.linkedDeliveries = deliveryService.getLinkedDeliveries(share.shareId, SubjectType.SHARE)
            result.add(details)
        }
        return result
    }

    override fun getCampaignWidgetDetailedShares(campaignId: Long): List<ShareDetails> {
        val shares = shareDAO.selectCampaignSharesFiltered(campaignId, EnumSet.of(ShareStatus.ACTIVE), false, 0, 0)
        val result = ArrayList<ShareDetails>(shares.size)
        for (share in shares) {
            val details = ShareDetails(share)
            details.linkedDeliveries = deliveryService.getEnabledLinkedDeliveries(share.shareId, SubjectType.SHARE)
            result.add(details)
        }
        return result
    }

    override fun getCampaignSharesFiltered(campaignId: Long): List<Share> {
        return shareDAO.selectCampaignSharesFiltered(campaignId, EnumSet.of(ShareStatus.ACTIVE, ShareStatus.INVESTING,
                ShareStatus.INVESTING_ALL_ALLOWED, ShareStatus.INVESTING_WITHOUT_MODERATION), true, 0, 0)
    }

    override fun getCampaignSharesFilteredForWidgets(campaignId: Long): List<Share> {
        return shareDAO.selectCampaignSharesFiltered(campaignId, EnumSet.of(ShareStatus.ACTIVE), true, 0, 0)
    }

    @NonTransactional
    override fun getCampaignBackersNew(campaignId: Long, offset: Int, limit: Int): List<CampaignBacker> {
        val backers = campaignDAO.selectCampaignBackers(campaignId, ShareUtils.getOrderObjectTypes(), offset, limit)
        if (backers.isEmpty()) {
            return backers
        }
        val ids = ArrayList<Long>()
        for (b in backers) {
            ids.add(b.profileId)
        }
        val profiles = profileDAO.selectSearchedByIds(ids)
        val profilesMap = HashMap<Long, Profile>(profiles.size)
        for (p in profiles) {
            profilesMap[p.profileId] = p
        }
        for (b in backers) {
            val p = profilesMap[b.profileId]
            b.displayName = p?.displayName
            b.alias = p?.alias
            b.userGender = p?.userGender ?: Gender.NOT_SET
            b.imageUrl = p?.imageUrl
            b.cityName = p?.cityName
            b.countryName = p?.countryName
            b.cityNameEng = p?.cityNameEng
            b.countryNameEng = p?.countryNameEng
        }
        return backers
    }

    override fun getCampaignBackersCount(campaignId: Long): Int {
        return campaignDAO.getCampaignBackerCount(campaignId)
    }

    override fun getShare(shareId: Long): Share? {
        return shareDAO.select(shareId)
    }

    @Throws(NotFoundException::class)
    override fun getShareSafe(shareId: Long): Share {
        return getShare(shareId) ?: throw NotFoundException(Share::class.java, shareId)
    }

    @Throws(NotFoundException::class)
    private fun saveShareWithoutCheckPermission(clientId: Long, share: Share): Share {
        var share = share
        if (share.shareId <= 0) {
            share.descriptionHtml = formattingService.replaceExternalLinks(share.descriptionHtml)
            if ("null" == share.imageUrl) {
                share.imageUrl = ""
            }
            share.timeAdded = Date()
            shareDAO.insert(share)
            profileNewsService.addShareNews(ProfileNews.Type.SHARE_ADDED, clientId, share, share.campaignId ?: 0)
        } else {
            val currentShare = getShareSafe(share.shareId)
            currentShare.name = StringUtils.trim(share.name)
            currentShare.nameHtml = currentShare.name
            currentShare.description = StringUtils.trim(share.description)
            currentShare.descriptionHtml = formattingService.replaceExternalLinks(share.descriptionHtml)
            currentShare.imageId = share.imageId
            currentShare.imageUrl = if ("null" == share.imageUrl) "" else share.imageUrl
            currentShare.price = share.price
            currentShare.amount = share.amount
            currentShare.rewardInstruction = StringUtils.trim(share.rewardInstruction)
            currentShare.questionToBuyer = StringUtils.trim(share.questionToBuyer)
            currentShare.order = share.order
            currentShare.shareStatus = share.shareStatus
            currentShare.estimatedDeliveryTime = share.estimatedDeliveryTime

            shareDAO.update(currentShare)
            share = ShareEditDetails(currentShare)
            if (share.shareStatus == ShareStatus.DISABLED) {
                profileNewsService.addShareNews(ProfileNews.Type.SHARE_DELETED, clientId, share, share.campaignId ?: 0)
            } else {
                profileNewsService.addShareNews(ProfileNews.Type.SHARE_CHANGED, clientId, share, share.campaignId ?: 0)
            }
        }
        return share
    }

    @Throws(NotFoundException::class, PermissionException::class)
    override fun saveShare(clientId: Long, share: Share): Share {
        getEditableCampaignSafe(clientId, share.campaignId ?: 0)
        val saved = saveShareWithoutCheckPermission(clientId, share)
        if (share is ShareEditDetails) {
            deliveryService.updateDefaultDeliveryServices(clientId, share)
        }
        return saved
    }

    @Throws(NotFoundException::class, PermissionException::class)
    override fun disableShare(clientId: Long, shareId: Long) {
        validateRemoveShare(clientId, shareId)
        shareDAO.disableShare(shareId)
    }

    @Throws(PermissionException::class)
    private fun verifyGroupAdminOrModerator(clientId: Long, profileId: Long) {
        if (!permissionService.isAdmin(clientId, profileId) && !permissionService.isGroupModerator(clientId, profileId)) {
            throw PermissionException(MessageCode.USER_NOT_ADMIN)
        }
    }

    @Throws(NotFoundException::class, PermissionException::class)
    override fun getShareReadyForPurchase(shareId: Long, count: Int): Share {
        val result = getShareSafe(shareId)
        val campaign = getCampaignSafe(result.campaignId)
        if (CampaignStatus.ACTIVE !== campaign.status) {
            throw PermissionException(MessageCode.CAMPAIGN_NOT_ACTIVE)
        }
        if (result.amount > 0 && result.amount - result.purchaseCount < count) {
            throw PermissionException(MessageCode.NOT_ENOUGH_SHARE_AMOUNT)
        }

        return result
    }

    @Throws(PermissionException::class, NotFoundException::class)
    override fun checkOnCampaignIsFinished(campaignId: Long, clientId: Long) {
        val campaign = getCampaignForUpdateSafe(campaignId)
        checkOnCampaignIsFinished(campaign, clientId)
        updateCampaign(campaign)
    }

    // WARNING do not use update inside
    @Throws(PermissionException::class, NotFoundException::class)
    private fun checkOnCampaignIsFinished(campaign: Campaign, clientId: Long) {

        if (campaign.isFinishOnTargetReach) {
            if (isTargetAmountCollected(campaign)) { // only target amount is specified
                changeCampaignStatus(CampaignStatus.FINISHED, campaign, clientId)
            }
            return
        }

        // both finish time and target amount are specified
        if (isCampaignTimeFinished(campaign)) { // both finish time and target amount are specified, and time has expired
            changeCampaignStatus(CampaignStatus.FINISHED, campaign, clientId)
        } else {
            logger.error("Try finish campaign " + campaign.campaignId)
        }

    }

    @Throws(PermissionException::class, NotFoundException::class)
    override fun checkOnCampaignHasToStart(campaignId: Long, clientId: Long) {
        val campaign = getCampaignForUpdateSafe(campaignId)
        checkOnCampaignHasToStart(campaign, clientId)
        updateCampaign(campaign)
    }

    // WARNING do not use update inside
    @Throws(PermissionException::class, NotFoundException::class)
    private fun checkOnCampaignHasToStart(campaign: Campaign, clientId: Long) {

        if (campaign.status != CampaignStatus.APPROVED) {
            return
        }

        // start time is specified and before now
        if (isItTimeToStartCampaign(campaign)) {
            changeCampaignStatus(CampaignStatus.ACTIVE, campaign, clientId)
        } else {
            logger.error("Try start campaign " + campaign.campaignId)
        }

    }

    // NOT use update inside
    @Throws(PermissionException::class)
    private fun updateAfterSharePurchaseManipulation(campaign: Campaign, count: Int, amount: BigDecimal, shareId: Long) {
        run {
            val selected = shareDAO.selectForUpdate(shareId)
            val newCount = selected.purchaseCount + count
            if (newCount < 0) {
                throw PermissionException(MessageCode.SHARE_PURCHASE_ALREADY_CANCELLED)
            }
            val newAmount = selected.purchaseSum.add(amount)
            if (BigDecimal.ZERO.compareTo(newAmount) > 0) {
                throw PermissionException(MessageCode.SHARE_PURCHASE_ALREADY_CANCELLED)
            }
            selected.purchaseCount = newCount
            selected.purchaseSum = newAmount
            shareDAO.update(selected)
        }

        val newCount = campaign.purchaseCount + count
        if (newCount < 0) {
            throw PermissionException(MessageCode.SHARE_PURCHASE_ALREADY_CANCELLED)
        }

        val newAmount = campaign.purchaseSum.add(amount)
        if (BigDecimal.ZERO.compareTo(newAmount) > 0) {
            throw PermissionException(MessageCode.SHARE_PURCHASE_ALREADY_CANCELLED)
        }

        campaign.purchaseCount = campaign.purchaseCount + count
        campaign.purchaseSum = newAmount
        setCollectedAmount(campaign, amount)
        if (amount.signum() > 0) {
            campaign.timeLastPurchased = Date()
        }
    }

    private fun setCollectedAmount(campaign: Campaign, amount: BigDecimal) {
        if (campaign.targetStatus !== CampaignTargetStatus.FAIL) {
            if (campaign.sponsorAlias != null) {
                val sponsor = sponsorDAO.selectSponsor(campaign.sponsorAlias ?: "")
                if (sponsor != null) {
                    computeAmountWithSponsor(campaign, amount, sponsor.maxDonate, sponsor.multiplier)
                } else {
                    logger.error("There is no sponsor with alias = [" + campaign.sponsorAlias + "] in sponsors table.")
                    campaign.collectedAmount = campaign.purchaseSum
                }
            } else {
                campaign.collectedAmount = campaign.purchaseSum
            }
        }
    }

    @Throws(PermissionException::class, NotFoundException::class)
    override fun updateAfterSharePurchase(campaignId: Long, count: Int, amount: BigDecimal, shareId: Long) {
        val campaign = getCampaignForUpdateSafe(campaignId)
        updateAfterSharePurchaseManipulation(campaign, count, amount, shareId)
        checkOnCampaignCanMakeVipOffer(campaign, amount)
        checkOnCampaignIsSuccess(campaign)
        updateCampaign(campaign)

        if (campaign.status !== CampaignStatus.FINISHED) {
            val newsType = getCampaignNews(campaign, amount)
            if (newsType != null) {
                profileNewsService.addProfileNews(newsType, campaign.creatorProfileId, campaign.campaignId, campaign.campaignId)
            }
            if (check35PercentNews(campaign, amount)) {
                profileNewsService.addProfileNews(CAMPAIGN_REACHED_35, campaign.creatorProfileId, campaign.campaignId, campaign.campaignId)
            }

        }
    }


    private fun getCampaignSponsorByTags(campaignId: Long): Sponsor? {
        val sponsors = sponsorDAO.selectAll()
        val tags = campaignTagDAO.selectTagsByCampaign(campaignId)
        var sponsor: Sponsor? = null
        if (!Utils.empty(sponsors) && !Utils.empty(tags)) {
            for (tag in tags) {
                if (tag.sponsorAlias == null) {
                    continue
                }
                for (s in sponsors) {
                    if (s.alias == tag.sponsorAlias) {
                        sponsor = s
                        break
                    }
                }
            }
        }
        return sponsor
    }

    // WARNING not use update inside
    @Throws(NotFoundException::class)
    private fun checkOnCampaignCanMakeVipOffer(campaign: Campaign, lastPurchaseAmount: BigDecimal) {
        if (campaign.isCanMakeVipOffer || campaign.isIgnoreTargetAmount || isCharityCampaign(campaign)) {
            return
        }

        val targetHalf = campaign.targetAmount?.divide(BigDecimal(2), 2, RoundingMode.HALF_UP) ?: return
        val collected = campaign.collectedAmount
        if (isTargetReached(targetHalf, collected.subtract(lastPurchaseAmount), collected)) {
            campaign.isCanMakeVipOffer = true

            val contacts = campaignContactDAO.selectList(campaign.campaignId, 0, 0)
            val emails = getEmails(contacts)
            notificationService.sendCampaignCanMakeVipOfferNotification(emails)
        }
    }

    private fun isCharityCampaign(campaign: Campaign): Boolean {
        val tags = campaignTagDAO.selectTagsByCampaign(campaign.campaignId)
        return IterableUtils.matchesAny(tags) { `object` -> CampaignTag.CHARITY == `object`.mnemonicName }
    }

    // WARNING - use only in OrderService all permission rights check there
    @Throws(PermissionException::class, NotFoundException::class)
    override fun updateAfterSharePurchaseCancellation(clientId: Long, campaignId: Long, count: Int, amount: BigDecimal, shareId: Long) {

        val campaign = getCampaignForUpdateSafe(campaignId)
        updateAfterSharePurchaseManipulation(campaign, -count, amount.negate(), shareId)

        val isSucceed = campaign.targetStatus === CampaignTargetStatus.SUCCESS
        val canContinue = campaign.isFinishOnTargetReach || !isCampaignTimeFinished(campaign)

        if (isSucceed && canContinue && !isTargetAmountCollected(campaign)) {
            campaign.targetStatus = CampaignTargetStatus.NONE
        }
        updateCampaign(campaign)
    }

    /**
     * Updates currentCampaign fields by not null campaign fields changes
     * campaign status from DRAFT on CampaignEdit.Section.ALL
     */
    @Throws(PermissionException::class, NotFoundException::class)
    private fun updateCampaignFields(clientId: Long, currentCampaign: Campaign, campaign: Campaign) {

        updateCampaignFieldsCommon(currentCampaign, campaign)

        updateDescription(currentCampaign, campaign)

        if (currentCampaign.purchaseCount == 0 || permissionService.hasAdministrativeRole(clientId)) {
            currentCampaign.targetAmount = campaign.targetAmount
            currentCampaign.timeFinish = DateUtils.getEndOfDate(campaign.timeFinish)
        }

        currentCampaign.updaterProfileId = clientId
        currentCampaign.timeUpdated = Date()
        if (permissionService.hasAdministrativeRole(clientId)) {
            currentCampaign.timeStart = campaign.timeStart
        }
        campaign.timeUpdated = currentCampaign.timeUpdated
    }

    private fun updateDescription(currentCampaign: Campaign, campaign: Campaign) {
        if (StringUtils.isEmpty(campaign.descriptionHtml)) {
            campaign.description = getTransformedDescription(campaign.description, campaign.profileId ?: -1)
            campaign.descriptionHtml = Bbcode.transform(campaign.description)
            campaign.descriptionHtml = formattingService.replaceExternalLinks(campaign.descriptionHtml)
        }
        if (StringUtils.isEmpty(campaign.description)) {
            campaign.description = StringUtils.EMPTY
        }
        currentCampaign.description = campaign.description
        currentCampaign.descriptionHtml = getTransformedDescriptionHtml(campaign)
    }

    fun getTransformedDescriptionHtml(campaign: Campaign): String {
        val cleanHtml = formattingService.cleanHtml(campaign.descriptionHtml)

        val profileId = campaign.profileId
        val photosUrlsMap = formattingService.getRichPhotosOfWrongOwner(profileId ?: -1, cleanHtml)

        var photosMap: MutableMap<String, Photo> = HashMap()
        for ((key, value) in photosUrlsMap) {
            val photo = Photo()
            photo.imageUrl = value
            photosMap[key] = photo
        }

        photosMap = photoService.transformPhotosToCampaignAlbum(profileId
                ?: -1, 0, Constants.ALBUM_USER_HIDDEN, photosMap).toMutableMap()

        val matcher = PHOTO_HTML_PATTERN.matcher(cleanHtml)
        val sb = StringBuffer()
        while (matcher.find()) {
            val photoToReplace = photosMap[matcher.group(1)]
            if (photoToReplace != null) {
                val replacement = replacePhotoHtml(matcher.group(), photoToReplace)
                matcher.appendReplacement(sb, replacement)
            }

        }
        matcher.appendTail(sb)
        return sb.toString()
    }

    private fun getTransformedDescription(description: String?, profileId: Long): String? {
        var campaignDescription = description
        val photosUrlsMap = Bbcode.getUrlMap(profileId, campaignDescription)

        var photosMap: MutableMap<String, Photo> = HashMap()
        for ((key, value) in photosUrlsMap) {
            val photo = Photo()
            photo.imageUrl = value
            photosMap[key] = photo
        }

        photosMap = photoService.transformPhotosToCampaignAlbum(profileId, 0, Constants.ALBUM_USER_HIDDEN, photosMap).toMutableMap()

        for ((oldPhotoId, photo) in photosMap) {
            val newPhotoId = photo.photoId.toString()
            campaignDescription = Bbcode.transformCampaignDescription(profileId, campaignDescription, oldPhotoId, newPhotoId)
        }
        return campaignDescription
    }

    /**
     * Creates donate share for specified campaign with unbounded amount and
     * zero price.
     */
    private fun createDonateShare(campaign: Campaign): ShareEditDetails {
        val result = Share()
        result.shareStatus = ShareStatus.ACTIVE
        result.profileId = campaign.profileId ?: -1
        result.campaignId = campaign.campaignId
        result.price = BigDecimal.ZERO
        result.name = messageSource.getMessage(MessageCode.SHARE_DONATE_NAME.errorPropertyName, null,
                MessageCode.SHARE_DONATE_NAME.errorPropertyName, LocaleContextHolder.getLocale())
        result.description = messageSource.getMessage(MessageCode.SHARE_DONATE_DESCRIPTION.errorPropertyName, null,
                MessageCode.SHARE_DONATE_DESCRIPTION.errorPropertyName, LocaleContextHolder.getLocale())

        return ShareEditDetails(result)
    }

    /**
     * Insert new photo album for campaign
     *
     * @param clientId  inserting user profile id
     * @param profileId campaign group-owner profile id
     * @return created album id
     */
    // TODO do we need this?
    private fun insertCampaignAlbum(clientId: Long, profileId: Long): Long {
        val album = PhotoAlbum()
        album.profileId = profileId
        album.authorProfileId = clientId
        album.albumTypeId = Constants.ALBUM_CAMPAIGN
        album.viewPermission = PermissionLevel.EVERYBODY
        album.timeAdded = Date()
        album.timeUpdated = Date()
        album.photosCount = 0
        album.viewsCount = 0
        photoAlbumDAO.insert(album)

        return album.getAlbumId() ?: 0
    }

    @Throws(PermissionException::class, NotFoundException::class)
    override fun changeCampaignStatus(clientId: Long, campaignId: Long, status: CampaignStatus): Campaign {
        return changeCampaignStatusByGlobalManagerInner(clientId, campaignId, status)
    }

    @Throws(PermissionException::class, NotFoundException::class)
    private fun changeCampaignStatusByGlobalManagerInner(clientId: Long, campaignId: Long, status: CampaignStatus): Campaign {
        if (!permissionService.hasAdministrativeRole(clientId)) {
            throw PermissionException(MessageCode.GLOBAL_ADMIN_PERMISSION_REQUIRED)
        }
        val campaign = getCampaignForUpdateSafe(campaignId)
        if (campaign.status === status) {
            return campaign
        }

        if (!CampaignStatusUtils.isNextActionVisible(campaign.status, status) && !permissionService.isSuperAdmin(clientId)) {
            throw PermissionException(MessageCode.CAMPAIGN_ILLEGAL_STATUS_TRANSITION)
        }

        changeCampaignStatusByHuman(status, campaign, clientId)
        updateCampaign(campaign)
        if (campaign.status === CampaignStatus.PATCH) {
            notificationService.sendEmailCampaignInStatusPatchToAdmins(campaign)
        }
        return campaign
    }


    // NOT use update inside
    @Throws(PermissionException::class, NotFoundException::class)
    private fun changeCampaignStatusByAuthor(clientId: Long, campaign: Campaign, campaignStatus: CampaignStatus) {
        if (campaign.status !== campaignStatus) {
            changeCampaignStatusByHuman(campaignStatus, campaign, clientId)
        }
    }

    @Throws(NotFoundException::class, PermissionException::class)
    override fun getMyDraftCampaigns(clientId: Long): List<Campaign> {
        val draftCampaigns = ArrayList<Campaign>()
        val groupsIds = profileSubscriptionDAO.selectUserOwnGroupsId(clientId)
        groupsIds.add(clientId)
        draftCampaigns.addAll(campaignDAO.selectLiteDraftCampaignsByOwnerId(groupsIds))
        return draftCampaigns
    }

    @Throws(PermissionException::class)
    private fun changeCampaignStatusByHuman(status: CampaignStatus, campaign: Campaign, clientId: Long) {
        changeCampaignStatus(status, campaign, clientId)
    }

    // WARNING not use update inside
    @Throws(PermissionException::class)
    private fun changeCampaignStatus(status: CampaignStatus, campaign: Campaign, clientId: Long) {
        if (status === CampaignStatus.DELETED) {
            getCampaignsCountWithPurchasedOrReservedShares(campaign.campaignId)?.let {
                if (it > 0) {
                    throw PermissionException(MessageCode.CAMPAIGN_HAS_PURCHASED_OR_RESERVED_SHARES)
                }
            }
            campaign.campaignAlias = null
        }

        if (status === CampaignStatus.ACTIVE || status === CampaignStatus.APPROVED) {
            checkContractor(campaign)
        }

        if (status === CampaignStatus.ACTIVE) {
            campaign.targetStatus = CampaignTargetStatus.NONE
        }

        if (status === CampaignStatus.ACTIVE && campaign.timeStart == null) {
            campaign.timeStart = Date()
        }

        if (status === CampaignStatus.FINISHED) {
            val now = Date()
            if (campaign.timeFinish == null || (campaign.timeFinish?.compareTo(now) ?: 0) > 0) {
                campaign.timeFinish = now
            }
            if (campaign.isFinishOnTargetReach || !campaign.isIgnoreTargetAmount) {
                campaign.targetStatus = if (isCampaignSuccessful(campaign)) CampaignTargetStatus.SUCCESS else CampaignTargetStatus.FAIL
            }
        }

        val prevStatus = campaign.status

        campaign.status = status
        campaign.timeUpdated = Date()

        delay(Runnable {
            profileDAO.updateProjectsCount(campaign.creatorProfileId)
            logger.info("Projects count for profile " + campaign.creatorProfileId + " updated")
            profileDAO.updateHasActiveProjects(campaign.creatorProfileId)
            logger.info("Active projects flag for profile " + campaign.creatorProfileId + " updated")

            val newsType = getCampaignNews(prevStatus, status, campaign.targetStatus)
            if (newsType != null) {
                // hack to show in news
                val newsProfileId: Long
                if (newsType.isInternal) {
                    newsProfileId = clientId
                } else {
                    newsProfileId = campaign.creatorProfileId
                }
                val extraParams = HashMap<String, String>()
                extraParams["profileId"] = java.lang.Long.toString(clientId)
                profileNewsService.addProfileNews(newsType, newsProfileId, campaign.campaignId, campaign.campaignId, extraParams)
            }

            universalAnalyticsService.trackChangeCampaignStatus(clientId, prevStatus, status, campaign.name, campaign.targetStatus)
        }, 1000, TimeUnit.MILLISECONDS)

        if (status === CampaignStatus.ACTIVE) {
            delay(Runnable {
                val url = getCampaignUrl(campaign.webCampaignAlias)
                val response = FbService.refreshFbCache(url)
                logger.info(response)
            }, 100, TimeUnit.MILLISECONDS)
        }
    }

    private fun getCampaignUrl(webCampaignAlias: String?): String {
        return projectService.getUrl(ProjectType.MAIN, "/campaigns/" + webCampaignAlias)
    }

    @Throws(NotFoundException::class)
    private fun getCampaignForUpdateSafe(campaignId: Long): Campaign {
        return campaignDAO.selectForUpdate(campaignId) ?: throw NotFoundException(Campaign::class.java, campaignId)
    }

    @Throws(PermissionException::class)
    private fun checkContractor(campaign: Campaign) {
        val contractor = contractorDAO.selectByCampaignId(campaign.campaignId)
        if (contractor == null || contractor.contractorId <= 0) {
            throw PermissionException(MessageCode.CONTRACTOR_REQUIRED)
        }

        if (contractor.type === ContractorType.INDIVIDUAL && StringUtils.isBlank(contractor.passportNumber)) {
            throw PermissionException(MessageCode.CONTRACTOR_PASSPORT_ERROR)
        }

    }

    @Throws(NotFoundException::class)
    override fun createCampaignComment(authorId: Long, shareId: Long, comment: String): Comment {
        val share = getShareSafe(shareId)
        val campaign = getCampaignSafe(share.campaignId)

        val result = Comment()
        result.authorProfileId = authorId
        result.objectId = campaign.campaignId
        result.profileId = campaign.profileId
        result.objectType = ObjectType.CAMPAIGN
        result.text = comment
        result.timeAdded = Date()

        return result
    }

    override fun getCampaignsCount(profileId: Long, status: CampaignStatus): Int {
        return campaignDAO.getCampaignsCount(profileId, status)
    }

    override fun getCampaignsCount(profileId: Long, statuses: EnumSet<CampaignStatus>): Int {
        return campaignDAO.getCampaignsCountByStatuses(profileId, statuses)
    }

    override fun getCampaignsByDateRange(timeStartFrom: Date?, timeStartTo: Date?, timeFinishFrom: Date?, timeFinishTo: Date?): List<Campaign> {
        return campaignDAO.selectCampaignsByDateRange(timeStartFrom, timeStartTo, timeFinishFrom, timeFinishTo)
    }

    override fun getCampaignContacts(clientId: Long, campaignId: Long, offset: Int, limit: Int): List<CampaignContact> {
        return campaignContactDAO.selectList(campaignId, offset, limit)
    }

    @Throws(PermissionException::class, NotFoundException::class)
    override fun saveCampaignContact(clientId: Long, campaignContact: CampaignContact) {
        val campaign = getCampaignSafe(campaignContact.campaignId)
        verifyGroupAdminOrModerator(clientId, campaign.profileId ?: -1)

        campaignContactDAO.insert(campaignContact)
    }

    @Throws(NotFoundException::class, PermissionException::class)
    override fun deleteCampaignContact(clientId: Long, campaignId: Long, email: String) {
        val campaign = getCampaignSafe(campaignId)
        verifyGroupAdminOrModerator(clientId, campaign.profileId ?: -1)

        campaignContactDAO.delete(campaignId, email)
    }

    @Throws(NotFoundException::class, PermissionException::class)
    override fun updateCampaignContactList(clientId: Long, campaignId: Long, emailList: List<String>) {
        val campaign = getCampaignSafe(campaignId)
        verifyGroupAdminOrModerator(clientId, campaign.profileId ?: -1)

        val campaignContactList = getCampaignContacts(clientId, campaignId, 0, 0)
        for (campaignContact in campaignContactList) {
            var isExists = false
            for (email in emailList) {
                if (isNotBlank(email) && email.equals(campaignContact.email, ignoreCase = true)) {
                    isExists = true
                    break
                }
            }
            if (!isExists) {
                deleteCampaignContact(clientId, campaignId, campaignContact.email ?: "")
            }
        }

        for (email in emailList) {
            var isExists = false
            for (campaignContact in campaignContactList) {
                if (isNotBlank(email) && email.equals(campaignContact.email, ignoreCase = true)) {
                    isExists = true
                    break
                }
            }
            if (!isExists) {
                val newCampaignContact = CampaignContact()
                newCampaignContact.campaignId = campaignId
                newCampaignContact.email = email
                val userPrivateInfo = userPrivateInfoDAO.selectByEmail(email)
                if (userPrivateInfo != null) {
                    val profile = profileDAO.selectById(userPrivateInfo.userId)
                    if (profile != null) {
                        newCampaignContact.contactProfileId = profile.profileId
                        newCampaignContact.contactName = profile.displayName
                        newCampaignContact.imageUrl = profile.imageUrl
                    }
                }
                campaignContactDAO.insert(newCampaignContact)
            }
        }
    }

    override fun getCampaignDetailedSharesForEdit(campaignId: Long): List<ShareEditDetails> {
        val shares = getActiveCampaignShares(campaignId)
        val result = ArrayList<ShareEditDetails>(shares.size)
        for (share in shares) {
            val details = ShareEditDetails(share)
            details.linkedDeliveries = deliveryService.getLinkedDeliveries(share.shareId, SubjectType.SHARE)
            result.add(details)
        }
        return result
    }

    @Throws(NotFoundException::class)
    override fun sendCampaignAuthorFeedbackMessage(clientId: Long, campaignId: Long, message: String, senderEmail: String, userDisplayName: String) {
        val campaign = getCampaignSafe(campaignId)

        sendFeedbackOnList(message, campaign, getCampaignContacts(clientId, campaignId, 0, 0), senderEmail, clientId, userDisplayName, false)
    }

    @Throws(NotFoundException::class)
    override fun sendCampaignCuratorsFeedbackMessage(clientId: Long, campaignId: Long, message: String, senderEmail: String, userDisplayName: String) {
        val campaign = getCampaignSafe(campaignId)

        val campaignContacts = campaignDAO.selectCurators(campaignId)
        addSupportTeamToList(campaignContacts.toMutableList())
        sendFeedbackOnList(message, campaign, campaignContacts, senderEmail, clientId, userDisplayName, false)
    }

    @Throws(NotFoundException::class)
    override fun sendCampaignSpamFeedbackMessage(clientId: Long, campaignId: Long, message: String, senderEmail: String, userDisplayName: String) {
        val campaign = getCampaignSafe(campaignId)
        val campaignContacts = ArrayList<CampaignContact>()
        addSupportTeamToList(campaignContacts)
        sendFeedbackOnList(message, campaign, campaignContacts, senderEmail, clientId, userDisplayName, true)
    }

    private fun addSupportTeamToList(campaignContacts: MutableList<CampaignContact>) {
        val supportTeamContact = CampaignContact()
        supportTeamContact.email = defaultCampaignFeedbackEmail
        campaignContacts.add(supportTeamContact)
    }

    private fun sendFeedbackOnList(message: String, campaign: Campaign, campaignContacts: List<CampaignContact>, fromEmail: String, senderUserId: Long, userDisplayName: String, spam: Boolean?) {
        for (contact in campaignContacts) {
            mailClient.sendCampaignFeedbackEmail(contact.email
                    ?: "", message, campaign, fromEmail, senderUserId, userDisplayName, spam)
        }
    }

    @Throws(NotFoundException::class, PermissionException::class)
    override fun getPossibleCampaignContacts(clientId: Long, campaignId: Long): List<CampaignContact> {
        val campaign = getCampaignSafe(campaignId)
        verifyGroupAdminOrModerator(clientId, campaign.profileId ?: -1)

        val contacts = ArrayList<CampaignContact>()

        val adminList = profileSubscriptionService.getAdminList(campaign.creatorProfileId)
        if (adminList != null) {
            for (campaignAdmin in adminList) {
                val userPrivateInfo = userPrivateInfoDAO.selectByUserId(campaignAdmin.profileId)
                if (userPrivateInfo == null || userPrivateInfo.email == null) {
                    continue
                }

                val contact = CampaignContact()
                contact.campaignId = campaign.campaignId
                contact.contactName = campaignAdmin.displayName
                contact.contactProfileId = campaignAdmin.profileId
                contact.email = userPrivateInfo.email
                contact.imageUrl = campaignAdmin.smallImageUrl

                contacts.add(contact)
            }
        }

        if (permissionService.isPlanetaAdmin(clientId)) {
            val planetaAdmins = userPrivateInfoDAO.selectByStatus(EnumSet.of(UserStatus.MANAGER, UserStatus.ADMIN, UserStatus.SUPER_ADMIN))
            if (planetaAdmins != null) {
                val map = HashMap<Long, UserPrivateInfo>()
                for (userPrivateInfo in planetaAdmins) {
                    map[userPrivateInfo.userId] = userPrivateInfo
                }
                val profileList = profileDAO.selectSearchedByIds(map.keys)

                for (profile in profileList) {
                    val planetaAdmin = map[profile.profileId]
                    if (planetaAdmin != null) {
                        val contact = CampaignContact()
                        contact.campaignId = campaign.campaignId
                        contact.contactName = profile.displayName
                        contact.contactProfileId = profile.profileId
                        contact.email = planetaAdmin.email
                        contact.imageUrl = profile.smallImageUrl

                        contacts.add(contact)
                    }
                }
            }
        }

        return contacts
    }

    override fun getCampaignsByContractorId(contractorId: Long, offset: Int, limit: Int): List<Campaign> {
        return campaignDAO.selectCampaignsByContractorId(contractorId, offset, limit)
    }

    override fun getCampaignsCountWithPurchasedOrReservedShares(campaignId: Long): Int? {
        return campaignDAO.selectCampaignsCountWithPurchasedOrReservedShares(campaignId)
    }

    @Throws(NotFoundException::class)
    override fun sendCampaignDeclinedEmail(clientId: Long, campaignId: Long, message: String) {
        val campaign = getCampaignSafe(campaignId)
        val adminList = profileSubscriptionService.getAdminList(campaign.creatorProfileId)
        val creatorProfile = profileService.getProfileSafe(campaign.creatorProfileId)
        if (creatorProfile.profileType === ProfileType.USER) {
            adminList.add(creatorProfile)
        }
        for (admin in adminList) {
            val clientPrivateInfo = userPrivateInfoDAO.selectByUserId(admin.profileId)
            if (clientPrivateInfo == null) {
                logger.error("No such user " + admin.profileId)
                continue
            }
            mailClient.sendCampaignDeclinedEmail(clientPrivateInfo.email ?: "", campaign, message)
        }
    }

    override fun getCampaignsTargetAmount(status: CampaignStatus): BigDecimal {
        return campaignDAO.selectCampaignsTargetAmount(status)
    }

    override fun getCampaignsPurchasedAmount(status: CampaignStatus): BigDecimal {
        return campaignDAO.selectCampaignsPurchasedAmount(status)
    }

    override fun getCampaignsCountBetween(dateBegin: Date, dateEnd: Date, status: CampaignStatus): Int? {
        return campaignDAO.selectCampaignsCountBetween(dateBegin, dateEnd, status)
    }

    //    @Override
    @Throws(NotFoundException::class, PermissionException::class)
    private fun validateRemoveShare(clientId: Long, shareId: Long) {
        val share = getShareSafe(shareId)
        getEditableCampaignSafe(clientId, share.campaignId ?: 0)
        if (!canRemoveShare(share)) {
            throw PermissionException(MessageCode.REJECT_SHARE_DELETE)
        }

    }

    private fun canRemoveShare(share: Share): Boolean {
        return !(share.purchaseCount > 0 || (orderService?.getOrdersCountForObject(share.shareId, OrderObjectType.SHARE,
                EnumSet.of(PaymentStatus.COMPLETED, PaymentStatus.CANCELLED), null) ?: 0) > 0)
    }

    override fun getCampaignTagByMnemonic(mnemonicName: String): CampaignTag? {
        var mnemonicName = mnemonicName
        if (Utils.empty(mnemonicName)) {
            return null
        }
        mnemonicName = mnemonicName.toUpperCase()
        tagsMapLock.readLock().lock()
        try {
            return TAGS_MAP[mnemonicName]
        } finally {
            tagsMapLock.readLock().unlock()
        }
    }

    override fun getCampaignTagsByMnemonic(mnemonicNames: List<String>): List<CampaignTag> {
        if (mnemonicNames == null) {
            return emptyList()
        }
        val campaignTags = ArrayList<CampaignTag>(mnemonicNames.size)
        for (mnemonicName in mnemonicNames) {
            val campaignTag = getCampaignTagByMnemonic(mnemonicName)
            if (campaignTag != null) {
                campaignTags.add(campaignTag)
            }
        }
        return campaignTags
    }

    override fun getCampaignCurators(campaignId: Long): List<CampaignContact> {

        return campaignDAO.selectCurators(campaignId)
    }

    @Throws(PermissionException::class, NotFoundException::class)
    override fun addCampaignCurator(clientId: Long, contact: CampaignContact) {
        addCampaignCurator(clientId, contact.campaignId, contact.email ?: "")
    }

    @Throws(PermissionException::class, NotFoundException::class)
    override fun addCampaignCurator(clientId: Long, campaignId: Long?, email: String) {
        val campaign = getCampaignSafe(campaignId)
        verifyGroupAdminOrModerator(clientId, campaign.profileId ?: -1)

        campaignDAO.insertCampaignCurator(campaignId ?: 0, email)
    }

    @Throws(NotFoundException::class, PermissionException::class)
    override fun deleteCampaignCurator(clientId: Long, campaignId: Long, email: String) {
        val campaign = getCampaignSafe(campaignId)
        verifyGroupAdminOrModerator(clientId, campaign.profileId ?: -1)
        campaignDAO.deleteCampaignCurator(campaignId, email)
    }

    override fun getCampaignContactsCount(campaignId: Long): Int {
        return campaignContactDAO.getCount(campaignId)
    }

    override fun getBackedCampaignList(profileId: Long, offset: Int, limit: Int): List<Campaign> {
        return campaignDAO.getBackedCampaignList(profileId, ShareUtils.getOrderObjectTypes(), offset, limit)
    }

    override fun getUserBackedCampaignTagCountList(profileId: Long): List<Map<*, *>> {
        return campaignDAO.getUserBackedCampaignTagCountList(profileId)
    }

    override fun selectCampaignNewEventsCountList(campaignIds: List<Long>, profileId: Long, dateFrom: Date?): List<CampaignWithNewEventsCount> {
        return campaignDAO.selectCampaignNewEventsCountList(campaignIds, profileId, dateFrom)
    }

    @NonTransactional
    override fun selectCampaignNews(campaignEventsParam: CampaignEventsParam): ListWithCount<CampaignNews> {
        return campaignDAO.selectCampaignNews(campaignEventsParam)
    }

    override fun selectCampaignPosts(campaignEventsParam: CampaignEventsParam): ListWithCount<CampaignNews> {
        return campaignDAO.selectCampaignPosts(campaignEventsParam)
    }

    override fun selectCampaignComments(campaignEventsParam: CampaignEventsParam): ListWithCount<CampaignNews> {
        return campaignDAO.selectCampaignComments(campaignEventsParam)
    }

    @Throws(NotFoundException::class)
    override fun getCampaignId(campaignAlias: String): Long {
        var campaignId = NumberUtils.toLong(campaignAlias, 0)
        if (campaignId == 0L) {
            campaignId = campaignDAO.getCampaignId(campaignAlias)
            if (campaignId == 0L) {
                throw NotFoundException("campaign", "campaignAlias", campaignAlias)
            }
        }
        return campaignId
    }

    override fun getSponsors(aliasList: List<String>): List<Sponsor> {
        return sponsorDAO.selectSponsors(aliasList)
    }

    override fun getAuthorActiveCampaign(creatorProfileId: Long): Campaign {
        return campaignDAO.getAuthorActiveCampaign(creatorProfileId)
    }

    @NonTransactional
    @Throws(NotFoundException::class, PermissionException::class, IOException::class)
    override fun updateCampaignBackground(clientId: Long, campaignId: Long, backgroundUrl: String): Campaign {
        var backgroundUrl: String? = backgroundUrl
        if ("null" == backgroundUrl) {
            backgroundUrl = null
        }
        val campaign = updateCampaignBackgroundInner(clientId, campaignId, backgroundUrl)
        if (backgroundUrl != null) {
            val photo = uploaderService.renewBackground(clientId, campaignId)
            if (photo != null) {
                campaign.backgroundImageUrl = photo.imageUrl
            }
        }

        return campaign
    }

    override fun selectPurchasedShareIds(buyerId: Long, campaignId: Long): List<Long> {
        return campaignDAO.selectPurchasedShareIds(buyerId, campaignId)
    }

    override fun selectPurchasedCampaignIds(buyerId: Long): List<Long> {
        return campaignDAO.selectPurchasedCampaignIds(buyerId)
    }

    override fun getCampaignBySponsorAlias(alias: String, offset: Int, limit: Int): List<Campaign> {
        return campaignDAO.selectCampaignBySponsorAlias(alias, offset, limit)
    }

    @Transactional
    @Throws(NotFoundException::class, PermissionException::class)
    private fun updateCampaignBackgroundInner(clientId: Long, campaignId: Long, backgroundUrl: String?): Campaign {
        val campaign = getCampaignForUpdateSafe(campaignId)
        campaignPermissionService.checkCanCreate(clientId, campaign.creatorProfileId)
        campaign.backgroundImageUrl = backgroundUrl
        updateCampaign(campaign)
        return campaign
    }

    override fun getGtmCampaignInfoList(campaignIds: List<Long>): List<GtmCampaignInfo> {
        return campaignDAO.getGtmCampaignInfoList(campaignIds)
    }

    override fun getCollectedAmount(compainId: Long): BigDecimal {
        return campaignDAO.getCollectedAmount(compainId)
    }

    override fun countOrganizationCharityCampaigns(): Int {
        return campaignDAO.countOrganizationCharityCampaigns()
    }

    override fun charityCampaignsStats(date: Date): List<CharityCampaignsStats> {
        return campaignDAO.charityCampaignsStats(date)
    }

    override fun getDetailedShareForClient(clientId: Long, shareId: Long): ShareDetails? {
        val details = shareDAO.selectDetailed(shareId)
        details.linkedDeliveries = deliveryService.getEnabledLinkedDeliveries(shareId, SubjectType.SHARE)
        if (clientId > 0) {
            details.address = addressService.getLastPostAddress(clientId)
        }
        return details
    }

    override fun selectProfileIdsOfCampaignsWithEventsForDay(offset: Int, limit: Int, date: Date): List<Long> {
        return campaignDAO.selectProfileIdsOfCampaignsWithEventsForDay(offset, limit, date)
    }

    override fun selectCampaignsWithEventsForDayByProfileId(profileId: Long, offset: Int, limit: Int, date: Date): List<Campaign> {
        return campaignDAO.selectCampaignsWithEventsForDayByProfileId(profileId, offset, limit, date)
    }

    override fun selectAggregationStatsForDayByCampaignId(campaignId: Long, date: Date): CampaignStat {
        return campaignDAO.selectAggregationStatsForDayByCampaignId(campaignId, date)
    }

    override fun selectCampaignCommentsCountByDate(date: Date, commentsType: ObjectType, campaignId: Long): Int {
        return campaignDAO.selectCampaignCommentsCountByDate(date, commentsType, campaignId)
    }

    override fun getUserCampaignsStatuses(profileId: Long): List<MyCampaignsStatusesCount> {
        return campaignDAO.getUserCampaignsStatuses(profileId)
    }

    override fun getUserPurchasedCampaignsStatuses(profileId: Long): List<MyCampaignsStatusesCount> {
        return campaignDAO.getUserPurchasedCampaignsStatuses(profileId)
    }

    override fun getUserPurchasedAndRewardsCount(profileId: Long): List<MyPurchasesAndRewardsCount> {
        return campaignDAO.getUserPurchasedAndRewardsCount(profileId)
    }

    fun delay(task: Runnable, delay: Long, timeUnit: TimeUnit) {
        DELAYED_TASKS_EXECUTOR.schedule(task, delay, timeUnit)
    }

    companion object {

        private val DELAYED_TASKS_EXECUTOR = Executors.newSingleThreadScheduledExecutor()
        private const val TAGS_UPDATE_DELAY = 15 * 60 * 1000 // 15  minutes
        private val PHOTO_HTML_PATTERN = Pattern.compile("<p:photo.*?id=\"(\\d+)\".*?>")

        // we use LinkedHashMap to preserve insertion order of tags
        private val TAGS_MAP = LinkedHashMap<String, CampaignTag>()
        private val PROTECTED_STATUSES = EnumSet.of<CampaignStatus>(CampaignStatus.DECLINED, CampaignStatus.PAUSED)


        @Throws(PermissionException::class, NotFoundException::class)
        private fun checkOnCampaignIsSuccess(campaign: Campaign) {

            if (campaign.isFinishOnTargetReach) {
                if (isTargetAmountCollected(campaign)) { // only target amount is specified
                    markCampaignSuccessful(campaign)
                }
                return
            }

            if (campaign.isIgnoreTargetAmount) {
                if (campaign.purchaseSum.compareTo(BigDecimal.ZERO) > 0) {
                    markCampaignSuccessful(campaign)
                }
                return
            }

            if (CampaignTargetStatus.SUCCESS !== campaign.targetStatus && isTargetAmountCollected(campaign)) {
                markCampaignSuccessful(campaign)
            }
        }

        private fun markCampaignSuccessful(campaign: Campaign) {
            campaign.targetStatus = CampaignTargetStatus.SUCCESS
        }

        private fun isCampaignTimeFinished(campaign: Campaign): Boolean {
            return campaign.timeFinish?.before(Date()) == true
        }

        private fun isItTimeToStartCampaign(campaign: Campaign): Boolean {
            return campaign.timeStart?.before(Date()) == true
        }

        private fun isTargetAmountCollected(campaign: Campaign): Boolean {
            val res = campaign.targetAmount?.compareTo(campaign.collectedAmount) ?: return false
            return res <= 0
        }

        private fun isCampaignSuccessful(campaign: Campaign): Boolean {
            return if (campaign.isInvesting) {
                val res = campaign.targetAmount?.compareTo(campaign.collectedAmount)
                res != null && res <= 0
            } else {
                val res = campaign.targetAmount?.compareTo(campaign.collectedAmount.multiply(BigDecimal.valueOf(2)))
                res != null && res <= 0
            }

        }

        private fun getCampaignNews(campaign: Campaign, payment: BigDecimal): ProfileNews.Type? {
            var result: ProfileNews.Type? = null

            val target = campaign.targetAmount?.toInt() ?: -1
            if (target > 0) {
                val before = campaign.collectedAmount.subtract(payment).toInt()
                val after = campaign.collectedAmount.toInt()
                if (campaign.isInvesting) {
                    if (isTargetReached(target, before, after)) {
                        result = ProfileNews.Type.CAMPAIGN_REACHED_100
                    }
                    return result
                }
                if (isTargetReached(target, before, after)) {
                    result = ProfileNews.Type.CAMPAIGN_REACHED_100
                } else if (isTargetReached(target * 3 / 4, before, after)) {
                    result = ProfileNews.Type.CAMPAIGN_REACHED_75
                } else if (isTargetReached(target / 2, before, after)) {
                    result = ProfileNews.Type.CAMPAIGN_REACHED_50
                } else if (isTargetReached(target / 4, before, after)) {
                    result = ProfileNews.Type.CAMPAIGN_REACHED_25
                } else if (isTargetReached(target / 5, before, after)) {
                    result = ProfileNews.Type.CAMPAIGN_REACHED_20
                } else if (isTargetReached(target * 15 / 100, before, after)) {
                    result = ProfileNews.Type.CAMPAIGN_REACHED_15
                }
            }

            return result
        }


        // 35% news need to be created even if campaign reached 50% and more (see comment here https://planeta.atlassian.net/browse/PLANETA-17819)
        private fun check35PercentNews(campaign: Campaign, payment: BigDecimal): Boolean {
            val target = campaign.targetAmount?.toInt() ?: return false
            if (target > 0) {
                val before = campaign.collectedAmount.subtract(payment).toInt()
                val after = campaign.collectedAmount.toInt()
                if (isTargetReached(target * 35 / 100, before, after)) {
                    return true
                }
            }
            return false
        }

        private fun <T> isTargetReached(target: Comparable<T>, amountBefore: T, amountAfter: T): Boolean {
            return target.compareTo(amountBefore) > 0 && target.compareTo(amountAfter) <= 0
        }

        // not private for tests
        fun computeAmountWithSponsor(campaign: Campaign, amount: BigDecimal, sponsorMaxDonate: Long?, multiplier: Int) {
            val maxDonate = sponsorMaxDonate ?: (campaign.targetAmount?.toLong() ?: 0) * (multiplier - 1) / multiplier
            val sponsorRest = maxDonate - campaign.collectedAmount.toLong() * (multiplier - 1) / multiplier
            if (sponsorRest < amount.toLong() * (multiplier - 1) && amount.toLong() > 0 || amount.toLong() < 0 && sponsorRest * multiplier < amount.toLong()) {
                campaign.collectedAmount = campaign.collectedAmount.add(amount).add(BigDecimal(Math.max(sponsorRest, 0)))
            } else {
                campaign.collectedAmount = campaign.purchaseSum.multiply(BigDecimal(multiplier))
            }
        }

        private fun getEmails(contacts: List<CampaignContact>): Collection<String> {
            return ru.planeta.commons.lang.CollectionUtils.map(contacts,
                    CollectionUtils.MapFunction { contact -> contact.email })
        }


        private fun sortCampaigns(campaignList: List<Campaign>, campaignIds: List<Long>): List<Campaign> {
            val campaigns = arrayOfNulls<Campaign>(campaignIds.size)
            val result = ArrayList<Campaign>()
            for (campaign in campaignList) {
                val idx = campaignIds.indexOf(campaign.campaignId)
                campaigns[idx] = campaign
            }
            for (campaign in campaigns) {
                if (campaign != null) {
                    result.add(campaign)
                }
            }
            return result
        }

        private fun updateCampaignFieldsCommon(currentCampaign: Campaign, campaign: Campaign) {
            updateHtmlFieldQuirk(campaign)
            currentCampaign.name = campaign.name
            currentCampaign.campaignAlias = campaign.campaignAlias
            currentCampaign.nameHtml = campaign.nameHtml
            currentCampaign.shortDescription = campaign.shortDescription ?: ""
            currentCampaign.shortDescriptionHtml = campaign.shortDescriptionHtml
            currentCampaign.imageId = campaign.imageId
            currentCampaign.imageUrl = campaign.imageUrl
            currentCampaign.viewImageId = campaign.viewImageId
            currentCampaign.viewImageUrl = campaign.viewImageUrl
            currentCampaign.videoProfileId = campaign.videoProfileId
            currentCampaign.videoId = campaign.videoId
            currentCampaign.videoUrl = campaign.videoUrl
            currentCampaign.countryId = campaign.countryId
            currentCampaign.regionId = campaign.regionId
            currentCampaign.cityId = campaign.cityId
            currentCampaign.regionNameRus = campaign.regionNameRus
            currentCampaign.cityNameRus = campaign.cityNameRus
            currentCampaign.tags = campaign.tags
        }

        private fun plainTextToHtml(plainText: String?): String? {
            return ru.planeta.commons.lang.StringUtils.escapeHtml(
                    ru.planeta.commons.lang.StringUtils.stripLinksFromHtml(plainText)
            )
        }

        private fun updateHtmlFieldQuirk(campaign: Campaign) {
            campaign.nameHtml = plainTextToHtml(campaign.name)
            campaign.shortDescriptionHtml = plainTextToHtml(campaign.shortDescription)
        }

        private val PHOTO_ID_PATTERN = Pattern.compile("\\s+id=\"(\\d+)\"")
        private val PHOTO_OWNER_PATTERN = Pattern.compile("\\s+owner=\"(\\d+)\"")

        fun replacePhotoHtml(photoHtmlChunk: String, photoToReplace: Photo): String {
            val result = PHOTO_ID_PATTERN.matcher(photoHtmlChunk).replaceAll(" id=\"" + photoToReplace.photoId + "\"")
            return PHOTO_OWNER_PATTERN.matcher(result).replaceAll(" owner=\"" + photoToReplace.profileId + "\"")
        }

        /**
         * Create new default campaign from audion album
         *
         * @param profileId group=owner profile
         * @param albumId   audio album id
         * @return new Campaign
         */
        private fun createDefaultCampaign(creatorProfileId: Long, profileId: Long, albumId: Long): Campaign {
            val campaign = Campaign()
            campaign.profileId = profileId
            campaign.creatorProfileId = creatorProfileId
            campaign.name = ""
            campaign.description = ""
            campaign.descriptionHtml = ""
            campaign.shortDescription = ""
            campaign.albumId = albumId
            campaign.purchaseSum = BigDecimal.ZERO
            campaign.status = CampaignStatus.DRAFT
            campaign.targetStatus = CampaignTargetStatus.NONE

            return campaign
        }

        private fun getCampaignNews(prevStatus: CampaignStatus?, currStatus: CampaignStatus, campaignTargetStatus: CampaignTargetStatus): ProfileNews.Type? {
            var result: ProfileNews.Type? = null
            when (currStatus) {
                CampaignStatus.NOT_STARTED -> result = ProfileNews.Type.CAMPAIGN_ON_MODERATION
                CampaignStatus.PAUSED -> result = ProfileNews.Type.CAMPAIGN_PAUSED
                CampaignStatus.PATCH -> result = ProfileNews.Type.CAMPAIGN_ON_REWORK
                CampaignStatus.DECLINED -> result = ProfileNews.Type.CAMPAIGN_DECLINED
                CampaignStatus.DELETED -> result = ProfileNews.Type.CAMPAIGN_DELETED
                CampaignStatus.ACTIVE -> result = if (EnumSet.of<CampaignStatus>(CampaignStatus.PAUSED, CampaignStatus.FINISHED).contains(prevStatus))
                    ProfileNews.Type.CAMPAIGN_RESUMED
                else
                    CAMPAIGN_STARTED
                CampaignStatus.FINISHED -> result = if (campaignTargetStatus === CampaignTargetStatus.SUCCESS)
                    ProfileNews.Type.CAMPAIGN_FINISHED_SUCCESS
                else
                    ProfileNews.Type.CAMPAIGN_FINISHED_FAIL
                CampaignStatus.APPROVED -> result = ProfileNews.Type.CAMPAIGN_APPROVED
            }

            return result
        }
    }

    private fun updateCampaign(campaign: Campaign) {
        campaign.timeUpdated = Date()
        campaignDAO.update(campaign)
    }

    private fun insertCampaign(campaign: Campaign) {
        val now = Date()
        campaign.timeAdded = now
        campaign.timeUpdated = now
        if (campaign.timeLastPurchased == null) {
            campaign.timeLastPurchased = now
        }
        if (campaign.creatorProfileId == 0L) {
            campaign.creatorProfileId = campaign.profileId ?: throw RuntimeException("profileId for Campaign is NULL!!!!!!!!!!!!!")
        }
        campaignDAO.insert(campaign)
    }
}

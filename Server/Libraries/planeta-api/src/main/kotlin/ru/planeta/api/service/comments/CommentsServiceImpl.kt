package ru.planeta.api.service.comments

import org.apache.commons.lang3.StringUtils
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import ru.planeta.api.BaseService
import ru.planeta.api.exceptions.MessageCode
import ru.planeta.api.exceptions.NotFoundException
import ru.planeta.api.exceptions.PermissionException
import ru.planeta.api.service.notifications.NotificationService
import ru.planeta.api.text.FormattingService
import ru.planeta.commons.model.Gender
import ru.planeta.dao.profiledb.CommentDAO
import ru.planeta.dao.profiledb.CommentObjectDAO
import ru.planeta.model.enums.ObjectType
import ru.planeta.model.profile.Comment
import ru.planeta.model.profile.CommentObject
import ru.planeta.model.profile.Profile

import java.util.Date

/**
 * Base service for all services
 *
 * @author a.savanovich
 * Date: 27.10.11
 */
@Service
class CommentsServiceImpl(private val commentObjectDAO: CommentObjectDAO,
                          private val formattingService: FormattingService,
                          private val notificationService: NotificationService,
                          private val commentDAO: CommentDAO) : BaseService(), CommentsService {


    /**
     * Gets comments to user's object (child to the specified parentCommentId).
     * If parentCommentId is 0 that this is root comments.
     *
     * @param ownerId              Commentable object owner
     * @param objectId             Commentable object identifier
     * @param objectType           Object type
     * @param parentCommentId      Parent comment identifier
     * @param includeChildComments how many child comments will be included
     * @param offset               default 0
     * @param limit                of comments
     * @return list of comments
     */
    override fun getComments(clientId: Long, ownerId: Long, objectId: Long, objectType: ObjectType,
                             parentCommentId: Long, includeChildComments: Int, offset: Int, limit: Int, sort: String): List<Comment> {

        return if (includeChildComments > 0) {
            commentDAO!!.selectCommentsWithChilds(clientId, ownerId, objectId, objectType, parentCommentId, includeChildComments, offset, limit, sort)
        } else {
            commentDAO!!.selectComments(clientId, ownerId, objectId, objectType, parentCommentId, offset, limit, sort)
        }
    }

    /**
     * Gets (or creates if none) CommentObject for the specified comment
     */
    private fun getOrCreateCommentObject(comment: Comment): CommentObject {
        var commentableObject: CommentObject? = commentObjectDAO!!.select(comment)
        if (commentableObject == null) {
            commentableObject = CommentObject()
            commentableObject.profileId = comment.profileId
            commentableObject.objectType = comment.objectType
            commentableObject.objectId = comment.objectId
            commentObjectDAO.insert(commentableObject)
        }
        return commentableObject
    }

    override fun getCommentsWithChildAsPlain(clientId: Long, profileId: Long, objectId: Long, objectType: ObjectType, offset: Int, limit: Int): List<Comment> {
        return commentDAO.selectCommentsWithChildAsPlain(clientId, profileId, objectId, objectType, offset, limit)
    }


    /**
     * Adds new user's object comment
     *
     * @param comment to add
     * @return comment with id
     * @throws NotFoundException, PermissionException
     */
    @Throws(NotFoundException::class, PermissionException::class)
    override fun addComment(comment: Comment): Comment {
        val author = profileDAO.selectById(comment.authorProfileId)
                ?: throw NotFoundException("No user with id " + comment.authorProfileId)

        val commentableObject = getOrCreateCommentObject(comment)
        val profile = profileDAO.selectById(comment.profileId!!)
                ?: throw NotFoundException(Profile::class.java, comment.profileId)

        val level = calculateLevel(comment)

        commentObjectDAO!!.addCommentsCount(commentableObject, 1)

        comment.level = level
        comment.timeAdded = Date()
        comment.isDeleted = false
        comment.childsCount = 0

        comment.ownerProfileType = profile.profileType
        comment.ownerAlias = profile.alias
        comment.ownerImageUrl = profile.imageUrl
        comment.ownerName = profile.displayName

        comment.authorName = author.displayName
        comment.authorAlias = author.alias
        comment.authorGender = author.userGender ?: Gender.NOT_SET
        comment.authorImageUrl = author.imageUrl

        if (!formattingService!!.validateHtml(comment.textHtml)) {
            throw PermissionException(MessageCode.WRONG_CHARS)
        }

        var html = formattingService.formatPlainText(comment.text, comment.authorProfileId, false)
        if (!StringUtils.isBlank(comment.textHtml)) {
            html += comment.textHtml
        }
        comment.text = StringUtils.EMPTY
        comment.textHtml = html
        commentDAO!!.insert(comment)


        if (ObjectType.PRODUCT === comment.objectType) {
            notificationService!!.sendProductNewComment(comment)
        }

        return comment
    }

    @Throws(NotFoundException::class)
    private fun calculateLevel(comment: Comment): Int {
        var level = 0
        val parentComment: Comment?
        val parentCommentId = comment.parentCommentId
        if (parentCommentId != 0L) {
            parentComment = commentDAO!!.selectCommentById(comment.profileId!!, parentCommentId)
            if (parentComment == null || parentComment.isDeleted) {
                throw NotFoundException("No parent comment with id=" + parentCommentId + " objectId=" + comment.objectId + " profileId=" + comment.profileId)
            }
            level = parentComment.level + 1
        }
        return level
    }


    /**
     * Deletes specified user's comment (not deletes but marks this comment as deleted).
     *
     * @param profileId owner of comment
     * @param clientId  who deleteByProfileId
     * @param commentId comment id
     */
    @Throws(NotFoundException::class, PermissionException::class)
    override fun deleteComment(clientId: Long, profileId: Long, commentId: Long) {


        val comment = commentDAO!!.selectCommentById(profileId, commentId)

        // Comment does not exist -- returning
        if (comment == null || comment.isDeleted) {
            throw NotFoundException(Comment::class.java, commentId)
        }

        //Checking permissions
        if (!permissionService.isObjectOwnerOrAuthor(clientId, comment)) {
            throw PermissionException(MessageCode.PERMISSION_COMMENT_DELETE)
        }

        // Getting commentable object
        val commentableObject = commentObjectDAO!!.select(comment) ?: throw NotFoundException()

        commentObjectDAO.addCommentsCount(commentableObject, -1)
        commentDAO.delete(profileId, commentId)

    }

    /**
     * Restore deleteByProfileId comment
     *
     * @throws NotFoundException, PermissionException
     */
    @Throws(NotFoundException::class, PermissionException::class)
    override fun restoreComment(clientId: Long, profileId: Long, commentId: Long) {
        val comment = commentDAO!!.selectCommentById(profileId, commentId)
        if (comment == null || !comment.isDeleted) {
            throw NotFoundException(Comment::class.java, commentId)
        }
        //Checking permissions
        if (!permissionService.isObjectOwnerOrAuthor(clientId, comment)) {
            throw PermissionException(MessageCode.PERMISSION_COMMENT_DELETE)
        }

        val commentObject = commentObjectDAO!!.select(comment)
                ?: throw NotFoundException(CommentObject::class.java, comment.objectId)

        commentObjectDAO.addCommentsCount(commentObject, 1)
        commentDAO.restore(profileId, commentId)
    }


    /**
     * Select comments after startCommentId
     */
    override fun getLastComments(ownerId: Long, objectId: Long, objectType: ObjectType, startCommentId: Long, sort: String,
                                 offset: Int, limit: Int): List<Comment> {
        return commentDAO!!.selectLastComments(ownerId, objectId, objectType, startCommentId, sort, offset, limit)
    }

    override fun getCommentsCount(objectId: Long, objectType: ObjectType): Int {
        return commentDAO!!.getCommentsCount(objectId, objectType)
    }
}

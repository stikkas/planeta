package ru.planeta.moscowshow.model.result

import javax.xml.bind.annotation.XmlAccessType
import javax.xml.bind.annotation.XmlAccessorType
import javax.xml.bind.annotation.XmlElementWrapper
import javax.xml.bind.annotation.XmlRootElement
import ru.planeta.moscowshow.model.Place
import ru.planeta.moscowshow.model.StructElement

/**
 *
 * User: Serge Blagodatskih<stikkas17></stikkas17>@gmail.com><br></br>
 * Date: 14.10.16<br></br>
 * Time: 14:57
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
class ResultActionSchema : Result() {

    var Data: Data? = null

    val places: List<Place>?
        get() = Data?.Place

    val hallName: String?
        get() = Data?.hallName

    val hallAddress: String?
        get() = Data?.hallAddress

    val imageUrl: String?
        get() = Data?.imageURL

    val sectionsAndRows: List<StructElement>?
        get() = Data?.StructElement

}

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
class Data {

    @XmlElementWrapper(name = "places")
    var Place: List<Place>? = null

    @XmlElementWrapper(name = "sections_and_rows")
    var StructElement: List<StructElement>? = null

    var imageURL: String? = null
    var hallName: String? = null
    var hallAddress: String? = null

}

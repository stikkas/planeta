package ru.planeta.api.service.content

import ru.planeta.api.exceptions.NotFoundException
import ru.planeta.api.exceptions.PermissionException
import ru.planeta.api.model.json.BroadcastInfo
import ru.planeta.model.profile.broadcast.enums.BroadcastCategory
import ru.planeta.model.profile.broadcast.*
import java.io.File
import java.io.IOException
import java.util.Date

/**
 * Broadcasts service
 *
 * @author ds.kolyshev
 * Date: 19.03.12
 */
interface BroadcastService {

    fun getBroadcast(client: Long, broadcastId: Long): Broadcast?

    @Throws(NotFoundException::class)
    fun getBroadcastSafe(client: Long, broadcastId: Long): Broadcast

    fun selectByIdList(idList: List<Long>): List<Broadcast>

    @Throws(PermissionException::class, NotFoundException::class)
    fun saveBroadcast(clientId: Long, broadcast: Broadcast): Broadcast

    @Throws(PermissionException::class)
    fun deleteBroadcast(clientId: Long, broadcastId: Long)

    fun getBroadcastStreams(clientId: Long, broadcastId: Long, offset: Int, limit: Int): List<BroadcastStream>

    fun getBroadcastStream(clientId: Long, streamId: Long): BroadcastStream?

    @Throws(PermissionException::class, NotFoundException::class, IOException::class)
    fun saveBroadcastStream(clientId: Long, broadcastStream: BroadcastStream): BroadcastStream

    @Throws(PermissionException::class)
    fun deleteBroadcastStream(clientId: Long, streamId: Long)

    /**
     * Pauses specified stream
     *
     */
    @Throws(PermissionException::class, NotFoundException::class)
    fun pauseBroadcastStream(clientId: Long, profileId: Long, streamId: Long): BroadcastStream

    /**
     * Stops all ended broadcasts in LIVE status
     *
     */
    @Throws(IOException::class)
    fun stopEndedBroadcasts(time: Long)

    /**
     * Plays all started broadcasts in NOT_STARTED status
     *
     */
    fun liveStartedBroadcasts(time: Long)

    /**
     * Plays specified broadcast
     *
     */
    @Throws(PermissionException::class, NotFoundException::class)
    fun playBroadcast(clientId: Long, broadcastId: Long): Broadcast

    /**
     * Stops specified broadcast
     *
     */
    @Throws(PermissionException::class, NotFoundException::class)
    fun stopBroadcast(clientId: Long, broadcastId: Long): Broadcast

    /**
     * Make broadcast available only for some users
     *
     * @param clientId    client id
     * @param broadcastId broadcast id
     * @return updated broadcast
     * @throws PermissionException if client doesn't have permission to modify broadcast
     * @throws NotFoundException   if broadcast not found
     */
    @Throws(PermissionException::class, NotFoundException::class)
    fun makeBroadcastPrivate(clientId: Long, broadcastId: Long): Broadcast

    /**
     * Pauses specified broadcast
     *
     */
    @Throws(PermissionException::class, NotFoundException::class)
    fun pauseBroadcast(clientId: Long, broadcastId: Long): Broadcast

    /**
     * Sets default streamId for broadcast
     *
     */
    @Throws(PermissionException::class, NotFoundException::class)
    fun setDefaultStreamId(clientId: Long, broadcastId: Long, streamId: Long)

    /**
     * Schedules video file uploading for specified broadcast stream
     * Public for test
     *
     */
    fun scheduleSaveDumpVideo(stream: BroadcastStream, videoFile: File)

    /**
     * Checks if broadcasts is allowed for specified geo location
     * checking by ip and profile's city
     *
     */
    fun checkBroadcastGeoTargeting(clientId: Long, profileId: Long, broadcastId: Long, ip: String): Boolean?

    /**
     * Gets list of broadcast's geo targeting objects
     *
     */
    fun getBroadcastGeoTargeting(clientId: Long, broadcastId: Long): List<BroadcastGeoTargeting>

    /**
     * Inserts new geo targeting restriction for broadcast
     *
     */
    @Throws(PermissionException::class)
    fun addBroadcastGeoTargeting(clientId: Long, broadcastGeoTargeting: BroadcastGeoTargeting)

    /**
     * Removes all geo restrictions for broadcast
     *
     */
    @Throws(PermissionException::class)
    fun removeBroadcastGeoTargeting(clientId: Long, broadcastId: Long)

    /**
     * Removes geo restriction by parameters
     *
     * @throws PermissionException
     */
    @Throws(PermissionException::class)
    fun removeBroadcastGeoTargetingByParams(clientId: Long, broadcastId: Long, countryId: Int, cityId: Int, allowed: Boolean)

    /**
     * Returns [BroadcastInfo] object, which contains only public info, for profile with specified `profileId`
     *
     * @return broadcast info or null if no broadcast exists for specified profile id
     */
    fun getPublicBroadcastInfo(broadcastId: Long): BroadcastInfo

    /**
     * Checks if broadcasts is allowed for specified backer id
     *
     * @param clientId    backer id
     */
    @Throws(NotFoundException::class, PermissionException::class)
    fun checkBroadcastBackersTargeting(clientId: Long, broadcastId: Long): Boolean?

    /**
     * Gets list of broadcast's backers targeting objects
     *
     */
    fun getBroadcastBackersTargeting(clientId: Long, profileId: Long, broadcastId: Long): List<BroadcastBackersTargeting>

    /**
     * Inserts new backers targeting restriction for broadcast
     *
     */
    @Throws(PermissionException::class)
    fun addBroadcastBackersTargeting(clientId: Long, broadcastBackersTargeting: BroadcastBackersTargeting)

    /**
     * Removes specified backers restrictions for broadcast
     *
     */
    @Throws(PermissionException::class)
    fun removeBroadcastBackersTargeting(clientId: Long, broadcastId: Long, campaignId: Long)

    /**
     * Returns list of only dashboard visible upcoming broadcasts starting from `dateFrom`
     *
     * @param clientId client id
     * @param dateTo   finish date
     * @param offset   offset
     * @param limit    limit
     * @return list of upcoming broadcasts
     */
    fun getUpcomingBroadcasts(clientId: Long, dateTo: Date, offset: Int, limit: Int): List<Broadcast>

    /**
     * Returns list of upcoming broadcasts starting from `dateFrom`
     *
     * @param clientId client id
     * @param dateTo   finish date
     * @param onlyDashboard only dashboard visible
     * @param offset   offset
     * @param limit    limit
     * @return list of upcoming broadcasts
     */
    fun getUpcomingBroadcasts(clientId: Long, dateTo: Date, onlyDashboard: Boolean, offset: Int, limit: Int): List<Broadcast>

    /**
     * Returns list of archived broadcasts up to the `dateTo`
     *
     *
     *
     *
     * @param clientId client id
     * @param dateTo   end date
     * @param offset   offset
     * @param limit    limit
     * @return list of archived broadcasts
     */
    fun getArchivedBroadcasts(broadcastCategory: BroadcastCategory, clientId: Long, dateTo: Date, isPopularSort: Boolean, offset: Int, limit: Int): List<Broadcast>

    /**
     * Generates temporary links for closed broadcast and sends them to the provided list of emails
     * Links would expire after `validInHours` hours after first visit to that link
     *
     * @param clientId     client id
     * @param profileId    profile id
     * @param broadcastId  broadcast id
     * @param emails       list of emails
     * @param validInHours validity in hours
     * @throws PermissionException
     */
    @Throws(PermissionException::class, NotFoundException::class)
    fun generatePrivateBroadcastLinks(clientId: Long, profileId: Long, broadcastId: Long, emails: List<String>, validInHours: Int)

    /**
     * Returns list of [ru.planeta.model.profile.broadcast.BroadcastPrivateTargeting]s
     *
     * @param clientId    client id
     * @param broadcastId broadcast id
     * @return list of [ru.planeta.model.profile.broadcast.BroadcastPrivateTargeting]s
     * @throws PermissionException
     */
    @Throws(PermissionException::class)
    fun getBroadcastPrivateTargetings(clientId: Long, broadcastId: Long): List<BroadcastPrivateTargeting>

    /**
     * Checks if client has access to the private broadcast with specified link
     * If link is visited for the first time, this method sets it's first visit time
     *
     * @param clientId      client id
     * @param broadcastId   broadcast id
     * @param generatedLink generated link  @return  true or false, depending on whether client has access to broadcast
     */
    @Throws(PermissionException::class, NotFoundException::class)
    fun validateBroadcastLink(clientId: Long, broadcastId: Long, generatedLink: String): Boolean

    @Throws(PermissionException::class, NotFoundException::class)
    fun getBroadcastPrivateTargeting(clientId: Long, broadcastId: Long, generatedLink: String): BroadcastPrivateTargeting

    /**
     * Constructs link for private broadcast
     *
     * @param broadcastId         profile id
     * @param generatedLink     generated link
     * @return constructed link
     */
    fun constructPrivateBroadcastLink(broadcastId: Long, generatedLink: String): String

    fun selectForAdmin(searchString: String, dateFrom: Date, dateTo: Date, offset: Long, limit: Int): List<Broadcast>

    fun selectForAdminCount(searchString: String, dateFrom: Date, dateTo: Date): Long

    fun updateBroadcastViewsCount(defaultStreamId: Long)

    @Throws(NotFoundException::class, PermissionException::class, IOException::class)
    fun toggleBroadcastStreamState(clientId: Long, broadcastId: Long, streamId: Long)

    @Throws(NotFoundException::class, PermissionException::class)
    fun checkBroadcastProductTargeting(clientId: Long, broadcastId: Long): Boolean?

    fun getBroadcastProductTargeting(broadcastId: Long): List<BroadcastProductTargeting>

    @Throws(PermissionException::class)
    fun addBroadcastProductTargeting(clientId: Long, broadcastProductTargeting: BroadcastProductTargeting)

    @Throws(PermissionException::class)
    fun removeBroadcastProductTargeting(clientId: Long, broadcastProductTargeting: BroadcastProductTargeting)

    companion object {

        val PLANETA_PROFILE_ID: Long = 21765
    }
}

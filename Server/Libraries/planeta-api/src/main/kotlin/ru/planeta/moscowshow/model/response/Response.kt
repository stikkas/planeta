package ru.planeta.moscowshow.model.response

import ru.planeta.moscowshow.model.result.Result


/**
 * Базовый класс для ответов
 * User: Serge Blagodatskih<stikkas17></stikkas17>@gmail.com><br></br>
 * Date: 14.10.16<br></br>
 * Time: 13:09
 */
abstract class Response<T : Result> {

    abstract var result: T?

    val errorMessage: String?
        get() = result?.ErrorMessage

    // На разные ошибки код может быть один и тот же, замечено для кода 3
    // Поэтому смотрим по тексту ошибки
    // Похоже что код ошибки относится к Severity - для Warning всегда 3
    val isSessionClosed: Boolean
        get() = "Session closed" == result?.ErrorMessage || "Session with GUID not found" == result?.ErrorMessage

    fun hasErrors(): Boolean {
        return result?.ErrorCode != 0
    }
}

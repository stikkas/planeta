package ru.planeta.api.service.billing.order

import ru.planeta.api.aspect.transaction.NonTransactional
import ru.planeta.api.exceptions.NotFoundException
import ru.planeta.api.exceptions.PermissionException

/**
 * Created by asavan on 13.04.2017.
 */
interface BatchCancelerService {
    @NonTransactional
    fun batchCancelOrders(clientId: Long, orders: List<Long>, reason: String): Int


    @NonTransactional
    fun batchCancelCampaignOrders(clientId: Long, campaignId: Long, reason: String): Int
}

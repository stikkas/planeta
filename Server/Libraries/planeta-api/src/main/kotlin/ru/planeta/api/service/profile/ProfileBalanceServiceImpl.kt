package ru.planeta.api.service.profile

import org.apache.log4j.Logger
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import ru.planeta.api.exceptions.MessageCode
import ru.planeta.api.exceptions.PermissionException
import ru.planeta.dao.commondb.CreditTransactionDAO
import ru.planeta.dao.commondb.DebitTransactionDAO
import ru.planeta.dao.commondb.ProfileBalanceDAO
import ru.planeta.dao.payment.PaymentProviderDAO
import ru.planeta.model.common.CreditTransaction
import ru.planeta.model.common.DebitTransaction
import ru.planeta.model.common.PaymentProvider

import java.math.BigDecimal
import java.util.Date

/**
 * Standard implementation of ProfileBalanceService.
 * Increase/decrease profile balance and create debit/credit transaction for this operation
 *
 *
 * User: atropnikov
 * Date: 02.04.12
 * Time: 18:45
 */
@Service
class ProfileBalanceServiceImpl(
        private val providerDAO: PaymentProviderDAO,
        private val profileBalanceDAO: ProfileBalanceDAO,
        private val debitTransactionDAO: DebitTransactionDAO,
        private val creditTransactionDAO: CreditTransactionDAO
) : ProfileBalanceService {

    override fun increaseProfileBalance(profileId: Long, amountNet: BigDecimal, amountFee: BigDecimal,
                                        comment: String, paymentProviderId: Long?, externalSystemData: String): DebitTransaction {
        val result = DebitTransaction()
        result.profileId = profileId
        result.paymentProviderId = paymentProviderId
        result.externalSystemData = externalSystemData
        result.amountNet = amountNet
        result.amountFee = amountFee
        result.comment = comment
        result.timeAdded = Date()
        debitTransactionDAO.insert(result)

        //increase balance
        if (amountNet.compareTo(BigDecimal.ZERO) > 0 || amountFee.compareTo(BigDecimal.ZERO) > 0) {
            val currentBalance = getBalanceForUpdate(profileId)
            val resultBalance = currentBalance.add(amountNet).subtract(amountFee)
            log.info("increasing balance of profile #$profileId current balance: $currentBalance result balance: $resultBalance")
            profileBalanceDAO.update(profileId, resultBalance)
        }
        return result
    }

    override fun increaseProfileBalance(profileId: Long, amountNet: BigDecimal, amountFee: BigDecimal, comment: String): DebitTransaction {
        val provider = providerDAO.select(PaymentProvider.Type.PLANETA)
        return increaseProfileBalance(profileId, amountNet, amountFee, comment, provider.id, "")
    }

    @Throws(PermissionException::class)
    override fun decreaseProfileBalance(profileId: Long, amountNet: BigDecimal, amountFee: BigDecimal, comment: String): CreditTransaction {
        val balance = getBalanceForUpdate(profileId)
        val resultBalance = balance.subtract(amountNet.subtract(amountFee))
        if (resultBalance.compareTo(BigDecimal.ZERO) < 0) {
            throw PermissionException(MessageCode.NOT_ENOUGH_BALANCE_AMOUNT)
        }

        log.info("decreasing balance of profile #$profileId current balance: $balance result balance: $resultBalance")
        //create credit transaction
        val transaction = CreditTransaction()
        transaction.profileId = profileId
        transaction.amountNet = amountNet
        transaction.amountFee = amountFee
        transaction.comment = comment
        transaction.timeAdded = Date()
        creditTransactionDAO.insert(transaction)

        //decrease profile balance
        profileBalanceDAO.update(profileId, resultBalance)
        return transaction
    }

    override fun getBalance(profileId: Long): BigDecimal {
        var balance: BigDecimal? = profileBalanceDAO.select(profileId)
        if (balance == null) {
            profileBalanceDAO.insert(profileId)
            balance = profileBalanceDAO.select(profileId)
        }

        return balance
    }


    override fun getBalanceForUpdate(profileId: Long): BigDecimal {
        var balance: BigDecimal? = profileBalanceDAO.selectForUpdate(profileId)
        if (balance == null) {
            profileBalanceDAO.insert(profileId)
            balance = profileBalanceDAO.selectForUpdate(profileId)
        }

        return balance
    }

    override fun isEnoughMoney(profileId: Long, amount: BigDecimal): Boolean {
        return getBalance(profileId).compareTo(amount) >= 0
    }

    companion object {

        private val log = Logger.getLogger(ProfileBalanceServiceImpl::class.java)
    }
}


package ru.planeta.api.service.notifications

import ru.planeta.api.exceptions.NotFoundException
import ru.planeta.api.exceptions.PermissionException
import ru.planeta.api.model.ActionStatus
import ru.planeta.model.bibliodb.Book
import ru.planeta.model.bibliodb.BookOrder
import ru.planeta.model.bibliodb.Library
import ru.planeta.model.common.*
import ru.planeta.model.common.campaign.Campaign
import ru.planeta.model.common.campaign.CampaignSubscribe
import ru.planeta.model.common.charity.CharityEventOrder
import ru.planeta.model.common.school.Seminar
import ru.planeta.model.common.school.SeminarRegistrationCompany
import ru.planeta.model.common.school.SeminarRegistrationSoloSimple
import ru.planeta.model.profile.Comment
import ru.planeta.model.profile.Profile
import ru.planeta.model.promo.TechnobattleRegistration
import ru.planeta.model.trashcan.SchoolOnlineCourseApplication
import java.io.UnsupportedEncodingException
import java.util.Date

/**
 * New interface for notification service
 * Each notification event can be interesting to some number of people(profiles) - they can become recipients of these event
 * notifications.
 * All users are basically subscribed to some useful event notifications due to it's relations.
 */
interface NotificationService {
    /*
    for tests only
     */
    fun setAsynchronous(asynchronous: Boolean)

    fun sendCampaignCanMakeVipOfferNotification(authorsEmails: Collection<String>)

    fun sendPurchaseShareNotificationEmail(orderInfo: OrderInfo, rewardInstruction: String)

    fun sendOrderSentNotificationEmail(orderInfo: OrderInfo): Boolean

    fun sendMailFromAboutUsAdvertising(mailFrom: String, message: String)

    fun sendPurchaseBiblioNotificationEmail(orderInfo: OrderInfo)

    @Throws(NotFoundException::class)
    fun sendCampaignNewsAddedNotificationSecure(clientId: Long, campaignId: Long, profileId: Long, postId: Long): ActionStatus<*>

    fun sendPurchaseProductNotificationEmail(orderInfo: OrderInfo, groupedOrderObjects: Map<Long, List<OrderObjectInfo>>)

    @Throws(NotFoundException::class)
    fun sendUserBecameVipNotification(profile: Profile)

    fun sendOrderGivenNotificationEmail(orderInfo: OrderInfo): Boolean

    @Throws(NotFoundException::class, PermissionException::class)
    fun sendOrderCancelNotification(order: Order, reason: String)

    fun sendQuickstartGetBook(email: String, type: String)

    fun sendSchoolWebinarRegistration(email: String, seminar: Seminar)

    fun sendSchoolSolosimpleRegistration(seminarRegistration: SeminarRegistrationSoloSimple, seminar: Seminar)

    fun sendSchoolSoloRegistration(email: String, seminar: Seminar)

    fun sendCharityEventOrder(email: String, charityEventOrder: CharityEventOrder)

    fun sendSchoolPairRegistration(email: String, email2: String, seminar: Seminar)

    fun sendSchoolCompanyRegistration(seminarRegistration: SeminarRegistrationCompany, seminar: Seminar)

    fun sendSchoolOnlineCourse(application: SchoolOnlineCourseApplication)

    @Throws(NotFoundException::class, PermissionException::class)
    fun sendInvestingInvoiceCreatedNotification(investingOrderInfo: InvestingOrderInfo)

    @Throws(NotFoundException::class, PermissionException::class)
    fun sendInvestingInvoiceRequestNotification(investingOrderInfo: InvestingOrderInfo)

    fun sendPaymentInvalidStateNotification(processorName: String, transaction: TopayTransaction, error: Boolean)

    @Throws(NotFoundException::class)
    fun sendProductNewComment(comment: Comment)

    fun sendBiblioPurchaseNotification(libraries: List<Library>, bookOrders: Collection<BookOrder>, fio: String, buyerEmail: String)

    fun sendBiblioBookRequest(request: Book)

    fun sendTechnobattleProgrammLetter(email: String)

    fun sendTechnobattleManagerLetterAboutPromoCodesIsGoingToEnd(freeLitresPromoCodesCount: Int)

    fun sendPromoCodeLitresLetter(email: String, promoCode: String)

    fun sendTechnobattleRegistration(request: TechnobattleRegistration)

    fun sendBiblioLibraryActiveNotification(library: Library): Boolean

    fun sendBiblioPurchaseBookNotification(bookOrder: BookOrder, libraries: List<Library>, buyerName: String, date: Date, buyerEmail: String, emailTo: String?): Boolean

    @Throws(UnsupportedEncodingException::class)
    fun sendInteractiveCampaignFinished(userId: Long?, emailTo: String, userName: String)

    @Throws(NotFoundException::class, PermissionException::class)
    fun sendAuthorCampaignNotifications(profileId: Long, noReplyEmail: String, maxCampaignCount: Int, emailTo: String, date: Date)

    @Throws(NotFoundException::class, PermissionException::class)
    fun sendCampaignCampaignEndsSoonNotification(campaignSubscribe: CampaignSubscribe)

    @Throws(NotFoundException::class)
    fun campaignCreated(clientId: Long, campaign: Campaign)

    @Throws(NotFoundException::class)
    fun sendEmailCampaignInStatusPatchToAdmins(campaign: Campaign)

    fun sendPromoEmail(email: String, template: String)

    fun sendPromoEmail(email: String, template: String, params: Map<String, String>)

    @Throws(Exception::class)
    fun sendOrderCancelRequestEmails(orderId: Long, clientId: Long)
}

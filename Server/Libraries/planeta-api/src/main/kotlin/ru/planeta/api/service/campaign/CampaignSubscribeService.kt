package ru.planeta.api.service.campaign

import ru.planeta.model.common.campaign.CampaignSubscribe

/**
 * Created with IntelliJ IDEA.
 * User: michail
 * Date: 16.11.2017
 * Time: 16:11
 */
interface CampaignSubscribeService {
    fun getCampaignSubsctiptionsToSend(offset: Long, limit: Int): List<CampaignSubscribe>

    fun isSubscribedOnCampaignEnd(profileId: Long, campaignId: Long): Boolean?

    fun subscribeOnCampaignEnd(profileId: Long, campaignId: Long)

    fun unsubscribeOnCampaignEnd(profileId: Long, campaignId: Long)
}

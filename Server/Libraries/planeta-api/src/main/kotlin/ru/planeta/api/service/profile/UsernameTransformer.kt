package ru.planeta.api.service.profile

/**
 * Defines username rules and generates canonical form of username
 */
interface UsernameTransformer {
    /**
     * Transforms specified username to canonical form
     * @param username username from input
     * @return canonical username
     * @throws IllegalArgumentException if username is not valid (null or empty)
     */
    @Throws(IllegalArgumentException::class)
    fun normalize(username: String): String

    /**
     * Checks if canonical forms of two usernames are the same
     * @param username
     * @param otherUsername
     * @return
     */
    fun usernameEquals(username: String, otherUsername: String): Boolean
}

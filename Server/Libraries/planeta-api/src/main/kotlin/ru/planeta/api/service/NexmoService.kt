package ru.planeta.api.service

/**
 *
 * Created by asavan on 02.09.2016.
 */
interface NexmoService {
    fun send(message: String, phone: String?)
}

package ru.planeta.api.service.campaign

import ru.planeta.api.exceptions.NotFoundException
import ru.planeta.api.exceptions.PermissionException
import ru.planeta.model.commondb.CampaignFaq

interface CampaignFaqService {
    fun insertCampaignFaq(campaignFaq: CampaignFaq)

    fun updateCampaignFaq(campaignFaq: CampaignFaq)

    fun deleteCampaignFaq(campaignFaqId: Long?)

    fun selectCampaignFaq(campaignFaqId: Long?): CampaignFaq

    fun selectCampaignFaqListByCampaign(campaignId: Long?, offset: Int, limit: Int): List<CampaignFaq>

    fun selectCampaignFaqListByCampaign(campaignId: Long?): List<CampaignFaq>

    fun selectCampaignFaqCountByCampaign(campaignId: Long?): Int

    @Throws(NotFoundException::class, PermissionException::class)
    fun updateOrCreateCampaignFaq(clientId: Long, campaignFaq: CampaignFaq): CampaignFaq

    @Throws(NotFoundException::class, PermissionException::class)
    fun deleteCampaignFaq(clientId: Long, campaignId: Long, campaignFaqId: Long)

    @Throws(NotFoundException::class, PermissionException::class)
    fun resort(clientId: Long, idFrom: Long, idTo: Long, campaignId: Long)
}

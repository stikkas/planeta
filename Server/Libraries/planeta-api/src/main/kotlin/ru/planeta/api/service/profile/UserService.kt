package ru.planeta.api.service.profile

import ru.planeta.api.exceptions.PermissionException
import ru.planeta.model.common.ProfileBalance

import java.math.BigDecimal

/**
 * @author a.savanovich
 * Date: 28.10.11
 */
interface UserService {

    val usersCount: Long

    /**
     * deprecated. Use ProfileBalanceService
     * Returns profile balance
     *
     * @param profileId profile identifier
     * @return balance in RUR
     */
    @Deprecated("")
    fun getUserProfileBalance(profileId: Long): BigDecimal

    @Throws(PermissionException::class)
    fun getPersonalCabinet(profileId: Long): ProfileBalance
}

package ru.planeta.moscowshow.model.response

import ru.planeta.moscowshow.model.result.Result
import javax.xml.bind.annotation.XmlAccessType
import javax.xml.bind.annotation.XmlAccessorType
import javax.xml.bind.annotation.XmlRootElement
import ru.planeta.moscowshow.model.result.ResultActionList

/**
 *
 * User: Serge Blagodatskih<stikkas17></stikkas17>@gmail.com><br></br>
 * Date: 13.10.16<br></br>
 * Time: 10:12
 */
@XmlRootElement(name = "GetActionListExResponse")
@XmlAccessorType(XmlAccessType.FIELD)
class ResponseActionList : Response<ResultActionList>() {

    override var result: ResultActionList? = null


}

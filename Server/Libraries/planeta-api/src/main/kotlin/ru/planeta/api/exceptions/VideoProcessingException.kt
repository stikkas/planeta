package ru.planeta.api.exceptions

import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ResponseStatus


@ResponseStatus(value = HttpStatus.OK, reason = "Video is in process")
class VideoProcessingException : PermissionException()

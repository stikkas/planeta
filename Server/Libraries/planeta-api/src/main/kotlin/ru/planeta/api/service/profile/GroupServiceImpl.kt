package ru.planeta.api.service.profile

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import ru.planeta.dao.profiledb.ProfileSubscriptionDAO
import ru.planeta.model.enums.ProfileType
import ru.planeta.model.profile.Profile

/**
 * @author ds.kolyshev
 * Date: 31.10.11
 */
@Service
class GroupServiceImpl @Autowired
constructor(private val profileSubscriptionDAO: ProfileSubscriptionDAO) : GroupService {

    override fun getGroupsByUser(clientId: Long): List<Profile> {
        return profileSubscriptionDAO.getProfileList(clientId, "", true, ProfileType.GROUP, null!!, 0, 100)
    }
}

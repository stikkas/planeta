package ru.planeta.api.service.content

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import ru.planeta.api.exceptions.NotFoundException
import ru.planeta.api.exceptions.PermissionException
import ru.planeta.api.text.Message
import ru.planeta.api.text.MessageParserService
import ru.planeta.model.Constants

/**
 * Extracted from FormatterService to avoid cyclic dependencies
 */
@Service
class SmartFormatterServiceImpl : SmartFormatterService {
    @Autowired
    private val photoService: PhotoService? = null
    @Autowired
    private val videoService: VideoService? = null
    @Autowired
    private val messageParserService: MessageParserService? = null


    @Throws(NotFoundException::class, PermissionException::class)
    override fun extractAttachments(authorProfileId: Long, ownerProfileId: Long, messageText: String, vararg ignoreHosts: String): Message {
        val parsedMessage = messageParserService!!.extractAttachments(messageText, *ignoreHosts)
        photoService!!.addImageAttachmentsToAlbum(authorProfileId, ownerProfileId, 0, Constants.ALBUM_USER_HIDDEN, parsedMessage.imageAttachments)
        videoService!!.addExternalVideoAttachments(authorProfileId, ownerProfileId, parsedMessage.videoAttachments)
        return parsedMessage
    }

}

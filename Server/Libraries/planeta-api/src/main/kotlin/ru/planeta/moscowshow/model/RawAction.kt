package ru.planeta.moscowshow.model

import java.util.*
import javax.xml.bind.annotation.XmlAccessType
import javax.xml.bind.annotation.XmlAccessorType
import javax.xml.bind.annotation.XmlRootElement

/**
 * Не нужен
 * User: Serge Blagodatskih<stikkas17></stikkas17>@gmail.com><br></br>
 * Date: 13.10.16<br></br>
 * Time: 16:44
 */
@XmlRootElement(name = "RawAction")
@XmlAccessorType(XmlAccessType.FIELD)
class RawAction {

    val id: Long = 0
    val name: String? = null
    val description: String? = null
    private val createDate: Date? = null
    val actionDate: Date? = null
    val location: Long = 0
    val schemaID: Long = 0
    private val publishFlag: Boolean = false
    private val isPartner: Boolean = false
    val imageUrl: String? = null
    private val lang: String? = null
    private val autoSales: Boolean = false

    override fun toString(): String {
        return ("RawAction{" + "id=" + id + ", name=" + name + ", description="
                + description + ", createDate=" + createDate + ", actionDate="
                + actionDate + ", location=" + location + ", schemaID=" + schemaID
                + ", publishFlag=" + publishFlag + ", isPartner=" + isPartner + ", imageUrl="
                + imageUrl + ", lang=" + lang + ", autoSales=" + autoSales + '}'.toString())
    }

}

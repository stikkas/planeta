package ru.planeta.moscowshow.client

import javax.annotation.PostConstruct
import javax.annotation.PreDestroy
import org.apache.log4j.Logger
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Lazy
import org.springframework.oxm.jaxb.Jaxb2Marshaller
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Component
import org.springframework.ws.client.core.WebServiceTemplate
import org.springframework.ws.soap.client.SoapFaultClientException
import org.springframework.ws.soap.client.core.SoapActionCallback
import ru.planeta.moscowshow.model.OrderItem
import ru.planeta.moscowshow.model.request.Request
import ru.planeta.moscowshow.model.request.RequestActionList
import ru.planeta.moscowshow.model.request.RequestActionSchema
import ru.planeta.moscowshow.model.request.RequestCancelOrder
import ru.planeta.moscowshow.model.request.RequestCancelOrderByNumbers
import ru.planeta.moscowshow.model.response.ResponseActionList
import ru.planeta.moscowshow.model.request.RequestLoginUser
import ru.planeta.moscowshow.model.request.RequestLogout
import ru.planeta.moscowshow.model.request.RequestPayTicket
import ru.planeta.moscowshow.model.request.RequestPayTickets
import ru.planeta.moscowshow.model.response.ResponseLoginUser
import ru.planeta.moscowshow.model.request.RequestPlaceOrder
import ru.planeta.moscowshow.model.request.RequestSoldTickets
import ru.planeta.moscowshow.model.request.RequestUnpayTicket
import ru.planeta.moscowshow.model.request.RequestUnpayTickets
import ru.planeta.moscowshow.model.response.Response
import ru.planeta.moscowshow.model.response.ResponseActionSchema
import ru.planeta.moscowshow.model.response.ResponseCancelOrder
import ru.planeta.moscowshow.model.response.ResponseCancelOrderByNumbers
import ru.planeta.moscowshow.model.response.ResponseLogout
import ru.planeta.moscowshow.model.response.ResponsePayTicket
import ru.planeta.moscowshow.model.response.ResponsePayTickets
import ru.planeta.moscowshow.model.response.ResponsePlaceOrder
import ru.planeta.moscowshow.model.response.ResponseSoldTickets
import ru.planeta.moscowshow.model.response.ResponseUnpayTicket
import ru.planeta.moscowshow.model.response.ResponseUnpayTickets

/**
 * SOAP клиент для работы с MoscowShow билетным сервисом
 * User: Serge Blagodatskih<stikkas17></stikkas17>@gmail.com><br></br>
 * Date: 12.10.16<br></br>
 * Time: 16:47
 */
@Component
@Lazy
class MoscowShowClient {

    @Volatile
    private var sessionId: String? = null
    private val lock = Any()

    @Value("\${moscowshow.client.url:}")
    lateinit var defaultUrl: String

    private lateinit var webServiceTemplate: WebServiceTemplate

    @Autowired
    lateinit var loginUser: RequestLoginUser

    private val LOG = Logger.getLogger(MoscowShowClient::class.java)
    /**
     * Получить список концертов
     * @return объект с информацие об ошибке и списком концертов
     */
    fun listActions(): ResponseActionList {
        return exec(RequestActionList(), SoapAction.ACTION_LIST) as ResponseActionList
    }

    /**
     * Получить структуру зала для концерта (выборка настраивается)
     * @param actionID идентификатор концерта
     * @param sectionID идентификатор секции
     * @param fullSchema всю или нет схему
     * @return объект с информацие об ошибке и схемой зала
     */
    @JvmOverloads
    fun actionSchema(actionID: Long, sectionID: Long = 0, fullSchema: Boolean = true): ResponseActionSchema {
        return exec(RequestActionSchema(actionID, sectionID, fullSchema), SoapAction.ACTION_SCHEMA) as ResponseActionSchema
    }

    /**
     * Забронировать места на концерт
     * @param items список мест для бронирования
     * @return объект с информацие об ошибке и списком забронированных мест
     */
    fun placeOrder(items: List<OrderItem>): ResponsePlaceOrder {
        return exec(RequestPlaceOrder(items), SoapAction.PLACE_ORDER) as ResponsePlaceOrder
    }

    /**
     * Отменить бронь
     * @param items список мест для отмены
     * @return объект с информацие об ошибке
     */
    fun cancelOrder(items: List<OrderItem>): ResponseCancelOrder {
        return exec(RequestCancelOrder(items), SoapAction.CANCEL_ORDER) as ResponseCancelOrder
    }

    /**
     * Отменить бронь по номерам заказа
     * @param orderNumbers список номеров брони для отмены [ru.planeta.moscowshow.model.OrderPlaceResult.ticketCode]
     * @return объект с информацие об ошибке
     */
    fun cancelOrderByNumbers(orderNumbers: List<String>): ResponseCancelOrderByNumbers {
        return exec(RequestCancelOrderByNumbers(orderNumbers), SoapAction.CANCEL_ORDER_NUMBERS) as ResponseCancelOrderByNumbers
    }

    /**
     * Оплатить билет
     * @param ticketId идентификатор билета [ru.planeta.moscowshow.model.OrderPlaceResult.placeID]
     * @return объект с информацией об ошибке
     */
    fun payTicket(ticketId: Long): ResponsePayTicket {
        return exec(RequestPayTicket(ticketId), SoapAction.PAY_TICKET) as ResponsePayTicket
    }

    /**
     * Оплатить билеты
     * @param ticketIds идентификаторы билетов [ru.planeta.moscowshow.model.OrderPlaceResult.placeID]
     * @return объект с информацией об ошибке
     */
    fun payTickets(ticketIds: List<Long>): ResponsePayTickets {
        return exec(RequestPayTickets(ticketIds), SoapAction.PAY_TICKETS) as ResponsePayTickets
    }

    /**
     * Отменить оплату билета
     * @param ticketId идентификатор билета [ru.planeta.moscowshow.model.OrderPlaceResult.placeID]
     * @return объект с информацией об ошибке
     */
    fun unpayTicket(ticketId: Long): ResponseUnpayTicket {
        return exec(RequestUnpayTicket(ticketId), SoapAction.UNPAY_TICKET) as ResponseUnpayTicket
    }

    /**
     * Отменить оплату билетов
     * @param ticketIds идентификаторы билетов [ru.planeta.moscowshow.model.OrderPlaceResult.placeID]
     * @return объект с информацией об ошибке
     */
    fun unpayTickets(ticketIds: List<Long>): ResponseUnpayTickets {
        return exec(RequestUnpayTickets(ticketIds), SoapAction.UNPAY_TICKETS) as ResponseUnpayTickets
    }

    /**
     * Получить список проданных билетов
     * @return объект с информацией об ошибке и списком проданных билетов
     */
    fun listSoldTickets(): ResponseSoldTickets {
        return exec(RequestSoldTickets(), SoapAction.SOLD_TICKETS) as ResponseSoldTickets
    }

    @PostConstruct
    private fun init() {
        webServiceTemplate = WebServiceTemplate()
        val marshaller = Jaxb2Marshaller()
        marshaller.setPackagesToScan("ru.planeta.moscowshow.model")
        webServiceTemplate.defaultUri = defaultUrl
        webServiceTemplate.marshaller = marshaller
        webServiceTemplate.unmarshaller = marshaller
        LOG.info(MoscowShowClient::class.java.name + ": STARTED")
    }

    private fun getSessionId(): String? {
        if (sessionId == null) {
            synchronized(lock) {
                if (sessionId == null) {
                    val response = webServiceTemplate.marshalSendAndReceive(loginUser,
                            SoapActionCallback(SoapAction.LOGIN_USER)) as ResponseLoginUser
                    if (response.hasErrors()) {
                        LOG.error("Login: " + response.errorMessage)
                    }
                    sessionId = response.sessionId
                }
            }
        }
        return sessionId
    }

    private fun exec(requestPayload: Request, action: String): Any {
        requestPayload.sessionID = getSessionId()
        val callback = SoapActionCallback(action)
        var response = webServiceTemplate.marshalSendAndReceive(requestPayload, callback) as Response<*>
        if (response.isSessionClosed) {
            sessionId = null
            requestPayload.sessionID = getSessionId()
            response = webServiceTemplate.marshalSendAndReceive(requestPayload, callback) as Response<*>
        }
        return response
    }

}


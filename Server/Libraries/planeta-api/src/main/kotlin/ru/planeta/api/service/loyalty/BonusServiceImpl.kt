package ru.planeta.api.service.loyalty

import org.springframework.context.annotation.Lazy
import org.springframework.stereotype.Service
import ru.planeta.api.Utils
import ru.planeta.api.exceptions.NotFoundException
import ru.planeta.api.service.campaign.CampaignService
import ru.planeta.dao.commondb.BonusDAO
import ru.planeta.model.common.loyalty.Bonus
import ru.planeta.model.common.loyalty.BonusPriorityItem
import java.math.BigDecimal
import java.math.RoundingMode

/**
 * Created with IntelliJ IDEA.
 * User: alexa_000
 * Date: 04.04.14
 * Time: 20:55
 */

@Service
class BonusServiceImpl(var bonusDAO: BonusDAO,
                       @Lazy
                       var campaignService: CampaignService) : BonusService {

    override fun getBonuses(offset: Int, limit: Int): List<Bonus> {
        return bonusDAO.selectBonuses(offset, limit)
    }

    override fun getAllBonuses(offset: Int, limit: Int): List<Bonus> {
        return bonusDAO.selectAllBonuses(offset, limit)
    }

    override fun getBonus(bonusId: Long): Bonus? {
        val bonus = bonusDAO.select(bonusId)
        if (bonus != null && !Utils.empty(bonus.campaignAlias)) {
            val alias = bonus.campaignAlias
            val campaign = campaignService.getCampaign(alias!!)
            if (campaign != null && campaign.targetAmount!!.compareTo(BigDecimal.ZERO) > 0) {
                val campaignProgress = campaign.collectedAmount
                        .divide(campaign.targetAmount, 2, RoundingMode.HALF_UP)
                        .multiply(BigDecimal(100))
                        .toInt()

                bonus.campaignProgress = campaignProgress
                bonus.campaignName = campaign.name
            }
        }
        return bonus
    }

    @Throws(NotFoundException::class)
    override fun getBonusSafe(bonusId: Long): Bonus {
        return getBonus(bonusId) ?: throw NotFoundException(Bonus::class.java, bonusId)
    }

    override fun reorderBonuses(bonusPriorityItems: List<BonusPriorityItem>) {
        bonusDAO.updateBonusesOrder(bonusPriorityItems)
    }

    override fun saveBonus(bonus: Bonus) {
        if (bonus.bonusId > 0) {
            bonusDAO.update(bonus)
        } else {
            bonusDAO.insert(bonus)
        }
    }

}

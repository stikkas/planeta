package ru.planeta.api.service.campaign

import org.springframework.stereotype.Service
import ru.planeta.api.exceptions.MessageCode
import ru.planeta.api.exceptions.NotFoundException
import ru.planeta.api.exceptions.PermissionException
import ru.planeta.api.service.security.PermissionService
import ru.planeta.dao.commondb.CampaignFaqDAO
import ru.planeta.model.common.campaign.Campaign
import ru.planeta.model.commondb.CampaignFaq
import ru.planeta.model.commondb.CampaignFaqExample
import java.util.*

@Service
class CampaignFaqServiceImpl(var permissionService: PermissionService,
                             private var campaignService: CampaignService,
                             private var campaignFaqDAO: CampaignFaqDAO) : CampaignFaqService {

    val orderByClause: String = "order_num"


    fun insertCampaignFaqAllFields(campaignFaq: CampaignFaq) {
        campaignFaqDAO.insert(campaignFaq)
    }


    fun updateCampaignFaqAllFields(campaignFaq: CampaignFaq) {
        campaignFaqDAO.updateByPrimaryKey(campaignFaq)
    }

    fun insertOrUpdateCampaignFaq(campaignFaq: CampaignFaq) {
        val selectedCampaignFaq = campaignFaqDAO.selectByPrimaryKey(campaignFaq.campaignFaqId)
        if (selectedCampaignFaq == null) {

            campaignFaqDAO.insertSelective(campaignFaq)
        } else {
            campaignFaqDAO.updateByPrimaryKeySelective(campaignFaq)
        }
    }

    fun insertOrUpdateCampaignFaqAllFields(campaignFaq: CampaignFaq) {

        val selectedCampaignFaq = campaignFaqDAO.selectByPrimaryKey(campaignFaq.campaignFaqId)
        if (selectedCampaignFaq == null) {

            campaignFaqDAO.insert(campaignFaq)
        } else {
            campaignFaqDAO.updateByPrimaryKey(campaignFaq)
        }
    }

    override fun deleteCampaignFaq(id: Long?) {
        campaignFaqDAO.deleteByPrimaryKey(id)
    }

    override fun selectCampaignFaq(campaignFaqId: Long?): CampaignFaq {
        return campaignFaqDAO.selectByPrimaryKey(campaignFaqId)
    }

    fun getCampaignFaqExample(offset: Int, limit: Int): CampaignFaqExample {
        val example = CampaignFaqExample()
        example.offset = offset
        example.limit = limit
        example.or()
        example.orderByClause = orderByClause
        return example
    }

    fun selectCampaignFaqCount(): Int {
        return campaignFaqDAO.countByExample(getCampaignFaqExample(0, 0))
    }

    fun selectCampaignFaqList(offset: Int, limit: Int): List<CampaignFaq> {
        return campaignFaqDAO.selectByExample(getCampaignFaqExample(offset, limit))
    }

    fun selectCampaignFaqList(campaignFaqIdList: List<Long>?): List<CampaignFaq> {
        return if (campaignFaqIdList == null || campaignFaqIdList.isEmpty()) ArrayList() else campaignFaqDAO.selectByIdList(campaignFaqIdList)
    }

    fun getCampaignFaqByCampaignExample(campaignId: Long?, offset: Int, limit: Int): CampaignFaqExample {
        val example = CampaignFaqExample()
        example.offset = offset
        example.limit = limit
        example.or()
                .andCampaignIdEqualTo(campaignId)
        example.orderByClause = orderByClause
        return example
    }

    override fun selectCampaignFaqCountByCampaign(campaignId: Long?): Int {
        return campaignFaqDAO.countByExample(getCampaignFaqByCampaignExample(campaignId, 0, 0))
    }

    override fun selectCampaignFaqListByCampaign(campaignId: Long?, offset: Int, limit: Int): List<CampaignFaq> {
        return campaignFaqDAO.selectByExample(getCampaignFaqByCampaignExample(campaignId, offset, limit))
    }

    fun selectCampaignFaqListByCampaign(campaignId: Long?, campaignFaqIdList: List<Long>?): List<CampaignFaq> {
        return if (campaignFaqIdList == null || campaignFaqIdList.isEmpty()) ArrayList() else campaignFaqDAO.selectByIdList(campaignFaqIdList)
    }

    override fun insertCampaignFaq(campaignFaq: CampaignFaq) {
        val list = selectCampaignFaqListByCampaign(campaignFaq.campaignId, 0, 0)
        if (campaignFaq.orderNum == null) {
            campaignFaq.orderNum = list.size + 1
        } else {
            for (faq in list) {
                campaignFaq.orderNum?.let {
                    val num = faq.orderNum
                    if (num != null) {
                        if (num >= it) {
                            faq.orderNum = (faq.orderNum ?: 0) + 1
                            campaignFaqDAO.updateByPrimaryKeySelective(faq)
                        }
                    }
                }
            }
        }
        campaignFaqDAO.insertSelective(campaignFaq)
    }

    override fun updateCampaignFaq(campaignFaq: CampaignFaq) {
        val selectedFaq = selectCampaignFaq(campaignFaq.campaignFaqId)
        val selectedFaqONum = selectedFaq.orderNum
        val campaignFaqONum = campaignFaq.orderNum
        if (selectedFaqONum != campaignFaqONum) {
            val list = selectCampaignFaqListByCampaign(campaignFaq.campaignId, 0, 0)

            if (selectedFaqONum != null && campaignFaqONum != null) {
                if (selectedFaqONum < campaignFaqONum) {
                    for (faq in list) {
                        val faqONum = faq.orderNum
                        if (faqONum != null) {
                            if (faqONum > selectedFaqONum && faqONum <= campaignFaqONum) {
                                faq.orderNum = (faq.orderNum ?: 0) - 1
                                campaignFaqDAO.updateByPrimaryKeySelective(faq)
                            }
                        }
                    }
                } else {
                    for (faq in list) {
                        val oNum = faq.orderNum
                        val faqNum = campaignFaq.orderNum
                        if (oNum != null && faqNum != null) {
                            if (oNum >= faqNum && oNum < selectedFaqONum) {
                                faq.orderNum = oNum + 1
                                campaignFaqDAO.updateByPrimaryKeySelective(faq)
                            }
                        }
                    }
                }
            }

        }
        campaignFaqDAO.updateByPrimaryKeySelective(campaignFaq)
    }

    override fun selectCampaignFaqListByCampaign(campaignId: Long?): List<CampaignFaq> {
        return selectCampaignFaqListByCampaign(campaignId, 0, 0)
    }

    @Throws(NotFoundException::class, PermissionException::class)
    override fun updateOrCreateCampaignFaq(clientId: Long, campaignFaq: CampaignFaq): CampaignFaq {
        val campaign = campaignService.getCampaign(campaignFaq.campaignId)
                ?: throw NotFoundException(Campaign::class.java, campaignFaq.campaignId)
        if (!permissionService.isAdmin(clientId, campaign.profileId!!)) {
            throw PermissionException(MessageCode.USER_NOT_ADMIN)
        }
        if (campaignFaq.campaignFaqId == 0L) {
            insertCampaignFaq(campaignFaq)
        } else {
            updateCampaignFaq(campaignFaq)
        }
        return campaignFaq
    }

    @Throws(NotFoundException::class, PermissionException::class)
    override fun deleteCampaignFaq(clientId: Long, campaignId: Long, campaignFaqId: Long) {
        val campaign = campaignService.getCampaign(campaignId)
                ?: throw NotFoundException(Campaign::class.java, campaignId)
        if (!permissionService.isAdmin(clientId, campaign.profileId!!)) {
            throw PermissionException(MessageCode.USER_NOT_ADMIN)
        }
        deleteCampaignFaq(campaignFaqId)
    }

    @Throws(NotFoundException::class, PermissionException::class)
    override fun resort(clientId: Long, idFrom: Long, idTo: Long, campaignId: Long) {
        val campaign = campaignService.getCampaign(campaignId)
                ?: throw NotFoundException(Campaign::class.java, campaignId)
        if (!permissionService.isAdmin(clientId, campaign.profileId!!)) {
            throw PermissionException(MessageCode.USER_NOT_ADMIN)
        }

        var orderNumFrom = -1
        var orderNumTo = -1

        val campaignFaqs = selectCampaignFaqListByCampaign(campaignId, 0, 0)
        for (campaignFaq in campaignFaqs) {
            if (campaignFaq.campaignFaqId == idFrom) {
                orderNumFrom = campaignFaq.orderNum ?: 0
            }
            if (campaignFaq.campaignFaqId == idTo) {
                orderNumTo = campaignFaq.orderNum ?: 0
            }
        }

        if (orderNumFrom == orderNumTo || orderNumFrom < 0 || orderNumTo < 0) {
            return
        }
        if (orderNumFrom < orderNumTo) {
            for (campaignFaq in campaignFaqs) {
                val campaignFaqOrderNum = campaignFaq.orderNum
                if (campaignFaqOrderNum != null) {
                    if (campaignFaqOrderNum == orderNumFrom) {
                        campaignFaq.orderNum = orderNumTo
                        campaignFaqDAO.updateByPrimaryKey(campaignFaq)
                    } else if (campaignFaqOrderNum in (orderNumFrom + 1)..orderNumTo) {
                        campaignFaq.orderNum = (campaignFaq.orderNum ?: 0) - 1
                        campaignFaqDAO.updateByPrimaryKey(campaignFaq)
                    }
                }
            }
        } else {
            for (faqParagraph in campaignFaqs) {
                val faqParagraphOrderNum = faqParagraph.orderNum
                if (faqParagraphOrderNum != null) {
                    if (faqParagraphOrderNum == orderNumFrom) {
                        faqParagraph.orderNum = orderNumTo
                        campaignFaqDAO.updateByPrimaryKey(faqParagraph)
                    } else if (orderNumTo <= faqParagraphOrderNum && faqParagraphOrderNum < orderNumFrom) {
                        faqParagraph.orderNum = faqParagraphOrderNum + 1
                        campaignFaqDAO.updateByPrimaryKey(faqParagraph)
                    }
                }
            }
        }
    }

}

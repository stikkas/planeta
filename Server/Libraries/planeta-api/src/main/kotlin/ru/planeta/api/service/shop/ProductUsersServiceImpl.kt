package ru.planeta.api.service.shop

import org.apache.commons.collections4.IterableUtils
import org.apache.commons.lang3.StringEscapeUtils
import org.apache.commons.lang3.StringUtils
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import ru.planeta.api.BaseService
import ru.planeta.api.exceptions.MessageCode
import ru.planeta.api.exceptions.NotFoundException
import ru.planeta.api.exceptions.PaymentException
import ru.planeta.api.exceptions.PermissionException
import ru.planeta.dao.shopdb.ProductTagDAO
import ru.planeta.model.shop.Category
import ru.planeta.model.shop.Product
import ru.planeta.model.shop.ProductInfo
import ru.planeta.model.shop.enums.ProductCategory
import ru.planeta.model.shop.enums.ProductState
import ru.planeta.model.shop.enums.ProductState.*
import ru.planeta.model.shop.enums.ProductStatus
import java.util.*

/**
 * User: m.shulepov
 */
@Service
class ProductUsersServiceImpl(private val productTagDAO: ProductTagDAO) : BaseService(), ProductUsersService {

    override val currentProductsCount: Long
        get() = productDAO.selectCurrentProductsCount()

    private fun orderByAttributes(product: ProductInfo) {
        if (product.productState !== META) {
            return
        }
        product.childrenProducts?.sortWith(Comparator { attribute1,
                                                       attribute2 ->
            attribute1.productAttribute.index - attribute2.productAttribute.index
        })
    }

    @Throws(PermissionException::class, NotFoundException::class)
    override fun saveProduct(clientId: Long, product: ProductInfo): ProductInfo {

        permissionService.checkIsAdmin(clientId, product.merchantProfileId!!)

        if (product.productId != 0L && product.productState === META) {
            removeDeletedChildren(clientId, product)
        }

        // to detect SOLID_PRODUCT
        if (product.productState === ATTRIBUTE) {
            if (product.parentProductId > 0) {
                val parentProduct = productDAO.selectCurrent(product.parentProductId)
                if (parentProduct != null && parentProduct.productState !== META) {
                    product.productState = SOLID_PRODUCT
                }
            } else {
                product.productState = SOLID_PRODUCT
            }
        }

        saveProductVersion(product)
        // create children products from product and insert them
        if (product.productState === META) {
            createUpdateChildrenProducts(product)
            product.totalQuantity = countAndUpdateTotalQuantity(product.productId)
        }

        return product

    }

    @Throws(PermissionException::class, NotFoundException::class)
    override fun updateProductTags(clientId: Long, productId: Long, tags: List<Category>): List<Category> {
        val currentProduct = getProductCurrent(clientId, productId)
        permissionService.checkIsAdmin(clientId, currentProduct.merchantProfileId!!)

        currentProduct.setTags(tags)
        saveProductVersion(currentProduct)

        return tags
    }

    @Throws(PermissionException::class, NotFoundException::class)
    private fun removeDeletedChildren(clientId: Long, parent: ProductInfo) {
        val currentProduct = productDAO.selectCurrent(parent.productId)
                ?: throw NotFoundException(Product::class.java, parent.productId)

        val currentChildrenProducts = productDAO.selectChildrenProducts(currentProduct.productId)
        for (childrenProduct in currentChildrenProducts) {
            var isDeleted = true
            if (parent.childrenProducts != null) {
                for (freshChild in parent.childrenProducts!!) {
                    if (freshChild.productAttribute == childrenProduct.productAttribute) {
                        isDeleted = false
                        break
                    }
                }
            }

            if (isDeleted) {
                deleteProduct(clientId, childrenProduct.productId)
            }
        }
    }

    @Throws(NotFoundException::class)
    override fun getProductCurrent(clientId: Long, productId: Long): ProductInfo {
        val product = productDAO.selectCurrent(productId) ?: throw NotFoundException(Product::class.java, productId)

        val productInfo = ProductInfo(product)
        fullFillChildren(productInfo)
        return productInfo
    }

    @Throws(NotFoundException::class)
    override fun getProductSafe(productId: Long): ProductInfo {

        val product = productDAO.selectProduct(productId) ?: throw NotFoundException(Product::class.java, productId)

        return ProductInfo(product)
    }

    @Throws(PermissionException::class, NotFoundException::class)
    override fun pauseProduct(clientId: Long, productId: Long) {
        verifyAdministrativeRole(clientId)

        val product = productDAO.selectCurrent(productId) ?: throw NotFoundException(Product::class.java, productId)

        // store admin can only pause sale of a product with 'ACTIVE' status
        if (product.productStatus !== ProductStatus.ACTIVE) {
            throw PermissionException()
        }
        if (product.productState === ATTRIBUTE) {
            throw NotFoundException("Can not pause a child product #" + product.parentProductId)
        }

        productDAO.updateStatusActiveToPause(productId)

    }

    @Throws(NotFoundException::class, PermissionException::class)
    override fun startProduct(clientId: Long, productId: Long) {
        verifyAdministrativeRole(clientId)

        val product = productDAO.selectCurrent(productId) ?: throw NotFoundException(Product::class.java, productId)

        // store admin can only start sale of a product with 'PAUSE' status
        if (product.productStatus !== ProductStatus.PAUSE) {
            throw PermissionException(MessageCode.STORE_ADMIN_START_PAUSED_ORDER)
        }
        if (product.productState === ATTRIBUTE) {
            throw NotFoundException("Can not start a child product #" + product.parentProductId)
        }

        productDAO.updateStatusPauseToActiveAndSetStartDate(productId)
    }

    @Throws(PermissionException::class)
    private fun verifyAdministrativeRole(clientId: Long) {
        if (!permissionService.hasAdministrativeRole(clientId)) {
            throw PermissionException(MessageCode.USER_NOT_ADMIN)
        }
    }

    override fun getTopProducts(offset: Int, limit: Int, query: String, tagId: Int, status: String): List<ProductInfo> {

        val result = ArrayList<ProductInfo>()

        val products = productDAO.selectTopLevelProducts(offset, limit, query, tagId, status)
        for (product in products) {
            val productInfo = ProductInfo(product as Product)
            fullFillChildren(productInfo)
            result.add(productInfo)
        }

        return result
    }

    @Throws(NotFoundException::class, PermissionException::class)
    override fun deleteProduct(clientId: Long, productId: Long) {
        verifyAdministrativeRole(clientId)

        val product = getLastProductAfterPermissionPass(clientId, productId)
        product.dropChildrenProducts()

        val withChildren: Boolean
        when (product.productState) {
            ATTRIBUTE -> {
                if (product.totalOnHoldQuantity > 0) {
                    throw PermissionException(MessageCode.PRODUCT_ON_HOLD)
                }
                withChildren = false
            }
            META -> {
                if (product.totalOnHoldQuantity > 0) {
                    throw PermissionException(MessageCode.PRODUCT_ON_HOLD)
                }
                withChildren = true
            }
            SOLID_PRODUCT -> withChildren = false
            else -> withChildren = false
        }

        productDAO.updateStatusCurrentToOld(productId, withChildren)
        countAndUpdateTotalQuantity(product.parentProductId)
    }

    override fun magicProductsSearch(sortedProductIds: List<Long>): List<ProductInfo> {
        val productsDump = productDAO.selectProductsByIds(sortedProductIds)

        //get all products and rearrange them to tree and linear map both.
        val prodsMapLinear = HashMap<Long, ProductInfo>(productsDump.size)
        for (product in productsDump) {
            prodsMapLinear[product.productId] = product
        }

        rearrangeProductsDumpToTree(productsDump, prodsMapLinear)

        val products = ArrayList<ProductInfo>()
        for (id in sortedProductIds) {
            products.add(prodsMapLinear[id] ?: ProductInfo())
        }

        return products
    }

    private fun rearrangeProductsDumpToTree(productsStore: List<ProductInfo>, prodsMapLinear: Map<Long, ProductInfo>): List<ProductInfo> {
        val products = ArrayList<ProductInfo>()
        for (product in productsStore) {
            if (product.productState == ATTRIBUTE) {
                val parentMETA = prodsMapLinear[product.parentProductId]
                if (parentMETA != null) {
                    parentMETA.childrenProducts!!.add(product)
                } else {
                    products.add(product)
                }
            }
            if (product.productState == META || product.productState === SOLID_PRODUCT) {
                products.add(product)
            }
        }
        return products
    }

    @Throws(NotFoundException::class, PermissionException::class)
    override fun updateProductTotalQuantity(clientId: Long, productId: Long, quantity: Int) {

        val product = productDAO.selectCurrent(productId) ?: throw NotFoundException(Product::class.java, productId)
        permissionService.checkIsAdmin(clientId, product.merchantProfileId!!)

        if (quantity < product.totalOnHoldQuantity) {
            throw PermissionException("New quantity less then products hold for order. Product #$productId")
        }

        if (product.productState === META) {
            throw PermissionException("Total quantity of " + product.productState + " cannot be modified directly.  Product #" + productId)
        }

        productDAO.updateTotalQuantity(productId, quantity.toLong())

        if (product.parentProductId != 0L) {
            countAndUpdateTotalQuantity(product.parentProductId)
        }
    }

    @Throws(PermissionException::class, NotFoundException::class)
    override fun getProductsByParent(clientId: Long, parentId: Long): List<ProductInfo> {
        getLastProductAfterPermissionPass(clientId, parentId)
        val products = productDAO.selectChildrenProducts(parentId)
        val result = ArrayList<ProductInfo>()
        for (product in products) {
            val productInfo = ProductInfo(product)
            fullFillChildren(productInfo)
            result.add(productInfo)
        }
        return result
    }

    override fun getProductShortCurrent(myProfileId: Long, productId: Long): Product? {
        return if (productId == 0L) {
            null
        } else productDAO.selectCurrent(productId)
    }

    override fun getSimilarProducts(productId: Long, limit: Int): List<Product> {
        return productDAO.getSimilarProducts(productId, limit)
    }

    override fun getProducts(productsIds: List<Long>): List<ProductInfo> {
        //todo сделать кэширование
        val products = productDAO.selectActiveProductByIdList(productsIds)
        for (product in products) {
            fullFillChildren(product)
        }
        return products
    }

    @Throws(NotFoundException::class, PaymentException::class)
    override fun getProductReadyForPurchase(buyerId: Long, productId: Long, count: Int): ProductInfo {
        if (count <= 0) {
            throw PaymentException(MessageCode.INCORRECT_PRODUCT_AMOUNT)
        }
        if (productId == 0L) {
            throw IllegalArgumentException("zero.id.productId")
        }
        val product = productDAO.selectCurrent(productId) ?: throw NotFoundException(ProductInfo::class.java, productId)
        val result = ProductInfo(product)

        if (result.productCategory === ProductCategory.PHYSICAL && result.readyForPurchaseQuantity < count) {
            throw PaymentException(MessageCode.NOT_ENOUGH_PRODUCT_AMOUNT)
        }
        if (META == result.productState) {
            throw PaymentException(MessageCode.NOT_CHILD_PRODUCT)
        }

        return result
    }

    @Throws(NotFoundException::class)
    override fun updateProductAfterPurchase(buyerId: Long, productId: Long, count: Int) {

        productDAO.updateTotalPurchaseCount(productId, count)
        val product = productDAO.selectProduct(productId) ?: throw NotFoundException(Product::class.java, productId)

        if (product.productCategory !== ProductCategory.PHYSICAL) {
            return
        }

        if (product.productState === ATTRIBUTE) {
            productDAO.updateTotalPurchaseCount(product.parentProductId, count)
        }
        if (product.productState === SOLID_PRODUCT) {
            productDAO.updateTotalPurchaseCount(productId, count)
        }
    }

    @Throws(NotFoundException::class, PermissionException::class)
    override fun updateProductAfterPurchaseCancellation(buyerId: Long, productId: Long, count: Int) {
        updateProductAfterPurchase(buyerId, productId, -count)
    }

    /**
     * specified parent-product children fields updated, quantities inserted and saved.
     *
     * @param parent - modified
     */
    private fun createUpdateChildrenProducts(parent: ProductInfo) {

        val vault = ProductInfo()
        for (child in parent.childrenProducts!!) {
            vault.copy(child)
            child.copy(parent)

            child.productId = vault.productId
            child.totalOnHoldQuantity = vault.totalOnHoldQuantity
            child.totalQuantity = vault.totalQuantity
            child.productAttribute = vault.productAttribute
            child.childrenCount = 0
            child.timeAdded = Date()
            child.timeUpdated = Date()

            child.parentProductId = parent.productId
            child.productState = ATTRIBUTE
            child.contentUrls = vault.contentUrls
            saveProductVersion(child)
        }
    }

    override fun bindToProfile(productId: Long, referrerId: Long, showOnCampaign: Boolean, campaignIds: LongArray) {
        productDAO.bindToProfile(productId, referrerId, showOnCampaign, campaignIds)
    }

    override fun getProductListForCampaignByReferrer(referrerId: Long, campaignId: Long, offset: Int, limit: Int): List<Product> {
        return productDAO.getProductListForCampaignByReferrer(referrerId, campaignId, offset, limit)
    }

    override fun getProductsCountByReferrer(referrerId: Long): Int {
        return productDAO.getProductsCountByReferrer(referrerId)
    }

    @Throws(NotFoundException::class)
    override fun cartItemToDTO(productId: Long, count: Int): Map<String, Any> {
        val product = getProductSafe(productId)
        val dto = HashMap<String, Any>()
        var rightCount = count
        if (product.productCategory === ProductCategory.PHYSICAL) {
            rightCount = Math.min(count, product.totalQuantity - product.totalOnHoldQuantity)
        }
        dto["countChanged"] = rightCount != count
        dto["objectId"] = productId.toString()
        dto["objectImageUrl"] = product.coverImageUrl as Any
        dto["objectName"] = product.name as Any
        dto["price"] = product.price as Any
        dto["count"] = rightCount
        dto["productCategory"] = product.productCategory
        product.startSaleDate?.let {
            if (it > Date()) {
                dto["startSaleDate"] = product.startSaleDate as Any
            }
        }
        dto["cashAvailable"] = product.isCashAvailable
        product.getTags()?.let {
            if (it.isNotEmpty()) {
                dto["mainTag"] = it[0].value as Any
            } else {
                dto["mainTag"] = "" as Any
            }
        }
        dto["parentObjectId"] = product.parentProductId
        dto["noFreeDelivery"] = IterableUtils.matchesAny(product.getTags()) { category -> category.mnemonicName == Category.NO_FREE_DELIVERY }
        dto["attribute"] = product.productAttribute.value as Any
        return dto
    }

    override fun getDonateProductByReferrer(profileId: Long): Long {
        return productDAO.getDonateProductByReferrer(profileId)
    }

    @Throws(NotFoundException::class, PermissionException::class)
    override fun cloneProduct(clientId: Long, productId: Long): ProductInfo {
        val baseProduct = ProductInfo(productDAO.selectCurrent(productId)!!)
        fullFillChildren(baseProduct)
        val newProduct = ProductInfo()
        newProduct.cloneWithChildren(baseProduct)
        newProduct.name = newProduct.name!! + " [clone]"
        newProduct.nameHtml = newProduct.nameHtml!! + " [clone]"
        return saveProduct(clientId, newProduct)
    }

    override fun removeNewTagsFromOldProducts() {
        productTagDAO.removeNewTagsFromOldProducts()
    }

    private fun saveProductVersion(product: ProductInfo) {
        if (!StringUtils.isEmpty(product.description)) {
            product.description = product.description!!.trim { it <= ' ' }
            /*product.setDescriptionHtml(formattingService.formatPlainText(product.getDescription(), 0, true));*/
        }

        product.nameHtml = StringEscapeUtils.escapeHtml4(product.name)

        if (product.startSaleDate == null) {
            product.startSaleDate = Date()
        }

        productDAO.save(product)
        productTagDAO.updateTags(product.productId, product.getTags())

        val productUpdated = productDAO.selectCurrent(product.productId)
        product.productStatus = productUpdated!!.productStatus
    }


    /**
     * product tree according to `selectionGroup`
     *
     * @param product        product container
     */
    private fun fullFillChildren(product: ProductInfo) {
        if (product.productState === ATTRIBUTE || product.productState === SOLID_PRODUCT) {
            return
        }

        var totalQuantity: Int = 0
        var totalOnHoldQuantity = 0
        val childrenProducts = productDAO.selectChildrenProducts(product.productId) ?: return

        childrenProducts.forEach {
            val childInfo = ProductInfo(it)
            fullFillChildren(childInfo)
            totalQuantity += childInfo.totalQuantity
            totalOnHoldQuantity += childInfo.totalOnHoldQuantity
            product.childrenProducts?.add(childInfo)
        }

        product.childrenCount = childrenProducts.size
        orderByAttributes(product)
        product.totalQuantity = totalQuantity
        product.totalOnHoldQuantity = totalOnHoldQuantity
    }

    /**
     * returns last version of product if requester have permissions for it.
     * permission role no less than admin and product exists.
     *
     * @param clientId  requester
     * @param productId target product
     * @return LAST version of product
     */
    @Throws(NotFoundException::class, PermissionException::class)
    private fun getLastProductAfterPermissionPass(clientId: Long, productId: Long): ProductInfo {
        val product = productDAO.selectCurrent(productId) ?: throw NotFoundException(Product::class.java, productId)

        permissionService.checkIsAdmin(clientId, product.merchantProfileId!!)
        return ProductInfo(product)
    }

    /**
     * updates total quantity count on product by confirmed children.
     *
     * @param productId product id
     * @return counted quantity
     */
    private fun countAndUpdateTotalQuantity(productId: Long): Int {
        val product = productDAO.selectCurrent(productId) ?: return 0

        if (product.productCategory === ProductCategory.DIGITAL) return 1

        if (product.productState === ProductState.ATTRIBUTE || product.productState === ProductState.SOLID_PRODUCT) {
            return product.totalQuantity
        }
        //META, GROUP
        var totalQuantity = 0
        val currentChildren = productDAO.selectChildrenProducts(productId)

        for (child in currentChildren) {
            if (child.productState === ProductState.META) {
                totalQuantity += countAndUpdateTotalQuantity(child.productId)
            } else {
                totalQuantity += child.totalQuantity
            }
        }

        productDAO.updateTotalQuantity(productId, totalQuantity.toLong())
        return totalQuantity
    }
}

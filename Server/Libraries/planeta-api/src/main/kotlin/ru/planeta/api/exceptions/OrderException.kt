package ru.planeta.api.exceptions

/**
 * Order exception.<br></br>
 * Throws during order creation, processing or validation.<br></br>
 * User: eshevchenko
 */
class OrderException : BaseException {

    constructor(cause: Throwable) : super(cause) {}

    constructor(messageCode: MessageCode, formattedMessage: String) : super(messageCode, formattedMessage) {}

    constructor(messageCode: MessageCode) : super(messageCode) {}
}

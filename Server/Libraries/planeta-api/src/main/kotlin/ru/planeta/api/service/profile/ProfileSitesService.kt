package ru.planeta.api.service.profile


import ru.planeta.model.profiledb.ProfileSites

interface ProfileSitesService {

    fun insertOrUpdateProfileSites(profileSites: ProfileSites)

    fun selectProfileSites(profileId: Long): ProfileSites
}

package ru.planeta.api.service.promo

import org.apache.log4j.Logger
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import ru.planeta.dao.promo.TechnobattleVoterDAO
import ru.planeta.model.promo.TechnobattleVote
import ru.planeta.model.promo.VoteType

/**
 * Created with IntelliJ IDEA.
 * User: michail
 * Date: 16.01.2017
 * Time: 14:52
 */
@Service
class VoteServiceImpl(var technobattleVoterDAO: TechnobattleVoterDAO) : VoteService {


    override fun checkVoteExistingOrAddNewVote(userId: String, projectId: Long, voteType: VoteType): TechnobattleVote? {
        if (technobattleVoterDAO.selectVotesCountForUser(userId, voteType) == 0) {
            val vote = TechnobattleVote(userId, projectId, voteType)
            technobattleVoterDAO.insert(vote)
            return vote
        } else {
            log.warn("User $userId already votes using $voteType")
            return null
        }
    }

    override fun getVotesCountForProject(projectId: Long): Int {
        return technobattleVoterDAO!!.selectVotesCountForProject(projectId)
    }

    companion object {
        private val log = Logger.getLogger(VoteServiceImpl::class.java)
    }
}

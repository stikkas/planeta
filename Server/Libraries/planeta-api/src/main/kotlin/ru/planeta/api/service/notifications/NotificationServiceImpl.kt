package ru.planeta.api.service.notifications

import com.fasterxml.jackson.core.type.TypeReference
import com.fasterxml.jackson.databind.ObjectMapper
import org.apache.commons.codec.digest.DigestUtils
import org.apache.commons.io.IOUtils
import org.apache.commons.lang3.StringUtils
import org.apache.commons.lang3.time.DateUtils.addDays
import org.apache.commons.lang3.tuple.Pair
import org.apache.log4j.Logger
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.MessageSource
import org.springframework.context.annotation.Lazy
import org.springframework.stereotype.Service
import ru.planeta.api.Utils
import ru.planeta.api.exceptions.MessageCode
import ru.planeta.api.exceptions.NotFoundException
import ru.planeta.api.exceptions.PermissionException
import ru.planeta.api.helper.ProjectService
import ru.planeta.api.mail.MailClient
import ru.planeta.api.model.ActionStatus
import ru.planeta.api.model.enums.image.ImageType
import ru.planeta.api.model.json.PremailerResponse
import ru.planeta.api.service.biblio.BookService
import ru.planeta.api.service.billing.order.OrderService
import ru.planeta.api.service.campaign.CampaignService
import ru.planeta.api.service.common.PlanetaManagersService
import ru.planeta.api.service.configurations.ConfigurationService
import ru.planeta.api.service.configurations.ImageHelperFunctions
import ru.planeta.api.service.configurations.StaticNodesService
import ru.planeta.api.service.profile.AuthorNotificationService
import ru.planeta.api.service.profile.ProfileService
import ru.planeta.api.service.profile.ProfileSubscriptionService
import ru.planeta.api.service.security.PermissionService
import ru.planeta.api.utils.DateUtilsRus
import ru.planeta.commons.lang.DateUtils.isBeforeNow
import ru.planeta.commons.web.WebUtils
import ru.planeta.dao.bibliodb.LibraryDAO
import ru.planeta.dao.commondb.UserPrivateInfoDAO
import ru.planeta.dao.commondb.CampaignDAO
import ru.planeta.dao.commondb.CampaignTagDAO
import ru.planeta.dao.commondb.ShareDAO
import ru.planeta.dao.profiledb.PostDAO
import ru.planeta.dao.shopdb.ProductDAO
import ru.planeta.model.bibliodb.Book
import ru.planeta.model.bibliodb.BookOrder
import ru.planeta.model.bibliodb.BookTag
import ru.planeta.model.bibliodb.Library
import ru.planeta.model.common.*
import ru.planeta.model.common.campaign.Campaign
import ru.planeta.model.common.campaign.CampaignSubscribe
import ru.planeta.model.common.charity.CharityEventOrder
import ru.planeta.model.common.school.Seminar
import ru.planeta.model.common.school.SeminarRegistrationCompany
import ru.planeta.model.common.school.SeminarRegistrationSoloSimple
import ru.planeta.model.enums.*
import ru.planeta.model.enums.ProfileStatus.USER_BOUNCED
import ru.planeta.model.mail.MailMessagePriority
import ru.planeta.model.profile.Comment
import ru.planeta.model.profile.Post
import ru.planeta.model.profile.Profile
import ru.planeta.model.promo.TechnobattleRegistration
import ru.planeta.model.shop.Product
import ru.planeta.model.shop.enums.DeliveryType
import ru.planeta.model.stat.CampaignStat
import ru.planeta.model.trashcan.SchoolOnlineCourseApplication
import java.io.IOException
import java.io.UnsupportedEncodingException
import java.math.BigDecimal
import java.net.URL
import java.net.URLEncoder
import java.util.*
import java.util.concurrent.Executors
import javax.annotation.PostConstruct
import javax.servlet.http.HttpServletResponse

@Service
class NotificationServiceImpl(
        @Value("\${support.email}")
        private val supportEmail: String,
        @Value("\${vipCurator.email}")
        private val vipCuratorEmail: String,
        @Value("\${investingManagerEmail.email:admin@dev.planeta.ru}")
        private val investingManagerEmail: String,
        @Value("\${shop.email:admin@dev.planeta.ru}")
        private val shopEmail: String,
        @Value("\${noreply.email}")
        private val noreplyEmail: String,
        @Value("\${widgets.application.host}")
        private val widgetsHost: String,
        @Value("\${application.host}")
        private val applicationHost: String,
        private val notificationTemplateService: NotificationTemplateService,
        private val campaignDAO: CampaignDAO,
        private val campaignTagDAO: CampaignTagDAO,
        private val userPrivateInfoDAO: UserPrivateInfoDAO,
        protected var messageSource: MessageSource,
        protected val permissionService: PermissionService,
        private val configurationService: ConfigurationService,
        private val staticNodesService: StaticNodesService,
        private val projectService: ProjectService,
        private val productDAO: ProductDAO,
        private val postDAO: PostDAO,
        private val mailClient: MailClient,
        private val profileService: ProfileService,
        private val orderService: OrderService,
        private val bookService: BookService,
        private val libraryDAO: LibraryDAO,
        @Lazy
        private val campaignService: CampaignService,
        private val authorNotificationService: AuthorNotificationService,
        private val shareDAO: ShareDAO,
        private val profileSubscriptionService: ProfileSubscriptionService,
        private val planetaManagersService: PlanetaManagersService) : NotificationService {

    private var dateUtilsRus: DateUtilsRus? = null


    private val executorService = Executors.newFixedThreadPool(4)

    // This variable is used for tests purposes only
    private var isAsynchronous = true

    enum class RichTemplateType {
        PREMAILER_TRANSFORM,
        NEW_BLOG_POST_IN_CAMPAIGN
    }

    @PostConstruct
    fun initDateUtilsRusService() {
        dateUtilsRus = DateUtilsRus(messageSource)
    }

    override fun setAsynchronous(asynchronous: Boolean) {
        isAsynchronous = asynchronous
    }

    @Throws(NotFoundException::class)
    override fun sendCampaignNewsAddedNotificationSecure(clientId: Long, campaignId: Long, profileId: Long, postId: Long): ActionStatus<*> {
        val post = postDAO.select(postId) ?: throw NotFoundException(Post::class.java, postId)
        if (permissionService.hasAdministrativeRole(clientId)) {
            sendCampaignNewsAddedNotification(campaignId, post)
            return ActionStatus.createSuccessStatus<Any>()
        } else if (permissionService.isProfileAdmin(clientId, profileId)) {
            if (isPostNotificationAllow(post)) {
                sendCampaignNewsAddedNotification(campaignId, post)
                return ActionStatus.createSuccessStatus<Any>()
            } else {
                return ActionStatus.createErrorStatus<Any>("wrong.campaignSubscribersNotificationSettings.frequencyExceeded", messageSource)
            }
        }

        return ActionStatus.createErrorStatus<Any>("wrong.campaignSubscribersNotificationSettings.accessDenied", messageSource)
    }

    override fun sendPurchaseProductNotificationEmail(orderInfo: OrderInfo, groupedOrderObjects: Map<Long, List<OrderObjectInfo>>) {
        try {
            val params = HashMap<String, Any>()
            params["order"] = orderInfo
            params["orderObjects"] = groupedOrderObjects
            params["dearByGender"] = getMessage("dear.gender", orderInfo.buyerGender.code)

            var isDigital = false
            for (orderObjectInfo in orderInfo.orderObjectsInfo) {
                if (orderObjectInfo.downloadUrls != null && (orderObjectInfo.downloadUrls?.isEmpty() != true)) {
                    isDigital = true
                    break
                }
            }
            val mailTo = orderInfo.buyerEmail
            params["mailTo"] = mailTo as Any
            params["isDigital"] = isDigital
            val emailSubject = getMessage("email.subject.order.accepted")
            val emailBody = notificationTemplateService.applyCustomTemplate(ORDER_PURCHASED_TEMPLATE, params)

            mailClient.sendMessageRich(ORDER_PURCHASED_TEMPLATE, mailTo!!, supportEmail!!, emailSubject, emailBody, MailMessagePriority.HIGH)
        } catch (ex: Exception) {
            log.warn("Exception on rich e-mail send", ex)
        }

    }

    override fun sendMailFromAboutUsAdvertising(mailFrom: String, message: String) {
        try {
            mailClient.sendMailFromAboutUsAdvertising(mailFrom, message)
        } catch (ex: Exception) {
            log.warn("Exception on e-mail send from about us contact form", ex)
        }

    }

    override fun sendPurchaseBiblioNotificationEmail(orderInfo: OrderInfo) {
        try {
            val bookMap = HashMap<Long, Int>()
            val libraries = ArrayList<Library>()

            for (orderObject in orderInfo.orderObjectsInfo) {
                when (orderObject.objectType) {
                    OrderObjectType.BOOK -> (bookMap as java.util.Map<Long, Int>).merge(orderObject.objectId, 1) { a, b -> a + b }
                    OrderObjectType.LIBRARY -> {
                        val library = libraryDAO.select(orderObject.objectId)
                        if (library != null) {
                            libraries.add(library)
                        }
                    }
                }
            }

            val books = ArrayList<Pair<Book, Int>>()
            for ((key, value) in bookMap) {
                val book = bookService.getBookById(key)
                if (value != null && book != null) {
                    books.add(Pair.of(book, value))
                }
            }

            val params = HashMap<String, Any>()
            params["order"] = orderInfo
            params["books"] = books
            params["libraries"] = libraries
            params["dearByGender"] = getMessage("dear.gender", orderInfo.buyerGender.code)

            val mailTo = orderInfo.buyerEmail
            params["mailTo"] = mailTo as Any
            val emailSubject = getMessage("biblio.email.subject.order.accepted")
            val emailBody = notificationTemplateService.applyCustomTemplate(BIBLIO_ORDER_PURCHASED_TEMPLATE, params)

            if (StringUtils.isBlank(emailBody)) {
                log.warn("sendPurchaseBiblioNotificationEmail: empty emailBody for order " + orderInfo.orderId)
            } else {
                mailClient.sendMessageRich(BIBLIO_ORDER_PURCHASED_TEMPLATE, mailTo!!, supportEmail!!, emailSubject, emailBody, MailMessagePriority.HIGH)
            }
        } catch (ex: Exception) {
            log.warn("Exception on rich e-mail send", ex)
        }

    }

    @Throws(NotFoundException::class)
    override fun sendUserBecameVipNotification(profile: Profile) {
        val params = Utils.map(
                "profile", profile,
                "unsubscribeHash", DigestUtils.md5Hex(SECRET_KEY + profile.profileId),
                "count", configurationService.vipClubJoinCondition.getCount(),
                "amount", humanNumber(configurationService.vipClubJoinCondition.amount ?: BigDecimal.ZERO),
                "second", humanNumber(configurationService.vipClubJoinCondition.second ?: BigDecimal.ZERO)
        )

        val emailSubject = getMessage("email.subject.vip.welcome")
        log.debug("VipNotificationEmailSubject $emailSubject")
        val emailBody = notificationTemplateService.applyCustomTemplate(USER_BECAME_VIP, params.toMutableMap())
        log.debug("VipNotificationEmailBody $emailBody")

        mailClient.sendMessageRich(USER_BECAME_VIP, getUserEmailSafe(profile)!!, noreplyEmail!!, emailSubject, emailBody, MailMessagePriority.DEFAULT)
    }

    @Throws(NotFoundException::class)
    private fun getUserEmailSafe(profile: Profile): String? {
        if (profile.status === USER_BOUNCED) {
            throw NotFoundException(MessageCode.WRONG_EMAIL)
        }
        val u = userPrivateInfoDAO.selectByUserId(profile.profileId)
        if (u == null || u.email == null) {
            throw NotFoundException(UserPrivateInfo::class.java, profile.profileId)
        }
        return u.email
    }

    private fun getMessage(code: String, vararg args: Any): String {
        return messageSource.getMessage(code, args, Locale.getDefault())
    }

    override fun sendCampaignCanMakeVipOfferNotification(authorsEmails: Collection<String>) {
        val emailSubject = getMessage("email.subject.vip.invite")
        val params = Utils.map("count", configurationService.vipClubJoinCondition.getCount(),
                "amount", humanNumber(configurationService.vipClubJoinCondition.amount ?: BigDecimal.ZERO))
        val emailBody = notificationTemplateService.applyCustomTemplate(CAMPAIGN_BECAME_VIP_OFFERS_ALLOWED, params.toMutableMap())

        for (email in authorsEmails) {
            mailClient.sendMessageRich(CAMPAIGN_BECAME_VIP_OFFERS_ALLOWED, email, vipCuratorEmail!!, emailSubject, emailBody, MailMessagePriority.DEFAULT)
        }
    }

    private fun isPostNotificationAllow(post: Post): Boolean {
        val notificationTime = post.notificationTime
        return notificationTime == null || isBeforeNow(addDays(notificationTime, configurationService.newsNotificationDaysLimit))
    }


    private fun sendCampaignNewsAddedNotification(campaignId: Long, post: Post) {
        val campaign = campaignDAO.selectCampaignById(campaignId) ?: return
        val emailsTask = Runnable {
            post.notificationTime = Date()
            postDAO.update(post)

            val params = HashMap<String, Any>()
            val blogPostUrl = projectService.getUrl(ProjectType.MAIN, "/campaigns/" + campaign.webCampaignAlias + "/updates!post" + post.id)
            val blogPostTextHtml = formatPostForEmail(post, blogPostUrl)
            if (!blogPostTextHtml.isEmpty()) {

                params["blogPostHtml"] = blogPostTextHtml
                params["blogPostLink"] = blogPostUrl
                params["noInlineStyleHtml"] = blogPostTextHtml
                params["campaignName"] = campaign.name as Any
                params["campaignId"] = campaign.campaignId
                params["blogPostTitle"] = post.title as Any
                params["blogPostDate"] = dateUtilsRus?.dateFormat(post.timeAdded) as Any
                params["templateName"] = RichTemplateType.NEW_BLOG_POST_IN_CAMPAIGN.toString()
                val emailSubject = getMessage("email.subject.campaign.new.blogpost", campaign.name!!, post.title!!)
                var offset = 0
                val limit = 900

                for (iter in 0 until ERRCOUNT) {
                    val userPrivateInfos = userPrivateInfoDAO.selectSubscriberListToSpam(campaign.creatorProfileId, offset, limit)
                    if (userPrivateInfos.isEmpty()) {
                        break
                    }
                    for (userPrivateInfo in userPrivateInfos) {
                        sendCampaignNewsEmail(emailSubject, userPrivateInfo, params)
                    }
                    offset += limit
                }
            }
        }
        executeTask(emailsTask)
    }

    override fun sendPurchaseShareNotificationEmail(orderInfo: OrderInfo, rewardInstruction: String) {
        val hasInstruction = StringUtils.isNotBlank(rewardInstruction)

        try {
            getProfileForEmail(orderInfo.buyerId)
            val share = orderInfo.orderObjectsInfo.iterator().next()
            val params = HashMap<String, Any>()
            params["order"] = orderInfo
            params["share"] = share
            params["supportEmail"] = supportEmail

            if (orderInfo.deliveryType !== DeliveryType.NOT_SET) {
                params["address"] = orderInfo.deliveryAddress as Any
                val delivery = orderInfo.linkedDelivery
                if (delivery?.price == null) {
                    delivery?.price = BigDecimal.ZERO
                }
                params["delivery"] = delivery as Any
            }

            if (hasInstruction) {
                params["instruction"] = rewardInstruction
            }

            val emailTo = orderInfo.buyerEmail
            params["mailTo"] = emailTo as Any

            val emailBody = notificationTemplateService.applyCustomTemplate(SHARE_PURCHASE_EMAIL_TEMPLATE, params)
            val emailTitle = messageSource.getMessage("thanks.for.purchasing.shares", null, Locale.getDefault())
            mailClient.sendMessageRich(SHARE_PURCHASE_EMAIL_TEMPLATE, emailTo!!, supportEmail!!, emailTitle, emailBody, MailMessagePriority.HIGH)

        } catch (ex: Exception) {
            log.warn("Exception on rich e-mail send", ex)
        }

    }

    override fun sendOrderSentNotificationEmail(orderInfo: OrderInfo): Boolean {

        try {
            getProfileForEmail(orderInfo.buyerId)
            val params = HashMap<String, Any>()
            params["order"] = orderInfo
            params["address"] = orderInfo.deliveryAddress as Any
            params["userName"] = orderInfo.buyerName as Any

            val emailTo = orderInfo.buyerEmail
            params["mailTo"] = emailTo as Any

            params["emailNode"] = EmailNodes.BODY
            val emailBody = notificationTemplateService.applyCustomTemplate(ORDER_SENT_TEMPLATE, params)
            if (Utils.empty(emailBody)) {
                log.warn("Address of failed delivery is: ")
                log.warn(orderInfo.deliveryAddress)
                return false
            }
            params["emailNode"] = EmailNodes.SUBJECT
            val emailTitle = notificationTemplateService.applyCustomTemplate(ORDER_SENT_TEMPLATE, params)
            mailClient.sendMessageRich(ORDER_SENT_TEMPLATE, emailTo!!, supportEmail!!, emailTitle, emailBody, MailMessagePriority.DEFAULT)
        } catch (ex: Exception) {
            log.warn("Exception on rich e-mail send", ex)
            return false
        }

        return true
    }

    override fun sendOrderGivenNotificationEmail(orderInfo: OrderInfo): Boolean {
        try {
            getProfileForEmail(orderInfo.buyerId)
            val params = HashMap<String, Any>()
            params["order"] = orderInfo
            params["userName"] = orderInfo.buyerName as Any
            val emailTo = orderInfo.buyerEmail
            params["mailTo"] = emailTo as Any

            params["emailNode"] = EmailNodes.BODY
            val emailBody = notificationTemplateService.applyCustomTemplate(ORDER_GIVEN_TEMPLATE, params)
            if (Utils.empty(emailBody)) {
                log.warn("Id of failed order is: ")
                log.warn(orderInfo.orderId)
                return false
            }
            params["emailNode"] = EmailNodes.SUBJECT
            val emailTitle = notificationTemplateService.applyCustomTemplate(ORDER_GIVEN_TEMPLATE, params)
            mailClient.sendMessageRich(ORDER_GIVEN_TEMPLATE, emailTo!!, supportEmail!!, emailTitle, emailBody, MailMessagePriority.DEFAULT)
        } catch (ex: Exception) {
            log.warn("Exception on rich e-mail send", ex)
            return false
        }

        return true
    }

    @Throws(NotFoundException::class, PermissionException::class)
    private fun getProfileForEmail(profileId: Long): Profile {
        val profile = profileService.getProfileSafe(profileId)
        if (profile.status === USER_BOUNCED) {
            throw PermissionException(MessageCode.WRONG_EMAIL)
        }
        return profile
    }

    override fun sendOrderCancelNotification(order: Order, reason: String) {
        try {

            val profile = getProfileForEmail(order.buyerId)
            val email = getUserEmailSafe(profile)

            val serviceText: String
            when (order.orderType) {
                OrderObjectType.PRODUCT -> serviceText = messageSource.getMessage("email.service.text.shop", null, Locale.getDefault())
                OrderObjectType.DELIVERY -> serviceText = messageSource.getMessage("email.service.text.delivery", null, Locale.getDefault())
                OrderObjectType.SHARE, OrderObjectType.INVESTING, OrderObjectType.INVESTING_WITH_ALL_ALLOWED_INVOICE, OrderObjectType.INVESTING_WITHOUT_MODERATION -> {
                    val campaign = orderService.getCampaignByOrderId(order.orderId)
                    serviceText = messageSource.getMessage("email.service.text.campaign", null, Locale.getDefault()) + " \"" + campaign.name + "\""
                }
                else -> serviceText = ""
            }
            mailClient.sendOrderCancelledEmailToUser(email!!, profile, order, reason, serviceText)
        } catch (ex: Exception) {
            log.warn("Exception on rich e-mail send", ex)
        }

    }

    override fun sendSchoolWebinarRegistration(email: String, seminar: Seminar) {
        try {
            mailClient.sendSchoolWebinarRegistration(email, seminar, getMailThumbnaiUrl(seminar))
        } catch (ex: Exception) {
            log.warn("Exception on e-mail send", ex)
        }

    }

    override fun sendSchoolOnlineCourse(application: SchoolOnlineCourseApplication) {
        try {
            mailClient.sendSchoolOnlineCourse(application.email!!, application.name!!)
        } catch (ex: Exception) {
            log.warn("Exception on e-mail send", ex)
        }

    }

    override fun sendSchoolSolosimpleRegistration(seminarRegistration: SeminarRegistrationSoloSimple, seminar: Seminar) {
        try {
            if (seminarRegistration.seminarFormat == true) {
                mailClient.sendSchoolSolosimpleRegistration(seminarRegistration.email!!, seminar, getMailThumbnaiUrl(seminar))
            } else {
                mailClient.sendSchoolSolosimpleOfflineRegistration(seminarRegistration.email!!, seminar, getMailThumbnaiUrl(seminar))
            }
        } catch (ex: Exception) {
            log.warn("Exception on e-mail send", ex)
        }

    }


    override fun sendBiblioBookRequest(request: Book) {
        try {
            mailClient.sendBiblioBookRequest(request.email!!, request)
        } catch (ex: Exception) {
            log.error("Exception on sendBiblioBookRequest", ex)
        }

    }

    override fun sendTechnobattleProgrammLetter(email: String) {
        mailClient.sendTechnobattleProgrammLetter(email)
    }

    override fun sendTechnobattleManagerLetterAboutPromoCodesIsGoingToEnd(freeLitresPromoCodesCount: Int) {
        mailClient.sendTechnobattleManagerLetterAboutPromoCodesIsGoingToEnd(freeLitresPromoCodesCount)
    }

    override fun sendPromoCodeLitresLetter(email: String, promoCode: String) {
        mailClient.sendLitresPromoCodeLetter(email, promoCode)
    }

    override fun sendTechnobattleRegistration(request: TechnobattleRegistration) {
        try {
            mailClient.sendTechnobattleRegistrationManager(request.email!!, request)
        } catch (ex: Exception) {
            log.error("Exception on sendTechnobattleRegistration", ex)
        }

    }


    override fun sendSchoolSoloRegistration(email: String, seminar: Seminar) {
        try {
            val seminarTag = campaignTagDAO.select(seminar.tagId!!)
            mailClient.sendSchoolSoloRegistration(email, seminar, seminarTag, getMailThumbnaiUrl(seminar))
        } catch (ex: Exception) {
            log.warn("Exception on e-mail send", ex)
        }

    }

    override fun sendCharityEventOrder(email: String, charityEventOrder: CharityEventOrder) {
        try {
            val manageEmail = "alex_planeta@planeta.ru"
            log.warn("Sending message to $email")
            mailClient.sendCharityEventOrder(email, charityEventOrder)
            log.warn("Sending message to $manageEmail")
            mailClient.sendCharityEventOrder(manageEmail, charityEventOrder)
        } catch (ex: Exception) {
            log.warn("Exception on e-mail send", ex)
        }

    }

    override fun sendSchoolPairRegistration(email: String, email2: String, seminar: Seminar) {
        try {
            val seminarTag = campaignTagDAO.select(seminar.tagId!!)
            val seminarImageUrl = getMailThumbnaiUrl(seminar)
            mailClient.sendSchoolSoloRegistration(email, seminar, seminarTag, seminarImageUrl)
            mailClient.sendSchoolSoloRegistration(email2, seminar, seminarTag, seminarImageUrl)
        } catch (ex: Exception) {
            log.warn("Exception on e-mail send", ex)
        }

    }

    override fun sendSchoolCompanyRegistration(seminarRegistration: SeminarRegistrationCompany, seminar: Seminar) {
        try {
            if (seminarRegistration.seminarFormat == true) {
                mailClient.sendSchoolCompanyRegistration(seminarRegistration.email!!, seminar, getMailThumbnaiUrl(seminar))
            } else {
                mailClient.sendSchoolCompanyOfflineRegistration(seminarRegistration.email!!, seminar, getMailThumbnaiUrl(seminar))
            }
        } catch (ex: Exception) {
            log.warn("Exception on e-mail send", ex)
        }

    }


    override fun sendInvestingInvoiceCreatedNotification(investingOrderInfo: InvestingOrderInfo) {
        try {
            val order = orderService.getOrderSafe(investingOrderInfo.investingOrderInfoId!!)
            val profile = profileService.getProfileSafe(order.buyerId)
            val campaign = orderService.getCampaignByOrderId(investingOrderInfo.investingOrderInfoId!!)
            val email = getUserEmailSafe(profile)
            mailClient.sendInvestingInvoiceCreated(email!!, profile.displayName!!, projectService!!.getCampaignUrl(campaign),
                    campaign.name!!, projectService.getInvoicePrintUrl(order.topayTransactionId), order.topayTransactionId)
        } catch (e: Exception) {
            log.error(e)
        }

    }

    override fun sendInvestingInvoiceRequestNotification(investingOrderInfo: InvestingOrderInfo) {
        try {
            val order = orderService.getOrderSafe(investingOrderInfo.investingOrderInfoId!!)
            val profile = profileService.getProfileSafe(order.buyerId)
            val email = getUserEmailSafe(profile)
            if (order.orderType === OrderObjectType.BIBLIO) {
                mailClient.sendBiblioInvoiceCreated(email!!, profile.displayName!!, projectService!!.getInvoicePrintUrl(order.topayTransactionId), order.topayTransactionId)
                return
            }
            val campaign = orderService.getCampaignByOrderId(investingOrderInfo.investingOrderInfoId ?: 0)
            val investInfo: String
            if (investingOrderInfo.userType === ContractorType.INDIVIDUAL) {
                investInfo = "ФЛ " + investingOrderInfo.lastName + " " + investingOrderInfo.firstName + " " + investingOrderInfo.middleName
            } else {
                investInfo = "ЮЛ " + investingOrderInfo.orgName
            }
            if (investingOrderInfo.moderateStatus === ModerateStatus.NEW) {
                mailClient.sendInvestingInvoiceRequest(email!!, profile.displayName!!, projectService!!.getCampaignUrl(campaign), campaign.name!!)
            } else if (investingOrderInfo.moderateStatus === ModerateStatus.APPROVED) {
                mailClient.sendInvestingInvoiceCreated(email!!, profile.displayName!!, projectService!!.getCampaignUrl(campaign), campaign.name!!, projectService.getInvoicePrintUrl(order.topayTransactionId), order.topayTransactionId)
            }
            mailClient.sendInvestingInvoiceRequestManager(investingManagerEmail!!, projectService!!.getUserUrl(profile), profile.displayName!!, projectService.getCampaignUrl(campaign), campaign.name!!, investInfo, projectService.getInvestingOrderURL(investingOrderInfo))
        } catch (e: Exception) {
            log.error(e)
        }

    }


    override fun sendPaymentInvalidStateNotification(processorName: String, transaction: TopayTransaction, error: Boolean) {
        if (error) {
            mailClient.sendPaymentInvalidStateError(processorName, transaction)
        } else {
            mailClient.sendPaymentInvalidStateReject(processorName, transaction)
        }
    }

    @Throws(NotFoundException::class)
    override fun sendProductNewComment(comment: Comment) {
        val product = productDAO.selectProduct(comment.objectId!!)
                ?: throw NotFoundException(Product::class.java, comment.objectId)

        mailClient.sendProductComment(shopEmail!!, product, comment)
    }

    private fun executeTask(task: Runnable) {
        if (isAsynchronous) {
            executorService.execute(task)
        } else {
            task.run()
        }
    }

    override fun sendQuickstartGetBook(email: String, type: String) {
        try {
            mailClient.sendQuickstartGetBookEmail(email, type)
        } catch (ex: Exception) {
            log.warn("Exception on e-mail send", ex)
        }

    }

    private fun sendCampaignNewsEmail(emailSubject: String, userPrivateInfo: UserPrivateInfo, commonParams: Map<String, Any>) {

        val params = HashMap(commonParams)
        val emailTo = userPrivateInfo.email
        params["mailTo"] = emailTo
        params["unsubscribeUrl"] = authorNotificationService.getUnsubscribeUrl(userPrivateInfo.userId)

        val emailMessage = notificationTemplateService.applyRichTemplate(params)

        if (StringUtils.isNotBlank(emailMessage)) {
            mailClient.sendMessageRich(NotificationServiceImpl.RichTemplateType.NEW_BLOG_POST_IN_CAMPAIGN.toString(), emailTo!!,
                    noreplyEmail!!, emailSubject, emailMessage, MailMessagePriority.LOW)
        }
    }

    internal fun formatPostForEmail(post: Post, blogPostUrl: String): String {
        var blogPostTextHtml = post.postText
        blogPostTextHtml = Utils.replaceAwayUrlsToMailUrls(blogPostTextHtml
                ?: "", projectService!!.getUrl(ProjectType.MAIN))
        blogPostTextHtml = getHtmlWithReplacedMediaRichTags(blogPostTextHtml, blogPostUrl)
        try {

            val blogPostTextHtmlOuter = getInlineStyleHtmlTextFromOuterService(blogPostTextHtml)
            if (blogPostTextHtmlOuter != null) {
                blogPostTextHtml = blogPostTextHtmlOuter
            }
        } catch (e: IOException) {
            log.error(e)
        }

        blogPostTextHtml = PremailerResponse.unEscapedAmpersands(blogPostTextHtml ?: "")
        return blogPostTextHtml
    }

    private fun getHtmlWithReplacedMediaRichTags(htmlWithRichTags: String, blogPostUrl: String): String {
        return getHtmlWithReplacedMediaRichTags(htmlWithRichTags, blogPostUrl, WebUtils.appendProtocolIfNecessary(staticNodesService.staticNode, true))
    }

    @Throws(IOException::class)
    private fun getInlineStyleHtmlTextFromOuterService(blogPostTextHtml: String?): String? {
        val params = HashMap<String, Any>()
        params["noInlineStyleHtml"] = blogPostTextHtml ?: ""
        params["templateName"] = RichTemplateType.PREMAILER_TRANSFORM.toString()

        val toPremailerHtml = notificationTemplateService.applyRichTemplate(params)

        val requestMap = HashMap<String, String>()
        requestMap["html"] = toPremailerHtml

        val response = WebUtils.uploadMapExternal("http://premailer.dialect.ca/api/0.1/documents", requestMap)
                ?: return null

        var premailerResponse: PremailerResponse? = null
        try {
            premailerResponse = ObjectMapper().readValue<PremailerResponse>(response, object : TypeReference<PremailerResponse>() {

            })
        } catch (e: Exception) {
            log.error(e)
        }

        if (premailerResponse == null || premailerResponse.status != HttpServletResponse.SC_CREATED) {
            return null
        }

        var inlineStyledHtml = IOUtils.toString(URL(premailerResponse?.documents?.get("html")))
        val tagName = "body"
        val firstIndex = inlineStyledHtml.indexOf("<$tagName>")
        val lastIndex = inlineStyledHtml.lastIndexOf("</$tagName>")
        inlineStyledHtml = inlineStyledHtml.substring(firstIndex + tagName.length + 2, lastIndex)

        return inlineStyledHtml
    }

    override fun sendBiblioPurchaseNotification(libraries: List<Library>, bookOrders: Collection<BookOrder>, fio: String, buyerEmail: String) {
        val date = Date()

        for (library in libraries) {
            sendBiblioPurchaseLibraryNotification(library, bookOrders, fio, date)
        }

        for (bookOrder in bookOrders) {
            sendBiblioPurchaseBookNotification(bookOrder, libraries, fio, date, buyerEmail, bookOrder.book?.email)
        }
    }

    private fun sendBiblioPurchaseLibraryNotification(library: Library, bookOrders: Collection<BookOrder>, buyerName: String, date: Date) {
        try {
            val params = HashMap<String, Any>()

            val emailTo = library.email
            if (StringUtils.isEmpty(emailTo)) {
                log.warn("Empty emailTo for library purchase notification: libraryId = " + library.libraryId)
            }

            params["mailTo"] = emailTo as Any
            params["library"] = library
            params["bookOrders"] = bookOrders
            params["date"] = date
            params["buyerName"] = buyerName

            val emailTitle = getMessage("biblio.purchase.library.notification")
            val emailBody = notificationTemplateService.applyCustomTemplate(BIBIIO_PURCHASE_LIBRARY_NOTIFICATION, params)
            if (Utils.empty(emailBody)) {
                log.warn("Empty email body for library purchase notification: libraryId = " + library.libraryId)
            }

            mailClient.sendMessageRich(BIBIIO_PURCHASE_LIBRARY_NOTIFICATION, emailTo!!, supportEmail!!, emailTitle, emailBody, MailMessagePriority.DEFAULT)
        } catch (ex: Exception) {
            log.warn("Exception on rich e-mail send", ex)
        }

    }


    override fun sendBiblioLibraryActiveNotification(library: Library): Boolean {
        try {
            val emailTo = library.email
            if (StringUtils.isEmpty(emailTo)) {
                log.warn("Empty emailTo for library active notification: libraryId = " + library.libraryId)
                return false
            }

            mailClient.sendBiblioLibraryActive(emailTo!!, Date())
        } catch (ex: Exception) {
            log.warn("Exception on rich e-mail send", ex)
            return false
        }

        return true
    }


    override fun sendBiblioPurchaseBookNotification(bookOrder: BookOrder, libraries: List<Library>, buyerName: String,
                                                    date: Date, buyerEmail: String, emailTo: String?): Boolean {
        try {

            val params = HashMap<String, Any>()

            if (StringUtils.isEmpty(emailTo)) {
                log.warn("Empty emailTo for book Purchase notification: libraryId = " + bookOrder.book?.bookId)
                return false
            }

            params["mailTo"] = emailTo as Any
            params["libraries"] = libraries
            params["bookOrder"] = bookOrder
            params["date"] = date
            params["buyerName"] = buyerName
            params["buyerEmail"] = buyerEmail

            val emailTitle = getMessage("biblio.purchase.book.notification")
            val emailBody: String

            val bookTags = bookService.getBookTagsByBookId(bookOrder.book!!.bookId)
            var isBookContainRussianPostTag = false
            for (bookTag in bookTags) {
                if (BookTag.RUSSIANPOST == bookTag.mnemonicName) {
                    isBookContainRussianPostTag = true
                    break
                }
            }
            if (isBookContainRussianPostTag) {
                emailBody = notificationTemplateService.applyCustomTemplate(BIBIIO_PURCHASE_RUSSIANPOST_BOOK_NOTIFICATION, params)
            } else {
                emailBody = notificationTemplateService.applyCustomTemplate(BIBIIO_PURCHASE_BOOK_NOTIFICATION, params)
            }

            if (Utils.empty(emailBody)) {
                log.warn("Empty email body for library purchase notification: bookId = " + bookOrder.book?.bookId)
                return false
            }

            mailClient.sendMessageRich(BIBIIO_PURCHASE_BOOK_NOTIFICATION, emailTo!!, supportEmail!!, emailTitle, emailBody, MailMessagePriority.HIGH)
        } catch (ex: Exception) {
            log.warn("Exception on rich e-mail send", ex)
            return false
        }

        return true
    }


    private fun createCertImageUrl(userId: Long?): String {
        return WebUtils.appendProtocolIfNecessary("$widgetsHost/image?type=SCHOOL&profileId=$userId", true)
    }

    private fun createCertShareUrl(userId: Long?): String {
        return WebUtils.appendProtocolIfNecessary("$applicationHost/welcome/school-cert.html?profileId=$userId", true)
    }

    @Throws(UnsupportedEncodingException::class)
    override fun sendInteractiveCampaignFinished(userId: Long?, emailTo: String, userName: String) {
        if (userId == null) {
            log.error("sendInteractiveCampaignFinished: userId is null")
            return
        }

        val objectMap = HashMap<String, Any>()
        objectMap["userName"] = userName
        objectMap["mailTo"] = emailTo
        objectMap["certUrl"] = createCertImageUrl(userId)
        objectMap["shareUrl"] = URLEncoder.encode(createCertShareUrl(userId), "UTF-8")
        objectMap["shareImageUrl"] = URLEncoder.encode(createCertImageUrl(userId), "UTF-8")

        val template = notificationTemplateService.applyCustomTemplate(INTERACTIVE_CAMPAIGN_FINISHED, objectMap)
        mailClient.sendMessageRich(
                INTERACTIVE_CAMPAIGN_FINISHED,
                emailTo,
                "no-reply@planeta.ru",
                messageSource.getMessage("online.school.finished.subject.email", null, Locale.getDefault()),
                template,
                MailMessagePriority.HIGH)

    }

    @Throws(NotFoundException::class, PermissionException::class)
    override fun sendAuthorCampaignNotifications(profileId: Long, noReplyEmail: String, maxCampaignCount: Int, emailTo: String, date: Date) {
        getProfileForEmail(profileId)

        val campaigns = campaignService.selectCampaignsWithEventsForDayByProfileId(profileId, 0, maxCampaignCount, date)
        val statsList = ArrayList<CampaignStat>(campaigns.size)

        for (campaign in campaigns) {
            val stats = campaignService.selectAggregationStatsForDayByCampaignId(campaign.campaignId, date)
            stats.campaign = campaign
            stats.campaignComments = campaignService.selectCampaignCommentsCountByDate(date, ObjectType.CAMPAIGN, campaign.campaignId)
            stats.newsComments = campaignService.selectCampaignCommentsCountByDate(date, ObjectType.POST, campaign.campaignId)
            stats.shares = shareDAO.selectSharesPurchasedInfoOnDateByCampaignId(date, campaign.campaignId)

            stats.statisticUrl = projectService.getUrl(ProjectType.MAIN, "/campaigns/" + campaign.campaignId + "/orders")
            stats.campaignUrl = projectService.getUrl(ProjectType.MAIN, "/campaigns/" + campaign.campaignId)

            statsList.add(stats)
        }

        val objectMap = HashMap<String, Any>()
        objectMap["statsList"] = statsList
        objectMap["mailTo"] = emailTo
        objectMap["date"] = date
        val template = notificationTemplateService.applyCustomTemplate(AUTHOR_NOTIFICATION_TEMPLATE_FILE_NEW, objectMap)

        log.debug("Trying send email notification: profileId - $profileId")
        mailClient.sendMessageRich("author_notification_template", emailTo, noReplyEmail, messageSource!!.getMessage("new.events.in.your.projects", null, Locale.getDefault()), template, MailMessagePriority.LOW)
    }

    @Throws(NotFoundException::class, PermissionException::class)
    override fun sendCampaignCampaignEndsSoonNotification(campaignSubscribe: CampaignSubscribe) {
        val profile = getProfileForEmail(campaignSubscribe.profileId)
        val email = getUserEmailSafe(profile)

        val campaign = campaignService.getCampaign(campaignSubscribe.campaignId)
                ?: throw NotFoundException(Campaign::class.java, campaignSubscribe.campaignId)

        val objectMap = HashMap<String, Any>()
        objectMap["mailTo"] = email as Any
        objectMap["campaignId"] = campaign.campaignId
        objectMap["campaignName"] = campaign.name as Any
        val template = notificationTemplateService.applyCustomTemplate(CAMPAIGN_ENDS_SOON, objectMap)

        mailClient.sendMessageRich(CAMPAIGN_ENDS_SOON, email!!, noreplyEmail!!,
                messageSource.getMessage("campaign.ends.soon", arrayOf<Any>(campaign.name!!), Locale.getDefault()),
                template, MailMessagePriority.LOW)
    }

    private fun getMailThumbnaiUrl(seminar: Seminar): String {
        return getMailThumbnaiUrl(seminar.getBackgroundImage())
    }

    private fun getMailThumbnaiUrl(url: String?): String {
        return ImageHelperFunctions.getThumbnailUrlStatic(url, ThumbnailType.HUGE, ImageType.PHOTO, staticNodesService)
    }

    @Throws(NotFoundException::class)
    override fun campaignCreated(clientId: Long, campaign: Campaign) {
        val profile = profileService.getProfileSafe(campaign.creatorProfileId)

        val campaignManager = planetaManagersService.getCampaignManager(campaign.campaignId)
        if (profile.profileType === ProfileType.USER) {
            val clientPrivateInfo = userPrivateInfoDAO.selectByUserId(campaign.creatorProfileId)
            if (clientPrivateInfo != null) {
                mailClient.sendToManagerGroupCreatedCampaign(clientPrivateInfo.email!!, profile, campaign, campaignManager)
                mailClient.sendToUserCreatedGroupCampaign(clientPrivateInfo.email!!, profile, campaign,
                        profileService.getProfileSafe(clientId), getCampaignImage(campaign), campaignManager)
            } else {
                log.error("No private info " + campaign.creatorProfileId)
            }
        } else if (profile.profileType === ProfileType.GROUP) {
            mailClient.sendToManagerGroupCreatedCampaign("", profile, campaign, campaignManager)
        }
    }

    private fun getCampaignImage(campaign: Campaign): String {
        return ImageHelperFunctions.getProxyThumbnailUrlStatic(staticNodesService, campaign.imageUrl, ThumbnailType.ORIGINAL,
                ImageType.PHOTO, 218, 133, false, true)
    }

    @Throws(NotFoundException::class)
    override fun sendEmailCampaignInStatusPatchToAdmins(campaign: Campaign) {
        val profile = profileService.getProfileSafe(campaign.creatorProfileId)
        val adminList = profileSubscriptionService.getAdminList(profile.profileId)
        if (profile.profileType === ProfileType.USER) {
            adminList.add(profile)
        }
        val campaignImageUrl = getCampaignImage(campaign)
        val campaignManager = planetaManagersService.getCampaignManager(campaign.campaignId)
        for (admin in adminList) {
            val clientPrivateInfo = userPrivateInfoDAO.selectByUserId(admin.profileId)
            if (clientPrivateInfo != null) {
                mailClient.sendToUserTransferredToPatch(clientPrivateInfo.email!!, profile, campaign, admin, campaignImageUrl, campaignManager)
            }

        }
    }

    override fun sendPromoEmail(email: String, template: String) {
        mailClient.sendPromoEmail(email, template)
    }

    override fun sendPromoEmail(email: String, template: String, params: Map<String, String>) {
        mailClient.sendPromoEmail(email, template, params)
    }

    /**
     * Отправляет письма в саппорт и пользователю, при запросе на аннулирование заказа из личного кабинета пользователем
     * @param orderId идентификатор заказа
     * @param clientId идентификатор профиля пользователя
     * @throws NotFoundException не найден объект по идентификатору
     * @throws PermissionException не хватает прав доступа
     */
    @Throws(Exception::class)
    override fun sendOrderCancelRequestEmails(orderId: Long, clientId: Long) {
        val orderInfo = orderService.getOrderInfo(clientId, orderId)
        val campaign = campaignService.getCampaign(orderInfo.orderObjectCampaignId)
        val clientPrivateInfo = userPrivateInfoDAO.selectByUserId(clientId)

        if (orderInfo == null) {
            throw NotFoundException("Order with id " + orderId + "wasn't found.")
        }

        if (campaign == null) {
            throw NotFoundException("Campaign with id " + orderInfo.orderObjectCampaignId + "wasn't found.")
        }

        if (clientPrivateInfo == null) {
            throw NotFoundException("Profile with id " + clientId + "wasn't found.")
        }

        if (orderInfo.buyerId != clientId) {
            throw Exception("Client ID doesn't match the buyer ID!")
        }

        mailClient.sendCancelPurchaseEmailForSupport(clientPrivateInfo.email!!, campaign.name!!, orderId)
        mailClient.sendCancelPurchaseEmailForUser(clientPrivateInfo.email!!, orderId)
    }

    companion object {

        private val log = Logger.getLogger(NotificationServiceImpl::class.java)

        const val SHARE_PURCHASE_EMAIL_TEMPLATE = "share-purchase-email-templates.ftl"
        const val USER_BECAME_VIP = "user-became-vip.ftl"
        const val CAMPAIGN_BECAME_VIP_OFFERS_ALLOWED = "campaign-became-vip-offers-allowed.ftl"
        const val ORDER_SENT_TEMPLATE = "order-sent-template.ftl"
        const val ORDER_GIVEN_TEMPLATE = "order-given-template.ftl"
        const val ORDER_PURCHASED_TEMPLATE = "order-purchased-template.ftl"
        const val BIBLIO_ORDER_PURCHASED_TEMPLATE = "biblio-order-purchased-template.ftl"
        const val SECRET_KEY = "CFE628B652611CB8"
        const val BIBIIO_PURCHASE_LIBRARY_NOTIFICATION = "biblio-library-purchase-autoletter.ftl"
        const val BIBIIO_PURCHASE_BOOK_NOTIFICATION = "biblio-book-purchase-autoletter.ftl"
        const val BIBIIO_PURCHASE_RUSSIANPOST_BOOK_NOTIFICATION = "biblio-russianpost-book-purchase-autoletter.ftl"
        const val AUTHOR_NOTIFICATION_TEMPLATE_FILE_NEW = "author-campaign-notification-new.ftl"
        const val CAMPAIGN_ENDS_SOON = "campaign-ends-soon.ftl"
        const val INTERACTIVE_CAMPAIGN_FINISHED = "interactive-campaign-finished.ftl"
        const val ERRCOUNT = 200 * 1000

        fun getHtmlWithReplacedMediaRichTags(htmlWithRichTags: String, blogPostUrl: String, staticNode: String): String {
            var htmlWithRichTags = htmlWithRichTags
            htmlWithRichTags = ParsingPhotoElement().replace(htmlWithRichTags, blogPostUrl, staticNode)
            htmlWithRichTags = ParsingVideoElement().replace(htmlWithRichTags, blogPostUrl, staticNode)
            htmlWithRichTags = ParsingAudioElement().replace(htmlWithRichTags, blogPostUrl, staticNode)
            return htmlWithRichTags
        }

        private fun humanNumber(value: BigDecimal): String {
            return ru.planeta.commons.lang.StringUtils.humanNumber(value.toLong().toString())
        }
    }
}


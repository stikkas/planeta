package ru.planeta.api.service.billing.order

import ru.planeta.api.exceptions.NotFoundException
import ru.planeta.api.exceptions.PermissionException
import ru.planeta.model.common.CreditTransaction
import ru.planeta.model.common.DebitTransaction
import ru.planeta.model.common.MoneyTransaction
import ru.planeta.model.enums.MoneyTransactionType
import ru.planeta.model.enums.OrderObjectType

/**
 * Created by eshevchenko.
 */
interface MoneyTransactionService {

    fun getDebitTransaction(transactionId: Long): DebitTransaction?

    @Throws(NotFoundException::class)
    fun getDebitTransactionSafe(transactionId: Long): DebitTransaction

    fun getCreditTransaction(transactionId: Long): CreditTransaction?

    @Throws(NotFoundException::class)
    fun getCreditTransactionSafe(transactionId: Long): CreditTransaction

    @Throws(PermissionException::class)
    fun getTransactions(clientId: Long, profileId: Long, transactionType: MoneyTransactionType?, orderObjectType: OrderObjectType?,
                        offset: Int, limit: Int): List<MoneyTransaction>

}

package ru.planeta.api.service.profile

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import ru.planeta.api.BaseService
import ru.planeta.api.exceptions.MessageCode
import ru.planeta.api.exceptions.NotFoundException
import ru.planeta.api.exceptions.PermissionException
import ru.planeta.api.service.configurations.ConfigurationService
import ru.planeta.api.service.notifications.NotificationService
import ru.planeta.dao.profiledb.VipClubDAO
import ru.planeta.model.enums.ProfileStatus
import java.util.*

/**
 *
 * Created by a.savanovich on 05.08.2015.
 */
@Service
class VipClubServiceImpl(private var vipClubDAO: VipClubDAO, private val configurationService: ConfigurationService,
                         private var notificationService: NotificationService) : BaseService(), VipClubService {

    @Throws(PermissionException::class, NotFoundException::class)
    override fun changeUserVipStatus(clientId: Long, userId: Long, isVip: Boolean) {
        if (!permissionService.hasAdministrativeRole(clientId)) {
            throw PermissionException(MessageCode.USER_NOT_ADMIN)
        }

        changeUserVipStatus(userId, isVip)
    }


    @Throws(NotFoundException::class)
    override fun changeUserVipStatus(userId: Long, isVip: Boolean): Boolean {
        val profile = profileDAO.selectById(userId) ?: return false
        if (profile.isVip xor isVip) {
            profile.isVip = isVip
            profile.vipObtainDate = Date()
            profileDAO.update(profile)

            if (isVip && configurationService.needNotifyVipClubMember() && profile.status !== ProfileStatus.USER_BOUNCED) {
                notificationService.sendUserBecameVipNotification(profile)
            }
            return true
        }
        return false
    }

    override fun getAllNewVips(offset: Int, limit: Int): List<Long> {
        return vipClubDAO.getVipIds(offset, limit)
    }
}

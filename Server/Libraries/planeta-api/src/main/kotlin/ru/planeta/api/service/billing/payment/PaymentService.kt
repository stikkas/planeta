package ru.planeta.api.service.billing.payment

import ru.planeta.api.exceptions.NotFoundException
import ru.planeta.api.exceptions.PaymentException
import ru.planeta.api.exceptions.PermissionException
import ru.planeta.model.common.Order
import ru.planeta.model.common.PaymentMethod
import ru.planeta.model.common.PaymentProvider
import ru.planeta.model.common.TopayTransaction
import ru.planeta.model.enums.ProjectType
import ru.planeta.model.enums.TopayTransactionStatus
import java.math.BigDecimal
import java.util.Date

/**
 * Payments management service.<br></br>
 * Provides base methods for working with payment transactions.
 */
interface PaymentService {

    @Throws(PaymentException::class)
    fun createInternalPayment(profileId: Long, amount: BigDecimal, paymentMethod: PaymentMethod): TopayTransaction

    /**
     * Creates payment transaction [TopayTransaction] for profile with specified amount.<br></br>
     * Resolves payment system type by `paymentType` and `projectType`, and save it in payment transaction.
     *
     *
     *
     * @param profileId profile identifier;
     * @param amount    amount;
     * @param paymentMethodId
     * @param phone
     * @return created payment transaction.
     * @throws Exception
     */
    @Throws(PaymentException::class)
    fun createPayment(profileId: Long, amount: BigDecimal, paymentMethodId: Long, projectType: ProjectType, phone: String): TopayTransaction

    /**
     * Creates payment transaction, and associates it with presented order [Order].
     *
     *
     * @param order       assigned order;
     * @param amountToPay amount to pay
     * @param paymentMethodId
     * @param projectType payment source project type;  @return payment transaction assigned with order.
     * @param phone
     * @param tlsSupported
     * @throws PaymentException
     */
    @Throws(PaymentException::class)
    fun createOrderPayment(order: Order, amountToPay: BigDecimal, paymentMethodId: Long, projectType: ProjectType, phone: String?, tlsSupported: Boolean): TopayTransaction


    /**
     * Creates new payment transaction with the same parameters `profileId` and `amountNet` as the pattern transaction.<br></br>
     * If pattern payment transaction was associated with order, then order reassign with new transaction.
     *
     *
     *
     * @param transactionId pattern payment transaction identifier
     * @param paymentMethodId
     * @param phone
     * @return created transaction.
     */
    @Throws(NotFoundException::class, PaymentException::class, PermissionException::class)
    fun createPaymentFrom(transactionId: Long, paymentMethodId: Long, phone: String): TopayTransaction

    @Throws(PermissionException::class)
    fun getPayments(clientId: Long, query: String, status: TopayTransactionStatus, paymentMethodId: Long?, paymentProviderId: Long?, projectType: ProjectType, dateFrom: Date, dateTo: Date, orderOrTransactionId: Long, offset: Int, limit: Int): List<TopayTransaction>

    /**
     * Gets payment transaction by identifier.
     *
     * @param transactionId payment transaction identifier;
     * @return [TopayTransaction] instance or `null` if transaction with presented identifier not exists.
     */
    fun getPayment(transactionId: Long?): TopayTransaction

    /**
     * Updates payment transaction's status.<br></br>
     * If transaction's status already is set to `status`, then transaction not updated.
     *
     * @param transaction payment transaction;
     * @param status      new payment transaction status;
     */
    fun updatePaymentStatus(transaction: TopayTransaction, status: TopayTransactionStatus)

    @Throws(PermissionException::class)
    fun getOrderPayments(clientId: Long, orderId: Long): List<TopayTransaction>

    @Throws(PaymentException::class)
    fun processPayment(transaction: TopayTransaction, externalSystemData: String): Boolean

    fun getPaymentMethod(paymentMethodId: Long): PaymentMethod

    fun getPaymentProvider(paymentProviderId: Long): PaymentProvider

}

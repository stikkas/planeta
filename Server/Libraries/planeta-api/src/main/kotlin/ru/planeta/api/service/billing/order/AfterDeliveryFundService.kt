package ru.planeta.api.service.billing.order

import org.springframework.stereotype.Service
import ru.planeta.api.exceptions.NotFoundException
import ru.planeta.api.exceptions.OrderException
import ru.planeta.api.exceptions.PermissionException
import ru.planeta.model.common.Order
import ru.planeta.model.common.OrderObject
import ru.planeta.model.enums.OrderObjectType

/**
 * Created by a.savanovich on 18.10.2016.
 */
@Service
class AfterDeliveryFundService : AfterFundService {
    @Throws(NotFoundException::class, PermissionException::class, OrderException::class)
    override fun purchase(order: Order, orderObjects: List<OrderObject>) {
        // do nothing
    }

    override fun isMyType(type: OrderObjectType): Boolean {
        return OrderObjectType.DELIVERY === type
    }
}

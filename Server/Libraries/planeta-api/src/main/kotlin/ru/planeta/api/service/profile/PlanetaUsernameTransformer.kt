package ru.planeta.api.service.profile

import org.apache.commons.lang3.StringUtils
import org.springframework.stereotype.Component
import ru.planeta.api.exceptions.MessageCode

@Component
class PlanetaUsernameTransformer : UsernameTransformer {

    @Throws(IllegalArgumentException::class)
    override fun normalize(username: String): String {
        if (!isValidUsername(username)) {
            throw IllegalArgumentException(MessageCode.WRONG_EMAIL.errorPropertyName)
        }
        return normalizeInner(username)
    }

    private fun normalizeInner(username: String): String {
        return username.trim { it <= ' ' }.toLowerCase()
    }


    private fun isValidUsername(username: String): Boolean {
        return StringUtils.isNotBlank(username)
    }

    override fun usernameEquals(username: String, otherUsername: String): Boolean {
        return (isValidUsername(username)
                && isValidUsername(otherUsername)
                && normalizeInner(username) == normalizeInner(otherUsername))
    }
}

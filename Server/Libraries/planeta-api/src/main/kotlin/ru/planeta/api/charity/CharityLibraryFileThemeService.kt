package ru.planeta.api.charity

import ru.planeta.model.charitydb.LibraryFileTheme
import ru.planeta.model.charitydb.LibraryFileThemeExample

interface CharityLibraryFileThemeService {
    val orderByClause: String
    fun insertLibraryFileTheme(libraryFileTheme: LibraryFileTheme)
    fun insertLibraryFileThemeAllFields(libraryFileTheme: LibraryFileTheme)
    fun updateLibraryFileTheme(libraryFileTheme: LibraryFileTheme)
    fun updateLibraryFileThemeAllFields(libraryFileTheme: LibraryFileTheme)
    fun insertOrUpdateLibraryFileTheme(libraryFileTheme: LibraryFileTheme)
    fun insertOrUpdateLibraryFileThemeAllFields(libraryFileTheme: LibraryFileTheme)
    fun deleteLibraryFileTheme(id: Long?)
    fun selectLibraryFileTheme(themeId: Long?): LibraryFileTheme
    fun getLibraryFileThemeExample(offset: Int, limit: Int): LibraryFileThemeExample
    fun selectLibraryFileThemeCount(): Int
    fun selectLibraryFileThemeList(offset: Int, limit: Int): List<LibraryFileTheme>
}


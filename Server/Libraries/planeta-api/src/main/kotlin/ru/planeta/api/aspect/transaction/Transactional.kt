package ru.planeta.api.aspect.transaction

import java.lang.annotation.Retention
import java.lang.annotation.RetentionPolicy

/**
 * @author p.vyazankin
 * @since 2/18/13 8:05 PM
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(AnnotationTarget.CLASS, AnnotationTarget.FILE, AnnotationTarget.FUNCTION, AnnotationTarget.PROPERTY_GETTER, AnnotationTarget.PROPERTY_SETTER)
annotation class Transactional

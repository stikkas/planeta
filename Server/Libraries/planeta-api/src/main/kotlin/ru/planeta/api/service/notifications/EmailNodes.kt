package ru.planeta.api.service.notifications

/**
 * @author Andrew.Arefyev@gmail.com
 * 09.01.14 21:09
 */
enum class EmailNodes {
    SUBJECT, BODY, EMAIL_FROM, EMAIL_TO
}

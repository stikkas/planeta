package ru.planeta.api.service.profile

import org.apache.commons.lang3.time.DateUtils
import org.springframework.stereotype.Service
import ru.planeta.dao.commondb.QuickstartGetBookMapper
import ru.planeta.model.commondb.QuickstartGetBook
import ru.planeta.model.commondb.QuickstartGetBookExample
import java.util.*


/**
 *
 * Created by kostiagn on 26.08.2015.
 */
@Service
class QuickstartGetBookServiceImpl(private var quickstartGetBookMapper: QuickstartGetBookMapper) : QuickstartGetBookService {

    override fun alreadySendLastDay(email: String, type: String): Boolean {
        val list = selectQuickstartGetBookList(email, type, DateUtils.addDays(Date(), -1), 0, 1)
        return !list.isEmpty();
    }

    fun selectQuickstartGetBookList(email: String, type: String, date: Date, offset: Int, limit: Int): List<QuickstartGetBook> {
        val example = QuickstartGetBookExample()
        example.offset = offset
        example.limit = limit
        example.or().andEmailEqualTo(email).andBookTypeEqualTo(type).andTimeAddedGreaterThanOrEqualTo(date)

        return quickstartGetBookMapper.selectByExample(example)
    }


    override fun insertQuickstartGetBook(quickstartGetBook: QuickstartGetBook) {
        quickstartGetBook.timeAdded = Date()
        quickstartGetBookMapper.insertSelective(quickstartGetBook)
    }

    fun insertQuickstartGetBookAllFields(quickstartGetBook: QuickstartGetBook) {
        quickstartGetBook.timeAdded = Date()
        quickstartGetBookMapper.insert(quickstartGetBook)
    }

    fun updateQuickstartGetBook(quickstartGetBook: QuickstartGetBook) {

        quickstartGetBookMapper.updateByPrimaryKeySelective(quickstartGetBook)
    }

    fun updateQuickstartGetBookAllFields(quickstartGetBook: QuickstartGetBook) {

        quickstartGetBookMapper.updateByPrimaryKey(quickstartGetBook)
    }

    fun insertOrUpdateQuickstartGetBook(quickstartGetBook: QuickstartGetBook) {

        val selectedQuickstartGetBook = quickstartGetBookMapper.selectByPrimaryKey(quickstartGetBook.email ?: "",
                quickstartGetBook.bookType ?: "", quickstartGetBook.timeAdded ?: Date())
        if (selectedQuickstartGetBook == null) {
            quickstartGetBook.timeAdded = Date()
            quickstartGetBookMapper.insertSelective(quickstartGetBook)
        } else {
            quickstartGetBookMapper.updateByPrimaryKeySelective(quickstartGetBook)
        }
    }

    fun insertOrUpdateQuickstartGetBookAllFields(quickstartGetBook: QuickstartGetBook) {

        val selectedQuickstartGetBook = quickstartGetBookMapper.selectByPrimaryKey(quickstartGetBook.email ?: "",
                quickstartGetBook.bookType ?: "", quickstartGetBook.timeAdded ?: Date())
        if (selectedQuickstartGetBook == null) {
            quickstartGetBook.timeAdded = Date()
            quickstartGetBookMapper.insert(quickstartGetBook)
        } else {
            quickstartGetBookMapper.updateByPrimaryKey(quickstartGetBook)
        }
    }

    fun selectQuickstartGetBook(email: String, bookType: String, timeAdded: Date): QuickstartGetBook {
        return quickstartGetBookMapper.selectByPrimaryKey(email, bookType, timeAdded)
    }

    private fun COMMENT_SECTION_QuickstartGetBook() {

    }

    fun getQuickstartGetBookExample(offset: Int, limit: Int): QuickstartGetBookExample {
        val example = QuickstartGetBookExample()
        example.offset = offset
        example.limit = limit
        example.or()
        return example
    }

    fun selectQuickstartGetBookCount(): Int {
        return quickstartGetBookMapper.countByExample(getQuickstartGetBookExample(0, 0))
    }

    fun selectQuickstartGetBookList(offset: Int, limit: Int): List<QuickstartGetBook> {
        return quickstartGetBookMapper.selectByExample(getQuickstartGetBookExample(offset, limit))
    }
}

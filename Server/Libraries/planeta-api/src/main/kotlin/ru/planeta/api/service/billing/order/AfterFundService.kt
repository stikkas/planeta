package ru.planeta.api.service.billing.order

import ru.planeta.api.exceptions.NotFoundException
import ru.planeta.api.exceptions.OrderException
import ru.planeta.api.exceptions.PermissionException
import ru.planeta.model.common.Order
import ru.planeta.model.common.OrderObject
import ru.planeta.model.enums.OrderObjectType

/**
 * Created by a.savanovich on 17.10.2016.
 */
interface AfterFundService {
    @Throws(NotFoundException::class, PermissionException::class, OrderException::class)
    fun purchase(order: Order, orderObjects: List<OrderObject>)

    fun isMyType(type: OrderObjectType): Boolean
}

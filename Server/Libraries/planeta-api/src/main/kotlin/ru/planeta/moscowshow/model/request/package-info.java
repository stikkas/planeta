@XmlSchema(namespace = "http://moscowshow.handydev.com/", elementFormDefault = XmlNsForm.QUALIFIED,
        xmlns = @XmlNs(namespaceURI = "http://moscowshow.handydev.com/", prefix = "mos"))
package ru.planeta.moscowshow.model.request;

import javax.xml.bind.annotation.XmlNs;
import javax.xml.bind.annotation.XmlNsForm;
import javax.xml.bind.annotation.XmlSchema;

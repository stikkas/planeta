package ru.planeta.moscowshow.model.request

import javax.xml.bind.annotation.XmlAccessType
import javax.xml.bind.annotation.XmlAccessorType
import javax.xml.bind.annotation.XmlElementWrapper
import javax.xml.bind.annotation.XmlRootElement
import ru.planeta.moscowshow.model.OrderItem

/**
 *
 * User: Serge Blagodatskih<stikkas17></stikkas17>@gmail.com><br></br>
 * Date: 14.10.16<br></br>
 * Time: 17:16
 */
@XmlRootElement(name = "PlaceOrder")
@XmlAccessorType(XmlAccessType.FIELD)
class RequestPlaceOrder : Request {

    @XmlElementWrapper(name = "orderItems")
    var OrderItemEx: List<OrderItem>? = null


    constructor()

    constructor(orderItems: List<OrderItem>) {
        this.OrderItemEx = orderItems
    }

}

package ru.planeta.api.exceptions

/**
 * Date: 26.10.12
 * Time: 15:11
 */
class AlreadyExistException : Exception {

    constructor() {}

    constructor(s: String) : super(s) {}
}

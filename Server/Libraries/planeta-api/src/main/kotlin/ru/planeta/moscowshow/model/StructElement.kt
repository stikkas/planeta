package ru.planeta.moscowshow.model

import java.math.BigDecimal
import javax.xml.bind.annotation.XmlAccessType
import javax.xml.bind.annotation.XmlAccessorType
import javax.xml.bind.annotation.XmlElement
import javax.xml.bind.annotation.XmlRootElement

/**
 * Описание элемента структуры: ряда или секции
 * User: Serge Blagodatskih<stikkas17></stikkas17>@gmail.com><br></br>
 * Date: 14.10.16<br></br>
 * Time: 14:24
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
class StructElement {

    /**
     * ID в базе данных.
     */
    var id: Long = 0
        internal set

    /**
     * Название секции или ряда.
     */
    var name: String? = null
        internal set

    /**
     * Значения цен мест, доступных для заказа в секции (для ряда список пустой).
     */
    @XmlElement(name = "value")
    var priceValues: List<BigDecimal>? = null
        internal set

    /**
     * Минимальная координата X мест секции.
     */
    var minX: Double = 0.toDouble()
        internal set

    /**
     * Минимальная координата Y мест секции.
     */
    var minY: Double = 0.toDouble()
        internal set

    /**
     * Максимальная координата X мест секции.
     */
    var maxX: Double = 0.toDouble()
        internal set

    /**
     * Максимальная координата Y мест секции.
     */
    var maxY: Double = 0.toDouble()
        internal set

    /**
     * Стоимость места в секции или ряду по умолчанию.
     */
    var price: BigDecimal? = null
        internal set

    /**
     * ID родительского элемента для ряда; для секции — null.
     */
    var parentId: Long = 0
        internal set

    override fun toString(): String {
        return ("StructElement{"
                + "id=" + id + ", name=" + name
                + ", priceValues=" + priceValues
                + ", minX=" + minX + ", minY=" + minY
                + ", maxX=" + maxX + ", maxY=" + maxY
                + ", price=" + price + ", parentId=" + parentId + '}'.toString())
    }

}

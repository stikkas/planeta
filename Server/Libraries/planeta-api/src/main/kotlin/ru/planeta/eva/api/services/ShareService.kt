package ru.planeta.eva.api.services

import org.springframework.stereotype.Service
import ru.planeta.api.service.campaign.DeliveryService
import ru.planeta.api.service.geo.AddressService
import ru.planeta.dao.commondb.PurchaseShareInfoDAO
import ru.planeta.dao.commondb.ShareDAO
import ru.planeta.dto.PurchaseShareInfoDTO
import ru.planeta.dto.SaveShareInfoDTO
import ru.planeta.dto.ShareDTO
import ru.planeta.model.common.delivery.SubjectType
import java.math.BigDecimal

@Service
class ShareService(private val purchaseShareInfoDAO: PurchaseShareInfoDAO,
                   private val deliveryService: DeliveryService,
                   private val addressService: AddressService,
                   private val shareDAO: ShareDAO) {

    fun getPurchaseShareInfo(infoId: Long, myProfileId: Long): PurchaseShareInfoDTO? {
        val purchaseInfo: PurchaseShareInfoDTO? = purchaseShareInfoDAO.findPurchaseShareInfo(infoId)

        if (purchaseInfo != null) {
            val shareDetailed = purchaseInfo.shareDetailed
            if (shareDetailed != null) {
                shareDetailed.linkedDeliveries = deliveryService.getEnabledLinkedDeliveries(shareDetailed.shareId, SubjectType.SHARE)
            }

            if (myProfileId > 0) {
                purchaseInfo.storedAddress = addressService.getLastPostAddress(myProfileId)
            }
        }

        return purchaseInfo
    }

    fun savePurchaseShareInfo(shareInfo: SaveShareInfoDTO): Int {
        return purchaseShareInfoDAO.savePurchaseShareInfo(shareInfo)
    }

    fun checkPurchaseShare(shareInfo: SaveShareInfoDTO): Boolean {
        val share = shareDAO.select(shareInfo.shareId)
        if (share != null) {
            val price = share.price
            if (price != null) {
                val correctPrice = price * BigDecimal(shareInfo.quantity) <= shareInfo.donateAmount
                if (share.amount > 0) {
                    return correctPrice && shareInfo.quantity <= share.amount - share.purchaseCount
                }
                return correctPrice
            }
        }
        return false
    }

    fun shareInfo(shareId: Long): ShareDTO? {
        val share = shareDAO.findShare(shareId)
        if (share != null) {
            share.linkedDeliveries = deliveryService.getEnabledLinkedDeliveries(shareId, SubjectType.SHARE)
        }
        return share
    }
}


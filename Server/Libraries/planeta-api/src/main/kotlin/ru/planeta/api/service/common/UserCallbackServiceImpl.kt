package ru.planeta.api.service.common

import org.apache.commons.collections4.CollectionUtils
import org.apache.log4j.Logger
import org.springframework.stereotype.Service
import ru.planeta.api.BaseService
import ru.planeta.api.exceptions.MessageCode
import ru.planeta.api.exceptions.PermissionException
import ru.planeta.api.model.ConfigurationType
import ru.planeta.api.model.json.UserCallbackInfo
import ru.planeta.api.service.NexmoService
import ru.planeta.api.service.billing.payment.PaymentService
import ru.planeta.api.service.configurations.ConfigurationService
import ru.planeta.api.service.profile.ProfileService
import ru.planeta.dao.commondb.UserCallbackDAO
import ru.planeta.model.common.*
import ru.planeta.model.common.campaign.Share
import ru.planeta.model.enums.OrderObjectType
import ru.planeta.model.profile.Profile
import java.math.BigDecimal
import java.util.*

/**
 *
 * Created by eshevchenko on 02.02.15.
 */
@Service
class UserCallbackServiceImpl(
        private var configurationService: ConfigurationService,
        private var callbackDAO: UserCallbackDAO,
        private var paymentService: PaymentService,
        private var smsClient: NexmoService,
        private var profileService: ProfileService) : BaseService(), UserCallbackService {

    private var receiver: String? = null
    @Synchronized
    private fun getReceiver(): String? {
        if (receiver != null) {
            return receiver
        }
        val conf = configurationService.getConfigurationByKey(ConfigurationType.CALLBACK_RECIEVER_PHONE_KEY)
        if (conf != null) {
            receiver = conf.stringValue
        }

        if (receiver == null) {
            throw RuntimeException("wrong configuration in database")
        }
        return receiver
    }


    override fun createFailurePaymentCallback(userId: Long, userPhone: String, paymentId: Long?, amount: BigDecimal): UserCallback {
        val callback = UserCallback.createFailurePayment(userId, userPhone, paymentId ?: 0, amount)
        return createCallback(callback)
    }

    override fun createOrderingProblemCallback(userId: Long, userPhone: String, orderType: OrderObjectType, objectId: Long?, amount: BigDecimal): UserCallback {
        val callback = UserCallback.createOrderingProblem(userId, userPhone, orderType, objectId ?: 0, amount)
        return createCallback(callback)
    }

    private fun createCallback(callback: UserCallback): UserCallback {
        val id = callbackDAO.insert(callback)
        return callbackDAO.selectById(id)
    }

    @Throws(PermissionException::class)
    override fun processUserCallback(managerId: Long, callbackId: Long): UserCallbackInfo {
        verifyAdminPermission(managerId)
        callbackDAO.updateProcessingStatus(callbackId, managerId)
        return collectCallbacksInfo(listOf(callbackDAO.selectById(callbackId)))[0]
    }

    override fun getCallbacksByIds(callbackIds: List<Long>): List<UserCallbackInfo> {
        if (callbackIds == null || callbackIds.isEmpty()) return ArrayList()
        val callbacks = callbackDAO.selectUserCallbacksByIds(callbackIds)
        return collectCallbacksInfo(callbacks)
    }


    internal fun getSMSTextToManager(phone: String, money: String, orderId: Long?): String {
        val sb = StringBuilder()
        sb.append(messageSource.getMessage("request.for.a.call.back.from", null, Locale.getDefault())).append(" ")
        sb.append(phone)
        sb.append(" ")
        if (orderId != null) {
            sb.append(messageSource.getMessage("append.order", null, Locale.getDefault())).append(" ")
            sb.append(orderId)
            sb.append(" ")
        }
        sb.append(messageSource.getMessage("append.amount", null, Locale.getDefault())).append(" ")
        sb.append(money)
        return sb.toString()
    }

    override fun sendSmsToManager(phone: String, money: String, orderId: Long?) {
        val smsTextToManager = getSMSTextToManager(phone, money, orderId)
        smsClient.send(smsTextToManager, getReceiver())
    }


    @Throws(PermissionException::class)
    override fun getProcessedCallbacks(clientId: Long, type: UserCallback.Type, dateFrom: Date, dateTo: Date, offset: Int, limit: Int): List<UserCallbackInfo> {
        verifyAdminPermission(clientId)
        val callbacks = callbackDAO.selectUserCallbacks(type, true, dateFrom, dateTo, offset, limit)
        return collectCallbacksInfo(callbacks)
    }

    @Throws(PermissionException::class)
    override fun getUnprocessedCallbacks(clientId: Long, type: UserCallback.Type, dateFrom: Date, dateTo: Date, offset: Int, limit: Int): List<UserCallbackInfo> {
        verifyAdminPermission(clientId)
        val callbacks = callbackDAO.selectUserCallbacks(type, false, dateFrom, dateTo, offset, limit)
        return collectCallbacksInfo(callbacks)
    }

    @Throws(PermissionException::class)
    override fun getAllCallbacks(clientId: Long, type: UserCallback.Type, processed: Boolean?, dateFrom: Date, dateTo: Date, offset: Int, limit: Int): List<UserCallbackInfo> {
        verifyAdminPermission(clientId)
        val callbacks = callbackDAO.selectUserCallbacks(type, processed, dateFrom, dateTo, offset, limit)
        return collectCallbacksInfo(callbacks)
    }

    override fun isCallbacksAvailable(amount: BigDecimal): Boolean {
        return configurationService.callbackAvailabilityCondition.check(Calendar.getInstance(), amount)
    }

    @Throws(PermissionException::class)
    override fun getCallback(clientId: Long, callbackId: Long): UserCallback {
        verifyAdminPermission(clientId)
        return callbackDAO.selectById(callbackId)
    }

    @Throws(PermissionException::class)
    override fun getCallbackComments(clientId: Long, callbackId: Long): List<UserCallbackComment> {
        verifyAdminPermission(clientId)
        val list = callbackDAO.selectCallbackComments(callbackId)
        return loadAuthorNamesForCallbackComments(list)
    }

    private fun loadAuthorNamesForCallbackComments(list: List<UserCallbackComment>): List<UserCallbackComment> {
        for (comment in list) {
            comment.authorName = profileDAO.selectById(comment.authorId)?.displayName
        }

        return list
    }

    @Throws(PermissionException::class)
    override fun addCallbackComment(clientId: Long, callbackId: Long, text: String) {
        verifyAdminPermission(clientId)
        val comment = UserCallbackComment()
        comment.authorId = clientId
        comment.callbackId = callbackId
        comment.text = text
        callbackDAO.insertCallbackComment(comment)
    }

    override fun wrapCallback(callback: UserCallback): UserCallbackInfo {
        return collectCallbacksInfo(listOf(callback))[0]
    }

    @Throws(PermissionException::class)
    private fun verifyAdminPermission(managerId: Long) {
        if (!permissionService.hasAdministrativeRole(managerId)) {
            throw PermissionException(MessageCode.USER_NOT_ADMIN)
        }
    }


    private fun collectCallbacksInfo(callbacks: List<UserCallback>): List<UserCallbackInfo> {
        val result = ArrayList<UserCallbackInfo>()
        // collects ids
        val userIds = HashSet<Long>()
        val managerIds = HashSet<Long>()
        val shareIds = HashSet<Long>()
        for (callback in callbacks) {
            if (callback.userId > 0) {
                userIds.add(callback.userId)
            }
            managerIds.add(callback.managerId)

            if (callback.type === UserCallback.Type.ORDERING_PROBLEM) {
                when (callback.orderType) {
                    OrderObjectType.SHARE -> callback.orderObjectId?.let { shareIds.add(it) }
                }
            }
        }
        // selects additional info
        val usersInfo = userPrivateInfoDAO.selectUsersPrivateInfo(userIds)
        val profiles = profileService.getProfiles(CollectionUtils.union(userIds, managerIds))
        val shares = shareDAO.select(shareIds)
        // creates lookup maps
        val userInfoById = HashMap<Long, UserPrivateInfo>(usersInfo.size)
        for (userInfo in usersInfo) {
            userInfoById[userInfo.userId] = userInfo
        }
        val profileById = HashMap<Long, Profile>(profiles.size)
        for (profile in profiles) {
            profileById[profile.profileId] = profile
        }
        val shareById = HashMap<Long, Share>(profiles.size)
        for (share in shares) {
            shareById[share.shareId] = share
        }
        // fills additional info
        for (callback in callbacks) {
            val collected = UserCallbackInfo.createFrom(callback)

            val manager = profileById[callback.managerId]
            if (manager != null) {
                collected.managerName = manager.displayName
            }
            val user = profileById[callback.userId]
            if (user != null) {
                collected.userName = user.displayName
                collected.userEmail = userInfoById[callback.userId]?.email
            }

            if (collected.type === UserCallback.Type.ORDERING_PROBLEM) {
                collected.orderObject = shareById[collected.orderObjectId]
                val share = shareById[collected.orderObjectId] ?: continue
                share.campaignId?.let {
                    campaignDAO.selectCampaignById(it)?.let {
                        collected.campaignName = it.name
                        collected.campaignAlias = it.webCampaignAlias
                    }
                }
            } else {
                val paymentId = collected.paymentId
                val orderId = getOrderIdByPaymentId(paymentId)
                if (orderId == null) {
                    log.error("No orderId in transaction $paymentId")
                    continue
                }
                collected.orderId = orderId
                val c = campaignDAO.selectCampaignByOrder(orderId)
                if (c == null) {
                    log.error("No campaign in transaction $paymentId")
                    continue
                }
                collected.campaignName = c.name
                collected.campaignAlias = c.webCampaignAlias
            }

            result.add(collected)
        }

        return result
    }

    override fun getOrderIdByPaymentId(paymentId: Long?): Long? {
        val transaction = paymentService.getPayment(paymentId)
        return transaction.orderId
    }

    companion object {


        private val log = Logger.getLogger(UserCallbackServiceImpl::class.java)
    }
}

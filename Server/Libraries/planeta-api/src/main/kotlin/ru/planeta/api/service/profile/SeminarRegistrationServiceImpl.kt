package ru.planeta.api.service.profile

import org.springframework.stereotype.Service
import ru.planeta.dao.commondb.BaseSeminarRegistrationService

@Service
class SeminarRegistrationServiceImpl : BaseSeminarRegistrationService(), SeminarRegistrationService

package ru.planeta.api.search

/**
 * This class represents status filter for campaigns
 * User: m.shulepov
 */
enum class CampaignStatusFilter {
    EDITOR_CHOICE,
    ACTIVE,
    NEW,
    FINISHED,
    SUCCESSFUL,
    DISCUSSED,
    CLOSE_TO_FINISH,
    RECORD_HOLDERS,
    LAST_UPDATED,
    CHARITY,
    ACTIVE_AND_FINISHED,
    RECOMMENDED
}

package ru.planeta.eva.api.services

import org.springframework.stereotype.Service
import ru.planeta.dao.maildb.MailUserDAO

@Service
class MailerSubscriptionService(private val mailUserDAO: MailUserDAO) {

    fun subscribeNewUser(email: String) {
        if (mailUserDAO.checkSubscriber(email)) {
            mailUserDAO.forceSubscribeUserByEmail(email)
        } else {
            mailUserDAO.addNewSubscriber(email)
        }
    }
}

package ru.planeta.api.model.json

import ru.planeta.model.common.UserCallback

/**
 *
 * Created by eshevchenko on 06.02.15.
 */
class UserCallbackInfo : UserCallback() {

    var userName: String? = null
    var userEmail: String? = null
    var managerName: String? = null
    var campaignName: String? = null
    var campaignAlias: String? = null
    var orderObject: Any? = null
    var orderId: Long? = null

    companion object {

        fun createFrom(callback: UserCallback): UserCallbackInfo {
            val result = UserCallbackInfo()
            result.userCallbackId = callback.userCallbackId
            result.type = callback.type
            result.userId = callback.userId
            result.userPhone = callback.userPhone
            result.paymentId = callback.paymentId
            result.orderType = callback.orderType
            result.orderObjectId = callback.orderObjectId
            result.amount = callback.amount
            result.managerId = callback.managerId
            result.isProcessed = callback.isProcessed
            result.timeAdded = callback.timeAdded
            result.timeUpdated = callback.timeUpdated
            result.commentsCount = callback.commentsCount

            return result
        }
    }
}

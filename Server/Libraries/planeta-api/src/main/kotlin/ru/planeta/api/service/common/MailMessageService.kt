package ru.planeta.api.service.common

import ru.planeta.api.exceptions.NotFoundException
import ru.planeta.model.mail.MailMessage
import ru.planeta.model.mail.MailMessageStatus

/**
 * Created with IntelliJ IDEA.
 * User: michail
 * Date: 19.05.16
 * Time: 18:57
 */

interface MailMessageService {
    fun insertMailNotification(message: MailMessage): MailMessage

    @Throws(NotFoundException::class)
    fun getMailNotificationSafeById(id: Long): List<MailMessage>

    @Throws(NotFoundException::class)
    fun getMailNotificationSafeByExtMsgId(extMsgId: String): List<MailMessage>

    @Throws(NotFoundException::class)
    fun setExternalMessageId(id: Long, extMsgId: String)

    @Throws(NotFoundException::class)
    fun setStatusByExtMsgId(extMsgId: String, status: MailMessageStatus)

    fun saveOpenedMailInfo(messageId: Long?)
}

package ru.planeta.api.service.billing.order

import org.springframework.beans.factory.annotation.Autowired
import ru.planeta.api.exceptions.MessageCode
import ru.planeta.api.exceptions.NotFoundException
import ru.planeta.api.exceptions.OrderException
import ru.planeta.api.exceptions.PermissionException
import ru.planeta.api.utils.OrderUtils
import ru.planeta.commons.lang.CollectionUtils
import ru.planeta.model.common.Order
import ru.planeta.model.common.OrderObject
import ru.planeta.model.concert.Place
import ru.planeta.model.enums.OrderObjectType
import ru.planeta.model.enums.ProjectType
import ru.planeta.model.shop.enums.PaymentType
import ru.planeta.moscowshow.client.MoscowShowClient

import java.math.BigDecimal
import java.util.ArrayList
import java.util.Date

/**
 * Created by a.savanovich on 17.10.2016.
 */
//@Service
class ConcertAfterFundService : AfterFundService {

    @Autowired
    private val orderService: OrderService? = null

    @Autowired
    private val moscowShowClient: MoscowShowClient? = null

    @Throws(NotFoundException::class, PermissionException::class, OrderException::class)
    override fun purchase(order: Order, orderObjects: List<OrderObject>) {

        val placeIds = CollectionUtils.map(orderObjects) { sourceObject -> sourceObject.objectId }
        if (moscowShowClient!!.payTickets(placeIds).hasErrors()) {
            throw OrderException(MessageCode.PERMISSION_MESSAGE_DENIED)
        }
    }

    override fun isMyType(type: OrderObjectType): Boolean {
        return OrderObjectType.TICKET === type
    }

    @Throws(PermissionException::class, NotFoundException::class, OrderException::class)
    fun createOrder(clientId: Long, places: List<Place>): Order {

        var order = Order()
        val now = Date()

        val orderObjects = ArrayList<OrderObject>()

        order.buyerId = clientId
        order.orderType = OrderObjectType.TICKET
        order.timeAdded = now
        order.timeUpdated = now
        order.paymentType = PaymentType.NOT_SET
        order.projectType = ProjectType.CONCERT

        for (place in places) {
            val orderObject1 = OrderObject()
            orderObject1.merchantId = DEFAULT_MERCHANT_PROFILE_ID
            orderObject1.objectId = place.placeId
            orderObject1.orderObjectType = order.orderType
            orderObject1.price = place.price
            orderObjects.add(orderObject1)
        }

        val objectsPrice = OrderUtils.calculateOrderObjectsPrice(orderObjects)
        order.totalPrice = objectsPrice
        orderService!!.storeToDb(order, orderObjects)
        return order
    }

    companion object {

        private val DEFAULT_MERCHANT_PROFILE_ID: Long = 21765
    }
}

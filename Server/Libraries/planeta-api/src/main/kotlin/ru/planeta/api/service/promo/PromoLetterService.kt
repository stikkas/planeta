package ru.planeta.api.service.promo

import ru.planeta.model.common.campaign.CampaignTag

import java.math.BigDecimal

/**
 * Created with IntelliJ IDEA.
 * User: michail
 * Date: 19.07.2017
 * Time: 15:45
 */
interface PromoLetterService {
    fun sendPromoLetter(email: String, totalPrice: BigDecimal, campaignTags: Collection<CampaignTag>, orderId: Long)
}

package ru.planeta.eva.api.services

import org.springframework.stereotype.Service
import ru.planeta.api.model.ConfigurationType
import ru.planeta.api.model.ShortLinkExtraSocialLink
import ru.planeta.api.service.configurations.ConfigurationService
import ru.planeta.dao.commondb.ShortLinkDAO
import ru.planeta.model.common.ShortLink
import ru.planeta.model.enums.ShortLinkStatus
import java.net.URI
import java.net.URISyntaxException

@Service
class ShortLinkService(private var shortLinkDAO: ShortLinkDAO,
                       private var configurationService: ConfigurationService) {

    fun getExistsSocialShortLinksByBasename(shortlinkBasename: String,
                                            socialShortlinkAdditionsList: List<String>,
                                            offset: Int,
                                            limit: Int): List<ShortLink>? {
        return shortLinkDAO.selectExistsSocialShortLinksByBasename(shortlinkBasename,
                socialShortlinkAdditionsList, offset, limit)
    }

    fun generateMissingSocialLinks(clientId: Long, shortLinkId: Long, socialShortLinkAdditionsList: String) {
        val shortLink = shortLinkDAO.select(shortLinkId) ?: return
        val socialShortLinkSuffixList: List<String>? = this.getSocialShortLinkSuffixList()

        var neededSocialShortLinkList: List<String>? = null
        if (socialShortLinkSuffixList != null) {
            neededSocialShortLinkList = socialShortLinkSuffixList.map { shortLink.shortLink + "-" + it }
        }

        var existsSocialShortLinksByBasename: List<ShortLink>? = null
        if (socialShortLinkSuffixList != null) {
            shortLink.shortLink?.let {
                existsSocialShortLinksByBasename = this.getExistsSocialShortLinksByBasename(it,
                        socialShortLinkSuffixList, 0, 100)
            }
        }

        var existsSocialShortLinksNames = existsSocialShortLinksByBasename?.map { it.shortLink }

        if (neededSocialShortLinkList != null && existsSocialShortLinksNames != null) {
            val shortLinksToCreate = neededSocialShortLinkList.minus(existsSocialShortLinksNames)
            var filteredExtraShortSocialLinks = this.getSocialShortLinkSuffixFromConfig()
            for (shortLinkLocal in filteredExtraShortSocialLinks) {
                shortLinkLocal.name = shortLink.shortLink + "-" + shortLinkLocal.name
            }
            filteredExtraShortSocialLinks = filteredExtraShortSocialLinks.filter { it.name in shortLinksToCreate }
            for (extraShortSocialLink in filteredExtraShortSocialLinks) {
                var shortLinkLocal: ShortLink = ShortLink()
                shortLinkLocal.shortLink = extraShortSocialLink.name
                shortLinkLocal.profileId = shortLink.profileId
                shortLink.redirectAddressUrl?.let {
                    shortLinkLocal.redirectAddressUrl = this.appendUriString(it, extraShortSocialLink.urlParams)
                }
                shortLinkLocal.shortLinkStatus = ShortLinkStatus.ON
                shortLinkDAO.insert(shortLinkLocal)
            }
        }
    }

    fun getSocialShortLinkSuffixList(): List<String> {
        return this.getSocialShortLinkSuffixFromConfig().map { it.name }
    }

    fun getSocialShortLinkSuffixFromConfig(): List<ShortLinkExtraSocialLink> {
        return configurationService.getJsonArrayConfig(ShortLinkExtraSocialLink::class.java,
                ConfigurationType.SHORTLINK_EXTRA_SOCIAL_LINKS)
    }

    fun isItExtraSocialShortLink(linkName: String): Boolean {
        val socialShortLinkSuffixList: List<String> = this.getSocialShortLinkSuffixList() ?: return false

        for (suffix in socialShortLinkSuffixList) {
            if (linkName.endsWith("-$suffix")) {
                return true
            }
        }

        return false
    }

    @Throws(URISyntaxException::class)
    private fun appendUri(uri: String, appendQuery: String): URI {
        val oldUri = URI(uri)

        var newQuery: String? = oldUri.query
        if (newQuery == null) {
            newQuery = appendQuery
        } else {
            newQuery += "&$appendQuery"
        }

        return URI(oldUri.scheme, oldUri.authority,
                oldUri.path, newQuery, oldUri.fragment)
    }

    @Throws(URISyntaxException::class)
    private fun appendUriString(uri: String, appendQuery: String): String {
        return this.appendUri(uri, appendQuery).toString()
    }

}


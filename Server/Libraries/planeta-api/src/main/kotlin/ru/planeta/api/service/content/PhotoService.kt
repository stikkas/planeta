package ru.planeta.api.service.content

import ru.planeta.api.exceptions.NotFoundException
import ru.planeta.api.exceptions.PermissionException
import ru.planeta.api.text.ImageAttachment
import ru.planeta.model.profile.media.Photo

/**
 * Profile's photo service
 *
 * @author ds.kolyshev
 * Date: 27.10.11
 */
interface PhotoService {

    /**
     * Gets list of photos for `USER_HIDDEN` album type
     */
    @Throws(PermissionException::class)
    fun getUserHiddenPhotos(clientId: Long, profileId: Long, offset: Int, limit: Int): List<Photo>

    /**
     * Gets specified photo
     */
    fun getPhoto(clientId: Long, profileId: Long, photoId: Long): Photo?

    /**
     * Updates profile's photo
     */
    @Throws(NotFoundException::class, PermissionException::class)
    fun savePhoto(clientId: Long, photo: Photo): Photo

    /**
     * Deletes specified photo
     */
    @Throws(NotFoundException::class, PermissionException::class)
    fun deletePhoto(clientId: Long, profileId: Long, photoId: Long)

    /**
     * Inserts new avatar photo album
     *
     */
    fun insertAvatarPhotoAlbum(clientId: Long, profileId: Long, albumTypeId: Int, imageId: Long, imageUrl: String): Long

    /**
     * While parsing message from wall, add all images to some user album, CHANGES imageAttachments in-place
     * @param clientId images owner profileId
     * @param albumId with 0 hidden album will be used
     * @param albumTypeId with 0 hidden album will be used
     * @param attachments image attachments from parsed text message
     */
    fun addImageAttachmentsToAlbum(clientId: Long, ownerProfileId: Long, albumId: Long, albumTypeId: Int, attachments: List<ImageAttachment>): List<Photo>

    fun transformPhotosToCampaignAlbum(ownerProfileId: Long, albumId: Long, albumTypeId: Int, photosMap: Map<String, Photo>): Map<String, Photo>

    @Throws(PermissionException::class)
    fun getUserHiddenPhotosCount(clientId: Long, ownerProfileId: Long): Long
}

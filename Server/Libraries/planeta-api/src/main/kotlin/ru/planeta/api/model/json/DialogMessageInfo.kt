package ru.planeta.api.model.json

import ru.planeta.model.msg.DialogMessage
import ru.planeta.model.msg.DialogObject

import java.io.Serializable
import java.util.ArrayList
import java.util.Date

class DialogMessageInfo : DialogObject, Serializable {
    var messageId: Long = 0
    var userId: Long = 0
    var messageText: String? = null
    var messageTextHtml: String? = null
    var isDeleted: Boolean = false
    var timeAdded: Date? = null
    var userImageUrl: String? = null
    var userDisplayName: String? = null
    var userAlias: String? = null
    var isNewMessage: Boolean = false

    constructor() : super(0) {
    }

    constructor(dialogMessage: DialogMessage) : this() {
        copyDialogMessage(dialogMessage)
    }

    private fun copyDialogMessage(dialogMessage: DialogMessage) {
        dialogId = dialogMessage.dialogId
        messageId = dialogMessage.messageId
        userId = dialogMessage.userId
        messageText = dialogMessage.messageText
        messageTextHtml = dialogMessage.messageTextHtml
        isDeleted = dialogMessage.isDeleted
        timeAdded = dialogMessage.timeAdded
        userImageUrl = dialogMessage.userImageUrl
        userDisplayName = dialogMessage.userDisplayName
        userAlias = dialogMessage.userAlias
    }

    companion object {

        fun copyDialogMessages(dialogMessages: List<DialogMessage>): List<DialogMessageInfo> {
            val list = ArrayList<DialogMessageInfo>(dialogMessages.size)
            for (dialogMessage in dialogMessages) {
                list.add(DialogMessageInfo(dialogMessage))
            }
            return list
        }
    }
}

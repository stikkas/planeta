package ru.planeta.api.service.billing.order

import ru.planeta.model.enums.OrderObjectType

/**
 * Created by a.savanovich on 17.10.2016.
 */
interface AfterFundServiceFactory {
    operator fun get(type: OrderObjectType): AfterFundService?
}

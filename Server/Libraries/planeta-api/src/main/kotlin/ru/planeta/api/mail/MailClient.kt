package ru.planeta.api.mail

import ru.planeta.api.exceptions.NotFoundException
import ru.planeta.commons.model.Gender
import ru.planeta.model.bibliodb.Book
import ru.planeta.model.common.*
import ru.planeta.model.common.campaign.Campaign
import ru.planeta.model.common.campaign.CampaignTag
import ru.planeta.model.common.charity.CharityEventOrder
import ru.planeta.model.common.school.Seminar
import ru.planeta.model.mail.MailMessagePriority
import ru.planeta.model.profile.Comment
import ru.planeta.model.profile.Profile
import ru.planeta.model.promo.TechnobattleRegistration
import ru.planeta.model.shop.Product

import java.math.BigDecimal
import java.util.Date

/**
 * Client mailing with notification service
 *
 * @author ds.kolyshev
 * Date: 19.01.12
 */
interface MailClient {

    fun sendRegistrationCompleteEmail(email: String, password: String, regCode: String)

    fun sendAutoRegistrationCompleteEmail(email: String, password: String, regCode: String)

    fun sendRegistrationCompleteEmail(email: String, regCode: String)

    /**
     * Sends password recovery email
     *
     */
    fun sendPasswordRecoveryEmail(email: String, regCode: String, url: String, redirectUrl: String)

    /**
     * Sends campaign feedback email
     *
     * @param email           campaign contact's email string
     */
    fun sendCampaignFeedbackEmail(email: String, message: String, campaign: Campaign, userEmail: String, userId: Long, userDisplayName: String, spam: Boolean?)


    fun sendChangeManagerForCampaignEmail(campaign: Campaign, to: String, comment: String, authorEmail: String)

    fun sendCampaignNotActiveNotificationEmail(order: OrderInfo)

    fun sendNotEnoughShareAmountNotificationEmail(order: OrderInfo)

    fun sendNotEnoughProductAmountNotificationEmail(order: OrderInfo)

    fun sendBalanceCreditedNotificationEmail(email: String, displayName: String, amount: BigDecimal, respondentGender: Gender)

    fun sendBonusOrderAddedNotifications(orderInfo: OrderInfo)

    /**
     * Sends new order added in shop notification
     *
     */
    fun sendShopNewOrderAddedNotification(orderInfo: OrderInfo, groupedOrderObjects: Map<Long, List<OrderObjectInfo>>)

    /**
     * Send to user e-mail from project manager about new project in group
     * @param email     user e-mail
     * @param group     campaign owner
     * @param campaign  target campaign
     * @param requester requester
     * @param imageUrl
     * @param campaignManager
     */
    fun sendToUserCreatedGroupCampaign(email: String, group: Profile, campaign: Campaign, requester: Profile, imageUrl: String, campaignManager: PlanetaManager)

    /**
     * Send an e-mail to the campaign moderators about new project in unofficial group
     *
     * @param group    campaign owner
     * @param campaign target campaign
     * @param campaignManager
     */
    fun sendToManagerGroupCreatedCampaign(userEmail: String, group: Profile, campaign: Campaign, campaignManager: PlanetaManager?)

    /**
     * send to user link to broadcast
     *
     * @param email             email address
     * @param broadcastName     broadcast name
     * @param broadcastUrl      link to broadcast
     * @param linkLifeTimeHours link lifetime in hours after first follow
     */
    fun sendBroadcastLinkToUser(email: String, broadcastName: String, broadcastUrl: String, linkLifeTimeHours: Int)

    fun sendBroadcastSubscriptionNotification(email: String, broadcastName: String, broadcastUrl: String, userDisplayName: String)

    fun sendMailFromAboutUsAdvertising(email: String, message: String)

    fun sendErrorEmailReport(size: Int, errors: String)

    fun sendDailyCampaignsStartedEmail(email: String, campaigns: List<Campaign>)

    fun sendDailyCampaignsFinishedEmail(email: String, campaigns: List<Campaign>)

    /**
     * send rich message
     *
     * @param priority
     * @param template need for logs
     */
    fun sendMessageRich(template: String, mailTo: String, mailFrom: String, emailSubject: String, emailMessage: String, priority: MailMessagePriority)

    fun sendToUserTransferredToPatch(email: String, group: Profile, campaign: Campaign, requester: Profile, imageUrl: String, campaignManager: PlanetaManager)

    fun sendFeedbackEmail(email: String, message: String, theme: String, userId: Long)

    fun sendFeedbackEmailQuickstart(email: String, message: String, theme: String)

    fun sendCampaignDeclinedEmail(email: String, campaign: Campaign, message: String)

    @Throws(NotFoundException::class)
    fun sendOrderCancelledEmailToUser(email: String, profile: Profile, order: Order, reason: String, serviceText: String)

    fun sendQuickstartGetBookEmail(email: String, type: String)

    fun setAsynchronous(isAsynchronous: Boolean)

    fun sendSchoolWebinarRegistration(email: String, seminar: Seminar, seminarImageUrl: String)

    fun sendSchoolOnlineCourse(email: String, userName: String)

    fun sendSchoolSolosimpleRegistration(email: String, seminar: Seminar, seminarImageUrl: String)

    fun sendSchoolSolosimpleOfflineRegistration(email: String, seminar: Seminar, seminarImageUrl: String)

    fun sendSchoolSoloRegistration(email: String, seminar: Seminar, seminarTag: CampaignTag, seminarImageUrl: String)

    fun sendCharityEventOrder(email: String, charityEventOrder: CharityEventOrder)

    fun sendSchoolCompanyRegistration(email: String, seminar: Seminar, seminarImageUrl: String)

    fun sendSchoolCompanyOfflineRegistration(email: String, seminar: Seminar, seminarImageUrl: String)

    fun sendInvestingInvoiceCreated(email: String, userName: String, campaignURL: String, campaignName: String,
                                    invoiceURL: String, invoiceId: Long)

    fun sendBiblioInvoiceCreated(email: String, userName: String, invoiceURL: String, invoiceId: Long)

    fun sendInvestingInvoiceRequest(email: String, userName: String, campaignURL: String, campaignName: String)

    fun sendInvestingInvoiceRequestManager(email: String, userURL: String, userName: String, campaignURL: String,
                                           campaignName: String, investInfo: String, requestURL: String)

    fun sendPaymentInvalidStateReject(processorName: String, transaction: TopayTransaction)

    fun sendPaymentInvalidStateError(processorName: String, transaction: TopayTransaction)

    fun sendProductComment(email: String, product: Product, comment: Comment)

    fun sendBiblioBookRequest(email: String, request: Book)

    fun sendBiblioBookPriceChange(oldPrice: Int, newPrice: Int, changeDate: Date, bookName: String)

    fun sendBiblioLibraryActive(email: String, changeDate: Date)

    fun sendTechnobattleProgrammLetter(email: String)

    fun sendTechnobattleManagerLetterAboutPromoCodesIsGoingToEnd(freeLitresPromoCodesCount: Int)

    fun sendLitresPromoCodeLetter(email: String, promoCode: String)

    fun sendTechnobattleRegistrationManager(email: String, request: TechnobattleRegistration)

    fun sendPromoEmail(email: String, template: String)

    fun sendPromoEmail(email: String, template: String, params: Map<String, String>)

    fun sendCampaignCampaignTwoDaysEnd(email: String, campaignName: String)

    @Throws(InterruptedException::class)
    fun sendCancelPurchaseEmailForUser(mailTo: String, orderId: Long)

    @Throws(InterruptedException::class)
    fun sendCancelPurchaseEmailForSupport(userEmail: String, campaignName: String, orderId: Long)
}

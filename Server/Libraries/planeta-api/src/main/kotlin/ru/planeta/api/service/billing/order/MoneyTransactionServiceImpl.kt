package ru.planeta.api.service.billing.order

import org.springframework.stereotype.Service
import ru.planeta.api.exceptions.NotFoundException
import ru.planeta.api.exceptions.PermissionException
import ru.planeta.api.service.security.PermissionService
import ru.planeta.dao.commondb.CreditTransactionDAO
import ru.planeta.dao.commondb.DebitTransactionDAO
import ru.planeta.dao.commondb.MoneyTransactionDAO
import ru.planeta.model.common.CreditTransaction
import ru.planeta.model.common.DebitTransaction
import ru.planeta.model.common.MoneyTransaction
import ru.planeta.model.enums.MoneyTransactionType
import ru.planeta.model.enums.OrderObjectType

/**
 *
 * Created by eshevchenko on 12.02.14.
 */
@Service
class MoneyTransactionServiceImpl(private val moneyTransactionDAO: MoneyTransactionDAO,
                                  private val debitTransactionDAO: DebitTransactionDAO,
                                  private val creditTransactionDAO: CreditTransactionDAO,
                                  private val permissionService: PermissionService) : MoneyTransactionService {


    override fun getDebitTransaction(transactionId: Long): DebitTransaction? {
        return debitTransactionDAO.select(transactionId)
    }

    @Throws(NotFoundException::class)
    override fun getDebitTransactionSafe(transactionId: Long): DebitTransaction {
        return getDebitTransaction(transactionId)
                ?: throw NotFoundException(DebitTransaction::class.java, transactionId)
    }

    override fun getCreditTransaction(transactionId: Long): CreditTransaction? {
        return creditTransactionDAO.select(transactionId)
    }

    @Throws(NotFoundException::class)
    override fun getCreditTransactionSafe(transactionId: Long): CreditTransaction {
        return getCreditTransaction(transactionId)
                ?: throw NotFoundException(CreditTransaction::class.java, transactionId)
    }

    @Throws(PermissionException::class)
    override fun getTransactions(clientId: Long, profileId: Long, transactionType: MoneyTransactionType?,
                                 orderObjectType: OrderObjectType?, offset: Int, limit: Int): List<MoneyTransaction> {
        permissionService.checkIsAdmin(clientId, profileId)
        return moneyTransactionDAO.selectMoneyTransaction(profileId, transactionType, orderObjectType, offset, limit)
    }

}

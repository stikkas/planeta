package ru.planeta.api.service.common

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import ru.planeta.api.exceptions.NotFoundException
import ru.planeta.dao.commondb.MailMessageDAO
import ru.planeta.model.mail.MailMessage
import ru.planeta.model.mail.MailMessageStatus

/**
 * User: michail
 * Date: 19.05.16
 * Time: 19:01
 */
@Service
class MailMessageServiceImpl : MailMessageService {
    @Autowired
    internal var mailMessageDAO: MailMessageDAO? = null

    override fun insertMailNotification(message: MailMessage): MailMessage {
        mailMessageDAO!!.insertMailNotification(message)
        return message
    }

    @Throws(NotFoundException::class)
    override fun getMailNotificationSafeById(id: Long): List<MailMessage> {
        val mailMessages = mailMessageDAO!!.getMailNotificationById(id)
        if (mailMessages.isEmpty()) {
            throw NotFoundException(MailMessage::class.java, id)
        }
        return mailMessages
    }

    @Throws(NotFoundException::class)
    override fun getMailNotificationSafeByExtMsgId(extMsgId: String): List<MailMessage> {
        val emailNotification = mailMessageDAO!!.getMailNotificationByExtMsgId(extMsgId.toLowerCase())
        if (emailNotification.isEmpty()) {
            throw NotFoundException(MailMessage::class.java, "External message id", extMsgId)
        }
        return emailNotification
    }

    @Throws(NotFoundException::class)
    override fun setExternalMessageId(id: Long, extMsgId: String) {
        mailMessageDAO!!.setExternalMessageIdAndSentStatus(id, extMsgId)
    }

    override fun setStatusByExtMsgId(extMsgId: String, status: MailMessageStatus) {
        mailMessageDAO!!.updateStatus(extMsgId, status)
    }

    override fun saveOpenedMailInfo(messageId: Long?) {
        mailMessageDAO!!.saveOpenedMailInfo(messageId!!)
    }
}


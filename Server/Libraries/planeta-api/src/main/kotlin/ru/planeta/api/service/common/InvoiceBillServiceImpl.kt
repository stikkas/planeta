package ru.planeta.api.service.common

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import ru.planeta.dao.commondb.InvoiceBillDAO
import ru.planeta.model.common.InvoiceBill

/**
 *
 * User: Serge Blagodatskih<stikkas17></stikkas17>@gmail.com><br></br>
 * Date: 16.02.16<br></br>
 * Time: 16:33
 */
@Service
class InvoiceBillServiceImpl : InvoiceBillService {

    @Autowired
    private val ibDao: InvoiceBillDAO? = null

    override fun getProfileBillByNumber(buyerId: Long, number: Long): InvoiceBill {
        return ibDao!!.select(buyerId, number)
    }

    override fun getProfileBills(buyerId: Long, query: String, offset: Int, limit: Int): List<InvoiceBill> {
        return ibDao!!.select(buyerId, query, offset, limit)
    }
}

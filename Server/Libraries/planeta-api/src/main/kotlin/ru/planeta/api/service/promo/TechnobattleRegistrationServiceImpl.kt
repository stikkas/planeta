package ru.planeta.api.service.promo

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import ru.planeta.dao.promo.TechnobattleRegistrationDAO
import ru.planeta.model.promo.TechnobattleRegistration

import java.util.Date

/**
 * User: michail
 * Date: 18.11.2016
 * Time: 12:52
 */

@Service
class TechnobattleRegistrationServiceImpl @Autowired
constructor(private val technobattleRegistrationDAO: TechnobattleRegistrationDAO) : TechnobattleRegistrationService {

    override fun insertRegistrationRequest(registration: TechnobattleRegistration): Int {
        registration.timeAdded = Date()
        return technobattleRegistrationDAO.insert(registration)
    }
}

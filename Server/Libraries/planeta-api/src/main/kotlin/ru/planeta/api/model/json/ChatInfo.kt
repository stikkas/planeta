package ru.planeta.api.model.json

import ru.planeta.model.profile.chat.Chat
import ru.planeta.model.profile.chat.ChatUser

import java.util.HashSet

/**
 * This class contains info about chat
 *
 * @author m.shulepov
 */
class ChatInfo(@get:Synchronized @set:Synchronized var chat: Chat?) {

    // Contains either all chat users or some random amount (numOfUsers always equal to full number of users in chat)
    @get:Synchronized
    @set:Synchronized
    var chatUsers: Set<ChatUser>? = null
    @get:Synchronized
    @set:Synchronized
    var numOfUsers: Long = 0

    var isEnabled: Boolean = false

    init {
        chatUsers = HashSet()
    }

}

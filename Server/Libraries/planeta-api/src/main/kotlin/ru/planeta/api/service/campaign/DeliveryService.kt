package ru.planeta.api.service.campaign

import ru.planeta.api.exceptions.NotFoundException
import ru.planeta.api.exceptions.PermissionException
import ru.planeta.model.common.Address
import ru.planeta.model.common.campaign.ShareEditDetails
import ru.planeta.model.common.delivery.BaseDelivery
import ru.planeta.model.common.delivery.LinkedDelivery
import ru.planeta.model.common.delivery.SubjectType
import ru.planeta.model.shop.Product

/**
 * @author Andrew.Arefyev@gmail.com
 * 29.10.13 19:08
 */
interface DeliveryService {

    @Throws(NotFoundException::class)
    fun getLinkedDelivery(subjectId: Long, deliveryId: Long, subjectType: SubjectType): LinkedDelivery

    fun getLinkedDeliveries(shareId: Long, subjectType: SubjectType): List<LinkedDelivery>

    fun getEnabledLinkedDeliveries(subjectId: Long, subjectType: SubjectType): List<LinkedDelivery>

    @Throws(PermissionException::class)
    fun saveLinkedDelivery(clientId: Long, delivery: LinkedDelivery)

    fun getBaseDeliveries(searchString: String?): List<BaseDelivery>

    /**
     * unlinked delivery services except default ones.
     * @param shareId
     * @param subjectType
     * @return
     */
    fun getUnlinkedBaseDeliveries(shareId: Long, subjectType: SubjectType): List<BaseDelivery>

    @Throws(PermissionException::class)
    fun getBaseDelivery(clientId: Long, deliveryId: Long): BaseDelivery

    @Throws(PermissionException::class)
    fun saveBaseDelivery(clientId: Long, delivery: BaseDelivery)

    @Throws(PermissionException::class)
    fun removeBaseDelivery(clientId: Long, deliveryTemplateId: Long)

    @Throws(PermissionException::class)
    fun linkDefaultDeliveryToShare(clientId: Long, shareId: Long, serviceId: Long, storedAddress: Address)

    @Throws(PermissionException::class)
    fun disableLinkDelivery(clientId: Long, subjectId: Long, serviceId: Long, subjectType: SubjectType)

    @Throws(PermissionException::class)
    fun updateDefaultDeliveryServices(clientId: Long, shareDetails: ShareEditDetails)

    fun filterProductDeliveries(deliveries: List<LinkedDelivery>, products: List<Product>)

}

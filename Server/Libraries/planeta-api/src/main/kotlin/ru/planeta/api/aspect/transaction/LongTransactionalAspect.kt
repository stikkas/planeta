package ru.planeta.api.aspect.transaction

import org.apache.log4j.Logger
import org.aspectj.lang.ProceedingJoinPoint
import ru.planeta.dao.TransactionScope

/**
 * @author a.savanovich
 * @since 12/12/16 13:21 PM
 */
class LongTransactionalAspect {

    @Throws(Throwable::class)
    fun transaction(joinPoint: ProceedingJoinPoint): Any {
        val transactionScope = TransactionScope.createOrGetCurrent()
        val begin = System.currentTimeMillis()
        try {
            val result = joinPoint.proceed()
            transactionScope.commit()
            return result
        } finally {
            val end = System.currentTimeMillis()
            if (end - begin > 5000) {
                log.info("Long method " + joinPoint.signature.toLongString() + " " + (end - begin))
            }
            transactionScope.close()
        }
    }

    companion object {

        private val log = Logger.getLogger(LongTransactionalAspect::class.java)
    }
}

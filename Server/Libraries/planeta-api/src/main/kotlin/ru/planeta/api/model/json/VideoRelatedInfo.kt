package ru.planeta.api.model.json

import ru.planeta.model.profile.media.Video

/**
 * Class for related video json
 *
 * @author ds.kolyshev
 * Date: 30.10.12
 */
class VideoRelatedInfo {
    var profileId: Long = 0
    var videoId: Long = 0
    var imageUrl: String? = null
    var duration: Int = 0
    var videoPageUrl: String? = null
    var name: String? = null

    constructor() {

    }

    constructor(video: Video) {
        this.profileId = video.profileId!!
        this.videoId = video.videoId
        this.imageUrl = video.imageUrl
        this.duration = video.duration
        this.name = video.name
    }
}

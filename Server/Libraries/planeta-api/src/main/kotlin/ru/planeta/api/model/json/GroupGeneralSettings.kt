package ru.planeta.api.model.json

import ru.planeta.model.enums.GroupCategory

/**
 * @author ds.kolyshev
 * Date: 11.11.11
 */
class GroupGeneralSettings {

    var profileId: Long = 0
    var displayName: String? = null
    var alias: String? = null
    var summary: String? = null
    var biography: String? = null
    var contactInfo: String? = null
    var siteUrl: String? = null
    var genre: String? = null
    var groupCategory: GroupCategory? = null
}

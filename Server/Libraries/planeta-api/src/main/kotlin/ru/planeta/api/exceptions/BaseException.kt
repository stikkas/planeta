package ru.planeta.api.exceptions

import org.apache.commons.lang3.StringUtils

/**
 * Base exception.<br></br>
 * Encapsulates [messageCode][ru.planeta.api.exceptions.MessageCode] that identifies exception's code.<br></br>
 * Created by eshevchenko on 26.12.13.
 */
abstract class BaseException : Exception {

    var messageCode: MessageCode? = null
        private set
    var arguments: Any? = null

    protected constructor() {}

    protected constructor(message: String) : super(message) {
        this.messageCode = MessageCode.getByValue(message)
    }

    protected constructor(message: String, cause: Throwable) : super(message, cause) {
        this.messageCode = MessageCode.getByValue(message)
    }

    protected constructor(cause: Throwable) : super(cause) {}

    @JvmOverloads constructor(messageCode: MessageCode, formattedMessage: String = "") : super(StringUtils.defaultIfBlank<String>(formattedMessage, messageCode.errorPropertyName)) {
        this.messageCode = messageCode
    }

    constructor(messageCode: MessageCode, vararg arguments: Any) {
        this.messageCode = messageCode
        this.arguments = arguments
    }

}

package ru.planeta.api.service.notifications

import ru.planeta.commons.web.WebUtils

internal class ParsingVideoElement : ParsingRichElement() {

    override val tag: String
        get() = "video"

    override fun getImage(paramsString: String, staticNode: String): String {
        return ("$staticNode/video-stub.jpg?"
                + WebUtils.tryUrlEncodeMap("UTF-8",
                "name", ParsingRichElement.getAttr(paramsString, "name"),
                "duration", ParsingRichElement.getDuration(paramsString),
                "url", ParsingRichElement.getAttr(paramsString, "image")
        ))
    }

}

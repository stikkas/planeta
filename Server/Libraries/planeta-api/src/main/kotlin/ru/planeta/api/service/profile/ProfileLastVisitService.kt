package ru.planeta.api.service.profile

import ru.planeta.model.enums.ProfileLastVisitType

import java.util.Date

interface ProfileLastVisitService {
    fun insertOrUpdateProfileLastVisit(profileId: Long, profileLastVisitType: ProfileLastVisitType)

    fun insertOrUpdateProfileLastVisit(profileId: Long, profileLastVisitType: ProfileLastVisitType, objectId: Long)

    fun selectProfileLastVisit(profileId: Long, profileLastVisitType: ProfileLastVisitType): Date

    fun selectProfileLastVisit(profileId: Long, profileLastVisitType: ProfileLastVisitType, objectId: Long): Date
}

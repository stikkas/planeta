package ru.planeta.api.search

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import ru.planeta.model.common.campaign.CampaignTag
import ru.planeta.model.common.campaign.enums.CampaignStatus
import java.math.BigDecimal
import java.util.Date
import java.util.EnumSet

@JsonIgnoreProperties(ignoreUnknown = true)
class SearchCampaigns {
    var sortOrderList: List<SortOrder>? = null
    var query: String? = null
    var campaignTags: MutableList<CampaignTag>? = null
    var categories: List<String>? = null
    var status: CampaignStatusFilter? = CampaignStatusFilter.ACTIVE_AND_FINISHED
    var completePercentsRange: Range<Int>? = null
    var targetAmountRange: Range<BigDecimal>? = null
    var collectedAmountRange: Range<BigDecimal>? = null
    var finishTimeRange: Range<Date>? = null
    var timeAddedRange: Range<Date>? = null
    var campaignStatus: EnumSet<CampaignStatus>? = null
    var managerId: Long? = null
    var isDontShowUnsuccessfulProject: Boolean? = null
    var isWithoutPromoCampaignsAtFirstPage: Boolean = false
    var countryId: Long = 0
    var regionId: Long = 0
    var cityId: Long = 0
    var regionNameRus: String? = null
    var regionNameEng: String? = null
    var cityNameRus: String? = null
    var cityNameEng: String? = null
    var offset = 0
    var limit = 20

    constructor() {
    }

    constructor(sortOrderList: List<SortOrder>, query: String?, campaignTags: List<CampaignTag>?, status: CampaignStatusFilter?, completePercentsRange: Range<Int>?, targetAmountRange: Range<BigDecimal>?, finishTimeRange: Range<Date>?, timeAddedRange: Range<Date>?, campaignStatus: EnumSet<CampaignStatus>?, managerId: Long?, dontShowUnsuccessfulProject: Boolean?, withoutPromoCampaignsAtFirstPage: Boolean, offset: Int, limit: Int) {
        this.sortOrderList = sortOrderList
        this.query = query
        this.campaignTags = campaignTags?.toMutableList()
        this.status = status
        this.completePercentsRange = completePercentsRange
        this.targetAmountRange = targetAmountRange
        this.finishTimeRange = finishTimeRange
        this.timeAddedRange = timeAddedRange
        this.campaignStatus = campaignStatus
        this.managerId = managerId
        this.isDontShowUnsuccessfulProject = dontShowUnsuccessfulProject
        this.isWithoutPromoCampaignsAtFirstPage = withoutPromoCampaignsAtFirstPage
        this.offset = offset
        this.limit = limit
    }

    constructor(sortOrderList: List<SortOrder>, query: String, campaignTags: List<CampaignTag>, campaignStatus: EnumSet<CampaignStatus>, offset: Int, limit: Int) {
        this.sortOrderList = sortOrderList
        this.query = query
        this.campaignTags = campaignTags.toMutableList()
        this.campaignStatus = campaignStatus
        this.offset = offset
        this.limit = limit
    }

    constructor(sortOrderList: List<SortOrder>, campaignTags: List<CampaignTag>, campaignStatus: EnumSet<CampaignStatus>, targetAmountRange: Range<BigDecimal>, limit: Int) {
        this.sortOrderList = sortOrderList
        this.campaignTags = campaignTags.toMutableList()
        this.campaignStatus = campaignStatus
        this.targetAmountRange = targetAmountRange
        this.limit = limit
    }

    constructor(sortOrderList: List<SortOrder>, query: String, campaignStatus: EnumSet<CampaignStatus>, offset: Int, limit: Int) {
        this.sortOrderList = sortOrderList
        this.query = query
        this.campaignStatus = campaignStatus
        this.offset = offset
        this.limit = limit
    }
}

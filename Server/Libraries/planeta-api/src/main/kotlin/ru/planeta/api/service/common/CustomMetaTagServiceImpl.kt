package ru.planeta.api.service.common

import org.apache.commons.collections4.CollectionUtils
import org.springframework.stereotype.Service
import ru.planeta.dao.commondb.CustomMetaTagDAO
import ru.planeta.model.commondb.CustomMetaTag
import ru.planeta.model.commondb.CustomMetaTagExample
import ru.planeta.model.enums.CustomMetaTagType
import java.util.*

@Service
class CustomMetaTagServiceImpl(private var customMetaTagDAO: CustomMetaTagDAO) : CustomMetaTagService {

    override fun selectCustomMetaTag(customMetaTagType: CustomMetaTagType, tagPath: String): CustomMetaTag? {
        val list = customMetaTagDAO.selectByExample(getCustomMetaTagExample(customMetaTagType, tagPath, 0, 1))
        return if (CollectionUtils.isEmpty(list)) CustomMetaTag() else list[0]
    }


    val orderByClause: String = "custom_meta_tag_id"
    fun insertCustomMetaTag(customMetaTag: CustomMetaTag) {
        customMetaTagDAO.insertSelective(customMetaTag)
    }

    fun insertCustomMetaTagAllFields(customMetaTag: CustomMetaTag) {
        customMetaTagDAO.insert(customMetaTag)
    }

    fun updateCustomMetaTag(customMetaTag: CustomMetaTag) {
        customMetaTagDAO.updateByPrimaryKeySelective(customMetaTag)
    }

    fun updateCustomMetaTagAllFields(customMetaTag: CustomMetaTag) {
        customMetaTagDAO.updateByPrimaryKey(customMetaTag)
    }

    override fun insertOrUpdateCustomMetaTag(customMetaTag: CustomMetaTag) {
        val selectedCustomMetaTag = customMetaTagDAO.selectByPrimaryKey(customMetaTag.customMetaTagId)
        if (selectedCustomMetaTag == null) {

            customMetaTagDAO.insertSelective(customMetaTag)
        } else {
            customMetaTagDAO.updateByPrimaryKeySelective(customMetaTag)
        }
    }

    fun insertOrUpdateCustomMetaTagAllFields(customMetaTag: CustomMetaTag) {
        val selectedCustomMetaTag = customMetaTagDAO.selectByPrimaryKey(customMetaTag.customMetaTagId)
        if (selectedCustomMetaTag == null) {

            customMetaTagDAO.insert(customMetaTag)
        } else {
            customMetaTagDAO.updateByPrimaryKey(customMetaTag)
        }
    }

    override fun deleteCustomMetaTag(id: Long?) {
        customMetaTagDAO.deleteByPrimaryKey(id)
    }

    fun selectCustomMetaTag(customMetaTagId: Long?): CustomMetaTag {
        return customMetaTagDAO.selectByPrimaryKey(customMetaTagId)
    }

    fun getCustomMetaTagExample(customMetaTagType: CustomMetaTagType, offset: Int, limit: Int): CustomMetaTagExample {
        val example = CustomMetaTagExample()
        example.offset = offset
        example.limit = limit
        example.or()
                .andCustomMetaTagTypeEqualTo(customMetaTagType)
        example.orderByClause = orderByClause
        return example
    }

    fun selectCustomMetaTagCount(customMetaTagType: CustomMetaTagType): Int {
        return customMetaTagDAO.countByExample(getCustomMetaTagExample(customMetaTagType, 0, 0))
    }

    override fun selectCustomMetaTagList(customMetaTagType: CustomMetaTagType, offset: Int, limit: Int): List<CustomMetaTag> {
        return customMetaTagDAO.selectByExample(getCustomMetaTagExample(customMetaTagType, offset, limit))
    }

    fun selectCustomMetaTag(customMetaTagType: CustomMetaTagType): CustomMetaTag? {
        val list = customMetaTagDAO.selectByExample(getCustomMetaTagExample(customMetaTagType, 0, 0))
        return if (list.size > 0) list[0] else null
    }

    fun selectCustomMetaTagList(customMetaTagType: CustomMetaTagType, customMetaTagIdList: List<Long>?): List<CustomMetaTag> {
        return if (customMetaTagIdList == null || customMetaTagIdList.isEmpty()) ArrayList() else customMetaTagDAO.selectByIdList(customMetaTagIdList)
    }

    fun getCustomMetaTagExample(customMetaTagType: CustomMetaTagType, tagPath: String, offset: Int, limit: Int): CustomMetaTagExample {
        val example = CustomMetaTagExample()
        example.offset = offset
        example.limit = limit
        example.or()
                .andCustomMetaTagTypeEqualTo(customMetaTagType)
                .andTagPathEqualTo(tagPath)
        example.orderByClause = orderByClause
        return example
    }

    fun selectCustomMetaTagCount(customMetaTagType: CustomMetaTagType, tagPath: String): Int {
        return customMetaTagDAO.countByExample(getCustomMetaTagExample(customMetaTagType, tagPath, 0, 0))
    }

    fun selectCustomMetaTagList(customMetaTagType: CustomMetaTagType, tagPath: String, offset: Int, limit: Int): List<CustomMetaTag> {
        return customMetaTagDAO.selectByExample(getCustomMetaTagExample(customMetaTagType, tagPath, offset, limit))
    }

    fun selectCustomMetaTagList(customMetaTagType: CustomMetaTagType, tagPath: String, customMetaTagIdList: List<Long>?): List<CustomMetaTag> {
        return if (customMetaTagIdList == null || customMetaTagIdList.isEmpty()) ArrayList() else customMetaTagDAO.selectByIdList(customMetaTagIdList)
    }

    fun getCustomMetaTagExample(customMetaTagType: CustomMetaTagType, customMetaTagId: Long?, offset: Int, limit: Int): CustomMetaTagExample {
        val example = CustomMetaTagExample()
        example.offset = offset
        example.limit = limit
        example.or()
                .andCustomMetaTagTypeEqualTo(customMetaTagType)
                .andCustomMetaTagIdEqualTo(customMetaTagId)
        example.orderByClause = orderByClause
        return example
    }

    fun selectCustomMetaTagCount(customMetaTagType: CustomMetaTagType, customMetaTagId: Long?): Int {
        return customMetaTagDAO.countByExample(getCustomMetaTagExample(customMetaTagType, customMetaTagId, 0, 0))
    }

    fun selectCustomMetaTagList(customMetaTagType: CustomMetaTagType, customMetaTagId: Long?, offset: Int, limit: Int): List<CustomMetaTag> {
        return customMetaTagDAO.selectByExample(getCustomMetaTagExample(customMetaTagType, customMetaTagId, offset, limit))
    }

    override fun selectCustomMetaTag(customMetaTagType: CustomMetaTagType, customMetaTagId: Long?): CustomMetaTag? {
        val list = customMetaTagDAO.selectByExample(getCustomMetaTagExample(customMetaTagType, customMetaTagId, 0, 0))
        return if (list.size > 0) list[0] else null
    }

    fun selectCustomMetaTagList(customMetaTagType: CustomMetaTagType, customMetaTagId: Long?, customMetaTagIdList: List<Long>?): List<CustomMetaTag> {
        return if (customMetaTagIdList == null || customMetaTagIdList.isEmpty()) ArrayList() else customMetaTagDAO.selectByIdList(customMetaTagIdList)
    }
}


package ru.planeta.api

import org.apache.commons.lang3.StringUtils
import org.jsoup.Jsoup
import ru.planeta.api.text.FormattingService
import ru.planeta.commons.web.WebUtils
import ru.planeta.model.common.Identified
import ru.planeta.model.profile.Identifier
import java.math.BigDecimal
import java.net.MalformedURLException
import java.net.URI
import java.net.URISyntaxException
import java.net.URL
import java.util.*
import java.util.regex.Matcher
import java.util.regex.Pattern

/**
 * Created with IntelliJ IDEA.
 * User: Alexander
 * Date: 02.11.12
 * Time: 18:05
 */
object Utils {
    // not use double constructor here http://stackoverflow.com/questions/2389457/java-loss-of-precision
    private val MIN_MONEY = BigDecimal("0.01")
    private val DOTS = '\u2026'
    private val REPLACE_EXTERNAL_LINKS = Pattern.compile("<a(.*?)\\s+href=(\\\\*?['\"])(.*?)\\2", Pattern.CASE_INSENSITIVE or Pattern.DOTALL)

    /**
     * returns first non null element from array
     *
     */
    @JvmStatic
    @Deprecated("")
    // deprecated. possible heap pollution
    fun <T> nvl(vararg objects: T?): T? {
        return objects.find { it != null }
    }

    /**
     * null-safe contains for string
     *
     */
    @JvmStatic
    fun contains(src: String, what: String): Boolean {
        return !empty(src) && !empty(what) && src.contains(what)
    }

    @JvmStatic
    fun split(`in`: String, separators: String): Collection<String> {
        if (empty(`in`) || empty(separators)) {
            return emptyList()
        }
        val tokens = ArrayList<String>()
        val st = StringTokenizer(`in`, separators)
        while (st.hasMoreTokens()) {
            tokens.add(st.nextToken())
        }
        return tokens
    }

    /**
     * null-safe empty check for string
     *
     * @param s
     * @return true if string is empty or null
     */
    @JvmStatic
    fun empty(s: CharSequence?): Boolean {
        return s == null || s.length == 0
    }

    @JvmStatic
    fun empty(collection: Collection<*>?): Boolean {
        return collection == null || collection.isEmpty()
    }

    @JvmStatic
    fun empty(arr: Array<Any>?): Boolean {
        return arr == null || arr.size == 0
    }

    @JvmStatic
    fun empty(map: Map<*, *>?): Boolean {
        return map == null || map.isEmpty()
    }

    @JvmStatic
    @Deprecated("")
    // deprecated. possible heap pollution
    fun <T> `in`(obj: T?, vararg arr: T): Boolean {
        if (obj == null || arr.isEmpty()) {
            return false
        }
        return arr.contains(obj)
    }


    /**
     * null-safe trimming text by word to specified length
     *
     */
    @JvmStatic
    fun abbreviate(text: String, length: Int): String? {
        var text = text
        if (!empty(text) && text.length > length) {
            var index = -1
            var i = length
            while (i > 0 && index == -1) {
                if (Character.isWhitespace(text[i])) {
                    index = i
                }
                --i
            }
            text = text.substring(0, if (index > 0) index else length) + DOTS
        }
        return text
    }


    @JvmStatic
    fun isPlanetaUrl(url: String): Boolean {
        return StringUtils.contains(url, "planeta.ru") || StringUtils.contains(url, "localhost")
    }

    /**
     * @param responseText     - html/text or application/json response text with possible external anchors
     * @param awayUrl          url with required parameter "to", like vk.com/away.php
     * @param validUrlPatterns - url patterns like
     * @return text with external anchors wrapped into away
     */
    @JvmStatic
    fun replaceExternalUrls(responseText: String?, awayUrl: String, urlParamName: String, validUrlPatterns: List<Pattern>): String {
        val matcher = REPLACE_EXTERNAL_LINKS.matcher(responseText ?: "")
        val sb = StringBuffer()
        while (matcher.find()) {
            val url = matcher.group(3).trim { it <= ' ' }
            if (isExternalUrlToRedirect(url, validUrlPatterns)) {
                val params = HashMap<String, String>()
                params[urlParamName] = url
                val redirectUrl = WebUtils.createUrl(awayUrl, params, true)
                val mayBeEscapedQuote = matcher.group(2)
                var replacement = "<a target=" + mayBeEscapedQuote + "_blank" + mayBeEscapedQuote + " href=" + mayBeEscapedQuote + redirectUrl + mayBeEscapedQuote
                if (StringUtils.isNotBlank(matcher.group(1))) {
                    replacement += " " + matcher.group(1)
                }
                matcher.appendReplacement(sb, Matcher.quoteReplacement(replacement))
            }
        }
        matcher.appendTail(sb)
        return sb.toString()
    }

    @JvmStatic
    fun isExternalUrlToRedirect(url: String, validUrlPatterns: List<Pattern>): Boolean {
        // first check for not-http links like ftp and mailto
        try {
            val uri = URL(url)
            if (!(uri.protocol == "http" || uri.protocol == "https")) {
                return false
            }
        } catch (ignored: MalformedURLException) {
        }

        val urlWithHttp = WebUtils.appendProtocolIfNecessary(url, false)
        for (validPattern in validUrlPatterns) {
            if (validPattern.matcher(urlWithHttp).matches()) {
                return false
            }
        }
        try { // it is more secure than isPlanetaUrl
            val uri = URI(urlWithHttp)
            val host = uri.host
            return host != null && !(host.endsWith("planeta.ru") || host.startsWith("localhost"))
        } catch (e1: URISyntaxException) {
            return false
        }

    }

    @JvmStatic
    fun replaceAwayUrlsToMailUrls(htmlWithAwayUrls: String, domain: String): String {
        val document = Jsoup.parse(htmlWithAwayUrls)
        val elements = document.select("a")
        for (element in elements) {
            val replacedHref = element.attr("href").replace(("(https?://.*?)?" + FormattingService.AWAY_URL).toRegex(), domain + FormattingService.AWAY_MAIL)
            element.attr("href", replacedHref)
        }
        return document.body().html()
    }

    @JvmStatic
    fun map(vararg objects: Any): Map<String, Any> {
        if (objects.size < 2) {
            return emptyMap()
        }
        val map = HashMap<String, Any>(objects.size / 2)
        for (i in 0 until objects.size / 2) {
            map[objects[i * 2].toString()] = objects[i * 2 + 1]
        }
        return map
    }

    @JvmStatic
    fun smap(vararg objects: Any): Map<String, String> {
        if (objects.size < 2) {
            return emptyMap()
        }
        val map = HashMap<String, String>(objects.size / 2)
        for (i in 0 until objects.size / 2) {
            map[objects[i * 2].toString()] = objects[i * 2 + 1].toString()
        }
        return map
    }

    @JvmStatic
    fun map(vararg objects: String): Map<String, String> {
        if (objects.size < 2) {
            return emptyMap()
        }
        val map = HashMap<String, String>(objects.size / 2)
        for (i in 0 until objects.size / 2) {
            map[objects[i * 2]] = objects[i * 2 + 1]
        }
        return map
    }

    @JvmStatic
    fun put(map: MutableMap<String, String>, vararg objects: String): Map<String, String> {
        if (objects.size < 2) {
            return emptyMap()
        }
        for (i in 0 until objects.size / 2) {
            map[objects[i * 2]] = objects[i * 2 + 1]
        }
        return map
    }

    @JvmStatic
    fun equals(n1: BigDecimal?, n2: BigDecimal?): Boolean {
        if (n1 == null || n2 == null) {
            throw IllegalArgumentException()
        }
        return n1.subtract(n2).abs().compareTo(MIN_MONEY) <= 0
    }

    @JvmStatic
    fun <T : Identified> map(objects: Collection<T>): Map<Long, T> {
        val map = HashMap<Long, T>(objects.size)
        for (t in objects) {
            map[t.id] = t
        }
        return map
    }

    @JvmStatic
    fun concat(separator: String, vararg args: String): String {
        val sb = StringBuilder()
        for (s in args) {
            sb.append(s).append(separator)
        }
        sb.deleteCharAt(sb.length - separator.length)
        return sb.toString()
    }


    @JvmStatic
    fun concat(separator: String, vararg args: Any): String {
        val sb = StringBuilder()
        for (o in args) {
            sb.append(nvl(o, "")).append(separator)
        }
        sb.delete(sb.length - separator.length, sb.length)
        return sb.toString()
    }

    @JvmStatic
    fun getIdList(list: Collection<Identifier>): List<Long> {
        val result = ArrayList<Long>(list.size)
        list.mapTo(result) { it.id }
        return result
    }
}


package ru.planeta.api.service.shop

import ru.planeta.api.exceptions.NotFoundException
import ru.planeta.api.exceptions.PaymentException
import ru.planeta.api.exceptions.PermissionException
import ru.planeta.model.shop.Category
import ru.planeta.model.shop.Product
import ru.planeta.model.shop.ProductInfo

/**
 * Product's users service
 *
 * @author ds.kolyshev
 * Date: 27.07.12
 */
interface ProductUsersService {

    val currentProductsCount: Long

    @Throws(PermissionException::class, NotFoundException::class)
    fun updateProductTags(clientId: Long, productId: Long, tags: List<Category>): List<Category>

    /**
     * Gets current version of product with child-attributes-products.
     *
     * @param clientId  requester
     * @param productId product
     * @return product with internal tree
     */
    @Throws(NotFoundException::class)
    fun getProductCurrent(clientId: Long, productId: Long): ProductInfo

    /**
     * Gets lite product
     *
     * @param productId product
     * @return product with internal tree
     */
    @Throws(NotFoundException::class)
    fun getProductSafe(productId: Long): ProductInfo

    /**
     * Sets product's current version to pause
     *
     * @param clientId  requester
     * @param productId product to pause
     * @throws NotFoundException if product is not valid for pause
     */
    @Throws(PermissionException::class, NotFoundException::class)
    fun pauseProduct(clientId: Long, productId: Long)

    /**
     * Sets metaproduct's current version and version of its children (sizes) to ACTIVE.
     *
     * @param clientId  admin (who starts profileId)
     * @param productId metaproduct (which has size-children) id
     */
    @Throws(NotFoundException::class, PermissionException::class)
    fun startProduct(clientId: Long, productId: Long)

    /**
     * current and new if not current found product versions
     *
     */
    fun getTopProducts(offset: Int, limit: Int, query: String, tagId: Int, status: String): List<ProductInfo>

    /**
     * Puts product current version to archive.
     *
     * @param clientId  requester
     * @param productId product
     */
    @Throws(NotFoundException::class, PermissionException::class)
    fun deleteProduct(clientId: Long, productId: Long)


    /**
     * Returns the product has passed all required checks for purchasing.
     *
     * @param buyerId   requester
     * @param productId product
     * @param count     quantity to bye
     * @return Null or product
     */
    @Throws(NotFoundException::class, PaymentException::class)
    fun getProductReadyForPurchase(buyerId: Long, productId: Long, count: Int): ProductInfo

    /**
     * Increases the total count purchases of the product and parent (if META) to the specified `count`.
     *
     * @param buyerId   requester
     * @param productId product
     * @param count     quantity purchased
     * @throws NotFoundException
     */
    @Throws(NotFoundException::class)
    fun updateProductAfterPurchase(buyerId: Long, productId: Long, count: Int)

    /**
     * Decreases the total count purchases of the product to the specified count.
     *
     * @param buyerId   requester
     * @param productId product
     * @param count     quantity to return
     * @throws NotFoundException
     */
    @Throws(NotFoundException::class, PermissionException::class)
    fun updateProductAfterPurchaseCancellation(buyerId: Long, productId: Long, count: Int)

    fun magicProductsSearch(sortedProductIds: List<Long>): List<ProductInfo>

    /**
     * Updates quantity of product in warehouses
     *
     * @param clientId requester
     * @throws NotFoundException
     * @throws PermissionException
     */
    @Throws(NotFoundException::class, PermissionException::class)
    fun updateProductTotalQuantity(clientId: Long, productId: Long, quantity: Int)

    /**
     * selectCampaignById ACTIVE, PAUSE, newly created products without moderator confirmation.
     *
     * @param myProfileId requester
     * @param parentId    parent product Id or 0
     * @return [List]&lt;[ProductInfo]&gt;
     */
    @Throws(PermissionException::class, NotFoundException::class)
    fun getProductsByParent(myProfileId: Long, parentId: Long): List<ProductInfo>

    /**
     * Creates or saves new version of a product with child-attributes-products.
     * Increases child-attributes-products versions. All fields would be copied from parent to child products.
     * Inserts history record for current changes.
     *
     * @param clientId requester
     * @param product  product with full detalization structure inside
     * @return same saved product with updated version, status and productId
     */
    @Throws(PermissionException::class, NotFoundException::class)
    fun saveProduct(clientId: Long, product: ProductInfo): ProductInfo

    /**
     * get product without children
     *
     * @param myProfileId requester
     * @param productId   product id
     * @return product without nested content
     */
    fun getProductShortCurrent(myProfileId: Long, productId: Long): Product?

    fun getSimilarProducts(productId: Long, limit: Int): List<Product>

    fun getProducts(productsIds: List<Long>): List<ProductInfo>

    fun bindToProfile(productId: Long, referrerId: Long, showOnCampaign: Boolean, campaignIds: LongArray)

    fun getProductListForCampaignByReferrer(referrerId: Long, campaignId: Long, offset: Int, limit: Int): List<Product>

    fun getProductsCountByReferrer(referrerId: Long): Int

    @Throws(NotFoundException::class)
    fun cartItemToDTO(productId: Long, count: Int): Map<String, Any>

    fun getDonateProductByReferrer(profileId: Long): Long

    @Throws(NotFoundException::class, PermissionException::class)
    fun cloneProduct(clientId: Long, productId: Long): ProductInfo

    fun removeNewTagsFromOldProducts()
}

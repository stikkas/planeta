package ru.planeta.api.aspect.transaction

import java.lang.annotation.Retention
import java.lang.annotation.RetentionPolicy

/**
 * @author a.savanovich
 * @since 12/12/16 13:21 PM
 *
 * transaction is long. and we know it. Don't disturb with error log.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(AnnotationTarget.CLASS, AnnotationTarget.FILE, AnnotationTarget.FUNCTION, AnnotationTarget.PROPERTY_GETTER, AnnotationTarget.PROPERTY_SETTER)
annotation class LongTransactional

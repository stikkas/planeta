package ru.planeta.api.service.billing.order

import org.apache.commons.collections4.CollectionUtils
import org.apache.log4j.Logger
import org.springframework.context.annotation.Lazy
import org.springframework.stereotype.Service
import ru.planeta.api.exceptions.NotFoundException
import ru.planeta.api.exceptions.OrderException
import ru.planeta.api.exceptions.PermissionException
import ru.planeta.api.helper.ShareUtils
import ru.planeta.api.news.LoggerService
import ru.planeta.api.service.campaign.CampaignService
import ru.planeta.api.service.notifications.NotificationService
import ru.planeta.api.service.profile.ProfileSubscriptionService
import ru.planeta.api.service.promo.PromoLetterService
import ru.planeta.api.utils.OrderUtils
import ru.planeta.model.common.Order
import ru.planeta.model.common.OrderObject
import ru.planeta.model.enums.OrderObjectType
import ru.planeta.model.news.ProfileNews
import java.util.concurrent.Executors

/**
 *
 * Created by a.savanovich on 17.10.2016.
 */
@Service
class ShareAfterFundServiceImpl(
        @Lazy
        private val campaignService: CampaignService,
        @Lazy
        protected val notificationService: NotificationService,
        private val profileSubscriptionService: ProfileSubscriptionService,
        private val profileNewsService: LoggerService,
        @Lazy
        private val orderService: OrderService,
        private val promoLetterService: PromoLetterService) : AfterFundService {
    private val executorService = Executors.newSingleThreadExecutor()

    private val logger = Logger.getLogger(ShareAfterFundServiceImpl::class.java)

    @Throws(NotFoundException::class, PermissionException::class, OrderException::class)
    override fun purchase(order: Order, orderObjects: List<OrderObject>) {
        purchaseShares(order, orderObjects, order.buyerId)
    }

    override fun isMyType(type: OrderObjectType): Boolean {
        return ShareUtils.isShareType(type)
    }

    @Throws(PermissionException::class, NotFoundException::class, OrderException::class)
    private fun purchaseShares(order: Order, orderObjects: List<OrderObject>, clientId: Long) {
        val orderObject = orderObjects[0]
        val count = CollectionUtils.size(orderObjects)
        val share = campaignService.getShareReadyForPurchase(orderObject.objectId, count)
        val campaign = campaignService.getCampaignSafe(share.campaignId)
        profileSubscriptionService.subscribeSafe(order.buyerId, campaign.creatorProfileId)
        campaignService.updateAfterSharePurchase(campaign.campaignId, count, order.totalPrice, share.shareId)

        if (!OrderUtils.isGiftOrder(order)) {
            val orderInfo = orderService.getOrderInfo(order)
            notificationService.sendPurchaseShareNotificationEmail(orderInfo, share.rewardInstruction!!)


            executorService.execute {
                try {
                    promoLetterService.sendPromoLetter(orderInfo.buyerEmail
                            ?: "", order.totalPrice, campaign.tags, orderInfo.orderId)
                } catch (e: Exception) {
                    logger.error(e)
                }
            }
        }

        profileNewsService.addShareNews(ProfileNews.Type.SHARE_PURCHASED, clientId, share, campaign.campaignId)
    }

}

package ru.planeta.api.service.profile


import ru.planeta.model.commondb.QuickstartGetBook

/**
 * Created by kostiagn on 26.08.2015.
 */
interface QuickstartGetBookService {
    fun insertQuickstartGetBook(quickstartGetBook: QuickstartGetBook)

    fun alreadySendLastDay(email: String, type: String): Boolean

}

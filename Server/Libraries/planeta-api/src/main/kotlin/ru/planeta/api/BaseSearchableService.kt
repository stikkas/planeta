package ru.planeta.api

import org.springframework.beans.factory.annotation.Autowired
import ru.planeta.api.search.SearchService

/**
 * Base service for all services
 *
 * @author a.savanovich
 * Date: 01.02.13
 */
// IMPORTANT SearchService can't contains any service extends from this class
open class BaseSearchableService : BaseService() {

    @Autowired
    lateinit var searchService: SearchService
}

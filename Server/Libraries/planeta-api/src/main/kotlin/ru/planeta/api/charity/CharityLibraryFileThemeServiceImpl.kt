package ru.planeta.api.charity

import org.springframework.stereotype.Service
import ru.planeta.dao.charitydb.LibraryFileThemeDAO
import ru.planeta.model.charitydb.LibraryFileTheme
import ru.planeta.model.charitydb.LibraryFileThemeExample

@Service
class CharityLibraryFileThemeServiceImpl(private var libraryFileThemeMapper: LibraryFileThemeDAO) : CharityLibraryFileThemeService {

    override val orderByClause: String
        get() = "theme_id"

    override fun insertLibraryFileTheme(libraryFileTheme: LibraryFileTheme) {
        libraryFileThemeMapper.insertSelective(libraryFileTheme)
    }

    override fun insertLibraryFileThemeAllFields(libraryFileTheme: LibraryFileTheme) {


        libraryFileThemeMapper.insert(libraryFileTheme)
    }

    override fun updateLibraryFileTheme(libraryFileTheme: LibraryFileTheme) {

        libraryFileThemeMapper.updateByPrimaryKeySelective(libraryFileTheme)
    }

    override fun updateLibraryFileThemeAllFields(libraryFileTheme: LibraryFileTheme) {
        libraryFileThemeMapper.updateByPrimaryKey(libraryFileTheme)
    }

    override fun insertOrUpdateLibraryFileTheme(libraryFileTheme: LibraryFileTheme) {

        val selectedLibraryFileTheme = libraryFileThemeMapper.selectByPrimaryKey(libraryFileTheme.themeId)
        if (selectedLibraryFileTheme == null) {

            libraryFileThemeMapper.insertSelective(libraryFileTheme)
        } else {
            libraryFileThemeMapper.updateByPrimaryKeySelective(libraryFileTheme)
        }
    }

    override fun insertOrUpdateLibraryFileThemeAllFields(libraryFileTheme: LibraryFileTheme) {

        val selectedLibraryFileTheme = libraryFileThemeMapper.selectByPrimaryKey(libraryFileTheme.themeId)
        if (selectedLibraryFileTheme == null) {

            libraryFileThemeMapper.insert(libraryFileTheme)
        } else {
            libraryFileThemeMapper.updateByPrimaryKey(libraryFileTheme)
        }
    }

    override fun deleteLibraryFileTheme(id: Long?) {
        libraryFileThemeMapper.deleteByPrimaryKey(id)
    }

    override fun selectLibraryFileTheme(themeId: Long?): LibraryFileTheme {
        return libraryFileThemeMapper.selectByPrimaryKey(themeId)
    }

    private fun COMMENT_SECTION_LibraryFileTheme() {

    }

    override fun getLibraryFileThemeExample(offset: Int, limit: Int): LibraryFileThemeExample {
        val example = LibraryFileThemeExample()
        example.offset = offset
        example.limit = limit
        example.or()
        example.orderByClause = orderByClause
        return example
    }

    override fun selectLibraryFileThemeCount(): Int {
        return libraryFileThemeMapper.countByExample(getLibraryFileThemeExample(0, 0))
    }

    override fun selectLibraryFileThemeList(offset: Int, limit: Int): List<LibraryFileTheme> {
        return libraryFileThemeMapper.selectByExample(getLibraryFileThemeExample(offset, limit))
    }
}
package ru.planeta.moscowshow.model

import java.math.BigDecimal
import java.util.Date
import javax.xml.bind.annotation.XmlAccessType
import javax.xml.bind.annotation.XmlAccessorType
import javax.xml.bind.annotation.XmlRootElement

/**
 * Описание места в зале
 * User: Serge Blagodatskih<stikkas17></stikkas17>@gmail.com><br></br>
 * Date: 14.10.16<br></br>
 * Time: 14:14
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
class Place {

    /**
     * ID места в таблице std_positions (не используется)
     */
    val id: Int = 0

    /**
     * Название места (Обычно "Место 1", "Место 2", и т.п.)
     */
    val name: String? = null

    /**
     * Номер места
     */
    val position: Int = 0

    /**
     * Стоимость места
     */
    val price: BigDecimal? = null

    /**
     * X-координата на схеме зала
     */
    val x: Int = 0

    /**
     * Y-координата на схеме зала
     */
    val y: Int = 0

    /**
     * ID секции
     */
    val sectionId: Long = 0

    /**
     * ID ряда
     */
    val rowId: Long = 0

    /**
     * Состояние места
     * <dl>
     * <dt>0</dt><dd>свободно</dd>
     * <dt>1</dt><dd>недоступно к заказу</dd>
     * <dt>3</dt><dd>забронировано</dd>
     * <dt>5</dt><dd>неоплачено, валидировано 1 раз или более</dd>
     * <dt>4</dt><dd>оплачено, валидировано 1 раз</dd>
     * <dt>6</dt><dd>оплачено, валидировано более 1 раза</dd>
    </dl> *
     */
    val state: Int = 0

    /**
     * Только для суперпользователей: кем зарезервировано место, после валидации — описание результатов валидации
     */
    val description: String? = null

    /**
     * Только для суперпользователей: Дата валидации
     */
    val validationDate: Date? = null

    /**
     * Только для суперпользователей: Количество попыток валидации места
     */
    val validationCount: Short = 0

    /**
     * Ссылка на запись в таблице position
     */
    val positionId: Long = 0

    override fun toString(): String {
        return ("Place{" + "id=" + id + ", name=" + name
                + ", position=" + position + ", price=" + price
                + ", x=" + x + ", y=" + y + ", sectionId=" + sectionId
                + ", rowId=" + rowId + ", state=" + state + ", description="
                + description + ", validationDate=" + validationDate
                + ", validationCount=" + validationCount + ", positionId=" + positionId + '}'.toString())
    }

}

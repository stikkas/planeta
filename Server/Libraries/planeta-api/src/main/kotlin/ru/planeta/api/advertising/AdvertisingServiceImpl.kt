package ru.planeta.api.advertising

import org.apache.commons.lang3.StringUtils
import org.apache.commons.lang3.StringUtils.isEmpty
import org.springframework.stereotype.Service
import ru.planeta.api.exceptions.NotFoundException
import ru.planeta.api.service.billing.payment.system.yamoney.mws.utils.MarshallingUtils
import ru.planeta.dao.commondb.AdvertisingDAO
import ru.planeta.model.advertising.AdvertisingDTO
import ru.planeta.model.advertising.AdvertisingState
import ru.planeta.model.advertising.VastDTO
import ru.planeta.model.advertising.geenerated.ObjectFactory
import ru.planeta.model.advertising.geenerated.VAST
import java.io.StringWriter
import java.math.BigInteger
import java.util.*
import javax.validation.constraints.NotNull
import javax.xml.bind.JAXBContext
import javax.xml.bind.JAXBException
import javax.xml.datatype.DatatypeConfigurationException
import javax.xml.datatype.DatatypeConstants
import javax.xml.datatype.DatatypeFactory

/**
 * Created with IntelliJ IDEA.
 * Date: 20.01.14
 * Time: 14:18
 */
@Service
class AdvertisingServiceImpl(private var advertisingDAO: AdvertisingDAO) : AdvertisingService {

    override fun extractVast(advertisingDTO: AdvertisingDTO): VAST {
        val vastString = advertisingDTO.vastXml
        val vast: VAST
        if (isEmpty(vastString)) {
            val factory = ObjectFactory()
            vast = factory.createVAST()
        } else {
            vast = MarshallingUtils.unmarshal(VAST::class.java, vastString)
        }

        return vast
    }

    @Throws(JAXBException::class)
    override fun putVastString(vast: VAST, advertisingDTO: AdvertisingDTO) {
        val jaxbContext = JAXBContext.newInstance(VAST::class.java)
        val marshaller = jaxbContext.createMarshaller()
        val vastString = StringWriter()
        marshaller.marshal(vast, vastString)
        advertisingDTO.vastXml = vastString.toString()
    }

    override fun extractDTO(vast: VAST): VastDTO {
        val dto = VastDTO()
        if (vast.ad!!.size > 0) {
            val ad = vast.ad!![0]
            dto.adId = java.lang.Long.parseLong(ad.id!!)
            val inLine = ad.inLine
            dto.adName = inLine!!.adTitle
            dto.description = inLine.description
            val creative = inLine.creatives!!.creative!![0]
            val linear = creative.linear
            val mediaFile = linear!!.mediaFiles!!.mediaFile!![0]

            dto.deliveryType = mediaFile.delivery ?: ""
            dto.width = mediaFile.width!!.toInt()
            dto.height = mediaFile.height!!.toInt()
            dto.mediaType = mediaFile.type
            dto.videoUrl = mediaFile.value

            dto.isSkippable = StringUtils.isNotEmpty(linear.skipoffset)

            if (linear.duration == null) {
                dto.duration = 0
            } else {
                val calendar = linear.duration!!.toGregorianCalendar()
                calendar.timeZone = TimeZone.getTimeZone("0")
                val time = (calendar.timeInMillis * 0.001).toLong()
                dto.duration = time
            }

            val clickThrough = linear.videoClicks!!.clickThrough

            dto.redirectUrl = clickThrough!!.value
        }

        return dto
    }

    @Throws(DatatypeConfigurationException::class)
    override fun createNewVast(dto: VastDTO): VAST {
        val factory = ObjectFactory()

        val vast = factory.createVAST()
        vast.version = "2.0"
        val ads = vast.ad
        val ad = factory.createVASTAd()
        ad.id = dto.adId.toString()
        val inLine = factory.createVASTAdInLine()
        ad.inLine = inLine
        val adSystem = factory.createAdSystemType()
        adSystem.version = "3.0"
        adSystem.value = "planeta advertising system"
        inLine.adSystem = adSystem
        inLine.adTitle = dto.adName
        inLine.description = if (StringUtils.isEmpty(dto.description)) "" else dto.description

        val creatives = factory.createVASTAdInLineCreatives()
        val creativesList = creatives.creative
        val creative = factory.createVASTAdInLineCreativesCreative()
        creative.sequence = BigInteger.valueOf(1)
        creativesList!!.add(creative)
        inLine.creatives = creatives

        val linear = factory.createVASTAdInLineCreativesCreativeLinear()
        creative.linear = linear


        val calendar = GregorianCalendar()
        calendar.timeZone = TimeZone.getTimeZone("0")
        calendar.timeInMillis = dto.duration * 1000
        /*        System.out.println(calendar.getTimeInMillis());
        System.out.println(calendar.get(Calendar.HOUR));
        System.out.println(calendar.get(Calendar.MINUTE));*/

        val xmlGregorianCalendar = DatatypeFactory.newInstance().newXMLGregorianCalendar(calendar)
        xmlGregorianCalendar.timezone = DatatypeConstants.FIELD_UNDEFINED
        xmlGregorianCalendar.millisecond = DatatypeConstants.FIELD_UNDEFINED
        linear.duration = xmlGregorianCalendar

        if (dto.isSkippable) {
            //todo replace with dynamic time
            linear.skipoffset = "00:00:05"
        }

        /*        TrackingEventsType trackingEventsType = factory.createTrackingEventsType();
        List<TrackingEventsType.Tracking> trackings = trackingEventsType.getTracking();
        TrackingEventsType.Tracking tracking = factory.createTrackingEventsTypeTracking();
        tracking.setEvent("firstQuartile");
        tracking.setValue("some shit tracking value");
        trackings.add(tracking);
        linear.setTrackingEvents(trackingEventsType);*/

        val mediaFiles = factory.createVASTAdInLineCreativesCreativeLinearMediaFiles()
        val mediaFileList = mediaFiles.mediaFile
        val mediaFile = factory.createVASTAdInLineCreativesCreativeLinearMediaFilesMediaFile()
        mediaFile.delivery = dto.deliveryType
        //mediaFile.setBitrate(BigInteger.valueOf(512));
        mediaFile.width = BigInteger.valueOf(dto.width.toLong())
        mediaFile.height = BigInteger.valueOf(dto.height.toLong())
        mediaFile.type = if (StringUtils.isNotEmpty(dto.mediaType)) dto.mediaType else "video/mp4"
        mediaFile.value = dto.videoUrl
        mediaFileList!!.add(mediaFile)
        linear.mediaFiles = mediaFiles

        val videoClicksType = factory.createVideoClicksType()
        val clickThrough = factory.createVideoClicksTypeClickThrough()
        clickThrough.value = dto.redirectUrl
        videoClicksType.clickThrough = clickThrough

        linear.videoClicks = videoClicksType

        ads!!.add(ad)

        return vast
    }

    override fun saveAdvertising(dto: AdvertisingDTO) {
        advertisingDAO!!.saveAdvertising(dto)
    }

    @NotNull
    @Throws(NotFoundException::class)
    override fun loadLocalAdvertising(broadcastId: Long): AdvertisingDTO {
        return loadLocalAdvertising(broadcastId, null)
    }

    @NotNull
    @Throws(NotFoundException::class)
    private fun loadLocalAdvertising(broadcastId: Long, state: AdvertisingState?): AdvertisingDTO {
        val list = advertisingDAO!!.getAdvertising(broadcastId, 0, state!!)

        if (list.isEmpty()) {
            throw NotFoundException()
        }

        return list[0]
    }

    @Throws(NotFoundException::class)
    override fun loadActiveLocalAdvertising(broadcastId: Long): AdvertisingDTO {
        return loadLocalAdvertising(broadcastId, AdvertisingState.THIS)
    }

    override fun loadGlobalAdvertising(): List<AdvertisingDTO> {
        return advertisingDAO.getAdvertising(0, 0, null)
    }

    @Throws(NotFoundException::class)
    override fun loadActiveGlobalAdvertising(): AdvertisingDTO {
        val list = advertisingDAO!!.getAdvertising(0, 0, AdvertisingState.GLOBAL)

        if (list.size == 0) {
            throw NotFoundException()
        }

        return list[Random().nextInt(list.size)]
    }

    override fun deleteAdvertising(broadcastId: Long) {
        advertisingDAO.deleteAdvertising(broadcastId)
    }

    override fun startFinishByDateAdvertising() {
        var list = advertisingDAO.activeByDateAdvertising(Date())
        for (advertisingDTO in list) {
            advertisingDTO.dateTimeFrom = null
            advertisingDTO.dateTimeTo = null
            advertisingDTO.state = AdvertisingState.NONE
            advertisingDAO.saveAdvertising(advertisingDTO)
        }

        list = advertisingDAO.inactiveByDateAdvertising(Date())
        for (advertisingDTO in list) {
            if (advertisingDTO.broadcastId > 0) {
                advertisingDTO.state = AdvertisingState.THIS
            } else {
                advertisingDTO.state = AdvertisingState.GLOBAL
            }
            advertisingDAO.saveAdvertising(advertisingDTO)
        }
    }

    override fun updateAdvertisingWhenCampaignIsNotActive(campaignId: Long, isActive: Boolean) {
        val list = advertisingDAO.getAdvertisingByCampaign(campaignId)
        for (advertisingDTO in list) {
            if (isActive) {
                if (advertisingDTO.broadcastId > 0) {
                    advertisingDTO.state = AdvertisingState.THIS
                } else {
                    advertisingDTO.state = AdvertisingState.GLOBAL
                }
            } else {
                advertisingDTO.state = AdvertisingState.NONE
            }

            advertisingDAO.saveAdvertising(advertisingDTO)
        }
    }
}

package ru.planeta.api.mail

import org.apache.commons.lang3.StringEscapeUtils
import org.apache.commons.lang3.StringUtils
import org.apache.log4j.Logger
import org.apache.tools.ant.util.DateUtils
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.MessageSource
import org.springframework.stereotype.Repository
import ru.planeta.api.Utils.nvl
import ru.planeta.api.Utils.put
import ru.planeta.api.Utils.smap
import ru.planeta.api.helper.ProjectService
import ru.planeta.api.utils.PriorityUtils
import ru.planeta.commons.model.Gender
import ru.planeta.commons.web.WebUtils
import ru.planeta.commons.web.WebUtils.encodeUrl
import ru.planeta.model.bibliodb.Book
import ru.planeta.model.common.*
import ru.planeta.model.common.campaign.Campaign
import ru.planeta.model.common.campaign.CampaignTag
import ru.planeta.model.common.charity.CharityEventOrder
import ru.planeta.model.common.school.Seminar
import ru.planeta.model.enums.ProfileType
import ru.planeta.model.enums.ProjectType
import ru.planeta.model.mail.MailMessagePriority
import ru.planeta.model.profile.Comment
import ru.planeta.model.profile.Profile
import ru.planeta.model.promo.TechnobattleRegistration
import ru.planeta.model.shop.Product
import ru.planeta.model.shop.enums.DeliveryType
import ru.planeta.model.shop.enums.PaymentType
import java.math.BigDecimal
import java.math.RoundingMode
import java.text.SimpleDateFormat
import java.util.*
import javax.annotation.PreDestroy


/**
 * @author ds.kolyshev
 * Date: 19.01.12
 */
@Repository
class MailClientImpl(protected val messageSource: MessageSource,
                     private val projectService: ProjectService) : MailClient {

    @Value("\${product.format.tmpl}")
    private val productTemplate: String? = null


    private val executorService = PriorityUtils.newFixedThreadPoolPriorityExecutor(4)

    @Value("\${applicationName:}")
    private val applicationName: String? = null

    @Value("\${jetty.port:}")
    private val jettyport: String? = null

    // This variable is used for tests purposes only
    private var isAsynchronous = true

    private val appHostName = projectService.getHost(ProjectType.MAIN)
    private val appHost = projectService.getUrl(ProjectType.MAIN)
    private val appAdminHost = projectService.getUrl(ProjectType.ADMIN)
    private val appProjectsHost = projectService.getUrl(ProjectType.MAIN)
    private val appShopHost = projectService.getUrl(ProjectType.SHOP)
    private val appTVHost = projectService.getUrl(ProjectType.TV)
    private val baseHostUrl = projectService.getUrl(ProjectType.MAIN)
    @Value("\${payment.support.email}")
    private val paymentSupportEmail: String? = null

    private object Templates {

        const val REGISTRATION = "registration"
        const val AUTO_REGISTRATION = "auto.registration"
        const val REGISTRATION_RESEND = "registration.resend"
        const val PASSWORD_RECOVERY = "password.recovery"
        const val FEEDBACK = "feedback"
        const val FEEDBACK_QUICKSTART = "feedback.quickstart"
        const val ERROR_REPORT = "error.report"
        const val QUICKSTART_GET_BOOK = "quickstart.get.book"
        const val PAYMENT_ERROR = "payment.done.validate.error"
        const val PAYMENT_REJECT = "payment.done.validate.reject"

        const val CHANGE_MANAGER_FOR_CAMPAIGN = "campaign.change.manager"

        const val BALANCE_CREDITED_NOTICE = "balance.credited.notice"

        const val GIFT_MONEY_NOTICE = "gift.money.notice"

        const val DAILY_STARTED_CAMPAIGNS_NOTIFICATION = "daily.started.campaigns.notification"
        const val DAILY_FINISHED_CAMPAIGNS_NOTIFICATION = "daily.finished.campaigns.notification"

        const val ABOUT_US_ADVERTISING_CONTACT_FORM = "about.us.advertising.contact.form"

        const val ORDER_CANCELED = "order.canceled"

        object Start {
            /**
             * to user: Your created project in group. E-mail without attach
             */
            const val YOU_CREATED_GROUP_CAMPAIGN = "you.created.group.campaign"
            const val PROJECT_TRANSFERRED_TO_PATCH = "project.transferred.patch"

            /**
             * to projects manager/admins/authors: Created project in group
             */
            const val GROUP_CREATED_CAMPAIGN = "group.created.campaign"

            const val CAMPAIGN_FEEDBACK = "campaign.feedback"
            const val CAMPAIGN_DECLINED_EMAIL = "campaign.declined.email"
            const val CAMPAIGN_TWO_DAYS_END = "campaign.two.days.end"

            const val CURATOR_BONUS_ORDER_ADDED = "bonus.order.added"
            const val BUYER_BONUS_ORDER_CREATED = "bonus.order.created"
        }

        object Purchase {

            val CANCEL_PURCHASE_FOR_USER = "purchase.cancel.user"
            val CANCEL_PURCHASE_FOR_SUPPORT = "purchase.cancel.support"

            object Error {

                val CAMPAIGN_NOT_ACTIVE = "purchase.error.campaign.not.active"
                val NOT_ENOUGH_SHARE_AMOUNT = "purchase.error.not.enough.share.amount"
                val NOT_ENOUGH_PRODUCT_AMOUNT = "purchase.error.not.enough.product.amount"

                val SUPP_CAMPAIGN_NOT_ACTIVE = "support.purchase.error.campaign.not.active"
                val SUPP_NOT_ENOUGH_SHARE_AMOUNT = "support.purchase.error.not.enough.share.amount"
                val SUPP_NOT_ENOUGH_PRODUCT_AMOUNT = "support.purchase.error.not.enough.product.amount"
            }
        }

        object Shop {
            val SHOP_NEW_ORDER = "shop.order.added"
            val SHOP_NEW_COMMENT = "shop.new.comment"
        }

        interface TV {
            companion object {
                val YOUR_BROADCAST_LINK = "your.broadcast.link"
                val BROADCAST_EMAIL_SUBSCRIPTION = "broadcast.subscription.notification"
            }
        }

        interface School {
            companion object {
                val WEBINAR_REGISTRATION = "school.webinar"
                val ONLINE_COURSE_REQUEST = "school.course_request"
                val SOLOSIMPLE_REGISTRATION = "school.solosimple"
                val SOLOSIMPLE_REGISTRATION_OFFLINE = "school.solosimple.offline"
                val SOLO_REGISTRATION = "school.solo"
                val COMPANY_REGISTRATION = "school.company"
                val COMPANY_REGISTRATION_OFFLINE = "school.company.offline"
            }
        }

        interface Charity {
            companion object {
                val EVENT_ORDER = "charity.event.order"
            }
        }

        interface Investing {
            companion object {
                val INVOICE_REQUEST = "investing.invoice.request"
                val INVOICE_CREATED = "investing.invoice.created"
                val INVOICE_REQUEST_MANAGER = "investing.invoice.request.manager"
            }
        }

        interface Biblio {
            companion object {
                val INVOICE_CREATED = "biblio.invoice.created"
                val ORDER_PURCHASED = "biblio.order.purchased"
                val BOOK_REQUEST = "biblio.book.request"
                val BOOK_CHANGE_PRICE = "biblio.price.change"
                val LIBRARY_ACTIVE = "biblio.library.active"
            }
        }

        interface Promo {
            companion object {
                val TECHNOBATTLE_PROGRAMM = "promo.technobattle.programm"
                val TECHNOBATTLE_PROMO_CODES_IS_GOING_TO_END = "promo.technobattle.promo.codes.is.going.to.end"
                val TECHNOBATTLE_REGISTER_MANAGER = "promo.technobattle.register.manager"
                val TECHNOBATTLE_PROMO_CODE = "promo.technobattle.promocode"
            }
        }

    }

    private class Parameters : HashMap<String, String>() {

        fun add(name: String, value: Any?): Parameters {
            if (value != null) {
                put(name, value.toString())
            }
            return this
        }
    }

    @PreDestroy
    private fun shutdown() {
        executorService.shutdown()
    }


    override fun sendRegistrationCompleteEmail(email: String, password: String, regCode: String) {
        sendSafeHtmlEmail(Templates.REGISTRATION, email, smap(
                "password", password,
                "regCode", regCode,
                "url", appHost
        ), MailMessagePriority.HIGH)
    }

    override fun sendAutoRegistrationCompleteEmail(email: String, password: String, regCode: String) {
        sendSafeHtmlEmail(Templates.AUTO_REGISTRATION, email, smap(
                "password", password,
                "regCode", regCode,
                "url", appHost,
                "escapedRedirectUrl", encodeUrl("$appHost/?oldPassword=$password")
        ), MailMessagePriority.HIGH)
    }

    override fun sendRegistrationCompleteEmail(email: String, regCode: String) {
        sendSafeHtmlEmail(Templates.REGISTRATION_RESEND, email, smap(
                "regCode", regCode,
                "url", appHost
        ), MailMessagePriority.HIGH)
    }

    override fun sendPasswordRecoveryEmail(email: String, regCode: String, url: String, redirectUrl: String) {
        sendSafeHtmlEmail(Templates.PASSWORD_RECOVERY, email, smap(
                "regCode", regCode,
                "url", url,
                "redirectUrl", redirectUrl
        ), MailMessagePriority.HIGH)
    }

    override fun sendFeedbackEmail(email: String, message: String, theme: String, userId: Long) {
        sendSafeHtmlEmail(Templates.FEEDBACK, email, smap(
                "message", message,
                "theme", theme,
                "userId", userId

        ), MailMessagePriority.DEFAULT)
    }

    override fun sendFeedbackEmailQuickstart(email: String, message: String, theme: String) {
        sendSafeHtmlEmail(Templates.FEEDBACK_QUICKSTART, email, smap(
                "message", message,
                "theme", theme
        ), MailMessagePriority.DEFAULT)
    }

    override fun sendCampaignFeedbackEmail(email: String, message: String, campaign: Campaign, userEmail: String, userId: Long, userDisplayName: String, spam: Boolean?) {
        val templateName = Templates.Start.CAMPAIGN_FEEDBACK
        sendSafeHtmlEmail(templateName, userEmail, smap(
                "message", message,
                "authorEmail", email,
                "userDisplayName", userDisplayName,
                "userProfileId", userId,
                "campaignName", campaign.name as Any,
                "campaignAlias", campaign.webCampaignAlias as Any,
                "campaignId", campaign.campaignId,
                "isSpam", spam as Any
        ), MailMessagePriority.DEFAULT)
    }

    override fun sendChangeManagerForCampaignEmail(campaign: Campaign, to: String, comment: String, authorEmail: String) {
        val templateName = Templates.CHANGE_MANAGER_FOR_CAMPAIGN
        sendSafeHtmlEmail(templateName, to, smap(
                "campaignName", campaign.name as Any,
                "campaignId", campaign.campaignId,
                "comment", comment,
                "authorEmail", authorEmail
        ), MailMessagePriority.DEFAULT)
    }

    /**
     * Generates simple payment info.
     *
     * @param orderInfo order
     * @return payment type
     */
    private fun getPaymentTypeName(orderInfo: OrderInfo): String {
        val msgCode = if (PaymentType.CASH === orderInfo.paymentType) "order.payment.type.cash" else "order.payment.type.planeta"
        return getMessage(msgCode)
    }

    override fun sendCampaignNotActiveNotificationEmail(order: OrderInfo) {
        sendShareOrderPurchasingErrorNotificationEmail(Templates.Purchase.Error.CAMPAIGN_NOT_ACTIVE, order)
        sendSupportShareOrderPurchasingErrorNotificationEmail(Templates.Purchase.Error.SUPP_CAMPAIGN_NOT_ACTIVE, order)
    }

    override fun sendNotEnoughShareAmountNotificationEmail(order: OrderInfo) {
        sendShareOrderPurchasingErrorNotificationEmail(Templates.Purchase.Error.NOT_ENOUGH_SHARE_AMOUNT, order)
        sendSupportShareOrderPurchasingErrorNotificationEmail(Templates.Purchase.Error.SUPP_NOT_ENOUGH_SHARE_AMOUNT, order)
    }

    override fun sendNotEnoughProductAmountNotificationEmail(order: OrderInfo) {
        sendShareOrderPurchasingErrorNotificationEmail(Templates.Purchase.Error.NOT_ENOUGH_PRODUCT_AMOUNT, order)
        sendSupportShareOrderPurchasingErrorNotificationEmail(Templates.Purchase.Error.SUPP_NOT_ENOUGH_PRODUCT_AMOUNT, order)
    }

    private fun sendShareOrderPurchasingErrorNotificationEmail(templateName: String, order: OrderInfo) {
        val `object` = order.orderObjectsInfo[0]
        sendSafeHtmlEmail(templateName, order.buyerEmail, order.buyerGender, smap(
                "orderId", order.orderId,
                "userName", order.buyerName as Any,
                "shareName", `object`.objectName as Any,
                "campaignName", `object`.ownerName as Any,
                "groupName", `object`.merchantName as Any
        ), MailMessagePriority.DEFAULT)
    }

    private fun sendSupportShareOrderPurchasingErrorNotificationEmail(templateName: String, order: OrderInfo) {
        val `object` = order.orderObjectsInfo[0]
        sendSafeHtmlEmail(templateName, paymentSupportEmail, order.buyerGender, smap(
                "orderId", order.orderId,
                "userId", order.buyerId,
                "userName", order.buyerName as Any,
                "objectName", `object`.objectName as Any,
                "campaignId", `object`.ownerId,
                "campaignName", `object`.ownerName as Any
        ), MailMessagePriority.DEFAULT)
    }

    override fun sendBalanceCreditedNotificationEmail(email: String, displayName: String, amount: BigDecimal, respondentGender: Gender) {
        sendSafeHtmlEmail(Templates.BALANCE_CREDITED_NOTICE, email, respondentGender, smap(
                "userName", displayName,
                "amount", amount
        ), MailMessagePriority.DEFAULT)
    }

    override fun sendMailFromAboutUsAdvertising(senderEmail: String, message: String) {
        sendSafeHtmlEmail(Templates.ABOUT_US_ADVERTISING_CONTACT_FORM, senderEmail, smap(
                "message", message,
                "senderEmail", senderEmail
        ), MailMessagePriority.DEFAULT)
    }

    override fun sendErrorEmailReport(size: Int, errors: String) {
        var port = jettyport
        if (port == null) {
            port = "<Unknown>"
        }

        val jettyPort = port
        sendSafeHtmlEmail(Templates.ERROR_REPORT, DEV_TEAM_EMAIL, smap(
                "errors", errors,
                "errorsCount", size,
                "port", jettyPort,
                "applicationName", applicationName as Any
        ), MailMessagePriority.LOW)
    }

    override fun sendDailyCampaignsStartedEmail(email: String, campaigns: List<Campaign>) {
        sendSafeHtmlEmail(Templates.DAILY_STARTED_CAMPAIGNS_NOTIFICATION, email, smap("campaigns", formatCampaigns(campaigns, appHost)), MailMessagePriority.LOW)
    }

    override fun sendDailyCampaignsFinishedEmail(email: String, campaigns: List<Campaign>) {
        sendSafeHtmlEmail(Templates.DAILY_FINISHED_CAMPAIGNS_NOTIFICATION, email, smap("campaigns", formatCampaigns(campaigns, appHost)), MailMessagePriority.LOW)
    }

    override fun sendBonusOrderAddedNotifications(order: OrderInfo) {
        val bonusName = order.orderObjectsInfo.iterator().next().objectName
        // send buyer notifiacation
        sendSafeHtmlEmail(Templates.Start.BUYER_BONUS_ORDER_CREATED, order.buyerEmail, smap(
                "userName", order.buyerName as Any,
                "bonusName", bonusName as Any
        ), MailMessagePriority.DEFAULT)
        // send VIP curator notifiacation
        val params = getOrderCommonParams(order)
                .add("bonusName", bonusName)

        if (order.deliveryType === DeliveryType.CUSTOMER_PICKUP) {
            params.add("deliveryType", getMessage("order.delivery.type.pickup"))
        }

        sendSafeHtmlEmail(Templates.Start.CURATOR_BONUS_ORDER_ADDED, order.buyerEmail, params, MailMessagePriority.DEFAULT)
    }

    override fun sendShopNewOrderAddedNotification(orderInfo: OrderInfo, groupedOrderObjectsInfo: Map<Long, List<OrderObjectInfo>>) {
        var comment: String? = ""

        for ((_, list) in groupedOrderObjectsInfo) {
            if (!list.isEmpty()) {
                comment = list[0].comment
                break
            }
        }

        val params = getOrderCommonParams(orderInfo)
                .add("products", formatOrderObjects(groupedOrderObjectsInfo, productTemplate))
                .add("comment", comment)

        sendSafeHtmlEmail(Templates.Shop.SHOP_NEW_ORDER, orderInfo.buyerEmail, params, MailMessagePriority.DEFAULT)
    }

    private fun getOrderCommonParams(orderInfo: OrderInfo): Parameters {

        val linkedDelivery = orderInfo.linkedDelivery
        val deliveryAddress = orderInfo.deliveryAddress

        val params = Parameters()
                .add("orderId", orderInfo.orderId)
                .add("timeAdded", SimpleDateFormat("dd.MM.yy HH:mm").format(orderInfo.timeAdded))
                .add("buyerName", orderInfo.buyerName)
                .add("buyerEmail", orderInfo.buyerEmail)
                .add("deliveryPrice", orderInfo.deliveryPrice.setScale(2, BigDecimal.ROUND_HALF_UP))
                .add("totalPrice", orderInfo.totalPrice.setScale(2, BigDecimal.ROUND_HALF_UP))
                .add("paymentType", getPaymentTypeName(orderInfo))
                .add("deliveryType", if (linkedDelivery == null || StringUtils.isEmpty(linkedDelivery.name)) "" else linkedDelivery.name)
                .add("deliveryAddress", formatAddress(deliveryAddress))

        if (deliveryAddress != null) {
            params.add("addresseeName", nvl(deliveryAddress.fio, StringUtils.EMPTY))
                    .add("addresseeSurname", StringUtils.EMPTY)
                    .add("comment", StringUtils.EMPTY)
                    .add("buyerPhone", nvl(deliveryAddress.phone, StringUtils.EMPTY))
        } else {
            params.add("addresseeName", StringUtils.EMPTY)
                    .add("addresseeSurname", StringUtils.EMPTY)
                    .add("comment", StringUtils.EMPTY)
                    .add("buyerPhone", StringUtils.EMPTY)
        }
        return params
    }

    private fun sendCampaignChangedEmailNotification(templateName: String, email: String, group: Profile, campaign: Campaign, requester: Profile, imageUrl: String, campaignManager: PlanetaManager) {
        val params = smap(
                "userName", requester.displayName as Any,
                "groupId", group.profileId as Any,
                "groupName", group.displayName as Any,
                "campaignId", campaign.campaignId as Any,
                "campaignAlias", campaign.webCampaignAlias as Any,
                "campaignName", campaign.name as Any,
                "campaignShortDescription", campaign.shortDescription as Any,
                "targetAmount", ru.planeta.commons.lang.StringUtils.humanNumber(campaign.targetAmount!!.toLong().toString()),
                "collectedAmount", ru.planeta.commons.lang.StringUtils.humanNumber(campaign.collectedAmount.toLong().toString()),
                "completionPercentage", campaign.completionPercentage.toLong(),
                "imageUrl", imageUrl
        )
        addManagerContacts(params.toMutableMap(), campaignManager)
        sendSafeHtmlEmail(templateName, email, params, MailMessagePriority.DEFAULT)
    }

    override fun sendCampaignDeclinedEmail(email: String, campaign: Campaign, message: String) {
        sendSafeHtmlEmail(Templates.Start.CAMPAIGN_DECLINED_EMAIL, email, smap(
                "message", StringUtils.defaultIfBlank(StringEscapeUtils.unescapeHtml4(message), ""), "campaignName",
                campaign.name as Any, "campaignWebAlias", campaign.webCampaignAlias as Any
        ), MailMessagePriority.DEFAULT)
    }

    override fun sendToUserCreatedGroupCampaign(email: String, group: Profile, campaign: Campaign, requester: Profile, imageUrl: String, campaignManager: PlanetaManager) {
        sendCampaignChangedEmailNotification(Templates.Start.YOU_CREATED_GROUP_CAMPAIGN, email, group, campaign, requester, imageUrl, campaignManager)
    }

    override fun sendToUserTransferredToPatch(email: String, group: Profile, campaign: Campaign, requester: Profile, imageUrl: String, campaignManager: PlanetaManager) {
        sendCampaignChangedEmailNotification(Templates.Start.PROJECT_TRANSFERRED_TO_PATCH, email, group, campaign, requester, imageUrl, campaignManager)
    }

    override fun sendToManagerGroupCreatedCampaign(userEmail: String, profile: Profile, campaign: Campaign, campaignManager: PlanetaManager?) {
        logger.info("sendToManagerGroupCreatedCampaign: started.")

        val params = smap(
                "groupId", profile.profileId,
                "groupName", profile.displayName as Any,
                "campaignId", campaign.campaignId,
                "campaignAlias", campaign.webCampaignAlias as Any,
                "campaignName", campaign.name as Any,
                "campaignDescription", campaign.description as Any,
                "userOrGroup", if (profile.profileType === ProfileType.USER) "user" else "group"
        )
        sendSafeHtmlEmail(Templates.Start.GROUP_CREATED_CAMPAIGN, userEmail,
                addManagerContacts(params.toMutableMap(), campaignManager),
                MailMessagePriority.DEFAULT)
        logger.info("sendToManagerGroupCreatedCampaign: email sent to " + (if (campaignManager == null) "null" else campaignManager.email) + " from campaign " + campaign.campaignId)
    }

    override fun sendBroadcastLinkToUser(email: String, broadcastName: String, broadcastUrl: String, linkLifeTimeHours: Int) {
        sendSafeHtmlEmail(Templates.TV.YOUR_BROADCAST_LINK, email, smap(
                "broadcastUrl", broadcastUrl,
                "broadcastName", broadcastName,
                "hours", linkLifeTimeHours
        ), MailMessagePriority.DEFAULT)
    }

    override fun sendBroadcastSubscriptionNotification(email: String, broadcastName: String, broadcastUrl: String, userDisplayName: String) {
        sendSafeHtmlEmail(Templates.TV.BROADCAST_EMAIL_SUBSCRIPTION, email, smap(
                "broadcastUrl", broadcastUrl,
                "broadcastTitle", broadcastName,
                "userDisplayName", userDisplayName
        ), MailMessagePriority.DEFAULT)
    }

    private fun getCommonParams(email: String?, templateName: String, gender: Gender?, priority: MailMessagePriority?): Parameters {
        val params = Parameters()
                .add("userEmail", email)
                .add("escapedEmail", encodeUrl(email))
                .add("templateName", templateName)
                .add("appHost", appHost)
                .add("baseHostUrl", baseHostUrl)
                .add("appHostName", appHostName)
                .add("appAdminHost", appAdminHost)
                .add("appProjectsHost", appProjectsHost)
                .add("appShopHost", appShopHost)
                .add("appTVHost", appTVHost)
        if (gender != null) {
            params.add("dearByGender", getSafeMessage("dear.gender", "No gender", gender.code))
        }

        if (priority != null) {
            params.add("priority", priority)
        } else {
            logger.error("No priority set in $templateName")
        }

        return params
    }

    private fun getMessage(msgCode: String, vararg args: Any): String {
        return messageSource.getMessage(msgCode, args, Locale.getDefault())
    }

    private fun getSafeMessage(msgCode: String, defaultMessage: String, vararg args: Any): String {
        return messageSource.getMessage(msgCode, args, defaultMessage, Locale.getDefault())
    }

    override fun sendMessageRich(template: String, mailTo: String, mailFrom: String, emailSubject: String, emailMessage: String, priority: MailMessagePriority) {
        //template needed for logs
        sendSafe(Urls.SEND_MAIL, template, mailTo, null, Parameters()
                .add("template", template)
                .add("mailto", mailTo)
                .add("mailfrom", mailFrom)
                .add("subject", emailSubject)
                .add("message", emailMessage), priority)
    }

    private fun sendSafe(uri: String, templateName: String, mailTo: String?, gender: Gender?, params: Map<String, String>?, priority: MailMessagePriority) {
        val task = Runnable {
            try {
                val data = getCommonParams(mailTo, templateName, gender, priority)
                if (params != null) {
                    data.putAll(params)
                }
                logger.info("MailClient: try send email to $mailTo, template name is $templateName, priority is $priority")
                val response = WebUtils.uploadMap(projectService.getUrlHttp(ProjectType.NOTIFICATION_SERVICE, uri), data)
                logger.info("MailClient: response of sending to $mailTo: $response")
            } catch (ex: Exception) {
                logger.warn(String.format("MailClient: error sending %s notification", templateName), ex)
            }
        }
        if (isAsynchronous) {
            executorService.submit(task, priority.code)
        } else {
            task.run()
        }
    }

    private fun sendSafeHtmlEmail(templateName: String, email: String?, gender: Gender?, params: Map<String, String>?, priority: MailMessagePriority) {
        sendSafe(Urls.SEND_HTML, templateName, email, gender, params, priority)
    }

    fun sendSafeHtmlEmail(templateName: String, email: String?, params: Map<String, String>?, priority: MailMessagePriority) {
        sendSafeHtmlEmail(templateName, email, null, params, priority)
    }

    override fun sendOrderCancelledEmailToUser(email: String, profile: Profile, order: Order, reason: String, serviceText: String) {
        sendSafeHtmlEmail(Templates.ORDER_CANCELED, email, smap(
                "orderId", order.orderId,
                "reason", if (StringUtils.isBlank(reason)) "" else reason,
                "serviceText", if (StringUtils.isBlank(serviceText)) "" else serviceText,
                "webUserAlias", profile.webAlias
        ), MailMessagePriority.DEFAULT)
    }

    override fun sendQuickstartGetBookEmail(email: String, type: String) {
        val templateName = Templates.QUICKSTART_GET_BOOK
        sendSafeHtmlEmail(templateName, email, smap(
                "quickstartType", type
        ), MailMessagePriority.DEFAULT)
    }

    override fun setAsynchronous(isAsynchronous: Boolean) {
        this.isAsynchronous = isAsynchronous
    }

    override fun sendSchoolOnlineCourse(email: String, userName: String) {
        sendSafeHtmlEmail(Templates.School.ONLINE_COURSE_REQUEST, email, smap("userName", userName), MailMessagePriority.DEFAULT)
    }

    override fun sendSchoolWebinarRegistration(email: String, seminar: Seminar, seminarImageUrl: String) {
        sendSafeHtmlEmail(Templates.School.WEBINAR_REGISTRATION, email, smap("seminarName", seminar.getName() as Any,
                "seminarDateStart", SimpleDateFormat("dd.MM.yyyy").format(seminar.timeStart),
                "seminarTimeStart", SimpleDateFormat("HH:mm").format(seminar.timeStart),
                "seminarTimeEnd", SimpleDateFormat("HH:mm").format(org.apache.commons.lang3.time.DateUtils.addHours(seminar.timeStart!!, 2)),
                "seminarImageUrl", seminarImageUrl), MailMessagePriority.DEFAULT)
    }

    override fun sendSchoolSolosimpleRegistration(email: String, seminar: Seminar, seminarImageUrl: String) {
        sendSafeHtmlEmail(Templates.School.SOLOSIMPLE_REGISTRATION, email, smap("seminarName", seminar.getName() as Any,
                "seminarTimeStart", SimpleDateFormat("dd.MM.yyyy HH:mm").format(seminar.timeStart),
                "seminarVenue", seminar.getVenue() as Any,
                "seminarImageUrl", seminarImageUrl), MailMessagePriority.DEFAULT)
    }

    override fun sendSchoolSolosimpleOfflineRegistration(email: String, seminar: Seminar, seminarImageUrl: String) {
        sendSafeHtmlEmail(Templates.School.SOLOSIMPLE_REGISTRATION_OFFLINE, email, smap("seminarName", seminar.getName() as Any,
                "seminarTimeStart", SimpleDateFormat("dd.MM.yyyy HH:mm").format(seminar.timeStart),
                "seminarVenue", seminar.getVenue() as Any,
                "seminarImageUrl", seminarImageUrl), MailMessagePriority.DEFAULT)
    }

    override fun sendSchoolSoloRegistration(email: String, seminar: Seminar, seminarTag: CampaignTag, seminarImageUrl: String) {
        sendSafeHtmlEmail(Templates.School.SOLO_REGISTRATION, email, smap("seminarTag", seminarTag.name as Any,
                "senimatTimeStartSub14", SimpleDateFormat("dd.MM.yyyy HH:mm")
                .format(org.apache.commons.lang3.time.DateUtils.addDays(seminar.timeStart!!, -1)),
                "seminarImageUrl", seminarImageUrl), MailMessagePriority.DEFAULT)
    }

    override fun sendCharityEventOrder(email: String, charityEventOrder: CharityEventOrder) {
        sendSafeHtmlEmail(Templates.Charity.EVENT_ORDER, email, smap("profileId", charityEventOrder.profileId,
                "fio", charityEventOrder.fio as Any,
                "email", charityEventOrder.email as Any,
                "phone", charityEventOrder.phone as Any,
                "organizationName", charityEventOrder.organizationName as Any,
                "eventGoal", charityEventOrder.eventGoal as Any,
                "eventResult", charityEventOrder.eventResult as Any,
                "eventExpectedTime", charityEventOrder.eventExpectedTime as Any,
                "eventAudience", charityEventOrder.eventAudience as Any,
                "participantsCount", charityEventOrder.participantsCount,
                "eventFormat", charityEventOrder.eventFormat as Any), MailMessagePriority.DEFAULT)
    }

    override fun sendSchoolCompanyRegistration(email: String, seminar: Seminar, seminarImageUrl: String) {
        sendSafeHtmlEmail(Templates.School.COMPANY_REGISTRATION, email, smap("seminarName", seminar.getName() as Any,
                "seminarTimeStart", SimpleDateFormat("dd.MM.yyyy HH:mm").format(seminar.timeStart),
                "seminarVenue", seminar.getVenue() as Any,
                "seminarImageUrl", seminarImageUrl), MailMessagePriority.DEFAULT)
    }

    override fun sendSchoolCompanyOfflineRegistration(email: String, seminar: Seminar, seminarImageUrl: String) {
        sendSafeHtmlEmail(Templates.School.COMPANY_REGISTRATION_OFFLINE, email, smap("seminarName", seminar.getName() as Any,
                "seminarTimeStart", SimpleDateFormat("dd.MM.yyyy HH:mm").format(seminar.timeStart),
                "seminarVenue", seminar.getVenue() as Any,
                "seminarImageUrl", seminarImageUrl), MailMessagePriority.DEFAULT)
    }

    override fun sendInvestingInvoiceRequest(email: String, userName: String, campaignURL: String, campaignName: String) {
        sendSafeHtmlEmail(Templates.Investing.INVOICE_REQUEST, email,
                smap("userName", userName,
                        "campaignURL", campaignURL,
                        "campaignName", campaignName), MailMessagePriority.HIGH)
    }

    override fun sendInvestingInvoiceRequestManager(email: String, userURL: String, userName: String, campaignURL: String, campaignName: String, investInfo: String, requestURL: String) {
        sendSafeHtmlEmail(Templates.Investing.INVOICE_REQUEST_MANAGER, email,
                smap("userURL", userURL,
                        "userName", userName,
                        "campaignURL", campaignURL,
                        "campaignName", campaignName,
                        "investInfo", investInfo,
                        "requestURL", requestURL), MailMessagePriority.DEFAULT)
    }

    override fun sendInvestingInvoiceCreated(email: String, userName: String, campaignURL: String, campaignName: String, invoiceURL: String, invoiceId: Long) {
        sendSafeHtmlEmail(Templates.Investing.INVOICE_CREATED, email,
                smap("userName", userName,
                        "campaignURL", campaignURL,
                        "campaignName", campaignName,
                        "invoiceURL", invoiceURL,
                        "invoiceId", invoiceId), MailMessagePriority.HIGH)
    }

    override fun sendPaymentInvalidStateError(processorName: String, transaction: TopayTransaction) {
        sendSafeHtmlEmail(Templates.PAYMENT_ERROR, DEV_TEAM_EMAIL, smap("paymentProcessorName", processorName,
                "paymentDate", SimpleDateFormat("dd.MM.yyyy HH:mm").format(transaction.timeUpdated),
                "transactionId", transaction.transactionId), MailMessagePriority.LOW)
    }

    override fun sendProductComment(email: String, product: Product, comment: Comment) {
        sendSafeHtmlEmail(Templates.Shop.SHOP_NEW_COMMENT, email, smap(
                "productId", product.productId,
                "productName", product.name as Any,
                "commentText", comment.textHtml as Any
        ), MailMessagePriority.DEFAULT)
    }

    override fun sendPaymentInvalidStateReject(processorName: String, transaction: TopayTransaction) {
        sendSafeHtmlEmail(Templates.PAYMENT_REJECT, DEV_TEAM_EMAIL, smap("paymentProcessorName", processorName,
                "paymentDate", SimpleDateFormat("dd.MM.yyyy HH:mm").format(transaction.timeUpdated),
                "transactionId", transaction.transactionId), MailMessagePriority.LOW)
    }

    override fun sendBiblioInvoiceCreated(email: String, userName: String, invoiceURL: String, invoiceId: Long) {
        sendSafeHtmlEmail(Templates.Biblio.INVOICE_CREATED, email,
                smap("userName", userName,
                        "invoiceURL", invoiceURL,
                        "invoiceId", invoiceId), MailMessagePriority.HIGH)
    }

    override fun sendBiblioBookRequest(email: String, request: Book) {
        sendSafeHtmlEmail(Templates.Biblio.BOOK_REQUEST, email,
                smap("requestEmail", request.email as Any,
                        "requestTitile", request.title as Any,
                        "requestContact", request.contact as Any,
                        "requestSite", request.site as Any
                ), MailMessagePriority.DEFAULT)
    }

    override fun sendBiblioBookPriceChange(oldPrice: Int, newPrice: Int, changeDate: Date, bookName: String) {
        sendSafeHtmlEmail(Templates.Biblio.BOOK_CHANGE_PRICE, "biblio@planeta.ru",
                smap("oldprice", oldPrice,
                        "newprice", newPrice,
                        "bookname", bookName,
                        "date", SimpleDateFormat("dd.MM.yyyy HH:mm").format(changeDate)
                ), MailMessagePriority.DEFAULT)
    }

    override fun sendBiblioLibraryActive(email: String, changeDate: Date) {
        sendSafeHtmlEmail(Templates.Biblio.LIBRARY_ACTIVE, email,
                smap(
                        "date", SimpleDateFormat("dd.MM.yyyy HH:mm").format(changeDate)
                ), MailMessagePriority.DEFAULT)
    }

    override fun sendTechnobattleProgrammLetter(email: String) {
        sendSafeHtmlEmail(Templates.Promo.TECHNOBATTLE_PROGRAMM, email, null, MailMessagePriority.DEFAULT)
    }

    override fun sendTechnobattleManagerLetterAboutPromoCodesIsGoingToEnd(freeLitresPromoCodesCount: Int) {
        sendSafeHtmlEmail(Templates.Promo.TECHNOBATTLE_PROMO_CODES_IS_GOING_TO_END, "", smap("freeLitresPromoCodesCount", freeLitresPromoCodesCount), MailMessagePriority.DEFAULT)
    }

    override fun sendLitresPromoCodeLetter(email: String, promoCode: String) {
        sendSafeHtmlEmail(Templates.Promo.TECHNOBATTLE_PROMO_CODE, email, smap(
                "promoCode", promoCode
        ), MailMessagePriority.DEFAULT)
    }

    override fun sendTechnobattleRegistrationManager(email: String, request: TechnobattleRegistration) {
        sendSafeHtmlEmail(Templates.Promo.TECHNOBATTLE_REGISTER_MANAGER, email,
                smap("requestType", request.type,
                        "requestName", request.name as Any,
                        "requestEmail", request.email as Any,
                        "requestPhone", request.phone as Any,
                        "requestCity", request.city as Any,
                        "requestDescription", request.description as Any
                ), MailMessagePriority.DEFAULT)
    }

    override fun sendPromoEmail(email: String, template: String) {
        sendSafeHtmlEmail(template, email, null, MailMessagePriority.LOW)
    }

    override fun sendPromoEmail(email: String, template: String, params: Map<String, String>) {
        sendSafeHtmlEmail(template, email, params, MailMessagePriority.LOW)
    }

    override fun sendCampaignCampaignTwoDaysEnd(email: String, campaignName: String) {
        sendSafeHtmlEmail(Templates.Start.CAMPAIGN_TWO_DAYS_END, email,
                smap("campaignName", campaignName),
                MailMessagePriority.LOW)
    }

    override fun sendCancelPurchaseEmailForUser(mailTo: String, orderId: Long) {
        sendSafeHtmlEmail(
                Templates.Purchase.CANCEL_PURCHASE_FOR_USER,
                mailTo,
                smap("orderId", orderId),
                MailMessagePriority.DEFAULT
        )
    }

    override fun sendCancelPurchaseEmailForSupport(userEmail: String, campaignName: String, orderId: Long) {
        sendSafeHtmlEmail(
                Templates.Purchase.CANCEL_PURCHASE_FOR_SUPPORT,
                SUPPORT_EMAIL,
                smap("orderId", orderId, "userEmail", userEmail, "campaignName", campaignName),
                MailMessagePriority.DEFAULT
        )
    }

    companion object {

        private val logger = Logger.getLogger(MailClient::class.java)
        private val DEV_TEAM_EMAIL = "devteam@planeta.ru"
        private val SUPPORT_EMAIL = "support@planeta.ru"

        private fun formatOrderObjects(groupedOrderObjectsInfo: Map<Long, List<OrderObjectInfo>>, productTemplate: String?): String {
            val result = StringBuilder()

            var i = 1
            for (orderObjectsInfo in groupedOrderObjectsInfo.values) {
                val objectInfo = orderObjectsInfo.iterator().next()
                val formatted = String.format(productTemplate ?: "", i++, objectInfo.objectName, orderObjectsInfo.size,
                        objectInfo.price.setScale(2, RoundingMode.HALF_UP))
                result.append(formatted)
            }

            return result.toString()
        }

        private fun formatCampaigns(campaigns: List<Campaign>, appHost: String): String {
            val sb = StringBuilder()
            for (campaign in campaigns) {
                sb.append("[tr][td]").append(campaign.name).append("[/td]")
                sb.append("[td]")
                sb.append(DateUtils.format(campaign.timeFinish, "dd.MM.yyyy HH:mm"))
                sb.append("[/td][td]").append(appHost).append("/campaigns/").append(campaign.webCampaignAlias).append("[/td][/tr]")
            }
            return sb.toString()
        }

        private fun formatAddress(deliveryAddress: DeliveryAddress?): String {
            if (deliveryAddress == null) {
                return ""
            }

            val stringBuilder = StringBuilder()
            if (deliveryAddress.zipCode != null) {
                stringBuilder.append(deliveryAddress.zipCode)
            }
            if (deliveryAddress.country != null) {
                stringBuilder.append(", ")
                stringBuilder.append(deliveryAddress.country)
            }
            if (deliveryAddress.city != null) {
                stringBuilder.append(", ")
                stringBuilder.append(deliveryAddress.city)
            }
            if (deliveryAddress.address != null) {
                stringBuilder.append(", ")
                stringBuilder.append(deliveryAddress.address)
            }

            return stringBuilder.toString()
        }

        private fun addManagerContacts(params: MutableMap<String, String>, planetaManager: PlanetaManager?): Map<String, String> {
            if (planetaManager == null) {
                logger.warn("Passed manager is null")
                return params
            }
            return put(params,
                    "managerEmail", planetaManager.email ?: "",
                    "managerName", planetaManager.name ?: "",
                    "managerDisplayName", planetaManager.fullName ?: ""
            )
        }
    }
}

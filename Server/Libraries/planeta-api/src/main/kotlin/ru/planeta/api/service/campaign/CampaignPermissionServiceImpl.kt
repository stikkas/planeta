package ru.planeta.api.service.campaign

import org.springframework.stereotype.Service
import ru.planeta.api.exceptions.MessageCode
import ru.planeta.api.exceptions.PermissionException
import ru.planeta.api.service.security.PermissionService
import ru.planeta.dao.commondb.CampaignPermissionDAO
import ru.planeta.model.common.campaign.Campaign
import ru.planeta.model.common.campaign.CampaignPermission
import ru.planeta.model.enums.PermissionLevel

import java.util.Date

/**
 * User: a.savanovich
 * Date: 17.12.13
 * Time: 15:29
 */
@Service
class CampaignPermissionServiceImpl(private var campaignPermissionDAO: CampaignPermissionDAO,
                                    private var permissionService: PermissionService) : CampaignPermissionService {

    override fun isEditable(clientId: Long, campaign: Campaign): Boolean {
        if (permissionService!!.hasAdministrativeRole(clientId)) {
            return true
        }

        return if (isAuthorOrAuthorAdmin(clientId, campaign.creatorProfileId)) {
            campaign.canChange() || isEditableByAuthor(campaign.campaignId)
        } else false

    }

    override fun isAuthorOrAuthorAdmin(clientId: Long, creatorProfileId: Long): Boolean {
        return clientId == creatorProfileId || permissionService!!.isProfileAdmin(clientId, creatorProfileId)
    }

    @Throws(PermissionException::class)
    override fun checkIsEditable(clientId: Long, campaign: Campaign) {
        if (!isEditable(clientId, campaign)) {
            throw PermissionException(MessageCode.PERMISSION_ADMIN)
        }
    }

    @Throws(PermissionException::class)
    override fun tooglePermission(editable: Boolean, campaignId: Long, managerId: Long) {
        permissionService.checkAdministrativeRole(managerId)
        if (editable) {
            val campaignPermission = CampaignPermission()
            campaignPermission.campaignId = campaignId
            campaignPermission.managerId = managerId
            campaignPermission.timeAdded = Date()
            campaignPermissionDAO.insert(campaignPermission)
        } else {
            campaignPermissionDAO.delete(campaignId)
        }
    }

    override fun isEditableByAuthor(campaignId: Long): Boolean {
        val campaignPermission = campaignPermissionDAO!!.select(campaignId) ?: return false
        return if (campaignPermission.writeLevelCode <= PermissionLevel.ADMINS.code) {
            true
        } else false
    }


    override fun canCreate(clientId: Long, creatorProfileId: Long): Boolean {
        return clientId == creatorProfileId ||
                permissionService.hasAdministrativeRole(clientId) ||
                permissionService.isProfileAdmin(clientId, creatorProfileId)

    }

    @Throws(PermissionException::class)
    override fun checkCanCreate(clientId: Long, creatorProfileId: Long) {
        if (!canCreate(clientId, creatorProfileId)) {
            throw PermissionException()
        }
    }
}

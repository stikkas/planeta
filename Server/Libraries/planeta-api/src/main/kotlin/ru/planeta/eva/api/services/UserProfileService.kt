package ru.planeta.eva.api.services

import org.springframework.stereotype.Service
import ru.planeta.api.exceptions.NotFoundException
import ru.planeta.dao.*
import ru.planeta.dao.commondb.ModerationMessageDAO
import ru.planeta.dao.commondb.UserPrivateInfoDAO
import ru.planeta.dao.maildb.MailUserDAO
import ru.planeta.dao.profiledb.NewsDAO
import ru.planeta.dto.MyProjectCardDTO
import ru.planeta.dto.MyPurchasedProjectCardDTO
import ru.planeta.entity.Profile
import ru.planeta.model.common.UserPrivateInfo
import ru.planeta.model.common.campaign.enums.CampaignStatus
import ru.planeta.model.common.campaign.enums.CampaignTargetStatus
import java.util.*

@Service
class UserProfileService(private val myProjectCardDAO: MyProjectCardDAO,
                         private val myPurchasedProjectCardDAO: MyPurchasedProjectCardDAO,
                         private val moderationMessageDAO: ModerationMessageDAO,
                         private val mailUserDAO: MailUserDAO,
                         private val newsDAO: NewsDAO,
                         private val purchaseDAO: PurchaseDAO,
                         private val userPrivateInfoDAO: UserPrivateInfoDAO,
                         private var repository: ProfileRepositoryDAO) {

    fun selectProfile(profileId: Long): Profile? {
        return repository.findOne(profileId)
    }

    fun saveProfile(profile: Profile) {
        val now = Date()
        profile.timeUpdated = now
        if (profile.profileId == null) {
            profile.timeAdded = now
            repository.insert(profile)
        } else {
            repository.update(profile)
        }
    }

    fun updateSubscriptions(profile: Profile) {
        saveProfile(profile)

        val privateInfo = userPrivateInfoDAO.selectByUserId(profile.profileId)
                ?: throw NotFoundException(UserPrivateInfo::class.java, profile.profileId)

        if (profile.isReceiveNewsletters) {
            privateInfo.email?.let {
                mailUserDAO.forceSubscribeUserByEmail(it)
            }
        } else {
            privateInfo.email?.let {
                mailUserDAO.unsubscribeUserByEmail(it)
            }
        }
    }

    fun getMyProjects(profileId: Long): List<MyProjectCardDTO> {
        val campaigns = myProjectCardDAO.selectByProfileId(profileId)
        campaigns.forEach { cmp ->
            val campaignId = cmp.campaignId ?: throw NotFoundException("campaign_without_id")
            if (cmp.status == CampaignStatus.DECLINED) {
                cmp.declineCase = moderationMessageDAO.selectLastCampaignModerationMessage(campaignId).message
            } else if (cmp.status == CampaignStatus.PAUSED) {
                cmp.pauseCase = moderationMessageDAO.selectLastCampaignModerationMessage(campaignId).message
            }
            if (cmp.webCampaignAlias == null) {
                cmp.webCampaignAlias = "$campaignId"
            }
        }
        return campaigns
    }

    fun getMyProjectsStats(campaignIds: List<Long>) = myProjectCardDAO.getStatistics(campaignIds)

    fun getMyPurchasedProjects(myProfileId: Long, status: CampaignStatus, targetStatus: CampaignTargetStatus?, offset: Long, limit: Int): List<MyPurchasedProjectCardDTO> {
        val campaigns = myPurchasedProjectCardDAO.select(myProfileId, status, targetStatus, offset, limit)
        campaigns.forEach { cmp ->
            val campaignId: Long = cmp.campaignId ?: throw NotFoundException("campaign_without_id")
            if (cmp.webCampaignAlias == null) {
                cmp.webCampaignAlias = "$campaignId"
            }
            val news = newsDAO.selectLastCampaignNews(campaignId)
            if (news != null) {
                cmp.newsId = news.newsId
                cmp.newsTitle = news.newsTitle
                cmp.newsTimeAdded = news.newsTimeAdded
            }
            cmp.shares = purchaseDAO.selectMyPurchasesByCampaignId(myProfileId, campaignId)
        }
        return campaigns
    }
}


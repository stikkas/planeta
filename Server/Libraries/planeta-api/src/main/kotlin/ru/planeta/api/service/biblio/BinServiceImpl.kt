package ru.planeta.api.service.biblio

import org.springframework.context.annotation.Scope
import org.springframework.context.annotation.ScopedProxyMode
import org.springframework.stereotype.Service
import org.springframework.web.context.WebApplicationContext
import ru.planeta.model.bibliodb.Bin
import ru.planeta.dao.bibliodb.BinDAO

import java.util.Date

/**
 * WARNING not use Autowiring inside. This is session data only
 *
 * @author stikkas<stikkas></stikkas>@yandex.ru>
 */
@Service
@Scope(value = WebApplicationContext.SCOPE_SESSION, proxyMode = ScopedProxyMode.TARGET_CLASS)
class BinServiceImpl : BinService {

    private var bin: Bin? = null

    override fun save(bin: Bin, binDAO: BinDAO) {
        if (bin.profileId <= 0) {
            this.bin = bin
            return
        }
        val _bin = bin
        if (_bin == null || _bin.hasChanges(bin)) {
            if (_bin.id > 0) {
                binDAO.deleteRelations(_bin)
            } else {
                binDAO.insertBin(_bin)
            }
            binDAO.insertBookRelations(_bin)
            binDAO.insertLibraryRelations(_bin)
            binDAO.updateBin(_bin)
            this.bin = _bin
        }
    }

    override fun clear(bin: Bin, binDAO: BinDAO) {
        binDAO.deleteRelations(bin)
        this.bin = null
    }

    override fun get(profileId: Long, binDAO: BinDAO): Bin {
        var bin = this.bin
        if (bin == null) {
            if (profileId > 0) {
                bin = binDAO.findByProfileId(profileId)
            }
            if (bin == null) {
                bin = Bin()
                if (profileId > 0) {
                    bin.profileId = profileId
                    binDAO.insertBin(bin)
                }
            }
        } else if (profileId > 0 && bin!!.profileId != profileId) {
            if (bin.isEmpty) {
                val b = binDAO.findByProfileId(profileId)
                if (b != null) {
                    bin = b
                } else {
                    bin.profileId = profileId
                    if (bin!!.id > 0) {
                        updateBin(bin, binDAO)
                    } else {
                        binDAO.insertBin(bin)
                    }
                }
            } else {
                val b = binDAO.findByProfileId(profileId)
                if (b != null) {
                    binDAO.deleteBin(b)
                }
                bin.profileId = profileId
                if (bin.id > 0) {
                    updateBin(bin, binDAO)
                } else {
                    binDAO.insertBin(bin)
                }
            }
        }
        this.bin = bin
        return bin
    }

    private fun updateBin(bin: Bin, dao: BinDAO) {
        bin.timeUpdated = Date()
        dao.updateBin(bin)
    }

}

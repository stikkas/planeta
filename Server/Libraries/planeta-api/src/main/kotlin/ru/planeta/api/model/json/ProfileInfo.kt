package ru.planeta.api.model.json

import ru.planeta.model.enums.ProfileRelationStatus
import ru.planeta.model.profile.Profile

import java.util.EnumSet

/**
 * Incapsulates profile information
 *
 * @author ameshkov
 */
open class ProfileInfo {

    var profile: Profile? = null
    var profileRelationStatus: EnumSet<ProfileRelationStatus>? = null


    constructor() {
        profile = Profile()
    }

    constructor(profile: Profile) {
        this.profile = profile
    }

    override fun equals(o: Any?): Boolean {
        if (this === o) return true
        if (o !is ProfileInfo) return false
        val that = o as ProfileInfo?
        return profile!!.profileId == that!!.profile!!.profileId
    }

    override fun hashCode(): Int {
        return profile!!.profileId.toInt()
    }
}

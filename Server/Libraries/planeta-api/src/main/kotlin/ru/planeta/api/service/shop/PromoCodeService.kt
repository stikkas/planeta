package ru.planeta.api.service.shop

import ru.planeta.api.exceptions.NotFoundException
import ru.planeta.model.shop.Discount
import ru.planeta.model.shop.ExtendedPromoCode
import ru.planeta.model.shop.PromoCode

/**
 * Created with IntelliJ IDEA.
 * User: michail
 * Date: 25.03.16
 * Time: 15:19
 * To change this template use File | Settings | File Templates.
 */
interface PromoCodeService {
    @Throws(NotFoundException::class)
    fun getPromoCode(code: String): PromoCode

    fun getPromoCodeIdByCode(code: String, alsoDisabled: Boolean): Long

    @Throws(NotFoundException::class)
    fun getPromoCodeById(promoCodeId: Long): PromoCode

    fun getExtendedPromoCodeById(promoCodeId: Long): ExtendedPromoCode

    fun getPromoCodesList(searchString: String, offset: Int, limit: Int): List<PromoCode>

    fun getExtendedPromoCodesList(searchString: String, offset: Int, limit: Int): List<ExtendedPromoCode>

    fun getExtendedPromoCodesListCount(searchString: String): Long

    fun insertPromoCode(promoCode: PromoCode)

    fun updatePromoCode(promoCode: PromoCode)

    fun markPromoCodeDisabled(promoCodeId: Long)

    fun markPromoCodeEnabled(promoCodeId: Long)

    fun insertOrUpdatePromoCode(promoCode: PromoCode)

    fun generateOne(promoCode: PromoCode)

    fun generateList(promoCode: PromoCode, count: Int)

    // generate object of promoCode, not string
    @Deprecated("")
    fun generateCode(codeLength: Int): String

    fun applyCode(clientId: Long, code: String, cartItems: Map<Long, Int>): Discount

    fun applyCode(clientId: Long, promoCodeId: Long, cartItems: Map<Long, Int>): Discount

    @Throws(NotFoundException::class)
    fun decreaseUsageCount(promoCodeId: Long)
}

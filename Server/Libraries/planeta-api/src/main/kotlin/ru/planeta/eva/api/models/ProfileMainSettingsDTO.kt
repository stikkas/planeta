package ru.planeta.eva.api.models

import ru.planeta.commons.model.Gender
import java.util.*

class ProfileMainSettingsDTO {
    var displayName: String = ""
    var alias: String? = null
    var userGender = Gender.NOT_SET
    var userBirthDate: Date? = null
    var cityId: Int = 0
    var countryId: Int = 0
    var phoneNumber: String? = null
    var summary: String? = null
}

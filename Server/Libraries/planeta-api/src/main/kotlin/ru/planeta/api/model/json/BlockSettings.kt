package ru.planeta.api.model.json

import ru.planeta.model.enums.BlockSettingType
import ru.planeta.model.enums.PermissionLevel

/**
 *
 * Class BlockSettings
 * @author a.tropnikov
 */
class BlockSettings {

    var profileId: Long = 0
    var views: Map<BlockSettingType, PermissionLevel>? = null
    var comments: Map<BlockSettingType, PermissionLevel>? = null
    var writes: Map<BlockSettingType, PermissionLevel>? = null
    var creates: Map<BlockSettingType, PermissionLevel>? = null
    var names: Map<BlockSettingType, String>? = null
    var votes: Map<BlockSettingType, PermissionLevel>? = null
}

package ru.planeta.api.service.content

import ru.planeta.api.exceptions.NotFoundException
import ru.planeta.api.exceptions.PermissionException
import ru.planeta.model.profile.media.AudioTrack

/**
 *
 * @author s.fionov
 * Date: 28.10.11
 */
interface AudioService {

    /**
     * Gets specified profile audio tracks
     */
    fun getAudioTracks(clientId: Long, profileId: Long, offset: Int, limit: Int, searchString: String?): List<AudioTrack>

    /**
     * Gets specified audio track
     */
    fun getAudioTrack(clientId: Long, profileId: Long, trackId: Long): AudioTrack

    /**
     * Gets specified audio track
     */
    @Throws(NotFoundException::class)
    fun getAudioTrackSafe(clientId: Long, profileId: Long, trackId: Long): AudioTrack

    /**
     * Saves specified audio track.
     * Throws NotFoundException if such audio track does not exist.
     *
     */
    @Throws(NotFoundException::class, PermissionException::class)
    fun saveAudioTrack(clientId: Long, audioTrack: AudioTrack): AudioTrack

    /**
     * Gets audio tracks from the specified album
     *
     */
    fun getAudioAlbumTracks(clientId: Long, profileId: Long, albumId: Long, offset: Int, limit: Int): List<AudioTrack>

    /**
     * Gets audio tracks from the specified playlist (satisfy to the search string)
     */
    fun getPlaylistTracks(clientId: Long, profileId: Long, playlistId: Int, searchString: String): List<AudioTrack>


    /**
     * Gets the list of tracks by clientId and profileId. Skipping new track, that was
     * added at index = 0, and iterates this list, updating each track increasing AlbumTrackNumber by 1
     *
     */
    @Throws(PermissionException::class)
    fun switchTracksPositionsAfterAddTrack(clientId: Long, profileId: Long)
}

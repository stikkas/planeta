package ru.planeta.api.service.biblio

import org.springframework.stereotype.Service
import ru.planeta.api.exceptions.NotFoundException
import ru.planeta.api.service.notifications.NotificationService
import ru.planeta.model.bibliodb.Book
import ru.planeta.model.bibliodb.Library
import ru.planeta.model.bibliodb.enums.BiblioObjectStatus

/**
 *
 * User: Serge Blagodatskih<stikkas17></stikkas17>@gmail.com><br></br>
 * Date: 27.04.16<br></br>
 * Time: 15:03
 */
@Service
class RequestServiceImpl(private val libraryService: LibraryService,
                         private val notificationService: NotificationService,
                         private val bookService: BookService) : RequestService {

    @Throws(NotFoundException::class)
    override fun saveLibraryRequest(library: Library) {
        libraryService.save(library, notificationService)
    }

    override fun saveBookRequest(book: Book) {
        book.status = BiblioObjectStatus.REQUEST
        bookService.saveBook(book)
    }
}

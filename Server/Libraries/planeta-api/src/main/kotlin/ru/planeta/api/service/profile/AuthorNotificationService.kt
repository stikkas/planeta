package ru.planeta.api.service.profile

import org.apache.commons.codec.digest.DigestUtils
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import ru.planeta.api.helper.ProjectService
import ru.planeta.commons.web.WebUtils
import ru.planeta.model.enums.ProjectType

/**
 * Date: 12.05.2015
 * Time: 17:53
 */
@Service
class AuthorNotificationService @Autowired
constructor(private val projectService: ProjectService) {

    fun getUnsubscribeUrl(id: Long): String {
        return WebUtils.Parameters().add("id", id).add("hash", getSubscriptionsKey(id))
                .createUrl(projectService.getUrl(ProjectType.MAIN, "/author-unsubscribe"), false)
    }

    companion object {
        private val SALT = "SALTISGOOD"

        fun getSubscriptionsKey(id: Long): String {
            return DigestUtils.md5Hex(SALT + id.toString() + SALT)
        }
    }

}

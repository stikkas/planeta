package ru.planeta.api.service.content

import ru.planeta.model.common.CachedVideo

/**
 * Created with IntelliJ IDEA.
 * User: connanedoile
 * Date: 4/10/12
 * Time: 11:32 AM
 */
interface VideoCacheService {

    /**
     *
     * @param url
     * @return
     */
    fun getVideo(url: String): CachedVideo

    /**
     *
     * @param cachedVideoId
     * @return
     */
    fun getVideo(cachedVideoId: Long): CachedVideo
}

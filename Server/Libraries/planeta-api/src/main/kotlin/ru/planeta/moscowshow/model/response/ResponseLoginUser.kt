package ru.planeta.moscowshow.model.response

import javax.xml.bind.annotation.XmlAccessType
import javax.xml.bind.annotation.XmlAccessorType
import javax.xml.bind.annotation.XmlRootElement
import ru.planeta.moscowshow.model.result.Result

/**
 *
 * User: Serge Blagodatskih<stikkas17></stikkas17>@gmail.com><br></br>
 * Date: 13.10.16<br></br>
 * Time: 10:12
 */
@XmlRootElement(name = "loginUserResponse")
@XmlAccessorType(XmlAccessType.FIELD)
class ResponseLoginUser : Response<Result>() {

    private val loginUserResult: LoginUserResult? = null

    val sessionId: String?
        get() = loginUserResult?.SessionID

    override var result: Result? = null
        get() = loginUserResult
}

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
class LoginUserResult : Result() {
    var SessionID: String? = null
}


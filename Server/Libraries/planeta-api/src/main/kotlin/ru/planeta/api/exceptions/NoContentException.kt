package ru.planeta.api.exceptions

import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ResponseStatus

/**
 * This exception is thrown when there is no content
 */
@ResponseStatus(value = HttpStatus.NO_CONTENT, reason = "No content")
class NoContentException : Exception {

    constructor() {}

    constructor(messageCode: MessageCode) : super(messageCode.errorPropertyName) {}
}

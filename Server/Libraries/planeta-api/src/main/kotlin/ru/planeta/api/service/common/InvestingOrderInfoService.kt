package ru.planeta.api.service.common

import java.util.Date

import ru.planeta.api.exceptions.NotFoundException
import ru.planeta.api.exceptions.PermissionException
import ru.planeta.model.common.InvestingOrderInfo
import ru.planeta.model.enums.ContractorType
import ru.planeta.model.enums.ModerateStatus

/**
 *
 * User: Serge Blagodatskih<stikkas17></stikkas17>@gmail.com><br></br>
 * Date: 28.01.16<br></br>
 * Time: 10:40
 */
interface InvestingOrderInfoService {

    /**
     * update current additional info of order or create new
     *
     * @param info additianl investing info of order
     * @throws PermissionException
     * @throws NotFoundException
     */
    @Throws(NotFoundException::class, PermissionException::class)
    fun save(info: InvestingOrderInfo)

    /**
     * find info of investing order
     *
     * @param userId id of the buyer
     * @param userType type of the buyer
     * @return found information
     */
    fun select(userId: Long, userType: ContractorType): InvestingOrderInfo

    /**
     * Search additional information of investing orders in date interval
     * @param status
     * @param start beginnig date interval
     * @param end ending date interval
     * @param offset start page (starts with 0)
     * @param limit page size
     * @return list of found informations
     */
    fun select(status: ModerateStatus, start: Date, end: Date, offset: Int, limit: Int): List<InvestingOrderInfo>

    /**
     * Search additional information of investing orders by ids
     * @param ids
     * @return list of found information
     */
    fun select(ids: List<Long>): List<InvestingOrderInfo>

    /**
     * get count of the investing information items
     * @return count
     */
    fun count(): Long

    /**
     * find info of investing order
     *
     * @param orderId id of the order
     * @return found information
     */
    fun select(orderId: Long): InvestingOrderInfo

    /**
     * deleteByProfileId info of order from db
     *
     * @param orderId id of order
     */
    fun delete(orderId: Long?)

    /**
     *
     * Смена статуса модерации
     *
     * @param orderId of order info
     * @param status
     * @throws PermissionException
     * @throws NotFoundException
     */
    @Throws(NotFoundException::class, PermissionException::class)
    fun changeModerateStatus(orderId: Long, status: ModerateStatus)
}

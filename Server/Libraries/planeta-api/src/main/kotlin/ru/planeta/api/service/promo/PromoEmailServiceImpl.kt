package ru.planeta.api.service.promo

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import ru.planeta.dao.trashcan.PromoEmailDAO
import ru.planeta.model.trashcan.PromoEmail

import java.util.Date

@Service
class PromoEmailServiceImpl @Autowired
constructor(private val promoEmailDAO: PromoEmailDAO) : PromoEmailService {

    override fun insert(promoEmail: PromoEmail) {
        promoEmail.timeSent = Date()
        promoEmailDAO.insert(promoEmail)
    }
}

package ru.planeta.api.service.common

import ru.planeta.model.common.WelcomeStats

interface StatsService {

    fun selectWelcomeStats(): WelcomeStats
}

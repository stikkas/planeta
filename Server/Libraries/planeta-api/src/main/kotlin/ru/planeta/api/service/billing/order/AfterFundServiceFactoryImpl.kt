package ru.planeta.api.service.billing.order

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import ru.planeta.model.enums.OrderObjectType

/**
 * Created by a.savanovich on 17.10.2016.
 */
@Service
class AfterFundServiceFactoryImpl : AfterFundServiceFactory {

    @Autowired
    private val services: List<AfterFundService>? = null

    override fun get(type: OrderObjectType): AfterFundService? {
        for (service in services!!) {
            if (service.isMyType(type)) {
                return service
            }
        }
        return null
    }
}

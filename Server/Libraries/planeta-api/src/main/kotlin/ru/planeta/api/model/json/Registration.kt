package ru.planeta.api.model.json

import com.fasterxml.jackson.annotation.JsonIgnore
import ru.planeta.commons.web.RegistrationData
import ru.planeta.commons.model.Gender

import java.util.Date

/**
 * Represents pending registration
 *
 * @author ameshkov
 */
class Registration {

    @get:JsonIgnore
    @set:JsonIgnore
    var registrationData = RegistrationData()
    var email: String? = null
    var password: String? = null
    var username: String? = null

    var firstName: String
        get() = registrationData.firstName
        set(firstName) {
            registrationData.firstName = firstName
        }

    var lastName: String
        get() = registrationData.lastName
        set(lastName) {
            registrationData.lastName = lastName
        }

    var gender: Gender
        get() = registrationData.gender
        set(gender) {
            registrationData.gender = gender
        }

    var birthday: Date
        get() = registrationData.userBirthDate
        set(birthday) {
            registrationData.userBirthDate = birthday
        }

    var avatarUrl: String
        get() = registrationData.photoUrl
        set(avatarUrl) {
            registrationData.photoUrl = avatarUrl
        }
}

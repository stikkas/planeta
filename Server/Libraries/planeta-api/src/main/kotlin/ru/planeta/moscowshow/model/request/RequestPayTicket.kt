package ru.planeta.moscowshow.model.request

import javax.xml.bind.annotation.XmlAccessType
import javax.xml.bind.annotation.XmlAccessorType
import javax.xml.bind.annotation.XmlRootElement

/**
 *
 * User: Serge Blagodatskih<stikkas17></stikkas17>@gmail.com><br></br>
 * Date: 13.10.16<br></br>
 * Time: 17:59
 */
@XmlRootElement(name = "payTicket")
@XmlAccessorType(XmlAccessType.FIELD)
class RequestPayTicket : Request {

    var ticketID: Long = 0

    constructor()

    constructor(ticketID: Long) {
        this.ticketID = ticketID
    }


}

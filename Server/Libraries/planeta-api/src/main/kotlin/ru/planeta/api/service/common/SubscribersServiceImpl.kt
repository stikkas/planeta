package ru.planeta.api.service.common

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import ru.planeta.dao.commondb.SubscribersDAO

/**
 * SubscribersService implementation.
 * Inserts new subscribers to the commondb.subscribers table.
 */
@Service
class SubscribersServiceImpl : SubscribersService {
    @Autowired
    private val subscribersDAO: SubscribersDAO? = null

    override fun subscribe(subscriptionCode: String, email: String) {
        if (isSubscribed(subscriptionCode, email)) {
            return
        }
        subscribersDAO!!.insert(subscriptionCode, email)
    }

    override fun isSubscribed(subscriptionCode: String, email: String): Boolean {
        return subscribersDAO!!.isSubscribed(subscriptionCode, email)
    }
}

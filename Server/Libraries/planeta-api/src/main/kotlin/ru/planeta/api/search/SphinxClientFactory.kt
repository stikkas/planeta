package ru.planeta.api.search

import org.sphx.api.SphinxClient
import org.sphx.api.SphinxException
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Component

/**
 * Factory for sphinx client.
 * For now -- just creates new sphinx client and connects to the specified host:port.
 *
 * TODO: Cache client in thread local var
 *
 * @author ameshkov
 */
@Component
class SphinxClientFactory {

    // <editor-fold defaultstate="collapsed" desc="Autowired">
    private var host: String? = null
    private var port: Int = 0
    // </editor-fold>

    /**
     * Creates new sphinx client
     * @return
     */
    val sphinxClient: SphinxClient
        @Throws(SphinxException::class)
        get() {
            val sphinxClient = SphinxClient(host, port)
            sphinxClient.SetConnectTimeout(10000)
            sphinxClient.SetRetries(3)
            return sphinxClient
        }

    @Value("\${sphinx.host:localhost}")
    fun setHost(host: String) {
        this.host = host
    }

    @Value("\${sphinx.port:9312}")
    fun setPort(port: Int) {
        this.port = port
    }
}

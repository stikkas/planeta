package ru.planeta.api.service.billing.payment

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import ru.planeta.api.exceptions.PermissionException
import ru.planeta.api.service.security.PermissionService
import ru.planeta.dao.commondb.BasePaymentErrorCommentsService
import ru.planeta.model.commondb.PaymentErrorComments

/**
 * Created by kostiagn on 17.08.2015.
 */

@Service
class PaymentErrorCommentsServiceImpl : BasePaymentErrorCommentsService(), PaymentErrorCommentsService {
    @Autowired
    private val permissionService: PermissionService? = null

    @Throws(PermissionException::class)
    override fun insertOrUpdatePaymentErrorComments(clientId: Long, paymentErrorId: Long, text: String) {
        permissionService!!.checkAdministrativeRole(clientId)
        super.insertOrUpdatePaymentErrorComments(PaymentErrorComments.builder
                .paymentErrorId(paymentErrorId)
                .text(text)
                .profileId(clientId)
                .build())
    }
}

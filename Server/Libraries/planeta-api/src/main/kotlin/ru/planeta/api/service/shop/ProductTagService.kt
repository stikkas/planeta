package ru.planeta.api.service.shop

import ru.planeta.model.shop.Category

interface ProductTagService {

    val productTagsCount: Int
    fun getProductTags(limit: Int, offset: Int): List<Category>

    fun getTagByMnemonicName(mnemonicName: String): Category

    fun getTagByMnemonicNameList(mnemonicNames: List<String>): List<Category>

    fun getProductTagById(categoryId: Long): Category

    fun saveProductTag(category: Category)
}

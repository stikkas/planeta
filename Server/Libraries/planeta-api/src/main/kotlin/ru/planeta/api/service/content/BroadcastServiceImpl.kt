package ru.planeta.api.service.content

import org.apache.commons.lang3.StringUtils
import org.apache.commons.lang3.SystemUtils
import org.apache.commons.lang3.RandomStringUtils
import org.apache.log4j.Logger
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service
import ru.planeta.api.exceptions.NotFoundException
import ru.planeta.api.exceptions.PermissionException
import ru.planeta.api.geo.GeoResolverWebService
import ru.planeta.api.mail.MailClient
import ru.planeta.api.model.json.BroadcastInfo
import ru.planeta.api.helper.ProjectService
import ru.planeta.api.service.billing.order.OrderService
import ru.planeta.api.service.configurations.StaticNodesService
import ru.planeta.api.service.security.PermissionService
import ru.planeta.commons.console.RuntimeHelper
import ru.planeta.commons.text.Bbcode
import ru.planeta.commons.web.WebUtils
import ru.planeta.dao.commondb.SequencesDAO
import ru.planeta.dao.profiledb.*
import ru.planeta.model.common.OrderInfo
import ru.planeta.model.common.OrderObjectInfo
import ru.planeta.model.enums.OrderObjectType
import ru.planeta.model.enums.ProjectType
import ru.planeta.model.profile.broadcast.*
import ru.planeta.model.profile.broadcast.enums.BroadcastCategory
import ru.planeta.model.profile.broadcast.enums.BroadcastRecordStatus
import ru.planeta.model.profile.broadcast.enums.BroadcastStatus
import ru.planeta.model.profile.broadcast.enums.BroadcastType
import ru.planeta.model.profile.location.City
import ru.planeta.model.profile.location.Country
import java.io.File
import java.io.IOException
import java.math.BigDecimal
import java.util.Date
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors
import java.util.concurrent.TimeoutException

/**
 * @author ds.kolyshev
 * Date: 19.03.12
 */
@Service
class BroadcastServiceImpl(
        protected val broadcastStreamDAO: BroadcastStreamDAO,
        protected val permissionService: PermissionService,
        protected val broadcastDAO: BroadcastDAO,
        private val broadcastGeoTargetingDAO: BroadcastGeoTargetingDAO,
        private val broadcastBackersTargetingDAO: BroadcastBackersTargetingDAO,
        private val broadcastProductTargetingDAO: BroadcastProductTargetingDAO,
        private val broadcastPrivateTargetingDAO: BroadcastPrivateTargetingDAO,
        protected val sequencesDAO: SequencesDAO,
        private val orderService: OrderService,
        private val projectService: ProjectService,

        @Value("\${rtmp.server.url}")
        private val rtmpServerUrl: String?,
        @Value("\${rtmp.dump.path}")
        private val rtmpDumpPath: String?,
        @Value("\${rtmp.erlyvideo.loginpasswords}")
        private val rtmpErlyVideoLoginPasswords: Array<String>,
        private val staticNodesService: StaticNodesService,
        private val geoResolverWebService: GeoResolverWebService,
        private val mailClient: MailClient) : BroadcastService {


    private val executorService = Executors.newFixedThreadPool(2)
    /**
     * Refreshes erlyvideo login-passwords file and runs update bash script
     */
    val erlyVideoAuthentication: String
        get() {
            val length = rtmpErlyVideoLoginPasswords?.size ?: 0
            val index = (Math.random() * (length - 1)).toInt()
            return rtmpErlyVideoLoginPasswords?.get(index) ?: ""
        }

    override fun getBroadcast(clientId: Long, broadcastId: Long): Broadcast? {
        return broadcastDAO!!.selectById(broadcastId)
    }

    @Throws(NotFoundException::class)
    override fun getBroadcastSafe(client: Long, broadcastId: Long): Broadcast {
        return getBroadcast(client, broadcastId) ?: throw NotFoundException(Broadcast::class.java, broadcastId)
    }

    override fun selectByIdList(idList: List<Long>): List<Broadcast> {
        return broadcastDAO!!.selectByIdList(idList)
    }

    override fun selectForAdmin(searchString: String, dateFrom: Date, dateTo: Date, offset: Long, limit: Int): List<Broadcast> {
        return broadcastDAO!!.selectForAdmin(searchString, dateFrom, dateTo, offset, limit)
    }

    override fun selectForAdminCount(searchString: String, dateFrom: Date, dateTo: Date): Long {
        return broadcastDAO!!.selectForAdminCount(searchString, dateFrom, dateTo)
    }


    override fun updateBroadcastViewsCount(defaultStreamId: Long) {
        broadcastDAO!!.updateBroadcastViewsCount(defaultStreamId)
    }

    @Throws(NotFoundException::class, PermissionException::class, IOException::class)
    override fun toggleBroadcastStreamState(clientId: Long, broadcastId: Long, streamId: Long) {
        val broadcast = getBroadcast(clientId, broadcastId) ?: throw NotFoundException(Broadcast::class.java, streamId)
        if (broadcast.broadcastStatus === BroadcastStatus.LIVE) {
            val stream = getBroadcastStream(clientId, streamId)
                    ?: throw NotFoundException(BroadcastStream::class.java, streamId)
            val status = if (BroadcastStatus.PAUSED === stream.broadcastStatus) BroadcastStatus.LIVE else BroadcastStatus.PAUSED
            stream.broadcastStatus = status
            saveBroadcastStream(clientId, stream)
        }
    }

    @Throws(PermissionException::class, NotFoundException::class)
    override fun saveBroadcast(clientId: Long, broadcast: Broadcast): Broadcast {
        checkBroadcastWritePermission(clientId)

        if (broadcast.broadcastId == 0L) {
            addBroadcast(clientId, broadcast)
            return broadcast
        }

        val currentBroadcast = broadcastDAO!!.selectById(broadcast.broadcastId)
                ?: throw NotFoundException(Broadcast::class.java, broadcast.broadcastId)

        currentBroadcast.name = broadcast.name
        currentBroadcast.description = broadcast.description
        currentBroadcast.descriptionHtml = broadcast.description
        currentBroadcast.imageId = broadcast.imageId
        currentBroadcast.imageUrl = broadcast.imageUrl
        currentBroadcast.isImageCustomDescription = broadcast.isImageCustomDescription
        currentBroadcast.imageDescription = broadcast.imageDescription
        currentBroadcast.imageDescriptionHtml = Bbcode.transform(broadcast.imageDescription)
        currentBroadcast.defaultStreamId = broadcast.defaultStreamId
        currentBroadcast.pausedImageId = broadcast.pausedImageId
        currentBroadcast.pausedImageUrl = broadcast.pausedImageUrl
        currentBroadcast.isPausedCustomDescription = broadcast.isPausedCustomDescription
        currentBroadcast.pausedDescription = broadcast.pausedDescription
        currentBroadcast.pausedDescriptionHtml = Bbcode.transform(broadcast.pausedDescription)
        currentBroadcast.closedImageId = broadcast.closedImageId
        currentBroadcast.closedImageUrl = broadcast.closedImageUrl
        currentBroadcast.isClosedCustomDescription = broadcast.isClosedCustomDescription
        currentBroadcast.closedDescription = broadcast.closedDescription
        currentBroadcast.closedDescriptionHtml = Bbcode.transform(broadcast.closedDescription)
        currentBroadcast.finishedImageId = broadcast.finishedImageId
        currentBroadcast.finishedImageUrl = broadcast.finishedImageUrl
        currentBroadcast.isFinishedCustomDescription = broadcast.isFinishedCustomDescription
        currentBroadcast.finishedDescription = broadcast.finishedDescription
        currentBroadcast.finishedDescriptionHtml = Bbcode.transform(broadcast.finishedDescription)
        currentBroadcast.isAutoSubscribe = broadcast.isAutoSubscribe
        currentBroadcast.isAutoStart = broadcast.isAutoStart
        currentBroadcast.isArchived = broadcast.isArchived
        currentBroadcast.isClosed = broadcast.isClosed
        currentBroadcast.broadcastCategory = broadcast.broadcastCategory
        currentBroadcast.isChatEnabled = broadcast.isChatEnabled
        currentBroadcast.isAnonymousCanView = broadcast.isAnonymousCanView
        currentBroadcast.chatFakeUsersCount = broadcast.chatFakeUsersCount
        if (!currentBroadcast.isVisibleOnDashboard && broadcast.isVisibleOnDashboard) {
            currentBroadcast.timeArchived = Date()
        }

        currentBroadcast.isVisibleOnDashboard = broadcast.isVisibleOnDashboard
        currentBroadcast.timeBegin = broadcast.timeBegin
        currentBroadcast.timeEnd = broadcast.timeEnd
        currentBroadcast.advertismentBannerHtml = broadcast.advertismentBannerHtml

        currentBroadcast.viewPermission = broadcast.viewPermission
        currentBroadcast.timeUpdated = Date()

        if (broadcast.timeEnd != null && broadcast.timeEnd!!.compareTo(Date()) > 0) {
            if (currentBroadcast.broadcastStatus === BroadcastStatus.FINISHED) {
                currentBroadcast.broadcastStatus = BroadcastStatus.NOT_STARTED
            } else {
                currentBroadcast.broadcastStatus = currentBroadcast.broadcastStatus
            }
        } else {
            currentBroadcast.broadcastStatus = BroadcastStatus.FINISHED
        }

        broadcastDAO.update(currentBroadcast)

        return currentBroadcast
    }

    @Throws(PermissionException::class)
    override fun deleteBroadcast(clientId: Long, broadcastId: Long) {

        checkBroadcastWritePermission(clientId)
        broadcastDAO!!.delete(broadcastId)

        //deleteByProfileId all streams
        broadcastStreamDAO!!.deleteByBroadcastId(broadcastId)

    }

    override fun getBroadcastStreams(clientId: Long, broadcastId: Long, offset: Int, limit: Int): List<BroadcastStream> {
        return broadcastStreamDAO!!.selectByBroadcastId(broadcastId, offset, limit)
    }

    override fun getBroadcastStream(clientId: Long, streamId: Long): BroadcastStream? {
        return broadcastStreamDAO!!.selectById(streamId)
    }

    @Throws(PermissionException::class, NotFoundException::class, IOException::class)
    override fun saveBroadcastStream(clientId: Long, broadcastStream: BroadcastStream): BroadcastStream {
        checkBroadcastWritePermission(clientId)

        if (broadcastStream.streamId == 0L) {
            addBroadcastStream(clientId, broadcastStream)
            return broadcastStream
        }

        val currentBroadcastStream = broadcastStreamDAO!!.selectById(broadcastStream.streamId)
                ?: throw NotFoundException(BroadcastStream::class.java, broadcastStream.streamId)

        if (currentBroadcastStream.broadcastRecordStatus === BroadcastRecordStatus.RECORD_FINISHED) {
            currentBroadcastStream.broadcastRecordStatus = BroadcastRecordStatus.WILL_BE_RECORD
        } else {
            currentBroadcastStream.broadcastRecordStatus = broadcastStream.broadcastRecordStatus
        }

        currentBroadcastStream.broadcastType = broadcastStream.broadcastType
        currentBroadcastStream.broadcastUrl = broadcastStream.broadcastUrl
        currentBroadcastStream.name = broadcastStream.name
        currentBroadcastStream.description = broadcastStream.description
        currentBroadcastStream.embedVideoHtml = broadcastStream.embedVideoHtml
        currentBroadcastStream.imageId = broadcastStream.imageId
        currentBroadcastStream.imageUrl = broadcastStream.imageUrl
        currentBroadcastStream.videoId = broadcastStream.videoId
        currentBroadcastStream.videoUrl = broadcastStream.videoUrl
        currentBroadcastStream.timeUpdated = Date()
        currentBroadcastStream.timeBegin = broadcastStream.timeBegin
        currentBroadcastStream.timeEnd = broadcastStream.timeEnd
        currentBroadcastStream.viewPermission = broadcastStream.viewPermission
        currentBroadcastStream.broadcastStatus = broadcastStream.broadcastStatus
        broadcastStreamDAO.update(currentBroadcastStream)

        return currentBroadcastStream
    }

    @Throws(PermissionException::class)
    override fun deleteBroadcastStream(clientId: Long, streamId: Long) {
        checkBroadcastWritePermission(clientId)
        broadcastStreamDAO!!.delete(streamId)
    }

    @Throws(PermissionException::class, NotFoundException::class)
    override fun pauseBroadcastStream(clientId: Long, profileId: Long, streamId: Long): BroadcastStream {

        checkBroadcastWritePermission(clientId)

        val currentBroadcastStream = broadcastStreamDAO!!.selectById(streamId)
                ?: throw NotFoundException(BroadcastStream::class.java, streamId)

        pauseBroadcastStream(currentBroadcastStream)

        return currentBroadcastStream

    }

    @Throws(IOException::class)
    override fun stopEndedBroadcasts(time: Long) {
        val broadcasts = broadcastDAO!!.selectEndedByTime(time)
        for (broadcast in broadcasts) {
            if (broadcast != null && broadcast.isAutoStart) {
                stopBroadcast(broadcast)
                logger.info("Broadcast with id=" + broadcast.broadcastId + " successfully stopped")
            }
        }
    }

    override fun liveStartedBroadcasts(time: Long) {
        val broadcasts = broadcastDAO!!.selectStartedByTime(time)
        for (broadcast in broadcasts) {
            if (broadcast != null && broadcast.isAutoStart) {
                playBroadcast(broadcast)
                logger.info("Broadcast with id=" + broadcast.broadcastId + " successfully started")
            }
        }
    }

    @Throws(PermissionException::class, NotFoundException::class)
    override fun playBroadcast(clientId: Long, broadcastId: Long): Broadcast {
        checkBroadcastWritePermission(clientId)
        val broadcast = broadcastDAO!!.selectById(broadcastId)
                ?: throw NotFoundException(Broadcast::class.java, broadcastId)

        playBroadcast(broadcast)
        return broadcast
    }

    @Throws(PermissionException::class, NotFoundException::class)
    override fun stopBroadcast(clientId: Long, broadcastId: Long): Broadcast {
        checkBroadcastWritePermission(clientId)

        val broadcast = broadcastDAO!!.selectById(broadcastId)
                ?: throw NotFoundException(Broadcast::class.java, broadcastId)

        stopBroadcast(broadcast)
        return broadcast
    }

    @Throws(PermissionException::class, NotFoundException::class)
    override fun makeBroadcastPrivate(clientId: Long, broadcastId: Long): Broadcast {
        checkBroadcastWritePermission(clientId)

        val broadcast = broadcastDAO!!.selectById(broadcastId)
                ?: throw NotFoundException(Broadcast::class.java, broadcastId)


        broadcast.isClosed = true
        broadcast.timeUpdated = Date()
        broadcastDAO.update(broadcast)

        return broadcast
    }

    @Throws(PermissionException::class, NotFoundException::class)
    override fun pauseBroadcast(clientId: Long, broadcastId: Long): Broadcast {
        checkBroadcastWritePermission(clientId)

        val broadcast = broadcastDAO!!.selectById(broadcastId)
                ?: throw NotFoundException(Broadcast::class.java, broadcastId)

        pauseBroadcast(broadcast)
        return broadcast
    }

    @Throws(PermissionException::class, NotFoundException::class)
    override fun setDefaultStreamId(clientId: Long, broadcastId: Long, streamId: Long) {
        checkBroadcastWritePermission(clientId)

        val broadcast = broadcastDAO!!.selectById(broadcastId)
                ?: throw NotFoundException(Broadcast::class.java, broadcastId)

        broadcast.defaultStreamId = streamId
        broadcastDAO.update(broadcast)
    }

    private fun playBroadcast(broadcast: Broadcast) {
        broadcast.broadcastStatus = BroadcastStatus.LIVE
        broadcast.timeUpdated = Date()
        broadcastDAO!!.update(broadcast)

        val streams = broadcastStreamDAO!!.selectByBroadcastId(
                broadcast.broadcastId, 0, 0)
        for (stream in streams) {
            playBroadcastStream(stream)
        }
        logger.info("Broadcast with id=" + broadcast.broadcastId + " successfully started")
    }

    private fun playBroadcastStream(stream: BroadcastStream) {
        if (stream.broadcastRecordStatus === BroadcastRecordStatus.WILL_BE_RECORD && stream.broadcastType === BroadcastType.VIDEO) {
            stream.broadcastRecordStatus = BroadcastRecordStatus.RECORDING
            startRtmpDump(stream)
        }
        //Here we can parse info from stream

        //Set stream to live
        stream.broadcastStatus = BroadcastStatus.LIVE
        stream.timeUpdated = Date()
        broadcastStreamDAO!!.update(stream)
    }

    private fun stopBroadcast(broadcast: Broadcast) {
        broadcast.broadcastStatus = BroadcastStatus.FINISHED
        broadcast.timeUpdated = Date()
        broadcastDAO!!.update(broadcast)

        val streams = broadcastStreamDAO!!.selectByBroadcastId(
                broadcast.broadcastId, 0, 0)
        for (stream in streams) {
            stopBroadcastStream(stream)
        }

        logger.info("Broadcast with id=" + broadcast.broadcastId + " successfully stopped")
    }

    private fun pauseBroadcast(broadcast: Broadcast) {
        broadcast.broadcastStatus = BroadcastStatus.PAUSED
        broadcast.timeUpdated = Date()
        broadcastDAO!!.update(broadcast)

        val streams = broadcastStreamDAO!!.selectByBroadcastId(
                broadcast.broadcastId, 0, 0)
        for (stream in streams) {
            pauseBroadcastStream(stream)
        }

        logger.info("Broadcast with id=" + broadcast.broadcastId + " successfully paused")
    }

    private fun stopBroadcastStream(stream: BroadcastStream) {
        if (stream.broadcastRecordStatus === BroadcastRecordStatus.RECORDING && stream.broadcastType === BroadcastType.VIDEO) {
            stream.broadcastRecordStatus = BroadcastRecordStatus.RECORD_FINISHED
            val videoFile = stopRtmpDump(stream)
            if (videoFile != null && videoFile.exists()) {
                scheduleSaveDumpVideo(stream, videoFile)
            }
        }

        //Set stream to Finished
        stream.broadcastStatus = BroadcastStatus.FINISHED
        stream.timeUpdated = Date()
        broadcastStreamDAO!!.update(stream)
    }

    private fun pauseBroadcastStream(stream: BroadcastStream) {
        stream.broadcastStatus = BroadcastStatus.PAUSED
        stream.timeUpdated = Date()
        broadcastStreamDAO!!.update(stream)
    }

    /**
     * Schedules video uploading
     */
    override fun scheduleSaveDumpVideo(stream: BroadcastStream, videoFile: File) {
        //saveDumpStreamVideo(stream, videoFile);
        executorService.submit { saveDumpStreamVideo(stream, videoFile) }
    }

    override fun checkBroadcastGeoTargeting(clientId: Long, profileId: Long, broadcastId: Long, ip: String): Boolean? {
        val allowedTargets = broadcastGeoTargetingDAO!!.selectBroadcastGeoTargeting(broadcastId, true)
        val disallowedTargets = broadcastGeoTargetingDAO.selectBroadcastGeoTargeting(broadcastId, false)

        if (allowedTargets.isEmpty() && disallowedTargets.isEmpty()) {
            return true
        }
        val countryId: Int
        val cityId: Int
        if (org.apache.commons.lang3.StringUtils.isNotBlank(ip)) {
            val city = geoResolverWebService!!.resolveCity(ip)
            if (city == null) {
                cityId = 0
                val country = geoResolverWebService.resolveCountry(ip)
                countryId = country?.countryId ?: 0
            } else {
                countryId = city.countryId
                cityId = city.objectId
            }

        } else {
            return false
        }
        return (allowedTargets.isEmpty() || containsCityOfCountry(allowedTargets, countryId.toLong(), cityId.toLong())) && (disallowedTargets.isEmpty() || !containsCityOfCountry(disallowedTargets, countryId.toLong(), cityId.toLong()))

    }

    override fun getBroadcastGeoTargeting(clientId: Long, broadcastId: Long): List<BroadcastGeoTargeting> {
        return broadcastGeoTargetingDAO!!.selectBroadcastGeoTargeting(broadcastId, null)
    }

    @Throws(PermissionException::class)
    override fun addBroadcastGeoTargeting(clientId: Long, broadcastGeoTargeting: BroadcastGeoTargeting) {
        checkBroadcastWritePermission(clientId)
        broadcastGeoTargetingDAO!!.insert(broadcastGeoTargeting)
    }

    @Throws(PermissionException::class)
    override fun removeBroadcastGeoTargeting(clientId: Long, broadcastId: Long) {
        checkBroadcastWritePermission(clientId)
        broadcastGeoTargetingDAO!!.delete(broadcastId)
    }

    @Throws(PermissionException::class)
    override fun removeBroadcastGeoTargetingByParams(clientId: Long, broadcastId: Long, countryId: Int, cityId: Int, allowed: Boolean) {
        checkBroadcastWritePermission(clientId)
        broadcastGeoTargetingDAO!!.deleteByCountryIdCityId(broadcastId, countryId, cityId, allowed)
    }

    override fun getPublicBroadcastInfo(broadcastId: Long): BroadcastInfo {
        val broadcast = getEventBroadcastWithoutPermissionCheck(broadcastId) ?: return BroadcastInfo()

        val broadcastInfo = BroadcastInfo()
        broadcastInfo.broadcast = broadcast

        val streams = broadcastStreamDAO.selectByBroadcastId(broadcast.broadcastId, 0, 0)
        removePrivateInfoFromStreams(streams)
        broadcastInfo.streams = streams

        return broadcastInfo
    }

    override fun getUpcomingBroadcasts(clientId: Long, dateTo: Date, offset: Int, limit: Int): List<Broadcast> {
        return getUpcomingBroadcasts(clientId, dateTo, true, offset, limit)
    }

    override fun getUpcomingBroadcasts(clientId: Long, dateTo: Date, onlyDashboard: Boolean, offset: Int, limit: Int): List<Broadcast> {
        return broadcastDAO!!.selectUpcomingBroadcasts(dateTo, onlyDashboard, offset, limit)
    }

    override fun getArchivedBroadcasts(broadcastCategory: BroadcastCategory, clientId: Long, dateTo: Date, isPopularSort: Boolean, offset: Int, limit: Int): List<Broadcast> {
        return broadcastDAO!!.selectArchivedBroadcasts(broadcastCategory, dateTo, isPopularSort, offset, limit)
    }

    @Throws(PermissionException::class, NotFoundException::class)
    override fun generatePrivateBroadcastLinks(clientId: Long, profileId: Long, broadcastId: Long, emails: List<String>, validInHours: Int) {
        checkBroadcastWritePermission(clientId)

        val broadcast = getBroadcast(clientId, broadcastId)
                ?: throw NotFoundException(Broadcast::class.java, broadcastId)

        var broadcastPrivateTargeting: BroadcastPrivateTargeting
        for (email in emails) {
            broadcastPrivateTargeting = BroadcastPrivateTargeting()
            broadcastPrivateTargeting.profileId = profileId
            broadcastPrivateTargeting.broadcastId = broadcastId
            broadcastPrivateTargeting.email = email

            broadcastPrivateTargeting.validInHours = validInHours
            broadcastPrivateTargeting.generatedLink = RandomStringUtils.randomAlphanumeric(GENERATED_RANDOM_LINK_LENGTH)

            broadcastPrivateTargetingDAO!!.insert(broadcastPrivateTargeting)
            mailClient!!.sendBroadcastLinkToUser(email, broadcast.name?:"",
                    constructPrivateBroadcastLink(broadcastId, broadcastPrivateTargeting.generatedLink
                            ?: ""), validInHours)
        }
    }

    override fun constructPrivateBroadcastLink(broadcastId: Long, generatedLink: String): String {
        return projectService!!.getUrl(ProjectType.TV, "/broadcast/$broadcastId/$generatedLink/private")
    }

    @Throws(PermissionException::class)
    override fun getBroadcastPrivateTargetings(clientId: Long, broadcastId: Long): List<BroadcastPrivateTargeting> {

        return broadcastPrivateTargetingDAO!!.selectBroadcastPrivateTargetings(broadcastId)
    }

    @Throws(PermissionException::class, NotFoundException::class)
    override fun validateBroadcastLink(clientId: Long, broadcastId: Long, generatedLink: String): Boolean {
        return !getBroadcastPrivateTargeting(clientId, broadcastId, generatedLink).isExpired
    }

    @Throws(PermissionException::class, NotFoundException::class)
    override fun getBroadcastPrivateTargeting(clientId: Long, broadcastId: Long, generatedLink: String): BroadcastPrivateTargeting {

        val broadcastPrivateTargeting = broadcastPrivateTargetingDAO!!.getBroadcastPrivateTargetingByLink(broadcastId, generatedLink)
                ?: throw NotFoundException("Broadcast Private Targeting not found: $generatedLink")

        // link is accessed for the first time
        if (broadcastPrivateTargeting.firstVisitTime == null) {
            broadcastPrivateTargeting.firstVisitTime = Date()
            broadcastPrivateTargetingDAO.update(broadcastPrivateTargeting)
        }
        return broadcastPrivateTargeting
    }

    @Throws(NotFoundException::class, PermissionException::class)
    override fun checkBroadcastBackersTargeting(clientId: Long, broadcastId: Long): Boolean? {
        val backersTargetings = broadcastBackersTargetingDAO!!.selectBroadcastBackersTargetings(broadcastId)
        if (backersTargetings == null || backersTargetings.isEmpty()) {
            return true
        }

        val buyerOrdersInfo = orderService!!.getBuyerOrdersInfo(clientId, clientId, OrderObjectType.SHARE, null, null, 0, 0)

        for (targeting in backersTargetings) {
            val campaignId = targeting.campaignId
            var campaignSumm = BigDecimal.ZERO
            for (orderInfo in buyerOrdersInfo) {
                val orderObjectsInfo = orderInfo.orderObjectsInfo
                if (orderObjectsInfo != null && !orderObjectsInfo.isEmpty()) {
                    for (orderObject in orderObjectsInfo) {
                        if (orderObject.ownerId == campaignId) {
                            campaignSumm = campaignSumm.add(orderInfo.totalPrice)
                            break
                        }
                    }
                }
            }

            if (campaignSumm.compareTo(targeting.limitSumm!!) >= 0) {
                return true
            }
        }

        return false
    }

    override fun getBroadcastBackersTargeting(clientId: Long, profileId: Long, broadcastId: Long): List<BroadcastBackersTargeting> {
        return broadcastBackersTargetingDAO!!.selectBroadcastBackersTargetings(broadcastId)
    }

    @Throws(PermissionException::class)
    override fun addBroadcastBackersTargeting(clientId: Long, broadcastBackersTargeting: BroadcastBackersTargeting) {
        checkBroadcastWritePermission(clientId)
        broadcastBackersTargetingDAO!!.insert(broadcastBackersTargeting)
    }

    @Throws(PermissionException::class)
    override fun removeBroadcastBackersTargeting(clientId: Long, broadcastId: Long, campaignId: Long) {
        checkBroadcastWritePermission(clientId)
        broadcastBackersTargetingDAO!!.delete(broadcastId, campaignId)
    }

    @Throws(PermissionException::class)
    override fun checkBroadcastProductTargeting(clientId: Long, broadcastId: Long): Boolean? {
        val productTargetings = broadcastProductTargetingDAO!!.selectBroadcastProductTargetings(broadcastId)
        if (productTargetings == null || productTargetings.isEmpty()) {
            return true
        }


        for (targeting in productTargetings) {
            if (orderService!!.isProfileHasPurchasedObject(clientId, targeting.productId, OrderObjectType.PRODUCT)) {
                return true
            }
        }

        return false
    }

    override fun getBroadcastProductTargeting(broadcastId: Long): List<BroadcastProductTargeting> {
        return broadcastProductTargetingDAO!!.selectBroadcastProductTargetings(broadcastId)
    }

    @Throws(PermissionException::class)
    override fun addBroadcastProductTargeting(clientId: Long, broadcastProductTargeting: BroadcastProductTargeting) {
        checkBroadcastWritePermission(clientId)
        broadcastProductTargetingDAO!!.insert(broadcastProductTargeting)
    }

    @Throws(PermissionException::class)
    override fun removeBroadcastProductTargeting(clientId: Long, broadcastProductTargeting: BroadcastProductTargeting) {
        checkBroadcastWritePermission(clientId)
        broadcastProductTargetingDAO!!.delete(broadcastProductTargeting)
    }

    /**
     * Uploads video file for broadcast stream
     */
    private fun saveDumpStreamVideo(stream: BroadcastStream, videoFile: File) {
        try {
            var staticNode = staticNodesService!!.staticNode
            staticNode = WebUtils.appendProtocolIfNecessary(staticNode, false)

            val urlString = (staticNode + "/uploadVideo?clientId=" + stream.profileId
                    + "&ownerId=" + stream.profileId
                    + "&broadcastStreamId=" + stream.streamId
                    + "&userfile1=" + stream.dumpFileName)
            logger.debug("Uploading dumped video file for " + stream.streamUrlId)
            logger.debug(urlString)
            val result = WebUtils.uploadMultipartStream(urlString, videoFile)
            logger.debug(result)
        } catch (ex: Exception) {
            logger.error(ex.toString())
        }

    }

    private fun addBroadcast(clientId: Long, broadcast: Broadcast): Broadcast {
        broadcast.authorProfileId = clientId
        broadcast.timeAdded = Date()
        broadcast.timeUpdated = Date()
        broadcast.descriptionHtml = Bbcode.transform(broadcast.description)
        broadcast.imageDescriptionHtml = Bbcode.transform(broadcast.imageDescription)
        broadcast.pausedDescriptionHtml = Bbcode.transform(broadcast.pausedDescription)
        broadcast.finishedDescriptionHtml = Bbcode.transform(broadcast.finishedDescription)
        broadcast.closedDescriptionHtml = Bbcode.transform(broadcast.closedDescription)
        if (broadcast.isVisibleOnDashboard) {
            broadcast.timeArchived = Date()
        }

        broadcastDAO!!.insert(broadcast)

        return broadcast
    }

    /**
     * Adds new broadcast stream
     *
     * @throws ru.planeta.api.exceptions.PermissionException
     * @throws ru.planeta.api.exceptions.NotFoundException
     */
    @Throws(PermissionException::class, NotFoundException::class)
    private fun addBroadcastStream(clientId: Long, broadcastStream: BroadcastStream): BroadcastStream {
        broadcastStream.authorProfileId = clientId
        broadcastStream.timeAdded = Date()
        broadcastStream.timeUpdated = Date()
        broadcastStream.streamId = sequencesDAO!!.selectNextLong(BroadcastStream::class.java)
        broadcastStream.broadcastUrl = ""
        broadcastStreamDAO!!.insert(broadcastStream)

        val broadcast = getBroadcast(clientId, broadcastStream.broadcastId)
        if (broadcast != null && broadcast.defaultStreamId == 0L) {
            setDefaultStreamId(clientId, broadcastStream.broadcastId, broadcastStream.streamId)
        }

        val loginPasswordQueryString = erlyVideoAuthentication
        val broadcastUrl = rtmpServerUrl + broadcastStream.streamUrlId + "?" + loginPasswordQueryString

        broadcastStream.broadcastUrl = broadcastUrl
        broadcastStreamDAO.update(broadcastStream)


        return broadcastStream
    }

    /**
     * Stops rtmp dump for stream
     */
    private fun stopRtmpDump(stream: BroadcastStream): File? {

        if (!SystemUtils.IS_OS_UNIX) {
            return null
        }

        val command = "/bin/bash " + System.getProperty("user.dir") + "/killrtmp.sh " + stream.streamUrlId
        logger.debug("Processing rtmp stop dump for " + stream.streamUrlId)
        logger.debug(command)

        var exitValue = -1
        try {
            exitValue = RuntimeHelper.executeCommandLine(command, STOP_DUMP_RUNTIME_TIMEOUT)

            val file = File(rtmpDumpPath!! + stream.dumpFileName)
            if (!file.parentFile.exists()) {
                val result = file.parentFile.mkdirs()
                if (!result) {
                    throw IOException("Folder exist")
                }
            }

            return file

        } catch (e: InterruptedException) {
            logger.error("InterruptedException", e)
        } catch (e: IOException) {
            logger.error("IOException", e)
        } catch (e: TimeoutException) {
            logger.error("TimeoutException", e)
        } finally {
            logger.debug("Exit value:$exitValue")
        }

        return null
    }

    /**
     * Starts rtmp dump for stream
     */
    private fun startRtmpDump(stream: BroadcastStream) {

        if (!SystemUtils.IS_OS_UNIX) {
            return
        }

        // startrtmp.sh rtmp_url destination
        val command = "/bin/bash " + System.getProperty("user.dir") + "/startrtmp.sh " + stream.broadcastUrl + " " + rtmpDumpPath + stream.dumpFileName
        logger.debug("Processing rtmp start dump for " + stream.streamUrlId)
        logger.debug(command)

        var exitValue = -1
        try {
            exitValue = RuntimeHelper.executeCommandLine(command)
        } catch (e: InterruptedException) {
            logger.error("InterruptedException", e)
        } catch (e: IOException) {
            logger.error("IOException", e)
        } catch (e: TimeoutException) {
            logger.error("TimeoutException", e)
        } finally {
            logger.debug("Exit value:$exitValue")
        }
    }

    @Throws(PermissionException::class)
    private fun checkBroadcastWritePermission(clientId: Long) {
        if (!permissionService!!.hasAdministrativeRole(clientId)) {
            throw PermissionException()
        }
    }

    /**
     * Returns broadcast for event with specified `profileId`
     * or null if no broadcast is found
     *
     * @param broadcastId event profile id
     * @return returns broadcast or null
     */
    private fun getEventBroadcastWithoutPermissionCheck(broadcastId: Long): Broadcast? {

        val broadcast = broadcastDAO!!.selectById(broadcastId)
        if (broadcast != null) {
            return broadcast
        }

        val broadcasts = broadcastDAO.selectByProfileId(broadcastId, 0, 1)
        return if (broadcasts != null && !broadcasts.isEmpty()) {
            broadcasts[0]
        } else null
    }

    companion object {

        private val STOP_DUMP_RUNTIME_TIMEOUT: Long = 2000000
        private val GENERATED_RANDOM_LINK_LENGTH = 15

        val logger = Logger.getLogger(BroadcastServiceImpl::class.java)


        private fun containsCityOfCountry(geoTargets: List<BroadcastGeoTargeting>, countryId: Long, cityId: Long): Boolean {
            for (targeting in geoTargets) {
                if (targeting.countryId != 0L && targeting.countryId == countryId && (targeting.cityId == 0L || targeting.cityId == cityId)) {
                    return true
                }
            }
            return false
        }

        private fun removePrivateInfoFromStreams(streams: List<BroadcastStream>) {
            for (stream in streams) {
                val endIndex = StringUtils.indexOf(stream.broadcastUrl, "?")
                stream.broadcastUrl = StringUtils.substring(stream.broadcastUrl, 0, endIndex)
            }
        }
    }

}

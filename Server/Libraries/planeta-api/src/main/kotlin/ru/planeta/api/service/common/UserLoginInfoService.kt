package ru.planeta.api.service.common

import ru.planeta.model.common.UserLoginInfo
import ru.planeta.model.enums.ProfileRelationStatus

import java.util.Date

/**
 * Service for working with UserLoginInfo
 *
 * User: sshendyapin
 * Date: 28.01.13
 * Time: 20:12
 */
interface UserLoginInfoService {

    /**
     * Add(insert) new UserLoginInfo if there is no such UserLoginInfo in our DB, if there is one, then
     * this method selects existing UserLoginInfo and updates it.
     *
     * @param userLoginInfo     UserLoginInfo to insert or update
     * @return                  returns <tt>1</tt> if insert/update was successful, otherwise returns <tt>0</tt>
     */
    fun addOrUpdateUserLoginInfo(userLoginInfo: UserLoginInfo): Int

    /**
     * Selects specified UserLoginInfo according to given profileId, userIpAddress and userAgent
     *
     * @param profileId         user's profileId
     * @param userIpAddress     user's IP address
     * @param userAgent         user's agent
     */
    fun getUserLoginInfo(profileId: Long, userIpAddress: String, userAgent: String): UserLoginInfo

    fun getLastUserLoginInfo(profileId: Long): UserLoginInfo
}

package ru.planeta.api.service.profile

import ru.planeta.model.commondb.SeminarRegistration

interface SeminarRegistrationService {
    fun insertOrUpdateSeminarRegistration(seminarRegistration: SeminarRegistration)

    fun selectSeminarRegistrationListBySeminarId(profileId: Long?, offset: Int, limit: Int): List<SeminarRegistration>
}

package ru.planeta.api.aspect.transaction

import org.apache.log4j.Logger
import org.aspectj.lang.ProceedingJoinPoint
import ru.planeta.dao.TransactionScope

/**
 *
 */
class NonTransactionalAspect {

    @Throws(Throwable::class)
    fun transaction(joinPoint: ProceedingJoinPoint): Any {
        if (TransactionScope.isInTestTransactionNesting) {
            log.error("NonTransactional in transaction " + joinPoint.signature.toLongString())
            throw RuntimeException("NonTransactional in transaction " + joinPoint.signature.toLongString())
        }

        return joinPoint.proceed()
    }

    companion object {

        private val log = Logger.getLogger(NonTransactionalAspect::class.java)
    }
}

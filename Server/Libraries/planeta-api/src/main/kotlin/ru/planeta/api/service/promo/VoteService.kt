package ru.planeta.api.service.promo

import ru.planeta.model.promo.TechnobattleVote
import ru.planeta.model.promo.VoteType

/**
 * Created with IntelliJ IDEA.
 * User: michail
 * Date: 16.01.2017
 * Time: 14:50
 */

interface VoteService {
    fun checkVoteExistingOrAddNewVote(userId: String, projectId: Long, voteType: VoteType): TechnobattleVote?

    fun getVotesCountForProject(projectId: Long): Int
}

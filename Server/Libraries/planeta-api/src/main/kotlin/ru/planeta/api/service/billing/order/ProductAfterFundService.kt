package ru.planeta.api.service.billing.order

import com.fasterxml.jackson.databind.ObjectMapper
import org.apache.commons.codec.binary.Base64
import org.apache.commons.collections4.CollectionUtils
import org.apache.log4j.Logger
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Lazy
import org.springframework.stereotype.Service
import ru.planeta.api.BaseService
import ru.planeta.api.exceptions.*
import ru.planeta.api.exceptions.MessageCode.NOT_ENOUGH_PRODUCT_AMOUNT
import ru.planeta.api.mail.MailClient
import ru.planeta.api.model.json.DeliveryInfo
import ru.planeta.api.service.campaign.DeliveryService
import ru.planeta.api.service.geo.DeliveryAddressService
import ru.planeta.api.service.geo.GeoService
import ru.planeta.api.service.notifications.NotificationService
import ru.planeta.api.service.shop.ProductUsersService
import ru.planeta.api.utils.OrderUtils
import ru.planeta.commons.onlinekassa.*
import ru.planeta.commons.web.WebUtils
import ru.planeta.dao.commondb.UserPrivateInfoDAO
import ru.planeta.dao.shopdb.ProductDAO
import ru.planeta.model.common.*
import ru.planeta.model.common.delivery.SubjectType
import ru.planeta.model.enums.OrderObjectType
import ru.planeta.model.enums.ProjectType
import ru.planeta.model.shop.Discount
import ru.planeta.model.shop.Product
import ru.planeta.model.shop.enums.DeliveryType
import ru.planeta.model.shop.enums.PaymentType
import java.math.BigDecimal
import java.util.*
import java.util.concurrent.Executors

/**
 * Created by a.savanovich on 17.10.2016.
 */
@Service
class ProductAfterFundService(
        private val productDAO: ProductDAO,
        private val productUsersService: ProductUsersService,
        @Lazy
        private val orderService: OrderService,
        @Lazy
        private val notificationService: NotificationService,
        private val mailClient: MailClient,
        private val deliveryService: DeliveryService,
        private val geoService: GeoService,
        private val deliveryAddressService: DeliveryAddressService,
        private val userPrivateInfoDAO: UserPrivateInfoDAO,
        @Value("\${cloudpayments.online.kassa.api.url}")
        private val cloudPaymentsOnlineKassaApiUrl: String,
        @Value("\${cloudpayments.online.kassa.inn}")
        private val cloudPaymentsOnlineKassaInn: String,
        @Value("\${cloudpayments.online.kassa.public.id}")
        private val cloudPaymentsOnlineKassaPublicId: String,
        @Value("\${cloudpayments.online.kassa.secret.key}")
        private val cloudPaymentsOnlineKassaSecretKey: String,
        @Value("\${cloudpayments.online.kassa.enabled:false}")
        private val isCloudPaymentsOnlineKassaEnabled: Boolean) : AfterFundService {

    private val executorService = Executors.newSingleThreadExecutor()

    private val credentials: String
        get() = Base64.encodeBase64String("$cloudPaymentsOnlineKassaPublicId:$cloudPaymentsOnlineKassaSecretKey".toByteArray())

    override fun isMyType(type: OrderObjectType): Boolean {
        return OrderObjectType.PRODUCT === type
    }

    @Throws(NotFoundException::class, PermissionException::class, OrderException::class)
    override fun purchase(order: Order, orderObjects: List<OrderObject>) {
        val objectsByProductId = groupOrderObjectsByObjectId(orderObjects)

        if (order.paymentType !== PaymentType.CASH) {
            try {
                removeOrderedProductFromStore(order.orderId)
            } catch (e: ChangeProductQuantityException) {
                throw OrderException(NOT_ENOUGH_PRODUCT_AMOUNT)
            }

        }

        for ((key, value) in objectsByProductId) {
            val count = CollectionUtils.size(value)
            productUsersService!!.updateProductAfterPurchase(order.buyerId, key, count)
        }

        if (order.paymentType !== PaymentType.CASH) {
            sendPurchaseProductNotificationEmail(order)
        }
    }

    private fun sendReceiptToOnlineKassa(order: OrderInfo, groupedOrderObjects: Map<Long, List<OrderObjectInfo>>): String {
        var answer = ""
        try {
            val items = ArrayList<CustomerReceiptItem>()
            for (orderObjectInfoList in groupedOrderObjects.values) {
                val item = CustomerReceiptItem()
                item.quantity = orderObjectInfoList.size
                item.price = orderObjectInfoList[0].price
                item.amount = orderObjectInfoList[0].price.multiply(BigDecimal.valueOf(orderObjectInfoList.size.toLong()))
                item.label = orderObjectInfoList[0].objectName
                item.vat = null
                items.add(item)
            }

            val userPrivateInfo = userPrivateInfoDAO!!.selectByUserId(order.buyerId)
            val customerReceipt = CustomerReceipt()
            customerReceipt.items = items
            customerReceipt.setTaxationSystem(TaxationSystem.LIGHT_DOHOD_RASHOD)
            if (userPrivateInfo!!.email != null) {
                customerReceipt.email = userPrivateInfo.email
            }

            val receipt = Receipt()
            receipt.inn = cloudPaymentsOnlineKassaInn
            receipt.invoiceId = order.orderId.toString()
            receipt.accountId = order.buyerId.toString()
            receipt.setType(ReceiptType.INCOME)
            receipt.customerReceipt = customerReceipt

            val objectMapper = ObjectMapper()
            val postData = objectMapper.writeValueAsString(receipt)
            log.info("Sending productInfo to onlinekassa: orderId=" + order.orderId + "; postData: " + postData)
            answer = WebUtils.uploadStringWithBasicHttpAauth(cloudPaymentsOnlineKassaApiUrl, postData, "application/json", "utf-8", credentials)
            log.info("Response from onlinekassa: orderId=" + order.orderId + "; answer: " + answer)
        } catch (ex: Exception) {
            log.error("Error during sending ProductInfo to onlinekassa: orderId=" + order.orderId + "; " + ex)
        }

        return answer
    }

    @Throws(PermissionException::class, NotFoundException::class)
    private fun sendPurchaseProductNotificationEmail(result: Order) {
        val orderInfo = orderService!!.getOrderInfo(result)
        val groupedOrderObjects = OrderUtils.groupOrderObjectsByObjectId(orderInfo.orderObjectsInfo, null)
        notificationService!!.sendPurchaseProductNotificationEmail(orderInfo, groupedOrderObjects)
        mailClient!!.sendShopNewOrderAddedNotification(orderInfo, groupedOrderObjects)

        if (isCloudPaymentsOnlineKassaEnabled) {
            executorService.execute {
                try {
                    sendReceiptToOnlineKassa(orderInfo, groupedOrderObjects)
                } catch (e: Exception) {
                    log.error(e)
                }
            }

        }
    }


    @Throws(NotFoundException::class, PermissionException::class, OrderException::class, PaymentException::class)
    fun createOrderFromShop(buyerId: Long, paymentType: PaymentType?, deliveryInfo: DeliveryInfo?, deliveryAddress: DeliveryAddress?, products: Map<Long, Int>, discount: Discount?): Order {
        val now = Date()
        val orderObjects = ArrayList<OrderObject>(products.size)
        val order = Order()
        order.buyerId = buyerId
        order.orderType = OrderObjectType.PRODUCT
        order.timeAdded = now
        order.timeUpdated = now
        order.totalPrice = BigDecimal.ZERO
        order.paymentType = paymentType ?: PaymentType.NOT_SET
        order.projectType = ProjectType.SHOP


        for ((productId, count) in products) {

            val product = productUsersService!!.getProductReadyForPurchase(buyerId, productId, count)

            for (i in 0 until count) {
                val orderObject = OrderObject()
                orderObject.merchantId = product.merchantProfileId ?: -1
                orderObject.objectId = product.productId
                orderObject.orderObjectType = order.orderType
                orderObject.price = product.price
                orderObject.comment = deliveryInfo!!.comment
                orderObject.estimatedDeliveryTime = null
                orderObjects.add(orderObject)
            }

        }

        if (deliveryInfo != null) {
            order.deliveryPrice = deliveryInfo.deliveryPrice
            order.deliveryType = deliveryInfo.deliveryType
            order.deliveryDepartmentId = deliveryInfo.deliveryDepartmentId
        }

        if (discount != null) {
            order.promoCodeId = discount.promoCodeId
            order.discount = BigDecimal(discount.totalPriceDiscount)
            if (discount.isFreeDelivery) {
                order.discount = order.discount.add(order.deliveryPrice)
            }
        }

        OrderStateVerifier.Assert.isTrue(order.buyerId > 0, "Buyer is anonymous user")
        OrderStateVerifier.Assert.isNotEmpty(orderObjects, "Order has no any nested object")

        var objectsPrice = OrderUtils.calculateOrderObjectsPrice(orderObjects)
        if (order.totalPrice.compareTo(objectsPrice) > 0) {
            objectsPrice = order.totalPrice
        }

        if (discount == null) {
            order.totalPrice = objectsPrice.add(order.deliveryPrice)
        } else {
            var totalPrice = objectsPrice.subtract(BigDecimal(discount.totalPriceDiscount))
            if (!discount.isFreeDelivery) {
                totalPrice = totalPrice.add(order.deliveryPrice)
            }
            order.totalPrice = totalPrice
        }

        orderService!!.storeToDb(order, orderObjects)

        if (order.deliveryType === DeliveryType.CUSTOMER_PICKUP) {
            val subjectType = SubjectType.fromOrderObjectType(order.orderType!!)
            val objectId: Long = 0
            val linkedDelivery = deliveryService!!.getLinkedDelivery(objectId, order.deliveryDepartmentId, subjectType!!)
            val address = linkedDelivery.address

            val phone = if (address!!.phone == null) "" else " " + address.phone!!
            deliveryAddress!!.address = address.street!! + phone
            deliveryAddress.city = address.city

            // TODO remake this
            if (address.country == null || "0" == address.country) {
                if (address.countryId != 0) {
                    val country = geoService!!.getCountryById(address.countryId)
                    deliveryAddress.country = country!!.name
                } else {
                    deliveryAddress.country = ""
                }
            } else {
                deliveryAddress.country = address.country
            }
            deliveryAddress.zipCode = ""
        }

        if (deliveryAddress != null) {
            deliveryAddress.orderId = order.orderId
            deliveryAddressService!!.saveDeliveryAddress(deliveryAddress)
        }

        if (order.paymentType === PaymentType.CASH) {
            sendPurchaseProductNotificationEmail(order)
        }
        return order
    }


    @Throws(PermissionException::class, ChangeProductQuantityException::class, NotFoundException::class)
    fun removeOrderedProductFromStore(orderId: Long) {
        val storedQuantity = HashMap<Long, Int>()
        for (oo in orderService.getOrderObjects(orderId)) {
            val productId = oo.objectId
            if (storedQuantity[productId] == null) {
                storedQuantity[productId] = 1
            } else {
                storedQuantity[productId] = (storedQuantity[productId] ?: 0) + 1
            }
        }

        for ((productId, quantity) in storedQuantity) {
            val p = productDAO.selectProduct(productId) ?: throw NotFoundException(Product::class.java, productId)
            if (p.totalQuantity < quantity) {
                throw ChangeProductQuantityException()
            }
            productDAO.updateTotalQuantityDelta(productId, (-quantity).toLong())

            if (p.parentProductId != 0L) {
                productDAO.updateTotalQuantityDelta(p.parentProductId, (-quantity).toLong())
            }
        }
    }

    companion object {

        private val log = Logger.getLogger(ProductAfterFundService::class.java)


        @Throws(NotFoundException::class, PermissionException::class)
        private fun groupOrderObjectsByObjectId(orderObjects: List<OrderObject>): Map<Long, Set<OrderObject>> {
            val result = HashMap<Long, HashSet<OrderObject>>()
            for (orderObject in orderObjects) {
                val objectSet = getCachedObject(orderObject.objectId, result, object : BaseService.Getter<HashSet<OrderObject>> {
                    override fun get(objectId: Long): HashSet<OrderObject> {
                        return HashSet()
                    }
                })
                objectSet?.add(orderObject)
            }

            return result
        }

        @Throws(NotFoundException::class)
        private fun <T> getCachedObject(objectId: Long, cache: MutableMap<Long, T>, getter: BaseService.Getter<T>): T? {
            if (!cache.containsKey(objectId)) {
                cache[objectId] = getter[objectId]
            }
            return cache[objectId]
        }
    }


}

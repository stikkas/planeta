package ru.planeta.api.service.profile

import ru.planeta.api.exceptions.NotFoundException
import ru.planeta.api.exceptions.PermissionException
import ru.planeta.api.model.json.ProfileInfo
import ru.planeta.model.profile.Profile
import ru.planeta.model.profile.User

/**
 * Service for working with common profiles data
 *
 * @author ameshkov
 */
interface ProfileService {

    /**
     * Gets info for anonymous profile
     */
    val anonymousProfile: ProfileInfo

    /**
     * Gets profile only data by profile id
     *
     * @param profileId interested profile identifier
     * @return profile object
     */
    fun getProfile(profileId: Long): Profile?

    fun getProfileWithDeleted(profileId: Long): Profile

    /**
     * Gets profile only data by alias
     */
    fun getProfile(alias: String): Profile?


    /**
     * Gets info on the specified profile (including relation between you)
     */
    @Throws(NotFoundException::class, PermissionException::class)
    fun <T : ProfileInfo> getProfileInfo(myProfileId: Long, profileId: Long): T


    @Throws(NotFoundException::class, PermissionException::class)
    fun <T : ProfileInfo> getProfile(myProfileId: Long, alias: String): T

    /**
     * Adds new active relation between active profile and passive profile
     *
     */
    @Throws(NotFoundException::class, PermissionException::class)
    fun relationAddActive(activeProfileId: Long, passiveProfileId: Long)

    /**
     * Sets avatar and small avatars for specified profile
     *
     */
    @Throws(NotFoundException::class, PermissionException::class)
    fun setProfileAvatar(clientId: Long, profileId: Long, imageId: Long?): Profile

    @Deprecated("")
    @Throws(NotFoundException::class, PermissionException::class)
    fun setGroupDisplayNameAndAvatar(clientId: Long, profileId: Long, displayName: String, imageId: Long)

    @Throws(NotFoundException::class)
    fun getUser(profileId: Long): User

    /**
     * Sets profile's small avatar
     *
     */
    @Throws(PermissionException::class, NotFoundException::class)
    fun setProfileSmallAvatar(clientId: Long, profileId: Long, imageId: Long): Profile

    fun getProfiles(ids: Collection<Long>): List<Profile>

    @Throws(NotFoundException::class)
    fun getProfileSafe(profileId: Long): Profile

    @Throws(NotFoundException::class)
    fun getProfileSafe(alias: String): Profile

    @Throws(NotFoundException::class, PermissionException::class)
    fun getOrCreateHiddenGroup(clientId: Long): Profile

    fun setIsShowBackedCampaigns(myProfileId: Long, isShowBackedCampaigns: Boolean)

    @Throws(NotFoundException::class)
    fun unsubscribeProfileFromAuthorSubscription(id: Long)

    fun generateRandomAvatarUrl(): String

    @Throws(PermissionException::class, NotFoundException::class)
    fun setDisplayName(clientId: Long, profileId: Long, displayName: String)

    fun getShopProductsProfiles(tagId: Long, limit: Int): List<Profile>

    fun updateNewSubscribersCount(profileId: Long)
}

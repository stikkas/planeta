package ru.planeta.api.service.common

import ru.planeta.api.exceptions.NotFoundException
import ru.planeta.api.exceptions.PermissionException
import ru.planeta.model.common.Banner
import ru.planeta.model.enums.BannerStatus
import ru.planeta.model.enums.BannerType
import java.util.EnumSet

/**
 * User: a.savanovich
 * Date: 18.05.12
 * Time: 11:36
 */
interface BannerService {
    /**
     * update current banner or create new
     *
     * @param clientId @param clientId - id of profile who tries to access banner service
     * @throws PermissionException if client profile have no permissions to access banner service
     */
    @Throws(PermissionException::class)
    fun save(clientId: Long, banner: Banner)

    @Throws(PermissionException::class)
    fun createFromCampaignId(clientId: Long, campaignId: Long, html: String): Banner


    fun createSimpleBanner(): Banner

    @Throws(PermissionException::class)
    fun createTopBanner(clientId: Long, bannerRaw: Banner): Banner

    /**
     * copy current banner and insert it into db with new bannerId
     *
     * @throws PermissionException
     */
    @Throws(PermissionException::class)
    fun copy(clientId: Long, banner: Banner)


    @Throws(PermissionException::class)
    fun reloadFromDb(clientId: Long)


    fun reload()

    /**
     * Choose a banner of given type and having mask that matches given url.
     * Filters banners with previous ids.
     *
     * @param previousIds - previously shown banners ids
     * @param clientId - id of profile who tries to access banner service or 0(everybody)
     * @param requestUrl - request page url
     */
    fun selectRandom(clientId: Long, isAuthor: Boolean, type: BannerType?, previousIds: List<Long>?, requestUrl: String?): Banner?

    /**
     * show in admin editor
     *
     * @param clientId @param clientId - id of profile who tries to access banner service
     * @param bannerStatuses
     * @throws PermissionException if client profile have no permissions to access banner service
     */
    @Throws(PermissionException::class)
    fun selectList(clientId: Long, bannerStatuses: EnumSet<BannerStatus>, offset: Int, limit: Int): List<Banner>

    fun selectBanners(clientId: Long, isAuthor: Boolean, type: BannerType?, previousIds: List<Long>?, requestUrl: String?): List<Banner>

    @Throws(PermissionException::class)
    fun saveWithMaskFormat(clientId: Long, banner: Banner)

    /**
     * Select current banner
     *
     * @param clientId @param clientId - id of profile who tries to access banner service or 0(everybody)
     * @param bannerId of current banner
     */
    @Throws(NotFoundException::class, PermissionException::class)
    fun select(clientId: Long, bannerId: Long): Banner

    /**
     * deleteByProfileId banner from db
     *
     * @param clientId @param clientId - id of profile who tries to access banner service
     * @throws PermissionException if client profile have no permissions to access banner service
     */
    @Throws(PermissionException::class)
    fun delete(clientId: Long, bannerId: Long)

}

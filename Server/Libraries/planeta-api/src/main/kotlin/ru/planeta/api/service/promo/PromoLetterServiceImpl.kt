package ru.planeta.api.service.promo

import org.apache.log4j.Logger
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Lazy
import org.springframework.stereotype.Service
import ru.planeta.api.Utils
import ru.planeta.api.service.notifications.NotificationService
import ru.planeta.commons.lang.CollectionUtils
import ru.planeta.model.common.campaign.CampaignTag
import ru.planeta.model.trashcan.PromoCode
import ru.planeta.model.trashcan.PromoConfig
import ru.planeta.model.trashcan.PromoEmail

import java.math.BigDecimal
import java.util.Date


/**
 * Created with IntelliJ IDEA.
 * User: michail
 * Date: 19.07.2017
 * Time: 15:45
 */
@Service
class PromoLetterServiceImpl(@Lazy
                             private val notificationService: NotificationService,
                             private val promoCodeService: ExternalPromoCodeService,
                             private val promoConfigService: PromoConfigService,
                             private val promoEmailService: PromoEmailService) : PromoLetterService {
    private val logger = Logger.getLogger(PromoLetterServiceImpl::class.java)

    override fun sendPromoLetter(email: String, totalPrice: BigDecimal, campaignTags: Collection<CampaignTag>, orderId: Long) {
        val campaignTagIds = CollectionUtils.map(campaignTags) { tag -> tag.id.toLong() }
        val configs = promoConfigService.getCompatibleConfigs(campaignTagIds, totalPrice, email)

        for (config in configs) {
            validateAndSend(email, orderId, config)
        }
    }

    private fun validateAndSend(email: String, orderId: Long, promoConfig: PromoConfig?) {
        if (promoConfig == null) {
            return
        }

        val template = promoConfig.mailTemplate
        logger.info("Promo email template: " + template!!)

        if (promoConfig.isHasPromocode) {
            sendPromoCodeEmail(promoConfig, email, orderId, template)
        } else {
            notificationService.sendPromoEmail(email, template)
            promoEmailService.insert(PromoEmail(promoConfig.configId, 0, email, orderId))
        }
    }

    private fun sendPromoCodeEmail(promoConfig: PromoConfig, email: String, orderId: Long, template: String?) {
        val promoCode = promoCodeService.getLastNonUsed(promoConfig.configId)
        if (promoCode != null) {
            promoCodeService.usePromoCode(promoCode)
            promoEmailService.insert(PromoEmail(promoConfig.configId, promoCode.codeId, email, orderId))
            notificationService.sendPromoEmail(email, template!!, Utils.smap(PROMO_CODE, promoCode.secretCode as Any))
        }
    }

    companion object {

        private val PROMO_CODE = "promoCode"
    }
}

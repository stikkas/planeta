package ru.planeta.api.service.profile

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import ru.planeta.api.exceptions.MessageCode
import ru.planeta.api.exceptions.PermissionException
import ru.planeta.api.service.security.PermissionService
import ru.planeta.dao.commondb.ProfileBalanceDAO
import ru.planeta.dao.commondb.UserPrivateInfoDAO
import ru.planeta.model.common.ProfileBalance

import java.math.BigDecimal
import java.math.RoundingMode

/**
 * User service
 *
 * @author a.savanovich
 * Date: 28.10.11
 */
@Service
class UserServiceImpl : UserService {

    @Autowired
    private val profileBalanceDAO: ProfileBalanceDAO? = null
    @Autowired
    private val userPrivateInfoDAO: UserPrivateInfoDAO? = null

    override val usersCount: Long
        get() = userPrivateInfoDAO!!.selectUsersCount()

    override fun getUserProfileBalance(profileId: Long): BigDecimal {
        return profileBalanceDAO!!.select(profileId).setScale(2, RoundingMode.HALF_UP)
    }

    @Throws(PermissionException::class)
    override fun getPersonalCabinet(profileId: Long): ProfileBalance {
        if (profileId == PermissionService.ANONYMOUS_USER_PROFILE_ID) {
            throw PermissionException(MessageCode.ANONYMOUS_CANT_HAVE_ACCESS)
        }
        return profileBalanceDAO!!.getPersonalCabinet(profileId)
    }
}

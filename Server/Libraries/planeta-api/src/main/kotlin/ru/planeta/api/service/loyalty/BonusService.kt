package ru.planeta.api.service.loyalty

import ru.planeta.api.exceptions.NotFoundException
import ru.planeta.model.common.loyalty.Bonus
import ru.planeta.model.common.loyalty.BonusPriorityItem

import java.util.Date

/**
 * Created with IntelliJ IDEA.
 * User: alexa_000
 * Date: 04.04.14
 * Time: 20:54
 */
interface BonusService {

    fun getBonuses(offset: Int, limit: Int): List<Bonus>

    fun getBonus(bonusId: Long): Bonus?

    @Throws(NotFoundException::class)
    fun getBonusSafe(bonusId: Long): Bonus

    fun reorderBonuses(bonusPriorityItems: List<BonusPriorityItem>)

    fun saveBonus(bonus: Bonus)

    fun getAllBonuses(offset: Int, limit: Int): List<Bonus>
}

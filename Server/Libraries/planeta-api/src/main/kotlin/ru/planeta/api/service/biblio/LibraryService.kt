package ru.planeta.api.service.biblio

import ru.planeta.api.exceptions.NotFoundException
import ru.planeta.api.service.notifications.NotificationService
import ru.planeta.model.bibliodb.Library
import ru.planeta.model.bibliodb.enums.LibraryType
import java.io.File
import java.io.IOException

interface LibraryService {

    val countActiveLibraries: Long

    fun searchClusterLibraries(searchStr: String?, libraryType: LibraryType?, north: Double, south: Double, east: Double, west: Double, zoomLevel: Long, minZoomLevel: Long): List<*>

    fun selectLibrariesList(libraryIdList: List<Long>): List<Library>

    fun getLibrary(libraryId: Long): Library?

    @Throws(NotFoundException::class)
    fun save(library: Library, notificationService: NotificationService)

    fun delete(library: Library)

    fun randomLibrary(exludedLibraries: Collection<Library>): Library

    fun hasLibraryRequests(name: String, email: String): Boolean
}

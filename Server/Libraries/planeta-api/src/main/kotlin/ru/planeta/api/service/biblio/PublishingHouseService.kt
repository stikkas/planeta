package ru.planeta.api.service.biblio

/*
 * Created by Alexey on 06.06.2016.
 */

import ru.planeta.model.bibliodb.PublishingHouse

interface PublishingHouseService {

    fun getPublishingHouses(searchRow: String, offset: Int, limit: Int): List<PublishingHouse>

    fun getPublishingHouseById(publishingHouseId: Long): PublishingHouse

    fun addPublishingHouse(publishingHouse: PublishingHouse)

    fun selectCountPublishingHouses(searchRow: String): Long
}

package ru.planeta.api.model.json

import ru.planeta.model.stat.log.DBLogRecord
import ru.planeta.model.stat.log.HttpInteractionLogRecord

/**
 * Database log record info.<br></br>
 * User: eshevchenko
 */
class DBLogRecordInfo : DBLogRecord() {

    var request: HttpInteractionLogRecord? = null
    var response: HttpInteractionLogRecord? = null

    companion object {

        fun create(record: DBLogRecord, request: HttpInteractionLogRecord, response: HttpInteractionLogRecord): DBLogRecordInfo {
            val result = DBLogRecordInfo()
            result.id = record.id
            result.message = record.message
            result.loggerType = record.loggerType
            result.objectId = record.objectId
            result.subjectId = record.subjectId
            result.request = request
            result.response = response
            result.timeAdded = record.timeAdded

            return result
        }
    }
}

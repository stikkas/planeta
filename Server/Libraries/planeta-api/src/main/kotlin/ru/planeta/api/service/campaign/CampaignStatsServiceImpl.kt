package ru.planeta.api.service.campaign

import org.apache.log4j.Logger
import org.sphx.api.SphinxException
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Lazy
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Service
import ru.planeta.api.BaseSearchableService
import ru.planeta.api.Utils
import ru.planeta.api.exceptions.MessageCode
import ru.planeta.api.exceptions.NotFoundException
import ru.planeta.api.exceptions.PermissionException
import ru.planeta.api.search.CampaignStatusFilter
import ru.planeta.api.search.SearchCampaigns
import ru.planeta.api.search.SortOrder
import ru.planeta.api.service.billing.order.OrderService
import ru.planeta.api.service.configurations.ConfigurationService
import ru.planeta.api.service.geo.GeoService
import ru.planeta.model.common.OrderInfo
import ru.planeta.model.common.campaign.Campaign
import ru.planeta.model.common.campaign.CampaignSearchFilter
import ru.planeta.model.common.campaign.CampaignTag
import ru.planeta.model.common.campaign.Share
import ru.planeta.model.shop.enums.DeliveryStatus
import ru.planeta.model.shop.enums.DeliveryType
import ru.planeta.model.shop.enums.PaymentStatus
import ru.planeta.reports.ReportType
import ru.planeta.reports.SimpleReport
import java.math.BigDecimal
import java.util.*
import java.util.concurrent.locks.ReentrantReadWriteLock

/**
 * @author m.shulepov
 * Date: 14.06.13
 */
@Service
@Lazy(false)
class CampaignStatsServiceImpl @Autowired
constructor(private var configurationService: ConfigurationService,
            var orderService: OrderService,
            private var geoService: GeoService,
            var campaignService: CampaignService) : BaseSearchableService(), CampaignStatsService {

    private var relatedCampaignTags: List<CampaignTag> = ArrayList()
    private val successfulCampaignsLock = ReentrantReadWriteLock()

    // @PostConstruct
    @Scheduled(fixedDelay = UPDATE_SUCCESSFUL_CAMPAIGNS_STATS_DELAY)
    protected fun updateSuccessfulCampaigns() {

        val tagMnemonics = configurationService.relatedCampaignTagMnemonics
        val campaignTags = campaignService.getCampaignTagsByMnemonic(tagMnemonics)


        successfulCampaignsLock.writeLock().lock()
        try {
            relatedCampaignTags = campaignTags
        } finally {
            successfulCampaignsLock.writeLock().unlock()
        }

    }

    private fun getRelatedCampaignTags(): List<CampaignTag> {
        successfulCampaignsLock.readLock().lock()
        try {
            if (relatedCampaignTags.isEmpty()) {
                successfulCampaignsLock.readLock().unlock()
                updateSuccessfulCampaigns()
                successfulCampaignsLock.readLock().lock()
            }
            return relatedCampaignTags
        } finally {
            successfulCampaignsLock.readLock().unlock()
        }
    }

    @Deprecated("")
    @Throws(NotFoundException::class)
    override fun getSimilarShares(shareId: Long, limit: Int): List<ShareWithCampaign> {
        var limit = limit
        val share = campaignService.getShare(shareId) ?: throw NotFoundException(Share::class.java, shareId)
        val initialPrice = share.price
        if (initialPrice == BigDecimal.ZERO) {
            return emptyList()
        }

        val campaign = campaignService.getCampaign(share.campaignId)
                ?: throw NotFoundException(Campaign::class.java, share.campaignId)
        val similarTag = campaign.mainTag
        val campaignTags = ArrayList<CampaignTag>()
        similarTag?.let {
            campaignTags.add(it)
        }

        try {
            val sharesWithCampaigns = ArrayList<ShareWithCampaign>()
            val similarCampaigns = searchService.getTopFilteredCampaignsSearchResult(
                    SearchCampaigns(listOf(SortOrder.SORT_BY_COLLECTION_RATE), null, campaignTags,
                            CampaignStatusFilter.ACTIVE, null, null, null, null, null, null, null, false, 0,
                            SIMILAR_CAMPAIGNS_LIMIT))
            similarCampaigns.searchResultRecords?.forEach {
                val campaignShares = campaignService.getCampaignSharesFiltered(it.campaignId)
                for (campaignShare in campaignShares) {
                    if (campaignShare.shareId != shareId) {
                        sharesWithCampaigns.add(ShareWithCampaign(campaignShare, it))
                    }
                }
            }

            // sort in ascending order by closeness of share price to initialPrice
            Collections.sort(sharesWithCampaigns) { share, other ->
                val diff1 = share.price!!.subtract(initialPrice!!).abs()
                val diff2 = other.price!!.subtract(initialPrice).abs()
                diff1.compareTo(diff2)
            }
            limit = Math.min(sharesWithCampaigns.size, limit)
            return sharesWithCampaigns.subList(0, limit)
        } catch (e: SphinxException) {
            log.error(e)
        }

        return emptyList()
    }

    @Throws(SphinxException::class)
    override fun getRelatedCampaigns(ignoreCampaignId: Long): Collection<Campaign> {
        return searchService.searchForCampaignRelated(getRelatedCampaignTags(), ignoreCampaignId) ?: emptyList()
    }

    override fun getSharesSaleInfo(campaignId: Long, from: Date?, to: Date?, offset: Int, limit: Int): List<Share> {
        return shareDAO.selectSharesSaleInfo(campaignId, from, to, offset, limit)
    }

    @Throws(NotFoundException::class, PermissionException::class)
    override fun getReport(clientId: Long, reportType: ReportType, campaignSearchFilter: CampaignSearchFilter): SimpleReport {

        val campaign = campaignService.getCampaignSafe(campaignSearchFilter.campaignId)
        if (!permissionService.isAdmin(clientId, campaign.profileId!!)) {
            throw PermissionException(MessageCode.USER_NOT_ADMIN)
        }

        campaignSearchFilter.clientId = clientId
        val ordersInfo = orderService.getCampaignOrders(campaignSearchFilter)

        val report = reportType.createReport("campaign_orders_report_" + campaignSearchFilter.campaignId)
        report.addCaptionRow(*getMessage("report.campaign.purchase").split(";".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray())

        for (order in ordersInfo) {

            var delivery = getMessage("report.campaign.delivery.not.set")
            if (order.deliveryType !== DeliveryType.NOT_SET) {
                val linkedDelivery = order.linkedDelivery
                if (linkedDelivery != null && linkedDelivery.location != null) {
                    linkedDelivery.location = geoService.getLocationWithParents(linkedDelivery.location)
                    delivery = (Utils.nvl(linkedDelivery.name, "")
                            + " " + if (linkedDelivery.location != null && linkedDelivery.location!!.name != null) linkedDelivery.location!!.name else "")
                }
            }

            report.addRow(
                    order.timeAdded, order.orderId, order.totalPrice, order.deliveryPrice, getCampaignOrderStatus(order), order.buyerName, order.buyerEmail, order.orderObjectsTotalPrice, order.orderObjectName, order.orderObjectsCount, order.orderObjectPrice, if (order.deliveryType === DeliveryType.NOT_SET) null else order.deliveryPrice, delivery
            )

            if (order.deliveryAddress != null && order.deliveryType !== DeliveryType.NOT_SET) {
                val deliveryAddress = order.deliveryAddress

                report.addCells(
                        deliveryAddress!!.zipCode, deliveryAddress.country, deliveryAddress.city, deliveryAddress.address, deliveryAddress.phone, deliveryAddress.fio
                ).skipCell()
            } else {
                report.skipCell(5)
                report.addCell(order.buyerName).skipCell()
            }

            report.addCells(order.orderObjectComment, order.trackingCode)
            if (order.cashOnDeliveryCost!!.compareTo(BigDecimal.ZERO) > 0) {
                report.addCell(order.cashOnDeliveryCost)
            }
        }

        return report
    }

    private fun getCampaignOrderStatus(orderInfo: OrderInfo): String {
        var result = ""
        if (PaymentStatus.COMPLETED === orderInfo.paymentStatus) {
            when (orderInfo.deliveryStatus) {
                DeliveryStatus.PENDING -> result = getMessage("campaign.order.status.new")
                DeliveryStatus.DELIVERED -> result = getMessage("campaign.order.status.completed")
                DeliveryStatus.GIVEN -> result = getMessage("campaign.order.status.given")
                DeliveryStatus.CANCELLED -> result = getMessage("campaign.order.status.cancelled")
            }
        } else if (PaymentStatus.CANCELLED === orderInfo.paymentStatus) {
            result = getMessage("campaign.order.status.cancelled")
        }
        return result
    }

    private fun getMessage(messageCode: String): String {
        return messageSource.getMessage(messageCode, null, "", Locale.getDefault())
    }

    companion object {

        private val log = Logger.getLogger(CampaignStatsServiceImpl::class.java)

        private const val UPDATE_SUCCESSFUL_CAMPAIGNS_STATS_DELAY = (15 * 60 * 1000).toLong() // 15 minutes

        private val SIMILAR_CAMPAIGNS_LIMIT = 10
    }
}

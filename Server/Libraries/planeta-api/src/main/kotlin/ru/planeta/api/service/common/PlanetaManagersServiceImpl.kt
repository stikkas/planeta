package ru.planeta.api.service.common

import org.apache.log4j.Logger
import org.springframework.stereotype.Service
import ru.planeta.api.exceptions.PermissionException
import ru.planeta.api.mail.MailClient
import ru.planeta.api.news.LoggerService
import ru.planeta.api.service.campaign.CampaignService
import ru.planeta.dao.commondb.PlanetaManagersDAO
import ru.planeta.dao.commondb.UserPrivateInfoDAO
import ru.planeta.model.common.PlanetaCampaignManager
import ru.planeta.model.common.PlanetaManager
import ru.planeta.model.common.PlanetaManagerRelation
import ru.planeta.model.common.campaign.CampaignTag
import ru.planeta.model.news.ProfileNews
import java.util.*

@Service
class PlanetaManagersServiceImpl(private val planetaManagersDAO: PlanetaManagersDAO,
                                 private val mailClient: MailClient,
                                 private val userPrivateInfoDAO: UserPrivateInfoDAO,
                                 private val profileNewsService: LoggerService) : PlanetaManagersService {

    var campaignService: CampaignService? = null

    override val campaignTagManagersRelations: List<PlanetaManagerRelation<*>>
        get() = planetaManagersDAO.selectPlanetaCampaignTagManagerRelations()

    override fun getAllManagers(active: Boolean?): List<PlanetaManager> {
        return planetaManagersDAO.selectAllManagers(active)
    }

    override fun changeCampaignTagManagerRelation(managerId: Long?, tagId: Long?) {
        planetaManagersDAO.updateManagerForTag(managerId!!, tagId!!)
    }

    override fun createManager(name: String, fullName: String, email: String, profileId: Long?): PlanetaManager {
        return planetaManagersDAO.insertNewManager(name, fullName, email, profileId)
    }

    override fun saveManager(manager: PlanetaManager) {
        planetaManagersDAO.updateManager(manager)
    }

    override fun getCampaignManagerByTag(campaignTag: CampaignTag?): PlanetaManager? {
        return if (campaignTag == null) {
            null
        } else planetaManagersDAO.getCampaignManager(campaignTag)
    }

    @Throws(PermissionException::class)
    override fun setManagerForCurrentObject(managerForCurrentObject: PlanetaCampaignManager, clientId: Long) {
        if (managerForCurrentObject.managerId == 0L) {
            throw PermissionException("Manager is empty")
        }

        val currentManager = planetaManagersDAO.getManagerForCurrentCampaign(managerForCurrentObject.campaignId)
        if (currentManager != null) {
            planetaManagersDAO.updateManagerForCurrentObject(managerForCurrentObject)

            val campaign = campaignService?.getCampaign(managerForCurrentObject.campaignId)
            val manager = getManagerById(managerForCurrentObject.managerId)
            var authorEmail: String? = ""

            if (campaign != null) {
                val userPrivateInfo = userPrivateInfoDAO.selectByUserId(campaign.authorProfileId)
                authorEmail = if (userPrivateInfo != null) userPrivateInfo.email else ""
            }

            campaign?.let {
                mailClient.sendChangeManagerForCampaignEmail(it, manager.email
                        ?: "", managerForCurrentObject.comment ?: "", authorEmail ?: "")
            }

            logger.info("Manager for campaignId = " + managerForCurrentObject.campaignId
                    + " switched to managerId = " + managerForCurrentObject.managerId
                    + " by userId = " + clientId)
            val extraParamsMap = HashMap<String, String>()
            extraParamsMap["prevManagerId"] = currentManager.managerId.toString()
            extraParamsMap["newManagerId"] = managerForCurrentObject.managerId.toString()
            profileNewsService.addProfileNews(ProfileNews.Type.MANAGER_FOR_CAMPAIGN_UPDATED,
                    clientId,
                    managerForCurrentObject.managerId,
                    managerForCurrentObject.campaignId,
                    extraParamsMap)
        } else {
            planetaManagersDAO.insertManagerForCurrentObject(managerForCurrentObject)

            logger.info("Manager for campaignId = " + managerForCurrentObject.campaignId
                    + " added with managerId = " + managerForCurrentObject.managerId
                    + " by userId = " + clientId)
            val extraParamsMap = HashMap<String, String>()
            extraParamsMap["prevManagerId"] = "0"
            extraParamsMap["newManagerId"] = managerForCurrentObject.managerId.toString()
            profileNewsService.addProfileNews(ProfileNews.Type.MANAGER_FOR_CAMPAIGN_UPDATED,
                    clientId,
                    managerForCurrentObject.managerId,
                    managerForCurrentObject.campaignId,
                    extraParamsMap)
        }
    }

    override fun getCampaignManager(campaignId: Long): PlanetaManager {
        return planetaManagersDAO.getManagerForCurrentCampaign(campaignId)
    }

    override fun getManagersForCurrentCampaigns(campaignIdList: List<Long>): List<PlanetaManager> {
        return planetaManagersDAO.getManagersForCurrentCampaigns(campaignIdList)
    }

    override fun getManagerId(profileId: Long?): Long {
        return planetaManagersDAO.getManagerId(profileId)
    }

    private fun getManagerByProfileId(profileId: Long): PlanetaManager {
        return planetaManagersDAO.selectManagerByProfileId(profileId)
    }

    private fun getManagerById(id: Long): PlanetaManager {
        return planetaManagersDAO.selectManagerById(id)
    }

    companion object {

        private val logger = Logger.getLogger(PlanetaManagersServiceImpl::class.java)
    }
}

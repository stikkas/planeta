package ru.planeta.api.service.billing.order

import org.apache.commons.collections4.CollectionUtils
import org.apache.commons.collections4.Transformer
import org.apache.commons.lang3.StringUtils
import org.apache.log4j.Logger
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Lazy
import org.springframework.stereotype.Service
import ru.planeta.api.BaseService
import ru.planeta.api.exceptions.BaseException
import ru.planeta.api.exceptions.NotFoundException
import ru.planeta.api.exceptions.PermissionException
import ru.planeta.api.service.biblio.BookService
import ru.planeta.api.service.biblio.LibraryService
import ru.planeta.api.service.campaign.CampaignService
import ru.planeta.api.service.common.InvestingOrderInfoService
import ru.planeta.api.service.geo.GeoService
import ru.planeta.api.service.loyalty.BonusService
import ru.planeta.api.service.profile.ProfileService
import ru.planeta.api.service.shop.ProductUsersService
import ru.planeta.dao.commondb.OrderDAO
import ru.planeta.dao.commondb.OrderObjectDAO
import ru.planeta.model.common.*
import ru.planeta.model.common.campaign.Campaign
import ru.planeta.model.common.campaign.Share
import ru.planeta.model.common.delivery.SubjectType
import ru.planeta.model.enums.OrderObjectType
import ru.planeta.model.enums.ProfileType
import ru.planeta.model.profile.Profile
import ru.planeta.model.shop.ProductInfo
import ru.planeta.model.shop.enums.ProductCategory
import java.io.UnsupportedEncodingException
import java.net.URLEncoder
import java.util.*

/**
 * Implements [OrderInfoService] interface.<br></br>
 * Created by eshevchenko.
 */
@Service
class OrderInfoServiceImpl(
        private val productUsersService: ProductUsersService,
        private val libraryService: LibraryService,
        private val profileService: ProfileService,
        @Lazy
        private val campaignService: CampaignService,
        private val bonusService: BonusService,
        private val geoService: GeoService,
        private val ioiService: InvestingOrderInfoService,
        private val bookService: BookService,
        private val orderDAO: OrderDAO,
        private val orderObjectDAO: OrderObjectDAO,
        @Value("\${dp.application.host}")
        private val dpHost: String) : OrderInfoService {

    private val logger = Logger.getLogger(OrderInfoServiceImpl::class.java)
    @Throws(NotFoundException::class)
    override fun getOrdersInfo(orders: Collection<Order>, needCollectObjects: Boolean): Collection<OrderInfo> {
        try {
            return collectOrdersInfo(orders, needCollectObjects)
        } finally {
            removeCache()
        }
    }

    override fun getOrderObjectsInfo(clientId: Long, orderObjects: Collection<OrderObject>): Collection<OrderObjectInfo> {
        try {
            return collectOrderObjectsInfo(orderObjects)
        } finally {
            removeCache()
        }
    }

    @Throws(NotFoundException::class)
    override fun getOrdersForOHMPage(offset: Int, limit: Int): List<Map<String, Any>> {
        return orderDAO.selectOrdersInforForOHMPage(offset, limit)
    }

    // never use this
    @Deprecated("")
    private fun collectOrdersInfo(orders: Collection<Order>, needCollectObjects: Boolean): List<OrderInfo> {
        val result = ArrayList<OrderInfo>()

        if (CollectionUtils.isNotEmpty(orders)) {

            for (order in orders) {
                try {
                    getCached(OrderInfo::class.java, order.orderId, object : BaseService.Getter<OrderInfo> {
                        @Throws(NotFoundException::class)
                        override fun get(objectId: Long): OrderInfo {
                            val oot = order.orderType
                            if (oot === OrderObjectType.INVESTING || oot === OrderObjectType.INVESTING_WITHOUT_MODERATION
                                    || oot === OrderObjectType.INVESTING_WITH_ALL_ALLOWED_INVOICE || oot === OrderObjectType.BIBLIO) {
                                order.investInfo = ioiService!!.select(objectId)
                            }
                            return OrderInfo.createFrom(order)
                        }
                    })
                } catch (e: NotFoundException) {
                    logger.error(e)
                }

            }

            if (needCollectObjects) {

                // TODO DANGER!!!
                val orderObjects = orderObjectDAO.selectOrdersObjects(getEntityCache(OrderInfo::class.java).keys, 0L)

                for (orderObjectInfo in collectOrderObjectsInfo(orderObjects)) {
                    val orderInfo: OrderInfo
                    try {
                        orderInfo = getCached(OrderInfo::class.java, orderObjectInfo.orderId)
                    } catch (e: NotFoundException) {
                        logger.error(e)
                        continue
                    }

                    orderInfo.addOrderObjectInfo(orderObjectInfo)
                }
            }

            for (order in orders) {
                try {
                    val orderInfo = getCached(OrderInfo::class.java, order.orderId)

                    val orderObjectId = if (orderInfo.orderType === OrderObjectType.SHARE) orderInfo.orderObjectsInfo[0].objectId else 0

                    val subjectType = SubjectType.fromOrderObjectType(order.orderType!!)

                    val orderInfoCommonData = orderDAO.selectOrderInfoCommonData(order.orderId, orderObjectId, order.deliveryDepartmentId, subjectType!!)

                    orderInfo.deliveryAddress = orderInfoCommonData.deliveryAddress

                    orderInfo.buyerName = orderInfoCommonData.buyerName
                    orderInfo.buyerAlias = orderInfoCommonData.buyerAlias
                    orderInfo.buyerImageUrl = orderInfoCommonData.buyerImageUrl
                    orderInfo.buyerGender = orderInfoCommonData.buyerGender
                    orderInfo.buyerEmail = orderInfoCommonData.buyerEmail
                    orderInfo.orderObjectCampaignId = orderInfoCommonData.orderObjectCampaignId
                    orderInfo.orderObjectCampaignName = orderInfoCommonData.orderObjectCampaignName

                    val deliveryAddress = orderInfoCommonData.deliveryAddress
                    val address = Address()

                    if (deliveryAddress != null) {
                        address.country = deliveryAddress.country
                        address.city = deliveryAddress.city
                        address.street = deliveryAddress.address
                        address.phone = deliveryAddress.phone
                    }

                    orderInfoCommonData.linkedDelivery!!.address = address

                    orderInfo.linkedDelivery = orderInfoCommonData.linkedDelivery
                    if (orderInfo.linkedDelivery!!.location != null) {
                        orderInfo.linkedDelivery!!.location = geoService.getLocationWithParents(orderInfo.linkedDelivery!!.location)
                    }
                    result.add(orderInfo)
                } catch (e: Exception) {
                    logger.error(e)
                }

            }
        }
        return result
    }

    private fun collectOrderObjectsInfo(orderObjects: Collection<OrderObject>): Collection<OrderObjectInfo> {
        val result = ArrayList<OrderObjectInfo>()

        if (CollectionUtils.isNotEmpty(orderObjects)) {
            for (orderObject in orderObjects) {
                try {
                    val orderObjectInfo = OrderObjectInfo.createFrom(orderObject)
                    fillMerchantInfo(orderObjectInfo)
                    fillSpecificObjectInfo(orderObjectInfo)
                    result.add(orderObjectInfo)
                } catch (ex: Exception) {
                    // TODO: Why share exception handles in this level instead of fillShareInfo() ?
                    logger.error("Share info not exist", ex)
                }

            }
        }

        return result
    }

    private fun fillMerchantInfo(objectInfo: OrderObjectInfo) {
        try {
            var merchant = getCached(Profile::class.java, objectInfo.merchantId, object : BaseService.Getter<Profile> {
                @Throws(NotFoundException::class)
                override fun get(objectId: Long): Profile {
                    return profileService!!.getProfileSafe(objectId)
                }
            })

            if (merchant!!.profileType === ProfileType.HIDDEN_GROUP) {
                merchant = getCached(Profile::class.java, merchant!!.creatorProfileId, object : BaseService.Getter<Profile> {
                    @Throws(NotFoundException::class)
                    override fun get(objectId: Long): Profile {
                        return profileService!!.getProfileSafe(objectId)
                    }
                })
            }

            objectInfo.merchantAlias = merchant!!.alias
            objectInfo.merchantName = merchant.displayName
            objectInfo.merchantImageUrl = merchant.imageUrl
            objectInfo.merchantId = merchant.profileId
        } catch (e: NotFoundException) {
            //skip, for bonuses now it usual
        }

    }

    @Throws(NotFoundException::class, PermissionException::class)
    private fun fillSpecificObjectInfo(orderObjectInfo: OrderObjectInfo) {
        try {
            when (orderObjectInfo.objectType) {
                OrderObjectType.SHARE -> fillShareInfo(orderObjectInfo)
                OrderObjectType.PRODUCT -> fillProductInfo(orderObjectInfo)
                OrderObjectType.BONUS -> fillBonusInfo(orderObjectInfo)
                OrderObjectType.BOOK -> fillBookInfo(orderObjectInfo)
                OrderObjectType.LIBRARY -> fillLibraryInfo(orderObjectInfo)
            }
        } catch (e: BaseException) {
            if (OrderObjectType.SHARE === orderObjectInfo.objectType) {
                logger.info("Can't fill specific " + orderObjectInfo.objectType + " info:", e)
            } else {
                logger.error("Can't fill specific " + orderObjectInfo.objectType + " info:", e)
            }
        }

    }

    @Throws(NotFoundException::class)
    private fun fillProductInfo(`object`: OrderObjectInfo) {
        val product = getCached(ProductInfo::class.java, `object`.objectId, object : BaseService.Getter<ProductInfo> {
            @Throws(NotFoundException::class)
            override fun get(objectId: Long): ProductInfo {
                return productUsersService!!.getProductSafe(objectId)
            }
        })

        if (product!!.productCategory === ProductCategory.DIGITAL
                && product!!.contentUrls != null
                && !product.contentUrls!!.isEmpty()) {
            `object`.downloadUrls = CollectionUtils.collect(product.contentUrls, Transformer { url ->
                try {
                    return@Transformer "https://" + dpHost + "/profile/download/" + `object`.orderObjectId + "?downloadUrl=" + URLEncoder.encode(url, "UTF-8")
                } catch (e: UnsupportedEncodingException) {
                    logger.error("Can't transform url $url: $e")
                    return@Transformer ""
                }
            })
        }

        `object`.startSaleDate = product!!.startSaleDate

        val merchant = getCached(Profile::class.java, product.merchantProfileId!!, object : BaseService.Getter<Profile> {
            @Throws(NotFoundException::class)
            override fun get(objectId: Long): Profile {
                return profileService!!.getProfileSafe(objectId)
            }
        })

        setSpecificObjectInfo(`object`, merchant!!.profileId, merchant.displayName, formatProductName(product), product.coverImageUrl, null)
        `object`.textForMail = product.textForMail
    }

    @Throws(NotFoundException::class)
    private fun fillBonusInfo(`object`: OrderObjectInfo) {
        val bonus = bonusService.getBonus(`object`.objectId)
        setSpecificObjectInfo(`object`, -1, "PLANETA", bonus?.name, bonus?.imageUrl, null)
    }

    @Throws(NotFoundException::class)
    private fun fillBookInfo(`object`: OrderObjectInfo) {
        val book = bookService!!.getBookById(`object`.objectId)
        if (book != null) {
            setSpecificObjectInfo(`object`, book.publishingHouseId, book.publishingHouseName, book.title, book.imageUrl, null)
            `object`.info = book.bonus
        }
    }

    @Throws(NotFoundException::class)
    private fun fillLibraryInfo(`object`: OrderObjectInfo) {
        val library = libraryService.getLibrary(`object`.objectId)
        if (library != null) {
            `object`.objectName = library.name
            `object`.info = library.address
        }
    }

    @Throws(NotFoundException::class, PermissionException::class)
    private fun fillShareInfo(`object`: OrderObjectInfo) {
        val share = getCached(Share::class.java, `object`.objectId, object : BaseService.Getter<Share> {
            @Throws(NotFoundException::class)
            override fun get(objectId: Long): Share {
                return campaignService.getShareSafe(objectId)
            }
        })
        val campaign = getCached(Campaign::class.java, share!!.campaignId!!, object : BaseService.Getter<Campaign> {
            @Throws(NotFoundException::class)
            override fun get(objectId: Long): Campaign {
                return campaignService.getCampaignSafe(objectId)
            }
        })

        setSpecificObjectInfo(`object`, share.campaignId!!, campaign!!.name, share.name, getShareImageUrl(campaign, share), share.estimatedDeliveryTime)
    }

    companion object {

        private val collectingSuperCache = object : ThreadLocal<HashMap<Class<*>, HashMap<Long, Any>>>() {
            override fun initialValue(): HashMap<Class<*>, HashMap<Long, Any>> {
                return HashMap()
            }
        }

        private fun getEntityCache(entityClass: Class<*>): MutableMap<Long, Any> {
            val superCache = collectingSuperCache.get()

            var result: HashMap<Long, Any>? = superCache[entityClass]
            if (result == null) {
                result = HashMap()
                superCache[entityClass] = result
            }

            return result
        }

        @Throws(NotFoundException::class)
        private fun <T> getCached(clazz: Class<T>, id: Long): T {
            return getEntityCache(clazz)[id] as T
        }

        @Throws(NotFoundException::class)
        private fun <T> getCached(clazz: Class<T>, id: Long, getter: BaseService.Getter<T>): T? {
            val entityCache = getEntityCache(clazz)

            var entity = entityCache[id] as? T
            if (entity == null) {
                entity = getter[id]
                entityCache[id] = entity as Any
            }

            return entity
        }

        private fun removeCache() {
            val superCache = collectingSuperCache.get()
            for (entityCache in superCache.values) {
                entityCache.clear()
            }
            superCache.clear()

            collectingSuperCache.remove()
        }

        private fun formatProductName(product: ProductInfo): String? {
            val productAttribute = product.productAttribute
            return if (productAttribute != null && productAttribute.value != null)
                String.format("%s (%s)", product.name, productAttribute.value)
            else
                product.name
        }

        private fun getShareImageUrl(campaign: Campaign, share: Share): String? {
            var imageUrl = share.imageUrl
            if (StringUtils.isBlank(imageUrl)) {
                imageUrl = if (StringUtils.isNotBlank(campaign.viewImageUrl)) campaign.viewImageUrl else campaign.imageUrl
            }
            return imageUrl
        }

        private fun setSpecificObjectInfo(target: OrderObjectInfo, ownerId: Long, ownerName: String?, objectName: String?, objectImageUrl: String?, estimatedDeliveryTime: Date?) {
            target.ownerId = ownerId
            target.ownerName = ownerName
            target.objectName = objectName
            target.objectImageUrl = objectImageUrl
            target.estimatedDeliveryTime = estimatedDeliveryTime
        }
    }
}

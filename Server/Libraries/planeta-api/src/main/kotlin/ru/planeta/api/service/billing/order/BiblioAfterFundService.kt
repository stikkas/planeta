package ru.planeta.api.service.billing.order

import org.apache.log4j.Logger
import org.springframework.context.annotation.Lazy
import org.springframework.stereotype.Service
import org.springframework.util.Assert
import ru.planeta.api.exceptions.NotFoundException
import ru.planeta.api.exceptions.OrderException
import ru.planeta.api.exceptions.PermissionException
import ru.planeta.api.service.biblio.BookService
import ru.planeta.api.service.biblio.LibraryService
import ru.planeta.api.service.notifications.NotificationService
import ru.planeta.api.service.profile.AuthorizationService
import ru.planeta.api.service.security.PermissionService
import ru.planeta.api.utils.OrderUtils
import ru.planeta.dao.profiledb.ProfileDAO
import ru.planeta.model.bibliodb.Bin
import ru.planeta.model.bibliodb.BookOrder
import ru.planeta.model.bibliodb.Library
import ru.planeta.model.bibliodb.enums.BiblioObjectStatus
import ru.planeta.model.common.Order
import ru.planeta.model.common.OrderObject
import ru.planeta.model.enums.OrderObjectType
import ru.planeta.model.enums.ProjectType
import ru.planeta.model.shop.enums.PaymentType
import java.math.BigDecimal
import java.util.*

/*
 * Created by a.savanovich on 17.10.2016.
 */

@Service
class BiblioAfterFundService(
        @Lazy
        private val orderService: OrderService,
        @Lazy
        private val notificationService: NotificationService,
        private val profileDAO: ProfileDAO,
        private val libraryService: LibraryService,
        private val bookService: BookService,
        private val permissionService: PermissionService,
        private val authorizationService: AuthorizationService) : AfterFundService {

    @Throws(NotFoundException::class, PermissionException::class, OrderException::class)
    override fun purchase(order: Order, orderObjects: List<OrderObject>) {
        sendPurchaseBiblioNotificationEmail(order)
        var buyerName: String? = null
        val investInfo = order.investInfo

        val libraries = ArrayList<Library>()
        val bookOrders = ArrayList<BookOrder>()

        for (info in orderObjects) {
            if (info.orderObjectType === OrderObjectType.LIBRARY) {
                val library = libraryService.getLibrary(info.objectId)
                if (library != null) {
                    libraries.add(library)
                }
            } else if (info.orderObjectType === OrderObjectType.BOOK) {
                val book = bookService.getBookById(info.objectId)

                if (bookOrders.isEmpty()) {
                    bookOrders.add(BookOrder(book, 1))
                } else {
                    var found = false
                    for (bo in bookOrders) {
                        if (bo.book!!.bookId == book.bookId) {
                            bo.count = bo.count + 1
                            found = true
                            break
                        }
                    }
                    if (!found) {
                        bookOrders.add(BookOrder(book, 1))
                    }
                }
            }
        }

        if (investInfo != null) {
            log.info("investInfo.orgName (buyerName) = " + investInfo.orgName!!)
            buyerName = investInfo.orgName
        } else {
            val profile = profileDAO.selectById(order.buyerId)

            if (profile != null) {
                buyerName = profile.displayName
            }
        }

        val email = authorizationService.getUserPrivateEmailById(order.buyerId)
        notificationService.sendBiblioPurchaseNotification(libraries, bookOrders, buyerName!!, email!!)
    }

    override fun isMyType(type: OrderObjectType): Boolean {
        return OrderObjectType.BIBLIO === type
    }

    @Throws(PermissionException::class, NotFoundException::class)
    private fun sendPurchaseBiblioNotificationEmail(result: Order) {
        val orderInfo = orderService.getOrderInfo(result)
        notificationService.sendPurchaseBiblioNotificationEmail(orderInfo)
    }

    @Throws(PermissionException::class, NotFoundException::class, OrderException::class)
    fun createBiblioOrder(clientId: Long, buyerId: Long, biblioMerchantId: Long, donateAmount: BigDecimal?, biblioBin: Bin): Order {
        permissionService.checkIsAdmin(clientId, buyerId)

        val now = Date()
        val orderObjects = ArrayList<OrderObject>()
        val order = Order()
        order.buyerId = buyerId
        order.orderType = OrderObjectType.BIBLIO
        order.timeAdded = now
        order.timeUpdated = now
        order.totalPrice = donateAmount ?: BigDecimal.ZERO
        order.paymentType = PaymentType.NOT_SET
        order.projectType = ProjectType.BIBLIO

        for (book in biblioBin.books) {
            val count = book.count
            val bookSelected = bookService.getBookById(book.book!!.bookId)
            Assert.isTrue(bookSelected.status === BiblioObjectStatus.ACTIVE, "Wrong book status")
            Assert.isTrue(count > 0, "Can not create order with count nested objects less than zero.")
            for (i in 0 until count) {
                val orderObject = OrderObject()
                orderObject.merchantId = biblioMerchantId
                orderObject.objectId = book.book!!.bookId
                orderObject.orderObjectType = OrderObjectType.BOOK
                orderObject.price = book.book!!.price
                orderObject.comment = null
                orderObject.estimatedDeliveryTime = null
                orderObjects.add(orderObject)
            }
        }

        for (lib in biblioBin.libraries) {
            val librarySelected = libraryService.getLibrary(lib.libraryId)
            Assert.isTrue(librarySelected!!.status === BiblioObjectStatus.ACTIVE, "Wrong book status")
            val orderObject = OrderObject()
            orderObject.merchantId = biblioMerchantId
            orderObject.objectId = lib.libraryId
            orderObject.orderObjectType = OrderObjectType.LIBRARY
            orderObject.price = BigDecimal.ZERO
            orderObject.comment = null
            orderObject.estimatedDeliveryTime = null
            orderObjects.add(orderObject)
        }

        var objectsPrice = OrderUtils.calculateOrderObjectsPrice(orderObjects)
        if (order.totalPrice.compareTo(objectsPrice) > 0) {
            objectsPrice = order.totalPrice
        }
        order.totalPrice = objectsPrice.add(order.deliveryPrice)
        orderService.storeToDb(order, orderObjects)

        return order
    }

    companion object {

        private val log = Logger.getLogger(BiblioAfterFundService::class.java)
    }


}

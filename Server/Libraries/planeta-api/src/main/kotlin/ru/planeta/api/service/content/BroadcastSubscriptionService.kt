package ru.planeta.api.service.content

import ru.planeta.api.model.BroadcastSubscriptionDTO
import ru.planeta.model.profile.broadcast.BroadcastSubscription

import java.util.Date

/**
 * Broadcast subscriptions service
 *
 * @author: ds.kolyshev
 * Date: 16.05.13
 */
interface BroadcastSubscriptionService {

    /**
     * Adds new broadcast subscription
     *
     * @param clientId
     * @param broadcastSubscription
     */
    fun addBroadcastSubscription(clientId: Long, broadcastSubscription: BroadcastSubscription)

    fun addBroadcastSubscription(dto: BroadcastSubscriptionDTO)

    /**
     * Deletes specified broadcast subscription
     *
     * @param clientId
     * @param profileId
     * @param broadcastId
     */
    fun deleteBroadcastSubscription(clientId: Long, profileId: Long, broadcastId: Long)

    /**
     * Gets broadcast subscription for specified client
     *
     * @param clientId subscriber identifier
     * @param profileId
     * @param broadcastId
     * @return
     */
    fun getBroadcastSubscription(clientId: Long, profileId: Long, broadcastId: Long): BroadcastSubscription

    /**
     * Notifies subscriptions for upcoming broadcasts of specified date
     *
     * @param date
     */
    fun notifyUpcomingBroadcastSubscriptions(date: Date)
}

package ru.planeta.api.search

import org.sphx.api.SphinxClient
import org.sphx.api.SphinxException
import ru.planeta.api.exceptions.NotFoundException
import ru.planeta.api.exceptions.PermissionException
import ru.planeta.api.model.json.UserCallbackInfo
import ru.planeta.api.news.ProfileNewsDTO
import ru.planeta.api.search.filters.SearchOrderFilter
import ru.planeta.model.bibliodb.Library
import ru.planeta.model.bibliodb.LibraryFilter
import ru.planeta.model.common.InvestingOrderInfo
import ru.planeta.model.common.OrderInfo
import ru.planeta.model.common.campaign.Campaign
import ru.planeta.model.common.campaign.CampaignTag
import ru.planeta.model.common.campaign.ShareForSearch
import ru.planeta.model.common.campaign.enums.CampaignStatus
import ru.planeta.model.common.school.SeminarWithTagNameAndCityName
import ru.planeta.model.enums.ModerateStatus
import ru.planeta.model.enums.PostFilterType
import ru.planeta.model.enums.ProfileType
import ru.planeta.model.profile.ProfileForAdminsWithEmail
import ru.planeta.model.profile.broadcast.Broadcast
import ru.planeta.model.profile.broadcast.enums.BroadcastCategory
import ru.planeta.model.shop.ProductInfo
import java.util.Date

/**
 * Service for full-text searches.
 *
 * @author s.fionov
 */
interface SearchService {

    @Throws(SphinxException::class, PermissionException::class)
    fun adminSearchForProfiles(query: String, type: ProfileType, offset: Int, limit: Int): SearchResult<ProfileForAdminsWithEmail>

    /**
     * Search for users
     *
     * @param clientId       Identifier of the user that does the search
     * @param query          Search query
     * @param offset         Offset
     * @param limit          Limit
     * @return Users search result
     */
    @Throws(SphinxException::class)
    fun searchForUsers(clientId: Long, query: String, offset: Int, limit: Int): SearchResult<ProfileSearchResult>

    /**
     * Search for blogs
     *
     * @param clientId    User that does the search
     * @param query       Query
     * @param profileId   Blog post owner profile id
     * @param offset      Offset
     * @param limit       Limit
     * @return Blogs search result
     */
    @Throws(SphinxException::class)
    fun searchForNewsPosts(clientId: Long, query: String, profileId: Long?, filterType: PostFilterType?,
                           offset: Int, limit: Int): SearchResult<ProfileNewsDTO>

    @Throws(SphinxException::class)
    fun searchForLibraries(filter: LibraryFilter): SearchResult<Library>

    @Throws(SphinxException::class)
    fun searchForSeminarsByTagId(tagId: Long?, dateFrom: Date, offset: Int, limit: Int): SearchResult<SeminarWithTagNameAndCityName>

    @Throws(SphinxException::class)
    fun searchForCampaignsAdmins(clientId: Long, query: String, offset: Int, limit: Int, campaignStatus: CampaignStatus,
                                 managerId: Long?, timeAddedRange: Range<Date>, sortOrderList: List<SortOrder>, campaignTagId: Int?): SearchResult<Campaign>

    @Throws(SphinxException::class)
    fun searchForCampaigns(searchCampaigns: SearchCampaigns): SearchResult<Campaign>

    @Throws(SphinxException::class)
    fun getTopFilteredCampaignsSearchResult(searchCampaigns: SearchCampaigns): SearchResult<Campaign>

    @Throws(SphinxException::class)
    fun searchForCampaignRelated(tag: List<CampaignTag>, ignoreCampaignId: Long): Collection<Campaign>?

    fun searchForCampaignRelatedForFinishedCampaign(campaign: Campaign): Collection<Campaign>

    @Throws(SphinxException::class)
    fun searchForBroadcasts(clientId: Long, sortBy: Int, query: String, profileId: Int?, filterField: String?,
                            offset: Int, limit: Int, broadcastCategory: BroadcastCategory): SearchResult<Broadcast>

    @Throws(SphinxException::class)
    fun searchForBroadcasts(query: String, dateFrom: Date, dateTo: Date, offset: Int, limit: Int): SearchResult<Broadcast>

    @Throws(SphinxException::class)
    fun searchForSeminars(query: String, dateFrom: Date, dateTo: Date, offset: Int, limit: Int): SearchResult<SeminarWithTagNameAndCityName>

    @Throws(SphinxException::class, NotFoundException::class)
    fun searchForCampaignOrders(searchOrderFilter: SearchOrderFilter): SearchResult<OrderInfo>

    @Throws(SphinxException::class, NotFoundException::class)
    fun searchForMerchantOrders(searchOrderFilter: SearchOrderFilter): SearchResult<OrderInfo>

    @Throws(SphinxException::class, NotFoundException::class)
    fun searchForOrders(searchOrderFilter: SearchOrderFilter): SearchResult<OrderInfo>

    @Throws(SphinxException::class, NotFoundException::class)
    fun searchForBiblioOrders(searchOrderFilter: SearchOrderFilter): SearchResult<OrderInfo>

    @Throws(SphinxException::class, NotFoundException::class, PermissionException::class)
    fun searchForUserCallback(clientId: Long, query: String, processed: Boolean?, dateFrom: Date, dateTo: Date, offset: Int, limit: Int): SearchResult<UserCallbackInfo>

    @Throws(SphinxException::class, NotFoundException::class)
    fun searchForInvestingOrderInfo(query: String, status: ModerateStatus, dateFrom: Date, dateTo: Date, offset: Int,
                                    limit: Int): SearchResult<InvestingOrderInfo>

    @Throws(SphinxException::class)
    fun searchForProductsNew(productsSearch: ProductsSearch): List<ProductInfo>

    @Throws(SphinxException::class)
    fun searchForProducts(productsSearch: ProductsSearch): SearchResult<ProductInfo>

    @Throws(SphinxException::class)
    fun searchForShares(searchShares: SearchShares): SearchResult<ShareForSearch>

    @Throws(SphinxException::class)
    fun searchSharesForWelcome(searchShares: SearchShares): List<ShareForSearch>
}

package ru.planeta.api.model.json

import java.math.BigDecimal
import java.util.Calendar

import ru.planeta.commons.lang.DateUtils.*

/**
 * Created by eshevchenko.
 */
class UserCallbackAvailabilityCondition {

    private var stdHourFrom: Int? = null
    private var stdHourTo: Int? = null
    private var addHourFrom: Int? = null
    private var addHourTo: Int? = null
    private var addDays: IntArray? = null
    private var holidays: Array<Calendar>? = null
    var minAmount: BigDecimal? = null
        get() = if (field == null) Defaults.MIN_AMOUNT else field

    private object Defaults {
        internal val ADD_DAYS = intArrayOf(Calendar.SATURDAY)
        internal val STD_HOURS_FROM = 11
        internal val STD_HOURS_TO = 19
        internal val ADD_HOURS_FROM = 11
        internal val ADD_HOURS_TO = 18
        internal val MIN_AMOUNT = BigDecimal(5000)
        internal val holidays = arrayOf(Calendar.Builder().set(Calendar.YEAR, Calendar.getInstance().get(Calendar.YEAR))
                .set(Calendar.MONTH, Calendar.JUNE).set(Calendar.DATE, 11).build(), Calendar.Builder().set(Calendar.YEAR, Calendar.getInstance().get(Calendar.YEAR))
                .set(Calendar.MONTH, Calendar.JUNE).set(Calendar.DATE, 12).build(), Calendar.Builder().set(Calendar.YEAR, Calendar.getInstance().get(Calendar.YEAR))
                .set(Calendar.MONTH, Calendar.NOVEMBER).set(Calendar.DATE, 5).build())
    }

    fun getStdHourFrom(): Int? {
        return if (stdHourFrom == null) Defaults.STD_HOURS_FROM else stdHourFrom
    }

    fun setStdHourFrom(stdHourFrom: Int?) {
        this.stdHourFrom = stdHourFrom
    }

    fun getStdHourTo(): Int? {
        return if (stdHourTo == null) Defaults.STD_HOURS_TO else stdHourTo
    }

    fun setStdHourTo(stdHourTo: Int?) {
        this.stdHourTo = stdHourTo
    }

    fun getAddHourFrom(): Int? {
        return if (addHourFrom == null) Defaults.ADD_HOURS_FROM else addHourFrom
    }

    fun setAddHourFrom(addHourFrom: Int?) {
        this.addHourFrom = addHourFrom
    }

    fun getAddHourTo(): Int? {
        return if (addHourTo == null) Defaults.ADD_HOURS_TO else addHourTo
    }

    fun setAddHourTo(addHourTo: Int?) {
        this.addHourTo = addHourTo
    }

    fun getAddDays(): IntArray {
        return addDays ?: Defaults.ADD_DAYS
    }

    fun setAddDays(addDays: IntArray) {
        this.addDays = addDays
    }

    fun getHolidays(): Array<Calendar> {
        return holidays ?: Defaults.holidays
    }

    fun setHolidays(holidays: Array<Calendar>) {
        this.holidays = holidays
    }

    fun check(calendar: Calendar, amount: BigDecimal): Boolean {
        var addDay = false
        for (day in addDays!!) {
            if (isDayWeekToday(calendar, day)) {
                addDay = true
                break
            }
        }
        var isHoliday = false
        for (holiday in holidays!!) {
            if (isToday(calendar, holiday)) {
                isHoliday = true
                break
            }
        }

        return !isHoliday && (addDay && isHoursBetween(calendar, addHourFrom!!, addHourTo!! - 1) ||
                isWeekday(calendar) && isHoursBetween(calendar, stdHourFrom!!, stdHourTo!! - 1)) && amount.compareTo(minAmount) >= 0
    }
}


package ru.planeta.api.service.profile

import org.springframework.stereotype.Service
import ru.planeta.dao.profiledb.BaseProfileLastVisitService
import ru.planeta.model.enums.ProfileLastVisitType
import ru.planeta.model.profiledb.ProfileLastVisit
import java.util.*

@Service
class ProfileLastVisitServiceImpl : BaseProfileLastVisitService(), ProfileLastVisitService {
    override fun insertOrUpdateProfileLastVisit(profileId: Long, profileLastVisitType: ProfileLastVisitType) {
        insertOrUpdateProfileLastVisit(profileId, profileLastVisitType, 0L)
    }

    override fun insertOrUpdateProfileLastVisit(profileId: Long, profileLastVisitType: ProfileLastVisitType, objectId: Long) {
        super.insertOrUpdateProfileLastVisit(ProfileLastVisit.builder.profileId(profileId).profileLastVisitType(profileLastVisitType).objectId(objectId).build())
    }

    override fun selectProfileLastVisit(profileId: Long, profileLastVisitType: ProfileLastVisitType): Date {
        return selectProfileLastVisit(profileId, profileLastVisitType, 0L)
    }

    override fun selectProfileLastVisit(profileId: Long, profileLastVisitType: ProfileLastVisitType, objectId: Long): Date {
        var profileLastVisit: ProfileLastVisit? = super.selectProfileLastVisit(profileId, profileLastVisitType, objectId)
        if (profileLastVisit == null) {
            profileLastVisit = super.selectProfileLastVisit(0L, profileLastVisitType, 0L)
        }
        return profileLastVisit.timeUpdated ?: Date()
    }


}

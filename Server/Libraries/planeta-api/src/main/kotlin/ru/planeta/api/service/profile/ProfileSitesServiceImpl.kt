package ru.planeta.api.service.profile


import org.springframework.stereotype.Service
import ru.planeta.dao.profiledb.BaseProfileSitesService
import ru.planeta.model.profiledb.ProfileSites

@Service
class ProfileSitesServiceImpl : BaseProfileSitesService(), ProfileSitesService {

    override fun selectProfileSites(profileId: Long): ProfileSites {
        var ps: ProfileSites? = profileSitesMapper!!.selectByPrimaryKey(profileId)
        if (ps == null) {
            ps = createEmptyObject()
            ps.profileId = profileId
        }
        return ps
    }

    private fun createEmptyObject(): ProfileSites {
        val ps = ProfileSites()
        ps.siteUrl = ""
        ps.twitterUrl = ""
        ps.googleUrl = ""
        ps.facebookUrl = ""
        ps.vkUrl = ""
        return ps
    }


}

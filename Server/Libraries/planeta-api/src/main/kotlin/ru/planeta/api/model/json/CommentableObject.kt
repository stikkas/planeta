package ru.planeta.api.model.json

import ru.planeta.model.enums.ObjectType
import ru.planeta.model.profile.ICommentable

/**
 * Created by IntelliJ IDEA.
 * Date: 24.10.12
 * Time: 16:15
 * To change this template use File | Settings | File Templates.
 */
class CommentableObject : ICommentable {

    override var profileId: Long? = 0
    override var objectId: Long = 0
    override var objectType: ObjectType? = null

    constructor() {}

    constructor(profileId: Long, objectId: Long, objectType: ObjectType) {
        this.profileId = profileId
        this.objectId = objectId
        this.objectType = objectType

    }
}

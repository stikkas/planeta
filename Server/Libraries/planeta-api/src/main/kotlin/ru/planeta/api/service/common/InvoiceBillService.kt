package ru.planeta.api.service.common

import ru.planeta.model.common.InvoiceBill

/**
 *
 * User: Serge Blagodatskih<stikkas17></stikkas17>@gmail.com><br></br>
 * Date: 16.02.16<br></br>
 * Time: 16:33
 */
interface InvoiceBillService {

    /**
     * @param clientId client profile identificator
     * @param number  bill number (transaction identificator)
     * @return bill
     */
    fun getProfileBillByNumber(clientId: Long, number: Long): InvoiceBill

    /**
     * @param clientId client profile identificator
     * @param searchString search criteria by merchantName or campaignName
     * @param offset start of data
     * @param limit  amount of data
     * @return bills
     */
    fun getProfileBills(clientId: Long, searchString: String, offset: Int, limit: Int): List<InvoiceBill>
}

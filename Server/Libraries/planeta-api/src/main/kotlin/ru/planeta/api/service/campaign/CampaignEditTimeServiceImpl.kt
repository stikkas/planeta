package ru.planeta.api.service.campaign

import org.apache.log4j.Logger
import org.springframework.stereotype.Service
import ru.planeta.api.aspect.transaction.NonTransactional
import ru.planeta.api.exceptions.MessageCode
import ru.planeta.api.exceptions.NotFoundException
import ru.planeta.api.exceptions.PermissionException
import ru.planeta.api.helper.ProjectService
import ru.planeta.api.service.profile.ProfileService
import ru.planeta.dao.commondb.CampaignEditTimeDAO
import ru.planeta.model.enums.ProjectType

@Service
class CampaignEditTimeServiceImpl(private var campaignEditTimeDAO: CampaignEditTimeDAO,
                                  private var profileService: ProfileService,
                                  private var projectService: ProjectService) : CampaignEditTimeService {

    @NonTransactional
    @Throws(PermissionException::class)
    override fun updateAndCheck(campaignId: Long, profileId: Long) {
        if (profileId > 0 && campaignId > 0) {
            val newRecord = if (campaignEditTimeDAO.checkIfExists(campaignId, profileId))
                campaignEditTimeDAO.update(campaignId, profileId)
            else
                campaignEditTimeDAO.insert(campaignId, profileId)

            val last = campaignEditTimeDAO.selectLast(campaignId, profileId)
            if (last != null && newRecord.lastTimeActive!!.time - last.lastTimeActive!!.time < MIN_DIFFERENCE) {
                val otherProfileId = last.profileId
                try {
                    throw PermissionException(MessageCode.CAMPAIGN_IS_EDITING,
                            arrayOf<Any>(projectService.getUrl(ProjectType.MAIN, otherProfileId.toString()),
                                    profileService.getProfileSafe(otherProfileId).displayName as Any))
                } catch (ex: NotFoundException) {
                    log.error(ex)
                }

            }
        }
    }

    companion object {

        private val MIN_DIFFERENCE = 180000

        private val log = Logger.getLogger(CampaignEditTimeServiceImpl::class.java)
    }

}

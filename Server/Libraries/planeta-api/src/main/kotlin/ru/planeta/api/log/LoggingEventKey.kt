package ru.planeta.api.log

import org.apache.log4j.spi.LoggingEvent
import org.apache.log4j.spi.ThrowableInformation
import ru.planeta.commons.web.WebUtils

import javax.servlet.http.HttpServletRequest

/**
 * @author p.vyazankin
 * @since 3/5/13 11:48 AM
 */
class LoggingEventKey internal constructor(val loggingEvent: LoggingEvent, request: HttpServletRequest?) {
    private val message: Any?
    private val exceptionClass: Class<*>?
    val httpRequestData: String
    private val requestUri: String?

    init {    // this part of logic could be changed later (it's a grouping logic)
        this.message = loggingEvent.getMessage()
        val throwableInformation = loggingEvent.getThrowableInformation()

        this.exceptionClass = if (throwableInformation != null) throwableInformation!!.getThrowable().javaClass else null
        this.httpRequestData = WebUtils.requestDataToString(request)
        this.requestUri = request?.requestURI
    }

    override fun equals(o: Any?): Boolean {
        if (this === o) return true
        if (o == null || javaClass != o.javaClass) return false

        val that = o as LoggingEventKey?

        if (if (exceptionClass != null) exceptionClass != that!!.exceptionClass else that!!.exceptionClass != null) {
            return false
        }

        if (message != null && message != that.message) {
            return false
        }

        return if (requestUri != null && requestUri != that.requestUri) {
            false
        } else true


    }

    override fun hashCode(): Int {
        var result = message!!.hashCode()
        result = 31 * result + (exceptionClass?.hashCode() ?: 0)
        return result
    }
}

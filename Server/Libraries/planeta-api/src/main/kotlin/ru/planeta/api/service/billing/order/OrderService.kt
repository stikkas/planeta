package ru.planeta.api.service.billing.order

import ru.planeta.api.exceptions.*
import ru.planeta.api.model.json.DeliveryInfo
import ru.planeta.model.common.*
import ru.planeta.model.common.campaign.Campaign
import ru.planeta.model.common.campaign.CampaignSearchFilter
import ru.planeta.model.enums.OrderObjectType
import ru.planeta.model.enums.ProjectType
import ru.planeta.model.shop.enums.DeliveryStatus
import ru.planeta.model.shop.enums.PaymentStatus
import java.math.BigDecimal
import java.util.*

/**
 * Service provides orders creation and management methods.
 * User: eshevchenko
 */
interface OrderService {

    /**
     * Gets a collection of buyer orders in accordance with the specified parameters.
     * In the result collection does not include unpaid orders with non-cash payment type.
     *
     * @param clientId   client profile identifier;
     * @param buyerId    buyer profile identifier;
     * @param objectType querying order objects type;
     * @param dateFrom   beginning of the date range;
     * @param dateTo     end of the date range;
     * @param offset     count skipped orders from beginning;
     * @param limit      count querying orders. If limit equals 0, then parameters `offset` and `limit` will be ignored;
     * @return collection of fully filled [OrderInfo].
     * @throws PermissionException
     * @throws NotFoundException
     */
    @Throws(PermissionException::class, NotFoundException::class)
    fun getBuyerOrdersInfo(clientId: Long, buyerId: Long, objectType: OrderObjectType?, dateFrom: Date?, dateTo: Date?, offset: Int, limit: Int): Collection<OrderInfo>

    @Throws(PermissionException::class, NotFoundException::class)
    fun getProfilePurchaseOrders(clientId: Long, query: String?, buyerId: Long, objectType: OrderObjectType?, offset: Int, limit: Int): Collection<OrderInfo>

    @Throws(PermissionException::class, NotFoundException::class)
    fun getUserOrdersRewardsInfo(buyerId: Long, offset: Int, limit: Int): Collection<OrderInfoForProfilePage>

    /**
     * Gets a collection of merchant orders in accordance with the specified parameters.
     * In the result collection does not include unpaid orders with non-cash payment type.
     *
     * @param clientId       client profile identifier;
     * @param merchantId     merchant identifier;
     * @param objectType     querying order objects type;
     * @param paymentStatus  payment status;
     * @param deliveryStatus delivery status;
     * @param dateFrom       beginning of the date range;
     * @param dateTo         end of the date range;
     * @param offset         count skipped orders from beginning;
     * @param limit          count querying orders. If limit equals 0, then parameters `offset` and `limit` will be ignored;
     * @return collection of fully filled [OrderInfo].
     * @throws NotFoundException
     * @throws PermissionException
     */
    @Throws(PermissionException::class, NotFoundException::class)
    fun getMerchantOrdersInfo(clientId: Long, merchantId: Long, objectType: OrderObjectType, paymentStatus: PaymentStatus?, deliveryStatus: DeliveryStatus?, dateFrom: Date?, dateTo: Date?, offset: Int, limit: Int): Collection<OrderInfo>

    @Throws(NotFoundException::class)
    fun getOrders(orderIds: Collection<Long>?, orderObjectType: OrderObjectType?, paymentStatus: PaymentStatus?, from: Date?, to: Date?, offset: Int, limit: Int): Collection<OrderInfo>

    @Throws(NotFoundException::class)
    fun getOrders(email: String, orderObjectType: OrderObjectType?, paymentStatus: PaymentStatus?, from: Date?, to: Date?, offset: Int, limit: Int): Collection<OrderInfo>

    /**
     * deprecated. use next method
     * Gets order by identifier.
     *
     * @param orderId order identifier;
     * @return order.
     * @throws NotFoundException
     */
    @Deprecated("")
    @Throws(NotFoundException::class)
    fun getOrderSafe(orderId: Long): Order

    @Throws(NotFoundException::class, PermissionException::class)
    fun getOrderSafe(clientId: Long, orderId: Long): Order

    @Throws(NotFoundException::class)
    fun getOrderForUpdateSafe(orderId: Long): Order

    @Throws(NotFoundException::class, PermissionException::class)
    fun getOrderInfo(clientId: Long, orderId: Long): OrderInfo

    // use getOrderInfo(long clientId, long orderId)
    @Deprecated("")
    @Throws(NotFoundException::class, PermissionException::class)
    fun getOrderInfo(order: Order): OrderInfo

    @Deprecated("")
    fun getOrder(orderId: Long): Order?

    fun getOrderForUpdate(orderId: Long): Order?

    /**
     * Gets all order's objects by order identifier..
     *
     * @param orderId order identifier.
     * @return order's objects list. Empty list, If presented order not exist.
     */
    fun getOrderObjects(orderId: Long): List<OrderObject>

    fun getOrdersCountForObject(objectId: Long, objectType: OrderObjectType, paymentStatuses: EnumSet<PaymentStatus>, deliveryStatuses: EnumSet<DeliveryStatus>?): Int

    fun getMinCompletedOrderId(buyerId: Long, objectType: OrderObjectType): Long

    @Deprecated("")
    fun getOrderObject(orderObjectId: Long): OrderObject?

    /**
     * Assign order with specified payment transaction identifier.
     *
     * @param order to assign
     */
    fun assignOrderWithPayment(order: Order, transaction: TopayTransaction)

    @Throws(NotFoundException::class)
    fun markOrderAsFailed(orderId: Long, e: BaseException)

    /**
     * TODO make private. Use only inside
     * Tries delivery (set order's delivery status to **DELIVERED**) order.
     * This operation allowed only for paid orders.
     *
     *
     * After successfully purchasing, statuses are set to:
     *
     *  * payment status: **COMPLETED**;
     *  * delivery status: **DELIVERED**;
     *
     *
     * @param clientId           client profile identifier;
     * @param orderId            order identifier;
     * @param cashOnDeliveryCost @throws NotFoundException
     * @throws PermissionException
     * @throws OrderException
     */
    @Throws(NotFoundException::class, PermissionException::class, OrderException::class)
    fun delivery(clientId: Long, orderId: Long, trackingCode: String?, cashOnDeliveryCost: BigDecimal, deliveryComment: String?, isSendNotification: Boolean, deliveryStatus: DeliveryStatus)

    /**
     * Tries perform order purchase.
     * If in buyer balance enough money for purchase, and all order's objects are available for purchase, then performed purchase.
     * Funds transfer from buyer to merchants balances, according to the contained order's objects.
     *
     *
     * After successfully purchasing, statuses are set to:
     *
     *  * payment status: **COMPLETED**;
     *  * delivery status: **PENDING**;
     *
     *
     * @param order order for purchase;
     * @throws PermissionException
     * @throws NotFoundException
     * @throws OrderException
     */
    @Deprecated("")
    @Throws(PermissionException::class, NotFoundException::class, OrderException::class)
    fun purchase(order: Order)

    @Throws(PermissionException::class, NotFoundException::class, OrderException::class)
    fun purchase(orderId: Long)

    /**
     * Creates order with share object and donate amount.<br></br>
     * `donateAmount` can be greater than share price.
     * If `donateAmount` less than share price [OrderException] will be thrown.
     *
     * @param clientId        client profile identifier;
     * @param buyerId         buyer profile identifier;
     * @param shareId         share identifier;
     * @param donateAmount    donate amount;
     * @param count           shares count
     * @return order.
     * @throws PermissionException
     * @throws NotFoundException
     * @throws OrderException
     */
    @Throws(PermissionException::class, NotFoundException::class, OrderException::class)
    fun createOrderWithShare(clientId: Long, buyerId: Long, shareId: Long, donateAmount: BigDecimal?, count: Int,
                             buyerAnswerToMerchantQuestion: String?, deliveryAddress: DeliveryAddress?, deliveryInfo: DeliveryInfo?, projectType: ProjectType): Order

    fun storeToDb(order: Order, orderObjects: Collection<OrderObject>)

    @Throws(PermissionException::class, OrderException::class, NotFoundException::class)
    fun createBonusOrder(clientId: Long, bonusId: Long, deliveryAddress: DeliveryAddress, deliveryInfo: DeliveryInfo): Order

    @Throws(PermissionException::class, NotFoundException::class, OrderException::class)
    fun createDeliveryOrder(clientId: Long, buyerId: Long, amount: BigDecimal, projectType: ProjectType): Order

    /**
     *
     *  Cancels share, product, delivery and bonus orders with no reason and no notification to user
     * @param clientId client profile identifier;
     * @param orderId  order identifier;
     */
    @Throws(PermissionException::class, OrderException::class, NotFoundException::class)
    fun cancelOrderQuietly(clientId: Long, orderId: Long)

    /**
     *
     *  Cancels share order.
     * Money returns from merchants balances to buyer balance.
     * This operation allowed only for paid orders.
     * Rollback purchase count for share, and, if need, automatically restarts campaign.
     *
     *
     * Sets order's statuses to:
     *
     *  * payment status: **CANCELLED**;
     *  * delivery status: **CANCELLED**;
     *
     *
     * @param clientId client profile identifier;
     * @param orderId  order with share identifier;
     * @throws NotFoundException
     * @throws PermissionException
     * @throws OrderException
     */
    @Throws(PermissionException::class, OrderException::class, NotFoundException::class)
    fun cancelSharePurchase(clientId: Long, orderId: Long, reason: String?, isSendNotification: Boolean)

    /**
     * Tries cancel order.
     * Funds transfer from merchants balances to buyer balance , according to the contained order's objects.
     * This operation allowed only for paid orders.
     *
     *
     * After successfully cancellation, statuses are set to:
     *
     *  * payment status: **CANCELLED**;
     *  * delivery status: **CANCELLED** or **REJECTED**
     *
     *
     * @param clientId       order owner or planeta admin
     * @param orderId        product order
     * @param note           custom message on some states reached
     * @param needNotifyUser whether to notify user
     */
    @Throws(PermissionException::class, NotFoundException::class, OrderException::class)
    fun cancelProductPurchase(clientId: Long, orderId: Long, note: String?, needNotifyUser: Boolean)

    /**
     * Tries cancel order made from delivery-payment page.
     * Funds transfer from planeta balance to buyer balance
     * This operation allowed only for paid orders.
     *
     *
     * After successfully cancellation, statuses are set to:
     *
     *  * payment status: **CANCELLED**;
     *
     *
     * @param clientId       order owner or planeta admin
     * @param orderId        product order
     */
    @Throws(PermissionException::class, NotFoundException::class, OrderException::class)
    fun cancelDeliveryPurchase(clientId: Long, orderId: Long)

    /**
     * Gets [OrderObjectInfo] collection for order.
     *
     * @param clientId client identifier;
     * @param orderId  order identifier;
     * @return fully filled collection of [OrderObjectInfo] for order with specified identifier.
     * @throws PermissionException
     * @throws NotFoundException
     */
    @Throws(PermissionException::class, NotFoundException::class)
    fun getOrderObjectsInfo(clientId: Long, orderId: Long): Collection<OrderObjectInfo>?

    /**
     * Updates order's delivery status.
     *
     * @param clientId        client identifier;
     * @param order         order;
     * @param deliveryStatus  new deliver status;
     * @param deliveryComment @throws PermissionException
     * @throws NotFoundException
     * @throws OrderException
     */
    @Throws(PermissionException::class, NotFoundException::class, OrderException::class)
    fun updateDeliveryStatus(clientId: Long, order: Order, deliveryStatus: DeliveryStatus, trackingCode: String, cashOnDeliveryCost: BigDecimal, deliveryComment: String)

    @Throws(PermissionException::class)
    fun correctBuyerAnswer(clientId: Long, orderId: Long, objectId: Long, answer: String)

    @Throws(NotFoundException::class, PermissionException::class)
    fun addDeliveryAddress(clientId: Long, orderId: Long, deliveryAddress: DeliveryAddress?)

    /**
     * @return collection of order info objects
     * @throws PermissionException
     * @throws NotFoundException
     */
    @Throws(PermissionException::class, NotFoundException::class)
    fun getCampaignOrders(campaignSearchFilter: CampaignSearchFilter): Collection<OrderInfoForReport>

    fun setQuestionToBuyer(orderInfoCollection: Collection<OrderInfo>): Collection<OrderInfo>

    fun getBuyerOrdersStatForVipCondition(profileId: Long): Map<String, Any>

    @Throws(NotFoundException::class, PermissionException::class)
    fun cancelBonusOrder(clientId: Long, orderId: Long)

    @Throws(PermissionException::class, NotFoundException::class, OrderException::class)
    fun cancelBiblioPurchase(clientId: Long, orderId: Long)

    @Throws(NotFoundException::class, PermissionException::class)
    fun completeBonusOrder(clientId: Long, orderId: Long)

    @Throws(PermissionException::class, ChangeProductQuantityException::class, NotFoundException::class, OrderException::class)
    fun changeOrderStatus(clientId: Long, orderId: Long, newDeliveryStatus: DeliveryStatus, sendNotification: Boolean, note: String, trackingCode: String, cashOnDeliveryCost: BigDecimal, deliveryComment: String)

    @Throws(NotFoundException::class)
    fun getCampaignByOrderId(orderId: Long): Campaign

    fun isProfileHasPurchasedObject(profileId: Long, objectId: Long, orderObjectType: OrderObjectType): Boolean
}

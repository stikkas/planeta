package ru.planeta.moscowshow.model.response

import javax.xml.bind.annotation.XmlAccessType
import javax.xml.bind.annotation.XmlAccessorType
import javax.xml.bind.annotation.XmlRootElement
import ru.planeta.moscowshow.model.result.ResultActionSchema

/**
 *
 * User: Serge Blagodatskih<stikkas17></stikkas17>@gmail.com><br></br>
 * Date: 13.10.16<br></br>
 * Time: 18:13
 */
@XmlRootElement(name = "GetActionSchemaExResponse")
@XmlAccessorType(XmlAccessType.FIELD)
class ResponseActionSchema : Response<ResultActionSchema>() {

    override var result: ResultActionSchema? = null
}

package ru.planeta.api.model.json

class VideoInfoForFlash(videoInfo: VideoInfo) : VideoInfo(videoInfo.cachedVideo) {

    override val duration: Int
        get() = super.duration * 1000

    init {
        this.video = videoInfo.video
    }
}

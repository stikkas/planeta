package ru.planeta.api.exceptions

import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ResponseStatus

@ResponseStatus(value = HttpStatus.OK, reason = "Video not found")
class VideoNotFoundException : NotFoundException()

package ru.planeta.api.service.billing.order

import ru.planeta.api.exceptions.NotFoundException
import ru.planeta.model.common.OrderInfo
import ru.planeta.model.common.OrderObjectInfo
import ru.planeta.model.common.Order
import ru.planeta.model.common.OrderObject

/**
 * Collects transient order information.<br></br>
 * Created by eshevchenko.
 */
interface OrderInfoService {

    /**
     *
     * deprecated this method too ugly. not use it
     * Collects additional order information for presented orders.
     *
     * @param orders             orders collection;
     * @param needCollectObjects should be FALSE!!!
     * @return collected orders list.
     * @throws NotFoundException
     */
    @Deprecated("")
    @Throws(NotFoundException::class)
    fun getOrdersInfo(orders: Collection<Order>, needCollectObjects: Boolean): Collection<OrderInfo>

    /**
     * Collects additional order objects information for presented order objects.
     *
     * @param clientId     client profile client profile identifier;
     * @param orderObjects order objects collection;
     * @return collected order objects list.
     */
    fun getOrderObjectsInfo(clientId: Long, orderObjects: Collection<OrderObject>): Collection<OrderObjectInfo>

    @Throws(NotFoundException::class)
    fun getOrdersForOHMPage(offset: Int, limit: Int): List<Map<String, Any>>
}

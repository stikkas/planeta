package ru.planeta.api.search

import java.util.*

/**
 * Fields of the search index
 *
 * @author ameshkov
 */
enum class SearchIndexField(// <editor-fold defaultstate="collapsed" desc="Constructor and properties">
        val label: String, val weight: Int = 1) {

    USER_GENDER("user_gender"),
    USER_DISPLAY_NAME("display_name", 10),

    GROUP_NAME("name", 100),
    GROUP_DESCRIPTION("description", 5),
    GROUP_GENRE("genres"),
    GROUP_USERS_COUNT("users_count"),


    VIDEO_NAME("name", 10),
    VIDEO_DURATION("duration"),
    VIDEO_TIME_ADDED("time_added"),
    VIDEO_TIME_UPDATED("time_updated"),
    VIDEO_IS_HD("is_hd"),
    VIDEO_VIEWS_COUNT("views_count"),

    BROADCAST_TIME_BEGIN("time_begin"),
    BROADCAST_TIME_END("time_end"),
    BROADCAST_TIME_UPDATED("time_updated"),
    BROADCAST_VIEWS_COUNT("views_count"),
    IS_CONCERTS("is_concerts"),
    IS_NASHE_RADIO("is_nashe_radio"),
    IS_ALIVE("is_alive"),
    IS_SCHOOL("is_school"),

    SEMINAR_TIME_START("time_start"),

    PROFILE_ID("profile_id", 10),
    PROFILE_TYPE_ID("profile_type_id"),
    PROFILE_TIME_ADDED("time_added"),
    PROFILE_STATUS("status"),

    CAMPAIGN_ID("campaign_id"),
    CAMPAIGN_NAME("name", 100),
    CAMPAIGN_GROUP_NAME("group_name", 100),
    CAMPAIGN_DESCRIPTION("description", 5),
    CAMPAIGN_SHORT_DESCRIPTION("short_description", 5),
    CAMPAIGN_TAG("campaign_tag"),
    CAMPAIGN_STATUS("status"),
    CAMPAIGN_MANAGER_ID("manager_id"),
    CAMPAIGN_LAST_PURCHASE_DATE("time_last_purchased"),
    CAMPAIGN_LAST_NEWS_PUBLISH_DATE("time_last_news_published"),
    CAMPAIGN_TIME_STARTED("time_start"),
    CAMPAIGN_TIME_FINISHED("time_finish"),
    CAMPAIGN_TIME_ADDED("time_added"),
    CAMPAIGN_TIME_UPDATED("time_updated"),
    CAMPAIGN_TARGET_AMOUNT("target_amount"),
    CAMPAIGN_PURCHASE_SUM("purchase_sum"),
    CAMPAIGN_COLLECTED_SUM("collected_amount"),
    CAMPAIGN_COMPLETE_PERCENTS("complete_percents"),
    CAMPAIGN_MEDIAN_PRICE("median_price"),
    CAMPAIGN_SORT_BY_COLLECTION_RATE("sort_by_collection_rate"),
    CAMPAIGN_SPONSOR("sponsor_alias"),
    CAMPAIGN_TARGET_STATUS("target_status"),
    COMMENTS_COUNT("comments_count"),
    CAMPAIGN_COUNTRY("country_id"),
    CAMPAIGN_REGION("region_id"),
    CAMPAIGN_CITY("city_id"),
    CAMPAIGN_MAIN_PAGE("show_main_page"),
    CAMPAIGN_PURCHASE_PERCENT_OF_TARGET("purchase_percent_of_target"),


    PRODUCT_ID("product_id"),
    PRODUCT_TYPE("type"),
    PRODUCT_PRICE("price"),
    PRODUCT_MERCHANT_PROFILE_ID("merchant_profile_id"),
    PRODUCT_TIME_LAST_PURCHASED("time_last_purchased"),
    PRODUCT_TOTAL_PURCHASE_COUNT("total_purchase_count"),
    PRODUCT_TIME_ADDED("time_added"),
    PRODUCT_CATEGORY("category_id"),
    PRODUCT_REFERRER_ID("referrer_id"),


    ORDER_ORDER_ID("order_id"),
    ORDER_TYPE("order_type"),
    ORDER_TIME_ADDED("time_added"),
    ORDER_PAYMENT_STATUS("payment_status"),
    ORDER_DELIVERY_STATUS("delivery_status"),
    ORDER_CAMPAIGN_ID("campaign_id"),
    ORDER_SHARE_ID("share_id"),
    ORDER_PRODUCT_ID("product_id"),
    ORDER_SHARE_NAME("share_name"),
    ORDER_MERCHANT_ID("merchant_id"),
    ORDER_BUYER_ID("buyer_id"),
    ORDER_BUYER_EMAIL("buyer_email"),

    SEMINAR_ID("seminar_id"),
    SEMINAR_ID_FOR_SEARCH("id_for_search"),
    LIBRARY_ID("library_id"),
    LIBRARY_ID_FOR_SEARCH("id_for_search"),
    LIBRARY_TIME_ADDED("time_added"),
    LIBRARY_STATUS("library_status"),
    LIBRARY_TYPE("library_type"),
    TAG_ID("tag_id"),
    CITY_ID("city_id"),

    USER_CALLBACK_TIME_ADDED("time_added"),
    USER_CALLBACK_PROCESSED("processed"),

    INVESTING_ORDER_INFO_TIME_ADDED("time_added"),
    INVESTING_ORDER_INFO_STATUS("moderate_status"),

    SHARE_PURCHASE_COUNT("purchase_count"),
    SHARE_AMOUNT("amount"),
    SHARE_PRICE("price"),
    SHARE_NAME("name"),
    SHARE_PURCHASE_DATE("order_time_added"),
    SHARE_LEFT_AMOUNT_IN_PERCENT("left_amount_in_percent");


    companion object {

        private val WEIGHT_MAP = HashMap<String, Int>()

        init {
            for (i in SearchIndexField.values()) {
                WEIGHT_MAP[i.label] = i.weight
            }
        }

        val weightMap: MutableMap<String, Int>
            get() = HashMap(WEIGHT_MAP)
    }
    // </editor-fold>
}

package ru.planeta.api.service.campaign

import org.springframework.stereotype.Service
import ru.planeta.dao.commondb.CampaignSubscribeDAO
import ru.planeta.model.common.campaign.CampaignSubscribe

/**
 * Created with IntelliJ IDEA.
 * User: michail
 * Date: 16.11.2017
 * Time: 16:11
 */
@Service
class CampaignSubscribeServiceImpl(private var campaignSubscribeDAO: CampaignSubscribeDAO) : CampaignSubscribeService {

    override fun getCampaignSubsctiptionsToSend(offset: Long, limit: Int): List<CampaignSubscribe> {
        return campaignSubscribeDAO.getCampaignSubsctiptionsToSend(offset, limit)
    }

    override fun isSubscribedOnCampaignEnd(profileId: Long, campaignId: Long): Boolean? {
        return campaignSubscribeDAO.isSubscribedOnCampaignEnd(profileId, campaignId)
    }

    override fun subscribeOnCampaignEnd(profileId: Long, campaignId: Long) {
        if (isSubscribedOnCampaignEnd(profileId, campaignId) == true) {
            return
        }
        campaignSubscribeDAO.subscribeOnCampaignEnd(profileId, campaignId)
    }

    override fun unsubscribeOnCampaignEnd(profileId: Long, campaignId: Long) {
        if ((isSubscribedOnCampaignEnd(profileId, campaignId) != true)) {
            return
        }
        campaignSubscribeDAO.unsubscribeOnCampaignEnd(profileId, campaignId)
    }


}

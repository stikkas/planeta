package ru.planeta.api.model.json

import ru.planeta.model.profile.broadcast.Broadcast
import ru.planeta.model.profile.broadcast.BroadcastStream
import ru.planeta.model.profile.media.Photo

/**
 * @author ds.kolyshev
 * Date: 27.03.12
 */
class BroadcastInfo {
    var broadcast: Broadcast? = null
    var streams: List<BroadcastStream>? = null
    var avatars: List<Photo>? = null
}

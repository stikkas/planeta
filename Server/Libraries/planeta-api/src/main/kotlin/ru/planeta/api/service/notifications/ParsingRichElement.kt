package ru.planeta.api.service.notifications

import org.apache.commons.lang3.math.NumberUtils

import java.util.regex.Matcher
import java.util.regex.Pattern

internal abstract class ParsingRichElement {

    abstract val tag: String

    abstract fun getImage(paramString: String, staticNode: String): String

    fun replace(tx: String, blogPostUrl: String, staticNode: String): String {
        val p1 = Pattern.compile("<p:$tag ([^>]*)>\\s*</p:$tag>")
        val p2 = Pattern.compile("<p:$tag ([^>]*)/>")
        val result = replace(p1, tx, blogPostUrl, staticNode)
        return replace(p2, result, blogPostUrl, staticNode)
    }

    private fun replace(pattern: Pattern, tx: String, blogPostUrl: String, staticNode: String): String {
        val m = pattern.matcher(tx)
        val sb = StringBuilder()
        var copyFrom = 0
        while (m.find()) {
            sb.append(tx.substring(copyFrom, m.start()))
            sb.append("<a target=\"_blank\" href=\"").append(blogPostUrl).append("\">")
                    .append("<img src=\"")
                    .append(getImage(m.group(1), staticNode))
                    .append("\" ")
                    .append("style=\"max-width:100%;\"")
                    .append(">")
                    .append("</a>")
            copyFrom = m.end()
        }
        sb.append(tx.substring(copyFrom))
        return sb.toString()

    }

    companion object {

        fun getAttr(tx: String, attrName: String): String {
            var attrName = attrName
            attrName += "=\""
            var st = tx.indexOf(attrName)
            if (st >= 0) {
                st += attrName.length
                val en = tx.indexOf("\"", st)
                if (en > st) {
                    return tx.substring(st, en)
                }
            }
            return ""
        }

        fun getDuration(paramsString: String): String {
            val duration = getAttr(paramsString, "duration")
            val durationIntMills = NumberUtils.toInt(duration, 0) * 1000
            return if (durationIntMills > 0) durationIntMills.toString() else duration
        }
    }
}

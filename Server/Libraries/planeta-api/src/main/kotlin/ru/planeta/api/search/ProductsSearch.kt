package ru.planeta.api.search

import ru.planeta.model.shop.Category
import java.math.BigDecimal
import java.util.ArrayList
import java.util.EnumSet

class ProductsSearch {
    var query: String? = null
    var productTags: List<Category>? = null
    var productTagsMnemonics: List<String>? = null
    var referrerIds: List<Long>? = null
    var sortOrder = SortOrder.SORT_DEFAULT
    var priceFrom: Int = 0
    var priceTo: Int = 0
    var offset: Int = 0
    var limit = 12

    constructor() {

    }

    constructor(query: String, productTags: List<Category>, referrerIds: List<Long>, sortOrder: SortOrder, priceFrom: Int, priceTo: Int, offset: Int, limit: Int) {
        this.query = query
        this.productTags = productTags
        this.referrerIds = referrerIds
        this.sortOrder = sortOrder
        this.offset = offset
        this.limit = limit
        this.priceFrom = priceFrom
        this.priceTo = priceTo
    }
}

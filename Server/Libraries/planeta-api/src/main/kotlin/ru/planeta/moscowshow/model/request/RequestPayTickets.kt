package ru.planeta.moscowshow.model.request

import javax.xml.bind.annotation.*

/**
 *
 * User: Serge Blagodatskih<stikkas17></stikkas17>@gmail.com><br></br>
 * Date: 13.10.16<br></br>
 * Time: 17:59
 */
@XmlRootElement(name = "payTickets")
@XmlAccessorType(XmlAccessType.FIELD)
class RequestPayTickets : Request {

    @XmlElementWrapper(name = "ticketIDs")
    @XmlElement(name = "long")
    var ids: List<Long>? = null

    constructor()

    constructor(ids: List<Long>) {
        this.ids = ids
    }

}

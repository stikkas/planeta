package ru.planeta.api.service.promo

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import ru.planeta.dao.trashcan.PromoConfigDAO
import ru.planeta.dao.trashcan.PromoEmailDAO
import ru.planeta.model.enums.PromoConfigStatus
import ru.planeta.model.trashcan.PromoConfig

import java.math.BigDecimal
import java.util.ArrayList
import java.util.Date

@Service
class PromoConfigServiceImpl @Autowired
constructor(private val promoConfigDAO: PromoConfigDAO, private val promoEmailDAO: PromoEmailDAO) : PromoConfigService {

    override fun selectAll(): List<PromoConfig> {
        return promoConfigDAO.selectAll()
    }

    override fun getCompatibleConfigs(campaignTags: Collection<Long>, totalPrice: BigDecimal, email: String): List<PromoConfig> {
        val possibleCompatibleConfigs = promoConfigDAO.selectCompatible(campaignTags, totalPrice)
        val compatibleConfigs = ArrayList<PromoConfig>()
        for (pcc in possibleCompatibleConfigs) {
            if (pcc.usageCount == 0) {
                compatibleConfigs.add(pcc)
            } else {
                if (promoEmailDAO.selectSentEmailsCountForUser(email, pcc.configId) < pcc.usageCount) {
                    compatibleConfigs.add(pcc)
                }
            }
        }
        return compatibleConfigs
    }

    override fun insert(clientId: Long, promoConfig: PromoConfig) {
        promoConfig.creatorId = clientId
        promoConfig.timeAdded = Date()
        promoConfigDAO.insert(promoConfig)
    }

    override fun update(clientId: Long, promoConfig: PromoConfig) {
        promoConfig.updaterId = clientId
        promoConfig.timeUpdated = Date()
        promoConfigDAO.update(promoConfig)
    }

    override fun selectById(configId: Long): PromoConfig {
        return promoConfigDAO.selectById(configId)
    }

    override fun switchStatus(clientId: Long, configId: Long) {
        val config = selectById(configId)
        if (config != null) {
            when (config.status) {
                PromoConfigStatus.ACTIVE -> config.status = PromoConfigStatus.PAUSED
                PromoConfigStatus.PAUSED -> config.status = PromoConfigStatus.ACTIVE
            }
            update(clientId, config)
        }
    }

    override fun finishConfigs() {
        promoConfigDAO.finishConfigs()
    }
}

package ru.planeta.api.service.billing.payment

import org.apache.log4j.Logger
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import ru.planeta.api.exceptions.MessageCode
import ru.planeta.api.exceptions.NotFoundException
import ru.planeta.api.exceptions.PermissionException
import ru.planeta.api.service.configurations.ConfigurationService
import ru.planeta.api.service.security.PermissionService
import ru.planeta.dao.commondb.OrderDAO
import ru.planeta.dao.commondb.TopayTransactionDAO
import ru.planeta.dao.commondb.BasePaymentErrorsService
import ru.planeta.model.common.Order
import ru.planeta.model.common.TopayTransaction
import ru.planeta.model.enums.PaymentErrorStatus
import ru.planeta.model.enums.PaymentErrorType
import ru.planeta.model.commondb.PaymentErrors

import java.math.BigDecimal

/**
 *
 * Created by kostiagn on 10.07.2015.
 */
@Service
class PaymentErrorsServiceImpl(
        private var orderDAO: OrderDAO,
        private var configurationService: ConfigurationService,
        private var topayTransactionDAO: TopayTransactionDAO,
        private var permissionService: PermissionService) : BasePaymentErrorsService(), PaymentErrorsService {
    override fun createPaymentErrors(transaction: TopayTransaction) {

        try {
            val largeSum = BigDecimal(configurationService.paymentErrorsLargeSum)

            if (largeSum.compareTo(transaction.amountNet) <= 0) {
                createPaymentErrorsLargeSum(transaction)
            } else {
                val prevFailTransaction = topayTransactionDAO.getPrevFailTransaction(transaction.transactionId)
                if (prevFailTransaction != null) {
                    createPaymentErrorsTwoFailPayments(transaction, prevFailTransaction)
                }
            }
        } catch (e: Exception) {
            log.error("cannot create PaymentErrors by TopayTransaction", e)
        }


    }

    private fun createPaymentErrorsTwoFailPayments(transaction: TopayTransaction, prevFailTransaction: TopayTransaction) {
        var paymentErrors = selectPaymentErrors(prevFailTransaction.transactionId, PaymentErrorType.TWO_FAIL_PAYMENTS)
        if (paymentErrors != null) {
            paymentErrors.transactionId = transaction.transactionId
            paymentErrors.orderId = transaction.orderId
            updatePaymentErrors(paymentErrors)
        } else {
            paymentErrors = PaymentErrors.builder
                    .orderId(transaction.orderId)
                    .transactionId(transaction.transactionId)
                    .profileId(transaction.profileId)
                    .paymentErrorStatus(PaymentErrorStatus.NEW)
                    .paymentErrorType(PaymentErrorType.TWO_FAIL_PAYMENTS)
                    .amount(transaction.amountNet ?: BigDecimal.ZERO)
                    .build()
            setCampaignId(paymentErrors)
            insertPaymentErrors(paymentErrors)
        }
    }

    private fun createPaymentErrorsLargeSum(transaction: TopayTransaction) {
        val paymentErrors = PaymentErrors.builder
                .orderId(transaction.orderId)
                .transactionId(transaction.transactionId)
                .profileId(transaction.profileId)
                .paymentErrorStatus(PaymentErrorStatus.NEW)
                .paymentErrorType(PaymentErrorType.LARGE_SUM_FAIL_PAYMENT)
                .amount(transaction.amountNet ?: BigDecimal.ZERO)
                .build()
        setCampaignId(paymentErrors)
        insertPaymentErrors(paymentErrors)
    }

    private fun setCampaignId(paymentErrors: PaymentErrors) {
        val campaignId = orderDAO.selectCampaignIdByOrderId(paymentErrors.orderId!!)
        paymentErrors.campaignId = campaignId
    }

    override fun createPaymentErrors(order: Order, messageCode: MessageCode) {
        try {
            val topayTransaction = topayTransactionDAO.select(order.topayTransactionId)
            val paymentErrors = PaymentErrors.builder
                    .orderId(order.orderId)
                    .transactionId(order.topayTransactionId)
                    .profileId(order.buyerId)
                    .paymentErrorStatus(PaymentErrorStatus.NEW)
                    .amount(topayTransaction?.amountNet ?: BigDecimal.ZERO)
                    .build()

            when (messageCode) {
                MessageCode.CAMPAIGN_NOT_ACTIVE -> paymentErrors.paymentErrorType = PaymentErrorType.CAMPAIGN_NOT_ACTIVE
                MessageCode.NOT_ENOUGH_SHARE_AMOUNT -> paymentErrors.paymentErrorType = PaymentErrorType.NOT_ENOUGH_SHARE_AMOUNT
                MessageCode.NOT_ENOUGH_PRODUCT_AMOUNT -> paymentErrors.paymentErrorType = PaymentErrorType.NOT_ENOUGH_PRODUCT_AMOUNT
                MessageCode.ORDER_NOT_ENOUGH_MONEY_FOR_PURCHASE -> paymentErrors.paymentErrorType = PaymentErrorType.ORDER_NOT_ENOUGH_MONEY_FOR_PURCHASE
                else -> {
                    paymentErrors.paymentErrorType = PaymentErrorType.OTHER_ERRORS
                    paymentErrors.setErrorText(order.errorCode)
                }
            }
            setCampaignId(paymentErrors)
            insertPaymentErrors(paymentErrors)
        } catch (e: Exception) {
            log.error("cannot create PaymentErrors by Order", e)
        }

    }

    @Throws(NotFoundException::class, PermissionException::class)
    override fun changeStatus(clientId: Long, paymentErrorId: Long, paymentErrorStatus: PaymentErrorStatus) {
        permissionService.checkAdministrativeRole(clientId)
        val paymentErrors = selectPaymentErrors(paymentErrorId)
                ?: throw NotFoundException(PaymentErrors::class.java, paymentErrorId)
        if (paymentErrors.paymentErrorStatus?.checkTransition(paymentErrorStatus) != true) {
            throw IllegalArgumentException(String.format("cannot change payment error status from %s to %s. PaymentErrorsIs = %d",
                    paymentErrors.paymentErrorStatus, paymentErrorStatus, paymentErrorId))
        }

        paymentErrors.paymentErrorStatus = paymentErrorStatus
        paymentErrors.managerId = clientId
        updatePaymentErrors(paymentErrors)
    }

    companion object {
        private val log = Logger.getLogger(PaymentErrorsServiceImpl::class.java)
    }
}

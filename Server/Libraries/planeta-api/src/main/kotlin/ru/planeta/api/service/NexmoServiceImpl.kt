package ru.planeta.api.service

import com.nexmo.messaging.sdk.NexmoSmsClient
import com.nexmo.messaging.sdk.NexmoSmsClientSSL
import com.nexmo.messaging.sdk.SmsSubmissionResult
import com.nexmo.messaging.sdk.messages.UnicodeMessage
import org.apache.log4j.Logger
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Service
import java.util.LinkedList

/**
 * Date: 01.06.2015
 * Time: 13:34
 */
@Service
class NexmoServiceImpl : NexmoService {

    private var nexmoSmsClient: NexmoSmsClient? = null

    private var queue: MutableList<UnicodeMessage> = LinkedList()
    private val lock = Any()

    private val client: NexmoSmsClient?
        @Synchronized get() {
            if (nexmoSmsClient == null) {
                init()
            }
            return nexmoSmsClient
        }


    // @PostConstruct
    @Synchronized
    private fun init() {
        if (nexmoSmsClient != null) {
            nexmoSmsClient = null
        }

        // getReceiver();

        try {
            nexmoSmsClient = NexmoSmsClientSSL(API_KEY, API_SECRET)
        } catch (e: Exception) {
            logger.error("Failed to instanciate a Nexmo Client")
        }

    }

    @Scheduled(fixedDelay = SMS_REPEAT_DELAY.toLong())
    fun scheduledResend() {
        synchronized(lock) {
            var current: List<UnicodeMessage>
            if (queue.isEmpty()) {
                return
            }
            current = queue
            queue = LinkedList()
            for (mes in current) {
                send(mes)
            }
        }
    }

    private fun send(mes: UnicodeMessage) {
        val results: Array<SmsSubmissionResult>
        try {
            results = client!!.submitMessage(mes)
        } catch (e: Exception) {
            logger.error("Failed to communicate with the Nexmo Client", e)
            return
        }

        // Evaluate the results of the submission attempt ...
        logger.trace("... Message submitted in [ " + results.size + " ] parts")
        for (i in results.indices) {
            val result = results[i]
            logger.trace("--------- part [ " + (i + 1) + " ] ------------")
            logger.trace("Status [ " + result.status + " ] ...")
            if (result.status == SmsSubmissionResult.STATUS_OK) {
                logger.trace("SUCCESS")
            } else if (result.temporaryError) {
                logger.error("TEMPORARY FAILURE - RETRY SCHEDULED")
                logger.trace(result.errorText)
                synchronized(lock) {
                    queue.add(mes)
                }
            } else {
                logger.error("SUBMISSION FAILED!")
                logger.error("Message-Id [ " + result.messageId + " ] ...")
                logger.error("Error-Text [ " + result.errorText + " ] ...")
            }
            if (results[i].messagePrice != null) {
                logger.trace("Message-Price [ " + result.messagePrice + " ] ...")
            }
            if (results[i].remainingBalance != null) {
                logger.trace("Remaining-Balance [ " + result.remainingBalance + " ] ...")
            }
        }
    }


    override fun send(message: String, phone: String?) {
        send(UnicodeMessage(FROM, phone, message))
    }

    companion object {

        private val API_KEY = "87004530"
        private val API_SECRET = "9e2d075e"
        private val FROM = "19282388665"

        private val logger = Logger.getLogger(NexmoServiceImpl::class.java)
        private const val SMS_REPEAT_DELAY = 1000 * 60 * 3 // 3 min
    }

}

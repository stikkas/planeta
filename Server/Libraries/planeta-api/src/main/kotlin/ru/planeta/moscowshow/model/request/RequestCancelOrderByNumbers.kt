package ru.planeta.moscowshow.model.request

import javax.xml.bind.annotation.XmlAccessType
import javax.xml.bind.annotation.XmlAccessorType
import javax.xml.bind.annotation.XmlElementWrapper
import javax.xml.bind.annotation.XmlRootElement

/**
 *
 * User: Serge Blagodatskih<stikkas17></stikkas17>@gmail.com><br></br>
 * Date: 14.10.16<br></br>
 * Time: 17:16
 */
@XmlRootElement(name = "cancelOrderByOrderNumbers")
@XmlAccessorType(XmlAccessType.FIELD)
class RequestCancelOrderByNumbers : Request {

    @XmlElementWrapper(name = "orderNumbers")
    private var string: List<String>? = null

    constructor()

    constructor(items: List<String>) {
        this.string = items
    }

}

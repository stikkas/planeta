package ru.planeta.api.log

import org.apache.commons.lang3.StringUtils
import org.apache.commons.lang3.exception.ExceptionUtils
import org.apache.log4j.Level
import org.apache.log4j.spi.Filter
import org.apache.log4j.spi.LoggingEvent
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.web.context.request.RequestContextHolder
import org.springframework.web.context.request.ServletRequestAttributes
import ru.planeta.api.mail.MailClient
import java.text.SimpleDateFormat
import java.util.*

/**
 * Accumulates error messages to post all once at time.
 *
 * @author p.vyazankin
 * @since 3/2/13 5:57 PM
 */
class ErrorAccumulatorFilter : Filter() {

    private var accumulator: MutableMap<LoggingEventKey, AccumulationInfo> = HashMap()
    private val syncRoot = Any()

    // list of strings to not depend of spring and jetty libs
    private val exclusionsString = Arrays.asList("IOException", "MissingServletRequestParameterException", "EofException", "MethodArgumentTypeMismatchException", "BindException")
    @Autowired
    private val mailClient: MailClient? = null

    @Value("\${error.accumulator.filter.need.log.request:true}")
    private val needLogRequest: Boolean = false

    /**
     * Filter is used to accumulate error messages and send emails with them.
     */
    init {
        if (instance != null) {
            throw RuntimeException("You can't create more than one " + this.javaClass.simpleName)
        }

        instance = this
    }

    /**
     * Sends all accumulated errors.
     */
    fun destroy() {
        sendErrorReports()
    }


    /**
     * Accumulating errors and sends email in the end of accumulation period (or when maximum accumulation quantity exceeded).
     * Denies all log messages.
     */
    override fun decide(loggingEvent: LoggingEvent): Int {

        if (instance != null) {
            instance!!.decideInner(loggingEvent)
        }

        return Filter.ACCEPT
    }

    @Scheduled(fixedRate = ACCUMULATION_PERIOD)
    fun scheduledSend() {
        sendErrorReports()
    }

    private fun decideInner(loggingEvent: LoggingEvent) {
        if (loggingEvent.level.isGreaterOrEqual(Level.ERROR) && (loggingEvent.getThrowableInformation() == null ||
                        !exclusionsString.contains(loggingEvent.throwableInformation.throwable::class.java.simpleName))) {
            val requestAttributes = RequestContextHolder.getRequestAttributes() as ServletRequestAttributes
            val key = LoggingEventKey(loggingEvent, if (requestAttributes == null || !needLogRequest) null else requestAttributes.request)
            synchronized(syncRoot) {
                var info: AccumulationInfo? = accumulator[key]

                if (info != null) {
                    info.oneMore(loggingEvent)
                } else {
                    info = AccumulationInfo(loggingEvent)
                    accumulator.put(key, info)
                }
            }
        }
    }

    private fun sendErrorReports() {
        try {
            var toSend: Map<LoggingEventKey, AccumulationInfo>

            synchronized(syncRoot) {

                toSend = accumulator
                accumulator = HashMap()

                if (!toSend.isEmpty()) {
                    mailClient!!.sendErrorEmailReport(toSend.size, getAccumulateString(toSend, needLogRequest))
                }
            }
        } catch (ex: Exception) {
            System.err.println(Date())
            ex.printStackTrace()
        }

    }

    companion object {
        private const val ACCUMULATION_PERIOD = 30 * 60 * 1000L // 30 minutes

        @JvmStatic
        var instance: ErrorAccumulatorFilter? = null
            private set

        private fun getAccumulateString(accumulatorState: Map<LoggingEventKey, AccumulationInfo>, needLogRequest: Boolean): String {
            val errors = StringBuilder("\n\n")
            for ((key, value) in accumulatorState) {
                val loggingEvent = key.loggingEvent

                if (needLogRequest && StringUtils.isNotEmpty(key.httpRequestData)) {
                    errors.append("Http request data:")
                    errors.append("\n")
                    errors.append(key.httpRequestData)
                }

                val size = value.size()
                errors.append(size)

                if (size == 1) {
                    errors.append(" error")
                } else {
                    errors.append(" errors")
                }

                val simpleDateFormat = SimpleDateFormat()
                errors.append(" like this one: \n\n")
                        .append(simpleDateFormat.format(Date(loggingEvent.timeStamp))).append(" ")
                        .append(loggingEvent.getLevel()).append(" ")
                        .append(loggingEvent.getMessage()).append(" \n")

                val throwableInformation = loggingEvent.getThrowableInformation()
                if (throwableInformation != null) {
                    errors.append(ExceptionUtils.getStackTrace(throwableInformation!!.getThrowable()))
                }
                errors.append("\n----------------\n\n\n\n")
            }
            return errors.toString()
        }
    }

}

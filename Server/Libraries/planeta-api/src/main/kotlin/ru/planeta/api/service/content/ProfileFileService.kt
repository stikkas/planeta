package ru.planeta.api.service.content

import ru.planeta.api.exceptions.PermissionException
import ru.planeta.model.profile.ProfileFile

/**
 *
 * @author ds.kolyshev
 * Date: 17.03.2013
 */
interface ProfileFileService {

    /**
     * Returns list of profile's files
     */
    @Throws(PermissionException::class)
    fun getProfileFiles(clientId: Long, profileId: Long, offset: Int, limit: Int): List<ProfileFile>

    /**
     * Returns file by identifier
     */
    @Throws(PermissionException::class)
    fun getProfileFile(clientId: Long, profileId: Long, fileId: Long): ProfileFile

    /**
     * Adds new file for profile
     */
    @Throws(PermissionException::class)
    fun addProfileFile(clientId: Long, profileFile: ProfileFile): ProfileFile

    @Throws(PermissionException::class)
    fun deleteProfileFile(clientId: Long, profileId: Long, fileId: Long)

    @Throws(PermissionException::class)
    fun updateProfileFileTitleAndDescription(clientId: Long, profileFile: ProfileFile)
}

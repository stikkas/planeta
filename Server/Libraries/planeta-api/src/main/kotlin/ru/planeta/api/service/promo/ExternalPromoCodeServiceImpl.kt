package ru.planeta.api.service.promo

import org.springframework.stereotype.Service
import ru.planeta.commons.lang.CollectionUtils
import ru.planeta.dao.trashcan.ExternalPromoCodeDAO
import ru.planeta.model.trashcan.PromoCode

@Service
class ExternalPromoCodeServiceImpl(private val promoCodeDAO: ExternalPromoCodeDAO) : ExternalPromoCodeService {

    override fun getLastNonUsed(type: Long): PromoCode {
        return promoCodeDAO.selectLastNonUsed(type)
    }

    override fun updatePromoCode(code: PromoCode) {
        promoCodeDAO.updatePromoCode(code)
    }

    override fun usePromoCode(code: PromoCode) {
        promoCodeDAO.usePromoCode(code)
    }

    override fun getAllCodes(type: Long): List<String>? {
        val codes = promoCodeDAO.selectAll(type)
        return if (codes == null || codes.isEmpty()) {
            null
        } else CollectionUtils.map(codes, CollectionUtils.MapFunction<PromoCode, String> { it.secretCode })
    }

    override fun getUsedPromoCodesCount(configId: Long): Long {
        return promoCodeDAO.selectUsedPromoCodesCount(configId)
    }

    override fun insertCodes(type: Long, codes: List<String>) {
        promoCodeDAO.insertCodes(type, codes)
    }
}

package ru.planeta.api.log

import org.apache.commons.lang3.exception.ExceptionUtils
import org.apache.log4j.Logger
import ru.planeta.api.aspect.logging.DBLoggerTypeInfoHolder
import ru.planeta.api.log.service.DBLogService
import ru.planeta.model.stat.log.LoggerType

/**
 * Database logger. Writes messages to db_log.<br></br>
 * User: eshevchenko
 */
class DBLogger(private val loggerType: LoggerType,
               private val dbLogService: DBLogService,
               private val logUndefinedObject: Boolean) : Logger(loggerType.name.toLowerCase() + "Logger") {

    private fun commonLog(message: Any?, t: Throwable?) {
        if (message == null) {
            return
        }
        val sb = StringBuilder(message.toString())
        if (t != null) {
            sb.append("\n").append(ExceptionUtils.getStackTrace(t))
        }

        val objectId = DBLoggerTypeInfoHolder.getInfoByLoggerType(Long::class.java, loggerType)
        if (objectId != null || logUndefinedObject) {
            dbLogService.addRecordToLog(sb.toString(), loggerType, objectId, 0L, 0L, 0L)
        }
    }

    override fun trace(message: Any) {
        commonLog(message, null)
    }

    override fun trace(message: Any, t: Throwable) {
        commonLog(message, t)
    }

    override fun debug(message: Any) {
        commonLog(message, null)
    }

    override fun debug(message: Any, t: Throwable) {
        commonLog(message, t)
    }

    override fun error(message: Any) {
        commonLog(message, null)
    }

    override fun error(message: Any, t: Throwable) {
        commonLog(message, t)
    }

    override fun fatal(message: Any) {
        commonLog(message, null)
    }

    override fun fatal(message: Any, t: Throwable) {
        commonLog(message, t)
    }

    override fun info(message: Any) {
        commonLog(message, null)
    }

    override fun info(message: Any, t: Throwable) {
        commonLog(message, t)
    }

    override fun warn(message: Any, t: Throwable) {
        commonLog(message, t)
    }

    override fun warn(message: Any) {
        commonLog(message, null)
    }

    companion object {

        fun getLogger(loggerType: LoggerType, dbLogService: DBLogService): Logger {
            return DBLogger(loggerType, dbLogService, false)
        }
    }
}

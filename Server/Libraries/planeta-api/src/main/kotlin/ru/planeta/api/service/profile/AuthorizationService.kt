package ru.planeta.api.service.profile

import ru.planeta.api.exceptions.NotFoundException
import ru.planeta.api.exceptions.PermissionException
import ru.planeta.api.model.PasswordChange
import ru.planeta.api.model.UserAuthorizationInfo
import ru.planeta.model.common.UserPrivateInfo
import ru.planeta.model.common.auth.ExternalAuthentication
import ru.planeta.model.common.auth.UserCredentials
import ru.planeta.model.enums.EmailStatus

/**
 * @author ds.kolyshev
 * Date: 29.08.11
 */

// TODO Использовать сервис из пакета ru.planeta.eva.api. По необходимости переносить методы отсюда туда
interface AuthorizationService {

    /**
     * User's registration check confirmation code from email
     *
     */
    @Throws(NotFoundException::class, PermissionException::class)
    fun checkRegCode(regCode: String, email: String): UserPrivateInfo

    /**
     * User's registration final step - check code and save password
     */
    @Throws(PermissionException::class, NotFoundException::class)
    fun confirmRegistration(regCode: String, email: String): UserPrivateInfo

    /**
     * Returns null if login is unsuccessfull
     */
    @Throws(NotFoundException::class)
    fun getUserInfoByUsername(email: String): UserAuthorizationInfo?

    /**
     * Checks if there's active password recovery request
     */
    fun passwordRecoveryRequest(regCode: String): UserPrivateInfo?

    // TODO Используем ru.planeta.eva.api.AuthorizationService.saveUserPassword
    @Throws(NotFoundException::class, PermissionException::class)
    fun saveUserPassword(clientId: Long, change: PasswordChange): UserPrivateInfo

    /**
     * User's password recovery final step - check code and save password
     */
    @Throws(NotFoundException::class)
    fun passwordRecoveryChangePassword(passwordRecoveryCode: String, password: String): UserPrivateInfo?

    @Deprecated("") // используем ru.planeta.eva.api.AuthorizationService.checkPassword
    fun checkPassword(userPrivateInfo: UserPrivateInfo?, password: String): Boolean

    @Throws(PermissionException::class)
    fun addCredentials(credentials: UserCredentials)

    fun getUserPrivateInfoByUsername(email: String): UserPrivateInfo?

    fun checkEmail(email: String): EmailStatus

    @Throws(NotFoundException::class)
    fun getUserInfo(externalAuthentication: ExternalAuthentication): UserAuthorizationInfo?

    @Throws(PermissionException::class)
    fun addSocialNetworkCredentials(myProfileId: Long, externalAuthentication: ExternalAuthentication)

    fun getUserPrivateInfoById(profieId: Long): UserPrivateInfo?

    fun getUserPrivateEmailById(profieId: Long): String?

    fun isEmailExists(email: String?): Boolean
}

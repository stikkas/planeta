package ru.planeta.api.service.campaign

import org.springframework.validation.BindingResult
import ru.planeta.dao.profiledb.ArrayListWithCount
import ru.planeta.model.common.Contractor
import ru.planeta.model.common.ContractorWithCampaigns

/**
 * User: a.savanovich
 * Date: 22.11.13
 * Time: 16:52
 */
interface ContractorService {

    fun select(contractorId: Long): Contractor

    fun select(name: String): Contractor

    fun add(contractor: Contractor, profileId: Long, campaignId: Long?)

    fun selectList(offset: Int, limit: Int): List<Contractor>

    fun selectList(profileId: Long, offset: Int, limit: Int): List<Contractor>

    fun delete(contractorId: Long)

    fun insertRelation(contractorId: Long, campaignId: Long)

    fun selectByCampaignId(campaignId: Long): Contractor

    fun getContractorsSearch(searchString: String, offset: Int, limit: Int): ArrayListWithCount<ContractorWithCampaigns>

    fun selectByInn(inn: String): Contractor

    fun validateContractor(bindingResult: BindingResult, campaignId: Long?, contractor: Contractor)
}

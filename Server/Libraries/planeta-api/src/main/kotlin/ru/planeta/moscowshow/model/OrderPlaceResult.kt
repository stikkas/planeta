package ru.planeta.moscowshow.model

import javax.xml.bind.annotation.XmlAccessType
import javax.xml.bind.annotation.XmlAccessorType
import javax.xml.bind.annotation.XmlRootElement

/**
 * Информация о элементе заказа
 * User: Serge Blagodatskih<stikkas17></stikkas17>@gmail.com><br></br>
 * Date: 14.10.16<br></br>
 * Time: 17:33
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
class OrderPlaceResult {

    /**
     * ID секции.
     */
    var sectionID: Long = 0
        internal set

    /**
     * Текстовое описание билета.
     */
    var ticketName: String? = null
        internal set

    /**
     * URL для скачивания PDF файла билета (выдаётся после вызова функции печати билетов).
     */
    var url: String? = null
        internal set

    /**
     * ID билета.
     */
    var placeID: Long = 0
        internal set

    /**
     * ID мероприятия.
     */
    var actionID: Long = 0
        internal set

    /**
     * ID ряда (при заказе без места равен 0).
     */
    var rowID: Long = 0
        internal set

    /**
     * Место в ряду (при заказе без места равен 0).
     */
    var position: Int = 0
        internal set

    /**
     * 14-значный код билета (печатается на бланке билета вместе с информацией о мероприятии, дате, секции, ряде, месте).
     */
    var ticketCode: String? = null
        internal set

    /**
     * 12-значный код заказа.
     */
    var orderCode: String? = null
        internal set

    /**
     * Используется для электронных билетов.
     */
    var confirmationCode: String? = null
        internal set

    override fun toString(): String {
        return ("OrderPlaceResult{" + "sectionID=" + sectionID + ", ticketName="
                + ticketName + ", url=" + url + ", placeID=" + placeID + ", actionID=" + actionID
                + ", rowID=" + rowID + ", position=" + position + ", ticketCode=" + ticketCode
                + ", orderCode=" + orderCode + ", confirmationCode=" + confirmationCode + '}'.toString())
    }

}

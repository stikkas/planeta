package ru.planeta.api.service.campaign

import org.apache.commons.lang3.tuple.Pair
import ru.planeta.api.aspect.transaction.NonTransactional
import ru.planeta.api.exceptions.NotFoundException
import ru.planeta.api.exceptions.PermissionException
import ru.planeta.api.service.billing.order.OrderService
import ru.planeta.dao.ListWithCount
import ru.planeta.model.charity.CharityCampaignsStats
import ru.planeta.model.common.campaign.*
import ru.planeta.model.common.campaign.enums.CampaignStatus
import ru.planeta.model.common.campaign.param.CampaignEventsParam
import ru.planeta.model.enums.ObjectType
import ru.planeta.model.profile.CampaignBacker
import ru.planeta.model.profile.Comment
import ru.planeta.model.stat.CampaignStat
import java.io.IOException
import java.math.BigDecimal
import java.util.Date
import java.util.EnumSet

/**
 * Crowd funding campaigns service User: atropnikov Date: 21.03.12 Time: 16:09
 */
interface CampaignService {

    val campaignsCategories: List<CampaignTag>

    /**
     * get total stats from all projects
     *
     * @return CampaignTotalStats
     */

    val totalCollectedAmount: BigDecimal

    val charityPromoCampaigns: List<Campaign>

    val charityTopCampaigns: List<Campaign>

    //    void validateRemoveShare(long clientId, long shareId) throws PermissionException, NotFoundException;

    val allTags: List<CampaignTag>

    val purchasedForAllTime: BigDecimal

    val sponsors: List<Sponsor>

    val successfulCharityCampaignsCount: Int

    val successfulCampaignsCount: Int

    val campaignsCountPercentagesByCustomTags: List<Pair<String, Long>>

    fun getCampaignsCategorieById(campaignTagId: Long): CampaignTag

    @Throws(PermissionException::class)
    fun save(campaignTag: CampaignTag)

    @Throws(NotFoundException::class, PermissionException::class)
    fun saveCampaign(clientId: Long, campaing: Campaign, force: Boolean)

    /**
     * Select campaigns for specified profile and status
     *
     * @param profileId profile id
     * @param statuses statuses
     * @param offset offset
     * @param limit limit
     * @return list of campaigns
     */
    fun getCampaignsByStatus(profileId: Long, statuses: EnumSet<CampaignStatus>, offset: Int, limit: Int): List<Campaign>

    /**
     * Returns list of campaigns finished
     *
     * @return list of campaigns
     */
    fun getFinishedCampaignsByTime(offset: Int, limit: Int): List<Campaign>

    fun getStartedCampaignsByTime(offset: Int, limit: Int): List<Campaign>

    /**
     * Select specified campaign
     *
     * @param campaignId campaign id
     * @return campaign
     */
    fun getCampaign(campaignId: Long?): Campaign?

    /**
     * Select campaign by alias if campaignAlias can convert to long selectCampaignById
     * campaign by id
     *
     * @param campaignAlias campaign alias or id
     * @return campaign or null if campaign not found
     */
    fun getCampaign(campaignAlias: String): Campaign?

    @Throws(NotFoundException::class)
    fun getCampaignSafe(campaignId: Long?): Campaign

    @Throws(NotFoundException::class, PermissionException::class)
    fun updateDescriptionAndMetaData(clientId: Long, campaignId: Long, descriptionHtml: String, metaData: String, sponsorAlias: String)

    @Throws(NotFoundException::class, PermissionException::class)
    fun getEditableCampaignSafe(clientId: Long, campaignId: Long): Campaign

    @Throws(NotFoundException::class, PermissionException::class)
    fun getCampaignSafe(clientId: Long, campaignAlias: String): Campaign

    /**
     * Select campaign with shares collection
     *
     * @param clientId clients profile id
     * @param campaignId campaign id
     * @return campaign
     */
    @Throws(NotFoundException::class, PermissionException::class)
    fun getCampaignWithShares(clientId: Long, campaignId: Long, isSharePriceOrderByDesc: Boolean): Campaign

    /**
     * Create new empty campaign only for tests
     *
     * @param clientId client id
     * @param profileId profile id
     * @return campaign
     */
    @Throws(PermissionException::class, NotFoundException::class)
    fun insertCampaign(clientId: Long, creatorProfileId: Long, profileId: Long): Campaign

    /**
     * Insert or update campaign
     *
     * @param clientId client id
     */
    @Throws(PermissionException::class, NotFoundException::class)
    fun publishCampaign(clientId: Long, campaignId: Long)

    /**
     * Marks campaign as deleted, without actually removing it from db; Note:
     * allow remove campaign only with status DRAFT;
     *
     * @param clientId client id
     * @param campaignId campaign id
     */
    @Throws(PermissionException::class, NotFoundException::class)
    fun removeCampaign(clientId: Long, campaignId: Long)

    @Throws(PermissionException::class, NotFoundException::class)
    fun toggleDraftVisible(clientId: Long, campaignId: Long)

    /**
     * Start specified campaign
     *
     * @param clientId client id
     * @param campaignId campaign id
     * @return modified campaign
     */
    @Throws(NotFoundException::class, PermissionException::class)
    fun startCampaign(clientId: Long, campaignId: Long): Campaign

    /**
     * Pause campaign
     *
     * @param clientId client id
     * @param campaignId campaign id
     * @return modified campaign
     */
    @Throws(NotFoundException::class, PermissionException::class)
    fun pauseCampaign(clientId: Long, campaignId: Long): Campaign

    /**
     * Returns list of new campaign backers
     *
     * @param campaignId campaign id
     * @param offset offset
     * @param limit limit
     * @return list of new campaign backers
     */
    @NonTransactional
    fun getCampaignBackersNew(campaignId: Long, offset: Int, limit: Int): List<CampaignBacker>

    /**
     * Returns campaign backers' count
     *
     * @param campaignId campaign id
     * @return campaign backers' count
     */
    fun getCampaignBackersCount(campaignId: Long): Int

    /**
     * Returns share by id
     *
     * @param shareId share id
     * @return share
     */
    fun getShare(shareId: Long): Share?

    @Throws(NotFoundException::class)
    fun getShareSafe(shareId: Long): Share

    /**
     * Insert or update specified share
     *
     * @param clientId client id
     * @param share share
     */
    @Throws(PermissionException::class, NotFoundException::class)
    fun saveShare(clientId: Long, share: Share): Share

    @Throws(PermissionException::class, NotFoundException::class)
    fun disableShare(clientId: Long, shareId: Long)

    /**
     * Get share, which is ready for purchase
     *
     * @param shareId share id
     * @param count count
     * @return share
     */
    @Throws(NotFoundException::class, PermissionException::class)
    fun getShareReadyForPurchase(shareId: Long, count: Int): Share

    /**
     * Check campaign is finished and do some required operations, such as reach
     * of target: success or fail
     *
     * @param campaignId campaign id
     */
    @Throws(PermissionException::class, NotFoundException::class)
    fun checkOnCampaignIsFinished(campaignId: Long, clientId: Long)

    @Throws(PermissionException::class, NotFoundException::class)
    fun checkOnCampaignHasToStart(campaignId: Long, clientId: Long)

    /**
     * Updates share's and campaign's purchase amount and purchase count.
     *
     * @param campaignId campaign;
     * @param count shares count;
     */
    @Throws(PermissionException::class, NotFoundException::class)
    fun updateAfterSharePurchase(campaignId: Long, count: Int, amount: BigDecimal, shareId: Long)

    /**
     * Rollbacks [.updateAfterSharePurchase] changes and restart campaign
     * if need.
     *
     * @param campaignId campaign;
     * @param clientId client profile identifier;
     * @param count shares count;
     */
    @Throws(PermissionException::class, NotFoundException::class)
    fun updateAfterSharePurchaseCancellation(clientId: Long, campaignId: Long, count: Int, price: BigDecimal, shareId: Long)

    /**
     * get collected money of campaign
     *
     */
    fun getCollectedAmount(compainId: Long): BigDecimal

    fun getCampaignSharesFiltered(campaignId: Long): List<Share>

    fun getCampaignSharesFilteredForWidgets(campaignId: Long): List<Share>

    @Throws(NotFoundException::class)
    fun createCampaignComment(authorId: Long, shareId: Long, comment: String): Comment

    fun getCampaignsCount(profileId: Long, status: CampaignStatus): Int

    fun getCampaignsCount(profileId: Long, statuses: EnumSet<CampaignStatus>): Int

    fun getCampaignsByDateRange(timeStartFrom: Date?, timeStartTo: Date?, timeFinishFrom: Date?, timeFinishTo: Date?): List<Campaign>

    /**
     * Gets list of contacts for campaign
     *
     */
    fun getCampaignContacts(clientId: Long, campaignId: Long, offset: Int, limit: Int): List<CampaignContact>

    /**
     * Save campaign's contact
     *
     */
    @Throws(PermissionException::class, NotFoundException::class)
    fun saveCampaignContact(clientId: Long, campaignContact: CampaignContact)

    /**
     * Delete campaign's contact
     *
     */
    @Throws(NotFoundException::class, PermissionException::class)
    fun deleteCampaignContact(clientId: Long, campaignId: Long, email: String)

    /**
     * Sends campaign feedback email message
     *
     */
    @Throws(NotFoundException::class)
    fun sendCampaignAuthorFeedbackMessage(clientId: Long, campaignId: Long, message: String, senderEmail: String, userDisplayName: String)

    @Throws(NotFoundException::class)
    fun sendCampaignCuratorsFeedbackMessage(clientId: Long, campaignId: Long, message: String, senderEmail: String, userDisplayName: String)

    @Throws(NotFoundException::class)
    fun sendCampaignSpamFeedbackMessage(clientId: Long, campaignId: Long, message: String, senderEmail: String, userDisplayName: String)

    /**
     * Gets list of possible contacts for specified campaign
     *
     */
    @Throws(NotFoundException::class, PermissionException::class)
    fun getPossibleCampaignContacts(clientId: Long, campaignId: Long): List<CampaignContact>

    @Throws(PermissionException::class, NotFoundException::class)
    fun changeCampaignStatus(clientId: Long, campaignId: Long, status: CampaignStatus): Campaign

    /**
     * @param clientId campaigns user owner id
     * @return my draft campaigns list
     */
    @Throws(NotFoundException::class, PermissionException::class)
    fun getMyDraftCampaigns(clientId: Long): List<Campaign>

    fun getCampaignDetailedShares(campaignId: Long): List<ShareDetails>

    fun getCampaignWidgetDetailedShares(campaignId: Long): List<ShareDetails>

    fun getCampaignsByContractorId(contractorId: Long, offset: Int, limit: Int): List<Campaign>

    fun getCampaignTagByMnemonic(mnemonicName: String): CampaignTag?

    fun getCampaignTagsByMnemonic(mnemonicNames: List<String>): List<CampaignTag>

    fun getCampaignCurators(campaignId: Long): List<CampaignContact>

    @Throws(PermissionException::class, NotFoundException::class)
    fun addCampaignCurator(clientId: Long, contact: CampaignContact)

    @Throws(PermissionException::class, NotFoundException::class)
    fun addCampaignCurator(clientId: Long, campaignId: Long?, email: String)

    @Throws(NotFoundException::class, PermissionException::class)
    fun deleteCampaignCurator(clientId: Long, campaignId: Long, email: String)

    fun getCampaignContactsCount(campaignId: Long): Int

    fun getCampaignsCountWithPurchasedOrReservedShares(campaignId: Long): Int?

    @Throws(NotFoundException::class)
    fun sendCampaignDeclinedEmail(clientId: Long, campaignId: Long, message: String)

    @Throws(NotFoundException::class, PermissionException::class)
    fun updateCampaignContactList(clientId: Long, campaignId: Long, emailList: List<String>)

    fun getCampaignDetailedSharesForEdit(campaignId: Long): List<ShareEditDetails>

    fun getCampaignsForAdminSearch(clientId: Long, query: String,
                                   offset: Int, limit: Int,
                                   managerId: Long?,
                                   dateFrom: Date?, dateTo: Date?,
                                   campaignStatuses: EnumSet<CampaignStatus>, campaignTagId: Int?, stringOrderBy: String): List<Campaign>

    fun getCampaignsForAdminSearchCount(clientId: Long, query: String,
                                        managerId: Long?,
                                        dateFrom: Date?, dateTo: Date?,
                                        campaignStatuses: EnumSet<CampaignStatus>, campaignTagId: Int?): Int

    @Throws(PermissionException::class, NotFoundException::class)
    fun getDraftCampaign(clientId: Long, campaignId: Long): Campaign

    @Throws(PermissionException::class, NotFoundException::class)
    fun undoChanges(clientId: Long, campaignId: Long): Campaign

    fun getBackedCampaignList(profileId: Long, offset: Int, limit: Int): List<Campaign>

    fun getUserBackedCampaignTagCountList(profileId: Long): List<Map<*, *>>

    fun getCampaignsTargetAmount(active: CampaignStatus): BigDecimal

    fun getCampaignsPurchasedAmount(status: CampaignStatus): BigDecimal

    fun getCampaignsCountBetween(dateBegin: Date, dateEnd: Date, status: CampaignStatus): Int?

    fun selectCampaignNewEventsCountList(campaignIds: List<Long>, profileId: Long, dateFrom: Date?): List<CampaignWithNewEventsCount>

    @NonTransactional
    fun selectCampaignNews(campaignEventsParam: CampaignEventsParam): ListWithCount<CampaignNews>

    fun selectCampaignPosts(campaignEventsParam: CampaignEventsParam): ListWithCount<CampaignNews>

    fun selectCampaignComments(campaignEventsParam: CampaignEventsParam): ListWithCount<CampaignNews>

    @Throws(NotFoundException::class)
    fun getCampaignId(campaignAlias: String): Long

    @Throws(NotFoundException::class)
    fun updateCampaignStatusSpecialForTheNeedsOfAdmins(clientId: Long, campaignId: Long): Boolean

    fun getCampaigns(ids: List<Long>): List<Campaign>

    fun getSponsors(aliasList: List<String>): List<Sponsor>

    fun getAuthorActiveCampaign(creatorProfileId: Long): Campaign

    @NonTransactional
    @Throws(NotFoundException::class, PermissionException::class, IOException::class)
    fun updateCampaignBackground(clientId: Long, campaignId: Long, backgroundUrl: String): Campaign

    fun selectPurchasedShareIds(buyerId: Long, campaignId: Long): List<Long>

    fun selectPurchasedCampaignIds(buyerId: Long): List<Long>

    @Deprecated("")
    fun getCampaignBySponsorAlias(alias: String, offset: Int, limit: Int): List<Campaign>

    fun getGtmCampaignInfoList(campaignIds: List<Long>): List<GtmCampaignInfo>

    fun countOrganizationCharityCampaigns(): Int

    fun charityCampaignsStats(date: Date): List<CharityCampaignsStats>

    /**
     * Возвращает информацию по акции с дополнительными данными, особенными для зарегестрированного покупателя
     *
     * @param clientId - идентификатор пользователя
     * @param shareId - идентификатор акции
     * @return информацию по акции
     */
    fun getDetailedShareForClient(clientId: Long, shareId: Long): ShareDetails?

    fun selectProfileIdsOfCampaignsWithEventsForDay(offset: Int, limit: Int, date: Date): List<Long>

    fun selectCampaignsWithEventsForDayByProfileId(profileId: Long, offset: Int, limit: Int, date: Date): List<Campaign>

    fun selectAggregationStatsForDayByCampaignId(campaignId: Long, date: Date): CampaignStat

    fun selectCampaignCommentsCountByDate(date: Date, commentsType: ObjectType, campaignId: Long): Int

    fun setOrderService(orderService: OrderService)

    fun getUserCampaignsStatuses(profileId: Long): List<MyCampaignsStatusesCount>

    fun getUserPurchasedCampaignsStatuses(profileId: Long): List<MyCampaignsStatusesCount>

    fun getUserPurchasedAndRewardsCount(profileId: Long): List<MyPurchasesAndRewardsCount>
}


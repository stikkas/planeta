package ru.planeta.api.service.shop

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Service
import ru.planeta.api.service.security.PermissionService
import ru.planeta.dao.shopdb.CartDAO
import ru.planeta.dao.shopdb.ProductDAO
import ru.planeta.model.shop.Product
import ru.planeta.model.shop.ShoppingCartShortItem

import java.util.ArrayList
import java.util.Date

/**
 * Cart service implementation
 * User: Andrew Arefyev
 * Date: 05.12.12
 * Time: 17:43
 */
@Service
class CartServiceImpl(private val cartDAO: CartDAO,
                      private val productDAO: ProductDAO,
                      private val permissionService: PermissionService) : CartService {
    override fun clear(clientId: Long): Int {
        return cartDAO.clearCart(clientId)
    }

    override fun getCart(buyerId: Long): List<ShoppingCartShortItem> {

        val result = ArrayList<ShoppingCartShortItem>()
        for (item in cartDAO.select(buyerId)) {
            val product = productDAO.selectCurrent(item.productId)
            if (product != null) {
                var quantityForPurchase = product.totalQuantity - product.totalOnHoldQuantity
                if (product.purchaseLimit > 0) {
                    quantityForPurchase = Math.min(product.purchaseLimit, quantityForPurchase)
                }
                if (quantityForPurchase < item.quantity) {
                    put(buyerId, product.productId, quantityForPurchase)
                    result.add(ShoppingCartShortItem(item.productId, quantityForPurchase))
                } else {
                    result.add(item)
                }
            }
        }

        return result
    }

    override fun put(profileId: Long, productId: Long?, quantity: Int) {
        if (permissionService.isAnonymous(profileId)) {
            return
        }
        if (cartDAO.update(profileId, productId!!, quantity) != 1) {
            cartDAO.insert(profileId, productId, quantity)
        }
    }

    override fun remove(profileId: Long?, productId: Long) {
        cartDAO.delete(profileId!!, productId)
    }

    @Scheduled(fixedRate = CART_COLLECTOR_CHECK_INTERVAL.toLong())
    fun scheduleObsoleteCartsCollector() {
        val ids = cartDAO.selectCartOwnersIds()
        val currentDateTime = Date()
        for (cartId in ids) {
            val lastTimeUpdated = cartDAO.getLastTimeUpdated(cartId)
            if (lastTimeUpdated == null || lastTimeUpdated.time + CART_LIFETIME_INTERVAL < currentDateTime.time) {
                clear(cartId)
            }
        }
    }

    companion object {
        const val CART_COLLECTOR_CHECK_INTERVAL = 15 * 60 * 1000
        const val CART_LIFETIME_INTERVAL = 24 * 60 * 60 * 1000
    }
}

package ru.planeta.api.service.campaign

import org.apache.log4j.Logger
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import org.springframework.validation.BindingResult
import org.springframework.validation.ValidationUtils
import ru.planeta.api.news.LoggerService
import ru.planeta.dao.commondb.ContractorDAO
import ru.planeta.dao.profiledb.ArrayListWithCount
import ru.planeta.model.common.Contractor
import ru.planeta.model.common.ContractorWithCampaigns
import ru.planeta.model.enums.ContractorPositionType
import ru.planeta.model.enums.ContractorType
import ru.planeta.model.news.ProfileNews

import java.util.Date

/**
 * User: a.savanovich
 * Date: 22.11.13
 * Time: 16:54
 */
@Service
class ContractorServiceImpl @Autowired
constructor(private val contractorDAO: ContractorDAO, private val profileNewsService: LoggerService) : ContractorService {

    override fun select(contractorId: Long): Contractor {
        return contractorDAO.select(contractorId)
    }

    override fun select(name: String): Contractor {
        return contractorDAO.select(name)
    }

    override fun selectByInn(inn: String): Contractor {
        return contractorDAO.selectByInn(inn)
    }

    override fun selectByCampaignId(campaignId: Long): Contractor {
        return contractorDAO.selectByCampaignId(campaignId)
    }

    override fun add(contractor: Contractor, profileId: Long, campaignId: Long?) {
        val currentDate = Date()
        contractor.timeUpdated = currentDate
        if (contractor.type !== ContractorType.INDIVIDUAL) {
            contractor.name = cutLegalPrefix(contractor.name)
        }
        if (contractor.contractorId == 0L) {
            contractor.timeAdded = currentDate
            contractorDAO.insert(contractor)
            profileNewsService.addProfileNews(ProfileNews.Type.ADD_CAMPAIGN_CONTRACTOR, profileId, contractor.contractorId, campaignId!!)

        } else {
            contractorDAO.update(contractor)
            profileNewsService.addProfileNews(ProfileNews.Type.CHANGE_CAMPAIGN_CONTRACTOR, profileId, contractor.contractorId, campaignId!!)
            LOGGER.info("contractor " + contractor.contractorId + " was changed by profileId = " + profileId + ": birthDayDate = " + contractor.birthDate + "; issueDate = " + contractor.issueDate + ".")
        }
    }

    override fun insertRelation(contractorId: Long, campaignId: Long) {
        val contractorById = contractorDAO.select(contractorId)
        if (contractorById == null) {
            contractorDAO.deleteRelation(campaignId)
            return
        }
        val contractorByCampaign = contractorDAO.selectByCampaignId(campaignId)
        if (contractorByCampaign == null) {
            contractorDAO.insertRelation(contractorId, campaignId)
        } else {
            contractorDAO.updateRelation(contractorId, campaignId)
        }
    }

    override fun selectList(offset: Int, limit: Int): List<Contractor> {
        return contractorDAO.selectList(offset, limit)
    }

    override fun selectList(profileId: Long, offset: Int, limit: Int): List<Contractor> {
        return contractorDAO.selectList(profileId, offset, limit)
    }

    override fun delete(contractorId: Long) {
        contractorDAO.delete(contractorId)
        contractorDAO.deleteAllRelationByContractorId(contractorId)
    }

    override fun getContractorsSearch(searchString: String, offset: Int, limit: Int): ArrayListWithCount<ContractorWithCampaigns> {
        return contractorDAO.selectContractorsSearch(searchString, offset, limit)
    }

    private fun cutLegalPrefix(str: String?): String {
        for (prefix in PREFIXES) {
            val index = str!!.indexOf("$prefix ")
            if (index == 0) {
                return str.substring(prefix.length + 1, str.length)
            }
        }
        return str ?: ""
    }

    override fun validateContractor(bindingResult: BindingResult, campaignId: Long?, contractor: Contractor) {
        ValidationUtils.rejectIfEmptyOrWhitespace(bindingResult, "phone", "field.required")
        ValidationUtils.rejectIfEmptyOrWhitespace(bindingResult, "name", "field.required")

        if (contractor.type !== ContractorType.INDIVIDUAL && contractor.type !== ContractorType.INDIVIDUAL_ENTREPRENEUR) {
            if (ContractorPositionType.NOT_SET === contractor.position) {
                bindingResult.rejectValue("position", "field.required")
            } else if (ContractorPositionType.OTHER === contractor.position) {
                ValidationUtils.rejectIfEmpty(bindingResult, "customPosition", "field.required")
            }
        }

        if (contractor.countryId == 0L) {
            bindingResult.rejectValue("countryId", "field.required")
        }

        if (contractor.countryId == 1L) {
            if (contractor.cityId == 0L) {
                bindingResult.rejectValue("cityId", "field.required")
            }


            /*if (contractor.getType() == ContractorType.INDIVIDUAL && contractor.getCountryId() == 1 && permissionService.isPlanetaAdmin(BaseController.myProfileId())) {
                ValidationUtils.rejectIfEmptyOrWhitespace(bindingResult, "passportNumber", "field.required");
            }*/

            if (contractor.type === ContractorType.INDIVIDUAL
                    || contractor.type === ContractorType.INDIVIDUAL_ENTREPRENEUR
                    || contractor.type === ContractorType.OOO
                    || contractor.type === ContractorType.CHARITABLE_FOUNDATION
                    || contractor.type === ContractorType.LEGAL_ENTITY) {

                //ValidationUtils.rejectIfEmpty(bindingResult, "scanPassportUrl", "field.required");
                ValidationUtils.rejectIfEmpty(bindingResult, "initialsWithLastName", "field.required")
                ValidationUtils.rejectIfEmpty(bindingResult, "legalAddress", "field.required")
                ValidationUtils.rejectIfEmpty(bindingResult, "beneficiaryBank", "field.required")

                if (contractor.countryId == 1L) {
                    if (contractor.type !== ContractorType.LEGAL_ENTITY) {
                        ValidationUtils.rejectIfEmpty(bindingResult, "correspondingAccount", "field.required")
                    }
                    ValidationUtils.rejectIfEmpty(bindingResult, "bic", "field.required")
                    ValidationUtils.rejectIfEmpty(bindingResult, "checkingAccount", "field.required")

                    if (contractor.type !== ContractorType.INDIVIDUAL) {
                        ValidationUtils.rejectIfEmpty(bindingResult, "inn", "field.required")
                    }
                }

                if (contractor.type === ContractorType.INDIVIDUAL) {
                    ValidationUtils.rejectIfEmpty(bindingResult, "birthDate", "field.required")
                    ValidationUtils.rejectIfEmpty(bindingResult, "unit", "field.required")
                    ValidationUtils.rejectIfEmpty(bindingResult, "issueDate", "field.required")
                    ValidationUtils.rejectIfEmpty(bindingResult, "authority", "field.required")
                    ValidationUtils.rejectIfEmpty(bindingResult, "passportNumber", "field.required")
                }

                if (contractor.type === ContractorType.INDIVIDUAL_ENTREPRENEUR) {
                    ValidationUtils.rejectIfEmpty(bindingResult, "actualAddress", "field.required")
                    ValidationUtils.rejectIfEmpty(bindingResult, "ogrn", "field.required")
                }

                if (contractor.type === ContractorType.OOO
                        || contractor.type === ContractorType.CHARITABLE_FOUNDATION
                        || contractor.type === ContractorType.LEGAL_ENTITY) {
                    ValidationUtils.rejectIfEmpty(bindingResult, "actualAddress", "field.required")
                    ValidationUtils.rejectIfEmpty(bindingResult, "ogrn", "field.required")
                    ValidationUtils.rejectIfEmpty(bindingResult, "responsiblePerson", "field.required")
                    ValidationUtils.rejectIfEmpty(bindingResult, "kpp", "field.required")
                }
            }
        }
    }

    companion object {
        private val PREFIXES = arrayOf("ИП", "ООО")
        protected val LOGGER = Logger.getLogger(ContractorServiceImpl::class.java)
    }
}

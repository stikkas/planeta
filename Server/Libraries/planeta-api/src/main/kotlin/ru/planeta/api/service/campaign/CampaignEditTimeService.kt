package ru.planeta.api.service.campaign

import ru.planeta.api.exceptions.PermissionException

/**
 *
 * User: Serge Blagodatskih<stikkas17></stikkas17>@gmail.com><br></br>
 * Date: 23.03.16<br></br>
 * Time: 10:00
 */
interface CampaignEditTimeService {
    /**
     * Update record or insert and check if another person is editing campaign
     * @param campaignId ID of campaign
     * @param profileId ID of current person
     * @throws PermissionException if another person is editing campaign
     */
    @Throws(PermissionException::class)
    fun updateAndCheck(campaignId: Long, profileId: Long)
}

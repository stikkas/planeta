package ru.planeta.api.exceptions

/**
 * Created with IntelliJ IDEA.
 * Date: 30.07.13
 * Time: 18:12
 */
class RegistrationException(val code: MessageCode) : Exception(code.errorPropertyName)

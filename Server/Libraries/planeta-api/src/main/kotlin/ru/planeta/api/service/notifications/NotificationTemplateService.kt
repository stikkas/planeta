package ru.planeta.api.service.notifications

/**
 * Notification template using [FreeMarker](http://freemarker.sourceforge.net/docs/) template engine
 */
interface NotificationTemplateService {

    /**
     * Applies notification template to the specified list of objects used in this notification.
     * Empty notification template is defined with notification type (blog-post-comment, event cancel etc.)
     * and notification method (email, sms, web-interface). This templates are written with Freemarker Template Language
     * and collected in configuration file.
     * Filling of the template is performed using FreeMarker template engine with no default parameters, only using
     * objects map.
     * Note that FTL isn't tolerant to missing values, so fill objects map accurately,
     * null-value equals to no-key-value.
     * Method throws no exceptions, in case of template missing or bad templating objects write messages to log.
     *
     * @param templateFileName templateFileName
     * @param objects template parameters map
     * If it is possible to create String from Object manually, do it, see Ignored test testBadTemplate
     * Don't use dotted names like notificatons.count as key - freemarker assumes it is "notifications" submap "count" key
     * and can't find it.
     * @return filled template - text(html|xml|etc.) of notification ready to send to user
     * @see [possible Object types](http://freemarker.sourceforge.net/docs/pgui_quickstart_createdatamodel.html)
     */

    fun applyCustomTemplate(templateFileName: String, objects: MutableMap<String, Any>): String

    fun applyRichTemplate(objectMap: MutableMap<String, Any>): String

}

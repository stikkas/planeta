package ru.planeta.moscowshow.model.request

import javax.xml.bind.annotation.XmlAccessType
import javax.xml.bind.annotation.XmlAccessorType
import javax.xml.bind.annotation.XmlRootElement

/**
 *
 * User: Serge Blagodatskih<stikkas17></stikkas17>@gmail.com><br></br>
 * Date: 17.10.16<br></br>
 * Time: 17:33
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
open class Request {

    var sessionID: String? = null

}

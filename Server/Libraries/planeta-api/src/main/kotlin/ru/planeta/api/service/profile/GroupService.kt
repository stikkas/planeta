package ru.planeta.api.service.profile

import ru.planeta.model.profile.Profile

/**
 * Group's general service
 *
 * @author ds.kolyshev
 * Date: 27.10.11
 */
interface GroupService {

    /**
     * Gets list of groups for specified user with specific relation status.<br></br>
     *
     * @param clientId user identifier
     */
    fun getGroupsByUser(clientId: Long): List<Profile>
}

package ru.planeta.api.service.common

import org.springframework.stereotype.Service
import ru.planeta.dao.commondb.SeminarDAO
import ru.planeta.dao.commondb.CampaignTagDAO
import ru.planeta.model.common.campaign.CampaignTag
import ru.planeta.model.common.school.Seminar
import ru.planeta.model.common.school.SeminarExample
import ru.planeta.model.common.school.SeminarWithTagNameAndCityName
import ru.planeta.model.enums.SeminarType
import java.util.*

@Service
class SeminarServiceImpl(private var campaignTagDAO: CampaignTagDAO,
                         private var seminarDAO: SeminarDAO) : SeminarService {


    val orderByClause: String
        get() = "seminar_id"

    fun insertSeminar(seminar: Seminar) {
        seminar.timeAdded = Date()
        seminarDAO.insertSelective(seminar)
    }

    fun insertSeminarAllFields(seminar: Seminar) {
        seminar.timeAdded = Date()

        seminarDAO.insert(seminar)
    }

    override fun updateSeminar(seminar: Seminar) {

        seminarDAO.updateByPrimaryKeySelective(seminar)
    }

    fun updateSeminarAllFields(seminar: Seminar) {

        seminarDAO.updateByPrimaryKey(seminar)
    }

    override fun insertOrUpdateSeminar(seminar: Seminar) {

        val selectedSeminar = seminarDAO.selectByPrimaryKey(seminar.seminarId)
        if (selectedSeminar == null) {
            seminar.timeAdded = Date()
            seminarDAO.insertSelective(seminar)
        } else {
            seminarDAO.updateByPrimaryKey(seminar)
        }
    }

    fun insertOrUpdateSeminarSelective(seminar: Seminar) {

        val selectedSeminar = seminarDAO.selectByPrimaryKey(seminar.seminarId)
        if (selectedSeminar == null) {
            seminar.timeAdded = Date()
            seminarDAO.insert(seminar)
        } else {
            seminarDAO.updateByPrimaryKeySelective(seminar)
        }
    }

    fun deleteSeminar(id: Long?) {
        seminarDAO.deleteByPrimaryKey(id)
    }

    override fun selectSeminar(seminarId: Long?): Seminar {
        return seminarDAO.selectByPrimaryKey(seminarId)
    }

    fun getSeminarIsShownOnMainPageExample(isShownOnMainPage: Boolean?, offset: Int, limit: Int): SeminarExample {
        val example = SeminarExample()
        example.offset = offset
        example.limit = limit
        example.or()
                .andIsShownOnMainPageEqualTo(isShownOnMainPage)
        example.orderByClause = orderByClause
        return example
    }


    override fun selectSeminarListIsShownOnMainPage(isShownOnMainPage: Boolean?, offset: Int, limit: Int): List<Seminar> {
        return seminarDAO.selectByExample(getSeminarIsShownOnMainPageExample(isShownOnMainPage, offset, limit))
    }

    override fun selectSeminarWithTagNameAndCityName(seminarId: Long?): SeminarWithTagNameAndCityName {
        return seminarDAO.selectSeminarWithTagNameAndCityNameByPrimaryKey(seminarId)
    }

    override fun selectSeminarWithTagNameAndCityNameListIsShownOnMainPage(isShownOnMainPage: Boolean?, offset: Int, limit: Int): List<SeminarWithTagNameAndCityName> {
        return seminarDAO.selectSeminarWithTagNameAndCityNameByExample(getSeminarIsShownOnMainPageExample(isShownOnMainPage, offset, limit))
    }

    override fun selectSeminarWithTagNameList(seminarIdList: List<Long>?): List<SeminarWithTagNameAndCityName> {
        return if (seminarIdList == null || seminarIdList.isEmpty()) ArrayList() else seminarDAO.selectSeminarWithTagNameAndCityNameByIdList(seminarIdList)
    }

    override fun shownSeminarOnMainPage(seminarId: Long?) {
        //set isShownOnMainPage for all seminars to false
        val emptySeminar = SeminarExample()
        val falseMainPageSeminar = Seminar()
        falseMainPageSeminar.isShownOnMainPage = false
        seminarDAO.updateByExampleSelective(falseMainPageSeminar, emptySeminar)

        //set isShownOnMainPage for seminar with seminarId to true
        val selectedSeminar = seminarDAO.selectByPrimaryKey(seminarId)
        if (selectedSeminar != null) {
            selectedSeminar.isShownOnMainPage = true
            seminarDAO.updateByPrimaryKey(selectedSeminar)
        }
    }

    override fun hideSeminarFromMainPage(seminarId: Long?) {
        //set isShownOnMainPage for seminar with seminarId to false
        val selectedSeminar = seminarDAO.selectByPrimaryKey(seminarId)
        if (selectedSeminar != null) {
            selectedSeminar.isShownOnMainPage = false
            seminarDAO.updateByPrimaryKey(selectedSeminar)
        }
    }

    override fun selectCampaignTagsExistsInSeminars(): List<CampaignTag> {
        return campaignTagDAO.selectTagsExistsInSeminars()
    }

    override fun removeSeminar(seminarId: Long?) {
        val selectedSeminar = seminarDAO.selectByPrimaryKey(seminarId)
        if (selectedSeminar != null) {
            selectedSeminar.seminarType = SeminarType.DELETED
            seminarDAO.updateByPrimaryKey(selectedSeminar)
        }
    }
}


package ru.planeta.api.service.content

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import ru.planeta.api.BaseService
import ru.planeta.api.exceptions.NotFoundException
import ru.planeta.api.exceptions.PermissionException
import ru.planeta.api.service.configurations.StaticNodesService
import ru.planeta.api.text.ImageAttachment
import ru.planeta.dao.profiledb.PhotoAlbumDAO
import ru.planeta.dao.profiledb.PhotoDAO
import ru.planeta.model.Constants
import ru.planeta.model.enums.BlockSettingType
import ru.planeta.model.enums.PermissionLevel
import ru.planeta.model.enums.ThumbnailType
import ru.planeta.model.profile.media.Photo
import ru.planeta.model.profile.media.PhotoAlbum
import ru.planeta.utils.ThumbConfiguration
import java.util.*

/**
 * @author ds.kolyshev
 * Date: 27.10.11
 */
@Service
class PhotoServiceImpl(private val staticNodesService: StaticNodesService,
                       private val photoDAO: PhotoDAO,
                       private val photoAlbumDAO: PhotoAlbumDAO
                       ) : BaseService(), PhotoService {


    @Throws(PermissionException::class)
    override fun getUserHiddenPhotos(clientId: Long, profileId: Long, offset: Int, limit: Int): List<Photo> {
        permissionService.checkIsAdmin(clientId, profileId)
        val avatarsAlbum = photoAlbumDAO!!.selectAlbumByType(profileId, Constants.ALBUM_USER_HIDDEN)
        return if (avatarsAlbum != null) {
            photoDAO!!.selectPhotoByAlbumId(profileId, avatarsAlbum.getAlbumId()!!, offset, limit)
        } else emptyList()
    }

    @Throws(PermissionException::class)
    override fun getUserHiddenPhotosCount(clientId: Long, profileId: Long): Long {
        permissionService.checkIsAdmin(clientId, profileId)
        val avatarsAlbum = photoAlbumDAO!!.selectAlbumByType(profileId, Constants.ALBUM_USER_HIDDEN)
        return if (avatarsAlbum != null) {
            photoDAO!!.selectPhotoCountByAlbumId(profileId, avatarsAlbum.getAlbumId()!!)
        } else {
            0
        }
    }


    override fun insertAvatarPhotoAlbum(clientId: Long, profileId: Long, albumTypeId: Int, imageId: Long, imageUrl: String): Long {
        val photoAlbum = PhotoAlbum()
        photoAlbum.profileId = profileId
        photoAlbum.authorProfileId = clientId
        photoAlbum.photosCount = 1
        photoAlbum.timeAdded = Date()
        photoAlbum.timeUpdated = Date()
        photoAlbum.viewPermission = PermissionLevel.EVERYBODY
        photoAlbum.viewsCount = 0
        photoAlbum.albumTypeId = albumTypeId
        photoAlbum.title = "Album title"
        photoAlbum.imageUrl = imageUrl
        photoAlbum.imageId = imageId

        photoAlbumDAO!!.insert(photoAlbum)

        return photoAlbum.getAlbumId()!!
    }

    override fun addImageAttachmentsToAlbum(clientId: Long, ownerProfileId: Long, albumId: Long, albumTypeId: Int, attachments: List<ImageAttachment>): List<Photo> {
        var albumId = albumId
        val photos = ArrayList<Photo>(attachments.size)

        albumId = getAlbumId(ownerProfileId, albumId, albumTypeId)

        for (imageAttachment in attachments) {
            val externalUrl = imageAttachment.url
            val photo = getPhotoByUrl(clientId, ownerProfileId, albumId, externalUrl)

            photos.add(photo)
            imageAttachment.ownerId = ownerProfileId
            imageAttachment.url = photo.imageUrl
            imageAttachment.objectId = photo.objectId!!
        }

        return photos
    }

    private fun getPhotoByUrl(clientId: Long, ownerProfileId: Long, albumId: Long, externalUrl: String?): Photo {
        val proxyUrl = staticNodesService!!.wrapImageUrl(externalUrl, ThumbConfiguration(ThumbnailType.ORIGINAL))

        val photo = Photo()
        photo.profileId = ownerProfileId
        photo.albumId = albumId
        photo.authorProfileId = clientId
        photo.viewsCount = 0
        photo.description = ""
        photo.imageUrl = proxyUrl
        photoDAO!!.insert(photo)

        return photo
    }

    override fun transformPhotosToCampaignAlbum(ownerProfileId: Long, albumId: Long, albumTypeId: Int, photosMap: Map<String, Photo>): Map<String, Photo> {
        var albumId = albumId
        albumId = getAlbumId(ownerProfileId, albumId, albumTypeId)

        val newPhotoMap = HashMap<String, Photo>(photosMap.size)
        for ((oldPhotoId, photo) in photosMap) {
            val externalUrl = photo.imageUrl
            val newPhoto = getPhotoByUrl(ownerProfileId, ownerProfileId, albumId, externalUrl)

            newPhotoMap[oldPhotoId] = newPhoto
        }

        return newPhotoMap
    }


    private fun getAlbumId(ownerProfileId: Long, albumId: Long, albumTypeId: Int): Long {
        var albumId = albumId
        if (albumId == 0L) {
            var album: PhotoAlbum? = photoAlbumDAO!!.selectAlbumByType(ownerProfileId, albumTypeId)
            if (album == null) {
                album = PhotoAlbum()
                album.profileId = ownerProfileId
                album.authorProfileId = ownerProfileId
                album.albumTypeId = albumTypeId
                // TODO: check if existing album (of HIDDEN type) has the same permission level
                album.viewPermission = PermissionLevel.EVERYBODY
                album.timeAdded = Date()
                album.timeUpdated = Date()
                album.photosCount = 0
                album.viewsCount = 0
                photoAlbumDAO.insert(album)
            }
            albumId = album.getAlbumId()!!
        }
        return albumId
    }

    override fun getPhoto(clientId: Long, profileId: Long, photoId: Long): Photo? {
        return photoDAO!!.selectPhotoById(profileId, photoId)
    }

    @Throws(NotFoundException::class, PermissionException::class)
    override fun savePhoto(clientId: Long, photo: Photo): Photo {
        if (!permissionService.checkAddPermission(clientId, photo.profileId!!, BlockSettingType.PHOTO)) {
            throw PermissionException()
        }

        val currentPhoto = photoDAO!!.selectPhotoById(photo.profileId!!, photo.photoId)
                ?: throw NotFoundException(Photo::class.java, photo.photoId)
        currentPhoto.description = photo.description
        photoDAO.update(currentPhoto)
        return currentPhoto
    }

    @Throws(NotFoundException::class, PermissionException::class)
    override fun deletePhoto(clientId: Long, profileId: Long, photoId: Long) {

        val photo = photoDAO!!.selectPhotoById(profileId, photoId)
                ?: throw NotFoundException(Photo::class.java, photoId)

        val album = photoAlbumDAO!!.selectAlbumById(photo.profileId!!, photo.albumId)
                ?: throw NotFoundException(PhotoAlbum::class.java, photo.albumId)

        permissionService.checkIsAdmin(clientId, profileId)

        album.photosCount = album.photosCount - 1
        photoAlbumDAO.update(album)

        photoDAO.delete(profileId, photoId)

    }


}

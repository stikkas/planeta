package ru.planeta.api.model.json

import ru.planeta.model.shop.enums.DeliveryType

import java.math.BigDecimal

/**
 * User: eshevchenko
 */
class DeliveryInfo {

    var deliveryDepartmentId: Long = 0
        private set
    var deliveryPrice = BigDecimal.ZERO
        private set
    var deliveryType = DeliveryType.NOT_SET
        private set
    var comment: String? = ""

    constructor() {}

    constructor(deliveryServiceId: Long, deliveryPrice: BigDecimal, deliveryType: DeliveryType) {
        this.deliveryDepartmentId = deliveryServiceId
        this.deliveryPrice = deliveryPrice
        this.deliveryType = deliveryType
    }

    constructor(deliveryServiceId: Long, deliveryPrice: BigDecimal, deliveryType: DeliveryType, comment: String) {
        this.deliveryDepartmentId = deliveryServiceId
        this.deliveryPrice = deliveryPrice
        this.deliveryType = deliveryType
        this.comment = comment
    }
}

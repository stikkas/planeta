package ru.planeta.api.service.campaign

import ru.planeta.api.exceptions.PermissionException
import ru.planeta.model.common.campaign.Campaign

/**
 * User: a.savanovich
 * Date: 17.12.13
 * Time: 15:29
 */
interface CampaignPermissionService {

    fun isEditable(clientId: Long, campaign: Campaign): Boolean

    @Throws(PermissionException::class)
    fun checkIsEditable(clientId: Long, campaign: Campaign)

    fun isAuthorOrAuthorAdmin(clientId: Long, creatorProfileId: Long): Boolean

    @Throws(PermissionException::class)
    fun tooglePermission(editable: Boolean, campaignId: Long, managerId: Long)

    fun isEditableByAuthor(campaignId: Long): Boolean

    fun canCreate(clientId: Long, creatorProfileId: Long): Boolean

    @Throws(PermissionException::class)
    fun checkCanCreate(clientId: Long, creatorProfileId: Long)
}

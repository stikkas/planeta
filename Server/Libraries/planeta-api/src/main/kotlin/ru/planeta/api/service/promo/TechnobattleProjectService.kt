package ru.planeta.api.service.promo

import ru.planeta.model.promo.TechnobattleProject

/**
 * Created with IntelliJ IDEA.
 * User: michail
 * Date: 17.01.2017
 * Time: 12:43
 */
interface TechnobattleProjectService {
    fun getProject(projectId: Long): TechnobattleProject
    fun getProjects(offset: Int, limit: Int): List<TechnobattleProject>
    fun insertOrUpdate(project: TechnobattleProject)
}

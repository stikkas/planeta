package ru.planeta.api.service.biblio

import ru.planeta.api.exceptions.NotFoundException
import ru.planeta.model.bibliodb.Book
import ru.planeta.model.bibliodb.Library
import ru.planeta.model.bibliodb.Request

/**
 *
 * User: Serge Blagodatskih<stikkas17></stikkas17>@gmail.com><br></br>
 * Date: 27.04.16<br></br>
 * Time: 15:01
 */
interface RequestService {

    @Throws(NotFoundException::class)
    fun saveLibraryRequest(library: Library)

    fun saveBookRequest(book: Book)
}

package ru.planeta.api.service.promo

import ru.planeta.model.trashcan.PromoEmail

interface PromoEmailService {
    fun insert(promoEmail: PromoEmail)
}

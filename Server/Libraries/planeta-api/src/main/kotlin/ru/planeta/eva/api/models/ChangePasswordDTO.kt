package ru.planeta.eva.api.models

class ChangePasswordDTO {

    var oldPassword: String = ""
    var newPassword: String = ""
    var rePassword: String = ""
}

package ru.planeta.api.service.common

import ru.planeta.api.exceptions.NotFoundException
import ru.planeta.api.exceptions.PermissionException
import ru.planeta.model.common.campaign.CampaignTag
import ru.planeta.model.common.PlanetaManager
import ru.planeta.model.common.PlanetaCampaignManager
import ru.planeta.model.common.PlanetaManagerRelation

/**
 * Service for working with planeta managers
 *
 * @author m.shulepov
 * Date: 21.03.13
 */
interface PlanetaManagersService {

    /**
     * Returns all campaign relations with managers
     *
     * @return list of [PlanetaManagerRelation] objects
     */
    val campaignTagManagersRelations: List<PlanetaManagerRelation<*>>

    /**
     * Returns planeta manager for campaigns of specified `campaignTag`
     *
     * @param campaignTag campaign tag
     * @return Returns planeta manager for campaigns of specified `categoryTag`
     */
    fun getCampaignManagerByTag(campaignTag: CampaignTag?): PlanetaManager?

    @Throws(PermissionException::class, NotFoundException::class)
    fun setManagerForCurrentObject(managerForCurrentObject: PlanetaCampaignManager, clientId: Long)

    fun getCampaignManager(campaignId: Long): PlanetaManager

    fun getAllManagers(active: Boolean?): List<PlanetaManager>
    fun getManagerId(profileId: Long?): Long

    fun changeCampaignTagManagerRelation(managerId: Long?, tagId: Long?)

    fun createManager(name: String, fullName: String, email: String, profileId: Long?): PlanetaManager

    fun saveManager(manager: PlanetaManager)

    fun getManagersForCurrentCampaigns(campaignIdList: List<Long>): List<PlanetaManager>


}

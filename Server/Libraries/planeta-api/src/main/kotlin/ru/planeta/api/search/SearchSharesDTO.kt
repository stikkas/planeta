package ru.planeta.api.search

import com.fasterxml.jackson.annotation.JsonIgnoreProperties

@JsonIgnoreProperties(ignoreUnknown = true)
class SearchSharesDTO {

    var data: SearchShares? = null
}

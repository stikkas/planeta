package ru.planeta.api.service.biblio

import org.springframework.stereotype.Service
import ru.planeta.api.Utils
import ru.planeta.api.exceptions.NotFoundException
import ru.planeta.api.service.notifications.NotificationService
import ru.planeta.dao.bibliodb.LibraryDAO
import ru.planeta.model.bibliodb.Library
import ru.planeta.model.bibliodb.enums.BiblioObjectStatus
import ru.planeta.model.bibliodb.enums.LibraryType
import java.util.*

@Service
class LibraryServiceImpl(private val libraryDAO: LibraryDAO) : LibraryService {

    override val countActiveLibraries: Long
        get() = libraryDAO.countActiveLibraries()

    override fun searchClusterLibraries(searchStr: String?, libraryType: LibraryType?, north: Double, south: Double, east: Double, west: Double,
                                        zoomLevel: Long, minZoomLevel: Long): List<*> {
        val newString = searchStr?.trim { it <= ' ' }
        return if (zoomLevel > minZoomLevel) {
            libraryDAO.searchLibraries(newString!!, libraryType!!, north, south, east, west)
        } else {
            libraryDAO.searchLibraryClusters(newString!!, libraryType!!, north, south, east, west)
        }
    }

    override fun selectLibrariesList(libraryIdList: List<Long>): List<Library> {
        return if (libraryIdList == null || libraryIdList.isEmpty()) {
            ArrayList()
        } else libraryDAO.selectListByIds(libraryIdList)
    }

    override fun getLibrary(libraryId: Long): Library? {
        return libraryDAO.select(libraryId)
    }

    override fun delete(library: Library) {
        library.status = BiblioObjectStatus.DELETED
        libraryDAO.update(library)
    }

    @Throws(NotFoundException::class)
    override fun save(library: Library, notificationService: NotificationService) {
        if (library.libraryId == -1L) {
            library.timeAdded = Date()
            libraryDAO.insert(library)
        } else {
            val fromDb = libraryDAO.select(library.libraryId)
                    ?: throw NotFoundException(Library::class.java, library.libraryId)
            val oldStatus = fromDb.status
            if (oldStatus !== library.status && library.status === BiblioObjectStatus.ACTIVE) {
                notificationService.sendBiblioLibraryActiveNotification(library)
            }
            libraryDAO.update(library)
        }
    }

    override fun randomLibrary(exludedLibraries: Collection<Library>): Library {
        var exludedLibraries = exludedLibraries
        if (Utils.empty(exludedLibraries)) {
            exludedLibraries = mutableListOf()
        }
        return libraryDAO.randomLibrary(exludedLibraries)
    }

    override fun hasLibraryRequests(name: String, email: String): Boolean {
        return libraryDAO.hasLibraryRequests(name, email)
    }
}

package ru.planeta.api.service.biblio

/*
 * Created by Alexey on 06.06.2016.
 */

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import ru.planeta.api.BaseService
import ru.planeta.dao.bibliodb.PublishingHouseDAO
import ru.planeta.model.bibliodb.PublishingHouse

@Service
class PublishingHouseServiceImpl @Autowired
constructor(private val publishingHouseDAO: PublishingHouseDAO) : BaseService(), PublishingHouseService {

    override fun getPublishingHouses(searchRow: String, offset: Int, limit: Int): List<PublishingHouse> {
        return publishingHouseDAO.getPublishingHouses(searchRow, offset, limit)
    }

    override fun getPublishingHouseById(publishingHouseId: Long): PublishingHouse {
        return publishingHouseDAO.getPublishingHouseById(publishingHouseId)
    }

    override fun addPublishingHouse(publishingHouse: PublishingHouse) {
        if (publishingHouse.publishingHouseId == 0L) {
            publishingHouseDAO.insertPublishingHouse(publishingHouse)
        } else {
            publishingHouseDAO.updatePublishingHouse(publishingHouse)
        }
    }

    override fun selectCountPublishingHouses(searchRow: String): Long {
        return publishingHouseDAO.selectCountPublishingHouses(searchRow)
    }
}

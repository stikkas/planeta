package ru.planeta.api.service.campaign

import ru.planeta.api.exceptions.NotFoundException
import ru.planeta.api.exceptions.PermissionException
import ru.planeta.model.common.campaign.CampaignTargeting

import javax.validation.constraints.NotNull

interface CampaignTargetingService {
    fun getCampaignTargetingByCampaignId(campaignId: Long): CampaignTargeting

    @Throws(PermissionException::class, NotFoundException::class)
    fun insertOrUpdate(clientId: Long, @NotNull campaignTargeting: CampaignTargeting)
}

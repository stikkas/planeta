package ru.planeta.api.advertising

import ru.planeta.api.exceptions.NotFoundException
import ru.planeta.model.advertising.AdvertisingDTO
import ru.planeta.model.advertising.VastDTO
import ru.planeta.model.advertising.geenerated.VAST

import javax.xml.bind.JAXBException
import javax.xml.datatype.DatatypeConfigurationException
import javax.validation.constraints.NotNull

/**
 * Created with IntelliJ IDEA.
 * Date: 16.01.14
 * Time: 13:59
 */
interface AdvertisingService {
    fun extractVast(advertisingDTO: AdvertisingDTO): VAST

    @Throws(JAXBException::class)
    fun putVastString(vast: VAST, advertisingDTO: AdvertisingDTO)

    fun extractDTO(vast: VAST): VastDTO

    @Throws(DatatypeConfigurationException::class)
    fun createNewVast(dto: VastDTO): VAST

    fun saveAdvertising(dto: AdvertisingDTO)

    @NotNull
    @Throws(NotFoundException::class)
    fun loadLocalAdvertising(broadcastId: Long): AdvertisingDTO

    @Throws(NotFoundException::class)
    fun loadActiveLocalAdvertising(broadcastId: Long): AdvertisingDTO

    fun loadGlobalAdvertising(): List<AdvertisingDTO>

    @Throws(NotFoundException::class)
    fun loadActiveGlobalAdvertising(): AdvertisingDTO

    fun deleteAdvertising(broadcastId: Long)

    fun startFinishByDateAdvertising()

    fun updateAdvertisingWhenCampaignIsNotActive(campaignId: Long, isActive: Boolean)
}

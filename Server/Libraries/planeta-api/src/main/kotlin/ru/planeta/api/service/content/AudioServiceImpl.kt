package ru.planeta.api.service.content

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import ru.planeta.api.exceptions.NotFoundException
import ru.planeta.api.exceptions.PermissionException
import ru.planeta.api.service.security.PermissionService
import ru.planeta.dao.profiledb.AudioTrackDAO
import ru.planeta.model.profile.media.AudioTrack
import java.util.Collections

/**
 * @author s.fionov
 * Date: 28.10.11
 */
@Service
class AudioServiceImpl(private val audioTrackDAO: AudioTrackDAO, private val permissionService: PermissionService) : AudioService {
    override fun getAudioTracks(clientId: Long, profileId: Long, offset: Int, limit: Int, searchString: String?): List<AudioTrack> {
        return audioTrackDAO!!.selectAudioTracks(profileId, offset, limit, searchString!!)
    }

    override fun getAudioAlbumTracks(clientId: Long, profileId: Long, albumId: Long, offset: Int, limit: Int): List<AudioTrack> {
        return audioTrackDAO!!.selectAudioTracksByAlbumId(profileId, albumId, offset, limit)
    }

    override fun getAudioTrack(clientId: Long, profileId: Long, trackId: Long): AudioTrack {
        return audioTrackDAO!!.selectAudioTrackById(profileId, trackId)
    }

    @Throws(NotFoundException::class)
    override fun getAudioTrackSafe(clientId: Long, profileId: Long, trackId: Long): AudioTrack {
        return audioTrackDAO!!.selectAudioTrackById(profileId, trackId)
                ?: throw NotFoundException(AudioTrack::class.java, trackId)
    }

    @Throws(NotFoundException::class, PermissionException::class)
    override fun saveAudioTrack(clientId: Long, audioTrack: AudioTrack): AudioTrack {

        permissionService!!.checkIsAdmin(clientId, audioTrack.profileId!!)

        val currentTrack = audioTrackDAO!!.selectAudioTrackById(audioTrack.profileId!!, audioTrack.trackId)
                ?: throw NotFoundException("Audio track not found")

        currentTrack.artistName = audioTrack.artistName
        currentTrack.authors = audioTrack.authors
        currentTrack.lyrics = audioTrack.lyrics
        currentTrack.trackName = audioTrack.trackName

        audioTrackDAO.update(currentTrack)


        return currentTrack

    }

    override fun getPlaylistTracks(clientId: Long, profileId: Long, playlistId: Int, searchString: String): List<AudioTrack> {

        return if (permissionService!!.isAdmin(clientId, profileId)) {
            audioTrackDAO!!.selectAudioTracksByPlaylistId(profileId, playlistId, searchString)
        } else emptyList()

    }


    @Throws(PermissionException::class)
    override fun switchTracksPositionsAfterAddTrack(clientId: Long, profileId: Long) {

        val tracks = getAudioTracks(clientId, profileId, 0, 0, null)

        for (i in 1 until tracks.size) {
            val audioTrack = tracks[i]
            audioTrack.albumTrackNumber = i + 1
            audioTrackDAO!!.update(audioTrack)
        }


    }
}

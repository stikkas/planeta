package ru.planeta.eva.api.services

import org.apache.commons.lang3.StringUtils
import org.springframework.stereotype.Service
import ru.planeta.api.exceptions.MessageCode
import ru.planeta.api.exceptions.NotFoundException
import ru.planeta.api.exceptions.PermissionException
import ru.planeta.api.service.profile.PasswordEncoder
import ru.planeta.api.service.security.PermissionService
import ru.planeta.dao.commondb.UserPrivateInfoDAO
import ru.planeta.eva.api.models.ChangePasswordDTO
import ru.planeta.model.common.UserPrivateInfo

@Service
class AuthorizationService(private val permissionService: PermissionService,
                           private val userPrivateInfoDAO: UserPrivateInfoDAO,
                           private val passwordEncoder: PasswordEncoder) {

    @Throws(NotFoundException::class, PermissionException::class)
    fun saveUserPassword(clientId: Long, change: ChangePasswordDTO, profileId: Long): UserPrivateInfo {
        permissionService.checkIsAdmin(clientId, profileId)

        val userPrivateInfo = userPrivateInfoDAO.selectByUserId(profileId)
                ?: throw NotFoundException(UserPrivateInfo::class.java, profileId)

        val newPassword = change.newPassword.trim { it <= ' ' }

        if (!checkPassword(userPrivateInfo, change.oldPassword)) {
            throw PermissionException(MessageCode.WRONG_PASSWORDCHANGE_CURRENTPASSWORD)
        }

        userPrivateInfo.password = passwordEncoder.encode(newPassword)
        userPrivateInfoDAO.update(userPrivateInfo)
        return userPrivateInfo
    }

    fun checkPassword(userPrivateInfo: UserPrivateInfo?, inputPassword: String): Boolean {
        return if (StringUtils.isBlank(inputPassword) || userPrivateInfo == null) {
            false
        } else passwordEncoder.matches(inputPassword, userPrivateInfo.password ?: "")
    }
}

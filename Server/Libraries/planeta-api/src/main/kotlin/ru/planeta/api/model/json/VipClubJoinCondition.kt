package ru.planeta.api.model.json

import com.fasterxml.jackson.annotation.JsonIgnoreProperties

import java.math.BigDecimal

/**
 *
 * Created by eshevchenko on 03.12.14.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
class VipClubJoinCondition {

    /**
     * Purchases count.
     */
    private var count: Int? = null

    /**
     * Total amount of purchases.
     */
    var amount: BigDecimal? = null
        get() = if (field == null) DEFAULT_AMOUNT else field

    /**
     * Second Total amount of purchases.
     */
    var second: BigDecimal? = null
        get() = if (field == null) DEFAULT_SECOND else field

    fun getCount(): Int {
        return if (count == null) {
            DEFAULT_COUNT
        } else count!!
    }

    fun setCount(count: Int?) {
        this.count = count
    }

    companion object {

        private val DEFAULT_COUNT = 2
        private val DEFAULT_AMOUNT = BigDecimal(20000)
        private val DEFAULT_SECOND = BigDecimal(15000)
    }
    /*
    public boolean check(int count, BigDecimal amount) {
        return (count >= getCount()) && (amount.compareTo(getAmount()) >= 0);
    }
*/
}

package ru.planeta.api.log


import org.apache.log4j.spi.LoggingEvent
import java.util.LinkedList

/**
 * @author p.vyazankin
 * @since 3/5/13 11:47 AM
 */
class AccumulationInfo(loggingEvent: LoggingEvent) {
    val earliestTimestamp: Long
    private val loggingEvents = LinkedList<LoggingEvent>()

    init {
        earliestTimestamp = loggingEvent.timeStamp
        loggingEvents.add(loggingEvent)
    }

    fun oneMore(loggingEvent: LoggingEvent) {
        loggingEvents.add(loggingEvent)
    }

    fun size(): Int {
        return loggingEvents.size
    }
}

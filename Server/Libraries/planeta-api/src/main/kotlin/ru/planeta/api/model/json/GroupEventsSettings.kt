package ru.planeta.api.model.json

import ru.planeta.model.enums.PermissionLevel

/**
 * @author ds.kolyshev
 * Date: 11.11.11
 */
class GroupEventsSettings {
    var profileId: Long = 0

    var eventsCreatePermission = PermissionLevel.EVERYBODY
    var eventsOrderPermission = PermissionLevel.EVERYBODY
}

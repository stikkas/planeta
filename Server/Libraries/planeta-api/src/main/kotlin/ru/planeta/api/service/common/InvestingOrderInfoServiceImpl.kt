package ru.planeta.api.service.common

import org.springframework.context.annotation.Lazy
import org.springframework.stereotype.Service
import ru.planeta.api.exceptions.NotFoundException
import ru.planeta.api.exceptions.PermissionException
import ru.planeta.api.service.notifications.NotificationService
import ru.planeta.dao.commondb.InvestingOrderInfoDAO
import ru.planeta.model.common.InvestingOrderInfo
import ru.planeta.model.enums.ContractorType
import ru.planeta.model.enums.ModerateStatus
import java.util.*

/**
 *
 * User: Serge Blagodatskih<stikkas17></stikkas17>@gmail.com><br></br>
 * Date: 28.01.16<br></br>
 * Time: 10:56
 */
@Service
class InvestingOrderInfoServiceImpl(
        private var ioiDao: InvestingOrderInfoDAO,
        @Lazy
        private var notificationService: NotificationService) : InvestingOrderInfoService {

    @Throws(NotFoundException::class, PermissionException::class)
    override fun save(info: InvestingOrderInfo) {
        if (ioiDao.select(info.investingOrderInfoId!!) == null) {
            ioiDao.insert(info)
            notificationService.sendInvestingInvoiceRequestNotification(info)
        } else {
            ioiDao.update(info)
        }
    }

    override fun select(userId: Long, userType: ContractorType): InvestingOrderInfo {
        return ioiDao.select(userId, userType) ?: InvestingOrderInfo()
    }

    override fun select(orderId: Long): InvestingOrderInfo {
        return ioiDao.select(orderId) ?: InvestingOrderInfo()
    }

    override fun delete(orderId: Long?) {
        ioiDao.delete(orderId!!)
    }

    override fun select(status: ModerateStatus, start: Date, end: Date, offset: Int, limit: Int): List<InvestingOrderInfo> {
        return ioiDao.selectList(status, start, end, offset, limit)
    }

    override fun select(ids: List<Long>): List<InvestingOrderInfo> {
        return ioiDao.selectList(ids)
    }

    override fun count(): Long {
        return ioiDao.count()
    }

    @Throws(NotFoundException::class, PermissionException::class)
    override fun changeModerateStatus(orderId: Long, status: ModerateStatus) {
        val info = select(orderId)
        if (status === ModerateStatus.APPROVED) {
            notificationService.sendInvestingInvoiceCreatedNotification(info)
        }
        info.moderateStatus = status
        save(info)
    }
}

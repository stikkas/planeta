package ru.planeta.api.exceptions

import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ResponseStatus

@ResponseStatus(value = HttpStatus.FORBIDDEN, reason = "Access denied")
open class PermissionException : BaseException {

    constructor() {}


    @Deprecated("Use another constructor")
    constructor(message: String) : super(message) {
    }

    constructor(messageCode: MessageCode) : super(messageCode) {}

    constructor(messageCode: MessageCode, formattedMessage: String) : super(messageCode, formattedMessage) {}

    constructor(messageCode: MessageCode, vararg arguments: Any) : super(messageCode, arguments) {}
}

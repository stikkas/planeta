package ru.planeta.api.service.content

import org.apache.log4j.Logger
import org.sphx.api.SphinxException
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import ru.planeta.api.BaseService
import ru.planeta.api.exceptions.*
import ru.planeta.api.model.json.VideoInfo
import ru.planeta.api.model.json.VideoInfoForFlash
import ru.planeta.api.text.VideoAttachment
import ru.planeta.commons.external.oembed.provider.VideoType
import ru.planeta.dao.TransactionScope
import ru.planeta.dao.profiledb.VideoDAO
import ru.planeta.model.common.CachedVideo
import ru.planeta.model.enums.BlockSettingType
import ru.planeta.model.enums.PermissionLevel
import ru.planeta.model.profile.media.Video
import ru.planeta.model.profile.media.enums.VideoConversionStatus
import ru.planeta.model.profile.media.enums.VideoQuality
import java.security.InvalidParameterException
import java.util.Date
import java.util.EnumSet

/**
 * @author ds.kolyshev
 * Date: 28.10.11
 */
@Service
class VideoServiceImpl(
        private val videoCacheService: VideoCacheService,
        private val videoDAO: VideoDAO) : BaseService(), VideoService {


    @Throws(VideoPermissionException::class, VideoNotFoundException::class)
    override fun getVideo(clientId: Long, profileId: Long, videoId: Long): VideoInfo {
        val videoInfo: VideoInfo
        val video = videoDAO.selectVideoById(profileId, videoId) ?: throw VideoNotFoundException()
//
        if (video.cachedVideoId > 0) {
            val cachedVideo = videoCacheService.getVideo(video.cachedVideoId)
            videoInfo = VideoInfo(video, cachedVideo)
        } else {
            videoInfo = VideoInfo(video)
        }
        //check whole category permission && check that video permission
        return if (permissionService.checkViewPermission(clientId, profileId, BlockSettingType.VIDEO) && permissionService.checkVideoViewPermission(clientId, profileId, video)) {
            videoInfo
        } else {
            throw VideoPermissionException(videoInfo.name, videoInfo.authorName, videoInfo.ownerName)
        }
    }

    @Throws(VideoPermissionException::class, VideoNotFoundException::class, VideoProcessingException::class)
    override fun getVideoToSee(clientId: Long, profileId: Long, videoId: Long): VideoInfo {
        val videoInfo = getVideo(clientId, profileId, videoId)
        if (videoInfo.status === VideoConversionStatus.PROCESSING) {
            throw VideoProcessingException()
        }
        return videoInfo
    }

    override fun getVideoSafely(clientId: Long, profileId: Long, videoId: Long): VideoInfo? {
        try {
            return getVideo(clientId, profileId, videoId)
        } catch (e1: VideoNotFoundException) {
            log.info("Video not found exception: $videoId")
            return null
        } catch (e2: VideoPermissionException) {
            log.info("Video permission exception: $videoId")
            return null
        }

    }

    @Throws(NotFoundException::class, PermissionException::class)
    override fun saveVideo(clientId: Long, video: Video): Video {


        if (!permissionService.checkWritePermission(clientId, video.profileId ?: -1, BlockSettingType.VIDEO)) {
            throw PermissionException()
        }
        val currentVideo = videoDAO.selectVideoById(video.profileId!!, video.videoId)
                ?: throw NotFoundException("Video not found")

        currentVideo.description = video.description
        currentVideo.name = video.name
        currentVideo.timeUpdated = Date()
        currentVideo.viewPermission = video.viewPermission
        videoDAO.update(currentVideo)


        return currentVideo

    }

    @Throws(NotFoundException::class, PermissionException::class)
    override fun deleteVideo(clientId: Long, profileId: Long, videoId: Long) {


        if (!permissionService.checkWritePermission(clientId, profileId, BlockSettingType.VIDEO)) {
            throw PermissionException()
        }

        val currentVideo = videoDAO.selectVideoById(profileId, videoId) ?: throw NotFoundException("Video not found")

        videoDAO.delete(profileId, videoId)

    }

    /**
     * Adds Youtube video
     */
    @Throws(NotFoundException::class, PermissionException::class)
    override fun addExternalVideo(clientId: Long, profileId: Long, externalUrl: String, videoType: VideoType?): VideoInfo {
        val transactionScope = TransactionScope.createOrGetCurrent()
        try {
            if (!permissionService.checkAddPermission(clientId, profileId, BlockSettingType.VIDEO)) {
                throw PermissionException()
            }

            val cachedVideo = videoCacheService.getVideo(externalUrl)
                    ?: throw NotFoundException("not.found.external.video")

            val cachedExternalVideo = videoDAO.selectVideoByCacheId(profileId, clientId, cachedVideo.cachedVideoId)
            if (cachedExternalVideo != null) {
                return VideoInfo(cachedExternalVideo, cachedVideo)
            }

            val video = Video()
            video.name = cachedVideo.title
            video.description = cachedVideo.description
            video.duration = cachedVideo.duration.toInt()
            video.videoUrl = cachedVideo.externalId
            video.profileId = profileId
            video.status = VideoConversionStatus.PROCESSED
            video.authorProfileId = clientId

            // TODO: Upload image instead of wrapping it in proxy
            video.imageId = 0
            video.imageUrl = cachedVideo.thumbnailUrl
            video.availableQualities = EnumSet.of(VideoQuality.NOT_SET)
            video.viewPermission = PermissionLevel.EVERYBODY
            if (VideoType.EXTERNAL_HIDDEN == videoType) {
                video.videoType = videoType
            } else {
                video.videoType = if (cachedVideo.videoType == null) VideoType.EXTERNAL else cachedVideo.videoType
            }
            video.timeAdded = Date()
            video.timeUpdated = Date()
            video.cachedVideoId = cachedVideo.cachedVideoId

            videoDAO.insert(video)

            transactionScope.commit()

            return VideoInfo(video, cachedVideo)
        } catch (e: InvalidParameterException) {
            throw InvalidParameterException("youtube.error.invalid")
        } catch (e: PermissionException) {
            log.warn("Error adding external video: $externalUrl", e)
            throw e
        } finally {
            transactionScope.close()
        }
    }

    @Throws(NotFoundException::class)
    override fun getExternalVideoDetails(url: String): VideoInfo {
        val cachedVideo = videoCacheService.getVideo(url) ?: throw NotFoundException("not.found.external.video")

        return VideoInfo(cachedVideo)
    }

    @Throws(PermissionException::class, SphinxException::class, VideoNotFoundException::class)
    override fun getVideoInfoExtended(clientId: Long, profileId: Long, videoId: Long): VideoInfoForFlash {
        val videoInfo = getVideo(clientId, profileId, videoId)
        return VideoInfoForFlash(videoInfo)
    }

    @Throws(NotFoundException::class, PermissionException::class)
    override fun addExternalVideoAttachments(clientId: Long, ownerProfileId: Long, videoAttachments: List<VideoAttachment>) {
        if (videoAttachments == null) {
            throw NotFoundException()
        }
        for (attachment in videoAttachments) {
            if (attachment.videoType != VideoType.PLANETA) {
                val videoInfo = addExternalVideo(clientId, ownerProfileId, attachment.videoUrl, VideoType.EXTERNAL_HIDDEN)
                if (videoInfo.video == null) {
                    throw NotFoundException()
                }
                attachment.ownerId = ownerProfileId
                attachment.objectId = videoInfo?.video?.objectId ?: 0
            }
        }
    }

    companion object {

        private val log = Logger.getLogger(VideoServiceImpl::class.java)
    }

}

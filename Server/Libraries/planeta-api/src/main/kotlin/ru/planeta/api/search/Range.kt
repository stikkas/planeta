package ru.planeta.api.search

/**
 * @author m.shulepov
 * Date: 24.06.13
 */
class Range<T>(val min: T, val max: T)


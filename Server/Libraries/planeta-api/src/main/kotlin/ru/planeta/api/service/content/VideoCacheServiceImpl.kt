package ru.planeta.api.service.content

import org.apache.log4j.Logger
import org.springframework.stereotype.Service
import ru.planeta.api.BaseService
import ru.planeta.api.service.configurations.StaticNodesService
import ru.planeta.commons.external.oembed.Oembed
import ru.planeta.dao.commondb.VideoCacheDAO
import ru.planeta.model.common.CachedVideo
import ru.planeta.utils.ThumbConfiguration

/**
 * Created with IntelliJ IDEA.
 * User: connanedoile
 * Date: 4/10/12
 * Time: 11:32 AM
 */
@Service
class VideoCacheServiceImpl(val videoCacheDAO: VideoCacheDAO,
                            private val staticNodesService: StaticNodesService) : VideoCacheService {


    override fun getVideo(url: String): CachedVideo {
        try {
            val naturalUrl = Oembed.getNaturalUrl(url) ?: return CachedVideo()
            var cachedVideo: CachedVideo? = videoCacheDAO.selectByUrl(naturalUrl)
            if (cachedVideo != null) {
                return cachedVideo
            }

            val oembedInfo = Oembed.getExternalVideoInfo(naturalUrl) ?: return CachedVideo()

            cachedVideo = CachedVideo()

            cachedVideo.url = oembedInfo.naturalUrl
            cachedVideo.description = oembedInfo.description
            cachedVideo.title = oembedInfo.name
            cachedVideo.duration = oembedInfo.duration.toLong()
            cachedVideo.html = oembedInfo.html
            cachedVideo.width = oembedInfo.width
            cachedVideo.height = oembedInfo.height

            val commentThumb = ThumbConfiguration()
            // TODO: Move to constants
            commentThumb.width = 640
            commentThumb.height = 360
            commentThumb.isCrop = true
            commentThumb.isPad = false

            val imagePath = staticNodesService.wrapImageUrl(oembedInfo.thumbnailUrl, commentThumb)

            cachedVideo.thumbnailUrl = imagePath
            cachedVideo.externalId = oembedInfo.id
            cachedVideo.videoType = oembedInfo.videoType

            saveCachedVideo(cachedVideo)
            return cachedVideo
        } catch (ex: Exception) {
            logger.warn("Error getting external video data", ex)
            return CachedVideo()
        }

    }

    override fun getVideo(cachedVideoId: Long): CachedVideo {
        return videoCacheDAO!!.selectById(cachedVideoId)
    }

    protected fun saveCachedVideo(cachedVideo: CachedVideo?): CachedVideo? {
        if (cachedVideo != null) {
            videoCacheDAO.insert(cachedVideo)
        }
        return cachedVideo
    }

    companion object {

        protected val logger = Logger.getLogger(BaseService::class.java)
    }
}

package ru.planeta.api.service.common

import ru.planeta.model.common.campaign.CampaignTag
import ru.planeta.model.common.school.Seminar
import ru.planeta.model.common.school.SeminarWithTagNameAndCityName

interface SeminarService {
    fun updateSeminar(seminar: Seminar)
    fun insertOrUpdateSeminar(seminar: Seminar)
    fun shownSeminarOnMainPage(seminarId: Long?)
    fun hideSeminarFromMainPage(seminarId: Long?)
    fun selectSeminar(seminarId: Long?): Seminar
    fun selectSeminarWithTagNameAndCityName(seminarId: Long?): SeminarWithTagNameAndCityName
    fun selectSeminarWithTagNameList(seminarIdList: List<Long>?): List<SeminarWithTagNameAndCityName>
    fun selectSeminarListIsShownOnMainPage(isShownOnMainPage: Boolean?, offset: Int, limit: Int): List<Seminar>
    fun selectSeminarWithTagNameAndCityNameListIsShownOnMainPage(isShownOnMainPage: Boolean?, offset: Int, limit: Int): List<SeminarWithTagNameAndCityName>
    fun selectCampaignTagsExistsInSeminars(): List<CampaignTag>
    fun removeSeminar(seminarId: Long?)
}

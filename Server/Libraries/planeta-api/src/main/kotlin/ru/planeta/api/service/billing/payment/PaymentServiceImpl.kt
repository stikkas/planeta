package ru.planeta.api.service.billing.payment

import org.apache.log4j.Logger
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service
import ru.planeta.api.BaseService
import ru.planeta.api.exceptions.MessageCode.*
import ru.planeta.api.exceptions.NotFoundException
import ru.planeta.api.exceptions.PaymentException
import ru.planeta.api.exceptions.PermissionException
import ru.planeta.api.log.DBLogger
import ru.planeta.api.log.LoggerListProxy
import ru.planeta.api.log.service.DBLogService
import ru.planeta.api.service.billing.order.OrderService
import ru.planeta.api.service.profile.ProfileBalanceService
import ru.planeta.dao.commondb.TopayTransactionDAO
import ru.planeta.dao.payment.PaymentMethodDAO
import ru.planeta.dao.payment.PaymentProviderDAO
import ru.planeta.model.common.Order
import ru.planeta.model.common.PaymentMethod
import ru.planeta.model.common.PaymentProvider
import ru.planeta.model.common.TopayTransaction
import ru.planeta.model.enums.OrderObjectType
import ru.planeta.model.enums.ProjectType
import ru.planeta.model.enums.TopayTransactionStatus
import ru.planeta.model.stat.log.LoggerType
import java.math.BigDecimal
import java.util.*

/**
 * Created by IntelliJ IDEA.
 * User: atropnikov
 * Date: 22.12.11
 * Time: 12:11
 */
@Service
class PaymentServiceImpl @Autowired
constructor(private var dbLogService: DBLogService,
            private var orderService: OrderService,
            private var providerDAO: PaymentProviderDAO,
            private var paymentMethodDAO: PaymentMethodDAO,
            private var paymentErrorsService: PaymentErrorsService,
            private var profileBalanceService: ProfileBalanceService,
            private var topayTransactionDAO: TopayTransactionDAO) : BaseService(), PaymentService {

    @Value("\${maximum.payment.per.one.transaction:100000}")
    private val maxPerOperation: Int? = null

    private val logger: Logger

    init {
        logger = LoggerListProxy("paymentServiceLogger", Logger.getLogger(PaymentServiceImpl::class.java), DBLogger.getLogger(LoggerType.PAYMENT, dbLogService))
    }


    @Throws(PaymentException::class)
    override fun createInternalPayment(profileId: Long, amount: BigDecimal, paymentMethod: PaymentMethod): TopayTransaction {
        if (paymentMethod == null || !paymentMethod.isInternal) {
            throw PaymentException(PAYMENT_NOT_INTERNAL_SYSTEM_TYPE)
        }
        val transaction = createPayment(profileId, 0L, amount, ProjectType.MAIN, paymentMethod.id, null, null, true)
        transaction.paymentProviderId = providerDAO.select(PaymentProvider.Type.PLANETA).id
        return transaction
    }

    @Throws(PaymentException::class)
    override fun createPayment(profileId: Long, amount: BigDecimal, paymentMethodId: Long, projectType: ProjectType, phone: String): TopayTransaction {
        return createPayment(profileId, 0L, amount, projectType, paymentMethodId, null, phone, true)
    }

    @Throws(PaymentException::class)
    private fun createPayment(profileId: Long, orderId: Long, amountNet: BigDecimal?, projectType: ProjectType?, paymentMethodId: Long, expireDate: Date?, phone: String?, tlsSupported: Boolean): TopayTransaction {
        if (BigDecimal.ZERO.compareTo(amountNet) > 0) {
            throw PaymentException(PAYMENT_AMOUNT_NOT_IN_RANGE)
        }
        if (permissionService.isAnonymous(profileId)) {
            throw PaymentException(PAYMENT_CAN_NOT_CREATE_FOR_ANONYMOUS)
        }
        val paymentMethod = getPaymentMethod(paymentMethodId) ?: throw PaymentException(PAYMENT_METHOD_NOT_FOUND)

        if (expireDate != null) {
            if (expireDate.before(Date())) {
                throw PaymentException(PAYMENT_EXPIRED)
            }
        }

        val result = TopayTransaction()
        result.profileId = profileId
        result.orderId = orderId
        result.amountNet = amountNet
        result.amountFee = BigDecimal.ZERO
        result.status = TopayTransactionStatus.NEW
        result.timeAdded = Date()
        result.projectType = projectType
        result.paymentMethodId = paymentMethodId
        result.expireDate = expireDate
        result.param1 = phone
        result.isTlsSupported = tlsSupported

        try {

            topayTransactionDAO.insert(result)
            logger.info(BaseService.Companion.getMessage(PAYMENT_CREATED, messageSource, result.transactionId))

            return result
        } catch (e: Exception) {
            val msg = String.format("Error creating payment transaction: profileId[%d] amountNet[%s]", profileId, amountNet)
            logger.error(msg, e)
            throw PaymentException(PAYMENT_CREATING_ERROR, e)
        }

    }

    @Throws(PaymentException::class)
    override fun createOrderPayment(order: Order, amountToPay: BigDecimal, paymentMethodId: Long, projectType: ProjectType, phone: String?, tlsSupported: Boolean): TopayTransaction {
        var expireDate: Date? = null
        if (order.orderType === OrderObjectType.SHARE) {
            expireDate = campaignDAO.selectCampaignByOrder(order.orderId).timeFinish
        }
        val result = createPayment(order.buyerId, order.orderId, amountToPay, projectType, paymentMethodId, expireDate, phone, tlsSupported)
        orderService.assignOrderWithPayment(order, result)
        return result
    }

    @Throws(NotFoundException::class, PaymentException::class, PermissionException::class)
    override fun createPaymentFrom(transactionId: Long, paymentMethodId: Long, phone: String): TopayTransaction {
        val payment = topayTransactionDAO.select(transactionId)
                ?: throw NotFoundException(TopayTransaction::class.java, transactionId)
        updatePaymentStatus(payment, TopayTransactionStatus.ERROR)

        val result = createPayment(payment.profileId, payment.orderId, payment.amountNet, payment.projectType, paymentMethodId, payment.expireDate, phone, true)
        if (payment.orderId > 0) {
            val order = orderService.getOrderSafe(payment.orderId)
            orderService.assignOrderWithPayment(order, result)
        }

        return result
    }

    override fun getPayment(transactionId: Long?): TopayTransaction {
        return topayTransactionDAO.select(transactionId ?: 0)
    }

    @Deprecated("")
    @Synchronized
    override fun updatePaymentStatus(transaction: TopayTransaction, status: TopayTransactionStatus) {
        if (transaction.status === status) {
            return
        }

        transaction.status = status
        transaction.timeUpdated = Date()
        topayTransactionDAO.update(transaction)

        if (status === TopayTransactionStatus.ERROR) {
            paymentErrorsService.createPaymentErrors(transaction)
        }
    }

    @Throws(PaymentException::class)
    override fun processPayment(transaction: TopayTransaction, externalSystemData: String): Boolean {
        if (transaction.debitTransactionId > 0) {
            logger.warn("transaction #" + transaction.transactionId + " already has debit transaction")
            return false
        }

        if (!EnumSet.of(TopayTransactionStatus.NEW, TopayTransactionStatus.ERROR).contains(transaction.status)) {
            logger.warn("transaction #" + transaction.transactionId + " already processed with state " + transaction.status)
            return false
        }
        try {
            val profileId = transaction.profileId
            val provider = providerDAO.select(transaction.paymentProviderId)
            val comment = provider.type.toString() + " payment."
            val amountNet = transaction.amountNet
            if (amountNet?.compareTo(BigDecimal.ZERO) ?: 0 > 0) {
                val debitTransaction = profileBalanceService
                        .increaseProfileBalance(profileId, amountNet ?: BigDecimal.ZERO, BigDecimal.ZERO, comment, provider.id, externalSystemData)

                transaction.debitTransactionId = debitTransaction.transactionId
            }
            updatePaymentStatus(transaction, TopayTransactionStatus.DONE)
            return true
        } catch (e: Exception) {
            updatePaymentStatus(transaction, TopayTransactionStatus.ERROR)
            throw PaymentException(e)
        }

    }


    override fun getPaymentMethod(paymentMethodId: Long): PaymentMethod {
        return paymentMethodDAO.select(paymentMethodId)
    }


    override fun getPaymentProvider(paymentProviderId: Long): PaymentProvider {
        return providerDAO.select(paymentProviderId)
    }

    @Throws(PermissionException::class)
    override fun getPayments(clientId: Long, query: String, status: TopayTransactionStatus, paymentMethodId: Long?, paymentProviderId: Long?, projectType: ProjectType, dateFrom: Date, dateTo: Date, orderOrTransactionId: Long, offset: Int, limit: Int): List<TopayTransaction> {
        verifyPlanetaAdminPermission(clientId)
        return topayTransactionDAO.selectTopayTransactions(query, status, paymentMethodId, paymentProviderId, projectType, dateFrom, dateTo, orderOrTransactionId, offset, limit)
    }

    @Throws(PermissionException::class)
    override fun getOrderPayments(clientId: Long, orderId: Long): List<TopayTransaction> {
        verifyPlanetaAdminPermission(clientId)
        return topayTransactionDAO.selectOrderTopayTransactions(orderId)
    }

    @Throws(PermissionException::class)
    private fun verifyPlanetaAdminPermission(clientId: Long) {
        permissionService.checkAdministrativeRole(clientId)
    }

}

package ru.planeta.api

import org.springframework.beans.factory.annotation.Autowired
import ru.planeta.dao.commondb.ShareDAO
import ru.planeta.dao.commondb.OrderDAO
import ru.planeta.dao.commondb.OrderObjectDAO
import ru.planeta.dao.commondb.UserPrivateInfoDAO
import ru.planeta.dao.commondb.CampaignDAO
import ru.planeta.dao.profiledb.ProfileDAO
import ru.planeta.dao.shopdb.ProductDAO


/**
 * Base class for services using data access layer
 */
open class BaseDaoService {

    @Autowired
    open lateinit var profileDAO: ProfileDAO

    @Autowired
    open lateinit var userPrivateInfoDAO: UserPrivateInfoDAO

    @Autowired
    open lateinit var orderDAO: OrderDAO

    @Autowired
    open lateinit var campaignDAO: CampaignDAO

    @Autowired
    open lateinit var shareDAO: ShareDAO

    @Autowired
    open lateinit var productDAO: ProductDAO

    @Autowired
    open lateinit var orderObjectDAO: OrderObjectDAO

}

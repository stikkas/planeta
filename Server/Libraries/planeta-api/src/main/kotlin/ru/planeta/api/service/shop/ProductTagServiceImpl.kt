package ru.planeta.api.service.shop

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import ru.planeta.dao.shopdb.ProductTagDAO
import ru.planeta.model.shop.Category

import java.util.ArrayList
import java.util.Collections

@Service
class ProductTagServiceImpl(private val productTagDAO: ProductTagDAO) : ProductTagService {

    override val productTagsCount: Int
        get() = productTagDAO!!.productTagsCount

    override fun getProductTags(limit: Int, offset: Int): List<Category> {
        return productTagDAO!!.selectAll(limit, offset)
    }

    override fun getTagByMnemonicName(mnemonicName: String): Category {
        return productTagDAO!!.getTagByMnemonicName(mnemonicName)
    }

    override fun getTagByMnemonicNameList(mnemonicNames: List<String>): List<Category> {
        if (mnemonicNames == null || mnemonicNames.isEmpty()) {
            return emptyList()
        }

        val categories = productTagDAO!!.getTagByMnemonicNameList(mnemonicNames)
        val tags = ArrayList<Category>(mnemonicNames.size)
        for (mnemonicName in mnemonicNames) {
            for (category in categories) {
                if (category.mnemonicName == mnemonicName) {
                    tags.add(category)
                }
            }
        }

        return tags
    }

    override fun getProductTagById(categoryId: Long): Category {
        return productTagDAO!!.getProductTagById(categoryId)
    }

    override fun saveProductTag(category: Category) {
        if (category.categoryId == 0L) {
            productTagDAO!!.insertProductTag(category)
        } else {
            productTagDAO!!.updateProductTag(category)
        }
    }
}

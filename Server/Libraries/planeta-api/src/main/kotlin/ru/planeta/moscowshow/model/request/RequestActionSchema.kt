package ru.planeta.moscowshow.model.request

import javax.xml.bind.annotation.XmlAccessType
import javax.xml.bind.annotation.XmlAccessorType
import javax.xml.bind.annotation.XmlRootElement

/**
 *
 * User: Serge Blagodatskih<stikkas17></stikkas17>@gmail.com><br></br>
 * Date: 13.10.16<br></br>
 * Time: 17:59
 */
@XmlRootElement(name = "GetActionSchemaEx")
@XmlAccessorType(XmlAccessType.FIELD)
class RequestActionSchema : Request {

    var actionID: Long = 0
    var sectionID: Long = 0
    var forceFullSchema: Boolean = false

    constructor()

    constructor(actionID: Long, sectionID: Long, forceFullSchema: Boolean) {
        this.actionID = actionID
        this.sectionID = sectionID
        this.forceFullSchema = forceFullSchema
    }
}

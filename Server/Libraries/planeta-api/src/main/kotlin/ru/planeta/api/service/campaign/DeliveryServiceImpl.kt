package ru.planeta.api.service.campaign

import org.apache.commons.collections4.CollectionUtils
import org.apache.commons.collections4.IterableUtils
import org.apache.commons.collections4.Predicate
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import ru.planeta.api.BaseService
import ru.planeta.api.exceptions.MessageCode
import ru.planeta.api.exceptions.NotFoundException
import ru.planeta.api.exceptions.PermissionException
import ru.planeta.api.service.geo.AddressService
import ru.planeta.api.service.geo.GeoService
import ru.planeta.dao.commondb.DeliveryDAO
import ru.planeta.model.common.Address
import ru.planeta.model.common.campaign.ShareEditDetails
import ru.planeta.model.common.delivery.BaseDelivery
import ru.planeta.model.common.delivery.LinkedDelivery
import ru.planeta.model.common.delivery.SubjectType
import ru.planeta.model.profile.location.LocationType
import ru.planeta.model.shop.Category
import ru.planeta.model.shop.Product
import ru.planeta.model.shop.enums.DeliveryType
import java.math.BigDecimal

/**
 * @author Andrew.Arefyev@gmail.com
 * 30.10.13 13:15
 */
@Service
class DeliveryServiceImpl(private var deliveryDAO: DeliveryDAO,
                          private var geoService: GeoService,
                          private var addressService: AddressService)
    : BaseService(), DeliveryService {
    override fun getLinkedDeliveries(subjectId: Long, subjectType: SubjectType): List<LinkedDelivery> {
        val services = deliveryDAO.select(subjectId, subjectType)
        for (service in services) {
            service.location = geoService.getLocation(service.location)
        }
        return services
    }

    override fun getEnabledLinkedDeliveries(subjectId: Long, subjectType: SubjectType): List<LinkedDelivery> {
        val services = deliveryDAO.selectEnabled(subjectId, subjectType)
        for (service in services) {
            service.location = geoService.getLocation(service.location)
        }
        return services
    }

    @Throws(PermissionException::class)
    override fun updateDefaultDeliveryServices(clientId: Long, shareDetails: ShareEditDetails) {

        if (shareDetails.pickupByCustomer) {
            linkDefaultDeliveryToShare(clientId, shareDetails.shareId, BaseDelivery.DEFAULT_CUSTOMER_PICKUP_SERVICE_ID, shareDetails.storedAddress)
        } else {
            disableDeliveryLink(shareDetails.shareId, BaseDelivery.DEFAULT_CUSTOMER_PICKUP_SERVICE_ID, SubjectType.SHARE)
        }
        if (shareDetails.deliverToCustomer) {
            linkDefaultDeliveryToShare(clientId, shareDetails.shareId, BaseDelivery.DEFAULT_DELIVERY_SERVICE_ID, shareDetails.storedAddress)
        } else {
            disableDeliveryLink(shareDetails.shareId, BaseDelivery.DEFAULT_DELIVERY_SERVICE_ID, SubjectType.SHARE)
        }
    }

    override fun filterProductDeliveries(deliveries: List<LinkedDelivery>, products: List<Product>) {
        for (product in products) {
            // products with category NO_RUSSIAN_POST_CATEGORY should have no RUSSIAN_POST delivery
            if (IterableUtils.matchesAny(product.getTags()) { `object` -> Category.NO_RUSSIAN_POST_CATEGORY == `object`.mnemonicName }) {
                CollectionUtils.filter(deliveries) { `object` -> DeliveryType.RUSSIAN_POST !== `object`.serviceType }
            }

            // products with category NO_WORLD_DELIVERY_CATEGORY should have no RUSSIAN_POST delivery to the WORLD region
            if (IterableUtils.matchesAny(product.getTags()) { `object` -> Category.NO_WORLD_DELIVERY_CATEGORY == `object`.mnemonicName }) {
                CollectionUtils.filter(deliveries) { `object` -> !(DeliveryType.RUSSIAN_POST === `object`.serviceType && LocationType.WORLD === `object`.location?.locationType) }
            }
        }
    }


    override fun getBaseDeliveries(searchString: String?): List<BaseDelivery> {
        var serviceId: Long? = null
        if (searchString != null) {
            try {
                // try to get serviceId from searchString
                serviceId = java.lang.Long.parseLong(searchString)
            } catch (e: NumberFormatException) {
                // ignore
            }

        }
        val services = deliveryDAO.selectBaseDeliveries(serviceId, searchString)
        for (service in services) {
            service.location = geoService.getLocationWithParents(service.location)
        }
        return services
    }

    @Throws(PermissionException::class)
    override fun saveBaseDelivery(clientId: Long, delivery: BaseDelivery) {
        permissionService.checkAdministrativeRole(clientId)
        if (delivery.serviceType === DeliveryType.CUSTOMER_PICKUP && delivery.serviceId != BaseDelivery.DEFAULT_CUSTOMER_PICKUP_SERVICE_ID) {
            addressService.saveAddress(delivery.address)
        }
        if (delivery.serviceId != 0L) {
            deliveryDAO.updateBaseDelivery(delivery)
        } else {
            deliveryDAO.insert(delivery)
        }
    }

    @Throws(PermissionException::class)
    override fun removeBaseDelivery(clientId: Long, deliveryTemplateId: Long) {
        permissionService.checkAdministrativeRole(clientId)
        deliveryDAO.removeBaseDelivery(deliveryTemplateId)
    }

    @Throws(PermissionException::class)
    override fun disableLinkDelivery(clientId: Long, shareId: Long, serviceId: Long, subjectType: SubjectType) {
        permissionService.checkAdministrativeRole(clientId)
        disableDeliveryLink(shareId, serviceId, subjectType)
    }

    private fun disableDeliveryLink(subjectId: Long, serviceId: Long, subjectType: SubjectType) {
        val delivery = deliveryDAO.select(subjectId, serviceId, subjectType)
        if (delivery != null) {
            delivery.isEnabled = false
            deliveryDAO.updateLinkedDelivery(delivery)
        }
    }

    @Throws(PermissionException::class)
    override fun getBaseDelivery(clientId: Long, deliveryId: Long): BaseDelivery {
        val service = deliveryDAO.selectBaseDelivery(deliveryId)
        service.location = geoService.getLocationWithParents(service.location)
        return service
    }

    @Throws(PermissionException::class)
    override fun saveLinkedDelivery(clientId: Long, delivery: LinkedDelivery) {
        if (delivery.serviceId < 0) {
            //todo add check for share ownership
            if (delivery.serviceType === DeliveryType.CUSTOMER_PICKUP) {
                addressService.saveAddress(delivery.address)
            }
        } else if (!permissionService.hasAdministrativeRole(clientId)) {
            throw PermissionException(MessageCode.PERMISSION_ADMIN)
        } else {
            if (delivery.address == null) {
                delivery.address = Address()
            }
            delivery?.address?.setAddressId(null)
        }
        if (existsDeliveryService(delivery.getShareId(), delivery.serviceId, delivery.subjectType)) {
            deliveryDAO.updateLinkedDelivery(delivery)
        } else {
            deliveryDAO.addDeliveryToShare(delivery)
        }

    }

    @Throws(NotFoundException::class)
    override fun getLinkedDelivery(subjectId: Long, deliveryId: Long, subjectType: SubjectType): LinkedDelivery {
        val service = deliveryDAO.select(subjectId, deliveryId, subjectType)
                ?: throw NotFoundException(LinkedDelivery::class.java, deliveryId)
        //todo change to return null
        service.location = geoService.getLocationWithParents(service.location)
        return service
    }

    override fun getUnlinkedBaseDeliveries(subjectId: Long, subjectType: SubjectType): List<BaseDelivery> {
        val deliveryServices = deliveryDAO.selectUnlinkedBaseDeliveries(subjectId, subjectType)
        for (service in deliveryServices) {
            service.location = geoService.getLocationWithParents(service.location)
        }
        return deliveryServices
    }

    @Throws(PermissionException::class)
    override fun linkDefaultDeliveryToShare(clientId: Long, shareId: Long, deliveryServiceId: Long, address: Address) {
        var delivery: LinkedDelivery? = null
        try {
            delivery = getLinkedDelivery(shareId, deliveryServiceId, SubjectType.SHARE)
        } catch (ignored: NotFoundException) {
        }

        if (delivery == null || !delivery.isEnabled) {
            if (delivery == null) {
                val baseDeliveryService = deliveryDAO.selectBaseDelivery(deliveryServiceId)
                delivery = LinkedDelivery(baseDeliveryService)
                delivery.setShareId(shareId)
                delivery.price = BigDecimal.ZERO
                delivery.publicNote = ""
            }
            if (delivery.serviceType === DeliveryType.CUSTOMER_PICKUP) {
                address.setAddressId(delivery.address?.getAddressId())
                delivery.address = address
            }

            delivery.isEnabled = true
            saveLinkedDelivery(clientId, delivery)
        } else if (delivery.serviceType === DeliveryType.CUSTOMER_PICKUP) {
            address.setAddressId(delivery.address?.getAddressId())
            addressService.saveAddress(address)
        }
    }


    private fun existsDeliveryService(shareId: Long, serviceId: Long, subjectType: SubjectType): Boolean {
        return deliveryDAO.select(shareId, serviceId, subjectType) != null
    }

}


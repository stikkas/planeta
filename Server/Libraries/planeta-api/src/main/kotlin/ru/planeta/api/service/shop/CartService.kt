package ru.planeta.api.service.shop

import ru.planeta.model.shop.ShoppingCartShortItem

/**
 * Created by IntelliJ IDEA.
 * User: Andrew Arefyev
 * Date: 05.12.12
 * Time: 10:36
 */
interface CartService {

    /**
     * clear shopping cart.
     *
     * @param clientId cart owner id
     * @return count rows deleted
     */
    fun clear(clientId: Long): Int

    /**
     * get all cart items
     *
     * @param buyerId buyer profile Id
     * @return cart items
     */
    fun getCart(buyerId: Long): List<ShoppingCartShortItem>

    /**
     * add or update cart item
     *
     * @param profileId buyer profile id
     * @param productId product id
     * @param quantity  new quantity
     */
    fun put(profileId: Long, productId: Long?, quantity: Int)

    /**
     * removes one position from cart
     *
     * @param profileId buyer id
     * @param productId product id
     */
    fun remove(profileId: Long?, productId: Long)

}

package ru.planeta.api.search

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import ru.planeta.model.common.campaign.CampaignTag

@JsonIgnoreProperties(ignoreUnknown = true)
class SearchShares {

    var query: String? = null
    var sortOrderList: List<SortOrder> = mutableListOf()
    var campaignTags: List<CampaignTag>? = null
    var priceFrom: Int = 0
    var priceTo: Int = 0
    var offset = 0
    var limit = 12
    var campaignTagsIds: List<Int>? = null
    var isWelcomeSearch: Boolean = false

    constructor() {

    }

    constructor(sortOrderList: List<SortOrder>, query: String, campaignTagsIds: List<Int>, offset: Int, limit: Int) {
        this.sortOrderList = sortOrderList
        this.query = query
        this.campaignTagsIds = campaignTagsIds
        this.offset = offset
        this.limit = limit
    }

    constructor(sortOrderList: List<SortOrder>, query: String, offset: Int, limit: Int) {
        this.sortOrderList = sortOrderList
        this.query = query
        this.offset = offset
        this.limit = limit
    }

    constructor(sortOrderList: List<SortOrder>, priceFrom: Int, priceTo: Int, isWelcomeSearch: Boolean, limit: Int) {
        this.sortOrderList = sortOrderList
        this.priceFrom = priceFrom
        this.priceTo = priceTo
        this.isWelcomeSearch = isWelcomeSearch
        this.limit = limit
    }
}

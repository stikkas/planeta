package ru.planeta.api.model.json

import ru.planeta.model.profile.Profile
import ru.planeta.model.profile.User

/**
 *
 * Class UserInfo
 * @author a.tropnikov
 */
class UserInfo : ProfileInfo {
    var user: User? = null

    constructor() : super() {
        user = User()
    }

    constructor(user: User, profile: Profile) : super(profile) {
        this.user = user
    }

}

package ru.planeta.api.service.campaign

import org.apache.commons.lang3.StringUtils
import org.apache.commons.lang3.StringEscapeUtils
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import ru.planeta.dao.commondb.ModerationMessageDAO
import ru.planeta.model.common.ModerationMessage
import ru.planeta.model.common.campaign.enums.CampaignStatus
import java.util.EnumSet

/**
 * User: a.savanovich
 * Date: 26.11.13
 * Time: 18:01
 */
@Service
class ModerationMessageServiceImpl(private var moderationMessageDAO: ModerationMessageDAO) : ModerationMessageService {

    override fun getLastCampaignModerationMessage(campaignId: Long): ModerationMessage {
        return moderationMessageDAO.selectLastCampaignModerationMessage(campaignId)
    }

    override fun getLastCampaignModerationMessageList(profileId: Long, campaignStatuses: EnumSet<CampaignStatus>): List<Map<*, *>> {
        return moderationMessageDAO.selectLastCampaignModerationMessageList(profileId, campaignStatuses)
    }


    override fun getAllCampaignModerationMessages(campaignId: Long, offset: Int, limit: Int): List<ModerationMessage> {
        return moderationMessageDAO.selectAllCampaignModerationMessages(campaignId, offset, limit)
    }

    override fun addModerationMessage(moderationMessage: ModerationMessage) {
        val message = StringEscapeUtils.escapeHtml4(StringUtils.trimToEmpty(moderationMessage.message))
        moderationMessage.message = message
        moderationMessageDAO.insertModerationMessage(moderationMessage)
    }
}

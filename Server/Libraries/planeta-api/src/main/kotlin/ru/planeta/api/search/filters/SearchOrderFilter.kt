package ru.planeta.api.search.filters

import ru.planeta.model.enums.OrderObjectType
import ru.planeta.model.shop.enums.DeliveryStatus
import ru.planeta.model.shop.enums.PaymentStatus

import java.util.Date

/*
 * Created by kostiagn on 28.04.2015.
 */
class SearchOrderFilter {
    var clientId: Long = 0
    var campaignId: Long = 0
    var merchantId: Long = 0
    var buyerId: Long = 0
    var shareId: Long = 0
    var productId: Long = 0
    var orderObjectType: OrderObjectType? = null
    var dateFrom: Date? = null
    var dateTo: Date? = null
    var paymentStatus: PaymentStatus? = null
    var deliveryStatus: DeliveryStatus? = null
    var searchString: String? = null
    var cityAndCountrySearchString: String? = null
    var offset: Int = 0
    var limit: Int = 0

    val orderObjectTypeId: Int
        get() = if (orderObjectType == null) 0 else orderObjectType!!.code

    class Builder {
        private val obj: SearchOrderFilter

        init {
            this.obj = SearchOrderFilter()
        }

        fun build(): SearchOrderFilter {
            return this.obj
        }

        fun clientId(clientId: Long): Builder {
            obj.clientId = clientId
            return this
        }

        fun campaignId(campaignId: Long): Builder {
            obj.campaignId = campaignId
            return this
        }

        fun merchantId(merchantId: Long): Builder {
            obj.merchantId = merchantId
            return this
        }

        fun buyerId(buyerId: Long): Builder {
            obj.buyerId = buyerId
            return this
        }

        fun shareId(shareId: Long): Builder {
            obj.shareId = shareId
            return this
        }

        fun productId(productId: Long): Builder {
            obj.productId = productId
            return this
        }

        fun dateFrom(dateFrom: Date): Builder {
            obj.dateFrom = dateFrom
            return this
        }

        fun dateTo(dateTo: Date): Builder {
            obj.dateTo = dateTo
            return this
        }

        fun paymentStatus(paymentStatus: PaymentStatus): Builder {
            obj.paymentStatus = paymentStatus
            return this
        }

        fun deliveryStatus(deliveryStatus: DeliveryStatus): Builder {
            obj.deliveryStatus = deliveryStatus
            return this
        }

        fun searchString(searchString: String): Builder {
            obj.searchString = searchString
            return this
        }

        fun cityAndCountrySearchString(cityAndCountrySearchString: String): Builder {
            obj.cityAndCountrySearchString = cityAndCountrySearchString
            return this
        }

        fun offset(offset: Int): Builder {
            obj.offset = offset
            return this
        }

        fun limit(limit: Int): Builder {
            obj.limit = limit
            return this
        }
    }

    companion object {

        val builder: Builder
            get() = Builder()
    }
}

package ru.planeta.api.service.promo

import ru.planeta.model.promo.TechnobattleRegistration

/**
 * Created with IntelliJ IDEA.
 * User: michail
 * Date: 18.11.2016
 * Time: 12:51
 */
interface TechnobattleRegistrationService {
    fun insertRegistrationRequest(registration: TechnobattleRegistration): Int
}

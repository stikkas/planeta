package ru.planeta.api.exceptions

import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ResponseStatus

@ResponseStatus(value = HttpStatus.OK, reason = "Access denied")
class VideoPermissionException(var videoName: String?, var videoAuthorName: String?, var videoOwnerName: String?) : PermissionException()

package ru.planeta.api.service.biblio

import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import ru.planeta.dao.bibliodb.BookDAO
import ru.planeta.dao.bibliodb.BookTagDAO
import ru.planeta.model.bibliodb.Book
import ru.planeta.model.bibliodb.BookFilter
import ru.planeta.model.bibliodb.BookTag
import java.util.*

/*
 * Created by Alexey on 12.04.2016.
 */

@Transactional(readOnly = true)
@Service
class BookServiceImpl(private val bookDAO: BookDAO, private val bookTagDAO: BookTagDAO) : BookService {

    override val allBooks: List<Book>
        get() = bookDAO.selectBooks()

    override val indexBooks: List<Book>
        get() = bookDAO.selectBooksForIndexPage()

    override val countActiveBooks: Long
        get() = bookDAO.countActiveBooks()


    override fun getBookTags(limit: Long, offset: Long): List<BookTag> {
        return bookTagDAO.getBookTags(limit, offset)
    }

    override fun isBooksContainTag(bookIds: List<Long>, bookTagName: String): Boolean {
        return bookTagDAO.existsTagInBooks(bookIds, bookTagName)
    }

    @Transactional(readOnly = false)
    override fun saveBook(book: Book) {
        if (book.bookId == 0L) {
            book.timeAdded = Date()
            bookDAO.insertBook(book)
        } else {
            bookDAO.updateBook(book)
        }
        bookTagDAO.setTagRelation(book.bookId, book.tags)
    }

    @Transactional(readOnly = false)
    override fun deleteBook(bookId: Long?) {
        if (bookId != null) {
            bookDAO.deleteBook(bookId)
        }
    }

    override fun getActiveBookById(bookId: Long): Book {
        return bookDAO.selectActiveBookById(bookId)
    }

    override fun getBookById(bookId: Long): Book {
        return bookDAO.selectBookById(bookId)
    }

    override fun getBookTagsByBookId(bookId: Long): List<BookTag> {
        return bookTagDAO.getBookTagsByBookId(bookId)
    }

    override fun searchBooks(filter: BookFilter): List<Book> {
        return bookDAO.searchBooks(filter)
    }

    override fun getBookForChangePrice(date: Date, offset: Long, limit: Long): List<Book> {
        return bookDAO.selectBookForChangePrice(date, offset, limit)
    }

    override fun selectCountSearchBooks(filter: BookFilter): Long {
        return bookDAO.selectCountSearchBooks(filter)
    }

    override fun hasBookRequests(name: String, email: String): Boolean {
        return bookDAO.selectHasRequests(name, email)
    }
}

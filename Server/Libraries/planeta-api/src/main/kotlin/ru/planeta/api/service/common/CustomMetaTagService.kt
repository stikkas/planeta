package ru.planeta.api.service.common


import ru.planeta.model.enums.CustomMetaTagType
import ru.planeta.model.commondb.CustomMetaTag

interface CustomMetaTagService {
    fun insertOrUpdateCustomMetaTag(customMetaTag: CustomMetaTag)

    fun deleteCustomMetaTag(id: Long?)

    fun selectCustomMetaTagList(customMetaTagType: CustomMetaTagType, offset: Int, limit: Int): List<CustomMetaTag>

    fun selectCustomMetaTag(customMetaTagType: CustomMetaTagType, customMetaTagId: Long?): CustomMetaTag?

    fun selectCustomMetaTag(customMetaTagType: CustomMetaTagType, tagPath: String): CustomMetaTag?

}

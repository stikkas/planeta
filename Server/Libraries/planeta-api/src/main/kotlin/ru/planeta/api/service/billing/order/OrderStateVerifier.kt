package ru.planeta.api.service.billing.order

import org.apache.commons.collections4.CollectionUtils
import org.apache.commons.lang3.ObjectUtils
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.MessageSource
import org.springframework.stereotype.Component
import ru.planeta.api.exceptions.NotFoundException
import ru.planeta.api.exceptions.OrderException
import ru.planeta.api.exceptions.PermissionException
import ru.planeta.model.common.Order
import ru.planeta.model.common.OrderObject
import ru.planeta.model.common.TransactionalObject
import ru.planeta.model.enums.OrderObjectType
import ru.planeta.model.shop.enums.PaymentStatus

import java.util.*

import ru.planeta.api.exceptions.MessageCode.ORDER_ILLEGAL_STATE

/**
 *
 * Created by eshevchenko on 03.02.14.
 */
@Component
class OrderStateVerifier @Autowired
constructor(private val messageSource: MessageSource) {

    object Assert {

        @Throws(OrderException::class)
        internal fun isTrue(expression: Boolean, message: String) {
            if (!expression) {
                throw OrderException(ORDER_ILLEGAL_STATE, message)
            }
        }

        @Throws(OrderException::class)
        internal fun isEquals(obj1: Any?, obj2: Any?, message: String) {
            isTrue(ObjectUtils.equals(obj1, obj2), message)
        }

        @Throws(OrderException::class)
        internal fun isNotEquals(obj1: Any, obj2: Any, message: String) {
            isTrue(!ObjectUtils.equals(obj1, obj2), message)
        }

        @Throws(OrderException::class)
        fun isNotEmpty(collection: Collection<*>, message: String) {
            isTrue(CollectionUtils.isNotEmpty(collection), message)
        }
    }


    private fun getMessage(order: Order, description: String, vararg args: Any): String {
        val code = ORDER_ILLEGAL_STATE.errorPropertyName
        val stateDescription = String.format(description, *args)

        return messageSource.getMessage(code, arrayOf(stateDescription, order.orderId), code, Locale.getDefault())
    }

    @Throws(OrderException::class)
    private fun verifyTransactionalObject(order: Order, transactionalObject: TransactionalObject) {
        if (NON_PAYED_STATUSES.contains(order.paymentStatus)) {
            Assert.isEquals(transactionalObject.creditTransactionId, 0L, getMessage(order, "Has credit transaction for non payed order"))
            Assert.isEquals(transactionalObject.debitTransactionId, 0L, getMessage(order, "Has debit transaction for non payed order"))
        } else {
            Assert.isNotEquals(transactionalObject.creditTransactionId, 0L, getMessage(order, "Hasn't credit transaction for non pending order"))
            Assert.isNotEquals(transactionalObject.debitTransactionId, 0L, getMessage(order, "Hasn't debit transaction for non pending order"))
        }

        if (order.paymentStatus === PaymentStatus.CANCELLED) {
            Assert.isNotEquals(transactionalObject.cancelCreditTransactionId, 0L, getMessage(order, "Hasn't cancel credit transaction for cancelled order"))
            Assert.isNotEquals(transactionalObject.cancelDebitTransactionId, 0L, getMessage(order, "Hasn't cancel debit transaction for cancelled order"))
        } else {
            Assert.isEquals(transactionalObject.cancelCreditTransactionId, 0L, getMessage(order, "Has cancel credit transaction for non cancelled order"))
            Assert.isEquals(transactionalObject.cancelDebitTransactionId, 0L, getMessage(order, "Has cancel credit transaction for non cancelled order"))
        }
    }

    @Throws(NotFoundException::class, OrderException::class, PermissionException::class)
    private fun verifyOrder(expectedType: OrderObjectType?, order: Order, orderObjects: Collection<OrderObject>?) {
        Assert.isTrue(order.buyerId > 0, getMessage(order, "Buyer is anonymous user"))
        Assert.isEquals(expectedType, order.orderType, getMessage(order, "Order is not %s type", expectedType as Any))
        verifyTransactionalObject(order, order)

        if (OrderObjectType.DELIVERY !== order.orderType) {     // Delivery order has no order_objects
            Assert.isNotEmpty(orderObjects
                    ?: emptyList<OrderObject>(), getMessage(order, "Order has no any nested object"))
            orderObjects?.forEach {
                if (expectedType === OrderObjectType.BIBLIO) {
                    // TODO: Переделать на что-то более нормальное
                    when (it.orderObjectType) {
                        OrderObjectType.LIBRARY, OrderObjectType.BOOK -> {
                        }
                        else -> Assert.isTrue(false, getMessage(order, "Biblio order object is wrong %s type", it.orderObjectType as Any))
                    }
                } else {
                    Assert.isEquals(expectedType, it.orderObjectType, getMessage(order, "Order object is not %s type", expectedType as Any))
                }
                Assert.isEquals(order.orderId, it.orderId, getMessage(order, "Invalid order object reference"))
            }
        }
    }

    @Throws(NotFoundException::class, OrderException::class, PermissionException::class)
    fun verify(order: Order, orderObjects: Collection<OrderObject>?) {
        when (order.orderType) {
            OrderObjectType.INVESTING, OrderObjectType.INVESTING_WITHOUT_MODERATION, OrderObjectType.INVESTING_WITH_ALL_ALLOWED_INVOICE -> {
            }
            else -> verifyOrder(order.orderType, order, orderObjects)
        }
    }

    companion object {
        private val NON_PAYED_STATUSES = EnumSet.of(PaymentStatus.PENDING, PaymentStatus.RESERVED, PaymentStatus.OVERDUE, PaymentStatus.FAILED)
    }
}

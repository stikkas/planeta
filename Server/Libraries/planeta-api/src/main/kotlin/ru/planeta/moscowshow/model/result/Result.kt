package ru.planeta.moscowshow.model.result

import javax.xml.bind.annotation.XmlAccessType
import javax.xml.bind.annotation.XmlAccessorType
import javax.xml.bind.annotation.XmlRootElement

/**
 * Базовый класс результатов выполнения операций
 * Содержит два важных поля: ErrorCode (0, если нет ошибки)
 * ErrorMessage (описание ошибки в текстовом виде)
 * User: Serge Blagodatskih<stikkas17></stikkas17>@gmail.com><br></br>
 * Date: 14.10.16<br></br>
 * Time: 13:04
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
open class Result {
    var ErrorCode: Int = 0
    var ErrorMessage: String? = null
}


package ru.planeta.api.search

import org.apache.commons.collections4.CollectionUtils
import org.apache.commons.lang3.ArrayUtils
import org.apache.commons.lang3.RandomUtils
import org.apache.commons.lang3.StringUtils
import org.apache.commons.lang3.math.NumberUtils
import org.apache.commons.lang3.time.DateUtils
import org.apache.commons.lang3.time.StopWatch
import org.apache.log4j.Logger
import org.sphx.api.SphinxClient
import org.sphx.api.SphinxException
import org.sphx.api.SphinxMatch
import org.sphx.api.SphinxResult
import org.springframework.stereotype.Service
import ru.planeta.api.Utils
import ru.planeta.api.exceptions.NotFoundException
import ru.planeta.api.exceptions.PermissionException
import ru.planeta.api.model.json.UserCallbackInfo
import ru.planeta.api.news.ProfileNewsDTO
import ru.planeta.api.news.ProfileNewsService
import ru.planeta.api.search.SearchIndexField.*
import ru.planeta.api.search.filters.SearchOrderFilter
import ru.planeta.api.service.biblio.LibraryService
import ru.planeta.api.service.billing.order.OrderInfoService
import ru.planeta.api.service.campaign.CampaignService
import ru.planeta.api.service.common.InvestingOrderInfoService
import ru.planeta.api.service.common.SeminarService
import ru.planeta.api.service.common.UserCallbackService
import ru.planeta.api.service.configurations.ConfigurationService
import ru.planeta.api.service.shop.ProductUsersService
import ru.planeta.commons.text.TranslitConverter
import ru.planeta.dao.commondb.OrderDAO
import ru.planeta.dao.commondb.CampaignDAO
import ru.planeta.dao.commondb.ShareSearchDAO
import ru.planeta.dao.profiledb.BroadcastDAO
import ru.planeta.dao.profiledb.ProfileDAO
import ru.planeta.dao.profiledb.ProfileForAdminsDAO
import ru.planeta.dao.profiledb.ProfileSubscriptionDAO
import ru.planeta.model.bibliodb.Library
import ru.planeta.model.bibliodb.LibraryFilter
import ru.planeta.model.common.InvestingOrderInfo
import ru.planeta.model.common.OrderInfo
import ru.planeta.model.common.campaign.Campaign
import ru.planeta.model.common.campaign.CampaignTag
import ru.planeta.model.common.campaign.ShareForSearch
import ru.planeta.model.common.campaign.enums.CampaignStatus
import ru.planeta.model.common.school.SeminarWithTagNameAndCityName
import ru.planeta.model.enums.*
import ru.planeta.model.profile.ProfileForAdminsWithEmail
import ru.planeta.model.profile.broadcast.Broadcast
import ru.planeta.model.profile.broadcast.enums.BroadcastCategory
import ru.planeta.model.shop.ProductInfo
import java.math.BigDecimal
import java.util.*
import java.util.concurrent.atomic.AtomicBoolean

/**
 * Service for full-text search implementation.
 *
 * @author s.fionov
 */
@Service
class SearchServiceImpl(
        private var factory: SphinxClientFactory,
        private var broadcastDAO: BroadcastDAO,
        private var campaignDAO: CampaignDAO,
        private var profileDAO: ProfileDAO,
        private var subscriptionDAO: ProfileSubscriptionDAO,
        private var configurationService: ConfigurationService,
        private var campaignService: CampaignService,
        private var productUsersService: ProductUsersService,
        private var orderDAO: OrderDAO,
        private var orderInfoService: OrderInfoService,
        private var seminarService: SeminarService,
        private var newsService: ProfileNewsService,
        private var shareSearchDAO: ShareSearchDAO,
        private var userCallbackService: UserCallbackService,
        private var investingOrderInfoService: InvestingOrderInfoService,
        private var libraryService: LibraryService,
        private var profileForAdminsDAO: ProfileForAdminsDAO) : SearchService {

    @Throws(SphinxException::class)
    override fun adminSearchForProfiles(query: String, type: ProfileType,
                                        offset: Int, limit: Int): SearchResult<ProfileForAdminsWithEmail> {
        var query = query

        query = StringUtils.replace(query, "@", "_")
        val sphinxClient = getUsersSphinxClient(offset, limit)

        if (type != null) {
            sphinxClient.SetFilter(PROFILE_TYPE_ID.label, type.code, false)
        }

        val result = SearchResult<ProfileForAdminsWithEmail>()
        val sphinxResult = trySearchWithTranslation(sphinxClient, query, SearchIndex.PROFILES, result)
        val profiles = profileForAdminsDAO.selectByIds(searchResultToListIds(sphinxResult))
        result.searchResultRecords = profiles

        return result
    }

    @Throws(SphinxException::class)
    override fun searchForUsers(clientId: Long, query: String,
                                offset: Int, limit: Int): SearchResult<ProfileSearchResult> {

        val searchResult: SearchResult<ProfileSearchResult>
        if (Utils.empty(query)) {
            val profiles = profileDAO.searchProfiles(clientId, offset, limit)
            val results = ArrayList<ProfileSearchResult>(profiles.size)
            for (p in profiles) {
                results.add(ProfileSearchResult(p))
            }
            searchResult = SearchResult(0, profileDAO.selectProfilesCount(), results)
        } else {
            val sphinxClient = getUsersSphinxClient(offset, limit)
            searchResult = SearchResult()
            val sphinxResult = trySearchWithTranslation(sphinxClient, query, SearchIndex.USERS, searchResult)
            val searchResultRecords = populateProfileSearchResults(sphinxResult, clientId)
            sortByMatches(searchResultRecords, sphinxResult.matches)
            searchResult.searchResultRecords = searchResultRecords
        }
        if (!Utils.empty(searchResult.searchResultRecords)) {
            val ids = ArrayList<Long>()
            searchResult.searchResultRecords?.forEach {
                ids.add(it.profile.profileId)
            }
            val subscriptions = HashSet(subscriptionDAO.selectSubscriptions(clientId, ids))
            searchResult.searchResultRecords?.forEach {
                it.isSubscribed = subscriptions.contains(it.profile.profileId)
            }
        }
        return searchResult
    }

    @Throws(SphinxException::class)
    internal fun getUsersSphinxClient(offset: Int, limit: Int): SphinxClient {
        val sphinxClient = factory.sphinxClient
        sphinxClient.SetLimits(offset, limit, MAX_MATCHES)
        sphinxClient.SetMaxQueryTime(MAX_QUERY_TIME)
        sphinxClient.SetSortMode(SphinxClient.SPH_SORT_EXTENDED, "@id DESC")
        return sphinxClient
    }

    @Throws(SphinxException::class)
    override fun searchForNewsPosts(clientId: Long, query: String, profileId: Long?, filterType: PostFilterType?,
                                    offset: Int, limit: Int): SearchResult<ProfileNewsDTO> {

        val sphinxClient = getUsersSphinxClient(offset, limit)

        if (profileId != null) {
            sphinxClient.SetFilter(PROFILE_ID.label, profileId, false)
        }
        if (filterType === PostFilterType.CAMPAIGNS) {
            sphinxClient.SetFilterRange(CAMPAIGN_ID.label, 1, java.lang.Long.MAX_VALUE, false)
        } else if (filterType === PostFilterType.USERS) {
            sphinxClient.SetFilter(CAMPAIGN_ID.label, 0L, false)
        }

        val searchResult = SearchResult<ProfileNewsDTO>()
        val sphinxResult = trySearchWithTranslation(sphinxClient, query, SearchIndex.POSTS, searchResult)

        val postIds = searchResultToListIds(sphinxResult)
        searchResult.searchResultRecords = newsService.getProfileNewsPost(postIds)
        return searchResult
    }

    @Throws(SphinxException::class)
    override fun searchForBroadcasts(clientId: Long, sortBy: Int, query: String, profileId: Int?, filterField: String?,
                                     offset: Int, limit: Int, broadcastCategory: BroadcastCategory): SearchResult<Broadcast> {

        val searchResult = SearchResult<Broadcast>()
        val sphinxClient = factory.sphinxClient
        sphinxClient.SetLimits(offset, limit, MAX_MATCHES)
        sphinxClient.SetMaxQueryTime(MAX_QUERY_TIME)

        val sortOrder = SortOrder.getByCode(sortBy)
        when (sortOrder) {
            SortOrder.SORT_BY_COUNT -> sphinxClient.SetSortMode(SphinxClient.SPH_SORT_EXTENDED, BROADCAST_VIEWS_COUNT.label + " DESC")

            SortOrder.SORT_BY_DATE -> sphinxClient.SetSortMode(SphinxClient.SPH_SORT_EXTENDED, BROADCAST_TIME_BEGIN.label + " DESC")
            else -> sphinxClient.SetSortMode(SphinxClient.SPH_SORT_EXTENDED, BROADCAST_TIME_BEGIN.label + " DESC")
        }

        fillWeightMaps(sphinxClient, filterField ?: "")

        if (broadcastCategory != null) {
            when (broadcastCategory) {
                BroadcastCategory.CONCERTS -> sphinxClient.SetFilter(IS_CONCERTS.label, 1, false)
                BroadcastCategory.NASHE_360 -> sphinxClient.SetFilter(IS_NASHE_RADIO.label, 1, false)
                BroadcastCategory.ALIVE -> sphinxClient.SetFilter(IS_ALIVE.label, 1, false)
                BroadcastCategory.SCHOOL -> sphinxClient.SetFilter(IS_SCHOOL.label, 1, false)
                else -> throw RuntimeException("Sphinx filter cant set for '$broadcastCategory' broadcast category")
            }
        }

        val sphinxResult = trySearchWithTranslation(sphinxClient, query, SearchIndex.BROADCASTS, searchResult)

        val searchResultRecords = broadcastDAO.selectByIdList(searchResultToListIds(sphinxResult))
        searchResult.searchResultRecords = searchResultRecords
        return searchResult
    }

    @Throws(SphinxException::class)
    override fun searchForBroadcasts(query: String, dateFrom: Date, dateTo: Date, offset: Int, limit: Int): SearchResult<Broadcast> {
        val searchResult = SearchResult<Broadcast>()
        val sphinxClient = factory.sphinxClient
        sphinxClient.SetLimits(offset, limit, MAX_MATCHES)
        sphinxClient.SetMaxQueryTime(MAX_QUERY_TIME)

        sphinxClient.SetSortMode(SphinxClient.SPH_SORT_EXTENDED, BROADCAST_TIME_BEGIN.label + " DESC")
        setFilterDateRange(sphinxClient, BROADCAST_TIME_BEGIN, BROADCAST_TIME_END, dateFrom, dateTo)

        val sphinxResult = trySearchWithTranslation(sphinxClient, query, SearchIndex.BROADCASTS, searchResult)

        val list = broadcastDAO.selectByIdList(searchResultToListIds(sphinxResult))

        searchResult.searchResultRecords = list
        return searchResult
    }

    @Throws(SphinxException::class)
    override fun searchForSeminars(query: String, dateFrom: Date, dateTo: Date, offset: Int, limit: Int): SearchResult<SeminarWithTagNameAndCityName> {
        var query = query
        val searchResult = SearchResult<SeminarWithTagNameAndCityName>()
        val sphinxClient = factory.sphinxClient
        sphinxClient.SetLimits(offset, limit, MAX_MATCHES)
        sphinxClient.SetMaxQueryTime(MAX_QUERY_TIME)

        sphinxClient.SetSortMode(SphinxClient.SPH_SORT_EXTENDED, SEMINAR_TIME_START.label + " DESC")

        val seminarId = NumberUtils.toLong(query)
        if (seminarId > 0) {
            sphinxClient.SetFilter(SEMINAR_ID_FOR_SEARCH.label, seminarId, false)
            query = ""
        }
        setFilterDateRange(sphinxClient, SEMINAR_TIME_START, dateFrom, dateTo)

        val sphinxResult = trySearchWithTranslation(sphinxClient, query, SearchIndex.SEMINARS, searchResult)

        val list = seminarService.selectSeminarWithTagNameList(searchResultToListIds(sphinxResult))

        searchResult.searchResultRecords = list
        return searchResult
    }

    @Throws(SphinxException::class)
    override fun searchForLibraries(filter: LibraryFilter): SearchResult<Library> {
        val searchResult = SearchResult<Library>()
        val sphinxClient = factory.sphinxClient
        sphinxClient.SetLimits(filter.offset, filter.limit, MAX_MATCHES)
        sphinxClient.SetMaxQueryTime(MAX_QUERY_TIME)

        val libraryId = NumberUtils.toLong(filter.searchStr)
        if (libraryId > 0) {
            sphinxClient.SetFilter(LIBRARY_ID_FOR_SEARCH.label, libraryId, false)
            filter.searchStr = ""
        } else {
            filter.statuses?.let {
                if (!it.isNotEmpty()) {
                    val stats = IntArray(it.size)
                    var i = 0
                    it.forEach {
                        stats[i++] = it.code
                    }
                    sphinxClient.SetFilter(LIBRARY_STATUS.label, stats, false)
                }
            }
        }
        if (filter.libraryType != null) {
            sphinxClient.SetFilter(LIBRARY_TYPE.label, filter?.libraryType?.code ?: 0, false)
        }

        val dateFrom = filter.dateFrom
        val dateTo = filter.dateTo
        if (dateFrom != null && dateTo != null) {
            sphinxClient.SetFilterRange(LIBRARY_TIME_ADDED.label,
                    getTimeWithoutTimeZone(dateFrom),
                    getTimeWithoutTimeZone(dateTo),
                    false)
        }

        val sphinxResult = trySearchWithTranslation(sphinxClient, filter.searchStr, SearchIndex.BIBLIO_LIBRARIES, searchResult)

        val list = libraryService.selectLibrariesList(searchResultToListIds(sphinxResult))

        searchResult.searchResultRecords = list
        return searchResult
    }

    @Throws(SphinxException::class)
    override fun searchForSeminarsByTagId(tagId: Long?, dateFrom: Date, offset: Int, limit: Int): SearchResult<SeminarWithTagNameAndCityName> {
        val searchResult = SearchResult<SeminarWithTagNameAndCityName>()
        val sphinxClient = factory.sphinxClient
        sphinxClient.SetLimits(offset, limit, MAX_MATCHES)
        sphinxClient.SetMaxQueryTime(MAX_QUERY_TIME)

        sphinxClient.SetSortMode(SphinxClient.SPH_SORT_EXTENDED, SEMINAR_TIME_START.label + " ASC")

        if (tagId != null) {
            if (tagId > 0) {
                sphinxClient.SetFilter(TAG_ID.label, tagId, false)
            } else {
                sphinxClient.SetFilter(TAG_ID.label, tagId, true)
            }
        }
        setFilterDateRange(sphinxClient, SEMINAR_TIME_START, dateFrom, null)

        val sphinxResult = trySearchWithTranslation(sphinxClient, "", SearchIndex.SEMINARS, searchResult)

        val list = seminarService.selectSeminarWithTagNameList(searchResultToListIds(sphinxResult))

        searchResult.searchResultRecords = list
        return searchResult
    }

    @Throws(SphinxException::class, NotFoundException::class, PermissionException::class)
    override fun searchForUserCallback(clientId: Long, query: String, processed: Boolean?, dateFrom: Date, dateTo: Date, offset: Int, limit: Int): SearchResult<UserCallbackInfo> {
        var searchResult = SearchResult<UserCallbackInfo>()
        val results: List<UserCallbackInfo>
        if (Utils.empty(query)) {
            results = userCallbackService.getAllCallbacks(clientId, null!!, processed, dateFrom, dateTo, offset, limit)
            searchResult = SearchResult(0, 100, results)
        } else {
            val sphinxClient = factory.sphinxClient
            sphinxClient.SetLimits(offset, limit, MAX_MATCHES)
            sphinxClient.SetMaxQueryTime(5000)

            sphinxClient.SetSortMode(SphinxClient.SPH_SORT_EXTENDED, USER_CALLBACK_TIME_ADDED.label + " DESC")

            if (processed != null) {
                sphinxClient.SetFilter(USER_CALLBACK_PROCESSED.label, if (processed) 1 else 0, false)
            }
            setFilterDateRange(sphinxClient, USER_CALLBACK_TIME_ADDED, dateFrom, dateTo)

            val sphinxResult = trySearchWithTranslation(sphinxClient, query, SearchIndex.USER_CALLBACKS, searchResult)
            results = userCallbackService.getCallbacksByIds(searchResultToListIds(sphinxResult))
            searchResult.searchResultRecords = results
        }
        return searchResult
    }

    @Throws(SphinxException::class, NotFoundException::class)
    override fun searchForInvestingOrderInfo(query: String, status: ModerateStatus, dateFrom: Date, dateTo: Date, offset: Int, limit: Int): SearchResult<InvestingOrderInfo> {
        var searchResult = SearchResult<InvestingOrderInfo>()
        val results: List<InvestingOrderInfo>
        if (Utils.empty(query)) {
            results = investingOrderInfoService.select(status, dateFrom, dateTo, offset, limit)
            searchResult = SearchResult(0, investingOrderInfoService.count(), results)
        } else {
            val sphinxClient = factory.sphinxClient
            sphinxClient.SetLimits(offset, limit, MAX_MATCHES)
            sphinxClient.SetMaxQueryTime(MAX_QUERY_TIME)

            sphinxClient.SetSortMode(SphinxClient.SPH_SORT_EXTENDED, INVESTING_ORDER_INFO_TIME_ADDED.label + " DESC")

            sphinxClient.SetFilter(INVESTING_ORDER_INFO_STATUS.label, status.code, false)
            setFilterDateRange(sphinxClient, INVESTING_ORDER_INFO_TIME_ADDED, dateFrom, dateTo)

            val sphinxResult = trySearchWithTranslation(sphinxClient, query, SearchIndex.INVESTING_ORDERS_INFO, searchResult)
            results = investingOrderInfoService.select(searchResultToListIds(sphinxResult))
            searchResult.searchResultRecords = results
        }
        return searchResult
    }

    @Deprecated("")
    @Throws(SphinxException::class)
    override fun searchForCampaignsAdmins(clientId: Long, query: String,
                                          offset: Int, limit: Int,
                                          campaignStatus: CampaignStatus, managerId: Long?,
                                          timeAddedRange: Range<Date>, sortOrderList: List<SortOrder>, campaignTagId: Int?): SearchResult<Campaign> {

        val statuses = if (campaignStatus === CampaignStatus.ALL) {
            EnumSet.of(CampaignStatus.APPROVED, CampaignStatus.ACTIVE, CampaignStatus.FINISHED, CampaignStatus.DRAFT,
                    CampaignStatus.PAUSED, CampaignStatus.PATCH, CampaignStatus.NOT_STARTED, CampaignStatus.DECLINED)
        } else {
            EnumSet.of(campaignStatus)
        }
        var tags: List<CampaignTag>? = null
        if (campaignTagId != null) {
            val tag = CampaignTag()
            tag.id = campaignTagId
            tags = listOf(tag)
        }
        val searchCampaigns = SearchCampaigns(sortOrderList, query, tags, null, null, null, null, timeAddedRange, statuses, managerId, false, false, offset, limit)
        return searchForCampaigns(searchCampaigns)
    }

    @Throws(SphinxException::class)
    override fun searchForCampaignRelated(tagList: List<CampaignTag>, ignoreCampaignId: Long): Collection<Campaign>? {
        val searchCampaigns = SearchCampaigns(listOf(SortOrder.SORT_DEFAULT), tagList, EnumSet.of(CampaignStatus.ACTIVE), Range(BigDecimal.ONE, BigDecimal.valueOf(Long.MAX_VALUE)),
                tagList.size + 1)
        val searchResult = searchForCampaigns(searchCampaigns)
        val result = searchResult.searchResultRecords
        return ru.planeta.commons.lang.CollectionUtils.subList(result, 0, tagList.size).filter {
            it.campaignId != ignoreCampaignId
        }
    }

    override fun searchForCampaignRelatedForFinishedCampaign(campaign: Campaign): Collection<Campaign> {
        val result = ArrayList<Campaign>()
        var searchResult: SearchResult<Campaign>
        try {
            val mainTag = campaign.mainTag
            val searchCampaigns = SearchCampaigns(listOf(SortOrder.SORT_BY_COMPLETE_PERCENTAGE), "",
                    if (mainTag != null) listOf(mainTag) else emptyList(),
                    EnumSet.of(CampaignStatus.ACTIVE), 0, 5)
            searchResult = searchForCampaigns(searchCampaigns)
            searchResult.searchResultRecords?.forEach {
                if (it.campaignId != campaign.campaignId) {
                    result.add(it)
                    if (result.size == 4) {
                        return result
                    }
                }
            }
        } catch (ignore: SphinxException) {
        }

        try {
            val searchCampaigns = SearchCampaigns(listOf(SortOrder.SORT_BY_COMPLETE_PERCENTAGE), "", EnumSet.of(CampaignStatus.ACTIVE), 0, 10)
            searchResult = searchForCampaigns(searchCampaigns)

            searchResult.searchResultRecords?.forEach {
                var found = false
                for (foundCampaign in result) {
                    if (foundCampaign.campaignId == it.campaignId) {
                        found = true
                        break
                    }
                }
                if (!found && it.campaignId != campaign.campaignId) {
                    result.add(it)
                    if (result.size >= 4) {
                        return result
                    }
                }
            }

        } catch (ignore: SphinxException) {
            return result
        }

        return result
    }

    @Throws(SphinxException::class)
    override fun searchForCampaigns(searchCampaigns: SearchCampaigns): SearchResult<Campaign> {
        val sphinxClient = factory.sphinxClient
        sphinxClient.SetLimits(searchCampaigns.offset, searchCampaigns.limit, MAX_MATCHES)
        sphinxClient.SetMaxQueryTime(MAX_QUERY_TIME)

        val stringSortBy = getStringSortBy(searchCampaigns.sortOrderList ?: emptyList(), sphinxClient)

        if (StringUtils.isNotEmpty(stringSortBy)) {
            sphinxClient.SetSortMode(SphinxClient.SPH_SORT_EXTENDED, stringSortBy)
        }

        val sortorder = searchCampaigns.sortOrderList?.get(0)
        if (sortorder != null && sortorder == SortOrder.SORT_RELEVANCE) {
            sphinxClient.SetSortMode(SphinxClient.SPH_SORT_RELEVANCE, null)
            val weightMap = HashMap<String, Int>()
            weightMap[SearchIndexField.CAMPAIGN_NAME.label] = 1000
            weightMap[SearchIndexField.CAMPAIGN_GROUP_NAME.label] = 100
            weightMap[SearchIndexField.CAMPAIGN_SHORT_DESCRIPTION.label] = 10
            weightMap[SearchIndexField.CAMPAIGN_DESCRIPTION.label] = 1
            sphinxClient.SetFieldWeights(weightMap)
        } else {
            fillWeightMaps(sphinxClient)
        }

        if (searchCampaigns.campaignStatus == null) {
            searchCampaigns.campaignStatus = EnumSet.of(CampaignStatus.ACTIVE)
        }
        searchCampaigns.campaignStatus?.let {
            if (it.isNotEmpty()) {
                val statusIds = IntArray(it.size)
                var i = 0
                it.forEach { status ->
                    statusIds[i] = status.code
                    i++
                }
                sphinxClient.SetFilter(CAMPAIGN_STATUS.label, statusIds, false)
            }
        }
        if (!CollectionUtils.isEmpty(searchCampaigns.campaignTags)) {
            if (CampaignStatusFilter.CHARITY == searchCampaigns.status) {
                searchCampaigns.campaignTags?.forEach {
                    sphinxClient.SetFilter(CAMPAIGN_TAG.label, it.id, false)
                }
            } else {
                searchCampaigns.campaignTags?.let {
                    val idsArray = IntArray(it.size)
                    for (j in idsArray.indices) {
                        idsArray[j] = it[j].id
                    }
                    sphinxClient.SetFilter(CAMPAIGN_TAG.label, idsArray, false)
                }
            }
        } else if (EnumSet.of(CampaignStatusFilter.ACTIVE, CampaignStatusFilter.RECOMMENDED).contains(searchCampaigns.status)) {
            // exclude charity
            sphinxClient.SetFilter(CAMPAIGN_TAG.label, CampaignTag.CHARITY_TAG_ID, true)
        }

        if (searchCampaigns.completePercentsRange != null) {
            val min = searchCampaigns.completePercentsRange?.min
            val max = searchCampaigns.completePercentsRange?.max
            sphinxClient.SetFilterRange(CAMPAIGN_COMPLETE_PERCENTS.label, min ?: 0, max ?: Integer.MAX_VALUE, false)
        }

        if (searchCampaigns.targetAmountRange != null) {
            val minTargetAmount = searchCampaigns.targetAmountRange?.min?.toFloat() ?: 0f
            val maxTargetAmount = searchCampaigns.targetAmountRange?.max?.toFloat() ?: Float.MAX_VALUE

            sphinxClient.SetFilterFloatRange(CAMPAIGN_TARGET_AMOUNT.label, minTargetAmount, maxTargetAmount, false)
        }

        if (searchCampaigns.collectedAmountRange != null) {
            val minTargetAmount = searchCampaigns.collectedAmountRange?.min?.toFloat() ?: 0f
            val maxTargetAmount = searchCampaigns.collectedAmountRange?.max?.toFloat() ?: Float.MAX_VALUE

            sphinxClient.SetFilterFloatRange(CAMPAIGN_COLLECTED_SUM.label, minTargetAmount, maxTargetAmount, false)
        }

        if (searchCampaigns.finishTimeRange != null) {
            val min = searchCampaigns.finishTimeRange?.min
            val max = searchCampaigns.finishTimeRange?.max
            val timeStart = if (min == null) 0 else getTimeWithoutTimeZone(min)
            val timeFinish = if (max == null) Long.MAX_VALUE else getTimeWithoutTimeZone(max)

            sphinxClient.SetFilterRange(CAMPAIGN_TIME_FINISHED.label, timeStart, timeFinish, false)
        }

        if (searchCampaigns.timeAddedRange != null) {
            val min = searchCampaigns.timeAddedRange?.min
            val max = searchCampaigns.timeAddedRange?.max
            val timeAddedStart = if (min == null) 0 else getTimeWithoutTimeZone(min)
            val timeAddedFinish = if (max == null) Long.MAX_VALUE else getTimeWithoutTimeZone(max)

            sphinxClient.SetFilterRange(CAMPAIGN_TIME_ADDED.label, timeAddedStart, timeAddedFinish, false)
        }

        if (searchCampaigns.status == CampaignStatusFilter.RECORD_HOLDERS) {
            val purchaseSumThreshold = BigDecimal.valueOf(configurationService.campaignSuccessThreshold.toLong())
            sphinxClient.SetFilterFloatRange(CAMPAIGN_COLLECTED_SUM.label, purchaseSumThreshold.toFloat(), java.lang.Float.MAX_VALUE, false)
        }

        if (searchCampaigns.status == CampaignStatusFilter.LAST_UPDATED) {
            sphinxClient.SetSortMode(SphinxClient.SPH_SORT_EXTENDED, CAMPAIGN_LAST_NEWS_PUBLISH_DATE.label + " DESC")
            sphinxClient.SetFilter(CAMPAIGN_LAST_NEWS_PUBLISH_DATE.label, 0, true)
        }

        if (searchCampaigns.status == CampaignStatusFilter.SUCCESSFUL) {
            sphinxClient.SetFilter(CAMPAIGN_TARGET_STATUS.label, 1, false)
        }

        if (java.lang.Boolean.TRUE == searchCampaigns.isDontShowUnsuccessfulProject) {
            sphinxClient.SetFilter(CAMPAIGN_TARGET_STATUS.label, 2, true)
        }

        searchCampaigns.managerId?.let {
            sphinxClient.SetFilter(CAMPAIGN_MANAGER_ID.label, it, false)
        }

        if (searchCampaigns.isWithoutPromoCampaignsAtFirstPage) {
            sphinxClient.SetFilter(CAMPAIGN_MAIN_PAGE.label, 1, true)
        }

        if (searchCampaigns.cityId != 0L) {
            sphinxClient.SetFilter(CAMPAIGN_CITY.label, searchCampaigns.cityId, false)
        } else if (searchCampaigns.regionId != 0L) {
            sphinxClient.SetFilter(CAMPAIGN_REGION.label, searchCampaigns.regionId, false)
        } else if (searchCampaigns.countryId != 0L) {
            sphinxClient.SetFilter(CAMPAIGN_COUNTRY.label, searchCampaigns.countryId, false)
        }

        val searchResult = SearchResult<Campaign>()

        val sphinxResult = trySearchWithTranslation(sphinxClient, searchCampaigns.query, SearchIndex.CAMPAIGNS, searchResult)

        var campaignIds: MutableList<Long> = searchResultToListIds(sphinxResult)


        campaignIds = modifyResultsAfterCampaignSearch(campaignIds, searchCampaigns, configurationService.commercialCampaigns)
        val resultRecords = campaignDAO.getCampaignsByIds(campaignIds)
        searchResult.searchResultRecords = resultRecords
        return searchResult
    }

    /**
     * Populates list with profile search result.
     * If there are no search results -- returns null.
     */
    private fun populateProfileSearchResults(sphinxResult: SphinxResult, clientId: Long): List<ProfileSearchResult> {

        val objects = searchResultToListIds(sphinxResult)
        val selectList = profileDAO.selectSearchedByIds(objects, clientId)
        val list = ArrayList<ProfileSearchResult>(selectList.size)
        for (i in selectList) {
            list.add(ProfileSearchResult(i))
        }
        return list
    }

    @Throws(SphinxException::class)
    override fun getTopFilteredCampaignsSearchResult(searchCampaigns: SearchCampaigns): SearchResult<Campaign> {
        val stopWatch = StopWatch()
        stopWatch.start()

        var searchResult: SearchResult<Campaign>
        try {
            when (searchCampaigns.status) {
                CampaignStatusFilter.FINISHED -> {
                    searchCampaigns.campaignStatus = EnumSet.of(CampaignStatus.FINISHED)
                    searchResult = searchForCampaigns(searchCampaigns)
                }
                CampaignStatusFilter.ACTIVE_AND_FINISHED, CampaignStatusFilter.RECORD_HOLDERS, CampaignStatusFilter.SUCCESSFUL, CampaignStatusFilter.LAST_UPDATED -> {
                    searchCampaigns.campaignStatus = EnumSet.of(CampaignStatus.ACTIVE, CampaignStatus.FINISHED)
                    searchResult = searchForCampaigns(searchCampaigns)
                }
                CampaignStatusFilter.RECOMMENDED, CampaignStatusFilter.ACTIVE, CampaignStatusFilter.NEW, CampaignStatusFilter.DISCUSSED -> {
                    searchCampaigns.campaignStatus = EnumSet.of(CampaignStatus.ACTIVE)
                    searchResult = searchForCampaigns(searchCampaigns)
                }
                CampaignStatusFilter.CHARITY -> {
                    searchCampaigns.campaignStatus = EnumSet.of(CampaignStatus.ACTIVE, CampaignStatus.FINISHED)
                    var campaignTags: MutableList<CampaignTag>? = searchCampaigns.campaignTags
                    if (campaignTags == null || campaignTags.isEmpty()) {
                        campaignTags = ArrayList(1)
                    }
                    campaignService.getCampaignTagByMnemonic(CampaignTag.CHARITY)?.let {
                        campaignTags.add(it)
                    }
                    searchCampaigns.campaignTags = campaignTags
                    searchResult = searchForCampaigns(searchCampaigns)
                }
                CampaignStatusFilter.CLOSE_TO_FINISH -> {
                    var startTime = Date()
                    var finishTime = DateUtils.addMonths(Date(), 1)
                    val finishTimeRange = searchCampaigns.finishTimeRange
                    if (finishTimeRange != null) {
                        if (finishTimeRange.min != null) {
                            startTime = if (startTime.before(finishTimeRange.min)) finishTimeRange.min else startTime
                        }
                        if (finishTimeRange.max != null) {
                            finishTime = if (finishTime.after(finishTimeRange.max)) finishTimeRange.max else finishTime
                        }
                    }
                    if (startTime.after(finishTime)) {
                        searchResult = SearchResult(0, 0, listOf())
                    } else {
                        val timeRange = Range(startTime, finishTime)
                        searchCampaigns.sortOrderList = listOf(SortOrder.SORT_BY_TIME_FINISHED_ASC)
                        searchCampaigns.finishTimeRange = timeRange
                        searchCampaigns.campaignStatus = EnumSet.of(CampaignStatus.ACTIVE)
                        searchResult = searchForCampaigns(searchCampaigns)
                    }
                }
                else -> {
                    searchResult = SearchResult(0, 0, listOf())
                    log.error("No such state " + searchCampaigns.status)
                }
            }
        } finally {
            stopWatch.stop()
        }

        searchResult.searchTime = stopWatch.time

        return searchResult
    }

    @Throws(SphinxException::class, NotFoundException::class)
    override fun searchForCampaignOrders(searchOrderFilter: SearchOrderFilter): SearchResult<OrderInfo> {
        searchOrderFilter.orderObjectType = OrderObjectType.SHARE
        return searchForOrders(searchOrderFilter)
    }

    @Throws(SphinxException::class, NotFoundException::class)
    override fun searchForMerchantOrders(searchOrderFilter: SearchOrderFilter): SearchResult<OrderInfo> {
        searchOrderFilter.orderObjectType = OrderObjectType.PRODUCT
        return searchForOrders(searchOrderFilter)
    }

    @Throws(SphinxException::class, NotFoundException::class)
    override fun searchForBiblioOrders(searchOrderFilter: SearchOrderFilter): SearchResult<OrderInfo> {
        searchOrderFilter.orderObjectType = OrderObjectType.BIBLIO
        return searchForOrders(searchOrderFilter)
    }

    @Throws(SphinxException::class, NotFoundException::class)
    override fun searchForOrders(searchOrderFilter: SearchOrderFilter): SearchResult<OrderInfo> {
        return searchForOrders(SearchIndex.ORDERS, searchOrderFilter)
    }

    @Throws(SphinxException::class, NotFoundException::class)
    internal fun searchForOrders(searchIndex: SearchIndex, searchOrderFilter: SearchOrderFilter): SearchResult<OrderInfo> {

        val sphinxClient = factory.sphinxClient
        sphinxClient.SetLimits(searchOrderFilter.offset, if (searchOrderFilter.limit != 0) searchOrderFilter.limit else Integer.MAX_VALUE, MAX_MATCHES)
        sphinxClient.SetMaxQueryTime(5000)
        sphinxClient.SetSortMode(SphinxClient.SPH_SORT_EXTENDED, ORDER_TIME_ADDED.label + " DESC")

        setFilter(sphinxClient, ORDER_CAMPAIGN_ID, searchOrderFilter.campaignId)
        setFilter(sphinxClient, ORDER_SHARE_ID, searchOrderFilter.shareId)
        setFilter(sphinxClient, ORDER_PRODUCT_ID, searchOrderFilter.productId)
        setFilter(sphinxClient, ORDER_PAYMENT_STATUS, searchOrderFilter.paymentStatus)
        setFilter(sphinxClient, ORDER_DELIVERY_STATUS, searchOrderFilter.deliveryStatus)
        setFilter(sphinxClient, ORDER_MERCHANT_ID, searchOrderFilter.merchantId)
        setFilter(sphinxClient, ORDER_TYPE, searchOrderFilter.orderObjectTypeId.toLong())

        setFilterDateRange(sphinxClient, ORDER_TIME_ADDED, searchOrderFilter.dateFrom, searchOrderFilter.dateTo)

        val searchResult = SearchResult<OrderInfo>()
        val sphinxResult = trySearchWithTranslation(sphinxClient, searchOrderFilter.searchString, searchIndex, searchResult)

        val orderList = orderDAO.selectOrdersByIds(searchResultToListIds(sphinxResult))

        searchResult.searchResultRecords = orderInfoService.getOrdersInfo(orderList, true) as List<OrderInfo>
        return searchResult
    }

    @Throws(SphinxException::class)
    override fun searchForProductsNew(productsSearch: ProductsSearch): List<ProductInfo> {
        return searchForProducts(productsSearch).searchResultRecords ?: listOf()
    }

    @Throws(SphinxException::class)
    override fun searchForProducts(productsSearch: ProductsSearch): SearchResult<ProductInfo> {
        val sphinxClient = factory.sphinxClient
        sphinxClient.SetLimits(productsSearch.offset, if (productsSearch.limit != 0) productsSearch.limit else MAX_MATCHES)
        sphinxClient.SetMaxQueryTime(5000)

        var stringSortBy: String? = null
        when (productsSearch.sortOrder) {
            SortOrder.SORT_DEFAULT -> stringSortBy = PRODUCT_TIME_LAST_PURCHASED.label + " DESC"
            SortOrder.SORT_BY_TIME_ADDED_DESC -> stringSortBy = PRODUCT_TIME_ADDED.label + " DESC"
            SortOrder.SORT_BY_TIME_ADDED_ASC -> stringSortBy = PRODUCT_TIME_ADDED.label + " ASC"
            SortOrder.SORT_BY_PRICE_DESC -> stringSortBy = PRODUCT_PRICE.label + " DESC"
            SortOrder.SORT_BY_PRICE_ASC -> stringSortBy = PRODUCT_PRICE.label + " ASC"
        }

        if (StringUtils.isNotEmpty(stringSortBy)) {
            sphinxClient.SetSortMode(SphinxClient.SPH_SORT_EXTENDED, stringSortBy)
        }

        if (!CollectionUtils.isEmpty(productsSearch.productTags)) {
            productsSearch.productTags?.forEach {
                sphinxClient.SetFilter(PRODUCT_CATEGORY.label, it.categoryId, false)
            }
        }

        if (!CollectionUtils.isEmpty(productsSearch.referrerIds)) {
            val referrerIds = productsSearch.referrerIds
            val intArray = ArrayUtils.toPrimitive(referrerIds?.toTypedArray())
            sphinxClient.SetFilter(PRODUCT_REFERRER_ID.label, intArray, false)
        }

        var minTargetAmount = (if (productsSearch.priceFrom > 0) productsSearch.priceFrom else 0).toFloat()
        val maxTargetAmount = (if (productsSearch.priceTo > 0) productsSearch.priceTo else MAX_PRICE).toFloat()

        if (minTargetAmount > maxTargetAmount) {
            minTargetAmount = 0f
        }

        sphinxClient.SetFilterFloatRange(SHARE_PRICE.label, minTargetAmount, maxTargetAmount, false)

        val searchResult = SearchResult<ProductInfo>()
        val sphinxResult = trySearchWithTranslation(sphinxClient, productsSearch.query, SearchIndex.PRODUCTS, searchResult)
        val productIds = searchResultToListIds(sphinxResult)

        searchResult.searchResultRecords = productUsersService.magicProductsSearch(productIds)
        return searchResult
    }

    @Throws(SphinxException::class)
    override fun searchForShares(searchShares: SearchShares): SearchResult<ShareForSearch> {
        val sphinxClient = factory.sphinxClient
        sphinxClient.SetLimits(searchShares.offset, searchShares.limit, MAX_MATCHES)
        sphinxClient.SetMaxQueryTime(MAX_QUERY_TIME)

        var excludeUnlimitedRewards = false
        var fewRewards = false
        var rareRewards = false

        var stringSortBy = ""
        for (sortOrder in searchShares.sortOrderList) {
            var s: String? = null
            when (sortOrder) {
                SortOrder.SORT_BY_PRICE_DESC -> s = SHARE_PRICE.label + " DESC"
                SortOrder.SORT_BY_PRICE_ASC -> s = SHARE_PRICE.label + " ASC"
                SortOrder.SORT_BY_TIME_ADDED_DESC -> s = CAMPAIGN_TIME_STARTED.label + " DESC"
                SortOrder.SORT_BY_TIME_ADDED_ASC -> s = CAMPAIGN_TIME_STARTED.label + " ASC"
                SortOrder.SORT_BY_COUNT_DESC -> s = SHARE_AMOUNT.label + " DESC"
                SortOrder.SORT_BY_COUNT_ASC -> {
                    s = SHARE_AMOUNT.label + " ASC"
                    excludeUnlimitedRewards = true
                }
                SortOrder.SORT_BY_PURCHASE_COUNT_DESC -> s = SHARE_PURCHASE_COUNT.label + " DESC"
                SortOrder.SORT_BY_PURCHASE_COUNT_ASC -> s = SHARE_PURCHASE_COUNT.label + " ASC"
                SortOrder.SORT_BY_NAME_ASC -> s = SHARE_NAME.label + " ASC"
                SortOrder.SORT_BY_NAME_DESC -> s = SHARE_NAME.label + " DESC"
                SortOrder.SORT_BY_PURCHASE_DATE_DESC -> s = SHARE_PURCHASE_DATE.label + " DESC"
                SortOrder.SORT_RARE -> {
                    s = SHARE_AMOUNT.label + " ASC"
                    rareRewards = true
                }
                SortOrder.SORT_FEW -> {
                    s = SHARE_LEFT_AMOUNT_IN_PERCENT.label + " ASC"
                    fewRewards = true
                }
            }

            if (s != null) {
                if (StringUtils.isNotEmpty(stringSortBy)) {
                    stringSortBy += ", "
                }
                stringSortBy += s
            }
        }

        if (StringUtils.isNotEmpty(stringSortBy)) {
            sphinxClient.SetSortMode(SphinxClient.SPH_SORT_EXTENDED, stringSortBy)
        }

        if (excludeUnlimitedRewards) {
            sphinxClient.SetFilterRange(SHARE_AMOUNT.label, 1, Integer.MAX_VALUE, false)
        }

        searchShares.campaignTags?.let {
            if (!it.isEmpty()) {
                val idsArray = IntArray(it.size)
                for (i in idsArray.indices) {
                    idsArray[i] = it[i].id
                }
                sphinxClient.SetFilter(CAMPAIGN_TAG.label, idsArray, false)
            }
        }

        var minTargetAmount = (if (searchShares.priceFrom > 0) searchShares.priceFrom else 0).toFloat()
        val maxTargetAmount = (if (searchShares.priceTo > 0) searchShares.priceTo else MAX_PRICE).toFloat()

        if (minTargetAmount > maxTargetAmount) {
            minTargetAmount = 0f
        }

        sphinxClient.SetFilterFloatRange(SHARE_PRICE.label, minTargetAmount, maxTargetAmount, false)

        //
        // Вознаграждения из проектов, которые собрали от 50% до 90% от заданной финансовой цели
        // Вознаграждения из проектов без цели сюда не попадают
        //
        if (searchShares.isWelcomeSearch) {
            sphinxClient.SetFilterRange(CAMPAIGN_PURCHASE_PERCENT_OF_TARGET.label, 50, 90, false)
        }

        //
        //Вознаграждения, которых изначально было от 1 до 10 шт, а на данный момент они ещё есть в наличие и сумма сбора
        //проекта не менее 10% от заданной цели
        //Безлимитные акции сюда не попадают
        //Акции из проектов без цели сюда не попадают
        //
        if (rareRewards) {
            sphinxClient.SetFilterRange(SHARE_AMOUNT.label, 1, 10, false)
            sphinxClient.SetFilterRange(CAMPAIGN_PURCHASE_PERCENT_OF_TARGET.label, 10, Integer.MAX_VALUE, false)
        }

        //
        //Вознаграждения, которых осталось в наличии менее 30% от изначального кол-ва.
        //Безлимитные акции сюда не попадают
        //
        if (fewRewards) {
            sphinxClient.SetFilterRange(SHARE_LEFT_AMOUNT_IN_PERCENT.label, 1, 30, false)
        }

        val searchResult = SearchResult<ShareForSearch>()

        val sphinxResult = trySearchWithTranslation(sphinxClient, searchShares.query, SearchIndex.SHARES, searchResult)

        var shareIds: MutableList<Long> = searchResultToListIds(sphinxResult)

        shareIds = modifyResultsAfterShareSearch(shareIds, searchShares, configurationService.commercialShares)


        searchResult.searchResultRecords = shareSearchDAO.select(shareIds)
        return searchResult
    }

    @Throws(SphinxException::class)
    override fun searchSharesForWelcome(searchShares: SearchShares): List<ShareForSearch> {
        val shares = ArrayList<ShareForSearch>()
        var count = 0

        //TODO (придумать, как сделать это говно красиво)
        while (shares.size < 4 && count < MAX_MATCHES / 100) {
            val result = searchForShares(searchShares).searchResultRecords

            result?.forEach { record ->
                val status = AtomicBoolean(true)

                if (shares.size == 0) {
                    shares.add(record)
                } else {
                    shares.forEach { exist ->
                        if (record.campaignId === exist.campaignId) {
                            status.set(false)
                        }
                    }

                    if (status.get()) {
                        if (shares.size < 4) {
                            shares.add(record)
                        }
                    }
                }
            }

            count++
            searchShares.offset = searchShares.offset + searchShares.limit
        }

        return shares
    }

    companion object {

        private val MAX_MATCHES = 10000
        private val MAX_QUERY_TIME = 5000

        private val MAX_PRICE = Integer.MAX_VALUE

        private val COMMENTS_COUNT_THRESHOLD = 10
        private val log = Logger.getLogger(SearchServiceImpl::class.java)

        private fun modifyResultsAfterCampaignSearch(campaignIds: MutableList<Long>, searchCampaigns: SearchCampaigns, promoCampaginIds: Set<Long>): MutableList<Long> {
            var campaignIds = campaignIds
            if (CollectionUtils.isEmpty(promoCampaginIds)) {
                return campaignIds
            }

            // condition for promo inection on recommended search
            if (CampaignStatusFilter.RECOMMENDED == searchCampaigns.status && searchCampaigns.offset == 12    // recommended campaigns starts with offset 12
            ) {
                campaignIds = substituteResultList(campaignIds, promoCampaginIds, 3) // first 3 positions
            } else if (CampaignStatusFilter.ACTIVE_AND_FINISHED == searchCampaigns.status
                    && StringUtils.isEmpty(searchCampaigns.query)
                    && searchCampaigns.countryId == 0L
                    && searchCampaigns.regionId == 0L
                    && searchCampaigns.cityId == 0L
                    && CollectionUtils.isEmpty(searchCampaigns.campaignTags)
                    && searchCampaigns.offset == 0
                    && searchCampaigns.managerId == null
                    && searchCampaigns.finishTimeRange == null) {
                campaignIds = substituteResultList(campaignIds, promoCampaginIds, searchCampaigns.limit) // first page
            }// condition for promo injection on big search
            return campaignIds
        }

        private fun substituteResultList(ids: MutableList<Long>, promoIds: Set<Long>, maxPosition: Int): MutableList<Long> {
            var promoIds = promoIds
            val initialSize = ids.size

            // firstly remove all promo elements
            ids.removeAll(promoIds)

            // secondary remove other unuseful elements
            val diffSize = ids.size + promoIds.size - initialSize
            var i = 0
            while (i < diffSize && i < initialSize) {
                ids.removeAt(RandomUtils.nextInt(0, ids.size))
                i++
            }

            val endPosition: Int
            if (maxPosition > promoIds.size) {
                endPosition = maxPosition - promoIds.size + 1
            } else {
                // all promo projects fills full result set, need to shuffle and add on first position
                endPosition = 1
                val promoIdsList = ArrayList(promoIds)
                Collections.shuffle(promoIdsList)
                promoIds = HashSet(promoIdsList)
            }

            var counter = 0
            for (promo in promoIds) {
                if (counter++ >= initialSize) {
                    break
                }
                ids.add(RandomUtils.nextInt(0, endPosition), promo)
            }

            return ids
        }

        private fun searchResultToListIds(sphinxResult: SphinxResult): MutableList<Long> {
            if (sphinxResult.total == 0 || sphinxResult.matches.isEmpty()) {
                return mutableListOf()
            }
            val objects = ArrayList<Long>(sphinxResult.matches.size)

            for (match in sphinxResult.matches) {
                objects.add(match.docId)
            }

            return objects
        }

        @Throws(SphinxException::class)
        @JvmOverloads
        fun getStringSortBy(sortOrderList: List<SortOrder>, sphinxClient: SphinxClient? = null): String {
            var stringSortBy = ""

            for (sortOrder in sortOrderList) {
                var s: String? = null
                when (sortOrder) {
                    SortOrder.SORT_DEFAULT -> s = CAMPAIGN_LAST_PURCHASE_DATE.label + " DESC"
                    SortOrder.SORT_BY_SUM -> s = CAMPAIGN_COLLECTED_SUM.label + " DESC"
                    SortOrder.SORT_BY_DATE, SortOrder.SORT_BY_TIME_STARTED_DESC -> s = CAMPAIGN_TIME_STARTED.label + " DESC"
                    SortOrder.SORT_BY_TIME_STARTED_ASC -> s = CAMPAIGN_TIME_STARTED.label + " ASC"
                    SortOrder.SORT_BY_TIME_ADDED_DESC -> s = CAMPAIGN_TIME_ADDED.label + " DESC"
                    SortOrder.SORT_BY_TIME_ADDED_ASC -> s = CAMPAIGN_TIME_ADDED.label + " ASC"
                    SortOrder.SORT_BY_TIME_UPDATED_DESC -> s = CAMPAIGN_TIME_UPDATED.label + " DESC"
                    SortOrder.SORT_BY_TIME_UPDATED_ASC -> s = CAMPAIGN_TIME_UPDATED.label + " ASC"
                    SortOrder.SORT_BY_TIME_FINISHED_ASC -> s = CAMPAIGN_TIME_FINISHED.label + " ASC"
                    SortOrder.SORT_BY_TIME_FINISHED_DESC -> s = CAMPAIGN_TIME_FINISHED.label + " DESC"
                    SortOrder.SORT_BY_TIME_STARTED_THAN_CREATED -> s = CAMPAIGN_TIME_STARTED.label + " DESC, " + CAMPAIGN_TIME_ADDED.label + " DESC"
                    SortOrder.SORT_BY_MEDIAN_PRICE -> s = CAMPAIGN_MEDIAN_PRICE.label + " ASC, " + CAMPAIGN_LAST_PURCHASE_DATE.label + " DESC"
                    SortOrder.SORT_BY_COLLECTION_RATE -> s = CAMPAIGN_SORT_BY_COLLECTION_RATE.label + " DESC"
                    SortOrder.SORT_BY_TIME_LAST_NEWS_PUBLISHED -> s = CAMPAIGN_LAST_NEWS_PUBLISH_DATE.label + " DESC"
                    SortOrder.SORT_BY_COMPLETE_PERCENTAGE -> s = CAMPAIGN_COMPLETE_PERCENTS.label + " DESC"
                    SortOrder.SORT_BY_COMMENTS_COUNT -> {
                        s = COMMENTS_COUNT.label + " DESC"
                        sphinxClient?.SetFilterRange(COMMENTS_COUNT.label, COMMENTS_COUNT_THRESHOLD.toLong(), java.lang.Long.MAX_VALUE, false)
                    }
                }
                if (s != null) {
                    if (StringUtils.isNotEmpty(stringSortBy)) {
                        stringSortBy += ", "
                    }
                    stringSortBy += s
                }
            }
            return stringSortBy
        }

        /**
         * Executes query against sphinx server. Also sets match mode.
         * If query contains only AND operators - than SPH_MATCH_ALL mode is chosen.
         * If query contains only OR operators - than SPH_MATCH_ANY mode is chosen.
         */
        @Throws(SphinxException::class)
        private fun executeQuery(sphinxClient: SphinxClient, query: String?, index: SearchIndex): SphinxResult? {
            var queryToExecute = query
            if (StringUtils.contains(query, "@")) {
                sphinxClient.SetMatchMode(SphinxClient.SPH_MATCH_BOOLEAN)
            } else if (!StringUtils.contains(query, "|")) {
                sphinxClient.SetMatchMode(SphinxClient.SPH_MATCH_ALL)
                queryToExecute = StringUtils.replace(query, "&", " ")
            } else if (!StringUtils.contains(query, "&")) {
                sphinxClient.SetMatchMode(SphinxClient.SPH_MATCH_ANY)
                queryToExecute = StringUtils.replace(query, "|", " ")
            } else {
                sphinxClient.SetMatchMode(SphinxClient.SPH_MATCH_BOOLEAN)
            }
            return sphinxClient.Query(queryToExecute, index.label)
        }

        @Throws(SphinxException::class)
        private fun trySearchWithTranslation(sphinxClient: SphinxClient, query: String?, index: SearchIndex, searchResult: SearchResult<*>): SphinxResult {
            return trySearchWithTranslationNoEscapedQuery(sphinxClient, if (StringUtils.isBlank(query)) query else SphinxClient.EscapeString(query), index, searchResult)
        }

        @Throws(SphinxException::class)
        private fun trySearchWithTranslationNoEscapedQuery(sphinxClient: SphinxClient, query: String?, index: SearchIndex, searchResult: SearchResult<*>): SphinxResult {
            var sphinxResult = trySearch(sphinxClient, query, index, searchResult)
            if (!Utils.empty(query)) {
                if (sphinxResult.totalFound < 1) {
                    sphinxResult = trySearch(sphinxClient, TranslitConverter.toTranslit(query), index, searchResult)
                }
                if (sphinxResult.totalFound < 1) {
                    val convertedQuery = TranslitConverter.changeEngToRusLayout(query)
                    if (convertedQuery != null) {
                        sphinxResult = trySearch(sphinxClient, convertedQuery, index, searchResult)
                    }
                }
                if (sphinxResult.totalFound < 1) {
                    val convertedQuery = TranslitConverter.changeRusToEngLayout(query)
                    if (convertedQuery != null) {
                        sphinxResult = trySearch(sphinxClient, SphinxClient.EscapeString(convertedQuery), index, searchResult)
                    }
                }
            }
            return sphinxResult
        }

        /**
         * Search for query and prepare results.
         */
        @Throws(SphinxException::class)
        fun trySearch(sphinxClient: SphinxClient, query: String?, index: SearchIndex, searchResult: SearchResult<*>): SphinxResult {
            val stopWatch = StopWatch()
            stopWatch.start()
            val sphinxResult: SphinxResult?

            val searchTime: Long
            try {
                sphinxResult = executeQuery(sphinxClient, query, index)
            } finally {
                stopWatch.stop()
                searchTime = stopWatch.time
            }

            if (sphinxResult == null) {
                log.error("Error searching for " + query + ": " + sphinxClient.GetLastError())
                throw SphinxException(sphinxClient.GetLastError())
            } else {
                searchResult.searchTime = searchTime
                searchResult.estimatedCount = sphinxResult.totalFound.toLong()

                return sphinxResult
            }
        }

        /**
         * Set field weights preset.
         * If filterField is specified, search only that field.
         */
        private val WEIGHT = 1000

        @Throws(SphinxException::class)
        private fun fillWeightMaps(sphinxClient: SphinxClient, vararg filterFields: String) {
            val fields = ArrayList<String>()
            for (field in filterFields) {
                if (field != null) {
                    fields.add(field)
                }
            }

            val weightMap = SearchIndexField.weightMap
            if (!fields.isEmpty()) {
                for (field in fields) {
                    weightMap[field] = WEIGHT
                }
                sphinxClient.SetFilterRange("@weight", 0, WEIGHT - 1, true)
            }
            sphinxClient.SetFieldWeights(weightMap)
        }

        private fun sortByMatches(list: List<ProfileSearchResult>?, matches: Array<SphinxMatch>) {
            val comparator = object : Comparator<ProfileSearchResult> {
                @Transient

                val matchmap = mutableMapOf<Long, Int>()

                init {
                    for (i in matches.indices) {
                        matchmap[matches[i].docId] = i
                    }
                }

                override fun compare(p1: ProfileSearchResult, p2: ProfileSearchResult): Int {

                    return Integer.signum((matchmap[p1.profile.profileId] ?: 0) - (matchmap[p2.profile.profileId] ?: 0))
                }
            }
            if (list != null) {
                Collections.sort(list, comparator)
            }
        }

        @Throws(SphinxException::class)
        private fun setFilter(sphinxClient: SphinxClient, searchIndexField: SearchIndexField, value: Long) {
            if (value > 0) {
                sphinxClient.SetFilter(searchIndexField.label, value, false)
            }

        }

        @Throws(SphinxException::class)
        private fun setFilter(sphinxClient: SphinxClient, searchIndexField: SearchIndexField, enumWithCode: Codable?) {
            if (enumWithCode != null) {
                sphinxClient.SetFilter(searchIndexField.label, enumWithCode.code, false)
            }

        }

        @Throws(SphinxException::class)
        private fun setFilterDateRange(sphinxClient: SphinxClient, fieldDateFrom: SearchIndexField, fieldDateTo: SearchIndexField, dateFrom: Date?, dateTo: Date?) {
            var dateFrom = dateFrom
            var dateTo = dateTo
            if (dateFrom != null || dateTo != null) {
                var timeStart: Long = 0
                if (dateFrom != null) {
                    dateFrom = DateUtils.truncate(dateFrom, Calendar.DATE)
                    timeStart = getTimeWithoutTimeZone(dateFrom)
                }
                var timeFinish = java.lang.Long.MAX_VALUE
                if (dateTo != null) {
                    dateTo = DateUtils.truncate(dateTo, Calendar.DATE)
                    dateTo = DateUtils.addDays(dateTo, 1)
                    timeFinish = getTimeWithoutTimeZone(dateTo) - 1
                }
                if (fieldDateFrom == fieldDateTo) {
                    sphinxClient.SetFilterRange(fieldDateFrom.label, timeStart, timeFinish, false)
                } else {
                    sphinxClient.SetFilterRange(fieldDateFrom.label, 0, timeFinish, false)
                    sphinxClient.SetFilterRange(fieldDateTo.label, timeStart, java.lang.Long.MAX_VALUE, false)
                }
            }
        }

        private val timeZoneOffset = (Calendar.getInstance().get(Calendar.ZONE_OFFSET) + Calendar.getInstance().get(Calendar.DST_OFFSET)).toLong()

        private fun getTimeWithoutTimeZone(date: Date): Long {
            return (date.time + timeZoneOffset) / 1000
        }

        @Throws(SphinxException::class)
        private fun setFilterDateRange(sphinxClient: SphinxClient, field: SearchIndexField, dateFrom: Date?, dateTo: Date?) {
            setFilterDateRange(sphinxClient, field, field, dateFrom, dateTo)

        }

        private fun modifyResultsAfterShareSearch(shareIds: MutableList<Long>, searchShares: SearchShares, promoShareIds: Set<Long>): MutableList<Long> {
            var shareIds = shareIds
            if (CollectionUtils.isEmpty(promoShareIds)) {
                return shareIds
            }

            // condition for promo injection on big search
            if (CollectionUtils.isNotEmpty(searchShares.sortOrderList)
                    && SortOrder.SORT_BY_PURCHASE_COUNT_DESC == searchShares.sortOrderList[0]
                    && searchShares.offset == 0
                    && CollectionUtils.isEmpty(searchShares.campaignTags)
                    && !searchShares.isWelcomeSearch
                    && StringUtils.isEmpty(searchShares.query)
                    && searchShares.priceFrom == 0
                    && searchShares.priceTo == 0) {
                shareIds = substituteResultList(shareIds, promoShareIds, searchShares.limit)  // first page
            }
            return shareIds
        }
    }

}

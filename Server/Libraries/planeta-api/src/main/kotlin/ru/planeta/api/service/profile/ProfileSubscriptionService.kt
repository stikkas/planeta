package ru.planeta.api.service.profile

import ru.planeta.model.profile.Profile
import ru.planeta.model.profile.ProfileRelationShort
import ru.planeta.model.profile.ProfileSubscriptionInfo

import java.util.Date

/**
 * Silly service working only with subscriptions
 */
interface ProfileSubscriptionService {
    fun subscribe(profileId: Long, subjectProfileId: Long)

    fun subscribeSafe(profileId: Long, subjectProfileId: Long)

    fun unsubscribe(profileId: Long, subjectProfileId: Long)

    fun doISubscribeYou(myProfileId: Long, yourProfileId: Long): Boolean

    fun getSubscriptionsCount(profileId: Long): Int

    fun getAdminSubscriptionsCount(profileId: Long): Int

    fun getSubscribersCount(profileId: Long): Long

    fun getAdminSubscribersCount(profileId: Long): Int

    fun getNewSubscribersCount(profileId: Long): Long

    fun getSubscriberList(profileId: Long, query: String?, isAdmin: Boolean?, lastViewSubscribersDate: Date?, offset: Int, limit: Int): List<ProfileSubscriptionInfo>

    fun getSubscriptionList(profileId: Long, query: String?, isAdmin: Boolean?, offset: Int, limit: Int): List<ProfileSubscriptionInfo>

    fun getAdminList(profileId: Long): MutableList<Profile>

    /**
     * change admin status of EXISTING subscription from profileId to subjectProfileId
     * if subscription not exists it will NOT be created
     *
     * @param profileId        admin profile
     * @param subjectProfileId group profile
     * @param isAdmin          - key to toggle
     */
    fun toggleAdmin(profileId: Long, subjectProfileId: Long, isAdmin: Boolean)

    /**
     * checks if profileId is admin of subjectProfileId
     */
    fun isAdmin(profileId: Long, subjectProfileId: Long): Boolean

    fun getProfileRelationShortList(profileId: Long, profileIdList: List<Long>): List<ProfileRelationShort>

}

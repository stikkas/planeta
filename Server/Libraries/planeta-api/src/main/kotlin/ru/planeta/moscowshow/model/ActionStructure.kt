package ru.planeta.moscowshow.model

import java.util.Date
import javax.xml.bind.annotation.XmlAccessType
import javax.xml.bind.annotation.XmlAccessorType
import javax.xml.bind.annotation.XmlRootElement

/**
 *
 * User: Serge Blagodatskih<stikkas17></stikkas17>@gmail.com><br></br>
 * Date: 13.10.16<br></br>
 * Time: 16:40
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
class ActionStructure {

    val actionDate: Date? = null
    val id: Long = 0
    val name: String? = null
    val hall: String? = null
    val location: String? = null
    val rawAction: RawAction? = null

    override fun toString(): String {
        return ("ActionStructure{" + "ActionDate=" + actionDate + ", ID="
                + id + ", Name=" + name + ", Hall=" + hall + ", Location="
                + location + ", rawAction=" + rawAction + '}'.toString())
    }

}

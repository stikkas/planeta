package ru.planeta.moscowshow.model.response

import javax.xml.bind.annotation.XmlAccessType
import javax.xml.bind.annotation.XmlAccessorType
import javax.xml.bind.annotation.XmlRootElement
import ru.planeta.moscowshow.model.result.Result

/**
 * Ответ об оплате билета
 * User: Serge Blagodatskih<stikkas17></stikkas17>@gmail.com><br></br>
 * Date: 13.10.16<br></br>
 * Time: 10:12
 */
@XmlRootElement(name = "payTicketsResponse")
@XmlAccessorType(XmlAccessType.FIELD)
class ResponsePayTickets : Response<Result>() {

    override var result: Result? = null
}


package ru.planeta.api.service.campaign

import ru.planeta.model.common.ModerationMessage
import ru.planeta.model.common.campaign.enums.CampaignStatus

import java.util.EnumSet

/**
 * User: a.savanovich
 * Date: 26.11.13
 * Time: 18:00
 */
interface ModerationMessageService {
    fun getLastCampaignModerationMessage(campaignId: Long): ModerationMessage

    fun getLastCampaignModerationMessageList(profileId: Long, campaignStatuses: EnumSet<CampaignStatus>): List<Map<*, *>>

    fun getAllCampaignModerationMessages(campaignId: Long, offset: Int, limit: Int): List<ModerationMessage>

    fun addModerationMessage(moderationMessage: ModerationMessage)
}

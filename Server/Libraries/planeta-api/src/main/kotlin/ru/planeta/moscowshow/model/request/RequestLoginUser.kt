package ru.planeta.moscowshow.model.request

import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Component
import javax.xml.bind.annotation.XmlAccessType
import javax.xml.bind.annotation.XmlAccessorType
import javax.xml.bind.annotation.XmlRootElement

/**
 *
 * User: Serge Blagodatskih<stikkas17></stikkas17>@gmail.com><br></br>
 * Date: 13.10.16<br></br>
 * Time: 10:09
 */
@Component
@XmlRootElement(name = "loginUser")
@XmlAccessorType(XmlAccessType.FIELD)
class RequestLoginUser {

    @Value("\${moscowshow.login.user:}")
    lateinit var login: String

    @Value("\${moscowshow.login.password:}")
    lateinit var password: String


}

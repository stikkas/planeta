package ru.planeta.api.mail

import com.fasterxml.jackson.databind.ObjectMapper
import org.apache.commons.io.IOUtils
import org.apache.commons.lang3.math.NumberUtils
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import ru.planeta.api.service.configurations.StaticNodesService
import ru.planeta.commons.web.WebUtils
import ru.planeta.dao.profiledb.PhotoDAO
import ru.planeta.model.Constants
import ru.planeta.model.profile.media.Photo

import java.io.IOException
import java.net.URL
import java.nio.charset.StandardCharsets
import java.util.HashMap

/**
 * Date: 07.09.12
 *
 * @author s.kalmykov
 */
@Service
class UploaderServiceImpl(private val staticNodesService: StaticNodesService,
                          private val photoDAO: PhotoDAO) : UploaderService {

    @Throws(IOException::class)
    override fun transferProfilePhoto(photoUrl: String, profileId: Long): Photo? {
        var staticNode = staticNodesService.getStaticNodeOrCurrent(photoUrl)
        staticNode = WebUtils.appendProtocolIfNecessary(staticNode, false)
        val params = WebUtils.Parameters()
                .add("clientId", profileId)
                .add("ownerId", profileId)
                .add("albumId", 0)
                .add("albumTypeId", Constants.ALBUM_AVATAR)
                .add("url", photoUrl)
        val urlString = params.createUrl("$staticNode/transferImage", false)

        val result = IOUtils.toString(URL(urlString), StandardCharsets.UTF_8)
        return getPhoto(result, profileId)
    }

    @Throws(IOException::class)
    private fun getPhoto(result: String, profileId: Long): Photo? {
        try {
            val parsed = ObjectMapper().readValue(result, HashMap::class.java)
            if (parsed["success"] as Boolean) {
                val photoId = NumberUtils.toLong((parsed["result"] as HashMap<*, *>)["photoId"].toString())
                return photoDAO.selectPhotoById(profileId, photoId)
            }
        } catch (ex: Exception) {
            throw IOException("Image transfer parsing failed: $result")
        }

        return null
    }


    @Throws(IOException::class)
    override fun renewBackground(clientId: Long, campaignId: Long): Photo? {
        var staticNode = staticNodesService.staticNode
        staticNode = WebUtils.appendProtocolIfNecessary(staticNode, false)
        val urlString = "$staticNode/backgroundImage"

        val params = WebUtils.Parameters().add("clientId", clientId).add("ownerId", campaignId)
        val result = WebUtils.uploadMap(urlString, params.params) ?: return null
        return getPhoto(result, clientId)
    }
}

package ru.planeta.moscowshow.model

import java.math.BigDecimal
import javax.xml.bind.annotation.XmlAccessType
import javax.xml.bind.annotation.XmlAccessorType
import javax.xml.bind.annotation.XmlRootElement

/**
 * общая для обоих видов заказов (ByStructure/ByBlocks) структура элемента заказа
 * User: Serge Blagodatskih<stikkas17></stikkas17>@gmail.com><br></br>
 * Date: 14.10.16<br></br>
 * Time: 12:29
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
class OrderItem {

    /**
     * ID мероприятия
     */
    var actionID: Long = 0

    /**
     * ID секции
     */
    var sectionID: Long = 0

    /**
     * ID ряда (при заказе без места равен 0).
     */
    var rowID: Long = 0

    /**
     * Оплачен ли билет (при заказе установить в false)
     */
    var isPaid: Boolean = false

    /**
     * Количество мест - всегда 1
     */
    val count = 1

    /**
     * Номер места
     */
    var position: Int = 0

    /**
     * Цена места (установить в стоимость места из описания зала)
     */
    var price: BigDecimal? = null

    constructor()

    constructor(actionId: Long, sectionId: Long, rowId: Long, position: Int, price: BigDecimal) {
        this.actionID = actionId
        this.sectionID = sectionId
        this.rowID = rowId
        this.position = position
        this.price = price
    }
}

package ru.planeta.api.service.profile

import ru.planeta.api.exceptions.NotFoundException
import ru.planeta.api.exceptions.PermissionException
import ru.planeta.api.model.json.ProfileGeneralSettings
import ru.planeta.model.profile.Profile

/**
 * Interface ProfileSettingsService
 *
 * @author a.tropnikov
 */
interface ProfileSettingsService {

    @Throws(NotFoundException::class, PermissionException::class)
    fun saveProfileGeneralSettings(clientId: Long, profileGeneralSettings: ProfileGeneralSettings): Profile

    @Throws(NotFoundException::class, PermissionException::class)
    fun unsubscribeUserFromSpam(clientId: Long, profileId: Long, unsubscribe: Boolean)

    @Throws(PermissionException::class, NotFoundException::class)
    fun unSubscribeUserFromAllMailNotifications(clientId: Long, profileId: Long)

    @Throws(PermissionException::class, NotFoundException::class)
    fun unSubscribeUserFromAllMailNotificationsUnsafe(profileId: Long)
}

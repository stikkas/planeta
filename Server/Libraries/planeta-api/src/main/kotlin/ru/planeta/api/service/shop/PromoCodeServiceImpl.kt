package ru.planeta.api.service.shop

import org.apache.commons.collections4.IterableUtils
import org.apache.commons.collections4.Predicate
import org.apache.commons.lang3.RandomStringUtils
import org.apache.commons.lang3.tuple.Pair
import org.apache.ibatis.exceptions.PersistenceException
import org.apache.log4j.Logger
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import ru.planeta.api.BaseService
import ru.planeta.api.exceptions.NotFoundException
import ru.planeta.dao.shopdb.PromoCodeDAO
import ru.planeta.model.shop.*
import ru.planeta.model.shop.enums.DiscountType
import ru.planeta.model.shop.enums.PromoCodeUsageType
import java.util.*

import ru.planeta.model.shop.enums.PromoCodeUsageType.UNLIMITED

/**
 * Created with IntelliJ IDEA.
 * User: michail
 * Date: 25.03.16
 * Time: 15:23
 */


@Service
class PromoCodeServiceImpl : BaseService(), PromoCodeService {

    @Autowired
    private val promoCodesDAO: PromoCodeDAO? = null

    @Autowired
    private val productService: ProductUsersService? = null

    override fun generateCode(codeLength: Int): String {
        return RandomStringUtils.randomAlphanumeric(codeLength)
    }

    override fun getPromoCodeIdByCode(code: String, alsoDisabled: Boolean): Long {
        val promoCodeId = promoCodesDAO!!.getPromoCodeIdByCode(code, alsoDisabled)
        return promoCodeId ?: 0
    }

    @Throws(NotFoundException::class)
    override fun getPromoCode(code: String): PromoCode {
        return promoCodesDAO!!.getPromoCodeByCode(code) ?: throw NotFoundException(PromoCode::class.java)
    }

    @Throws(NotFoundException::class)
    override fun getPromoCodeById(promoCodeId: Long): PromoCode {
        return promoCodesDAO!!.getPromoCodeById(promoCodeId)
                ?: throw NotFoundException(PromoCode::class.java, promoCodeId)
    }

    override fun getExtendedPromoCodeById(promoCodeId: Long): ExtendedPromoCode {
        return promoCodesDAO!!.getExtendedPromoCodeById(promoCodeId)
    }

    override fun getPromoCodesList(searchString: String, offset: Int, limit: Int): List<PromoCode> {
        return promoCodesDAO!!.getPromoCodesList(searchString, offset, limit)
    }

    override fun getExtendedPromoCodesList(searchString: String, offset: Int, limit: Int): List<ExtendedPromoCode> {
        return promoCodesDAO!!.getExtendedPromoCodesList(searchString, offset, limit)
    }

    override fun getExtendedPromoCodesListCount(searchString: String): Long {
        return promoCodesDAO!!.getExtendedPromoCodesListCount(searchString)
    }

    override fun insertPromoCode(promoCode: PromoCode) {
        promoCode.timeAdded = Date()
        promoCodesDAO!!.insert(promoCode)
    }

    override fun updatePromoCode(promoCode: PromoCode) {
        promoCodesDAO!!.update(promoCode)
    }

    override fun markPromoCodeDisabled(promoCodeId: Long) {
        promoCodesDAO!!.markDisabled(promoCodeId)
    }

    override fun markPromoCodeEnabled(promoCodeId: Long) {
        promoCodesDAO!!.markEnabled(promoCodeId)
    }

    override fun insertOrUpdatePromoCode(promoCode: PromoCode) {
        if (promoCode.promoCodeId > 0) {
            updatePromoCode(promoCode)
        } else {
            insertPromoCode(promoCode)
        }
    }

    override fun generateOne(promoCode: PromoCode) {
        for (errorcount in 0 until ERRCOUNT) {
            promoCode.code = RandomStringUtils.randomAlphanumeric(PromoCode.CODE_LENGTH)
            try {
                insertPromoCode(promoCode)
                return
            } catch (e: Exception) {
                logger.error(e.message)
            }

        }

        throw PersistenceException("Promocode cant be ")
    }

    override fun generateList(promoCode: PromoCode, count: Int) {
        for (i in 0 until count) {
            generateOne(promoCode)
        }
    }

    override fun applyCode(clientId: Long, code: String, cartItems: Map<Long, Int>): Discount {
        try {
            val promoCode = getPromoCode(code)
            return validate(clientId, promoCode, cartItems) ?: Discount()
        } catch (ex: Exception) {
            logger.warn(ex)
            return Discount()
        }

    }

    override fun applyCode(clientId: Long, promoCodeId: Long, cartItems: Map<Long, Int>): Discount {
        try {
            val promoCode = getPromoCodeById(promoCodeId)
            return validate(clientId, promoCode, cartItems) ?: Discount()
        } catch (ex: Exception) {
            logger.error(ex.message)
            return Discount()
        }

    }

    private fun prepeareProperCart(cartItems: Map<Long, Int>, productTag: Long): ArrayList<Pair<Product, Int>> {
        val wellPreparedCart = ArrayList<Pair<Product, Int>>()
        for ((key, value) in cartItems) {
            try {
                val product = productService!!.getProductSafe(key)
                if (productTag == 0L || productHasTag(product, productTag)) {
                    wellPreparedCart.add(Pair.of(product, value))
                }
            } catch (ex: NotFoundException) {
                logger.error("Error while preparing proper cart: $ex")
            }

        }
        return wellPreparedCart
    }

    private fun validate(clientId: Long, promoCode: PromoCode, cartItems: Map<Long, Int>): Discount? {
        var discount: Discount? = null
        logger.info("Processing promocode " + promoCode.promoCodeId)

        val properCart = prepeareProperCart(cartItems, promoCode.productTagId)
        if (properCart.isEmpty()) {
            logger.error("Proper shopping cart is empty")
            return null
        }

        val properOrderPrice = calculateProperCartPrice(properCart)

        if (validateMinOrderPrice(promoCode, properOrderPrice)
                && validateProductCount(promoCode, properCart)
                && validateDate(promoCode)
                && validateUsage(promoCode, clientId)) {
            discount = calculateDiscount(promoCode, properCart, properOrderPrice)
        } else {
            logger.error("Validation failed for promocode " + promoCode.promoCodeId + " Discount is null")
        }

        return discount
    }

    @Throws(NotFoundException::class)
    override fun decreaseUsageCount(promoCodeId: Long) {
        val promoCode = getPromoCodeById(promoCodeId)

        if (promoCode.codeUsageType !== UNLIMITED) {
            decreaseUsageCount(promoCode)
            promoCodesDAO!!.update(promoCode)
        }
    }

    companion object {
        private val logger = Logger.getLogger(PromoCodeServiceImpl::class.java)
        private val ERRCOUNT = 10

        private fun validateProductCount(promoCode: PromoCode, products: List<Pair<Product, Int>>): Boolean {
            val result: Boolean
            var productsInCart: Long = 0

            if (promoCode.minProductCount > 0) {
                val productTag = promoCode.productTagId
                for (cartItem in products) {
                    if (productTag == 0L || productHasTag(cartItem.left, productTag)) {
                        productsInCart += cartItem.right.toLong()
                    }
                }
                result = productsInCart >= promoCode.minProductCount
            } else {
                result = true
            }

            if (!result) {
                logger.warn("Promocode " + promoCode.promoCodeId + " product count validation failed: " +
                        " MinProductCount is " + promoCode.minProductCount +
                        " , actual product count is " + productsInCart)
            }
            return result
        }

        private fun validateMinOrderPrice(promoCode: PromoCode, orderPrice: Long): Boolean {
            val result = promoCode.minOrderPrice <= orderPrice
            if (!result) {
                logger.warn("Promocode " + promoCode.promoCodeId + " min order price validation failed: " +
                        " UsageType is " + promoCode.codeUsageType.toString() +
                        ", MinOrderPrice is " + promoCode.minOrderPrice +
                        ", actual OrderPrice is " + orderPrice)
            }
            return result
        }

        private fun productHasTag(product: Product, tagId: Long): Boolean {
            return IterableUtils.matchesAny(product.getTags()) { `object` -> `object`.categoryId == tagId }
        }

        private fun validateUsage(promoCode: PromoCode, profileId: Long): Boolean {
            val result: Boolean
            when (promoCode.codeUsageType) {
                UNLIMITED -> result = true
                PromoCodeUsageType.LIMITED -> result = promoCode.usageCount > 0
                PromoCodeUsageType.PERSONAL -> result = promoCode.usageCount > 0 && profileId == promoCode.profileId
                PromoCodeUsageType.USERLIMITED ->
                    // now unsopported
                    result = false
                else -> result = false
            }
            if (!result) {
                logger.warn("Promocode " + promoCode.promoCodeId + " usage validation failed: " +
                        " UsageType is " + promoCode.codeUsageType +
                        ", UsageCount is " + promoCode.usageCount +
                        ", ProfileId is " + promoCode.profileId)
            }
            return result
        }

        private fun validateDate(promoCode: PromoCode): Boolean {
            val now = Date()
            val result = promoCode.timeBegin!!.before(now) && promoCode.timeEnd!!.after(now)
            if (!result) {
                logger.warn("Promocode " + promoCode.promoCodeId + " date validation failed: " +
                        " TimeBegin is " + promoCode.timeBegin +
                        ", TimeEnd is " + promoCode.timeEnd +
                        ", now is " + now)
            }
            return result
        }

        private fun calculateDiscount(promoCode: PromoCode, cart: List<Pair<Product, Int>>, orderPrice: Long): Discount? {
            val discount = Discount()

            when (promoCode.discountType) {
                DiscountType.ABSOLUTE -> {
                    val discountAmount = promoCode.discountAmount
                    discount.totalPriceDiscount = if (discountAmount < orderPrice) discountAmount else orderPrice
                    logger.info("Calculating discount for promocode " + promoCode.promoCodeId +
                            ", discount is " + discount.totalPriceDiscount)
                }
                DiscountType.RELATIVE -> {
                    discount.totalPriceDiscount = orderPrice * promoCode.discountAmount / 100
                    logger.info("Calculating discount for promocode " + promoCode.promoCodeId +
                            ", discount is " + discount.totalPriceDiscount)
                }
                DiscountType.PRODUCTFREE -> {
                    var freeProductsCount = promoCode.discountAmount
                    var freeProductsPrice: Long = 0
                    if (freeProductsCount > 0) {
                        Collections.sort(cart) { o1, o2 -> o1.left.price!!.compareTo(o2.left.price!!) }

                        for (cartItem in cart) {
                            var i = 0
                            while (i < cartItem.right && freeProductsCount > 0) {
                                freeProductsCount--
                                freeProductsPrice += cartItem.left.price!!.toLong()
                                i++
                            }
                            if (freeProductsCount == 0L) {
                                discount.totalPriceDiscount = freeProductsPrice
                                break
                            }
                        }
                    }
                    logger.info("Calculating discount for promocode " + promoCode.promoCodeId +
                            ", discount is " + discount.totalPriceDiscount)
                }
                else -> {
                    logger.error("Unsupported discount type.")
                    return null
                }
            }

            if (promoCode.isFreeDelivery) {
                discount.isFreeDelivery = true
            }

            discount.promoCodeId = promoCode.promoCodeId

            return discount
        }

        private fun calculateProperCartPrice(cart: ArrayList<Pair<Product, Int>>): Long {
            var orderPrice: Long = 0
            for (cartItem in cart) {
                orderPrice += cartItem.left.price!!.toLong() * cartItem.right
            }
            return orderPrice
        }

        @Throws(UnsupportedOperationException::class)
        private fun decreaseUsageCount(promoCode: PromoCode) {
            val usageCount = promoCode.usageCount
            if (usageCount > 0) {
                promoCode.usageCount = usageCount - 1
            } else {
                throw UnsupportedOperationException()
            }
        }
    }
}

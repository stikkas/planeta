package ru.planeta.api.service.common

import ru.planeta.model.common.UserLastOnlineTime

interface UserLastOnlineTimeService {
    /**
     * Add(insert) new userLastOnlineTime if there is no such userLastOnlineTime in our DB,
     * if there is one, then this method selects existing UserLoginInfo and updates it.
     *
     * @param userLastOnlineTime     userLastOnlineTime to insert or update
     * @return                       returns <tt>1</tt> if insert/update was successful, otherwise returns <tt>0</tt>
     */
    fun addOrUpdateUserLastOnlineTime(userLastOnlineTime: UserLastOnlineTime): Int


    fun addOrUpdateUserLastOnlineTime(userId: Long): Int

    /**
     * Selects specified userLastOnlineTime according to given profileId
     *
     * @param profileId         user's profileId
     * @return
     */
    fun getUserLastOnlineTime(profileId: Long): UserLastOnlineTime

    fun getUsersLastOnlineTime(profileIds: List<Long>): List<UserLastOnlineTime>
}

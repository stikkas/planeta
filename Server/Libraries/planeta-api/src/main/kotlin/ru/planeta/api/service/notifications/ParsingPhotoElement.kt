package ru.planeta.api.service.notifications

internal class ParsingPhotoElement : ParsingRichElement() {

    override val tag: String
        get() = "photo"

    override fun getImage(paramsString: String, staticNode: String): String {
        return ParsingRichElement.getAttr(paramsString, "image")
    }
}

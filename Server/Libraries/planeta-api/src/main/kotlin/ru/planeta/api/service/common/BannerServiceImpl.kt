package ru.planeta.api.service.common

import org.apache.commons.lang3.time.DateUtils
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import ru.planeta.api.exceptions.NotFoundException
import ru.planeta.api.exceptions.PermissionException
import ru.planeta.api.service.security.PermissionService
import ru.planeta.dao.commondb.BannerDAO
import ru.planeta.model.common.Banner
import ru.planeta.model.enums.BannerStatus
import ru.planeta.model.enums.BannerType
import ru.planeta.model.enums.TargetingType
import java.util.*
import java.util.regex.Pattern

/**
 * User: a.savanovich
 * Date: 18.05.12
 * Time: 11:33
 */
@Service
class BannerServiceImpl(private val bannerDAO: BannerDAO, private val permissionService: PermissionService) : BannerService {

    private var banners: List<Banner>? = null
    private val syncObject = Any()

    private fun getBanners(): List<Banner>? {
        synchronized(syncObject) {
            if (banners == null) {
                loadFromBase()
            }
            return banners
        }
    }

    @Transactional
    @Throws(PermissionException::class)
    override fun save(clientId: Long, banner: Banner) {
        checkPermission(clientId)

        val date = Date()
        if (banner.bannerId == 0L) {
            banner.timeAdded = date
            banner.timeUpdated = date
            bannerDAO.insert(banner)
        } else {
            banner.timeUpdated = date
            bannerDAO.update(banner)
        }
    }

    @Throws(PermissionException::class)
    override fun createFromCampaignId(clientId: Long, campaignId: Long, html: String): Banner {
        val banner = Banner()
        banner.timeStart = Date()
        banner.name = "Campaign$campaignId"
        banner.status = BannerStatus.ON
        banner.timeFinish = DateUtils.addYears(Date(), 1)
        banner.maskView = "*/campaigns/$campaignId"
        banner.type = BannerType.CAMPAIGN_SIDEBAR
        banner.html = html
        banner.showCounter = Integer.MAX_VALUE
        saveWithMaskFormat(clientId, banner)
        return banner
    }

    override fun createSimpleBanner(): Banner {
        val banner = Banner()
        banner.status = BannerStatus.ON
        banner.weight = 1
        banner.timeStart = Date()
        banner.timeFinish = DateUtils.addWeeks(Date(), 1)
        banner.maskView = "*/"
        banner.type = BannerType.LOW_LINE
        banner.showCounter = Integer.MAX_VALUE
        return banner
    }

    @Throws(PermissionException::class)
    override fun createTopBanner(clientId: Long, bannerRaw: Banner): Banner {
        val banner = Banner()
        banner.name = bannerRaw.name
        banner.maskView = bannerRaw.maskView
        banner.html = bannerRaw.html
        banner.status = BannerStatus.ON
        banner.weight = 1
        banner.timeStart = Date()
        banner.timeFinish = DateUtils.addYears(Date(), 1)
        banner.type = bannerRaw.type
        banner.showCounter = Integer.MAX_VALUE
        saveWithMaskFormat(clientId, banner)
        return banner
    }

    @Throws(PermissionException::class)
    private fun checkPermission(clientId: Long) {
        permissionService.checkAdministrativeRole(clientId)
    }

    @Transactional
    @Throws(PermissionException::class)
    override fun copy(clientId: Long, banner: Banner) {
        banner.bannerId = 0
        banner.status = BannerStatus.OFF
        save(clientId, banner)
    }

    @Transactional(readOnly = true)
    @Throws(PermissionException::class)
    override fun reloadFromDb(clientId: Long) {
        checkPermission(clientId)
        loadFromBase()
    }

    private fun loadFromBase() {
        val bannerList = selectActualBanners(0, 0)
        synchronized(syncObject) {
            banners = bannerList
        }
    }

    // @Scheduled(fixedRate = UPDATE_BANNERS_DELAY)
    @Transactional
    override fun reload() {
        if (banners != null) {
            for (banner in banners!!) {
                bannerDAO.setShowCounter(banner.bannerId, banner.showCounter)
            }
        }
        loadFromBase()
    }

    private fun selectActualBanners(offset: Int, limit: Int): List<Banner> {
        val now = Date()
        return bannerDAO.selectList(null, EnumSet.of(BannerStatus.ON), now, now, offset, limit)
    }

    private fun checkAuthorsTargeting(banner: Banner, isAuthor: Boolean): Boolean {
        for (targetingType in banner.targetingType) {
            if (!isAuthor && targetingType === TargetingType.AUTHORS || isAuthor && targetingType === TargetingType.NOT_AUTHORS)
                return false
        }
        return true
    }

    @Transactional(readOnly = true)
    override fun selectRandom(clientId: Long, isAuthor: Boolean, type: BannerType?, previousIds: List<Long>?, requestUrl: String?): Banner? {
        val goodBanners = selectBanners(clientId, isAuthor, type, previousIds, requestUrl)
        return getRandom(goodBanners)
    }


    @Transactional(readOnly = true)
    override fun selectBanners(clientId: Long, isAuthor: Boolean, type: BannerType?, previousIds: List<Long>?, requestUrl: String?): List<Banner> {

        //retrieve
        val banners = getBanners()
        val goodBanners = ArrayList<Banner>()
        //filter
        for (banner in banners!!) {
            if (type != null) {
                if (banner.type == null || banner.type !== type) {
                    continue
                }
            }
            if (requestUrl != null) {
                if (banner.mask == null || !requestUrl.matches((banner.mask ?: "").toRegex())) {
                    continue
                }
            }
            if (previousIds != null) {
                if (previousIds.contains(banner.bannerId)) {
                    continue
                }
            }
            if (banner.showCounter < 1) {
                continue
            }

            if (banner.targetingType != null && !checkAuthorsTargeting(banner, isAuthor)) {
                continue
            }

            goodBanners.add(banner)
        }

        return goodBanners
    }

    @Transactional
    @Throws(PermissionException::class)
    override fun saveWithMaskFormat(clientId: Long, banner: Banner) {
        banner.mask = getMaskFromView(banner.maskView, arrayOf("*"), arrayOf(".*"))
        save(clientId, banner)
    }

    private fun getMaskFromView(maskView: String?, replaceWhat: Array<String>, replaceWith: Array<String>): String {
        var mask: String
        for (i in replaceWhat.indices) {
            val what = replaceWhat[i]
            val with = replaceWith[i]
            //
            var maskParts = maskView!!.split(Pattern.quote(what).toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
            if (maskView.endsWith(what)) {
                maskParts = Arrays.copyOf(maskParts, maskParts.size + 1)
                maskParts[maskParts.size - 1] = ""
            }
            //
            if (maskParts.size > 1) {
                var j = 0
                var part = maskParts[j++]
                mask = getMaskFromView(part, replaceWhat, replaceWith)
                while (j < maskParts.size) {
                    part = maskParts[j++]
                    mask = mask + with + getMaskFromView(part, replaceWhat, replaceWith)
                }
                return mask
            }
        }
        //
        if (maskView != null && maskView.length > 0) {
            mask = Pattern.quote(maskView)
        } else {
            mask = ""
        }
        return mask
    }

    @Transactional(readOnly = true)
    @Throws(NotFoundException::class, PermissionException::class)
    override fun select(clientId: Long, bannerId: Long): Banner {
        checkPermission(clientId)
        return bannerDAO.select(bannerId) ?: throw NotFoundException(Banner::class.java, bannerId)
    }

    @Transactional(readOnly = true)
    @Throws(PermissionException::class)
    override fun selectList(clientId: Long, bannerStatuses: EnumSet<BannerStatus>, offset: Int, limit: Int): List<Banner> {
        checkPermission(clientId)
        return bannerDAO.selectList(bannerStatuses, offset, limit)
    }

    @Transactional
    @Throws(PermissionException::class)
    override fun delete(clientId: Long, bannerId: Long) {
        checkPermission(clientId)
        bannerDAO.delete(bannerId)
    }

    private fun getRandom(bannerList: List<Banner>): Banner? {
        if (bannerList.isEmpty()) {
            return null
        }
        // Compute the total weight of all items together
        var totalWeight = 0
        for (b in bannerList) {
            totalWeight += b.weight
        }
        // Now choose a random item
        var random = Math.random() * totalWeight
        for (b in bannerList) {
            random -= b.weight.toDouble()
            if (random <= 0) {
                b.decreaseShowCounter()
                return b
            }
        }
        return null
    }

}

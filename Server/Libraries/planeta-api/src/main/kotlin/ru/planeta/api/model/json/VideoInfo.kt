package ru.planeta.api.model.json

import com.fasterxml.jackson.annotation.JsonIgnore
import ru.planeta.commons.external.oembed.provider.VideoType
import ru.planeta.commons.model.Gender
import ru.planeta.model.common.CachedVideo
import ru.planeta.model.enums.ObjectType
import ru.planeta.model.enums.PermissionLevel
import ru.planeta.model.enums.ProfileType
import ru.planeta.model.profile.ICommentable
import ru.planeta.model.profile.media.Video
import ru.planeta.model.profile.media.enums.VideoConversionStatus
import ru.planeta.model.profile.media.enums.VideoQuality
import java.util.*

/**
 * Class used to return video data from server
 */
open class VideoInfo : ICommentable {

    @get:JsonIgnore
    var video: Video? = null
    @get:JsonIgnore
    var cachedVideo: CachedVideo? = null

    var videoPageUrl: String? = null
    var relatedVideoInfos: List<VideoRelatedInfo>? = null
    var featuredVideo: VideoInfo? = null

    val html: String?
        get() = if (cachedVideo == null) null else cachedVideo!!.html

    val availableQualities: EnumSet<VideoQuality>?
        get() = if (video == null) null else video!!.availableQualities


    val description: String?
        get() = if (video == null) cachedVideo!!.description else video!!.description

    open val duration: Int
        get() = if (video == null) cachedVideo!!.duration.toInt() else video!!.duration

    val imageId: Long
        get() = if (video == null) 0 else video!!.imageId

    val imageUrl: String?
        get() = if (video == null) cachedVideo!!.thumbnailUrl else video!!.imageUrl

    val storyboardUrl: String?
        get() = if (video == null) null else video!!.storyboardUrl

    val name: String?
        get() = if (video == null) cachedVideo!!.title else video!!.name

    val qualityModesUrls: Map<VideoQuality, String>?
        get() = if (video == null) null else video!!.qualityModesUrls

    val status: VideoConversionStatus?
        get() = if (video == null) VideoConversionStatus.PROCESSED else video!!.status

    val timeAdded: Date?
        get() = if (video == null) cachedVideo!!.timeAdded else video!!.timeAdded

    val timeUpdated: Date?
        get() = if (video == null) cachedVideo!!.timeAdded else video!!.timeUpdated

    val videoId: Long
        get() = if (video == null) 0 else video!!.videoId

    val videoType: VideoType?
        get() = if (video == null) cachedVideo!!.videoType else video!!.videoType

    val videoUrl: String?
        get() = if (video == null) cachedVideo!!.url else video!!.videoUrl

    val viewPermission: PermissionLevel?
        get() = if (video == null) PermissionLevel.EVERYBODY else video!!.viewPermission

    val viewsCount: Int
        get() = if (video == null) 0 else video!!.viewsCount

    val downloadsCount: Int
        get() = if (video == null) 0 else video!!.downloadsCount

    val authorAlias: String?
        get() = if (video == null) null else video!!.authorAlias

    val authorGender: Gender?
        get() = if (video == null) null else video!!.authorGender

    val authorImageUrl: String?
        get() = if (video == null) null else video!!.authorImageUrl

    val authorName: String?
        get() = if (video == null) null else video!!.authorName

    val authorProfileId: Long
        get() = if (video == null) 0 else video!!.authorProfileId

    val commentsCount: Int
        get() = if (video == null) 0 else video!!.commentsCount

    val commentsPermission: PermissionLevel
        get() = if (video == null) PermissionLevel.EVERYBODY else video!!.commentsPermission

    val commentsPermissionCode: Int
        get() = if (video == null) 0 else video!!.commentsPermissionCode

    val ownerAlias: String?
        get() = if (video == null) null else video!!.ownerAlias

    val ownerImageUrl: String?
        get() = if (video == null) null else video!!.ownerImageUrl

    val ownerName: String?
        get() = if (video == null) null else video!!.ownerName

    val ownerProfileType: ProfileType?
        get() = if (video == null) null else video!!.ownerProfileType

    val ownerProfileTypeCode: Int
        get() = if (video == null) 0 else video!!.ownerProfileTypeCode

    override var profileId: Long? = 0
        get() = video?.profileId ?: 0


    override val objectId: Long
        get() = video?.objectId ?: 0

    override var objectType: ObjectType? = ObjectType.VIDEO
        get() = video?.voteObjectType ?: ObjectType.VIDEO


    val tagIds: List<Long>?
        get() = if (video == null) null else video!!.tagIds

    constructor(profileId: Long, videoId: Long) {
        this.video = Video()
        video!!.profileId = profileId
        video!!.videoId = videoId
    }


    constructor(video: Video) {
        this.video = video
    }

    constructor(cachedVideo: CachedVideo?) {
        this.cachedVideo = cachedVideo
    }

    constructor(video: Video, cachedVideo: CachedVideo) {
        this.video = video
        this.cachedVideo = cachedVideo
    }
}

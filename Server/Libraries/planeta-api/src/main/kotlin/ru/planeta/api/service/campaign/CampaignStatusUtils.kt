package ru.planeta.api.service.campaign

import ru.planeta.model.common.campaign.enums.CampaignStatus
import ru.planeta.model.common.campaign.enums.CampaignStatus.*
import java.util.Collections
import java.util.EnumSet
import java.util.HashMap

/**
 * User: s.makarov
 * Date: 24.10.13
 * Time: 20:16
 */
object CampaignStatusUtils {
    private var moderationMap: Map<CampaignStatus, EnumSet<CampaignStatus>>? = null

    init {
        val map = object : HashMap<CampaignStatus, EnumSet<CampaignStatus>>() {
            init {
                put(NOT_STARTED, EnumSet.of(DECLINED, APPROVED, ACTIVE, PATCH, DELETED))
                put(PATCH, EnumSet.of(NOT_STARTED, DECLINED, APPROVED, ACTIVE, DELETED))
                put(DECLINED, EnumSet.of(PATCH))
                put(APPROVED, EnumSet.of(ACTIVE, DECLINED, DELETED))
                put(ACTIVE, EnumSet.of(PAUSED, FINISHED))
                put(PAUSED, EnumSet.of(ACTIVE, FINISHED))
                put(FINISHED, EnumSet.of(ACTIVE))
                put(DRAFT, EnumSet.of(DELETED))
            }
        }
        moderationMap = Collections.unmodifiableMap(map)
    }

    // for js only
    fun getModerationMap(): Map<CampaignStatus, EnumSet<CampaignStatus>> {
        return HashMap(moderationMap!!)
    }

    fun isNextActionVisible(currentAction: CampaignStatus?, nextAction: CampaignStatus): Boolean {
        val currentMap = moderationMap!![currentAction] ?: return false

        return currentMap.contains(nextAction)
    }

    fun isStatusProtected(status: CampaignStatus): Boolean {
        return EnumSet.of<CampaignStatus>(PAUSED, DECLINED).contains(status) || status.isDraft
    }
}

package ru.planeta.api.service.profile

import ru.planeta.api.exceptions.NotFoundException
import ru.planeta.api.exceptions.PermissionException

/**
 *
 * Created by a.savanovich on 05.08.2015.
 */

interface VipClubService {

    /**
     * Toggles user's VIP-flag if it is not already setted.
     *
     * @param clientId client profile identifier;
     * @param userId   VIP-user profile identifier;
     * @param isVip    new VIP-flag's value;
     * @throws PermissionException
     * @throws NotFoundException
     */
    @Throws(PermissionException::class, NotFoundException::class)
    fun changeUserVipStatus(clientId: Long, userId: Long, isVip: Boolean)

    @Throws(NotFoundException::class)
    fun changeUserVipStatus(userId: Long, isVip: Boolean): Boolean

    fun getAllNewVips(offset: Int, limit: Int): List<Long>

}

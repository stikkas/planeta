package ru.planeta.api.model.json

/**
 * @author ds.kolyshev
 * Date: 23.12.11
 */
class PasswordRecoveryChange {

    var regCode: String? = null
    var password: String? = null
    var confirmation: String? = null
}

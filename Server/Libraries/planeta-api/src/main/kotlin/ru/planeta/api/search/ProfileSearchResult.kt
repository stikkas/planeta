package ru.planeta.api.search

import ru.planeta.model.profile.Profile

/**
 *
 * @author s.fionov
 */
class ProfileSearchResult {
    val profile: Profile
    var isSubscribed = false

    @JvmOverloads
    constructor(profile: Profile, subscribed: Boolean = false) {
        this.profile = profile
        this.isSubscribed = subscribed
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as ProfileSearchResult

        if (profile.profileId != other.profile.profileId) return false

        return true
    }

    override fun hashCode(): Int {
        return profile.profileId.hashCode()
    }

}

package ru.planeta.api.service.promo

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import ru.planeta.dao.promo.TechnobattleProjectDAO
import ru.planeta.dao.promo.TechnobattleVoterDAO
import ru.planeta.model.promo.TechnobattleProject

/**
 * User: michail
 * Date: 17.01.2017
 * Time: 12:44
 */
@Service
class TechnobattleProjectServiceImpl @Autowired
constructor(private val technobattleProjectDAO: TechnobattleProjectDAO, private val technobattleVoterDAO: TechnobattleVoterDAO) : TechnobattleProjectService {

    override fun getProject(projectId: Long): TechnobattleProject {
        val project = technobattleProjectDAO.selectOne(projectId)
        if (project != null) {
            project.votesCount = technobattleVoterDAO.selectVotesCountForProject(projectId)
        }

        return project
    }

    override fun getProjects(offset: Int, limit: Int): List<TechnobattleProject> {
        return technobattleProjectDAO.selectList(offset, limit)
    }

    override fun insertOrUpdate(project: TechnobattleProject) {
        if (project.projectId > 0) {
            technobattleProjectDAO.update(project)
        } else {
            technobattleProjectDAO.insert(project)
        }
    }
}

package ru.planeta.moscowshow.client

/**
 *
 * User: Serge Blagodatskih<stikkas17></stikkas17>@gmail.com><br></br>
 * Date: 13.10.16<br></br>
 * Time: 12:39
 */
object SoapAction {

    private const val BASE = "http://moscowshow.handydev.com/"

    /**
     * <h2>Вход в систему</h2>
     * <h3>Аргументы</h3>
     * <dl>
     * <dt>login</dt><dd>Логин</dd>
     * <dt>password</dt><dd>Пароль</dd>
    </dl> *
     * <h3>Возвращает</h3>
     *
     * Результат входа в систему, содержащий ID сессии, который в дальнейшем используется для работы, или описание ошибки в случае сбоя входа в систему.
     */
    const val LOGIN_USER = BASE + "loginUser"

    /**
     * <h2>Выход из системы (в настоящее время выполнять его необязательно)</h2>
     * <h3>Аргументы</h3>
     * <dl>
     * <dt>sessionID</dt><dd>ID сессии</dd>
    </dl> *
     * <h3>Возвращает</h3>
     *
     * Результат выхода или ошибку
     */
    const val LOGOUT_USER = BASE + "logout"

    /**
     * <h2>Получить список мероприятий</h2>
     * <h3>Аргументы</h3>
     * <dl><dt>sessionID</dt><dd>ID сессии</dd></dl>
     * <h2>Возвращает</h2>
     *
     * Список объектов ActionStructure.
     */
    const val ACTION_LIST = BASE + "GetActionListEx"

    /**
     * <h2>Получить фоновую картинку и список мест (со статусом свободно/забронировано) для мероприятия. </h2>
     * <h3>Аргументы</h3>
     * <dl>
     * <dt>sessionID</dt><dd>ID сессии</dd>
     * <dt>actionID</dt><dd>ID мероприятия</dd>
     * <dt>sectionID</dt><dd>ID секции для получения списка мест только из этой секции, или 0 для получения всех мест зала</dd>
     * <dt>forceFullSchema</dt>
     * <dd>При значении true всегда возвращаются все места мероприятия. При значении false при количестве мест более 3000 список мест не возвращается,
     * возвращается только набор секций зала. Для запроса мест в секции нужно повторить вызов с заданием sectionID</dd>
    </dl> *
     * <h3>Возвращает</h3>
     *
     * Структура SchemaResultEx в поле Data результата
     */
    const val ACTION_SCHEMA = BASE + "GetActionSchemaEx"

    /**
     * <h2>Забронировать список мест</h2>
     * <h3>Аргументы</h3>
     * <dl>
     * <dt>sessionID</dt><dd>ID сессии</dd>
     * <dt>orderItems</dt><dd>Список мест</dd>
    </dl> *
     * <h2>Возвращает</h2>
     *
     * Структура OrderResultEx, содержащая список забронированных мест вместе с ID билетов. Если хотя бы одно из мест забронировать не удалось, не бронируется ничего и возвращается описание ошибки.
     */
    const val PLACE_ORDER = BASE + "PlaceOrder"

    /**
     * <h2>Отменить бронировку мест</h2>
     * <h3>Аргументы</h3>
     * <dl>
     * <dt>sessionID</dt><dd>ID сессии</dd>
     * <dt>orderItems</dt><dd>Список мест для отмены брони</dd>
    </dl> *
     * <h2>Возвращает</h2>
     *
     * Структура OrderResult, используется только ErrorCode и ErrorMessage
     */
    const val CANCEL_ORDER = BASE + "cancelOrder"

    /**
     * <h2>Отменить бронировку места по номеру билета (билетов)</h2>
     * <h3>Аргументы</h3>
     * <dl>
     * <dt>sessionID</dt><dd>ID сессии</dd>
     * <dt>orderNumbers</dt><dd>Список номеров билетов</dd>
    </dl> *
     * <h2>Возвращает</h2>
     *
     * Структура OrderResult, используется только ErrorCode и ErrorMessage
     */
    const val CANCEL_ORDER_NUMBERS = BASE + "cancelOrderByOrderNumbers"

    /**
     * <h2>Оплатить билет</h2>
     * <h3>Аргументы</h3>
     * <dl>
     * <dt>sessionID</dt><dd>ID сессии</dd>
     * <dt>ticketID</dt><dd>ID билета</dd>
    </dl> *
     * <h2>Возвращает</h2>
     *
     * Стандартная структура Result
     */
    const val PAY_TICKET = BASE + "payTicket"

    /**
     * <h2>Оплатить билеты</h2>
     * <h3>Аргументы</h3>
     * <dl>
     * <dt>sessionID</dt><dd>ID сессии</dd>
     * <dt>ticketIDs</dt><dd>Список ID билетов</dd>
    </dl> *
     * <h2>Возвращает</h2>
     *
     * Стандартная структура Result
     */
    const val PAY_TICKETS = BASE + "payTickets"

    /**
     * <h2>Отменить оплату билета</h2>
     * <h3>Аргументы</h3>
     * <dl>
     * <dt>sessionID</dt><dd>ID сессии</dd>
     * <dt>ticketID</dt><dd>ID билета</dd>
    </dl> *
     * <h2>Возвращает</h2>
     *
     * Стандартная структура Result
     */
    const val UNPAY_TICKET = BASE + "unpayTicket"

    /**
     * <h2>Отменить оплату билетов</h2>
     * <h3>Аргументы</h3>
     * <dl>
     * <dt>sessionID</dt><dd>ID сессии</dd>
     * <dt>ticketIDs</dt><dd>Список ID билетов</dd>
    </dl> *
     * <h2>Возвращает</h2>
     *
     * Стандартная структура Result
     */
    const val UNPAY_TICKETS = BASE + "unpayTickets"

    /**
     * <h2>Получить список проданных билетов</h2>
     * <h3>Аргументы</h3>
     * <dl>
     * <dt>sessionID</dt><dd>ID сессии</dd>
    </dl> *
     * <h2>Возвращает</h2>
     *
     * Список проданных билетов
     */
    const val SOLD_TICKETS = BASE + "getSoldTickets"
}

package ru.planeta.api.service.common

/**
 * Service managing subscribers and different subscriptions.
 * Very simple for now.
 */
interface SubscribersService {

    /**
     * Subscribes user email for `subscriptionCode`.
     * Ignores, if user already subscribed.
     *
     * @param subscriptionCode
     * @param email
     */
    fun subscribe(subscriptionCode: String, email: String)

    /**
     * Checks is email subscribed to event with code `subscriptionCode`.
     *
     * @param subscriptionCode subscription code;
     * @param email user email;
     * @return `true` if user subscribed, `false` otherwise.
     */
    fun isSubscribed(subscriptionCode: String, email: String): Boolean
}

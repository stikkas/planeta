package ru.planeta.api.service.billing.order

import org.apache.log4j.Logger
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import ru.planeta.api.aspect.transaction.NonTransactional
import ru.planeta.dao.commondb.OrderDAO
import ru.planeta.model.common.Order
import ru.planeta.model.common.campaign.CampaignSearchFilter
import ru.planeta.model.shop.enums.PaymentStatus

/**
 * Created by asavan on 13.04.2017.
 */
@Service
class BatchCancelerServiceImpl : BatchCancelerService {
    @Autowired
    private val orderService: OrderService? = null

    @Autowired
    private val orderDAO: OrderDAO? = null


    @NonTransactional
    override fun batchCancelOrders(clientId: Long, orderIds: List<Long>, reason: String): Int {
        var count = 0
        for (orderId in orderIds) {
            try {
                orderService!!.cancelSharePurchase(clientId, orderId, reason, true)
                ++count
            } catch (ex: Exception) {
                log.error(ex)
            }

        }
        return count
    }

    @NonTransactional
    override fun batchCancelCampaignOrders(clientId: Long, campaignId: Long, reason: String): Int {
        var count = 0

        val searchFilter = CampaignSearchFilter()
        searchFilter.clientId = clientId
        searchFilter.campaignId = campaignId
        searchFilter.offset = 0
        searchFilter.limit = 0
        searchFilter.paymentStatus = PaymentStatus.COMPLETED
        val orders = orderDAO!!.selectFilteredOrdersForCampaign(searchFilter)
        for (order in orders) {
            try {
                orderService!!.cancelSharePurchase(clientId, order.orderId, reason, true)
                ++count
            } catch (ex: Exception) {
                log.error(ex)
            }

        }
        return count
    }

    companion object {

        private val log = Logger.getLogger(BatchCancelerServiceImpl::class.java)
    }
}

package ru.planeta.api.service.biblio

import ru.planeta.model.bibliodb.Bin
import ru.planeta.dao.bibliodb.BinDAO

/**
 * @author stikkas<stikkas></stikkas>@yandex.ru>
 */
interface BinService {

    fun save(bin: Bin, binDAO: BinDAO)

    operator fun get(profileId: Long, binDAO: BinDAO): Bin

    fun clear(bin: Bin, binDAO: BinDAO)
}

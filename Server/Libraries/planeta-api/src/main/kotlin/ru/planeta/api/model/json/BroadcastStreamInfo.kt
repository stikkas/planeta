package ru.planeta.api.model.json

/**
 * Helper class for stream player
 *
 * @author ds.kolyshev
 * Date: 21.03.12
 */
class BroadcastStreamInfo {
    var cover: String? = null
    var host: String? = null
    var stream: String? = null
}

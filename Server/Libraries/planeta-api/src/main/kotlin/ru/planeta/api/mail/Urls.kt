package ru.planeta.api.mail

/**
 * Notification service exported service urls
 *
 * Date: 26.06.12
 *
 * @author s.kalmykov
 */
object Urls {
    val SEND_HTML = "/send.html"
    val SEND_MAIL = "/send-email.html"
}

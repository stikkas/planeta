package ru.planeta.api.service.billing.payment

import ru.planeta.api.exceptions.MessageCode
import ru.planeta.api.exceptions.NotFoundException
import ru.planeta.api.exceptions.PermissionException
import ru.planeta.model.common.Order
import ru.planeta.model.common.TopayTransaction
import ru.planeta.model.enums.PaymentErrorStatus
import ru.planeta.model.filters.PaymentErrorsFilter
import ru.planeta.model.commondb.PaymentErrors
import ru.planeta.model.commondb.PaymentErrorsEx
import ru.planeta.model.commondb.PaymentErrorsWithComments

/**
 * Created by kostiagn on 10.07.2015.
 */

interface PaymentErrorsService {
    fun selectPaymentErrors(paymentErrorId: Long?): PaymentErrors

    fun selectPaymentErrorsExListByFilter(paymentErrorsFilter: PaymentErrorsFilter): List<PaymentErrorsEx>

    fun selectPaymentErrorsWithComments(paymentErrorId: Long?): PaymentErrorsWithComments

    fun selectPaymentErrorsCountByFilter(paymentErrorsFilter: PaymentErrorsFilter): Int

    fun createPaymentErrors(transaction: TopayTransaction)

    fun createPaymentErrors(order: Order, messageCode: MessageCode)

    @Throws(NotFoundException::class, PermissionException::class)
    fun changeStatus(profileId: Long, paymentErrorId: Long, paymentErrorStatus: PaymentErrorStatus)
}

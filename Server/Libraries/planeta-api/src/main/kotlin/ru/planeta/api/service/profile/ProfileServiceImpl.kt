package ru.planeta.api.service.profile

import org.apache.commons.lang3.RandomUtils
import org.apache.commons.lang3.StringUtils
import org.apache.commons.lang3.math.NumberUtils
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service
import ru.planeta.api.BaseService
import ru.planeta.api.exceptions.MessageCode
import ru.planeta.api.exceptions.NotFoundException
import ru.planeta.api.exceptions.PermissionException
import ru.planeta.api.model.json.GroupInfo
import ru.planeta.api.model.json.ProfileInfo
import ru.planeta.api.model.json.UserInfo
import ru.planeta.api.service.security.PermissionService
import ru.planeta.api.text.HtmlValidator
import ru.planeta.commons.model.Gender
import ru.planeta.dao.profiledb.GroupDAO
import ru.planeta.dao.profiledb.PhotoDAO
import ru.planeta.dao.profiledb.UserDAO
import ru.planeta.model.enums.*
import ru.planeta.model.profile.Profile
import ru.planeta.model.profile.User
import java.util.EnumSet

/**
 * Profile service implementation
 *
 * @author ameshkov
 */
@Service
class ProfileServiceImpl(private var userDAO: UserDAO,
                         private var photoDAO: PhotoDAO,
                         private var groupDAO: GroupDAO,
                         private var profileSubscriptionService: ProfileSubscriptionService,
                         @Value("\${static.host}")
                         private var staticHost: String?) : BaseService(), ProfileService {

    override val anonymousProfile: ProfileInfo
        get() {
            val profileInfo = ProfileInfo()
            val profile = Profile()
            profile.profileId = PermissionService.ANONYMOUS_USER_PROFILE_ID
            profile.profileType = ProfileType.USER
            profile.profileTypeCode = ProfileType.USER.code
            profileInfo.profile = profile
            return profileInfo
        }

    /**
     * Gets profile by its identifier.
     */
    override fun getProfile(profileId: Long): Profile? {
        return profileDAO.selectById(profileId)
    }

    override fun getProfileWithDeleted(profileId: Long): Profile {
        return profileDAO.selectWithDeletedById(profileId)
    }

    override fun getProfile(alias: String): Profile? {
        val profileId = NumberUtils.toLong(alias, 0)
        return if (profileId == 0L) {
            profileDAO.selectByAlias(alias)
        } else {
            profileDAO.selectById(profileId)
        }
    }

    @Throws(NotFoundException::class, PermissionException::class)
    override fun <T : ProfileInfo> getProfileInfo(myProfileId: Long, profileId: Long): T {
        val profile = getProfileSafe(profileId)
        return getProfileInfo(myProfileId, profile)
    }

    @Throws(NotFoundException::class, PermissionException::class)
    override fun <T : ProfileInfo> getProfile(myProfileId: Long, alias: String): T {
        val profile = getProfileSafe(alias)
        return getProfileInfo(myProfileId, profile)
    }

    @Deprecated("")
    @Throws(NotFoundException::class, PermissionException::class)
    private fun <T : ProfileInfo> getProfileInfo(clientId: Long, profile: Profile): T {

        val result: ProfileInfo
        val profileId = profile.profileId
        val relationStatus = getProfileRelationStatus(clientId, profile.profileId)

        val hiddenPrivateInfo = !(relationStatus.contains(ProfileRelationStatus.SELF) || relationStatus.contains(ProfileRelationStatus.ADMIN))

        when (profile.profileType) {
            ProfileType.USER -> result = getUserProfile(profile, profileId, clientId, hiddenPrivateInfo)
            ProfileType.GROUP -> result = getGroupProfileInfo(profile, profileId)
            ProfileType.HIDDEN_GROUP -> {
                val creatorProfile = getProfileSafe(profile.creatorProfileId)
                creatorProfile.profileType = ProfileType.HIDDEN_GROUP
                creatorProfile.profileId = profile.profileId
                creatorProfile.creatorProfileId = profile.creatorProfileId
                result = ProfileInfo(creatorProfile)
            }
            else -> throw NotFoundException("Wrong profile type: " + profile.profileType!!)
        }

        if (hiddenPrivateInfo) {
            hideProfilePrivateInfo(profile)
        }
        if (profile.profileType !== ProfileType.HIDDEN_GROUP) {
            result.profile = profile
            result.profileRelationStatus = relationStatus
        }
        return result as T
    }

    private fun hideProfilePrivateInfo(profile: Profile) {
        profile.userGender = Gender.NOT_SET
        profile.cityId = 0
        profile.countryId = 0
        profile.cityName = null
        profile.countryName = null
        profile.userBirthDate = null
        //profile.setSummary(null);
        profile.phoneNumber = null
    }

    private fun getProfileRelationStatus(clientId: Long, profileId: Long): EnumSet<ProfileRelationStatus> {

        val result = EnumSet.noneOf(ProfileRelationStatus::class.java)

        if (profileId == clientId) {
            result.add(ProfileRelationStatus.SELF)
        }

        if (permissionService.hasAdministrativeRole(clientId) || profileSubscriptionService!!.isAdmin(clientId, profileId)) {
            result.add(ProfileRelationStatus.ADMIN)
        }
        if (profileSubscriptionService!!.doISubscribeYou(clientId, profileId)) {
            result.add(ProfileRelationStatus.SUBSCRIPTION)
        }
        if (profileSubscriptionService.doISubscribeYou(profileId, clientId)) {
            result.add(ProfileRelationStatus.SUBSCRIBER)
        }
        if (permissionService.hasNoComment(clientId)) {
            result.add(ProfileRelationStatus.NO_COMMENT)
        }
        if (result.isEmpty()) {
            result.add(ProfileRelationStatus.NOT_SET)
        }

        return result
    }

    @Throws(NotFoundException::class, PermissionException::class)
    override fun relationAddActive(activeProfileId: Long, passiveProfileId: Long) {
        val activeProfile = getProfileSafe(activeProfileId)
        val passiveProfile = getProfileSafe(passiveProfileId)
        if (activeProfile.profileType === ProfileType.USER && (passiveProfile.profileType === ProfileType.USER || passiveProfile.profileType === ProfileType.GROUP)) {
            profileSubscriptionService!!.subscribe(activeProfileId, passiveProfileId)
        }

        if (passiveProfile.profileType === ProfileType.USER && (activeProfile.profileType === ProfileType.USER || activeProfile.profileType === ProfileType.GROUP)) {
            profileSubscriptionService!!.subscribe(passiveProfileId, activeProfileId)
        }
    }


    /**
     * Sets profile avatar
     */
    @Throws(NotFoundException::class, PermissionException::class)
    override fun setProfileAvatar(clientId: Long, profileId: Long, imageId: Long?): Profile {
        permissionService.checkIsAdmin(clientId, profileId)
        val profile = getProfileSafe(profileId)

        val needUpdate = setProfileAvatar(profile, imageId!!)
        if (needUpdate) {
            profileDAO.update(profile)
        }

        return profile
    }

    @Throws(NotFoundException::class)
    private fun setProfileAvatar(profile: Profile, imageId: Long): Boolean {
        val oldImageId = profile.imageId
        if (oldImageId == imageId) {
            return false
        }
        val imageUrl: String?
        if (imageId != 0L) {
            val photo = photoDAO.selectPhotoById(profile.profileId, imageId)
                    ?: throw NotFoundException("Unknown image ID $imageId")

            imageUrl = photo.imageUrl
        } else {
            imageUrl = generateRandomAvatarUrl()
            profile.imageUrl = imageUrl
            profile.smallImageUrl = imageUrl
        }

        profile.imageId = imageId
        profile.imageUrl = imageUrl
        profile.smallImageId = imageId
        profile.smallImageUrl = imageUrl
        return true
    }

    @Throws(NotFoundException::class, PermissionException::class)
    override fun setGroupDisplayNameAndAvatar(clientId: Long, profileId: Long, displayName: String, imageId: Long) {
        permissionService.checkIsAdmin(clientId, profileId)
        val profile = getProfileSafe(profileId)

        val needUpdate = setProfileAvatar(profile, imageId)
        updateDisplayName(displayName, profile, needUpdate)
    }

    @Throws(PermissionException::class)
    private fun updateDisplayName(displayName: String, profile: Profile, needUpdate: Boolean) {
        var needUpdate = needUpdate
        if (StringUtils.isNotBlank(displayName) && displayName != profile.displayName) {
            val newName = HtmlValidator.cleanHtml(displayName)
            if (newName != null && newName.length > 250) {
                throw PermissionException(MessageCode.FIELD_LENGTH_DEFAULT)
            }
            profile.displayName = newName
            needUpdate = true
        }
        if (needUpdate) {
            profileDAO.update(profile)
        }
    }

    @Throws(NotFoundException::class)
    override fun getUser(profileId: Long): User {
        return userDAO.select(profileId) ?: throw NotFoundException(User::class.java, profileId)
    }

    @Throws(PermissionException::class, NotFoundException::class)
    override fun setProfileSmallAvatar(clientId: Long, profileId: Long, imageId: Long): Profile {
        if (!permissionService.isAdmin(clientId, profileId)) {
            throw PermissionException()
        }

        val profile = getProfileSafe(profileId)
        val photo = photoDAO.selectPhotoById(profileId, imageId) ?: throw NotFoundException("Unknown image ID $imageId")

        profile.smallImageId = imageId
        profile.smallImageUrl = photo.imageUrl

        profileDAO.update(profile)
        return profile
    }

    @Throws(NotFoundException::class)
    private fun getUserProfile(profile: Profile, profileId: Long, myProfileId: Long, isHiddenUserPrivateInfo: Boolean): UserInfo {
        val user = getUser(profile.profileId)
        //If unable to view user private info, hide
        if (isHiddenUserPrivateInfo) {
            //profile.setSummary(null);
            user.martialStatus = MartialStatus.NOT_SET
        }

        if (myProfileId != profileId) {
            //hide specified user parameters
            user.usersRequestInCount = 0
            user.usersRequestOutCount = 0
        }
        return UserInfo(user, profile)
    }

    @Throws(NotFoundException::class)
    private fun getGroupProfileInfo(profile: Profile, profileId: Long): GroupInfo {
        val group = groupDAO.selectByProfileId(profileId)
        return GroupInfo(group!!, profile)
    }

    override fun getProfiles(ids: Collection<Long>): List<Profile> {
        return profileDAO.selectSearchedByIds(ids)
    }

    @Throws(NotFoundException::class)
    override fun getProfileSafe(profileId: Long): Profile {
        return getProfile(profileId) ?: throw NotFoundException(Profile::class.java, profileId)
    }

    @Throws(NotFoundException::class)
    override fun getProfileSafe(alias: String): Profile {
        return getProfile(alias) ?: throw NotFoundException("Profile with alias '$alias' not found!")
    }

    @Throws(NotFoundException::class, PermissionException::class)
    override fun getOrCreateHiddenGroup(clientId: Long): Profile {
        val hiddenGroup = profileDAO.getHiddenGroup(clientId)
        if (hiddenGroup != null) {
            return hiddenGroup
        }

        // check this user exist
        getProfileSafe(clientId)

        val profile = Profile()
        profile.status = ProfileStatus.GROUP_ACTIVE
        profile.profileType = ProfileType.HIDDEN_GROUP
        profile.groupCategory = GroupCategory.OTHER
        profile.usersCount = 1
        profile.creatorProfileId = clientId

        profileDAO.insert(profile)
        return profile
    }

    override fun setIsShowBackedCampaigns(myProfileId: Long, isShowBackedCampaigns: Boolean) {
        val profile = profileDAO.selectById(myProfileId)
        if (profile != null) {
            profile.isShowBackedCampaigns = isShowBackedCampaigns
            profileDAO.update(profile)
        }
    }

    @Deprecated("")
    @Throws(PermissionException::class, NotFoundException::class)
    override fun setDisplayName(clientId: Long, profileId: Long, displayName: String) {
        permissionService.checkIsAdmin(clientId, profileId)
        val profile = getProfileSafe(profileId)
        updateDisplayName(displayName, profile, false)
    }

    @Throws(NotFoundException::class)
    override fun unsubscribeProfileFromAuthorSubscription(id: Long) {
        val profile = getProfileSafe(id)
        profile.isReceiveMyCampaignNewsletters = false
        profileDAO.update(profile)
    }

    override fun generateRandomAvatarUrl(): String {
        return String.format("https://%s/images/avatars/ava-u-0%d.jpg", staticHost, RandomUtils.nextInt(1, 7))
    }

    override fun getShopProductsProfiles(tagId: Long, limit: Int): List<Profile> {
        return profileDAO.getShopProductsProfiles(tagId, limit)
    }

    override fun updateNewSubscribersCount(profileId: Long) {
        profileDAO.updateNewSubscribersCount(profileId, profileSubscriptionService!!.getNewSubscribersCount(profileId))
    }
}

package ru.planeta.api.service.biblio

import ru.planeta.model.bibliodb.BookFilter
/*
 * Created by Alexey on 12.04.2016.
 */
import ru.planeta.model.bibliodb.Book
import ru.planeta.model.bibliodb.BookTag

import java.util.Date

interface BookService {

    val allBooks: List<Book>

    val indexBooks: List<Book>

    val countActiveBooks: Long

    fun searchBooks(filter: BookFilter): List<Book>

    fun selectCountSearchBooks(filter: BookFilter): Long

    fun isBooksContainTag(bookIds: List<Long>, bookTagName: String): Boolean

    fun getBookTags(limit: Long, offset: Long): List<BookTag>

    fun saveBook(Book: Book)

    fun deleteBook(bookId: Long?)

    fun getBookById(bookId: Long): Book

    fun getActiveBookById(bookId: Long): Book

    fun getBookTagsByBookId(bookId: Long): List<BookTag>

    fun getBookForChangePrice(date: Date, offset: Long, limit: Long): List<Book>

    fun hasBookRequests(name: String, email: String): Boolean
}

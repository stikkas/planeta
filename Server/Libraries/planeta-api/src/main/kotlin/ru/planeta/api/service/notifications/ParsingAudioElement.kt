package ru.planeta.api.service.notifications

import ru.planeta.commons.web.WebUtils

internal class ParsingAudioElement : ParsingRichElement() {

    override val tag: String
        get() = "audio"

    override fun getImage(paramsString: String, staticNode: String): String {
        return ("$staticNode/audio-stub.jpg?"
                + WebUtils.tryUrlEncodeMap("UTF-8",
                "name", ParsingRichElement.getAttr(paramsString, "name"),
                "artist", ParsingRichElement.getAttr(paramsString, "artist"),
                "duration", ParsingRichElement.getDuration(paramsString),
                "width", "290"
        ))
    }
}

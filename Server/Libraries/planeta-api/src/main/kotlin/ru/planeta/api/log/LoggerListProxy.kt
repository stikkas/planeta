package ru.planeta.api.log

import org.apache.log4j.Logger

/**
 * Propagates logs to list of loggers.
 * @author p.vyazankin
 * @since 6/26/13 4:32 PM
 */
class LoggerListProxy(name: String, vararg loggers: Logger) : Logger(name) {

    private val loggers: Array<out Logger>

    init {

        if (loggers.size == 0) {
            throw IllegalArgumentException("Loggers array is empty.")
        }
        this.loggers = loggers
    }

    override fun debug(message: Any) {
        for (logger in loggers) {
            logger.debug(message)
        }
    }

    override fun debug(message: Any, t: Throwable) {
        for (logger in loggers) {
            logger.debug(message, t)
        }
    }

    override fun error(message: Any) {
        for (logger in loggers) {
            logger.error(message)
        }
    }

    override fun error(message: Any, t: Throwable) {
        for (logger in loggers) {
            logger.error(message, t)
        }
    }

    override fun fatal(message: Any) {
        for (logger in loggers) {
            logger.fatal(message)
        }
    }

    override fun fatal(message: Any, t: Throwable) {
        for (logger in loggers) {
            logger.fatal(message, t)
        }
    }

    override fun info(message: Any) {
        for (logger in loggers) {
            logger.info(message)
        }
    }

    override fun info(message: Any, t: Throwable) {
        for (logger in loggers) {
            logger.info(message, t)
        }
    }

    override fun warn(message: Any) {
        for (logger in loggers) {
            logger.warn(message)
        }
    }

    override fun warn(message: Any, t: Throwable) {
        for (logger in loggers) {
            logger.warn(message, t)
        }
    }

    override fun trace(message: Any) {
        for (logger in loggers) {
            logger.trace(message)
        }
    }

    override fun trace(message: Any, t: Throwable) {
        for (logger in loggers) {
            logger.trace(message, t)
        }
    }
}

package ru.planeta.api.service.profile

import ru.planeta.api.exceptions.NotFoundException
import ru.planeta.api.exceptions.PermissionException
import ru.planeta.model.common.CreditTransaction
import ru.planeta.model.common.DebitTransaction

import java.math.BigDecimal

/**
 * Interface for operations with profile balance
 *
 *
 * User: atropnikov
 * Date: 02.04.12
 * Time: 18:45
 */
interface ProfileBalanceService {

    /**
     * Increase profile balance on (amountNet - amountFee) sum. Also create debit transaction for this operation;
     *
     * @param profileId
     * @param amountNet
     * @param amountFee
     * @param comment
     * @param paymentProviderId
     * @return
     * @throws NotFoundException - if balance not found
     */
    fun increaseProfileBalance(profileId: Long, amountNet: BigDecimal, amountFee: BigDecimal, comment: String,
                               paymentProviderId: Long?, externalSystemData: String): DebitTransaction

    /**
     * Increase profile balance on (amountNet - amountFee) sum. Also create debit transaction for this operation;
     *
     * @param profileId
     * @param amountNet
     * @param amountFee
     * @param comment
     * @return
     * @throws NotFoundException - if balance not found
     */
    fun increaseProfileBalance(profileId: Long, amountNet: BigDecimal, amountFee: BigDecimal, comment: String): DebitTransaction

    /**
     * Decrease profile balance on (amountNet - amountFee) sum. Also create credit transaction for this operation;
     *
     * @param profileId
     * @param amountNet
     * @param amountFee
     * @param comment
     * @return
     * @throws NotFoundException   - if balance not found
     * @throws PermissionException - throw with code NOT_ENOUGH_BALANCE_AMOUNT
     */
    @Throws(PermissionException::class)
    fun decreaseProfileBalance(profileId: Long, amountNet: BigDecimal, amountFee: BigDecimal, comment: String): CreditTransaction

    /**
     * Returns profile balance by id
     *
     * @param profileId
     * @return
     */
    fun getBalance(profileId: Long): BigDecimal

    fun getBalanceForUpdate(profileId: Long): BigDecimal

    fun isEnoughMoney(profileId: Long, amount: BigDecimal): Boolean
}

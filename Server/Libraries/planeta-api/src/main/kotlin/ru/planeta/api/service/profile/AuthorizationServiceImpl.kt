package ru.planeta.api.service.profile

import org.apache.commons.lang3.StringUtils
import org.springframework.stereotype.Service
import ru.planeta.api.aspect.transaction.NonTransactional
import ru.planeta.api.exceptions.MessageCode
import ru.planeta.api.exceptions.NotFoundException
import ru.planeta.api.exceptions.PermissionException
import ru.planeta.api.model.PasswordChange
import ru.planeta.api.model.UserAuthorizationInfo
import ru.planeta.api.service.security.PermissionService
import ru.planeta.commons.web.MxLookup.SMTPMXLookup
import ru.planeta.dao.commondb.UserCredentialsDAO
import ru.planeta.dao.commondb.UserPrivateInfoDAO
import ru.planeta.dao.profiledb.ProfileDAO
import ru.planeta.model.common.UserPrivateInfo
import ru.planeta.model.common.auth.ExternalAuthentication
import ru.planeta.model.common.auth.UserCredentials
import ru.planeta.model.enums.EmailStatus
import ru.planeta.model.enums.ProfileStatus
import java.util.*
import java.util.concurrent.*


// TODO Использовать сервис из пакета ru.planeta.eva.api. По необходимости переносить методы отсюда туда
@Service
class AuthorizationServiceImpl(
        private val profileService: ProfileService,
        private val userCredentialDAO: UserCredentialsDAO,
        protected val userPrivateInfoDAO: UserPrivateInfoDAO,
        private val passwordEncoder: PasswordEncoder,
        private val usernameTransformer: UsernameTransformer,
        private val profileDAO: ProfileDAO,
        private val permissionService: PermissionService) : AuthorizationService {
    private val executor = Executors.newCachedThreadPool()

    @Throws(NotFoundException::class, PermissionException::class)
    override fun checkRegCode(regCode: String, emailUsername: String): UserPrivateInfo {
        val userPrivateInfo = userPrivateInfoDAO!!.selectByRegCode(regCode)

        userPrivateInfo?.timeUpdated!!
        if (!usernameTransformer!!.usernameEquals(emailUsername, userPrivateInfo.username ?: "")) {
            throw PermissionException(MessageCode.REGISTRATION_CURRENT_REGCODE_NOT_EXISTS)
        }
        val calendar = Calendar.getInstance()
        calendar.add(Calendar.DAY_OF_MONTH, -3)
        if (calendar.time.after(userPrivateInfo.timeUpdated!!)) {
            throw PermissionException(MessageCode.REGISTRATION_CURRENT_REGCODE_EXPIRED)
        }
        return userPrivateInfo
    }

    override fun checkPassword(userPrivateInfo: UserPrivateInfo?, inputPassword: String): Boolean {
        return if (StringUtils.isBlank(inputPassword) || userPrivateInfo == null) {
            false
        } else passwordEncoder!!.matches(inputPassword, userPrivateInfo.password ?: "")
    }

    @Throws(PermissionException::class)
    override fun addCredentials(credentials: UserCredentials) {
        val duplicateCredentials = userCredentialDAO!!.select(credentials.userName!!, credentials.credentialType!!)
        if (duplicateCredentials == null) {
            userCredentialDAO.insert(credentials)
        } else {
            throw PermissionException("userCredentials exists: " + credentials.userName!!)
        }
    }


    @Throws(PermissionException::class, NotFoundException::class)
    override fun confirmRegistration(regCode: String, email: String): UserPrivateInfo {
        val userPrivateInfo = checkRegCode(regCode, email)
        setUserActive(userPrivateInfo)
        return userPrivateInfo
    }

    @Throws(NotFoundException::class)
    private fun setUserActive(userPrivateInfo: UserPrivateInfo) {
        val profile = profileService!!.getProfileSafe(userPrivateInfo.userId)
        profile.status = ProfileStatus.USER_ACTIVE
        profileDAO!!.update(profile)
        userPrivateInfo.regCode = null
        userPrivateInfoDAO!!.update(userPrivateInfo)
    }

    @Throws(NotFoundException::class)
    override fun getUserInfoByUsername(username: String): UserAuthorizationInfo? {
        val userPrivateInfo = getUserPrivateInfoByUsername(username) ?: return null
        return getUserAuthorizationInfo(userPrivateInfo)
    }

    override fun getUserPrivateInfoByUsername(email: String): UserPrivateInfo? {
        try {
            val username = usernameTransformer!!.normalize(email)
            return userPrivateInfoDAO!!.selectByUsername(username)
        } catch (e: IllegalArgumentException) {
            return null
        }

    }


    override fun getUserPrivateEmailById(profileId: Long): String? {
        val userPrivateInfo = userPrivateInfoDAO!!.selectByUserId(profileId) ?: return null
        return userPrivateInfo.username
    }

    @NonTransactional
    override fun checkEmail(email: String): EmailStatus {
        if (isEmailExists(email)) {
            return EmailStatus.ALREADY_EXIST
        }

        val task = Callable<Any> {
            if (SMTPMXLookup.isAddressValid(email)) {
                EmailStatus.NEW_VALID_EMAIL
            } else EmailStatus.MAY_BE_NOT_VALID
        }
        val future = executor.submit(task)
        var result = EmailStatus.MAY_BE_NOT_VALID
        try {
            result = future.get(5, TimeUnit.SECONDS) as EmailStatus
        } catch (ex: TimeoutException) {
            // handle the timeout
        } catch (e: InterruptedException) {
            // handle the interrupts
        } catch (e: ExecutionException) {
            // handle other exceptions
        } finally {
            future.cancel(true) // may or may not desire this
        }

        return result

    }

    override fun isEmailExists(email: String?): Boolean {
        return StringUtils.isNotBlank(email) && getUserPrivateInfoByUsername(email ?: "") != null
    }

    @Throws(NotFoundException::class)
    override fun getUserInfo(externalAuthentication: ExternalAuthentication): UserAuthorizationInfo? {
        val selectedCredentials = userCredentialDAO!!.select(externalAuthentication.username!!, externalAuthentication.credentialType!!)
                ?: return null
        val userPrivateInfo = userPrivateInfoDAO!!.selectByUserId(selectedCredentials.profileId)
                ?: throw NotFoundException(UserPrivateInfo::class.java, selectedCredentials.profileId)
        return getUserAuthorizationInfo(userPrivateInfo)
    }


    override fun passwordRecoveryRequest(regCode: String): UserPrivateInfo? {
        return userPrivateInfoDAO!!.selectByRegCode(regCode)
    }

    override fun getUserPrivateInfoById(profileId: Long): UserPrivateInfo? {
        return userPrivateInfoDAO!!.selectByUserId(profileId)
    }

    @Throws(NotFoundException::class)
    override fun passwordRecoveryChangePassword(passwordRecoveryCode: String, password: String): UserPrivateInfo? {
        val userPrivateInfo = userPrivateInfoDAO!!.selectByRegCode(passwordRecoveryCode)

        if (userPrivateInfo != null) {
            userPrivateInfo.password = passwordEncoder!!.encode(password)
            setUserActive(userPrivateInfo)
        }

        return userPrivateInfo
    }

    @Throws(PermissionException::class)
    override fun addSocialNetworkCredentials(myProfileId: Long, externalAuthentication: ExternalAuthentication) {
        val userCredentials = UserCredentials()
        userCredentials.profileId = myProfileId
        userCredentials.credentialType = externalAuthentication.credentialType
        userCredentials.userName = externalAuthentication.username
        addCredentials(userCredentials)
    }


    @Throws(NotFoundException::class)
    private fun getUserAuthorizationInfo(userPrivateInfo: UserPrivateInfo): UserAuthorizationInfo {

        val profile = profileService!!.getProfileSafe(userPrivateInfo.userId)
        val user = profileService.getUser(profile.profileId)

        val userAuthorizationInfo = UserAuthorizationInfo()
        userAuthorizationInfo.profile = profile
        userAuthorizationInfo.user = user
        userAuthorizationInfo.userPrivateInfo = userPrivateInfo
        return userAuthorizationInfo
    }

    @Throws(NotFoundException::class, PermissionException::class)
    override fun saveUserPassword(clientId: Long, change: PasswordChange): UserPrivateInfo {
        permissionService!!.checkIsAdmin(clientId, change.profileId)

        val userPrivateInfo = userPrivateInfoDAO!!.selectByUserId(change.profileId)
                ?: throw NotFoundException(UserPrivateInfo::class.java, change.profileId)

        val newPassword = change.password.trim { it <= ' ' }

        if (!checkPassword(userPrivateInfo, change.currentPassword)) {
            throw PermissionException(MessageCode.WRONG_PASSWORDCHANGE_CURRENTPASSWORD)
        }

        userPrivateInfo.password = passwordEncoder!!.encode(newPassword)
        userPrivateInfoDAO.update(userPrivateInfo)
        return userPrivateInfo
    }

}

package ru.planeta.api.service.content

import ru.planeta.api.exceptions.NotFoundException
import ru.planeta.api.exceptions.PermissionException
import ru.planeta.api.text.Message

/**
 * User: Sergei
 * Date: 22.01.13
 * Time: 13:54
 */
interface SmartFormatterService {

    /**
     * Extracts list of attachments from the specified message text.
     * Ignores links to hosts from "ignoreHosts" parameter.
     *
     * @param messageText
     * @param ignoreHosts
     * @return
     */
    @Throws(NotFoundException::class, PermissionException::class)
    fun extractAttachments(clientId: Long, ownerId: Long, messageText: String, vararg ignoreHosts: String): Message
}

package ru.planeta.eva.api.services

import org.springframework.stereotype.Service
import ru.planeta.dao.profiledb.AudioTrackDAO

@Service
class MediaService(private val audioDAO: AudioTrackDAO) {
    fun getAudio(trackId: Long) = audioDAO.selectAudio(trackId)
}


package ru.planeta.api.model.json

import java.util.regex.Matcher
import java.util.regex.Pattern

/**
 * Created with IntelliJ IDEA.
 * User: user
 * Date: 15.10.13
 * Time: 12:50
 * To change this template use File | Settings | File Templates.
 */
class PremailerResponse {

    var version: String? = null
    var status: Int = 0
    var message: String? = null

    var options: Map<String, String>? = null
    var documents: Map<String, String>? = null

    companion object {
        private val HTML_IMG_SRC_PATTERN = "(?i)(<img([^>]+)>)"

        fun unEscapedAmpersands(html: String): String {
            val p = Pattern.compile(HTML_IMG_SRC_PATTERN)
            val m = p.matcher(html)

            val unEscapeHtml = StringBuffer()

            while (m.find()) {
                val rep = m.group(1)
                m.appendReplacement(unEscapeHtml, rep.replace("&amp;".toRegex(), "&"))
            }
            m.appendTail(unEscapeHtml)
            return unEscapeHtml.toString()
        }
    }
}

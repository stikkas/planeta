package ru.planeta.api.service.comments

import ru.planeta.api.exceptions.NotFoundException
import ru.planeta.api.exceptions.PermissionException
import ru.planeta.model.enums.ObjectType
import ru.planeta.model.profile.Comment

/**
 * Service for comments
 *
 * @author a.savanovich
 * Date: 27.10.11
 */
interface CommentsService {

    /**
     * Gets comments to object (child to the specified parentCommentId).
     * If parentCommentId is 0 that this is root comments.
     *
     * @param ownerId              Commentable object owner
     * @param objectId             Commentable object identifier
     * @param objectType           Object type
     * @param parentCommentId      Parent comment identifier
     * @param includeChildComments If "true" than child comments will be included
     */
    fun getComments(clientId: Long, ownerId: Long, objectId: Long, objectType: ObjectType, parentCommentId: Long, includeChildComments: Int, offset: Int, limit: Int, sort: String): List<Comment>

    /**
     * Select comments after startCommentId
     *
     */
    fun getLastComments(ownerId: Long, objectId: Long, objectType: ObjectType, startCommentId: Long, sort: String, offset: Int, limit: Int): List<Comment>

    /**
     * Adds new comment
     *
     */
    @Throws(NotFoundException::class, PermissionException::class)
    fun addComment(comment: Comment): Comment

    /**
     * Deletes specified comment
     *
     */
    @Throws(NotFoundException::class, PermissionException::class)
    fun deleteComment(clientId: Long, profileId: Long, commentId: Long)

    /**
     * Restore deleteByProfileId comment
     *
     */
    @Throws(NotFoundException::class, PermissionException::class)
    fun restoreComment(clientId: Long, profileId: Long, commentId: Long)


    /**
     * returns the number of comments
     * @param objectId - object id
     * @param objectType - object type
     */
    fun getCommentsCount(objectId: Long, objectType: ObjectType): Int

    fun getCommentsWithChildAsPlain(clientId: Long, profileId: Long, objectId: Long, objectType: ObjectType, offset: Int, limit: Int): List<Comment>
}

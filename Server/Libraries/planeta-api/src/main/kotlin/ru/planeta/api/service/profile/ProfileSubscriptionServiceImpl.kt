package ru.planeta.api.service.profile

import org.apache.log4j.Logger
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import ru.planeta.api.BaseService
import ru.planeta.dao.profiledb.ProfileSubscriptionMapperDAO
import ru.planeta.dao.profiledb.ProfileSubscriptionDAO
import ru.planeta.model.enums.ProfileLastVisitType
import ru.planeta.model.profiledb.ProfileSubscription
import ru.planeta.model.profiledb.ProfileSubscriptionExample
import ru.planeta.model.profile.Profile
import ru.planeta.model.profile.ProfileRelationShort
import ru.planeta.model.profile.ProfileSubscriptionInfo

import java.util.Date

@Service
class ProfileSubscriptionServiceImpl @Autowired
constructor(private val profileSubscriptionMapper: ProfileSubscriptionMapperDAO,
            private val profileSubscriptionDAO: ProfileSubscriptionDAO,
            private val profileLastVisitService: ProfileLastVisitService) : BaseService(), ProfileSubscriptionService {

    override fun subscribe(profileId: Long, subjectProfileId: Long) {
        val profileSubscription = profileSubscriptionMapper.selectByPrimaryKey(profileId, subjectProfileId)
        if (profileSubscription == null) {
            profileSubscriptionMapper.insertSelective(ProfileSubscription.builder
                    .profileId(profileId)
                    .subjectProfileId(subjectProfileId)
                    .timeUpdated(Date())
                    .build())
            updateProfileSubscribersCount(subjectProfileId)
            updateProfileSubscribersCount(profileId)
        }
    }

    private fun updateProfileSubscribersCount(profileId: Long) {
        profileDAO.updateSubscribersCount(profileId)
    }


    override fun subscribeSafe(profileId: Long, subjectProfileId: Long) {
        try {
            subscribe(profileId, subjectProfileId)
        } catch (ex: Exception) {
            log.error("Subscribe fail", ex)
        }

    }

    override fun unsubscribe(profileId: Long, subjectProfileId: Long) {
        val profileSubscription = profileSubscriptionMapper.selectByPrimaryKey(profileId, subjectProfileId)
        if (profileSubscription != null) {
            profileSubscriptionMapper.deleteByPrimaryKey(profileId, subjectProfileId)
            updateProfileSubscribersCount(subjectProfileId)
            updateProfileSubscribersCount(profileId)
        }
    }

    override fun doISubscribeYou(myProfileId: Long, yourProfileId: Long): Boolean {
        return profileSubscriptionMapper.selectByPrimaryKey(myProfileId, yourProfileId) != null
    }

    override fun getSubscriptionsCount(profileId: Long): Int {
        val pse = ProfileSubscriptionExample()
        pse.or().andProfileIdEqualTo(profileId)
        return profileSubscriptionMapper.countByExample(pse)
    }

    override fun getAdminSubscriptionsCount(profileId: Long): Int {
        val pse = ProfileSubscriptionExample()
        pse.or().andProfileIdEqualTo(profileId).andIsAdminEqualTo(true)
        return profileSubscriptionMapper.countByExample(pse)
    }

    override fun getSubscribersCount(profileId: Long): Long {
        return profileSubscriptionDAO.getSubscribersCount(profileId)
    }

    override fun getAdminSubscribersCount(profileId: Long): Int {
        val pse = ProfileSubscriptionExample()
        pse.or().andSubjectProfileIdEqualTo(profileId).andIsAdminEqualTo(true)
        return profileSubscriptionMapper.countByExample(pse)
    }

    override fun getNewSubscribersCount(profileId: Long): Long {
        val lastVisit = profileLastVisitService.selectProfileLastVisit(profileId, ProfileLastVisitType.SUBSCRIBERS_PAGE)
                ?: return getSubscribersCount(profileId)
        return profileSubscriptionDAO.getNewSubscribersCount(profileId, lastVisit)
    }

    override fun getSubscriberList(profileId: Long, query: String?, isAdmin: Boolean?, lastViewSubscribersDate: Date?, offset: Int, limit: Int): List<ProfileSubscriptionInfo> {
        val result = profileSubscriptionDAO.getSubscriberList(profileId, query, isAdmin, lastViewSubscribersDate, offset, limit)
        if (result.size > 1000) {
            log.error("too big SubscriberList " + profileId + " size " + result.size)
        }
        return result
    }

    override fun getSubscriptionList(profileId: Long, query: String?, isAdmin: Boolean?, offset: Int, limit: Int): List<ProfileSubscriptionInfo> {
        return profileSubscriptionDAO.getSubscriptionList(profileId, query, isAdmin, offset, limit)
    }

    override fun getAdminList(profileId: Long): MutableList<Profile> {
        return profileSubscriptionDAO.getSubscriberProfileList(profileId, null, true, null, null, 0, 0)

    }

    override fun toggleAdmin(profileId: Long, subjectProfileId: Long, isAdmin: Boolean) {
        val profileSubscription = profileSubscriptionMapper.selectByPrimaryKey(profileId, subjectProfileId)
        if (profileSubscription != null && (profileSubscription.isAdmin == null || isAdmin != profileSubscription.isAdmin)) {
            profileSubscription.isAdmin = isAdmin
            profileSubscriptionMapper.updateByPrimaryKey(profileSubscription)
        }
    }

    override fun isAdmin(profileId: Long, subjectProfileId: Long): Boolean {
        // exclude "self" admin subscription
        if (profileId == subjectProfileId) {
            return true
        }
        val profileSubscription = profileSubscriptionMapper.selectByPrimaryKey(profileId, subjectProfileId)
        return profileSubscription?.isAdmin == true
    }

    override fun getProfileRelationShortList(profileId: Long, profileIdList: List<Long>): List<ProfileRelationShort> {
        return profileSubscriptionDAO.getProfileRelationShortList(profileId, profileIdList)

    }

    companion object {
        private val log = Logger.getLogger(ProfileSubscriptionServiceImpl::class.java)
    }
}

package ru.planeta.api.service.common

import org.springframework.stereotype.Service
import ru.planeta.dao.commondb.StatsDAO
import ru.planeta.model.common.WelcomeStats

@Service
class StatsServiceImpl(private val statsDAO: StatsDAO) : StatsService {

    override fun selectWelcomeStats(): WelcomeStats {
        return statsDAO.selectWelcomeStats()
    }
}

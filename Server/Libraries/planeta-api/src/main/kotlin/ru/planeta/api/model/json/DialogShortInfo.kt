package ru.planeta.api.model.json

import ru.planeta.commons.model.Gender
import ru.planeta.model.msg.Dialog
import ru.planeta.model.msg.DialogMessage

class DialogShortInfo {
    var dialog: Dialog? = null
    var lastMessage: DialogMessage? = null
    var unreadMessagesCount: Int = 0
    var firstUnreadMessageId: Long = 0
    var companionId: Long = 0
    var companionName: String? = null
    var companionAlias: String? = null
    var companionImageUrl: String? = null
    var companionGender: Gender? = null
}

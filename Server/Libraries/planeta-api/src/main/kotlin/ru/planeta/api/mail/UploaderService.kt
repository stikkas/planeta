package ru.planeta.api.mail

import ru.planeta.model.profile.media.Photo

import java.io.IOException

/**
 * Date: 07.09.12
 *
 * @author s.kalmykov
 */
interface UploaderService {
    /**
     * Transfers external image to planeta uploader
     * @param photoUrl external url, maybe without protocol
     */
    @Throws(IOException::class)
    fun transferProfilePhoto(photoUrl: String, profileId: Long): Photo?

    @Throws(IOException::class)
    fun renewBackground(clientId: Long, campaignId: Long): Photo?
}

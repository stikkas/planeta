package ru.planeta.api.service.content

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import ru.planeta.api.exceptions.PermissionException
import ru.planeta.api.service.security.PermissionService
import ru.planeta.dao.profiledb.ProfileFileDAO
import ru.planeta.model.profile.ProfileFile

/**
 * @author ds.kolyshev
 * Date: 17.03.13
 */
@Service
class ProfileFileServiceImpl @Autowired
constructor(private val profileFileDAO: ProfileFileDAO, protected val permissionService: PermissionService) : ProfileFileService {

    @Throws(PermissionException::class)
    override fun getProfileFiles(clientId: Long, profileId: Long, offset: Int, limit: Int): List<ProfileFile> {
        permissionService.checkIsAdmin(clientId, profileId)
        return profileFileDAO.selectList(profileId, offset, limit)
    }

    @Throws(PermissionException::class)
    override fun getProfileFile(clientId: Long, profileId: Long, fileId: Long): ProfileFile {
        permissionService.checkIsAdmin(clientId, profileId)
        return profileFileDAO.selectById(profileId, fileId)
    }

    @Throws(PermissionException::class)
    override fun addProfileFile(clientId: Long, profileFile: ProfileFile): ProfileFile {
        permissionService.checkIsAdmin(clientId, profileFile.profileId!!)
        return profileFileDAO.insert(profileFile)
    }

    @Throws(PermissionException::class)
    override fun deleteProfileFile(clientId: Long, profileId: Long, fileId: Long) {
        permissionService.checkIsAdmin(clientId, profileId)
        profileFileDAO.delete(profileId, fileId)
    }

    @Throws(PermissionException::class)
    override fun updateProfileFileTitleAndDescription(clientId: Long, profileFile: ProfileFile) {
        permissionService.checkIsAdmin(clientId, profileFile.profileId!!)
        profileFileDAO.updateFileTitleAndDescription(profileFile)
    }

}

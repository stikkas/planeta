package ru.planeta.api.exceptions

import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ResponseStatus

/**
 * Created with IntelliJ IDEA.
 * User: michail
 * Date: 17.10.16
 * Time: 12:39
 */
@ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "Bad request")
class MoscowShowInteractionException(message: String) : BaseException(message)

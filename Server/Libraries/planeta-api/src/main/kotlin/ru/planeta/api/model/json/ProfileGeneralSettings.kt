package ru.planeta.api.model.json

import ru.planeta.commons.model.Gender

class ProfileGeneralSettings {
    var profileId: Long = 0
    var alias: String? = null
    var gender = Gender.NOT_SET
    var userBirthDate: Long? = null
    var cityId: Int = 0
    var countryId: Int = 0
    var phoneNumber: String? = null
    var summary: String? = null
    var isLimitMessages: Boolean = false
    var isReceiveNewsletters: Boolean = false
    var isShowBackedCampaigns: Boolean = false
    var isReceiveMyCampaignNewsletters: Boolean = false
    var displayName: String? = null
}

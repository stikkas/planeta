package ru.planeta.api.service.billing.payment

import ru.planeta.api.exceptions.PermissionException
import ru.planeta.model.commondb.PaymentErrorComments
import ru.planeta.model.commondb.PaymentErrors

/**
 * Created by kostiagn on 17.08.2015.
 */
interface PaymentErrorCommentsService {
    @Throws(PermissionException::class)
    fun insertOrUpdatePaymentErrorComments(clientId: Long, paymentErrorId: Long, text: String)

    fun selectPaymentErrorCommentsList(paymentErrorId: Long?, offset: Int, limit: Int): List<PaymentErrorComments>

}

package ru.planeta.api.service.shop

import org.apache.commons.collections4.CollectionUtils
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import ru.planeta.dao.shopdb.ProductAttributeDAO
import ru.planeta.model.shop.Category
import ru.planeta.model.shop.ProductAttribute

import java.util.*

/**
 * User: m.shulepov
 * Date: 03.09.12
 * Time: 13:06
 */
@Service
class ProductAttributeServiceImpl(private val productAttributeDAO: ProductAttributeDAO,
                                  private val productTagService: ProductTagService) : ProductAttributeService {

    override val tagToAttributesTypeMap: Map<String, List<ProductAttribute>>
        get() {
            val result = HashMap<String, List<ProductAttribute>>()
            for (tag in productTagService.getProductTags(0, 0)) {
                val attributeTypes = productAttributeDAO.selectByType(tag.categoryId)
                if (CollectionUtils.isNotEmpty(attributeTypes)) {
                    result[tag.mnemonicName ?: ""] = attributeTypes
                }
            }

            return result
        }

}

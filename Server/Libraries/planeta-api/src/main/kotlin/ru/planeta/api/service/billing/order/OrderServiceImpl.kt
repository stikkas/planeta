package ru.planeta.api.service.billing.order

import org.apache.commons.collections4.CollectionUtils
import org.apache.commons.lang3.StringUtils
import org.apache.log4j.Logger
import org.springframework.context.MessageSource
import org.springframework.context.annotation.Lazy
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import org.springframework.util.Assert
import ru.planeta.api.BaseService
import ru.planeta.api.Utils
import ru.planeta.api.exceptions.*
import ru.planeta.api.exceptions.MessageCode.*
import ru.planeta.api.log.DBLogger
import ru.planeta.api.log.LoggerListProxy
import ru.planeta.api.log.service.DBLogService
import ru.planeta.api.mail.MailClient
import ru.planeta.api.model.json.DeliveryInfo
import ru.planeta.api.news.LoggerService
import ru.planeta.api.service.billing.payment.PaymentErrorsService
import ru.planeta.api.service.campaign.CampaignService
import ru.planeta.api.service.campaign.DeliveryService
import ru.planeta.api.service.geo.DeliveryAddressService
import ru.planeta.api.service.geo.GeoService
import ru.planeta.api.service.loyalty.BonusService
import ru.planeta.api.service.notifications.NotificationService
import ru.planeta.api.service.profile.ProfileBalanceService
import ru.planeta.api.service.security.PermissionService
import ru.planeta.api.service.shop.ProductUsersService
import ru.planeta.api.utils.Locker
import ru.planeta.api.utils.OrderUtils
import ru.planeta.dao.commondb.CampaignDAO
import ru.planeta.dao.commondb.OrderDAO
import ru.planeta.dao.commondb.OrderObjectDAO
import ru.planeta.dao.profiledb.ProfileDAO
import ru.planeta.dao.shopdb.ProductDAO
import ru.planeta.model.Constants
import ru.planeta.model.common.*
import ru.planeta.model.common.campaign.Campaign
import ru.planeta.model.common.campaign.CampaignSearchFilter
import ru.planeta.model.common.campaign.enums.CampaignStatus
import ru.planeta.model.common.campaign.enums.ShareStatus
import ru.planeta.model.common.delivery.BaseDelivery
import ru.planeta.model.common.delivery.SubjectType
import ru.planeta.model.enums.OrderObjectType
import ru.planeta.model.enums.ProjectType
import ru.planeta.model.news.ProfileNews
import ru.planeta.model.shop.enums.DeliveryStatus
import ru.planeta.model.shop.enums.DeliveryType
import ru.planeta.model.shop.enums.PaymentStatus
import ru.planeta.model.shop.enums.PaymentType
import ru.planeta.model.stat.log.LoggerType
import java.math.BigDecimal
import java.util.*
import javax.annotation.PostConstruct

@Transactional(readOnly = true)
@Service
class OrderServiceImpl(
        private var productUsersService: ProductUsersService,
        private var messageSource: MessageSource,
        private var orderStateVerifier: OrderStateVerifier,
        private var moneyTransactionService: MoneyTransactionService,
        private var orderInfoService: OrderInfoService,
        private var bonusService: BonusService,
        private var dbLogService: DBLogService,
        private var deliveryAddressService: DeliveryAddressService,
        private var paymentErrorsService: PaymentErrorsService,
        private var profileNewsService: LoggerService,
        private var profileBalanceService: ProfileBalanceService,
        private var afterFundServiceFactory: AfterFundServiceFactory,
// TODO remove later
        private var deliveryService: DeliveryService,
        private var geoService: GeoService,
        @Lazy
        private var notificationService: NotificationService,
        private var productDAO: ProductDAO,
        private var orderDAO: OrderDAO,
        private var orderObjectDAO: OrderObjectDAO,
        private var campaignDAO: CampaignDAO,
        @Lazy
        private var campaignService: CampaignService,
        private var profileDAO: ProfileDAO,
        private var permissionService: PermissionService,
        private var mailClient: MailClient) : OrderService {

    private lateinit var logger: LoggerListProxy

    private val profilesLocker = Locker<Long>("profile")

    private val date: Date
        get() {
            val calendar = GregorianCalendar()
            calendar.set(Calendar.DAY_OF_YEAR, 1)
            calendar.set(Calendar.HOUR, 0)
            calendar.set(Calendar.MINUTE, 0)
            calendar.set(Calendar.SECOND, 0)
            calendar.set(Calendar.MILLISECOND, 0)
            return calendar.time
        }

    @PostConstruct
    fun init() {
        logger = LoggerListProxy("orderServiceLogger", Logger.getLogger(OrderServiceImpl::class.java), DBLogger.getLogger(LoggerType.ORDER, dbLogService))
    }

    /**
     * Transfers money from `creditorId` profile to `debtorId` profile (creates credit transaction for creditor, and debit transaction for debtor, with fee and net amounts).<br></br>
     * Identifiers of created transactions sets to transactional object.
     *
     * @param creditorId creditor profile identifier;
     * @param debtorId   debtor profile identifier;
     * @param amountNet  net amount;
     * @param amountFee  fee amount;
     * @param comment    transactions comment;
     * @param object     transactional object;
     * @throws PermissionException
     * @throws NotFoundException
     */
    @Throws(PermissionException::class, NotFoundException::class)
    private fun fund(creditorId: Long, debtorId: Long, amountNet: BigDecimal, amountFee: BigDecimal, comment: String, `object`: TransactionalObject) {
        val creditTransactionId = profileBalanceService.decreaseProfileBalance(creditorId, amountNet, amountFee, comment).transactionId
        val debitTransactionId = profileBalanceService.increaseProfileBalance(debtorId, amountNet, amountFee, comment).transactionId

        `object`.creditTransactionId = creditTransactionId
        `object`.debitTransactionId = debitTransactionId
    }

    /**
     * Checks debit and credit transactions for debtor and creditor by identifiers from transactional object.<br></br>
     * If check is passed then cancels credit transaction.<br></br>
     * Transfers money from `debtorId` profile back to`creditorId` profile (creates credit transaction for debtor, and debit transaction for creditor, with fee and net amounts from cancelled transaction).<br></br>
     * Identifiers of created transactions sets to transactional object.
     *
     * @param creditorId creditor profile identifier;
     * @param debtorId   debtor profile identifier;
     * @param comment    transactions comment;
     * @param object     transactional object;
     * @throws PermissionException
     * @throws NotFoundException
     */
    @Throws(PermissionException::class, NotFoundException::class)
    private fun refund(creditorId: Long, debtorId: Long, comment: String, `object`: TransactionalObject) {
        moneyTransactionService.getDebitTransactionSafe(`object`.debitTransactionId)
        val creditTransaction = moneyTransactionService.getCreditTransactionSafe(`object`.creditTransactionId)
        val amountNet = creditTransaction.amountNet ?: BigDecimal.ZERO
        val creditTransactionId = profileBalanceService.decreaseProfileBalance(debtorId, amountNet, creditTransaction.amountFee!!, comment).transactionId
        val debitTransactionId = profileBalanceService.increaseProfileBalance(creditorId, amountNet, creditTransaction.amountFee!!, comment).transactionId

        `object`.cancelCreditTransactionId = creditTransactionId
        `object`.cancelDebitTransactionId = debitTransactionId
    }

    /**
     * Transfers money from buyer profile, presented in `order`, to each merchant profile of contained order objects.<br></br>
     * At first money transfers to Planeta's profile, and then from Planeta's profile money transfers to each merchant profile.
     *
     * @param order        order
     * @param orderObjects
     * @throws PermissionException
     * @throws NotFoundException
     */
    @Throws(PermissionException::class, NotFoundException::class)
    private fun fund(order: Order, orderObjects: List<OrderObject>) {
        val comment: String
        val merchantId: Long
        if (orderObjects.isEmpty()) {
            if (order.orderType === OrderObjectType.DELIVERY) {
                merchantId = Constants.PLANETA_PROFILE_ID
                comment = formatTransactionComment(DELIVERY, order)
            } else {
                logger.error("No orderObjects in order " + order.orderId)
                return
            }
        } else {
            comment = formatTransactionComment(COMPLETION, order)
            val firstOrderObject = orderObjects[0]
            merchantId = firstOrderObject.merchantId
        }


        fund(order.buyerId, merchantId, order.totalPrice, BigDecimal.ZERO, comment, order)
    }

    /**
     * `refund` is reverse operation to the [.fund].<br></br>
     * At first money transfers from each merchant profile to Planeta's profile, and then from Planeta's profile money transfers to buyer profile.
     *
     * @param order order
     * @throws NotFoundException
     * @throws PermissionException
     */
    @Throws(NotFoundException::class, PermissionException::class)
    private fun refund(order: Order) {
        val orderObjects = getOrderObjects(order.orderId)
        if (orderObjects.isEmpty()) {
            logger.error("No orderObjects in order " + order.orderId)
            return
        }

        val firstOrderObject = orderObjects[0]
        val comment = formatTransactionComment(CANCELLATION, order)
        refund(order.buyerId, firstOrderObject.merchantId, comment, order)
        saveOrder(order)
    }

    @Throws(NotFoundException::class, PermissionException::class)
    private fun refundDeliveryOrder(order: Order) {
        val comment = formatTransactionComment(CANCELLATION, order)
        val merchantId = moneyTransactionService.getDebitTransactionSafe(order.debitTransactionId).profileId
        refund(order.buyerId, merchantId, comment, order)
        saveOrder(order)
    }

    @Throws(PermissionException::class, NotFoundException::class)
    override fun getBuyerOrdersInfo(clientId: Long, buyerId: Long, objectType: OrderObjectType?, dateFrom: Date?, dateTo: Date?, offset: Int, limit: Int): Collection<OrderInfo> {
        verifyAdminPermission(clientId, buyerId)
        val objectTypes: Collection<OrderObjectType>
        if (objectType == null) {
            objectTypes = emptyList()
        } else {
            objectTypes = EnumSet.of(objectType)
        }

        val orders = orderDAO.selectBuyerOrders(buyerId, objectTypes, null, null, dateFrom, dateTo, offset, limit)
        return orderInfoService.getOrdersInfo(orders, true)
    }

    @Throws(PermissionException::class, NotFoundException::class)
    override fun getProfilePurchaseOrders(clientId: Long, query: String?, buyerId: Long, orderObjectTypes: OrderObjectType?, offset: Int, limit: Int): Collection<OrderInfo> {
        verifyAdminPermission(clientId, buyerId)
        val orders = orderDAO.searchPurchasesOrders(query, buyerId, orderObjectTypes, offset, limit)
        return orderInfoService.getOrdersInfo(orders, true)
    }

    @Throws(PermissionException::class, NotFoundException::class)
    override fun getUserOrdersRewardsInfo(buyerId: Long, offset: Int, limit: Int): Collection<OrderInfoForProfilePage> {
        return orderDAO.selectUserOrdersRewardsInfo(buyerId, offset, limit)
    }

    @Throws(PermissionException::class, NotFoundException::class)
    override fun getMerchantOrdersInfo(clientId: Long, merchantId: Long, objectType: OrderObjectType, paymentStatus: PaymentStatus?, deliveryStatus: DeliveryStatus?, dateFrom: Date?, dateTo: Date?, offset: Int, limit: Int): Collection<OrderInfo> {
        if (merchantId == 0L) {
            verifyGlobalManagerPermission(clientId, USER_NOT_ADMIN)
        } else {
            verifyAdminPermission(clientId, merchantId)
        }

        val orders = orderDAO.getMerchantOrdersInfo(merchantId, objectType, paymentStatus, deliveryStatus, dateFrom, dateTo, offset, limit)
        return orderInfoService.getOrdersInfo(orders, true)
    }

    @Throws(NotFoundException::class)
    override fun getOrders(orderIds: Collection<Long>?, orderObjectType: OrderObjectType?, paymentStatus: PaymentStatus?, from: Date?, to: Date?, offset: Int, limit: Int): Collection<OrderInfo> {
        val orders = orderDAO.selectOrders(orderIds, orderObjectType!!, paymentStatus!!, from!!, to!!, offset, limit)
        return orderInfoService.getOrdersInfo(orders, true)
    }

    @Throws(NotFoundException::class)
    override fun getOrders(email: String, orderObjectType: OrderObjectType?, paymentStatus: PaymentStatus?, from: Date?, to: Date?, offset: Int, limit: Int): Collection<OrderInfo> {
        val orders = orderDAO.selectOrders(email, orderObjectType!!, paymentStatus!!, from!!, to!!, offset, limit)
        return orderInfoService.getOrdersInfo(orders, true)
    }

    @Deprecated("")
    @Throws(PermissionException::class, NotFoundException::class)
    private fun getFilteredCampaignOrders(campaignSearchFilter: CampaignSearchFilter): Collection<OrderInfoForReport> {
        if (DB_REQUEST_LIMIT < campaignSearchFilter.limit) {
            logger.warn("getFilteredCampaignOrders: too large limit from client " + campaignSearchFilter.limit)
        }

        val ignoreLimit = campaignSearchFilter.limit == 0
        var itemsRest = campaignSearchFilter.limit    // items to search; used only if !ignoreLimit

        val result = ArrayList<OrderInfoForReport>()


        for (iter in 0 until ERRCOUNT) {
            val currentLimit: Int
            if (ignoreLimit) {
                // use DB_REQUEST_LIMIT and break on empty selectCampaignById
                currentLimit = DB_REQUEST_LIMIT
            } else {
                // use min (itemsRest, DB_REQUEST_LIMIT) and also break on itemsRest = 0
                currentLimit = if (itemsRest < DB_REQUEST_LIMIT) itemsRest else DB_REQUEST_LIMIT
            }
            campaignSearchFilter.limit = currentLimit
            val orders = orderDAO.selectFilteredCampaignOrdersForReport(campaignSearchFilter)

            // cant selectCampaignById from db anymore
            if (orders.isEmpty()) {
                break
            }

            result.addAll(orders)

            // we have limit and need to decrease itemsRest
            if (!ignoreLimit) {
                itemsRest -= currentLimit
                if (itemsRest <= 0) {
                    break
                }
            }

            // we can selectCampaignById more orders from db and we dont reach limit
            campaignSearchFilter.offset = campaignSearchFilter.offset + currentLimit
        }

        return result
    }

    @Throws(PermissionException::class, NotFoundException::class)
    override fun getOrderObjectsInfo(clientId: Long, orderId: Long): Collection<OrderObjectInfo>? {
        val result = orderInfoService.getOrderObjectsInfo(clientId, getOrderObjects(orderId))

        val order = getOrderSafe(orderId)
        if (!permissionService.isAdmin(clientId, order.buyerId)) {
            CollectionUtils.filter(result) { o -> permissionService.isAdmin(clientId, o.merchantId) }
        }
        return result
    }

    @Throws(NotFoundException::class)
    override fun getOrderSafe(orderId: Long): Order {
        return getOrder(orderId) ?: throw NotFoundException(Order::class.java, orderId)
    }

    @Throws(NotFoundException::class)
    override fun getOrderForUpdateSafe(orderId: Long): Order {
        return getOrderForUpdate(orderId) ?: throw NotFoundException(Order::class.java, orderId)
    }

    override fun getOrder(orderId: Long): Order? {
        return orderDAO.select(orderId)
    }

    override fun getOrderForUpdate(orderId: Long): Order? {
        return orderDAO.selectForUpdate(orderId)
    }

    override fun getOrderObjects(orderId: Long): List<OrderObject> {
        return orderObjectDAO.selectOrderObjects(orderId)
    }

    override fun getOrdersCountForObject(objectId: Long, objectType: OrderObjectType, paymentStatuses: EnumSet<PaymentStatus>, deliveryStatuses: EnumSet<DeliveryStatus>?): Int {
        return orderDAO.selectOrdersCountForObject(objectId, objectType, paymentStatuses, deliveryStatuses)
    }

    override fun getMinCompletedOrderId(buyerId: Long, objectType: OrderObjectType): Long {
        return orderDAO.selectMinCompletedOrderId(buyerId, objectType)
    }

    override fun getOrderObject(orderObjectId: Long): OrderObject? {
        return orderObjectDAO.select(orderObjectId)
    }

    override fun assignOrderWithPayment(order: Order, transaction: TopayTransaction) {
        order.paymentType = PaymentType.PAYMENT_SYSTEM
        order.topayTransactionId = transaction.transactionId
        saveOrder(order)
        logger.info(getMessage(ORDER_ASSIGNED_WITH_PAYMENT, order.orderId, transaction.transactionId))
    }

    @Transactional
    @Throws(NotFoundException::class, PermissionException::class)
    override fun addDeliveryAddress(clientId: Long, orderId: Long, deliveryAddress: DeliveryAddress?) {
        if (deliveryAddress == null) {
            return
        }

        verifyGlobalManagerPermission(clientId, USER_NOT_ADMIN)

        //require to throw NotFoundException if order not exist
        val order = getOrderSafe(orderId)

        val deliveryAddressId = deliveryAddressService.getDeliveryAddress(orderId).id
        deliveryAddress.orderId = orderId
        deliveryAddress.id = deliveryAddressId
        deliveryAddressService.saveDeliveryAddress(deliveryAddress)
    }

    @Transactional
    @Throws(PermissionException::class)
    override fun correctBuyerAnswer(clientId: Long, orderId: Long, objectId: Long, answer: String) {
        verifyGlobalManagerPermission(clientId, USER_NOT_ADMIN)
        orderObjectDAO.updateOrderObjectComment(orderId, objectId, answer)
    }

    @Transactional
    @Throws(NotFoundException::class, PermissionException::class, OrderException::class)
    override fun cancelSharePurchase(clientId: Long, orderId: Long, reason: String?, isSendNotification: Boolean) {
        logger.info("cancelSharePurchase started, thread #" + Thread.currentThread().id)
        val order = getOrderForUpdateSafe(orderId)
        logger.info("cancelSharePurchase selected, thread #" + Thread.currentThread().id)
        val orderObjects = getOrderObjects(orderId)
        val shareId = orderObjects.iterator().next().objectId
        val share = campaignService.getShareSafe(shareId)
        val campaign = campaignService.getCampaignSafe(share.campaignId)

        verifyGlobalManagerPermission(clientId, USER_NOT_ADMIN)

        orderStateVerifier.verify(order, orderObjects)

        if (PaymentStatus.CANCELLED === order.paymentStatus) {
            logger.info("cancelSharePurchase error, thread #" + Thread.currentThread().id)
            throw PermissionException(SHARE_PURCHASE_ALREADY_CANCELLED)
        }
        if (PaymentStatus.COMPLETED === order.paymentStatus) {
            refund(order)
        }

        updatePaymentStatus(order, PaymentStatus.CANCELLED)
        updateDeliveryStatus(order, DeliveryStatus.CANCELLED, null, null, null)

        campaignService.updateAfterSharePurchaseCancellation(clientId, campaign.campaignId, CollectionUtils.size(orderObjects), order.totalPrice, shareId)
        logger.info(getMessage(ORDER_CANCELLED, orderId, clientId))
        profileNewsService.addProfileNews(ProfileNews.Type.UPDATE_ORDER_STATUS_CANCEL, clientId, orderId)

        if (isSendNotification) {
            notificationService.sendOrderCancelNotification(order, reason!!)
        }

        logger.info("cancelSharePurchase stopped, thread #" + Thread.currentThread().id)
    }

    @Transactional
    @Throws(PermissionException::class, NotFoundException::class, OrderException::class)
    override fun cancelProductPurchase(clientId: Long, orderId: Long, note: String?, needNotifyUser: Boolean) {
        verifyGlobalManagerPermission(clientId, GLOBAL_ADMIN_CANCEL_ORDER)

        val order = getOrderForUpdateSafe(orderId)
        val orderObjects = getOrderObjects(orderId)
        orderStateVerifier.verify(order, orderObjects)

        if (PaymentStatus.CANCELLED === order.paymentStatus) {
            throw PermissionException(PRODUCT_PURCHASE_ALREADY_CANCELLED)
        }

        val isOrderPaid = PaymentStatus.COMPLETED === order.paymentStatus
        val isPendingDelivery = DeliveryStatus.PENDING === order.deliveryStatus

        if (isOrderPaid) {
            refund(order)
        }
        updateDeliveryStatus(order, if (isPendingDelivery) DeliveryStatus.CANCELLED else DeliveryStatus.REJECTED, null, null, null)
        val isOldStatusIsPending = order.paymentStatus === PaymentStatus.PENDING
        updatePaymentStatus(order, PaymentStatus.CANCELLED)

        if (isOrderPaid || order.paymentType === PaymentType.CASH && isOldStatusIsPending) {
            val storedQuantity = HashMap<Long, Int>()
            for (oo in orderObjects) {
                val productId = oo.objectId
                if (storedQuantity[productId] == null) {
                    storedQuantity[productId] = 1
                } else {
                    storedQuantity[productId] = (storedQuantity[productId] ?: 0) + 1
                }
            }
            for ((key, value) in storedQuantity) {
                productDAO.updateTotalQuantityDelta(key, value.toLong())
            }
        }

        val objectsByProductId = groupOrderObjectsByObjectId(orderObjects)
        for (productId in objectsByProductId.keys) {
            val count = CollectionUtils.size(objectsByProductId[productId])
            if (isOrderPaid) {
                productUsersService.updateProductAfterPurchaseCancellation(order.buyerId, productId, count)
            }
        }

        if (needNotifyUser) {
            notificationService.sendOrderCancelNotification(order, note!!)
        }
        logger.info(getMessage(ORDER_CANCELLED, orderId, clientId))
    }

    @Transactional
    @Throws(PermissionException::class, NotFoundException::class, OrderException::class)
    override fun cancelDeliveryPurchase(clientId: Long, orderId: Long) {
        verifyGlobalManagerPermission(clientId, GLOBAL_ADMIN_CANCEL_ORDER)

        val order = getOrderForUpdateSafe(orderId)
        orderStateVerifier.verify(order, null) // Delivery order has no order_objects

        if (OrderObjectType.DELIVERY !== order.orderType) {
            throw OrderException(NOT_DELIVERY_ORDER)
        }

        if (PaymentStatus.CANCELLED === order.paymentStatus) {
            throw PermissionException(PRODUCT_PURCHASE_ALREADY_CANCELLED)
        }

        val isOrderPaid = PaymentStatus.COMPLETED === order.paymentStatus

        if (isOrderPaid) {
            refundDeliveryOrder(order)
            updatePaymentStatus(order, PaymentStatus.CANCELLED)
            logger.info(getMessage(ORDER_CANCELLED, orderId, clientId))
        } else {
            logger.warn(getMessage(ORDER_NOT_CANCELLED, orderId, clientId))
        }
    }

    @Transactional
    @Throws(PermissionException::class, NotFoundException::class, OrderException::class)
    override fun cancelBiblioPurchase(clientId: Long, orderId: Long) {
        verifyGlobalManagerPermission(clientId, GLOBAL_ADMIN_CANCEL_ORDER)

        val order = getOrderForUpdateSafe(orderId)
        orderStateVerifier.verify(order, getOrderObjects(orderId))

        if (OrderObjectType.BIBLIO !== order.orderType) {
            throw OrderException(NOT_BIBLIO_ORDER)
        }

        if (PaymentStatus.CANCELLED === order.paymentStatus) {
            throw PermissionException(PRODUCT_PURCHASE_ALREADY_CANCELLED)
        }

        val isOrderPaid = PaymentStatus.COMPLETED === order.paymentStatus

        if (isOrderPaid) {
            refund(order)
            updatePaymentStatus(order, PaymentStatus.CANCELLED)
            logger.info(getMessage(ORDER_CANCELLED, orderId, clientId))
        } else {
            logger.warn(getMessage(ORDER_NOT_CANCELLED, orderId, clientId))
        }
    }

    @Transactional
    @Throws(PermissionException::class, NotFoundException::class, OrderException::class)
    override fun updateDeliveryStatus(clientId: Long, order: Order, deliveryStatus: DeliveryStatus, trackingCode: String, cashOnDeliveryCost: BigDecimal, deliveryComment: String) {
        verifyGlobalManagerPermission(clientId, GLOBAL_ADMIN_CHANGE_DELIVERY_STATUS)
        updateDeliveryStatus(order, deliveryStatus, trackingCode, cashOnDeliveryCost, deliveryComment)
    }

    private fun updatePaymentStatus(order: Order, paymentStatus: PaymentStatus) {
        val current = order.paymentStatus
        if (paymentStatus !== current) {
            order.paymentStatus = paymentStatus
            saveOrder(order)
            orderDAO.updateProfileBackedCount(order.buyerId)
            logger.info(getMessage(ORDER_PAYMENT_STATUS_CHANGED, order.orderId, current, paymentStatus))
        } else {
            logger.error("Try to change status to order " + order.orderId + " from " + current + " to " + paymentStatus)
        }
    }

    private fun updateDeliveryStatus(order: Order, deliveryStatus: DeliveryStatus, trackingCode: String?, cashOnDeliveryCost: BigDecimal?, deliveryComment: String?): Boolean {
        var cashOnDeliveryCost = cashOnDeliveryCost
        var changed = false
        val current = order.deliveryStatus
        cashOnDeliveryCost = Utils.nvl(cashOnDeliveryCost, BigDecimal.ZERO)
        if (!(deliveryStatus === current && StringUtils.equals(trackingCode, order.trackingCode) && cashOnDeliveryCost == Utils.nvl(order.cashOnDeliveryCost, BigDecimal.ZERO))) {
            order.trackingCode = trackingCode
            order.cashOnDeliveryCost = cashOnDeliveryCost
            order.deliveryStatus = deliveryStatus
            if (deliveryComment != null) {
                order.deliveryComment = deliveryComment
            }
            saveOrder(order)
            logger.info(getMessage(ORDER_DELIVERY_STATUS_CHANGED, order.orderId, current, deliveryStatus))
            changed = true
        }
        return changed
    }

    @Transactional
    @Throws(NotFoundException::class)
    override fun markOrderAsFailed(orderId: Long, e: BaseException) {
        val messageCode = e.messageCode
        val order = getOrderForUpdateSafe(orderId)
        if (messageCode != null) {
            try {
                val orderInfo = getOrderInfo(order)
                when (messageCode) {
                    CAMPAIGN_NOT_ACTIVE -> mailClient.sendCampaignNotActiveNotificationEmail(orderInfo)
                    NOT_ENOUGH_SHARE_AMOUNT -> mailClient.sendNotEnoughShareAmountNotificationEmail(orderInfo)
                    NOT_ENOUGH_PRODUCT_AMOUNT -> mailClient.sendNotEnoughProductAmountNotificationEmail(orderInfo)
                }
            } catch (collectException: BaseException) {
                logger.warn("Can't collect order info.", collectException)
            }

        }

        order.errorCode = messageCode?.errorPropertyName ?: e.message
        paymentErrorsService.createPaymentErrors(order, messageCode!!)
        updatePaymentStatus(order, PaymentStatus.FAILED)
    }

    @Throws(NotFoundException::class, PermissionException::class, OrderException::class)
    override fun delivery(clientId: Long, orderId: Long, trackingCode: String?, cashOnDeliveryCost: BigDecimal, deliveryComment: String?, isSendNotification: Boolean, deliveryStatus: DeliveryStatus) {
        // TODO: quick fix. for projects need implement another business logic
        //        verifyAdminPermission(clientId, "Only global admin can perform delivery operation.");
        var isSend = false

        val order = getOrderSafe(orderId)
        if (PaymentStatus.COMPLETED !== order.paymentStatus) {
            throwOrderException(ORDER_CAN_NOT_PERFORM, "delivery not paid order", order.orderId, order.paymentStatus)
        }

        if (updateDeliveryStatus(order, deliveryStatus, trackingCode, cashOnDeliveryCost, deliveryComment) && isSendNotification) {
            val orderInfo = getOrderInfo(order)
            if (orderInfo.deliveryAddress?.id == 0L) {
                logger.warn("Delivery for non delivery order. Order id: $orderId")
            }

            if (deliveryStatus === DeliveryStatus.GIVEN) {
                isSend = notificationService.sendOrderGivenNotificationEmail(orderInfo)
            } else if (deliveryStatus === DeliveryStatus.IN_TRANSIT) {
                isSend = notificationService.sendOrderSentNotificationEmail(orderInfo)
            }

            if (!isSend) {
                logger.warn("Order notification sent failed. Order id: $orderId")
            }
        }
    }

    @Transactional
    @Throws(PermissionException::class, NotFoundException::class, OrderException::class)
    override fun purchase(order: Order) {
        Assert.notNull(order, "Order can not be null.")
        logger.info("Purchase order #" + order.orderId + " with payment status " + order.paymentStatus)
        if (!EnumSet.of(PaymentStatus.PENDING, PaymentStatus.RESERVED).contains(order.paymentStatus)) {
            logger.warn("Order # " + order.orderId + " already processed")
            return
        }
        if (order.paymentType === PaymentType.CASH) {
            profileBalanceService.increaseProfileBalance(order.buyerId, order.totalPrice, BigDecimal.ZERO, "Paid by cash.")
        }
        if (!profileBalanceService.isEnoughMoney(order.buyerId, order.totalPrice)) {
            throwOrderException(ORDER_NOT_ENOUGH_MONEY_FOR_PURCHASE, order.orderId, order.buyerId)
        }
        val orderObjects = getOrderObjects(order.orderId)
        orderStateVerifier.verify(order, orderObjects)

        val buyerId = order.buyerId
        profilesLocker.lock(buyerId)
        try {
            fund(order, orderObjects)
            val service = afterFundServiceFactory.get(order.orderType ?: OrderObjectType.SHARE)
            if (service != null) {
                service.purchase(order, orderObjects)
            } else {
                throwOrderException(ORDER_ILLEGAL_STATE, "Unknown order type", order.orderId)
            }

            updatePaymentStatus(order, PaymentStatus.COMPLETED)
        } finally {
            profilesLocker.unlock(buyerId)
        }

        logger.info(getMessage(ORDER_PURCHASE_SUCCESS, order.orderId))
    }

    @Transactional
    @Throws(PermissionException::class, NotFoundException::class, OrderException::class)
    override fun purchase(orderId: Long) {
        purchase(getOrderForUpdateSafe(orderId))
    }

    @Throws(OrderException::class)
    private fun throwOrderException(messageCode: MessageCode, vararg args: Any) {
        throw OrderException(messageCode, getMessage(messageCode, *args))
    }

    @Transactional
    @Throws(PermissionException::class, NotFoundException::class, OrderException::class)
    override fun createOrderWithShare(clientId: Long, buyerId: Long, shareId: Long, donateAmount: BigDecimal?, count: Int,
                                      buyerAnswerToMerchantQuestion: String?, deliveryAddress: DeliveryAddress?, deliveryInfo: DeliveryInfo?, projectType: ProjectType): Order {
        verifyAdminPermission(clientId, buyerId)

        var order = Order()
        val share = campaignService.getShareReadyForPurchase(shareId, count)

        val now = Date()

        val orderObjects = ArrayList<OrderObject>()

        order.buyerId = buyerId
        order.orderType = OrderObjectType.SHARE
        order.timeAdded = now
        order.timeUpdated = now
        order.totalPrice = donateAmount ?: BigDecimal.ZERO
        order.paymentType = PaymentType.NOT_SET
        order.projectType = projectType

        Assert.isTrue(count > 0, "Can not create order with count nested objects less than zero.")

        for (i in 0 until count) {
            val orderObject1 = OrderObject()
            orderObject1.merchantId = share.profileId ?: -1
            orderObject1.objectId = share.shareId
            orderObject1.orderObjectType = order.orderType
            orderObject1.price = share.price
            orderObject1.comment = buyerAnswerToMerchantQuestion
            orderObject1.estimatedDeliveryTime = share.estimatedDeliveryTime
            orderObjects.add(orderObject1)
        }

        if (deliveryInfo != null) {
            order.deliveryPrice = deliveryInfo.deliveryPrice
            order.deliveryType = deliveryInfo.deliveryType
            order.deliveryDepartmentId = deliveryInfo.deliveryDepartmentId
        }

        orderStateVerifier.verify(order, orderObjects)

        var objectsPrice = OrderUtils.calculateOrderObjectsPrice(orderObjects)
        if (order.totalPrice > objectsPrice) {
            objectsPrice = order.totalPrice
        }

        order.totalPrice = objectsPrice.add(order.deliveryPrice)

        storeToDb(order, orderObjects)

        if (order.deliveryType === DeliveryType.CUSTOMER_PICKUP) {
            val subjectType = SubjectType.fromOrderObjectType(order.orderType ?: OrderObjectType.SHARE)
            val objectId = orderObjects.iterator().next().objectId
            val linkedDelivery = deliveryService.getLinkedDelivery(objectId, order.deliveryDepartmentId, subjectType!!)
            val address = linkedDelivery.address

            val phone = if (address?.phone == null) "" else " " + address.phone
            var deliveryAddress = deliveryAddress
            if (deliveryAddress == null) {
                deliveryAddress = DeliveryAddress()
            }
            deliveryAddress.address = (address?.street ?: "") + phone
            deliveryAddress.city = address?.city

            if (address?.country == null || "0" == address?.country) {
                if (address?.countryId != null && address.countryId != 0) {
                    val country = geoService.getCountryById(address.countryId)
                    deliveryAddress.country = country?.name
                } else {
                    deliveryAddress.country = ""
                }
            } else {
                deliveryAddress.country = address.country
            }
            deliveryAddress.zipCode = address?.zipCode
        }

        if (deliveryAddress != null) {
            deliveryAddress.orderId = order.orderId
            deliveryAddressService.saveDeliveryAddress(deliveryAddress)
        }
        val shareStatus = share.shareStatus
        if (null != shareStatus) {
            when (shareStatus) {
                ShareStatus.INVESTING, ShareStatus.INVESTING_ALL_ALLOWED -> order.orderType = OrderObjectType.INVESTING
                ShareStatus.INVESTING_WITHOUT_MODERATION -> order.orderType = OrderObjectType.INVESTING_WITHOUT_MODERATION
                else -> {
                }
            }
        }
        return order
    }

    @Transactional
    override fun storeToDb(order: Order, orderObjects: Collection<OrderObject>) {
        saveOrder(order)
        for (orderObject in orderObjects) {
            orderObject.orderId = order.orderId
            orderObjectDAO.insert(orderObject)
        }
    }

    @Transactional
    @Throws(PermissionException::class, OrderException::class, NotFoundException::class)
    override fun createBonusOrder(buyerId: Long, bonusId: Long, deliveryAddress: DeliveryAddress, deliveryInfo: DeliveryInfo): Order {
        val bonus = bonusService.getBonus(bonusId)
        if (bonus == null || bonus.available < 1) {
            throw OrderException(MessageCode.BONUS_NOT_AVAILABLE)
        }
        var order = Order()
        run {
            val projectType = ProjectType.MAIN
            val deliveryPrice = BigDecimal.ZERO
            val now = Date()
            order.buyerId = buyerId
            order.orderType = OrderObjectType.BONUS
            order.timeAdded = now
            order.timeUpdated = now
            order.paymentType = PaymentType.CASH
            order.projectType = projectType
            order.deliveryPrice = deliveryPrice
            order.deliveryType = deliveryInfo.deliveryType
            order.deliveryDepartmentId = deliveryInfo.deliveryDepartmentId
            val orderObject = OrderObject()
            orderObject.merchantId = Constants.PLANETA_PROFILE_ID
            orderObject.objectId = bonusId
            orderObject.orderObjectType = order.orderType
            orderObject.price = BigDecimal.ZERO
            order.totalPrice = BigDecimal.ZERO
            saveOrder(order)
            orderObject.orderId = order.orderId
            orderObjectDAO.insert(orderObject)

            if (order.deliveryType === DeliveryType.CUSTOMER_PICKUP) {
                val linkedDelivery = deliveryService.getLinkedDelivery(0L, BaseDelivery.DEFAULT_CUSTOMER_PICKUP_SERVICE_ID, SubjectType.SHOP)
                val address = linkedDelivery.address
                deliveryAddress.address = address?.street
                deliveryAddress.phone = address?.phone
                deliveryAddress.city = address?.city
                deliveryAddress.country = address?.country
                deliveryAddress.zipCode = address?.zipCode
            }

            deliveryAddress.orderId = order.orderId
            deliveryAddressService.saveDeliveryAddress(deliveryAddress)
        }
        order.paymentStatus = PaymentStatus.PENDING
        saveOrder(order)
        bonus.available = bonus.available - 1
        bonusService.saveBonus(bonus)

        val orderInfo = getOrderInfo(order)
        mailClient.sendBonusOrderAddedNotifications(orderInfo)

        return order
    }

    @Transactional
    @Throws(PermissionException::class, NotFoundException::class, OrderException::class)
    override fun createDeliveryOrder(clientId: Long, buyerId: Long, amount: BigDecimal, projectType: ProjectType): Order {
        verifyAdminPermission(clientId, buyerId)
        val now = Date()
        var order = Order()
        order.buyerId = buyerId
        order.orderType = OrderObjectType.DELIVERY
        order.timeAdded = now
        order.timeUpdated = now
        order.totalPrice = amount
        order.paymentType = PaymentType.PAYMENT_SYSTEM
        order.projectType = projectType
        saveOrder(order)
        return order
    }

    @Transactional
    @Throws(PermissionException::class, OrderException::class, NotFoundException::class)
    override fun cancelOrderQuietly(clientId: Long, orderId: Long) {
        val order = getOrderSafe(orderId)
        when (order.orderType) {
            OrderObjectType.SHARE -> cancelSharePurchase(clientId, orderId, null, false)
            OrderObjectType.PRODUCT -> cancelProductPurchase(clientId, orderId, null, false)
            OrderObjectType.DELIVERY -> cancelDeliveryPurchase(clientId, orderId)
            OrderObjectType.BONUS -> cancelBonusOrder(clientId, orderId)
            OrderObjectType.BIBLIO -> cancelBiblioPurchase(clientId, orderId)
            else -> {
                val formattedMessage = BaseService.Companion.getMessage(ORDER_UNKNOWN_TYPE, messageSource, order.orderType as Any, orderId)
                throw OrderException(ORDER_UNKNOWN_TYPE, formattedMessage)
            }
        }
    }

    @Throws(PermissionException::class, NotFoundException::class)
    override fun getOrderInfo(clientId: Long, orderId: Long): OrderInfo {
        val order = getOrderSafe(clientId, orderId)

        return getOrderInfo(order)
    }

    @Throws(NotFoundException::class, PermissionException::class)
    override fun getOrderSafe(clientId: Long, orderId: Long): Order {
        val order = getOrderSafe(orderId)
        verifyAdminPermission(clientId, order.buyerId)
        return order
    }

    @Throws(PermissionException::class, NotFoundException::class)
    override fun getOrderInfo(order: Order): OrderInfo {
        return orderInfoService.getOrdersInfo(listOf(order), true).iterator().next()
    }

    /**
     * Verifies `clientId` is admin for `profileId`.
     *
     * @param clientId  client profile identifier;
     * @param profileId validated profile identifier;
     * @throws PermissionException
     */
    @Throws(PermissionException::class)
    private fun verifyAdminPermission(clientId: Long, profileId: Long) {
        permissionService.checkIsAdmin(clientId, profileId)
    }

    @Throws(PermissionException::class)
    private fun verifyGlobalManagerPermission(clientId: Long, errMessage: MessageCode) {
        if (!permissionService.hasAdministrativeRole(clientId)) {
            throw PermissionException(errMessage)
        }
    }

    private fun getMessage(messageCode: MessageCode, vararg args: Any): String {
        return BaseService.Companion.getMessage(messageCode, messageSource, *args)
    }

    @Throws(PermissionException::class, NotFoundException::class)
    override fun getCampaignOrders(campaignSearchFilter: CampaignSearchFilter): Collection<OrderInfoForReport> {
        val campaign = campaignService.getCampaignSafe(campaignSearchFilter.campaignId)

        verifyAdminPermission(campaignSearchFilter.clientId, campaign.profileId!!)

        return getFilteredCampaignOrders(campaignSearchFilter)
    }

    override fun setQuestionToBuyer(orderInfoCollection: Collection<OrderInfo>): Collection<OrderInfo> {
        try {
            for (order in orderInfoCollection) {
                val shareId = order.orderObjectsInfo[0].objectId
                order.questionToBuyer = campaignService.getShareSafe(shareId).questionToBuyer
            }
        } catch (ignored: Exception) {
            logger.warn(ignored)
        }

        return orderInfoCollection
    }

    override fun getBuyerOrdersStatForVipCondition(profileId: Long): Map<String, Any> {
        if (profileId == -1L) {
            return Utils.map("amount", 0, "count", 0)
        }
        val types = EnumSet.of(OrderObjectType.PRODUCT, OrderObjectType.SHARE, OrderObjectType.BOOK, OrderObjectType.LIBRARY)

        var dateFrom: Date = date
        val bonus = orderDAO.selectBuyerLastBonus(profileId, dateFrom)
        if (bonus != null && (bonus.timeAdded?.compareTo(dateFrom) ?: 0) > 0) {
            dateFrom = bonus.timeAdded ?: Date()
        }
        val orders = orderDAO.selectOrdersAfterTime(profileId, types, PaymentStatus.COMPLETED, dateFrom, 0, 0)

        var sum = BigDecimal.ZERO
        for (o in orders) {
            sum = sum.add(o.totalPrice)
        }
        return Utils.map("amount", sum, "count", orders.size, "hasBonuses", bonus != null)
    }

    @Transactional
    @Throws(NotFoundException::class, PermissionException::class)
    override fun cancelBonusOrder(clientId: Long, orderId: Long) {
        verifyGlobalManagerPermission(clientId, USER_NOT_ADMIN)
        val order = getOrderForUpdateSafe(orderId)

        if (PaymentStatus.CANCELLED === order.paymentStatus) {
            throw PermissionException(BONUS_ORDER_ALREADY_CANCELLED)
        }

        order.paymentStatus = PaymentStatus.CANCELLED
        order.deliveryStatus = DeliveryStatus.REJECTED
        val profile = profileDAO.selectById(order.buyerId)
        saveOrder(order)
        if (profile != null) {
            profile.isVip = true
            profileDAO.update(profile)
        }

        val objects = getOrderObjects(orderId)
        for (oo in objects) {
            bonusService.getBonus(oo.objectId)?.let {
                it.available += 1
                bonusService.saveBonus(it)
            }
        }
    }

    @Transactional
    @Throws(NotFoundException::class, PermissionException::class)
    override fun completeBonusOrder(clientId: Long, orderId: Long) {
        verifyGlobalManagerPermission(clientId, USER_NOT_ADMIN)
        val order = getOrderForUpdateSafe(orderId)
        order.paymentStatus = PaymentStatus.COMPLETED
        order.deliveryStatus = DeliveryStatus.IN_TRANSIT
        saveOrder(order)
    }

    @Transactional
    @Throws(PermissionException::class, NotFoundException::class, OrderException::class)
    override fun changeOrderStatus(clientId: Long, orderId: Long, deliveryStatus: DeliveryStatus, sendNotification: Boolean,
                                   note: String, trackingCode: String, cashOnDeliveryCost: BigDecimal,
                                   deliveryComment: String) {

        val formattedMessage: String
        if (deliveryStatus === DeliveryStatus.PENDING) {
            formattedMessage = BaseService.Companion.getMessage(ORDER_ILLEGAL_STATE, messageSource, "Cant change order status to $deliveryStatus", orderId)
            throw OrderException(ORDER_ILLEGAL_STATE, formattedMessage)
        }

        permissionService.checkAdministrativeRole(clientId)

        val order = getOrderSafe(orderId)
        if (order.deliveryStatus !== deliveryStatus) {
            when (deliveryStatus) {
                DeliveryStatus.IN_TRANSIT -> {
                    if (order.deliveryStatus !== DeliveryStatus.PENDING) {
                        formattedMessage = BaseService.Companion.getMessage(ORDER_ILLEGAL_STATE, messageSource, "Order delivery status is " + order.deliveryStatus, order.orderId)
                        throw OrderException(ORDER_ILLEGAL_STATE, formattedMessage)
                    }

                    updateDeliveryStatus(clientId, order, DeliveryStatus.IN_TRANSIT, trackingCode, cashOnDeliveryCost, deliveryComment)
                    if (sendNotification) {
                        notificationService.sendOrderSentNotificationEmail(getOrderInfo(order))
                    }
                }
                DeliveryStatus.DELIVERED, DeliveryStatus.GIVEN -> {
                    if (order.paymentType === PaymentType.CASH) {
                        purchase(order)
                    }
                    delivery(clientId, orderId, trackingCode, cashOnDeliveryCost, deliveryComment, sendNotification, deliveryStatus)
                    profileNewsService.addProfileNews(ProfileNews.Type.UPDATE_ORDER_STATUS_GIVEN, clientId, orderId)
                }
                DeliveryStatus.REJECTED, DeliveryStatus.CANCELLED -> cancelProductPurchase(clientId, orderId, note, sendNotification)
                else -> {
                    formattedMessage = BaseService.Companion.getMessage(ORDER_ILLEGAL_STATE, messageSource, "Order delivery status is " + order.deliveryStatus, order.orderId)
                    throw OrderException(ORDER_ILLEGAL_STATE, formattedMessage)
                }
            }

        } else {
            order.trackingCode = trackingCode
            order.cashOnDeliveryCost = cashOnDeliveryCost
            order.deliveryComment = deliveryComment
            saveOrder(order)

            if (sendNotification) {
                val orderInfo = getOrderInfo(order)
                when (deliveryStatus) {
                    DeliveryStatus.IN_TRANSIT -> notificationService.sendOrderSentNotificationEmail(orderInfo)
                    DeliveryStatus.DELIVERED, DeliveryStatus.GIVEN -> {
                        var isSend = false
                        if (deliveryStatus === DeliveryStatus.GIVEN) {
                            isSend = notificationService.sendOrderGivenNotificationEmail(orderInfo)
                        } else if (deliveryStatus === DeliveryStatus.DELIVERED) {
                            isSend = notificationService.sendOrderSentNotificationEmail(orderInfo)
                        }
                        if (!isSend) {
                            logger.warn("Order notification sent failed. Order id: $orderId")
                        }
                    }
                    DeliveryStatus.REJECTED, DeliveryStatus.CANCELLED -> {
                        notificationService.sendOrderCancelNotification(order, note)
                        logger.info(getMessage(ORDER_CANCELLED, orderId, clientId))
                    }
                    else -> {
                        formattedMessage = BaseService.Companion.getMessage(ORDER_ILLEGAL_STATE, messageSource, "Order delivery status is " + order.deliveryStatus, order.orderId)
                        throw OrderException(ORDER_ILLEGAL_STATE, formattedMessage)
                    }
                }
            }
        }
    }

    @Throws(NotFoundException::class)
    override fun getCampaignByOrderId(orderId: Long): Campaign {
        return campaignDAO.selectCampaignByOrder(orderId) ?: throw NotFoundException(Campaign::class.java)
    }

    override fun isProfileHasPurchasedObject(profileId: Long, objectId: Long, orderObjectType: OrderObjectType): Boolean {
        return orderObjectDAO.isProfileHasPurchasedObject(profileId, objectId, orderObjectType)
    }

    private fun saveOrder(order: Order) {
        order.timeUpdated = Date()
        if (order.orderId == null) {
            order.timeAdded = Date()
            orderDAO.insert(order)
        } else {
            orderDAO.update(order)
        }
    }

    companion object {

        private val CANCELLATION = "cancellation"
        private val COMPLETION = "completion"
        private val DELIVERY = "delivery"
        val ERRCOUNT = 1000 * 1000 // 1M orders
        val DB_REQUEST_LIMIT = 50  // limit for db queries

        private fun formatTransactionComment(action: String, order: Order): String {
            return String.format("Order %s: orderId[%d];", action, order.orderId)
        }

        private val MAP_PARAM_CAMPAIGN_STATUS_ACTIVE = object : HashMap<String, CampaignStatus>() {
            init {
                put("campaignStatus", CampaignStatus.ACTIVE)
            }
        }

        @Throws(NotFoundException::class, PermissionException::class)
        private fun groupOrderObjectsByObjectId(orderObjects: List<OrderObject>): Map<Long, Set<OrderObject>> {
            val result = HashMap<Long, HashSet<OrderObject>>()
            for (orderObject in orderObjects) {
                val objectSet = getCachedObject(orderObject.objectId, result,
                        object : BaseService.Getter<HashSet<OrderObject>> {
                            override fun get(objectId: Long): HashSet<OrderObject> {
                                return HashSet()
                            }
                        })
                objectSet?.add(orderObject)
            }

            return result
        }

        @Throws(NotFoundException::class)
        private fun <T> getCachedObject(objectId: Long, cache: MutableMap<Long, T>, getter: BaseService.Getter<T>): T? {
            if (!cache.containsKey(objectId)) {
                cache[objectId] = getter[objectId]
            }
            return cache[objectId]
        }
    }

}

package ru.planeta.api.service.common

import ru.planeta.api.exceptions.PermissionException
import ru.planeta.api.model.json.UserCallbackInfo
import ru.planeta.model.common.UserCallback
import ru.planeta.model.common.UserCallbackComment
import ru.planeta.model.enums.OrderObjectType
import java.math.BigDecimal
import java.util.Date

/**
 *
 * Created by eshevchenko on 02.02.15.
 */
interface UserCallbackService {

    fun createFailurePaymentCallback(userId: Long, userPhone: String, paymentId: Long?, amount: BigDecimal): UserCallback

    fun createOrderingProblemCallback(userId: Long, userPhone: String, orderType: OrderObjectType, objectId: Long?, amount: BigDecimal): UserCallback

    @Throws(PermissionException::class)
    fun processUserCallback(managerId: Long, callbackId: Long): UserCallbackInfo

    @Throws(PermissionException::class)
    fun getProcessedCallbacks(clientId: Long, type: UserCallback.Type, dateFrom: Date, dateTo: Date, offset: Int, limit: Int): List<UserCallbackInfo>

    @Throws(PermissionException::class)
    fun getUnprocessedCallbacks(clientId: Long, type: UserCallback.Type, dateFrom: Date, dateTo: Date, offset: Int, limit: Int): List<UserCallbackInfo>

    @Throws(PermissionException::class)
    fun getAllCallbacks(clientId: Long, type: UserCallback.Type, processed: Boolean?, dateFrom: Date, dateTo: Date, offset: Int, limit: Int): List<UserCallbackInfo>

    fun isCallbacksAvailable(amount: BigDecimal): Boolean

    @Throws(PermissionException::class)
    fun getCallback(clientId: Long, callbackId: Long): UserCallback

    @Throws(PermissionException::class)
    fun getCallbackComments(clientId: Long, callbackId: Long): List<UserCallbackComment>

    @Throws(PermissionException::class)
    fun addCallbackComment(clientId: Long, callbackId: Long, text: String)

    fun wrapCallback(callback: UserCallback): UserCallbackInfo

    fun getCallbacksByIds(callbackIds: List<Long>): List<UserCallbackInfo>

    fun sendSmsToManager(phone: String, money: String, orderId: Long?)

    fun getOrderIdByPaymentId(paymentId: Long?): Long?
}

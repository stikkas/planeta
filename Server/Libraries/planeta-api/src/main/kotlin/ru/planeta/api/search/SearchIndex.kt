package ru.planeta.api.search

/**
 * Search indexes enumeration
 *
 * @author ameshkov
 */
enum class SearchIndex(// <editor-fold defaultstate="collapsed" desc="Constructor and properties">
        val label: String) {

    USERS("user,user_delta"),
    PROFILES("profile,profile_delta"),
    POSTS("post,post_delta"),
    BROADCASTS("broadcast,broadcast_delta"),
    SEMINARS("seminar"),
    USER_CALLBACKS("user_callbacks"),
    CAMPAIGNS("campaign,campaign_delta"),
    SHARES("share"),
    PRODUCTS("product"),
    ORDERS("order"),
    INVESTING_ORDERS_INFO("investing_orders_info"),
    BIBLIO_LIBRARIES("library")
    // </editor-fold>
}

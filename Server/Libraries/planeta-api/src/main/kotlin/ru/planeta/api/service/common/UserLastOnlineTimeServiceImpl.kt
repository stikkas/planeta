package ru.planeta.api.service.common

import org.apache.log4j.Logger
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import ru.planeta.api.service.security.PermissionService
import ru.planeta.dao.commondb.UserLastOnlineTimeDAO
import ru.planeta.model.common.UserLastOnlineTime

import java.util.Date

@Service
class UserLastOnlineTimeServiceImpl(private var userLastOnlineTimeDAO: UserLastOnlineTimeDAO) : UserLastOnlineTimeService {

    override fun addOrUpdateUserLastOnlineTime(userLastOnlineTime: UserLastOnlineTime): Int {
        if (PermissionService.ANONYMOUS_USER_PROFILE_ID == userLastOnlineTime.profileId) {
            log.error("cannot save user last online time for anonymous profile")
            return 0
        }

        val selectedUserLastOnlineTime = userLastOnlineTimeDAO!!.select(userLastOnlineTime.profileId)
        val result: Int
        if (selectedUserLastOnlineTime == null) {
            result = userLastOnlineTimeDAO.insert(userLastOnlineTime)
        } else {
            result = userLastOnlineTimeDAO.update(userLastOnlineTime)
        }
        return result
    }

    override fun addOrUpdateUserLastOnlineTime(userId: Long): Int {
        return addOrUpdateUserLastOnlineTime(UserLastOnlineTime(userId, Date()))
    }

    override fun getUserLastOnlineTime(profileId: Long): UserLastOnlineTime {
        return userLastOnlineTimeDAO.select(profileId)
    }

    override fun getUsersLastOnlineTime(profileIds: List<Long>): List<UserLastOnlineTime> {
        return userLastOnlineTimeDAO.selectList(profileIds)
    }

    companion object {

        private val log = Logger.getLogger(UserLastOnlineTimeServiceImpl::class.java)
    }


}

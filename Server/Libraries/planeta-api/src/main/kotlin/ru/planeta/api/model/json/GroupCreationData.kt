package ru.planeta.api.model.json

import ru.planeta.model.enums.GroupCategory
import ru.planeta.model.enums.ProfileType

/**
 * Model for group creation
 *
 * @author ds.kolyshev
 * Date: 03.11.11
 */
class GroupCreationData {

    var displayName: String? = null
    var webSite: String? = null
    var alias: String? = null
    var summary: String? = null
    var contactInfo: String? = null
    var category: GroupCategory? = null
    var profileType = ProfileType.GROUP
}

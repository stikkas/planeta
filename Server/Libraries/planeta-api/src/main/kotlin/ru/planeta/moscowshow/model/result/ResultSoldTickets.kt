package ru.planeta.moscowshow.model.result

import javax.xml.bind.annotation.XmlAccessType
import javax.xml.bind.annotation.XmlAccessorType
import javax.xml.bind.annotation.XmlElementWrapper
import javax.xml.bind.annotation.XmlRootElement
import ru.planeta.moscowshow.model.OrderPlaceResult

/**
 *
 * User: Serge Blagodatskih<stikkas17></stikkas17>@gmail.com><br></br>
 * Date: 14.10.16<br></br>
 * Time: 17:14
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
class ResultSoldTickets : Result() {

    @XmlElementWrapper(name = "orderList")
    var places: List<OrderPlaceResult>? = null
}

package ru.planeta.api.service.campaign

import org.springframework.stereotype.Service
import ru.planeta.api.BaseService
import ru.planeta.api.exceptions.MessageCode
import ru.planeta.api.exceptions.NotFoundException
import ru.planeta.api.exceptions.PermissionException
import ru.planeta.dao.commondb.CampaignTargetingDAO
import ru.planeta.model.common.campaign.Campaign
import ru.planeta.model.common.campaign.CampaignTargeting
import javax.validation.constraints.NotNull

@Service
class CampaignTargetingServiceImpl(private var campaignTargetingDAO: CampaignTargetingDAO) : BaseService(), CampaignTargetingService {

    override fun getCampaignTargetingByCampaignId(campaignId: Long): CampaignTargeting {
        return campaignTargetingDAO.selectByCampaignId(campaignId)
    }

    @Throws(PermissionException::class, NotFoundException::class)
    override fun insertOrUpdate(clientId: Long, @NotNull campaignTargeting: CampaignTargeting) {
        val campaign = campaignDAO.selectCampaignById(campaignTargeting.campaignId)
                ?: throw NotFoundException(Campaign::class.java, campaignTargeting.campaignId)
        if (!permissionService.isAdmin(clientId, campaign.profileId ?: -1)) {
            throw PermissionException(MessageCode.PERMISSION_ADMIN)
        }
        val selectedCampaignTargeting = campaignTargetingDAO.selectByCampaignId(campaignTargeting.campaignId)
        if (selectedCampaignTargeting != null) {
            campaignTargetingDAO.update(campaignTargeting)
        } else {
            campaignTargetingDAO.insert(campaignTargeting)
        }
    }
}

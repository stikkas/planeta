package ru.planeta.api.service.profile

import org.apache.commons.codec.digest.DigestUtils

import org.springframework.stereotype.Component

@Component
class PlanetaPasswordEncoder : PasswordEncoder {
    override fun encode(rawPassword: CharSequence): String {
        return DigestUtils.sha256Hex(DigestUtils.md5Hex(rawPassword.toString()))
    }

    override fun matches(rawPassword: CharSequence, encodedPassword: String): Boolean {
        return encodedPassword != null && encodedPassword == encode(rawPassword)
    }
}

package ru.planeta.api.service.shop

import ru.planeta.model.shop.ProductAttribute

/**
 * Product attributes service
 *
 * @author m.shulepov
 */
interface ProductAttributeService {

    val tagToAttributesTypeMap: Map<String, List<ProductAttribute>>

}

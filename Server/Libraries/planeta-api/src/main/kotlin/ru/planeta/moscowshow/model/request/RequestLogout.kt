package ru.planeta.moscowshow.model.request

import javax.xml.bind.annotation.XmlAccessType
import javax.xml.bind.annotation.XmlAccessorType
import javax.xml.bind.annotation.XmlRootElement

/**
 *
 * User: Serge Blagodatskih<stikkas17></stikkas17>@gmail.com><br></br>
 * Date: 13.10.16<br></br>
 * Time: 15:40
 */
@XmlRootElement(name = "logout")
@XmlAccessorType(XmlAccessType.FIELD)
class RequestLogout : Request {

    constructor()

    constructor(sessionId: String) {
        sessionID = sessionId
    }

}

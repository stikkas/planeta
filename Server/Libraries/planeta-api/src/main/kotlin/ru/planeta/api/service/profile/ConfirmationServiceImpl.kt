package ru.planeta.api.service.profile

import com.google.common.cache.CacheBuilder
import org.apache.log4j.Logger
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import ru.planeta.api.service.NexmoService
import java.security.SecureRandom
import java.util.*
import java.util.concurrent.TimeUnit

/**
 * Date: 03.07.12
 *
 * @author s.kalmykov
 */
@Service
class ConfirmationServiceImpl(private val smsClient: NexmoService) : ConfirmationService {

    private val confirmationProcessesByPhone = CacheBuilder.newBuilder()
            .concurrencyLevel(4)
            .expireAfterWrite(2, TimeUnit.MINUTES)
            .build<String, ConfirmationProcess>()

    class ConfirmationProcess(val clientId: Long, val address: String, val confirmationCallback: Runnable?, confirmationCode: String) {

        val confirmationCodeList: MutableList<String>
        var attemptsLeft: Int = 0

        val lastConfirmationCode: String?
            get() = if (confirmationCodeList.isEmpty()) {
                null
            } else confirmationCodeList[confirmationCodeList.size - 1]

        init {
            attemptsLeft = MAX_CONFIRMATION_ATTEMPTS_COUNT
            confirmationCodeList = ArrayList(MAX_CONFIRMATION_ATTEMPTS_COUNT.toInt())
            confirmationCodeList.add(confirmationCode)
        }

        fun decreaseAttempsLeft() {
            attemptsLeft -= 1
        }

        fun addConfirmationCode(confirmationCode: String) {
            if (attemptsLeft <= MAX_CONFIRMATION_ATTEMPTS_COUNT) {
                confirmationCodeList.add(confirmationCode)
            } else {
                log.warn("addConfirmationCode skipped for client $clientId: attempts limit exceeded")
            }
        }
    }


    private fun confirmProcess(process: ConfirmationProcess?, confirmationCode: String): ConfirmationService.Status {
        var hasError = false

        if (process == null) {
            return ConfirmationService.Status.NOT_FOUND
        }

        // if code not found, and attemptsLeft exceeded
        // check it after confirmationCodeList loop, because user can use one of the codes from confirmationCodeList
        if (process.attemptsLeft <= 0) {
            return ConfirmationService.Status.ATTEMPTS_EXCEEDED
        }

        for (processConfirmationCode in process.confirmationCodeList) {
            if (processConfirmationCode == confirmationCode) {
                try {
                    if (process.confirmationCallback != null) {
                        process.confirmationCallback.run()
                    }
                    return ConfirmationService.Status.CONFIRMED
                } catch (e: Exception) {
                    hasError = true
                }

            }
        }

        if (hasError) {
            return ConfirmationService.Status.ERROR
        }

        // if code not found, and attemptsLeft not exceeded
        process.decreaseAttempsLeft()
        return ConfirmationService.Status.WRONG_CODE
    }

    override fun confirm(processId: String, confirmationCode: String): ConfirmationService.Status {
        val process = getConfirmationProcess(processId) ?: return ConfirmationService.Status.NOT_FOUND
        val confirmStatus = confirmProcess(process, confirmationCode)
        if (confirmStatus == ConfirmationService.Status.CONFIRMED) {
            confirmationProcessesByPhone.invalidate(processId)
        }
        return confirmStatus
    }

    override fun createPhoneConfirmation(clientId: Long, phone: String, callback: Runnable?): String {
        var getProcess = getConfirmationProcess(phone)
        if (getProcess != null) {
            if (getProcess.attemptsLeft <= 0) {
                return ""
            }

            // repeat code
            getProcess.addConfirmationCode(generateSecureCode())
            getProcess.decreaseAttempsLeft()
        } else {
            // new code
            getProcess = createConfirmationProcess(clientId, phone, callback)
        }

        val message = "Vash kod: " + getProcess.lastConfirmationCode!!
        smsClient.send(message, phone)
        return putPhoneProcess(phone, getProcess)

    }


    private fun getConfirmationProcess(confirmationProcessID: String): ConfirmationProcess? {
        return confirmationProcessesByPhone.getIfPresent(confirmationProcessID)
    }


    override fun reSendCode(confirmationProcessID: String): String {
        val process = getConfirmationProcess(confirmationProcessID)
        return if (process != null) {
            createPhoneConfirmation(process.clientId, process.address, process.confirmationCallback)
        } else ""
    }


    @Synchronized
    private fun putPhoneProcess(phone: String, process: ConfirmationProcess): String {
        confirmationProcessesByPhone.put(phone, process)
        return phone
    }

    companion object {
        internal val log = Logger.getLogger(ConfirmationServiceImpl::class.java)

        private val MAX_CONFIRMATION_ATTEMPTS_COUNT: Int = 3


        private fun createConfirmationProcess(clientId: Long, address: String, callback: Runnable?): ConfirmationProcess {
            return ConfirmationProcess(clientId, address, callback, generateSecureCode())
        }

        private fun generateSecureCode(): String {
            val random = SecureRandom()
            val bytes = random.generateSeed(5)
            val sb = StringBuilder()
            for (num in bytes) {
                sb.append(Math.abs(num % 10))
            }
            return sb.toString()
        }
    }


}

package ru.planeta.api.service.content

import org.apache.log4j.Logger
import org.springframework.stereotype.Service
import ru.planeta.api.helper.ProjectService
import ru.planeta.api.mail.MailClient
import ru.planeta.api.model.BroadcastSubscriptionDTO
import ru.planeta.dao.commondb.UserPrivateInfoDAO
import ru.planeta.dao.profiledb.BroadcastDAO
import ru.planeta.dao.profiledb.BroadcastSubscriptionDAO
import ru.planeta.model.enums.ProjectType
import ru.planeta.model.profile.broadcast.BroadcastSubscription
import ru.planeta.model.profile.broadcast.enums.BroadcastSubscriptionStatus
import java.util.*

/**
 * @author ds.kolyshev
 * Date: 16.05.13
 */
@Service
class BroadcastSubscriptionServiceImpl(private val userPrivateInfoDAO: UserPrivateInfoDAO,
                                       private val mailClient: MailClient,
                                       private val broadcastSubscriptionDAO: BroadcastSubscriptionDAO,
                                       private val broadcastService: BroadcastService,
                                       private val projectService: ProjectService,
                                       private val broadcastDAO: BroadcastDAO) : BroadcastSubscriptionService {


    override fun addBroadcastSubscription(clientId: Long, broadcastSubscription: BroadcastSubscription) {
        broadcastSubscriptionDAO!!.delete(broadcastSubscription.profileId!!, broadcastSubscription.broadcastId, clientId)
        broadcastSubscription.subscriberProfileId = clientId
        broadcastSubscriptionDAO.insert(broadcastSubscription)
    }

    override fun addBroadcastSubscription(dto: BroadcastSubscriptionDTO) {
        val broadcastSubscription = BroadcastSubscription()
        broadcastSubscription.profileId = dto.broadcastProfileId
        broadcastSubscription.broadcastId = dto.broadcastId
        broadcastSubscription.subscriberProfileId = dto.profileId
        broadcastSubscription.isOnSite = false
        broadcastSubscription.isOnEmail = true
        addBroadcastSubscription(dto.profileId, broadcastSubscription)
    }

    override fun deleteBroadcastSubscription(clientId: Long, profileId: Long, broadcastId: Long) {
        broadcastSubscriptionDAO!!.delete(profileId, broadcastId, clientId)

    }

    override fun getBroadcastSubscription(clientId: Long, profileId: Long, broadcastId: Long): BroadcastSubscription {
        return broadcastSubscriptionDAO!!.selectByBroadcastSubscriberId(profileId, broadcastId, clientId)
    }

    override fun notifyUpcomingBroadcastSubscriptions(date: Date) {
        val upcomingBroadcasts = broadcastService!!.getUpcomingBroadcasts(0, date, false, 0, 0)
        for (broadcast in upcomingBroadcasts) {

            if (BroadcastSubscriptionStatus.PROGRESS == broadcast.broadcastSubscriptionStatus && broadcast.timeBegin!!.time - date.time <= SENT_STEP_TIME) {

                val subscriptionList = broadcastSubscriptionDAO!!.selectByBroadcastId(broadcast.profileId!!, broadcast.broadcastId, 0, 0)

                val url = projectService!!.getUrl(ProjectType.TV, "broadcast/" + broadcast.profileId!!)
                for (subscription in subscriptionList) {
                    val userPrivateInfo = userPrivateInfoDAO!!.selectByUserId(subscription.subscriberProfileId)
                    if (userPrivateInfo != null) {
                        mailClient!!.sendBroadcastSubscriptionNotification(userPrivateInfo.email?: "", broadcast.name?:"", url, userPrivateInfo.username?:"")
                    } else {
                        log.warn("No user info " + subscription.subscriberProfileId)
                    }
                }

                broadcast.broadcastSubscriptionStatus = BroadcastSubscriptionStatus.SENT
                broadcastDAO!!.update(broadcast)

                log.debug("Broadcast #" + broadcast.broadcastId + " notified for 24h")
            }

            if (BroadcastSubscriptionStatus.NOT_STARTED == broadcast.broadcastSubscriptionStatus && broadcast.timeBegin!!.time - date.time <= PROGRESS_STEP_TIME) {

                broadcast.broadcastSubscriptionStatus = BroadcastSubscriptionStatus.PROGRESS
                broadcastDAO!!.update(broadcast)

                log.debug("Broadcast #" + broadcast.broadcastId + " notified for 3h")
            }
        }

    }

    companion object {

        private const val PROGRESS_STEP_TIME = (24 * 60 * 60 * 1000).toLong()
        private const val SENT_STEP_TIME = (3 * 60 * 60 * 1000).toLong()
        private val log = Logger.getLogger(BroadcastSubscriptionServiceImpl::class.java)
    }
}

package ru.planeta.api.exceptions

/**
 * This exception is thrown when payment processing error occurs.
 *
 *
 * User: eshevchenko
 */
class PaymentException : BaseException {

    constructor(e: Throwable) : super(e) {}

    constructor(message: String) : super(message) {}

    constructor(message: String, e: Throwable) : super(message, e) {}
    constructor(messageCode: MessageCode) : super(messageCode) {}

    constructor(messageCode: MessageCode, formattedMessage: String) : super(messageCode, formattedMessage) {}

    constructor(messageCode: MessageCode, e: Throwable) : super(messageCode.errorPropertyName, e) {}
}

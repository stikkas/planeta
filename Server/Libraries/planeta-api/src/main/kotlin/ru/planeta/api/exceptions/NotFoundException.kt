package ru.planeta.api.exceptions

import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ResponseStatus

/**
 * This exception is thrown when requested resource is not found
 *
 * @author ameshkov
 */
@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "Resource is not found")
open class NotFoundException : BaseException {

    constructor() {}

    constructor(message: String) : super(message) {}

    constructor(messageCode: MessageCode) : super(messageCode.errorPropertyName) {}

    constructor(messageCode: MessageCode, formattedMessage: String) : super(messageCode, formattedMessage) {}

    constructor(objectClass: Class<*>) : this(String.format("%s not found.", objectClass.simpleName)) {}

    constructor(objectName: String, objectId: Long?) : this(String.format("%s with id %d not found.", objectName, objectId)) {}

    constructor(objectName: String, name: String, value: String) : this(String.format("%s with name '%s' and value '%s' not found.", objectName, name, value)) {}

    constructor(objectClass: Class<*>, objectId: Long?) : this(objectClass.simpleName, objectId) {}

    constructor(objectClass: Class<*>, fieldName: String, fieldValue: String) : this(String.format("%s with %s = %s not found.", objectClass.simpleName, fieldName, fieldValue)) {}

}

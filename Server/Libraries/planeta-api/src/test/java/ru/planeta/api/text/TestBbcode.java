package ru.planeta.api.text;


import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import ru.planeta.commons.text.Bbcode;

import static org.junit.Assert.*;

/**
 * https://planeta.atlassian.net/browse/PLANETA-7253
 */
public class TestBbcode {
    
    private static final Logger log = Logger.getLogger(TestBbcode.class);
    
    @Test
    public void testTransform() throws Exception {
        String bbcode = "[url=http://planeta.ru]planeta.ru[/url]";
        String html = Bbcode.transform(bbcode);
        Assert.assertEquals("<a href=\"http://planeta.ru\">planeta.ru</a>", html);

        bbcode = "[url=planeta.ru]planeta.ru[/url]";
        html = Bbcode.transform(bbcode);
        Assert.assertEquals("<a href=\"http://planeta.ru\">planeta.ru</a>", html);


        String text = "[b]OPA[/b] Did you read http://www.youtube.com/watch?v=4wxjtds28fE&feature=related? Or http://www.youtube.com/watch? Or our planeta.ru url http://planeta.ru/23709/blog/111!comment123?";
        html = Bbcode.transform(text);
        Assert.assertTrue(html.contains("<a href=\"http://planeta.ru/23709/blog/111!comment123\""));

    }

    @Ignore
    @Test
    public void testAudioShortcutTransform() {
        String bbcode = "[audio id=18370 owner=3550 duration=0:35 name=Fuck artist=Fuck Fuck /]";
        String expectedText = "<p:audio id=\"18370\" owner=\"3550\" duration=\"0:35\" name=\"Длинное название трека со всякими штуками\" artist=\"Длинное имя артиста тынц\"></p:audio>";
        String formattedText = Bbcode.transform(bbcode);
        assertEquals(expectedText, formattedText);
    }

    @Test
    public void testImgShortcutTransform() {
        String bbcode = "[photo id=8494 owner=3550 width=360 height=240]http://s1.portalplaneta.ru/img/00/00/00/00/00/00/21/2e/photo_preview.jpg[/photo]";
        String formattedText = Bbcode.transform(bbcode);
        String expectedText = "<p:photo id=\"8494\" owner=\"3550\" image=\"http://s1.portalplaneta.ru/img/00/00/00/00/00/00/21/2e/photo_preview.jpg\" width=\"360\" height=\"240\"></p:photo>";
        assertEquals(expectedText, formattedText);

        bbcode = "[photo id=8494 owner=3550]http://s1.portalplaneta.ru/img/00/00/00/00/00/00/21/2e/photo_preview.jpg[/photo]";
        formattedText = Bbcode.transform(bbcode);
        expectedText = "<p:photo id=\"8494\" owner=\"3550\" image=\"http://s1.portalplaneta.ru/img/00/00/00/00/00/00/21/2e/photo_preview.jpg\"></p:photo>";
        assertEquals(expectedText, formattedText);
    }

    @Test
    public void testImgShortLinkTransform() {
        String bbcode = "[imgshortlink id=8882 owner=2323]photo[/imgshortlink]";
        String formattedText = Bbcode.transform(bbcode);
        String expectedText = "<a href=\"#/photo/2323/8882\">photo</a>";
        assertEquals(expectedText, formattedText);
    }

    @Test
    public void testStripBbcode() {
        String bbcode = "[photo id=8494 owner=3550]http://s1.portalplaneta.ru/img/00/00/00/00/00/00/21/2e/photo_preview.jpg[/photo]\n" +
            "[audio id=18370 owner=3550 duration=0:34 name=Длинное название трека со всякими штуками artist=Длинное имя артиста тынц /]\n" +
            "[video id=8494 owner=3550 duration=1:23:05]http://s1.portalplaneta.ru/img/00/00/00/00/00/00/21/2e/photo_preview.jpg[/video]\n" +
            "[imgshortlink id=8882 owner=2323]photo[/imgshortlink]";

        assertTrue(org.apache.commons.lang3.StringUtils.trim(Bbcode.stripBbcode(bbcode)).length() == 0);
    }

    @Test
    public void testParseEmail() {
        String bbcode = "Bla bla bla ( with letter on email naivno.com@yandex.ru )";

        assertFalse(Bbcode.transform(bbcode).contains("<a href"));
    }

    @Test
    public void testParseLinks() {
        String bbcode = "[b][url]http://dot1.com[/url][/b] (http://dot2.com/test#new) [b][url=http://dot3.com]link[/url][/b]  (http://dot4.com)";
        String result = "<span style=\"font-weight:bold;\"><a href=\"http://dot1.com\">http://dot1.com</a></span> (<a href=\"http://dot2.com/test#new\">http://dot2.com/test#new</a>) <span style=\"font-weight:bold;\"><a href=\"http://dot3.com\">link</a></span>  (<a href=\"http://dot4.com\">http://dot4.com</a>)";
        assertEquals(result, Bbcode.transform(bbcode));
        String nobbcode = "http://dot1.com";
        String noBbcodeResult = "<a href=\"http://dot1.com\">http://dot1.com</a>";
        assertEquals(noBbcodeResult, Bbcode.transform(nobbcode));
    }

    @Ignore
    @Test
    public void testTransformUrls() {
        String bbcode = "Друзья!\n" +
            "\n" +
            "[b]С января 2013 г. сайт о культуре и обществе Colta.ru становится фактически единственным общественным СМИ в России.[/b]\n" +
            "\n" +
            "[imgshortcut id=166345 owner=93127]https://s2.planeta.ru/i/289c8/original.jpg[/imgshortcut]\n" +
            "[i]Три сета одежды, сделанной в единственном экземпляре для нового  клипа \"Ляписа Трубецкого\" \"Пути народа\" (600 000 просмотров за первые две недели). Уникальные вещи, которые могут стать вашими![/i]\n" +
            "\n" +
            " \n" +
            "[b]ОБЩЕСТВЕННОЕ СМИ? ЧТО ЭТО?[/b]\n" +
            "\n" +
            "Это значит, что наш сайт будет существовать без помощи государства, без хозяев и инвесторов, только на средства общества. Он совершенно независим и обещает именно так работать на своей территории - территории культуры и духа времени.\n" +
            "\n" +
            "Это эксперимент. Мы сами не знаем, чем он закончится, но нам чертовски интересно. Мы надеемся, что интересно будет и вам.\n" +
            "\n" +
            "[imgshortcut id=144361 owner=93127]https://s2.planeta.ru/i/233e8/original.jpg[/imgshortcut]\n" +
            "[i]Значок-плеер \"За Кольту...\" со сборником лучших русских песен 2012 года. Который может стать вашим[/i]\n" +
            " \n" +
            "[b]КТО ДАЛ НАМ ДЕНЕГ НА ОТКРЫТИЕ, И ЧТО ТЕПЕРЬ?[/b]\n" +
            " \n" +
            "У сайта Colta.ru нет хозяина - ни государственного, ни частного. Основа его жизнедеятельности – это пожертвования отдельных лиц и гранты.\n" +
            "\n" +
            "Colta.ru смогла открыться снова благодаря первым пожертвованиям людей, входящих в Попечительский совет сайта. Его список скоро будет оглашен.\n" +
            "\n" +
            "Кстати, Попечительский совет – это совершенно открытая структура. В него можно войти при стартовом взносе в 300 000 рублей. \n" +
            "Если вы вдруг заходите присоединиться и узнать больше о Совете, пишите нам на адрес sovet @ colta.ru. С огромной благодарностью обсудим!\n" +
            "\n" +
            "[imgshortcut id=164032 owner=93127]https://s2.planeta.ru/i/280bf/original.jpg[/imgshortcut]\n" +
            "[i]Единственный шанс попасть за сцену Большого театра. И послушать рассказ про его настоящую, тайную жизнь от пресс-секретаря Большого Екатерины Новиковой[/i]\n" +
            "\n" +
            "\n" +
            "[b]СКОЛЬКО НАМ НУЖНО?[/b]\n" +
            " \n" +
            "Вообще, чтобы сайт Colta.ru работал до конца 2013 года, нам нужно 2 700 000 рублей.\n" +
            "\n" +
            "Но на первые два месяца нам пока необходимо 400 000 руб.Их мы собираем здесь. Остальные – с апреля уже на нашем сайте.\n" +
            "\n" +
            "[videoplayer id=12128 owner=93127 duration=3:34]http://s1.planeta.ru/p?url=http%3A%2F%2Fi.ytimg.com%2Fvi%2FpLsaewo3nls%2F0.jpg&width=450&height=0&crop=false&disableAnimatedGif=false[/videoplayer]\n" +
            "[i]Костюмы из этого клипа \"Ляписа Трубецкого\" могут стать вашими. Не упустите модные вещи с уникальной историей! [/i]\n" +
            " \n" +
            "[b]ТАК КАК КОНКРЕТНО ПОМОЧЬ?[/b]\n" +
            " \n" +
            "Вы можете в правой колонке перевести нам любую сумму от 100 рублей. Безвозмездно в поддержку сайта. Или получив за это что-то взамен.\n" +
            "\n" +
            "Имя каждого, кто нам помог, конечно, с благодарностью появится в списке [b][url=http://www.colta.ru/thanks/]Нам помогают[/url][/b] на сайте. \n" +
            "Эта страница самая главная. Без нее сайта вовсе бы не было.\n" +
            "\n" +
            "[imgshortcut id=159896 owner=6]https://s2.planeta.ru/i/2340b/original.jpg[/imgshortcut]\n" +
            "[i]Принт Валерия Чтака на холсте, который может стать вашим (остался всего один!)[/i]\n" +
            "\n" +
            "[b]ЧТО ДАЛЬШЕ?[/b]\n" +
            " \n" +
            "Дальше наступило уже сейчас. Мы снова ежедневно с вами по адресу Colta.ru - с большим, чем раньше, количеством обновлений, \n" +
            "написанных, как и прежде, лучшими людьми в профессии.\n" +
            "\n" +
            "Все это время мы ценили и чувствовали вашу поддержку!  И мы надеемся на нее и сейчас. Мы со своей стороны постараемся не подкачать.\n" +
            "\n" +
            "И гигантское вам всем спасибо!  Потому что этот сайт теперь уже точно не только наш, но и ваш.\n" +
            "\n" +
            "[imgshortcut id=164038 owner=93127]https://s2.planeta.ru/i/280c5/original.jpg[/imgshortcut]\n" +
            "[i]Заседание Попечительского совета[/i]";

        String html = Bbcode.transform(bbcode);

        log.info(html);
    }

    @Test
    public void testCampaignReplace() throws Exception {
        String inBbCode = "tyjtyjtyj\n" +
                "\n" +
                "[photo  id=245303 owner=636285]http://s2.dev.planeta.ru/p?url=http%3A%2F%2Fwww.ecmo.ru%2Fdata%2FHimki-forest.jpeg&width=1000&height=1000&crop=false&disableAnimatedGif=false[/photo]\n" +
                "\n" +
                "[photo id=245284 owner=636285]http://s1.dev.planeta.ru/p?url=http%3A%2F%2Fleaksource.files.wordpress.com%2F2013%2F02%2Fanonymous-mask.jpg&width=1000&height=1000&crop=false&disableAnimatedGif=false[/photo]\n" +
                "\n" +
                "[video id=32934 owner=636285 duration=76 name=Все ненавидят криса- Разговор с психологом [Kuraj-Bambey.Ru]]http://s1.dev.planeta.ru/i/3bd49/1373873575253_renamed.jpg[/video]";
        String outBbCode = Bbcode.transformCampaignDescription(555, inBbCode, "245303", "777777");
        Assert.assertTrue(outBbCode.contains("777777"));
        Assert.assertTrue(outBbCode.contains("555"));
    }
}

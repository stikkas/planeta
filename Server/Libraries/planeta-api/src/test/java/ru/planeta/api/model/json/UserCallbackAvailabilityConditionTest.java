package ru.planeta.api.model.json;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import ru.planeta.api.service.configurations.ConfigurationService;
import ru.planeta.api.service.configurations.ConfigurationServiceImpl;
import ru.planeta.dao.commondb.ConfigurationDAO;
import ru.planeta.model.common.Configuration;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.Arrays;
import java.util.Calendar;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;

@RunWith(SpringJUnit4ClassRunner.class)
public class UserCallbackAvailabilityConditionTest {


    @MockBean
    private ConfigurationDAO configurationDAO;

    private int[] weekDays = new int[]{Calendar.MONDAY, Calendar.TUESDAY, Calendar.WEDNESDAY, Calendar.THURSDAY, Calendar.FRIDAY};

    private UserCallbackAvailabilityCondition configuration;

    @Before
    public void setUp() {
        ConfigurationService configurationService = new ConfigurationServiceImpl(configurationDAO);
        Configuration config = new Configuration();
        config.setStringValue("{\"stdHourFrom\": 11, \"stdHourTo\": 19, \"addHourFrom\": 11, \"addHourTo\": 18, \"addDays\": [7], " +
                "\"holidays\": [\"2018-06-11\", \"2018-06-12\", \"2018-11-05\"], \"minAmount\": 5000 }");
        given(configurationDAO.select("user.callback.availability.condition")).willReturn(config);
        configuration = configurationService.getCallbackAvailabilityCondition();
    }

    @Test
    public void checkConfiguration() throws ParseException {
        assertThat(configuration).isNotNull();
        assertThat(configuration.getStdHourFrom()).isEqualTo(11);
        assertThat(configuration.getStdHourTo()).isEqualTo(19);
        assertThat(configuration.getAddHourFrom()).isEqualTo(11);
        assertThat(configuration.getAddHourTo()).isEqualTo(18);
        assertThat(configuration.getAddDays()).hasSize(1).contains(Calendar.SATURDAY);
        assertThat(configuration.getMinAmount()).isEqualTo(new BigDecimal(5000));
        Calendar[] holidays = configuration.getHolidays();
        assertThat(Arrays.asList(holidays)).hasSize(3);
    }

    @Test
    public void check() {
        checkWeekDays(new BigDecimal(100), 11, 19, false);
        checkWeekDays(new BigDecimal(5000), 11, 19, true);
        checkWeekDays(new BigDecimal(5011), 11, 19, true);

        Calendar calendar = Calendar.getInstance();
        // Проверяем субботу
        calendar.set(Calendar.DAY_OF_WEEK, Calendar.SATURDAY);
        checkHours(11, 18, calendar, new BigDecimal(1000), false);
        checkHours(11, 18, calendar, new BigDecimal(5000), true);
        checkHours(11, 18, calendar, new BigDecimal(10000), true);

        // Проверяем воскресенье
        calendar.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY);
        checkHours(11, 19, calendar, new BigDecimal(1000), false);
        checkHours(11, 19, calendar, new BigDecimal(5000), false);
        checkHours(11, 19, calendar, new BigDecimal(10000), false);

        // Проверяем праздники
        calendar.set(Calendar.YEAR, 2018);
        calendar.set(Calendar.MONTH, Calendar.JUNE);
        calendar.set(Calendar.DATE, 11);
        checkHours(11, 19, calendar, new BigDecimal(1000), false);
        checkHours(11, 19, calendar, new BigDecimal(5000), false);
        checkHours(11, 19, calendar, new BigDecimal(10000), false);

        calendar.set(Calendar.DATE, 12);
        checkHours(11, 19, calendar, new BigDecimal(1000), false);
        checkHours(11, 19, calendar, new BigDecimal(5000), false);
        checkHours(11, 19, calendar, new BigDecimal(10000), false);

        calendar.set(Calendar.MONTH, Calendar.NOVEMBER);
        calendar.set(Calendar.DATE, 5);
        checkHours(11, 19, calendar, new BigDecimal(1000), false);
        checkHours(11, 19, calendar, new BigDecimal(5000), false);
        checkHours(11, 19, calendar, new BigDecimal(10000), false);
    }

    private void checkWeekDays(BigDecimal amount, int startHour, int endHour, boolean expected) {
        Calendar calendar = Calendar.getInstance();
        for (int day : weekDays) {
            calendar.set(Calendar.DAY_OF_WEEK, day);
            checkHours(startHour, endHour, calendar, amount, expected);
        }
    }

    private void checkHours(int startHour, int endHour, Calendar calendar, BigDecimal amount, boolean expected) {
        for (int hour = 0; hour < 24; ++hour) {
            calendar.set(Calendar.HOUR_OF_DAY, hour);
            if (hour < startHour || hour >= endHour) {
                assertThat(configuration.check(calendar, amount)).isFalse();
            } else {
                assertThat(configuration.check(calendar, amount)).isEqualTo(expected);
            }
        }
    }

}

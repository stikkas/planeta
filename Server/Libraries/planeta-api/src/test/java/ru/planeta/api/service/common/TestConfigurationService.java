package ru.planeta.api.service.common;

import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import ru.planeta.api.exceptions.NotFoundException;
import ru.planeta.api.model.ConfigurationType;
import ru.planeta.api.service.configurations.ConfigurationService;
import ru.planeta.model.common.Configuration;
import ru.planeta.test.AbstractTest;

import java.util.List;
import java.util.Set;

import static org.junit.Assert.*;

/**
 * @author ds.kolyshev
 *         Date: 17.05.12
 */
public class TestConfigurationService extends AbstractTest {

    @Autowired
    private ConfigurationService configurationService;

    @Test
    public void testConfigurationService() {

        Configuration configuration = new Configuration();
        configuration.setKey("planeta.test.key");
        configuration.setIntValue(123);
        configuration.setStringValue("test string value");

        configurationService.saveConfiguration(configuration);

        Configuration selected = configurationService.getConfigurationByKey(configuration.getKey());
        assertConfigurationsEquals(configuration, selected);

        List<Configuration> list = configurationService.getConfigurationList(0, 1);
        assertEquals(1, list.size());

        configuration.setBooleanValue(true);
        configuration.setIntValue(configuration.getIntValue() + 1);
        configuration.setStringValue(null);

        configurationService.saveConfiguration(configuration);
        selected = configurationService.getConfigurationByKey(configuration.getKey());
        assertConfigurationsEquals(configuration, selected);

        configurationService.deleteConfigurationByKey(configuration.getKey());
        selected = configurationService.getConfigurationByKey(configuration.getKey());
        assertNull(selected);


    }

    private static void assertConfigurationsEquals(Configuration expected, Configuration actual) {
        assertNotNull(actual);
        assertEquals(expected.getKey(), actual.getKey());
        assertEquals(expected.getStringValue(), actual.getStringValue());
        assertEquals(expected.getIntValue(), actual.getIntValue());
        assertEquals(expected.getBooleanValue(), actual.getBooleanValue());
    }

    @Test
    public void testCache() throws NotFoundException {
        configurationService.saveStrValue("123,345,567", ConfigurationType.COMMERICAL_CAMPAIGNS);
        Set<Long> list = configurationService.getCommercialCampaigns();
        configurationService.saveStrValue("1231,3451,5671", ConfigurationType.COMMERICAL_CAMPAIGNS);
        Set<Long> listCached = configurationService.getCommercialCampaigns();
        assertEquals(list, listCached);

        configurationService.saveStrValue("123,345,567", ConfigurationType.COMMERCIAL_SHARES);
        list = configurationService.getCommercialShares();
        configurationService.saveStrValue("1231,3451,5671", ConfigurationType.COMMERICAL_CAMPAIGNS);
        listCached = configurationService.getCommercialShares();
        assertEquals(list, listCached);
    }
}

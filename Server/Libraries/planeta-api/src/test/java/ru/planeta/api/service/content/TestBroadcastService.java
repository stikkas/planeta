package ru.planeta.api.service.content;

import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import ru.planeta.api.exceptions.NotFoundException;
import ru.planeta.api.exceptions.PermissionException;
import ru.planeta.api.exceptions.OrderException;
import ru.planeta.api.service.billing.order.OrderService;
import ru.planeta.commons.text.Bbcode;
import ru.planeta.model.common.campaign.Campaign;
import ru.planeta.model.common.Order;
import ru.planeta.model.common.campaign.Share;
import ru.planeta.model.common.campaign.enums.CampaignStatus;
import ru.planeta.model.profile.broadcast.enums.BroadcastRecordStatus;
import ru.planeta.model.profile.broadcast.enums.BroadcastStatus;
import ru.planeta.model.profile.broadcast.enums.BroadcastSubscriptionStatus;
import ru.planeta.model.enums.PermissionLevel;
import ru.planeta.model.profile.broadcast.*;
import ru.planeta.test.AbstractTest;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.*;

/**
 * @author ds.kolyshev
 *         Date: 19.03.12
 */

@Ignore // этот тест зависит от geowebservice, который в свою очередь зависит от этой библиотеки, поэтому этот тест надо перенести куда-то в другое место
public class TestBroadcastService extends AbstractTest {

	@Autowired
	private BroadcastService broadcastService;
	@Autowired
	private BroadcastSubscriptionService broadcastSubscriptionService;
	@Autowired
	private OrderService orderService;

	@Test
	public void testBroadcastService() throws NotFoundException, PermissionException, IOException {

		long clientId = insertPlanetaAdminPrivateInfo().getUserId();
		long groupId = registerSimpleGroup(clientId);
		long eventId = insertEventProfile(clientId, groupId).getProfileId();

		Broadcast broadcast = createBroadcast(eventId);
		Broadcast insertedBroadcast = broadcastService.saveBroadcast(clientId, broadcast);
		assertBroadcastsEquals(broadcast, insertedBroadcast);

		insertedBroadcast = broadcastService.getBroadcast(clientId, broadcast.getBroadcastId());
		assertBroadcastsEquals(broadcast, insertedBroadcast);


		//update broadcast
		broadcast.setName("test name 2");
		broadcast.setDescription("test description 2");

		Broadcast updatedBroadcast = broadcastService.saveBroadcast(clientId, broadcast);
		assertBroadcastsEquals(broadcast, updatedBroadcast);

		//add stream
		BroadcastStream broadcastStream = createStream(eventId, broadcast);
		BroadcastStream insertedStream = broadcastService.saveBroadcastStream(clientId, broadcastStream);
		assertBroadcastStreamsEquals(broadcastStream, insertedStream);

		//update streams
		insertedStream.setBroadcastUrl("test url 2");
		insertedStream.setName("test name 2");
		insertedStream.setEmbedVideoHtml("<test embed 2/>");
		BroadcastStream updatedStream = broadcastService.saveBroadcastStream(clientId, insertedStream);
		assertBroadcastStreamsEquals(insertedStream, updatedStream);

		//add 1 more stream
		broadcastService.saveBroadcastStream(clientId, createStream(eventId, broadcast));
		List<BroadcastStream> streams = broadcastService.getBroadcastStreams(clientId, broadcast.getBroadcastId(), 0, 0);
		assertEquals(2, streams.size());

		//deleteByProfileId stream
		broadcastService.deleteBroadcastStream(clientId, updatedStream.getStreamId());
		streams = broadcastService.getBroadcastStreams(clientId, broadcast.getBroadcastId(), 0, 0);
		assertEquals(1, streams.size());

		//deleteByProfileId broadcast
		broadcastService.deleteBroadcast(clientId, broadcast.getBroadcastId());
		assertNull(broadcastService.getBroadcast(clientId, broadcast.getBroadcastId()));
		streams = broadcastService.getBroadcastStreams(clientId, broadcast.getBroadcastId(), 0, 0);
		assertEquals(0, streams.size());

	}

	@Ignore
	@Test
	public void testBroadcastGeoTargeting() throws NotFoundException, PermissionException, IOException {

		long clientId = insertPlanetaAdminPrivateInfo().getUserId();
		long groupId = registerSimpleGroup(clientId);
		long eventId = insertEventProfile(clientId, groupId).getProfileId();

		Broadcast broadcast = createBroadcast(eventId);
		Broadcast insertedBroadcast = broadcastService.saveBroadcast(clientId, broadcast);
		assertBroadcastsEquals(broadcast, insertedBroadcast);

		assertTrue(broadcastService.checkBroadcastGeoTargeting(clientId, eventId, broadcast.getBroadcastId(), "127.0.0.1"));

		BroadcastGeoTargeting broadcastGeoTargeting = createBroadcastGeoTargeting(broadcast, 2, 1, true);
		broadcastService.addBroadcastGeoTargeting(clientId, broadcastGeoTargeting);
		broadcastGeoTargeting = createBroadcastGeoTargeting(broadcast, 2, 2, true);
		broadcastService.addBroadcastGeoTargeting(clientId, broadcastGeoTargeting);
		broadcastGeoTargeting = createBroadcastGeoTargeting(broadcast, 2, 3, true);
		broadcastService.addBroadcastGeoTargeting(clientId, broadcastGeoTargeting);
		assertFalse(broadcastService.checkBroadcastGeoTargeting(clientId, eventId, broadcast.getBroadcastId(), "127.0.0.1"));

		List<BroadcastGeoTargeting> list = broadcastService.getBroadcastGeoTargeting(clientId, broadcast.getBroadcastId());
		assertNotNull(list);
		assertEquals(3, list.size());

		broadcastService.removeBroadcastGeoTargetingByParams(clientId, broadcast.getBroadcastId(), 2, 2, true);
		list = broadcastService.getBroadcastGeoTargeting(clientId, broadcast.getBroadcastId());
		assertNotNull(list);
		assertEquals(2, list.size());
		assertFalse(broadcastService.checkBroadcastGeoTargeting(clientId, eventId, broadcast.getBroadcastId(), "127.0.0.1"));

		broadcastService.removeBroadcastGeoTargeting(clientId, broadcast.getBroadcastId());
		assertTrue(broadcastService.checkBroadcastGeoTargeting(clientId, eventId, broadcast.getBroadcastId(), "127.0.0.1"));
	}

	@Test
	public void testBroadcastBackersTargeting() throws NotFoundException, PermissionException, IOException, OrderException {

		final long clientId = insertPlanetaAdminPrivateInfo().getUserId();
		final long adminId = getProfileId(registerPlanetaAdminUser());
		final long groupId = registerNewOfficialGroup(adminId);
		final long eventId = insertEventProfile(clientId, groupId).getProfileId();

		increaseProfileBalance(clientId, BigDecimal.TEN.multiply(BigDecimal.TEN));

		Broadcast broadcast = createBroadcast(eventId);
		Broadcast insertedBroadcast = broadcastService.saveBroadcast(clientId, broadcast);
		assertBroadcastsEquals(broadcast, insertedBroadcast);

		assertTrue(broadcastService.checkBroadcastBackersTargeting(clientId, broadcast.getBroadcastId()));

		Campaign campaign = createCampaign(adminId, groupId, BigDecimal.TEN.multiply(new BigDecimal(5)), CampaignStatus.ACTIVE);
		Share share = createShare(adminId, groupId, campaign, BigDecimal.TEN, 1);

		BroadcastBackersTargeting targeting = new BroadcastBackersTargeting();
		targeting.setProfileId(broadcast.getProfileId());
		targeting.setBroadcastId(broadcast.getBroadcastId());
		targeting.setCampaignId(share.getCampaignId());
		targeting.setLimitSumm(BigDecimal.ONE);
		broadcastService.addBroadcastBackersTargeting(clientId, targeting);
		assertFalse(broadcastService.checkBroadcastBackersTargeting(clientId, broadcast.getBroadcastId()));

		Order order = orderService.createOrderWithShare(adminId, clientId, share.getShareId(), share.getPrice(), 1, getTestStr("answer"), null, null, null);
		orderService.purchase(order);
		assertTrue(broadcastService.checkBroadcastBackersTargeting(clientId, broadcast.getBroadcastId()));

		List<BroadcastBackersTargeting> list = broadcastService.getBroadcastBackersTargeting(clientId, eventId, broadcast.getBroadcastId());
		assertNotNull(list);
		assertEquals(1, list.size());

		broadcastService.removeBroadcastBackersTargeting(clientId, broadcast.getBroadcastId(), targeting.getCampaignId());
		assertTrue(broadcastService.checkBroadcastBackersTargeting(clientId, broadcast.getBroadcastId()));

		//check purchase with donation
		targeting = new BroadcastBackersTargeting();
		targeting.setProfileId(broadcast.getProfileId());
		targeting.setBroadcastId(broadcast.getBroadcastId());
		targeting.setCampaignId(share.getCampaignId());
		targeting.setLimitSumm(BigDecimal.TEN.add(BigDecimal.TEN).add(BigDecimal.TEN));
		broadcastService.addBroadcastBackersTargeting(clientId, targeting);
		assertFalse(broadcastService.checkBroadcastBackersTargeting(clientId, broadcast.getBroadcastId()));

		share = createShare(adminId, groupId, campaign, BigDecimal.TEN, 4);
		//buy with donate
		order = orderService.createOrderWithShare(adminId, clientId, share.getShareId(), share.getPrice().add(BigDecimal.TEN).add(BigDecimal.ONE), 1, getTestStr("answer"), null, null, null);
		orderService.purchase(order);
		assertTrue(broadcastService.checkBroadcastBackersTargeting(clientId, broadcast.getBroadcastId()));

	}

	@Test
	public void testPlayStopAll() throws NotFoundException, PermissionException, IOException {


		long clientId = insertPlanetaAdminPrivateInfo().getUserId();
		long groupId = registerSimpleGroup(clientId);
		long eventId = insertEventProfile(clientId, groupId).getProfileId();

		Broadcast broadcast = createBroadcast(eventId);
		Broadcast insertedBroadcast = broadcastService.saveBroadcast(clientId, broadcast);
		assertBroadcastsEquals(broadcast, insertedBroadcast);

		//add stream
		BroadcastStream broadcastStream = createStream(eventId, broadcast);
		broadcastStream.setBroadcastRecordStatus(BroadcastRecordStatus.NO_RECORD);
		BroadcastStream insertedStream = broadcastService.saveBroadcastStream(clientId, broadcastStream);
		assertBroadcastStreamsEquals(broadcastStream, insertedStream);
		broadcast.setDefaultStreamId(broadcastStream.getStreamId());

		//check broadcast to live
		broadcastService.liveStartedBroadcasts(broadcastStream.getTimeBegin().getTime() + 1000);
		broadcastStream.setBroadcastStatus(BroadcastStatus.LIVE);
		broadcast.setBroadcastStatus(BroadcastStatus.LIVE);
		Broadcast livedBroadcast = broadcastService.getBroadcast(clientId, broadcast.getBroadcastId());
		assertBroadcastsEquals(broadcast, livedBroadcast);
		BroadcastStream livedBroadcastStream = broadcastService.getBroadcastStream(clientId, broadcastStream.getStreamId());
		assertBroadcastStreamsEquals(broadcastStream, livedBroadcastStream);

		//check broadcast to stop
		broadcastService.stopEndedBroadcasts(broadcastStream.getTimeEnd().getTime() + 10000);
		broadcast.setBroadcastStatus(BroadcastStatus.FINISHED);
		Broadcast stoppedBroadcast = broadcastService.getBroadcast(clientId, broadcast.getBroadcastId());
		assertBroadcastsEquals(broadcast, stoppedBroadcast);
		broadcastStream.setBroadcastStatus(BroadcastStatus.FINISHED);
		BroadcastStream stoppedBroadcastStream = broadcastService.getBroadcastStream(clientId, broadcastStream.getStreamId());
		assertBroadcastStreamsEquals(broadcastStream, stoppedBroadcastStream);
	}

	@Test
	public void testPlayStopOne() throws NotFoundException, PermissionException, IOException {


		long clientId = insertPlanetaAdminPrivateInfo().getUserId();
		long groupId = registerSimpleGroup(clientId);
		long eventId = insertEventProfile(clientId, groupId).getProfileId();

		Broadcast broadcast = createBroadcast(eventId);
		Broadcast insertedBroadcast = broadcastService.saveBroadcast(clientId, broadcast);
		assertBroadcastsEquals(broadcast, insertedBroadcast);

		//add stream
		BroadcastStream broadcastStream = createStream(eventId, broadcast);
		BroadcastStream insertedStream = broadcastService.saveBroadcastStream(clientId, broadcastStream);
		assertBroadcastStreamsEquals(broadcastStream, insertedStream);
		broadcast.setDefaultStreamId(insertedStream.getStreamId());

		//check broadcast to live
		broadcastService.playBroadcast(clientId, broadcast.getBroadcastId());
		broadcast.setBroadcastStatus(BroadcastStatus.LIVE);
		Broadcast livedBroadcast = broadcastService.getBroadcast(clientId, broadcast.getBroadcastId());
		assertBroadcastsEquals(broadcast, livedBroadcast);
		//check stream is live
		broadcastStream.setBroadcastStatus(BroadcastStatus.LIVE);
		broadcastStream.setBroadcastRecordStatus(BroadcastRecordStatus.RECORDING);
		BroadcastStream livedStream = broadcastService.getBroadcastStream(clientId, broadcastStream.getStreamId());
		assertBroadcastStreamsEquals(broadcastStream, livedStream);
		//pause stream
		broadcastStream.setBroadcastStatus(BroadcastStatus.PAUSED);
		livedStream = broadcastService.pauseBroadcastStream(clientId, eventId, broadcastStream.getStreamId());
		assertBroadcastStreamsEquals(broadcastStream, livedStream);

		broadcastService.playBroadcast(clientId, broadcast.getBroadcastId());
		//check broadcast to pause
		broadcastService.pauseBroadcast(clientId, broadcast.getBroadcastId());
		broadcast.setBroadcastStatus(BroadcastStatus.PAUSED);
		Broadcast pausedBroadcast = broadcastService.getBroadcast(clientId, broadcast.getBroadcastId());
		assertBroadcastsEquals(broadcast, pausedBroadcast);
		//check stream is paused
		broadcastStream.setBroadcastStatus(BroadcastStatus.PAUSED);
		broadcastStream.setBroadcastRecordStatus(BroadcastRecordStatus.RECORDING);
		BroadcastStream pausedStream = broadcastService.getBroadcastStream(clientId, broadcastStream.getStreamId());
		assertBroadcastStreamsEquals(broadcastStream, pausedStream);

		//check broadcast to stop
		broadcastService.stopBroadcast(clientId, broadcast.getBroadcastId());
		broadcast.setBroadcastStatus(BroadcastStatus.FINISHED);
		Broadcast stoppedBroadcast = broadcastService.getBroadcast(clientId, broadcast.getBroadcastId());
		assertBroadcastsEquals(broadcast, stoppedBroadcast);
		//check stream is stopped
		broadcastStream.setBroadcastStatus(BroadcastStatus.FINISHED);
		broadcastStream.setBroadcastRecordStatus(BroadcastRecordStatus.RECORD_FINISHED);
		livedStream = broadcastService.getBroadcastStream(clientId, broadcastStream.getStreamId());
		assertBroadcastStreamsEquals(broadcastStream, livedStream);
	}

	@Test
	public void testStreamOutdatedCreateEdit() throws NotFoundException, PermissionException, IOException {

		long clientId = insertPlanetaAdminPrivateInfo().getUserId();
		long groupId = registerSimpleGroup(clientId);
		long eventId = insertEventProfile(clientId, groupId).getProfileId();

		Broadcast broadcast = createBroadcast(eventId);
		broadcastService.saveBroadcast(clientId, broadcast);
		assertEquals(BroadcastStatus.NOT_STARTED, broadcast.getBroadcastStatus());

		//update stream to start and end before now
		broadcast.setTimeBegin(new Date(new Date().getTime() - 1000000));
		broadcast.setTimeEnd(new Date(broadcast.getTimeBegin().getTime() + 20000));
		Broadcast updated = broadcastService.saveBroadcast(clientId, broadcast);
		broadcast.setBroadcastStatus(BroadcastStatus.FINISHED);
		assertBroadcastsEquals(broadcast, updated);
		assertEquals(BroadcastStatus.FINISHED, updated.getBroadcastStatus());

	}

	@Test
	public void testGeneratePrivateBroadcastLinks() throws NotFoundException, PermissionException, IOException {

		long clientId = insertPlanetaAdminPrivateInfo().getUserId();
		long groupId = registerSimpleGroup(clientId);
		long eventId = insertEventProfile(clientId, groupId).getProfileId();

		Broadcast broadcast = createBroadcast(eventId);
		broadcastService.saveBroadcast(clientId, broadcast);
		assertEquals(BroadcastStatus.NOT_STARTED, broadcast.getBroadcastStatus());

        // make broadcast private
        broadcastService.makeBroadcastPrivate(clientId, broadcast.getBroadcastId());
        broadcast = broadcastService.getBroadcast(clientId, broadcast.getBroadcastId());
		assertNotNull(broadcast);
		assertTrue(broadcast.isClosed());

		// generate private broadcast links
        List<String> emails = Arrays.asList("test1@gmail.com", "test2@gmail.com", "test3@gmail.com");
        int validFor = 4;
        broadcastService.generatePrivateBroadcastLinks(clientId, eventId, broadcast.getBroadcastId(), emails, validFor);

        List<BroadcastPrivateTargeting> broadcastPrivateTargetings = broadcastService.getBroadcastPrivateTargetings(clientId, broadcast.getBroadcastId());
        assertNotNull(broadcastPrivateTargetings);
        for (BroadcastPrivateTargeting broadcastPrivateTargeting : broadcastPrivateTargetings) {
            assertNotNull(broadcastPrivateTargeting.getGeneratedLink());
            assertEquals(validFor, broadcastPrivateTargeting.getValidInHours());
        }

    }

	@Test
	public void testBroadcastSubscriptions() throws NotFoundException, PermissionException {
		long clientId = insertPlanetaAdminPrivateInfo().getUserId();
		long groupId = registerSimpleGroup(clientId);
		long eventId = insertEventProfile(clientId, groupId).getProfileId();

		Broadcast broadcast = createBroadcast(eventId);
		Broadcast insertedBroadcast = broadcastService.saveBroadcast(clientId, broadcast);
		assertBroadcastsEquals(broadcast, insertedBroadcast);

		Date now = new Date();
		broadcast.setTimeBegin(new Date(now.getTime() + 2 * 60 * 60 * 1000));
		broadcast.setTimeEnd(new Date(now.getTime() + 4 * 60 * 60 * 1000));
		broadcastService.saveBroadcast(clientId, broadcast);

		BroadcastSubscription broadcastSubscription = new BroadcastSubscription();
		broadcastSubscription.setSubscriberProfileId(clientId);
		broadcastSubscription.setProfileId(broadcast.getProfileId());
		broadcastSubscription.setBroadcastId(broadcast.getBroadcastId());
		broadcastSubscription.setOnSite(true);
		broadcastSubscription.setOnEmail(true);
		broadcastSubscriptionService.addBroadcastSubscription(clientId, broadcastSubscription);

		BroadcastSubscription subscription = broadcastSubscriptionService.getBroadcastSubscription(clientId, broadcast.getProfileId(), broadcast.getBroadcastId());
		assertNotNull(subscription);

		broadcastSubscriptionService.notifyUpcomingBroadcastSubscriptions(now);
		Broadcast selected = broadcastService.getBroadcast(clientId, broadcast.getBroadcastId());
		broadcast.setBroadcastSubscriptionStatus(BroadcastSubscriptionStatus.PROGRESS);
		assertBroadcastsEquals(broadcast, selected);

		broadcastSubscriptionService.notifyUpcomingBroadcastSubscriptions(now);
		selected = broadcastService.getBroadcast(clientId, broadcast.getBroadcastId());
		broadcast.setBroadcastSubscriptionStatus(BroadcastSubscriptionStatus.SENT);
		assertBroadcastsEquals(broadcast, selected);

		broadcastSubscriptionService.deleteBroadcastSubscription(clientId, broadcast.getProfileId(), broadcast.getBroadcastId());
		assertNull(broadcastSubscriptionService.getBroadcastSubscription(clientId, broadcast.getProfileId(), broadcast.getProfileId()));
	}

	/**
	 * With test is only for debugging - must be ignored
	 *
	 * @throws NotFoundException
	 * @throws PermissionException
	 */

	@Ignore
	@Test
	public void testSaveDumpedVideo() throws NotFoundException, PermissionException {

		long clientId = insertPlanetaAdminPrivateInfo().getUserId();
		long groupId = registerSimpleGroup(clientId);
		long eventId = insertEventProfile(clientId, groupId).getProfileId();

		Broadcast broadcast = createBroadcast(eventId);
		Broadcast insertedBroadcast = broadcastService.saveBroadcast(clientId, broadcast);
		assertBroadcastsEquals(broadcast, insertedBroadcast);

		//add stream
		BroadcastStream broadcastStream = createStream(eventId, broadcast);
		File videoFile = new File("D:\\Temp\\demo.3gp");
		assertTrue(videoFile.exists());

		broadcastService.scheduleSaveDumpVideo(broadcastStream, videoFile);


	}

    @Test
    public void testUpdateBroadcastViewsCount() throws NotFoundException, PermissionException {
        long clientId = insertPlanetaAdminPrivateInfo().getUserId();
		long groupId = registerSimpleGroup(clientId);
        long eventId = insertEventProfile(clientId, groupId).getProfileId();

        Broadcast broadcast = createBroadcast(eventId);
        Broadcast insertedBroadcast = broadcastService.saveBroadcast(clientId, broadcast);
        assertEquals(0, insertedBroadcast.getViewsCount());

        broadcastService.updateBroadcastViewsCount(broadcast.getDefaultStreamId());
        insertedBroadcast = broadcastService.saveBroadcast(clientId, broadcast);
        assertEquals(1, insertedBroadcast.getViewsCount());
    }

	private static void assertBroadcastStreamsEquals(BroadcastStream expected, BroadcastStream actual) {
		assertNotNull(actual);
		assertEquals(expected.getAuthorProfileId(), actual.getAuthorProfileId());
		assertEquals(expected.getProfileId(), actual.getProfileId());
		assertEquals(expected.getBroadcastId(), actual.getBroadcastId());
		assertEquals(expected.getName(), actual.getName());
		assertEquals(expected.getDescription(), actual.getDescription());
		assertEquals(expected.getEmbedVideoHtml(), actual.getEmbedVideoHtml());
		assertEquals(expected.getBroadcastUrl(), actual.getBroadcastUrl());
		assertEquals(expected.getBroadcastStatus(), actual.getBroadcastStatus());
		assertEquals(expected.getBroadcastRecordStatus(), actual.getBroadcastRecordStatus());
		assertEquals(expected.getBroadcastType(), actual.getBroadcastType());
	}

	private static void assertBroadcastsEquals(Broadcast expected, Broadcast actual) {
		assertNotNull(actual);
		assertEquals(expected.getBroadcastId(), actual.getBroadcastId());
		assertEquals(expected.getProfileId(), actual.getProfileId());
		assertEquals(expected.getAuthorProfileId(), actual.getAuthorProfileId());
		assertEquals(expected.getName(), actual.getName());
		assertEquals(expected.getDescription(), actual.getDescription());
		assertEquals(Bbcode.transform(expected.getDescription()), actual.getDescriptionHtml());
		assertEquals(expected.getBroadcastStatus(), actual.getBroadcastStatus());
		assertEquals(expected.getBroadcastSubscriptionStatus(), actual.getBroadcastSubscriptionStatus());
		assertEquals(expected.getDefaultStreamId(), actual.getDefaultStreamId());
		assertEquals(expected.getTimeBegin(), actual.getTimeBegin());
		assertEquals(expected.getTimeEnd(), actual.getTimeEnd());
		assertEquals(expected.isAutoStart(), actual.isAutoStart());
		assertEquals(expected.isAutoSubscribe(), actual.isAutoSubscribe());
		assertEquals(expected.isArchived(), actual.isArchived());
		assertEquals(expected.getPausedImageId(), actual.getPausedImageId());
		assertEquals(expected.getPausedImageUrl(), actual.getPausedImageUrl());
		assertEquals(expected.isPausedCustomDescription(), actual.isPausedCustomDescription());
		assertEquals(expected.getPausedDescription(), actual.getPausedDescription());
		assertEquals(Bbcode.transform(expected.getPausedDescription()), actual.getPausedDescriptionHtml());
		assertEquals(expected.getFinishedImageId(), actual.getFinishedImageId());
		assertEquals(expected.getFinishedImageUrl(), actual.getFinishedImageUrl());
	}

	private static Broadcast createBroadcast(long eventId) {
		Broadcast broadcast = new Broadcast();
		broadcast.setProfileId(eventId);
		broadcast.setName("test name");
		broadcast.setDescription("test description");
		broadcast.setViewPermission(PermissionLevel.EVERYBODY);
		broadcast.setTimeBegin(new Date(new Date().getTime() + 1000000));
		broadcast.setTimeEnd(new Date(broadcast.getTimeBegin().getTime() + 20000));
		broadcast.setAutoStart(true);
		broadcast.setPausedCustomDescription(true);
		broadcast.setPausedDescription("paused state desc");
		return broadcast;
	}

	private static BroadcastStream createStream(long eventId, Broadcast broadcast) {
		BroadcastStream broadcastStream = new BroadcastStream();
		broadcastStream.setBroadcastId(broadcast.getBroadcastId());
		broadcastStream.setProfileId(eventId);
		broadcastStream.setBroadcastUrl("test url");
		broadcastStream.setTimeBegin(new Date(new Date().getTime() + 1000000));
		broadcastStream.setTimeEnd(new Date(broadcastStream.getTimeBegin().getTime() + 20000));
		return broadcastStream;
	}

	private static BroadcastGeoTargeting createBroadcastGeoTargeting(Broadcast broadcast, int countryId, int cityId, boolean allowed) {
		BroadcastGeoTargeting broadcastGeoTargeting = new BroadcastGeoTargeting();
		broadcastGeoTargeting.setProfileId(broadcast.getProfileId());
		broadcastGeoTargeting.setBroadcastId(broadcast.getBroadcastId());
		broadcastGeoTargeting.setCityId(cityId);
		broadcastGeoTargeting.setCountryId(countryId);
		broadcastGeoTargeting.setAllowed(allowed);
		return broadcastGeoTargeting;
	}
}

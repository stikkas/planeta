package ru.planeta.api.service.promo;

import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import ru.planeta.model.common.campaign.CampaignTag;
import ru.planeta.test.AbstractTest;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: michail
 * Date: 14.10.16
 * Time: 18:34
 */
@Ignore
public class PromoLetterServiceTest extends AbstractTest {
    @Autowired
    PromoLetterService promoLetterService;

    @Test
    public void testSome() {
        List<CampaignTag> tags = new ArrayList<>();
        tags.add(new CampaignTag(1));
        tags.add(new CampaignTag(2));
        tags.add(new CampaignTag(3));
        tags.add(new CampaignTag(4));
        tags.add(new CampaignTag(5));
        promoLetterService.sendPromoLetter("michail.michail@gmail.com", new BigDecimal(2000), tags, 100501);
    }

}

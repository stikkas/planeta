package ru.planeta.api.service.social;

import org.junit.Assert;
import org.junit.Test;
import ru.planeta.commons.web.social.vk.VkURIBuilder;
import ru.planeta.test.AbstractTest;

import java.util.ArrayList;
import java.util.List;

public class TestVkService extends AbstractTest {

    @Test
    public void testVKURIBuilder() throws Exception {

        List<Long> recipients = new ArrayList<>();
        recipients.add(111L);
        recipients.add(222L);

        VkURIBuilder vkURIBuilder = new VkURIBuilder("https://api.vk.com/method/secure.sendNotification");
        vkURIBuilder.addParameter("foo", true);
        vkURIBuilder.addParameter("recipients", recipients);
        vkURIBuilder.addParameter("bar", 999L);

        Assert.assertEquals("https://api.vk.com/method/secure.sendNotification?foo=true&bar=999&recipients=111,222", vkURIBuilder.toString());


        vkURIBuilder = new VkURIBuilder("https://api.vk.com/method/secure.sendNotification");
        vkURIBuilder.addParameter("foo", true);
        vkURIBuilder.addParameter("bar", 999L);

        Assert.assertEquals("https://api.vk.com/method/secure.sendNotification?foo=true&bar=999", vkURIBuilder.toString());
    }

}

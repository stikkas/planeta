package ru.planeta.api.service.shop;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.ibatis.exceptions.PersistenceException;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import ru.planeta.api.exceptions.NotFoundException;
import ru.planeta.model.shop.*;
import ru.planeta.model.shop.enums.DiscountType;
import ru.planeta.model.shop.enums.PromoCodeUsageType;
import ru.planeta.test.AbstractTest;

import java.util.*;

import static org.junit.Assert.*;

/**
 * Created with IntelliJ IDEA.
 * User: michail
 * Date: 28.03.16
 * Time: 12:00
 * To change this template use File | Settings | File Templates.
 */
public class TestPromoCodeService extends AbstractTest {
    @Autowired
    PromoCodeService promoCodeService;

    @Autowired
    ProductUsersService productUsersService;


    private HashMap<Long, Integer> getTestCart(int size, int tagId) {
        HashMap<Long, Integer> cart = new HashMap<>();
        List<ProductInfo> productList = productUsersService.getTopProducts(0, size, "", tagId, null);
        for (ProductInfo product : productList) {
            cart.put(product.getObjectId(), 1);
        }
        return cart;
    }

    private Product getTestProduct(HashMap<Long, Integer> cart) throws NotFoundException {
        Map.Entry<Long, Integer> entry = cart.entrySet().iterator().next();
        Product product = productUsersService.getProductSafe(entry.getKey());
        return product;
    }

    private PromoCode createTestPromoCode(String code) {
        Calendar calendarBegin = Calendar.getInstance();
        calendarBegin.set(Calendar.YEAR, 1970);
        Calendar calendarEnd = Calendar.getInstance();
        calendarEnd.set(Calendar.YEAR, 2100);

        PromoCode promoCode = new PromoCode();
        promoCode.setTitle("testTitle");
        promoCode.setCode(code);
        promoCode.setDiscountType(DiscountType.ABSOLUTE);
        promoCode.setDiscountAmount(100);
        promoCode.setFreeDelivery(true);
        promoCode.setTimeBegin(calendarBegin.getTime());    // unlimited
        promoCode.setTimeEnd(calendarEnd.getTime());        // unlimited
        promoCode.setMinOrderPrice(0);
        promoCode.setCodeUsageType(PromoCodeUsageType.UNLIMITED);
        promoCode.setUsageCount(1000000);                   // unlimited
        promoCode.setProfileId(0);
        promoCode.setProductTagId(0);
        return promoCode;
    }

    @Test
    public void testInsertPromoCode() throws Exception {
        PromoCode promoCode = createTestPromoCode("testCode");
        promoCode.setCodeUsageType(PromoCodeUsageType.PERSONAL);
        promoCodeService.insertPromoCode(promoCode);
        PromoCode actualPromoCode = promoCodeService.getPromoCode(promoCode.getCode());
        assertPromoCodesEquals(promoCode, actualPromoCode);
        assertNotNull(actualPromoCode.getTimeAdded());
    }

    @Test
    public void testUpdatePromoCode() throws Exception {
        PromoCode promoCode = createTestPromoCode("testCode");
        promoCodeService.insertPromoCode(promoCode);

        promoCode.setTitle("testTitle1");
        promoCode.setCode("testCode1");
        promoCode.setDiscountType(DiscountType.RELATIVE);
        promoCode.setDiscountAmount(200);
        promoCode.setFreeDelivery(false);
        promoCode.setTimeBegin(new Date());
        promoCode.setTimeEnd(new Date());
        promoCode.setMinOrderPrice(10001);
        promoCode.setCodeUsageTypeCode(1);
        promoCode.setUsageCount(500001);
        promoCode.setProfileId(1911);
        promoCode.setProductTagId(2);
        promoCodeService.updatePromoCode(promoCode);

        PromoCode actualPromoCode = promoCodeService.getPromoCode(promoCode.getCode());
        assertPromoCodesEquals(promoCode, actualPromoCode);
    }

    @Test
    public void testSelectCodeById() throws NotFoundException {
        PromoCode promoCode = createTestPromoCode("testCodeById");
        promoCodeService.insertPromoCode(promoCode);
        promoCode = promoCodeService.getPromoCode(promoCode.getCode());
        PromoCode actualPromoCode = promoCodeService.getPromoCodeById(promoCode.getPromoCodeId());
        assertPromoCodesEquals(promoCode, actualPromoCode);
    }

    @Test(expected = NotFoundException.class)
    public void testSelectEmptyPromoCode() throws NotFoundException {
        PromoCode promoCodeFromDb = promoCodeService.getPromoCode("");
        fail();
    }

    @Test(expected = NotFoundException.class)
    public void testSelectNonexistentPromoCode() throws NotFoundException {
        PromoCode promoCode = createTestPromoCode("testCodeInserted");
        promoCodeService.insertPromoCode(promoCode);
        promoCode.setCode("testCodeUpdated");
        promoCodeService.updatePromoCode(promoCode);
        PromoCode promoCodeFromDb = promoCodeService.getPromoCode("testCodeInserted");
        fail();
    }

    @Test(expected = PersistenceException.class)
    public void testInsertDuplicatePromoCode() throws Exception {
        PromoCode promoCode = new PromoCode("testTitle", "testCodeUnique");
        promoCodeService.insertPromoCode(promoCode);
        promoCodeService.insertPromoCode(promoCode);
        fail();
    }

    @Test
    public void testGeneratePromoCode() throws NotFoundException {
        PromoCode promoCode = new PromoCode("testGenerated", "");
        promoCodeService.generateOne(promoCode);
        PromoCode actualPromoCode = promoCodeService.getPromoCode(promoCode.getCode());
        assertPromoCodesEquals(promoCode, actualPromoCode);
    }

    private void assertPromoCodesEquals(PromoCode actual, PromoCode expected) {
        assertNotNull(actual);
        assertEquals(expected.getTitle(), actual.getTitle());
        assertEquals(expected.getCode(), actual.getCode());
        assertEquals(expected.getDiscountAmount(), actual.getDiscountAmount());
        assertEquals(expected.getDiscountType(), actual.getDiscountType());
        assertEquals(expected.isFreeDelivery(), actual.isFreeDelivery());
        assertEquals(expected.getTimeBegin(), actual.getTimeBegin());
        assertEquals(expected.getTimeEnd(), actual.getTimeEnd());
        assertEquals(expected.getMinOrderPrice(), actual.getMinOrderPrice());
        assertEquals(expected.getCodeUsageType(), actual.getCodeUsageType());
        assertEquals(expected.getUsageCount(), actual.getUsageCount());
        assertEquals(expected.getCodeUsageType(), actual.getCodeUsageType());
        assertEquals(expected.getPromoCodeId(), actual.getPromoCodeId());
    }


    @Test
    @Ignore
    public void calculateDiscountForFreeProducts() {
        HashMap<Long, Integer> cart = getTestCart(5, 1);
        cart.putAll(getTestCart(2, 2));
        for (Map.Entry<Long, Integer> entry : cart.entrySet()) {
            cart.put(entry.getKey(), 3);
        }
        PromoCode promoCode = createTestPromoCode("testProductFree");
        promoCode.setDiscountType(DiscountType.PRODUCTFREE);
        promoCode.setMinProductCount(7);
        promoCode.setDiscountAmount(2);
        promoCode.setProductTagId(1);
        promoCodeService.insertPromoCode(promoCode);
        Discount discount = promoCodeService.applyCode(191, promoCode.getCode(), cart);
        assertNotNull(discount);
        assertNotEquals(discount.getTotalPriceDiscount(), 0);

        promoCode.setProductTagId(2);
        promoCodeService.updatePromoCode(promoCode);
        discount = promoCodeService.applyCode(191, promoCode.getCode(), cart);
        assertNull(discount);
    }

    @Test
    public void calculateDiscount() throws NotFoundException {
        HashMap<Long, Integer> cart = getTestCart(1, 1);
        Product product = getTestProduct(cart);
        PromoCode promoCode = createTestPromoCode("testCodeDiscount");
        promoCode.setDiscountType(DiscountType.ABSOLUTE);
        promoCode.setDiscountAmount(1);
        promoCode.setFreeDelivery(false);
        promoCodeService.insertPromoCode(promoCode);
        Discount discount = promoCodeService.applyCode(191, promoCode.getCode(), cart);
        assertNotNull(discount);
        assertEquals(discount.isFreeDelivery(),false);
        assertEquals(discount.getTotalPriceDiscount(), promoCode.getDiscountAmount());

        promoCode.setDiscountAmount(product.getPrice().intValue() * 2);
        promoCodeService.updatePromoCode(promoCode);
        discount = promoCodeService.applyCode(191, promoCode.getCode(), cart);
        assertNotNull(discount);
        assertEquals(discount.isFreeDelivery(),false);
        assertEquals(discount.getTotalPriceDiscount(), product.getPrice().longValue());

        promoCode.setDiscountType(DiscountType.RELATIVE);
        promoCode.setDiscountAmount(10);
        promoCodeService.updatePromoCode(promoCode);
        discount = promoCodeService.applyCode(191, promoCode.getCode(), cart);
        assertNotNull(discount);
        assertEquals(discount.isFreeDelivery(),false);
        assertEquals(discount.getTotalPriceDiscount(),
                Math.round(product.getPrice().longValue() * promoCode.getDiscountAmount()/100));

        promoCode.setFreeDelivery(true);
        promoCodeService.updatePromoCode(promoCode);
        discount = promoCodeService.applyCode(191, promoCode.getCode(), cart);
        assertNotNull(discount);
        assertEquals(discount.isFreeDelivery(),true);
    }

    @Test
    public void validatePromoCodeByProductCount() throws NotFoundException {
        HashMap<Long, Integer> cart = getTestCart(10, 1);
        PromoCode promoCode = createTestPromoCode("testCodeValidateTag");

        promoCode.setMinProductCount(10);
        promoCodeService.insertPromoCode(promoCode);
        Discount discount = promoCodeService.applyCode(191, promoCode.getCode(), cart);
        assertNotNull(discount);

        promoCode.setMinProductCount(11);
        promoCodeService.updatePromoCode(promoCode);
        discount = promoCodeService.applyCode(191, promoCode.getCode(), cart);
        assertNull(discount);

        cart = getTestCart(1, 1);
        Product product = getTestProduct(cart);
        promoCode.setProductTagId(product.getTags().get(0).getCategoryId());
        promoCode.setMinProductCount(1);
        promoCodeService.updatePromoCode(promoCode);
        discount = promoCodeService.applyCode(191, promoCode.getCode(), cart);
        assertNotNull(discount);

        promoCode.setMinProductCount(0);
        promoCodeService.updatePromoCode(promoCode);
        discount = promoCodeService.applyCode(191, promoCode.getCode(), cart);
        assertNotNull(discount);

        promoCode.setProductTagId(product.getTags().get(0).getCategoryId() + 1);
        promoCode.setMinProductCount(1);
        promoCodeService.updatePromoCode(promoCode);
        discount = promoCodeService.applyCode(191, promoCode.getCode(), cart);
        assertNull(discount);
    }


    @Test
    public void validatePromoCodeByProductTag() throws NotFoundException {
        HashMap<Long, Integer> cart = getTestCart(1, 1);
        Product product = getTestProduct(cart);
        PromoCode promoCode = createTestPromoCode("testCodeValidateTag");
        promoCode.setProductTagId(product.getTags().get(0).getCategoryId() + 1);
        promoCodeService.insertPromoCode(promoCode);
        Discount discount = promoCodeService.applyCode(191, promoCode.getCode(), cart);
        assertNull(discount);

        promoCode.setProductTagId(product.getTags().get(0).getCategoryId());
        promoCodeService.updatePromoCode(promoCode);
        discount = promoCodeService.applyCode(191, promoCode.getCode(), cart);
        assertNotNull(discount);
    }

    @Test
    public void validatePromoCodeByTime() throws NotFoundException {
        Calendar calendarYesterday = Calendar.getInstance();
        calendarYesterday.add(Calendar.DATE, -1);
        Calendar calendarTomorrow = Calendar.getInstance();
        calendarTomorrow.add(Calendar.DATE, 1);

        HashMap<Long, Integer> cart = getTestCart(1, 1);
        PromoCode promoCode = createTestPromoCode("testCodeValidateDate");
        promoCode.setTimeBegin(calendarTomorrow.getTime());
        promoCode.setTimeEnd(calendarTomorrow.getTime());
        promoCodeService.insertPromoCode(promoCode);
        Discount discount = promoCodeService.applyCode(191, promoCode.getCode(), cart);
        assertNull(discount);

        promoCode.setTimeBegin(calendarYesterday.getTime());
        promoCode.setTimeEnd(calendarYesterday.getTime());
        promoCodeService.updatePromoCode(promoCode);
        discount = promoCodeService.applyCode(191, promoCode.getCode(), cart);
        assertNull(discount);

        promoCode.setTimeBegin(calendarTomorrow.getTime());
        promoCode.setTimeEnd(calendarYesterday.getTime());
        promoCodeService.updatePromoCode(promoCode);
        discount = promoCodeService.applyCode(191, promoCode.getCode(), cart);
        assertNull(discount);

        promoCode.setTimeBegin(calendarYesterday.getTime());
        promoCode.setTimeEnd(calendarTomorrow.getTime());
        promoCodeService.updatePromoCode(promoCode);
        discount = promoCodeService.applyCode(191, promoCode.getCode(), cart);
        assertNotNull(discount);
    }

    @Test
    public void validateUserLimitedPromoCode() throws NotFoundException {
        HashMap<Long, Integer> cart = getTestCart(1, 1);
        PromoCode promoCode = createTestPromoCode("testCodeValidateUserLimited");
        promoCode.setCodeUsageType(PromoCodeUsageType.PERSONAL);
        promoCode.setProfileId(13);
        promoCode.setUsageCount(1);
        promoCodeService.insertPromoCode(promoCode);
        Discount discount = promoCodeService.applyCode(191, promoCode.getCode(), cart);
        assertNull(discount);

        promoCode.setProfileId(191);
        promoCode.setUsageCount(0);
        promoCodeService.updatePromoCode(promoCode);
        discount = promoCodeService.applyCode(191, promoCode.getCode(), cart);
        assertNull(discount);

        promoCode.setProfileId(191);
        promoCode.setUsageCount(1);
        promoCodeService.updatePromoCode(promoCode);
        discount = promoCodeService.applyCode(191, promoCode.getCode(), cart);
        assertNotNull(discount);
    }

    @Test
    public void validateLimitedPromoCode() throws NotFoundException {
        HashMap<Long, Integer> cart = getTestCart(1, 1);
        PromoCode promoCode = createTestPromoCode("testCodeValidateLimited");
        promoCode.setCodeUsageType(PromoCodeUsageType.LIMITED);
        promoCode.setProfileId(13);
        promoCode.setUsageCount(0);
        promoCodeService.insertPromoCode(promoCode);
        Discount discount = promoCodeService.applyCode(191, promoCode.getCode(), cart);
        assertNull(discount);

        promoCode.setUsageCount(1);
        promoCodeService.updatePromoCode(promoCode);
        discount = promoCodeService.applyCode(191, promoCode.getCode(), cart);
        assertNotNull(discount);
    }

    @Test
    public void validateNonexistentPromoCode() {
        PromoCode promoCode = createTestPromoCode("testCodeValidateNonExistent");
        promoCodeService.insertPromoCode(promoCode);
        promoCode.setCode("testCodeValidateNonExistent1");
        promoCodeService.updatePromoCode(promoCode);
        HashMap<Long, Integer> cart = getTestCart(1, 1);
        Discount discount = promoCodeService.applyCode(191, "testCodeValidateNonExistent", cart);
        assertNull(discount);
    }

    @Test
    public void validateExistentPromoCode() throws NotFoundException {
        PromoCode promoCode = createTestPromoCode("testCodeValidateExistent");
        promoCodeService.insertPromoCode(promoCode);
        HashMap<Long, Integer> cart = getTestCart(1, 1);
        Discount discount = promoCodeService.applyCode(191, "testCodeValidateExistent", cart);
        assertNotNull(discount);
        assertEquals(discount.getPromoCodeId(),promoCodeService.getPromoCode(promoCode.getCode()).getPromoCodeId());
    }

    @Test
    public void checkPromoCodeUsage() throws NotFoundException {
        PromoCode promoCode = createTestPromoCode("testCodeUsage");
        promoCode.setUsageCount(10);
        promoCode.setCodeUsageType(PromoCodeUsageType.PERSONAL);
        promoCodeService.insertPromoCode(promoCode);
        promoCodeService.decreaseUsageCount(promoCode.getPromoCodeId());
        PromoCode actualPromoCode = promoCodeService.getPromoCode(promoCode.getCode());
        assertEquals(promoCode.getUsageCount() - 1, actualPromoCode.getUsageCount());

        promoCode.setUsageCount(10);
        promoCode.setCodeUsageType(PromoCodeUsageType.LIMITED);
        promoCodeService.updatePromoCode(promoCode);
        promoCodeService.decreaseUsageCount(promoCode.getPromoCodeId());
        actualPromoCode = promoCodeService.getPromoCode(promoCode.getCode());
        assertEquals(promoCode.getUsageCount() - 1, actualPromoCode.getUsageCount());

        promoCode.setUsageCount(10);
        promoCode.setCodeUsageType(PromoCodeUsageType.UNLIMITED);
        promoCodeService.updatePromoCode(promoCode);
        promoCodeService.decreaseUsageCount(promoCode.getPromoCodeId());
        actualPromoCode = promoCodeService.getPromoCode(promoCode.getCode());
        assertEquals(promoCode.getUsageCount(), actualPromoCode.getUsageCount());
    }

    @Test(expected = NotFoundException.class)
    public void checkPromoCodeCases() throws NotFoundException {
        String uppercase = "testUPPERCASE";
        PromoCode promoCode = createTestPromoCode(uppercase);
        promoCodeService.insertPromoCode(promoCode);
        PromoCode promoCodeFromDb1 = promoCodeService.getPromoCode(uppercase);
        assertNotNull(promoCodeFromDb1);

        promoCodeService.getPromoCode(uppercase.toLowerCase());
        fail();
    }

    @Test
    public void testSelectList() {
        int count = 10;
        String title = "testList_" + RandomStringUtils.randomAlphabetic(10);
        promoCodeService.generateList(new PromoCode(title),count);
        List<PromoCode> promoCodeList = promoCodeService.getPromoCodesList(title, 0, 0);
        assertEquals(promoCodeList.size(),count);
    }

    @Test
    public void testSelectExtendedList() {
        int count = 10;
        String title = "testExtendedList_" + RandomStringUtils.randomAlphabetic(10);
        promoCodeService.generateList(new PromoCode(title),count);
        List<ExtendedPromoCode> promoCodeList = promoCodeService.getExtendedPromoCodesList(title, 0, 0);
        assertEquals(promoCodeList.size(),count);
    }

    @Test
    public void testSelectExtended() throws NotFoundException {
        PromoCode promoCode = createTestPromoCode("testCodeValidateExistent");
        promoCodeService.insertPromoCode(promoCode);
        long codeId = promoCodeService.getPromoCode(promoCode.getCode()).getPromoCodeId();
        ExtendedPromoCode extendedPromoCode = promoCodeService.getExtendedPromoCodeById(codeId);
        assertPromoCodesEquals(promoCode, extendedPromoCode);
    }

    @Test(expected = NotFoundException.class)
    public void testDisabled() throws NotFoundException {
        PromoCode promoCode = createTestPromoCode("testDisabled");
        promoCodeService.insertPromoCode(promoCode);
        promoCode = promoCodeService.getPromoCode(promoCode.getCode());
        promoCodeService.markPromoCodeDisabled(promoCode.getPromoCodeId());
        ExtendedPromoCode extendedPromoCode = promoCodeService.getExtendedPromoCodeById(promoCode.getPromoCodeId());
        assertEquals(extendedPromoCode.isDisabled(), true);
        promoCode = promoCodeService.getPromoCode(promoCode.getCode());
        fail();
    }
}

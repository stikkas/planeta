package ru.planeta.api.service.common;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import ru.planeta.model.common.UserLastOnlineTime;
import ru.planeta.model.profile.Profile;
import ru.planeta.test.AbstractTest;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.*;

public class TestUserLastOnlineTimeService extends AbstractTest {

    @Autowired
    UserLastOnlineTimeService userLastOnlineTimeService;

    @Test
    public void testAddAndSelectUserLastOnlineTime() {
        Profile profile = insertUserProfile();
        UserLastOnlineTime userLastOnlineTime = new UserLastOnlineTime(profile.getProfileId(), new Date());

        UserLastOnlineTime selectedUserLastOnlineTime = userLastOnlineTimeService.getUserLastOnlineTime(profile.getProfileId());
        assertNull(selectedUserLastOnlineTime);

        int result = userLastOnlineTimeService.addOrUpdateUserLastOnlineTime(userLastOnlineTime);
        assertEquals(1, result);

        selectedUserLastOnlineTime = userLastOnlineTimeService.getUserLastOnlineTime(profile.getProfileId());
        assertNotNull(selectedUserLastOnlineTime);
        assertEquals(userLastOnlineTime.getProfileId(), selectedUserLastOnlineTime.getProfileId());
        assertEquals(userLastOnlineTime.getLastOnlineTime(), selectedUserLastOnlineTime.getLastOnlineTime());

        userLastOnlineTime.setLastOnlineTime(new Date());
        result = userLastOnlineTimeService.addOrUpdateUserLastOnlineTime(userLastOnlineTime);
        assertEquals(1, result);

        UserLastOnlineTime selectedUserLastOnlineTimeUpdated = userLastOnlineTimeService.getUserLastOnlineTime(profile.getProfileId());
        assertNotNull(selectedUserLastOnlineTimeUpdated);
        assertEquals(selectedUserLastOnlineTime.getProfileId(), selectedUserLastOnlineTimeUpdated.getProfileId());
        assertFalse(selectedUserLastOnlineTime.getLastOnlineTime().equals(selectedUserLastOnlineTimeUpdated.getLastOnlineTime()));
    }

    @Test
    public void testSelectList() {
        List<Long> allUsers = new ArrayList<Long>();
        List<Long> onlineUsers = new ArrayList<Long>();
        for (int i = 0; i < 5; i++) {
            Profile profile = insertUserProfile();
            allUsers.add(profile.getProfileId());
            if (i % 2 == 0) {
                userLastOnlineTimeService.addOrUpdateUserLastOnlineTime(new UserLastOnlineTime(profile.getProfileId(), new Date()));
                onlineUsers.add(profile.getProfileId());
            }
            List<UserLastOnlineTime> selectedUsers = userLastOnlineTimeService.getUsersLastOnlineTime(allUsers);
            assertTrue(compareList(selectedUsers,onlineUsers));
        }

    }

    private boolean compareList(List<UserLastOnlineTime> selectedUsers, List<Long> users) {
        if (selectedUsers.size() != users.size()) {
            return false;
        }
        for (Long userId : users) {
            boolean isFound = false;
            for (UserLastOnlineTime userLastOnlineTime : selectedUsers) {
                if (userLastOnlineTime.getProfileId() == userId) {
                    isFound = true;
                    break;
                }
            }
            if (!isFound) {
                return false;
            }
        }
        return true;
    }
}

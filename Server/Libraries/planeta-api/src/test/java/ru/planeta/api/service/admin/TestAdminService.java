package ru.planeta.api.service.admin;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import ru.planeta.api.exceptions.NotFoundException;
import ru.planeta.api.exceptions.PermissionException;
import ru.planeta.dao.profiledb.ProfileDAO;
import ru.planeta.model.common.UserPrivateInfo;
import ru.planeta.model.enums.ProfileStatus;
import ru.planeta.model.enums.ProfileType;
import ru.planeta.model.profile.Profile;
import ru.planeta.test.AbstractTest;

import static org.junit.Assert.*;

/**
 * Tests for admin service methods
 *
 * @author ds.kolyshev
 *         Date: 17.11.11
 */
public class TestAdminService extends AbstractTest {

    @Autowired
    private AdminService adminService;

    @Autowired
    private ProfileDAO profileDAO;

    @Test
    public void testGroups() throws NotFoundException, PermissionException {
        //insert profile
        UserPrivateInfo info = insertPlanetaAdminPrivateInfo("test@tratatata.com", "123");

        long clientId = info.getUserId();

        long profileId = registerSimpleGroup(clientId);

        //update status
        adminService.setProfileStatus(clientId, profileId, ProfileStatus.GROUP_ACTIVE);
        Profile updated = profileDAO.selectById(profileId);
        assertNotNull(updated);
        assertEquals(profileId, updated.getProfileId());
        assertEquals(ProfileType.GROUP, updated.getProfileType());
        assertEquals(ProfileStatus.GROUP_ACTIVE, updated.getStatus());

    }

}

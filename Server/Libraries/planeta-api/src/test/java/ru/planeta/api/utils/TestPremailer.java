package ru.planeta.api.utils;

import org.junit.Test;
import ru.planeta.api.model.json.PremailerResponse;
import ru.planeta.test.AbstractTest;
import static org.junit.Assert.assertEquals;

/**
 * User: s.makarov
 * Date: 13.05.14
 * Time: 19:00
 */
public class TestPremailer extends AbstractTest {

    @Test
    public void testDecodeImgSrcRegexp() {
        String escapedHtml = "<img src=\"http://s1.dev.planeta.ru/video-stub.jpg?duration=1284000&amp;name=%D1%87%D1%82%D0%BE+%D1%82%D0%B0%D0%BA%D0%BE%D0%B5+%D0%B1%D0%B5%D0%B9%D1%81%D0%B1%D0%BE%D0%BB&amp;url=http%3A%2F%2Fs2.dev.planeta.ru%2Fp%3Furl%3Dhttp%253A%252F%252Fi1.ytimg.com%252Fvi%252F8ucwht2V844%252F0.jpg%26amp%3Bwidth%3D400%26amp%3Bheight%3D300%26amp%3Bpad%3Dtrue\" style=\"max-width: 100%; border: 0;\">" +
                "<img src=\"http://s1.dev.planeta.ru/video-stub.jpg?duration=1284000&amp;name=%D1%87%D1%82%D0%BE+%D1%82%D0%B0%D0%BA%D0%BE%D0%B5+%D0%B1%D0%B5%D0%B9%D1%81%D0%B1%D0%BE%D0%BB&amp;url=http%3A%2F%2Fs2.dev.planeta.ru%2Fp%3Furl%3Dhttp%253A%252F%252Fi1.ytimg.com%252Fvi%252F8ucwht2V844%252F0.jpg%26amp%3Bwidth%3D400%26amp%3Bheight%3D300%26amp%3Bpad%3Dtrue\" style=\"max-width: 100%; border: 0;\">";
        String unEscapedHtml = "<img src=\"http://s1.dev.planeta.ru/video-stub.jpg?duration=1284000&name=%D1%87%D1%82%D0%BE+%D1%82%D0%B0%D0%BA%D0%BE%D0%B5+%D0%B1%D0%B5%D0%B9%D1%81%D0%B1%D0%BE%D0%BB&url=http%3A%2F%2Fs2.dev.planeta.ru%2Fp%3Furl%3Dhttp%253A%252F%252Fi1.ytimg.com%252Fvi%252F8ucwht2V844%252F0.jpg%26amp%3Bwidth%3D400%26amp%3Bheight%3D300%26amp%3Bpad%3Dtrue\" style=\"max-width: 100%; border: 0;\">" +
                "<img src=\"http://s1.dev.planeta.ru/video-stub.jpg?duration=1284000&name=%D1%87%D1%82%D0%BE+%D1%82%D0%B0%D0%BA%D0%BE%D0%B5+%D0%B1%D0%B5%D0%B9%D1%81%D0%B1%D0%BE%D0%BB&url=http%3A%2F%2Fs2.dev.planeta.ru%2Fp%3Furl%3Dhttp%253A%252F%252Fi1.ytimg.com%252Fvi%252F8ucwht2V844%252F0.jpg%26amp%3Bwidth%3D400%26amp%3Bheight%3D300%26amp%3Bpad%3Dtrue\" style=\"max-width: 100%; border: 0;\">";
        assertEquals(unEscapedHtml, PremailerResponse.Companion.unEscapedAmpersands(escapedHtml));
    }
}

package ru.planeta.api.service.common;

import org.apache.commons.lang3.time.DateUtils;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import ru.planeta.model.common.UserLoginInfo;
import ru.planeta.model.profile.Profile;
import ru.planeta.test.AbstractTest;

import java.util.Date;

import static org.junit.Assert.*;

/**
 * User: sshendyapin
 * Date: 28.01.13
 * Time: 20:33
 */
public class TestUserLoginInfoService extends AbstractTest {

    @Autowired
    UserLoginInfoService userLoginInfoService;

    @Test
    public void testAddAndSelectUserLoginInfo() {
        Date twoDaysBefore = DateUtils.addHours(new Date(), -2);

        UserLoginInfo userLoginInfo = new UserLoginInfo();
        Profile profile = insertUserProfile();

        userLoginInfo.setProfileId(profile.getProfileId());
        userLoginInfo.setUserIpAddress("127.0.0.1");
        userLoginInfo.setUserAgent("Mozilla/5.0 (Windows NT 6.1; WOW64; rv:18.0) Gecko/20100101 Firefox/18.0");
        userLoginInfo.setTimeLogin(twoDaysBefore);

        //checking that there is no such userLoginInfo
        UserLoginInfo selectedUserLoginInfo = userLoginInfoService.getUserLoginInfo(profile.getProfileId(), userLoginInfo.getUserIpAddress(), userLoginInfo.getUserAgent());
        assertNull(selectedUserLoginInfo);

        //checking that addOrUpdate method returns correct value
        int result = userLoginInfoService.addOrUpdateUserLoginInfo(userLoginInfo);
        assertEquals(1, result);


        selectedUserLoginInfo = userLoginInfoService.getUserLoginInfo(profile.getProfileId(), userLoginInfo.getUserIpAddress(), userLoginInfo.getUserAgent());
        assertNotNull(selectedUserLoginInfo);
        assertEquals(userLoginInfo.getProfileId(), selectedUserLoginInfo.getProfileId());
        assertEquals(userLoginInfo.getUserAgent(), selectedUserLoginInfo.getUserAgent());
        assertEquals(userLoginInfo.getUserIpAddress(), selectedUserLoginInfo.getUserIpAddress());
        assertEquals(userLoginInfo.getTimeLogin(), selectedUserLoginInfo.getTimeLogin());

        userLoginInfo.setTimeLogin(new Date());
        result = userLoginInfoService.addOrUpdateUserLoginInfo(userLoginInfo);
        assertEquals(1, result);

        UserLoginInfo selectedUserLoginInfoUpdated = userLoginInfoService.getUserLoginInfo(profile.getProfileId(), userLoginInfo.getUserIpAddress(), userLoginInfo.getUserAgent());
        assertNotNull(selectedUserLoginInfoUpdated);
        assertEquals(selectedUserLoginInfo.getProfileId(), selectedUserLoginInfoUpdated.getProfileId());
        assertEquals(selectedUserLoginInfo.getUserAgent(), selectedUserLoginInfoUpdated.getUserAgent());
        assertEquals(selectedUserLoginInfo.getUserIpAddress(), selectedUserLoginInfoUpdated.getUserIpAddress());
        assertNotSame(selectedUserLoginInfo.getTimeLogin(), selectedUserLoginInfoUpdated.getTimeLogin());
    }
}

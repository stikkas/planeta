package ru.planeta.api.service.shop;

import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import ru.planeta.api.exceptions.NotFoundException;
import ru.planeta.api.exceptions.PermissionException;
import ru.planeta.dao.shopdb.ProductDAO;
import ru.planeta.model.shop.Product;
import ru.planeta.model.shop.ProductInfo;
import ru.planeta.model.shop.enums.ProductStatus;
import ru.planeta.test.AbstractTest;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

/**
 * User: a.savanovich
 * Date: 28.06.12
 * Time: 16:17
 */

public class TestProductUsersService extends AbstractTest {

    @Autowired
    private ProductDAO productDao;

    @Test
    public void testCreateProduct() throws PermissionException, NotFoundException {
        long clientId = insertUserProfile().getProfileId();
        long merchantId = registerSimpleGroup(clientId);

        ProductInfo product = createProductMETA(merchantId);
        List<ProductInfo> childs = new ArrayList<ProductInfo>();
        childs.add(createChildProductVersion(product));

        childs.get(0).setProductAttribute(createProductAttribute("S"));
        product.setChildrenProducts(childs);
        productUsersService.saveProduct(clientId, product);

        // approve product, otherwise it won't be visible for user
        final long productId = product.getProductId();
        productDao.updateStatusPauseToActive(productId);
        product.setProductStatus(ProductStatus.ACTIVE);

        //assertProductsEquals(products.get(0), product);

        ProductInfo selected = productUsersService.getProductCurrent(clientId, productId);
        assertProductsEquals(selected, product);
        assertNotNull(selected.getChildrenProducts());

        //test getProductSafe  on product
        ProductInfo product1 = createProductMETA(merchantId);
        productUsersService.saveProduct(merchantId, product1);

        productDao.updateStatusCurrentToOld(product1.getProductId(), false);
        try {
            productUsersService.getProductCurrent(clientId, product1.getProductId());
            fail();
        } catch (NotFoundException ignored) {

        }
            /* TODO think about it
						productUsersService.deleteProduct(merchantId,product1.getProductId(),product1.getVersion());
						product1 = productUsersService.getProductSafe(merchantId,product1.getProductId());
						assertEquals(product1.getProductStatus(),ProductStatus.DELETED);
						assertNull("Deleted product",product1);*/
    }

    private static void assertProductsEquals(Product actual, Product expected) {
        assertNotNull(actual);
        assertEquals(expected.getProductId(), actual.getProductId());
        assertEquals(expected.getMerchantProfileId(), actual.getMerchantProfileId());
        assertEquals("ppId", expected.getParentProductId(), actual.getParentProductId());
        assertEquals(expected.getName(), actual.getName());
        assertEquals(expected.getProductStatus(), actual.getProductStatus());
        assertEquals(expected.getDescription(), actual.getDescription());
        assertEquals(expected.getStoreId(), actual.getStoreId());
        assertEquals(expected.getProductState(), actual.getProductState());
        // check attribute
        if (expected.getProductAttribute() != null) {
            assertNotNull(actual.getProductAttribute());
            assertEquals(expected.getProductAttribute().getAttributeTypeId(), actual.getProductAttribute().getAttributeTypeId());
            assertEquals(expected.getProductAttribute().getValue(), actual.getProductAttribute().getValue());
        }
    }

    @Test
    @Ignore
    public void testProductCampaigns() throws NotFoundException {
        long[] list = new long[2];
        list[0] = 111L;
        list[1] = 222L;
        productUsersService.bindToProfile(1070L, 191L, false, list);

        Product p = productUsersService.getProductCurrent(191L, 1070L);
        int a = 1;
    }
}

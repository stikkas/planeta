package ru.planeta.api.text;

import org.apache.commons.lang3.StringUtils;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import ru.planeta.api.service.campaign.CampaignService;
import ru.planeta.model.common.campaign.Campaign;
import ru.planeta.test.AbstractTest;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Tests for message parser
 */
public class TestMessageParser extends AbstractTest {

    @Autowired
    private MessageParserService messageParserService;

    private static final String TEST_TEXT = "Multiple pictures here:\n" +
            "http://sincere.ly/mark/files/2012/02/three-packs-of-dollars.jpeg,\n" +
            "https://lh4.googleusercontent.com/-NhLv11d7DY4/Tzi1lPrLeYI/AAAAAAAAGB0/0nDm5OogsAI/w402/Men_and_Women.jpg,\n" +
            "https://lh3.googleusercontent.com/-6FgsUF4MaqA/TzghAestnmI/AAAAAAAAAlE/FDFxQre4S0w/h301/IMG_20120212_200442.jpg\n" +
            "https://images1-focus-opensocial.googleusercontent.com/gadgets/proxy?url=https%3A%2F%2Fsincere.ly%2Fmark%2Ffiles%2F2012%2F02%2Fthree-packs-of-dollars.jpeg&container=focus&gadget=a&rewriteMime=image%2F%2A%26refresh=31536000&resize_h=120&no_expand=1\n" +
            "http://www.youtube.com/watch?v=ILTgRN-UvpY&feature=g-logo&context=G21eff32FOAAAAAAAAAA\n" +
            "https://www.youtube.com/watch?v=J9dsuLx3Ukw&feature=related";


    @Test
    public void testExtractMessageParts() {

        List<MessagePart> messageParts = messageParserService.extractMessageParts(TEST_TEXT);

        assertNotNull(messageParts);
        assertTrue(messageParts.size() > 6);
        assertTrue(messageParts.get(0).getText().equals("Multiple pictures here:\n"));

        List<String> urls = new ArrayList<String>();
        for (MessagePart part : messageParts) {
            if (part.getMessagePartType() == MessagePartType.LINK) {
                urls.add(part.getText());
            }
        }
        assertEquals(6, urls.size());
        assertTrue(urls.contains("http://sincere.ly/mark/files/2012/02/three-packs-of-dollars.jpeg"));
        assertTrue(urls.contains("https://lh4.googleusercontent.com/-NhLv11d7DY4/Tzi1lPrLeYI/AAAAAAAAGB0/0nDm5OogsAI/w402/Men_and_Women.jpg"));
        assertTrue(urls.contains("https://lh3.googleusercontent.com/-6FgsUF4MaqA/TzghAestnmI/AAAAAAAAAlE/FDFxQre4S0w/h301/IMG_20120212_200442.jpg"));
        assertTrue(urls.contains("https://images1-focus-opensocial.googleusercontent.com/gadgets/proxy?url=https%3A%2F%2Fsincere.ly%2Fmark%2Ffiles%2F2012%2F02%2Fthree-packs-of-dollars.jpeg&container=focus&gadget=a&rewriteMime=image%2F%2A%26refresh=31536000&resize_h=120&no_expand=1"));
        assertTrue(urls.contains("http://www.youtube.com/watch?v=ILTgRN-UvpY&feature=g-logo&context=G21eff32FOAAAAAAAAAA"));
        assertTrue(urls.contains("https://www.youtube.com/watch?v=J9dsuLx3Ukw&feature=related"));
    }

    @Test
    public void testParseMessage() {
        Message message = messageParserService.extractAttachments(TEST_TEXT);
        assertNotNull(message);
        // This is unstable, im tired
//        assertEquals(4, message.getImageAttachments().size());
        assertEquals(2, message.getVideoAttachments().size());
    }

    @Test
    public void testParseSpecialLinksMessage() throws UnsupportedEncodingException {
        String url1 = "http://www.youtube.com/watch?v=4wxjtds28fE&feature=related";
        // http://ru.wikipedia.org/wiki/Незаконное_простое_число
        String url2 = URLDecoder.decode("http%3A%2F%2Fru.wikipedia.org%2Fwiki%2F%D0%9D%D0%B5%D0%B7%D0%B0%D0%BA%D0%BE%D0%BD%D0%BD%D0%BE%D0%B5_%D0%BF%D1%80%D0%BE%D1%81%D1%82%D0%BE%D0%B5_%D1%87%D0%B8%D1%81%D0%BB%D0%BE", "utf-8");
        String url3 = "http://www.lastfm.ru/music/The+Beatles/The+Beatles+(The+White+Album)";
        String testText = StringUtils.join(new String[]{url1, url2, url3}, " ,");

        List<MessagePart> messageParts = messageParserService.extractMessageParts(testText);

        assertNotNull(messageParts);

        List<String> urls = new ArrayList<String>();
        for (MessagePart part : messageParts) {
            if (part.getMessagePartType() == MessagePartType.LINK) {
                urls.add(part.getText());
            }
        }

        assertTrue(urls.contains(url1));
        assertTrue(urls.contains(url2));
        assertTrue(urls.contains(url3));
    }

    @Test
    public void testLinks() throws Exception {
        String text = "Did you read http://www.youtube.com/watch?v=4wxjtds28fE&feature=related? Or http://www.youtube.com/watch? Or our planeta.ru url http://planeta.ru/23709/blog/111!comment123?";
        List<MessagePart> messageParts = messageParserService.extractMessageParts(text);

        assertNotNull(messageParts);

        List<String> urls = new ArrayList<String>();
        for (MessagePart part : messageParts) {
            if (part.getMessagePartType() == MessagePartType.LINK) {
                urls.add(part.getText());
            }
        }

        assertTrue(urls.contains("http://www.youtube.com/watch?v=4wxjtds28fE&feature=related"));
        assertTrue(urls.contains("http://planeta.ru/23709/blog/111!comment123"));
        assertTrue(urls.contains("http://www.youtube.com/watch"));
    }

    @Test
    public void testEmail() throws Exception {
        String emailText = " kalmykov.sergei@gmail.com  jkjkj ";
        List<MessagePart> messageParts = messageParserService.extractMessageParts(emailText);

        assertNotNull(messageParts);

        List<String> urls = new ArrayList<String>();
        for (MessagePart part : messageParts) {
            if (part.getMessagePartType() == MessagePartType.LINK) {
                urls.add(part.getText());
            }
        }

        assertTrue(urls.isEmpty());

    }
}

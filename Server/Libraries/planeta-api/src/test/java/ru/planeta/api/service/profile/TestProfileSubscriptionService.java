package ru.planeta.api.service.profile;

import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import ru.planeta.api.exceptions.NotFoundException;
import ru.planeta.api.exceptions.PermissionException;
import ru.planeta.commons.model.Gender;
import ru.planeta.model.common.UserPrivateInfo;
import ru.planeta.model.enums.*;
import ru.planeta.model.profile.Profile;
import ru.planeta.model.profile.ProfileRelationShort;
import ru.planeta.model.profile.ProfileSubscriptionInfo;
import ru.planeta.test.AbstractTest;

import java.util.*;

import static org.junit.Assert.*;
import static org.junit.Assert.assertTrue;

public class TestProfileSubscriptionService extends AbstractTest {

    @Autowired
    private ProfileSubscriptionService profileSubscriptionService;
    @Autowired
    private ProfileLastVisitService profileLastVisitService;

    @Test
    public void testSubscribeUnsubscribe() {
        Profile firstProfile = createProfile();
        Profile secondProfile = createProfile();

        profileSubscriptionService.subscribe(firstProfile.getProfileId(), secondProfile.getProfileId());
        assertTrue(profileSubscriptionService.doISubscribeYou(firstProfile.getProfileId(), secondProfile.getProfileId()));
        assertFalse(profileSubscriptionService.doISubscribeYou(secondProfile.getProfileId(), firstProfile.getProfileId()));

        profileSubscriptionService.unsubscribe(firstProfile.getProfileId(), secondProfile.getProfileId());
        assertFalse(profileSubscriptionService.doISubscribeYou(firstProfile.getProfileId(), secondProfile.getProfileId()));
        assertFalse(profileSubscriptionService.doISubscribeYou(secondProfile.getProfileId(), firstProfile.getProfileId()));

        profileSubscriptionService.subscribe(firstProfile.getProfileId(), secondProfile.getProfileId());
        assertTrue(profileSubscriptionService.doISubscribeYou(firstProfile.getProfileId(), secondProfile.getProfileId()));
        assertFalse(profileSubscriptionService.doISubscribeYou(secondProfile.getProfileId(), firstProfile.getProfileId()));

        profileSubscriptionService.subscribe(firstProfile.getProfileId(), secondProfile.getProfileId());
        assertTrue(profileSubscriptionService.doISubscribeYou(firstProfile.getProfileId(), secondProfile.getProfileId()));
        assertFalse(profileSubscriptionService.doISubscribeYou(secondProfile.getProfileId(), firstProfile.getProfileId()));

    }
    @Test
    public void testSubscribersCount() {
        Profile profile = createProfile();
        List<Profile> list = new ArrayList<Profile>();
        for (int i = 0; i < 3; i++) {
            Profile otherProfile = createProfile();
            list.add(otherProfile);
            profileSubscriptionService.subscribe(otherProfile.getProfileId(), profile.getProfileId());
            assertEquals(i + 1, profileSubscriptionService.getSubscribersCount(profile.getProfileId()));
        }
        for (int i = 0; i < 3; i++) {
            profileSubscriptionService.unsubscribe(list.get(i).getProfileId(), profile.getProfileId());
            assertEquals(2 - i, profileSubscriptionService.getSubscribersCount(profile.getProfileId()));
        }
    }

    @Test
    public void testSubscriptionsCount() {
        Profile profile = createProfile();
        List<Profile> list = new ArrayList<Profile>();
        for (int i = 0; i < 3; i++) {
            Profile otherProfile = createProfile();
            list.add(otherProfile);
            profileSubscriptionService.subscribe(profile.getProfileId(), otherProfile.getProfileId());
            assertEquals(i + 1, profileSubscriptionService.getSubscriptionsCount(profile.getProfileId()));
        }
        for (int i = 0; i < 3; i++) {
            profileSubscriptionService.unsubscribe(profile.getProfileId(), list.get(i).getProfileId());
            assertEquals(2 - i, profileSubscriptionService.getSubscriptionsCount(profile.getProfileId()));
        }
    }

    @Test
    public void testNewSubscribersCount() {
        Profile profile = createProfile();
        for (int i = 0; i < 3; i++) {
            profileSubscriptionService.subscribe(createProfile().getProfileId(), profile.getProfileId());
            assertEquals(i + 1, profileSubscriptionService.getNewSubscribersCount(profile.getProfileId()));
        }

        profileLastVisitService.insertOrUpdateProfileLastVisit(profile.getProfileId(), ProfileLastVisitType.SUBSCRIBERS_PAGE);

        for (int i = 0; i < 3; i++) {
            profileSubscriptionService.subscribe(createProfile().getProfileId(), profile.getProfileId());
            assertEquals(i + 1, profileSubscriptionService.getNewSubscribersCount(profile.getProfileId()));
        }
    }

    @Test
    public void testSelectSubscriberList() {
        Profile profile = createProfile();

        List<Profile> list = new ArrayList<Profile>();
        for (int i = 0; i < 3; i++) {
            Profile otherProfile = createProfile();
            list.add(otherProfile);
            profileSubscriptionService.subscribe(otherProfile.getProfileId(), profile.getProfileId());
        }


        List<ProfileSubscriptionInfo> selectedList = profileSubscriptionService.getSubscriberList(profile.getProfileId(), "", false, null, 0, 20);
        assertEquals(3, selectedList.size());
        for (int i = 0; i < 3; i++) {
            compareProfile(list.get(2 - i), selectedList.get(i));
        }

        Profile firstProfile = list.get(0);
        firstProfile.setDisplayName("name first profile");
        getProfileDAO().update(firstProfile);

        selectedList = profileSubscriptionService.getSubscriberList(profile.getProfileId(), "first", false, null, 0, 20);
        assertEquals(1, selectedList.size());
        compareProfile(firstProfile, selectedList.get(0));


        Date date = new Date();
        Profile lastProfile = createProfile();
        list.add(lastProfile);
        profileSubscriptionService.subscribe(lastProfile.getProfileId(), profile.getProfileId());
        selectedList = profileSubscriptionService.getSubscriberList(profile.getProfileId(), null, false, date, 0, 20);
        assertEquals(4, selectedList.size());
        for (int i = 0; i < 4; i++) {
            compareProfile(list.get(3 - i), selectedList.get(i));

        }
//        assertTrue(selectedList.get(0).getIsNew());
//        assertFalse(selectedList.get(1).getIsNew());
//        assertFalse(selectedList.get(2).getIsNew());
//        assertFalse(selectedList.get(3).getIsNew());

    }

    @Test
    public void testSelectSubscriptionList() {
        Profile profile = createProfile();

        List<Profile> list = new ArrayList<Profile>();
        for (int i = 0; i < 3; i++) {
            Profile otherProfile = createProfile();
            list.add(otherProfile);
            profileSubscriptionService.subscribe(profile.getProfileId(), otherProfile.getProfileId());
        }

        List<ProfileSubscriptionInfo> selectedList = profileSubscriptionService.getSubscriptionList(profile.getProfileId(), "", false, 0, 20);
        assertEquals(3, selectedList.size());
        for (int i = 0; i < 3; i++) {
            compareProfile(list.get(2 - i), selectedList.get(i));
        }


        Profile firstProfile = list.get(0);
        firstProfile.setDisplayName("name first profile");
        getProfileDAO().update(firstProfile);

        selectedList = profileSubscriptionService.getSubscriptionList(profile.getProfileId(), "first", false, 0, 20);
        assertEquals(1, selectedList.size());
        compareProfile(firstProfile, selectedList.get(0));

    }

    @Test
    public void testSetAdmin() throws NotFoundException, PermissionException {
        long p1 = createProfile().getProfileId();
        long p2 = createProfile().getProfileId();
        profileSubscriptionService.subscribe(p1, p2);
        profileSubscriptionService.toggleAdmin(p1, p2, true);
        assertEquals(1, profileSubscriptionService.getAdminSubscribersCount(p2));
        assertEquals(0, profileSubscriptionService.getAdminSubscriptionsCount(p2));
        assertEquals(0, profileSubscriptionService.getAdminSubscribersCount(p1));
        assertEquals(1, profileSubscriptionService.getAdminSubscriptionsCount(p1));

        profileSubscriptionService.toggleAdmin(p1, p2, false);
        assertEquals(0, profileSubscriptionService.getAdminSubscribersCount(p2));
        assertEquals(0, profileSubscriptionService.getAdminSubscriptionsCount(p2));
        assertEquals(0, profileSubscriptionService.getAdminSubscribersCount(p1));
        assertEquals(0, profileSubscriptionService.getAdminSubscriptionsCount(p1));


        profileSubscriptionService.toggleAdmin(p1, p2, true);

        long p3 = createProfile().getProfileId();
        profileSubscriptionService.subscribe(p3, p2);
        profileSubscriptionService.toggleAdmin(p3, p2, true);

        assertEquals(2, profileSubscriptionService.getAdminSubscribersCount(p2));

        List<ProfileSubscriptionInfo> list = profileSubscriptionService.getSubscriberList(p2, null, true, null, 0, 0);
        assertEquals(2, list.size());
//        assertTrue(list.get(0).getIsAdmin());
//        assertTrue(list.get(1).getIsAdmin());

        profileSubscriptionService.toggleAdmin(p3, p2, false);
        assertEquals(1, profileSubscriptionService.getAdminSubscribersCount(p2));

        list = profileSubscriptionService.getSubscriberList(p2, null, null, null, 0, 0);
        assertEquals(2, list.size());
//        assertFalse(list.get(0).getIsAdmin());
//        assertTrue(list.get(1).getIsAdmin());

    }

    private void compareProfile(Profile expectedProfile, Profile actualProfile) {
        assertEquals(expectedProfile.getProfileId(), actualProfile.getProfileId());
        assertEquals(expectedProfile.getProfileType(), actualProfile.getProfileType());
        assertEquals(expectedProfile.getImageUrl(), actualProfile.getImageUrl());
        assertEquals(expectedProfile.getImageId(), actualProfile.getImageId());
        assertEquals(expectedProfile.getDisplayName(), actualProfile.getDisplayName());
        assertEquals(expectedProfile.getAlias(), actualProfile.getAlias());
        assertEquals(expectedProfile.getStatus(), actualProfile.getStatus());
        assertEquals(expectedProfile.getCityId(), actualProfile.getCityId());
        assertEquals(expectedProfile.getCountryId(), actualProfile.getCountryId());
        assertEquals(expectedProfile.getUserBirthDate(), actualProfile.getUserBirthDate());
        assertEquals(expectedProfile.getUserGender(), actualProfile.getUserGender());
        assertEquals(expectedProfile.getGroupCategory(), actualProfile.getGroupCategory());

    }


    private Profile createProfile() {
        Profile profile = new Profile();
        profile.setProfileType(ProfileType.USER);
        profile.setImageUrl("/imageurl" + Long.toString(new Random().nextInt()));
        profile.setImageId(1);
        profile.setDisplayName("name" + Long.toString(new Random().nextInt()));
        profile.setAlias("alias" + Long.toString(new Random().nextInt()));
        profile.setStatus(ProfileStatus.NOT_SET);
        profile.setCityId(1);
        profile.setCountryId(1);
        profile.setUserBirthDate(new Date());
        profile.setUserGender(Gender.NOT_SET);
        profile.setGroupCategory(GroupCategory.MUSIC);
        getProfileDAO().insert(profile);
        return profile;
    }



//    @Test
//    public void testGetProfileRelationShortList(){
//        UserPrivateInfo user1 = insertUserPrivateInfo("" + Math.random(), "" + Math.random());
//        UserPrivateInfo user2 = insertPlanetaAdminPrivateInfo("" + Math.random(), "" + Math.random());
//
//
//        List<ProfileRelationShort> list = profileSubscriptionService.getProfileRelationShortList(user1.getUserId(), Arrays.asList(user1.getUserId(), user2.getUserId()));
//        assertNotNull(list);
//        assertEquals(2, list.size());
//        assertEquals(user1.getUserId(), list.get(0).getProfileId());
//        assertEquals(user2.getUserId(), list.get(1).getProfileId());
//        System.out.println();
//
//    }

    @Ignore
    @Test
    public void testGetSubscrCount() {
        long profileId = 21765;

        long start = System.currentTimeMillis();
        long subscribersCount = profileSubscriptionService.getSubscribersCount(profileId);
        long step1length = System.currentTimeMillis() - start;

        start = System.currentTimeMillis();
        long newSubscribersCount = profileSubscriptionService.getNewSubscribersCount(profileId);
        long step2length = System.currentTimeMillis() - start;

        start = System.currentTimeMillis();
        long subscriptionsCount = profileSubscriptionService.getSubscriptionsCount(profileId);
        long step3length = System.currentTimeMillis() - start;

        long total = step1length + step2length + step3length;
    }
}

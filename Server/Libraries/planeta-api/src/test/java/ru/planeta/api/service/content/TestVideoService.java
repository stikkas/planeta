package ru.planeta.api.service.content;

import org.junit.Ignore;
import org.junit.Test;
import org.sphx.api.SphinxException;
import org.springframework.beans.factory.annotation.Autowired;
import ru.planeta.api.exceptions.NotFoundException;
import ru.planeta.api.exceptions.PermissionException;
import ru.planeta.api.model.json.VideoInfo;
import ru.planeta.api.model.json.VideoInfoForFlash;
import ru.planeta.dao.profiledb.PhotoAlbumDAO;
import ru.planeta.dao.profiledb.VideoDAO;
import ru.planeta.model.enums.PermissionLevel;
import ru.planeta.model.profile.media.Video;
import ru.planeta.model.profile.media.enums.VideoConversionStatus;
import ru.planeta.model.profile.media.enums.VideoQuality;
import ru.planeta.test.AbstractTest;

import java.util.Date;
import java.util.EnumSet;

import static org.junit.Assert.*;

/**
 * @author ds.kolyshev
 * Date: 28.10.11
 */
public class TestVideoService extends AbstractTest {

	@Autowired
	private VideoService videoService;
	@Autowired
	private VideoDAO videoDAO;
	@Autowired
	private PhotoAlbumDAO photoAlbumDAO;

    @Test
    public void testVideoService() throws NotFoundException, PermissionException, SphinxException {
        
        
            long profileId = insertUserProfile().getProfileId();

            Video video = createNewVideo(profileId, profileId);
            videoDAO.insert(video);

            VideoInfo insertedVideo = videoService.getVideo(profileId, video.getProfileId(), video.getVideoId());
            assertNotNull(insertedVideo);
            checkVideos(video, insertedVideo);

            VideoInfoForFlash extendedInfo = videoService.getVideoInfoExtended(profileId, video.getProfileId(), video.getVideoId());
			assertNotNull(extendedInfo);
			checkVideos(video, extendedInfo);

            video.setDescription("New video description");
            Video savedVideo = videoService.saveVideo(video.getProfileId(), video);
            assertNotNull(savedVideo);
            assertEquals(video.getDescription(), savedVideo.getDescription());

            videoService.deleteVideo(profileId, savedVideo.getProfileId(), savedVideo.getVideoId());
            //assertNull(videoService.getVideo(clientId, video.getProfileId(), video.getVideoId()));

        
    }

	@Test
    @Ignore
    public void testYoutube() throws Exception {
        
        
            long profileId = insertUserProfile().getProfileId();
            // +100500 video with bad chars
            VideoInfo video = videoService.addExternalVideo(profileId, profileId, "http://www.youtube.com/watch?v=zTVfQtLMQQw", null);
            assertTrue(video != null);

        

    }

    private void checkVideos(Video expected, VideoInfo actual) {
        assertNotNull(actual);
        assertEquals(expected.getProfileId(), actual.getProfileId());
        assertEquals(expected.getVideoId(), actual.getVideoId());
        assertEquals(expected.getAuthorProfileId(), actual.getAuthorProfileId());
        assertEquals(expected.getDescription(), actual.getDescription());
        assertEquals(expected.getDownloadsCount(), actual.getDownloadsCount());
        assertEquals(expected.getViewsCount(), actual.getViewsCount());
        assertEquals(expected.getVideoUrl(), actual.getVideoUrl());
        assertEquals(expected.getName(), actual.getName());
        assertEquals(expected.getStatus(), actual.getStatus());
        assertEquals(expected.getImageUrl(), actual.getImageUrl());
        assertEquals(expected.getImageId(), actual.getImageId());
        assertTrue(expected.getAvailableQualities().containsAll(actual.getAvailableQualities()));
        assertTrue(actual.getAvailableQualities().containsAll(expected.getAvailableQualities()));
        assertNotNull(expected.getTimeAdded());
        assertNotNull(expected.getTimeUpdated());
    }

	private static Video createNewVideo(long profileId, long clientId) {
		Video video = new Video();
		video.setProfileId(profileId);
		video.setAuthorProfileId(clientId);
		video.setViewsCount(0);
		video.setDescription("description");
		video.setName("test video name");
		video.setAvailableQualities(EnumSet.of(VideoQuality.MODE_480P));
		video.setVideoUrl("video url");
		video.setImageId(199999);
		video.setImageUrl("http://test_video/url");
		video.setStatus(VideoConversionStatus.PROCESSING);
		video.setTimeAdded(new Date());
		video.setTimeUpdated(new Date());
		video.setViewPermission(PermissionLevel.EVERYBODY);
		return video;
	}
}

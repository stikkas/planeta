package ru.planeta.api.service.social;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import ru.planeta.api.exceptions.NotFoundException;
import ru.planeta.api.exceptions.PermissionException;
import ru.planeta.model.common.ShareServiceType;
import ru.planeta.model.stat.SharedObject;
import ru.planeta.model.stat.SharedObjectStats;
import ru.planeta.test.AbstractTest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * User: m.shulepov
 * Date: 21.06.12
 * Time: 14:05
 */
public class TestSharedObjectService extends AbstractTest {

	@Autowired
    private SharedObjectService sharedObjectService;

    @Test
    public void testProcessSharedObject() throws PermissionException, NotFoundException {
        
        
            long profileOne = insertUserProfile().getProfileId();
            long profileTwo = insertUserProfile().getProfileId();

            String sharedUrl = "https://planeta.ru/campaigns/testcampaign1";

            // share object on FACEBOOK with profileOne
            SharedObject sharedObject = createSharedObject(profileOne, sharedUrl, ShareServiceType.FACEBOOK);
            sharedObjectService.processSharedObject(profileOne, null, sharedObject);

            // share object on TWITTER with profileOne
            sharedObject = createSharedObject(profileOne, sharedUrl, ShareServiceType.TWITTER);
            sharedObjectService.processSharedObject(profileOne, null, sharedObject);

            // share object on TWITTER with profileTwo
            sharedObject = createSharedObject(profileTwo, sharedUrl, ShareServiceType.TWITTER);
            sharedObjectService.processSharedObject(profileTwo, null, sharedObject);

            SharedObjectStats selected = sharedObjectService.getSharedObjectByUrl(sharedUrl);
            assertNotNull(selected);
            assertEquals(sharedObject.getUrl(), selected.getUrl());
            assertEquals(3, selected.getCount());

            // share object on TWITTER with profileTwo
            sharedObject = createSharedObject(profileTwo, sharedUrl, ShareServiceType.TWITTER);
            sharedObjectService.processSharedObject(profileTwo, null, sharedObject);

            selected = sharedObjectService.getSharedObjectByUrl(sharedUrl);
            assertNotNull(selected);
            assertEquals(sharedObject.getUrl(), selected.getUrl());
            assertEquals(3, selected.getCount());
        
    }

    private static SharedObject createSharedObject(long clientId, String url, ShareServiceType shareServiceType) {
        SharedObject sharedObject = new SharedObject();
        sharedObject.setAuthorProfileId(clientId);
        sharedObject.setUrl(url);
        sharedObject.setShareServiceType(shareServiceType);
        return sharedObject;
    }

}

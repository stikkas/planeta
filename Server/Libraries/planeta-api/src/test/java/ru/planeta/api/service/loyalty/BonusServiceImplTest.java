/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.planeta.api.service.loyalty;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import ru.planeta.api.exceptions.NotFoundException;
import ru.planeta.model.common.loyalty.Bonus;
import ru.planeta.test.AbstractTest;

/**
 * @author Serge Blagodatskih<stikkas17@gmail.com>
 */
public class BonusServiceImplTest extends AbstractTest {

    private long wrongId = -172;

    @Autowired
    private BonusService bonusService;

    @Test
    public void testGetBonusSafe() throws Exception {
        try {
            bonusService.getBonusSafe(wrongId);
            fail("Expected an NotFoundException to be thrown");
        } catch (NotFoundException ex) {
            assertThat(ex.getMessage(), is(String.format("%s with id %d not found.", Bonus.class.getSimpleName(), wrongId)));
        }
    }
}

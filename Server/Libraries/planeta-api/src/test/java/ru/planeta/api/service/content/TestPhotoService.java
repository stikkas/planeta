package ru.planeta.api.service.content;

import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import ru.planeta.api.exceptions.NotFoundException;
import ru.planeta.api.exceptions.PermissionException;
import ru.planeta.model.enums.PermissionLevel;
import ru.planeta.model.profile.media.Photo;
import ru.planeta.model.profile.media.PhotoAlbum;
import ru.planeta.test.AbstractTest;

import static org.junit.Assert.*;

/**
 * @author ds.kolyshev
 *         Date: 27.10.11
 */
public class TestPhotoService extends AbstractTest {

	@Autowired
	private PhotoService photoService;

	@Test
	@Ignore
	public void testPhotoService() throws NotFoundException, PermissionException {

		long profileId = insertUserProfile().getProfileId();
		long clientId = profileId;

		PhotoAlbum photoAlbum = new PhotoAlbum();
		photoAlbum.setProfileId(profileId);
		photoAlbum.setTitle("Test Album Title");
		photoAlbum.setAuthorProfileId(clientId);
		photoAlbum.setViewPermission(PermissionLevel.EVERYBODY);

		Photo photo = new Photo();
		photo.setProfileId(photoAlbum.getProfileId());
		photo.setAlbumId(photoAlbum.getAlbumId());
		photo.setImageUrl("http://test_url");

		photoDAO.insert(photo);
		Photo insertedPhoto = photoService.getPhoto(clientId, photo.getProfileId(), photo.getPhotoId());
		assertNotNull(insertedPhoto);
//		assertEquals(photoAlbum.getAlbumId(), insertedPhoto.getAlbumId());

		photo.setDescription("New photo description");
		Photo savedPhoto = photoService.savePhoto(photo.getProfileId(), photo);
		assertNotNull(savedPhoto);
		assertEquals(savedPhoto.getDescription(), photo.getDescription());

        // put 5 more same photos to album
        photo.setPhotoId(0);
        photoDAO.insert(photo); //2
        photo.setPhotoId(0);
        photoDAO.insert(photo); //3
        photo.setPhotoId(0);
        photoDAO.insert(photo); //4
        photo.setPhotoId(0);
        photoDAO.insert(photo); //5
        photo.setPhotoId(0);
        photoDAO.insert(photo); //6

		photoService.deletePhoto(clientId, photo.getProfileId(), photo.getPhotoId());
		assertNull(photoService.getPhoto(clientId, photo.getProfileId(), photo.getPhotoId()));

	}

	@Test
	@Ignore
	public void testPhotoExceptions() throws NotFoundException, PermissionException {

		long profileId = insertUserProfile().getProfileId();
		long clientId = insertUserProfile().getProfileId();

		PhotoAlbum photoAlbum = new PhotoAlbum();
		photoAlbum.setProfileId(profileId);
		photoAlbum.setTitle("Test Album Title");
		photoAlbum.setAuthorProfileId(clientId);
		photoAlbum.setViewPermission(PermissionLevel.EVERYBODY);

		Photo photo = new Photo();
		photo.setProfileId(photoAlbum.getProfileId());
		photo.setAlbumId(photoAlbum.getAlbumId());
		photo.setImageUrl("http://test_url");
		photoDAO.insert(photo);

		Photo insertedPhoto = photoService.getPhoto(clientId, photo.getProfileId(), photo.getPhotoId());
		assertNotNull(insertedPhoto);

		Photo notPermittedPhoto = photoService.getPhoto(clientId, photo.getProfileId(), photo.getPhotoId());
		assertNull(notPermittedPhoto);

		photoService.deletePhoto(profileId, photo.getProfileId(), photo.getPhotoId());
		assertNull(photoService.getPhoto(clientId, photo.getProfileId(), photo.getPhotoId()));
	}
}

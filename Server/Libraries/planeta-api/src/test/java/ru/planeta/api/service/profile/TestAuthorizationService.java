package ru.planeta.api.service.profile;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import ru.planeta.api.model.UserAuthorizationInfo;
import ru.planeta.api.service.registration.RegistrationService;
import ru.planeta.dao.commondb.UserPrivateInfoDAO;
import ru.planeta.model.common.UserPrivateInfo;
import ru.planeta.test.AbstractTest;

import static org.junit.Assert.*;

/**
 * @author ds.kolyshev
 * Date: 29.08.11
 */
//@Ignore
public class TestAuthorizationService extends AbstractTest {

	@Autowired
	private AuthorizationService authorizationService;
	@Autowired
	private UserPrivateInfoDAO userPrivateInfoDAO;

    @Autowired
    private PasswordEncoder passwordEncoder;

	@Autowired
	private RegistrationService registrationService;

	@Test
	public void testRegisterNewUser() throws Exception {
		
		
			String email = "testuser@test.ee";
			String password = "123123";

			UserAuthorizationInfo user = registerUser(email, password);
			assertNotNull(user);

		
	}

	@Test
	public void testLogin() throws Exception {
		
		
			String email = "testuser111@test.ee";
			String password = "123123";

			UserAuthorizationInfo user = registerUser(email, password);
			assertNotNull(user);

			UserAuthorizationInfo info = authorizationService.getUserInfoByUsername(email);
			assertNotNull(info);
			assertEquals(user.getProfile().getProfileId(), info.getProfile().getProfileId());
		
	}

    @Test
    public void testRecoverPassword() throws Exception {
        
        
            String email = "testuser@test.ee";
            String password = "123123";
            String newPassword = "321321";

            UserAuthorizationInfo user = registerUser(email, password);
			assertNotNull(user);

            UserPrivateInfo userInfo = registrationService.recoverPassword(email, "http://planeta.ru", "");

            assertNotNull(userInfo);
            assertNotNull(userInfo.getRegCode());

            userInfo = authorizationService.passwordRecoveryRequest(userInfo.getRegCode());
            assertNotNull(userInfo);

            authorizationService.passwordRecoveryChangePassword(userInfo.getRegCode(), newPassword);

            UserPrivateInfo userPrivateInfo = userPrivateInfoDAO.selectByEmail(email);
            assertNotNull(userPrivateInfo);
            assertTrue(passwordEncoder.matches(newPassword, userPrivateInfo.getPassword()));
            assertNull(userPrivateInfo.getRegCode());

        
    }

}

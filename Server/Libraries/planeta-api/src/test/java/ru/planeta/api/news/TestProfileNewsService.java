package ru.planeta.api.news;

import org.apache.commons.lang3.time.DateUtils;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import ru.planeta.api.exceptions.PermissionException;
import ru.planeta.api.service.profile.ProfileSubscriptionService;
import ru.planeta.model.common.campaign.Campaign;
import ru.planeta.model.news.ProfileNews;
import ru.planeta.model.profile.Post;
import ru.planeta.model.profile.Profile;
import ru.planeta.test.AbstractTest;

import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 *
 * Created by alexa_000 on 17.11.2014.
 */
public class TestProfileNewsService extends AbstractTest {

    @Autowired
    private ProfileNewsService profileNewsService;

    @Autowired
    private LoggerService loggerService;

    @Autowired
    private ProfileSubscriptionService subscriptionService;

    @Test
    public void test() {
        Profile profile = insertUserProfile();
        Campaign campaign = insertCampaign(profile.getProfileId());

        Profile profile2 = insertUserProfile();

        subscriptionService.subscribe(profile2.getProfileId(), profile.getProfileId());

        loggerService.addProfileNews(ProfileNews.Type.CAMPAIGN_STARTED, campaign.getProfileId(), campaign.getCampaignId(), campaign.getCampaignId());

        List<ProfileNewsDTO> profile1News = profileNewsService.getProfileNews(profile.getProfileId(), profile.getProfileId(), false, 0, 0);
        List<ProfileNewsDTO> profile2News = profileNewsService.getProfileNews(profile2.getProfileId(), profile2.getProfileId(), false, 0, 0);

        assertTrue(subscriptionService.doISubscribeYou(profile2.getProfileId(), profile.getProfileId()));

        assertEquals(1, profile1News.size());
        assertEquals(1, profileNewsService.selectCountForProfile(profile.getProfileId(), null, false));

        assertEquals(1, profile2News.size());
        assertEquals(1, profileNewsService.selectCountForProfile(profile2.getProfileId(), null, false));

        Date now = new Date();

        Date later = DateUtils.addMinutes(now, 1);

        assertEquals(0, profileNewsService.selectCountForProfile(profile.getProfileId(), later, false));
        assertEquals(0, profileNewsService.selectCountForProfile(profile2.getProfileId(), later, false));
    }

    @Test
    public void testGetProfileNews() throws PermissionException {
        Profile profile1 = insertUserProfile();
        Profile profile2 = insertUserProfile();

        Post post1 = new Post();
        post1.setProfileId(profile1.getProfileId());

        Post post2 = new Post();
        post2.setProfileId(profile2.getProfileId());

        profileNewsService.addProfileNewsPost(post1.getProfileId(), post1);
        profileNewsService.addProfileNewsPost(post2.getProfileId(), post2);

        subscriptionService.subscribe(profile2.getProfileId(), profile1.getProfileId());

        List<ProfileNewsDTO> list = profileNewsService.getProfileNews(profile1.getProfileId(), profile1.getProfileId(), false, 0, 0);
        assertEquals(1, list.size());
        assertEquals(post1.getId(), list.get(0).getPostId());

        list = profileNewsService.getProfileNews(profile2.getProfileId(), profile1.getProfileId(), false, 0, 0);
        assertEquals(1, list.size());
        assertEquals(post1.getId(), list.get(0).getPostId());


        list = profileNewsService.getProfileNews(profile2.getProfileId(), profile2.getProfileId(), false, 0, 0);
        assertEquals(2, list.size());
        // reverse orders of posts: last posted post is on top
        assertEquals(post2.getId(), list.get(0).getPostId());
        assertEquals(post1.getId(), list.get(1).getPostId());

        list = profileNewsService.getProfileNews(profile1.getProfileId(), profile2.getProfileId(), false, 0, 0);
        assertEquals(1, list.size());
        assertEquals(post2.getId(), list.get(0).getPostId());
    }
}

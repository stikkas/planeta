package ru.planeta.api.text;

import org.apache.commons.lang3.StringEscapeUtils;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import ru.planeta.commons.lang.StringUtils;
import ru.planeta.commons.text.Bbcode;
import ru.planeta.model.profile.chat.ChatMessage;
import ru.planeta.test.AbstractTest;


import static org.junit.Assert.assertEquals;

/**
 * Created with IntelliJ IDEA.
 * User: Александр
 * Date: 06.03.13
 * Time: 12:38
 */
public class TestFormattingService extends AbstractTest {

    @Autowired
    private FormattingService formattingService;

    @Test
    public void testEscaping() {
        String stringWithXml = "<div>\"русский текст\"</div>";
        String escaped = "&lt;div&gt;&quot;русский текст&quot;&lt;/div&gt;";
        Assert.assertEquals(escaped, StringEscapeUtils.escapeHtml4(stringWithXml));
    }

    @Test
    public void testEscaping2() throws Exception {
        String text = "Ёё 10 августа 2013 года, [b] ololo = ololo [/b] пройдет  VII фестиваль искусства колокольного звона «Колокольные звоны России», в рамках которого пройдет I фестиваль керамистов России, посвященный 105 году со дня кончины Веры Саввишной урождённой Мамонтовой,в память о которой был построен Троицкий храм д. Аверкиево.\n" +
            "\n" +
            "Наш храм, построенный в начале ХХ-го века  на средства Александра Дмитриевича Самарина, имеет архитектуру древнерусского храма и отличается от других построенных в это же время. Самарин А. Д. построил храм в память, о своей внезапно умершей супруги, Веры Саввишны Мамонтовой (художники Серов и Васнецов писали с нее портреты: «Девочка с персиками», «Девушка с кленовой веткой (Портрет Веры Саввишны Мамонтовой)». В свое время храм подвергся разрушению и разграблению, как и многие другие храмы Русской Православной Церкви. В данный момент храм находится в стадии реставрации.\n" +
            "\n" +
            "Задача фестиваля - возрождение интереса к колокольному звону и приобщение к его основам, а также культурным традициям нашего Отечества.\n" +
            "\n" +
            "Фестиваль колокольного искусства приобретает всё большую известность, ежегодно собирая все больше зрителей из разных уголков России.  Выступления известных звонарей из Москвы, Ярославля, Владимира и других городов на звоннице храма сменяются с выступлениями известных исполнителей и музыкальных коллективов на полностью оснащенной сцене, монтируемой близ храма. В 2010 году, фестиваль проходил с участием президента фонда «Золотой Витязь», народного артиста, члена Патриаршего совета по культуре Н. П. Бурляева.                                           \n" +
            "\n" +
            "В рамках фестиваля пройдет благотворительная выставка художника Андрея Прахова - члена Творческого Союза Художников России в поддержку восстановления Троицкого храма,  а так же акция к 100 летию постройки храма «оставь свое имя в истории».     На фестивале соберутся керамисты России которые будут соревноваться в своем мастерстве.\n" +
            "\n" +
            "С уважением,\n" +
            "Настоятель Троицкого храма д. Аверкиево\n" +
            "Иерей Алексей Попков.";

        String escaped = StringUtils.stripNonPrintableSaveWhiteSpace(text);
        Assert.assertFalse(escaped.contains("\u8232"));
        Assert.assertTrue(escaped.contains("Ёё"));

    }

    @Test
    public void testLinkExtracting() {
        String textWithLink = "ля ля ля<script>http://ya.ru</script> http://vk.com/planetaru/photos ля ля ля";
        String formattedText = "ля ля ля&lt;script&gt;<a href=\"/api/util/away.html?to=http%3A%2F%2Fya.ru\" target=\"_blank\" rel=\"nofollow noopener\">http://ya.ru</a>&lt;/script&gt; <a href=\"http://vk.com/planetaru/photos\" target=\"_blank\" rel=\"nofollow noopener\">http://vk.com/planetaru/photos</a> ля ля ля";
        Assert.assertEquals(formattedText, formattingService.formatMessage(textWithLink, 0, true));
        Assert.assertEquals(formattedText, formattingService.formatPlainText(textWithLink, 0, true));
    }

    @Test
    public void testBBcodeExtracting() {
        String textWithBBcodeAndHtml = "ля ля ля[photo id=1 owner=2]http://somewhere.com/something.jpg[/photo]<script>http://ya.ru</script>ля ля ля";
        String formatted = "ля ля ля<p:photo id=\"1\" owner=\"2\" image=\"http://somewhere.com/something.jpg\"></p:photo>&lt;script&gt;<a target=\"_blank\" href=\"/api/util/away.html?to=http%3A%2F%2Fya.ru\">http://ya.ru</a>&lt;/script&gt;ля ля ля";
        Assert.assertEquals(formatted, formattingService.formatPlainText(textWithBBcodeAndHtml, 0, true));
    }

    @Test
    public void testComplexBBcode() {
        String buggyText =
                "[video id=13033 owner=101287 duration=169]https://s2.planeta.ru/pqwe.jpg[/video] text text" +
                        "[video id=13033 owner=101287 duration=169 name=dghjagsd ajdg hjagsdjg]https://s2.planeta.ru/pqwe.jpg[/video]" +
                        "[audio id=53367 owner=101287 duration=245 name=Электростул fhfthtr artist=МПТРИ /]" +
                        "[video id=13049 owner=101287 duration=161]http://s1.planeta.ru/pqwe.jpg[/video]";
        String resultText = "<p:video id=\"13033\" owner=\"101287\" image=\"https://s2.planeta.ru/pqwe.jpg\" duration=\"169\"></p:video> text text<p:video id=\"13033\" owner=\"101287\" image=\"https://s2.planeta.ru/pqwe.jpg\" duration=\"169\" name=\"dghjagsd ajdg hjagsdjg\"></p:video><p:audio id=\"53367\" owner=\"101287\" duration=\"245\" name=\"Электростул fhfthtr\" artist=\"МПТРИ\"></p:audio><p:video id=\"13049\" owner=\"101287\" image=\"http://s1.planeta.ru/pqwe.jpg\" duration=\"161\"></p:video>";
        Assert.assertEquals(resultText, formattingService.formatPlainText(buggyText, 0, false));
    }

    @Test
    public void testFormatWithLineBreaksAfterLinks() {
        String textWithLineBreaks = "Link:\n https://planeta.ru asd\n New line";
        String formattedText = "Link:<br> <a href=\"https://planeta.ru\" target=\"_blank\" rel=\"nofollow noopener\">https://planeta.ru</a> asd<br> New line";

        ChatMessage chatMessage = new ChatMessage();
        chatMessage.setMessageText(textWithLineBreaks);
        chatMessage.setMessageTextHtml(formattingService.formatPlainText(chatMessage.getMessageText(), 0, true));
        assertEquals(formattedText, chatMessage.getMessageTextHtml());
    }

    @Test
    public void testWithBbcode() {
        String bbcodeTextWithoutHeight = "text before photo [photo id=123 owner=345]http://someserver.org/someurl.jpg[/photo] text after photo";
        String formattedText = "text before photo <p:photo id=\"123\" owner=\"345\" image=\"http://someserver.org/someurl.jpg\"></p:photo> text after photo";

        assertEquals(formattedText, formattingService.formatPlainText(bbcodeTextWithoutHeight, 0, true));
    }

    @Test
    public void testBbcodeExternalLinks() throws Exception {
        String description = "[url]http://vk.com/learson[/url]";
        String description2 = "[url=\"http://www.kinopoisk.ru/film/763742/\"]http://www.kinopoisk.ru/film/763742/[/url]";
        String result = formattingService.cleanHtml(Bbcode.transform(description));
        String result2 = formattingService.cleanHtml(Bbcode.transform(description2));
        String result3 = formattingService.replaceExternalLinks(
            "                                    Всем привет! Позвольте представиться. Мы Максим Малявин и Екатерина Безымянная - авторы этого проекта. <br><br>Как вы полагаете, чем могли бы  заняться два известных и совершенно непохожих  блогера, <a href=\"http://prostitutka-ket.livejournal.com/ \">http://prostitutka-ket.livejournal.com/ </a> <span style=\"font-weight:bold;\">Проститутка Кэт </span>и <a href=\"http://dpmmax.livejournal.com/ \">http://dpmmax.livejournal.com/ </a> <span style=\"font-weight:bold;\">Добрый психиатр</span>  в свободное от работы время? Текстом, конечно же! Точнее, сценарием пьесы. Самой настоящей. Катя недрогнувшей рукой отдала мне на растерзание типажи своих клиентов и истории про них, я постарался впихнуть невпихуемое в псевдошекспировский размер и ритм  и вуаля!<br><br>Спектакль \"Идиоты запоминаются сильнее\" - это своего рода сказка. Сказка для взрослых. Точнее, воплощение сбывшейся мечты супернаивной девушки, которая смотрит порнофильм и втайне верит, что всё будет хорошо, и персонажи в итоге поженятся. И в то же время, персонажи спектакля  вполне реальные люди. Это клиенты, о которых пишет в своей книге столь же реально существующая проститутка. Кстати, она и главный герой сюжета, и автор книги, и соавтор сценария. Едина в трёх ипостасях, что называется.<br><br><div class=\"image-big-preview\" data-json=\"{&quot;id&quot;:&quot;255889&quot;,&quot;owner&quot;:&quot;115075&quot;,&quot;image&quot;:&quot;https://s2.planeta.ru/i/3e791/1373542737197_renamed.jpg&quot;,&quot;index&quot;:0}\" data-role=\"photo\">         <a href=\"https://s2.planeta.ru/i/3e791/1373542737197_renamed.jpg\">             <img src=\"https://s2.planeta.ru/i/3e791/huge.jpg\">         </a>     </div><br><br>У пьесы, помимо специфического сюжета, есть ещё как минимум две особенности. Во-первых, слог, которым изъясняются её герои. Так говорили бы герои Шекспира, окажись они в нашем веке. Во-вторых, искромётный юмор и тонны напалма, припасённые авторами пьесы, поэтому готовьтесь: будет отчаянно смешно. Неудивительно  в написании пьесы принимал самое активное участие психиатр. Вы не читали психиатрические байки Максима Малявина? Вы серьёзно? Счастливчик: вам предстоит много и долго рыдать от смеха. Если, конечно, вы не страдаете ханжеством, в противном случае медицина и Мельпомена бессильны.<br><br><span style=\"font-weight:bold;\">Чем ещё примечателен проект?</span> Тем, что <span style=\"font-weight:bold;\">кастинг актёров пройдёт при непосредственном участии читателей блогов его авторов</span>. Это их голоса определят, кому из предложенных кандидатов предстоит выйти на сцену. Это они, среди прочих, будут присутствовать в зрительном зале: ведь спектакль, на наш взгляд - вполне достойный повод для девиртуализации даже самых заядлых пользователей рунета!<br><br>Возможность напрямую (хоть и виртуально) поучаствовать в кастинге  не единственная изюминка для интернет-пользователей. У нас в запасе ещё несколько позиций, в которых выбор будет предоставлен будущему зрителю. Поэтому проект можно смело назвать интерактивным.<br><br>Поскольку процесс постановки спектакля \"Идиоты запоминаются сильнее\" - мероприятие весьма затратное: аренда, кастинги, репетиции, костюмы и декорации, мегатонны табака, коньяка и рижского бальзама, мы решили обратиться к вам за помощью. <span style=\"font-weight:bold;\">Общими усилиями мы сможем сделать этот спектакль реальностью и позвать вас на премьеру уже в сентябре-октябре.</span>\n"
        );
        Assert.assertEquals("<a target=\"_blank\" href=\"/api/util/away.html?to=http%3A%2F%2Fvk.com%2Flearson\">http://vk.com/learson</a>", result);
        Assert.assertEquals("<a target=\"_blank\" href=\"/api/util/away.html?to=http%3A%2F%2Fwww.kinopoisk.ru%2Ffilm%2F763742%2F\">http://www.kinopoisk.ru/film/763742/</a>", result2);
        Assert.assertTrue(result3.contains("api/util/away"));

    }

}

package ru.planeta.test;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.apache.log4j.Logger;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import ru.planeta.api.exceptions.NotFoundException;
import ru.planeta.api.exceptions.PermissionException;
import ru.planeta.api.exceptions.RegistrationException;
import ru.planeta.api.mail.MailClient;
import ru.planeta.api.model.UserAuthorizationInfo;
import ru.planeta.api.model.json.GroupCreationData;
import ru.planeta.api.search.SearchService;
import ru.planeta.api.service.admin.AdminService;
import ru.planeta.api.service.billing.payment.PaymentService;
import ru.planeta.api.service.billing.payment.settings.PaymentSettingsService;
import ru.planeta.api.service.campaign.CampaignService;
import ru.planeta.api.service.geo.AddressService;
import ru.planeta.api.service.notifications.NotificationService;
import ru.planeta.api.service.profile.ProfileBalanceService;
import ru.planeta.api.service.profile.ProfileServiceImpl;
import ru.planeta.api.service.profile.ProfileSubscriptionService;
import ru.planeta.api.service.registration.RegistrationService;
import ru.planeta.api.service.shop.ProductUsersService;
import ru.planeta.api.service.statistic.UniversalAnalyticsService;
import ru.planeta.commons.model.Gender;
import ru.planeta.dao.TransactionScope;
import ru.planeta.dao.commondb.ProfileBalanceDAO;
import ru.planeta.dao.commondb.UserPrivateInfoDAO;
import ru.planeta.dao.commondb.CampaignDAO;
import ru.planeta.dao.commondb.ShareDAO;
import ru.planeta.dao.profiledb.GroupDAO;
import ru.planeta.dao.profiledb.PhotoDAO;
import ru.planeta.dao.profiledb.ProfileDAO;
import ru.planeta.dao.shopdb.ProductAttributeDAO;
import ru.planeta.model.common.UserPrivateInfo;
import ru.planeta.model.common.campaign.Campaign;
import ru.planeta.model.common.campaign.CampaignTag;
import ru.planeta.model.common.campaign.Share;
import ru.planeta.model.common.campaign.enums.CampaignStatus;
import ru.planeta.model.common.campaign.enums.ShareStatus;
import ru.planeta.model.enums.*;
import ru.planeta.model.profile.Group;
import ru.planeta.model.profile.Profile;
import ru.planeta.model.shop.Category;
import ru.planeta.model.shop.ProductAttribute;
import ru.planeta.model.shop.ProductInfo;
import ru.planeta.model.shop.enums.ProductCategory;
import ru.planeta.model.shop.enums.ProductState;

import java.io.File;
import java.math.BigDecimal;
import java.util.*;

import static org.junit.Assert.*;


/**
 * @author pvyazankin
 * @since 17.10.12 14:30
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:spring/testApplicationContext.xml", "classpath:spring/applicationContext-*.xml", "classpath*:/spring/applicationContext-dao.xml"})
public abstract class AbstractTest {

    private NotificationService notificationService;
    private RegistrationService registrationService;


    private MailClient mailClient;

    @Autowired
    public void setRegistrationService(RegistrationService registrationService) {
        this.registrationService = registrationService;
        this.registrationService.setAsynchronous(false);
    }
    @Autowired
    private UniversalAnalyticsService universalAnalyticsService;

    @Autowired
    public void setMailClient(MailClient mailClient) {
        this.mailClient = mailClient;
        this.mailClient.setAsynchronous(false);
    }

    @Autowired
    public void setNotificationService(NotificationService notificationService) {
        this.notificationService = notificationService;
        this.notificationService.setAsynchronous(false);
    }

    @Autowired
    public void setUniversalAnalyticsService(UniversalAnalyticsService universalAnalyticsService) {
        this.universalAnalyticsService = universalAnalyticsService;
        this.universalAnalyticsService.setUgauid("");
    }


    protected static Logger log = Logger.getLogger(AbstractTest.class);

    @Autowired
    private ApplicationContext context;
    @Autowired
    private ProfileDAO profileDAO;
    @Autowired
    protected UserPrivateInfoDAO userPrivateInfoDAO;
    @Autowired
    protected ProfileServiceImpl profileService;
    @Autowired
    protected AdminService adminService;
    @Autowired
    protected ShareDAO shareDAO;
    @Autowired
    protected ProductUsersService productUsersService;
    @Autowired
    protected ProductAttributeDAO productAttributeDAO;
    @Autowired
    protected CampaignService campaignService;
    @Autowired
    protected SearchService searchService;
    @Autowired
    protected CampaignDAO campaignDAO;
    @Autowired
    protected PhotoDAO photoDAO;
    @Autowired
    protected AddressService addressService;
    @Autowired
    private ProfileBalanceService profileBalanceService;
    @Autowired
    protected PaymentService paymentService;
    @Autowired
    protected PaymentSettingsService paymentSettingsService;

    private TransactionScope transactionScope;

    public NotificationService getNotificationService() {
        return notificationService;
    }

    @Autowired
    private ProfileSubscriptionService profileSubscriptionService;
    @Autowired
    private GroupDAO groupDAO;

    @Autowired
    private ProfileBalanceDAO profileBalanceDAO;

    private ProfileBalanceDAO getProfileBalanceDAO() {
        return profileBalanceDAO;
    }

    private GroupDAO getGroupDAO() {
        return groupDAO;
    }


    protected TransactionScope getTransactionScope() {
        return transactionScope;
    }

    /**
     * Cleans directories used while testing
     */
    public static void cleanUp() throws Exception {
        File staticDir = new File(".\\planeta_static");
        File tmpDir = new File(".\\tmp");

        if (staticDir.exists()) {
            log.info("Cleaning 'planeta_static' directory");
            FileUtils.deleteDirectory(staticDir);
            log.info("Directory 'planeta_static' has been cleaned");
        }

        if (tmpDir.exists()) {
            log.info("Cleaning 'tmp' directory");
            FileUtils.deleteDirectory(tmpDir);
            log.info("Directory 'tmp' has been cleaned");
        }
    }

    protected Profile insertUserProfile() {
        Profile profile = new Profile();
        profile.setProfileId(0);
        profile.setAlias("alias" + rndStr());
        profile.setImageId(1);
        profile.setImageUrl("http://image" + rndStr() + ".com");
        profile.setDisplayName("name" + rndStr());
        profile.setProfileType(ProfileType.USER);
        profile.setStatus(ProfileStatus.USER_ACTIVE);



        profile.setCityId(1);
        profile.setCountryId(1);
        profile.setUserGender(Gender.NOT_SET);

        getProfileDAO().insert(profile);
        return profile;
    }

    protected UserPrivateInfo insertUserPrivateInfo(String email, String password) {
        Profile user = insertUserProfile();
        UserPrivateInfo userPrivateInfo = new UserPrivateInfo();
        userPrivateInfo.setUserId(user.getProfileId());
        userPrivateInfo.setEmail(email.trim().toLowerCase());
        userPrivateInfo.setUsername(userPrivateInfo.getEmail());
        userPrivateInfo.setPassword(password);
        userPrivateInfo.setUserStatus(EnumSet.of(UserStatus.USER));

        userPrivateInfoDAO.insert(userPrivateInfo);
        return userPrivateInfo;
    }

    public UserPrivateInfo insertUserPrivateInfo(long profileId) {
        UserPrivateInfo userPrivateInfo = new UserPrivateInfo();
        userPrivateInfo.setUserId(profileId);
        userPrivateInfo.setUserStatus(EnumSet.of(UserStatus.USER));
        userPrivateInfo.setTimeAdded(new Date());
        userPrivateInfo.setTimeUpdated(new Date());
        userPrivateInfo.setPassword("123");
        userPrivateInfo.setUsernameAndEmail("__test__" + profileId + "@test.ru");
        userPrivateInfoDAO.insert(userPrivateInfo);
        return userPrivateInfo;
    }


    protected UserPrivateInfo insertPlanetaAdminPrivateInfo(String email, String password) {
        UserPrivateInfo info = insertUserPrivateInfo(email, password);
        info.setUserStatus(EnumSet.of(UserStatus.ADMIN));
        userPrivateInfoDAO.update(info);
        return info;
    }

    protected UserPrivateInfo insertPlanetaAdminPrivateInfo() {
        String timeStamp = String.valueOf(Calendar.getInstance().getTimeInMillis()) + String.valueOf((int) (100 * Math.random()));
        String email = String.format("junit.admin%s@test.ee", timeStamp);

        return insertPlanetaAdminPrivateInfo(email, timeStamp);
    }

    protected long planetaAdminProfileId() {
        return insertPlanetaAdminPrivateInfo().getUserId();
    }


    private static GroupCreationData getNewGroupCreationData() {
        GroupCreationData groupCreationData = new GroupCreationData();
        groupCreationData.setAlias("test alias");
        groupCreationData.setCategory(GroupCategory.MUSIC);
        groupCreationData.setContactInfo("test contact info");
        groupCreationData.setSummary("test description");
        groupCreationData.setDisplayName("test name for tests " + new Random().nextInt());
        groupCreationData.setWebSite("test site url");

        return groupCreationData;
    }

    protected void setGroupIsMerchant(long profileId) throws NotFoundException, PermissionException {
        long adminId = insertPlanetaAdminPrivateInfo().getUserId();
        adminService.setProfileStatus(adminId, profileId, ProfileStatus.GROUP_ACTIVE_OFFICIAL);
    }

    public Profile privateCreateGroup(long clientId, GroupCreationData groupCreationData) throws NotFoundException, PermissionException {
        // check this user exist
        profileService.getProfileSafe(clientId);

        Profile profile = new Profile();
        profile.setStatus(ProfileStatus.GROUP_ACTIVE);
        profile.setDisplayName(groupCreationData.getDisplayName());
        profile.setProfileType(groupCreationData.getProfileType());
        profile.setGroupCategory(groupCreationData.getCategory());
        profile.setUsersCount(1);
        profile.setCreatorProfileId(clientId);
        profile.setDisplayName(groupCreationData.getDisplayName());
        profile.setSummary(groupCreationData.getSummary());

        getProfileDAO().insert(profile);

        Group group = new Group();
        group.setProfileId(profile.getProfileId());
        group.setSiteUrl(groupCreationData.getWebSite());
        group.setContactInfo(groupCreationData.getContactInfo());
        getGroupDAO().insert(group);

        if (profile.getProfileType() != ProfileType.HIDDEN_GROUP) {
            profileSubscriptionService.subscribe(clientId, profile.getProfileId());
            profileSubscriptionService.toggleAdmin(clientId, profile.getProfileId(), true);
        }

        return profile;
    }


    private long getNewGroupProfileId(long clientId, GroupCreationData groupCreationData) throws NotFoundException, PermissionException {
        Profile group = privateCreateGroup(clientId, groupCreationData);
        return group.getProfileId();
    }



    protected List<Category> insertCategory(String categoryName) {
        Category category = new Category();
        category.setValue(categoryName);

        return Collections.singletonList(category);
    }


    protected ProductInfo createProductVersion(long merchantId, ProductInfo parent, ProductState productState) throws PermissionException, NotFoundException {

        ru.planeta.model.shop.ProductInfo product = new ru.planeta.model.shop.ProductInfo();
        long storeId = merchantId;
        product.setParentProductId(0);
        if (parent != null) {
            merchantId = parent.getMerchantProfileId();
            storeId = parent.getStoreId();
            product.setParentProductId(parent.getProductId());
        }
        product.setName(productState.name() + rndStr());
        product.setMerchantProfileId(merchantId);
        product.setStoreId(storeId);
        List<String> imageUrls = new ArrayList<String>();
        imageUrls.add("http://image1" + rndStr() + ".jpg");
        imageUrls.add("http://image2" + rndStr() + ".jpg");
        product.setImageUrls(imageUrls);
        product.setDescription("DESCRIPTION" + rndStr());
        product.setDescriptionHtml("<h>" + rndStr() + "</p>");
        product.setTags(insertCategory("Test category"));
        product.setProductState(productState);
        product.setObjectType(ObjectType.PRODUCT);
        product.setProductCategory(ProductCategory.PHYSICAL);
        product.setTimeUpdated(new Date());
        product.setTimeAdded(new Date());
        productUsersService.saveProduct(merchantId, product);

        return product;
    }

    protected ProductInfo createProductMETA(long merchantId) throws PermissionException, NotFoundException {
        return createProductVersion(merchantId, null, ProductState.META);
    }


    protected ProductInfo createChildProductVersion(ProductInfo parent) throws PermissionException, NotFoundException {
        ProductInfo product = new ProductInfo();
        product.copy(parent);
        product.setProductId(0);
        product.setProductState(ProductState.ATTRIBUTE);
        product.setChildrenProducts(null);

        productUsersService.saveProduct(insertPlanetaAdminPrivateInfo().getUserId(), product);
        parent.getChildrenProducts().add(product);
        return product;
    }

    protected ProductAttribute createProductAttribute(String value) {
        ProductAttribute attribute = new ProductAttribute();
        attribute.setAttributeTypeId(1);
        attribute.setValue(value);
        return attribute;
    }


    protected Campaign insertCampaign(long ownerId) {
        Campaign campaign = new Campaign();
        campaign.setProfileId(ownerId);
        campaign.setName("Campaign Name");
        campaign.setDescription("Campaign Description");
        campaign.setDescriptionHtml("Campaign Description");
        campaign.setTargetAmount(null);
        campaign.setTags(Collections.<CampaignTag>emptyList());
        Date date = new Date();
        DateUtils.addDays(date, 100);
        campaign.setTimeFinish(date);
        campaign.setPurchaseCount(0);
        campaign.setPurchaseSum(BigDecimal.ZERO);
        campaign.setStatus(CampaignStatus.DRAFT);;
        campaignDAO.insert(campaign);
        return campaign;
    }

    protected Share insertShare(long groupId, long campaignId) {
        Share share = new Share();
        share.setProfileId(groupId);
        share.setCampaignId(campaignId);
        share.setName("name");
        share.setDescription("desc");
        share.setDescriptionHtml("desc");
        share.setImageId(100L);
        share.setImageUrl("img url");
        share.setPrice(BigDecimal.ONE);
        share.setAmount(10);
        share.setShareStatus(ShareStatus.ACTIVE);
        shareDAO.insert(share);
        return share;
    }

    protected Profile insertEventProfile(long userProfileId, long groupProfileId) throws NotFoundException {
        Profile group = getProfileDAO().selectById(groupProfileId);
        if (group == null) {
            throw new NotFoundException("Group", groupProfileId);
        }

        //Inserting new event
        Profile event = insertNewEvent(userProfileId);

        // User that created event becomes admin of the event
        return event;
    }

    private Profile insertNewEvent(long creatorProfileId) {
        Profile event = new Profile();
        event.setProfileType(ProfileType.EVENT);
        event.setDisplayName("New event test");
        event.setCreatorProfileId(creatorProfileId);
        getProfileDAO().insert(event);

        return event;
    }


    public static int randInt() {
        return (int) Math.round(Math.random() * 1000);
    }

    public static long randInt(int max) {
        return Math.round(Math.random() * max);
    }

    @Deprecated
    public static String rndStr() {
        return "_" + UUID.randomUUID().toString();
    }

    public static long random() {
        return Math.abs(Math.round(Math.random() * 1000));
    }

    protected void increaseProfileBalance(long profileId, BigDecimal amount) throws NotFoundException {
        profileBalanceService.increaseProfileBalance(profileId, amount, BigDecimal.ZERO, "test increasing");
    }

    protected BigDecimal getProfileBalance(long profileId) {
        return profileBalanceService.getBalance(profileId);
    }

    protected void assertBalanceEquals(long profileId, BigDecimal amount) {
        assertEqualsBigDecimal(amount, getProfileBalance(profileId));
    }

    protected static void assertEqualsBigDecimal(BigDecimal expected, BigDecimal actual) {
        assertEquals(0, expected.compareTo(actual));
    }

    protected ApplicationContext getContext() {
        return context;
    }

    protected ProfileDAO getProfileDAO() {
        return profileDAO;
    }

    protected static long getProfileId(UserAuthorizationInfo authorizationInfo) {
        return authorizationInfo.getProfile().getProfileId();
    }

    protected UserAuthorizationInfo registerUser(String email, String password) throws NotFoundException, PermissionException, RegistrationException {
        UserAuthorizationInfo authorizationInfo = registrationService.registerByEmail(email, password, null);
        assertNotNull(authorizationInfo);

        // change user's status to active, so user will not have any permission constraints
        Profile profile = authorizationInfo.getProfile();
        profile.setStatus(ProfileStatus.USER_ACTIVE);
        getProfileDAO().update(profile);

        assertNotNull(authorizationInfo);
        assertNotNull(authorizationInfo.getUserPrivateInfo().getRegCode());
        assertEquals(email, authorizationInfo.getUserPrivateInfo().getEmail());
        assertTrue(authorizationInfo.getUserPrivateInfo().getUserId() > 0);

        return authorizationInfo;
    }

    protected UserAuthorizationInfo registerUser() throws PermissionException, NotFoundException {
        try {
            return registerUser(getTestEmail("user"), getTestStr("password"));
        } catch (RegistrationException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            return null;
        }
    }

    protected UserAuthorizationInfo registerPlanetaAdminUser() throws PermissionException, NotFoundException {
        return registerUserWithStatus(UserStatus.ADMIN);
    }

    protected UserAuthorizationInfo registerSuperAdminUser() throws PermissionException, NotFoundException {
        return registerUserWithStatus(UserStatus.SUPER_ADMIN);
    }

    private UserAuthorizationInfo registerUserWithStatus(UserStatus userStatus) throws PermissionException, NotFoundException {
        UserAuthorizationInfo result = registerUser();

        UserPrivateInfo userPrivateInfo = result.getUserPrivateInfo();
        userPrivateInfo.setUserStatus(EnumSet.of(userStatus));
        userPrivateInfoDAO.update(userPrivateInfo);

        return result;
    }

    protected long registerNewOfficialGroup(long clientId) throws PermissionException, NotFoundException {
        long profileId = registerSimpleGroup(clientId);
        adminService.setProfileStatus(clientId, profileId, ProfileStatus.GROUP_ACTIVE_OFFICIAL);
        return profileId;
    }


    protected long registerSimpleGroup(long clientId) throws NotFoundException, PermissionException {
        return getNewGroupProfileId(clientId, getNewGroupCreationData());
    }

    protected Share createShareWithCampaign(long clientId, long profileId, BigDecimal price, int count) throws PermissionException, NotFoundException {
        Campaign campaign = createCampaign(clientId, profileId, price.multiply(new BigDecimal(count)), CampaignStatus.ACTIVE);
        return createShare(clientId, profileId, campaign, price, count);
    }

    protected Campaign createCampaign(long clientId, long profileId, BigDecimal targetAmount, CampaignStatus status) throws PermissionException, NotFoundException {
        Campaign result = campaignService.insertCampaign(clientId, profileId, profileId);
        result.setStatus(status);
        result.setTimeFinish(null);
        result.setName("Campaign");
        result.setTargetAmount(targetAmount);
        campaignDAO.update(result);

        return result;
    }

    protected Campaign insertCampaign(long clientId, long profileId) throws PermissionException, NotFoundException {
        return createCampaign(clientId, profileId, BigDecimal.TEN, CampaignStatus.DRAFT);
    }

    protected Share createShare(long clientId, long profileId, Campaign campaign, BigDecimal price, int count) throws PermissionException, NotFoundException {
        Share share = new Share();
        share.setProfileId(profileId);
        share.setCampaignId(campaign.getCampaignId());
        share.setName(getTestStr("name"));
        share.setDescription(getTestStr("description"));
        share.setImageId(100L);
        share.setImageUrl(getTestUrl());
        share.setAmount(count);
        share.setTimeAdded(new Date());
        share.setPrice(price);

        Share result = campaignService.saveShare(clientId, share);
        assertNotNull(result);
        return result;
    }

    protected static String getTestStr(String str) {
        return String.format("junit_%s_%d", str, random());
    }

    protected static String getTestUrl(String uri) {
        return String.format("http://%s.com/%s", getTestStr("site"), uri);
    }

    protected static String getTestUrl() {
        return getTestUrl("uri-stub");
    }

    protected static String getTestEmail(String name) {
        String timestamp = String.valueOf(Calendar.getInstance().getTimeInMillis());
        return String.format("%s@test.ee", getTestStr(name) + timestamp);
    }
}

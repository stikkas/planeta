package ru.planeta.api.search;

import org.apache.log4j.Logger;
import org.junit.Ignore;
import org.junit.Test;
import org.sphx.api.SphinxException;
import ru.planeta.model.common.campaign.Campaign;
import ru.planeta.model.common.campaign.CampaignTag;
import ru.planeta.model.common.campaign.enums.CampaignStatus;
import ru.planeta.model.common.school.SeminarWithTagNameAndCityName;
import ru.planeta.test.AbstractTest;
import java.math.BigDecimal;
import java.util.*;

import static java.util.Collections.singletonList;
import static org.junit.Assert.*;

/**
 * Campaigns search test
 *
 * @author ds.kolyshev
 *         Date: 04.06.12
 */
public class TestCampaignsSearch extends AbstractTest {

    private static final Logger log = Logger.getLogger(TestCampaignsSearch.class);

    @Test
    // @Ignore //TODO jabber remove After deploy
    public void test() throws SphinxException {

        SearchCampaigns searchCampaigns = new SearchCampaigns(singletonList(SortOrder.SORT_BY_COLLECTION_RATE), "", EnumSet.of(CampaignStatus.ACTIVE, CampaignStatus.FINISHED), 0, 10);
        SearchResult searchResult = searchService.searchForCampaigns(searchCampaigns);
        assertNotNull(searchResult);
        assertNotNull(searchResult.getSearchResultRecords());
    }

    @Test
    public void testNo() throws SphinxException {

        SearchCampaigns searchCampaigns = new SearchCampaigns(singletonList(SortOrder.SORT_BY_COLLECTION_RATE), "№", EnumSet.of(CampaignStatus.ACTIVE, CampaignStatus.FINISHED), 0, 10);
        SearchResult searchResult = searchService.searchForCampaigns(searchCampaigns);
        assertNotNull(searchResult);
        assertNotNull(searchResult.getSearchResultRecords());
    }


    @Test
    public void test2() throws SphinxException {

        final int TOP_CAMPAIGNS_LIMIT = 20;
        CampaignStatusFilter statusFilter = CampaignStatusFilter.ACTIVE;
        List<CampaignTag> tags = campaignService.getAllTags();
        tags.remove(campaignService.getCampaignTagByMnemonic(CampaignTag.Companion.getCHARITY()));
        SearchResult<Campaign> searchResult = searchService.getTopFilteredCampaignsSearchResult(new SearchCampaigns(singletonList(SortOrder.SORT_DEFAULT), "", tags, statusFilter, null, null, null, null, null, null, null, false, 0, TOP_CAMPAIGNS_LIMIT));
        List<Campaign> campaigns = searchResult.getSearchResultRecords();
        assertNotNull(campaigns);
        for (Campaign campaign : campaigns) {
            List<CampaignTag> tagList = campaign.getTags();
            boolean contains = false;
            for (CampaignTag tag : tags) {
                if (tagList.contains(tag)) {
                    contains = true;
                    break;
                }
            }
            assertTrue(contains);
        }
    }

    @Test
    public void testCampaignSphinxSearch() throws SphinxException {


        final Range<Integer> readinessPercentRange = new Range<>(null, null);
        final Range<BigDecimal> targetAmountRange = new Range<>(null, null);
        final Range<Date> dateRange = new Range<>(null, null);
        SearchResult<Campaign> searchResult = searchService.getTopFilteredCampaignsSearchResult(
                new SearchCampaigns(singletonList(SortOrder.SORT_DEFAULT), "", null, CampaignStatusFilter.RECORD_HOLDERS, readinessPercentRange, targetAmountRange, dateRange, null, null, null, null, false, 0, 100));

        assertNotNull(searchResult);
        assertNotNull(searchResult.getSearchResultRecords());
        assertFalse(searchResult.getSearchResultRecords().isEmpty());

        searchResult = searchService.getTopFilteredCampaignsSearchResult(new SearchCampaigns(singletonList(SortOrder.SORT_DEFAULT), "", null, CampaignStatusFilter.ACTIVE, null, null, null, null, null, null, null, false, 0, 1000));
        assertNotNull(searchResult);
        assertNotNull(searchResult.getSearchResultRecords());

//		for (Campaign campaign : searchResult.getSearchResultRecords()) {
//			assertEquals(CampaignStatus.ACTIVE, campaign.getStatus());
//		}
    }

    @Test
    @Ignore
    public void testSeminarSphinxSearch() throws SphinxException {
        Calendar dateFrom = new GregorianCalendar(2011, Calendar.JULY, 3);
        Calendar dateTo = new GregorianCalendar(2017, Calendar.JULY, 3);

        SearchResult<SeminarWithTagNameAndCityName> seminarSearchResult = searchService.searchForSeminars("1", dateFrom.getTime(), dateTo.getTime(), 0, 10);

        List<SeminarWithTagNameAndCityName> seminars = seminarSearchResult.getSearchResultRecords();
        System.out.println();
    }

    @Test
    @Ignore
    public void testSeminarSphinxSearchByTagId() throws SphinxException {
        Calendar dateFrom = new GregorianCalendar(2011, Calendar.JULY, 3);
        Calendar dateTo = new GregorianCalendar(2017, Calendar.JULY, 3);

        SearchResult<SeminarWithTagNameAndCityName> seminarSearchResult = searchService.searchForSeminarsByTagId(0L, dateFrom.getTime(), 0, 10);

        List<SeminarWithTagNameAndCityName> seminars = seminarSearchResult.getSearchResultRecords();
        System.out.println();
    }
}

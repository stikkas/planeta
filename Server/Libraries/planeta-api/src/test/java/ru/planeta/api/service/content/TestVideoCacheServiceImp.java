package ru.planeta.api.service.content;

import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import ru.planeta.model.common.CachedVideo;
import ru.planeta.test.AbstractTest;

import static org.junit.Assert.assertNotNull;


/**
 * Created with IntelliJ IDEA.
 * User: connanedoile
 * Date: 4/10/12
 * Time: 4:43 PM
 */
public class TestVideoCacheServiceImp extends AbstractTest {

	@Autowired
	private VideoCacheService videoCacheService;

    @Test
    @Ignore
    public void testGetVideo() throws Exception {
        CachedVideo cachedVideo = videoCacheService.getVideo("http://vimeo.com/7100569");
        assertNotNull(cachedVideo);
    }
}

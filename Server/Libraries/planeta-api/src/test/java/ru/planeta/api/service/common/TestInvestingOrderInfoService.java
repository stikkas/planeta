package ru.planeta.api.service.common;

import java.util.Date;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import ru.planeta.api.exceptions.NotFoundException;
import ru.planeta.api.exceptions.PermissionException;
import ru.planeta.dao.commondb.InvestingOrderInfoDAO;
import ru.planeta.model.common.InvestingOrderInfo;
import ru.planeta.test.AbstractTest;

/**
 *
 * User: Serge Blagodatskih<stikkas17@gmail.com><br>
 * Date: 28.01.16<br>
 * Time: 11:24
 */
public class TestInvestingOrderInfoService extends AbstractTest {

    @Autowired
    private InvestingOrderInfoService service;

    @Autowired
    private InvestingOrderInfoDAO dao;

    private final long ID = Long.MAX_VALUE;
    private final InvestingOrderInfo item = new InvestingOrderInfo(ID, new Date());

    private InvestingOrderInfo createNewItem(long id) {
        InvestingOrderInfo it = new InvestingOrderInfo();
        it.setInvestingOrderInfoId(id);
        return it;
    }

    @Test
    public void testSaveAndSelect() throws NotFoundException, PermissionException {
        service.save(createNewItem(ID - 1));
        InvestingOrderInfo info1 = service.select(ID - 1);
        assertNotNull(info1);
        assertEquals(Long.valueOf(ID - 1), info1.getInvestingOrderInfoId());

        dao.insert(item);
        info1 = service.select(ID);
        assertNotNull(info1);
        assertEquals(Long.valueOf(ID), info1.getInvestingOrderInfoId());

        info1.setFirstName("First");
        service.save(info1);
        info1 = service.select(ID);
        assertEquals(Long.valueOf(ID), info1.getInvestingOrderInfoId());
        assertEquals("First", info1.getFirstName());
    }

    @Test
    public void testDelete() throws NotFoundException, PermissionException {
        service.save(createNewItem(ID - 1));
        assertNotNull(service.select(ID - 1));
        service.delete(ID - 1);
        assertNull(service.select(ID - 1));
    }
}

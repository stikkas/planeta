package ru.planeta.api.service.campaign;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import ru.planeta.api.exceptions.NotFoundException;
import ru.planeta.api.exceptions.PermissionException;
import ru.planeta.dao.commondb.CampaignFaqDAO;
import ru.planeta.model.common.UserPrivateInfo;
import ru.planeta.model.common.campaign.Campaign;
import ru.planeta.model.common.campaign.enums.CampaignStatus;
import ru.planeta.model.commondb.CampaignFaq;
import ru.planeta.test.AbstractTest;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.*;

public class TestCampaignFaqService extends AbstractTest {
    @Autowired
    private CampaignFaqService campaignFaqService;
    @Autowired
    private CampaignFaqDAO campaignFaqDAO;

    @Test
    public void testInsertUpdateDeleteSelect() throws NotFoundException, PermissionException {
        Campaign campaign = createCampaign();

        CampaignFaq faq = CampaignFaq.Companion.getBuilder()
                .campaignId(campaign.getCampaignId())
                .question("Question 1")
                .answer("Answer 1")
                .build();
        campaignFaqService.insertCampaignFaq(faq);

        CampaignFaq selectedFaq = campaignFaqService.selectCampaignFaq(faq.getCampaignFaqId());
        assertNotNull(selectedFaq);
        assertEquals(faq.getCampaignId(), selectedFaq.getCampaignId());

        faq.setAnswer("Answer 11");
        campaignFaqService.updateCampaignFaq(faq);

        selectedFaq = campaignFaqService.selectCampaignFaq(faq.getCampaignFaqId());
        assertNotNull(selectedFaq);
        assertEquals(faq.getCampaignId(), selectedFaq.getCampaignId());

        campaignFaqService.deleteCampaignFaq(faq.getCampaignFaqId());
        selectedFaq = campaignFaqService.selectCampaignFaq(faq.getCampaignFaqId());
        assertNull(selectedFaq);


    }

    @Test
    public void testOrderNum() throws NotFoundException, PermissionException {
        Campaign campaign = createCampaign();

        List<CampaignFaq> list = new ArrayList<>();
        CampaignFaq faq = CampaignFaq.Companion.getBuilder()
                .campaignId(campaign.getCampaignId())
                .question("Question 1")
                .answer("Answer 1")
                .build();

        list.add(faq);
        campaignFaqService.insertCampaignFaq(faq);

        faq = CampaignFaq.Companion.getBuilder()
                .campaignId(campaign.getCampaignId())
                .question("Question 2")
                .answer("Answer 2")
                .build();
        list.add(faq);
        campaignFaqService.insertCampaignFaq(faq);

        faq = CampaignFaq.Companion.getBuilder()
                .campaignId(campaign.getCampaignId())
                .question("Question 3")
                .answer("Answer 3")
                .build();
        list.add(faq);
        campaignFaqService.insertCampaignFaq(faq);

        List<CampaignFaq> selectedList = campaignFaqService.selectCampaignFaqListByCampaign(campaign.getCampaignId());
        assertNotNull(selectedList);
        assertEquals(selectedList.size(), list.size());
        for (int i = 0; i < list.size(); i++) {
            compareFaq(selectedList.get(i), list.get(i));
        }
    }

    @Test
    public void testOrderNum2() throws NotFoundException, PermissionException {
        Campaign campaign = createCampaign();

        List<CampaignFaq> list = new ArrayList<>();

        CampaignFaq faq = CampaignFaq.Companion.getBuilder()
                .campaignId(campaign.getCampaignId())
                .question("Question 3")
                .orderNum(1)
                .answer("Answer 3")
                .build();

        list.add(0, faq);
        campaignFaqService.insertCampaignFaq(faq);

        faq = CampaignFaq.Companion.getBuilder()
                .campaignId(campaign.getCampaignId())
                .question("Question 2")
                .answer("Answer 2")
                .orderNum(1)
                .build();

        list.add(0, faq);
        campaignFaqService.insertCampaignFaq(faq);

        faq = CampaignFaq.Companion.getBuilder()
                .campaignId(campaign.getCampaignId())
                .question("Question 1")
                .answer("Answer 1")
                .orderNum(1)
                .build();

        list.add(0, faq);
        campaignFaqService.insertCampaignFaq(faq);


        List<CampaignFaq> selectedList = campaignFaqService.selectCampaignFaqListByCampaign(campaign.getCampaignId());
        assertNotNull(selectedList);
        assertEquals(selectedList.size(), list.size());
        for (int i = 0; i < list.size(); i++) {
            compareFaq(selectedList.get(i), list.get(i));
        }

    }


    @Test
    public void testReplaceFaq() throws NotFoundException, PermissionException {
        Campaign campaign = createCampaign();

        List<CampaignFaq> list = new ArrayList<>();

        for (int i = 0; i < 5; i++) {
            CampaignFaq faq = CampaignFaq.Companion.getBuilder()
                    .campaignId(campaign.getCampaignId())
                    .question("Question " + (i + 1))
                    .answer("Answer " + (i + 1))
                    .build();

            list.add(faq);
            campaignFaqService.insertCampaignFaq(faq);
        }
        CampaignFaq faq = list.get(0);
        faq.setOrderNum(3);
        campaignFaqService.updateCampaignFaq(faq);

        list.remove(0);
        list.add(2, faq);

        List<CampaignFaq> selectedList = campaignFaqService.selectCampaignFaqListByCampaign(campaign.getCampaignId());
        assertNotNull(selectedList);
        assertEquals(selectedList.size(), list.size());
        for (int i = 0; i < list.size(); i++) {
            compareFaq(selectedList.get(i), list.get(i));
        }


    }

    @Test
    public void testReplaceFaq2() throws NotFoundException, PermissionException {
        Campaign campaign = createCampaign();

        List<CampaignFaq> list = new ArrayList<>();

        for (int i = 0; i < 5; i++) {
            CampaignFaq faq = CampaignFaq.Companion.getBuilder()
                    .campaignId(campaign.getCampaignId())
                    .question("Question " + (i + 1))
                    .answer("Answer " + (i + 1))
                    .build();

            list.add(faq);
            campaignFaqService.insertCampaignFaq(faq);
        }
        CampaignFaq faq = list.get(4);
        faq.setOrderNum(3);
        campaignFaqService.updateCampaignFaq(faq);

        list.remove(4);
        list.add(2, faq);

        List<CampaignFaq> selectedList = campaignFaqService.selectCampaignFaqListByCampaign(campaign.getCampaignId());
        assertNotNull(selectedList);
        assertEquals(selectedList.size(), list.size());
        for (int i = 0; i < list.size(); i++) {
            compareFaq(selectedList.get(i), list.get(i));
        }
    }


    @Test
    public void testSelectByIdList() {
        List<Long> idList = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            CampaignFaq faq = CampaignFaq.Companion.getBuilder()
                    .campaignId(0L)
                    .question("Question ")
                    .answer("Answer ")
                    .build();

            campaignFaqService.insertCampaignFaq(faq);
            idList.add(faq.getCampaignFaqId());
        }
        for (int i = 0; i < 4; i++) {
            checkIdOrder(idList, campaignFaqDAO.selectByIdList(idList));
            idList.add(idList.get(0));
            idList.remove(0);
        }
    }

    private void checkIdOrder(List<Long> expectedList, List<CampaignFaq> actualList) {
        assertEquals(expectedList.size(), actualList.size());
        for (int i = 0; i < expectedList.size(); i++) {
            assertEquals(expectedList.get(i), actualList.get(i).getCampaignFaqId());
        }

    }

    private Campaign createCampaign() throws NotFoundException, PermissionException {
        UserPrivateInfo user = insertPlanetaAdminPrivateInfo();
        long clientId = user.getUserId();
        long profileId = registerSimpleGroup(clientId);
        setGroupIsMerchant(profileId);
        return createCampaign(clientId, profileId, BigDecimal.TEN, CampaignStatus.ACTIVE);
    }

    private void compareFaq(CampaignFaq expected, CampaignFaq actual) {
        assertNotNull(expected);
        assertEquals(expected.getCampaignId(), actual.getCampaignId());
        assertEquals(expected.getAnswer(), actual.getAnswer());
        assertEquals(expected.getQuestion(), actual.getQuestion());
    }


    @Test
    public void resort() throws NotFoundException, PermissionException {
        final Campaign campaign = createCampaign();
        long campaignId = campaign.getCampaignId();
        long profileId = campaign.getCreatorProfileId();


        List<CampaignFaq> list = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            list.add(insertCampaignFaq(campaignId));
        }

        for (int i = 0; i < list.size(); i++) {
            final CampaignFaq campaignFaq = list.get(i);
            assertEquals((Integer) (i + 1), campaignFaq.getOrderNum());
        }


        resortAndCheck(1, 3, list, profileId);
        resortAndCheck(3, 1, list, profileId);
        resortAndCheck(0, 3, list, profileId);
        resortAndCheck(3, 0, list, profileId);
        resortAndCheck(2, 4, list, profileId);
        resortAndCheck(4, 2, list, profileId);
        resortAndCheck(0, 4, list, profileId);
        resortAndCheck(4, 0, list, profileId);

    }

    private CampaignFaq insertCampaignFaq(Long campaignId) {
        CampaignFaq faq = CampaignFaq.Companion.getBuilder()
                .campaignId(campaignId)
                .question("Question " + System.currentTimeMillis())
                .answer("Answer " + System.currentTimeMillis())
                .build();
        campaignFaqService.insertCampaignFaq(faq);
        return faq;
    }

    private void resortAndCheck(int indexFrom, int indexTo, List<CampaignFaq> list, long profileId) throws NotFoundException, PermissionException {
        final Long campaignId = list.get(indexFrom).getCampaignId();
        campaignFaqService.resort(profileId, list.get(indexFrom).getCampaignFaqId(), list.get(indexTo).getCampaignFaqId(), campaignId);
        final CampaignFaq removed = list.remove(indexFrom);
        list.add(indexTo, removed);

        final List<CampaignFaq> selectedList = campaignFaqService.selectCampaignFaqListByCampaign(campaignId, 0, 0);
        for (int i = 0; i < list.size(); i++) {
            assertEquals(list.get(i).getCampaignFaqId(), selectedList.get(i).getCampaignFaqId());
            assertEquals((Integer) (i + 1), selectedList.get(i).getOrderNum());
        }

    }
}

package ru.planeta.api.service.registration;

import org.apache.commons.lang3.tuple.Pair;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import ru.planeta.api.exceptions.PermissionException;
import ru.planeta.api.model.UserAuthorizationInfo;
import ru.planeta.model.enums.ProfileStatus;
import ru.planeta.model.stat.RequestStat;
import ru.planeta.test.AbstractTest;

import java.util.UUID;

public class RegistrationServiceTest extends AbstractTest {

    private RegistrationService registrationService;

    @Autowired
    public void setRegistrationService(RegistrationService registrationService) {
        this.registrationService = registrationService;
        this.registrationService.setAsynchronous(false);
    }

    @Test
    public void testRegisterByEmail() throws Exception {
        final String testEmail = getTestEmail("user");
        RequestStat stat = new RequestStat();
        stat.setIp("127.0.0.1");
        UserAuthorizationInfo userAuthInfo = registrationService.registerByEmail(testEmail, getTestStr("password"), stat);
        Assert.assertNotNull(userAuthInfo);
        Assert.assertNotNull(userAuthInfo.getUserPrivateInfo());
        Assert.assertNotNull(userAuthInfo.getUser());

        Assert.assertEquals(ProfileStatus.NOT_SET, userAuthInfo.getProfile().getStatus());
    }

    @Test
    @Ignore
    public void testAutoRegisterByEmail() throws PermissionException {
        for (int i = 0; i < 1000; i ++) {
            long start = System.currentTimeMillis();
            String email = UUID.randomUUID().toString().replace("-", "") + "@fakeinbox.com";
            Pair<UserAuthorizationInfo, String> pair = registrationService.autoRegisterGetPassword(email, "test", null);
            long finish = System.currentTimeMillis();
            if (finish - start > 1000) {
                System.out.println(finish - start);
            }
        }
    }
}

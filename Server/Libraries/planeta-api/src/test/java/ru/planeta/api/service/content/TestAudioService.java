package ru.planeta.api.service.content;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import ru.planeta.api.exceptions.NotFoundException;
import ru.planeta.dao.profiledb.AudioTrackDAO;
import ru.planeta.model.profile.Profile;
import ru.planeta.model.profile.media.AudioTrack;
import ru.planeta.test.AbstractTest;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Tests for audio service
 *
 * @author s.fionov
 *         Date: 28.10.11
 */
public class TestAudioService extends AbstractTest {

    @Autowired
    private AudioService audioService;
    @Autowired
    private AudioTrackDAO audioTrackDAO;

    /**
     * Test for group audio tracks
     *
     * @throws NotFoundException
     */
    @Test
    public void testAudioTracks() throws Exception {


        Profile user = insertUserProfile();
        long profileId = user.getProfileId();
        long clientId = profileId;
        //add new track
        final long albumId = 0;
        AudioTrack audioTrack = getNewAudioTrack(profileId, albumId);
        audioTrackDAO.insert(audioTrack);
        List<AudioTrack> tracks = audioService.getAudioTracks(clientId, profileId, 0, 10, null);
        assertNotNull(tracks);
        assertEquals(1, tracks.size());

        //check track
        AudioTrack insertedTrack = tracks.get(0);
        checkAudioTrack(audioTrack, insertedTrack);

        //check selectCampaignById by id
        AudioTrack selectedTrack = audioService.getAudioTrack(clientId, profileId, audioTrack.getTrackId());
        checkAudioTrack(audioTrack, selectedTrack);

        //check updating track's info
        insertedTrack.setTrackName("upd track name");
        insertedTrack.setArtistName("upd artist name");
        audioService.saveAudioTrack(clientId, insertedTrack);

        tracks = audioService.getAudioTracks(clientId, profileId, 0, 10, null);
        assertNotNull(tracks);
        assertEquals(1, tracks.size());
        checkAudioTrack(tracks.get(0), insertedTrack);

    }

    /**
     * Test for audio album tracks relations
     *
     * @throws Exception
     */
    @Test
    public void testAudioAlbumTracks() throws Exception {


        Profile user = insertUserProfile();
        long profileId = user.getProfileId();
        long clientId = profileId;
        long albumId = 0;
        AudioTrack audioTrack = getNewAudioTrack(profileId, albumId);
        audioTrackDAO.insert(audioTrack);
        AudioTrack insertedTrack = audioService.getAudioTracks(clientId, profileId, 0, 10, null).get(0);
        checkAudioTrack(audioTrack, insertedTrack);

        //selectCampaignById album's tracks
        List<AudioTrack> albumTracks = audioService.getAudioAlbumTracks(clientId, profileId, albumId, 0, 10);
        assertNotNull(albumTracks);
        assertEquals(1, albumTracks.size());
        checkAudioTrack(audioTrack, albumTracks.get(0));
    }

    private static AudioTrack getNewAudioTrack(long profileId, long albumId) {
        AudioTrack audioTrack = new AudioTrack();
        audioTrack.setProfileId(profileId);
        audioTrack.setArtistName("test artist");
        audioTrack.setTrackName("test trackname");
        audioTrack.setAuthors("authors");
        audioTrack.setLyrics("lyrics");
        audioTrack.setTrackUrl("trackUrl");
        audioTrack.setTrackDuration(1899);
        audioTrack.setAlbumId(albumId);
        return audioTrack;
    }

    private static void checkAudioTrack(AudioTrack expected, AudioTrack actual) {
        assertNotNull(actual);
        assertEquals(expected.getTrackId(), actual.getTrackId());
        assertEquals(expected.getArtistName(), actual.getArtistName());
        assertEquals(expected.getTrackName(), actual.getTrackName());
        assertEquals(expected.getAuthors(), actual.getAuthors());
        assertEquals(expected.getLyrics(), actual.getLyrics());
        assertEquals(expected.getTrackUrl(), actual.getTrackUrl());
        assertEquals(expected.getTrackDuration(), actual.getTrackDuration());
        assertEquals(expected.getAlbumId(), actual.getAlbumId());
    }
}

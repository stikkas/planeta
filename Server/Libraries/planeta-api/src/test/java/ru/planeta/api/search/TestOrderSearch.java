package ru.planeta.api.search;

import org.junit.Ignore;
import org.junit.Test;
import org.sphx.api.SphinxException;
import org.springframework.beans.factory.annotation.Autowired;
import ru.planeta.api.exceptions.NotFoundException;
import ru.planeta.api.search.filters.SearchOrderFilter;
import ru.planeta.test.AbstractTest;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;

public class TestOrderSearch extends AbstractTest {
    @Autowired
    SearchServiceImpl searchService;

    @Ignore
    @Test
    public void test() throws SphinxException, NotFoundException {
        SearchOrderFilter searchOrderFilter = new SearchOrderFilter();
        searchOrderFilter.setCampaignId(444);
        searchOrderFilter.setSearchString("");
        searchOrderFilter.setCityAndCountrySearchString("Абхазия");

//        SearchResult searchResult = searchService.searchForOrders(SearchIndex.ORDERS, searchOrderFilter);
//        assertNotNull(searchResult);
//        assertNotNull(searchResult.getSearchResultRecords());
//        assertFalse(searchResult.getSearchResultRecords().isEmpty());
    }
}

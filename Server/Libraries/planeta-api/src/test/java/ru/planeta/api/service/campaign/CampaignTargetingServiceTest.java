package ru.planeta.api.service.campaign;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import ru.planeta.api.exceptions.NotFoundException;
import ru.planeta.api.exceptions.PermissionException;
import ru.planeta.model.common.UserPrivateInfo;
import ru.planeta.model.common.campaign.Campaign;
import ru.planeta.model.common.campaign.CampaignTargeting;
import ru.planeta.test.AbstractTest;
import static org.junit.Assert.*;

public class CampaignTargetingServiceTest extends AbstractTest {
    @Autowired
    private CampaignTargetingService campaignTargetingService;

    @Test
    public void testCampaignTargetingBase() throws NotFoundException, PermissionException {
        UserPrivateInfo user = insertPlanetaAdminPrivateInfo();
        long clientId = user.getUserId();
        long profileId = registerSimpleGroup(clientId);
        setGroupIsMerchant(profileId);

        Campaign campaign = insertCampaign(clientId, profileId);

        long campaignId = campaign.getCampaignId();
        String vkTargetingId = "123";

        CampaignTargeting campaignTargeting = new CampaignTargeting();
        campaignTargeting.setCampaignId(campaignId);
        campaignTargeting.setVkTargetingId(vkTargetingId);

        campaignTargetingService.insertOrUpdate(191, campaignTargeting);

        CampaignTargeting selectedCampaignTargeting = campaignTargetingService.getCampaignTargetingByCampaignId(campaignId);
        assertNotNull(selectedCampaignTargeting);
        assertEquals(campaignId, selectedCampaignTargeting.getCampaignId());
        assertEquals(vkTargetingId, selectedCampaignTargeting.getVkTargetingId());
        assertNull(selectedCampaignTargeting.getFbTargetingId());

        String newVkTargetingId = "321";
        String newFbTargetingId = "654";

        selectedCampaignTargeting.setVkTargetingId(newVkTargetingId);
        selectedCampaignTargeting.setFbTargetingId(newFbTargetingId);

        campaignTargetingService.insertOrUpdate(191, selectedCampaignTargeting);

        CampaignTargeting updatedCampaignTargeting = campaignTargetingService.getCampaignTargetingByCampaignId(selectedCampaignTargeting.getCampaignId());
        assertEquals(selectedCampaignTargeting.getCampaignId(), updatedCampaignTargeting.getCampaignId());
        assertEquals(newVkTargetingId, updatedCampaignTargeting.getVkTargetingId());
        assertEquals(newFbTargetingId, updatedCampaignTargeting.getFbTargetingId());
    }
}

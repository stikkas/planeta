package ru.planeta.api.service.shop;

import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import ru.planeta.api.exceptions.NotFoundException;
import ru.planeta.api.exceptions.PermissionException;
import ru.planeta.model.shop.Product;
import ru.planeta.model.shop.ProductInfo;
import ru.planeta.model.shop.enums.ProductStatus;
import ru.planeta.test.AbstractTest;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * @author ds.kolyshev
 * Date: 30.07.12
 */
public class TestProductModerationService extends AbstractTest {
	@Autowired
    private ProductUsersService productService;

    @Test
    @Ignore
    public void testVersionsModeration() throws NotFoundException, PermissionException {
        
        
            long moderatorProfileId = insertPlanetaAdminPrivateInfo().getUserId();
            long clientId = insertUserProfile().getProfileId();
            long merchantId = registerSimpleGroup(clientId);

            //create product with childs
            ProductInfo product = createProductMETA(merchantId);
            List<ProductInfo> childs = new ArrayList<ProductInfo>();
            for (int i = 0; i < 3; i++) {
                childs.add(new ProductInfo());
            }
            childs.get(0).setProductAttribute(createProductAttribute("9.5"));
            childs.get(1).setProductAttribute(createProductAttribute("10"));
            childs.get(2).setProductAttribute(createProductAttribute("10.5"));
            product.setChildrenProducts(childs);

            productService.saveProduct(clientId, product);

            //approve version
            //List<ProductChangesetInfo> toApproveList = productModerationService.getProductChangesetInfoList(moderatorProfileId, 0, 0);


            product.setProductStatus(ProductStatus.ACTIVE);
			productService.startProduct(clientId,product.getProductId());
            ProductInfo activated = productService.getProductCurrent(moderatorProfileId, product.getProductId());
            assertProductsEquals(activated, product);


            //check last version selectCampaignById
			productService.saveProduct(clientId,product);
            Product lastVersion = productService.getProductCurrent(moderatorProfileId, product.getProductId());
			assertProductsEquals(lastVersion, product);

    }

    private static void assertProductsEquals(Product actual, Product expected) {
        assertNotNull(actual);
        assertEquals(expected.getProductId(), actual.getProductId());
        assertEquals(expected.getMerchantProfileId(), actual.getMerchantProfileId());
        assertEquals(expected.getParentProductId(), actual.getParentProductId());
        assertEquals(expected.getName(), actual.getName());
        assertEquals(expected.getProductStatus(), actual.getProductStatus());
        assertEquals(expected.getDescription(), actual.getDescription());
        assertEquals(expected.getStoreId(), actual.getStoreId());
        //check attribute
        if (expected.getProductAttribute() != null) {
            assertNotNull(actual.getProductAttribute());
            assertEquals(expected.getProductAttribute().getAttributeTypeId(), actual.getProductAttribute().getAttributeTypeId());
            assertEquals(expected.getProductAttribute().getValue(), actual.getProductAttribute().getValue());
        }
        //TODO: check childs
    }

    private static void checkChildStatus(ProductInfo product, int childCount, ProductStatus status) {
        assertNotNull(product.getChildrenProducts());
        assertEquals(childCount, product.getChildrenProducts().size());
        for (int i = 0; i < childCount; i++) {
            assertEquals(status, product.getChildrenProducts().get(i).getProductStatus());
        }
    }
}

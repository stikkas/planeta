package ru.planeta.api.service.billing.order;

import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import ru.planeta.api.service.biblio.BookService;
import ru.planeta.model.bibliodb.BookTag;
import ru.planeta.model.common.Order;
import ru.planeta.model.common.OrderInfo;
import ru.planeta.model.common.OrderObject;
import ru.planeta.test.AbstractTest;

import java.util.List;

import static org.junit.Assert.*;

public class BiblioAfterFundServiceTest extends AbstractTest{
    @Autowired
    private BiblioAfterFundService biblioAfterFundService;
    @Autowired
    private OrderService orderService;

    @Ignore
    @Test
    public void purchase() throws Exception {
        long orderId = 714748;
        Order order = orderService.getOrder(orderId);
        assertNotNull(order);
        List<OrderObject> orderObjectsList = orderService.getOrderObjects(orderId);

        biblioAfterFundService.purchase(order, orderObjectsList);
    }

}

package ru.planeta.api.service.social;

import org.junit.Ignore;
import org.junit.Test;
import ru.planeta.api.model.ExternalUserInfo;
import ru.planeta.commons.web.WebUtils;
import ru.planeta.test.AbstractTest;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Date: 18.09.12
 *
 * @author s.kalmykov
 */
// ignore to not disturb external services. Remove if necessary
@Ignore
public class ExternalServiceTest extends AbstractTest {

    @Test
    public void testFbFlushCache() throws IOException {
        // https://graph.facebook.com/oauth/access_token?client_id=283941085052946&client_secret=c3181474330be1669bc81c33b0167b0d&grant_type=client_credentials
        String getAccessTokenUri = "https://graph.facebook.com/oauth/access_token";
        String getAccessTokenUrl = WebUtils.createUrl(getAccessTokenUri, new WebUtils.Parameters()
                .add("client_id", "283941085052946")
                .add("client_secret", "c3181474330be1669bc81c33b0167b0d")
                .add("grant_type", "client_credentials").getParams());

        String token = WebUtils.downloadString(getAccessTokenUrl);
        if(token != null) {
            String campaignUrl = "https://planeta.ru/campaigns/brailleglove";
            String scrapeUri = "https://graph.facebook.com/";
            String result = WebUtils.uploadMapExternal(scrapeUri, new WebUtils.Parameters().
                    add("id", campaignUrl).
                    add("scrape", "true").
                    add("access_token",token.substring(token.indexOf("=") + 1)).getParams());
            //  curl -v --data "id=https://planeta.ru/campaigns/350&scrape=true" https://graph.facebook.com/  - 500
            //  curl -v --data "id=https://planeta.ru/campaigns/brailleglove&scrape=true" https://graph.facebook.com/  - 200 Ok
        }
    }


    @Test
    public void testMailRuTest() throws Exception {
        String expectedSignature = "4a05af66f80da18b308fa7e536912bae";
        String accessToken = "be6ef89965d58e56dec21acb9b62bdaa";
        String SECRET = "3dad9cbf9baaa0360c0f2ba372d25716";

        String myToken = "12322296aca3eb441358987c1971d9a7";
        String mySecret = "167e46ca32678a04b1a3efc220e6f2f0";
        String APP_ID = "687562";

        String request = MailRuServiceImpl.generateDataUrl(myToken, mySecret, APP_ID, "users.getInfo");

        String exampleRequest = MailRuServiceImpl.generateDataUrl(accessToken, SECRET, "423004", "friends.get");
        Matcher matcher = (Pattern.compile(".*sig=([^&]*)")).matcher(exampleRequest);
        matcher.find();
        String sig = matcher.group(1);
        assertEquals(expectedSignature, sig);


        assertNotNull(request);
        String mailRuResponse = "[{\"pic_50\":\"http://avt.appsmail.ru/mail/ru.planeta/_avatar50\",\"friends_count\":0,\"pic_22\":\"http://avt.appsmail.ru/mail/ru.planeta/_avatar22\",\"nick\":\"Планета Ру\",\"is_verified\":1,\"is_online\":1,\"pic_big\":\"http://avt.appsmail.ru/mail/ru.planeta/_avatarbig\",\"last_name\":\"Ру\",\"has_pic\":0,\"email\":\"ru.planeta@mail.ru\",\"pic_190\":\"http://avt.appsmail.ru/mail/ru.planeta/_avatar190\",\"referer_id\":\"\",\"vip\":0,\"pic_32\":\"http://avt.appsmail.ru/mail/ru.planeta/_avatar32\",\"birthday\":\"01.01.2000\",\"referer_type\":\"\",\"link\":\"http://my.mail.ru/mail/ru.planeta/\",\"location\":{\"country\":{\"name\":\"Россия\",\"id\":\"24\"},\"city\":{\"name\":\"Москва\",\"id\":\"25\"},\"region\":{\"name\":\"Москва\",\"id\":\"999999\"}},\"uid\":\"10732621195843539795\",\"app_installed\":1,\"pic_128\":\"http://avt.appsmail.ru/mail/ru.planeta/_avatar128\",\"sex\":1,\"pic\":\"http://avt.appsmail.ru/mail/ru.planeta/_avatar\",\"pic_small\":\"http://avt.appsmail.ru/mail/ru.planeta/_avatarsmall\",\"pic_180\":\"http://avt.appsmail.ru/mail/ru.planeta/_avatar180\",\"first_name\":\"Планета\",\"pic_40\":\"http://avt.appsmail.ru/mail/ru.planeta/_avatar40\"}]";
        ExternalUserInfo info =  MailRuServiceImpl.parseJSON(mailRuResponse);
        assertNotNull(info);
//        http://www.appsmail.ru/platform/api?secure=1&method=friends.get&app_id=1324730981306483817&session_key=be6ef89965d58e56dec21acb9b62bdaa&sig=f45024d9d183132f70f084ea767d39ea
//        http://www.appsmail.ru/platform/api?method=friends.get&app_id=423004&session_key=be6ef89965d58e56dec21acb9b62bdaa&secure=1&sig=4a05af66f80da18b308fa7e536912bae

//        Server returned HTTP response code: 400 for URL:
// http://www.appsmail.ru/platform/api?secure=1&method=users.getInfo&app_id=687562&session_key=12322296aca3eb441358987c1971d9a7&sig=aa1591229b96a9f3f2bcd8e1f082eedd
// http://www.appsmail.ru/platform/api?secure=1&method=users.getInfo&app_id=687562&session_key=12322296aca3eb441358987c1971d9a7&sig=aa1591229b96a9f3f2bcd8e1f082eedd

// http://www.appsmail.ru/platform/api?secure=1&method=friends.get&app_id=423004&session_key=be6ef89965d58e56dec21acb9b62bdaa&sig=4a05af66f80da18b308fa7e536912bae
// http://www.appsmail.ru/platform/api?method=friends.get&app_id=423004&session_key=be6ef89965d58e56dec21acb9b62bdaa&secure=1&sig=4a05af66f80da18b308fa7e536912bae
    }

}

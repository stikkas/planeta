package ru.planeta.api.service.campaign;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import ru.planeta.model.common.delivery.LinkedDelivery;
import ru.planeta.model.common.delivery.SubjectType;
import ru.planeta.test.AbstractTest;

import java.util.List;

import static org.junit.Assert.assertTrue;

/**
 * Created with IntelliJ IDEA.
 * Date: 29.12.2015
 * Time: 19:11
 * To change this template use File | Settings | File Templates.
 */
public class DeliveryServiceImplTest extends AbstractTest {

    @Autowired
    DeliveryService deliveryService;

    @Test
    public void testGetLinkedDeliveries() throws Exception {

        List<LinkedDelivery> list = deliveryService.getLinkedDeliveries(0, SubjectType.SHOP);

        assertTrue(list.size() > 0);
    }


}

package ru.planeta.api.faq;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import ru.planeta.api.service.faq.FaqArticleService;
import ru.planeta.model.commondb.FaqArticle;
import ru.planeta.test.AbstractTest;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by kostiagn on 29.12.2015.
 */
public class TestFaqArticleService extends AbstractTest {
    @Autowired
    private FaqArticleService faqArticleService;

    private static final long TEST_CATEGORY_ID = 100L;


    @Test
    public void testResort() {
        List<FaqArticle> list = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            list.add(insertFaqArticle());
        }

        for (int i = 0; i < list.size(); i++) {
            FaqArticle faqArticle = list.get(i);
            assertEquals((Integer) (i + 1), faqArticle.getOrderNum());
        }


        resortAndCheck(1, 3, list);
        resortAndCheck(3, 1, list);
        resortAndCheck(0, 3, list);
        resortAndCheck(3, 0, list);
        resortAndCheck(2, 4, list);
        resortAndCheck(4, 2, list);
        resortAndCheck(0, 4, list);
        resortAndCheck(4, 0, list);

    }

    private void resortAndCheck(int indexFrom, int indexTo, List<FaqArticle> list) {
        faqArticleService.resort(list.get(indexFrom).getFaqArticleId(), list.get(indexTo).getFaqArticleId(), TEST_CATEGORY_ID);
        FaqArticle removed = list.remove(indexFrom);
        list.add(indexTo, removed);

        final List<FaqArticle> selectedList = faqArticleService.selectFaqArticleListByFaqCategory(TEST_CATEGORY_ID, 0, 0);
        for (int i = 0; i < list.size(); i++) {
            assertEquals(list.get(i).getFaqArticleId(), selectedList.get(i).getFaqArticleId());
            assertEquals((Integer) (i + 1), selectedList.get(i).getOrderNum());
        }
    }


    private FaqArticle insertFaqArticle() {
        final FaqArticle faqArticle = FaqArticle.Companion.getBuilder().faqCategoryId(TEST_CATEGORY_ID).title("faq article #" + System.currentTimeMillis()).build();
        faqArticleService.insertFaqArticle(faqArticle);
        return faqArticle;
    }

}

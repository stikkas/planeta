package ru.planeta.api.service.shop;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import ru.planeta.model.shop.Category;
import ru.planeta.test.AbstractTest;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

/*
 * Created by Alexey on 06.09.2016.
 */

public class ProductTagServiceImplTest extends AbstractTest {

    @Autowired
    ProductTagService productTagService;

    @Test
    public void testGetTagByMnemonicNameList() throws Exception {
        List<Category> productTags = productTagService.getTagByMnemonicNameList(Arrays.asList("CHARITY", "BOOKS"));
        Assert.assertEquals("CHARITY", productTags.get(0).getMnemonicName());
        Assert.assertEquals("BOOKS", productTags.get(1).getMnemonicName());
    }
}

package ru.planeta.api.mail;

import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import ru.planeta.test.AbstractTest;

import java.util.Date;

/**
 *
 * Created by asavan on 30.11.2016.
 */
public class MailClientImplTest extends AbstractTest {
    @Autowired
    private MailClient mailClient;
    @Test
    @Ignore
    public void sendBiblioBookPriceChange() throws Exception {
        mailClient.sendBiblioBookPriceChange(20, 100, new Date(), "Lib1");
    }

}

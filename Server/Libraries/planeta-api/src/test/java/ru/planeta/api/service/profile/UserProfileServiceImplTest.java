package ru.planeta.api.service.profile;

import org.junit.Test;
import ru.planeta.api.exceptions.NotFoundException;
import ru.planeta.model.profile.Profile;
import ru.planeta.test.AbstractTest;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;

/**
 * @author Serge Blagodatskih<stikkas17@gmail.com>
 */
public class UserProfileServiceImplTest extends AbstractTest {
  

    private static final long wrongId = -172;

    @Test
    public void testGetProfileSafe() throws Exception {
        try {
            profileService.getProfileSafe(wrongId);
            fail("Expected an NotFoundException to be thrown");
        } catch (NotFoundException ex) {
            assertThat(ex.getMessage(), is(String.format("%s with id %d not found.", Profile.class.getSimpleName(), wrongId)));
        }
    }
    
}

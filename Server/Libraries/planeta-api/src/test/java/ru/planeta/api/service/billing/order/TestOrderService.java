package ru.planeta.api.service.billing.order;

import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import ru.planeta.api.exceptions.*;
import ru.planeta.api.log.service.DBLogServiceImpl;
import ru.planeta.model.common.OrderInfo;
import ru.planeta.model.common.OrderObjectInfo;
import ru.planeta.dao.commondb.OrderDAO;
import ru.planeta.model.common.*;
import ru.planeta.model.common.campaign.Campaign;
import ru.planeta.model.common.campaign.Share;
import ru.planeta.model.enums.*;
import ru.planeta.model.shop.enums.*;
import ru.planeta.dao.TransactionScope;
import ru.planeta.test.AbstractTest;

import java.math.BigDecimal;
import java.util.*;

import static org.junit.Assert.*;

/**
 * User: atropnikov, eshevchenko
 * Date: 09.04.12
 * Time: 13:16
 */
public class TestOrderService extends AbstractTest {

    @MockBean
    private DBLogServiceImpl dbLogService;

    @Autowired
    private OrderService orderService;

    @Test
    public void testOrderInfoSelects() throws NotFoundException, PermissionException, OrderException {
        final BigDecimal START_BALANCE = new BigDecimal(1000);
        final BigDecimal OBJECT_PRICE = BigDecimal.TEN;
        final int OBJECT_COUNT = 10;

        final long adminId = getProfileId(registerPlanetaAdminUser());
        final long buyerId = getProfileId(registerUser());
        final long merchantId = registerNewOfficialGroup(adminId);
        increaseProfileBalance(buyerId, START_BALANCE);

        Share share = createShareWithCampaign(adminId, merchantId, OBJECT_PRICE, OBJECT_COUNT);
        Campaign campaign = campaignService.getCampaignSafe(share.getCampaignId());
        purchaseShare(buyerId, buyerId, share.getShareId(), null, 1, null);
        purchaseShare(buyerId, buyerId, share.getShareId(), null, 1, null);

        Collection<OrderInfo> orderInfoList = orderService.getBuyerOrdersInfo(buyerId, buyerId, null, null, null, 0, 0);
        assertNotNull(orderInfoList);
        assertEquals(2, orderInfoList.size());
        assertEquals(orderInfoList.size(), orderService.getMerchantOrdersInfo(merchantId, merchantId, null, null, null, null, null, 0, 0).size());

        orderInfoList = orderService.getBuyerOrdersInfo(buyerId, buyerId, OrderObjectType.SHARE, null, null, 0, 0);
        assertEquals(2, orderInfoList.size());
        assertEquals(orderInfoList.size(), orderService.getMerchantOrdersInfo(merchantId, merchantId, OrderObjectType.SHARE, null, null, null, null, 0, 0).size());

        OrderObjectInfo objectInfo = getFirstOrderObjectInfo(orderInfoList.iterator().next());
        assertEquals(share.getName(), objectInfo.getObjectName());
        assertEquals(share.getImageUrl(), objectInfo.getObjectImageUrl());
//        assertEquals(share.getCampaignId(), objectInfo.getOwnerId());
        assertEquals(campaign.getName(), objectInfo.getOwnerName());
    }

    private void checkOrderStatus(Order order, PaymentStatus paymentStatus, DeliveryStatus deliveryStatus) {
        assertNotNull(order);
        assertEquals(paymentStatus, order.getPaymentStatus());
        assertEquals(deliveryStatus, order.getDeliveryStatus());
    }

    private void assertOrderPayed(Order order) {
        checkOrderStatus(order, PaymentStatus.COMPLETED, DeliveryStatus.PENDING);
    }

    private void assertOrderFailed(Order order) {
        checkOrderStatus(order, PaymentStatus.FAILED, DeliveryStatus.PENDING);
    }

    private OrderObjectInfo getFirstOrderObjectInfo(OrderInfo orderInfo) {
        assertFalse(orderInfo.getOrderObjectsInfo().isEmpty());
        return orderInfo.getOrderObjectsInfo().get(0);
    }

    private void checkOrderTransactions(Order order) {
        final boolean isPending = EnumSet.of(PaymentStatus.PENDING, PaymentStatus.RESERVED, PaymentStatus.FAILED, PaymentStatus.OVERDUE).contains(order.getPaymentStatus());
        final boolean isCancelled = (order.getPaymentStatus() == PaymentStatus.CANCELLED);

        checkTransactionalObject(order, isPending, isCancelled);
        for (OrderObject orderObject : orderService.getOrderObjects(order.getOrderId())) {
            checkTransactionalObject(orderObject, isPending, isCancelled);
        }
    }

    private static void checkTransactionalObject(TransactionalObject transactionalObject, boolean isPending, boolean isCancelled) {
        if (isPending) {
            assertTrue(transactionalObject.getCreditTransactionId() == 0);
            assertTrue(transactionalObject.getDebitTransactionId() == 0);
        } else {
            assertTrue(transactionalObject.getCreditTransactionId() != 0);
            assertTrue(transactionalObject.getDebitTransactionId() != 0);
        }

        if (isCancelled) {
            assertTrue(transactionalObject.getCancelCreditTransactionId() != 0);
            assertTrue(transactionalObject.getCancelDebitTransactionId() != 0);
        } else {
            assertTrue(transactionalObject.getCancelCreditTransactionId() == 0);
            assertTrue(transactionalObject.getCancelDebitTransactionId() == 0);
        }
    }

    private Order purchaseShare(long clientId, long buyerId, long shareId, BigDecimal donateAmount, int count, String buyerAnswerToMerchantQuestion) throws NotFoundException, PermissionException, OrderException {
        Order result = orderService.createOrderWithShare(clientId, buyerId, shareId, donateAmount, count, buyerAnswerToMerchantQuestion, null, null, null);
        orderService.purchase(result);

        return result;
    }

    /**
     * Run test manually only. Test persists test data.<br>
     * Test checks transaction rollback after purchasing error.<br>
     * For success finish you must disable {@link TransactionScope} test surrounding (emulates system behavior at controller level).<br>
     *
     * @throws NotFoundException
     * @throws PermissionException
     * @throws OrderException
     * @throws PaymentException
     */
    @Ignore
    @Test
    public void testPurchasingErrorRollback() throws NotFoundException, PermissionException, OrderException, PaymentException {
        final int SHARE_COUNT = 1;

        final long adminId = getProfileId(registerPlanetaAdminUser());
        final long merchantGroupId = registerNewOfficialGroup(adminId);
        final long successBuyerId = getProfileId(registerUser());
        final long failBuyerId = getProfileId(registerUser());

        final Share share = createShareWithCampaign(adminId, merchantGroupId, BigDecimal.TEN, SHARE_COUNT);

        // creates two orders for the last share
        Order successOrder = orderService.createOrderWithShare(successBuyerId, successBuyerId, share.getShareId(), null, SHARE_COUNT, getTestStr("answer"), null, null, null);
        Order failOrder = orderService.createOrderWithShare(failBuyerId, failBuyerId, share.getShareId(), null, SHARE_COUNT, getTestStr("answer"), null, null, null);

        // first order purchase must be successfully
        TopayTransaction payment = paymentService.createOrderPayment(successOrder, successOrder.getTotalPrice(), 1, ProjectType.WIDGETS, null, true);
//        assertTrue(paymentProcessingService.tryProcessPayment(payment.getTransactionId(), payment.getPaymentSystemType(), getTestStr("extSysData")));

        payment = paymentService.getPayment(payment.getTransactionId());
        assertTrue(payment.getStatus().isDone());

        successOrder = orderService.getOrderSafe(successOrder.getOrderId());
        assertOrderPayed(successOrder);
        checkOrderTransactions(successOrder);
        assertBalanceEquals(successBuyerId, BigDecimal.ZERO);
        assertBalanceEquals(merchantGroupId, successOrder.getTotalPrice());

        final BigDecimal merchantBalance = getProfileBalance(merchantGroupId);

        // second order purchase must be failure, but payment amount must be stay at buyer balance
        payment = paymentService.createOrderPayment(failOrder, failOrder.getTotalPrice(), 1, ProjectType.WIDGETS, null, true);
//        assertTrue(paymentProcessingService.tryProcessPayment(payment.getTransactionId(), payment.getPaymentSystemType(), getTestStr("extSysData")));

        payment = paymentService.getPayment(payment.getTransactionId());
        assertTrue(payment.getStatus().isDone());

        failOrder = orderService.getOrderSafe(failOrder.getOrderId());
        assertOrderFailed(failOrder);
        checkOrderTransactions(failOrder);
        assertBalanceEquals(failBuyerId, failOrder.getTotalPrice());
        assertBalanceEquals(merchantGroupId, merchantBalance);
    }



    @Autowired
    private OrderDAO orderDAO;

    @Test
    @Ignore
    public void testShareOrderDeliveryPayment() throws NotFoundException, PermissionException, OrderException, PaymentException {
        final long adminId = getProfileId(registerPlanetaAdminUser());
        final long merchantGroupId = registerNewOfficialGroup(adminId);
        final long buyerId = getProfileId(registerUser());

        final BigDecimal SHARE_PRICE = BigDecimal.TEN;
        final BigDecimal SHARE_DELIVERY_PRICE = BigDecimal.TEN;

        Share share = createShareWithCampaign(adminId, merchantGroupId, SHARE_PRICE, 1);
        Order order = orderService.createOrderWithShare(buyerId, buyerId, share.getShareId(), null, 1, "", null, null, null);
        updateDeliveryPrice(order, SHARE_DELIVERY_PRICE);

        TopayTransaction payment = paymentService.createOrderPayment(order, order.getTotalPrice(), 1, ProjectType.MAIN, null, true);

        paymentService.processPayment(payment, null);
        assertBalanceEquals(merchantGroupId, SHARE_PRICE.add(SHARE_DELIVERY_PRICE));
    }

    private void updateDeliveryPrice(Order order, BigDecimal deliveryPrice) {
        BigDecimal totalPrice = order.getTotalPrice().subtract(order.getDeliveryPrice()).add(deliveryPrice);

        order.setDeliveryPrice(deliveryPrice);
        order.setTotalPrice(totalPrice);
        orderDAO.update(order);
    }


    @Test
    @Ignore
    public void testVityaAccount() throws Exception {
        long vityaId = 123501;
        orderService.getProfilePurchaseOrders(vityaId, "", vityaId, null, 0, 20);
    }
}

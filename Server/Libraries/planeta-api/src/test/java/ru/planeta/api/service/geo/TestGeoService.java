package ru.planeta.api.service.geo;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import ru.planeta.model.profile.location.City;
import ru.planeta.model.profile.location.Country;
import ru.planeta.model.profile.location.GeoConstants;
import ru.planeta.test.AbstractTest;

import java.util.Collection;
import java.util.List;

import static org.junit.Assert.*;

/**
 * User: eshevchenko
 * Date: 13.06.12
 * Time: 10:17
 */
public class TestGeoService extends AbstractTest {

	@Autowired
    private GeoService geoService;

    @Test
    public void testCountry() {
        Country countryById = geoService.getCountryById(GeoConstants.INSTANCE.getRUSSIA());
        assertNotNull(countryById);
        Country countryByName = geoService.getCountryByName(countryById.getCountryNameRus());
        assertNotNull(countryByName);
        assertEquals(countryById, countryByName);

        checkGeoList(geoService.getCountries(), GeoConstants.INSTANCE.getTOTAL_COUNTRIES_COUNT());
    }

    @Test
    public void testCity() {
        City city = geoService.getCityById(GeoConstants.INSTANCE.getMOSCOW());
        assertNotNull(city);
        checkCitiesBySubstring(city.getNameRus());
    }

    private void checkCitiesBySubstring(String name) {
        String subName = name.substring(0, name.length() / 2);
        List<City> cities = geoService.getCountryCitiesBySubstring(GeoConstants.INSTANCE.getRUSSIA(), subName, 0, 100);
        checkGeoList(cities, 4);

        City firstCity = cities.get(0);
        City lastCity = cities.get(cities.size() - 1);
        assertTrue(firstCity.getNameRus().compareToIgnoreCase(lastCity.getNameRus()) < 0);

        checkGeoList(geoService.getCountryCitiesBySubstring(GeoConstants.INSTANCE.getRUSSIA(), "", 0, 10));
    }

    private static void checkGeoList(Collection<?> geoList) {
        assertNotNull(geoList);
        assertFalse(geoList.isEmpty());
        assertNotNull(geoList.iterator().next());
    }

    private static void checkGeoList(Collection<?> geoList, long expectedSize) {
        checkGeoList(geoList);
        assertEquals(expectedSize, geoList.size());
    }
}

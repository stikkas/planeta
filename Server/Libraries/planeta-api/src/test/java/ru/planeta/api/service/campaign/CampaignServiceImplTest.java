/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.planeta.api.service.campaign;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import ru.planeta.api.exceptions.NotFoundException;
import ru.planeta.model.common.campaign.Share;
import ru.planeta.model.common.campaign.Campaign;
import ru.planeta.test.AbstractTest;

/**
 * @author Serge Blagodatskih<stikkas17@gmail.com>
 */
public class CampaignServiceImplTest extends AbstractTest {

    private long wrongId = -172;

    @Autowired
    private CampaignService campaignService;

    @Test
    public void testGetCampaignSafeThrowNotFoundException() throws Exception {
        try {
            campaignService.getCampaignSafe(wrongId);
            fail("Expected an NotFoundException to be thrown");
        } catch (NotFoundException ex) {
            assertThat(ex.getMessage(), is(String.format("%s with id %d not found.", Campaign.class.getSimpleName(), wrongId)));
        }
    }

    @Test
    public void testGetEditableCampaignSafeThrowNotFoundException() throws Exception {
        try {
            campaignService.getEditableCampaignSafe(0, wrongId);
            fail("Expected an NotFoundException to be thrown");
        } catch (NotFoundException ex) {
            assertThat(ex.getMessage(), is(String.format("%s with id %d not found.", Campaign.class.getSimpleName(), wrongId)));
        }
    }

    @Test
    public void testGetShareSafeThrowNotFoundException() throws Exception {
        try {
            campaignService.getShareSafe(wrongId);
            fail("Expected an NotFoundException to be thrown");
        } catch (NotFoundException ex) {
            assertThat(ex.getMessage(), is(String.format("%s with id %d not found.", Share.class.getSimpleName(), wrongId)));
        }

    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.planeta.api.service.billing.order;

import org.apache.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import ru.planeta.api.exceptions.NotFoundException;
import ru.planeta.api.service.profile.ProfileBalanceService;
import ru.planeta.dao.commondb.OrderDAO;
import ru.planeta.dao.commondb.CampaignDAO;
import ru.planeta.dao.commondb.ShareDAO;
import ru.planeta.model.common.Order;
import ru.planeta.model.common.campaign.Campaign;
import ru.planeta.model.common.campaign.Share;
import ru.planeta.model.shop.enums.PaymentStatus;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

/**
 * @author Serge Blagodatskih<stikkas17@gmail.com>
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:spring/testApplicationContext.xml", "classpath:spring/applicationContext-*.xml", "classpath*:/spring/applicationContext-dao.xml"})
public class OrderServiceImplTest {

    @Autowired
    private OrderService orderService;

    @Autowired
    private BatchCancelerService batchCancelerService;

    @Autowired
    private ProfileBalanceService profileBalanceService;

    private static final Logger log = Logger.getLogger(OrderServiceImplTest.class);


    @Autowired
    private ShareDAO shareDAO;
    @Autowired
    private CampaignDAO campaignDAO;

    @Autowired
    private OrderDAO orderDAO;
    private static final long ORDER_ID = 666548;
    private static final long CAMPAIGN_ID = 60426;

    private class ShareCancelThread extends Thread {
        private long orderId;
        private int result = 0;

        public void setOrderId(long orderId) {
            this.orderId = orderId;
        }

        @Override
        public void run() {
            try {
                orderService.cancelSharePurchase(191, orderId, "", false);
                result = 1;
            } catch (Exception e) {
                log.warn(e);
            }
        }

        public int getResult() {
            return result;
        }
    }

    private class ShareCancelButchThread extends Thread {
        private long campaignId;
        private int result = 0;

        public void setCampaignId(long campaignId) {
            this.campaignId = campaignId;
        }

        @Override
        public void run() {
            try {
                result = batchCancelerService.batchCancelCampaignOrders(191, campaignId, "eewwe");
            } catch (Exception e) {
                log.warn(e);
            }
        }

        public int getResult() {
            return result;
        }
    }


    @Test
    public void testCancelSharePurchases() throws InterruptedException, NotFoundException {
        before();
        List<ShareCancelThread> pool = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            ShareCancelThread thread = new ShareCancelThread();
            thread.setOrderId(ORDER_ID);
            pool.add(thread);
        }


        System.out.println("Test started");

        for (ShareCancelThread thread : pool) {
            thread.start();
        }

        int result = 0;
        for (ShareCancelThread thread : pool) {
            try {
                thread.join();
                result += thread.getResult();
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        }
        // assertEquals(1, result);
        assertTrue(result <= 1);
    }

    private void before() {
        final long merchantId = 656022;
        final long shareId = 230884;
        Order order = orderDAO.select(ORDER_ID);
        assertNotNull(order);
        order.setPaymentStatus(PaymentStatus.COMPLETED);
        order.setCancelCreditTransactionId(0);
        order.setCancelDebitTransactionId(0);
        orderDAO.update(order);

        Share share = shareDAO.select(shareId);
        share.setPurchaseCount(1);
        share.setPurchaseSum(share.getPrice());
        shareDAO.update(share);

        Campaign campaign = campaignDAO.selectCampaignById(CAMPAIGN_ID);
        campaign.setPurchaseCount(1);
        campaign.setPurchaseSum(share.getPrice());
        campaignDAO.update(campaign);

        profileBalanceService.increaseProfileBalance(merchantId, new BigDecimal(10000), BigDecimal.ZERO, "");
    }

    @Test
    public void testCancelButchSharePurchases() throws InterruptedException, NotFoundException {
        before();
        Campaign c = orderService.getCampaignByOrderId(ORDER_ID);
        List<ShareCancelButchThread> pool = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            ShareCancelButchThread thread = new ShareCancelButchThread();
            thread.setCampaignId(CAMPAIGN_ID);
            pool.add(thread);
        }


        System.out.println("Test started");

        for (ShareCancelButchThread thread : pool) {
            thread.start();
        }

        int result = 0;
        for (ShareCancelButchThread thread : pool) {
            try {
                thread.join();
                result += thread.getResult();
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        }
        assertTrue(result <= 1);
    }

    @Test
    public void compareTest() {
        assertFalse(BigDecimal.ZERO.compareTo(BigDecimal.TEN) > 0);
        assertFalse(BigDecimal.ZERO.compareTo(BigDecimal.ZERO) > 0);
        assertTrue(BigDecimal.ZERO.compareTo(BigDecimal.TEN.negate()) > 0);

    }


}

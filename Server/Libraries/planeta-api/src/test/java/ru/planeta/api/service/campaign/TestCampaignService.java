package ru.planeta.api.service.campaign;

import org.apache.commons.lang3.time.DateUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import ru.planeta.api.exceptions.NotFoundException;
import ru.planeta.api.exceptions.OrderException;
import ru.planeta.api.exceptions.PermissionException;
import ru.planeta.api.log.service.DBLogServiceImpl;
import ru.planeta.api.mail.MailClient;
import ru.planeta.api.service.billing.order.OrderService;
import ru.planeta.dao.commondb.CampaignDAO;
import ru.planeta.model.common.Contractor;
import ru.planeta.model.common.Order;
import ru.planeta.model.common.UserPrivateInfo;
import ru.planeta.model.common.campaign.*;
import ru.planeta.model.common.campaign.enums.CampaignStatus;
import ru.planeta.model.common.campaign.enums.CampaignTargetStatus;
import ru.planeta.model.profile.Profile;
import ru.planeta.model.profile.media.Photo;
import ru.planeta.test.AbstractTest;

import javax.naming.directory.InvalidAttributesException;
import java.math.BigDecimal;
import java.util.*;

import static org.junit.Assert.*;

/**
 * User: atropnikov
 * Date: 26.03.12
 * Time: 13:07
 */
public class TestCampaignService extends AbstractTest {
    @MockBean
    private DBLogServiceImpl dbLogService;
    @Autowired
    private CampaignService campaignService;
    @Autowired
    private CampaignDAO campaignDAO;
    @Autowired
    private OrderService orderService;
    @Autowired
    private ContractorService contractorService;

    @Autowired
    private MailClient mailClient;

    @Autowired
    public void setMailClient(MailClient mailClient) {
        this.mailClient = mailClient;
        this.mailClient.setAsynchronous(true);
    }

    @Test
    public void testSelectPurchasedCampaigns() throws Exception {
        UserPrivateInfo user = insertPlanetaAdminPrivateInfo();
        long clientId = user.getUserId();
        long profileId = registerSimpleGroup(clientId);
        setGroupIsMerchant(profileId);

        Campaign campaign = insertCampaign(clientId, profileId);
        Share share = campaignService.saveShare(clientId, createShare(profileId, campaign.getCampaignId()));

        increaseProfileBalance(clientId, new BigDecimal(20));

        campaign.setDescriptionHtml("<p:photo url=\"http://www.some-url.ru\"/> - Это \"Типограф\"?\n" +
                " — Нет, это «Типограф»!");
        campaign.setShortDescription("Это \"Типограф\"?\n — Нет, это «Типограф»!");

        ArrayList<CampaignTag> tags = new ArrayList<CampaignTag>();
        tags.add(campaignService.getAllTags().get(0));
        campaign.setTags(tags);

        updateCampaign(clientId, campaign);

        startCampaign(clientId, campaign.getCampaignId());
    }

    @Test
    public void testInsertUpdate() throws PermissionException, NotFoundException {
        long clientId = insertPlanetaAdminPrivateInfo().getUserId();
        long groupId = registerSimpleGroup(clientId);
        setGroupIsMerchant(groupId);

        Campaign campaign = insertCampaign(clientId, groupId);

        List<Campaign> campaigns = campaignService.getCampaignsByStatus(groupId, null, 0, 10);
        assertEquals(campaigns.size(), 1);
        assertTrue(campaigns.get(0).getCampaignId() > 0);
        assertTrue(campaigns.get(0).getAlbumId() > 0);

        campaigns = campaignService.getCampaignsByStatus(-1, EnumSet.of(CampaignStatus.DRAFT), 0, 10);
        assertTrue(!campaigns.isEmpty());

        // test auto creating donate share
        campaign = campaignService.getCampaignWithShares(clientId, campaign.getCampaignId(), false);
        assertNotNull(campaign.getShares());
        assertEquals(1, campaign.getShares().size());
        Share share = campaign.getShares().get(0);
        assertEquals(0, share.getPrice().compareTo(BigDecimal.ZERO));
        assertEquals(0, share.getAmount());
    }

    @Test
    public void testBackersCount() throws PermissionException, NotFoundException {
        int count = campaignService.getCampaignBackersCount(304);
        System.out.print(count);
    }

    @Ignore
    @Test
    public void testSelectCampaignSharesOrerBy() throws PermissionException, NotFoundException {
        long campaignId = 43908;
        Campaign campaign = campaignDAO.selectWithShares(campaignId, true);
        System.out.println("DESC");
        for (Share share : campaign.getShares()) {
            System.out.println(share.getPrice() + " " + share.getOrder());
        }

        System.out.println();
        System.out.println("ASC");

        campaign = campaignDAO.selectWithShares(campaignId, false);
        for (Share share : campaign.getShares()) {
            System.out.println(share.getPrice() + " " + share.getOrder());
        }
    }

    // @Ignore
    @Test
    public void testSelectWithShares() throws PermissionException, NotFoundException, InvalidAttributesException {
        long clientId = insertPlanetaAdminPrivateInfo().getUserId();
        long groupId = registerSimpleGroup(clientId);
        setGroupIsMerchant(groupId);

        long campaignId = insertCampaign(clientId, groupId).getCampaignId();
        long shareId = campaignService.saveShare(clientId, createShare(groupId, campaignId)).getShareId();
        Share share = campaignService.getShare(shareId);

        Campaign campaign = campaignService.getCampaignWithShares(clientId, campaignId, false);
        campaign.addShare(new ShareEditDetails(share));
        campaign.setTags(Collections.singletonList(campaignService.getAllTags().get(0)));

        updateCampaign(clientId, campaign);

        campaign = campaignService.getCampaign(campaignId);
        assertNotNull(campaign);
        // assertNull(campaign.getShares());

        campaign = campaignService.getCampaignWithShares(clientId, campaignId, false);
        assertNotNull(campaign);
        assertNotNull(campaign.getShares());
        assertEquals(1, campaign.getShares().size());

        List<Campaign> campaigns = campaignService.getCampaignsByStatus(groupId, null, 0, 10);
        assertEquals(1, campaigns.size());
        // assertNull(campaigns.get(0).getShares());
    }

    @Test
    public void testStartCampaign() throws PermissionException, NotFoundException {
        long clientId = insertPlanetaAdminPrivateInfo().getUserId();
        long groupId = registerSimpleGroup(clientId);
        setGroupIsMerchant(groupId);

        Campaign campaign = insertCampaign(clientId, groupId);

        //can't start campaign with status doesn't equal to PAUSED
        try {
            startCampaign(clientId, campaign.getCampaignId());
            assertTrue(false);
        } catch (Exception ex) {
            assertTrue(true);
        }
        campaign.setStatus(CampaignStatus.PAUSED);
        campaignDAO.update(campaign);
        startCampaign(clientId, campaign.getCampaignId());

        campaign = campaignService.getCampaign(campaign.getCampaignId());
        assertNotNull(campaign);
        assertEquals(campaign.getStatus(), CampaignStatus.ACTIVE);
        assertNotNull(campaign.getTimeStart());
    }

    @Test
    public void testPauseCampaign() throws Exception {
        long clientId = insertPlanetaAdminPrivateInfo().getUserId();
        long groupId = registerSimpleGroup(clientId);
        setGroupIsMerchant(groupId);

        Campaign campaign = insertCampaign(clientId, groupId);

        //can't pause campaign with status doesn't equal to ACTIVE
        try {
            campaignService.pauseCampaign(clientId, campaign.getCampaignId());
            assertTrue(false);
        } catch (Exception ex) {
            assertTrue(true);
        }
        campaign.setStatus(CampaignStatus.ACTIVE);
        campaignDAO.update(campaign);

        campaignService.pauseCampaign(clientId, campaign.getCampaignId());

        campaign = campaignService.getCampaign(campaign.getCampaignId());
        assertNotNull(campaign);
        assertEquals(campaign.getStatus(), CampaignStatus.PAUSED);
    }


    @Test
    @Ignore
    public void testShare() throws NotFoundException, PermissionException, InvalidAttributesException {
        long clientId = insertPlanetaAdminPrivateInfo().getUserId();
        long groupId = registerSimpleGroup(clientId);
        setGroupIsMerchant(groupId);

        Campaign campaign = insertCampaign(clientId, groupId);

        Share share = createShare(groupId, campaign.getCampaignId());
        campaignService.saveShare(clientId, share);
        assertTrue(share.getShareId() > 0);

        List<Share> shares = ((CampaignServiceImpl) campaignService).getCampaignShares(campaign.getCampaignId());
        assertEquals(2, shares.size());
        assertShareEquals(shares.get(1), share);

        share.setName("new name");
        share.setDescription("new desc");
        share.setImageId(1000L);
        share.setImageUrl("new img url");
        share.setPrice(BigDecimal.ONE);
        share.setAmount(1000);
        campaignService.saveShare(clientId, share);

        campaign = campaignService.getCampaignSafe(campaign.getCampaignId());
        shares = ((CampaignServiceImpl) campaignService).getCampaignShares(campaign.getCampaignId());
        for (Share share1 : shares) {
            campaign.addShare(new ShareEditDetails(share1));
        }
        CampaignTag charityTag = campaignService.getCampaignTagByMnemonic(CampaignTag.Companion.getCHARITY());
        campaign.addTag(charityTag);


        updateCampaign(clientId, campaign);

        shares = campaignService.getCampaignSharesFiltered(campaign.getCampaignId());
        assertEquals(1, shares.size()); // one share with zero amount
        assertShareEquals(shares.get(0), share);

    }

    @Test
    public void testRemoveCampaign() throws PermissionException, NotFoundException {
        long clientId = insertPlanetaAdminPrivateInfo().getUserId();
        long groupId = registerSimpleGroup(clientId);
        setGroupIsMerchant(groupId);

        long campaignId = insertCampaign(clientId, groupId).getCampaignId();
        long shareId = campaignService.saveShare(clientId, createShare(groupId, campaignId)).getShareId();

        // deleteByProfileId campaign with zero purchases
        Campaign campaign = campaignService.getCampaignSafe(campaignId);
        campaign.setStatus(CampaignStatus.ACTIVE);
        campaignDAO.update(campaign);
        campaignService.removeCampaign(clientId, campaignId);

        assertNull(campaignService.getCampaign(campaignId));
//        assertTrue(campaignService.getCampaignSharesFiltered(campaignId).isEmpty());
    }

    @Test
    public void testCampaignStatusChanging() throws Exception {
        final BigDecimal SHARE_PRICE = BigDecimal.ONE;
        final int SHARE_COUNT = 3;
        final BigDecimal TARGET_AMOUNT = SHARE_PRICE.multiply(new BigDecimal(SHARE_COUNT));
        final BigDecimal BUYER_BALANCE = new BigDecimal(20);

        final long adminId = insertPlanetaAdminPrivateInfo().getUserId();
        final long merchantId = registerUser().getProfile().getProfileId();
        final long merchantGroupId = registerSimpleGroup(merchantId);
        setGroupIsMerchant(merchantGroupId);
        increaseProfileBalance(adminId, BUYER_BALANCE);

        Campaign campaign = new Campaign(insertCampaign(merchantId, merchantGroupId));

        campaign.setTimeFinish(null);
        campaign.setTargetAmount(TARGET_AMOUNT);

        ArrayList<CampaignTag> tags = new ArrayList<>();
        tags.add(campaignService.getAllTags().get(0));
        campaign.setTags(tags);


        updateCampaign(adminId, campaign);

        final long campaignId = campaign.getCampaignId();

        Share share = createShare(merchantGroupId, campaignId);
        share.setPrice(SHARE_PRICE);
        share.setAmount(SHARE_COUNT);
        campaignService.saveShare(merchantId, share);

        campaign = new Campaign(campaignService.getCampaign(campaignId));

        assertCampaignInStatuses(campaignId, CampaignStatus.NOT_STARTED, CampaignTargetStatus.NONE);
        campaign = new Campaign(campaignService.getCampaign(campaignId));
        startCampaign(adminId, campaign.getCampaignId());
        assertCampaignInStatuses(campaignId, CampaignStatus.ACTIVE, CampaignTargetStatus.NONE);


        Order order = purchaseShare(adminId, adminId, share.getShareId(), null, 1, null);
        assertCampaignInStatuses(campaignId, CampaignStatus.ACTIVE, CampaignTargetStatus.NONE);

        Order donateOrder = purchaseShare(adminId, adminId, share.getShareId(), share.getPrice().add(BigDecimal.ONE), 1, null);
        assertCampaignInStatuses(campaignId, CampaignStatus.ACTIVE, CampaignTargetStatus.SUCCESS);

        orderService.cancelSharePurchase(adminId, donateOrder.getOrderId(), rndStr(), false);
        assertCampaignInStatuses(campaignId, CampaignStatus.ACTIVE, CampaignTargetStatus.NONE);

        try {
            startCampaign(merchantId, campaign.getCampaignId());
            fail();
        } catch (NotFoundException e) {
            fail();
        } catch (PermissionException ignored) {

        }

        assertCampaignInStatuses(campaignId, CampaignStatus.ACTIVE, CampaignTargetStatus.NONE);

        startCampaign(adminId, campaign.getCampaignId());
        assertCampaignInStatuses(campaignId, CampaignStatus.ACTIVE, CampaignTargetStatus.NONE);

        // isFinishOnTargetReach

        updateCampaign(adminId, campaign);

        // !isFinishOnTargetReach && isIgnoreTargetAmount
        campaign.setTargetAmount(null);
        campaign.setTimeFinish(DateUtils.addDays(new Date(), 1));

        updateCampaign(adminId, campaign);
        startCampaign(adminId, campaign.getCampaignId());
        assertCampaignInStatuses(campaignId, CampaignStatus.ACTIVE, CampaignTargetStatus.NONE);

        order = purchaseShare(adminId, adminId, share.getShareId(), null, 1, null);
        assertCampaignInStatuses(campaignId, CampaignStatus.ACTIVE, CampaignTargetStatus.SUCCESS);

        campaign.setTimeFinish(DateUtils.addDays(new Date(), -1));
        updateCampaign(adminId, campaign);
        assertCampaignInStatuses(campaignId, CampaignStatus.ACTIVE, CampaignTargetStatus.SUCCESS);

        // 1 rub
        campaign.setTargetAmount(TARGET_AMOUNT);
        campaign.setTimeFinish(DateUtils.addDays(new Date(), 1));
        updateCampaign(adminId, campaign);
        assertCampaignInStatuses(campaignId, CampaignStatus.ACTIVE, CampaignTargetStatus.SUCCESS);

        campaign.setTimeFinish(DateUtils.addDays(new Date(), 1));
        updateCampaign(adminId, campaign);
        assertCampaignInStatuses(campaignId, CampaignStatus.ACTIVE, CampaignTargetStatus.SUCCESS);

        campaign.setTimeFinish(DateUtils.addDays(new Date(), -1));
        updateCampaign(adminId, campaign);
    }

    private Order purchaseShare(long clientId, long buyerId, long shareId, BigDecimal donateAmount, int count, String buyerAnswerToMerchantQuestion) 
            throws NotFoundException, PermissionException, OrderException {
        Order result = orderService.createOrderWithShare(clientId, buyerId, shareId, donateAmount, count, buyerAnswerToMerchantQuestion, null, null, null);
        orderService.purchase(result);
        return result;
    }

    private void assertCampaignInStatuses(long campaignId, CampaignStatus status, CampaignTargetStatus targetStatus) {
        Campaign campaign = campaignService.getCampaign(campaignId);
        assertNotNull(campaign);
        assertEquals(status, campaign.getStatus());
        assertEquals(targetStatus, campaign.getTargetStatus());
    }

    @Test
    public void testTotalStats() {
        BigDecimal result = campaignService.getTotalCollectedAmount();
        assertNotNull(result);
    }


    @Test
    public void testCampaignContacts() throws NotFoundException, PermissionException {
        UserPrivateInfo clientUserInfo = insertPlanetaAdminPrivateInfo();
        long clientId = clientUserInfo.getUserId();
        long groupId = registerSimpleGroup(clientId);
        setGroupIsMerchant(groupId);

        Campaign campaign = insertCampaign(clientId, groupId);
        Profile contactProfile = insertUserProfile();
        String contactEmail = "testmail123@test.ee";
        String contactEmail2 = "testmail1234@test.ee";

        CampaignContact campaignContact = new CampaignContact();
        campaignContact.setCampaignId(campaign.getCampaignId());
        campaignContact.setContactProfileId(contactProfile.getProfileId());
        campaignContact.setContactName(contactProfile.getDisplayName());
        campaignContact.setEmail(contactEmail);
        campaignService.saveCampaignContact(clientId, campaignContact);

        List<CampaignContact> list = campaignService.getCampaignContacts(clientId, campaign.getCampaignId(), 0, 0);
        assertNotNull(list);
        assertEquals(2, list.size());
        //Default contact for all campaigns
        assertCampaignContactsEquals(list.get(0), clientUserInfo.getEmail(), clientId, clientUserInfo.getUsername(), campaign.getCampaignId());
        assertCampaignContactsEquals(list.get(1), contactEmail, contactProfile.getProfileId(), contactProfile.getDisplayName(), campaign.getCampaignId());

        CampaignContact campaignContact2 = new CampaignContact();
        campaignContact2.setCampaignId(campaign.getCampaignId());
        campaignContact2.setEmail(contactEmail2);
        campaignService.saveCampaignContact(clientId, campaignContact2);

        list = campaignService.getCampaignContacts(clientId, campaign.getCampaignId(), 0, 0);
        assertNotNull(list);
        assertEquals(3, list.size());

        assertCampaignContactsEquals(list.get(0), clientUserInfo.getEmail(), clientId, clientUserInfo.getUsername(), campaign.getCampaignId());
        assertCampaignContactsEquals(list.get(1), contactEmail2, null, null, campaign.getCampaignId());
        assertCampaignContactsEquals(list.get(2), contactEmail, contactProfile.getProfileId(), contactProfile.getDisplayName(), campaign.getCampaignId());

        campaignService.deleteCampaignContact(clientId, campaign.getCampaignId(), contactEmail);

        list = campaignService.getCampaignContacts(clientId, campaign.getCampaignId(), 0, 0);
        assertNotNull(list);
        assertEquals(2, list.size());

        assertCampaignContactsEquals(list.get(0), clientUserInfo.getEmail(), clientId, clientUserInfo.getUsername(), campaign.getCampaignId());
        assertCampaignContactsEquals(list.get(1), contactEmail2, null, null, campaign.getCampaignId());
    }

    @Test
    public void testInsertUpdateTags() throws Exception {
        long clientId = insertPlanetaAdminPrivateInfo().getUserId();
        long groupId = registerSimpleGroup(clientId);
        setGroupIsMerchant(groupId);

        Campaign campaign = insertCampaign(clientId, groupId);
        assertEquals(0, campaign.getTags().size());
        List<Campaign> campaigns = campaignService.getCampaignsByStatus(groupId, null, 0, 10);
        assertEquals(0, campaigns.get(0).getTags().size());

        ArrayList<CampaignTag> tags = new ArrayList<CampaignTag>();
        tags.add(campaignService.getAllTags().get(0));
        campaign.setTags(tags);
        updateCampaign(clientId, campaign);
        campaigns = campaignService.getCampaignsByStatus(groupId, null, 0, 10);
        assertEquals(1, campaigns.get(0).getTags().size());
        campaign = campaignService.getCampaignSafe(campaign.getCampaignId());
        assertEquals(1, campaign.getTags().size());

    }

    private void assertCampaignContactsEquals(CampaignContact campaignContact, String contactEmail, Long profileId, String displayName, long campaignId) {
        assertEquals(campaignId, campaignContact.getCampaignId());
        assertEquals(contactEmail, campaignContact.getEmail());
        if (profileId != null) {
            assertEquals(profileId.longValue(), campaignContact.getContactProfileId());
        }
        if (displayName != null) {
            assertEquals(displayName, campaignContact.getContactName());
        }
    }

    private Share createShare(long groupId, long campaignId) {
        Share share = new Share();
        share.setProfileId(groupId);
        share.setCampaignId(campaignId);
        share.setName("name");
        share.setDescription("desc");
        share.setImageId(100L);
        share.setImageUrl("img url");
        share.setPrice(BigDecimal.TEN);
        share.setAmount(10);
        share.setTimeAdded(new Date());

        return share;
    }

    private void assertShareEquals(Share expected, Share actual) {
        assertNotNull(expected);
        assertEquals(expected.getShareId(), actual.getShareId());
        assertEquals(expected.getCampaignId(), actual.getCampaignId());
        assertEquals(expected.getProfileId(), actual.getProfileId());
        assertEquals(expected.getName(), actual.getName());
        assertEquals(expected.getDescription(), actual.getDescription());
        assertEquals(expected.getImageId(), actual.getImageId());
        assertEquals(expected.getImageUrl(), actual.getImageUrl());
        assertEquals(expected.getPrice().compareTo(actual.getPrice()), 0);
        assertEquals(expected.getAmount(), actual.getAmount());
        assertEquals(expected.getTimeAdded(), actual.getTimeAdded());
    }

    private void startCampaign(long clientId, long campaignId) throws NotFoundException, PermissionException {
        Campaign campaign = campaignService.getCampaignSafe(campaignId);
        Contractor contractor = new Contractor();
        contractor.setName("Vasya" + campaign.getCampaignId());
        contractor.setPassportNumber("1234567890");
        Contractor selected = contractorService.select(contractor.getName());
        if (selected == null) {
            contractorService.add(contractor, 11111, 34205L);
        } else {
            contractor.setContractorId(selected.getContractorId());
        }
        contractorService.insertRelation(contractor.getContractorId(), campaign.getCampaignId());
        campaignService.startCampaign(clientId, campaign.getCampaignId());
    }

    @Test
    public void testDescriptionHtmlTransform() throws Exception {
        String photoChunk = "<p:photo id=\"258291\" owner=\"116589\" image=\"http://s1.dev.planeta.ru/i/3f0f3/1392026799539_renamed.jpg\" width=\"298\" clazz=\"mce-img-right\">";
        String expected = "<p:photo id=\"666\" owner=\"777\" image=\"http://s1.dev.planeta.ru/i/3f0f3/1392026799539_renamed.jpg\" width=\"298\" clazz=\"mce-img-right\">";
        Photo photo = new Photo();
        photo.setPhotoId(666);
        photo.setProfileId(777L);
//        String replacement = CampaignServiceImpl.Companion.replacePhotoHtml(photoChunk, photo);
//        assertEquals(expected, replacement);

        String html = "<ul><li><p:photo id=\"258291\" owner=\"116589\" image=\"http://s1.dev.planeta.ru/i/3f0f3/1392026799539_renamed.jpg\" width=\"298\" clazz=\"mce-img-right\"></p:photo>\n" +
                "<h1><em>asdfasdf<span class=\"mce-strikethrough\">lkjhk</span></em></h1>\n" +
                "</li><li>\n" +
                "<h1>asdfasdf</h1>\n" +
                "</li><li>\n" +
                "<h1>asdfasdf</h1>\n" +
                "</li></ul>\n" +
                "<div>asdf</div>\n" +
                "<p>&nbsp;</p>\n" +
                "<p>&nbsp;</p>\n" +
                "<p><p:audio duration=\"313\" name=\"Невский Проспект\" owner=\"23709\" artist=\"Сплин\" id=\"13220\"></p:audio></p>\n" +
                "<p><p:photo id=\"260592\" owner=\"23709\" image=\"http://s1.dev.planeta.ru/i/3f9f0/1395060997517_renamed.jpg\"></p:photo></p>\n";

        Campaign campaign = new Campaign();
        campaign.setProfileId(777L);
        campaign.setCampaignId(555);
        campaign.setDescriptionHtml(html);
        String transformedDescriptionHtml = ((CampaignServiceImpl) campaignService).getTransformedDescriptionHtml(campaign);
        assertTrue(transformedDescriptionHtml.contains("owner=\"777\" image=\"http://s1.dev.planeta.ru/i/3f0f3/1392026799539_renamed.jpg\""));
        assertTrue(transformedDescriptionHtml.contains("owner=\"777\" image=\"http://s1.dev.planeta.ru/i/3f9f0/1395060997517_renamed.jpg\""));

    }

    @Test
    public void testSelectCampaignsWithTags() throws Exception {
        UserPrivateInfo clientUserInfo = insertPlanetaAdminPrivateInfo();
        long profileId = clientUserInfo.getUserId();
        long groupId = profileService.getOrCreateHiddenGroup(profileId).getProfileId();

        Campaign campaign = insertCampaign(profileId, groupId);
        Share share = campaignService.saveShare(profileId, createShare(profileId, campaign.getCampaignId()));

        campaign.setTimeFinish(null);
        campaign.setTargetAmount(BigDecimal.TEN);
        campaign.setName("ruyqtwreyuqtwet");
        campaign.setDescriptionHtml("<p:photo url=\"http://www.some-url.ru\"/> - Это \"Типограф\"?\n" +
                " — Нет, это «Типограф»!");
        campaign.setShortDescription("Это \"Типограф\"?\n — Нет, это «Типограф»!");

        ArrayList<CampaignTag> tags = new ArrayList<CampaignTag>();
        tags.addAll(campaignService.getAllTags());
        campaign.setTags(tags);

        updateCampaign(profileId, campaign);

        List<Campaign> list = campaignService.getCampaignsByStatus(groupId, null, 0, 1);
        assertNotNull(list);
        assertEquals(1, list.size());
        assertEquals(campaignService.getAllTags().size(), list.get(0).getTags().size());

        list = campaignService.getCampaignsByStatus(groupId, null, 1, 1);
        assertNotNull(list);
        assertEquals(0, list.size());

    }

    @Test
    public void testSponsorAddRemove() {
        Campaign campaign = new Campaign();
        campaign.setTargetAmount(new BigDecimal(200000));
        campaign.setCollectedAmount(BigDecimal.ZERO);
        campaign.setPurchaseSum(new BigDecimal(103000));
        long sponsorMaxDonate = 100000L;
        int multiplier = 2;
//        CampaignServiceImpl.Companion.computeAmountWithSponsor(campaign, new BigDecimal(103000), sponsorMaxDonate, multiplier);

        assertEquals(203000, campaign.getCollectedAmount().intValue());

        campaign.setPurchaseSum(new BigDecimal(101000));
//        CampaignServiceImpl.Companion.computeAmountWithSponsor(campaign, new BigDecimal(-2000), sponsorMaxDonate, multiplier);

        assertEquals(201000, campaign.getCollectedAmount().intValue());

        campaign.setPurchaseSum(new BigDecimal(99000));
//        CampaignServiceImpl.Companion.computeAmountWithSponsor(campaign, new BigDecimal(-2000), sponsorMaxDonate, multiplier);
        assertEquals(99*multiplier*1000, campaign.getCollectedAmount().intValue());

    }

    @Test
    public void testGetCampaignsCountPercentagesByCustomTags() {
        List<Pair<String, Long>> map = campaignService.getCampaignsCountPercentagesByCustomTags();
        assertNotNull(map);
    }

    private void updateCampaign(long clientId, Campaign campaign) throws NotFoundException, PermissionException, InvalidAttributesException {
        campaignService.saveCampaign(clientId, campaign, false);
        campaignService.publishCampaign(clientId, campaign.getCampaignId());
    }
}

package ru.planeta.api.service.common;

import org.apache.commons.lang3.StringUtils;
import org.junit.After;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import ru.planeta.api.log.service.DBLogService;
import ru.planeta.api.model.json.DBLogRecordInfo;
import ru.planeta.dao.statdb.DBLogDAO;
import ru.planeta.model.stat.log.LoggerType;
import ru.planeta.model.stat.log.BaseDBLogRecord;
import ru.planeta.model.stat.log.HttpInteractionLogRecord;
import ru.planeta.dao.TransactionScope;
import ru.planeta.test.AbstractTest;

import java.util.List;

import static org.junit.Assert.*;
import static org.junit.Assert.assertNotEquals;

/**
 * Database logging service test.<br>
 * User: eshevchenko
 */
public class TestDBLogService extends AbstractTest {

    /**
     * Test identifiers must have minus value.
     */
    private static final long OBJECT_ID = -1L;
    private static final long SUBJECT_ID = -2L;

    @Autowired
    private DBLogDAO dbLogDAO;
    @Autowired
    private DBLogService dbLogService;

    /**
     * Clears persisted log data.<br>
     * Log is written directly to the database using JDBC, bypassing {@link TransactionScope}.
     */
    @After
    public void clearTestData() {
        dbLogDAO.deleteTestData();
    }

    @Test
    public void testDBLogging() {
        final long reqId = dbLogService.addRequestToLog(getTestStr("metaData"));
        final long respId = dbLogService.addResponseToLog(reqId, getTestStr("metaData"));
        dbLogService.addRecordToLog(getTestStr("message"), LoggerType.ORDER, OBJECT_ID, SUBJECT_ID, reqId, respId);

        List<DBLogRecordInfo> records = dbLogService.getLogRecords(LoggerType.ORDER, OBJECT_ID, SUBJECT_ID);

        assertNotNull(records);
        assertEquals(1, records.size());
        checkDBLogRecord(records.get(0));
    }

    @Test
    public void testDeferredRefInfoAttaching() {
        final long reqId = dbLogService.addRequestToLog(getTestStr("metaData"));
        final long respId = dbLogService.addResponseToLog(reqId, getTestStr("metaData"));
        final long recId = dbLogService.addRecordToLog(getTestStr("message"), LoggerType.PAYMENT);

        dbLogService.attachRefInfo(recId, OBJECT_ID, SUBJECT_ID, reqId, respId);

        List<DBLogRecordInfo> records = dbLogService.getLogRecords(LoggerType.PAYMENT, OBJECT_ID, SUBJECT_ID);

        assertNotNull(records);
        assertEquals(1, records.size());
        checkDBLogRecord(records.get(0));
    }

    private static void checkDBLogRecord(DBLogRecordInfo record) {
        checkBaseDBLogRecord(record);
        assertTrue(StringUtils.isNotBlank(record.getMessage()));
        assertEquals(OBJECT_ID, record.getObjectId());
        assertEquals(SUBJECT_ID, record.getSubjectId());

        checkHttpRequestLogRecord(record.getRequest());
        checkHttpResponseLogRecord(record.getResponse());

    }

    private static void checkHttpRequestLogRecord(HttpInteractionLogRecord request) {
        checkBaseDBLogRecord(request);
        assertTrue(StringUtils.isNotBlank(request.getMetaData()));
        assertTrue(request.isRequest());
        assertEquals(0L, request.getRequestId());
    }

    private static void checkHttpResponseLogRecord(HttpInteractionLogRecord response) {
        checkBaseDBLogRecord(response);
        assertTrue(StringUtils.isNotBlank(response.getMetaData()));
        assertFalse(response.isRequest());
        assertNotEquals(0L, response.getRequestId());
    }

    private static void checkBaseDBLogRecord(BaseDBLogRecord dbLogRecordInfo) {
        assertNotNull(dbLogRecordInfo);
        assertNotEquals(0L, dbLogRecordInfo.getId());
        assertNotEquals(0L, dbLogRecordInfo.getTimeAdded());
    }
}

package ru.planeta.api.service.common;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import ru.planeta.api.model.enums.image.ImageType;
import ru.planeta.api.service.configurations.StaticNodesService;
import ru.planeta.model.enums.ThumbnailType;
import ru.planeta.test.AbstractTest;
import ru.planeta.utils.ThumbConfiguration;

import java.util.List;

import static org.junit.Assert.*;

/**
 * Date: 09.07.12
 *
 * @author s.kalmykov
 */
public class TestStaticNodesService extends AbstractTest {

	@Autowired
	private StaticNodesService staticNodesService;
	public final String imageUrl = "http://freeimagesarchive.com/data/media/255/4_kaley+cuoco.jpg";

	@Test
	public void testStaticNodesStuff() {
		List<String> staticNodes = staticNodesService.getStaticNodes();
		assertNotNull(staticNodes);
		assertTrue(staticNodes.size() > 0);

		assertNotNull(staticNodesService.getStaticNode());
		assertNotNull(staticNodesService.getStaticNode());
		assertNotNull(staticNodesService.getResourceHost());
	}

	@Test
	public void testThumbnailsStuff() {
		assertNotNull(staticNodesService.getThumbnailUrl("", ThumbnailType.SMALL, ImageType.VIDEO));

		String thumbnailUrl = staticNodesService.getThumbnailUrl(imageUrl, ThumbnailType.SMALL, ImageType.PHOTO);
		assertNotNull(thumbnailUrl);

		String thumbnailUrl1 = staticNodesService.getThumbnailUrl(thumbnailUrl, ThumbnailType.MEDIUM, ImageType.USER);
		assertNotNull(thumbnailUrl1);
	}

    @Test
    public void testWrapImageUrl() throws Exception {
        String wrappedUrl = staticNodesService.wrapImageUrl(imageUrl, new ThumbConfiguration(ThumbnailType.ORIGINAL));
        // check urlencoding
        assertFalse(wrappedUrl.contains(imageUrl));
    }
}

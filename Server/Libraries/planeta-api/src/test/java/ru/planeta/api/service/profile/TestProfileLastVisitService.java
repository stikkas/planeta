package ru.planeta.api.service.profile;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import ru.planeta.model.enums.ProfileLastVisitType;
import ru.planeta.model.profiledb.ProfileLastVisit;
import ru.planeta.model.profile.Profile;
import ru.planeta.test.AbstractTest;

import java.util.Date;


import static org.junit.Assert.*;

public class TestProfileLastVisitService extends AbstractTest {
    @Autowired
    private ProfileLastVisitService profileLastVisitService;

    @Test
    public void testInsertOrUpdate() {
        Profile profile = insertUserProfile();
        ProfileLastVisit profileLastVisit = ProfileLastVisit.Companion.getBuilder()
                .profileId(profile.getProfileId())
                .profileLastVisitType(ProfileLastVisitType.COMMENTS_TAB)
                .objectId(0L)
                .build();

        Date dateBefore = new Date();
        profileLastVisitService.insertOrUpdateProfileLastVisit(profileLastVisit.getProfileId(), profileLastVisit.getProfileLastVisitType());

        Date visitDate = profileLastVisitService.selectProfileLastVisit(profileLastVisit.getProfileId(), profileLastVisit.getProfileLastVisitType());
        assertTrue(visitDate.compareTo(dateBefore) >= 0);
        assertTrue(visitDate.compareTo(new Date()) <= 0);


        dateBefore = new Date();
        profileLastVisitService.insertOrUpdateProfileLastVisit(profileLastVisit.getProfileId(), profileLastVisit.getProfileLastVisitType());
        visitDate = profileLastVisitService.selectProfileLastVisit(profileLastVisit.getProfileId(), profileLastVisit.getProfileLastVisitType());
        assertTrue(visitDate.compareTo(dateBefore) >= 0);
        assertTrue(visitDate.compareTo(new Date()) <= 0);


        profileLastVisit = ProfileLastVisit.Companion.getBuilder()
                .profileId(profile.getProfileId())
                .profileLastVisitType(ProfileLastVisitType.CAMPAIGN_PAGE)
                .objectId(1234L)
                .build();

        dateBefore = new Date();
        profileLastVisitService.insertOrUpdateProfileLastVisit(profileLastVisit.getProfileId(), profileLastVisit.getProfileLastVisitType(), profileLastVisit.getObjectId());
        visitDate = profileLastVisitService.selectProfileLastVisit(profileLastVisit.getProfileId(), profileLastVisit.getProfileLastVisitType(), profileLastVisit.getObjectId());
        assertTrue(visitDate.compareTo(dateBefore) >= 0);
        assertTrue(visitDate.compareTo(new Date()) <= 0);


        dateBefore = new Date();
        profileLastVisitService.insertOrUpdateProfileLastVisit(profileLastVisit.getProfileId(), profileLastVisit.getProfileLastVisitType(), profileLastVisit.getObjectId());
        visitDate = profileLastVisitService.selectProfileLastVisit(profileLastVisit.getProfileId(), profileLastVisit.getProfileLastVisitType(), profileLastVisit.getObjectId());
        assertTrue(visitDate.compareTo(dateBefore) >= 0);
        assertTrue(visitDate.compareTo(new Date()) <= 0);
    }

}

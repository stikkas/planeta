package ru.planeta.api.service.concert;

/*
 * Created by Alexey on 17.10.2016.
 */
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import ru.planeta.api.exceptions.MoscowShowInteractionException;
import ru.planeta.api.exceptions.NotFoundException;
import ru.planeta.moscowshow.model.OrderItem;
import ru.planeta.test.AbstractTest;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

@Ignore
public class ConcertHoldServiceTest extends AbstractTest {

    @Autowired
    private PlaceService placeService;

    @Ignore
    @Test
    public void testConcertHold() throws InterruptedException, NotFoundException, MoscowShowInteractionException {
        OrderItem item = new OrderItem(1L, 2L, 3L, 4, new BigDecimal(500));
        OrderItem item2 = new OrderItem(3L, 4L, 5L, 6, new BigDecimal(500));
        List<OrderItem> items = Arrays.asList(item, item2);

        placeService.hold(items);
    }
}

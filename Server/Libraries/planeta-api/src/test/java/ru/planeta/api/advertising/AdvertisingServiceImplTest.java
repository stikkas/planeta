package ru.planeta.api.advertising;

import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import ru.planeta.model.advertising.AdvertisingDTO;
import ru.planeta.model.advertising.VastDTO;
import ru.planeta.model.advertising.geenerated.VAST;
import ru.planeta.test.AbstractTest;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.StringReader;
import java.io.StringWriter;

/**
 * Created with IntelliJ IDEA.
 * Date: 20.01.14
 * Time: 20:27
 */
public class AdvertisingServiceImplTest extends AbstractTest {

    @Autowired
    private AdvertisingService advertisingService;

    @Test
    public void testLoadVASTByBroadcast() throws Exception {
        VAST vast = advertisingService.extractVast(new AdvertisingDTO());
        JAXBContext jaxbContext = JAXBContext.newInstance(VAST.class);
        Marshaller marshaller = jaxbContext.createMarshaller();
        marshaller.marshal(vast, System.out);
        System.out.println();
    }

    @Test
    @Ignore
    public void testLoadVASTStringByBroadcastId() throws Exception {

    }

    @Test
    public void testPutVastString() throws Exception {
        VastDTO dto = new VastDTO();
        dto.setAdId(100500);
        dto.setAdName("test AD name");
        dto.setSkippable(true);
        dto.setDeliveryType("blablabla");
        dto.setDuration(100000);
        dto.setHeight(512);
        dto.setWidth(513);
        dto.setMediaType("some media type");
        dto.setRedirectUrl("http://google.com");
        dto.setVideoUrl("http://youtube.com");

        AdvertisingDTO advertisingDTO = new AdvertisingDTO();

        VAST vast = advertisingService.createNewVast(dto);

        advertisingService.putVastString(vast, advertisingDTO);

        System.out.println(advertisingDTO.getVastXml());
    }

    @Test
    @Ignore
    public void testWrap() throws Exception {

    }

    @Test
    public void testCreateNewVast() throws Exception {
        VastDTO dto = new VastDTO();
        dto.setAdId(100500);
        dto.setAdName("test AD name");
        dto.setSkippable(true);
        dto.setDeliveryType("blablabla");
        dto.setDuration(100000);
        dto.setHeight(512);
        dto.setWidth(513);
        dto.setMediaType("some media type");
        dto.setRedirectUrl("http://google.com");
        dto.setVideoUrl("http://youtube.com");

        VAST vast = advertisingService.createNewVast(dto);
        JAXBContext jaxbContext = JAXBContext.newInstance(VAST.class);
        Marshaller marshaller = jaxbContext.createMarshaller();
        StringWriter writer = new StringWriter();
        marshaller.marshal(vast, writer);

        StringReader reader = new StringReader(writer.toString());
        Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
        vast = (VAST) unmarshaller.unmarshal(reader);
    }
}

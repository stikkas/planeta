package ru.planeta.api.service.comments;

import org.apache.log4j.Logger;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import ru.planeta.api.model.UserAuthorizationInfo;
import ru.planeta.commons.text.Bbcode;
import ru.planeta.dao.profiledb.CommentDAO;
import ru.planeta.dao.profiledb.CommentObjectDAO;
import ru.planeta.dao.profiledb.VideoDAO;
import ru.planeta.model.enums.ObjectType;
import ru.planeta.model.enums.ProfileStatus;
import ru.planeta.model.profile.media.enums.VideoConversionStatus;
import ru.planeta.model.profile.media.enums.VideoQuality;
import ru.planeta.model.profile.*;
import ru.planeta.model.profile.media.Video;
import ru.planeta.test.AbstractTest;

import java.util.Date;
import java.util.EnumSet;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Tests for CommentService
 *
 * @author a.savanovich
 *         Date: 27.10.11
 */
public class TestCommentService extends AbstractTest {

    private static final Logger log = Logger.getLogger(TestCommentService.class);

    @Autowired
    CommentsService commentsService;
    @Autowired
    VideoDAO videoDAO;
    @Autowired
    CommentDAO commentDAO;
    @Autowired
    CommentObjectDAO commentObjectDAO;

    @Test
    public void testCommentAddRemove() throws Exception {
        // we need user in base to comment smthng
        String email = "testuser@test.ee";
        String password = "123123";
        UserAuthorizationInfo user = registerUser(email, password);

        // now we need only userId
        long profileId = user.getProfile().getProfileId();
        Video video = createNewVideo();
        video.setProfileId(user.getProfile().getProfileId());
        videoDAO.insert(video);

        // try to comment video
        String text = "Test VIdeo Comment";
        Comment comment = createComment(video.getProfileId(), profileId, text);
        comment.setObjectId(video.getVideoId());
        // comment.setObjectType(vi)

        int commentsNumberBeforeFirst = videoDAO.selectVideoById(comment.getProfileId(), comment.getObjectId()).getCommentsCount();
        // Adding comment
        Comment userComment = commentsService.addComment(comment);
        assertTrue(userComment.getObjectId() > 0);
        // Checking count of comments for this video
        CommentObject commentableObject = commentObjectDAO.select(comment);
        assertEquals(1, commentableObject.getCommentsCount());
        // Deleting comment and checking comments count again
        commentsService.deleteComment(comment.getProfileId(), comment.getProfileId(), comment.getCommentId());
        Comment selected = commentDAO.selectCommentById(comment.getProfileId(), comment.getCommentId());
        commentableObject = commentObjectDAO.select(comment);
        assertNotNull(selected);
        assertTrue(selected.isDeleted());
        assertEquals(0, commentableObject.getCommentsCount());
        // Checking restore mechanism
        commentsService.restoreComment(comment.getProfileId(), comment.getProfileId(), comment.getCommentId());
        selected = commentDAO.selectCommentById(comment.getProfileId(), comment.getCommentId());
        commentableObject = commentObjectDAO.select(comment);
        assertEquals(1, commentableObject.getCommentsCount());
        assertNotNull(selected);
        assertFalse(selected.isDeleted());

        int commentsNumberAfterFirst = videoDAO.selectVideoById(comment.getProfileId(), comment.getObjectId()).getCommentsCount();
        assertEquals(1, commentsNumberAfterFirst);
        assertNotNull(userComment);
        assertTrue(userComment.getCommentId() > 0);
        assertEquals(userComment.getProfileId(), video.getProfileId());
        assertEquals(Bbcode.transform(text), userComment.getTextHtml());

        Comment votedComment = commentDAO.selectCommentById(userComment.getProfileId(), userComment.getCommentId());
        assertNotNull(votedComment);
    }

    @Test
    public void testAttachYoutubeLink() throws Exception {

        // we need user in base to comment smthng
        String email = "testuser@test.ee";
        String password = "123123";
        UserAuthorizationInfo user = registerUser(email, password);

        // now we need only userId
        long profileId = user.getProfile().getProfileId();
        Video video = createNewVideo();
        video.setProfileId(user.getProfile().getProfileId());
        videoDAO.insert(video);

        // try to comment video
        String text = "http://www.youtube.com/watch?v=kJDblLpnBA8";
        Comment comment = createComment(video.getProfileId(), profileId, text);
        comment.setObjectId(video.getVideoId());
        // comment.setObjectType(vi)

        int commentsNumberBeforeFirst = videoDAO.selectVideoById(comment.getProfileId(), comment.getObjectId()).getCommentsCount();
        // Adding comment
        Comment userComment = commentsService.addComment(comment);
        assertTrue(userComment.getObjectId() > 0);
        log.info(userComment.getTextHtml());
        // Checking count of comments for this video
        CommentObject commentableObject = commentObjectDAO.select(comment);
        assertEquals(1, commentableObject.getCommentsCount());
        // Deleting comment and checking comments count again
        commentsService.deleteComment(comment.getProfileId(), comment.getProfileId(), comment.getCommentId());
        Comment selected = commentDAO.selectCommentById(comment.getProfileId(), comment.getCommentId());
        assertNotNull(selected);
        commentableObject = commentObjectDAO.select(comment);
        assertNotNull(commentableObject);
        assertTrue(selected.isDeleted());
        assertEquals(0, commentableObject.getCommentsCount());
        // Checking restore mechanism
        commentsService.restoreComment(comment.getProfileId(), comment.getProfileId(), comment.getCommentId());
        selected = commentDAO.selectCommentById(comment.getProfileId(), comment.getCommentId());
        assertNotNull(selected);
        commentableObject = commentObjectDAO.select(comment);
        assertEquals(1, commentableObject.getCommentsCount());
        assertFalse(selected.isDeleted());

        int commentsNumberAfterFirst = videoDAO.selectVideoById(comment.getProfileId(), comment.getObjectId()).getCommentsCount();
        assertEquals(1, commentsNumberAfterFirst);
        assertNotNull(userComment);
        assertTrue(userComment.getCommentId() > 0);
        assertEquals(userComment.getProfileId(), video.getProfileId());
//            assertEquals(Bbcode.transform(text), userComment.getTextHtml());

    }

    @Test
    public void testNotVerifiedAccountAddComment() throws Exception {

        String email = "test2user@test.ee";
        String password = "123123";
        UserAuthorizationInfo user = registerUser(email, password);
        Profile profile = user.getProfile();
        profile.setStatus(ProfileStatus.NOT_SET);
        getProfileDAO().update(profile);

        // now we need only userId
        long profileId = profile.getProfileId();
        Video video = createNewVideo();
        video.setProfileId(profile.getProfileId());
        videoDAO.insert(video);

        // try to comment video (should throw exception, since user has not verified his/her account)
        String text = "Test VIdeo Comment";
        Comment comment = createComment(video.getProfileId(), profileId, text);
        comment.setObjectId(video.getVideoId());

        commentsService.addComment(comment);
    }

    @Test
    public void testCommentGetLastComments() throws Exception {

        // we need user in base to comment smthng
        String email = "testuser@test.ee";
        String password = "123123";
        UserAuthorizationInfo user = registerUser(email, password);

        // now we need only userId
        long profileId = user.getProfile().getProfileId();
        Video video = createNewVideo();
        video.setProfileId(user.getProfile().getProfileId());
        videoDAO.insert(video);

        // try to comment video
        String text = "Test VIdeo Comment";
        Comment comment = createComment(video.getProfileId(), profileId, text);
        comment.setObjectId(video.getVideoId());

        commentsService.addComment(comment);
        List<Comment> lastComments = commentsService.getLastComments(video.getProfileId(), video.getVideoId(), ObjectType.VIDEO, 0, "desc", 0, 0);
        assertEquals(1, lastComments.size());

        lastComments = commentsService.getLastComments(video.getProfileId(), video.getVideoId(), ObjectType.VIDEO, comment.getCommentId(), "desc", 0, 0);
        assertEquals(0, lastComments.size());

    }

    private Comment createComment(long ownerId, long authorId, String text) {

        Comment comment = new Comment();
        comment.setAuthorProfileId(authorId);
        comment.setProfileId(ownerId);
        comment.setObjectType(ObjectType.VIDEO);
        comment.setText(text);
        comment.setParentCommentId(0);
        return comment;
    }

    private static Video createNewVideo() {
        Video video = new Video();
        video.setProfileId(99988L);
        video.setAuthorProfileId(99987);
        video.setViewsCount(0);
        video.setDescription("description");
        video.setName("test video name");
        video.setAvailableQualities(EnumSet.of(VideoQuality.MODE_480P));
        video.setVideoUrl("video url");
        video.setImageId(199999);
        video.setImageUrl("http://test_video/url");
        video.setStatus(VideoConversionStatus.PROCESSING);
        video.setTimeAdded(new Date());
        video.setTimeUpdated(new Date());
        video.setDuration(112);
        return video;
    }
}

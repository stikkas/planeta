package ru.planeta.api.service;

import org.apache.commons.lang3.time.DateUtils;
import org.apache.log4j.Logger;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.MessageSource;
import ru.planeta.api.exceptions.NotFoundException;
import ru.planeta.api.exceptions.OrderException;
import ru.planeta.api.exceptions.PermissionException;
import ru.planeta.api.log.service.DBLogServiceImpl;
import ru.planeta.api.service.billing.order.OrderService;
import ru.planeta.api.service.campaign.CampaignService;
import ru.planeta.api.service.campaign.ContractorService;
import ru.planeta.api.service.comments.CommentsService;
import ru.planeta.api.service.notifications.NotificationService;
import ru.planeta.dao.commondb.CampaignDAO;
import ru.planeta.model.common.Contractor;
import ru.planeta.model.common.Order;
import ru.planeta.model.common.OrderInfo;
import ru.planeta.model.common.OrderObjectInfo;
import ru.planeta.model.common.campaign.Campaign;
import ru.planeta.model.common.campaign.CampaignTag;
import ru.planeta.model.common.campaign.Share;
import ru.planeta.model.common.campaign.enums.CampaignStatus;
import ru.planeta.model.enums.ObjectType;
import ru.planeta.model.profile.Comment;
import ru.planeta.model.profile.Profile;
import ru.planeta.model.shop.ProductInfo;
import ru.planeta.test.AbstractTest;

import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.util.*;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * @author s.kalmykov
 *         Date: 11.04.12
 */
public class TestNotificationService extends AbstractTest {

    private static final Logger log = Logger.getLogger(TestNotificationService.class);


    @MockBean
    private DBLogServiceImpl dbLogService;
    @Autowired
    private NotificationService notificationService;

    @Autowired
    private CampaignService campaignService;
    @Autowired
    private OrderService orderService;
    @Autowired
    private CampaignDAO campaignDAO;
    @Autowired
    private CommentsService commentsService;
    @Value("${static.host}")
    private String staticAppHost;

    @Autowired
    private MessageSource messageSource;

    @Autowired
    private ContractorService contractorService;

    @Test
    public void testShopNotifications() throws Exception {
        Profile groupCreator1 = insertUserProfile();
        long groupInfo1 = registerSimpleGroup(groupCreator1.getProfileId());
        setGroupIsMerchant(groupInfo1); // +1 notification

        // product comment
        Comment comment = new Comment();
        comment.setText("some text");
        comment.setProfileId(groupInfo1);
        Profile commentator = insertUserProfile();
        ProductInfo product = createProductMETA(groupInfo1);
        comment.setAuthorProfileId(commentator.getProfileId());
        comment.setObjectType(ObjectType.PRODUCT);
        comment.setObjectId(product.getProductId());
        commentsService.addComment(comment); // no notification now

    }

    @Test
    public void testProjectsNotifications() throws Exception {
        Profile admin = getProfileDAO().selectById(insertPlanetaAdminPrivateInfo().getUserId());
        long groupCreator1 = insertPlanetaAdminPrivateInfo().getUserId();
        long groupInfo1 = registerSimpleGroup(groupCreator1);
        setGroupIsMerchant(groupInfo1);


        // product comment
        Comment comment = new Comment();
        comment.setText("some text");
        comment.setProfileId(groupInfo1);
        Profile commentator = registerUser("notifications-campaign-buyer@testtest.ee", "123").getProfile();
        Campaign campaign = createCampaign(groupCreator1, groupInfo1, BigDecimal.TEN, CampaignStatus.PAUSED);
        campaign.setDescription("Short description");

        campaignService.publishCampaign(groupCreator1, campaign.getCampaignId());

        Share share = campaignService.saveShare(groupCreator1,
                insertShare(groupInfo1, campaign.getCampaignId()));
        Share share1 = insertShare(groupInfo1, campaign.getCampaignId());
        share1.setName("another share");
        campaignService.saveShare(groupCreator1, share1);


        startCampaign(groupCreator1, campaign);
        increaseProfileBalance(commentator.getProfileId(), BigDecimal.TEN);
        purchaseShare(commentator.getProfileId(), commentator.getProfileId(), share.getShareId(), null, 1, null);
        Order order = purchaseShare(commentator.getProfileId(), commentator.getProfileId(), share1.getShareId(), null, 1, null);
        assertNotNull(admin);
        orderService.cancelSharePurchase(admin.getProfileId(), order.getOrderId(), "", false);
        comment.setAuthorProfileId(commentator.getProfileId());
        comment.setObjectType(ObjectType.CAMPAIGN);
        comment.setObjectId(campaign.getCampaignId());
        commentsService.addComment(comment);

    }

    private Order purchaseShare(long clientId, long buyerId, long shareId, BigDecimal donateAmount, int count, String buyerAnswerToMerchantQuestion) throws NotFoundException, PermissionException, OrderException {
        Order result = orderService.createOrderWithShare(clientId, buyerId, shareId, donateAmount, count, buyerAnswerToMerchantQuestion, null, null, null);
        orderService.purchase(result);

        return result;
    }

    @Ignore
    @Test
    public void purchaseBiblio() throws NotFoundException, PermissionException, OrderException {
        Order result = orderService.getOrder(551353);
        orderService.purchase(result);
    }

    @Test
    public void testCampaignReachedAmount() throws PermissionException, NotFoundException {
        Profile group = insertUserProfile();

        Campaign campaign = new Campaign();
        campaign.setProfileId(group.getProfileId());
        campaign.setName("Campaign Name");
        campaign.setDescription("Campaign Description");
        campaign.setDescriptionHtml("Campaign Description");
        campaign.setTargetAmount(new BigDecimal(11111));
        campaign.setTags(Collections.<CampaignTag>emptyList());
        Date date = new Date();
        DateUtils.addDays(date, 100);
        campaign.setTimeFinish(date);
        campaign.setPurchaseCount(0);
        campaign.setPurchaseSum(new BigDecimal(45108));
        campaign.setStatus(CampaignStatus.DRAFT);
        campaignDAO.insert(campaign);

        campaign = campaignDAO.selectCampaignsByStatus(group.getProfileId(), null, 0, 20).get(0);
        assertTrue(campaign.getName().equals("Campaign Name"));

        Share share = insertShare(group.getProfileId(), campaign.getCampaignId());

        campaignService.updateAfterSharePurchase(campaign.getCampaignId(), 1, new BigDecimal(100), share.getShareId());
        log.info(campaign.getPurchaseSum());
    }


    private void startCampaign(long clientId, Campaign campaign) throws NotFoundException, PermissionException {
        Contractor contractor = new Contractor();
        contractor.setName("Vasya" + campaign.getCampaignId());
        contractor.setPassportNumber("1234567890");
        contractorService.add(contractor, 11111, 34205L);
        contractorService.insertRelation(contractor.getContractorId(), campaign.getCampaignId());
        campaignService.startCampaign(clientId, campaign.getCampaignId());
    }

    @Test
    public void testMessageSource() {
        for (int i = 0; i < 300; ++i) {
            int wordCaseNumber = ru.planeta.commons.lang.StringUtils.calcWordCaseNumber(i);
            System.out.println(i + " " + messageSource.getMessage("successful.campaigns.stats.message", new Object[]{i, wordCaseNumber}, Locale.getDefault()));
        }
    }

    @Ignore
    @Test
    public void testSendSharePurchaseNotification() {
        OrderInfo order = new OrderInfo();
        order.setBuyerEmail("michail.michail@gmail.com");
        ArrayList<OrderObjectInfo> orderObjects = new ArrayList<>();
        orderObjects.add(new OrderObjectInfo());
        order.setOrderObjectsInfo(orderObjects);
        notificationService.sendPurchaseShareNotificationEmail(order, "");
    }

    @Test
    public void testSendCampaignChangedStatusNotification() throws Exception {
        Profile admin = getProfileDAO().selectById(insertPlanetaAdminPrivateInfo().getUserId());
        long groupCreator1 = insertPlanetaAdminPrivateInfo().getUserId();
        // group creation notification
        long groupInfo1 = registerNewOfficialGroup(groupCreator1);

        Campaign campaign = createCampaign(groupCreator1, groupInfo1, BigDecimal.TEN, CampaignStatus.ACTIVE);
        campaign.setDescription("Short description");

        // creation notification on description create
        campaignService.publishCampaign(groupCreator1, campaign.getCampaignId());
        campaign.setTags(Collections.<CampaignTag>emptyList());
        // no notification
        campaignService.publishCampaign(groupCreator1, campaign.getCampaignId());
        // campaign started notification
        startCampaign(groupCreator1, campaign);
        campaignService.changeCampaignStatus(admin.getProfileId(), campaign.getCampaignId(), CampaignStatus.FINISHED);
    }

    @Ignore
    @Test
    public void testVipNotification() throws NotFoundException {
        long userId = 191;
        Profile profile = getProfileDAO().selectById(userId);
        if (profile == null) {
            throw new NotFoundException(Profile.class, userId);
        }

        notificationService.sendUserBecameVipNotification(profile);
    }

    @Test
    @Ignore
    public void testSendAuthorCampaignsNotifications() throws NotFoundException, PermissionException {
        for (int i = 1; i < 10000; ++i) {
            Date date = DateUtils.addDays(new Date(), -i);
            List<Long> profileIds = campaignService.selectProfileIdsOfCampaignsWithEventsForDay(0, 1, date);

            if (!profileIds.isEmpty()) {
                notificationService.sendAuthorCampaignNotifications(profileIds.get(0), "noreply@planeta.ru", 20, "asavanster@gmail.com", date);
                break;
            }
        }
    }

    @Test
    @Ignore
    public void testSendInteractiveCampaignFinished() throws UnsupportedEncodingException {
        notificationService.sendInteractiveCampaignFinished(191L, "michail.michail@gmail.com", "Hulio Iglesias");
    }

    @Test
    @Ignore
    public void sendOrderCancelRequestEmails() throws Exception {
        notificationService.sendOrderCancelRequestEmails(931372, 393705);
    }
}

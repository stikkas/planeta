package ru.planeta.api.service.billing.payment;

import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import ru.planeta.api.exceptions.NotFoundException;
import ru.planeta.api.exceptions.OrderException;
import ru.planeta.api.exceptions.PaymentException;
import ru.planeta.api.exceptions.PermissionException;
import ru.planeta.api.service.billing.order.OrderService;
import ru.planeta.model.common.Order;
import ru.planeta.model.common.campaign.Share;
import ru.planeta.model.common.TopayTransaction;
import ru.planeta.model.enums.ProjectType;
import ru.planeta.model.enums.TopayTransactionStatus;
import ru.planeta.test.AbstractTest;

import java.math.BigDecimal;

import static org.junit.Assert.*;

/**
 * Payment service test.
 * User: e.shevchenko
 */
public class TestPaymentService extends AbstractTest {

    @Autowired
    private OrderService orderService;

    @Test
    @Ignore
    public void testPaymentCreating() throws Exception {
        final long profileId = getProfileId(registerUser());
        final long adminId = getProfileId(registerPlanetaAdminUser());
        final long merchantId = registerNewOfficialGroup(adminId);

        final ProjectType projectType = ProjectType.MAIN;
        final long paymentMethodId = 9;

        final BigDecimal amountToPay = BigDecimal.TEN;
        final BigDecimal sharePrice = BigDecimal.TEN.add(BigDecimal.ONE);

        TopayTransaction transaction = paymentService.createPayment(profileId, amountToPay, 1, projectType, null);
        assertNotNull(transaction);
        assertEquals(profileId, transaction.getProfileId());
        assertEquals(0, transaction.getOrderId());
        assertEquals(projectType, transaction.getProjectType());
        assertEqualsBigDecimal(amountToPay, transaction.getAmountNet());
        assertEqualsBigDecimal(calculateAmountFee(transaction), transaction.getAmountFee());
        assertTransactionStatusEquals(transaction, TopayTransactionStatus.NEW);

        Share share = createShareWithCampaign(adminId, merchantId, sharePrice, 1);
        Order order = orderService.createOrderWithShare(profileId, profileId, share.getShareId(), share.getPrice(), 1, getTestStr("replay"), null, null, null);

        TopayTransaction orderPayment = paymentService.createOrderPayment(order, amountToPay, paymentMethodId, projectType, null, true);
        order = orderService.getOrderSafe(order.getOrderId());
        assertNotNull(orderPayment);
        assertNotEquals(orderPayment.getOrderId(), 0);
        assertEquals(profileId, orderPayment.getProfileId());
        assertEquals(order.getOrderId(), orderPayment.getOrderId());
        assertEquals(orderPayment.getTransactionId(), order.getTopayTransactionId());
        assertEquals(projectType, orderPayment.getProjectType());
        assertEqualsBigDecimal(amountToPay, orderPayment.getAmountNet());
        assertEqualsBigDecimal(calculateAmountFee(transaction), orderPayment.getAmountFee());
        assertTransactionStatusEquals(orderPayment, TopayTransactionStatus.NEW);

        TopayTransaction fromOrderPayment = paymentService.createPaymentFrom(orderPayment.getTransactionId(), paymentMethodId, null);
        order = orderService.getOrderSafe(order.getOrderId());
        assertNotNull(fromOrderPayment);
        assertNotEquals(fromOrderPayment.getOrderId(), 0);
        assertEquals(profileId, fromOrderPayment.getProfileId());
        assertEquals(order.getOrderId(), fromOrderPayment.getOrderId());
        assertEquals(fromOrderPayment.getTransactionId(), order.getTopayTransactionId());
        assertEquals(projectType, fromOrderPayment.getProjectType());
        assertEqualsBigDecimal(amountToPay, orderPayment.getAmountNet());
        assertEqualsBigDecimal(calculateAmountFee(transaction), orderPayment.getAmountFee());
        assertTransactionStatusEquals(orderPayment, TopayTransactionStatus.NEW);
    }

    @Test
    @Ignore
    public void testPaymentProcessing() throws NotFoundException, PermissionException, OrderException, PaymentException {
        final String extSysData = getTestStr("externalSystemData");

        final long buyerId = getProfileId(registerUser());
        final long adminId = getProfileId(registerPlanetaAdminUser());
        final long merchantId = registerNewOfficialGroup(adminId);

        final BigDecimal sharePrice = BigDecimal.TEN;
        final Share share = createShareWithCampaign(adminId, merchantId, sharePrice, 1);
        final Order order = orderService.createOrderWithShare(buyerId, buyerId, share.getShareId(), share.getPrice(), 1, getTestStr("replay"), null, null, null);
        final TopayTransaction payment = paymentService.createOrderPayment(order, sharePrice, 1, ProjectType.MAIN, null, true);

        assertBalanceEquals(buyerId, BigDecimal.ZERO);
        assertBalanceEquals(merchantId, BigDecimal.ZERO);

//        assertTrue(paymentProcessingService.tryProcessPayment(payment.getTransactionId(), payment.getPaymentSystemType(), extSysData));

        assertBalanceEquals(buyerId, BigDecimal.ZERO);
        assertBalanceEquals(merchantId, sharePrice);
        assertEquals(1, orderService.getBuyerOrdersInfo(buyerId, buyerId, null, null, null, 0, 0).size());

//        assertTrue(paymentProcessingService.tryProcessPayment(payment.getTransactionId(), payment.getPaymentSystemType(), extSysData));
//        assertFalse(paymentProcessingService.tryProcessPayment(0L, PaymentSystemType.PLANETA, extSysData));
    }

    @Test
    @Ignore
    public void testPaymentManipulation() throws NotFoundException, PermissionException, PaymentException {
        final long profileId = getProfileId(registerUser());
        final BigDecimal AMOUNT_NET = BigDecimal.TEN;
        final BigDecimal AMOUNT_FEE = BigDecimal.ZERO;

        TopayTransaction transaction = paymentService.createPayment(profileId, AMOUNT_NET, 9, ProjectType.MAIN, null);
        assertNotNull(transaction);
        assertEquals(profileId, transaction.getProfileId());
        assertEqualsBigDecimal(AMOUNT_NET, transaction.getAmountNet());
        assertEqualsBigDecimal(calculateAmountFee(transaction), transaction.getAmountFee());
        assertTransactionStatusEquals(transaction, TopayTransactionStatus.NEW);

        final long transactionId = transaction.getTransactionId();
        transaction = paymentService.getPayment(transactionId);
        assertTransactionStatusEquals(transaction, TopayTransactionStatus.DONE);
        assertBalanceEquals(profileId, AMOUNT_NET.subtract(AMOUNT_FEE));

        paymentService.updatePaymentStatus(transaction, TopayTransactionStatus.ERROR);
        transaction = paymentService.getPayment(transactionId);
        assertTransactionStatusEquals(transaction, TopayTransactionStatus.ERROR);
    }

    private BigDecimal calculateAmountFee(TopayTransaction payment) {
        return BigDecimal.ZERO;
    }


    private void assertTopayTransactionEquals(TopayTransaction expected, TopayTransaction actual) {
        assertNotNull(expected);
        assertNotNull(actual);
        assertEquals(expected.getTransactionId(), actual.getTransactionId());
        assertEquals(expected.getProfileId(), actual.getProfileId());
        assertEquals(expected.getOrderId(), actual.getOrderId());
        assertEqualsBigDecimal(expected.getAmountNet(), actual.getAmountNet());
        assertEqualsBigDecimal(expected.getAmountFee(), actual.getAmountFee());
        assertEquals(expected.getStatus(), actual.getStatus());
        assertEquals(expected.getDebitTransactionId(), actual.getDebitTransactionId());
        assertEquals(expected.getParam1(), actual.getParam1());
        assertEquals(expected.getParam2(), actual.getParam2());

        assertEquals(expected.getPaymentProviderId(), actual.getPaymentProviderId());
        assertEquals(expected.getPaymentMethodId(), actual.getPaymentMethodId());
    }

    private void assertTransactionStatusEquals(TopayTransaction transaction, TopayTransactionStatus status) {
        assertNotNull(transaction);
        assertNotNull(status);
        assertEquals(status, transaction.getStatus());
    }
}

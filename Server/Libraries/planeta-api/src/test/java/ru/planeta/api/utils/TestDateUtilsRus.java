package ru.planeta.api.utils;

import org.apache.log4j.Logger;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import ru.planeta.test.AbstractTest;

import javax.annotation.PostConstruct;
import java.util.Calendar;
import java.util.Locale;

import static org.junit.Assert.assertEquals;
import org.junit.Ignore;

/**
 * Created with IntelliJ IDEA.
 * User: user
 * Date: 18.10.13
 * Time: 17:29
 * To change this template use File | Settings | File Templates.
 */
@Ignore
public class TestDateUtilsRus extends AbstractTest {
    private Logger log = Logger.getLogger(TestDateUtilsRus.class);

    @Autowired
    public MessageSource messageSource;

    private DateUtilsRus dateUtilsRus;

    @PostConstruct
    public void initDateUtilsRusService() {
        dateUtilsRus = new DateUtilsRus(messageSource);
    }



    @Test
    public void testDateUtilsRus() {
        int year = 2013;
        int month = 8;
        int date = 7;
        int hour = 17;
        int minute = 48;
        int millisecond = 0;

        Calendar calendar = Calendar.getInstance();

        calendar.set(Calendar.YEAR, year);
        calendar.set(Calendar.MONTH, month);
        calendar.set(Calendar.DAY_OF_MONTH, date);
        calendar.set(Calendar.HOUR_OF_DAY, hour);
        calendar.set(Calendar.MINUTE, minute);
        calendar.set(Calendar.MILLISECOND, millisecond);

        String customSeparator = dateUtilsRus.getCustomRusSeparator();
        String stringDate = dateUtilsRus.dateFormat(calendar.getTime());
        String handMadeDate = date + " " + messageSource.getMessage("list.months", null, "", Locale.getDefault()).split("\\|")[month] + " " + year + " " + customSeparator + " " + hour + ":" + minute;
        assertEquals(stringDate, handMadeDate);
        log.error(calendar.getTime() + " to " + stringDate);
    }
}

package ru.planeta.api.promo;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import ru.planeta.api.service.promo.PromoConfigService;
import ru.planeta.api.service.promo.PromoEmailService;
import ru.planeta.model.trashcan.PromoConfig;
import ru.planeta.model.trashcan.PromoEmail;
import ru.planeta.test.AbstractTest;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 *
 * Created by alexa_000 on 17.11.2014.
 */
public class PromoServicesTest extends AbstractTest {
    @Autowired
    private PromoConfigService promoConfigService;
    @Autowired
    private PromoEmailService promoEmailService;

    @Ignore
    @Test
    public void testGetCompatibleConfigs() {
        Collection<Long> compTags = new ArrayList<Long>() {{
            add(17L);
            add(3L);
            add(5L);
        }};
        List<PromoConfig> configs = promoConfigService.getCompatibleConfigs(compTags, BigDecimal.ONE, "qwerty.nyoron@gmail.com");
        Assert.assertNotNull(configs);
    }

    @Ignore
    @Test
    public void testInsertEmail() {
        promoEmailService.insert(new PromoEmail(3, 100, "qqq@qqq.qqq", 100));
    }

    @Ignore
    @Test
    public void testSwitch() {
        PromoConfig config = promoConfigService.selectById(3);
        promoConfigService.switchStatus(191, 3);
        PromoConfig config1 = promoConfigService.selectById(3);
        Assert.assertNotEquals(config.getStatus(), config1.getStatus());
    }
}

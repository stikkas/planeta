package ru.planeta.api.service.concert;

import org.junit.Ignore;
import org.junit.Test;
import ru.planeta.model.common.Address;

public class ConcertServiceNewTest {

    @Test
    @Ignore
    public void testGenerateYandexMap() throws Exception {
        Address address = new Address("238530", "Россия", "Зеленоградск, Зеленоградский район, Калининградская область", "Курортный проспект, 10", "");
        String yandexUrl = ConcertMapServiceImpl.generateYandexMapUrl(address, 20.479648, 54.961410);
        System.out.println(yandexUrl);
    }
}

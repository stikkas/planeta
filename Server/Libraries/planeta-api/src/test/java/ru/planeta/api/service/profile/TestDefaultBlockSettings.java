package ru.planeta.api.service.profile;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import ru.planeta.api.service.configurations.DefaultBlockSettings;
import ru.planeta.model.enums.BlockSettingType;
import ru.planeta.model.enums.PermissionLevel;
import ru.planeta.model.profile.Profile;
import ru.planeta.model.profile.ProfileBlockSetting;
import ru.planeta.test.AbstractTest;

import java.util.Map;

import static org.junit.Assert.assertEquals;

public class TestDefaultBlockSettings extends AbstractTest {

	@Autowired
	private DefaultBlockSettings defaultBlockSettings;

    @Test
    public void testWallSettings() {
        Profile profile = insertUserProfile();
        Map<BlockSettingType, ProfileBlockSetting> defaultSettings = defaultBlockSettings.getDefaultPermissionLevels(profile.getProfileType());
        ProfileBlockSetting wallSetting = defaultSettings.get(BlockSettingType.WALL);
        assertEquals(wallSetting.getCommentPermission(), PermissionLevel.EVERYBODY);
        assertEquals(wallSetting.getAddPermission(), PermissionLevel.FRIENDS);
    }
}

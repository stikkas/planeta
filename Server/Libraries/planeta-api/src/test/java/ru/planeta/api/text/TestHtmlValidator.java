package ru.planeta.api.text;

import org.junit.Test;
import ru.planeta.test.AbstractTest;

import static org.junit.Assert.*;

/**
 * Tests for HtmlValidator
 *
 * @author m.shulepov
 */
public class TestHtmlValidator extends AbstractTest {

    /**
     * Test valid some planeta specific js methods
     */
    @Test
    public void testValidateHtml() {
        String nonValidHtml1 = "<div><a width=\"15\" height=\"15\" href=\"javascript:workspace.playTrack(1111, 222);\"></a>"
                + "<b onclick=\"workspace.playTrack(1111, 222);\"></b>"
                + "<b onclick=\"workspace.playTrack(1111, 222);\"></b></div>";
        String nonValidHtml2 = "<div><a href='javascript:alert(\"XSS!!\");'></a></div>";
        String nonValidHtml3 = "<div><b onclick='alert(\"XSS!!\");'></b></div>";
        String nonValidHtml4 = "<div><b onclick=\"alert('XSS!!');\"></b></div>";
        String nonValidHtml5 = "<script>alert('не нажимай ОК, а то все поедет на прод');</script>";

        assertFalse(HtmlValidator.validateHtml(nonValidHtml1));
        String expected = "<div><a width=\"15\" height=\"15\"></a><b></b><b></b></div>";
        assertEquals(expected, HtmlValidator.cleanHtml(nonValidHtml1));

        assertFalse(HtmlValidator.validateHtml(nonValidHtml2));
        expected = "<div><a></a></div>";
        assertEquals(expected, HtmlValidator.cleanHtml(nonValidHtml2));

        assertFalse(HtmlValidator.validateHtml(nonValidHtml3));
        expected = "<div><b></b></div>";
        assertEquals(expected, HtmlValidator.cleanHtml(nonValidHtml3));

        assertFalse(HtmlValidator.validateHtml(nonValidHtml4));
        expected = "<div><b></b></div>";
        assertEquals(expected, HtmlValidator.cleanHtml(nonValidHtml4));

        assertFalse(HtmlValidator.validateHtml(nonValidHtml5));
        expected = "alert('не нажимай ОК, а то все поедет на прод');";
        assertEquals(expected, HtmlValidator.cleanHtml(nonValidHtml5));

        String html = "<p>dsfdsf</p><br></br><p>dsfs</p>";
        assertTrue(HtmlValidator.validateHtml(html));
        assertEquals(html, HtmlValidator.cleanHtml(html));

        String text = "Just a plain text";
        assertTrue(HtmlValidator.validateHtml(text));
        assertEquals(text, HtmlValidator.cleanHtml(text));
    }

    /**
     * Various XSS attacks from http://ha.ckers.org/xss.html
     * also remember about img embedded XSS <img src='http://vk.com?command=delete'>
     */
    @Test
    public void testXss() {
        String inputHtml = "<IMG SRC=&#0000106&#0000097&#0000118&#0000097&#0000115&#0000099&#0000114&#0000105&#0000112&#0000116&#0000058&#0000097&#0000108&#0000101&#0000114&#0000116&#0000040&#0000039&#0000088&#0000083&#0000083&#0000039&#0000041>"
                + "<IMG SRC=JaVaScRiPt:alert('XSS')>" + "<IMG SRC=`&#106;&#97;&#118;&#97;&#115;&#99;&#114;&#105;&#112;&#116;&#58;&#97;&#108;&#101;&#114;&#116;&#40;&#39;&#88;&#83;&#83;&#39;&#41;`>" +
                "<IMG SRC=&#x6A&#x61&#x76&#x61&#x73&#x63&#x72&#x69&#x70&#x74&#x3A&#x61&#x6C&#x65&#x72&#x74&#x28&#x27&#x58&#x53&#x53&#x27&#x29>" +
                "<IMG \"\"\"><SCRIPT>alert(\"XSS\")</SCRIPT>\">";
        assertFalse(HtmlValidator.validateHtml(inputHtml));

        inputHtml = "<IMG SRC=\"jav\tascript:alert('XSS');\">"
                + "<IMG SRC=\"jav&#x09;ascript:alert('XSS');\">"
                + "<BGSOUND SRC=\"javascript:alert('XSS');\">"
                + "<iframe src=http://ha.ckers.org/scriptlet.html <";
        assertFalse(HtmlValidator.validateHtml(inputHtml));

        inputHtml = "<LINK REL=\"stylesheet\" HREF=\"javascript:alert('XSS');\">";
        assertFalse(HtmlValidator.validateHtml(inputHtml));

        inputHtml = "<img STYLE=\"behavior: url(xss.htc);\"/>"
                + "<IMG SRC='vbscript:msgbox(\"XSS\")'>"
                + "<META HTTP-EQUIV=\"refresh\" CONTENT=\"0;url=data:text/html;base64,PHNjcmlwdD5hbGVydCgnWFNTJyk8L3NjcmlwdD4K\">"
                + "<IFRAME SRC=\"javascript:alert(\"XSS\");\"></IFRAME>"
                + "<DIV STYLE=\"background-image: url(javascript:alert('XSS'))\"/>";
        assertFalse(HtmlValidator.validateHtml(inputHtml));

        inputHtml = "<!--This is a comment. Comments are not displayed in the browser-->" +
                "<p>This is a paragraph.</p>";
        assertFalse(HtmlValidator.validateHtml(inputHtml));
    }

    /**
     * Test tags closure
     */
    @Test
    public void testTagsClosure() {
        String html = "<b><a>a1</a>a2" +
                            "<span>111</span>" +
                            "<span />" +
                            "<div>222" +
                                "<a>333" +
                                    "<div><a>444</div>" +
                        "a3a4</b>";
        String resultHtml = HtmlValidator.cleanHtml(html);
        String expected = "<b><a>a1</a>a2<span>111</span><span></span></b><div><b>222<a>333</a></b><div><b><a>444</a></b></div><b>a3a4</b></div>";
        assertEquals(expected, resultHtml);
    }

    /**
     * Test comments
     */
    @Test
    public void testComments() {
        String html = "<div><b><a>a1</a>a2" +
                "<!-- 1111 -->" +
                "a3a4</b></div><!-- 2222 --><div>a5<!-- 3333 --></div>";
        String resultHtml = HtmlValidator.cleanHtml(html);
        String expected = "<div><b><a>a1</a>a2a3a4</b></div><div>a5</div>";
        assertEquals(expected, resultHtml);
    }

    /**
     * Specific custom tags
     */
    @Test
    public void testCustomTags() {
        String inputHtml = "<p>Some text!</p>"
                + "<br/>"
                + "<p:photo id=\"8494\" owner=\"3550\" image=\"http://s1.portalplaneta.ru/img/00/00/00/00/00/00/21/2e/photo_preview.jpg\" type=\"photo\" ></p:photo>"
                + "<p:audio id=\"18370\" owner=\"3550\" name=\"Длинное название трека со всякими штуками\" duration=\"0:35\" artist=\"Длинное имя артиста тынц\" ></p:audio>"
                + "<p:video id=\"8494\" owner=\"3550\" image=\"http://s1.portalplaneta.ru/img/00/00/00/00/00/00/21/2e/photo_preview.jpg\" duration=\"1:05:24\" ></p:video>"
                + "<p>Some more text</p>"
                + "http://someurl.com";
        assertTrue(HtmlValidator.validateHtml(inputHtml));


        String emptyTagsHtml = "<p:photo></p:photo>  <p:audio></p:audio> <p:video></p:video>";
        assertFalse(HtmlValidator.validateHtml(emptyTagsHtml));
    }

    @Test
    public void testUsualString() {
        String inputHtml = "Vasya petrov";
        String outHtml = HtmlValidator.cleanHtml(inputHtml);
        assertEquals(inputHtml, outHtml);
    }

}

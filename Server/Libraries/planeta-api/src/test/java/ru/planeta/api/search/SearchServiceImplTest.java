package ru.planeta.api.search;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import ru.planeta.model.profile.ProfileForAdminsWithEmail;
import ru.planeta.test.AbstractTest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 *
 * Created by asavan on 10.05.2017.
 */
public class SearchServiceImplTest extends AbstractTest {

    @Autowired
    private SearchService searchService;

    @Test
    public void adminSearchForProfiles() throws Exception {
        String query = "va@planeta.ru";
        SearchResult<ProfileForAdminsWithEmail> res = searchService.adminSearchForProfiles(query, null, 0, 10);
        assertNotNull(res);
        assertEquals(1, res.getEstimatedCount());
        assertEquals(query, res.getSearchResultRecords().get(0).getEmail());
    }

}

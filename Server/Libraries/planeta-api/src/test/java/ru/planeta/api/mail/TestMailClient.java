package ru.planeta.api.mail;

import org.apache.commons.lang3.StringEscapeUtils;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import ru.planeta.api.exceptions.NotFoundException;
import ru.planeta.api.exceptions.OrderException;
import ru.planeta.api.exceptions.PermissionException;
import ru.planeta.api.service.billing.order.OrderService;
import ru.planeta.api.service.common.PlanetaManagersService;
import ru.planeta.api.service.notifications.NotificationService;
import ru.planeta.model.common.OrderInfo;
import ru.planeta.model.common.OrderObjectInfo;
import ru.planeta.model.common.PlanetaManager;
import ru.planeta.model.common.campaign.Campaign;
import ru.planeta.model.profile.Profile;
import ru.planeta.test.AbstractTest;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;

import static org.junit.Assert.assertTrue;

/**
 * @author ds.kolyshev
 *         Date: 19.01.12
 */
public class TestMailClient extends AbstractTest {
    private MailClient mailClient;

    @Autowired
    public void setMailClient(MailClient mailClient) {
        this.mailClient = mailClient;
        this.mailClient.setAsynchronous(false);
    }

    @Autowired
    private NotificationService notificationService;

    @Autowired
    private OrderService orderService;

    @Autowired
    private PlanetaManagersService planetaManagersService;


    @Ignore
    @Test
    public void testManagerEmails() throws NotFoundException {
        Profile profile = profileService.getProfile(594649);
        Campaign campaign = campaignService.getCampaignSafe(33175L);
        PlanetaManager campaignManager = planetaManagersService.getCampaignManager(campaign.getCampaignId());
        mailClient.sendToManagerGroupCreatedCampaign("michail.michail.michail@yandex.ru", profile, campaign, campaignManager);
        mailClient.sendToUserCreatedGroupCampaign("michail.michail.michail@yandex.ru", profile, campaign, profile, "https://s3.planeta.ru/i/151c44/huge.jpg", campaignManager);
    }

    @Ignore
    @Test
    public void testFtlTemplateMail() {
        ArrayList<String> emails = new ArrayList<>();
        emails.add("michail.michail@gmail.com");
        notificationService.sendCampaignCanMakeVipOfferNotification(emails);
    }

    @Ignore
    @Test
    public void testDbTemplateMail() {
        String email = "michail.michail_sss.sd";
        notificationService.sendQuickstartGetBook(email, "book");
    }

    @Test
    @Ignore
    public void testSendEmails() throws IOException {
        String emailTo = "labnik@gmail.com";
        /*mailClient.sendRegistrationEmail(emailTo, "test-regCode-1231-123123-12", "http://test.planeta.ru?p1=123&p2=321");
        mailClient.sendRegistrationCompleteEmail(emailTo, "test password");
        mailClient.sendFeedbackEmail(emailTo, "test feeedback message");
        mailClient.sendNotificationEmail(emailTo, "Test first name", "Test last name", 11, "уведомлений");
        mailClient.sendPasswordRecoveryEmail(emailTo, "test-regCode-p-recovery", "test-url");*/

        mailClient.sendRegistrationCompleteEmail(emailTo, "test password", "test-regCode-1231-123123-12");
    }

    @Test
    public void testBillyBandUnescape() throws Exception {
//        MailClient mailClient = TestHelper.getContext().getBean(MailClient.class);
//        Profile profile = TestHelper.registerUser();
//        String emailTo = "kalmykov.sergei@gmail.com";
//        String name = "Новый альбом Billy&apos;s Band";
        String name = "Новый альбом &apos;";
        String escapedName = StringEscapeUtils.unescapeXml(name);
        assertTrue(!name.equals(escapedName));
    }


    private String getTmpUserEmial() {
        //временный почтовый ящик можно завести здесь www.guerrillamail.com/inbox
        String email = "";
        if (email.indexOf("@") < 0) {
            throw new RuntimeException("mail is not set");
        }
        return email;
    }

    @Test
    @Ignore
    public void testSendBonusOrderAddedNotifications() {
        //почту админа нужно установить в шаблоне в поле "Получатель"
        //mail.dev.planeta.ru/edit.html?templateName=bonus.order.added

        String userEmail = getTmpUserEmial();


        OrderInfo order = new OrderInfo();
        order.setOrderId(777L);
        order.setTimeAdded(new Date());
        order.setTotalPrice(new BigDecimal(100));
        order.setDeliveryPrice(new BigDecimal(10));
        OrderObjectInfo orderObject = new OrderObjectInfo();
        orderObject.setObjectName("-- test Bonus name test --");
        order.addOrderObjectInfo(orderObject);
        order.setBuyerName(" -- test Buyer name test ---");
        order.setBuyerEmail(userEmail);

        mailClient.sendBonusOrderAddedNotifications(order);
    }

    @Test
    @Ignore
    public void testSendFeedbackEmail() {
        //почту админа нужно установить в шаблоне в поле "Получатель"
        //mail.dev.planeta.ru/edit.html?templateName=feedback

        String email = "test@test.ru";
        String message = "-- test Message test --";
        String theme = "-- test Theme test --";

        mailClient.sendFeedbackEmail(email, message, theme, -1);
    }

    @Test
    @Ignore
    public void testSendOrderSentNotificationEmail() throws NotFoundException, PermissionException, OrderException {
        OrderInfo orderInfo = orderService.getOrderInfo(126096, 138939);
        notificationService.sendOrderSentNotificationEmail(orderInfo);
    }
}

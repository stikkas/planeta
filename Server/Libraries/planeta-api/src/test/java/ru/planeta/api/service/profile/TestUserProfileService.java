package ru.planeta.api.service.profile;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import ru.planeta.api.exceptions.NotFoundException;
import ru.planeta.api.exceptions.PermissionException;
import ru.planeta.api.model.json.*;
import ru.planeta.dao.profiledb.ProfileBlockSettingDAO;
import ru.planeta.dao.profiledb.UserDAO;
import ru.planeta.model.enums.*;
import ru.planeta.model.profile.*;
import ru.planeta.test.AbstractTest;

import static org.junit.Assert.*;

/**
 * Tests for profile service
 *
 * @author ds.kolyshev
 *         Date: 31.10.11
 */
public class TestUserProfileService extends AbstractTest {

    @Autowired
    private UserDAO userDAO;
    @Autowired
    private ProfileBlockSettingDAO profileBlockSettingDAO;


    @Test
    public void testUserAndGroupPrivateInfoPermission() throws NotFoundException, PermissionException {


        Profile profile = insertUserProfile();
        Profile client = insertUserProfile();

        User user = new User();
        user.setMartialStatus(MartialStatus.MARRIED);
        user.setProfileId(profile.getProfileId());
        userDAO.insert(user);

        addBlockSettingsPermission(profile, BlockSettingType.ABOUT, PermissionLevel.ADMINS, PermissionLevel.ADMINS);
        addBlockSettingsPermission(profile, BlockSettingType.CONTACT_INFO, PermissionLevel.ADMINS, PermissionLevel.ADMINS);

        UserInfo userInfo = profileService.getProfileInfo(client.getProfileId(), profile.getProfileId());
        assertNotNull(userInfo);
        assertEquals(MartialStatus.NOT_SET, userInfo.getUser().getMartialStatus());

    }

    private ProfileBlockSetting addBlockSettingsPermission(Profile profile, BlockSettingType blockSettingType,
                                                           PermissionLevel viewPermission, PermissionLevel commentPermission) {

        ProfileBlockSetting result = new ProfileBlockSetting();
        result.setProfileId(profile.getProfileId());
        result.setBlockSettingType(blockSettingType);
        result.setViewPermission(viewPermission);
        result.setCommentPermission(commentPermission);

        profileBlockSettingDAO.insert(result);

        return result;
    }

}

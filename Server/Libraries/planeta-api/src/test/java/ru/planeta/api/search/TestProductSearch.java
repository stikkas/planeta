package ru.planeta.api.search;

/*
 * Created by Alexey on 24.08.2016.
 */

import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.sphx.api.SphinxException;
import org.springframework.beans.factory.annotation.Autowired;
import ru.planeta.api.exceptions.NotFoundException;
import ru.planeta.dao.shopdb.ProductDAO;
import ru.planeta.model.shop.Category;
import ru.planeta.model.shop.ProductInfo;
import ru.planeta.test.AbstractTest;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@Ignore
public class TestProductSearch extends AbstractTest {
    @Autowired
    ProductDAO productDAO;

    private static final Logger log = Logger.getLogger(TestCampaignsSearch.class);

    @Test
    public void testProductsSearch() throws SphinxException, NotFoundException {
        ProductInfo productInfo = productUsersService.getProductSafe(1245L);

        Category category1 = new Category();
        Category category2 = new Category();

        category1.setCategoryId(8);
        category2.setCategoryId(5);

        List<ProductInfo> products = searchService.searchForProductsNew(
                new ProductsSearch(null, Collections.singletonList(category1), null, SortOrder.SORT_BY_TIME_ADDED_DESC, 0, 0,  0, 100)
        );

        Assert.assertTrue(productIsFound(productInfo, products));

        products = searchService.searchForProductsNew(
                new ProductsSearch(null, Arrays.asList(category1, category2), null, SortOrder.SORT_BY_PRICE_ASC, 0, 0, 0, 100)
        );

        Assert.assertFalse(productIsFound(productInfo, products));

        products = searchService.searchForProductsNew(
                new ProductsSearch(null, null, Collections.singletonList(20034L), SortOrder.SORT_BY_PRICE_ASC,  0, 0, 0, 100)
        );

        Assert.assertTrue(productIsFound(productInfo, products));

        products = searchService.searchForProductsNew(
                new ProductsSearch(null, null, Arrays.asList(20034L, 25289L), SortOrder.SORT_BY_PRICE_ASC, 0, 0, 0, 100)
        );

        Assert.assertTrue(productIsFound(productInfo, products));

        products = searchService.searchForProductsNew(
                new ProductsSearch("дгьут", null, null, SortOrder.SORT_BY_PRICE_ASC, 0, 0, 0, 100)
        );

        Assert.assertTrue(!products.isEmpty());

        products = searchService.searchForProductsNew(
                new ProductsSearch(null, null, null, SortOrder.SORT_BY_TIME_ADDED_DESC, 0, 0, 0, 100)
        );

        Assert.assertTrue(products.get(0).getTimeAdded().compareTo(products.get(1).getTimeAdded()) == 1);

        products = searchService.searchForProductsNew(
                new ProductsSearch(null, null, null, SortOrder.SORT_BY_TIME_ADDED_ASC, 0, 0, 0, 100)
        );

        Assert.assertTrue(products.get(0).getTimeAdded().compareTo(products.get(1).getTimeAdded()) == -1);
    }

    private static boolean productIsFound(ProductInfo productInfo, List<ProductInfo> products) {
        return products.indexOf(productInfo) != -1;
    }
}

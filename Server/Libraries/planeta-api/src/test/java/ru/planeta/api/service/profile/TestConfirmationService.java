package ru.planeta.api.service.profile;

import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import ru.planeta.model.profile.Profile;
import ru.planeta.test.AbstractTest;

/**
 * Date: 06.07.12
 *
 * @author s.kalmykov
 */
public class TestConfirmationService extends AbstractTest {

	@Autowired
	private ConfirmationServiceImpl confirmationService;

    @Ignore
    @Test
    public void testSmsConfirmationSend() throws Exception {
        Profile profile = insertUserProfile();
        confirmationService.createPhoneConfirmation(profile.getProfileId(), "+79169200139", null);
        confirmationService.createPhoneConfirmation(profile.getProfileId(), "+79169200139", null);
        confirmationService.reSendCode("+79169200139");

        confirmationService.confirm("+79169200139", "12345");
    }
}

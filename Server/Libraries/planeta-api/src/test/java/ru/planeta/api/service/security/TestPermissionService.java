package ru.planeta.api.service.security;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import ru.planeta.api.exceptions.NotFoundException;
import ru.planeta.api.exceptions.PermissionException;
import ru.planeta.model.profile.Profile;
import ru.planeta.test.AbstractTest;

import static org.junit.Assert.*;

public class TestPermissionService extends AbstractTest {

    @Autowired
    private PermissionService permissionService;

    @Test
    public void testUserEditPermissionAccept() {

        Profile firstProfile = insertUserProfile();
        assertTrue(permissionService.isAdmin(firstProfile.getProfileId(), firstProfile.getProfileId()));
    }

    @Test
    public void testUserEditPermissionDenied() {

        Profile firstProfile = insertUserProfile();
        Profile secondProfile = insertUserProfile();
        assertFalse(permissionService.isAdmin(firstProfile.getProfileId(), secondProfile.getProfileId()));
    }


    @Test
    public void testGroupEditPermissionAccept() throws NotFoundException, PermissionException {
        Profile firstProfile = insertUserProfile();
        long groupProfileId = registerSimpleGroup(firstProfile.getProfileId());
        assertTrue(permissionService.isAdmin(firstProfile.getProfileId(), groupProfileId));
    }

    @Test
    public void testGroupEditPermissionDenied() throws NotFoundException, PermissionException {
        Profile firstProfile = insertUserProfile();
        Profile secondProfile = insertUserProfile();
        long groupProfileId = registerSimpleGroup(firstProfile.getProfileId());
        assertFalse(permissionService.isAdmin(secondProfile.getProfileId(), groupProfileId));

    }

}

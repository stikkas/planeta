package ru.planeta.api.service.common;

import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import ru.planeta.api.exceptions.PermissionException;
import ru.planeta.model.common.Banner;
import ru.planeta.model.common.UserPrivateInfo;
import ru.planeta.model.enums.BannerStatus;
import ru.planeta.model.enums.BannerType;
import ru.planeta.model.enums.TargetingType;
import ru.planeta.test.AbstractTest;

import java.util.*;

import static org.junit.Assert.*;

/**
 * User: a.savanovich
 * Date: 18.05.12
 * Time: 17:40
 */
public class TestBannerService extends AbstractTest {

	@Autowired
	private BannerService bannerService;

    @Test
    public void testSelectRandom() throws PermissionException {
        UserPrivateInfo user = insertPlanetaAdminPrivateInfo();
        //
        List<Long> previousId = new LinkedList<>();
        List<Long> assertList = new LinkedList<>();
        //
        for (int i = 0; i < 5; i++) {
            Banner banner = getNewBanner();
            bannerService.save(user.getUserId(), banner);
            assertTrue(banner.getBannerId() > 0);
            if (i < 3) {
                previousId.add(banner.getBannerId());
            } else {
                assertList.add(banner.getBannerId());
            }
        }
        //
        bannerService.reloadFromDb(user.getUserId());
        //
        for (int i = 0; i < 2; i++) {
            Banner banner = bannerService.selectRandom(user.getUserId(), false, BannerType.TEASER, previousId, "/group/CXUIY123123CDSCD/test.html");
            assertNotNull(banner);
            assertTrue(assertList.contains(banner.getBannerId()));
            assertFalse(previousId.contains(banner.getBannerId()));
            previousId.add(banner.getBannerId());
        }
    }

    private static Banner getNewBanner() {
        Banner banner = new Banner();
        banner.setName("name");
        banner.setHtml("html");
        banner.setStatus(BannerStatus.ON);
        banner.setMask("/group/CXUIY123123CDSCD/.*\\.html");
        banner.setType(BannerType.TEASER);
        banner.setShowCounter(Integer.MAX_VALUE);
        banner.setTimeStart(new GregorianCalendar(1990, Calendar.OCTOBER, 10).getTime());
        banner.setTimeFinish(new GregorianCalendar(2030, Calendar.OCTOBER, 10).getTime());
        return banner;
    }

    @Ignore
    @Test
    public void testTargeting() throws PermissionException {
        UserPrivateInfo user = insertPlanetaAdminPrivateInfo();
        Banner banner = getNewBanner();
        banner.setTargetingType(EnumSet.of(TargetingType.AUTHORS, TargetingType.NOT_AUTHORS));
        bannerService.save(user.getUserId(), banner);
        List<Banner> list = bannerService.selectList(user.getUserId(), EnumSet.allOf(BannerStatus.class), 0, 1000);
    }

}

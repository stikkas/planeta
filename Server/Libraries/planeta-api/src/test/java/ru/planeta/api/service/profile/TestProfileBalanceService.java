package ru.planeta.api.service.profile;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import ru.planeta.api.exceptions.NotFoundException;
import ru.planeta.api.exceptions.PermissionException;
import ru.planeta.dao.commondb.CreditTransactionDAO;
import ru.planeta.dao.commondb.DebitTransactionDAO;
import ru.planeta.dao.commondb.ProfileBalanceDAO;
import ru.planeta.model.common.CreditTransaction;
import ru.planeta.model.common.DebitTransaction;
import ru.planeta.test.AbstractTest;

import java.math.BigDecimal;

import static org.junit.Assert.*;

/**
 * User: atropnikov
 * Date: 02.04.12
 * Time: 19:16
 */
public class TestProfileBalanceService extends AbstractTest {

    @Autowired
    private ProfileBalanceDAO profileBalanceDAO;
    @Autowired
    private DebitTransactionDAO debitTransactionDAO;
    @Autowired
    private CreditTransactionDAO creditTransactionDAO;
	@Autowired
	private ProfileBalanceService profileBalanceService;

    @Test
    public void testIncreaseDecrease() throws NotFoundException, PermissionException {
            long profileId = insertUserProfile().getProfileId();
            profileBalanceDAO.insert(profileId);

            try {
                profileBalanceService.decreaseProfileBalance(profileId, BigDecimal.TEN, BigDecimal.ZERO, null);
                assertFalse(true);
            } catch (Exception ex) {
                assertTrue(true);
            }

            BigDecimal amountNet = BigDecimal.TEN;
            BigDecimal amountFee = BigDecimal.ONE;
            BigDecimal resultAmount = amountNet.subtract(amountFee);

            DebitTransaction debitTransaction = profileBalanceService.increaseProfileBalance(profileId, amountNet, amountFee, null);
            assertEquals(profileBalanceDAO.select(profileId).compareTo(resultAmount), 0);
            assertTrue(debitTransaction.getExternalSystemData().isEmpty());

            CreditTransaction creditTransaction = profileBalanceService.decreaseProfileBalance(profileId, amountNet, amountFee, null);
            assertEquals(profileBalanceDAO.select(profileId).compareTo(BigDecimal.ZERO), 0);

            //check for transactions
            DebitTransaction selectedDebitTransaction = debitTransactionDAO.select(debitTransaction.getTransactionId());
            assertNotNull(selectedDebitTransaction);
            assertEquals(selectedDebitTransaction.getProfileId(), profileId);
            assertEquals(selectedDebitTransaction.getAmountFee().compareTo(amountFee), 0);
            assertEquals(selectedDebitTransaction.getAmountNet().compareTo(amountNet), 0);

            CreditTransaction selectedCreditTransaction = creditTransactionDAO.select(creditTransaction.getTransactionId());
            assertNotNull(selectedCreditTransaction);
            assertEquals(selectedCreditTransaction.getProfileId(), profileId);
            assertEquals(selectedCreditTransaction.getAmountFee().compareTo(amountFee), 0);
            assertEquals(selectedCreditTransaction.getAmountNet().compareTo(amountNet), 0);
    }
}

package ru.planeta.api.text;

import org.junit.Assert;
import org.junit.Test;
import ru.planeta.api.Utils;

import java.util.LinkedList;
import java.util.List;
import java.util.regex.Pattern;

public class TestExternalUrls {
    
    @Test
    public void testIsExternal() throws Exception {
        String url1 = "vk.com/planetaru/photos";
        String url2 = "https://vk.com/planetaru/photos";
        String url3 = "https://planeta.ru/planetaru/photos";
        String url4 = "http://vkontakte.com/planetaru/photos";
        String url5 = "/{{= ProfileUtils.getUserLink(profileId, alias)}}";

        List<Pattern> patterns = new LinkedList<Pattern>();
        patterns.add(Pattern.compile("http://vk.com/planetaru.*"));

        Assert.assertFalse(Utils.INSTANCE.isExternalUrlToRedirect(url1, patterns));
        Assert.assertFalse(Utils.INSTANCE.isExternalUrlToRedirect(url2, patterns));
        Assert.assertFalse(Utils.INSTANCE.isExternalUrlToRedirect(url3, patterns));
        Assert.assertTrue(Utils.INSTANCE.isExternalUrlToRedirect(url4, patterns));
        Assert.assertFalse(Utils.INSTANCE.isExternalUrlToRedirect(url5, patterns)) ;

        String html = "<a href=\"https://vk.com/planetaportal/photos\" data-attr='noattr'>\n" +
            "<a data-attr='noattr' href=\"http://vkontakte.com/planetaru/photos\">\n" +
            "<a data-attr='noattr' href=\"http://planeta.ru/any_url/you_want\">\n";
        String result = Utils.INSTANCE.replaceExternalUrls(html, "http://vk.com/away.php", "to", patterns);
        Assert.assertTrue(result.contains("/away.php?to=http%3A%2F%2Fvkontakte.com"));

        String json = "{\"success\":true,\"errorMessage\":null,\"errorClass\":null,\"result\":{\"profileId\":23709,\"voteObjectId\":123151,\"voteObjectType\":\"BLOGPOST\",\"authorProfileId\":23709,\"rating\":0,\"tagIds\":[],\"authorName\":\"kalmykov.sergei\",\"authorAlias\":null,\"authorImageUrl\":\"http://s1.planeta.ru/i/6b32/1336738629527_renamed.jpg\",\"authorGender\":\"MALE\",\"ownerProfileTypeCode\":1,\"ownerName\":\"kalmykov.sergei\",\"ownerAlias\":null,\"ownerImageUrl\":\"http://s1.planeta.ru/i/6b32/1336738629527_renamed.jpg\",\"commentsCount\":0,\"commentsPermission\":\"NONE\",\"lastCommentTimeAdded\":1368547688221,\"title\":\"Попытка задизейблить кнопки отправки 1\",\"headingTextHtml\":\"<a href=\\\"http://stackoverflow.com/questions/3292464/intercept-the-view-response-in-spring-mvc-3\\\">http://stackoverflow.com/questions/3292464/intercept-the-view-response-in-spring-mvc-3</a>\",\"postTextHtml\":\"<p><a href=\\\"http://stackoverflow.com/questions/3292464/intercept-the-view-response-in-spring-mvc-3\\\">http://stackoverflow.com/questions/3292464/intercept-the-view-response-in-spring-mvc-3</a></p>\",\"status\":\"PUBLIC\",\"viewPermission\":\"EVERYBODY\",\"timeAdded\":1366021868572,\"timeUpdated\":1368547796664,\"behalfOfGroup\":false,\"campaignId\":0,\"publicationDateTime\":null,\"tags\":\"\",\"categoryId\":0,\"canComment\":false,\"readCommentsCount\":0,\"objectId\":123151,\"publicBlog\":false,\"blockSettingsType\":\"PHOTO\",\"blockSettingType\":\"BLOG\",\"objectType\":\"BLOGPOST\",\"blogPostId\":123151,\"ownerProfileType\":\"USER\",\"commentsPermissionCode\":0,\"ownerWebAlias\":\"23709\"},\"fieldErrors\":null}";
        String jsonResult = Utils.INSTANCE.replaceExternalUrls(json, "http://vk.com/away.php", "to", patterns);
        Assert.assertTrue(jsonResult.contains("/away.php?to=http%3A%2F%2Fstackoverflow.com"));
    }

}

package ru.planeta.api.service;

import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import ru.planeta.api.service.common.UserCallbackService;
import ru.planeta.test.AbstractTest;

import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicLong;

/**
 * Date: 01.06.2015
 * Time: 15:47
 */
public class NexmoServiceTest extends AbstractTest {
    @Autowired
    private UserCallbackService callbackService;

    private ScheduledExecutorService executorService = Executors.newSingleThreadScheduledExecutor();

    private static final long DELAY = 1000;
    private static final long COUNT = 10;
    private CountDownLatch latch;

    @Test
    @Ignore //only for manual test
    public void testSend() throws Exception {
        latch = new CountDownLatch(1);
        counter = new AtomicLong(COUNT);
        executorService.schedule(new Runner(), DELAY, TimeUnit.MILLISECONDS);
        latch.await();
        System.out.println("sending done");
    }

    private AtomicLong counter;

    private class Runner implements Callable {

        @Override
        public Object call() throws Exception {
            System.out.println("sending message #" + counter.get());
            callbackService.sendSmsToManager("+7 (999) 123456789", "200500", counter.getAndAdd(-1));
            System.out.println(" ...done!");
            if (counter.get() > 0) {
                executorService.schedule(new Runner(), DELAY, TimeUnit.MILLISECONDS);
            }
            if (counter.get() == 0) {
                latch.countDown();
            }
            return true;
        }
    }
}

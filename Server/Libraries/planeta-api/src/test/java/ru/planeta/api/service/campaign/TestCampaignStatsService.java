package ru.planeta.api.service.campaign;


import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import ru.planeta.api.exceptions.NotFoundException;
import ru.planeta.api.exceptions.PermissionException;
import ru.planeta.model.common.campaign.CampaignSearchFilter;
import ru.planeta.reports.ReportType;
import ru.planeta.test.AbstractTest;

import java.util.Date;

public class TestCampaignStatsService extends AbstractTest {

    @Autowired
    private CampaignStatsService campaignStatsService;

    @Test
    public void testGetReport(){
        CampaignSearchFilter filter = new CampaignSearchFilter();
        filter.setCampaignId(22152);

        try {
            long t = new Date().getTime();
            campaignStatsService.getReport(111452,ReportType.CSV,filter);
            System.out.println("done in "+(new Date().getTime() - t));
        } catch (NotFoundException | PermissionException ignored) {
        }
    }

}

package ru.planeta.api.search;

/*
 * Created by Alexey on 24.08.2016.
 */

import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.Test;
import org.sphx.api.SphinxException;
import ru.planeta.api.exceptions.NotFoundException;
import ru.planeta.model.common.campaign.CampaignTag;
import ru.planeta.model.common.campaign.ShareForSearch;
import ru.planeta.test.AbstractTest;
import java.util.ArrayList;
import java.util.List;

public class TestSharesSearch extends AbstractTest {

    private static final Logger log = Logger.getLogger(TestCampaignsSearch.class);

    @Test
    public void testSharesSearch() throws SphinxException, NotFoundException {
        SearchShares searchShares = new SearchShares();

        CampaignTag campaignTag = new CampaignTag();
        CampaignTag campaignTag2 = new CampaignTag();

        campaignTag.setId(1);
        campaignTag2.setId(2);

        List<CampaignTag> tags = new ArrayList<CampaignTag>();

        tags.add(campaignTag);
        tags.add(campaignTag2);

        searchShares.setCampaignTags(tags);

        List<SortOrder> sortOrder = new ArrayList<SortOrder>();
        sortOrder.add(SortOrder.SORT_BY_PRICE_DESC);

        searchShares.setSortOrderList(sortOrder);


        SearchResult<ShareForSearch> shares = searchService.searchForShares(searchShares);

        Assert.assertTrue(shares.getSearchResultRecords().size() > 0);
    }
}

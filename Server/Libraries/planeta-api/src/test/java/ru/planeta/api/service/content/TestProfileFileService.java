package ru.planeta.api.service.content;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import ru.planeta.api.exceptions.PermissionException;
import ru.planeta.model.common.UserPrivateInfo;
import ru.planeta.model.profile.Profile;
import ru.planeta.model.profile.ProfileFile;
import ru.planeta.test.AbstractTest;

import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * @author: ds.kolyshev
 * Date: 17.03.13
 */
public class TestProfileFileService extends AbstractTest {

	@Autowired ProfileFileService profileFileService;

	@Test
	public void testProfileFileService() throws PermissionException {
		UserPrivateInfo client = insertPlanetaAdminPrivateInfo();
		Profile profile = insertUserProfile();

		long clientId = client.getUserId();
		long profileId = profile.getProfileId();
		ProfileFile profileFile = createProfileFile(profileId, clientId);
		profileFileService.addProfileFile(clientId, profileFile);
		assertTrue(profileFile.getFileId() > 0);

		ProfileFile selected = profileFileService.getProfileFile(clientId, profileId, profileFile.getFileId());
		assertFilesEquals(profileFile, selected);

		List<ProfileFile> list = profileFileService.getProfileFiles(clientId, profileId, 0, 0);
		assertNotNull(list);
		assertEquals(1, list.size());
		ProfileFile inserted = list.get(0);
		assertFilesEquals(profileFile, inserted);
	}

	private void assertFilesEquals(ProfileFile expected, ProfileFile actual) {
		assertNotNull(actual);
		assertEquals(expected.getFileId(), actual.getFileId());
		assertEquals(expected.getAuthorId(), actual.getAuthorId());
		assertEquals(expected.getDescription(), actual.getDescription());
		assertEquals(expected.getFileUrl(), actual.getFileUrl());
		assertEquals(expected.getName(), actual.getName());
		assertEquals(expected.getProfileId(), actual.getProfileId());
		assertEquals(expected.getTimeAdded(), actual.getTimeAdded());
		assertEquals(expected.getTimeUpdated(), actual.getTimeUpdated());
		assertEquals(expected.getSize(), actual.getSize());
		assertEquals(expected.getExtension(), actual.getExtension());
	}

	private ProfileFile createProfileFile(long ownerId, long authorId) {
		ProfileFile profileFile = new ProfileFile();
		profileFile.setProfileId(ownerId);
		profileFile.setAuthorId(authorId);
		profileFile.setTimeAdded(new Date());
		profileFile.setTimeUpdated(new Date());
		profileFile.setDescription("Description");
		profileFile.setName("Name");
		profileFile.setFileUrl("http://files.com/file.ext");
		profileFile.setSize(112323);
		profileFile.setExtension("ext");

		return profileFile;
	}
}

package ru.planeta.api.faq;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import ru.planeta.api.service.faq.FaqArticleService;
import ru.planeta.api.service.faq.FaqParagraphService;
import ru.planeta.model.commondb.FaqArticle;
import ru.planeta.model.commondb.FaqParagraph;
import ru.planeta.test.AbstractTest;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * Created by kostiagn on 29.12.2015.
 */
public class TestFaqParagraphService extends AbstractTest {

    @Autowired
    private FaqArticleService faqArticleService;
    @Autowired
    private FaqParagraphService faqParagraphService;

    private static final long TEST_CATEGORY_ID = 100L;



    @Test
    public void testResort() {
        final Long faqArticleId = insertFaqArticle().getFaqArticleId();

        List<FaqParagraph> list = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            list.add(insertFaqParagraph(faqArticleId));
        }

        for (int i = 0; i < list.size(); i++) {
            FaqParagraph faqParagraph= list.get(i);
            assertEquals((Integer) (i + 1), faqParagraph.getOrderNum());
        }


        resortAndCheck(1, 3, list);
        resortAndCheck(3, 1, list);
        resortAndCheck(0, 3, list);
        resortAndCheck(3, 0, list);
        resortAndCheck(2, 4, list);
        resortAndCheck(4, 2, list);
        resortAndCheck(0, 4, list);
        resortAndCheck(4, 0, list);

    }

    private void resortAndCheck(int indexFrom, int indexTo, List<FaqParagraph> list) {
        final Long faqArticleId = list.get(indexFrom).getFaqArticleId();
        faqParagraphService.resort(list.get(indexFrom).getFaqParagraphId(), list.get(indexTo).getFaqParagraphId(), faqArticleId);
        FaqParagraph removed = list.remove(indexFrom);
        list.add(indexTo, removed);

        final List<FaqParagraph> selectedList = faqParagraphService.selectFaqParagraphListByFaqArticle(faqArticleId, 0, 0);
        for (int i = 0; i < list.size(); i++) {
            assertEquals(list.get(i).getFaqParagraphId(), selectedList.get(i).getFaqParagraphId());
            assertEquals((Integer) (i + 1), selectedList.get(i).getOrderNum());
        }
    }


    private FaqArticle insertFaqArticle() {
        final FaqArticle faqArticle = FaqArticle.Companion.getBuilder().faqCategoryId(TEST_CATEGORY_ID).title("faq article #" + System.currentTimeMillis()).build();
        faqArticleService.insertFaqArticle(faqArticle);
        return faqArticle;
    }
    private FaqParagraph insertFaqParagraph(long faqArticleId) {
        final FaqParagraph faqParagraph = FaqParagraph.Companion.getBuilder()
                .faqArticleId(faqArticleId)
                .title("faq paragraph title " + System.currentTimeMillis())
                .annotation("faq paragraph annotation " + System.currentTimeMillis())
                .textHtml("faq paragraph text html " + System.currentTimeMillis())
                .build();
        faqParagraphService.insertFaqParagraph(faqParagraph);
        return faqParagraph;
    }
}

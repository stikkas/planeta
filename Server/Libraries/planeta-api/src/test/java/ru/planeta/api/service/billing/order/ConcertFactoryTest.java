package ru.planeta.api.service.billing.order;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import ru.planeta.model.enums.OrderObjectType;
import ru.planeta.test.AbstractTest;

import static org.junit.Assert.*;

/**
 * Created by a.savanovich on 19.10.2016.
 */
public class ConcertFactoryTest extends AbstractTest {

    @Autowired
    private AfterFundServiceFactory afterFundServiceFactory;

    @Test
    public void test() {
        AfterFundService concertService = afterFundServiceFactory.get(OrderObjectType.TICKET);
        assertNull(concertService);
    }
}

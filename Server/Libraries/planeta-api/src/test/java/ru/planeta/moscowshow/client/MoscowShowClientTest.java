package ru.planeta.moscowshow.client;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.CollectionUtils;
import ru.planeta.moscowshow.model.result.ResultActionSchema;
import ru.planeta.moscowshow.model.ActionStructure;
import ru.planeta.moscowshow.model.OrderItem;
import ru.planeta.moscowshow.model.OrderPlaceResult;
import ru.planeta.moscowshow.model.Place;
import ru.planeta.moscowshow.model.response.ResponseActionList;
import ru.planeta.moscowshow.model.response.ResponseActionSchema;
import ru.planeta.moscowshow.model.response.ResponseCancelOrder;
import ru.planeta.moscowshow.model.response.ResponseCancelOrderByNumbers;
import ru.planeta.moscowshow.model.response.ResponsePayTicket;
import ru.planeta.moscowshow.model.response.ResponsePayTickets;
import ru.planeta.moscowshow.model.response.ResponsePlaceOrder;
import ru.planeta.moscowshow.model.response.ResponseSoldTickets;
import ru.planeta.moscowshow.model.response.ResponseUnpayTicket;
import ru.planeta.moscowshow.model.response.ResponseUnpayTickets;
import ru.planeta.moscowshow.model.StructElement;
import ru.planeta.test.AbstractTest;

/**
 *
 * User: Serge Blagodatskih<stikkas17@gmail.com><br>
 * Date: 12.10.16<br>
 * Time: 16:24
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:spring/applicationContext-*.xml", "classpath*:/spring/applicationContext-dao.xml",
    "classpath*:/spring/applicationContext-geo.xml"})
@Ignore
public class MoscowShowClientTest extends AbstractTest {

//  Comment out before run tests
//    @Autowired
    private MoscowShowClient moscowShowClient;

    @Before
    public void init() {
        Assert.assertNotNull(moscowShowClient);
    }

    @Ignore
    @Test
    public void testListAction() throws InterruptedException {
        for (ActionStructure action : getActions()) {
            System.out.println(action);
        }
    }

    @Ignore
    @Test
    public void testActionSchema() {
        ResultActionSchema schema = getSchema(null);
        System.out.println(schema.getHallName() + ": " + schema.getHallAddress() + ": " + schema.getImageUrl());
        List<Place> places = schema.getPlaces();
        if (!CollectionUtils.isEmpty(places)) {
            System.out.println("Schema has " + places.size() + " places");
            int i = 0;
            for (Place place : places) {
                if (++i > 5) {
                    break;
                }
                System.out.println(place);
            }
        } else {
            System.out.println("places are not available");
        }
        List<StructElement> sections = schema.getSectionsAndRows();
        if (!CollectionUtils.isEmpty(sections)) {
            System.out.println("Schema has " + sections.size() + " sections and rows");
            int i = 0;
            for (StructElement element : sections) {
                if (++i > 5) {
                    break;
                }
                System.out.println(element);
            }
        } else {
            System.out.println("sections and rows are not available");
        }
    }

    @Ignore
    @Test
    public void testPlaceOrder() {
        List<ActionStructure> actions = getActions();
        if (actions.isEmpty()) {
            return;
        }
        Long actionId = actions.get(0).getId();
        ResultActionSchema schema = getSchema(actionId);
        List<OrderItem> itemsToOrder = new ArrayList<>();
        List<Place> places = schema.getPlaces();
        if (!places.isEmpty()) {
            int i = 0;
            for (Place place : places) {
                if (place.getState() == 0) {
                    itemsToOrder.add(new OrderItem(actionId, place.getSectionId(), place.getRowId(),
                            place.getPosition(), place.getPrice()));
                    if (++i > 1) {
                        break;
                    }
                }
            }
        } else {
            System.out.println("places are not available");
        }

        System.out.println("ATTEMP ORDER " + itemsToOrder.size() + " places");
        ResponsePlaceOrder rpo = moscowShowClient.placeOrder(itemsToOrder);
        if (rpo.hasErrors()) {
            System.out.println("ERROR PLACE ORDER: " + rpo.getErrorMessage());
        } else {
            List<OrderPlaceResult> orders = rpo.getResult().getPlaces();
            if (orders.isEmpty()) {
                System.out.println("WHY PLACE ORDERS is EMPTY?");
            } else {
                for (OrderPlaceResult order : orders) {
                    System.out.println(order);
                }
            }
        }

    }

    @Ignore
    @Test
    public void testCancelOrder() {
        List<ActionStructure> actions = getActions();
        if (actions.isEmpty()) {
            return;
        }
        Long actionId = actions.get(0).getId();
        ResultActionSchema schema = getSchema(actionId);
        List<OrderItem> itemsToCancel = new ArrayList<>();
        List<Place> places = schema.getPlaces();
        if (!places.isEmpty()) {
            for (Place place : places) {
                if (place.getState() == 3) {
                    System.out.println(place);
                    itemsToCancel.add(new OrderItem(actionId, place.getSectionId(), place.getRowId(),
                            place.getPosition(), place.getPrice()));
                }
            }
        } else {
            System.out.println("places are not available");
        }

        System.out.println("ATTEMP CANCEL " + itemsToCancel.size() + " places");
        ResponseCancelOrder rco = moscowShowClient.cancelOrder(itemsToCancel);
        if (rco.hasErrors()) {
            System.out.println("ERROR CANCEL ORDERS: " + rco.getErrorMessage());
        } else {
            System.out.println("SUCCESS CANCEL ORDERS");
        }

    }

    @Ignore
    @Test
    public void testCancelOrderByNumbers() {
        List<ActionStructure> actions = getActions();
        if (actions.isEmpty()) {
            return;
        }
        Long actionId = actions.get(0).getId();
        ResultActionSchema schema = getSchema(actionId);
        List<OrderItem> itemsToOrder = new ArrayList<>();
        List<Place> places = schema.getPlaces();
        int freePlaces = 0;
        if (!places.isEmpty()) {
            int i = 0;
            for (Place place : places) {
                if (place.getState() == 0) {
                    if (i < 2) {
                        itemsToOrder.add(new OrderItem(actionId, place.getSectionId(), place.getRowId(),
                                place.getPosition(), place.getPrice()));
                        ++i;
                    }
                    ++freePlaces;
                }
            }
        } else {
            System.out.println("places are not available");
        }

        System.out.println("ATTEMPT ORDER " + itemsToOrder.size() + " places");
        ResponsePlaceOrder rpo = moscowShowClient.placeOrder(itemsToOrder);
        if (rpo.hasErrors()) {
            System.out.println("ERROR PLACE ORDER: " + rpo.getErrorMessage());
        } else {
            List<OrderPlaceResult> orders = rpo.getResult().getPlaces();
            List<String> numbers = new ArrayList<>();
            for (OrderPlaceResult order : orders) {
                System.out.println(order);
                numbers.add(order.getTicketCode());
            }
            System.out.println("ATTEMPT CANCEL ORDERS BY NUMBERS");
            ResponseCancelOrderByNumbers rcobn = moscowShowClient.cancelOrderByNumbers(numbers);
            if (rcobn.hasErrors()) {
                System.out.println("BY NUMBERS ERROR: " + rcobn.getErrorMessage());
            } else {
                // Проверяем кол-во свободных мест
                for (Place place : getSchema(actionId).getPlaces()) {
                    if (place.getState() == 0) {
                        --freePlaces;
                    }
                }
                Assert.assertTrue(freePlaces == 0);
            }
        }
    }

    @Ignore
    @Test
    public void testPayTicket() {
        List<ActionStructure> actions = getActions();
        if (actions.isEmpty()) {
            return;
        }
        Long actionId = actions.get(0).getId();
        ResultActionSchema schema = getSchema(actionId);
        List<OrderItem> itemsToOrder = new ArrayList<>();
        List<Place> places = schema.getPlaces();
        int paidPlaces = 0;
        if (!places.isEmpty()) {
            int i = 0;
            for (Place place : places) {
                if (place.getState() == 0) {
                    if (i < 1) {
                        itemsToOrder.add(new OrderItem(actionId, place.getSectionId(), place.getRowId(),
                                place.getPosition(), place.getPrice()));
                        ++i;
                    }
                } else if (place.getState() == 1) {
                    ++paidPlaces;
                }
            }
        } else {
            System.out.println("places are not available");
        }

        ResponsePlaceOrder rpo = moscowShowClient.placeOrder(itemsToOrder);
        if (rpo.hasErrors()) {
            System.out.println("ERROR PLACE ORDER: " + rpo.getErrorMessage());
        } else {
            List<OrderPlaceResult> orders = rpo.getResult().getPlaces();
            List<Long> ticketIds = new ArrayList<>();
            for (OrderPlaceResult order : orders) {
                System.out.println(order);
                ticketIds.add(order.getPlaceID());
            }
            Assert.assertTrue(ticketIds.size() == 1);
            System.out.println("ATTEMPT PAY ORDER: " + ticketIds.get(0));
            ResponsePayTicket rpt = moscowShowClient.payTicket(ticketIds.get(0));
            if (rpt.hasErrors()) {
                System.out.println("PAY ERROR: " + rpt.getErrorMessage());
            } else {
                // Проверяем кол-во оплаченых мест
                for (Place place : getSchema(actionId).getPlaces()) {
                    if (place.getState() == 1) {
                        --paidPlaces;
                    }
                }
                Assert.assertTrue(paidPlaces == -1);
            }
        }
    }

    @Ignore
    @Test
    public void testPayTickets() {
        List<ActionStructure> actions = getActions();
        if (actions.isEmpty()) {
            return;
        }
        Long actionId = actions.get(0).getId();
        ResultActionSchema schema = getSchema(actionId);
        List<OrderItem> itemsToOrder = new ArrayList<>();
        List<Place> places = schema.getPlaces();
        int paidPlaces = 0;
        if (!places.isEmpty()) {
            int i = 0;
            for (Place place : places) {
                if (place.getState() == 0) {
                    if (i < 2) {
                        itemsToOrder.add(new OrderItem(actionId, place.getSectionId(), place.getRowId(),
                                place.getPosition(), place.getPrice()));
                        ++i;
                    }
                } else if (place.getState() == 1) {
                    ++paidPlaces;
                }
            }
        } else {
            System.out.println("places are not available");
        }

        ResponsePlaceOrder rpo = moscowShowClient.placeOrder(itemsToOrder);
        if (rpo.hasErrors()) {
            System.out.println("ERROR PLACE ORDER: " + rpo.getErrorMessage());
        } else {
            List<OrderPlaceResult> orders = rpo.getResult().getPlaces();
            List<Long> ticketIds = new ArrayList<>();
            for (OrderPlaceResult order : orders) {
                System.out.println(order);
                ticketIds.add(order.getPlaceID());
            }
            Assert.assertTrue(ticketIds.size() == 2);
            ResponsePayTickets rpts = moscowShowClient.payTickets(ticketIds);
            if (rpts.hasErrors()) {
                System.out.println("PAY TICKETS ERROR: " + rpts.getErrorMessage());
            } else {
                // Проверяем кол-во оплаченых мест
                for (Place place : getSchema(actionId).getPlaces()) {
                    if (place.getState() == 1) {
                        --paidPlaces;
                    }
                }
                Assert.assertTrue(paidPlaces == -2);
            }
        }
    }

    @Ignore
    @Test
    public void testUnpayTicket() {
        List<ActionStructure> actions = getActions();
        if (actions.isEmpty()) {
            return;
        }
        Long actionId = actions.get(0).getId();
        ResultActionSchema schema = getSchema(actionId);
        List<OrderItem> itemsToOrder = new ArrayList<>();
        List<Place> places = schema.getPlaces();
        int paidPlaces = 0;
        if (!places.isEmpty()) {
            int i = 0;
            for (Place place : places) {
                if (place.getState() == 0) {
                    if (i < 1) {
                        itemsToOrder.add(new OrderItem(actionId, place.getSectionId(), place.getRowId(),
                                place.getPosition(), place.getPrice()));
                        ++i;
                    }
                } else if (place.getState() == 1) {
                    ++paidPlaces;
                }
            }
        } else {
            System.out.println("places are not available");
        }

        ResponsePlaceOrder rpo = moscowShowClient.placeOrder(itemsToOrder);
        if (rpo.hasErrors()) {
            System.out.println("ERROR PLACE ORDER FOR UNPAY: " + rpo.getErrorMessage());
        } else {
            List<OrderPlaceResult> orders = rpo.getResult().getPlaces();
            List<Long> ticketIds = new ArrayList<>();
            for (OrderPlaceResult order : orders) {
                ticketIds.add(order.getPlaceID());
            }
            Assert.assertTrue(ticketIds.size() == 1);
            ResponsePayTicket rpt = moscowShowClient.payTicket(ticketIds.get(0));
            if (rpt.hasErrors()) {
                System.out.println("PAY ERROR FOR UNPAY: " + rpt.getErrorMessage());
            } else {
                ResponseUnpayTicket rut = moscowShowClient.unpayTicket(ticketIds.get(0));
                if (rut.hasErrors()) {
                    System.out.println("UNPAY ERROR: " + rpt.getErrorMessage());

                } else {
                    // Проверяем кол-во оплаченых мест
                    for (Place place : getSchema(actionId).getPlaces()) {
                        if (place.getState() == 1) {
                            --paidPlaces;
                        }
                    }
                    Assert.assertTrue(paidPlaces == 0);
                }
            }
        }
    }

    @Ignore
    @Test
    public void testUnpayTicketsAndListSoldTickets() {
        ResponseSoldTickets rst = moscowShowClient.listSoldTickets();
        if (rst.hasErrors()) {
            System.out.println("SOLD TICKETS: " + rst.getErrorMessage());
        } else {
            List<Long> soldIds = new ArrayList<>();
            for (OrderPlaceResult pr : rst.getResult().getPlaces()) {
                soldIds.add(pr.getPlaceID());
            }
            if (soldIds.isEmpty()) {
                System.out.println("WE DON'T HAVE SOLD TICKETS");
            } else {
                ResponseUnpayTickets ruts = moscowShowClient.unpayTickets(soldIds);
                if (ruts.hasErrors()) {
                    System.out.println("UNPAY TICKETS ERROR: " + ruts.getErrorMessage());
                } else {
                    Assert.assertTrue(moscowShowClient.listSoldTickets().getResult().getPlaces().isEmpty());
                }
            }
        }
    }

    private List<ActionStructure> getActions() {
        ResponseActionList response = moscowShowClient.listActions();
        if (response.hasErrors()) {
            System.out.println("ACTIONS ERROR: " + response.getErrorMessage());
            return Collections.EMPTY_LIST;
        } else {
            List<ActionStructure> actions = response.getResult().getActions();
            Assert.assertNotNull(actions);
            if (actions.isEmpty()) {
                System.out.println("actions are not available");
            }
            return actions;
        }
    }

    private ResultActionSchema getSchema(Long actionId) {
        if (actionId == null) {
            List<ActionStructure> actions = getActions();
            if (!actions.isEmpty()) {
                actionId = actions.get(0).getId();
            }
        }
        ResponseActionSchema schemaResponse = moscowShowClient.actionSchema(actionId);
        if (schemaResponse.hasErrors()) {
            System.out.println("ERROR SCHEMA: " + schemaResponse.getErrorMessage());
        }
        return schemaResponse.getResult();
    }

}

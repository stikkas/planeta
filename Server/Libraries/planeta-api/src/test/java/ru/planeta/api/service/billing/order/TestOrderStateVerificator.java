package ru.planeta.api.service.billing.order;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import ru.planeta.api.exceptions.BaseException;
import ru.planeta.api.exceptions.NotFoundException;
import ru.planeta.api.exceptions.OrderException;
import ru.planeta.api.exceptions.PermissionException;
import ru.planeta.model.common.Order;
import ru.planeta.model.common.OrderObject;
import ru.planeta.model.common.campaign.Share;
import ru.planeta.model.common.TransactionalObject;
import ru.planeta.model.shop.enums.PaymentStatus;
import ru.planeta.test.AbstractTest;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.fail;

/**
 *
 * Created by eshevchenko on 04.02.14.
 */
public class TestOrderStateVerificator extends AbstractTest {

    @Autowired
    private OrderStateVerifier validator;
    @Autowired
    private OrderService orderService;

    @Test
    public void test() throws NotFoundException, PermissionException, OrderException {
        final long buyerId = getProfileId(registerUser());
        final long adminId = getProfileId(registerSuperAdminUser());
        final long merchantId = registerNewOfficialGroup(adminId);
        final long nonMerchantId = registerSimpleGroup(adminId);

        Share share = createShareWithCampaign(adminId, merchantId, BigDecimal.TEN, 10);

        Order order = orderService.createOrderWithShare(buyerId, buyerId, share.getShareId(), null, 5, getTestStr("answer"), null, null, null);
        final long orderId = order.getOrderId();
        List<OrderObject> orderObjects = orderService.getOrderObjects(orderId);

        validator.verify(order, orderObjects);

        try {
            validator.verify(order, new ArrayList<OrderObject>());
            fail("Empty order objects.");
        } catch (BaseException e) {
            e.printStackTrace();
        }

        try {
            for (OrderObject orderObject : orderObjects) {
                orderObject.setMerchantId(nonMerchantId);
            }
            validator.verify(order, orderObjects);
            // fail("Non merchant owner.");
        } catch (BaseException e) {
            for (OrderObject orderObject : orderObjects) {
                orderObject.setMerchantId(merchantId);
            }
            e.printStackTrace();
        }

        try {
            order.setPaymentStatus(PaymentStatus.COMPLETED);
            validator.verify(order, orderObjects);
            fail("Payment status not applicable for money transactions.");
        } catch (BaseException e) {
            order.setPaymentStatus(PaymentStatus.PENDING);
            e.printStackTrace();
        }

        try {
            setMoneyTransactions(order, 1L);
            for (OrderObject orderObject : orderObjects) {
                setMoneyTransactions(orderObject, 1L);
            }
            validator.verify(order, orderObjects);
            fail("Payment status not applicable for money transactions.");
        } catch (BaseException e) {
            setMoneyTransactions(order, 0L);
            for (OrderObject orderObject : orderObjects) {
                setMoneyTransactions(orderObject, 0L);
            }
            e.printStackTrace();
        }
    }

    private static void setMoneyTransactions(TransactionalObject transactionalObject, long value) {
        transactionalObject.setCreditTransactionId(value);
        transactionalObject.setDebitTransactionId(value);
        transactionalObject.setCancelCreditTransactionId(value);
        transactionalObject.setCancelDebitTransactionId(value);
    }
}

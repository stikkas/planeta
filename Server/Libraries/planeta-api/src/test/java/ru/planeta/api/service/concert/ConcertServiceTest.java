package ru.planeta.api.service.concert;

import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import ru.planeta.api.exceptions.MoscowShowInteractionException;
import ru.planeta.api.exceptions.NotFoundException;
import ru.planeta.commons.web.WebUtils;
import ru.planeta.test.AbstractTest;

import java.io.ByteArrayInputStream;
import java.nio.charset.StandardCharsets;

/**
 * Created with IntelliJ IDEA.
 * User: michail
 * Date: 14.10.16
 * Time: 18:34
 */
@Ignore
public class ConcertServiceTest extends AbstractTest {
    @Autowired
    ConcertService concertService;

    @Autowired
    ConcertImportService concertImportService;

    @Ignore
    @Test
    public void testMoscowShowConcertsExport() throws NotFoundException, MoscowShowInteractionException {
        concertImportService.importMoscowShowConcerts();
    }

    @Ignore
    @Test
    public void testPlaceController() {
        String url = "http://localhost:8101/api/public/place-order.json";
        String body = "{\"email\":\"dsfasdf@asgsdfgdsfg.fg\"," +
                " \"orderItemList\": [{" +
                "    \"actionID\": \"1\", " +
                "    \"sectionID\": \"2\"," +
                "    \"rowID\": \"3\"," +
                "    \"position\": \"4\"," +
                "    \"price\": \"5\"" +
                "  }, {" +
                "    \"actionID\": \"6\"," +
                "    \"sectionID\": \"7\"," +
                "    \"rowID\": \"8\"," +
                "    \"position\": \"9\"," +
                "    \"price\": \"0\"" +
                "  }]}";

        byte[] respStream = WebUtils.upload(url, new ByteArrayInputStream(body.getBytes(StandardCharsets.UTF_8)), "application/json", false);
    }
}

package ru.planeta.api.log;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.spi.Filter;
import org.apache.log4j.spi.LoggingEvent;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import ru.planeta.test.AbstractTest;

import javax.annotation.PostConstruct;

import static org.junit.Assert.assertEquals;

/**
 * @author p.vyazankin
 * @since 3/2/13 6:49 PM
 */
public class TestErrorAccumulatorFilter extends AbstractTest {

    @Autowired
    private ErrorAccumulatorFilter errorAccumulatorFilter;

    private static final Logger log = Logger.getLogger(TestErrorAccumulatorFilter.class);


    @PostConstruct
    public void init() {
        if (errorAccumulatorFilter == null) {
            errorAccumulatorFilter = ErrorAccumulatorFilter.Companion.getInstance();
        }
        if (errorAccumulatorFilter == null) {
            errorAccumulatorFilter = new ErrorAccumulatorFilter();
        }
    }

    @Test
    public void testNeverDecline() throws Exception {
        assertEquals(Filter.ACCEPT, errorAccumulatorFilter.decide(new LoggingEvent("", log, Level.ERROR, "message", new Exception())));
        assertEquals(Filter.ACCEPT, errorAccumulatorFilter.decide(new LoggingEvent("", log, Level.INFO, "message", new Exception())));
        assertEquals(Filter.ACCEPT, errorAccumulatorFilter.decide(new LoggingEvent("", log, Level.ALL, "message", new Exception())));
        assertEquals(Filter.ACCEPT, errorAccumulatorFilter.decide(new LoggingEvent("", log, Level.DEBUG, "message", new Exception())));
        assertEquals(Filter.ACCEPT, errorAccumulatorFilter.decide(new LoggingEvent("", log, Level.FATAL, "message", new Exception())));
        assertEquals(Filter.ACCEPT, errorAccumulatorFilter.decide(new LoggingEvent("", log, Level.OFF, "message", new Exception())));
        assertEquals(Filter.ACCEPT, errorAccumulatorFilter.decide(new LoggingEvent("", log, Level.TRACE, "message", new Exception())));
        assertEquals(Filter.ACCEPT, errorAccumulatorFilter.decide(new LoggingEvent("", log, Level.WARN, "message", new Exception())));
    }

    @Test   @Ignore
    public void testSmtpAppenderNothingToSend() throws Exception {
        log.debug("debug");
        log.info("info");
        log.warn("warn");
        log.trace("trace");
    }

    @Test   @Ignore // There are no test mailbox, so enable this test on local machine only. Change target mail address in MailClientImpl too.
    public void testSmtpAppenderSend() throws Exception {
        try {
            throw new RuntimeException("This is test RuntimeException");
        } catch (RuntimeException e) {
            log.error("test error", e);
            log.fatal("test fatal", e);
            log.error("test error", e);
            log.fatal("test fatal", e);
            log.fatal("unique test fatal", e);
        }
    }
}

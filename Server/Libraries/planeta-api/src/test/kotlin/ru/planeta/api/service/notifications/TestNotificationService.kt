package ru.planeta.api.service.notifications

import org.junit.Ignore
import org.junit.Test
import org.springframework.beans.factory.annotation.Autowired
import ru.planeta.api.Utils
import ru.planeta.api.mail.MailClientImpl
import ru.planeta.commons.model.Gender
import ru.planeta.model.common.Address
import ru.planeta.model.common.OrderInfo
import ru.planeta.model.common.OrderObjectInfo
import ru.planeta.model.common.delivery.BaseDelivery
import ru.planeta.model.common.delivery.LinkedDelivery
import ru.planeta.model.mail.MailMessagePriority
import ru.planeta.model.profile.Post
import ru.planeta.model.promo.TechnobattleRegistration
import ru.planeta.model.shop.enums.DeliveryType
import ru.planeta.model.shop.enums.PaymentType
import ru.planeta.test.AbstractTest
import java.math.BigDecimal
import java.util.*

import org.junit.Assert.assertEquals
import ru.planeta.api.Utils.smap
import ru.planeta.api.service.notifications.NotificationServiceImpl.Companion.SHARE_PURCHASE_EMAIL_TEMPLATE
import ru.planeta.api.service.notifications.NotificationServiceImpl.Companion.USER_BECAME_VIP
import ru.planeta.api.service.notifications.NotificationServiceImpl.Companion.CAMPAIGN_BECAME_VIP_OFFERS_ALLOWED
import ru.planeta.api.service.notifications.NotificationServiceImpl.Companion.ORDER_SENT_TEMPLATE
import ru.planeta.api.service.notifications.NotificationServiceImpl.Companion.ORDER_GIVEN_TEMPLATE
import ru.planeta.api.service.notifications.NotificationServiceImpl.Companion.ORDER_PURCHASED_TEMPLATE
import ru.planeta.api.service.notifications.NotificationServiceImpl.Companion.BIBLIO_ORDER_PURCHASED_TEMPLATE
import ru.planeta.api.service.notifications.NotificationServiceImpl.Companion.SECRET_KEY
import ru.planeta.api.service.notifications.NotificationServiceImpl.Companion.BIBIIO_PURCHASE_LIBRARY_NOTIFICATION
import ru.planeta.api.service.notifications.NotificationServiceImpl.Companion.BIBIIO_PURCHASE_BOOK_NOTIFICATION
import ru.planeta.api.service.notifications.NotificationServiceImpl.Companion.BIBIIO_PURCHASE_RUSSIANPOST_BOOK_NOTIFICATION
import ru.planeta.api.service.notifications.NotificationServiceImpl.Companion.AUTHOR_NOTIFICATION_TEMPLATE_FILE_NEW
import ru.planeta.api.service.notifications.NotificationServiceImpl.Companion.CAMPAIGN_ENDS_SOON
import ru.planeta.api.service.notifications.NotificationServiceImpl.Companion.INTERACTIVE_CAMPAIGN_FINISHED
import ru.planeta.api.service.notifications.NotificationServiceImpl.Companion.ERRCOUNT
import ru.planeta.api.service.notifications.NotificationServiceImpl.RichTemplateType.NEW_BLOG_POST_IN_CAMPAIGN
import ru.planeta.commons.web.WebUtils.encodeUrl

/**
 * User: a.savanovich
 * Date: 09.01.14
 * Time: 16:30
 */

class TestNotificationService : AbstractTest() {

    @Autowired
    lateinit var notificationService: NotificationServiceImpl

    @Autowired
    lateinit var notificationTemplateService: NotificationTemplateService

    @Autowired
    lateinit var mailClient: MailClientImpl

    private val address: Address
        get() {
            val address = Address()
            address.zipCode = "zip123"
            address.city = "city"
            address.street = "street"
            address.street = "phone"
            return address
        }

    @Test
    @Ignore
    @Throws(Exception::class)
    fun test() {

        val video11 = "<p:video duration=\"95\" id=\"46044\" name=\"Трейлер TV-версии фильма &laquo;Медведи Камчатки. Начало жизни&raquo;\" owner=\"195268\" image=\"http://s2.planeta.ru/p?url=https%3A%2F%2Fi.ytimg.com%2Fvi%2FdfXpHdty2pw%2Fsddefault.jpg&amp;width=400&amp;height=300&amp;pad=true\"></p:video>"
        val video21 = "<a target=\"_blank\" href=\"##blogPostUrl##\"><img src=\"https://s1.dev.planeta.ru/video-stub.jpg?duration=95000&name=%D0%A2%D1%80%D0%B5%D0%B9%D0%BB%D0%B5%D1%80+TV-%D0%B2%D0%B5%D1%80%D1%81%D0%B8%D0%B8+%D1%84%D0%B8%D0%BB%D1%8C%D0%BC%D0%B0+%26laquo%3B%D0%9C%D0%B5%D0%B4%D0%B2%D0%B5%D0%B4%D0%B8+%D0%9A%D0%B0%D0%BC%D1%87%D0%B0%D1%82%D0%BA%D0%B8.+%D0%9D%D0%B0%D1%87%D0%B0%D0%BB%D0%BE+%D0%B6%D0%B8%D0%B7%D0%BD%D0%B8%26raquo%3B&url=http%3A%2F%2Fs2.planeta.ru%2Fp%3Furl%3Dhttps%253A%252F%252Fi.ytimg.com%252Fvi%252FdfXpHdty2pw%252Fsddefault.jpg%26amp%3Bwidth%3D400%26amp%3Bheight%3D300%26amp%3Bpad%3Dtrue\"></a>"

        val video12 = "<p:video image=\"http://s2.planeta.ru/p?url=https%3A%2F%2Fi.ytimg.com%2Fvi%2FdfXpHdty2pw%2Fsddefault.jpg&amp;width=400&amp;height=300&amp;pad=true\" id=\"46044\"  duration=\"95\"  name=\"Трейлер TV-версии фильма &laquo;Медведи Камчатки. Начало жизни&raquo;\" owner=\"195268\"  />"
        val video22 = "<a target=\"_blank\" href=\"##blogPostUrl##\"><img src=\"https://s1.dev.planeta.ru/video-stub.jpg?duration=95000&name=%D0%A2%D1%80%D0%B5%D0%B9%D0%BB%D0%B5%D1%80+TV-%D0%B2%D0%B5%D1%80%D1%81%D0%B8%D0%B8+%D1%84%D0%B8%D0%BB%D1%8C%D0%BC%D0%B0+%26laquo%3B%D0%9C%D0%B5%D0%B4%D0%B2%D0%B5%D0%B4%D0%B8+%D0%9A%D0%B0%D0%BC%D1%87%D0%B0%D1%82%D0%BA%D0%B8.+%D0%9D%D0%B0%D1%87%D0%B0%D0%BB%D0%BE+%D0%B6%D0%B8%D0%B7%D0%BD%D0%B8%26raquo%3B&url=http%3A%2F%2Fs2.planeta.ru%2Fp%3Furl%3Dhttps%253A%252F%252Fi.ytimg.com%252Fvi%252FdfXpHdty2pw%252Fsddefault.jpg%26amp%3Bwidth%3D400%26amp%3Bheight%3D300%26amp%3Bpad%3Dtrue\"></a>"

        val audio11 = "<p:audio duration=\"324\" name=\"Черный Кузнец\" artist=\"Чёрный Кузнец\" owner=\"27509\" id=\"18085\"></p:audio>"
        val audio21 = "<a target=\"_blank\" href=\"##blogPostUrl##\"><img src=\"https://s1.dev.planeta.ru/audio-stub.jpg?duration=324000&width=290&name=%D0%A7%D0%B5%D1%80%D0%BD%D1%8B%D0%B9+%D0%9A%D1%83%D0%B7%D0%BD%D0%B5%D1%86&artist=%D0%A7%D1%91%D1%80%D0%BD%D1%8B%D0%B9+%D0%9A%D1%83%D0%B7%D0%BD%D0%B5%D1%86\"></a>"

        val audio12 = "<p:audio    owner=\"27509\"  name=\"Черный Кузнец\"  duration=\"324\" artist=\"Чёрный Кузнец\" id=\"18085\" />"
        val audio22 = "<a target=\"_blank\" href=\"##blogPostUrl##\"><img src=\"https://s1.dev.planeta.ru/audio-stub.jpg?duration=324000&width=290&name=%D0%A7%D0%B5%D1%80%D0%BD%D1%8B%D0%B9+%D0%9A%D1%83%D0%B7%D0%BD%D0%B5%D1%86&artist=%D0%A7%D1%91%D1%80%D0%BD%D1%8B%D0%B9+%D0%9A%D1%83%D0%B7%D0%BD%D0%B5%D1%86\"></a>"

        val photo11 = "<p:photo id=\"248812\" owner=\"861\" image=\"http://s1.planeta.ru/i/3cbec/1372374418381_renamed.jpg\"></p:photo>"
        val photo21 = "<a target=\"_blank\" href=\"##blogPostUrl##\"><img src=\"http://s1.planeta.ru/i/3cbec/1372374418381_renamed.jpg\"></a>"

        val photo12 = "<p:photo image=\"http://s1.planeta.ru/i/3cbed/1372374419471_renamed.jpg\" id=\"248813\" owner=\"861\"/>"
        val photo22 = "<a target=\"_blank\" href=\"##blogPostUrl##\"><img src=\"http://s1.planeta.ru/i/3cbed/1372374419471_renamed.jpg\"></a>"


        val staticNode = "https://s1.dev.planeta.ru"
        //        String beforeString = getString(video11, audio11, photo11, video12, photo12, audio12);
        var beforeString: String
        var afterString: String
        var htmlWithReplacedMediaRichTags: String


        beforeString = getString(video11)
        afterString = getString(video21)
        htmlWithReplacedMediaRichTags = NotificationServiceImpl.getHtmlWithReplacedMediaRichTags(beforeString, "##blogPostUrl##", staticNode)
        assertEquals(afterString, htmlWithReplacedMediaRichTags)


        beforeString = getString(video12)
        afterString = getString(video22)
        htmlWithReplacedMediaRichTags = NotificationServiceImpl.getHtmlWithReplacedMediaRichTags(beforeString, "##blogPostUrl##", staticNode)
        assertEquals(afterString, htmlWithReplacedMediaRichTags)


        beforeString = getString(audio11)
        afterString = getString(audio21)
        htmlWithReplacedMediaRichTags = NotificationServiceImpl.getHtmlWithReplacedMediaRichTags(beforeString, "##blogPostUrl##", staticNode)
        assertEquals(afterString, htmlWithReplacedMediaRichTags)


        beforeString = getString(audio12)
        afterString = getString(audio22)
        htmlWithReplacedMediaRichTags = NotificationServiceImpl.getHtmlWithReplacedMediaRichTags(beforeString, "##blogPostUrl##", staticNode)
        assertEquals(afterString, htmlWithReplacedMediaRichTags)


        beforeString = getString(photo11)
        afterString = getString(photo21)
        htmlWithReplacedMediaRichTags = NotificationServiceImpl.getHtmlWithReplacedMediaRichTags(beforeString, "##blogPostUrl##", staticNode)
        assertEquals(afterString, htmlWithReplacedMediaRichTags)


        beforeString = getString(photo12)
        afterString = getString(photo22)
        htmlWithReplacedMediaRichTags = NotificationServiceImpl.getHtmlWithReplacedMediaRichTags(beforeString, "##blogPostUrl##", staticNode)
        assertEquals(afterString, htmlWithReplacedMediaRichTags)


        beforeString = getString(video11, audio11, photo11, photo12, video12, audio12)
        afterString = getString(video21, audio21, photo21, photo22, video22, audio22)
        htmlWithReplacedMediaRichTags = NotificationServiceImpl.getHtmlWithReplacedMediaRichTags(beforeString, "##blogPostUrl##", staticNode)
        assertEquals(afterString, htmlWithReplacedMediaRichTags)


    }

    private fun getString(vararg s: String): String {
        val txt = "\n<p>aaa bbb ccc</p>\n"
        val sb = StringBuilder(txt)
        for (s1 in s) {
            sb.append(s1).append(txt)
        }
        return sb.toString()
    }


    private fun getOrderInfo(paymentType: PaymentType, deliveryType: DeliveryType): OrderInfo {
        val order = OrderInfo()
        order.buyerGender = Gender.NOT_SET
        order.buyerName = "qweqwe"
        order.orderId = 123
        order.trackingCode = "trackingCode"
        order.cashOnDeliveryCost = BigDecimal(123123)
        order.deliveryComment = "deliveryComment"
        order.totalPrice = BigDecimal(123)

        val orderObjectInfoList = ArrayList<OrderObjectInfo>()
        val orderObjectInfo = getOrderObjectInfo("qwe")
        orderObjectInfoList.add(orderObjectInfo)
        order.orderObjectsInfo = orderObjectInfoList


        order.deliveryType = deliveryType
        if (deliveryType === DeliveryType.DELIVERY) {
            order.deliveryPrice = BigDecimal(100500)
        } else {
            order.deliveryPrice = BigDecimal(0)
        }


        order.paymentType = paymentType

        val linkedDelivery = LinkedDelivery()
        linkedDelivery.name = "linkedDeliveryName"

        val address = address
        linkedDelivery.address = address

        order.linkedDelivery = linkedDelivery

        return order
    }

    private fun getOrderObjectInfo(orderName: String): OrderObjectInfo {
        val orderObjectInfo = OrderObjectInfo()
        orderObjectInfo.objectName = orderName
        orderObjectInfo.price = BigDecimal(99999)
        orderObjectInfo.ownerId = 123
        orderObjectInfo.ownerName = "ownerName"
        orderObjectInfo.merchantName = "merchantName"
        return orderObjectInfo
    }


    class OrderPurchasedEmailType(var isDigital: Boolean, var paymentType: PaymentType, var deliveryType: DeliveryType)


    @Test
    @Ignore
    @Throws(Exception::class)
    fun testOrderPurchasedEmail() {

        val orderTypes = HashMap<String, OrderPurchasedEmailType>()
        orderTypes["NotDigital_Cash"] = OrderPurchasedEmailType(false, PaymentType.CASH, DeliveryType.NOT_SET)
        orderTypes["NotDigital_NotCash_Delivery"] = OrderPurchasedEmailType(false, PaymentType.PAYMENT_SYSTEM, DeliveryType.DELIVERY)
        orderTypes["NotDigital_NotCash_Pickup"] = OrderPurchasedEmailType(false, PaymentType.NOT_SET, DeliveryType.CUSTOMER_PICKUP)
        orderTypes["Digital"] = OrderPurchasedEmailType(true, PaymentType.NOT_SET, DeliveryType.NOT_SET)


        for (orderName in orderTypes.keys) {
            val emailType = orderTypes[orderName]

            val order = getOrderInfo(emailType?.paymentType ?: PaymentType.CASH, emailType?.deliveryType ?: DeliveryType.DELIVERY)

            val params = HashMap<String, Any>()
            params["order"] = order
            params["address"] = "г. Клин"
            params["userName"] = "Pawa"
            params["dearByGender"] = "Уважаемый"
            params["isDigital"] = emailType?.isDigital as Any


            val orderObjectInfo = getOrderObjectInfo(orderName)

            val groupedOrderObjectsList = ArrayList<OrderObjectInfo>()
            groupedOrderObjectsList.add(orderObjectInfo)

            val groupedOrderObjects = HashMap<Long, List<OrderObjectInfo>>()
            groupedOrderObjects[1L] = groupedOrderObjectsList

            params["orderObjects"] = groupedOrderObjects


            sendTestEmail(ORDER_PURCHASED_TEMPLATE, "OrderPurchasedEmail", params)
        }
    }


    @Test
    @Ignore
    @Throws(Exception::class)
    fun testSharePurchaseEmail() {

        val order = getOrderInfo(PaymentType.NOT_SET, DeliveryType.DELIVERY)

        val share = getOrderObjectInfo("name")


        var delivery: LinkedDelivery? = null
        val baseDeliveryService = BaseDelivery()
        delivery = LinkedDelivery(baseDeliveryService)
        delivery.name = "name"
        delivery.setShareId(123)
        delivery.price = BigDecimal.ZERO
        delivery.publicNote = "publicNote" // null || string
        delivery.serviceType = DeliveryType.DELIVERY


        var address: Address? = null
        address = address


        var instruction: String? = null
        instruction = "instruction"


        val params = HashMap<String, Any>()
        params["order"] = order
        params["share"] = share
        params["delivery"] = delivery
        params["address"] = address as Any
        params["instruction"] = instruction


        sendTestEmail(SHARE_PURCHASE_EMAIL_TEMPLATE, "SharePurchaseEmail", params)
    }


    @Test
    @Ignore
    @Throws(Exception::class)
    fun testNewBlogPostInCampaignEmail() {
        val params = HashMap<String, Any>()
        sendTestEmail("new-blog-post-in-campaign.ftl", "newBlogPostInCampaign", params)
    }

    @Test
    @Ignore
    @Throws(Exception::class)
    fun testCampaignBecameVipOffersAllowedEmail() {
        val params = HashMap<String, Any>()
        params["count"] = "3"
        params["amount"] = "15 000"

        sendTestEmail(CAMPAIGN_BECAME_VIP_OFFERS_ALLOWED, "campaignBecameVipOffersAllowed", params)
    }


    @Test
    @Ignore
    @Throws(Exception::class)
    fun testUserBecameVipEmail() {
        val params = HashMap<String, Any>()
        params["count"] = "3"
        params["amount"] = "15 000"

        sendTestEmail(USER_BECAME_VIP, "userBecameVip", params)
    }


    @Test
    @Ignore
    @Throws(Exception::class)
    fun testOrderSentEmail() {

        val params = HashMap<String, Any>()
        params["userName"] = "userName"


        val order = getOrderInfo(PaymentType.NOT_SET, DeliveryType.DELIVERY)
        params["order"] = order

        val address = address
        params["address"] = address


        sendTestEmail(ORDER_SENT_TEMPLATE, "orderSent", params)
    }


    @Test
    @Ignore
    @Throws(Exception::class)
    fun testAllFtlEmail() {
        testOrderPurchasedEmail()
        testSharePurchaseEmail()
        testNewBlogPostInCampaignEmail()
        testCampaignBecameVipOffersAllowedEmail()
        testUserBecameVipEmail()
        testOrderSentEmail()
    }

    @Test
    fun testReplaceMailAwayUrls() {
        val postText = "<p>Текст до <a target=\"_blank\" href=\"/api/util/away.html?to=https%3A%2F%2Fya.ru\">https://planeta.ru/campaigns/43518</a> " + "Текст после <a target=\"_blank\" href=\"/api/util/away.html?to=https%3A%2F%2Fya.ru\">#Хэштэг</a></p>"
        val postTextHtml = Utils.replaceAwayUrlsToMailUrls(postText, "https://domain.name")
        val postTextHtmlExpected = "<p>Текст до <a target=\"_blank\" href=\"https://domain.name/api/util/away_mail.html?to=https%3A%2F%2Fya.ru\">https://planeta.ru/campaigns/43518</a> " + "Текст после <a target=\"_blank\" href=\"https://domain.name/api/util/away_mail.html?to=https%3A%2F%2Fya.ru\">#Хэштэг</a></p>"
        assertEquals(postTextHtml, postTextHtmlExpected)
    }

    @Throws(Exception::class)
    private fun sendTestEmail(templateFileName: String, emailName: String, params: MutableMap<String, Any>) {
        val post = Post()
        val postText = "<p><a target=\"_blank\" href=\"/api/util/away.html?to=https%3A%2F%2Fvk.com%2Faway.php%3Fto%3Dhttps%253A%252F%252Fplaneta.ru%252Fcampaigns%252F43518%26amp%3Bpost%3D346355846_326\">https://planeta.ru/campaigns/43518</a> можно приобрести наши лимитированные подарочные скетчбуки и цифровую копию фильма &quot;Инволюция&quot; и таким образом поддержать проект фонда помощи животным! Все денежные средства будут переданы фонду. Спасибо каждому, кто помогает! <a target=\"_blank\" href=\"/api/util/away.html?to=https%3A%2F%2Fvk.com%2Ffeed%3Fsection%3Dsearch%26amp%3Bq%3D%2523%25D0%25A9%25D0%25B5%25D0%25B4%25D1%2580%25D1%258B%25D0%25B9%25D0%2592%25D1%2582%25D0%25BE%25D1%2580%25D0%25BD%25D0%25B8%25D0%25BA\">#ЩедрыйВторник</a></p>\n" + "<p><p:photo id=\"1436922\" owner=\"557722\" image=\"https://s3.planeta.ru/i/15ecfa/1480433697894_renamed.jpg\"></p:photo></p>\n"
        post.postText = postText
        val postTextHtml = notificationService!!.formatPostForEmail(post, "blogPostUrl")
        params["mailTo"] = "michail.michail@gmail.com"
        params["campaignName"] = "campaignName"
        params["campaignId"] = 350
        params["blogPostTitle"] = "blogPostTitle"
        params["blogPostDate"] = Date()
        params["blogPostHtml"] = postTextHtml
        params["blogPostLink"] = "http://ya.ru"
        params["noInlineStyleHtml"] = postTextHtml
        params["templateName"] = NEW_BLOG_POST_IN_CAMPAIGN.toString()
        params["unsubscribeUrl"] = "unsubscribeUrl"
        params["emailNode"] = EmailNodes.BODY
        val emailBody = notificationTemplateService!!.applyCustomTemplate(templateFileName, params)

        mailClient!!.sendMessageRich(templateFileName, "michail.michail@gmail.com", "noreply@planeta.ru", emailName, emailBody, MailMessagePriority.DEFAULT)
    }


    @Test
    @Ignore
    fun testHtml() {
        mailClient!!.sendSafeHtmlEmail("auto.registration", "pawa@mail.ru", Utils.smap(
                "password", "qwery",
                "regCode", "dadasdasdas",
                "url", "site.ru",
                "escapedRedirectUrl", encodeUrl("https://site.ru/?oldPassword=" + "qwerty")
        ), MailMessagePriority.DEFAULT)
    }

    @Ignore
    @Test
    fun testSkipValidation() {
        notificationService!!.sendTechnobattleProgrammLetter("michail.michail@gmail.com")

        val registration = TechnobattleRegistration()
        registration.type = TechnobattleRegistration.Type.REGISTRAION
        registration.name = "name"
        registration.email = "michail.michail@gmail.com"
        registration.description = "desc"
        registration.city = "city"
        registration.phone = "phone"
        notificationService.sendTechnobattleRegistration(registration)
    }

    @Ignore
    @Test
    fun testAttentionPromoCodesIsGoingToEnd() {
        notificationService!!.sendTechnobattleManagerLetterAboutPromoCodesIsGoingToEnd(999)
    }

}

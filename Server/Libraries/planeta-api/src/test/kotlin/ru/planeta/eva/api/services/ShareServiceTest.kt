package ru.planeta.eva.api.services

import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit4.SpringRunner
import ru.planeta.dao.TransactionScope
import ru.planeta.dto.SaveShareInfoDTO


@RunWith(SpringRunner::class)
@ContextConfiguration(locations = ["classpath*:spring/applicationContext-*.xml",
    "classpath*:/spring/applicationContext-dao.xml",
    "classpath*:/spring/applicationContext-geo.xml"])
@SpringBootTest
class ShareServiceTest {

    @Autowired
    lateinit var shareService: ShareService
    lateinit var transactionScope: TransactionScope


    @Before
    @Throws(Exception::class)
    fun setUp() {
        transactionScope = TransactionScope.createOrGetCurrentTest()
    }

    @After
    @Throws(Exception::class)
    fun tearDown() {
        transactionScope.close()
    }

    @Test
    fun testGetPurchaseShareInfo() {
        val id = shareService.savePurchaseShareInfo(SaveShareInfoDTO(182697, 10, 200000.toBigDecimal()))
        val purchaseShareInfo = shareService.getPurchaseShareInfo(1, 0)
    }
}
package ru.planeta.eva.api.services

import org.junit.After
import org.junit.Before
import org.junit.Ignore
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner
import ru.planeta.commons.model.Gender
import ru.planeta.eva.api.TestWebConfig
import ru.planeta.dao.TransactionScope
import ru.planeta.entity.Profile
import ru.planeta.model.enums.ProfileStatus
import ru.planeta.model.enums.ProfileType
import java.util.*
import kotlin.test.assertNotEquals

@RunWith(SpringJUnit4ClassRunner::class)
@ContextConfiguration(classes = [TestWebConfig::class])
@Ignore
class UserProfileServiceTest {


    private lateinit var transactionScope: TransactionScope

    @Before
    fun init() {
        transactionScope = TransactionScope.createOrGetCurrent()
    }

    @After
    fun close() {
        transactionScope.close()
    }

    @Autowired
    lateinit var userProfileService: UserProfileService

    @Test
    @Ignore
    fun testProfileService() {
        val profile = Profile()

        // Тестим добавление профиля
        profile.profileId = -23
        profile.creatorProfileId = 1
        profile.profileType = ProfileType.USER
        profile.imageUrl = "test_url"
        profile.imageId = 12
        profile.smallImageUrl = "small_image_url"
        profile.smallImageId = 13
        profile.alias = "alias"
        profile.status = ProfileStatus.USER_ACTIVE
        profile.cityId = 1
        profile.countryId = 1
        profile.userBirthDate = Date()
        profile.userGender = Gender.MALE
        profile.usersCount = 1
        profile.displayName = "user_test"
        profile.summary = "test_user"
        profile.vip = true
        profile.vipObtainDate = Date()
        profile.isReceiveNewsletters = true
        profile.isShowBackedCampaigns = true
        profile.backedCount = 1
        profile.projectsCount = 1
        profile.authorProjectsCount = 1
        profile.subscribersCount = 1
        profile.newSubscribersCount = 1
        profile.subscriptionsCount = 1
        profile.receiveMyCampaignNewsletters = true
        profile.phoneNumber = "123"

        userProfileService.saveProfile(profile)

        var existProfile = userProfileService.selectProfile(profile.profileId)

        assertNotEquals(profile, existProfile)

        // Тестим обновление профиля
        profile.profileId = -23
        profile.creatorProfileId = 2
        profile.profileType = ProfileType.USER
        profile.imageUrl = "test_url_2"
        profile.imageId = 123
        profile.smallImageUrl = "small_image_url2"
        profile.smallImageId = 133
        profile.alias = "alias2"
        profile.status = ProfileStatus.USER_ACTIVE
        profile.cityId = 2
        profile.countryId = 3
        profile.userBirthDate = Date()
        profile.userGender = Gender.FEMALE
        profile.usersCount = 2
        profile.displayName = "user_test2"
        profile.summary = "test_user2"
        profile.vip = false
        profile.vipObtainDate = Date()
        profile.isReceiveNewsletters = false
        profile.isShowBackedCampaigns = false
        profile.backedCount = 12
        profile.projectsCount = 12
        profile.authorProjectsCount = 12
        profile.subscribersCount = 12
        profile.newSubscribersCount = 12
        profile.subscriptionsCount = 122
        profile.receiveMyCampaignNewsletters = false
        profile.phoneNumber = "123222"

        userProfileService.saveProfile(profile)
        existProfile = userProfileService.selectProfile(profile.profileId)

        assertNotEquals(profile, existProfile)
    }
}
package ru.planeta.eva.api

import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.ImportResource

@Configuration
@ImportResource(locations = ["classpath*:spring/applicationContext-*.xml",
    "classpath*:/spring/applicationContext-dao.xml",
    "classpath*:/spring/applicationContext-geo.xml",
    "classpath*:/ru/planeta/spring/propertyConfigurer.xml"])

@ComponentScan(basePackages = ["ru.planeta.eva.api.services"])
class TestWebConfig
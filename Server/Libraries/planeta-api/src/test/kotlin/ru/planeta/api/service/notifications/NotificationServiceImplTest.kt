package ru.planeta.api.service.notifications

import org.junit.Ignore
import org.junit.Test
import org.springframework.beans.factory.annotation.Autowired
import ru.planeta.dao.bibliodb.BookDAO
import ru.planeta.dao.bibliodb.LibraryDAO
import ru.planeta.model.bibliodb.BookOrder
import ru.planeta.model.common.campaign.CampaignSubscribe
import ru.planeta.test.AbstractTest
import java.util.*

/**
 *
 * Created by asavan on 26.06.2017.
 */
class NotificationServiceImplTest : AbstractTest() {
    @Autowired
    lateinit var libraryDAO: LibraryDAO
    @Autowired
    lateinit var bookDAO: BookDAO

    @Test
    @Ignore
    @Throws(Exception::class)
    fun sendBiblioPurchaseNotification() {

        val libs = libraryDAO!!.selectListByIds(Arrays.asList(3L, 4L, 5L))

        val book = bookDAO!!.selectBookById(29)
        val bookOrder = BookOrder(book, 1)
        notificationService.sendBiblioPurchaseBookNotification(bookOrder, libs, "Малашин Петр Сергеевич", Date(),
                "asavanster@gmail.com", "asavanster@gmail.com")
    }

    @Ignore
    @Test
    @Throws(Exception::class)
    fun sendCampaignEndsSoon() {
        val cs = CampaignSubscribe()
        cs.profileId = 191
        cs.campaignId = 66754
        notificationService.sendCampaignCampaignEndsSoonNotification(cs)
    }

}

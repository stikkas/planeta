package ru.planeta.eva.api.services

import org.junit.Assert
import org.junit.Ignore
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit4.SpringRunner
import ru.planeta.dao.TransactionScope


@RunWith(SpringRunner::class)
@ContextConfiguration(locations = ["classpath*:spring/applicationContext-*.xml",
    "classpath*:/spring/applicationContext-dao.xml",
    "classpath*:/spring/applicationContext-geo.xml"])
@SpringBootTest
class MediaServiceTest {
    @Autowired
    lateinit var mediaService: MediaService
    lateinit var transactionScope: TransactionScope

    @Test
    @Ignore
    fun testSelect() {
        val audio = mediaService.getAudio(21352)
        Assert.assertNotNull(audio)
    }
}
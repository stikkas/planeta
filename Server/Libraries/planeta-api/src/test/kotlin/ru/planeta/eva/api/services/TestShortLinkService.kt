package ru.planeta.eva.api.services

import org.junit.After
import org.junit.Before
import org.junit.Ignore
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit4.SpringRunner
import ru.planeta.api.model.ConfigurationType
import ru.planeta.api.model.ShortLinkExtraSocialLink
import ru.planeta.api.service.configurations.ConfigurationService
import ru.planeta.dao.TransactionScope


@RunWith(SpringRunner::class)
@ContextConfiguration(locations = ["classpath*:spring/applicationContext-*.xml",
    "classpath*:/spring/applicationContext-dao.xml",
    "classpath*:/spring/applicationContext-geo.xml"])
@SpringBootTest
class TestShortLinkService {

    @Autowired
    lateinit var shortLinkService: ShortLinkService
    @Autowired
    lateinit var configurationService: ConfigurationService
    lateinit var transactionScope: TransactionScope


    @Before
    @Throws(Exception::class)
    fun setUp() {
        transactionScope = TransactionScope.createOrGetCurrentTest()
    }

    @After
    @Throws(Exception::class)
    fun tearDown() {
        transactionScope.close()
    }

    @Test
    @Ignore
    fun testGenerateMissingSocialLinks() {
        val clientId = 137617L;
        val shortLinkId = 408L;
        val shortLinkExtraSocialLinks: List<ShortLinkExtraSocialLink> = configurationService.getJsonArrayConfig(ShortLinkExtraSocialLink::class.java, ConfigurationType.SHORTLINK_EXTRA_SOCIAL_LINKS)
        System.out.println(shortLinkExtraSocialLinks);
        for(short in shortLinkExtraSocialLinks) {
            System.out.println(short.name);
            System.out.println(short.urlParams);
            System.out.println();
        }

        val items = listOf(1, 2, 3, 4, 5)

        // Lambdas are code blocks enclosed in curly braces.
        shortLinkExtraSocialLinks.forEach { it.name = it.name }
        //shortLinkService.generateMissingSocialLinks(clientId, shortLinkId, configuration.stringValue)
    }
}
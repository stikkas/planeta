package ru.planeta.api.service.notifications

import org.junit.Before
import org.junit.Ignore
import org.junit.Test
import org.springframework.beans.factory.annotation.Autowired
import ru.planeta.api.mail.MailClient
import ru.planeta.model.mail.MailMessagePriority
import ru.planeta.test.AbstractTest

class TestMailClient : AbstractTest() {
    @Autowired
    lateinit var mailClient1: MailClient

    @Before
    fun setMailClient() {
        this.mailClient1.setAsynchronous(true)
    }

    @Test
    @Ignore
    @Throws(InterruptedException::class)
    fun testPriorityMails() {
        for (iter in 0..99) {
            mailClient1.sendMessageRich("test.template", "dev@planeta.ru", "noreply@planeta.ru", "LOW10", "LOW", MailMessagePriority.LOW)
            mailClient1.sendMessageRich("test.template", "dev@planeta.ru", "noreply@planeta.ru", "HIGH10", "HIGH", MailMessagePriority.HIGH)
        }

        Thread.sleep((1000 * 60 * 10).toLong())
    }

}

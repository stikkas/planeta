package ru.planeta.api.service.notifications

import org.junit.Assert
import org.junit.Ignore
import org.junit.Test
import org.springframework.beans.factory.annotation.Autowired
import ru.planeta.api.exceptions.NotFoundException
import ru.planeta.api.service.common.MailMessageService
import ru.planeta.model.mail.MailMessage
import ru.planeta.model.mail.MailMessageStatus
import ru.planeta.test.AbstractTest

/**
 * Created with IntelliJ IDEA.
 * User: michail
 * Date: 30.05.16
 * Time: 15:55
 */
class TestMailNotification : AbstractTest() {
    @Autowired
    lateinit var mailMessageService: MailMessageService

    @Ignore
    @Test
    @Throws(NotFoundException::class)
    fun testSetExternalMsgId() {
        val intMessageId: Long = 1788574
        val extMessageId = "gsom"
        mailMessageService!!.setExternalMessageId(intMessageId, extMessageId)

        val notification = mailMessageService.getMailNotificationSafeById(intMessageId)[0]
        Assert.assertEquals(notification.externalMessageId, extMessageId)
        Assert.assertEquals(notification.status, MailMessageStatus.SENT)
        Assert.assertNotNull(notification.timeUpdated)
    }

    @Ignore
    @Test
    @Throws(NotFoundException::class)
    fun testUpdateStatus() {
        val intMessageId: Long = 1788574
        val extMessageId = "gsom"
        mailMessageService.setExternalMessageId(intMessageId, extMessageId)
        mailMessageService.setStatusByExtMsgId(extMessageId, MailMessageStatus.BOUNCE)

        val notification = mailMessageService.getMailNotificationSafeByExtMsgId(extMessageId)[0]
        Assert.assertEquals(notification.status, MailMessageStatus.BOUNCE)
        Assert.assertNotNull(notification.timeUpdated)
    }
}

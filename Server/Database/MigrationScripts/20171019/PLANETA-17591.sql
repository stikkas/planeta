﻿alter table shopdb.products add column content_urls text[];

UPDATE shopdb.products set content_urls[0] = content_url;

ALTER TABLE shopdb.products DROP COLUMN content_url;
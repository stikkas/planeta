CREATE TABLE profiledb.broadcast_product_targeting
(
  broadcast_id BIGINT                   NOT NULL,
  product_id   BIGINT                   NOT NULL,

  CONSTRAINT broadcast_product_targeting_pkey PRIMARY KEY (broadcast_id, product_id)
);

grant select, INSERT, UPDATE, DELETE on profiledb.broadcast_product_targeting to planeta ;
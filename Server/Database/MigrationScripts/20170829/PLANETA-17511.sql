ALTER TYPE profiledb.profile_news_type ADD VALUE 'CAMPAIGN_INTERACTIVE_STEP_START';
ALTER TYPE profiledb.profile_news_type ADD VALUE 'CAMPAIGN_INTERACTIVE_STEP_NAME';
ALTER TYPE profiledb.profile_news_type ADD VALUE 'CAMPAIGN_INTERACTIVE_STEP_EMAIL';
ALTER TYPE profiledb.profile_news_type ADD VALUE 'CAMPAIGN_INTERACTIVE_STEP_PROCEED';
ALTER TYPE profiledb.profile_news_type ADD VALUE 'CAMPAIGN_INTERACTIVE_STEP_INFO';
ALTER TYPE profiledb.profile_news_type ADD VALUE 'CAMPAIGN_INTERACTIVE_STEP_DURATION';
ALTER TYPE profiledb.profile_news_type ADD VALUE 'CAMPAIGN_INTERACTIVE_STEP_PRICE';
ALTER TYPE profiledb.profile_news_type ADD VALUE 'CAMPAIGN_INTERACTIVE_STEP_VIDEO';
ALTER TYPE profiledb.profile_news_type ADD VALUE 'CAMPAIGN_INTERACTIVE_STEP_DESCRIPTION';
ALTER TYPE profiledb.profile_news_type ADD VALUE 'CAMPAIGN_INTERACTIVE_STEP_REWARD';
ALTER TYPE profiledb.profile_news_type ADD VALUE 'CAMPAIGN_INTERACTIVE_STEP_COUNTERPARTY';
ALTER TYPE profiledb.profile_news_type ADD VALUE 'CAMPAIGN_INTERACTIVE_STEP_CHECK';
ALTER TYPE profiledb.profile_news_type ADD VALUE 'CAMPAIGN_INTERACTIVE_STEP_FINAL';
ALTER TYPE profiledb.profile_news_type ADD VALUE 'CAMPAIGN_INTERACTIVE_STEP_FINAL_DRAFT';

ALTER TYPE profiledb.profile_news_type ADD VALUE 'SAVE_CAMPAIGN_INTERACTIVE_STEP_INFO';
ALTER TYPE profiledb.profile_news_type ADD VALUE 'SAVE_CAMPAIGN_INTERACTIVE_STEP_DURATION';
ALTER TYPE profiledb.profile_news_type ADD VALUE 'SAVE_CAMPAIGN_INTERACTIVE_STEP_PRICE';
ALTER TYPE profiledb.profile_news_type ADD VALUE 'SAVE_CAMPAIGN_INTERACTIVE_STEP_VIDEO';
ALTER TYPE profiledb.profile_news_type ADD VALUE 'SAVE_CAMPAIGN_INTERACTIVE_STEP_DESCRIPTION';
ALTER TYPE profiledb.profile_news_type ADD VALUE 'SAVE_CAMPAIGN_INTERACTIVE_STEP_REWARD';
ALTER TYPE profiledb.profile_news_type ADD VALUE 'SAVE_CAMPAIGN_INTERACTIVE_STEP_COUNTERPARTY';
ALTER TYPE profiledb.profile_news_type ADD VALUE 'CREATE_DRAFT_CAMPAIGN_INTERACTIVE_STEP_INFO';
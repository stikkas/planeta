CREATE TABLE statdb.order_shortlink_stats (
  shortlink_stats_id BIGINT NOT NULL,
  shortlink_id BIGINT NOT NULL,
  order_id BIGINT NOT NULL,
  referrer TEXT,
  time_added TIMESTAMP WITHOUT TIME ZONE DEFAULT now() NOT NULL,
  PRIMARY KEY(shortlink_stats_id)
)
WITH (oids = false);

ALTER TABLE statdb.order_shortlink_stats
  ALTER COLUMN shortlink_stats_id SET STATISTICS 0;

ALTER TABLE statdb.order_shortlink_stats
  ALTER COLUMN shortlink_id SET STATISTICS 0;

ALTER TABLE statdb.order_shortlink_stats
  ALTER COLUMN order_id SET STATISTICS 0;

ALTER TABLE statdb.order_shortlink_stats
  ALTER COLUMN referrer SET STATISTICS 0;

ALTER TABLE statdb.order_shortlink_stats
  ALTER COLUMN time_added SET STATISTICS 0;

CREATE SEQUENCE statdb.seq_order_shortlink_stats_id
  INCREMENT 1 MINVALUE 1
  MAXVALUE 9223372036854775807 START 1;
CREATE TABLE IF NOT EXISTS commondb.auth (
  auth_id BIGINT PRIMARY KEY NOT NULL,
  auth_token VARCHAR(256) NOT NULL,
  auth_authentication BYTEA NOT NULL,
  time_added TIMESTAMP WITHOUT TIME ZONE NOT NULL
);


CREATE SEQUENCE commondb.seq_auth_id
  INCREMENT 1 MINVALUE 1
  MAXVALUE 9223372036854775807 START 1
  CACHE 1;

CREATE INDEX auth_token_idx ON commondb.auth(auth_token);
GRANT ALL ON commondb.auth TO planeta;
GRANT ALL ON commondb.seq_auth_id TO planeta;


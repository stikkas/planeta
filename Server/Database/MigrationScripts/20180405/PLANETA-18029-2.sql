ALTER TABLE commondb.payment_methods ADD COLUMN name_en VARCHAR(50) NOT NULL DEFAULT '';
ALTER TABLE commondb.payment_methods ADD COLUMN description_en VARCHAR(255) NOT NULL DEFAULT '';

UPDATE commondb.payment_methods SET
  name = 'Карты банков России',
  image_url = '/images/balance-choice/sber.png',
  description = 'Принимаются карты только банков России',
  name_en = 'Russian Banks'' Cards',
  description_en = 'Only Russian Banks'' Cards are accepted for payment'
WHERE alias = 'CARD_SBERBANK';

UPDATE commondb.payment_methods SET
  name = 'Карты других банков',
  image_url = '/images/balance-choice/credit-card.png',
  description = 'Принимаются любые карты',
  name_en = 'Rest of The World',
  description_en = 'All cards are accepted for payment'
WHERE alias = 'CARD';

UPDATE commondb.payment_methods SET
  image_url = '/images/balance-choice/other-payment.png',
  description = 'Яндекс Деньги, WebMoney и прочие',
  name_en = 'Other types of payment',
  description_en = 'Yandex.Money, WebMoney etc'
WHERE alias = 'OTHER';


UPDATE commondb.payment_methods SET
  image_url = '',
  name_en = 'Online banking',
  parent_id = 9
WHERE alias = 'INTERNET_BANKING';

UPDATE commondb.payment_methods SET
  image_url = '',
  name = 'Наличные деньги и мобильные платежи',
  name_en = 'Cash & mobile payments',
  parent_id = 9
WHERE alias = 'CASH';

INSERT INTO commondb.payment_methods (id, alias, image_url, name, name_en, parent_id) VALUES (
    117, 'ELECTRONIC_MONEY', '', 'Электронные деньги', 'E-money', 9
);

UPDATE commondb.payment_methods SET
  image_url = '/images/balance-choice/sber-online.png',
  name_en = 'Sberbank Online'
WHERE alias = 'SBERBANK_ONLINE';

UPDATE commondb.payment_methods SET
  image_url = '/images/balance-choice/alfa.png',
  name_en = 'Alfa-Click internet banking'
WHERE alias = 'ALFA_CLICK';

UPDATE commondb.payment_methods SET
  image_url = '/images/balance-choice/psb.png',
  name_en = 'PSB-Retail Promsvyazbank'
WHERE alias = 'PSB_RETAIL';

UPDATE commondb.payment_methods SET
  image_url = '/images/balance-choice/tinkoff.png',
  name_en = 'Tinkoff Bank'
WHERE alias = 'TINKOFF';

UPDATE commondb.payment_methods SET
  image_url = '/images/balance-choice/yandex.png',
  name_en = 'Yandex Money',
  parent_id = 117
WHERE alias = 'YANDEX_MONEY';

UPDATE commondb.payment_methods SET
  image_url = '/images/balance-choice/wm.png',
  name_en = 'WebMoney',
  parent_id = 117
WHERE alias = 'WEB_MONEY';

UPDATE commondb.payment_methods SET
  image_url = '/images/balance-choice/qiwi.png',
  name_en = 'QIWI wallet',
  parent_id = 117
WHERE alias = 'QIWI';


UPDATE commondb.payment_methods SET
  image_url = '/images/balance-choice/mobile-payment.png',
  name_en = 'Mobile payments',
  parent_id = 101
WHERE alias = 'MOBILE';

UPDATE commondb.payment_methods SET
  image_url = '/images/balance-choice/banks-branches.png',
  name_en = 'Bank branches'
WHERE alias = 'BANK_OFFICE';

UPDATE commondb.payment_methods SET
  image_url = '/images/balance-choice/terminals.png',
  name_en = 'Payment terminals'
WHERE alias = 'PAYMENT_TERMINAL';

UPDATE commondb.payment_methods SET
  image_url = '/images/balance-choice/cash-transfer.png',
  name_en = 'Money transfers'
WHERE alias = 'MONEY_TRANSFER';

UPDATE commondb.project_payment_methods SET priority = 5 WHERE project_type = 'MAIN' and payment_method_id = 101;

INSERT INTO commondb.project_payment_methods (project_type, payment_method_id, priority, enabled) VALUES (
  'MAIN',
  117,
  4,
  TRUE
);

UPDATE commondb.project_payment_methods SET enabled = FALSE WHERE project_type = 'MAIN' and payment_method_id = 108;
alter table commondb.payment_methods add COLUMN parent_id_old BIGINT DEFAULT null;

UPDATE commondb.payment_methods SET parent_id_old = 0 WHERE alias = 'CARD_SBERBANK';
UPDATE commondb.payment_methods SET parent_id_old = 0 WHERE alias = 'CARD';
UPDATE commondb.payment_methods SET parent_id_old = 0 WHERE alias = 'YANDEX_MONEY';
UPDATE commondb.payment_methods SET parent_id_old = 0 WHERE alias = 'INTERNET_BANKING';
UPDATE commondb.payment_methods SET parent_id_old = 0 WHERE alias = 'CASH';
UPDATE commondb.payment_methods SET parent_id_old = 0 WHERE alias = 'MOBILE';
UPDATE commondb.payment_methods SET parent_id_old = 0 WHERE alias = 'QIWI';
UPDATE commondb.payment_methods SET parent_id_old = 0 WHERE alias = 'WEB_MONEY';
UPDATE commondb.payment_methods SET parent_id_old = 0 WHERE alias = 'OTHER';
UPDATE commondb.payment_methods SET parent_id_old = 100 WHERE alias = 'SBERBANK_ONLINE';
UPDATE commondb.payment_methods SET parent_id_old = 100 WHERE alias = 'ALFA_CLICK';
UPDATE commondb.payment_methods SET parent_id_old = 100 WHERE alias = 'PSB_RETAIL';
UPDATE commondb.payment_methods SET parent_id_old = 100 WHERE alias = 'TINKOFF';
UPDATE commondb.payment_methods SET parent_id_old = 100 WHERE alias = 'OTHER_BANKS';
UPDATE commondb.payment_methods SET parent_id_old = 101 WHERE alias = 'BANK_OFFICE';
UPDATE commondb.payment_methods SET parent_id_old = 101 WHERE alias = 'PAYMENT_TERMINAL';
UPDATE commondb.payment_methods SET parent_id_old = 101 WHERE alias = 'MONEY_TRANSFER';
UPDATE commondb.payment_methods SET parent_id_old = 101 WHERE alias = 'CELL_POINT';

alter table commondb.payment_methods add COLUMN image_url_old VARCHAR(100) DEFAULT null;
alter table commondb.payment_methods add COLUMN name_old VARCHAR(50) DEFAULT null;

UPDATE commondb.payment_methods SET image_url_old = '/images/balance/sbercard.png', name_old = 'Картой банков России' WHERE alias = 'CARD_SBERBANK';
UPDATE commondb.payment_methods SET image_url_old = '/images/balance/credit-card.png', name_old = 'Картой других банков' WHERE alias = 'CARD';
UPDATE commondb.payment_methods SET image_url_old = '/images/balance/yandex.png', name_old = 'Яндекс Деньги' WHERE alias = 'YANDEX_MONEY';
UPDATE commondb.payment_methods SET image_url_old = '/images/balance/online-payment.png', name_old = 'Онлайн банки' WHERE alias = 'INTERNET_BANKING';
UPDATE commondb.payment_methods SET image_url_old = '/images/balance/cash-other.png', name_old = 'Оплата наличными' WHERE alias = 'CASH';
UPDATE commondb.payment_methods SET image_url_old = '/images/balance/mobile-payment.png', name_old = 'Мобильные платежи' WHERE alias = 'MOBILE';
UPDATE commondb.payment_methods SET image_url_old = '/images/balance/qiwi.png', name_old = 'QIWI кошелек' WHERE alias = 'QIWI';
UPDATE commondb.payment_methods SET image_url_old = '/images/balance/wm.png', name_old = 'WebMoney' WHERE alias = 'WEB_MONEY';
UPDATE commondb.payment_methods SET image_url_old = '/images/balance/other-payment.png', name_old = 'Другие способы' WHERE alias = 'OTHER';
UPDATE commondb.payment_methods SET image_url_old = '/images/balance/sber.png', name_old = 'Сбербанк Онлайн' WHERE alias = 'SBERBANK_ONLINE';
UPDATE commondb.payment_methods SET image_url_old = '/images/balance/alfa.png', name_old = 'Интернет-банк Альфа-Клик' WHERE alias = 'ALFA_CLICK';
UPDATE commondb.payment_methods SET image_url_old = '/images/balance/psb.png', name_old = 'PSB-Retail Промсвязьбанк' WHERE alias = 'PSB_RETAIL';
UPDATE commondb.payment_methods SET image_url_old = '/images/balance/tinkoff.png', name_old = 'Тинькофф Банк' WHERE alias = 'TINKOFF';
UPDATE commondb.payment_methods SET image_url_old = '/images/balance/online-other.png', name_old = 'Другие онлайн-банки' WHERE alias = 'OTHER_BANKS';
UPDATE commondb.payment_methods SET image_url_old = '/images/balance/banks-branches.png', name_old = 'Отделения банков' WHERE alias = 'BANK_OFFICE';
UPDATE commondb.payment_methods SET image_url_old = '/images/balance/terminals.png', name_old = 'Терминалы оплаты' WHERE alias = 'PAYMENT_TERMINAL';
UPDATE commondb.payment_methods SET image_url_old = '/images/balance/cash-transfer.png', name_old = 'Денежные переводы' WHERE alias = 'MONEY_TRANSFER';


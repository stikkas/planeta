CREATE TABLE commondb.purchase_share_info
(
  id BIGINT PRIMARY KEY NOT NULL,
  share_id BIGINT NOT NULL,
  quantity INTEGER NOT NULL,
  donate_amount NUMERIC(10, 2) NOT NULL
);

GRANT SELECT, INSERT, UPDATE, DELETE ON commondb.purchase_share_info TO planeta;

CREATE SEQUENCE commondb.purchase_share_info_id_seq INCREMENT 1 START 1;

ALTER SEQUENCE commondb.purchase_share_info_id_seq OWNER TO planeta;

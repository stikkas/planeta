ALTER TABLE commondb.base_delivery_service ADD COLUMN name_en VARCHAR(100) NOT NULL DEFAULT '';
ALTER TABLE commondb.base_delivery_service ADD COLUMN description_en TEXT;

UPDATE commondb.base_delivery_service
SET description = 'Осуществляется Почтой России', name_en = 'Delivery by mail', description_en = 'Russian Post'
WHERE service_id = -1;

UPDATE commondb.base_delivery_service
SET description  = 'Из офиса компании Planeta.ru или напрямую у автора.', name_en = 'Customer pick up',
  description_en = 'Pick up from Planeta.ru or Author''s location.'
WHERE service_id = -2;

CREATE SEQUENCE commondb.seq_purchase_share_info_id
  INCREMENT 1 MINVALUE 1
  MAXVALUE 9223372036854775807 START 1
  CACHE 1;

GRANT ALL ON commondb.seq_purchase_share_info_id TO planeta;
UPDATE commondb.sponsors
SET
  project_card_html = '<style>
    .inj-project-card-approve{
        display: inline-block;
        vertical-align: top;
        margin: 0 0 0 6px;
        padding: 6px 17px 6px 8px;
        background: #1a8cff;
        border-radius: 20px;
    }
    .inj-project-card-approve-logo{
        display: inline-block;
        vertical-align: top;
        margin: -2px 8px -2px 0;
        font-size: 0;
        line-height: 0;
    }
    .inj-project-card-approve-lbl{
        display: inline-block;
        vertical-align: top;
    }
</style>
<script>
    injection.init([''projectCard'', ''APPROVE''], function(el) {
        var supported = $(''.project-card-supported'', el);
        supported.addClass(''compact'');
        supported.attr(''data-tooltip'', supported.attr(''data-tooltip-text''));
        if ( !el.data(''inj-approve-badge'') ) {
            el.data(''inj-approve-badge'', true);
            $(''.project-card-badges'', el).append(''<span class="inj-project-card-approve"><span class="inj-project-card-approve-logo"><svg width="16" height="16" viewBox="0 0 16 16"><path d="M14.6 3.5c.1-.2.2-.5.2-.8 0-.9-.7-1.6-1.6-1.6-.3 0-.6.1-.8.2C9.3-.8 5-.5 2.3 2.3c-3.1 3.1-3.1 8.2 0 11.3 3.1 3.1 8.2 3.1 11.3 0 2.8-2.7 3.1-6.9 1-10.1zm-1.7 9.4c-2.7 2.7-7.1 2.7-9.8 0-2.7-2.7-2.7-7.1 0-9.8 2.4-2.4 6-2.7 8.7-.9-.1.2-.1.3-.1.5 0 .9.7 1.6 1.6 1.6.2 0 .4 0 .5-.1 1.8 2.7 1.5 6.3-.9 8.7zm-1.5-8.3c1.9 1.9 1.9 4.9 0 6.8-1.9 1.9-4.9 1.9-6.8 0-1.9-1.9-1.9-4.9 0-6.8 1.9-1.9 4.9-1.9 6.8 0z" fill="#fff"/></svg></span><span class="inj-project-card-approve-lbl">Planeta.ru рекомендует</span></span>'');
        }
    });
</script>'
WHERE alias = 'APPROVE';
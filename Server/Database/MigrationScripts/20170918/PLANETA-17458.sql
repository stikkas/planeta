CREATE SEQUENCE trashcan.seq_school_online_application_id
INCREMENT 1 START 1;

CREATE TABLE trashcan.school_online_applications (
  application_id BIGINT NOT NULL,
  name TEXT NOT NULL,
  email TEXT NOT NULL UNIQUE,
  time_added TIMESTAMP WITHOUT TIME ZONE NOT NULL,
  invite_sent BOOLEAN DEFAULT FALSE NOT NULL,
  PRIMARY KEY(application_id)
)
WITH (oids = false);

ALTER TABLE trashcan.school_online_applications
  ALTER COLUMN application_id SET STATISTICS 0;

ALTER TABLE trashcan.school_online_applications
  ALTER COLUMN name SET STATISTICS 0;

ALTER TABLE trashcan.school_online_applications
  ALTER COLUMN email SET STATISTICS 0;

ALTER TABLE trashcan.school_online_applications
  ALTER COLUMN time_added SET STATISTICS 0;

ALTER TABLE trashcan.school_online_applications
  ALTER COLUMN invite_sent SET STATISTICS 0;
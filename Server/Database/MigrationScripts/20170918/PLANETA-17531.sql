INSERT INTO commondb.mail_templates ("name", subject, footer, content_bbcode, from_address, content_html, reply_to_address, to_address, use_footer_and_header, skip_logging) 
	VALUES ('school.course_request', 'Ваша заявка принята.', NULL, '', 'noreply@planeta.ru', '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

    <head>

        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width" />

        <title>Welcome</title>

        
        
        

    
<style type="text/css">
.ReadMsgBody {
width: 100%;
}
.ExternalClass {
width: 100%;
}
body {
-webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%;
}
body {
width: 100% !important; min-width: 100%; margin: 0; padding: 0;
}
img {
outline: none; text-decoration: none; -ms-interpolation-mode: bicubic; display: block; vertical-align: middle;
}
body {
padding: 0; margin: 0;
}
body {
color: #515054; font-family: "Arial", sans-serif; font-weight: normal; text-align: left; line-height: 1.3;
}
a:hover {
color: #018cb2 !important;
}
.main-btn:hover {
text-decoration: none !important; color: #fff !important; border-color: #007dfa !important; background: #007dfa !important; box-shadow: 0 15px 30px rgba(0, 125, 250, 0.5) !important;
}
</style>
</head>

    <body style="-webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; width: 100% !important; min-width: 100%; color: #515054; font-family: ''Arial'', sans-serif; font-weight: normal; text-align: left; line-height: 1.3; margin: 0; padding: 0;">

        <div style="display: none; max-height: 0; min-height: 0; height: 0; font-size: 0; line-height: 0; overflow: hidden; margin: 0; padding: 0;">ТЭГЛАЙН&nbsp;&mdash; Главные новости Planeta.ru за&nbsp;неделю</div>

        <table style="border-spacing: 0; border-collapse: collapse; height: 100%; width: 100%; -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; color: #9d9d9d; font-family: ''Open Sans'', -apple-system, BlinkMacSystemFont, ''Segoe UI'', Roboto, Helvetica, Arial, sans-serif; font-weight: 300; text-align: left; line-height: 17px; font-size: 12px; background: #161f26; margin: 0; padding: 0;" bgcolor="#161f26">
            <tr>
                <td style="vertical-align: top; font-size: 0; line-height: 0; padding: 25px 0 0;" valign="top">&nbsp;</td>
            </tr>
            <tr>
                <td style="vertical-align: top; padding: 0;" valign="top">

                    <center style="width: 100%; min-width: 480px;">
                        <table style="border-spacing: 0; border-collapse: collapse; width: 480px; text-align: left; margin: auto;">

                            <tr>
                                <td style="vertical-align: top; text-align: center; padding: 0 0 25px;" align="center" valign="top">
                                    <a href="https://school.planeta.ru/" utm-content="headerlogo" style="color: #01c8fe; text-decoration: underline;">
                                        <img src="http://files.planeta.ru/mailer/interactive/i/logo.png" width="136" height="31" style="outline: none; text-decoration: none; -ms-interpolation-mode: bicubic; display: inline; vertical-align: middle; border: none;" />
                                    </a>
                                </td>
                            </tr>

                            
                            <tr>
                                <td style="vertical-align: top; padding: 0;" valign="top">
                                    <img src="http://files.planeta.ru/mailer/interactive/i/main-img.jpg" width="480" height="300" style="outline: none; text-decoration: none; -ms-interpolation-mode: bicubic; display: block; vertical-align: middle;" />
                                </td>
                            </tr>
                            

                            
                            <tr>
                                <td style="vertical-align: top; padding: 0;" valign="top">

                                    <table style="border-spacing: 0; border-collapse: collapse; width: 100%;">
                                        <tr>
                                            <td style="vertical-align: top; font-size: 24px; line-height: 33px; color: #000; text-align: center; background: #fff; padding: 29px 48px 4px;" align="center" valign="top" bgcolor="#fff">

                                                Спасибо, ${userName}!

                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="vertical-align: top; font-size: 16px; line-height: 24px; color: #454553; background: #fff; padding: 10px 48px 32px;" valign="top" bgcolor="#fff">

                                                <p style="font-size: inherit; line-height: inherit; color: inherit; margin: 1.5em 0;">Ваша заявка на&nbsp;участие в&nbsp;
                                                    <nobr>онлайн-курсе</nobr> &laquo;Создай
                                                    <nobr>краудфандиговый</nobr> проект за&nbsp;60&nbsp;минут&raquo; принята.</p>

                                                <p style="font-size: inherit; line-height: inherit; color: inherit; margin: 1.5em 0;">Теперь вы&nbsp;в&nbsp;числе первых, кто узнает о&nbsp;запуске курса. Ждите письма!</p>

                                                <p style="font-size: inherit; line-height: inherit; color: inherit; margin: 1.5em 0;">Ну, а&nbsp;пока мы&nbsp;наводим лоск в&nbsp;нашем
                                                    <nobr>онлайн-Хогвартсе</nobr> и&nbsp;спорим по&nbsp;поводу формы усов Егора Ельчина, ведущего курса, вот вам первое задание по&nbsp;вашему будущему проекту (или идее этого проекта, а&nbsp;если и&nbsp;её&nbsp;нет&nbsp;&mdash; фантазируйте!):</p>

                                                <ol style="font-size: inherit; line-height: inherit; color: inherit; padding-left: 36px; margin: 1.5em 0;">
                                                    <li style="font-size: inherit; line-height: inherit; color: inherit;"><b>Посчитайте</b>: сколько всего денег вам потребуется на&nbsp;реализацию вашей идеи?</li>
                                                    <li style="font-size: inherit; line-height: inherit; color: inherit;"><b>Оцените свои силы и&nbsp;время</b>: сколько времени вы&nbsp;готовы заниматься краудфандингом: 30, 60, 90 дней?</li>
                                                    <li style="font-size: inherit; line-height: inherit; color: inherit;"><b>Поразмыслите</b>: какие вознаграждения вы можете предложить спонсорам вашей
                                                        <nobr>крауд-кампании</nobr>? Хотя&nbsp;бы
                                                        <nobr>три-пять</nobr> вариантов, но&nbsp;лучше больше!</li>
                                                    <li style="font-size: inherit; line-height: inherit; color: inherit;"><b>Придумайте</b>: что вы&nbsp;будете говорить в видеообращении, набросайте тезисы, а&nbsp;в&nbsp;идеале&nbsp;&mdash; запишите черновик видео (да, можно прямо на&nbsp;телефон!)</li>
                                                </ol>

                                                <p style="font-size: inherit; line-height: inherit; color: inherit; margin: 1.5em 0;">Можете проигнорировать это задание, но, если подготовитесь, обучение вы&nbsp;пройдёте намного быстрее. Вас ожидают интерактивный видеокурс и&nbsp;масса дополнительных материалов к&nbsp;нему&nbsp;&mdash; в&nbsp;том числе механики новейших успешных
                                                    <nobr>крауд-проектов</nobr> Planeta.ru.</p>

                                                <p style="font-size: inherit; line-height: inherit; color: inherit; margin: 1.5em 0;">Обучение у&nbsp;нас традиционно бесплатное. Запуск курса мы&nbsp;планируем на&nbsp;
                                                    <nobr>середину-конец</nobr> сентября 2017&nbsp;г. Будем стараться быстрее, потому как ждем этого, как и&nbsp;вы, с&nbsp;нетерпением!</p>

                                                <div style="font-family: Georgia, ''Times New Roman'', Times, serif; font-style: italic; font-size: inherit; line-height: inherit; text-align: right; color: inherit;" align="right">
                                                    С&nbsp;уважением<span style="margin-right: -4.31px;">,</span>
                                                    <br>команда Planeta.ru
                                                </div>

                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="vertical-align: top; padding: 0;" valign="top">&nbsp;</td>
                                        </tr>
                                    </table>

                                </td>
                            </tr>
                            

                            
                            <tr>
                                <td style="vertical-align: top; font-size: 0; line-height: 0; padding: 4px 0 0;" valign="top">&nbsp;</td>
                            </tr>
                            <tr>
                                <td style="vertical-align: top; padding: 0;" valign="top">
                                    <table style="border-spacing: 0; border-collapse: collapse; width: 100%; background: #fff;" bgcolor="#fff">
                                        <tr>
                                            <td style="vertical-align: top; font-size: 14px; line-height: 24px; text-align: center; font-weight: 200; color: #454553; padding: 39px 35px 24px;" align="center" valign="top">
                                                <b>Делитесь <nobr>онлайн-курсом</nobr></b> &laquo;Создай краудфандинговый проект за&nbsp;60&nbsp;минут&raquo;&nbsp;&mdash; пусть все видят, что вы&nbsp;решительно&nbsp;настроены воплотить свою идею в&nbsp;жизнь!
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" style="vertical-align: top; padding: 0 0 45px;" valign="top">
                                                <table style="border-spacing: 0; border-collapse: collapse; margin: auto;">
                                                    <tr>
                                                        <td style="vertical-align: top; padding: 0 12px;" valign="top">
                                                            <a href="https://www.facebook.com/planetaru" style="color: #01c8fe; text-decoration: underline;">
                                                                <img src="http://files.planeta.ru/mailer/interactive/i/share-ico-fb.png" width="43" height="43" style="outline: none; text-decoration: none; -ms-interpolation-mode: bicubic; display: block; vertical-align: middle; border: none;" />
                                                            </a>
                                                        </td>
                                                        <td style="vertical-align: top; padding: 0 12px;" valign="top">
                                                            <a href="https://vk.com/planetaru" style="color: #01c8fe; text-decoration: underline;">
                                                                <img src="http://files.planeta.ru/mailer/interactive/i/share-ico-vk.png" width="43" height="43" style="outline: none; text-decoration: none; -ms-interpolation-mode: bicubic; display: block; vertical-align: middle; border: none;" />
                                                            </a>
                                                        </td>
                                                        <td style="vertical-align: top; padding: 0 12px;" valign="top">
                                                            <a href="https://ok.ru/planetaportal" style="color: #01c8fe; text-decoration: underline;">
                                                                <img src="http://files.planeta.ru/mailer/interactive/i/share-ico-ok.png" width="43" height="43" style="outline: none; text-decoration: none; -ms-interpolation-mode: bicubic; display: block; vertical-align: middle; border: none;" />
                                                            </a>
                                                        </td>
                                                        <td style="vertical-align: top; padding: 0 12px;" valign="top">
                                                            <a href="https://t.me/planetaru" style="color: #01c8fe; text-decoration: underline;">
                                                                <img src="http://files.planeta.ru/mailer/interactive/i/share-ico-tg.png" width="43" height="43" style="outline: none; text-decoration: none; -ms-interpolation-mode: bicubic; display: block; vertical-align: middle; border: none;" />
                                                            </a>
                                                        </td>
                                                        <td style="vertical-align: top; padding: 0 12px;" valign="top">
                                                            <a href="https://twitter.com/PlanetaPortal" style="color: #01c8fe; text-decoration: underline;">
                                                                <img src="http://files.planeta.ru/mailer/interactive/i/share-ico-tw.png" width="43" height="43" style="outline: none; text-decoration: none; -ms-interpolation-mode: bicubic; display: block; vertical-align: middle; border: none;" />
                                                            </a>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            

                            
                            <tr>
                                <td style="vertical-align: top; padding: 0;" valign="top">
                                    <table style="border-spacing: 0; border-collapse: collapse; width: 100%;">
                                        <tr>
                                            <td style="vertical-align: top; font-size: 0; line-height: 0; padding: 14px 0 0;" valign="top">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td style="vertical-align: top; font-size: 12px; line-height: 17px; text-align: left; padding: 18px 19px 0;" align="left" valign="top">
                                                Мы&nbsp;отправили это письмо на&nbsp;адрес <a href="mailto:cooryliof@gmail.com" style="color: #01c8fe; text-decoration: underline;">cooryliof@gmail.com</a>, так как он&nbsp;был оставлен на&nbsp;странице предварительной регистрации на&nbsp;
                                                <nobr>онлайн-курс</nobr> &laquo;Создай краудфандинговый проект за&nbsp;60&nbsp;минут&raquo; от&nbsp;Planeta.ru.
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="vertical-align: top; font-size: 12px; line-height: 17px; text-align: left; padding: 18px 19px 0;" align="left" valign="top">
                                                Если у&nbsp;вас остались вопросы, пишите нам на&nbsp;<a href="mailto:school@planeta.ru" style="color: #01c8fe; text-decoration: underline;">school@planeta.ru</a>.
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="vertical-align: top; font-size: 0; line-height: 0; padding: 14px 0 0;" valign="top">&nbsp;</td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            

                        </table>
                    </center>
                </td>
            </tr>
            <tr>
                <td style="vertical-align: top; font-size: 0; line-height: 0; padding: 25px 0 0;" valign="top">&nbsp;</td>
            </tr>
        </table>

    </body>

</html>', 'school@planeta.ru', '${userEmail}', false, false);

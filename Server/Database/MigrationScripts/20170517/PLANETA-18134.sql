CREATE TABLE IF NOT EXISTS trashcan.opened_emails (
  message_id BIGINT PRIMARY KEY NOT NULL,
  time_added TIMESTAMP WITHOUT TIME ZONE NOT NULL
) WITHOUT OIDS;

GRANT ALL ON trashcan.opened_emails TO planeta;

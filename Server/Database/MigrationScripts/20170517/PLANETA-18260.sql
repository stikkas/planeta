INSERT INTO commondb.configuration (key, str_value)
VALUES ('shortlink.extra.social.links', '[{
    "name": "fs",
    "urlParams": "utm_source=facebook&utm_medium=social&utm_campaign=planeta-social-networks&utm_content=link_text_image_video_audio"
}, {
    "name": "vs",
    "urlParams": "utm_source=vk&utm_medium=social&utm_campaign=planeta-social-networks&utm_content=link_text_image_video_audio"
}, {
    "name": "is",
    "urlParams": "utm_source=instagram&utm_medium=social&utm_campaign=planeta-social-networks&utm_content=text_image_video"
}, {
    "name": "fc",
    "urlParams": "utm_source=facebook&utm_medium=social&utm_campaign=planeta-charity-social-networks&utm_content=link_text_image_video_audio"
}, {
    "name": "vc",
    "urlParams": "utm_source=vk&utm_medium=social&utm_campaign=planeta-charity-social-networks&utm_content=link_text_image_video_audio"
}, {
    "name": "ic",
    "urlParams": "utm_source=instagram&utm_medium=social&utm_campaign=planeta-charity-social-networks&utm_content=text_image_video"
}]');
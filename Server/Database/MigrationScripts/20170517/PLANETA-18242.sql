ALTER TABLE profiledb.profiles DROP COLUMN event_time_begin;
ALTER TABLE profiledb.profiles DROP COLUMN event_time_end;
ALTER TABLE profiledb.profiles DROP COLUMN event_groups_count;
ALTER TABLE profiledb.profiles DROP COLUMN event_type;
ALTER TABLE profiledb.profiles DROP COLUMN event_place;
ALTER TABLE profiledb.profiles DROP COLUMN event_users_count;

ALTER TYPE profiledb.profile_news_type ADD VALUE 'PROFILE_SUBSCRIPTIONS_CHANGED';
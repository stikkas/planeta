CREATE TABLE trashcan.quiz (
  quiz_id BIGINT NOT NULL,
  alias TEXT UNIQUE,
  name TEXT NOT NULL,
  description TEXT,
  enabled BOOLEAN DEFAULT false NOT NULL,
  time_added TIMESTAMP WITHOUT TIME ZONE DEFAULT now() NOT NULL,
  PRIMARY KEY(quiz_id)
)
WITH (oids = false);

ALTER TABLE trashcan.quiz
  ALTER COLUMN quiz_id SET STATISTICS 0;

ALTER TABLE trashcan.quiz
  ALTER COLUMN name SET STATISTICS 0;

ALTER TABLE trashcan.quiz
  ALTER COLUMN description SET STATISTICS 0;

ALTER TABLE trashcan.quiz
  ALTER COLUMN enabled SET STATISTICS 0;

ALTER TABLE trashcan.quiz
  ALTER COLUMN time_added SET STATISTICS 0;

ALTER TABLE trashcan.quiz_question
  ADD COLUMN has_custom_radio BOOLEAN DEFAULT false NOT NULL;

ALTER TABLE trashcan.quiz_question
  ADD COLUMN custom_radio_text TEXT;


CREATE SEQUENCE trashcan.seq_quiz_id
INCREMENT 1 START 10;



CREATE TABLE trashcan.quiz_question (
  quiz_question_id BIGINT NOT NULL,
  question_text TEXT NOT NULL,
  type INTEGER NOT NULL,
  PRIMARY KEY(quiz_question_id)
)
WITH (oids = false);

ALTER TABLE trashcan.quiz_question
  ADD COLUMN start_value INTEGER DEFAULT 0 NOT NULL;

ALTER TABLE trashcan.quiz_question
  ADD COLUMN end_value INTEGER DEFAULT 0 NOT NULL;

ALTER TABLE trashcan.quiz_question
  ALTER COLUMN quiz_question_id SET STATISTICS 0;

ALTER TABLE trashcan.quiz_question
  ALTER COLUMN question_text SET STATISTICS 0;

ALTER TABLE trashcan.quiz_question
  ALTER COLUMN type SET STATISTICS 0;

CREATE SEQUENCE trashcan.seq_quiz_question_id
INCREMENT 1 START 10;


CREATE TABLE trashcan.quiz_question_option (
  quiz_question_option_id BIGINT NOT NULL,
  quiz_question_id BIGINT NOT NULL,
  option_text TEXT NOT NULL,
  PRIMARY KEY(quiz_question_option_id)
)
WITH (oids = false);

ALTER TABLE trashcan.quiz_question_option
  ADD COLUMN option_order_num INTEGER;

ALTER TABLE trashcan.quiz_question_option
  ALTER COLUMN quiz_question_option_id SET STATISTICS 0;

ALTER TABLE trashcan.quiz_question_option
  ALTER COLUMN quiz_question_id SET STATISTICS 0;

ALTER TABLE trashcan.quiz_question_option
  ALTER COLUMN option_text SET STATISTICS 0;

CREATE SEQUENCE trashcan.seq_quiz_question_option_id
INCREMENT 1 START 10;



CREATE TABLE trashcan.quiz_answer (
  quiz_answer_id BIGINT NOT NULL,
  quiz_id BIGINT NOT NULL,
  quiz_question_id BIGINT NOT NULL,
  quiz_question_option_id BIGINT DEFAULT 0 NOT NULL,
  answer_custom_text TEXT,
  answer_integer INTEGER,
  answer_boolean BOOLEAN,
  user_id BIGINT NOT NULL,
  time_added TIMESTAMP(20) WITHOUT TIME ZONE NOT NULL,
  PRIMARY KEY(quiz_answer_id)
)
WITH (oids = false);

ALTER TABLE trashcan.quiz_answer
  ALTER COLUMN quiz_answer_id SET STATISTICS 0;

ALTER TABLE trashcan.quiz_answer
  ALTER COLUMN quiz_id SET STATISTICS 0;

ALTER TABLE trashcan.quiz_answer
  ALTER COLUMN quiz_question_id SET STATISTICS 0;

ALTER TABLE trashcan.quiz_answer
  ALTER COLUMN quiz_question_option_id SET STATISTICS 0;

ALTER TABLE trashcan.quiz_answer
  ALTER COLUMN answer_custom_text SET STATISTICS 0;

ALTER TABLE trashcan.quiz_answer
  ALTER COLUMN user_id SET STATISTICS 0;

ALTER TABLE trashcan.quiz_answer
  ALTER COLUMN time_added SET STATISTICS 0;

CREATE SEQUENCE trashcan.seq_quiz_answer_id
INCREMENT 1 START 10;



CREATE TABLE trashcan."quiz_question_relation" (
  quiz_question_relation_id BIGINT NOT NULL,
  quiz_id BIGINT NOT NULL,
  quiz_question_id BIGINT NOT NULL,
  quiz_question_order_num INTEGER,
  PRIMARY KEY(quiz_question_relation_id)
)
WITH (oids = false);

ALTER TABLE trashcan."quiz_question_relation"
  ALTER COLUMN quiz_question_relation_id SET STATISTICS 0;

ALTER TABLE trashcan."quiz_question_relation"
  ALTER COLUMN quiz_id SET STATISTICS 0;

ALTER TABLE trashcan."quiz_question_relation"
  ALTER COLUMN quiz_question_id SET STATISTICS 0;

ALTER TABLE trashcan."quiz_question_relation"
  ALTER COLUMN quiz_question_order_num SET STATISTICS 0;

CREATE UNIQUE INDEX "quiz_question_relation_idx" ON trashcan."quiz_question_relation"
USING btree (quiz_id, quiz_question_id);

CREATE SEQUENCE trashcan.seq_quiz_question_relation_id
INCREMENT 1 START 10;

CREATE TABLE trashcan.quiz_invite_mail (
  quiz_invite_mail_id BIGINT NOT NULL,
  quiz_id BIGINT NOT NULL,
  user_id BIGINT NOT NULL,
  time_added TIMESTAMP WITHOUT TIME ZONE NOT NULL,
  PRIMARY KEY(quiz_invite_mail_id)
)
WITH (oids = false);

ALTER TABLE trashcan.quiz_invite_mail
  ALTER COLUMN quiz_invite_mail_id SET STATISTICS 0;

ALTER TABLE trashcan.quiz_invite_mail
  ALTER COLUMN quiz_id SET STATISTICS 0;

ALTER TABLE trashcan.quiz_invite_mail
  ALTER COLUMN user_id SET STATISTICS 0;

ALTER TABLE trashcan.quiz_invite_mail
  ALTER COLUMN time_added SET STATISTICS 0;
CREATE TABLE trashcan.user_feedback (
  user_feedback_id BIGINT NOT NULL,
  "order_id" BIGINT,
  "user_id" BIGINT NOT NULL,
  "campaign_id" BIGINT,
  score INTEGER NOT NULL,
  extra_testing BOOLEAN DEFAULT FALSE NOT NULL,
  page_type BIGINT NOT NULL,
  time_added TIMESTAMP WITHOUT TIME ZONE DEFAULT now() NOT NULL,
  PRIMARY KEY(user_feedback_id)
)
WITH (oids = false);

ALTER TABLE trashcan.user_feedback
  ALTER COLUMN user_feedback_id SET STATISTICS 0;

ALTER TABLE trashcan.user_feedback
  ALTER COLUMN "order_id" SET STATISTICS 0;

ALTER TABLE trashcan.user_feedback
  ALTER COLUMN "user_id" SET STATISTICS 0;

ALTER TABLE trashcan.user_feedback
  ALTER COLUMN "campaign_id" SET STATISTICS 0;

ALTER TABLE trashcan.user_feedback
  ALTER COLUMN score SET STATISTICS 0;

ALTER TABLE trashcan.user_feedback
  ALTER COLUMN extra_testing SET STATISTICS 0;

ALTER TABLE trashcan.user_feedback
  ALTER COLUMN page_type SET STATISTICS 0;

ALTER TABLE trashcan.user_feedback
  ALTER COLUMN time_added SET STATISTICS 0;

CREATE SEQUENCE trashcan.seq_user_feedback_id
INCREMENT 1 START 1;

CREATE INDEX user_feedback_order_id_idx ON trashcan.user_feedback
USING btree (order_id);

CREATE INDEX user_feedback_user_id_idx ON trashcan.user_feedback
USING btree (user_id);

CREATE INDEX user_feedback_campaign_id_idx ON trashcan.user_feedback
USING btree (campaign_id);
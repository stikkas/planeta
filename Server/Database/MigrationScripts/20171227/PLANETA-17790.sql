alter table shopdb.products add column time_started TIMESTAMP;
update shopdb.products set time_started = now();
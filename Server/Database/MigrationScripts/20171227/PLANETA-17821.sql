drop function commondb.can_be_vip(profile_id bigint, limit_count integer, amount integer, second_amount integer);

create function commondb.can_be_vip(profile_id bigint, limit_count integer, amount integer, second_amount integer) returns boolean
LANGUAGE plpgsql
AS $$
DECLARE
  last_time_bonus timestamp;
  bonuses_count integer;
  orders_count integer;
  last_amount numeric;
  year_start timestamp;
BEGIN
  select date_trunc('year', now()) into year_start;
  SELECT o.time_added into last_time_bonus FROM commondb.orders o
  WHERE o.order_type = 7 AND (o.payment_status = 0 OR o.payment_status = 1) AND o.buyer_id = profile_id ORDER BY o.time_added DESC LIMIT 1;
  SELECT count(o.order_id) into bonuses_count FROM commondb.orders o
  WHERE o.order_type = 7 AND (o.payment_status = 0 OR o.payment_status = 1) AND o.buyer_id = profile_id AND o.time_added > year_start;
  if bonuses_count >= 12 THEN
    RETURN false;
  END IF;
  IF  last_time_bonus IS NULL THEN
    SELECT COUNT(o.order_id), SUM(o.total_price) into orders_count, last_amount
    FROM commondb.orders o
    WHERE o.order_type IN (1, 2, 11) AND o.payment_status = 1
          AND o.buyer_id = profile_id AND o.time_added > year_start GROUP BY o.buyer_id;
    RETURN orders_count >= limit_count AND last_amount >= amount;
  ELSE
    SELECT SUM(o.total_price) into last_amount
    FROM commondb.orders o
    WHERE o.order_type IN (1, 2, 11) AND o.payment_status = 1
          AND o.buyer_id = profile_id AND o.time_added > last_time_bonus AND o.time_added > year_start GROUP BY o.buyer_id;
    RETURN last_amount >= second_amount;
  END IF;
END;
$$;
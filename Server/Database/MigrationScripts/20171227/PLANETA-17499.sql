ALTER TABLE trashcan.quiz_question
  ADD COLUMN enabled BOOLEAN DEFAULT true NOT NULL;
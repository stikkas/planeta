alter table shopdb.products add column show_on_campaign boolean not null default true;
alter table shopdb.products add column campaign_ids bigint[];
UPDATE commondb.users_private_info
SET username = '_DELETED_' || username || '_'
WHERE user_id IN (SELECT user_id
                  FROM commondb.users_private_info
                  WHERE email ILIKE '_DELETED_%' AND NOT (username ILIKE '_DELETED_%'));

ALTER TABLE profiledb.profiles ADD COLUMN time_added timestamp without time zone;
ALTER TABLE profiledb.profiles ADD COLUMN time_updated timestamp without time zone;

ALTER TABLE profiledb.profile_sites ADD COLUMN time_added timestamp without time zone;
ALTER TABLE profiledb.profile_sites ADD COLUMN time_updated timestamp without time zone;
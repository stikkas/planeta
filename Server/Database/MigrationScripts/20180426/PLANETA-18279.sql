INSERT INTO commondb.mail_templates (template_id, name, subject, footer, content_bbcode, from_address, content_html, reply_to_address, to_address, use_footer_and_header, skip_logging) 
VALUES 
  (nextval('commondb.seq_mail_template_id'), 'photobookfest.promo', 'Ваш пригласительный на Photobookfest', null, '', 'noreply@planeta.ru', '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"><html><head><title>Фотобукфест</title><meta http-equiv="Content-Type" content="text/html; charset=utf-8"/><meta name="viewport" content="width=device-width"/></head><body style="-webkit-text-size-adjust: 100%; margin: 0; padding: 0; -ms-text-size-adjust: 100%; width: 100% !important; min-width: 100%; color: #515054; font-family: ''Arial'', sans-serif; font-weight: normal; text-align: left; line-height: 16px; font-size: 12px;"><div style="display: none; margin: 0; padding: 0; max-height: 0; min-height: 0; height: 0; font-size: 0; line-height: 0; overflow: hidden;">Planeta.ru и не только</div><table style="border-spacing: 0; margin: 0; padding: 0; border-collapse: collapse; height: 100%; width: 100%; -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; color: #454553; font-family: ''Open Sans'', -apple-system, BlinkMacSystemFont, ''Segoe UI'', Roboto, Helvetica, Arial, sans-serif; font-weight: 100; text-align: left; line-height: 1.4; font-size: 14px; background-color: #f4f4f4;" bgcolor="#f4f4f4"><tr><td style="vertical-align: top; font-size: 0; line-height: 0; padding: 25px 0 0;" valign="top">&nbsp;</td></tr><tr><td style="vertical-align: top; padding: 0;" valign="top"><center style="width: 100%; min-width: 480px;"><table style="border-spacing: 0; border-collapse: collapse; width: 480px; text-align: left; margin: auto;"><tr><td align="center" style="vertical-align: top; padding: 0 0 25px;" valign="top"><table style="border-spacing: 0; border-collapse: collapse;"><tr><td style="vertical-align: middle; padding: 0 15px;" valign="middle"><a href="https://planeta.ru/" style="color: #1a8cff; text-decoration: underline;"><img style="outline: none; text-decoration: none; -ms-interpolation-mode: bicubic; display: inline; vertical-align: middle; border: none;" src="https://s4.planeta.ru/i/1dd559/1516348789150_renamed.jpg"/></a></td></tr></table></td></tr><tr><td style="vertical-align: top; padding: 0;" valign="top"><table style="border-spacing: 0; border-collapse: collapse; width: 100%;"><tr><td style="vertical-align: top; padding: 0;" valign="top"><div><img style="outline: none; text-decoration: none; -ms-interpolation-mode: bicubic; display: block; vertical-align: middle; border: none;" src="https://s4.planeta.ru/i/1fdb9a/1523973280034_renamed.jpg"/></div></td></tr><tr><td style="vertical-align: top; background-color: #fff; border-bottom-style: solid; border-bottom-color: #1a8cff; border-bottom-width: 4px; padding: 31px 46px 40px;" bgcolor="#fff" valign="top"><table style="border-spacing: 0; border-collapse: collapse; width: 100%;"><tr><td style="vertical-align: top; font-size: 24px; line-height: 33px; font-weight: 300; text-align: center; color: #000; padding: 0 0 32px;" align="center" valign="top"></td></tr><tr><td style="vertical-align: top; font-size: 16px; line-height: 24px; font-weight: 300; text-align: left; padding: 0;" align="left" valign="top"><div>
<div>Здравствуйте!</div>

<div>&nbsp;</div>

<div>Спасибо, что поддерживаете фотопроекты на&nbsp;Planeta.ru!</div>

<div>Вас ждёт подарок от&nbsp;нашего партнёра&nbsp;&mdash; бесплатное посещение всех выставок и&nbsp;лекций московского международного фестиваля актуальной фотографии и&nbsp;фотографической книги Photobookfest 2018 (кроме закрытых мастер-классов и&nbsp;воркшопов).</div>

<div>&nbsp;</div>

<div>Photobookfest проходит в&nbsp;Центре фотографии имени братьев Люмьер (Москва, Болотная набережная, д.&nbsp;3, стр.&nbsp;1) с&nbsp;20&nbsp;апреля по&nbsp;3&nbsp;июня 2018 года.</div>

<div>&nbsp;</div>

<div><strong>Как получить бесплатный пригласительный на&nbsp;Photobookfest?</strong></div>

<ul>
    <li>Напишите, пожалуйста, электронное письмо со&nbsp;своими фамилией, именем и&nbsp;номером заказа в&nbsp;крауд-проекте (он&nbsp;пришёл в&nbsp;подтверждающем покупку в&nbsp;проекте письме) на&nbsp;<strong><a href="mailto:press@lumiere.ru?subject=%D0%9F%D0%BB%D0%B0%D0%BD%D0%B5%D1%82%D0%B0" rel="noreferrer" target="_blank">press@lumiere.ru</a></strong>.</li>
    <li>В&nbsp;теме письма укажите, пожалуйста, &laquo;Планета&raquo;. Организаторы фестиваля внесут Вас в&nbsp;списки гостей, на&nbsp;входе нужно будет только представиться.</li>
    <li>Обратите внимание, что единоразовое участие в&nbsp;крауд-проекте даёт право на&nbsp;получение пригласительного билета на&nbsp;одно лицо в&nbsp;течение одного буднего или рабочего дня, выбранного Вами.<br>
    &nbsp;</li>
</ul>

<div><strong>А&nbsp;если я&nbsp;живу не&nbsp;в&nbsp;Москве?</strong><br>
<br>
Photobookfest 2018 проходит с&nbsp;20&nbsp;апреля по&nbsp;3&nbsp;июня. Если Вы&nbsp;окажетесь в&nbsp;Москве в&nbsp;это время, будем рады видеть Вас на&nbsp;фестивале. Если у&nbsp;Вас не&nbsp;получится присутствовать лично, Вы&nbsp;можете пригласить вместо себя кого-то из&nbsp;знакомых.<br>
<br>
<strong>Полезные ссылки:</strong><br>
Программа и&nbsp;полное расписание Photobookfest&nbsp;&mdash;&nbsp;<a href="http://photobookfest.com/calendar" rel="noreferrer" target="_blank">здесь</a>.<br>
Время работы и&nbsp;схема проезда к&nbsp;Центру братьев Люмьер&nbsp;&mdash;&nbsp;<a href="http://www.lumiere.ru/contacts/" rel="noreferrer" target="_blank">здесь.</a><br>
&nbsp;</div>

<div>Приятных впечатлений!&nbsp;</div>

<div>
<div align="right" style="text-align: right; color: #8a887c; font-size: 14px; font-style: italic;"></div>

<div>&nbsp;</div>

<div align="center" style="text-align: center;"><strong><a href="mailto:press@lumiere.ru?subject=%D0%9F%D0%BB%D0%B0%D0%BD%D0%B5%D1%82%D0%B0" isbutton="" style="color: rgb(255, 255, 255); border-color: rgb(26, 140, 255); border-style: solid; border-width: 25px 58px; text-decoration: none; display: inline-block; vertical-align: top; font-size: 16px; line-height: 20px; white-space: nowrap; background: rgb(26, 140, 255); box-shadow: rgba(26, 140, 255, 0.5) 0px 15px 30px; border-radius: 5px;" target="_blank">Получить пригласительный</a></strong></div>

<div style="font-size: 35px; line-height: 1;">&nbsp;&nbsp;</div>

<div style="line-height: 1.4; font-size: 13px; color: rgb(69, 69, 83); text-align: right;"><em><span style="font-size:16px;"><span style="color: rgb(127, 140, 141); line-height: 1.4;">С&nbsp;уважением,<br>
команда Planeta.ru</span></span></em></div>
</div>
</div>

<div>&nbsp;</div></td></tr></table></td></tr></table></td></tr><tr><td style="vertical-align: top; padding: 0;" valign="top"><table style="border-spacing: 0; border-collapse: collapse; width: 100%;"><tr><td style="vertical-align: top; font-size: 12px; line-height: 17px; font-weight: 300; text-align: center; color: #454553; padding: 19px 0 0;" align="center" valign="top"><div>Данное сообщение отправлено автоматически, пожалуйста,<br>
не&nbsp;отвечайте на&nbsp;него. Пишите нам на&nbsp;<a href="mailto:support@planeta.ru" style="color: #1a8cff; text-decoration: underline;" target="_blank">support@planeta.ru</a>.</div></td></tr></table></td></tr></table></center></td></tr><tr><td style="vertical-align: top; font-size: 0; line-height: 0; padding: 25px 0 0;" valign="top">&nbsp;</td></tr></table></body></html>', '', '${userEmail}', false, false);


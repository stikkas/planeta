create TABLE trashcan.promo_config (
  promo_id BIGINT NOT NULL PRIMARY KEY ,
  name varchar(250) NOT NULL ,
  status INTEGER NOT NULL DEFAULT 0,
  campaign_tag BIGINT[],
  mail_template varchar(250),
  time_start timestamp,
  time_finish timestamp,
  has_promocode BOOLEAN DEFAULT FALSE ,
  price_condition NUMERIC(10,2),
  usage_count INTEGER DEFAULT 0
);

grant select, insert, update, delete on trashcan.promo_config to planeta;

insert into trashcan.promo_config(promo_id, name, status, campaign_tag, mail_template, time_start, time_finish, has_promocode, price_condition, usage_count)
    VALUES
      (1, 'IVI', 1, '{17}', 'ivi.promo', '2018-01-01', '2018-04-16', true, 500, 0),
      (5, 'Expert', 1, '{24,25}', 'expert.promo', '2018-01-01', '2018-06-01', false, 1, 0),
      (3, 'Bookmate', 1, '{3}', 'bookmate.promo', '2018-01-01', '2018-07-16', true, 100, 1),
      (4, 'Photobookfest', 1, '{5}', 'photobookfest.promo', '2018-01-01', '2018-06-02', false, 1, 0);

create SEQUENCE trashcan.promo_config_id_seq START 6 INCREMENT 1;

grant select, update on trashcan.promo_config_id_seq to planeta;



create table trashcan.promo_emails (
  email_id BIGINT NOT NULL PRIMARY KEY,
  promo_id BIGINT NOT NULL,
  promo_code_id BIGINT,
  email text NOT NULL,
  time_sent TIMESTAMP,
  order_id BIGINT
);

grant select, insert, update, delete on trashcan.promo_emails to planeta;

create SEQUENCE trashcan.promo_email_id_seq START 1 INCREMENT 1;

grant select, update on trashcan.promo_email_id_seq to planeta;


ALTER TABLE trashcan.external_promo_code RENAME COLUMN promo_id TO code_id;
ALTER TABLE trashcan.external_promo_code RENAME COLUMN type TO promo_config_id;

INSERT INTO trashcan.promo_emails (email_id, promo_id, promo_code_id, email, time_sent, order_id)
  SELECT
    nextval('trashcan.promo_email_id_seq'),
    promo_config_id,
    code_id,
    email,
    time_added,
    order_id
  FROM trashcan.external_promo_code
  WHERE status = 1;

  
drop TABLE trashcan.ivi_promo_code;
drop TABLE trashcan.yandex_music_promo_code;


CREATE INDEX promo_config_conditions_idx on trashcan.promo_config(campaign_tag, price_condition, time_start, time_finish);
CREATE INDEX promo_emails_email_idx on trashcan.promo_emails(email);


alter table trashcan.promo_config ADD COLUMN creator_profile_id BIGINT;
alter table trashcan.promo_config ADD COLUMN updater_profile_id BIGINT;
alter table trashcan.promo_config ADD COLUMN time_added timestamp;
alter table trashcan.promo_config ADD COLUMN time_updated timestamp;
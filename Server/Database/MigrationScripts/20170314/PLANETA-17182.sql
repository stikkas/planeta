ALTER TABLE commondb.campaigns
  ADD COLUMN vk_targeting_code TEXT;

ALTER TABLE commondb.campaigns
  ALTER COLUMN vk_targeting_code SET DEFAULT NULL;

ALTER TABLE commondb.campaigns
  ADD COLUMN fb_targeting_code TEXT;

ALTER TABLE commondb.campaigns
  ALTER COLUMN fb_targeting_code SET DEFAULT NULL;
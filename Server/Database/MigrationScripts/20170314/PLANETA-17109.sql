CREATE OR REPLACE FUNCTION maildb.collect_info (
)
  RETURNS void AS
$body$
DECLARE
  rec RECORD;
  rec2 RECORD;
BEGIN
  drop table if exists tmp_values;
  create temp table tmp_values(
    profile_id bigint,

    email TEXT NOT NULL,
    display_name TEXT,
    last_active TIMESTAMP(0) WITHOUT TIME ZONE,

    country TEXT,
    region TEXT,
    city TEXT,
    user_birth_date TIMESTAMP(0) WITHOUT TIME ZONE,
    balance NUMERIC,
    days_registered BIGINT,
    registration_source TEXT,
    CONSTRAINT tmp_values_pkey PRIMARY KEY(profile_id)
  );
  DROP INDEX IF EXISTS tmp_values_email;
  CREATE UNIQUE INDEX tmp_values_email ON tmp_values(email);

  insert into tmp_values
    SELECT profile.profile_id,
      private_info.email,
      profile.display_name,
      now() as last_active,
      profile.country_id,
      COALESCE((SELECT region_id FROM profiledb.city city WHERE city.city_id = profile.city_id),0) AS region_id,
      profile.city_id,
      profile.user_birth_date,
      balance.balance,
      date_part('days', now() - private_info.time_added) as days_registered,
      (case
       when enter_url ~ 'utm_source=mixuni' then '7'
       when referrer_url ~ 'vk.com' and enter_url !~ 'offers_adv' then '1'
       when referrer_url ~ 'vkontakte.ru' and enter_url !~ 'offers_adv' then '1'
       when referrer_url ~ 'vk.com' and enter_url ~ 'offers_adv' then '6'
       when referrer_url ~ 'vkontakte.ru' and enter_url ~ 'offers_adv' then '6'
       when referrer_url ~ 'google.com' then '2'
       when referrer_url ~ 'yandex.com' then '2'
       when referrer_url ~ 'go.mail.ru' then '2'
       when referrer_url ~ 'mail.ru' then '2'
       when referrer_url ~ 'facebook.com' then '3'
       when referrer_url = '' then '4'
       when referrer_url = NULL then '4'
       when referrer_url ~ 'planeta.ru' then '3'
       else '5' end) as registration_source
    FROM profiledb.profiles profile
      JOIN commondb.users_private_info  private_info ON private_info.user_id = profile.profile_id
      LEFT JOIN commondb.profile_balances balance ON balance.profile_id = profile.profile_id
      LEFT JOIN commondb.user_sources src ON src.profile_id = profile.profile_id
    WHERE profile_type_id = 1 --active user (email confirm)
          AND profile.status = 1 --user
          AND profile.is_receive_newsletters
          AND private_info.email IS NOT NULL
          AND private_info.email != ''
          AND EXISTS(select 1 from profiledb.users usr WHERE usr.profile_id = profile.profile_id)
  ;






  drop table if exists tmp_shares1;
  create temp table tmp_shares1(
    profile_id bigint,
    share_last_order TIMESTAMP WITHOUT TIME ZONE,
    share_max_price NUMERIC,
    share_count BIGINT,
    CONSTRAINT tmp_shares1_pkey PRIMARY KEY(profile_id)
  );

  insert into tmp_shares1
    SELECT t3.buyer_id, max(t3.time_added) as share_last_order, max(t4.price) as share_max_price, count(t4.price) as share_count
    FROM commondb.orders t3
      LEFT JOIN commondb.order_objects t4
        ON t3.order_id = t4.order_id
    WHERE t4.order_object_type = 1
          AND t3.payment_status = 1
    GROUP BY buyer_id;





  drop table if exists tmp_shares ;
  create temp table tmp_shares(
    profile_id bigint,
    share_ids TEXT[],
    campaign_category_types TEXT[],
    CONSTRAINT tmp_shares_pkey PRIMARY KEY(profile_id)
  );

  insert into tmp_shares
    SELECT ord.buyer_id ,array_agg(share.share_id)
    FROM commondb.orders ord
      JOIN commondb.order_objects object ON ord.order_id = object.order_id
      JOIN commondb.shares share ON share.share_id = object.object_id
    WHERE ord.payment_status = 1
    group by ord.buyer_id;


  update tmp_shares
  set campaign_category_types =
  (
    select array_agg(tag_id) campaign_category_types
    from(
          SELECT distinct ord.buyer_id , (select tag_id from commondb.campaign_tag_relations tr where tr.campaign_id = campaign.campaign_id order by order_num asc limit 1) as tag_id
          FROM commondb.orders ord
            JOIN commondb.order_objects object ON ord.order_id = object.order_id
            JOIN commondb.shares share ON share.share_id = object.object_id
            JOIN commondb.campaigns campaign ON campaign.campaign_id = share.campaign_id
          WHERE ord.payment_status = 1
                and ord.buyer_id = tmp_shares.profile_id
        ) as t1
    group by buyer_id);





  drop table if exists tmp_subscribers;
  create temp table tmp_subscribers(
    profile_id bigint,
    subscribers TEXT[],
    CONSTRAINT tmp_subscribers_pkey PRIMARY KEY(profile_id)
  );
  insert into tmp_subscribers
    SELECT profile_id, array_agg(subject_profile_id)
    from profiledb.profile_subscription
    GROUP BY profile_id;





  UPDATE maildb.values
  SET
    email                   = tmp_values.email,
    display_name            = tmp_values.display_name,
    last_active             = tmp_values.last_active,
    country                 = tmp_values.country,
    region                  = tmp_values.region,
    city                    = tmp_values.city,
    user_birth_date         = tmp_values.user_birth_date,
    balance                 = tmp_values.balance,
    days_registered         = tmp_values.days_registered,
    registration_source     = tmp_values.registration_source,

    share_last_order        = tmp_shares1.share_last_order,
    share_max_price         = tmp_shares1.share_max_price,
    share_count             = coalesce(tmp_shares1.share_count,0),

    share_ids               = tmp_shares.share_ids,
    campaign_category_types = tmp_shares.campaign_category_types,

    subscribers             = tmp_subscribers.subscribers
  FROM tmp_values
    LEFT JOIN tmp_shares       on tmp_shares.profile_id = tmp_values.profile_id
    LEFT JOIN tmp_shares1      on tmp_shares1.profile_id = tmp_values.profile_id
    LEFT JOIN tmp_subscribers  on tmp_subscribers.profile_id = tmp_values.profile_id
  WHERE tmp_values.email = maildb.values.email
  ;

END;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
ALTER TABLE statdb.widgets_stats ALTER COLUMN referer TYPE TEXT;
ALTER TABLE statdb.campaign_stats_ref ALTER COLUMN referer TYPE TEXT;

-- бэкап таблицы stat_events
create table test.stat_events_bak as select * from commondb.stat_events;


------------------------------------
------------------------------------
------------------------------------
------------------------------------
create or replace function commondb.campaign_stats_general_interval(date_from timestamp without time zone, date_to timestamp without time zone) returns TABLE(date date, campaign_id bigint, visitors bigint, views bigint, purchases bigint, buyers bigint, amount numeric, affiliate_amount numeric, comments bigint)
LANGUAGE plpgsql
AS $$
BEGIN
  RETURN QUERY

  select
    c.stat_date as date,
    c.campaign_id,
    c.visitors,
    c.views,
    coalesce(b.purchases, 0),
    coalesce(b.buyers, 0),
    coalesce(b.amount, 0),
    0::numeric as affiliate_amount,
    c.comments
  from
    (SELECT a.stat_date,
       a.campaign_id,
       count(DISTINCT a.visitor_id) AS visitors,
       sum(a.views) AS views,
       sum(a.comments) AS comments
     FROM ( SELECT (date_trunc('DAY'::text, e.date))::date AS stat_date,
                   e.object_id AS campaign_id,
                   CASE e.type
                   WHEN 'CAMPAIGN_VIEW'::text THEN e.visitor_id
                   ELSE NULL::bigint
                   END AS visitor_id,
                   CASE e.type
                   WHEN 'CAMPAIGN_VIEW'::text THEN 1
                   ELSE 0
                   END AS views,
                   CASE e.type
                   WHEN 'CAMPAIGN_COMMENT'::text THEN 1
                   ELSE 0
                   END AS comments
            FROM commondb.stat_events e
            WHERE e.date between date_from and date_to) a
     GROUP BY a.stat_date, a.campaign_id) c
    LEFT JOIN
    (
      SELECT
        t.campaign_id,
        t.stat_date,
        count(distinct t.order_id) as purchases,
        count(distinct t.buyer_id) as buyers,
        sum(t.amount_net) as amount
      FROM
        (
          select distinct s.campaign_id,
            date_trunc('DAY'::text, dt.time_added)::date AS stat_date,
            o.order_id,
            o.buyer_id,
            dt.amount_net from
            commondb.shares s
            JOIN commondb.order_objects oo on oo.object_id = s.share_id
            JOIN commondb.orders o on o.order_id = oo.order_id
            JOIN commondb.debit_transactions dt ON dt.transaction_id = o.debit_transaction_id
          WHERE dt.time_added between date_from and date_to
                AND o.payment_status = 1
                AND o.order_type = 1
        ) t
      GROUP BY t.campaign_id, stat_date
    ) b
      on c.campaign_id = b.campaign_id
         and c.stat_date = b.stat_date;
END;
$$;


create or replace function commondb.campaign_stats_country_interval(date_from timestamp without time zone, date_to timestamp without time zone) returns TABLE(date date, campaign_id bigint, country character varying, visitors bigint, views bigint, purchases bigint, affiliate_amount numeric)
LANGUAGE plpgsql
AS $$
BEGIN
  RETURN QUERY
  select
    c.date,
    c.campaign_id,
    c.country,
    c.visitors,
    c.views,
    coalesce(b.purchases,0) as purchases,
    0::numeric as affiliate_amount
  from
    (
      SELECT a.date,
        a.campaign_id,
        a.country,
        count(DISTINCT a.visitor_id) AS visitors,
        sum(a.views) AS views,
        sum(a.affiliate_amount) AS affiliate_amount
      FROM ( SELECT (date_trunc('DAY'::text, e.date))::date AS date,
                    e.object_id AS campaign_id,
               e.country,
               visitor_id,
               e.affiliate_amount,
                    1 as views
             FROM commondb.stat_events e
             WHERE e.date between date_from and date_to and type = 'CAMPAIGN_VIEW') a
      GROUP BY a.date, a.campaign_id, a.country
    ) c
    LEFT JOIN
    (
      SELECT
        s.campaign_id as campaign_id,
        coalesce(pc.country_name_ru, '') as country,
        count(distinct ogs.order_id) as purchases
      FROM commondb.shares s
        JOIN commondb.order_objects oo on oo.object_id = s.share_id
        JOIN commondb.orders o on o.order_id = oo.order_id
        JOIN statdb.order_geo_stats ogs on ogs.order_id = o.order_id
        LEFT JOIN profiledb.country pc on pc.country_id = ogs.country_id
      WHERE o.time_added between date_from and date_to
            AND o.payment_status in (1,2)
            AND o.order_type = 1
      GROUP BY s.campaign_id, pc.country_name_ru
    ) b
      on c.campaign_id = b.campaign_id
         and c.country = b.country;
END;
$$;


create or replace function commondb.campaign_stats_city_interval(date_from timestamp without time zone, date_to timestamp without time zone) returns TABLE(date date, campaign_id bigint, city character varying, visitors bigint, views bigint, purchases bigint, affiliate_amount numeric)
LANGUAGE plpgsql
AS $$
BEGIN
  RETURN QUERY
  select
    c.date,
    c.campaign_id,
    c.city,
    c.visitors,
    c.views,
    coalesce(b.purchases,0) as purchases,
    0::numeric as affiliate_amount
  from
    (
      SELECT a.date,
        a.campaign_id,
        a.city,
        count(DISTINCT a.visitor_id) AS visitors,
        sum(a.views) AS views,
        sum(a.affiliate_amount) AS affiliate_amount
      FROM ( SELECT (date_trunc('DAY'::text, e.date))::date AS date,
                    e.object_id AS campaign_id,
               e.city,
               visitor_id,
               e.affiliate_amount,
                    1 AS views
             FROM commondb.stat_events e
             WHERE e.date between date_from and date_to and type = 'CAMPAIGN_VIEW') a
      GROUP BY a.date, a.campaign_id, a.city
    ) c
    LEFT JOIN
    (
      SELECT
        s.campaign_id as campaign_id,
        coalesce(pc.city_name_ru, '') as city,
        count(distinct ogs.order_id) as purchases
      FROM commondb.shares s
        JOIN commondb.order_objects oo on oo.object_id = s.share_id
        JOIN commondb.orders o on o.order_id = oo.order_id
        JOIN statdb.order_geo_stats ogs on ogs.order_id = o.order_id
        LEFT JOIN profiledb.city pc on pc.city_id = ogs.city_id
      WHERE o.time_added between date_from and date_to
            AND o.payment_status in (1,2)
            AND o.order_type = 1
      GROUP BY s.campaign_id, pc.city_name_ru
    ) b
      on c.campaign_id = b.campaign_id
         and c.city = b.city;
END;
$$;



create or replace function commondb.campaign_stats_ref_interval(date_from timestamp without time zone, date_to timestamp without time zone) returns TABLE(date date, campaign_id bigint, referer character varying, visitors bigint, views bigint, purchases bigint, affiliate_amount numeric)
LANGUAGE plpgsql
AS $$
BEGIN
  RETURN QUERY
  select
    c.stat_date as date,
    c.campaign_id,
    c.referer,
    c.visitors,
    c.views,
    coalesce(b.purchases,0) as purchases,
    0::numeric as affiliate_amount
  from
    (
      SELECT a.stat_date,
        a.campaign_id,
        a.referer,
        count(DISTINCT a.visitor_id) AS visitors,
        sum(a.views) AS views
      FROM ( SELECT (date_trunc('DAY'::text, e.date))::date AS stat_date,
                    e.object_id AS campaign_id,
               e.referer,
               e.visitor_id,
                    1 AS views
             FROM commondb.stat_events e
             WHERE e.date between date_from and date_to AND type in ('CAMPAIGN_VIEW')) a
      GROUP BY a.stat_date, a.campaign_id, a.referer
    ) c
    LEFT JOIN
    (
      SELECT s.campaign_id,
        count(distinct o.order_id) as purchases,
        coalesce(coalesce(substring(ogs.referrer_url from '.*://([^/]*)'), ogs.referrer_url),'') as referer
      from
        commondb.shares s
        JOIN commondb.order_objects oo on oo.object_id = s.share_id
        JOIN commondb.orders o on o.order_id = oo.order_id
        JOIN statdb.order_geo_stats ogs on ogs.order_id = o.order_id
      WHERE o.time_added between date_from and date_to
            AND o.payment_status in (1,2)
            AND o.order_type = 1
      GROUP BY 1, 3
    ) b
      on c.campaign_id = b.campaign_id
         and c.referer = b.referer;
END;
$$;

-----------------------
-----------------------
-----------------------
-----------------------


DELETE FROM statdb.campaign_stats_general WHERE date > '2017-08-08';
DELETE FROM statdb.campaign_stats_country WHERE date > '2017-08-08';
DELETE FROM statdb.campaign_stats_city    WHERE date > '2017-08-08';
DELETE FROM statdb.campaign_stats_ref     WHERE date > '2017-08-08';



-- внимание! время выполнения скрипта - 30 минут!
DO $$
DECLARE
  statDate TIMESTAMP := '2017-08-09' :: TIMESTAMP;
  nowDate  TIMESTAMP := DATE_TRUNC('DAY', now()) :: TIMESTAMP;
BEGIN
  LOOP
    EXIT WHEN statDate >= nowDate;
    RAISE NOTICE 'begin aggregating stats of %', statDate;
	
    INSERT INTO statdb.campaign_stats_general (date, campaign_id, visitors, views, purchases, comments, amount, buyers)
      SELECT
        rt.date,
        rt.campaign_id,
        rt.visitors,
        rt.views,
        rt.purchases,
        rt.comments,
        rt.amount,
        rt.buyers
      FROM commondb.campaign_stats_general_interval(DATE_TRUNC('DAY', statDate) :: TIMESTAMP,
                                                    DATE_TRUNC('DAY', statDate + '1 day' :: INTERVAL) :: TIMESTAMP) rt;


	
    INSERT INTO statdb.campaign_stats_ref (date, campaign_id, referer, visitors, views, purchases, affiliate_amount)
      SELECT
        rt.date,
        rt.campaign_id,
        COALESCE(rt.referer, ''),
        rt.visitors,
        rt.views,
        rt.purchases,
        rt.affiliate_amount
      FROM commondb.campaign_stats_ref_interval(DATE_TRUNC('DAY', statDate) :: TIMESTAMP,
                                                DATE_TRUNC('DAY', statDate + '1 day' :: INTERVAL) :: TIMESTAMP) rt;

    INSERT INTO statdb.campaign_stats_country (date, campaign_id, country, visitors, views, purchases, affiliate_amount)
      SELECT
        rt.date,
        rt.campaign_id,
        COALESCE(rt.country, ''),
        rt.visitors,
        rt.views,
        rt.purchases,
        rt.affiliate_amount
      FROM commondb.campaign_stats_country_interval(DATE_TRUNC('DAY', statDate) :: TIMESTAMP,
                                                    DATE_TRUNC('DAY', statDate + '1 day' :: INTERVAL) :: TIMESTAMP) rt;

    INSERT INTO statdb.campaign_stats_city (date, campaign_id, city, visitors, views, purchases, affiliate_amount)
      SELECT
        rt.date,
        rt.campaign_id,
        COALESCE(rt.city, ''),
        rt.visitors,
        rt.views,
        rt.purchases,
        rt.affiliate_amount
      FROM commondb.campaign_stats_city_interval(DATE_TRUNC('DAY', statDate) :: TIMESTAMP,
                                                 DATE_TRUNC('DAY', statDate + '1 day' :: INTERVAL) :: TIMESTAMP) rt;


    RAISE NOTICE 'stop aggregating stats of %', statDate;
    statDate := statDate + '1 day' :: INTERVAL;
  END LOOP;
END;
$$;


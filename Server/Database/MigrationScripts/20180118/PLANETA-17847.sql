alter table maildb.campaigns add column date_sent TIMESTAMP;
CREATE INDEX campaigns_date_sent_idx on maildb.campaigns (date_sent);
update maildb.campaigns set date_sent = time_to_send where time_to_send is not null;
update maildb.campaigns set date_sent = date_confirmed where time_to_send is null;

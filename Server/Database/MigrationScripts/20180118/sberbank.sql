-- АХТУНГ!!!!
-- на продакшне выполнять только при включении сбербанка в бой!!!

INSERT INTO commondb.payment_providers (id, type, name, url, emulated, internal, processor_class, processor_params, processor_params_for_test)
VALUES (nextval('commondb.seq_payment_provider_id'), 'SBERBANK', 'Сбербанк', 'http://sberbank.ru', false, false, 'ru.planeta.payment.processors.sberbank.SberbankPaymentProcessor',
        '{"login":"planeta1-api","password":"planeta1","url":"https://3dsec.sberbank.ru/payment/rest/"}', '{"login":"planeta1-api","password":"planeta","url":"https://3dsec.sberbank.ru/payment/rest/"}');

INSERT INTO commondb.payment_tools (id, name, code, payment_provider_id, min, max, commission, monthly_limit, enabled, tls_supported)
VALUES (nextval('commondb.seq_payment_tool_id'), 'sberbank cards', '', currval('commondb.seq_payment_provider_id'), 0, 0, 2, 0, true, true);

INSERT INTO commondb.payment_methods (id, alias, image_url, name, description, mobile, need_phone, deferred, card, internal, promo, parent_id)
VALUES (nextval('commondb.seq_payment_method_id'), 'CARD_SBERBANK', '/images/balance/sbercard.png', 'Оплата картой сбербанка', null, false, false, false, true, false, false, 0);

insert into commondb.project_payment_methods(project_type, payment_method_id, enabled)
    VALUES
      ('MAIN', currval('commondb.seq_payment_method_id'), true),
      ('SHOP', currval('commondb.seq_payment_method_id'), true),
      ('BIBLIO', currval('commondb.seq_payment_method_id'), true);

insert into commondb.project_payment_tools(id, project_type, payment_method_id, payment_tool_id)
VALUES
  (nextval('commondb.seq_project_payment_tool_id'), 'MAIN', currval('commondb.seq_payment_method_id'), currval('commondb.seq_payment_tool_id')),
  (nextval('commondb.seq_project_payment_tool_id'), 'BIBLIO', currval('commondb.seq_payment_method_id'), currval('commondb.seq_payment_tool_id')),
  (nextval('commondb.seq_project_payment_tool_id'), 'SHOP', currval('commondb.seq_payment_method_id'), currval('commondb.seq_payment_tool_id'));


-- КОНЕЦ АХТУНГА
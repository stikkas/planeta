CREATE TABLE bibliodb.magazine_info (
  magazine_info_id BIGINT NOT NULL,
  price_list_id BIGINT NOT NULL,
  magazine_name TEXT NOT NULL,
  p_index TEXT NOT NULL,
  msp INTEGER NOT NULL,
  periodicity INTEGER NOT NULL,
  part_price_without_tax NUMERIC(10,2) NOT NULL,
  tax_percentage NUMERIC(5,4) NOT NULL,
  part_price_with_tax NUMERIC(10,2) NOT NULL,
  comment_no_magazine_month TEXT,
  PRIMARY KEY(magazine_info_id)
)
WITH (oids = false);

ALTER TABLE bibliodb.magazine_info
  ALTER COLUMN magazine_info_id SET STATISTICS 0;

ALTER TABLE bibliodb.magazine_info
  ALTER COLUMN price_list_id SET STATISTICS 0;

ALTER TABLE bibliodb.magazine_info
  ALTER COLUMN magazine_name SET STATISTICS 0;

ALTER TABLE bibliodb.magazine_info
  ALTER COLUMN p_index SET STATISTICS 0;

ALTER TABLE bibliodb.magazine_info
  ALTER COLUMN msp SET STATISTICS 0;

ALTER TABLE bibliodb.magazine_info
  ALTER COLUMN periodicity SET STATISTICS 0;

ALTER TABLE bibliodb.magazine_info
  ALTER COLUMN part_price_without_tax SET STATISTICS 0;

ALTER TABLE bibliodb.magazine_info
  ALTER COLUMN tax_percentage SET STATISTICS 0;

ALTER TABLE bibliodb.magazine_info
  ALTER COLUMN part_price_with_tax SET STATISTICS 0;

ALTER TABLE bibliodb.magazine_info
  ALTER COLUMN comment_no_magazine_month SET STATISTICS 0;

CREATE SEQUENCE bibliodb.magazine_info_seq
START 1;

/*--------------------------------------------*/

CREATE TABLE bibliodb.magazine_price_list (
  price_list_id BIGINT NOT NULL,
  name TEXT NOT NULL,
  time_added TIMESTAMP WITHOUT TIME ZONE NOT NULL,
  PRIMARY KEY(price_list_id)
)
WITH (oids = false);

ALTER TABLE bibliodb.magazine_price_list
  ALTER COLUMN price_list_id SET STATISTICS 0;

ALTER TABLE bibliodb.magazine_price_list
  ALTER COLUMN name SET STATISTICS 0;

ALTER TABLE bibliodb.magazine_price_list
  ALTER COLUMN time_added SET STATISTICS 0;

CREATE SEQUENCE bibliodb.magazine_price_list_seq
START 1;
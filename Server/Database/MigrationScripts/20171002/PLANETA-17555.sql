CREATE TABLE trashcan.interactive_editor_data (
  data_id BIGINT NOT NULL,
  user_id BIGINT NOT NULL UNIQUE,
  campaign_id BIGINT NOT NULL,
  contractor_type_percent INTEGER NOT NULL,
  desired_collected_amount_percent INTEGER,
  reached_step_num INTEGER NOT NULL,
  forms_settings TEXT,
  agree_store_personal_data BOOLEAN DEFAULT FALSE NOT NULL,
  agree_receive_lessons_materials_on_email BOOLEAN DEFAULT FALSE NOT NULL,
  PRIMARY KEY(data_id)
)
WITH (oids = false);

ALTER TABLE trashcan.interactive_editor_data
  ALTER COLUMN data_id SET STATISTICS 0;

ALTER TABLE trashcan.interactive_editor_data
  ALTER COLUMN user_id SET STATISTICS 0;

ALTER TABLE trashcan.interactive_editor_data
  ALTER COLUMN campaign_id SET STATISTICS 0;

ALTER TABLE trashcan.interactive_editor_data
  ALTER COLUMN contractor_type_percent SET STATISTICS 0;

ALTER TABLE trashcan.interactive_editor_data
  ALTER COLUMN reached_step_num SET STATISTICS 0;

ALTER TABLE trashcan.interactive_editor_data
  ALTER COLUMN forms_settings SET STATISTICS 0;

ALTER TABLE trashcan.interactive_editor_data
  ALTER COLUMN agree_store_personal_data SET STATISTICS 0;

ALTER TABLE trashcan.interactive_editor_data
  ALTER COLUMN agree_receive_lessons_materials_on_email SET STATISTICS 0;

ALTER TABLE trashcan.interactive_editor_data
  ADD COLUMN time_added TIMESTAMP WITHOUT TIME ZONE NOT NULL;

CREATE SEQUENCE trashcan.seq_interactive_editor_data_id
INCREMENT 1 START 1;
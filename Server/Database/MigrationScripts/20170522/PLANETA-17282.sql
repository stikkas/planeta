﻿-- https://planeta.atlassian.net/browse/PLANETA-17282
UPDATE commondb.campaigns SET target_status = 2
WHERE target_status = 1 AND collected_amount * 2 < target_amount;
CREATE TABLE commondb.campaign_targeting (
  campaign_id BIGINT NOT NULL,
  vk_targeting_id TEXT,
  fb_targeting_id TEXT,
  PRIMARY KEY(campaign_id)
)
WITH (oids = false);

ALTER TABLE commondb.campaign_targeting
  ALTER COLUMN campaign_id SET STATISTICS 0;

ALTER TABLE commondb.campaign_targeting
  ALTER COLUMN vk_targeting_id SET STATISTICS 0;

ALTER TABLE commondb.campaign_targeting
  ALTER COLUMN fb_targeting_id SET STATISTICS 0;
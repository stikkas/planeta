ALTER TABLE commondb.contractors
  ADD COLUMN "position" INTEGER DEFAULT 0 NOT NULL;

COMMENT ON COLUMN commondb.contractors."position"
IS 'This field us using in a contract as a prefix for responsible person.';
﻿ALTER TABLE commondb.contractors
  ADD COLUMN legal_address TEXT;

ALTER TABLE commondb.contractors
  ALTER COLUMN legal_address SET DEFAULT NULL;

/*-----------------------------------------*/

ALTER TABLE commondb.contractors
  ADD COLUMN actual_address TEXT;

ALTER TABLE commondb.contractors
  ALTER COLUMN actual_address SET DEFAULT NULL;

/*-----------------------------------------*/

ALTER TABLE commondb.contractors
  ADD COLUMN authority TEXT;

ALTER TABLE commondb.contractors
  ALTER COLUMN authority SET DEFAULT NULL;

/*-----------------------------------------*/

ALTER TABLE commondb.contractors
  ADD COLUMN issue_date TIMESTAMP WITHOUT TIME ZONE;

ALTER TABLE commondb.contractors
  ALTER COLUMN issue_date SET DEFAULT NULL;

/*-----------------------------------------*/

ALTER TABLE commondb.contractors
  ADD COLUMN checking_account TEXT;

ALTER TABLE commondb.contractors
  ALTER COLUMN checking_account SET DEFAULT NULL;

COMMENT ON COLUMN commondb.contractors.checking_account
IS 'raschetnyj schet';

/*-----------------------------------------*/

ALTER TABLE commondb.contractors
  ADD COLUMN beneficiary_bank TEXT;

ALTER TABLE commondb.contractors
  ALTER COLUMN beneficiary_bank SET DEFAULT NULL;

COMMENT ON COLUMN commondb.contractors.beneficiary_bank
IS 'bank poluchatelya';

/*-----------------------------------------*/

ALTER TABLE commondb.contractors
  ADD COLUMN corresponding_account TEXT;

ALTER TABLE commondb.contractors
  ALTER COLUMN corresponding_account SET DEFAULT NULL;

/*-----------------------------------------*/

ALTER TABLE commondb.contractors
  ADD COLUMN bic TEXT;

ALTER TABLE commondb.contractors
  ALTER COLUMN bic SET DEFAULT NULL;

/*-----------------------------------------*/

ALTER TABLE commondb.contractors
  ADD COLUMN unit TEXT;

ALTER TABLE commondb.contractors
  ALTER COLUMN unit SET DEFAULT NULL;

COMMENT ON COLUMN commondb.contractors.unit
IS 'kod podrazdeleniya';

/*-----------------------------------------*/

ALTER TABLE commondb.contractors
ADD COLUMN birth_date TIMESTAMP(0) WITHOUT TIME ZONE;

ALTER TABLE commondb.contractors
ALTER COLUMN birth_date SET DEFAULT null;

/*-----------------------------------------*/

ALTER TABLE commondb.contractors
  ADD COLUMN kpp TEXT;

ALTER TABLE commondb.contractors
  ALTER COLUMN kpp SET DEFAULT NULL;

/*-----------------------------------------*/

ALTER TABLE commondb.contractors
  ADD COLUMN responsible_person TEXT;

ALTER TABLE commondb.contractors
  ALTER COLUMN responsible_person SET DEFAULT NULL;
ALTER TABLE commondb.contractors
  ADD COLUMN scan_passport_url TEXT;

ALTER TABLE commondb.contractors
  ALTER COLUMN scan_passport_url SET DEFAULT NULL;
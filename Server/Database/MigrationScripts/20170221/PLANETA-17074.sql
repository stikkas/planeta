ALTER TABLE trashcan.litres_promo_code
  ADD COLUMN vote_type INTEGER;

CREATE UNIQUE INDEX litres_promo_code_email_and_vote_type_idx ON trashcan.litres_promo_code
USING btree (email, vote_type);

DROP INDEX trashcan.litres_promo_code_email_index;
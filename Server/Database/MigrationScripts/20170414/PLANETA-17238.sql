ALTER TABLE promodb.techbattle_project
  ADD COLUMN campaign_id BIGINT UNIQUE;

ALTER TABLE promodb.techbattle_project
  ADD COLUMN campaign_alias TEXT UNIQUE;

INSERT INTO commondb.configuration (key, int_value) VALUES ('techbattle.new.source.profile.id', 21765);
INSERT INTO commondb.configuration (key, str_value) VALUES ('techbattle.new.slider.ids', '54572,54467,54027');
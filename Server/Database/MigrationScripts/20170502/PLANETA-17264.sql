CREATE OR REPLACE FUNCTION commondb.after_restore_from_backup (
)
  RETURNS void AS
$body$
BEGIN
  DELETE FROM statdb.static_node;

  UPDATE profiledb.users             SET notification_email = 'noreply@planeta.ru';
  UPDATE commondb.campaign_contacts  SET email = 'noreply@planeta.ru';
  UPDATE commondb.mail_templates     SET to_address = 'noreply@planeta.ru' WHERE to_address LIKE '%planeta.ru%' AND name != 'error.report';
  UPDATE commondb.mail_templates     SET from_address = 'noreply@planeta.ru' WHERE name != 'error.report';
  UPDATE commondb.mail_templates     SET subject =
  '(' || COALESCE ((select str_value FROM commondb.configuration WHERE key = 'stand.name'), 'TEST')
  || ') � ���������� ${applicationName} ��������� ${errorsCount} ������'
  WHERE name = 'error.report';

  UPDATE commondb.planeta_managers   SET email = 'noreply@planeta.ru';
  UPDATE commondb.configuration      SET boolean_value = false where key = 'megaplan.enabled';
  UPDATE commondb.users_private_info SET email = replace(email,'@','_TEST_')
    , username = replace(username,'@','_TEST_')
  WHERE status & 32 != 32 and status & 64 != 64;
  UPDATE maildb.users SET email = replace(email,'@','_TEST_');
  UPDATE maildb."values" SET email = replace(email,'@','_TEST_');
  UPDATE maildb.campaigns SET time_to_send = NULL WHERE time_to_send > now() - interval '1 hour';
  UPDATE commondb.payment_providers SET  processor_params  =  processor_params_for_test;
  UPDATE bibliodb.library SET library_email = replace(library_email,'@','_TEST_');
  UPDATE bibliodb.book SET book_email = replace(book_email,'@','_TEST_');
END;
$body$
LANGUAGE 'plpgsql'
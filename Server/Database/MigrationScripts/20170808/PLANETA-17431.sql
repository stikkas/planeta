﻿UPDATE commondb.payment_methods
SET mobile = true, need_phone = TRUE
WHERE alias = 'MOBILE';

UPDATE commondb.project_payment_methods
SET enabled = false
WHERE payment_method_id in (select id from commondb.payment_methods where alias in ('MTS','MEGAFON', 'BEELINE'));

select setval('commondb.seq_project_payment_tool_id', (select  max(id) from commondb.project_payment_tools));

INSERT INTO commondb.project_payment_tools(id, project_type, payment_method_id, payment_tool_id, enabled, priority)
    VALUES
      (nextval('commondb.seq_project_payment_tool_id'), 'MAIN', 102, 4, true, 1),
      (nextval('commondb.seq_project_payment_tool_id'), 'SHOP', 102, 4, true, 1),
      (nextval('commondb.seq_project_payment_tool_id'), 'BIBLIO', 102, 4, true, 1),
      (nextval('commondb.seq_project_payment_tool_id'), 'INVEST_ALL_ALLOWED', 102, 4, true, 1);
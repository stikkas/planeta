INSERT INTO sphinx.sph_counter (source_id, source_name, range_max_id)
    VALUES (
    (SELECT MAX(source_id) + 1 FROM sphinx.sph_counter),
    'share00_src',
    (SELECT MAX(share_id) FROM commondb.shares)
	);
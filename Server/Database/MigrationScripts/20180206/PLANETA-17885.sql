UPDATE commondb.mail_templates SET content_bbcode = '[p]Вам назначен новый проект: [url=${appHostName}/campaigns/${campaignId}]${campaignName}[/url][/p]
[p][b]Email автора:[/b] ${authorEmail}[/p]
[p][b]Комментарий:[/b] ${comment}[/p]' WHERE name = 'campaign.change.manager';
CREATE EXTENSION pgcrypto;
alter table commondb.users_private_info alter COLUMN password TYPE varchar(64);
create table test.users_private_info_bak as select * from commondb.users_private_info;
update commondb.users_private_info set password = ENCODE(DIGEST(password,'sha256'),'hex');
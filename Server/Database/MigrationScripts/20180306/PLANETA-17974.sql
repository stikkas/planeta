create index linked_delivery_service_subject_type_subject_id_service_id_idx on commondb.linked_delivery_service
     USING btree(subject_type,subject_id,service_id);

create index delivery_address_country_id on commondb.delivery_address
    USING BTREE (country);

create index delivery_address_order_id on commondb.delivery_address
    USING BTREE (order_id);

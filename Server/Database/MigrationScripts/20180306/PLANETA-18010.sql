CREATE TABLE test.shares_bak_180221 as select * from commondb.shares;

update commondb.shares
set name = 'Поддержать на любую сумму', name_html = 'Поддержать на любую сумму'
where price = 0
 and (name = 'Без награды' or name = 'Поддержать без вознаграждения');
﻿CREATE TABLE statdb.campaign_after_purchase_feedback_letter (
  feedback_letter_id BIGINT NOT NULL,
  user_id BIGINT NOT NULL UNIQUE,
  order_id BIGINT NOT NULL,
  time_added TIMESTAMP WITHOUT TIME ZONE DEFAULT now() NOT NULL,
  is_mail_sent BOOLEAN DEFAULT FALSE NOT NULL,
  time_updated TIMESTAMP WITHOUT TIME ZONE,
  PRIMARY KEY(feedback_letter_id)
)
WITH (oids = false);

CREATE SEQUENCE statdb.seq_campaign_after_purchase_feedback_letter_id
  INCREMENT 1 MINVALUE 1
  START 1;


INSERT INTO commondb.short_links (short_link_id, short_link_status, short_link, redirect_address_url, cookie_name, cookie_value, profile_id) VALUES (nextval('commondb.seq_short_link_id'), 1, 'feedback-of-reward', 'https://planeta.ru/quiz/feedback-of-reward?utm_source=newsletter&utm_medium=email&utm_campaign=feedback-of-reward', '', '', 493527);

INSERT INTO commondb.mail_templates (template_id, name, subject, footer, content_bbcode, from_address, content_html, reply_to_address, to_address, use_footer_and_header, skip_logging) VALUES (nextval('commondb.seq_mail_template_id'), 'after.purchase.feedback', 'Поделитесь вашим мнением!', null, '', 'noreply@planeta.ru', '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"><html><head><title>Опросник пользователей после покупки</title><meta http-equiv="Content-Type" content="text/html; charset=utf-8"/><meta name="viewport" content="width=device-width"/></head><body style="-webkit-text-size-adjust: 100%; margin: 0; padding: 0; -ms-text-size-adjust: 100%; width: 100% !important; min-width: 100%; color: #515054; font-family: ''Arial'', sans-serif; font-weight: normal; text-align: left; line-height: 16px; font-size: 12px;"><div style="display: none; margin: 0; padding: 0; max-height: 0; min-height: 0; height: 0; font-size: 0; line-height: 0; overflow: hidden;">Planeta.ru и не только</div><table style="border-spacing: 0; margin: 0; padding: 0; border-collapse: collapse; height: 100%; width: 100%; -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; color: #454553; font-family: ''Open Sans'', -apple-system, BlinkMacSystemFont, ''Segoe UI'', Roboto, Helvetica, Arial, sans-serif; font-weight: 100; text-align: left; line-height: 1.4; font-size: 14px; background-color: #f4f4f4;" bgcolor="#f4f4f4"><tr><td style="vertical-align: top; font-size: 0; line-height: 0; padding: 25px 0 0;" valign="top">&nbsp;</td></tr><tr><td style="vertical-align: top; padding: 0;" valign="top"><center style="width: 100%; min-width: 480px;"><table style="border-spacing: 0; border-collapse: collapse; width: 480px; text-align: left; margin: auto;"><tr><td align="center" style="vertical-align: top; padding: 0 0 25px;" valign="top"><table style="border-spacing: 0; border-collapse: collapse;"><tr><td style="vertical-align: middle; padding: 0 15px;" valign="middle"><a href="https://planeta.ru/" style="color: #1a8cff; text-decoration: underline;"><img style="outline: none; text-decoration: none; -ms-interpolation-mode: bicubic; display: inline; vertical-align: middle; border: none;" src="https://s4.planeta.ru/i/1dd559/1516348789150_renamed.jpg"/></a></td></tr></table></td></tr><tr><td style="vertical-align: top; padding: 0;" valign="top"><table style="border-spacing: 0; border-collapse: collapse; width: 100%;"><tr><td style="vertical-align: top; padding: 0;" valign="top"><div><a href="https://l.dev.planeta.ru/feedback-of-reward"><img style="outline: none; text-decoration: none; -ms-interpolation-mode: bicubic; display: block; vertical-align: middle; border: none;" src="https://s3.planeta.ru/f/5221/1517235697697_renamed.jpg"/></a></div></td></tr><tr><td style="vertical-align: top; background-color: #fff; border-bottom-style: solid; border-bottom-color: #1a8cff; border-bottom-width: 4px; padding: 31px 46px 40px;" bgcolor="#fff" valign="top"><table style="border-spacing: 0; border-collapse: collapse; width: 100%;"><tr><td style="vertical-align: top; font-size: 24px; line-height: 33px; font-weight: 300; text-align: center; color: #000; padding: 0 0 32px;" align="center" valign="top">Добрый день!</td></tr><tr><td style="vertical-align: top; font-size: 16px; line-height: 24px; font-weight: 300; text-align: left; padding: 0;" align="left" valign="top"><div>Вы&nbsp;совершили покупку в&nbsp;проекте:<br>
&laquo;<strong>${campaignName}</strong>&raquo;</div>

<div><br>
Помогите нам сделать процесс покупки удобнее, ответив на&nbsp;4 простых вопроса.</div>

<div>&nbsp;</div>

<div>&nbsp;</div>

<div style="text-align: center;"><a href="https://l.planeta.ru/feedback-of-reward" isbutton="" style="vertical-align: top; font-size: 16px; line-height: 20px; white-space: nowrap; text-align: center; min-width: 190px; font-weight: 500; box-shadow: rgba(26, 140, 255, 0.5) 0px 15px 30px; background: rgb(26, 140, 255); border-color: rgb(26, 140, 255); border-style: solid; border-width: 25px 30px; border-radius: 5px; color: rgb(255, 255, 255); display: inline-block; text-decoration: none;" target="_blank">Перейти к&nbsp;опросу</a>&nbsp;</div>

<div>&nbsp;</div>

<div>&nbsp;</div>

<div align="right" style="text-align: right; color: #8a887c; font-size: 14px; font-style: italic;">С&nbsp;уважением,<br>
команда Planeta.ru</div></td></tr></table></td></tr></table></td></tr><tr><td style="vertical-align: top; padding: 0;" valign="top"><table style="border-spacing: 0; border-collapse: collapse; width: 100%;"><tr><td style="vertical-align: top; font-size: 12px; line-height: 17px; font-weight: 300; text-align: center; color: #454553; padding: 19px 0 0;" align="center" valign="top"><div>Данное сообщение отправлено автоматически, пожалуйста,<br>
не&nbsp;отвечайте на&nbsp;него. Пишите нам на&nbsp;<a href="mailto:support@planeta.ru" style="color: #1a8cff; text-decoration: underline;" target="_blank">support@planeta.ru</a>.</div></td></tr></table></td></tr></table></center></td></tr><tr><td style="vertical-align: top; font-size: 0; line-height: 0; padding: 25px 0 0;" valign="top">&nbsp;</td></tr></table></body></html>', 'support@planeta.ru', '${userEmail}', false, false);

GRANT ALL ON SCHEMA statdb TO planeta;
GRANT ALL ON ALL TABLES IN SCHEMA statdb TO planeta;
GRANT ALL ON ALL SEQUENCES IN SCHEMA statdb TO planeta;
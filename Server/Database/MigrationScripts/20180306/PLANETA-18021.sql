﻿CREATE TABLE IF NOT EXISTS trashcan.appeared_banner (
    id bigint NOT NULL,
    banner_id bigint NOT NULL,
    profile_id bigint NOT NULL,
    time_added timestamp without time zone DEFAULT now() NOT NULL,
    referer text
);

ALTER TABLE trashcan.appeared_banner OWNER TO planeta;

COMMENT ON TABLE trashcan.appeared_banner IS 'Информация о показаных баннерах';
COMMENT ON COLUMN trashcan.appeared_banner.id IS 'идентификатор';
COMMENT ON COLUMN trashcan.appeared_banner.banner_id IS 'идентификатор баннера';
COMMENT ON COLUMN trashcan.appeared_banner.profile_id IS 'идентификатор пользователя';
COMMENT ON COLUMN trashcan.appeared_banner.referer IS 'где показали';

CREATE SEQUENCE trashcan.appeared_banner_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

ALTER TABLE trashcan.appeared_banner_id_seq OWNER TO planeta;


create or replace view trashcan.click_banners_today as
  select count(cb.banner_id) cnt, b.name, cb.banner_id from trashcan.clicked_banner cb left outer join commondb.banners b on
                                                                                                                            cb.banner_id = b.banner_id
  where cb.time_added BETWEEN now()::date AND now()::date + interval '1 day' group by b.name, cb.banner_id;

create or replace view trashcan.click_banners_last_7_days as
  select count(cb.banner_id) cnt, b.name, cb.banner_id from trashcan.clicked_banner cb left outer join commondb.banners b on
                                                                                                                            cb.banner_id = b.banner_id
  where cb.time_added BETWEEN now()::date - interval '6 day' AND now()::date + interval '1 day' group by b.name, cb.banner_id;

create or replace view trashcan.click_banners_last_month as
  select count(cb.banner_id) cnt, b.name, cb.banner_id from trashcan.clicked_banner cb left outer join commondb.banners b on
                                                                                                                            cb.banner_id = b.banner_id
  where cb.time_added BETWEEN now()::date - interval '1 month' AND now()::date + interval '1 day' group by b.name, cb.banner_id;

create or replace view trashcan.click_banners_last_6_months as
  select count(cb.banner_id) cnt, b.name, cb.banner_id from trashcan.clicked_banner cb left outer join commondb.banners b on
                                                                                                                            cb.banner_id = b.banner_id
  where cb.time_added BETWEEN now()::date - interval '6 month' AND now()::date + interval '1 day' group by b.name, cb.banner_id;

create or replace view trashcan.click_banners_last_12_months as
  select count(cb.banner_id) cnt, b.name, cb.banner_id from trashcan.clicked_banner cb left outer join commondb.banners b on
                                                                                                                            cb.banner_id = b.banner_id
  where cb.time_added BETWEEN now()::date - interval '12 month' AND now()::date + interval '1 day' group by b.name, cb.banner_id;


create or replace view trashcan.appear_banners_today as
  select count(cb.banner_id) cnt, b.name, cb.banner_id from trashcan.appeared_banner cb left outer join commondb.banners b on
                                                                                                                             cb.banner_id = b.banner_id
  where cb.time_added BETWEEN now()::date AND now()::date + interval '1 day' group by b.name, cb.banner_id;

create or replace view trashcan.appear_banners_last_7_days as
  select count(cb.banner_id) cnt, b.name, cb.banner_id from trashcan.appeared_banner cb left outer join commondb.banners b on
                                                                                                                             cb.banner_id = b.banner_id
  where cb.time_added BETWEEN now()::date - interval '6 day' AND now()::date + interval '1 day' group by b.name, cb.banner_id;

create or replace view trashcan.appear_banners_last_month as
  select count(cb.banner_id) cnt, b.name, cb.banner_id from trashcan.appeared_banner cb left outer join commondb.banners b on
                                                                                                                             cb.banner_id = b.banner_id
  where cb.time_added BETWEEN now()::date - interval '1 month' AND now()::date + interval '1 day' group by b.name, cb.banner_id;

create or replace view trashcan.appear_banners_last_6_months as
  select count(cb.banner_id) cnt, b.name, cb.banner_id from trashcan.appeared_banner cb left outer join commondb.banners b on
                                                                                                                             cb.banner_id = b.banner_id
  where cb.time_added BETWEEN now()::date - interval '6 month' AND now()::date + interval '1 day' group by b.name, cb.banner_id;

create or replace view trashcan.appear_banners_last_12_months as
  select count(cb.banner_id) cnt, b.name, cb.banner_id from trashcan.appeared_banner cb left outer join commondb.banners b on
                                                                                                                             cb.banner_id = b.banner_id
  where cb.time_added BETWEEN now()::date - interval '12 month' AND now()::date + interval '1 day' group by b.name, cb.banner_id;


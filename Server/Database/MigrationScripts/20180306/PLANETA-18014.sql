UPDATE commondb.mail_templates
SET content_bbcode = '[p]Ваш заказ №${orderId} ${serviceText} был аннулирован. ${reason}[/p]
    [p]Вы можете воспользоваться денежными средствами одним из следующих способов:[ul][li]потратить их на покупку товаров [url=${appShopHost}]в интернет-магазине[/url],[/li][li]стать спонсором [url=${appHost}/search/projects/query=&status=active]проекта[/url],[/li][li]помочь [url=${appHost}/search/projects/query=&status=CHARITY]благотворительным проектам[/url].[/li][li]вернуть денежные средства в соответствии с  [url=${appHost}/faq/article/10!paragraph84]Условиями возврата денежных средств[/url].[/li][/ul]С информацией о своих покупках и платежах вы можете ознакомиться на странице [url=${appHost}/account]«Мои заказы»[/url].[/p]'
WHERE name = 'order.canceled';

UPDATE commondb.mail_templates
SET content_bbcode = '[h2]Здравствуйте, ${userName}![/h2]
[p]Вы оплатили покупку №${orderId} в проекте "${campaignName}".[/p]
[p]К сожалению, ваш платеж дошел до нас слишком поздно, и проект "${campaignName}" уже закрылся. Деньги, уплаченные Вами, находятся в вашем личном кошельке.[/p]
[p]В таком случае вы можете воспользоваться одной из следующих возможностей:[ul][li]потратить деньги на покупку других [url=${appShopHost}]товаров[/url].[/li][li]потратить деньги на покупку других [url=${appHost}/search/projects/query=&status=active]акций[/url].[/li][li]пожертвовать деньги на [url=${appHost}/search/projects/query=&status=CHARITY]благотворительные проекты[/url].[/li][li]вернуть деньги в соответствии с [url=${appHost}/faq/article/10!paragraph84]условиями возврата денежных средств[/url].[/li][/ul]С информацией о своих покупках и платежах вы можете ознакомиться на странице [url=${appHost}/account]«Мои заказы»[/url].[/p]'
  WHERE name = 'purchase.error.campaign.not.active';

UPDATE commondb.mail_templates
SET content_bbcode = '[h2]Здравствуйте, ${userName}![/h2]
[p]Вы оплатили покупку №${orderId} в проекте "${campaignName}".[/p]
[p]К сожалению, ваш платеж дошел до нас слишком поздно, и акции "${shareName}" уже закончились. Деньги, уплаченные Вами, находятся в вашем личном кошельке.[/p][p]В таком случае вы можете воспользоваться одной из следующих возможностей:[ul][li]потратить деньги на покупку [url=${appShopHost}]товаров[/url].[/li][li]потратить деньги на покупку [url=${appHost}/search/projects/query=&status=active]акций[/url].[/li][li]пожертвовать деньги на [url=${appHost}/search/projects/query=&status=CHARITY]благотворительные проекты[/url].[/li][li]вернуть деньги в соответствии с [url=${appHost}/faq/article/10!paragraph84]условиями возврата денежных средств[/url].[/li][/ul]С информацией о своих покупках и платежах вы можете ознакомиться на странице [url=${appHost}/account]«Мои заказы»[/url].[/p]'
WHERE name = 'purchase.error.not.enough.share.amount';

UPDATE commondb.mail_templates
SET content_bbcode = '[h3]Здравствуйте, ${userName}![/h3]
[p]Вы оплатили покупку №${orderId} в магазине [url=${appShopHost}]Planeta.ru[/url].
К сожалению, ваш платеж дошел до нас слишком поздно, и товар уже закончился. Деньги, уплаченные Вами, находятся в вашем личном кошельке.[/p][p]В таком случае вы можете воспользоваться одной из следующих возможностей:[ul][li]потратить деньги на покупку других [url=${appShopHost}]товаров[/url].[/li][li]потратить деньги на покупку [url=${appHost}/search/projects/query=&status=active]акций[/url].[/li][li]пожертвовать деньги на [url=${appHost}/search/projects/query=&status=CHARITY]благотворительные проекты[/url].[/li][li]вернуть деньги в соответствии с [url=${appHost}/faq/article/10!paragraph84]условиями возврата денежных средств[/url].[/li][/ul]С информацией о своих покупках и платежах вы можете ознакомиться на странице [url=${appHost}/account]«Мои заказы»[/url].[/p]'
WHERE name = 'purchase.error.not.enough.product.amount';


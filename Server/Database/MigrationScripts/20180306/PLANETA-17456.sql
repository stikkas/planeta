create table trashcan.ivi_order (
  order_id BIGINT NOT NULL PRIMARY KEY
);

grant select,insert,update,delete on trashcan.ivi_order to planeta;
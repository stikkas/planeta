CREATE OR REPLACE FUNCTION maintenance.diff_nodes(OUT node_name TEXT, OUT action TEXT, OUT table_name TEXT)
RETURNS SETOF record AS
$$
DECLARE
	rec record;
    rec2 record;
    v_etalon_node_id INTEGER;
BEGIN
	SELECT t1.node_id
      INTO v_etalon_node_id
      FROM maintenance.database_nodes t1
     WHERE t1.is_etalon = TRUE;

	FOR rec IN (SELECT t1.node_id, t1.host || ':' || t1.dbname AS node_name
    			  FROM maintenance.database_nodes t1
   			     WHERE t1.is_etalon = FALSE
    ) LOOP
    
    	FOR rec2 IN (SELECT t1.table_name
         			  FROM maintenance.get_node_tables(rec.node_id) t1
                      LEFT JOIN maintenance.get_node_tables(v_etalon_node_id) t2
                        ON t1.table_name = t2.table_name
         			 WHERE t2.table_name IS NULL
        ) LOOP
        	node_name := rec.node_name;
            action := 'Deleted';
            table_name = rec2.table_name;
            RETURN NEXT;
        END LOOP;
        
    	FOR rec2 IN (SELECT t1.table_name
         			  FROM maintenance.get_node_tables(v_etalon_node_id) t1
                      LEFT JOIN maintenance.get_node_tables(rec.node_id) t2
                        ON t1.table_name = t2.table_name
         			 WHERE t2.table_name IS NULL
        ) LOOP
        	node_name := rec.node_name;
            action := 'Created';
            table_name = rec2.table_name;
            RETURN NEXT;
        END LOOP;

    	FOR rec2 IN (SELECT t1.table_name
         			  FROM maintenance.get_node_tables(v_etalon_node_id) t1
                      JOIN maintenance.get_node_tables(rec.node_id) t2
                        ON t1.table_name = t2.table_name
                     WHERE maintenance.get_table_create_script(v_etalon_node_id, t1.table_name) != maintenance.get_table_create_script(rec.node_id, t2.table_name)
        ) LOOP
        	node_name := rec.node_name;
            action := 'Changed';
            table_name = rec2.table_name;
            RETURN NEXT;
        END LOOP;
        

    END LOOP;
    
    RETURN;
END;
$$ LANGUAGE 'plpgsql';
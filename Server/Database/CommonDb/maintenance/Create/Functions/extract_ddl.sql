CREATE OR REPLACE FUNCTION maintenance.extract_ddl(IN table_name text, IN db_name text, 
			IN host text, IN user_name text, IN pwd text)
RETURNS text AS
$$
	my $table_name = $_[0];
    my $db_name = $_[1];
    my $host = $_[2];
    my $user_name = $_[3];
    my $pwd = $_[4];
	my $str = `pg_dump -s -t $table_name -h $host -U $user_name $db_name`;
    return $str;
$$ LANGUAGE 'plperlu';
CREATE OR REPLACE FUNCTION maintenance.get_sql_select_cluster_tables()
RETURNS TEXT AS
$$
	SELECT 'SELECT table_schema || ''.'' || table_name FROM information_schema.tables WHERE table_schema IN (''profiledb'', ''msgdb'');'::TEXT;
$$ LANGUAGE 'sql';
CREATE OR REPLACE FUNCTION maintenance.get_table_create_script(IN p_node_id INTEGER, IN p_table_name TEXT)
RETURNS TEXT AS
$$
	SELECT maintenance.extract_ddl($2, t1.dbname, t1.host, t1.username, t1."password") AS script
      FROM maintenance.database_nodes t1
     WHERE t1.node_id = $1
     LIMIT 1;
$$ LANGUAGE 'sql';
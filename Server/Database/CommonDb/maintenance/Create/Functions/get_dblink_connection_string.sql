CREATE OR REPLACE FUNCTION maintenance.get_dblink_connection_string(IN p_node_id INTEGER, OUT connection_string TEXT)
RETURNS TEXT AS
$$
	SELECT 'host=' || t1.host || ' dbname=' || t1.dbname || ' port=5432 user=' || t1.username || ' password=' || t1."password" 
      FROM maintenance.database_nodes t1
     WHERE node_id = $1;
$$ LANGUAGE 'sql';
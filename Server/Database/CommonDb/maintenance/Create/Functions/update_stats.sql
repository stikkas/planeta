CREATE OR REPLACE FUNCTION maintenance.update_commentsstats(OUT p_profile BIGINT, OUT p_object_id BIGINT, OUT p_date DATE, OUT p_comments INTEGER)
RETURNS SETOF record AS
$$
DECLARE
	rec record;
	rec2 record;
    v_etalon_node_id INTEGER;
BEGIN
	SELECT t1.node_id
      INTO v_etalon_node_id
      FROM maintenance.database_nodes t1
     WHERE t1.is_etalon = TRUE;

	FOR rec IN (SELECT t1.node_id, t1.host || ':' || t1.dbname AS node_name
    			  FROM maintenance.database_nodes t1
   			     WHERE t1.is_etalon = FALSE
    ) LOOP	
		FOR rec2 IN (SELECT profile_id 			  
        FROM profiledb.profiles
        
        ) LOOP
			 SELECT COUNT(*) as comment_id,
					profile_id,
					object_id,
					date_trunc('hour', "time_added")  as hour
			INTO p_comments, p_profile, p_object_id, p_date		
			   FROM profiledb.comments t1
			   JOIN profiledb.profiles t2
				 ON t1.author_profile_id = t2.profile_id
			  WHERE t1.owner_profile_id = rec2.profile_id
				AND t1.object_type_id = 4
				AND NOT t1.is_deleted
		   GROUP BY 2, 3, 4
		   ORDER BY 4 DESC;
           IF p_profile IS NOT NULL THEN
            RETURN NEXT;     
           END IF; 
        END LOOP;
    
    	   

    END LOOP;
    
    RETURN;
END;
$$ LANGUAGE 'plpgsql';

SELECT * FROM maintenance.update_commentsstats();
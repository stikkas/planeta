CREATE OR REPLACE FUNCTION maintenance.get_node_tables(IN p_node_id INTEGER, OUT table_name TEXT)
RETURNS SETOF TEXT AS
$$
	SELECT *
  	  FROM dblink(maintenance.get_dblink_connection_string($1), maintenance.get_sql_select_cluster_tables()) AS (table_name TEXT)
$$ LANGUAGE 'sql';
CREATE TABLE "maintenance"."database_nodes" (
  "node_id" INTEGER NOT NULL, 
  "host" TEXT NOT NULL, 
  "dbname" TEXT NOT NULL, 
  "username" TEXT NOT NULL, 
  "password" TEXT NOT NULL, 
  "is_etalon" BOOLEAN NOT NULL, 
  PRIMARY KEY("node_id")
) WITH OIDS;

COMMENT ON TABLE "maintenance"."database_nodes"
IS 'Contains list of database notes';

COMMENT ON COLUMN "maintenance"."database_nodes"."node_id"
IS 'Node identifier';

COMMENT ON COLUMN "maintenance"."database_nodes"."host"
IS 'Host name';

COMMENT ON COLUMN "maintenance"."database_nodes"."dbname"
IS 'Database name';

COMMENT ON COLUMN "maintenance"."database_nodes"."username"
IS 'Admin user name';

COMMENT ON COLUMN "maintenance"."database_nodes"."password"
IS 'Admin password';

COMMENT ON COLUMN "maintenance"."database_nodes"."is_etalon"
IS 'Is this node etalon for others';

INSERT INTO maintenance.database_nodes (node_id, host, dbname, username, password, is_etalon)
     VALUES (1, '127.0.0.1', 'userdb01', 'planeta', '7fAOTofWR44ge7Eq', true);
INSERT INTO maintenance.database_nodes (node_id, host, dbname, username, password, is_etalon)
     VALUES (2, '127.0.0.1', 'userdb02', 'planeta', '7fAOTofWR44ge7Eq', false);

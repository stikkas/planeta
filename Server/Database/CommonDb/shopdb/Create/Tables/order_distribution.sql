CREATE TABLE shopdb.order_distribution (
  order_id BIGINT, 
  product_id BIGINT, 
  product_version BIGINT, 
  warehouse_id BIGINT, 
  quantity BIGINT, 
  PRIMARY KEY(order_id, warehouse_id, product_id, product_version)
) WITH OIDS;
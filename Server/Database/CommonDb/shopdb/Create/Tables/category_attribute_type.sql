CREATE TABLE shopdb.category_attribute_type (
	category_id BIGINT NOT NULL, 
	attribute_type_id BIGINT NOT NULL,
	CONSTRAINT category_attribute_type_pkey PRIMARY KEY(category_id, attribute_type_id)
) WITHOUT OIDS;

COMMENT ON COLUMN shopdb.category_attribute_type.category_id
IS 'Category identifier';

COMMENT ON COLUMN shopdb.category_attribute_type.attribute_type_id
IS 'Attribute type identifier';

-- Insert initial values
INSERT INTO shopdb.category_attribute_type (
    category_id,
    attribute_type_id)
  VALUES
    (1, 1),
    (2, 2),
    (3, 3),
    (4, 4);
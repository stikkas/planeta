CREATE TABLE shopdb.stores (
	profile_id BIGINT NOT NULL, 
	title TEXT,
	description TEXT,
	description_html TEXT,
	contacts TEXT,
	email TEXT,
	phone_number TEXT,
	other_contacts TEXT,
	status INTEGER NOT NULL, 
	time_added TIMESTAMP WITHOUT TIME ZONE DEFAULT now() NOT NULL,
	time_updated TIMESTAMP WITHOUT TIME ZONE DEFAULT now() NOT NULL,
	CONSTRAINT stores_pkey PRIMARY KEY(profile_id)
) WITHOUT OIDS;

COMMENT ON COLUMN shopdb.stores.profile_id
IS 'Store profile identifier';

COMMENT ON COLUMN shopdb.stores.title
IS 'Store title';

COMMENT ON COLUMN shopdb.stores.description
IS 'Store description';

COMMENT ON COLUMN shopdb.stores.description_html
IS 'Html code of store description';

COMMENT ON COLUMN shopdb.stores.contacts
IS 'Contact info of the store';

COMMENT ON COLUMN shopdb.stores.email
IS 'Email address(es) of the store';

COMMENT ON COLUMN shopdb.stores.phone_number
IS 'Phone number(s) of the store';

COMMENT ON COLUMN shopdb.stores.other_contacts
IS 'Other contact info of the store';

COMMENT ON COLUMN shopdb.stores.status
IS 'Store status';

COMMENT ON COLUMN shopdb.stores.time_added
IS 'Store time added';

COMMENT ON COLUMN shopdb.stores.time_updated
IS 'Store time updated';
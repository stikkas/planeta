CREATE TABLE shopdb.product_warehouse_quantity (
	product_id BIGINT NOT NULL,
	warehouse_id BIGINT NOT NULL,
	quantity INTEGER NOT NULL,
	CONSTRAINT product_warehouse_quantity_keyword_pkey PRIMARY KEY(product_id, warehouse_id)
) WITHOUT OIDS;


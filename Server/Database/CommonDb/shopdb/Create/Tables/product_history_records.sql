﻿CREATE TABLE shopdb.product_history_records (
  record_id BIGINT NOT NULL,
  product_id BIGINT NOT NULL,
  version INTEGER NOT NULL,
  author_profile_id BIGINT NOT NULL,
  time_added TIMESTAMP WITHOUT TIME ZONE DEFAULT now() NOT NULL,
  comment VARCHAR(250),
  CONSTRAINT product_history_records_pkey PRIMARY KEY(record_id)
) WITHOUT OIDS;

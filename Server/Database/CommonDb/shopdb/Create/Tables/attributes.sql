﻿CREATE TABLE shopdb.attributes (
	attribute_id BIGINT NOT NULL,
	value VARCHAR(256),
	type INTEGER NOT NULL,
	index INTEGER DEFAULT 0,
	CONSTRAINT attributes_pkey PRIMARY KEY(attribute_id)
) WITHOUT OIDS;

COMMENT ON COLUMN shopdb.attributes.attribute_id
IS 'Attribute identifier';

COMMENT ON COLUMN shopdb.attributes.value
IS 'Attribute value';

COMMENT ON COLUMN shopdb.attributes.type
IS 'Attribute type';

COMMENT ON COLUMN shopdb.attributes.index
IS 'Attribute index in type';

-- Insert initial values
INSERT INTO shopdb.attributes (
    attribute_id,
    type,
    value,
    index)
  VALUES
    (1, 1, 'XS', 1),
    (2, 1, 'S', 2),
    (3, 1, 'M', 3),
    (4, 1, 'L', 4),
    (5, 1, 'XL', 5),
    (6, 1, 'XXL', 6);
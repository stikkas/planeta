CREATE TABLE shopdb.categories (
	category_id BIGINT NOT NULL, 
	value TEXT,
	CONSTRAINT categories_pkey PRIMARY KEY(category_id)
) WITHOUT OIDS;

COMMENT ON COLUMN shopdb.categories.category_id
IS 'Category identifier';

COMMENT ON COLUMN shopdb.categories.value
IS 'Category value';

-- Insert initial values

INSERT INTO shopdb.categories (
    category_id,
    value)
  VALUES
    (1, 'Одежда'),
    (2, 'Диски'),
    (3, 'Книги'),
    (4, 'Другое');

CREATE TABLE shopdb.attribute_types (
  attribute_type_id BIGINT NOT NULL,
  value             VARCHAR(256),
  CONSTRAINT attribute_types_pkey PRIMARY KEY (attribute_type_id)
) WITHOUT OIDS;

COMMENT ON COLUMN shopdb.attribute_types.attribute_type_id
IS 'Attribute type identifier';

COMMENT ON COLUMN shopdb.attribute_types.value
IS 'Attribute type value';

-- Insert initial values
INSERT INTO shopdb.attribute_types (
    attribute_type_id,
    value)
  VALUES
    (1, 'Размер'),
    (2, 'Свойства диска'),
    (3, 'Свойства книги'),
    (4, 'Свойства товара');

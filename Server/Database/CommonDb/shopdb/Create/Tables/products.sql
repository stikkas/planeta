﻿CREATE TABLE shopdb.products (
  product_id BIGINT NOT NULL, 
  version INTEGER NOT NULL, 
  merchant_profile_id BIGINT NOT NULL, 
  store_id BIGINT NOT NULL, 
  price NUMERIC(16,2), 
  currency TEXT, 
  total_purchase_count INTEGER NOT NULL, 
  purchase_limit INTEGER NOT NULL, 
  type INTEGER NOT NULL, 
  product_status INTEGER NOT NULL, 
  product_category INTEGER NOT NULL, 
  image_urls TEXT[], 
  cover_image_url TEXT,
  table_image_url TEXT,
  name VARCHAR(256), 
  description TEXT, 
  description_html TEXT, 
  product_attribute_id INTEGER, 
  time_added TIMESTAMP WITHOUT TIME ZONE DEFAULT now() NOT NULL, 
  time_updated TIMESTAMP WITHOUT TIME ZONE DEFAULT now() NOT NULL, 
  parent_product_id BIGINT, 
  product_attribute_type INTEGER, 
  product_attribute_value VARCHAR(25),
  product_attribute_index INTEGER DEFAULT 0,
  product_state INTEGER DEFAULT 1 NOT NULL,
  total_on_hold_quantity BIGINT DEFAULT 0 NOT NULL, 
  total_quantity BIGINT DEFAULT 0 NOT NULL, 
  time_last_purchased NOT NULL TIMESTAMP WITHOUT TIME ZONE DEFAULT now(),
  CONSTRAINT products_pk PRIMARY KEY(product_id, version)
) WITHOUT OIDS;

COMMENT ON COLUMN shopdb.products.type
IS 'код categories.category_id';

COMMENT ON COLUMN shopdb.products.total_quantity
IS 'total quantity on all warehouses';

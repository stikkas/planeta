CREATE TABLE shopdb.shopping_cart (
  profile_id BIGINT NOT NULL,
  product_id BIGINT NOT NULL,
  quantity INTEGER DEFAULT 1 NOT NULL,
  time_updated TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT now(),
  PRIMARY KEY(profile_id, product_id)
) ;

COMMENT ON TABLE shopdb.shopping_cart
IS 'shopping cart for online purchasing';

COMMENT ON COLUMN shopdb.shopping_cart.profile_id
IS 'buyer profile Id';

COMMENT ON COLUMN shopdb.shopping_cart.product_id
IS 'product id from products';

COMMENT ON COLUMN shopdb.shopping_cart.quantity
IS 'wished product quantity';

COMMENT ON COLUMN shopdb.shopping_cart.time_updated
IS 'last time updated';
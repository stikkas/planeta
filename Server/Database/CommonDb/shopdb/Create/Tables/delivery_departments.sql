CREATE TABLE shopdb.delivery_departments (
	delivery_department_id BIGINT NOT NULL, 
	name TEXT,
	description TEXT,
	description_html TEXT,
	phone_number TEXT,
	contacts TEXT,
	image_url TEXT,
	is_active BOOLEAN DEFAULT false NOT NULL,
	delivery_type INT NOT NULL DEFAULT 0,
	time_added TIMESTAMP WITHOUT TIME ZONE DEFAULT now() NOT NULL,
	time_updated TIMESTAMP WITHOUT TIME ZONE DEFAULT now() NOT NULL,
	CONSTRAINT delivery_department_pkey PRIMARY KEY(delivery_department_id)
) WITHOUT OIDS;

COMMENT ON COLUMN shopdb.delivery_departments.delivery_department_id
IS 'Delivery service identifier';

COMMENT ON COLUMN shopdb.delivery_departments.name
IS 'Delivery service name';

COMMENT ON COLUMN shopdb.delivery_departments.description
IS 'Delivery service description';

COMMENT ON COLUMN shopdb.delivery_departments.description
IS 'Html code of elivery service description';

COMMENT ON COLUMN shopdb.delivery_departments.phone_number
IS 'Phone number of delivery service';

COMMENT ON COLUMN shopdb.delivery_departments.contacts
IS 'Contact info of delivery service';

COMMENT ON COLUMN shopdb.delivery_departments.is_active
IS 'Indicates whether delivery service is active';

COMMENT ON COLUMN shopdb.delivery_departments.delivery_type
IS 'Delivery type';

COMMENT ON COLUMN shopdb.delivery_departments.time_added
IS 'Delivery service time added';

COMMENT ON COLUMN shopdb.delivery_departments.time_updated
IS 'Delivery service time updated';

INSERT INTO
  shopdb.delivery_departments
  (
    delivery_department_id,
    name,
    is_active,
    time_added,
    time_updated,
    delivery_type,
    delivery_price
  )
  VALUES
  (
    1,
    'Самовывоз из офиса',
    true,
    CURRENT_TIMESTAMP,
    CURRENT_TIMESTAMP,
    5,
    0
  ),
  (
    2,
    'Самовывоз из постамата в Москве',
    true,
    CURRENT_TIMESTAMP,
    CURRENT_TIMESTAMP,
    6,
    200
  ),
  (
    3,
    'Курьерская доставка по Москве',
    true,
    CURRENT_TIMESTAMP,
    CURRENT_TIMESTAMP,
    4,
    250
  ),
  (
    4,
    'Курьерская доставка по Московской области(не более 20км от МКАД)',
    true,
    CURRENT_TIMESTAMP,
    CURRENT_TIMESTAMP,
    4,
    500
  ),
  (
    5,
    'Доставка Почтой России в любой регион',
    true,
    CURRENT_TIMESTAMP,
    CURRENT_TIMESTAMP,
    3,
    300
  ),
  (
    6,
    'Курьерская доставка по Санкт-Петербургу (в пределах КАД)',
    true,
    CURRENT_TIMESTAMP,
    CURRENT_TIMESTAMP,
    4,
    250
  );
  
ALTER SEQUENCE shopdb.seq_delivery_department_id RESTART WITH 7; 
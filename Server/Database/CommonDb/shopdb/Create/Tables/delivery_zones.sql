CREATE TABLE shopdb.delivery_zones (
	delivery_zone_id BIGINT NOT NULL, 
	delivery_department_id BIGINT NOT NULL,
	name TEXT,
	price NUMERIC(16,2) DEFAULT 0 NOT NULL,
	time_added TIMESTAMP WITHOUT TIME ZONE DEFAULT now() NOT NULL,
	time_updated TIMESTAMP WITHOUT TIME ZONE DEFAULT now() NOT NULL,
	CONSTRAINT delivery_zones_pkey PRIMARY KEY(delivery_zone_id)
) WITHOUT OIDS;

COMMENT ON COLUMN shopdb.delivery_zones.delivery_zone_id
IS 'Delivery zone identifier';

COMMENT ON COLUMN shopdb.delivery_zones.delivery_department_id
IS 'Delivery service identifier';

COMMENT ON COLUMN shopdb.delivery_zones.name
IS 'Delivery zone name';

COMMENT ON COLUMN shopdb.delivery_zones.price
IS 'Price for a delivery zone';

COMMENT ON COLUMN shopdb.delivery_zones.time_added
IS 'Delivery zone time added';

COMMENT ON COLUMN shopdb.delivery_zones.time_updated
IS 'Delivery zone time updated';
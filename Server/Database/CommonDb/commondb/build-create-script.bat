copy ..\..\echo-scripts.bat .\echo-scripts.bat
set output=%1

if (%1) == () (
	echo Setting output to create.sql
	set output=create.sql
)

del %output%

echo Loading sequences
call echo-scripts.bat Create\Sequences >> %output%

echo Loading tables
call echo-scripts.bat Create\Tables >> %output%

if errorlevel 1 echo build-create-script was unsuccessful.

echo build-create-script.bat ended work
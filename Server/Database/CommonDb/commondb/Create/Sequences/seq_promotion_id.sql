CREATE SEQUENCE commondb.seq_promotion_id
  INCREMENT 1 MINVALUE 1
  MAXVALUE 9223372036854775807 START 1
  CACHE 1;

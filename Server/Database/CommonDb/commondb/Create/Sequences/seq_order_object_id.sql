create sequence commondb.seq_order_object_id
  increment 1 minvalue 1
  maxvalue 9223372036854775807 start 1
  cache 1;
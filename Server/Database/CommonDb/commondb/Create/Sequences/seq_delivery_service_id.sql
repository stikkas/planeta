CREATE SEQUENCE commondb.seq_delivery_service_id
INCREMENT 1 MINVALUE 1
MAXVALUE 9223372036854775807 START 1
CACHE 1;
CREATE SEQUENCE commondb.seq_manager_id
INCREMENT 1 MINVALUE 1 START 1;

ALTER TABLE commondb.campaign_managers
ALTER COLUMN manager_id SET DEFAULT nextval('commondb.seq_manager_id');
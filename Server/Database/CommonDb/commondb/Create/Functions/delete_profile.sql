CREATE OR REPLACE FUNCTION commondb.delete_profile (p_id bigint) RETURNS integer
LANGUAGE plpgsql
AS $$
DECLARE
  user_type INT;
  result TEXT;
BEGIN

  SELECT INTO user_type profile_type_id
  FROM profiledb.profiles
  WHERE profile_id = p_id;
  IF user_type = 1 THEN
    UPDATE profiledb.profiles
    SET
      image_url = null,
      image_id = 0,
      small_image_url = null,
      small_image_id = 0,
      name = null,
      alias = null,
      status = 8,
      city_id = 0,
      country_id = 0,
      rating = 0,
      user_birth_date = null,
      user_gender = 0,
      user_first_name = null,
      user_last_name = null,
      display_name = 'Пользователь удален',
      display_name_format = 0,
      summary = null
    WHERE profile_id = p_id;

    UPDATE commondb.users_private_info
    SET status = 4
    WHERE user_id = p_id;

    UPDATE  maildb.users
    SET unsubscribed = true
    WHERE email = (SELECT email FROM commondb.users_private_info  WHERE user_id = p_id);

    UPDATE commondb.users_private_info
    SET username = '_DELETED_'||user_id||'_'||username||'_'
      ,email = '_DELETED_'||user_id||'_'||email||'_',
      time_updated = now()
    WHERE user_id = p_id;

    UPDATE profiledb.users
    SET notification_email = '_DELETED_'||profile_id||'_'||notification_email||'_'
    WHERE profile_id = p_id;
  END IF;

  IF user_type = 2 THEN
    UPDATE profiledb.profiles
    SET
      name = '_DELETED_'|| name,
      status = 8
    WHERE profile_id = p_id;
  END IF;

  DELETE FROM profiledb.audio_albums
  WHERE owner_profile_id = p_id;

  DELETE FROM profiledb.audio_tracks
  WHERE owner_profile_id = p_id;

  DELETE FROM profiledb.blog_categories
  WHERE profile_id = p_id;

  DELETE FROM profiledb.blog_posts
  WHERE owner_profile_id = p_id;

  DELETE FROM profiledb.comment_objects
  WHERE profile_id = p_id;

  DELETE FROM profiledb.daily_features
  WHERE profile_id = p_id;

  DELETE FROM profiledb.events
  WHERE profile_id = p_id;

  DELETE FROM profiledb.groups
  WHERE profile_id = p_id;

  DELETE FROM profiledb.notifications
  WHERE profile_id = p_id;

  DELETE FROM profiledb.photo_albums
  WHERE owner_profile_id = p_id;

  DELETE FROM profiledb.photos
  WHERE owner_profile_id = p_id;

  DELETE FROM profiledb.playlist_tracks
  WHERE owner_profile_id = p_id;

  DELETE FROM profiledb.playlists
  WHERE owner_profile_id = p_id;

  DELETE FROM profiledb.profile_backgrounds
  WHERE profile_id = p_id;

  DELETE FROM profiledb.profile_block_settings
  WHERE profile_id = p_id;

  DELETE FROM profiledb.profile_communication_channels
  WHERE profile_id = p_id;

  DELETE FROM profiledb.profile_dialogs
  WHERE profile_id = p_id
        OR companion_profile_id = p_id;

  DELETE FROM profiledb.profile_news
  WHERE profile_id = p_id;

  DELETE FROM profiledb.profile_subscription
  WHERE profile_id = p_id or subject_profile_id = p_id;

  DELETE FROM profiledb.profile_relations
  WHERE profile_id = p_id
        OR subject_profile_id = p_id;

  DELETE FROM profiledb.profile_settings
  WHERE profile_id = p_id;

  DELETE FROM profiledb.profile_status
  WHERE profile_id = p_id;

  DELETE FROM profiledb.tag_objects
  WHERE profile_id = p_id;

  DELETE FROM profiledb.tags
  WHERE profile_id = p_id;

  DELETE FROM profiledb.videos
  WHERE owner_profile_id = p_id;

  DELETE FROM profiledb.wall_posts
  WHERE owner_profile_id = p_id;

  SELECT commondb.delete_profile_feed(p_id) into result;
  RETURN user_type;
END;
$$

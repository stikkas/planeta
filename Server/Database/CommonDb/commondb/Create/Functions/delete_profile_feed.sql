CREATE OR REPLACE FUNCTION commondb.delete_profile_feed(
  p_id BIGINT
)
  RETURNS TEXT AS
  $body$
  DECLARE
    result TEXT;
  BEGIN

    SELECT
      PUBLIC.upload_page('http://feed.planeta.ru/delete-feed-item-with-type-code.json'
                         || '?profileId=' || profile_id
                         || '&objectId=' || object_id
                         || '&feedItemTypeCode=' || TYPE, '') into result
    FROM profiledb.feed
    WHERE profile_id = p_id;
    RETURN result;
  END;
  $body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
CREATE OR REPLACE FUNCTION commondb.decrease_balance (
  p_profile_id bigint,
  p_amount numeric
)
RETURNS pg_catalog.void AS
$body$
declare
  v_can_decrease boolean := false;
begin
  select (balance >= p_amount) into v_can_decrease
    from commondb.profile_balances
    where profile_id = p_profile_id;

  if (v_can_decrease) then
    -- credit transaction creation
    insert into commondb.credit_transactions(transaction_id, profile_id, amount_net, amount_fee, comment)
      values (nextval('commondb.seq_credit_transaction_id'), p_profile_id, p_amount, 0, 'money returning');
    -- balance increasing
    update commondb.profile_balances
      set balance = (balance - p_amount)
      where profile_id = p_profile_id;
  end if;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;

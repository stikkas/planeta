CREATE TABLE commondb.scheduled_blog_posts (
  profile_id BIGINT NOT NULL,
  blog_post_id BIGINT NOT NULL,
  scheduled_time timestamp WITHOUT TIME ZONE DEFAULT NOW() NOT NULL,
  PRIMARY KEY (profile_id, blog_post_id)
);
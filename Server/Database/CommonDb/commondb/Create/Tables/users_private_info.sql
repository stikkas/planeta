CREATE TABLE commondb.users_private_info (
  user_id BIGINT NOT NULL, 
  email VARCHAR(64) NOT NULL, 
  status INTEGER DEFAULT 1 NOT NULL,
  password VARCHAR(32) NOT NULL, 
  username VARCHAR(64) NOT NULL,
  time_added timestamp NOT NULL DEFAULT now(),
  reg_code VARCHAR(64),
  CONSTRAINT users_private_info_email_key UNIQUE(email), 
  CONSTRAINT users_private_info_pkey PRIMARY KEY(user_id),
  CONSTRAINT users_private_info_username_key PRIMARY KEY(username)
) WITHOUT OIDS;

CREATE UNIQUE INDEX users_private_info_reg_code_uq ON commondb.users_private_info
  USING btree (reg_code);
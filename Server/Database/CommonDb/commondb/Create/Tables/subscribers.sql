CREATE TABLE commondb.subscribers (
  subscription_code TEXT NOT NULL, 
  email TEXT NOT NULL
) WITHOUT OIDS;

COMMENT ON COLUMN commondb.subscribers.subscription_code
IS 'Subscription code';

COMMENT ON COLUMN commondb.subscribers.email
IS 'Subscriber email';

GRANT SELECT ON commondb.subscribers TO planeta;
GRANT UPDATE ON commondb.subscribers TO planeta;
GRANT DELETE ON commondb.subscribers TO planeta;
GRANT INSERT ON commondb.subscribers TO planeta;
CREATE TABLE commondb.user_callbacks (
  user_callback_id BIGINT                                    NOT NULL,
  type             TEXT                                      NOT NULL,
  user_id          BIGINT                                    NOT NULL DEFAULT 0,
  user_phone       VARCHAR(16)                               NOT NULL,
  order_type       INTEGER,
  order_object_id  BIGINT                                    NOT NULL DEFAULT 0,
  payment_id       BIGINT                                    NOT NULL DEFAULT 0,
  amount           NUMERIC(10, 2)                            NOT NULL,
  manager_id       BIGINT                                    NOT NULL DEFAULT 0,
  processed        BOOLEAN                                   NOT NULL DEFAULT FALSE,
  time_added       TIMESTAMP WITHOUT TIME ZONE DEFAULT now() NOT NULL,
  time_updated     TIMESTAMP WITHOUT TIME ZONE,
  CONSTRAINT user_callbacks_pkey PRIMARY KEY (user_callback_id)
) WITHOUT OIDS;

COMMENT ON COLUMN commondb.user_callbacks.user_callback_id
IS 'User''s callback request unique identifier.';

COMMENT ON COLUMN commondb.user_callbacks.type
IS 'Type of callback request.';

COMMENT ON COLUMN commondb.user_callbacks.user_id
IS 'User''s profile identifier.';

COMMENT ON COLUMN commondb.user_callbacks.user_phone
IS 'User''s phone number.';

COMMENT ON COLUMN commondb.user_callbacks.payment_id
IS 'Related problem payment identifier.';

COMMENT ON COLUMN commondb.user_callbacks.order_type
IS 'Related order type.';

COMMENT ON COLUMN commondb.user_callbacks.order_object_id
IS 'Related order object identifier.';

COMMENT ON COLUMN commondb.user_callbacks.amount
IS 'Related order''s price or payment''s amount.';

COMMENT ON COLUMN commondb.user_callbacks.manager_id
IS 'Manager''s profile identifier';

COMMENT ON COLUMN commondb.user_callbacks.processed
IS 'Processing status flag.';

COMMENT ON COLUMN commondb.user_callbacks.time_added
IS 'Order creating time';

COMMENT ON COLUMN commondb.user_callbacks.time_updated
IS 'Order last modify time';

COMMENT ON COLUMN commondb.user_callbacks.time_added
IS 'Request creation time.';

COMMENT ON COLUMN commondb.user_callbacks.time_updated
IS 'Request processing time.';
CREATE TABLE commondb.base_delivery_service (
  service_id BIGINT DEFAULT 0 NOT NULL,
  location_id BIGINT NOT NULL,
  name VARCHAR(100) NOT NULL,
  description TEXT,
  address_id BIGINT DEFAULT 0,
  is_enabled BOOLEAN DEFAULT true,
  type INTEGER DEFAULT 0 NOT NULL,
  location_type INTEGER DEFAULT 0 NOT NULL
)
WITH (oids = false);

COMMENT ON COLUMN commondb.base_delivery_service.type
IS 'DeliveryServiceType:
0 - delivery
1 - customer pickup';

COMMENT ON COLUMN commondb.base_delivery_service.location_type
IS 'LocationType:
CITY(0), COUNTRY(1), REGION(2);';
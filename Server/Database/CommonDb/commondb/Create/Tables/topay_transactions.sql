CREATE TABLE commondb.topay_transactions (
  transaction_id BIGINT NOT NULL, 
  profile_id BIGINT NOT NULL,
  order_id BIGINT NOT NULL DEFAULT 0,
  amount_net NUMERIC(10,2) NOT NULL,
  amount_fee NUMERIC(10,2) NOT NULL, 
  status INTEGER NOT NULL, 
  debit_transaction_id BIGINT NOT NULL DEFAULT 0,
  time_added TIMESTAMP WITHOUT TIME ZONE DEFAULT now() NOT NULL, 
  time_updated TIMESTAMP WITHOUT TIME ZONE, 
  payment_system_id INTEGER NOT NULL DEFAULT 0,
  project_id INTEGER NOT NULL DEFAULT 1,
  payment_type_id INTEGER NOT NULL DEFAULT 0,
  param_1 TEXT,
  param_2 TEXT,
  ext_base_transaction_id VARCHAR(50) NULL,
  ext_transaction_id VARCHAR(50) NULL,
  ext_error_code INT NULL,
  ext_error_message VARCHAR(500) NULL,
  CONSTRAINT topay_transactions_pkey PRIMARY KEY(transaction_id)
) WITHOUT OIDS;

COMMENT ON COLUMN commondb.topay_transactions.transaction_id
IS 'Transaction identifer';

COMMENT ON COLUMN commondb.topay_transactions.profile_id
IS 'Profile identifier';

COMMENT ON COLUMN commondb.topay_transactions.order_id
IS 'Assigned order identifier, if exists';

COMMENT ON COLUMN commondb.topay_transactions.amount_net
IS 'Amount net';

COMMENT ON COLUMN commondb.topay_transactions.amount_fee
IS 'Amount fee';

COMMENT ON COLUMN commondb.topay_transactions.status
IS 'Status';

COMMENT ON COLUMN commondb.topay_transactions.debit_transaction_id
IS 'Debit transaction identifier';

COMMENT ON COLUMN commondb.topay_transactions.time_added
IS 'Time added';

COMMENT ON COLUMN commondb.topay_transactions.time_updated
IS 'Time updated';

COMMENT ON COLUMN commondb.topay_transactions.payment_system_id
IS 'Payment system identifier';

COMMENT ON COLUMN commondb.topay_transactions.project_id
IS 'Planeta project identifier';

COMMENT ON COLUMN commondb.topay_transactions.payment_type_id
IS 'Payment type identifier';

COMMENT ON COLUMN commondb.topay_transactions.param_1
IS 'Extra parameter for payment system specific data';

COMMENT ON COLUMN commondb.topay_transactions.ext_transaction_id
IS 'External payment transaction identifier';

COMMENT ON COLUMN commondb.topay_transactions.ext_base_transaction_id
IS 'External base payment transaction identifier (for recurrent payments)';

COMMENT ON COLUMN commondb.topay_transactions.ext_error_code
IS 'External payment error code';

COMMENT ON COLUMN commondb.topay_transactions.ext_error_message
IS 'External payment error description';



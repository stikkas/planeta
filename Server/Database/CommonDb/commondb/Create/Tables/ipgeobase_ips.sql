CREATE TABLE commondb.ipgeobase_ips (
  begin_ip BIGINT NOT NULL, 
  end_ip BIGINT NOT NULL, 
  ip_begin_end VARCHAR(255) NOT NULL,  
  country_id varchar(255) NOT NULL,
  city_id INTEGER, 
  CONSTRAINT ip_pkey PRIMARY KEY(begin_ip)
);
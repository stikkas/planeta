CREATE TABLE commondb.ipgeobase_names (
  city_id INTEGER NOT NULL, 
  city_name VARCHAR(255) NOT NULL, 
  region_name VARCHAR(255) NOT NULL,
  region_name_other VARCHAR(255) NOT NULL,
  latitude  VARCHAR(255) NOT NULL,
  longitude VARCHAR(255) NOT NULL,
  CONSTRAINT ipgeobase_city_pkey PRIMARY KEY(city_id)
);
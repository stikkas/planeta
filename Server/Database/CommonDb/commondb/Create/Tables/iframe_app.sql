CREATE TABLE "commondb"."iframe_app" (
  "iframe_app_id" BIGINT NOT NULL,
  "iframe_app_type_id" INTEGER NOT NULL,
  "iframe_app_external_id" TEXT NOT NULL,
  "iframe_app_secret_key" TEXT NOT NULL,
  "iframe_app_html" TEXT,
  "iframe_app_name" TEXT,
  "campaign_id" BIGINT NOT NULL,
  CONSTRAINT "iframe_app_pkey" PRIMARY KEY("iframe_app_id")
) WITH OIDS;
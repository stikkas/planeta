CREATE TABLE commondb.bonuses (
  bonus_id BIGINT NOT NULL,
  name TEXT NOT NULL,
  description TEXT NOT NULL,
  description_html TEXT NOT NULL,
  image_id BIGINT,
  image_url TEXT,
  price INTEGER DEFAULT 0 NOT NULL,
  CONSTRAINT bonuses_pkey PRIMARY KEY(bonus_id)
)
WITH (oids = false);

COMMENT ON COLUMN commondb.bonuses.bonus_id
IS 'Bonus identifier';

COMMENT ON COLUMN commondb.bonuses.name
IS 'Bonus name';

COMMENT ON COLUMN commondb.bonuses.description
IS 'Bonus description';

COMMENT ON COLUMN commondb.bonuses.description_html
IS 'Bonus description html';

COMMENT ON COLUMN commondb.bonuses.image_id
IS 'Bonus image identifier';

COMMENT ON COLUMN commondb.bonuses.image_url
IS 'Bonus image url';

COMMENT ON COLUMN commondb.bonuses.price
IS 'Bonus price';
CREATE TABLE commondb.promo_codes (
  code VARCHAR(256) NOT NULL,
  email VARCHAR(256),
  time_added TIMESTAMP WITHOUT TIME ZONE,
  confirmcode TEXT,
  amount NUMERIC(10,2) DEFAULT 0 NOT NULL,
  CONSTRAINT promo_codes_pkey PRIMARY KEY(code)
)
WITH (oids = false);

CREATE UNIQUE INDEX email_idx ON commondb.promo_codes
  USING btree ((lower((email)::text)) COLLATE pg_catalog."default");
CREATE TABLE commondb.moderation_messages (
  message_id BIGSERIAL DEFAULT nextval('commondb.seq_moderation_messages_message_id'::text),
  campaign_id BIGINT NOT NULL,
  sender_id BIGINT NOT NULL,
  message TEXT NOT NULL,
  time_added TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT now() NOT NULL,
  status INTEGER NOT NULL,
  CONSTRAINT moderation_messages_pkey PRIMARY KEY(message_id)
)
WITH (oids = false);
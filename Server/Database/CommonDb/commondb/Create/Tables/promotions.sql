CREATE TABLE commondb.promotions (
  promotion_id BIGINT NOT NULL, 
  profile_id BIGINT NOT NULL, 
  name VARCHAR(512) NOT NULL, 
  description TEXT, 
  description_html TEXT, 
  time_begin TIMESTAMP WITHOUT TIME ZONE NOT NULL, 
  time_end TIMESTAMP WITHOUT TIME ZONE NOT NULL, 
  status INTEGER NOT NULL, 
  finished_description TEXT, 
  finished_description_html TEXT, 
  CONSTRAINT prereleases_pkey PRIMARY KEY(promotion_id)
) WITHOUT OIDS;

COMMENT ON COLUMN commondb.promotions.promotion_id
IS 'Promotion identifier';

COMMENT ON COLUMN commondb.promotions.profile_id
IS 'Profile identifier';

COMMENT ON COLUMN commondb.promotions.name
IS 'Name';

COMMENT ON COLUMN commondb.promotions.description
IS 'Description';

COMMENT ON COLUMN commondb.promotions.description_html
IS 'Description html';

COMMENT ON COLUMN commondb.promotions.time_begin
IS 'Time begin';

COMMENT ON COLUMN commondb.promotions.time_end
IS 'Time end';

COMMENT ON COLUMN commondb.promotions.status
IS 'Promotion status';

COMMENT ON COLUMN commondb.promotions.finished_description
IS 'Finished description';

COMMENT ON COLUMN commondb.promotions.finished_description_html
IS 'Finished description html';
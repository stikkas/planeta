CREATE TABLE commondb.campaign_tags (
  tag_id BIGINT NOT NULL,
  tag_name TEXT NOT NULL,
  mnemonic_name TEXT NOT NULL,
  preferred_order INTEGER DEFAULT 0 NOT NULL,
  CONSTRAINT campaign_tags_pkey PRIMARY KEY(tag_id)
) 
WITH (oids = false);

COMMENT ON COLUMN commondb.campaign_tags.tag_id
IS 'Tag identifier';

COMMENT ON COLUMN commondb.campaign_tags.tag_name
IS 'Tag name';

COMMENT ON COLUMN commondb.campaign_tags.mnemonic_name
IS 'Tag mnemonic name';

COMMENT ON COLUMN commondb.campaign_tags.preferred_order
IS 'Preferred order for tag';
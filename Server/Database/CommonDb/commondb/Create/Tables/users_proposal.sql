CREATE TABLE commondb.users_proposal (
  id BIGINT NOT NULL,
  progile_id BIGINT NOT NULL,
  campaign_id BIGINT NOT NULL DEFAULT 0,
  content VARCHAR(1000),
  time_added TIMESTAMP WITHOUT TIME ZONE DEFAULT now() NOT NULL,
  CONSTRAINT users_proposal_pkey PRIMARY KEY(id)
) WITHOUT OIDS;

COMMENT ON COLUMN commondb.users_proposal.id
IS 'User proposal identifier.';

COMMENT ON COLUMN commondb.users_proposal.profile_id
IS 'User profile identifier.';

COMMENT ON COLUMN commondb.users_proposal.campaign_id
IS 'Subject campaign identifier';

COMMENT ON COLUMN commondb.users_proposal.content
IS 'Content of proposal.';

COMMENT ON COLUMN commondb.users_proposal.time_added
IS 'Proposal creation time.';


CREATE TABLE "commondb"."user_last_online_time" (
  "profile_id" BIGINT NOT NULL,
  "last_online_time" TIMESTAMP WITHOUT TIME ZONE DEFAULT now() NOT NULL,
  CONSTRAINT "user_last_online_time_pkey" PRIMARY KEY("profile_id")
) WITHOUT OIDS;

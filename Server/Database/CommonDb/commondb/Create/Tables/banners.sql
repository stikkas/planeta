CREATE TABLE commondb.banners (
  banner_id BIGINT NOT NULL, 
  name VARCHAR(256) NOT NULL,
  html TEXT,
  status INTEGER DEFAULT 0 NOT NULL;
  time_added TIMESTAMP WITHOUT TIME ZONE DEFAULT now() NOT NULL, 
  time_updated TIMESTAMP WITHOUT TIME ZONE DEFAULT now() NOT NULL, 
  CONSTRAINT banners_pkey PRIMARY KEY(banner_id)
) WITHOUT OIDS;

COMMENT ON COLUMN commondb.banners.time_added
IS 'Time added';
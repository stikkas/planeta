CREATE TABLE commondb.orders (
  order_id BIGINT NOT NULL,
  order_type INTEGER NOT NULL DEFAULT 0,
  buyer_id BIGINT NOT NULL,
  topay_transaction_id BIGINT NOT NULL DEFAULT 0, 
  credit_transaction_id BIGINT NOT NULL DEFAULT 0, 
  debit_transaction_id BIGINT NOT NULL DEFAULT 0, 
  cancel_credit_transaction_id BIGINT NOT NULL DEFAULT 0, 
  cancel_debit_transaction_id BIGINT NOT NULL DEFAULT 0, 
  payment_status INT NOT NULL,
  payment_type INT NOT NULL,
  delivery_status INT NOT NULL,
  delivery_type INT NOT NULL,  
  delivery_price NUMERIC(10,2) NOT NULL DEFAULT 0, 
  delivery_service_id INT DEFAULT 0,
	addressee_id BIGINT NOT NULL DEFAULT 0,
  time_added TIMESTAMP WITHOUT TIME ZONE DEFAULT now() NOT NULL,
  time_updated TIMESTAMP WITHOUT TIME ZONE DEFAULT now() NOT NULL,
  reserve_time_expired TIMESTAMP WITHOUT TIME ZONE,
  total_price NUMERIC(10,2) DEFAULT 0 NOT NULL,
  error_code TEXT,
  CONSTRAINT orders_items_pkey PRIMARY KEY(order_id)
) WITHOUT OIDS;

COMMENT ON COLUMN commondb.orders.order_id
IS 'Order identifier';

COMMENT ON COLUMN commondb.orders.order_type
IS 'Order type, specifies type of contained objects.';

COMMENT ON COLUMN commondb.orders.buyer_id
IS 'Buyer identifier';

COMMENT ON COLUMN commondb.orders.delivery_price
IS 'Delivery price';

COMMENT ON COLUMN commondb.orders.delivery_status
IS 'Order delivery status';

COMMENT ON COLUMN commondb.orders.payment_status
IS 'Order payment status';

COMMENT ON COLUMN commondb.orders.delivery_type
IS 'Order delivery type';

COMMENT ON COLUMN commondb.orders.payment_type
IS 'Order payment type';

COMMENT ON COLUMN commondb.orders.address_id
IS 'Order address identifier';

COMMENT ON COLUMN commondb.orders.delivery_service_id
IS 'Delivery service identifier';

COMMENT ON COLUMN commondb.orders.time_added
IS 'Order creating time';

COMMENT ON COLUMN commondb.orders.time_updated
IS 'Order last modify time';

COMMENT ON COLUMN commondb.orders.topay_transaction_id
IS 'Order topay transaction, if order assigned with deferred payment';

COMMENT ON COLUMN commondb.orders.credit_transaction_id
IS 'Order credit transaction';

COMMENT ON COLUMN commondb.orders.debit_transaction_id
IS 'Order debit Transaction';

COMMENT ON COLUMN commondb.orders.cancel_credit_transaction_id
IS 'Cancel credit transaction';

COMMENT ON COLUMN commondb.orders.cancel_debit_transaction_id
IS 'Cancel debit Transaction';

COMMENT ON COLUMN commondb.orders.total_price
IS 'Order total price';

COMMENT ON COLUMN commondb.orders.error_code
IS 'Order processing error code, or empty string.';

COMMENT ON COLUMN commondb.orders.reserve_time_expired
IS 'Order reservation time expired.';

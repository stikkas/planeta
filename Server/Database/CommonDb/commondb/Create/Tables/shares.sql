CREATE TABLE commondb.shares (
  share_id BIGINT NOT NULL, 
  campaign_id BIGINT NOT NULL, 
  profile_id BIGINT NOT NULL, 
  name TEXT NOT NULL, 
  description TEXT NOT NULL, 
  image_id BIGINT, 
  image_url TEXT, 
  price NUMERIC(10,2) NOT NULL, 
  amount INTEGER NOT NULL, 
  time_added TIMESTAMP WITHOUT TIME ZONE DEFAULT now() NOT NULL,
  reward_instruction TEXT,  
  order_num INTEGER DEFAULT 0,
  CONSTRAINT shares_pkey PRIMARY KEY(share_id)
) WITHOUT OIDS;

COMMENT ON COLUMN commondb.shares.share_id
IS 'Share identifier';

COMMENT ON COLUMN commondb.shares.campaign_id
IS 'Share campaign';

COMMENT ON COLUMN commondb.shares.profile_id
IS 'Share profile';

COMMENT ON COLUMN commondb.shares.name
IS 'Share name';

COMMENT ON COLUMN commondb.shares.description
IS 'Share description';

COMMENT ON COLUMN commondb.shares.image_id
IS 'Share image';

COMMENT ON COLUMN commondb.shares.image_url
IS 'Share image url';

COMMENT ON COLUMN commondb.shares.price
IS 'Share price';

COMMENT ON COLUMN commondb.shares.amount
IS 'Share amount, 0 is unlimited';

COMMENT ON COLUMN commondb.shares.time_added
IS 'Share time added';

COMMENT ON COLUMN commondb.shares.reward_instruction
IS 'Instruction to obtain reward';

COMMENT ON COLUMN commondb.shares.order_num
IS 'Field that set shares order';
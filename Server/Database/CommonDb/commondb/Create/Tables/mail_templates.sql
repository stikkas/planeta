﻿CREATE TABLE commondb.mail_templates (
  template_id INTEGER DEFAULT nextval('commondb.seq_mail_template_id') NOT NULL,
  name VARCHAR(250) NOT NULL,
  subject TEXT,
  footer TEXT,
  content_bbcode TEXT,
  from_address VARCHAR(500),
  CONSTRAINT mail_templates_pkey PRIMARY KEY(template_id)
) WITHOUT OIDS;

COMMENT ON COLUMN commondb.mail_templates.template_id
IS 'Template identifier';

COMMENT ON COLUMN commondb.mail_templates.name
IS 'Template name';

COMMENT ON COLUMN commondb.mail_templates.subject
IS 'Template subject';

COMMENT ON COLUMN commondb.mail_templates.footer
IS 'Template footer';

COMMENT ON COLUMN commondb.mail_templates.content_bbcode
IS 'Template content bbcode';

COMMENT ON COLUMN commondb.mail_templates.from_address
IS 'Template from address';


/* Data for the 'commondb.mail_templates' table  (Records 1 - 20) */

INSERT INTO commondb.mail_templates ("name", "subject", "footer", "content_bbcode", "from_address")
        VALUES (E'test', E'test', E'С наилучшими пожеланиями,<br/>\r\n\r\nкоманда <a style=\"color:#399cbf;\" href=\"${appHost}\">Планеты</a>!<br/>', E'[h1]Здравствуйте, Варлаам Виниаминович![/h1]\r\n\r\n[p]Вы зарегистрировались в проекте Планета.\r\n\r\nАктивируйте аккаунт для того, чтобы получить доступ ко всем функциям на Планете.[/p]\r\n\r\n[p]Ваш пароль:[/p]\r\n\r\n\r\n\r\n[h2]Заголовок второго уровня[/h2][h4]Подзаголовок обычно бывает более длинным, чем заголовок, под которым он находится. А используются они вместе или по отдельности.[/h4][p]Во время своего правления Генрих проводил наступательную <a href=\"#\" style=\"color:#399cbf;\"><font color=\"#399cbf\">внешнюю политику</font></a>. Талантливый правитель и искусный политик, он смог значительно укрепить Германское королевство, расширив его территорию за счёт присоединения Лотарингии.[/p][h3]Заголовок третьего уровня[/h3][p]Для обороны от набегов венгров Генрих строил укреплённые поселения (бурги) и создал мощную конницу, что позволило ему добиться решающей победы при Риаде. Во время правления Генриха началось завоевание полабских славян.[/p][p]Какой-нибудь нумерованный список может быть представлен следующим образом:[/p][ol][li]Первый пункт[/li][li]Второй элемент[/li][li]Третий очень длинный и непонятный[/li][li]Четвертый[/li][/ol][p]А ненумерованный список может быть представлен так:[/p][ul][li]Первый пункт[/li][li]Второй элемент[/li][li]Третий очень длинный и непонятный[/li][li]Четвертый[/li][/ul]\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n[h2]h2_header[/h2]\r\n\r\n[h3]h3_header[/h3]\r\n\r\n[h4]h4_header[/h4]', E'noreply@planeta.ru');

INSERT INTO commondb.mail_templates ("name", "subject", "footer", "content_bbcode", "from_address")
        VALUES (E'balance.credited.notice', E'Ваш баланс на Планете пополнен.', E'Если у Вас возникли какие-то трудности, то Вы всегда можете сообщить о них в нашу <a style=\"color:#399cbf;\" href=\"mailto:support@planeta.ru\">службу поддержки</a>.', E'[h3]${dearByGender} ${userName}![/h3][p]Ваш баланс на Планете пополнен на [b]${amount}[/b] руб.[/p]', E'noreply@planeta.ru');

INSERT INTO commondb.mail_templates ("name", "subject", "footer", "content_bbcode", "from_address")
        VALUES (E'registration.resend', E'Планета - Подтверждение регистрации', E'С наилучшими пожеланиями,<br/>\r\n\r\nкоманда <a style=\"color:#399cbf;\" href=\"${appHost}\">Планеты</a>!<br/>', E'[h2]Благодарим вас за приземление на Планету![/h2]\r\n\r\n[p]Ваш логин: [b]${email}[/b][/p]\r\n\r\n[p]Для завершения регистрации на Планете вам необходимо в течение двух недель обратиться в специальный иммиграционный пункт, находящийся по адресу: [url=${url}/welcome/confirm.html?email=${email}&regCode=${regCode}]${url}/welcome/confirm.html?email=${email}&regCode=${regCode}[/url][/p]\r\n\r\n[p]Впрочем, вы можете этого и не делать, но по истечении срока все визы будут аннулированы, а аккаунт подвергнется бану.[/p]', E'noreply@planeta.ru');

INSERT INTO commondb.mail_templates ("name", "subject", "footer", "content_bbcode", "from_address")
        VALUES (E'confirmation.default', E'Подтверждение действия', E'Если Вы получили это письмо по ошибке, то Вы всегда можете сообщить об этом в нашу <a style=\"color:#399cbf;\" href=\"mailto:admin@planeta.ru\">службу поддержки</a>.', E'[p]${message}[/p] [p]Код подтверждения: ${code}.[/p]', E'noreply@planeta.ru');

INSERT INTO commondb.mail_templates ("name", "subject", "footer", "content_bbcode", "from_address")
        VALUES (E'purchase.share.notice', E'Спасибо за приобретение акции на Планете!', E'Если у Вас возникли какие-то трудности, то Вы всегда можете сообщить о них в нашу <a style=\"color:#399cbf;\" href=\"mailto:support@planeta.ru\">службу поддержки</a>.', E'[h3]${dearByGender} ${userName}![/h3][p]Вы приобрели на сайте planeta.ru акции [i]${shareName}[/i] проекта [url=${appHost}/campaigns/${projectLink}]${projectName}[/url] от сообщества [i]${groupName}[/i].[/p][ul][li]Номер заказа: [b]${orderId}[/b][/li][li]Стоимость заказа: [b]${totalPrice} руб.[/b][/li][li]Цена одной акции: [b]${price} руб.[/b][/li][li]Количество: [b]${count}[/b][/li][/ul][p]Вы можете ознакомиться со списком ваших покупок на планете на странице [url]${appHost}/account[/url].\r\n\r\nИнформация о том, как получить вознаграждение, будет указана в распечатке акции, либо выслана вам по окончании проекта.[/p]', E'noreply@planeta.ru');

INSERT INTO commondb.mail_templates ("name", "subject", "footer", "content_bbcode", "from_address")
        VALUES (E'purchase.share.notice.courier', E'Спасибо за приобретение акции на Планете!', E'Если у Вас возникли какие-то трудности, то Вы всегда можете сообщить о них в нашу <a style=\"color:#399cbf;\" href=\"mailto:support@planeta.ru\">службу поддержки</a>.', E'[h3]${dearByGender} ${userName}![/h3][p]Вы приобрели на сайте planeta.ru акции [i]${shareName}[/i] проекта [url=${appHost}/campaigns/${projectLink}]${projectName}[/url] от сообщества [i]${groupName}[/i].[/p][ul][li]Номер заказа: [b]${orderId}[/b][/li][li]Стоимость заказа: [b]${totalPrice} руб.[/b][/li][li]Цена одной акции: [b]${price} руб.[/b][/li][li]Количество: [b]${count}[/b][/li][/ul][p]Вы можете ознакомиться со списком ваших покупок на планете на странице [url]${appHost}/account[/url].[/p]\r\n\r\n[p]Заказ будет доставлен по указанному Вами адресу:\r\n\ ${zipCode}, ${country}, ${city}, ${street}, тел.: ${phone}.[/p]', E'noreply@planeta.ru');
INSERT INTO commondb.mail_templates ("name", "subject", "footer", "content_bbcode", "from_address")
        VALUES (
          E'purchase.share.notice.customer.pickup',
          E'Спасибо за приобретение акции на Планете!',
          E'Если у Вас возникли какие-то трудности, то Вы всегда можете сообщить о них в нашу <a style=\"color:#399cbf;\" href=\"mailto:support@planeta.ru\">службу поддержки</a>.',
          E'[h3]${dearByGender} ${userName}![/h3]
          [p]Вы приобрели на сайте planeta.ru акции [i]${shareName}[/i] проекта [url=${appHost}/campaigns/${projectLink}]${projectName}[/url] от сообщества [i]${groupName}[/i].[/p]
          [ul][li]Номер заказа: [b]${orderId}[/b][/li]
          [li]Стоимость заказа: [b]${totalPrice} руб.[/b][/li]
          [li]Цена одной акции: [b]${price} руб.[/b][/li]
          [li]Количество: [b]${count}[/b][/li][/ul]
          [p]Вы можете ознакомиться со списком ваших покупок на планете на странице [url]${appHost}/account[/url].[/p]
          \r\n\r\n[p]Заказ можно будет забрать по адресу:\r\n\ ${zipCode}, ${country}, ${city}, ${street}, тел.: ${phone}.[/p]',
          E'noreply@planeta.ru');
INSERT INTO commondb.mail_templates ("name", "subject", "footer", "content_bbcode", "from_address")
  VALUES (E'purchase.share.customer.pickup.with.instruction.notice',
          E'Спасибо за приобретение акции на Планете!',
          E'Если у Вас возникли какие-то трудности, то Вы всегда можете сообщить о них в нашу <a style=\"color:#399cbf;\" href=\"mailto:support@planeta.ru\">службу поддержки</a>.',
          E'[h3]${dearByGender} ${userName}![/h3]
          [p]Вы приобрели на сайте planeta.ru акцию [i]${shareName}[/i] проекта [url=${appHost}/${projectLink}]${projectName}[/url] от сообщества [i]${groupName}[/i].[/p]
          [ul][li]Номер заказа: [b]${orderId}[/b][/li]
          [li]Стоимость заказа: [b]${totalPrice} руб.[/b][/li]
          [li]Цена одной акции: [b]${price} руб.[/b][/li]
          [li]Количество: [b]${count}[/b][/li][/ul]
          [p]Вы можете ознакомиться со списком ваших покупок на планете на странице [url]${appHost}/account[/url].\r\n\r\nКак получить вознаграждение:[/p]
          [p]${instruction}[/p]
          \r\n[p]Заказ можно будет забрать по адресу:\r\n\ ${zipCode}, ${country}, ${city}, ${street}, тел.: ${phone}.[/p]',
          E'noreply@planeta.ru');
INSERT INTO commondb.mail_templates ("name", "subject", "footer", "content_bbcode", "from_address")
  VALUES (E'purchase.share.with.instruction.notice', E'Спасибо за приобретение акции на Планете!', E'Если у Вас возникли какие-то трудности, то Вы всегда можете сообщить о них в нашу <a style=\"color:#399cbf;\" href=\"mailto:support@planeta.ru\">службу поддержки</a>.', E'[h3]${dearByGender} ${userName}![/h3][p]Вы приобрели на сайте planeta.ru акцию [i]${shareName}[/i] проекта [url=${appHost}/${projectLink}]${projectName}[/url] от сообщества [i]${groupName}[/i].[/p][ul][li]Номер заказа: [b]${orderId}[/b][/li][li]Стоимость заказа: [b]${totalPrice} руб.[/b][/li][li]Цена одной акции: [b]${price} руб.[/b][/li][li]Количество: [b]${count}[/b][/li][/ul][p]Вы можете ознакомиться со списком ваших покупок на планете на странице [url]${appHost}/account[/url].\r\n\r\nКак получить вознаграждение:[/p]\r\n${instruction}', E'noreply@planeta.ru');

INSERT INTO commondb.mail_templates ("name", "subject", "footer", "content_bbcode", "from_address")
        VALUES (E'feedback', E'feedback', E'С наилучшими пожеланиями,<br/>\r\n\r\nкоманда <a style=\"color:#399cbf;\" href=\"${appHost}\">Планеты</a>!<br/>', E'[p]${message}[/p]', E'support@dev.planeta.ru');

INSERT INTO commondb.mail_templates ("name", "subject", "footer", "content_bbcode", "from_address")
        VALUES (E'notification', E'Планета - Новые уведомления', E'С наилучшими пожеланиями,<br/>\r\n\r\nкоманда <a style=\"color:#399cbf;\" href=\"${appHost}\">Планеты</a>!<br/>', E'[h1]Здравствуйте, ${displayName}![/h1]\r\n\r\n[p]Вашего внимания заслуживают следующие события: ${content} Вы можете просмотреть Ваши уведомления на странице [url=${appHost}/notifications]${appHost}/notifications[/url][/p]', E'noreply@planeta.ru');

INSERT INTO commondb.mail_templates ("name", "subject", "footer", "content_bbcode", "from_address")
        VALUES (E'temp.template', E'Спасибо за приобретение акции на Планете!', E'Если у Вас возникли какие-то трудности, то Вы всегда можете сообщить о них в нашу <a style=\"color:#399cbf;\" href=\"mailto:admin@planeta.ru\">службу поддержки</a>.', E'[h2]Благодарим вас за вклад в музыкальную деятельность группы Brainstorm.[/h2]\r\n\r\nДля того, что скачать альбом, перейдите по указанной ссылке [url]http://s1.planeta.ru/promo/Another_Still_Life.zip[/url].\r\n\r\nСкачивание начнется автоматически.\r\n\r\nАльбом находится в архиве формата .ZIP. Для извлечения файлов вам потребуется программа-распаковщик архивов (например, WinRAR, WinZIP).', E'noreply@planeta.ru');

INSERT INTO commondb.mail_templates ("name", "subject", "footer", "content_bbcode", "from_address")
        VALUES (E'password.recovery', E'Планета - Восстановление пароля', E'С наилучшими пожеланиями,<br/>\r\n\r\nкоманда <a style=\"color:#399cbf;\" href=\"${appHost}\">Планеты</a>!<br/>', E'[p]Чтобы изменить Ваш пароль, пожалуйста, пройдите по ссылке:[url=${url}/welcome/confirm-recover.html?email=${email}&regCode=${regCode}]${url}/welcome/confirm-recover.html?email=${email}&regCode=${regCode}[/url][/p]', E'noreply@planeta.ru');

INSERT INTO commondb.mail_templates ("name", "subject", "footer", "content_bbcode", "from_address")
        VALUES (E'purchase.products.notice', E'Ваш заказ принят!', E'Если у Вас возникли вопросы по поводу оформления заказа и покупки товаров, Вы можете связаться с нами по телефону <a style="color:#399cbf;" href="callto:+74992657464">(499)265 74 64</a> или написать на наш <a style="color:#399cbf;" href="mailto:shop@planeta.ru">e-mail</a>.', E'[h3]${dearByGender} ${userName}![/h3][p]Ваш заказ принят![/p][p]В ближайшее время с Вами свяжется наш оператор для подтверждения заказа.[/p]
Список товаров:[table][tr][th][i]№ п/п[/i][/th][th][i]Наименование[/i][/th][th][i]Количество[/i][/th][th][i]Стоимость[/i][/th][/tr]${products}[/table]
[ul][li]Общая Стоимость заказа: [b]${totalPrice}[/b] руб.;[/li][li]Стоимость доставки: [b]${deliveryPrice}[/b] руб.;[/li][/ul]
[ul][li]Выбранный способ оплаты: [b]${paymentType}[/b];[/li][li]Выбранный способ доставки: [b]${deliveryType}[/b];[/li][/ul]
[p]Вы можете ознакомиться со списком ваших покупок на Планете на странице [url]${appHost}/account[/url].[/p]', E'noreply@planeta.ru');

INSERT INTO commondb.mail_templates ("name", "subject", "footer", "content_bbcode", "from_address")
        VALUES (E'registration', E'Планета - Подтверждение регистрации', E'С наилучшими пожеланиями,<br/>\r\n\r\nкоманда <a style=\"color:#399cbf;\" href=\"${appHost}\">Планеты</a>!<br/>', E'[h2]Благодарим вас за приземление на Планету![/h2]\r\n\r\n[p]Ваш логин: [b]${email}[/b][/p][p]Пароль, указанный при регистрации: [b]${password}[/b][/p]\r\n\r\n[p]Для завершения регистрации на Планете вам необходимо в течение двух недель обратиться в специальный иммиграционный пункт, находящийся по адресу: [url=${url}/welcome/confirm.html?email=${email}&regCode=${regCode}]${url}/welcome/confirm.html?email=${email}&regCode=${regCode}[/url][/p]\r\n\r\n[p]Впрочем, вы можете этого и не делать, но по истечении срока все визы будут аннулированы, а аккаунт подвергнется бану.[/p]', E'noreply@planeta.ru');

INSERT INTO commondb.mail_templates ("name", "subject", "footer", "content_bbcode", "from_address")
        VALUES (E'project.other.merchant.confirm', E'Планета - подтверждение участия в акционировании', E'С уважением,<br/>\r\nМария Сорокина<br/>\r\nменеджер немузыкальных проектов портала <a style=\"color:#399cbf;\" href=\"http://planeta.ru/\">planeta.ru</a><p style=\"margin-top:0px; margin-bottom:0;\"/>\r\n8 926 910 01 51<br/>\r\n<a style=\"color:#399cbf;\" href=\"mailto:maria@planeta.ru\">maria@planeta.ru</a>', E'[h3]Здравствуйте![/h3][p]Ваша заявка на участие в акционировании принята.\r\nПрежде чем запустить проект мы предлагаем вам заполнить небольшую таблицу. Она поможет ответить на важнейшие вопросы и позволит более тщательно подготовиться к старту проекта.\r\nПока вы ее заполняете, мы уже торопимся связаться с вами. Если вы уже высылали нам свой номер телефона, мы сделаем это в ближайшее время.\r\nТакже вы можете связаться с нами лично по телефону или e-mail, указанным в подписи.[/p]\r\n[p]Всего вам самого доброго![/p]', E'noreply@planeta.ru');

INSERT INTO commondb.mail_templates ("name", "subject", "footer", "content_bbcode", "from_address")
        VALUES (E'purchase.tickets.notice', E'Спасибо за приобретение билетов на Планете!', E'Если у Вас возникли какие-то трудности, то Вы всегда можете сообщить о них в нашу <a style=\"color:#399cbf;\" href=\"mailto:support@planeta.ru\">службу поддержки</a>.', E'[h3]${dearByGender} ${userName}![/h3][p]Вы приобрели на сайте planeta.ru билеты на событие [url=${appConcertsHost}/concerts/${eventLink}]${eventName}[/url].[/p][p]Заказ №[b]${orderId}[/b] от [b]${timeAdded}[/b] общей стоимостью [b]${totalPrice}[/b] руб.[/p]\r\[p]Cписок билетов:[table][tr][th][i]№ п/п[/i][/th][th][i]Наименование[/i][/th][th][i]Количество[/i][/th][th][i]Стоимость[/i][/th][/tr]${tickets}[/table][/p][p]Для того, чтобы получить билет, распечатайте бланк приобритенного билета в разделе [i]Мои покупки[/i] на странице [url]${appHost}/account[/url].[/p]', E'noreply@planeta.ru');

INSERT INTO commondb.mail_templates ("name", "subject", "footer", "content_bbcode", "from_address")
  VALUES (E'gift.money.notice', E'Подарок от Планеты!', E'Если у Вас возникли какие-то трудности, то Вы всегда можете сообщить о них в нашу <a style=\"color:#399cbf;\" href=\"mailto:support@planeta.ru\">службу поддержки</a>.', E'[h3]${dearByGender} ${userName}![/h3][p]"Планета" дарит Вам [b]${amount}[/b] ${rubles} на Ваш счёт.[/p]\r\n[p]${message}[/p]\r\n[p]Вы можете ознакомиться с состоянием Вашего счета [url=${appHost}/account/payments]здесь[/url].[/p]', E'noreply@planeta.ru');

INSERT INTO commondb.mail_templates ("name", "subject", "footer", "content_bbcode", "from_address")
  VALUES (E'gift.share.notice', E'Подарок от Планеты!', E'Если у Вас возникли какие-то трудности, то Вы всегда можете сообщить о них в нашу <a style=\"color:#399cbf;\" href=\"mailto:support@planeta.ru\">службу поддержки</a>.', E'[h3]${dearByGender} ${userName}![/h3][p]"Планета" дарит Вам ${shareCount} [b]${shareName}[/b] проекта [url=${appHost}/campaigns/${campaignId}]${campaignName}[/url] от сообщества [url=${appHost}/${groupId}]${groupName}[/url].[/p]\r\n[p]${message}[/p]\r\n[p]Вы можете ознакомиться со списком ваших покупок и подарков [url=${appHost}/account/purchases]здесь[/url].\r\nИнформация о том, как получить вознаграждение, будет указана в распечатке акции, либо выслана вам по окончании проекта.[/p]', E'noreply@planeta.ru');

INSERT INTO commondb.mail_templates ("name", "subject", "footer", "content_bbcode", "from_address")
	VALUES (E'gift.ticket.notice', E'Подарок от Планеты!', E'Если у Вас возникли какие-то трудности, то Вы всегда можете сообщить о них в нашу <a style=\"color:#399cbf;\" href=\"mailto:support@planeta.ru\">службу поддержки</a>.', E'[h3]${dearByGender} ${userName}![/h3][p]"Планета" дарит Вам ${ticketCount} [b]${ticketName}[/b] на концерт [url=${appConcertsHost}/concerts/${eventId}]${eventName}[/url].[/p]\r\n[p]${message}[/p]\r\n[p]Для того, чтобы получить билет, распечатайте бланк билета на странице [url=${appHost}/account/purchases]"Мои покупки"[/url].[/p]', E'noreply@planeta.ru');

INSERT INTO commondb.mail_templates ("name", "subject", "footer", "content_bbcode", "from_address")
	VALUES (E'payment.fail.user.feedback', E'Ошибка платежа', E'Если у Вас возникли какие-то трудности, то Вы всегда можете сообщить о них в нашу <a style=\"color:#399cbf;\" href=\"mailto:admin@planeta.ru\">службу поддержки</a>.', E'[h3]${dearByGender} ${userName}![/h3][p]Вы обратились в службу поддержки пользователей по вопросам оплаты.[/p][p]Специалист службы поддержки ответит Вам в течение дня.[/p]', E'payment.support@planeta.ru');

INSERT INTO commondb.mail_templates ("name", "subject", "footer", "content_bbcode", "from_address")
	VALUES (E'payment.fail.admin.feedback', E'Проблема с оплатой у пользователя ${userName}', E'<p></p>', E'[p]Пользователь ${userEmail} просит связаться с ним в связи с ошибкой проведения платежа.[/p][p]Пользователь приложил сообщение:[/p][p]${message}[/p]', E'noreply@planeta.ru');

INSERT INTO commondb.mail_templates ("name", "subject", "footer", "content_bbcode", "from_address")
  VALUES (
    E'group.created.campaign',
    E'Заявка на создание проекта',
    E'<br/>
      С наилучшими пожеланиями,<br/>
      команда <a style="color:#399cbf;" href="${appHost}">Планеты</a>!<br/>',
    E'[h1]здравствуйте, ${managerName},[/h1]
      [p]Сообщество [url=${appAdminHost}/moderator/group-info.html?profileId=${groupId}]${groupName}[/url] подало заявку на создание проекта [url=${appHost}/campaigns/${campaignId}]${campaignName}[/url].[/p]',
    E'${managerEmail}'
  );

INSERT INTO commondb.mail_templates ("name", "subject", "footer", "content_bbcode", "from_address")
  VALUES (
    E'you.created.group.campaign',
    E'Планета - заявка на участие в акционировании',
    E'С уважением,<br/>
      ${managerDisplayName}<br/>
      менеджер проектов портала Планета<p style="margin-top:0px; margin-bottom:0;"/>
      <a style="color:#399cbf;" href="mailto:${managerEmail}">${managerEmail}</a>',
    E'[h3]Здравствуйте, ${userName}![/h3]
      [p]Ваша заявка на создание проекта [url=${appHost}/campaigns/${campaignId}]"${campaignName}"[/url] принята.[/p]',
    E'${managerEmail}'
  );

INSERT INTO commondb.mail_templates ("name", "subject", "footer", "content_bbcode", "from_address")
  VALUES (
    E'your.broadcast.link',
    E'Планета -  Ваша ссылка на просмотр трансляции',
    E'С наилучшими пожеланиями,<br/>команда <a style="color:#399cbf;" href="${appHost}">Планеты</a>!<br/>',
    E'[h3]Здравствуйте![/h3][p]Ваша ссылка на просмотр трансляции "${broadcastName}": [url=${broadcastUrl}]${broadcastUrl}[/url].[/p][p]Ссылка будет действительна в течение ${hours} часов в с момента первого перехода.[/p][p]Приятного просмотра![/p]',
    E'support@planeta.ru'
  );


INSERT INTO commondb.mail_templates ("name", "subject", "footer", "content_bbcode", "from_address")
    VALUES (
        E'purchase.error.campaign.not.active',
        E'Ошибка оплаты заказа из-за завершения проекта',
        E'С наилучшими пожеланиями,<br>
команда <a href="${appHost}">Планеты</a>!',
        E'[h3]Здравствуйте, ${userName}![/h3]
[p]Вы оплатили покупку №${orderId} в проекте "${campaignName}".
К сожалению, ваш платеж дошел до нас слишком поздно, и проект "${campaignName}" уже закрылся. Деньги, уплаченные Вами, находятся в вашем личном кошельке.[/p][p]В таком случае вы можете воспользоваться одной из следующих возможностей:[ul][li]потратить деньги на покупку других [url=${appHost}/search/projects/query=&status=active]акций[/url] .[/li][li]пожертвовать деньги на [url=${appHost}/search/projects/query=&status=CHARITY]благотворительные проекты[/url].[/li][li]вернуть деньги в соответствии с [url=${appHost}/welcome/payment-return.html]Условиями возврата денежных средств[/url].[/li][/ul]С информацией о своих покупках и платежах вы можете ознакомиться на странице [url=${appHost}/account]личного кабинета[/url].[/p]',
        E'support@dev.planeta.ru'
    );

INSERT INTO commondb.mail_templates ("name", "subject", "footer", "content_bbcode", "from_address")
    VALUES (
        E'purchase.error.not.enough.share.amount',
        E'Ошибка оплаты заказа из-за окончания акций',
        E'С наилучшими пожеланиями,<br>
команда <a href="${appHost}">Планеты</a>!',
        E'[h3]Здравствуйте, ${userName}![/h3]
[p]Вы оплатили покупку №${orderId} в проекте "${campaignName}".
К сожалению, ваш платеж дошел до нас слишком поздно, и акции "${shareName}" уже закончились. Деньги, уплаченные Вами, находятся в вашем личном кошельке.[/p][p]В таком случае вы можете воспользоваться одной из следующих возможностей:[ul][li]потратить деньги на покупку других [url=${appHost}/search/projects/query=&status=active]акций[/url] .[/li][li]пожертвовать деньги на [url=${appHost}/search/projects/query=&status=CHARITY]благотворительные проекты[/url].[/li][li]вернуть деньги в соответствии с [url=${appHost}/welcome/payment-return.html]Условиями возврата денежных средств[/url].[/li][/ul]С информацией о своих покупках и платежах вы можете ознакомиться на странице [url=${appHost}/account]личного кабинета[/url].[/p]',
        E'support@dev.planeta.ru'
    );

INSERT INTO commondb.mail_templates ("name", "subject", "footer", "content_bbcode", "from_address")
VALUES (E'bonus.order.added',
        E'Новый заказ бонуса',
        E'С наилучшими пожеланиями, <br/> команда <a href="${appHost}">Планеты</a>!<br/>',
        E'[h3]Новый бонусный заказ на Планете[/h3]
    [p]Создан новый заказ с бонусом [i]"${bonusName}"[/i]:[/p]
    [ul]
    [li]Номер заказа:[b]${orderId}[/b][/li]
    [li]Дата создания:[b]${timeAdded}[/b][/li]
    [li]Покупатель:[b]${buyerName}[/b], email:[b]${buyerEmail}[/b][/li]
    [li]Получатель:[b]${addresseeName}[/b], тел.:[b]${buyerPhone}[/b][/li]
    [li]Способ доставки:[b]${deliveryType}[/b][/li]
    [li]Адрес доставки:[b]${deliveryAddress}[/b][/li]
    [/ul]
    [p]Для работы с заказом воспользуйтесь [url=${appAdminHost}/moderator/loyalty-orders.html]страницей администрирования заказов[/url].[/p]',
        E'noreply@planeta.ru');
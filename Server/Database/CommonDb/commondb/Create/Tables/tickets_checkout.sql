CREATE TABLE commondb.tickets_checkout (
  order_id BIGINT NOT NULL,
  order_object_id BIGINT NOT NULL,
  time_checkout TIMESTAMP WITHOUT TIME ZONE DEFAULT now() NOT NULL
) WITHOUT OIDS;

COMMENT ON COLUMN commondb.tickets_checkout.order_id
IS 'Order indentifier';

COMMENT ON COLUMN commondb.tickets_checkout.order_object_id
IS 'Order object indentifier';

COMMENT ON COLUMN commondb.tickets_checkout.time_checkout
IS 'Time ticket checked out';
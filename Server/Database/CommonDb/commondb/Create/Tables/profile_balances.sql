CREATE TABLE commondb.profile_balances (
  profile_id BIGINT NOT NULL, 
  balance NUMERIC(16,4) DEFAULT 0 NOT NULL,
  CONSTRAINT profile_balances_pkey PRIMARY KEY(profile_id)
) WITHOUT OIDS;

COMMENT ON COLUMN commondb.profile_balances.profile_id
IS 'Profile identifier';

COMMENT ON COLUMN commondb.profile_balances.balance
IS 'Balance';

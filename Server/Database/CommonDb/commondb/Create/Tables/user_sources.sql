CREATE TABLE commondb.user_sources (
  profile_id BIGINT NOT NULL PRIMARY KEY,
  enter_url TEXT NOT NULL,
  referrer_url TEXT
);
COMMENT ON COLUMN commondb.user_sources.profile_id
IS 'Id of just registered profile';
COMMENT ON COLUMN commondb.user_sources.enter_url
IS 'First Planeta url visited by just registered user';
COMMENT ON COLUMN commondb.user_sources.referrer_url
IS 'Referrer url of first visit of just registered user';
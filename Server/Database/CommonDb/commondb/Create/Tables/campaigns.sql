﻿CREATE TABLE commondb.campaigns (
  campaign_id BIGINT NOT NULL,
  profile_id BIGINT NOT NULL,
  name TEXT NOT NULL,
  description TEXT NOT NULL,
  description_html TEXT NOT NULL,
  short_description TEXT,
  image_id BIGINT,
  image_url TEXT,
  time_start TIMESTAMP WITHOUT TIME ZONE,
  time_finish TIMESTAMP WITHOUT TIME ZONE,
  target_amount NUMERIC(16,2) DEFAULT 0 NOT NULL,
  collected_amount NUMERIC(16,2) DEFAULT 0 NOT NULL,
  ignore_target_amount BOOLEAN NOT NULL,
  notification_method INTEGER NOT NULL,
  status INTEGER NOT NULL,
  time_added TIMESTAMP WITHOUT TIME ZONE DEFAULT now() NOT NULL,
  time_updated TIMESTAMP WITHOUT TIME ZONE DEFAULT now() NOT NULL,
  album_id BIGINT,
  purchase_sum NUMERIC(16,2) DEFAULT 0 NOT NULL,
  purchase_count INTEGER DEFAULT 0 NOT NULL,
  target_status INTEGER NOT NULL,
  finish_on_target_reach BOOLEAN NOT NULL,
  view_image_id BIGINT,
  view_image_url TEXT,
  video_id BIGINT,
  video_url TEXT,
  video_profile_id BIGINT,
  time_last_purchased NOT NULL TIMESTAMP WITHOUT TIME ZONE DEFAULT now(),
  time_last_news_published TIMESTAMP WITHOUT TIME ZONE DEFAULT NULL,
  affiliate_program_enabled BOOLEAN DEFAULT FALSE NOT NULL,
  can_make_vip_offer BOOLEAN DEFAULT FALSE NOT NULL,
  CONSTRAINT campaigns_pkey PRIMARY KEY(campaign_id)
) WITHOUT OIDS;

COMMENT ON COLUMN commondb.campaigns.campaign_id
IS 'Campaign indentifier';

COMMENT ON COLUMN commondb.campaigns.profile_id
IS 'Campaign profile';

COMMENT ON COLUMN commondb.campaigns.name
IS 'Campaign name';

COMMENT ON COLUMN commondb.campaigns.description
IS 'Campaign description';

COMMENT ON COLUMN commondb.campaigns.description_html
IS 'Campaign description html';

COMMENT ON COLUMN commondb.campaigns.short_description
IS 'Campaign short description';

COMMENT ON COLUMN commondb.campaigns.image_id
IS 'Campaign image';

COMMENT ON COLUMN commondb.campaigns.image_url
IS 'Campaign image url';

COMMENT ON COLUMN commondb.campaigns.time_start
IS 'Campaign start time';

COMMENT ON COLUMN commondb.campaigns.time_finish
IS 'Campaign time finish';

COMMENT ON COLUMN commondb.campaigns.target_amount
IS 'Campaign financial target';

COMMENT ON COLUMN commondb.campaigns.collected_amount
IS 'Contains the project collected amount, and not updated after campaign finished.';

COMMENT ON COLUMN commondb.campaigns.ignore_target_amount
IS 'Is campaign ignore target amount';

COMMENT ON COLUMN commondb.campaigns.notification_method
IS 'Campaign notification method (email, sms, message)';

COMMENT ON COLUMN commondb.campaigns.status
IS 'Campaign status';

COMMENT ON COLUMN commondb.campaigns.time_added
IS 'Time added';

COMMENT ON COLUMN commondb.campaigns.time_updated
IS 'Time updated';

COMMENT ON COLUMN commondb.campaigns.album_id
IS 'Campaign album';

COMMENT ON COLUMN commondb.campaigns.purchase_sum
IS 'Campaign overall purchase sum';

COMMENT ON COLUMN commondb.campaigns.purchase_count
IS 'Campaign overall purchase count';

COMMENT ON COLUMN commondb.campaigns.target_status
IS 'Campaign target status (none, success or fail)';

COMMENT ON COLUMN commondb.campaigns.finish_on_target_reach
IS 'Finish on reach target amount';

COMMENT ON COLUMN commondb.campaigns.view_image_id
IS 'View image id';

COMMENT ON COLUMN commondb.campaigns.view_image_url
IS 'View image url';

COMMENT ON COLUMN commondb.campaigns.video_id
IS 'Video id';

COMMENT ON COLUMN commondb.campaigns.video_url
IS 'Video url';

COMMENT ON COLUMN commondb.campaigns.video_profile_id
IS 'Video owner''s identifier';

COMMENT ON COLUMN commondb.campaigns.time_last_purchased
IS 'time last purchased';

COMMENT ON COLUMN commondb.campaigns.time_last_news_published
IS 'time last news added';

COMMENT ON COLUMN commondb.affiliate_program_enabled
IS 'indicates whether widget program is enabled';

COMMENT ON COLUMN commondb.can_make_vip_offer
IS 'Indicates whether campaign authors make offer in VIP club';


CREATE TABLE commondb.top_special (
  top_special_id BIGINT NOT NULL, 
  owner_profile_id BIGINT NOT NULL, 
  object_type_id INTEGER NOT NULL, 
  top_position INTEGER NOT NULL, 
  top_special_url TEXT NOT NULL, 
  CONSTRAINT top_special_pkey PRIMARY KEY(top_special_id, owner_profile_id, object_type_id)
) WITHOUT OIDS;

COMMENT ON COLUMN commondb.top_special.top_special_id
IS 'top special object identifier';

COMMENT ON COLUMN commondb.top_special.owner_profile_id
IS 'owner profile identifier';

COMMENT ON COLUMN commondb.top_special.object_type_id
IS 'object type identifier';

COMMENT ON COLUMN commondb.top_special.top_position
IS 'object''''s position in top';

COMMENT ON COLUMN commondb.top_special.top_special_url
IS 'top specified url to represent';
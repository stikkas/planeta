CREATE TABLE commondb.addressees (
  addressee_id BIGINT NOT NULL,
  address_id BIGINT NOT NULL,
  name VARCHAR(128),
  surname VARCHAR(128),
  patronymic VARCHAR(128),
  phone TEXT,
  comment VARCHAR(512),
  CONSTRAINT addressees_pkey PRIMARY KEY(addressee_id)
) WITHOUT OIDS;

COMMENT ON COLUMN commondb.addressees.addressee_id
IS 'Addressee identifier';

COMMENT ON COLUMN commondb.addressees.address_id
IS 'Addressee''s address identifier';

COMMENT ON COLUMN commondb.addressees.name
IS 'Addressee''s name';

COMMENT ON COLUMN commondb.addressees.surname
IS 'Addressee''s surname';

COMMENT ON COLUMN commondb.addressees.patronymic
IS 'Addressee''s patronymic';

COMMENT ON COLUMN commondb.addressees.phone
IS 'Addressee''s contact phone number';

COMMENT ON COLUMN commondb.addressees.comment
IS 'Addressee''s comment';

INSERT INTO commondb.addressees (
  addressee_id,
  address_id,
  name,
  surname,
  patronymic,
  comment
)
  SELECT
    nextval('commondb.seq_addressee_id'),
    address_id,
    'shop.planeta.ru',
    'shop.planeta.ru',
    'shop.planeta.ru',
    'shop.planeta.ru'
  FROM commondb.addresses
  WHERE street = 'Нижний Сусальный пер., дом 4, строение 5';

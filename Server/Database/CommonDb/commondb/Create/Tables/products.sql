CREATE TABLE commondb.products (
  product_id BIGINT NOT NULL, 
  profile_id BIGINT NOT NULL, 
  description TEXT, 
  description_html TEXT, 
  image_url VARCHAR(256), 
  image_id BIGINT, 
  contract TEXT, 
  time_added TIMESTAMP WITHOUT TIME ZONE DEFAULT now() NOT NULL, 
  product_type_id INTEGER NOT NULL, 
  amount INTEGER DEFAULT 0 NOT NULL, 
  amount_per_user INTEGER DEFAULT 0 NOT NULL, 
  purchase_count INTEGER DEFAULT 0 NOT NULL, 
  purchase_sum NUMERIC(10,2) DEFAULT 0 NOT NULL, 
  price NUMERIC(10,2), 
  name TEXT, 
  status INTEGER DEFAULT 1 NOT NULL, 
  delivery_type INTEGER DEFAULT 1 NOT NULL, 
  CONSTRAINT products_pkey PRIMARY KEY(product_id)
) WITHOUT OIDS;

COMMENT ON COLUMN commondb.products.product_id
IS 'Product identifier';

COMMENT ON COLUMN commondb.products.profile_id
IS 'Profile identifier';

COMMENT ON COLUMN commondb.products.description
IS 'Product description';

COMMENT ON COLUMN commondb.products.description_html
IS 'Product description html';

COMMENT ON COLUMN commondb.products.image_url
IS 'Preview image url';

COMMENT ON COLUMN commondb.products.image_id
IS 'Preview''s image id';

COMMENT ON COLUMN commondb.products.contract
IS 'Product''s contract text';

COMMENT ON COLUMN commondb.products.time_added
IS 'Time added';

COMMENT ON COLUMN commondb.products.product_type_id
IS 'Product type';

COMMENT ON COLUMN commondb.products.amount
IS 'Product amount';

COMMENT ON COLUMN commondb.products.amount_per_user
IS 'Amount per user';

COMMENT ON COLUMN commondb.products.purchase_count
IS 'Purchases count';

COMMENT ON COLUMN commondb.products.purchase_sum
IS 'Purchase sum';

COMMENT ON COLUMN commondb.products.price
IS 'Product price';

COMMENT ON COLUMN commondb.products.name
IS 'Product name';

COMMENT ON COLUMN commondb.products.status
IS 'Product status';

COMMENT ON COLUMN commondb.products.delivery_type
IS 'Delivery type identifier';

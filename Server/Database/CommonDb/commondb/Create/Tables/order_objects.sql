CREATE TABLE commondb.order_objects (
  order_object_id BIGINT NOT NULL,
  order_id BIGINT NOT NULL,
  object_id BIGINT NOT NULL, 
  merchant_id BIGINT NOT NULL, 
  credit_transaction_id BIGINT DEFAULT 0 NOT NULL, 
  debit_transaction_id BIGINT DEFAULT 0 NOT NULL, 
  cancel_credit_transaction_id BIGINT DEFAULT 0 NOT NULL, 
  cancel_debit_transaction_id BIGINT DEFAULT 0 NOT NULL, 
  order_object_type INTEGER NOT NULL, 
  price NUMERIC(10,2) DEFAULT 0 NOT NULL, 
  product_version INTEGER DEFAULT 1 NOT NULL,
  ticket_purchase_count BIGINT,
  CONSTRAINT order_objects_pkey PRIMARY KEY(order_object_id)
) WITHOUT OIDS;

COMMENT ON COLUMN commondb.order_objects.order_id
IS 'Order object identifier';

COMMENT ON COLUMN commondb.order_objects.order_id
IS 'Order identifier';

COMMENT ON COLUMN commondb.order_objects.object_id
IS 'Ordered object identifier';

COMMENT ON COLUMN commondb.order_objects.merchant_id
IS 'Merchant identifier';

COMMENT ON COLUMN commondb.order_objects.credit_transaction_id
IS 'Credit transaction';

COMMENT ON COLUMN commondb.order_objects.debit_transaction_id
IS 'Debit Transaction';

COMMENT ON COLUMN commondb.order_objects.cancel_credit_transaction_id
IS 'Cancel credit transaction';

COMMENT ON COLUMN commondb.order_objects.cancel_debit_transaction_id
IS 'Cancel debit Transaction';

COMMENT ON COLUMN commondb.order_objects.order_object_type
IS 'Ordered object type';

COMMENT ON COLUMN commondb.order_objects.price
IS 'Price of ordered object';

COMMENT ON COLUMN commondb.order_objects.product_version
IS 'Ordered product version (used only with products)';

COMMENT ON COLUMN commondb.order_objects.ticket_purchase_count
IS 'Ticket purchase count for first(used only with tickets)';
CREATE TABLE commondb.iframe_feed
(
  iframe_feed_id BIGINT PRIMARY KEY NOT NULL,
  iframe_app_type_id INT NOT NULL,
  iframe_feed_type_id INT NOT NULL,
  profile_id BIGINT NOT NULL,
  object_id BIGINT NOT NULL,
  data TEXT NOT NULL,
  html TEXT NOT NULL,
  time_added TIMESTAMP
);
CREATE INDEX iframe_feed_app_type_profile_time_added_idx ON commondb.iframe_feed ( iframe_app_type_id, profile_id, time_added );

﻿CREATE TABLE commondb.addresses (
  address_id BIGINT NOT NULL,
  street     TEXT   NOT NULL,
  city       TEXT   NOT NULL,
  country    TEXT,
  zip_code   TEXT,
  phone      TEXT,
  CONSTRAINT addresses_pkey PRIMARY KEY (address_id)
) WITHOUT OIDS;

COMMENT ON COLUMN commondb.addresses.address_id
IS 'Address identifier';

COMMENT ON COLUMN commondb.addresses.street
IS 'Street and house';

COMMENT ON COLUMN commondb.addresses.city
IS 'City name';

COMMENT ON COLUMN commondb.addresses.country
IS 'Country';

COMMENT ON COLUMN commondb.addresses.zip_code
IS 'Zip code';

COMMENT ON COLUMN commondb.addresses.phone
IS 'Phone number';

INSERT INTO commondb.addresses (
  address_id,
  street,
  city,
  country,
  zip_code
)
  VALUES (
    nextval('commondb.seq_address_id'),
    'Нижний Сусальный пер., дом 4, строение 5',
    'Российская Федерация, г.Москва',
    'Российская Федерация',
    '105064'
  );

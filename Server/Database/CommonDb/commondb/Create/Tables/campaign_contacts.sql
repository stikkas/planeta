CREATE TABLE commondb.campaign_contacts (
  campaign_id BIGINT NOT NULL,
  email TEXT NOT NULL,
  contact_name TEXT,
  contact_profile_id BIGINT
);

COMMENT ON TABLE commondb.campaign_contacts
IS 'Campaign email contacts';
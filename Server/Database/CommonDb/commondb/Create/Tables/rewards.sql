CREATE TABLE commondb.rewards (
  reward_id BIGINT NOT NULL, 
  share_id BIGINT NOT NULL, 
  profile_id BIGINT NOT NULL, 
  name TEXT NOT NULL, 
  description TEXT NOT NULL, 
  image_id BIGINT, 
  image_url TEXT, 
  time_added TIMESTAMP WITHOUT TIME ZONE DEFAULT now() NOT NULL, 
  CONSTRAINT rewards_pkey PRIMARY KEY(reward_id)
) WITHOUT OIDS;

COMMENT ON COLUMN commondb.rewards.reward_id
IS 'Reward identifier';

COMMENT ON COLUMN commondb.rewards.share_id
IS 'Reward share';

COMMENT ON COLUMN commondb.rewards.profile_id
IS 'Reward profile';

COMMENT ON COLUMN commondb.rewards.name
IS 'Reward name';

COMMENT ON COLUMN commondb.rewards.description
IS 'Reward descriptioin';

COMMENT ON COLUMN commondb.rewards.image_id
IS 'Reward image';

COMMENT ON COLUMN commondb.rewards.image_url
IS 'Reward image url';

COMMENT ON COLUMN commondb.rewards.time_added
IS 'Time added';
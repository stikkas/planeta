CREATE TABLE commondb.banner_keywords (
  banner_id BIGINT NOT NULL,
  keywords TEXT NOT NULL,
  CONSTRAINT banners_keyword_pkey PRIMARY KEY(banner_id, keywords)
) WITHOUT OIDS;




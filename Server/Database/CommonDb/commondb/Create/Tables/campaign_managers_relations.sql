CREATE TABLE commondb.campaign_managers_relations (
  manager_id BIGINT NOT NULL,
  category INTEGER NOT NULL,
  managed_object_type INTEGER NOT NULL,
  CONSTRAINT campaign_manager_relations_pkey PRIMARY KEY(manager_id, category, managed_object_type)
);

COMMENT ON TABLE commondb.campaign_managers_relations
IS 'campaign category type distribution by managers';

COMMENT ON COLUMN commondb.campaign_managers_relations.manager_id
IS 'manager id';

COMMENT ON COLUMN commondb.campaign_managers_relations.category
IS 'category type code';

COMMENT ON COLUMN commondb.campaign_managers_relations.managed_object_type
IS 'managed object type';
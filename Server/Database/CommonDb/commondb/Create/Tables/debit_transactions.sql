CREATE TABLE commondb.debit_transactions (
  transaction_id BIGINT NOT NULL, 
  profile_id BIGINT NOT NULL, 
	payment_system_id INTEGER NOT NULL,
	external_system_data TEXT, 
  amount_net NUMERIC(10,2) NOT NULL, 
  amount_fee NUMERIC(10,2) NOT NULL, 
  comment TEXT, 
  time_added TIMESTAMP WITHOUT TIME ZONE DEFAULT now() NOT NULL, 
  CONSTRAINT debit_transactions_pkey PRIMARY KEY(transaction_id)
) WITHOUT OIDS;

COMMENT ON COLUMN commondb.debit_transactions.transaction_id
IS 'Transaction identifier';

COMMENT ON COLUMN commondb.debit_transactions.profile_id
IS 'Profile identifier';

COMMENT ON COLUMN commondb.debit_transactions.payment_system_id
IS 'Payment system identifier';

COMMENT ON COLUMN commondb.debit_transactions.external_system_data
IS 'Payment system specific data';

COMMENT ON COLUMN commondb.debit_transactions.amount_net
IS 'Amount net';

COMMENT ON COLUMN commondb.debit_transactions.amount_fee
IS 'Amount fee';

COMMENT ON COLUMN commondb.debit_transactions.comment
IS 'Comment';

COMMENT ON COLUMN commondb.debit_transactions.time_added
IS 'Time added';

CREATE TABLE commondb.campaign_managers (
  manager_id BIGINT NOT NULL,
  name TEXT NOT NULL,
  full_name TEXT NOT NULL,
  email TEXT NOT NULL,
  PRIMARY KEY(manager_id)
) ;

COMMENT ON TABLE commondb.campaign_managers
IS 'campaign categories managers';

COMMENT ON COLUMN commondb.campaign_managers.name
IS 'short name for presentation';

CREATE TABLE commondb.delivery_address (
  delivery_address_id BIGINT PRIMARY KEY NOT NULL,
  order_id            BIGINT             NOT NULL,
  address             TEXT               NOT NULL,
  city                TEXT DEFAULT ''    NOT NULL,
  country             TEXT DEFAULT ''    NOT NULL,
  zip_code            TEXT DEFAULT ''    NOT NULL,
  phone               TEXT               NOT NULL,
  fio                 TEXT               NOT NULL
);
﻿CREATE TABLE commondb.news (
  news_id SERIAL,
  time_added TIMESTAMP WITHOUT TIME ZONE DEFAULT now() NOT NULL,
  title VARCHAR(256) NOT NULL,
  text_html TEXT NOT NULL,
  link VARCHAR(256) NOT NULL,
  CONSTRAINT news_pkey PRIMARY KEY(news_id)
) WITHOUT OIDS;

COMMENT ON COLUMN commondb.news.news_id
IS 'news identifier';

COMMENT ON COLUMN commondb.news.time_added
IS 'Added time';

COMMENT ON COLUMN commondb.news.title
IS 'News title';

COMMENT ON COLUMN commondb.news.text_html
IS 'Transformed bb-code';

COMMENT ON COLUMN commondb.news.link
IS 'News link';

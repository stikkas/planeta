CREATE TABLE commondb.payment_card_bindings (
  binding_id               BIGINT                                    NOT NULL,
  user_id                  BIGINT                                    NOT NULL,
  card_number_mask         VARCHAR(20)                               NOT NULL,
  card_cvv                 VARCHAR(3),
  payment_system_id        INTEGER                                   NOT NULL,
  payment_provider_id      BIGINT                                    NOT NULL,
  payment_provider_account TEXT,
  access_token             TEXT                                      NOT NULL,
  active                   BOOLEAN                                   NOT NULL DEFAULT TRUE,
  time_added               TIMESTAMP WITHOUT TIME ZONE DEFAULT now() NOT NULL,
  CONSTRAINT user_card_bindings_pkey PRIMARY KEY (binding_id)
) WITHOUT OIDS;

COMMENT ON COLUMN commondb.payment_card_bindings.binding_id
IS 'Unique binding identifier.';

COMMENT ON COLUMN commondb.payment_card_bindings.user_id
IS 'User profile identifier.';

COMMENT ON COLUMN commondb.payment_card_bindings.card_number_mask
IS 'Card number mask string.';

COMMENT ON COLUMN commondb.payment_card_bindings.card_cvv
IS 'Card cvv code.';

COMMENT ON COLUMN commondb.payment_card_bindings.payment_system_id
IS 'Payment system identifier.';

COMMENT ON COLUMN commondb.payment_card_bindings.payment_provider_id
IS 'Payment system provider identifier.';

COMMENT ON COLUMN commondb.payment_card_bindings.payment_provider_account
IS 'Payment system account identifier.';

COMMENT ON COLUMN commondb.payment_card_bindings.access_token
IS 'Specific for payment system card access token.';

COMMENT ON COLUMN commondb.payment_card_bindings.active
IS 'Enable for choice flag.';

COMMENT ON COLUMN commondb.payment_card_bindings.time_added
IS 'Binding creation time.';

CREATE TABLE commondb.product_photos (
  product_id BIGINT NOT NULL, 
  profile_id BIGINT NOT NULL, 
  image_id BIGINT NOT NULL, 
  image_profile_id BIGINT NOT NULL,
  image_url VARCHAR(256), 
  CONSTRAINT product_photos_pkey PRIMARY KEY(product_id, image_id)
) WITHOUT OIDS;

COMMENT ON COLUMN commondb.product_photos.product_id
IS 'Product identifier';

COMMENT ON COLUMN commondb.product_photos.profile_id
IS 'Profile identifier';

COMMENT ON COLUMN commondb.product_photos.image_id
IS 'Image identifier';

COMMENT ON COLUMN commondb.product_photos.image_profile_id
IS 'Image profile identifier';

COMMENT ON COLUMN commondb.product_photos.image_url
IS 'Image url';

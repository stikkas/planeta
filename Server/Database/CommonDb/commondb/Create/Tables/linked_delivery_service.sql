CREATE TABLE commondb.linked_delivery_service (
  share_id   BIGINT DEFAULT 0         NOT NULL,
  service_id BIGINT DEFAULT 0         NOT NULL,
  price      NUMERIC(11, 2) DEFAULT 0 NOT NULL,
  note       TEXT,
  is_enabled BOOLEAN DEFAULT TRUE,
  custom_address_id BIGINT,
  CONSTRAINT delivery_service_id_key UNIQUE (share_id, service_id)
);

COMMENT ON COLUMN commondb.linked_delivery_service.custom_address_id
IS 'if null and service type is customer_pickup use base_delivery_service.address_id';
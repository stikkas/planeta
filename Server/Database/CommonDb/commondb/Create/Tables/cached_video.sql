﻿CREATE TABLE commondb.cached_video (
  cached_video_id BIGINT NOT NULL,
  url VARCHAR(255) NOT NULL,
  title TEXT,
  description TEXT,
  html TEXT,
  duration BIGINT NOT NULL,
  width INTEGER,
  height INTEGER,
  thumbnail_url VARCHAR(255) NOT NULL,
  time_added TIMESTAMP WITHOUT TIME ZONE DEFAULT now(),
  video_type SMALLINT NOT NULL,
  external_id VARCHAR(100),
  CONSTRAINT "сached_video_pkey" PRIMARY KEY(cached_video_id)
) WITHOUT OIDS;

CREATE UNIQUE INDEX cached_video_url_idx ON commondb.cached_video
  USING btree (url);

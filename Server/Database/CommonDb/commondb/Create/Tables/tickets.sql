CREATE TABLE commondb.tickets (
  ticket_id BIGINT NOT NULL,
  profile_id BIGINT NOT NULL,
  name TEXT NOT NULL,
  ticket_category INTEGER NOT NULL,
  time_added TIMESTAMP WITHOUT TIME ZONE DEFAULT now() NOT NULL,
  time_updated TIMESTAMP WITHOUT TIME ZONE DEFAULT now() NOT NULL,
  price NUMERIC(10,2) NOT NULL,
  amount INTEGER DEFAULT 0 NOT NULL,
  purchase_count INTEGER DEFAULT 0 NOT NULL,
  in_process_count INTEGER DEFAULT 0 NOT NULL,
  CONSTRAINT tickets_pkey PRIMARY KEY(ticket_id)
) WITHOUT OIDS;

COMMENT ON COLUMN commondb.tickets.ticket_id
IS 'Ticket indentifier';

COMMENT ON COLUMN commondb.tickets.profile_id
IS 'Ticket profile';

COMMENT ON COLUMN commondb.tickets.name
IS 'Ticket name';

COMMENT ON COLUMN commondb.tickets.ticket_category
IS 'Ticket category';

COMMENT ON COLUMN commondb.tickets.time_added
IS 'Time added';

COMMENT ON COLUMN commondb.tickets.time_updated
IS 'Time updated';

COMMENT ON COLUMN commondb.tickets.price
IS 'Price of the ticket';

COMMENT ON COLUMN commondb.tickets.amount
IS 'Amount of tickets of this type';

COMMENT ON COLUMN commondb.tickets.purchase_count
IS 'Tickets purchase count of this type';

COMMENT ON COLUMN commondb.tickets.in_process_count
IS 'Tickets are in process now (cannot be processed by others)';
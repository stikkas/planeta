CREATE TABLE commondb.address_relations (
  address_id BIGINT NOT NULL,
  addressee_id BIGINT NOT NULL,
  addressee_type_code SMALLINT NOT NULL,
  last_used BOOLEAN DEFAULT FALSE,
  time_added TIMESTAMP WITHOUT TIME ZONE DEFAULT NOW()
);

COMMENT ON COLUMN commondb.address_relations.address_id
IS 'Address identifier';

COMMENT ON COLUMN commondb.address_relations.addressee_id
IS 'Addressee identifier';

COMMENT ON COLUMN commondb.address_relations.addressee_type_code
IS 'Type of addressee (profile, shop, ...)';

COMMENT ON COLUMN commondb.address_relations.last_used
IS 'Flag that indicates whether this address was last used';

COMMENT ON COLUMN commondb.address_relations.time_added
IS 'Relationship time added, needed for list-of-addresses sort order';
CREATE TABLE commondb.campaign_tag_relations (
  campaign_id BIGINT NOT NULL,
  tag_id BIGINT NOT NULL,
  order_num INTEGER DEFAULT 0 NOT NULL,
  CONSTRAINT campaign_tag_relations_pkey PRIMARY KEY(campaign_id, tag_id),
  CONSTRAINT campaign_ord_num_unique UNIQUE(campaign_id, order_num)
)
WITH (oids = false);

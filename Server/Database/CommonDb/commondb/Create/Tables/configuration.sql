CREATE TABLE commondb.configuration (
  key TEXT NOT NULL,
  int_value INTEGER,
  str_value TEXT,
  boolean_value BOOLEAN,
  CONSTRAINT configuration_key_key UNIQUE(key)
) WITHOUT OIDS;

COMMENT ON TABLE commondb.configuration
IS 'Configuration properties';

COMMENT ON COLUMN commondb.configuration.key
IS 'Property key';

COMMENT ON COLUMN commondb.configuration.int_value
IS 'Property integer value';

COMMENT ON COLUMN commondb.configuration.str_value
IS 'Property string value';

COMMENT ON COLUMN commondb.configuration.boolean_value
IS 'Property boolean value';

INSERT INTO
  commondb.configuration
(
  key,
  int_value,
  str_value,
  boolean_value
)
VALUES (
  'planeta.shop.hot.deals',
  null,
  '<div class="hot-slider-wrap">
<ul class="hot-slider">
<li class="hot-slider-item">
<a href="#">
<img src="http://static.dev.planeta.ru/images/content/hot-slider-1.jpg">
</a>
</li>
<li class="hot-slider-item">
<a href="#">
<img src="http://static.dev.planeta.ru/images/content/hot-slider-2.jpg">
</a>
</li>
</ul>
<div class="hot-slider-prev">
<b class="arrow"></b>
</div>
<div class="hot-slider-next">
<b class="arrow"></b>
</div>
</div>',
  null
);

INSERT INTO
  commondb.configuration
(
  key,
  int_value,
  str_value,
  boolean_value
)
VALUES (
  'planeta.top.hot.deals',
  null,
  '<div class="hot-slider-wrap tops-slider"> <ul class="hot-slider"> <li class="hot-slider-item"> <a href="http://start.planeta.ru/campaigns/43"> <img src="http://s1.planeta.ru/i/7b9c/original.jpg"> </a> </li> <li class="hot-slider-item"> <a href="http://start.planeta.ru/campaigns/26"> <img src="http://s1.planeta.ru/i/7b9b/original.jpg"> </a> </li> <li class="hot-slider-item"> <a href="http://start.planeta.ru/campaigns/45"> <img src="http://s1.planeta.ru/i/7b9a/original.jpg"> </a> </li> </ul> <div class="hot-slider-prev"> <b class="arrow"></b> </div> 
  <div class="hot-slider-next"> <b class="arrow"></b> </div> 
  </div>',
  null
  );
  

INSERT INTO commondb.configuration (key, int_value)
VALUES ('planeta.min.profile.rating', -5);

INSERT INTO commondb.configuration (key, int_value)
VALUES('planeta.low.rating.profile.allowed.comments.count', 2);
/*
INSERT INTO commondb.configuration (key, str_value)
	VALUES ('payment.types.properties', '
	  BEELINE;Билайн;/images/balance/beeline.png;true,
		MEGAFON;Мегафон;/images/balance/megafon.png;true,
		MTS;МТС;/images/balance/mts.png;true,
		CARD;Оплата картой;/images/balance/visa.png;false,
		YANDEX_MONEY;Яндекс.Деньги;/images/balance/yandex.png;false,
		QIWI;QIWI кошелек;/images/balance/qiwi.png;true,
		WEB_MONEY;WebMoney;/images/balance/wm.png;false,
		OTHER;Другие способы;/images/balance/other.png;false,
		SMS;SMS-оплата;/images/balance/sms.png;false');

INSERT INTO commondb.configuration (key, str_value)
  VALUES ('main.available.payment.types', '
	  BEELINE;      QIWI,
		MEGAFON;      QIWI,
		MTS;          QIWI,
		CARD;         ALFABANK,
		YANDEX_MONEY; YANDEX_MONEY,
		QIWI;         QIWI,
		WEB_MONEY;    DENGIONLINE,
		OTHER;        ROBOKASSA');

INSERT INTO commondb.configuration (key, str_value)
  VALUES ('shop.available.payment.types', '
		CARD;         ALFABANK,
		YANDEX_MONEY; YANDEX_MONEY,
		QIWI;         CHRONOPAY,
		WEB_MONEY;    CHRONOPAY');

INSERT INTO commondb.configuration (key, str_value)
  VALUES ('mobile.available.payment.types', '
    BEELINE;      QIWI,
		MEGAFON;      QIWI,
		MTS;          QIWI,
		CARD;         ALFABANK,
		QIWI;         QIWI,
		OTHER;        W1');

INSERT INTO commondb.configuration (key, str_value)
  VALUES ('concert.new.available.payment.types', '
    BEELINE;      QIWI,
		MEGAFON;      QIWI,
		MTS;          QIWI,
		CARD;         ALFABANK,
		YANDEX_MONEY; YANDEX_MONEY,
		WEB_MONEY;    DENGIONLINE');

INSERT INTO commondb.configuration (key, str_value)
VALUES ('robokassa.payment.tools.properties', '
		SMS;          OtherMegafonR;        5;  0;  0,
		CARD;         BANKOCEAN2R;          5;  0;  0,
		YANDEX_MONEY; YandexMerchantOceanR; 5;  0;  0,
		QIWI;         QiwiR;                5;  0;  0,
		WEB_MONEY;    WMRM;                 5;  0;  0,
		OTHER;        Other;                5;  0;  0');

INSERT INTO commondb.configuration (key, str_value)
VALUES ('dengionline.payment.tools.properties', '
		MEGAFON;      39;   4;  0;  0,
		MTS;          104;  4;  0;  0,
		CARD;         60;   4;  0;  0,
		QIWI;         14;   4;  0;  0,
		WEB_MONEY;    2;    4;  0;  0');

INSERT INTO commondb.configuration (key, str_value)
VALUES ('alfabank.payment.tools.properties', '
		CARD;         alfabank;   3.5;  0;  0');

INSERT INTO commondb.configuration (key, str_value)
VALUES ('qiwi.payment.tools.properties', '
		QIWI;         qiwi;   3.5;  0;  0,
		MEGAFON;      1801;   3.5;  0;  0,
		MTS;          1800;   3.5;  0;  0,
		BEELINE;      1004;   3.5;  0;  0');

INSERT INTO commondb.configuration (key, str_value)
VALUES ('w1.payment.tools.properties', '
		OTHER; w1; 3.5;  0;  0');

INSERT INTO commondb.configuration (key, str_value)
VALUES ('yandex.money.payment.tools.properties', '
		YANDEX_MONEY; PC;   3;    100000;  0,
		CARD;         AC;   3;    100000;  200000,
		WEB_MONEY;    WM;   3;    15000;   150000,
		MEGAFON;      MC;   3;    0;       0,
		MTS;          MC;   3;    0;       0,
		BEELINE;      MC;   3;    0;       0');

INSERT INTO commondb.configuration (key, str_value)
VALUES ('chronopay.payment.tools.properties', '
		CARD;         1;  0;    0;  0,
		YANDEX_MONEY; 16; 5.18; 0;  0,
		QIWI;         21; 3.8;  0;  0,
		WEB_MONEY;    15; 3.5;  0;  0');
*/
INSERT INTO commondb.configuration (key, int_value, description)
	VALUES ('planeta.low.rating.profile.allowed.comments.count', 2, 
	'колличество комментариев, которые может оставить пользователь с низким рейтингом');

INSERT INTO commondb.configuration (key, int_value, description)
	VALUES ('planeta.start.hot.campaign', 0, 'на витрину будет выводится проект №');

INSERT INTO commondb.configuration (key, str_value)
VALUES ('user.callback.availability.condition', '{
  "hourFrom" : 12,
  "hourTo" : 19,
  "minAmount" : 5000
}');

INSERT INTO commondb.configuration (key, int_value, description)
  VALUES ('news.notification.days.limit', 7, 'ограничение частоты рассылки новостей (в днях)');
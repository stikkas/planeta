CREATE TABLE commondb.user_credentials (
  profile_id BIGINT NOT NULL,
  username TEXT NOT NULL,
  encrypted_secret TEXT NOT NULL,
  credential_type_code INT NOT NULL,
  credential_status_code INT NOT NULL DEFAULT 0,
  CONSTRAINT user_credentials_pk PRIMARY KEY (username, credential_type_code)
);

COMMENT ON COLUMN commondb.user_credentials.profile_id
IS 'Id of profile who can authorize via record credentials';
COMMENT ON COLUMN commondb.user_credentials.username
IS 'Login or username or some external id';
COMMENT ON COLUMN commondb.user_credentials.encrypted_secret
IS 'Encrypted password or some secret key, credential_type-dependent, can be empty string';
COMMENT ON COLUMN commondb.user_credentials.credential_type_code
IS 'Type of authorization: Email, Vkontakte, Facebook, Twitter etc.';
COMMENT ON COLUMN commondb.user_credentials.credential_status_code
IS 'Is credentials confirmed/deleted';
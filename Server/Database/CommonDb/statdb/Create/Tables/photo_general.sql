CREATE TABLE statdb.photo_general (
  profile_id BIGINT NOT NULL, 
  report_date DATE NOT NULL, 
  hour INTEGER NOT NULL, 
  photo_id BIGINT NOT NULL, 
  views_count INTEGER NOT NULL, 
  CONSTRAINT photo_general_pkey PRIMARY KEY(profile_id, report_date, hour, photo_id)
) WITHOUT OIDS;

COMMENT ON COLUMN statdb.photo_general.profile_id
IS 'Profile identifier';

COMMENT ON COLUMN statdb.photo_general.report_date
IS 'Report date';

COMMENT ON COLUMN statdb.photo_general.hour
IS 'Hour(0-23)';

COMMENT ON COLUMN statdb.photo_general.photo_id
IS 'Photo identifier';

COMMENT ON COLUMN statdb.photo_general.views_count
IS 'Views count';

CREATE TABLE statdb.photo_albums_general (
  profile_id BIGINT NOT NULL, 
  report_date DATE NOT NULL, 
  hour INTEGER NOT NULL, 
  album_id INTEGER NOT NULL, 
  views_count INTEGER NOT NULL, 
  CONSTRAINT photo_album_general_pkey PRIMARY KEY(profile_id, report_date, hour, album_id)
) WITHOUT OIDS;

COMMENT ON COLUMN statdb.photo_albums_general.profile_id
IS 'Profile identifier';

COMMENT ON COLUMN statdb.photo_albums_general.report_date
IS 'Report date';

COMMENT ON COLUMN statdb.photo_albums_general.hour
IS 'Hour(0-23)';

COMMENT ON COLUMN statdb.photo_albums_general.album_id
IS 'Album identifier';

COMMENT ON COLUMN statdb.photo_albums_general.views_count
IS 'Views count';

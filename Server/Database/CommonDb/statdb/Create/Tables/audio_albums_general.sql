CREATE TABLE statdb.audio_albums_general (
  profile_id BIGINT NOT NULL, 
  report_date DATE NOT NULL, 
  hour INTEGER NOT NULL, 
  album_id INTEGER NOT NULL, 
  listenings_count INTEGER DEFAULT 0 NOT NULL, 
  downloads_count INTEGER DEFAULT 0 NOT NULL, 
  CONSTRAINT group_audio_albums_general_pkey PRIMARY KEY(profile_id, report_date, hour, album_id)
) WITHOUT OIDS;

COMMENT ON TABLE statdb.audio_albums_general
IS 'General statistics on group''s audio albums';

COMMENT ON COLUMN statdb.audio_albums_general.profile_id
IS 'Group identifier';

COMMENT ON COLUMN statdb.audio_albums_general.report_date
IS 'Report date';

COMMENT ON COLUMN statdb.audio_albums_general.hour
IS 'Hour(0-23)';

COMMENT ON COLUMN statdb.audio_albums_general.album_id
IS 'Album identifier';

COMMENT ON COLUMN statdb.audio_albums_general.listenings_count
IS 'Listenings count';

COMMENT ON COLUMN statdb.audio_albums_general.downloads_count
IS 'Downloads count';

CREATE INDEX audio_album_general_idx ON statdb.audio_albums_general
USING btree ("report_date" DESC, "hour" DESC, "listenings_count" DESC);
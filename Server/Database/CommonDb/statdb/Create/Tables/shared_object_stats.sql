CREATE TABLE statdb.shared_object_stats (
  shared_object_id BIGINT NOT NULL, 
  url VARCHAR(256) NOT NULL, 
  total_shares INTEGER DEFAULT 0 NOT NULL, 
  time_added TIMESTAMP WITHOUT TIME ZONE DEFAULT now() NOT NULL, 
  time_updated TIMESTAMP WITHOUT TIME ZONE DEFAULT now() NOT NULL, 
  CONSTRAINT shared_url_stats_pkey PRIMARY KEY(shared_object_id), 
  CONSTRAINT shared_url_unique UNIQUE(url)
) WITHOUT OIDS;

COMMENT ON COLUMN statdb.shared_object_stats.shared_object_id
IS 'Shared object identifier';

COMMENT ON COLUMN statdb.shared_object_stats.url
IS 'Unique url of shared object';

COMMENT ON COLUMN statdb.shared_object_stats.total_shares
IS 'Total shares count';

COMMENT ON COLUMN statdb.shared_object_stats.time_added
IS 'Time, this record was added';

COMMENT ON COLUMN statdb.shared_object_stats.time_updated
IS 'Last time, this record was updated';
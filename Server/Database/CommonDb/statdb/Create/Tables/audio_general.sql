CREATE TABLE statdb.audio_general (
  profile_id BIGINT NOT NULL, 
  report_date DATE NOT NULL, 
  hour INTEGER NOT NULL, 
  track_id BIGINT NOT NULL, 
  listenings_count INTEGER DEFAULT 0 NOT NULL, 
  downloads_count INTEGER DEFAULT 0 NOT NULL, 
  unique_listeners INTEGER DEFAULT 0 NOT NULL, 
  unique_downloaders INTEGER DEFAULT 0 NOT NULL, 
  CONSTRAINT group_audio_general_pkey PRIMARY KEY(profile_id, report_date, hour, track_id)
) WITHOUT OIDS;

COMMENT ON TABLE statdb.audio_general
IS 'Group''s audio general statistics';

COMMENT ON COLUMN statdb.audio_general.profile_id
IS 'Group identifier';

COMMENT ON COLUMN statdb.audio_general.report_date
IS 'Report date';

COMMENT ON COLUMN statdb.audio_general.hour
IS 'Hour (0-23)';

COMMENT ON COLUMN statdb.audio_general.track_id
IS 'Track identifier';

COMMENT ON COLUMN statdb.audio_general.listenings_count
IS 'Listenings count';

COMMENT ON COLUMN statdb.audio_general.downloads_count
IS 'Downloads count';

CREATE INDEX audio_general_idx ON statdb.audio_general
USING btree ("report_date" DESC, "hour" DESC, "listenings_count" DESC);

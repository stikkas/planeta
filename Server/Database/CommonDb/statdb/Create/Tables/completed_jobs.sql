CREATE TABLE statdb.completed_jobs (
  job_name VARCHAR(128),
  report_date DATE NOT NULL,
  hour INTEGER NOT NULL,
  comment TEXT,
  CONSTRAINT completed_jobs_general_pkey PRIMARY KEY(job_name, report_date, hour)
) WITHOUT OIDS;


INSERT INTO statdb.completed_jobs(
	job_name,
    report_date,
    hout
) VALUES (
	"get_votes_stats",
	CURRENT_DATE - 10000,
	0
)

CREATE TABLE statdb.shared_object_detail_stats (
  shared_object_id BIGINT NOT NULL,
  author_profile_id BIGINT DEFAULT -1 NOT NULL, 
  author_uuid VARCHAR(100) DEFAULT NULL, 
  share_service_type INTEGER DEFAULT 0 NOT NULL, 
  time_added TIMESTAMP WITHOUT TIME ZONE DEFAULT now() NOT NULL
) WITHOUT OIDS;

COMMENT ON COLUMN statdb.shared_object_detail_stats.shared_object_id
IS 'Shared object identifier';

COMMENT ON COLUMN statdb.shared_object_detail_stats.author_profile_id
IS 'Profile identifier of the author, who shared object';

COMMENT ON COLUMN statdb.shared_object_detail_stats.author_uuid
IS 'Profile uuid';

COMMENT ON COLUMN statdb.shared_object_detail_stats.share_service_type
IS 'Share service type';

COMMENT ON COLUMN statdb.shared_object_detail_stats.time_added
IS 'Time, this record was added';

CREATE INDEX shared_object_detail_stats_idx 
	ON statdb.shared_object_detail_stats (shared_object_id);
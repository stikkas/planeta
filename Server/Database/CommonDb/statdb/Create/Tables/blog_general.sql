CREATE TABLE statdb.blog_general (
  profile_id BIGINT NOT NULL, 
  report_date DATE NOT NULL, 
  hour INTEGER NOT NULL, 
  blog_post_id INTEGER NOT NULL, 
  views_count INTEGER NOT NULL, 
  comments_count INTEGER NOT NULL,
  rating INTEGER DEFAULT 0 NOT NULL, 
  votes_count INTEGER DEFAULT 0 NOT NULL, 
  unique_voters_count INTEGER DEFAULT 0 NOT NULL, 
  unique_commentators INTEGER DEFAULT 0, 
  CONSTRAINT blog_general_pkey PRIMARY KEY(profile_id, report_date, hour, blog_post_id)
) WITHOUT OIDS;

COMMENT ON COLUMN statdb.blog_general.profile_id
IS 'Profile identifier';

COMMENT ON COLUMN statdb.blog_general.report_date
IS 'Report date';

COMMENT ON COLUMN statdb.blog_general.hour
IS 'Hour(0-23)';

COMMENT ON COLUMN statdb.blog_general.blog_post_id
IS 'Blog identifier';

COMMENT ON COLUMN statdb.blog_general.views_count
IS 'Views count';

CREATE INDEX blog_general_idx ON statdb.blog_general
USING btree ("report_date" DESC, "views_count" DESC, "hour" DESC);
CREATE TABLE statdb.db_log
(
  id             BIGINT NOT NULL,
  message        TEXT   NOT NULL,
  logger_type_id INT    NOT NULL DEFAULT 0,
  object_id      BIGINT NOT NULL DEFAULT 0,
  subject_id     BIGINT NOT NULL DEFAULT 0,
  request_id     BIGINT NOT NULL DEFAULT 0,
  response_id    BIGINT NOT NULL DEFAULT 0,
  time_added     BIGINT NOT NULL,
  CONSTRAINT db_log_pk PRIMARY KEY (id)
)
WITH (
OIDS = FALSE
);
ALTER TABLE statdb.db_log
OWNER TO planeta;

COMMENT ON COLUMN statdb.db_log.id
IS 'Unique identifier';

COMMENT ON COLUMN statdb.db_log.message
IS 'Logger message';

COMMENT ON COLUMN statdb.db_log.logger_type_id
IS 'Type of logger (PAYMENT(1), ORDER(2))';

COMMENT ON COLUMN statdb.db_log.object_id
IS 'Loggable object identifier';

COMMENT ON COLUMN statdb.db_log.subject_id
IS 'Subject identifier, optionally uses with different logger types';

COMMENT ON COLUMN statdb.db_log.request_id
IS 'Request identifier may be 0';

COMMENT ON COLUMN statdb.db_log.response_id
IS 'Response identifier may be 0';

COMMENT ON COLUMN statdb.db_log.time_added
IS 'Time of record inserting';
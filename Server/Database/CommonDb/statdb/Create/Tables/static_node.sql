-- Table: statdb.static_node

-- DROP TABLE statdb.static_node;

CREATE TABLE statdb.static_node
(
  domain character varying NOT NULL,
  free_space_kb bigint NOT NULL,
  time_updated timestamp without time zone NOT NULL DEFAULT now(),
  CONSTRAINT static_node_pk PRIMARY KEY (domain )
)
WITH (
  OIDS=FALSE
);
ALTER TABLE statdb.static_node
  OWNER TO planeta;
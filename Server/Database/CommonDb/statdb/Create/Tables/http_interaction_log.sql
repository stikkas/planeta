CREATE TABLE statdb.http_interaction_log
(
  id         BIGINT  NOT NULL,
  meta_data  TEXT    NOT NULL,
  request_id BIGINT  NOT NULL DEFAULT 0,
  is_request BOOLEAN NOT NULL DEFAULT TRUE,
  time_added BIGINT  NOT NULL,
  CONSTRAINT http_interaction_log_pk PRIMARY KEY (id)
)
WITH (
OIDS = FALSE
);

ALTER TABLE statdb.http_interaction_log
OWNER TO planeta;

COMMENT ON COLUMN statdb.http_interaction_log.id
IS 'Unique identifier';

COMMENT ON COLUMN statdb.http_interaction_log.meta_data
IS 'Meta data of request or response (Headers, Cookies etc.)';

COMMENT ON COLUMN statdb.http_interaction_log.request_id
IS 'Requests identifier if presented record is response (for all requests equals 0)';

COMMENT ON COLUMN statdb.http_interaction_log.is_request
IS 'Request flag (for all requests is true)';

COMMENT ON COLUMN statdb.http_interaction_log.time_added
IS 'Time of record inserting';
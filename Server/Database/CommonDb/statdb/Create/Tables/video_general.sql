CREATE TABLE statdb.video_general (
  profile_id BIGINT NOT NULL, 
  report_date DATE NOT NULL, 
  hour INTEGER NOT NULL, 
  video_id BIGINT NOT NULL, 
  views_count INTEGER DEFAULT 0 NOT NULL, 
  viewers_count INTEGER DEFAULT 0 NOT NULL, 
  CONSTRAINT group_video_general_pkey PRIMARY KEY(profile_id, report_date, hour, video_id)
) WITHOUT OIDS;

COMMENT ON TABLE statdb.video_general
IS 'Contains group''s video general statistics';

COMMENT ON COLUMN statdb.video_general.profile_id
IS 'Group identifier';

COMMENT ON COLUMN statdb.video_general.report_date
IS 'Report date';

COMMENT ON COLUMN statdb.video_general.hour
IS 'Hour (0-23)';

COMMENT ON COLUMN statdb.video_general.video_id
IS 'Video identifier';

COMMENT ON COLUMN statdb.video_general.views_count
IS 'Views count';

COMMENT ON COLUMN statdb.video_general.viewers_count
IS 'Viewers count';

CREATE INDEX video_general_idx ON statdb.video_general
USING btree ("report_date" DESC, "hour" DESC, "views_count" DESC);

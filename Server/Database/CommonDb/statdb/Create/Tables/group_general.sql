CREATE TABLE statdb.group_general (
  profile_id BIGINT NOT NULL, 
  category_id INTEGER NOT NULL, 
  report_date DATE NOT NULL, 
  hour INTEGER NOT NULL, 
  views_count INTEGER NOT NULL, 
  joined_count INTEGER DEFAULT 0 NOT NULL, 
  refused_count INTEGER DEFAULT 0 NOT NULL, 
  unique_user_count INTEGER DEFAULT 0 NOT NULL, 
  unique_members INTEGER DEFAULT 0, 
  CONSTRAINT group_general_pkey PRIMARY KEY(profile_id, category_id, report_date, hour)
) WITHOUT OIDS;

COMMENT ON COLUMN statdb.group_general.profile_id
IS 'Profile identifier';

COMMENT ON COLUMN statdb.group_general.report_date
IS 'Report date';

COMMENT ON COLUMN statdb.group_general.hour
IS 'Hour(0-23)';

COMMENT ON COLUMN statdb.group_general.views_count
IS 'Views count';

CREATE INDEX group_general_idx ON statdb.group_general
USING btree ("report_date" DESC, "hour" DESC, "views_count" DESC);
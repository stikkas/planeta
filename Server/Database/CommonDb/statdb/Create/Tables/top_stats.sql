CREATE TABLE statdb.top_stats (
  profile_id BIGINT NOT NULL,
  object_id BIGINT NOT NULL,
  object_type_id BIGINT NOT NULL,
  position INTEGER NOT NULL,
  CONSTRAINT top_stats_general_pkey PRIMARY KEY(profile_id, object_id, object_type_id)
) WITHOUT OIDS;

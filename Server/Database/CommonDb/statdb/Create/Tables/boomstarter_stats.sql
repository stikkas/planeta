CREATE TABLE statdb.boomstarter_stats (
  report_date TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, 
  project_url TEXT NOT NULL, 
  project_name TEXT NOT NULL, 
  project_category VARCHAR(255) NOT NULL, 
  project_backers INTEGER NOT NULL, 
  project_pledgesum INTEGER NOT NULL, 
  project_targetsum INTEGER NOT NULL, 
  project_daysleft VARCHAR(255) NOT NULL, 
  project_average REAL NOT NULL, 
  CONSTRAINT boomstarter_stats_pkey PRIMARY KEY(report_date, project_url)
) 
WITH (oids = false);
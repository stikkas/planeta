CREATE OR REPLACE FUNCTION statdb.get_comments_stats(IN p_date TIMESTAMP,
  out comment_count integer,
  out commentators integer,
  out owner_profile_id bigint,
  out object_id bigint,
  out time_added timestamp)
RETURNS SETOF record AS
$$
    CLUSTER 'planeta';
    RUN ON ALL;
	SELECT COUNT(*) as comment_count,
	       COUNT(distinct author_profile_id) as commentators,
		   owner_profile_id,
		   object_id,
		   date_trunc('hour', time_added) as time_added
	  FROM profiledb.comments
     WHERE object_type_id = 4
       AND NOT is_deleted
       AND time_added > p_date
  GROUP BY 3,4,5;
$$ LANGUAGE 'plproxy';

SELECT * FROM statdb.get_comments_stats(CURRENT_DATE - 10);

﻿CREATE OR REPLACE FUNCTION statdb.get_blogs_report (
  out title varchar,
  out tags varchar,
  out group_name varchar,
  out genre text,
  out group_category_id integer,
  out time_added timestamp,
  out profile_id bigint,
  out blog_post_id bigint,
  out alias varchar
)
RETURNS SETOF record AS
$body$
CLUSTER 'planeta';
RUN ON ALL;

SELECT profiles.profile_id, alias, blog_post_id, title, tags, profiles.name as group_name, genre, group_category_id, 
blog_posts.time_added
FROM profiledb.blog_posts
JOIN profiledb.profiles ON blog_posts.owner_profile_id = profiles.profile_id
JOIN profiledb.groups ON profiles.profile_id = groups.profile_id
WHERE profiles.profile_type_id = 2 AND profiles.status IN (2, 5, 6);
$body$
LANGUAGE 'plproxy';
﻿CREATE OR REPLACE FUNCTION statdb.get_video_report (
  out video_name varchar,
  out description text,
  out group_name varchar,
  out genre text,
  out group_category_id integer,
  out time_added timestamp,
  out profile_id bigint,
  out video_id bigint,
  out alias varchar
)
RETURNS SETOF record AS
$body$
CLUSTER 'planeta';
RUN ON ALL;

SELECT profiles.profile_id, alias, video_id, videos.name as video_name, videos.description, profiles.name as group_name,
genre, group_category_id, videos.time_added
FROM profiledb.videos
JOIN profiledb.profiles ON videos.owner_profile_id = profiles.profile_id
JOIN profiledb.groups ON profiles.profile_id = groups.profile_id
WHERE profiles.profile_type_id = 2 AND profiles.status IN (2, 5, 6);
$body$
LANGUAGE 'plproxy';
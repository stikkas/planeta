CREATE OR REPLACE FUNCTION statdb.update_all_top_stats (
)
RETURNS pg_catalog.void AS
$body$
BEGIN
PERFORM statdb.update_group_top_stats();
PERFORM statdb.update_audio_top_stats();
PERFORM statdb.update_video_top_stats();
PERFORM statdb.update_blog_top_stats();
PERFORM statdb.update_campaign_top_stats();
PERFORM statdb.update_audio_albums_top_stats();
PERFORM statdb.update_top_comments_stats();
RETURN;
END;
$body$
LANGUAGE 'plpgsql';

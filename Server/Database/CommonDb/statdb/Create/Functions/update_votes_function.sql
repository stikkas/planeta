CREATE OR REPLACE FUNCTION statdb.update_votes_stats (
)
RETURNS pg_catalog.void AS
$$
DECLARE
    rec record;
    rec2 record;
    v_time TIMESTAMP;
    v_report_date DATE;
    v_hour INTEGER;
BEGIN

    v_time = now();
    SELECT report_date,
           hour
      FROM statdb.completed_jobs
     WHERE job_name = 'update_votes'
  ORDER BY report_date DESC, hour DESC
     LIMIT 1
      INTO v_report_date, v_hour;

    IF ( v_report_date >= v_time::DATE AND  v_hour >= EXTRACT(hour from v_time))
    THEN RETURN;
    END IF;

    FOR rec IN (SELECT * FROM statdb.get_votes_stats((v_report_date || ' ' || v_hour || ':00:00')::TIMESTAMP)
    ) LOOP

      IF (SELECT profile_id
            FROM statdb.blog_general
           WHERE profile_id = rec.owner_profile_id
             AND report_date = rec.time_added::DATE
             AND hour = EXTRACT(hour from rec.time_added)
             AND blog_post_id = rec.blog_post_id
        ) IS NULL THEN
             INSERT INTO statdb.blog_general(
                  profile_id,
                  blog_post_id,
                  report_date,
                  hour,
                  views_count,
                  unique_voters_count,
                  rating
           ) VALUES (
                  rec.owner_profile_id,
                  rec.blog_post_id,
                  rec.time_added,
                  EXTRACT(hour from rec.time_added),
                  abs(rec.rating),
                  abs(rec.rating),
                  rec.rating
           );
     ELSE
         UPDATE statdb.blog_general
            SET
                rating = rec.rating,
                unique_voters_count = rec.voters_count
          WHERE profile_id = rec.owner_profile_id
            AND report_date = rec.time_added::DATE
            AND hour = EXTRACT(hour from rec.time_added)
            AND blog_post_id = rec.blog_post_id
              ;
   END IF;
 END LOOP;

-- remember already counted in special row
INSERT INTO statdb.completed_jobs(
	job_name,
	report_date,
    hour
  ) VALUES (
  	'update_votes',
     v_time,
     EXTRACT(hour from v_time)     
  );

RETURN;
END;
$$ LANGUAGE 'plpgsql';


SELECT * FROM statdb.update_votes_stats();

SELECT * FROM statdb.blog_general;


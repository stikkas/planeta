CREATE OR REPLACE FUNCTION statdb.get_top_groups_stats (
  out profile_id bigint,
  out object_id bigint
)
RETURNS SETOF record AS
$body$
CLUSTER 'planeta';
RUN ON ALL;
	SELECT profile_id,
		   0 as object_id
	  FROM profiledb.profiles
	 WHERE users_count >= 200
	   AND status = 2
	   AND profile_type_id = 2
    LIMIT 1000; 
$body$
LANGUAGE 'plproxy';

SELECT * FROM statdb.get_top_groups_stats();
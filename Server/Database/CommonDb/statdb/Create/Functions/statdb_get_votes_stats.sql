CREATE OR REPLACE FUNCTION statdb.get_votes_stats (
  p_date_from timestamp,
  out rating integer,
  out voters_count integer,
  out owner_profile_id bigint,
  out blog_post_id bigint,
  out time_added timestamp
)
RETURNS SETOF record AS
$body$
    CLUSTER 'planeta';
    RUN ON ALL;
    SELECT sum(t1.vote_value) as rating,
           COUNT(*) as voters_count,
           t1.profile_id AS owner_profile_id,
           t1.object_id AS blog_post_id,
           date_trunc('hour', time_updated) AS time_added
      FROM profiledb.votes t1
     WHERE t1.time_updated > p_date_from
  GROUP BY 3, 4, 5;
$body$
LANGUAGE 'plproxy'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100 ROWS 1000;
﻿CREATE OR REPLACE FUNCTION statdb.get_group_info (
  out profile_id bigint,
  out name varchar,
  out genre text,
  out group_category_id integer,
  out summary text
)
RETURNS SETOF record AS
$body$
CLUSTER 'planeta';
RUN ON ALL;

SELECT profiles.profile_id, profiles.name, profiles.summary, genre, group_category_id
FROM profiledb.groups
JOIN profiledb.profiles ON profiles.profile_id = groups.profile_id
WHERE profiles.status IN (2, 5, 6);
$body$
LANGUAGE 'plproxy';
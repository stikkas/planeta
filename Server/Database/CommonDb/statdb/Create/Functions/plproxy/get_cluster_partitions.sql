CREATE OR REPLACE FUNCTION plproxy.get_cluster_partitions (
  cluster_name text
)
RETURNS SETOF text AS
$body$
BEGIN
    IF cluster_name = 'planeta' THEN
        RETURN NEXT 'dbname=planeta_node1 host=localhost user=planeta';
        RETURN NEXT 'dbname=planeta_node2 host=localhost user=planeta';
        RETURN;
    END IF;
    
    IF cluster_name = 'commoncluster' THEN
        RETURN NEXT 'dbname=planeta_common host=localhost user=planeta';
        RETURN;
    END IF;
    
    RAISE EXCEPTION 'Unknown cluster';
END;
$body$
LANGUAGE 'plpgsql'
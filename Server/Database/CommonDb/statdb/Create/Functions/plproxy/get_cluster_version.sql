CREATE OR REPLACE FUNCTION plproxy.get_cluster_version (
  cluster_name text
)
RETURNS integer AS
$body$
BEGIN
    IF cluster_name = 'planeta' THEN
        RETURN 1;
    END IF;
    
    IF cluster_name = 'commoncluster' THEN
        RETURN 1;
    END IF;
    
    RAISE EXCEPTION 'Unknown cluster';
END;
$body$
LANGUAGE 'plpgsql'
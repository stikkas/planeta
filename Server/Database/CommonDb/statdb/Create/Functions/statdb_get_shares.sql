﻿CREATE OR REPLACE FUNCTION statdb.get_shares (
  out project_name varchar,
  out share_name varchar,
  out category_type integer,
  out description text,
  out profile_id bigint,
  out time_added timestamp,
  out campaign_id bigint,
  out share_id bigint
)
RETURNS SETOF record AS
$body$
CLUSTER 'commoncluster';

SELECT shares.share_id, shares.campaign_id, campaigns.name AS project_name, shares.name AS share_name, category_type, 
       shares.description, campaigns.profile_id, shares.time_added
FROM commondb.shares
JOIN commondb.campaigns ON campaigns.campaign_id = shares.campaign_id;
$body$
LANGUAGE 'plproxy';
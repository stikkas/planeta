﻿CREATE OR REPLACE FUNCTION statdb.get_audio_report (
  out profile_id bigint,
  out track_name varchar,
  out album_name varchar,
  out artist_name varchar,
  out description text,
  out group_name varchar,
  out genre text,
  out group_category_id integer,
  out time_added timestamp,
  out album_id bigint,
  out alias varchar
)
RETURNS SETOF record AS
$body$
CLUSTER 'planeta';
RUN ON ALL;
select profiles.profile_id, alias, audio_tracks.album_id, 
track_name, album_name, audio_tracks.artist_name, audio_albums.description,
profiles.name as group_name, genre, group_category_id, audio_tracks.time_added
from profiledb.audio_tracks
JOIN profiledb.profiles ON audio_tracks.owner_profile_id = profiles.profile_id
JOIN profiledb.audio_albums ON audio_tracks.album_id = audio_albums.album_id
JOIN profiledb.groups ON profiles.profile_id = groups.profile_id
WHERE profiles.profile_type_id = 2 AND profiles.status IN (2, 5, 6);
$body$
LANGUAGE 'plproxy';
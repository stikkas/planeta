CREATE OR REPLACE FUNCTION statdb.update_audio_top_stats (
)
RETURNS pg_catalog.void AS 
$$
DECLARE
    rec record;
    v_position INTEGER;
BEGIN

DELETE FROM statdb.top_stats
      WHERE object_type_id = 9;

v_position = 0;
FOR rec IN ( SELECT profile_id,
             track_id AS object_id
        FROM statdb.audio_general
    ORDER BY report_date DESC, hour DESC, listenings_count DESC
    ) LOOP
    	v_position = v_position + 1;
        
      IF v_position > 100 THEN
        EXIT;  -- exit loop
	 END IF;
        
        
        IF (SELECT profile_id
            FROM statdb.top_stats
           WHERE profile_id = rec.profile_id
             AND object_id = rec.object_id
             AND object_type_id = 9
        ) IS NULL THEN
        INSERT INTO statdb.top_stats (
			profile_id,
			object_id,
			object_type_id,
			position
		) VALUES (
			rec.profile_id,
			rec.object_id,
			9,
			v_position
		);
        END IF;
    END LOOP;
     
RETURN;
END;
$$ LANGUAGE 'plpgsql';

SELECT * from statdb.update_audio_top_stats();

SELECT * FROM statdb.top_stats WHERE object_type_id = 9 ORDER BY position;
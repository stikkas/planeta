CREATE OR REPLACE FUNCTION statdb.update_campaign_top_stats (
)
RETURNS pg_catalog.void AS
$body$
DECLARE
    rec record;
    v_position INTEGER;
	v_object_type_code INTEGER;
BEGIN

v_object_type_code = 15;

DELETE FROM statdb.top_stats
      WHERE object_type_id = v_object_type_code;

v_position = 1;
FOR rec IN ( SELECT * FROM statdb.get_campaigns_stats()
    ) LOOP

        
      IF v_position > 100 THEN
        EXIT;  -- exit loop
	 END IF;
        
        
        IF (SELECT profile_id
            FROM statdb.top_stats
           WHERE profile_id = rec.profile_id
             AND object_id = rec.object_id
             AND object_type_id = v_object_type_code
        ) IS NULL THEN
        INSERT INTO statdb.top_stats (
			profile_id,
			object_id,
			object_type_id,
			position
		) VALUES (
			rec.profile_id,
			rec.object_id,
			v_object_type_code,
			v_position
		);
       	v_position = v_position + 1;
        END IF;
    END LOOP;
     
RETURN;
END;
$body$
LANGUAGE 'plpgsql'
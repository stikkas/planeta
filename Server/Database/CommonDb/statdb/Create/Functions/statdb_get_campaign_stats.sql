﻿CREATE OR REPLACE FUNCTION statdb.get_campaigns_stats (
  out profile_id bigint,
  out object_id bigint
)
RETURNS SETOF record AS
$body$
CLUSTER 'commoncluster';
	SELECT profile_id,
		   campaign_id as object_id
	  FROM commondb.campaigns
	 WHERE time_last_purchased IS NOT NULL
       AND status = 4
  ORDER BY time_last_purchased DESC LIMIT 1000;
$body$
LANGUAGE 'plproxy'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100 ROWS 1000;

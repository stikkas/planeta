CREATE OR REPLACE FUNCTION statdb.update_group_top_stats (
)
RETURNS pg_catalog.void AS 
$$
DECLARE
    rec record;
    v_position INTEGER;
	v_object_type_code INTEGER;
BEGIN

v_object_type_code = 1;

DELETE FROM statdb.top_stats
      WHERE object_type_id = v_object_type_code;

v_position = 1;
FOR rec IN ( SELECT t1.profile_id,
             0 AS object_id
        FROM statdb.group_general t1

	    JOIN ( SELECT * FROM statdb.get_top_groups_stats()) t2 ON t1.profile_id = t2.profile_id
   	   WHERE category_id = 1
    ORDER BY report_date DESC, hour DESC, views_count DESC
    ) LOOP
        
      IF v_position > 100 THEN
        EXIT;  -- exit loop
	 END IF;
        
        
        IF (SELECT profile_id
            FROM statdb.top_stats
           WHERE profile_id = rec.profile_id
             AND object_id = rec.object_id
             AND object_type_id = v_object_type_code
        ) IS NULL THEN
        INSERT INTO statdb.top_stats (
			profile_id,
			object_id,
			object_type_id,
			position
		) VALUES (
			rec.profile_id,
			rec.object_id,
			v_object_type_code,
			v_position
		);
        
       	v_position = v_position + 1;
        END IF;
    END LOOP;
     
RETURN;
END;
$$ LANGUAGE 'plpgsql';
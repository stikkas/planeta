﻿CREATE OR REPLACE FUNCTION statdb.get_group_report (
  out name varchar,
  out summary text,
  out artists varchar,
  out time_added timestamp,
  out profile_id bigint,
  out alias varchar
)
RETURNS SETOF record AS
$body$
CLUSTER 'planeta';
RUN ON ALL;

SELECT groups.profile_id, alias,  profiles.name, profiles.summary, 
		array_to_string(ARRAY(SELECT user_first_name || ' ' || user_last_name
                                FROM profiledb.profile_relations
                                JOIN profiledb.profiles ON profiles.profile_id = profiledb.profile_relations.subject_profile_id
                                WHERE profile_relations.profile_id = groups.profile_id
                                		AND profile_relations.status & 2048 = 2048), ',') as artists,
		time_added
FROM profiledb.groups
JOIN profiledb.profiles ON profiles.profile_id = groups.profile_id;
$body$
LANGUAGE 'plproxy';
CREATE OR REPLACE FUNCTION statdb.update_blog_top_stats (
)
RETURNS pg_catalog.void AS 
$$
DECLARE
    rec record;
    v_position INTEGER;
	v_object_type_code INTEGER;
BEGIN

v_object_type_code = 4;

DELETE FROM statdb.top_stats
      WHERE object_type_id = v_object_type_code;

v_position = 0;
FOR rec IN ( SELECT profile_id,
             blog_post_id AS object_id
        FROM statdb.blog_general
    ORDER BY report_date DESC, hour DESC, views_count DESC
    ) LOOP
    	v_position = v_position + 1;
        
      IF v_position > 100 THEN
        EXIT;  -- exit loop
	 END IF;
        
        
        IF (SELECT profile_id
            FROM statdb.top_stats
           WHERE profile_id = rec.profile_id
             AND object_id = rec.object_id
             AND object_type_id = v_object_type_code
        ) IS NULL THEN
        INSERT INTO statdb.top_stats (
			profile_id,
			object_id,
			object_type_id,
			position
		) VALUES (
			rec.profile_id,
			rec.object_id,
			v_object_type_code,
			v_position
		);
        END IF;
    END LOOP;
     
RETURN;
END;
$$ LANGUAGE 'plpgsql';

SELECT * from statdb.update_blog_top_stats();

SELECT * FROM statdb.top_stats WHERE object_type_id = 4 ORDER BY position;
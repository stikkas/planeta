CREATE OR REPLACE FUNCTION statdb.get_top_comments_stats (
  out comment_id bigint,
  out owner_profile_id bigint,
  out rating integer
)
RETURNS SETOF record AS
$body$
CLUSTER 'planeta';
RUN ON ALL;
  SELECT t1.comment_id,
  	     t1.owner_profile_id,
         t4.votes_total AS rating
    FROM profiledb.comments t1
    JOIN profiledb.profiles t2
      ON t1.owner_profile_id = t2.profile_id
    JOIN profiledb.vote_objects t4
      ON t1.comment_id = t4.object_id
     AND t4.object_type_code = 16
     AND t1.owner_profile_id = t4.profile_id
   WHERE t1.object_type_id = 4
     AND t1.text_html !~* E'.*<img.*?>.*'
     AND t1.text_html !~* E'.*<div.*?>.*'
     AND t1.time_added >= CURRENT_DATE - 14
     AND t1.is_deleted = FALSE
     AND t4.votes_total > 0
     AND t1.level = 0
   ORDER BY t4.votes_total DESC;
$body$
LANGUAGE 'plproxy'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100 ROWS 1000;
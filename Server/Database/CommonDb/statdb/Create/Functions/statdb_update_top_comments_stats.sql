CREATE OR REPLACE FUNCTION statdb.update_top_comments_stats()
RETURNS void AS
$$
DECLARE
	v_object_type_id INTEGER := 16;
BEGIN
	DELETE FROM statdb.top_stats WHERE object_type_id = v_object_type_id;
    
    INSERT INTO statdb.top_stats (profile_id, object_id, object_type_id, position)
		 SELECT t1.owner_profile_id AS profile_id,
	   			t1.comment_id AS object_id,
       			16 AS object_type_id,
       			row_number() OVER (ORDER BY rating) AS position
  		   FROM statdb.get_top_comments_stats() t1;
END;
$$ LANGUAGE 'plpgsql';
﻿CREATE OR REPLACE FUNCTION statdb.get_photo_report (
  out photo_id bigint,
  out title varchar,
  out description varchar,
  out group_name varchar,
  out genre text,
  out group_category_id integer,
  out time_added timestamp,
  out profile_id bigint,
  out album_id bigint,
  out alias varchar
)
RETURNS SETOF record AS
$body$
CLUSTER 'planeta';
RUN ON ALL;

SELECT profiles.profile_id, alias, photos.album_id, photo_id, title, photos.description,
profiles.name as group_name, genre, group_category_id, photos.time_added
from profiledb.photos
JOIN profiledb.profiles ON photos.owner_profile_id = profiles.profile_id
JOIN profiledb.photo_albums ON photos.album_id = photo_albums.album_id
JOIN profiledb.groups ON profiles.profile_id = groups.profile_id
WHERE profiles.profile_type_id = 2 AND profiles.status IN (2, 5, 6) AND photo_albums.album_type_id = 1;
$body$
LANGUAGE 'plproxy';
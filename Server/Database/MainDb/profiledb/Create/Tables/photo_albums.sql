CREATE TABLE profiledb.photo_albums (
  album_id BIGINT NOT NULL, 
  owner_profile_id BIGINT NOT NULL, 
  author_profile_id BIGINT NOT NULL, 
  album_type_id INTEGER DEFAULT 1 NOT NULL, 
  title VARCHAR(512), 
  image_url VARCHAR(256), 
  image_id BIGINT, 
  tags VARCHAR(256), 
  view_permission INTEGER NOT NULL, 
  time_added TIMESTAMP WITHOUT TIME ZONE DEFAULT now() NOT NULL, 
  time_updated TIMESTAMP WITHOUT TIME ZONE DEFAULT now() NOT NULL, 
  photos_count INTEGER DEFAULT 0 NOT NULL, 
  views_count INTEGER DEFAULT 0 NOT NULL, 
  CONSTRAINT photo_albums_pkey PRIMARY KEY(owner_profile_id, album_id)
) WITHOUT OIDS;

COMMENT ON TABLE profiledb.photo_albums
IS 'Contains group''s photo albums';

COMMENT ON COLUMN profiledb.photo_albums.album_id
IS 'Album''s identifier';

COMMENT ON COLUMN profiledb.photo_albums.owner_profile_id
IS 'Owner''s identifier';

COMMENT ON COLUMN profiledb.photo_albums.author_profile_id
IS 'Author''s identifier';

COMMENT ON COLUMN profiledb.photo_albums.album_type_id
IS 'Album''s type.';

COMMENT ON COLUMN profiledb.photo_albums.title
IS 'Album''s title';

COMMENT ON COLUMN profiledb.photo_albums.image_url
IS 'Preview image url';

COMMENT ON COLUMN profiledb.photo_albums.image_id
IS 'preview''s photo id';

COMMENT ON COLUMN profiledb.photo_albums.tags
IS 'Tags list';

COMMENT ON COLUMN profiledb.photo_albums.view_permission
IS 'View permission';

COMMENT ON COLUMN profiledb.photo_albums.photos_count
IS 'Photos count';

COMMENT ON COLUMN profiledb.photo_albums.views_count
IS 'Views count';

CREATE TABLE profiledb.notification_settings (
  setting_id INT8 PRIMARY KEY NOT NULL,
  profile_id INT8,
  notification_types_group_code INT4,
  notification_methods_code INT4
);
CREATE UNIQUE INDEX notification_settings_setting_id_uq ON profiledb.notification_settings (
        notification_methods_code, notification_types_group_code, profile_id
);

COMMENT ON COLUMN profiledb.notification_settings.setting_id
IS 'Notification setting identifier';

COMMENT ON COLUMN profiledb.notification_settings.profile_id
IS 'Recipient profile identifier';

COMMENT ON COLUMN profiledb.notification_settings.notification_types_group_code
IS 'Notification types group';

COMMENT ON COLUMN profiledb.notification_settings.notification_methods_code
IS 'EnumSet code of notification methods';


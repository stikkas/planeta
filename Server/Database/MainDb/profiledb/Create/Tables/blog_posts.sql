CREATE TABLE profiledb.blog_posts (
  blog_post_id BIGINT NOT NULL,
  owner_profile_id BIGINT NOT NULL,
  author_profile_id BIGINT,
  title VARCHAR(256) NOT NULL,
  heading_text TEXT,
  heading_text_html TEXT,
  post_text TEXT,
  post_text_html TEXT,
  tags VARCHAR(256),
  status INTEGER NOT NULL,
  view_permission INTEGER NOT NULL,
  rating INTEGER DEFAULT 0 NOT NULL,
  time_added TIMESTAMP WITHOUT TIME ZONE DEFAULT now() NOT NULL,
  time_updated TIMESTAMP WITHOUT TIME ZONE DEFAULT now() NOT NULL,
  is_public_blog BOOLEAN DEFAULT false,
  on_behalf_of_group BOOLEAN DEFAULT FALSE NOT NULL,
  campaign_id BIGINT DEFAULT 0,
  CONSTRAINT blog_posts_pkey PRIMARY KEY(owner_profile_id, blog_post_id)
) WITHOUT OIDS;

COMMENT ON COLUMN profiledb.blog_posts.blog_post_id
IS 'Blog post identifier';

COMMENT ON COLUMN profiledb.blog_posts.owner_profile_id
IS 'Owner''s profile identifier';

COMMENT ON COLUMN profiledb.blog_posts.author_profile_id
IS 'Author''s profile identifier';

COMMENT ON COLUMN profiledb.blog_posts.title
IS 'Post title';

COMMENT ON COLUMN profiledb.blog_posts.heading_text
IS 'Post heading text (bbcode)';

COMMENT ON COLUMN profiledb.blog_posts.heading_text_html
IS 'Post heading text (html)';

COMMENT ON COLUMN profiledb.blog_posts.post_text
IS 'Post text (bbcode)';

COMMENT ON COLUMN profiledb.blog_posts.post_text_html
IS 'Post text (html)';

COMMENT ON COLUMN profiledb.blog_posts.summary
IS 'Post summary (bbcode)';

COMMENT ON COLUMN profiledb.blog_posts.summary_html
IS 'Post summary (html)';

COMMENT ON COLUMN profiledb.blog_posts.tags
IS 'List of tags';

COMMENT ON COLUMN profiledb.blog_posts.status
IS 'Post status (draft or public)';

COMMENT ON COLUMN profiledb.blog_posts.rating
IS 'Post rating';

COMMENT ON COLUMN profiledb.blog_posts.on_behalf_of_group
IS 'Blogpost behalf of group';

COMMENT ON COLUMN profiledb.blog_posts.campaign_id
IS 'Campaign identifier';

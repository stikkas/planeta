CREATE TABLE profiledb.blog_categories (
  profile_id BIGINT NOT NULL,
  category_id INTEGER NOT NULL,
  parent_category_id INTEGER DEFAULT 0,
  name VARCHAR(256),
  time_added TIMESTAMP WITHOUT TIME ZONE DEFAULT now() NOT NULL,
  time_updated TIMESTAMP WITHOUT TIME ZONE DEFAULT now() NOT NULL,
  PRIMARY KEY(profile_id, category_id)
) WITHOUT OIDS;

COMMENT ON COLUMN profiledb.blog_categories.profile_id
IS 'Category owner''s profile identifier';

COMMENT ON COLUMN profiledb.blog_categories.category_id
IS 'Category identifier';

COMMENT ON COLUMN profiledb.blog_categories.parent_category_id
IS 'Parent category identifier';

COMMENT ON COLUMN profiledb.blog_categories.name
IS 'Category name';

COMMENT ON COLUMN profiledb.blog_categories.time_added
IS 'Time added';

COMMENT ON COLUMN profiledb.blog_categories.time_updated
IS 'Time updated';

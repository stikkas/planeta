CREATE TABLE profiledb.profile_communication_channels (
  profile_id BIGINT NOT NULL, 
  channel_id INTEGER, 
  channel_value VARCHAR(250)
) WITH OIDS;

COMMENT ON TABLE profiledb.profile_communication_channels
IS 'Profile''s communication channels (vk, yandex, etc)';

COMMENT ON COLUMN profiledb.profile_communication_channels.profile_id
IS 'User''s profile identifier';

COMMENT ON COLUMN profiledb.profile_communication_channels.channel_id
IS 'Channel identifier';

COMMENT ON COLUMN profiledb.profile_communication_channels.channel_value
IS 'Channel value';

ALTER TABLE profiledb.profile_communication_channels
  ALTER COLUMN channel_id SET NOT NULL;

ALTER TABLE profiledb.profile_communication_channels
  ADD CONSTRAINT profile_communication_channels_pkey 
    PRIMARY KEY (profile_id, channel_id);
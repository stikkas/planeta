CREATE TABLE profiledb.objects_comment_reads (
  owner_profile_id BIGINT NOT NULL,
  object_id BIGINT NOT NULL,
  object_type BIGINT NOT NULL DEFAULT 0,
  user_profile_id BIGINT NOT NULL,
  last_comment_id BIGINT DEFAULT 0,
  last_comment_count INTEGER DEFAULT 0,
  PRIMARY KEY(owner_profile_id, object_id, user_profile_id)
) WITHOUT OIDS;

COMMENT ON COLUMN profiledb.objects_comment_reads.owner_profile_id
IS 'Owner''s identifier';

COMMENT ON COLUMN profiledb.objects_comment_reads.object_id
IS 'comment object identifier';

COMMENT ON COLUMN profiledb.objects_comment_reads.object_type
IS 'comment object type code';

COMMENT ON COLUMN profiledb.objects_comment_reads.user_profile_id
IS 'Reader''s identifier';

COMMENT ON COLUMN profiledb.objects_comment_reads.last_comment_id
IS 'Last comment id';

COMMENT ON COLUMN profiledb.objects_comment_reads.last_comment_count
IS 'Last comment count';

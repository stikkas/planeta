﻿CREATE TABLE profiledb.objects_comment_subscriptions (
  owner_profile_id BIGINT NOT NULL,
  object_id BIGINT NOT NULL,
  object_type INTEGER NOT NULL,
  client_id BIGINT NOT NULL,
  CONSTRAINT objects_comment_subsciptions_pkey PRIMARY KEY(owner_profile_id, object_id, object_type, client_id)
) WITHOUT OIDS;

COMMENT ON COLUMN profiledb.objects_comment_subscriptions.owner_profile_id
IS 'Owner profile identifier';

COMMENT ON COLUMN profiledb.objects_comment_subscriptions.object_id
IS 'Object identifier';

COMMENT ON COLUMN profiledb.objects_comment_subscriptions.object_type
IS 'Object type';

COMMENT ON COLUMN profiledb.objects_comment_subscriptions.client_id
IS 'Client identifier';

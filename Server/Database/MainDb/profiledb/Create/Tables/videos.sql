﻿CREATE TABLE profiledb.videos (
  video_id BIGINT NOT NULL, 
  owner_profile_id BIGINT NOT NULL, 
  author_profile_id BIGINT NOT NULL, 
  name VARCHAR(250) NOT NULL, 
  description TEXT, 
  duration INTEGER DEFAULT 0 NOT NULL, 
  image_url VARCHAR(256), 
  image_id BIGINT, 
  video_url VARCHAR(512) NOT NULL, 
  available_quality_modes INTEGER DEFAULT 0 NOT NULL, 
  status SMALLINT DEFAULT 1 NOT NULL, 
  time_added TIMESTAMP WITHOUT TIME ZONE DEFAULT now(), 
  time_updated TIMESTAMP WITHOUT TIME ZONE DEFAULT now(), 
  views_count INTEGER DEFAULT 0 NOT NULL, 
  downloads_count INTEGER DEFAULT 0 NOT NULL, 
  video_type SMALLINT DEFAULT 1 NOT NULL, 
  view_permission INTEGER DEFAULT 1 NOT NULL, 
  cached_video_id BIGINT DEFAULT 0 NOT NULL, 
  storyboard_url VARCHAR(256) DEFAULT NULL::character varying, 
  CONSTRAINT videos_pkey PRIMARY KEY(owner_profile_id, video_id)
) WITH OIDS;

COMMENT ON COLUMN profiledb.videos.video_id
IS 'Identifier';

COMMENT ON COLUMN profiledb.videos.owner_profile_id
IS 'Owner''s profile identifier';

COMMENT ON COLUMN profiledb.videos.author_profile_id
IS 'Author''s identifier';

COMMENT ON COLUMN profiledb.videos.name
IS 'Name';

COMMENT ON COLUMN profiledb.videos.description
IS 'Description';

COMMENT ON COLUMN profiledb.videos.duration
IS 'Video duration';

COMMENT ON COLUMN profiledb.videos.image_url
IS 'Preview image url';

COMMENT ON COLUMN profiledb.videos.image_id
IS 'Preview photo identifier';

COMMENT ON COLUMN profiledb.videos.video_url
IS 'Original file url';

COMMENT ON COLUMN profiledb.videos.available_quality_modes
IS 'Available modes flag';

COMMENT ON COLUMN profiledb.videos.status
IS 'video status';

COMMENT ON COLUMN profiledb.videos.views_count
IS 'Views count';

COMMENT ON COLUMN profiledb.videos.downloads_count
IS 'Downloads count';

COMMENT ON COLUMN profiledb.videos.video_type
IS 'Video type (1 - Planeta, 2 - Youtube)';

COMMENT ON COLUMN profiledb.videos.view_permission
IS 'Video''s view permission';

COMMENT ON COLUMN profiledb.videos.cached_video_id
IS 'If > 0 - link to cached video.';

COMMENT ON COLUMN profiledb.videos.storyboard_url
IS 'Storyboard image url';
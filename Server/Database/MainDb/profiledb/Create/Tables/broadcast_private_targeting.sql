CREATE TABLE profiledb.broadcast_private_targeting (
  broadcast_id BIGINT NOT NULL,
  profile_id BIGINT NOT NULL,
  email VARCHAR(256) NOT NULL,
  generated_link VARCHAR(256) NOT NULL,
  first_visit_time TIMESTAMP WITHOUT TIME ZONE,
  valid_in_hours NUMERIC(4,2),
  time_added TIMESTAMP WITHOUT TIME ZONE DEFAULT now() NOT NULL
) 
WITH (oids = false);

COMMENT ON TABLE profiledb.broadcast_private_targeting
IS 'Profile''s private broadcasts targeting restrictions';

COMMENT ON COLUMN profiledb.broadcast_private_targeting.broadcast_id
IS 'Broadcast''s identifier';

COMMENT ON COLUMN profiledb.broadcast_private_targeting.profile_id
IS 'Broadcast''s Owner''s identifier';

COMMENT ON COLUMN profiledb.broadcast_private_targeting.email
IS 'Email address of user, for whom link was generated';

COMMENT ON COLUMN profiledb.broadcast_private_targeting.generated_link
IS 'Randomly generated link for private broadcast';

COMMENT ON COLUMN profiledb.broadcast_private_targeting.first_visit_time
IS 'First time when private broadcast was visited';

COMMENT ON COLUMN profiledb.broadcast_private_targeting.valid_in_hours
IS 'Link validtity in hours. Time when link would be expired from the first visit';

COMMENT ON COLUMN profiledb.broadcast_private_targeting.time_added
IS 'Time when this record was added';

CREATE INDEX broadcast_private_targeting_idx ON profiledb.broadcast_private_targeting
(profile_id, broadcast_id);

-- END: private broadcasts
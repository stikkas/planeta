CREATE TABLE profiledb.tags (
  tag_id BIGINT NOT NULL, 
  profile_id BIGINT NOT NULL, 
  object_type_id INTEGER DEFAULT 0 NOT NULL, 
  tag_name VARCHAR(128) NOT NULL, 
  objects_count INTEGER NOT NULL, 
  CONSTRAINT tags_pkey PRIMARY KEY(profile_id, tag_id)
) WITHOUT OIDS;

COMMENT ON TABLE profiledb.tags
IS 'Tags added by this profile';

COMMENT ON COLUMN profiledb.tags.tag_id
IS 'Tag identifier';

COMMENT ON COLUMN profiledb.tags.profile_id
IS 'Profile identifier';

COMMENT ON COLUMN profiledb.tags.object_type_id
IS 'Object type identifier';

COMMENT ON COLUMN profiledb.tags.tag_name
IS 'Tag name';

COMMENT ON COLUMN profiledb.tags.objects_count
IS 'Count of objects with this tag';

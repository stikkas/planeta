CREATE TABLE profiledb.chats (
  chat_id BIGINT NOT NULL,
  owner_profile_id BIGINT NOT NULL,
  owner_object_id BIGINT NOT NULL,
  owner_object_type_code BIGINT NOT NULL,	
  time_added TIMESTAMP WITHOUT TIME ZONE DEFAULT now() NOT NULL, 
  CONSTRAINT chats_pkey PRIMARY KEY(owner_profile_id, chat_id)
) WITH OIDS;

COMMENT ON TABLE profiledb.chats
IS 'Table containing chats';

COMMENT ON COLUMN profiledb.chats.chat_id
IS 'Chat identifier';

COMMENT ON COLUMN profiledb.chats.owner_profile_id
IS 'Owner profile identifier';

COMMENT ON COLUMN profiledb.chats.owner_object_id
IS 'Chat owner profile identifier';

COMMENT ON COLUMN profiledb.chats.owner_object_type_code
IS 'Chat owner profile type';

COMMENT ON COLUMN profiledb.chats.time_added
IS 'Time when chat was created';

CREATE TABLE profiledb.votes
(
  vote_id bigint PRIMARY KEY NOT NULL,
  vote_value int NOT NULL,
  author_id bigint NOT NULL,
  profile_id bigint NOT NULL,
  object_id bigint NOT NULL,
  object_type_code int NOT NULL,
  time_updated TIMESTAMP DEFAULT now() NOT NULL
);
CREATE UNIQUE INDEX one_vote_per_user_uc ON profiledb.votes ( profile_id, object_id, object_type_code, author_id);
CREATE INDEX object_likes_idx ON profiledb.votes ( profile_id, object_id, object_type_code, vote_value);

COMMENT ON COLUMN profiledb.votes.vote_id IS 'Unique id of vote of some author to some object (equal to one_vote_per_user_uc)';
COMMENT ON COLUMN profiledb.votes.vote_value IS 'Value of vote ( -1, 0 or 1)';
COMMENT ON COLUMN profiledb.votes.author_id IS 'Vote author (voter)';
COMMENT ON COLUMN profiledb.votes.profile_id IS 'Vote object profileId';
COMMENT ON COLUMN profiledb.votes.object_id IS 'Vote object';
COMMENT ON COLUMN profiledb.votes.object_type_code IS 'Vote object type (IVotable)';
COMMENT ON COLUMN profiledb.votes.time_updated IS 'Just for history or sort order';

CREATE SEQUENCE profiledb.seq_vote_id
INCREMENT 1 MINVALUE 1
MAXVALUE 9223372036854775807 START 1
CACHE 1;
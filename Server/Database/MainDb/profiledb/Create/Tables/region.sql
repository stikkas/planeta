CREATE TABLE profiledb.region (
  region_id INTEGER NOT NULL, 
  country_id INTEGER NOT NULL, 
  region_name_ru VARCHAR(255), 
  region_name_en VARCHAR(255), 
  CONSTRAINT region_pkey PRIMARY KEY(region_id)
) WITHOUT OIDS;

CREATE INDEX region_country_id_idx ON profiledb.region
  USING btree (country_id);

BEGIN;

/* Data for the 'profiledb.region' table  (Records 1 - 500) */

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1, 1, E'������ � ���������� ���.', E'Moscow & Moscow Region');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (2, 1, E'�����-��������� � �������', E'Saint-Petersburg and Region');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (3, 1, E'������', E'Adygeya');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (4, 1, E'��������� ����', E'Altaiskii krai');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (5, 1, E'�������� ���.', E'Amurskaya obl.');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (6, 1, E'������������� ���.', E'Arhangelskaya obl.');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (7, 1, E'������������ ���.', E'Astrahanskaya obl.');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (8, 1, E'������������(��������)', E'Bashkortostan(Bashkiriya)');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (9, 1, E'������������ ���.', E'Belgorodskaya obl.');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (10, 1, E'�������� ���.', E'Bryanskaya obl.');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (11, 1, E'�������', E'Buryatiya');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (12, 1, E'������������ ���.', E'Vladimirskaya obl.');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (13, 1, E'������������� ���.', E'Volgogradskaya obl.');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (14, 1, E'����������� ���.', E'Vologodskaya obl.');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (15, 1, E'����������� ���.', E'Voronezhskaya obl.');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (16, 1, E'��������', E'Dagestan');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (17, 1, E'��������� ���.', E'Evreiskaya obl.');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (18, 1, E'���������� ���.', E'Ivanovskaya obl.');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (19, 1, E'��������� ���.', E'Irkutskaya obl.');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (20, 1, E'���������-��������', E'Kabardino-Balkariya');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (21, 1, E'��������������� ���.', E'Kaliningradskaya obl.');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (22, 1, E'��������', E'Kalmykiya');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (23, 1, E'��������� ���.', E'Kaluzhskaya obl.');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (24, 1, E'���������� ���.', E'Kamchatskaya obl.');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (25, 1, E'�������', E'Kareliya');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (26, 1, E'����������� ���.', E'Kemerovskaya obl.');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (27, 1, E'��������� ���.', E'Kirovskaya obl.');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (28, 1, E'����', E'Komi');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (29, 1, E'����������� ���.', E'Kostromskaya obl.');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (30, 1, E'������������� ����', E'Krasnodarskii krai');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (31, 1, E'������������ ����', E'Krasnoyarskii krai');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (32, 1, E'���������� ���.', E'Kurganskaya obl.');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (33, 1, E'������� ���.', E'Kurskaya obl.');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (34, 1, E'�������� ���.', E'Lipetskaya obl.');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (35, 1, E'����������� ���.', E'Magadanskaya obl.');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (36, 1, E'����� ��', E'Marii El');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (37, 1, E'��������', E'Mordoviya');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (38, 1, E'���������� ���.', E'Murmanskaya obl.');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (39, 1, E'������������� (�����������)', E'Nizhegorodskaya (Gorkovskaya)');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (40, 1, E'������������ ���.', E'Novgorodskaya obl.');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (41, 1, E'������������� ���.', E'Novosibirskaya obl.');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (42, 1, E'������ ���.', E'Omskaya obl.');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (43, 1, E'������������ ���.', E'Orenburgskaya obl.');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (44, 1, E'��������� ���.', E'Orlovskaya obl.');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (45, 1, E'���������� ���.', E'Penzenskaya obl.');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (46, 1, E'�������� ����', E'Permskiy krai');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (47, 1, E'���������� ����', E'Primorskii krai');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (48, 1, E'��������� ���.', E'Pskovskaya obl.');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (49, 1, E'���������� ���.', E'Rostovskaya obl.');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (50, 1, E'��������� ���.', E'Ryazanskaya obl.');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (51, 1, E'��������� ���.', E'Samarskaya obl.');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (52, 1, E'����������� ���.', E'Saratovskaya obl.');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (53, 1, E'���� (������)', E'Saha (Yakutiya)');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (54, 1, E'�������', E'Sahalin');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (55, 1, E'������������ ���.', E'Sverdlovskaya obl.');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (56, 1, E'�������� ������', E'Severnaya Osetiya');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (57, 1, E'���������� ���.', E'Smolenskaya obl.');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (58, 1, E'�������������� ����', E'Stavropolskii krai');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (59, 1, E'���������� ���.', E'Tambovskaya obl.');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (60, 1, E'���������', E'Tatarstan');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (61, 1, E'�������� ���.', E'Tverskaya obl.');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (62, 1, E'������� ���.', E'Tomskaya obl.');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (63, 1, E'���� (��������� ����.)', E'Tuva (Tuvinskaya Resp.)');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (64, 1, E'�������� ���.', E'Tulskaya obl.');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (65, 1, E'��������� ���. � �����-���������� ��', E'Tyumenskaya obl. i Hanty-Mansiiskii AO');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (66, 1, E'��������', E'Udmurtiya');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (67, 1, E'����������� ���.', E'Ulyanovskaya obl.');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (68, 1, E'��������� ���.', E'Uralskaya obl.');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (69, 1, E'����������� ����', E'Habarovskii krai');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (70, 1, E'�������', E'Hakasiya');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (71, 1, E'����������� ���.', E'Chelyabinskaya obl.');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (72, 1, E'������-���������', E'Checheno-Ingushetiya');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (73, 1, E'��������� ���.', E'Chitinskaya obl.');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (74, 1, E'�������', E'Chuvashiya');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (75, 1, E'��������� ��', E'Chukotskii AO');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (76, 1, E'�����-�������� ��', E'Yamalo-Nenetskii AO');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (77, 1, E'����������� ���.', E'Yaroslavskaya obl.');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (78, 1, E'���������-���������� ����������', E'Karachaeva-Cherkesskaya Respublica');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (79, 2, E'��������� ���.', E'Vinnitskaya obl.');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (80, 2, E'��������� ���.', E'Volynskaya obl.');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (81, 2, E'���������������� ���.', E'Dnepropetrovskaya obl.');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (82, 2, E'�������� ���.', E'Donetskaya obl.');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (83, 2, E'����������� ���.', E'Zhitomirskaya obl.');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (84, 2, E'������������ ���.', E'Zakarpatskaya obl.');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (85, 2, E'����������� ���.', E'Zaporozhskaya obl.');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (86, 2, E'�����-����������� ���.', E'Ivano-Frankovskaya obl.');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (87, 2, E'�������� ���.', E'Kievskaya obl.');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (88, 2, E'�������������� ���.', E'Kirovogradskaya obl.');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (89, 2, E'�������� ���.', E'Krymskaya obl.');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (90, 2, E'��������� ���.', E'Luganskaya obl.');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (91, 2, E'��������� ���.', E'Lvovskaya obl.');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (92, 2, E'������������ ���.', E'Nikolaevskaya obl.');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (93, 2, E'�������� ���.', E'Odesskaya obl.');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (94, 2, E'���������� ���.', E'Poltavskaya obl.');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (95, 2, E'��������� ���.', E'Rovenskaya obl.');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (96, 2, E'������� ���.', E'Sumskaya obl.');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (97, 2, E'������������� ���.', E'Ternopolskaya obl.');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (98, 2, E'�������', E'Ukraina');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (99, 2, E'����������� ���.', E'Harkovskaya obl.');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (100, 2, E'���������� ���.', E'Hersonskaya obl.');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (101, 2, E'����������� ���.', E'Hmelnitskaya obl.');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (102, 2, E'���������� ���.', E'Cherkasskaya obl.');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (103, 2, E'������������ ���.', E'Chernigovskaya obl.');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (104, 2, E'����������� ���.', E'Chernovitskaya obl.');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (105, 3, E'�������', E'�������');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (106, 4, E'Australian Capital Territory', E'Australian Capital Territory');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (107, 4, E'New South Wales', E'New South Wales');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (108, 4, E'None', E'None');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (109, 4, E'Northern Territory', E'Northern Territory');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (110, 4, E'Queensland', E'Queensland');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (111, 4, E'South Australia', E'South Australia');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (112, 4, E'Tasmania', E'Tasmania');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (113, 4, E'Victoria', E'Victoria');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (114, 4, E'Western Australia', E'Western Australia');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (115, 5, E'Burgenland', E'Burgenland');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (116, 5, E'Krnten', E'Krnten');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (117, 5, E'Niedersterreich', E'Niedersterreich');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (118, 5, E'Obersterreich', E'Obersterreich');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (119, 5, E'Salzburg', E'Salzburg');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (120, 5, E'Steiermark', E'Steiermark');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (121, 5, E'Tirol', E'Tirol');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (122, 5, E'Vorarlberg', E'Vorarlberg');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (123, 5, E'Wien', E'Wien');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (124, 6, E'�����������', E'�����������');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (125, 6, E'�������� �������', E'�������� �������');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (126, 6, E'������������� ���.', E'������������� ���.');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (127, 7, E'Berat', E'Berat');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (128, 7, E'Diber', E'Diber');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (129, 7, E'Durres', E'Durres');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (130, 7, E'Elbasan', E'Elbasan');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (131, 7, E'Fier', E'Fier');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (132, 7, E'Gjirokaster', E'Gjirokaster');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (133, 7, E'Korce', E'Korce');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (134, 7, E'Kukes', E'Kukes');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (135, 7, E'None', E'None');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (136, 7, E'Shkoder', E'Shkoder');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (137, 7, E'Tirane', E'Tirane');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (138, 7, E'Vlore', E'Vlore');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (139, 8, E'�����', E'Algeria');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (140, 9, E'������', E'Angola');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (141, 10, E'Anguilla', E'Anguilla');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (142, 11, E'Andorra la Vella', E'Andorra la Vella');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (143, 11, E'Canillo', E'Canillo');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (144, 11, E'Encamp', E'Encamp');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (145, 11, E'Escaldes-Engordany', E'Escaldes-Engordany');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (146, 11, E'La Massana', E'La Massana');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (147, 11, E'None', E'None');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (148, 11, E'Ordino', E'Ordino');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (149, 11, E'Sant Julia de Loria', E'Sant Julia de Loria');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (150, 12, E'������� � �������', E'Antigua and Barbuda');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (151, 13, E'����������', E'Willemstad');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (152, 14, E'Buenos Aires', E'Buenos Aires');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (153, 14, E'Catamarca', E'Catamarca');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (154, 14, E'Chaco', E'Chaco');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (155, 14, E'Chubut', E'Chubut');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (156, 14, E'Crdoba', E'Crdoba');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (157, 14, E'Corrientes', E'Corrientes');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (158, 14, E'Distrito Federal', E'Distrito Federal');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (159, 14, E'Entre Ros', E'Entre Ros');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (160, 14, E'Formosa', E'Formosa');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (161, 14, E'Jujuy', E'Jujuy');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (162, 14, E'La Pampa', E'La Pampa');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (163, 14, E'La Rioja', E'La Rioja');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (164, 14, E'Mendoza', E'Mendoza');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (165, 14, E'Misiones', E'Misiones');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (166, 14, E'Neuqun', E'Neuqun');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (167, 14, E'Ro Negro', E'Ro Negro');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (168, 14, E'Salta', E'Salta');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (169, 14, E'San Juan', E'San Juan');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (170, 14, E'San Luis', E'San Luis');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (171, 14, E'Santa Cruz', E'Santa Cruz');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (172, 14, E'Santa Fe', E'Santa Fe');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (173, 14, E'Santiago del Estero', E'Santiago del Estero');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (174, 14, E'Tierra del Fuego', E'Tierra del Fuego');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (175, 14, E'Tucumn', E'Tucumn');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (176, 15, E'Aragatsotn', E'Aragatsotn');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (177, 15, E'Ararat', E'Ararat');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (178, 15, E'Armavir', E'Armavir');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (179, 15, E'Geghark''unik''', E'Geghark''unik''');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (180, 15, E'Kotayk''', E'Kotayk''');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (181, 15, E'Lorri', E'Lorri');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (182, 15, E'None', E'None');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (183, 15, E'Shirak', E'Shirak');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (184, 15, E'Syunik''', E'Syunik''');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (185, 15, E'Tavush', E'Tavush');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (186, 15, E'Vayots'' Dzor', E'Vayots'' Dzor');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (187, 15, E'Yerevan', E'Yerevan');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (188, 16, E'�������', E'�������');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (189, 17, E'����������', E'Afghanistan');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (190, 18, E'������', E'������');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (191, 19, E'���������', E'Bangladesh');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (192, 20, E'��������', E'Barbados');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (193, 21, E'�������', E'Bahrain');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (194, 22, E'��������� ���.', E'Brestskaya obl.');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (195, 22, E'��������� ���.', E'Vitebskaya obl.');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (196, 22, E'���������� ���.', E'Gomelskaya obl.');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (197, 22, E'����������� ���.', E'Grodnenskaya obl.');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (198, 22, E'������� ���.', E'Minskaya obl.');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (199, 22, E'����������� ���.', E'Mogilevskaya obl.');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (200, 23, E'Belize', E'Belize');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (201, 24, E'Antwerpen', E'Antwerpen');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (202, 24, E'Brabant Wallon', E'Brabant Wallon');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (203, 24, E'Bruxelles', E'Bruxelles');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (204, 24, E'Hainaut', E'Hainaut');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (205, 24, E'Lige', E'Lige');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (206, 24, E'Limburg', E'Limburg');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (207, 24, E'Luxembourg', E'Luxembourg');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (208, 24, E'Namur', E'Namur');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (209, 24, E'Oost-Vlaanderen', E'Oost-Vlaanderen');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (210, 24, E'Vlaams Brabant', E'Vlaams Brabant');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (211, 24, E'West-Vlaanderen', E'West-Vlaanderen');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (212, 25, E'�����', E'Benin');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (213, 26, E'Hamilton', E'Hamilton');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (214, 27, E'Blagoevgrad', E'Blagoevgrad');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (215, 27, E'Burgas', E'Burgas');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (216, 27, E'Dobrich', E'Dobrich');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (217, 27, E'Gabrovo', E'Gabrovo');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (218, 27, E'Grad Sofiya', E'Grad Sofiya');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (219, 27, E'Khaskovo', E'Khaskovo');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (220, 27, E'Kurdzhali', E'Kurdzhali');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (221, 27, E'Kyustendil', E'Kyustendil');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (222, 27, E'Lovech', E'Lovech');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (223, 27, E'Montana', E'Montana');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (224, 27, E'None', E'None');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (225, 27, E'Pazardzhik', E'Pazardzhik');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (226, 27, E'Pernik', E'Pernik');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (227, 27, E'Pleven', E'Pleven');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (228, 27, E'Plovdiv', E'Plovdiv');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (229, 27, E'Razgrad', E'Razgrad');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (230, 27, E'Ruse', E'Ruse');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (231, 27, E'Shumen', E'Shumen');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (232, 27, E'Silistra', E'Silistra');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (233, 27, E'Sliven', E'Sliven');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (234, 27, E'Smolyan', E'Smolyan');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (235, 27, E'Sofiya', E'Sofiya');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (236, 27, E'Stara Zagora', E'Stara Zagora');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (237, 27, E'Turgovishte', E'Turgovishte');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (238, 27, E'Varna', E'Varna');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (239, 27, E'Veliko Turnovo', E'Veliko Turnovo');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (240, 27, E'Vidin', E'Vidin');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (241, 27, E'Vratsa', E'Vratsa');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (242, 27, E'Yambol', E'Yambol');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (243, 28, E'Chuquisaca', E'Chuquisaca');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (244, 28, E'Cochabamba', E'Cochabamba');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (245, 28, E'El Beni', E'El Beni');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (246, 28, E'La Paz', E'La Paz');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (247, 28, E'None', E'None');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (248, 28, E'Oruro', E'Oruro');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (249, 28, E'Potos', E'Potos');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (250, 28, E'Santa Cruz', E'Santa Cruz');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (251, 29, E'Federation of Bosnia and Herzegovina', E'Federation of Bosnia and Herzegovina');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (252, 29, E'None', E'None');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (253, 29, E'Republika Srpska', E'Republika Srpska');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (254, 30, E'��������', E'Botswana');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (255, 31, E'Acre', E'Acre');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (256, 31, E'Alagoas', E'Alagoas');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (257, 31, E'Amapa', E'Amapa');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (258, 31, E'Amazonas', E'Amazonas');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (259, 31, E'Bahia', E'Bahia');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (260, 31, E'Ceara', E'Ceara');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (261, 31, E'Distrito Federal', E'Distrito Federal');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (262, 31, E'Espirito Santo', E'Espirito Santo');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (263, 31, E'Goias', E'Goias');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (264, 31, E'Maranhao', E'Maranhao');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (265, 31, E'Minas Gerais', E'Minas Gerais');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (266, 31, E'Paraiba', E'Paraiba');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (267, 31, E'Parana', E'Parana');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (268, 31, E'Rio de Janeiro', E'Rio de Janeiro');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (269, 31, E'Rio Grande do Norte', E'Rio Grande do Norte');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (270, 31, E'Rio Grande do Sul', E'Rio Grande do Sul');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (271, 31, E'Rondonia', E'Rondonia');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (272, 31, E'Roraima', E'Roraima');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (273, 31, E'Santa Catarina', E'Santa Catarina');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (274, 31, E'Sao Paulo', E'Sao Paulo');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (275, 31, E'Sergipe', E'Sergipe');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (276, 32, E'���-����', E'���-����');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (277, 33, E'������', E'Brunei');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (278, 34, E'������� ����', E'Burkina Faso');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (279, 35, E'�������', E'Burundi');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (280, 36, E'�����', E'Bhutan');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (281, 37, E'����-���', E'Mata-Utu');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (282, 38, E'�������', E'Vanuatu');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (283, 39, E'Channel Islands', E'Channel Islands');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (284, 39, E'England - East', E'England - East');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (285, 39, E'England - East Midlands', E'England - East Midlands');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (286, 39, E'England - London', E'England - London');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (287, 39, E'England - North East', E'England - North East');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (288, 39, E'England - North West', E'England - North West');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (289, 39, E'England - South East', E'England - South East');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (290, 39, E'England - South West', E'England - South West');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (291, 39, E'England - West Midlands', E'England - West Midlands');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (292, 39, E'England - Yorks & Humber', E'England - Yorks & Humber');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (293, 39, E'Isle of Man', E'Isle of Man');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (294, 39, E'Northern Ireland', E'Northern Ireland');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (295, 39, E'Scotland Central', E'Scotland Central');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (296, 39, E'Scotland North', E'Scotland North');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (297, 39, E'Scotland South', E'Scotland South');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (298, 39, E'Wales North', E'Wales North');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (299, 39, E'Wales South', E'Wales South');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (300, 40, E'Bacs-Kiskun', E'Bacs-Kiskun');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (301, 40, E'Bekes', E'Bekes');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (302, 40, E'Borsod-Abauj-Zemplen', E'Borsod-Abauj-Zemplen');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (303, 40, E'Budapest', E'Budapest');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (304, 40, E'Csongrad', E'Csongrad');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (305, 40, E'Debrecen', E'Debrecen');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (306, 40, E'Fejer', E'Fejer');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (307, 40, E'Gyor', E'Gyor');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (308, 40, E'Gyor-Moson-Sopron', E'Gyor-Moson-Sopron');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (309, 40, E'Heves', E'Heves');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (310, 40, E'Jasz-Nagykun-Szolnok', E'Jasz-Nagykun-Szolnok');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (311, 40, E'Komarom-Esztergom', E'Komarom-Esztergom');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (312, 40, E'Miskolc', E'Miskolc');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (313, 40, E'Nograd', E'Nograd');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (314, 40, E'Pecs', E'Pecs');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (315, 40, E'Pest', E'Pest');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (316, 40, E'Somogy', E'Somogy');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (317, 40, E'Szabolcs-Szatmar-Bereg', E'Szabolcs-Szatmar-Bereg');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (318, 40, E'Szeged', E'Szeged');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (319, 40, E'Tolna', E'Tolna');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (320, 40, E'Vas', E'Vas');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (321, 40, E'Veszprem', E'Veszprem');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (322, 40, E'Zala', E'Zala');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (323, 41, E'Amazonas', E'Amazonas');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (324, 41, E'Anzoategui', E'Anzoategui');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (325, 41, E'Apure', E'Apure');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (326, 41, E'Aragua', E'Aragua');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (327, 41, E'Barinas', E'Barinas');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (328, 41, E'Bolvar', E'Bolvar');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (329, 41, E'Carabobo', E'Carabobo');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (330, 41, E'Cojedes', E'Cojedes');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (331, 41, E'Delta Amacuro', E'Delta Amacuro');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (332, 41, E'Distrito Federal', E'Distrito Federal');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (333, 41, E'Falcn', E'Falcn');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (334, 41, E'Gurico', E'Gurico');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (335, 41, E'Lara', E'Lara');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (336, 41, E'Mrida', E'Mrida');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (337, 41, E'Miranda', E'Miranda');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (338, 41, E'Monagas', E'Monagas');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (339, 41, E'None', E'None');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (340, 41, E'Nueva Esparta', E'Nueva Esparta');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (341, 41, E'Portuguesa', E'Portuguesa');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (342, 41, E'Sucre', E'Sucre');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (343, 41, E'Tchira', E'Tchira');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (344, 41, E'Yaracuy', E'Yaracuy');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (345, 41, E'Zulia', E'Zulia');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (346, 42, E'��������� �����', E'East Timor');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (347, 43, E'Dong Bang Song Hong', E'Dong Bang Song Hong');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (348, 43, E'Dong Nam Bo', E'Dong Nam Bo');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (349, 43, E'Duyen Hai Mien Trung', E'Duyen Hai Mien Trung');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (350, 43, E'Khu Bon Cu', E'Khu Bon Cu');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (351, 43, E'Mien Nui Va Trung Du', E'Mien Nui Va Trung Du');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (352, 43, E'Thai Nguyen', E'Thai Nguyen');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (353, 44, E'�����', E'Gabon');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (354, 45, E'Artibonite', E'Artibonite');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (355, 45, E'Grand''Anse', E'Grand''Anse');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (356, 45, E'Nord-Ouest', E'Nord-Ouest');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (357, 45, E'Ouest', E'Ouest');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (358, 45, E'Sud', E'Sud');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (359, 45, E'Sud-Est', E'Sud-Est');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (360, 46, E'������', E'Guyana');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (361, 47, E'������', E'Gambia');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (362, 48, E'����', E'Ghana');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (363, 49, E'Grande-Terre', E'Grande-Terre');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (364, 49, E'���-���', E'���-���');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (365, 50, E'Alta Verapaz', E'Alta Verapaz');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (366, 50, E'Baja Verapaz', E'Baja Verapaz');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (367, 50, E'Chimaltenango', E'Chimaltenango');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (368, 50, E'Chiquimula', E'Chiquimula');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (369, 50, E'Escuintla', E'Escuintla');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (370, 50, E'Guatemala', E'Guatemala');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (371, 50, E'Jutiapa', E'Jutiapa');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (372, 50, E'Petn', E'Petn');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (373, 50, E'Quetzaltenango', E'Quetzaltenango');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (374, 50, E'Totonicapn', E'Totonicapn');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (375, 50, E'Zacapa', E'Zacapa');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (376, 51, E'������', E'Guinea');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (377, 52, E'�����', E'�����');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (378, 53, E'Baden-Wrttemberg', E'Baden-Wrttemberg');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (379, 53, E'Bayern', E'Bayern');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (380, 53, E'Berlin', E'Berlin');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (381, 53, E'Brandenburg', E'Brandenburg');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (382, 53, E'Bremen', E'Bremen');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (383, 53, E'Hamburg', E'Hamburg');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (384, 53, E'Hessen', E'Hessen');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (385, 53, E'Mecklenburg-Vorpommern', E'Mecklenburg-Vorpommern');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (386, 53, E'Niedersachsen', E'Niedersachsen');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (387, 53, E'Nordrhein-Westfalen', E'Nordrhein-Westfalen');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (388, 53, E'Rheinland-Pfalz', E'Rheinland-Pfalz');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (389, 53, E'Saarland', E'Saarland');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (390, 53, E'Sachsen', E'Sachsen');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (391, 53, E'Sachsen-Anhalt', E'Sachsen-Anhalt');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (392, 53, E'Schleswig-Holstein', E'Schleswig-Holstein');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (393, 53, E'Thringen', E'Thringen');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (394, 54, E'����-�����-����', E'����-�����-����');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (395, 55, E'Gibraltar', E'Gibraltar');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (396, 56, E'������������', E'������������');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (397, 57, E'�������', E'�������');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (398, 58, E'����-��������', E'����-��������');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (399, 59, E'����������', E'����������');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (400, 60, E'Aitolia kai Akarnania', E'Aitolia kai Akarnania');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (401, 60, E'Akhaia', E'Akhaia');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (402, 60, E'Argolis', E'Argolis');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (403, 60, E'Arkadhia', E'Arkadhia');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (404, 60, E'Arta', E'Arta');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (405, 60, E'Attiki', E'Attiki');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (406, 60, E'Dhodhekanisos', E'Dhodhekanisos');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (407, 60, E'Drama', E'Drama');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (408, 60, E'Evritania', E'Evritania');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (409, 60, E'Evros', E'Evros');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (410, 60, E'Evvoia', E'Evvoia');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (411, 60, E'Florina', E'Florina');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (412, 60, E'Fthiotis', E'Fthiotis');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (413, 60, E'Grevena', E'Grevena');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (414, 60, E'Ilia', E'Ilia');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (415, 60, E'Imathia', E'Imathia');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (416, 60, E'Ioannina', E'Ioannina');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (417, 60, E'Iraklion', E'Iraklion');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (418, 60, E'Kardhitsa', E'Kardhitsa');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (419, 60, E'Kastoria', E'Kastoria');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (420, 60, E'Kavala', E'Kavala');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (421, 60, E'Kefallinia', E'Kefallinia');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (422, 60, E'Kerkira', E'Kerkira');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (423, 60, E'Khalkidhiki', E'Khalkidhiki');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (424, 60, E'Khania', E'Khania');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (425, 60, E'Khios', E'Khios');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (426, 60, E'Kikladhes', E'Kikladhes');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (427, 60, E'Kilkis', E'Kilkis');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (428, 60, E'Korinthia', E'Korinthia');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (429, 60, E'Kozani', E'Kozani');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (430, 60, E'Lakonia', E'Lakonia');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (431, 60, E'Larisa', E'Larisa');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (432, 60, E'Lasithi', E'Lasithi');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (433, 60, E'Lesvos', E'Lesvos');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (434, 60, E'Levkas', E'Levkas');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (435, 60, E'Magnisia', E'Magnisia');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (436, 60, E'Messinia', E'Messinia');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (437, 60, E'None', E'None');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (438, 60, E'Pella', E'Pella');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (439, 60, E'Pieria', E'Pieria');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (440, 60, E'Preveza', E'Preveza');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (441, 60, E'Rethimni', E'Rethimni');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (442, 60, E'Rodhopi', E'Rodhopi');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (443, 60, E'Samos', E'Samos');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (444, 60, E'Serrai', E'Serrai');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (445, 60, E'Thesprotia', E'Thesprotia');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (446, 60, E'Thessaloniki', E'Thessaloniki');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (447, 60, E'Trikala', E'Trikala');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (448, 60, E'Voiotia', E'Voiotia');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (449, 60, E'Xanthi', E'Xanthi');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (450, 60, E'Zakinthos', E'Zakinthos');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (451, 61, E'�������', E'�������');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (452, 61, E'������', E'������');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (453, 62, E'Arhus', E'Arhus');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (454, 62, E'Bornholm', E'Bornholm');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (455, 62, E'Frederiksborg', E'Frederiksborg');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (456, 62, E'Fyn', E'Fyn');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (457, 62, E'Kobenhavn', E'Kobenhavn');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (458, 62, E'None', E'None');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (459, 62, E'Nordjylland', E'Nordjylland');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (460, 62, E'Ribe', E'Ribe');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (461, 62, E'Ringkobing', E'Ringkobing');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (462, 62, E'Roskilde', E'Roskilde');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (463, 62, E'Sonderjylland', E'Sonderjylland');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (464, 62, E'Staden Kobenhavn', E'Staden Kobenhavn');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (465, 62, E'Storstrom', E'Storstrom');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (466, 62, E'Vejle', E'Vejle');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (467, 62, E'Vestsjalland', E'Vestsjalland');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (468, 62, E'Viborg', E'Viborg');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (469, 63, E'����-����', E'Saint Helier');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (470, 64, E'�������', E'Djibouti');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (471, 65, E'�����-�������', E'�����-�������');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (472, 66, E'���-������', E'Al Qhira');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (473, 66, E'�����', E'Aswan');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (474, 66, E'�����', E'Asyut');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (475, 66, E'����-�����', E'Beni Suef');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (476, 66, E'������', E'Gharbia');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (477, 66, E'������', E'Damietta');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (478, 66, E'������', E'Egypt');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (479, 66, E'�����', E'Sinai');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (480, 67, E'������', E'Zambia');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (481, 68, E'�������� ������', E'Western Sahara');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (482, 69, E'��������', E'Zimbabwe');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (483, 70, E'����-���', E'����-���');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (484, 70, E'���������', E'���������');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (485, 70, E'����� �������', E'����� �������');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (486, 70, E'����-����', E'����-����');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (487, 70, E'�������', E'�������');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (488, 70, E'�����', E'�����');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (489, 70, E'��������', E'��������');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (490, 70, E'�������', E'�������');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (491, 71, E'Bangla', E'Bangla');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (492, 71, E'Chhattisgarh', E'Chhattisgarh');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (493, 71, E'Karnataka', E'Karnataka');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (494, 71, E'������-������', E'������-������');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (495, 71, E'�����', E'�����');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (496, 71, E'�����', E'�����');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (497, 71, E'��������', E'��������');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (498, 71, E'������ � ������', E'������ � ������');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (499, 71, E'�����', E'�����');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (500, 71, E'������', E'������');

COMMIT;

BEGIN;

/* Data for the 'profiledb.region' table  (Records 501 - 1000) */

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (501, 71, E'������-������', E'������-������');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (502, 71, E'�������', E'�������');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (503, 71, E'����������', E'����������');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (504, 71, E'������', E'������');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (505, 71, E'�������', E'�������');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (506, 71, E'���������', E'���������');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (507, 71, E'����������', E'����������');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (508, 71, E'��������', E'��������');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (509, 71, E'�������', E'�������');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (510, 71, E'�����-������', E'�����-������');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (511, 71, E'�������', E'�������');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (512, 71, E'���������', E'���������');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (513, 72, E'���������', E'���������');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (514, 73, E'��������', E'Jordan');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (515, 74, E'������', E'������');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (516, 74, E'�����', E'�����');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (517, 74, E'�����', E'�����');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (518, 75, E'Azarbayjan-e Khavari', E'Azarbayjan-e Khavari');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (519, 75, E'Esfahan', E'Esfahan');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (520, 75, E'Hamadan', E'Hamadan');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (521, 75, E'Kordestan', E'Kordestan');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (522, 75, E'Yazd', E'Yazd');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (523, 75, E'������', E'������');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (524, 75, E'�����������', E'�����������');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (525, 75, E'����������', E'����������');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (526, 75, E'�������', E'�������');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (527, 75, E'����', E'����');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (528, 75, E'�������', E'�������');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (529, 75, E'��������', E'��������');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (530, 76, E'Carlow', E'Carlow');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (531, 76, E'Cavan', E'Cavan');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (532, 76, E'Clare', E'Clare');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (533, 76, E'Cork', E'Cork');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (534, 76, E'Donegal', E'Donegal');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (535, 76, E'Dublin', E'Dublin');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (536, 76, E'Galway', E'Galway');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (537, 76, E'Kerry', E'Kerry');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (538, 76, E'Kildare', E'Kildare');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (539, 76, E'Kilkenny', E'Kilkenny');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (540, 76, E'Laois', E'Laois');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (541, 76, E'Leitrim', E'Leitrim');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (542, 76, E'Limerick', E'Limerick');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (543, 76, E'Longford', E'Longford');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (544, 76, E'Louth', E'Louth');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (545, 76, E'Mayo', E'Mayo');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (546, 76, E'Meath', E'Meath');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (547, 76, E'Monaghan', E'Monaghan');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (548, 76, E'Offaly', E'Offaly');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (549, 76, E'Roscommon', E'Roscommon');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (550, 76, E'Sligo', E'Sligo');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (551, 76, E'Tipperary', E'Tipperary');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (552, 76, E'Waterford', E'Waterford');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (553, 76, E'Westmeath', E'Westmeath');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (554, 76, E'Wexford', E'Wexford');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (555, 76, E'Wicklow', E'Wicklow');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (556, 77, E'Arnessysla', E'Arnessysla');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (557, 77, E'Austur-Hunavatnssysla', E'Austur-Hunavatnssysla');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (558, 77, E'Borgarfjardarsysla', E'Borgarfjardarsysla');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (559, 77, E'Eyjafjardarsysla', E'Eyjafjardarsysla');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (560, 77, E'Gullbringusysla', E'Gullbringusysla');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (561, 77, E'Myrasysla', E'Myrasysla');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (562, 77, E'None', E'None');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (563, 77, E'Nordur-Tingeyjarsysla', E'Nordur-Tingeyjarsysla');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (564, 77, E'Strandasysla', E'Strandasysla');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (565, 77, E'Sudur-Mulasysla', E'Sudur-Mulasysla');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (566, 77, E'Vestur-Bardastrandarsysla', E'Vestur-Bardastrandarsysla');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (567, 78, E'A Corua', E'A Corua');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (568, 78, E'lava', E'lava');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (569, 78, E'Albacete', E'Albacete');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (570, 78, E'Alicante/Alacant', E'Alicante/Alacant');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (571, 78, E'Almera', E'Almera');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (572, 78, E'Asturias', E'Asturias');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (573, 78, E'vila', E'vila');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (574, 78, E'Badajoz', E'Badajoz');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (575, 78, E'Barcelona', E'Barcelona');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (576, 78, E'Burgos', E'Burgos');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (577, 78, E'Cceres', E'Cceres');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (578, 78, E'Cdiz', E'Cdiz');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (579, 78, E'Cantabria', E'Cantabria');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (580, 78, E'Castelln/Castell', E'Castelln/Castell');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (581, 78, E'Ceuta', E'Ceuta');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (582, 78, E'Ciudad Real', E'Ciudad Real');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (583, 78, E'Crdoba', E'Crdoba');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (584, 78, E'Cuenca', E'Cuenca');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (585, 78, E'Girona', E'Girona');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (586, 78, E'Granada', E'Granada');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (587, 78, E'Guadalajara', E'Guadalajara');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (588, 78, E'Guipzcoa', E'Guipzcoa');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (589, 78, E'Huelva', E'Huelva');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (590, 78, E'Huesca', E'Huesca');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (591, 78, E'Illes Balears', E'Illes Balears');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (592, 78, E'Jan', E'Jan');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (593, 78, E'La Rioja', E'La Rioja');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (594, 78, E'Las Palmas', E'Las Palmas');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (595, 78, E'Len', E'Len');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (596, 78, E'Lleida', E'Lleida');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (597, 78, E'Lugo', E'Lugo');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (598, 78, E'Madrid', E'Madrid');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (599, 78, E'Mlaga', E'Mlaga');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (600, 78, E'Melilla', E'Melilla');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (601, 78, E'Murcia', E'Murcia');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (602, 78, E'Navarra', E'Navarra');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (603, 78, E'Ourense', E'Ourense');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (604, 78, E'Palencia', E'Palencia');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (605, 78, E'Pontevedra', E'Pontevedra');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (606, 78, E'Salamanca', E'Salamanca');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (607, 78, E'Santa Cruz de Tenerife', E'Santa Cruz de Tenerife');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (608, 78, E'Segovia', E'Segovia');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (609, 78, E'Sevilla', E'Sevilla');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (610, 78, E'Soria', E'Soria');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (611, 78, E'Tarragona', E'Tarragona');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (612, 78, E'Teruel', E'Teruel');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (613, 78, E'Toledo', E'Toledo');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (614, 78, E'Valncia', E'Valncia');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (615, 78, E'Valladolid', E'Valladolid');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (616, 78, E'Vizcaya', E'Vizcaya');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (617, 78, E'Zamora', E'Zamora');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (618, 78, E'Zaragoza', E'Zaragoza');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (619, 79, E'Abruzzo - Chieti', E'Abruzzo - Chieti');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (620, 79, E'Abruzzo - L''Aquila', E'Abruzzo - L''Aquila');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (621, 79, E'Abruzzo - Pescara', E'Abruzzo - Pescara');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (622, 79, E'Abruzzo - Teramo', E'Abruzzo - Teramo');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (623, 79, E'Basilicata - Matera', E'Basilicata - Matera');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (624, 79, E'Basilicata - Potenza', E'Basilicata - Potenza');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (625, 79, E'Calabria - Catanzaro', E'Calabria - Catanzaro');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (626, 79, E'Calabria - Cosenza', E'Calabria - Cosenza');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (627, 79, E'Calabria - Crotone', E'Calabria - Crotone');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (628, 79, E'Calabria - Reggio Calabria', E'Calabria - Reggio Calabria');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (629, 79, E'Calabria - Vibo Valentia', E'Calabria - Vibo Valentia');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (630, 79, E'Campania - Avellino', E'Campania - Avellino');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (631, 79, E'Campania - Benevento', E'Campania - Benevento');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (632, 79, E'Campania - Caserta', E'Campania - Caserta');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (633, 79, E'Campania - Napoli', E'Campania - Napoli');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (634, 79, E'Campania - Salerno', E'Campania - Salerno');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (635, 79, E'Emilia Romagna - Bologna', E'Emilia Romagna - Bologna');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (636, 79, E'Emilia Romagna - Ferrara', E'Emilia Romagna - Ferrara');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (637, 79, E'Emilia Romagna - Forl-Cesena', E'Emilia Romagna - Forl-Cesena');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (638, 79, E'Emilia Romagna - Modena', E'Emilia Romagna - Modena');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (639, 79, E'Emilia Romagna - Parma', E'Emilia Romagna - Parma');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (640, 79, E'Emilia Romagna - Piacenza', E'Emilia Romagna - Piacenza');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (641, 79, E'Emilia Romagna - Ravenna', E'Emilia Romagna - Ravenna');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (642, 79, E'Emilia Romagna - Reggio Emilia', E'Emilia Romagna - Reggio Emilia');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (643, 79, E'Emilia Romagna - Rimini', E'Emilia Romagna - Rimini');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (644, 79, E'Friuli Venezia Giulia - Gorizia', E'Friuli Venezia Giulia - Gorizia');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (645, 79, E'Friuli Venezia Giulia - Pordenone', E'Friuli Venezia Giulia - Pordenone');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (646, 79, E'Friuli Venezia Giulia - Trieste', E'Friuli Venezia Giulia - Trieste');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (647, 79, E'Friuli Venezia Giulia - Udine', E'Friuli Venezia Giulia - Udine');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (648, 79, E'Lazio - Frosinone', E'Lazio - Frosinone');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (649, 79, E'Lazio - Latina', E'Lazio - Latina');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (650, 79, E'Lazio - Rieti', E'Lazio - Rieti');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (651, 79, E'Lazio - Roma', E'Lazio - Roma');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (652, 79, E'Lazio - Viterbo', E'Lazio - Viterbo');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (653, 79, E'Liguria - Genova', E'Liguria - Genova');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (654, 79, E'Liguria - Imperia', E'Liguria - Imperia');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (655, 79, E'Liguria - La Spezia', E'Liguria - La Spezia');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (656, 79, E'Liguria - Savona', E'Liguria - Savona');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (657, 79, E'Lombardia - Bergamo', E'Lombardia - Bergamo');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (658, 79, E'Lombardia - Brescia', E'Lombardia - Brescia');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (659, 79, E'Lombardia - Como', E'Lombardia - Como');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (660, 79, E'Lombardia - Cremona', E'Lombardia - Cremona');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (661, 79, E'Lombardia - Lecco', E'Lombardia - Lecco');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (662, 79, E'Lombardia - Lodi', E'Lombardia - Lodi');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (663, 79, E'Lombardia - Mantova', E'Lombardia - Mantova');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (664, 79, E'Lombardia - Milano', E'Lombardia - Milano');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (665, 79, E'Lombardia - Pavia', E'Lombardia - Pavia');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (666, 79, E'Lombardia - Sondrio', E'Lombardia - Sondrio');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (667, 79, E'Lombardia - Varese', E'Lombardia - Varese');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (668, 79, E'Marche - Ancona', E'Marche - Ancona');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (669, 79, E'Marche - Ascoli Piceno', E'Marche - Ascoli Piceno');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (670, 79, E'Marche - Macerata', E'Marche - Macerata');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (671, 79, E'Marche - Pesaro - Urbino', E'Marche - Pesaro - Urbino');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (672, 79, E'Molise - Campobasso', E'Molise - Campobasso');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (673, 79, E'Molise - Isernia', E'Molise - Isernia');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (674, 79, E'Piemonte - Alessandria', E'Piemonte - Alessandria');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (675, 79, E'Piemonte - Biella', E'Piemonte - Biella');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (676, 79, E'Piemonte - Cuneo', E'Piemonte - Cuneo');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (677, 79, E'Piemonte - Novara', E'Piemonte - Novara');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (678, 79, E'Piemonte - Torino', E'Piemonte - Torino');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (679, 79, E'Piemonte - Verbania', E'Piemonte - Verbania');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (680, 79, E'Piemonte - Vercelli', E'Piemonte - Vercelli');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (681, 79, E'Puglia - Bari', E'Puglia - Bari');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (682, 79, E'Puglia - Brindisi', E'Puglia - Brindisi');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (683, 79, E'Puglia - Foggia', E'Puglia - Foggia');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (684, 79, E'Puglia - Lecce', E'Puglia - Lecce');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (685, 79, E'Puglia - Taranto', E'Puglia - Taranto');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (686, 79, E'Sardegna - Cagliari', E'Sardegna - Cagliari');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (687, 79, E'Sardegna - Nuoro', E'Sardegna - Nuoro');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (688, 79, E'Sardegna - Oristano', E'Sardegna - Oristano');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (689, 79, E'Sardegna - Sassari', E'Sardegna - Sassari');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (690, 79, E'Sicilia - Agrigento', E'Sicilia - Agrigento');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (691, 79, E'Sicilia - Catania', E'Sicilia - Catania');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (692, 79, E'Sicilia - Messina', E'Sicilia - Messina');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (693, 79, E'Sicilia - Palermo', E'Sicilia - Palermo');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (694, 79, E'Sicilia - Ragusa', E'Sicilia - Ragusa');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (695, 79, E'Sicilia - Siracusa', E'Sicilia - Siracusa');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (696, 79, E'Sicilia - Trapani', E'Sicilia - Trapani');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (697, 79, E'Toscana - Arezzo', E'Toscana - Arezzo');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (698, 79, E'Toscana - Firenze', E'Toscana - Firenze');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (699, 79, E'Toscana - Grosseto', E'Toscana - Grosseto');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (700, 79, E'Toscana - Livorno', E'Toscana - Livorno');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (701, 79, E'Toscana - Lucca', E'Toscana - Lucca');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (702, 79, E'Toscana - Massa Carrara', E'Toscana - Massa Carrara');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (703, 79, E'Toscana - Pisa', E'Toscana - Pisa');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (704, 79, E'Toscana - Pistoia', E'Toscana - Pistoia');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (705, 79, E'Toscana - Prato', E'Toscana - Prato');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (706, 79, E'Toscana - Siena', E'Toscana - Siena');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (707, 79, E'Trentino Alto Adige - Bolzano', E'Trentino Alto Adige - Bolzano');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (708, 79, E'Trentino Alto Adige - Trento', E'Trentino Alto Adige - Trento');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (709, 79, E'Umbria - Perugia', E'Umbria - Perugia');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (710, 79, E'Umbria - Terni', E'Umbria - Terni');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (711, 79, E'Valle d''Aosta - Aosta', E'Valle d''Aosta - Aosta');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (712, 79, E'Veneto - Belluno', E'Veneto - Belluno');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (713, 79, E'Veneto - Padova', E'Veneto - Padova');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (714, 79, E'Veneto - Rovigo', E'Veneto - Rovigo');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (715, 79, E'Veneto - Treviso', E'Veneto - Treviso');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (716, 79, E'Veneto - Venezia', E'Veneto - Venezia');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (717, 79, E'Veneto - Verona', E'Veneto - Verona');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (718, 79, E'Veneto - Vicenza', E'Veneto - Vicenza');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (719, 80, E'�����', E'Yemen');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (720, 81, E'����-�����', E'Cape Verde');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (721, 82, E'����������� ���. (�������������� ���.)', E'Akmolinskaya obl. (Tselinogradskaya obl.)');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (722, 82, E'����������� ���.', E'Aktyubinskaya obl.');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (980, 137, E'Finnmark', E'Finnmark');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (723, 82, E'����-�������� ���.', E'Alma-Atinskaya obl.');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (724, 82, E'��������-������������� ���.', E'Vostochno-Kazahstanskaya obl.');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (725, 82, E'���������� ���.', E'Gurevskaya obl.');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (726, 82, E'�������������� ���.', E'Dzhezkazganskaya obl.');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (727, 82, E'���������� ���. (����������� ���.)', E'Zhambylskaya obl. (Dzhambulskaya obl.)');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (728, 82, E'�������-������������� ���.', E'Zapadno-Kazahstanskaya obl.');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (729, 82, E'���������', E'Kazahstan');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (730, 82, E'�������������� ���.', E'Karagandinskaya obl.');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (731, 82, E'������������� ���.', E'Kzylordinskaya obl.');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (732, 82, E'������������ ���.', E'Kokchetavskaya obl.');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (733, 82, E'������������ ���.', E'Kustanaiskaya obl.');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (734, 82, E'������������� (������������� ���.)', E'Mangystauskaya (Mangyshlakskaya obl.)');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (735, 82, E'������������ ���.', E'Pavlodarskaya obl.');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (736, 82, E'������-������������� ���.', E'Severo-Kazahstanskaya obl.');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (737, 82, E'�����-���������� ���.', E'Taldy-Kurganskaya obl.');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (738, 82, E'���������� ���.', E'Turgaiskaya obl.');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (739, 82, E'����������� ���.', E'Chimkentskaya obl.');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (740, 83, E'��������', E'Cambodia');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (741, 84, E'Littoral', E'Littoral');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (742, 84, E'Sudouest', E'Sudouest');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (743, 84, E'��������', E'North');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (744, 84, E'�����������', E'Central');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (745, 85, E'Alberta', E'Alberta');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (746, 85, E'British Columbia', E'British Columbia');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (747, 85, E'Manitoba', E'Manitoba');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (748, 85, E'New Brunswick', E'New Brunswick');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (749, 85, E'Newfoundland', E'Newfoundland');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (750, 85, E'Northwest Territories', E'Northwest Territories');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (751, 85, E'Nova Scotia', E'Nova Scotia');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (752, 85, E'Nunavut', E'Nunavut');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (753, 85, E'Ontario', E'Ontario');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (754, 85, E'Prince Edward Island', E'Prince Edward Island');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (755, 85, E'Quebec', E'Quebec');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (756, 85, E'Saskatchewan', E'Saskatchewan');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (757, 85, E'Yukon Territory', E'Yukon Territory');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (758, 86, E'����', E'Doha');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (759, 87, E'Central', E'Central');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (760, 87, E'Coast', E'Coast');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (761, 87, E'Nairobi Area', E'Nairobi Area');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (762, 87, E'North-Eastern', E'North-Eastern');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (763, 87, E'Western', E'Western');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (764, 88, E'Government controlled area', E'Government controlled area');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (765, 88, E'Turkish controlled area', E'Turkish controlled area');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (766, 89, E'None', E'None');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (767, 90, E'Anhui', E'Anhui');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (768, 90, E'Beijing', E'Beijing');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (769, 90, E'Chongqing', E'Chongqing');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (770, 90, E'Fujian', E'Fujian');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (771, 90, E'Gansu', E'Gansu');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (772, 90, E'Guangdong', E'Guangdong');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (773, 90, E'Guangxi', E'Guangxi');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (774, 90, E'Guizhou', E'Guizhou');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (775, 90, E'Hainan', E'Hainan');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (776, 90, E'Hebei', E'Hebei');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (777, 90, E'Heilongjiang', E'Heilongjiang');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (778, 90, E'Henan', E'Henan');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (779, 90, E'Hubei', E'Hubei');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (780, 90, E'Hunan', E'Hunan');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (781, 90, E'Jiangsu', E'Jiangsu');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (782, 90, E'Jiangxi', E'Jiangxi');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (783, 90, E'Jilin', E'Jilin');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (784, 90, E'Liaoning', E'Liaoning');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (785, 90, E'Nei Mongol', E'Nei Mongol');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (786, 90, E'Ningxia', E'Ningxia');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (787, 90, E'Qinghai', E'Qinghai');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (788, 90, E'Shaanxi', E'Shaanxi');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (789, 90, E'Shandong', E'Shandong');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (790, 90, E'Shanghai', E'Shanghai');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (791, 90, E'Shanxi', E'Shanxi');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (792, 90, E'Sichuan', E'Sichuan');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (793, 90, E'Tianjin', E'Tianjin');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (794, 90, E'Xinjiang', E'Xinjiang');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (795, 90, E'Yunnan', E'Yunnan');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (796, 90, E'Zhejiang', E'Zhejiang');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (797, 91, E'Amazonas', E'Amazonas');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (798, 91, E'Antioquia', E'Antioquia');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (799, 91, E'Atlntico', E'Atlntico');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (800, 91, E'Casanare', E'Casanare');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (801, 91, E'Cauca', E'Cauca');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (802, 91, E'Csar', E'Csar');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (803, 91, E'Choc', E'Choc');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (804, 91, E'Crdoba', E'Crdoba');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (805, 91, E'Cundinamarca', E'Cundinamarca');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (806, 91, E'Distrito Especial', E'Distrito Especial');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (807, 91, E'Huila', E'Huila');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (808, 91, E'La Guajira', E'La Guajira');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (809, 91, E'Meta', E'Meta');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (810, 91, E'Narino', E'Narino');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (811, 91, E'Norte de Santander', E'Norte de Santander');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (812, 91, E'Quindo', E'Quindo');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (813, 91, E'Risaralda', E'Risaralda');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (814, 91, E'Santander', E'Santander');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (815, 91, E'Tolima', E'Tolima');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (816, 92, E'���������', E'Comoros');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (817, 93, E'����� (Brazzaville)', E'Congo (Brazzaville)');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (818, 94, E'�����', E'Congo');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (819, 95, E'Alajuela', E'Alajuela');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (820, 95, E'Cartago', E'Cartago');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (821, 95, E'Guanacaste', E'Guanacaste');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (822, 95, E'Heredia', E'Heredia');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (823, 95, E'Limn', E'Limn');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (824, 95, E'Puntarenas', E'Puntarenas');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (825, 95, E'San Jos', E'San Jos');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (826, 96, E'���-�''�����', E'Cote D''Ivoire');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (827, 97, E'Camaguey', E'Camaguey');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (828, 97, E'Ciego de Avila', E'Ciego de Avila');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (829, 97, E'Ciudad de la Habana', E'Ciudad de la Habana');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (830, 97, E'Granma', E'Granma');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (831, 97, E'Guantanamo', E'Guantanamo');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (832, 97, E'Holguin', E'Holguin');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (833, 97, E'Isla de la Juventud', E'Isla de la Juventud');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (834, 97, E'La Habana', E'La Habana');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (835, 97, E'Las Tunas', E'Las Tunas');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (836, 97, E'Matanzas', E'Matanzas');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (837, 97, E'None', E'None');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (838, 97, E'Pinar del Rio', E'Pinar del Rio');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (839, 97, E'Sancti Spiritus', E'Sancti Spiritus');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (840, 97, E'Santiago de Cuba', E'Santiago de Cuba');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (841, 97, E'Villa Clara', E'Villa Clara');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (842, 98, E'al-Jahra', E'al-Jahra');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (843, 98, E'al-Kuwayt', E'al-Kuwayt');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (844, 99, E'������', E'������');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (845, 100, E'�����-�������� ���.', E'�����-�������� ���.');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (846, 100, E'����������', E'����������');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (847, 100, E'��������� ���.', E'��������� ���.');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (848, 100, E'������ ���.', E'������ ���.');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (849, 100, E'��������� ���.', E'��������� ���.');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (850, 101, E'����', E'Laos');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (851, 102, E'������', E'Latviya');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (852, 103, E'������', E'Lesotho');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (853, 104, E'�������', E'Liberia');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (854, 105, E'������', E'Beirut');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (855, 106, E'Tarabulus', E'Tarabulus');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (856, 106, E'�������', E'Bengasi');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (857, 107, E'�����', E'Litva');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (858, 108, E'Balzers', E'Balzers');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (859, 108, E'Eschen', E'Eschen');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (860, 108, E'Ruggell', E'Ruggell');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (861, 108, E'Schellenberg', E'Schellenberg');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (862, 108, E'Triesen', E'Triesen');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (863, 108, E'Vaduz', E'Vaduz');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (864, 109, E'Diekirch', E'Diekirch');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (865, 109, E'Grevenmacher', E'Grevenmacher');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (866, 109, E'Luxembourg', E'Luxembourg');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (867, 109, E'None', E'None');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (868, 110, E'��������', E'Mauritius');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (869, 111, E'����������', E'Mauritania');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (870, 112, E'����������', E'Madagascar');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (871, 113, E'Aracinovo', E'Aracinovo');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (872, 113, E'Berovo', E'Berovo');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (873, 113, E'Bitola', E'Bitola');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (874, 113, E'Bosilovo', E'Bosilovo');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (875, 113, E'Dolna Banjica', E'Dolna Banjica');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (876, 113, E'Drugovo', E'Drugovo');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (877, 113, E'Gevgelija', E'Gevgelija');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (878, 113, E'Gostivar', E'Gostivar');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (879, 113, E'Karpos', E'Karpos');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (880, 113, E'Kavadarci', E'Kavadarci');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (881, 113, E'Kisela Voda', E'Kisela Voda');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (882, 113, E'Kumanovo', E'Kumanovo');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (883, 113, E'Makedonska Kamenica', E'Makedonska Kamenica');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (884, 113, E'Negotino', E'Negotino');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (885, 113, E'Ohrid', E'Ohrid');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (886, 113, E'Prilep', E'Prilep');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (887, 113, E'Radovis', E'Radovis');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (888, 113, E'Saraj', E'Saraj');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (889, 113, E'Star Dojran', E'Star Dojran');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (890, 113, E'Struga', E'Struga');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (891, 113, E'Strumica', E'Strumica');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (892, 113, E'Sveti Nikole', E'Sveti Nikole');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (893, 113, E'Tetovo', E'Tetovo');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (894, 113, E'Veles', E'Veles');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (895, 114, E'������', E'Malawi');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (896, 115, E'��������', E'Malaysia');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (897, 116, E'����', E'Mali');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (898, 117, E'����������� �-��', E'Maldives');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (899, 118, E'Malta', E'Malta');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (900, 119, E'���-��-�����', E'Fort-de-France');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (901, 120, E'Aguascalientes', E'Aguascalientes');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (902, 120, E'Baja California', E'Baja California');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (903, 120, E'Baja California Sur', E'Baja California Sur');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (904, 120, E'Campeche', E'Campeche');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (905, 120, E'Chiapas', E'Chiapas');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (906, 120, E'Chihuahua', E'Chihuahua');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (907, 120, E'Coahuila de Zaragoza', E'Coahuila de Zaragoza');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (908, 120, E'Colima', E'Colima');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (909, 120, E'Distrito Federal', E'Distrito Federal');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (910, 120, E'Durango', E'Durango');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (911, 120, E'Guanajuato', E'Guanajuato');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (912, 120, E'Guerrero', E'Guerrero');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (913, 120, E'Hidalgo', E'Hidalgo');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (914, 120, E'Jalisco', E'Jalisco');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (915, 120, E'Mxico', E'Mxico');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (916, 120, E'Michoacn de Ocampo', E'Michoacn de Ocampo');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (917, 120, E'Morelos', E'Morelos');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (918, 120, E'Nayarit', E'Nayarit');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (919, 120, E'None', E'None');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (920, 120, E'Nuevo Len', E'Nuevo Len');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (921, 120, E'Oaxaca', E'Oaxaca');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (922, 120, E'Puebla', E'Puebla');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (923, 120, E'Quertaro de Arteaga', E'Quertaro de Arteaga');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (924, 120, E'Quintana Roo', E'Quintana Roo');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (925, 120, E'San Luis Potos', E'San Luis Potos');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (926, 120, E'Sinaloa', E'Sinaloa');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (927, 120, E'Sonora', E'Sonora');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (928, 120, E'Tabasco', E'Tabasco');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (929, 120, E'Tamaulipas', E'Tamaulipas');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (930, 120, E'Tlaxcala', E'Tlaxcala');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (931, 120, E'Veracruz-Llave', E'Veracruz-Llave');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (932, 120, E'Yucatn', E'Yucatn');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (933, 121, E'��������', E'Mozambique');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (934, 122, E'�������', E'Moldova');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (935, 123, E'Monaco', E'Monaco');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (936, 124, E'����-�����', E'Ulan Bator');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (937, 125, E'�������', E'Morocco');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (938, 125, E'������', E'Tangier');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (939, 126, E'������ (�����)', E'Myanmar (Burma)');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (940, 127, E'������ ���', E'������ ���');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (941, 128, E'�������', E'Namibia');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (942, 129, E'�����', E'Nauru');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (943, 130, E'�����', E'Nepal');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (944, 131, E'�����', E'Niger');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (945, 132, E'�������', E'Nigeria');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (946, 133, E'Drenthe', E'Drenthe');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (947, 133, E'Flevoland', E'Flevoland');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (948, 133, E'Friesland', E'Friesland');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (949, 133, E'Gelderland', E'Gelderland');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (950, 133, E'Groningen', E'Groningen');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (951, 133, E'Limburg', E'Limburg');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (952, 133, E'Noord-Brabant', E'Noord-Brabant');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (953, 133, E'Noord-Holland', E'Noord-Holland');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (954, 133, E'Overijssel', E'Overijssel');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (955, 133, E'Utrecht', E'Utrecht');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (956, 133, E'Zeeland', E'Zeeland');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (957, 133, E'Zuid-Holland', E'Zuid-Holland');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (958, 134, E'Granada', E'Granada');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (959, 134, E'Len', E'Len');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (960, 134, E'Managua', E'Managua');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (961, 134, E'Rio San Juan', E'Rio San Juan');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (962, 135, E'Auckland', E'Auckland');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (963, 135, E'Bay of Plenty', E'Bay of Plenty');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (964, 135, E'Canterbury', E'Canterbury');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (965, 135, E'Gisborne', E'Gisborne');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (966, 135, E'Manawatu-Wanganui', E'Manawatu-Wanganui');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (967, 135, E'Marlborough', E'Marlborough');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (968, 135, E'Nelson', E'Nelson');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (969, 135, E'Northland', E'Northland');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (970, 135, E'Otago', E'Otago');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (971, 135, E'Southland', E'Southland');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (972, 135, E'Taranaki', E'Taranaki');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (973, 135, E'Tasman', E'Tasman');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (974, 135, E'Wellington', E'Wellington');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (975, 135, E'West Coast', E'West Coast');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (976, 136, E'�����', E'Noumea');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (977, 137, E'Akershus', E'Akershus');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (978, 137, E'Aust-Agder', E'Aust-Agder');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (979, 137, E'Buskerud', E'Buskerud');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (981, 137, E'Hedmark', E'Hedmark');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (982, 137, E'Hordaland', E'Hordaland');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (983, 137, E'More og Romsdal', E'More og Romsdal');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (984, 137, E'None', E'None');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (985, 137, E'Nord-Trondelag', E'Nord-Trondelag');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (986, 137, E'Nordland', E'Nordland');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (987, 137, E'Oppland', E'Oppland');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (988, 137, E'Oslo', E'Oslo');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (989, 137, E'Ostfold', E'Ostfold');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (990, 137, E'Rogaland', E'Rogaland');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (991, 137, E'Sogn og Fjordane', E'Sogn og Fjordane');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (992, 137, E'Sor-Trondelag', E'Sor-Trondelag');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (993, 137, E'Telemark', E'Telemark');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (994, 137, E'Troms', E'Troms');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (995, 137, E'Vest-Agder', E'Vest-Agder');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (996, 137, E'Vestfold', E'Vestfold');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (997, 138, E'��������', E'Kingston');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (998, 139, E'��� ����', E'Abu Dhabi');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (999, 139, E'�����', E'Dubai');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1000, 140, E'����', E'Oman');

COMMIT;

BEGIN;

/* Data for the 'profiledb.region' table  (Records 1001 - 1500) */

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1001, 141, E'��������', E'Pakistan');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1002, 142, E'Chiriqu', E'Chiriqu');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1003, 142, E'Cocl', E'Cocl');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1004, 142, E'Herrera', E'Herrera');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1005, 142, E'Los Santos', E'Los Santos');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1006, 142, E'Panam', E'Panam');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1007, 143, E'����� ����� ������', E'Papua New Guinea');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1008, 144, E'Alto Paran', E'Alto Paran');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1009, 144, E'Central', E'Central');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1010, 144, E'Cordillera', E'Cordillera');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1011, 144, E'Paraguar', E'Paraguar');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1012, 144, E'San Pedro', E'San Pedro');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1013, 145, E'Amazonas', E'Amazonas');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1014, 145, E'Ancash', E'Ancash');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1015, 145, E'Arequipa', E'Arequipa');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1016, 145, E'Ayacucho', E'Ayacucho');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1017, 145, E'Cajamarca', E'Cajamarca');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1018, 145, E'Callao', E'Callao');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1019, 145, E'Cusco', E'Cusco');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1020, 145, E'Hunuco', E'Hunuco');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1021, 145, E'Ica', E'Ica');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1022, 145, E'Junn', E'Junn');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1023, 145, E'La Libertad', E'La Libertad');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1024, 145, E'Lambayeque', E'Lambayeque');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1025, 145, E'Lima', E'Lima');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1026, 145, E'Loreto', E'Loreto');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1027, 145, E'Madre de Dios', E'Madre de Dios');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1028, 145, E'None', E'None');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1029, 145, E'Pasco', E'Pasco');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1030, 145, E'Piura', E'Piura');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1031, 145, E'Puno', E'Puno');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1032, 145, E'San Martn', E'San Martn');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1033, 145, E'Tacna', E'Tacna');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1034, 145, E'Tumbes', E'Tumbes');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1035, 145, E'Ucayali', E'Ucayali');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1036, 146, E'���������', E'Adamstown');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1037, 147, E'Biala Podlaska', E'Biala Podlaska');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1038, 147, E'Bialystok', E'Bialystok');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1039, 147, E'Bielsko', E'Bielsko');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1040, 147, E'Bydgoszcz', E'Bydgoszcz');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1041, 147, E'Chelm', E'Chelm');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1042, 147, E'Czestochowa', E'Czestochowa');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1043, 147, E'Dolnoslaskie', E'Dolnoslaskie');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1044, 147, E'Elblag', E'Elblag');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1045, 147, E'Gdansk', E'Gdansk');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1046, 147, E'Gorzow', E'Gorzow');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1047, 147, E'Jelenia Gora', E'Jelenia Gora');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1048, 147, E'Kalisz', E'Kalisz');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1049, 147, E'Katowice', E'Katowice');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1050, 147, E'Kielce', E'Kielce');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1051, 147, E'Konin', E'Konin');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1052, 147, E'Koszalin', E'Koszalin');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1053, 147, E'Krakow', E'Krakow');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1054, 147, E'Kujawsko-Pomorskie', E'Kujawsko-Pomorskie');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1055, 147, E'Legnica', E'Legnica');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1056, 147, E'Leszno', E'Leszno');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1057, 147, E'Lodz', E'Lodz');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1058, 147, E'Lodzkie', E'Lodzkie');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1059, 147, E'Lomza', E'Lomza');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1060, 147, E'Lublin', E'Lublin');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1061, 147, E'Lubuskie', E'Lubuskie');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1062, 147, E'Malopolskie', E'Malopolskie');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1063, 147, E'Mazowieckie', E'Mazowieckie');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1064, 147, E'None', E'None');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1065, 147, E'Nowy Sacz', E'Nowy Sacz');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1066, 147, E'Olsztyn', E'Olsztyn');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1067, 147, E'Opole', E'Opole');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1068, 147, E'Ostroleka', E'Ostroleka');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1069, 147, E'Pila', E'Pila');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1070, 147, E'Piotrkow', E'Piotrkow');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1071, 147, E'Plock', E'Plock');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1072, 147, E'Podkarpackie', E'Podkarpackie');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1073, 147, E'Pomorskie', E'Pomorskie');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1074, 147, E'Poznan', E'Poznan');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1075, 147, E'Przemysl', E'Przemysl');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1076, 147, E'Radom', E'Radom');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1077, 147, E'Rzeszow', E'Rzeszow');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1078, 147, E'Siedlce', E'Siedlce');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1079, 147, E'Sieradz', E'Sieradz');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1080, 147, E'Skierniewice', E'Skierniewice');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1081, 147, E'Slaskie', E'Slaskie');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1082, 147, E'Slupsk', E'Slupsk');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1083, 147, E'Suwalki', E'Suwalki');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1084, 147, E'Swietokrzyskie', E'Swietokrzyskie');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1085, 147, E'Tarnobrzeg', E'Tarnobrzeg');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1086, 147, E'Tarnow', E'Tarnow');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1087, 147, E'Torun', E'Torun');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1088, 147, E'Walbrzych', E'Walbrzych');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1089, 147, E'Warminsko-Mazurskie', E'Warminsko-Mazurskie');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1090, 147, E'Warszawa', E'Warszawa');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1091, 147, E'Wielkopolskie', E'Wielkopolskie');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1092, 147, E'Wloclawek', E'Wloclawek');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1093, 147, E'Wroclaw', E'Wroclaw');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1094, 147, E'Zachodniopomorskie', E'Zachodniopomorskie');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1095, 147, E'Zamosc', E'Zamosc');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1096, 147, E'Zielona Gora', E'Zielona Gora');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1097, 148, E'Aveiro', E'Aveiro');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1098, 148, E'Azores', E'Azores');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1099, 148, E'Beja', E'Beja');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1100, 148, E'Braga', E'Braga');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1101, 148, E'Braganca', E'Braganca');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1102, 148, E'Castelo Branco', E'Castelo Branco');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1103, 148, E'Coimbra', E'Coimbra');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1104, 148, E'Evora', E'Evora');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1105, 148, E'Faro', E'Faro');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1106, 148, E'Guarda', E'Guarda');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1107, 148, E'Leiria', E'Leiria');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1108, 148, E'Lisboa', E'Lisboa');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1109, 148, E'Madeira', E'Madeira');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1110, 148, E'None', E'None');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1111, 148, E'Portalegre', E'Portalegre');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1112, 148, E'Porto', E'Porto');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1113, 148, E'Santarem', E'Santarem');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1114, 148, E'Setubal', E'Setubal');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1115, 148, E'Viana do Castelo', E'Viana do Castelo');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1116, 148, E'Vila Real', E'Vila Real');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1117, 148, E'Viseu', E'Viseu');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1118, 149, E'Puerto Rico', E'Puerto Rico');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1119, 150, E'Saint-Denis', E'Saint-Denis');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1120, 151, E'������', E'Rwanda');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1121, 152, E'Alba', E'Alba');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1122, 152, E'Arad', E'Arad');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1123, 152, E'Arges', E'Arges');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1124, 152, E'Bacau', E'Bacau');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1125, 152, E'Bihor', E'Bihor');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1126, 152, E'Bistrita-Nasaud', E'Bistrita-Nasaud');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1127, 152, E'Botosani', E'Botosani');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1128, 152, E'Braila', E'Braila');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1129, 152, E'Brasov', E'Brasov');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1130, 152, E'Bucuresti', E'Bucuresti');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1131, 152, E'Buzau', E'Buzau');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1132, 152, E'Calarasi', E'Calarasi');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1133, 152, E'Caras-Severin', E'Caras-Severin');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1134, 152, E'Cluj', E'Cluj');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1135, 152, E'Constanta', E'Constanta');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1136, 152, E'Covasna', E'Covasna');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1137, 152, E'Dambovita', E'Dambovita');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1138, 152, E'Dolj', E'Dolj');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1139, 152, E'Galati', E'Galati');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1140, 152, E'Giurgiu', E'Giurgiu');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1141, 152, E'Gorj', E'Gorj');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1142, 152, E'Harghita', E'Harghita');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1143, 152, E'Hunedoara', E'Hunedoara');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1144, 152, E'Ialomita', E'Ialomita');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1145, 152, E'Iasi', E'Iasi');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1146, 152, E'Ilfov', E'Ilfov');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1147, 152, E'Maramures', E'Maramures');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1148, 152, E'Mehedinti', E'Mehedinti');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1149, 152, E'Mures', E'Mures');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1150, 152, E'Neamt', E'Neamt');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1151, 152, E'Olt', E'Olt');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1152, 152, E'Prahova', E'Prahova');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1153, 152, E'Salaj', E'Salaj');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1154, 152, E'Satu Mare', E'Satu Mare');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1155, 152, E'Sibiu', E'Sibiu');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1156, 152, E'Suceava', E'Suceava');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1157, 152, E'Teleorman', E'Teleorman');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1158, 152, E'Timis', E'Timis');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1159, 152, E'Tulcea', E'Tulcea');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1160, 152, E'Valcea', E'Valcea');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1161, 152, E'Vaslui', E'Vaslui');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1162, 152, E'Vrancea', E'Vrancea');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1163, 153, E'Alabama', E'Alabama');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1164, 153, E'Alaska', E'Alaska');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1165, 153, E'American Samoa', E'American Samoa');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1166, 153, E'Arizona', E'Arizona');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1167, 153, E'Arkansas', E'Arkansas');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1168, 153, E'California', E'California');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1169, 153, E'Colorado', E'Colorado');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1170, 153, E'Connecticut', E'Connecticut');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1171, 153, E'Delaware', E'Delaware');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1172, 153, E'District of Columbia', E'District of Columbia');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1173, 153, E'Florida', E'Florida');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1174, 153, E'Georgia', E'Georgia');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1175, 153, E'Hawaii', E'Hawaii');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1176, 153, E'Idaho', E'Idaho');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1177, 153, E'Illinois', E'Illinois');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1178, 153, E'Indiana', E'Indiana');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1179, 153, E'Iowa', E'Iowa');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1180, 153, E'Kansas', E'Kansas');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1181, 153, E'Kentucky', E'Kentucky');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1182, 153, E'Louisiana', E'Louisiana');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1183, 153, E'Maine', E'Maine');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1184, 153, E'Maryland', E'Maryland');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1185, 153, E'Massachusetts', E'Massachusetts');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1186, 153, E'Michigan', E'Michigan');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1187, 153, E'Minnesota', E'Minnesota');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1188, 153, E'Mississippi', E'Mississippi');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1189, 153, E'Missouri', E'Missouri');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1190, 153, E'Montana', E'Montana');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1191, 153, E'Nebraska', E'Nebraska');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1192, 153, E'Nevada', E'Nevada');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1193, 153, E'New Hampshire', E'New Hampshire');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1194, 153, E'New Jersey', E'New Jersey');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1195, 153, E'New Mexico', E'New Mexico');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1196, 153, E'New York', E'New York');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1197, 153, E'North Carolina', E'North Carolina');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1198, 153, E'North Dakota', E'North Dakota');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1199, 153, E'Ohio', E'Ohio');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1200, 153, E'Oklahoma', E'Oklahoma');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1201, 153, E'Oregon', E'Oregon');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1202, 153, E'Pennsylvania', E'Pennsylvania');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1203, 153, E'Rhode Island', E'Rhode Island');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1204, 153, E'South Carolina', E'South Carolina');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1205, 153, E'South Dakota', E'South Dakota');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1206, 153, E'Tennessee', E'Tennessee');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1207, 153, E'Texas', E'Texas');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1208, 153, E'Utah', E'Utah');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1209, 153, E'Vermont', E'Vermont');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1210, 153, E'Virgin Islands', E'Virgin Islands');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1211, 153, E'Virginia', E'Virginia');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1212, 153, E'Washington', E'Washington');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1213, 153, E'West Virginia', E'West Virginia');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1214, 153, E'Wisconsin', E'Wisconsin');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1215, 153, E'Wyoming', E'Wyoming');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1216, 154, E'Ahuachapan', E'Ahuachapan');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1217, 154, E'La Libertad', E'La Libertad');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1218, 154, E'La Paz', E'La Paz');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1219, 154, E'La Union', E'La Union');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1220, 154, E'San Miguel', E'San Miguel');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1221, 154, E'San Salvador', E'San Salvador');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1222, 155, E'�����', E'Samoa');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1223, 156, E'Acquaviva', E'Acquaviva');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1224, 156, E'Chiesanuova', E'Chiesanuova');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1225, 156, E'Serravalle', E'Serravalle');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1226, 157, E'���-����', E'San Tome');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1227, 158, E'���������� ������', E'Saudi Arabia');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1228, 159, E'���������', E'Swaziland');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1229, 160, E'������', E'Castries');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1230, 161, E'����������', E'Jamestown');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1231, 162, E'Korea', E'Korea');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1232, 163, E'��������', E'Seychelles');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1233, 164, E'���-����', E'Saint Pierre');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1234, 165, E'�������', E'Senegal');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1235, 166, E'������', E'Basseterre');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1236, 167, E'���������', E'Kingstown');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1237, 168, E'���������', E'Vojvodina');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1238, 168, E'������ � �������', E'Kosovo and Metohija');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1239, 168, E'����������� ������', E'Central Serbia');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1240, 169, E'��������', E'Singapore');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1241, 170, E'������', E'Damascus');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1242, 171, E'Bratislava', E'Bratislava');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1243, 171, E'Kosice', E'Kosice');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1244, 171, E'Nitra', E'Nitra');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1245, 171, E'None', E'None');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1246, 171, E'Presov', E'Presov');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1247, 171, E'Trencin', E'Trencin');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1248, 171, E'Trnava', E'Trnava');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1249, 172, E'Bohinj', E'Bohinj');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1250, 172, E'Brezovica', E'Brezovica');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1251, 172, E'None', E'None');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1252, 173, E'���������� �-��', E'Solomon Islands');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1253, 174, E'��������', E'Mogadishu');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1254, 175, E'�����', E'Sudan');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1255, 176, E'Paramaribo', E'Paramaribo');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1256, 177, E'�������', E'Freetown');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1257, 178, E'�����-������������ ���.', E'�����-������������ ���.');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1258, 178, E'��������� ���.', E'��������� ���.');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1259, 178, E'������-��������� ���.', E'������-��������� ���.');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1260, 178, E'���������� ���.', E'���������� ���.');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1261, 178, E'�����������', E'�����������');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1262, 179, E'�������', E'Taiwan');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1263, 180, E'���������', E'���������');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1264, 180, E'�������', E'Thailand');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1265, 180, E'��� ����', E'��� ����');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1266, 181, E'��������', E'Tanzania');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1267, 182, E'����', E'Togo');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1268, 183, E'�������', E'Fakaofo');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1269, 184, E'�����', E'Tonga');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1270, 185, E'����-��-�����', E'Port of Spain');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1271, 186, E'������', E'Tuvalu');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1272, 187, E'Tunisia', E'Tunisia');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1273, 188, E'����������� ���.', E'����������� ���.');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1274, 188, E'������������� ���.', E'������������� ���.');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1275, 188, E'��������� ���.', E'��������� ���.');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1276, 188, E'���������� ���.', E'���������� ���.');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1277, 188, E'����������� ���.', E'����������� ���.');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1278, 189, E'Grand Turk', E'Grand Turk');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1279, 190, E'Bartin', E'Bartin');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1280, 190, E'Bayburt', E'Bayburt');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1281, 190, E'Karabuk', E'Karabuk');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1282, 190, E'�����', E'Adana');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1283, 190, E'�����', E'Aydin');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1284, 190, E'������', E'Amasya');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1285, 190, E'������', E'Ankara');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1286, 190, E'�������', E'Antalya');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1287, 190, E'������', E'Artvin');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1288, 190, E'�����', E'Afion');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1289, 190, E'���������', E'Balikesir');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1290, 190, E'��������', E'Bilecik');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1291, 190, E'�����', E'Bursa');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1292, 190, E'���������', E'Gaziantep');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1293, 190, E'�������', E'Denizli');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1294, 190, E'�����', E'Izmir');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1295, 190, E'�������', E'Isparta');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1296, 190, E'�����', E'Icel');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1297, 190, E'�������', E'Kayseri');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1298, 190, E'����', E'Kars');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1299, 190, E'��������', E'Kodjaeli');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1300, 190, E'�����', E'Konya');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1301, 190, E'����������', E'Kirklareli');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1302, 190, E'�������', E'Kutahya');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1303, 190, E'�������', E'Malatya');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1304, 190, E'������', E'Manisa');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1305, 190, E'�������', E'Sakarya');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1306, 190, E'������', E'Samsun');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1307, 190, E'�����', E'Sivas');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1308, 190, E'�������', E'Istanbul');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1309, 190, E'�����', E'Corum');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1310, 190, E'������', E'Edirne');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1311, 190, E'������', E'Elazig');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1312, 190, E'���������', E'Erzincan');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1313, 190, E'�������', E'Erzurum');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1314, 190, E'���������', E'Eskisehir');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1315, 191, E'Jinja', E'Jinja');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1316, 191, E'Kampala', E'Kampala');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1317, 192, E'����������� ���.', E'����������� ���.');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1318, 192, E'��������� ���.', E'��������� ���.');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1319, 192, E'���������� ���.', E'���������� ���.');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1320, 192, E'������������', E'������������');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1321, 192, E'��������������� ���.', E'��������������� ���.');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1322, 192, E'���������� ���.', E'���������� ���.');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1323, 192, E'������������ ���.', E'������������ ���.');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1324, 192, E'������������� ���.', E'������������� ���.');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1325, 192, E'���������������� ���.', E'���������������� ���.');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1326, 192, E'������������� ���.', E'������������� ���.');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1327, 192, E'����������� ���.', E'����������� ���.');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1328, 192, E'���������� ���.', E'���������� ���.');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1329, 192, E'���������� ���.', E'���������� ���.');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1330, 193, E'Canelones', E'Canelones');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1331, 193, E'Colonia', E'Colonia');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1332, 193, E'Lavalleja', E'Lavalleja');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1333, 193, E'Maldonado', E'Maldonado');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1334, 193, E'Montevideo', E'Montevideo');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1335, 193, E'Paysand', E'Paysand');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1336, 193, E'Ro Negro', E'Ro Negro');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1337, 193, E'Rivera', E'Rivera');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1338, 193, E'Salto', E'Salto');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1339, 193, E'Tacuaremb', E'Tacuaremb');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1340, 193, E'Treinta y Tres', E'Treinta y Tres');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1341, 194, E'��������', E'��������');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1342, 195, E'�����', E'Fiji');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1343, 196, E'���������', E'Philippines');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1344, 197, E'�land', E'�land');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1345, 197, E'Eastern Finland', E'Eastern Finland');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1346, 197, E'Lapland', E'Lapland');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1347, 197, E'None', E'None');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1348, 197, E'Oulu', E'Oulu');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1349, 197, E'Southern Finland', E'Southern Finland');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1350, 197, E'Western Finland', E'Western Finland');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1351, 198, E'Ain', E'Ain');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1352, 198, E'Aisne', E'Aisne');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1353, 198, E'Allier', E'Allier');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1354, 198, E'Alpes-de-Haute-Provence', E'Alpes-de-Haute-Provence');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1355, 198, E'Alpes-Maritimes', E'Alpes-Maritimes');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1356, 198, E'Ardche', E'Ardche');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1357, 198, E'Ardennes', E'Ardennes');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1358, 198, E'Arige', E'Arige');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1359, 198, E'Aube', E'Aube');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1360, 198, E'Aude', E'Aude');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1361, 198, E'Aveyron', E'Aveyron');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1362, 198, E'Bas Rhin', E'Bas Rhin');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1363, 198, E'Bouches-du-Rhne', E'Bouches-du-Rhne');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1364, 198, E'Calvados', E'Calvados');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1365, 198, E'Cantal', E'Cantal');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1366, 198, E'Charente', E'Charente');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1367, 198, E'Charente Maritime', E'Charente Maritime');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1368, 198, E'Cher', E'Cher');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1369, 198, E'Corrze', E'Corrze');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1370, 198, E'Corse', E'Corse');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1371, 198, E'Cte d''Or', E'Cte d''Or');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1372, 198, E'Ctes d''Armor', E'Ctes d''Armor');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1373, 198, E'Creuse', E'Creuse');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1374, 198, E'Deux-Svres', E'Deux-Svres');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1375, 198, E'Dordogne', E'Dordogne');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1376, 198, E'Doubs', E'Doubs');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1377, 198, E'Drme', E'Drme');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1378, 198, E'Essone', E'Essone');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1379, 198, E'Eure', E'Eure');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1380, 198, E'Eure-et-Loire', E'Eure-et-Loire');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1381, 198, E'Finistre', E'Finistre');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1382, 198, E'Gard', E'Gard');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1383, 198, E'Gers', E'Gers');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1384, 198, E'Gironde', E'Gironde');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1385, 198, E'Haut Rhin', E'Haut Rhin');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1386, 198, E'Haute-Garonne', E'Haute-Garonne');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1387, 198, E'Haute-Loire', E'Haute-Loire');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1388, 198, E'Haute-Marne', E'');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1389, 198, E'Haute-Sane', E'Haute-Sane');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1390, 198, E'Haute-Savoie', E'Haute-Savoie');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1391, 198, E'Haute-Vienne', E'Haute-Vienne');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1392, 198, E'Hautes-Alpes', E'Hautes-Alpes');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1393, 198, E'Hautes-Pyrnes', E'Hautes-Pyrnes');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1394, 198, E'Hauts-de-Seine', E'Hauts-de-Seine');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1395, 198, E'Hrault', E'Hrault');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1396, 198, E'Ille et Vilaine', E'Ille et Vilaine');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1397, 198, E'Indre', E'Indre');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1398, 198, E'Indre-et-Loire', E'Indre-et-Loire');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1399, 198, E'Isre', E'Isre');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1400, 198, E'Jura', E'Jura');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1401, 198, E'Landes', E'Landes');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1402, 198, E'Loir-et-Cher', E'Loir-et-Cher');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1403, 198, E'Loire', E'Loire');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1404, 198, E'Loire Atlantique', E'Loire Atlantique');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1405, 198, E'Loiret', E'Loiret');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1406, 198, E'Lot', E'Lot');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1407, 198, E'Lot-et-Garonne', E'Lot-et-Garonne');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1408, 198, E'Lozre', E'Lozre');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1409, 198, E'Maine et Loire', E'Maine et Loire');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1410, 198, E'Manche', E'Manche');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1411, 198, E'Marne', E'Marne');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1412, 198, E'Mayenne', E'Mayenne');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1413, 198, E'Meurthe-et-Moselle', E'Meurthe-et-Moselle');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1414, 198, E'Meuse', E'Meuse');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1415, 198, E'Morbihan', E'Morbihan');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1416, 198, E'Moselle', E'Moselle');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1417, 198, E'Nord', E'Nord');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1418, 198, E'Oise', E'Oise');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1419, 198, E'Orne', E'Orne');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1420, 198, E'Paris', E'Paris');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1421, 198, E'Pas-de-Calais', E'Pas-de-Calais');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1422, 198, E'Puy-de-Dme', E'Puy-de-Dme');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1423, 198, E'Pyrnes-Atlantiques', E'Pyrnes-Atlantiques');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1424, 198, E'Pyrnes-Orientales', E'Pyrnes-Orientales');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1425, 198, E'Rhne', E'Rhne');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1426, 198, E'Sane et Loire', E'Sane et Loire');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1427, 198, E'Savoie', E'Savoie');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1428, 198, E'Seine-et-Marne', E'Seine-et-Marne');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1429, 198, E'Seine-Maritime', E'Seine-Maritime');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1430, 198, E'Seine-Saint-Denis', E'Seine-Saint-Denis');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1431, 198, E'Somme', E'Somme');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1432, 198, E'Tarn', E'Tarn');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1433, 198, E'Tarn-et-Garonne', E'Tarn-et-Garonne');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1434, 198, E'Territoire de Belfort', E'Territoire de Belfort');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1435, 198, E'Val-d''Oise', E'Val-d''Oise');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1436, 198, E'Val-de-Marne', E'Val-de-Marne');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1437, 198, E'Var', E'Var');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1438, 198, E'Vaucluse', E'Vaucluse');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1439, 198, E'Vende', E'Vende');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1440, 198, E'Vienne', E'Vienne');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1441, 198, E'Vosges', E'Vosges');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1442, 198, E'Yonne', E'Yonne');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1443, 198, E'Yvelines', E'Yvelines');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1444, 199, E'Caennes', E'Caennes');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1445, 200, E'�������', E'�������');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1446, 201, E'Dubrovacko-Neretvanska', E'Dubrovacko-Neretvanska');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1447, 201, E'Grad Zagreb', E'Grad Zagreb');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1448, 201, E'Istarska', E'Istarska');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1449, 201, E'Karlovacka', E'Karlovacka');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1450, 201, E'Medimurska', E'Medimurska');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1451, 201, E'None', E'None');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1452, 201, E'Primorsko-Goranska', E'Primorsko-Goranska');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1453, 201, E'Sibensko-Kninska', E'Sibensko-Kninska');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1454, 201, E'Sisacko-Moslavacka', E'Sisacko-Moslavacka');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1455, 201, E'Splitsko-Dalmatinska', E'Splitsko-Dalmatinska');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1456, 201, E'Varazdinska', E'Varazdinska');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1457, 201, E'Zagrebacka', E'Zagrebacka');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1458, 202, E'���', E'Chad');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1459, 203, E'����������', E'Montenegro');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1460, 204, E'Hlavni Mesto Praha', E'Hlavni Mesto Praha');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1461, 204, E'Jihocesky Kraj', E'Jihocesky Kraj');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1462, 204, E'Jihomoravsky Kraj', E'Jihomoravsky Kraj');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1463, 204, E'Karlovarsky Kraj', E'Karlovarsky Kraj');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1464, 204, E'Kralovehradecky Kraj', E'Kralovehradecky Kraj');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1465, 204, E'Liberecky Kraj', E'Liberecky Kraj');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1466, 204, E'Moravskoslezsky Kraj', E'Moravskoslezsky Kraj');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1467, 204, E'None', E'None');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1468, 204, E'Olomoucky Kraj', E'Olomoucky Kraj');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1469, 204, E'Pardubicky Kraj', E'Pardubicky Kraj');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1470, 204, E'Plzensky Kraj', E'Plzensky Kraj');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1471, 204, E'Stredocesky Kraj', E'Stredocesky Kraj');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1472, 204, E'Ustecky Kraj', E'Ustecky Kraj');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1473, 204, E'Vysocina', E'Vysocina');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1474, 204, E'Zlinsky Kraj', E'Zlinsky Kraj');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1475, 205, E'Aisn del General Carlos Ibnez del Campo', E'Aisn del General Carlos Ibnez del Campo');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1476, 205, E'Antofagasta', E'Antofagasta');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1477, 205, E'Araucana', E'Araucana');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1478, 205, E'Atacama', E'Atacama');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1479, 205, E'Bo-Bo', E'Bo-Bo');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1480, 205, E'Coquimbo', E'Coquimbo');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1481, 205, E'Libertador General Bernardo O''Higgins', E'Libertador General Bernardo O''Higgins');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1482, 205, E'Los Lagos', E'Los Lagos');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1483, 205, E'Magallanes y de la Antrtica Chilena', E'Magallanes y de la Antrtica Chilena');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1484, 205, E'Maule', E'Maule');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1485, 205, E'Region Metropolitana', E'Region Metropolitana');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1486, 205, E'Tarapac', E'Tarapac');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1487, 205, E'Valparaso', E'Valparaso');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1488, 206, E'Aargau', E'Aargau');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1489, 206, E'Appenzell Ausserrhoden', E'Appenzell Ausserrhoden');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1490, 206, E'Appenzell Innerrhoden', E'Appenzell Innerrhoden');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1491, 206, E'Basel-Landschaft', E'Basel-Landschaft');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1492, 206, E'Basel-Stadt', E'Basel-Stadt');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1493, 206, E'Bern', E'Bern');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1494, 206, E'Fribourg', E'Fribourg');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1495, 206, E'Genve', E'Genve');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1496, 206, E'Glarus', E'Glarus');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1497, 206, E'Graubnden', E'Graubnden');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1498, 206, E'Jura', E'Jura');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1499, 206, E'Luzern', E'Luzern');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1500, 206, E'Neuchtel', E'Neuchtel');

COMMIT;

BEGIN;

/* Data for the 'profiledb.region' table  (Records 1501 - 1611) */

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1501, 206, E'Nidwalden', E'Nidwalden');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1502, 206, E'Obwalden', E'Obwalden');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1503, 206, E'Sankt Gallen', E'Sankt Gallen');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1504, 206, E'Schaffhausen', E'Schaffhausen');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1505, 206, E'Schwyz', E'Schwyz');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1506, 206, E'Solothurn', E'Solothurn');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1507, 206, E'Thurgau', E'Thurgau');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1508, 206, E'Ticino', E'Ticino');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1509, 206, E'Uri', E'Uri');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1510, 206, E'Valais', E'Valais');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1511, 206, E'Vaud', E'Vaud');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1512, 206, E'Zug', E'Zug');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1513, 206, E'Zrich', E'Zrich');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1514, 207, E'Blekinge Lan', E'Blekinge Lan');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1515, 207, E'Dalarnas Lan', E'Dalarnas Lan');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1516, 207, E'Gavleborgs Lan', E'Gavleborgs Lan');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1517, 207, E'Gotlands Lan', E'Gotlands Lan');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1518, 207, E'Hallands Lan', E'Hallands Lan');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1519, 207, E'Jamtlands Lan', E'Jamtlands Lan');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1520, 207, E'Jonkopings Lan', E'Jonkopings Lan');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1521, 207, E'Kalmar Lan', E'Kalmar Lan');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1522, 207, E'Kronobergs Lan', E'Kronobergs Lan');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1523, 207, E'None', E'None');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1524, 207, E'Norrbottens Lan', E'Norrbottens Lan');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1525, 207, E'Orebro Lan', E'Orebro Lan');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1526, 207, E'Ostergotlands Lan', E'Ostergotlands Lan');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1527, 207, E'Skane Lan', E'Skane Lan');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1528, 207, E'Sodermanlands Lan', E'Sodermanlands Lan');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1529, 207, E'Stockholms Lan', E'Stockholms Lan');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1530, 207, E'Uppsala Lan', E'Uppsala Lan');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1531, 207, E'Varmlands Lan', E'Varmlands Lan');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1532, 207, E'Vasterbottens Lan', E'Vasterbottens Lan');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1533, 207, E'Vasternorrlands Lan', E'Vasternorrlands Lan');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1534, 207, E'Vastmanlands Lan', E'Vastmanlands Lan');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1535, 207, E'Vastra Gotaland', E'Vastra Gotaland');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1536, 208, E'���-�����', E'Sri Lanka');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1537, 209, E'Azuay', E'Azuay');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1538, 209, E'Bolvar', E'Bolvar');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1539, 209, E'Chimborazo', E'Chimborazo');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1540, 209, E'El Oro', E'El Oro');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1541, 209, E'Galpagos', E'Galpagos');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1542, 209, E'Guayas', E'Guayas');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1543, 209, E'Imbabura', E'Imbabura');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1544, 209, E'Loja', E'Loja');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1545, 209, E'Manab', E'Manab');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1546, 209, E'Napo', E'Napo');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1547, 209, E'Pastaza', E'Pastaza');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1548, 209, E'Pichincha', E'Pichincha');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1549, 209, E'Tungurahua', E'Tungurahua');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1550, 210, E'������', E'������');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1551, 211, E'�������', E'Eritrea');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1552, 212, E'�������', E'Estoniya');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1553, 213, E'�������', E'Ethiopia');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1554, 214, E'�����������', E'Johannesburg');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1555, 215, E'Cheju', E'Cheju');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1556, 215, E'Chollabuk', E'Chollabuk');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1557, 215, E'Chollanam', E'Chollanam');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1558, 215, E'Chungcheongbuk', E'Chungcheongbuk');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1559, 215, E'Chungcheongnam', E'Chungcheongnam');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1560, 215, E'Incheon', E'Incheon');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1561, 215, E'Kangweon', E'Kangweon');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1562, 215, E'Kwangju', E'Kwangju');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1563, 215, E'Kyeonggi', E'Kyeonggi');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1564, 215, E'Kyeongsangbuk', E'Kyeongsangbuk');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1565, 215, E'Kyeongsangnam', E'Kyeongsangnam');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1566, 215, E'Pusan', E'Pusan');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1567, 215, E'Seoul', E'Seoul');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1568, 215, E'Taegu', E'Taegu');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1569, 215, E'Taejeon', E'Taejeon');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1570, 215, E'Ulsan', E'Ulsan');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1571, 215, E'����� �����', E'South Korea');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1572, 216, E'����� ������', E'����� ������');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1573, 217, E'������', E'������');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1574, 218, E'����', E'����');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1575, 218, E'������', E'������');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1576, 218, E'�������', E'�������');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1577, 218, E'�����', E'�����');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1578, 218, E'�������', E'�������');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1579, 218, E'�����', E'�����');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1580, 218, E'�������', E'�������');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1581, 218, E'��������', E'��������');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1582, 218, E'��������', E'��������');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1583, 218, E'�����', E'�����');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1584, 218, E'����', E'����');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1585, 218, E'�����', E'�����');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1586, 218, E'��������', E'��������');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1587, 218, E'������', E'������');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1588, 218, E'��������', E'��������');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1589, 218, E'����', E'����');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1590, 218, E'�������', E'�������');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1591, 218, E'������', E'������');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1592, 218, E'�������', E'�������');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1593, 218, E'�����', E'�����');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1594, 218, E'����', E'����');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1595, 218, E'�������', E'�������');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1596, 218, E'����', E'����');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1597, 218, E'��������', E'��������');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1598, 218, E'����', E'����');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1599, 218, E'�����', E'�����');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1600, 218, E'��������', E'��������');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1601, 218, E'������', E'������');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1602, 218, E'�����', E'�����');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1603, 218, E'�����', E'�����');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1604, 218, E'�������', E'�������');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1605, 218, E'��������', E'��������');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1606, 218, E'��������', E'��������');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1607, 218, E'��������', E'��������');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1608, 218, E'�����', E'�����');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1609, 218, E'�����', E'�����');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1610, 218, E'�������', E'�������');

INSERT INTO profiledb.region ("region_id", "country_id", "region_name_ru", "region_name_en")
VALUES (1611, 218, E'�������', E'�������');

COMMIT;
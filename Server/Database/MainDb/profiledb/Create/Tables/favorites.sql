﻿CREATE TABLE profiledb.favorites (
  owner_profile_id BIGINT NOT NULL,
  object_id BIGINT NOT NULL,
  object_type_id INTEGER NOT NULL,
  client_id BIGINT NOT NULL,
  CONSTRAINT favorites_pkey PRIMARY KEY(owner_profile_id, object_id, object_type_id, client_id)
) WITHOUT OIDS;

COMMENT ON COLUMN profiledb.favorites.owner_profile_id
IS 'Owner profile identifier';

COMMENT ON COLUMN profiledb.favorites.object_id
IS 'Object identifier';

COMMENT ON COLUMN profiledb.favorites.object_type_id
IS 'Object type';

COMMENT ON COLUMN profiledb.favorites.client_id
IS 'Client identifier';

CREATE INDEX favorites_idx ON profiledb.favorites
  USING btree (client_id);
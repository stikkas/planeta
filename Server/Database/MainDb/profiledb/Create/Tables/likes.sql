CREATE SEQUENCE profiledb.seq_like_id
INCREMENT 1 MINVALUE 1
MAXVALUE 9223372036854775807 START 1
CACHE 1;

CREATE TABLE profiledb.likes (
  like_id bigint NOT NULL PRIMARY KEY ,
  author_id bigint NOT NULL ,
  profile_id bigint NOT NULL ,
  object_id bigint NOT NULL ,
  object_type_code int NOT NULL,
  time_added DATE NOT NULL DEFAULT NOW(),
  CONSTRAINT one_like_per_user_uc UNIQUE (author_id, object_id, object_type_code)
);
CREATE INDEX object_id_likes_idx ON profiledb.likes (profile_id, object_id, object_type_code);


COMMENT ON TABLE profiledb.likes
IS 'Planeta likes (+1)';
COMMENT ON COLUMN profiledb.likes.like_id
IS 'Likes identifier';
COMMENT ON COLUMN profiledb.likes.author_id
IS 'Profile id of person who likes';
COMMENT ON COLUMN profiledb.likes.profile_id
IS 'Profile id of liked content owner';
COMMENT ON COLUMN profiledb.likes.object_id
IS 'Object id of liked content (photo/video/other)';
COMMENT ON COLUMN profiledb.likes.object_type_code
IS 'Type of liked content';
COMMENT ON COLUMN profiledb.likes.time_added
IS 'Time of liking';
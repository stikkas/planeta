CREATE TABLE profiledb.profile_settings (
  profile_id BIGINT NOT NULL,
  default_tab VARCHAR(32),
  vote_permission INTEGER,
  background_image_id BIGINT,
  background_rotation_style INTEGER,
  background_rotation_period INTEGER,
  background_color VARCHAR(50),
  daily_rotation_style INTEGER,
  daily_rotation_period INTEGER,
  daily_feature_id INTEGER,
  play_sound_when_new_message_arrives BOOLEAN DEFAULT true not null,
  CONSTRAINT profile_settings_pkey PRIMARY KEY(profile_id)
) WITHOUT OIDS;

COMMENT ON COLUMN profiledb.profile_settings.background_image_id
IS 'Background image identifier';

COMMENT ON COLUMN profiledb.profile_settings.background_rotation_style
IS 'Background image rotation style';

COMMENT ON COLUMN profiledb.profile_settings.background_rotation_period
IS 'Background image rotation period';

COMMENT ON COLUMN profiledb.profile_settings.background_color
IS 'Background color';

COMMENT ON COLUMN profiledb.profile_settings.daily_rotation_style
IS 'Feature of the day rotation style';

COMMENT ON COLUMN profiledb.profile_settings.daily_rotation_period
IS 'Feature of the day rotation period';

COMMENT ON COLUMN profiledb.profile_settings.daily_feature_id
IS 'Current daily feature identifier, 0 means group doesn''t want this functionality';

COMMENT ON COLUMN profiledb.profile_settings.play_sound_when_new_message_arrives
IS 'Play sound when new message arrives';

CREATE TABLE profiledb.broadcast_subscriptions (
  broadcast_id          BIGINT                NOT NULL,
  owner_profile_id      BIGINT                NOT NULL,
  subscriber_profile_id BIGINT                NOT NULL,
  on_email              BOOLEAN DEFAULT FALSE NOT NULL,
  on_site               BOOLEAN DEFAULT FALSE NOT NULL
) WITHOUT OIDS;

COMMENT ON TABLE profiledb.broadcast_subscriptions
IS 'Profile''s broadcast subscriptions';

COMMENT ON COLUMN profiledb.broadcast_subscriptions.subscriber_profile_id
IS 'Subscriber''s profile identifier';

COMMENT ON COLUMN profiledb.broadcast_subscriptions.on_email
IS 'Notification on email';

COMMENT ON COLUMN profiledb.broadcast_subscriptions.on_site
IS 'Notification on site';

GRANT SELECT ON profiledb.broadcast_subscriptions TO planeta;
GRANT UPDATE ON profiledb.broadcast_subscriptions TO planeta;
GRANT DELETE ON profiledb.broadcast_subscriptions TO planeta;
GRANT INSERT ON profiledb.broadcast_subscriptions TO planeta;
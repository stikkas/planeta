CREATE TABLE profiledb.broadcast_geo_targeting (
  broadcast_id BIGINT NOT NULL, 
  profile_id BIGINT NOT NULL,
  city_id BIGINT,
  country_id BIGINT,
  allowed BOOLEAN
) WITHOUT OIDS;

COMMENT ON TABLE profiledb.broadcasts
IS 'Profile''s broadcasts geo targeting restrictions';

COMMENT ON COLUMN profiledb.broadcasts.broadcast_id
IS 'Broadcast''s identifier';

COMMENT ON COLUMN profiledb.broadcasts.profile_id
IS 'Owner''s identifier';

COMMENT ON COLUMN profiledb.broadcasts.city_id
IS 'City''s identifier';

COMMENT ON COLUMN profiledb.broadcasts.country_id
IS 'Country''s identifier';

COMMENT ON COLUMN profiledb.broadcasts.allowed
IS 'Flag allowed';
CREATE TABLE profiledb.chat_messages (
  message_id BIGINT NOT NULL, 
  chat_id BIGINT NOT NULL, 
  owner_profile_id BIGINT NOT NULL,	
  author_profile_id BIGINT NOT NULL, 
  message_text TEXT NOT NULL, 
  message_text_html TEXT NOT NULL, 
  is_deleted BOOLEAN DEFAULT false NOT NULL, 
  time_added TIMESTAMP WITHOUT TIME ZONE DEFAULT now() NOT NULL, 
  CONSTRAINT messages_pkey PRIMARY KEY(owner_profile_id, message_id)
) WITH OIDS;

COMMENT ON TABLE profiledb.chat_messages
IS 'Chat messages';

COMMENT ON COLUMN profiledb.chat_messages.message_id
IS 'Message identifier';

COMMENT ON COLUMN profiledb.chat_messages.chat_id
IS 'Chat identifier';

COMMENT ON COLUMN profiledb.chat_messages.owner_profile_id
IS 'Owner profile identifier';

COMMENT ON COLUMN profiledb.chat_messages.author_profile_id
IS 'Message author identifier';

COMMENT ON COLUMN profiledb.chat_messages.message_text
IS 'Message text (bb-code)';

COMMENT ON COLUMN profiledb.chat_messages.message_text_html
IS 'Transformed bb-code';

COMMENT ON COLUMN profiledb.chat_messages.is_deleted
IS 'Is this message marked as deleted';

COMMENT ON COLUMN profiledb.chat_messages.time_added
IS 'Time added;

CREATE INDEX chat_messages_idx ON profiledb.chat_messages (chat_id, owner_profile_id);
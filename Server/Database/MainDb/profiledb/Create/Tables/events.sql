CREATE TABLE profiledb.events (
  profile_id BIGINT NOT NULL, 
  description TEXT NOT NULL, 
  place VARCHAR(512) NOT NULL, 
  description_html TEXT, 
  user_join_permission INTEGER NOT NULL, 
  group_join_permission INTEGER NOT NULL, 
  is_merchant BOOLEAN DEFAULT false, 
  special_offer VARCHAR(256) DEFAULT NULL::character varying, 
  age_limit VARCHAR(256) DEFAULT NULL::character varying, 
  event_ticket_type INTEGER DEFAULT 0 NOT NULL, 
  CONSTRAINT events_pkey PRIMARY KEY(profile_id)
) WITHOUT OIDS;

COMMENT ON TABLE profiledb.events
IS 'Events specific data';

COMMENT ON COLUMN profiledb.events.profile_id
IS 'Event''s profile identifier';

COMMENT ON COLUMN profiledb.events.description
IS 'Event''s description';

COMMENT ON COLUMN profiledb.events.place
IS 'Event''s place';

COMMENT ON COLUMN profiledb.events.is_merchant
IS 'Event ticket merchant qualifier';

COMMENT ON COLUMN profiledb.events.age_limit
IS 'Age limit for event';

COMMENT ON COLUMN profiledb.events.event_ticket_type
IS 'Event''s tickets type';
﻿CREATE TABLE profiledb.broadcast_streams (
  stream_id BIGINT NOT NULL,
  broadcast_id BIGINT NOT NULL,
  owner_profile_id BIGINT NOT NULL,
  author_profile_id BIGINT NOT NULL,
  broadcast_url VARCHAR(256) NOT NULL,
  broadcast_type INTEGER DEFAULT 0 NOT NULL,
  broadcast_status INTEGER DEFAULT 0 NOT NULL,
  broadcast_record_status INTEGER DEFAULT 0 NOT NULL,
  name VARCHAR(256) DEFAULT NULL::character varying,
  description TEXT,
  image_url VARCHAR(256) DEFAULT NULL::character varying,
  image_id BIGINT,
  video_url VARCHAR(256) DEFAULT NULL::character varying,
  video_id BIGINT,
  time_added TIMESTAMP WITHOUT TIME ZONE DEFAULT now() NOT NULL,
  time_updated TIMESTAMP WITHOUT TIME ZONE DEFAULT now() NOT NULL,
  views_count INTEGER DEFAULT 0 NOT NULL,
  view_permission INTEGER DEFAULT 0 NOT NULL,
  time_begin TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT now() NOT NULL,
  time_end TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT now() NOT NULL,
  embed_video_html TEXT,
  CONSTRAINT broadcast_streams_pkey PRIMARY KEY(stream_id)
) WITHOUT OIDS;

COMMENT ON TABLE profiledb.broadcast_streams
IS 'Broadcast''s streams';

COMMENT ON COLUMN profiledb.broadcast_streams.stream_id
IS 'Stream''s identifier';

COMMENT ON COLUMN profiledb.broadcast_streams.broadcast_id
IS 'Broadcast''s identifier';

COMMENT ON COLUMN profiledb.broadcast_streams.owner_profile_id
IS 'Owner''s identifier';

COMMENT ON COLUMN profiledb.broadcast_streams.author_profile_id
IS 'Author''s identifier';

COMMENT ON COLUMN profiledb.broadcast_streams.broadcast_url
IS 'Broadcast url';

COMMENT ON COLUMN profiledb.broadcast_streams.broadcast_type
IS 'Broadcast type';

COMMENT ON COLUMN profiledb.broadcast_streams.broadcast_status
IS 'Broadcast status';

COMMENT ON COLUMN profiledb.broadcast_streams.broadcast_record_status
IS 'Broadcast record status';

COMMENT ON COLUMN profiledb.broadcast_streams.name
IS 'Name';

COMMENT ON COLUMN profiledb.broadcast_streams.description
IS 'Description';

COMMENT ON COLUMN profiledb.broadcast_streams.image_url
IS 'Image url';

COMMENT ON COLUMN profiledb.broadcast_streams.image_id
IS 'Image identifier';

COMMENT ON COLUMN profiledb.broadcast_streams.video_url
IS 'Video url';

COMMENT ON COLUMN profiledb.broadcast_streams.video_id
IS 'Video identifier';

COMMENT ON COLUMN profiledb.broadcast_streams.time_added
IS 'Time added';

COMMENT ON COLUMN profiledb.broadcast_streams.time_updated
IS 'Time updated';

COMMENT ON COLUMN profiledb.broadcast_streams.views_count
IS 'Views count';

COMMENT ON COLUMN profiledb.broadcast_streams.view_permission
IS 'View permission';

COMMENT ON COLUMN profiledb.broadcast_streams.embed_video_html
IS 'Embed video html';

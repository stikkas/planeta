CREATE TABLE profiledb.feed (
  item_id BIGINT NOT NULL, 
  type INTEGER NOT NULL, 
  profile_id BIGINT NOT NULL, 
  object_id BIGINT NOT NULL, 
  group_id BIGINT NOT NULL, 
  time_added TIMESTAMP WITHOUT TIME ZONE DEFAULT now() NOT NULL, 
  time_updated TIMESTAMP WITHOUT TIME ZONE DEFAULT now() NOT NULL, 
  PRIMARY KEY(item_id)
) WITHOUT OIDS;

ALTER TABLE profiledb.feed
  OWNER TO planeta;

COMMENT ON TABLE profiledb.feed
IS 'Contains profile news feed';

COMMENT ON COLUMN profiledb.feed.item_id
IS 'Unique identifier';

COMMENT ON COLUMN profiledb.feed.type
IS 'Feed item type';

COMMENT ON COLUMN profiledb.feed.profile_id
IS 'Owner profile identifier';

COMMENT ON COLUMN profiledb.feed.object_id
IS 'Source object identifier';

COMMENT ON COLUMN profiledb.feed.group_id
IS 'Group identifier';

CREATE INDEX feed_profile_id_type_object_id_idx ON profiledb.feed
  USING btree (profile_id, type, object_id);
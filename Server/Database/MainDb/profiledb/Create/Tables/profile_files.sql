﻿CREATE TABLE profiledb.profile_files (
  file_id BIGINT NOT NULL,
  owner_profile_id BIGINT NOT NULL,
  author_profile_id BIGINT NOT NULL,
  file_name TEXT NOT NULL,
  file_url TEXT NOT NULL,
  description TEXT NOT NULL,
  time_added TIMESTAMP WITHOUT TIME ZONE DEFAULT now() NOT NULL,
  time_updated TIMESTAMP WITHOUT TIME ZONE DEFAULT now() NOT NULL,
  file_size BIGINT,
  extension VARCHAR(32),
  CONSTRAINT profile_files_pkey PRIMARY KEY(owner_profile_id, file_id)
) WITHOUT OIDS;

COMMENT ON COLUMN profiledb.profile_files.file_id
IS 'Profile file identifier';

COMMENT ON COLUMN profiledb.profile_files.owner_profile_id
IS 'Owner profile identifier';

COMMENT ON COLUMN profiledb.profile_files.author_profile_id
IS 'Author profile identifier';

COMMENT ON COLUMN profiledb.profile_files.file_name
IS 'File name';

COMMENT ON COLUMN profiledb.profile_files.file_url
IS 'File url';

COMMENT ON COLUMN profiledb.profile_files.description
IS 'Description';

COMMENT ON COLUMN profiledb.profile_files.time_added
IS 'Time added';

COMMENT ON COLUMN profiledb.profile_files.time_updated
IS 'Time updated';

COMMENT ON COLUMN profiledb.profile_files.file_size
IS 'File size';

COMMENT ON COLUMN profiledb.profile_files.extension
IS 'File extension';

CREATE TABLE profiledb.broadcasts (
  broadcast_id BIGINT NOT NULL, 
  owner_profile_id BIGINT NOT NULL, 
  author_profile_id BIGINT NOT NULL, 
  tags VARCHAR(256) DEFAULT NULL::CHARACTER VARYING, 
  NAME VARCHAR(256) DEFAULT NULL::CHARACTER VARYING, 
  description TEXT, 
  time_added TIMESTAMP WITHOUT TIME ZONE DEFAULT now() NOT NULL,
  time_updated TIMESTAMP WITHOUT TIME ZONE DEFAULT now() NOT NULL, 
  views_count INTEGER DEFAULT 0 NOT NULL, 
  view_permission INTEGER DEFAULT 0 NOT NULL, 
  broadcast_status INTEGER DEFAULT 0, 
  broadcast_subscription_status INTEGER DEFAULT 0,
  default_stream_id BIGINT DEFAULT 0,
  description_html TEXT, 
  time_begin TIMESTAMP WITHOUT TIME ZONE DEFAULT now() NOT NULL, 
  time_end TIMESTAMP WITHOUT TIME ZONE DEFAULT now() NOT NULL, 
  auto_start BOOLEAN DEFAULT FALSE, 
  auto_subscribe BOOLEAN DEFAULT FALSE, 
  archived BOOLEAN DEFAULT FALSE, 
  adv_banner_html TEXT,
  visible_on_dashboard BOOLEAN DEFAULT FALSE, 
  closed BOOLEAN DEFAULT FALSE NOT NULL,
  time_archived TIMESTAMP WITHOUT TIME ZONE DEFAULT NULL,
  order_num INTEGER DEFAULT 0 NOT NULL, 
  CONSTRAINT broadcasts_pkey PRIMARY KEY(broadcast_id)
) WITHOUT OIDS;

COMMENT ON TABLE profiledb.broadcasts
IS 'Profile''s broadcasts';

COMMENT ON COLUMN profiledb.broadcasts.broadcast_id
IS 'Broadcast''s identifier';

COMMENT ON COLUMN profiledb.broadcasts.owner_profile_id
IS 'Owner''s identifier';

COMMENT ON COLUMN profiledb.broadcasts.author_profile_id
IS 'Author''s identifier';

COMMENT ON COLUMN profiledb.broadcasts.tags
IS 'Tags';

COMMENT ON COLUMN profiledb.broadcasts.name
IS 'Name';

COMMENT ON COLUMN profiledb.broadcasts.description
IS 'Description';

COMMENT ON COLUMN profiledb.broadcasts.time_added
IS 'Time added';

COMMENT ON COLUMN profiledb.broadcasts.time_updated
IS 'Time updated';

COMMENT ON COLUMN profiledb.broadcasts.views_count
IS 'Views count';

COMMENT ON COLUMN profiledb.broadcasts.view_permission
IS 'View permission';

COMMENT ON COLUMN profiledb.broadcasts.broadcast_status
IS 'Broadcast status';

COMMENT ON COLUMN profiledb.broadcasts.default_stream_id
IS 'Default stream''s identifier';

COMMENT ON COLUMN profiledb.broadcasts.description_html
IS 'Description html';

COMMENT ON COLUMN profiledb.broadcasts.time_begin
IS 'Time begin';

COMMENT ON COLUMN profiledb.broadcasts.time_end
IS 'Time end';

COMMENT ON COLUMN profiledb.broadcasts.auto_start
IS 'Auto start';

COMMENT ON COLUMN profiledb.broadcasts.auto_subscribe
IS 'Auto subscribe';

COMMENT ON COLUMN profiledb.broadcasts.archived
IS 'Broadcast is available in archive';

COMMENT ON COLUMN profiledb.broadcasts.adv_banner_html
IS 'Advertisment banner html';

COMMENT ON COLUMN profiledb.broadcasts.broadcast_subscription_status
IS 'Subscription complete status';
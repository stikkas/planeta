﻿CREATE TABLE profiledb.objects_events_subscriptions (
  owner_profile_id BIGINT NOT NULL,
  object_id BIGINT NOT NULL,
  object_type INTEGER NOT NULL,
  client_id BIGINT NOT NULL,
  event_type INTEGER NOT NULL,
  CONSTRAINT objects_events_subscriptions_pkey PRIMARY KEY(owner_profile_id, object_id, object_type, client_id, event_type)
) WITHOUT OIDS;

COMMENT ON COLUMN profiledb.objects_events_subscriptions.owner_profile_id
IS 'Owner profile identifier';

COMMENT ON COLUMN profiledb.objects_events_subscriptions.object_id
IS 'Object identifier';

COMMENT ON COLUMN profiledb.objects_events_subscriptions.object_type
IS 'Object type';

COMMENT ON COLUMN profiledb.objects_events_subscriptions.client_id
IS 'Client identifier';

COMMENT ON COLUMN profiledb.objects_events_subscriptions.event_type
IS 'Event type';
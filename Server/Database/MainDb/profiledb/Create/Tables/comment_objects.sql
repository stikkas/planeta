CREATE TABLE profiledb.comment_objects (
  profile_id BIGINT NOT NULL,
  object_type_id INTEGER NOT NULL,
  object_id BIGINT NOT NULL,
  comments_count INTEGER DEFAULT 0 NOT NULL,
  comments_permission INTEGER NOT NULL,
  time_updated TIMESTAMP WITHOUT TIME ZONE DEFAULT now(),
  CONSTRAINT profile_comments_pkey PRIMARY KEY(profile_id, object_type_id, object_id)
) WITHOUT OIDS;

COMMENT ON TABLE profiledb.comment_objects
IS 'Comments to the profile objects';

COMMENT ON COLUMN profiledb.comment_objects.profile_id
IS 'Profile identifier';

COMMENT ON COLUMN profiledb.comment_objects.object_type_id
IS 'Object type identifier';

COMMENT ON COLUMN profiledb.comment_objects.object_id
IS 'Object id';

COMMENT ON COLUMN profiledb.comment_objects.comments_count
IS 'Count of comments';

COMMENT ON COLUMN profiledb.comment_objects.comments_permission
IS 'Comment permission';

COMMENT ON COLUMN profiledb.comment_objects.time_updated
IS 'Time updated';

CREATE TABLE profiledb.profile_feed_settings (
  profile_id BIGINT NOT NULL,
  subject_profile_id BIGINT NOT NULL,
  item_id BIGINT,
  type BIGINT
) WITHOUT OIDS;

COMMENT ON COLUMN profiledb.profile_feed_settings.profile_id
IS 'Profile identifier';

COMMENT ON COLUMN profiledb.profile_feed_settings.subject_profile_id
IS 'Profile identifier in feed';

COMMENT ON COLUMN profiledb.profile_feed_settings.item_id
IS 'Feed item identifier';

COMMENT ON COLUMN profiledb.profile_feed_settings.type
IS 'Feed item type';
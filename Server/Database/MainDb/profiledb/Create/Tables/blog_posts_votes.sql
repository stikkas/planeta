CREATE TABLE profiledb.blog_posts_votes (
  blog_post_id INTEGER NOT NULL, 
  owner_profile_id BIGINT NOT NULL, 
  author_profile_id BIGINT NOT NULL, 
  vote SMALLINT DEFAULT 0, 
  time_added TIMESTAMP WITHOUT TIME ZONE DEFAULT now() NOT NULL, 
  CONSTRAINT group_blog_posts_votes_pkey PRIMARY KEY(owner_profile_id, blog_post_id, author_profile_id)
) WITH OIDS;

COMMENT ON COLUMN profiledb.blog_posts_votes.blog_post_id
IS 'Blog post identifier';

COMMENT ON COLUMN profiledb.blog_posts_votes.owner_profile_id
IS 'Blog post owner profile identifier';

COMMENT ON COLUMN profiledb.blog_posts_votes.author_profile_id
IS 'Vote author profile identifier';

COMMENT ON COLUMN profiledb.blog_posts_votes.vote
IS 'Blog vote';

COMMENT ON COLUMN profiledb.blog_posts_votes.time_added
IS 'Added time';

CREATE TABLE profiledb.profile_dialogs (
  profile_id BIGINT NOT NULL, 
  companion_profile_id INTEGER, 
  dialog_id BIGINT NOT NULL, 
  unread_messages_count INTEGER DEFAULT 0 NOT NULL, 
  has_messages BOOLEAN DEFAULT false NOT NULL, 
  time_added TIMESTAMP WITHOUT TIME ZONE DEFAULT now() NOT NULL, 
  time_updated TIMESTAMP WITHOUT TIME ZONE DEFAULT now() NOT NULL,
  first_unread_message_id BIGINT NOT NULL DEFAULT 0;
  CONSTRAINT profile_dialogs_companion_profile_id_idx UNIQUE(profile_id, companion_profile_id), 
  CONSTRAINT profile_dialogs_pkey PRIMARY KEY(profile_id, dialog_id)
) WITHOUT OIDS;

COMMENT ON TABLE profiledb.profile_dialogs
IS 'Contains user''s dialogs links';

COMMENT ON COLUMN profiledb.profile_dialogs.profile_id
IS 'Profile identifier';

COMMENT ON COLUMN profiledb.profile_dialogs.companion_profile_id
IS 'Profile identifier of the dialog-companion';

COMMENT ON COLUMN profiledb.profile_dialogs.dialog_id
IS 'Dialog identifier';

COMMENT ON COLUMN profiledb.profile_dialogs.unread_messages_count
IS 'Count of unread messages';

COMMENT ON COLUMN profiledb.profile_dialogs.has_messages
IS 'Signals that this dialog has messages or not';

COMMENT ON COLUMN profiledb.profile_dialogs.first_unread_message_id
IS 'id of first unread message';

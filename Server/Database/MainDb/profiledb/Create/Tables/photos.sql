CREATE TABLE profiledb.photos (
  photo_id BIGINT NOT NULL, 
  album_id BIGINT NOT NULL, 
  owner_profile_id BIGINT NOT NULL, 
  author_profile_id BIGINT NOT NULL, 
  description VARCHAR(512), 
  tags VARCHAR(256), 
  image_url VARCHAR(256) NOT NULL, 
  views_count INTEGER DEFAULT 0 NOT NULL, 
  time_added TIMESTAMP WITHOUT TIME ZONE DEFAULT now(), 
  CONSTRAINT photos_pkey PRIMARY KEY(owner_profile_id, album_id, photo_id)
) WITH OIDS;

COMMENT ON TABLE profiledb.photos
IS 'Group photos';

COMMENT ON COLUMN profiledb.photos.photo_id
IS 'Photo''s identifier';

COMMENT ON COLUMN profiledb.photos.album_id
IS 'Album''s identifier';

COMMENT ON COLUMN profiledb.photos.owner_profile_id
IS 'Owner''s identifier';

COMMENT ON COLUMN profiledb.photos.author_profile_id
IS 'Author user identifier';

COMMENT ON COLUMN profiledb.photos.description
IS 'Description';

COMMENT ON COLUMN profiledb.photos.tags
IS 'Tags';

COMMENT ON COLUMN profiledb.photos.image_url
IS 'Image url';

COMMENT ON COLUMN profiledb.photos.views_count
IS 'Views count';

COMMENT ON COLUMN profiledb.photos.time_added
IS 'Timestamp added';

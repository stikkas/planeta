CREATE TABLE profiledb.users (
  profile_id BIGINT NOT NULL, 
  martial_status INTEGER DEFAULT 0 NOT NULL, 
  description TEXT, 
  description_html TEXT, 
  friends_count INTEGER DEFAULT 0 NOT NULL, 
  groups_count INTEGER DEFAULT 0 NOT NULL, 
  users_request_in_count INTEGER DEFAULT 0 NOT NULL, 
  users_request_out_count INTEGER DEFAULT 0 NOT NULL, 
  notification_email VARCHAR(64), 
  CONSTRAINT users_pkey PRIMARY KEY(profile_id)
) WITHOUT OIDS;

COMMENT ON TABLE profiledb.users
IS 'Contains user''s data';

COMMENT ON COLUMN profiledb.users.profile_id
IS 'User''s profile identifier';

COMMENT ON COLUMN profiledb.users.martial_status
IS 'User''s martial status';

COMMENT ON COLUMN profiledb.users.description
IS 'User''s description (about myself)';

COMMENT ON COLUMN profiledb.users.description_html
IS 'User''s description html';

COMMENT ON COLUMN profiledb.users.users_request_in_count
IS 'Users request in count';

COMMENT ON COLUMN profiledb.users.users_request_out_count
IS 'Users request out count';

COMMENT ON COLUMN profiledb.users.notification_email
IS 'Email for notifications';

COMMENT ON COLUMN profiledb.users.notification_method
IS 'Notifications method';

COMMENT ON COLUMN profiledb.users.notification_level
IS 'Notifications level';

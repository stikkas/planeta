CREATE TABLE profiledb.tag_objects (
  tag_id BIGINT NOT NULL, 
  profile_id BIGINT NOT NULL, 
  object_type_id INTEGER DEFAULT 0 NOT NULL, 
  object_id BIGINT NOT NULL, 
  CONSTRAINT tag_objects_pkey PRIMARY KEY(profile_id, tag_id, object_id, object_type_id)
) WITHOUT OIDS;

COMMENT ON TABLE profiledb.tag_objects
IS 'Links tags to objects';

COMMENT ON COLUMN profiledb.tag_objects.tag_id
IS 'Tag identifier';

COMMENT ON COLUMN profiledb.tag_objects.profile_id
IS 'Profile identifier';

COMMENT ON COLUMN profiledb.tag_objects.object_type_id
IS 'Object type identifier';

COMMENT ON COLUMN profiledb.tag_objects.object_id
IS 'Tagged object identifier';
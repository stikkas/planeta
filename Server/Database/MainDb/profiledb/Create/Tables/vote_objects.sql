CREATE TABLE profiledb.vote_objects
(
  profile_id bigint NOT NULL,
  object_id bigint NOT NULL,
  object_type_code int NOT NULL,
  votes_total int NOT NULL,
  positive_votes int NOT NULL,
  PRIMARY KEY (profile_id, object_id, object_type_code)
);

COMMENT ON COLUMN profiledb.vote_objects.profile_id IS 'Vote object profileId';
COMMENT ON COLUMN profiledb.vote_objects.object_id IS 'Vote object';
COMMENT ON COLUMN profiledb.vote_objects.object_type_code IS 'Vote object type';
COMMENT ON COLUMN profiledb.vote_objects.votes_total IS 'Object rating (see external_rating in myBatis), updated only with voting';

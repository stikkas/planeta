CREATE TABLE profiledb.wall_posts (
  wall_post_id BIGINT NOT NULL, 
  owner_profile_id BIGINT NOT NULL, 
  author_profile_id BIGINT NOT NULL, 
  view_permission INT NOT NULL,  
  text_html TEXT, 
  post_type INT NOT NULL,
  repost_profile_id BIGINT DEFAULT 0 NOT NULL,
  repost_wall_post_id BIGINT DEFAULT 0 NOT NULL,
  share_url VARCHAR(512), 
  time_added TIMESTAMP WITHOUT TIME ZONE DEFAULT now(), 
  CONSTRAINT wall_posts_pkey PRIMARY KEY(owner_profile_id, wall_post_id)
) WITH OIDS;


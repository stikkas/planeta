CREATE TABLE profiledb.audio_albums (
  album_id BIGINT NOT NULL,
  owner_profile_id BIGINT NOT NULL,
  author_profile_id BIGINT NOT NULL,
  album_name VARCHAR(256) NOT NULL,
  artist_name VARCHAR(256) NOT NULL,
  year INTEGER DEFAULT 0 NOT NULL,
  record_label VARCHAR(128),
  status INTEGER DEFAULT 0 NOT NULL,
  image_url VARCHAR(256),
  image_id BIGINT DEFAULT 0 NOT NULL,
  description TEXT,
  tracks_count INTEGER DEFAULT 0 NOT NULL,
  downloads_count INTEGER DEFAULT 0 NOT NULL,
  listenings_count INTEGER DEFAULT 0 NOT NULL,
  time_added TIMESTAMP WITHOUT TIME ZONE DEFAULT now() NOT NULL,
  CONSTRAINT audio_albums_pkey PRIMARY KEY(owner_profile_id, album_id)
) WITH OIDS;

COMMENT ON COLUMN profiledb.audio_albums.album_id
IS 'Audio album''s identifier';

COMMENT ON COLUMN profiledb.audio_albums.owner_profile_id
IS 'Owner''s profile identifier';

COMMENT ON COLUMN profiledb.audio_albums.author_profile_id
IS 'Author''s profile identifier';

COMMENT ON COLUMN profiledb.audio_albums.album_name
IS 'Audio album''s name';

COMMENT ON COLUMN profiledb.audio_albums.artist_name
IS 'Artist name';

COMMENT ON COLUMN profiledb.audio_albums.year
IS 'Year';

COMMENT ON COLUMN profiledb.audio_albums.record_label
IS 'Record label name';

COMMENT ON COLUMN profiledb.audio_albums.status
IS 'Album''s status (recording or finished)';

COMMENT ON COLUMN profiledb.audio_albums.image_url
IS 'Album''s cover image url';

COMMENT ON COLUMN profiledb.audio_albums.image_id
IS 'Album''s cover image id';

COMMENT ON COLUMN profiledb.audio_albums.description
IS 'Album''s description';

COMMENT ON COLUMN profiledb.audio_albums.tracks_count
IS 'Audio tracks count';

COMMENT ON COLUMN profiledb.audio_albums.time_added
IS 'Time added';

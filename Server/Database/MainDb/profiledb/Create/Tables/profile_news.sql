CREATE TABLE profiledb.profile_news (
  profile_id BIGINT NOT NULL, 
  news_type INTEGER NOT NULL, 
  object_id BIGINT NOT NULL,
  internal BOOLEAN NOT NULL DEFAULT FALSE,
  time_added TIMESTAMP WITHOUT TIME ZONE DEFAULT now() NOT NULL
) WITH OIDS;

COMMENT ON COLUMN profiledb.profile_news.profile_id
IS 'Profile identifier';

COMMENT ON COLUMN profiledb.profile_news.news_type
IS 'News type';

COMMENT ON COLUMN profiledb.profile_news.object_id
IS 'Object identifier';

COMMENT ON COLUMN profiledb.profile_news.time_added
IS 'Time added';

COMMENT ON COLUMN profiledb.profile_news.internal
IS 'Internal news flag';


CREATE TABLE profiledb.profiles (
  profile_id BIGINT NOT NULL,
  profile_type_id INTEGER NOT NULL,
  image_url VARCHAR(256),
  image_id BIGINT DEFAULT 0 NOT NULL,
  name VARCHAR(256),
  alias VARCHAR(64),
  status INTEGER DEFAULT 0 NOT NULL,
  city_id INTEGER DEFAULT 0 NOT NULL,
  country_id INTEGER DEFAULT 0 NOT NULL,
  rating INTEGER DEFAULT 0 NOT NULL,
  user_birth_date TIMESTAMP WITHOUT TIME ZONE,
  user_gender INTEGER DEFAULT 0 NOT NULL,
  user_first_name VARCHAR(128),
  user_last_name VARCHAR(128),
  event_place VARCHAR(256) NOT NULL,
  event_type INTEGER DEFAULT 0 NOT NULL,
  group_category_id INTEGER DEFAULT 0 NOT NULL,
  event_time_begin TIMESTAMP WITHOUT TIME ZONE,
  event_time_end TIMESTAMP WITHOUT TIME ZONE,
  event_groups_count INTEGER DEFAULT 0 NOT NULL,
  event_users_count INTEGER DEFAULT 0 NOT NULL,
  creator_profile_id BIGINT DEFAULT 0 NOT NULL,
  users_count INTEGER DEFAULT 0 NOT NULL,
  display_name_format INTEGER DEFAULT 0,
  display_name VARCHAR(256),
  small_image_id BIGINT DEFAULT 0 NOT NULL,
  small_image_url VARCHAR(256),
  summary VARCHAR(512),
  CONSTRAINT profiles_pkey PRIMARY KEY(profile_id)
) WITHOUT OIDS;

COMMENT ON TABLE profiledb.profiles
IS 'Contains all profiles';

COMMENT ON COLUMN profiledb.profiles.profile_id
IS 'Profile identifier';

COMMENT ON COLUMN profiledb.profiles.profile_type_id
IS 'Profile type identifier';

COMMENT ON COLUMN profiledb.profiles.image_url
IS 'Profile image url';

COMMENT ON COLUMN profiledb.profiles.image_id
IS 'Profile image identifier';

COMMENT ON COLUMN profiledb.profiles.name
IS 'Profile name';

COMMENT ON COLUMN profiledb.profiles.alias
IS 'Profile alias';

COMMENT ON COLUMN profiledb.profiles.status
IS 'Profile status';

COMMENT ON COLUMN profiledb.profiles.city_id
IS 'City identifier';

COMMENT ON COLUMN profiledb.profiles.country_id
IS 'Country identifier';

COMMENT ON COLUMN profiledb.profiles.rating
IS 'Profile''s rating';

COMMENT ON COLUMN profiledb.profiles.user_birth_date
IS 'User''s birth date';

COMMENT ON COLUMN profiledb.profiles.user_gender
IS 'User''s gender';

COMMENT ON COLUMN profiledb.profiles.user_first_name
IS 'User''s first name';

COMMENT ON COLUMN profiledb.profiles.user_last_name
IS 'User''s last name';

COMMENT ON COLUMN profiledb.profiles.group_category_id
IS 'Group''s category';

COMMENT ON COLUMN profiledb.profiles.event_time_begin
IS 'Event''s start date';

COMMENT ON COLUMN profiledb.profiles.event_time_end
IS 'Event''s end date';

COMMENT ON COLUMN profiledb.profiles.event_groups_count
IS 'Event''s group-members count';

COMMENT ON COLUMN profiledb.profiles.event_users_count
IS 'Event''s user-members count';

COMMENT ON COLUMN profiledb.profiles.display_name_format
IS 'Display name format code';

COMMENT ON COLUMN profiledb.profiles.display_name
IS 'Display name';

COMMENT ON COLUMN profiledb.profiles.small_image_id
IS 'Profile small image identifier';

COMMENT ON COLUMN profiledb.profiles.small_image_url
IS 'Profile small image url';

CREATE UNIQUE INDEX profiles_alias_uq ON profiledb.profiles
  USING btree (alias);
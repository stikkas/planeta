CREATE TABLE profiledb.comments_votes (
  comment_id BIGINT NOT NULL, 
  owner_profile_id BIGINT NOT NULL, 
  object_id INTEGER NOT NULL, 
  object_type_id INTEGER NOT NULL, 
  author_profile_id BIGINT NOT NULL, 
  vote SMALLINT DEFAULT 0, 
  time_added TIMESTAMP WITHOUT TIME ZONE DEFAULT now() NOT NULL, 
  CONSTRAINT comments_votes_pkey PRIMARY KEY(comment_id, owner_profile_id, author_profile_id)
) WITHOUT OIDS;

ALTER TABLE profiledb.comments_votes
  ALTER COLUMN owner_profile_id SET STATISTICS 0;

COMMENT ON COLUMN profiledb.comments_votes.comment_id
IS 'Comment identifier';

COMMENT ON COLUMN profiledb.comments_votes.owner_profile_id
IS 'Object owner profile identifier';

COMMENT ON COLUMN profiledb.comments_votes.object_id
IS 'Object identifier';

COMMENT ON COLUMN profiledb.comments_votes.object_type_id
IS 'Object_type_identifier';

COMMENT ON COLUMN profiledb.comments_votes.author_profile_id
IS 'Vote author profile identifier';

COMMENT ON COLUMN profiledb.comments_votes.vote
IS 'Comment vote';

COMMENT ON COLUMN profiledb.comments_votes.time_added
IS 'Added time';

CREATE TABLE profiledb.audio_tracks (
  track_id BIGINT NOT NULL,
  owner_profile_id BIGINT NOT NULL,
  author_profile_id BIGINT NOT NULL,
  album_id BIGINT,
  track_name VARCHAR(250) NOT NULL,
  artist_name VARCHAR(250),
  authors TEXT,
  lyrics TEXT,
  track_duration INTEGER NOT NULL,
  track_url VARCHAR(500) NOT NULL,
  listenings_count INTEGER DEFAULT 0 NOT NULL,
  downloads_count INTEGER DEFAULT 0 NOT NULL,
  uploader_profile_id BIGINT DEFAULT 0 NOT NULL,
  uploader_track_id BIGINT DEFAULT 0 NOT NULL,
  uploader_album_id BIGINT,
  has_mp3 BOOLEAN DEFAULT false,
  album_track_number INTEGER DEFAULT 0,
  time_added TIMESTAMP WITHOUT TIME ZONE DEFAULT now() NOT NULL,
  CONSTRAINT group_audio_tracks_pkey PRIMARY KEY(owner_profile_id, track_id)
) WITH OIDS;

COMMENT ON TABLE profiledb.audio_tracks
IS 'Group''s audio tracks';

COMMENT ON COLUMN profiledb.audio_tracks.track_id
IS 'Audiotrack''s identifier';

COMMENT ON COLUMN profiledb.audio_tracks.owner_profile_id
IS 'Group''s identifier';

COMMENT ON COLUMN profiledb.audio_tracks.author_profile_id
IS 'Author user identifier';

COMMENT ON COLUMN profiledb.audio_tracks.album_id
IS 'Audio album identifier';

COMMENT ON COLUMN profiledb.audio_tracks.track_name
IS 'Track name';

COMMENT ON COLUMN profiledb.audio_tracks.artist_name
IS 'Artist name';

COMMENT ON COLUMN profiledb.audio_tracks.authors
IS 'Authors/compositors names';

COMMENT ON COLUMN profiledb.audio_tracks.lyrics
IS 'Lyrics';

COMMENT ON COLUMN profiledb.audio_tracks.track_duration
IS 'Track duration in seconds';

COMMENT ON COLUMN profiledb.audio_tracks.track_url
IS 'Track url';

COMMENT ON COLUMN profiledb.audio_tracks.listenings_count
IS 'Count of listenings';

COMMENT ON COLUMN profiledb.audio_tracks.downloads_count
IS 'Downloads count';

COMMENT ON COLUMN profiledb.audio_tracks.uploader_profile_id
IS 'Uploader''s identifier';

COMMENT ON COLUMN profiledb.audio_tracks.uploader_track_id
IS 'Uploader track identifier';

COMMENT ON COLUMN profiledb.audio_tracks.uploader_album_id
IS 'Uploader album identifier';

COMMENT ON COLUMN profiledb.audio_tracks.album_track_number
IS 'Album track position';

COMMENT ON COLUMN profiledb.audio_tracks.time_added
IS 'Time added';

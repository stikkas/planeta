CREATE TABLE profiledb.comments (
  comment_id BIGINT NOT NULL, 
  parent_comment_id BIGINT NOT NULL, 
  owner_profile_id BIGINT NOT NULL, 
  object_id INTEGER NOT NULL, 
  object_type_id INTEGER NOT NULL, 
  level INTEGER NOT NULL, 
  childs_count INTEGER DEFAULT 0 NOT NULL, 
  author_profile_id BIGINT NOT NULL, 
  time_added TIMESTAMP WITHOUT TIME ZONE DEFAULT now() NOT NULL, 
  is_deleted BOOLEAN DEFAULT false NOT NULL, 
  text TEXT NOT NULL, 
  text_html TEXT NOT NULL, 
  rating INTEGER DEFAULT 0 NOT NULL, 
  CONSTRAINT comments_pkey PRIMARY KEY(owner_profile_id, comment_id)
) WITH OIDS;

COMMENT ON COLUMN profiledb.comments.comment_id
IS 'Comment identifier';

COMMENT ON COLUMN profiledb.comments.parent_comment_id
IS 'Parent comment identifier or 0';

COMMENT ON COLUMN profiledb.comments.owner_profile_id
IS 'Object owner profile identifier';

COMMENT ON COLUMN profiledb.comments.object_id
IS 'Parent object identifier';

COMMENT ON COLUMN profiledb.comments.object_type_id
IS 'Parent object type identifier';

COMMENT ON COLUMN profiledb.comments.level
IS 'Comment level';

COMMENT ON COLUMN profiledb.comments.childs_count
IS 'Child comments count';

COMMENT ON COLUMN profiledb.comments.author_profile_id
IS 'Comment''s author profile identifier';

COMMENT ON COLUMN profiledb.comments.time_added
IS 'Time added';

COMMENT ON COLUMN profiledb.comments.is_deleted
IS 'Is deleted?';

COMMENT ON COLUMN profiledb.comments.text
IS 'Text';

COMMENT ON COLUMN profiledb.comments.text_html
IS 'Text html';

COMMENT ON COLUMN profiledb.comments.rating
IS 'Comment rating';

CREATE INDEX comments_author_profile_id_idx ON profiledb.comments
  USING btree ("author_profile_id");

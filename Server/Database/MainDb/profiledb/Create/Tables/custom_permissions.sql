CREATE TABLE profiledb.custom_permission (
  object_id BIGINT NOT NULL, 
  object_type BIGINT NOT NULL, 
  owner_profile_id BIGINT NOT NULL, 
  client_profile_id BIGINT NOT NULL,
  permission_type INTEGER NOT NULL
) WITHOUT OIDS;

COMMENT ON COLUMN profiledb.custom_permission.object_id
IS 'Object identifier';

COMMENT ON COLUMN profiledb.custom_permission.permission_type
IS 'Like READ WRITE or COMMENT codes';

COMMENT ON COLUMN profiledb.custom_permission.client_profile_id
IS 'Id of member who have access';

COMMENT ON COLUMN profiledb.custom_permission.owner_profile_id
IS 'Id of member whos object is';
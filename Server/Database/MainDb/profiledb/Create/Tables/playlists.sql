CREATE TABLE profiledb.playlists (
  playlist_id INTEGER NOT NULL, 
  owner_profile_id BIGINT NOT NULL, 
  name VARCHAR(250) NOT NULL, 
  tracks_count INTEGER DEFAULT 0 NOT NULL, 
  CONSTRAINT playlists_pkey PRIMARY KEY(owner_profile_id, playlist_id)
) WITH OIDS;

COMMENT ON TABLE profiledb.playlists
IS 'User''s playlists';

COMMENT ON COLUMN profiledb.playlists.playlist_id
IS 'Playlist''s identifier';

COMMENT ON COLUMN profiledb.playlists.owner_profile_id
IS 'Owner profile''s identifier';

COMMENT ON COLUMN profiledb.playlists.name
IS 'Playlist''s name';

COMMENT ON COLUMN profiledb.playlists.tracks_count
IS 'Tracks count';

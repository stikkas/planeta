﻿CREATE TABLE profiledb.country (
  country_id INTEGER NOT NULL, 
  country_name_ru VARCHAR(50) NOT NULL, 
  country_name_en VARCHAR(50) NOT NULL, 
  CONSTRAINT country_pkey PRIMARY KEY(country_id)
) WITHOUT OIDS;

/* Data for the 'profiledb.country' table  (Records 1 - 218) */

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (1, E'Россия', E'Russia');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (2, E'Украина', E'Ukraine');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (3, E'Абхазия', E'Abkhazia');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (4, E'Австралия', E'Australia');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (5, E'Австрия', E'Austria');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (6, E'Азербайджан', E'Azerbaijan');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (7, E'Албания', E'Albania');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (8, E'Алжир', E'Algeria');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (9, E'Ангола', E'Angola');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (10, E'Ангуилья', E'Anguilla');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (11, E'Андорра', E'Andorra');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (12, E'Антигуа и Барбуда', E'Antigua and Barbuda');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (13, E'Антильские о-ва', E'Netherlands Antilles');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (14, E'Аргентина', E'Argentina');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (15, E'Армения', E'Armenia');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (16, E'Арулько', E'Aruba');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (17, E'Афганистан', E'Afghanistan');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (18, E'Багамские о-ва', E'Bahamas');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (19, E'Бангладеш', E'Bangladesh');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (20, E'Барбадос', E'Barbados');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (21, E'Бахрейн', E'Bahrain');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (22, E'Беларусь', E'Belarus');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (23, E'Белиз', E'Belize');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (24, E'Бельгия', E'Belgium');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (25, E'Бенин', E'Benin');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (26, E'Бермуды', E'Bermuda');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (27, E'Болгария', E'Bulgaria');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (28, E'Боливия', E'Bolivia');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (29, E'Босния/Герцеговина', E'Bosnia and Herzegovina');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (30, E'Ботсвана', E'Botswana');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (31, E'Бразилия', E'Brazil');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (32, E'Британские Виргинские о-ва', E'British Virgin Islands');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (33, E'Бруней', E'Brunei');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (34, E'Буркина Фасо', E'Burkina Faso');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (35, E'Бурунди', E'Burundi');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (36, E'Бутан', E'Bhutan');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (37, E'Валлис и Футуна о-ва', E'Wallis and Futuna');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (38, E'Вануату', E'Vanuatu');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (39, E'Великобритания', E'United Kingdom');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (40, E'Венгрия', E'Hungary');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (41, E'Венесуэла', E'Venezuela');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (42, E'Восточный Тимор', E'East Timor');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (43, E'Вьетнам', E'Vietnam');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (44, E'Габон', E'Gabon');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (45, E'Гаити', E'Haiti');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (46, E'Гайана', E'Guyana');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (47, E'Гамбия', E'Gambia');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (48, E'Гана', E'Ghana');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (49, E'Гваделупа', E'Guadeloupe');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (50, E'Гватемала', E'Guatemala');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (51, E'Гвинея', E'Guinea');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (52, E'Гвинея-Бисау', E'Guinea-Bissau');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (53, E'Германия', E'Germany');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (54, E'Гернси о-в', E'Guernsey');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (55, E'Гибралтар', E'Gibraltar');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (56, E'Гондурас', E'Honduras');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (57, E'Гонконг', E'Hong Kong');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (58, E'Гренада', E'Grenada');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (59, E'Гренландия', E'Greenland');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (60, E'Греция', E'Greece');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (61, E'Грузия', E'Georgia');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (62, E'Дания', E'Denmark');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (63, E'Джерси о-в', E'Jersey');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (64, E'Джибути', E'Djibouti');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (65, E'Доминиканская республика', E'Dominican Republic');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (66, E'Египет', E'Egypt');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (67, E'Замбия', E'Zambia');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (68, E'Западная Сахара', E'Western Sahara');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (69, E'Зимбабве', E'Zimbabwe');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (70, E'Израиль', E'Israel');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (71, E'Индия', E'India');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (72, E'Индонезия', E'Indonesia');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (73, E'Иордания', E'Jordan');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (74, E'Ирак', E'Iraq');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (75, E'Иран', E'Iran');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (76, E'Ирландия', E'Ireland');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (77, E'Исландия', E'Iceland');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (78, E'Испания', E'Spain');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (79, E'Италия', E'Italy');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (80, E'Йемен', E'Yemen');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (81, E'Кабо-Верде', E'Cape Verde');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (82, E'Казахстан', E'Kazakhstan');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (83, E'Камбоджа', E'Cambodia');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (84, E'Камерун', E'Cameroon');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (85, E'Канада', E'Canada');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (86, E'Катар', E'Qatar');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (87, E'Кения', E'Kenya');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (88, E'Кипр', E'Cyprus');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (89, E'Кирибати', E'Kiribati');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (90, E'Китай', E'China');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (91, E'Колумбия', E'Colombia');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (92, E'Коморские о-ва', E'Comoros');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (93, E'Конго (Brazzaville)', E'Congo (Brazzaville)');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (94, E'Конго (Kinshasa)', E'Congo (Kinshasa)');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (95, E'Коста-Рика', E'Costa Rica');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (96, E'Кот-д''Ивуар', E'Cote D''Ivoire');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (97, E'Куба', E'Cuba');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (98, E'Кувейт', E'Kuwait');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (99, E'Кука о-ва', E'Cook Islands');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (100, E'Кыргызстан', E'Kyrgyzstan');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (101, E'Лаос', E'Laos');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (102, E'Латвия', E'Latvia');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (103, E'Лесото', E'Lesotho');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (104, E'Либерия', E'Liberia');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (105, E'Ливан', E'Lebanon');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (106, E'Ливия', E'Libya');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (107, E'Литва', E'Lithuania');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (108, E'Лихтенштейн', E'Liechtenstein');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (109, E'Люксембург', E'Luxembourg');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (110, E'Маврикий', E'Mauritius');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (111, E'Мавритания', E'Mauritania');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (112, E'Мадагаскар', E'Madagascar');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (113, E'Македония', E'Macedonia');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (114, E'Малави', E'Malawi');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (115, E'Малайзия', E'Malaysia');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (116, E'Мали', E'Mali');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (117, E'Мальдивские о-ва', E'Maldives');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (118, E'Мальта', E'Malta');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (119, E'Мартиника о-в', E'Martinique');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (120, E'Мексика', E'Mexico');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (121, E'Мозамбик', E'Mozambique');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (122, E'Молдова', E'Moldova');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (123, E'Монако', E'Monaco');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (124, E'Монголия', E'Mongolia');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (125, E'Марокко', E'Morocco');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (126, E'Мьянма (Бирма)', E'Myanmar (Burma)');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (127, E'Мэн о-в', E'Isle of Man');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (128, E'Намибия', E'Namibia');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (129, E'Науру', E'Nauru');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (130, E'Непал', E'Nepal');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (131, E'Нигер', E'Niger');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (132, E'Нигерия', E'Nigeria');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (133, E'Нидерланды (Голландия)', E'Netherlands');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (134, E'Никарагуа', E'Nicaragua');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (135, E'Новая Зеландия', E'New Zealand');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (136, E'Новая Каледония о-в', E'New Caledonia');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (137, E'Норвегия', E'Norway');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (138, E'Норфолк о-в', E'Norfolk Island');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (139, E'О.А.Э.', E'United Arab Emirates');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (140, E'Оман', E'Oman');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (141, E'Пакистан', E'Pakistan');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (142, E'Панама', E'Panama');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (143, E'Папуа Новая Гвинея', E'Papua New Guinea');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (144, E'Парагвай', E'Paraguay');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (145, E'Перу', E'Peru');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (146, E'Питкэрн о-в', E'Pitcairn Islands');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (147, E'Польша', E'Poland');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (148, E'Португалия', E'Portugal');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (149, E'Пуэрто Рико', E'Puerto Rico');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (150, E'Реюньон', E'Reunion');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (151, E'Руанда', E'Rwanda');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (152, E'Румыния', E'Romania');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (153, E'США', E'United States');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (154, E'Сальвадор', E'El Salvador');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (155, E'Самоа', E'Samoa');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (156, E'Сан-Марино', E'San Marino');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (157, E'Сан-Томе и Принсипи', E'Sao Tome and Principe');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (158, E'Саудовская Аравия', E'Saudi Arabia');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (159, E'Свазиленд', E'Swaziland');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (160, E'Святая Люсия', E'Saint Lucia');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (161, E'Святой Елены о-в', E'Saint Helena');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (162, E'Северная Корея', E'North Korea');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (163, E'Сейшеллы', E'Seychelles');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (164, E'Сен-Пьер и Микелон', E'Saint Pierre and Miquelon');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (165, E'Сенегал', E'Senegal');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (166, E'Сент Китс и Невис', E'Saint Kitts and Nevis');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (167, E'Сент-Винсент и Гренадины', E'Saint Vincent and the Grenadines');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (168, E'Сербия', E'Serbia');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (169, E'Сингапур', E'Singapore');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (170, E'Сирия', E'Syria');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (171, E'Словакия', E'Slovakia');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (172, E'Словения', E'Slovenia');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (173, E'Соломоновы о-ва', E'Solomon Islands');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (174, E'Сомали', E'Somalia');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (175, E'Судан', E'Sudan');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (176, E'Суринам', E'Suriname');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (177, E'Сьерра-Леоне', E'Sierra Leone');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (178, E'Таджикистан', E'Tajikistan');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (179, E'Тайвань', E'Taiwan');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (180, E'Таиланд', E'Thailand');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (181, E'Танзания', E'Tanzania');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (182, E'Того', E'Togo');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (183, E'Токелау о-ва', E'Tokelau');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (184, E'Тонга', E'Tonga');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (185, E'Тринидад и Тобаго', E'Trinidad and Tobago');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (186, E'Тувалу', E'Tuvalu');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (187, E'Тунис', E'Tunisia');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (188, E'Туркменистан', E'Turkmenistan');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (189, E'Туркс и Кейкос', E'Turks and Caicos Islands');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (190, E'Турция', E'Turkey');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (191, E'Уганда', E'Uganda');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (192, E'Узбекистан', E'Uzbekistan');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (193, E'Уругвай', E'Uruguay');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (194, E'Фарерские о-ва', E'Faroe Islands');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (195, E'Фиджи', E'Fiji');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (196, E'Филиппины', E'Philippines');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (197, E'Финляндия', E'Finland');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (198, E'Франция', E'France');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (199, E'Французская Гвинея', E'French Guiana');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (200, E'Французская Полинезия', E'French Polynesia');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (201, E'Хорватия', E'Croatia');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (202, E'Чад', E'Chad');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (203, E'Черногория', E'Montenegro');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (204, E'Чехия', E'Czech Republic');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (205, E'Чили', E'Chile');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (206, E'Швейцария', E'Switzerland');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (207, E'Швеция', E'Sweden');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (208, E'Шри-Ланка', E'Sri Lanka');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (209, E'Эквадор', E'Ecuador');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (210, E'Экваториальная Гвинея', E'Equatorial Guinea');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (211, E'Эритрея', E'Eritrea');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (212, E'Эстония', E'Estonia');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (213, E'Эфиопия', E'Ethiopia');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (214, E'ЮАР', E'South Africa');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (215, E'Южная Корея', E'South Korea');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (216, E'Южная Осетия', E'South Ossetia');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (217, E'Ямайка', E'Jamaica');

INSERT INTO profiledb.country ("country_id", "country_name_ru", "country_name_en")
VALUES (218, E'Япония', E'Japan');
CREATE TABLE profiledb.playlist_tracks (
  owner_profile_id BIGINT NOT NULL, 
  playlist_id INTEGER NOT NULL, 
  order_num BIGSERIAL, 
  track_id BIGINT NOT NULL, 
  CONSTRAINT playlist_tracks_pkey PRIMARY KEY(owner_profile_id, playlist_id, order_num)
) WITH OIDS;

COMMENT ON TABLE profiledb.playlist_tracks
IS 'User playlist''s tracks';

COMMENT ON COLUMN profiledb.playlist_tracks.owner_profile_id
IS 'User''s identifier';

COMMENT ON COLUMN profiledb.playlist_tracks.playlist_id
IS 'Playlist''s identifier';

COMMENT ON COLUMN profiledb.playlist_tracks.order_num
IS 'Order number';

COMMENT ON COLUMN profiledb.playlist_tracks.track_id
IS 'Track''s identifier';

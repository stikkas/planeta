CREATE TABLE profiledb.profile_block_settings (
  profile_id BIGINT NOT NULL, 
  block_type_id INTEGER NOT NULL, 
  view_permission INTEGER DEFAULT 0 NOT NULL, 
  comment_permission INTEGER DEFAULT 0 NOT NULL, 
  write_permission INTEGER DEFAULT 0 NOT NULL,
  vote_permission INTEGER DEFAULT 0 NOT NULL,
  add_permission INTEGER DEFAULT 0 NOT NULL,
  block_name VARCHAR(32) DEFAULT NULL,
  CONSTRAINT profile_block_settings_pkey PRIMARY KEY(block_type_id, profile_id)
) WITHOUT OIDS;

COMMENT ON TABLE profiledb.profile_block_settings
IS 'Settings for profile content blocks';

COMMENT ON COLUMN profiledb.profile_block_settings.profile_id
IS 'Profile identifier';

COMMENT ON COLUMN profiledb.profile_block_settings.block_type_id
IS 'Object type (content block) identifier';

COMMENT ON COLUMN profiledb.profile_block_settings.is_enabled
IS 'Is block visible or not on the profile page';

COMMENT ON COLUMN profiledb.profile_block_settings.view_permission
IS 'View permission';

COMMENT ON COLUMN profiledb.profile_block_settings.comment_permission
IS 'Comment permission';

COMMENT ON COLUMN profiledb.profile_block_settings.write_permission
IS 'Who can edit and delete objects';

COMMENT ON COLUMN profiledb.profile_block_settings.add_permission
IS 'Who can create new objects';

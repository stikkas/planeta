CREATE TABLE profiledb.profile_relations (
  profile_id BIGINT NOT NULL, 
  subject_profile_id BIGINT NOT NULL, 
  status INTEGER NOT NULL, 
  CONSTRAINT profile_relations_pkey PRIMARY KEY(profile_id, subject_profile_id)
) WITHOUT OIDS;

COMMENT ON TABLE profiledb.profile_relations
IS 'Represents relation between two profiles';

COMMENT ON COLUMN profiledb.profile_relations.profile_id
IS 'Object of relation';

COMMENT ON COLUMN profiledb.profile_relations.subject_profile_id
IS 'Subject of relation';

COMMENT ON COLUMN profiledb.profile_relations.status
IS 'Relation status';

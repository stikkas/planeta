CREATE TABLE profiledb.profile_backgrounds (
  profile_id BIGINT,
  background_id BIGINT,
  background_image_id BIGINT,
  background_color VARCHAR(50)
) WITHOUT OIDS;

COMMENT ON COLUMN profiledb.profile_backgrounds.profile_id
IS 'Profile''s identifier';

COMMENT ON COLUMN profiledb.profile_backgrounds.background_id
IS 'Background''s identifier';

COMMENT ON COLUMN profiledb.profile_backgrounds.background_image_id
IS 'Image''s identifier';

COMMENT ON COLUMN profiledb.profile_backgrounds.background_color
IS 'Background''s color';
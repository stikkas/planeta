CREATE TABLE profiledb.profile_help_sections (
  profile_id BIGINT NOT NULL,
  section_code INTEGER NOT NULL
) WITHOUT OIDS;

COMMENT ON TABLE profiledb.profile_help_sections
IS 'Visited help sections for specified profile';

COMMENT ON COLUMN profiledb.profile_help_sections.profile_id
IS 'Profile''s identifier';

COMMENT ON COLUMN profiledb.profile_help_sections.section_code
IS 'Section''s code';
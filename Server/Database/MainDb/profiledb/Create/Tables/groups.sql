CREATE TABLE profiledb.groups (
  profile_id BIGINT NOT NULL,
  description TEXT,
  description_html TEXT,
  biography TEXT,
  biography_html TEXT,
  contact_info TEXT,
  site_url VARCHAR(512),
  avatar_rotation_style INTEGER,
  avatar_rotation_period INTEGER,
  join_permission INTEGER,
  download_content_permission INTEGER,
  events_create_permission INTEGER,
  events_order_permission INTEGER,
  members_notification_enabled BOOLEAN,
  background_image_id BIGINT,
  background_rotation_style INTEGER,
  background_rotation_period INTEGER,
  is_merchant BOOLEAN DEFAULT false NOT NULL,
  daily_rotation_style INTEGER,
  daily_rotation_period INTEGER,
  daily_feature_id BIGINT,
  genre TEXT,
  CONSTRAINT groups_pkey PRIMARY KEY(profile_id)
) WITHOUT OIDS;

COMMENT ON TABLE profiledb.groups
IS 'Group''s specific data';

COMMENT ON COLUMN profiledb.groups.profile_id
IS 'Group identifier';

COMMENT ON COLUMN profiledb.groups.description
IS 'Description (bbcode)';

COMMENT ON COLUMN profiledb.groups.description_html
IS 'Description (html)';

COMMENT ON COLUMN profiledb.groups.biography
IS 'Biography (bbcode)';

COMMENT ON COLUMN profiledb.groups.biography_html
IS 'Biography (html)';

COMMENT ON COLUMN profiledb.groups.contact_info
IS 'Contact info';

COMMENT ON COLUMN profiledb.groups.site_url
IS 'Site url';

COMMENT ON COLUMN profiledb.groups.avatar_rotation_style
IS 'Avatar rotation style setting';

COMMENT ON COLUMN profiledb.groups.avatar_rotation_period
IS 'Avatar rotation period setting';

COMMENT ON COLUMN profiledb.groups.join_permission
IS 'Group join permission';

COMMENT ON COLUMN profiledb.groups.download_content_permission
IS 'Group content download permission';

COMMENT ON COLUMN profiledb.groups.events_create_permission
IS 'Group events create permission';

COMMENT ON COLUMN profiledb.groups.events_order_permission
IS 'Group events order permission';

COMMENT ON COLUMN profiledb.groups.members_notification_enabled
IS 'Group members notification';

COMMENT ON COLUMN profiledb.groups.is_merchant
IS 'Group merchant status';

COMMENT ON COLUMN profiledb.groups.daily_rotation_style
IS 'Feature of the day rotation style';

COMMENT ON COLUMN profiledb.groups.daily_rotation_period
IS 'Feature of the day rotation period';

COMMENT ON COLUMN profiledb.groups.daily_feature_id
IS 'Current daily feature identifier, 0 means group doesn''t want this functionality';

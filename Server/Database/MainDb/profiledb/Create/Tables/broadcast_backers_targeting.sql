CREATE TABLE profiledb.broadcast_backers_targeting (
  broadcast_id BIGINT NOT NULL, 
  profile_id BIGINT NOT NULL,
  campaign_id BIGINT NOT NULL,
  limit_summ NUMERIC(10,2) DEFAULT 0 NOT NULL
) WITHOUT OIDS;

COMMENT ON TABLE profiledb.broadcast_backers_targeting
IS 'Profile''s broadcasts backers targeting restrictions';

COMMENT ON COLUMN profiledb.broadcast_backers_targeting.broadcast_id
IS 'Broadcast''s identifier';

COMMENT ON COLUMN profiledb.broadcast_backers_targeting.profile_id
IS 'Broadcast''s Owner''s identifier';

COMMENT ON COLUMN profiledb.broadcast_backers_targeting.campaign_id
IS 'Campaign''s identifier';

COMMENT ON COLUMN profiledb.broadcast_backers_targeting.limit_summ
IS 'Limiting summ on campaigns purchases';
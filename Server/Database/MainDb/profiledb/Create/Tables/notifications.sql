CREATE TABLE profiledb.notifications (
  notification_id BIGINT NOT NULL,
  profile_id BIGINT,
  notification_type_id INTEGER,
  text_html TEXT,
  is_read BOOLEAN,
  time_added TIMESTAMP WITHOUT TIME ZONE DEFAULT now(),
  source_id BIGINT DEFAULT 1 NOT NULL,
  group_id BIGINT DEFAULT 1 NOT NULL,
  group_size BIGINT DEFAULT 0 NOT NULL,
  CONSTRAINT notifications_pkey PRIMARY KEY(notification_id)
) WITH OIDS;

COMMENT ON COLUMN profiledb.notifications.notification_id
IS 'Notification identifier';

COMMENT ON COLUMN profiledb.notifications.profile_id
IS 'Profile identifier';

COMMENT ON COLUMN profiledb.notifications.notification_type_id
IS 'Notification type';

COMMENT ON COLUMN profiledb.notifications.notification_image_url
IS 'Notification image url';

COMMENT ON COLUMN profiledb.notifications.notification_href
IS 'Notification href';

COMMENT ON COLUMN profiledb.notifications.text_html
IS 'Text html';

COMMENT ON COLUMN profiledb.notifications.is_read
IS 'Is read';

COMMENT ON COLUMN profiledb.notifications.time_added
IS 'Time added';

COMMENT ON COLUMN profiledb.notifications.source_id
IS 'Id of notification source (planeta or shop or ...)';

COMMENT ON COLUMN profiledb.notifications.group_id
IS 'ID of grouping object (like blog post id for group of comments)';

COMMENT ON COLUMN profiledb.notifications.group_size
IS 'Number of notifications in notification group';
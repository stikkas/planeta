CREATE TABLE profiledb.daily_features (
  profile_id BIGINT NOT NULL, 
  post_text TEXT, 
  post_text_html TEXT, 
  feature_id BIGINT NOT NULL
) WITHOUT OIDS;

COMMENT ON COLUMN profiledb.daily_features.profile_id
IS 'Profile''s identifer';

COMMENT ON COLUMN profiledb.daily_features.post_text
IS 'Post text(bbcode)';

COMMENT ON COLUMN profiledb.daily_features.post_text_html
IS 'Post text(html)';

COMMENT ON COLUMN profiledb.daily_features.feature_id
IS 'Feature''s identifier';

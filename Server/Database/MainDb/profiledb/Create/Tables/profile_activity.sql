CREATE TABLE profiledb.profile_activity (
  profile_id BIGINT NOT NULL,
  subject_profile_id BIGINT NOT NULL,
  activity_time TIMESTAMP WITHOUT TIME ZONE DEFAULT now() NOT NULL,
  PRIMARY KEY(profile_id, subject_profile_id)
) WITHOUT OIDS;


COMMENT ON COLUMN profiledb.profile_activity.profile_id
IS 'Profile identifier';

COMMENT ON COLUMN profiledb.profile_activity.subject_profile_id
IS 'The companion''s profile id or the community''s profile id';

COMMENT ON COLUMN profiledb.profile_activity.activity_time
IS 'Last time when subject_profile_id''s page was visited';
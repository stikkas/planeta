CREATE TABLE profiledb.global_region (
  region_id INTEGER NOT NULL,
  region_name_ru VARCHAR(50) NOT NULL,
  region_name_en VARCHAR(50) NOT NULL,
  CONSTRAINT global_region_pkey PRIMARY KEY(region_id)
)
WITH (oids = false);

COMMENT ON COLUMN profiledb.global_region.region_id
IS 'Unique identifier';

COMMENT ON COLUMN profiledb.global_region.region_name_ru
IS 'Region Russian name';

COMMENT ON COLUMN profiledb.global_region.region_name_en
IS 'Region English name';

INSERT INTO
  profiledb.global_region
(
  region_id,
  region_name_ru,
  region_name_en
)
VALUES (
  1,
  'СНГ',
  'CIS'
),
(
  2,
  'Дальнее зарубежье',
  'Foreign countries'
);
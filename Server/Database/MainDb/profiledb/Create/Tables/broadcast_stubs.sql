CREATE TABLE profiledb.broadcast_stubs (
  broadcast_id              BIGINT                NOT NULL,
  owner_profile_id          BIGINT                NOT NULL,
  image_url                 VARCHAR(256) DEFAULT NULL :: CHARACTER VARYING,
  image_id                  BIGINT,
  image_custom_description  BOOLEAN DEFAULT FALSE NOT NULL,
  image_description         TEXT DEFAULT NULL,
  image_description_html    TEXT DEFAULT NULL,
  paused_image_url          VARCHAR(256) DEFAULT NULL :: CHARACTER VARYING,
  paused_image_id           BIGINT,
  paused_custom_description BOOLEAN DEFAULT FALSE NOT NULL,
  paused_description        TEXT DEFAULT NULL,
  paused_description_html   TEXT DEFAULT NULL,
  finished_image_url        VARCHAR(256) DEFAULT NULL :: CHARACTER VARYING,
  finished_image_id         BIGINT,
  finished_custom_description BOOLEAN DEFAULT FALSE NOT NULL,
  finished_description      TEXT DEFAULT NULL,
  finished_description_html TEXT DEFAULT NULL,
  closed_image_url          VARCHAR(256) DEFAULT NULL :: CHARACTER VARYING,
  closed_image_id           BIGINT,
  closed_custom_description BOOLEAN DEFAULT FALSE NOT NULL,
  closed_description        TEXT DEFAULT NULL,
  closed_description_html   TEXT DEFAULT NULL,
  CONSTRAINT broadcast_stubs_pkey PRIMARY KEY (broadcast_id)
) WITHOUT OIDS;

COMMENT ON TABLE profiledb.broadcast_stubs
IS 'Profile''s broadcast stubs';

COMMENT ON COLUMN profiledb.broadcast_stubs.image_url
IS 'Image url for not started state';

COMMENT ON COLUMN profiledb.broadcast_stubs.image_id
IS 'Image identifier for not started state';

COMMENT ON COLUMN profiledb.broadcast_stubs.image_custom_description
IS 'Flag to use custom description for not started state';

COMMENT ON COLUMN profiledb.broadcast_stubs.image_description
IS 'Image description for not started state';

COMMENT ON COLUMN profiledb.broadcast_stubs.image_description_html
IS 'Image description html for not started state';

COMMENT ON COLUMN profiledb.broadcast_stubs.paused_image_url
IS 'Image url for paused state';

COMMENT ON COLUMN profiledb.broadcast_stubs.paused_image_id
IS 'Image id for paused state';

COMMENT ON COLUMN profiledb.broadcast_stubs.finished_image_url
IS 'Image url for finished state';

COMMENT ON COLUMN profiledb.broadcast_stubs.finished_image_id
IS 'Image url for finished state';

COMMENT ON COLUMN profiledb.broadcast_stubs.closed_image_url
IS 'Image url for not allowed viewers';

COMMENT ON COLUMN profiledb.broadcast_stubs.closed_image_id
IS 'Image url for not allowed viewers';
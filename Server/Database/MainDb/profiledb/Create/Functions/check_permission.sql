CREATE OR REPLACE FUNCTION profiledb.check_permission (
 p_object_id BIGINT, -- Object being checked
 p_object_type INTEGER,
 p_profile_id BIGINT, -- Object owner profile identifier
 p_client_id BIGINT, -- Client profile id (user that requests this object)
 p_client_permission_level INTEGER, -- Permission level of the client user for object owner profile
 p_object_permission_level INTEGER, -- Requested object permission level
 p_permission_type INTEGER -- Permission type (1 -- view permission)
)
RETURNS BOOLEAN AS
$body$
BEGIN
	-- If client is ADMIN to object owner profile -- ignoring custom_permission setting
	-- If object_permission is not custom -- just comparing object and client permission levels
    IF (p_object_permission_level != 38)
    OR (p_client_permission_level >= 40) THEN
  		RETURN (p_object_permission_level <= p_client_permission_level);
    ELSE
  
  	-- Checking if custom permission is set for the specified client
  		RETURN (EXISTS(
       		SELECT 1
         	  FROM profiledb.custom_permission t1
      		 WHERE t1.object_id = p_object_id
          	   AND t1.object_type = p_object_type
          	   AND t1.owner_profile_id = p_profile_id
          	   AND t1.client_profile_id = p_client_id 
          	   AND t1.permission_type = p_permission_type
    	));
    END IF;    
END;
$body$
LANGUAGE 'plpgsql';	
CREATE OR REPLACE FUNCTION profiledb.check_relation_status (
  p_status integer,
  p_relation_status text
)
RETURNS boolean AS
$body$
	SELECT $1 & t1.relation_status = t1.relation_status
      FROM (
	SELECT CASE WHEN lower($2) = 'not_set' THEN 0
    			WHEN lower($2) = 'self' THEN 1
                WHEN lower($2) = 'no_relation' THEN 2
                WHEN lower($2) = 'request_in' THEN 4
                WHEN lower($2) = 'request_out' THEN 8
                WHEN lower($2) = 'active' THEN 16
                WHEN lower($2) = 'blocked_by_me' THEN 64
                WHEN lower($2) = 'blocked_by_other' THEN 128
                WHEN lower($2) = 'admin' THEN 1024
                WHEN lower($2) = 'artist' THEN 2048
    		END AS relation_status
    ) t1
$body$
LANGUAGE 'sql'
IMMUTABLE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;

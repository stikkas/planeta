CREATE OR REPLACE FUNCTION profiledb.get_database_node (
  p_id bigint
)
RETURNS text AS
$body$
	SELECT CASE WHEN $1 % 2 = 0 
    			THEN 'host=127.0.0.1 dbname=userdb01'
    			ELSE 'host=127.0.0.1 dbname=userdb02'
      		END AS connstr;
$body$
LANGUAGE 'sql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;

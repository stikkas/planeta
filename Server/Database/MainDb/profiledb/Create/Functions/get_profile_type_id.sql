CREATE OR REPLACE FUNCTION profiledb.get_profile_type_id (
  v_profile_type text
)
RETURNS integer AS
$body$
	SELECT CASE WHEN lower($1) = 'user' THEN 1
    		    WHEN lower($1) = 'group' THEN 2
                WHEN lower($1) = 'event' THEN 3
     	    END AS profile_type_id;
$body$
LANGUAGE 'sql'
IMMUTABLE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;

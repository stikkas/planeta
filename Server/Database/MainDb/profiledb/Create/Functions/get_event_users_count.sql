CREATE OR REPLACE FUNCTION profiledb.get_event_users_count (
  p_profile_id bigint
)
RETURNS bigint AS
$body$
	SELECT cnt
      FROM dblink(profiledb.get_database_node($1),
      			  'SELECT count(*)
                     FROM profiledb.profile_relations t1
                     JOIN profiledb.profiles t2
                       ON t1.subject_profile_id = t2.profile_id
                      AND t2.profile_type_id = profiledb.get_profile_type_id(''user'')
                    WHERE t1.profile_id = ' || $1 || '
                      AND profiledb.check_relation_status(t1.status, ''active'');')
                      AS (cnt BIGINT);
$body$
LANGUAGE 'sql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;

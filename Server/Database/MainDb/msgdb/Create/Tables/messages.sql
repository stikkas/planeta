CREATE TABLE msgdb.messages (
  dialog_id BIGINT NOT NULL, 
  message_id BIGINT NOT NULL, 
  user_id BIGINT NOT NULL, 
  message_text TEXT NOT NULL, 
  message_text_html TEXT NOT NULL, 
  is_deleted BOOLEAN DEFAULT false NOT NULL, 
  time_added TIMESTAMP WITHOUT TIME ZONE DEFAULT now() NOT NULL, 
  CONSTRAINT messages_pkey PRIMARY KEY(dialog_id, message_id)
) WITH OIDS;

COMMENT ON TABLE msgdb.messages
IS 'Dialog messages';

COMMENT ON COLUMN msgdb.messages.dialog_id
IS 'Dialog identifier';

COMMENT ON COLUMN msgdb.messages.message_id
IS 'Message identifier';

COMMENT ON COLUMN msgdb.messages.user_id
IS 'Author user identifier';

COMMENT ON COLUMN msgdb.messages.message_text
IS 'Message text (bb-code)';

COMMENT ON COLUMN msgdb.messages.message_text_html
IS 'Transformed bb-code';

COMMENT ON COLUMN msgdb.messages.is_deleted
IS 'Is this message is marked as deleted';

CREATE TABLE msgdb.dialog_users (
  dialog_id BIGINT NOT NULL, 
  user_id BIGINT NOT NULL, 
  time_added TIMESTAMP WITHOUT TIME ZONE DEFAULT now() NOT NULL, 
  CONSTRAINT dialog_users_pkey PRIMARY KEY(dialog_id, user_id)
) WITH OIDS;

COMMENT ON TABLE msgdb.dialog_users
IS 'List of users taking part in the dialog';

COMMENT ON COLUMN msgdb.dialog_users.dialog_id
IS 'Dialog identifier';

COMMENT ON COLUMN msgdb.dialog_users.user_id
IS 'Participant user identifier';

COMMENT ON COLUMN msgdb.dialog_users.time_added
IS 'now()';

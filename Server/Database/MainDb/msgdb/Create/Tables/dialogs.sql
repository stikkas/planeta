CREATE TABLE msgdb.dialogs (
  dialog_id BIGINT NOT NULL, 
  name VARCHAR(256), 
  creator_user_id BIGINT NOT NULL, 
  last_message_id BIGINT NOT NULL, 
  messages_count INTEGER DEFAULT 0 NOT NULL, 
  time_added TIMESTAMP WITHOUT TIME ZONE DEFAULT now() NOT NULL, 
  CONSTRAINT dialogs_pkey PRIMARY KEY(dialog_id)
) WITH OIDS;

COMMENT ON TABLE msgdb.dialogs
IS 'Table containing dialogs';

COMMENT ON COLUMN msgdb.dialogs.dialog_id
IS 'Dialog identifier';

COMMENT ON COLUMN msgdb.dialogs.name
IS 'Dialog name';

COMMENT ON COLUMN msgdb.dialogs.creator_user_id
IS 'Identifier of the user created this dialog';

COMMENT ON COLUMN msgdb.dialogs.last_message_id
IS 'Last dialog message identifier';

COMMENT ON COLUMN msgdb.dialogs.messages_count
IS 'Count of messages in a dialog';

COMMENT ON COLUMN msgdb.dialogs.time_added
IS 'Time when dialog was created';

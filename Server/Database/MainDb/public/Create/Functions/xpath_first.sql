CREATE OR REPLACE FUNCTION public.xpath_first(IN p_xpath TEXT, IN p_xml TEXT)
RETURNS text AS
$$
BEGIN
	RETURN (xpath(p_xpath, p_xml::xml)::TEXT[])[1];
EXCEPTION
WHEN OTHERS THEN
	RAISE NOTICE 'Error while doing xpath: %', SQLERRM;
    RETURN '';
END;
$$ LANGUAGE 'plpgsql';
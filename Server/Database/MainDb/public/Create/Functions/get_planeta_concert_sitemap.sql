CREATE OR REPLACE FUNCTION public.get_planeta_concert_sitemap (
  host text = 'planeta.ru'::text,
  records_offset bigint = 0,
  records_limit bigint = 50000
)
RETURNS text AS
$body$
DECLARE
  result TEXT;
BEGIN
SELECT E'<?xml version="1.0" encoding="UTF-8"?>\n' ||
xmlelement(name "urlset", XMLATTRIBUTES(E'http://www.sitemaps.org/schemas/sitemap/0.9' as "xmlns",
           E'http://www.w3.org/1999/xhtml' as "xmlns:xhtml"),
       xmlagg(xmlelement(name "url",
		xmlelement(name "loc", loc::xml),
    CASE
    WHEN mobile_url is not NULL THEN
      xmlelement(name "xhtml:link", XMLATTRIBUTES('alternate' as "rel", 'only screen and (max-width: 640px)' as "media", mobile_url as "href"))
              END) ORDER BY x.profile_id ASC))
        INTO result
		FROM
(
	  SELECT p.profile_id,
	 		 E'http://concert.'||host||E'/concerts/'|| p.profile_id::text as loc,
      E'http://m.'||host||E'/'||p.profile_id::text || '/profile.html' AS mobile_url
   		FROM profiledb.profiles p
	   WHERE p.profile_type_id = 3 --event
         AND p.status not in (0,2)
         AND p.event_type = 1 --concert
) x
OFFSET records_offset LIMIT records_limit;
result := REGEXP_REPLACE(result, E'><',E'>\n<','ig');
result := REGEXP_REPLACE(result, E'\n<(/?url>|loc)',E'\n\t<\\1','ig');
result := REGEXP_REPLACE(result, E'\t<(/?loc)',E'\t\t<\\1','ig');
RETURN result;
END;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
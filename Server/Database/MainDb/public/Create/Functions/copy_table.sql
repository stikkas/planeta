CREATE OR REPLACE FUNCTION public.copy_table (
  p_source_db text,
  p_source_table text,
  p_source_host text,
  p_source_db_user text,
  p_source_db_pwd text
)
RETURNS pg_catalog.void AS
$body$
DECLARE
	v_create_script text;
    v_schema_name text;
    v_table_name text;
    v_dest_table text;
    v_create_table_script text;
    v_create_constraints_script text;
    v_fields text;
    v_matches text[];
    v_rename_table_script text :=
'DROP TABLE IF EXISTS [SCHEMA].[SRC_TABLE]; ' ||
'ALTER TABLE [DEST_TABLE] RENAME TO [SRC_TABLE];';
	v_dblink text :=
'INSERT INTO [DEST_TABLE] ' ||
'SELECT * FROM public.dblink (''dbname=[DB_NAME] port=[PORT] ' ||
'			 		     host=[HOST] user=[USER] password=[PWD]''::TEXT, ' ||
' 					   ''SELECT * FROM [SCHEMA].[SRC_TABLE];''::TEXT) AS [FIELDS]';
BEGIN
	IF p_source_table NOT LIKE '%.%' THEN
    	v_schema_name := 'public';
        v_table_name := p_source_table;
	ELSE
    	v_schema_name := (regexp_split_to_array(p_source_table, E'\\.')::TEXT[])[1];
    	v_table_name := (regexp_split_to_array(p_source_table, E'\\.')::TEXT[])[2];          
    END IF;
    v_dest_table := v_schema_name || '.tmp_' || v_table_name;

	v_create_script := extract_ddl(p_source_table, p_source_db, p_source_host, p_source_db_user, p_source_db_pwd);
    v_create_script := replace(v_create_script, 'character varying', 'varchar');
	v_create_script := regexp_replace(v_create_script, ' DEFAULT nextval\(.*?\)', '');
    v_create_script := regexp_replace(v_create_script, 'CREATE TRIGGER .*?;', '');

	SELECT regexp_matches(v_create_script, E'((\s|.)*?CREATE TABLE ((\s|.)*?(CONSTRAINT|\\));))', 'gi')
      INTO v_matches;
    
    v_fields := regexp_replace(v_matches[3], '(NOT NULL)|(NULL)|( DEFAULT [^,]*?)', '', 'gi');
    IF (v_fields LIKE '%CONSTRAINT%') THEN
	    v_fields := regexp_replace(v_fields, ',\s*CONSTRAINT.*', '');
    END IF;
    IF (v_fields NOT LIKE '%);') THEN
        v_fields := v_fields || ');';
    END IF;

    v_create_table_script := replace(v_matches[1], 'CREATE TABLE ' || v_table_name, 'CREATE TABLE ' || v_dest_table);
    v_create_table_script := 'DROP TABLE IF EXISTS ' || v_dest_table || '; ' || v_create_table_script;
    
    v_create_constraints_script := regexp_replace(v_create_script, E'((\s|.)*?CREATE TABLE (\s|.)*?\\);)', '', 'gi');
    
    v_rename_table_script := replace(v_rename_table_script, '[DEST_TABLE]', v_dest_table);
    v_rename_table_script := replace(v_rename_table_script, '[SCHEMA]', v_schema_name);
    v_rename_table_script := replace(v_rename_table_script, '[SRC_TABLE]', v_table_name);
    
    v_dblink := replace(v_dblink, '[DEST_TABLE]', v_dest_table);
    v_dblink := replace(v_dblink, '[SCHEMA]', v_schema_name);
    v_dblink := replace(v_dblink, '[DB_NAME]', p_source_db);
    v_dblink := replace(v_dblink, '[PORT]', '5432');
    v_dblink := replace(v_dblink, '[HOST]', p_source_host);
    v_dblink := replace(v_dblink, '[USER]', p_source_db_user);
    v_dblink := replace(v_dblink, '[PWD]', p_source_db_pwd);
    v_dblink := replace(v_dblink, '[SRC_TABLE]', v_table_name);
    v_dblink := replace(v_dblink, '[FIELDS]', v_fields);

    RAISE NOTICE '%', v_create_table_script;
    RAISE NOTICE '%', v_create_constraints_script;
    RAISE NOTICE '%', v_rename_table_script;    
    RAISE NOTICE '%', v_dblink;
    
    EXECUTE v_create_table_script;
    EXECUTE v_dblink;
    EXECUTE v_rename_table_script;
    EXECUTE v_create_constraints_script;
END;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
CREATE OR REPLACE FUNCTION get_photo_sitemap (host text = E'planeta.ru'::text)
  RETURNS text AS
  $body$
  DECLARE
    result TEXT;
  BEGIN
    select E'<?xml version="1.0" encoding="UTF-8"?>\n' ||
           xmlelement(name "urlset", XMLATTRIBUTES(E'http://www.sitemaps.org/schemas/sitemap/0.9' as "xmlns",
                      E'http://www.google.com/schemas/sitemap-image/1.1' as "xmlns:image"),
                      xmlagg(url_tag_value::xml)) INTO result from
                              (select p.profile_id, pa.album_id,
                                 xmlelement(name "url",
                                            xmlelement(name "loc",
                                                       E'http://'||host::text||'/'|| COALESCE(p.alias, p.profile_id::text)|| '/albums/' || pa.album_id::text ), xmlagg(img.imgs::xml)) as url_tag_value
                               from profiledb.profiles p
                                 join profiledb.photo_albums pa on pa.owner_profile_id = p.profile_id
                                 join (
                                        SELECT phs.album_id, xmlelement(name "image:image", xmlelement(name "image:loc", phs.image_url)) as imgs
                                        FROM profiledb.photos phs
                                        WHERE true
                                      ) img ON img.album_id = pa.album_id
                               WHERE pa.view_permission = 1
                                     AND p.profile_type_id = 2 --group
                                     AND p.status not in (2,5,6)
                               GROUP BY p.profile_id, pa.album_id, p.alias
                               HAVING count(img.*) > 0) x;
RETURN result;
END;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
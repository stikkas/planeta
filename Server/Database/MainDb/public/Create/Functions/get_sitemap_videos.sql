CREATE OR REPLACE FUNCTION public.get_sitemap_videos (
  host text = E'planeta.ru'::text
)
  RETURNS text AS
  $body$
  -- version 0.2
  DECLARE
    result TEXT;
  BEGIN
    SELECT E'<?xml version="1.0" encoding="UTF-8"?>\n' ||
           xmlelement(name "urlset",
                      XMLATTRIBUTES(
                      E'http://www.sitemaps.org/schemas/sitemap/0.9' as "xmlns",
                      E'http://www.google.com/schemas/sitemap-video/1.1' as "xmlns:video"),
                      xmlagg(
                          xmlelement(name "url",
                                     xmlelement(name "loc", loc),
                                     xmlelement(name "video:video",
                                                xmlelement(name "video:thumbnail_loc",video_thumbnail_loc),
                                                xmlelement(name "video:title", video_title),
                                                xmlelement(name "video:description", video_description),
                                                xmlelement(name "video:player_loc", video_player_loc),
                                                xmlelement(name "video:duration", video_duration),
                                                xmlelement(name "video:view_count", video_view_count),
                                                xmlelement(name "video:gallery_loc",  XMLATTRIBUTES(video_gallery_loc_title AS "title"), video_gallery_loc),
                                                xmlelement(name "video:uploader", XMLATTRIBUTES( video_uploader_info AS "info"), video_uploader)
                                     )
                          )
                      )
           ) INTO result
    FROM
      (SELECT E'http://tv.'||host||E'/video-frame?profileId='||v.owner_profile_id||E'&videoId='||v.video_id AS video_player_loc,
              E'http://tv.'||host||E'/video/'||v.owner_profile_id::text||E'/'||v.video_id::text AS loc,
              v.image_url AS video_thumbnail_loc,
              v.duration AS video_duration,
              E'http://tv.'||host||E'/'||v.owner_profile_id::text||'/video' AS video_gallery_loc,
              p.display_name AS video_gallery_loc_title,
              substring(v.name from 1 for 100) AS video_title,
              substring(v.description from 1 for 2048) AS video_description,
              p.display_name AS video_uploader,
              E'http://tv.'||host||E'/'||p.profile_id::text AS video_uploader_info,
              v.views_count AS video_view_count
       FROM profiledb.videos AS v
         JOIN profiledb.profiles p on p.profile_id = v.owner_profile_id
       WHERE v.video_type = 1 and v.image_url is not NULL
             AND v.view_permission = 1
      ) x;
    result := REGEXP_REPLACE(result, E'><',E'>\n<','ig');
    RETURN result;
  END
  $body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
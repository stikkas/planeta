CREATE OR REPLACE FUNCTION grant_to_all_sequences(IN p_schema TEXT, IN p_privilege TEXT, p_user TEXT)
RETURNS void AS
$$
DECLARE
	rec record;
BEGIN
	FOR rec IN (SELECT t1.sequence_schema || '.' || t1.sequence_name AS sequence_name
				  FROM information_schema.sequences t1
				 WHERE t1.sequence_schema = $1
	) LOOP
		
		EXECUTE 'GRANT ' || p_privilege || ' ON ' || rec.sequence_name || ' TO ' || p_user;
    
    END LOOP;
END;
$$ LANGUAGE 'plpgsql';
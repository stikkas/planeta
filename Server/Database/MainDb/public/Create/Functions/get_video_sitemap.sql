CREATE OR REPLACE FUNCTION get_video_sitemap (host text = E'planeta.ru'::text)
  RETURNS text AS
  $body$
  DECLARE
    result TEXT;
  BEGIN
    select E'<?xml version="1.0" encoding="UTF-8"?>\n<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"\n' ||
           E'xmlns:video="http://www.google.com/schemas/sitemap-video/1.1">\n' ||
           xmlagg(url::xml) || '</urlset>' into result from (
            (select t1.profile_id, E'<url>\n\t<loc>http://tv.'||host||'/video/' || COALESCE(alias, t1.profile_id::text) || '/'
                                   || t2.video_id || E'</loc>\n\t<video:video>\n\t\t'
                                   || xmlelement(name "video:thumbnail_loc", t2.image_url) || E'\n\t\t'
                                   || xmlelement(name "video:title", t2.name) || E'\n\t\t'
                                   || xmlelement(name "video:description", t2.description) || E'\n\t\t'
                                   || xmlelement(name "video:content_loc", t2.video_url) || E'\n\t\t'
                                   || xmlelement(name "video:duration", t2.duration) || E'\n\t'
                                   || E'</video:video>\n</url>\n' as url
             from profiledb.profiles t1 join profiledb.videos  t2 on t1.profile_id = t2.owner_profile_id
             where t2.video_type = 1 and t2.image_url is not NULL)
             order by profile_id, url asc) videos;
RETURN result;
END;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
CREATE OR REPLACE FUNCTION public.dynamic_query_for_commoncluster (
  input_query text
)
RETURNS SETOF record AS
$body$
DECLARE
    rec RECORD;
BEGIN
    FOR rec IN EXECUTE input_query
    LOOP
        RETURN NEXT rec;
    END LOOP;
    RETURN;
END;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100 ROWS 1000;

CREATE OR REPLACE FUNCTION public.dynamic_query_for_planetacluster (
  input_query text,
  fake_input integer = 0
)
  RETURNS SETOF record AS
  $body$
DECLARE
    rec RECORD;
BEGIN
    FOR rec IN EXECUTE input_query
    LOOP
        RETURN NEXT rec;
    END LOOP;
    RETURN;
END;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100 ROWS 1000;
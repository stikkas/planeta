CREATE OR REPLACE FUNCTION public.get_planeta_shop_sitemap (
        host text = 'shop.planeta.ru'::text,
        records_offset bigint = 0,
        records_limit bigint = 50000
        )
        RETURNS text AS
        $body$
        DECLARE
          result TEXT;
        BEGIN
          SELECT
            E'<?xml version="1.0" encoding="UTF-8"?>\n' ||
            xmlelement(name "urlset", XMLATTRIBUTES (
                       E'http://www.sitemaps.org/schemas/sitemap/0.9' as "xmlns",
                       E'http://www.w3.org/1999/xhtml' as "xmlns:xhtml"
                       ),
                       xmlagg(xmlelement(name "url",
                             xmlelement(name "loc", url :: xml),
                             CASE
                             WHEN mobile_url is not NULL THEN
                               xmlelement(name "xhtml:link", XMLATTRIBUTES ('alternate' as "rel",
                                          'only screen and (max-width: 640px)' as "media", mobile_url as
                                          "href"))
                             END
                       )))
          INTO result
          FROM ((
            SELECT E'http://'||host||E'/delivery' AS url, null as mobile_url
          )
          UNION
          (
            SELECT E'http://'||host||E'/payment' AS url, null as mobile_url
          )
          UNION
          (
            SELECT E'http://'||host||E'/exchange' AS url, null as mobile_url
          )
          UNION
          (
            SELECT E'http://'||host||E'/contacts' AS url, null as mobile_url
          )
          UNION
          (
            SELECT E'http://'||host||E'/faq' AS url, null as mobile_url
          )
          UNION
          (
            SELECT E'http://'||host||E'/offer' AS url, null as mobile_url
          )
          UNION
          (
            SELECT E'http://'||host||E'/products/'||p.product_id AS url, null as mobile_url
            FROM shopdb.products p
            WHERE p.product_state in (1, 3)
            AND p.product_status = 1
          )
          UNION
          (
            SELECT DISTINCT E'http://'||host||E'/group/products/'||p.store_id AS url, null as mobile_url
            FROM shopdb.products p
            WHERE p.product_state in (1, 3)
            AND p.product_status = 1
          )) x
          OFFSET records_offset LIMIT records_limit;
          result := REGEXP_REPLACE(result, E'><', E'>\n<', 'ig');
          result := REGEXP_REPLACE(result, E'\n<(/?url>|loc)', E'\n\t<\\1', 'ig');
          result := REGEXP_REPLACE(result, E'\t<(/?loc)', E'\t\t<\\1', 'ig');
          RETURN result;
        END;
        $body$
        LANGUAGE 'plpgsql'
        VOLATILE
        CALLED ON NULL INPUT
        SECURITY INVOKER
        COST 100;
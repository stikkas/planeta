CREATE OR REPLACE FUNCTION grant_to_all_tables(IN p_schema TEXT, IN p_privilege TEXT, p_user TEXT)
RETURNS void AS
$$
DECLARE
	rec record;
BEGIN
	FOR rec IN (SELECT t1.table_schema || '.' || t1.table_name AS table_name
				  FROM information_schema.tables t1
				 WHERE t1.table_schema = $1
	) LOOP
		
		EXECUTE 'GRANT ' || p_privilege || ' ON ' || rec.table_name || ' TO ' || p_user;
    
    END LOOP;
END;
$$ LANGUAGE 'plpgsql';
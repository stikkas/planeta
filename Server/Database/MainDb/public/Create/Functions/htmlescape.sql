CREATE OR REPLACE FUNCTION htmlescape (IN string text)
RETURNS text AS
$$
use HTML::Entities;
my $string=$_[0];
return encode_entities($string);
$$ LANGUAGE plperlu IMMUTABLE;

CREATE OR REPLACE FUNCTION htmlunescape (IN string text)
  RETURNS text AS
  $$
use HTML::Entities;
my $string=$_[0];
return decode_entities($string);
$$ LANGUAGE plperlu IMMUTABLE;
CREATE OR REPLACE FUNCTION public.extract_ddl (
  table_name text,
  db_name text,
  host text,
  user_name text,
  pwd text
)
RETURNS text AS
$body$
	my $table_name = $_[0];
    my $db_name = $_[1];
    my $host = $_[2];
    my $user_name = $_[3];
    my $pwd = $_[4];
	my $str = `PGPASSWORD=$pwd && export PGPASSWORD && pg_dump -s -t $table_name -h $host -U $user_name $db_name`;
    return $str;
$body$
LANGUAGE 'plperlu'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;

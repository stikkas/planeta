CREATE OR REPLACE FUNCTION public.upload_page (
  text,
  text
)
RETURNS text AS
$body$
use HTTP::Request::Common qw(POST);  
use LWP::UserAgent; 
my $url = $_[0];
my $data = $_[1];

$ua = LWP::UserAgent->new();  
my $req = POST $url, [ data=> $data ];
$content = $ua->request($req)->decoded_content();
return $content;
$body$
LANGUAGE 'plperlu'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;

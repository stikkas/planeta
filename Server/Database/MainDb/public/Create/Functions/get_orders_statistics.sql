CREATE OR REPLACE FUNCTION public.get_orders_statistics (
  start_timestamp timestamp = '2012-01-01',
  finish_timestamp timestamp = '2015-01-01'
)
  RETURNS table (buyers_with BIGINT, shares BIGINT, products BIGINT, donates BIGINT, tickets BIGINT, total BIGINT) AS
  $body$
BEGIN
return query (select count(t1.buyer_id) as buyers_with, coalesce(shares_count, 0) as shares, coalesce(products_count,0) as products, coalesce(donate_count,0) as donates, coalesce(ticket_count,0) as tickets, total_count as total from
(select buyer_id, count(order_id) as total_count from commondb.orders where order_type > 0 and time_updated >= start_timestamp and time_updated < finish_timestamp and payment_status = 1 group by buyer_id) t1
left join (select buyer_id, count(order_id) as products_count from commondb.orders where order_type = 2 and time_updated >= start_timestamp and time_updated < finish_timestamp and payment_status = 1 group by buyer_id) t2  on t1.buyer_id = t2.buyer_id
left join (select buyer_id, count(order_id) as donate_count from commondb.orders where order_type = 3  and time_updated >= start_timestamp and time_updated < finish_timestamp and payment_status = 1 group by buyer_id) t3  on t1.buyer_id = t3.buyer_id
left join (select buyer_id, count(order_id) as ticket_count from commondb.orders where order_type = 5  and time_updated >= start_timestamp and time_updated < finish_timestamp and payment_status = 1 group by buyer_id) t5  on t1.buyer_id = t5.buyer_id
left join (select buyer_id, count(order_id) as shares_count from commondb.orders where order_type = 1  and time_updated >= start_timestamp and time_updated < finish_timestamp and payment_status = 1 group by buyer_id) t6  on t1.buyer_id = t6.buyer_id
where total_count > 0 group by shares, products, donates, tickets, total_count order by total_count desc);
END;
$body$
LANGUAGE 'plpgsql';

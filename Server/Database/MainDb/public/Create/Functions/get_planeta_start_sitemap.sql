CREATE OR REPLACE FUNCTION public.get_planeta_start_sitemap (
  host text = 'planeta.ru'::text,
  results_offset bigint = 0,
  results_limit bigint = 50000
)
RETURNS text AS
$body$
DECLARE
  result TEXT;
BEGIN
SELECT E'<?xml version="1.0" encoding="UTF-8"?>\n' ||
xmlelement(name "urlset", XMLATTRIBUTES(E'http://www.sitemaps.org/schemas/sitemap/0.9' as "xmlns",
           E'http://www.w3.org/1999/xhtml' as "xmlns:xhtml"),
       xmlagg(xmlelement(name "url",
		xmlelement(name "loc", loc::xml),
    CASE
    WHEN mobile_url is not NULL THEN
      xmlelement(name "xhtml:link", XMLATTRIBUTES('alternate' as "rel", 'only screen and (max-width: 640px)' as "media", mobile_url as "href"))
    END) ORDER BY x.campaign_id ASC)
)
  INTO result
  FROM
(
 SELECT c.campaign_id,
   	    E'http://start.'||host||E'/campaigns/'||c.campaign_id  AS loc,
        E'http://m.'||host||E'/campaigns/'||c.campaign_id || '.html' AS mobile_url
   FROM commondb.campaigns c
  WHERE c.status IN (4,5)
  UNION
  SELECT 0 AS campaign_id, E'http://start.'||host||E'/' AS loc, null as mobile_url
  UNION
  SELECT 0 AS campaign_id, E'http://start.'||host||E'/crowdfunding' AS loc, null as mobile_url
  UNION
  SELECT 0 AS campaign_id, E'http://start.'||host||E'/funding-rules' AS loc, null as mobile_url
) x
OFFSET results_offset LIMIT results_limit;
result := REGEXP_REPLACE(result, E'><',E'>\n<','ig');
result := REGEXP_REPLACE(result, E'\n<(/?url>|loc)',E'\n\t<\\1','ig');
result := REGEXP_REPLACE(result, E'\t<(/?loc)',E'\t\t<\\1','ig');
RETURN result;
END;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
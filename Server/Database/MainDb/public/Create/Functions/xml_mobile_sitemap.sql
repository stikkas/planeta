-- create or replace view remote_campaigns as
--   select *
--   from (select campaign_id, profile_id  from commondb.campaigns)
--     as t1;
--
-- create or replace view remote_campaigns as
--   select *
--   from dblink('dbname=planeta_common', 'select campaign_id, profile_id  from commondb.campaigns')
--     as t1(campaign_id BIGINT, profile_id BIGINT);
GRANT SELECT ON remote_campaigns TO planeta;



CREATE OR REPLACE FUNCTION public.xml_mobile_sitemap (
  host text = 'planeta.ru'::text,
  results_offset bigint = 0,
  results_limit bigint = 50000
)
  RETURNS text AS
  $body$
  DECLARE
    result text;
  BEGIN
    SELECT E'<?xml version="1.0" encoding="UTF-8"?>\n' ||
           xmlelement(name "urlset", XMLATTRIBUTES(
                      E'http://www.sitemaps.org/schemas/sitemap/0.9' AS "xmlns",
                      E'http://www.google.com/schemas/sitemap-mobile/1.0' AS "xmlns:mobile"),
                      xmlagg(xmlelement(name "url",
                                        xmlelement(name "loc", url),
                                        xmlelement(name "mobile:mobile")
                             ) ORDER BY x_limited.profile_id ASC))
    INTO result
    FROM (SELECT * FROM
                     ((
                        SELECT p.profile_id,
                          E'http://m.'||host||'/'|| COALESCE(p.alias, p.profile_id::text)|| '/profile' AS url
                        FROM profiledb.profiles p
                        WHERE p.profile_type_id = 3 -- event
                              AND p.status NOT IN (0,2)
                              OR p.profile_type_id = 2 --group
                                 AND p.status IN (2,5,6)
                      )
                      UNION
                      ( SELECT p.profile_id,
                          E'http://m.'||host||'/'|| COALESCE(p.alias, p.profile_id::TEXT)||
                          CASE WHEN pperm.block_type_id = 305 THEN E'/blog'
                          WHEN pperm.block_type_id = 36 THEN E'/forum'
                          END as url
                        FROM profiledb.profiles p
                          JOIN profiledb.profile_block_settings pperm on p.profile_id = pperm.profile_id
                        WHERE pperm.view_permission = 1
                              AND pperm.block_type_id IN (305, 36)
                              AND p.profile_type_id = 2
                              AND p.status in (2,5,6)
                      )
                      UNION
                      (
                        SELECT p.profile_id,
                          E'http://m.'||host||'/'|| COALESCE(p.alias, p.profile_id::TEXT)||
                          CASE WHEN bp.is_public_blog THEN E'/forum/'
                          ELSE E'/blog/'
                          END
                          || bp.blog_post_id as url
                        FROM profiledb.profiles p
                          JOIN profiledb.profile_block_settings pperm ON p.profile_id = pperm.profile_id
                          JOIN profiledb.blog_posts bp ON bp.owner_profile_id = p.profile_id
                        WHERE pperm.view_permission = 1
                              AND (  pperm.block_type_id = 305 AND NOT bp.is_public_blog
                                     OR pperm.block_type_id = 36 AND bp.is_public_blog
                        )
                              AND p.profile_type_id = 2
                              AND p.status in (2,5,6)
                              AND bp.status = 2
                              AND bp.view_permission = 1
                        ORDER BY p.profile_id ASC, bp.blog_post_id ASC
                      )
                      UNION (
                       select profile_id,
                         E'http://m.'||host||'/campaigns/'||campaign_id AS url
                       from remote_campaigns
                     )) x OFFSET results_offset LIMIT results_limit) x_limited
    ;

    result := REGEXP_REPLACE(result, E'><',E'>\n<','ig');
    result := REGEXP_REPLACE(result, E'\n<(/?url>|loc|mobile)',E'\n\t<\\1','ig');
    result := REGEXP_REPLACE(result, E'\t<(/?loc|mobile)',E'\t\t<\\1','ig');

    RETURN result;
  END
  $body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
CREATE OR REPLACE FUNCTION html_sitemap (host text = E'planeta.ru'::text)
  RETURNS text AS
  $body$
  DECLARE
    result TEXT;
  BEGIN
    select E'<?xml version="1.0" encoding="UTF-8"?>\n<urlset>\n' ||
           xmlagg(loc::xml) || '</urlset>' into result from (
    select E'<category>\n\t<category_id>' || group_category_id || E'</category_id>\n' ||
           xmlagg(loc::xml) || '</category>' as loc from
                                                      (
                                                        select E'<url>\n'
                                                               || xmlelement(name "location", '/' || COALESCE(alias, profile_id::text)) || E'\n\t\t'
                                                               || xmlelement(name "name", display_name) || E'\n\t\t'
                                                               || E'</url>\n' as loc, group_category_id
                                                        from profiledb.profiles t1 where profile_type_id = 2 and status = 6 ) official_groups group by group_category_id order by group_category_id
  ) categories;
RETURN result;
END;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
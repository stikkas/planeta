CREATE OR REPLACE FUNCTION grant_to_all_functions(IN p_schema TEXT, IN p_privilege TEXT, p_user TEXT)
RETURNS void AS
$$
DECLARE
	rec record;
BEGIN
	FOR rec IN (SELECT t1.routine_schema || '.' || t1.routine_name AS function_name,
    			       '(' || array_to_string(array(SELECT t2.parameter_mode || ' ' || t2.data_type
										       FROM information_schema.parameters t2
										      WHERE t1.specific_schema = t2.specific_schema
                                                AND t1.specific_name = t2.specific_name
									       ORDER BY t2.ordinal_position), ',') || ')' AS parameters
				  FROM information_schema.routines t1
				 WHERE t1.routine_schema = $1
	) LOOP
	
    	BEGIN		
			EXECUTE 'GRANT ' || p_privilege || ' ON FUNCTION ' || rec.function_name || rec.parameters || ' TO ' || p_user;
        EXCEPTION
        WHEN OTHERS THEN
        	RAISE NOTICE '%', SQLERRM;
        END;
    
    END LOOP;
END;
$$ LANGUAGE 'plpgsql';
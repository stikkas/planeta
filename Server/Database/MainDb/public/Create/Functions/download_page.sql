CREATE OR REPLACE FUNCTION public.download_page (
  text
)
RETURNS text AS
$body$
use LWP::Simple;
use Encode;
my $url = $_[0];
return decode_utf8(get($url));
$body$
LANGUAGE 'plperlu'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;

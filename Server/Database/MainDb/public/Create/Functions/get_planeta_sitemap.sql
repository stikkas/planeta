CREATE OR REPLACE FUNCTION public.get_planeta_sitemap (
  host text = 'planeta.ru'::text,
  records_offset bigint = 0,
  records_limit bigint = 50000
)
RETURNS text AS
$body$
DECLARE
  result TEXT;
BEGIN
  SELECT E'<?xml version="1.0" encoding="UTF-8"?>\n' ||
xmlelement(name "urlset", XMLATTRIBUTES(
           E'http://www.sitemaps.org/schemas/sitemap/0.9' as "xmlns",
           E'http://www.w3.org/1999/xhtml' as "xmlns:xhtml"
           ),
       xmlagg(xmlelement(name "url",
		xmlelement(name "loc", url::xml),
    CASE
      WHEN mobile_url is not NULL THEN
        xmlelement(name "xhtml:link", XMLATTRIBUTES('alternate' as "rel", 'only screen and (max-width: 640px)' as "media", mobile_url as "href"))
    END
              ) ORDER BY x.profile_id ASC))
  INTO result
  FROM
((
	  SELECT p.profile_id,
	 		 E'http://'||host||E'/'|| COALESCE(p.alias, p.profile_id::text) AS url,
      E'http://m.'||host||E'/' || COALESCE(p.alias, p.profile_id::text) || '/profile.html' AS mobile_url
   		FROM profiledb.profiles p
	   WHERE p.profile_type_id = 3 -- event
         AND p.status NOT IN (0,2)
	      OR p.profile_type_id = 2 --group
		 AND p.status IN (2,5,6)
    )
    UNION
    ( SELECT p.profile_id,
	 		 E'http://'||host||E'/'|| COALESCE(p.alias, p.profile_id::text)||
             CASE WHEN pperm.block_type_id = 305 THEN E'/blog/'
             WHEN pperm.block_type_id = 36 THEN E'/fanblog/'
             END AS url,
      E'http://m.'||host||E'/'|| COALESCE(p.alias, p.profile_id::text)||
            CASE WHEN pperm.block_type_id = 305 THEN E'/blog/'
            WHEN pperm.block_type_id = 36 THEN E'/forum/'
            END AS mobile_url
     	FROM profiledb.profiles p
        JOIN profiledb.profile_block_settings pperm on p.profile_id = pperm.profile_id
       WHERE pperm.view_permission = 1
         AND pperm.block_type_id IN (305, 36)
         AND p.profile_type_id = 2
		 AND p.status IN (2,5,6)
    )
    UNION
    (
	  SELECT p.profile_id,
	 		 E'http://'||host||E'/'|| COALESCE(p.alias, p.profile_id::text)||
             CASE WHEN bp.is_public_blog THEN E'/fanblog/'
                  ELSE E'/blog/'
             END
             ||bp.blog_post_id as url,
       E'http://m.'||host||E'/'|| COALESCE(p.alias, p.profile_id::text)||
            CASE WHEN bp.is_public_blog THEN E'/forum/'
            ELSE E'/blog/'
            END
            ||bp.blog_post_id || '.html' as mobile_url
     	FROM profiledb.profiles p
        JOIN profiledb.profile_block_settings pperm ON p.profile_id = pperm.profile_id
	    JOIN profiledb.blog_posts bp ON bp.owner_profile_id = p.profile_id
       WHERE pperm.view_permission = 1
         AND (  pperm.block_type_id = 305 AND NOT bp.is_public_blog
             OR pperm.block_type_id = 36 AND bp.is_public_blog
	         )
         AND p.profile_type_id = 2
		 AND p.status IN (2,5,6)
         AND bp.status = 2
         AND bp.view_permission = 1
       ORDER BY p.profile_id ASC, bp.blog_post_id ASC
    )
    UNION
    SELECT 0 AS profile_id, E'http://'||host||E'/welcome/agreement.html' AS loc, null as mobile_url
    UNION
    SELECT 0 AS profile_id, E'http://'||host||E'/welcome/about.html' AS loc, null as mobile_url
    UNION
    SELECT 0 AS profile_id, E'http://'||host||E'/welcome/contacts.html' AS loc, null as mobile_url
    UNION
    SELECT 0 AS profile_id, E'http://'||host||E'/welcome/support.html' AS loc, null as mobile_url
    UNION
    SELECT 0 AS profile_id, E'http://'||host||E'/welcome/faq.html' AS loc, null as mobile_url
    UNION
    SELECT 0 AS profile_id, E'http://'||host||E'/welcome/partners.html' AS loc, null as mobile_url
    UNION
    SELECT 0 AS profile_id, E'http://'||host||E'/welcome/projects-agreement.html' AS loc, null as mobile_url
    UNION
    SELECT 0 AS profile_id, E'http://'||host||E'/welcome/concert-agreement.html' AS loc, null as mobile_url
    UNION
    SELECT 0 AS profile_id, E'http://'||host||E'/welcome/instruction.html' AS loc, null as mobile_url
    UNION
    SELECT 0 AS profile_id, E'http://'||host||E'/welcome/potential.html' AS loc, null as mobile_url
    UNION
    SELECT 0 AS profile_id, E'http://'||host||E'/welcome/material.html' AS loc, null as mobile_url
) x
OFFSET records_offset LIMIT records_limit;
result := REGEXP_REPLACE(result, E'><',E'>\n<','ig');
result := REGEXP_REPLACE(result, E'\n<(/?url>|loc)',E'\n\t<\\1','ig');
result := REGEXP_REPLACE(result, E'\t<(/?loc)',E'\t\t<\\1','ig');
RETURN result;
END;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
CREATE TABLE sphinx.sph_counter (
  source_id INTEGER NOT NULL, 
  source_name VARCHAR(150), 
  range_max_id BIGINT, 
  CONSTRAINT source_id PRIMARY KEY(source_id)
) WITH OIDS;

COMMENT ON TABLE sphinx.sph_counter
IS 'Sphinxsearch range maximum ids for delta indexes';

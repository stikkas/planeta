@echo off
set message=

if (%1) == () (
	set message=[DIRECTORY] parameter is not specified
	goto error
)

set directory=%1
goto start

:start

pushd %directory%

for %%i in (*.sql) do (echo ---------- Start of file: %%i -----------) & (more < %%i) & (echo ---------- End of file: %%i -----------)

popd
if errorlevel 1 echo Echo-scripts was unsuccessful.

goto exit

:error
echo Error:
echo %message%
:usage
echo Usage:
echo echo-scripts.bat [DIRECTORY]
echo [DIRECTORY]	  Directory which scripts will be printed
:exit
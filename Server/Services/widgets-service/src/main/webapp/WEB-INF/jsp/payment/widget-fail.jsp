<%@ include file="/WEB-INF/jsp/includes/generated/doctype-header.jsp" %>
<head>
    <%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
    <%@ page trimDirectiveWhitespaces="true" %>
    <%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
    <%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
    <%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
    
    <script type="text/javascript" src="//${hf:getStaticBaseUrl("/res")}/js/libs.js?compress=${properties['static.compress']}&flushCache=${properties['static.flushCache']}"></script>
    <script type="text/javascript" src="//${hf:getStaticBaseUrl("/res")}/js/payment.js?compress=${properties['static.compress']}&flushCache=${properties['static.flushCache']}"></script>
    <%@include file="/WEB-INF/jsp/includes/generated/common-favicon.jsp" %>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">

    <!-- Sharing meta data: start -->
    <link rel="image_src" href=""/>
    <meta property="og:site_name" content="Planeta.ru"/>
    <meta property="og:title" content=" <fmt:formatDate value="${event.profile.displayName}" pattern="dd.MM.yy"/>" />
    <meta property="og:image" content="" />
    <meta property="og:description" content="<c:out value="${event.profile.summary}"/> <spring:message code="payment.fail.jsp.propertie.1" text="default text"> </spring:message> <fmt:formatNumber maxFractionDigits="0" value="${lowestTicketPrice}" /> <spring:message code="payment.fail.jsp.propertie.2" text="default text"> </spring:message>" />
    <meta name="description" content="<c:out value="${event.profile.summary}"/> <spring:message code="payment.fail.jsp.propertie.1" text="default text"> </spring:message> <fmt:formatNumber maxFractionDigits="0" value="${lowestTicketPrice}" /> <spring:message code="payment.fail.jsp.propertie.2" text="default text"> </spring:message>"/>
    <!-- Sharing meta data: end -->

    <title><spring:message code="payment.fail.jsp.propertie.3" text="default text"> </spring:message></title>
    <link type="text/css" media="only screen" rel="stylesheet" href="//${hf:getStaticBaseUrl("")}/css-generated/project-reward.css"/>
    <link type="text/css" rel="stylesheet" href="//${hf:getStaticBaseUrl("")}/css-generated/payment.css"/>
</head>
<body>
<script>
    $(document).ready(function() {
        $('.js-close').click(function () {
            parent.postMessage("close", "*");
        });
    });
</script>
    <div class="pr-modal-close js-close"><i></i></div>
    <div class="pln-content-box">
        <div class="pln-content-box_cont">
    
            <div class="pln-payment-error-loyal">
                <div class="pln-payment-error-loyal_row">
                    <div class="pln-payment-error-loyal_head">
                        <span class="hl">
                            <spring:message code="payment.fail.jsp.propertie.13" text="default text"> </spring:message>
                        </span><spring:message code="payment.fail.jsp.propertie.14" text="default text"> </spring:message>
                    </div>
    
                    <div class="pln-payment-error-loyal_head-text">
                        <spring:message code="payment.fail.jsp.propertie.15" text="default text"> </spring:message>
                    </div>    
                    
                </div>    
                
            </div>
        </div>
    </div>
</body>
</html>
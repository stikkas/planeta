<%@ page contentType="text/javascript;charset=UTF-8" language="java" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/includes/taglibs.jsp"%>

var workspace = {
    staticNodesService : new StaticNodesService('${staticNode}', ${staticNodes != null ? hf:toJson(staticNodes) : '[]'}, '${hf:getStaticBaseUrl("")}'),

    getResourceUrl: function (url) {
        return this.staticNodesService.getResourceUrl(url);
    },

    hosts: {
        mainHost: '${properties["application.host"]}',
        staticAppUrl: '${properties["static.host"]}',
        widgetsAppUrl: '${properties["widgets.application.host"]}'
    }
};
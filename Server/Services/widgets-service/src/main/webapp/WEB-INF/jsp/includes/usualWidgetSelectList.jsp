<%@include file="/WEB-INF/jsp/includes/generated/taglibs.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<c:if test="${not empty shares}">
<div class="awc-item cf awc-widget-1">
    <div class="awc-head">240 x 400 <a class="awc-head-link" href="javascript:void(0);">Назад</a></div>
    <div class="awc-farme" style="position: relative">
        <iframe class="js-widget-iframe" data-widget-type="CAMPAIGN_240X400_WITH_SHARE" src=""
                frameborder="0" width="240" height="400"></iframe>
        <div style="position: absolute;top: 0;right: 0;bottom: 0;left: 0;background:black;opacity:0"></div>
    </div>
</div>
</c:if>
<div class="awc-item cf awc-widget-2">
    <div class="awc-head">300 x 250 <a class="awc-head-link" href="javascript:void(0);">Назад</a></div>
    <div class="awc-farme" style="position: relative">
        <iframe class="js-widget-iframe" data-widget-type="CAMPAIGN_300X250" src="" frameborder="0"
                width="300" height="250"></iframe>
        <div style="position: absolute;top: 0;right: 0;bottom: 0;left: 0;background:black;opacity:0"></div>
    </div>
</div>
<div class="awc-item cf awc-widget-9">
    <div class="awc-head">192 x 240 <a class="awc-head-link" href="javascript:void(0);">Назад</a></div>
    <div class="awc-farme" style="position: relative">
        <iframe class="js-widget-iframe" data-widget-type="CAMPAIGN_192X240" src="" frameborder="0"
                width="192" height="240"></iframe>
        <div style="position: absolute;top: 0;right: 0;bottom: 0;left: 0;background:black;opacity:0"></div>
    </div>
</div>
<div class="awc-item cf awc-widget-8">
    <div class="awc-head">200 x 200 <a class="awc-head-link" href="javascript:void(0);">Назад</a></div>
    <div class="awc-farme" style="position: relative">
        <iframe class="js-widget-iframe" data-widget-type="CAMPAIGN_200X200" src="" frameborder="0"
                width="200" height="200"></iframe>
        <div style="position: absolute;top: 0;right: 0;bottom: 0;left: 0;background:black;opacity:0"></div>
    </div>
</div>
<c:if test="${campaign.targetAmount > 0}">
    <div class="awc-item cf awc-widget-3">
        <div class="awc-head">240 x 240 <a class="awc-head-link" href="javascript:void(0);">Назад</a></div>
        <div class="awc-farme" style="position: relative">
            <iframe class="js-widget-iframe" data-widget-type="CAMPAIGN_240X240" src="" frameborder="0"
                    width="240" height="240"></iframe>
            <div style="position: absolute;top: 0;right: 0;bottom: 0;left: 0;background:black;opacity:0"></div>
        </div>
    </div>
</c:if>
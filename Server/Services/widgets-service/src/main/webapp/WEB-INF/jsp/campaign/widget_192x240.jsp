<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ include file="/WEB-INF/jsp/includes/generated/doctype-header.jsp" %>
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Проект ${campaign.name}</title>
    <link rel="stylesheet" type="text/css"  href="//${hf:getStaticBaseUrl('')}/css-generated/affiliate-widget.css">
    <%@ include file="/WEB-INF/jsp/includes/google_analytics.jsp" %>
</head>
<body>



<a class="widget-block widget-9<c:if test="${campaign.targetAmount >= 10000000}">  target-over-8</c:if>" href="https://${properties["application.host"]}/campaigns/${campaign.campaignId}<c:if test="${not empty refererParam}">?${refererParam}</c:if>" target="_blank">
        <span class="cover-wrap">
            <span class="cover"><img src="${hf:getThumbnailUrl(campaign.imageUrl, "BIG", "PRODUCT")}" alt="${campaign.name}"></span>
            <span class="name"><span class="name-val">${hf:getShrinkedString(campaign.name, 128)}</span></span>
        </span>
        <span class="widget-cont">
            <span class="meta meta-vertical meta-box">
                <span class="meta-item">
                    <span class="meta-label">собрано</span>
                    <span class="meta-value"><fmt:formatNumber value="${campaign.collectedAmount}"/>&nbsp;<span class="b-rub">Р</span></span>
                </span>
                <c:if test="${campaign.timeFinish != null && campaign.targetAmount <= 0}">
                    <span class="meta-item">
                        <span class="meta-label"><spring:message code="decl.left" arguments="${hf:plurals(daysLeft)}"/></span>
                        <c:set var="now" value="<%=new java.util.Date()%>" />
                        <c:set var="daysLeft" value="${hf:getDateDifferenceInDays(now, campaign.timeFinish)}"/>
                        <span class="meta-value">
                            <span class="hl">${daysLeft}</span>
                            &nbsp;<spring:message code="decl.days" arguments="${hf:plurals(daysLeft)}"/>
                        </span>
                        <br/>
                    </span>
                </c:if>

                <c:if test="${campaign.targetAmount > 0}">
                    <c:set var="progressNum" value="${(campaign.collectedAmount / targetAmountLong) * 100}"/>
                    <fmt:formatNumber var="progress" value="${(campaign.collectedAmount / targetAmountLong) * 100}" maxFractionDigits="0"/>
                    <span class="meta-item">
                        <span class="meta-label">цель проекта</span>
                        <span class="meta-value">${progress}&nbsp;<span class="b-rub">Р</span></span>
                    </span>
                </c:if>


            </span>
            <c:if test="${campaign.targetAmount > 0}">
                <span class="progress-block">
                    <span class="progress">
                        <span class="progress-bar" style="width:${progressNum > 100 ? 100 : progressNum}%"></span>
                    </span>
                </span>
            </c:if>
            <span class="donate-btn-center"><span class="btn btn-block">Поддержать</span></span>
        </span>
</a>

</body>
</html>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ include file="/WEB-INF/jsp/includes/generated/doctype-header.jsp" %>

<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Проект ${campaign.name}</title>
    <link rel="stylesheet" type="text/css"  href="//${hf:getStaticBaseUrl('')}/css-generated/affiliate-widget.css">
    <%@ include file="/WEB-INF/jsp/includes/google_analytics.jsp" %>
</head>
<body>
    <iframe src="${url}" width="${widget.width}" height="${widget.height}" frameborder="0"></iframe>
</body>
</html>
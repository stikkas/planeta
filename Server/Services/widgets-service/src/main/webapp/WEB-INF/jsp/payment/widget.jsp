<%@ page contentType="text/html; charset=UTF-8" language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="c-rt" uri="http://java.sun.com/jstl/core_rt" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ct" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="hf" uri="http://planeta.ru/taglibs/HelperFunctions"%>
<%@ taglib prefix="p" uri="http://planeta.ru/tags"%>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Покупка вознаграждений</title>

    <p:script src="payment-widget.js"/>
    <link type="text/css" media="only screen" rel="stylesheet" href="//${hf:getStaticBaseUrl("")}/css-generated/project-reward.css"/>

    <script id="video-tmpl" type="text/x-jquery-template">
        <div class="pv-media-view">
            <div class="project-media-view">
                <div class="pmv-cont">
                    <span class="central-block">
                        <img class="pmv-img" src="{{= ImageUtils.getThumbnailUrl(image, ImageUtils.HUGE, ImageType.PHOTO) }}">
                        <span class="pmv-watermark"></span>
                    </span>
                    <span class="vertical-align"></span>
                    <span class="video-play"></span>
                </div>
            </div>
        </div>
    </script>
    <script id="audio-tmpl" type="text/x-jquery-template">
        <div class="track-list">
            <div class="track-list-item row" data-role="audio">
                <div class="track-list-item-count">
                    <a href="javascript:void(0)" class="play-track"></a>
                </div>
                <div class="track-list-item-name">
                    <div class="track-list-item-name-title">{{= $data.name}}</div>
                    <div class="track-list-item-name-author">
                        <a href="javascript:void(0)">{{= $data.artist}}</a>
                    </div>
                </div>
                <div class="track-list-item-option">
                    <div class="track-list-item-option-time">{{= StringUtils.humanDuration($data.duration)}}</div>
                </div>
                <div class="track-list-item-moving-shadow"></div>
                <div class="track-list-item-progress-bar-wrap">
                    <div class="track-list-item-progress-bar">
                        <div class="track-list-item-progress-bar-load"></div>
                    </div>
                </div>
            </div>
        </div>
    </script>
    <script id="photo-tmpl" type="text/x-jquery-template">
        <br/>
        <img src="{{= ImageUtils.getThumbnailUrl(image, ImageUtils.HUGE, ImageType.PHOTO) }}"/>
        <br/>
    </script>
    <script type="text/javascript">
        var workspace = {
            staticNodesService: new StaticNodesService('${staticNode}', ${staticNodes != null ? hf:toJson(staticNodes) : '[]'}, '${hf:getStaticBaseUrl("")}'),
            topNavigationBar: {},
            getResourceUrl: function (url) {
                return this.staticNodesService.getResourceUrl(url);
            },
            hosts: {
                main: '${properties["application.host"]}',
                staticNode: '${staticNode}',
                staticNodes: ${hf:toJson(staticNodes)},
                statServiceUrl: '${properties["statistics.service.url"]}',
                tv: '${properties["tv.application.host"]}',
                shop: '${properties["shop.application.host"]}',
                mobile: '${properties["mobile.application.host"]}',
                widgets: '${properties["widgets.application.host"]}',
                resources: '${hf:getStaticBaseUrl("")}',
                cas: '${properties["cas.app.path.secured"]}'
            },
            isAuthorized: ${isAuthorized}
        };
        workspace.stats = StatisticService.init({ statServiceUrl: workspace.hosts.statServiceUrl, clientId: 0});
        workspace.extend = function(options) {
            $.extend(this, options);
            return this;
        };
        workspace.extend({
            showErrorMessage: function(message, timeout, context) {
                if (!message) return;

                var timeout2 = timeout;
                this.closeAlert();
                timeout2 = typeof  timeout2 == 'undefined' ? 5000 : timeout2;
                var alertEl = $('<div class="alert alert-error"><span>' + message + '</span><span class="close" >×</span></div>');

                this.alertEl = alertEl;
                var self = this;
                $(alertEl).find(".close").click(function(e) {
                    e.preventDefault();
                    self.closeAlert();
                });
                if (timeout2) {
                    _.delay(function() {
                        self.closeAlert();
                    }, timeout2);
                }
                var $context = context || $('body');
                $context.prepend(alertEl);
            },

            closeAlert: function() {
                if (this.alertEl) {
                    this.alertEl.remove();
                    this.alertEl = null;
                }
            }

        });

        var model = {
            showEmailAlert: function (text) {
                $('#hint').html(text);
                $('.js-email-alert').show();
            },
            hideEmailAlert: function () {
                $('.js-email-alert').hide();
            },
            update: function () {
                this.donateAmount = shares[this.shareId].price * this.quantity || ${recommendedDonate};
                this.render();
            },
            render: function () {
                var checked = $('.donate-condition-item.checked');
                if (!this.shareId) {
                    return;
                }
                checked.find('.b-t-b-item-qty-spn').text(model.quantity);
                if (shares[this.shareId].quantity <= 1) {
                    checked.find('.b-t-b-item-qty-minus').addClass('disabled');
                } else {
                    checked.find('.b-t-b-item-qty-minus').removeClass('disabled');
                }
                $('#donate-sum-input').val(this.donateAmount);
            },
            validate: function () {
                var valid = false;
                if (!this.shareId || !shares[this.shareId]) {
                    workspace.showErrorMessage('Ни одно вознаграждение не выбрано');
                } else if (this.quantity <= 0) {
                    workspace.showErrorMessage('Количество вознаграждений не может быть нулевым');
                } else if (shares[this.shareId].price * this.quantity > model.donateAmount) {
                    workspace.showErrorMessage('Сумма не может быть меньше, чем суммарная стоимость выбранных вознаграждений');
                } else {
                    valid = true;
                }
                return valid;
            },
            validate2: function () {
                var err;
                if (!this.paymentMethodId) {
                    $('.step-2 .modal-scroll-content').scrollbar('scrollto', 'bottom');
                    err = 'Выберите способ оплаты';
                } else if (!this.shareId) {
                    err = 'Вознаграждение не выбрано';
                }
                if (err) {
                    workspace.showErrorMessage(err);
                }
                return !err;
            }
        };
        <c:if test="${isAuthorized}">
            model.authorized = true; model.email = '${email}';
            <c:if test="${not empty myProfile.profile.displayName}">
                model.customerName = '<c:out value="${myProfile.profile.displayName}"/>';
            </c:if>
        </c:if>
        var shares = {
        <c:forEach items="${shares}" var="share" varStatus="i">
            ${share.shareId}: {price:${share.price}, name: '<c:out value="${share.name}"/>', quantity: 1, reply: ''}<c:if test="${!i.last}">,</c:if>
        </c:forEach>
        };


        $(document).ready(function() {
            workspace.stats.trackStatEvent('CAMPAIGN_VIEW', ${campaign.campaignId});
            workspace.campaignId = ${campaign.campaignId};
            var frames = [];
            frames.push($('[data-frame=shares]').frame({
                events: {
                    'change #donate-sum-input': 'onChangeDonateAmount',
                    'click #submit-btn':'onSubmit',
                    'click .b-t-b-item-qty-plus':'incShareCount',
                    'click .b-t-b-item-qty-minus':'decShareCount',
                    'click .donate-condition-item:not(.disabled)': 'onChangeShare'
                },
                init: function () {

                    this.$('textarea').change(function () {
                        model.reply = shares[model.shareId].reply = this.value;
                    }).elastic().each(function () {
                                $(this).limitInput({
                                    limit: 255,
                                    remTextEl: '.rest-count',
                                    toggleCounter: {
                                        show: "focusin",
                                        hide: "focusout"
                                    },
                                    enableNegative: true
                                });
                            });

                    var min = -1;
                    for (var i in shares) {
                        if (min < 0 || min > shares[i].price) {
                            min = shares[i].price
                        }
                    }
                    this.$('#donate-sum-input').val(min);
                },
                onChangeShare: function(e) {
                    var this$ = $(e.currentTarget);
                    if (this$.is('.checked')) {
                        return;
                    }
                    var checked = this.$('.donate-condition-item.checked');
                    checked.removeClass('checked');
                    checked.find('.donate-condition-left-col .radiobox').removeClass('active');
                    this$.addClass('checked');
                    this$.find('.donate-condition-left-col .radiobox').addClass('active');
                    this$.find('.donate-condition-question').show();
                    model.shareId = this$.attr('data-id');
                    model.quantity = shares[model.shareId].quantity;
                    model.reply = shares[model.shareId].reply;
                    model.update();
                    this.layout();
                },
                incShareCount: function() {
                    model.quantity = ++shares[model.shareId].quantity;
                    model.update();
                },
                decShareCount: function() {
                    if (shares[model.shareId].quantity <= 1) {
                        return;
                    }
                    model.quantity = --shares[model.shareId].quantity;
                    model.update();
                },
                onSubmit: function() {
                    if (model.validate()) {
                        $('.js-share-delivery').hide();
                        $('.js-share-delivery[data-share-id=' + model.shareId + ']').show();
                        this.next();
                    }
                },
                onChangeDonateAmount: function(e) {
                    var number = parseInt(e.currentTarget.value);
                    model.donateAmount = number > 0 ? number : 0;
                },
                show: function () {
                    model.render();
                }
            }));
            frames.push($('[data-frame=checkout]').frame({
                events: {
                    'click .js-share-delivery': 'onSelectDelivery',
                    'click .bpt-item': 'onSelectPayment',
                    'change input[name]': 'onInputChange',
                    'change select[name]': 'onSelectChange',
                    'click .b-reward-body-rechange':'goBack',
                    'click .p-reward-footer-accept': 'onConfirm'
                },
                onSelectDelivery: function(e) {
                    var this$ = $(e.currentTarget);
                    this.$('.js-share-delivery').removeClass('checked');
                    this.$('.radiobox').removeClass('active');
                    this$.addClass('checked').find('.radiobox').addClass('active');
                    _.extend(model, this$.data());
                    this.$('#total-cost').text(StringUtils.humanNumber(model.donateAmount + model.deliveryPrice));
                    this.layout();
                },
                onSelectPayment: function(e) {
                    var this$ = $(e.currentTarget);
                    this.$('.bpt-item').removeClass('active');
                    this$.addClass('active');
                    model.paymentMethodId = this$.attr('data-value');
                    if (this$.attr('data-phone-required') == 'true') {
                        this.$('.buy-payment-type-phone').show();
                    } else {
                        this.$('.buy-payment-type-phone').hide();
                    }
                    this.layout();
                },
                onInputChange: function(e) {
                    var that = $(e.currentTarget);
                    model[that.attr('name')] = that.val();
                },
                onSelectChange: function(e) {
                    var that = $(e.currentTarget);
                    model[that.attr('name')] = that.val();
                    try {
                        model['country'] = that.find('option[value=' + that.val() + ']').text().trim();
                    } catch (e) {}
                },
                goBack: function(){
                    model.serviceId = 0;
                    this.prev();
                },
                onConfirm: function() {
                    var self = this;
                    if (this.validate() && model.validate2()) {
                        var data = {};
                        for (var i in model) {
                            if (!$.isFunction(model[i])) {
                                data[i] = model[i];
                            }
                        }
                        if (data.paymentPhone) {
                            data.phone = data.paymentPhone.replace(/[\\+,\(,\),\s,\-]/g, '');
                        }
                        model.hideEmailAlert();
                        $.ajax('/widgets/pay', {
                            data: JSON.stringify(data),
                            dataType: 'json',
                            contentType: 'application/json',
                            type: 'post'
                        }).done(function(response) {
                            if (!response.success) {
                                if (response.fieldErrors && response.fieldErrors.email) {
                                    var msg = response.fieldErrors.email +
                                            '<br>Попробуйте осуществить покупку на сайте <a href="https://' + workspace.hosts.main + '/campaigns/' + workspace.campaignId + '/donate/' + data.shareId + '">Planeta.ru</a>';
                                    model.showEmailAlert(msg);
                                } else {
                                    response.errorMessage && workspace.showErrorMessage(response.errorMessage);
                                    self.highliteErrors(response.fieldErrors);
                                }
                            } else {
                                window.location.href = response.result;
                            }
                        });
                    }
                },
                init: function () {
                    this.$('#paymentPhone').mask('+7(999)999-99-99');
                },
                show: function () {
                    var share = shares[model.shareId];
                    this.$('#total-cost').text(StringUtils.humanNumber(model.donateAmount));
                    this.$('.js-share-name').html(share.name);
                    this.layout();
                }
            }));
            $('.js-close').click(function () {
                parent.postMessage("close", "*");
            });
            $('#donate-sum-input').numbersOnly();
            frames[0].show();
        });

    </script>
</head>
<body>
<div class="pr-modal-close js-close"><i></i></div>

<div class="project-reward" data-role="frame" data-frame="shares" data-frame-next="checkout" style="display:none">
<div class="project-reward-header">
    <div class="p-reward-header-title">
        Поддержите проект <span class="p-reward-header-title-spn"><c:out value="${campaign.name}"/></span>, купив одно из вознаграждений:
    </div>
</div>
<div class="project-reward-body">
<div class="scroll">
<div class="modal-scroll-content">
<div class="b-reward-body-list">
<div class="project-donate-condition-wrap">

    <div class="project-donate-condition">
        <c:forEach items="${shares}" var="share">
            <c:set var="isAllSold" value="${share.amount != 0 && share.amount == share.purchaseCount}"/>
            <c:if test="${not isAllSold}">
                <c:set var="imgSrc" value="${empty share.imageUrl ? '/images/content/concert-cover-1.jpg' : share.imageUrl}"/>
                <c:set var="amount" value="${share.amount - share.purchaseCount}"/>
                <div class="donate-condition-item" data-id="${share.shareId}">
                    <div class="donate-condition-left-col">
                        <div class="radio-row clearfix">
                            <span class="radiobox"></span>
                    <span class="radiobox-label">
                    <c:if test="${share.price > 0}">
                        <fmt:formatNumber value="${share.price}"/> <span class="b-rub">Р</span>
                    </c:if>
                    </span>
                        </div>
                        <c:if test="${share.price > 0}">
                        <div class="donate-condition-counter">
                            <div class="d-condition-counter-badge">Вознаграждение выбрано</div>
                            <div class="d-condition-counter-btns">
                                <span class="b-t-b-item-qty-minus disabled"></span>
                                <span class="b-t-b-item-qty-spn">1</span>
                                <span class="b-t-b-item-qty-plus"></span>
                            </div>

                            <c:if test="${amount > 0}">
                                <div class="b-t-b-item-qty-count">
                                    <spring:message code="decl.objectsRemFemale" arguments="${hf:plurals(amount)}"/><br>
                                    <fmt:formatNumber value="${amount}"/> <spring:message code="decl.shares" arguments="${hf:plurals(amount)}"/>
                                </div>
                            </c:if>
                        </div>
                        </c:if>
                    </div>
                    <c:if test="${share.price > 0 && share.imageUrl != null && share.imageUrl != \"\"}">
                    <div class="share-img">
                        <img src="${hf:getThumbnailUrl(share.imageUrl, 'PRODUCT_COVER', 'PRODUCT')}" class="pull-right">
                    </div>
                    </c:if>
                    <div class="donate-condition-cont">
                        <div class="donate-condition-head"><c:out value="${share.name}"/></div>
                        <div class="donate-condition-descr">
                                ${not empty share.descriptionHtml ? share.descriptionHtml : share.description}
                        </div>
                        <c:if test="${!empty share.rewardInstruction}">
                            <div class="donate-condition-question error1">
                                <div class="dcq-head">Способы получения:</div>
                                <div class="dcq-text">
                                        ${share.rewardInstruction}
                                </div>
                            </div>
                        </c:if>
                        <c:if test="${!empty share.questionToBuyer}">
                            <div class="donate-condition-question error1">
                                <div class="dcq-head">Автор спрашивает:</div>
                                <div class="dcq-text">
                                        ${share.questionToBuyer}
                                </div>
                                <div class="dcq-textarea clearfix">
                                    <textarea required="1" type="text" placeholder="Ваш ответ"></textarea>
                                    <span class="help-inline error-message hidden">Обязательное поле</span>
                                    <div class="rest-count rest-count-less1"></div>
                                </div>
                            </div>
                        </c:if>
                    </div>
                </div>
            </c:if>
        </c:forEach>
    </div>
</div>
</div>
</div>
</div>

</div>
<div class="project-reward-footer">
    <div class="cf">
        <div class="donate-sum-cont">
            <div class="donate-sum-label">
                <label>Введите сумму</label>
            </div>
            <div class="donate-sum-input">
                <input id="donate-sum-input" type="text" pattern="\d+" value="${recommendedDonate}">
                <span class="input-hl b-rub">Р</span>
            </div>
            <div class="donate-sum-descr">
                Сумма остаётся на Ваше усмотрение, в её пределах Вы можете приобрести одно из доступных вознаграждений проекта.
            </div>
        </div>
        <a id="submit-btn" class="p-reward-footer-accept flat-btn" href="javascript:void(0)">Продолжить</a>
        <a class="p-reward-footer-decline flat-btn gray-on-white js-close" href="javascript:void(0)">Отказаться</a>
    </div>
</div>
</div>


<div class="project-reward step-2" data-role="frame" data-frame="checkout" data-frame-prev="shares" style="display: none">
<div class="project-reward-header">
    <div class="p-reward-header-title">Вы покупаете вознаграждение:</div>
    <div class="b-reward-body-list">
        <div class="project-donate-condition">

            <div class="donate-condition-item-s">
                <div class="donate-condition-cont" style="margin-left: 0px;">
                    <div class="donate-condition-head js-share-name"></div>
                </div>
            </div>

        </div>

        <a class="b-reward-body-rechange" href="javascript:void(0)">Выбрать другое вознаграждение</a>
    </div>
</div>

<div class="project-reward-body cf">
    <div class="project-donate-condition">
        <div class="scroll">
            <div class="modal-scroll-content">
                <c:forEach var="share" items="${shares}">
                <c:forEach var="delivery" items="${share.linkedDeliveries}">
                <div data-share-id="${share.shareId}"
                     data-service-id="${delivery.serviceId}"
                     data-delivery-price="${delivery.price}"
                     class="js-share-delivery donate-condition-item">
                    <div class="donate-condition-left-col">
                        <div class="radio-row">
                            <span class="radiobox"></span>
                            <c:if test="${delivery.price > 0}">
                                <span class="radiobox-label">${delivery.price} <span class="b-rub">Р</span></span>
                            </c:if>
                        </div>
                    </div>
                    <div class="donate-condition-cont">
                        <div class="donate-condition-head">${delivery.name}</div>
                        <div class="donate-condition-descr" style="white-space: pre;">${delivery.publicNote}</div>
                    <c:choose>
                        <c:when test="${delivery.serviceType == 'CUSTOMER_PICKUP'}">
                            <div class="donate-condition-descr">${delivery.address.city}, ${delivery.address.street}
                                <br/>тел.: ${delivery.address.phone}
                            </div>
                            <div id="pickup-panel" class="donate-condition-question slide-on-select error1">
                                <div class="dcq-head">Получатель</div>
                                <div class="dcq-textarea clearfix input-field">

                                    <input required="true" type="text" placeholder="Иванов Иван Иванович" name="customerName"
                                    <c:if test="${isAuthorized}">
                                    <c:if test="${not empty myProfile.profile.displayName}">
                                           value="<c:out value="${myProfile.profile.displayName}"/>"
                                    </c:if>
                                    </c:if>>
                                    <div class="help-inline error-message hidden"></div>
                                    <div class="rest-count rest-count-less1"></div>
                                </div>
                                <div class="dcq-head">Контактный телефон</div>
                                <div class="dcq-textarea clearfix input-field">
                                    <input required="true" type="text" placeholder="+7-909-123-45-67" name="phone"
                                           value="">

                                    <div class="help-inline error-message hidden"></div>
                                    <div class="rest-count rest-count-less1"></div>
                                </div>
                            </div>
                        </c:when>
                        <c:when test="${delivery.serviceType == 'DELIVERY' || delivery.serviceType == 'RUSSIAN_POST'}">


                                <div id="address-panel" class="donate-condition-question slide-on-select error1">
                                    <div class="dcq-head">Страна</div>
                                    <div class="dcq-textarea clearfix input-field">
                                        <select name="countryId" class="span5">
                                            <option value="0">
                                                - Не выбрано -
                                            </option>
                                            <c:forEach var="country" items="${countries}">
                                                <option value="${country.countryId}">
                                                    <c:out value="${country.countryNameRus}" />
                                                </option>
                                            </c:forEach>
                                        </select>

                                        <div class="help-inline error-message hidden"></div>
                                        <div class="rest-count rest-count-less1"></div>
                                    </div>
                                    <div class="dcq-head">Город</div>
                                    <div class="dcq-textarea clearfix input-field">
                                        <input required="true" type="text" placeholder="Москва" name="city"
                                               value="">

                                        <div class="help-inline error-message hidden"></div>
                                        <div class="rest-count rest-count-less1"></div>
                                    </div>
                                    <div class="dcq-head">Адрес</div>
                                    <div class="dcq-textarea clearfix input-field">
                                        <input required="true" type="text"
                                               placeholder="3-я улица Строителей, д. 302-бис, кв. 50, вход со двора"
                                               name="street" value="">

                                        <div class="help-inline error-message hidden"></div>
                                        <div class="rest-count rest-count-less1"></div>
                                    </div>
                                    <div class="dcq-head">Почтовый индекс</div>
                                    <div class="dcq-textarea clearfix input-field">
                                        <input required="true" type="text" placeholder="123456" name="zipCode" value="">

                                        <div class="help-inline error-message hidden"></div>
                                        <div class="rest-count rest-count-less1"></div>
                                    </div>
                                    <div class="dcq-head">Получатель</div>
                                    <div class="dcq-textarea clearfix input-field">

                                    <input required="true" type="text" placeholder="Иванов Иван Иванович"
                                               name="customerName"
                                            <c:if test="${isAuthorized}">
                                                <c:if test="${not empty myProfile.profile.displayName}">
                                                    value="<c:out value="${myProfile.profile.displayName}"/>"
                                                </c:if>
                                            </c:if>>
                                        <div class="help-inline error-message hidden"></div>
                                        <div class="rest-count rest-count-less1"></div>
                                    </div>
                                    <div class="dcq-head">Контактный телефон</div>
                                    <div class="dcq-textarea clearfix input-field">
                                        <input required="true" type="text" placeholder="+7-909-123-45-67" name="phone"
                                               value="">

                                        <div class="help-inline error-message hidden"></div>
                                        <div class="rest-count rest-count-less1"></div>
                                    </div>
                                </div>

                        </c:when>
                    </c:choose>
                    </div>
                </div>
                </c:forEach>
                </c:forEach>
                <div class="project-reward-payment">
                    <div class="bpb-item">
                        <div class="bpb-head">Оплатить <b>со внешних счетов остаток в</b> <span id="total-cost"></span> <span class="b-rub">Р</span></div>
                        <div class="buy-payment-type cf">
                            <c:forEach items="${paymentMethods}" var="type">
                                <div class="bpt-item" data-value="${type.id}" data-phone-required="${type.needPhone}">
                                    <div class="bpt-img"><img src="//${hf:getStaticBaseUrl("")}${type.imageUrl}"></div>
                                    <div class="bpt-name">${type.name}</div>
                                </div>
                            </c:forEach>
                        </div>

                        <div class="buy-payment-type-phone form-inline" style="display:none">
                            <span class="bptp-label">Введите номер Вашего телефона:</span>
                            <input id="paymentPhone" class="bptp-input" required="true" name="paymentPhone" type="text">
                            <span class="bptp-help">* Номер телефона является обязательным параметром.</span>
                        </div>
                    </div>
                </div>

            </div>
        </div>

    </div>

</div>

<div class="project-reward-footer">
    <div class="cf">
        <c:choose>
            <c:when test="${hasEmail}">
                <div class="buy-payment-name">
                    <div class="bpn-label">
                        <label>Вы авторизованы как</label>
                    </div>
                    <div class="bpn-input">
                        <a class="bpn-user-link" target="_blank" href="//${properties['application.host']}/${myProfile.profile.webAlias}">
                            <span class="bpn-user-ava"><img src="${hf:getUserAvatarUrl(myProfile.profile.imageUrl, 'USER_SMALL_AVATAR', 'MALE')}"></span>
                            <span class="bpn-user-name"><c:out value="${myProfile.profile.displayName}"/></span>
                        </a>
                    </div>
                </div>
            </c:when>
            <c:otherwise>
                <div class="donate-sum-cont">
                    <div class="buy-payment-email project-donate-step-2">
                        <div class="bpe-label">
                            <label>Введите Ваш E-mail</label>
                        </div>
                        <div class="bpe-input dropdown popup-open">
                            <input type="text" name="email" value>
                            <div class="dropdown-popup project-reward-buy-action js-email-alert" style="display:none">
                                <div class="dropdown-popup-caret"></div>
                                <div class="dropdown-popup-body">
                                    <div id="hint">Возможно, Вы имели в виду<br><span class="suggestion"><span class="address">kurtizanka</span>@<a href="#" class="domain">mail.ru</a></span>?</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </c:otherwise>
        </c:choose>
        <a class="p-reward-footer-accept flat-btn" href="javascript:void(0)">Продолжить</a>
        <a class="p-reward-footer-decline flat-btn gray-on-white js-close" href="javascript:void(0)">Отказаться</a>
    </div>
</div>
</div>
</body>
</html>
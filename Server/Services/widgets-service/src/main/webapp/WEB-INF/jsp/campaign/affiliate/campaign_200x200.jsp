<%@include file="/WEB-INF/jsp/includes/generated/taglibs.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<a class="widget-block widget-8<c:if test="${params.moneyTarget >= 10000000 || params.moneyCollected >= 10000000}" > target-over-8</c:if>" href="${params.campaignUrl}" target="_blank">
    <span class="cover-wrap">
      <span class="cover">
        <img src="${hf:getThumbnailUrl(params.campaignImgUrl, "BIG", "PRODUCT")}">
      </span>
      <span class="name">${params.campaignName}</span>
    </span>
    <span class="widget-cont">
      <span class="meta">
        <span class="meta-item">
          <span class="meta-value">
            <fmt:formatNumber value="${params.moneyCollected}"/>
                <span class="b-rub">Р</span>
          </span>
          <span class="meta-label">собрано</span>
        </span>
      </span>

            <span class="progress-block">
            <span class="progress">
                <c:choose>
                    <c:when test="${params.moneyTarget > 0}">
                        <c:set var="percentCollected">${params.percentCollected}</c:set>
                    </c:when>
                    <c:otherwise>
                        <c:set var="percentCollected">100</c:set>
                    </c:otherwise>
                </c:choose>
              <span class="progress-bar" style="width:${percentCollected}%;"></span>
            </span>
          </span>

        <span class="donate-btn-center">
        <span class="btn-link">Поддержать</span>
      </span>
    </span>
</a>

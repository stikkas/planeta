<%@include file="/WEB-INF/jsp/includes/generated/taglibs.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<a class="widget-block widget-6 horizontal-widget  ${params.theme}<c:if test="${params.moneyTarget <= 0}" > not-target</c:if><c:if test="${(params.moneyTarget >= 10000000 || params.moneyCollected >= 10000000)}" > target-over-8</c:if>" href="${params.campaignUrl}" target="_blank">
    <span class="cover-wrap">
      <span class="cover">
        <img src="${hf:getThumbnailUrl(params.campaignImgUrl, "BIG", "PRODUCT")}">
      </span>
    </span>
    <span class="widget-cont">
      <span class="name">${params.campaignName}</span>
        <c:if test="${params.moneyTarget > 0}" >
            <span class="progress-block">
            <span class="progress">
              <span class="progress-bar" style="width:${params.percentCollected}%;"></span>
            </span>
          </span>
        </c:if>
        <span class="donate-btn">
        <span class="btn">Поддержать</span>
      </span>
      <span class="meta meta-box">
        <span class="meta-item">
          <span class="meta-label">собрано</span>
          <span class="meta-value">
            <fmt:formatNumber value="${params.moneyCollected}"/>
                <span class="b-rub">Р</span>
          </span>
        </span>
        <span class="meta-item">
            <c:choose>
                <c:when test="${params.moneyTarget > 0}">
                  <span class="meta-label">цель проекта</span>
                  <span class="meta-value">
                    <fmt:formatNumber value="${params.moneyTarget}"/>
                        <span class="b-rub">Р</span>
                  </span>
                </c:when>
                <c:otherwise>
                    <span class="meta-label">Без финансовой цели</span>
                </c:otherwise>
            </c:choose>
        </span>
      </span>
    </span>
</a>

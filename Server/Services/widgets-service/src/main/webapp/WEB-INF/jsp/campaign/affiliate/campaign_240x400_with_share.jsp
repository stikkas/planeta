<%@include file="/WEB-INF/jsp/includes/generated/taglibs.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<a class="widget-block widget-1  ${params.theme}<c:if test="${params.moneyTarget <= 0}" > not-target</c:if><c:if test="${(params.moneyTarget >= 10000000 || params.moneyCollected >= 10000000)}" > target-over-8</c:if>" href="${params.campaignUrl}" target="_blank">
    <span class="cover-wrap">
      <span class="cover">
        <img src="${hf:getThumbnailUrl(params.campaignImgUrl, "BIG", "PRODUCT")}">
      </span>
    </span>
    <span class="widget-cont">
      <span class="name">${params.campaignName}</span>
      <span class="pledged">
        <fmt:formatNumber value="${params.moneyCollected}"/>
            <span class="b-rub">Р</span>
      </span>
        <c:if test="${params.moneyTarget > 0}" >
            <span class="progress-block">
            <span class="progress">
              <span class="progress-bar" style="width:${params.percentCollected}%;"></span>
            </span>
          </span>
        </c:if>
    </span>
    <span class="rewards">
      <span class="rewards-label">Вознаграждение проекта</span>
      <span class="rewards-cont">
        <span class="rewards-str"></span>
        <span class="rewards-name">${params.shareName}</span>
        <span class="widget-logo"></span>
        <span class="rewards-price">
          ${params.sharePrice}
              <span class="b-rub">Р</span>
        </span>
      </span>
    </span>
</a>

<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@include file="/WEB-INF/jsp/includes/generated/taglibs.jsp"%>
<%@include file="/WEB-INF/jsp/includes/generated/common-favicon.jsp" %>
<%@include file="/WEB-INF/jsp/includes/generated/common-templates.jsp" %>
<meta charset="UTF-8">
<c:if test="${campaign != null}">
    <title>${campaign.name}</title>
    <meta property="og:site_name" content="Planeta.ru"/>
    <meta property="og:title" content="<c:out value='${campaign.name}'/>"/>
    <meta property="og:description" content="<c:out value='${campaign.shortDescription}'/>"/>
    <meta name="description" content="<c:out value='${campaign.shortDescription}'/>"/>
    <meta property="og:image" content="<c:out value='${hf:getThumbnailUrl(campaign.imageUrl, "ALBUM_COVER", "EVENT")}'/>"/>
    <meta property="og:type" content="website"/>

    <link type="text/css" rel="stylesheet" href="//${hf:getStaticBaseUrl("")}/css-generated/affiliate.css" />
    <p:script src="templates.js" />
    <p:script src="widgetslist.js" />
</c:if>
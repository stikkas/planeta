<script type="text/javascript">
  var _gaq = _gaq || [];

  if (document.location.host.indexOf('localhost') >= 0 || document.location.host.indexOf('dev.planeta.ru') >= 0) {
      window.gauid = 'UA-40553911-1';
  }
  var pluginUrl = '//www.google-analytics.com/plugins/ga/inpage_linkid.js';
  _gaq.push(['_require', 'inpage_linkid', pluginUrl]);
  _gaq.push(['_setAccount', window.gauid || 'UA-29907913-1']);
  _gaq.push(['_trackPageview']);
  _gaq.push(['_setDomainName', (document.location.host.indexOf('localhost') >= 0) ? 'none' : document.location.host]);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
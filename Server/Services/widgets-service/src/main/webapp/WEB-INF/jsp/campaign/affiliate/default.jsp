<%@include file="/WEB-INF/jsp/includes/generated/taglibs.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<html>
<head>
    <meta charset="UTF-8">
    <title>Виджет</title>
    <link href="//${staticUrl}/css-generated/affiliate-widget.css" media="only screen" type="text/css" rel="stylesheet">
    <c:if test="${useExtendedJs}">
        <script src="//${staticUrl}/res/js/planeta-widget.js?compress=${properties['static.compress']}&flushCache=${properties['static.flushCache']}"></script>
    </c:if>
    <style>
        body
        {
            -webkit-text-stroke: 1px transparent;
            -webkit-font-smoothing: antialiased;
        }
        body {
            font-size: 125%;
            letter-spacing: 0.05em;
            line-height: 1.3em;
        }

        body>* {
            font-size: 85%;
            line-height: 1.3em;
        }
        body {
            -webkit-font-smoothing: subpixel-antialiased !important;
            -webkit-backface-visibility: hidden;
            -moz-backface-visibility:    hidden;
            -ms-backface-visibility:     hidden;
        }
    </style>
</head>
<body>
<jsp:include page="${name}.jsp">
    <jsp:param name="params" value="${params}"/>
</jsp:include>
</body>
</html>
<%@include file="/WEB-INF/jsp/includes/generated/taglibs.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<div class="affiliate-widgets-action-list mrg-b-25 js-shares-container hide">

    <h4>Вознаграждения проекта</h4>
    <c:forEach items="${shares}" var="share" varStatus="counter">
        <label class="radio">
            <input type="radio" class="js-radio-input" name="optionsRadios"
                   data-share-id="${share.shareId}" <c:if test="${counter.index == 0}">checked=""</c:if> />
            <span class="awa-name">
                <span class="span6">${share.name}</span>
                <b class="span blue">${share.price} <span class="b-rub">Р</span></b>
            </span>
        </label>
    </c:forEach>
</div>
<div class="widget-item-option">
    <c:if test="${sources != null}">
        <div class="affiliate-widgets-choice-options">
            <div class="aw-choice-options-label">
                <p class="mrg-b-5"><b>Рекламный поток</b></p>
                <p class="mrg-b-15"><small class="muted">Разделяйте Ваши источники трафика и выбирайте наиболее эффективные</small></p>
            </div>
            <div class="mrg-b-25">
                <select class="mrg-b-0" id="js-stream-list" required>
                    <option items="-1">Без потока</option>
                    <c:forEach items="${sources}" var="s">
                        <option value="${s.sourceId}">${s.name}</option>
                    </c:forEach>
                </select>
                <a data-toggle="tab" class="js-stream-create-a btn btn-mini mrg-l-10">Создать поток</a>
            </div>
        </div>
    </c:if>
    <div class="affiliate-widgets-choice-options content-inline mrg-b-0">
        <b>Тип промо-материала:</b>

        <ul class="nav nav-pills mrg-l-0 mrg-b-0">
            <li>
                <a href="javascript:void(0);" class="js-render-type" data-toggle="tab" data-render-type="image">Картинка</a>
            </li>
            <li class="active">
                <a href="javascript:void(0);" class="js-render-type" data-toggle="tab" data-render-type="iframe">iFrame</a>
            </li>
        </ul>
    </div>

    <textarea rows="4" class="aw-action-list-textarea" id="js-iframe-html">
    </textarea>
</div>
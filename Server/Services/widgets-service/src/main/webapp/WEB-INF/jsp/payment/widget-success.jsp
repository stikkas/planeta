<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<%@ include file="/WEB-INF/jsp/includes/taglibs.jsp" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta property="og:site_name" content="planeta.ru"/>
    <meta property="og:title" content="<spring:message code="payment-success.jsp.propertie.1" text="default text"> </spring:message><c:out value="${firstObject.ownerName}"/>"/>
    <meta property="og:image" content="${hf:getThumbnailUrl(firstObject.objectImageUrl, "ORIGINAL", "PRODUCT")}"/>
    <meta property="og:description" content="${hf:escapeEcmaScript(sharingDescription)}"/>
    <title><spring:message code="payment-success.jsp.propertie.2" text="default text"> </spring:message></title>
    <link type="text/css" media="only screen" rel="stylesheet" href="//${hf:getStaticBaseUrl("")}/css-generated/project-reward.css"/>
    <link type="text/css" rel="stylesheet" href="//${hf:getStaticBaseUrl("")}/css-generated/payment.css"/>
    <%@include file="/WEB-INF/jsp/includes/generated/stat-counters-head.jsp" %>
    <script type="text/javascript" src="https://${hf:getStaticBaseUrl("/res")}/js/libs.js?compress=${properties['static.compress']}&flushCache=${properties['static.flushCache']}"></script>
    <script type="text/javascript" src="https://${hf:getStaticBaseUrl("")}/js/utils/plugins/share-widget.js?compress=${properties['static.compress']}&flushCache=${properties['static.flushCache']}"></script>
    <%@include file="/WEB-INF/jsp/includes/generated/common-templates.jsp" %>

</head>
<body>

<script type="text/javascript">
    $(function() {
        var getDataForShare = {
            url : "https://${properties['application.host']}/campaigns/${firstObject.ownerId}",
            title: '<spring:message code="payment-success.jsp.propertie.1" text="default text"> </spring:message> ${hf:escapeEcmaScript(firstObject.ownerName)}',
            className: 'horizontal donate-sharing sharing-mini', counterEnabled: false, hidden: false,
            description: '${hf:escapeEcmaScript(sharingDescription)}',
            imageUrl: '${hf:getThumbnailUrl(firstObject.objectImageUrl, "ORIGINAL", "PRODUCT")}'
        };
        $('.share-cont-horizontal').share(getDataForShare);
    });
    $(document).ready(function() {
        if (window.gtm) {
            var transactionInfo = {
                transactionId: '${order.orderId}',
                amount: '${order.totalPrice}'
            };
            var shareInfo = {
                shareId: '${firstObject.objectId}',
                name: '${firstObject.objectName}',
                price: '${firstObject.price}',
                quantity: '${firstObject.count}'
            };
            window.gtm.trackPurchaseShares(transactionInfo, ${hf:toJson(gtmCampaignInfo)}, shareInfo);
        }

        $('.js-close').click(function () {
            parent.postMessage("close", "*");
        });
    });
</script>

<div class="pr-modal-close js-close"><i></i></div>
    <div class="pln-payment-box">
        <c:choose>
            <c:when test="${transaction.status == 'NEW' && !isMobilePayment && !isDeferred}">
                <div class="pln-payment-success">
                    <div class="pln-payment-success_head">
                        <spring:message code="payment-success.jsp.propertie.7" text="default text"> </spring:message>
                    </div>
                    <div class="pln-payment-success_sub-head">
                    </div>

                    <div class="pln-payment-success_widget cf">

                        <div class="pln-payment-success_widget_i">
                            <div class="pln-payment-success_widget_ico">
                                <span class="success-widget-icon success-widget-icon-time"></span>
                            </div>
                            <div class="pln-payment-success_widget_text">
                                <spring:message code="payment-success.jsp.propertie.8" text="default text"> </spring:message>
                            </div>
                        </div>

                        <div class="pln-payment-success_widget_i">
                            <div class="pln-payment-success_widget_ico">
                                <span class="success-widget-icon success-widget-icon-mail"></span>
                            </div>
                            <div class="pln-payment-success_widget_text">
                                <spring:message code="payment-success.jsp.propertie.9" text="default text"> </spring:message>
                            </div>
                        </div>

                        <div class="pln-payment-success_widget_i">
                            <div class="pln-payment-success_widget_ico">
                                <span class="success-widget-icon success-widget-icon-flag"></span>
                            </div>
                            <div class="pln-payment-success_widget_text">
                                <spring:message code="payment-success.jsp.propertie.10" text="default text"> </spring:message> <a href="/account"><spring:message code="payment-success.jsp.propertie.11" text="default text"> </spring:message></a>
                            </div>
                        </div>
                    </div>
                </div>
            </c:when>
            <c:otherwise>
                <div class="pln-payment-success">
                    <c:choose>
                        <c:when test="${isMobilePayment}">
                            <div class="pln-payment-success_head">
                                <spring:message code="payment-success.jsp.propertie.12" text="default text"> </spring:message>
                            </div>
                            <div class="pln-payment-success_sub-head">
                                <spring:message code="payment-success.jsp.propertie.31" text="default text"> </spring:message>
                                <br>
                                <spring:message code="payment-success.jsp.propertie.32" text="default text"> </spring:message>
                            </div>
                        </c:when>
                        <c:when test="${isDeferred}">
                            <div class="pln-payment-success_head">
                                <spring:message code="payment-success.jsp.propertie.14" text="default text"> </spring:message>
                            </div>
                            <div class="pln-payment-success_sub-head">
                                <spring:message code="payment-success.jsp.propertie.15" text="default text"> </spring:message>
                            </div>
                        </c:when>
                        <c:otherwise>
                            <div class="pln-payment-success_head">
                                <spring:message code="payment-success.jsp.propertie.16" text="default text"> </spring:message> <span class="hl"><spring:message code="payment-success.jsp.propertie.17" text="default text"> </spring:message></span>!
                            </div>
                            <div class="pln-payment-success_sub-head">
                                <spring:message code="payment-success.jsp.propertie.18" text="default text"></spring:message> <spring:message code="payment-success.jsp.propertie.18.1" text="default text"></spring:message><spring:message code="payment-success.jsp.propertie.18.2" text="default text"></spring:message><b>${order.buyerEmail}</b><spring:message code="payment-success.jsp.propertie.19" text="default text"></spring:message>
                            </div>
                        </c:otherwise>
                    </c:choose>

                    <div class="pln-payment-success_widget cf">
                        <div class="pln-payment-success_widget_i">
                            <div class="pln-payment-success_widget_ico">
                                <span class="success-widget-icon success-widget-icon-flag"></span>
                            </div>
                            <div class="pln-payment-success_widget_text">
                                <spring:message code="payment-success.jsp.propertie.20" text="default text"> </spring:message> <a href="${redirectUrl}"><spring:message code="payment-success.jsp.propertie.11" text="default text"> </spring:message></a>
                            </div>
                        </div>

                        <div class="pln-payment-success_widget_i">
                            <div class="pln-payment-success_widget_ico">
                                <span class="success-widget-icon success-widget-icon-star"></span>
                            </div>
                            <div class="pln-payment-success_widget_text">
                                <spring:message code="payment-success.jsp.propertie.21" text="default text"> </spring:message> <a href="https://${properties['application.host']}/campaigns/${campaign.webCampaignAlias}"><spring:message code="payment-success.jsp.propertie.22" text="default text"> </spring:message></a>
                            </div>
                        </div>

                        <div class="pln-payment-success_widget_i">

                            <div class="pln-payment-success_widget_sharing">
                                <div class="pln-payment-success_widget_sharing-head">
                                    <spring:message code="payment-success.jsp.propertie.23" text="default text"> </spring:message>
                                </div>
                                <div class="share-cont-horizontal"></div>
                            </div>

                            <div class="pln-payment-success_widget_text">
                                <spring:message code="payment-success.jsp.propertie.24" text="default text"> </spring:message>
                            </div>
                        </div>
                    </div>
                </div>
            </c:otherwise>
        </c:choose>
    </div>                
                
</body>
</html>

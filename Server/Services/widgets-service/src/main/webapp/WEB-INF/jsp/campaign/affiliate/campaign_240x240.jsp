<%@include file="/WEB-INF/jsp/includes/generated/taglibs.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<a class="widget-round widget-3 ${params.theme.font.asString}<c:if test="${params.moneyTarget <= 0}" > not-target</c:if>" href="${params.campaignUrl}" target="_blank">
    <span class="widget-arc" data-procent="${params.percentCollected}"></span>
        <span class="wr-cont">
            <span class="name">${params.campaignName}</span>
            <span class="pledged">
                <c:if test="${params.moneyTarget > 0}" >
                    <span class="pledged-val" data-finish="${params.moneyTarget}"><fmt:formatNumber value="${params.moneyCollected}"/></span>
                    <span class="custom-rub"><span class="cr-r">Р</span><span class="cr-dash">&ndash;</span></span>
                </c:if>
            </span>
            <span class="wr-donate">Поддержать</span>
        </span>
</a>

<script>
    var color = {
        'bg-black': {
            fill: '#000',
            strokeBgColor: '#dcdcdc'
        },
        'bg-white': {
            fill: '#fff',
            strokeBgColor: '#dcdcdc'
        }
    };

    var arc = $('.widget-arc');
    arc.arcDraw({
        radius: 118,
        fill: color['${params.theme.background.asString}'].fill,
        strokeWidth: 3,
        strokeBgColor: color['${params.theme.background.asString}'].strokeBgColor,
        strokeColor: '#26aedd'
    });

    <c:if test="${(params.moneyTarget > 0) && (params.moneyTarget > params.moneyCollected)}" >
    var widget = $('.widget-round');
    var pledged = $('.pledged-val');
    var pledgedVal = parseInt(pledged.text().replace(/\s/g, ''), 10);
    var pledgedFinish = parseInt(pledged.data('finish'), 10);
    pledged.data('queueVal', pledgedVal);

    widget.hover(function () {
        arc.arcDraw('animate', {start: arc.data('procent'), end: 100});
        numAnimate(pledged, pledgedVal, pledgedFinish);
    }, function () {
        arc.arcDraw('animate', {start: 100, end: arc.data('procent')});
        numAnimate(pledged, pledgedFinish, pledgedVal);
    });

    function numAnimate($el, from, to) {
        $({
            factVal: $el.data('queueVal') > from ? $el.data('queueVal') : from
        }).stop(1).animate({
                    factVal: to
                }, {
                    duration: 1000,
                    easing: 'swing',
                    step: function (value) {
                        $el.html(StringUtils.humanNumber(Math.floor(value)));
                        $el.data('queueVal', value);
                    }
                });
    }
    </c:if>
</script>

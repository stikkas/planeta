<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ include file="/WEB-INF/jsp/includes/generated/doctype-header.jsp" %>
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Проект ${campaign.name}</title>
    <link rel="stylesheet" href="//${hf:getStaticBaseUrl('')}/css/project-widget.css">
    <%@ include file="/WEB-INF/jsp/includes/google_analytics.jsp" %>
</head>
<body>
    <div class="shop-catalogue-list widget-336x280">
        <a href="https://${properties["application.host"]}/campaigns/${campaign.campaignId}<c:if test="${not empty refererParam}">?${refererParam}</c:if>" class="shop-catalogue-item" target="_blank">
            <div class="item-pic-wrap">
                <div class="item-pic-bg-cover"></div>
                <div class="item-pic">
                <span class="item-pic-link">
                    <img class="central-block" src="${hf:getThumbnailUrl(campaign.imageUrl, "BIG", "PRODUCT")}" alt="${campaign.name}">
                    <span class="vertical-align"></span>
                </span>
                </div>
                <div class="item-pic-bg"></div>
            </div>
            <div class="item-cont">
                <div class="item-cont-bg"></div>
                <div class="item-cont-planeta-logo"></div>
                <div class="item-info">
                    <div class="item-name">
                        <div class="item-name-cont">${hf:getShrinkedString(campaign.name, 128)}</div>
                    </div>
                </div>
                <div class="item-action">
                    <div class="action-info clearfix">
                        <div class="clearfix link-icon cont-va-middle pull-left">
                            <i class="icon-project icon-gray"></i>
                            <span class="text-icon">
                                <span class="hl price-count"><fmt:formatNumber value="${campaign.collectedAmount}"/></span> рублей собрано
                            </span>
                        </div>

                        <c:if test="${campaign.timeFinish != null}">
                            <div class="clearfix link-icon cont-va-middle">
                                <i class="icon-gray icon-alarm-clock"></i>
                                <span class="text-icon">
                                    <c:set var="now" value="<%=new java.util.Date()%>" />
                                    <c:set var="daysLeft" value="${hf:getDateDifferenceInDays(now, campaign.timeFinish)}"/>
                                    <span class="hl">${daysLeft}</span>
                                    &nbsp;<spring:message code="decl.days" arguments="${hf:plurals(daysLeft)}"/>
                                    &nbsp;<spring:message code="decl.left" arguments="${hf:plurals(daysLeft)}"/>
                                </span>
                            </div>
                        </c:if>
                    </div>
                </div>
            </div>
        </a>
    </div>
</body>
</html>
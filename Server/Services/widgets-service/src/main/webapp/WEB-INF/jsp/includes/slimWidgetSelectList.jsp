<%@include file="/WEB-INF/jsp/includes/generated/taglibs.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<div class="awc-item cf awc-widget-4" style="float:none">
    <div class="awc-head">728 x 90 <a class="awc-head-link" href="javascript:void(0);">Назад</a></div>
    <div class="awc-farme" style="position: relative">
        <iframe class="js-widget-iframe" data-widget-type="CAMPAIGN_728X90" src="" frameborder="0"
                width="728" height="90"></iframe>
        <div style="position: absolute;top: 0;right: 0;bottom: 0;left: 0;background:black;opacity:0"></div>
    </div>
</div>
<div class="awc-item cf awc-widget-5" style="float:none">
    <div class="awc-head">600 x 120 <a class="awc-head-link" href="javascript:void(0);">Назад</a></div>
    <div class="awc-farme" style="position: relative">
        <iframe class="js-widget-iframe" data-widget-type="CAMPAIGN_600X120_WITH_MONEY_TARGET" src=""
                frameborder="0" width="600" height="120"></iframe>
        <div style="position: absolute;top: 0;right: 0;bottom: 0;left: 0;background:black;opacity:0"></div>
    </div>
</div>
<div class="awc-item cf awc-widget-6" style="float:none">
    <div class="awc-head">600 x 120 <a class="awc-head-link" href="javascript:void(0);">Назад</a></div>
    <div class="awc-farme" style="position: relative">
        <iframe class="js-widget-iframe" data-widget-type="CAMPAIGN_600X120" src="" frameborder="0"
                width="600" height="120"></iframe>
        <div style="position: absolute;top: 0;right: 0;bottom: 0;left: 0;background:black;opacity:0"></div>
    </div>
</div>
<div class="awc-item cf awc-widget-7" style="float:none">
    <div class="awc-head">468 x 80 <a class="awc-head-link" href="javascript:void(0);">Назад</a></div>
    <div class="awc-farme" style="position: relative">
        <iframe class="js-widget-iframe" data-widget-type="CAMPAIGN_468X80" src="" frameborder="0"
                width="468" height="80"></iframe>
        <div style="position: absolute;top: 0;right: 0;bottom: 0;left: 0;background:black;opacity:0"></div>
    </div>
</div>
<%@ page contentType="text/javascript;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="hf" uri="http://planeta.ru/taglibs/HelperFunctions"%>
        var Planeta = function(id) {
            this.id = id;
        };
        Planeta.prototype = {
            campaignId: ${param.campaignId},
            appHost: '${properties['widgets.application.host']}',
            overlay: {},
            init: function() {
                var el = document.getElementById(this.id);
                if (el) {
                    el.onclick = this.bind(this.showModal);
                }
            },
            showModal: function() {
                this.scrollFix();
                document.getElementsByTagName('html')[0].className += ' reward-modal-open reward-modal-margin';
                this.overlay = this.createLayout();
                document.body.appendChild(this.overlay);
            },

            hideModal: function () {
                this.remove(this.overlay);
                this.removeClass(document.getElementsByTagName('html')[0], 'reward-modal-open');
                this.removeClass(document.getElementsByTagName('html')[0], 'reward-modal-margin');
            },

            createLayout: function() {
                var ie7 = document.all && !document.querySelector;

                var $overlay = this.createElement('div', {
                    position: 'fixed',
                    top: 0,
                    left: 0,
                    bottom: 0,
                    right: 0,
                    zIndex: 20000,
                    height: '100%',
                    background: 'rgba(0,0,0,.7)',
                    overflow: 'auto',
                    overflowY: 'scroll',
                    textAlign: 'center',
                    whiteSpace: 'nowrap',
                    fontSize: 0,
                    lineHeight: 0
                });

                var $wrap = this.createElement('div', {
                    display: 'inline-block',
                    whiteSpace: 'normal',
                    verticalAlign: 'middle',
                    textAlign: 'left',
                    padding: '20px 0'
                });

                if (ie7) {
                    $wrap.style.display = 'inline';
                    $wrap.style.zoom = 1;
                }

                var $verticalFix = this.createElement('div', {
                    display: 'inline-block',
                    verticalAlign: 'middle',
                    width: 0,
                    height: '100%'
                });

                if (ie7) {
                    $verticalFix.style.display = 'inline';
                    $verticalFix.style.zoom = 1;
                }

                var $cont = this.createElement('div', {position: 'relative'});

                var $loader = this.createElement('div', {
                    position: 'absolute',
                    top: 0,
                    bottom: 0,
                    left: 0,
                    right: 0,
                    zIndex: 10,
                    background: '#fff url(https://${hf:getStaticBaseUrl("")}/images/planeta/notif-loader.gif) no-repeat 50% 50%'
                });

                var $loaderImg = this.createElement('div', {
                    position: 'absolute',
                    top: 0,
                    bottom: 0,
                    left: 0,
                    right: 0,
                    background: 'url(https://${hf:getStaticBaseUrl("")}/images/banners/logo-300x600.png) no-repeat 50% 100px'
                });

                var $frame = this.createElement('iframe', {
                    position: 'relative',
                    margin: 0,
                    padding: 0,
                    outline: 'none',
                    border: 0,
                    background: 'transparent'
                });
                $frame.onload = function() {
                    $loader.style.zIndex = -1;
                };
                $frame.setAttribute('frameborder', 0);
                $frame.setAttribute('width', 950);
                $frame.setAttribute('height', 650);
                var url = "//" + this.appHost + "/widgets/payment-widget?campaignId=" + this.campaignId;
                $frame.setAttribute('src', url);
                $overlay.appendChild($wrap);
                $overlay.appendChild($verticalFix);
                $wrap.appendChild($cont);
                $loader.appendChild($loaderImg);
                $cont.appendChild($loader);
                $cont.appendChild($frame);

                return $overlay;
            },

            scrollFix: function() {
                this.remove('reward-modal-style');
                this.remove('reward-modal-margin');

                var style = document.createElement('style');
                style.setAttribute('id', 'reward-modal-style');
                style.setAttribute('type', 'text/css');
                style.innerHTML = '.reward-modal-lock-test{overflow-y:hidden!important}.reward-modal-open{overflow:visible}.reward-modal-open body{overflow:hidden}';

                document.head.appendChild(style);
                var html = document.getElementsByTagName('html')[0];
                var w1 = window.innerWidth;
                var htmlClass = html.className;
                html.className += ' reward-modal-lock-test';
                var w2 = window.innerWidth;
                html.className = htmlClass;
                style = document.createElement('style');
                style.setAttribute('id', 'reward-modal-margin');
                style.setAttribute('type', 'text/css');
                style.innerHTML = '.reward-modal-margin{margin-right:' + (w2 - w1) + 'px;}';
                document.head.appendChild(style);
            },

            createElement: function(name, style) {
                var el = document.createElement(name);
                if (style) {
                    this.css(el, style);
                }
                return el;
            },
            css: function(el, style) {
                for (var i in style) {
                    el.style[i] = style[i];
                }
            },
            remove: function(id) {
                var el = typeof(id) == 'string' ? document.getElementById(id) : id;
                el && el.parentNode && el.parentNode.removeChild(el);
            },
            removeClass: function(el, className) {
                el.className = el.className.replace(new RegExp('\\s*' + className, 'g'), '');
            },
            bind: function(fn) {
                var ctx = this;
                return function() {
                    fn.apply(ctx, arguments);
                };
            }
        };
var planeta = {
    instance: {},
    init: function (id) {
        if (this.instance[id]) {
            this.instance[id].showModal();
            return;
        }
        var self = this;
        window.addEventListener('message', function(e) {
            if (e.data == 'close') {
                self.close();
            }
        }, false);

        this.instance[id] = new Planeta(id);
        this.instance[id].init();
    },
    close: function () {
        for (var id in this.instance) {
            this.instance[id].hideModal();
            <%--deleteByProfileId this.instance[id];--%>
        }
    },
    changeConfig: function (id, conf) {
        if (!this.instance[id]) return;
        for (var f in conf) {
            this.instance[id][f] = conf[f];
        }
    }};

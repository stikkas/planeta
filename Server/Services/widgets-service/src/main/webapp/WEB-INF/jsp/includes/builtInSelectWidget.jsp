<%@include file="/WEB-INF/jsp/includes/generated/taglibs.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<div class="widget-button-preview-block">
    <div class="row">
        <div class="span5">

            <div class="widget-button-preview js-preview-button-container">

            </div>

            <div class="wbp-descr">
                Так будет<br>
                выглядеть кнопка<br>
                на Вашем сайте
            </div>

            <div class="wbp-descr-link-block js-show-preview">
                <span class="wbp-descr-link">Показать превью</span>
            </div>

        </div>

        <div class="span13">
            <h3><b>Встроенная покупка</b></h3>

            <p>Вставляя данную кнопку на сайт, вы позволите посетителю поддержать проект не переходя на сайт Planeta.ru с Вашего ресурса. Покупка будет осуществлена во всплывающем окне.</p>

            <br>

            <label><b>Скопируйте код кнопки и вставьте на сайт.</b>

            <textarea rows="8" class="aw-action-list-textarea mrg-t-10 span9 js-builtin-sales-textarea">

            </textarea>
            </label>
        </div>
    </div>
</div>
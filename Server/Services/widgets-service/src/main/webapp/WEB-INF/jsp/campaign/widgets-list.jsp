<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ include file="/WEB-INF/jsp/includes/generated/doctype-header.jsp" %>
<head>
    <%@ include file="/WEB-INF/jsp/includes/head.jsp" %>
    <script type="text/javascript">
        <c:import url="/WEB-INF/jsp/includes/common-js.jsp"/>
    </script>
</head>

<body>
<script type="text/javascript">
    page = WidgetsPage;
    page.campaignId = ${campaignId};

    $(document).ready(function () {
        var link = 'https://' + workspace.hosts.mainHost + '/campaigns/' + page.webCampaignAlias;
        var shareData = {
            counterEnabled: false,
            hidden: false,
            parseMetaTags: true,
            url: link
        };
        shareData.className = 'sharing-popup-social horizontal donate-sharing';
        $('#sharing-block').share(shareData);
    });
</script>

<div id="affilate-wrap">
    <div class="project-link-block">
        <div class="container">
            <div class="project-link-block-left">
                <img class="pl-block-img" src="${hf:getThumbnailUrl(campaign.imageUrl, 'ALBUM_COVER', 'EVENT')}">
            </div>
            <div class="project-link-block-right">
                <div class="pl-block-title"><c:out value="${campaign.name}"/></div>
                <div class="pl-block-bottom">
                    <span class="mrg-r-5">Ссылка на проект</span>
                    <input class="pl-block-bottom-input span10" onClick="this.select();" type="text" value="https://${properties['application.host']}/campaigns/${campaign.webCampaignAlias}">
                </div>
                <div id="sharing-block"></div>
            </div>
        </div>
    </div>

    <div id="global-container" class="container">
        <div id="main-container" class="row main-container">

            <div class="main-container">
                <div class="widget-list">
                    <h2>Выберите один среди множества промо-материалов</h2>
                    <h3>Кликните по нему. Скопируйте код. Вставьте код на свой сайт или блог.</h3>

                    <div class="modal-widget-nav">
                        <a class="modal-widget-nav-item active" href="javascript:void(0);">
                            <i class="modal-widget-nav-icon-default"></i>
                            <span class="modal-widget-nav-text">Обычный</span>
                        </a>
                        <a class="modal-widget-nav-item" href="javascript:void(0);">
                            <i class="modal-widget-nav-icon-slim"></i>
                            <span class="modal-widget-nav-text">Slim</span>
                        </a>
                        <a class="modal-widget-nav-item js-built-in-sales" href="javascript:void(0);">
                            <i class="modal-widget-nav-icon-bay"></i>
                            <span class="modal-widget-nav-text">Встроенная покупка</span>
                        </a>
                    </div>

                    <div class="affiliate-widgets-choice-options content-inline mrg-t-30 mrg-b-20">
                        <b>Цвет промо-материала:</b>

                        <ul class="nav nav-pills mrg-l-0 mrg-b-0">
                            <li class="js-color active" data-color="WHITE">
                                <a href="javascript:void(0);">Белый</a>
                            </li>
                            <li class="js-color" data-color="BLACK">
                                <a href="javascript:void(0);">Черный</a>
                            </li>
                        </ul>
                    </div>


                    <div class="affiliate-widgets-choice-list clearfix">
                        <div class="awc-list-pane active">
                            <%@ include file="/WEB-INF/jsp/includes/usualWidgetSelectList.jsp"%>
                        </div>
                        <div class="awc-list-pane">
                            <%@ include file="/WEB-INF/jsp/includes/slimWidgetSelectList.jsp"%>
                        </div>
                        <div class="awc-list-pane">
                            <%@ include file="/WEB-INF/jsp/includes/builtInSelectWidget.jsp"%>
                        </div>
                        <div class="awc-list-opt hide">
                            <%@ include file="/WEB-INF/jsp/includes/selectWidgetOptions.jsp"%>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

</body>
package ru.planeta.widgets.controllers;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.TypeMismatchException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import ru.planeta.api.exceptions.NoContentException;
import ru.planeta.api.exceptions.NotFoundException;
import ru.planeta.api.exceptions.PermissionException;
import ru.planeta.api.helper.ProjectService;
import ru.planeta.api.model.enums.widget.WidgetBackgroundColor;
import ru.planeta.api.model.enums.widget.WidgetFontColor;
import ru.planeta.api.model.enums.widget.WidgetNames;
import ru.planeta.api.model.widget.WidgetColorTheme;
import ru.planeta.api.service.billing.payment.settings.PaymentSettingsService;
import ru.planeta.api.service.campaign.CampaignService;
import ru.planeta.api.service.geo.GeoService;
import ru.planeta.api.service.statistic.StatisticsService;
import ru.planeta.api.service.widget.WidgetService;
import ru.planeta.api.web.controllers.services.BaseControllerService;
import ru.planeta.commons.web.CookieUtils;
import ru.planeta.commons.web.ImageUtils;
import ru.planeta.commons.web.WebUtils;
import ru.planeta.model.common.campaign.Campaign;
import ru.planeta.model.common.campaign.Share;
import ru.planeta.model.common.campaign.ShareDetails;
import ru.planeta.model.enums.ProjectType;
import ru.planeta.model.stat.StatEventType;
import ru.planeta.widgets.model.TypedImage;
import ru.planeta.widgets.model.WidgetType;

import javax.annotation.PostConstruct;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Controller for widgets
 *
 * @author m.shulepov
 */
@Controller
public class WidgetsController {

    private static final Logger log = Logger.getLogger(WidgetsController.class);

    @Autowired
    private BaseControllerService baseControllerService;
    @Autowired
    private WidgetService widgetService;
    @Autowired
    private CampaignService campaignService;
    @Autowired
    private PaymentSettingsService paymentSettingsService;
    @Autowired
    private StatisticsService statisticsService;
    @Autowired
    private GeoService geoService;
    @Autowired
    private ProjectService projectService;

    private String widgetsHost;

    @Value("${static.host}")
    private String staticAppHost;
    @Value("${prerender.application.host:}")
    private String prerenderHost;

    @Value("${application.host}")
    private String mainAppHost;

    /*
    private List<String> trustedDomains;

    @PostConstruct
    private void fillTrustedDomains() {
        trustedDomains = new ArrayList<>();
        trustedDomains.add(mainAppHost);
    }
    */

    @PostConstruct
    public void init() {
        widgetsHost = projectService.getUrl(ProjectType.WIDGETS);
    }

    private static String getPrerenderHostTemplate(String host) {
        return "http://" + host + "/%s";
    }

    @RequestMapping(value = Urls.CAMPAIGN_WIDGET, method = RequestMethod.GET)
    public ModelAndView getCampaignWidget(@RequestParam(value = "campaignid") long campaignId,
                                          @RequestParam(value = "type", defaultValue = "STANDARD") String widgetName,
                                          @RequestHeader(value = "Referer", required = false) String referer) throws PermissionException, NotFoundException {
        Campaign campaign = campaignService.getCampaignSafe(campaignId);

        WidgetType widgetType = getWidgetTypeFromString(widgetName);
        ModelAndView modelAndView = new ModelAndView(getViewName(widgetType, Actions.CAMPAIGN_WIDGET));
        modelAndView.addObject("campaign", campaign);
        modelAndView.addObject("widgetType", widgetType);
        modelAndView.addObject("targetAmountLong", campaign.getTargetAmount().longValue());
        return modelAndView;
    }

    private static BigDecimal getRecommendedDonate(List<? extends Share> shares) {
        BigDecimal recommendedDonate = BigDecimal.ZERO;
        for (Share share1 : shares) {
            if (share1.getPrice().compareTo(recommendedDonate) < 0 && share1.getPrice().compareTo(BigDecimal.ZERO) > 0 ||
                    recommendedDonate.compareTo(BigDecimal.ZERO) == 0) {
                recommendedDonate = share1.getPrice();
            }
        }
        return recommendedDonate;
    }

    private String buildTypedImageUrl(TypedImage image, Map<String, String[]> params) {
        String url = WebUtils.appendProtocolIfNecessary(mainAppHost + image.getUrl(), true);
        Map<String, String> additionalParams = new HashMap<>();
        for (Map.Entry<String, String[]> entry : params.entrySet()) {
            if (StringUtils.isEmpty(entry.getKey())
                    || entry.getKey().equals("type")
                    || entry.getValue() == null
                    || entry.getValue().length == 0) {
                continue;
            }
            additionalParams.put(entry.getKey(), entry.getValue()[0]);
        }
        return WebUtils.createUrl(url, additionalParams);
    }

    private static TypedImage getTypedImageFromString(String type) {
        TypedImage image = TypedImage.SCHOOL;
        try {
            image = TypedImage.valueOf(type);
        } catch (Exception ignored) {
            log.warn("TypeCastError", ignored);
        }
        return image;
    }

    private static WidgetType getWidgetTypeFromString(String type) {
        WidgetType widgetType = WidgetType.STANDARD;
        try {
            widgetType = WidgetType.valueOf(type.toUpperCase());
        } catch (Exception ignored) {
            log.warn("TypeCastError", ignored);
        }
        return widgetType;
    }

    // Private helper methods

    private static String getViewName(WidgetType widgetType, Actions action) {
        return action.getPath() + "_" + widgetType.getName();
    }

    private String getRequestUrl(String url, WidgetNames widgetName) {
        return String.format(getPrerenderHostTemplate(prerenderHost), url, widgetName.getWidth(), widgetName.getHeight());
    }

    private String getRequestUrl(String url) {
        return String.format(getPrerenderHostTemplate(prerenderHost), url);
    }

    @RequestMapping(value = Urls.AFFILIATE_CAMPAIGN_WIDGET, method = RequestMethod.GET)
    public ModelAndView getAffiliateCampaignWidget(@RequestParam("name") WidgetNames name,
                                                   @RequestParam(value = "background", defaultValue = "WHITE") WidgetBackgroundColor backgroundColor,
                                                   @RequestParam(value = "font-color", defaultValue = "BLACK") WidgetFontColor fontColor,
                                                   @RequestParam("campaign-id") long campaignId,
                                                   @RequestParam(value = "share-id", defaultValue = "0") long shareId,
                                                   HttpServletRequest request
    ) throws Exception {

        statisticsService.trackEvent(StatEventType.WIDGET_IFRAME_SHOW, campaignId, WebUtils.getReferer(request), getVisitorId(request));
        ModelAndView mav = new ModelAndView(Actions.AFFILIATE_CAMPAIGN_WIDGET.toString());
        mav.addObject("staticUrl", staticAppHost);
        mav.addObject("useExtendedJs", name == WidgetNames.CAMPAIGN_240X240);
        mav.addObject("name", name);
        mav.addObject("params", widgetService.getCampaignWidgetDTO(name, campaignId, shareId, new WidgetColorTheme(backgroundColor, fontColor)));
        return mav;
    }

    private static long getVisitorId(HttpServletRequest request) {
        return NumberUtils.toLong(CookieUtils.getCookieValue(request.getCookies(), CookieUtils.VISITOR_COOKIE_NAME, null));
    }

    @RequestMapping(value = Urls.AFFILIATE_CAMPAIGN_WIDGET_IMAGE, method = RequestMethod.GET)
    public void getAffiliateCampaignWidgetImage(@RequestParam(value = "name") WidgetNames widgetName,
                                                @RequestParam(value = "campaign-id", required = false) Long campaignId,
                                                HttpServletRequest request,
                                                HttpServletResponse response) throws NotFoundException, IOException {

        if (widgetName == null) {
            throw new NotFoundException(WidgetNames.class, 0);
        }
        if (campaignId != null && campaignId != 0) {
            statisticsService.trackEvent(StatEventType.WIDGET_IMAGE_SHOW, campaignId, WebUtils.getReferer(request), getVisitorId(request));
        }
        String url = getEncodedUrl(WebUtils.appendProtocolIfNecessary(getImageRequestUrl(Urls.AFFILIATE_CAMPAIGN_WIDGET_IFRAME, request), false));
        String requestUrl = getRequestUrl(url, widgetName);
        log.info("Request url: " + requestUrl);
        response.setContentType("image/png");
        downloadAndCutImage(widgetName.getWidth(), widgetName.getHeight(), requestUrl, response.getOutputStream());
    }

    @RequestMapping(method = RequestMethod.GET, value = Urls.RENDER_IMAGE)
    public void renderTypedImage(@RequestParam(value = "type") String type,
                                 HttpServletRequest request,
                                 HttpServletResponse response) throws NotFoundException, IOException {
        if (StringUtils.isEmpty(type)) {
            throw new NotFoundException("image type is empty");
        }

        TypedImage image = getTypedImageFromString(type);
        String url = getEncodedUrl(buildTypedImageUrl(image, request.getParameterMap()));
        String requestUrl = getRequestUrl(url);
        log.info("Request url: " + requestUrl);
        response.setContentType("image/png");
        downloadAndCutImage(image.getWidth(), image.getHeight(), requestUrl, response.getOutputStream());
    }

    /*
    //@RequestMapping(method = RequestMethod.GET, value = Urls.RENDER_IMAGE)
    public void renderCustomImage(@RequestParam(value = "url") String url,
                                  @RequestParam(value = "width") int width,
                                  @RequestParam(value = "height") int height,
                            HttpServletRequest request,
                            HttpServletResponse response) throws NotFoundException, IOException, PermissionException, URISyntaxException {
        if (StringUtils.isEmpty(url)) {
            throw new NotFoundException("url is empty");
        }
        if (!isTrustedDomain(url)) {
            throw new PermissionException("domain is not trusted");
        }
        String requestUrl = getEncodedUrl(WebUtils.appendProtocolIfNecessary(url, true));
        requestUrl = getRequestUrl(requestUrl);
        log.info("Request url: " + requestUrl);
        response.setContentType("image/png");
        downloadAndCutImage(width, height, requestUrl, response.getOutputStream());
    }

    private boolean isTrustedDomain(String url) throws URISyntaxException {
        URI uri = new URI(url);
        String validatedDomain = uri.getHost() + (uri.getPort()!=-1  ? ":" + uri.getPort() : "");
        for (String domain : trustedDomains) {
            if (validatedDomain.equals(domain)) {
                return true;
            }
        }
        return false;
    }
    */


    private void downloadAndCutImage(int width, int height, String requestUrl, OutputStream outputStream) {
        try {
            URL link = new URL(requestUrl);
            InputStream inputStream = link.openStream();
            try {
                ImageUtils.crop(inputStream, width, height, outputStream);
            } finally {
                IOUtils.closeQuietly(inputStream);
            }
        } catch (Exception ex) {
            log.warn("Image get error", ex);
        }
    }

    private static String getEncodedUrl(String imageRequestUrl) {
        try {
            return URLEncoder.encode(imageRequestUrl, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            return imageRequestUrl;
        }
    }


    private String getImageRequestUrl(String path, HttpServletRequest request) {
        return widgetsHost + path + "?image=true&" + request.getQueryString();
    }

    @RequestMapping(value = Urls.AFFILIATE_CAMPAIGN_WIDGET_IFRAME, method = RequestMethod.GET)
    public ModelAndView getAffiliateIframeImage(@RequestParam(value = "name", required = false) WidgetNames widgetName,
                                                HttpServletRequest request) {

        ModelAndView mav = new ModelAndView("iframe-widget");
        mav.addObject("widget", widgetName);
        mav.addObject("url", getImageRequestUrl(Urls.AFFILIATE_CAMPAIGN_WIDGET, request));
        return mav;
    }

    @RequestMapping(value = Urls.PAYMENT_WIDGET, method = RequestMethod.GET)
    public ModelAndView getPaymentWidget(@RequestParam long campaignId,
                                         HttpServletRequest request) throws NotFoundException, PermissionException {

        statisticsService.trackEvent(StatEventType.WIDGET_PURCHASE_SHOW, campaignId, WebUtils.getReferer(request), getVisitorId(request));
        List<ShareDetails> shares = campaignService.getCampaignWidgetDetailedShares(campaignId);
        return baseControllerService.createDefaultModelAndView(Actions.PAYMENT_WIDGET, ProjectType.WIDGETS)
                .addObject("backersCount", campaignService.getCampaignBackersCount(campaignId))
                .addObject("paymentMethods", paymentSettingsService.getPaymentMethodsPlain(ProjectType.WIDGETS, false, false))
                .addObject("campaign", campaignService.getCampaign(campaignId))
                .addObject("shares", shares)
                .addObject("recommendedDonate", getRecommendedDonate(shares))
                .addObject("countries", geoService.getCountries());
    }

    @RequestMapping(value = Urls.WIDGETS_LIST, method = RequestMethod.GET)
    public ModelAndView getWidgetListForCampaignExternal(@RequestParam long campaignId) throws NotFoundException, PermissionException {
        ModelAndView mav = baseControllerService.createDefaultModelAndView(Actions.WIDGETS_LIST, ProjectType.WIDGETS);
        mav.addObject("campaign", campaignService.getCampaignSafe(campaignId));
        mav.addObject("shares", campaignService.getCampaignSharesFilteredForWidgets(campaignId));
        mav.addObject("campaignId", campaignId);
        return mav;
    }

    @ExceptionHandler(TypeMismatchException.class)
    public void handleTypeMismatchException(HttpServletRequest request, TypeMismatchException ex) throws IOException, ServletException, NoContentException {
        // Rethrow exception if request is not for screenshot image
        if (!(request.getParameter("image") == null || request.getRequestURI().equals(Urls.AFFILIATE_CAMPAIGN_WIDGET_IMAGE))) {
            if (ex != null) {
                throw ex;
            }
        } else {
            throw new NoContentException();
        }
    }
}

package ru.planeta.widgets.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import ru.planeta.api.exceptions.NotFoundException;
import ru.planeta.api.exceptions.PermissionException;
import ru.planeta.api.model.ActionStatus;
import ru.planeta.model.profile.Profile;
import ru.planeta.widgets.service.DigitalAppAuthService;

/**
 * Controller for digital applications
 *
 * @author m.shulepov
 */
@Controller
public class DigitalAppController {

    @Autowired
    private DigitalAppAuthService digitalAppAuthService;

    @RequestMapping(value = Urls.AUTH_APP_USER, method = RequestMethod.GET)
    @ResponseBody
    public ActionStatus authProfile(@RequestParam(value = "username", required = true) String username,
                                    @RequestParam(value = "password", required = true) String password,
                                    @RequestParam(value = "appId", required = true) long appId) throws PermissionException, NotFoundException {

        Profile profile = digitalAppAuthService.getProfile(username, password, appId);

        return ActionStatus.createSuccessStatus(profile);
    }

}

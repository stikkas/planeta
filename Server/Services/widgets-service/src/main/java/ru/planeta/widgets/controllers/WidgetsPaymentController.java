package ru.planeta.widgets.controllers;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.context.MessageSource;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import ru.planeta.api.exceptions.*;
import ru.planeta.api.helper.ProjectService;
import ru.planeta.api.model.ActionStatus;
import ru.planeta.api.model.json.DeliveryInfo;
import ru.planeta.api.service.billing.order.OrderService;
import ru.planeta.api.service.billing.payment.PaymentService;
import ru.planeta.api.service.geo.DeliveryAddressService;
import ru.planeta.api.web.controllers.services.BasePaymentControllerService;
import ru.planeta.api.web.model.SharePurchase;
import ru.planeta.model.common.DeliveryAddress;
import ru.planeta.model.common.Order;
import ru.planeta.model.common.TopayTransaction;
import ru.planeta.model.enums.ProjectType;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;

import static ru.planeta.api.web.authentication.AuthHelper.isAnonymous;

/**
 * Created with IntelliJ IDEA.
 * User: a.volkov
 * Date: 22.10.13
 * Time: 13:51
 */

@RestController
public class WidgetsPaymentController {

    private final ProjectService projectService;
    private final MessageSource messageSource;
    private final PaymentService paymentService;
    private final OrderService orderService;
    private final BasePaymentControllerService basePaymentControllerService;
    private final DeliveryAddressService deliveryAddressService;

    public WidgetsPaymentController(ProjectService projectService,
                                    MessageSource messageSource,
                                    PaymentService paymentService,
                                    OrderService orderService,
                                    BasePaymentControllerService basePaymentControllerService,
                                    DeliveryAddressService deliveryAddressService) {
        this.projectService = projectService;
        this.messageSource = messageSource;
        this.paymentService = paymentService;
        this.orderService = orderService;
        this.basePaymentControllerService = basePaymentControllerService;
        this.deliveryAddressService = deliveryAddressService;
    }

    @PostMapping(Urls.PAY)
    public ActionStatus pay(@Valid @RequestBody SharePurchase purchase, BindingResult result,
                            HttpServletRequest request, HttpServletResponse response) throws
            NotFoundException, UnsupportedEncodingException, OrderException,
            PermissionException, PaymentException, RegistrationException {
        boolean wasAnonimous = isAnonymous();
        if (result.hasErrors()) {
            return ActionStatus.createErrorStatus(result, messageSource);
        }
        //unescaping customerName
        purchase.setCustomerName(StringEscapeUtils.unescapeHtml4(purchase.getCustomerName()));
        long shareId = purchase.getShareId();

        Pair<BindingResult, Long> errorsAndUserId = basePaymentControllerService
                .checkEmailAndGetProfileId(purchase.getEmail(), result, request, response);
        if (errorsAndUserId.getLeft().hasErrors()) {
            return ActionStatus.createErrorStatus(result, messageSource);
        }
        long buyerId = errorsAndUserId.getRight();

        DeliveryInfo deliveryInfo = basePaymentControllerService.getDeliveryInfo(shareId, purchase.getServiceId());

        DeliveryAddress deliveryAddress = deliveryAddressService.getDeliveryAddress(purchase.getAddress(), purchase.getCustomerName());

        final Order order = orderService.createOrderWithShare(buyerId, buyerId, shareId,
                purchase.getDonateAmount(), purchase.getQuantity(), purchase.getReply(), deliveryAddress, deliveryInfo, ProjectType.WIDGETS);

        BigDecimal donateAmount = purchase.getDonateAmount();
        BigDecimal toPay = deliveryInfo == null ? donateAmount : donateAmount.add(deliveryInfo.getDeliveryPrice());
        final TopayTransaction transaction = paymentService.createOrderPayment(order, toPay, purchase.getPaymentMethodId(), ProjectType.WIDGETS,
                purchase.getPhone(), true);
        String redirectUrl = projectService.getPaymentGateRedirectUrl(transaction);

        return ActionStatus.createSuccessStatus(redirectUrl);
    }
}


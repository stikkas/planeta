package ru.planeta.widgets.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created with IntelliJ IDEA.
 * User: a.volkov
 * Date: 22.10.13
 * Time: 19:35
 */
@Controller
public class WidgetsResourceController {

    @RequestMapping(value = "/js/button.js", method = RequestMethod.GET)
    public String getButtonScript() {
        return "js/button";
    }

}

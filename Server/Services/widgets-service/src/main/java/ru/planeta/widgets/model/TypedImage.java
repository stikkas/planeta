package ru.planeta.widgets.model;

/**
 * Created with IntelliJ IDEA.
 * User: michail
 * Date: 29.09.2017
 * Time: 11:40
 */
public enum TypedImage {
    SCHOOL("/welcome/school-cert-render.html?renderAsPng=1", 1024, 724);
    private String url;
    private int width;
    private int height;

    TypedImage(String url, int width, int height) {
        this.url = url;
        this.width = width;
        this.height = height;
    }

    public String getUrl() {
        return url;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

}
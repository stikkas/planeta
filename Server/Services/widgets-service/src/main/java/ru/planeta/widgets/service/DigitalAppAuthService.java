package ru.planeta.widgets.service;

import ru.planeta.api.exceptions.NotFoundException;
import ru.planeta.api.exceptions.PermissionException;
import ru.planeta.model.profile.Profile;

/**
 * Authentication service for digital apps
 * User: m.shulepov
 */
public interface DigitalAppAuthService {

    /**
     * Returns profile if supplied credentials are valid and profile has permission to access application with specified <code>appId</code>
     * @param username
     * @param password
     * @param appId
     * @return
     * @throws NotFoundException
     * @throws PermissionException
     */
    Profile getProfile(String username, String password, long appId) throws NotFoundException, PermissionException;

}

package ru.planeta.widgets.model;

/**
 * Enum that represents widget types
 * Note: {@code name} property of this enum participates in constructing path to the specified widget,
 * if you change the name also change the corresponding file name
 * User: m.shulepov
 */
public enum WidgetType {

    STANDARD("standard", 228, 289),
    FAT_SKYSCRAPER("240x400", 240, 400),
    SQUARE_250("250x250", 250, 250),
    SQUARE_336("336x280", 336, 280);

    private String name;
    private int width;
    private int height;

    WidgetType(String name, int width, int height) {
        this.name = name;
        this.width = width;
        this.height = height;
    }

    public String getName() {
        return name;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

}

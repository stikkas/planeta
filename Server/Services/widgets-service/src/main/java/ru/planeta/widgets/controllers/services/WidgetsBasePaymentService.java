package ru.planeta.widgets.controllers.services;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import ru.planeta.api.web.controllers.services.BasePaymentService;
import ru.planeta.model.common.Order;
import ru.planeta.model.common.TopayTransaction;
import ru.planeta.widgets.controllers.Actions;

@Service
public class WidgetsBasePaymentService implements BasePaymentService {

    @NotNull
    @Override
    public String getPaymentSuccessActionName(@Nullable Order order) {
        return Actions.PAYMENT_SUCCESS.toString();
    }

    @Nullable
    @Override
    public String getPaymentSourceUrl(@NotNull TopayTransaction transaction) {
        return null;
    }
}

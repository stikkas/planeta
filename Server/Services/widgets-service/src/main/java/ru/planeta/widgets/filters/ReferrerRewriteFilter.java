package ru.planeta.widgets.filters;

import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * User: a.volkov
 * Date: 27.11.13
 * Time: 18:40
 */
public class ReferrerRewriteFilter implements Filter {

    private static final Logger log = Logger.getLogger(ReferrerRewriteFilter.class);

    private static final String HTTP_REFERER = "Referer";
    private static final String REF_PARAM = "ref";

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest httpRequest = (HttpServletRequest) request;

        HttpServletRequestWrapper wrapper = new HttpServletRequestWrapper(httpRequest) {
            @Override
            public String getHeader(String name) {
                String value = super.getHeader(name);
                if (HTTP_REFERER.equals(name)) {
                    String ref = super.getParameter(REF_PARAM);
                    if (ref != null) {
                        try {
                            value = new String(Base64.decodeBase64(ref), "UTF-8");
                        } catch (Exception e) {
                            log.error("can't decode referer header from request param", e);
                        }
                    }
                }
                return value;
            }
        };
        chain.doFilter(wrapper, response);
    }

    @Override
    public void destroy() {
    }
}

package ru.planeta.widgets.service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import ru.planeta.api.exceptions.MessageCode;
import ru.planeta.api.exceptions.NotFoundException;
import ru.planeta.api.exceptions.PermissionException;
import ru.planeta.api.model.UserAuthorizationInfo;
import ru.planeta.model.common.OrderInfo;
import ru.planeta.model.common.OrderObjectInfo;
import ru.planeta.api.service.billing.order.OrderService;
import ru.planeta.api.service.profile.AuthorizationService;
import ru.planeta.model.enums.OrderObjectType;
import ru.planeta.model.enums.UserStatus;
import ru.planeta.model.profile.Profile;

import java.io.IOException;
import java.util.*;

/**
 * User: m.shulepov
 * Date: 17.10.12
 * Time: 16:22
 */
@Service
public class DigitalAppAuthServiceImpl implements DigitalAppAuthService {

    @Autowired
    private AuthorizationService authorizationService;

    @Autowired
    private OrderService orderService;

    private Map<Long, List<Long>> appRelatedShares;

    @Value("${app.related.shares}")
    public void parseAppRelatedShares(String appRelatedSharesJson) throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        appRelatedShares = objectMapper.readValue(appRelatedSharesJson, new TypeReference<Map<Long, List<Long>>>() {
        });
    }

    @Override
    public Profile getProfile(String username, String password, long appId) throws NotFoundException, PermissionException {
        UserAuthorizationInfo userAuthorizationInfo = authorizationService.getUserInfoByUsername(username);
        if (userAuthorizationInfo == null) {
            throw new PermissionException(MessageCode.WRONG_EMAIL);
        }

        if (userAuthorizationInfo.getPassword() == null || !authorizationService.checkPassword(userAuthorizationInfo.getUserPrivateInfo(), password)) {
            throw new PermissionException(MessageCode.ERROR_AUTHENTICATION_CREDENTIALS_BAD);
        }

        EnumSet<UserStatus> userStatus = userAuthorizationInfo.getUserStatus();

        if (!(userStatus.contains(UserStatus.ADMIN) || userStatus.contains(UserStatus.SUPER_ADMIN))) {
            verifyPermission(userAuthorizationInfo.getProfile().getProfileId(), appId);
        }

        return userAuthorizationInfo.getProfile();
    }

    /**
     * Verify that profile has permission to access application with specified <code>appId</code>
     * <p>This method throws {@link PermissionException} if profile doesn't have permission to access specified application</p>
     *
     * @param profileId profile identifier
     * @param appId     application identifier
     * @throws PermissionException
     * @throws NotFoundException   if profile with specified <code>profileId</code> was not found
     */
    private void verifyPermission(long profileId, long appId) throws PermissionException, NotFoundException {
        // check if mapping for application exists
        if (appRelatedShares.get(appId) == null) {
            throw new PermissionException();
        }

        Collection<OrderInfo> profileShareOrders = orderService.getBuyerOrdersInfo(profileId, profileId, OrderObjectType.SHARE, null, null, 0, 0);
        // combine all order object ids into single collection
        List<Long> orderObjectIds = new ArrayList<Long>();
        for (OrderInfo orderInfo : profileShareOrders) {
            for (OrderObjectInfo orderObjectInfo : orderInfo.getOrderObjectsInfo()) {
                orderObjectIds.add(orderObjectInfo.getObjectId());
            }
        }

        // check if profile has purchased any application related shares
        if (!CollectionUtils.containsAny(orderObjectIds, appRelatedShares.get(appId))) {
            throw new PermissionException(MessageCode.PERMISSION_VOTE_DENIED);
        }
    }

}

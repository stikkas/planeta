package ru.planeta.widgets.controllers;

/**
 * Contains widgets app urls
 *
 * User: m.shulepov
 */
public class Urls {

    public static final String CAMPAIGN_WIDGET = "/widgets/campaign-widget.html";
    public static final String AFFILIATE_CAMPAIGN_WIDGET = "/widgets/affiliate-campaign-widget.html";
    public static final String AFFILIATE_CAMPAIGN_WIDGET_IMAGE = "/widgets/affiliate-campaign-widget-image";
    public static final String AFFILIATE_CAMPAIGN_WIDGET_IFRAME = "/widgets/affiliate-campaign-widget-iframe";
    public static final String PAYMENT_WIDGET = "/widgets/payment-widget";
    public static final String PAY = "/widgets/pay";
    public static final String RENDER_IMAGE = "/image";

    // digital application
    public static final String AUTH_APP_USER = "/auth-app-user.json";

    public static final String PAYMENT_SUCCESS = "/payment-success";
    public static final String PAYMENT_FAIL = "/payment-fail";
    public static final String WIDGETS_LIST = "/widgets-external.html";
}
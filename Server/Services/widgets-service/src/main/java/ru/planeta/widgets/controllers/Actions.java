package ru.planeta.widgets.controllers;

import ru.planeta.api.web.controllers.IAction;

/**
 * User: m.shulepov
 */
public enum Actions implements IAction {

    CAMPAIGN_WIDGET(Prefixes.CAMPAIGN, "widget"),
    PAYMENT_WIDGET(Prefixes.PAYMENT, "widget"),
    AFFILIATE_CAMPAIGN_WIDGET(Prefixes.AFFILIATE_CAMPAIGN, "default"),
    PAYMENT_SUCCESS(Prefixes.PAYMENT, "widget-success"),
    PAYMENT_FAIL(Prefixes.PAYMENT, "widget-fail"),
    WIDGETS_LIST(Prefixes.CAMPAIGN, "widgets-list");

    private final String viewName;
    private String path;

    private Actions(String text) {
        this.viewName = text;
        this.path = text;
    }

    private Actions(Prefixes prefix, String text) {
        this.path = prefix.getText() + text;
        viewName = text;
    }

    @Override
    public String getPath() {
        return path;
    }

    @Override
    public String getActionName() {
        return viewName;
    }

    @Override
    public String toString() {
        return getPath();
    }

    private static enum Prefixes {

        CAMPAIGN("campaign/"),
        AFFILIATE_CAMPAIGN("campaign/affiliate/"),
        PAYMENT("payment/");

        private String text;

        private Prefixes(String text) {
            this.text = text;
        }

        public String getText() {
            return text;
        }
    }

}






package ru.planeta.digitalproduct.service

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import ru.planeta.dao.commondb.ShortLinkDAO
import ru.planeta.model.common.ShortLink

/**
 *
 * Created by a.savanovich on 28.09.2016.
 */
@Service
class ShortLinkServiceImpl (private val shortLinkDAO: ShortLinkDAO) : ShortLinkService {

    override fun getUrl(alias: String): ShortLink? {
        return shortLinkDAO.selectByAlias(alias)
    }
}

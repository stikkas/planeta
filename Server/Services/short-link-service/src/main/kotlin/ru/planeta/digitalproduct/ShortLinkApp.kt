package ru.planeta.digitalproduct

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.context.annotation.ImportResource

/**
 *
 * Date: 18.07.16<br></br>
 * Time: 09:31
 */
@ImportResource(locations = ["classpath*:/ru/planeta/spring/propertyConfigurer.xml",
    "classpath*:/spring/applicationContext-global.xml",
    "classpath*:/spring/applicationContext-dao.xml"])
@SpringBootApplication(scanBasePackages = ["ru.planeta"])
class ShortLinkApp {

    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            SpringApplication.run(ShortLinkApp::class.java, *args)
        }
    }
}



package ru.planeta.digitalproduct.controllers

import org.apache.commons.lang3.StringUtils
import org.apache.log4j.Logger
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.GetMapping
import ru.planeta.commons.model.OrderShortLinkStat
import ru.planeta.commons.web.IpUtils
import ru.planeta.commons.web.WebUtils
import ru.planeta.dao.trashcan.RefererDAO
import ru.planeta.digitalproduct.service.ShortLinkService
import ru.planeta.model.enums.ProjectType
import ru.planeta.model.stat.RefererStatType
import ru.planeta.model.stat.RequestStat
import java.net.URI
import java.net.URISyntaxException
import java.util.*
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

@Controller
class LinkController (private val shortLinkService: ShortLinkService,
                      private val refererDAO: RefererDAO) {

    @GetMapping("/live-check")
    fun liveCheck(response: HttpServletResponse) = response.sendError(HttpServletResponse.SC_OK)

    @GetMapping("/*", "/**")
    fun router(request: HttpServletRequest, response: HttpServletResponse) {
        try {
            var alias = request.requestURI
            log.info(alias)
            if (StringUtils.isEmpty(alias)) {
                response.sendError(HttpServletResponse.SC_BAD_REQUEST)
                return
            }
            if (alias.startsWith("/")) {
                alias = alias.substring(1)
            }
            val redirectUrl = shortLinkService.getUrl(alias)
            if (redirectUrl != null) {
                val stat = RequestStat()
                stat.timeAdded = Date()
                stat.ip = IpUtils.getRemoteAddr(request)
                stat.cid = WebUtils.getGtmCidFromCookie(request)
                stat.referer = WebUtils.getReferer(request)
                stat.projectType = ProjectType.LINK
                stat.type = RefererStatType.LINK
                stat.externalId = redirectUrl.shortLinkId // миграцию нужно что-ли сделать на новый параметр
                stat.shortLinkId = redirectUrl.shortLinkId

                refererDAO.insert(stat)

                try {
                    /*if (StringUtils.isNotEmpty(redirectUrl.cookieName) && StringUtils.isNotEmpty(redirectUrl.cookieValue)) {
                        CookieUtils.setCookie(response, redirectUrl.cookieName, redirectUrl.cookieValue, cookieDomain)
                    }*/

                    redirectUrl.redirectAddressUrl = appendUri(redirectUrl.redirectAddressUrl,
                            OrderShortLinkStat.SHORTLINK_ID_COOKIE_NAME + "=" + redirectUrl.shortLinkId.toString()).toString()
                    redirectUrl.redirectAddressUrl = appendUri(redirectUrl.redirectAddressUrl,
                            OrderShortLinkStat.SHORTLINK_REFERRER_COOKIE_NAME + "=" + WebUtils.getReferer(request)).toString()
                } catch (ex: Exception) {
                    log.error(ex)
                }


                response.sendRedirect(redirectUrl.redirectAddressUrl)
            } else {
                response.sendError(HttpServletResponse.SC_NOT_FOUND)
            }
        } catch (ex: Exception) {
            log.error(ex)
            response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR)
        }

    }

    companion object {
        private val log = Logger.getLogger(LinkController::class.java)
    }

    @Throws(URISyntaxException::class)
    private fun appendUri(uri: String, appendQuery: String): URI {
        val oldUri = URI(uri)

        var newQuery: String? = oldUri.query
        if (newQuery == null) {
            newQuery = appendQuery
        } else {
            newQuery += "&$appendQuery"
        }

        return URI(oldUri.scheme, oldUri.authority,
                oldUri.path, newQuery, oldUri.fragment)
    }

}








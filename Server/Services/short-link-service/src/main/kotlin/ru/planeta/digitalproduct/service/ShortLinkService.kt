package ru.planeta.digitalproduct.service

import ru.planeta.model.common.ShortLink

/**
 * Created by a.savanovich on 28.09.2016.
 */
interface ShortLinkService {
    fun getUrl(alias: String): ShortLink?
}

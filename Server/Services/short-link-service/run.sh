#!/bin/sh

cd /opt/java/planeta_stat_util

export LANG=en_US.UTF-8
touch /var/run/planeta/planeta_stat_util.lock

/usr/lib/jvm/jdk1.8.0_91/bin/java -Duser.timezone=Etc/GMT-4 \
     -Dservername=.dev.planeta.ru -jar statistics-service-1.0-SNAPSHOT.jar --server.port=8095 --spring.profiles.active=dev

#!/bin/sh

cd /opt/java/digital_product_service

export LANG=en_US.UTF-8
touch /var/run/planeta/digital_product_service.lock

/usr/lib/jvm/jdk1.8.0_91/bin/java -Duser.timezone=Etc/GMT-4 \
     -Dservername=.dev.planeta.ru -jar digital-product-service-1.0-SNAPSHOT.jar --server.port=8295 --spring.profiles.active=dev

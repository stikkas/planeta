package ru.planeta.digitalproduct.controllers;

import org.apache.commons.collections4.IterableUtils;
import org.apache.commons.collections4.Predicate;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.ibatis.annotations.Param;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import ru.planeta.api.exceptions.NotFoundException;
import ru.planeta.api.exceptions.PermissionException;
import ru.planeta.api.service.billing.order.OrderService;
import ru.planeta.api.service.shop.ProductUsersService;
import ru.planeta.api.web.authentication.AuthHelper;
import ru.planeta.api.web.controllers.services.BaseControllerService;
import ru.planeta.commons.lang.DateUtils;
import ru.planeta.commons.web.WebUtils;
import ru.planeta.model.common.Order;
import ru.planeta.model.common.OrderObject;
import ru.planeta.model.shop.Product;
import ru.planeta.model.shop.enums.PaymentStatus;

import javax.servlet.http.HttpServletResponse;
import java.util.Map;

@Controller
public class DigitalProductController {
    // url for x-accel-redirect
    @Value("${private.cdn.url}")
    private String privateResourceHost;

    // url for http HEAD check
    @Value("${public.cdn.url}")
    private String publicResourceHost;

    @Autowired
    OrderService orderService;

    @Autowired
    ProductUsersService productUsersService;

    @Autowired
    BaseControllerService baseControllerService;

    private static final Logger log = Logger.getLogger(DigitalProductController.class);

    @RequestMapping(value = "/profile/download/{orderObjectId}", method = RequestMethod.GET)
    public void downloadDigitalProduct(@PathVariable("orderObjectId") Long orderObjectId, @Param("downloadUrl") final String downloadUrl, HttpServletResponse response) {
        log.info("download product "+ orderObjectId);

        try {
            if (StringUtils.isEmpty(downloadUrl)) {
                throw new NotFoundException("No downloadUrl present");
            }

            OrderObject oo = orderService.getOrderObject(orderObjectId);
            if (oo == null) {
                throw new NotFoundException(OrderObject.class, orderObjectId);
            }
            Order order = orderService.getOrderSafe(AuthHelper.myProfileId(), oo.getOrderId());
            if (order.getPaymentStatus() != PaymentStatus.COMPLETED) {
                throw new NotFoundException("Order not paid " + oo.getOrderId());
            }

            Product product = productUsersService.getProductCurrent(order.getBuyerId(), oo.getObjectId());

            if (DateUtils.isAfterNow(product.getStartSaleDate())) {
                throw new NotFoundException("Product not ready yet");
            }

            if (product.getContentUrls() == null || product.getContentUrls().isEmpty()) {
                throw new NotFoundException("Product has no digital content");
            }

            String contentUrl = IterableUtils.find(product.getContentUrls(), new Predicate<String>() {
                @Override
                public boolean evaluate(String object) {
                    return downloadUrl.equals(object);
                }
            });

            if (StringUtils.isEmpty(contentUrl)) {
                throw new NotFoundException("Product has no content at " + downloadUrl);
            }

            String fileName = FilenameUtils.getName(contentUrl);
            if (!fileExists(contentUrl)) {
                throw new NotFoundException("File " + contentUrl + " not exists at cdn host.");
            }

            String privateUrl = privateResourceHost + contentUrl;
            log.info(privateUrl);
            response.setHeader("Content-disposition", "attachment; filename=\"" + fileName + "\"");
            response.setHeader("X-Accel-Redirect", privateUrl);
            response.setStatus(HttpServletResponse.SC_OK);
        } catch (PermissionException ex) {
            log.error(ex);
            response.setStatus(HttpServletResponse.SC_FORBIDDEN);
        } catch (NotFoundException ex) {
            log.error(ex);
            response.setStatus(HttpServletResponse.SC_NOT_FOUND);
        }
    }

    private boolean fileExists(String contentUrl) {
        if (StringUtils.isEmpty(contentUrl)) {
            return false;
        }
        Map<String, String> headers = WebUtils.headRequest(publicResourceHost + contentUrl, 500, 500);
        return headers != null;
    }
}

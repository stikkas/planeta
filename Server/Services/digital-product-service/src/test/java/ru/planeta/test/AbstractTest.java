package ru.planeta.test;

import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * @author: ds.kolyshev
 * Date: 29.01.13
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:spring/applicationContext-*.xml", "classpath*:/spring/applicationContext-dao.xml", "classpath*:/spring/applicationContext-geo.xml",})
public abstract class AbstractTest {
	@Autowired
	protected ApplicationContext context;
}

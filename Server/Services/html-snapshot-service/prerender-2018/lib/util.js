const url = require('url');
const crypto = require('crypto');
const fs = require('fs');
const mkdirp = require('mkdirp');
const util = exports = module.exports = {};

util.htmlFilesStoragePath = '/usr/share/prerender/html/';
util.sizeRegexp = /\d+X\d+/;
util.pdfRenderParam = 'renderAsPdf';
util.pdfRenderFlag = util.pdfRenderParam + '=1';
util.isImageParam =  'enderAsPng';
util.isImageFlag =  util.isImageParam + '=1';
util.isWidgetFlag = '/widgets/affiliate-campaign-widget';
util.isRenewHtmlFileParam = 'renewHtml';
util.isRenewHtmlFileFlag = util.isRenewHtmlFileParam + '=1';

// Normalizes unimportant differences in URLs - e.g. ensures
// http://google.com/ and http://google.com normalize to the same string
util.normalizeUrl = function(u) {
	return url.format(url.parse(u, true));
};

/**
 *
 * @param md5
 */
util.convertMd5ToPathSplitByTwoSymbols = function (md5) {
    let splitMd5 = md5.match(/.{1,2}/g);
    let resultPath = '';
    splitMd5.forEach(function(element) {
        resultPath += element + '/';
    });
    return resultPath;
};

util.savePageToHtml = function (url, content) {
    let requestedUrlMd5 = crypto.createHash('md5').update(util.normalizeUrl(util.getUrl(url))).digest('hex');
    let innerPathMd5 = util.convertMd5ToPathSplitByTwoSymbols(requestedUrlMd5);
    let pathToFile = util.htmlFilesStoragePath + innerPathMd5 + requestedUrlMd5 + '.html';

    let isDirectoryExists = fs.existsSync(util.htmlFilesStoragePath + innerPathMd5);
    if (!isDirectoryExists) {
        mkdirp(util.htmlFilesStoragePath + innerPathMd5, function (err) {
            if (err) {
                util.log(err);
            } else {
                util.log('savePageToHtmlFile');
                fs.writeFileSync(pathToFile, content, 'utf-8');
            }
        });
    } else {
        let isFileExists = fs.existsSync(pathToFile);

        if (!isFileExists) {
            util.log('savePageToHtmlFile');
            fs.writeFileSync(pathToFile, content, 'utf-8');
        }
    }
};

util.loadPageFromHtml = function (url) {
    let requestedUrlMd5 = crypto.createHash('md5').update(util.normalizeUrl(util.getUrl(url))).digest('hex');
    let innerPathMd5 = util.convertMd5ToPathSplitByTwoSymbols(requestedUrlMd5);
    let pathToFile = util.htmlFilesStoragePath + innerPathMd5 + requestedUrlMd5 + '.html';
    return pathToFile;
};

util.removeURLParameter = function(url, parameter) {
    //prefer to use l.search if you have a location/link object
    var urlparts= url.split('?');
    if (urlparts.length>=2) {

        var prefix= encodeURIComponent(parameter)+'=';
        var pars= urlparts[1].split(/[&;]/g);

        //reverse iteration as may be destructive
        for (var i= pars.length; i-- > 0;) {
            //idiom for string.startsWith
            if (pars[i].lastIndexOf(prefix, 0) !== -1) {
                pars.splice(i, 1);
            }
        }

        url= urlparts[0] + (pars.length > 0 ? '?' + pars.join('&') : "");
        return url;
    } else {
        return url;
    }
};

util.getOptions = function(req) {

	var requestedUrl = req.url;

    let optionsObj = {};
    if(req.method === 'GET') {
        optionsObj = url.parse(requestedUrl, true).query;
    } else if (req.method === 'POST') {
        optionsObj = req.body;
    }

	//new API starts with render so we'll parse the URL differently if found
	if(requestedUrl.indexOf('/render') === 0) {
		return {
			url: util.getUrl(optionsObj.url),
			renderType: optionsObj.renderType || 'html',
			userAgent: optionsObj.userAgent,
			fullpage: optionsObj.fullpage || false,
			width: optionsObj.width ? +optionsObj.width : optionsObj.width,
			height: optionsObj.height ? +optionsObj.height : optionsObj.height,
			followRedirects: optionsObj.followRedirects,
			javascript: optionsObj.javascript
		}

	} else {
	    let returnObject = {
            url: util.getUrl(requestedUrl),
            renderType: 'html'
        };

        let cleanedUrl = returnObject.url;
        if (cleanedUrl) {
            if (cleanedUrl.indexOf(util.pdfRenderFlag) > 0) {
                cleanedUrl = util.removeURLParameter(cleanedUrl, util.pdfRenderParam);

                returnObject.url = cleanedUrl;
                returnObject.renderType = 'pdf';
            } else {
                let isWidget = cleanedUrl.indexOf(util.isWidgetFlag) > 0;
                let isImage =  cleanedUrl.indexOf(util.isImageFlag) > 0;

                if (isWidget || isImage) {
                    returnObject.renderType = 'png';
                    if (isWidget) {
                        let size = util.getSize(cleanedUrl);
                        if (size) {
                            if (size.width) {
                                returnObject.width = size.width;
                            }
                            if (size.height) {
                                returnObject.height = size.height;
                            }
                        }
                    }
                    if (isImage) {
                        cleanedUrl = util.removeURLParameter(cleanedUrl, util.isImageParam);
                        returnObject.url = cleanedUrl;

                        if (optionsObj && optionsObj.width) {
                            returnObject.width = +optionsObj.width;
                        }
                        if (optionsObj && optionsObj.height) {
                            returnObject.height = +optionsObj.height;
                        }
                    }
                }
            }

        }
        util.log('cleanedUrl: ' + cleanedUrl);
        util.log('returnObject.url: ' + returnObject.url);
		return returnObject;
	}
};

util.getSize = function (url) {
    let name = util.getParam(url, 'name');
    if (name) {
        let sizeStr = name.match(util.sizeRegexp)[0];
        if (sizeStr) {
            let sizeArr = sizeStr.split('X');
            return {
                width: parseInt(sizeArr[0], 10),
                height: parseInt(sizeArr[1], 10)
            };
        }
    }
};

util.getParam = function (url, paramName) {
    let m = url.match(new RegExp('[?&]' + paramName + '=([^&#]*)'));
    if (m) {
        return m[1];
    }
};

// Gets the URL to prerender from a request, stripping out unnecessary parts
util.getUrl = function(requestedUrl) {
	var decodedUrl, realUrl = requestedUrl,
		parts;

	if (!requestedUrl) {
		return '';
	}

	realUrl = realUrl.replace(/^\//, '');

	try {
		decodedUrl = decodeURIComponent(realUrl);
	} catch (e) {
		decodedUrl = realUrl;
	}

	//encode a # for a non #! URL so that we access it correctly
	decodedUrl = this.encodeHash(decodedUrl);

	//if decoded url has two query params from a decoded escaped fragment for hashbang URLs
	if (decodedUrl.indexOf('?') !== decodedUrl.lastIndexOf('?')) {
		decodedUrl = decodedUrl.substr(0, decodedUrl.lastIndexOf('?')) + '&' + decodedUrl.substr(decodedUrl.lastIndexOf('?') + 1);
	}

	parts = url.parse(decodedUrl, true);

	// Remove the _escaped_fragment_ query parameter
	if (parts.query && parts.query['_escaped_fragment_'] !== undefined) {

		if (parts.query['_escaped_fragment_'] && !Array.isArray(parts.query['_escaped_fragment_'])) {
			parts.hash = '#!' + parts.query['_escaped_fragment_'];
		}

		delete parts.query['_escaped_fragment_'];
		delete parts.search;
	}

	// Bing was seen accessing a URL like /?&_escaped_fragment_=
	delete parts.query[''];

	var newUrl = url.format(parts);

	//url.format encodes spaces but not arabic characters. decode it here so we can encode it all correctly later
	try {
		newUrl = decodeURIComponent(newUrl);
	} catch (e) {}

	newUrl = this.encodeHash(newUrl);

	return newUrl;
};

util.encodeHash = function(url) {
	if (url.indexOf('#!') === -1 && url.indexOf('#') >= 0) {
		url = url.replace(/#/g, '%23');
	}

	return url;
};

util.log = function() {
    let getOutOfHere = false;
    let stringArguments = JSON.stringify(arguments);
    if (stringArguments && stringArguments.indexOf('csp-report') >= 0) {
        getOutOfHere = true;
    }

	if (process.env.DISABLE_LOGGING || getOutOfHere) {
		return;
	}

	console.log.apply(console.log, [new Date().toISOString()].concat(Array.prototype.slice.call(arguments, 0)));
};
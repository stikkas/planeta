/*jslint ignore*/
module.exports = {
    pageLoaded: (req, res, next) => {
        if (!req.prerender.content || req.prerender.renderType != 'html') {
            return next();
        }

        let replacements = {
            scripts: [/<script(?:(?!\s+?type="application\/ld\+json").*?)>(?:[\S\s]*?)<\/script>/ig, ''],
            noscripts: [/<noscript(?:.*?)>(?:.*?)<\/noscript>/ig, ''],
            comments: [/<!--(?:.*?)-->/ig, ''],
            whiteSpace: [/\n(?:\s+)\n/ig, '\n'],
            canonicalLinks: [/<link rel="canonical" href="">/ig, ''],
            metaFragment: [/<meta name="fragment" content="!">/ig, ''],
            emptyHrefs: [/href="#?"/ig, 'href="javascript:void(0)"'],
            lazyLoad1: [/(<img [^>]*)src(=["'].*?["'])(.*?)data-original(=["'].*?["'])([^>]*>)/ig,"$1$3src$4$5"],
            lazyLoad2: [/(<img [^>]*)data-original(=["'].*?["'])(.*?)src(=["'].*?["'])([^>]*>)/ig,"$1$3src$2$5"],
        };

        for(let key in replacements) {
            if (!replacements.hasOwnProperty(key) ) {
                return;
            }
            let regexp = replacements[key][0];
            let replacement = replacements[key][1];

            let matches = req.prerender.content.toString().match(regexp);
            for (let i = 0; matches && i < matches.length; i++) {
                if (matches[i].indexOf('application/ld+json') === -1) {
                    req.prerender.content = req.prerender.content.toString().replace(matches[i], replacement);
                }
            }
        }

        next();
    }
};

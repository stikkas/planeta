const util = require('../util.js');

module.exports = {
    pageLoaded: (req, res, next) => {
        if (!req.prerender.content || req.prerender.renderType != 'html') {
            return next();
        }

        let requestedUrl = req.url;
        util.savePageToHtml(requestedUrl, req.prerender.content);
        next();
    }
};
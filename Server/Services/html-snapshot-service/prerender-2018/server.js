#!/usr/bin/env node
var prerender = require('./lib');

var server = prerender({
  chromeFlags: ['--no-sandbox', '--headless', '--disable-gpu', '--remote-debugging-port=9222', '--hide-scrollbars']
});

server.use(prerender.sendPrerenderHeader());
server.use(prerender.blockResources());
server.use(prerender.removeScriptTagsPatched());
server.use(prerender.httpHeaders());
server.use(prerender.removeLinkAway());
server.use(prerender.savePageToHtmlFile());

server.start();
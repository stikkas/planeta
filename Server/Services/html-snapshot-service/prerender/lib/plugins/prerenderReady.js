var _ = require('lodash');

module.exports = {
    onPhantomPageCreate: function (phantom, req, res, next) {
        var page = req && req.prerender && req.prerender.page;
        if (!page) {
            next();
            return;
        }

        var url = req.prerender.url;
        var isWidget = url && url.indexOf("/widgets/affiliate-campaign-widget") > 0;

        if (!isWidget) {
            page.set('onLoadFinished', function () {
                //если прерход на 404 происходить внутри js'a то phantom при открытии сраницы (req.prerender.page.open) возвращает fail
                page.evaluate(function () {
                    return window.location.href;
                }, function (currentUrl) {
                    if (currentUrl.indexOf('/404.html') > 0) {
                        req.prerender.stage = 2;
                        res.send(404);
                    }
                });
            });
            page.set('onInitialized', _.bind(this.onPageInitialize, this, page));
            page.get('settings.loadImages', function () {
                page.set('settings.loadImages', false);
                next();
            });
        } else {
            next();
        }

    },

    onPageInitialize: function (page) {
        if (page.isAlreadyRunning) {
            return;
        }
        page.isAlreadyRunning = true;
        page.evaluate(function () {
            function getAppView() {
                return window.workspace && window.workspace.appView;
            }

            function isReady() {
                if (!window.workspace || !workspace.navigationState || !workspace.appView || !workspace.getNavigationMapItem || !workspace.appModel || !workspace.appModel.attributes) {
                    return true;
                }
                var navItem = workspace.getNavigationMapItem(workspace.appModel.attributes.profileModel);
                if (navItem && (navItem.sidebarViewType || navItem.headerViewType || navItem.menuViewType || navItem.contentViewType || navItem.rightSidebarViewType)) {
                    return false;
                }
                return true;
            }

            function calcPrerenderReady() {
                if (isReady()) {
                    window.prerenderReady = true;
                    return;
                }
                function findFirstNonRenderView(param) {
                    var views = _.isArray(param) ? param : param._childViews;
                    return _(views).find(function (view) {
                        if (view.renderDfd && !view.renderDfd.isRejected() && !view.renderDfd.isResolved()) {
                            return view;
                        }
                        return findFirstNonRenderView(view);
                    });
                }

                if (this.prerenderReadyTimeout) {
                    clearTimeout(this.prerenderReadyTimeout);
                }
                this.prerenderReadyTimeout = setTimeout(function () {
                    var appView = getAppView();
                    var nonRenderedView = findFirstNonRenderView(appView);
                    if (nonRenderedView) {
                        nonRenderedView.renderDfd.always(function () {
                            calcPrerenderReady();
                        });
                    } else if (_.size(appView._childViews) === 0) {
                        setTimeout(calcPrerenderReady, 500);
                    } else {
                        window.prerenderReady = true;
                    }
                }, 10);
            }


            function calcPrerenderReadyTimeout() {
                setTimeout(function () {
                    calcPrerenderReady();
                }, 10);
            }


            window.prerenderReady = false;
            window.noLazyLoadImage = true;
            if (window.jQuery) {
                jQuery(document).ready(calcPrerenderReadyTimeout);
            } else {
                document.addEventListener("DOMContentLoaded", calcPrerenderReadyTimeout, false);
            }

        });
    }
};

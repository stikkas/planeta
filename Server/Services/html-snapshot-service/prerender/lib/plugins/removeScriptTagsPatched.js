/*jslint ignore*/

module.exports = {
    beforeSend: function(req, res, next) {
        if (!req.prerender.documentHTML) {
            return next();
        }

        var replacements = {

            scripts: [/<script(?:(?!\s+?type="application\/ld\+json").*?)>(?:[\S\s]*?)<\/script>/ig, ''],
            noscripts: [/<noscript(?:.*?)>(?:.*?)<\/noscript>/ig, ''],
            comments: [/<!--(?:.*?)-->/ig, ''],
            whiteSpace: [/\n(?:\s+)\n/ig, '\n'],
            canonicalLinks: [/<link rel="canonical" href="">/ig, ''],
            metaFragment: [/<meta name="fragment" content="!">/ig, ''],
            emptyHrefs: [/href="#?"/ig, 'href="javascript:void(0)"'],
            lazyLoad1: [/(<img [^>]*)src(=["'].*?["'])(.*?)data-original(=["'].*?["'])([^>]*>)/ig,"$1$3src$4$5"],
            lazyLoad2: [/(<img [^>]*)data-original(=["'].*?["'])(.*?)src(=["'].*?["'])([^>]*>)/ig,"$1$3src$2$5"],
        };

        for(var key in replacements) {
            if (!replacements.hasOwnProperty(key) ) {
                return;
            }
            var regexp = replacements[key][0];
            var replacement = replacements[key][1];

            req.prerender.documentHTML = req.prerender.documentHTML.toString().replace(regexp, replacement);

        }
        next();
    }
};

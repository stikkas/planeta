//если прерход на 404 происходить внутри js'a то phantom при открытии сраницы (req.prerender.page.open) возвращает fail
//и пререндер отдает 502

module.exports = {
    onPhantomPageCreate: function (phantom, req, res, next) {
        var page = req && req.prerender && req.prerender.page;
        if (page) {
            var url = req.prerender.url;
            var isWidget = url && ((url.indexOf("/widgets/affiliate-campaign-widget") > 0) || (url.indexOf("renderAsPng=1") > 0));

            if (!isWidget) {
                page.set('onLoadFinished', function () {
                    if (req.prerender.stage < 2) {
                        page.evaluate(function () {
                            return window.location.href;
                        }, function (currentUrl) {
                            if (currentUrl.indexOf('/404.html') > 0) {
                                req.prerender.stage = 2;
                                res.send(404);
                            }
                        });
                    }
                });
            }
        }

        next();
    }
};
module.exports = {


    onPhantomPageCreate: function (phantom, req, res, next) {
        var page = req && req.prerender && req.prerender.page;
        if (!page) {
            next();
            return;
        }

        var self = this;
        var url = req.prerender.url;
        var isWidget = url && url.indexOf("/widgets/affiliate-campaign-widget") > 0;
        var isImage =  url && url.indexOf("renderAsPng=1") > 0;

        req.isWidget = isWidget || isImage;
        page.get('settings.loadImages', function (loadImage) {
            if (isWidget) {
                var size = self.getSize(url);
                if (size) {
                    page.set('viewportSize', size);
                    page.set('settings.loadImages', true);
                }
            } else if(isImage) {
                page.set('settings.loadImages', true);
            }
            next();
        });
    },

    sizeRegexp: /\d+X\d+/,

    getSize: function (url) {
        var name = this.getParam(url, 'name');
        if (name) {
            var sizeStr = name.match(this.sizeRegexp)[0];
            if (sizeStr) {
                var sizeArr = sizeStr.split('X');
                return {
                    width: parseInt(sizeArr[0], 10),
                    height: parseInt(sizeArr[1], 10)
                };
            }
        }
    },

    getParam: function (url, paramName) {
        var m = url.match(new RegExp('[?&]' + paramName + '=([^&#]*)'));
        if (m) {
            return m[1];
        }
    },

    afterPhantomRequest: function (req, res, next) {
        var page = req && req.prerender && req.prerender.page;
        if (!page || !req.isWidget) {
            next();
            return;
        }

        page.renderBase64('PNG', function (img) {
            req.prerender.documentImage = new Buffer(img, 'base64');
            next();
        });

    }
};
module.exports = {
    awayLinkRegExp: new RegExp('href="/api/util/away.html\\?to=([^"]*)"', 'g'),
    beforeSend: function (req, res, next) {
        if (req.prerender.documentHTML) {
            req.prerender.documentHTML = req.prerender.documentHTML.replace(this.awayLinkRegExp, function (a, awayLink) {
                return 'href="' + decodeURIComponent(awayLink) + '"';
            });
        }
        next();
    }
};
fs = require('fs');


module.exports = {

//    path: "d:/temp/prerender/",
    path: "/var/data/",

    getA: function (fileName, req, res) {
        return '<a href="' + fileName + '">' + req.url + '</a> '
        + " " + req.prerender.documentHTML.length
        + " " + (new Date().getTime() - req.prerender.start.getTime()) + 'ms'
        + "  (" + req.headers['user-agent'] + ") "
        + "  " + req.headers['referer']
        + "<br/>";
    },
    beforeSend: function (req, res, next) {
        try {
            var self = this;

            function fmt(d) {
                return d.getFullYear()
                + "-" + (d.getMonth() + 1 > 9 ? "" : 0) + (d.getMonth() + 1)
                + "-" + (d.getDate() > 9 ? '' : '0') + d.getDate()
                + "-" + (d.getHours() > 9 ? '' : '0') + d.getHours()
                + "-" + (d.getMinutes() > 9 ? '' : '0') + d.getMinutes()
                + "-" + (d.getSeconds() > 9 ? '' : '0') + d.getSeconds()
                + "-" + d.getMilliseconds();
            }

            function now() {
                return fmt(new Date());
            }

            var dirName = now().substring(0, 10) + '/';
            var fullDirName = this.path + dirName;
            if (!fs.existsSync(fullDirName)) {
                fs.mkdirSync(fullDirName);
                fs.appendFile(this.path + "index.html", '<a href="' + dirName + 'index.html">' + dirName + '</a></br>', function (err) {
                    if (err) {
                        console.log('cannot append to file ' + self.path + 'index.html', err);
                    }
                });
            }

            var fileName = now() + ".html";

            fs.appendFile(fullDirName + "index.html", this.getA(fileName, req, res), function (err) {
                if (err) {
                    console.log('cannot append to file ' + fullDirName + 'index.html', err);
                }
            });
            fs.writeFile(fullDirName + fileName, req.prerender.documentHTML);
        } catch (e) {
        }
        next();
    }
};
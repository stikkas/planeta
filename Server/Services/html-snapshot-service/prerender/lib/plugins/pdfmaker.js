var util = require('../util.js');

module.exports = {


    onPhantomPageCreate: function (phantom, req, res, next) {
        var page = req && req.prerender && req.prerender.page;
        if (!page) {
            next();
            return;
        }
        var pdfRenderFlag = "renderAsPdf=1"; // may be & or ? infront
        var url = req.prerender.url;
        var isPdf = url && url.indexOf(pdfRenderFlag) > 0;
        req.isPdf = isPdf;

        if (isPdf) {
            var index = url.indexOf("pdfName=")+8;
            var isLandscape = url.indexOf("landScape=1") > 0;
            var lastIndex = url.indexOf(".pdf", index) + 4;
            var pdfName = url.substring(index,  lastIndex);
            req.pdfName = pdfName;
            req.prerender.url = req.prerender.url.substring(0, url.indexOf(pdfRenderFlag) - 1);
            util.log(req.prerender.url);
            page.set('paperSize', {  format: "A4",  orientation: isLandscape ? "landscape" : "portrait", margin: '1cm' });
            page.set('settings.loadImages', true);
        }
        next();
    },

    afterPhantomRequest: function (req, res, next) {
        var page = req && req.prerender && req.prerender.page;
        if (!page || !req.isPdf) {
            next();
            return;
        }
        util.log("rendered url", req.prerender.url);
        var name = '/tmp/invoice/' + req.pdfName;
        req.prerender.documentPdf = name;
        util.log('starting render with name', name);
        page.render(name, function() {
            next();
        });

    }
};
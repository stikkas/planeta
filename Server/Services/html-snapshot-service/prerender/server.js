#!/usr/bin/env node
var prerender = require('./lib');

var server = prerender({
    workers: process.env.PHANTOM_CLUSTER_NUM_WORKERS || 15,
    iterations: process.env.PHANTOM_WORKER_ITERATIONS || 10,
    phantomBasePort: process.env.PHANTOM_CLUSTER_BASE_PORT || 12300,
    messageTimeout: process.env.PHANTOM_CLUSTER_MESSAGE_TIMEOUT,
    phantomArguments: ["--load-images=false", "--ignore-ssl-errors=true", "--disk-cache=false", "--web-security=false"]
});


server.use(prerender.logger());
server.use(prerender.widgets());
server.use(prerender.pdfmaker());
//server.use(prerender.prerenderReady());
server.use(prerender.check404());
server.use(prerender.blacklist());
server.use(prerender.removeScriptTagsPatched());
server.use(prerender.httpHeaders());
server.use(prerender.removeLinkAway());

server.start();







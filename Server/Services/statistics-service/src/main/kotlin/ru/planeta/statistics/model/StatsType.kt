package ru.planeta.statistics.model

import ru.planeta.model.enums.Codable

/**
 * User: a.savanovich
 * Date: 23.04.2016
 * Time: 2:53
 */
enum class StatsType (private val value: Int) : Codable {
    VIDEO(0), AUDIO_TRACK(1), PHOTO(3);

    override fun getCode(): Int = value

}


package ru.planeta.statistics

import org.apache.ibatis.datasource.unpooled.UnpooledDataSource
import org.apache.ibatis.transaction.jdbc.JdbcTransactionFactory
import org.mybatis.spring.SqlSessionFactoryBean
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.EnableAutoConfiguration
import org.springframework.context.annotation.*
import org.springframework.context.annotation.ComponentScan.Filter
import org.springframework.core.io.ClassPathResource
import org.springframework.core.io.Resource
import org.springframework.remoting.httpinvoker.HttpInvokerProxyFactoryBean
import org.springframework.scheduling.annotation.EnableAsync
import org.springframework.scheduling.annotation.EnableScheduling
import org.springframework.web.servlet.config.annotation.PathMatchConfigurer
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter
import ru.planeta.api.geo.GeoResolverWebService
import ru.planeta.api.log.ErrorAccumulatorFilter

/**
 *
 * User: Serge Blagodatskih<stikkas17></stikkas17>@gmail.com><br></br>
 * Date: 18.07.16<br></br>
 * Time: 09:31
 */
@EnableAsync
@EnableScheduling
@ImportResource(locations = ["classpath*:/ru/planeta/spring/propertyConfigurer.xml",
    "classpath*:/spring/applicationContext-global.xml",
    "classpath*:/spring/applicationContext-dao.xml"])
@Configuration
@EnableAutoConfiguration
@ComponentScan(basePackages = ["ru.planeta"],
        excludeFilters = [Filter(pattern = arrayOf("ru\\.planeta\\.api\\.web\\.controllers\\.(?!LiveCheckController).*"),
                type = FilterType.REGEX)])
class StatisticsApp : WebMvcConfigurerAdapter() {

    @Bean(name = ["errorAccumulatorFilter"], destroyMethod = "destroy")
    fun errorAccumulatorFilter(): ErrorAccumulatorFilter = ErrorAccumulatorFilter()

    @Bean(name = ["commondbTransactionFactory"])
    fun jdbcTransactionFactory(): JdbcTransactionFactory = JdbcTransactionFactory()

    @Bean(name = ["commondbDataSource"])
    fun getUnpooledDataSource(@Value("\${commondb.driver}") driver: String,
                              @Value("\${commondb.url}") url: String,
                              @Value("\${commondb.user}") user: String,
                              @Value("\${commondb.password}") password: String): UnpooledDataSource = UnpooledDataSource(driver, url, user, password)


    @Bean(name = ["commondbSqlSessionFactory"])
    fun getSqlSessionFactoryBean(@Value("\${commondb.driver}") driver: String,
                                 @Value("\${commondb.url}") url: String, @Value("\${commondb.user}") user: String,
                                 @Value("\${commondb.password}") password: String): SqlSessionFactoryBean {
        val factoryBean = SqlSessionFactoryBean()
        factoryBean.setDataSource(getUnpooledDataSource(driver, url, user, password))
        factoryBean.setTransactionFactory(jdbcTransactionFactory())
        factoryBean.setMapperLocations(arrayOf<Resource>(ClassPathResource("/ibatis/commondb/*.xml"),
                ClassPathResource("/ibatis/statdb/*.xml")))
        return factoryBean
    }

    @Bean(name = ["geoResolverWebService"])
    fun getHttpInvokerProxyFactoryBean(@Value("\${geowebservice.service.host}") geoHost: String): HttpInvokerProxyFactoryBean {
        val httpInvokerProxyFactoryBean = HttpInvokerProxyFactoryBean()
        httpInvokerProxyFactoryBean.serviceUrl = "http://$geoHost/geoResolverWebService.service"
        httpInvokerProxyFactoryBean.serviceInterface = GeoResolverWebService::class.java
        return httpInvokerProxyFactoryBean
    }

    override fun configurePathMatch(configurer: PathMatchConfigurer) {
        configurer.isUseSuffixPatternMatch = false
    }

    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            SpringApplication.run(StatisticsApp::class.java, *args)
        }

    }

}


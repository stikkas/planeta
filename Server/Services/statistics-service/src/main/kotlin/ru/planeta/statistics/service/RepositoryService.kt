package ru.planeta.statistics.service

import org.apache.log4j.Logger
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import ru.planeta.dao.profiledb.AudioTrackDAO
import ru.planeta.dao.profiledb.PhotoDAO
import ru.planeta.dao.profiledb.VideoDAO
import ru.planeta.statistics.model.ObjectKey
import ru.planeta.statistics.model.StatsType

/**
 * @author ameshkov
 */
@Service
class RepositoryService (
        private val videoDAO: VideoDAO,
        private val audioTrackDAO: AudioTrackDAO,
        private val photoDAO: PhotoDAO) {

    fun saveStats(objectKey: ObjectKey, objectStat: Int) {
        val statsType = objectKey.statsType
        if (null != statsType)
            when (statsType) {
                StatsType.VIDEO -> saveVideoStats(objectKey, objectStat)
                StatsType.AUDIO_TRACK -> saveAudioTrackStats(objectKey, objectStat)
                StatsType.PHOTO -> savePhotoStats(objectKey, objectStat)
            }
    }


    private fun saveVideoStats(objectKey: ObjectKey, objectStat: Int) {
        debugStartDump("video")
        videoDAO.updateViewsCount(objectKey.ownerId, objectKey.objectId, objectStat)
        LOG.debug("Video stats dumped successfully")
    }


    private fun saveAudioTrackStats(objectKey: ObjectKey, objectStat: Int) {
        debugStartDump("audio")
        audioTrackDAO.updateListeningsCount(objectKey.ownerId, objectKey.objectId, objectStat)
        LOG.debug("Group audio stats dumped successfully")
    }

    private fun savePhotoStats(objectKey: ObjectKey, objectStat: Int) {
        debugStartDump("photo")
        photoDAO.updateViewsCount(objectKey.ownerId, objectKey.objectId, objectStat)
        LOG.debug("Photo stats dumped successfully")
    }

    companion object {
        private val LOG = Logger.getLogger(RepositoryService::class.java)
        private fun debugStartDump(title: String) {
            LOG.debug("Starting dump $title stats")
        }
    }
}


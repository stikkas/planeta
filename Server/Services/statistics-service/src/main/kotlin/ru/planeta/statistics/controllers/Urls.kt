package ru.planeta.statistics.controllers


const val AUDIO_LISTEN = "/audio-listen"
const val PHOTO_VIEW = "/photo-view"
const val VIDEO_VIEW = "/video-view"
const val LIVE_CHECK = "/live-check"
const val STAT_EVENT = "/stat-event"
const val BANNER_CLICK = "/api/public/our-reclame-clicked"
const val BANNER_APPEAR = "/api/public/our-reclame-appear"
const val HAS_BLOCKER = "/api/public/has-reclame-trimmer"
const val NOPROFILE_ID = "/api/public/noprofileId"
const val CHECK_GEO_IP = "/check-ip"

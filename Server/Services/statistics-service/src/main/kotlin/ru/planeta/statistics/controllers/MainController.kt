package ru.planeta.statistics.controllers

import org.apache.commons.lang3.StringUtils
import org.apache.commons.lang3.math.NumberUtils
import org.apache.log4j.Logger
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.HttpHeaders.CONTENT_TYPE
import org.springframework.http.MediaType.IMAGE_JPEG_VALUE
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.ResponseBody
import ru.planeta.api.Utils.nvl
import ru.planeta.api.geo.GeoResolverWebService
import ru.planeta.api.service.profile.ProfileService
import ru.planeta.api.service.security.PermissionService
import ru.planeta.commons.web.CookieUtils
import ru.planeta.commons.web.IpUtils
import ru.planeta.commons.web.WebUtils
import ru.planeta.dao.profiledb.GeoLocationDAO
import ru.planeta.dao.trashcan.AdBlockStatisticsDAO
import ru.planeta.dao.trashcan.BannerStatisticsDAO
import ru.planeta.model.profile.location.City
import ru.planeta.model.profile.location.Country
import ru.planeta.model.stat.StatEventType
import ru.planeta.statistics.service.StatisticsService
import java.math.BigDecimal
import java.util.*
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

/**
 * User: Serge Blagodatskih<stikkas17></stikkas17>@gmail.com><br></br>
 * Date: 18.07.16<br></br>
 * Time: 09:48
 */
@Controller
class MainController(private val geoResolverService: GeoResolverWebService,
                     private val geoLocationDAO: GeoLocationDAO,
                     private val statisticsService: StatisticsService,
                     private val profileService: ProfileService,
                     private val bannerStatisticsDAO: BannerStatisticsDAO,
                     private val adBlockStatisticsDAO: AdBlockStatisticsDAO) {


    @Value("\${enabled.ip.instead.city.and.country.name:true}")
    private var enabledIpInsteadCityAndCountryName: Boolean = false

    @GetMapping(BANNER_CLICK)
    fun bannerClicked(@RequestParam("reclameId") bannerId: Long,
                      @RequestParam(name = PARAM_PROFILE_ID, defaultValue = "-1") profileId: Long,
                      @RequestParam(name = "referer") referer: String,
                      resp: HttpServletResponse) {
        bannerStatisticsDAO.insertClicked(profileId, bannerId, referer)
        resp.setHeader(CONTENT_TYPE, IMAGE_JPEG_VALUE)
        resp.status = HttpServletResponse.SC_OK
    }

    @GetMapping(BANNER_APPEAR)
    fun bannerAppeared(@RequestParam("reclameId") bannerId: Long,
                       @RequestParam(name = PARAM_PROFILE_ID, defaultValue = "-1") profileId: Long,
                       @RequestParam(name = "referer") referer: String,
                       resp: HttpServletResponse) {
        bannerStatisticsDAO.insertAppeared(profileId, bannerId, referer)
        resp.setHeader(CONTENT_TYPE, IMAGE_JPEG_VALUE)
        resp.status = HttpServletResponse.SC_OK
    }

    @GetMapping(HAS_BLOCKER)
    fun hasBlocked(@RequestParam(name = PARAM_PROFILE_ID, defaultValue = "-1") profileId: Long,
                   req: HttpServletRequest, resp: HttpServletResponse) {
        val cid = WebUtils.getGtmCidFromCookie(req)
        val ip = IpUtils.getRemoteAddr(req)
        adBlockStatisticsDAO.hasAdBlock(profileId, cid, ip)
        resp.setHeader(CONTENT_TYPE, IMAGE_JPEG_VALUE)
        resp.status = HttpServletResponse.SC_OK
    }

    @GetMapping(NOPROFILE_ID)
    fun noProfileId(@RequestParam(name = PARAM_PROFILE_ID, defaultValue = "-1") profileId: Long,
                    @RequestParam(name = "from_url", required = false) fromUrl: String,
                    @RequestParam(name = "model", required = false) modelName: String,
                    req: HttpServletRequest, resp: HttpServletResponse) {
        LOG.error("Request without profileId where should be - " + fromUrl + ", profileId = " + profileId
                + ", model = " + modelName)
        resp.setHeader(CONTENT_TYPE, IMAGE_JPEG_VALUE)
        resp.status = HttpServletResponse.SC_OK
    }

    @GetMapping(STAT_EVENT)
    fun statEvent(req: HttpServletRequest, resp: HttpServletResponse) = handleStatEvent(req, resp)

    @GetMapping(LIVE_CHECK)
    @ResponseBody
    fun liveCheck(req: HttpServletRequest, resp: HttpServletResponse): String {
        WebUtils.setDefaultHeaders(resp)
        return "OK"
    }

    @GetMapping(CHECK_GEO_IP)
    @ResponseBody
    fun checkGeoIp(@RequestParam(required = false) ip: String?,
                   req: HttpServletRequest, resp: HttpServletResponse): String {
        val netAddress = if (ip.isNullOrBlank()) IpUtils.getRemoteAddr(req) else ip
        WebUtils.setDefaultHeaders(resp)

        return if (netAddress != null && netAddress.isNotBlank()) {
            val city = geoResolverService.resolveCity(netAddress)

            var result = "ip=" + netAddress
            val country = if (city != null) {
                result += "cityName=" + city.nameEn
                geoLocationDAO.getCountriesByIds(Collections.singletonList(city.countryId)).iterator().next()
            } else
                geoResolverService.resolveCountry(netAddress)
            country?.let { result += "countryName=" + country.countryNameEn }
            result
        } else "No ip"
    }

    @GetMapping(path = [AUDIO_LISTEN, PHOTO_VIEW, VIDEO_VIEW])
    fun doGet(req: HttpServletRequest, resp: HttpServletResponse) {
        val requestUrl = req.requestURI
        LOG.debug("Request " + requestUrl)

        val profileId = NumberUtils.toLong(req.getParameter(PARAM_PROFILE_ID))
        if (profileId == 0L) {
            LOG.error("Invalid value of profileId " + req.getParameter(PARAM_PROFILE_ID))
            LOG.error("Request: " + req)
            return
        }

        val objectId = NumberUtils.toLong(req.getParameter(PARAM_OBJECT_ID))
        if (objectId == 0L) {
            LOG.error("Invalid value of objectId " + req.getParameter(PARAM_OBJECT_ID))
            return
        }

        val albumId = NumberUtils.toLong(req.getParameter(PARAM_CHILD_OBJECT_ID))

        var clientId = NumberUtils.toLong(req.getParameter(PARAM_CLIENT_ID), PermissionService.ANONYMOUS_USER_PROFILE_ID)
        if (clientId == PermissionService.ANONYMOUS_USER_PROFILE_ID) {
            clientId = if (!CookieUtils.hasCookie(req, STATISTIC_SERVICE_COOKIE_NAME)) {
                generateAnonymousClientId()
            } else {
                NumberUtils.createLong(CookieUtils.getCookieValue(req.cookies, STATISTIC_SERVICE_COOKIE_NAME, "0"))
            }
        }

        if (!CookieUtils.hasCookie(req, STATISTIC_SERVICE_COOKIE_NAME)) {
            CookieUtils.setCookie(resp, STATISTIC_SERVICE_COOKIE_NAME, clientId.toString(), CookieUtils.ONE_MONTH, req.serverName)
        }

        LOG.debug("Processing request $requestUrl profileId: $profileId objectId: $objectId")

        when (requestUrl) {
            AUDIO_LISTEN -> statisticsService.trackAudioListen(profileId, objectId, albumId)
            PHOTO_VIEW -> {
                if (albumId == 0L) {
                    LOG.error("Invalid value of childObjectId")
                }
                statisticsService.trackPhotoView(profileId, objectId, albumId)
            }
            VIDEO_VIEW -> statisticsService.trackVideoView(profileId, objectId)
        }

        resp.setHeader(CONTENT_TYPE, IMAGE_JPEG_VALUE)
        resp.status = HttpServletResponse.SC_OK

        LOG.debug("Request $requestUrl processed successfully")
    }

    private fun handleStatEvent(req: HttpServletRequest, resp: HttpServletResponse) {
        try {
            val type = StatEventType.valueOf(req.getParameter("type"))
            val visitorId = NumberUtils.toLong(nvl(req.getParameter("visitor"), CookieUtils.getCookieValue(req.cookies, CookieUtils.VISITOR_COOKIE_NAME, null)))

            val objectId = NumberUtils.toLong(req.getParameter("object"))
            val clientId = NumberUtils.toLong(req.getParameter("clientId"))
            val amountString = req.getParameter("amount")

            val amount = if (amountString == null) {
                BigDecimal.ZERO
            } else {
                BigDecimal(amountString)
            }

            val uri = req.getParameter("uri") ?: ""

            var referer = WebUtils.getHost(req.getParameter("ref"))
            if (StringUtils.isBlank(referer)) {
                referer = CookieUtils.getCookieValue(req.cookies, CookieUtils.EXTERNAL_REFERRER, null)
            }

            val ip = IpUtils.getRemoteAddr(req)

            var cityName: String
            var countryName: String

            var affiliateId: Long = NumberUtils.toLong(req.getParameter("affiliate"))
            var sourceId: Long = NumberUtils.toLong(req.getParameter("source"))

            if (referer != null && referer.contains("localhost")) {
                referer = ""
            }

            val cityAndCountryNames = getCityAndCountryName(clientId, ip)
            cityName = cityAndCountryNames.cityName
            countryName = cityAndCountryNames.countryName
            statisticsService.trackEvent(type, visitorId, affiliateId, sourceId, objectId, uri, referer, cityName, countryName, amount)
        } catch (e: Exception) {
            LOG.error("error while tracking stat event", e)
        }

        resp.setHeader(CONTENT_TYPE, IMAGE_JPEG_VALUE)
        resp.status = HttpServletResponse.SC_OK
    }

    private fun getCityAndCountryName(clientId: Long, ip: String): CityAndCountryNames {
        var cityName: String? = null
        var countryName: String? = null
        var city: City? = null
        var country: Country? = null
        if (StringUtils.isNotBlank(ip)) {
            city = geoResolverService.resolveCity(ip)
            if (city == null) {
                LOG.warn("Not recognise city ip " + ip)
                country = geoResolverService.resolveCountry(ip)
            } else {
                country = geoLocationDAO.getCountriesByIds(Collections.singletonList(city.countryId)).iterator().next()
                LOG.debug("Recognise ip " + ip)
            }
            if (country == null) {
                LOG.warn("Not recognise country for ip " + ip)
            }
        } else {
            LOG.error("Blank ip")
        }
        if (city != null) {
            cityName = city.name
        }
        if (country != null) {
            countryName = country.name
        }

        //если гео резольвер не определил страну и город, пробуем взять из профиля
        if (city == null && country == null && clientId > 0) {
            val profile = profileService.getProfile(clientId)
            if (profile != null) {
                cityName = profile.cityName
                countryName = profile.countryName
            }
        }

        if (cityName == null && countryName == null && clientId > 0 && enabledIpInsteadCityAndCountryName) {
            cityName = ip
            countryName = ip
        }
        return CityAndCountryNames(cityName ?: "", countryName ?: "")

    }

    private class CityAndCountryNames(val cityName: String, val countryName: String)

    companion object {

        const val STATISTIC_SERVICE_COOKIE_NAME = "statserviceuuid"
        private val LOG = Logger.getLogger(MainController::class.java)

        const val PARAM_PROFILE_ID = "profileId"
        const val PARAM_OBJECT_ID = "objectId"
        const val PARAM_CHILD_OBJECT_ID = "childObjectId"
        const val PARAM_CLIENT_ID = "clientId"

        private fun generateAnonymousClientId(): Long {
            val basePart = 100000000000L // to prevent crossing with profileIds
            val timestampPart = System.nanoTime() * 1000000 // statistically increasing clientIds (here nanoTime is supposed positive!!!)
            return basePart + timestampPart + UUID.randomUUID().hashCode() // to differ users with same timestamp
        }
    }
}






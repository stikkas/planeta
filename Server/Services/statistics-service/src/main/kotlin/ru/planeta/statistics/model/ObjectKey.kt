package ru.planeta.statistics.model

/**
 * Represents key for any object in our system.
 * OwnerId is either group or user identifier.
 * ObjectId is object's identifier (object is photo, video or audio)
 *
 * @author ameshkov
 */
data class ObjectKey(val ownerId: Long, val objectId: Long, val statsType: StatsType?)

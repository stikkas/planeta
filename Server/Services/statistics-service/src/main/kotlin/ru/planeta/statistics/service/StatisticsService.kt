package ru.planeta.statistics.service

import org.apache.commons.lang3.StringUtils
import org.apache.log4j.Logger
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Service
import ru.planeta.api.Utils.nvl
import ru.planeta.dao.statdb.StatEventDAO
import ru.planeta.model.stat.StatEvent
import ru.planeta.model.stat.StatEventType
import ru.planeta.statistics.model.ObjectKey
import ru.planeta.statistics.model.StatsType
import java.math.BigDecimal
import java.util.*

/**
 * @author ameshkov
 */
@Service
class StatisticsService(private val repositoryService: RepositoryService,
                        private val statEventDAO: StatEventDAO) {
    private val log: Logger = Logger.getLogger(StatisticsService::class.java)
    private val lock = Object()

    private var statsArray: MutableMap<ObjectKey, Int> = HashMap()


    fun trackVideoView(profileId: Long, videoId: Long) {
        val objectKey = ObjectKey(profileId, videoId, StatsType.VIDEO)
        incrementStats(statsArray, objectKey, 1)
        log.debug("Video view saved")
    }

    fun trackAudioListen(profileId: Long, trackId: Long, albumId: Long) {
        val objectKey = ObjectKey(profileId, trackId, StatsType.AUDIO_TRACK)
        incrementStats(statsArray, objectKey, 1)
        log.debug("Audio listen saved")
    }

    fun trackPhotoView(profileId: Long, photoId: Long, albumId: Long) {
        val objectKey = ObjectKey(profileId, photoId, StatsType.PHOTO)
        incrementStats(statsArray, objectKey, 1)
        log.debug("Photo view saved")
    }

    fun trackEvent(type: StatEventType, visitorId: Long, affiliateId: Long, sourceId: Long, objectId: Long, uri: String, referer: String?, city: String, country: String, amount: BigDecimal) {
        val event = StatEvent()
        event.type = type
        event.visitorId = visitorId
        event.affiliateId = affiliateId
        event.sourceId = sourceId
        event.objectId = objectId
        event.uri = uri
        event.referer = nvl(referer, StringUtils.EMPTY)
        event.city = nvl(city, StringUtils.EMPTY)
        event.country = nvl(country, StringUtils.EMPTY)
        event.amount = amount
        event.affiliateAmount = BigDecimal.ZERO
        statEventDAO.insert(event)
    }

    fun incrementStats(statsMap: MutableMap<ObjectKey, Int>, objectKey: ObjectKey, viewsCount: Int) {
        synchronized(lock, {
            val objectStat = statsMap[objectKey]
            if (objectStat == null) {
                statsMap[objectKey] = viewsCount
            } else {
                statsMap[objectKey] = objectStat + viewsCount
            }
        })
    }

    @Scheduled(fixedDelay = DUMP_DELAY)
    fun dumpStat() {

        log.debug("Starting dump statistics...")

        var statsArrayDump: MutableMap<ObjectKey, Int>

        statsArrayDump = synchronized(lock, {
            // rotate stats
            statsArrayDump = statsArray
            statsArray = HashMap()
            statsArrayDump
        })

        statsArrayDump.forEach({ repositoryService.saveStats(it.key, it.value) })
        log.debug("Dump statistics finished successfully")
    }

    companion object {

        const val DUMP_DELAY = 1000L * 60
    }

}

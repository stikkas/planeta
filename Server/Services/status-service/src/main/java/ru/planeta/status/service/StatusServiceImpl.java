package ru.planeta.status.service;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import ru.planeta.api.service.common.UserLastOnlineTimeService;
import ru.planeta.model.common.UserLastOnlineTime;

import javax.annotation.Nonnull;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Class StatusServiceImpl
 *
 * @author a.tropnikov
 */
@Service
public class StatusServiceImpl implements StatusService {

    private final UserLastOnlineTimeService userLastOnlineTimeService;

    private static final Logger log = Logger.getLogger(StatusServiceImpl.class);

    private Map<Long, Date> usersMap = new ConcurrentHashMap<>();

    private static final long UPDATE_ONLINE_DELAY_MS = 10 * 60 * 1000; // 10 min

    @Value("${online.period}")
    private long onlinePeriod;

    @Autowired
    public StatusServiceImpl(UserLastOnlineTimeService userLastOnlineTimeService) {
        this.userLastOnlineTimeService = userLastOnlineTimeService;
    }

    @Override
    public void trackUserOnline(long userId) {
        if (userId > 0) {
            usersMap.put(userId, new Date());
            // userLastOnlineTimeService.addOrUpdateUserLastOnlineTime(userId);
        }
    }

    @Override
    public void trackUserOffline(long userId) {
        if (userId > 0) {
            userLastOnlineTimeService.addOrUpdateUserLastOnlineTime(userId);
            usersMap.remove(userId);
        }
    }

    @Override
    public List<Long> getOnlineUsers(@Nonnull List<Long> users) {
        if (users.isEmpty()) {
            return Collections.emptyList();
        }
        List<Long> onlineUsers = new ArrayList<>(users.size());

        for (Long userId : users) {
            Date lastActivityDate = usersMap.get(userId);
            if (lastActivityDate != null) {
                onlineUsers.add(userId);
            }
        }
        return onlineUsers;
    }

    @Override
    public Collection<Long> getAllOnlineUsers() {
        return usersMap.keySet();
    }


    @Override
    public UserLastOnlineTime getUserLastOnlineTime(long userId) {
        Date lastActivityDate = usersMap.get(userId);
        if (lastActivityDate != null) {
            return new UserLastOnlineTime(userId, lastActivityDate, true);
        }
        return userLastOnlineTimeService.getUserLastOnlineTime(userId);
    }

    @Override
    public List<UserLastOnlineTime> getUsersLastOnlineTime(@Nonnull List<Long> users) {
        List<Long> offlineUsers = new ArrayList<>();
        List<UserLastOnlineTime> result = new ArrayList<>(users.size());
        for (Long userId : users) {
            Date lastActivityDate = usersMap.get(userId);
            if (lastActivityDate != null) {
                result.add(new UserLastOnlineTime(userId, lastActivityDate, true));
            } else {
                offlineUsers.add(userId);
            }
        }
        result.addAll(userLastOnlineTimeService.getUsersLastOnlineTime(offlineUsers));
        return result;
    }


    @Scheduled(fixedRate = UPDATE_ONLINE_DELAY_MS)
    private void scheduleUpdateOnline() {
        final long nowTime = new Date().getTime();
        for (Iterator<Map.Entry<Long, Date>> it = usersMap.entrySet().iterator(); it.hasNext(); ) {
            Map.Entry<Long, Date> entry = it.next();
            long lastActivityTime = entry.getValue().getTime();
            userLastOnlineTimeService.addOrUpdateUserLastOnlineTime(new UserLastOnlineTime(entry.getKey(), entry.getValue()));
            if (nowTime - lastActivityTime > onlinePeriod) {
                it.remove();
            }
        }
    }

    @Override
    public int getOnlineUsersCount() {
        return usersMap.size();
    }

}

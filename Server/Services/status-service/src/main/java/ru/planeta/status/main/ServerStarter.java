package ru.planeta.status.main;

import org.apache.log4j.Logger;
import org.eclipse.jetty.server.Handler;
import org.eclipse.jetty.server.NCSARequestLog;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.handler.HandlerCollection;
import org.eclipse.jetty.server.handler.RequestLogHandler;
import org.eclipse.jetty.servlet.ServletHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import ru.planeta.commons.console.ParametersUtils;
import ru.planeta.status.servlet.StatusServlet;

import javax.servlet.Servlet;

/**
 * @author atropnikov
 *         Class ServerStarter
 */
public class ServerStarter {

    private static Logger log = Logger.getLogger(ServerStarter.class);
    private static ClassPathXmlApplicationContext applicationContext = new ClassPathXmlApplicationContext("spring/applicationContext-*.xml", "classpath*:/spring/applicationContext-dao.xml", "classpath*:/spring/applicationContext-global.xml", "classpath*:/spring/applicationContext-geo.xml");
    private static Server server;

    public static void main(String[] args) {
        try {
            Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {
                @Override
                public void run() {
                    onShutDown();
                }
            }));

            String mapping = ParametersUtils.getParameter(args, "--status-mapping", "/*");
            int port = Integer.valueOf(ParametersUtils.getParameter(args, "--port", "9095"));
            String serverLogs = ParametersUtils.getParameter(args, "--server-logs", "disabled");

            Servlet servlet = applicationContext.getBean(StatusServlet.class);

            ServletHandler handler = new ServletHandler();
            ServletHolder holder = new ServletHolder(servlet);
            handler.addServletWithMapping(holder, mapping);

            HandlerCollection handlers = new HandlerCollection();
            RequestLogHandler requestLogHandler = new RequestLogHandler();
            handlers.setHandlers(new Handler[]{handler, requestLogHandler});

            log.info("Starting status server on port: " + port);

            server = new Server(port);
            server.setHandler(handlers);

            if ("disabled".equals(serverLogs) || "0".equals(serverLogs)) {
                log.info("Server logging is disabled");
            } else {
                NCSARequestLog requestLog = new NCSARequestLog(serverLogs);
                requestLog.setRetainDays(90);
                requestLog.setAppend(true);
                requestLog.setExtended(false);
                requestLog.setLogTimeZone("GMT+3");
                requestLogHandler.setRequestLog(requestLog);
            }

            server.start();
            log.info("Status server has been started successfully");
            server.join();

        } catch (Exception ex) {
            log.error("Unhandled exception in the main thread", ex);
        }
    }

        private static void onShutDown() {
            log.info("Stopping server and application context");
            try {
                server.stop();
                log.info("Status server has been stopped successfully");
                applicationContext.close();
                log.info("Application context has been closed successfully");
            } catch (Exception ex) {
                log.error("Error stopping server or closing application context", ex);
            }
        }
}






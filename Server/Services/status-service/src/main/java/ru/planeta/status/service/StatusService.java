package ru.planeta.status.service;

import ru.planeta.model.common.UserLastOnlineTime;

import java.util.Collection;
import java.util.List;

/**
 *
 * Interface StatusService
 * @author a.tropnikov
 */
public interface StatusService {

    void trackUserOnline(long userId);

    void trackUserOffline(long userId);

    List<Long> getOnlineUsers(List<Long> users);

    Collection<Long> getAllOnlineUsers();

    UserLastOnlineTime getUserLastOnlineTime(long userId);

    List<UserLastOnlineTime> getUsersLastOnlineTime(List<Long> users);

    int getOnlineUsersCount();
}

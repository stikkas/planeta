package ru.planeta.status.servlet;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Controller;
import ru.planeta.commons.web.WebUtils;
import ru.planeta.model.common.UserLastOnlineTime;
import ru.planeta.status.service.StatusService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 *
 * @author atropnikov
 * Class StatusServlet
 */
@Controller
public class StatusServlet extends HttpServlet {

    private static final Logger log = Logger.getLogger(StatusServlet.class);
    private static final String TRACK_ONLINE_USER_REQUEST_KEY = "/count/track-online";
    private static final String TRACK_OFFLINE_USER_REQUEST_KEY = "/count/track-offline";
    private static final String GET_ONLINE_USERS_REQUEST_KEY = "/count/get-online";
    private static final String GET_ONLINE_USERS_FULL_INFO_REQUEST_KEY = "/count/get-online-for-admins";
    private static final String LIVE_CHECK = "/live-check";
    private static final String GET_USER_LAST_ONLINE_TIME_REQUEST_KEY = "/count/get-user-last-online-time.json";
    private static final String USER_ID_PARAMETER_KEY = "u";
    private static final String USER_LIST_PARAMETER_KEY = "users";

    private final StatusService statusService;

    @Autowired
    public StatusServlet(StatusService statusService) {
        this.statusService = statusService;
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if (!addAccessOrigin(req.getHeader("Origin"), resp)) {
            return;
        }
        try {
            if (TRACK_ONLINE_USER_REQUEST_KEY.equals(req.getRequestURI())) {
                statusService.trackUserOnline(getUserIdParameter(req));
                WebUtils.setDefaultHeaders(resp);
                writeAnswer(resp);
            } else if (TRACK_OFFLINE_USER_REQUEST_KEY.equals(req.getRequestURI())) {
                statusService.trackUserOffline(getUserIdParameter(req));
                WebUtils.setDefaultHeaders(resp);
                writeAnswer(resp);
            } else if (GET_ONLINE_USERS_REQUEST_KEY.equals(req.getRequestURI())) {
                getOnlineUsers(req, resp);
            }else if (GET_ONLINE_USERS_FULL_INFO_REQUEST_KEY.equals(req.getRequestURI())) {
                getOnlineUsersFullInfo(req, resp);
            } else if (GET_USER_LAST_ONLINE_TIME_REQUEST_KEY.equals(req.getRequestURI())) {
                getUserLastOnlineTime(req, resp);
            } else if (LIVE_CHECK.equals(req.getRequestURI())) {
                WebUtils.setDefaultHeaders(resp);
                resp.getWriter().write("OK");
                resp.getWriter().flush();
            }
        } catch (Exception ex) {
            log.error("Error processing request", ex);
            resp.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
        }
        System.out.println("I'm done!");
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if (!addAccessOrigin(req.getHeader("Origin"), resp)) {
            return;
        }
        try {
            if (GET_ONLINE_USERS_REQUEST_KEY.equals(req.getRequestURI())) {
                getOnlineUsers(req, resp);
            }
        } catch (Exception ex) {
            log.error("Error processing request", ex);
            resp.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
        }
    }

    private static boolean addAccessOrigin(String origin, HttpServletResponse resp) {
        boolean result = false;
        if (origin == null) { // С того же домена 
            result = true;
        } else if (origin.contains("/localhost:") || origin.endsWith("planeta.ru")) { // Доверенные домены
            resp.addHeader("Access-Control-Allow-Origin", origin);
            result = true;
        }
        return result;
    }

    private static int getUserIdParameter(HttpServletRequest request) {
        return NumberUtils.toInt(request.getParameter(USER_ID_PARAMETER_KEY), 0);
    }

    private static void writeAnswer(HttpServletResponse response) throws IOException {
        response.setHeader(HttpHeaders.CONTENT_TYPE, "image/png");
        response.setContentLength(0);
        response.getOutputStream().flush();
    }

    /**
     * returns JSONP callback with online users list
     *
     */
    private void getOnlineUsers(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String[] userList = request.getParameterValues(USER_LIST_PARAMETER_KEY + "[]");
        if (userList == null) {
            String users = request.getParameter(USER_LIST_PARAMETER_KEY);

            if (users != null) {
                if (!users.matches("([0-9]+,)*[0-9]+")) {
                    throw (new IOException("Parse error"));
                }

                userList = users.split(",");
            }
        }

        final List<Long> list;
        if (userList == null || userList.length == 0) {
            list = null;
        } else {
            list = new ArrayList<>(userList.length);
            for (String i : userList) {
                list.add(Long.parseLong(i));
            }
        }

        final String json;
        if (list != null) {
            List<Long> onlineList = statusService.getOnlineUsers(list);
            json = "{\"users\":[" + StringUtils.join(onlineList, ',') + "]}";
        } else {
            json = "{\"count\":" + statusService.getOnlineUsersCount() + "}";
        }

        writeAnswerWithHeaders(request, response, json);
    }

    private void getOnlineUsersFullInfo(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Collection<Long> onlineList = statusService.getAllOnlineUsers();
        final String json = "{\"profiles\":[" + StringUtils.join(onlineList, ',') + "]}";
        writeAnswerWithHeaders(request, response, json);
    }

    private static void writeAnswerWithHeaders(HttpServletRequest request, HttpServletResponse response, String json) throws IOException {
        setHeaders(response);
        writeAnswer(request, response, json);
    }

    private static void setHeaders(HttpServletResponse response) {
        response.setHeader(HttpHeaders.CONTENT_TYPE, "application/json");
        response.setHeader(HttpHeaders.CONNECTION, "close");
        response.setHeader(HttpHeaders.PRAGMA, "no-cache");
        response.setHeader(HttpHeaders.CACHE_CONTROL, "no-cache");
    }

    private void getUserLastOnlineTime(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        UserLastOnlineTime userLastOnlineTime = statusService.getUserLastOnlineTime(getUserIdParameter(request));
        String json = userLastOnlineTime == null ? "{}" : toJson(userLastOnlineTime);
        writeAnswerWithHeaders(request, response, json);
    }

    private static void writeAnswer(HttpServletRequest request, HttpServletResponse response, String json) throws IOException {
        PrintWriter out = response.getWriter();
        String callback = request.getParameter("callback");
        if (callback != null) {
            out.print(callback + "(" + json + ");");
        } else {
            out.print(json);
        }
        out.close();
    }

    private static String toJson(Object obj) {
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            return objectMapper.writeValueAsString(obj);

        } catch (IOException e) {
            return "{}";
        }
    }
}

package ru.planeta.status.test;

import org.junit.After;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import ru.planeta.dao.TransactionScope;

/**
 * @author: ds.kolyshev
 * Date: 27.01.13
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:spring/applicationContext-*.xml", "classpath*:/spring/applicationContext-dao.xml", "classpath*:/spring/applicationContext-geo.xml"})
public abstract class AbstractTest {

	@Autowired
	protected ApplicationContext context;

	static {
		System.setProperty("port", "9095");
		System.setProperty("appid", "status-service");
		System.setProperty("ip", "192.168.10.130");
	}

	private TransactionScope transactionScope;

	@Before
	public void setUp() throws Exception {
		transactionScope = TransactionScope.createOrGetCurrentTest();
	}

	@After
	public void tearDown() throws Exception {
		transactionScope.close();
	}


}

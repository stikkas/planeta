package ru.planeta.status.service;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import ru.planeta.model.common.UserLastOnlineTime;
import ru.planeta.status.test.AbstractTest;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Class TestStatusService
 *
 * @author a.tropnikov
 */
public class TestStatusService extends AbstractTest {

    @Autowired
    private StatusService statusService;

    @Test
    public void testOnlineUsers() {
        long testUserId = 1000000000;

        statusService.trackUserOnline(testUserId);

        List<Long> users = new ArrayList<Long>();
        users.add(testUserId);
        users.add(testUserId + 1);

        List<Long> onlineUsers = statusService.getOnlineUsers(users);
        assertNotNull(onlineUsers);
        assertTrue(onlineUsers.size() == 1);
        assertTrue(onlineUsers.get(0) == testUserId);

        statusService.trackUserOffline(testUserId);

        onlineUsers = statusService.getOnlineUsers(users);
        assertNotNull(onlineUsers);
        assertTrue(onlineUsers.isEmpty());
    }


    @Test
    public void testUsersLastOnlineTime() {
        List<Long> users = new ArrayList<Long>();

        long onlineUserId = 1000000000;
        users.add(onlineUserId);
        statusService.trackUserOnline(onlineUserId);

        long offlineUserId = onlineUserId + 1;
        users.add(offlineUserId);
        statusService.trackUserOnline(offlineUserId);
        statusService.trackUserOffline(offlineUserId);

        long user = onlineUserId + 2;
        users.add(user);

        List<UserLastOnlineTime> selectedUsers = statusService.getUsersLastOnlineTime(users);
        assertNotNull(selectedUsers);
        assertTrue(selectedUsers.size() == 2);

        UserLastOnlineTime selectedOnlineUser = null, selectedOfflineUser = null;
        for (int i = 0; i < 2; i++) {
            if(selectedUsers.get(i).getProfileId() == onlineUserId){
                selectedOnlineUser = selectedUsers.get(i);
            }
            if (selectedUsers.get(i).getProfileId() == offlineUserId){
                selectedOfflineUser = selectedUsers.get(i);

            }
        }


        assertNotNull(selectedOnlineUser);
        assertTrue(selectedOnlineUser.isOnline());

        assertNotNull(selectedOfflineUser);
        assertFalse(selectedOfflineUser.isOnline());


    }

    @Test
    public void testUserLastOnlineTime() {
        long testUserId = 1000000000;
        statusService.trackUserOnline(testUserId);
        UserLastOnlineTime selectedUser = statusService.getUserLastOnlineTime(testUserId);
        assertNotNull(selectedUser);
        assertEquals(selectedUser.getProfileId(), testUserId);

        statusService.trackUserOnline(testUserId);
        selectedUser = statusService.getUserLastOnlineTime(testUserId);
        assertNotNull(selectedUser);
        assertEquals(selectedUser.getProfileId(), testUserId);

    }
}

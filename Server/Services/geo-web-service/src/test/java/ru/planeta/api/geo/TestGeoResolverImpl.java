package ru.planeta.api.geo;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;

import org.apache.log4j.Logger;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import ru.planeta.geowebservice.GeoResolver;
import ru.planeta.geowebservice.GeoResolverImpl;
import ru.planeta.model.profile.location.City;
import ru.planeta.model.profile.location.Country;
import ru.planeta.test.AbstractTest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import org.junit.Ignore;

/**
 * User: a.savanovich
 * Date: 17.04.12
 * Time: 15:00
 */
public class TestGeoResolverImpl extends AbstractTest {

    @Autowired
    private GeoResolver geoResolver;

    private static final Logger log = Logger.getLogger(TestGeoResolverImpl.class);
    private static final String[] IPS = new String[]{"117.111.28.210",
            "58.141.28.209",
            "58.120.248.21",
            "223.62.16.9",
            "211.246.68.144",
            "211.169.204.14",
            "210.178.108.6",
            "202.168.154.165",
            "202.168.154.141",
            "185.158.219.25",
            "185.147.48.1",
            "185.144.78.88",
            "122.34.88.246",
            "121.137.97.39",
            "121.135.225.74",
            "116.84.121.233",
            "114.70.9.222",
            "113.199.68.115",
            "107.167.98.17",
            "107.167.103.119",
            "106.242.90.202",
            "116.121.211.80",
            "107.167.112.216"
    };

    // @Ignore
    @Test
    public void testResolveCityAndCountry() throws Exception {
        City city = geoResolver.resolveCity("92.241.173.132");
        assertNotNull(city);
        assertEquals("\u041c\u043e\u0441\u043a\u0432\u0430", city.getNameRus());

        Country country = geoResolver.resolveCountry("92.241.173.132");
        assertNotNull(country);
        assertEquals("\u0420\u043e\u0441\u0441\u0438\u044f", country.getCountryNameRus());
    }

    @Ignore
    @Test
    public void testUAKiev() {
        City city2 = geoResolver.resolveCity("62.149.0.112");
        assertNotNull(city2);
        assertEquals("\u041a\u0438\u0435\u0432", city2.getNameRus());

        City city3 = geoResolver.resolveCity("91.215.125.27");
        assertNotNull(city3);
        assertEquals("\u041a\u0438\u0435\u0432", city3.getNameRus());
    }

    @Ignore
    @Test
    public void testNotFoundCountries() {
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(
                Files.newInputStream(Paths.get(System.getProperty("user.home"), "bad_country.txt"))))) {
            String ip;
            int countries = 0;
            while ((ip = reader.readLine()) != null) {
                Country country = geoResolver.resolveCountry(ip);
                if (country != null) {
                    ++countries;
                }
            }
            System.out.printf("Resolved %d countries\n", countries);
        } catch (IOException ex) {
            log.error(ex);
        }
    }

    @Ignore
    @Test
    public void testNotFoundCities() {
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(
                Files.newInputStream(Paths.get(System.getProperty("user.home"), "bad_city.txt"))));
                PrintWriter pw = new PrintWriter(Paths.get(System.getProperty("user.home"), "not_found_ips.txt").toString())) {
            String ip;
            int countries = 0;
            while ((ip = reader.readLine()) != null) {
                City city = geoResolver.resolveCity(ip);
                if (city == null) {
                    pw.println(ip);
                } else {
                    System.out.println(city.getNameRus());
                }
            }
            System.out.printf("Resolved %d cities\n", countries);
        } catch (IOException ex) {
            log.error(ex);
        }
    }

    @Test
    @Ignore
    public void ip2c() throws IOException {
        String ip = "114.70.9.222";
        String code = GeoResolverImpl.getCountryCodeFromIp2cOrg(ip);
        assertEquals("KR", code);
    }

    @Ignore
    @Test
    public void testUnrecognized() throws IOException {
        for(String ip : IPS) {
            String code = GeoResolverImpl.getCountryCodeFromIp2cOrg(ip);
            log.info(ip + " " + code);
        }

    }

    @Test
    // @Ignore
    public void testUnrecognizedCountry() throws IOException {
        for(String ip : IPS) {
            Country country = geoResolver.resolveCountry(ip);
            assertNotNull(country);
            log.info(ip + " " + country.getName());
        }

    }
}

package ru.planeta.api.geo;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import ru.planeta.model.profile.location.City;
import ru.planeta.model.profile.location.Country;
import ru.planeta.test.AbstractTest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * User: d.kolyshev
 * Date: 20.02.2013
 */
public class TestGeoResolverWebService extends AbstractTest {

	@Autowired
	private GeoResolverWebService geoResolverWebService;

    @Test
    public void testResolveCityAndCountry() throws Exception {
		City city = geoResolverWebService.resolveCity("92.241.173.132");
		assertNotNull(city);
		assertEquals("\u041c\u043e\u0441\u043a\u0432\u0430", city.getNameRus());

		Country country = geoResolverWebService.resolveCountry("92.241.173.132");
		assertNotNull(country);
		assertEquals("\u0420\u043e\u0441\u0441\u0438\u044f", country.getCountryNameRus());

        Country country2 = geoResolverWebService.resolveCountry("107.167.112.216");
        assertNotNull(country2);
        assertEquals("\u0421\u0428\u0410", country2.getCountryNameRus());
    }

    @Test
    public void testUAKiev() {
        City city2 = geoResolverWebService.resolveCity("62.149.0.112");
        assertNotNull(city2);
        assertEquals("\u041a\u0438\u0435\u0432", city2.getNameRus());

        City city3 = geoResolverWebService.resolveCity("91.215.125.27");
        assertNotNull(city3);
        assertEquals("\u041a\u0438\u0435\u0432", city3.getNameRus());
    }
}

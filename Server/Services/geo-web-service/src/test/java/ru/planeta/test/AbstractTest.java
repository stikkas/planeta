package ru.planeta.test;

import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * @author pvyazankin
 * @since 17.10.12 14:30
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:spring/applicationContext-*.xml", "classpath*:/spring/applicationContext-dao.xml"})
public abstract class AbstractTest {

	@Autowired
	protected ApplicationContext context;
}

package ru.planeta.geowebservice;

import ru.planeta.model.profile.location.City;
import ru.planeta.model.profile.location.Country;

import javax.annotation.Nullable;

/**
 * User: a.savanovich
 * Date: 17.04.12
 * Time: 13:57
 */
public interface GeoResolver {

    @Nullable
	City resolveCity(String ip);

    @Nullable
    Country resolveCountry(String ip);
}



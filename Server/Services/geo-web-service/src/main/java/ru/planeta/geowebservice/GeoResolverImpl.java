package ru.planeta.geowebservice;

import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import ru.planeta.api.service.geo.GeoService;
import ru.planeta.commons.geo.GeoLookupService;
import ru.planeta.commons.geo.IpBlock;
import ru.planeta.commons.geo.IpBlockLocation;
import ru.planeta.dao.commondb.GeoLookupDAO;
import ru.planeta.model.profile.location.City;
import ru.planeta.model.profile.location.Country;

import javax.annotation.Nullable;
import javax.annotation.PreDestroy;
import java.io.IOException;
import java.net.URL;
import java.util.List;

/**
 * User: a.savanovich
 * Date: 17.04.12
 * Time: 14:00
 */
@Service
@Lazy(false)
public class GeoResolverImpl implements GeoResolver {

    private static final Logger log = Logger.getLogger(GeoResolverImpl.class);
    private final GeoLookupDAO geoLookupDAO;
    private final GeoService geoService;
    private GeoLookupService geoLookupService;
    private static final long UPDATE_GEO_BASE_DELAY = 2 * 24 * 60 * 60 * 1000; // two days
    private static final int LIMIT = 1000;

    @Autowired
    public GeoResolverImpl(GeoLookupDAO geoLookupDAO, GeoService geoService) {
        this.geoLookupDAO = geoLookupDAO;
        this.geoService = geoService;
        init();
    }

    private synchronized void setGeoLookupService(IpBlockLocation ipBlockLocation) {
        if (geoLookupService == null) {
            geoLookupService = new GeoLookupService();
        }
        geoLookupService.resetBlockLocations(ipBlockLocation);
    }

    @Scheduled(fixedDelay = UPDATE_GEO_BASE_DELAY)
    void init() {
        try {
            IpBlockLocation ipBlockLocation = new IpBlockLocation();
            int offset = 0;
            for (int i = 0; i < 1000; ++i) {
                List<IpBlock> blocks = geoLookupDAO.selectBlocks(offset, LIMIT);
                if (blocks.isEmpty()) {
                    break;
                }
                ipBlockLocation.addLocations(blocks);
                offset += LIMIT;
            }
            setGeoLookupService(ipBlockLocation);
        } catch (Exception ex) {
            log.error("Error loading locations", ex);
        }
    }

    @Override
    @Nullable
    public City resolveCity(String ip) {
        try {
            if (ip == null) {
                log.error("Error usage. check your code");
                return null;
            }

            int locationId = getLocationId(ip);
            if (locationId == -1) {
                return null;
            }
            if (locationId == 0) {
                log.error("Location is 0 for ip=" + ip);
                return null;
            }

            Integer cityIdPlaneta = geoLookupDAO.getCityId(locationId);
            if (cityIdPlaneta != null) {
                return geoService.getCityById(cityIdPlaneta);
            } else {
                log.error("No city recognized for location " + locationId);
                return null;
            }

        } catch (Exception ex) {
            log.error("Exception while getting city from ip " + ip, ex);
            return null;
        }
    }

    private synchronized int getLocationId(String ip) {
        return geoLookupService.getLocationId(ip);
    }

    @Override
    @Nullable
    public Country resolveCountry(String ip) {
        try {
            String countryCode = getCountryCode(ip);
            if (countryCode == null || countryCode.equals("--")) {
                log.info("Maxmind failed. try ip2c " + ip);
                countryCode = getCountryCodeFromIp2cOrg(ip);
                log.info("ip2c code " + countryCode);
                return getCountryByCode(ip, countryCode, true);
            }
            Country country = getCountryByCode(ip, countryCode, false);
            if (country == null) {
                countryCode = getCountryCodeFromIp2cOrg(ip);
                return getCountryByCode(ip, countryCode, true);
            }
            return country;
        } catch (Exception ex) {
            log.error("Exception while getting country from ip " + ip, ex);
        }
        return null;
    }

    private Country getCountryByCode(String ip, String countryCode, boolean needLog) {
        Integer countryId = geoLookupDAO.getCountryByCode(countryCode);
        if (countryId != null) {
            return geoService.getCountryById(countryId);
        } else if (needLog) {
            if ("ZZ".equals(countryCode)) {
                log.warn("ZZ code for ip " + ip);
            } else {
                log.error("Not recognise country for ip " + ip + " code " + countryCode);
            }
        }
        return null;
    }

    // http://about.ip2c.org/#inputs
    public static String getCountryCodeFromIp2cOrg(String ip) throws IOException {
        String answer = IOUtils.toString(new URL("http://ip2c.org/" + ip));
        return answer.split(";")[1];
    }

    @PreDestroy
    public void finish() {
        geoLookupService.finish();
    }

    private synchronized String getCountryCode(String ip) {
        return geoLookupService.getCountryCode(ip);
    }
}

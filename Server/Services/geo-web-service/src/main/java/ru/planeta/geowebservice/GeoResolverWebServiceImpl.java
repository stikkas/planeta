package ru.planeta.geowebservice;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.planeta.api.geo.GeoResolverWebService;
import ru.planeta.model.profile.location.City;
import ru.planeta.model.profile.location.Country;

import javax.annotation.Nonnull;

/**
 * @author: ds.kolyshev
 * Date: 20.02.13
 */
@Service
public class GeoResolverWebServiceImpl implements GeoResolverWebService {

	@Autowired
	private GeoResolver geoResolver;

	@Override
	public City resolveCity(@Nonnull String ip) {
		return geoResolver.resolveCity(ip);
	}

	@Override
	public Country resolveCountry(@Nonnull String ip) {
		return geoResolver.resolveCountry(ip);
	}
}

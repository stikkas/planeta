package ru.planeta.service;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import ru.planeta.image.ImageInfo;
import ru.planeta.model.enums.ThumbnailType;
import ru.planeta.test.AbstractTest;
import ru.planeta.utils.ThumbConfiguration;

import java.io.File;

import static org.junit.Assert.assertEquals;

/**
 * User: s.kalmykov
 * Date: 16.04.12
 * Time: 0:02
  */
public class TestThumbGeneratonService extends AbstractTest {

    private static Logger LOG = Logger.getLogger(TestThumbGeneratonService.class);

    @Autowired
    private ThumbGenerationService thumbGenerationService;

    @Test
    public void testExternalVideoCashing() throws Exception {
        File testFile = getFileFromClasspath("test.jpg");
        File dstFile = new File("testfile222.jpg");
        FileUtils.copyFile(testFile, dstFile);

        ThumbConfiguration configuration = new ThumbConfiguration();
        configuration.setCrop(false);
        configuration.setWidth(420);
        configuration.setHeight(420);
        configuration.setPad(true);

        thumbGenerationService.generateOneThumbnail(testFile, dstFile, configuration, true);

        ImageInfo cachedThumbInfo = new ImageInfo(dstFile);
        assertEquals(configuration.getWidth(), cachedThumbInfo.getWidth());

    }

    @Test
    public void testMedium() throws Exception {
        File testFile = getFileFromClasspath("real.jpg");
        File dstFile = new File("medium.jpg");
        FileUtils.copyFile(testFile, dstFile);

        ThumbConfiguration configuration = new ThumbConfiguration(ThumbnailType.MEDIUM);
        thumbGenerationService.generateOneThumbnail(testFile, dstFile, configuration, true);

        ImageInfo cachedThumbInfo = new ImageInfo(dstFile);
        assertEquals(configuration.getWidth(), cachedThumbInfo.getWidth());

    }

    @Test
    public void testCrop() throws Exception {
        File testFile = getFileFromClasspath("test.jpg");
        File dstFile = new File("testCropped.jpg");
        FileUtils.copyFile(testFile, dstFile);

        ThumbConfiguration configuration = new ThumbConfiguration();
        configuration.setWidth(640);
        configuration.setHeight(390);
        configuration.setCrop(false);
        configuration.setBackgroundColor("green");
        /*configuration.setDisableAnimatedGif(disableAnimatedGif);*/
        configuration.setPad(true);
        thumbGenerationService.generateOneThumbnail(testFile, dstFile, configuration, true);

        ImageInfo cachedThumbInfo = new ImageInfo(dstFile);
        assertEquals(configuration.getWidth(), cachedThumbInfo.getWidth());

    }

    @Test
    public void testPreview() throws Exception {
        File testFile = getFileFromClasspath("we_need_you.jpg");
        File dstFile = new File("preview.jpg");
        FileUtils.copyFile(testFile, dstFile);

        ThumbConfiguration configuration = new ThumbConfiguration(ThumbnailType.PHOTOPREVIEW);
        configuration.setPad(true);
        thumbGenerationService.generateOneThumbnail(testFile, dstFile, configuration, true);

        ImageInfo cachedThumbInfo = new ImageInfo(dstFile);
        assertEquals(configuration.getHeight(), cachedThumbInfo.getHeight());
    }

    @Test
    public void testGenerateDir() throws Exception {
        String[] a = {"89fca", "bf210", "84716", "95566", "9556f", "7348f", "9556a"};
        for (String str : a) {
            LOG.debug(BaseService.generateFilePath(Long.parseLong(str, 16)));
        }
    }
}

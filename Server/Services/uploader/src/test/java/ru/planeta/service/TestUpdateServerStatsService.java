package ru.planeta.service;

import org.apache.commons.io.FileSystemUtils;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import ru.planeta.dao.statdb.StatStaticNodeDAO;
import ru.planeta.model.stat.StatStaticNode;
import ru.planeta.model.stat.StaticNodeFreeSpaceComparator;
import ru.planeta.test.AbstractTest;

import java.util.Collection;
import java.util.TreeSet;

/**
 * @author pvyazankin
 * @since 02.11.12 11:26
 */
public class TestUpdateServerStatsService extends AbstractTest {

    private static final StaticNodeFreeSpaceComparator COMPARATOR = new StaticNodeFreeSpaceComparator(false);

    @Autowired
    private UpdateServerStatsService updateServerStatsService;
    @Autowired
    private StatStaticNodeDAO statStaticNodeDAO;
    @Value("${static.node}")
    private String currentNode;


    @Test
    @Ignore
    public void updateServerStatsTest() throws Exception {
        long freeSpace = FileSystemUtils.freeSpaceKb();
        updateServerStatsService.updateServerStats();

        long acceptableDeviation = 10l;

        TreeSet<StatStaticNode> nodes = new TreeSet(COMPARATOR);
        statStaticNodeDAO.findAll().forEach(node -> nodes.add(node));
        long actual = -100500;
        for (StatStaticNode node : nodes) {
            if (currentNode.equals(node.getDomain())) {
                actual = node.getFreeSpaceKB();
            }
        }
        Assert.assertTrue(Math.abs(freeSpace - actual) < acceptableDeviation);
    }


}

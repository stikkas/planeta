package ru.planeta.service;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.im4java.core.IM4JavaException;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import ru.planeta.dao.profiledb.PhotoAlbumDAO;
import ru.planeta.dao.profiledb.PhotoDAO;
import ru.planeta.domain.PhotoFile;
import ru.planeta.image.*;
import ru.planeta.model.Constants;
import ru.planeta.model.enums.PermissionLevel;
import ru.planeta.model.enums.ThumbnailType;
import ru.planeta.model.profile.media.Photo;
import ru.planeta.model.profile.media.PhotoAlbum;
import ru.planeta.test.BaseTestSet;
import ru.planeta.utils.ThumbConfiguration;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.*;

/**
 * @author ds.kolyshev
 *         Date: 28.10.11
 */
public class TestImageContentService extends BaseTestSet {

    private static final Logger log = Logger.getLogger(TestImageContentService.class);

    @Autowired
    private PhotoDAO photoDAO;
    @Autowired
    private PhotoAlbumDAO photoAlbumDAO;
    @Autowired
    private ImageContentService imageContentService;
    @Autowired
    private ThumbGenerationService thumbGenerationService;
    @Autowired
    private ImageConfigurationsMap imageConfigurationsMap;

    @Test
    public void testUploadPhotoToNewAlbum() throws Exception {
        File testFile = getFileFromClasspath("test.jpg");

        File dstFile = new File("testfile.jpg");
        FileUtils.copyFile(testFile, dstFile);

        long profileId = 118717;//insertUserProfile().getProfileId();
        long albumId = 0;
        int albumTypeId = 0;

        PhotoFile photoFile = imageContentService.uploadPhotoFile(dstFile, profileId, profileId, albumId, albumTypeId, 0, null);
        File uploadedFile = photoFile.getFile();
        // check file uploaded
        assertTrue(uploadedFile.exists());

        PhotoAlbum createdAlbum = photoAlbumDAO.selectAlbumByType(profileId, Constants.INSTANCE.getALBUM_DEFAULT());

        assertEquals(createdAlbum.getProfileId(), profileId);
        assertEquals(createdAlbum.getAlbumTypeId(), Constants.INSTANCE.getALBUM_DEFAULT());
        assertTrue(createdAlbum.getPhotosCount() > 0);
        assertEquals(photoFile.getPhoto().getPhotoId(), createdAlbum.getImageId());
        assertNotNull(photoFile.getPhoto().getImageUrl());

        checkGroupPhoto(profileId, profileId, createdAlbum.getAlbumId());
        checkThumbnails(uploadedFile);

        imageContentService.regenerateThumbnails(profileId, profileId, photoFile.getPhoto().getPhotoId());
        checkThumbnails(uploadedFile);
    }

    @Test
    public void testUploadPhotoToExistingAlbum() throws Exception {
        File testFile = getFileFromClasspath("test.jpg");

        File dstFile = new File("testfile.jpg");
        FileUtils.copyFile(testFile, dstFile);

        long profileId = insertUserProfile().getProfileId();
        int albumTypeId = Constants.INSTANCE.getALBUM_DEFAULT();

        PhotoAlbum photoAlbum = new PhotoAlbum();
        photoAlbum.setAuthorProfileId(profileId);
        photoAlbum.setProfileId(profileId);
        photoAlbum.setPhotosCount(0);
        photoAlbum.setTimeAdded(new Date());
        photoAlbum.setTimeUpdated(new Date());
        photoAlbum.setViewPermission(PermissionLevel.EVERYBODY);
        photoAlbum.setViewsCount(0);
        photoAlbum.setAlbumTypeId(albumTypeId);
        photoAlbum.setTitle("Album title");

        photoAlbumDAO.insert(photoAlbum);

        PhotoFile photoFile = imageContentService.uploadPhotoFile(dstFile, profileId, profileId, photoAlbum.getAlbumId(), albumTypeId, 0, null);
        File uploadedFile = photoFile.getFile();
        // check file uploaded
        assertTrue(uploadedFile.exists());

        PhotoAlbum updatedPhotoAlbum = photoAlbumDAO.selectAlbumById(profileId, photoAlbum.getAlbumId());
        assertNotNull(updatedPhotoAlbum);
        assertEquals(photoAlbum.getPhotosCount() + 1, updatedPhotoAlbum.getPhotosCount());
        assertEquals(photoFile.getPhoto().getPhotoId(), updatedPhotoAlbum.getImageId());
        assertEquals(photoFile.getPhoto().getImageUrl(), updatedPhotoAlbum.getImageUrl());

        checkGroupPhoto(profileId, profileId, photoAlbum.getAlbumId());

        checkThumbnails(uploadedFile);
    }

    @Test
    public void testCropImage() throws URISyntaxException, IOException, ImageOperationException, IM4JavaException, InterruptedException {
        File dstFile = null;

        try {
            File testFile = getFileFromClasspath("test.jpg");

            dstFile = new File("testfile.jpg");
            FileUtils.copyFile(testFile, dstFile);

            ImageInfo originalFileInfo = new ImageInfo(dstFile);
            assertNotNull(originalFileInfo);
            int cropX = 10;
            int cropY = 5;
            int cropWidth = 50;
            int cropHeight = 50;

            File croppedFile = imageContentService.cropImage(dstFile, cropX, cropY, cropWidth, cropHeight);
            assertTrue(croppedFile.exists());

            ImageInfo croppedFileInfo = new ImageInfo(croppedFile);
            assertEquals(cropHeight, croppedFileInfo.getHeight());
            assertEquals(cropWidth, croppedFileInfo.getWidth());
        } finally {
            FileUtils.deleteQuietly(dstFile);
        }
    }

    @Test
    public void testCropGifImage() throws URISyntaxException, IOException, ImageOperationException, IM4JavaException, InterruptedException {
        File dstFile = null;

        try {
            File testGifFile = getFileFromClasspath("testGif.gif");

            dstFile = new File("testGifFile.gif");
            FileUtils.copyFile(testGifFile, dstFile);

            ImageInfo originalFileInfo = new ImageInfo(dstFile);
            assertNotNull(originalFileInfo);
            int cropX = 100;
            int cropY = 250;
            int cropWidth = 50;
            int cropHeight = 50;

            File croppedFile = imageContentService.cropImage(dstFile, cropX, cropY, cropWidth, cropHeight);
            assertTrue(croppedFile.exists());

            ImageInfo croppedFileInfo = new ImageInfo(croppedFile);
            assertTrue(croppedFileInfo.getWidth() == cropWidth);
            assertTrue(croppedFileInfo.getHeight() == cropHeight);
        } finally {
            FileUtils.deleteQuietly(dstFile);
        }
    }

    @Test
    public void testCheckThumbnail() throws Exception {
        File testFile = getFileFromClasspath("test.jpg");

        File dstFile = new File("testfile.jpg");
        FileUtils.copyFile(testFile, dstFile);

        long profileId = insertUserProfile().getProfileId();
        long albumId = 0;
        int albumTypeId = 0;

        PhotoFile photoFile = imageContentService.uploadPhotoFile(dstFile, profileId, profileId, albumId, albumTypeId, 0, null);
        File uploadedFile = photoFile.getFile();
        // check file uploaded
        assertTrue(uploadedFile.exists());

        checkThumbnails(uploadedFile);

        File thumbFile = thumbGenerationService.checkThumbnail(photoFile.getPhoto().getPhotoId(), ThumbnailType.USER_AVATAR.getThumbFileName() + ".jpg", false);
        assertTrue(thumbFile.exists());

        boolean deleted = thumbFile.delete();
        assertFalse(thumbFile.exists());
        assertTrue(deleted);

        thumbFile = thumbGenerationService.checkThumbnail(photoFile.getPhoto().getPhotoId(), ThumbnailType.USER_AVATAR.getThumbFileName() + ".jpg", false);
        assertTrue(thumbFile.exists());

        checkThumbnails(uploadedFile);

    }

    @Test
    public void testBadGifConvert() throws Exception {
        File testFile = getFileFromClasspath("bad_gif.gif");
        ImageInfo originalFileInfo = new ImageInfo(testFile);
        assertNotNull(originalFileInfo);
    }

    @Test
    @Ignore
    public void testBadWebPConvert() throws Exception {
        File testFile = getFileFromClasspath("bad.webp");
        ImageInfo originalFileInfo = new ImageInfo(testFile);
        assertNotNull(originalFileInfo);
    }

    @Test
    public void testBad2GifConvert() throws Exception {
        File testFile = getFileFromClasspath("bad_gif_2.gif");
        ImageInfo originalFileInfo = new ImageInfo(testFile);
        assertNotNull(originalFileInfo);
    }

    @Test
    @Ignore
    public void testTooManyFilesOpen() throws URISyntaxException, IOException, ImageOperationException, InterruptedException, IM4JavaException {
        List<File> dstFiles = new ArrayList<>();

        try {
            File testGifFile = getFileFromClasspath("test.jpg");

            for (int i = 0; i < 1000; i++) {
                File file = new File("testFile" + i + ".jpg");
                dstFiles.add(file);
                FileUtils.copyFile(testGifFile, file);
                ImageInfo originalFileInfo = new ImageInfo(file);
                assertNotNull(originalFileInfo);
            }

        } finally {
            for (File file : dstFiles) {
                FileUtils.deleteQuietly(file);
            }
        }
    }

    private void checkGroupPhoto(long clientId, long profileId, long albumId) {
        Photo createdPhoto = photoDAO.selectPhotoByAlbumId(profileId, albumId, 0, 10).get(0);

        assertEquals(createdPhoto.getProfileId(), profileId);
        assertEquals(createdPhoto.getAlbumId(), albumId);
        assertEquals(createdPhoto.getAuthorProfileId(), clientId);
    }

    /**
     * Check generated thumbnails
     *
     * @param uploadedFile file to upload
     */
    private void checkThumbnails(File uploadedFile) throws IOException, InterruptedException, IM4JavaException {
        ImageConfiguration imageConfiguration = imageConfigurationsMap.getDefaultImageConfiguration();
        ImageInfo originalImageInfo = new ImageInfo(uploadedFile);
        for (ThumbConfiguration thumbConfiguration : imageConfiguration.getThumbConfigurations()) {
            File targetFile = new File(uploadedFile.getParent(), thumbConfiguration.getName() + ".jpg");
            log.info("Checking thumbnail: " + targetFile.getPath());
            assertTrue(targetFile.exists());

            ImageInfo imageInfo = new ImageInfo(targetFile);

            int flag = imageConfigurationsMap.getFlagSaveSide();
            int thumbHeight = thumbConfiguration.getHeight();
            int thumbWidth = thumbConfiguration.getWidth();

            if (thumbHeight != flag && thumbWidth == flag) {
                assertTrue(imageInfo.getHeight() <= thumbHeight);
                //assertEquals(originalImageInfo.getWidth(), imageInfo.getWidth());
            }
            if (thumbHeight == flag && thumbWidth != flag) {
                assertTrue(imageInfo.getWidth() <= thumbWidth);
                //assertEquals(originalImageInfo.getHeight(), imageInfo.getHeight());
            }

            if (thumbHeight != flag && thumbWidth != flag) {
                if (thumbWidth == 0 && thumbHeight == 0) {
                    //check not resd with 0,0
                    assertEquals(originalImageInfo.getWidth(), imageInfo.getWidth());
                    assertEquals(originalImageInfo.getHeight(), imageInfo.getHeight());
                } else if (thumbWidth == 0) {
                    assertTrue(imageInfo.getHeight() <= thumbHeight);
                } else if (thumbHeight == 0) {
                    assertTrue(imageInfo.getWidth() <= thumbWidth);
                } else {
                    Float imageHeight = (float) originalImageInfo.getHeight();
                    Float imageWidth = (float) originalImageInfo.getWidth();

                    if (thumbConfiguration.isCrop()) {
                        assertTrue(imageInfo.getHeight() <= thumbHeight);
                        assertTrue(imageInfo.getWidth() <= thumbWidth);
                    } else {
                        if (imageWidth / imageHeight > (float) thumbWidth / (float) thumbHeight) {
                            assertTrue(imageInfo.getHeight() <= thumbHeight);
                        } else {
                            assertTrue(imageInfo.getWidth() <= thumbWidth);
                        }
                    }
                }
            }

            assertEquals(BucketType.JPEG, imageInfo.getBucketType());
        }
    }


}

package ru.planeta.service;

import org.junit.Assert;
import org.junit.Test;

import java.net.URI;
import java.net.URL;
import java.net.URLDecoder;

/**
 * @author p.vyazankin
 * @since 2/5/13 12:16 PM
 */
public class TestCustomOperations {

    @Test
    public void createUrlFromEncodedUrlString() throws Exception {
        String s = "http%3A%2F%2Fimg0.joyreactor.cc%2Fpics%2Fpost%2Fanime-%25D0%25BA%25D1%2580%25D0%25B0%25D1%2581%25D0%25B8%25D0%25B2%25D1%258B%25D0%25B5-%25D0%25BA%25D0%25B0%25D1%2580%25D1%2582%25D0%25B8%25D0%25BD%25D0%25BA%25D0%25B8-557440.jpeg";
        URL url0 = new URL(URLDecoder.decode(s));
        URL url = new URL(new URI(s).getPath());
        Assert.assertNotNull(url);
    }
}

package ru.planeta.video;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by a.savanovich on 31.08.2016.
 */
public class VideoInfoParserTest {

    @Test
    public void testGetVideoConfigString() throws Exception {
        String codec = "Video: h264 (Main) (avc1 / 0x31637661), yuv420p(tv, bt709), 1920x1080, 5591 kb/s, 25 fps, 25 tbr, 25k tbn, 50 tbc (default)";
        String input = VideoInfoParser.getVideoConfigString(codec, 0);
        String[] info = input.split(",");

        String codecName = info[0];
        String[] size = info[2].split("x");

        int width = Integer.parseInt(size[0].trim());
        final int height;
        int spaceIndex = size[1].indexOf(" ");
        if (spaceIndex != -1) {
            height = Integer.parseInt(size[1].substring(0, spaceIndex));
        } else {
            height = Integer.parseInt(size[1].trim());
        }

        assertEquals(1080, height);
        assertEquals(1920, width);
        assertEquals("h264", codecName);
    }
}
package ru.planeta.service;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.im4java.core.IM4JavaException;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import ru.planeta.api.exceptions.NotFoundException;
import ru.planeta.image.ImageOperationException;
import ru.planeta.test.BaseTestSet;
import ru.planeta.utils.ThumbConfiguration;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.SocketTimeoutException;
import java.net.URISyntaxException;

import static org.junit.Assert.assertTrue;

/**
 * @author a.savanovich
 * @since 08.02.12
 */
public class TestProxyContentService extends BaseTestSet {

    @Autowired
    private ProxyContentService proxyContentService;

    private static Logger LOG = Logger.getLogger(TestProxyContentService.class);

    @Test
    public void testProxyService() throws Exception {
//        String url = "http://s015.radikal.ru/i331/1011/35/533ca0e8e602.gif";
        try {
            String url = "http://st.kinopoisk.ru/im/kadr/1/3/0/kinopoisk.ru-Charlize-Theron-1308166.jpg";

            ThumbConfiguration thumbConfiguration = new ThumbConfiguration();
            thumbConfiguration.setHeight(50);
            thumbConfiguration.setWidth(130);
            thumbConfiguration.setName(url);
            String image;
            try {
                image = proxyContentService.getRelativePath(url, thumbConfiguration, false);
            } catch (SocketTimeoutException e) {
                return;
            }
            assertTrue(!StringUtils.isEmpty(image));
        } catch (SocketTimeoutException e) {
            e.printStackTrace();
        }

    }

    @Test
    @Ignore
    public void testProxyServiceNotFound() throws Exception {
        // not-existing image
        String url = "http://img1.joyreactor.cc/pics/post/%D0%B3%D0%B8%D1%84%D0%BA%D0%B8-%D0%B4%D0%B5%D0%B2%D1%83%D1%88%D0%BA%D0%B8-%D0%BF%D0%BE%D1%86%D0%B5%D0%BB%D1%83%D0%B9-44407d1.gif";//"http://s015.radikal.ru/i331/1011/35/533ca0e8asdfasdfasdfe602.gif";

        ThumbConfiguration thumbConfiguration = new ThumbConfiguration();
        thumbConfiguration.setName(url);
        try {
            proxyContentService.getRelativePath(url, thumbConfiguration, false);
        } catch (SocketTimeoutException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Test(expected = IM4JavaException.class)
    public void testProxyServiceNotAnImage() throws Exception {
        // not-an-image
        String url = "http://www.urbandictionary.com";

        ThumbConfiguration thumbConfiguration = new ThumbConfiguration();
        thumbConfiguration.setName(url);
        proxyContentService.getRelativePath(url, thumbConfiguration, false);
    }

    @Test
    @Ignore
    public void googleTest() throws NotFoundException, InterruptedException, IOException, URISyntaxException, ImageOperationException, IM4JavaException {
        String url = "https://lh3.googleusercontent.com/kjaA1xi-joeYmw72Uhe9BH544LW0apiw6bHhNItdbf4auPvKdlsRn4yreSKHIIFPBGz3ktesvVUWSQofRNk0PTQw7WStUgue=w1920-h1080-rw-no";
        String path = ProxyContentServiceImpl.getDirFromUrl(url);
        ThumbConfiguration thumbConfiguration = new ThumbConfiguration();
        thumbConfiguration.setName(url);
        proxyContentService.getRelativePath(url, thumbConfiguration, false);
        LOG.info(path);
    }
}

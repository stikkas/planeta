package ru.planeta.service;

import org.apache.commons.io.FileUtils;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import ru.planeta.api.exceptions.PermissionException;
import ru.planeta.domain.ProfileFileModel;
import ru.planeta.model.profile.ProfileFile;
import ru.planeta.test.AbstractTest;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * @author: ds.kolyshev
 * Date: 20.03.13
 */
public class TestProfileFileContentService extends AbstractTest {

	@Autowired
	private ProfileFileContentService profileFileContentService;

	@Test
	public void testUpload() throws URISyntaxException, IOException, PermissionException {
			File testFile = getFileFromClasspath("test.jpg");

			File dstFile = new File("testfile.jpg");
			FileUtils.copyFile(testFile, dstFile);

			long profileId = insertPlanetaAdminPrivateInfo().getUserId();

			ProfileFileModel profileFileModel = profileFileContentService.uploadProfileFileModel(dstFile, dstFile.getName(), profileId, profileId);
			File uploadedFile = profileFileModel.getFile();
			// check file uploaded
			assertTrue(uploadedFile.exists());

			ProfileFile profileFile = profileFileModel.getProfileFile();
			assertNotNull(profileFile);
			assertTrue(profileFile.getFileId() > 0);
			assertNotNull(profileFile.getFileUrl());
			assertEquals(profileId, profileFile.getProfileId());
			assertEquals(profileId, profileFile.getAuthorId());
			//assertEquals(dstFile.getName(), profileFile.getName());
	}
}

package ru.planeta.service;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import ru.planeta.image.StringParameters;
import ru.planeta.test.AbstractTest;

import java.awt.*;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

/**
 * @author s.kalmykov
 *         Date: 22.03.2012
 */
public class TestStubThumbService extends AbstractTest {

    @Autowired
    private StubThumbService stubThumbService;

    @Test
    public void testVideoStub() throws IOException {

        File dstFile;
        String name = "ХУЙ,!%_)%(Х(*?%)(*?)%:_%Ёрdp ------------------------asdfasdfasdfasdfsdf-------";
        String duration = "1:30:45";
        String extension = "png";
        String baseImageUrl = "http://s1.planeta.ru/i/55a6/original.jpg";
//        StaticNodesService staticNodesService = TestHelper.getContext().getBean(StaticNodesService.class);
        String overlayUrl = "http://" + /*staticNodesService.getResourceHost()*/ "static.dev.planeta.ru" + "/images/bb-video-big.png";
        dstFile = new File("testfile1." + extension);
//        FileUtils.touch(dstFile);


        StubThumbService videoStubThumbService = stubThumbService;
        OutputStream os = new FileOutputStream(dstFile);
        StringParameters params = new StringParameters(
                12, Font.PLAIN, "Tahoma", 1024, 600, Color.white, Color.cyan, 5, 3, 10, 8);
        params.imageExtension = extension;
        videoStubThumbService.drawVideoToStream(baseImageUrl, overlayUrl, name, duration, os, params);
        os.flush();

    }

    @Test
    public void testAudioStub() throws IOException {

        File dstFile;
        String name = "Sleep Away ХУЙ,!%_)%(Х(*?%)(*?)%:_%Ёрdp -)%(Х(*?----------";
        String duration = "1:45:03";
        String artist = "Bob Acri  АРТИСТ ХУЙ,!%_)%(Х(*?%)(*?)%:_%Ёрdp)%(Х(*?%)(*?)%:_%Ёрd)%(Х(*?%)(*?)%:_%Ёрd)%(Х(*?%)(*?)%:_%Ёрd)%(Х(*?%)(*?)%:_%Ёрd)%(Х(*?%)(*?)%:_%Ёрd -------------------------------";
        String extension = "png";
//        StaticNodesService staticNodesService = TestHelper.getContext().getBean(StaticNodesService.class);
        String playUrl = "http://" + /*staticNodesService.getResourceHost()*/ "static.dev.planeta.ru" + "/images/icon/aac-play.png";
        int width = 300;
        dstFile = new File("testfile2." + extension);
//        FileUtils.touch(dstFile);

        StubThumbService audioStubThumbService = stubThumbService;
        StringParameters params = new StringParameters(
            12, Font.PLAIN, "Tahoma", width, 45, Color.black, Color.white, 5, 0, 5, 8);
        OutputStream os = new FileOutputStream(dstFile);
        audioStubThumbService.drawAudioToStream(playUrl, name, artist, duration, params, os);
        os.flush();
    }
}

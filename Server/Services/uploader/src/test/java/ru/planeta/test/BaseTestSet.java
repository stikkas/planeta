package ru.planeta.test;

import org.junit.AfterClass;

/**
 * Base class for all test sets
 */
public class BaseTestSet extends AbstractTest {

    @AfterClass
    public static void afterAll() throws Exception {
        cleanUp();
    }
}

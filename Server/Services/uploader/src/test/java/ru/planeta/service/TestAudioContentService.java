package ru.planeta.service;

import org.apache.commons.io.FileUtils;
import org.jaudiotagger.audio.exceptions.CannotReadException;
import org.jaudiotagger.audio.exceptions.InvalidAudioFrameException;
import org.jaudiotagger.audio.exceptions.ReadOnlyFileException;
import org.jaudiotagger.tag.TagException;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import ru.planeta.api.exceptions.NotFoundException;
import ru.planeta.api.exceptions.PermissionException;
import ru.planeta.dao.profiledb.AudioTrackDAO;
import ru.planeta.domain.AudioFile;
import ru.planeta.model.profile.media.AudioTrack;
import ru.planeta.test.BaseTestSet;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;

import static org.junit.Assert.*;

/**
 * @author ds.kolyshev
 *         Date: 28.10.11
 */
public class TestAudioContentService extends BaseTestSet {
    @Autowired
    private AudioContentServiceImpl audioContentService;
    @Autowired
    private AudioTrackDAO audioTrackDAO;

    @Test
    public void testUploadAudioTrack() throws IOException, InterruptedException, InvalidAudioFrameException, ReadOnlyFileException, TagException, CannotReadException, PermissionException, NotFoundException {
		File dstFile = null;

        try {

            File testFile = getFileFromClasspath("demo2.mp3");
            dstFile = new File("testfile.mp3");
            FileUtils.copyFile(testFile, dstFile);

			long profileId = insertUserProfile().getProfileId();

            long audioAlbumId = 0;

            AudioFile uploadedFile = audioContentService.uploadAudioTrackFile(dstFile, dstFile.getName(), profileId, profileId, audioAlbumId);
            assertTrue(uploadedFile.getFile().exists());

            AudioTrack uploadedTrack = audioTrackDAO.selectAudioTracks(profileId, 0, 10).get(0);

            assertEqualsTracks(uploadedFile.getAudioTrack(), uploadedTrack);
            assertEquals(profileId, uploadedTrack.getProfileId());
            assertEquals("GusGus", uploadedTrack.getArtistName());
            assertEquals("Over", uploadedTrack.getTrackName());
            assertEquals("\u0442\u0435\u0441\u0442\u043e\u0432\u044b\u0439 \u043a\u043e\u043c\u043f\u043e\u0437\u0438\u0442\u043e\u0440", uploadedTrack.getAuthors());
            assertEquals(1, uploadedTrack.getAlbumTrackNumber());
            assertEquals(299, uploadedTrack.getTrackDuration());
            assertEquals(audioAlbumId, uploadedTrack.getAlbumId());
            assertEquals(profileId, uploadedTrack.getAuthorProfileId());
            assertEquals(profileId, uploadedTrack.getUploaderProfileId());
            assertEquals(uploadedTrack.getTrackId(), uploadedTrack.getUploaderTrackId());
            assertEquals(uploadedTrack.getAlbumId(), uploadedTrack.getUploaderAlbumId());

            File testFile2 = getFileFromClasspath("demo.mp3");
            dstFile = new File("testfile2.mp3");
            FileUtils.copyFile(testFile2, dstFile);
            AudioFile uploadedFile2 = audioContentService.uploadAudioTrackFile(dstFile, dstFile.getName(), profileId, profileId, audioAlbumId);
            assertNotNull(uploadedFile2);
            AudioTrack uploadedTrack2 = audioTrackDAO.selectAudioTracks(profileId, 0, 10).get(0);

            assertEquals(1, uploadedTrack2.getAlbumTrackNumber());
            assertEquals(uploadedTrack2.getTrackId(), audioTrackDAO.selectAudioTracks(profileId, 0, 10).get(0).getTrackId());
            assertEquals(uploadedTrack2.getTrackId(), audioTrackDAO.selectAudioTracks(profileId, 0, 10).get(0).getUploaderTrackId());

			audioContentService.reconvertAudio(profileId, profileId, uploadedTrack2.getTrackId());

        } finally {
            FileUtils.deleteQuietly(dstFile);
        }
    }

    private static void assertEqualsTracks(AudioTrack audioTrack, AudioTrack uploadedTrack) {
        assertEquals(audioTrack.getProfileId(), uploadedTrack.getProfileId());
        assertEquals(audioTrack.getArtistName(), uploadedTrack.getArtistName());
        assertEquals(audioTrack.getTrackName(), uploadedTrack.getTrackName());
        assertEquals(audioTrack.getAuthors(), uploadedTrack.getAuthors());
        assertEquals(audioTrack.getAlbumTrackNumber(), uploadedTrack.getAlbumTrackNumber());
        assertEquals(audioTrack.getTrackDuration(), uploadedTrack.getTrackDuration());
        assertEquals(audioTrack.getAlbumId(), uploadedTrack.getAlbumId());
        assertEquals(audioTrack.getAuthorProfileId(), uploadedTrack.getAuthorProfileId());
        assertEquals(audioTrack.getUploaderProfileId(), uploadedTrack.getUploaderProfileId());
        assertEquals(uploadedTrack.getTrackId(), uploadedTrack.getUploaderTrackId());
        assertEquals(uploadedTrack.getAlbumId(), uploadedTrack.getUploaderAlbumId());
        assertEquals(audioTrack.getTrackId(), audioTrack.getUploaderTrackId());
        assertEquals(audioTrack.getAlbumId(), audioTrack.getUploaderAlbumId());
    }

    @Test
    public void testUserUploadAudioTrack() throws URISyntaxException, IOException, PermissionException, NotFoundException, InvalidAudioFrameException, ReadOnlyFileException, CannotReadException, TagException {
		File dstFile = null;

        try {

            File testFile = getFileFromClasspath("demo2.mp3");
            dstFile = new File("testfile.mp3");
            FileUtils.copyFile(testFile, dstFile);

			long profileId = insertUserProfile().getProfileId();

            AudioFile uploadedFile = audioContentService.uploadAudioTrackFile(dstFile, dstFile.getName(), profileId, profileId, 0);
            assertTrue(uploadedFile.getFile().exists());

            AudioTrack uploadedTrack = audioTrackDAO.selectAudioTracks(profileId, 0, 10).get(0);

            File testFile2 =  getFileFromClasspath("demo.mp3");
            dstFile = new File("testfile2.mp3");
            FileUtils.copyFile(testFile2, dstFile);
            audioContentService.uploadAudioTrackFile(dstFile, dstFile.getName(), profileId, profileId, 0);
            AudioTrack uploadedTrack2 = audioTrackDAO.selectAudioTracks(profileId, 0, 10).get(0);

            assertEquals(1, uploadedTrack2.getAlbumTrackNumber());
            assertEquals(2, audioTrackDAO.selectAudioTracks(profileId, 0, 10).get(1).getAlbumTrackNumber());
            assertEquals(audioTrackDAO.selectAudioTracks(profileId, 0, 10).get(1).getTrackId(), uploadedTrack.getTrackId());
            assertEquals(audioTrackDAO.selectAudioTracks(profileId, 0, 10).get(0).getTrackId(), uploadedTrack2.getTrackId());

            assertEquals(2, audioTrackDAO.selectAudioTracks(profileId, 0, 10).get(1).getAlbumTrackNumber());
            assertEquals(audioTrackDAO.selectAudioTracks(profileId, 0, 10).get(1).getTrackId(), uploadedTrack.getTrackId());
            assertEquals(audioTrackDAO.selectAudioTracks(profileId, 0, 10).get(0).getTrackId(), uploadedTrack2.getTrackId());

            File testFile3 = getFileFromClasspath("demo.mp3");
            dstFile = new File("testfile3.mp3");
            FileUtils.copyFile(testFile3, dstFile);
            audioContentService.uploadAudioTrackFile(dstFile, dstFile.getName(), profileId, profileId, 0);
            AudioTrack uploadedTrack3 = audioTrackDAO.selectAudioTracks(profileId, 0, 10).get(0);

            assertEquals(1, uploadedTrack3.getAlbumTrackNumber());
            assertEquals(2, audioTrackDAO.selectAudioTracks(profileId, 0, 10).get(1).getAlbumTrackNumber());
            assertEquals(audioTrackDAO.selectAudioTracks(profileId, 0, 10).get(0).getTrackId(), uploadedTrack3.getTrackId());
            assertEquals(audioTrackDAO.selectAudioTracks(profileId, 0, 10).get(1).getTrackId(), uploadedTrack2.getTrackId());
            assertEquals(audioTrackDAO.selectAudioTracks(profileId, 0, 10).get(2).getTrackId(), uploadedTrack.getTrackId());

        } finally {
            FileUtils.deleteQuietly(dstFile);
        }
    }


    @Test
    public void testMp3ConvertationAudioTrack() throws Exception {

		File dstFile = null;

        try {

			File testFile = getFileFromClasspath("flac_file.flac");
			dstFile = new File("testfile.flac");
			FileUtils.copyFile(testFile, dstFile);

			long profileId = insertUserProfile().getProfileId();
			long audioAlbumId = 0;

            AudioFile uploadedAudioFile = audioContentService.uploadAudioTrackFile(dstFile, dstFile.getName(), profileId, profileId, audioAlbumId);
            File uploadedFile = uploadedAudioFile.getFile();
            // check file uploaded
            assertTrue(uploadedFile.exists());

            AudioTrack uploadedTrack = audioTrackDAO.selectAudioTracks(profileId, 0, 10).get(0);
            assertFalse(uploadedTrack.isHasMp3());

            //call synchronized method to test
            audioContentService.convertAudio(uploadedAudioFile);
            //check convertation results
            File mp3File = new File(uploadedFile.getPath().substring(0, uploadedFile.getPath().lastIndexOf('.')) + ".mp3");
            assertTrue(mp3File.exists());
            uploadedTrack = audioTrackDAO.selectAudioTracks(profileId, 0, 10).get(0);
            assertTrue(uploadedTrack.isHasMp3());

            assertEquals(profileId, uploadedTrack.getProfileId());
            assertEquals("The Beatles", uploadedTrack.getArtistName());
            assertEquals("Yesterday", uploadedTrack.getTrackName());
            assertEquals("", uploadedTrack.getAuthors());
            assertEquals(126, uploadedTrack.getTrackDuration());
            assertEquals(audioAlbumId, uploadedTrack.getAlbumId());
            assertEquals(profileId, uploadedTrack.getAuthorProfileId());
            assertEquals(profileId, uploadedTrack.getUploaderProfileId());
            assertEquals(uploadedTrack.getTrackId(), uploadedTrack.getUploaderTrackId());
            assertEquals(uploadedTrack.getAlbumId(), uploadedTrack.getUploaderAlbumId());
        } finally {
            FileUtils.deleteQuietly(dstFile);
        }
    }

    @Test
    public void testMp3ConvertationFromWavFile() throws Exception {
        File dstFile = null;
        try {
            File testFile = getFileFromClasspath("wav_demo.wav");
            dstFile = new File("testfileWav.wav");
            FileUtils.copyFile(testFile, dstFile);
            long profileId = insertUserProfile().getProfileId();
            long audioAlbumId = 0;

            AudioFile uploadedAudioFile = audioContentService.uploadAudioTrackFile(dstFile, dstFile.getName(), profileId, profileId, audioAlbumId);
            File uploadedFile = uploadedAudioFile.getFile();
            // check file uploaded
            assertTrue(uploadedFile.exists());

            AudioTrack uploadedTrack = audioTrackDAO.selectAudioTracks(profileId, 0, 10).get(0);
            assertFalse(uploadedTrack.isHasMp3());

            //call synchronized method to test
            audioContentService.convertAudio(uploadedAudioFile);
            //check convertation results
            File mp3File = new File(uploadedFile.getPath().substring(0, uploadedFile.getPath().lastIndexOf('.')) + ".mp3");
            assertTrue(mp3File.exists());
            uploadedTrack = audioTrackDAO.selectAudioTracks(profileId, 0, 10).get(0);
            assertEquals("testfileWav", uploadedTrack.getTrackName());
            assertTrue(uploadedTrack.isHasMp3());
        } finally {
            FileUtils.deleteQuietly(dstFile);
        }
    }

    @Test
    public void testAlbumDownloadingOperations() throws Exception {
		File dstFile = null;

        try {

            File testFile = getFileFromClasspath("demo2.mp3");
            dstFile = new File("testfile.mp3");
            FileUtils.copyFile(testFile, dstFile);

            long profileId = insertUserProfile().getProfileId();
			long audioAlbumId = 0;

            File uploadedFile = audioContentService.uploadAudioTrackFile(dstFile, dstFile.getName(), profileId, profileId, audioAlbumId).getFile();
            assertTrue(uploadedFile.exists());
            AudioTrack uploadedTrack = audioTrackDAO.selectAudioTracks(profileId, 0, 10).get(0);

            assertEquals(profileId, uploadedTrack.getProfileId());
            assertEquals(audioAlbumId, uploadedTrack.getAlbumId());
            assertEquals(profileId, uploadedTrack.getAuthorProfileId());

			File audioTrackFile = audioContentService.getAudioTrackFile(uploadedTrack);
			assertNotNull(audioTrackFile);
			assertTrue(audioTrackFile.exists());

        } finally {
            FileUtils.deleteQuietly(dstFile);
        }
    }

	@Test
	public void testTagsEncoding() throws URISyntaxException, IOException, InvalidAudioFrameException, ReadOnlyFileException, PermissionException, CannotReadException, TagException {
		File dstFile = null;

		try {
			long profileId = insertUserProfile().getProfileId();
			long audioAlbumId = 0;

			//File contains tags UTF32 and cp1251
			File testFile = getFileFromClasspath("tags_id3v2x_cp1251.mp3");
			dstFile = new File("id3v2x_cp1251.mp3");
			FileUtils.copyFile(testFile, dstFile);

            audioContentService.uploadAudioTrackFile(dstFile, dstFile.getName(), profileId, profileId, audioAlbumId).getFile();
            AudioTrack uploadedTrack = audioTrackDAO.selectAudioTracks(profileId, 0, 10).get(0);

            //artist name "Мантра" was UTF32-encoded
            assertEquals("\u041c\u0430\u043d\u0442\u0440\u0430", uploadedTrack.getArtistName());
            //title "Маха мантра Харе Кришна" was cp1251-encoded
            assertEquals("\u041c\u0430\u0445\u0430 \u043c\u0430\u043d\u0442\u0440\u0430 \u0425\u0430\u0440\u0435 \u041a\u0440\u0438\u0448\u043d\u0430", uploadedTrack.getTrackName());


			//File contains tags with special symbols
			testFile = getFileFromClasspath("tags_id3v2x_sp_sym.mp3");
			dstFile = new File("id3v2x_sp_sym.mp3");
			FileUtils.copyFile(testFile, dstFile);

            audioContentService.uploadAudioTrackFile(dstFile, dstFile.getName(), profileId, profileId, audioAlbumId).getFile();
            uploadedTrack = audioTrackDAO.selectAudioTracks(profileId, 0, 10).get(0);

            //Title and artist tag are "ॐ"
            assertEquals("\u0950", uploadedTrack.getArtistName());
            assertEquals("\u0950", uploadedTrack.getTrackName());

        } finally {
            FileUtils.deleteQuietly(dstFile);
        }
    }
}

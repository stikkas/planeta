package ru.planeta.test;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import ru.planeta.commons.model.Gender;
import ru.planeta.dao.TransactionScope;
import ru.planeta.dao.commondb.UserPrivateInfoDAO;
import ru.planeta.dao.profiledb.ProfileDAO;
import ru.planeta.model.common.UserPrivateInfo;
import ru.planeta.model.enums.GroupCategory;
import ru.planeta.model.enums.ProfileStatus;
import ru.planeta.model.enums.ProfileType;
import ru.planeta.model.enums.UserStatus;
import ru.planeta.model.profile.Profile;
import ru.planeta.service.*;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.EnumSet;

/**
 * @author pvyazankin
 * @since 17.10.12 14:30
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:spring/applicationContext-common.xml", "classpath*:spring/applicationContext-dao.xml"/*, "classpath*:spring/applicationContext-global.xml"*/, "classpath*:/spring/applicationContext-geo.xml"})
public abstract class AbstractTest {

    private static Logger log = Logger.getLogger(AbstractTest.class);

    private TransactionScope transactionScope;

    @Before
    public void setUp() throws Exception {
        transactionScope = TransactionScope.createOrGetCurrentTest();
    }

    @After
    public void tearDown() throws Exception {
        transactionScope.close();
    }


    @Autowired
    protected ApplicationContext context;
    @Autowired
    protected ProfileDAO profileDAO;
    @Autowired
    private ImageContentServiceImpl imageContentService;
    @Autowired
    private VideoContentServiceImpl videoContentService;
    @Autowired
    private AudioContentServiceImpl audioContentService;
    @Autowired
    private ThumbGenerationServiceImpl thumbGenerationService;
    @Autowired
    private ProxyContentServiceImpl proxyContentService;
	@Autowired
	private ProfileFileContentServiceImpl profileFileContentService;

	@Autowired
	protected UserPrivateInfoDAO userPrivateInfoDAO;


    @Value("${images.test.path}")
    private String imagesTestPath;
    @Value("${videos.test.path}")
    private String videosTestPath;
    @Value("${audiotracks.test.path}")
    private String audiotracksTestPath;
	@Value("${files.test.path}")
	private String filesTestPath;
    @Value("${proxy.test.directory}")
    private String proxyTestDir;
    @Value("${tempDirectoryPathTest}")
    private String tempDirectoryPathTest;


    static {
        System.setProperty("port", "8091");
        System.setProperty("appid", "uploader");
        System.setProperty("ip", "192.168.10.130");
    }

    @PostConstruct
    public void init() throws IOException {
        imageContentService.setImagesPath(imagesTestPath);
        videoContentService.setVideosPath(videosTestPath);
        audioContentService.setAudioTracksPath(audiotracksTestPath);
        audioContentService.setTempDirectoryPath(tempDirectoryPathTest);
		profileFileContentService.setFilesPath(filesTestPath);
        thumbGenerationService.setImagesPath(imagesTestPath);
        proxyContentService.setProxyRootDir(proxyTestDir);
    }

    public Profile insertUserProfile() {
        Profile profile = new Profile();
        profile.setProfileType(ProfileType.USER);
        profile.setImageUrl("/imageurl");
        profile.setImageId(1);
        profile.setDisplayName("displayName");
        profile.setAlias("alias");
        profile.setStatus(ProfileStatus.NOT_SET);
        profile.setCityId(1);
        profile.setCountryId(1);
        profile.setUserBirthDate(new Date());
        profile.setUserGender(Gender.NOT_SET);

        profileDAO.insert(profile);
        return profile;
    }

	protected UserPrivateInfo insertUserPrivateInfo(String email, String password) {
		Profile user = insertUserProfile();
		UserPrivateInfo userPrivateInfo = new UserPrivateInfo();
		userPrivateInfo.setUserId(user.getProfileId());
		userPrivateInfo.setEmail(email.trim().toLowerCase());
        userPrivateInfo.setUsername(userPrivateInfo.getEmail());
		userPrivateInfo.setPassword(password);
		userPrivateInfo.setUserStatus(EnumSet.of(UserStatus.USER));

		userPrivateInfoDAO.insert(userPrivateInfo);
		return userPrivateInfo;
	}


	protected UserPrivateInfo insertPlanetaAdminPrivateInfo(String email, String password) {
		UserPrivateInfo info = insertUserPrivateInfo(email, password);
		info.setUserStatus(EnumSet.of(UserStatus.ADMIN));
		userPrivateInfoDAO.update(info);
		return info;
	}

	protected UserPrivateInfo insertPlanetaAdminPrivateInfo() {
		String timeStamp = String.valueOf(Calendar.getInstance().getTimeInMillis()) + String.valueOf((int) (100 * Math.random()));
		String email = String.format("junit.admin%s@test.ee", timeStamp);

		return insertPlanetaAdminPrivateInfo(email, timeStamp);
	}

    /**
     * Cleans directories used while testing
     */
    public static void cleanUp() throws Exception {
        File staticDir = new File(".\\planeta_static");
        File tmpDir = new File(".\\tmp");

        if (staticDir.exists()) {
            log.info("Cleaning 'planeta_static' directory");
            FileUtils.deleteDirectory(staticDir);
            log.info("Directory 'planeta_static' has been cleaned");
        }

        if (tmpDir.exists()) {
            log.info("Cleaning 'tmp' directory");
            FileUtils.deleteDirectory(tmpDir);
            log.info("Directory 'tmp' has been cleaned");
        }
    }

    public Profile insertGroupProfile() {
        Profile profile = new Profile();
        profile.setProfileType(ProfileType.GROUP);
		profile.setGroupCategory(GroupCategory.OTHER);
        profile.setImageUrl("/imageurl");
        profile.setImageId(1);
        profile.setDisplayName("displayName");
        profile.setAlias("alias");
        profile.setStatus(ProfileStatus.NOT_SET);
        profile.setCityId(1);
        profile.setCountryId(1);
        profileDAO.insert(profile);
        return profile;
    }

    protected static File getFileFromClasspath(String path) {
        return new File(ClassLoader.getSystemClassLoader().getResource(path).getFile());
    }
}

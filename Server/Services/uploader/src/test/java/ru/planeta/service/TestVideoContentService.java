package ru.planeta.service;

import org.apache.commons.io.FileUtils;
import org.im4java.core.IM4JavaException;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import ru.planeta.api.exceptions.NotFoundException;
import ru.planeta.api.exceptions.PermissionException;
import ru.planeta.dao.profiledb.VideoDAO;
import ru.planeta.domain.VideoFile;
import ru.planeta.image.ImageOperationException;
import ru.planeta.model.profile.media.Video;
import ru.planeta.model.profile.media.enums.VideoConversionStatus;
import ru.planeta.model.profile.media.enums.VideoQuality;
import ru.planeta.test.BaseTestSet;
import ru.planeta.video.VideoConfiguration;
import ru.planeta.video.VideoConversionConfiguration;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;

import static org.junit.Assert.*;

/**
 * @author ds.kolyshev
 *         Date: 28.10.11
 */
public class TestVideoContentService extends BaseTestSet {

    @Autowired
    private VideoDAO videoDAO;
    @Autowired
    private VideoContentService videoContentService;
    @Autowired
    private VideoConversionConfiguration videoConversionConfiguration;

    @Test
    public void testUploadVideo() throws URISyntaxException, IOException, InterruptedException, PermissionException, ImageOperationException, NotFoundException, IM4JavaException {

        File dstFile = null;

        try {
            File testFile = getFileFromClasspath("demo.3gp");

            dstFile = new File("testfile.3gp");
            FileUtils.copyFile(testFile, dstFile);

            long profileId = insertUserProfile().getProfileId();
            VideoFile videoFile = videoContentService.createVideo(dstFile, dstFile.getName(), profileId, profileId, true);

            File uploadedFile = videoFile.getFile();
            //check file uploaded
            assertTrue(uploadedFile.exists());

            //check inserted db record
            Video video = videoDAO.selectVideoById(profileId, videoFile.getVideo().getVideoId());
            assertNotNull(video);
            assertEquals(profileId, video.getProfileId());
            assertNotNull(video.getVideoUrl());
            assertEquals(VideoConversionStatus.PROCESSING, video.getStatus());
            assertEquals(9, video.getDuration());

            //check converts
            //call synchronized method to test
            videoContentService.convertVideo(profileId, videoFile);
            //check results
            checkConversionResults(videoFile);

            video = videoDAO.selectVideoById(profileId, videoFile.getVideo().getVideoId());
            assertNotNull(video);
            assertEquals(profileId, video.getProfileId());
            assertEquals(profileId, video.getAuthorProfileId());
            assertNotNull(video.getVideoUrl());
            assertEquals(VideoConversionStatus.PROCESSED, video.getStatus());
            assertTrue(video.getAvailableQualities().contains(VideoQuality.MODE_240P));
            assertTrue(video.getImageId() > 0);
            assertNotNull(video.getImageUrl());
            assertNotNull(video.getStoryboardUrl());

            //recapture screenshot
            //videoContentService.recaptureScreenShot(clientId, profileId, videoFile.getVideo().getVideoId(), "1");

        } finally {
            FileUtils.deleteQuietly(dstFile);
        }
    }

    /**
     * Checks video convertation results
     *
     * @param videoFile video to convert
     */
    private void checkConversionResults(VideoFile videoFile) {
        for (VideoConfiguration videoConfiguration : videoConversionConfiguration.getVideoConfigurations()) {

            String convertResultPath = VideoContentServiceImpl.generateConvertedVideoFilePath(videoFile.getFile(), videoConfiguration);
            File targetFile = new File(convertResultPath);

            if (videoConfiguration.compareTo(videoFile.getOriginalVideoInfo()) >= 0
                    || videoConfiguration == videoConversionConfiguration.getDefaultConfiguration()) {
                assertTrue(targetFile.exists());
            } else {
                assertFalse(targetFile.exists());
            }

            if (targetFile.exists()) {
                boolean deleted = targetFile.delete();
                assertTrue(deleted);
            }
        }
    }
}

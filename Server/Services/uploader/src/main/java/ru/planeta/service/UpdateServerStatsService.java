package ru.planeta.service;

import org.apache.commons.io.FileSystemUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import ru.planeta.dao.statdb.StatStaticNodeDAO;
import ru.planeta.model.stat.StatStaticNode;

import java.io.IOException;
import java.util.Date;

/**
 * @author pvyazankin
 * @since 02.11.12 11:19
 */
@Service
@Lazy(value = false)
public class UpdateServerStatsService {

    private static final Logger log = Logger.getLogger(UpdateServerStatsService.class);

    private boolean isUploader = false;
    private final StatStaticNodeDAO statStaticNodeDAO;
    @Value("${static.node}")
    private String currentNode;

    @Value("${application.host}")
    private String appHost;

    @Value("${images.path}")
    private String imagesPath;

    @Autowired
    public UpdateServerStatsService(StatStaticNodeDAO statStaticNodeDAO) {
        this.statStaticNodeDAO = statStaticNodeDAO;
    }

    @Scheduled(fixedDelay = 1000 * 60 * 30, initialDelay = 1000 * 60) // every 30 minutes
    public void updateServerStats() throws IOException {
        if (!isUploader) {
            return;
        }
        StatStaticNode node = new StatStaticNode();

        node.setDomain(currentNode);
        node.setTimeUpdated(new Date());
        if(!appHost.contains("localhost")) {
            node.setFreeSpaceKB(FileSystemUtils.freeSpaceKb(imagesPath));
        } else {
            node.setFreeSpaceKB(FileSystemUtils.freeSpaceKb());
        }

        if (!currentNode.contains("localhost") && !currentNode.contains("127.0.0.1")) {
            statStaticNodeDAO.save(node);
        }
        log.info("Static node '" + node + " has been updated.");
    }

    public void setUploader(boolean uploader) {
        isUploader = uploader;
    }
}

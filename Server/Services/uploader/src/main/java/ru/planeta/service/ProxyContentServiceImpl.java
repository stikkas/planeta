package ru.planeta.service;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;
import org.im4java.core.IM4JavaException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import ru.planeta.api.exceptions.NotFoundException;
import ru.planeta.commons.concurrent.LockManager;
import ru.planeta.commons.web.WebUtils;
import ru.planeta.image.BucketType;
import ru.planeta.image.ImageInfo;
import ru.planeta.image.ImageOperationException;
import ru.planeta.model.Constants;
import ru.planeta.utils.ThumbConfiguration;

import javax.annotation.Nonnull;
import javax.annotation.PostConstruct;
import java.io.*;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLDecoder;
import java.util.HashMap;
import java.util.Map;

/**
 * @author a.savanovich
 * @since 08.02.12 17:26
 */
@Service
@Lazy(false)
public class ProxyContentServiceImpl extends BaseService implements ProxyContentService {

    @Autowired
    private ThumbGenerationService thumbGenerationService;
//    @Autowired
//    private StaticNodesService staticNodesService;

    private String proxyRootDir;
    private String relativeDirPrefix;
    private LockManager lockManager = new LockManager();
    private static final String EXIST_SUFFIX = "_exist";

    @PostConstruct
    public void init() throws Exception {
        File proxyRoot = new File(proxyRootDir);
        if (!proxyRoot.exists()) {
            if (!proxyRoot.mkdirs()) {
                throw new RuntimeException("Can't create all required directories " + proxyRoot.getAbsolutePath());
            }
        }
    }

    @Value("${proxy.directory}")
    public void setProxyRootDir(String proxyRootDir) {
        this.proxyRootDir = proxyRootDir;
    }

    @Value("${proxy.relative.dir}")
    public void setRelativeDirPrefix(String relativeDirPrefix) {
        this.relativeDirPrefix = relativeDirPrefix;
    }

    private String getProxyRootDir() {
        return proxyRootDir;
    }

    private String getRelativeDirPrefix() {
        return relativeDirPrefix;
    }

    private ThumbGenerationService getThumbGenerationService() {
        return thumbGenerationService;
    }

    private Map<String, FileNamesAndDir> map = new HashMap<>();


    @Override
    public String getRelativePath(@Nonnull final String url, ThumbConfiguration config, boolean flushCashe) throws IOException, ImageOperationException, IM4JavaException, InterruptedException, NotFoundException, URISyntaxException {
        if (url.isEmpty()) {
            throw new FileNotFoundException("Empty urls not supported");
        }
        final String dirSuffix = getDirFromUrl(url);
        final String dirName = getProxyRootDir() + dirSuffix;
        final String relativeDir = getRelativeDirPrefix() + dirSuffix;   // вычислена из урла

        lockManager.acquireSharedLock(url);
        try {
            FileNamesAndDir value = getFileNameAndDir(url);
            if (!flushCashe && value.getDirName() != null) {
                String realFileName = value.contains(getFileName(config));  // выкачивать уже не нужно, но нужно сгенерировать thumb по новой конфигурации
                if (realFileName != null) {
                    return value.getDirName() + realFileName;   // ок!
                }
            }
            lockManager.acquireExclusiveLock(url);
            try {
                return relativeDir + searchOnDiskOrCreate(value, config, url, relativeDir, dirName, flushCashe);
            } finally {
                lockManager.releaseExclusiveLock(url);
            }
        } finally {
            lockManager.release(url);
        }
    }

    /**
     * Returns generated file names and dir for external URLl<br />
     * If not found FileNamesAndDir#getDirName == null.
     */
    private synchronized FileNamesAndDir getFileNameAndDir(String url) {
        FileNamesAndDir value = map.get(url);
        if (value == null) {
            value = new FileNamesAndDir();
            map.put(url, value);
        }
        return value;
    }

    private static String changeValueInMap(String dirName, ThumbConfiguration config, FileNamesAndDir value) throws IOException {
        String realFileName = getRealFileName(dirName, config);
        value.add(getFileName(config), realFileName);
        return realFileName;
    }


    /**
     * Searches real file on disk, if there are no link in memory.
     */
    private String searchOnDiskOrCreate(FileNamesAndDir value, ThumbConfiguration config, @Nonnull String url, String relativeDir, String dirName, boolean flushCache) throws IOException, ImageOperationException, IM4JavaException, InterruptedException {
        final String realFileName;
        if (!flushCache) {
            if (value.getDirName() != null) {
                if (isExist(dirName, config)) {
                    realFileName = changeValueInMap(dirName, config, value);
                    return realFileName;
                }
            } else {
                value = new FileNamesAndDir();
                if (isExist(dirName, config)) {
                    value.setDirName(relativeDir);
                    realFileName = changeValueInMap(dirName, config, value);
                    return realFileName;
                }
            }
        }
        // create if doesn't exist
        realFileName = createNewFile(config, dirName, value, relativeDir, url, flushCache);
        return realFileName;
    }

    private static String decodeUrl(String url) {
        try {
            return URLDecoder.decode(StringEscapeUtils.unescapeJava(url));
        } catch (Exception ex) {
            return URLDecoder.decode(url);
        }
    }

    private static File getOriginalFile(String dirName, ThumbConfiguration original, @Nonnull String url) throws IOException, InterruptedException, IM4JavaException {
        File originalFile = new File(dirName + getFileName(original));

        File localDir = new File(dirName);
        if (!localDir.mkdirs()) {
            if (localDir.exists()) {
                log.debug(localDir.getAbsolutePath() + " already exists");
            } else {
                log.error("Can't create dir " + dirName);
            }
        }

        String decodeUrl = decodeUrl(url);
//        if (staticNodesService.isStaticServerPath(url)) {
//            decodeUrl = WebUtils.appendProtocolIfNecessary(url, false);
//        }

        FileUtils.copyURLToFile(new URL(decodeUrl), originalFile, Constants.INSTANCE.getCONNECTION_TIMEOUT_MILLISECONDS(), Constants.INSTANCE.getREAD_TIMEOUT_MILLISECONDS());

        // rename original
        ImageInfo info = new ImageInfo(originalFile);
        File originalNewFile = new File(dirName + originalFile.getName() + "." + info.getBucketType().getExtension());
        if (!originalFile.renameTo(originalNewFile)) {
            throw new IOException("Can't rename file " + originalFile.getName());
        }
        originalFile = originalNewFile;
        createFlagFile(dirName, original, originalFile);

        return originalFile;
    }

    private static void createFlagFile(String dirName, ThumbConfiguration config, File destinationFile) throws IOException {
        FileUtils.writeStringToFile(new File(dirName + getFileName(config) + EXIST_SUFFIX), destinationFile.getName());
    }

    /**
     * Creating new file with required parameters
     */
    private String createNewFile(ThumbConfiguration config, String dirName, FileNamesAndDir fileNamesAndDir, String relativeDir, @Nonnull String url, boolean flushCache) throws IOException, ImageOperationException, InterruptedException, IM4JavaException {
        final File originalFile = createOriginalFile(dirName, fileNamesAndDir, relativeDir, url, flushCache);
        File destinationFile = generateThumbnail(dirName, originalFile, config);
        fileNamesAndDir.add(getFileName(config), destinationFile.getName());
        createFlagFile(dirName, config, destinationFile);
        return destinationFile.getName();
    }

    private static File createOriginalFile(String dirName, FileNamesAndDir fileNamesAndDir, String relativeDir, @Nonnull String url, boolean flushCache) throws IOException, InterruptedException, IM4JavaException {
        ThumbConfiguration original = new ThumbConfiguration();
        final File originalFile;

        if (flushCache) {
            originalFile = getOriginalFile(dirName, original, url);
            fileNamesAndDir.setDirName(relativeDir);
            fileNamesAndDir.add(getFileName(original), originalFile.getName());
            // if dir not found (?)
        } else if (!StringUtils.isEmpty(fileNamesAndDir.getDirName())) {
            originalFile = new File(dirName + fileNamesAndDir.contains(getFileName(original)));

            // if file doesn't exist
        } else if (!isExist(dirName, original)) {
            originalFile = getOriginalFile(dirName, original, url);
            fileNamesAndDir.setDirName(relativeDir);
            fileNamesAndDir.add(getFileName(original), originalFile.getName());

            //
        } else {
            fileNamesAndDir.setDirName(relativeDir);
            originalFile = new File(dirName + changeValueInMap(dirName, original, fileNamesAndDir));
        }
        return originalFile;
    }

    private File generateThumbnail(String dirName, @Nonnull File originalFile, ThumbConfiguration config) throws IOException, ImageOperationException, IM4JavaException, InterruptedException {
        ImageInfo info = new ImageInfo(originalFile);
        String ext = ".jpg";
        if (info.getBucketType() == BucketType.GIF) {
            ext = ".gif";
        }
        File file = new File(dirName + getFileName(config) + ext);
        return getThumbGenerationService().generateOneThumbnail(originalFile, file, config, true);
    }

    static String getDirFromUrl(String url) {

        StringBuilder sb = new StringBuilder();
        String host = WebUtils.getHost(url);
        String clippedUrl = getClippedUrl(url);
        hexShaDirString(host, sb);
        hexShaDirString(clippedUrl, sb);
        return sb.toString();
    }

    private static String getClippedUrl(String str) {
        int beginIndex = 0;
        int httpIndex = str.indexOf("http://");
        if (httpIndex >= 0) {
            beginIndex = httpIndex + 7;
        } else {
            int httpsIndex = str.indexOf("https://");
            if (httpsIndex >= 0) {
                beginIndex = httpsIndex + 8;
            }
        }
        int wwwIndex = str.indexOf("www.");
        if (wwwIndex >= 0) {
            beginIndex = wwwIndex + 4;
        }
        int endIndex = str.indexOf('/', beginIndex);
        if (endIndex > 0) {
            return str.substring(endIndex);
        }
        return str.substring(beginIndex);
    }

    private static void hexShaDirString(String str, StringBuilder sb) {
        byte[] byteData = DigestUtils.sha256(str);
        for (int i = 0; i < byteData.length; i++) {
            String hex = Integer.toHexString(0xff & byteData[i]);
            if (hex.length() == 1) {
                sb.append('0');
            }
            sb.append(hex);
            if (i % 4 == 3) {
                sb.append(System.getProperty("file.separator"));
            }
        }
    }

    private static String getFileName(ThumbConfiguration config) {
        String result = config.getHeight() + "_" + config.getWidth();
        if (config.isCrop()) {
            result += "_crop";
        }
        if (config.isPad()) {
            result += "_pad";
        }
        if (config.isDisableAnimatedGif()) {
            result += "_nogif";
        }
        if (config.isBackgroundColorSet()) {
            result += "_bgcolor" + config.getBackgroundColor().replaceAll("[^0-9a-zA-Z]", "");
        }
        return result;
    }

    private static String getRealFileName(String dirName, ThumbConfiguration config) throws IOException {
        try (BufferedReader reader = new BufferedReader(new FileReader(dirName + getFileName(config) + EXIST_SUFFIX))) {
            return reader.readLine();
        }
    }

    /**
     * Checks is file with required config exists in required dirName
     */
    private static boolean isExist(String dirName, ThumbConfiguration config) {
        String filename = getFileName(config) + EXIST_SUFFIX;
        File file = new File(dirName + filename);
        return file.exists();
    }

    private static class FileNamesAndDir {
        private String dirName;
        private Map<String, String> names;

        public String getDirName() {
            return dirName;
        }

        public void setDirName(String dirName) {
            this.dirName = dirName;
        }

        public String contains(String name) {
            return names.get(name);
        }

        public void add(String key, String value) {
            names.put(key, value);
        }

        public FileNamesAndDir() {
            names = new HashMap<>();
        }
    }
}

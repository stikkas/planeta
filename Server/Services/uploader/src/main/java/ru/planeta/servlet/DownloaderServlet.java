package ru.planeta.servlet;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.log4j.Logger;
import org.im4java.core.IM4JavaException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Controller;
import ru.planeta.api.exceptions.NotFoundException;
import ru.planeta.api.service.configurations.StaticNodesService;
import ru.planeta.commons.web.WebUtils;
import ru.planeta.image.ImageOperationException;
import ru.planeta.image.StringParameters;
import ru.planeta.service.ProxyContentService;
import ru.planeta.service.StubThumbService;
import ru.planeta.utils.ThumbConfiguration;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;

import static org.springframework.http.HttpHeaders.CONTENT_TYPE;

/**
 * Downloader servlet
 *
 * @author ds.kolyshev
 *         Date: 11.01.12
 */
@Controller
public class DownloaderServlet extends BaseServlet {

    private static final Logger log = Logger.getLogger(DownloaderServlet.class);

    private static final String LIVE_CHECK = "/live-check";
    private final StubThumbService stubThumbService;
    private final StaticNodesService staticNodesService;
    private final ProxyContentService proxyContentService;

    @Value("${proxy.url}")
    private String proxyUrl;
    @Value("${videostub.url}")
    private String videoStubUrl;
    @Value("${audiostub.url}")
    private String audioStubUrl;

    @Autowired
    public DownloaderServlet(StubThumbService stubThumbService, StaticNodesService staticNodesService, ProxyContentService proxyContentService) {
        this.stubThumbService = stubThumbService;
        this.staticNodesService = staticNodesService;
        this.proxyContentService = proxyContentService;
    }


    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws IOException {
        try {
            String uri = req.getRequestURI();
            if (uri == null) {
                log.error("No such api, method group or method:\n" + WebUtils.requestDataToString(req));
                resp.sendError(HttpServletResponse.SC_BAD_REQUEST, "No such api, method group or method");
            } else if (uri.equalsIgnoreCase(LIVE_CHECK)) {
                WebUtils.setDefaultHeaders(resp);
                resp.getWriter().write("OK");
                resp.getWriter().flush();
            } else if (uri.equalsIgnoreCase(CROSSDOMAIN_XML_KEY)) {
                writeResponseXml(resp, CROSS_DOMAIN_XML);
            } else if (uri.startsWith(getAudioTracksUrl())
                    || uri.startsWith(getImagesUrl())
                    || uri.startsWith(getVideosUrl())
                    || uri.startsWith(getPromosUrl())
                    || uri.startsWith(getFilesUrl())) {
                writeResponseXAccelRedirect(resp, req);
            } else if (uri.startsWith(proxyUrl)) {
                writeProxyResponseXAccelRedirect(resp, req);
            } else if (uri.startsWith(videoStubUrl)) {
                generateVideoStub(resp, req);
            } else if (uri.startsWith(audioStubUrl)) {
                generateAudioStub(resp, req);
            } else {
                resp.sendError(HttpServletResponse.SC_NOT_FOUND, "No such api, method group or method");
            }
        } catch (Exception ex) {
            log.error("Error processing request:\n" + WebUtils.requestDataToString(req), ex);
            resp.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, ex.getMessage());
        }
    }

    /**
     * @param resp 200 OK response with Content-Type: image/...
     * @param req  thumbnail.png request
     */
    private void generateVideoStub(HttpServletResponse resp, HttpServletRequest req) throws NotFoundException {
        String name = getRequestParameter(req, "name"); // optional
        if (name == null) {
            name = "";
        }
        String duration = getRequestParameter(req, "duration");
        String url = getRequestParameter(req, "url"); // required
        String videoIdStr = getRequestParameter(req, "videoId");
        String profileIdStr = getRequestParameter(req, "profileId");
        String config = getRequestParameter(req, "config");

        if(StringUtils.isEmpty(url)) {
            if(StringUtils.isEmpty(videoIdStr) || StringUtils.isEmpty(profileIdStr)) {
                throw new NotFoundException("url.required");
            }
            url = getVideoContentService().getVideoImageUrl(Long.valueOf(profileIdStr), Long.valueOf(videoIdStr));
            log.debug("videoStubUrl: " + url);

        }

        final String overlayUrl = WebUtils.appendProtocolIfNecessary(staticNodesService.getResourceHost() + "/images/bb-video-big.png", false);

        resp.setStatus(HttpServletResponse.SC_OK);

//        resp.setHeader("X-Accel-Redirect", image);
        try {
            final StringParameters params;
            if(StringUtils.isNotBlank(config) && config.equals(StringParameters.CAMPAIGN_EDITOR_CONFIG_KEY)) {
                params = StringParameters.campaignDrawVideoSettings();
            } else {
                params = StringParameters.defaultDrawVideoSettings();
            }
            resp.setHeader(CONTENT_TYPE, "image/" + params.imageExtension);

            OutputStream out = (resp.getOutputStream());
            stubThumbService.drawVideoToStream(url, overlayUrl, name, duration, out, params);
            out.flush();
        } catch (Exception e) {
            log.warn(e);
        }

    }

    private void generateAudioStub(HttpServletResponse resp, HttpServletRequest req) throws Exception {
        String name = getRequestParameter(req, "name"); // required
        String duration = getRequestParameter(req, "duration"); // required
        if (duration == null) {
            log.warn("No duration\n request:\n" + WebUtils.requestDataToString(req));
            // proxyNoContent(resp);
            // return;
        }
        String artist = getRequestParameter(req, "artist"); // optional
        if (artist == null) {
            artist = "";
        }

        String widthStr = getRequestParameter(req, "width"); // optional
        int width = 0;
        if (Integer.valueOf(widthStr) != null) {
            width = Integer.valueOf(widthStr);
        }

        final String playUrl = WebUtils.appendProtocolIfNecessary(staticNodesService.getResourceHost() + "/images/icon/aac-play.png", false);

        resp.setStatus(HttpServletResponse.SC_OK);
//        resp.setHeader("X-Accel-Redirect", image);
        try {
            StringParameters params = StringParameters.defaultDrawAudioSettings();
            if (width > params.stubWidth) {
                params.stubWidth = width;
            }
            resp.setHeader(HttpHeaders.CONTENT_TYPE, "image/" + params.imageExtension);
            OutputStream out = (resp.getOutputStream());
            stubThumbService.drawAudioToStream(playUrl, name, artist, duration, params, out);
            out.flush();
        } catch (IOException e) {
            log.error("Can't generate audio stub: ", e);
        }
    }

    private void writeProxyResponseXAccelRedirect(HttpServletResponse resp, HttpServletRequest req) throws Exception {

        // String directoriesPath = pathParams[0]; // must be proxy
        Integer height = NumberUtils.createInteger(getRequestParameter(req, "height"));
        Integer width = NumberUtils.createInteger(getRequestParameter(req, "width"));
        String bgcolor = getRequestParameter(req, "bgcolor");


        boolean isCrop = "true".equals(getRequestParameter(req, "crop"));
        boolean disableAnimatedGif = "true".equals(getRequestParameter(req, "disableAnimatedGif"));
        String url = getRequestParameter(req, "url");
        if (StringUtils.isEmpty(url)) {
            log.warn("Null proxy url :\n" + WebUtils.requestDataToString(req));
            proxyNoContent(resp);
            return;
        }
        boolean flushCache = "true".equals(getRequestParameter(req, "flushCache"));
        boolean padding = "true".equals(getRequestParameter(req, "pad"));

        ThumbConfiguration config = new ThumbConfiguration();
        if (bgcolor != null) {
            config.setBackgroundColor(bgcolor);
        }
        config.setHeight(height == null ? 0 : height);
        config.setWidth(width == null ? 0 : width);
        config.setCrop(isCrop);
        config.setDisableAnimatedGif(disableAnimatedGif);
        config.setPad(padding);
        final String image;
        try {
            image = proxyContentService.getRelativePath(url, config, flushCache);
        } catch (IOException e) {
            log.warn("ProxyContentService failed with exception: ", e);
            log.warn("Failed on url :\n" + WebUtils.requestDataToString(req));
            proxyNoContent(resp);
            return;
        } catch (Exception e) {
            log.error("ProxyContentService failed with exception: ", e);
            log.error("Failed on url :\n" + WebUtils.requestDataToString(req));
            proxyNoContent(resp);
            return;
        }
        resp.setStatus(HttpServletResponse.SC_OK);
        resp.setHeader("X-Accel-Redirect", image);
        resp.setHeader("Cache-Control", "public; max-age=31536000");
        resp.getOutputStream().flush();
    }

    /**
     * Helper method writing response generated for x-accel-redirects
     *
     * @param resp 200 OK response with Content-Type: image/...
     * @param req  http://s1.planeta.ru/images/some-thumbnail.png request
     */
    private void writeResponseXAccelRedirect(HttpServletResponse resp, HttpServletRequest req) throws IOException, ImageOperationException, InterruptedException, IM4JavaException {
        String seekParameter = getRequestParameter(req, VIDEO_SEEK_PARAMETER_KEY);
        boolean flushCache = "true".equals(getRequestParameter(req, "flushCache"));

        String xAccelRedirectHeader = getPathsGenerationService().generateRedirectUrl(req.getRequestURI(), flushCache);
        if (seekParameter != null && !seekParameter.isEmpty()) {
            xAccelRedirectHeader = xAccelRedirectHeader + "?start=" + seekParameter;
        }

        resp.setStatus(HttpServletResponse.SC_OK);
        resp.setHeader("X-Accel-Redirect", xAccelRedirectHeader);
        log.debug(xAccelRedirectHeader);

        resp.getOutputStream().flush();
    }

}

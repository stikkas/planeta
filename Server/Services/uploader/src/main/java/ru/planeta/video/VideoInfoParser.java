package ru.planeta.video;

import org.apache.log4j.Logger;

import javax.annotation.Nullable;

/**
 * Parser of the video info
 *
 * @author ds.kolyshev
 *         Date: 12.09.11
 */
public class VideoInfoParser {

    private static final String DURATION_KEY = "Duration: ";
    private static final String STREAM_KEY = "Stream #";
    private static final String BITRATE_KEY = "bitrate:";
    private static final String VIDEO_STREAM_KEY = "Video: ";
    private static final String AUDIO_STREAM_KEY = "Audio: ";
    private static final String VIDEO_STREAM_FPS_KEY = "fps";
    private static final String AUDIO_AR_KEY = "Hz";

    private static final Logger log = Logger.getLogger(VideoInfoParser.class);

    /**
     * Parses configuration from ffmpeg out string
     *
     * @param input Parses video configuration from ffmpeg output stream
     * @return Video info object
     */
    @Nullable
    public static VideoInfo parseVideoConfiguration(String input) {
        if (input == null || input.isEmpty()) {
            return null;
        }

        int durationIndex = input.indexOf(DURATION_KEY);
        if (durationIndex < 0) {
            return null;
        }
        String durationString = input.substring(durationIndex + DURATION_KEY.length(), input.indexOf(", start", durationIndex));
        int duration = parseTimeSpanFromString(durationString);


        int bitrateIndex = input.indexOf(BITRATE_KEY);
        int streamsIndex = input.indexOf(STREAM_KEY, bitrateIndex);
        if (streamsIndex <= 0) {
            return null;
        }
        String bitrate = input.substring(bitrateIndex + BITRATE_KEY.length(), streamsIndex).trim();

        input = input.substring(streamsIndex);

        int videoStreamIndex = input.indexOf(VIDEO_STREAM_KEY);
        if (videoStreamIndex <= 0) {
            return null;
        }
        input = getVideoConfigString(input, videoStreamIndex);

        String[] info = input.split(",");

        String codecName = info[0];
        String[] size = info[2].split("x");
        int width = Integer.parseInt(size[0].trim());
        final int height;
        int spaceIndex = size[1].indexOf(" ");
        if (spaceIndex != -1) {
            height = Integer.parseInt(size[1].substring(0, spaceIndex));
        } else {
            height = Integer.parseInt(size[1].trim());
        }
		String fpsString = info[4].trim();
		String fps = "";
		if (fpsString.contains(VIDEO_STREAM_FPS_KEY)) {
			spaceIndex = fpsString.indexOf(" ");
			fps = fpsString.substring(0, spaceIndex).trim();
		}
        log.debug("fps " + fps);

        String ar = extractAr(input);
        // TODO add ffmpeg parse ar
        VideoInfo videoInfo = new VideoInfo();
        videoInfo.setBitrate(bitrate);
        videoInfo.setDuration(duration);
        videoInfo.setCodec(codecName);
        videoInfo.setWidth(width);
        videoInfo.setHeight(height);
		videoInfo.setOriginalFps(fps);
        videoInfo.setOriginalAr(ar);
        return videoInfo;
    }

    static String getVideoConfigString(String input, int videoStreamIndex) {
        input = input.substring(videoStreamIndex + VIDEO_STREAM_KEY.length());

        log.debug("Video key " + input);
        // remove braces
        input = input.replaceAll("\\s*\\([^\\)]*\\)\\s*", "");;

        log.debug("After replace " + input);
        return input;
    }

    private static String extractAr(String input) {
        int spaceIndex;
        int audioStreamIndex = input.indexOf(AUDIO_STREAM_KEY);
        input = input.substring(audioStreamIndex + AUDIO_STREAM_KEY.length());
        String[] audioInfo = input.split(",");
        String arString = audioInfo[1].trim();
        String ar = "";
        if (arString.contains(AUDIO_AR_KEY)) {
            spaceIndex = arString.indexOf(" ");
            ar = arString.substring(0, spaceIndex).trim();
        }
        log.debug("ar " + ar);
        return ar;
    }

    /**
     * Parses time interval from string
     *
     * @param durationString "HH:mm:ss:SS" formatted
     * @return Duration in seconds
     */
    private static int parseTimeSpanFromString(String durationString) {
        String[] durationParts = durationString.split(":");
        int hours = Integer.parseInt(durationParts[0]);
        int minutes = Integer.parseInt(durationParts[1]);
        int seconds = Integer.parseInt(durationParts[2].split("\\.")[0]);
        int milliseconds = Integer.parseInt(durationParts[2].split("\\.")[1]) * 10;
        return ((((hours * 60) + minutes) * 60 + seconds) * 1000 + milliseconds) / 1000;
    }
}

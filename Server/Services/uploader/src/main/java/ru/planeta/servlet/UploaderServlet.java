package ru.planeta.servlet;

import com.googlecode.jsonplugin.JSONException;
import com.googlecode.jsonplugin.JSONUtil;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.log4j.Logger;
import org.im4java.core.IM4JavaException;
import org.jaudiotagger.audio.exceptions.CannotReadException;
import org.jaudiotagger.audio.exceptions.InvalidAudioFrameException;
import org.jaudiotagger.audio.exceptions.ReadOnlyFileException;
import org.jaudiotagger.tag.TagException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import ru.planeta.api.exceptions.NotFoundException;
import ru.planeta.api.exceptions.PermissionException;
import ru.planeta.commons.console.RuntimeHelper;
import ru.planeta.commons.web.WebUtils;
import ru.planeta.dao.commondb.CampaignDAO;
import ru.planeta.domain.ActionStatus;
import ru.planeta.domain.UploadFilesRequestKey;
import ru.planeta.image.CropParams;
import ru.planeta.image.ImageOperationException;
import ru.planeta.model.Constants;
import ru.planeta.model.common.campaign.Campaign;
import ru.planeta.model.profile.ProfileFile;
import ru.planeta.model.profile.media.AudioTrack;
import ru.planeta.model.profile.media.Photo;
import ru.planeta.model.profile.media.Video;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URI;

/**
 * Uploader servlet
 *
 * @author ameshkov
 */
@Controller
public class UploaderServlet extends BaseServlet {
    private static final Logger log = Logger.getLogger(UploaderServlet.class);

    private static final String LIVE_CHECK = "/live-check";

    private final CampaignDAO campaignDAO;

    @Value("${proxy.url}")
    private String proxyUrl;

    @Autowired
    public UploaderServlet(CampaignDAO campaignDAO) {
        this.campaignDAO = campaignDAO;
    }

    @Override
    protected void doOptions(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setHeader("Access-Control-Allow-Origin", "*");
        resp.setHeader("Access-Control-Allow-Methods", "GET, POST, OPTIONS");
        //resp.setHeader("Access-Control-Allow-Headers", "Content-Type");
        resp.setHeader("Access-Control-Max-Age", "86400");
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setHeader("Access-Control-Allow-Origin", "*");
        resp.setHeader("Access-Control-Allow-Headers", "Content-Type, X-File-Name, X-File-Type, X-File-Size");
        try {
            // Always setting connection: close header
            //resp.setHeader("Connection", "close");

            if (req.getRequestURI() == null) {
                log.error("No such api, method group or method:\n" + WebUtils.requestDataToString(req));
                ActionStatus<String> actionStatus =
                        ActionStatus.createErrorStatus("No such api, method group or method:\n" + req.getRequestURI());
                writeResponse(resp, actionStatus);
                return;
            }

            String clientIdParameter = getRequestParameterSafe(req, CLIENT_ID_PARAMETER_KEY);
            String ownerIdParameter = getRequestParameterSafe(req, OWNER_ID_PARAMETER_KEY);
            long clientId = NumberUtils.toLong(clientIdParameter);
            // long clientId = myProfileId();
            long ownerId = NumberUtils.toLong(ownerIdParameter);

            if (req.getRequestURI().startsWith(BACKGROUND_IMAGE_KEY)) {
                Photo photo = addCampaignBackgroundImage(clientId, ownerId);
                ActionStatus<Photo> actionStatus = ActionStatus.createSuccessStatus(photo);
                log.debug(actionStatus);
                writeResponse(resp, actionStatus);
                return;
            }


            UploadFilesRequestKey uploadKey = UploadFilesRequestKey.getByValue(req.getRequestURI());

            if (uploadKey == UploadFilesRequestKey.PROFILE_AUDIO) {
                uploadProfileAudio(req, resp, clientId, ownerId);
            } else if (uploadKey == UploadFilesRequestKey.PROFILE_IMAGE) {
                uploadProfileImage(req, resp, clientId, ownerId);
            } else if (uploadKey == UploadFilesRequestKey.PROFILE_VIDEO) {
                uploadProfileVideo(req, resp, clientId, ownerId);
            } else if (uploadKey == UploadFilesRequestKey.PROFILE_FILE) {
                uploadProfileFile(req, resp, clientId, ownerId);
            }
        } catch (Exception ex) {
            log.error("Error processing request:\n" + WebUtils.requestDataToString(req), ex);
            ActionStatus<String> actionStatus =
                    ActionStatus.createErrorStatus("Error processing request:\n" + WebUtils.requestDataToString(req) + "\n");
            writeResponse(resp, actionStatus);
        }
    }

    // @SuppressWarnings({"ConstantConditions"})
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        try {
            if (req.getRequestURI() == null) {
                log.error("No such api, method group or method:\n" + WebUtils.requestDataToString(req));
                resp.sendError(HttpServletResponse.SC_BAD_REQUEST, "No such api, method group or method");
                return;
            }

            if (req.getRequestURI().equalsIgnoreCase(LIVE_CHECK)) {
                WebUtils.setDefaultHeaders(resp);
                resp.getWriter().write("OK");
                resp.getWriter().flush();
                return;
            }
            if (req.getRequestURI().equalsIgnoreCase(CROSSDOMAIN_XML_KEY)) {
                writeResponseXml(resp, CROSS_DOMAIN_XML);
                return;
            }

            String clientIdParameter = getRequestParameterSafe(req, CLIENT_ID_PARAMETER_KEY);
            String ownerIdParameter = getRequestParameterSafe(req, OWNER_ID_PARAMETER_KEY);
            long clientId = NumberUtils.toLong(clientIdParameter);
            long ownerId = NumberUtils.toLong(ownerIdParameter);

            String url = getRequestParameterSafe(req, TRANSFER_URL_PARAMETER_KEY);
            if (StringUtils.isEmpty(url)) {
                throw new RuntimeException("Error downloading file url id empty");
            }

            boolean isMyFile = serverPath.equals(WebUtils.getHost(url)) && !proxyUrl.equals(new URI(url).getPath());
            File reqFile = null;
            if (!isMyFile) {
                reqFile = downloadFileFromUrl(url);
            } else {
                final String fileName = getPathsGenerationService().generateFullFileName(new URI(url).getPath());
                if (fileName != null) {
                    File existFile = new File(fileName);
                    if (existFile.exists()) {
                        File tempDir = new File(System.getProperty("java.io.tmpdir"));
                        reqFile = new File(tempDir + "/" + System.currentTimeMillis() + "_" + existFile.getName());
                        FileUtils.copyFile(existFile, reqFile);
                    }
                }
            }

            if (reqFile == null || !reqFile.exists()) {
                throw new RuntimeException("Error downloading file from url:" + url);
            }

            try {
                if (req.getRequestURI().startsWith(TRANSFER_IMAGE_KEY)) {
                    transferProfileImage(resp, req, clientId, ownerId, reqFile);
                } else if (req.getRequestURI().startsWith(CROP_IMAGE_KEY)) {
                    transferCropProfileImage(resp, req, clientId, ownerId, reqFile);
                }
            } finally {
                if (reqFile.exists()) {
                    if (!reqFile.delete()) {
                        log.warn("Cannot deleteByProfileId file " + reqFile.getName());
                    }
                }
            }

        } catch (Exception ex) {
            log.error("Error processing request:\n" + WebUtils.requestDataToString(req), ex);
            resp.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, ex.getMessage());
        }
    }


    private void transferProfileImage(HttpServletResponse resp, HttpServletRequest req, long clientId, long ownerId, @Nonnull File reqFile) throws Exception {
        // parse parameters
        String albumIdParameter = getRequestParameter(req, ALBUM_ID_PARAMETER_KEY);
        String albumTypeIdParameter = getRequestParameterSafe(req, ALBUM_TYPE_ID_PARAMETER_KEY);
        long albumId = NumberUtils.toLong(albumIdParameter);
        int albumTypeId = NumberUtils.toInt(albumTypeIdParameter);

        // upload
        Photo photo = getImageContentService().uploadPhoto(reqFile, clientId, ownerId, albumId, albumTypeId, null);
        ActionStatus<Photo> actionStatus = ActionStatus.createSuccessStatus(photo);
        log.debug(actionStatus);
        writeResponse(resp, actionStatus);
    }


    private Photo addCampaignBackgroundImage(long clientId, long ownerId, @Nonnull String rawdata) throws Exception {
        // parse parameters
        // upload
        String imageDataBytes = rawdata.substring(rawdata.indexOf(",") + 1);
        byte[] data = Base64.decodeBase64(imageDataBytes);
        File tempDir = new File(System.getProperty("java.io.tmpdir"));
        // TODO: Use /var/tmp (from application.properties) instead of tmp-dir
        File reqFile = File.createTempFile("MultiPart", ".jpg", tempDir);
        FileUtils.writeByteArrayToFile(reqFile, data);
        return getImageContentService().uploadPhoto(reqFile, clientId, ownerId, 0, Constants.INSTANCE.getALBUM_CAMPAIGN(), null);
    }

    private Photo addCampaignBackgroundImage(long clientId, long campaignId) throws Exception {
        Campaign campaign = campaignDAO.selectForUpdate(campaignId);
        if (campaign == null) {
            throw new NotFoundException(Campaign.class, campaignId);
        }
        Photo photo = addCampaignBackgroundImage(clientId, clientId, campaign.getBackgroundImageUrl());
        campaign.setBackgroundImageUrl(photo.getImageUrl());
        campaignDAO.update(campaign);
        return photo;
    }

    private void transferCropProfileImage(HttpServletResponse resp, HttpServletRequest req, long clientId, long ownerId, @Nonnull File reqFile) throws Exception {
        String albumIdParameter = getRequestParameter(req, ALBUM_ID_PARAMETER_KEY);
        String albumTypeIdParameter = getRequestParameterSafe(req, ALBUM_TYPE_ID_PARAMETER_KEY);
        long albumId = albumIdParameter != null ? NumberUtils.toLong(albumIdParameter) : 0;
        int albumTypeId = NumberUtils.toInt(albumTypeIdParameter);

        String cropXParameter = getRequestParameterSafe(req, CROP_IMAGE_X_PARAMETER_KEY);
        String cropYParameter = getRequestParameterSafe(req, CROP_IMAGE_Y_PARAMETER_KEY);
        int cropX = NumberUtils.toInt(cropXParameter);
        int cropY = NumberUtils.toInt(cropYParameter);

        String cropWidthParameter = getRequestParameterSafe(req, CROP_IMAGE_WIDTH_PARAMETER_KEY);
        String cropHeightParameter = getRequestParameterSafe(req, CROP_IMAGE_HEIGHT_PARAMETER_KEY);
        int cropWidth = NumberUtils.toInt(cropWidthParameter);
        int cropHeight = NumberUtils.toInt(cropHeightParameter);

        CropParams cropParams = new CropParams(cropX, cropY, cropWidth, cropHeight);

        //we can first crop file
        //File cropped = getImageContentService().cropImage(reqFile, cropX, cropY, cropWidth, cropHeight);

        //and then upload cropped
        Photo photo = getImageContentService().uploadPhoto(reqFile, clientId, ownerId, albumId, albumTypeId, cropParams);
        ActionStatus<Photo> actionStatus = ActionStatus.createSuccessStatus(photo);
        writeResponse(resp, actionStatus);
    }


    private void uploadProfileImage(HttpServletRequest req, HttpServletResponse resp, long clientId, long ownerId)
            throws IOException, ImageOperationException, IM4JavaException, InterruptedException, PermissionException, NotFoundException {
        File reqFile = getUploadedFile(req, resp);

        if (reqFile == null) {
            return;
        }
        String albumIdParameter = getRequestParameterSafe(req, ALBUM_ID_PARAMETER_KEY);
        String albumTypeIdParameter = getRequestParameterSafe(req, ALBUM_TYPE_ID_PARAMETER_KEY);
        long albumId = NumberUtils.toLong(albumIdParameter);
        int albumTypeId = NumberUtils.toInt(albumTypeIdParameter);

        Photo photo = getImageContentService().uploadPhoto(reqFile, clientId, ownerId, albumId, albumTypeId, null);

        String redirectTo = getRequestParameter(req, "redirect");
        if (redirectTo != null && !redirectTo.isEmpty()) {
            resp.sendRedirect(redirectTo + "?id=" + photo.getObjectId() + "&owner=" + photo.getProfileId());
        } else {
            ActionStatus<Photo> actionStatus = ActionStatus.createSuccessStatus(photo);
            writeResponse(resp, actionStatus);
        }
    }

    private void uploadProfileAudio(HttpServletRequest req, HttpServletResponse resp, long clientId, long ownerId)
            throws IOException, InvalidAudioFrameException, ReadOnlyFileException, CannotReadException, TagException, PermissionException {
        File reqFile = getUploadedFile(req, resp);
        if (reqFile == null) {
            return;
        }
        String reqFileName = getFileNameHeader(req);
        uglyPermissionHack(reqFileName);

        String albumIdParameter = getRequestParameterSafe(req, ALBUM_ID_PARAMETER_KEY);
        long albumId = NumberUtils.toLong(albumIdParameter);

        AudioTrack audioTrack = getAudioContentService().uploadAudioTrack(reqFile, reqFileName, clientId, ownerId, albumId);
        ActionStatus<AudioTrack> actionStatus = ActionStatus.createSuccessStatus(audioTrack);
        writeResponse(resp, actionStatus);
    }

    private void uploadProfileVideo(HttpServletRequest req, HttpServletResponse resp, long clientId, long ownerId)
            throws IOException, PermissionException, InterruptedException, NotFoundException {
        File reqFile = getUploadedFile(req, resp);

        if (reqFile == null) {
            return;
        }

        String reqFileName = getFileNameHeader(req);
        uglyPermissionHack(reqFileName);

        String broadcastStreamIdParameter = getRequestParameter(req, BROADCAST_STREAM_ID_PARAMETER_KEY);
        final Video video;
        if (broadcastStreamIdParameter != null && !broadcastStreamIdParameter.isEmpty()) {
            long broadcastStreamId = NumberUtils.toLong(broadcastStreamIdParameter);
            video = getVideoContentService().uploadBroadcastStreamVideo(reqFile, reqFileName, clientId, ownerId, broadcastStreamId);
        } else {
            video = getVideoContentService().uploadVideo(reqFile, reqFileName, clientId, ownerId);
        }

        ActionStatus<Video> actionStatus = ActionStatus.createSuccessStatus(video);
        writeResponse(resp, actionStatus);
    }

    private void uploadProfileFile(HttpServletRequest req, HttpServletResponse resp, long clientId, long ownerId)
            throws IOException, PermissionException {
        File reqFile = getUploadedFile(req, resp);
        if (reqFile == null) {
            return;
        }
        String reqFileName = getFileNameHeader(req);
        uglyPermissionHack(reqFileName);

        ProfileFile file = getProfileFileContentService().uploadProfileFile(reqFile, reqFileName, clientId, ownerId);

        ActionStatus<ProfileFile> actionStatus = ActionStatus.createSuccessStatus(file);
        writeResponse(resp, actionStatus);
    }

    /**
     * Helper method to construct response from actionStatus
     *
     */
    private static void writeResponse(HttpServletResponse resp, ActionStatus actionStatus) throws IOException {
        if (actionStatus == null) {
            resp.setStatus(HttpServletResponse.SC_NO_CONTENT);
        } else {
            try {
                byte[] bytes = JSONUtil.serialize(actionStatus).getBytes("utf-8");
                resp.setStatus(HttpServletResponse.SC_OK);
                resp.setContentType("text/plain; charset=utf-8");

                OutputStream outputStream = resp.getOutputStream();
                outputStream.write(bytes);
                outputStream.flush();

            } catch (JSONException e) {
                resp.sendError(HttpServletResponse.SC_BAD_REQUEST, "Error creating json:\n" + e.getMessage());
            }
        }
    }

    /**
     * Gets file from upload module request
     */
    @Nullable
    private static File getUploadedFile(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        String reqFilePath = req.getHeader(FILE_PATH_HEADER_NAME);
        log.debug("uploaded file path:" + reqFilePath);
        uglyPermissionHack(reqFilePath);
        File reqFile = new File(reqFilePath);
        if (!reqFile.exists()) {
            log.error("File not valid:\n" + WebUtils.requestDataToString(req));
            resp.sendError(HttpServletResponse.SC_BAD_REQUEST, "File not valid");
            return null;
        }

        return reqFile;
    }

    private static void uglyPermissionHack(String reqFilePath) {
        try {
            String command = "sudo /bin/chmod 666 " + reqFilePath;
            log.warn("Ugly Command " + command);
            RuntimeHelper.executeCommandLine(command);
        } catch (Exception e) {
            log.error(e);
        }
    }

}
package ru.planeta.image;

import java.awt.*;
import java.awt.font.FontRenderContext;
import java.awt.image.BufferedImage;

/**
* User: a.savanovich
* Date: 16.12.13
* Time: 13:35
*/
public class StringParameters {

    public static final String CAMPAIGN_EDITOR_CONFIG_KEY = "campaign";

    public int fontSize = 12;
    public int fontStyle = Font.PLAIN;
    public String fontFamily = "Tahoma";
    public int stubHeight = 300;
    public int stubWidth = 400;
    public Color textColor = Color.white;
    public Color bgColor = Color.black;
    public int stringPaddingX = 5;
    public int stringPaddingY = 3;
    public int padding = 10;
    public int borderRadius = 8;
    public int imageType = 5;
    public String imageExtension = "png";

    public StringParameters(int fontSize, int fontStyle, String fontFamily, int stubWidth, int stubHeight, Color textColor, Color bgColor, int stringPaddingX, int stringPaddingY, int padding, int borderRadius) {
        this.fontSize = fontSize;
        this.fontStyle = fontStyle;
        this.fontFamily = fontFamily;
        this.stubHeight = stubHeight;
        this.stubWidth = stubWidth;
        this.textColor = textColor;
        this.bgColor = bgColor;
        this.stringPaddingX = stringPaddingX;
        this.stringPaddingY = stringPaddingY;
        this.padding = padding;
        this.borderRadius = borderRadius;
    }

    public int maxWidth(String duration) {
        Font curFont = getFont();
        if (duration == null)
            duration = "1:45:03";
        FontRenderContext frc = (new BufferedImage(stubWidth, stubHeight, imageType)).createGraphics().getFontRenderContext();
        return (int) (curFont.getStringBounds(duration, frc).getWidth() + padding + 2 * stringPaddingX);
    }

    public int lineSpace() {
        return 2;
    }

    public Font getFont() {
        return new Font(fontFamily, fontStyle, fontSize);
    }

    public static StringParameters defaultDrawVideoSettings() {
        return new StringParameters(12, Font.PLAIN, "Tahoma", 480, 270, Color.white, Color.black, 5, 3, 5, 8);
    }

    public static StringParameters defaultDrawAudioSettings() {
        return new StringParameters(12, Font.PLAIN, "Tahoma", 400, 45, Color.black, Color.white, 5, 0, 5, 8);
    }

    public static StringParameters campaignDrawVideoSettings() {
        return new StringParameters(12, Font.PLAIN, "Tahoma", 640, 360, Color.white, Color.black, 5, 3, 5, 8);
    }
}

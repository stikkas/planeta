package ru.planeta.video;

/**
 * video mode configuration
 *
 * @author ds.kolyshev
 * Date: 08.09.11
 */
public class VideoConfiguration implements Comparable<VideoConfiguration> {

    private int qualityMode;
    private String name;
    private String codec;
    private int width;
    private int height;
    private String previewTime;
    private boolean defaultConfiguration;
	private String bitrate;

    public boolean isDefaultConfiguration() {
        return defaultConfiguration;
    }

    public void setDefaultConfiguration(boolean defaultConfiguration) {
        this.defaultConfiguration = defaultConfiguration;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getQualityMode() {
        return qualityMode;
    }

    public void setQualityMode(int qualityMode) {
        this.qualityMode = qualityMode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPreviewTime() {
        return previewTime;
    }

    public void setPreviewTime(String previewTime) {
        this.previewTime = previewTime;
    }

    public String getCodec() {
        return codec;
    }

    public void setCodec(String codec) {
        this.codec = codec;
    }

	public String getBitrate() {
		return bitrate;
	}

	public void setBitrate(String bitrate) {
		this.bitrate = bitrate;
	}

	@Override
    public int compareTo(VideoConfiguration o) {
        return -getWidth() + o.getWidth();
    }
}

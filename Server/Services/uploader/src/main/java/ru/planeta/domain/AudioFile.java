package ru.planeta.domain;

import ru.planeta.model.profile.media.AudioTrack;

import java.io.File;

/**
 * @author ds.kolyshev
 * Date: 28.10.11
 */
public class AudioFile {
	private AudioTrack audioTrack;
    private File file;

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }

    public AudioTrack getAudioTrack() {
        return audioTrack;
    }

    public void setAudioTrack(AudioTrack audioTrack) {
        this.audioTrack = audioTrack;
    }
}

package ru.planeta.service;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.im4java.core.ConvertCmd;
import org.im4java.core.IM4JavaException;
import org.im4java.core.IMOperation;
import org.im4java.core.MontageCmd;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import ru.planeta.api.exceptions.NotFoundException;
import ru.planeta.api.exceptions.PermissionException;
import ru.planeta.api.service.content.PhotoService;
import ru.planeta.domain.PhotoFile;
import ru.planeta.image.BucketType;
import ru.planeta.image.CropParams;
import ru.planeta.image.ImageInfo;
import ru.planeta.image.ImageOperationException;
import ru.planeta.model.Constants;
import ru.planeta.model.enums.BlockSettingType;
import ru.planeta.model.profile.media.Photo;
import ru.planeta.model.profile.media.PhotoAlbum;
import ru.planeta.model.profile.media.Video;
import ru.planeta.video.VideoConverter;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Date;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

/**
 * @author ds.kolyshev
 * Date: 28.10.11
 */
@Service
public class ImageContentServiceImpl extends BaseService implements ImageContentService {

	@Value("${images.path}")
    private String imagesPath;
	@Value("${images.url}")
    private String imagesUrl;
	@Autowired
    private ThumbGenerationService thumbGenerationService;
	@Autowired
    private PhotoService photoService;

    private final Executor executorService = Executors.newSingleThreadExecutor();

	public void setImagesPath(String imagesPath) {
		this.imagesPath = imagesPath;
	}

	private static final long MAX_AVAILABLE_GIF_SIZE = 10 * 1024 * 1024; // 10 MB

    @Override
    public Photo uploadPhoto(@Nonnull File file, long clientId, long profileId, long albumId, int albumTypeId, CropParams cropParams)
            throws ImageOperationException, IOException, InterruptedException, IM4JavaException, PermissionException, NotFoundException {
        return uploadPhotoFile(file, clientId, profileId, albumId, albumTypeId, 0, cropParams).getPhoto();
    }

	@Override
	public PhotoFile uploadPhotoFile(@Nonnull File file, long clientId, long profileId, long albumId, final int albumTypeId, long imageId, CropParams cropParams)
			throws ImageOperationException, IOException, InterruptedException, IM4JavaException, PermissionException, NotFoundException {

		if (!getPermissionService().checkAddPermission(clientId, profileId, BlockSettingType.PHOTO)) {
			if (albumTypeId == Constants.INSTANCE.getALBUM_DEFAULT()) {
				throw new PermissionException();
			} else if (!getPermissionService().isAdmin(clientId, profileId)) {
                throw new PermissionException();
			}
		}

		// Getting image properties
		// If exception is not thrown - everything is ok, saving image
		ImageInfo imageInfo = new ImageInfo(file);
		BucketType fileBucketType = BucketType.JPEG;

        if (imageInfo.getBucketType() == BucketType.GIF && file.length() > MAX_AVAILABLE_GIF_SIZE) {
            log.info("User ID: " + clientId + " tried to upload to big gif image [" + file.length() + " bytes] to Profile ID: " + profileId);
            throw new PermissionException();
        }

		if (imageInfo.getBucketType() == BucketType.GIF) {
			fileBucketType = BucketType.GIF;
		}

		File renamedFile = new File(file.getParent(), new Date().getTime() + "_renamed." + fileBucketType.getExtension());
        FileUtils.moveFile(file, renamedFile);

		if (imageId == 0) {
			imageId = getSequencesDAO().selectNextLong(Photo.class);
		}
		Photo photo = preparePhoto(clientId, profileId, albumId, albumTypeId, imageId, renamedFile.getName());

        File originalFile = moveOriginalFile(renamedFile, imageId, imagesPath);

        final File uncroppedFileCopy = new File(originalFile.getParent() + "/" + "uncropped." + FilenameUtils.getExtension(originalFile.getName()));
        FileUtils.copyFile(originalFile, uncroppedFileCopy);
        final File targetFile = cropParams != null ? cropImage(originalFile, cropParams) : originalFile;

        if (albumTypeId != Constants.INSTANCE.getALBUM_VIDEO_STORYBOARDS()) {
            if (imageInfo.getBucketType() == BucketType.GIF) {
                thumbGenerationService.generateThumbnails(targetFile, uncroppedFileCopy, true, albumTypeId, true);
                executorService.execute(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            thumbGenerationService.generateThumbnails(targetFile, uncroppedFileCopy, true, albumTypeId, false);
                        } catch (Exception e) {
                            //ignore
                        }
                    }
                });
            } else {
                thumbGenerationService.generateThumbnails(targetFile, uncroppedFileCopy, true, albumTypeId, false);
            }
        }

		PhotoFile photoFile = new PhotoFile();
		photoFile.setFile(targetFile);
		photoFile.setPhoto(photo);
		return photoFile;
	}

    @Override
    @Nonnull
    public File cropImage(@Nonnull File file, CropParams cropParams) throws ImageOperationException, IOException, InterruptedException, IM4JavaException {
        return cropImage(file, cropParams.getCropX(), cropParams.getCropY(), cropParams.getCropWidth(), cropParams.getCropHeight());

    }

    @Override
    @Nonnull
    public File cropImage(@Nonnull File file, int cropX, int cropY, int cropWidth, int cropHeight) throws ImageOperationException, IOException, InterruptedException, IM4JavaException {
        return cropImageStatic(file, cropX, cropY, cropWidth, cropHeight);

    }

    @Nonnull
    private static File cropImageStatic(@Nonnull File file, int cropX, int cropY, int cropWidth, int cropHeight) throws IOException, IM4JavaException, InterruptedException {
        // Getting image properties
        // If exception is not thrown - everything is ok, saving image
        ImageInfo imageInfo = new ImageInfo(file);
        log.info("imageInfo widhth: " + imageInfo.getWidth() + " imageInfo height: " + imageInfo.getHeight());
        log.info("cropWidth: " + cropWidth + " cropHeight: " + cropHeight + " cropX: " + cropX + " cropY: " + cropY);

        IMOperation op = new IMOperation();
        op.addImage(file.getAbsolutePath());
        op.strip();
        op.autoOrient();
        op.crop(cropWidth, cropHeight, cropX, cropY);
        op.p_repage();

        BucketType thumbBucketType = BucketType.JPEG;
        if (imageInfo.getBucketType() == BucketType.GIF) {
            thumbBucketType = BucketType.GIF;
        }
        op.addImage(thumbBucketType.getExtension() + ":" + file.getAbsolutePath());

        ConvertCmd convertCmd = new ConvertCmd();
        convertCmd.run(op);

        return file;
    }

    @Override
    @Nullable
	public File storyboardVideo(File videoFile, Video video) throws IOException, InterruptedException, IM4JavaException {
		File targetFile = new File(videoFile.getParent(), "storyboard." + BucketType.JPEG.getExtension());

		File storyboardDirectory = VideoConverter.captureStoryboard(videoFile, video);
		if (storyboardDirectory != null && storyboardDirectory.isDirectory()) {
			IMOperation op = new IMOperation();
			op.geometry(64, 48, 0, 0);
			op.background("black");
			op.tile("30x");
			op.tileOffset(0);

			File[] files = storyboardDirectory.listFiles();
			if (files != null) {
				Arrays.sort(files, new Comparator<File>() {
					@Override
					public int compare(File o1, File o2) {
						return o1.getName().compareTo(o2.getName());
					}
				});

				for (File image : files) {
					op.addImage(image.getAbsolutePath());
				}

				op.addImage(targetFile.getAbsolutePath());

				MontageCmd montageCmd = new MontageCmd();
				montageCmd.run(op);

				FileUtils.deleteQuietly(storyboardDirectory);

				return targetFile;
			}
		}

		return null;
	}

	@Override
	public void regenerateThumbnails(long clientId, long ownerId, long imageId) throws NotFoundException, PermissionException, IOException, InterruptedException, IM4JavaException, ImageOperationException {
			if (!getPermissionService().checkAddPermission(clientId, ownerId, BlockSettingType.PHOTO)) {
				throw new PermissionException();
			}

			Photo photo = getPhotoDAO().selectPhotoById(ownerId, imageId);
			if (photo == null) {
				throw new NotFoundException(Photo.class, imageId);
			}

			String imageUrl = photo.getImageUrl();
			String fileName = imageUrl.substring(imageUrl.lastIndexOf("/"));
			File file = new File(generateFullFilePathToCopy(fileName, imageId, imagesPath));
			if (!file.exists()) {
				throw new NotFoundException("image file", imageId);
			}

			PhotoAlbum photoAlbum = getPhotoAlbumDAO().selectAlbumById(photo.getProfileId(), photo.getAlbumId());
			if (photoAlbum == null) {
				throw new NotFoundException(PhotoAlbum.class, photo.getAlbumId());
			}

			int albumTypeId = photoAlbum.getAlbumTypeId();
			if (albumTypeId != Constants.INSTANCE.getALBUM_VIDEO_STORYBOARDS()) {
				thumbGenerationService.generateThumbnails(file, null, true, albumTypeId, false);
			}
	}

	/**
     * Prepares profile's photo
     * Inserts new one if not exists or returns existing
     *
     */
    private Photo preparePhoto(long clientId, long profileId, long albumId, int albumTypeId, long imageId, String fileName) throws UnsupportedEncodingException {
        String imageUrl = generateUrl(fileName, imageId, imagesUrl);

        albumId = preparePhotoAlbum(clientId, profileId, albumId, albumTypeId, imageId, imageUrl);

        Photo currentPhoto = getPhotoDAO().selectPhotoById(profileId, imageId);
        if (currentPhoto != null) {
            currentPhoto.setImageUrl(imageUrl);
            getPhotoDAO().update(currentPhoto);

            return currentPhoto;
        }

        Photo photo = new Photo();
        photo.setPhotoId(imageId);
        photo.setProfileId(profileId);
        photo.setAlbumId(albumId);
        photo.setAuthorProfileId(clientId);
        photo.setViewsCount(0);
        photo.setDescription("");
        photo.setImageUrl(imageUrl);
        getPhotoDAO().insert(photo);

        return photo;
    }

    /**
     * Prepares photo album for photo inserting
     *
     */
    private long preparePhotoAlbum(long clientId, long profileId, long albumId, int albumTypeId, long imageId, String imageUrl) {
        if (albumTypeId == 0) {
            albumTypeId = Constants.INSTANCE.getALBUM_DEFAULT();
        }

        if (albumTypeId != Constants.INSTANCE.getALBUM_DEFAULT()
                && albumTypeId != Constants.INSTANCE.getALBUM_GROUP_PRODUCT()
                && albumTypeId != Constants.INSTANCE.getALBUM_CAMPAIGN()) {
            PhotoAlbum photoAlbum = getPhotoAlbumDAO().selectAlbumByType(profileId, albumTypeId);
            if (photoAlbum != null) {
                albumId = photoAlbum.getAlbumId();
            }
        }

        if (albumId == 0) {
            albumId = photoService.insertAvatarPhotoAlbum(clientId, profileId, albumTypeId, imageId, imageUrl);
        } else {
            updatePhotoAlbum(profileId, albumId, imageId, imageUrl);
        }

        return albumId;
    }

    /**
     * Updates existing photo album on adding photo
     *
     */
    private void updatePhotoAlbum(long profileId, long albumId, long imageId, String imageUrl) {
        PhotoAlbum photoAlbum = getPhotoAlbumDAO().selectAlbumById(profileId, albumId);
        if (photoAlbum == null) {
            throw new IllegalArgumentException("Profile " + profileId + " does not have album " + albumId);
        }

        if (photoAlbum.getPhotosCount() == 0) {
            photoAlbum.setImageId(imageId);
            photoAlbum.setImageUrl(imageUrl);
        }
        photoAlbum.setPhotosCount(photoAlbum.getPhotosCount() + 1);
        getPhotoAlbumDAO().update(photoAlbum);
    }
}

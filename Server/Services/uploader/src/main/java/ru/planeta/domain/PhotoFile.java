package ru.planeta.domain;

import ru.planeta.model.profile.media.Photo;

import java.io.File;

/**
 * @author ds.kolyshev
 * Date: 09.09.11
 */
public class PhotoFile {
    private Photo photo;
    private File file;

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }

	public Photo getPhoto() {
		return photo;
	}

	public void setPhoto(Photo photo) {
		this.photo = photo;
	}
}

package ru.planeta.service;

import org.im4java.core.IM4JavaException;
import ru.planeta.api.exceptions.NotFoundException;
import ru.planeta.api.exceptions.PermissionException;
import ru.planeta.domain.PhotoFile;
import ru.planeta.image.CropParams;
import ru.planeta.image.ImageOperationException;
import ru.planeta.model.profile.media.Photo;
import ru.planeta.model.profile.media.Video;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.io.File;
import java.io.IOException;

/**
 * @author ds.kolyshev
 * Date: 28.10.11
 */
public interface ImageContentService {

	/**
	 * Uploads image file
	 *
	 */
	Photo uploadPhoto(File file, long clientId, long profileId, long albumId, int albumTypeId, CropParams cropParams) throws ImageOperationException, IOException, InterruptedException, IM4JavaException, PermissionException, NotFoundException;

	/**
	 * Uploads image file
	 *
	 */
	PhotoFile uploadPhotoFile(File file, long clientId, long profileId, long albumId, int albumTypeId, long imageId, CropParams cropParams) throws ImageOperationException, IOException, InterruptedException, IM4JavaException, PermissionException, NotFoundException;

    /**
     * Crops image with specified parameters
	 *
     */
    @Nonnull
    File cropImage(File file, int cropX, int cropY, int cropWidth, int cropHeight) throws ImageOperationException, IOException, InterruptedException, IM4JavaException;

	@Nonnull
	File cropImage(@Nonnull File file, CropParams cropParams) throws ImageOperationException, IOException, InterruptedException, IM4JavaException;

	/**
	 * Generates storyboard image for specified video file
	 *
	 */
    @Nullable
	File storyboardVideo(File videoFile, Video video) throws IOException, InterruptedException, IM4JavaException;

	/**
	 * Regenerates thumbnail for selected image
	 */
	void regenerateThumbnails(long clientId, long ownerId, long imageId) throws NotFoundException, PermissionException, IOException, InterruptedException, IM4JavaException, ImageOperationException;
}

package ru.planeta.main;

import org.apache.log4j.Logger;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import ru.planeta.commons.console.ParametersUtils;
import ru.planeta.service.ThumbGenerationService;
import ru.planeta.service.UpdateServerStatsService;
import ru.planeta.servlet.DownloaderServlet;
import ru.planeta.servlet.UploaderServlet;

import javax.servlet.http.HttpServlet;
import org.eclipse.jetty.server.Handler;
import org.eclipse.jetty.server.NCSARequestLog;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.handler.HandlerCollection;
import org.eclipse.jetty.server.handler.RequestLogHandler;
import org.eclipse.jetty.servlet.FilterHolder;
import org.eclipse.jetty.servlet.FilterMapping;
import org.eclipse.jetty.servlet.ServletHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.eclipse.jetty.servlets.MultiPartFilter;

public class UploaderStarter {

    private static Logger log = Logger.getLogger(UploaderStarter.class);
    private static ClassPathXmlApplicationContext applicationContext;
    private static Server server;

    public static void main(String[] args) {
        try {
            Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {
                @Override
                public void run() {
                    onShutDown();
                }
            }));

            int port = Integer.valueOf(ParametersUtils.getParameter(args, "--port", "8091"));
            System.setProperty("port", String.valueOf(port));
            String serverLogsPath = ParametersUtils.getParameter(args, "--server-logs", "./logs/jetty-yyyy_mm_dd.request.log");
            boolean isThumbGenerationMode = !ParametersUtils.getParameter(args, "--transform", "0").equals("0");

            boolean isDownloader = !ParametersUtils.getParameter(args, "--downloader", "0").equals("0");
            if (isDownloader) {
                System.setProperty("appid", "downloader");
            } else {
                System.setProperty("appid", "uploader");
            }

            applicationContext = new ClassPathXmlApplicationContext("spring/applicationContext-*.xml", "classpath*:/spring/applicationContext-dao.xml", "classpath*:/spring/applicationContext-geo.xml", "classpath*:/spring/applicationContext-global.xml");

            if (isThumbGenerationMode) {
                log.info("Starting thumbnails generation..");

                ThumbGenerationService thumbGenerationService = applicationContext.getBean(ThumbGenerationService.class);
                thumbGenerationService.generateThumbnails(false);
            } else {
                HttpServlet servlet = isDownloader ? applicationContext.getBean(DownloaderServlet.class) : applicationContext.getBean(UploaderServlet.class);
                if (!isDownloader) {
                    UpdateServerStatsService serviceUpdater = applicationContext.getBean(UpdateServerStatsService.class);
                    serviceUpdater.setUploader(true);
                }
                log.info("Starting " + (isDownloader ? "downloader" : "uploader") + " service on port " + port);
                createServer(servlet, port, serverLogsPath, !isDownloader);

                server.start();

                log.info("Service has been started successfully");

                server.join();
            }
        } catch (Exception ex) {
            log.error("Unhandled exception in the main thread", ex);
        }
    }

    private static void createServer(HttpServlet servlet, int port, String serverLogsPath, boolean isMultiPartFilterEnabled) {
        server = new Server(port);


        ServletHolder servletHolder = new ServletHolder(servlet);
        ServletHandler handler = new ServletHandler();
        handler.addServletWithMapping(servletHolder, "/*");

        if (isMultiPartFilterEnabled) {
            MultiPartFilter multiPartFilter = new MultiPartFilter();
            FilterHolder holder = new FilterHolder(multiPartFilter);
            holder.setName("fileupload");
            holder.setInitParameter("delete", "true");
            holder.setInitParameter("deleteFiles", "true");
            FilterMapping filterMapping = new FilterMapping();
            filterMapping.setPathSpec("/*");
            filterMapping.setFilterName(holder.getName());
            handler.addFilter(holder, filterMapping);
        }

        HandlerCollection handlers = new HandlerCollection();
        RequestLogHandler requestLogHandler = new RequestLogHandler();
        handlers.setHandlers(new Handler[]{handler, requestLogHandler});

        server.setHandler(handlers);

        NCSARequestLog requestLog = new NCSARequestLog(serverLogsPath);
        requestLog.setRetainDays(90);
        requestLog.setAppend(true);
        requestLog.setExtended(false);
        requestLog.setLogTimeZone("GMT+3");
        requestLogHandler.setRequestLog(requestLog);
    }

        public static void onShutDown() {
            log.info("Stopping server and application context");
            try {
                server.stop();
                log.info("Server has been stopped successfully");
                applicationContext.close();
                log.info("Application context has been closed successfully");
            } catch (Exception ex) {
                log.error("Error stopping server or closing application context", ex);
            }
        }
}

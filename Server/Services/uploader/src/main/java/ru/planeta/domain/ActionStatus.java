package ru.planeta.domain;

/**
 * Represents action status for json serializing
 *
 * @author ds.kolyshev
 * Date: 21.09.11
 */
public class ActionStatus<T> {

	public static <T> ActionStatus<T> createSuccessStatus() {
		ActionStatus<T> status = new ActionStatus<T>();
		status.setSuccess(true);
		return status;
	}

	public static <T> ActionStatus<T> createSuccessStatus(T result) {
		ActionStatus<T> status = createSuccessStatus();
		status.setResult(result);
		return status;
	}

	public static <T> ActionStatus<T> createErrorStatus(String message) {
		ActionStatus<T> status = new ActionStatus<T>();
		status.setSuccess(false);
		status.setErrorMessage(message);
		return status;
	}

	private boolean success;
	private String errorMessage;
	private T result;

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public T getResult() {
		return result;
	}

	public void setResult(T result) {
		this.result = result;
	}

}

package ru.planeta.service;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.im4java.core.IM4JavaException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import ru.planeta.api.exceptions.NotFoundException;
import ru.planeta.api.exceptions.PermissionException;
import ru.planeta.commons.external.oembed.provider.VideoType;
import ru.planeta.dao.TransactionScope;
import ru.planeta.dao.profiledb.BroadcastStreamDAO;
import ru.planeta.dao.profiledb.VideoDAO;
import ru.planeta.domain.VideoFile;
import ru.planeta.image.ImageOperationException;
import ru.planeta.model.Constants;
import ru.planeta.model.enums.*;
import ru.planeta.model.profile.broadcast.BroadcastStream;
import ru.planeta.model.profile.media.Photo;
import ru.planeta.model.profile.media.Video;
import ru.planeta.model.profile.media.enums.VideoConversionStatus;
import ru.planeta.model.profile.media.enums.VideoQuality;
import ru.planeta.video.VideoConfiguration;
import ru.planeta.video.VideoConversionConfiguration;
import ru.planeta.video.VideoConverter;
import ru.planeta.video.VideoInfo;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.util.EnumSet;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @author ds.kolyshev
 *         Date: 28.10.11
 */
@Service
public class VideoContentServiceImpl extends BaseService implements VideoContentService {

    @Value("${videos.path}")
    private String videosPath;
    @Value("${videos.url}")
    private String videosUrl;
    @Value("${videoValidExtensionsString}")
    private String videoValidExtensionsString;

    private ExecutorService executorService;

    private final ImageContentService imageContentService;
    private final VideoConversionConfiguration videoConversionConfiguration;
    private final BroadcastStreamDAO broadcastStreamDAO;
    private final VideoDAO videoDAO;

    @Autowired
    public VideoContentServiceImpl(VideoConversionConfiguration videoConversionConfiguration, ImageContentService imageContentService, BroadcastStreamDAO broadcastStreamDAO, VideoDAO videoDAO) {
        this.videoConversionConfiguration = videoConversionConfiguration;
        this.imageContentService = imageContentService;
        this.broadcastStreamDAO = broadcastStreamDAO;
        this.videoDAO = videoDAO;
    }

    private VideoDAO getVideoDAO() {
        return videoDAO;
    }


    @Value("${videoConverting.threadsCount}")
    public void setVideoConvertingThreadsCount(int videoConvertingThreadsCount) {
        executorService = Executors.newFixedThreadPool(videoConvertingThreadsCount);
    }

    public void setVideosPath(String videosPath) {
        this.videosPath = videosPath;
    }

    @Override
    public Video uploadVideo(File reqFile, String reqFileName, long clientId, long profileId) throws IOException, PermissionException, InterruptedException, NotFoundException {
        VideoFile videoFile = createVideo(reqFile, reqFileName, clientId, profileId, true);

        Video video = videoFile.getVideo();

        try {
            VideoConfiguration defaultConfiguration = videoConversionConfiguration.getDefaultConfiguration();
            addVideoPreview(clientId, VideoConversionStatus.PROCESSING, video, defaultConfiguration, videoFile.getFile());
        } catch (IM4JavaException | ImageOperationException e) {
            log.error(e);
        }

        video.setHeight(videoFile.getOriginalVideoInfo().getHeight());
        video.setWidth(videoFile.getOriginalVideoInfo().getWidth());

        scheduleVideoConversion(clientId, videoFile);
        return video;
    }

    @Override
    public Video uploadBroadcastStreamVideo(File reqFile, String reqFileName, long clientId, long profileId, long broadcastStreamId) throws IOException, PermissionException, InterruptedException, NotFoundException {
        VideoFile videoFile = createVideo(reqFile, reqFileName, clientId, profileId, false);

        scheduleVideoConversion(clientId, videoFile);

        Video video = videoFile.getVideo();
        updateBroadcastStreamVideo(broadcastStreamId, video);

        return video;
    }

    @Override
    public VideoFile createVideo(@Nullable File file, String reqFileName, long clientId, long profileId, boolean validateDuration) throws IOException, PermissionException, InterruptedException, NotFoundException {
        TransactionScope transactionScope = TransactionScope.createOrGetCurrent();
        try {
            if (!getPermissionService().checkAddPermission(clientId, profileId, BlockSettingType.VIDEO)) {
                throw new PermissionException();
            }

            if (!validateVideoFile(file, reqFileName)) {
                throw new IOException("File extension is not valid: " + reqFileName);
            }

            VideoInfo videoInfo = VideoConverter.getVideoInfo(file);
            if (file == null || videoInfo == null) {
                throw new IOException("File is null ");
            }
            if (validateDuration && !validateVideoDuration(videoInfo)) {
                throw new IOException("File not a video, or smth is wrong: " + file.getName());
            }

            //rename file to save original file extension
            File renamedFile = new File(file.getParent(), getNewFileName(reqFileName));
            FileUtils.moveFile(file, renamedFile);

            long videoId = getSequencesDAO().selectNextLong(Video.class);
            Video insertedVideo = insertVideo(videoId, clientId, profileId, renamedFile.getName(), videoInfo.getDuration(), reqFileName);

            //save renamed file to dir tree
            File targetFile = moveOriginalFile(renamedFile, videoId, videosPath);
            transactionScope.commit();

            VideoFile videoFile = new VideoFile();
            videoFile.setFile(targetFile);
            videoFile.setVideo(insertedVideo);
            videoFile.setOriginalVideoInfo(videoInfo);

            return videoFile;

        } catch (InterruptedException e) {
            log.error("File is not a compatible video!", e);
            throw e;
        } finally {
            transactionScope.close();
        }
    }


    /**
     * Asynchronous converting - schedules sync converting
     */
    private void scheduleVideoConversion(final long clientId, final VideoFile videoFile) {
        log.info("Client " + clientId + " schedule converting " + videoFile.getOriginalVideoInfo().getName());
        executorService.submit(new Runnable() {
            @Override
            public void run() {
                convertVideo(clientId, videoFile);
            }
        });
    }

    @Override
    public void convertVideo(long clientId, VideoFile videoFile) {
        log.info("Client " + clientId + " start converting " + videoFile.getOriginalVideoInfo().getName());
        boolean isScreenShotCaptured = false;
        VideoConversionStatus convertResult = VideoConversionStatus.PROCESSED;
        Video video = videoFile.getVideo();

        //convert starting from highest Possible
        //VideoConfiguration defaultConfiguration = videoConversionConfiguration.getDefaultConfiguration();
        VideoConfiguration minimumConfiguration = videoConversionConfiguration.getMinimumConfiguration();
        for (VideoConfiguration videoConfiguration : videoConversionConfiguration.getVideoConfigurations()) {
            log.debug("Converting video to " + videoConfiguration.getName());

            log.debug("Check if we can possibly convert to this bitrate");
            if (validateVideoConfiguration(videoFile, videoConfiguration, minimumConfiguration)) {
                String outFilePath = generateConvertedVideoFilePath(videoFile.getFile(), videoConfiguration);
                final File convertedVideoFile;
                try {
                    convertedVideoFile = VideoConverter.convertVideo(videoFile.getFile(), videoFile.getOriginalVideoInfo(), videoConfiguration, outFilePath);
                    if (convertedVideoFile != null) {
                        log.debug("Video has been converted");

                        if (!isScreenShotCaptured) {
                            convertResult = addVideoPreview(clientId, convertResult, video, videoConfiguration, convertedVideoFile);
                            convertResult = addVideoStoryBoard(clientId, convertResult, video, convertedVideoFile);
                            isScreenShotCaptured = true;
                        }

                        updateVideoQualityModes(video, videoConfiguration.getQualityMode());
                    } else {
                        log.warn("Video conversion failed..something strange happens");
                        convertResult = VideoConversionStatus.ERROR;
                    }

                } catch (Exception e) {
                    log.error("Video conversion failed", e);
                    convertResult = VideoConversionStatus.ERROR;
                }
            }
        }

        //set status
        updateVideoStatus(video, convertResult);
        log.info("Client " + clientId + " stop converting " + videoFile.getOriginalVideoInfo().getName());

    }

    static String generateConvertedVideoFilePath(File file, VideoConfiguration videoConfiguration) {
        return file.getParent() + "/" + videoConfiguration.getName() + ".mp4";
    }

    @Override
    public String getVideoImageUrl(long profileId, long videoId) {
        Video video = getVideoDAO().selectVideoById(profileId, videoId);
        if (video == null) {
            return null;
        }
        return video.getImageUrl();
    }


    /**
     * Validates video file
     *
     * @param fileName original file name
     */
    private boolean validateVideoFile(File file, String fileName) {
        log.debug("Validating file extension");
        for (String validExtension : videoValidExtensionsString.split(" ")) {
            if (file != null && fileName != null && fileName.toLowerCase().endsWith(validExtension)) {
                return true;
            }
        }

        log.debug("is null=" + (file == null));
        log.debug("is valid=false");
        return false;
    }

    /**
     * Validates video info
     *
     */
    private static boolean validateVideoDuration(VideoInfo videoInfo) {
        log.debug("Validating video headers");

        if (videoInfo == null) {
            log.debug("videoInfo is null");
            return false;
        }

        log.debug("Validating video duration");
        //should be more than 2 seconds
        if (videoInfo.getDuration() < 2) {
            log.debug("Video duration = " + Long.toString(videoInfo.getDuration()));
            return false;
        }

        log.debug("Ok");
        return true;
    }

    /**
     * Inserts new profile's video
     *
     */
    private Video insertVideo(long videoId, long clientId, long profile, String fileName, int duration, String reqFileName) throws UnsupportedEncodingException {
        String videoUrl = generateUrl(fileName, videoId, videosUrl);
        String videoName = FilenameUtils.getBaseName(reqFileName);
        Video video = new Video();
        video.setVideoId(videoId);
        video.setProfileId(profile);
        video.setAuthorProfileId(clientId);
        video.setAvailableQualities(EnumSet.of(VideoQuality.NOT_SET));
        video.setDescription("");
        video.setName(videoName);
        video.setImageId(0);
        video.setVideoUrl(videoUrl);
        video.setViewsCount(0);
        video.setStatus(VideoConversionStatus.PROCESSING);
        video.setDuration(duration);
        video.setVideoType(VideoType.PLANETA);
        video.setTimeAdded(new Date());

        getVideoDAO().insert(video);
        return video;
    }

    private static boolean validateVideoConfiguration(VideoFile videoFile, VideoConfiguration videoConfiguration, VideoConfiguration minimumConfiguration) {
        VideoInfo originalVideoInfo = videoFile.getOriginalVideoInfo();
        if (originalVideoInfo.compareTo(videoConfiguration) > 0
                && !videoConfiguration.equals(minimumConfiguration)) {
            log.warn("WARNING! Original video size width lesser than convert config(not minimum config)");
            return false;
        }
        return true;
    }

    /**
     * Captures screenshot from video and updates video
     *
     */
    private VideoConversionStatus addVideoPreview(long clientId, VideoConversionStatus convertationResult, Video video, VideoConfiguration videoConfiguration, File convertedVideoFile) throws IOException, InterruptedException, ImageOperationException, IM4JavaException, PermissionException, NotFoundException {
        File screenshotFile = VideoConverter.captureScreenshot(convertedVideoFile, videoConfiguration);
        if (screenshotFile != null) {
            log.debug("Screenshot captured");
            insertVideoPreview(clientId, screenshotFile, video);
        } else {
            log.error("ERROR: Cannot capture screenshot");
            return VideoConversionStatus.ERROR;
        }
        return convertationResult;
    }

    /**
     * Uploads screenshots then updates video imageId
     *
     */
    private void insertVideoPreview(long clientId, @Nonnull File screenshotFile, @Nonnull Video video) throws IOException, ImageOperationException, IM4JavaException, InterruptedException, PermissionException, NotFoundException {
        log.debug("Sending screenshots to ImageUploaderService");
        Photo uploadedScreenshot = imageContentService.uploadPhotoFile(screenshotFile, clientId, video.getProfileId(), 0L, Constants.INSTANCE.getALBUM_VIDEO_PREVIEWS(), video.getImageId(), null).getPhoto();

        if (uploadedScreenshot != null) {
            Video currentVideo = getVideoDAO().selectVideoById(video.getProfileId(), video.getVideoId());
            if (currentVideo != null) {
                currentVideo.setImageId(uploadedScreenshot.getPhotoId());
                currentVideo.setImageUrl(uploadedScreenshot.getImageUrl());
                getVideoDAO().update(currentVideo);
                log.debug("Video preview updated");
            }

            video.setImageId(uploadedScreenshot.getPhotoId());
            video.setImageUrl(uploadedScreenshot.getImageUrl());
            video.setTimeUpdated(new Date());
            if (currentVideo == null) {
                getVideoDAO().insert(video);
            }
        }

        log.debug("Screenshots saved");
    }

    /**
     * Captures storyboard screenshots from video and then updates video
     */
    private VideoConversionStatus addVideoStoryBoard(long clientId, VideoConversionStatus convertationResult, Video video, File convertedVideoFile) throws IOException, InterruptedException, IM4JavaException, ImageOperationException, PermissionException, NotFoundException {
        File storyboardFile = imageContentService.storyboardVideo(convertedVideoFile, video);
        if (storyboardFile != null) {
            log.debug("Storyboard captured");
            insertVideoStoryboard(clientId, storyboardFile, video);
        } else {
            log.error("ERROR: Cannot capture storyboard");
            return VideoConversionStatus.ERROR;
        }
        return convertationResult;
    }

    /**
     * Uploads storyboard image then updates video
     */
    private void insertVideoStoryboard(long clientId, @Nonnull File screenshotFile, Video video) throws IOException, ImageOperationException, IM4JavaException, InterruptedException, PermissionException, NotFoundException {
        log.debug("Sending storyboard to ImageUploaderService");
        Photo uploadedScreenshot = imageContentService.uploadPhotoFile(screenshotFile, clientId, video.getProfileId(), 0L, Constants.INSTANCE.getALBUM_VIDEO_PREVIEWS(), 0, null).getPhoto();

        if (uploadedScreenshot != null) {
            Video currentVideo = getVideoDAO().selectVideoById(video.getProfileId(), video.getVideoId());
            if (currentVideo != null) {
                currentVideo.setStoryboardUrl(uploadedScreenshot.getImageUrl());
                getVideoDAO().update(currentVideo);
                log.debug("Video storyboard updated");
            } else {
                log.error("No VideoStoryboard " + video.getVideoId());
            }

            video.setStoryboardUrl(uploadedScreenshot.getImageUrl());
            video.setTimeUpdated(new Date());
        }

        log.debug("Storyboard saved");
    }

    /**
     * Updates video quality modes
     */
    private void updateVideoQualityModes(Video video, int qualityMode) {
        TransactionScope transactionScope = TransactionScope.createOrGetCurrent();
        try {
            Video currentVideo = getVideoDAO().selectVideoById(video.getProfileId(), video.getVideoId());
            if (currentVideo != null) {
                currentVideo.getAvailableQualities().add(VideoQuality.Companion.getByValue(qualityMode));
                getVideoDAO().update(currentVideo);
            }
            transactionScope.commit();
        } catch (Exception e) {
            log.error("Error occured", e);
        } finally {
            transactionScope.close();
        }

    }

    /**
     * Updates video convertation status
     *
     */
    private void updateVideoStatus(Video video, final VideoConversionStatus videoStatus) {
        TransactionScope transactionScope = TransactionScope.createOrGetCurrent();
        try {
            Video currentVideo = getVideoDAO().selectVideoById(video.getProfileId(), video.getVideoId());
            if (currentVideo == null) {
                log.warn("No such video " + video.getVideoId());
                return;
            }

            currentVideo.setStatus(videoStatus);
            getVideoDAO().update(currentVideo);

            transactionScope.commit();
        } catch (Exception e) {
            log.error("Error occured", e);
        } finally {
            transactionScope.close();
        }
    }

    private void updateBroadcastStreamVideo(long broadcastStreamId, Video video) throws NotFoundException {
        BroadcastStream stream = broadcastStreamDAO.selectById(broadcastStreamId);
        if (stream == null) {
            throw new NotFoundException(BroadcastStream.class, broadcastStreamId);
        }
        stream.setVideoId(video.getVideoId());
        stream.setVideoUrl(video.getVideoUrl());
        broadcastStreamDAO.update(stream);
    }
}

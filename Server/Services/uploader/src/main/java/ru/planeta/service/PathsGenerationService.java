package ru.planeta.service;

import org.im4java.core.IM4JavaException;
import ru.planeta.image.ImageOperationException;

import java.io.IOException;

/**
 * File urls generator for servlet
 *
 * @author ds.kolyshev
 * Date: 19.10.11
 */
public interface PathsGenerationService {

    /**
     * Generates real url for content files for x-accel-redirect
     */
    String generateRedirectUrl(String uri, boolean flushCache) throws IOException, ImageOperationException, IM4JavaException, InterruptedException;

    String reGenerateRedirectUrl(String uri, boolean flushCache) throws IOException, ImageOperationException, IM4JavaException, InterruptedException;

    String generateFullFileName(String uri) throws IOException, ImageOperationException, IM4JavaException, InterruptedException;
}

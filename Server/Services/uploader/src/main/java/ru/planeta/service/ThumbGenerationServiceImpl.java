package ru.planeta.service;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.im4java.core.ConvertCmd;
import org.im4java.core.IM4JavaException;
import org.im4java.core.IMOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import ru.planeta.commons.concurrent.LockManager;
import ru.planeta.image.*;
import ru.planeta.model.Constants;
import ru.planeta.model.enums.ThumbnailType;
import ru.planeta.utils.ThumbConfiguration;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

/**
 * @author ds.kolyshev
 * Date: 20.09.11
 */
@Service
public class ThumbGenerationServiceImpl extends BaseService implements ThumbGenerationService {

	@Value("${images.path}")
    private String imagesPath;
	@Autowired
    private ImageConfigurationsMap imageConfigurationsMap;

    private LockManager lockManager = new LockManager();

    public void setImagesPath(String imagesPath) {
        this.imagesPath = imagesPath;
    }

    private ImageConfigurationsMap getImageConfigurationsMap() {
        return imageConfigurationsMap;
    }

    @Override
    public void generateThumbnails(File file, File uncroppedFile, boolean forceTransform, int albumTypeId, boolean noAnimate) throws ImageOperationException, IOException, InterruptedException, IM4JavaException {
        ImageConfiguration configuration = getImageConfigurationByAlbumType(albumTypeId);

        //generate "real" thumbnail
        ImageInfo imageInfo = new ImageInfo(file);
        BucketType thumbBucketType = BucketType.JPEG;
        if (imageInfo.getBucketType() == BucketType.GIF) {
            thumbBucketType = BucketType.GIF;
        }

        ThumbConfiguration realThumbConfiguration = configuration.getThumbConfigurationByName("real");
        File realFile = null;
        if (uncroppedFile != null) {
            realFile = new File(uncroppedFile.getParent(), realThumbConfiguration.getName() + "." + thumbBucketType.getExtension());
            convertImage(uncroppedFile, imageInfo, realFile, realThumbConfiguration, thumbBucketType, noAnimate);
        } else {
            realFile = new File(file.getParent(), realThumbConfiguration.getName() + "." + thumbBucketType.getExtension());
            convertImage(file, imageInfo, realFile, realThumbConfiguration, thumbBucketType, noAnimate);
        }

        //generate other thumbnails
        ImageInfo originalImageInfo = new ImageInfo(file);
        BucketType originalThumbBucketType = BucketType.JPEG;
        if (originalImageInfo.getBucketType() == BucketType.GIF) {
            originalThumbBucketType = BucketType.GIF;
        }

        for (ThumbConfiguration thumbConfiguration : configuration.getThumbConfigurationsWithoutReal()) {
            File thumbFile = new File(file.getParent(), thumbConfiguration.getName() + "." + originalThumbBucketType.getExtension());
            if (forceTransform || !thumbFile.exists()) {
                convertImage(file, originalImageInfo, thumbFile, thumbConfiguration, originalThumbBucketType, noAnimate);
            }
        }
    }

    private ImageConfiguration getImageConfigurationByAlbumType(int albumTypeId) {
        ImageConfiguration imageConfiguration = getImageConfigurationsMap().getConfigurations().get(albumTypeId);
        if (imageConfiguration == null) {
            imageConfiguration = getImageConfigurationsMap().getDefaultImageConfiguration();
        }
        return imageConfiguration;
    }

    @Override
    public void generateThumbnails(boolean forceTransform) throws IOException, ImageOperationException, IM4JavaException, InterruptedException {
        log.debug("Generating thumbnails..");
        generateThumbnailsRecursive(new File(imagesPath), forceTransform);
    }

    @Override
    public File generateOneThumbnail(File originalFile, File targetFile, ThumbConfiguration thumbConfiguration, boolean forceTransform) throws ImageOperationException, IOException, InterruptedException, IM4JavaException {
        ImageInfo imageInfo = new ImageInfo(originalFile);

        BucketType thumbBucketType = BucketType.JPEG;
        if (imageInfo.getBucketType() == BucketType.GIF) {
            thumbBucketType = BucketType.GIF;
        }

        if (!forceTransform && targetFile.exists()) {
            return targetFile;
        }

        convertImage(originalFile, imageInfo, targetFile, thumbConfiguration, thumbBucketType, false);
        return targetFile;
    }

    @Override
    public File checkThumbnail(long fileId, String targetFileName, boolean flushCache) throws UnsupportedEncodingException {
        //get target file
        File targetFile = new File(generateFullFilePathToCopy(targetFileName, fileId, imagesPath));
        if (targetFile.exists()) {
            return targetFile;
        }

        //get original file
        String extension = ".jpg";
        int lastPointIndex = targetFileName.lastIndexOf(".");
        if (lastPointIndex > 0) {
            extension = targetFileName.substring(lastPointIndex);
        }
		extension = ".gif".equals(extension) ? extension : ".jpg";
        File originalFile = new File(targetFile.getParent(), ThumbnailType.REAL.getThumbFileName() + extension);
        if (!originalFile.exists()) {
			originalFile = new File(targetFile.getParent(), ThumbnailType.ORIGINAL.getThumbFileName() + extension);
			if (!originalFile.exists()) {
				log.debug("Cannot find nether original or real file for:" + targetFile.getPath());
				return null;
			}
        }

        //get thumb configuration
        String fileName = lastPointIndex > 0 ? targetFileName.substring(0, lastPointIndex) : targetFileName;
        fileName = fileName.replace("\\", "/").replace("//", "/");
        if (fileName.startsWith("/")) {
            fileName = fileName.substring(1);
        }
        ThumbConfiguration thumbConfiguration = getImageConfigurationsMap().getDefaultImageConfiguration().getThumbConfigurationByName(fileName);
        if (thumbConfiguration == null) {
            return null;
        }

        lockManager.acquireExclusiveLock(fileId);
        try {
            return generateOneThumbnail(originalFile, targetFile, thumbConfiguration, flushCache);
        } catch (Exception e) {
			log.debug("Cannot generate thumb for " + originalFile.getPath() + " : " + e.getMessage());
            return null;
        } finally {
            lockManager.releaseExclusiveLock(fileId);
        }
    }

    private void generateThumbnailsRecursive(File directory, boolean forceTransform) throws IOException, ImageOperationException, IM4JavaException, InterruptedException {
        File[] listFiles = directory.listFiles();
        if (listFiles == null) {
            return;
        }

		File realFile = null;
		File originalFile = null;
        for (File listFile : listFiles) {
            if (listFile.isDirectory()) {
                generateThumbnailsRecursive(listFile, forceTransform);
            } else {
                String fileName = listFile.getName();
                int pointIndex = fileName.lastIndexOf(".");
                if (pointIndex > 0 && fileName.substring(0, pointIndex).equals(ThumbnailType.ORIGINAL.getThumbFileName())) {
					originalFile = listFile;
                } else if (pointIndex > 0 && fileName.substring(0, pointIndex).equals(ThumbnailType.REAL.getThumbFileName())) {
					realFile = listFile;
				}
            }
        }

		if (originalFile != null && realFile == null) {
			//NOTE: This is for migration thumbs generation from original to real
			int lastPointIndex = originalFile.getName().lastIndexOf(".");
			String extension = "";
			if (lastPointIndex > 0) {
				extension = originalFile.getName().substring(lastPointIndex);
			}
			realFile = new File(originalFile.getParent(), ThumbnailType.REAL.getThumbFileName() + extension);
			log.debug("Copy original to real: " + realFile.getPath());
			FileUtils.copyFile(originalFile, realFile);
		}
		if (realFile != null) {
			log.debug("Checking image: " + realFile.getPath());
			generateThumbnails(realFile, null, forceTransform, Constants.INSTANCE.getALBUM_DEFAULT(), false);
		}
    }

    private void convertImage(File originalFile, ImageInfo imageInfo, File targetFile, ThumbConfiguration thumbConfiguration, BucketType thumbBucketType, boolean noAnimate) throws IOException, InterruptedException, IM4JavaException {
        log.debug("Generating thumbnail: " + targetFile.getPath());

        int thumbWidth = thumbConfiguration.getWidth();
        int thumbHeight = thumbConfiguration.getHeight();

        IMOperation op = new IMOperation();
        op.addImage(originalFile.getAbsolutePath());
        op.strip();
        op.autoOrient();
        if (imageInfo.getBucketType() == BucketType.JPEG) {
            op.interlace("Plane");
        }

        if (imageInfo.getBucketType() == BucketType.JPEG || imageInfo.getBucketType() == BucketType.PNG) {
            op.flatten();
        }

        if (thumbWidth > 0 && thumbHeight > 0) {
            float imageWidth = (float) imageInfo.getWidth();
            float imageHeight = (float) imageInfo.getHeight();

            if(!thumbConfiguration.isCrop() && thumbConfiguration.isPad()){
                op.bordercolor(thumbConfiguration.getBackgroundColor());
                op.compose("copy");
                if (imageWidth / imageHeight > (float) thumbWidth / (float) thumbHeight) {
                    int determinateHeight = Math.round((thumbWidth*imageHeight/imageWidth));
                    if ((thumbHeight % 2 == 0) && ((determinateHeight % 2) != 0) ||
                            (thumbHeight % 2 != 0) && ((determinateHeight % 2) == 0)) {
                        int determineWidth = Math.round(++determinateHeight*imageWidth/imageHeight);
                        op.resize(determineWidth);
                        op.crop(thumbConfiguration.getWidth(), determinateHeight, 0 ,0);
                    } else {
                        op.resize(thumbWidth);
                    }

                    final float bordersSize = thumbHeight - determinateHeight;
                    op.border(0, (int)(bordersSize/2));
                } else {
                    int determinateWidth = Math.round((thumbHeight*imageWidth/imageHeight));
                    if ((thumbWidth % 2 == 0) && ((determinateWidth % 2) != 0) ||
                            (thumbWidth % 2 != 0) && ((determinateWidth % 2) == 0)) {
                        int determinateHeight = Math.round(++determinateWidth*imageHeight/imageWidth);
                        op.resize(determinateWidth);
                        op.crop(determinateWidth, thumbConfiguration.getHeight(), 0 ,0);
                    } else {
                        op.resize(determinateWidth, thumbHeight);
                    }

                    final float bordersSize = thumbWidth - determinateWidth;
                    op.border((int)(bordersSize/2), 0);
                }
            } else {
                if (imageWidth / imageHeight > (float) thumbWidth / (float) thumbHeight) {
                    thumbWidth = getImageConfigurationsMap().getFlagSaveSide();

                } else {
                    thumbHeight = getImageConfigurationsMap().getFlagSaveSide();
                }

                if (thumbHeight < imageHeight || thumbWidth < imageWidth) {
                    addResizeOperations(op, thumbWidth, thumbHeight, imageInfo);
                    if (thumbConfiguration.isCrop()) {
                        op.crop(thumbConfiguration.getWidth(), thumbConfiguration.getHeight(), 0, 0);
                    }
                    op.p_repage();
                } else {
                    //adjust images if smaller side is less than thumb's side(regulated by crop param)
                    if (thumbConfiguration.isCrop()) {
                        addResizeOperations(op, thumbWidth, thumbHeight, imageInfo);
                        op.crop(thumbConfiguration.getWidth(), thumbConfiguration.getHeight(), 0, 0);
                        op.p_repage();
                    }
                }
                // set black paddings if aspect ratio is other
                if(thumbConfiguration.isPad()){
                    if(imageWidth > thumbConfiguration.getWidth()){
                        op.resize();
                    }
                }
            }

        } else if (thumbWidth > 0 && thumbHeight == 0 && thumbWidth < imageInfo.getWidth()) {
            addResizeOperations(op, thumbWidth, getImageConfigurationsMap().getFlagSaveSide(), imageInfo);
        } else if (thumbHeight > 0 && thumbWidth == 0 && thumbHeight < imageInfo.getHeight()) {
            addResizeOperations(op, getImageConfigurationsMap().getFlagSaveSide(), thumbHeight, imageInfo);
        }

        //for disabled animation gif - we tak only first frame
        if ((thumbConfiguration.isDisableAnimatedGif() || noAnimate) && imageInfo.getBucketType() == BucketType.GIF) {
            op.delete("1--1");
        }

        if (imageInfo.getBucketType() == BucketType.JPEG) {
            op.quality(85d);
        }

        op.addImage(thumbBucketType.getExtension() + ":" + targetFile.getAbsolutePath());

        ConvertCmd convertCmd = new ConvertCmd();
        log.debug("cmd: convert " + StringUtils.join(op.getCmdArgs(), " "));
        convertCmd.run(op);
    }

    /**
     * Adds resize operations depending on imageInfo
     *
     */
    private static void addResizeOperations(IMOperation op, int thumbWidth, int thumbHeight, ImageInfo imageInfo) {
        if (imageInfo.getBucketType() == BucketType.GIF) {
            op.coalesce();
            op.thumbnail(thumbWidth, thumbHeight);
        } else {
            op.resize(thumbWidth, thumbHeight);
        }
    }
}

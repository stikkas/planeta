package ru.planeta.service;

import ru.planeta.api.exceptions.NotFoundException;
import ru.planeta.api.exceptions.PermissionException;
import ru.planeta.domain.VideoFile;
import ru.planeta.model.profile.media.Video;

import java.io.File;
import java.io.IOException;

/**
 * @author ds.kolyshev
 * Date: 28.10.11
 */
public interface VideoContentService {

	/**
	 * Uploads video file
	 *
     * @param reqFileName original file name
	 */
	Video uploadVideo(File reqFile, String reqFileName, long clientId, long profileId) throws IOException, PermissionException, InterruptedException, NotFoundException;

    /**
     * Uploads video file and add link to broadcast stream
     */
    Video uploadBroadcastStreamVideo(File reqFile, String reqFileName, long clientId, long profileId, long broadcastStreamId) throws IOException, PermissionException, InterruptedException, NotFoundException;

	/**
     * Inserts db record - video in status processing
     * Makes directories and copies original file
     *
     * On failure sets error status
     * @param validateDuration flag video duration validation needed not to check broadcast dump results
     */
    VideoFile createVideo(File file, String reqFileName, long clientId, long profileId, boolean validateDuration) throws IOException, PermissionException, InterruptedException, NotFoundException;

    /**
     * Synchronized converting
     */
    void convertVideo(long clientId, VideoFile videoFile) throws IOException, InterruptedException;

	String getVideoImageUrl(long profileId, long videoId);
}

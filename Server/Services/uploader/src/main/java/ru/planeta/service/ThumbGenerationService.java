package ru.planeta.service;

import org.im4java.core.IM4JavaException;
import ru.planeta.image.ImageOperationException;
import ru.planeta.utils.ThumbConfiguration;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

/**
 * @author ds.kolyshev
 * Date: 20.09.11
 */
public interface ThumbGenerationService {

    /**
     * Generates thumbnails for image file
     *
     * @param file original file
     * @param uncroppedFile
     * @param forceTransform overwrite existing files flag
     * @param albumTypeId
     * @throws ImageOperationException
     * @throws IOException
     * @throws InterruptedException
     * @throws IM4JavaException
     */
    void generateThumbnails(File file, File uncroppedFile, boolean forceTransform, int albumTypeId, boolean noAnimate) throws ImageOperationException, IOException, InterruptedException, IM4JavaException;

    /**
     * Generates thumbnails for all images in images.path
     *
     * @param forceTransform overwrite existing files flag
     */
    void generateThumbnails(boolean forceTransform) throws IOException, ImageOperationException, IM4JavaException, InterruptedException;


    File generateOneThumbnail(File originalFile, File targetFile, ThumbConfiguration thumbConfiguration, boolean forceTransform) throws ImageOperationException, IOException, InterruptedException, IM4JavaException;

    /**
     * Checks thumb's file existence and generates if not file not exists
     * @param fileId
     * @param targetFileName
     * @param flushCache
     * @return
     */
    File checkThumbnail(long fileId, String targetFileName, boolean flushCache) throws UnsupportedEncodingException;


}

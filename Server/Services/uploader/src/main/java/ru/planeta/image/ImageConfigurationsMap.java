package ru.planeta.image;

import java.util.Map;

/**
 * Class for map configs of image configurations
 *
 * @author ds.kolyshev
 * Date: 03.05.12
 */
public class ImageConfigurationsMap {

    private Map<Integer, ImageConfiguration> configurations;
    private int flagSaveSide;

    public void setConfigurations(Map<Integer, ImageConfiguration> configurations) {
        this.configurations = configurations;
    }

    public Map<Integer, ImageConfiguration> getConfigurations() {
        return configurations;
    }

    public void setFlagSaveSide(int flagSaveSide) {
        this.flagSaveSide = flagSaveSide;
    }

    public int getFlagSaveSide() {
        return flagSaveSide;
    }

    public ImageConfiguration getDefaultImageConfiguration() {
        return this.configurations.get(0);
    }
}

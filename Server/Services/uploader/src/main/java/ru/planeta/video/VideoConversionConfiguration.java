package ru.planeta.video;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Video convertation configuration
 *
 * @author ds.kolyshev
 * Date: 08.09.11
 */
public class VideoConversionConfiguration {

    private List<VideoConfiguration> videoConfigurations;
    private VideoConfiguration defaultConfiguration;
    private VideoConfiguration minimumConfiguration;

    public void setVideoConfigurations(List<VideoConfiguration> videoConfigurations) {
        this.videoConfigurations = videoConfigurations;
        this.selectDefaultConfiguration(videoConfigurations);
        this.selectMinimumConfiguration(videoConfigurations);
    }

    private void selectDefaultConfiguration(List<VideoConfiguration> videoConfigurations) {
        for (VideoConfiguration videoConfiguration : videoConfigurations) {
            if (videoConfiguration.isDefaultConfiguration()) {
                this.defaultConfiguration = videoConfiguration;
                break;
            }
        }
        if (defaultConfiguration == null) {
            defaultConfiguration = videoConfigurations.get(0);
        }
    }

    private void selectMinimumConfiguration(List<VideoConfiguration> videoConfigurations) {

        List<VideoConfiguration> list = new ArrayList<>(videoConfigurations);
        // Sort better to lesser
        Collections.sort(list);
        this.minimumConfiguration = list.get(list.size() - 1);
    }

    public List<VideoConfiguration> getVideoConfigurations() {
        return videoConfigurations;
    }

    public VideoConfiguration getDefaultConfiguration() {
        return defaultConfiguration;
    }

    public VideoConfiguration getMinimumConfiguration() {
        return minimumConfiguration;
    }

}

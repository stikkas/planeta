package ru.planeta.domain;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;

/**
 * @author ds.kolyshev
 * Date: 26.10.11
 */
public enum UploadFilesRequestKey {
	PROFILE_IMAGE("/uploadImage"),
	PROFILE_AUDIO("/uploadAudio"),
	PROFILE_VIDEO("/uploadVideo"),
	PROFILE_FILE("/uploadFile");

	private String requestKey;

	private UploadFilesRequestKey(String requestKey) {
		this.requestKey = requestKey;
	}

	private static final Map<String, UploadFilesRequestKey> lookup = new HashMap<String, UploadFilesRequestKey>();
	static {
		for (UploadFilesRequestKey s : EnumSet.allOf(UploadFilesRequestKey.class)) {
			lookup.put(s.requestKey, s);
		}
	}
	public static UploadFilesRequestKey getByValue(String key) {
		return lookup.get(key);
	}
}

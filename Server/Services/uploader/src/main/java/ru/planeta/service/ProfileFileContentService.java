package ru.planeta.service;

import ru.planeta.api.exceptions.PermissionException;
import ru.planeta.domain.ProfileFileModel;
import ru.planeta.model.profile.ProfileFile;

import java.io.File;
import java.io.IOException;

/**
 * @author: ds.kolyshev
 * Date: 20.03.13
 */
public interface ProfileFileContentService {

	/**
	 * Uploads profile file
	 *
	 * @param reqFile
	 * @param reqFileName
	 * @param clientId
	 * @param ownerId
	 * @return
	 */
	ProfileFile uploadProfileFile(File reqFile, String reqFileName, long clientId, long ownerId) throws PermissionException, IOException;

	/**
	 * Uploads profile file model
	 *
	 * @param reqFile
	 * @param reqFileName
	 * @param clientId
	 * @param ownerId
	 * @return
	 */
	ProfileFileModel uploadProfileFileModel(File reqFile, String reqFileName, long clientId, long ownerId) throws PermissionException, IOException;
}

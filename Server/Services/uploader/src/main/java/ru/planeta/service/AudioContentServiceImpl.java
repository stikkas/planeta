package ru.planeta.service;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.log4j.Logger;
import org.jaudiotagger.audio.AudioFileIO;
import org.jaudiotagger.audio.AudioHeader;
import org.jaudiotagger.tag.FieldKey;
import org.jaudiotagger.tag.Tag;
import org.jaudiotagger.tag.TagField;
import org.jaudiotagger.tag.id3.AbstractTagFrame;
import org.jaudiotagger.tag.id3.ID3v1TagField;
import org.jaudiotagger.tag.vorbiscomment.VorbisCommentTagField;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import ru.planeta.api.exceptions.NotFoundException;
import ru.planeta.api.exceptions.PermissionException;
import ru.planeta.api.service.content.AudioService;
import ru.planeta.dao.profiledb.AudioTrackDAO;
import ru.planeta.domain.AudioFile;
import ru.planeta.model.enums.BlockSettingType;
import ru.planeta.model.profile.media.AudioTrack;

import java.io.*;
import java.net.URL;
import java.util.Date;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @author ds.kolyshev
 *         Date: 28.10.11
 */
@Service
public class AudioContentServiceImpl extends BaseService implements AudioContentService {


    private static Logger log = Logger.getLogger(AudioContentServiceImpl.class);

    private String audioTracksPath;
    private String audioTracksUrl;
    private String tempDirectoryPath;
    private ExecutorService executorService;

    private final AudioService audioService;
    private final AudioTrackDAO audioTrackDAO;

    @Autowired
    public AudioContentServiceImpl(AudioService audioService, AudioTrackDAO audioTrackDAO) {
        this.audioService = audioService;
        this.audioTrackDAO = audioTrackDAO;
    }

    private AudioTrackDAO getAudioTrackDAO() {
        return audioTrackDAO;
    }

    @Value("${audiotracks.path}")
    public void setAudioTracksPath(String audioTracksPath) {
        this.audioTracksPath = audioTracksPath;
    }

    @Value("${audiotracks.url}")
    public void setAudioTracksUrl(String audioTracksUrl) {
        this.audioTracksUrl = audioTracksUrl;
    }

    @Value("${tempDirectoryPath}")
    public void setTempDirectoryPath(String tempDirectoryPath) throws IOException {
        this.tempDirectoryPath = tempDirectoryPath;
        File file = new File(tempDirectoryPath);
        if (!file.exists()) {
            boolean created = file.mkdirs();
            if (!created) {
                throw new IOException();
            }
        }
    }

    @Value("${audioConverting.threadsCount}")
    public void setAudioConvertingThreadsCount(int audioConvertingThreadsCount) {
        executorService = Executors.newFixedThreadPool(audioConvertingThreadsCount);
    }

    @Override
    public AudioTrack uploadAudioTrack(File file, String fileName, long clientId, long profileId, long albumId) throws IOException, PermissionException {
        AudioFile audioFile = uploadAudioTrackFile(file, fileName, clientId, profileId, albumId);
        AudioTrack audioTrack = audioFile.getAudioTrack();
        if (!audioTrack.isHasMp3()) {
            scheduleAudioConvertation(clientId, audioFile);
        }

        return audioTrack;
    }

    @Override
    public void scheduleAudioConvertation(final long clientId, final AudioFile audioFile) {
        executorService.submit(new Runnable() {

            @Override
            public void run() {
                try {
                    convertAudio(audioFile);
                } catch (Exception e) {
                    log.error("Error due to converitng audio was swallowed.", e);
                }
            }
        });
    }

    void convertAudio(AudioFile audioFile) throws Exception {
        log.debug("Converting audio to mp3");

        File inputFile = audioFile.getFile();
        //int dotIndex = inputFile.getPath().lastIndexOf('.');
        //String outFilePath = inputFile.getPath().substring(0, dotIndex) + ".mp3";

        String trackOriginalFileName = getTrackMp3FileName(audioFile.getAudioTrack());
        String outFilePath = generateFullFilePathToCopy(trackOriginalFileName, audioFile.getAudioTrack().getTrackId(), audioTracksPath);

        File mp3File = convertToMp3(inputFile, outFilePath);
        if (mp3File != null) {
            updateAudioConvertation(audioFile.getAudioTrack(), mp3File);
        } else {
            log.debug("Audio convertation failed..smth strange happens");
        }
    }

    @Override
    public File getAudioTrackFile(AudioTrack track) throws IOException {
        String trackOriginalFileName = getTrackMp3FileName(track);
        log.debug("Track:" + trackOriginalFileName);
        File trackFile = new File(generateFullFilePathToCopy(trackOriginalFileName, track.getTrackId(), audioTracksPath));
        if (!trackFile.exists()) {
            log.debug("File doesn't exist:" + trackFile.getAbsolutePath());
            FileUtils.copyURLToFile(new URL(track.getTrackUrl()), trackFile);
        }
        return trackFile;
    }

    @Override
    public void reconvertAudio(final long clientId, long ownerId, long trackId) throws NotFoundException {
        AudioTrack audioTrack = audioService.getAudioTrackSafe(clientId, ownerId, trackId);
        String trackUrl = audioTrack.getTrackUrl();
        String fileName = trackUrl.substring(trackUrl.lastIndexOf("/"));
        File file = new File(generateFullFilePathToCopy(fileName, trackId, audioTracksPath));
        if (!file.exists()) {
            throw new NotFoundException("audio file", trackId);
        }

        final AudioFile audioFile = new AudioFile();
        audioFile.setFile(file);
        audioFile.setAudioTrack(audioTrack);

        executorService.submit(new Runnable() {
            @Override
            public void run() {
                try {
                    convertAudio(audioFile);
                } catch (Exception e) {
                    log.error("Error due to converitng audio was swallowed.", e);
                }
            }
        });
    }

    AudioFile uploadAudioTrackFile(File file, String fileName, long clientId, long profileId, long albumId) throws IOException, PermissionException {
        if (!getPermissionService().checkAddPermission(clientId, profileId, BlockSettingType.AUDIO)) {
            throw new PermissionException();
        }

        File renamedFile = new File(tempDirectoryPath, file.getName() + fileName.substring(fileName.lastIndexOf('.')));
        moveUploadedFile(file, renamedFile);
        file = renamedFile;

        String defaultTrackName = fileName.substring(0, fileName.lastIndexOf('.'));
        AudioTrack audioTrackObject = parseAudioInfo(file, defaultTrackName);

        long trackId = getSequencesDAO().selectNextLong(AudioTrack.class);
        AudioTrack track = insertAudioTrack(clientId, profileId, trackId, file.getName(), audioTrackObject, albumId);

        if (albumId == 0) {
            // switching users tracks positions according to added track
            audioService.switchTracksPositionsAfterAddTrack(clientId, profileId);
        }


        //Saving file
        File targetFile = moveOriginalFile(file, trackId, audioTracksPath);
        AudioFile audioFile = new AudioFile();
        audioFile.setFile(targetFile);
        audioFile.setAudioTrack(track);
        return audioFile;
    }

    private static String getTrackMp3FileName(AudioTrack track) {
        String trackUrl = track.getTrackUrl();
        String trackOriginalFileName = trackUrl.substring(trackUrl.lastIndexOf("/") + 1);
        if (!trackOriginalFileName.endsWith(".mp3")) {
            trackOriginalFileName = trackOriginalFileName.substring(0, trackOriginalFileName.lastIndexOf(".")) + ".mp3";
        }
        return trackOriginalFileName;
    }

    /**
     * Converts specified file to mp3
     *
     */
    private static File convertToMp3(File inputFile, String outFilePath) throws IOException, InterruptedException {
        File targetFile = new File(outFilePath);
        if (!targetFile.getParentFile().exists()) {
            boolean created = targetFile.getParentFile().mkdirs();
            if (!created) {
                throw new IOException();
            }
        }

        File tempFile = new File(targetFile.getParentFile(), new Date().getTime() + ".mp3");

        //ffmpeg -i infile -y outfile
        String[] command = {
                "ffmpeg",
                "-i", inputFile.getAbsolutePath(),
                "-y",
                tempFile.getPath()};

        //convert ogg files with: ffmpeg -i infile.ogg -map_metadata 0:0,s0 -y outfile.mp3
        if (inputFile.getAbsolutePath().endsWith(".ogg")) {
            command = new String[]{
                    "ffmpeg",
                    "-i", inputFile.getAbsolutePath(),
                    "-map_metadata",
                    "0:0,s0",
                    "-y",
                    tempFile.getPath()};
        }

        Process process = null;
        try {

            process = Runtime.getRuntime().exec(command);

            InputStream stderr = process.getErrorStream();
            InputStreamReader inputStreamReader = new InputStreamReader(stderr);
            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
            String line;
            log.debug("<OUT>");
            while ((line = bufferedReader.readLine()) != null) {
                log.debug(line);
            }
            log.debug("</OUT>");

            int exitValue = process.waitFor();
            log.debug("Process exitValue: " + exitValue);

            if (exitValue != 0) {
                return null;
            }

        } finally {
            if (process != null) {
                process.destroy();
            }
        }

        FileUtils.moveFile(tempFile, targetFile);
        return targetFile;
    }

    /**
     * Updates audio track from mp3 parse results
     */
    private void updateAudioConvertation(AudioTrack audioTrack, File mp3File) throws IOException {
        AudioTrack currentTrack = getAudioTrackDAO().selectAudioTrackById(audioTrack.getProfileId(), audioTrack.getTrackId());
        AudioTrack info = parseAudioInfo(mp3File, audioTrack.getTrackName());
        currentTrack.setHasMp3(info.isHasMp3());
        currentTrack.setArtistName(info.getArtistName());
        currentTrack.setTrackName(info.getTrackName());
        currentTrack.setTrackDuration(info.getTrackDuration());
        currentTrack.setTrackUrl(generateUrl(mp3File.getName(), audioTrack.getTrackId(), audioTracksUrl));
        getAudioTrackDAO().update(currentTrack);
    }

    /**
     * Inserts new profile audio track
     */
    private AudioTrack insertAudioTrack(long clientId, long profileId, long trackId, String fileName, AudioTrack audioTrack, long albumId) throws UnsupportedEncodingException {
        String trackUrl = generateUrl(fileName, trackId, audioTracksUrl);

        AudioTrack track = new AudioTrack();
        track.setProfileId(profileId);
        track.setTrackId(trackId);
        track.setTrackUrl(trackUrl);
        track.setArtistName(audioTrack.getArtistName());
        track.setAuthors(audioTrack.getAuthors());
        track.setTrackDuration(audioTrack.getTrackDuration());
        track.setTrackName(audioTrack.getTrackName());
        track.setAlbumId(albumId);
        track.setAuthorProfileId(clientId);
        track.setUploaderProfileId(profileId);
        track.setUploaderTrackId(trackId);
        track.setUploaderAlbumId(albumId);
        track.setHasMp3(audioTrack.isHasMp3());
        track.setAlbumTrackNumber(1);

        getAudioTrackDAO().insert(track);
        return track;
    }

    /**
     * Parse info from audio tags
     *
     * @param defaultTrackName track name to be set if no tag exists
     */
    private static AudioTrack parseAudioInfo(File file, String defaultTrackName) {
        String artistName = "";
        String trackName = "";
        String authors = "";
        int albumTrackNumber = 0;
        int trackDuration = 0;
        boolean isMp3 = false;

        try {
            org.jaudiotagger.audio.AudioFile f = AudioFileIO.read(file);
            Tag tag = f.getTag();

            AudioHeader audioHeader = f.getAudioHeader();
            if (audioHeader != null) {
                trackDuration = audioHeader.getTrackLength();
                if ("MPEG-1 Layer 3".equals(audioHeader.getFormat()) || "MPEG-2 Layer 3".equals(audioHeader.getFormat())) {
                    isMp3 = true;

                    //if file is mp3 - parse tags, else parse tags after convertation
                    try {
                        if (tag != null) {
                            artistName = parseTagField(tag, FieldKey.ARTIST);
                            trackName = parseTagField(tag, FieldKey.TITLE);
                            authors = parseTagField(tag, FieldKey.COMPOSER);
                            String trackNumber = parseTagField(tag, FieldKey.TRACK);
                            if (trackNumber != null && trackNumber.contains("/")) {
                                trackNumber = trackNumber.substring(0, trackNumber.indexOf("/"));
                            }
                            albumTrackNumber = NumberUtils.toInt(trackNumber, 0);
                        }
                    } catch (Exception ex) {
                        log.debug("Error parsing audio file info: " + ex.getMessage());
                    }
                } else {
                    trackName = defaultTrackName;
                }
            }

        } catch (Exception ex) {
            log.debug("Error parsing audio file header: " + ex.getMessage());
        }

        AudioTrack audioTrack = new AudioTrack();
        audioTrack.setArtistName(artistName);

        audioTrack.setTrackName(trackName.isEmpty() ? defaultTrackName : trackName);
        audioTrack.setAuthors(authors);
        audioTrack.setTrackDuration(trackDuration);
        audioTrack.setHasMp3(isMp3);
        audioTrack.setAlbumTrackNumber(albumTrackNumber);
        return audioTrack;
    }

    /**
     * Parses info from jaudiotagger field in all encodings
     *
     */
    private static String parseTagField(Tag tag, FieldKey fieldKey) throws UnsupportedEncodingException {
        TagField tagField = tag.getFirstField(fieldKey);

        if (tagField == null) {
            return "";
        }

        if (tagField instanceof AbstractTagFrame) {
            AbstractTagFrame frame = (AbstractTagFrame) tagField;
            if (frame.getBody() != null) {
                String text = frame.getBody().getObjectValue("Text").toString();
                if (frame.getBody().getTextEncoding() == 1) {
                    return text.replace("\u0000", "");
                } else {
                    byte[] bytes = text.getBytes("UTF32");
                    return new String(bytes, "cp1251").replace("\u0000", "");
                }
            }
        }

        int firstValueIndex = 12;
        if (fieldKey == FieldKey.TITLE
                && (tagField instanceof ID3v1TagField || tagField instanceof VorbisCommentTagField)) {
            firstValueIndex = 11;
        }

        byte[] bytes = tagField.getRawContent();
        if (bytes.length < firstValueIndex) {
            return "";
        }

        byte[] sbytes = ArrayUtils.subarray(bytes, firstValueIndex - 1, bytes.length);
        return getEncodedString(sbytes);
    }

    /**
     * Attempts to detect correct string encoding and return encoded string
     *
     */
    private static String getEncodedString(byte[] bytes) throws UnsupportedEncodingException {
        String fieldValueEncoded = new String(bytes, "UTF8");
        if (fieldValueEncoded.contains("\ufffd")) {
            //Most possible - it is not utf8 encoded string
            fieldValueEncoded = new String(bytes, "cp1251");
            if (fieldValueEncoded.contains("\u0000")
                    || fieldValueEncoded.contains("\u0004")) {
                //Most possible - it is not cp1251 encoded string
                fieldValueEncoded = new String(bytes, "UTF16");
            }
        }
        return fieldValueEncoded.replace("\u0000", "");
    }

}


package ru.planeta.image;

/**
 *
 * @author ameshkov
 */
public class ImageOperationException extends Exception {

	public ImageOperationException() {
	}

	public ImageOperationException(String message) {
		super(message);
	}

	public ImageOperationException(Throwable cause) {
		super(cause);
	}

	public ImageOperationException(String message, Throwable cause) {
		super(message, cause);
	}
}

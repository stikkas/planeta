package ru.planeta.video;

import org.apache.commons.lang3.SystemUtils;
import org.apache.log4j.Logger;
import ru.planeta.commons.lang.StringUtils;
import ru.planeta.commons.math.SizeUtils;
import ru.planeta.model.profile.media.Video;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.io.*;

/**
 * Video converter class.
 * Wraps ffmpeg commands and do some video specific stuff.
 *
 * @author ds.kolyshev
 * Date: 12.09.11
 */
public class VideoConverter {
    private static final Logger log = Logger.getLogger(VideoConverter.class);

    /**
     * Gets video info
     *
     * @param file video file
     * @return parsed video info
     */
    @Nullable
    public static VideoInfo getVideoInfo(File file) throws InterruptedException, IOException {

        final String[] command = {
                "ffmpeg",
                "-i", file.getAbsolutePath(),
        };

        Process process = null;
        try {
            log.debug("Executing: " + StringUtils.arrayToString(command, " "));
            process = Runtime.getRuntime().exec(command);

            InputStream stderr = process.getErrorStream();
            InputStreamReader inputStreamReader = new InputStreamReader(stderr);
            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
            String line;
            StringBuilder sb = new StringBuilder();

            log.debug("<OUT>");
            while ((line = bufferedReader.readLine()) != null) {
                log.debug(line);
                sb.append(line);
            }
            log.debug("</OUT>");

            int exitValue = process.waitFor();

            log.debug("Process exitValue: " + exitValue);

            return VideoInfoParser.parseVideoConfiguration(sb.toString());
        } finally {
            if (process != null) {
                process.destroy();
            }
        }
    }

    /**
     * Converts video to new video file
     *
     * @param file video file
     * @param videoInfo video info
     * @param videoConfiguration configuration
	 * @param outFilePath path to place result file
     * @return converted video file
     */
    public static File convertVideo(File file, VideoInfo videoInfo, VideoConfiguration videoConfiguration, String outFilePath) throws IOException, InterruptedException {
        SizeUtils.Size targetSize = SizeUtils.resizeHeight(videoInfo.getWidth(), videoInfo.getHeight(), videoConfiguration.getHeight(), 4);
        String sizeParameter = targetSize.getWidth() + "x" + targetSize.getHeight();
		String originalFpsParameter = (videoInfo.getOriginalFps() != null && !videoInfo.getOriginalFps().isEmpty()) ? videoInfo.getOriginalFps() : "25";
        String originalAr = (videoInfo.getOriginalAr() != null && !videoInfo.getOriginalAr().isEmpty()) ? videoInfo.getOriginalAr() : "44100";

        log.debug(outFilePath);

        File targetFile = createParentDir(outFilePath);

        //ffmpeg command:
        //ffmpeg -r originalFps -i input_file -strict experimental -y -b:v 1000k -flags +loop -cmp +chroma -partitions +parti4x4+partp4x4+partp8x8+partb8x8 -subq 5 -trellis 2 -refs 1 -coder 0 -me_range 16 -g 50 -keyint_min 25 -sc_threshold 40 -i_qfactor 0.71 -bt 500k -maxrate 14M -bufsize 10M -rc_eq 'blurCplx^(1-qComp)' -qcomp 0.6 -qmin 10 -qmax 51 -qdiff 4 -level 31 -vcodec libx264 -s sizeParameter -aq 1000 -ar 44100 out_file
        final String[] command = {
                "ffmpeg",
                "-i", file.getAbsolutePath(),
                "-strict", "experimental",
                "-y",
                "-b:v", videoConfiguration.getBitrate(),
                "-flags", "+loop", "-cmp", "chroma", "-partitions",
                "+parti4x4+partp4x4+partp8x8+partb8x8",
                "-subq", "5", "-trellis", "2",
                "-refs", "1", "-coder", "0",
                "-me_range", "16", "-g", "50",
                "-keyint_min", "25", "-sc_threshold", "40",
                "-i_qfactor", "0.71", "-bt", "500k",
                "-maxrate", "14M", "-bufsize", "10M",
                "-rc_eq", "'blurCplx^(1-qComp)'",
                "-qcomp", "0.6", "-qmin", "10",
                "-qmax", "51", "-qdiff", "4",
                "-level", "31",
                "-vcodec", "libx264",
				"-aq", "1000",
                "-s", sizeParameter,
				"-ar", originalAr,
				"-r", originalFpsParameter,
                outFilePath};

		int exitValue = executeRuntimeCommand(StringUtils.arrayToString(command, " "));
		if (exitValue != 0) {
            return null;
        }

        postConvertProcessing(targetFile);

        return targetFile;
    }

    /**
     * Captures screen shot at specified second from video and saves jpg
	 *
     * @param videoFile video file
     * @param videoConfiguration configuration
     * @param second second
     * @param format indicates if captured screenshot will be formatted with padding to 4:3
     * @return result thumbnail image file
     */
    @Nullable
    private static File captureScreenshot(File videoFile, VideoConfiguration videoConfiguration, String second, boolean format) throws IOException, InterruptedException {
        final String thumbnailName = videoFile.getParent() + "/" + videoConfiguration.getName() + (format ? "_formatted" : "") + ".jpg";
        VideoInfo videoInfo = getVideoInfo(videoFile);
        if (videoInfo == null) {
            log.warn("Can't get video info");
            return null;
        }

        SizeUtils.Size size = SizeUtils.fit(videoInfo.getWidth(), videoInfo.getHeight(), videoConfiguration.getWidth(), videoConfiguration.getHeight());
        String sizeParameter = size.getWidth() + "x" + size.getHeight();
        StringBuilder padParameter = new StringBuilder();
        padParameter.append("pad=");
        padParameter.append(videoConfiguration.getWidth());
        padParameter.append(":");
        padParameter.append(videoConfiguration.getHeight());
        padParameter.append(":");
        padParameter.append((videoConfiguration.getWidth() - size.getWidth()) / 2);
        padParameter.append(":");
        padParameter.append((videoConfiguration.getHeight() - size.getHeight()) / 2);
        padParameter.append(":0x000000");

        //ffmpeg -i 240p.mp4 -an -ss 00:00:01 -r 1 -vframes 1 -s 320x240 -y -f mjpeg 240p.jpg
        String thumbCommand =  "ffmpeg" +
                " -i " + videoFile.getAbsolutePath() +
                " -an -ss " + second +
                " -r 1 -vframes 1" +
                " -s " + sizeParameter +
                " -y -f mjpeg " +
                (format ? " -vf " + padParameter.toString() : "") +
                " " + thumbnailName;

		int exitValue = executeRuntimeCommand(thumbCommand);
		if (exitValue != 0) {
            log.warn("Can't get video Screenshot");
            return null;
        }

		return new File(thumbnailName);
    }

    /**
     * Captures screen shot from video and saves jpg
     *
     * @param videoFile video file
     * @param videoConfiguration configuration
     * @return result thumbnail image file
     */
    @Nullable
    public static File captureScreenshot(File videoFile, VideoConfiguration videoConfiguration) throws IOException, InterruptedException {
        return captureScreenshot(videoFile, videoConfiguration, "3", false);
    }

	/**
	 * Captures screen shot series storyboard from video
	 *
	 *
	 * @param videoFile video file
	 * @return directory with storyboard images
	 */
	public static File captureStoryboard(File videoFile, Video video) throws IOException, InterruptedException {
		String outFilePath = videoFile.getParent() + "/storyboard/storyboard%05d.jpg";

        File targetFile = createParentDir(outFilePath);

		int duration = video.getDuration();
		final int frames;
		if (duration <= 120) {
			frames = 60;
		} else if (duration <= 200) {
			frames = 120;
		} else if (duration <= 600) {
			frames = 210;
		} else if (duration < 1200) {
			frames = 300;
		} else {
			frames = 390;
		}

		double rate = duration != 0 ? frames / (double) duration : 0.01;

		//ffmpeg -i 240p.mp4 -r 0.1 -vframes 10 -s 64x48 -y -f image2 /storyboard/storyboard%05d.jpg
		String thumbCommand =  "ffmpeg" +
				" -i " + videoFile.getAbsolutePath() +
				" -r " + rate +
				" -vframes " + frames +
				" -s " + "64x48" +
				" -y -f image2 " +
				" " + outFilePath;

		int exitValue = executeRuntimeCommand(thumbCommand);
		if (exitValue != 0) {
            return null;
        }

		return new File(targetFile.getParent());
	}

    @Nonnull
    private static File createParentDir(String outFilePath) {
        File targetFile = new File(outFilePath);
        if (!targetFile.getParentFile().exists()) {
            targetFile.getParentFile().mkdirs();
        }
        return targetFile;
    }

    /**
	 * Executes runtime command
	 *
	 * @param commandString command
	 * @return process exit value
	 */
	private static int executeRuntimeCommand(String commandString) throws IOException, InterruptedException {
		int exitValue;
		Process process = null;
		try {
			log.debug("Executing: " + commandString);
			process = Runtime.getRuntime().exec(commandString);
			InputStream stderr = process.getErrorStream();
			InputStreamReader inputStreamReader = new InputStreamReader(stderr);
			BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
			String line;
			log.debug("<OUT>");
			while ((line = bufferedReader.readLine()) != null) {
                log.debug(line);
            }
			log.debug("</OUT>");

			exitValue = process.waitFor();
			log.debug("Process exitValue: " + exitValue);

		} finally {
			if (process != null) {
                process.destroy();
            }
		}
		return exitValue;
	}

	/**
	 * Executes some specific s1 video post convert commands
	 *
	 * @param videoFile video file
	 */
	private static void postConvertProcessing(File videoFile) {

        if (!SystemUtils.IS_OS_UNIX) {
            return;
        }

        String qtCommand = "qtfaststart " + videoFile.getAbsolutePath();
        String chModCommand = "chmod 644 " + videoFile.getAbsolutePath();
        Process process = null;
        try {

            log.debug("Processing qtfaststart");
            process = Runtime.getRuntime().exec(qtCommand);
            log.debug("Process exitValue: " + process.waitFor());

            log.debug("Processing chmod");
            process = Runtime.getRuntime().exec(chModCommand);
            log.debug("Process exitValue: " + process.waitFor());

        } catch (InterruptedException e) {
            log.error("InterruptedException", e);
        } catch (IOException e) {
            log.error("IOException", e);
        } finally {
            if (process != null) {
                process.destroy();
            }
        }
    }
}

package ru.planeta.service;

import ru.planeta.api.exceptions.NotFoundException;
import ru.planeta.api.exceptions.PermissionException;
import ru.planeta.domain.AudioFile;
import ru.planeta.model.profile.media.AudioTrack;

import java.io.File;
import java.io.IOException;

/**
 * Audio files uploading service
 *
 * @author ds.kolyshev
 * Date: 28.10.11
 */
public interface AudioContentService {

    /**
	 * Uploads file to server
	 *
     */
    AudioTrack uploadAudioTrack(File file, String reqFileName, long clientId, long profileId, long albumId) throws IOException, PermissionException;

    /**
     * Synchronized converting
     */
    void scheduleAudioConvertation(long clientId, AudioFile audioFile);

	/**
	 * Get track file
	 */
	File getAudioTrackFile(AudioTrack track) throws IOException;

	/**
	 * Schedules audio track convertation
	 */
	void reconvertAudio(long clientId, long ownerId, long trackId) throws NotFoundException;
}

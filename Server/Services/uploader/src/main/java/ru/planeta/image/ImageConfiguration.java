package ru.planeta.image;

import ru.planeta.utils.ThumbConfiguration;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * Represents configuration for generating thumbnails while uploading photo
 *
 * @author ameshkov
 */
public class ImageConfiguration {

	private List<ThumbConfiguration> thumbConfigurations;
    private static final Map<String, ThumbConfiguration> lookup = new TreeMap<String, ThumbConfiguration>();

	public List<ThumbConfiguration> getThumbConfigurations() {
		return thumbConfigurations;
	}

	public List<ThumbConfiguration> getThumbConfigurationsWithoutReal() {
		ThumbConfiguration real = getThumbConfigurationByName("real");
		if (thumbConfigurations != null && thumbConfigurations.contains(real)) {
			List<ThumbConfiguration> configurations = new ArrayList<ThumbConfiguration>(thumbConfigurations);
			configurations.remove(real);
			return configurations;
		} else {
			return thumbConfigurations;
		}
	}

	public void setThumbConfigurations(List<ThumbConfiguration> thumbConfigurations) {
		this.thumbConfigurations = thumbConfigurations;
        for(ThumbConfiguration thumbConfiguration : thumbConfigurations) {
            lookup.put(thumbConfiguration.getName(), thumbConfiguration);
        }
	}

    public ThumbConfiguration getThumbConfigurationByName(String name) {
        return lookup.get(name);
    }
}

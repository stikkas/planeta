package ru.planeta.video;

/**
 * Common video information
 *
 * @author ds.kolyshev
 * Date: 12.09.11
 */
public class VideoInfo extends VideoConfiguration {

    private int duration;
    private String bitrate;
	private String originalFps;
    private String originalAr;

    public String getOriginalAr() {
        return originalAr;
    }

    public void setOriginalAr(String originalAr) {
        this.originalAr = originalAr;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public String getBitrate() {
        return bitrate;
    }

    public void setBitrate(String bitrate) {
        this.bitrate = bitrate;
    }

	public String getOriginalFps() {
		return originalFps;
	}

	public void setOriginalFps(String originalFps) {
		this.originalFps = originalFps;
	}
}

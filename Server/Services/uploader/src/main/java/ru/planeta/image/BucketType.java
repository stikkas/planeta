package ru.planeta.image;

import java.io.Serializable;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author ameshkov
 */
public enum BucketType implements Serializable {

	JPEG(0, "jpg", "jpeg"),
	GIF(1, "gif", "gif"),
	PNG(2, "png", "png"),
	BMP(4, "bmp", "bmp"),
	TIFF(5, "tiff", "tiff"),
	BMP3(6, "bmp", "bmp"),
	PAM(7, "pam", "webP");
	private int value;
	private String extension;
	private String codecName;

	private BucketType(int val, String extension, String codecName) {
		value = val;
		this.extension = extension;
		this.codecName = codecName;
	}

	public Integer getValue() {
		return value;
	}

	public String getCodecName() {
		return codecName;
	}

	public String getExtension() {
		return extension;
	}

	/**
	 * Only for usage in file storage. Not for external files.
	 *
	 * @param filename
	 * @param offset
	 * @return
	 */
	public static BucketType parse(String filename, int offset) {
		if (filename.charAt(offset) == '.') {
			offset++;
		}
		String outerExtension = StringUtils.substring(filename, offset).toLowerCase();
		for (BucketType bType : values()) {
			if (bType.extension.equals(outerExtension)) {
				return bType;
			}
		}
		throw new IllegalArgumentException("Filename extension is not known");
	}
}

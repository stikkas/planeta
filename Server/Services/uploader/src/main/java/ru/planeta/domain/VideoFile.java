package ru.planeta.domain;

import ru.planeta.model.profile.media.Video;
import ru.planeta.video.VideoInfo;

import java.io.File;

/**
 * @author ds.kolyshev
 * Date: 12.09.11
 */
public class VideoFile {
    private Video video;
	private VideoInfo originalVideoInfo;
    private File file;

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }

    public VideoInfo getOriginalVideoInfo() {
        return originalVideoInfo;
    }

    public void setOriginalVideoInfo(VideoInfo originalVideoInfo) {
        this.originalVideoInfo = originalVideoInfo;
    }

	public Video getVideo() {
		return video;
	}

	public void setVideo(Video video) {
		this.video = video;
	}
}

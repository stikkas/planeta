package ru.planeta.service;

import org.apache.commons.io.FileUtils;
import org.im4java.core.IM4JavaException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import ru.planeta.image.ImageOperationException;

import java.io.File;
import java.io.IOException;
import java.net.URLDecoder;
import java.net.URLEncoder;

/**
 * @author ds.kolyshev
 *         Date: 19.10.11
 */
@Service
public class PathsGenerationServiceImpl extends BaseService implements PathsGenerationService {
    private static final String audioTracksDirectory = "/audios/";
    private static final String imagesDirectory = "/images/";
    private static final String videosDirectory = "/videos/";
    private static final String promosDirectory = "/promo/";
    private static final String filesDirectory = "/files/";

    @Value("${audiotracks.url}")
    private String audioTracksUrl;
    @Value("${images.url}")
    private String imagesUrl;
    @Value("${videos.url}")
    private String videosUrl;
    @Value("${promos.url}")
    private String promosUrl;
    @Value("${files.url}")
    private String filesUrl;

    @Value("${audiotracks.path}")
    private String audioTracksPath;
    @Value("${videos.path}")
    private String videoPath;
    @Value("${images.path}")
    private String imagePath;

    @Autowired
    private ThumbGenerationService thumbGenerationService;

    @Override
    public String generateRedirectUrl(String uri, boolean flushCache) throws IOException, ImageOperationException, IM4JavaException, InterruptedException {
        String[] pathParams = uri.substring(1).split("/");
        if (pathParams.length != 3) {
            return null; //NOTE: for now only
        }

        String directoriesPath = "/" + pathParams[0] + "/";
        String fileIdString = pathParams[1];
        String fileName = "/" + pathParams[2];


        long fileId = Long.parseLong(fileIdString, 16);
        final String directoryKey;
        if (directoriesPath.equalsIgnoreCase(audioTracksUrl))
            directoryKey = audioTracksDirectory;
        else if (directoriesPath.equalsIgnoreCase(videosUrl))
            directoryKey = videosDirectory;
        else if (directoriesPath.equalsIgnoreCase(promosUrl))
            directoryKey = promosDirectory;
        else if (directoriesPath.equalsIgnoreCase(imagesUrl))
            directoryKey = imagesDirectory;
        else if (directoriesPath.equalsIgnoreCase(filesUrl))
            directoryKey = filesDirectory;
        else
            return null;

        String directory = directoryKey.replace("\\", "/").replace("//", "/");
        if (directory.startsWith(".")) {
            directory = directory.substring(1);
        }

        if (directoryKey.equalsIgnoreCase(imagesDirectory)) {
            //Check if image exists and creates new if not
            thumbGenerationService.checkThumbnail(fileId, fileName, flushCache);
        }
        final String decoded;
        if (fileName.contains("%")) {
            log.debug("Before decode " + fileName.toLowerCase());
            decoded = URLEncoder.encode(fileName.toLowerCase(), "UTF-8");
        } else {
            decoded = fileName.toLowerCase();
        }

        return directory + generateFilePath(fileId) + decoded;
    }


    @Override
    public String reGenerateRedirectUrl(String uri, boolean flushCache) throws IOException, ImageOperationException, IM4JavaException, InterruptedException {

        final String ROOT = "/var/www/planeta_static";
        String[] pathParams = uri.substring(1).split("/");
        if (pathParams.length != 3) {
            return null; //NOTE: for now only
        }

        String fileIdString = pathParams[1];
        String fileName = "/" + pathParams[2];


        long fileId = Long.parseLong(fileIdString, 16);

        String oldName = ROOT + filesDirectory + generateFilePath(fileId) + fileName.toLowerCase();

        String newName = ROOT + filesDirectory + generateFilePath(fileId) + getNewFileName(fileName);
        FileUtils.copyFile(new File(oldName), new File(newName));


        return newName;

    }

    @Override
    public String generateFullFileName(String uri) throws IOException, ImageOperationException, IM4JavaException, InterruptedException {
        String[] pathParams = uri.substring(1).split("/");
        if (pathParams.length != 3) {
            return null; //NOTE: for now only
        }

        String directoriesPath = "/" + pathParams[0] + "/";
        String fileIdString = pathParams[1];
        String fileName = "/" + pathParams[2];


        long fileId = Long.parseLong(fileIdString, 16);
        final String directoryKey;
        if (directoriesPath.equalsIgnoreCase(audioTracksUrl))
            directoryKey = audioTracksPath;
        else if (directoriesPath.equalsIgnoreCase(videosUrl))
            directoryKey = videoPath;
        else if (directoriesPath.equalsIgnoreCase(imagesUrl))
            directoryKey = imagePath;
        else
            return null;

        String directory = directoryKey.replace("\\", "/").replace("//", "/");
        if (directory.startsWith(".")) {
            directory = directory.substring(1);
        }

        if (directoryKey.equalsIgnoreCase(imagesDirectory)) {
            //Check if image exists and creates new if not
            thumbGenerationService.checkThumbnail(fileId, fileName, false);
        }

        String resultRedirectUrl = directory + generateFilePath(fileId) + fileName.toLowerCase();
        return resultRedirectUrl;
    }

}

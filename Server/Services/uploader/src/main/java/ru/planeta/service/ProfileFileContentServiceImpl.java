package ru.planeta.service;

import org.apache.commons.collections4.Equator;
import org.apache.commons.collections4.IterableUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import ru.planeta.api.exceptions.MessageCode;
import ru.planeta.api.exceptions.PermissionException;
import ru.planeta.api.service.content.ProfileFileService;
import ru.planeta.domain.ProfileFileModel;
import ru.planeta.model.profile.ProfileFile;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Arrays;
import java.util.Date;

/**
 * @author ds.kolyshev
 * Date: 20.03.13
 */
@Service
public class ProfileFileContentServiceImpl extends BaseService implements ProfileFileContentService {

    @Value("${files.path}")
	private String filesPath;
	@Value("${files.url}")
	private String filesUrl;

	private final ProfileFileService profileFileService;

	private static final String[] VALID_EXTENSIONS = {"jpg", "png", "bmp", "pdf", "jpeg", "jpe"};

	@Autowired
	public ProfileFileContentServiceImpl(ProfileFileService profileFileService) {
		this.profileFileService = profileFileService;
	}

	public void setFilesPath(String filesPath) {
		this.filesPath = filesPath;
	}

	@Override
	public ProfileFile uploadProfileFile(File reqFile, String reqFileName, long clientId, long ownerId) throws PermissionException, IOException {
		return uploadProfileFileModel(reqFile, reqFileName, clientId, ownerId).getProfileFile();
	}

	@Override
	public ProfileFileModel uploadProfileFileModel(File file, String reqFileName, long clientId, long profileId) throws PermissionException, IOException {
		getPermissionService().checkIsAdmin(clientId, profileId);

		if (!getPermissionService().hasAdministrativeRole(clientId)) {
            if (!IterableUtils.contains(Arrays.asList(VALID_EXTENSIONS), FilenameUtils.getExtension(reqFileName),
			new Equator<String>() {
				@Override
				public boolean equate(String s, String t1) {
					return s.equalsIgnoreCase(t1);
				}

				@Override
				public int hash(String s) {
					return 0;
				}
			})) {
                throw new PermissionException(MessageCode.FIELD_ERROR);
            }
		}

		long profileFileId = getSequencesDAO().selectNextLong(ProfileFile.class);

		File renamedFile = new File(file.getParentFile(), getNewFileName(reqFileName));
		FileUtils.moveFile(file, renamedFile);
		reqFileName = renamedFile.getName();

		File targetFile = moveOriginalFile(renamedFile, profileFileId, filesPath);
		ProfileFile profileFile = prepareProfileFile(clientId, profileId, profileFileId, reqFileName, targetFile);

		ProfileFileModel profileFileModel = new ProfileFileModel();
		profileFileModel.setFile(targetFile);
		profileFileModel.setProfileFile(profileFile);
		return profileFileModel;
	}

    private ProfileFile prepareProfileFile(long clientId, long profileId, long profileFileId, String fileName, File targetFile) throws UnsupportedEncodingException, PermissionException {
		String fileUrl = generateUrl(fileName, profileFileId, filesUrl);

		ProfileFile profileFile = new ProfileFile();
		profileFile.setFileId(profileFileId);
		profileFile.setProfileId(profileId);
		profileFile.setAuthorId(clientId);
		profileFile.setDescription("");
		profileFile.setName(fileName);
		profileFile.setFileUrl(fileUrl);
		profileFile.setTimeAdded(new Date());
		profileFile.setTimeUpdated(new Date());
		if (!fileName.isEmpty()) {
			profileFile.setExtension(FilenameUtils.getExtension(fileName));
		}
		profileFile.setSize(FileUtils.sizeOf(targetFile));

		return profileFileService.addProfileFile(clientId, profileFile);
	}
}

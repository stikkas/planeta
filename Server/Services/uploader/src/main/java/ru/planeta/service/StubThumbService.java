package ru.planeta.service;

import ru.planeta.image.StringParameters;

import java.io.IOException;
import java.io.OutputStream;

/**
 * @author s.kalmykov
 *         Date: 22.03.2012
 */
public interface StubThumbService {

    void drawVideoToStream(String url, String overlayUrl, String name, String duration, OutputStream out, StringParameters params) throws IOException;

    void drawAudioToStream(String playUrl, String name, String artist, String duration, StringParameters params, OutputStream out) throws IOException;
}

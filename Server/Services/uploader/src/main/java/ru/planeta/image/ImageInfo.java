package ru.planeta.image;

import org.apache.log4j.Logger;
import org.im4java.core.IM4JavaException;
import org.im4java.core.IMOperation;
import org.im4java.core.IdentifyCmd;
import org.im4java.core.ImageCommand;
import org.im4java.process.ArrayListOutputConsumer;

import javax.annotation.Nonnull;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Represents image info
 * 
 * @author ameshkov
 */
public class ImageInfo {

	private final boolean isAnimated;
	private final int height;
	private final int width;
	private final BucketType bucketType;

	private transient static final Logger log = Logger.getLogger(ImageInfo.class);

	public ImageInfo(String path) throws IOException, InterruptedException, IM4JavaException {
		this(new File(path));
	}

	public ImageInfo(@Nonnull File f) throws IOException, IM4JavaException, InterruptedException {

		IMOperation op = new IMOperation();
            op.format("%m %w %h\n");
		op.addImage(f.getAbsolutePath());

		try {
            ImageCommand identify = new IdentifyCmd();
			ArrayListOutputConsumer output = new ArrayListOutputConsumer();
			identify.setOutputConsumer(output);
			identify.run(op);
			List<String> cmdOutput = new ArrayList<String>();
            for (String out : output.getOutput()) {
                if (out != null && !out.isEmpty()) {
                    cmdOutput.add(out);
                }
            }
            if (!cmdOutput.isEmpty()) {
                isAnimated = cmdOutput.size() > 1;
                String[] parts = cmdOutput.get(0).trim().split(" ");
                String bucketTypeStr = parts[0];
                bucketType = BucketType.valueOf(bucketTypeStr);
                width = Integer.parseInt(parts[1]);
                height = Integer.parseInt(parts[2]);
            } else {
                throw new IllegalArgumentException("ImageMagic can't operate with image loaded from : " + f.getAbsolutePath());
            }

		} catch (Exception ex) {
			log.error("Problem with file " + f.getAbsolutePath(), ex);
			throw ex;
		} finally {
			op.dispose();
        }
	}

	public boolean isIsAnimated() {
		return isAnimated;
	}

	public int getHeight() {
		return height;
	}

	public int getWidth() {
		return width;
	}

	public BucketType getBucketType() {
		return bucketType;
	}
}

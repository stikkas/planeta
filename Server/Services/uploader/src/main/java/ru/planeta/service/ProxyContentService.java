package ru.planeta.service;

import org.im4java.core.IM4JavaException;
import ru.planeta.api.exceptions.NotFoundException;
import ru.planeta.utils.ThumbConfiguration;
import ru.planeta.image.ImageOperationException;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.net.URISyntaxException;

/**
 * @author  a.savanovich
 * @since 08.02.12 14:46
 */
public interface ProxyContentService {
    /**
     * Returns relative path in our filesystem if file has already downloaded, else tries to download, and if file dowloads to much time, service return URL itself (? should be checked ?)
     * @param url file URL
     * @param config configuration of required thumb (size and forman gif, png, jpeg, etc.)
     */
    String getRelativePath(@Nonnull String url, ThumbConfiguration config, boolean flushCashe) throws IOException, ImageOperationException, IM4JavaException, InterruptedException, NotFoundException, URISyntaxException;
}

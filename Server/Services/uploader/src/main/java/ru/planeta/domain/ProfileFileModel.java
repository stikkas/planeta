package ru.planeta.domain;

import ru.planeta.model.profile.ProfileFile;

import java.io.File;

/**
 * @author: ds.kolyshev
 * Date: 20.03.13
 */
public class ProfileFileModel {
	private ProfileFile profileFile;
	private File file;

	public ProfileFile getProfileFile() {
		return profileFile;
	}

	public void setProfileFile(ProfileFile profileFile) {
		this.profileFile = profileFile;
	}

	public File getFile() {
		return file;
	}

	public void setFile(File file) {
		this.file = file;
	}
}

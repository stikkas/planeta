package ru.planeta.service;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import ru.planeta.api.service.security.PermissionService;
import ru.planeta.dao.commondb.SequencesDAO;
import ru.planeta.dao.profiledb.*;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Date;

/**
 * Base uploader service
 *
 * @author ameshkov
 */
@Component
public abstract class BaseService {

    protected static final Logger log = Logger.getLogger(BaseService.class);
    private SequencesDAO sequencesDAO;
    private PhotoDAO photoDAO;
    private PhotoAlbumDAO photoAlbumDAO;

    private PermissionService permissionService;

    private String serverPath;

    @Value("${static.node}")
    public void setServerPath(String serverPath) {
        if (serverPath.startsWith("http://") || serverPath.startsWith("https://")) {
            throw new IllegalArgumentException("serverPath must not start with http://");
        }
        this.serverPath = serverPath;
    }

    protected String getServerPath() {
        return serverPath;
    }

    @Autowired
    public void setSequencesDAO(SequencesDAO sequencesDAO) {
        this.sequencesDAO = sequencesDAO;
    }

    protected SequencesDAO getSequencesDAO() {
        return sequencesDAO;
    }


    protected PhotoDAO getPhotoDAO() {
        return photoDAO;
    }

    @Autowired
    public void setPhotoDAO(PhotoDAO photoDAO) {
        this.photoDAO = photoDAO;
    }

    protected PhotoAlbumDAO getPhotoAlbumDAO() {
        return photoAlbumDAO;
    }

    @Autowired
    public void setPhotoAlbumDAO(PhotoAlbumDAO photoAlbumDAO) {
        this.photoAlbumDAO = photoAlbumDAO;
    }

    @Autowired
    public void setPermissionService(PermissionService permissionService) {
        this.permissionService = permissionService;
    }

    protected PermissionService getPermissionService() {
        return permissionService;
    }

    /**
     * Creates directory for the file with specified fileId.
     *
     */
    protected static File createFileDirectory(long fileId, String baseDirectory) {
        String[] dirs = new String[8];

        String str = StringUtils.leftPad(Long.toHexString(fileId), 16, "0");

        for (int i = 0; i < 16; i += 2) {
            dirs[i / 2] = new String(new char[]{str.charAt(i), str.charAt(i + 1)});
        }

        StringBuilder sb = new StringBuilder(baseDirectory);
        if (!baseDirectory.endsWith("/")) {
            sb.append("/");
        }

        for (String dir : dirs) {
            sb.append(dir);
            sb.append("/");
        }

        File file = new File(sb.toString());
        if (!file.exists()) {
            file.mkdirs();
        }

        return file;
    }

    /**
     * Generates unique file path
     *
     */
    protected static String generateFilePath(long fileId) {
        String[] dirs = new String[8];

        String str = StringUtils.leftPad(Long.toHexString(fileId), 16, "0");

        for (int i = 0; i < 16; i += 2) {
            dirs[i / 2] = new String(new char[]{str.charAt(i), str.charAt(i + 1)});
        }

        return StringUtils.join(dirs, '/');
    }

    /**
     * Generates url from fileName, fileId and directories path
     *
     * @param fileId           Identifier of entity
     */
    protected String generateUrl(String originalFileName, long fileId, String directoriesUrl) throws UnsupportedEncodingException {
        directoriesUrl = directoriesUrl.replace("\\", "/").replace("//", "/");
        if (directoriesUrl.startsWith(".")) {
            directoriesUrl = directoriesUrl.substring(1);
        }

        String encodedFileName = encodeFileName(originalFileName);
        //hardcoded server path for now
        return "https://" + getServerPath() + directoriesUrl + Long.toHexString(fileId) + "/" + encodedFileName;
    }

    /**
     * Copies original file to directories path
     *
     * @throws IOException
     */
    protected static File moveOriginalFile(File file, long entityId, String directoriesPath) throws IOException {

        File targetFile = initTargetCopyFile(file, entityId, directoriesPath);
        moveUploadedFile(file, targetFile);

        return targetFile;
    }

    protected static void moveUploadedFile(File file, File targetFile) throws IOException {
        if (!targetFile.getParentFile().exists()) {
            targetFile.getParentFile().mkdirs();
        }

        try {
            FileUtils.moveFile(file, targetFile);
        } catch (Exception ex) {
            log.error("Cannot move file " + file + " " + targetFile, ex);
            FileUtils.copyFile(file, targetFile);
        }
    }

    private static File initTargetCopyFile(File file, long entityId, String directoriesPath) throws IOException {
        File targetFile = new File(generateFullFilePathToCopy(stripFileName(file.getName()), entityId, directoriesPath));
        if (!targetFile.getParentFile().exists()) {
            targetFile.getParentFile().mkdirs();
        }

        return targetFile;
    }

    /**
     * Generates full file path to copy specified fileName corresponded with entityId
     *
     */
    protected static String generateFullFilePathToCopy(String fileName, long entityId, String directoriesPath) {

        String originalFilePath = generateFilePath(entityId) + "/" + fileName;

        StringBuilder sb = new StringBuilder(directoriesPath);
        if (!directoriesPath.endsWith("/")) {
            sb.append("/");
        }
        sb.append(originalFilePath);

        return sb.toString();
    }

    protected static String getNewFileName(String reqFileName) {
        return new Date().getTime() + "_renamed." + FilenameUtils.getExtension(reqFileName);
    }


    /**
     * Encode file name replacing special characters and transliterating cyrillic
     *
     */
    private static String encodeFileName(String fileName) throws UnsupportedEncodingException {
        String stripped = stripFileName(fileName);
        return URLEncoder.encode(stripped, "UTF-8");
    }

    private static String stripFileName(String fileName) {
        return ru.planeta.commons.lang.FileUtils.stripLongFilename(fileName).toLowerCase();
    }
}

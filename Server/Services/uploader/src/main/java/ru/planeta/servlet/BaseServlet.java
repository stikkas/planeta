package ru.planeta.servlet;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import ru.planeta.commons.web.WebUtils;
import ru.planeta.service.*;

import javax.annotation.Nonnull;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLDecoder;
import java.security.InvalidParameterException;
import java.util.Date;

/**
 * Base class for servlets
 *
 * @author ds.kolyshev
 *         Date: 11.01.12
 */
public abstract class BaseServlet extends HttpServlet {
    public static final String CROSSDOMAIN_XML_KEY = "/crossdomain.xml";
    public static final String TRANSFER_IMAGE_KEY = "/transferImage";
    public static final String BACKGROUND_IMAGE_KEY = "/backgroundImage";
    public static final String FILE_PATH_HEADER_NAME = "X-File-Path";
    public static final String FILE_NAME_HEADER_NAME = "X-File-Name";
    public static final String FILE_SIZE_HEADER_NAME = "X-File-Size";
    public static final String CLIENT_ID_PARAMETER_KEY = "clientId";
    public static final String OWNER_ID_PARAMETER_KEY = "ownerId";
    public static final String ALBUM_ID_PARAMETER_KEY = "albumId";
    public static final String ALBUM_TYPE_ID_PARAMETER_KEY = "albumTypeId";
    public static final String BROADCAST_STREAM_ID_PARAMETER_KEY = "broadcastStreamId";
    public static final String VIDEO_SEEK_PARAMETER_KEY = "start";
    public static final String CONVERT_VIDEO_KEY = "/convertVideo";
    public static final String UPDATE_STATE_KEY = "/updateState";
    public static final String VIDEO_ID_PARAMETER_KEY = "videoId";
    public static final String TRACK_ID_PARAMETER_KEY = "audioId";
    public static final String PHOTO_ID_PARAMETER_KEY = "photoId";
    public static final String SECOND_PARAMETER_KEY = "second";
    public static final String TRANSFER_URL_PARAMETER_KEY = "url";
    public static final String CROP_IMAGE_KEY = "/cropImage";
    public static final String CROP_IMAGE_X_PARAMETER_KEY = "cropX";
    public static final String CROP_IMAGE_Y_PARAMETER_KEY = "cropY";
    public static final String CROP_IMAGE_WIDTH_PARAMETER_KEY = "cropWidth";
    public static final String CROP_IMAGE_HEIGHT_PARAMETER_KEY = "cropHeight";
    public static final String CROSS_DOMAIN_XML = "<cross-domain-policy><allow-access-from domain=\"*\"/></cross-domain-policy>";

    private static final Logger log = Logger.getLogger(BaseServlet.class);

    @Autowired
    private PathsGenerationService pathsGenerationService;
    @Autowired
    private AudioContentService audioContentService;
    @Autowired
    private ImageContentService imageContentService;
    @Autowired
    private VideoContentService videoContentService;
    @Autowired
    private ProfileFileContentService profileFileContentService;

    @Value("${audiotracks.url}")
    private String audioTracksUrl;
    @Value("${images.url}")
    private String imagesUrl;
    @Value("${videos.url}")
    private String videosUrl;
    @Value("${promos.url}")
    private String promosUrl;
    @Value("${files.url}")
    private String filesUrl;
    @Value("${tempDirectoryPath}")
    private String tempDirectoryPath;
    @Value("${static.node}")
    protected String serverPath;

    protected static void setDefaultHeaders(HttpServletResponse response) {
        response.setStatus(HttpServletResponse.SC_OK);
        response.setHeader(HttpHeaders.CONTENT_TYPE, "text/html");
        response.setHeader(HttpHeaders.CONNECTION, "close");
        response.setHeader(HttpHeaders.PRAGMA, "no-cache");
        response.setHeader(HttpHeaders.CACHE_CONTROL, "no-cache");
    }


    protected PathsGenerationService getPathsGenerationService() {
        return pathsGenerationService;
    }

    protected AudioContentService getAudioContentService() {
        return audioContentService;
    }

    protected ImageContentService getImageContentService() {
        return imageContentService;
    }

    protected VideoContentService getVideoContentService() {
        return videoContentService;
    }

    protected ProfileFileContentService getProfileFileContentService() {
        return profileFileContentService;
    }

    protected String getAudioTracksUrl() {
        return audioTracksUrl;
    }

    protected String getImagesUrl() {
        return imagesUrl;
    }

    protected String getVideosUrl() {
        return videosUrl;
    }

    protected String getPromosUrl() {
        return promosUrl;
    }

    protected String getFilesUrl() {
        return filesUrl;
    }

    protected String getTempDirectoryPath() {
        return tempDirectoryPath;
    }


    /**
     * Helper method for getting parameter from request
     * Throws exception if parameter is empty
     *
     */
    protected static String getRequestParameterSafe(HttpServletRequest request, String parameterKey) {
        String parameter = getRequestParameter(request, parameterKey);

        if (parameter == null || parameter.isEmpty()) {
            log.error("No parameter " + parameterKey + " "+WebUtils.requestDataToString(request));
            throw new InvalidParameterException("Parameters not valid: " + parameterKey);
        }

        return parameter;
    }

    /**
     * Helper method for getting parameter from request
     *
     */
    protected static String getRequestParameter(HttpServletRequest request, String parameterKey) {
        String parameter = request.getParameter(parameterKey);
        if (parameter == null || parameter.isEmpty()) {
            String query = request.getQueryString();
            if (query == null || query.isEmpty()) {
                return parameter;
            }
            String[] keyValues = query.split("&");
            for (String keyValue : keyValues) {
                String[] split = keyValue.split("=");
                if (split.length > 1 && split[0].equals(parameterKey)) {
                    parameter = split[1];
                    break;
                }
            }
        }

        return parameter;
    }

    /**
     * Helper method for writing xml response
     *
     * @throws IOException
     */
    protected static void writeResponseXml(HttpServletResponse resp, String responseText) throws IOException {
        if (responseText.isEmpty()) {
            resp.setStatus(HttpServletResponse.SC_NO_CONTENT);
        } else {
            resp.setStatus(HttpServletResponse.SC_OK);
            resp.setContentType("text/xml; charset=utf-8");
            //resp.getOutputStream().write(responseText.getBytes());
            resp.getOutputStream().println(responseText);
            resp.getOutputStream().flush();
        }
    }

    /**
     * Downloads file from specified url
     *
     * @throws IOException
     */
    @Nonnull
    protected File downloadFileFromUrl(String urlString) throws IOException {
        File file = new File(getTempDirectoryPath(), new Date().getTime() + "_" + StringUtils.substring(urlString.substring(urlString.lastIndexOf('/') + 1), 0, 100));
        FileUtils.copyURLToFile(new URL(urlString), file);
        return file;
    }

    protected static void proxyNoContent(HttpServletResponse response) throws IOException {
        response.sendError(HttpServletResponse.SC_NOT_FOUND);
    }

    protected static String getFileNameHeader(HttpServletRequest req) {
        try {
            return URLDecoder.decode(req.getHeader(FILE_NAME_HEADER_NAME), "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return "";
        }
    }
}

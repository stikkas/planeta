package ru.planeta.service;

import org.apache.commons.lang3.math.NumberUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.planeta.api.service.configurations.StaticNodesService;
import ru.planeta.commons.lang.DateUtils;
import ru.planeta.commons.web.WebUtils;
import ru.planeta.image.StringParameters;

import javax.annotation.Nonnull;
import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.font.FontRenderContext;
import java.awt.geom.AffineTransform;
import java.awt.geom.Rectangle2D;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URL;

/**
 * @author s.kalmykov
 *         Date: 22.03.2012
 */
@Service
public class StubThumbServiceImpl implements StubThumbService {
    @Autowired
    private StaticNodesService staticNodesService;

    @Override
    public void drawVideoToStream(String url, String overlayUrl, String name, @Nonnull String duration, OutputStream out, StringParameters params) throws IOException {
        BufferedImage before;
        if(url == null) {
            url = WebUtils.appendProtocolIfNecessary(staticNodesService.getResourceHost() + "/images/upload/video-processing.png", false);
        }
        before = webGet(url);
        before = stretch(before, params.stubWidth, params.stubHeight);
        BufferedImage overlay = webGet(overlayUrl);
        BufferedImage image = setCenteredOverlay(before, overlay);
        int durationMaxWidth = params.maxWidth(duration);
        if (StringUtils.isNotEmpty(name)) {   // draw name
            drawString(image, name, params.padding, image.getHeight() - params.padding,
                    image.getWidth() - 2 * params.padding - 4 * params.stringPaddingX - durationMaxWidth, params);
        }
        int intDuration = NumberUtils.toInt(duration);
        if (intDuration > 0) {
            duration = DateUtils.humanDuration(intDuration);
        }
        if (StringUtils.isNotEmpty(duration)) {
            drawRightAlignedString(image, duration, image.getHeight() - params.padding, durationMaxWidth, params);
        }
        ImageIO.write(image, params.imageExtension, out);
    }

    @Override
    public void drawAudioToStream(String playUrl, @Nonnull String name, String artist, @Nonnull String duration, StringParameters params, OutputStream out) throws IOException {
        int initialFontSize = params.fontSize;
        BufferedImage play = webGet(playUrl);
        if(play.getType() != 0) { // hack for custom types = 0
            params.imageType = play.getType();
        }
        BufferedImage image = new BufferedImage(params.stubWidth, params.stubHeight, params.imageType);
        Graphics2D g2 = image.createGraphics();
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g2.setColor(params.bgColor);
        g2.fillRoundRect(0, 0, image.getWidth(), image.getHeight(), params.borderRadius, params.borderRadius);
        g2.setColor(new Color(0xe6e6e6));
        g2.drawLine(params.padding, 0, image.getWidth() - params.padding, 0);
        g2.drawLine(params.padding, image.getHeight() - 1, image.getWidth() - params.padding, image.getHeight() - 1);

        int center = (int) (0.5 * image.getHeight());
        FontRenderContext frc = g2.getFontRenderContext();
        int lineSpace = params.lineSpace();
        int durationMaxWidth = params.maxWidth(null);
        int offsetX = 45;//image.getHeight();
        params.fontStyle = Font.BOLD;
        if (StringUtils.isNotEmpty(name)) {
            drawString(image, name, offsetX, center - (int) (lineSpace * 0.5), image.getWidth() - 2 * params.padding - offsetX - durationMaxWidth, params);
        }
//        params.textColor = new Color(0x808080);
        params.fontStyle = Font.PLAIN;
        params.fontSize = initialFontSize - 1;
        durationMaxWidth = params.maxWidth(null);
        Font currentFont = params.getFont();
        if (StringUtils.isNotEmpty(artist)) {
            drawString(image, artist,
                    offsetX, center + (int) (lineSpace * 0.5) + (int) currentFont.getStringBounds(artist, frc).getHeight() + params.stringPaddingY,
                    image.getWidth() - 2 * params.padding - image.getHeight() - durationMaxWidth, params);
        }
//        params.textColor = new Color(0x555555);
        params.fontSize = initialFontSize - 2;
        currentFont = params.getFont();
        durationMaxWidth = params.maxWidth(duration);
        int intDuration = NumberUtils.toInt(duration, 0);
        if (intDuration > 0) {
            duration = DateUtils.humanDuration(intDuration);
        }
        if (StringUtils.isNotEmpty(duration)) {
            drawRightAlignedString(image, duration, center + (int) (currentFont.getStringBounds(duration, frc).getHeight() / 2), durationMaxWidth, params);
        }

        Color old = g2.getColor();
        g2.drawRoundRect(params.padding, (int) (center - play.getHeight() * 0.5), play.getWidth(), play.getHeight(), params.borderRadius, params.borderRadius);
        g2.setColor(new Color(0xf4f4f4));
        g2.fillRoundRect(params.padding, (int) (center - play.getHeight() * 0.5), play.getWidth(), play.getHeight(), params.borderRadius, params.borderRadius);
        g2.setColor(old);

        image = setOverlay(image, play, params.padding, (int) (center - play.getHeight() * 0.5));
        params.fontSize = initialFontSize;
        ImageIO.write(image, params.imageExtension, out);
    }

    /**
     * downloads image from inet
     *
     * @param baseImageUrl image url to get
     * @return got image
     */
    private static BufferedImage webGet(@Nonnull String baseImageUrl) throws IOException {
        return ImageIO.read(new URL(baseImageUrl));
    }


    /**
     * Draws overlay on image
     *
     * @param bgImage image to draw on
     * @param fgImage   overlay image
     * @param fgX       upper-left corner of drawn overlay in baseImage coordinates (left)
     * @param fgY       upper-left corner of drawn overlay in baseImage coordinates (up)
     * @return redrawn image
     */
    private static BufferedImage setOverlay(BufferedImage bgImage, BufferedImage fgImage, int fgX, int fgY) {
        Graphics2D g = bgImage.createGraphics();
        g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g.drawImage(bgImage, 0, 0, null);
        g.drawImage(fgImage, fgX, fgY, null);
        g.dispose();
        return bgImage;
    }


    private static BufferedImage stretch(BufferedImage image, int width, int height) {
        BufferedImage after = new BufferedImage(width, height, image.getType());
        AffineTransform at = new AffineTransform();
        at.scale((double) width / image.getWidth(), (double) height / image.getHeight());
        AffineTransformOp scaleOp = new AffineTransformOp(at, AffineTransformOp.TYPE_BILINEAR);
        after = scaleOp.filter(image, after);
        return after;
    }

    private static BufferedImage setCenteredOverlay(BufferedImage image, BufferedImage overlay) {
        double bordersWidth = image.getWidth() - overlay.getWidth();
        double bordersHeight = image.getHeight() - overlay.getHeight();
        int fgX = (int) (bordersWidth / 2);
        int fgY = (int) (bordersHeight / 2);
        return setOverlay(image, overlay, fgX, fgY);
    }


    private static void drawRightAlignedString(BufferedImage image, @Nonnull String string, int Y, int maxWidth, StringParameters params) {
        drawString(image, string, image.getWidth() - maxWidth, Y, maxWidth, params);
    }


    private static void drawString(BufferedImage image, @Nonnull String string, int X, int Y, int maxWidth, StringParameters params) {
        Graphics2D g2 = image.createGraphics();
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        FontRenderContext frc = g2.getFontRenderContext();
        Font font = params.getFont();
        Rectangle2D bounds = font.getStringBounds(string, frc);
        while (bounds.getWidth() > maxWidth && string.length() > 3) {
            string = string.substring(0, string.length() - 4) + "...";
            bounds = font.getStringBounds(string, frc);
        }

        g2.setColor(params.bgColor);
        g2.fillRoundRect(X, Y - (int) bounds.getHeight() - 2 * params.stringPaddingY, (int) bounds.getWidth() + 2 * params.stringPaddingX, (int) bounds.getHeight() + 2 * params.stringPaddingY,
                params.borderRadius, params.borderRadius);
//        g2.drawRoundRect();
        g2.setColor(params.textColor);
        g2.setFont(font);
        g2.drawString(string, X + params.stringPaddingX, Y - params.stringPaddingY - 2); // -2 for ','
        g2.dispose();
    }

}

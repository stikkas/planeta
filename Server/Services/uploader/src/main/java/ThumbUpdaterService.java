import java.io.*;

/**
 *
 * Created by asavan on 14.12.2016.
 */
public class ThumbUpdaterService {

    private static final String EXIST_SUFFIX = "_exist";

    public static void main(String[] args) {
        File[] files = new File("/var/www/planeta_static/proxy/").listFiles();
        showFiles(files, 0, null);
    }

    public static void showFiles(File[] files, int level, File parent) {
        if (level > 200) {
            return;
        }
        if (files == null) {
            return;
        }
        boolean containsBad = false;
        boolean containsGood = false;
        try {
            for (File file : files) {
                if (file.isDirectory()) {
                    showFiles(file.listFiles(), level + 1, file); // Calls same method again.
                } else {
                    if (file.getName().contains("bgcolorblack")) {
                        containsBad = true;
                        // break;
                    }

                    if (file.getName().equals("0_0_exist")) {
                        containsGood = true;
                        // break;
                    }
                }
            }
            if (containsBad && !containsGood) {

                for (File file : files) {
                    if (file.getName().equals("0_0_bgcolorblack_exist")) {
                        System.out.println("File: " + file.getAbsolutePath());
                        String newname = file.getAbsolutePath().replace("bgcolorblack_", "");

                        makeNewExist(newname, getRealFileName(file));
                        // forceDelete(file);

                    }
                }
                // Runtime.getRuntime().exec("/bin/sh -c 'ls "+ parent.getAbsolutePath() + " | tail -1'");

                System.out.println("\n");
            }
        } catch (Exception ex) {
            System.out.println(ex.toString());
        }
    }

    public static void makeNewExist(String newname, String data) throws IOException, InterruptedException {
//        System.out.println("Data: " + data);
//        System.out.println("NewName: " + newname);
        try(  PrintWriter out = new PrintWriter( newname )  ){
            out.print( data );
        }
        // Thread.sleep(2000);
    }

    private static String getRealFileName(File file) throws IOException {
        String result;
        BufferedReader reader = new BufferedReader(new FileReader(file));
        try {
            result = reader.readLine();
        } finally {
            reader.close();
        }
        return result;
    }


    // 0_0_bgcolorblack_exist




//    public static void forceDelete(File file) throws IOException {
//        boolean filePresent = file.exists();
//        if (!file.deleteByProfileId()) {
//            if (!filePresent){
//                throw new FileNotFoundException("File does not exist: " + file);
//            }
//            String message =
//                    "Unable to deleteByProfileId file: " + file;
//            throw new IOException(message);
//        }
//    }

}






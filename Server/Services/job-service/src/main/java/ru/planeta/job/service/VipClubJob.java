package ru.planeta.job.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import ru.planeta.api.service.profile.VipClubService;

import java.util.List;

/**
 *
 * Created by a.savanovich on 05.08.2015.
 */
@Service
public class VipClubJob extends SimpleLooperJob<Long> {
    private static final long UPDATE_DELAY = 1000 * 60 * 10; // 10 min
    private final VipClubService vipClubService;

    @Autowired
    public VipClubJob(VipClubService vipClubService) {
        this.vipClubService = vipClubService;
    }

    @Override
    public List<Long> selectList(int offset, int limit) {
        return vipClubService.getAllNewVips(offset, limit);
    }

    @Override
    public boolean update(Long profileId) throws Exception {
        return vipClubService.changeUserVipStatus(profileId, true);
    }

    @Scheduled(fixedDelay = UPDATE_DELAY)
    void doJobScheduled() {
        doLoggableJob();
    }
}


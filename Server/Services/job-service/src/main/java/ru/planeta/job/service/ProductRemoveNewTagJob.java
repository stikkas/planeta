package ru.planeta.job.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import ru.planeta.api.service.shop.ProductUsersService;

@Service
public class ProductRemoveNewTagJob extends AbstractPrintableJob {
    private final ProductUsersService productUsersService;

    @Autowired
    public ProductRemoveNewTagJob(ProductUsersService productUsersService) {
        this.productUsersService = productUsersService;
    }

    @Override
    public void doJob() {
        productUsersService.removeNewTagsFromOldProducts();
    }

    @Scheduled(cron = "0 0 5 * * ?")
    void doJobScheduled() {
        doLoggableJob();
    }
}

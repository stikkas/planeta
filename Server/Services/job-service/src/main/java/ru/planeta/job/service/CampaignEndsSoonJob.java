package ru.planeta.job.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import ru.planeta.api.service.campaign.CampaignSubscribeService;
import ru.planeta.api.service.notifications.NotificationService;
import ru.planeta.model.common.campaign.CampaignSubscribe;

import java.util.List;

@Service
public class CampaignEndsSoonJob extends SimpleLooperJob<CampaignSubscribe> {
    private final CampaignSubscribeService campaignSubscribeService;
    private final NotificationService notificationService;

    @Value("${noreply.email}")
    private String noReplyEmail;

    @Autowired
    public CampaignEndsSoonJob(CampaignSubscribeService campaignSubscribeService, NotificationService notificationService) {
        this.campaignSubscribeService = campaignSubscribeService;
        this.notificationService = notificationService;
    }

    @Override
    public List<CampaignSubscribe> selectList(int offset, int limit) {
        return campaignSubscribeService.getCampaignSubsctiptionsToSend(offset, limit);
    }

    @Override
    public boolean update(CampaignSubscribe campaignSubscribe) throws Exception {
        notificationService.sendCampaignCampaignEndsSoonNotification(campaignSubscribe);
        return false;
    }

    @Scheduled(cron = "0 0 21 * * ?")
    void doJobScheduled() {
        doLoggableJob();
    }

}

package ru.planeta.job.service;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import ru.planeta.api.mail.MailClient;
import ru.planeta.api.mail.MailClientImpl;
import ru.planeta.api.service.biblio.BookService;
import ru.planeta.model.bibliodb.Book;

import java.util.Date;
import java.util.List;


/*
 * Created by Alexey on 20.04.2016.
 */

@Service
public class SetNewBookPriceJob extends SimpleLooperJob<Book> {

    private final BookService bookService;
    private final MailClient mailClient;

    private static final Logger log = Logger.getLogger(SetNewBookPriceJob.class);
    private static final String EVERY_DAY_AT_ONE_MINUTE_MIDNIGHT = "0 1 0 * * *";

    @Autowired
    public SetNewBookPriceJob(BookService bookService, MailClient mailClient) {
        this.bookService = bookService;
        this.mailClient = mailClient;
    }

    @Scheduled(cron = EVERY_DAY_AT_ONE_MINUTE_MIDNIGHT)
    void doJobScheduled() {
        doLoggableJob();
    }

    @Override
    public List<Book> selectList(int offset, int limit) {
        return bookService.getBookForChangePrice(new Date(), offset, limit);
    }

    @Override
    public boolean update(Book book) throws Exception {
        mailClient.sendBiblioBookPriceChange(book.getPrice().intValue(), book.getNewPrice().intValue(), new Date(), book.getTitle());
        book.setPrice(book.getNewPrice());
        book.setChangePriceDate(null);
        book.setNewPrice(null);
        log.info("Update book price with id " + book.getBookId());
        bookService.saveBook(book);
        return true;
    }
}

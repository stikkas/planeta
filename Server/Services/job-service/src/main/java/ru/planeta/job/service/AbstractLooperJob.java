package ru.planeta.job.service;

import ru.planeta.api.aspect.transaction.NonTransactional;

/**
 *
 * Created by asavan on 11.11.2016.
 */
abstract class AbstractLooperJob<T> extends AbstractPrintableJob {
    @Override
    @NonTransactional
    public void doJob() throws Exception {
       getLooper().doJob();
    }

    abstract Looper<T> getLooper();
}

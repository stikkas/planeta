package ru.planeta.job.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import ru.planeta.api.service.promo.PromoConfigService;


@Service
public class PromoConfigUpdateJob extends AbstractPrintableJob {
    private final PromoConfigService promoConfigService;

    @Autowired
    public PromoConfigUpdateJob(PromoConfigService promoConfigService) {
        this.promoConfigService = promoConfigService;
    }

    @Override
    public void doJob() {
        promoConfigService.finishConfigs();
    }

    @Scheduled(cron = "0 0 1 * * *")
    void doJobScheduled() {
        doLoggableJob();
    }

}

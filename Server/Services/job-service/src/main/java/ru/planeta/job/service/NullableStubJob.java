package ru.planeta.job.service;

import java.util.Date;

/**
 * Created by asavan on 11.11.2016.
 */
abstract class NullableStubJob implements TimebleAndExecutable {
    @Override
    public Date getLastTimeStarted() {
        return null;
    }

    @Override
    public Date getLastTimeFinished() {
        return null;
    }

    final public void doLoggableJob() {
        try {
            doJob();
        } catch (Exception ignored) {

        }
    }

    @Override
    public String getName() {
        return getClass().getSimpleName();
    }

    @Override
    public boolean isRunning() {
        return false;
    }
}

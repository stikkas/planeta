package ru.planeta.job.service;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

/**
 * User: a.savanovich
 * Date: 20.04.12
 * Time: 12:14
 */
@Service
public class CitiesGeoBaseRotateJob extends AbstractGeoJob {

    private static final long UPDATE_GEO_BASE_DELAY = 2 * 24 * 60 * 60 * 1000; // two days
    private static final String UPDATE_DELAY = "0 01 5 */2 * ?"; // 5:01 every two days

    private static final String GEO_CITIES_FILENAME = "cities.txt";

    private static final String TEMP_BD = "public.ipgeobase_names";
    private static final String PROD_DB = "commondb.ipgeobase_names";

    @Override
    public void doJob() throws Exception {
        downloadData(TEMP_BD, PROD_DB, GEO_CITIES_FILENAME);
    }

    @Scheduled(cron = UPDATE_DELAY)
    void doJobScheduled() {
        doLoggableJob();
    }

}

package ru.planeta.job.service;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import ru.planeta.commons.web.WebUtils;
import ru.planeta.dao.bibliodb.LibraryDAO;
import ru.planeta.model.bibliodb.Library;

import java.util.Collections;
import java.util.List;

@Service
public class LibraryRegionResolveJob extends SimpleLooperJob<Library> {
    private static final String LIBRARY_REGIONS_JOB_INTERVAL = "0 0 2 * * ?";
    private static final String YANDEX_GEOCODER_URL = "https://geocode-maps.yandex.ru/1.x/";

    private static final Logger log = Logger.getLogger(LibraryRegionResolveJob.class);

    private final LibraryDAO libraryDAO;

    @Autowired
    public LibraryRegionResolveJob(LibraryDAO libraryDAO) {
        this.libraryDAO = libraryDAO;
    }

    @Scheduled(cron = LIBRARY_REGIONS_JOB_INTERVAL)
    void doJobScheduled() {
        doLoggableJob();
    }


    @Override
    public boolean update(Library library) throws Exception {
        String jsonAddress = WebUtils.uploadMapExternal(YANDEX_GEOCODER_URL, new WebUtils.Parameters().add("format", "json")
                .add("geocode", library.getLongitude() + "," + library.getLatitude())
                .add("results", 1).getParams());

        if (jsonAddress == null) {
            return false;
        }
        ObjectMapper om = new ObjectMapper();
        JsonNode jsonNode = om.readTree(jsonAddress);
        log.debug(jsonNode);
        if (jsonNode.findPath("AdministrativeAreaName") != null) {
            String region = jsonNode.findPath("AdministrativeAreaName").asText();
            library.setRegion(region);
            log.debug("find region " + region);

            libraryDAO.update(library);
            return true;
        }
        return false;
    }

    @Override
    public List<Library> selectList(int offset, int limit) {
        Library library = libraryDAO.selectFirstLibraryWithoutRegion(offset);
        if (library == null) {
            return Collections.emptyList();
        }
        return Collections.singletonList(library);
    }

}

package ru.planeta.job.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import ru.planeta.dao.statdb.StatsUpdateDAO;

/**
 * User: a.savanovich
 * Date: 29.02.12
 * Time: 11:54
 */
@Service
public class StatUpdateJob extends AbstractPrintableJob {
    private static final long UPDATE_DELAY = 60 * 60 * 1000;

    private final StatsUpdateDAO statsUpdateDAO;

    @Autowired
    public StatUpdateJob(StatsUpdateDAO statsUpdateDAO) {
        this.statsUpdateDAO = statsUpdateDAO;
    }

    @Override
    public void doJob() {
        statsUpdateDAO.updateCommentsStats();
    }

    @Scheduled(fixedDelay = UPDATE_DELAY)
    void doJobScheduled() {
        doLoggableJob();
    }

}

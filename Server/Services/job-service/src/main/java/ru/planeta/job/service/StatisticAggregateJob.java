package ru.planeta.job.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import ru.planeta.api.aspect.transaction.LongTransactional;
import ru.planeta.dao.statdb.CampaignStatDAO;
import ru.planeta.dao.statdb.StatEventDAO;
import ru.planeta.dao.statdb.WidgetStatDAO;

/**
 * Created by asavan on 17.06.2017.
 */
@Service
public class StatisticAggregateJob extends AbstractPrintableJob {

    private final StatEventDAO statEventDAO;
    private final CampaignStatDAO campaignStatDAO;

    private final WidgetStatDAO widgetStatDAO;

    @Autowired
    public StatisticAggregateJob(StatEventDAO statEventDAO, CampaignStatDAO campaignStatDAO, WidgetStatDAO widgetStatDAO) {
        this.statEventDAO = statEventDAO;
        this.campaignStatDAO = campaignStatDAO;
        this.widgetStatDAO = widgetStatDAO;
    }


    @Override
    @LongTransactional
    public void doJob() {
        campaignStatDAO.aggregate();
        widgetStatDAO.aggregate();
        statEventDAO.cleanAggregatedRecords();
    }

    @Scheduled(cron = "0 0 1 * * *") //every day in 1:0:0
    void doJobScheduled() {
        doLoggableJob();
    }

}

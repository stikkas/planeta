package ru.planeta.job.service;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import ru.planeta.api.news.LoggerService;
import ru.planeta.dao.commondb.MailMessageDAO;
import ru.planeta.dao.profiledb.ProfileDAO;
import ru.planeta.model.enums.ProfileStatus;
import ru.planeta.model.news.ProfileNews;
import ru.planeta.model.profile.Profile;

import java.util.List;

@Service
public class EmailSpammerScanJob extends SimpleLooperJob<Long> {

    private static final long UPDATE_DELAY = 1000 * 60 * 5;
    private static final int SAME_MESSAGES_COUNT_SPAM_THRESHOLD = 10;
    private static final long JOB_PROFILE_ID = -73;

    private static final Logger log = Logger.getLogger(EmailSpammerScanJob.class);

    @Autowired
    private MailMessageDAO mailMessageDAO;
    @Autowired
    private ProfileDAO profileDAO;
    @Autowired
    private LoggerService loggerService;

    @Scheduled(fixedDelay = UPDATE_DELAY)
    void doJobScheduled() {
        doLoggableJob();
    }


    @Override
    public List<Long> selectList(int offset, int limit) {
        return mailMessageDAO.getSpammers(SAME_MESSAGES_COUNT_SPAM_THRESHOLD, offset, limit);
    }

    @Override
    public boolean update(Long profileId) throws Exception {
        Profile profile = profileDAO.selectById(profileId);
        if (profile != null) {
            profile.setStatus(ProfileStatus.USER_SPAMMER);
            profileDAO.update(profile);
            loggerService.addProfileNews(ProfileNews.Type.MAIL_SPAMMER_BLOCKED, JOB_PROFILE_ID, profileId, 0);
            log.info("Profile " + profile.getProfileId() + " marked as email spammer.");
            return true;
        } else {
            return false;
        }
    }
}

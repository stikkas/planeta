package ru.planeta.job.service;

import org.apache.commons.lang3.time.DateUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import ru.planeta.api.service.registration.RegistrationService;
import ru.planeta.dao.commondb.UserPrivateInfoDAO;
import ru.planeta.dao.profiledb.ProfileDAO;
import ru.planeta.model.common.UserPrivateInfo;
import ru.planeta.model.enums.ProfileStatus;
import ru.planeta.model.profile.Profile;

import java.util.Date;
import java.util.List;

/**
 * User: sshendyapin
 * Date: 01.11.12
 * Time: 16:36
 */

// TODO rewrite this. Need update status after email send to UNSUBSCRIBED. Not selectCampaignById such users again
@Service
public class EmailSubscribeRemainderJob extends NullableStubJob implements BaseJob {

    private static final Logger log = Logger.getLogger(EmailSubscribeRemainderJob.class);

    @Autowired
    private UserPrivateInfoDAO userPrivateInfoDAO;
    @Autowired
    private ProfileDAO profileDAO;

    @Autowired
    private RegistrationService registrationService;

    private static final int NOTIFICATION_EMAIL_DELAY = 1000 * 60 * 60; //one hour

    @Scheduled(fixedDelay = NOTIFICATION_EMAIL_DELAY)
    public void sendSecondNotificationEmail() {

        final Date twoDaysAgo = DateUtils.addDays(new Date(), -2);
        final Date twoDaysPlusOneHourAgo = DateUtils.addMilliseconds(twoDaysAgo, NOTIFICATION_EMAIL_DELAY);

        final List<UserPrivateInfo> userPrivateInfoList = userPrivateInfoDAO.selectUserPrivateInfoNotNullRegCode(twoDaysAgo, twoDaysPlusOneHourAgo, 0, 0);
        for (final UserPrivateInfo user : userPrivateInfoList) {
            try {
                final Profile profile = profileDAO.selectById(user.getUserId());
                if (profile != null && ProfileStatus.USER_ACTIVE != profile.getStatus()) {
                    registrationService.resendRegistrationCompleteEmail(user.getUserId());
                }
            } catch (Exception e) {
                log.error(e);
            }
        }
    }

    @Override
    public void doJob() {
        sendSecondNotificationEmail();
    }
}

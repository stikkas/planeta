package ru.planeta.job.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import ru.planeta.api.advertising.AdvertisingService;
import ru.planeta.api.service.content.BroadcastService;

import java.io.IOException;
import java.util.Date;

/**
 * Job starts/stops broadcasts by their time begin/time end
 *
 * @author ds.kolyshev
 *         Date: 20.03.12
 */
@Service
public class BroadcastJob extends AbstractPrintableJob {

    private static final long BROADCAST_UPDATE_DELAY = 5* 60 * 1000; // 5 min

    private final BroadcastService broadcastService;
    private final AdvertisingService advertisingService;

    @Autowired
    public BroadcastJob(BroadcastService broadcastService, AdvertisingService advertisingService) {
        this.broadcastService = broadcastService;
        this.advertisingService = advertisingService;
    }

    /**
     * Scheduled method
     */
    @Scheduled(fixedDelay = BROADCAST_UPDATE_DELAY)
    void doJobScheduled() {
        doLoggableJob();
    }

    @Override
    public void doJob() throws Exception {
        broadcastsUpdate(new Date().getTime());
        advertisingUpdate();
    }

    private void advertisingUpdate() {
        advertisingService.startFinishByDateAdvertising();
    }

    /**
     * Public for tests
     */
    private void broadcastsUpdate(long time) throws IOException {
        broadcastService.stopEndedBroadcasts(time);
        broadcastService.liveStartedBroadcasts(time);
    }

}

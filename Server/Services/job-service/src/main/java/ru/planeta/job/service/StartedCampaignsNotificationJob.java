package ru.planeta.job.service;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import ru.planeta.api.mail.MailClient;
import ru.planeta.api.service.campaign.CampaignService;
import ru.planeta.model.common.campaign.Campaign;

import java.util.Date;
import java.util.List;

/**
 * User: sshendyapin
 * Date: 10.07.13
 * Time: 15:58
 */
@Service
public class StartedCampaignsNotificationJob extends AbstractPrintableJob {

    private String[] seoEmails;

    @Autowired
    public StartedCampaignsNotificationJob(MailClient mailClient, CampaignService campaignService) {
        this.mailClient = mailClient;
        this.campaignService = campaignService;
    }

    @Value("${seo.notification.emails}")
    public synchronized void setSeoEmails(String[] seoEmails) {
        this.seoEmails = seoEmails;
    }

    private static final Logger log = Logger.getLogger(StartedCampaignsNotificationJob.class);
    private static final String EVERY_DAY_AT_ONE_MINUTE_MIDNIGHT = "0 1 0 * * *";

    private final MailClient mailClient;
    private final CampaignService campaignService;

    @Scheduled(cron = EVERY_DAY_AT_ONE_MINUTE_MIDNIGHT)
    void doJobScheduled() {
        doLoggableJob();
    }

    @Override
    public void doJob() {
        if (seoEmails.length > 0) {
            Date now = new Date();
            sendStartedCampaignsNotification(now);
            sendFinishedCampaignsNotification(now);
        } else {
            log.warn("No emails");
        }
    }

    private void sendFinishedCampaignsNotification(Date now) {
        List<Campaign> campaigns = campaignService.getCampaignsByDateRange(null, null, DateUtils.addDays(now, -1), now);
        if (!CollectionUtils.isEmpty(campaigns)) {
            for (String seoEmail : seoEmails) {
                mailClient.sendDailyCampaignsFinishedEmail(seoEmail, campaigns);
            }
        }
    }

    private void sendStartedCampaignsNotification(Date now) {
        List<Campaign> campaigns = campaignService.getCampaignsByDateRange(DateUtils.addDays(now, -1), now, null, null);
        if (!CollectionUtils.isEmpty(campaigns)) {
            for (String seoEmail : seoEmails) {
                mailClient.sendDailyCampaignsStartedEmail(seoEmail, campaigns);
            }
        }
    }

}

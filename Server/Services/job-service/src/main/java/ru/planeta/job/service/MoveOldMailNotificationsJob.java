package ru.planeta.job.service;

import org.apache.commons.lang3.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import ru.planeta.dao.commondb.MailMessageDAO;

import java.util.Date;

/**
 * User: michail
 * Date: 14.12.2016
 * Time: 12:42
 */
@Service
public class MoveOldMailNotificationsJob extends AbstractPrintableJob {
    private final MailMessageDAO mailMessageDAO;

    private static final String MOVE_MAIL_TIME = "0 0 4 * * *"; // 4 a.m

    @Autowired
    public MoveOldMailNotificationsJob(MailMessageDAO mailMessageDAO) {
        this.mailMessageDAO = mailMessageDAO;
    }

    @Override
    public void doJob() {
        Date date = DateUtils.addDays(new Date(), -1);
        mailMessageDAO.moveOldMailNotifications(date);
    }

    @Scheduled(cron = MOVE_MAIL_TIME)
    void doJobScheduled() {
        doLoggableJob();
    }


}

package ru.planeta.job.service;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import ru.planeta.api.aspect.transaction.NonTransactional;
import ru.planeta.api.service.campaign.CampaignService;
import ru.planeta.model.common.campaign.Campaign;

import java.util.List;

/**
 * User: atropnikov
 * Date: 09.04.12
 * Time: 18:36
 */
@Service
public class CampaignStartJob extends SimpleLooperJob<Campaign> {

    private static final long CAMPAIGN_STATUS_UPDATE_DELAY = 1000 * 60 * 5; // 5 min

    private static final Logger log = Logger.getLogger(CampaignStartJob.class);

    private final CampaignService campaignService;
    private static final long JOB_PROFILE_ID = -2;

    @Autowired
    public CampaignStartJob(CampaignService campaignService) {
        this.campaignService = campaignService;
    }

    @Scheduled(fixedDelay = CAMPAIGN_STATUS_UPDATE_DELAY)
    @NonTransactional
    void doJobScheduled() {
        doLoggableJob();
    }

    @Override
    public List<Campaign> selectList(int offset, int limit) {
        return campaignService.getStartedCampaignsByTime(offset, limit);
    }

    @Override
    public boolean update(Campaign item) throws Exception {
        log.info("Start campaign with id " + item.getCampaignId());
        campaignService.checkOnCampaignHasToStart(item.getCampaignId(), JOB_PROFILE_ID);
        return true;
    }
}
package ru.planeta.job.service;

import org.postgresql.copy.CopyManager;
import org.postgresql.core.BaseConnection;
import org.postgresql.ds.PGSimpleDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import ru.planeta.commons.archive.ZipUtils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * Created by a.savanovich on 14.11.2016.
 */
public abstract class AbstractGeoJob extends AbstractPrintableJob {
    private static final long UPDATE_GEO_BASE_DELAY = 2 * 24 * 60 * 60 * 1000; // two days
    private static final String GEO_BASE_URL = "http://ipgeobase.ru/files/db/Main/geo_files.zip";
    @Autowired
    private PGSimpleDataSource dataSource;

    private static String rotate(String prodDbName, String tmpDbName) {
        return "INSERT INTO " + prodDbName + " SELECT * FROM " + tmpDbName + ";";
    }

    private static String cleanBd(String bdName) {
        return "TRUNCATE " + bdName + ";";
    }

    private static String copy(String bdName) {
        return "COPY " + bdName + " FROM STDIN NULL AS '-'";
    }

    private static boolean executeCommand(String sql, Connection connection) throws SQLException {
        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            return statement.execute();
        }
    }

    protected void downloadData(String tempBd, String prodDb, String geoFilename) throws IOException, SQLException {
        // Step1: executeSql("DROP  IF EXISTS; CREATE TABLE public.blahblah
        URL url = new URL(GEO_BASE_URL);
        InputStream inputStream = url.openStream();
        InputStream zipArchiveInputStream = ZipUtils.unzipFile(inputStream, geoFilename);
        InputStreamReader br = new InputStreamReader(zipArchiveInputStream, "windows-1251");
        try (Connection connection = dataSource.getConnection()) {
            executeCommand(cleanBd(tempBd), connection);

            // Step2: COPY to tmp_blahblah
            CopyManager copyManager = new CopyManager((BaseConnection) connection);
            copyManager.copyIn(copy(tempBd), br);
            br.close();
            // Step3: DROP commondb.ipgeobase_ips, ALTER TABLE ipgeobase_ips SET SCHEMA commondb;
            executeCommand(cleanBd(prodDb) + " " + rotate(prodDb, tempBd) + " " + cleanBd(tempBd), connection);
        }
    }


}

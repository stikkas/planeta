package ru.planeta.job.service;

import org.apache.commons.lang3.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import ru.planeta.dao.commondb.MailMessageDAO;

import java.util.Date;

/*
 * Created by Alexey on 20.04.2016.
 */

@Service
public class CleanMailNotificationsLogsJob extends AbstractPrintableJob {
    private final MailMessageDAO mailMessageDAO;

    private static final String CLEAN_MAIL_TIME = "0 0 2 * * *"; // 2 a.m

    @Autowired
    public CleanMailNotificationsLogsJob(MailMessageDAO mailMessageDAO) {
        this.mailMessageDAO = mailMessageDAO;
    }

    @Override
    public void doJob() {
        Date date = DateUtils.addMonths(new Date(), -3);
        mailMessageDAO.cleanMailNotificationsLogs(date);
    }

    @Scheduled(cron = CLEAN_MAIL_TIME)
    void doJobScheduled() {
        doLoggableJob();
    }

}

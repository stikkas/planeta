package ru.planeta.job.service;

/**
 * Class BaseJob
 *
 * @author a.tropnikov
 */
public interface BaseJob {
    void doJob() throws Exception;
}

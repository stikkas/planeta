package ru.planeta.job.service;

import ru.planeta.api.aspect.transaction.NonTransactional;

/**
 *
 * Created by asavan on 11.11.2016.
 */
abstract class SimpleLooperJob<T> extends AbstractLooperJob<T> implements Selector<T>, Updater<T> {
    @Override
    @NonTransactional
    Looper<T> getLooper() {
        return new Looper<>(
                this,
                this);
    }
}

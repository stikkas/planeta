package ru.planeta.job.service;

import org.apache.commons.lang3.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import ru.planeta.dao.trashcan.CampaignDraftDAO;

import java.util.Date;

/*
 * Created by asavan on 22.11.2016.
 */

@Service
public class CleanDraftTrashJob extends AbstractPrintableJob {
    private final CampaignDraftDAO campaignDraftDAO;

    private static final String CLEAN_MAIL_TIME = "0 0 4 * * *"; // 4a.m

    @Autowired
    public CleanDraftTrashJob(CampaignDraftDAO campaignDraftDAO) {
        this.campaignDraftDAO = campaignDraftDAO;
    }

    @Override
    public void doJob() {
        Date date = DateUtils.addMonths(new Date(), -1);
        campaignDraftDAO.cleanDrafts(date);
    }

    @Scheduled(cron = CLEAN_MAIL_TIME)
    void doJobScheduled() {
        doLoggableJob();
    }

}

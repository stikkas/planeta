package ru.planeta.job.service;

import org.apache.commons.lang3.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import ru.planeta.dao.bibliodb.BinDAO;

import java.util.Date;

/**
 * Job clear table bin for bibliodb
 */
@Service
public class BiblioBinClearJob extends AbstractPrintableJob {

    private static final long  UPDATE_DELAY = 10 * 60 * 1000; // 10 min

    private final BinDAO binDAO;

    @Autowired
    public BiblioBinClearJob(BinDAO binDAO) {
        this.binDAO = binDAO;
    }

    @Scheduled(fixedDelay = UPDATE_DELAY)
    @Override
    void doJobScheduled() {
        doLoggableJob();
    }

    @Override
    public void doJob() throws Exception {
        Date deleteBefore = DateUtils.addDays(new Date(), -1);
        binDAO.deleteAllOlderDate(deleteBefore);
    }

}

package ru.planeta.job.service;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

/**
 * User: a.savanovich
 * Date: 20.04.12
 * Time: 12:14
 */
@Service
public class IpGeoBaseRotateJob extends AbstractGeoJob {

    private static final String GEO_FILENAME = "cidr_optim.txt";
    private static final String TEMP_BD = "public.ipgeobase_ips";
    private static final String PROD_DB = "commondb.ipgeobase_ips";
    private static final String UPDATE_DELAY = "0 01 5 * * SUN"; // 5:01 every sunday

    @Scheduled(cron = UPDATE_DELAY)
    void doJobScheduled() {
        doLoggableJob();
    }

    @Override
    public void doJob() throws Exception {
        downloadData(TEMP_BD, PROD_DB, GEO_FILENAME);
    }

}

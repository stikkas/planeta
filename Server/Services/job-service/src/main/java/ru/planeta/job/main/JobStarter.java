package ru.planeta.job.main;

import org.apache.log4j.Logger;
import org.eclipse.jetty.server.Handler;
import org.eclipse.jetty.server.NCSARequestLog;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.handler.DefaultHandler;
import org.eclipse.jetty.server.handler.HandlerCollection;
import org.eclipse.jetty.server.handler.RequestLogHandler;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import ru.planeta.commons.console.ParametersUtils;
import ru.planeta.job.servlet.JobServlet;

/**
 * @author ameshkov
 */
public class JobStarter {

    private static Logger log = Logger.getLogger(JobStarter.class);
    private static ClassPathXmlApplicationContext applicationContext;
    private static Server server;

    public static void main(String[] args) {
        try {
           Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {
               @Override
               public void run() {
                   onShutDown();
               }
           }));

            int port = Integer.valueOf(ParametersUtils.getParameter(args, "--port", "8091"));
            String serverLogs = ParametersUtils.getParameter(args, "--server-logs", "./logs/jetty-yyyy_mm_dd.request.log");

            applicationContext = new ClassPathXmlApplicationContext("spring/applicationContext-*.xml", "classpath*:/spring/applicationContext-dao.xml", "classpath*:/spring/applicationContext-geo.xml");

            log.info("Starting job service on port: " + port);
            server = new Server(port);

            JobServlet servlet = applicationContext.getBean(JobServlet.class);
            ServletHolder servletHolder = new ServletHolder(servlet);
            HandlerCollection handlers = new HandlerCollection();
            RequestLogHandler requestLogHandler = new RequestLogHandler();

            String path = JobStarter.class.getClassLoader().getResource("").getPath();

            ServletContextHandler servletContextHandler = new ServletContextHandler(ServletContextHandler.SESSIONS);
            servletContextHandler.setContextPath("/");
            servletContextHandler.setResourceBase(path);
            servletContextHandler.addServlet(servletHolder,"/");
            handlers.setHandlers(new Handler[]{servletContextHandler, requestLogHandler, new DefaultHandler()});
            server.setHandler(handlers);

            NCSARequestLog requestLog = new NCSARequestLog(serverLogs);
            requestLog.setRetainDays(90);
            requestLog.setAppend(true);
            requestLog.setExtended(false);
            requestLog.setLogTimeZone("GMT+3");
            requestLogHandler.setRequestLog(requestLog);

            server.start();

            log.info("Job server has been started successfully");

            server.join();
        } catch (Exception ex) {
            log.error("Unhandled exception in the main thread", ex);
        }
    }

    private static void onShutDown() {
        log.info("Stopping server and application context");
        try {
            server.stop();
            log.info("Server has been stopped successfully");
            applicationContext.close();
            log.info("Application context has been closed successfully");
        } catch (Exception ex) {
            log.error("Error stopping server or closing application context", ex);
        }
    }
}





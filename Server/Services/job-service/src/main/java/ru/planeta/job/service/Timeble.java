package ru.planeta.job.service;

import java.util.Date;

/**
 *
 * Created by asavan on 11.11.2016.
 */
public interface Timeble {
    Date getLastTimeStarted();


    Date getLastTimeFinished();

}

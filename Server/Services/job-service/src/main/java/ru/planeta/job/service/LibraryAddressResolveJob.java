package ru.planeta.job.service;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import ru.planeta.commons.web.WebUtils;
import ru.planeta.dao.bibliodb.LibraryDAO;
import ru.planeta.model.bibliodb.Library;

import java.util.Collections;
import java.util.List;

@Service
public class LibraryAddressResolveJob extends SimpleLooperJob<Library> {
    private static final String LIBRARY_COORDINATES_JOB_INTERVAL = "0 0 1 * * ?";
    private static final String YANDEX_GEOCODER_URL = "https://geocode-maps.yandex.ru/1.x/";
    private static final Logger log = Logger.getLogger(LibraryAddressResolveJob.class);

    private final LibraryDAO libraryDAO;

    @Autowired
    public LibraryAddressResolveJob(LibraryDAO libraryDAO) {
        this.libraryDAO = libraryDAO;
    }

    @Scheduled(cron = LIBRARY_COORDINATES_JOB_INTERVAL)
    void doJobScheduled() {
        doLoggableJob();
    }

    @Override
    public boolean update(Library library) throws Exception {
        String jsonAddress = WebUtils.uploadMapExternal(YANDEX_GEOCODER_URL, new WebUtils.Parameters().add("format", "json")
                .add("geocode", library.getAddress())
                .add("results", 1).getParams());

        if (jsonAddress == null) {
            return false;
        }

        ObjectMapper om = new ObjectMapper();
        JsonNode jsonNode = om.readTree(jsonAddress);
        log.debug(jsonNode);
        JsonNode point = jsonNode.findPath("Point");
        if (point == null) {
            return false;
        }
        String[] coord = point.get("pos").asText().split(" ");

        library.setLatitude(Double.valueOf(coord[1]));
        library.setLongitude(Double.valueOf(coord[0]));
        log.debug("find point " + coord[0] + ", " + coord[1]);

        libraryDAO.update(library);
        return true;
    }

    @Override
    public List<Library> selectList(int offset, int limit) {
        Library library = libraryDAO.selectFirstLibraryWithoutCoordinates(offset);
        if (library == null) {
            return Collections.emptyList();
        }
        return Collections.singletonList(library);
    }

}

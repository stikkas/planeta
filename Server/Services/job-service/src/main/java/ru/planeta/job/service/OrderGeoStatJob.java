package ru.planeta.job.service;

import org.apache.commons.lang3.time.DateUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import ru.planeta.api.geo.GeoResolverWebService;
import ru.planeta.dao.statdb.OrderGeoStatDAO;
import ru.planeta.model.profile.location.City;
import ru.planeta.model.profile.location.Country;
import ru.planeta.model.stat.OrderGeoStat;

import java.util.Date;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: michail
 * Date: 27.06.16
 * Time: 15:53
 */
@Service
public class OrderGeoStatJob extends AbstractLooperJob<OrderGeoStat> implements Updater<OrderGeoStat> {

    private final OrderGeoStatDAO orderGeoStatDAO;
    private final GeoResolverWebService geoResolver;

    private static final Logger log = Logger.getLogger(OrderGeoStatJob.class);

    private static final int RUNNING_INTERVAL = 1000 * 60 * 5;

    @Autowired
    public OrderGeoStatJob(OrderGeoStatDAO orderGeoStatDAO, GeoResolverWebService geoResolver) {
        this.orderGeoStatDAO = orderGeoStatDAO;
        this.geoResolver = geoResolver;
    }

    @Scheduled(fixedDelay = RUNNING_INTERVAL)
    void doJobScheduled() {
        doLoggableJob();
    }

    @Override
    Looper<OrderGeoStat> getLooper() {
        final Date dateFrom = DateUtils.addDays(new Date(), -1);

        return new Looper<>(new Selector<OrderGeoStat>() {
            @Override
            public List<OrderGeoStat> selectList(int offset, int limit) {
                return orderGeoStatDAO.selectUnresolved(dateFrom, offset, limit);
            }
        }, this);
    }


    @Override
    public boolean update(OrderGeoStat stat) throws Exception {
        String ipAddress = stat.getStringIpAddress();
        if (StringUtils.isNotBlank(ipAddress)) {
            City city = geoResolver.resolveCity(ipAddress);
            if (city != null) {
                stat.setCityId(city.getObjectId());
                stat.setCountryId(city.getCountryId());
            } else {
                Country country = geoResolver.resolveCountry(ipAddress);
                if (country != null) {
                    stat.setCountryId(country.getCountryId());
                } else {
                    log.info("Order " + stat.getOrderId() + ": can't resolve ip " + ipAddress);
                }
            }
        } else {
            log.info("Order " + stat.getOrderId() + " has no ipAddress");
        }
        orderGeoStatDAO.update(stat);
        return true;
    }
}

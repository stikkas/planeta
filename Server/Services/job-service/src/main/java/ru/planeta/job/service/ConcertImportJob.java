package ru.planeta.job.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.planeta.api.service.concert.ConcertImportService;

/**
 * User: michail
 * Date: 31.10.2016
 * Time: 12:13
 */

@Service
public class ConcertImportJob extends AbstractPrintableJob {
    @Autowired
    private ConcertImportService concertImportService;

    private final static int IMPORT_DELAY = 60 * 60 * 1000;

    @Override
    public void doJob() throws Exception {
        concertImportService.importMoscowShowConcerts();
    }

    void doJobScheduled() {
        doLoggableJob();
    }

}

package ru.planeta.job.service;

/*
 * Created by Alexey on 11.11.2016.
 */

import org.apache.commons.lang3.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import ru.planeta.api.service.campaign.CampaignService;
import ru.planeta.api.service.notifications.NotificationService;
import ru.planeta.api.service.profile.AuthorizationService;

import java.util.Date;
import java.util.List;

@Service
public class AuthorCampaignNotificationsJob extends SimpleLooperJob<Long> {
    private final CampaignService campaignService;
    private final NotificationService notificationService;
    private final AuthorizationService authorizationService;

    private static final int MAX_CAMPAIGNS_COUNT_PER_USER = 20;

    @Value("${noreply.email}")
    private String noReplyEmail;

    @Autowired
    public AuthorCampaignNotificationsJob(CampaignService campaignService, NotificationService notificationService, AuthorizationService authorizationService) {
        this.campaignService = campaignService;
        this.notificationService = notificationService;
        this.authorizationService = authorizationService;
    }

    @Override
    public List<Long> selectList(int offset, int limit) {
        return campaignService.selectProfileIdsOfCampaignsWithEventsForDay(offset, limit, DateUtils.addDays(new Date(), -1));
    }

    @Override
    public boolean update(Long profileId) throws Exception {
        String email = authorizationService.getUserPrivateEmailById(profileId);
        if (email != null) {
            notificationService.sendAuthorCampaignNotifications(profileId, noReplyEmail, MAX_CAMPAIGNS_COUNT_PER_USER, email, DateUtils.addDays(new Date(), -1));
        }

        return false;
    }

    @Scheduled(cron = "0 0 3 * * ?")
    void doJobScheduled() {
        doLoggableJob();
    }

}

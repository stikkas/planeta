package ru.planeta.job.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import ru.planeta.api.mail.UploaderService;
import ru.planeta.dao.commondb.CampaignDAO;
import ru.planeta.model.common.campaign.Campaign;

import java.util.List;

@Service
public class BackgroundRenewJob extends SimpleLooperJob<Campaign> {

    private static final long UPDATE_DELAY = 1000 * 60 * 30;

    private final CampaignDAO campaignDAO;
    private final UploaderService uploaderService;

    @Autowired
    public BackgroundRenewJob(CampaignDAO campaignDAO, UploaderService uploaderService) {
        this.campaignDAO = campaignDAO;
        this.uploaderService = uploaderService;
    }

    @Scheduled(fixedDelay = UPDATE_DELAY)
    void doJobScheduled() {
        doLoggableJob();
    }

    @Override
    public List<Campaign> selectList(int offset, int limit) {
        return campaignDAO.selectNotRenewedBackground(offset, limit);
    }

    @Override
    public boolean update(Campaign c) throws Exception {
        return uploaderService.renewBackground(c.getCreatorProfileId(), c.getCampaignId()) != null;
    }
}

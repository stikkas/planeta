package ru.planeta.job.service;

/*
 * Created by Alexey on 09.11.2016.
 */

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.planeta.api.service.concert.PlaceService;
import ru.planeta.model.concert.Place;
import ru.planeta.model.concert.PlaceState;
import ru.planeta.moscowshow.client.MoscowShowClient;
import ru.planeta.moscowshow.model.OrderItem;
import ru.planeta.moscowshow.model.response.ResponseCancelOrder;

import java.util.Collections;
import java.util.List;

@Service
public class UnholdTicketsJob extends SimpleLooperJob<Place> {

    private static final Logger log = Logger.getLogger(UnholdTicketsJob.class);
    private static final long UPDATE_DELAY = 5 * 60 * 1000;

    private final PlaceService placeService;
    private final MoscowShowClient moscowShowClient;

    @Autowired
    public UnholdTicketsJob(PlaceService placeService, MoscowShowClient moscowShowClient) {
        this.placeService = placeService;
        this.moscowShowClient = moscowShowClient;
    }

    //@Scheduled(fixedDelay = UPDATE_DELAY)
    void doJobScheduled() {
        doLoggableJob();
    }

    @Override
    public List<Place> selectList(int offset, int limit) {
        return placeService.selectReservedPlaces(offset, limit);
    }

    @Override
    public boolean update(Place place) throws Exception {
        place.setState(PlaceState.FREE);
        ResponseCancelOrder response = moscowShowClient.cancelOrder(Collections.singletonList(new OrderItem(place.getExternalConcertId(), place.getSectionId(), place.getRowId(), place.getPosition(), place.getPrice())));

        if (!response.hasErrors()) {
            placeService.updatePlace(place);
            return true;
        } else {
            log.error(response);
            return false;
        }
    }
}

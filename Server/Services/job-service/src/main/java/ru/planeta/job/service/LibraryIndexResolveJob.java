package ru.planeta.job.service;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import ru.planeta.commons.web.WebUtils;
import ru.planeta.dao.bibliodb.LibraryDAO;
import ru.planeta.model.bibliodb.Library;

import java.net.URL;
import java.util.Collections;
import java.util.List;

@Service
public class LibraryIndexResolveJob extends SimpleLooperJob<Library> {
    private static final String LIBRARY_INDEX_JOB_INTERVAL = "0 0 3 * * ?";
    private static final String GOOGLE_GEOCODER_URL = "https://maps.googleapis.com/maps/api/geocode/json";
    private static final Logger log = Logger.getLogger(LibraryRegionResolveJob.class);

    private final LibraryDAO libraryDAO;

    @Autowired
    public LibraryIndexResolveJob(LibraryDAO libraryDAO) {
        this.libraryDAO = libraryDAO;
    }


    @Scheduled(cron = LIBRARY_INDEX_JOB_INTERVAL)
    void doJobScheduled() {
        doLoggableJob();
    }

    @Override
    public boolean update(Library library) throws Exception {
        String jsonAddress = IOUtils.toString(new URL(new WebUtils.Parameters().add("address", library.getAddress()).createUrl(GOOGLE_GEOCODER_URL, false)));

        ObjectMapper om = new ObjectMapper();
        JsonNode jsonNode = om.readTree(jsonAddress);
        log.debug(jsonNode);
        if ("OVER_QUERY_LIMIT".equals(jsonNode.findPath("status").asText())) {
            log.debug("exceeded your rate-limit");
            // TODO need fast exit here
            return false;
        }
        if (jsonNode.findValue("address_components") != null) {
            ArrayNode addressComponents = (ArrayNode) jsonNode.findValue("address_components");
            ObjectNode postCodeObj = (ObjectNode) addressComponents.get(addressComponents.size() - 1);
            if (postCodeObj.get("types").get(0).asText().equals("postal_code")) {
                String index = postCodeObj.get("long_name").asText();
                library.setPostIndex(index);
                log.debug("find index " + index);
                libraryDAO.update(library);
                return true;

            }
        }
        return false;
    }

    @Override
    public List<Library> selectList(int offset, int limit) {
        Library library = libraryDAO.selectFirstLibraryWithoutPostIndex(offset);

        if (library == null) {
            return Collections.emptyList();
        }
        return Collections.singletonList(library);
    }

}

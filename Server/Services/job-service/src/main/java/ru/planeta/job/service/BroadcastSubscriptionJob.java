package ru.planeta.job.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import ru.planeta.api.service.content.BroadcastSubscriptionService;

import java.util.Date;

/**
 * Job notifies subscribers about upcoming broadcasts
 *
 * @author ds.kolyshev
 * Date: 16.05.13
 */
@Service
public class BroadcastSubscriptionJob extends AbstractPrintableJob {

    private static final long BROADCAST_SUBSCRIPTION_UPDATE_DELAY = 15 * 60 * 1000; // 15 minutes

	private final BroadcastSubscriptionService broadcastSubscriptionService;

    @Autowired
    public BroadcastSubscriptionJob(BroadcastSubscriptionService broadcastSubscriptionService) {
        this.broadcastSubscriptionService = broadcastSubscriptionService;
    }

    /**
     * Scheduled method
     */
    @Scheduled(fixedDelay = BROADCAST_SUBSCRIPTION_UPDATE_DELAY)
    void doJobScheduled() {
        doLoggableJob();
    }

    @Override
    public void doJob() throws Exception {
        broadcastSubscriptionService.notifyUpcomingBroadcastSubscriptions(new Date());
    }

}

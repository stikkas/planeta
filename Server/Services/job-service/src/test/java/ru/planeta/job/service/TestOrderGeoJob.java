package ru.planeta.job.service;

import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import ru.planeta.test.AbstractTest;

/**
 * Created with IntelliJ IDEA.
 * User: michail
 * Date: 21.07.16
 * Time: 13:13
 */
public class TestOrderGeoJob extends AbstractTest {
    @Autowired
    private OrderGeoStatJob orderGeoStatJob;

    @Test
    @Ignore
    public void testJob() throws Exception {
        orderGeoStatJob.doJob();
    }
}

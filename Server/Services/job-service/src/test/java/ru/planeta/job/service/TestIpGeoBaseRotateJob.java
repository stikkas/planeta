package ru.planeta.job.service;

import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import ru.planeta.test.AbstractTest;

/**
 * User: a.savanovich
 * Date: 20.04.12
 * Time: 12:29
 */
@Ignore
public class TestIpGeoBaseRotateJob extends AbstractTest {
	@Autowired
	private IpGeoBaseRotateJob ipGeoBaseRotateJob;

    @Autowired
    private CitiesGeoBaseRotateJob citiesGeoBaseRotateJob;

    @Test
    public void testUpdateDb() throws Exception {
		ipGeoBaseRotateJob.doJob();
    }

    @Test
    public void reinsertCities() throws Exception {
        citiesGeoBaseRotateJob.doLoggableJob();
    }
}

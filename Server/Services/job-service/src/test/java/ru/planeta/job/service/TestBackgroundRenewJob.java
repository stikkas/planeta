package ru.planeta.job.service;

import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import ru.planeta.test.AbstractTest;

/**
 *
 * Created by a.savanovich on 28.09.2015.
 */
@Ignore
public class TestBackgroundRenewJob  extends AbstractTest {

    @Autowired
    private BackgroundRenewJob backgroundRenewJob;

    @Test
    public void test() throws Exception {
        backgroundRenewJob.doJob();
    }
}

package ru.planeta.job.service;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import ru.planeta.test.AbstractTest;

/*
 * Created by Alexey on 20.04.2016.
 */

public class TestSetNewBookPriceJob extends AbstractTest{
    @Autowired
    private SetNewBookPriceJob setNewBookPriceJob;

    @Test
    public void testSetNewBookPriceJob() {
        setNewBookPriceJob.doLoggableJob();
    }
}

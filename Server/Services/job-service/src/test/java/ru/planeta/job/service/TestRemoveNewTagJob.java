package ru.planeta.job.service;

import org.apache.commons.collections4.IterableUtils;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import ru.planeta.api.service.shop.ProductUsersService;
import ru.planeta.model.shop.Category;
import ru.planeta.model.shop.ProductInfo;
import ru.planeta.test.AbstractTest;

import java.util.ArrayList;
import java.util.List;


public class TestRemoveNewTagJob extends AbstractTest {

	@Autowired
	private ProductRemoveNewTagJob productRemoveNewTagJob;

	@Autowired
    private ProductUsersService productUsersService;

    @Test
    @Ignore
    public void test() {
        List<Long> ids = new ArrayList<>();
        ids.add(1079L);
        ids.add(4112L);
        ids.add(4158L);
        List<ProductInfo> products = productUsersService.getProducts(ids);
        for(ProductInfo productInfo : products) {
            List<Category> tags = productInfo.getTags();
            Category newCategory = IterableUtils.find(tags, category -> "NEW".equals(category.getMnemonicName()));
            Assert.assertNotNull(newCategory);
        }

        productRemoveNewTagJob.doJob();
        products = productUsersService.getProducts(ids);
        for(ProductInfo productInfo : products) {
            List<Category> tags = productInfo.getTags();
            Category newCategory = IterableUtils.find(tags, category -> "NEW".equals(category.getMnemonicName()));
            if (productInfo.getProductId() == 4158) {
                Assert.assertNull(newCategory);
            } else {
                Assert.assertNotNull(newCategory);
            }
        }

    }
}

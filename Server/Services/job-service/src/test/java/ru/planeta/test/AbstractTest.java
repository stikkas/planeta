package ru.planeta.test;

import org.junit.After;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import ru.planeta.api.service.notifications.NotificationService;
import ru.planeta.dao.TransactionScope;

/**
 * @author: ds.kolyshev
 * Date: 29.01.13
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath*:spring/applicationContext-*.xml"})
public abstract class AbstractTest {

    private TransactionScope transactionScope;

    @Before
    public void setUp() throws Exception {
        transactionScope = TransactionScope.createOrGetCurrentTest();
    }

    @After
    public void tearDown() throws Exception {
        transactionScope.close();
    }

	@Autowired
	protected ApplicationContext context;

	protected NotificationService notificationService;

	@Autowired
	public void setNotificationService(NotificationService notificationService) {
		this.notificationService = notificationService;
		this.notificationService.setAsynchronous(false);
	}

}

<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>

<html lang="ru">
<head>
    <meta charset="UTF-8">
    <title>Planeta</title>

    <script type="text/javascript">
        function postToCP() {
            var frm = document.forms[0];
            frm.submit();
        }
    </script>
</head>
<body onload="postToCP()">

<form action="/payments/cloudpayments.html" method="POST" style="display: none;">
    <input type="hidden" name="transactionId" value="${transactionId}">
    <input type="hidden" name="sign" value="${sign}">
    <input type="submit">
</form>


</body>
</html>
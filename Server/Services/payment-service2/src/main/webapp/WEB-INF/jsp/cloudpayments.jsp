<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>

<html lang="ru">

<head>
    <meta charset="UTF-8">
    <title>Planeta</title>
    <meta http-equiv="Cache-Control" content="private">
    <script src="https://widget.cloudpayments.ru/bundles/cloudpayments"></script>
</head>
<body>

<script type="text/javascript">
    var payHandler = function () {
        var L10n = {
            _dictionary: {
                "ru": {
                    "paymentTitle": "Оплата на planeta.ru"
                },
                "en": {
                    "paymentTitle": "Payment on planeta.ru"
                }
            }
        };

        var translate = function (word, lang) {
            if (!lang) {
                lang = "ru";
            }
            return L10n._dictionary[lang][word] || word;
        };

        var langCookie = '${pageContext.response.locale}';
        var lang = (langCookie && langCookie === "en") ? "en" : "ru";
        var params = {};
        if (lang === "en") {
            params = {language: "en-US"};
        }
        var payments = new cp.CloudPayments(params);
        payments.charge({ // options
                    publicId: '${publicId}',
                    description: translate('paymentTitle', lang),
                    amount: ${transaction.amountNet}, //сумма
                    currency: 'RUB',
                    invoiceId: ${transaction.transactionId}, //номер заказа
                    accountId: "${userEmail}"
                },
                function (options) { // success
                    window.location.href = '/payment-done.html?transactionid=${transaction.transactionId}'
                },
                function (reason, options) { // fail
                    window.location.href = '/payment-not-done.html?transactionid=${transaction.transactionId}&reason='
                        + JSON.stringify(reason)
                        + '&options=' + JSON.stringify(options);
            });
    };
    payHandler();
</script>

</body>
</html>
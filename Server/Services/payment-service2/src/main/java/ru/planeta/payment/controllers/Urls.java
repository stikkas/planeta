package ru.planeta.payment.controllers;

/**
 * Created with IntelliJ IDEA.
 * User: alexa_000
 * Date: 07.05.14
 * Time: 15:03
 */
public interface Urls {

    String CALLBACK = "/callback/{alias}";
    String NON_PARSE_CALLBACK = "/streamcallback/{alias}";
    String CALLBACK_CLOUD = "/callback-cloudpayments/";
    String CALLBACK_OLD = "/{alias}-payment";
    String CHECK_CALLBACK = "/callback/check/{alias}";
    String CHECK_CALLBACK_OLD = "/{alias}-check-order";


    String VALIDATE_PAYMENTS = "/validate-payments.html";
    String REDIRECT = "/redirect.html";
    String PAYMENT_DONE = "/payment-done.html";
    String PAYMENT_NOT_DONE = "/payment-not-done.html";
    String CLOUD_PAYMENTS = "/payments/cloudpayments.html";
}

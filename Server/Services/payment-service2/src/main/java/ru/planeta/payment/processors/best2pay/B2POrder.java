package ru.planeta.payment.processors.best2pay;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Date;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: a.volkov
 * Date: 23.04.14
 * Time: 18:50
 */
@XmlRootElement(name = "order")
@XmlAccessorType(XmlAccessType.FIELD)
public class B2POrder {

    @XmlElement
    public long id;
    @XmlElement
    public String state;
    @XmlElement(name = "inprogress")
    public int inProgress;
    @XmlElement
    public Date date;
    @XmlElement
    public int amount;
    @XmlElement
    public int currency;
    @XmlElement
    public String email;
    @XmlElement
    public String phone;
    @XmlElement
    public long reference;
    @XmlElement
    public String description;
    @XmlElement
    public String signature;
    @XmlElement
    public List<B2POperation> operations;
}
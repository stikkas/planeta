package ru.planeta.payment.processors.robokassa.models;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * Created by eshevchenko on 11.08.14.
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class Result {

    public static final int SUCCESS_CODE = 0;

    @XmlElement(name = "Code")
    public Integer code;
    @XmlElement(name = "Description")
    public String description;

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isError() {
        return getCode() != SUCCESS_CODE;
    }
}

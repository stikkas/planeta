package ru.planeta.payment.processors.robokassa.models;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import java.math.BigDecimal;

/**
 * Created by eshevchenko on 11.08.14.
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class Info {
    @XmlElement(name = "IncCurrLabel")
    private String incCurrLabel;
    @XmlElement(name = "IncSum")
    private BigDecimal incSum;
    @XmlElement(name = "IncAccount")
    private String incAccount;
    @XmlElement(name = "PaymentMethod")
    private PaymentMethod paymentMethod;
    @XmlElement(name = "OutCurrLabel")
    private String outCurrLabel;
    @XmlElement(name = "OutSum")
    private BigDecimal outSum;

    public String getIncCurrLabel() {
        return incCurrLabel;
    }

    public void setIncCurrLabel(String incCurrLabel) {
        this.incCurrLabel = incCurrLabel;
    }

    public BigDecimal getIncSum() {
        return incSum;
    }

    public void setIncSum(BigDecimal incSum) {
        this.incSum = incSum;
    }

    public String getIncAccount() {
        return incAccount;
    }

    public void setIncAccount(String incAccount) {
        this.incAccount = incAccount;
    }

    public PaymentMethod getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(PaymentMethod paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public String getOutCurrLabel() {
        return outCurrLabel;
    }

    public void setOutCurrLabel(String outCurrLabel) {
        this.outCurrLabel = outCurrLabel;
    }

    public BigDecimal getOutSum() {
        return outSum;
    }

    public void setOutSum(BigDecimal outSum) {
        this.outSum = outSum;
    }
}

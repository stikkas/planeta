package ru.planeta.payment.processors.robokassa.models;


import ru.planeta.payment.processors.robokassa.models.enums.StateCode;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.datatype.XMLGregorianCalendar;

/**
 * Created by eshevchenko on 11.08.14.
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class State {
    @XmlElement(name = "Code")
    private StateCode code;
    @XmlElement(name = "RequestDate")
    private XMLGregorianCalendar RequestDate;
    @XmlElement(name = "StateDate")
    private XMLGregorianCalendar StateDate;

    public StateCode getCode() {
        return code;
    }

    public void setCode(StateCode code) {
        this.code = code;
    }

    public XMLGregorianCalendar getRequestDate() {
        return RequestDate;
    }

    public void setRequestDate(XMLGregorianCalendar requestDate) {
        RequestDate = requestDate;
    }

    public XMLGregorianCalendar getStateDate() {
        return StateDate;
    }

    public void setStateDate(XMLGregorianCalendar stateDate) {
        StateDate = stateDate;
    }
}

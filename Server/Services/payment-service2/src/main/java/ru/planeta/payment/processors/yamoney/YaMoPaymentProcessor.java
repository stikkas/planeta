package ru.planeta.payment.processors.yamoney;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import ru.planeta.api.Utils;
import ru.planeta.api.exceptions.NotFoundException;
import ru.planeta.api.exceptions.PaymentException;
import ru.planeta.api.service.billing.payment.system.yamoney.mws.MwsClient;
import ru.planeta.api.service.billing.payment.system.yamoney.mws.exceptions.YaMoException;
import ru.planeta.api.service.billing.payment.system.yamoney.mws.models.ListOrdersResponse;
import ru.planeta.api.service.billing.payment.system.yamoney.mws.models.Order;
import ru.planeta.commons.lang.DateUtils;
import ru.planeta.commons.lang.XmlStyleStringBuilder;
import ru.planeta.commons.web.WebUtils;
import ru.planeta.dao.commondb.OrderDAO;
import ru.planeta.model.common.PaymentProvider;
import ru.planeta.model.common.TopayTransaction;
import ru.planeta.model.enums.ProjectType;
import ru.planeta.payment.processors.AbstractPaymentProcessor;
import ru.planeta.payment.processors.CallbackResult;
import ru.planeta.payment.processors.PaymentResult;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.servlet.http.HttpServletRequest;
import java.io.InputStream;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;

import static ru.planeta.api.Utils.INSTANCE;

/**
 * Created by a.volkov
 */
public class YaMoPaymentProcessor extends AbstractPaymentProcessor {

    private static final class Params {
        public static final String SHOP_ID = "shopId";
        public static final String SHOP_ARTICLE_ID = "shopArticleId";
        public static final String SCID = "scid";
        public static final String SUM = "sum";
        public static final String CUSTOMER_NUMBER = "customerNumber";
        public static final String ORDER_NUMBER = "_orderNumber";
        public static final String SHOP_SUCCESS_URL = "shopSuccessURL";
        public static final String SHOP_FAIL_URL = "shopFailURL";
        public static final String REQUEST_DATETIME = "requestDatetime";
        public static final String ACTION = "action";
        public static final String MD5 = "md5";
        public static final String INVOICE_ID = "invoiceId";
        public static final String ORDER_CREATED_DATETIME = "orderCreatedDatetime";
        public static final String ORDER_SUM_AMOUNT = "orderSumAmount";
        public static final String ORDER_SUM_CURRENCY_PAYCASH = "orderSumCurrencyPaycash";
        public static final String ORDER_SUM_BANK_PAYCASH = "orderSumBankPaycash";
        public static final String SHOP_SUM_AMOUNT = "shopSumAmount";
        public static final String SHOP_SUM_CURRENCY_PAYCASH = "shopSumCurrencyPaycash";
        public static final String SHOP_SUM_BANK_PAYCASH = "shopSumBankPaycash";
        public static final String PAYMENT_PAYER_CODE = "paymentPayerCode";
        public static final String PAYMENT_DATETIME = "paymentDatetime";
        public static final String PERFORMED_DATETIME = "performedDatetime";
        public static final String CODE = "code";
        public static final String MESSAGE = "Message";
        public static final String TECH_MESSAGE = "techMessage";
        public static final String PAYMENT_TYPE = "paymentType";
        public static final String CPS_EMAIL = "cps_email";
        public static final String CPS_PHONE = "cps_phone";

        public static final String CDD_PAN_MASK = "cdd_pan_mask";
        public static final String REBILLING_ON = "rebillingOn";
        public static final String BASE_INVOICE_ID = "baseInvoiceId";
    }

    private static final class Actions {
        public static final String CHECK_ORDER = "checkOrder";
        public static final String PAYMENT_AVISO = "paymentAviso";
    }

    private static final class ResultCodes {
        public static final int SUCCESS = 0;
        public static final int AUTH_ERROR = 1;
        public static final int CONTRACT_CHANGE_SUCCESS = 2;
        public static final int REJECT_PAYMENT_RECEIVE = 100;
        public static final int REQUEST_PROCESSING_ERROR = 200;
        public static final int TECH_ERROR = 1000;
    }

    private static final class Auth {
        final String id;
        final String scid;
        final String secret;

        private Auth(String id, String scid, String secret) {
            this.id = id;
            this.scid = scid;
            this.secret = secret;
        }
    }

    private Map<String, Auth> authByShopId = new HashMap<String, Auth>();

    @Autowired
    private OrderDAO orderDAO;

    @Value("classpath:/cert/planeta_yamoney.p12")
    private Resource keyStoreFile;
    @Value("${yamo.mws.keyStorePassword:underground}")
    private String keyStorePassword;
    @Value("${yamo.mws.serviceUrl:}")
    private String mwsUrl;
    @Value("${yamo.mws.communicationTimeout:50000}")
    private Integer communicationTimeout;
    @Value("${yamoney.trustStore:}")
    private String trustStore;
    @Value("${yamoney.trustStorePassword:changeit}")
    private String trustStorePassword;

    private MwsClient mwsClient;

    private String id;
    private String scid;
    private String secret;

    private String shopId;
    private String shopScid;
    private String shopSecret;

    private String url;

    public String getSecret() {
        return secret;
    }

    public void setSecret(String secret) {
        this.secret = secret;
    }

    public String getShopSecret() {
        return shopSecret;
    }

    public void setShopSecret(String shopSecret) {
        this.shopSecret = shopSecret;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getScid() {
        return scid;
    }

    public void setScid(String scid) {
        this.scid = scid;
    }

    public String getShopId() {
        return shopId;
    }

    public void setShopId(String shopId) {
        this.shopId = shopId;
    }

    public String getShopScid() {
        return shopScid;
    }

    public void setShopScid(String shopScid) {
        this.shopScid = shopScid;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public void init() {
        super.init();

        try {
            mwsClient = MwsClient.createFor(mwsUrl, communicationTimeout, trustStore, trustStorePassword)
                    .registerAccount(NumberUtils.toLong(id), keyStoreFile, keyStorePassword)
                    .registerAccount(NumberUtils.toLong(shopId), keyStoreFile, keyStorePassword)
                    .setLogger(log)
                    .build();
        } catch (YaMoException e) {
            log.error("YandexMoney Merchant WS initialization error: ", e);
        }

        authByShopId.put(id, new Auth(id, scid, secret));
        authByShopId.put(shopId, new Auth(shopId, shopScid, shopSecret));
    }



    private String redirect(@Nonnull TopayTransaction transaction) throws PaymentException {
        Auth auth = getAuth(transaction);
        final long shopId = NumberUtils.toLong(auth.id);
        String email = getAuthorizationService().getUserPrivateEmailById(transaction.getProfileId());

        return getAuthParams(auth)
                .add(Params.ORDER_NUMBER, transaction.getTransactionId())
                .add(Params.SUM, transaction.getAmountNet().setScale(2, RoundingMode.HALF_UP))
                .add(Params.CUSTOMER_NUMBER, transaction.getProfileId())
                .add(Params.SHOP_SUCCESS_URL, getReturnUrl())
                .add(Params.SHOP_FAIL_URL, getReturnUrl(new WebUtils.Parameters().add(ERR_PARAM, 1)))
                .add(Params.PAYMENT_TYPE, transaction.getPaymentToolCode())
                .add(Params.CPS_EMAIL, INSTANCE.nvl(email, StringUtils.EMPTY))
                .add(Params.CPS_PHONE, INSTANCE.nvl(transaction.getParam1(), StringUtils.EMPTY))
                .createUrl(url, true);
    }


    @Override
    public PaymentResult validate(@Nonnull TopayTransaction payment, @Nullable Map<String, String> params) throws PaymentException {
        if (params != null && params.containsKey("error")) {
            log.warn("request contains error param = " + params.get("error") + " skip validation");
            return PaymentResult.ERROR;
        }
        try {
            Auth auth = getAuth(payment);
            long shopId = NumberUtils.toLong(auth.id);
            long invoiceId = NumberUtils.toLong(payment.getExtTransactionId());

            ListOrdersResponse response = mwsClient.listOrdersByInvoiceId(shopId, invoiceId);

            if (response != null && !INSTANCE.empty(response.orders)) {
                Order order = response.orders.get(0);
                if (order.paid) {
                    return PaymentResult.COMPLETED;
                } else {
                    if (order.avisoStatus == null) {
                        // yandex bug. see https://planeta.atlassian.net/browse/PLANETA-16841
                        return PaymentResult.ERROR;
                    }
                    switch (order.avisoStatus) {
                        case SUCCESSFULLY_NOTIFIED:
                        case NOT_DELIVERED_BUT_SUCCESSFULLY_PROCESSED:
                            return PaymentResult.COMPLETED;
                        case NOT_CREATED_YET:
                        case EXPECT_SENDING:
                            return PaymentResult.WAITING;
                        case DELIVERED_BUT_CONTRACTOR_REJECT:
                            return PaymentResult.REJECTED;
                        case DELIVERY_REJECTED:
                        default:
                            return PaymentResult.ERROR;
                    }
                }
            } else {
                return PaymentResult.ERROR;
            }

        } catch (Exception e) {
            log.error("Can't validate: ", e);
            return PaymentResult.NOT_IMPLEMENTED;
        }
    }

    @Override
    public boolean cancel(@Nonnull TopayTransaction transaction) {
        return false;
    }

    @Override
    public String prepareAndGetRedirectUrl(@Nonnull TopayTransaction transaction) throws PaymentException, NotFoundException {
        return redirect(transaction);
    }

    private boolean checkDigitSign(Map<String, String> params) {
        String outcome = params.get(Params.MD5);
        String calculated = generateDigitSign(params);

        boolean isEquals = StringUtils.equals(outcome, calculated);
        if (!isEquals) {
            log.warn(String.format("Yandex.Money digital sign incorrect: outcome[%s] calculated[%s]", outcome, calculated));
        }
        return isEquals;
    }

    @Override
    public CallbackResult handleCallback(Map<String, String> params, HttpServletRequest request) throws PaymentException {
        try {
            TopayTransaction payment = getTransaction(params);
            PaymentResult status;
            int resultCode;
            if (payment == null) {
                log.info("transaction #" + params.get(Params.ORDER_NUMBER));
                status = PaymentResult.ERROR;
                resultCode = ResultCodes.REJECT_PAYMENT_RECEIVE;
            } else if (!Utils.INSTANCE.equals(payment.getAmountNet(), new BigDecimal(params.get(Params.ORDER_SUM_AMOUNT)))) {
                log.info("wrong amount");
                status = PaymentResult.ERROR;
                resultCode = ResultCodes.REJECT_PAYMENT_RECEIVE;
            } else if (!checkDigitSign(params)) {
                log.info("wrong digit sign");
                status = PaymentResult.ERROR;
                resultCode = ResultCodes.AUTH_ERROR;
            } else {
                status = PaymentResult.COMPLETED;
                resultCode = ResultCodes.SUCCESS;
            }
            String response = formatResponse(Actions.PAYMENT_AVISO, params.get(Params.INVOICE_ID), params.get(Params.SHOP_ID), resultCode);
            log.info("Yandex.Money paymentAviso response: \n" + response);
            return new CallbackResult(status, payment, response, "Content-Type", "text/xml");
        } catch (Exception e) {
            throw new PaymentException(e);
        }
    }


    private String generateDigitSign(Map<String, String> params) {
        String shopId = params.get(Params.SHOP_ID);
        String sign = StringUtils.EMPTY;
        if (authByShopId.containsKey(shopId)) {
            sign = INSTANCE.concat(";",
                    params.get(Params.ACTION),
                    params.get(Params.ORDER_SUM_AMOUNT),
                    params.get(Params.ORDER_SUM_CURRENCY_PAYCASH),
                    params.get(Params.ORDER_SUM_BANK_PAYCASH),
                    shopId,
                    params.get(Params.INVOICE_ID),
                    params.get(Params.CUSTOMER_NUMBER),
                    authByShopId.get(shopId).secret);
        }
        return DigestUtils.md5Hex(sign).toUpperCase();
    }

    @Nullable
    private TopayTransaction getTransaction(Map<String, String> params) {
        TopayTransaction result = null;
        try {
            String baseInvoiceId = params.get(Params.BASE_INVOICE_ID);
            result = StringUtils.isBlank(baseInvoiceId)
                    ? getTransaction(NumberUtils.toLong(params.get(Params.ORDER_NUMBER)))
                    : getTransactionByExtId(PaymentProvider.Type.YANDEX_MONEY, baseInvoiceId, true);
        } finally {
            enableCallbackDBLogging(result);
        }

        return result;
    }

    @Override
    public CallbackResult handleCheckCallback(Map<String, String> params, InputStream content) throws PaymentException {
        int resultCode = ResultCodes.SUCCESS;
        TopayTransaction payment = getTransaction(params);
        if (payment == null) {
            resultCode = ResultCodes.REJECT_PAYMENT_RECEIVE;
        } else if (!checkDigitSign(params)) {
            resultCode = ResultCodes.AUTH_ERROR;
        } else {
            final boolean isNewPayment = payment.getStatus().isNew();
            final boolean isApplicableAmount = Utils.INSTANCE.equals(payment.getAmountNet(), new BigDecimal(params.get(Params.ORDER_SUM_AMOUNT)));
            if (!(isNewPayment && isApplicableAmount)) {
                resultCode = ResultCodes.REJECT_PAYMENT_RECEIVE;
            }
        }

        if (resultCode == ResultCodes.SUCCESS) {
            payment.setExtTransactionId(params.get(Params.INVOICE_ID));
            transactionDAO.update(payment);
        }

        String response = formatResponse(Actions.CHECK_ORDER, params.get(Params.INVOICE_ID), params.get(Params.SHOP_ID), resultCode);
        log.info("Yandex.Money checkOrder response: \n" + response);
        return new CallbackResult(PaymentResult.COMPLETED, payment, response, "Content-Type", "text/xml");
    }

    private static WebUtils.Parameters getAuthParams(Auth auth) {
        return new WebUtils.Parameters()
                .add(Params.SHOP_ID, auth.id)
                .add(Params.SCID, auth.scid);
    }

    private static String formatResponse(String action, String invoice, String shopId, int code) {
        return XmlStyleStringBuilder.createUnclosed(action + "Response")
                .attribute(Params.PERFORMED_DATETIME, DateUtils.formatToISO8601(new Date()))
                .attribute(Params.CODE, code)
                .attribute(Params.INVOICE_ID, invoice)
                .attribute(Params.SHOP_ID, shopId)
                .toString();
    }


    private Auth getAuth(TopayTransaction transaction) {
        Auth auth;
        long orderId = transaction.getOrderId();
        if (orderId > 0 && EnumSet.of(ProjectType.MAIN, ProjectType.WIDGETS).contains(transaction.getProjectType()) && !orderDAO.isCharityOrder(orderId)) {
            auth = authByShopId.get(id);
        } else {
            auth = authByShopId.get(shopId);
        }
        return auth;
    }
    
    @Override
    public boolean canValidate() {
        return true;
    }
}

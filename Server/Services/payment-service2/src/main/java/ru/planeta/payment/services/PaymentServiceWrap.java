package ru.planeta.payment.services;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.lang3.RandomUtils;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.stereotype.Service;
import ru.planeta.api.Utils;
import ru.planeta.api.aspect.transaction.NonTransactional;
import ru.planeta.api.aspect.transaction.Transactional;
import ru.planeta.api.exceptions.NotFoundException;
import ru.planeta.api.exceptions.PaymentException;
import ru.planeta.api.log.DBLogger;
import ru.planeta.api.log.LoggerListProxy;
import ru.planeta.api.log.service.DBLogService;
import ru.planeta.api.helper.ProjectService;
import ru.planeta.api.service.billing.payment.PaymentService;
import ru.planeta.api.utils.Locker;
import ru.planeta.dao.commondb.BillsFor1cDAO;
import ru.planeta.dao.commondb.TopayTransactionDAO;
import ru.planeta.dao.payment.PaymentProviderDAO;
import ru.planeta.dao.payment.PaymentToolDAO;
import ru.planeta.model.common.BillsFor1c;
import ru.planeta.model.common.PaymentProvider;
import ru.planeta.model.common.PaymentTool;
import ru.planeta.model.common.TopayTransaction;
import ru.planeta.model.enums.ProjectType;
import ru.planeta.model.enums.TopayTransactionStatus;
import ru.planeta.model.stat.log.LoggerType;
import ru.planeta.payment.processors.AbstractPaymentProcessor;
import ru.planeta.payment.processors.CallbackResult;
import ru.planeta.payment.processors.PaymentProcessor;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.servlet.http.HttpServletRequest;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

import static org.apache.commons.lang3.StringUtils.isEmpty;
import static ru.planeta.api.Utils.empty;

/**
 * Main service of Payment Gate app.
 * All methods used transactions should be synced through Locker
 * <p/>
 * Created with IntelliJ IDEA.
 * User: a.volkov
 * Date: 23.04.14
 * Time: 19:12
 */
@Service
public class PaymentServiceWrap {

    private static final BigDecimal HUNDRED = new BigDecimal(100);

    private static int AB_METHOD_ID_CONDITION = 4;  // CARD
    private static List<Integer> AB_TOOL_ID_CHOOSE = new ArrayList<Integer>() {{
        add(110);   // cloudpayments
        add(113);   // sberbank
    }};

    private final Map<String, PaymentProcessor> processorsCache = new ConcurrentHashMap<>();

    private final PaymentProcessingService paymentProcessingService;
    private final ObjectMapper objectMapper;
    private final PaymentProviderDAO providerDAO;
    private final PaymentToolDAO paymentToolDAO;
    private final BillsFor1cDAO billsFor1cDAO;
    private final TopayTransactionDAO transactionDAO;
    private final AutowireCapableBeanFactory beanFactory;
    private final ProjectService projectService;
    private final PaymentService paymentService;

    private final Locker<Long> locker = new Locker<Long>("transaction");

    private final Logger log;

    @Autowired
    public PaymentServiceWrap(PaymentProcessingService paymentProcessingService, ObjectMapper objectMapper, PaymentProviderDAO providerDAO, PaymentToolDAO paymentToolDAO, BillsFor1cDAO billsFor1cDAO, TopayTransactionDAO transactionDAO, AutowireCapableBeanFactory beanFactory, ProjectService projectService, DBLogService logService, PaymentService paymentService) {
        this.paymentProcessingService = paymentProcessingService;
        this.objectMapper = objectMapper;
        this.providerDAO = providerDAO;
        this.paymentToolDAO = paymentToolDAO;
        this.billsFor1cDAO = billsFor1cDAO;
        this.transactionDAO = transactionDAO;
        this.beanFactory = beanFactory;
        this.projectService = projectService;
        this.paymentService = paymentService;
        log = new LoggerListProxy("orderServiceLogger", Logger.getLogger(PaymentServiceWrap.class), DBLogger.Companion.getLogger(LoggerType.ORDER, logService));
    }

    /**
     * create new or getting from cache payment processor for provider
     *
     * @param provider payment provider
     * @return instance of PaymentProcessor
     */
    @Transactional
    @Nonnull
    private PaymentProcessor getProcessor(@Nullable PaymentProvider provider) {
        if (provider == null || isEmpty(provider.getProcessorClass())) {
            throw new IllegalArgumentException();
        }
        String key = provider.getProcessorClass() + ":" + provider.getProcessorParams();
        PaymentProcessor processor = processorsCache.get(key);
        if (processor == null) {
            try {
                Class<PaymentProcessor> clazz = (Class<PaymentProcessor>) Class.forName(provider.getProcessorClass());
                if (isEmpty(provider.getProcessorParams())) {
                    processor = clazz.newInstance();
                } else {
                    processor = objectMapper.readValue(provider.getProcessorParams(), clazz);
                }
                beanFactory.autowireBean(processor);
                processor.init();
                processorsCache.put(key, processor);
            } catch (Exception e) {
                log.error("can't instantiate payment processor class", e);
                throw new IllegalArgumentException(e);
            }
        }
        return processor;
    }

    @Transactional
    @Nonnull
    private PaymentProcessor getProcessor(@Nonnull TopayTransaction transaction) {
        return getProcessor(providerDAO.select(transaction.getPaymentProviderId()));
    }

    // for tests
    @Nonnull
    public Pair<PaymentProcessor, TopayTransaction> getProcessorAndTransaction(long transactionId) throws NotFoundException {
        TopayTransaction transaction = getTransaction(transactionId);
        PaymentProcessor processor = getProcessor(providerDAO.select(transaction.getPaymentProviderId()));
        return ImmutablePair.of(processor, transaction);
    }

    // @Transactional
    public String getRedirectUrl(@Nonnull TopayTransaction transaction) throws PaymentException {
        if (transaction.getPaymentMethodId() == 0 || transaction.getProjectType() == null) {
            throw new IllegalArgumentException();
        }

        PaymentTool tool;
        if (transaction.getProjectType() != ProjectType.MAIN) {
            tool = getPaymentTool(transaction.getProfileId(), transaction.getPaymentMethodId(), transaction.getProjectType(), transaction.getAmountNet(), transaction.isTlsSupported());
        } else {
            tool = getPaymentToolAB(transaction.getProfileId(), transaction.getPaymentMethodId(), transaction.getProjectType(), transaction.getAmountNet(), transaction.isTlsSupported());
        }
        //if no tools found, can't continue payment processing
        if (tool == null) {
            log.error("No payment tool " + transaction.getTransactionId());
            return projectService.getPaymentFailUrl(transaction);
        }

        PaymentProvider provider = providerDAO.select(tool.getPaymentProviderId());
        transaction.setPaymentToolCode(tool.getCode());
        transaction.setPaymentProviderId(provider.getId());
        transaction.setAmountFee(tool.getCommission().multiply(transaction.getAmountNet()).divide(HUNDRED, BigDecimal.ROUND_HALF_UP));
        // transactionDAO.update(transaction);

        String url;
        try {
            PaymentProcessor processor = getProcessor(provider);
            url = processor.prepareAndGetRedirectUrl(transaction);
            log.info("redirect url: " + url);
        } catch (Exception e) {
            url = projectService.getPaymentFailUrl(transaction);
            log.error("error while redirecting to payment system " + provider.getName(), e);
        }
        transactionDAO.update(transaction);
        return url;
    }

    @Nullable
    private PaymentTool getPaymentToolAB(long profileId, long paymentMethodId, ProjectType projectType, @Nullable BigDecimal amount, boolean tlsSupported) {
        if (paymentMethodId != AB_METHOD_ID_CONDITION) {
            return getPaymentTool(profileId, paymentMethodId, projectType, amount, tlsSupported);
        }

        int toolId = AB_TOOL_ID_CHOOSE.get(RandomUtils.nextInt(0, 2));
        log.info("Payment tools AB test, payment tool with id " + toolId + " chosen");
        return paymentToolDAO.getToolByToolId(toolId);
    }

    @Nullable
    private PaymentTool getPaymentTool(long profileId, long paymentMethodId, ProjectType projectType, @Nullable BigDecimal amount, boolean tlsSupported) {
        List<PaymentTool> tools = paymentToolDAO.getTools(paymentMethodId, projectType, amount, tlsSupported);
        if (Utils.empty(tools)) {
            return null;
        }
        PaymentTool tool = null;

        //selecting appropriate tool, checking limits
        for (PaymentTool t : tools) {
            //filter here
            if (t.getMonthlyLimit().compareTo(BigDecimal.ZERO) > 0 && profileId > 0 && amount != null) {
                BigDecimal paid = transactionDAO.selectMonthlyPaidSum(profileId, t.getPaymentProviderId(), t.getCode());
                if (paid.add(amount).compareTo(t.getMonthlyLimit()) < 0) {
                    tool = t;
                    break;
                }
            } else {
                tool = t;
                break;
            }
        }
        return tool;
    }

    @NonTransactional
    public TopayTransaction validate(long transactionId, @Nullable Map<String, String> params) throws PaymentException, NotFoundException {
        locker.lock(transactionId);
        try {
            return validateInternal(transactionId, params);
        } finally {
            locker.unlock(transactionId);
        }
    }

    private TopayTransaction validateInternal(long transactionId, @Nullable Map<String, String> params) throws NotFoundException {
        log.info("validating transaction #" + transactionId);
        TopayTransaction transaction = getTransaction(transactionId);

        if (!EnumSet.of(TopayTransactionStatus.NEW, TopayTransactionStatus.ERROR).contains(transaction.getStatus())) {
            log.warn("transaction #" + transactionId + " already processed with status " + transaction.getStatus());
            return transaction;
        }
        PaymentProcessor processor = getProcessor(transaction);
        try {
            switch (processor.validate(transaction, params)) {
                case COMPLETED:
                    paymentProcessingService.process(transaction);
                    break;
                case REJECTED:
                    paymentService.updatePaymentStatus(transaction, TopayTransactionStatus.ERROR);
                    break;
                case ERROR:
                    transactionDAO.update(transaction);
                    break;
                case WAITING:
                case NOT_IMPLEMENTED:
                    //TODO: do nothing in this case
                    break;
            }
        } catch (Exception e) {
            log.error("error while validating payment", e);
        }

        return transaction;
    }

    @NonTransactional
    public boolean cancel(final long transactionId) throws NotFoundException {
        locker.lock(transactionId);
        try {
            return cancelInternal(transactionId);
        } finally {
            locker.unlock(transactionId);
        }
    }

    @Transactional
    private boolean cancelInternal(long transactionId) throws NotFoundException {
        TopayTransaction transaction = getTransaction(transactionId);
        return getProcessor(transaction).cancel(transaction);
    }

    @NonTransactional
    @Nullable
    public CallbackResult handlePaymentProviderCallback(PaymentProvider.Type type, Map<String, String> params, HttpServletRequest request) throws PaymentException {
        PaymentProvider provider = providerDAO.select(type);
        CallbackResult result = null;
        try {
            result = handlePaymentCallback(provider, params, request);
        } catch (Exception e) {
            log.error("error handling payment system callback " + type.toString(), e);
        } finally {
            if (result != null && result.getTransaction() != null) {
                AbstractPaymentProcessor.enableCallbackDBLogging(result.getTransaction());
            }
        }
        if (result != null && result.getTransaction() != null) {
            TopayTransaction transaction = result.getTransaction();
            locker.lock(transaction.getTransactionId());
            try {
                handlePaymentCallbackResult(result);
            } finally {
                locker.unlock(transaction.getTransactionId());
            }
        }
        return result;
    }

    @Transactional
    @Nullable
    private CallbackResult handlePaymentCallback(PaymentProvider provider, Map<String, String> params, HttpServletRequest request) throws Exception {
        return getProcessor(provider).handleCallback(params, request);
    }

    private void handlePaymentCallbackResult(@Nonnull CallbackResult result) {
        TopayTransaction transaction = transactionDAO.select(result.getTransaction().getTransactionId());
        if (transaction == null) {
            log.error("No transaction in db: " + result.getTransaction().getTransactionId());
            return;
        }
        if (!EnumSet.of(TopayTransactionStatus.NEW, TopayTransactionStatus.ERROR).contains(transaction.getStatus())) {
            log.warn("transaction #" + transaction.getTransactionId() + " already processed with state " + transaction.getStatus());
            return;
        }
        switch (result.getPaymentResult()) {
            case COMPLETED:
                paymentProcessingService.process(transaction);
                break;
            case REJECTED:
                paymentService.updatePaymentStatus(transaction, TopayTransactionStatus.ERROR);
                break;
        }
    }

    @Transactional
    @Nullable
    public CallbackResult handlePaymentProviderCheckCallback(PaymentProvider.Type type, Map<String, String> params, InputStream content) throws PaymentException {
        PaymentProcessor processor = getProcessor(providerDAO.select(type));
        CallbackResult result = null;
        try {
            result = processor.handleCheckCallback(params, content);
        } catch (Exception e) {
            log.error("error handling payment system callback", e);
        }
        return result;
    }

    @Nonnull
    public TopayTransaction getTransaction(long transactionId) throws NotFoundException {
        TopayTransaction transaction = transactionDAO.select(transactionId);
        if (transaction == null) {
            log.error("transaction with id " + transactionId + " not found");
            throw new NotFoundException(TopayTransaction.class, transactionId);
        }
        return transaction;
    }

    public List<TopayTransaction> getTransactionsForPooling(TopayTransactionStatus status, int offset, int limit) {
        return transactionDAO.selectForPooling(status, offset, limit);
    }

    public List<BillsFor1c> getTransactionsForPooling1c(int offset, int limit) {
        return billsFor1cDAO.selectListByStatus(TopayTransactionStatus.NEW, offset, limit);
    }

    public List<TopayTransaction> getPayments(TopayTransactionStatus status, Date from, Date to) {
        return transactionDAO.selectTopayTransactions(null, status, null, null, null, from, to, 0L, 0, 0);
    }

}

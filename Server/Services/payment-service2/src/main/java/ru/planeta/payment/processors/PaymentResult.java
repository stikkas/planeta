package ru.planeta.payment.processors;

/**
 * Created with IntelliJ IDEA.
 * User: alexa_000
 * Date: 07.05.14
 * Time: 15:46
 */
public enum PaymentResult {
    COMPLETED,
    REJECTED,
    WAITING,
    ERROR,
    NOT_IMPLEMENTED
}

package ru.planeta.payment.processors.rbkmoney;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.log4j.Logger;
import ru.planeta.api.Utils;
import ru.planeta.api.exceptions.NotFoundException;
import ru.planeta.api.exceptions.PaymentException;
import ru.planeta.commons.web.WebUtils;
import ru.planeta.model.common.TopayTransaction;
import ru.planeta.model.profile.Profile;
import ru.planeta.payment.processors.AbstractPaymentProcessor;
import ru.planeta.payment.processors.CallbackResult;
import ru.planeta.payment.processors.PaymentResult;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Map;

import static ru.planeta.api.Utils.nvl;

/**
 * Created with IntelliJ IDEA.
 * User: a.volkov
 * Date: 27.10.2014
 * Time: 18:46
 */
public class RBKMoneyPaymentProcessor extends AbstractPaymentProcessor {

    private static final String REDIRECT_URL = "https://sko.rbkmoney.ru/opencms/opencms/default/index.html";
    private static final String PAYMENT_URL = "https://rbkmoney.ru/eshop/makepurchase.ashx";
    private static final String PAYMENT_REDIRECT_URL = "https://rbkmoney.ru/acceptpurchase.aspx";
    private static final String REFUND_URL = "https://rbkmoney.ru/payments/refund.xml";

    private static final Logger log = Logger.getLogger(RBKMoneyPaymentProcessor.class);


    private static final String CURRENCY = "RUR";

    private String shopId;
    private String secret;
    private boolean ignoreSign = false;

    private static class Error {
        private final int code;
        private final String message;

        private Error(int code, String message) {
            this.code = code;
            this.message = message;
        }

        public static Error parse(String in) {
            int index = in.indexOf(' ');
            String codeStr = in.substring(0, index);
            String descrStr = in.substring(index + 1);
            int code = Integer.parseInt(codeStr.substring(codeStr.indexOf('=') + 1));
            String descr = descrStr.substring(descrStr.indexOf('=') + 1);
            return new Error(code, descr);
        }

        @Override
        public String toString() {
            return code + ": " + message;
        }
    }

    private interface Params {
        String ORDER_ID = "orderId";
        String SHOP_ID = "eshopId";
        String INVOICE_ID = "invoiceId";
        String PAYMENT_ID = "paymentId";
        String SERVICE_NAME = "serviceName";
        String AMOUNT = "recipientAmount";
        String CURRENCY = "recipientCurrency";
        String EMAIL = "user_email";
        String DESCR = "serviceName";
        String PREFERENCE = "preference";
        String SUCCESS_URL = "successUrl";
        String FAIL_URL = "failUrl";
        String SIGN = "hash";
    }

    private interface CallbackParams {
        String SHOP_ID = "eshopId";
        String EMAIL = "userEmail";
        String INVOICE_ID = "invoiceId";
        String PAYMENT_ID = "paymentId";
        String ORDER_ID = "orderId";
        String SHOP_ACCOUNT = "eshopAccount";
        String AMOUNT = "purchaseAmount";
        String PAYMENT_AMOUNT = "paymentAmount";
        String PAYER_ACCOUNT = "payerAccount";
        String PAYMENT_DATE = "paymentData";
        String STATUS = "paymentStatus";
        String DESCR = "serviceName";
        String SIGN = "hash";
        String USER_NAME = "userName";
    }


    public boolean isIgnoreSign() {
        return ignoreSign;
    }

    public void setIgnoreSign(boolean ignoreSign) {
        this.ignoreSign = ignoreSign;
    }

    public String getShopId() {
        return shopId;
    }

    public void setShopId(String shopId) {
        this.shopId = shopId;
    }

    public String getSecret() {
        return secret;
    }

    public void setSecret(String secret) {
        this.secret = secret;
    }

    private String redirect(@Nonnull TopayTransaction transaction) throws PaymentException, NotFoundException {
        Profile profile = profileService.getProfileSafe(transaction.getProfileId());
        String email = getAuthorizationService().getUserPrivateEmailById(profile.getProfileId());

        BigDecimal amount = transaction.getAmountNet().setScale(2, RoundingMode.HALF_UP);
        String descr = getDescriptionMessage(transaction, profile);
        WebUtils.Parameters params = new WebUtils.Parameters()
                .add(Params.AMOUNT, transaction.getAmountNet())
                .add(Params.CURRENCY, CURRENCY)
                .add(Params.SHOP_ID, shopId)
                .add(Params.EMAIL, email)
                .add(Params.DESCR, descr)
                .add(Params.ORDER_ID, transaction.getTransactionId())
                .add(Params.SUCCESS_URL, super.getReturnUrl())
                .add(Params.SIGN, sign(amount, email, descr, transaction.getTransactionId()));

        return params.createUrl(PAYMENT_REDIRECT_URL, false);
    }

    @Override
    public PaymentResult validate(@Nonnull TopayTransaction transaction, @Nullable Map<String, String> params) throws PaymentException {
        return PaymentResult.NOT_IMPLEMENTED;
    }

    @Override
    public boolean cancel(@Nonnull TopayTransaction transaction) {
        return false;
    }

    @Override
    public CallbackResult handleCallback(Map<String, String> params, HttpServletRequest request) throws PaymentException {
        int status = NumberUtils.toInt(params.get(CallbackParams.STATUS));
        if (status == 0) {
            return null;
        }
        long transactionId = NumberUtils.toLong(params.get(CallbackParams.ORDER_ID));
        BigDecimal amount = new BigDecimal(Utils.nvl(params.get(CallbackParams.PAYMENT_AMOUNT), params.get(CallbackParams.AMOUNT)));

        String userName = params.get(CallbackParams.USER_NAME);
        String userEmail = params.get(CallbackParams.EMAIL);
        String descr = params.get(CallbackParams.DESCR);
        String sign = params.get(CallbackParams.SIGN);
        String payerAccount = params.get(CallbackParams.PAYER_ACCOUNT);
        String paymentDate = params.get(CallbackParams.PAYMENT_DATE);

        PaymentResult paymentResult;

        TopayTransaction transaction = getTransaction(transactionId);
        String ourSign = callbackSign(amount, payerAccount, userName, userEmail, status, descr, transactionId, paymentDate);
        log.info("our sign: " + ourSign);
        if (transaction == null) {
            log.info("transaction #" + transactionId + " not found");
            paymentResult = PaymentResult.ERROR;
        } else if (!Utils.equals(transaction.getAmountNet(), amount)) {
            log.info("amount doesn't match for transaction #" + transactionId);
            paymentResult = PaymentResult.ERROR;
        } else if (!ignoreSign && !ourSign.equals(sign)) {
            log.info("sign doesn't match for transaction #" + transactionId);
            paymentResult = PaymentResult.ERROR;
        } else {
            switch (status) {
                case 3:
                    paymentResult = PaymentResult.WAITING;
                    break;
                case 4:
                    paymentResult = PaymentResult.REJECTED;
                    break;
                case 5:
                    paymentResult = PaymentResult.COMPLETED;
                    break;
                default:
                    log.warn("unknown payment status: " + status);
                    paymentResult = PaymentResult.WAITING;
            }
        }
        return new CallbackResult(paymentResult, transaction, "OK");
    }

    private String sign(BigDecimal amount, String email, String descr, long transactionId) {
        String sign = Utils.concat("::", shopId, amount.setScale(2, RoundingMode.HALF_UP), CURRENCY, email, Utils.nvl(descr, ""), transactionId, "", secret);
        return DigestUtils.md5Hex(sign);
    }

    String callbackSign(BigDecimal amount, String payerAccount, String userName, String userEmail,
                                int paymentStatus, String descr, long transactionId, String paymentDate) {
        String sign = Utils.concat("::", shopId, transactionId, descr, payerAccount,
                amount.setScale(2, RoundingMode.HALF_UP), CURRENCY,
                paymentStatus, userName, userEmail, paymentDate, secret);
        log.info(sign);
        return DigestUtils.md5Hex(sign);
    }

    @Override
    public String prepareAndGetRedirectUrl(@Nonnull TopayTransaction transaction) throws PaymentException, NotFoundException {
        return redirect(transaction);
    }
}

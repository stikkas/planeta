package ru.planeta.payment.processors.sberbank;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.Map;

@JsonAutoDetect
@JsonIgnoreProperties(ignoreUnknown = true)
public class CallbackRequest {
    public interface Params {
        String MDORDER = "mdOrder";
        String ORDERNUM = "orderNumber";
        String OPERATION = "operation";
        String STATUS = "status";
        String CHECKSUM = "checksum";

        String OPERATION_DEPOSITED = "deposited";
    }

    private String mdOrder;
    private Integer orderNumber;
    private String checksum;
    private String operation;
    private Integer status;

    public CallbackRequest(Map<String, String> params) {
        this.mdOrder = params.get(Params.MDORDER);
        this.orderNumber = Integer.parseInt(params.get(Params.ORDERNUM));
        this.checksum = params.get(Params.CHECKSUM);
        this.operation = params.get(Params.OPERATION);
        this.status = Integer.parseInt(params.get(Params.STATUS));
    }

    public String getMdOrder() {
        return mdOrder;
    }

    public void setMdOrder(String mdOrder) {
        this.mdOrder = mdOrder;
    }

    public Integer getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(Integer orderNumber) {
        this.orderNumber = orderNumber;
    }

    public String getChecksum() {
        return checksum;
    }

    public void setChecksum(String checksum) {
        this.checksum = checksum;
    }

    public String getOperation() {
        return operation;
    }

    public void setOperation(String operation) {
        this.operation = operation;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
}

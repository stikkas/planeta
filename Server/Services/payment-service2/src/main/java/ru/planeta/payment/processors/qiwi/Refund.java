package ru.planeta.payment.processors.qiwi;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created with IntelliJ IDEA.
 * User: a.volkov
 * Date: 15.05.14
 * Time: 12:51
 */
@JsonAutoDetect
@JsonIgnoreProperties(ignoreUnknown = true)
public class Refund {
    private String status;
    private int error;
    private String user;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getError() {
        return error;
    }

    public void setError(int error) {
        this.error = error;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }
}

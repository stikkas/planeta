package ru.planeta.payment.services;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import ru.planeta.api.service.notifications.NotificationService;
import ru.planeta.model.common.BillsFor1c;
import ru.planeta.model.common.TopayTransaction;
import ru.planeta.model.enums.TopayTransactionStatus;
import ru.planeta.payment.processors.PaymentProcessor;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: a.volkov
 * Date: 05.05.14
 * Time: 17:05
 */
@Service
@Lazy(false)
public class PoolingService {

    private static final long POOL_UPDATE_DELAY = 1000 * 60 * 5; // 5 min
    private static final Logger log = Logger.getLogger(PoolingService.class);

    private final PaymentServiceWrap paymentServiceWrap;
    private final NotificationService notificationService;

    @Autowired
    public PoolingService(PaymentServiceWrap paymentServiceWrap, NotificationService notificationService) {
        this.paymentServiceWrap = paymentServiceWrap;
        this.notificationService = notificationService;
    }

    @Scheduled(fixedDelay = POOL_UPDATE_DELAY)
    public void pool() {
        log.info("Start pooling");
        List<TopayTransaction> transactions = paymentServiceWrap.getTransactionsForPooling(TopayTransactionStatus.NEW, 0, 1000);
        for (TopayTransaction t : transactions) {
            try {
                paymentServiceWrap.validate(t.getTransactionId(), null);
            } catch (Exception e) {
                log.error(e);
            }
        }
        log.info("Stop pooling");
    }


    @Scheduled(fixedDelay = POOL_UPDATE_DELAY * 5)
    public void pool1c() {
        log.info("Start 1C pooling");
        List<BillsFor1c> transactions = paymentServiceWrap.getTransactionsForPooling1c(0, 100);
        for (BillsFor1c t : transactions) {
            try {
                paymentServiceWrap.validate(t.getTopayTransactionId(), null);
            } catch (Exception e) {
                log.error(e);
            }
        }
        log.info("Stop 1C pooling");
    }
    
    private static final String EVERY_DAY_AT_TWO_AM = "0 0 2 * * ?";
    
    @Scheduled(cron = EVERY_DAY_AT_TWO_AM)
    public void checkPaymentTransaction() {
        log.info("Start checking payments");
        int i = 0;
        int checked = 0;
        List<TopayTransaction> transactions = paymentServiceWrap.getTransactionsForPooling(TopayTransactionStatus.DONE, i++, 1);
        while(!transactions.isEmpty()) {
            TopayTransaction transaction = transactions.get(0);
            try {
                PaymentProcessor processor = paymentServiceWrap.getProcessorAndTransaction(transaction.getTransactionId()).getLeft();
                if(processor.canValidate()) {
                    log.info("Check transaction #" + transaction.getTransactionId());
                    final String processorName = processor.getClass().getSimpleName();
                    log.info("Processor name " + processorName);
                    switch(processor.validate(transaction, null)) {
                    case ERROR:
                        log.warn("Transacton #"+transaction.getTransactionId()+" ERROR on validation.");
                        notificationService.sendPaymentInvalidStateNotification(processorName, transaction, true);
                        break;
                    case REJECTED:
                        log.warn("Transacton #"+transaction.getTransactionId()+" REJECTED on validation.");
                        notificationService.sendPaymentInvalidStateNotification(processorName, transaction, false);
                        break;
                    default:
                    }
                    checked++;
                }
            } catch (Exception e) {
                log.error(e);
            }
            transactions = paymentServiceWrap.getTransactionsForPooling(TopayTransactionStatus.DONE, i++, 1);
        }
        log.info("End checking payments. Checked " + checked + " transactions. Total transactions: " + i);
    }
    
    
}

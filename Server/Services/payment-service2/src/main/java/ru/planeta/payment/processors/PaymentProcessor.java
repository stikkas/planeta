package ru.planeta.payment.processors;

import ru.planeta.api.exceptions.NotFoundException;
import ru.planeta.api.exceptions.PaymentException;
import ru.planeta.model.common.TopayTransaction;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.servlet.http.HttpServletRequest;
import java.io.InputStream;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: a.volkov
 * Date: 23.04.14
 * Time: 17:34
 */
public interface PaymentProcessor {

    String ERR_PARAM = "error";


    String prepareAndGetRedirectUrl(@Nonnull TopayTransaction transaction) throws PaymentException, NotFoundException;

    /**
     * validate that payment is done
     *
     * @param transaction transaction
     * @param params
     * @return true if payment is valid
     * @throws ru.planeta.api.exceptions.PaymentException
     */
    PaymentResult validate(@Nonnull TopayTransaction transaction, @Nullable Map<String, String> params) throws PaymentException;

    /**
     * cancel payment if it's supported by payment system
     *
     * @param transaction transaction
     * @return true if operation was successful
     */
    boolean cancel(@Nonnull TopayTransaction transaction);

    /**
     * handle callback from payment system
     *
     * @param request request
     * @param params  parameters
     * @return 
     * @throws ru.planeta.api.exceptions.PaymentException
     */
    @Nullable
    CallbackResult handleCallback(Map<String, String> params, HttpServletRequest request) throws PaymentException;

    @Nullable
    CallbackResult handleStreamCallback(HttpServletRequest request) throws PaymentException;

    CallbackResult handleCheckCallback(Map<String, String> params, InputStream content) throws PaymentException;

    void init();
    
    boolean canValidate();
}

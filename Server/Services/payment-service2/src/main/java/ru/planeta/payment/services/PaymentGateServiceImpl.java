package ru.planeta.payment.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.planeta.api.aspect.transaction.NonTransactional;
import ru.planeta.api.exceptions.NotFoundException;
import ru.planeta.api.exceptions.OrderException;
import ru.planeta.api.exceptions.PaymentException;
import ru.planeta.api.exceptions.PermissionException;
import ru.planeta.api.remote.PaymentGateService;
import ru.planeta.model.common.TopayTransaction;
import ru.planeta.model.enums.TopayTransactionStatus;

/**
 * User: a.volkov
 * Date: 13.05.14
 * Time: 11:43
 */
@Service("paymentGateService")
public class PaymentGateServiceImpl implements PaymentGateService {

    private final PaymentServiceWrap paymentServiceWrap;
    private final PaymentProcessingService paymentProcessingService;

    @Autowired
    public PaymentGateServiceImpl(PaymentServiceWrap paymentServiceWrap, PaymentProcessingService paymentProcessingService) {
        this.paymentServiceWrap = paymentServiceWrap;
        this.paymentProcessingService = paymentProcessingService;
    }

    @Override
    public boolean cancelPayment(long transactionId) throws NotFoundException {
        return paymentServiceWrap.cancel(transactionId);
    }

    @Override
    @NonTransactional
    public boolean validate(long transactionId) throws PaymentException, NotFoundException {
        TopayTransaction transaction = paymentServiceWrap.validate(transactionId, null);
        return transaction.getStatus() == TopayTransactionStatus.DONE;
    }


    @Override
    public void purchaseOrder(long orderId) throws PermissionException, NotFoundException, OrderException {
        paymentProcessingService.purchaseOrderNotSafe(orderId);
    }
}

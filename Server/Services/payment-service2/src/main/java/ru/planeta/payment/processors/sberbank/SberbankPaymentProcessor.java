package ru.planeta.payment.processors.sberbank;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.codec.binary.Hex;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Value;
import ru.planeta.api.Utils;
import ru.planeta.api.exceptions.NotFoundException;
import ru.planeta.api.exceptions.PaymentException;
import ru.planeta.commons.web.WebUtils;
import ru.planeta.model.common.TopayTransaction;
import ru.planeta.payment.processors.AbstractPaymentProcessor;
import ru.planeta.payment.processors.CallbackResult;
import ru.planeta.payment.processors.PaymentResult;
import ru.planeta.payment.processors.alfabank.ReverseResponse;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.math.BigDecimal;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.*;

import static ru.planeta.api.Utils.empty;

public class SberbankPaymentProcessor extends AbstractPaymentProcessor {
    private interface Params {
        String LOGIN = "userName";
        String PASSWORD = "password";
        String TRANSACTION_ID = "orderNumber";
        String AMOUNT = "amount";
        String CURRENCY = "currency";
        String RETURN_URL = "returnUrl";
        String LANGUAGE = "language";
        String PAYMENT_ID = "orderId";
        String FORM_URL = "formUrl";
        String ERROR_CODE = "errorCode";
        String ERROR_MESSAGE = "errorMessage";
    }

    private enum Service {
        REGISTER("register.do"),
        REVERSE("reverse.do"),
        STATUS("getOrderStatusExtended.do");

        public final String uri;

        Service(String uri) {
            this.uri = uri;
        }
    }

    private static final String LANG = "ru";
    private static final Integer CURRENCY = 643;
    private static final BigDecimal HUNDRED = new BigDecimal(100);

    @Value("${sberbank.secret.key:}")
    private String secretKey;

    private String url;
    private String login;
    private String password;

    private final ObjectMapper mapper;

    public SberbankPaymentProcessor() {
        mapper = new ObjectMapper();
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String prepareAndGetRedirectUrl(@Nonnull TopayTransaction transaction) throws PaymentException, NotFoundException {
        return prepare(transaction);
    }

    private String prepare(@Nonnull TopayTransaction transaction) throws PaymentException {
        WebUtils.Parameters params = new WebUtils.Parameters()
                .add(Params.LOGIN, login)
                .add(Params.PASSWORD, password)
                .add(Params.TRANSACTION_ID, transaction.getTransactionId())
                .add(Params.AMOUNT, transaction.getAmountNet().multiply(HUNDRED).intValue())
                .add(Params.CURRENCY, CURRENCY)
                .add(Params.LANGUAGE, LANG)
                .add(Params.RETURN_URL, getReturnUrl(transaction.getTransactionId()));
        try {
            String response = IOUtils.toString(new URI(params.createUrl(getServiceUrl(Service.REGISTER), true)));
            log.info("Sberbank payment register response: " + response);
            if (Utils.empty(response)) {
                throw new PaymentException("have no response from Sberbank");
            }
            Map<String, String> data = mapper.readValue(response, new TypeReference<Map<String, String>>() {});
            int errorCode = data.containsKey(Params.ERROR_CODE) ? NumberUtils.toInt(data.get(Params.ERROR_CODE)) : 0;
            if (errorCode != 0) {
                transaction.setExtErrorCode(Integer.parseInt(data.get(Params.ERROR_CODE)));
                transaction.setExtErrorMessage(data.get(Params.ERROR_MESSAGE));
                throw new PaymentException("got error response from Sberbank: " + data);
            } else {
                transaction.setExtTransactionId(data.get(Params.PAYMENT_ID));
                return data.get(Params.FORM_URL);
            }
        } catch (IOException | URISyntaxException e) {
            log.error("error while register payment to Sberbank", e);
            throw new PaymentException(e);
        }
    }

    @Override
    public PaymentResult validate(@Nonnull TopayTransaction transaction, @Nullable Map<String, String> requestParams) throws PaymentException {
        WebUtils.Parameters params = new WebUtils.Parameters();
        params.add(Params.LOGIN, login)
                .add(Params.PASSWORD, password)
                .add(Params.PAYMENT_ID, transaction.getExtTransactionId())
                .add(Params.LANGUAGE, LANG);
        String url = params.createUrl(getServiceUrl(Service.STATUS), true);
        PaymentResult result;
        try {
            String response = IOUtils.toString(new URI(url));
            log.info("Sberbank status response: " + response);
            if (Utils.empty(response)) {
                return PaymentResult.ERROR;
            }
            StatusResponse statusResponse = mapper.readValue(response, StatusResponse.class);
            Integer orderStatus = statusResponse.getOrderStatus();
            String transactionId = statusResponse.getOrderNumber();

            log.info("Sberbank status code: " + orderStatus + "; transactionId: " + transactionId);
            if (orderStatus == null || Utils.empty(transactionId)) {
                return PaymentResult.ERROR;
            }

            switch (orderStatus) {
                case 2:
                    result = PaymentResult.COMPLETED;
                    break;
                default:
                    transaction.setExtErrorCode(statusResponse.getActionCode());
                    transaction.setExtErrorMessage(statusResponse.getActionDescription());
                    result = PaymentResult.REJECTED;
            }
        } catch (Exception e) {
            log.error("error while getting payment status from Sberbank", e);
            result = PaymentResult.ERROR;
        }
        return result;
    }

    @Override
    public boolean canValidate() {
        return true;
    }

    @Override
    public boolean cancel(@Nonnull TopayTransaction transaction) {
        WebUtils.Parameters params = new WebUtils.Parameters()
                .add(Params.LOGIN, login)
                .add(Params.PASSWORD, password)
                .add(Params.PAYMENT_ID, transaction.getExtTransactionId());
        boolean result;
        try {
            String response = IOUtils.toString(new URI((params.createUrl(getServiceUrl(Service.REVERSE), true))));
            log.info("Sberbank reverse response: " + response);
            if (Utils.empty(response)) {
                throw new PaymentException("have no response from Sberbank");
            }
            ReverseResponse reverseResponse = mapper.readValue(response, ReverseResponse.class);
            result = reverseResponse.getErrorCode() == null || reverseResponse.getErrorCode() == 0;
        } catch (Exception e) {
            log.error("error occur while canceling payment", e);
            result = false;
        }
        return result;
    }

    private String getServiceUrl(Service service) {
        return url.concat(service.uri);
    }


    // request http://site.ru/path?amount=123456&orderNumber=10747&checksum=DBBE9E54D42072D8CAF32C7F660DEB82086A25C14FD813888E231A99E1220AB3&mdOrder=3ff6962a-7dcc-4283-ab50-a6d7dd3386fe&operation=deposited&status=1
    // amount;123456;mdOrder;3ff6962a-7dcc-4283-ab50-a6d7dd3386fe;operation;deposited;orderNumber;10747;status;1;
    @Override
    public CallbackResult handleCallback(Map<String, String> params, HttpServletRequest request) {
        log.info("Sberbank callback message");
        if (params == null) {
            log.error("Error sberbank request: params is null");
            return getErrorResult();
        }

        CallbackRequest callbackRequest;
        try {
            callbackRequest = new CallbackRequest(params);
        } catch (Exception ex) {
            log.error("Sberbank: bad callback request");
            return getErrorResult();
        }

        if (StringUtils.isEmpty(callbackRequest.getOperation())
                || !callbackRequest.getOperation().equals(CallbackRequest.Params.OPERATION_DEPOSITED)) {
            log.error("Sberbank: callback operation " + callbackRequest.getOperation() + "is not supported.");
            return getErrorResult();
        }

        if (callbackRequest.getStatus() == null
                || StringUtils.isEmpty(callbackRequest.getMdOrder())
                || callbackRequest.getOrderNumber() == null) {
            log.error("Sberbank: bad callback request");
            return getErrorResult();
        }

        if (StringUtils.isEmpty(callbackRequest.getChecksum())) {
            log.error("Sberbank bad callback request: no checksum");
            return getErrorResult();
        }
        log.info("Sberbank checksum is " + callbackRequest.getChecksum());


        // populating paramString and get request checksum
        StringBuilder paramStringBuilder = new StringBuilder();
        Set<String> keySet = params.keySet();
        keySet.remove(CallbackRequest.Params.CHECKSUM);
        List<String> keys = new ArrayList<>(params.keySet());
        Collections.sort(keys);
        for (String key : keys) {
            paramStringBuilder.append(key).append(";").append(params.get(key)).append(";");
        }
        String paramString = paramStringBuilder.toString();

        if (StringUtils.isEmpty(paramString)) {
            log.error("Error sberbank request: no params");
            return getErrorResult();
        }
        log.info("Sberbank params is " + paramString);

        String calculatedChecksum;
        try {
            calculatedChecksum = generateHMacSHA256(secretKey, paramString).toUpperCase();
        } catch (Exception ex) {
            log.error("Sberbank error while calculating checksum:", ex);
            return getErrorResult();
        }

        if (!callbackRequest.getChecksum().equals(calculatedChecksum)) {
            log.error("Sberbank: bad checksum. calculated = " + calculatedChecksum + " sent = " + callbackRequest.getChecksum() + " params = " + paramString);
            return getErrorResult();
        }

        PaymentResult paymentResult = callbackRequest.getStatus() == 1 ? PaymentResult.COMPLETED : PaymentResult.REJECTED;
        final TopayTransaction topayTransaction;
        try {
            topayTransaction = getTransactionSafe(callbackRequest.getOrderNumber());
        } catch (Exception ex) {
            log.error("Sberbank: topay transaction with id " + callbackRequest.getOrderNumber() + " not found");
            return getErrorResult();
        }
        if (!callbackRequest.getMdOrder().equals(topayTransaction.getExtTransactionId())) {
            log.error("Sberbank: callback ext transaction id " + callbackRequest.getMdOrder() + " is not equals to ");
            return getErrorResult();
        }
        return new CallbackResult(paymentResult, topayTransaction, "");
    }

    @NotNull
    private CallbackResult getErrorResult() {
        return new CallbackResult(PaymentResult.ERROR, null, "");
    }

    private String generateHMacSHA256(final String key, final String data) throws InvalidKeyException, NoSuchAlgorithmException {
        final Mac hMacSHA256 = Mac.getInstance("HmacSHA256");
        byte[] hmacKeyBytes = key.getBytes(StandardCharsets.UTF_8);
        final SecretKeySpec secretKey = new SecretKeySpec(hmacKeyBytes, "HmacSHA256");
        hMacSHA256.init(secretKey);
        byte[] dataBytes = data.getBytes(StandardCharsets.UTF_8);
        byte[] res = hMacSHA256.doFinal(dataBytes);
        return new String(Hex.encodeHex(res));
    }
}

package ru.planeta.payment.processors.invoice;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;

import ru.planeta.api.exceptions.NotFoundException;
import ru.planeta.api.exceptions.PaymentException;
import ru.planeta.dao.commondb.BillsFor1cDAO;
import ru.planeta.model.common.BillsFor1c;
import ru.planeta.model.common.TopayTransaction;
import ru.planeta.model.enums.TopayTransactionStatus;
import ru.planeta.payment.processors.AbstractPaymentProcessor;
import ru.planeta.payment.processors.PaymentResult;

import javax.annotation.Nonnull;

public class InvoicePaymentProcessor extends AbstractPaymentProcessor {
    
    @Autowired
    private BillsFor1cDAO billsFor1CDAO;

    @Override
    public String prepareAndGetRedirectUrl(@Nonnull TopayTransaction transaction) throws PaymentException, NotFoundException {
        BillsFor1c bill = new BillsFor1c(transaction.getTransactionId(), transaction.getTimeAdded());
        bill.setAmount(transaction.getAmountNet());
        bill.setStatus(transaction.getStatus());
        billsFor1CDAO.insert(bill);
        return getReturnUrl();
    }

    @Override
    public PaymentResult validate(@Nonnull TopayTransaction transaction, Map<String, String> params) throws PaymentException {
        PaymentResult result;
        if(params == null) {
            try {
                BillsFor1c bill = billsFor1CDAO.select(transaction.getTransactionId());
                if(bill == null) {
                    throw new PaymentException("Bill for transaction #" + transaction.getTransactionId() + " not found!!");
                }
                if(bill.getTimePurchased() != null) {
                    result = PaymentResult.COMPLETED;
                    bill.setStatus(TopayTransactionStatus.DONE);
                    billsFor1CDAO.update(bill);
                } else {
                    result = PaymentResult.WAITING;
                }
                log.info("Current bill status: " + (bill.getStatus() == null?"null":bill.getStatus().toString()));
            }
            catch(Exception e) {
                log.error(e);
                result = PaymentResult.ERROR;                
            }
        } else {
            result = PaymentResult.WAITING;
        }
        return result;
    }

    @Override
    public boolean cancel(@Nonnull TopayTransaction transaction) {
        // TODO Auto-generated method stub
        return false;
    }

}

package ru.planeta.payment.processors;

import ru.planeta.api.Utils;
import ru.planeta.model.common.TopayTransaction;

import javax.annotation.Nonnull;
import java.util.Map;

import static ru.planeta.api.Utils.map;

/**
 * Created with IntelliJ IDEA.
 * User: a.volkov
 * Date: 15.05.14
 * Time: 15:41
 */
public class CallbackResult {

    private final TopayTransaction transaction;
    private final String content;
    private final Map<String, String> headers;
    private final PaymentResult paymentResult;

    public CallbackResult(@Nonnull PaymentResult paymentResult, TopayTransaction transaction, String content, String... headers) {
        this.transaction = transaction;
        this.content = content;
        this.headers = Utils.map(headers);
        this.paymentResult = paymentResult;
    }

    public CallbackResult(@Nonnull PaymentResult paymentResult, TopayTransaction transaction, String content, Map<String, String> headers) {
        this.transaction = transaction;
        this.content = content;
        this.headers = headers;
        this.paymentResult = paymentResult;
    }

    public TopayTransaction getTransaction() {
        return transaction;
    }

    public String getContent() {
        return content;
    }

    public Map<String, String> getHeaders() {
        return headers;
    }

    public PaymentResult getPaymentResult() {
        return paymentResult;
    }
}

package ru.planeta.payment.processors.planeta;

import ru.planeta.api.exceptions.NotFoundException;
import ru.planeta.api.exceptions.PaymentException;
import ru.planeta.model.common.Order;
import ru.planeta.model.common.PaymentMethod;
import ru.planeta.model.common.TopayTransaction;
import ru.planeta.payment.processors.AbstractPaymentProcessor;
import ru.planeta.payment.processors.PaymentResult;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.math.BigDecimal;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: a.volkov
 * Date: 20.05.14
 * Time: 17:31
 */
public class PlanetaPaymentProcessor extends AbstractPaymentProcessor {

    private String redirect(@Nonnull TopayTransaction transaction) throws PaymentException {
        PaymentMethod paymentMethod = paymentMethodDAO.select(transaction.getPaymentMethodId());
        //мы не должны пополнять баланс при оплате со счёта планеты, если это не промо
        // remove this logic to PaymentServiceImpl:220 and not set zero here
        if (!paymentMethod.isPromo()) {
            transaction.setAmountNet(BigDecimal.ZERO);
        }
        return getReturnUrl();
    }

    @Override
    public String prepareAndGetRedirectUrl(@Nonnull TopayTransaction transaction) throws PaymentException, NotFoundException {
        return redirect(transaction);
    }

    @Override
    public PaymentResult validate(@Nonnull TopayTransaction transaction, @Nullable Map<String, String> params) throws PaymentException {
        PaymentResult result = PaymentResult.COMPLETED;
        Order order = orderDAO.select(transaction.getOrderId());
        PaymentMethod paymentMethod = paymentMethodDAO.select(transaction.getPaymentMethodId());
        if (order == null) {
            result = paymentMethod.isPromo() ? PaymentResult.COMPLETED : PaymentResult.REJECTED;
        }
        return result;
    }

    @Override
    public boolean cancel(@Nonnull TopayTransaction transaction) {
        return false;
    }


}

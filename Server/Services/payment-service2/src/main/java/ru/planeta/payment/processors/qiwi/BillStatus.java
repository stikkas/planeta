package ru.planeta.payment.processors.qiwi;

/**
 * Created by eshevchenko on 25.03.14.
 */
public enum BillStatus {
    waiting,
    paid,
    rejected,
    unpaid,
    expired
}

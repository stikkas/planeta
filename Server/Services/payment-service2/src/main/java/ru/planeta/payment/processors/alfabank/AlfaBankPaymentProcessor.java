package ru.planeta.payment.processors.alfabank;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.lang3.math.NumberUtils;
import ru.planeta.api.Utils;
import ru.planeta.api.exceptions.NotFoundException;
import ru.planeta.api.exceptions.PaymentException;
import ru.planeta.commons.web.WebUtils;
import ru.planeta.model.common.TopayTransaction;
import ru.planeta.payment.processors.AbstractPaymentProcessor;
import ru.planeta.payment.processors.PaymentResult;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.Map;

import static ru.planeta.api.Utils.empty;

/**
 *
 * Created by alexa_000 on 14.05.2014.
 */
public class AlfaBankPaymentProcessor extends AbstractPaymentProcessor {

    private interface Params {
        String LOGIN = "userName";
        String PASSWORD = "password";
        String TRANSACTION_ID = "orderNumber";
        String AMOUNT = "amount";
        String CURRENCY = "currency";
        String RETURN_URL = "returnUrl";
        String LANGUAGE = "language";
        String PAYMENT_ID = "orderId";
        String FORM_URL = "formUrl";
        String ERROR_CODE = "errorCode";
        String ERROR_MESSAGE = "errorMessage";
    }

    private enum Service {
        REGISTER("register.do"),
        REVERSE("reverse.do"),
        STATUS("getOrderStatusExtended.do");

        public final String uri;

        Service(String uri) {
            this.uri = uri;
        }
    }

    private static final String LANG = "ru";
    private static final Integer CURRENCY = 810;
    private static final BigDecimal HUNDRED = new BigDecimal(100);


    private String url;
    private String login;
    private String password;

    private final ObjectMapper mapper;

    public AlfaBankPaymentProcessor() {
        mapper = new ObjectMapper();
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String prepareAndGetRedirectUrl(@Nonnull TopayTransaction transaction) throws PaymentException, NotFoundException {
        return prepare(transaction);
    }

    private String prepare(@Nonnull TopayTransaction transaction) throws PaymentException {
        WebUtils.Parameters params = new WebUtils.Parameters()
                .add(Params.LOGIN, login)
                .add(Params.PASSWORD, password)
                .add(Params.TRANSACTION_ID, transaction.getTransactionId())
                .add(Params.AMOUNT, transaction.getAmountNet().multiply(HUNDRED).intValue())
                .add(Params.CURRENCY, CURRENCY)
                .add(Params.LANGUAGE, LANG)
                .add(Params.RETURN_URL, getReturnUrl());
        try {
            String response = WebUtils.downloadString(params.createUrl(getServiceUrl(Service.REGISTER), true));
            log.info("Alfa Bank payment register response: " + response);
            if (Utils.empty(response)) {
                throw new PaymentException("have no response from Alfa Bank");
            }
            Map<String, String> data = mapper.readValue(response, new TypeReference<Map<String, String>>() {});
            int errorCode = data.containsKey(Params.ERROR_CODE) ? NumberUtils.toInt(data.get(Params.ERROR_CODE)) : 0;
            if (errorCode != 0) {
                transaction.setExtErrorCode(Integer.parseInt(data.get(Params.ERROR_CODE)));
                transaction.setExtErrorMessage(data.get(Params.ERROR_MESSAGE));
                throw new PaymentException("got error response from Alfa Bank: " + data);
            } else {
                transaction.setExtTransactionId(data.get(Params.PAYMENT_ID));
                return data.get(Params.FORM_URL);
            }
        } catch (IOException e) {
            log.error("error while register payment to Alfa Bank", e);
            throw new PaymentException(e);
        }
    }

    @Override
    public PaymentResult validate(@Nonnull TopayTransaction transaction, @Nullable Map<String, String> requestParams) throws PaymentException {
        WebUtils.Parameters params = new WebUtils.Parameters();
        params.add(Params.LOGIN, login)
                .add(Params.PASSWORD, password)
                .add(Params.PAYMENT_ID, transaction.getExtTransactionId())
                .add(Params.LANGUAGE, LANG);
        String url = params.createUrl(getServiceUrl(Service.STATUS), true);
        PaymentResult result;
        try {
            String response = WebUtils.downloadString(url);
            log.info("Alfa Bank status response: " + response);
            if (Utils.empty(response)) {
                return PaymentResult.ERROR;
            }
            StatusResponse statusResponse = mapper.readValue(response, StatusResponse.class);
            Integer orderStatus = statusResponse.getOrderStatus();
            String transactionId = statusResponse.getOrderNumber();

            log.info("Alfa Bank status code: " + orderStatus + "; transactionId: " + transactionId);
            if (orderStatus == null || Utils.empty(transactionId)) {
                return PaymentResult.ERROR;
            }

            switch (orderStatus) {
                case 2:
                    result = PaymentResult.COMPLETED;
                    break;
                default:
                    transaction.setExtErrorCode(statusResponse.getActionCode());
                    transaction.setExtErrorMessage(statusResponse.getActionDescription());
                    result = PaymentResult.REJECTED;
            }
        } catch (Exception e) {
            log.error("error while getting payment status from Alfa Bank", e);
            result = PaymentResult.ERROR;
        }
        return result;
    }
    
    @Override
    public boolean canValidate() {
        return true;
    }

    @Override
    public boolean cancel(@Nonnull TopayTransaction transaction) {
        WebUtils.Parameters params = new WebUtils.Parameters()
                .add(Params.LOGIN, login)
                .add(Params.PASSWORD, password)
                .add(Params.PAYMENT_ID, transaction.getExtTransactionId());
        boolean result;
        try {
            String response = WebUtils.downloadString(params.createUrl(getServiceUrl(Service.REVERSE), true));
            log.info("Alfa Bank reverse response: " + response);
            if (Utils.empty(response)) {
                throw new PaymentException("have no response from Alfa Bank");
            }
            ReverseResponse reverseResponse = mapper.readValue(response, ReverseResponse.class);
            result = reverseResponse.getErrorCode() == null || reverseResponse.getErrorCode() == 0;
        } catch (Exception e) {
            log.error("error occur while canceling payment", e);
            result = false;
        }
        return result;
    }

    private String getServiceUrl(Service service) {
        return url.concat(service.uri);
    }
}

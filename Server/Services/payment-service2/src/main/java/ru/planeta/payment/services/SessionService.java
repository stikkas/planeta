package ru.planeta.payment.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.planeta.dao.SessionDAO;

/**
 * User: a.volkov
 * Date: 06.05.14
 * Time: 12:49
 */
@Service
public class SessionService {

    private static final String TRANSACTION_ID = "transactionId";

    @Autowired
    private SessionDAO dao;

    public void setTransactionId(Long transactionId) {
        dao.put(TRANSACTION_ID, transactionId);
    }

    public Long getTransactionId() {
        return (Long) dao.get(TRANSACTION_ID);
    }
}

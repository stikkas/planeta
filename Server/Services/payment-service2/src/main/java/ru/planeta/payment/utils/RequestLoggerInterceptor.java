package ru.planeta.payment.utils;

import org.apache.log4j.Logger;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Enumeration;

/**
 * Created with IntelliJ IDEA.
 * User: a.volkov
 * Date: 13.05.14
 * Time: 17:55
 */
public class RequestLoggerInterceptor extends HandlerInterceptorAdapter {

    private static final Logger log = Logger.getLogger(RequestLoggerInterceptor.class);

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        StringBuilder sb = new StringBuilder();
        try {
            sb.append(request.getMethod()).append(" ").append(request.getRequestURI());

            sb.append("\nparams:");
            Enumeration<String> parameterNames = request.getParameterNames();
            while (parameterNames.hasMoreElements()) {
                String parameterName = parameterNames.nextElement();
                request.getParameter(parameterName);

                sb.append("\n\t").append(parameterName).append("=").append(request.getParameter(parameterName)).append("; ");
            }

            Cookie[] cookies = request.getCookies();
            sb.append("\ncookies:");
            if (cookies != null) {
                for (Cookie c : cookies) {
                    sb.append("\n\t").append(c.getName()).append("=").append(c.getValue()).append("; ");
                }
            }
        } catch (Exception e) {
            log.error("Request logging error: ", e);
        } finally {
            log.info(sb.toString());
        }

        return super.preHandle(request, response, handler);
    }
}

package ru.planeta.payment.processors;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import ru.planeta.api.aspect.logging.DBLoggerTypeInfoHolder;
import ru.planeta.api.exceptions.PaymentException;
import ru.planeta.api.log.DBLogger;
import ru.planeta.api.log.LoggerListProxy;
import ru.planeta.api.log.service.DBLogService;
import ru.planeta.api.helper.ProjectService;
import ru.planeta.api.service.profile.AuthorizationService;
import ru.planeta.api.service.profile.ProfileService;
import ru.planeta.commons.web.WebUtils;
import ru.planeta.dao.commondb.OrderDAO;
import ru.planeta.dao.commondb.TopayTransactionDAO;
import ru.planeta.dao.payment.PaymentMethodDAO;
import ru.planeta.dao.payment.PaymentProviderDAO;
import ru.planeta.model.common.PaymentProvider;
import ru.planeta.model.common.TopayTransaction;
import ru.planeta.model.enums.ProjectType;
import ru.planeta.model.profile.Profile;
import ru.planeta.model.stat.log.LoggerType;
import ru.planeta.payment.controllers.Urls;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.servlet.http.HttpServletRequest;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.Map;

/**
 *
 * Created by alexa_000 on 14.05.2014.
 */
public abstract class AbstractPaymentProcessor implements PaymentProcessor {

    @Value("${payment.comment.tmpl}")
    private String commentTmpl;

    @Autowired
    protected TopayTransactionDAO transactionDAO;
    @Autowired
    protected ProjectService projectService;
    @Autowired
    protected ProfileService profileService;
    @Autowired
    protected PaymentMethodDAO paymentMethodDAO;
    @Autowired
    protected PaymentProviderDAO paymentProviderDAO;
    @Autowired
    protected OrderDAO orderDAO;
    @Autowired
    protected DBLogService logService;

    @Autowired
    private AuthorizationService authorizationService;

    protected AuthorizationService getAuthorizationService() {
        return authorizationService;
    }


    protected Logger log = Logger.getLogger(this.getClass());

    protected String getUrl(@Nonnull String uri) {
        return projectService.getUrl(ProjectType.PAYMENT_GATE, uri);
    }

    public void init() {
        this.log = new LoggerListProxy("paymentProcessor", log, DBLogger.Companion.getLogger(LoggerType.PAYMENT, logService));
    }

    // use getReturnUrl(long transactionId)
    @Deprecated
    @Nonnull
    protected final String getReturnUrl() {
        return projectService.getUrl(ProjectType.PAYMENT_GATE, Urls.PAYMENT_DONE);
    }

    protected final String getReturnUrl(long transactionId) {
        WebUtils.Parameters params = new WebUtils.Parameters();
        params.add("transactionid", transactionId);
        return projectService.getUrl(ProjectType.PAYMENT_GATE, Urls.PAYMENT_DONE, params);
    }

    protected final String getReturnUrl(WebUtils.Parameters params) {
        return projectService.getUrl(ProjectType.PAYMENT_GATE, Urls.PAYMENT_DONE, params);
    }

    @Nullable
    protected final TopayTransaction getTransaction(long transactionId) {
        return transactionDAO.select(transactionId);
    }

    @Nonnull
    protected TopayTransaction getTransactionSafe(long transactionId) throws PaymentException {
        TopayTransaction transaction;
        transaction = getTransaction(transactionId);
        if (transaction == null) {
            throw new PaymentException("transaction #" + transactionId + " not found");
        }
        return transaction;
    }

    protected final TopayTransaction getTransactionByExtId(PaymentProvider.Type providerType, String externalPaymentId, boolean isRecurrent) {
        PaymentProvider provider = paymentProviderDAO.select(providerType);
        return transactionDAO.selectPaymentByExternalId(provider.getId(), externalPaymentId, isRecurrent);
    }

    @Override
    public CallbackResult handleCheckCallback(Map<String, String> params, InputStream content) throws PaymentException {
        throw new UnsupportedOperationException();
    }

    public static void enableCallbackDBLogging(@Nullable TopayTransaction payment) {
        if (payment != null) {
            DBLoggerTypeInfoHolder.setLoggerTypeInfo(LoggerType.PAYMENT, payment.getTransactionId());
        }
    }

    @Override
    public CallbackResult handleCallback(Map<String, String> params, HttpServletRequest request) throws PaymentException {
        return null;
    }

    @Nullable
    @Override
    public CallbackResult handleStreamCallback(HttpServletRequest request) throws PaymentException {
        return null;
    }
    
    @Override
    public boolean canValidate() {
        return false;
    }

    protected String getDescriptionMessage(TopayTransaction transaction, Profile profile) {
        return String.format(commentTmpl, clearName(profile.getDisplayName()), transaction.getAmountNet().setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue());
    }

    private static String clearName(String name) {
        StringBuilder sb = new StringBuilder();
        int s = 0;
        char c;
        for (int i = 0; i < name.length(); ++i) {
            c = name.charAt(i);
            switch (s) {
                case 0:
                    if (Character.isWhitespace(c)) {
                        s = 1;
                        sb.append(c);
                    } else if (Character.isAlphabetic(c)) {
                        sb.append(c);
                    }
                    break;
                case 1:
                    if (Character.isAlphabetic(c)) {
                        s = 0;
                        sb.append(c);
                    }
                    break;
            }
        }
        return sb.toString();
    }

}

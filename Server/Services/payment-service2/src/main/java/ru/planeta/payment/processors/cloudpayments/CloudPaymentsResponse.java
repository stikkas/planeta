package ru.planeta.payment.processors.cloudpayments;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.math.BigDecimal;

/**
 * Created by kostiagn on 11.11.2015.
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class CloudPaymentsResponse {

    @JsonProperty("Model")
    private Model model;
    @JsonProperty("Message")
    private String message;
    @JsonProperty("Success")
    private boolean success;

    public Model getModel() {
        return model;
    }

    public String getMessage() {
        return message;
    }

    public boolean isSuccess() {
        return success;
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class Model{
        @JsonProperty("PublicId")
        private String publicId;
        @JsonProperty("TransactionId")
        private String extTransactionId;
        @JsonProperty("InvoiceId")
        private long topayTransactionId;
        @JsonProperty("Amount")
        private BigDecimal amount;
        @JsonProperty("PaymentAmount")
        private BigDecimal paymentAmount;
        @JsonProperty("Email")
        private String userEmail;
        @JsonProperty("Status")
        private String status;

        public String getPublicId() {
            return publicId;
        }

        public String getExtTransactionId() {
            return extTransactionId;
        }

        public long getTopayTransactionId() {
            return topayTransactionId;
        }

        public BigDecimal getAmount() {
            return amount;
        }

        public BigDecimal getPaymentAmount() {
            return paymentAmount;
        }

        public String getUserEmail() {
            return userEmail;
        }

        public String getStatus() {
            return status;
        }
    }



}

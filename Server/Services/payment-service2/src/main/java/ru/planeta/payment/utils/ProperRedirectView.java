package ru.planeta.payment.utils;

import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.view.RedirectView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * User: a.volkov
 * Date: 13.05.14
 * Time: 12:51
 */
public class ProperRedirectView extends RedirectView {

    public ProperRedirectView(String url) {
        super(url);
    }

    /**
     * Copied from @link{RedirectView#sendRedirect} and removed calls to reponse.encodeRedirectURL() for
     * absolute target urls
     */
    @Override
    protected void sendRedirect(HttpServletRequest request, HttpServletResponse response, String targetUrl, boolean http10Compatible) throws IOException {

        boolean isAbsoluteUrl = StringUtils.startsWith(targetUrl, "http://") || StringUtils.startsWith(targetUrl, "https://");
        String encodedRedirectURL = isAbsoluteUrl ? targetUrl : response.encodeRedirectURL(targetUrl);

        if (http10Compatible) {
            // Always send status code 302.
            response.sendRedirect(encodedRedirectURL);
        } else {
            HttpStatus statusCode = getHttp11StatusCode(request, response, targetUrl);
            response.setStatus(statusCode.value());
            response.setHeader("Location", encodedRedirectURL);
        }
    }
}
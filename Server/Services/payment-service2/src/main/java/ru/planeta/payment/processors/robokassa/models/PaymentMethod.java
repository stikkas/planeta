package ru.planeta.payment.processors.robokassa.models;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * Created by eshevchenko on 11.08.14.
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class PaymentMethod {

    @XmlElement(name = "Code")
    private String code;
    @XmlElement(name = "Description")
    private String description;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}

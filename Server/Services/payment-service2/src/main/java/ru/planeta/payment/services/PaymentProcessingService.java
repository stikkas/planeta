package ru.planeta.payment.services;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.planeta.api.aspect.transaction.NonTransactional;
import ru.planeta.api.exceptions.BaseException;
import ru.planeta.api.exceptions.NotFoundException;
import ru.planeta.api.exceptions.OrderException;
import ru.planeta.api.exceptions.PermissionException;
import ru.planeta.api.log.DBLogger;
import ru.planeta.api.log.LoggerListProxy;
import ru.planeta.api.log.service.DBLogService;
import ru.planeta.api.mail.MailClient;
import ru.planeta.api.service.billing.order.OrderService;
import ru.planeta.api.service.billing.payment.PaymentService;
import ru.planeta.api.service.profile.AuthorizationService;
import ru.planeta.api.service.profile.ProfileService;
import ru.planeta.model.common.Order;
import ru.planeta.model.common.TopayTransaction;
import ru.planeta.model.enums.TopayTransactionStatus;
import ru.planeta.model.profile.Profile;
import ru.planeta.model.stat.log.LoggerType;

import javax.annotation.Nonnull;
import java.util.EnumSet;

/**
 * Payment processing service implementation.<br>
 * User: eshevchenko
 */
@Service
public class PaymentProcessingService {

    private final ProfileService profileService;
    private final PaymentService paymentService;
    private final OrderService orderService;
    private final AuthorizationService authorizationService;
    private final MailClient mailClient;

    private final Logger log;

    @Autowired
    public PaymentProcessingService(ProfileService profileService, PaymentService paymentService, DBLogService dbLogService, OrderService orderService, AuthorizationService authorizationService, MailClient mailClient) {
        this.profileService = profileService;
        this.paymentService = paymentService;
        this.orderService = orderService;
        this.authorizationService = authorizationService;
        this.mailClient = mailClient;
        log = new LoggerListProxy("paymentProcessingServiceLogger", Logger.getLogger(PaymentProcessingService.class), DBLogger.Companion.getLogger(LoggerType.PAYMENT, dbLogService));
    }


    @NonTransactional
    public boolean process(@Nonnull TopayTransaction payment) {
        try {
            boolean result = paymentService.processPayment(payment, payment.getComment());
            if (!result) {
                return false;
            }
            if (payment.getOrderId() == 0) {
                sendBalanceCreditedNotification(payment);
            } else {
                purchaseOrder(payment.getOrderId());
            }
            log.info("transaction #" + payment.getTransactionId() + " processed successfully with status " + payment.getStatus());
            return true;
        } catch (Exception e) {
            log.error("transaction #" + payment.getTransactionId() + " processing failed", e);
        }

        return false;
    }

    @NonTransactional
    public void purchaseOrder(long orderId) throws NotFoundException {
        try {
            orderService.purchase(orderId);
        } catch (BaseException e) {
            orderService.markOrderAsFailed(orderId, e);
            log.error("Order purchasing error: " + orderId + " ", e);
        }
    }


    public void purchaseOrderNotSafe(long orderId) throws NotFoundException, OrderException, PermissionException {
        final Order order = orderService.getOrderSafe(orderId);
        orderService.purchase(order);
    }

    private void sendBalanceCreditedNotification(TopayTransaction transaction) throws NotFoundException {
        final long profileId = transaction.getProfileId();
        final Profile profile = profileService.getProfileSafe(profileId);
        final String email = authorizationService.getUserPrivateEmailById(profileId);
        mailClient.sendBalanceCreditedNotificationEmail(email, profile.getDisplayName(), transaction.getAmountNet(), profile.getUserGender());
    }
}

package ru.planeta.payment.processors.wmoney;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.math.NumberUtils;
import ru.planeta.api.Utils;
import ru.planeta.api.exceptions.NotFoundException;
import ru.planeta.api.exceptions.PaymentException;
import ru.planeta.api.service.billing.payment.system.webmoney.rest.models.StateResponse;
import ru.planeta.commons.web.WebUtils;
import ru.planeta.model.common.TopayTransaction;
import ru.planeta.model.profile.Profile;
import ru.planeta.payment.processors.AbstractPaymentProcessor;
import ru.planeta.payment.processors.CallbackResult;
import ru.planeta.payment.processors.PaymentResult;

import javax.annotation.Nonnull;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.Map;

import static ru.planeta.api.Utils.empty;

public class WebMoneyPayPaymentProcessor extends AbstractPaymentProcessor {

    private interface Params {

        String LMI_PAID_AMOUNT = "LMI_PAID_AMOUNT";
        String LMI_PAID_CURRENCY = "LMI_PAID_CURRENCY";
        String LMI_MERCHANT_ID = "LMI_MERCHANT_ID";
        String LMI_PAYMENT_AMOUNT = "LMI_PAYMENT_AMOUNT";
        String LMI_CURRENCY = "LMI_CURRENCY";
        String LMI_PAYMENT_NO = "LMI_PAYMENT_NO";
        String LMI_PAYMENT_DESC = "LMI_PAYMENT_DESC";
        String JSON = "json";
        String LMI_HASH = "LMI_HASH";
        String LMI_PAYMENT_METHOD = "LMI_PAYMENT_METHOD";
        String LMI_PAYER_EMAIL = "LMI_PAYER_EMAIL";
        String LMI_SYS_PAYMENT_ID = "LMI_SYS_PAYMENT_ID";
        String LMI_SYS_PAYMENT_DATE = "LMI_SYS_PAYMENT_DATE";
        String LMI_PAYMENT_SYSTEM = "LMI_PAYMENT_SYSTEM";
        String LMI_SIM_MODE = "LMI_SIM_MODE";
    }

    private static final int PAYMENT_NOT_FOUND = -13;
    private static final int CONN_TIMEOUT = 30 * 1000;

    private interface Service {

        String INVOICE = "/Payment/Init";
        String PAYMENT_BY_INVOICE = "/partners/rest/getPaymentByInvoiceID";
    }

    private String merchantId;
    private String currency;
    private String url;
    private String secretWord = "";
    private String login;
    private String password;

    @Override
    public String prepareAndGetRedirectUrl(@Nonnull TopayTransaction transaction) throws PaymentException, NotFoundException {
        Profile profile = profileService.getProfileSafe(transaction.getProfileId());
        String email = getAuthorizationService().getUserPrivateEmailById(transaction.getProfileId());
        if (email == null) {
            throw new PaymentException("no email");
        }

        WebUtils.Parameters params = new WebUtils.Parameters()
                .add(Params.LMI_MERCHANT_ID, merchantId)
                .add(Params.LMI_PAYMENT_AMOUNT, transaction.getAmountNet().toPlainString())
                .add(Params.LMI_CURRENCY, currency)
                .add(Params.LMI_PAYMENT_NO, transaction.getTransactionId())
                .add(Params.LMI_PAYMENT_DESC, getDescriptionMessage(transaction, profile))
                .add(Params.LMI_PAYER_EMAIL, email);

        String redirectUrl = params.createUrl(url + Service.INVOICE, false);
        log.info("register paymaster invoice: " + redirectUrl);
        return redirectUrl;
    }

    @Override
    public PaymentResult validate(@Nonnull TopayTransaction transaction, Map<String, String> params) throws PaymentException {
        if (null == params) {
            try {
                log.info("Try process webmoney transaction " + transaction.getTransactionId());
                String nonce = String.valueOf(Math.random() * 100000);
                String invoiceId = String.valueOf(transaction.getTransactionId());
                String hash = Base64.encodeBase64String(DigestUtils.sha(login + ";" + password + ";" + nonce + ";" + invoiceId + ";" + merchantId));
                WebUtils.Parameters prms = new WebUtils.Parameters()
                        .add("login", login)
                        .add("nonce", nonce)
                        .add("invoiceid", invoiceId)
                        .add("siteAlias", merchantId)
                        .add("hash", hash);
                StateResponse response = new ObjectMapper().readValue(WebUtils.downloadStringWithTimeout(prms.createUrl(url + Service.PAYMENT_BY_INVOICE, false), CONN_TIMEOUT), StateResponse.class);
                int errorCode = response.getErrorCode();
                if (errorCode == 0) {
                    switch (response.getPayment().getState()) {
                        case "INITIATED":
                        case "PROCESSING":
                            return PaymentResult.WAITING;
                        case "CANCELLED":
                            return PaymentResult.REJECTED;
                        case "COMPLETE":
                            return PaymentResult.COMPLETED;
                    }
                } else {
                    if (errorCode == PAYMENT_NOT_FOUND) {
                        return PaymentResult.REJECTED;
                    }
                    log.error("WebMoneyPayPayment service return ErrorCode: " + errorCode);
                }
                log.info("finish process webmoney transaction " + transaction.getTransactionId());
            } catch (IOException ex) {
                log.error("Error with getting status of transaction by transactionId: " + ex.getMessage() + " " + transaction.getTransactionId(), ex);
            }
            return PaymentResult.ERROR;
        }
        log.debug(params);
        final PaymentResult result;
        if (!Utils.empty(params.get(Params.LMI_SYS_PAYMENT_ID))) {
            // Если ID внешней транзакции нам передали значит платёж прошёл и нам возможно уже выдали callback
            long payTransId = NumberUtils.toLong(params.get(Params.LMI_PAYMENT_NO));
            if (transaction.getTransactionId() != payTransId) {
                log.error("TransactionId from paymaster not equal our transactionId " + transaction.getTransactionId() + " " + payTransId);
                throw new PaymentException("TransactionId from paymaster not equal our transactionId. " + transaction.getTransactionId());
            }
            if (!Utils.empty(transaction.getExtTransactionId())) {
                if (!transaction.getExtTransactionId().equals(params.get(Params.LMI_SYS_PAYMENT_ID))) {
                    // Параметры запроса не совпадают с тем что посчитали мы сами, значит произошла какая-то байда
                    log.error("TransactionId from paymaster not equal our transactionId!!!" + transaction.getExtTransactionId() + " " + params.get(Params.LMI_SYS_PAYMENT_ID));
                    throw new PaymentException("External TransactionId from paymaster not equal saved external transactionId.");
                }
            }
            switch (transaction.getStatus()) {
                case DONE:
                    result = PaymentResult.COMPLETED;
                    break;
                default:
                    result = PaymentResult.WAITING;
            }
        } else {
            log.warn("Paymaster return user from FailURL, topaytransactionId = " + transaction.getTransactionId());
            result = PaymentResult.REJECTED;
        }
        return result;
    }

    @Override
    public boolean cancel(@Nonnull TopayTransaction transaction) {
        throw new UnsupportedOperationException();
    }

    @Override
    public CallbackResult handleCallback(Map<String, String> params, HttpServletRequest request) {
        log.info(params);
        PaymentResult status = PaymentResult.REJECTED;
        String response = "NO";
        try {
            long payTransId = Long.parseLong(params.get(Params.LMI_PAYMENT_NO));
            TopayTransaction transaction = getTransactionSafe(payTransId);
            if (!checkSign(params)) {
                log.error("Wrong hash from payment system");
            } else if (!params.get(Params.LMI_CURRENCY).equals(currency)) {
                log.error("Currency for payment: " + params.get(Params.LMI_CURRENCY) + ", Currency required: " + currency);
            } else if (!params.get(Params.LMI_PAID_CURRENCY).equals(currency)) {
                log.error("Currency paid: " + params.get(Params.LMI_PAID_CURRENCY) + ", Currency required: " + currency);
            } else if (!Utils.equals(transaction.getAmountNet(), new BigDecimal(params.get(Params.LMI_PAYMENT_AMOUNT)))) {
                log.error("Transaction amount: " + transaction.getAmountNet() + ", Payment amount: " + params.get(Params.LMI_PAYMENT_AMOUNT));
            } else if (!Utils.equals(transaction.getAmountNet(), new BigDecimal(params.get(Params.LMI_PAID_AMOUNT)))) {
                log.error("Transaction amount: " + transaction.getAmountNet() + ", Paid amount: " + params.get(Params.LMI_PAID_AMOUNT));
            } else {
                status = PaymentResult.COMPLETED;
                response = "YES";
                transaction.setExtTransactionId(params.get(Params.LMI_SYS_PAYMENT_ID));
                transactionDAO.update(transaction);
            }
            return new CallbackResult(status, transaction, response);
        } catch (NumberFormatException | PaymentException e) {
            log.error("error occur while handling webmoney callback request", e);
            return new CallbackResult(status, null, response);
        }
    }

    boolean checkSign(Map<String, String> params) {
        String[] keys = {Params.LMI_MERCHANT_ID,
            Params.LMI_PAYMENT_NO,
            Params.LMI_SYS_PAYMENT_ID,
            Params.LMI_SYS_PAYMENT_DATE,
            Params.LMI_PAYMENT_AMOUNT,
            Params.LMI_CURRENCY,
            Params.LMI_PAID_AMOUNT,
            Params.LMI_PAID_CURRENCY,
            Params.LMI_PAYMENT_SYSTEM,
            Params.LMI_SIM_MODE};
        StringBuilder sb = new StringBuilder();
        for (String key : keys) {
            String val = params.get(key);
            if (val == null) {
                val = "";
            }
            sb.append(val);
            sb.append(";");
        }
        sb.append(this.secretWord);
        String s = Base64.encodeBase64String(DigestUtils.md5(sb.toString()));
        String expected = params.get(Params.LMI_HASH);
        if (s.equals(expected)) {
            log.debug("our hash: " + s);
            return true;
        } else {
            log.warn("our hash: " + s);
            log.warn("paymaster hash: " + expected);
            return false;
        }
    }

    public String getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getSecretWord() {
        return secretWord;
    }

    public void setSecretWord(String secretWord) {
        this.secretWord = secretWord;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}

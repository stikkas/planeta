package ru.planeta.payment.processors.chronopay;

import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import ru.planeta.api.Utils;
import ru.planeta.api.exceptions.NotFoundException;
import ru.planeta.api.exceptions.PaymentException;
import ru.planeta.api.service.profile.AuthorizationService;
import ru.planeta.commons.web.WebUtils;
import ru.planeta.model.common.PaymentProvider;
import ru.planeta.model.common.TopayTransaction;
import ru.planeta.payment.controllers.Urls;
import ru.planeta.payment.processors.AbstractPaymentProcessor;
import ru.planeta.payment.processors.CallbackResult;
import ru.planeta.payment.processors.PaymentResult;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Map;

import static ru.planeta.api.Utils.nvl;

/**
 * Created with IntelliJ IDEA.
 * User: a.volkov
 * Date: 19.05.14
 * Time: 12:38
 */
public class ChronopayPaymentProcessor extends AbstractPaymentProcessor {

    private interface Params {
        String PRODUCT_ID = "product_id";
        String PRODUCT_PRICE = "product_price";
        String SIGN = "sign";
        String ORDER_ID = "order_id";
        String CB_URL = "cb_url";
        String CB_TYPE = "cb_type";
        String SUCCESS_URL = "success_url";
        String DECLINE_URL = "decline_url";
        String LANGUAGE = "language";
        String PAYMENT_TYPE_GROUP_ID = "payment_type_group_id";
        String TOTAL = "total";
        String TRANSACTION_ID = "transaction_id";
        String TRANSACTION_TYPE = "transaction_type";
        String CUSTOMER_ID = "customer_id";
        String PHONE = "phone";
        String EMAIL = "email";
    }

    private static final String LANG = "ru";

    private String login;
    private String password;
    private String url;

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }


    private String redirect(@Nonnull TopayTransaction transaction) {
        BigDecimal amount = transaction.getAmountNet().setScale(2, RoundingMode.HALF_UP);
        String email = getAuthorizationService().getUserPrivateEmailById(transaction.getProfileId());
        WebUtils.Parameters params = new WebUtils.Parameters()
                .add(Params.ORDER_ID, transaction.getTransactionId())
                .add(Params.PRODUCT_PRICE, amount)
                .add(Params.PAYMENT_TYPE_GROUP_ID, transaction.getPaymentToolCode())
                .add(Params.PHONE, Utils.nvl(transaction.getParam1(), ""))
                .add(Params.EMAIL, email)
                .add(Params.PRODUCT_ID, login)
                .add(Params.LANGUAGE, LANG)
                .add(Params.CB_URL, getUrl(Urls.CALLBACK.replace("{alias}", PaymentProvider.Type.CHRONOPAY.name().toLowerCase())))
                .add(Params.CB_TYPE, "P")
                .add(Params.SUCCESS_URL, getReturnUrl())
                .add(Params.DECLINE_URL, getReturnUrl());

        return params
                .add(Params.SIGN, sign(params))
                .createUrl(url, true);
    }

    @Override
    public String prepareAndGetRedirectUrl(@Nonnull TopayTransaction transaction) throws PaymentException, NotFoundException {
        return redirect(transaction);
    }

    @Override
    public PaymentResult validate(@Nonnull TopayTransaction transaction, @Nullable Map<String, String> params) throws PaymentException {
        return PaymentResult.NOT_IMPLEMENTED;
    }

    @Override
    public boolean cancel(@Nonnull TopayTransaction transaction) {
        return false;
    }

    @Override
    public CallbackResult handleCallback(Map<String, String> params, HttpServletRequest request) {
        String sign = params.get(Params.SIGN);
        String ourSign = callbackSign(params);
        log.info("chronopay sign: " + sign + "; our sign: " + ourSign);

        if (!sign.equals(ourSign)) {
            return null;
        }

        TopayTransaction transaction = null;
        PaymentResult result;
        try {
            transaction = getTransactionByExtId(PaymentProvider.Type.CHRONOPAY, params.get(Params.TRANSACTION_ID), false);
            if (transaction == null) {
                throw new PaymentException("transaction with ext id " + params.get(Params.TRANSACTION_ID) + " not found");
            }
            BigDecimal amount = new BigDecimal(params.get(Params.TOTAL));
            if (!Utils.equals(amount, transaction.getAmountNet())) {
                throw new PaymentException("amount doesn't match. payment ampunt: " + transaction.getAmountNet() + "; callback amount: " + amount);
            }
            result = PaymentResult.COMPLETED;
        } catch (Exception e) {
            result = PaymentResult.ERROR;
            log.error("chronopay callback handling error", e);
        }

        return new CallbackResult(result, transaction, "");
    }

    private String sign(WebUtils.Parameters params) {
        String result = login + "-" + params.get(Params.PRODUCT_PRICE) + "-" + params.get(Params.ORDER_ID) + "-" + password;
        return DigestUtils.md5Hex(result);
    }

    private String callbackSign(Map<String, String> params) {
        String result = login + params.get(Params.CUSTOMER_ID) + params.get(Params.TRANSACTION_ID) + params.get(Params.TRANSACTION_TYPE) + params.get(Params.TOTAL);
        return DigestUtils.md5Hex(result);
    }
}

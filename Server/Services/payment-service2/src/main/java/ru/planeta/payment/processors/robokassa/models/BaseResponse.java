package ru.planeta.payment.processors.robokassa.models;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

/**
 * Created by eshevchenko on 11.08.14.
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class BaseResponse {
}

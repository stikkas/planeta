package ru.planeta.payment.processors.cloudpayments;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.NameValuePair;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.impl.auth.BasicScheme;
import org.apache.http.impl.client.BasicAuthCache;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.springframework.web.util.ContentCachingRequestWrapper;
import ru.planeta.api.Utils;
import ru.planeta.api.exceptions.NotFoundException;
import ru.planeta.api.exceptions.PaymentException;
import ru.planeta.api.utils.PaymentUtils;
import ru.planeta.commons.web.WebUtils;
import ru.planeta.model.common.TopayTransaction;
import ru.planeta.model.enums.ProjectType;
import ru.planeta.payment.controllers.Urls;
import ru.planeta.payment.processors.AbstractPaymentProcessor;
import ru.planeta.payment.processors.CallbackResult;
import ru.planeta.payment.processors.PaymentResult;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.springframework.http.HttpHeaders.CONTENT_TYPE;
import static ru.planeta.api.Utils.empty;

/**
 *
 * Created by kostiagn on 10.11.2015.
 */
public class CloudPaymentsPaymentProcessor extends AbstractPaymentProcessor {
    private String publicId;
    private String password;
    private static final String CALLBACK_RESPONSE_GOOD = "{\"code\":0}";
    private static final String CALLBACK_RESPONSE_BAD = "{\"code\":10}";

    private static enum ApiFunc {
        CHECK_PAYMENT_STATUS("/payments/find");

        private String url;

        ApiFunc(String url) {
            this.url = url;
        }

        public String getUrl() {
            return url;
        }
    }

    @Override
    public String prepareAndGetRedirectUrl(@Nonnull TopayTransaction transaction) throws PaymentException, NotFoundException {
        WebUtils.Parameters params = new WebUtils.Parameters()
                .add("transactionId", transaction.getTransactionId())
                .add("sign", PaymentUtils.sign(transaction));

        return projectService.getUrl(ProjectType.PAYMENT_GATE, Urls.CLOUD_PAYMENTS, params);
    }


    @Override
    public PaymentResult validate(@Nonnull TopayTransaction transaction, @Nullable Map<String, String> params) throws PaymentException {
        CloudPaymentsResponse response = findPayment(transaction.getTransactionId());

        if (!response.isSuccess()) {
            if ("not found".equalsIgnoreCase(response.getMessage())) {
                if (params == null) {
                    return PaymentResult.ERROR;
                }
                return PaymentResult.REJECTED;
            }
            if (response.getModel() == null) {
                log.error("cloudpayments: error. message = " + toString(response.getMessage()));
                return PaymentResult.ERROR;
            }
            if ("Declined".equalsIgnoreCase(response.getModel().getStatus())) {
                return PaymentResult.REJECTED;

            }
        }
        if (!checkResponse(transaction, response)) {
            return PaymentResult.ERROR;
        }
        if (response.isSuccess() && "Completed".equalsIgnoreCase(response.getModel().getStatus())) {
            if (StringUtils.isNotBlank(response.getModel().getExtTransactionId())) {
                if(Utils.empty(transaction.getExtTransactionId())) {
                    transaction.setExtTransactionId(response.getModel().getExtTransactionId());
                } else {
                    log.warn("Trying change extTransactionId on validate!");
                }
            }
            return PaymentResult.COMPLETED;
        }

        return PaymentResult.ERROR;
    }
    
    @Override
    public boolean canValidate() {
        return true;
    }

    private boolean checkResponse(TopayTransaction transaction, CloudPaymentsResponse response) throws PaymentException {
        final CloudPaymentsResponse.Model model = response.getModel();
        if (model != null) {
            if (model.getTopayTransactionId() != transaction.getTransactionId()
                    || model.getAmount() == null || model.getAmount().compareTo(transaction.getAmountNet()) != 0
                    || model.getPaymentAmount() == null || model.getPaymentAmount().compareTo(transaction.getAmountNet()) != 0
                    ) {
                log.error("cloudpayments: data does not equal. model.topayTransactionId = " + model.getTopayTransactionId()
                                + " topayTransaction.transactionId = " + transaction.getTransactionId()
                                + " model.amount = " + toString(model.getAmount())
                                + " model.paymentAmount = " + toString(model.getPaymentAmount())
                                + "topayTransaction.amountNet = " + toString(transaction.getAmountNet())
                );
                return false;
            }
        }
        return true;
    }

    private String toString(Object o) {
        if (o == null) {
            return "null";
        }
        return o.toString();
    }


    @Override
    public boolean cancel(@Nonnull TopayTransaction transaction) {
        return false;
    }

    @Nullable
    @Override
    public CallbackResult handleCallback(Map<String, String> externalUnusedParams, HttpServletRequest request) throws PaymentException {
        String stringParams;

        if (request instanceof ContentCachingRequestWrapper) {
            ContentCachingRequestWrapper cachingRequestWrapper = (ContentCachingRequestWrapper) request;
            stringParams = new String(cachingRequestWrapper.getContentAsByteArray());
            if (stringParams.isEmpty()) {
                try {
                    stringParams = IOUtils.toString(cachingRequestWrapper.getInputStream());
                } catch (Exception e) {
                    log.error("cloudpayments: error while copy from request.getInputStream");
                    return new CallbackResult(PaymentResult.ERROR, null, CALLBACK_RESPONSE_GOOD);
                }
            }

        } else {
            try {
                stringParams = IOUtils.toString(request.getInputStream());
            } catch (Exception e) {
                log.error("cloudpayments: error while copy from request.getInputStream");
                return new CallbackResult(PaymentResult.ERROR, null, CALLBACK_RESPONSE_GOOD);
            }
        }

        String calculatedHMAC = calcHMAC(stringParams);
        log.info("String params:" + stringParams);
        String sentHMAC = request.getHeader("Content-HMAC");
        if (!StringUtils.equals(calculatedHMAC, sentHMAC)) {
            log.error("cloudpayments: bad HMAC. calculated = " + calculatedHMAC + " sent = " + sentHMAC + " params = " + stringParams);
            return new CallbackResult(PaymentResult.ERROR, null, CALLBACK_RESPONSE_GOOD);
        }

        final Map<String, String> params;
        try {
            params = WebUtils.getQueryParams(stringParams, "utf-8");
        } catch (UnsupportedEncodingException e) {
            throw new PaymentException(e.getMessage());
        }


        PaymentResult paymentResult = "Completed".equalsIgnoreCase(params.get("Status")) ? PaymentResult.COMPLETED : PaymentResult.REJECTED;


        long transactionId = NumberUtils.toLong(params.get("InvoiceId"));
        if (transactionId == 0) {
            log.error("cloudpayments: parameter InvoiceId is not specified");
            return new CallbackResult(PaymentResult.ERROR, null, CALLBACK_RESPONSE_GOOD);
        }
        final TopayTransaction topayTransaction = getTransactionSafe(transactionId);
        if (paymentResult == PaymentResult.COMPLETED) {
            String amountString = params.get("Amount");
            if (amountString == null) {
                log.error("cloudpayments: amount is null " + " topayTransaction.amountNet = " + topayTransaction.getAmountNet());
                return new CallbackResult(PaymentResult.ERROR, topayTransaction, CALLBACK_RESPONSE_BAD);
            }
            BigDecimal amount = new BigDecimal(amountString);
            if (amount.compareTo(topayTransaction.getAmountNet()) != 0) {
                log.error("cloudpayments: not equal amount = " + toString(amount) + " topayTransaction.amountNet = " + topayTransaction.getAmountNet());
                return new CallbackResult(PaymentResult.ERROR, topayTransaction, CALLBACK_RESPONSE_GOOD);
            }
        }

        if (topayTransaction.getExtTransactionId() != null) {
            if(!params.get("TransactionId").equals(topayTransaction.getExtTransactionId())) {
                log.error("cloudpayments: parameter TransactionId not equal saved extTransactionId");
                return new CallbackResult(PaymentResult.ERROR, topayTransaction, CALLBACK_RESPONSE_BAD);
            }
        }

        topayTransaction.setExtTransactionId(params.get("TransactionId"));

        return new CallbackResult(paymentResult, topayTransaction, CALLBACK_RESPONSE_GOOD);
    }

    @Override
    public CallbackResult handleCheckCallback(Map<String, String> params, InputStream content) throws PaymentException {
        log.info("Cloud Paymens check request: \n" + params);
        
        long transactionId = NumberUtils.toLong(params.get("InvoiceId"));
        if (transactionId == 0) {
            log.error("cloudpayments: parameter InvoiceId is not specified");
            return new CallbackResult(PaymentResult.ERROR, null, CALLBACK_RESPONSE_GOOD);
        }
        final TopayTransaction topayTransaction = getTransactionSafe(transactionId);

        if (topayTransaction.getExtTransactionId() != null) {
            if(!params.get("TransactionId").equals(topayTransaction.getExtTransactionId())) {
                log.error("cloudpayments: parameter TransactionId not equal saved extTransactionId");
                return new CallbackResult(PaymentResult.ERROR, topayTransaction, CALLBACK_RESPONSE_BAD);
            }
        }

        topayTransaction.setExtTransactionId(params.get("TransactionId"));
        transactionDAO.update(topayTransaction);
        return new CallbackResult(PaymentResult.WAITING, topayTransaction, CALLBACK_RESPONSE_GOOD);
    }

    private Mac sha256HMAC;

    private String calcHMAC(String message) throws PaymentException {
        if (message == null) {
            return "";
        }
        try {
            if (sha256HMAC == null) {
                sha256HMAC = Mac.getInstance("HmacSHA256");
            }
            SecretKeySpec secret_key = new SecretKeySpec(password.getBytes(), "HmacSHA256");
            sha256HMAC.init(secret_key);

            String hash = Base64.encodeBase64String(sha256HMAC.doFinal(message.getBytes()));
            return hash;
        } catch (Exception e) {
            throw new PaymentException("cloudpayments: cannot calculate HMAC", e);
        }
    }


    @Nonnull
    private CloudPaymentsResponse findPayment(long transactionId) throws PaymentException {
        try {
            final Map<String, Object> map = new HashMap<>();
            map.put("InvoiceId", transactionId);
            String response = apiRequest(ApiFunc.CHECK_PAYMENT_STATUS, map);
            log.info("Find cloudpayments payment: " + response);
            CloudPaymentsResponse result = new ObjectMapper().readValue(response, CloudPaymentsResponse.class);
            if (result == null || StringUtils.isEmpty(response)) {
                throw new PaymentException("cloudpayments: no response");
            }
            return result;
        } catch (IOException e) {
            throw new PaymentException("cloudpayments: error during findPayment parse response. transactionId = " + transactionId, e);
        }
    }

    private String apiRequest(ApiFunc apiFunc, Map<String, Object> params) throws PaymentException {
        try {
            CloseableHttpClient httpclient = HttpClientBuilder.create().build();


            HttpHost targetHost = new HttpHost("api.cloudpayments.ru", -1, "https");
            CredentialsProvider credsProvider = new BasicCredentialsProvider();
            credsProvider.setCredentials(
                    new AuthScope(targetHost.getHostName(), targetHost.getPort()),
                    new UsernamePasswordCredentials(publicId, password));

// Create AuthCache instance
            final BasicAuthCache authCache = new BasicAuthCache();
// Generate BASIC scheme object and add it to the local auth cache
            BasicScheme basicAuth = new BasicScheme();
            authCache.put(targetHost, basicAuth);

// Add AuthCache to the execution context
            HttpClientContext context = HttpClientContext.create();
            context.setCredentialsProvider(credsProvider);
            context.setAuthCache(authCache);

            HttpPost httpPost = new HttpPost(apiFunc.getUrl());
            httpPost.setHeader(CONTENT_TYPE, "application/x-www-form-urlencoded");
            List<NameValuePair> formparams = new ArrayList<>();
            for (String paramName : params.keySet()) {
                formparams.add(new BasicNameValuePair(paramName, params.get(paramName).toString()));
            }

            httpPost.setEntity(new UrlEncodedFormEntity(formparams, "UTF-8"));

            try (CloseableHttpResponse response = httpclient.execute(targetHost, httpPost, context)) {
                HttpEntity entity = response.getEntity();
                return IOUtils.toString(entity.getContent());
            }
        } catch (IOException e) {
            throw new PaymentException("cloudpayments: error during query to https://api.cloudpayments.ru" + apiFunc.getUrl(), e);
        }

    }

    public String getPublicId() {
        return publicId;
    }

    public void setPublicId(String publicId) {
        this.publicId = publicId;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }


}

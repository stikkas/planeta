package ru.planeta.payment.processors.qiwi;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.apache.http.Consts;
import org.apache.http.client.HttpResponseException;
import org.apache.http.client.fluent.Request;
import ru.planeta.api.Utils;
import ru.planeta.api.exceptions.NotFoundException;
import ru.planeta.api.exceptions.PaymentException;
import ru.planeta.commons.web.WebUtils;
import ru.planeta.model.common.PaymentMethod;
import ru.planeta.model.common.TopayTransaction;
import ru.planeta.model.enums.TopayTransactionStatus;
import ru.planeta.model.profile.Profile;
import ru.planeta.payment.processors.AbstractPaymentProcessor;
import ru.planeta.payment.processors.CallbackResult;
import ru.planeta.payment.processors.PaymentResult;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import static org.apache.commons.lang3.ObjectUtils.min;
import static org.springframework.http.HttpHeaders.CONTENT_TYPE;

/**
 * Created with IntelliJ IDEA.
 * User: a.volkov
 * Date: 15.05.14
 * Time: 12:35
 */

public class QiwiPaymentProcessor extends AbstractPaymentProcessor {

    private static final String RESPONSE_CONTENT_TYPE = "text/xml";
    private static final int RETRIES_COUNT = 3;

    private interface Params {
        String USER = "user";
        String AMOUNT = "amount";
        String CCY = "ccy";
        String COMMENT = "comment";
        String LIFETIME = "lifetime";
        String PAY_SOURCE = "pay_source";
        String PRV_NAME = "prv_name";

        String BILL_ID = "bill_id";
        String STATUS = "status";
    }

    private interface Headers {
        String ACCEPT = "Accept";
        String AUTHORIZATION = "Authorization";
    }

    private static final String BILL_URL = "https://api.qiwi.com/api/v2/prv/%s/bills/%d";
    private static final String REDIRECT_URL = "https://qiwi.com/order/external/main.action";

    private static final String PRV_NAME = "Planeta.ru";
    private static final String CURRENCY = "RUB";

    private static final String CALLBACK_RESPONSE = "<?xml version=\"1.0\"?><result><result_code>0</result_code></result>";

    private String login;
    private String password;
    private int billLifeTime;
    private int connectionTimeout;
    private String authorization;
    private final ObjectMapper mapper;

    public QiwiPaymentProcessor() {
        mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, true);
        mapper.configure(DeserializationFeature.UNWRAP_ROOT_VALUE, true);
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getBillLifeTime() {
        return billLifeTime;
    }

    public void setBillLifeTime(int billLifeTime) {
        this.billLifeTime = billLifeTime;
    }

    public int getConnectionTimeout() {
        return connectionTimeout;
    }

    public void setConnectionTimeout(int connectionTimeout) {
        this.connectionTimeout = connectionTimeout;
    }

    @Override
    public void init() {
        super.init();
        authorization = "Basic " + Base64.encodeBase64String((login + ":" + password).getBytes());
    }


    private static String preparePhone(String phone){
        if (StringUtils.isNotEmpty(phone)) {
                return phone.replaceAll("[^0-9]", "");
        }
        return phone;
    }

    private void prepare(TopayTransaction transaction) throws PaymentException {



        Profile profile = profileService.getProfile(transaction.getProfileId());
        WebUtils.Parameters params = new WebUtils.Parameters()
                .add(Params.USER, "tel:+" + preparePhone(transaction.getParam1()))
                .add(Params.AMOUNT, transaction.getAmountNet().setScale(2).toString())
                .add(Params.CCY, CURRENCY)
                .add(Params.COMMENT, getDescriptionMessage(transaction, profile))
                .add(Params.LIFETIME, formatToISO8601(min(transaction.getExpireDate(), DateUtils.addHours(new Date(), billLifeTime))))
                .add(Params.PAY_SOURCE, transaction.getPaymentToolCode())
                .add(Params.PRV_NAME, PRV_NAME);


        boolean success = false;
        String url = String.format(BILL_URL, login, transaction.getTransactionId());
        for (int i = 0; i < RETRIES_COUNT && !success; ++i) {
            if (i > 0) {
                log.error("Qiwi retries");
            }
            try {
                log.info("QIWI url: " + url);
                Request request = Request.Put(url);
                String qiwiResponse = execute(request, params);
                log.info("QIWI bill response: " + qiwiResponse);
                Response response = mapper.readValue(qiwiResponse, Response.class);
                if (response.getResultCode() != 0) {
                    transaction.setExtErrorCode(response.getResultCode());
                    throw new PaymentException("QIWI result code: " + response.getResultCode());
                }
                success = true;
            } catch (HttpResponseException ex) {
                log.error("error occur while creating QIWI bill for transaction #" + transaction.getTransactionId() +
                        " " + ex.getStatusCode() + " " + ex.getMessage() +
                        " " + url + " " + params.toString(), ex);
            } catch (Exception e) {
                log.error("error occur while creating QIWI bill for transaction #" + transaction.getTransactionId(), e);
            }
        }
        if (!success) {
            throw new PaymentException("can't create QIWI bill for transaction #" + transaction.getTransactionId());
        }
        log.info("QIWI bill request: " + params);
    }


    private String redirect(@Nonnull TopayTransaction transaction) {
        PaymentMethod method = paymentMethodDAO.select(transaction.getPaymentMethodId());
        return method.isMobile() ?
                getReturnUrl(transaction.getTransactionId()) :
                getRedirectUrl(transaction.getTransactionId());
    }

    @Override
    public String prepareAndGetRedirectUrl(@Nonnull TopayTransaction transaction) throws PaymentException, NotFoundException {
        prepare(transaction);
        return redirect(transaction);
    }

    @Override
    public PaymentResult validate(@Nonnull TopayTransaction transaction, @Nullable Map<String, String> params) throws PaymentException {
        Request request = Request.Get(String.format(BILL_URL, login, transaction.getTransactionId()));
        PaymentResult result;
        try {
            String response = execute(request, null);
            log.info("QIWI bill status response:\n" + response);
            Response resp = mapper.readValue(response, Response.class);
            if (resp.getResultCode() == 0) {
                Bill bill = resp.getBill();
                switch (bill.getStatus()) {
                    case paid:
                        result = PaymentResult.COMPLETED;
                        break;
                    case unpaid:
                    case rejected:
                    case expired:
                        result = PaymentResult.REJECTED;
                        break;
                    case waiting:
                        result = PaymentResult.WAITING;
                        break;
                    default:
                        log.warn("unknown qiwi bill status: " + bill.getStatus());
                        result = PaymentResult.ERROR;
                }
            } else {
                result = PaymentResult.ERROR;
            }
        } catch (IOException e) {
            log.warn("QIWI bill validation error " + transaction.getTransactionId(), e);
            transaction.setStatus(TopayTransactionStatus.ERROR);
            result = PaymentResult.ERROR;
        }
        return result;
    }
    
    @Override
    public boolean canValidate() {
        return true;
    }

    @Override
    public boolean cancel(@Nonnull TopayTransaction transaction) {
        //TODO: implement in future
        return false;
    }

    @Override
    public CallbackResult handleCallback(Map<String, String> params, HttpServletRequest request) throws PaymentException {
        log.info("QIWI callback request arrived");
        TopayTransaction transaction = null;
        PaymentResult paymentResult;
        try {
            long transactionId = NumberUtils.toLong(params.get(Params.BILL_ID), 0L);
            BillStatus status = BillStatus.valueOf(params.get(Params.STATUS));
            BigDecimal amount = new BigDecimal(params.get(Params.AMOUNT));
            log.info("transaction id: " + transactionId + "; status: " + status + "; amount: " + amount.doubleValue());
            transaction = getTransactionSafe(transactionId);
            if (!Utils.INSTANCE.equals(amount, transaction.getAmountNet())) {
                throw new PaymentException("invalid amount");
            }
            switch (status) {
                case paid:
                    paymentResult = PaymentResult.COMPLETED;
                    break;
                case unpaid:
                case rejected:
                case expired:
                    paymentResult = PaymentResult.REJECTED;
                    break;
                case waiting:
                    paymentResult = PaymentResult.WAITING;
                    break;
                default:
                    paymentResult = PaymentResult.ERROR;

            }
        } catch (Exception e) {
            paymentResult = PaymentResult.ERROR;
            log.error("QIWI callback handling error", e);
        }

        return new CallbackResult(paymentResult, transaction, CALLBACK_RESPONSE, CONTENT_TYPE, RESPONSE_CONTENT_TYPE);
    }


    private String getRedirectUrl(long transactionId) {
        String doneUrl = getReturnUrl(transactionId);
        WebUtils.Parameters params = new WebUtils.Parameters()
                .add("shop", login)
                .add("transaction", transactionId)
                .add("successUrl", doneUrl)
                .add("failUrl", doneUrl);
        return params.createUrl(REDIRECT_URL, false);
    }

    private String execute(Request request, WebUtils.Parameters params) throws IOException {
        if (params != null) {
            request.bodyForm(params.createForm().build(), Consts.UTF_8);
        }
        request.addHeader(Headers.ACCEPT, "text/json")
                .addHeader(Headers.AUTHORIZATION, authorization)
                .connectTimeout(connectionTimeout)
                .socketTimeout(connectionTimeout);
        return request.execute().returnContent().asString();
    }

    private static String formatToISO8601(Date date) {
        return new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").format(date);
    }
}

package ru.planeta.payment.processors.robokassa.models.enums;

import javax.xml.bind.annotation.XmlEnumValue;

/**
 * Created by eshevchenko on 11.08.14.
 */
public enum StateCode {
    @XmlEnumValue(value = "5")
    INITIALIZED,
    @XmlEnumValue(value = "10")
    OVERDUE,
    @XmlEnumValue(value = "50")
    IN_PROCESS,
    @XmlEnumValue(value = "60")
    REFUNDED,
    @XmlEnumValue(value = "80")
    STOPPED,
    @XmlEnumValue(value = "100")
    SUCCESS;
}

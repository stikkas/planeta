package ru.planeta.payment.processors.w1;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.math.NumberUtils;
import ru.planeta.api.Utils;
import ru.planeta.api.exceptions.NotFoundException;
import ru.planeta.api.exceptions.PaymentException;
import ru.planeta.commons.web.WebUtils;
import ru.planeta.model.common.TopayTransaction;
import ru.planeta.model.profile.Profile;
import ru.planeta.payment.processors.AbstractPaymentProcessor;
import ru.planeta.payment.processors.CallbackResult;
import ru.planeta.payment.processors.PaymentResult;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.io.OutputStream;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import static ru.planeta.api.Utils.empty;
import static ru.planeta.api.Utils.smap;

/**
 *
 * Created by alexa_000 on 16.05.2014.
 */
public class W1PaymentProcessor extends AbstractPaymentProcessor {

    private interface Params {
        String CULTURE_ID = "WMI_CULTURE_ID";
        String RECIPIENT_LOGIN = "WMI_RECIPIENT_LOGIN";
        String PTDISABLED = "WMI_PTDISABLED";
        String PTENABLED = "WMI_PTENABLED";
        String EXPIRED_DATE = "WMI_EXPIRED_DATE";
        String FAIL_URL = "WMI_FAIL_URL";
        String SUCCESS_URL = "WMI_SUCCESS_URL";
        String DESCRIPTION = "WMI_DESCRIPTION";
        String CURRENCY_ID = "WMI_CURRENCY_ID";
        String MERCHANT_ID = "WMI_MERCHANT_ID";
        String PAYMENT_AMOUNT = "WMI_PAYMENT_AMOUNT";
        String SIGNATURE = "WMI_SIGNATURE";
        String PAYMENT_NO = "WMI_PAYMENT_NO";
        String ORDER_ID = "WMI_ORDER_ID";
        String RESULT = "WMI_RESULT";
        String ORDER_STATE = "WMI_ORDER_STATE";
    }

    private interface RedirectParams {
        String MERCHANT_ID = "m";
        String PAYMENT_ID = "i";
    }

    private static final String CONTENT_TYPE = "application/x-www-form-urlencoded";
    private static final String ENC = "windows-1251";
    private static final String LANG = "ru-RU";
    private static final String CURRENCY = "643";
    private static final String CALLBACK_RESPONSE = "WMI_RESULT=OK";
    private static final String ORDER_ACCEPTED = "Accepted";

    private String login;
    private String password;
    private String url;
    private int billLifeTime;

    private void prepare(TopayTransaction transaction) throws PaymentException, NotFoundException {

        Profile profile = profileService.getProfileSafe(transaction.getProfileId());
        String email = getAuthorizationService().getUserPrivateEmailById(profile.getProfileId());
        BigDecimal amount = transaction.getAmountNet().setScale(2, RoundingMode.HALF_UP);

        try {
            Map<String, String> data = Utils.smap(
                    Params.PAYMENT_NO, transaction.getTransactionId(),
                    Params.PAYMENT_AMOUNT, amount,
                    Params.RECIPIENT_LOGIN, email,
                    Params.MERCHANT_ID, login,
                    Params.DESCRIPTION, "BASE64:" + Base64.encodeBase64String(getDescriptionMessage(transaction, profile).getBytes("UTF-8")),
                    Params.CURRENCY_ID, CURRENCY,
                    Params.SUCCESS_URL, getReturnUrl(transaction.getTransactionId()),
                    Params.FAIL_URL, getReturnUrl(transaction.getTransactionId()),
                    Params.CULTURE_ID, LANG);
            if (!Utils.empty(transaction.getPaymentToolCode())) {
                data.put(Params.PTENABLED, transaction.getPaymentToolCode());
            }
            data.put(Params.SIGNATURE, sign(data, password));

            String dataStr = WebUtils.tryUrlEncodeMap(data, ENC);
            log.info("sending data to w1: " + dataStr);
            HttpURLConnection conn = (HttpURLConnection) new URL(url).openConnection();
            try {
                conn.setDoOutput(true);
                conn.setDoInput(true);
                conn.setInstanceFollowRedirects(false);
                conn.setRequestMethod("POST");
                conn.setRequestProperty("Content-Type", CONTENT_TYPE);
                conn.setRequestProperty("Content-Length", String.valueOf(dataStr.length()));
                conn.setRequestProperty("Charset", ENC);
                conn.setUseCaches(false);
                try (OutputStream os = conn.getOutputStream()) {
                    IOUtils.copy(new StringReader(dataStr), os);
                }
                int responseCode = conn.getResponseCode();
                String location = conn.getHeaderField("Location");
                log.info("response code: " + responseCode + " location: " + location);
                if (responseCode != HttpURLConnection.HTTP_MOVED_TEMP) {
                    throw new PaymentException("unexpected response from w1 server. Code: " + responseCode);
                }
                transaction.setExtTransactionId(WebUtils.getQueryParams(location, ENC).get(RedirectParams.PAYMENT_ID));
            } finally {
                conn.disconnect();
            }

        } catch (IOException e) {
            throw new PaymentException(e);
        }
    }


    private String redirect(@Nonnull TopayTransaction transaction) {
        if (Utils.empty(transaction.getExtTransactionId())) {
            return getReturnUrl(transaction.getTransactionId());
        }
        WebUtils.Parameters parameters = new WebUtils.Parameters()
                .add(RedirectParams.MERCHANT_ID, login)
                .add(RedirectParams.PAYMENT_ID, transaction.getExtTransactionId());
        return parameters.createUrl(url, false);
    }

    @Override
    public PaymentResult validate(@Nonnull TopayTransaction transaction, @Nullable Map<String, String> params) throws PaymentException {
        return PaymentResult.NOT_IMPLEMENTED;
    }

    @Override
    public boolean cancel(@Nonnull TopayTransaction transaction) {
        return false;
    }

    @Override
    public String prepareAndGetRedirectUrl(@Nonnull TopayTransaction transaction) throws PaymentException, NotFoundException {
        prepare(transaction);
        return redirect(transaction);
    }

    @Override
    public CallbackResult handleCallback(Map<String, String> params, HttpServletRequest request) throws PaymentException {
        TopayTransaction transaction = null;
        PaymentResult result;
        if (Utils.empty(params)) {
            result = PaymentResult.ERROR;
            log.error("can't process w1 empty params");
        } else try {
//            String sign = params.get(Params.SIGNATURE);
            final long transactionId = NumberUtils.toLong(params.get(Params.PAYMENT_NO));
            transaction = getTransactionSafe(transactionId);
            if (!Utils.INSTANCE.equals(transaction.getAmountNet(), new BigDecimal(params.get(Params.PAYMENT_AMOUNT)))) {
                throw new PaymentException("illegal transaction amount");
            }

            String orderState = params.get(Params.ORDER_STATE);
            log.debug("transaction #" + transactionId + " order state #" + orderState);
            if (ORDER_ACCEPTED.equals(orderState)) {
                result = PaymentResult.COMPLETED;
            } else {
                result = PaymentResult.REJECTED;
            }
        } catch (Exception e) {
            result = PaymentResult.ERROR;
            log.error("can't process w1 callback", e);
            log.error("params " + params.toString());
        }
        return new CallbackResult(result, transaction, CALLBACK_RESPONSE);
    }

    private static String sign(Map<String, String> params, String password) throws UnsupportedEncodingException {
        List<String> names = new ArrayList<>(params.keySet());
        Collections.sort(names);
        StringBuilder sb = new StringBuilder();
        for (String name : names) {
            sb.append(params.get(name));
        }
        sb.append(password);
        return Base64.encodeBase64String(DigestUtils.md5(sb.toString().getBytes(ENC)));
    }


    public int getBillLifeTime() {
        return billLifeTime;
    }

    public void setBillLifeTime(int billLifeTime) {
        this.billLifeTime = billLifeTime;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }
}

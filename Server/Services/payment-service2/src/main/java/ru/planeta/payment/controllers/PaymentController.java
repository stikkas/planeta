package ru.planeta.payment.controllers;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import ru.planeta.api.Utils;
import ru.planeta.api.aspect.logging.BillingLoggableRequest;
import ru.planeta.api.aspect.logging.DBLoggerTypeInfoHolder;
import ru.planeta.api.exceptions.MessageCode;
import ru.planeta.api.exceptions.NotFoundException;
import ru.planeta.api.exceptions.PaymentException;
import ru.planeta.api.exceptions.PermissionException;
import ru.planeta.api.helper.ProjectService;
import ru.planeta.api.log.DBLogger;
import ru.planeta.api.log.LoggerListProxy;
import ru.planeta.api.log.service.DBLogService;
import ru.planeta.api.service.profile.AuthorizationService;
import ru.planeta.api.utils.PaymentUtils;
import ru.planeta.commons.web.WebUtils;
import ru.planeta.dao.payment.PaymentMethodDAO;
import ru.planeta.model.common.PaymentMethod;
import ru.planeta.model.common.PaymentProvider;
import ru.planeta.model.common.TopayTransaction;
import ru.planeta.model.enums.ProjectType;
import ru.planeta.model.enums.TopayTransactionStatus;
import ru.planeta.model.stat.log.LoggerType;
import ru.planeta.payment.processors.CallbackResult;
import ru.planeta.payment.processors.PaymentProcessor;
import ru.planeta.payment.processors.cloudpayments.CloudPaymentsPaymentProcessor;
import ru.planeta.payment.services.PaymentServiceWrap;
import ru.planeta.payment.services.SessionService;
import ru.planeta.payment.utils.ProperRedirectView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.*;

import static ru.planeta.api.Utils.empty;

/**
 * Created with IntelliJ IDEA.
 * User: a.volkov
 * Date: 23.04.14
 * Time: 20:04
 */
@Controller
public class PaymentController {


    private Logger logger;

    protected Logger getLogger() {
        if (logger == null) {
            logger = new LoggerListProxy("paymentControllerLogger",
                    Logger.getLogger(PaymentController.class),
                    DBLogger.Companion.getLogger(LoggerType.PAYMENT, dbLogService));
        }

        return logger;
    }


    private final PaymentServiceWrap paymentServiceWrap;
    private final SessionService sessionService;
    private final ProjectService projectService;
    private final PaymentMethodDAO paymentMethodDAO;
    private final AuthorizationService authorizationService;
    private final DBLogService dbLogService;

    private static final String[] FORBIDDEN_USER_AGENTS = new String[]{"skypeuripreview"};

    @Autowired
    public PaymentController(PaymentServiceWrap paymentServiceWrap, SessionService sessionService, ProjectService projectService, PaymentMethodDAO paymentMethodDAO, AuthorizationService authorizationService, DBLogService dbLogService) {
        this.paymentServiceWrap = paymentServiceWrap;
        this.sessionService = sessionService;
        this.projectService = projectService;
        this.paymentMethodDAO = paymentMethodDAO;
        this.authorizationService = authorizationService;
        this.dbLogService = dbLogService;
    }

    @BillingLoggableRequest
    @RequestMapping(value = Urls.REDIRECT, method = RequestMethod.GET)
    public ModelAndView redirect(@RequestParam long transactionId,
                                 @RequestParam("s") String sign) throws NotFoundException, PaymentException {
        final TopayTransaction transaction;
        try {
            transaction = paymentServiceWrap.getTransaction(transactionId);
        } catch (Exception ex) {
            getLogger().warn("transaction with id " + transactionId + " not found");
            return createRedirectModelAndView(projectService.getPaymentFailUrl(ProjectType.MAIN, transactionId));
        }
        PaymentMethod paymentMethod = paymentMethodDAO.select(transaction.getPaymentMethodId());
        if (!paymentMethod.isInternal() && (transaction.getStatus() != TopayTransactionStatus.NEW)) {
            getLogger().warn("illegal transaction status, transaction id " + transactionId + " status " + transaction.getStatus());
            return createRedirectModelAndView(projectService.getPaymentFailUrl(transaction));
        }
        if (!PaymentUtils.sign(transaction).equals(sign)) {
            getLogger().warn("signatures doesn't match, actual: " + sign + " expected: " + PaymentUtils.sign(transaction));
            return createRedirectModelAndView(projectService.getPaymentFailUrl(transaction));
        }
        sessionService.setTransactionId(transactionId);
        String url;

        try {
            url = paymentServiceWrap.getRedirectUrl(transaction);
        } catch (PaymentException e) {
            getLogger().error("error occur when redirecting", e);
            url = projectService.getPaymentFailUrl(transaction);
        }
        if (url == null) {
            getLogger().error("url is null for transaction " +  transactionId);
            url = projectService.getUrl(ProjectType.PAYMENT_GATE, Urls.PAYMENT_DONE);
        }
        return createRedirectModelAndView(url);
    }

    @BillingLoggableRequest
    @RequestMapping(value = Urls.PAYMENT_DONE, method = {RequestMethod.GET, RequestMethod.POST})
    public ModelAndView paymentDone(@RequestParam(value = "transactionid", required = false, defaultValue = "0") long transactionIdExternal, HttpServletRequest request) throws PermissionException {
        checkUserAgent(request);

        final Long transactionIdLong = sessionService.getTransactionId();

        final long transactionId;
        if (transactionIdExternal != 0) {
            transactionId = transactionIdExternal;
        } else if (transactionIdLong == null) {
            getLogger().error("can't get transaction id from session");
            return createRedirectModelAndView(projectService.getPaymentFailUrl(ProjectType.MAIN));
        } else {
            transactionId = transactionIdLong;
        }

        Map<String, String> requestParams = parseServletRequestParams(request);

        ModelAndView mav;
        try {
            TopayTransaction transaction = paymentServiceWrap.validate(transactionId, requestParams);
            if (EnumSet.of(TopayTransactionStatus.DONE, TopayTransactionStatus.NEW).contains(transaction.getStatus())) {
                mav = createRedirectModelAndView(projectService.getPaymentSuccessUrl(transaction.getProjectType(), transactionId));
            } else {
                mav = createRedirectModelAndView(projectService.getPaymentFailUrl(transaction.getProjectType(), transactionId));
            }
        } catch (Exception e) {
            getLogger().error("error while verifying payment", e);
            mav = createRedirectModelAndView(projectService.getPaymentFailUrl(ProjectType.MAIN, transactionId));
        }
        return mav;
    }

    @BillingLoggableRequest
    @RequestMapping(value = Urls.PAYMENT_NOT_DONE, method = {RequestMethod.GET, RequestMethod.POST})
    public ModelAndView paymentNotDone(@RequestParam(value = "transactionid", required = false, defaultValue = "0") long transactionIdExternal,
                                       @RequestParam(value = "reason", required = false, defaultValue = "") String reason,
                                       @RequestParam(value = "options", required = false, defaultValue = "") String options,
                                       HttpServletRequest request) throws PermissionException {
        DBLoggerTypeInfoHolder.setLoggerTypeInfo(LoggerType.PAYMENT, transactionIdExternal);
        getLogger().warn("PAYMENT IS NOT DONE [" + transactionIdExternal + "]: reason {" + reason + "}");
        getLogger().warn("PAYMENT IS NOT DONE [" + transactionIdExternal + "]: options {" + options + "}");
        return paymentDone(transactionIdExternal, request);
    }

    @RequestMapping(value = {Urls.CALLBACK, Urls.CALLBACK_OLD}, method = {RequestMethod.POST, RequestMethod.GET})
    @ResponseBody
    public void handleCallback(@PathVariable String alias, HttpServletRequest request, HttpServletResponse response) throws IOException, PaymentException {
        try {
            checkUserAgent(request);

            final PaymentProvider.Type type = getPaymentSystemType(alias);
            final Map<String, String> params = parseServletRequestParams(request);

            CallbackResult result = paymentServiceWrap.handlePaymentProviderCallback(type, params, request);

            if (result != null) {
                addHeaders(response, result.getHeaders());
                writeContent(response, result.getContent());
            }
        } catch (Exception e) {
            getLogger().error("error occur while handling callback request", e);
        }
    }

    @RequestMapping(value = Urls.NON_PARSE_CALLBACK, method = RequestMethod.POST)
    @ResponseBody
    public void handleNonParseCallback(@PathVariable String alias, HttpServletRequest request, HttpServletResponse response) throws IOException, PaymentException {
        try {
            checkUserAgent(request);

            final PaymentProvider.Type type = getPaymentSystemType(alias);
            CallbackResult result = paymentServiceWrap.handlePaymentProviderCallback(type, null, request);
            if (result != null) {
                addHeaders(response, result.getHeaders());
                writeContent(response, result.getContent());
            }
        } catch (Exception e) {
            getLogger().error("error occur while handling callback request", e);
        }
    }


    @RequestMapping(value = {Urls.CALLBACK_CLOUD}, method = {RequestMethod.POST})
    @ResponseBody
    public void handleCallbackCloudPayments(HttpServletRequest request, HttpServletResponse response) throws IOException, PaymentException {
        getLogger().info("CALLBACK_CLOUD");
        CallbackResult result = paymentServiceWrap.handlePaymentProviderCallback(PaymentProvider.Type.CLOUD_PAYMENTS, null, request);
        if (result != null) {
            addHeaders(response, result.getHeaders());
            writeContent(response, result.getContent());
        }
    }

    private static void checkUserAgent(HttpServletRequest request) throws PermissionException {
        final String userAgent = request.getHeader("User-Agent");
        if (userAgent != null) {
            for (String forbiddenAgent : FORBIDDEN_USER_AGENTS) {
                if (StringUtils.containsIgnoreCase(userAgent, forbiddenAgent)) {
                    throw new PermissionException(MessageCode.TOO_MANY_REQUESTS);
                }
            }
        }
    }

    @RequestMapping(value = {Urls.CHECK_CALLBACK, Urls.CHECK_CALLBACK_OLD}, method = {RequestMethod.POST, RequestMethod.GET})
    @ResponseBody
    public void handleCheckCallback(@PathVariable String alias, HttpServletRequest request, HttpServletResponse response) throws IOException, PaymentException {
        try {
            PaymentProvider.Type type = getPaymentSystemType(alias);
            Map<String, String> params = parseServletRequestParams(request);
            InputStream content = request.getInputStream();
            CallbackResult result = paymentServiceWrap.handlePaymentProviderCheckCallback(type, params, content);
            if (result != null) {
                addHeaders(response, result.getHeaders());
                writeContent(response, result.getContent());
            }
        } catch (Exception e) {
            getLogger().error("error occur while handling callback request", e);
        }
    }

    @RequestMapping(value = Urls.VALIDATE_PAYMENTS, method = RequestMethod.GET)
    @ResponseBody
    public String validatePayments(@RequestParam(required = false) Date from,
                                   @RequestParam(required = false) Date to) {
        StringBuilder response = new StringBuilder();
        Calendar calendar = Calendar.getInstance();
        if (to == null) {
            to = calendar.getTime();
        }
        if (from == null) {
            calendar.add(Calendar.DATE, -1);
            from = calendar.getTime();
        }
        List<TopayTransaction> payments = paymentServiceWrap.getPayments(TopayTransactionStatus.NEW, from, to);
        response.append("found ").append(payments.size()).append(" new transactions for period from ").append(from).append(" to ").append(to);
        response.append("\n");
        for (TopayTransaction t : payments) {
            try {
                t = paymentServiceWrap.validate(t.getTransactionId(), null);
                response.append("transaction ").append(t.getTransactionId()).append(" validated; status: ").append(t.getStatus());
                response.append("\n");
            } catch (Exception e) {
                response.append("validation of transaction ").append(t.getTransactionId()).append(" failed: ");
                response.append(e.getClass().getName()).append(": ").append(e.getMessage()).append("\r\n");
                getLogger().error("can't validate payment " + t.getTransactionId(), e);
            }
        }
        response.append("validation done");
        return response.toString();
    }

    private static void writeContent(HttpServletResponse response, String content) throws IOException {
        Writer writer = new OutputStreamWriter(response.getOutputStream());
        try {
            writer.write(content);
        } finally {
            IOUtils.closeQuietly(writer);
        }
    }

    private static void addHeaders(HttpServletResponse response, Map<String, String> headers) {
        if (Utils.empty(headers)) {
            return;
        }

        for (Map.Entry<String, String> header : headers.entrySet()) {
            response.addHeader(header.getKey(), header.getValue());
        }
    }

    private static Map<String, String> parseServletRequestParams(HttpServletRequest request) {
        return WebUtils.Parameters.parseServletRequest(request).getParams();
    }

    private static PaymentProvider.Type getPaymentSystemType(String alias) {
        return PaymentProvider.Type.valueOf(alias.replace('-', '_').toUpperCase());
    }

    private static ModelAndView createRedirectModelAndView(String url) {
        return new ModelAndView(new ProperRedirectView(url));
    }

    @RequestMapping(value = Urls.CLOUD_PAYMENTS, method = RequestMethod.GET)
    @ResponseBody
    public ModelAndView cloudpayments_prepare(@RequestParam(value = "transactionId") long transactionId,
                                              @RequestParam(value = "sign") String sign) throws NotFoundException {

        return new ModelAndView("cloudpayments-prepare")
                .addObject("transactionId", transactionId)
                .addObject("sign", sign);
    }


    @RequestMapping(value = Urls.CLOUD_PAYMENTS, method = RequestMethod.POST)
    @ResponseBody
    public ModelAndView cloudpayments(@RequestParam(value = "transactionId") long transactionId,
                                      @RequestParam(value = "sign") String sign,
                                      HttpServletResponse response) throws NotFoundException {
        final Pair<PaymentProcessor, TopayTransaction> processorAndTransaction = paymentServiceWrap.getProcessorAndTransaction(transactionId);
        TopayTransaction transaction = processorAndTransaction.getRight();
        CloudPaymentsPaymentProcessor processor = (CloudPaymentsPaymentProcessor) processorAndTransaction.getLeft();

        if (!PaymentUtils.sign(transaction).equals(sign)) {
            getLogger().warn("signatures doesn't match, actual: " + sign + " expected: " + PaymentUtils.sign(transaction));
            return createRedirectModelAndView(projectService.getPaymentFailUrl(transaction));
        }

        if (transaction.getExtTransactionId() != null) {
            getLogger().info("Transaction #" + transactionId + " second payment!");
            if (EnumSet.of(TopayTransactionStatus.DONE, TopayTransactionStatus.NEW).contains(transaction.getStatus())) {
                return createRedirectModelAndView(projectService.getPaymentSuccessUrl(transaction));
            } else {
                return createRedirectModelAndView(projectService.getPaymentFailUrl(transaction));
            }
        }

        if (transaction.getStatus() == TopayTransactionStatus.DONE) {
            getLogger().info("Transaction #" + transactionId + " already success!");
            return createRedirectModelAndView(projectService.getPaymentSuccessUrl(transaction));
        }
        if (transaction.getStatus() == TopayTransactionStatus.ERROR) {
            getLogger().warn("Transaction #" + transactionId + " in error state!");
            return createRedirectModelAndView(projectService.getPaymentFailUrl(transaction));
        }

        ModelAndView modelAndView = new ModelAndView("cloudpayments");
        modelAndView.addObject("transaction", transaction);
        modelAndView.addObject("publicId", processor.getPublicId());
        final String email = authorizationService.getUserPrivateEmailById(transaction.getProfileId());
        if (email != null) {
            modelAndView.addObject("userEmail", email);
        }
        // important
        WebUtils.setNoCacheResponseHeaders(response);
        return modelAndView;

    }
}





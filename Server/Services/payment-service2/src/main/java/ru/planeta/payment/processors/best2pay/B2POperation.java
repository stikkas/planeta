package ru.planeta.payment.processors.best2pay;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: a.volkov
 * Date: 23.04.14
 * Time: 18:52
 */
@XmlRootElement(name = "operation")
@XmlAccessorType(XmlAccessType.FIELD)
public class B2POperation {
    @XmlElement
    public long id;
    @XmlElement(name = "order_id")
    public long orderId;
    @XmlElement(name = "order_state")
    public String orderState;
    @XmlElement
    public long reference;
    @XmlElement
    public Date date;
    @XmlElement
    public String type;
    @XmlElement
    public String state;
    @XmlElement(name = "reason_code")
    public int reasonCode;
    @XmlElement
    public String message;
    @XmlElement
    public long amount;
    @XmlElement
    public int currency;
    @XmlElement
    public String signature;
    @XmlElement
    public String pan;
    @XmlElement
    public String name;
    @XmlElement
    public String token;
}

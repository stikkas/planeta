package ru.planeta.payment.processors.best2pay;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;
import ru.planeta.api.Utils;
import ru.planeta.api.exceptions.NotFoundException;
import ru.planeta.api.exceptions.PaymentException;
import ru.planeta.api.service.billing.payment.system.yamoney.mws.exceptions.ResponseParsingException;
import ru.planeta.api.service.billing.payment.system.yamoney.mws.utils.MarshallingUtils;
import ru.planeta.commons.web.WebUtils;
import ru.planeta.model.common.TopayTransaction;
import ru.planeta.model.profile.Profile;
import ru.planeta.payment.processors.AbstractPaymentProcessor;
import ru.planeta.payment.processors.CallbackResult;
import ru.planeta.payment.processors.PaymentResult;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.servlet.http.HttpServletRequest;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.IOException;
import java.io.StringReader;
import java.math.BigDecimal;
import java.util.Map;

import static ru.planeta.api.Utils.empty;

/**
 * Created with IntelliJ IDEA.
 * User: a.volkov
 * Date: 23.04.14
 * Time: 18:45
 */
public class Best2PayPaymentProcessor extends AbstractPaymentProcessor {

    private static final String VALIDATION_RESPONSE = "OK";
    private static final String VALIDATION_CONTENT_TYPE = "text/plain";
    private static final String SUCCESS_ORDER_STATE = "COMPLETED";
    private static final String SUCCESS_OP_STATE = "APPROVED";

    private long externalId;
    private String url;
    private int currency;
    private String signPswd;

    private SAXParserFactory parserFactory;

    private interface Params {

        String SHOP_ID = "sector";
        String ORDER_ID = "reference";
        String EXT_ORDER_ID = "id";
        String OPERATION_ID = "operation";
        String DESCRIPTION = "description";
        String RETURN_URL = "url";
        String SIGN = "signature";
        String PREAUTH = "preauth";
        String AMOUNT = "amount";
        String CURRENCY = "currency";
        String MODE = "mode";
        String EMAIL = "email";
        String GET_TOKEN = "get_token";
        String TOKEN = "token";
    }

    private interface Service {

        String REGISTER = "Register";
        String PURCHASE = "Purchase";
        String PURCHASE_BY_TOKEN = "PurchaseByToken";
        String ORDER = "Order";
        String OPERATION = "Operation";
        String REVERSE = "Reverse";
    }

    public Best2PayPaymentProcessor() {
        try {
            parserFactory = SAXParserFactory.newInstance();
        } catch (Exception e) {
            log.error(e);
        }
    }

    private void prepare(TopayTransaction transaction) throws PaymentException, NotFoundException {
        int amount = transaction.getAmountNet().multiply(new BigDecimal(100)).intValue();
        Profile profile = profileService.getProfileSafe(transaction.getProfileId());
        String email = getAuthorizationService().getUserPrivateEmailById(profile.getProfileId());
        WebUtils.Parameters params = new WebUtils.Parameters()
                .add(Params.AMOUNT, amount)
                .add(Params.CURRENCY, currency)
                .add(Params.SHOP_ID, externalId)
                .add(Params.DESCRIPTION, getDescriptionMessage(transaction, profile))
                .add(Params.ORDER_ID, transaction.getTransactionId())
                .add(Params.MODE, 1)
                .add(Params.RETURN_URL, super.getReturnUrl())
                .add(Params.SIGN, sign(externalId, amount, currency, signPswd));
        if (email != null) {
            params.add(Params.EMAIL, email);
        }
        log.info("register best2pay order:\n" + params.createUrl("", false));
        String response = WebUtils.uploadMapExternal(url + Service.REGISTER, params.getParams());
        log.info("best2pay payment registration response:" + response);
        if (response == null) {
            throw new PaymentException("no response from best2pay");
        } else if (response.contains("error")) {
            String errorDescr;
            try {
                B2PError error = MarshallingUtils.unmarshal(B2PError.class, response);
                transaction.setExtErrorCode(error.code);
                transaction.setExtErrorMessage(error.description);
                errorDescr = error.toString();
            } catch (Exception e) {
                errorDescr = e.toString();
            }
            throw new PaymentException(errorDescr);
        } else {
            if (StringUtils.isNotBlank(transaction.getExtTransactionId())) {
                // not rewrite if already have external number PLANETA-14731
                return;
            }
            transaction.setExtTransactionId(response);
        }
    }


    private String redirect(@Nonnull TopayTransaction transaction) {
        if (Utils.empty(transaction.getExtTransactionId())) {
            throw new IllegalArgumentException();
        }
        WebUtils.Parameters params = new WebUtils.Parameters()
                .add(Params.SHOP_ID, externalId)
                .add(Params.EXT_ORDER_ID, transaction.getExtTransactionId())
                .add(Params.SIGN, sign(externalId + transaction.getExtTransactionId() + signPswd));
        return params.createUrl(this.url + Service.PURCHASE, true);
    }

    @Override
    public String prepareAndGetRedirectUrl(@Nonnull TopayTransaction transaction) throws PaymentException, NotFoundException {
        prepare(transaction);
        return redirect(transaction);
    }

    @Override
    public PaymentResult validate(@Nonnull TopayTransaction transaction, @Nullable Map<String, String> requestParams) throws PaymentException {

        if (requestParams != null && requestParams.get("error") != null) {
            int error = NumberUtils.toInt(requestParams.get("error"));
            if (error == 127) {
                // user clicked "return to shop" button on payment page
                log.warn("request contains error param = " + error + " skip validation");
                return PaymentResult.REJECTED;
            }
        }

        final PaymentResult result;
        if (requestParams != null && !Utils.empty(requestParams.get(Params.OPERATION_ID))) {
            long operationId = NumberUtils.toLong(requestParams.get(Params.OPERATION_ID));
            if (operationId == 0) {
                log.error("best2pay operation request params fail: " + requestParams);
                return PaymentResult.ERROR;
            }
            WebUtils.Parameters params = new WebUtils.Parameters()
                    .add(Params.SHOP_ID, externalId)
                    .add(Params.EXT_ORDER_ID, transaction.getExtTransactionId())
                    .add(Params.OPERATION_ID, operationId)
                    .add(Params.SIGN, sign(externalId, transaction.getExtTransactionId(), operationId, signPswd));
            log.info("best2pay operation request params: " + params.getParams());
            String response = WebUtils.uploadMapExternal(url + Service.OPERATION, params.getParams());
            log.info("best2pay operation response:\n" + response);
            if (response == null) {
                log.warn("no response from best2pay");
                return PaymentResult.ERROR;
            }
            try {
                result = validateOperation(response);
            } catch (Exception e) {
                log.error("best2pay validating response error", e);
                return PaymentResult.ERROR;
            }
        } else {
            if (NumberUtils.toLong(transaction.getExtTransactionId()) == 0) {
                log.error("best2pay no external id" + transaction.getTransactionId());
                return PaymentResult.ERROR;
            }
            WebUtils.Parameters params = new WebUtils.Parameters()
                    .add(Params.SHOP_ID, externalId)
                    .add(Params.EXT_ORDER_ID, transaction.getExtTransactionId())
                    .add(Params.SIGN, sign(externalId, transaction.getExtTransactionId(), signPswd));
            String response = WebUtils.uploadMapExternal(url + Service.ORDER, params.getParams());
            log.info("best 2 pay order response:\n" + response);
            if (response == null) {
                log.warn("no response from best2pay");
                return PaymentResult.WAITING;
            }
            try {
                B2POrder order = MarshallingUtils.unmarshal(B2POrder.class, response);
                if (!checkSign(response, order.signature)) {
                    throw new PaymentException("invalid signature");
                }
                if (order.reference != transaction.getTransactionId()) {
                    throw new PaymentException("invalid order reference");
                }
                if (!Utils.equals(transaction.getAmountNet(), new BigDecimal(order.amount / 100.0))) {
                    throw new PaymentException("invalid order amount");
                }
                if (SUCCESS_ORDER_STATE.equals(order.state)) {
                    result = PaymentResult.COMPLETED;
                } else if (!Utils.empty(order.operations)) {
                    boolean success = false;
                    for (B2POperation op : order.operations) {
                        if ("PURCHASE".equals(op.type) && SUCCESS_OP_STATE.equals(op.state)) {
                            success = true;
                            break;
                        }
                    }
                    result = success ? PaymentResult.COMPLETED : PaymentResult.REJECTED;
                } else {
                    result = PaymentResult.WAITING;
                }
            } catch (Exception e) {
                log.error(e);
                return PaymentResult.ERROR;
            }
        }
        return result;
    }

    @Override
    public boolean canValidate() {
        return true;
    }

    // for tests
    public String getList(long transactionId) {
        WebUtils.Parameters params = new WebUtils.Parameters()
                .add(Params.SHOP_ID, externalId)
                .add(Params.ORDER_ID, transactionId)
                .add(Params.SIGN, sign(externalId, transactionId, signPswd));
        String response = WebUtils.uploadMapExternal(url + "Orders", params.getParams());
        return response;
    }

    @Override
    public boolean cancel(@Nonnull TopayTransaction transaction) {
        int amount = transaction.getAmountNet().intValue() * 100;
        WebUtils.Parameters params = new WebUtils.Parameters()
                .add(Params.SHOP_ID, externalId)
                .add(Params.EXT_ORDER_ID, transaction.getExtTransactionId())
                .add(Params.AMOUNT, amount)
                .add(Params.CURRENCY, currency)
                .add(Params.SIGN, sign(externalId, transaction.getExtTransactionId(), amount, currency, signPswd));
        String response = WebUtils.uploadMapExternal(url + Service.REVERSE, params.getParams());
        log.info("best2pay reverse response:\n" + response);
        boolean success = false;
        try {
            B2POperation op = MarshallingUtils.unmarshal(B2POperation.class, response);
            success = SUCCESS_OP_STATE.equals(op.state);
        } catch (Exception e) {
            log.error("error while parsing b2p reverse response", e);
        }
        return success;
    }

    @Override
    public CallbackResult handleCallback(Map<String, String> params, HttpServletRequest request) throws PaymentException {
        return handleStreamCallback(request);
    }

    @Override
    public CallbackResult handleStreamCallback(HttpServletRequest request) {
        TopayTransaction transaction = null;
        PaymentResult result;
        String xml = "";
        try {
            xml = IOUtils.toString(request.getInputStream());
            Pair<PaymentResult, TopayTransaction> res = validateOperationPair(xml);
            result = res.getLeft();
            transaction = res.getRight();
        } catch (Exception e) {
            result = PaymentResult.ERROR;
            log.error("can't process best2pay callback " + xml, e);
            log.error("can't process best2pay callback data " + WebUtils.requestDataToString(request), e);
        }
        return new CallbackResult(result, transaction, VALIDATION_RESPONSE, "Content-Type", VALIDATION_CONTENT_TYPE);
    }

    private PaymentResult validateOperation(String operationXml) throws PaymentException, ParserConfigurationException, SAXException, IOException, ResponseParsingException {
        return validateOperationPair(operationXml).getLeft();
    }

    private Pair<PaymentResult, TopayTransaction> validateOperationPair(String operationXml) throws PaymentException, ParserConfigurationException, SAXException, IOException, ResponseParsingException {
        B2POperation op = MarshallingUtils.unmarshal(B2POperation.class, operationXml);
        TopayTransaction transaction = getTransactionSafe(op.reference);
        if (!checkSign(operationXml, op.signature)) {
            throw new PaymentException("invalid signature");
        }
        BigDecimal amount = transaction.getAmountNet().multiply(new BigDecimal(100.0));
        if (!Utils.equals(amount, new BigDecimal(op.amount))) {
            throw new PaymentException("invalid payment amount");
        }
        PaymentResult result;
        if (SUCCESS_OP_STATE.equals(op.state)) {
            result = PaymentResult.COMPLETED;
        } else {
            result = PaymentResult.REJECTED;
        }
        return Pair.of(result, transaction);
    }

    private boolean checkSign(String xml, String sign) throws IOException, SAXException, ParserConfigurationException {
        final StringBuilder sb = new StringBuilder();
        final SAXParser parser = parserFactory.newSAXParser();
        parser.parse(new InputSource(new StringReader(xml)), new DefaultHandler() {
            private boolean ignore = false;

            @Override
            public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
                ignore = qName.equalsIgnoreCase(Params.SIGN);
            }

            @Override
            public void characters(char[] ch, int start, int length) throws SAXException {
                if (!ignore) {
                    String value = new String(ch, start, length);
                    if (StringUtils.isNotEmpty(value) && !"\n".equals(value)) {
                        sb.append(value);
                    }
                }
            }
        });
        sb.append(signPswd);
        String generatedSign = sign(sb.toString());
        log.debug("generated sign: " + generatedSign);
        log.debug("best2pay sign:  " + sign);
        return sign.equals(generatedSign);
    }

    private static String sign(String in) {
        return new String(Base64.encodeBase64(DigestUtils.md5Hex(in).getBytes()));
    }

    private String sign(Object... objects) {
        StringBuilder sb = new StringBuilder();
        for (Object o : objects) {
            sb.append(o);
        }
        return sign(sb.toString());
    }

    public long getExternalId() {
        return externalId;
    }

    public void setExternalId(long externalId) {
        this.externalId = externalId;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public int getCurrency() {
        return currency;
    }

    public void setCurrency(int currency) {
        this.currency = currency;
    }

    public String getSignPswd() {
        return signPswd;
    }

    public void setSignPswd(String signPswd) {
        this.signPswd = signPswd;
    }

}

package ru.planeta.payment.processors.robokassa.models;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by eshevchenko on 11.08.14.
 */
@XmlRootElement(name = "OperationStateResponse")
public class OperationStateResponse extends BaseResponse {

    @XmlElement(name = "Result")
    private Result result;
    @XmlElement(name = "State")
    private State state;
    @XmlElement(name = "Info")
    private Info info;

    public Result getResult() {
        return result;
    }

    public void setResult(Result result) {
        this.result = result;
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

    public Info getInfo() {
        return info;
    }

    public void setInfo(Info info) {
        this.info = info;
    }
}

package ru.planeta.payment.processors.robokassa;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
import ru.planeta.api.Utils;
import ru.planeta.api.exceptions.NotFoundException;
import ru.planeta.api.exceptions.PaymentException;
import ru.planeta.api.service.profile.AuthorizationService;
import ru.planeta.commons.web.WebUtils;
import ru.planeta.model.common.TopayTransaction;
import ru.planeta.model.profile.Profile;
import ru.planeta.payment.processors.AbstractPaymentProcessor;
import ru.planeta.payment.processors.CallbackResult;
import ru.planeta.payment.processors.PaymentResult;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.util.Map;

/**
 *
 * Created by alexa_000 on 16.05.2014.
 */
public class RobokassaPaymentProcessor extends AbstractPaymentProcessor {

    private interface Params {
        String LOGIN = "MrchLogin";
        String AMOUNT = "OutSum";
        String INVOICE_ID = "InvId";
        String DIDGIT_SIGN = "SignatureValue";
        String CURRENCY = "IncCurrLabel";
        String EMAIL = "Email";
        String DESCRIPTION = "Desc";
        String LOCALE = "Culture";
        String ENCODING = "Encoding";
    }

    private static final String ENC = "UTF-8";
    private static final String LANG = "ru";

    private String login;
    private String password;
    private String callbackPassword;
    private String url;

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getCallbackPassword() {
        return callbackPassword;
    }

    public void setCallbackPassword(String callbackPassword) {
        this.callbackPassword = callbackPassword;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }


    public String redirect(@Nonnull TopayTransaction transaction) throws NotFoundException {
        Profile profile = profileService.getProfileSafe(transaction.getProfileId());
        String email = getAuthorizationService().getUserPrivateEmailById(transaction.getProfileId());
        final BigDecimal amount = transaction.getAmountNet();
        WebUtils.Parameters params = new WebUtils.Parameters()
                .add(Params.AMOUNT, amount)
                .add(Params.INVOICE_ID, transaction.getTransactionId())
                .add(Params.CURRENCY, transaction.getPaymentToolCode())
                .add(Params.EMAIL, email)
                .add(Params.LOGIN, login)
                .add(Params.DESCRIPTION, getDescriptionMessage(transaction, profile))
                .add(Params.LOCALE, LANG)
                .add(Params.ENCODING, ENC);

        params.add(Params.DIDGIT_SIGN, sign(params));
        return params.createUrl(url, true);
    }

    @Override
    public PaymentResult validate(@Nonnull TopayTransaction transaction, @Nullable Map<String, String> params) throws PaymentException {
        return PaymentResult.NOT_IMPLEMENTED;
    }

    @Override
    public boolean cancel(@Nonnull TopayTransaction transaction) {
        return false;
    }

    @Override
    public CallbackResult handleCallback(Map<String, String> params, HttpServletRequest request) throws PaymentException {
        TopayTransaction transaction = null;
        PaymentResult result;
        try {
            log.info("robokassa callback: " + params);
            long transactionId = NumberUtils.toLong(params.get(Params.INVOICE_ID));
            transaction = getTransactionSafe(transactionId);
            BigDecimal amount = new BigDecimal(params.get(Params.AMOUNT));
            if (!Utils.INSTANCE.equals(amount, transaction.getAmountNet())) {
                throw new PaymentException("wrong amount");
            }
            String sign = params.get(Params.DIDGIT_SIGN);
            String ourSign = DigestUtils.md5Hex(params.get(Params.AMOUNT) + ":" + transactionId + ":" + callbackPassword).toUpperCase();
            log.debug("robokassa sign: " + sign);
            log.debug("our sign: " + ourSign);

            result = PaymentResult.COMPLETED;
        } catch (Exception e) {
            result = PaymentResult.ERROR;
            log.error("robokassa callback error", e);
        }
        return new CallbackResult(result, transaction, "OK");
    }

    private String sign(WebUtils.Parameters params) {
        String result = login + ":" + params.get(Params.AMOUNT) + ":" + params.get(Params.INVOICE_ID) + ":" + password;
        return DigestUtils.md5Hex(result);
    }

    @Override
    public String prepareAndGetRedirectUrl(@Nonnull TopayTransaction transaction) throws PaymentException, NotFoundException {
        return redirect(transaction);
    }
}

package ru.planeta.test;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import ru.planeta.payment.services.PoolingService;

public class PoolServiceTest extends AbstractTest {
    
    @Autowired
    private PoolingService poolingService;
    
    @Test
    public void testPaymentCheckPooler() {
        poolingService.checkPaymentTransaction();
    }  

}

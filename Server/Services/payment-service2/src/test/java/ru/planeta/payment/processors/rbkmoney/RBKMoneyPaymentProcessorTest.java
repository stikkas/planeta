package ru.planeta.payment.processors.rbkmoney;

import org.junit.Test;

import java.math.BigDecimal;

import static org.junit.Assert.*;

/**
 * Created by asavan on 24.11.2016.
 */
public class RBKMoneyPaymentProcessorTest {
    @Test
    public void getSign() throws Exception {
        RBKMoneyPaymentProcessor pp = new RBKMoneyPaymentProcessor();
        pp.setShopId("2030333");
        pp.setSecret("kjbfKbf883hjHHg");
        String sign = pp.callbackSign(new BigDecimal("1.0"), "RU176845879", "ay.meshkov@gmail.com", "ay.meshkov@gmail.com", 3, "Popolnenie lichnogo scheta polzovatelja Andrej Meshkov _na summu   1.00 rub._", 212663, "2014-11-12 11:53:06");
        System.out.println(sign);
        assertEquals("a3ddc4665d4e4d5924639e340c089aa1", sign);
    }

}
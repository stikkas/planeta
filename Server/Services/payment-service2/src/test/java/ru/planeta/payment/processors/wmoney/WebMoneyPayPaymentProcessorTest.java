package ru.planeta.payment.processors.wmoney;

import org.apache.commons.lang3.tuple.Pair;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import ru.planeta.api.exceptions.NotFoundException;
import ru.planeta.api.exceptions.PaymentException;
import ru.planeta.model.common.TopayTransaction;
import ru.planeta.payment.processors.PaymentProcessor;
import ru.planeta.payment.services.PaymentServiceWrap;
import ru.planeta.test.AbstractTest;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertTrue;

/**
 * User: a.savanovich
 * Date: 03.03.2016
 * Time: 16:04
 */
@Ignore
public class WebMoneyPayPaymentProcessorTest extends AbstractTest {
    @Autowired
    private PaymentServiceWrap paymentService;

    @Test
    public void testCheckSign() throws Exception {
        Map<String,String> params = new HashMap<>();
        params.put("LMI_SYS_PAYMENT_DATE","2016-02-05T15:01:14");
        params.put("LMI_HASH","4w8WmQ2H1eNdJOTBOjTmZw==");
        params.put("LMI_PAYMENT_NO","430830");
        params.put("LMI_PAYMENT_DESC","Пополнение личного счета пользователя petelin.alexey (на сумму 100.00 руб.)");
        params.put("LMI_SIM_MODE","0");
        params.put("LMI_PAYER_IP_ADDRESS","86.62.93.106");
        params.put("LMI_PAYMENT_REF","891");
        params.put("LMI_MERCHANT_ID","0d35ad8f-b14d-4c33-9996-e486da764980");
        params.put("LMI_PAYER_IDENTIFIER","161540299644");
        params.put("LMI_PAYER_COUNTRY","RU");
        params.put("LMI_SYS_PAYMENT_ID","43762305");
        params.put("LMI_CURRENCY","RUB");
        params.put("LMI_PAID_AMOUNT","100.00");
        params.put("LMI_PAYMENT_METHOD","Test");
        params.put("LMI_PAYMENT_AMOUNT","100.00");
        params.put("LMI_PAYMENT_SYSTEM","3");
        params.put("LMI_PAID_CURRENCY","RUB");
        WebMoneyPayPaymentProcessor p = new WebMoneyPayPaymentProcessor();
        assertTrue(p.checkSign(params));
    }

    @Test
    public void testValidate() throws PaymentException, NotFoundException {
        Pair<PaymentProcessor, TopayTransaction> pair = paymentService.getProcessorAndTransaction(657766);
        TopayTransaction transaction = pair.getRight();
        pair.getLeft().validate(transaction, null);
    }
}
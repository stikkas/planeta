package ru.planeta.test;

import org.apache.commons.lang3.tuple.Pair;
import org.apache.log4j.Logger;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import ru.planeta.api.exceptions.NotFoundException;
import ru.planeta.api.exceptions.PaymentException;
import ru.planeta.model.common.TopayTransaction;
import ru.planeta.payment.processors.PaymentProcessor;
import ru.planeta.payment.processors.best2pay.Best2PayPaymentProcessor;
import ru.planeta.payment.services.PaymentServiceWrap;

/**
 *
 * Created by a.savanovich on 01.07.2015.
 */
public class Best2PayTest extends AbstractTest {

    private static Logger log = Logger.getLogger(Best2PayTest.class);

    @Autowired
    private PaymentServiceWrap paymentService;
    @Test
    @Ignore
    public void testConcreateTransaction() throws NotFoundException, PaymentException {
        TopayTransaction result = paymentService.validate(391620, null);
        log.info(result);
    }


    @Test
    @Ignore
    public void testBest2PayOrder() throws NotFoundException, PaymentException {

        Pair<PaymentProcessor, TopayTransaction> result = paymentService.getProcessorAndTransaction(298491);
        Best2PayPaymentProcessor processor = (Best2PayPaymentProcessor) result.getLeft();
        String res = processor.getList(298491);
        System.out.println(res);

    }
}

package ru.planeta.test;

import static org.junit.Assert.*;

import java.util.Date;

import org.apache.commons.lang3.tuple.Pair;
import org.apache.log4j.Logger;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import ru.planeta.api.exceptions.NotFoundException;
import ru.planeta.api.exceptions.PaymentException;
import ru.planeta.dao.commondb.BillsFor1cDAO;
import ru.planeta.model.common.BillsFor1c;
import ru.planeta.model.common.TopayTransaction;
import ru.planeta.model.enums.TopayTransactionStatus;
import ru.planeta.payment.processors.PaymentProcessor;
import ru.planeta.payment.processors.PaymentResult;
import ru.planeta.payment.services.PaymentServiceWrap;

@Ignore
public class InvoicePayTest extends AbstractTest {
    private static Logger log = Logger.getLogger(InvoicePayTest.class); 

    @Autowired
    private PaymentServiceWrap paymentService;
    @Autowired
    private BillsFor1cDAO billsFor1cDAO;
    
    private final long transId = 425036;
    
    private void createBillFromTransaction() throws NotFoundException, PaymentException {
        BillsFor1c bill = billsFor1cDAO.select(transId);
        assertNull(bill);
        Pair<PaymentProcessor, TopayTransaction> pair = paymentService.getProcessorAndTransaction(transId);
        pair.getLeft().prepareAndGetRedirectUrl(pair.getRight());        
    }
    
    private void buyBillForTransaction() {
        BillsFor1c bill = billsFor1cDAO.select(transId);
        assertNotNull(bill);
        bill.setTimePurchased(new Date());
        billsFor1cDAO.update(bill);
    }
    
    @Test
    public void testPayPrepareAndRedirect() throws NotFoundException, PaymentException {
        createBillFromTransaction();
        BillsFor1c bill = billsFor1cDAO.select(transId);
        assertNotNull(bill);
        assertEquals(transId, bill.getTopayTransactionId().longValue());
        billsFor1cDAO.delete(bill.getTopayTransactionId());
    }
    
    @Test
    public void testPayValidate() throws NotFoundException, PaymentException {
        createBillFromTransaction();
        Pair<PaymentProcessor, TopayTransaction> pair = paymentService.getProcessorAndTransaction(transId);
        PaymentResult result = pair.getLeft().validate(pair.getRight(), null);
        log.info("Result of validate transaction #" + transId + ": " + result.toString());
        assertEquals(PaymentResult.WAITING, result);
        buyBillForTransaction();
        result = pair.getLeft().validate(pair.getRight(), null);
        log.info("Result of validate transaction #" + transId + ": " + result.toString());
        assertEquals(PaymentResult.COMPLETED, result);
        billsFor1cDAO.delete(pair.getRight().getTransactionId());
    }
    
    @Test
    public void testPayWrapValidate() throws NotFoundException, PaymentException {
        createBillFromTransaction();
        TopayTransaction transaction = paymentService.validate(transId, null);
        assertEquals(TopayTransactionStatus.NEW, transaction.getStatus());
        log.info("Status after validate transaction #" + transId + ": " + transaction.getStatus().toString());
        buyBillForTransaction();
        transaction = paymentService.validate(transId, null);
        assertEquals(TopayTransactionStatus.DONE, transaction.getStatus());
        log.info("Status after validate transaction #" + transId + ": " + transaction.getStatus().toString());
        billsFor1cDAO.delete(transaction.getTransactionId());
    }
}

package ru.planeta.test;

import org.apache.commons.codec.binary.Base64;
import org.apache.http.client.fluent.Request;
import org.apache.http.client.fluent.Response;
import org.junit.Ignore;
import org.junit.Test;
import ru.planeta.api.exceptions.NotFoundException;
import ru.planeta.api.exceptions.PaymentException;

import java.io.IOException;

/*
 * Created by Alexey on 05.04.2016.
 */

public class QiwiTest {

    private static final String login = "210274";
    private static final String BILL_URL = "https://api.qiwi.com/api/v2/prv/" + login + "/bills/477419";
    private static final Integer connectionTimeout = 25000;

    private static final String password = "GHJDjdk348GenczMp45";


    private interface Headers {
        String ACCEPT = "Accept";
        String AUTHORIZATION = "Authorization";
    }

    @Ignore
    @Test
    public void testQiwiGet() throws NotFoundException, PaymentException, IOException {
        String url = BILL_URL;
        System.out.println(url);
        Request request = Request.Get(url);
        try {
            String response = execute(request);
        } catch (IOException e) {
            System.out.println("lalala");
        }
    }

    private String execute(Request request) throws IOException {
        String authorization = "Basic " + Base64.encodeBase64String((login + ":" + password).getBytes());

        request.addHeader(Headers.ACCEPT, "text/json")
                .addHeader(Headers.AUTHORIZATION, authorization)
                .connectTimeout(connectionTimeout)
                .socketTimeout(connectionTimeout);

        Response response = request.execute();
        System.out.println(response);
        return response.returnContent().asString();
    }
}

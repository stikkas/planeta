package ru.planeta.test;

import org.apache.commons.lang3.tuple.Pair;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import ru.planeta.api.exceptions.NotFoundException;
import ru.planeta.api.exceptions.PaymentException;
import ru.planeta.model.common.TopayTransaction;
import ru.planeta.payment.processors.PaymentProcessor;
import ru.planeta.payment.services.PaymentServiceWrap;

/**
 *
 * User: Serge Blagodatskih<stikkas17@gmail.com><br>
 * Date: 15.06.16<br>
 * Time: 09:57
 */
@Ignore
public class WebMoneyTest extends AbstractTest {
    private final long transactionId = 490771l;

    @Autowired
    private PaymentServiceWrap paymentService;

    @Test
    public void testValidateWithoutParams() throws NotFoundException, PaymentException {
        System.setProperty("https.protocols", "TLSv1,TLSv1.1,TLSv1.2");
        Pair<PaymentProcessor, TopayTransaction> pair = paymentService.getProcessorAndTransaction(transactionId);
        System.out.println(pair.getKey().validate(pair.getValue(), null));
    }
}

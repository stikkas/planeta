package ru.planeta.test;

import org.junit.Ignore;
import org.junit.Test;
import ru.planeta.api.utils.Locker;

import java.util.Deque;
import java.util.LinkedList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertTrue;

/**
 * Created with IntelliJ IDEA.
 * User: a.volkov
 * Date: 25.08.14
 * Time: 16:53
 */
public class LockerTest {
    private static final int POOL_SIZE = 25;
    private static final Long PROFILE_ID = -1L;

    private final Locker<Long> locker = new Locker<Long>();
    private final Deque<Long> list = new LinkedList<Long>();

    @Test
    @Ignore
    public void test() throws InterruptedException {
        final ExecutorService executorService = Executors.newScheduledThreadPool(POOL_SIZE);
        for (int i = 0; i < POOL_SIZE; ++i) {
            executorService.submit(new Runnable() {
                @Override
                public void run() {
                    try {
                        locker.lock(PROFILE_ID);
                        list.addFirst(Thread.currentThread().getId());
                        //doing some hard work here
                        Thread.sleep(1000);
                        if (list.getFirst() == Thread.currentThread().getId()) {
                            list.removeFirst();
                        }
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    } finally {
                        locker.unlock(PROFILE_ID);
                    }
                }
            });
        }

        executorService.shutdown();
        executorService.awaitTermination(POOL_SIZE + 1, TimeUnit.SECONDS);
        assertTrue(list.size() == 0);
    }
}

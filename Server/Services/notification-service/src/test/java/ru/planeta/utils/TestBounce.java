package ru.planeta.utils;

import org.junit.Ignore;
import org.junit.Test;
import ru.planeta.commons.web.WebUtils;
import ru.planeta.test.AbstractTest;

import java.io.ByteArrayInputStream;
import java.nio.charset.StandardCharsets;

/**
 * Created with IntelliJ IDEA.
 * User: michail
 * Date: 19.05.16
 * Time: 18:07
 */
public class TestBounce extends AbstractTest {

    @Ignore
    @Test
    public void testAmazonBounce() {
        String url = "http://localhost:8183/bounce.json";
        String body = "{" +
                "  \"Type\" : \"Notification\"," +
                "  \"MessageId\" : \"91bc2d4c-21cf-5b91-b4ff-6284ec3f4eeb\"," +
                "  \"TopicArn\" : \"arn:aws:sns:eu-west-1:139488442941:bounce\"," +
                "  \"Message\" : \"{\\\"notificationType\\\":\\\"Bounce\\\",\\\"bounce\\\":{\\\"bounceType\\\":\\\"Permanent\\\",\\\"bounceSubType\\\":\\\"General\\\",\\\"bouncedRecipients\\\":[{\\\"emailAddress\\\":\\\"asdfasf@asdfsssa.dda\\\",\\\"action\\\":\\\"failed\\\",\\\"status\\\":\\\"5.4.4\\\",\\\"diagnosticCode\\\":\\\"smtp; 550 5.4.4 Invalid domain\\\"}],\\\"timestamp\\\":\\\"2016-06-01T11:49:04.290Z\\\",\\\"feedbackId\\\":\\\"010201550bcba819-7c167d24-ca5a-4f6f-8027-6d89f11e3773-000000\\\",\\\"reportingMTA\\\":\\\"dsn; a7-20.smtp-out.eu-west-1.amazonses.com\\\"},\\\"mail\\\":{\\\"timestamp\\\":\\\"2016-06-01T11:49:03.000Z\\\",\\\"source\\\":\\\"noreply@planeta.ru\\\",\\\"sourceArn\\\":\\\"arn:aws:ses:eu-west-1:139488442941:identity/planeta.ru\\\",\\\"sendingAccountId\\\":\\\"139488442941\\\",\\\"messageId\\\":\\\"010201550bcba6aa-2f864173-0db6-40ad-a698-2640cfab8ebf-000000\\\",\\\"destination\\\":[\\\"asdfasf@asdfsssa.dda\\\"]}}\"," +
                "  \"Timestamp\" : \"2016-06-01T11:49:04.341Z\"," +
                "  \"SignatureVersion\" : \"1\"," +
                "  \"Signature\" : \"ECW6F9ih6FBlwKJOPoFRdkUChrsLwIpW8ucBettzmGYKzj9w77z3Z/0VvpIkxoZHq9wmrat8rfjBKWmqp77A601UwcLlHYfxUdKIZWQQvOLFFs3MlPH4++DiU4RmOV4C7WuhSN7o9bjw1gDYOELpxXeOOzTE3/ALoSzLQ8mL57JZu1noNcfo1kTnIOuct+Vp3XbwgZs2sChNUX70fEV+VvkIDJXOybcU4Ywzchg3pSjYoBvjELmLzDCfcQcvey04PvIDCvyBOFmBnEliAqyJJSwZjRnjcXz0ppqc+kyfQxKJnAFdO52Kugz41CkzryA7VXHOtRDKqhvuCkobQq/S5w==\"," +
                "  \"SigningCertURL\" : \"https://sns.eu-west-1.amazonaws.com/SimpleNotificationService-bb750dd426d95ee9390147a5624348ee.pem\"," +
                "  \"UnsubscribeURL\" : \"https://sns.eu-west-1.amazonaws.com/?Action=Unsubscribe&SubscriptionArn=arn:aws:sns:eu-west-1:139488442941:bounce:3a666eb2-678f-4651-901c-3e0999946f12\"" +
                "}";
        String message = "{\"notificationType\":\"Bounce\",\"bounce\":{\"bounceType\":\"Permanent\",\"bounceSubType\":\"General\",\"bouncedRecipients\":[{\"emailAddress\":\"asdfasf@asdfsssa.dda\",\"action\":\"failed\",\"status\":\"5.4.4\",\"diagnosticCode\":\"smtp; 550 5.4.4 Invalid domain\"}],\"timestamp\":\"2016-06-01T11:49:04.290Z\",\"feedbackId\":\"010201550bcba819-7c167d24-ca5a-4f6f-8027-6d89f11e3773-000000\",\"reportingMTA\":\"dsn; a7-20.smtp-out.eu-west-1.amazonses.com\"},\"mail\":{\"timestamp\":\"2016-06-01T11:49:03.000Z\",\"source\":\"noreply@planeta.ru\",\"sourceArn\":\"arn:aws:ses:eu-west-1:139488442941:identity/planeta.ru\",\"sendingAccountId\":\"139488442941\",\"messageId\":\"010201550bcba6aa-2f864173-0db6-40ad-a698-2640cfab8ebf-000000\",\"destination\":[\"asdfasf@asdfsssa.dda\"]}}";

        // subscription: curl -H "Content-Type: text/plain" -X POST -d "{\"Type\" : \"SubscriptionConfirmation\", \"SubscribeURL\" : \"https://planeta.ru/api/util/jetty-port.json\"}" http://localhost:8183/bounce.json
        // bounce message: curl -H "Content-Type: text/plain" -X POST -d "{\"Type\":\"Notification\",\"Message\":{\"notificationType\":\"Bounce\",\"mail\":{\"messageId\":\"qwerty\",\"destination\":[\"michail.michail@gmail.com\"]}}}" http://localhost:8183/bounce.json
        // bounce message: curl -H "Content-Type: text/plain" -X POST -d "{\"Type\" : \"Notification\",\"MessageId\" : \"91bc2d4c-21cf-5b91-b4ff-6284ec3f4eeb\",\"TopicArn\" : \"arn:aws:sns:eu-west-1:139488442941:bounce\",\"Message\" :"{\"notificationType\":\"Bounce\",\"bounce\":{\"bounceType\":\"Permanent\",\"bounceSubType\":\"General\",\"bouncedRecipients\":[{\"emailAddress\":\"asdfasf@asdfsssa.dda\",\"action\":\"failed\",\"status\":\"5.4.4\",\"diagnosticCode\":\"smtp; 550 5.4.4 Invalid domain\"}],\"timestamp\":\"2016-06-01T11:49:04.290Z\",\"feedbackId\":\"010201550bcba819-7c167d24-ca5a-4f6f-8027-6d89f11e3773-000000\",\"reportingMTA\":\"dsn; a7-20.smtp-out.eu-west-1.amazonses.com\"},\"mail\":{\"timestamp\":\"2016-06-01T11:49:03.000Z\",\"source\":\"noreply@planeta.ru\",\"sourceArn\":\"arn:aws:ses:eu-west-1:139488442941:identity/planeta.ru\",\"sendingAccountId\":\"139488442941\",\"messageId\":\"010201550bcba6aa-2f864173-0db6-40ad-a698-2640cfab8ebf-000000\",\"destination\":[\"asdfasf@asdfsssa.dda\"]}}",\"Timestamp\" : \"2016-06-01T11:49:04.341Z\",\"SignatureVersion\" : \"1\",\"Signature\" : \"ECW6F9ih6FBlwKJOPoFRdkUChrsLwIpW8ucBettzmGYKzj9w77z3Z/0VvpIkxoZHq9wmrat8rfjBKWmqp77A601UwcLlHYfxUdKIZWQQvOLFFs3MlPH4++DiU4RmOV4C7WuhSN7o9bjw1gDYOELpxXeOOzTE3/ALoSzLQ8mL57JZu1noNcfo1kTnIOuct+Vp3XbwgZs2sChNUX70fEV+VvkIDJXOybcU4Ywzchg3pSjYoBvjELmLzDCfcQcvey04PvIDCvyBOFmBnEliAqyJJSwZjRnjcXz0ppqc+kyfQxKJnAFdO52Kugz41CkzryA7VXHOtRDKqhvuCkobQq/S5w==\",\"SigningCertURL\" :\"https://sns.eu-west-1.amazonaws.com/SimpleNotificationService-bb750dd426d95ee9390147a5624348ee.pem\",\"UnsubscribeURL\" :\"https://sns.eu-west-1.amazonaws.com/?Action=Unsubscribe&SubscriptionArn=arn:aws:sns:eu-west-1:139488442941:bounce:3a666eb2-678f-4651-901c-3e0999946f12\"}" http://localhost:8183/bounce.json

        //String bodySimple = "{\"notificationType\":\"Bounce\",\"mail\":{\"messageId\":\"00000137860315fd-34208509-5b74-41f3-95c5-22c1edc3c924-000000\"}}";
        byte[] respStream = WebUtils.upload(url, new ByteArrayInputStream(body.getBytes(StandardCharsets.UTF_8)), "text/plain", false);
    }
}

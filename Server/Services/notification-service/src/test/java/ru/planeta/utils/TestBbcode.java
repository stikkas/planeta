package ru.planeta.utils;

import org.junit.Test;
import ru.planeta.commons.text.Bbcode;
import ru.planeta.test.AbstractTest;

import static org.junit.Assert.assertEquals;

/**
 * @author ds.kolyshev
 * Date: 18.01.12
 */
public class TestBbcode extends AbstractTest {
    @Test
	public void testTransform() {

//		String customMailTagsText = "[h1]HeaderText1[/h1][p]PText...[/p][p]Ptext2[/p][h2]HeaderText2[/h2][foot]FooterText[/foot][li]LiText[/li]";
//		String transform = Bbcode.transform(customMailTagsText);
//		assertEquals("<h1 style=\"font-size:22px;margin-bottom:12px;margin-top:0;font-weight:normal;\">HeaderText1</h1><p style=\"margin-top:0; margin-bottom:6px;\" >PText...</p><p style=\"margin-top:0; margin-bottom:6px;\" >Ptext2</p><h2 style=\"font-size:18px;margin-bottom:0px;margin-top:0;font-weight:normal;\">HeaderText2</h2><table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"border-collapse: collapse; width: 100%;\"><tr height=\"24\" style=\"height: 24px;\"><td><img src=\"cid:pixel\" height=\"24\"/></td></tr><tr><td><font face=\"arial\" style=\"font-size: 12px; line-height: 18px;\" color=\"#666\"><i>FooterText</i></font></td></tr><tr height=\"32\" style=\"height: 32px;\"><td><img src=\"cid:pixel\" height=\"32\"/></td></tr></table><li><font color=\"#303030\">LiText</font></li>", transform);
        String simpleBBCodeTagsText = "[h1]Header[/h1]";
        String transform = Bbcode.transform(simpleBBCodeTagsText);
        assertEquals("<div style=\"line-height:1.2; font-size:1.4em; font-weight: 700;\">Header</div>".replaceAll("\\s", " ") , transform.replaceAll("(?sm)\\s+", " ").trim());
	}
}

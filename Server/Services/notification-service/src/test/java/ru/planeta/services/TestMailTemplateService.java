package ru.planeta.services;

import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import ru.planeta.model.mail.MailMessage;
import ru.planeta.model.mail.MailMessagePriority;
import ru.planeta.model.mail.MailAttachment;
import ru.planeta.model.mail.MailTemplate;
import ru.planeta.test.AbstractTest;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.zip.ZipOutputStream;

import static org.junit.Assert.*;

/**
 * @author ds.kolyshev
 *         Date: 12.01.12
 */
public class TestMailTemplateService extends AbstractTest {

	@Autowired
	private MailTemplateService mailTemplateService;
	@Autowired
	private MailAttachmentService mailAttachmentService;

    @Test
    public void testAddUpdateRemove() {
        
        
            MailTemplate mailTemplate = createMailTemplate();
            mailTemplateService.saveMailTemplate(mailTemplate);
            assertTrue(mailTemplate.getTemplateId() > 0);

            List<MailTemplate> list = mailTemplateService.getMailTemplates();
            assertNotNull(list);
            assertTrue(list.size() > 0);

            MailTemplate selected = mailTemplateService.getMailTemplate(mailTemplate.getTemplateId());
            assertTemplateEquals(selected, mailTemplate);

            mailTemplate.setContentBbcode(mailTemplate.getContentBbcode() + " 2");
            mailTemplate.setFromAddress(mailTemplate.getFromAddress() + " 2");
            mailTemplate.setName(mailTemplate.getName() + " 2");
            mailTemplate.setSubject(mailTemplate.getSubject() + " 2");

            mailTemplateService.saveMailTemplate(mailTemplate);

            MailTemplate updated = mailTemplateService.getMailTemplate(mailTemplate.getTemplateId());
            assertTemplateEquals(updated, mailTemplate);

            mailTemplateService.deleteMailTemplate(mailTemplate.getTemplateId());
            assertNull(mailTemplateService.getMailTemplate(mailTemplate.getTemplateId()));

        
    }

    @Test
    @Ignore
    public void testEnqueueEmail() {
        mailTemplateService.enqueueEmail(new MailMessage("noreply@planeta.ru", "", Collections.singletonList("michail.michail@gmail.com"),
                "test subj", "test cont", null, false, MailMessagePriority.LOW));
        mailTemplateService.enqueueEmail(new MailMessage("noreply@planeta.ru", "", Collections.singletonList("_DELETED_michail.michail@gmail.com_"),
                "test subj", "test cont", null, false, MailMessagePriority.LOW));
        mailTemplateService.enqueueEmail(new MailMessage("noreply@planeta.ru", "", Collections.singletonList(""),
                "test subj", "test cont", null, false, MailMessagePriority.LOW));
    }

    @Test
    @Ignore
    public void testSendEmail() throws Exception {
            MailTemplate mailTemplate = createMailTemplate();
            mailTemplateService.saveMailTemplate(mailTemplate);
            assertTrue(mailTemplate.getTemplateId() > 0);

            MailAttachment mailAttachment = createMailAttachment();
            mailAttachment.setTemplateId(mailTemplate.getTemplateId());
            mailAttachmentService.saveMailAttachment(mailAttachment);

            mailTemplateService.sendEmail(mailTemplate.getName(), new HashMap<String, String>() {{
                put("userEmail", "michail.michail@gmail.com");
            }}, MailMessagePriority.DEFAULT);

            mailTemplateService.deleteMailTemplate(mailTemplate.getTemplateId());
            assertNull(mailTemplateService.getMailTemplate(mailTemplate.getTemplateId()));

            // Uncomment this if your want to wait MailQueue for real send
            Thread.currentThread().sleep(8000);

    }

    private void assertTemplateEquals(MailTemplate actual, MailTemplate expected) {
        assertEquals(expected.getTemplateId(), actual.getTemplateId());
        assertEquals(expected.getContentBbcode(), actual.getContentBbcode());
        assertEquals(expected.getFromAddress(), actual.getFromAddress());
        assertEquals(expected.getName(), actual.getName());
        assertEquals(expected.getSubject(), actual.getSubject());
    }

    private MailTemplate createMailTemplate() {
        MailTemplate mailTemplate = new MailTemplate();
        mailTemplate.setContentBbcode("content bbcode test");
        mailTemplate.setReplyToAddress("support@planeta.ru");
        mailTemplate.setFromAddress("noreply@planeta.ru");
        mailTemplate.setToAddress("${userEmail}");
        mailTemplate.setName("test template name");
        mailTemplate.setSubject("test mail subject");
        return mailTemplate;
    }

    private MailAttachment createMailAttachment() {
        MailAttachment mailAttachment = new MailAttachment();
        mailAttachment.setFileName("проверка.zip");
        mailAttachment.setMimeType("application/zip");
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        try {
            ZipOutputStream zos = new ZipOutputStream(bos);
            zos.close();
        } catch (IOException ignored) {
			//exception ignored
		}
        mailAttachment.setContent(bos.toByteArray());
        return mailAttachment;
    }
}

package ru.planeta.services;

import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.task.TaskExecutor;
import ru.planeta.model.mail.MailMessage;
import ru.planeta.model.mail.MailMessagePriority;
import ru.planeta.model.MailMessageQueue;
import ru.planeta.test.AbstractTest;

import java.util.Collections;
import java.util.HashMap;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import static org.junit.Assert.*;

/**
 * Created with IntelliJ IDEA.
 * User: michail
 * Date: 06.12.2016
 * Time: 17:42
 */
public class MailQueueTest extends AbstractTest {
    @Autowired
    private MailTemplateService mailTemplateService;

    @Autowired
    private MailQueueImpl mailQueue;


    private static MailMessage createMailMessage(long id, MailMessagePriority priority) {
        return new MailMessage(id, "", "", Collections.singletonList(""), "", "", null, false, priority);
    }

    @Test
    public void MailQueueAddAndPullTest() {
        MailMessageQueue queue = new MailMessageQueue();
        queue.add(createMailMessage(1, MailMessagePriority.LOW));
        queue.add(createMailMessage(2, MailMessagePriority.LOW));
        queue.add(createMailMessage(3, MailMessagePriority.LOW));
        queue.add(createMailMessage(4, MailMessagePriority.DEFAULT));
        queue.add(createMailMessage(5, MailMessagePriority.LOW));
        queue.add(createMailMessage(6, MailMessagePriority.HIGH));
        queue.add(createMailMessage(7, MailMessagePriority.LOW));
        queue.add(createMailMessage(8, MailMessagePriority.LOW));
        queue.add(createMailMessage(9, MailMessagePriority.HIGH));
        queue.add(createMailMessage(10, MailMessagePriority.LOW));
        queue.add(createMailMessage(11, MailMessagePriority.LOW));
        queue.add(createMailMessage(12, MailMessagePriority.HIGH));
        queue.add(createMailMessage(13, MailMessagePriority.LOW));
        queue.add(createMailMessage(14, MailMessagePriority.LOW));

        MailMessage message = queue.poll();
        assertEquals(message.getPriority(), MailMessagePriority.HIGH);
        assertEquals(6, message.getMessageId());
        message = queue.poll();
        assertEquals(message.getPriority(), MailMessagePriority.HIGH);
        assertEquals(9, message.getMessageId());
        message = queue.poll();
        assertEquals(message.getPriority(), MailMessagePriority.HIGH);
        assertEquals(12, message.getMessageId());
        message = queue.poll();
        assertEquals(message.getPriority(), MailMessagePriority.DEFAULT);
        message = queue.poll();
        assertEquals(message.getPriority(), MailMessagePriority.LOW);
    }

    @Test
    @Ignore
    public void MailQueueAddAndSendTest() {
        final String templateName = "test.template";
        HashMap<String, String> paramsLow = new HashMap<>();
        paramsLow.put("userEmail", "dev@planeta.ru");
        paramsLow.put("text", "LOW");
        HashMap<String, String> paramsDefault = new HashMap<>();
        paramsDefault.put("userEmail", "dev@planeta.ru");
        paramsDefault.put("text", "DEFAULT");
        HashMap<String, String> paramsHigh = new HashMap<>();
        paramsHigh.put("userEmail", "dev@planeta.ru");
        paramsHigh.put("text", "HIGH");

        for (int i=0; i<20; i++) {
            mailTemplateService.sendEmail(templateName, paramsLow, MailMessagePriority.LOW);
        }
        for (int i=0; i<20; i++) {
            mailTemplateService.sendEmail(templateName, paramsHigh, MailMessagePriority.HIGH);
        }

        mailQueue.sendAll();
    }
}

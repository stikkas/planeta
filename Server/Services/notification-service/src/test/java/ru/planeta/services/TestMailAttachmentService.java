package ru.planeta.services;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import ru.planeta.api.exceptions.NotFoundException;
import ru.planeta.model.mail.MailAttachment;
import ru.planeta.test.AbstractTest;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.zip.ZipOutputStream;

import static org.junit.Assert.*;

/**
 * @author ds.kolyshev
 * Date: 12.01.12
 */
public class TestMailAttachmentService extends AbstractTest {

	private static final int TEMPLATE_ID = 998998;

	@Autowired
	private MailAttachmentService mailAttachmentService;

	@Test
	public void testAddUpdateRemove() throws Exception {
			MailAttachment mailAttachment = createMailAttachment();
			mailAttachmentService.saveMailAttachment(mailAttachment);
			assertTrue(mailAttachment.getAttachmentId() > 0);

			List<MailAttachment> list = mailAttachmentService.getMailAttachments(TEMPLATE_ID);
			assertNotNull(list);
			assertFalse(list.isEmpty());

			MailAttachment selected = mailAttachmentService.getMailAttachment(mailAttachment.getAttachmentId());
			assertAttachmentEquals(selected, mailAttachment);

			mailAttachment.setContent((new String(mailAttachment.getContent()) + " 2").getBytes());
			mailAttachment.setFileName(mailAttachment.getFileName() + ".txt");
			mailAttachment.setMimeType("text/plain");
			mailAttachment.setFileName(mailAttachment.getFileName() + " 2");
			mailAttachment.setTemplateId(mailAttachment.getTemplateId() + 1);

			mailAttachmentService.saveMailAttachment(mailAttachment);

			MailAttachment updated = mailAttachmentService.getMailAttachment(mailAttachment.getAttachmentId());
			assertAttachmentEquals(updated, mailAttachment);

			mailAttachmentService.deleteMailAttachment(mailAttachment.getAttachmentId());
			try {
				mailAttachmentService.getMailAttachment(mailAttachment.getAttachmentId());
			} catch (NotFoundException e) {
				return;
			}
			assertTrue(false);

	}

	private void assertAttachmentEquals(MailAttachment actual, MailAttachment expected) {
		assertEquals(expected.getAttachmentId(), actual.getAttachmentId());
		assertEquals(new String(expected.getContent()), new String(actual.getContent()));
		assertEquals(expected.getFileName(), actual.getFileName());
		assertEquals(expected.getMimeType(), actual.getMimeType());
	}

	private static MailAttachment createMailAttachment() {
		MailAttachment mailAttachment = new MailAttachment();
		mailAttachment.setFileName("test.zip");
		mailAttachment.setMimeType("application/zip");
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		try {
			ZipOutputStream zos = new ZipOutputStream(bos);
			zos.close();
		} catch (IOException ignored) {
		}
		mailAttachment.setContent(bos.toByteArray());
		mailAttachment.setTemplateId(TEMPLATE_ID);
		return mailAttachment;
	}
}

package ru.planeta.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import ru.planeta.api.exceptions.NotFoundException;
import ru.planeta.api.exceptions.PermissionException;
import ru.planeta.api.model.ActionStatus;
import ru.planeta.api.service.common.MailMessageService;
import ru.planeta.api.service.profile.ProfileSettingsService;
import ru.planeta.dao.commondb.UserPrivateInfoDAO;
import ru.planeta.dao.profiledb.ProfileDAO;
import ru.planeta.model.common.UserPrivateInfo;
import ru.planeta.model.enums.ProfileStatus;
import ru.planeta.model.mail.MailMessageStatus;
import ru.planeta.model.profile.Profile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.HashMap;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: michail
 * Date: 19.05.16
 * Time: 16:31
 */
@Controller
public class AmazonController {
    // amazon message envelope keys
    private final static String KEY_TYPE = "Type";
    private final static String VALUE_SUBSCR_CONFIRM = "SubscriptionConfirmation";
    private final static String VALUE_NOTIFICATION = "Notification";

    // amazon sns subscriprion message keys and values
    private final static String KEY_SUBSCR_URL = "SubscribeURL";
    private final static String KEY_MESSAGE = "Message";

    // amazon ses sub-message keys and values
    private final static String KEY_NOTIFICATION_TYPE = "notificationType";
    private final static String KEY_MAIL = "mail";
    private final static String KEY_MESSAGE_ID = "messageId";
    private final static String KEY_DESTINATION = "destination";

    @Autowired
    MailMessageService mailMessageService;

    @Autowired
    ProfileSettingsService profileSettingsService;

    @Autowired
    UserPrivateInfoDAO userPrivateInfoDAO;

    @Autowired
    ProfileDAO profileDAO;

    private static final Logger logger = Logger.getLogger(AmazonController.class);

    @RequestMapping(value = Urls.BOUNCE, method = RequestMethod.POST)
    @ResponseBody
    public ActionStatus processRequest(HttpServletRequest request) throws NotFoundException, IOException, PermissionException {
        logger.info("Received notification from Amazon");
        logger.info("Amazon content-type is " + request.getHeader("Content-Type"));

        final String requestString = IOUtils.toString(request.getInputStream());
        logger.info("Amazon request is " + requestString);

        final HashMap requestMap = new ObjectMapper().readValue(requestString, HashMap.class);

        if (requestMap == null) {
            throw new NotFoundException("Amazon request is empty.");
        }

        final String type = (String) requestMap.get(KEY_TYPE);
        if (type == null) {
            throw new NotFoundException("Amazon request has no type.");
        }

        if (VALUE_SUBSCR_CONFIRM.equals(type)) {
            return processConfirmRequest(requestMap);
        } else if (VALUE_NOTIFICATION.equals(type)) {
            final String message = (String) requestMap.get(KEY_MESSAGE);
            logger.info("Amazon message body is " + message);
            return processNotificationRequest(new ObjectMapper().readValue(message, HashMap.class));
        } else {
            throw new NotFoundException("Amazon request type " + type + " is not supported.");
        }
    }

    // for details, see http://docs.aws.amazon.com/sns/latest/dg/SendMessageToHttp.html
    private ActionStatus processConfirmRequest(HashMap requestMap) throws NotFoundException, IOException {
        final String url = (String) requestMap.get(KEY_SUBSCR_URL);
        if (url == null) {
            throw new NotFoundException("Confirmation url is empty.");
        }
        logger.info("Confirmation url is " + url);

        InputStream inputStream = (new URL(url)).openStream();
        try {
            logger.info("Confirmation info is " + IOUtils.toString(inputStream));
        } finally {
            IOUtils.closeQuietly(inputStream);
        }

        return ActionStatus.createSuccessStatus();
    }

    // for details, see http://docs.aws.amazon.com/ses/latest/DeveloperGuide/notification-examples.html
    private ActionStatus processNotificationRequest(HashMap message) throws NotFoundException, PermissionException {
        final String notificationType = (String) message.get(KEY_NOTIFICATION_TYPE);
        if (notificationType == null) {
            throw new NotFoundException("No notification type info present");
        }

        MailMessageStatus newStatus = MailMessageStatus.valueOf(notificationType.toUpperCase());
        logger.info("Received notification has type " + notificationType);

        HashMap mailMap = (HashMap) message.get(KEY_MAIL);
        if (mailMap == null) {
            throw new NotFoundException("No mail section present");
        }

        final String messageId = (String) mailMap.get(KEY_MESSAGE_ID);
        if (messageId == null) {
            throw new NotFoundException("No message id present");
        }

        final List<String> emailList = (List<String>) mailMap.get(KEY_DESTINATION);
        if (emailList == null || emailList.isEmpty()) {
            throw new NotFoundException("No email list present for messageId " + messageId);
        }

        try {
            mailMessageService.setStatusByExtMsgId(messageId, newStatus);
            logger.info("MessageId " + messageId + " mark as bounced.");
        } catch (Exception ex) {
            logger.error(ex);
        }

        if (newStatus != MailMessageStatus.DELIVERY) {
            for (String email : emailList) {
                if (org.apache.commons.lang3.StringUtils.isEmpty(email)) {
                    continue;
                }
                if (email.endsWith("@planeta.ru")) {
                    logger.info("User " + email + " detected as planeta user. Unsubscription skipped");
                    continue;
                }
                UserPrivateInfo user = userPrivateInfoDAO.selectByEmail(email);
                if (user != null) {
                    profileSettingsService.unSubscribeUserFromAllMailNotificationsUnsafe(user.getUserId());
                    Profile profile = profileDAO.selectById(user.getUserId());
                    if (profile != null) {
                        profile.setStatus(ProfileStatus.USER_BOUNCED);
                        profileDAO.update(profile);
                        logger.info("Profile " + profile.getProfileId() + " with " + email + " now has status USER_BOUNCED");
                    } else {
                        logger.warn("Profile with " + email + " not found. Status updating skipped.");
                    }
                    logger.info("User " + email + " unsubscribed from all emails.");
                } else {
                    logger.warn("User " + email + " not found. Unsubscribing skipped.");
                }
            }
        }
        return ActionStatus.createSuccessStatus();
    }


    @RequestMapping(value = Urls.OPEN_PIXEL, method = RequestMethod.GET)
    @ResponseBody
    public void mailWasOpened(@RequestParam(value = "id") Long messageId, HttpServletResponse response) throws Exception {
        response.setContentType("image/png");
        response.setContentLength(0);
        response.setStatus(HttpServletResponse.SC_OK);
        mailMessageService.saveOpenedMailInfo(messageId);
    }
}





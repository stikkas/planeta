package ru.planeta.services;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;
import ru.planeta.api.aspect.transaction.Transactional;
import ru.planeta.api.utils.ValidateUtils;
import ru.planeta.commons.text.Bbcode;
import ru.planeta.dao.commondb.MailTemplateDAO;
import ru.planeta.dao.commondb.UserPrivateInfoDAO;
import ru.planeta.dao.profiledb.ProfileDAO;
import ru.planeta.model.common.UserPrivateInfo;
import ru.planeta.model.enums.ProfileStatus;
import ru.planeta.model.enums.UserStatus;
import ru.planeta.model.mail.MailAttachment;
import ru.planeta.model.mail.MailMessage;
import ru.planeta.model.mail.MailMessagePriority;
import ru.planeta.model.mail.MailTemplate;
import ru.planeta.model.profile.Profile;

import javax.activation.DataSource;
import javax.activation.MimetypesFileTypeMap;
import javax.annotation.PostConstruct;
import javax.mail.util.ByteArrayDataSource;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

/**
 * @author ds.kolyshev
 *         Date: 12.01.12
 */
@Service
@Transactional
public class MailTemplateServiceImpl implements MailTemplateService {

    private static final Logger log = Logger.getLogger(MailTemplateServiceImpl.class);

    @Autowired
    private MailTemplateDAO mailTemplateDAO;
    @Autowired
    private MailQueue mailQueue;
    @Autowired
    private UserPrivateInfoDAO userPrivateInfoDAO;
    @Autowired
    private ProfileDAO profileDAO;

    private String mailTemplateContent;
    private String mailTemplateFooter;
    private String mailTemplateHeader;

    private static final String SECRET_KEY = "CFE628B652611CB8";

    @Value("classpath:template/mailTemplateContent.html")
    public void setMailTemplateContent(Resource resource) throws IOException {
        this.mailTemplateContent = IOUtils.toString(resource.getInputStream(), "UTF-8");
    }

    @Value("classpath:template/mailTemplateFooter.html")
    public void setMailTemplateFooter(Resource resource) throws IOException {
        this.mailTemplateFooter = IOUtils.toString(resource.getInputStream(), "UTF-8");
    }

    @Value("classpath:template/mailTemplateHeader.html")
    public void setMailTemplateHeader(Resource resource) throws IOException {
        this.mailTemplateHeader = IOUtils.toString(resource.getInputStream(), "UTF-8");
    }

    @PostConstruct
    public void initMultipart() throws IOException {
        MimetypesFileTypeMap typeMap = new MimetypesFileTypeMap();
        typeMap.addMimeTypes("image/gif gif GIF");
        typeMap.addMimeTypes("image/png png PNG");
        typeMap.addMimeTypes("image/jpeg jpg jpeg JPG JPEG");
    }

    @Override
    public List<MailTemplate> getMailTemplates() {
        return mailTemplateDAO.selectMailTemplates();
    }

    @Override
    public MailTemplate getMailTemplate(int templateId) {
        return mailTemplateDAO.select(templateId);
    }

    @Override
    public MailTemplate getMailTemplate(String templateName) {
        return mailTemplateDAO.selectByName(templateName);
    }

    @Override
    public void saveMailTemplate(MailTemplate mailTemplate) {
        if (mailTemplate.getTemplateId() == null || mailTemplate.getTemplateId() == 0) {
            mailTemplateDAO.insert(mailTemplate);
        } else {
            mailTemplateDAO.update(mailTemplate);
        }
    }

    @Override
    public void deleteMailTemplate(int templateId) {
        mailTemplateDAO.delete(templateId);
    }

    private String getContent(MailTemplate mailTemplate, Map<String, String> parameters) {
        if (StringUtils.isBlank(mailTemplate.getContentHtml())) {
            return mailTemplateHeader
                    + mailTemplateContent.replace("${content}", Bbcode.transform(replaceParameters(mailTemplate.getContentBbcode(), parameters)))
                    + mailTemplateFooter
                    ;
        }
        if (mailTemplate.isUseFooterAndHeader()) {
            return mailTemplateHeader
                    + mailTemplate.getContentHtml()
                    + mailTemplateFooter
                    ;

        }
        return mailTemplate.getContentHtml();
    }

    @Override
    public String getPreviewContentHtml(int templateId) {
        MailTemplate template = mailTemplateDAO.select(templateId);

        String content = getContent(template, null);
        content = content.replace("${subject}", template.getSubject());
        content = content.replaceAll(Pattern.quote("${appHost}"), "#");
        return content;
    }

    @Override
    public void sendEmail(String templateName, Map<String, String> parameters, MailMessagePriority priority) {
        String id = (templateName == null ? "null" : templateName) + "_" + System.currentTimeMillis();
        logDebug(id, "prepare email " + templateName);
        logDebug(id, "priority is " + priority);
        try {
            sendEmailInner(templateName, parameters, priority);
        } catch (Exception e) {
            log.error("Mail message send " + id + " error while preparing email" + ". Exception: ", e);
        }
    }

    private void sendEmailInner(String templateName, Map<String, String> parameters, MailMessagePriority priority) throws Exception {

        MailTemplate mailTemplate = mailTemplateDAO.selectByName(templateName);
        if (mailTemplate == null) {
            throw new Exception("Template " + templateName + " not found!");
        }

        // generate unsubscr code
        long userId = 0L;
        String unsubscribe = StringUtils.EMPTY;
        if (StringUtils.isNotBlank(parameters.get("userEmail"))) {
            UserPrivateInfo userPrivateInfo = userPrivateInfoDAO.selectByEmail(parameters.get("userEmail"));
            if (userPrivateInfo != null) {
                userId = userPrivateInfo.getUserId();
                unsubscribe = DigestUtils.md5Hex(SECRET_KEY + userId);
            }
        }

        String subject = ("true".equalsIgnoreCase(parameters.get("isSpam")) ? "[WARNING]" : "") + replaceParameters(mailTemplate.getSubject(), parameters);

        Map<String, DataSource> files = new HashMap<>();

        if (mailTemplate.getAttachments() != null) {
            for (MailAttachment attachment : mailTemplate.getAttachments()) {
                InputStream is = new ByteArrayInputStream(attachment.getContent());
                ByteArrayDataSource ds = new ByteArrayDataSource(is, attachment.getMimeType());
                ds.setName(attachment.getFileName());
                files.put(attachment.getFileName(), ds);
            }
        }

        String contentHtml = getContent(mailTemplate, parameters);

        contentHtml = replaceParameters(contentHtml, parameters);
        contentHtml = contentHtml.replace("${subject}", subject);
        contentHtml = contentHtml.replace("${unsubscribe}", unsubscribe);
        contentHtml = contentHtml.replace("${userId}", String.valueOf(userId));


        String mailFrom = replaceParameters(mailTemplate.getFromAddress(), parameters);
        String mailReplyTo = mailTemplate.getReplyToAddress() != null ? replaceParameters(mailTemplate.getReplyToAddress(), parameters) : null;
        final String mailTo;
        if (StringUtils.isBlank(mailTemplate.getToAddress())) {
            mailTo = parameters.get("userEmail");
        } else {
            mailTo = replaceParameters(mailTemplate.getToAddress(), parameters);
        }

        if (StringUtils.isBlank(mailTo)) {
            throw new Exception("mailTo is blank");
        }

        contentHtml = contentHtml.replace("${mailTo}", mailTo);

        List<String> toList = new ArrayList<>();

        // get valid adresseeees
        for(String toAddress : mailTo.split("\\s*,\\s*")) {
            if(StringUtils.isNotBlank(toAddress)) {
                if (!"true".equalsIgnoreCase(parameters.get("skipValidate"))) {
                    if (validateEmail(toAddress)) {
                        toList.add(toAddress);
                    } else {
                        log.warn("MailTemplateService: profile with email " + mailTo + " not found or bounced.");
                    }
                } else {
                    toList.add(toAddress);
                    log.info("Email for " + toAddress + " contains skipValidate flag. Validation skipped");
                }
            }
        }

        if (!toList.isEmpty()) {
            MailMessage mailMessage = new MailMessage(mailFrom, mailReplyTo, toList, subject, contentHtml, files, mailTemplate.isSkipMessageLogging(), priority);
            mailQueue.enqueue(mailMessage);
        } else {
            log.warn("Recepients list is empty.");
        }
    }

    private boolean validateEmail(String mailTo) {
        if(StringUtils.isBlank(mailTo) || !ValidateUtils.isValidEmail(mailTo)) {
            return false;
        }

        if (ValidateUtils.isPlanetaEmail(mailTo)) {
            log.info(mailTo + " is recognized as planeta email. Validating skipped");
            return true;
        }

        UserPrivateInfo userPrivateInfo = userPrivateInfoDAO.selectByEmail(mailTo);
        if (userPrivateInfo != null) {
            if (userPrivateInfo.getUserStatus().contains(UserStatus.DELETED)) {
                return false;
            }
            long userId = userPrivateInfo.getUserId();
            Profile profile = profileDAO.selectById(userId);
            if(profile != null && profile.getStatus() == ProfileStatus.USER_BOUNCED) {
                return false;
            }
        }
        return true;
    }

    private static void logDebug(String id, String message) {
        log.debug("Mail message send " + id + " " + message);
    }

    private static String replaceParameters(String str, Map<String, String> parameters) {
        if (parameters == null) {
            return str;
        }
        String content = str;

        for (Map.Entry<String, String> entry : parameters.entrySet()) {
            content = content.replace("${" + entry.getKey() + "}", entry.getValue());
        }

        return content;
    }


    @Override
    public void enqueueEmail(MailMessage mailMessage) {
        log.debug("prepare email to " + mailMessage.getToList());
        log.debug("priority is " + mailMessage.getPriority());
        ArrayList<String> toList = new ArrayList<>();
        for (String to : mailMessage.getTo()) {
            if (validateEmail(to)) {
                toList.add(to);
            }
        }
        if (!toList.isEmpty()) {
            mailMessage.setTo(toList);
            mailQueue.enqueue(mailMessage);
        } else {
            log.error("Recepients list is empty." + mailMessage.getSubject());
        }
    }
}

package ru.planeta.services;

import ru.planeta.model.mail.MailMessage;

/**
 * @author ds.kolyshev
 * Date: 12.01.12
 */
public interface MailQueue {
    void enqueue(MailMessage mailMessage);
}

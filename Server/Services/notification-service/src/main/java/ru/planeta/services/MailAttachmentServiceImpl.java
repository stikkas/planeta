package ru.planeta.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.web.multipart.MultipartFile;
import ru.planeta.api.aspect.transaction.Transactional;
import ru.planeta.api.exceptions.NotFoundException;
import ru.planeta.dao.commondb.MailAttachmentDAO;
import ru.planeta.model.mail.MailAttachment;

import java.io.IOException;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: s.fionov
 * Date: 14.08.12
 */
@Service
@Transactional
public class MailAttachmentServiceImpl implements MailAttachmentService {

    @Autowired
    private MailAttachmentDAO mailAttachmentDAO;

    @Override
    public List<MailAttachment> getMailAttachments(int templateId) {
        return mailAttachmentDAO.selectMailAttachments(templateId);
    }

    @Override
    public MailAttachment getMailAttachment(long attachmentId) throws NotFoundException {
        MailAttachment mailAttachment = mailAttachmentDAO.select(attachmentId);
        if (mailAttachment == null) {
            throw new NotFoundException(MailAttachment.class, attachmentId);
        }
        return mailAttachment;
    }

    @Override
    public void saveMailAttachment(MailAttachment mailAttachment) {
        if (mailAttachment.getAttachmentId() == null || mailAttachment.getAttachmentId() == 0) {
            mailAttachmentDAO.insert(mailAttachment);
        } else {
            mailAttachmentDAO.update(mailAttachment);
        }
    }

    @Override
    public void deleteMailAttachment(long attachmentId) {
        mailAttachmentDAO.delete(attachmentId);
    }

    @Override
    public MailAttachment saveMailAttachmentFromUploadFile(MultipartFile file, int templateId) throws IOException {
        MailAttachment mailAttachment = new MailAttachment();
        mailAttachment.setTemplateId(templateId);
        mailAttachment.setFileName(file.getOriginalFilename());
        mailAttachment.setMimeType(file.getContentType());
        mailAttachment.setContent(file.getBytes());
        saveMailAttachment(mailAttachment);
        return mailAttachment;
    }

    @Override
    public void copyAttachment(MailAttachment attachment, int templateId) {
        MailAttachment newAttachment = new MailAttachment();
        newAttachment.setTemplateId(templateId);
        newAttachment.setFileName(attachment.getFileName());
        newAttachment.setMimeType(attachment.getMimeType());
        newAttachment.setContent(attachment.getContent().clone());
        saveMailAttachment(newAttachment);
    }

    @Override
    public void copyAllAttachments(int templateId, int targetTemplateId) {
        List<MailAttachment> attachments = getMailAttachments(templateId);
        if (!CollectionUtils.isEmpty(attachments)) {
            for (MailAttachment attachment : attachments) {
                copyAttachment(attachment, targetTemplateId);
            }
        }
    }
}

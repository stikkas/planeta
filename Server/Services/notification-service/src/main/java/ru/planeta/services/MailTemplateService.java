package ru.planeta.services;

import ru.planeta.model.mail.MailMessage;
import ru.planeta.model.mail.MailMessagePriority;
import ru.planeta.model.mail.MailTemplate;

import java.util.List;
import java.util.Map;

/**
 * Mail service class
 *
 * @author ds.kolyshev
 *         Date: 12.01.12
 */
public interface MailTemplateService {
    /**
     * Gets list of current mail templates
     *
     */
    List<MailTemplate> getMailTemplates();

    /**
     * Gets specified
     *
     */
    MailTemplate getMailTemplate(int templateId);

    MailTemplate getMailTemplate(String templateName);

    /**
     * Saves or updates mail template
     *
     */
    void saveMailTemplate(MailTemplate mailTemplate);

    /**
     * Deletes mail template
     *
     */
    void deleteMailTemplate(int templateId);

    /**
     * Gets content html for the specified template and parameters
     *
     */
    String getPreviewContentHtml(int templateId);

    /**
     * Sends email using specified template
     * if value of switchSender is true, it send from email to email specified in template
     *
     */
    void sendEmail(String templateName, Map<String, String> parameters, MailMessagePriority priority);

    void enqueueEmail(MailMessage mailMessage);
}

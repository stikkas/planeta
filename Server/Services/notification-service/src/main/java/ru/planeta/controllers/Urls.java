package ru.planeta.controllers;

/**
 * Controllers urls.
 * MailController urls is in ru.planeta.api.mail package
 *
 * Created with IntelliJ IDEA.
 * User: s.fionov
 * Date: 15.08.12
 */
public class Urls {
    public static final String TEMPLATES = "/templates.html";
    public static final String EDIT = "/edit.html";
    public static final String DELETE = "/deleteByProfileId.html";
    public static final String PREVIEW = "/preview.html";
    public static final String GET_ATTACHMENT = "/get-attachment.html";
    public static final String DELETE_ATTACHMENT = "/deleteByProfileId-attachment.html";
    public static final String BOUNCE = "/bounce.json";
    public static final String OPEN_PIXEL = "/email-is-opened";
}


package ru.planeta.model;

import ru.planeta.model.mail.MailMessage;

import java.util.concurrent.Delayed;
import java.util.concurrent.TimeUnit;

public class MailMessageDelay implements Delayed {
    private final MailMessage mailMessage;
    private final long duration;

    public MailMessageDelay(long duration, MailMessage mailMessage) {
        this.duration = System.currentTimeMillis() + duration;
        this.mailMessage = mailMessage;
    }

    @Override
    public int compareTo(Delayed o) {
        return (int) (this.duration - ((MailMessageDelay) o).getDuration());
    }

    @Override
    /*
     * Expiration occurs when an element's getDelay(TimeUnit unit) method
     * returns a value less than or equal to zero.
     */
    public long getDelay(TimeUnit unit) {
        long diff = duration - System.currentTimeMillis();
        return unit.convert(diff, TimeUnit.MILLISECONDS);
    }

    public long getDuration() {
        return duration;
    }

    public MailMessage getMailMessage() {
        return mailMessage;
    }
}

package ru.planeta.model;

import ru.planeta.model.mail.MailMessage;

import java.util.Comparator;
import java.util.concurrent.DelayQueue;
import java.util.concurrent.PriorityBlockingQueue;

/**
 * Created with IntelliJ IDEA.
 * User: michail
 * Date: 06.12.2016
 * Time: 17:29
 */
public class MailMessageQueue  {
    private final PriorityBlockingQueue<MailMessage> queue;
    private final DelayQueue<MailMessageDelay> delayQueue;

    private static final int INITIAL_CAPACITY = 100;

    public MailMessageQueue() {
        queue = new PriorityBlockingQueue<>(INITIAL_CAPACITY, new Comparator<MailMessage>() {
            @Override
            public int compare(MailMessage o1, MailMessage o2) {
                return Integer.compare(o2.getPriority().getCode(), o1.getPriority().getCode());
            }
        });
        delayQueue = new DelayQueue<>();
    }

    public MailMessage take() throws InterruptedException {
        MailMessageDelay mailMessageDelay = delayQueue.poll();
        if (mailMessageDelay != null) {
            return mailMessageDelay.getMailMessage();
        }
        return queue.take();
    }

    public boolean add(MailMessage message) {
        return  queue.add(message);
    }

    public boolean addErrorMessage(MailMessage message) {
        MailMessageDelay mailMessageDelay = new MailMessageDelay(30000 * message.getAttemptsCount(), message);
        return  delayQueue.add(mailMessageDelay);
    }

    public MailMessage poll() {
        return queue.poll();
    }
}

package ru.planeta.controllers;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;
import ru.planeta.model.MailTemplateInfo;
import ru.planeta.model.mail.MailTemplate;
import ru.planeta.services.MailAttachmentService;
import ru.planeta.services.MailTemplateService;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.IOException;

/**
 * @author ds.kolyshev
 *         Date: 18.01.12
 */
@Controller
public class SettingsController {
    private final MailTemplateService mailTemplateService;
    private final MailAttachmentService mailAttachmentService;

    @Autowired
    public SettingsController(MailTemplateService mailTemplateService, MailAttachmentService mailAttachmentService) {
        this.mailTemplateService = mailTemplateService;
        this.mailAttachmentService = mailAttachmentService;
    }

    @RequestMapping(value = Urls.TEMPLATES, method = RequestMethod.GET)
    public ModelAndView viewTemplates() {
        ModelAndView modelAndView = new ModelAndView("templates");
        modelAndView.addObject("templates", mailTemplateService.getMailTemplates());
        return modelAndView;
    }

    @RequestMapping(value = Urls.EDIT, method = RequestMethod.GET)
    public ModelAndView editTemplate(ModelMap model,
                                     @RequestParam(value = "templateId", required = false, defaultValue = "0") int templateId,
                                     @RequestParam(value = "templateName", required = false, defaultValue = "") String templateName,
                                     @ModelAttribute MailTemplateInfo mailTemplateInfo) {

        if (StringUtils.isNotBlank(templateName)) {
            MailTemplate mailTemplate = mailTemplateService.getMailTemplate(templateName);
            if (mailTemplate != null) {
                return new ModelAndView(new RedirectView(Urls.EDIT + "?templateId=" + mailTemplate.getTemplateId()));
            }
        }
        ModelAndView modelAndView = new ModelAndView("edit", model);

        if (templateId != 0) {
            mailTemplateInfo.setMailTemplate(mailTemplateService.getMailTemplate(templateId));
        }

        return modelAndView;
    }

    @RequestMapping(value = Urls.EDIT, method = RequestMethod.POST)
    public ModelAndView saveTemplate(ModelMap model,
                                     @Valid MailTemplateInfo mailTemplateInfo, BindingResult result) throws IOException {

        if (result.hasErrors()) {
            return editTemplate(model, mailTemplateInfo.getMailTemplate().getTemplateId() != null ? mailTemplateInfo.getMailTemplate().getTemplateId() : 0, null, mailTemplateInfo);
        }

        MailTemplate mailTemplate = mailTemplateInfo.getMailTemplate();

        if (mailTemplateInfo.getAsNew() != null && mailTemplate.getTemplateId() != null) {
            int templateId = mailTemplate.getTemplateId();
            mailTemplate.setTemplateId(null);
            mailTemplateService.saveMailTemplate(mailTemplate);
            mailAttachmentService.copyAllAttachments(templateId, mailTemplate.getTemplateId());
        } else {
            mailTemplateService.saveMailTemplate(mailTemplate);
        }

        if (mailTemplate.getTemplateId() != null && mailTemplate.getTemplateId() > 0 && !mailTemplateInfo.getFile().isEmpty()) {
            mailAttachmentService.saveMailAttachmentFromUploadFile(mailTemplateInfo.getFile(), mailTemplate.getTemplateId());
        }
        return new ModelAndView(new RedirectView(Urls.TEMPLATES));
    }

    @RequestMapping(value = Urls.DELETE, method = RequestMethod.GET)
    public String deleteTemplate(@RequestParam(value = "templateId") int templateId) {
        mailTemplateService.deleteMailTemplate(templateId);
        return "redirect:" + Urls.TEMPLATES;
    }

    @RequestMapping(value = Urls.PREVIEW, method = RequestMethod.GET)
    @ResponseBody
    public void previewTemplate(@RequestParam(value = "templateId") int templateId,
                                HttpServletResponse response) throws IOException {

        response.setStatus(HttpServletResponse.SC_OK);
        response.setContentType("text/html; charset=utf-8");
        IOUtils.write(mailTemplateService.getPreviewContentHtml(templateId), response.getOutputStream(), "utf-8");
    }

    @RequestMapping(value = Urls.DELETE_ATTACHMENT, method = RequestMethod.GET)
    public ModelAndView deleteAttachment(@RequestParam(value = "templateId") int templateId,
                                         @RequestParam(value = "attachmentId") long attachmentId) {
        mailAttachmentService.deleteMailAttachment(attachmentId);

        ModelAndView modelAndView = new ModelAndView(new RedirectView(Urls.EDIT));
        modelAndView.addObject("templateId", templateId);
        return modelAndView;
    }
}

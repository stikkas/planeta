package ru.planeta.model;

import org.springframework.web.multipart.MultipartFile;
import ru.planeta.model.mail.MailTemplate;

/**
 * Created with IntelliJ IDEA.
 * User: s.fionov
 * Date: 14.08.12
 */
public class MailTemplateInfo {
    private MailTemplate mailTemplate;
    private MultipartFile file = null;
    private String asNew;

    public MailTemplateInfo(MailTemplate template) {
        this.setMailTemplate(template);
    }

    public MailTemplateInfo() {
    }

    public MailTemplate getMailTemplate() {
        return mailTemplate;
    }

    public void setMailTemplate(MailTemplate mailTemplate) {
        this.mailTemplate = mailTemplate;
    }

    public MultipartFile getFile() {
        return file;
    }

    public void setFile(MultipartFile file) {
        this.file = file;
    }

    public String getAsNew() {
        return asNew;
    }

    public void setAsNew(String asNew) {
        this.asNew = asNew;
    }
}

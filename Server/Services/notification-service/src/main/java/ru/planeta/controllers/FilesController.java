package ru.planeta.controllers;

import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import ru.planeta.api.exceptions.NotFoundException;
import ru.planeta.model.mail.MailAttachment;
import ru.planeta.services.MailAttachmentService;

import javax.mail.internet.MimeUtility;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * User: s.fionov
 * Date: 30.08.12
 */
@Controller
public class FilesController {

    @Autowired
    private MailAttachmentService mailAttachmentService;

    private static final Logger logger = Logger.getLogger(FilesController.class);

    @RequestMapping(value = Urls.GET_ATTACHMENT, method = RequestMethod.GET)
    public void getAttachment(@RequestParam(value = "attachmentId") long attachmentId,
                              HttpServletResponse response) throws IOException {
        try {
            MailAttachment mailAttachment = mailAttachmentService.getMailAttachment(attachmentId);
            response.setStatus(HttpServletResponse.SC_OK);
            response.setContentType(mailAttachment.getMimeType());
            response.addHeader(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + MimeUtility.encodeText(mailAttachment.getFileName()) + "\"");
            IOUtils.write(mailAttachment.getContent(), response.getOutputStream());
        } catch (NotFoundException ex) {
            logger.error("Error getting attachment", ex);
            response.sendError(HttpServletResponse.SC_NOT_FOUND);
        }
    }
}

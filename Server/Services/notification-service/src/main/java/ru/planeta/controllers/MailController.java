package ru.planeta.controllers;

import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import ru.planeta.api.mail.Urls;
import ru.planeta.model.mail.MailMessage;
import ru.planeta.model.mail.MailMessagePriority;
import ru.planeta.services.MailTemplateService;

import javax.activation.DataSource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

/**
 * @author ds.kolyshev
 *         Date: 18.01.12
 */
@Controller
public class MailController {

    private static final String CONTENT_TYPE = "text/html; charset=utf-8";
    private static final String ENC = "utf-8";
    private static final Logger log = Logger.getLogger(MailController.class);

    @Autowired
    private MailTemplateService mailTemplateService;

    @RequestMapping(value = Urls.INSTANCE.getSEND_HTML())
    @ResponseBody
    public void sendMail(@RequestParam(value = "templateName") String templateName,
                         @RequestParam(value = "priority", required = false, defaultValue = "DEFAULT") MailMessagePriority priority,
                         HttpServletRequest request, HttpServletResponse response) throws Exception {

        Map<String, String> parameters = getOptionalParameters(request);

        response.setStatus(HttpServletResponse.SC_OK);
        response.setContentType(CONTENT_TYPE);
        print(parameters);
        mailTemplateService.sendEmail(templateName, parameters, priority);
        IOUtils.write("OK", response.getOutputStream(), ENC);
    }

    /*
    used with .ftl templates
     */
    @RequestMapping(value = Urls.INSTANCE.getSEND_MAIL(), method = RequestMethod.POST)
    @ResponseBody
    public void sendEmail(@RequestParam(value = "mailto") String mailTo,
                          @RequestParam(value = "mailfrom") String mailFrom,
                          @RequestParam(value = "subject") String subject,
                          @RequestParam(value = "message") String contentHtml,
                          @RequestParam(value = "priority", required = false, defaultValue = "DEFAULT") MailMessagePriority priority,
                          HttpServletResponse response) throws Exception {

        response.setStatus(HttpServletResponse.SC_OK);
        response.setContentType(CONTENT_TYPE);
        MailMessage message = new MailMessage(mailFrom, mailFrom, Collections.singletonList(mailTo), subject,
                                            contentHtml, new HashMap<String, DataSource>(), false, priority);
        mailTemplateService.enqueueEmail(message);
        IOUtils.write("OK", response.getOutputStream(), ENC);
    }

    private static Map<String, String> getOptionalParameters(HttpServletRequest request) {
        Map<String, String> parameters = new HashMap<String, String>();
        Enumeration en = request.getParameterNames();
        while (en.hasMoreElements()) {
            String parameterName = (String) en.nextElement();
            parameters.put(parameterName, request.getParameter(parameterName));
        }
        return parameters;
    }

    private static void print(Map<String, String> parameters) {
        log.debug("Email " + parameters.get("userEmail") + " to " + parameters.get("displayName"));
    }
}

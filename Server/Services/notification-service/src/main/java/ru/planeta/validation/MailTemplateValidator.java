package ru.planeta.validation;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import ru.planeta.model.mail.MailTemplate;

/**
 * @author ds.kolyshev
 *         Date: 18.01.12
 */
@Component
public class MailTemplateValidator implements Validator {
    public boolean supports(Class<?> arg0) {
        return MailTemplate.class.isAssignableFrom(arg0);
    }

    public void validate(Object target, Errors errors) {
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "name", "name", "is required!");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "subject", "subject", "is required!");
        if (StringUtils.isBlank((String) errors.getFieldValue("mailTemplate.contentHtml"))) {
            ValidationUtils.rejectIfEmptyOrWhitespace(errors, "mailTemplate.contentBbcode", "contentBbcode", "is required!");
        }
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "fromAddress", "fromAddress", "is required!");
    }
}

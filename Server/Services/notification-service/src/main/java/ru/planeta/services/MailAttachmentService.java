package ru.planeta.services;

import org.springframework.web.multipart.MultipartFile;
import ru.planeta.api.exceptions.NotFoundException;
import ru.planeta.model.mail.MailAttachment;

import java.io.IOException;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: s.fionov
 * Date: 14.08.12
 */
public interface MailAttachmentService {
    /**
     * Gets list of mail attachments with specified templateId
     *
     * @return
     */
    List<MailAttachment> getMailAttachments(int templateId);

    /**
     * Gets specified
     *
     * @param attachmentId
     * @return
     */
    MailAttachment getMailAttachment(long attachmentId) throws NotFoundException;

    /**
     * Saves or updates mail attachment
     *
     * @param mailAttachment
     */
    void saveMailAttachment(MailAttachment mailAttachment);

    /**
     * Deletes mail attachment
     *
     * @param attachmentId
     */
    void deleteMailAttachment(long attachmentId);

    /**
     * Gets attachment from file
     * @param file
     * @param templateId
     * @return
     */
    MailAttachment saveMailAttachmentFromUploadFile(MultipartFile file, int templateId) throws IOException;

    /**
     * Copy attachment to another template
     * @param attachment
     * @param templateId
     */
    void copyAttachment(MailAttachment attachment, int templateId);

    /**
     * Copy attachments from one template to another
     */
    void copyAllAttachments(int templateId, int targetTemplateId);
}

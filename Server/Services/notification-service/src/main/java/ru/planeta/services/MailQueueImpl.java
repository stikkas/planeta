package ru.planeta.services;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.planeta.api.service.common.MailMessageService;
import ru.planeta.controllers.Urls;
import ru.planeta.mail.MailService;
import ru.planeta.model.MailMessageQueue;
import ru.planeta.model.mail.MailMessage;
import ru.planeta.model.mail.MailMessagePriority;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.mail.MessagingException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @author ds.kolyshev
 *         Date: 12.01.12
 */
@Service
public class MailQueueImpl implements MailQueue {

    private final static int MAX_ATTEMPTS_COUNT = 3;
    private static final Logger log = Logger.getLogger(MailQueueImpl.class);

    private final MailService mailService;
    private final MailMessageService mailMessageService;

    private MailMessageQueue messages = new MailMessageQueue();

    private volatile boolean interrupted = false;
    private static final boolean debug = false;            // set this flag true, if you don't want to send emails
    private static final boolean isAsynchronous = true;    // set this flag false, if you want to make sending synchronous

    private final ExecutorService executor = Executors.newSingleThreadExecutor();

    @Autowired
    public MailQueueImpl(MailService mailService, MailMessageService mailMessageService) {
        this.mailService = mailService;
        this.mailMessageService = mailMessageService;
    }

    @PreDestroy
    private void setInterruptedFlag() {
        log.info("Service predestroy");
        interrupted = true;
        executor.shutdown();
    }

    @PostConstruct
    private void startExecutor() {
        if (!isAsynchronous) {
            return;
        }

        executor.execute(new Runnable() {
            @Override
            public void run() {
                sendAll();
            }
        });
    }


    void sendAll() {
        while (!interrupted) {
            log.debug("sendAll iteration");
            final MailMessage message;
            try {
                message = messages.take();
                log.debug("sendAll take");
            } catch (InterruptedException ex) {
                log.warn("Interrupted exception catched ", ex);
                mailService.closeConnection();
                continue;
            }

            try {
                if (debug) {
                    log.info("MailQueue: debug message is " + message.getSubject() + ", priority is " + message.getPriority());
                    continue;
                }

                if (!mailService.isConnected()) {
                    boolean isOpened = mailService.openConnection();
                    if (!isOpened) {
                        throw new MessagingException("No connection");
                    }
                }

                log.info("MailQueue: trying to send message to " + message.getTo());
                message.setContentHtml(message.getContentHtml() + String.format("<img src=\"https://mail.planeta.ru%s?id=%d\">",
                        Urls.OPEN_PIXEL, message.getMessageId()));
                String extMessageId = mailService.send(message.getMessageId(), message.getFrom(), message.getReplyTo(),
                        message.getTo(), message.getSubject(), message.getContentHtml(), message.getMultipartFiles());
                mailMessageService.setExternalMessageId(message.getMessageId(), extMessageId);
                log.info("MailQueue: message send to " + message.getTo());
            } catch (MessagingException ex) {
                log.warn("MailQueue MessagingException: error sending email " + message.getMessageId(), ex);
                message.setPriority(MailMessagePriority.LOW);
                message.increaseAttemptsCount();
                if (message.getAttemptsCount() <= MAX_ATTEMPTS_COUNT) {
                    log.info("MailQueue: trying to re-add message to queue");
                    messages.addErrorMessage(message);
                    log.info("MailQueue: email with id " + message.getMessageId() + " attempts count increased, now " + message.getAttemptsCount());
                } else {
                    log.error("MailQueue: email with id " + message.getMessageId() + " not sent: max attempts count exceeded.", ex);
                }
            } catch (Exception ex) {
                log.error("MailQueue: error sending email " + message.getMessageId(), ex);
                mailService.closeConnection();
            }
        }
    }

    @Override
    public void enqueue(MailMessage mailMessage) {
        log.info("MailQueue: trying to add message to queue");
        mailMessageService.insertMailNotification(mailMessage);
        messages.add(mailMessage);
        log.info("MailQueue: message enqueued. Recepients are " + mailMessage.getToList());
    }

}


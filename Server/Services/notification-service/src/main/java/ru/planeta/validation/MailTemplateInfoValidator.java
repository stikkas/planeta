package ru.planeta.validation;


import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import ru.planeta.model.MailTemplateInfo;
import ru.planeta.model.mail.MailTemplate;

/**
 * Created with IntelliJ IDEA.
 * User: s.fionov
 * Date: 14.08.12
 */
@Component
public class MailTemplateInfoValidator implements Validator {
    public boolean supports(Class<?> arg0) {
        return MailTemplateInfo.class.isAssignableFrom(arg0);
    }

    public void validate(Object target, Errors errors) {
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "mailTemplate.name", "name", "is required!");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "mailTemplate.subject", "subject", "is required!");
        if (StringUtils.isBlank((String) errors.getFieldValue("mailTemplate.contentHtml"))) {
            ValidationUtils.rejectIfEmptyOrWhitespace(errors, "mailTemplate.contentBbcode", "contentBbcode", "is required!");
        }
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "mailTemplate.fromAddress", "fromAddress", "is required!");
    }

}

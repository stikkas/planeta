<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<div class="navbar navbar-inverse navbar-fixed-top">
	<div class="navbar-inner">
		<div class="container">
			<a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</a>
			<a class="brand" href="/">Planeta.ru</a>

			<div class="nav-collapse collapse">
				<ul class="nav">
					<li class="active"><a href="/">Список шаблонов</a></li>
				</ul>
			</div>
		</div>
	</div>
</div>
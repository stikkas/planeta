<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<!DOCTYPE html>

<html>
    <head>
		<%@ include file="/WEB-INF/includes/head.jsp" %>
		<title>Редактирование шаблона</title>
    </head>
    <body>
        <%@ include file="/WEB-INF/includes/header.jsp" %>

        <div class="container">
	        <div class="row">
		        <div class="span12">
			        <div style="color: red"></div>

			        <form:form method="post" commandName="mailTemplateInfo" enctype="multipart/form-data">
				        <form:input type="hidden" path="mailTemplate.templateId"/>

				        <fieldset>
					        <legend>Редактирование шаблона</legend>

					        <label>Название</label>
					        <form:input type="text" path="mailTemplate.name" cssClass="input-xxlarge"/> <form:errors
						        path="mailTemplate.name"/>

					        <label>Отправитель</label>
					        <form:input type="text" path="mailTemplate.fromAddress" cssClass="input-xxlarge"/> <form:errors
						        path="mailTemplate.fromAddress"/>

							<label>Получатель</label>
							<form:input type="text" path="mailTemplate.toAddress" cssClass="input-xxlarge"/>
							<form:errors path="mailTemplate.toAddress"/>

							<label>reply to</label>
							<form:input type="text" path="mailTemplate.replyToAddress" cssClass="input-xxlarge"/>
							<form:errors path="mailTemplate.replyToAddress"/>

					        <label>Тема письма</label>
					        <form:input type="text" path="mailTemplate.subject" cssClass="input-xxlarge"/> <form:errors
						        path="mailTemplate.subject"/>

					        <label>Контент (BB-код)</label>
					        <form:textarea path="mailTemplate.contentBbcode" cssClass="input-xxlarge" rows="10"/>
							<form:errors path="mailTemplate.contentBbcode"/>
							<label>Использовать footer и header</label>
							<form:checkbox path="mailTemplate.useFooterAndHeader"/>
							<label>Контент (HTML)</label>
							<form:textarea path="mailTemplate.contentHtml" cssClass="input-xxlarge" rows="10"/>
							<form:errors path="mailTemplate.contentHtml"/>

					        <label>Прикреплённые файлы:</label>
					        <ul>
						        <c:forEach var="attachment" items="${mailTemplateInfo.mailTemplate.attachments}">
							        <li>
								        <a href="get-attachment.html?attachmentId=${attachment.attachmentId}"><c:out
										        value="${attachment.fileName}"/></a>
								        (${attachment.mimeType})
								        <a href="<c:url value="delete-attachment.html">
                                    <c:param name="templateId" value="${mailTemplateInfo.mailTemplate.templateId}"/>
                                    <c:param name="attachmentId" value="${attachment.attachmentId}"/>
                                </c:url>">Удалить</a>
							        </li>
						        </c:forEach>
					        </ul>
					        <br/>
					        Прикрепить: <form:input type="file" path="file"/>

					        <hr>
							<a href="/preview.html?templateId=${mailTemplateInfo.mailTemplate.templateId}" target="_blank" class="btn btn-large" >Превью</a>
					        <button type="submit" class="btn btn-primary btn-large">Сохранить</button>
					        <button type="submit" class="btn btn-primary btn-large" name="asNew">Сохранить как новый</button>
				        </fieldset>

			        </form:form>

		        </div>
	        </div>
        </div>

    </body>
</html>
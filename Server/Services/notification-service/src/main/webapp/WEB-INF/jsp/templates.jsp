<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<!DOCTYPE html>

<html>
    <head>
		<%@ include file="/WEB-INF/includes/head.jsp" %>
		<title>Шаблоны писем</title>
    </head>
    <body>

        <%@ include file="/WEB-INF/includes/header.jsp" %>

        <div class="container">
	        <div class="row">
		        <div class="span12">
		            <h2>Шаблоны писем.</h2>
		        </div>
	        </div>
	        <div class="row">
		        <div class="span12">
			        <a class="btn btn-primary" href="/edit.html">
				        <i class="icon-plus icon-white"></i>
				        Добавить новый шаблон
			        </a>
		        </div>
		        <hr>
	        </div>
	        <div class="row">
		        <div class="span12">
			        <table class="table table-striped table-bordered">
				        <tbody>
					        <tr>
						        <th>Имя</th>
						        <th>Тема</th>
						        <th>Отправитель</th>
						        <th>Файлы</th>
						        <th>Команды</th>
					        </tr>

					        <c:if test="${fn:length(templates) == 0}">
						        <tr>
							        <td colspan="5" align="center">
								        Нет данных
							        </td>
						        </tr>
					        </c:if>
					        <c:forEach var="item" items="${templates}">
						        <tr>
							        <td>${item.name}</td>
							        <td>${item.subject}</td>
							        <td>${item.fromAddress}</td>
							        <td>${fn:length(item.attachments)}</td>
							        <td nowrap="nowrap">
								        <a class="btn btn-mini btn-primary" href="/edit.html?templateId=${item.templateId}">
									        Редактировать
								        </a>
								        <a class="btn btn-mini btn-warning" href="/delete.html?templateId=${item.templateId}"
								           onclick="return confirm('Вы уверены, что хотите удалить шаблон ${item.name}?');">
									        Удалить
								        </a>
								        <a class="btn btn-mini" href="/preview.html?templateId=${item.templateId}">
									        Превью
								        </a>
							        </td>
						        </tr>
					        </c:forEach>
				        </tbody>
			        </table>
		        </div>
	        </div>
	        <div class="row">
		        <div class="span12">
			        <a class="btn btn-primary" href="/edit.html">
				        <i class="icon-plus icon-white"></i>
				        Добавить новый шаблон
			        </a>
		        </div>
	        </div>
        </div>

    </body>
</html>
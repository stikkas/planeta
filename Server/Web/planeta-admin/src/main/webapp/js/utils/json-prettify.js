/*
 * Helper string functions. 
 * 
 * Usage
 * -----
 * "{0} is formatted {1} !".format("This", "string");   // 'This is formatted string !'
 *
 * "stringWithSeveralCase".containsIgnoreCase("case");  // true
 * "stringWithSeveralCase".containsIgnoreCase("WITH");  // true
 * 
 * "one|two|three".replaceAll("|", ", ")                // 'one, two, three'
 * "demo-string-for-replace".replaceAll("|", ", ")      // 'demostringforreplace'
 *
 */
_.extend(String.prototype, {

    format: function() {
        var formatted = this;
        for (arg in arguments) {
            formatted = formatted.replace("{" + arg + "}", arguments[arg]);
        }

        return formatted;
    },

    containsIgnoreCase: function(searchValue) {
        var strInLowerCase = this.toLowerCase();
        var searchStrInLowerCase = searchValue ? searchValue.toLowerCase() : '';

        return (strInLowerCase.indexOf(searchStrInLowerCase) != -1);
    },

    replaceAll: function(find, replaceWith) {
        return this.split(find).join(replaceWith);
    }
});

/*
 * Helper JQuery pseudo-selector.
 *
 * Usage
 * -----
 * With this in place,
 *
 * $("div:containsIgnoreCase('John')");
 *
 * would selectCampaignById all three of these elements:
 *
 * <div>john</div>
 * <div>John</div>
 * <div>hey hey JOHN hey hey</div>
 *
 */
$.extend($.expr[":"], {
    "containsIgnoreCase": function(elem, i, match, array) {
        return (elem.textContent || elem.innerText || "").toLowerCase().indexOf((match[3] || "").toLowerCase()) >= 0;
    }
});

/*
 * Collapsible JSON Formatter - Formatter and colorer of raw JSON code
 * 
 * Usage
 * -----
 * 
 *
 */

var JsonFormatter = function(attributes) {
    _.extend(this, attributes || {});
};

_.extend(JsonFormatter.prototype, {

    config: {
        indentUnit: '    ',
        quoteKeys: true,
        isCollapsible: true,
        collapseClass: 'icon-minus-sign',
        expandClass: 'icon-plus-sign'
    },

    formatted: { },

    /**
     * process - starts processing the JSON
     * @param json - input JSON string
     * @returns {String} html formatted JSON
     */
    _process: function(json) {
        try {
            if (_.isEmpty(json)) {
                json = '\"\"';
            }
            var parsed = _.isObject(json) ? json : JSON.parse("[" + json + "]")[0];
            return this._processObject(parsed, 0, false, false, false);
        } catch (e) {
            return "JSON invalid.\n" + e.message;
        }
    },

    _spanSurround: function(clazz, content) {
        return _.isEmpty(clazz) ? '<span>{0}</span>'.format(content) : '<span class="{0}">{1}</span>'.format(clazz, content);
    },
    _getFormatted: function(elemName, formatter) {
        if (!this.formatted.hasOwnProperty(elemName)) {
            this.formatted[elemName] = formatter();
        }

        return this.formatted[elemName];

    },
    _commaElement: function() {
        var self = this;
        return this._getFormatted('_commaElement', function() {
            return self._spanSurround('Comma', ',');
        });
    },
    _beginCollapsible: function(clazz, content) {
        var result = this._spanSurround(clazz, content);
        if (this.config.isCollapsible) {
            result += '<a href="javascript:void(0);" class="expander {0}"></a><span class="collapsible">'.format(this.config.collapseClass);
        }

        return result;
    },
    _endCollapsible: function(clazz, content, comma) {
        var result = this._spanSurround(clazz, content) + comma;
        if (this.config.isCollapsible) {
            result = '</span>' + result;
        }

        return result;
    },
    _formatArray: function(array, nestedLevel, comma, isPropertyContent) {
        var result = '';

        var self = this;
        if (_.isEmpty(array)) {
            result = self._getRow(nestedLevel, self._spanSurround('ArrayBrace', '[ ]') + comma, isPropertyContent);
        } else {
            result = self._getRow(nestedLevel, self._beginCollapsible('ArrayBrace', '['), isPropertyContent);
            var size = _.size(array);
            _.each(array, function(elem, i) {
                result += self._processObject(elem, nestedLevel + 1, i < (size - 1), true, false);
            });
            result += self._getRow(nestedLevel, self._endCollapsible('ArrayBrace', ']', comma));
        }

        return result;
    },
    _formatObject: function(obj, nestingLevel, comma, isPropertyContent) {
        var result = '';

        var self = this;
        if (_.isEmpty(obj)) {
            result = self._getRow(nestingLevel, self._spanSurround('ObjectBrace', '{ }') + comma, isPropertyContent);
        } else {
            result = self._getRow(nestingLevel, self._beginCollapsible('ObjectBrace', '{'), isPropertyContent);
            var size = _.size(obj);
            _.each(obj, function(value, key) {
                var clazz = 'PropertyName';
                if (_.isArray(value)) {
                    clazz += ' Array';
                } else if (_.isObject(value)) {
                    clazz += ' Object';
                }

                var _key = self._spanSurround(clazz, self._getKey(key));
                var _value = self._processObject(value, nestingLevel + 1, --size > 0, false, true);

                result += self._getRow(nestingLevel + 1, _key + ': ' + _value);
            });
            result += self._getRow(nestingLevel, self._endCollapsible('ObjectBrace', '}', comma));
        }

        return result;
    },
    _getKey: function(key) {
        return this.config.quoteKeys ? '"{0}"'.format(key) : key;
    },
    _processObject: function(obj, nestingLevel, needComma, isArray, isPropertyContent) {
        var result;
        var comma = needComma ? this._commaElement() : '';

        if (_.isArray(obj)) {
            result = this._formatArray(obj, nestingLevel, comma, isPropertyContent);
        } else if (_.isObject(obj)) {
            result = this._formatObject(obj, nestingLevel, comma, isPropertyContent);
        } else if (_.isNumber(obj)) {
            result = this._formatLiteral(obj, nestingLevel, comma, isArray, 'Number');
        } else if (_.isBoolean(obj)) {
            result = this._formatLiteral(obj, nestingLevel, comma, isArray, 'Boolean');
        } else if (_.isNull(obj)) {
            result = this._formatLiteral('null', nestingLevel, comma, isArray, 'Null');
        } else if (_.isUndefined(obj)) {
            result = this._formatLiteral('undefined', nestingLevel, comma, isArray, 'Null');
        } else {
            result = this._formatLiteral(obj.toString(), nestingLevel, comma, isArray, 'String');
        }

        return result;
    },
    _formatLiteral: function(literal, nestingLevel, comma, isArray, clazz) {
        if (clazz === 'String') {
            literal = '\"' + literal.replaceAll("\\", "\\\\").replaceAll('"', '\\"').replaceAll("<", "&lt;").replaceAll(">", "&gt;") + '\"';
        }

        var result = this._spanSurround('PropertyValue ' + clazz, literal) + comma;
        return isArray ? this._getRow(nestingLevel, result) : result;
    },
    _getIndent: function(nestingLevel) {
        var self = this;
        return this._getFormatted('indent' + nestingLevel, function() {
            var result = '';
            for (var i = 0; i < nestingLevel; i++) {
                result += self.config.indentUnit;
            }
            return result;
        });
    },
    _getRow: function(nestingLevel, data, isPropertyContent) {
        if (!_.isEmpty(data) && data.charAt(data.length - 1) != "\n") {
            data += "\n";
        }

        return isPropertyContent ? data : this._getIndent(nestingLevel) + data;
    },
    _changeAll: function(element, toRemoveClass, toAddClass) {
        var self = this;
        $(element).find('.expander').removeClass(toRemoveClass).addClass(toAddClass).each(function() {
            self.toggleExpanding(this);
        });
    },
    collapseAll: function(element) {
        this._changeAll(element, this.config.expandClass, this.config.collapseClass);
    },
    expandAll: function(element) {
        this._changeAll(element, this.config.collapseClass, this.config.expandClass);
    },
    toggleExpanding: function(element) {
        var el = $(element);
        var expander = el.hasClass('expander') ? el : el.find('.expander:first');
        var collapsible = expander.next('.collapsible');

        expander.toggleClass(this.config.collapseClass).toggleClass(this.config.expandClass);
        if (expander.hasClass(this.config.collapseClass)) {
            collapsible.show();
        } else {
            collapsible.hide();
        }
    },
    format: function(selector, json) {
        var self = this;
        $(selector).each(function() {
            var el = $(this);

            var jsonToProcess = json || (el.val() ? el.val() : el.text());
            el.html(self._process(jsonToProcess));

            el.find('.expander').click(function(e) {
                e.preventDefault();
                self.toggleExpanding(e.currentTarget);
            });
        });

        return self;
    }
});

var NlsJsonFormatter = function(nlsProperties) {

    var normalize = function(nls) {
        var result = _.extend(_.clone(nls), {
            key: {},
            value: nls.value || {}
        });

        _.each(nls.key, function(val, key) {
            if (_.isObject(val)) {
                result.key[key] = val.name;

                var normalized = normalize(val);
                _.extend(result.key, normalized.key);
                _.extend(result.value, normalized.value);
            } else {
                result.key[key] = val;
            }
        });

        return result;
    };

    _.extend(this, _.extend({}, normalize(nlsProperties), {original: nlsProperties}));
}

_.extend(NlsJsonFormatter.prototype, {

    _getProperty: function(propertyName, defaultValue, context) {
        return (context && context.hasOwnProperty(propertyName)) ? context[propertyName] : defaultValue;
    },

    _getKey: function(key) {
        var result = this._getProperty(key, key, this.key);
        return  _.isObject(result) ? result.name : result;
    },

    _getValue: function(key, value) {
        var result = this._getProperty(key, value, this.value);
        return  _.isObject(result) ? this._getProperty(value, value, result) : result;
    },

    _formatDateTime: function(value) {
        return DateUtils.formatDateTime(value, this._getProperty('datePattern', 'd.m.y', this));
    },

    _formatImageValueTag: function(href) {
        return this._formatAnchorTag(href, this._getProperty('image', 'image', this));
    },

    _formatEmailValueTag: function(email) {
        return this._formatAnchorTag('mailto:' + email, email);
    },

    _formatAnchorTag: function(href, text) {
        return '<a href="{0}">{1}</a>'.format(href, text);
    },

    _needTranslateOrReorder: function() {
        return !_.isEmpty(this.original.key)
    },
    _filterAndOrder: function(nls, source) {
        var self = this;
        var isArray = _.isArray(source);
        var result = isArray ? [] : {};

        if (isArray) {
            _.each(source, function(elem) {
                result.push(self._filterAndOrder(nls, elem));
            });
        } else {
            _.each(_.keys(nls.key), function(key) {
                if (source.hasOwnProperty(key)) {
                    var value = source[key];
                    result[key] = _.isObject(value) ? self._filterAndOrder(nls.key[key], value) : value;
                }
            });
        }

        return result;
    },
    format: function(selector, formatter, json) {
        var self = this;
        formatter.format(selector, self._needTranslateOrReorder() ? self._filterAndOrder(self.original, json) : json);

        var container = $(selector);
        container.find('.Null').each(function() {
            var el = $(this);
            el.text(self._getProperty('nullOrUndefined', el.text(), self))
        });

        container.find('.PropertyName:containsIgnoreCase("email")').next('.String').each(function() {
            var el = $(this);
            var email = el.text().replaceAll('"', '').trim();

            el.html(self._formatEmailValueTag(email))
        });

        container.find('.PropertyName:containsIgnoreCase("image")').next('.String').each(function() {
            var el = $(this);
            var src = el.text().replaceAll('"', '').trim();

            el.html(self._formatImageValueTag(src));
        });

        container.find('.PropertyName:containsIgnoreCase("date"), .PropertyName:containsIgnoreCase("time")').next('.Number').removeClass('Number').addClass('Date').each(function() {
                var el = $(this);
                var timestamp = parseInt(el.text(), 10);

                el.html(self._formatDateTime(timestamp))
            });

        if (self._needTranslateOrReorder()) {
            container.find('.PropertyName').each(function() {
                var el = $(this);
                var text = el.html();
                var key = text.replaceAll('"', '');

                el.html(text.replace(key, self._getKey(key)));

                if (!el.is('.Array, .Object')) {
                    el = el.next('.PropertyValue');
                    text = el.html()
                    var value = text.replaceAll('"', '');

                    el.html(text.replace(value, self._getValue(key, value)));
                }
            });
        }

        formatter.collapseAll(container);
        formatter.toggleExpanding(container);
    }
});
<%-- 
	Only included because the Servlet API doesn't allow us
	to set the
	welcome page in web.xml to a virtual URL.
	We set the welcome page in web.xml to be this page as follows:
--%>
<jsp:forward page="/dashboard.html"></jsp:forward>
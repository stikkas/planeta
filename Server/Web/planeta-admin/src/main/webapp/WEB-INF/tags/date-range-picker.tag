<%@ tag body-content="empty" pageEncoding="UTF-8" %>

<%@ include file="/WEB-INF/jsp/includes/taglibs.jsp" %>
<%@ attribute name="dateFrom" type="java.util.Date" required="true" %>
<%@ attribute name="dateTo" type="java.util.Date" required="true" %>
<%@ attribute name="todayButton" required="false" %>
<%@ attribute name="yesterdayButton" required="false" %>
<%@ attribute name="day7Button" required="false" %>
<%@ attribute name="day30Button" required="false" %>
<%@ attribute name="monthButton" required="false" %>
<%@ attribute name="lastMonthButton" required="false" %>
<%@ attribute name="allTimeButton" required="false" %>
<%@ attribute name="allButton" required="false" %>

<input id="dateFrom" name="dateFrom" type="hidden" value="${dateFrom.time}"/>
<input id="dateTo" name="dateTo" type="hidden" value="${dateTo.time}"/>
<c:remove var="dateFrom"/>
<c:remove var="dateTo"/>


<div class="row">
    <div class="">
        <%@include file="/WEB-INF/jsp/admin/date-range-picker.jsp" %>
    </div>
    <div class="btn-group col-lg-8">
        <c:if test="${allButton == true && todayButton != false || todayButton == true}">
            <div class="btn btn-default btn-outline" id="todayButton">Сегодня</div>
        </c:if>
        <c:if test="${allButton == true && yesterdayButton != false || yesterdayButton == true}">
            <div class="btn btn-default btn-outline" id="yesterdayButton">Вчера</div>
        </c:if>
        <c:if test="${allButton == true && day7Button != false || day7Button == true}">
            <div class="btn btn-default btn-outline" id="7dayButton">7 дней</div>
        </c:if>
        <c:if test="${allButton == true && day30Button != false || day30Button == true}">
            <div class="btn btn-default btn-outline" id="30dayButton">30 дней</div>
        </c:if>
        <c:if test="${allButton == true && monthButton != false || monthButton == true}">
            <div class="btn btn-default btn-outline" id="monthButton">Этот месяц</div>
        </c:if>
        <c:if test="${allButton == true && lastMonthButton != false || lastMonthButton == true}">
            <div class="btn btn-default btn-outline" id="lastMonthButton">Предыдущий месяц</div>
        </c:if>
        <c:if test="${allButton == true && allTimeButton != false || allTimeButton == true}">
            <div class="btn btn-default btn-outline" id="allTimeButton">Всё время</div>
        </c:if>
    </div>
</div>


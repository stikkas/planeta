<%@ tag body-content="empty" pageEncoding="UTF-8" %>
<%@ include file="/WEB-INF/jsp/includes/taglibs.jsp" %>


<%@ attribute name="url" required="true" %>
<%@ attribute name="path" required="true" %>
<%@ attribute name="label" required="false" %>
<%@ attribute name="type" required="true" %>

<label>
    <c:if test="${label != null}">
        ${label}
    </c:if>
    <c:if test="${label == null}">
        <c:choose>
            <c:when test="${type == 'IMAGE'}">
                Изображение
            </c:when>
            <c:when test="${type == 'FILE'}">
                Файл
            </c:when>
        </c:choose>
    </c:if>
</label>


<c:if test="${type == 'IMAGE'}">
    <input id="${path}" class="form-control ma-b-20" placeholder="Загрузите изображение или укажите его адрес" value="${url}" name="${path}">

    <div class="image-uploader js-iu-${path}">
        <div id="img-preview" class="js-preview-${path}" style="">
            <img src="${url}" <c:if test="${empty url}">style="display: none;"</c:if>>

            <div id="upload-no_image" class="img-no_preview js-ni-${path}" <c:if test="${not empty url}">style="display: none;"</c:if>>
                <span>
                  <span class="fa fa-plus"></span> <span class="js-text">Добавить изображение</span>
                </span>
            </div>

            <div id="upload-image" class="img-preview js-i-${path}" <c:if test="${empty url}">style="display: none;"</c:if>>
                <span>
                    <span class="fa fa-pencil"></span> Изменить изображение
                </span>
            </div>
        </div>

        <span class="error-message hide">Неподходящая картинка.</span>
    </div>
</c:if>

<c:if test="${type == 'FILE'}">
    <div class="input-group">
        <input id="${path}" class="form-control ma-b-20" placeholder="Загрузите файл или укажите его адрес" value="${url}" name="${path}">
        <span class="input-group-btn">
            <a class="btn btn-primary js-i-${path}">
                <i class="fa fa-file"></i>
            </a>
        </span>
    </div>
</c:if>

<script>
    $(document).ready(function () {
        var errors = {};
        window.checkFile = function (url, success, failure) {
            var img = new Image(),    // the
                loaded = false,
                errored = false;

            img.onload = function () {
                if (loaded) {
                    return;
                }

                loaded = true;

                if (success && success.call) {
                    success.call(img);
                }
            };

            img.onerror = function () {
                if (errored) {
                    return;
                }

                errors[url] = errored = true;

                if (failure && failure.call) {
                    failure.call(img);
                }
            };

            if (errors[url]) {
                img.onerror.call(img);
                return;
            }

            img.src = url;

            if (img.complete) {
                img.onload.call(img);
            }
        };

        var upload = function () {
            var profileId = workspace.appModel.get('myProfile').get('profileId');

            <c:if test="${type == 'IMAGE'}">
                UploadController.showUploadWelcomePromoImage(profileId, function (filesUploaded) {
                    if (!filesUploaded || !filesUploaded.length) {
                        return;
                    }
                    var uploadResult = filesUploaded.at(0).get('uploadResult');
                    if (!uploadResult) {
                        return;
                    }

                    var src = uploadResult.imageUrl;
                    $('.js-preview-${path} img').attr('src', src);
                    $('.js-preview-${path} img').show();
                    $('.js-ni-${path}').hide();
                    $('.js-i-${path}').show();

                    $("#${path}").val(src);
                    $(".js-iu-${path}").find(".error-message").addClass("hide");
                }, "Размер не менее 5x5.", "Загрузка картинки партнера");
            </c:if>

            <c:if test="${type == 'FILE'}">
                UploadController.showUploadFiles(profileId, function (filesUploaded) {
                    if (_.isEmpty(filesUploaded)) {
                        return
                    }

                    var images = _.map(filesUploaded.models, function (fileUploaded) {
                        return fileUploaded.get('uploadResult');
                    });

                    if (images.length > 0) {
                        $('input[name="url"]')[0].value = images[0].fileUrl;
                    }
                });
            </c:if>
        };


        $(".js-i-${path}").click(function () {
            upload();
        });

        $(".js-ni-${path}").click(function () {
            upload();
        });

        $('#url').change(function () {
            var src = $(this).val();

            $("#${path}").val(src);
        });
    });
</script>
<%@ tag body-content="empty" pageEncoding="UTF-8" %>

<%@ include file="/WEB-INF/jsp/includes/taglibs.jsp" %>

<%@ attribute name="buttonSearch" required="false" %>
<%@ attribute name="buttonClear" required="false" %>

<c:if test="${buttonSearch != false}">
    <button id="search" class="btn btn-primary">
        <i class="fa fa-search"></i> Поиск
    </button>
</c:if>

<c:if test="${buttonClear != false}">
    <button id="clear-filter" class="btn btn-default">
        <i class="fa fa-trash"></i> Очистить
    </button>
    <script>
        $('#clear-filter').click(function(e) {
            e.preventDefault();
            $('form').find('input,selectCampaignById').val('');
            $('#allTimeButton').click();
        });
    </script>
</c:if>
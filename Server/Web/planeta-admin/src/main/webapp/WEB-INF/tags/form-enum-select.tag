<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ tag body-content="empty" %>

<%@ include file="/WEB-INF/jsp/includes/taglibs.jsp" %>

<%@ attribute name="path" required="true" %>
<%@ attribute name="label" required="false" %>
<%@ attribute name="notSelectLabel" required="false" %>
<%@ attribute name="items" required="true" type="ru.planeta.model.enums.ReadableEnum[]" %>



<c:if test="${not empty label}">
    <form:label path="${path}" cssClass="form-label">${label}</form:label>
</c:if>

<c:if test="${empty disabled}">
    <c:set var="disabled" value="false" />
</c:if>

<c:if test="${empty readonly}">
    <c:set var="readonly" value="false" />
</c:if>

<form:select path="${path}" disabled="${disabled}" readonly="${readonly}" cssClass="form-control">
    <c:if test="${not empty notSelectLabel}">
        <option value="">${notSelectLabel}</option>
    </c:if>
    <c:forEach items="${items}" var="item">
        <form:option value="${item}" label="${hf:getMessage(item.descriptionMessageCode)}"/>
    </c:forEach>
</form:select>

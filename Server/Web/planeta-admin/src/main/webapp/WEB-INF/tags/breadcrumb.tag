<%@ tag body-content="empty" pageEncoding="UTF-8" %>
<%@ include file="/WEB-INF/jsp/includes/taglibs.jsp" %>


<%@ attribute name="title1" required="false" %>
<%@ attribute name="href1" required="false" %>
<%@ attribute name="title2" required="false" %>
<%@ attribute name="href2" required="false" %>
<%@ attribute name="title3" required="false" %>
<%@ attribute name="href3" required="false" %>
<%@ attribute name="title4" required="false" %>
<%@ attribute name="href4" required="false" %>
<%@ attribute name="caption" required="false" %>

<div class="row ">
  <div class="span12">
    <ul class="breadcrumb">
      <li><a href="/">Главная</a> <span class="divider">/</span></li>
      <c:if test="${not empty title1}"><li class="active"><c:if test="${not empty href1}"><a href="${href1}"></c:if>${title1}<c:if test="${not empty href1}"></a></c:if><c:if test="${not empty title2}"><span class="divider">/</span></c:if></li></c:if>
      <c:if test="${not empty title2}"><li class="active"><c:if test="${not empty href2}"><a href="${href2}"></c:if>${title2}<c:if test="${not empty href2}"></a></c:if><c:if test="${not empty title3}"><span class="divider">/</span></c:if></li></c:if>
      <c:if test="${not empty title3}"><li class="active"><c:if test="${not empty href3}"><a href="${href3}"></c:if>${title3}<c:if test="${not empty href2}"></a></c:if><c:if test="${not empty title4}"><span class="divider">/</span></c:if></li></c:if>
      <c:if test="${not empty title4}"><li class="active"><c:if test="${not empty href4}"><a href="${href4}"></c:if>${title4}<c:if test="${not empty href2}"></a></c:if><c:if test="${not empty title5}"><span class="divider">/</span></c:if></li></c:if>
    </ul>
  </div>
</div>

<div class="row ">
  <div class="span12">
    <div class="mrg-b-30">
      <h2>
        <c:if test="${not empty caption}">${caption}</c:if>
        <c:if test="${not empty title4 && empty caption}">${title4}</c:if>
        <c:if test="${not empty title3 && empty title4 && empty caption}">${title3}</c:if>
        <c:if test="${not empty title2 && empty title3 && empty title4 && empty caption}">${title2}</c:if>
        <c:if test="${not empty title1 && empty title2 && empty title3 && empty title4 && empty caption}">${title1}</c:if>
      </h2>
    </div>
  </div>
</div>
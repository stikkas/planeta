<%@ taglib prefix="s" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ include file="/WEB-INF/jsp/includes/generated/doctype-header.jsp" %>
<head>
    <%@include file="../head.jsp" %>

    <script type="text/javascript">
        $(document).ready(function() {
            $('.btn.promo-remove').on('click', function (e) {
                e.preventDefault();
                var href = e.currentTarget.href;
                Modal.showConfirm('Удалить файл?', "Подтвердите удаление", {
                    success: function (e) {
                        document.location = href;
                    }
                });
            });
        });
    </script>
</head>
<body>

<%@ include file="/WEB-INF/jsp/admin/navbar.jsp" %>

<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Библиотека файлов</h1>
        </div>
    </div>


    <div class="main-page-actions">
        <a class="btn btn-success btn-circle btn-outline btn-lg" href="/admin/charity/school-library-file-edit.html?libraryFileId=0" title="Добавить">
            <i class="fa fa-plus"></i>
        </a>

        <a class="btn btn-primary btn-circle btn-outline btn-lg" href="/admin/charity/school-library-themes.html" target="_blank" title="Список тем/разделов">
            <i class="fa fa-list"></i>
        </a>
    </div>

    <div class="row">
        <div class="col-lg-12 admin-table">
            <table class="table table-bordered table-striped">
                <thead>
                    <th>Файл</th>
                    <th>Id</th>
                    <th>Заголовок</th>
                    <th>Действие</th>
                </thead>

                <tbody>
                <c:forEach items="${libraryFiles}" var="libraryFile" varStatus="st">
                    <tr>
                        <td width="1px" style="text-align: center;">
                            <a class="ap-item" href="${libraryFile.url}" target="_blank" title="${libraryFile.header}">
                                <img src="//${hf:getStaticBaseUrl("")}/images/icon/downloading-file.png" alt="${libraryFile.header}" style="max-height: 32px; max-width: 116px;">
                            </a>
                            <b>${fn:split(libraryFile.url, '.')[fn:length(fn:split(libraryFile.url, '.'))-1]}</b>
                        </td>
                        <td>
                                ${libraryFile.libraryFileId}
                        </td>
                        <td>
                                ${libraryFile.header}
                        </td>
                        <td class="text-right">
                            <a href="/admin/charity/school-library-file-edit.html?libraryFileId=${libraryFile.libraryFileId}" class="btn btn-primary btn-outline" title="Редактировать">
                                <i class="fa fa-pencil"></i>
                            </a>
                            <a href="/admin/charity/school-library-file-delete.html?libraryFileId=${libraryFile.libraryFileId}" class="btn btn-danger btn-outline" title="Удалить">
                                <i class="fa fa-trash"></i>
                            </a>
                        </td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>
    </div>
</div>
</body>



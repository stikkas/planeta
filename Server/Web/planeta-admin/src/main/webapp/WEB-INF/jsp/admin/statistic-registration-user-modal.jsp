<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ include file="/WEB-INF/jsp/includes/generated/doctype-header.jsp" %>

    <head>
        <%@ include file="statistic-head.jsp" %>
    </head>

    <body>
        <div id="center-container" class="span18">

            <h2>Статистика по регистрациям
                <c:if test="${not empty dateFrom && not empty dateTo && dateTo > dateFrom}">
                    за <fmt:formatDate value="${dateFrom}" pattern="dd.MM.yyyy"/> - <fmt:formatDate value="${dateTo}" pattern="dd.MM.yyyy"/>
                </c:if>
                <c:if test="${not empty dateFrom}">
                    за <fmt:formatDate value="${dateFrom}" pattern="dd.MM.yyyy"/>
                </c:if>
            </h2>

            <table class="table table-bordered table-striped">

                <thead>
                    <th>Дата</th><th>Имя</th><th>Почта</th>
                </thead>

                <tbody>
                <c:forEach var="user" items="${users}" varStatus="status">
                    <tr>
                        <td><fmt:formatDate value="${user.timeAdded}" pattern="dd.MM.yyyy"/></td>
                        <td><a href="/moderator/user-info.html?profileId=${user.profile.profileId}">${user.profile.displayName}</a></td>
                        <td>${user.email}</td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>

            <form id="stat-params" method="GET">
                <div class="pull-left">
                    <input name="dateFrom" type="hidden" value="${dateFrom.time}"/>
                    <input name="dateTo" type="hidden" value="${dateTo.time}"/>
                    <button class="btn btn-primary" formaction="registrations-report.html">В Excel</button>
                </div>
            </form>

            <br>
            <br>
		</div>
	</body>
</html>


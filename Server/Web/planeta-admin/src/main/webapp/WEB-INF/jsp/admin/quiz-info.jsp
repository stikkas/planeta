<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ include file="/WEB-INF/jsp/includes/generated/doctype-header.jsp" %>
<link type="text/css" rel="stylesheet"
      href="//${hf:getStaticBaseUrl("")}/css/ui-lightness/jquery-ui-1.10.0-bootstrap.custom.css"/>
<head>
    <%@include file="head.jsp" %>
    <script type="text/javascript">
        $(document).ready(function () {
            var startIndex;
            //JQUERY plugin that makes possible to sort collection and move collection elements
            var sortable = $('table tbody').sortable({
                revert: true,
                handle: '.btn.js-move',

                helper: function (e, ui) {
                    ui.children().each(function () {
                        $(this).width($(this).width());
                    });
                    return ui;
                },

                start: function (event, ui) {
                    ui.placeholder.html(ui.helper.html());
                    startIndex = ui.item.index();
                },

                update: function (event, ui) {
                    var stopIndex = ui.item.index();
                    if (stopIndex != startIndex) {
                        var quizQuestionId = $(ui.item[0]).attr('quiz-question-id').trim();
                        $.ajax({
                            url: '/admin/quiz-questions-sort.html',
                            data: {
                                quizId: ${quiz.quizId},
                                quizQuestionId: quizQuestionId,
                                oldOrderNum: startIndex,
                                newOrderNum: stopIndex
                            },
                            success: function (response) {
                                console.log('success');
                            }
                        });
                    }
                }
            });

            var changeQuestEnabling = function (quizQuestionId, enabled) {
                var option = {
                    quizQuestionId: quizQuestionId,
                    enabled: enabled
                };
                Modal.showConfirm(enabled ? 'Показать вопрос?' : 'Скрыть вопрос?', "Подтвердите", {
                    success: function (e) {
                        $.post('/admin/quiz-question-enabling.json', option).done(function (response) {
                            if (response) {
                                if (response.success) {
                                    location.reload();
                                }
                            }
                        });
                    }
                });
            };

            $(".js-hide-question").click(function (e) {
                e.preventDefault();
                var $tr = $(e.currentTarget);
                var quizQuestionId = $tr.attr('quiz-question-id');
                changeQuestEnabling(quizQuestionId, false);
            });

            $(".js-show-question").click(function (e) {
                e.preventDefault();
                var $tr = $(e.currentTarget);
                var quizQuestionId = $tr.attr('quiz-question-id');
                changeQuestEnabling(quizQuestionId, true);
            });

            var tinyModel;
            moduleLoader.loadModule('attachments');
            moduleLoader.loadModule('tiny-mce-campaigns').done(function () {

                var modelClass = function (campaignModel) {
                    _.extend(this, new TinyMcePlaneta.EditorModel(campaignModel), {
                        config: TinyMcePlaneta.getQuizEditorConfiguration(),
                        width: 550,
                        height: 400,
                        saveToParentModel: function () {

                        },
                        destructPlugins: function () {
                            if (tinymce.activeEditor.plugins.planetafullscreencampaign.fullScreen) {
                                tinymce.activeEditor.execCommand("planetafullscreencampaign");
                            }
                        }
                    });
                };

                tinyModel = new modelClass();
                tinyModel.init($('.tinymce-body'));
            });
        });
    </script>
    <style>
        .help-inline {
            color: #cc1010;
        }
    </style>
</head>
<body>

<%@ include file="navbar.jsp" %>

<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                <c:if test="${quiz.quizId != 0}">Опрос № ${quiz.quizId}</c:if>
                <c:if test="${quiz.quizId == 0}">Создание опроса</c:if>
            </h1>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <%@include file="successMessage.jsp" %>
        </div>
    </div>

    <div class="main-page-actions">
        <form class="form-horizontal" id="stat-params" method="get">
            <c:if test="${quiz.alias != null}">
                <a href="${mainAppUrl}/quiz/${quiz.alias}" class="btn btn-primary btn-circle btn-outline btn-lg" title="Страница опроса">
                    <i class="fa fa-file-o"></i>
                </a>
            </c:if>
            <c:if test="${quiz.alias == null}">
                <a href="${mainAppUrl}/quiz/${quiz.quizId}" class="btn btn-primary btn-circle btn-outline btn-lg" title="Страница опроса">
                    <i class="fa fa-file-o"></i>
                </a>
            </c:if>

            <button class="btn btn-success btn-circle btn-outline btn-lg"
                    type="submit"
                    formaction="/moderator/quiz-answers-report.html"
                    onclick="$('#reportType').val('CSV');"
                    title="Скачать ответы в формате CSV">
                <i class="fa fa-download"></i>
            </button>
        </form>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <form:form method="POST" commandName="quiz" class="form-horizontal">
                <form:input type="hidden" path="quizId"/>

                <div class="row ma-b-20">
                    <div class="col-lg-6">
                        <label>Название</label>

                        <form:input type="text" path="name" cssClass="form-control"/>
                        <form:errors path="name"><span class="help-inline"><form:errors path="name"/></span></form:errors>
                        <p class="text-info"><b>Например:</b> Опрос для пользователей сервиса Planeta.ru</p>
                    </div>

                    <div class="col-lg-6">
                        <label>Алиас</label>
                        <form:input type="text" path="alias" cssClass="form-control"/>
                        <form:errors path="alias"><span class="help-inline"><form:errors path="alias"/></span></form:errors>
                    </div>
                </div>

                <div class="row ma-b-20">
                    <div class="col-lg-12">
                        <label>HTML описание</label>

                        <div id="description" class="tinymce-body">
                                ${quiz.description}
                        </div>
                        <form:errors path="description"><span class="help-inline"><form:errors path="description"/></span></form:errors>
                        <p class="text-info"><b>Например:</b> С целью улучшения качества сервиса, просим Вас ответить на следующие вопросы:</p>
                    </div>
                </div>

                <div class="row ma-b-20">
                    <div class="col-lg-12">
                        <label>Доступен для прохождения</label>
                        <form:select path="enabled" cssClass="form-control" id="enabled">
                            <option value="true" <c:if test="${quiz.enabled}">selected="true"</c:if>>Да</option>
                            <option value="false" <c:if test="${not quiz.enabled}">selected="true"</c:if>>Нет</option>
                        </form:select>
                    </div>
                </div>

                <div class="btn-group">
                    <button type="submit" class="btn btn-primary">Сохранить</button>
                    <a class="btn btn-default" href="/admin/quizzes.html">Отмена</a>
                </div>
            </form:form>
        </div>
    </div>

    <c:if test="${quiz.quizId > 0}">
        <h2>Вопросы (${quizQuestions.size()})</h2>

        <a href="/admin/quiz-question-edit.html?quizId=${quiz.quizId}&quizQuestionId=0" class="btn btn-xs btn-success ma-b-20">
            <i class="fa fa-plus"></i> Добавить вопрос
        </a>

        <div class="row">
            <div class="col-lg-12 admin-table">
                <table class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>Вопрос</th>
                            <th>Описание</th>
                            <th>Тип вопроса</th>
                            <th>Действие</th>
                        </tr>
                    </thead>

                    <tbody>
                    <c:forEach items="${quizQuestions}" var="quizQuestion" varStatus="st">
                        <tr quiz-question-id="${quizQuestion.quizQuestionId}" <c:if test="${!quizQuestion.enabled}"> style="background-color: white; opacity: .5;"</c:if>>
                            <td>${quizQuestion.questionText}</td>
                            <td>${quizQuestion.questionDescription}</td>
                            <td>
                                <c:if test="${quizQuestion.type == \"TEXT\"}">Текстовый ответ</c:if>
                                <c:if test="${quizQuestion.type == \"TEXT_BIG\"}">Текстовый ответ на много букв</c:if>
                                <c:if test="${quizQuestion.type == \"NUMBER\"}">Числовой ответ</c:if>
                                <c:if test="${quizQuestion.type == \"CHECKBOX_SIMPLE\"}">Да/нет</c:if>
                                <c:if test="${quizQuestion.type == \"CHECKBOX_MULTI\"}">Множественный выбор</c:if>
                                <c:if test="${quizQuestion.type == \"RADIO_NUMBER_RANGE\"}">Оценка в баллах от и до</c:if>
                                <c:if test="${quizQuestion.type == \"RADIO_OPTIONS\"}">Один из вариантов</c:if>
                            </td>
                            <td>
                                <div class="btn-group">
                                    <a class="btn btn-default btn-outline js-move" title="Переместить">
                                        <i class="fa fa-arrows"></i>
                                    </a>
                                    <a href="/admin/quiz-question-edit.html?quizId=${quiz.quizId}&quizQuestionId=${quizQuestion.quizQuestionId}"
                                       class="btn btn-primary"
                                       title="Редактировать">
                                        <i class="fa fa-pencil"></i>
                                    </a>
                                    <c:if test="${quizQuestion.enabled}">
                                        <button class="btn btn-warning btn-outline js-hide-question"
                                                quiz-question-id="${quizQuestion.quizQuestionId}"
                                                title="Скрыть вопрос">
                                            <i class="fa fa-eye-slash"></i>
                                        </button>
                                    </c:if>
                                    <c:if test="${!quizQuestion.enabled}">
                                        <button class="btn btn-success btn-outline js-show-question"
                                                quiz-question-id="${quizQuestion.quizQuestionId}"
                                                title="Показать вопрос">
                                            <i class="fa fa-eye"></i>
                                        </button>
                                    </c:if>
                                </div>
                            </td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
            </div>
        </div>
    </c:if>
</div>
</body>
</html>

<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ include file="/WEB-INF/jsp/includes/generated/doctype-header.jsp" %>
<head>
<%@include file="../head.jsp" %>

<script type="text/javascript">
    $(document).ready(function() {
        $('.btn.promo-remove').on('click', function(e) {
            e.preventDefault();
            var href = e.currentTarget.href;
            Modal.showConfirm('Удалить партнера?', "Подтвердите удаление", {
                success:function (e) {
                    document.location=href;
                }
            });
        });

        //
        var startIndex;
        //JQUERY plugin that makes possible to sort collection and move collection elements
        var sortable = $('table tbody').sortable({
            revert: true,
            handle: '.btn.js-move',

            helper: function(e, ui) {
                ui.children().each(function() {
                    $(this).width($(this).width());
                });
                return ui;
            },

            start: function(event, ui) {
                ui.placeholder.html(ui.helper.html());
                startIndex = ui.item.index();
            },

            update: function(event, ui) {
                var stopIndex = ui.item.index();
                if(stopIndex != startIndex) {
                    $('form#sort #startIndex').val(startIndex);
                    $('form#sort #stopIndex').val(stopIndex);
                    $('form#sort').submit();
                }
            }
        });
        sortable.disableSelection();
    });
</script>

</head>

<body>

<%@ include file="/WEB-INF/jsp/admin/navbar.jsp" %>

<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Реклама</h1>
        </div>
    </div>

    <div class="main-page-actions">
        <a class="btn btn-success btn-circle btn-outline btn-lg" href="/admin/biblio/advertise-biblio-fill.html" title="Добавить">
            <i class="fa fa-plus"></i>
        </a>
    </div>

    <div class="row">
        <div class="col-lg-12 admin-table">
            <table class="table table-bordered table-striped">
                <thead>
                <th>Заголовок</th>
                <th>Краткое описание</th>
                <th>Картинка</th>
                <th>Ссылка на видео</th>
                <th>Ссылка на текст</th>
                <th>Действие</th>
                </thead>
                <tbody>
                <c:forEach items="${configurationList}" var="promo" varStatus="st">
                    <tr>
                        <td style="max-width:200px">${promo.title}</td>
                        <td style="max-width:380px">${promo.annonce}</td>
                        <td width="1px"><img src="${promo.imageUrl}" alt="${promo.title}" style="max-height: 70px; max-width: 100px;"></td>
                        <td><a href="${promo.videoLink}" target="_blank">${promo.videoTitle}</a></td>
                        <td><a href="${promo.textLink}" target="_blank">${promo.textTitle}</a></td>
                        <td>
                            <div class="btn-group">
                                <a class="btn btn-default js-move" title="Переместить">
                                    <i class="fa fa-arrows"></i>
                                </a>
                                <a href="/admin/biblio/advertise-biblio-edit.html?id=${st.index}" class="btn btn-primary">
                                    <i class="fa fa-pencil"></i>
                                </a>
                                <a href="/admin/biblio/advertise-biblio-delete.html?id=${st.index}" class="btn btn-danger promo-remove">
                                    <i class="fa fa-trash"></i>
                                </a>
                            </div>
                        </td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>

            <form id="sort" action="/admin/biblio/advertise-biblio-sort.html" method="post">
                <input type="hidden" name="startIndex" id="startIndex" />
                <input type="hidden" name="stopIndex" id="stopIndex" />
            </form>
        </div>
    </div>
</body>




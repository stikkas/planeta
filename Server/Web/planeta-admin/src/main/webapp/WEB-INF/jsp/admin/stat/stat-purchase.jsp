<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ include file="/WEB-INF/jsp/includes/taglibs.jsp" %>


<div id="info">
    <table class="table table-bordered table-striped">
        <thead>
            <th>Дата</th>
            <th>Количество регистраций</th>
            <th>Продано вознаграждений (руб.)</th>
            <th>Продано товаров (руб.)</th>
            <th>Библиородина (руб.)</th>
            <th>Всего (руб.)</th>
        </thead>

        <tbody>
        <c:forEach var="row" items="${purchaseReports}" varStatus="status">
            <tr data-date-from="${row.key.time}" step="${step}">
                <td><fmt:formatDate value="${row.key}" pattern="dd.MM.yyyy"/></td>
                <td data-type="user">
                    <a href="#" class="statPopupLink">${row.value.regCount}</a>
                    <c:choose>
                        <c:when test="${row.value.regCountDiff < 0}">
                            <span class="text-danger" data-tooltip="Изменение значения по сравнению с предыдущим интервалом" rel="tooltip">
                                (${row.value.regCountDiff})
                            </span>
                        </c:when>
                        <c:when test="${row.value.regCountDiff > 0}">
                            <span class="text-success" data-tooltip="Изменение значения по сравнению с предыдущим интервалом" rel="tooltip">
                                (+${row.value.regCountDiff})
                            </span>
                        </c:when>
                        <c:otherwise>
                            (-)
                        </c:otherwise>
                    </c:choose>
                </td>
                <td data-type="share">
                    <a href="#" class="statPopupLink">${row.value.sharesAmount} / ${row.value.sharesSumOfObjectsCount}</a>
                    <c:choose>
                        <c:when test="${row.value.sharesAmountDiff < 0}">
                            <span class="text-danger" data-tooltip="Изменение значения по сравнению с предыдущим интервалом" rel="tooltip">
                                (${row.value.sharesAmountDiff})
                            </span>
                        </c:when>
                        <c:when test="${row.value.sharesAmountDiff > 0}">
                            <span class="text-success tooltip-enabled" data-tooltip="Изменение значения по сравнению с предыдущим интервалом" rel="tooltip">
                                (+${row.value.sharesAmountDiff})
                            </span>
                        </c:when>
                        <c:otherwise>
                            (-)
                        </c:otherwise>
                    </c:choose>
                </td>

                <td data-type="product">
                    <a href="#" class="statPopupLink">${row.value.productsAmount} / ${row.value.productsSumOfObjectsCount}</a>
                    <c:choose>
                        <c:when test="${row.value.productsAmountDiff < 0}">
                            <span class="text-danger" data-tooltip="Изменение значения по сравнению с предыдущим интервалом" rel="tooltip">
                                (${row.value.productsAmountDiff})
                            </span>
                        </c:when>
                        <c:when test="${row.value.productsAmountDiff > 0}">
                            <span class="text-success tooltip-enabled" data-tooltip="Изменение значения по сравнению с предыдущим интервалом" rel="tooltip">
                                (+${row.value.productsAmountDiff})
                            </span>
                        </c:when>
                        <c:otherwise>
                            (-)
                        </c:otherwise>
                    </c:choose>
                </td>

                <td data-type="biblio">
                    ${row.value.biblioPurchasesAmount} / ${row.value.biblioPurchasesCount}
                    <c:choose>
                        <c:when test="${row.value.biblioPurchasesAmountDiff < 0}">
                            <span class="text-danger" data-tooltip="Изменение значения по сравнению с предыдущим интервалом" rel="tooltip">
                                (${row.value.biblioPurchasesAmountDiff})
                            </span>
                        </c:when>
                        <c:when test="${row.value.biblioPurchasesAmountDiff > 0}">
                            <span class="text-success tooltip-enabled" data-tooltip="Изменение значения по сравнению с предыдущим интервалом" rel="tooltip">
                                (+${row.value.biblioPurchasesAmountDiff})
                            </span>
                        </c:when>
                        <c:otherwise>
                            (-)
                        </c:otherwise>
                    </c:choose>
                </td>

                <td>
                    ${row.value.totalAmount}
                    <c:choose>
                        <c:when test="${row.value.totalAmountDiff < 0}">
                            <span class="text-danger tooltip-enabled" data-tooltip="Изменение значения по сравнению с предыдущим интервалом" rel="tooltip">
                                (${row.value.totalAmountDiff})
                            </span>
                        </c:when>
                        <c:when test="${row.value.totalAmountDiff > 0}">
                            <span class="text-success tooltip-enabled" data-tooltip="Изменение значения по сравнению с предыдущим интервалом" rel="tooltip">
                                (+${row.value.totalAmountDiff})
                            </span>
                        </c:when>
                        <c:otherwise>
                            (-)
                        </c:otherwise>
                    </c:choose>
                </td>

            </tr>
        </c:forEach>
        <tr>
            <th>Итого</th>
            <th>${totalReport.regCount}</th>
            <th>${totalReport.sharesAmount}</th>
            <th>${totalReport.productsAmount}</th>
            <th>${totalReport.biblioPurchasesAmount}</th>
            <th>${totalReport.totalAmount}</th>
        </tr>
        </tbody>
    </table>
</div>

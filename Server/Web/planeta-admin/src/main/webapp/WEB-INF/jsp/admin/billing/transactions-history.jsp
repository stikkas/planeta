<%@ include file="/WEB-INF/jsp/includes/generated/doctype-header.jsp" %>
<html>
<head>
    <title>История транзакций пользователя</title>
    <%@ include file="/WEB-INF/jsp/admin/head.jsp" %>
</head>

<body>

<%@ include file="/WEB-INF/jsp/admin/navbar.jsp" %>

<div id="page-wrapper">


    <div class="row ">
        <div class="col-lg-12">
            <h1 class="page-header">Транзкации пользователя ${myProfile.profile.profileId}</h1>
        </div>
    </div>

    <c:if test="${not empty transactions}">
    <div class="row">
        <div class="col-lg-12 admin-table">
            <table class="table table-hover">
                <thead>
                <tr>
                    <th></th>
                    <th>Транзакция</th>
                    <th>Сумма</th>
                    <th>Дата</th>
                    <th>Комментарий</th>
                    <th>Тип заказа</th>
                </tr>
                </thead>
                <tbody>
                    <c:forEach items="${transactions}" var="transaction">
                        <tr>
                            <td>
                                <c:choose>
                                    <c:when test="${transaction.transactionType == 'DEBIT'}">
                                        <span class="label label-success">+</span>
                                    </c:when>
                                    <c:when test="${transaction.transactionType == 'CREDIT'}">
                                        <span class="label label-danger">&ndash;</span>
                                    </c:when>
                                </c:choose>
                            </td>
                            <td>${transaction.transactionId}</td>
                            <td>${transaction.amountNet}</td>
                            <td><fmt:formatDate value="${transaction.timeAdded}" pattern="dd.MM.yyyy HH:mm:ss"/></td>
                            <td>${transaction.comment}</td>
                            <td>
                                <c:if test="${transaction.orderObjectType != null}">
                                    ${transaction.orderObjectType}
                                </c:if>
                            </td>
                    </c:forEach>
                </tbody>
            </table>
        </div>
    </div>

    <%@ include file="../paginator.jsp" %>
    </c:if>
</body>
</html>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ include file="/WEB-INF/jsp/includes/generated/doctype-header.jsp" %>
<head>
    <%@ include file="../head.jsp" %>

    <script type="text/javascript">
        var setReturnUrl = function () {
            var buttonsForms = document.buttonsForm;
            if (buttonsForms) {
                for (var i = 0; i < buttonsForms.length; i++) {
                    buttonsForms[i].returnUrl.value = document.URL;
                }
            }
            return true;
        };

        $(document).ready(function () {
        });
    </script>
</head>
<body>

<%@ include file="/WEB-INF/jsp/admin/navbar.jsp" %>

<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Школа</h1>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <%@ include file="searchBoxSeminars.jsp" %>
        </div>
    </div>

    <c:if test="${!isThereSeminarOnMainPage}">
        <div class="row">
            <div class="col-lg-12">
                <div class="alert alert-danger bs-alert-old-docs">
                    <strong>Внимание!</strong> На главную страницу не выведен ни один проект!
                </div>
            </div>
        </div>
    </c:if>

    <div class="main-page-actions">
        <a class="btn btn-success btn-circle btn-outline btn-lg" href="/admin/seminar-info.html" title="Добавить занятие">
            <i class="fa fa-plus"></i>
        </a>
    </div>

    <div class="row">
        <div class="col-lg-12 admin-table">
            <table class="table table-striped table-hover table-bordered">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Название</th>
                    <th>Город</th>
                    <th>Дата начала</th>
                    <th>Тематика</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                <tr>
                </tr>
                <c:forEach var="seminar" items="${seminarList}" varStatus="status">
                    <tr <c:if test="${seminar.isShownOnMainPage}">class="success"</c:if>>
                        <td>${seminar.seminarId}</td>
                        <td>
                            <p><a href="https://${properties['school.application.host']}/school/${seminar.seminarId}"
                                  target="_blank">${seminar.name}</a></p>
                        </td>
                        <td>${seminar.cityNameRu}</td>
                        <td><fmt:formatDate value="${seminar.timeStart}" pattern="dd.MM.yyyy"/></td>
                        <td>${seminar.tagName}</td>
                        <td class="text-right">
                            <div>
                                <form name="buttonsForm" onsubmit="setReturnUrl();" class="btn-group">
                                    <a class="btn btn-outline btn-primary"
                                        href="/admin/seminar-info.html?seminarId=${seminar.seminarId}"
                                        title="Редактировать занятие">
                                        <i class="fa fa-pencil"></i>
                                    </a>
                                    <input name="reportType" id="reportType" type="hidden" value="">
                                    <input name="seminarId" id="seminarId" type="hidden" value="${seminar.seminarId}">
                                    <button class="btn btn-outline btn-success" type="submit"
                                            formaction="/admin/seminar-registrations-report.html"
                                            onclick="$('#reportType').val('EXCEL');"
                                            title="Скачать в формате Excel">
                                        <i class="fa fa-file-excel-o"></i>
                                    </button>

                                    <input name="reportType" id="seminarIdToChange" type="hidden"
                                           value="${seminar.seminarId}">
                                    <input name="returnUrl" id="returnUrl" type="hidden" value="">
                                    <input name="referrer" id="referrer" type="hidden" value="">
                                    <button class="btn btn-outline btn-danger" type="submit"
                                            formaction="/admin/seminar-remove.html" formmethod="POST"
                                            onclick="return confirm('Вы действительно хотите удалить занятие?');"
                                            title="Удалить">
                                        <i class="fa fa-trash"></i>
                                    </button>
                                    <c:if test="${seminar.isShownOnMainPage}">
                                        <button class="btn btn-outline btn-warning pull-right" type="submit"
                                                formaction="/admin/hide-seminar-from-main-page.html"
                                                formmethod="POST"
                                                title="Убрать с главной">
                                            <i class="fa fa-toggle-off" aria-hidden="true"></i>
                                        </button>
                                    </c:if>
                                    <c:if test="${!seminar.isShownOnMainPage}">
                                        <button class="btn btn-outline btn-info js-is-shown-on-main-page" type="submit"
                                                formaction="/admin/show-seminar-on-main-page.html"
                                                formmethod="POST"
                                                title="Показать на главной">
                                            <i class="fa fa-toggle-on" aria-hidden="true"></i>
                                        </button>
                                    </c:if>
                                </form>
                            </div>
                        </td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>
    </div>

    <%@ include file="../paginator.jsp" %>

</div>
</body>
</html>

<div class="panel panel-primary">
    <div class="panel-heading">Поиск</div>

    <div class="panel-body">
        <form id="stat-params" method="get">
            <input name="reportType" id="reportType" type="hidden" value=""/>

            <div class="row ma-b-20">
                <div class="col-lg-6">
                    <label>ID проекта или название проекта</label>
                    <input id="query" name="query" type="text" placeholder="" class="form-control" value="${query}">
                </div>

                <p:if test="${withCommentTypes}">
                    <div class="col-lg-3">
                        <label>Тип комментариев</label>

                        <select class="form-control" id="commentsType" name="commentsType"
                                onchange="$('#stat-params').submit();">
                            <option value=""
                                    <c:if test="${commentsType == ''}">selected="selected"</c:if>>
                                Все комментарии
                            </option>
                            <option value="CAMPAIGN"
                                    <c:if test="${commentsType == 'CAMPAIGN'}">selected="selected"</c:if>>
                                Комментарии к проекту
                            </option>
                            <option value="POST"
                                    <c:if test="${commentsType == 'POST'}">selected="selected"</c:if>>
                                Комментарии к новостям проекта
                            </option>
                        </select>
                    </div>
                </p:if>

                <p:if test="${withTypes || withShareTypes}">
                    <div class="col-lg-3">
                        <label>Событие</label>

                        <select class="form-control" id="profileNewsType"
                                name="profileNewsType" onchange="$('#stat-params').submit();">
                            <option value=""
                                    <c:if test="${profileNewsType == ''}">selected="selected"</c:if>>
                                Все
                            </option>
                            <p:if test="${withTypes}">
                                <option value="CAMPAIGN_ON_MODERATION"
                                        <c:if test="${profileNewsType == 'CAMPAIGN_ON_MODERATION'}">selected="selected"</c:if>>
                                    Создан и отправлен на модерацию
                                </option>
                                <option value="CAMPAIGN_PAUSED"
                                        <c:if test="${profileNewsType == 'CAMPAIGN_PAUSED'}">selected="selected"</c:if>>
                                    Приостановлен
                                </option>
                                <option value="CAMPAIGN_ON_REWORK"
                                        <c:if test="${profileNewsType == 'CAMPAIGN_ON_REWORK'}">selected="selected"</c:if>>
                                    Отправлен на доработку
                                </option>
                                <option value="CAMPAIGN_DECLINED"
                                        <c:if test="${profileNewsType == 'CAMPAIGN_DECLINED'}">selected="selected"</c:if>>
                                    Отклонен
                                </option>
                                <option value="CAMPAIGN_DELETED"
                                        <c:if test="${profileNewsType == 'CAMPAIGN_DELETED'}">selected="selected"</c:if>>
                                    Удалён
                                </option>
                                <option value="CAMPAIGN_STARTED"
                                        <c:if test="${profileNewsType == 'CAMPAIGN_STARTED'}">selected="selected"</c:if>>
                                    Запущен
                                </option>
                                <option value="CAMPAIGN_FINISHED_SUCCESS"
                                        <c:if test="${profileNewsType == 'CAMPAIGN_FINISHED_SUCCESS'}">selected="selected"</c:if>>
                                    Завершен удачно
                                </option>
                                <option value="CAMPAIGN_FINISHED_FAIL"
                                        <c:if test="${profileNewsType == 'CAMPAIGN_FINISHED_FAIL'}">selected="selected"</c:if>>
                                    Завершен не удачно
                                </option>
                                <option value="CAMPAIGN_RESUMED"
                                        <c:if test="${profileNewsType == 'CAMPAIGN_RESUMED'}">selected="selected"</c:if>>
                                    Возобновлен
                                </option>
                                <option value="CAMPAIGN_REACHED_15"
                                        <c:if test="${profileNewsType == 'CAMPAIGN_REACHED_15'}">selected="selected"</c:if>>
                                    Собрал 15%
                                </option>
                                <option value="CAMPAIGN_REACHED_20"
                                        <c:if test="${profileNewsType == 'CAMPAIGN_REACHED_20'}">selected="selected"</c:if>>
                                    Собрал 20%
                                </option>
                                <option value="CAMPAIGN_REACHED_25"
                                        <c:if test="${profileNewsType == 'CAMPAIGN_REACHED_25'}">selected="selected"</c:if>>
                                    Собрал 25%
                                </option>
                                <option value="CAMPAIGN_REACHED_35"
                                        <c:if test="${profileNewsType == 'CAMPAIGN_REACHED_35'}">selected="selected"</c:if>>
                                    Собрал 35%
                                </option>
                                <option value="CAMPAIGN_REACHED_50"
                                        <c:if test="${profileNewsType == 'CAMPAIGN_REACHED_50'}">selected="selected"</c:if>>
                                    Собрал 50%
                                </option>
                                <option value="CAMPAIGN_REACHED_75"
                                        <c:if test="${profileNewsType == 'CAMPAIGN_REACHED_75'}">selected="selected"</c:if>>
                                    Собрал 75%
                                </option>
                                <option value="CAMPAIGN_REACHED_100"
                                        <c:if test="${profileNewsType == 'CAMPAIGN_REACHED_100'}">selected="selected"</c:if>>
                                    Собрал 100%
                                </option>
                            </p:if>
                            <p:if test="${withShareTypes}">
                                <option value="SHARE_ADDED"
                                        <c:if test="${profileNewsType == 'SHARE_ADDED'}">selected="selected"</c:if>>
                                    Добавление вознаграждения
                                </option>
                                <option value="SHARE_PURCHASED"
                                        <c:if test="${profileNewsType == 'SHARE_PURCHASED'}">selected="selected"</c:if>>
                                    Покупка вознаграждения
                                </option>
                                <option value="SHARE_DELETED"
                                        <c:if test="${profileNewsType == 'SHARE_DELETED'}">selected="selected"</c:if>>
                                    Удаление вознаграждения
                                </option>
                            </p:if>
                        </select>
                    </div>
                </p:if>

                <c:if test="${withManagers}">
                    <div class="col-lg-3">
                        <label for="managerId">Менеджер</label>

                        <select class="form-control" id="managerId" name="managerId"
                                onchange="$('#stat-params').submit();">
                            <option value="">- Не выбрано -</option>
                            <c:forEach var="manager" items="${managersList}">
                                <option value="${manager. managerId}" <c:if
                                        test="${manager.managerId == managerId}"> selected</c:if>>
                                    <c:out value="${manager.fullName}"/>
                                </option>
                            </c:forEach>
                        </select>
                    </div>
                </c:if>
            </div>

            <div class="row ma-b-20">
                <div class="col-lg-12">
                    <ct:date-range-picker dateFrom="${campaignEventsParam.dateFrom}"
                                          dateTo="${campaignEventsParam.dateTo}" allButton="true"/>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12 btn-group">
                    <button class="btn btn-primary" type="submit">
                        <i class="fa fa-search"></i> Поиск
                    </button>
                    <button class="btn btn-success" type="submit"
                            formaction="${reportJson}"
                            onclick="$('#reportType').val('EXCEL');">
                        <i class="fa fa-download"></i> Excel
                    </button>
                    <button class="btn btn-info" type="submit"
                            formaction="${reportJson}"
                            onclick="$('#reportType').val('CSV');">
                        <i class="fa fa-download"></i> CSV
                    </button>

                    <button class="btn btn-default js-clear-form">
                        <i class="fa fa-trash-o"></i> Очистить
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>

<script>
    $(function() {
        $(document).on('click', '.js-clear-form', function () {
            $('#stat-params input').val('');
            $('#stat-params textarea').val('');
            $('#stat-params selectCampaignById').val('');
        });
    });
</script>
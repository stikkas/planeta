<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ include file="/WEB-INF/jsp/includes/generated/doctype-header.jsp" %>
<head>
    <%@ include file="head.jsp" %>
    <script type="text/javascript">
        var campaignStatus = '${campaignStatus}' || null;
        var campaignId = '${campaignId}' || null;
        $(function() {
            $('[data-contr-id]').each(function() {
                $(this).bind('click', function() {
                    var href = '/moderator/bind-contractor.json?contractorId='+$(this).data().contrId+'&campaignId='+campaignId;
                    Modal.showConfirm("Текущий контрагент проекта будет заменен", 'Подтверждение действия',{
                        success: function() {
                            document.location.href = href;
                        }
                    });
                })
            })
        });
    </script>
</head>
<body>

<c:if test="${empty campaignId}"><%@ include file="/WEB-INF/jsp/admin/navbar.jsp" %> </c:if>
    <div id="page-wrapper">
        <c:if test="${not empty campaignId}">
            <div class="main-page-actions">
                <a class="btn btn-danger btn-circle btn-outline btn-lg" href="/moderator/campaign-moderation-info.html?campaignId=${campaignId}" title="Отмена">
                    <i class="fa fa-chevron-left"></i>
                </a>
            </div>
        </c:if>

        <c:if test="${empty campaignId}">
            <div class="main-page-actions">
                <a class="btn btn-success btn-circle btn-outline btn-lg" href="/moderator/edit-contractor.html" title="Добавить нового контрагента">
                    <i class="fa fa-plus"></i>
                </a>
            </div>
        </c:if>


        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Контрагенты</h1>
            </div>
        </div>


        <div class="row">
            <div class="col-lg-12">
                <%@ include file="searchBoxContractors.jsp" %>
            </div>
        </div>

        <div class="row ">
            <div class="col-lg-12 admin-table">
                <table class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th width="50">#</th>
                        <th width="50">Название контрагента</th>
                        <th>Тип</th>
                        <th>Страна</th>
                        <th>Город</th>
                        <th>ИНН</th>
                        <th>ОГРН</th>
                        <th>Паспорт</th>
                        <th>Привязанные проекты</th>
                        <th><div class="text-right">Действия</div></th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:forEach var="сontractor" items="${contractors}">
                        <tr>
                            <td>${сontractor.contractorId}</td>
                            <td>${сontractor.name}</td>
                            <td><c:set var="type" scope="session" value="${сontractor.type}"/><%@ include file="../includes/contractor-types.jsp" %></td>
                            <td>${сontractor.countryNameRus}</td>
                            <td>${сontractor.cityNameRus}</td>
                            <td>${сontractor.inn}</td>
                            <td>${сontractor.ogrn}</td>
                            <td>${сontractor.passportNumber}</td>
                            <td>
                                <c:forEach var="campaign" items="${сontractor.campaigns}">
                                    - ${campaign.name}<br>
                                </c:forEach>
                            </td>
                            <td>
                                <div class="text-right">
                                    <div class="btn-group">
                                        <c:choose>
                                            <c:when test="${campaignId > 0}">
                                                <a class="btn btn-outline btn-sm btn-success" href="javascript:void(0)" data-contr-id="${сontractor.contractorId}">
                                                    Прикрепить
                                                </a>
                                            </c:when>
                                            <c:otherwise>
                                                <a class="btn btn-outline btn-primary" href="/moderator/edit-contractor.html?contractorId=${сontractor.contractorId}" title="Редактировать">
                                                    <i class="fa fa-pencil" aria-hidden="true"></i>
                                                </a>
                                                <a class="btn btn-outline btn-success" href="/moderator/campaigns.html?contractorId=${сontractor.contractorId}" title="Прикрепить проект">
                                                    <i class="fa fa-paperclip" aria-hidden="true"></i>
                                                </a>
                                            </c:otherwise>
                                        </c:choose>

                                    </div>
                                </div>
                            </td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
            </div>
        </div>

        <%@ include file="paginator.jsp" %>
    </div>
</body>
</html>


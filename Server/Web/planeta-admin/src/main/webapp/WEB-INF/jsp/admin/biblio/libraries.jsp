<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ include file="/WEB-INF/jsp/includes/generated/doctype-header.jsp" %>
<%@taglib prefix="p" uri="http://planeta.ru/tags" %>
<head>
    <%@ include file="/WEB-INF/jsp/admin/head.jsp" %>
</head>
<body>
<%@ include file="/WEB-INF/jsp/admin/navbar.jsp" %>
<script type="text/javascript">
    $(document).ready(function () {
        $('.js-deleteByProfileId').click(function (e) {
            e.preventDefault();
            var el = $(e.currentTarget);
            var link = el.attr("href");
            var tr = el.closest('tr');
            if (confirm("Вы действительно хотите удалить эту библиотеку?")) {
                $.post(link).done(function (resp) {
                    if (resp.success) {
                        tr.remove();
                    }
                });
            }

        })
    });
</script>

<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Библиотеки</h1>
        </div>
    </div>

    <div class="main-page-actions">
        <a class="btn btn-success btn-circle btn-outline btn-lg" href="/admin/library/add_library.html" title="Добавить библиотеку">
            <i class="fa fa-plus"></i>
        </a>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-primary">
                <div class="panel-heading">Поиск библиотек</div>
                <div class="panel-body">
                    <form id="search-frm" method="GET" action="/admin/library/libraries.html" class="clearfix">
                        <input type="hidden" name="offset" value="0"/>
                        <input type="hidden" name="limit" value="10"/>

                        <div class="row ma-b-20">
                            <div class="col-lg-6">
                                <label>Запрос</label>
                                <input class="form-control"
                                       type="text"
                                       placeholder="Введите название или часть адреса"
                                       id="searchStr"
                                       name="searchStr"
                                       value="<c:out value="${param.searchStr}" />"/>
                            </div>

                            <div class="col-lg-3">
                                <label>Статус</label>
                                <select class="form-control" id="status" name="status">
                                    <option value="" <c:if test="${empty param.status}">selected=''</c:if>>Все</option>
                                    <option value="ACTIVE" <c:if test="${param.status eq 'ACTIVE'}">selected=''</c:if>>
                                        Активные
                                    </option>
                                    <option value="PAUSED" <c:if test="${param.status eq 'PAUSED'}">selected=''</c:if>>На
                                        паузе
                                    </option>
                                </select>
                            </div>

                            <div class="col-lg-3">
                                <label>Тип</label>
                                <select class="form-control" id="libraryType" name="libraryType">
                                    <option value="" <c:if test="${empty param.libraryType}">selected=''</c:if>>Любой</option>
                                    <option value="PUBLIC" <c:if test="${param.libraryType eq 'PUBLIC'}">selected=''</c:if>>
                                        Публичные библиотеки
                                    </option>
                                    <option value="SCHOLASTIC"
                                            <c:if test="${param.libraryType eq 'SCHOLASTIC'}">selected=''</c:if>>Школьные
                                        библиотеки
                                    </option>
                                    <option value="SCIENTIFIC"
                                            <c:if test="${param.libraryType eq 'SCIENTIFIC'}">selected=''</c:if>>Научные
                                        библиотеки
                                    </option>
                                    <option value="DOMESTIC" <c:if test="${param.libraryType eq 'DOMESTIC'}">selected=''</c:if>>
                                        Детские и семейные библиотеки
                                    </option>
                                    <option value="SPECIAL" <c:if test="${param.libraryType eq 'SPECIAL'}">selected=''</c:if>>
                                        Специализированные библиотеки
                                    </option>
                                </select>
                            </div>
                        </div>


                        <div class="row ma-b-20">
                            <div class="col-lg-12">
                                <ct:date-range-picker dateFrom="${dateFrom}" dateTo="${dateTo}" allButton="true"/>
                            </div>
                        </div>

                        <%--TODO новая админка: очистка не работает--%>
                        <div class="btn-group ma-b-20">
                            <button id="search" class="btn btn-primary">
                                <i class="fa fa-search"></i> Поиск
                            </button>
                            <button id="clear-filter" class="btn btn-defaudlt">
                                <i class="fa fa-trash"></i> Очистить
                            </button>
                        </div>

                        <div class="input-group-btn">
                            <button class="btn btn-success" formaction="libraries-report.html">
                                <i class="fa fa-download"></i> Скачать в формате Excel
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <c:if test="${empty libraries}">
        <div class="row">
            <div class="col-lg-12">
                <h2 class="text-center">
                    <small>По Вашему запросу ничего не найдено ...</small>
                </h2>
                <hr>
            </div>
        </div>
    </c:if>

    <c:if test="${not empty libraries}">
        <div class="row">
            <div class="col-lg-12 admin-table">
                <table class="table table-hover table-bordered">
                    <thead>
                    <tr>
                        <th>Название</th>
                        <th>Статус</th>
                        <th>Регион</th>
                        <th>Адрес</th>
                        <th>Контактное лицо</th>
                        <th>Сайт</th>
                        <th>Email</th>
                        <th>Комментарий</th>
                        <th>Тип</th>
                        <th>Действия</th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:forEach items="${libraries}" var="library">
                        <tr>
                            <td>
                                    ${library.name}
                            </td>
                            <td>
                                <c:choose>
                                    <c:when test="${library.status eq 'ACTIVE'}">
                                        <span class="label label-success">Активна</span>
                                    </c:when>
                                    <c:when test="${library.status eq 'PAUSED'}">
                                        <span class="label label-info">Приостановлена</span>
                                    </c:when>
                                    <c:when test="${library.status eq 'DELETED'}">
                                        <span class="label label-danger">Удалена</span>
                                    </c:when>
                                </c:choose>
                                <br>Добавлено <fmt:formatDate pattern="dd.MM.yyyy" value="${library.timeAdded}"/>
                            </td>
                            <td>
                                    ${library.region}
                            </td>
                            <td>
                                    ${library.address}
                            </td>
                            <td>
                                    ${library.contractor}
                            </td>
                            <td>
                                    ${library.site}
                            </td>
                            <td>
                                    ${library.email}
                            </td>
                            <td>
                                    ${library.comment}
                            </td>
                            <td>
                                <c:choose>
                                    <c:when test="${library.libraryType eq 'PUBLIC'}">Публичные библиотеки</c:when>
                                    <c:when test="${library.libraryType eq 'SCHOLASTIC'}">Школьные библиотеки</c:when>
                                    <c:when test="${library.libraryType eq 'SCIENTIFIC'}">Научные библиотеки</c:when>
                                    <c:when test="${library.libraryType eq 'DOMESTIC'}">Детские и семейные библиотеки</c:when>
                                    <c:when test="${library.libraryType eq 'SPECIAL'}">Специализированные библиотеки</c:when>
                                </c:choose>
                            </td>
                            <td class="col-lg-1">
                                <div class="btn-group">
                                <a class="btn btn-outline btn-primary" href="/admin/library/library.html?libraryId=${library.libraryId}" title="Редактировать">
                                    <i class="fa fa-pencil"></i>
                                </a>
                                <a class="btn btn-danger btn-outline js-delete" href="/admin/library/delete_library.json?libraryId=${library.libraryId}" title="Удалить">
                                    <i class="fa fa-trash"></i>
                                </a>
                                </div>
                            </td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
                <%@ include file="../paginator.jsp" %>
            </div>
        </div>
    </c:if>
</div>
</body>
</html>


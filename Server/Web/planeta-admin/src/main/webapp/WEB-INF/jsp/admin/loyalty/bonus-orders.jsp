<%@ page contentType="text/html; charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ include file="/WEB-INF/jsp/includes/generated/doctype-header.jsp" %>
<head>
    <%@ include file="/WEB-INF/jsp/admin/head.jsp" %>
    <title>Проекты : Бонусные заказы</title>

    <script type="application/javascript">
        var bonusOrderStatuses_ru = {
            ALL: 'Все',
            PENDING: 'Новый',
            COMPLETED: 'Завершён',
            CANCELLED: 'Аннулирован'
        };

        function translateText(selector, translation) {
            $(selector).each(function (i, el) {
                var $el = $(el);
                var translated = translation[$el.text()];
                if (translated) {
                    $el.text(translated);
                }
            });
        }


        $(document).ready(function () {
            $('.address-details-switcher').click(function (e) {
                $(this).parents('tr').next('.address-details').toggle();
            });

            $('.cancel-order').click(function (e) {
                e.preventDefault();
                var $el = $(this);

                var $modal = $('#cancel-confirmation').modal('show');
                $modal.find('.confirm').off('click').click(function (e) {
                    e.preventDefault();
                    $el.off('click');

                    $.post('/moderator/cancel-loyalty-order.json', $el.data())
                        .done(function (response) {
                            if (response.success) {
                                $el.parents('tr').find('.badge').toggleClass('badge-info badge-important').text('Аннулирован');
                                $el.closest('td').empty().append('<small class="muted">Нет доступных ...</small>');
                            }
                        })
                        .always(function () {
                            $modal.modal('hide');
                        });
                });
            });
            $('.js-complete-order').click(function (e) {
                e.preventDefault();
                var $el = $(this);

                $.post('/moderator/complete-loyalty-order.json', $el.data())
                    .done(function (response) {
                        if (response.success) {
                            $el.parents('tr').find('.badge').toggleClass('badge-info badge-success').text('Завершен');
                            $el.closest('td').empty().append('<small class="muted">Нет доступных ...</small>');
                        }
                    });
            });

            translateText('#status option', bonusOrderStatuses_ru);
        });
    </script>

    <style>
        .table td {
            vertical-align: middle;
        }

        .details-caret {
            position: relative;
            top: -15px;
            left: 70%;
            width: 13px;
            height: 7px;
            background: url(/images/arrow/blog-reit-str.png);
        }
    </style>

</head>

<body>

<div class="modal-dialog" id="cancel-confirmation" tabindex="-1" role="dialog" aria-hidden="true" style="display: none">
    <div class="modal">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h3 id="myModalLabel">Аннулирование бонусного заказа</h3>
        </div>
        <div class="modal-body">
            <p>Вы действительно хотите аннулировать заказ?</p>
        </div>
        <div class="modal-footer">
            <button class="btn" data-dismiss="modal" aria-hidden="true">Отмена</button>
            <button class="btn btn-primary confirm">Аннулировать</button>
        </div>
    </div>
</div>

<%@ include file="/WEB-INF/jsp/admin/navbar.jsp" %>

<div id="page-wrapper">

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Заказы бонусов</h1>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-primary">
                <div class="panel-heading">Поиск бонусных заказов</div>

                <div class="panel-body">
                    <form>
                        <input type="hidden" id="offset" name="offset">
                        <input type="hidden" id="limit" name="limit">

                        <div class="row ma-b-20">
                            <div class="col-lg-8">
                                <label>Запрос</label>
                                <input class="form-control" type="text" id="query" name="query" value="${param.query}">
                            </div>

                            <div class="col-lg-4">
                                <label>Статус</label>
                                <select class="form-control" id="status" name="status">
                                    <option value="" <c:if test="${empty param.status}">selected</c:if>>ALL</option>
                                    <c:forEach items="${statuses}" var="s">
                                        <option value="${s}" <c:if test="${s eq param.status}">selected</c:if>>${s}</option>
                                    </c:forEach>
                                </select>
                            </div>
                        </div>

                        <div class="row ma-b-20">
                            <div class="col-lg-12">
                                <ct:date-range-picker dateFrom="${dateFrom}" dateTo="${dateTo}" allButton="true"/>
                            </div>
                        </div>

                        <div class="btn-group">
                            <button id="search" class="btn btn-primary">
                                <i class="fa fa-search"></i>
                                Поиск
                            </button>
                            <button class="btn btn-success" type="submit"
                                    formaction="/moderator/export-loyalty-orders.csv">
                                <i class="fa fa-download"></i>
                                Скачать в формате Excel
                            </button>
                            <button id="clear-filter" class="btn btn-default">
                                <i class="fa fa-trash"></i>
                                Очистить
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12 admin-table">
            <c:choose>
                <c:when test="${empty orders}">
                    <%@ include file="/WEB-INF/jsp/admin/empty-state.jsp" %>
                </c:when>
                <c:otherwise>
                    <table class="table table-hover table-bordered">
                        <thead>
                            <tr>
                                <th style="width: 8%;">№ заказа</th>
                                <th style="width: 12%;">Статус</th>
                                <th style="width: 33%;">Бонус</th>
                                <th style="width: 15%;">Получатель</th>
                                <th style="width: 12%;">Доставка</th>
                                <th style="width: 10%;">Дата</th>
                                <th style="width: 10%;">Операции</th>
                            </tr>
                        </thead>
                        
                        <tbody>
                        <c:forEach items="${orders}" var="order" varStatus="status">
                            <tr>
                                <td>${order.orderId}</td>
                                <td>
                                    <c:choose>
                                        <c:when test="${order.paymentStatus eq 'PENDING'}">
                                            <span class="label label-info">Новый</span>
                                        </c:when>
                                        <c:when test="${order.paymentStatus eq 'COMPLETED'}">
                                            <span class="label label-success">Завершен</span>
                                        </c:when>
                                        <c:when test="${order.paymentStatus eq 'CANCELLED'}">
                                            <span class="label label-important">Аннулирован</span>
                                        </c:when>
                                        <c:otherwise>
                                            <span class="label label-muted">статус "${order.paymentStatus}"</span>
                                        </c:otherwise>
                                    </c:choose>
                                </td>
                                <c:set var="bonus" value="${order.orderObjectsInfo[0]}"/>
                                <td>
                                    <img src="${hf:getThumbnailUrl(bonus.objectImageUrl, 'USER_SMALL_AVATAR', 'GROUP')}"/>
                                    <abbr title="${bonus.objectName}">${hf:getShrinkedString(bonus.objectName, 40)}</abbr>
                                </td>
                                <td>
                                    <small>
                                        <div>
                                            Пользователь <a href="/moderator/user-info.html?profileId=${order.buyerId}">№ ${order.buyerId}</a>
                                        </div>
                                        <div>
                                            <a href="mailto:${o.buyerEmail}">${o.buyerEmail}</a>
                                        </div>
                                    </small>
                                </td>
                                <td>
                                    <small>
                                        <c:choose>
                                            <c:when test="${order.deliveryType eq 'DELIVERY'}">
                                                <div>
                                                    Доставка почтой
                                                </div>
                                                <div>
                                                    по <a href="javascript:void(0);" class="address-details-switcher">адресу
                                                    &rarr;</a>
                                                </div>
                                            </c:when>
                                            <c:when test="${order.deliveryType eq 'CUSTOMER_PICKUP'}">
                                                <a href="javascript:void(0);"
                                                   class="address-details-switcher">Самовывоз</a>
                                            </c:when>
                                            <c:when test="${empty order.deliveryType or order.deliveryType eq 'NOT_SET'}">
                                                Не задано
                                            </c:when>
                                        </c:choose>
                                    </small>
                                </td>
                                <td>
                                    <small>
                                        <div>
                                            <fmt:formatDate value="${order.timeAdded}" pattern="dd.MM.yyyy"/>
                                        </div>
                                        <div class="muted">
                                            <fmt:formatDate value="${order.timeAdded}" pattern="HH:mm"/>
                                        </div>
                                    </small>
                                </td>
                                <td class="text-right">
                                    <c:choose>
                                        <c:when test="${order.paymentStatus eq 'PENDING'}">
                                            <div class="admin-table-actions">
                                                <button class="btn btn-warning btn-outline cancel-order" data-order-id="${order.orderId}" title="Аннулировать">
                                                    Аннулировать
                                                </button>
                                                <button class="btn btn-danger btn-outline js-complete-order" data-order-id="${order.orderId}" title="Завершить">
                                                    <i class="fa fa-stop"></i>
                                                </button>
                                            </div>
                                        </c:when>
                                        <c:otherwise>
                                            <small class="muted">Нет доступных ...</small>
                                        </c:otherwise>
                                    </c:choose>
                                </td>
                            </tr>
                            <tr class="address-details" style="display: none;">
                                <td colspan="7">
                                    <div class="details-caret"></div>
                                    <c:if test="${order.deliveryAddress != null}">
                                        <c:set var="addr" value="${order.deliveryAddress}"/>
                                        <address>
                                            <strong><c:out value="${addr.fio}"/></strong><br>
                                            <c:out value="${addr.zipCode}"/>, <c:out value="${addr.country}"/>, <c:out
                                                value="${addr.city}"/>, <c:out value="${addr.address}"/><br>
                                            <abbr title="Телефон">Тел:</abbr> ${addr.phone}
                                        </address>
                                    </c:if>
                                </td>
                            </tr>
                        </c:forEach>
                        </tbody>
                    </table>

                    <div>
                        <%@ include file="/WEB-INF/jsp/admin/paginator.jsp" %>
                    </div>
                    <div class="mrg-t-30"></div>

                </c:otherwise>
            </c:choose>
        </div>
    </div>

</div>
</body>
</html>


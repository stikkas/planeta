<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ include file="/WEB-INF/jsp/includes/generated/doctype-header.jsp" %>
<head>
    <title>Загрузить картинку</title>
    <link type="text/css" rel="stylesheet" href="//${hf:getStaticBaseUrl("")}/css/jquery.Jcrop.css">
    <link type="text/css" rel="stylesheet" href="//${hf:getStaticBaseUrl("")}/css/admin.Crop.css">

    <%@ include file="head.jsp" %>

    <script type="text/javascript">
        $(document).ready(function () {

            var justUploadedImages = [];
            var profileId = workspace.appModel.get('myProfile').get('profileId');

            $('.upload-file').bind('click', function () {
                UploadController.showUploadImages(profileId, function (filesUploaded) {
                    if (_.isEmpty(filesUploaded)) return;

                    var images = _.map(filesUploaded.models, function (fileUploaded) {
                        return fileUploaded.get('uploadResult');
                    });

                    location.href = location.pathname + "?" + $.param({
                        limit: images.length,
                        offset: 0
                    });
                });
            });

            $('.deleteByProfileId').on('click', function (e) {

                if (!confirm('Удалить фото?')) {
                    return;
                }
                var jPhoto = $(e.target).closest('[data-photo-id]');
                var photoId = jPhoto.attr('data-photo-id');

                var options = {
                    url: '/api/profile/deleteByProfileId-photo.json',
                    data: {
                        profileId: profileId,
                        photoId: photoId
                    },
                    context: this,
                    success: function (response) {
                        if (response.success) {
                            jPhoto.remove();
                            workspace.appView.showSuccessMessage('Фото удалено', 2500);
                        } else {
                            workspace.appView.showErrorMessage(response.errorMessage, 2500);
                        }
                    }
                };

                Backbone.sync('delete', this, options);
            });

            $('.crop').on('click', function (e) {
                var viewType = BaseView.extend({
                    template: '#basicModal'
                });

                var jPhoto = $(e.target).closest('[data-photo-id]');
                var a = jPhoto.find('.js-original-image-url');
                var url = a.attr('href');
                var photoId = jPhoto.attr('data-photo-id');


                UploadController.onCropClick(url, photoId, profileId, function (model) {
                    Modal.showAlert(model.get('imageUrl'), 'Скопируйте URL-адресс кропнутого изображения', null);
                });
            });
        })
    </script>
</head>
<body>

<%@ include file="/WEB-INF/jsp/admin/navbar.jsp" %>

<div id="page-wrapper">

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Загрузка картинок</h1>
        </div>
    </div>

    <div class="main-page-actions">
        <button class="btn btn-primary btn-circle btn-outline btn-lg upload-file"
                title="Загрузить изображения">
            <i class="fa fa-upload"></i>
        </button>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="images">
                <c:forEach items="${photosUploadedBefore}" var="photo">
                    <div class="images-preview" data-photo-id="${photo.photoId}">
                        <div class="img-wrap">
                            <img src="${hf:getThumbnailUrl(photo.imageUrl, 'PHOTOPREVIEW', 'PHOTO')}">
                        </div>

                        <div class="btn-group image-actions" title="Размеры">
                            <div class="btn-group col-lg-4">
                                <button type="button" class="btn btn-primary dropdown-toggle"
                                        data-toggle="dropdown"
                                        aria-haspopup="true"
                                        aria-expanded="false">

                                    <i class="fa fa-search-plus"></i> <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu">
                                    <li><a href="${photo.imageUrl}" target="_blank" class="js-original-image-url">Оригинал</a></li>
                                    <li><a href="${hf:getThumbnailUrl(photo.imageUrl, 'ORIGINAL', 'PHOTO')}" target="_blank">Пережатый оригинал</a></li>
                                    <li><a href="${hf:getThumbnailUrl(photo.imageUrl, 'BIG', 'PHOTO')}" target="_blank">Средний размер</a></li>
                                    <li><a href="${hf:getThumbnailUrl(photo.imageUrl, 'MEDIUM', 'PHOTO')}" target="_blank">Для страницы "Общайся"</a></li>
                                    <li><a href="${hf:getThumbnailUrl(photo.imageUrl, 'USER_SMALL_AVATAR', 'PHOTO')}" target="_blank">Маленький квадрат</a></li>
                                    <li><a href="${hf:getThumbnailUrl(photo.imageUrl, 'PRODUCT_COVER_NEW', 'PHOTO')}" target="_blank">Большой квадрат</a></li>
                                </ul>
                            </div>

                            <button class="btn btn-danger delete col-lg-4" title="Удалить">
                                <i class="fa fa-trash"></i>
                            </button>

                            <button class="btn btn-success crop col-lg-4" title="Обрезать">
                                <i class="fa fa-crop"></i>
                            </button>
                        </div>
                    </div>
                </c:forEach>
            </div>
        </div>
    </div>

    <%@ include file="paginator.jsp" %>
</div>
</body>



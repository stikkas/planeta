<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ include file="/WEB-INF/jsp/includes/generated/doctype-header.jsp" %>
<head>
    <%@include file="../head.jsp" %>
</head>

<body>
<%@ include file="/WEB-INF/jsp/admin/navbar.jsp" %>

<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Кампусы</h1>
        </div>
    </div>

    <div class="main-page-actions">
        <form method="get">
            <a href="/admin/edit-campus.html" class="btn btn-success btn-circle btn-outline btn-lg" title="Добавить">
                <i class="fa fa-plus"></i>
            </a>
        </form>
    </div>

    <div class="row">
        <div class="col-lg-12 admin-table">
            <table class="table table-bordered table-striped">
                <thead>
                <th>Эмблема</th>
                <th>Название</th>
                <th>Короткое название</th>
                <th>Категория</th>
                <th>Действие</th>
                </thead>
                <tbody>
                <c:forEach items="${campuses}" var="campus">
                    <tr>
                        <td>
                            <img src="${campus.imageUrl}" alt="${campus.shortName}" style="max-width: 300px">
                        </td>
                        <td>${campus.name}</td>
                        <td>${campus.shortName}</td>
                        <td>${campus.tagName}</td>
                        <td class="text-right">
                            <a class="btn btn-primary btn-outline"
                               href="/admin/edit-campus.html?campusId=${campus.id}"
                               title="Редактировать">
                                <i class="fa fa-pencil"></i>
                            </a>
                        </td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>
    </div>
</body>




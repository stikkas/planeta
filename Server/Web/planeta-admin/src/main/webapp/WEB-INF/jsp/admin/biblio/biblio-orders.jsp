<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="p" uri="http://planeta.ru/tags" %>
<html>
<head>
    <title>Библиотечные заказы</title>
    <%@ include file="../head.jsp" %>

    <script type="text/javascript">
        function cancelOrder(orderid) {
            if (confirm('Аннулировать заказ?')) {
                $.post("/admin/billing/cancel-order.json", {orderId: orderid}, function (response) {
                    if (response.success) {
                        alert("Заказ аннулирован");
                    } else {
                        alert(response.errorMessage);
                    }
                })
            }
            return false;
        }
    </script>
</head>
<body>

<%@ include file="/WEB-INF/jsp/admin/navbar.jsp" %>

<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12 admin-table">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Заказы библиородины</h1>
                </div>
            </div>

            <c:choose>
                <c:when test="${empty ordersInfo}">
                    <%@ include file="/WEB-INF/jsp/admin/empty-state.jsp" %>
                </c:when>
                <c:otherwise>
                    <table class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>№ Заказа<br> Дата</th>
                            <th>Пользователь</th>
                            <th>Журналы</th>
                            <th>Библиотеки</th>
                            <th>Сумма</th>
                            <th>Статус</th>
                            <th></th>
                        </tr>
                        </thead>

                        <c:forEach items="${ordersInfo}" var="orderInfo" varStatus="varStatus">
                            <tr>
                                <td id="messageContentForOrder${orderInfo.orderId}" colspan="6"
                                    style="padding:0;border:0;"></td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="purchases-number">№${orderInfo.orderId}</div>
                                    <div class="muted">
                                        <small>
                                            <nobr>${hf:dateFormat(orderInfo.timeAdded)}</nobr>
                                        </small>
                                    </div>
                                </td>
                                <td>
                                    <div class="order-user-name"><a
                                            href="${mainAppUrl}/${orderInfo.buyerId}">${orderInfo.buyerName}</a>
                                    </div>
                                    <div class="order-user-mail">${orderInfo.buyerEmail}</div>
                                    <c:if test="${orderInfo.investInfo != null}">
                                        <br/>
                                        <div><a class="btn"
                                                href="/admin/investing-order-info.html?orderId=${orderInfo.orderId}">Посмотреть
                                            реквизиты</a></div>
                                    </c:if>
                                </td>
                                <td>
                                    <c:forEach var="orderObjectsGroup"
                                               items="${hf:groupOrderObjectsByObjectId(orderInfo.orderObjectsInfo)}">
                                        <c:set var="orderObjectsInfo" value="${orderObjectsGroup.value}"/>
                                        <c:set var="orderObjectInfo" value="${orderObjectsInfo[0]}"/>
                                        <c:set var="orderObjectInfoCount" value="${fn:length(orderObjectsInfo)}"/>
                                        <c:if test="${orderObjectInfo.objectType == 'BOOK'}">

                                            <div class="media">
                                                <a class="pull-left"
                                                   href="https://${properties['biblio.application.host']}">
                                                    <img src='${hf:getThumbnailUrl(orderObjectInfo.objectImageUrl, "USER_SMALL_AVATAR", "PRODUCT")}'
                                                         alt="${orderObjectInfo.objectName}">
                                                </a>
                                                <div class="media-body">
                                                    <div>
                                                        <a href="https://${properties['biblio.application.host']}">${orderObjectInfo.objectName}</a>
                                                    </div>
                                                    <div class="muted">
                                                        <small>${orderObjectInfoCount} шт., ${orderObjectInfo.price}
                                                            руб.
                                                        </small>
                                                    </div>
                                                </div>
                                            </div>
                                        </c:if>
                                    </c:forEach>
                                </td>
                                <td>
                                    <div class="order-address">
                                        <c:forEach var="orderObjectsGroup"
                                                   items="${hf:groupOrderObjectsByObjectId(orderInfo.orderObjectsInfo)}">
                                            <c:set var="orderObjectsInfo" value="${orderObjectsGroup.value}"/>
                                            <c:set var="orderObjectInfo" value="${orderObjectsInfo[0]}"/>
                                            <c:set var="orderObjectInfoCount" value="${fn:length(orderObjectsInfo)}"/>
                                            <c:if test="${orderObjectInfo.objectType == 'LIBRARY'}">

                                                <div class="media">
                                                    <div class="media-body">
                                                        <div>
                                                                ${orderObjectInfo.objectName}
                                                        </div>
                                                    </div>
                                                </div>
                                            </c:if>
                                        </c:forEach>
                                    </div>
                                </td>
                                <td>
                                    <div class="lead">
                                        <nobr><b> ${orderInfo.totalPrice} руб.</b></nobr>
                                    </div>
                                </td>
                                <td>
                                    <form id="biblio-status-form">
                                        <c:choose>
                                            <c:when test="${orderInfo.paymentStatus == 'COMPLETED'}">
                                                <span class="label label-success">Оплачен</span>
                                            </c:when>
                                            <c:when test="${orderInfo.paymentStatus == 'CANCELLED'}">
                                                <span class="label label-info">Аннулирован</span>
                                            </c:when>
                                        </c:choose>
                                    </form>
                                </td>
                                <td>
                                    <button class="btn btn-warning" onclick="cancelOrder(${orderInfo.orderId})">
                                        Аннулировать
                                    </button>
                                </td>
                            </tr>
                        </c:forEach>
                    </table>
                </c:otherwise>
            </c:choose>
        </div>
    </div>

    <%@ include file="../paginator.jsp" %>
</div>
</body>
</html>

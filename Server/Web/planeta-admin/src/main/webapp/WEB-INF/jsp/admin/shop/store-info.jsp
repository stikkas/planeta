<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ include file="/WEB-INF/jsp/includes/generated/doctype-header.jsp" %>
<head>
    <%@ include file="/WEB-INF/jsp/admin/head.jsp" %>
    <link type="text/css" rel="stylesheet" href="//${hf:getStaticBaseUrl("")}/css-generated/shop-admin.css"/>
    <%--<link type="text/css" rel="stylesheet" href="//${hf:getStaticBaseUrl("")}/css/jquery.fancybox.css"/>--%>
    <title>Админка магазина сообщества <c:if test="${currentGroup.name!=null}">"${currentGroup.name}"</c:if><c:if test="${currentGroup.name==null}">#${currentGroup.profileId}</c:if></title>

    <script type="text/javascript">
        ShopUtils.setProductTags(${hf:toJson(productTags)});

        $(document).ready(function() {

            var products = new Shop.Models.Products({
                tagId: ${tagId}
            });
            new Shop.Views.Products({
                el: '.content-holder',
                model: products
            }).render();
            products.fetch();

        });
    </script>
</head>

<body>

<%@ include file="/WEB-INF/jsp/admin/navbar.jsp" %>

<div id="page-wrapper">

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Товары</h1>
        </div>
    </div>

    <div class="main-page-actions">
        <a class="btn btn-success btn-circle btn-outline btn-lg" href="/admin/products/0/edit" title="Добавить товар">
            <i class="fa fa-plus"></i>
        </a>
    </div>

    <div class="content-holder"></div>
</div>

</body>
</html>

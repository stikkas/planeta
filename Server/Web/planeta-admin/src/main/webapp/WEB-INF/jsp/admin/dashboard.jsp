<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ include file="/WEB-INF/jsp/includes/generated/doctype-header.jsp" %>
<head>
    <%@ include file="head.jsp" %>

    <script>
        $(function () {
            var stats = JSON.parse('${stats}');
            var yearStats= JSON.parse('${yearStats}');

            var shareStatistic = [];
            var shareYearStatistic = [];
            var shopStatistic = [];
            var shopYearStatistic = [];
            var biblioStatistic = [];
            var biblioYearStatistic = [];

            for (var key in stats) {
                var value = stats[key];

                var array = key.split(' ');
                var date = new Date(array[1] + ' ' +  array[2] + ' ' + array[5]);

                shareStatistic.push({
                    date: date.format("Y-m-d"),
                    value: value.shares.length > 0 ? value.shares[0].amount : 0
                });

                shopStatistic.push({
                    date: date.format("Y-m-d"),
                    value: value.products.length > 0 ? value.products[0].amount : 0
                });

                biblioStatistic.push({
                    date: date.format("Y-m-d"),
                    value: value.biblioPurchases.length > 0 ? value.biblioPurchases[0].amount : 0
                });
            }

            for (var key in yearStats) {
                var value = yearStats[key];

                var array = key.split(' ');
                var date = new Date(array[1] + ' ' +  array[2] + ' ' + array[5]);

                shareYearStatistic.push({
                    date: date.format("Y-m-d"),
                    value: value.shares.length > 0 ? value.shares[0].amount : 0
                });

                shopYearStatistic.push({
                    date: date.format("Y-m-d"),
                    value: value.products.length > 0 ? value.products[0].amount : 0
                });

                biblioYearStatistic.push({
                    date: date.format("Y-m-d"),
                    value: value.biblioPurchases.length > 0 ? value.biblioPurchases[0].amount : 0
                });
            }

            new Morris.Line({
                element: 'shares-stats',
                data: shareStatistic,
                xkey: 'date',
                ykeys: ['value'],
                labels: ['Сумма'],
                pointSize: 4,
                hideHover: 'auto',
                resize: true
            });

            new Morris.Line({
                element: 'shares-year-stats',
                data: shareYearStatistic,
                xkey: 'date',
                ykeys: ['value'],
                labels: ['Сумма'],
                pointSize: 4,
                hideHover: 'auto',
                resize: true
            });

            new Morris.Line({
                element: 'shop-stats',
                data: shopStatistic,
                xkey: 'date',
                ykeys: ['value'],
                labels: ['Сумма'],
                pointSize: 4,
                hideHover: 'auto',
                resize: true
            });


            new Morris.Line({
                element: 'shop-year-stats',
                data: shopYearStatistic,
                xkey: 'date',
                ykeys: ['value'],
                labels: ['Сумма'],
                pointSize: 4,
                hideHover: 'auto',
                resize: true
            });

            new Morris.Line({
                element: 'biblio-stats',
                data: biblioStatistic,
                xkey: 'date',
                ykeys: ['value'],
                labels: ['Сумма'],
                pointSize: 4,
                hideHover: 'auto',
                resize: true
            });


            new Morris.Line({
                element: 'biblio-year-stats',
                data: biblioYearStatistic,
                xkey: 'date',
                ykeys: ['value'],
                labels: ['Сумма'],
                pointSize: 4,
                hideHover: 'auto',
                resize: true
            });
        })
    </script>
</head>
<body>
<%@ include file="/WEB-INF/jsp/admin/navbar.jsp" %>

<div id="page-wrapper">

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Сводка</h1>
        </div>
    </div>

    <div class="main-page-actions">
        <a class="btn btn-primary btn-circle btn-outline btn-lg upload-file"
           href="/dashboard.html?flushCache=true"
           title="Обновить статистику">
            <i class="fa fa-refresh"></i>
        </a>
    </div>

    <div class="row ma-b-30 general-stats">
        <div class="col-lg-2 col-xs-6 tile_stats_count">
            <span class="count_top" title="Активных проектов">
                <i class="fa fa-file-o"></i> Активных проектов
            </span>
            <div class="count">${activeCampaigns}</div>
        </div>
        <div class="col-lg-2 col-xs-6 tile_stats_count">
            <span class="count_top" title="Проектов завершается в ближайшие 10 дней">
                <i class="fa fa-fire"></i> Проектов завершается в ближайшие 10 дней
            </span>
            <div class="count">${campaignsEndIn10Days}</div>
        </div>
        <div class="col-lg-2 col-xs-6 tile_stats_count">
            <span class="count_top" title="Товаров в продаже">
                <i class="fa fa-shopping-cart"></i> Товаров в продаже
            </span>
            <div class="count">${totalProductCount}</div>
        </div>
        <div class="col-lg-2 col-xs-6 tile_stats_count">
            <span class="count_top" title="Товаров куплено">
                <i class="fa fa-cart-arrow-down"></i> Товаров куплено
            </span>
            <div class="count">${totalPurchaseCount}</div>
        </div>
        <div class="col-lg-2 col-xs-6 tile_stats_count">
            <span class="count_top" title="Активных изданий">
                <i class="fa fa-book"></i> Активных изданий
            </span>
            <div class="count">${activeBooksCount}</div>
        </div>
        <div class="col-lg-2 col-xs-6 tile_stats_count">
            <span class="count_top" title="Активных библиотек">
                <i class="fa fa-building"></i> Активных библиотек
            </span>
            <div class="count">${activeLibrariesCount}</div>
        </div>
    </div>

    <div class="row ma-b-20">
        <div class="col-lg-7">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <i class="fa fa-bar-chart-o fa-fw"></i> Статистика по продажам акций за последние 60 дней
                </div>
                <div class="panel-body">
                    <div id="shares-stats" style="height: 300px;"></div>
                </div>
            </div>
        </div>

        <div class="col-lg-5">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <i class="fa fa-bar-chart-o fa-fw"></i> Статистика по продажам акций за год
                </div>
                <div class="panel-body">
                    <div id="shares-year-stats" style="height: 300px;"></div>
                </div>
            </div>
        </div>
    </div>

    <div class="row ma-b-20">
        <div class="col-lg-7">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <i class="fa fa-bar-chart-o fa-fw"></i> Статистика по продажам магазина за последние 60 дней
                </div>
                <div class="panel-body">
                    <div id="shop-stats" style="height: 300px;"></div>
                </div>
            </div>
        </div>

        <div class="col-lg-5">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <i class="fa fa-bar-chart-o fa-fw"></i> Статистика по продажам магазина за год
                </div>
                <div class="panel-body">
                    <div id="shop-year-stats" style="height: 300px;"></div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-7">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <i class="fa fa-bar-chart-o fa-fw"></i> Статистика по продажам Библиородины за последние 60 дней
                </div>
                <div class="panel-body">
                    <div id="biblio-stats" style="height: 300px;"></div>
                </div>
            </div>
        </div>

        <div class="col-lg-5">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <i class="fa fa-bar-chart-o fa-fw"></i> Статистика по продажам Библиородины за год
                </div>
                <div class="panel-body">
                    <div id="biblio-year-stats" style="height: 300px;"></div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>

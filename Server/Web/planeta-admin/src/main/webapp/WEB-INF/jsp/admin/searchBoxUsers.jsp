<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<form method="GET" action="${param.action}">

    <div class="form-group input-group">
        <input type="text" class="form-control"
               value="${searchString}"
               placeholder="Гараж Петрович  - введите ID, короткий адрес, или любое слово для поиска"
               id="xlInput"
               name="searchString">
        <span class="input-group-btn">
            <button class="btn btn-primary" type="submit">
                <i class="fa fa-search"></i>
            </button>
        </span>
    </div>
</form>

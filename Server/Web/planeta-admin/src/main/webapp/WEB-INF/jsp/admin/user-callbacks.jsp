<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ include file="/WEB-INF/jsp/includes/generated/doctype-header.jsp" %>
<%@taglib prefix="p" uri="http://planeta.ru/tags" %>
<head>
    <%@ include file="/WEB-INF/jsp/admin/head.jsp" %>

    <title>Заявки на обратный звонок</title>

</head>
<body>
<script>
    function processCallback(id) {
        $.post('/api/callback/process.json', {userCallbackId: id}, function (response) {
            if (response.success) {
                document.location.reload();
            } else {
                alert(response.errorMessage);
            }
        });
    }

    $(function() {
        $('#clear-filter').click(function(e) {
            e.preventDefault();
            $('form').find('input,selectCampaignById').val('');
            $('#allTimeButton').click();
        });
    })
</script>

<%@ include file="/WEB-INF/jsp/admin/navbar.jsp" %>

<div id="page-wrapper">

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Заявки на обратный звонок</h1>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-primary">
                <div class="panel-heading">Поиск заявок на обратный звонок</div>

                <div class="panel-body">
                    <form id="search-frm" method="GET" action="/moderator/user-callbacks.html" class="clearfix">
                        <div class="row ma-b-20">
                            <div class="col-lg-8">
                                <label>Запрос</label>
                                <input class="form-control"
                                       type="text"
                                       placeholder="Введите email, телефон, алиас или Id пользователя"
                                       id="searchStr" name="searchStr" value="${param.searchStr}">
                                <input type="hidden" name="offset" value="0"/>
                                <input type="hidden" name="limit" value="10"/>
                            </div>

                            <div class="col-lg-4">
                                <label>Статус</label>
                                <select class="form-control" id="processed" name="processed">
                                    <option value="-1" <c:if test="${param.processed eq '-1'}">selected</c:if>>Все</option>
                                    <option value="0"
                                            <c:if test="${param.processed eq null or param.processed eq '' or param.processed eq '0'}">selected</c:if>>
                                        Новые
                                    </option>
                                    <option value="1" <c:if test="${param.processed eq '1'}">selected</c:if>>Обработанные
                                    </option>
                                </select>
                            </div>
                        </div>

                        <div class="row ma-b-20">
                            <div class="col-lg-12">
                                <ct:date-range-picker dateFrom="${dateFrom}" dateTo="${dateTo}" allButton="true"/>
                            </div>
                        </div>

                        <button id="search" class="btn btn-primary">
                            <i class="fa fa-search"></i> Поиск
                        </button>

                        <button id="clear-filter" class="btn btn-default">
                            <i class="fa fa-trash"></i> Очистить
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>


    <c:if test="${empty callbacks}">
        <div class="row">
            <div class="col-lg-12">
                <h2 class="text-center">
                    <small>По Вашему запросу ничего не найдено ...</small>
                </h2>
                <hr>
            </div>
        </div>
    </c:if>

    <c:if test="${not empty callbacks}">
        <div class="row">
            <div class="col-lg-12 admin-table">
                <table class="table table-hover table-bordered">
                    <thead>
                    <tr>
                        <th>Тема заявки</th>
                        <th>Статус</th>
                        <th>Проект</th>
                        <th>Телефон</th>
                        <th>Пользователь</th>
                        <th>Время создания</th>
                        <th>Время обработки</th>
                        <th>Менеджер</th>
                        <th>Комментарии</th>
                        <th>Действия</th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:forEach items="${callbacks}" var="callback" varStatus="status">
                        <tr>
                            <td>
                                <small>
                                    <div>
                                        <c:choose>
                                            <c:when test="${callback.type eq 'FAILURE_PAYMENT'}">
                                                Платеж <a
                                                    href="/admin/billing/payment.html?paymentId=${callback.paymentId}">№${callback.paymentId}</a>
                                                <br>
                                                Заказ: <a
                                                    href="/admin/billing/order.html?orderId=${callback.orderId}">№${callback.orderId}</a>
                                            </c:when>
                                            <c:when test="${callback.type eq 'ORDERING_PROBLEM'}">
                                                <c:if test="${callback.orderType eq 'SHARE'}">
                                                    Заказ вознаграждения <a
                                                        href="${mainAppUrl}/campaigns/${callback.orderObject.campaignId}/donate/${callback.orderObject.shareId}">${callback.orderObject.name}</a>
                                                </c:if>
                                            </c:when>
                                        </c:choose>
                                    </div>
                                    <div>
                                        на сумму <b>${callback.amount}</b> руб.
                                    </div>
                                </small>
                            </td>
                            <td>
                                <c:if test="${not callback.processed}">
                                    <span class="label label-info">Новая</span>
                                </c:if>
                                <c:if test="${callback.processed}">
                                    <span class="label label-success">Обработана</span>
                                </c:if>
                            </td>
                            <td>
                                <c:if test="${not empty callback.campaignName}">
                                    <a href="${mainAppUrl}/campaigns/${callback.campaignAlias}"> ${callback.campaignName}</a>
                                </c:if>
                            </td>
                            <td>${callback.userPhone}</td>
                            <td>
                                <c:if test="${callback.userId gt 0}">
                                    <small>
                                        <div>
                                            Пользователь <a
                                                href="/moderator/user-info.html?profileId=${callback.userId}">${callback.userName}</a>
                                        </div>
                                        <div>
                                            <a href="mailto:${callback.userEmail}">${callback.userEmail}</a>
                                        </div>
                                    </small>
                                </c:if>
                                <c:if test="${callback.userId <= 0}">
                                    Аноним
                                </c:if>
                            </td>
                            <td>
                                <small>
                                    <div>
                                        <fmt:formatDate value="${callback.timeAdded}" pattern="dd.MM.yyyy"/>
                                    </div>
                                    <div class="muted">
                                        <fmt:formatDate value="${callback.timeAdded}" pattern="HH:mm"/>
                                    </div>
                                </small>
                            </td>
                            <td>
                                <small>
                                    <div>
                                        <fmt:formatDate value="${callback.timeUpdated}" pattern="dd.MM.yyyy"/>
                                    </div>
                                    <div class="muted">
                                        <fmt:formatDate value="${callback.timeUpdated}" pattern="HH:mm"/>
                                    </div>
                                </small>
                            </td>
                            <td>
                                <c:if test="${callback.managerId != 0}">
                                    Менеджер <a
                                        href="/moderator/user-info.html?profileId=${callback.managerId}">${callback.managerName}</a>
                                </c:if>
                            </td>
                            <td>
                                <a class="btn btn-info btn-outline"
                                   href="/moderator/user-callback-comment.html?callbackId=${callback.userCallbackId}" title="Комментировать">
                                    <i class="fa fa-comment"></i>
                                </a>
                                </br>
                                <a href="/moderator/user-callback-comment.html?callbackId=${callback.userCallbackId}">${callback.commentsCount}&nbsp;<spring:message
                                        code="decl.comments"
                                        arguments="${hf:plurals(callback.commentsCount)}"></spring:message></a>
                            </td>
                            <td>
                                <c:if test="${not callback.processed}">
                                    <button class="btn btn-success btn-outline" onclick="processCallback(${callback.userCallbackId})" title="Обработать">
                                        <i class="fa fa-check"></i>
                                    </button>
                                </c:if>
                            </td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
                <%@ include file="paginator.jsp" %>
            </div>
        </div>
    </c:if>
</div>
</body>
</html>


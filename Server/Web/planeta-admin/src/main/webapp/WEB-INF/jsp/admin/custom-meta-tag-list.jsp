<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ include file="/WEB-INF/jsp/includes/generated/doctype-header.jsp" %>
<head>
    <%@ include file="head.jsp" %>
    <link type="text/css" rel="stylesheet" href="/css/utils/bootstrap-table.min.css"/>
    <script type="text/javascript" src="/js/utils/bootstrap-table.min.js"/>
    <script type="text/javascript" src="/js/utils/bootstrap-table-ru-RU.min.js"/>
    <script type="text/javascript">
        function titleSorter(a,b) {
            var title1 = $(a).find("[sort-attr]='title'").text().replace(new RegExp('"','g'), ''),
                title2 = $(b).find("[sort-attr]='title'").text().replace(new RegExp('"','g'), '');
            if (title1 === "" || title2 === "" || title1 > title2) return -1;
            if (title1 < title2) return 1;
            return 0;
        }
    </script>
    <script type="text/javascript">
        function actionFormatter(value, row, index) {
            return [
                '<div class="btn-group">',
                    '<a href="javascript:void(0)" class="js-edit-href btn btn-outline btn-primary" title="Редактировать">',
                        '<i class="fa fa-pencil"></i>',
                    '</a>',
                    '<a href="javascript:void(0)" class="js-deleteByProfileId-href btn btn-outline btn-danger" title="Удалить">',
                        '<i class="fa fa-trash"></i>',
                    '</a>',
                '</div>'
            ].join('');
        }
        window.actionEvents = {
            'click .js-delete-href': function (e, value, row, index) {
                var self = this;
                Modal.showConfirm("Уверены что хотите удалить данный тег?", 'Подтверждение действия',{
                    success: function() {
                        $.ajax({
                            url: '/admin/custom-meta-tag-deleteByProfileId-new.json?customMetaTagId=' + parseInt($(row.id).text()),
                            success: function(response) {
                                document.location.href = "/admin/custom-meta-tag-list.html?customMetaTagType=" + $('#eventsTable').attr('data-custom').replace(RegExp('"','g'),'');
                            }
                        });
                    }
                });
            },
            'click .js-edit-href': function (e, value, row, index) {
                document.location.href =  "/admin/custom-meta-tag-edit-new.html?tagPath="
                + encodeURIComponent($('a', row.path).text()).replace(RegExp('"','g'),'')
                + "&customMetaTagType=" + $('#eventsTable').attr('data-custom').replace(RegExp('"','g'),'')
                + "&customMetaTagId=" + parseInt($(row.id).text());
            }
        };
    </script>

<body>

<%@ include file="/WEB-INF/jsp/admin/navbar.jsp" %>

<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <div class="mrg-b-30">
                <h1 class="page-header">Кастомные мета теги. ${customMetaTagTypeRus}</h1>
            </div>
        </div>
    </div>

    <div class="main-page-actions">
        <a class="btn btn-success btn-circle btn-outline btn-lg" href="custom-meta-tag-edit-new.html?customMetaTagType=${customMetaTagTypeId}" title="Добавить мета тег">
            <i class="fa fa-plus"></i>
        </a>
    </div>

    <div class="row">
        <div class="col-lg-12 admin-table">
            <table  id="eventsTable"
                    class="table table-striped table-hover table-bordered"
                    data-toggle="table"
                    data-sort-name="id"
                    data-sort-order="asc"
                    data-pagination="true"
                    data-search="true"
                    data-toolbar="#toolbar"
                    data-click-to-select="true"
                    data-custom="${customMetaTagTypeId}">
                <thead>
                <th data-field="id"
                    data-sortable="true">#</th>
                <th data-field="path"
                    data-sortable="true">Ссылка</th>
                <th data-field="title"
                    data-sortable="true"
                    data-sorter="titleSorter">Список тегов</th>
                <th data-field="action"
                    data-formatter="actionFormatter"
                    data-events="actionEvents">Действия</th>
                </thead>
                <tbody>
                <c:forEach var="item" items="${customMetaTags}" varStatus="st">
                    <tr>
                        <td>
                            <b>${item.customMetaTagId}</b>
                        </td>
                        <td>
                            <b><a href="https://${properties["application.host"]}${item.tagPath}">${item.tagPath}</a></b>
                        </td>
                        <td>
                            <dl class="dl-horizontal">
                                <c:if test="${not empty item.title}">
                                    <dt>title:</dt>
                                    <dd sort-attr="title">${item.title}</dd>
                                </c:if>
                                <c:if test="${not empty item.description}">
                                    <dt>description:</dt>
                                    <dd>${item.description}</dd>
                                </c:if>
                                <c:if test="${not empty item.keywords}">
                                    <dt>keywords:</dt>
                                    <dd>${item.keywords}</dd>
                                </c:if>
                                <c:if test="${not empty item.ogTitle}">
                                    <dt>ogTitle:</dt>
                                    <dd>${item.ogTitle}</dd>
                                </c:if>
                                <c:if test="${not empty item.ogDescription}">
                                    <dt>ogDescription:</dt>
                                    <dd>${item.ogDescription}</dd>
                                </c:if>
                                <c:if test="${not empty item.image}">
                                    <dt>image:</dt>
                                    <dd>${item.image}</dd>
                                </c:if>
                            </dl>
                        </td>
                        <td class="col-lg-1">
                        </td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>
    </div>
</div>
</body>
</html>

<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ include file="/WEB-INF/jsp/includes/generated/doctype-header.jsp" %>
<head>
    <%@include file="head.jsp" %>
</head>
<body>

<%@ include file="/WEB-INF/jsp/admin/navbar.jsp" %>

<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Спонсоры</h1>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <form action="<c:url value="/admin/save-sponsor.html"/>" method="post">
                <div class="row ma-b-20">
                    <div class="col-lg-4">
                        <label>Алиас</label>                         
                        <input type="text" name="alias" value="${sponsor.alias}" class="form-control" required="" <c:if test="${not empty sponsor.alias}">readonly="true"</c:if>>
                    </div>

                    <div class="col-lg-4">
                        <label>Множитель</label>
                        <input type="number" name="multiplier" value="${sponsor.multiplier}" class="form-control" required="">
                    </div>

                    <div class="col-lg-4">
                        <label>Максимальная допустимая сумма</label>
                        <input type="number" name="maxDonate" value="${sponsor.maxDonate}" class="form-control">
                    </div>
                </div>

                <div class="row ma-b-20">
                    <div class="col-lg-12">
                        <label>Код для карточки проекта</label>
                        <textarea rows="20" class="form-control" name="projectCardHtml"><c:out value="${sponsor.projectCardHtml}"/></textarea>
                    </div>
                </div>

                <div class="row ma-b-20">
                    <div class="col-lg-12">
                        <label>Код для страницы проекта</label>
                        <textarea rows="20" class="form-control" name="projectPageHtml"><c:out value="${sponsor.projectPageHtml}"/></textarea>
                    </div>
                </div>

                <div class="row ma-b-20">
                    <div class="col-lg-12">
                        <label>Код для страницы поиска</label>
                        <textarea rows="20" class="form-control" name="searchPageHtml"><c:out value="${sponsor.searchPageHtml}"/></textarea>
                    </div>
                </div>

                <div class="btn-group">
                    <input type="submit" class="btn btn-primary" value="Сохранить">
                    <a href="/admin/sponsors.html" class="btn btn-default">Отмена</a>
                </div>
            </form>
        </div>
    </div>
</body>

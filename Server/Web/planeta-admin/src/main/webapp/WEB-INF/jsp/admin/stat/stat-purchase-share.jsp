<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ include file="/WEB-INF/jsp/includes/taglibs.jsp" %>

<div id="info">
    <table class="table table-bordered table-striped">

        <thead>
            <th>Автор проекта</th>
            <th>Проект</th>
            <th>Вознаграждение, Цена</th>
            <th>Количество</th>
            <th>Сумма</th>
        </thead>

        <tbody>
        <c:forEach var="purchase" items="${purchaseReport}" varStatus="status">
            <tr>
                <c:if test="${purchase.groupProfile.profileType == 'GROUP'}">
                    <td><a href="/moderator/group-info.html?profileId=${purchase.groupProfile.profileId}"><c:out value="${purchase.groupProfile.displayName}"/></a></td>
                </c:if>
                <c:if test="${purchase.groupProfile.profileType == 'USER'}">
                    <td><a href="/moderator/user-info.html?profileId=${purchase.groupProfile.profileId}"><c:out value="${purchase.groupProfile.displayName}"/></a></td>
                </c:if>
                <td><a href="https://${properties["application.host"]}/campaigns/${purchase.campaignId}/orders"><c:out value="${purchase.campaignName}"/></a></td>
                <td><a href="https://${properties["application.host"]}/campaigns/${purchase.campaignId}/orders"><c:out value="${purchase.name}"/></a> <br> ${purchase.price}</td>
                <td>
                    <nobr>
                    ${purchase.count}
                    <c:choose>
                        <c:when test="${purchase.countDiff < 0}">
                            <span class="negative" data-toggle="tooltip" data-placement="top"
                                  title data-tooltip="Изменение значения по сравнению с предыдущим интервалом">
                                (${purchase.countDiff})</span>
                        </c:when>
                        <c:when test="${purchase.countDiff > 0}">
                            <span class="positive" data-toggle="tooltip" data-placement="top"
                                  title data-tooltip="Изменение значения по сравнению с предыдущим интервалом">
                                (+${purchase.countDiff})</span>
                        </c:when>
                        <c:otherwise>
                            (-)
                        </c:otherwise>
                    </c:choose>
                    </nobr>
                </td>
                <td>
                    <nobr>
                    ${purchase.amount}
                    <c:choose>
                        <c:when test="${purchase.amountDiff < 0}">
                            <span class="negative" data-toggle="tooltip" data-placement="top"
                                  title data-tooltip="Изменение значения по сравнению с предыдущим интервалом">
                                (${purchase.amountDiff})</span>
                        </c:when>
                        <c:when test="${purchase.amountDiff > 0}">
                            <span class="positive" data-toggle="tooltip" data-placement="top"
                                  title data-tooltip="Изменение значения по сравнению с предыдущим интервалом">
                                (+${purchase.amountDiff})</span>
                        </c:when>
                        <c:otherwise>
                            (-)
                        </c:otherwise>
                    </c:choose>
                    </nobr>
                </td>

            </tr>
        </c:forEach>
        <tr>
            <th>Итого</th>
            <th></th>
            <th></th>
            <th>${totalCount}</th>
            <th>${totalAmount}</th>
        </tr>
        </tbody>
    </table>
</div>

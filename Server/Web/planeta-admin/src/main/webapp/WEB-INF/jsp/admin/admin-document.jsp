<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ include file="/WEB-INF/jsp/includes/generated/doctype-header.jsp" %>

<head>
    <%@ include file="/WEB-INF/jsp/admin/head.jsp" %>
    <title>${pageTitle}</title>

    <link type="text/css" rel="stylesheet" href="/css/ui-lightness/jquery-ui-1.10.2.custom.min.css"/>

    <style>
        .control-label>span{
            display: none;
        }

        .control-label>span.required{
            display: inline;
            color: red;
        }

        .my-error{
            margin-left: 8px;
        }
    </style>

    <script type="text/javascript">
        function deleteProfileFile(ownerProfileId, fileId) {
            if (confirm('Вы уверены что хотите удалить файл?')) {
                $.post("/moderator/deleteByProfileId-profile-file.json",{
                    profileId: ownerProfileId,
                    fileId: fileId
                }, function (response) {
                    if (response.success) {
                        document.location.href = '/moderator/document-upload.html';
                    } else {
                        alert(response.errorMessage);
                    }
                });
            }
            return false;
        }

        $(document).ready(function () {
//            $("#time-added-box").datetimepicker();
            $("#time-added-box").datetimepicker('setDate', new Date(${profileFile.timeAdded.time}));

            $("#document-form").submit(function() {
                var timeAdded = $("#time-added-box").datetimepicker('getDate');
                if (timeAdded) {
                    $("#timeAdded").val(timeAdded.getTime());
                }

                return true;
            });
        });
    </script>
</head>

<body>

<%@ include file="/WEB-INF/jsp/admin/navbar.jsp" %>

<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                Документ <c:if  test="${not empty profileFile.title}">«${profileFile.title}»</c:if>
            </h1>
        </div>
    </div>



        <form:form
                id="document-form"
                commandName="profileFile"
                class="form-horizontal"
                method="post"
                action="/moderator/document.html">

            <form:input type="hidden" path="fileId"/>

            <div class="row">
                <div class="col-lg-12">
                    <label>Кому принадлежит: </label>
                    <a href="${mainAppUrl}/${profileFile.profileId}" target="_blank">${profileFile.profileId}</a>
                    <form:input type="hidden" path="profileId"/>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12">
                    <label>Кто добавил</label>
                    <a href="${mainAppUrl}/${profileFile.authorId}" target="_blank">${profileFile.authorId}</a>
                    <form:input type="hidden" path="authorId"/>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12">
                    <label>Ссылка</label>
                    <a href="${profileFile.fileUrl}" target="_blank">${profileFile.fileUrl}</a>
                    <form:input type="hidden" path="fileUrl"/>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12">
                    <label>Размер</label>
                        ${profileFile.size}
                        <form:input type="hidden" path="size"/>
                </div>
            </div>

            <div class="row ma-b-20">
                <div class="col-lg-12">
                    <label>Расширение</label>
                        ${profileFile.extension}
                        <form:input type="hidden" path="extension"/>
                </div>
            </div>

            <div class="row ma-b-20">
                <div class="col-lg-6">
                    <label>Заголовок</label>
                    <form:input type="text" path="title" cssClass="form-control"/>
                </div>

                <div class="col-lg-6">
                    <label>Дата создания</label>
                    <form:input type="hidden" path="timeAdded.time"/>
                    <form:input type="hidden" path="timeUpdated.time"/>

                    <input id="time-added-box" class="form-control" readonly type="text" value="<fmt:formatDate pattern="dd-MM-yyyy HH:mm" value="${profileFile.timeAdded}" />"/>
                </div>
            </div>

            <div class="row ma-b-20">
                <div class="col-lg-12">
                    <label>Описание</label>
                    <form:textarea path="description" id="description" rows="3" class="form-control"/>
                    <form:errors path="description" cssClass="error"/>
                </div>
            </div>


            <div class="form-actions">
                <form:input type="hidden" path="name"/>
                <div class="btn-group">
                    <button type="submit" class="btn btn-primary">Сохранить</button>
                    <a href="/moderator/document-upload.html" class="btn btn-default">Отмена</a>
                </div>

                <div class="pull-right">
                    <button formaction="javascript:void(0);" onclick="deleteProfileFile(${profileFile.profileId}, ${profileFile.fileId})" class="btn btn-danger">Удалить</button>
                </div>
            </div>
        </form:form>
</div>
</body>
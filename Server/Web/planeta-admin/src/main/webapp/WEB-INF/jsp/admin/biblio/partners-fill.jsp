<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags/form" %>
<%@ include file="/WEB-INF/jsp/includes/generated/doctype-header.jsp" %>
<head>
    <%@include file="../head.jsp" %>
</head>
<body>

<%@ include file="/WEB-INF/jsp/admin/navbar.jsp" %>

<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Новый партнёр</h1>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <s:form class="form-horizontal" commandName="configuration" method="post">
                <div class="row ma-b-20">
                    <div class="col-lg-6">
                        <label>Название партнёра</label>
                        <s:input path="name" type="text" placeholder="" class="form-control" required=""/>
                        <s:errors path="name" cssClass="error-message" element="span"/>
                    </div>

                    <div class="col-lg-6">
                        <label>Ссылка на партнера</label>
                        <s:input path="originalUrl" type="text" placeholder="" class="form-control" required=""/>
                        <s:errors path="originalUrl" cssClass="error-message" element="span"/>
                    </div>
                </div>

                <div class="row ma-b-20">
                    <div class="col-lg-12">
                        <ct:file url="${configuration.imageUrl}" path="imageUrl" type="IMAGE"/>
                    </div>
                </div>

                <div class="btn-group">
                    <button formaction="/admin/biblio/partners-biblio-fill.html" class="btn btn-primary">Сохранить</button>
                    <a href="/admin/biblio/partners-biblio-list.html" class="btn btn-default">Отмена</a>
                </div>
            </s:form>
        </div>
    </div>
</div>
</body>


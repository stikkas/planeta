<div class="row ma-t-20">
    <div class="col-lg-12 text-center empty-state">
        <i class="fa fa-meh-o"></i> <br> По вашему запросу ничего не найдено. <br> Попробуйте изменить критерии поиска.
    </div>
</div>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<div class="panel panel-primary">
    <div class="panel-heading">Поиск проекта</div>

    <div class="panel-body">
        <form method="get" action="/moderator/campaigns.html" class="clearfix">
            <div class="row ma-b-20">
                <div class="col-lg-5">
                    <label>Запрос</label>
                    <input class="form-control" type="text" placeholder="Введите ID или любое слово для поиска" name="searchString" value="${searchString}">
                    <input type="hidden" name="offset" value="0">
                    <input type="hidden" name="limit" value="10">
                    <input type="hidden" name="contractorId" value="${contractorId}">
                    <input name="reportType" id="reportType" type="hidden" value="">
                    <div class="js-sort-order-list-field">
                        <input type="hidden" name="sortOrderList" value="SORT_BY_TIME_STARTED_DESC">
                        <input type="hidden" name="sortOrderList" value="SORT_BY_TIME_ADDED_DESC">
                    </div>
                </div>

                <div class="col-lg-2">
                    <label>Менеджер</label>
                    <select name="managerId" class="form-control">
                        <option value="">- Не выбрано -</option>
                        <c:forEach var="manager" items="${managersList}">
                            <option value="${manager. managerId}" <c:if test="${manager.managerId == managerId}"> selected</c:if>>
                                <c:out value="${manager.fullName}" />
                            </option>
                        </c:forEach>
                    </select>
                </div>
                <div class="col-lg-3">
                    <label>Категория</label>
                    <select name="campaignTagId" class="form-control">
                        <option value=""> Все </option>
                        <c:forEach var="tag" items="${campaignTagsList}">
                            <option value="${tag.id}" <c:if test="${tag.id == campaignTagId}"> selected</c:if>>
                                <c:out value="${tag.name}"/>
                            </option>
                        </c:forEach>
                    </select>
                </div>
                <div class="col-lg-2">
                    <label>Статус</label>
                    <select name="campaignStatus" class="form-control">
                        <c:forEach var="campaignStatus" items="${campaignStatusList}">
                            <option value="${campaignStatus}"
                                    <c:if test="${campaignStatus.code == campaignStatusCode}"> selected</c:if>>
                                <c:set var="status" value="${campaignStatus}"/>
                                <%@ include file="campaign-statuses.jsp" %>
                            </option>
                        </c:forEach>
                    </select>
                </div>
            </div>

            <div class="row ma-b-20">
                <div class="col-lg-12">
                    <ct:date-range-picker dateFrom="${dateFrom}" dateTo="${dateTo}" allButton="true"/>
                </div>
            </div>

            <div class="btn-group">
                <button class="btn btn-primary" type="submit">
                    <i class="fa fa-search"></i> Поиск
                </button>
                <button class="btn btn-success" type="submit" formaction="/moderator/campaigns-report.html" onclick="$('#reportType').val('EXCEL');">
                    <i class="fa fa-download"></i> Excel
                </button>
                <button class="btn btn-info" type="submit" formaction="/moderator/campaigns-report.html" onclick="$('#reportType').val('CSV');">
                    <i class="fa fa-download"></i> CSV
                </button>
            </div>
        </form>
    </div>
</div>

<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ include file="/WEB-INF/jsp/includes/generated/doctype-header.jsp" %>
<head>
    <%@include file="head.jsp" %>
</head>
<body>

<%@ include file="/WEB-INF/jsp/admin/navbar.jsp" %>

<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                <c:if test="${banner.bannerId == 0}">
                    Новый баннер
                </c:if>
                <c:if test="${banner.bannerId != 0}">
                    Редактирование баннера
                </c:if>
            </h1>
        </div>
    </div>

    <form:form commandName="banner" class="form-horizontal">
        <form:hidden id="bannerId" path="bannerId"/>
        <form:hidden id="bannerOff" path="status"/>

        <div class="row ma-b-20">
            <div class="col-lg-4">
                <label for="edit_title">Название</label>
                <form:input type="text" path="name" id="edit_title" cssClass="form-control"/>
                <form:errors path="name" cssClass="error"/>
                <p class="help-block">Название используется только чтобы отличать баннеры.</p>
            </div>

            <div class="col-lg-4">
                <label for="type">Тип Баннера</label>
                <form:select path="type" id="type" items="${bannerTypes}" cssClass="form-control"/>
                <form:errors path="type" cssClass="error"/>
            </div>

            <div class="col-lg-4">
                <label for="maskView">Путь показа</label>
                <form:input path="maskView" id="maskView" cssClass="form-control"/>
                <p class="help-block">Изменяющуюся часть пути обозначьте символом * (звездочка)</p>
                <form:errors path="maskView" cssClass="error"/>
            </div>
        </div>

        <div class="row ma-b-20">
            <div class="col-lg-12">
                <label for="html">Html</label>
                <form:textarea path="html" id="html" rows="20" class="form-control"/>
                <form:errors path="html" cssClass="error"/>
            </div>
        </div>

        <div class="row ma-b-20">
            <div class="col-lg-12">
                <label for="targetingType">Тип Таргетинга</label>
                <form:select path="targetingType" id="targetingType" items="${targetingTypes}" cssClass="form-control"/>
                <form:errors path="targetingType" cssClass="error"/>
            </div>
        </div>

        <div class="btn-group">
            <button type="submit" formaction="/admin/save-banner-simple.html" class="btn btn-primary">
                <c:if test="${banner.bannerId == 0}">Добавить</c:if>
                <c:if test="${banner.bannerId != 0}">Cохранить</c:if>
            </button>
            <a href="/admin/banners-list.html" class="btn btn-default">Отмена</a>
        </div>

        <div class="pull-right">
            <button formaction="javascript:void(0);" onclick="preview()" class="btn btn-default btn-outline" title="Превью">
                <i class="fa fa-eye"></i>
            </button>
        </div>

    </form:form>


    <script type="text/javascript">
        function preview() {
            $("div#banner-example").removeClass("hidden");
            $("#banner_text").html($("#html").val());
        }
    </script>

    <div class="hidden" id="banner-example">
        <div id="banner_text"></div>
    </div>
</div>
</body>
</html>


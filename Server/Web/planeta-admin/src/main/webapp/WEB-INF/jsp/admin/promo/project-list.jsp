<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ include file="/WEB-INF/jsp/includes/generated/doctype-header.jsp" %>
<head>
    <%@include file="../head.jsp" %>
</head>
<body>

<%@ include file="/WEB-INF/jsp/admin/navbar.jsp" %>

<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Проекты</h1>
        </div>
    </div>

    <div class="main-page-actions">
        <a class="btn btn-success btn-circle btn-outline btn-lg" href="/moderator/promo/project.html?projectId=0" title="Добавить">
            <i class="fa fa-plus"></i>
        </a>
    </div>

    <div class="row">
        <div class="col-lg-12 admin-table">

            <table class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th>Изображение</th>
                        <th>Название</th>
                        <th>Профиль</th>
                        <th>Действие</th>
                    </tr>
                </thead>

                <tbody>
                    <c:forEach var="project" items="${projects}">
                        <tr>
                            <td><img src="${hf:getThumbnailUrl(project.smallImageUrl,"SMALL", "PHOTO")}"></td>
                            <td>${project.title}</td>
                            <td><a href="//${properties["application.host"]}/${project.profileId}">${project.profileId}</a></td>
                            <td class="text-right">
                                <a href="/moderator/promo/project.html?projectId=${project.projectId}"
                                   class="btn btn-primary btn-outline"
                                   title="Редактировать">
                                    <i class="fa fa-pencil"></i>
                                </a>
                            </td>
                        </tr>
                    </c:forEach>
                </tbody>
            </table>

        </div>
    </div>
</body>
</html>
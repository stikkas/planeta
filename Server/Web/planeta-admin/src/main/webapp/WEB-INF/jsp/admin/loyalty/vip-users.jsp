<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ include file="/WEB-INF/jsp/includes/generated/doctype-header.jsp" %>
<head>
    <%@ include file="/WEB-INF/jsp/admin/head.jsp" %>
    <title>Проекты : VIP-спонсоры</title>
    <style type="text/css">
    </style>

    <script>
        function Action(onClick) {
            this.onClick = onClick;
        };

        var actions = {
            kickOut: new Action(function (e) {
                e.preventDefault();
                var $el = $(e.currentTarget);

                var $modal = $('#kick-out-confirmation').modal('show');
                $modal.find('.confirm').off('click').click(function (e) {
                    e.preventDefault();
                    changeUserVipStatus($el, false)
                        .done(function () {
                            $modal.modal('hide');
                        });
                });
            }),
            invite: new Action(function (e) {
                e.preventDefault();
                var $el = $(e.currentTarget);
                changeUserVipStatus($el, true);
            })
        };

        function changeUserVipStatus($el, isVip) {
            $el.off('click');
            var data = {
                userId: $el.data('user-id'),
                isVip: isVip
            };

            return $.post('/admin/loyalty/change-vip-status.json', data)
                .done(function (response) {
                    if (response.success) {
                        location.reload();
                    }
                })
                .promise();
        };

        $(document).ready(function () {
            $('.action:not(.kickOut)').addClass('invite');

            _.map(actions, function (action, name) {
                $('.' + name).text(action.description).click(action.onClick);
            });
        });
    </script>
</head>

<body>

<div class="modal-dialog" id="kick-out-confirmation" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true" style="display: none">
    <div class="modal">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h3 id="myModalLabel">Исключение из клуба</h3>
        </div>
        <div class="modal-body">
            <p>Вы действительно хотите исключить пользователя из клуба?</p>
        </div>
        <div class="modal-footer">
            <button class="btn" data-dismiss="modal" aria-hidden="true">Отмена</button>
            <button class="btn btn-primary confirm">Исключить</button>
        </div>
    </div>
</div>


<%@ include file="/WEB-INF/jsp/admin/navbar.jsp" %>

<div id="page-wrapper">

    <div class="row">
        <div class="col-lg-12">
            <h1 class="">VIP-спонсоры</h1>
        </div>
    </div>

    <div class="row ">
        <div class="col-lg-12">
            <jsp:include page="/WEB-INF/jsp/admin/searchBoxUsers.jsp">
                <jsp:param name="action" value="/moderator/loyalty-vip-users.html"/>
            </jsp:include>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12 admin-table">
            <c:if test="${empty users}">
                <h2 class="text-center">
                    <small>По Вашему запросу ничего не найдено ...</small>
                </h2>
            </c:if>
            <c:if test="${not empty users}">
                <table class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th width="50">#</th>
                            <th width="50">Фото</th>
                            <th>Имя, ник, фамилия</th>
                            <th>Действия</th>
                        </tr>
                    </thead>

                    <tbody>
                        <c:forEach var="user" items="${users}">
                            <tr>
                                <td>${user.profileId}</td>
                                <td>
                                    <img src="${hf:getThumbnailUrl(user.imageUrl, "USER_SMALL_AVATAR", "USER")}"/>
                                </td>
                                <td>
                                    <a title="Страница пользователя" href="${mainAppUrl}/${user.profileId}" target="_blank">
                                            ${user.displayName}
                                    </a>
                                    <br>
                                        ${user.email}
                                </td>
                                <td class="text-right">
                                    <c:if test="${user.vip}">
                                        <button class="btn btn-danger btn-outline action kickOut"
                                                data-user-id="${user.profileId}" title="Исключить из клуба">
                                            <i class="fa fa-trash"></i>
                                        </button>
                                    </c:if>
                                    <c:if test="${!user.vip}">
                                        <button class="btn btn-success btn-outline action invite"
                                                data-user-id="${user.profileId}" title="Добавить в клуб">
                                            <i class="fa fa-plus"></i>
                                        </button>
                                    </c:if>
                                </td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>

                <%@ include file="/WEB-INF/jsp/admin/paginator.jsp" %>
            </c:if>
        </div>
    </div>
</div>

</body>
</html>


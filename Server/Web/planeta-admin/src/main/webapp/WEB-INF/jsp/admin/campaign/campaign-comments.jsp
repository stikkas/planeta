<%@ include file="/WEB-INF/jsp/includes/generated/doctype-header.jsp" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@taglib prefix="p" uri="http://planeta.ru/tags" %>
<head>
    <%@ include file="/WEB-INF/jsp/admin/head.jsp" %>
    <title>${pageTitle}</title>
    <style type="text/css">
    </style>

</head>

<body>

<%@ include file="/WEB-INF/jsp/admin/navbar.jsp" %>

<div id="page-wrapper">

    <c:set var="withTypes" value="true" scope="request"/>
    <c:set var="withManagers" value="true" scope="request"/>

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">${pageTitle}</h1>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <%@ include file="campaign-events-header.jsp" %>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12 admin-table">
            <c:choose>
                <c:when test="${empty campaignNewsList}">
                    <%@ include file="/WEB-INF/jsp/admin/empty-state.jsp" %>
                </c:when>
                <c:otherwise>
                    <table class="table table-bordered table-striped">

                        <thead>
                        <tr>
                            <th>Ид проекта</th>
                            <th>Название проекта</th>
                            <th>Тип комментария</th>
                            <th>Ид комментария</th>
                            <th>Комментарий</th>
                            <th>Дата</th>
                            <th>Менеджер</th>
                        </tr>
                        </thead>

                        <c:forEach items="${campaignNewsList}" var="campaignNews" varStatus="loopStatus">
                            <tr>
                                <td><a href="${mainAppUrl}/campaigns/${campaignNews.campaign.campaignId}"
                                       target="_blank">${campaignNews.campaign.campaignId}</a></td>
                                <td><c:out value="${campaignNews.campaign.name}"/></td>
                                <td>
                                    <c:choose>
                                        <c:when test="${campaignNews.comment.objectType == 'POST'}">
                                            К новости
                                        </c:when>
                                        <c:otherwise>
                                            К проекту
                                        </c:otherwise>
                                    </c:choose>
                                </td>
                                <td>
                                    <c:choose>
                                    <c:when test="${campaignNews.comment.objectType == 'POST'}">
                                    <a href="${mainAppUrl}/campaigns/${campaignNews.campaign.campaignId}/updates!post${campaignNews.comment.objectId}"
                                       target="_blank">
                                        </c:when>
                                        <c:otherwise>
                                        <a href="${mainAppUrl}/campaigns/${campaignNews.campaign.campaignId}/comments!comment${campaignNews.comment.commentId}"
                                           target="_blank">
                                            </c:otherwise>
                                            </c:choose>
                                                ${campaignNews.comment.commentId}
                                        </a>
                                </td>
                                <td><c:out value="${campaignNews.comment.textHtml}"/></td>
                                <td><fmt:formatDate value="${campaignNews.comment.timeAdded}"
                                                    pattern="dd.MM.yyyy HH:mm"/></td>
                                <td><c:out value="${campaignManagerList[loopStatus.index].fullName}"/></td>
                            </tr>
                        </c:forEach>
                    </table>
                </c:otherwise>
            </c:choose>
        </div>
    </div>

    <%@ include file="../paginator.jsp" %>
</div>
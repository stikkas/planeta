<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ include file="/WEB-INF/jsp/includes/generated/doctype-header.jsp" %>
<head>
    <%@ include file="../head.jsp" %>
</head>

<body>
    <%@ include file="/WEB-INF/jsp/admin/navbar.jsp" %>
    <div id="container" class="container">
        <ct:breadcrumb title1="Концерты" title2="Список концертов"/>

        <a class="btn" href="/admin/import-concerts.json">Импортировать</a>

        <hr />

        <form method="get">
            <a href="/admin/concert.html?concertId=0" class="btn"><i class="icon-plus"></i> Добавить</a>
        </form>

        <div class="row">
            <diчv class="span12">
                <table class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Ex ID</th>
                            <th>Заголовок</th>
                            <th>Изображение</th>
                            <th>Дата</th>
                            <th>Статус</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <c:forEach var="concert" items="${concerts}">
                            <tr>
                                <td>${concert.concertId}</td>
                                <td>${concert.externalConcertId}</td>
                                <td>${concert.title}</td>
                                <td><img style="max-width: 250px; max-height: 250px;" src="${concert.imageUrl}"></td>
                                <td><fmt:formatDate value="${concert.concertDate}" pattern="dd MMMM yyyy в hh:mm"/></td>
                                <td>
                                    <c:choose>
                                        <c:when test="${concert.status eq 'ACTIVE'}">
                                            <span class="badge badge-success">Активно</span>
                                        </c:when>
                                        <c:when test="${concert.status eq 'PAUSED'}">
                                            <span class="badge badge-info">Приостановлено</span>
                                        </c:when>
                                    </c:choose>
                                </td>

                                <td>
                                    <a href="/admin/concert.html?concertId=${concert.concertId}" class="btn"><i class="icon-pencil"></i></a>
                                </td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>

                <%@ include file="../paginator.jsp" %>
            </div>
        </div>
    </div>
</body>
</html>
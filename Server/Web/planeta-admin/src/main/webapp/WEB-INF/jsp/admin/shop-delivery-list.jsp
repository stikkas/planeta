<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ include file="/WEB-INF/jsp/includes/generated/doctype-header.jsp" %>
<head>
    <%@ include file="head.jsp" %>
    <link type="text/css" rel="stylesheet"
          href="//${hf:getStaticBaseUrl("")}/css/ui-lightness/jquery-ui-1.10.0-bootstrap.custom.css"/>
    <title>Управление службами достваки</title>

    <script type="text/javascript">
        $(function () {
            var attrs = ${hf:toJson(departments)};
            var view = new ShopAdmin.Views.Delivery({
                el: '#page-wrapper',
                model: new ShopAdmin.Models.DeliveryListModel(attrs)
            });
            view.render();
        });
    </script>
</head>

<body>

<%@ include file="navbar.jsp" %>

<div id="page-wrapper"></div>

</body>
</html>


<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<%-- Dividing css in 2 files (because of IE bug) --%>
<link type="text/css" rel="stylesheet" href="//${hf:getStaticBaseUrl("")}/admin-new/bootstrap/css/bootstrap.css"/>
<link type="text/css" rel="stylesheet" href="//${hf:getStaticBaseUrl("")}/admin-new/bootstrap/css/bootstrap-responsive.css"/>
<link type="text/css" rel="stylesheet" href="//${hf:getStaticBaseUrl("")}/admin-new/css/modal.css"/>
<link type="text/css" rel="stylesheet" href="//${hf:getStaticBaseUrl("")}/admin-new/css/upload.css"/>
<link type="text/css" rel="stylesheet" href="//${hf:getStaticBaseUrl("")}/admin-new/css/error.css"/>

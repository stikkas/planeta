<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ include file="/WEB-INF/jsp/includes/generated/doctype-header.jsp" %>
<head>
    <%@ include file="../head.jsp" %>

    <link type="text/css" rel="stylesheet" href="/css/ui-lightness/jquery-ui-1.10.2.custom.min.css"/>
    <link type="text/css" rel="stylesheet" href="/css/jquery.timepicker.css"/>

    <script>
        $(function () {
            $("#date-box").datetimepicker();

            $("#concertForm").submit(function() {
                var changePriceDate = $("#date-box").datetimepicker('getDate');
                if (changePriceDate) {
                    $("#concertDate").val(changePriceDate.getTime());
                }

                return true;
            });

            $(document).on('click', ".js-upload-image", function (e) {
                var imgSelector = $(e.currentTarget).find('img'),
                    inputSelector = $(e.currentTarget).next();

                uploadImage(imgSelector, inputSelector);
            });

            setImageParams($('#imageImg'));
            setImageParams($('#schemeImg'));

            function setImageParams(selector) {
                if(selector.attr('data-url')) {
                    selector.attr('src', selector.attr('data-url'));
                } else {
                    selector.attr('src', workspace.staticNodesService.getResourceUrl("/images/defaults/upload.png"));
                }
            }

            function uploadImage(imgSelector, inputSelector) {
                UploadController.showUploadBroadcastAvatar(-1, function (filesUploaded) {
                    if (!filesUploaded || !filesUploaded.length) {
                        return;
                    }
                    var uploadResult = filesUploaded.at(0).get('uploadResult');
                    if (!uploadResult) {
                        return;
                    }
                    inputSelector.val(uploadResult.imageUrl);
                    imgSelector.attr('src', uploadResult.imageUrl);
                });
            }
        });
    </script>
</head>

<body>
    <%@ include file="/WEB-INF/jsp/admin/navbar.jsp" %>

    <div id="container" class="container">
        <c:if test="${concert.concertId != 0}">
            <ct:breadcrumb title1="Концерты" title2="Редактирование концерта «${concert.title}»"/>
        </c:if>
        <c:if test="${concert.concertId == 0}">
            <ct:breadcrumb title1="Концерты" title2="Новый концерт"/>
        </c:if>

        <div class="row">
            <div id="center-container" class="span12">
                <form:form id="concertForm" class="form-horizontal" commandName="concert" method="post">
                    <input id="concertId" name="concertId" type="hidden" value="0">
                    <div class="well settings-container clearfix">
                        <div class="control-group">
                            <label class="control-label" for="externalConcertId">External ID <span class="required">*</span></label>
                            <div class="controls">
                                <input id="externalConcertId" type="number" name="externalConcertId" value="${concert.externalConcertId}">
                                <form:errors path="externalConcertId" cssClass="error-message my-error" element="span" />
                            </div>
                        </div>

                        <div class="control-group">
                            <label class="control-label" for="title">Название <span class="required">*</span></label>
                            <div class="controls">
                                <input id="title" type="text" name="title" value="${concert.title}">
                                <form:errors path="title" cssClass="error-message my-error" element="span" />
                            </div>
                        </div>

                        <div class="control-group">
                            <label class="control-label" for="description">Описание <span class="required">*</span></label>
                            <div class="controls">
                                <textarea class="span11" id="description" name="description" rows="6">${concert.description}</textarea>
                                <form:errors path="description" cssClass="error-message my-error" element="span" />
                            </div>
                        </div>

                        <div class="control-group">
                            <label class="control-label" for="date-box">Дата концерта</label>
                            <div class="controls">
                                <input id="concertDate" name="concertDate" type="hidden"/>
                                <input id="date-box" type="text" value="<fmt:formatDate pattern="dd-MM-yyyy hh:mm" value="${concert.concertDate}" />"/>
                            </div>
                        </div>

                        <div class="control-group">
                            <label class="control-label" for="imageUrl">Обложка <span class="required">*</span></label>
                            <div class="controls">
                                <a href="javascript:void(0)" class="js-upload-image">
                                    <img id="imageImg" src="" width="182" data-url="${concert.imageUrl}">
                                </a>
                                <input id="imageUrl" path="imageUrl" name="imageUrl" type="hidden" value="${concert.imageUrl}"/>
                                <form:errors path="imageUrl" cssClass="error-message my-error" element="span" />
                            </div>
                        </div>

                        <div class="control-group">
                            <label class="control-label" for="imageUrl">Схема зала <span class="required">*</span></label>
                            <div class="controls">
                                <a href="javascript:void(0)" class="js-upload-image">
                                    <img id="schemeImg" src="" data-url="${concert.schemeUrl}">
                                </a>
                                <input id="schemeUrl" path="schemeUrl" name="schemeUrl" type="hidden" value="${concert.schemeUrl}"/>
                                <form:errors path="schemeUrl" cssClass="error-message my-error" element="span" />
                            </div>
                        </div>

                        <div class="control-group">
                            <label class="control-label" for="hallId">ID зала <span class="required">*</span></label>
                            <div class="controls">
                                <input id="hallId" type="number" name="hallId" value="${concert.hallId}">
                                <form:errors path="hallId" cssClass="error-message my-error" element="span" />
                            </div>
                        </div>

                        <div class="control-group">
                            <label class="control-label" for="ageRestriction">Возрастное ограничение <span class="required">*</span></label>
                            <div class="controls">
                                <input id="ageRestriction" type="number" name="ageRestriction" value="${concert.ageRestriction}">
                                <form:errors path="ageRestriction" cssClass="error-message my-error" element="span" />
                            </div>
                        </div>

                        <div class="control-group">
                            <label for="status" class="control-label">Статус издания</label>
                            <div class="controls">
                                <select id="status" name="status">
                                    <c:forEach items="${concertStatuses}" var="status">
                                        <c:if test="${concert.status == status}">
                                            <option selected="selected">${status}</option>
                                        </c:if>
                                        <c:if test="${concert.status != status}">
                                            <option>${status}</option>
                                        </c:if>
                                    </c:forEach>
                                </select>
                            </div>
                        </div>

                        <div class="form-actions">
                            <c:if test="${concert.concertId == 0}">
                                <button type="submit" formaction="/admin/save-concert.json" class="btn btn-primary">
                                    Добавить
                                </button>
                            </c:if>
                            <c:if test="${concert.concertId != 0}">
                                <button type="submit" formaction="/admin/save-concert.json?concertId=${concert.concertId}" class="btn btn-primary">
                                    Сохранить
                                </button>
                            </c:if>
                            <a href="/admin/concert-list.html" class="btn">Назад</a>
                        </div>
                    </div>
                </form:form>
            </div>
        </div>
    </div>
</body>
</html>
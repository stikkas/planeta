<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<c:if test="${not empty logRecords}">
    <ul class="log-records">
        <c:forEach items="${logRecords}" var="logRecord" varStatus="status">
            <li>
                <div class="log-data message">
                    <pre><span class="title">Сообщение: </span>${fn:escapeXml(logRecord.message)}</pre>
                    <div class="timestamp">
                        <fmt:formatDate value="${hf:getDateByTime(logRecord.timeAdded)}" pattern="dd.MM.yyyy HH:mm:ss,SSS"/>
                    </div>
                </div>
                <c:if test="${not empty logRecord.request}">
                    <div class="log-data">
                        <div class="resizer">
                            <a href="javascript:void(0);" class="collapseAll" data-toggle="tooltip" title="Свернуть всё">
                                <i class="fa fa-minus"></i>
                            </a>
                            <a href="javascript:void(0);" class="expandAll" data-toggle="tooltip" title="Развернуть всё">
                                <i class="fa fa-plus"></i>
                            </a>
                        </div>
                        <pre><span class="title">Запрос: </span><span class="json-data">${fn:escapeXml(logRecord.request.metaData)}</span></pre>
                        <div class="timestamp">
                            <fmt:formatDate value="${hf:getDateByTime(logRecord.request.timeAdded)}" pattern="dd.MM.yyyy HH:mm:ss,SSS"/>
                        </div>
                    </div>
                </c:if>
                <c:if test="${not empty logRecord.response}">
                    <div class="log-data">
                        <div class="resizer">
                            <a href="javascript:void(0);" class="collapseAll" data-toggle="tooltip" title="Свернуть всё">
                                <i class="fa fa-minus"></i>
                            </a>
                            <a href="javascript:void(0);" class="expandAll" data-toggle="tooltip" title="Развернуть всё">
                                <i class="fa fa-plus"></i>
                            </a>
                        </div>
                        <pre><span class="title">Ответ: </span><span class="json-data">${fn:escapeXml(logRecord.response.metaData)}</span></pre>
                        <div class="timestamp">
                            <fmt:formatDate value="${hf:getDateByTime(logRecord.response.timeAdded)}" pattern="dd.MM.yyyy HH:mm:ss,SSS"/>
                        </div>
                    </div>
                </c:if>
            </li>
        </c:forEach>
    </ul>
</c:if>


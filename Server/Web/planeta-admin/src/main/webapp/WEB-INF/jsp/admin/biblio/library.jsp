<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="x" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags/form" %>
<%@ include file="/WEB-INF/jsp/includes/generated/doctype-header.jsp" %>
<head>
    <%@ include file="../head.jsp" %>
    <link type="text/css" rel="stylesheet" href="/css/ui-lightness/jquery-ui-1.10.2.custom.min.css"/>
</head>
<body>

<%@ include file="/WEB-INF/jsp/admin/navbar.jsp" %>

<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                <c:if test="${library.libraryId > 0}">
                    Редактирование библиотеки
                </c:if>
                <c:if test="${library.libraryId <= 0}">
                    Новая библиотека
                </c:if>
            </h1>
        </div>
    </div>

    <form:form
            id="library-form"
            commandName="library"
            class="form-horizontal span12"
            method="post"
            action="/admin/library/library.html">
        <input type="hidden" name="libraryId" value="${library.libraryId}"/>

        <div class="row ma-b-20">
            <div class="col-lg-3">
                <label>Название</label>
                <input type="text" name="name" class="form-control" value="<c:out value="${library.name}"/>"/>
            </div>

            <div class="col-lg-3">
                <label>Статус</label>
                <form:select path="status" id="status" cssClass="form-control" items="${biblioObjectStatuses}"/>
            </div>

            <div class="col-lg-3">
                <label>Регион</label>
                <input type="text" name="region" class="form-control" value="${library.region}"/>
            </div>

            <div class="col-lg-3">
                <label>Индекс</label>
                <input type="text" name="postIndex" class="form-control" value="${library.postIndex}"/>
            </div>
        </div>

        <div class="row ma-b-20">
            <div class="col-lg-3">
                <label>Адрес</label>
                <input type="text" name="address" class="form-control" value="<c:out value="${library.address}"/>"/>
            </div>

            <div class="col-lg-3">
                <label>Контактное лицо</label>
                <input type="text" name="contractor" class="form-control" value="${library.contractor}"/>
            </div>

            <div class="col-lg-3">
                <label>Email</label>
                <input type="text" name="email" class="form-control" value="${library.email}"/>
            </div>

            <div class="col-lg-3">
                <label>Сайт</label>
                <input type="text" name="site" class="form-control" value="${library.site}"/>
            </div>
        </div>

        <div class="row ma-b-20">
            <div class="col-lg-3">
                <label>Комментарий</label>
                <input type="text" name="comment" class="form-control" value="${library.comment}"/>
            </div>

            <div class="col-lg-3">
                <label>Долгота</label>
                <input type="text" name="longitude" class="form-control" value="${library.longitude}"/>
            </div>

            <div class="col-lg-3">
                <label>Широта</label>
                <input type="text" name="latitude" class="form-control" value="${library.latitude}"/>
            </div>

            <div class="col-lg-3">
                <label>Тип</label>

                <select id="libraryType" name="libraryType" class="form-control">
                    <option value="PUBLIC" <c:if test="${library.libraryType eq 'PUBLIC'}">selected=''</c:if>>
                        Публичные библиотеки
                    </option>
                    <option value="SCHOLASTIC"
                            <c:if test="${library.libraryType eq 'SCHOLASTIC'}">selected=''</c:if>>Школьные
                        библиотеки
                    </option>
                    <option value="SCIENTIFIC"
                            <c:if test="${library.libraryType eq 'SCIENTIFIC'}">selected=''</c:if>>Научные
                        библиотеки
                    </option>
                    <option value="DOMESTIC"
                            <c:if test="${library.libraryType eq 'DOMESTIC'}">selected=''</c:if>>Детские и
                        семейные библиотеки
                    </option>
                    <option value="SPECIAL" <c:if test="${library.libraryType eq 'SPECIAL'}">selected=''</c:if>>
                        Специализированные библиотеки
                    </option>
                </select>
            </div>
        </div>

        <div class="btn-group">
            <c:if test="${library.libraryId != -1}">
                <button type="submit" class="btn btn-primary">Сохранить</button>
            </c:if>
            <c:if test="${library.libraryId == -1}">
                <button type="submit" class="btn btn-primary">Добавить</button>
            </c:if>
            <a href="/admin/library/libraries.html" class="btn btn-default">Отмена</a>
        </div>
    </form:form>
</div>
</body>
</html>


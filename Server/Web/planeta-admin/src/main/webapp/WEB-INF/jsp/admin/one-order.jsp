<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ include file="/WEB-INF/jsp/includes/generated/doctype-header.jsp" %>
<c:set var="baseUrl" value="${hf:getStaticBaseUrl('')}"/>
<head>
    <title>Админка заказа</title>
    <%@ include file="head.jsp" %>

    <script>

        function changeDeliveryInfo(form) {
            var f = $(form);
            var deliveryStatus = f.find('selectCampaignById[name="deliveryStatus"] option:selected').text().trim().toUpperCase();

            var data = f.serializeObject();
            for (var i in data) {
                if (data[i] === 'on') {
                    data[i] = true;
                }
            }

            var btn = f.find('.btn');
            btn.addClass('disabled');
            $.ajax({
                url: '/moderator/order-change-delivery-status.json',
                type: 'post',
                dataType : 'json',
                contentType : 'application/json',
                data: JSON.stringify(data),
                success: function (responce) {
                    var orderMessageContainer = $('#messageContentForOrder' + data.orderId);
                    btn.removeClass('disabled');
                    if (responce.success) {
                        resetDeliveryStatusSelector(data.orderId);
                        var deliveryStatus = responce.result.deliveryStatus;
                        var messageHtml = "<strong>Заказ №" + data.orderId + "</strong> статус доставки обновлен успешно. </br> Новый статус <strong>\"" + deliveryStatusNaming[deliveryStatus] + "\"</strong>";
                        showSuccessMessage(orderMessageContainer, messageHtml);
                        setPaymentStatus(f);
                    } else {
                        showErrorMessage(orderMessageContainer, responce.errorMessage);
                    }
                },
                error: function (responce) {
                    errorChangeDeliveryStatus(orderId)
                }
            });

        }


        function revertDeliveryStatus(orderId) {
            var deliverySelect = $('#deliveryStatus' + orderId);
            deliverySelect.val(deliverySelect.data('prevValue'));
        }

        /**
        *
        * @param messageType required - "success" or "error"
        * @param messageHtml
        * @private
         */
        function _showMessage(container, messageType, messageHtml) {
            container.append("<div class=\"alert alert-" + messageType + "\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" +
                    messageHtml + "</div>");
        }
        function showErrorMessage(container, messageHtml) {
            if (typeof container == 'string' || typeof container == 'undefined') {
                messageHtml = container;
                container = $('#warning-container');
            }
            _showMessage(container,"error", messageHtml);
        }
        function showSuccessMessage(container, messageHtml){
            if (typeof container == 'string' || typeof container == 'undefined') {
                messageHtml = container;
                container = $('#warning-container');
            }
            _showMessage(container,"success", messageHtml);
        }

        var validDeliveryTransitions = ${hf:toJson(validTransitions)};
        //yes - it is ugly hack
        var deliveryStatusNaming = {
        <c:forEach items="${validTransitions}" var="entry" varStatus="entryStatus">
               '${entry.key}':'<spring:message code="delivery.status.naming" arguments="${entry.key.code}"/>' <c:if test="${!entryStatus.last}">,</c:if>
        </c:forEach>
        };

        function setPaymentStatus(form) {
            var f = $(form);
            f.find("option[value = 'PENDING']").prop("selected", false);
            f.find("option[value = 'COMPLETED']").prop("selected", true);
        }

        function resetDeliveryStatusSelector(orderId) {
            var deliverySelect = $('#deliveryStatus' + orderId);
            var selectedStatus = deliverySelect.val();
            deliverySelect.children('option').each(function(){
                var $option = $(this);
                if ($option.val() != selectedStatus) {
                    $option.remove();
                }
            });
            _(validDeliveryTransitions[selectedStatus]).each(function(status, index, list){
                $('<option>').appendTo(deliverySelect).html(deliveryStatusNaming[status]).val(status);
            });
            if (deliverySelect.children('option').length == 1) {
                deliverySelect.attr('disabled', 'disabled');
            }
        }
        $.expr[':']['data'] = function(_0, _1, match ) {
            var argumentVal = match[3];
            if (!argumentVal){
                return true;
            }
            var conditions = $.expr[':'].data[argumentVal];
            if (!conditions || conditions.length == 0) {
                var pairs = argumentVal.split(",");
                conditions = [];
                $.each(pairs, function(index, item) {
                    var _data = /([^<>=]*)([<>=]*)([^<>=]*)/ig.exec(item);
                    conditions.push({
                        a: _data[1],
                        comparator: _data[2],
                        b: _data[3]
                    });
                });
                $.expr[':'].data[argumentVal] = conditions;
            }
            var result = true;
            var $self = $(_0);
            $.each(conditions, function(index, c){
                result = result && eval(''+$self.data(c.a)+ c.comparator+ c.b);
            });
            return result;
        };

        function errorChangeDeliveryStatus(orderId, message) {
            var orderMessageContainer = $('#messageContentForOrder' + orderId);
            showErrorMessage(orderMessageContainer, "Произошла ошибка при обновлении статуса доставки. " + message);
            revertDeliveryStatus(orderId);
        }

    </script>
</head>

<body>
    <%@ include file="navbar.jsp" %>

    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Заказ № ${orderInfo.orderId}</h1>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12" id="warning-container"></div>
        </div>

        <div class="row">
            <div class="col-lg-12 admin-table">
                <c:choose>
                    <c:when test="${empty orderInfo}">
                        <%@ include file="/WEB-INF/jsp/admin/empty-state.jsp" %>
                    </c:when>
                    <c:otherwise>
                        <table class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>№ Заказа<br> Дата</th>
                                    <th>Товары</th>
                                    <th>Пользователь</th>
                                    <th>Адрес доставки</th>
                                    <th>Сумма</th>
                                    <th>Текущий&nbsp;статус</th>
                                </tr>
                            </thead>

                            <tbody>
                                <tr>
                                    <td id="messageContentForOrder${orderInfo.orderId}" colspan="6" style="padding:0;border:0;"></td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="purchases-number lead">№${orderInfo.orderId}</div>
                                        <div class="muted"><nobr>${hf:dateFormat(orderInfo.timeAdded)}</nobr></div>
                                    </td>
                                    <td>
                                        <c:forEach var="orderObjectsGroup" items="${hf:groupOrderObjectsByObjectId(orderInfo.orderObjectsInfo)}">
                                            <c:set var="orderObjectsInfo" value="${orderObjectsGroup.value}" />
                                            <c:set var="orderObjectInfo" value="${orderObjectsInfo[0]}" />
                                            <c:set var="orderObjectInfoCount" value="${fn:length(orderObjectsInfo)}" />

                                            <div class="media">
                                                <c:if test="${orderInfo.orderType != 'PRODUCT'}">
                                                    <img class="pull-left" src='${hf:getThumbnailUrl(orderObjectInfo.objectImageUrl, "MEDIUM", "PRODUCT")}' alt="${orderObjectInfo.objectName}">
                                                </c:if>
                                                <c:if test="${orderInfo.orderType == 'PRODUCT'}">
                                                    <a class="pull-left" href="https://${properties['shop.application.host']}/products/${orderObjectInfo.objectId}">
                                                        <img src='${hf:getThumbnailUrl(orderObjectInfo.objectImageUrl, "MEDIUM", "PRODUCT")}' alt="${orderObjectInfo.objectName}">
                                                    </a>
                                                </c:if>
                                                <div class="media-body">
                                                    <div>
                                                        <c:if test="${orderInfo.orderType != 'PRODUCT'}">${orderObjectInfo.objectName} <br>[${orderInfo.orderType}]</c:if>
                                                        <c:if test="${orderInfo.orderType == 'PRODUCT'}"><a href="https://${properties['shop.application.host']}/products/${orderObjectInfo.objectId}">${orderObjectInfo.objectName}</a><br>[${orderInfo.orderType}]</c:if>
                                                    </div>
                                                    <div class="lead">
                                                        ${orderObjectInfoCount} шт., ${orderObjectInfo.price} руб.
                                                    </div>
                                                </div>
                                                <c:if test="${not empty orderInfo.questionToBuyer}">
                                                    <div class="muted">Вопрос автора</div>
                                                    <c:out value="${orderInfo.questionToBuyer}"/>
                                                </c:if>
                                                <c:if test="${not empty orderObjectInfo.comment}">
                                                    <div class="muted">Ответ</div>
                                                    <c:out value="${orderObjectInfo.comment}"/>
                                                </c:if>
                                            </div>
                                        </c:forEach>
                                    </td>
                                    <td>
                                        <div class="order-user-name"><a href="${mainAppUrl}/${orderInfo.buyerId}">${orderInfo.buyerName}</a>
                                        </div>
                                        <div class="order-user-mail">${orderInfo.buyerEmail}</div>
                                    </td>
                                    <td>
                                        <div class="order-address">
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <c:if test="${empty orderInfo.linkedDelivery && (not empty orderInfo.deliveryAddress.phone)}">
                                                        <div><b>ФИО:</b></div>
                                                        <div><p>${orderInfo.deliveryAddress.fio}</p></div>
                                                        <div><b>Телефоны:</b></div>
                                                        <div><p>${orderInfo.deliveryAddress.phone} </p></div>
                                                    </c:if>
                                                    <c:if test="${orderInfo.deliveryType == \"NOT_SET\"}">&nbsp;</c:if>
                                                    <c:if test="${not empty orderInfo.linkedDelivery}">

                                                    <div id="add${varStatus.index}" class="mrg-t-10">
                                                        <c:if test="${(not empty orderInfo.deliveryAddress)}">
                                                        <div><b>Адресат</b></div>
                                                        <div><p>${orderInfo.deliveryAddress.fio}&nbsp;</p></div>
                                                        </c:if>
                                                        <div><b>Вид</b></div>
                                                        <div><p>${orderInfo.linkedDelivery.name} </p></div>
                                                        <c:choose>
                                                            <c:when test="${not empty orderInfo.deliveryAddress}">
                                                                <div><b>Адрес</b></div>
                                                                <div><p><c:if test="${not empty orderInfo.deliveryAddress.zipCode}">${orderInfo.deliveryAddress.zipCode}, </c:if>${orderInfo.deliveryAddress.city}, ${orderInfo.deliveryAddress.address}</p></div>
                                                                <c:if test="${not empty orderInfo.deliveryAddress.phone}">
                                                                    <div><b>Телефоны:</b></div>
                                                                    <div><p>${orderInfo.deliveryAddress.phone} </p></div>
                                                                </c:if>
                                                            </c:when>
                                                            <c:otherwise>
                                                                <div><p>Адрес не указан</p></div>
                                                            </c:otherwise>
                                                        </c:choose>
                                                    </div>
                                                    </c:if>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="lead"><nobr><b> ${orderInfo.totalPrice} руб.</b></nobr></div>
                                    </td>

                                    <td>
                                        <form onsubmit="changeDeliveryInfo(this); return false;">
                                            <input name="orderId" type="hidden" value="${orderInfo.orderId}">

                                            <div class="row ma-b-10">
                                                <div class="col-lg-12">
                                                    <label>Статус оплаты</label>
                                                    <select class="order-action-block order-status form-control" id="paymentStatusNew${orderInfo.orderId}" name="paymentStatusNew" disabled="true" data-order-id="${orderInfo.orderId}">
                                                        <option value="PENDING" <c:if test="${orderInfo.paymentStatus == 'PENDING'}">selected="selected"</c:if>>Не оплачен</option>
                                                        <option value="COMPLETED" <c:if test="${orderInfo.paymentStatus == 'COMPLETED'}">selected="selected"</c:if>>Оплачен</option>
                                                        <option value="CANCELLED" <c:if test="${orderInfo.paymentStatus == 'CANCELLED'}">selected="selected"</c:if>>Аннулирован</option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="row ma-b-10">
                                                <div class="col-lg-12">
                                                    <label>Статус доставки</label>
                                                    <select class="order-action-block order-status form-control"
                                                            id="deliveryStatus${orderInfo.orderId}"
                                                            name="deliveryStatus"
                                                            data-order-id="${orderInfo.orderId}"
                                                            data-prev-value="${orderInfo.deliveryStatus}"
                                                            <c:if test="${fn:length(validTransitions[orderInfo.deliveryStatus]) == 0}">
                                                                disabled="disabled"
                                                            </c:if>>
                                                        <option value="${orderInfo.deliveryStatus}" selected="selected">
                                                            <spring:message code="delivery.status.naming"
                                                                            arguments="${orderInfo.deliveryStatus.code}"/>
                                                        </option>
                                                        <c:forEach items="${validTransitions[orderInfo.deliveryStatus]}" var="validNewStatus">
                                                            <option value="${validNewStatus}">
                                                                <spring:message code="delivery.status.naming"
                                                                                arguments="${validNewStatus.code}"/>
                                                            </option>
                                                        </c:forEach>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="row ma-b-10">
                                                <div class="col-lg-12">
                                                    <label>Идентификатор</label>
                                                    <input name="trackingCode" type="text" class="form-control" value="${orderInfo.trackingCode}">
                                                </div>
                                            </div>

                                            <div class="row ma-b-10">
                                                <div class="col-lg-12">
                                                    <label>Наложенный платёж</label>
                                                    <input name="cashOnDeliveryCost" type="number" class="form-control" value="${orderInfo.cashOnDeliveryCost}">
                                                </div>
                                            </div>

                                            <div class="row ma-b-10">
                                                <div class="col-lg-12">
                                                    <label>Комментарий</label>
                                                    <textarea name="deliveryComment" class="form-control">${orderInfo.deliveryComment}</textarea>
                                                </div>
                                            </div>

                                            <div class="row ma-b-10" style="white-space: nowrap">
                                                <div class="col-lg-12">
                                                    <label>
                                                        <input type="checkbox" id="sendMessageToUser${orderInfo.orderId}" checked name="sendMessageToUser">
                                                        Отправлять уведомление
                                                    </label>
                                                </div>
                                            </div>

                                            <button type="submit" class="btn btn-primary btn-block" title="Сохранить">
                                                <i class="fa fa-floppy-o"></i>
                                            </button>
                                        </form>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </c:otherwise>
                </c:choose>

                <c:if test="${orderInfo.orderType == 'SHARE'}">
                    <a href="${mainAppUrl}/campaigns/${campaign.campaignId}">${campaign.name}</a>
                </c:if>

                <c:if test="${not empty transaction}">
                    <table class="table table-bordered table-striped">
                        <tr class="qa-transaction-container">
                            <td>
                                <a href="/admin/billing/payment.html?paymentId=${transaction.transactionId}"
                                   target="_blank">${transaction.transactionId}</a>
                            </td>
                            <td>
                                <c:choose>
                                    <c:when test="${transaction.status eq 'NEW'}">
                                        <span class="label label-info">Новая</span>
                                    </c:when>
                                    <c:when test="${transaction.status eq 'DONE'}">
                                        <span class="label label-success">Завершена</span>
                                    </c:when>
                                    <c:when test="${transaction.status eq 'DECLINE'}">
                                        <span class="label label-warning">Отклонена</span>
                                    </c:when>
                                    <c:when test="${transaction.status eq 'ERROR'}">
                                        <span class="label label-danger">Ошибка</span>
                                    </c:when>
                                    <c:when test="${transaction.status == 'CANCELED'}">
                                        <span class="label label-info">Отменен</span>
                                    </c:when>
                                </c:choose>
                            </td>
                            <td>
                                    ${transaction.projectType}
                            </td>
                            <td>
                                <img style="max-width:52px;max-height:52px" src="//${baseUrl}${methods[transaction.paymentMethodId].imageUrl}"/>
                            </td>
                            <td>
                                <c:choose>
                                    <c:when test="${transaction.paymentProviderId >= 0}">
                                        <img src="//${baseUrl}/images/payment/${providers[transaction.paymentProviderId].type}.gif">
                                    </c:when>
                                    <c:otherwise>
                                        <i class="icon-question-sign"></i>
                                    </c:otherwise>
                                </c:choose>
                            </td>
                        </tr>
                    </table>
                </c:if>

                <c:if test="${isSuperAdmin}">
                    <c:if test="${orderInfo.paymentStatus != 'COMPLETED'}">
                    <form action="/api/profile/purchase-order-by-id.json" method="POST">
                        <input name="orderId" type="hidden" value="${orderInfo.orderId}">
                        <input type="submit" class="btn btn-primary btn-block" value="Провести неудавшийся платеж">
                    </form>
                    </c:if>
                </c:if>
            </div>
        </div>
    </div>
</body>
</html>


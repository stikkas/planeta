<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ include file="/WEB-INF/jsp/includes/generated/doctype-header.jsp" %>
<head>
    <title>Прайс лист изданий Почты России</title>
    <%@ include file="../head.jsp" %>
</head>

<body>
    <%@ include file="../navbar.jsp" %>
    <div id="container" class="container">
        <ct:breadcrumb title1="Библиородина" title2="Почта России, издания" title3="${hf:escapeHtml4(magazinePriceList.name)} [№ ${magazinePriceList.priceListId}]"/>
        <div>
            <p>Добавлен: ${hf:dateFormatByTemplate(magazinePriceList.timeAdded, 'dd MMMM yyyy в HH:mm')}</p>
        </div>
        <div class="row">
            <div class="span12">
                <c:choose>
                    <c:when test="${empty magazineInfoList}">
                        <p>По данному запросу ничего не найдено.</p>
                    </c:when>
                    <c:otherwise>
                        <table class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th style="text-align: center; vertical-align: middle;">Наименование издания</th>
                                <th style="text-align: center; vertical-align: middle;">Подписной<br> индекс<br> издания</th>
                                <th style="text-align: center; vertical-align: middle;">МСП</th>
                                <th style="text-align: center; vertical-align: middle;">Периодичность<br> (кол-во выходов<br> за 6 месяцев)</th>
                                <th style="text-align: center; vertical-align: middle;">Цена<br> за партию<br> товара (за МСП)<br> без НДС, руб.</th>
                                <th style="text-align: center; vertical-align: middle;">НДС, %</th>
                                <th style="text-align: center; vertical-align: middle;">Цена за партию<br>
                                    товара (за МСП)<br>
                                    с НДС, руб.
                                </th>
                                <th style="text-align: center; vertical-align: middle;">Комментарий месяц, в котором издание не выходит</th>
                            </tr>
                            </thead>

                            <c:forEach items="${magazineInfoList}" var="magazineInfo" varStatus="varStatus">
                                <tr>
                                    <td>${hf:escapeHtml4(magazineInfo.magazineName)}</td>
                                    <td>${hf:escapeHtml4(magazineInfo.pIndex)}</td>
                                    <td>${magazineInfo.msp}</td>
                                    <td>${magazineInfo.periodicity}</td>
                                    <td>${magazineInfo.partPriceWithoutTax}</td>
                                    <td>${magazineInfo.taxPercentage}</td>
                                    <td>${magazineInfo.partPriceWithTax}</td>
                                    <td>${magazineInfo.commentNoMagazineMonth}</td>
                                </tr>
                            </c:forEach>
                        </table>
                    </c:otherwise>
                </c:choose>
            </div>
        </div>
    <%--<%@ include file="paginator.jsp" %>--%>
    </div>
</body>
</html>


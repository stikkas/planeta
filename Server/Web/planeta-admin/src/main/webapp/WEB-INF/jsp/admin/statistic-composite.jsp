<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ include file="/WEB-INF/jsp/includes/generated/doctype-header.jsp" %>
<head>
    <%@ include file="head.jsp" %>
</head>
<body>


<script type="text/javascript">

    $(document).ready(function () {

        $('#day').click(function () {
            $('#step').val('day');
            $('#stat-params').submit();
        });

        $('#week').click(function () {
            $('#step').val('week');
            $('#stat-params').submit();
        });

        $('#month').click(function () {
            $('#step').val('month');
            $('#stat-params').submit();
        });

        $('.statPopupLink').click(function () {
            onStatPopupLinkClicked($(this));
        });

        $('[rel="tooltip"]').tooltip([]);
    });

    function onStatPopupLinkClicked(e) {
        var td = e.parent();
        var type = td.attr('data-type');
        var dateFrom = td.parent().attr('data-date-from');
        dateFrom = new Date(dateFrom - 0);
        dateFrom.setHours(0);
        dateFrom.setMinutes(0);
        dateFrom.setSeconds(0);
        dateFrom.setMilliseconds(0);
        dateFrom = "" + dateFrom.getTime();
        var step = td.parent().attr('step').toUpperCase();
        switch (type) {
            case 'user':
                window.open("/admin/statistic-user-modal.html?dateFrom=" + dateFrom + '&step=' + step, 'Статистика по регистрациям', 'height=680,width=960');
                break;
            case 'share':
                window.open("/admin/statistic-share-modal.html?dateFrom=" + dateFrom + '&step=' + step, 'Статистика по вознаграждениям', 'height=680,width=960');
                break;
            case 'product':
                window.open("/admin/statistic-product-modal.html?dateFrom=" + dateFrom + '&step=' + step, 'Статистика по товарам', 'height=680,width=960');
                break;
        }
    }
</script>


<%@ include file="/WEB-INF/jsp/admin/navbar.jsp" %>
<div id="page-wrapper">

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Общая статистика</h1>
        </div>
    </div>

    <div class="panel panel-primary">
        <div class="panel-heading">Поиск</div>

        <div class="panel-body">
            <form id="stat-params">
                <div class="row ma-b-20">
                    <div class="col-lg-12">
                        <ct:date-range-picker dateFrom="${dateFrom}" dateTo="${dateTo}" allButton="true"/>
                    </div>
                </div>

                <div class="row ma-b-20">
                    <div class="col-lg-12">
                        <label>Шаг</label>

                        <div class="btn-group" data-toggle="buttons-radio">
                            <button data-datefilter-step="day" id="day"
                                    <c:choose>
                                        <c:when test='${step == "day"}'>class="btn btn-outline btn-default active"</c:when>
                                        <c:otherwise>class="btn btn-outline btn-default"</c:otherwise>
                                    </c:choose>
                            >День
                            </button>
                            <button data-datefilter-step="week" id="week"
                                    <c:choose>
                                        <c:when test='${step == "week"}'>class="btn btn-outline btn-default active"</c:when>
                                        <c:otherwise>class="btn btn-outline btn-default"</c:otherwise>
                                    </c:choose>
                            >Неделя
                            </button>
                            <button data-datefilter-step="month" id="month"
                                    <c:choose>
                                        <c:when test='${step == "month"}'>class="btn btn-outline btn-default active"</c:when>
                                        <c:otherwise>class="btn btn-outline btn-default"</c:otherwise>
                                    </c:choose>
                            >Месяц
                            </button>
                        </div>
                    </div>

                </div>

                <button class="btn btn-success" formaction="sales-report.html">
                    <i class="fa fa-download"></i> Скачать в формате Excel
                </button>

                <input id="step" name="step" type="hidden" value="${step}"/>
                <div class="row">&nbsp;</div>
            </form>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <%@include file="/WEB-INF/jsp/admin/stat/stat-purchase.jsp" %>
        </div>
    </div>
</div>
</body>
</html>


<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ include file="/WEB-INF/jsp/includes/generated/doctype-header.jsp" %>
<link type="text/css" rel="stylesheet" href="//${hf:getStaticBaseUrl("")}/css/ui-lightness/jquery-ui-1.10.0-bootstrap.custom.css"/>
<head>
    <%@include file="head.jsp" %>
    <script type="text/javascript">
        $(document).ready(function() {
            var campaign = ${hf:toJson(campaign)};
            new Delivery.Views.ShareList({
                el: '.js-share-list',
                collection: new BaseCollection(campaign.shares)
            }).render();
        });
    </script>
</head>
<body>
    <%@ include file="navbar.jsp" %>
    <div id="page-wrapper">
        <div class="row ">
            <div class="col-lg-12">
                <%@include file="successMessage.jsp"%>
            </div>
        </div>

        <div class="main-page-actions">
            <a class="btn btn-success btn-circle btn-outline btn-lg" href="/moderator/campaign-moderation-info.html?campaignId=${campaign.campaignId}" title="Модерация проекта">
                <i class="fa fa-lock"></i>
            </a>

            <a class="btn btn-info btn-circle btn-outline btn-lg" href="${mainAppUrl}/campaigns/${campaign.campaignId}" title="Страница проекта на Planeta.ru">
                <i class="fa fa-file-o"></i>
            </a>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Проект № ${campaign.campaignId}</h1>
            </div>
        </div>

        <div class="row ma-b-30">
            <div class="col-lg-3">
                <img src="${hf:getGroupAvatarUrl(campaign.imageUrl, "MEDIUM" ) }" alt="${groupProfile.displayName}" class="img-polaroid" />
            </div>

            <div class="col-lg-5">
                <div><b>Название: </b><c:out value="${campaign.name}"/></div>
                <div><b>Краткое описание: </b><c:out value="${campaign.shortDescription}"/></div>
            </div>

            <div class="col-lg-3">

            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">Описание проекта</div>
                    <div class="panel-body">
                        <form method="POST" action="/moderator/campaign-info.html" class="form-horizontal">
                            <fieldset>
                                <div class="row ma-b-20">
                                    <div class="col-lg-12">
                                        <input type="hidden" name="campaignId" value="${campaign.campaignId}">
                                        <label>HTML описание</label>
                                        <textarea class="form-control"
                                                  id="descriptionHtml"
                                                  name="descriptionHtml"
                                                  rows="10"
                                                  placeholder="Описание в формате HTML">${campaign.descriptionHtml}</textarea>
                                    </div>
                                </div>

                                <div class="row ma-b-20">
                                    <div class="col-lg-12">
                                        <label>Описание</label>
                                        <div>
                                            <c:out value="${campaign.description}"/>
                                        </div>
                                    </div>
                                </div>

                                <div class="row ma-b-20">
                                    <div class="col-lg-3">
                                        <label>Спонсор</label>

                                        <select id="sponsorAlias" name="sponsorAlias" class="form-control">
                                            <option value="- Не выбрано -">- Не выбрано -</option>
                                            <c:forEach var="sponsor" items="${sponsors}">
                                                <c:if test="${sponsor.alias == campaign.sponsorAlias}">
                                                    <option value="${sponsor.alias}" selected="selected">${sponsor.alias}</option>
                                                </c:if>
                                                <c:if test="${sponsor.alias != campaign.sponsorAlias}">
                                                    <option value="${sponsor.alias}">${sponsor.alias}</option>
                                                </c:if>
                                            </c:forEach>
                                        </select>
                                    </div>
                                </div>

                                <div class="row ma-b-20">
                                    <div class="col-lg-12">
                                        <label>Мета данные</label>
                                        <textarea class="form-control" rows="5" name="metaData"><c:out value="${campaign.metaData}"/></textarea>
                                    </div>
                                </div>

                                <button type="submit" class="btn btn-primary">Сохранить</button>
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12 admin-table">
                <h2>Вознаграждения проекта:</h2>

                <table class="table table-striped table-bordered table-hover dataTable no-footer dtr-inline">
                    <thead>
                        <tr>
                            <th class="span1">#</th>
                            <th class="span4">Название, описание проекта</th>
                            <th class="span1">цена</th>
                            <th class="span1">остаток</th>
                            <th class="span4">способы доставки</th>
                        </tr>
                    </thead>

                    <tbody class="js-share-list">

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</body>
</html>

<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ include file="/WEB-INF/jsp/includes/generated/doctype-header.jsp" %>

<head>
    <%@ include file="head.jsp" %>
</head>


<body>
    <%@ include file="/WEB-INF/jsp/admin/navbar.jsp" %>

    <div id="page-wrapper">
        <div class="row" style="position: fixed; z-index: 10000; width: 300px; right: 30px;">
            <div class="col-lg-12">
                <%@include file="successMessage.jsp"%>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Пользователь № ${profile.profileId}</h1>
            </div>
        </div>

        <div class="main-page-actions">
            <a class="btn btn-info btn-circle btn-outline btn-lg"
               href="/admin/billing/transactions-history.html?profileId=${profile.profileId}"
               title="История транзакций">
                <i class="fa fa-money"></i>
            </a>

            <a class="btn btn-primary btn-circle btn-outline btn-lg"
               href="${mainAppUrl}/${profile.profileId}"
               title="Страница пользователя" target="_blank">
                <i class="fa fa-user"></i>
            </a>

            <a class="btn btn-success btn-circle btn-outline btn-lg"
               href="/admin/login-as-user.html?profileId=${profile.profileId}"
               title="Зайти под юзером">
                <i class="fa fa-user-secret"></i>
            </a>

            <c:if test="${isSuperAdmin}">
                <a href="/admin/gifts.html?profileId=${profile.profileId}"
                   class="btn btn-info btn-circle btn-outline btn-lg"
                   title="Подарить подарок">
                    <i class="fa fa-diamond"></i>
                </a>
            </c:if>

            <form id="delete-user-form" class="form-horizontal" method="post" action="/admin/delete-user.html" style="display: inline-block">
                <input type="hidden" name="profileId" value="${profile.profileId}"/>

                <button id="delete-user" class="btn btn-danger btn-circle btn-outline btn-lg"
                        title="Удалить пользователя. Данные для которых есть статус 'Удален' переходят в этот статус, остальные удаляются физически. Профайл пользователя помечается удаленным">
                    <i class="fa fa-trash"></i>
                </button>
            </form>

            <c:if test="${regCode != null}">
                <form class="form-horizontal" method="post" action="/moderator/confirm-user-registration.html" style="display: inline-block">
                    <input type="hidden" name="profileId" value="${profile.profileId}"/>

                    <button id="confirmRegistration"
                            name="confirmRegistration"
                            title="Подтвердить регистрацию"
                            class="btn btn-success btn-circle btn-outline btn-lg"
                            type="submit">
                        <i class="fa fa-check"></i>
                    </button>
                </form>
            </c:if>

            <c:if test="${regCode != null}">
                <form class="form-horizontal" method="post" action="/moderator/send-registration-complete-email.html" style="display: inline-block">
                    <input type="hidden" name="profileId" value="${profile.profileId}"/>

                    <button id="sendActivationEmailOnceAgain"
                            name="sendActivationEmailOnceAgain"
                            class="btn btn-primary btn-circle btn-outline btn-lg"
                            type="submit"
                            title="Повторно выслать письмо подтверждения регистрации">
                        <i class="fa fa-envelope"></i>
                    </button>
                </form>
            </c:if>

            <form class="form-horizontal" method="post" action="/moderator/unsubscribe-user-from-all-email-notifications.html" style="display: inline-block">
                <input type="hidden" name="profileId" value="${profile.profileId}"/>
                <button class="btn btn-warning btn-circle btn-outline btn-lg" type="submit" title="Отписать от уведомлений на email">
                    <i class="fa fa-minus-circle"></i>
                </button>
            </form>
        </div>

        <div class="row">
            <div class="col-lg-5" style="min-height: 220px">
                <h3>Основные данные</h3>
                <img src="${hf:getUserAvatarUrl(profile.smallImageUrl, "USER_AVATAR", profile.userGender)}" alt='<c:out value="${profile.displayName}"/>' class="img-polaroid" style="margin-right: 20px; float: left">

                <div><b>Имя: </b><c:out value="${profile.displayName}"/></div>
                <div><b>E-mail: </b><a href='mailto:<c:out value="${email}"/>'><c:out value="${email}"/></a></div>
                <div><b>Дата регистрации:</b> <fmt:formatDate pattern="HH:mm dd.MM.yyyy" value="${userPrivateInfo.timeAdded}"/></div>
            </div>

            <c:if test="${isSuperAdmin}">
                <div class="col-lg-7">
                    <h3>Роль пользователя</h3>

                    <div class="panel panel-primary">
                        <div class="panel-body">
                            <div class="col-lg-12">
                                <div class="row">
                                    <form id="editUser" method="post">
                                        <input type="hidden" name="profileId" value="${profile.profileId}"/>
                                        <input type="hidden" value="USER" readonly="readonly" <c:if test="${hf:containsStringInEnumSet(userPrivateInfo.userStatus, 'USER')}">name="roleNames"</c:if>/>

                                        <fieldset>
                                            <div class="form-group">
                                                <div class="checkbox">
                                                    <label class="checkbox">
                                                        <input type="checkbox" name="roleNames" value="BLOCKED"
                                                               <c:if test="${hf:containsStringInEnumSet(userPrivateInfo.userStatus, 'BLOCKED')}">checked="yes"</c:if>>
                                                        Заблокирован <span class="muted">(Пользователь не сможет войти на сайт)</span>
                                                    </label>
                                                </div>
                                                <div class="checkbox">

                                                    <label class="checkbox">
                                                        <input type="checkbox" name="roleNames" value="DELETED"
                                                               <c:if test="${hf:containsStringInEnumSet(userPrivateInfo.userStatus, 'DELETED')}">checked="yes"</c:if>>
                                                        Удален <span class="muted">(В данный момент не используется)</span>
                                                    </label>
                                                </div>
                                                <div class="checkbox">
                                                    <label class="checkbox">
                                                        <input type="checkbox" name="roleNames" value="NO_COMMENT"
                                                               <c:if test="${hf:containsStringInEnumSet(userPrivateInfo.userStatus, 'NO_COMMENT')}">checked="yes"</c:if>>
                                                        Без комментариев <span class="muted">(Пользователь не может оставлять комментарии)</span>
                                                    </label>
                                                </div>
                                                <div class="checkbox">
                                                    <label class="checkbox">
                                                        <input type="checkbox" name="roleNames" value="ADMIN"
                                                               <c:if test="${hf:containsStringInEnumSet(userPrivateInfo.userStatus, 'ADMIN')}">checked="yes"</c:if>>
                                                        Админ <span class="muted">(Пользователь который может и видит почти ВСЕ)</span>
                                                    </label>
                                                </div>
                                                <div class="checkbox">
                                                    <label class="checkbox">
                                                        <input type="checkbox" name="roleNames" value="SUPER_ADMIN"
                                                               <c:if test="${hf:containsStringInEnumSet(userPrivateInfo.userStatus, 'SUPER_ADMIN')}">checked="yes"</c:if>>
                                                        Супер Админ <span class="muted">(Пользователь который может и видит ВСЕ)</span>
                                                    </label>
                                                </div>
                                            </div>

                                            <button id="submitRoles" name="submitRoles" class="btn btn-primary"
                                                    formaction="set-user-role.html" type="submit">Сохранить
                                            </button>
                                        </fieldset>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </c:if>
        </div>

        <h3>Пароли и письма</h3>

        <div class="row">
            <div class="col-lg-3">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        Изменение пароля
                    </div>

                    <div class="panel-body">
                        <form class="form-horizontal" method="post" action="/moderator/change-user-password.html">
                            <input type="hidden" name="profileId" value="${profile.profileId}"/>
                            <fieldset>

                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label>Новый пароль</label>
                                        <input class="form-control" type="password" name="password" placeholder="Новый пароль" >
                                        <p class="help-block">Изменяет пароль пользователя без подтверждений, будьте аккуратны.</p>
                                    </div>
                                </div>

                                <button id="submitNewPassword" name="submitNewPassword" class="btn btn-primary" type="submit">
                                    Сохранить
                                </button>
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>

            <div class="col-lg-3">
                <c:set value="${profile.alias}" var="aliasName"/>
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        Адрес страницы
                    </div>

                    <div class="panel-body">
                        <form id="changeAlias" class="form-horizontal" method="post" action="/moderator/event-set-alias.html">
                            <input type="hidden" name="profileId" value="${profile.profileId}"/>
                            <fieldset>

                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label>Новый адрес страницы</label>
                                        <input class="form-control" type="text" name="aliasName" placeholder="отсутствует">
                                        <p class="help-block">Новый адрес будет иметь вид: ${mainAppUrl}/<b id="newAlias"><c:choose><c:when test="${aliasName != null}"><c:out value="${aliasName}"/></c:when><c:otherwise>&lt;указанный адрес&gt;</c:otherwise></c:choose></b></p>
                                    </div>
                                </div>

                                <button id="changeAliasButton" name="changeAliasButton" class="btn btn-primary" type="submit">Сохранить</button>
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>

            <div class="col-lg-3">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        Изменение email
                    </div>

                    <div class="panel-body">
                        <form class="form-horizontal" method="post" action="/moderator/change-user-email.html">
                            <input type="hidden" name="profileId" value="${profile.profileId}"/>
                            <fieldset>

                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label>Новый email</label>
                                        <input class="form-control" type="text" name="email" placeholder="Новый email" >
                                        <p class="help-block">Изменяет email пользователя без подтверждений, будьте аккуратны.</p>
                                    </div>
                                </div>

                                <button id="submitNewEmail" name="submitNewEmail" class="btn btn-primary" type="submit">
                                    Сохранить
                                </button>
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>

            <div class="col-lg-3">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        Статус
                    </div>

                    <div class="panel-body">
                        <form class="form-horizontal" method="post" action="/moderator/user-set-status.html">
                            <input type="hidden" name="profileId" value="${profile.profileId}"/>
                            <fieldset>
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label>Новый статус</label>

                                        <select name="status" class="form-control">
                                            <option value="NOT_SET" <c:if test="${profile.status == 'NOT_SET'}">selected="selected"</c:if>>
                                                Не подтвердил регистрацию
                                            </option>

                                            <option value="USER_BOUNCED" <c:if test="${profile.status == 'USER_BOUNCED'}">selected="selected"</c:if>>
                                                Отписан
                                            </option>

                                            <option value="USER_ACTIVE" <c:if test="${profile.status == 'USER_ACTIVE'}">selected="selected"</c:if>>
                                                Активный
                                            </option>

                                            <option value="USER_SPAMMER" <c:if test="${profile.status == 'USER_SPAMMER'}">selected="selected"</c:if>>
                                                Спамер
                                            </option>
                                        </select>
                                    </div>
                                </div>

                                <button class="btn btn-primary" type="submit">
                                    Сохранить
                                </button>
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <script type="text/javascript">
            $("#deleteByProfileId-user").click(function (e) {
                e.preventDefault();
                var yes = confirm('Вы действительно хотите удалить пользователя? После удаления его будет невозможно восстановить.');

                if (yes) {
                    $('#deleteByProfileId-user-form').submit();
                }
            });
        </script>

        <h3>Контент</h3>

        <div class="row">
            <div class="col-lg-6">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        Удаление постов
                    </div>

                    <div class="panel-body">
                        <form id="delete-posts-form" class="form-horizontal" method="post" action="/admin/delete-user-posts.html">
                            <input type="hidden" name="profileId" value="${profile.profileId}"/>
                            <fieldset>
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <input type="hidden" name="profileId" value="${profile.profileId}"/>

                                        <label>Количество дней за которые нужно удалить посты</label>
                                        <input class="form-control" type="text" name="daysCount" placeholder="0"/>
                                    </div>
                                </div>

                                <button class="btn btn-primary" type="submit">
                                    Удалить
                                </button>
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>

            <div class="col-lg-6">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        Удаление комментариев
                    </div>

                    <div class="panel-body">
                        <form id="delete-comments-form" class="form-horizontal" method="post" action="/admin/delete-user-comments.html">
                            <input type="hidden" name="profileId" value="${profile.profileId}"/>
                            <fieldset>
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <input type="hidden" name="profileId" value="${profile.profileId}"/>

                                        <label>Количество дней за которые нужно удалить комментарии</label>
                                        <input class="form-control" type="text" name="daysCount" placeholder="0"/>
                                    </div>
                                </div>

                                <button class="btn btn-primary" type="submit">
                                    Удалить
                                </button>
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div>


        <h3>Деньги</h3>

        <c:if test="${isSuperAdmin}">
            <div class="row">
                <div class="col-lg-6">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            Баланс
                        </div>

                        <div class="panel-body">
                            <form id="decrease-balance-form" class="form-horizontal" method="post" action="/admin/decrease-user-balance.html">
                                <input type="hidden" name="profileId" value="${profile.profileId}"/>
                                <fieldset>
                                    <div class="col-lg-12">
                                        <input type="hidden" name="profileId" value="${profile.profileId}"/>

                                        <div class="form-group">
                                            <label>Текущий баланс: ${balance}</label>
                                        </div>

                                        <div class="form-group">
                                            <label>На сколько уменьшить баланс</label>
                                            <input class="form-control" type="text" name="amount"/>
                                            <p class="help-block">Итоговый баланс пользователя не может быть меньше нуля.</p>
                                        </div>
                                    </div>

                                    <button class="btn btn-primary" type="submit">
                                        Удалить
                                    </button>
                                </fieldset>
                            </form>
                        </div>
                    </div>
                </div>

                <div class="col-lg-6">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            Замороженная сумма
                        </div>

                        <div class="panel-body">
                            <form id="frozen-amount-form" class="form-horizontal" method="post">
                                <input type="hidden" name="profileId" value="${profile.profileId}"/>
                                <fieldset>
                                    <div class="col-lg-12">
                                        <input type="hidden" name="profileId" value="${profile.profileId}"/>

                                        <div class="form-group">
                                            <label>Замороженная сумма: ${frozenAmount}</label>
                                        </div>

                                        <label>Cумма</label>

                                        <div class="form-group input-group">
                                            <input type="text" class="form-control" name="amount">
                                            <span class="input-group-btn">
                                                <button class="btn btn-default js-all-frozen-amount" type="button">Вся сумма</button>
                                            </span>
                                        </div>

                                    </div>

                                    <button class="btn btn-primary js-freeze">Заморозить</button>
                                    <button class="btn btn-default js-unfreeze">Вернуть на счет</button>
                                    <button class="btn btn-default js-decrease">Списать</button>
                                </fieldset>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

            <script type="text/javascript">
                $("#decrease-balance-form .btn").click(function (e) {
                    e.preventDefault();
                    if (Modal) {
                        Modal.showConfirm("Вы действительно хотите уменьшить баланс пользователя?", "Подтверждение действия", {
                            success: function () {
                                $('#decrease-balance-form').submit();
                            }
                        });
                    }
                });

                $("#deleteByProfileId-posts-form .btn").click(function (e) {
                    e.preventDefault();
                    if (Modal) {
                        Modal.showConfirm("Вы действительно хотите удалить посты пользователя?", "Подтверждение действия", {
                            success: function () {
                                $('#deleteByProfileId-posts-form').submit();
                            }
                        });
                    }
                });

                $("#deleteByProfileId-comments-form .btn").click(function (e) {
                    e.preventDefault();
                    if (Modal) {
                        Modal.showConfirm("Вы действительно хотите удалить комментарии пользователя?", "Подтверждение действия", {
                            success: function () {
                                $('#deleteByProfileId-comments-form').submit();
                            }
                        });
                    }
                });

                var frozenAmountForm = $('#frozen-amount-form');

                function showError(msg){
                    var $el = workspace.appView.showErrorMessage(msg);
                    $el.css({
                        display: "inline-block",
                        position: "fixed",
                        top: 5,
                        right: 5,
                        "z-index": 2100
                    });
                }

                frozenAmountForm.find('.js-freeze').click(function(e){
                    e.preventDefault();
                    var amount = frozenAmountForm.find('[name=amount]').val() || 0;
                    if (Modal) {
                        if (amount <= 0) {
                            showError("Сумма не может быть меньшей или равной 0");
                        } else if (amount > ${balance}) {
                            showError("Сумма не может быть больше чем текущий баланс ${balance}");
                        } else {
                            Modal.showConfirm("Вы действительно хотите заморозить " + amount + " у пользователя?", "Подтверждение действия", {
                                success: function () {
                                    frozenAmountForm.attr('action', '/admin/freeze-amount.html');
                                    frozenAmountForm.submit();
                                }
                            });
                        }
                    }
                });


                frozenAmountForm.find('.js-unfreeze').click(function(e){
                    e.preventDefault();
                    var amount = frozenAmountForm.find('[name=amount]').val() || 0;
                    if (Modal) {
                        if (amount <= 0) {
                            showError("Сумма не может быть меньшей или равной 0");
                        } else if (amount > ${frozenAmount}) {
                            showError("Сумма не может быть больше чем замороженная сумма ${frozenAmount}");
                        } else {
                            Modal.showConfirm("Вы действительно хотите вернуть на счет пользователя (разморозить) " + amount + "?", "Подтверждение действия", {
                                success: function () {
                                    frozenAmountForm.attr('action', '/admin/unfreeze-amount.html');
                                    frozenAmountForm.submit();
                                }
                            });
                        }
                    }
                });

                frozenAmountForm.find('.js-decrease').click(function(e){
                    e.preventDefault();
                    var amount = frozenAmountForm.find('[name=amount]').val() || 0;
                    if (Modal) {
                        if (amount <= 0) {
                            showError("Сумма не может быть меньшей или равной 0");
                        } else if (amount > ${frozenAmount}) {
                            showError("Сумма не может быть больше чем замороженная сумма ${frozenAmount}");
                        } else {
                            Modal.showConfirm("Вы действительно хотите списать " + amount + " из замороженной суммы у пользователя?", "Подтверждение действия", {
                                success: function () {
                                    frozenAmountForm.attr('action', '/admin/decrease-frozen-amount.html');
                                    frozenAmountForm.submit();
                                }
                            });
                        }
                    }
                });

                frozenAmountForm.find('.js-all-frozen-amount').click(function(e){
                    e.preventDefault();
                    frozenAmountForm.find('[name=amount]').val(${frozenAmount});
                });

            </script>
        </c:if>
    </div>
</body>
</html>

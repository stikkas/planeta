<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ include file="/WEB-INF/jsp/includes/generated/doctype-header.jsp" %>
<head>
    <%@ include file="head.jsp" %>
</head>
<body>

<%@ include file="/WEB-INF/jsp/admin/navbar.jsp" %>

<div id="page-wrapper">
    <div class="row">
        <c:if test="${successMessage != null}">
        <div class="col-lg-12">
            <div class="alert alert-success">
                ${successMessage}
            </div>
        </div>
        </c:if>
        <c:if test="${errorMessage != null}">
            <div class="col-lg-12">
                <div class="alert alert-danger">
                        ${errorMessage}
                </div>
            </div>
        </c:if>
        <div class="col-lg-12">
            <div class="mrg-b-30">
                <h1 class="page-header">Подарок пользователю <a href="/moderator/user-info.html?profileId=${profileId}">№${profileId}</a></h1>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12" id="content-container">
            <form method="post">
                <div class="settings-container">
                    <div class="row ma-t-30">
                        <div class="col-lg-12 ma-b-20">
                            <label>Сумма подарка</label>
                            <input id="amount" name="amount" type="number" class="form-control" value="0">
                            <input id="profileId" name="profileId" type="hidden" class="form-control" value="${profileId}">
                        </div>
                        <div class="col-lg-12 ma-b-20">
                            <label>Баланс Планеты:&nbsp;&nbsp; </label>
                            <span class="text-bold text-success">${planetaBalance} руб.</span>
                        </div>
                        <div class="col-lg-12 ma-b-20">
                            <label for="comment">Комментарий</label>
                            <textarea id="comment" name="comment" class="form-control" rows="5"></textarea>
                        </div>
                    </div>
                    <div class="btn-group">
                        <button type="submit" class="btn btn-primary<c:if test='${planetaBalance <= 0}'> disabled</c:if>">Подарить</button>
                        <a href="/moderator/user-info.html?profileId=191" class="btn btn-default">Назад</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

</body>
</html>

<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ include file="/WEB-INF/jsp/includes/generated/doctype-header.jsp" %>
<head>
    <%@ include file="head.jsp" %>
</head>
<body>

<%@ include file="/WEB-INF/jsp/admin/navbar.jsp" %>

<div id="page-wrapper">

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Статистика по товарам</h1>
        </div>
    </div>

    <div class="panel panel-primary">
        <div class="panel-heading">Поиск</div>

        <div class="panel-body">
            <form id="stat-params" method="GET">
                <div class="row ma-b-20">
                    <div class="col-lg-12">
                        <ct:date-range-picker dateFrom="${dateFrom}" dateTo="${dateTo}" allButton="true"/>
                    </div>
                </div>

                <button class="btn btn-success" formaction="sales-products-report.html">
                    <i class="fa fa-download"></i> Скачать в формате Excel
                </button>
            </form>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <%@include file="/WEB-INF/jsp/admin/stat/stat-purchase-product.jsp" %>
        </div>
    </div>
</div>
</body>
</html>


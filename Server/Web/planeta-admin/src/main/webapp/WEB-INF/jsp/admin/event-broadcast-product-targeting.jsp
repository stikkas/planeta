<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ include file="/WEB-INF/jsp/includes/generated/doctype-header.jsp" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<head>
	<%@ include file="head.jsp" %>

</head>
<body>
<%@ include file="/WEB-INF/jsp/admin/navbar.jsp" %>
<div id="page-wrapper">

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                Ограничения по товарам для трансляции
                <a href="/moderator/event-broadcast-info.html?broadcastId=${broadcastProductTargeting.broadcastId}">
                    № ${broadcastProductTargeting.broadcastId}
                </a>
            </h1>
        </div>
    </div>

	<div class="row">
		<div class="col-lg-12">
			<form:form method="post" commandName="broadcastProductTargeting"
			           class="form-horizontal">
				<form:input type="hidden" path="broadcastId"/>
    
                <div class="row ma-b-20">
                    <div class="col-lg-6">
                        <label>ID товара</label>
                        <form:input type="text" path="productId" cssClass="form-control"/>
                        <form:errors path="productId"><span
                                class="help-inline"><form:errors path="productId"/></span></form:errors>
                    </div>
                </div>

                <div class="btn-group">
                    <button type="submit" class="btn btn-primary">Сохранить</button>
                    <a class="btn btn-default" href="/moderator/event-broadcast-info.html?broadcastId=${broadcastProductTargeting.broadcastId}">Отмена</a>
                </div>
			</form:form>
		</div>
	</div>
</div>
</body>

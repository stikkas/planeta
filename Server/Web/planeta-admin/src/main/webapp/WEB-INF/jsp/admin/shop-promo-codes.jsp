<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@taglib prefix="p" uri="http://planeta.ru/tags" %>
<html>
<head>
    <title>Промокоды</title>
    <%@ include file="/WEB-INF/jsp/admin/head.jsp" %>
</head>
<body>

<%@ include file="/WEB-INF/jsp/admin/navbar.jsp" %>

<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Промокоды</h1>
        </div>
    </div>

    <div class="main-page-actions">
        <a class="btn btn-success btn-circle btn-outline btn-lg" href="/moderator/shop/promo-code.html" title="Создать новый промокод">
            <i class="fa fa-plus"></i>
        </a>
    </div>

    <div class="row">
        <div class="col-lg-12">

            <form method=get id="promoCodeSearchForm">
                <div class="form-group input-group">
                    <input id="xlInput"
                           class="form-control"
                           type="text"
                           placeholder="Название промокода или его код"
                           name="searchString"
                           value="${searchString}">

                    <span class="input-group-btn">
                        <button class="btn btn-primary" type="submit">
                            <i class="fa fa-search"></i>
                        </button>
                    </span>
                </div>
            </form>
        </div>
    </div>

    <c:if test="${not empty promoCodesList}">
        <div class="row">
            <div class="col-lg-12 admin-table">
                <table class="table table-hover table-bordered">
                    <thead>
                        <tr>
                            <th>Название</th>
                            <th>Скидка</th>
                            <th>Ограничения</th>
                            <th>Время<br>действия</th>
                            <th>Осталось<br>использований</th>
                            <th>Категория<br>товара</th>
                            <th>Пользователь</th>
                            <th>Действие</th>
                        </tr>
                    </thead>
                    <tbody>
                    <c:forEach items="${promoCodesList}" var="promoCode">
                        <tr>
                            <td>
                                    ${promoCode.title}<br>
                                <small><span class="muted">${promoCode.code}</span></small>
                            </td>
                            <td>
                                <c:choose>
                                    <c:when test="${promoCode.discountType == 'ABSOLUTE'}">
                                        Абсолютная, <b>${promoCode.discountAmount} руб</b>
                                    </c:when>
                                    <c:when test="${promoCode.discountType == 'RELATIVE'}">
                                        Относительная, <b>${promoCode.discountAmount}%</b>
                                    </c:when>
                                    <c:when test="${promoCode.discountType == 'PRODUCTFREE'}">
                                        Бесплатные товары, <b>${promoCode.discountAmount} шт</b>
                                    </c:when>
                                </c:choose>
                                <br>
                                <small>
                                    <c:if test="${promoCode.freeDelivery}">
                                        Бесплатная доставка
                                    </c:if>
                                </small>
                            </td>
                            <td>
                                <c:choose>
                                    <c:when test="${promoCode.disabled}">
                                        <span class="label label-info">Отключен</span>
                                    </c:when>
                                    <c:otherwise>
                                        <c:choose>
                                            <c:when test="${promoCode.codeUsageType == 'UNLIMITED'}">
                                                <span class="label label-success">Без ограничений</span>
                                            </c:when>
                                            <c:when test="${promoCode.codeUsageType == 'LIMITED'}">
                                                <span class="label label-warning">По количеству</span>
                                            </c:when>
                                            <c:when test="${promoCode.codeUsageType == 'PERSONAL'}">
                                                <span class="label label-primary">Персональный</span>
                                            </c:when>
                                            <c:when test="${promoCode.codeUsageType == 'USERLIMITED'}">
                                                <span class="label label-primary">По пользователю</span>
                                            </c:when>
                                        </c:choose>
                                    </c:otherwise>
                                </c:choose>
                            </td>
                            <td>
                                <fmt:formatDate value="${promoCode.timeBegin}" pattern="dd.MM.yyyy"/>
                                &ndash;
                                <fmt:formatDate value="${promoCode.timeEnd}" pattern="dd.MM.yyyy"/>
                            </td>
                            <td>
                                <c:choose>
                                    <c:when test="${promoCode.codeUsageType != 'UNLIMITED'}">
                                        <c:if test="${promoCode.usageCount != ''}">
                                            ${promoCode.usageCount}
                                        </c:if>
                                    </c:when>
                                    <c:otherwise>
                                        &mdash;
                                    </c:otherwise>
                                </c:choose>
                            </td>
                            <td>
                                <c:if test="${promoCode.productTag != ''}">
                                    ${promoCode.productTag}
                                </c:if>
                            </td>
                            <td>
                                <c:choose>
                                    <c:when test="${promoCode.codeUsageType == 'PERSONAL'}">
                                        <c:if test="${promoCode.profileId != ''}">
                                            <a href="${mainAppUrl}/${promoCode.profileId}">${promoCode.profileDisplayName}</a>
                                        </c:if>
                                    </c:when>
                                    <c:otherwise>
                                        &mdash;
                                    </c:otherwise>
                                </c:choose>
                            </td>
                            <td class="text-right">
                                <form method="post" action="/moderator/shop/delete-promo-code.json" class="admin-table-actions">
                                    <input type="hidden" name="promoCodeId" value="${promoCode.promoCodeId}"/>
                                    <input type="hidden" name="disabled" value="${promoCode.disabled}"/>

                                    <a class="btn btn-outline btn-primary"
                                       href="/moderator/shop/promo-code.html?promoCodeId=${promoCode.promoCodeId}"
                                       title="Редактировать">
                                        <i class="fa fa-pencil"></i>
                                    </a>

                                        <c:choose>
                                            <c:when test="${promoCode.disabled}">
                                                <button type="submit" class="btn btn-success btn-outline" title="Включить">
                                                    <i class="fa fa-play"></i>
                                                </button>
                                            </c:when>
                                            <c:otherwise>
                                                <button type="submit" class="btn btn-warning btn-outline" title="Выключить">
                                                    <i class="fa fa-stop"></i>
                                                </button>
                                            </c:otherwise>
                                        </c:choose>
                                    </button>
                                </form>
                            </td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
            </div>
        </div>

        <%@ include file="paginator.jsp" %>
    </c:if>

</div>
</body>
</html>

<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<c:set var="page" value="${(offset div limit) + 1}"/>
<c:set var="pageCount" value="${((count - 1) - (count - 1) mod limit) / limit + 1}"/>
<c:set var="pagePrev" value="${page - 2}"/>

<c:if test="${pageCount > 1}">
    <div class="row">
        <div class="col-lg-12">
            <ul class="pagination">
                <c:if test="${page > 1}">
                    <li class="paginate_button">
                        <a href="javascript:void(0);" data-offset="${offset - limit}" data-limit="${limit}">Prev</a>
                    </li>
                </c:if>
                <c:if test="${page - 2 > 1}">
                    <li class="paginate_button">
                        <a href="javascript:void(0);" data-offset="0" data-limit="${limit}">1</a>
                    </li>
                    <li><a>...</a></li>
                </c:if>
                <c:if test="${page - 2 >= 1}">
                    <li class="paginate_button">
                        <a href="javascript:void(0);" data-offset="${offset - 2 * limit}" data-limit="${limit}">
                            <fmt:formatNumber maxFractionDigits="0" value="${page - 2}"/>
                        </a>
                    </li>
                </c:if>
                <c:if test="${page - 1 >= 1}">
                    <li class="paginate_button">
                        <a href="javascript:void(0);" data-offset="${offset - 1 * limit}" data-limit="${limit}">
                            <fmt:formatNumber maxFractionDigits="0" value="${page - 1}"/>
                        </a>
                    </li>
                </c:if>
                <c:if test="${page <= pageCount}">
                    <li class="paginate_button active">
                        <a href="javascript:void(0);" data-offset="${offset}" data-limit="${limit}">
                            <fmt:formatNumber maxFractionDigits="0" value="${page}"/>
                        </a>
                    </li>
                </c:if>
                <c:if test="${page + 1 <= pageCount}">
                    <li class="paginate_button">
                        <a href="javascript:void(0);" data-offset="${offset + 1 * limit}" data-limit="${limit}">
                            <fmt:formatNumber maxFractionDigits="0" value="${page + 1}"/>
                        </a>
                    </li>
                </c:if>
                <c:if test="${page + 2 <= pageCount}">
                    <li class="paginate_button">
                        <a href="javascript:void(0);" data-offset="${offset + 2 * limit}" data-limit="${limit}">
                            <fmt:formatNumber maxFractionDigits="0" value="${page + 2}"/>
                        </a>
                    </li>
                </c:if>
                <c:if test="${pageCount - page >= 1}">
                    <li class="paginate_button">
                        <a href="javascript:void(0);" data-offset="${offset + 1 * limit}" data-limit="${limit}">Next</a>
                    </li>
                </c:if>
            </ul>
        </div>
    </div>
</c:if>
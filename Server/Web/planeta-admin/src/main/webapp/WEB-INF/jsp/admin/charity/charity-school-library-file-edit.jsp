<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags/form" %>
<%@ include file="/WEB-INF/jsp/includes/generated/doctype-header.jsp" %>
<head>

    <%@include file="../head.jsp" %>

</head>
<body>

<%@ include file="/WEB-INF/jsp/admin/navbar.jsp" %>

<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                <c:if test="${libraryFile.themeId == null || libraryFile.themeId == 0}">
                    Добавление нового файла
                </c:if>
                <c:if test="${libraryFile.themeId > 0}">
                    Редактирование файла
                </c:if>
            </h1>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <s:form class="form-horizontal" commandName="libraryFile" method="post">
                <div class="row ma-b-20">
                    <div class="col-lg-12">
                        <label>Тема</label>
                        <form:select id="type" path="themeId" class="form-control">
                            <c:if test="${libraryFile.themeId == null || libraryFile.themeId == 0}">
                                <form:option value="0" selected="true"
                                             label="-- Выберите тему (раздел) для файла --" />
                            </c:if>
                            <c:forEach items="${libraryFileThemes}" var="libraryFileTheme" varStatus="st">
                                <c:if test="${libraryFileTheme.themeId == libraryFile.themeId}">
                                    <form:option value="${libraryFileTheme.themeId}" selected="true"
                                                 label="${libraryFileTheme.themeHeader}" />
                                </c:if>
                                <c:if test="${libraryFileTheme.themeId != libraryFile.themeId}">
                                    <form:option value="${libraryFileTheme.themeId}"
                                                 label="${libraryFileTheme.themeHeader}" />
                                </c:if>
                            </c:forEach>
                        </form:select>
                        <a href="/admin/charity/school-library-themes.html" target="_blank">Список тем/разделов</a>
                        <s:errors path="themeId" cssClass="error-message" element="span" />
                    </div>
                </div>

                <div class="row ma-b-20">
                    <div class="col-lg-6">
                        <ct:file url="${libraryFile.url}" path="url" label="Загрузить файл" type="FILE"/>
                    </div>
                </div>

                <div class="row ma-b-20">
                    <div class="col-lg-12">
                        <label>Заголовок</label>
                        <s:textarea path="header" type="text" placeholder="" class="form-control" required="" rows="4"/>
                        <s:errors path="header" cssClass="error-message" element="span" />
                    </div>
                    <s:input path="libraryFileId" type="hidden" />
                </div>

                <div class="btn-group">
                    <button formaction="/admin/charity/school-library-file-edit.html" class="btn btn-primary">
                        <c:if test="${libraryFile.themeId == null || libraryFile.themeId == 0}">
                            Добавить
                        </c:if>
                        <c:if test="${libraryFile.themeId > 0}">
                            Сохранить
                        </c:if>
                    </button>
                    <a href="/admin/charity/school-library-files.html" class="btn btn-default">Отмена</a>
                </div>

            </s:form>
        </div>
    </div>
</div>

</body>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ include file="/WEB-INF/jsp/includes/generated/doctype-header.jsp" %>
<head>
    <%@include file="head.jsp" %>
</head>
<body>
    <%@ include file="/WEB-INF/jsp/admin/navbar.jsp" %>
    <div id="container" class="container">



        <div class="row ">
            <div class="span12">
                <ul class="breadcrumb">
                    <li><a href="/">Главная</a> <span class="divider">/</span></li>
                    <li class="active">Модерация <span class="divider">/</span></li>
                    <li><a href="/moderator/users.html">Сообщества</a> <span class="divider">/</span></li>
                    <li class="active">Сообщество № ${groupProfile.profileId}</li>
                </ul>
            </div>
        </div>



        <div class="row ">
            <div class="span12">
                <%@include file="successMessage.jsp"%>
            </div>
        </div>



        <div class="row ">
            <div class="span12">
                <div class="mrg-b-30">
                    <h2>Сообщество № ${groupProfile.profileId}</h2>
                </div>
            </div>
        </div>



        <div class="row ">
            <div class="span2">
                <img src="${hf:getGroupAvatarUrl(groupProfile.imageUrl, "MEDIUM" ) }" alt="${groupProfile.displayName}" class="img-polaroid" />
            </div>
            <div class="span5">
                <div><b>Название: </b>${groupProfile.displayName}</div>
                <div><b>Описание: </b>${groupProfile.summary}</div>
                <div>
                    <b>Категория: </b>
                    <c:set value="${groupProfile.groupCategory}" var="group_category_type"/>
                    <%@ include file="/WEB-INF/jsp/includes/generated/group-category-translate.jsp" %>
                </div>
                </br>
                <div><b>Контактная информация: </b>${group.contactInfo}</div>
                <div>
                    <b>Статус: </b>
                    <c:if test="${groupProfile.status=='GROUP_PENDING'}">
                        На согласовании
                    </c:if>
                    <c:if test="${groupProfile.status=='GROUP_REJECTED'}">
                        Отклонено
                    </c:if>
                    <c:if test="${groupProfile.status=='GROUP_ACTIVE'}">
                        Согласовано
                    </c:if>
                    <c:if test="${groupProfile.status=='GROUP_ACTIVE_REQUEST_OFFICIAL'}">
                        Запрошен официальный статус
                    </c:if>
                    <c:if test="${groupProfile.status=='GROUP_ACTIVE_OFFICIAL'}">
                        Присвоен официальный статус
                    </c:if>
                </div>
            </div>
            <div class="span4">
                <a href="${mainAppUrl}/${groupProfile.profileId}">Страница сообщества</a>
            </div>
        </div>



        <div class="row ">
            <div class="span12">
                <form method="POST" action="/moderator/group-set-status.html" class="form-horizontal">
                    <fieldset>
                        <legend>Статус сообщества</legend>
                        <input type="hidden" name="profileId" value="${group.profileId}">

                        <div class="control-group">
                            <label class="control-label">Новый статус сообщества</label>

                            <div class="controls">
                                <select name="status">

                                    <option
                                            value="GROUP_ACTIVE_OFFICIAL"
                                            <c:if test="${groupProfile.status == 'GROUP_ACTIVE_OFFICIAL'}">selected="selected"</c:if>
                                            >Официальное
                                    </option>

                                    <option
                                            value="GROUP_ACTIVE"
                                            <c:if test="${groupProfile.status == 'GROUP_ACTIVE' || groupProfile.status == 'GROUP_ACTIVE_REQUEST_OFFICIAL'}">selected="selected"</c:if>
                                            >Согласовано
                                    </option>

                                    <option
                                            value="GROUP_REJECTED"
                                            <c:if test="${groupProfile.status == 'GROUP_REJECTED'}">selected="selected"</c:if>
                                            >Отклонено
                                    </option>
                                </select>
                                <div>
                                    <br /><strong><i>Официальное</i></strong> - сообщество подало документы на официальный статус и
                                        получило подтверждение; теперь оно получит доступ
                                        к кошельку на Планете и сможет принимать средства.
                                    <br /><strong><i>Согласовано</i></strong> - выставляется по умолчанию при регистрации.
                                    <br /><strong><i>Отклонено</i></strong> - используется для бана сообщества.
                                </div>
                            </div>
                        </div>
                        <div class="form-actions">
                            <button type="submit" class="btn btn-primary">Сохранить</button>
                        </div>
                    </fieldset>
                </form>
            </div>
        </div>



        <div class="row ">
            <div class="span12">
                <c:set value="${groupProfile.alias}" var="aliasName"/>
                <c:set value="${groupProfile.profileId}" var="profileId"/>
                <%@include file="/WEB-INF/jsp/includes/alias-name-setter.jsp" %>
            </div>
        </div>

        <div class="row ">
            <div class="span12">
                <form id="delete-group-form" method="post"
                      action="/admin/delete-group.html">
                    <input type="hidden" name="profileId" value="${groupProfile.profileId}"/>
                    <button type="submit" class="btn btn-primary">Удалить сообщество</button>
                </form>
            </div>
        </div>
        <script type="text/javascript">
            $("#deleteByProfileId-group-form .btn").click(function (e) {
                e.preventDefault();
                if (Modal) {
                    Modal.showConfirm("Вы действительно хотите удалить сообщество?" +
                            " После удаления сообщество будет невозможно восстановить.",
                            "Подтверждение удаления",
                            {
                                success: function () {
                                    $('#deleteByProfileId-group-form').submit();
                                }
                            });
                }
            });
        </script>

    </div>
</body>
</html>

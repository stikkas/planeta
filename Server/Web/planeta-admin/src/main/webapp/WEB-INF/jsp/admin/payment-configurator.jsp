<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ include file="/WEB-INF/jsp/includes/generated/doctype-header.jsp" %>
<head>
    <%@include file="head.jsp" %>
    <link type="text/css" rel="stylesheet" href="//${hf:getStaticBaseUrl("")}/admin-new/bootstrap/css/jquery-ui-1.9.2.custom.css"/>
</head>
<body>

<%@ include file="/WEB-INF/jsp/admin/navbar.jsp" %>

<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Конфигурилка платёжек</h1>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12 admin-table">
            <table class="table table-striped table-hover table-bordered admin-banner-image">
                <thead>
                    <tr>
                        <td style="color: #4392D4;">
                            <strong>ID</strong>
                        </td>

                        <td style="color: #4392D4;">
                            <strong>Проект</strong>
                        </td>

                        <td style="color: #4392D4;">
                            <strong>Платёжный метод</strong>
                        </td>

                        <td style="color: #4392D4;">
                            <strong>Платёжный инструмент</strong>
                        </td>

                        <td style="color: #4392D4;">
                            <strong>Активен</strong>
                        </td>

                        <td style="color: #4392D4;">
                            <strong>Приоритет</strong>
                        </td>

                        <td style="color: #4392D4;">
                            <strong>Действия</strong>
                        </td>
                    </tr>
                </thead>

                <tbody>
                    <c:forEach var="projectPaymentTool" items="${projectPaymentTools}">
                        <tr>
                            <form name="myform" action="/admin/save-project-payment-tool.html" method="POST">
                                <td>
                                    <input type="hidden" name="id" value="${projectPaymentTool.id}"><b>${projectPaymentTool.id}</b>
                                </td>

                                <td>
                                    <input type="hidden" name="projectType" value="${projectPaymentTool.projectType}"><b>${projectPaymentTool.projectType}</b>
                                </td>

                                <td>
                                    <c:forEach var="paymentMethod" items="${paymentMethods}">
                                        <c:if test="${projectPaymentTool.paymentMethodId == paymentMethod.id}">
                                            <input type="hidden" name="paymentMethodId" value="${paymentMethod.id}"><b>${paymentMethod.name}</b>
                                        </c:if>
                                    </c:forEach>
                                </td>

                                <td>
                                    <select id="paymentTools" name="paymentToolId" class="form-control">
                                        <c:forEach var="paymentTool" items="${paymentTools}">
                                            <c:if test="${paymentTool.id == projectPaymentTool.paymentToolId}">
                                                <option value="${paymentTool.id}" selected="selected">${paymentTool.name}</option>
                                            </c:if>
                                            <c:if test="${paymentTool.id != projectPaymentTool.paymentToolId}">
                                                <option value="${paymentTool.id}">${paymentTool.name}</option>
                                            </c:if>
                                        </c:forEach>
                                    </select>
                                </td>

                                <td>
                                    <c:if test="${projectPaymentTool.enabled}">
                                        <input type="checkbox" name="enabled" checked="checked">
                                    </c:if>
                                    <c:if test="${!projectPaymentTool.enabled}">
                                        <input type="checkbox" name="enabled">
                                    </c:if>
                                </td>

                                <td>
                                    <input type="hidden" name="priority" value="${projectPaymentTool.priority}"><b>${projectPaymentTool.priority}</b>
                                </td>

                                <td class="text-right">
                                    <button type="submit" class="btn btn-outline btn-primary" title="Сохранить">
                                        <i class="fa fa-floppy-o"></i>
                                    </button>
                                </td>
                            </form>
                        </tr>
                    </c:forEach>
                </tbody>
            </table>
        </div>
    </div>
</div>

</body>



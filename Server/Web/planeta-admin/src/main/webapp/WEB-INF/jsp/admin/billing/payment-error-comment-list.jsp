<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ include file="/WEB-INF/jsp/includes/generated/doctype-header.jsp" %>

<%@ include file="includes/badge-color.jsp" %>


<head>
    <%@ include file="../head.jsp" %>


</head>
<body>

<%@ include file="/WEB-INF/jsp/admin/navbar.jsp" %>


<%--//TODO новая админка: сделать красиво--%>
<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Комментарии к платежу ${paymentError.transactionId}</h1>
        </div>
    </div>


    <c:if test="${not empty paymentError.manager}">
        <div class="row">
            <div class="col-lg-12">
                <h4>Менеджер:</h4>
                <span>
                        ${paymentError.manager.displayName}
                </span>
            </div>
        </div>
    </c:if>

    <div class="row">
        <div class="col-lg-12">
            <h4>Пользователь:</h4>
            <div>
                <img src="${hf:getUserAvatarUrl(paymentError.profile.smallImageUrl, "USER_SMALL_AVATAR", paymentError.profile.userGender)}"
                     alt="${paymentError.profile.displayName}">
            </div>
            <div class="span4">

                <div><a href="/moderator/user-info.html?profileId=${paymentError.profile.profileId}"
                        target="_blank">${paymentError.profile.profileId}</a></div>
                <div>${paymentError.profile.displayName}</div>
                <div>${paymentError.email}</div>
            </div>
        </div>
    </div>

    <div class="row ">
        <div class="span1">Транзакция:</div>
        <div class="span8">
            <a href="/admin/billing/payment.html?paymentId=${paymentError.transactionId}"
               target="_blank">${paymentError.transactionId}</a>
        </div>
    </div>

    <div class="row ">
        <div class="span1">Заказ:</div>
        <div class="span8">
            <a href="/admin/billing/order.html?orderId=${paymentError.orderId}"
               target="_blank">${paymentError.orderId}</a>
        </div>
    </div>

    <div class="row ">
        <div class="span1">Проект:</div>
        <div class="span8">
            <a href="/moderator/campaign-moderation-info.html?campaignId=${paymentError.campaignId}"
               target="_blank">${paymentError.campaignId}</a>
        </div>
    </div>

    <div class="row ">
        <div class="span1">Сумма:</div>
        <div class="span8">
            ${paymentError.amount}
        </div>
    </div>

    <div class="row ">
        <div class="span1">Время создания</div>
        <div class="span8">
            <fmt:formatDate value="${paymentError.timeAdded}" pattern="dd.MM.yyyy HH:mm:ss"/>
        </div>
    </div>

    <div class="row ">
        <div class="span1">Время изменения</div>
        <div class="span8">
            <fmt:formatDate value="${paymentError.timeUpdated}" pattern="dd.MM.yyyy HH:mm:ss"/>
        </div>
    </div>

    <div class="row ">
        <div class="span1">Тип ошибки:</div>
        <div class="span8">
            <span class="badge ${typeColor[paymentError.paymentErrorType.name]}">${hf:getMessage(paymentError.paymentErrorType.descriptionMessageCode)}</span>
        </div>
    </div>

    <div class="row ">
        <div class="span1">Статус:</div>
        <div class="span8">
            ${hf:getMessage(paymentError.paymentErrorStatus.descriptionMessageCode)}
        </div>
    </div>

    <div class="row ">
        <div class="span1"></div>
        <div class="span8">

        </div>
    </div>

    <div class="row ">
        <div class="span1"></div>
        <div class="span8">

        </div>
    </div>

    <div class="row ">
        <div class="span1"></div>
        <div class="span8">

        </div>
    </div>

    <div class="row ">
        <div class="span1"></div>
        <div class="span8">

        </div>
    </div>


    <br>

    <div class="row ">
        <div class="span12">
            <h4>Добавить комментарий</h4>

            <form action="/admin/billing/payment-error-add-comment.html" method="post">
                <div>
                    <textarea name="text" rows="6" style="width:800px"></textarea>
                    <input name="paymentErrorId" type="hidden" value="${paymentError.paymentErrorId}"></input>
                </div>
                <div>
                    <button type="submit">Добавить</button>
                </div>
            </form>
        </div>
    </div>


    <div class="row ">
        <c:if test="${fn:length(paymentError.commentList) > 0}">
            <table class="table table-hover ">
                <col width="20%">
                <col width="20%">
                <col width="60%">
                <thead>
                <th>Время добавления</th>
                <th>Менеджер</th>
                <th>Текст</th>
                </thead>
                <tbody>
                <c:forEach items="${paymentError.commentList}" var="comment">
                    <tr>
                        <td><fmt:formatDate value="${comment.timeAdded}" pattern="dd.MM.yyyy HH:mm:ss"/></td>
                        <td>
                            <c:if test="${not empty comment.profile}">
                                <a href="/moderator/user-info.html?profileId=${comment.profileId}">${comment.profile.displayName}</a>
                            </c:if>
                        </td>
                        <td>${comment.text}</td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </c:if>
        <c:if test="${fn:length(paymentError.commentList) == 0}">
            <div>Нет комментариев</div>
        </c:if>
    </div>
</div>
</body>

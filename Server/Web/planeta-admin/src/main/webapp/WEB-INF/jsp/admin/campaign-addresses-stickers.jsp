<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ include file="/WEB-INF/jsp/includes/generated/doctype-header.jsp" %>

<head>
    <style>
        *{
            margin: 0;
            font-family: Arial, sans-serif;
            font-size: 18px;
            line-height: 1.4;
            text-align: center;
        }

        table{
            width: 100%;
            max-width: 100%;
            border-spacing: 0;
            border-collapse: collapse;
        }
        td{
            width: 50%;
            height: 5.7cm;
            vertical-align: middle;
            border: 0;
            padding: 0 30px;
        }

        .name{
            font-size: 22px;
            font-weight: 700;
        }
        .address{
            margin: 15px 0 8px;
            /*color: #999;*/
        }
        .index{
            /*color: #999;*/
            font-size: 23px;
            /*font-weight: 700;*/
        }

        .page{
            width: 210mm;
            height: 285mm;
            margin: 4mm 0mm 8mm;
            page-break-before: always;
        }

        @page {
            size: 21cm 29.7cm;
            margin: 0mm;
        }
    </style>
</head>
<body>
        <c:forEach var="orderInfo" items="${ordersInfo}" varStatus="loop">
        <c:if test="${loop.index%10==0}"><table class="page"></c:if>
        <c:if test="${loop.index%2==0}"><tr></c:if>
            <td contenteditable="true">
                <div class="name">${orderInfo.deliveryAddress.fio}</div>
                <div class="address">
                    ${orderInfo.deliveryAddress.country} ${orderInfo.deliveryAddress.city}
                    <br>
                    ${orderInfo.deliveryAddress.address}
                </div>
                <div class="index">${orderInfo.deliveryAddress.zipCode}</div>
            </td>
            <c:if test="${loop.index%2!=0}"></tr></c:if>
            <c:if test="${loop.index%10==9}"></table></c:if>
        </c:forEach>

        <c:forEach var="index" begin="${restCount}" end="${limitPerPage - 1}">
            <c:if test="${index%2==0}"><tr></c:if>
            <td>
                <div class="name"></div>
                <div class="address">
                    <br>
                </div>
                <div class="index"></div>
            </td>
            <c:if test="${index%2!=0}"></tr></c:if>
            <c:if test="${index == limitPerPage - 1}"></table></c:if>
        </c:forEach>

</body>
</html>

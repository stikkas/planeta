<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ include file="/WEB-INF/jsp/includes/generated/doctype-header.jsp" %>

<head>
    <%@ include file="../head.jsp" %>

    <link type="text/css" rel="stylesheet" href="/css/ui-lightness/jquery-ui-1.10.2.custom.min.css"/>
    <link type="text/css" rel="stylesheet" href="/css/jquery.timepicker.css"/>
    <link type="text/css" rel="stylesheet" href="//${hf:getStaticBaseUrl("")}/css/jquery.Jcrop.css">
    <link type="text/css" rel="stylesheet" href="//${hf:getStaticBaseUrl("")}/css/admin.Crop.css">

    <style>
        .col-12 {
            width: 870px;
            padding-left: 20px;
            margin-left: 0px;
            margin-bottom: 10px;
            border: 1px solid grey;
        }

        .col-4 {
            width: 120px
        }

        .col-4.partner {
            width: 200px
        }

        label {
            padding-top: 10px;
        }

        .wrap {
            width: 870px;
            margin: 0 auto;
            padding-top: 10px;
        }

        .wrap-row {
            margin-left: 20px
        }

        .news_feature_i {
            position: relative;
            display: block;
            font: 700 16px/24px Arial, sans-serif;
            font-family: 'ProximaNovaRegular', Arial, sans-serif;
            text-decoration: none;
            outline: none;
            margin-right: 20px;
        }

        .news_feature_i, .news_feature_i:hover {
            color: #fff;
            width: 120px;
        }

        .news_feature_img {
            display: block;
            height: 120px;
            width: 120px;
        }

        .news_feature_head {
            position: absolute;
            height: 100%;
            bottom: 0;
            left: 0;
            right: 0;
            padding: 8px 12px;
            background: rgba(0, 0, 0, 0.6)
        }

        .news_feature_i_p {
            position: relative;
            display: block;
            font: 14px/22px Arial, sans-serif;
            font-family: 'ProximaNovaRegular', Arial, sans-serif;
            text-decoration: none;
            outline: none;
            margin-right: 20px;
        }

        .news_feature_i_p, .news_feature_i_p:hover {
            color: #fff;
            width: 200px;
        }

        .news_feature_img_p {
            display: block;
            height: 80px;
            width: 200px;
        }

        /* ------ Region Autocompleter styles begin ------ */
        .ac_results {
            background: #fff;
            border: 1px solid #ccc;
            -webkit-box-shadow: 0 2px 4px #ccc;
            -moz-box-shadow: 0 2px 4px #ccc;
            box-shadow: 0 2px 4px #ccc;
            padding: 0 4px;
            z-index: 1060;
        }

        .ac_results ul {
            margin: 0 -4px;
        }

        .ac_results ul li {
            padding: 1px 4px;
            cursor: pointer;
        }

        .ac_results ul li.ac_over {
            background: #26aedd;
            color: #fff;
        }

        /* ------ Region Autocompleter styles end ------ */
    </style>

    <script type="text/javascript">
        $(document).ready(function () {
            RegionAutocompleter.autocomplete({
                countrySelect: $('#countryId'),
                cityNameInput: $('#cityName'),
                cityIdInput: $('#cityId'),
                selectedCountryId: '${countryId}'
            });
            $("#cityName").val('${cityNameRus}');

            $("#date-box-start").datetimepicker();
            $('#date-box-start').data("DateTimePicker").date(new Date(${seminar.timeStart.time}));

            $("#date-box-registration-end").datetimepicker();
            $('#date-box-registration-end').data("DateTimePicker").date(new Date(${seminar.timeRegistrationEnd.time}));

            $("#seminar-form").submit(function () {
                var dateStart = $("#date-box-start").data("DateTimePicker").date();
                if (dateStart) {
                    $("#timeStart").val(dateStart.valueOf());
                }

                var dateRegistrationEnd = $("#date-box-registration-end").data("DateTimePicker").date();
                if (dateRegistrationEnd) {
                    $("#timeRegistrationEnd").val(dateRegistrationEnd.valueOf());
                }


                return true;
            });

            $(document).on('click', ".upload-image", function () {
                var container = $(this).parent();
                UploadController.showUploadSeminarBackgroundImage(-1, function (filesUploaded) {
                    if (!filesUploaded || !filesUploaded.length) {
                        return;
                    }
                    var uploadResult = filesUploaded.at(0).get('uploadResult');
                    if (!uploadResult) {
                        return;
                    }
                    container.find('.image-url').val(uploadResult.imageUrl);
                    container.find('.image-id').val(uploadResult.objectId);
                    container.find('.image-img').attr('src', uploadResult.imageUrl);
                });
            });

            $(document).on('click', '.toggler', function () {
                var $this = $(this);
                var targetSelector = $this.data('targetSelector');
                var $target = $(targetSelector);
                if ($this.prop('checked')) {
                    $target.removeClass('hidden');
                } else {
                    $target.addClass('hidden');
                }
            });

            var model = new Communicate.Models.Main({
                initialSpeakers: new BaseCollection(${hf:toJson(speakers)}),
                initialPartners: new BaseCollection(${hf:toJson(partners)})
            });
            var view = new Communicate.Views.Page({
                model: model
            });
            view.render();
        });
    </script>

</head>
<body>

<%@ include file="/WEB-INF/jsp/admin/navbar.jsp" %>

<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                <c:if test="${seminar.seminarId != null && seminar.seminarId > 0}">Настройки занятия</c:if>
                <c:if test="${seminar.seminarId == null || seminar.seminarId == 0}">Cоздание нового занятия</c:if>
            </h1>
        </div>
    </div>

    <c:if test="${seminar.seminarId > 0}">
        <div class="main-page-actions">
            <a class="btn btn-warning" href="https://${properties['school.application.host']}/school/${seminar.seminarId}" target="_blank">
                Страница занятия
            </a>
        </div>
    </c:if>

    <div class="row">
        <div class="col-lg-12">
            <form:form id="seminar-form" method="post" commandName="seminar">
                <form:input type="hidden" path="seminarId"/>
                <form:input type="hidden" path="timeAdded" value="${seminar.timeAdded.time}"/>
                <form:input type="hidden" path="isShownOnMainPage" value="${seminar.isShownOnMainPage}"/>

                <div class="row ma-b-20">
                    <div class="col-lg-4">
                        <label>Название</label>
                        <form:input type="text" path="name" cssClass="form-control"/>
                        <form:errors path="name"><span class="help-inline" style="color: red;"><form:errors
                                path="name"/></span></form:errors>
                    </div>

                    <div class="col-lg-4">
                        <label>Страна</label>
                        <select path="countryId" class="form-control" id="countryId"></select>
                    </div>

                    <div class="col-lg-4">
                        <label>Город</label>
                        <form:input type="hidden" path="cityId" cssClass="form-control" id="cityId"/>
                        <input type="text" id="cityName" class="form-control"/>
                        <form:errors path="cityId"><span class="help-inline"><form:errors path="cityId"/></span></form:errors>
                    </div>
                </div>

                <div class="row ma-b-20">
                    <div class="col-lg-4">
                        <label>Класс занятия</label>
                        <form:select path="seminarType" cssClass="form-control">
                            <option value="SOLO"
                                    <c:if test="${seminar.seminarType == \"SOLO\"}">selected="selected"</c:if>>
                                Одиночное занятие
                            </option>
                            <option value="SOLOSIMPLE"
                                    <c:if test="${seminar.seminarType == \"SOLOSIMPLE\"}">selected="selected"</c:if>>
                                Одиночное занятие (упрощенное)
                            </option>
                            <option value="PAIR"
                                    <c:if test="${seminar.seminarType == \"PAIR\"}">selected="selected"</c:if>>
                                Парное занятие
                            </option>
                            <option value="WEBINAR"
                                    <c:if test="${seminar.seminarType == \"WEBINAR\"}">selected="selected"</c:if>>
                                Вебинар
                            </option>
                            <option value="COMPANY"
                                    <c:if test="${seminar.seminarType == \"COMPANY\"}">selected="selected"</c:if>>
                                Занятие для организации
                            </option>
                            <option value="EXTERNAL"
                                    <c:if test="${seminar.seminarType == \"EXTERNAL\"}">selected="selected"</c:if>>
                                Внешнее занятие
                            </option>
                        </form:select>
                    </div>

                    <div class="col-lg-4">
                        <label>Тематика занятия</label>
                        <form:select path="tagId" cssClass="form-control">
                            <c:forEach items="${campaignTags}" var="campaignTag">
                                <option
                                        <c:if test="${campaignTag.id == seminar.tagId}">selected</c:if>
                                        value="${campaignTag.id}">${campaignTag.name}
                                </option>
                            </c:forEach>
                        </form:select>
                    </div>

                    <div class="col-lg-4">
                        <label>Время начала</label>
                        <form:input id="timeStart" type="hidden" path="timeStart" cssClass="form-control"/>
                        <input id="date-box-start" class="form-control" value='' type="text"/>
                        <form:errors path="timeStart" cssClass="error-inline"/>
                    </div>
                </div>

                <div class="row ma-b-20">
                    <div class="col-lg-3">
                        <label>Время окончания приема заявок</label>
                        <form:input id="timeRegistrationEnd" type="hidden" path="timeRegistrationEnd" cssClass="form-control"/>
                        <input id="date-box-registration-end" class="form-control" value='' type="text"/>
                        <form:errors path="timeRegistrationEnd" cssClass="error-inline"/>
                    </div>

                    <div class="col-lg-3">
                        <label>Url</label>
                        <form:input type="text" path="url" cssClass="form-control"/>
                        <span class="help-inline" style="color: red;"><form:errors path="url"/></span>
                    </div>

                    <div class="col-lg-3">
                        <label>Место проведения</label>
                        <form:input type="text" path="venue" cssClass="form-control"/>
                        <span class="help-inline" style="color: red;"><form:errors path="venue"/></span>
                    </div>

                    <div class="col-lg-3">
                        <label>Ограничение на количество участников</label>
                        <form:input path="participantsLimit" min="0" type="number" cssClass="form-control"/>
                        <span class="help-inline" style="color: red;"><form:errors path="participantsLimit"/></span>
                    </div>
                </div>

                <div class="row ma-b-20">
                    <div class="col-lg-12">
                        <label>Короткое описание</label>
                        <form:textarea path="shortDescription" cssClass="form-control" rows="5"/>
                        <span class="help-inline" style="color: red;"><form:errors path="shortDescription"/></span>
                    </div>
                </div>

                <div class="row ma-b-20">
                    <div class="col-lg-12">
                        <label>Длинное описание</label>
                        <form:textarea path="description" cssClass="form-control" rows="5"/>
                        <span class="help-inline" style="color: red;"><form:errors path="description"/></span>
                    </div>
                </div>

                <div class="row ma-b-20">
                    <div class="col-lg-12">
                        <label>Условия обучения</label>
                        <form:textarea path="conditions" cssClass="form-control" rows="5"/>
                        <span class="help-inline" style="color: red;"><form:errors path="conditions"/></span>
                    </div>
                </div>


                <div class="row ma-b-20">
                    <div class="col-lg-12">
                        <label>Результаты обучения</label>
                        <form:textarea path="results" cssClass="form-control" rows="5"/>
                        <span class="help-inline" style="color: red;"><form:errors path="results"/></span>
                    </div>
                </div>

                <div class="row ma-b-20">
                    <div class="col-lg-12 checkbox">
                        <label>
                            <form:checkbox path="formatSelection"/>
                            Разрешить выбирать формат обучения (online/offline)
                        </label>
                    </div>
                </div>


                <div class="row ma-b-20">
                    <div id="items-container" class="col-lg-12"></div>
                </div>

                <form:input path="speakersIds" type="hidden"/>
                <form:errors path="speakersIds"><span class="help-inline" style="color: red;"><form:errors path="speakersIds"/></span></form:errors>

                <form:input path="partnersIds" type="hidden"/>
                <form:errors path="partnersIds"><span class="help-inline" style="color: red;"><form:errors path="partnersIds"/></span></form:errors>


                <div class="row ma-b-20">
                    <div class="col-lg-12">
                        <ct:file url="${seminar.backgroundImage}" path="backgroundImage" type="IMAGE" label="Фоновая картинка"/>
                    </div>
                </div>

                <div class="btn-group">
                    <button type="submit" class="btn btn-primary">
                        Сохранить
                    </button>
                    <a href="/admin/seminars.html" class="btn btn-default">Назад</a>
                </div>

            </form:form>
        </div>
    </div>

    <script id="main-container-template" type="text/x-jquery-template">
        <div id="js-speakers" class="panel panel-primary">
            <div class="panel-heading">Спикеры</div>
            <div class="panel-body"></div>
            <div class="panel-footer">
                <button class="btn btn-success" data-event-click="onAddSpeaker">
                    <i class="fa fa-plus"></i> Добавить спикера
                </button>
            </div>
        </div>

        <div id="js-partners" class="panel panel-primary">
            <div class="panel-heading">Партнеры</div>
            <div class="panel-body"></div>
            <div class="panel-footer">
                <button class="btn btn-success" data-event-click="onAddPartner">
                    <i class="fa fa-plus"></i> Добавить партнёра
                </button>
            </div>
        </div>
    </script>

    <script id="speaker-template" type="text/x-jquery-template">
        <a class="news_feature_i" href="javascript:void(0);">
            <img class="news_feature_img" src="{{= photoUrl}}">
            <span class="news_feature_head">{{= name}}</span>
        </a>
    </script>

    <script id="partner-template" type="text/x-jquery-template">
        <a class="news_feature_i_p" href="javascript:void(0);">
            <img class="news_feature_img_p" src="{{= logoUrl}}">
            <span class="news_feature_head">{{= siteUrl}}</span>
        </a>
    </script>

    <script id="speaker-modal-editor-template" type="text/x-jquery-template">
        <div class="modal-header">
            <a type="button" class="close" data-event-click="cancel">×</a>
            <h4>{{if speakerId}}Редактирование спикера{{else}}Создание спикера{{/if}}</h4>
        </div>

        <div class="modal-body">
            <form>
                <div class="row ma-b-20">
                    <div class="col-lg-12">
                        <label for="title">Имя спикера</label>
                        <input id="title" name="name" type="text" class="form-control" value="{{= name}}">
                    </div>
                </div>

                <div class="row ma-b-20">
                    <div class="col-lg-12">
                        <label for="description">Описание</label>
                        <textarea id="description" name="description" class="form-control" rows="5">{{= description}}</textarea>
                    </div>
                </div>

                <div class="row js-image-field"></div>
            </form>
        </div>

        <div class="modal-footer">
            <div class="btn-group">
                <button class="btn btn-default" data-event-click="cancel">Отменить</button>
                <button class="btn btn-primary" data-event-click="save">Сохранить</button>
            </div>
        </div>
    </script>

    <script id="partner-modal-editor-template" type="text/x-jquery-template">
        <div class="modal-header">
            <a type="button" class="close" data-event-click="cancel">×</a>
            <h4>{{if partnerId}}Редактирование партнера{{else}}Создание партнера{{/if}}</h4>
        </div>
        <div class="modal-body">
            <form>
                <div class="row ma-b-20">
                    <div class="col-lg-12">
                        <label for="title">URL</label>
                        <input id="title" name="siteUrl" type="text" placeholder="адрес сайта" class="form-control"
                               value="{{= siteUrl}}">
                    </div>
                </div>

                <div class="row">
                    <div class="js-image-field"></div>
                </div>
            </form>
        </div>
        <div class="modal-footer">
            <div class="btn-group">
                <button class="btn btn-default" data-event-click="cancel">Отменить</button>
                <button class="btn btn-primary" data-event-click="save">Сохранить</button>
            </div>
        </div>
    </script>

    <script id="item-actions-popover-template" type="text/x-jquery-template">
        <div class="arrow"></div>
        <div class="popover-inner">
            <h3 class="popover-title">Выберите действие</h3>
            <div class="popover-content">
                <div class="btn-group">
                    <a class="btn btn-primary js-edit">
                        <i class="fa fa-pencil" ttile="Редактирование"></i>
                    </a>
                    <a class="btn btn-danger js-delete">
                        <i class="fa fa-trash" title="Удаление"></i>
                    </a>
                </div>
            </div>
        </div>
    </script>

    <script id="communicate-view-image-template" type="text/x-jquery-template">
        <div class="col-lg-12">
            <label>Изображение</label>
            <div class="js-val"></div>
        </div>
    </script>

    <script id="communicate-view-image-load-view-template" type="text/x-jquery-template">
        <div class="js-progress"></div>
        <div class="js-drag-zone">
            <div class="btn btn-success" style=" position: relative; overflow: hidden;">
                Загрузить фото
                <input type="file" name="files[]" class="js-input-file"
                       style=" position: absolute; top: 0; right: 0; margin: 0; opacity: 0; font-size: 200px;">
            </div>
            <div class="btn btn-info js-link-to-image">Фото по ссылке</div>
        </div>
    </script>

    <script id="communicate-view-image-crop-view-template" type="text/x-jquery-template">
        <img src="{{if $data.thumbnail}}{{= ImageUtils.getThumbnailUrl(imageUrl, thumbnail.imageConfig, thumbnail.ImageType)}}{{else}}{{= imageUrl}}{{/if}}"
             {{if width}}style="width: {{= width}}px" {{/if}}>

        <div class="btn-group">
            <span class="btn btn-success" data-event-click="onCropClick" title="Обрезать">
                <i class="fa fa-crop"></i>
            </span>
            <span class="btn btn-danger" data-event-click="onRemoveClick" title="Удалить">
                <i class="fa fa-trash"></i>
            </span>
        </div>
    </script>

    <script id="communicate-load-image-field-progress-template" type="text/x-jquery-template">
        <div>
            {{if status !== FileUploadStatuses.SAVED}}
            Загрузка {{= Math.floor(uploadedSize / fileSize * 100)}}%
            {{else}}
            Обработка файла, подождите.
            {{/if}}
        </div>
        {{if status !== FileUploadStatuses.SAVED}}
        <div class="btn js-cancel">Отмена</div>
        {{/if}}
    </script>

    <script id="alert-template" type="text/x-jquery-template">
        <div class="alert fade in {{= className}}" style="z-index: 1100; position: fixed; top: 50px; right: 20px;">
            <button type="button" class="close" data-dismiss="alert">×</button>
            {{= message}}
        </div>
    </script>


    <%-- JS section --%>

    <script type="application/javascript">
        var Messenger = {
            showSuccess: function (message) {
                Messenger.showMessage(message, 'alert-success');
            },

            showError: function (message) {
                Messenger.showMessage(message, 'alert-error');
            },

            showMessage: function (message, className, delay) {
                var alert = $('#alert-template').tmpl({
                    className: className,
                    message: message
                });

                alert.appendTo('#container');
                _.delay(function () {
                    alert.remove();
                }, delay || 5000);
            }
        };

        var Communicate = {
            Models: {
                sync: function (url, data) {
                    var $dfd = $.Deferred();
                    $.ajax(url, _.extend({
                        type: 'POST',
                        data: JSON.stringify(data),
                        dataType: 'json',
                        contentType: 'application/json; charset=utf-8',
                        success: function (response) {
                            if (response.success) {
                                $dfd.resolve(response.result);
                            } else {
                                $dfd.reject(response.errorMessage, response.fieldErrors);
                                Messenger.showError('Ошибка сохранения данных');
                            }
                        }
                    }));

                    return $dfd.promise();
                }
            },
            Views: {}
        };

        Communicate.Models.Main = BaseModel.extend({

            initialize: function () {
                this.set(this.parseInitialItems());
            },

            parseInitialItems: function () {
                var result = {
                    speakers: new BaseCollection([]),
                    partners: new BaseCollection([])
                };

                var self = this;
                this.get('initialSpeakers').each(function (speaker) {
                    var cloned = speaker.clone();
                    cloned.on('change', self.triggerConfigChanged, self);
                    result.speakers.add(cloned);

                });
                this.get('initialPartners').each(function (partner) {
                    var cloned = partner.clone();
                    cloned.on('change', self.triggerConfigChanged, self);
                    result.partners.add(cloned);
                });

                result.speakers.on('add remove reset', this.triggerConfigChanged, this);
                result.partners.on('add remove reset', this.triggerConfigChanged, this);

                return result;
            },

            triggerConfigChanged: function () {
                this.trigger('configChanged');

                var speakerIds = [];
                _.each(this.get('speakers').models || [], function (speaker) {
                    speakerIds.push(speaker.get('speakerId'))
                });
                $('input[name="speakersIds"]').val(speakerIds);

                var partnerIds = [];
                _.each(this.get('partners').models || [], function (partner) {
                    partnerIds.push(partner.get('partnerId'))
                });
                $('input[name="partnersIds"]').val(partnerIds);
            },

            addItem: function (isSpeaker, isPartner) {
                var self = this;
                if (isSpeaker) {
                    new Communicate.Views.SpeakerModalEditor({
                        model: new BaseModel({
                            speaker: isSpeaker,
                            partner: isPartner,
                            name: "",
                            description: "",
                            logoUrl: "",
                            photoUrl: ""
                        }),
                        callback: {
                            save: function (model) {
                                $.ajax("/admin/speaker/save.json", _.extend({
                                    type: 'POST',
                                    data: JSON.stringify(model),
                                    dataType: 'json',
                                    contentType: 'application/json; charset=utf-8'
                                })).done(function (resp) {
                                    if (resp.success) {
                                        if (resp.result) {
                                            var itemType = 'speakers';
                                            if (resp.result.speakerId) {
                                                model.set("speakerId", resp.result.speakerId, {silent: true});
                                                self.get(itemType).add(model);
                                                Messenger.showSuccess('Спикер добавлен успешно!');
                                            }
                                        } else {
                                            Messenger.showError('При добавлении спикера не вернулся его ID!');
                                        }
                                    } else {
                                        Messenger.showError('Ошибка при добавлении спикера!');
                                    }
                                });
                            }
                        }
                    }).render();
                } else if (isPartner) {
                    new Communicate.Views.PartnerModalEditor({
                        model: new BaseModel({
                            speaker: isSpeaker,
                            partner: isPartner
                        }),
                        callback: {
                            save: function (model) {
                                $.ajax("/admin/partner/save.json", _.extend({
                                    type: 'POST',
                                    data: JSON.stringify(model),
                                    dataType: 'json',
                                    contentType: 'application/json; charset=utf-8'
                                })).done(function (resp) {
                                    if (resp.success) {
                                        if (resp.result) {
                                            var itemType = 'partners';
                                            if (resp.result.partnerId) {
                                                model.set("partnerId", resp.result.partnerId, {silent: true});
                                                self.get(itemType).add(model);
                                                Messenger.showSuccess('Партнер добавлен успешно!');
                                            }
                                        } else {
                                            Messenger.showError('При добавлении партнера не вернулся его ID!');
                                        }
                                    } else {
                                        Messenger.showError('Ошибка при добавлении партнера!');
                                    }
                                });
                            }
                        }
                    }).render();
                }
            },

            removeItem: function (item) {
                item.destroy();
            }
        });

        Communicate.Views.Item = BaseView.extend({
            viewEvents: {
                'click': 'onItemClicked'
            },

            afterRender: function () {
            }
        });

        Communicate.Views.Speakers = BaseListView.extend({
            itemViewType: Communicate.Views.Item.extend({
                template: '#speaker-template'
            }),
            className: 'speakers sortable'
        });

        Communicate.Views.Partners = BaseListView.extend({
            itemViewType: Communicate.Views.Item.extend({
                template: '#partner-template',
                className: 'col-4 partner'
            }),
            className: 'speakers sortable'
        });

        Communicate.Views.ItemActionsPopover = BaseView.extend({
            template: '#item-actions-popover-template',
            className: 'popover fade right in hide',
            viewEvents: {
                'click .js-edit': 'onEdit',
                'click .js-delete': 'onDelete'
            },

            showAt: function (x, y) {
                this.$el.show().css({
                    top: (y - (this.$el.height() / 2) - 10),
                    left: (x + 10)
                });
            },

            hide: function () {
                this.$el.hide();
            },

            execute: function (handler) {
                handler();
                this.hide();
            }
        });

        Communicate.Views.Page = BaseView.extend({
            el: '#items-container',
            template: '#main-container-template',

            construct: function () {
                this.actionsView = this.addChild(new Communicate.Views.ItemActionsPopover());

                this.addChildAtElement('#js-speakers .panel-body', new Communicate.Views.Speakers({
                    collection: this.model.get('speakers')
                }));

                this.addChildAtElement('#js-partners .panel-body', new Communicate.Views.Partners({
                    collection: this.model.get('partners')
                }));

                this.bind('onItemClicked', this.onItemClicked, this);
                this.bind('onEdit', this.onEdit, this);
                this.bind('onDelete', this.onDelete, this);

                this.model.on('configChanged', this.onConfigChanged, this);
            },

            onItemClicked: function (view, e) {
                var actions = this.actionsView;
                var isSameModel = actions.model && (actions.model.cid == view.model.cid);
                if (isSameModel && actions.$el.is(':visible')) {
                    actions.hide();
                } else {
                    actions.model = view.model;
                    actions.showAt(e.pageX, e.pageY);
                }
            },

            onAddSpeaker: function (e) {
                e.preventDefault();
                this.model.addItem(true, false);
            },

            onAddPartner: function (e) {
                e.preventDefault();
                this.model.addItem(false, true);
            },

            onDelete: function (view) {
                var model = this.model;
                this.actionsView.execute(function () {
                    model.removeItem(view.model);
                });
            },

            onEdit: function (view) {
                this.actionsView.execute(function () {
                    if (view.model.get('speakerId')) {
                        new Communicate.Views.SpeakerModalEditor({
                            model: view.model.clone(),
                            callback: {
                                save: function (model) {
                                    $.ajax("/admin/speaker/save.json", _.extend({
                                        type: 'POST',
                                        data: JSON.stringify(model),
                                        dataType: 'json',
                                        contentType: 'application/json; charset=utf-8'
                                    })).done(function (resp) {
                                        if (resp.success) {
                                            view.model.set(model.toJSON());
                                            Messenger.showSuccess('Спикер обновлен успешно!');
                                        } else {
                                            Messenger.showError('При обновлении спикера произошла ошибка!');
                                        }
                                    });
                                }
                            }
                        }).render();
                    } else if (view.model.get('partnerId')) {
                        new Communicate.Views.PartnerModalEditor({
                            model: view.model.clone(),
                            callback: {
                                save: function (model) {
                                    $.ajax("/admin/partner/save.json", _.extend({
                                        type: 'POST',
                                        data: JSON.stringify(model),
                                        dataType: 'json',
                                        contentType: 'application/json; charset=utf-8'
                                    })).done(function (resp) {
                                        if (resp.success) {
                                            view.model.set(model.toJSON());
                                            Messenger.showSuccess('Партнер обновлен успешно!');
                                        } else {
                                            Messenger.showError('При обновлении партнера произошла ошибка!');
                                        }
                                    });
                                }
                            }
                        }).render();
                    }
                });
            },

            onConfigChanged: function () {
                this.updateAddButtons();
            },

            afterRender: function () {
                var self = this;
                this.updateAddButtons();
                this.$el.find('.sortable').sortable({
                    items: '.col-4',
                    cancel: '.add-speaker'
                }).on('sortupdate', function (e, el) {
                    var $el = $(el.item);
                    self.onConfigChanged();
                });
            },

            updateAddButtons: function () {
                var self = this;
                this.$el.find('.add-speaker').each(function () {
                    var $el = $(this);
                    $el.toggle(true);
                });
            }
        });

        Communicate.Views.SpeakerModalEditor = Modal.OverlappedView.extend({
            hideWhenOverlapped: true,
            template: '#speaker-modal-editor-template',
            events: {
                'change input[name]': 'onControlChanged',
                'keyup input[name]': 'onControlChanged',
                'drop input[name]': 'onControlChanged',
                'change textarea[name]': 'onControlChanged',
                'keyup textarea[name]': 'onControlChanged',
                'drop textarea[name]': 'onControlChanged'
            },

            construct: function (options) {
                Modal.OverlappedView.prototype.construct.apply(this, arguments);
                this.callbacks = (options && options.callback) || {};

                var width = 120;
                var height = 120;
                this.cardImageModel = new Communicate.Models.ImageField({
                    originalImage: {
                        imageUrl: this.model.get('photoUrl')
                    },
                    aspectRatio: width / height,
                    width: width
                });

                this.addChildAtElement('.js-image-field', new Communicate.Views.ImageField({
                    model: this.cardImageModel,
                    controller: this.model
                }));
            },

            callback: function (callbackName) {
                var callback = this.callbacks[callbackName];
                if (_.isFunction(callback)) {
                    callback(this.model, this);
                }
            },

            onControlChanged: _.debounce(function (e) {
                var $el = $(e.currentTarget);
                var name = $el.attr('name');
                var value = $el.val();

                this.model.set(name, value.trim(), {silent: true});
                this.updateSaveButton();
            }, 700),

            afterRender: function () {
                Modal.OverlappedView.prototype.afterRender.apply(this, arguments);
                this.updateSaveButton();
            },

            updateSaveButton: function () {
                var photoUrl = this.model.get('photoUrl');
                var name = this.model.get('name');
                var description = this.model.get('description');

                var disabled = _.isEmpty(photoUrl) || _.isEmpty(name);
                this.$el.find('[data-event-click="save"]').prop('disabled', disabled);
            },

            save: function () {
                this.callback('save');
                this.dispose();
            },

            cancel: function () {
                this.callback('cancel');
                Modal.OverlappedView.prototype.cancel.apply(this, arguments);
            }
        });

        Communicate.Views.PartnerModalEditor = Communicate.Views.SpeakerModalEditor.extend({
            template: '#partner-modal-editor-template',

            construct: function (options) {
                Modal.OverlappedView.prototype.construct.apply(this, arguments);
                this.callbacks = (options && options.callback) || {};

                var width = 144;
                var height = 144;
                this.cardImageModel = new Communicate.Models.PartnerImageField({
                    originalImage: {
                        imageUrl: this.model.get('logoUrl')
                    },
                    width: width,
                    height: height
                });

                this.addChildAtElement('.js-image-field', new Communicate.Views.ImageField({
                    model: this.cardImageModel,
                    controller: this.model
                }));
            },

            updateSaveButton: function () {
                var logoUrl = this.model.get('logoUrl');
                var siteUrl = this.model.get('siteUrl');

                var disabled = _.isEmpty(logoUrl) || _.isEmpty(siteUrl);
                this.$el.find('[data-event-click="save"]').prop('disabled', disabled);
            }
        });

        Communicate.Views.ImageField = CrowdFund.Views.ImageField.extend({
            template: '#communicate-view-image-template',
            createLoadView: function () {
                return new Communicate.Views.ImageField.LoadView({model: this.model});
            },
            createCropView: function () {
                return new Communicate.Views.ImageField.CropView({model: this.model});
            }
        });

        Communicate.Models.ImageField = CrowdFund.Models.ImageField.extend({
            defaults: _.defaults({
                albumTypeId: AlbumTypes.ALBUM_COMMENT,
                thumbnail: {
                    imageConfig: ImageUtils.MEDIUM,
                    imageType: ImageType.PHOTO
                }
            }, CrowdFund.Models.ImageField.prototype.defaults),
            initialize: function () {
                CrowdFund.Models.ImageField.prototype.initialize.apply(this, arguments);
                this.set('profileId', workspace.appModel.get('profileModel').get('profileId'));
            },
            onImageUrlChange: function (model) {
                model.set('photoUrl', this.get('imageUrl'));
            }
        });

        Communicate.Models.PartnerImageField = Communicate.Models.ImageField.extend({
            onImageUrlChange: function (model) {
                model.set('logoUrl', this.get('imageUrl'));
            }
        });

        Communicate.Views.ImageField.LoadView = CrowdFund.Views.ImageField.LoadView.extend({
            template: '#communicate-view-image-load-view-template',
            createProgressView: function (fileUploadModel, fileUploader) {
                return new Communicate.Views.ImageField.LoadView.Progress({
                    model: fileUploadModel,
                    fileUploader: fileUploader
                });
            }
        });

        Communicate.Views.ImageField.LoadView.Progress = CrowdFund.Views.ImageField.LoadView.Progress.extend({
            template: '#communicate-load-image-field-progress-template'
        });

        Communicate.Views.ImageField.CropView = CrowdFund.Views.ImageField.CropView.extend({
            template: '#communicate-view-image-crop-view-template'
        });
    </script>

</div>
</body>
</html>


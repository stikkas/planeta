<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags/form" %>
<%@ include file="/WEB-INF/jsp/includes/generated/doctype-header.jsp" %>
<head>
    <%@include file="../head.jsp" %>
    <script>
        $(document).ready(function () {
            $("#imageUploader .upload-image").click(function () {
                UploadController.showUploadWelcomePromoImage(workspace.appModel.get('myProfile').get('profileId'), function (filesUploaded) {
                    if (!filesUploaded || !filesUploaded.length) {
                        return;
                    }
                    var uploadResult = filesUploaded.at(0).get('uploadResult');
                    if (!uploadResult) {
                        return;
                    }
                    var src = uploadResult.imageUrl;
                    var image = new Image();
                    image.src = src;
                    $(image).load(function () {
                        if (image.width >= 5 && image.height >= 5) {
                            $("#imageUrl").val(src);
                            $("#imageUploader").find(".error-message").addClass("hide");
                        } else {
                            $("#imageUrl").val("");
                            $("#imageUploader").find(".error-message").removeClass("hide");
                        }
                    });
                }, "Размер не менее 5x5.");
            });
        });

    </script>
</head>
<body>

<%@ include file="/WEB-INF/jsp/admin/navbar.jsp" %>

<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Партнеры</h1>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <s:form class="form-horizontal" commandName="configuration" method="post">
                <s:input path="id" type="hidden"/>

                <div class="row ma-b-20">
                    <div class="col-lg-6">
                        <ct:file url="${configuration.imageUrl}" path="imageUrl" type="IMAGE"/>
                    </div>
                </div>

                <div class="row ma-b-20">
                    <div class="col-lg-6">
                        <label>Партнер</label>
                        <s:input path="name" type="text" placeholder="" class="form-control" required=""/>
                        <s:errors path="name" cssClass="error-message" element="span"/>
                    </div>

                    <div class="col-lg-6">
                        <label>Ссылка</label>
                        <s:input path="originalUrl" type="text" placeholder="" class="form-control" required=""/>
                        <s:errors path="originalUrl" cssClass="error-message" element="span"/>
                    </div>
                </div>

                <div class="row ma-b-20">
                    <div class="col-lg-12">
                        <s:checkbox path="welcome" required=""/>
                        <label>Партнер на главной</label>
                        <p class="help-block">Будет ли показываться на главной в разделе "Партнеры"</p>
                        <s:errors path="welcome" cssClass="error-message" element="span"/>
                    </div>
                </div>

                <div class="row ma-b-20">
                    <div class="col-lg-12">
                        <s:checkbox path="curator" required=""/>
                        <label>Куратор на главной</label>
                        <p class="help-block">Будет ли выводиться на главной в разделе "Кураторы"</p>
                        <s:errors path="curator" cssClass="error-message" element="span"/>
                    </div>
                </div>

                <div class="btn-group">
                    <button formaction="/moderator/partners-promo-edit.html" class="btn btn-primary">
                        Сохранить
                    </button>
                    <a href="/moderator/partners-promo-list.html" class="btn btn-default">Отмена</a>
                </div>

            </s:form>
        </div>
    </div>
</div>
</body>

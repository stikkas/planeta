<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ include file="/WEB-INF/jsp/includes/generated/doctype-header.jsp" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<head>
	<%@ include file="head.jsp" %>

</head>
<body>
<%@ include file="/WEB-INF/jsp/admin/navbar.jsp" %>
<div id="page-wrapper">
	<div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Настройки трансляции</h1>
        </div>
	</div>

	<div class="row">
		<div class="span7">
            <form:form id="broadcast-form" method="post" commandName="broadcastPrivateTargeting" class="form-horizontal">
                <form:input type="hidden" path="broadcastId"/>
                <form:input type="hidden" path="profileId"/>

                    <label>Emails</label>
                    <form:textarea path="email" cssClass="input-xxlarge" rows="3"/> <form:errors
                        path="email"><span
                        class="help-inline"><form:errors path="email"/></span></form:errors>

                    <label>Время действия ссылки(в часах)</label>
                    <form:input id="validInHours" type="text" path="validInHours" cssClass="input-xxlarge"/>
                    <form:errors path="validInHours"><span class="help-inline"><form:errors path="validInHours"/></span></form:errors>

                    <hr>
                    <button type="submit" class="btn btn-primary">Сохранить</button>
            </form:form>
	    </div>

    </div>
</div>
</body>
</html>


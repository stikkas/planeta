<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ include file="/WEB-INF/jsp/includes/generated/doctype-header.jsp" %>
<head>
    <%@ include file="head.jsp" %>
</head>

<body>
    <%@ include file="/WEB-INF/jsp/admin/navbar.jsp" %>

    <div id="page-wrapper">

        <div class="row ">
            <div class="col-lg-12">
                <h1 class="page-header">Профили <small>Поиск профиля пользователя/сообщества</small></h1>
            </div>
        </div>

        <div class="row ">
            <div class="col-lg-12">
                <jsp:include page="/WEB-INF/jsp/admin/searchBoxUsers.jsp">
                    <jsp:param name="action" value="/moderator/users.html" />
                </jsp:include>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12 admin-table">
                <table class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th width="50">ID</th>
                        <th width="50">Фото</th>
                        <th>Имя, ник, фамилия</th>
                        <th width="300">
                            <div class="text-right">Действия</div>
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:forEach var="profile" items="${profiles}">
                        <c:set var="isUser" value="${profile.profileType == 'USER'}" />
                        <c:set var="isGroup" value="${profile.profileType == 'GROUP'}" />
                        <tr>
                            <td>${profile.profileId}</td>
                            <td>
                                    <img src="${hf:getGroupAvatarUrl(profile.imageUrl, "USER_SMALL_AVATAR")}"
                                         alt='<c:out value="${profile.displayName}"/>'/>
                            </td>
                            <td>
                                <c:if test="${isUser}">
                                    Пользователь:
                                    <a href="${mainAppUrl}/${profile.profileId}">
                                        <c:out value="${profile.displayName}"/>
                                    </a>
                                </c:if>
                                <c:if test="${isGroup}">
                                    Сообщество:
                                    <a href="${mainAppUrl}/${profile.profileId}">
                                        <c:out value="${profile.displayName}"/>
                                    </a>|
                                </c:if>
                                <br>
                                <c:out value="${profile.email}"/>
                            </td>
                            <td>
                                <div class="text-right">
                                    <div class="btn-group">
                                        <c:if test="${isUser}">
                                            <a class="btn btn-primary btn-outline" href="/moderator/user-info.html?profileId=${profile.profileId}" title="Редактирование профиля">
                                                <i class="fa fa-pencil"></i>
                                            </a>
                                            <a class="btn btn-success btn-outline" href="/admin/login-as-user.html?profileId=${profile.profileId}" title="Зайти под юзером">
                                                <i class="fa fa-user-secret"></i>
                                            </a>
                                        </c:if>
                                        <c:if test="${isGroup}">
                                            <a class="btn btn-small" href="/moderator/group-info.html?profileId=${profile.profileId}">
                                                Редактировать сообщество
                                            </a>
                                        </c:if>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
            </div>
        </div>

        <%@ include file="paginator.jsp" %>
    </div>
</body>
</html>


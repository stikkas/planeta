<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ include file="/WEB-INF/jsp/includes/generated/doctype-header.jsp" %>
<head>
    <%@ include file="/WEB-INF/jsp/admin/head.jsp" %>
</head>

<body>

<%@ include file="/WEB-INF/jsp/admin/navbar.jsp" %>

<div id="page-wrapper">

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Добавление города</h1>
        </div>
    </div>

    <form action="/admin/geo/add-new-city.html" method="post">
        <div class="row ma-b-20">
            <div class="col-lg-6">
                <label for="nameRu">Название населенного пункта:</label>
                <input id="nameRu" name="nameRu" type="text" class="form-control" value="${nameRu}">
            </div>

            <div class="col-lg-6">
                <label for="nameRu">Регион:</label>
                <select id="regionId" name="regionId" class="form-control">
                    <c:if test="${regionId == 0}">
                        <option value="0" selected>- выберите регион -</option>
                    </c:if>
                    <c:forEach var="region" items="${regions}">
                        <option value="${region.locationId}"
                                <c:if test="${region.locationId == regionId}">selected</c:if>>${region.name}</option>
                    </c:forEach>
                </select>
            </div>
        </div>

        <div class="btn-group ma-b-20">
            <button type="submit" class="btn btn-success" formmethod="GET">Найти похожий город в регионе
            </button>
            <c:if test="${regionId > 0}">
                <button type="submit" class="btn btn-danger" formmethod="post">Добавить населенный пункт к региону
                </button>
            </c:if>
        </div>
    </form>

    <c:if test="${not empty cities}">
        <div class="row">
            <label class="col-lg-3">Похожие города:</label>
        </div>
        <div class="row">
            <ul class="col-lg-5">
                <c:forEach var="city" items="${cities}">
                    <li class="span5">${city.name}</li>
                </c:forEach>
            </ul>
        </div>
    </c:if>

</div>
</body>
</html>


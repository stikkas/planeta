<%@ page contentType="text/csv; charset=UTF-8" pageEncoding="UTF-8" %><% out.print('\ufeff');%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %><%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>Номер заказа;Дата;Статус;Бонус;Получатель;Доставка;Имя получателя;Телефон;Адрес;
<c:forEach items="${orders}" var="o">
    <c:set var="addr" value="${o.deliveryAddress}"/>
    ${o.orderId};<fmt:formatDate value="${o.timeAdded}" pattern="dd.MM.yyyy"/>;${o.paymentStatus == 'PENDING' ? 'Новый' : (o.paymentStatus == 'COMPLETED' ? 'Завершен' : 'Аннулирован')};${o.orderObjectsInfo[0].objectName};${o.buyerEmail};${o.deliveryType == 'DELIVERY' ? 'Почтой' : 'Самовывоз'};<c:out value="${addr.fio}"/>;${addr.phone};<c:out value="${addr.zipCode}"/>, <c:out value="${addr.country}"/>, <c:out value="${addr.city}"/>, <c:out value="${addr.address}"/>;
</c:forEach>

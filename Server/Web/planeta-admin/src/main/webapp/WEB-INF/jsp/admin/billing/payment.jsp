<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ include file="/WEB-INF/jsp/includes/generated/doctype-header.jsp" %>
<head>
    <%@ include file="../head.jsp" %>
    <script type="text/javascript" src="/js/utils/json-prettify.js"></script>
    <link type="text/css" rel="stylesheet" href="/css/utils/json-prettify.css"/>
    <link type="text/css" rel="stylesheet" href="/css/utils/log-prettify.css"/>

    <c:set var="paymentAsJson" value="${hf:toJson(payment)}" />

    <script>
        var nls_ru = {
            datePattern: 'd.m.y H:i:s',
            image: 'изображение',
            link: 'ссылка',
            nullOrUndefined: 'пусто'
        };

        var paymentNlsProperties_ru = {
            key: {
                transactionId: '№ платежа',
                amountNet: 'Сумма',
                status: 'Статус',
                paymentMethodId: 'Способ оплаты',
                paymentProviderId: 'Платёжная система',
                projectType: 'Проект-источник платежа',
                param1: 'Экстра-параметр 1',
                param2: 'Экстра-параметр 2',
                timeAdded: 'Время создания',
                timeUpdated: 'Время обновления'
            },
            value: {
                paymentProviderId: '${paymentProvider != null ? paymentProvider.name : 'Не определен'}',
                paymentMethodId: '${paymentMethod != null ? paymentMethod.name : 'Не определен'}',
                projectType: {
                    MAIN: 'planeta.ru',
                    CAMPAIGN: 'start.planeta.ru',
                    CONCERT: 'concert.planeta.ru',
                    SHOP: 'shop.planeta.ru',
                    MOBILE: 'm.planeta.ru'
                },
                status: {
                    NEW: 'НОВЫЙ',
                    DONE: 'ЗАВЕРШЁН',
                    DECLINE: 'ОТКЛОНЁН',
                    ERROR: 'ОШИБКА'
                }
            }
        };

        $(document).ready(function() {
            var formatter = new JsonFormatter();

            var payment = ${paymentAsJson};
            new NlsJsonFormatter(nls_ru).format('.payment-detail-info', formatter, payment);
            new NlsJsonFormatter(_.extend(nls_ru, paymentNlsProperties_ru)).format('.payment-common-info', formatter, payment);

            formatter.format('.json-data');
            formatter.collapseAll('.json-data');
            
            $('.collapseAll').click(function(e) {
                e.preventDefault();
                var element = $(e.currentTarget).parent().next();
                formatter.collapseAll(element);
            });
            $('.expandAll').click(function(e) {
                e.preventDefault();
                var element = $(e.currentTarget).parent().next();
                formatter.expandAll(element);
            });

            $('.${initialTab}').addClass('active');
        });
    </script>

</head>
<body>

<%@ include file="/WEB-INF/jsp/admin/navbar.jsp" %>

<div id="page-wrapper">

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Платеж № ${payment.transactionId}</h1>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <ul class="col-lg-12 nav nav-tabs">
                <li class="common">
                    <a href="#tab-payment-common-info" data-toggle="tab">Кратко</a>
                </li>
                <li class="details">
                    <a href="#tab-payment-detail-info" data-toggle="tab">Детально</a>
                </li>
                <li class="log">
                    <a href="#tab-log" data-toggle="tab">Журнал</a>
                </li>
            </ul>
        </div>

        <div class="col-lg-12 tab-content">
            <div class="tab-pane common" id="tab-payment-common-info">
                <h3>Общая информация о платеже</h3>

                <div class="resizer">
                    <a href="javascript:void(0);" class="collapseAll" data-toggle="tooltip" title="Свернуть всё">
                        <i class="fa fa-minus"></i>
                    </a>
                    <a href="javascript:void(0);" class="expandAll" data-toggle="tooltip" title="Развернуть всё">
                        <i class="fa fa-plus"></i>
                    </a>
                </div>
                <pre class="payment-common-info"></pre>
            </div>

            <div class="tab-pane details" id="tab-payment-detail-info">
                <h3>Детальная информация о платеже</h3>

                <div class="resizer">
                    <a href="javascript:void(0);" class="collapseAll" data-toggle="tooltip" title="Свернуть всё">
                        <i class="fa fa-minus"></i>
                    </a>
                    <a href="javascript:void(0);" class="expandAll" data-toggle="tooltip" title="Развернуть всё">
                        <i class="fa fa-plus"></i>
                    </a>
                </div>
                <pre class="payment-detail-info"></pre>
            </div>

            <div class="tab-pane log" id="tab-log">
                <h3>
                    <c:choose>
                        <c:when test="${empty logRecords}">
                            Для данного платежа нет записей журнала обработки ...
                        </c:when>
                        <c:otherwise>
                            Список действий по обработке платежа
                        </c:otherwise>
                    </c:choose>
                </h3>

                <%@ include file="/WEB-INF/jsp/includes/log-records.jsp" %>

            </div>
        </div>
    </div>
</div>

</body>
</html>

<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ include file="/WEB-INF/jsp/includes/generated/doctype-header.jsp" %>
<head>
    <%@include file="head.jsp" %>
    <script type="text/javascript">

        $(document).ready(function () {
            new Delivery.Views.ModerateBaseServicesContent({
                el: '#container',
                model: new Delivery.Models.ModerateBaseServicesContent({
                    services:  ${hf:toJson(deliveryServices)}
                })
            }).render();
        });
    </script>
</head>

<body>

<%@ include file="/WEB-INF/jsp/admin/navbar.jsp" %>

<div id="page-wrapper">
    <div id="container">

    </div>
</div>

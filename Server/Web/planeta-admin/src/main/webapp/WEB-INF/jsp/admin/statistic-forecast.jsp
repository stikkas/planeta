<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ include file="/WEB-INF/jsp/includes/generated/doctype-header.jsp" %>
<head>
    <%@ include file="head.jsp" %>
</head>
<body>

<%@ include file="/WEB-INF/jsp/admin/navbar.jsp" %>
<div id="page-wrapper">

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Прогнозирование</h1>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12 admin-table">
            <table class="table forecast-table table-striped table-hover table-bordered">
                <thead>
                <th>Название проекта</th>
                <th>Старт проекта</th>
                <th>Окончание проекта</th>
                <th>Доход, руб./день</th>
                <th>Финансовая цель, руб.</th>
                <th>Собранная сумма (Кол-во покупок), руб. (шт.)</th>
                <th>Прогнозируемая сумма, руб.</th>
                <th>Необходимый прирост, руб./день</th>
                </thead>
                <tbody>
                <c:forEach items="${forecast}" var="row">
                    <tr>
                        <td>
                            <a href="${mainAppUrl}/campaigns/${row.campaignId}">
                                    ${row.name}
                            </a>
                        </td>
                        <td><fmt:formatDate value="${row.timeStart}" pattern="dd.MM.yyyy"/></td>
                        <td><fmt:formatDate value="${row.timeFinish}" pattern="dd.MM.yyyy"/></td>
                        <td>${row.revenuePerDay}</td>
                        <td>${row.targetAmount}</td>
                        <td>${row.amount} (${row.purchaseCount})</td>
                        <td>
                                ${row.planedAmount}
                            <c:if test="${row.neededRevenue > 0 && row.neededRevenue > row.revenuePerDay}">
                                <span class="negative"> (${row.planedAmount - row.targetAmount})</span>
                            </c:if>
                        </td>
                        <td>
                            <c:if test="${row.neededRevenue > 0 && row.neededRevenue > row.revenuePerDay}">
                                ${row.neededRevenue}
                            </c:if>
                        </td>

                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>
    </div>


</div>
</body>
</html>


<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ include file="/WEB-INF/jsp/includes/generated/doctype-header.jsp" %>
<head>
	<%@ include file="head.jsp" %>

	<style type="text/css">
		.ac_results {
			background: #fff;
			border: 1px solid #ccc;
			-webkit-box-shadow: 0 2px 4px #ccc;
			-moz-box-shadow: 0 2px 4px #ccc;
			box-shadow: 0 2px 4px #ccc;
			padding: 0 4px;
			z-index: 1060;
		}

		.ac_results ul {
			margin: 0 -4px;
		}

		.ac_results ul li {
			padding: 1px 4px;
			cursor: pointer;
		}

		.ac_results ul li.ac_over {
			background: #26aedd;
			color: #fff;
		}
	</style>

	<script type="text/javascript">
		$(document).ready(function () {
			//add autocomplete
			RegionAutocompleter.autocomplete({
				countrySelect: $('#countryId'),
				cityNameInput: $('#cityName'),
				cityIdInput: $('#cityId'),
				selectedCountryId: '${broadcastGeoTargeting.countryId}'
			});
		});
	</script>

</head>
<body>
<%@ include file="/WEB-INF/jsp/admin/navbar.jsp" %>
<div id="page-wrapper">

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                Настройки гео-о трансляции
                <a href="/moderator/event-broadcast-info.html?profileId=${broadcastGeoTargeting.profileId}&broadcastId=${broadcastGeoTargeting.broadcastId}">
                    № ${broadcastGeoTargeting.broadcastId}
                </a>
            </h1>
        </div>
    </div>

	<div class="row">
		<div class="col-lg-12">
			<form:form method="post" commandName="broadcastGeoTargeting"
			           class="form-horizontal">
				<form:input type="hidden" path="profileId"/>
				<form:input type="hidden" path="broadcastId"/>

                <div class="row ma-b-20">
                    <div class="col-lg-6">
                        <label>Страна</label>
                        <form:select path="countryId" cssClass="form-control" id="countryId">
                        </form:select>
                        <form:errors path="countryId"><span
                                class="help-inline"><form:errors path="countryId"/></span></form:errors>
                    </div>

                    <div class="col-lg-6">
                        <label>Город</label>
                        <form:input type="hidden" path="cityId" cssClass="form-control" id="cityId"/>
                        <input type="text" id="cityName" class="form-control"/>
                        <form:errors path="cityId"><span class="help-inline"><form:errors path="cityId"/></span></form:errors>
                    </div>
                </div>

                <div class="row ma-b-20">
                    <div class="col-lg-12">
                        <div class="checkbox">
                            <label>
                                <form:checkbox path="allowed" />
                                Разрешено
                            </label>
                        </div>
                    </div>
                </div>

                <div class="btn-group">
                    <button type="submit" class="btn btn-primary">Сохранить</button>
                    <a class="btn btn-default" href="/moderator/event-broadcast-info.html?broadcastId=${broadcastGeoTargeting.broadcastId}">Отмена</a>
                </div>
			</form:form>
		</div>
	</div>
</div>
</body>


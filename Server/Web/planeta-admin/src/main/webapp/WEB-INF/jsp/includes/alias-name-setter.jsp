<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<script type="text/javascript">
    $(document).ready(function () {
        $("#changeAliasButton").click(function () {
            $.ajax({
                url:"/moderator/change-alias.json",
                dataType:'json',
                type:"post",
                data:$("#changeAlias").serialize(),
                success:function (data) {
                    if (!data["success"]) {
                        $("#error-div").show();
                        $("#error-message").text(data["errorMessage"]);
                        $("#error-div").css("background-color", "#FAE5E3");
                        $("#error-message").css("color","#b94a48");
                    } else {
                        $("#error-div").show();
                        $("#error-message").text("Сохранено успешно!");
                        $("#error-message").css("color","#468847");
                        $("#error-div").css("background-color", "#dff0d8");
                        $("#newAlias").text($("#newAliasInput").val())
                    }
                }
            });
        });

        $('#close-alert').click(function () {
            $('#error-div').css('display', 'none');
        });
    });

</script>


<div class="panel panel-primary">
    <div class="panel-heading">
        Адрес страницы
    </div>

    <div class="panel-body">
        <form id="changeAlias" class="form-horizontal" method="post" action="/moderator/event-set-alias.html">
            <input type="hidden" name="profileId" value="${profile.profileId}"/>
            <fieldset>

                <div class="col-lg-12">
                    <div class="form-group">
                        <label>Новый адрес страницы</label>
                        <input class="form-control" type="text" name="aliasName" placeholder="отсутствует">
                        <p class="help-block">Новый адрес будет иметь вид: ${mainAppUrl}/<b id="newAlias"><c:choose><c:when test="${aliasName != null}"><c:out value="${aliasName}"/></c:when><c:otherwise>&lt;указанный адрес&gt;</c:otherwise></c:choose></b></p>
                    </div>
                </div>

                <button id="changeAliasButton" name="changeAliasButton" class="btn btn-primary" type="button">Сохранить</button>
            </fieldset>
        </form>
    </div>
</div>
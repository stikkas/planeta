<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ include file="/WEB-INF/jsp/includes/generated/doctype-header.jsp" %>
<%@taglib prefix="p" uri="http://planeta.ru/tags" %>
<head>
    <%@ include file="/WEB-INF/jsp/admin/head.jsp" %>

    <style type="text/css">
        img {
            max-width: 200px;
        }
    </style>

</head>
<body>

<%@ include file="/WEB-INF/jsp/admin/navbar.jsp" %>

<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Издательства</h1>
        </div>
    </div>

    <div class="main-page-actions">
        <form method="get">
            <a href="/admin/biblio/publishing-house.html" class="btn btn-success btn-circle btn-outline btn-lg" title="Добавить">
                <i class="fa fa-plus"></i>
            </a>
        </form>
    </div>

    <%--//TODO новая админка: не работает поиск--%>
    <div class="row">
        <div class="col-lg-12  ma-b-20">
            <form id="search-frm" method="GET" action="/admin/biblio/publishing-houses.html" class="clearfix">
                <input type="hidden" name="offset" value="0"/>
                <input type="hidden" name="limit" value="10"/>

                <div class="form-group input-group">
                    <input type="text"
                           class="form-control"
                           placeholder="Введите название"
                           id="xlInput"
                           name="searchString"
                           value="${searchString}">

                    <span class="input-group-btn">
                        <button class="btn btn-primary" type="submit">
                            <i class="fa fa-search"></i>
                        </button>
                    </span>
                </div>
                <div class="input-group-btn">
                    <button class="btn btn-success" formaction="publishing-houses-report.html">
                        <i class="fa fa-download"></i> Скачать в формате Excel
                    </button>
                </div>
            </form>
        </div>
    </div>

    <c:if test="${empty publishingHouses}">
        <div class="row">
            <div class="col-lg-12">
                <h2 class="text-center"><small>По Вашему запросу ничего не найдено ...</small></h2>
            </div>
        </div>
    </c:if>

    <c:if test="${not empty publishingHouses}">
        <div class="row">
            <div class="col-lg-12 admin-table">
                <table class="table table-hover table-bordered">
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>Название</th>
                            <th>Контрагент</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <c:forEach items="${publishingHouses}" var="house">
                            <tr>
                                <td> ${house.publishingHouseId} </td>
                                <td> ${house.name} </td>
                                <td> ${house.contractorName} </td>
                                <td>
                                    <a class="btn btn-outline btn-danger"
                                       href="/admin/biblio/publishing-house.html?publishingHouseId=${house.publishingHouseId}"
                                       title="Редактировать">
                                        <i class="fa fa-trash"></i>
                                    </a>
                                </td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
                <%@ include file="../paginator.jsp" %>
            </div>
        </div>
    </c:if>
</div>
</body>
</html>

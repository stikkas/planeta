<%@ include file="/WEB-INF/jsp/includes/generated/doctype-header.jsp" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@taglib prefix="p" uri="http://planeta.ru/tags" %>
<head>
    <%@ include file="/WEB-INF/jsp/admin/head.jsp" %>
    <title>${pageTitle}</title>
    <style type="text/css">
    </style>

</head>

<body>

<%@ include file="/WEB-INF/jsp/admin/navbar.jsp" %>

<div id="page-wrapper">

    <c:set var="withTypes" value="true" scope="request"/>
    <c:set var="withManagers" value="true" scope="request"/>

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">${pageTitle}</h1>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <%@ include file="campaign-events-header.jsp" %>
        </div>
    </div>


    <div class="row">
        <div class="col-lg-12 admin-table">
            <c:choose>
                <c:when test="${empty campaignNewsList}">
                    <%@ include file="/WEB-INF/jsp/admin/empty-state.jsp" %>
                </c:when>
                <c:otherwise>
                    <table class="table table-bordered table-striped">

                        <thead>
                        <tr>
                            <th>Ид проекта</th>
                            <th>Название проекта</th>
                            <th>Событие</th>
                            <th>Дата</th>
                            <th>Менеджер</th>
                        </tr>
                        </thead>

                        <c:forEach items="${campaignNewsList}" var="campaignNews" varStatus="loopStatus">
                            <tr>
                                <td><a href="${mainAppUrl}/campaigns/${campaignNews.campaign.campaignId}"
                                       target="_blank">${campaignNews.campaign.campaignId}</a></td>
                                <td><c:out value="${campaignNews.campaign.name}"/></td>
                                <td>
                                    <c:choose>
                                        <c:when test="${campaignNews.profileNews.type == 'CAMPAIGN_ON_MODERATION'}">Создан и отправлен на модерацию</c:when>
                                        <c:when test="${campaignNews.profileNews.type == 'CAMPAIGN_PAUSED'}">Приостановлен</c:when>
                                        <c:when test="${campaignNews.profileNews.type == 'CAMPAIGN_ON_REWORK'}">Отправлен на доработку</c:when>
                                        <c:when test="${campaignNews.profileNews.type == 'CAMPAIGN_DECLINED'}">Отклонен</c:when>
                                        <c:when test="${campaignNews.profileNews.type == 'CAMPAIGN_DELETED'}">Удалён</c:when>
                                        <c:when test="${campaignNews.profileNews.type == 'CAMPAIGN_STARTED'}">Запущен</c:when>
                                        <c:when test="${campaignNews.profileNews.type == 'CAMPAIGN_FINISHED_SUCCESS'}">Завершен успешно</c:when>
                                        <c:when test="${campaignNews.profileNews.type == 'CAMPAIGN_FINISHED_FAIL'}">Завершен не успешно</c:when>
                                        <c:when test="${campaignNews.profileNews.type == 'CAMPAIGN_REACHED_15'}">Собрал 15%</c:when>
                                        <c:when test="${campaignNews.profileNews.type == 'CAMPAIGN_REACHED_20'}">Собрал 20%</c:when>
                                        <c:when test="${campaignNews.profileNews.type == 'CAMPAIGN_REACHED_25'}">Собрал 25%</c:when>
                                        <c:when test="${campaignNews.profileNews.type == 'CAMPAIGN_REACHED_35'}">Собрал 35%</c:when>
                                        <c:when test="${campaignNews.profileNews.type == 'CAMPAIGN_REACHED_50'}">Собрал 50%</c:when>
                                        <c:when test="${campaignNews.profileNews.type == 'CAMPAIGN_REACHED_75'}">Собрал 75%</c:when>
                                        <c:when test="${campaignNews.profileNews.type == 'CAMPAIGN_REACHED_100'}">Собрал 100%</c:when>
                                        <c:when test="${campaignNews.profileNews.type == 'CAMPAIGN_RESUMED'}">Возобновлен</c:when>
                                        <c:when test="${campaignNews.profileNews.type == 'SHARE_PURCHASED'}">Покупка вознаграждения</c:when>
                                        <c:when test="${campaignNews.profileNews.type == 'SHARE_ADDED'}">Вознаграждение добавлена</c:when>
                                        <c:otherwise>${campaignNews.profileNews.type}</c:otherwise>
                                    </c:choose>
                                </td>
                                <td><fmt:formatDate value="${campaignNews.profileNews.timeAdded}"
                                                    pattern="dd.MM.yyyy HH:mm"/></td>
                                <td><c:out value="${campaignManagerList[loopStatus.index].fullName}"/></td>
                            </tr>
                        </c:forEach>
                    </table>
                </c:otherwise>
            </c:choose>
        </div>
    </div>

    <%@ include file="../paginator.jsp" %>
</div>

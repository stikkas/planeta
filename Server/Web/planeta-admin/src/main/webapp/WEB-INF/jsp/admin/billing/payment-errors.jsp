<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ page import="ru.planeta.model.enums.PaymentErrorType" %>
<%@ page import="ru.planeta.model.enums.PaymentErrorStatus" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ include file="/WEB-INF/jsp/includes/generated/doctype-header.jsp" %>
<c:set var="baseUrl" value="${hf:getStaticBaseUrl('')}"/>


<%@ include file="includes/badge-color.jsp" %>

<head>
    <%@ include file="../head.jsp" %>


</head>
<body>

<%@ include file="/WEB-INF/jsp/admin/navbar.jsp" %>
<div id="page-wrapper">

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Платежные ошибки</h1>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-primary">
                <div class="panel-heading">Поиск</div>

                <div class="panel-body">
                    <form:form modelAttribute="paymentErrorsFilter" method="GET">
                        <div class="row ma-b-20">
                            <div class="col-lg-6">
                                <label>Пользователь</label>
                                <form:input path="query"
                                            class="form-control"
                                            autofocus="true"
                                            placeholder="поиск по Id пользователя или email'у"/>
                            </div>

                            <div class="col-lg-3">
                                <ct:form-enum-select path="paymentErrorType"
                                                     items="<%= PaymentErrorType.values() %>"
                                                     label="Тип" notSelectLabel="Все"/>
                            </div>

                            <div class="col-lg-3">
                                <ct:form-enum-select path="paymentErrorStatus"
                                                     items="<%= PaymentErrorStatus.values() %>"
                                                     label="Статус" notSelectLabel="Все"/>
                            </div>
                        </div>
                        
                        <div class="row ma-b-20">
                            <div class="col-lg-3">
                                <label>Менеджер</label>
                                <form:input path="managerId" class="form-control" autofocus="true" placeholder="Id менеджера"/>
                            </div>
                           <div class="col-lg-3">
                                <label>Id проекта</label>
                                <form:input path="campaignId" class="form-control" autofocus="true" placeholder="Id проекта"/>
                            </div>
                           <div class="col-lg-3">
                                <label>Id заказа</label>
                                <form:input path="orderId" class="form-control" autofocus="true" placeholder="Id заказа"/>
                            </div>
                           <div class="col-lg-3">
                                <label>Id Транзакции</label>
                                <form:input path="transactionId" class="form-control" autofocus="true"
                                            placeholder="Id Транзакции"/>
                            </div>
                        </div>


                        <div class="row ma-b-20">
                            <div class="col-lg-12">
                                <ct:date-range-picker dateFrom="${paymentErrorsFilter.dateFrom}"
                                                      dateTo="${paymentErrorsFilter.dateTo}" allButton="true"/>
                            </div>
                        </div>

                        <ct:filter-buttons/>

                    </form:form>
                </div>
            </div>
        </div>
    </div>

    <c:if test="${empty paymentErrorList}">
        <div class="row">
            <div class="col-lg-12">
                <h2 class="text-center">
                    <small>По Вашему запросу ничего не найдено ...</small>
                </h2>
                <hr>
            </div>
        </div>
    </c:if>

    <c:if test="${not empty paymentErrorList}">
        <div class="row">
            <div class="col-lg-12 admin-table">
                <table class="table table-hover table-bordered">
                    <thead>
                        <tr>
                            <th>Тип</th>
                            <th>Id покупателя</th>
                            <th>e-mail</th>
                            <th>Id транзакции</th>
                            <th>Метод</th>
                            <th>Провайдер</th>
                            <th>Id заказа</th>
                            <th>Id проекта</th>
                            <th>Сумма транзакции</th>
                            <th>Датa</th>
                            <th>Все платежи</th>
                            <th></th>
                            <th>Комментарии</th>
                        </tr>
                    </thead>
                    <tbody>
                        <c:forEach items="${paymentErrorList}" var="paymentErrors" varStatus="status">
                            <tr class="qa-transaction-container" data-payment-error-id="${paymentErrors.paymentErrorId}">
                                <td>
                                    <span class="label ${typeColor[paymentErrors.paymentErrorType.name]}">
                                            ${hf:getMessage(paymentErrors.paymentErrorType.descriptionMessageCode)}
                                    </span>
                                </td>
                                <td>
                                    <a href="/moderator/user-info.html?profileId=${paymentErrors.profileId}">
                                            ${paymentErrors.profileId}
                                    </a>
                                </td>
                                <td>${paymentErrors.email}</td>
                                <td>
                                    <a href="/admin/billing/payment.html?paymentId=${paymentErrors.transactionId}" target="_blank">
                                            ${paymentErrors.transactionId}
                                    </a>
                                </td>
                                <td>
                                    <img style="max-width:52px;max-height:52px" src="//${baseUrl}${methods[paymentErrors.paymentMethodId].imageUrl}"/>
                                </td>
                                <td>
                                    <c:choose>
                                        <c:when test="${paymentErrors.paymentProviderId >= 0}">
                                            <img src="//${baseUrl}/images/payment/${providers[paymentErrors.paymentProviderId].type}.gif">
                                        </c:when>
                                        <c:otherwise>
                                            <i class="icon-question-sign"></i>
                                        </c:otherwise>
                                    </c:choose>
                                </td>
                                <td>
                                    <a href="/admin/billing/order.html?orderId=${paymentErrors.orderId}" target="_blank">
                                            ${paymentErrors.orderId}
                                    </a>
                                </td>
                                <td>
                                    <a href="/moderator/campaign-moderation-info.html?campaignId=${paymentErrors.campaignId}" target="_blank">
                                            ${paymentErrors.campaignId}
                                    </a>
                                </td>
                                <td>${paymentErrors.amount}</td>
                                <td><fmt:formatDate value="${paymentErrors.timeAdded}" pattern="dd.MM.yyyy HH:mm:ss"/></td>
                                <td>
                                    <a href="/admin/billing/payments.html?searchStr=${paymentErrors.profileId}" target="_blank">(?)</a>
                                </td>
                                <td>
                                    <div class="btn js-payment-error-to-in-progress btn-warning btn-outline"
                                         <c:if test="${paymentErrors.paymentErrorStatus == 'IN_PROGRESS'}">style="display:none"</c:if>
                                         title="В обработке">
                                        <i class="fa fa-pause"></i>
                                    </div>
                                    <div class="btn js-payment-error-to-resolved btn-success btn-outline"
                                         <c:if test="${paymentErrors.paymentErrorStatus == 'RESOLVED'}">style="display:none"</c:if>
                                         title="Решена">
                                        <i class="fa fa-check"></i>
                                    </div>
                                </td>
                                <td>
                                    <div class="btn btn-info btn-outline">
                                        <a href="/admin/billing/payment-error-comment-list.html?paymentErrorId=${paymentErrors.paymentErrorId}"
                                           title="Комментировать">
                                            <i class="fa fa-comment"></i>
                                        </a>
                                    </div>

                                    <br>

                                    <a href="/admin/billing/payment-error-comment-list.html?paymentErrorId=${paymentErrors.paymentErrorId}">
                                            ${paymentErrors.commentsCount}&nbsp;<spring:message code="decl.comments" arguments="${hf:plurals(paymentErrors.commentsCount)}"></spring:message>
                                    </a>
                                </td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
            </div>
        </div>
    </c:if>
    <c:set var="offset" value="${paymentErrorsFilter.offset}"/>
    <c:set var="limit" value="${paymentErrorsFilter.limit}"/>
    <%@ include file="../paginator.jsp" %>
</div>

<script>
    function changeStatus($el, status) {
        $.post('/admin/billing/payment-errors-change-status.json', {
            paymentErrorId: $el.closest('tr').data('paymentErrorId'),
            paymentErrorStatus: status
        }).done(function (response) {
            if (response.success) {
                $el.closest('tr').find('.btn').show();
                $el.hide();
            } else {
                alert(response.errorMessage);
            }
        });
    }

    $(".js-payment-error-to-in-progress").click(function () {
        var $el = $(this);
        changeStatus($el, "IN_PROGRESS");
    });
    $(".js-payment-error-to-resolved").click(function () {
        var $el = $(this);
        changeStatus($el, "RESOLVED");
    });
</script>


</body>
</html>

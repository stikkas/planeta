<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>


<link type="text/css" rel="stylesheet" href="//${hf:getStaticBaseUrl("")}/admin-new/css/modal.css"/>
<link type="text/css" rel="stylesheet" href="//${hf:getStaticBaseUrl("")}/admin-new/css/upload.css"/>
<link type="text/css" rel="stylesheet" href="//${hf:getStaticBaseUrl("")}/admin-new/css/error.css"/>
<link type="text/css" rel="stylesheet" href="//${hf:getStaticBaseUrl("")}/admin-new/css/communicate.css"/>
<link type="text/css" rel="stylesheet" href="//${hf:getStaticBaseUrl("")}/admin-new/css/manage-tables.css"/>
<link type="text/css" rel="stylesheet" href="//${hf:getStaticBaseUrl("")}/admin-new/css/manage-tables.css"/>
<link type="text/css" rel="stylesheet" href="//${hf:getStaticBaseUrl("")}/admin-new/css/select2.min.css"/>

<link type="text/css" rel="stylesheet" href="//${hf:getStaticBaseUrl("")}/admin-v2/css/bootstrap.min.css"/>
<link type="text/css" rel="stylesheet" href="//${hf:getStaticBaseUrl("")}/admin-v2/css/sb-admin-2.css"/>
<link type="text/css" rel="stylesheet" href="//${hf:getStaticBaseUrl("")}/admin-v2/css/font-awesome.min.css"/>
<link type="text/css" rel="stylesheet" href="//${hf:getStaticBaseUrl("")}/admin-v2/css/morris.css"/>
<link type="text/css" rel="stylesheet" href="//${hf:getStaticBaseUrl("")}/admin-v2/css/bootstrap-datetimepicker.min.css"/>
<link type="text/css" rel="stylesheet" href="//${hf:getStaticBaseUrl("")}/admin-v2/css/base.css"/>
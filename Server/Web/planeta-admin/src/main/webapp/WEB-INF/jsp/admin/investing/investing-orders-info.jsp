<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ include file="/WEB-INF/jsp/includes/generated/doctype-header.jsp" %>
<%@taglib prefix="p" uri="http://planeta.ru/tags" %>
<head>
    <%@ include file="/WEB-INF/jsp/admin/head.jsp" %>
</head>    
<body>

<%@ include file="/WEB-INF/jsp/admin/navbar.jsp" %>

<div id="page-wrapper">

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Заказы инвестинга</h1>
        </div>
    </div>

    <div class="row">
    <div class="col-lg-12">
        <div class="panel panel-primary">
            <div class="panel-heading">Поиск заказов</div>

            <div class="panel-body">
                <form id="search-frm" method="GET" action="/admin/investing-orders-info.html" class="clearfix">
                    <div class="row ma-b-20">
                        <div class="col-lg-6">
                            <label>Запрос</label>
                            <input class="form-control" type="text"
                                   placeholder="Введите email, телефон, алиас или ID пользователя"
                                   id="searchStr" name="searchStr" value="${param.searchStr}">
                            <input type="hidden" name="offset" value="0"/>
                            <input type="hidden" name="limit" value="20"/>
                        </div>

                        <div class="col-lg-3">
                            <label>Статус модерации</label>
                            <select class="form-control" id="status" name="status">
                                <option value="">Все</option>
                                <c:forEach items="${statuses}" var="s">
                                    <option value="${s}" <c:if test="${s eq param.status}">selected</c:if>>${s}</option>
                                </c:forEach>
                            </select>
                        </div>
                    </div>

                    <div class="row ma-b-20">
                        <div class="col-lg-12">
                            <ct:date-range-picker dateFrom="${dateFrom}" dateTo="${dateTo}" allButton="true"/>
                        </div>
                    </div>

                    <div class="btn-group">
                        <button id="search" class="btn btn-primary">
                            <i class="fa fa-search"></i> Поиск
                        </button>

                        <%--//TODO новая админка: тут явно что--%>
                        <button id="clear-filter" class="btn btn-default">
                            <i class="fa fa-trash"></i> Очистить
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
<div>

<c:if test="${empty investingOrdersInfo}">
<div class="row">
    <div class="col-lg-12">
        <h2 class="text-center"><small>По Вашему запросу ничего не найдено ...</small></h2>
        <hr>
    </div>
</div>
</c:if>

<c:if test="${not empty investingOrdersInfo}">
<div class="row">
    <div class="col-lg-12 admin-table">
        <table class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th>Номер счёта</th>
                    <th>Пользователь</th>
                    <th>Инвестор</th>
                    <th>Проект</th>
                    <th>Время создания</th>
                    <th>Время оплаты</th>
                    <th>Статус модерации</th>
                    <th>Действия</th>
                </tr>
            </thead>

            <tbody>
                <c:forEach items="${investingOrdersInfo}" var="investingOrderInfo" varStatus="status">
                <tr>
                    <td>
                        ${investingOrderInfo.transaction.transactionId}
                    </td>
                    <td>
                    <small>
                        Пользователь: <a href="${mainAppUrl}/${investingOrderInfo.userInfo.userId}">${investingOrderInfo.displayName}</a>
                        <br>
                        Email: <a href="mailto:${investingOrderInfo.userInfo.email}">${investingOrderInfo.userInfo.email}</a>
                        <br>
                        ${investingOrderInfo.phone}
                    </small>
                    </td>
                    <td>
                    <small>
                        <c:choose>
                        <c:when test="${investingOrderInfo.userType == 'INDIVIDUAL'}">
                        ФЛ ${investingOrderInfo.fio}  
                        </c:when>
                        <c:when test="${investingOrderInfo.userType == 'LEGAL_ENTITY'}">
                        ЮЛ&nbsp;${investingOrderInfo.orgName}
                        </c:when>
                        </c:choose>
                    </small>
                    </td>
                    <td>
                        <a href="${mainAppUrl}/campaigns/${investingOrderInfo.campaign.campaignId}">${investingOrderInfo.campaign.name}</a>
                    </td>
                    <td>
                        <small>
                            <div>
                                <fmt:formatDate value="${investingOrderInfo.transaction.timeAdded}" pattern="dd.MM.yyyy"/>
                            </div>
                            <div class="muted">
                                <fmt:formatDate value="${investingOrderInfo.transaction.timeAdded}" pattern="HH:mm"/>
                            </div>
                        </small>
                    </td>
                    <td>
                        <small>
                            <div>
                                <fmt:formatDate value="${investingOrderInfo.transaction.timeUpdated}" pattern="dd.MM.yyyy"/>
                            </div>
                            <div class="muted">
                                <fmt:formatDate value="${investingOrderInfo.transaction.timeUpdated}" pattern="HH:mm"/>
                            </div>
                        </small>
                    </td>
                    <td>
                        <c:choose>
                            <c:when test="${investingOrderInfo.status eq 'NEW'}">
                                <span class="label label-info">Новый</span>
                            </c:when>
                            <c:when test="${investingOrderInfo.status eq 'APPROVED'}">
                                <span class="label label-success">Одобрен</span>
                            </c:when>
                            <c:when test="${investingOrderInfo.status eq 'FAIL'}">
                                <span class="label label-important">Отклонён</span>
                            </c:when>
                        </c:choose>
                    </td>
                    <td>
                        <a class="btn btn-primary btn-outline" href="/admin/investing-order-info.html?orderId=${investingOrderInfo.investingOrderInfoId}" title="Редактировать счёт">
                            <i class="fa fa-pencil"></i>
                        </a>
                    </td>
                </tr>
                </c:forEach>
            </tbody>
        </table>
        <%@ include file="../paginator.jsp" %>
    </div>
</div>
</c:if>
</body>
</html>

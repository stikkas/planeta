<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ include file="/WEB-INF/jsp/includes/generated/doctype-header.jsp" %>
<%@taglib prefix="p" uri="http://planeta.ru/tags" %>
<head>
    <%@ include file="/WEB-INF/jsp/admin/head.jsp" %>

    <title>Комментарии к заявкам на обратный звонок</title>
</head>
<body>

<%@ include file="/WEB-INF/jsp/admin/navbar.jsp" %>

<div id="page-wrapper">

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Комментарии к заявкам на обратный звонок</h1>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <c:if test="${callback.type == 'FAILURE_PAYMENT'}">
                <strong>Платеж:</strong> <a href="/admin/billing/payment.html?paymentId=${callback.paymentId}">№${callback.paymentId}</a>
            </c:if>
            <c:if test="${callback.type == 'ORDERING_PROBLEM' and callback.orderType == 'SHARE'}">
                <strong>Заказ вознаграждения:</strong> <a href="${mainAppUrl}/campaigns/${callback.orderObject.campaignId}/donate/${callback.orderObject.shareId}">${callback.orderObject.shareId}</a>
            </c:if>
            <br>
            <strong>Сумма:</strong> ${callback.amount} руб.
            <br>
            <strong>Телефон:</strong> ${callback.userPhone}
            <c:if test="userId > 0">
                <br>

                <small>
                    <div>
                        Пользователь: <a
                            href="/moderator/user-info.html?profileId=${callback.userId}">${callback.userName}</a>
                    </div>
                    <div>
                        <a href="mailto:${callback.userEmail}">${callback.userEmail}</a>
                    </div>
                </small>
            </c:if>
            <c:if test="${callback.userId == 0}">
                Аноним
            </c:if>
            <br>
            <strong>Дата создания:</strong> <fmt:formatDate type="both" dateStyle="long" value="${callback.timeAdded}" />
        </div>
    </div>

    <c:if test="${callback.processed}">
        <div class="row">
            <div class="col-lg-12">
                <span class="label label-success">Обработано</span> Менеджер <a href="/moderator/user-info.html?profileId=${callback.managerId}">${callback.managerName}</a>
                <fmt:formatDate type="both" dateStyle="long" value="${callback.timeUpdated}" />
            </div>
        </div>
    </c:if>

    <br>

    <div class="row ma-b-30">
        <div class="col-lg-12">
             <form method="post">
                 <div class="row ma-b-20">
                     <div class="col-lg-12">
                         <label for="text">Добавить комментарий</label>
                         <textarea id="text" name="text" rows="6" class="form-control"></textarea>
                         <input name="callbackId" type="hidden" value="${param.callbackId}">
                     </div>
                 </div>

                 <button type="submit" class="btn btn-primary">Добавить</button>
             </form>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12 admin-table">
            <table class="table table-hover table-bordered">
                <thead>
                    <tr>
                        <th>Время добавления</th>
                        <th>Менеджер</th>
                        <th>Текст</th>
                    </tr>
                </thead>

                <tbody>
                    <c:forEach items="${comments}" var="comment">
                        <tr>
                            <td>
                                <fmt:formatDate type="both" dateStyle="long" value="${comment.timeAdded}" />
                            </td>
                            <td>
                                <a href="/moderator/user-info.html?profileId=${comment.authorId}">${comment.authorName}</a>
                            </td>
                            <td>
                                    ${comment.text}
                            </td>
                        </tr>
                    </c:forEach>
                </tbody>
            </table>

            <c:if test="${fn:length(comments) eq 0}">
                <div>Нет комментариев</div>
            </c:if>
        </div>
    </div>
</div>
</body>
</html>


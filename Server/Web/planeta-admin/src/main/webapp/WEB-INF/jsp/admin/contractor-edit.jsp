<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="x" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags/form" %>
<%@ include file="/WEB-INF/jsp/includes/generated/doctype-header.jsp" %>
<head>
    <%@ include file="head.jsp" %>
    <link type="text/css" rel="stylesheet" href="/css/ui-lightness/jquery-ui-1.10.2.custom.min.css"/>
    <link type="text/css" rel="stylesheet" href="/css/jquery.timepicker.css"/>
    <%--https://filesizejs.com--%>
    <script type="text/javascript" src="//${hf:getStaticBaseUrl("")}/js/lib/filesize.min.js"></script>
    <style>
        .control-label > span {
            display: none;
        }

        .control-label > span.required {
            display: inline;
            color: red;
        }

        .my-error {
            margin-left: 8px;
        }

        .js-cancel {
            margin-left: 16px;
        }

        .ac_results {
            background: #fff;
            border: 1px solid #ccc;
            -webkit-box-shadow: 0 2px 4px #ccc;
            -moz-box-shadow: 0 2px 4px #ccc;
            box-shadow: 0 2px 4px #ccc;
            padding: 0 4px;
            z-index: 1060;
        }

        .ac_results ul {
            margin: 0 -4px;
        }

        .ac_results ul li {
            padding: 1px 4px;
            cursor: pointer;
        }

        .ac_results ul li.ac_over {
            background: #26aedd;
            color: #fff;
        }
    </style>
    <script type="text/javascript">
        var currentUrl = document.location.href;

        function goBack() {
            window.location.href = '${backUrl}';
        }

        function deleteProfileFile(ownerProfileId, fileId) {
            if (confirm('Вы уверены что хотите удалить файл? Проверьте, что контрагент не используется в старых проектах!')) {
                $.post("/moderator/deleteByProfileId-profile-file.json", {
                    profileId: ownerProfileId,
                    fileId: fileId
                }, function (response) {
                    if (response.success) {
                        document.location.href = currentUrl;
                    } else {
                        alert(response.errorMessage);
                    }
                });
            }
            return false;
        }

        $(document).ready(function () {
            var campaignStatus = '${campaignStatus}' || null;


            var typeDrpdwn = $('#type'),
                countryDrpdwn = $('#countryId'),
                cityRow = $('.js-city'),
                innRow = $('.js-inn'),
                ogrnRow = $('.js-ogrn'),
                nameRow = $('.js-name'),
                passportRow = $('.js-passport');

            if (typeDrpdwn.val() == "INDIVIDUAL") {
                ogrnRow.hide();
            } else {
                passportRow.hide();
                ogrnRow.show();
                innRow.find('label>span').addClass('required');
                ogrnRow.find('label>span').addClass('required');
            }

            if (countryDrpdwn.val() != 1) {
                innRow.find('label>span').removeClass('required');
                ogrnRow.find('label>span').removeClass('required');
                cityRow.find('label>span').removeClass('required');
            }

            countryDrpdwn.bind('change', function () {
                if ($(this).val() == 1) {
                    cityRow.find('label>span').addClass('required');
                    if (typeDrpdwn.val() == 'INDIVIDUAL') {
                        innRow.find('label>span').removeClass('required');
                        ogrnRow.find('label>span').removeClass('required');
                        passportRow.find('label>span').addClass('required');
                    }
                    else {
                        passportRow.find('label>span').removeClass('required');
                        innRow.find('label>span').addClass('required');
                        ogrnRow.find('label>span').addClass('required');
                    }
                }
                else {
                    passportRow.find('label>span').removeClass('required');
                    innRow.find('label>span').removeClass('required');
                    ogrnRow.find('label>span').removeClass('required');
                    cityRow.find('label>span').removeClass('required');
                }
            });

            typeDrpdwn.bind('change', function () {
                if ($(this).val() == "INDIVIDUAL") {
                    passportRow.show();
                    passportRow.find('label>span').addClass('required');
                    innRow.find('label>span').removeClass('required');
                    ogrnRow.find('label>span').removeClass('required');
                    ogrnRow.hide();
                    nameRow.find('label').html('ФИО\n<span class="required">*</span>');
                }
                else {
                    passportRow.hide();
                    nameRow.find('label').html('Название\n<span class="required">*</span>');
                    ogrnRow.show();
                    if (countryDrpdwn.val() == 1) {
                        innRow.find('label>span').addClass('required');
                        ogrnRow.find('label>span').addClass('required');
                        passportRow.find('label>span').addClass('required');
                    } else {
                        innRow.find('label>span').removeClass('required');
                        ogrnRow.find('label>span').removeClass('required');
                        passportRow.find('label>span').removeClass('required');
                    }
                }
            });

            $('.js-want-save').bind('click', function () {
                Modal.showConfirm("Текущий контрагент проекта будет заменен", 'Подтверждение действия', {
                    success: function () {
                        $('.js-save').click();
                    }
                });
            });

            $("#date-box-birth-date").datetimepicker({
                format: 'DD.MM.YYYY'
            });

            <c:if test="${not empty contractor.birthDate}">
            $('#date-box-birth-date').data("DateTimePicker").date(new Date(${contractor.birthDate.time}))
            </c:if>

            $("#date-box-issue-date").datetimepicker({
                format: 'DD.MM.YYYY'
            });

            <c:if test="${not empty contractor.issueDate}">
            $('#date-box-issue-date').data("DateTimePicker").date(new Date(${contractor.issueDate.time}))
            </c:if>

            $("#parse-form").submit(function () {
                var birthDate = $("#date-box-birth-date").datetimepicker('date');
                if (birthDate) {
                    // http://momentjs.com/docs/#/displaying/format/
                    birthDate = DateUtils.addHours(DateUtils.middleOfDate(birthDate.format('x')), -DateUtils.getHoursDiff());
                    $("#birthDate").val(birthDate);
                }
                var issueDate = $("#date-box-issue-date").datetimepicker('date');
                if (issueDate) {
                    // http://momentjs.com/docs/#/displaying/format/
                    issueDate = DateUtils.addHours(DateUtils.middleOfDate(issueDate.format('x')), -DateUtils.getHoursDiff());
                    $("#issueDate").val(issueDate);
                }

                return true;
            });

            RegionAutocompleter.autocomplete({
                countrySelect: $('#countryId'),
                cityNameInput: $('#cityNameRus'),
                cityIdInput: $('#cityId'),
                selectedCountryId: '${country.countryId}'
            });

            var justUploadedImages = [];
            var profileId = workspace.appModel.get('myProfile').get('profileId');
            <c:if test="${campaign != null}">
                profileId = ${campaign.profileId};
            </c:if>

            $('#upload-file').bind('click', function () {
                UploadController.showUploadContractorFiles(profileId, function (filesUploaded) {
                    if (_.isEmpty(filesUploaded)) return;
                    var images = _.map(filesUploaded.models, function (fileUploaded) {
                        $('table.js-files-upload-before-table').find('tbody').prepend( '<tr><td><b>'
                            + fileUploaded.get('uploadResult').fileId
                            + '</b></td><td><a href=\"'
                            + fileUploaded.get('uploadResult').fileUrl +'\">'
                            + fileUploaded.get('uploadResult').name + '</a></td><td>'
                            + filesize(fileUploaded.get('uploadResult').size, {round: 0}) + '</td><td>'
                            + moment(fileUploaded.get('uploadResult').timeAdded).fromNow()+ '</td><td><a href=\"javascript:void(0);\"onclick=\"deleteProfileFile('
                            + fileUploaded.get('uploadResult').profileId +', '
                            + fileUploaded.get('uploadResult').fileId +')\"><i class=\"icon-trash\"></i></a></td></tr>');
                        return fileUploaded.get('uploadResult');
                    });
                });
            });
        })
    </script>
</head>

<body>

<%@ include file="/WEB-INF/jsp/admin/navbar.jsp" %>

<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                <c:choose>
                    <c:when test="${empty contractor || contractor.contractorId == 0}">
                        Новый контрагент
                    </c:when>
                    <c:otherwise>
                        Контрагент ${hf:escapeHtml4(contractor.name)}
                    </c:otherwise>
                </c:choose>
            </h1>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <s:form id="parse-form" commandName="contractor" method="post" action="/moderator/save-contractor.json">
                <input type="hidden" name="contractorId" value="${contractor.contractorId}"/>
                <input type="hidden" name="campaignId" id="campaignId" value="${campaignId}">

                <div class="row ma-b-20">
                    <div class="col-lg-6">
                        <label for="type">Тип <span class="required">*</span></label>
                        <select id="type" name="type" class="form-control">
                            <option value="INDIVIDUAL"
                                    <c:if test="${empty contractor || contractor.type == 'INDIVIDUAL'}">selected="selected"</c:if>>
                                Физическое лицо
                            </option>
                            <option value="INDIVIDUAL_ENTREPRENEUR"
                                    <c:if test="${contractor.type == 'INDIVIDUAL_ENTREPRENEUR'}">selected="selected"</c:if>>
                                ИП
                            </option>
                            <option value="OOO"
                                    <c:if test="${contractor.type == 'OOO'}">selected="selected"</c:if>>
                                ООО
                            </option>
                            <option value="CHARITABLE_FOUNDATION"
                                    <c:if test="${contractor.type == 'CHARITABLE_FOUNDATION'}">selected="selected"</c:if>>
                                НКО (в том числе благотворительный фонд)
                            </option>
                            <option value="LEGAL_ENTITY"
                                    <c:if test="${contractor.type == 'LEGAL_ENTITY'}">selected="selected"</c:if>>
                                Другое юридическое лицо
                            </option>
                        </select>
                    </div>

                    <div class="col-lg-6">
                        <div class="js-name">
                            <label>
                                <c:choose>
                                    <c:when test="${empty contractor || contractor.type == 'INDIVIDUAL'}">
                                        ФИО
                                    </c:when>
                                    <c:otherwise>Название</c:otherwise>
                                </c:choose>
                                <span class="required">*</span>
                            </label>

                            <s:input type="text" name="name" class="form-control" path="name"/>
                            <s:errors path="name" cssClass="error-message my-error" element="span"/>
                        </div>
                    </div>
                </div>

                <div class="row ma-b-20">
                    <div class="col-lg-6">
                        <div class="js-initialsWithLastName">
                            <label>Инициалы + Фамилия</label>
                            <s:input type="text" name="initialsWithLastName" class="form-control" path="initialsWithLastName"/>
                            <s:errors path="initialsWithLastName" cssClass="error-message my-error" element="span"/>
                        </div>
                    </div>

                    <div class="col-lg-6">
                        <div class="js-responsiblePerson">
                            <label>Ответственное лицо в родительном падеже(для Физ. и Юр. лиц) <span class="required">*</span></label>
                            <s:input type="text" name="responsiblePerson" path="responsiblePerson" class="form-control"/>
                            <s:errors path="responsiblePerson" cssClass="error-message my-error" element="span"/>
                        </div>
                    </div>
                </div>

                <div class="row ma-b-20">
                    <div class="col-lg-6">
                        <label>Телефон <span class="required">*</span></label>
                        <s:input type="text" name="phone" path="phone" class="form-control"/>
                        <s:errors path="phone" cssClass="error-message my-error" element="span"/>
                    </div>

                    <div class="col-lg-6">
                        <label>Страна регистрации <span class="required">*</span></label>

                        <select id="countryId" name="countryId" class="form-control">
                            <c:forEach var="country" items="${countries}">
                                <option value="${country.countryId}" <c:if
                                        test="${(empty contractor && country.countryId == 1) || contractor.countryId == country.countryId}"> selected</c:if>>
                                    <c:out value="${country.countryNameRus}"/>
                                </option>
                            </c:forEach>
                        </select>
                    </div>
                </div>

                <div class="row ma-b-20">
                    <div class="col-lg-6">
                        <label>Город <span class="required">*</span></label>

                        <s:input type="text" name="cityNameRus" path="cityNameRus" class="form-control"/>
                        <s:errors path="cityNameRus" cssClass="error-message my-error" element="span"/>
                        <s:errors path="cityId" cssClass="error-message my-error" element="span"/>
                        <s:input type="text" name="cityId" path="cityId" class="hidden"/>
                    </div>

                    <div class="col-lg-6">
                        <div class="js-birthDate">
                            <label>Дата рождения <span class="required">*</span></label>

                            <s:input id="birthDate" type="hidden" path="birthDate" cssClass="form-control"/>
                            <input id="date-box-birth-date" class="form-control" value='' type="text"/>
                            <s:errors path="birthDate" cssClass="error-message my-error" element="span"/>
                        </div>
                    </div>
                </div>

                <div class="row ma-b-20">
                    <div class="col-lg-6">
                        <div class="js-inn">
                            <label>ИНН <span class="required">*</span></label>
                            <s:input type="text" name="inn" path="inn" class="form-control"/>
                            <s:errors path="inn" cssClass="error-message my-error" element="span"/>
                        </div>
                    </div>

                    <div class="col-lg-6">
                        <div class="js-passport">
                            <label>Паспорт <span class="required">*</span></label>
                            <s:input type="text" name="passportNumber" path="passportNumber" class="form-control"/>
                            <s:errors path="passportNumber" cssClass="error-message my-error" element="span"/>
                        </div>
                    </div>

                    <div class="col-lg-6">
                        <div class="js-ogrn">
                            <label>ОГРН <span class="required">*</span></label>
                            <s:input type="text" name="ogrn" path="ogrn" class="form-control"/>
                            <s:errors path="ogrn" cssClass="error-message my-error" element="span" />
                        </div>
                    </div>
                </div>

                <div class="row ma-b-20">
                    <div class="col-lg-12">
                        <label>Прочие данные</label>
                        <textarea id="otherDetails" name="otherDetails" class="form-control" type="text" rows="6">${contractor.otherDetails}</textarea>
                    </div>
                </div>

                <div class="row ma-b-20">
                    <div class="col-lg-6">
                        <div class="js-legalAddress">
                            <label>Юридический адрес / Зарег. <span class="required">*</span></label>
                            <s:input type="text" name="legalAddress" path="legalAddress" class="form-control"/>
                            <s:errors path="legalAddress" cssClass="error-message my-error" element="span"/>
                        </div>
                    </div>

                    <div class="col-lg-6">
                        <div class="js-actualAddress">
                            <label>Фактический адрес <span class="required">*</span></label>
                            <s:input type="text" name="actualAddress" path="actualAddress" class="form-control"/>
                            <s:errors path="actualAddress" cssClass="error-message my-error" element="span"/>
                        </div>
                    </div>
                </div>

                <div class="row ma-b-20">
                    <div class="col-lg-4">
                        <div class="js-authority">
                            <label>Кем выдан паспорт <span class="required">*</span></label>
                            <s:input type="text" name="authority" path="authority" class="form-control"/>
                            <s:errors path="authority" cssClass="error-message my-error" element="span"/>
                        </div>
                    </div>

                    <div class="col-lg-4">
                        <div class="js-unit">
                            <label>Код подразделения <span class="required">*</span></label>
                            <s:input type="text" name="unit" path="unit" class="form-control"/>
                            <s:errors path="unit" cssClass="error-message my-error" element="span"/>
                        </div>
                    </div>

                    <div class="col-lg-4">
                        <div class="js-issueDate">
                            <label>Когда выдан паспорт <span>*</span></label>
                            <s:input id="issueDate" type="hidden" path="issueDate" cssClass="form-control"/>
                            <input id="date-box-issue-date" class="form-control" value='' type="text"/>
                            <s:errors path="issueDate" cssClass="error-message my-error" element="span"/>
                        </div>
                    </div>
                </div>


                <%-- новая админка: сделать сканы паспорта --%>
                <div class="row ma-b-20">
                    <div class="col-lg-12">
                        <label>Скан паспорта <span>*</span></label>
                        <c:if test="${campaignId == null}">
                            <div class="alert alert-danger bs-alert-old-docs">
                                <strong>Внимание!</strong>
                                Файлы привязанны к проекту, поэтому чтобы их загрузить
                                или увидеть нужно перейти в редактирование контрагента из модерации проекта,
                                к которому привязан данный контрагент.
                            </div>
                        </c:if>
                        <c:if test="${campaignId != null}">
                            <span id="upload-file" class="btn btn-info"><i class="icon-upload"></i> Загрузить скан</span>
                        </c:if>
                    </div>
                </div>

                <div class="row ma-b-20">
                    <div class="col-lg-12 admin-table">
                        <table class="table table-striped table-hover table-bordered js-files-upload-before-table">
                            <c:forEach var="file" items="${filesUploadedBefore}">
                                <tr>
                                    <td>
                                        <b>${file.fileId}</b>
                                    </td>
                                    <td>
                                        <a href="${file.fileUrl}">${file.name}</a>
                                    </td>
                                    <td>${hf:getFileSize(file.size)}</td>
                                    <td>${hf:dateFormat(file.timeAdded)}</td>
                                    <td>
                                        <a href="javascript:void(0);"
                                           onclick="deleteProfileFile(${file.profileId}, ${file.fileId})">
                                            <i class="icon-trash"></i>
                                        </a>
                                    </td>
                                </tr>
                            </c:forEach>
                        </table>
                    </div>
                </div>

                <div class="row ma-b-20">
                    <div class="col-lg-4">
                        <div class="js-beneficiaryBank">
                            <label>Банк получателя <span class="required">*</span></label>
                            <s:input type="text" name="beneficiaryBank" path="beneficiaryBank" class="form-control"/>
                            <s:errors path="beneficiaryBank" cssClass="error-message my-error" element="span"/>
                        </div>
                    </div>

                    <div class="col-lg-4">
                        <div class="js-bic">
                            <label>БИК <span class="required">*</span></label>
                            <s:input type="text" name="bic" path="bic" class="form-control"/>
                            <s:errors path="bic" cssClass="error-message my-error" element="span"/>
                        </div>
                    </div>

                    <div class="col-lg-4">
                        <div class="js-kpp">
                            <label>КПП <span class="required">*</span></label>
                            <s:input type="text" name="kpp" path="kpp" class="form-control"/>
                            <s:errors path="kpp" cssClass="error-message my-error" element="span"/>
                        </div>
                    </div>
                </div>

                <div class="row ma-b-20">
                    <div class="col-lg-6">
                        <div class="js-checkingAccount">
                            <label>Рас. счет № <span class="required">*</span></label>
                            <s:input type="text" name="checkingAccount" path="checkingAccount" class="form-control"/>
                            <s:errors path="checkingAccount" cssClass="error-message my-error" element="span"/>
                        </div>
                    </div>

                    <div class="col-lg-6">
                        <div class="js-correspondingAccount">
                            <label>Кор.Счет <span class="required">*</span></label>
                            <s:input type="text" name="correspondingAccount" path="correspondingAccount" class="form-control"/>
                            <s:errors path="correspondingAccount" cssClass="error-message my-error" element="span"/>
                        </div>
                    </div>
                </div>

                <div class="row ma-b-20">
                    <div class="col-lg-12">
                        <button type="submit" class="btn btn-primary hidden js-save">Сохранить</button>
                        <button type="button" class="btn btn-primary js-want-save">Сохранить</button>
                        <button type="button" class="btn btn-default js-cancel" onclick="goBack()">Отмена</button>
                    </div>
                </div>
            </s:form>
        </div>
    </div>
</div>

</body>
</html>

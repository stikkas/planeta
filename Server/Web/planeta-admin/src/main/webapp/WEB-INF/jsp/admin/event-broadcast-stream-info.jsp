<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ include file="/WEB-INF/jsp/includes/generated/doctype-header.jsp" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<head>
    <%@ include file="head.jsp" %>

    <script type="text/javascript">
        $(document).ready(function () {
            $(".upload-image").click(function () {
                UploadController.showUploadBroadcastStreamAvatar(-1, function (filesUploaded) {
                    if (!filesUploaded || !filesUploaded.length) {
                        return;
                    }
                    var uploadResult = filesUploaded.at(0).get('uploadResult');
                    if (!uploadResult) {
                        return;
                    }
                    $('#stream-image-url').val(uploadResult.imageUrl);
                    $('#stream-image-img').attr('src', uploadResult.imageUrl);
                });
            });

            var updateEmbedTextareaState = function (object) {
                var isAudio = object.val() == "AUDIO";
                $("#broadcastUrl").attr("required", isAudio);
            };

            var broadcastTypeEl = $("#broadcastType");
            updateEmbedTextareaState(broadcastTypeEl);
            broadcastTypeEl.change(function () {
                updateEmbedTextareaState($(this));
            });
        });
    </script>

</head>
<body>

<%@ include file="/WEB-INF/jsp/admin/navbar.jsp" %>

<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                Добавить поток для трансляции
                <a href="/moderator/event-broadcast-info.html?broadcastId=${broadcastStream.broadcastId}">
                    № ${broadcastStream.broadcastId}
                </a>
            </h1>
        </div>
    </div>
    
    <div class="main-page-actions">
        <c:if test="${broadcastStream.streamId > 0 && broadcastStatus == 'LIVE'}">
            <c:if test="${broadcastStream.broadcastStatus != 'PAUSED'}">
                <a class="btn btn-danger"
                    href="/moderator/event-broadcast-toggle-stream-state.html?broadcastId=${broadcastStream.broadcastId}&streamId=${broadcastStream.streamId}"
                    title="Остановить">
                    <i class="fa fa-stop"></i>
                </a>
            </c:if>
            <c:if test="${broadcastStream.broadcastStatus == 'PAUSED'}">
                <a class="btn btn-success"
                   href="/moderator/event-broadcast-toggle-stream-state.html?broadcastId=${broadcastStream.broadcastId}&streamId=${broadcastStream.streamId}"
                   title="Запустить">
                    <i class="fa fa-play"></i>
                </a>
            </c:if>
        </c:if>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <form:form method="post" commandName="broadcastStream"
                       class="form-horizontal">
                <form:input type="hidden" path="streamId"/>
                <form:input type="hidden" path="broadcastId"/>
        
                <div class="row ma-b-20">
                    <div class="col-lg-4">
                        <label>Название</label>
                        <form:input type="text" path="name" cssClass="form-control" required="required"/> <form:errors
                            path="name"><span
                            class="help-inline"><form:errors path="name"/></span></form:errors>

                    </div>

                    <div class="col-lg-4">
                        <label>Тип потока</label>

                        <form:select path="broadcastType" cssClass="form-control">
                            <option value="VIDEO"
                                    <c:if test="${broadcastStream.broadcastType == \"VIDEO\"}">selected="selected"</c:if>>
                                Видео поток
                            </option>
                            <option value="EMBED"
                                    <c:if test="${broadcastStream.broadcastType == \"EMBED\"}">selected="selected"</c:if>>
                                Встраиваемое видео
                            </option>
                        </form:select>
                    </div>


                    <div class="col-lg-4">
                        <label>Url потока</label>
                        <form:input type="text" path="broadcastUrl" cssClass="form-control"/> <form:errors
                            path="broadcastUrl"/>
                    </div>
                </div><div class="row ma-b-20">
                    <div class="col-lg-8">
                        <label>Embed</label>
                        <form:textarea path="embedVideoHtml" cssClass="form-control" rows="10"/> <form:errors
                            path="embedVideoHtml"/>
                    </div>

                    <div class="col-lg-4">
                        <label>Описание</label>
                        <form:textarea path="description" cssClass="form-control" rows="10"/> <form:errors
                            path="description"><span
                            class="help-inline"><form:errors path="description"/></span></form:errors>
                    </div>
                </div>
                    
                <div class="row ma-b-20">
                    <div class="col-lg-6">
                        <ct:file url="${broadcastStream.imageUrl}" path="imageUrl" type="IMAGE" label="Заглушка"/>
                    </div>
                </div>

                <div class="btn-group">
                    <button type="submit" class="btn btn-primary">Сохранить</button>
                    <a class="btn btn-default" href="/moderator/event-broadcast-info.html?broadcastId=${broadcastStream.broadcastId}">Отмена</a>
                </div>
            </form:form>
        </div>
    </div>
</div>
</body>


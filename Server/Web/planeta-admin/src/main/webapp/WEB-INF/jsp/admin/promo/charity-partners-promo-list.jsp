<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ include file="/WEB-INF/jsp/includes/generated/doctype-header.jsp" %>
<head>
    <%@include file="../head.jsp" %>

    <script type="text/javascript">
        $(document).ready(function() {
            $('.js-remove').on('click', function(e) {
                e.preventDefault();
                var href = e.currentTarget.href;

                if(confirm('Удалить новость?')) {
                    document.location = href;
                }
            });

            var startIndex;
            //JQUERY plugin that makes possible to sort collection and move collection elements
            var sortable = $('table tbody').sortable({
                revert: true,
                handle: '.btn.move',

                helper: function(e, ui) {
                    ui.children().each(function() {
                        $(this).width($(this).width());
                    });
                    return ui;
                },

                start: function(event, ui) {
                    ui.placeholder.html(ui.helper.html());
                    startIndex = ui.item.index();
                },

                update: function(event, ui) {
                    var stopIndex = ui.item.index();
                    if(stopIndex != startIndex) {
                        $('form#sort #startIndex').val(startIndex);
                        $('form#sort #stopIndex').val(stopIndex);
                        $('form#sort').submit();
                    }
                }
            });
            sortable.disableSelection();
        });
    </script>

</head>
<body>

<%@ include file="/WEB-INF/jsp/admin/navbar.jsp" %>

<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Партнеры</h1>
        </div>
    </div>

    <div class="main-page-actions">
        <a class="btn btn-success btn-circle btn-outline btn-lg" href="/moderator/charity-partners-promo-fill.html" title="Добавить нового партнёра">
            <i class="fa fa-plus"></i>
        </a>
    </div>

    <div class="row">
        <div class="col-lg-12 admin-table">
            <table class="table table-bordered table-striped">
                <thead>
                <th>Фото</th>
                <th>Партнер</th>
                <th>Действие</th>
                </thead>
                <tbody>
                <c:forEach items="${configurationList}" var="promo" varStatus="st">
                    <tr>
                        <td width="1px">
                            <a class="ap-item" href="${promo.originalUrl}" target="_blank" title="${promo.name}">
                                <img src="${promo.imageUrl}" alt="${promo.name}" style="max-height: 70px; max-width: 116px;">
                            </a>
                        </td>
                        <td>
                                ${promo.name}
                        </td>
                        <td class="col-lg-2">
                            <div class="btn-group">
                                <a href="/moderator/charity-partners-promo-edit.html?id=${st.index}" class="btn btn-primary" title="Редактировать">
                                    <i class="fa fa-pencil"></i>
                                </a>
                                <a href="/moderator/charity-partners-promo-delete.html?id=${st.index}" class="btn btn-danger js-remove" title="Удалить">
                                    <i class="fa fa-trash"></i>
                                </a>
                            </div>

                            <a class="btn btn-default move" title="Переместить" style="margin-left: 100px">
                                <i class="fa fa-arrows"></i>
                            </a>
                        </td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>

            <form id="sort" action="/moderator/charity-partners-promo-sort.html" method="post">
                <input type="hidden" name="startIndex" id="startIndex" />
                <input type="hidden" name="stopIndex" id="stopIndex" />
            </form>
        </div>

    </div>
</body>




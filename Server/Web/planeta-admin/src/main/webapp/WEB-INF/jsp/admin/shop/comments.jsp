<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ include file="/WEB-INF/jsp/includes/generated/doctype-header.jsp" %>
<head>
    <%@ include file="/WEB-INF/jsp/admin/head.jsp" %>
    <title></title>
</head>
<body>
<%@ include file="/WEB-INF/jsp/admin/navbar.jsp" %>
<c:set var="count" value="${comments.count}"/>
<c:set var="list" value="${comments.list}"/>

<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Комментарии к продуктам</h1>
        </div>
    </div>

    <%--//TODO новая админка: почему тут нет кнопки поиска и ничего не ищется?--%>

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-primary">
                <div class="panel-heading">Поиск</div>

                <div class="panel-body">
                    <form class="form-horizontal" id="stat-params" method="get">
                        <div class="row ma-b-20">
                            <div class="col-lg-12">
                                <label>ID или часть названия товара</label>

                                <input id="query"
                                       name="query"
                                       type="text"
                                       placeholder="Поиск"
                                       class="form-control"
                                       value="${query}">
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-12">
                                <ct:date-range-picker dateFrom="${dateFrom}" dateTo="${dateTo}" allButton="true"/>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12 admin-table">
            <c:choose>
                <c:when test="${empty list}">
                    <%@ include file="/WEB-INF/jsp/admin/empty-state.jsp" %>
                </c:when>
                <c:otherwise>
                    <table class="table table-bordered table-striped">

                        <thead>
                        <tr>
                            <th>Название товара</th>
                            <th>Текс комментария</th>
                            <th>Дата</th>
                        </tr>
                        </thead>

                        <c:forEach items="${list}" var="item" varStatus="loopStatus">
                            <tr>
                                <td>
                                    <a href="http://${properties['shop.application.host']}/products/${item.productId}"
                                       target="_blank">${item.productName}</a>
                                </td>
                                <td>${item.commentText}</td>
                                <td>
                                    <div><fmt:formatDate value="${item.timeAdded}" pattern="dd.MM.yyyy"/></div>
                                    <div class="muted"><fmt:formatDate value="${item.timeAdded}"
                                                                       pattern="HH:mm:ss"/></div>
                                </td>
                            </tr>
                        </c:forEach>
                    </table>
                </c:otherwise>
            </c:choose>
        </div>
    </div>

    <%@ include file="../paginator.jsp" %>
</div>
</body>
</html>


<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags/form" %>
<%@ include file="/WEB-INF/jsp/includes/generated/doctype-header.jsp" %>
<head>
    <%@include file="../head.jsp" %>
    <script>
        $(document).ready(function () {
            $("#imageUploader .upload-image").click(function () {
                UploadController.showUploadWelcomePromoImage(workspace.appModel.get('myProfile').get('profileId'), function (filesUploaded) {
                    if (!filesUploaded || !filesUploaded.length) {
                        return;
                    }
                    var uploadResult = filesUploaded.at(0).get('uploadResult');
                    if (!uploadResult) {
                        return;
                    }
                    var src = uploadResult.imageUrl;
                    var image = new Image();
                    image.src = src;
                    $(image).load(function () {
                        if (image.width >= 5 && image.height >= 5) {
                            $("#imageUrl").val(src);
                            $("#imageUploader").find(".error-message").addClass("hide");
                        } else {
                            $("#imageUrl").val("");
                            $("#imageUploader").find(".error-message").removeClass("hide");
                        }
                    });
                }, "Размер не менее 5x5.");
            });
        });

    </script>
</head>
<body>

<%@ include file="/WEB-INF/jsp/admin/navbar.jsp" %>

<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Кампусы</h1>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <s:form class="form-horizontal" commandName="campus" method="post">
                <s:input path="id" type="hidden"/>

                <div class="row ma-b-20">
                    <div class="col-lg-6">
                        <label>Название</label>
                        <s:input path="name" type="text" placeholder="" class="form-control" required=""/>
                        <p class="help-block">Полное название кампуса</p>
                        <s:errors path="name" cssClass="error-message" element="span"/>
                    </div>

                    <div class="col-lg-6">
                        <label>Короткое название</label>
                        <s:input path="shortName" type="text" placeholder="" class="form-control" required=""/>
                        <p class="help-block">Сокращенное название кампуса</p>
                        <s:errors path="shortName" cssClass="error-message" element="span"/>
                    </div>
                </div>

                <div class="row ma-b-20">
                    <div class="col-lg-6">
                        <ct:file url="${campus.imageUrl}" path="imageUrl" label="Эмблема" type="IMAGE"/>
                    </div>
                </div>

                <div class="row ma-b-20">
                    <div class="col-lg-12">
                        <label>Короткое название</label>
                        <form:select path="tagId" items="${tags}" itemValue="id" itemLabel="name"
                                     cssClass="form-control"/>
                        <p class="help-block">Категория ассоциируемая с кампусом</p>
                        <s:errors path="tagId" cssClass="error-message" element="span"/>
                    </div>
                </div>

                <div class="btn-group">
                    <button formaction="/admin/save-campus.html" class="btn btn-primary">Сохранить</button>
                    <a href="/admin/campuses.html" class="btn btn-default">Отмена</a>
                </div>
            </s:form>
        </div>
    </div>
</div>
</body>

<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ include file="/WEB-INF/jsp/includes/generated/doctype-header.jsp" %>
<head>
    <%@ include file="head.jsp" %>
</head>
<body>
<%@ include file="/WEB-INF/jsp/admin/navbar.jsp" %>
<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Настройки</h1>
        </div>
    </div>

    <div class="main-page-actions">
        <form method="get" action="/admin/configuration-edit.html">
            <input type="hidden" name="configurationKey" value="NEW"/>
            <button class="btn btn-success btn-circle btn-outline btn-lg" title="Добавить">
                <i class="fa fa-plus"></i>
            </button>
        </form>
    </div>

    <div class="row mr-b-20">
        <div class="col-lg-12">
            <form id="config-filter" method="get" action="/admin/configuration-list.html">

                <div class="form-group input-group">
                    <input type="text" class="form-control" placeholder="Поиск" id="query" name="query" value="${query}">
                    <span class="input-group-btn">
                        <button class="btn btn-primary" type="submit">
                            <i class="fa fa-search"></i>
                        </button>
                    </span>
                </div>
            </form>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12 admin-table">
            <table class="table table-striped table-hover table-bordered">
                <thead>
                <th>Ключ</th>
                <th>Число</th>
                <th>Строка</th>
                <th>Булево</th>
                <th>Действия</th>
                </thead>
                <tbody>
                <c:forEach var="configuration" items="${configurations}">
                    <tr>
                        <td>
                            <b>${configuration.key}</b>
                            </br>
                            <i>
                                <small>${configuration.description}</small>
                            </i>
                        </td>
                        <td>
                                ${configuration.intValue}
                        </td>
                        <c:set var="strVal" value="${fn:escapeXml(configuration.stringValue)}"/>
                        <td title="${strVal}">
                                ${hf:getShrinkedString(strVal,20)}
                        </td>
                        <td>
                                ${configuration.booleanValue}
                        </td>
                        <td class="text-right admin-table-actions">
                            <a href="/admin/configuration-edit.html?configurationKey=${configuration.key}" class="btn btn-primary btn-outline" title="Редактировать">
                                <i class="fa fa-pencil"></i>
                            </a>
                        </td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>
    </div>


</div>
</body>
</html>

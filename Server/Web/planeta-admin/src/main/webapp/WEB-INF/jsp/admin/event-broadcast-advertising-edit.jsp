<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ include file="/WEB-INF/jsp/includes/generated/taglibs.jsp" %>
<%@ include file="/WEB-INF/jsp/includes/generated/doctype-header.jsp" %>
<head>
<%@ include file="head.jsp" %>
<script>
var EditAdvertising = function () {
    var vastDTO = {
        adName: "who_cares",
        videoUrl: "${vastDTO.videoUrl}",
        adId:${vastDTO.adId},
        status: "${advertisingDTO.state}",
        redirectUrl: "${vastDTO.redirectUrl}",
        mediaType: "video/mp4",
        duration:${vastDTO.duration},
        width:${vastDTO.width},
        height:${vastDTO.height},
        deliveryType: "progressive",
        description: "${vastDTO.description}"
    };

    var searchers;

    this.init = function () {
        searchers = {};
        //if (vastDTO.adId > 0) {
        showProcessing(false);
        //}

        $("#js-show-load-video-modal").click(function () {
            var profileId = workspace.appModel.get('myProfile').get('profileId');
            UploadController.showUploadVideo(profileId, onVideoLoaded);
        });

        $("#videoUrl").bind("paste", function () {
            setTimeout(onVideoUrlPaste, 0);
        });
    };

    var onVideoUrlPaste = $.proxy(function (data) {
        vastDTO.videoUrl = $("#videoUrl")[0].value;
        vastDTO.duration = 0;
        updateInputs(vastDTO);
        showProcessing(false);
        $(".js-error-text").hide();
    }, this);

    var onVideoLoaded = $.proxy(function (data) {
        showProcessing(true);
        var video = data.models[0].attributes.uploadResult;
        vastDTO.mediaType = "video/mp4";
        vastDTO.duration = video.duration;
        vastDTO.adName = video.name;
        vastDTO.adId = video.videoId;
        $(".js-error-text").hide();
        if (video.status == "PROCESSING") {
            vastDTO.videoUrl = video.qualityModesUrls.NOT_SET;
            this.scheduleFileProcessingCompleteCheck(video);
            showProcessing(true);
        } else {
            vastDTO.height = "640";
            vastDTO.width = "480";
            vastDTO.videoUrl = video.videoUrl;
            updateInputs(vastDTO);
            showProcessing(false);
        }
    }, this);

    this.scheduleFileProcessingCompleteCheck = function (video) {
        for (var key in searchers) {
            searchers[key].dispose();
            delete searchers[key];
        }

        var path240p = video.qualityModesUrls.NOT_SET.replace(/null/, "240p.mp4");
        var path480p = video.qualityModesUrls.NOT_SET.replace(/null/, "480p.mp4");
        var path720p = video.qualityModesUrls.NOT_SET.replace(/null/, "720p.mp4");

        var checker = new PriorityChecker();

        var SuccessCallback = function (type) {
            //function call with searcher context
            return function () {
                if (checker(type)) {
                    vastDTO.videoUrl = this.path;
                    vastDTO.width = this.w;
                    vastDTO.height = this.h;
                    updateInputs(vastDTO);
                    setTimeout(function () {
                        showProcessing(false);
                    }, 10000);
                }
            }
        };

        var ErrorCallback = function (type) {
            return function () {
                delete searchers[type];
                if ($.isEmptyObject(searchers)) {
                    $(".js-error-text").show();
                    $("#js-processing-text").hide();
                }
            }
        };

        searchers[240] = new Searcher({
            path: path240p,
            onFoundCallback: new SuccessCallback(240),
            onFailCallback: new ErrorCallback(240),
            h: "240",
            w: "320"
        });

        searchers[480] = new Searcher({
            path: path480p,
            onFoundCallback: new SuccessCallback(480),
            onFailCallback: new ErrorCallback(480),
            h: "480",
            w: "640"
        });

        searchers[720] = new Searcher({
            path: path720p,
            onFoundCallback: new SuccessCallback(720),
            onFailCallback: new ErrorCallback(720),
            h: "720",
            w: "1280"
        });
    };

    this.init();
};

var PriorityChecker = function () {
    this.last = 0;
    this[480] = 720;
    this[240] = 240;
    this[720] = 480;
    var self = this;
    return function (current) {
        if (self.last < self[current]) {
            self.last = current;
            return true;
        }
        return false;
    }
};

var Searcher = function (data) {
    this.timeout = 0;
    this.tryes = 60;
    this.w = data.w;
    this.h = data.h;
    this.path = data.path;
    var tryInterval = 1000;
    var self = this;

    var tryFound = function () {
        $.ajax({
            type: 'HEAD',
            url: data.path,
            success: function () {
                if (self.tryes <= 0) return;
                data.onFoundCallback.call(self);
            },
            error: function () {
                if (self.tryes-- > 0) {
                    self.timeout = setTimeout(tryFound, tryInterval);
                } else {
                    data.onFailCallback.call(self);
                }
            }
        });
    };

    this.dispose = function () {
        if (self.tryes <= 0) return;
        self.tryes = 0;
        clearTimeout(this.timeout);
    };

    tryFound();
};

function showProcessing(bool) {
    if (bool) {
        $(".js-save-button").attr("disabled", "disabled");
        $("#js-processing-text").show();
    } else {
        $(".js-save-button").removeAttr("disabled");
        $("#js-processing-text").hide();
    }
}

function updateInputs(vastDTO) {
    for (var key in vastDTO) {
        $("input[name='" + key + "']").val(vastDTO[key]);
    }
}

$(document).ready(function () {
    //prevent submit form on press enter
    $(window).keydown(function (event) {
        if (event.keyCode == 13) {
            event.preventDefault();
            return false;
        }
    });

    $(".js-radio-checkbox").click(function () {
        var checked = this.checked;
        setReadonly($(".js-radio-checkbox").closest(".control-group").find(".controls"), true);
        if (checked) {
            $(".js-radio-checkbox").attr("checked", false);
            setReadonly($(this).closest(".control-group").find(".controls"), false);
            this.checked = checked;
        }
    });

    window.editAdvertising = new EditAdvertising();

    setReadonly($(".js-date-controls"), !${advertisingDTO.dateChecked});
    setReadonly($(".js-campaign-controls"), !${advertisingDTO.campaignChecked});

    $(".js-ad-form").submit(function () {
        $(".js-save-button").attr("disabled", true);
    });
});


function setReadonly(selector, flag) {
    flag ? selector.css("pointer-events", "none") : selector.css("pointer-events", "all");
    var datepickers = selector.find(".js-datepicker-container");
    if (datepickers.length) {
        datepickers.find("input").attr("disabled", flag);
        var dateInputs = selector.find("input").not("#campaignId");
        if (dateInputs.length) {
            dateInputs.attr("disabled", flag);
        }
    }
}

</script>
</head>
<body>
<%@ include file="/WEB-INF/jsp/admin/navbar.jsp" %>
<div id="container" class="container">
    <c:if test="${broadcastId > 0}">
        <ul class="breadcrumb">
            <li><a href="/">Главная</a> <span class="divider">/</span></li>
            <li class="active">Модерация <span class="divider">/</span></li>
                <a href="/moderator/event-broadcast-info.html?profileId=${eventId}&broadcastId=${advertisingDTO.broadcastId}">
                    Трансляция № ${advertisingDTO.broadcastId}</a></li>
        </ul>

        <div class="mrg-b-30">
            <h2>Настройки рекламы для трансляции № ${advertisingDTO.broadcastId}</h2>
        </div>
    </c:if>

    <div class="row">

        <div class="span7">
            <form action="" method="post" class="form-horizontal js-ad-form">

                <div class="control-group <form:errors path="vastDTO.adName">error</form:errors>">
                    <label class="control-label">Название </label>
                    <div class="controls">
                        <spring:bind path="vastDTO.adName">
                            <input type="text" name="${status.expression}" value="${status.value}"/>
                        </spring:bind>
                        <form:errors path="vastDTO.adName">
                            <span class="help-inline"><form:errors path="vastDTO.adName"/></span>
                        </form:errors>
                        <%--<form:errors path=""><span class="help-inline"><form:errors path=""/></span></form:errors>--%>
                    </div>
                </div>


                <form:input type="hidden" path="vastDTO.adId"/>
                <div class="control-group <form:errors path="vastDTO.videoUrl">error</form:errors>">
                    <label class="control-label">Видео </label>

                    <div class="controls">
                        <form:input path="vastDTO.videoUrl" cssClass="span3" />
                        <button type="button" id="js-show-load-video-modal" class="btn btn-primary">Загрузить</button>
                        <form:errors path="vastDTO.videoUrl">
                            <span class="help-inline"><form:errors path="vastDTO.videoUrl"/></span>
                        </form:errors>
                    </div>
                </div>

                <div class="control-group <form:errors path="vastDTO.redirectUrl">error</form:errors>">
                    <label class="control-label">Адресс перехода </label>

                    <div class="controls">
                        <form:input path="vastDTO.redirectUrl"/>
                        <form:errors path="vastDTO.redirectUrl">
                            <span class="help-inline"><form:errors path="vastDTO.redirectUrl"/></span>
                        </form:errors>
                    </div>
                </div>

                <div class="control-group" style="display: none">
                    <label class="control-label">Тип медиа </label>

                    <div class="controls">
                        <form:input path="vastDTO.mediaType" readonly="true"/>
                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label">Можно пропустить </label>

                    <div class="controls">
                        <form:checkbox path="vastDTO.skippable"/>
                    </div>
                </div>

                <div class="control-group" style="display: none">
                    <label class="control-label">Длительность </label>

                    <div class="controls">
                        <form:input path="vastDTO.duration" readonly="true"/>
                    </div>
                </div>

                <div class="control-group" style="display: none">
                    <label class="control-label">Ширина </label>

                    <div class="controls">
                        <form:input path="vastDTO.width" readonly="true"/>
                    </div>
                </div>

                <div class="control-group" style="display: none">
                    <label class="control-label">Высота </label>

                    <div class="controls">
                        <form:input path="vastDTO.height" readonly="true"/>
                    </div>
                </div>

                <div class="control-group <form:errors path="advertisingDTO.buttonText">error</form:errors>">
                    <label class="control-label">Текст кнопки </label>

                    <div class="controls">
                        <form:input path="advertisingDTO.buttonText"/>
                        <form:errors path="advertisingDTO.buttonText">
                            <span class="help-inline"><form:errors path="advertisingDTO.buttonText"/></span>
                        </form:errors>
                    </div>
                </div>

                <div class="control-group <form:errors path="advertisingDTO.campaignId"> error</form:errors>">
                    <label class="control-label">
                        Проект
                        <form:checkbox class="js-radio-checkbox" path="advertisingDTO.campaignChecked"/>
                    </label>

                    <div class="controls js-campaign-controls">
                        <form:input path="advertisingDTO.campaignId"/>
                        <form:errors path="advertisingDTO.campaignId">
                            <span class="help-inline"><form:errors path="advertisingDTO.campaignId"/></span>
                        </form:errors>
                        <span class="help-block">Реклама будет показываться пока проект активен</span>
                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label">
                        Дата
                        <form:checkbox path="advertisingDTO.dateChecked" class="js-radio-checkbox"/>
                    </label>
                    <%--                    <span>${hf:toJson(advertisingDTO)}</span>--%>
                    <div class="controls js-date-controls">
                        <form:input type="hidden" path="advertisingDTO.dateFrom"/>
                        <form:input type="hidden" path="advertisingDTO.dateTo"/>

                        <div class="js-datepicker-container">
                            <c:set value="${advertisingDTO.dateFrom}" var="dateFrom"/>
                            <c:set value="${advertisingDTO.dateTo}" var="dateTo"/>

                            <%@include file="date-range-picker.jsp" %>
                        </div>
                        <span class="help-block">Реклама будет показываться в этот интервал времени</span>
                    </div>

                </div>

                <div class="control-group" style="display: none">
                    <label class="control-label">Описание</label>

                    <div class="controls">
                        <form:textarea path="vastDTO.description"></form:textarea>
                    </div>

                </div>

                <div class="control-group">
                    <label class="control-label">Статус</label>

                    <div class="controls">
                        <form:select path="advertisingDTO.state">
                            <form:options items="${enumValues}"/>
                        </form:select>
                    </div>
                    <form:input path="vastDTO.deliveryType" type="hidden"/>
                </div>

                <div class="control-group">
                    <div class="controls">
                        <button type="submit" class="btn btn-primary js-save-button" disabled>Сохранить</button>
                        <span id="js-processing-text" style="display:none"><img
                                src="https://${hf:getStaticBaseUrl("")}/images/planeta/notif-loader.gif"/> Идет обработка...</span>
                        <span class="js-error-text alert alert-error"
                              style="display:none">Ошибка обработки, загрузите файл снова.</span>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

</body>


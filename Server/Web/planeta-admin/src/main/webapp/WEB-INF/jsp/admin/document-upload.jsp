<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ include file="/WEB-INF/jsp/includes/generated/doctype-header.jsp" %>
<head>
    <title>Загрузить документ</title>
    <%@ include file="head.jsp" %>

    <script type="text/javascript">

        $(document).ready(function () {

            var DEFAULT_DOCUMENT_OWNER_PROFILE_ID = 21765;
            var profileId = DEFAULT_DOCUMENT_OWNER_PROFILE_ID;

            $('.upload-file').bind('click', function () {
                UploadController.showUploadFiles(profileId, function (filesUploaded) {
                    if (_.isEmpty(filesUploaded)) return;

                    var images = _.map(filesUploaded.models, function (fileUploaded) {
                        return fileUploaded.get('uploadResult');
                    });

                    location.href = location.pathname + "?"+ $.param({
                        limit: images.length,
                        offset: 0
                    });
                });
            });

            $('.deleteByProfileId').on('click', function (e) {

                if(!confirm('Удалить фото?')){
                    return;
                }
                var jPhoto = $(e.target).closest('[data-photo-id]');
                var photoId = jPhoto.attr('data-photo-id');

                var options = {
                    url: '/api/profile/deleteByProfileId-photo.json',
                    data: {
                        profileId: DEFAULT_DOCUMENT_OWNER_PROFILE_ID,
                        photoId: photoId
                    },
                    context: this,
                    success: function (response) {
                        if (response.success) {
                            jPhoto.remove();
                            workspace.appView.showSuccessMessage('Документ удален', 2500);
                        } else {
                            workspace.appView.showErrorMessage(response.errorMessage, 2500);
                        }
                    }
                };

                Backbone.sync('delete', this, options);
            });
        })
    </script>
</head>
<body>

<%@ include file="/WEB-INF/jsp/admin/navbar.jsp" %>

<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Загрузка документов</h1>
        </div>
    </div>

    <div class="main-page-actions">
        <button class="btn btn-primary btn-circle btn-outline btn-lg upload-file"
                title="Загрузить документы">
            <i class="fa fa-upload"></i>
        </button>
    </div>

    <div class="row">
        <div class="col-lg-12 admin-table">

            <table class="table table-bordered table-stripped">
                <thead>
                    <th>Ссылка на документ</th>
                    <th>Добавил</th>
                    <th>Описание</th>
                    <th>Добавлен</th>
                    <th></th>
                </thead>

                <tbody>
                    <c:forEach items="${filesUploadedBefore}" var="file">
                        <tr>
                            <td>
                                <c:if test="${empty file.title}">
                                    <a href="${file.fileUrl}">${file.name}</a>
                                </c:if>
                                <c:if test="${not empty file.title}">
                                    <a href="${file.fileUrl}">${file.title} [.${file.extension}]</a>
                                </c:if>
                            </td>
                            <td><a href="${mainAppUrl}/${file.authorId}" target="_blank">${file.authorId}</a></td>
                            <td>${hf:escapeHtml4(file.description)}</td>
                            <td>${hf:dateFormat(file.timeAdded)}</td>
                            <td class="text-right">
                                <a href="/moderator/document.html?fileId=${file.fileId}&profileId=${file.profileId}"
                                   class="btn btn-primary btn-outline"
                                   title="Редактировать">
                                    <i class="fa fa-pencil"></i>
                                </a>
                            </td>
                        </tr>
                    </c:forEach>
                </tbody>
            </table>

            <%--//TODO новая админка: сделать пагинатор--%>
        </div>
    </div>
</div>
</body>


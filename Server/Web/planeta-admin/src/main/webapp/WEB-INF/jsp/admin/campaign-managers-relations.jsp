<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ include file="/WEB-INF/jsp/includes/generated/doctype-header.jsp" %>
<head>
    <%@ include file="head.jsp" %>
    <style type="text/css">

        .form-control a {
            box-sizing: border-box;
        }

        .form-control {
            margin-right: auto;
            margin-left: auto;
            /*width: 100%;*/
            display: block !important;
            margin-bottom: 1px;
            margin-top: 1px;
        }

        input.form-control {
            margin-left: 2px;
            width: 95%;
        }

        .w100 {
            width: 100%;
        }

        .w50 {
            width: 50%;
        }

        .w45 {
            width: 45%;
        }

        .w75 {
            width: 75%;
        }

        .w25 {
            width: 25%;
        }

        .w30 {
            width: 30%;
        }

        .middle-in-row {
            padding-left: 4px;
            display: block;
            margin-top: 2px;
            margin-bottom: 2px;
            margin-left: 5px;
            padding-top: 4px;
            padding-bottom: 4px;
        }

        .middle-in-row.hoverable:hover {
            margin-top: 1px;
            margin-bottom: 1px;
            margin-left: 4px;
            border: 1px solid #006600;
            border-radius: 4px;
        }

        .bordered-row.edit-mode {
            border-color: #960018;
            background-color: #560008;
        }

        .bordered-row {
            border-top: 1px solid #ffeff3;
            border-bottom: 1px solid #ffeff3;
        }

        .bordered-row.hoverable:hover {
            border-color: #006600;
            background-color: #ddffaa;
        }

        .bordered-row.hoverable.edit-mode:hover {
            border-color: #660000;
            background-color: #ff6633;
        }

        .row {
            margin-left: 0px;
        }

        .row [class*="span"]:first-child {
            margin-left: 0px;
        }

        [class*="span"] {
            paddig-left: 8px;
        }

        .cur-p {
            cursor: pointer;
        }

    </style>

    <script id="manager-editable-row-template" type="text/x-jquery-template">
        {{if editMode}}
            <div class="col-lg-3">
                <input type="text" name="name" class="form-control mrg-b-0" value="{{= name}}"/>
            </div>
            <input type="hidden" name="managerId" value="{{= managerId}}"/>
            <div class="col-lg-3">
                <input type="text" name="fullName" class="form-control mrg-b-0" value="{{= fullName}}"/>
            </div>
            <div class="col-lg-3">
                <input type="text" name="email" class="form-control mrg-b-0" value="{{= email}}"/>
            </div>
            <div class="col-lg-3">
                <input type="hidden" name="active" value="{{= !!active}}"/>
                <div class="btn-group text-right">
                    {{if active}}
                        <a class="js-toggle-active-state btn btn-danger">
                            <i class="fa fa-lock"></i> Заблокировать
                        </a>
                    {{else}}
                        <a class="js-toggle-active-state btn btn-primary">
                            <i class="fa fa-unlock"></i> Разблокировать
                        </a>
                    {{/if}}

                    <a class="js-save-changes btn btn-success">
                        <i class="fa fa-floppy-o"></i> Сохранить
                    </a>
                    <a class="js-stop-edit-mode btn btn-default">Отменить</a>
                </div>
            </div>
        {{else}}
            <div class="col-lg-3">
                <span class="middle-in-row hoverable1">  {{= name}} </span>
            </div>
            <div class="col-lg-3">
                <span class="middle-in-row hoverable1">{{= fullName}}</span>
            </div>

            <div class="col-lg-3">
                <span class="middle-in-row hoverable1">{{= email}}</span>
            </div>
            <div class="col-lg-2">
                <span class="middle-in-row">{{if active}}Активен{{else}}Заблокирован{{/if}}</span>
            </div>
            <div class="col-lg-1">
                <div class="btn-group">
                    <a class="js-start-edit-mode btn btn-warning w100">Изменить</a>
                </div>
            </div>
        {{/if}}
    </script>
    <script type="application/javascript">
        var ManagerModel = BaseModel.extend({});
        var ManagerEditableView = BaseView.extend({
            template: "#manager-editable-row-template",
            tagName: 'form',
            className: 'row bordered-row hoverable mrg-b-0',
            attributes: {
                'action': '/moderator/save-manager.html',
                'method': 'post'
            },
            events: {
                'click .js-save-changes:not(.disabled)': 'saveChanges',
                'dblclick': 'startEditMode',
                'click .js-start-edit-mode': 'startEditMode',
                'click .js-stop-edit-mode': 'stopEditMode',
                'click .js-toggle-active-state': 'toggleActiveState'
            },
            startEditMode: function () {
                this.model.set({
                    editMode: true
                });
                this.$el.addClass('edit-mode')
            },
            stopEditMode: function () {
                this.model.set({
                    editMode: false
                });
                this.$el.removeClass('edit-mode')
            },
            toggleActiveState: function () {
                this.model.set('active', !this.model.get('active'));
                this.saveChanges();
            },
            saveChanges: function () {
                this.$el.submit();
            }

        });
        var ManagersListView = BaseListView.extend({
            itemViewType: ManagerEditableView
        });
        $(document).ready(function () {
            var workers = ${hf:toJson(planetaWorkers)};

            new ManagersListView({
                el: '#js-anchor-for-managers-list',
                collection: new BaseCollection(${hf:toJson(campaignManagers)}, {
                    model: ManagerModel
                })
            }).render();

            $(document).on('input', '#profileId', function () {
                var profileId = parseInt($(this).val());

                workers.forEach(function (worker) {
                    if (worker.profileId === profileId) {
                        $('#email').val(worker.email);
                    }
                })
            });
        });
    </script>
</head>

<body>

<%@ include file="/WEB-INF/jsp/admin/navbar.jsp" %>

<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Менеджеры проектов</h1>
        </div>
    </div>

    <div class="row ma-b-30">
        <div class="col-lg-12">
            <div class="panel panel-success">
                <div class="panel-heading">Добавить менеджера</div>
                <div class="panel-body">
                    <form action="/moderator/add-manager.html" method="post">
                        <input type="hidden" name="active" value="true" class="form-control"/>

                        <div class="row ma-b-20">
                            <div class="col-lg-3">
                                <label for="profileId">Профиль</label>
                                <select id="profileId" class="form-control" name="profileId">
                                    <c:forEach var="worker" varStatus="" items="${planetaWorkers}">
                                        <option value="${worker.profileId}" data-email="">${worker.displayName}</option>
                                    </c:forEach>
                                </select>
                            </div>

                            <div class="col-lg-3">
                                <label for="email">Email</label>
                                <input id="email" type="text" name="email" class="form-control" readonly value="${planetaWorkers[0].email}"/>
                            </div>

                            <div class="col-lg-3">
                                <label for="name">Обращение</label>
                                <input id="name" type="text" name="name" class="form-control"/>
                            </div>

                            <div class="col-lg-3">
                                <label for="fullName">Полное имя</label>
                                <input id="fullName" type="text" name="fullName" class="form-control"/>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-12">
                                <button type="submit" class="btn btn-success">Добавить</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="row ma-b-20">
        <div class="col-lg-3">
            <span class="middle-in-row">
            <b>Обращение</b>
            </span>
        </div>
        <div class="col-lg-3">
            <span class="middle-in-row">
            <b>Полное имя</b>
            </span>
        </div>
        <div class="col-lg-3">
            <span class="middle-in-row">
            <b>e-mail</b>
            </span>
        </div>
        <div class="col-lg-2">
            <span class="middle-in-row">
            <b>Статус</b>
            </span>
        </div>
        <div class="col-lg-1">
            <span class="middle-in-row">
            <b>Операции</b>
            </span>
        </div>
    </div>

    <div class="row ma-b-30">
        <div ID="js-anchor-for-managers-list" class="span12"></div>
    </div>

    <hr>

    <div class="row">
        <div class="col-lg-12">
            <div class="mrg-b-30">
                <h2>Распределение менеджеров по категориям проектов</h2>
            </div>
        </div>
    </div>

    <div class="row ma-b-20">
        <div class="col-lg-4">
            <span class="middle-in-row">
            <b>Категория проекта</b>
            </span>
        </div>
        <div class="col-lg-6">
            <span class="middle-in-row">
            <b>Менеджер</b>
            </span>
        </div>
        <div class="col-lg-2 ">

        </div>
    </div>

    <c:forEach var="relation" varStatus="" items="${campaignManagersRelations}">
        <div class="row cur-p">
            <form action="/moderator/change-manager-relation.html" class="mrg-b-0" method="post">
                <input type="hidden" name="tagId" value="${relation.category.id}">
                <input type="hidden" name="managerId" value="">

                <div class="row bordered-row hoverable">
                    <div class="col-lg-4">
                        <span class="middle-in-row"> ${relation.category.name} </span>
                    </div>

                    <div class="col-lg-6">
                        <span class="middle-in-row">${relation.manager.fullName} (<a>${relation.manager.email}</a>)</span>
                    </div>

                    <div class="col-lg-2">
                        <div class="btn-group">
                            <a class="btn dropdown-toggle btn-danger form-control" data-toggle="dropdown">
                                <i class="icon icon-fire icon-white"></i>
                                Изменить
                                <span class="caret"></span>
                            </a>

                            <%--//TODO новая админка: переделать на человеческий селект--%>
                            <ul class="dropdown-menu" style="top:auto;">
                                <c:forEach var="manager" varStatus="" items="${campaignManagersActive}">
                                    <c:if test="${relation.manager.managerId != manager.managerId}">
                                        <li>
                                            <a class="js-change-manager"
                                               data-manager-id="${manager.managerId}">${manager.fullName}</a>
                                        </li>
                                    </c:if>
                                </c:forEach>
                            </ul>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </c:forEach>
</div>
<script type="application/javascript">
    $(document).ready(function () {
        $('.js-change-manager').click(function (e) {
            var managerId = $(e.currentTarget).data('managerId');
            var $form = $(e.currentTarget).closest('form');
            $form.find('[name=managerId]').val(managerId);
            $form.submit();
        });
    });
</script>

</body>
</html>


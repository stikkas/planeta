<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ include file="/WEB-INF/jsp/includes/generated/doctype-header.jsp" %>
<%@taglib prefix="p" uri="http://planeta.ru/tags" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags/form" %>

<head>
    <%@ include file="/WEB-INF/jsp/admin/head.jsp" %>
</head>

<body>

<%@ include file="/WEB-INF/jsp/admin/navbar.jsp" %>

<div id="page-wrapper">

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                <c:if test="${category.categoryId == 0}">
                    Новая категория
                </c:if>
                <c:if test="${category.categoryId != 0}">
                    Редактировать категорию
                </c:if>
            </h1>
        </div>
    </div>
    <div class="row">
        <dixv class="col-lg-12">
            <s:form id="categoryForm" class="form-horizontal" commandName="category" method="post">
                <input id="categoryId" name="categoryId" type="hidden" value="0">

                <div class="row ma-b-20">
                    <div class="col-lg-3">
                        <label for="value">Название</label>
                        <s:input type="text" name="value" class="form-control" path="value"/>
                        <s:errors path="value" cssClass="error-message my-error" element="span" />
                        <p class="help-block">Название категории русскими буквами</p>
                    </div>

                    <div class="col-lg-3">
                        <label for="mnemonicName">Алиас</label>
                        <s:input type="text" name="mnemonicName" class="form-control" path="mnemonicName"/>
                        <s:errors path="mnemonicName" cssClass="error-message my-error" element="span" />
                        <p class="help-block">Название категории БОЛЬШИМИ английскими буквами</p>
                    </div>

                    <div class="col-lg-3">
                        <label for="preferredOrder">Порядковый номер</label>
                        <s:input type="text" name="preferredOrder" class="form-control" path="preferredOrder"/>
                        <s:errors path="preferredOrder" cssClass="error-message my-error" element="span" />
                        <p class="help-block">Не обязательное поле</p>
                    </div>

                    <div class="col-lg-3">
                        <label for="tagType">Тип</label>
                        <select id="tagType" name="tagType" class="form-control">
                            <c:forEach items="${tagStatuses}" var="status">
                                <c:if test="${category.tagType == status}">
                                    <option selected="selected">${status}</option>
                                </c:if>
                                <c:if test="${category.tagType != status}">
                                    <option>${status}</option>
                                </c:if>
                            </c:forEach>
                        </select>
                    </div>
                </div>

                <div class="btn-group">
                    <c:if test="${category.categoryId == 0}">
                        <button type="submit" formaction="/moderator/shop/save-product-tag.html" class="btn btn-primary">
                            Добавить
                        </button>
                    </c:if>
                    <c:if test="${category.categoryId != 0}">
                        <button type="submit" formaction="/moderator/shop/save-product-tag.html?categoryId=${category.categoryId}" class="btn btn-primary">
                            Сохранить
                        </button>
                    </c:if>
                    <a href="/moderator/shop/product-tags.html" class="btn btn-default">
                        Назад
                    </a>
                </div>
            </s:form>
        </div>
    </div>
</div>
</body>
</html>


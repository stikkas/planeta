<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>


<div class="panel panel-primary">
    <div class="panel-heading">Поиск занятия</div>

    <div class="panel-body">
        <form method="get" action="" class="clearfix" id="stat-params">
            <div class="row ma-b-20">
                <div class="col-lg-12">
                    <input
                            id="xlInput"
                            class="form-control"
                            type="text"
                            placeholder="Введение в крадфаундинг  - введите ID, короткий адрес, или любое слово для поиска"
                            name="searchString"
                            value="${searchString}"
                    >
                    <input type="hidden" name="offset" value="0"/>
                    <input type="hidden" name="limit" value="10"/>
                </div>

            </div>

            <div class="row ma-b-20">
                <div class="col-lg-12">
                    <ct:date-range-picker dateFrom="${dateFrom}" dateTo="${dateTo}" allButton="true"/>
                </div>
            </div>

            <button class="btn btn-primary">
                <i class="fa fa-search"></i> Поиск
            </button>
        </form>
    </div>
</div>
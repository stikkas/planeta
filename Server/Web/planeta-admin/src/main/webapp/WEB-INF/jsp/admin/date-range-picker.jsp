<script>
    $(document).ready(function () {
        var from = new Date(2010, 0, 1);
        var to = new Date(2100, 0, 1);

        var $dataTimePickerFrom = $("#datetimepicker-from");
        $dataTimePickerFrom.datetimepicker({
            locale: 'ru',
            format: 'DD.MM.YYYY'
        });

        $dataTimePickerFrom.on('dp.change', function (event) {
            $('input[name="dateFrom"]').val(event.date.format('x'));
        });

        var dateFrom = $('input[name="dateFrom"]').val() || from.getTime();
        if (dateFrom) {
            $dataTimePickerFrom.data("DateTimePicker").date(new Date(+dateFrom));
        }

        var $dataTimePickerTo = $("#datetimepicker-to");
        $dataTimePickerTo.datetimepicker({
            locale: 'ru',
            format: 'DD.MM.YYYY'
        });
        $dataTimePickerTo.on('dp.change', function (event) {
            $('input[name="dateTo"]').val(event.date.format('x'));
        });

        var dateTo = $('input[name="dateTo"]').val() || to.getTime();
        if (dateTo) {
            $dataTimePickerTo.data("DateTimePicker").date(new Date(+dateTo));
        }

        // datepicker toggle
        $('#date-range-add-on').bind('click', function () {
            if ($(this).hasClass('disabled')) {
                return;
            }
            $('#datepicker-calendar').toggle();
            if ($('#datepicker-calendar').css('display') == 'none') {
                $('#stat-params').submit();
            }
        });

        function buttonClick() {
            var $this = $(this);
            var dates = $this.data('dates');
            $dataTimePickerFrom.data("DateTimePicker").date(dates[0]);
            $dataTimePickerTo.data("DateTimePicker").date(dates[1]);
            $('input[name="dateFrom"]').val(dates[0].getTime());
            $('input[name="dateTo"]').val(dates[1].getTime());
            $this.closest('form').submit();
        }

        var today = new Date();
        var yesterday = new Date(today.getFullYear(), today.getMonth(), today.getDate() - 1);

        $('#todayButton').click(buttonClick).data('dates', [today, today]);
        $('#yesterdayButton').click(buttonClick).data('dates', [yesterday, yesterday]);
        $('#7dayButton').click(buttonClick).data('dates', [new Date(today.getFullYear(), today.getMonth(), today.getDate() - 7), today]);
        $('#30dayButton').click(buttonClick).data('dates', [new Date(today.getFullYear(), today.getMonth(), today.getDate() - 30), today]);
        $('#monthButton').click(buttonClick).data('dates', [new Date(today.getFullYear(), today.getMonth(), 1), new Date(today.getFullYear(), today.getMonth() + 1, 0)]);
        $('#lastMonthButton').click(buttonClick).data('dates', [new Date(today.getFullYear(), today.getMonth() - 1, 1), new Date(today.getFullYear(), today.getMonth(), 0)]);
        $('#allTimeButton').click(buttonClick).data('dates', [from, to]);

        $('#date-range-field').change(function () {
            $('.btn').removeClass('active');
        });

        updateButtonsStates();
    });

    function dateEquals(d1, d2) {
        return d1.getYear() === d2.getYear() && d1.getMonth() === d2.getMonth() && d1.getDate() === d2.getDate();
    }

    function updateButtonsStates() {
        var datesS = [$('input[name="dateFrom"]').val(), $('input[name="dateTo"]').val()];
        var dates = {
            0: new Date(+datesS[0]),
            1: new Date(+datesS[1])
        };

        $('#todayButton,#yesterdayButton,#7dayButton,#30dayButton,#monthButton,#lastMonthButton,#allTimeButton').each(function () {
            var $this = $(this);
            $this.removeClass('active');
            var dt = $this.data('dates');
            console.log(dt);
            if (dt && dateEquals(dt[0], dates[0]) && dateEquals(dt[1], dates[1])) {
                $this.addClass('active');
            }
        });
    }

    function addDays(date, days) {
        return new Date(date.getTime() + days * (1000 * 60 * 60 * 24));
    }
</script>

<div class="col-lg-2">
    <div class="form-group">
        <div class='input-group date' id='datetimepicker-from'>
            <input id="date-box-from" value='' type='text' class="form-control" />
            <span class="input-group-addon">
                <span class="glyphicon glyphicon-calendar"></span>
            </span>
        </div>
    </div>
</div>

<div class="col-lg-2">
    <div class="form-group">
        <div class='input-group date' id='datetimepicker-to'>
            <input id="date-box-to" value='' type='text' class="form-control" />
            <span class="input-group-addon">
                <span class="glyphicon glyphicon-calendar"></span>
            </span>
        </div>
    </div>
</div>

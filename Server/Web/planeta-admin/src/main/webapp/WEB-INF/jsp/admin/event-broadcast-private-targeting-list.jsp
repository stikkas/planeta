<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ include file="/WEB-INF/jsp/includes/generated/doctype-header.jsp" %>

<head>
    <%@ include file="head.jsp" %>

    <link type="text/css" rel="stylesheet" href="/css/ui-lightness/jquery-ui-1.10.2.custom.min.css"/>
    <link type="text/css" rel="stylesheet" href="/css/jquery.timepicker.css"/>

</head>
<body>

<%@ include file="/WEB-INF/jsp/admin/navbar.jsp" %>

<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Ссыдки для просмотра трансляции
                <a href="/moderator/event-broadcast-info.html?broadcastId=${broadcast.broadcastId}">
                    № ${broadcast.broadcastId}
                </a>
            </h1>
        </div>
    </div>


    <div class="main-page-actions">
        <a class="btn btn-success btn-outline btn-circle btn-lg"
           href="/moderator/event-broadcast-private-targeting-add.html?broadcastId=${broadcast.broadcastId}"
            title="Добавить ссылки">
            <i class="fa fa-plus"></i>
        </a>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <table class="table table-striped table-hover table-bordered">
                <thead>
                <tr>
                    <th>Емейл юзера</th>
                    <th>Временная ссылка</th>
                    <th>Время перехода по ссылке</th>
                    <th>Время действия ссылки</th>
                </tr>
                </thead>
                <tbody>
                <c:forEach var="broadcastPrivateTargeting" items="${broadcastPrivateTargetings}">
                    <tr>
                        <td>
                                ${broadcastPrivateTargeting.email}
                        </td>
                        <td>
                                ${hf:constructPrivateBroadcastLink(broadcastPrivateTargeting.broadcastId, broadcastPrivateTargeting.generatedLink)}
                        </td>
                        <td>
                            <c:choose>
                                <c:when test="${broadcastPrivateTargeting.firstVisitTime == null}">
                                    Пользователь ещё не заходил
                                </c:when>
                                <c:otherwise>
                                    <fmt:formatDate value="${broadcastPrivateTargeting.firstVisitTime}"
                                                    pattern="dd/MM/yyyy HH:mm:ss"/>
                                </c:otherwise>
                            </c:choose>
                        </td>
                        <td>
                            <c:choose>
                                <c:when test="${broadcastPrivateTargeting.firstVisitTime == null}">
                                    через ${broadcastPrivateTargeting.validInHours} час(a/ов) после захода
                                </c:when>
                                <c:otherwise>
                                    до <fmt:formatDate value="${broadcastPrivateTargeting.expirationTime}"
                                                       pattern="dd/MM/yyyy HH:mm:ss"/>
                                </c:otherwise>
                            </c:choose>
                        </td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>
    </div>
</body>
</html>


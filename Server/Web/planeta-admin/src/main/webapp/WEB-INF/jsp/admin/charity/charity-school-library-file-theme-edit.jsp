<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags/form" %>
<%@ include file="/WEB-INF/jsp/includes/generated/doctype-header.jsp" %>
<head>
<%@include file="../head.jsp" %>
</head>
<body>

<%@ include file="/WEB-INF/jsp/admin/navbar.jsp" %>

<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                <c:if test="${libraryFileTheme.themeId == null || libraryFileTheme.themeId == 0}">
                    Добавление новой темы
                </c:if>
                <c:if test="${libraryFileTheme.themeId > 0}">
                    Редактирование темы
                </c:if>
            </h1>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <s:form class="form-horizontal" commandName="libraryFileTheme" method="post">
                <div class="row ma-b-20">
                    <div class="col-lg-12">
                        <label class="control-label">Заголовок</label>
                        <div class="controls">
                            <s:textarea path="themeHeader" type="text" placeholder="" class="form-control" required="" rows="4"/>
                            <s:errors path="themeHeader" cssClass="error-message" element="span" />
                        </div>
                    </div>
                    <s:input path="themeId" type="hidden" />
                </div>

                <div class="btn-group">
                    <button formaction="/admin/charity/school-library-theme-edit.html" class="btn btn-primary">
                        <c:if test="${libraryFileTheme.themeId == null || libraryFileTheme.themeId == 0}">
                            Добавить
                        </c:if>
                        <c:if test="${libraryFileTheme.themeId > 0}">
                            Сохранить
                        </c:if>
                    </button>
                    <a href="/admin/charity/school-library-themes.html" class="btn btn-default">Отмена</a>
                </div>
            </s:form>
        </div>
    </div>
</body>
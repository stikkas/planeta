<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ include file="/WEB-INF/jsp/includes/generated/doctype-header.jsp" %>
<head>
    <%@include file="head.jsp" %>
    <c:if test="${shortLink.shortLinkId != 0}">
        <script type="text/javascript">
            $(function () {
                $('.js-copy').click(function (e) {
                    e.preventDefault();

                    var succeeded = false;
                    var input = $('#js-link-to-copy');
                    input.select(); // Select the input node's contents
                    try {
                        // Copy it to the clipboard
                        succeeded = document.execCommand("copy");
                    } catch (e) {
                        succeeded = false;
                    }
                    if (succeeded) {
                        console.log("Скопировано");
                        workspace.appView.showSuccessMessage("Скопировано");
                    }
                });
            });

        </script>
    </c:if>
</head>
<body>

<%@ include file="/WEB-INF/jsp/admin/navbar.jsp" %>

<div id="page-wrapper">
    <div class="row">
        <div class="span12">
            <ul class="breadcrumb">
                <%--Хлебные крошки нужны ибо это боль каждый раз лезть во второй уровень меню--%>
                <li><a href="/">Главная</a> <span class="divider"></span></li>
                <li><a href="/admin/short-links-list.html">Редиректы</a> <span class="divider"></span></li>
                <li class="active">
                    <c:if test="${shortLink.shortLinkId == 0}">
                        Новое правило
                    </c:if>
                    <c:if test="${shortLink.shortLinkId != 0}">
                        Редактирование
                    </c:if>
                </li>
            </ul>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                <c:if test="${shortLink.shortLinkId == 0}">
                    Новое правило
                </c:if>
                <c:if test="${shortLink.shortLinkId != 0}">
                    Редактирование <c:if test="${isItExtraSocialShortLink}">[социальная ссылка]</c:if>
                </c:if>
            </h1>
        </div>
    </div>

    <%--//TODO новая админка: переделать эту хуету--%>
    <c:if test="${shortLink.shortLinkId != 0}">
        <div class="main-page-actions">
            <c:if test="${shortLink.shortLinkStatus == 'OFF'}">
                <button class="btn btn-success btn-circle btn-outline btn-lg on-off" title="Включить">
                    <i class="fa fa-play"></i>
                </button>
            </c:if>
            <c:if test="${shortLink.shortLinkStatus == 'ON'}">
                <button class="btn btn-danger btn-circle btn-outline btn-lg on-off" title="Выключить">
                    <i class="fa fa-stop"></i>
                </button>
            </c:if>
        </div>
    </c:if>

    <div class="row">
        <div class="col-lg-12">
            <form:form commandName="shortLink" class="form-horizontal">
                <form:hidden id="shortLinkId" path="shortLinkId"/>

                <%--//TODO новая админка: переделать эту хуету--%>
                <form:hidden id="switchOff" path="shortLinkStatus"/>

                <div class="row ma-b-20">
                    <div class="col-lg-6">
                        <label for="edit_title">Короткая ссылка</label>
                        <form:input type="text" path="shortLink" id="edit_title" class="form-control"/>
                        <form:errors path="shortLink" cssClass="error"/>
                        <p class="help-block">Введите слово example чтобы получить короткую ссылку вида ${redirectUrlHost}/example</p>
                    </div>

                    <div class="col-lg-6">
                        <label for="redirectAddressUrl">Адрес перехода</label>
                        <form:input path="redirectAddressUrl" id="redirectAddressUrl" class="form-control"/>
                        <p class="help-block">Полный путь куда должен вести редирект</p>
                        <form:errors path="redirectAddressUrl" cssClass="error"/>
                    </div>
                </div>

                <div class="row ma-b-20">
                    <div class="col-lg-6">
                        <label for="cookieName">Имя куки</label>
                        <form:input path="cookieName" id="cookieName" class="form-control"/>
                        <p class="help-block">кука с таким именем проставится пользователю на месяц</p>
                        <form:errors path="cookieName" cssClass="error"/>
                    </div>

                    <div class="col-lg-6">
                        <label for="cookieValue">Значение куки</label>
                        <form:input path="cookieValue" id="cookieValue" class="form-control"/>
                        <form:errors path="cookieValue" cssClass="error"/>
                    </div>
                </div>

                <c:if test="${shortLink.shortLinkId != 0}">
                    <div class="row">
                        <div class="col-lg-6">
                            <label for="edit_title">Ссылка будет такая</label>

                            <div class="input-group">
                                <input id="js-link-to-copy"
                                       type="text"
                                       class="form-control"
                                       value="${redirectUrlHost}/${shortLink.shortLink}"
                                       readonly>

                                <span class="input-group-btn">
                                    <button class="btn btn-info js-copy" title="Скопировать">
                                        <i class="fa fa-files-o"></i>
                                    </button>
                                </span>
                            </div>

                            <a href="${redirectUrlHost}/${shortLink.shortLink}" target="_blank">Перейти по ссылке</a>
                        </div>
                    </div>
                </c:if>

                <div class="btn-group">
                    <button type="submit" formaction="/admin/save-short-link.html" class="btn btn-primary">
                        <c:if test="${shortLink.shortLinkId == 0}">Добавить</c:if>
                        <c:if test="${shortLink.shortLinkId != 0}">Сохранить</c:if>
                    </button>

                    <a href="/admin/short-links-list.html" class="btn btn-default">Отмена</a>
                </div>

            </form:form>
        </div>
    </div>

    <c:if test="${shortLink.shortLinkId != 0 && !isItExtraSocialShortLink}">
    <hr>
    <div class="row">
        <div class="col-lg-3">
            <form:form method="get" commandName="shortLink" class="form-horizontal">
                <form:hidden id="shortLinkId" path="shortLinkId"/>
                <div class="btn-group">
                    <button type="submit" formaction="/admin/generate-social-short-links.html" class="btn btn-basic">
                        Сгенерировать недостающие ссылки для соцсетей
                    </button>
                </div>
            </form:form>
        </div>
        <div class="col-lg-3">
            <a href="/admin/configuration-edit.html?configurationKey=shortlink.extra.social.links"
               class="btn btn-default" title="Изменить" target="_blank">
                Редактировать список для генерации
                <i class="fa fa-edit"></i>
            </a>
        </div>
    </div>
    </c:if>
    <c:if test="${existsSocialShortLinksByBasename != null}">
    <br>
    <div class="row">
        <div class="col-lg-12 admin-table">
            <table class="table table-striped table-hover table-bordered">
                <c:forEach var="existsSocialShortLink" items="${existsSocialShortLinksByBasename}">
                    <tr>
                        <td>
                            <b>${existsSocialShortLink.shortLinkId}</b>
                        </td>
                        <td>
                            <a href="${redirectUrlHost}/${existsSocialShortLink.shortLink}">${existsSocialShortLink.shortLink}</a>
                        </td>
                        <td>
                            <a href="${existsSocialShortLink.redirectAddressUrl}">${existsSocialShortLink.redirectAddressUrl}</a>
                        </td>
                        <td class="text-right">
                            <form method="post" style="display:inline">
                                <input type="hidden" name="shortLinkId" value="${existsSocialShortLink.shortLinkId}"/>
                                <input type="hidden" name="shortLinkStatus" value="${existsSocialShortLink.shortLinkStatus}"/>

                                <div class="admin-table-actions">
                                    <a href="/admin/edit-short-link.html?shortLinkId=${existsSocialShortLink.shortLinkId}"
                                       class="btn btn-primary btn-outline"
                                       title="Редактировать">
                                        <i class="fa fa-pencil"></i>
                                    </a>
                                    <%--
                                    <button type="submit"
                                            formaction="/admin/deleteByProfileId-short-link.html"
                                            class="btn btn-danger btn-outline"
                                            title="Удалить">
                                        <i class="fa fa-trash"></i>
                                    </button>--%>
                                </div>
                            </form>
                        </td>
                    </tr>
                </c:forEach>
            </table>
        </div>
    </div>
    </c:if>

    <script type="text/javascript">
        (function () {

            $('.on-off').click(function (e) {
                e.preventDefault();
                var switcher = $('#switchOff');
                var value = (switcher.val() == 'ON');
                switcher.val(value ? 'OFF' : 'ON');
                var name = !value ? "Включить" : "Выключить";
                $(e.target).html(name);
            });

            $('.copy-target').click(function (e) {
                e.preventDefault();
                $('#shortLinkId').val(0);
                var text = $('#edit_title').val();
                text += "[копия]";
                $('#edit_title').val(text);
                $(e.target).hide();
            });
        })();
    </script>
</div>
</body>
</html>


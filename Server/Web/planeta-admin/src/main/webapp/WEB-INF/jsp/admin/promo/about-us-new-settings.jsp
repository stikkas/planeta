<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ include file="/WEB-INF/jsp/includes/generated/doctype-header.jsp" %>
<head>
    <title>О нас [НОВОЕ]</title>
    <%@ include file="../head.jsp" %>
</head>
<body>

<%@ include file="/WEB-INF/jsp/admin/navbar.jsp" %>

<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">О нас [НОВОЕ]</h1>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12 admin-table">
            <table class="table table-striped table-hover table-bordered admin-banner-image">
                <tbody>
                    <tr>
                        <td>
                            <b>На страницу будут выводиться проекты №,№,№</b>
                            <br/>
                            <i><small>укажите id проектов через запятую</small></i>
                        </td>
                        <td>
                            <form id="promo-start-form" style="display:inline" method="post" action="/moderator/about-us-new-settings.html">
                                <div class="form-group input-group">
                                    <input type="text" class="form-control" id="aboutUsCampaignsIds" name="aboutUsCampaignsIds" value="${hf:getMyListAsString(aboutUsCampaignsIds)}">
                                    <span class="input-group-btn">
                                        <button id="search" class="btn btn-primary" type="submit" title="Сохранить">
                                            <i class="fa fa-floppy-o"></i>
                                        </button>
                                    </span>
                                </div>
                            </form>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
</body>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ include file="/WEB-INF/jsp/includes/generated/doctype-header.jsp" %>
<%@taglib prefix="p" uri="http://planeta.ru/tags" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags/form" %>

<head>
    <%@ include file="/WEB-INF/jsp/admin/head.jsp" %>
    <link type="text/css" rel="stylesheet" href="/css/ui-lightness/jquery-ui-1.10.2.custom.min.css"/>
</head>
<body>

<%@ include file="/WEB-INF/jsp/admin/navbar.jsp" %>

<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                <c:if test="${house.publishingHouseId == 0}">
                    Новое издательство
                </c:if>
                <c:if test="${house.publishingHouseId != 0}">
                    Редактировать издательство
                </c:if>
            </h1>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <s:form id="publishingHouseForm" class="form-horizontal" commandName="house" method="post">
                <input id="publishingHouseId" name="publishingHouseId" type="hidden" value="${house.publishingHouseId}">

                <div class="row ma-b-20">
                    <div class="col-lg-4">
                        <label for="name">Название</label>
                        <s:input type="text" name="name" class="form-control" path="name"/>
                        <s:errors path="name" cssClass="error-message my-error" element="span" />
                    </div>

                    <div class="col-lg-4">
                        <label for="contractorId">Id контрагента</label>
                        <s:input type="text" name="contractorId" class="form-control" path="contractorId"/>
                        <s:errors path="contractorId" cssClass="error-message my-error" element="span" />
                    </div>
                </div>

                <div class="btn-group">
                    <c:if test="${house.publishingHouseId == 0}">
                        <button type="submit" formaction="/admin/biblio/publishing-house.html" class="btn btn-primary">
                            Добавить
                        </button>
                    </c:if>
                    <c:if test="${house.publishingHouseId != 0}">
                        <button type="submit" formaction="/admin/biblio/publishing-house.html?=${house.publishingHouseId}" class="btn btn-primary">
                            Сохранить
                        </button>
                    </c:if>
                    <a href="/admin/biblio/publishing-houses.html" class="btn btn-default">
                        Назад
                    </a>
                </div>
            </s:form>
        </div>
    </div>
</div>
</body>
</html>

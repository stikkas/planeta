<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ include file="/WEB-INF/jsp/includes/generated/doctype-header.jsp" %>
<head>
    <%@ include file="head.jsp" %>
    <style type="text/css">
        .sort-arrow.sort-arrow-desc{
            float: right;width: 0;height: 0;margin: 0;border: 8px solid transparent;border-top-color: steelblue;
        }
        .sort-arrow.sort-arrow-asc{
            float: right;width: 0;height: 0;margin: -8px 0 0 0;border: 8px solid transparent;border-bottom-color: steelblue;
        }
    </style>
    <script type="text/javascript">
        var contractorId = '${contractorId}' || null;
        $(function() {
            $('[data-camp-id]').each(function() {
                $(this).bind('click', function() {
                    var href = '/moderator/bind-contractor.json?contractorId='+contractorId+'&campaignId='+$(this).data().campId+'&fromContractors=true';
                    var campaignStatus = $(this).data().campStatus;
                    Modal.showConfirm("Текущий контрагент проекта будет заменен", 'Подтверждение действия',{
                        success: function() {
                            document.location.href = href;
                        }
                    });
                })
            });


            var sortOrderList = ${hf:toJson(sortOrderList)};

            function showSortOrder() {
                $('.js-sort-by .sort-arrow').remove();
                var $field = $('.js-sort-order-list-field').empty();
                _.each(sortOrderList, function (sortOrder) {
                    $field.append('<input type="hidden" name="sortOrderList" value="' + sortOrder + '">');
                    var isAsc = sortOrder.indexOf('_ASC') > 0;
                    //remove _ASC or _DESC
                    sortOrder = sortOrder.substr(0, sortOrder.lastIndexOf('_'));
                    var $arrow = $('<div class="sort-arrow"></div>');
                    $arrow.addClass(isAsc > 0 ? 'sort-arrow-asc' : 'sort-arrow-desc');
                    $('.js-sort-by[data-sort-by="' + sortOrder + '"]').prepend($arrow);
                });
            }
            showSortOrder();
            $('.js-sort-by').click(function () {
                var $this = $(this);
                var sortBy = $this.data('sortBy');
                if (sortOrderList[0] && sortOrderList[0].indexOf(sortBy) >= 0 && sortOrderList[0].indexOf("_DESC") > 0) {
                    sortOrderList = [sortBy + "_ASC"];
                } else {
                    sortOrderList = [sortBy + "_DESC"];
                }

                showSortOrder();
                $('form').submit();
            });
        });
    </script>
</head>
<body>
    <c:if test="${empty contractorId}">
        <%@ include file="/WEB-INF/jsp/admin/navbar.jsp" %>
    </c:if>

    <div id="page-wrapper">
        <c:if test="${not empty contractorId}">
            <a class="btn btn-large btn-danger" href="/moderator/contractors.html">
                Отмена
            </a>
        </c:if>

        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Проекты</h1>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <%@ include file="searchBoxCampaigns.jsp" %>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12 admin-table">
                <table class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th width="50">#</th>
                        <th width="50">Фото</th>
                        <th>Название, описание проекта</th>
                        <th>Статус</th>
                        <th class="js-sort-by" data-sort-by="SORT_BY_TIME_ADDED">Дата создания</th>
                        <th class="js-sort-by" data-sort-by="SORT_BY_TIME_STARTED">Дата запуска</th>
                        <th class="js-sort-by" data-sort-by="SORT_BY_TIME_FINISHED">Дата окончания</th>
                        <th>
                            <div class="text-right">Действия</div>
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:forEach var="campaign" items="${campaigns}">
                        <tr>
                            <td>${campaign.campaignId}</td>
                            <td><img src='${hf:getThumbnailUrl(campaign.imageUrl, "USER_SMALL_AVATAR", "PHOTO")}'/>
                            </td>
                            <td>
                                <a class="" title="Страница проекта" href="https://${properties['projects.application.host']}/campaigns/${campaign.campaignId}">
                                    <c:out value="${campaign.name}"/>
                                </a> <br> ${hf:escapeHtml4(campaign.shortDescription)}
                            </td>
                            <td>
                                <c:set var="status" value="${campaign.status}"/>
                                <%@ include file="campaign-statuses.jsp" %>
                            </td>
                            <td>
                                <fmt:formatDate value="${campaign.timeAdded}" pattern="dd.MM.yyyy"/>
                            </td>
                            <td>
                                <fmt:formatDate value="${campaign.timeStart}" pattern="dd.MM.yyyy"/>
                            </td>
                            <td>
                                <fmt:formatDate value="${campaign.timeFinish}" pattern="dd.MM.yyyy"/>
                            </td>

                            <td class="col-lg-1">
                                <c:if test="${empty contractorId}">
                                    <div class="text-right">
                                        <div class="btn-group">
                                            <a class="btn btn-primary btn-outline" href="/moderator/campaign-info.html?campaignId=${campaign.campaignId}" title="Редактирование проекта">
                                                <i class="fa fa-pencil"></i>
                                            </a>

                                            <a class="btn btn-success btn-outline" href="/moderator/campaign-moderation-info.html?campaignId=${campaign.campaignId}" title="Модерация проекта">
                                                <i class="fa fa-lock"></i>
                                            </a>
                                        </div>
                                    </div>
                                </c:if>
                                <c:if test="${not empty contractorId && campaign.statusCode != 3 && campaign.statusCode != 4 && campaign.statusCode != 5 && campaign.statusCode != 10}">
                                    <div class="text-right">
                                        <div class="btn-group">
                                            <a class="btn-success btn-outline" href="javascript:void(0)" data-camp-id="${campaign.campaignId}" data-camp-status="${campaign.status}" title="Прикрепить">
                                                <i class="fa fa-paperclip"></i>
                                            </a>
                                        </div>
                                    </div>
                                </c:if>
                            </td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
            </div>
        </div>
        <%@ include file="paginator.jsp" %>
    </div>
</body>
</html>


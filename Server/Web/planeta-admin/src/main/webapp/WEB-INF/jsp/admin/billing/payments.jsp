<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ include file="/WEB-INF/jsp/includes/generated/doctype-header.jsp" %>
<c:set var="baseUrl" value="${hf:getStaticBaseUrl('')}"/>
<head>
    <%@ include file="../head.jsp" %>

    <script>
        function cancelPayment(id, el) {
            $.post('/admin/billing/cancel-payment.json', {transactionId: id}, function (response) {
                if (response.success) {
                    $(el).hide();
                } else {
                    alert(response.errorMessage);
                }
            })
        }

        function processPayment(id, el) {
            $.post('/admin/billing/process-payment.json', {transactionId: id}, function (response) {
                if (response.success) {
                    $(el).hide();
                } else {
                    alert(response.errorMessage);
                }
            })
        }

        function cancelOrder(orderid, el) {
            if (confirm('Аннулировать заказ?')) {
                $.post("/admin/billing/cancel-order.json", {orderId: orderid}, function (response) {
                    if (response.success) {
                        $(el).hide();
                        alert("Заказ аннулирован");
                    } else {
                        alert(response.errorMessage);
                    }
                })
            }
        }

    </script>

    <style>
        .table td {
            vertical-align: middle;
        }

        .table .badge {
            position: relative;
            left: -10px;
            top: -5px;
        }

        .no-image {
            width: 52px;
            height: 35px;
        }
    </style>
</head>
<body>

<%@ include file="/WEB-INF/jsp/admin/navbar.jsp" %>
<div id="page-wrapper">

    <c:if test="${not empty error}">
        <div class="row">
            <div class="col-lg-12">
                <div class="alert alert-block fade in alert-error">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <legend class="text-error" style="width: 98%">Ошибка совершения операции.</legend>
                    <pre><c:out value="${error}"/></pre>
                </div>
            </div>
        </div>
    </c:if>

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Мониторинг платежных транзакций</h1>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-primary">
                <div class="panel-heading">Поиск платежных транзакций пользователя</div>

                <div class="panel-body">
                    <form id="search-frm" method="GET" action="/admin/billing/payments.html" class="clearfix">
                        <div class="row ma-b-20">
                            <div class="col-lg-6">
                                <label>Пользователь</label>
                                <input class="form-control"
                                       type="text"
                                       placeholder="Введите алиас или ID пользователя"
                                       id="searchStr" name="searchStr" value="${param.searchStr}">
                            </div>

                            <div class="col-lg-3">
                                <label>Проект</label>
                                <select class="form-control" id="projectType" name="projectType">
                                    <option value="">Все</option>
                                    <c:forEach items="${projectTypes}" var="s">
                                        <option value="${s}"
                                                <c:if test="${s eq param.projectType}">selected</c:if>>${s}</option>
                                    </c:forEach>
                                </select>
                            </div>

                            <div class="col-lg-3">
                                <label>Статус</label>
                                <select class="form-control" id="transactionStatus" name="transactionStatus">
                                    <option value="">Все</option>
                                    <c:forEach items="${statuses}" var="s">
                                        <option value="${s}"
                                                <c:if test="${s eq param.transactionStatus}">selected</c:if>>${s}</option>
                                    </c:forEach>
                                </select>
                            </div>
                        </div>

                        <div class="row ma-b-20">
                            <div class="col-lg-6">
                                <label>Заказ</label>
                                <input class="form-control"
                                       type="text"
                                       placeholder="Введите ID заказа для поиска"
                                       id="orderOrTransactionId" name="orderOrTransactionId"
                                       value="${param.orderOrTransactionId}">
                            </div>

                            <div class="col-lg-3">
                                <label>Платежный способ</label>
                                <select class="form-control" id="paymentType" name="paymentMethodId">
                                    <option value="">Все</option>
                                    <c:forEach items="${methodsList}" var="m">
                                        <option value="${m.id}"
                                                <c:if test="${m.id eq param.paymentMethodId}">selected</c:if>>${m.alias}</option>
                                    </c:forEach>
                                </select>
                            </div>

                            <%--//TODO новая админка: при обновлении страницы всегда выбирается планета--%>
                            <div class="col-lg-3">
                                <label>Платежная система</label>
                                <select class="form-control" name="paymentProviderId">
                                    <option value="">Все</option>
                                    <c:forEach items="${providersList}" var="p">
                                        <option value="${p.id}"
                                                <c:if test="${p.id eq param.paymentProviderId}">selected</c:if>>${p.name}</option>
                                    </c:forEach>
                                </select>
                            </div>
                        </div>

                        <div class="row ma-b-20">
                            <div class="col-lg-12">
                                <ct:date-range-picker dateFrom="${dateFrom}" dateTo="${dateTo}" allButton="true"/>
                            </div>
                        </div>

                        <button id="search" class="btn btn-primary">
                            <i class="fa fa-search"></i> Поиск
                        </button>
                        <button id="clear-filter" class="btn btn-default">
                            <i class="fa fa-trash"></i> Очистить
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <c:if test="${empty transactions}">
        <div class="row">
            <div class="col-lg-12">
                <h2 class="text-center">
                    <small>По Вашему запросу ничего не найдено ...</small>
                </h2>
                <hr>
            </div>
        </div>
    </c:if>

    <c:if test="${not empty transactions}">
        <div class="row">
            <div class="col-lg-12 admin-table">
                <table class="table table-hover table-bordered">
                    <thead>
                    <tr>
                        <th>№ платежа</th>
                        <th>Статус</th>
                        <th>Проект</th>
                        <th>Метод</th>
                        <th>Провайдер</th>
                        <th>Пользователь</th>
                        <th>Тип платежа</th>
                        <th>Создание</th>
                        <th>Обработка</th>
                        <c:if test="${isSuperAdmin}">
                            <th>Действие</th>
                        </c:if>
                    </tr>
                    </thead>
                    <tbody>
                    <c:forEach items="${transactions}" var="transaction" varStatus="status">
                        <c:set var="profileEmail" value="${profileEmailByTransactionId[transaction.transactionId]}"/>
                        <c:set var="isOrderBuying" value="${transaction.orderId > 0}"/>
                        <tr class="qa-transaction-container">
                            <td>
                                <a href="/admin/billing/payment.html?paymentId=${transaction.transactionId}"
                                   target="_blank">${transaction.transactionId}</a>
                            </td>
                            <td>
                                <c:choose>
                                    <c:when test="${transaction.status eq 'NEW'}">
                                        <span class="label label-primary">Новая</span>
                                    </c:when>
                                    <c:when test="${transaction.status eq 'DONE'}">
                                        <span class="label label-success">Завершена</span>
                                    </c:when>
                                    <c:when test="${transaction.status eq 'DECLINE'}">
                                        <span class="label label-warning">Отклонена</span>
                                    </c:when>
                                    <c:when test="${transaction.status eq 'ERROR'}">
                                        <span class="label label-danger">Ошибка</span>
                                    </c:when>
                                    <c:when test="${transaction.status == 'CANCELED'}">
                                        <span class="label label-info">Отменен</span>
                                    </c:when>
                                </c:choose>
                            </td>
                            <td>
                                    ${transaction.projectType}
                            </td>
                            <td>
                                <img style="max-width:52px;max-height:52px"
                                     src="//${baseUrl}${methods[transaction.paymentMethodId].imageUrl}"/>
                            </td>
                            <td>
                                <c:choose>
                                    <c:when test="${transaction.paymentProviderId >= 0}">
                                        <img src="//${baseUrl}/images/payment/${providers[transaction.paymentProviderId].type}.gif">
                                    </c:when>
                                    <c:otherwise>
                                        <i class="icon-question-sign"></i>
                                    </c:otherwise>
                                </c:choose>
                            </td>
                            <td>
                                <small>
                                    <div>
                                        Пользователь <a
                                            href="/moderator/user-info.html?profileId=${transaction.profileId}">№ ${transaction.profileId}</a>
                                    </div>
                                    <div>
                                        <a href="mailto:${profileEmail}">${profileEmail}</a>
                                    </div>
                                </small>
                            </td>
                            <td>
                                <small>
                                    <div>
                                        <c:choose>
                                            <c:when test="${isOrderBuying}">
                                                Оплата заказа <br><a
                                                    href="/admin/billing/order.html?orderId=${transaction.orderId}"
                                                    target="_blank">№ ${transaction.orderId}</a>
                                            </c:when>
                                            <c:otherwise>
                                                Пополнение баланса
                                            </c:otherwise>
                                        </c:choose>
                                    </div>
                                    <div style="white-space: nowrap">
                                        на сумму:
                                        <strong class="qa-amount-net"><fmt:formatNumber maxFractionDigits="2"
                                                                                        minFractionDigits="0"
                                                                                        value="${transaction.amountNet}"/></strong>
                                        руб.
                                    </div>
                                </small>
                            </td>
                            <td>
                                <small>
                                    <div>
                                        <fmt:formatDate value="${transaction.timeAdded}" pattern="dd.MM.yyyy"/>
                                    </div>
                                    <div class="muted">
                                        <fmt:formatDate value="${transaction.timeAdded}" pattern="HH:mm"/>
                                    </div>
                                </small>
                            </td>
                            <td>
                                <small>
                                    <div>
                                        <fmt:formatDate value="${transaction.timeUpdated}" pattern="dd.MM.yyyy"/>
                                    </div>
                                    <div class="muted">
                                        <fmt:formatDate value="${transaction.timeUpdated}" pattern="HH:mm"/>
                                    </div>
                                </small>
                            </td>
                            <c:if test="${isSuperAdmin}">
                                <td>
                                    <small>
                                        <c:choose>
                                            <c:when test="${transaction.status == 'NEW' || transaction.status == 'ERROR'}">
                                                <a href="javascript:void(0)"
                                                   onclick="processPayment(${transaction.transactionId}, this);"
                                                   class="js-process">Провести</a>
                                            </c:when>
                                            <c:when test="${transaction.status == 'DONE' && isOrderBuying}">
                                                <!--<a href="javascript:void(0)" onclick="cancelPayment(${transaction.transactionId}, this);" class="js-cancel">Отменить</a>-->
                                                <a href="javascript:void(0)"
                                                   onclick="cancelOrder(${transaction.orderId}, this);"
                                                   class="js-cancel">Аннулировать</a>
                                            </c:when>
                                        </c:choose>
                                    </small>
                                </td>
                            </c:if>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
                <%@ include file="../paginator.jsp" %>

            </div>
        </div>
    </c:if>
</div>

<div id="emulate-confirm-modal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="confirm-title"
     aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="confirm-title">Подтверждение эмуляции</h3>
    </div>
    <div class="modal-body">
        <p>Вы дествительно хотите сэмулировать платеж ?</p>
    </div>
    <div class="modal-footer">
        <button class="btn" data-dismiss="modal">Отмена</button>
        <button class="btn btn-primary" id="emulate">Да</button>
    </div>
</div>

</body>
</html>

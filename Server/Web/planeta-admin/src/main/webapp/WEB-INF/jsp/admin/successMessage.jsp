<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<c:if test="${successMessage != null && fn:length(successMessage)>0}">
    <div class="alert alert-success">
        <button type="button" class="close" data-dismiss="alert">×</button>
        ${successMessage}
    </div>
</c:if>

<c:if test="${errorMessage != null && fn:length(errorMessage)>0}">
    <div class="alert alert-danger">
        <button type="button" class="close" data-dismiss="alert">×</button>
        ${errorMessage}
    </div>
</c:if>


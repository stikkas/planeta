<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ include file="/WEB-INF/jsp/includes/generated/doctype-header.jsp" %>
<head>
    <%@ include file="head.jsp" %>
</head>
<body>
    <%@ include file="/WEB-INF/jsp/admin/navbar.jsp" %>
    <div id="page-wrapper">

        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Трансляции</h1>
            </div>
        </div>


        <div class="row">
            <div class="col-lg-12">
                <%@ include file="searchBoxEvents.jsp" %>
            </div>
        </div>

        <div class="main-page-actions">
            <a class="btn btn-success btn-circle btn-outline btn-lg" href="/moderator/event-broadcast-info.html?broadcastId=0" title="Создать трансляцию">
                <i class="fa fa-plus"></i>
            </a>
        </div>

        <div class="row">
            <div class="col-lg-12 admin-table">
                <table class="table table-striped table-hover table-bordered">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Фото</th>
                            <th>Название</th>
                            <th>Дата начала</th>
                            <th>Дата окончания</th>
                            <th>Попадает на витрину</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                    <tr>
                    </tr>
                    <c:forEach var="broadcast" items="${broadcastList}" varStatus="status">
                        <tr>
                            <td>${broadcast.broadcastId}</td>
                            <td><img src="${hf:getGroupAvatarUrl(broadcast.imageUrl, "USER_SMALL_AVATAR")}" alt="${broadcast.name}"/>
                            </td>
                            <td>
                                <p><a href="https://${properties['tv.application.host']}/broadcast/${broadcast.broadcastId}">${broadcast.name}</a></p>
                            </td>
                            <td><fmt:formatDate value="${broadcast.timeBegin}" pattern="dd.MM.yyyy"/></td>
                            <td><fmt:formatDate value="${broadcast.timeEnd}" pattern="dd.MM.yyyy"/></td>
                            <td>
                                <c:choose><c:when test="${broadcast.visibleOnDashboard}"><b>Да</b></c:when><c:otherwise><span class="muted">Нет</span></c:otherwise></c:choose>
                            </td>
                            <td>
                                <div>
                                    <a class="btn btn-outline btn-primary" href="/moderator/event-broadcast-info.html?broadcastId=${broadcast.broadcastId}" title="Редактировать трансляцию">
                                        <i class="fa fa-pencil"></i>
                                    </a>
                                </div>
                            </td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
            </div>
        </div>

        <%@ include file="paginator.jsp" %>
    </div>
</body>
</html>

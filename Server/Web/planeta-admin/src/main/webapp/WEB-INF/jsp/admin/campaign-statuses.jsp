<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<c:choose>
    <c:when test="${status == 'ALL'}">
        Все
    </c:when>
    <c:when test="${status == \"APPROVED\"}">
        <span class="label label-success">Утверждён</span>
    </c:when>
    <c:when test="${status == \"ACTIVE\"}">
        <span class="label label-primary">Активный</span>
    </c:when>
    <c:when test="${status == \"PAUSED\"}">
        <span class="label label-warning">Пауза</span>
    </c:when>
    <c:when test="${status == \"DRAFT\"}">
        <span class="label label-default">Черновик</span>
    </c:when>
    <c:when test="${status == \"NOT_STARTED\"}">
        <span class="label label-info">На модерации</span>
    </c:when>
    <c:when test="${status == \"PATCH\"}">
        <span class="label label-warning">На доработке</span>
    </c:when>
    <c:when test="${status == \"FINISHED\"}">
        <span class="label label-success">Завершен</span>
    </c:when>
    <c:when test="${status == \"DELETED\"}">
        <span class="label label-danger">Удален</span>
    </c:when>
    <c:when test="${status == \"DECLINED\"}">
        <span class="label label-danger">Отклонен</span>
    </c:when>
    <c:otherwise>
        Другое
    </c:otherwise>
</c:choose>

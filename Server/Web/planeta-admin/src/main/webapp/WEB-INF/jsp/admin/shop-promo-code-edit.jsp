<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ include file="/WEB-INF/jsp/includes/generated/doctype-header.jsp" %>
<%@taglib prefix="p" uri="http://planeta.ru/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html>
<head>
    <title>Редактор промокода</title>
    <%@ include file="/WEB-INF/jsp/admin/head.jsp" %>
    <link type="text/css" rel="stylesheet" href="/css/ui-lightness/jquery-ui-1.10.2.custom.min.css"/>
    <link type="text/css" rel="stylesheet" href="/css/jquery.timepicker.css"/>

    <style>
        label {
            padding-top: 10px;
        }

        input {
            padding-top: 10px;
        }

        .error {
            color: red;
            padding-left: 20px;
        }

        .info {
            color: grey;
            font-size: small;
        }

        .success {
            color: green;
            font-size: large;
        }
    </style>

    <script type="text/javascript">
        $(document).ready(function () {
            $("#date-box-begin").datetimepicker();

            <c:if test="${not empty promoCode.timeBegin.time}">
                $("#date-box-begin").data("DateTimePicker").date(new Date(${promoCode.timeBegin.time}));
            </c:if>

            $("#date-box-end").datetimepicker();
            <c:if test="${not empty promoCode.timeEnd.time}">
                $("#date-box-end").data("DateTimePicker").date(new Date(${promoCode.timeEnd.time}));
            </c:if>

            $("#js-promo-code-gen").click(function () {
                    $.get("/moderator/shop/generate-promo-code.json?count=1").done(function (data) {
                        if (data && data.result) {
                            $("#code").val(data.result);
                        }
                    });
                }
            );

            $("#promo-code-form").submit(function () {
                var dateStart = $("#date-box-begin").data("DateTimePicker").date().format('x');
                if (dateStart) {
                    $("#timeBegin").val(dateStart);
                }

                var dateRegistrationEnd = $("#date-box-end").data("DateTimePicker").date().format('x');
                if (dateRegistrationEnd) {
                    $("#timeEnd").val(dateRegistrationEnd);
                }

                return true;
            });
        });

    </script>
</head>

<body>

<%@ include file="/WEB-INF/jsp/admin/navbar.jsp" %>

<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                <c:choose>
                    <c:when test="${promoCode.promoCodeId > 0}">
                        Редактирование промокода №${promoCode.promoCodeId}
                    </c:when>
                    <c:otherwise>
                        Добавление промокода
                    </c:otherwise>
                </c:choose>
            </h1>
        </div>
    </div>

    <p:if test="${success}">
        <span class="success">Сохранено успешно.</span><br/>
    </p:if>

    <div class="row">
        <div class="col-lg-12">
            <form:form
                    id="promo-code-form"
                    commandName="promoCode"
                    class="form-horizontal"
                    method="post">
                <form:input type="hidden" path="promoCodeId"/>
                <!--<form:input type="hidden" path="timeAdded"/>-->

                <div class="row">
                    <div class="col-lg-12">
                        <label>Название</label>
                        <form:input type="text" path="title" cssClass="form-control"/>
                        <form:errors path="title" cssClass="error"/>
                    </div>
                </div>


                <h3>Скидки</h3>

                <div class="row">
                    <div class="col-lg-6">
                        <label>Тип скидки</label>
                        <form:select path="discountType" cssClass="form-control">
                            <option value="ABSOLUTE"
                                    <c:if test="${promoCode.discountType == \"ABSOLUTE\"}">selected="selected"</c:if>>
                                Абсолютная (рубли)
                            </option>
                            <option value="RELATIVE"
                                    <c:if test="${promoCode.discountType == \"RELATIVE\"}">selected="selected"</c:if>>
                                Относительная (проценты)
                            </option>
                            <option value="PRODUCTFREE"
                                    <c:if test="${promoCode.discountType == \"PRODUCTFREE\"}">selected="selected"</c:if>>
                                Бесплатные товары (штуки)
                            </option>
                        </form:select>
                        <form:errors path="discountType" cssClass="error"/>
                    </div>

                    <div class="col-lg-6">
                        <label>Размер скидки (рублей, процентов, штук товаров)</label>
                        <form:input type="number" cssClass="form-control" min="0" name="discountAmount" path="discountAmount"/>
                        <form:errors path="discountAmount" cssClass="error"/>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12">
                        <form:checkbox path="freeDelivery"/>
                        <label>Бесплатная доставка</label>
                        <span class="info">Бесплатная доставка может комбинироваться с любым типом скидки.</span><br/>
                        <form:errors path="freeDelivery" cssClass="error"/>
                    </div>
                </div>

                <hr/>

                <h3>Ограничения</h3>

                <div class="row ma-b-20">
                    <div class="col-lg-3">
                        <label>Тип использования</label>
                        <form:select path="codeUsageType" cssClass="form-control">
                            <option value="UNLIMITED"
                                    <c:if test="${promoCode.codeUsageType == \"UNLIMITED\"}">selected="selected"</c:if>>
                                Без ограничений
                            </option>
                            <option value="LIMITED"
                                    <c:if test="${promoCode.codeUsageType == \"LIMITED\"}">selected="selected"</c:if>>
                                По количеству
                            </option>
                            <option value="PERSONAL"
                                    <c:if test="${promoCode.codeUsageType == \"PERSONAL\"}">selected="selected"</c:if>>
                                Персональный
                            </option>
                        </form:select>
                        <form:errors path="codeUsageType" cssClass="error"/>
                    </div>

                    <div class="col-lg-3">
                        <label>Кол-во использований</label>
                        <form:input type="number" min="0" path="usageCount" cssClass="form-control"/>
                        <span class="info">Используется при ограничениях "по количеству", "по пользователю", "персональный"</span>
                        <form:errors path="usageCount" cssClass="error"/>
                    </div>

                    <div class="col-lg-3">
                        <label>ID пользователя</label>
                        <form:input type="number" min="0" path="profileId" cssClass="form-control"/>
                        <span class="info">Используется при ограничении "персональный"</span>
                        <form:errors path="profileId" cssClass="error"/>
                    </div>

                    <div class="col-lg-3">
                        <label>Категория товара</label>
                        <form:select path="productTagId" cssClass="form-control">
                            <option value="0"
                                    <c:if test="${promoCode.productTagId} == 0">selected</c:if>>
                                Не указана
                            </option>
                            <c:forEach items="${productTags}" var="productTag">
                                <option
                                        <c:if test="${productTag.categoryId == promoCode.productTagId}">selected</c:if>
                                        value="${productTag.categoryId}">${productTag.value}
                                </option>
                            </c:forEach>
                        </form:select>
                        <form:errors path="productTagId" cssClass="error"/>
                    </div>
                </div>

                <div class="row ma-b-20">
                    <div class="col-lg-3">
                        <label>Минимальная сумма</label>
                        <form:input type="number" min="0" path="minOrderPrice" cssClass="form-control"/>
                        <span class="info">Укажите 0, если код должен действовать для любых сумм</span>
                        <form:errors path="minOrderPrice" cssClass="error"/>
                    </div>

                    <div class="col-lg-3">
                        <label>Минимальный размер корзины</label>

                        <form:input type="number" min="0" path="minProductCount" cssClass="form-control"/>
                        <span class="info">Укажите 0, если код должен действовать для корзин любого размера</span>
                        <form:errors path="minProductCount" cssClass="error"/>
                    </div>

                    <div class="col-lg-3">
                        <label>Время начала действия</label>
                        <form:input id="timeBegin" type="hidden" path="timeBegin" />
                        <input id="date-box-begin" value='' type="text" class="form-control"/>
                        <form:errors path="timeBegin" cssClass="error"/>
                    </div>

                    <div class="col-lg-3">
                        <label>Время окончания действия</label>
                        <form:input id="timeEnd" type="hidden" path="timeEnd"/>
                        <input id="date-box-end" value='' type="text" class="form-control"/>
                        <form:errors path="timeEnd" cssClass="error"/>
                    </div>
                </div>

                <hr/>

                <div class="row ma-b-20">
                    <div class="col-lg-12">
                        <label>Код</label>

                        <%--//TODO новая админка: не работает кнопка сгенерирвоать--%>
                        <div class="input-group">
                            <form:input type="text" path="code" cssClass="form-control"/>
                            <span class="input-group-btn">
                                <button type="button" class="btn btn-primary" id="js-promo-code-gen">Сгенерировать</button>
                            </span>
                        </div>
                        <form:errors path="code" cssClass="error"/>
                    </div>
                </div>

                <div class="btn-group">
                    <button type="submit" class="btn btn-primary">
                        Сохранить
                    </button>
                    <a href="/moderator/shop/promo-codes.html" class="btn btn-default">Отмена</a>
                </div>

            </form:form>
        </div>
    </div>
</div>
</body>
</html>

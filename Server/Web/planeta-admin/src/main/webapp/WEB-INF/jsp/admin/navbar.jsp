﻿<c:set var="pageName" value="${requestScope['javax.servlet.forward.servlet_path']}" />

<nav class="navbar navbar-default navbar-dark navbar-static-top top-panel" role="navigation" style="margin-bottom: 0">
    <div class="navbar-header">
        <button type="button" class="navbar-toggle js-toggle-navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="https://${properties["application.host"]}">
            <%@ include file="/WEB-INF/jsp/includes/generated/irma/logo-white-svg.jsp" %>
        </a>
        <small>Admin panel v2.0</small>
    </div>

    <ul class="nav navbar-top-links navbar-right">
        <li class="dropdown">
            <a class="dropdown-toggle" data-toggle="dropdown" href="javascript:void(0)" aria-expanded="false">
                <i class="fa fa-user fa-fw"></i> <i class="fa fa-caret-down"></i>
            </a>
            <ul class="dropdown-menu dropdown-user dropdown-menu-dark">
                <div class="admin-user">
                    <img src="${hf:getUserAvatarUrl(myProfileProfile.smallImageUrl, "USER_SMALL_AVATAR", myProfileProfile.userGender)}"
                         alt="${hf:escapeHtml4(myProfileProfile.displayName)}" class="h-user_link-img">
                    <a href="/moderator/user-info.html?profileId=${myProfileProfile.profileId}">${myProfileProfile.displayName}</a>
                </div>

                <hr class="hr-small hr-dark">

                <li>
                    <a href="/logout">
                        <i class="fa fa-sign-out"></i> Выйти
                    </a>
                </li>
            </ul>
        </li>
    </ul>

    <div class="navbar-default navbar-dark sidebar" role="navigation">
        <div class="sidebar-nav sidebar-menu-block">
            <ul class="nav in" id="side-menu">
                <li class="first-level" title="Сводка">
                    <a href="/dashboard.html" class="firs-level-link">
                        <i class="fa fa-dashboard fa-fw"></i>
                        <span class="menu-item-text">Dashboard</span>
                    </a>
                </li>
                <li class="first-level" title="Модерация">
                    <a href="#" class="firs-level-link">
                        <i class="fa fa-lock fa-fw"></i>
                        <span class="menu-item-text">Модерация</span>
                        <span class="fa arrow"></span>
                    </a>

                    <ul class="nav nav-second-level collapse">
                        <li><a href="/moderator/users.html">Пользователи</a></li>
                        <li><a href="/moderator/campaigns.html">Проекты</a></li>
                        <li><a href="/moderator/manager-campaigns.html">Мои Проекты</a></li>
                        <li><a href="/moderator/contractors.html">Контрагенты</a></li>
                        <li><a href="/admin/investing-orders-info.html">Заказы инвестинга</a></li>
                        <li><a href="/moderator/orders.html">Заказы</a></li>
                        <li><a href="/moderator/broadcasts.html">Трансляции</a></li>
                        <li><a href="/admin/seminars.html">Школа</a></li>
                        <li><a href="/admin/concert-list.html">Концерты</a></li>
                        <li><a href="/moderator/promo/projects.html">Технобитва</a></li>
                    </ul>
                </li>

                <li class="first-level" title="Статистика">
                    <a href="#" class="firs-level-link">
                        <i class="fa fa-bar-chart-o fa-fw"></i>
                        <span class="menu-item-text"> Статистика</span>
                        <span class="fa arrow"></span>
                    </a>

                    <ul class="nav nav-second-level collapse">
                        <li><a href="/admin/statistic-composite.html">Общая статистика</a></li>
                        <li><a href="/admin/statistic-share.html">Статистика по вознаграждениям</a></li>
                        <li><a href="/admin/statistic-product.html">Статистика по товарам</a></li>
                        <li><a href="/admin/statistic-forecast.html">Прогнозирование</a></li>
                        <li><a href="http://reports.planeta.ru">Отчеты</a></li>
                    </ul>
                </li>

                <li class="first-level" title="Магазин">
                    <a href="#" class="firs-level-link">
                        <i class="fa fa-shopping-cart fa-fw"></i> <span class="menu-item-text">Магазин</span>
                        <span class="fa arrow"></span>
                    </a>

                    <ul class="nav nav-second-level collapse">
                        <li><a href="/moderator/shop/orders.html">Заказы магазина</a></li>
                        <li><a href="/admin/products/planeta.html">Товары магазина</a></li>
                        <li><a href="/moderator/shop/delivery-list.html">Службы доставки</a></li>
                        <li><a href="/moderator/shop/promo-codes.html">Промокоды</a></li>
                        <li><a href="/moderator/shop/product-tags.html">Категории товаров</a></li>
                    </ul>
                </li>

                <li class="first-level" title="Бонусы">
                    <a href="#" class="firs-level-link">
                        <i class="fa fa-gift fa-fw"></i> <span class="menu-item-text">Бонусы</span>
                        <span class="fa arrow"></span>
                    </a>

                    <ul class="nav nav-second-level collapse">
                        <li><a href="/moderator/loyalty-bonuses.html">Бонусы</a></li>
                        <li><a href="/moderator/loyalty-orders.html">Заказы бонусов</a></li>
                        <li><a href="/moderator/loyalty-vip-users.html">Вип-спонсоры</a></li>
                    </ul>
                </li>

                <li class="first-level" title="Контент">
                    <a href="#" class="firs-level-link">
                        <i class="fa fa-info fa-fw"></i> <span class="menu-item-text">Контент</span>
                        <span class="fa arrow"></span>
                    </a>

                    <ul class="nav nav-second-level collapse">
                        <li><a href="/admin/posts.html">Посты</a></li>
                        <li><a href="/moderator/start-promo-list.html">Проекты</a></li>
                        <li><a href="/moderator/broadcast-promo-list.html">ТВ</a></li>
                        <li><a href="/moderator/charity-broadcast-list.html">Благотворительность</a></li>
                        <li><a href="${mainAppUrl}/faq/edit">FAQ</a></li>
                        <li><a href="/moderator/about-us-new-settings.html">СМИ о нас [НОВОЕ]</a></li>
                        <li><a href="/moderator/partners-promo-list.html">Партнеры</a></li>
                        <li><a href="/admin/banners-list.html">Баннеры</a></li>
                        <li><a href="/admin/short-links-list.html">Редиректы</a></li>
                        <li><a href="/moderator/image-upload.html">Загрузить картинку</a></li>
                        <li><a href="/moderator/file-upload.html">Загрузить файл</a></li>
                        <li><a href="/moderator/document-upload.html">Загрузить документ</a></li>
                    </ul>
                </li>

                <li class="first-level" title="Уведомления">
                    <a href="#" class="firs-level-link">
                        <i class="fa fa-bell fa-fw"></i> <span class="menu-item-text">Уведомления</span>
                        <span class="fa arrow"></span>
                    </a>

                    <ul class="nav nav-second-level collapse">
                        <li><a href="/moderator/campaign-news.html">События в проектах</a></li>
                        <li><a href="/moderator/campaign-posts.html">Новости в проектах</a></li>
                        <li><a href="/moderator/campaign-comments.html">Комментарии  в проектах</a></li>
                        <li><a href="/moderator/campaign-shares.html">События вознаграждений в проектах</a></li>
                        <li><a href="/moderator/products-comments.html">Комментарии к продуктам</a></li>
                    </ul>
                </li>


                <li class="first-level" title="Прочее">
                    <a href="#" class="firs-level-link">
                        <i class="fa fa-ellipsis-h fa-fw"></i> <span class="menu-item-text">Прочее</span>
                        <span class="fa arrow"></span>
                    </a>

                    <ul class="nav nav-second-level collapse">
                        <li><a href="/moderator/campaign-managers-relations.html">Менеджеры проектов и категории</a></li>
                        <li><a href="/moderator/campaign-management.html">Статусы проектов</a></li>
                        <li><a href="/admin/geo/add-new-city.html">Управление городами</a></li>
                        <li><a href="/moderator/base-delivery-services.html">Службы доставки</a></li>
                        <li><a href="/moderator/campaign-addresses-stickers-from-file.html">Наклейки - адреса из файла</a></li>

                        <c:if test="${isSuperAdmin}">
                            <li><a href="/moderator/payment-configurator.html">Конфигурилка платёжек</a></li>
                        </c:if>

                        <li><a href="/admin/sponsors.html">Спонсоры</a></li>
                        <li><a href="/admin/campuses.html">Кампусы</a></li>
                        <li><a href="/moderator/campaign-tags.html">Категории проектов</a></li>

                        <li><a href="${properties['mailer-web.url']}">Рассылки</a></li>
                        <li><a href="${properties['planeta-mail.url']}">Транзакционные письма</a></li>
                        <li><a href="/admin/quizzes.html">Опросы</a></li>
                        <li><a href="/admin/promo-config-list.html">Промо-письма</a></li>
                    </ul>
                </li>

                <li class="first-level" title="Поддержка">
                    <a href="#" class="firs-level-link">
                        <i class="fa fa-user fa-fw"></i> <span class="menu-item-text">Поддержка</span>
                        <span class="fa arrow"></span>
                    </a>

                    <ul class="nav nav-second-level collapse">
                        <c:if test="${isAdmin}">
                            <li><a href="/admin/billing/payments.html">Платежи</a></li>
                            <li><a href="/admin/billing/payment-errors.html">Платежные ошибки</a></li>
                        </c:if>
                        <li><a href="/moderator/user-callbacks.html">Заявки на обратный звонок</a></li>

                    </ul>
                </li>

                <li class="first-level" title="Теги">
                    <a href="#" class="firs-level-link">
                        <i class="fa fa-tags fa-fw"></i> <span class="menu-item-text">Теги</span>
                        <span class="fa arrow"></span>
                    </a>

                    <ul class="nav nav-second-level collapse">
                        <li><a href="/admin/custom-meta-tag-list.html?customMetaTagType=MAIN">Планета</a></li>
                        <li><a href="/admin/custom-meta-tag-list.html?customMetaTagType=SHOP">Магазин</a></li>
                        <li><a href="/admin/custom-meta-tag-list.html?customMetaTagType=TV">ТВ</a></li>
                        <li><a href="/admin/custom-meta-tag-list.html?customMetaTagType=CONCERT">Концерт</a></li>
                        <li><a href="/admin/custom-meta-tag-list.html?customMetaTagType=SCHOOL">Школа</a></li>
                        <li><a href="/admin/custom-meta-tag-list.html?customMetaTagType=BIBLIO">Библиородина</a></li>
                        <li><a href="/admin/custom-meta-tag-list.html?customMetaTagType=CHARITY">Благотворительность</a></li>
                        <li><a href="/admin/custom-meta-tag-list.html?customMetaTagType=PROMO">Промо</a></li>
                    </ul>
                </li>

                <li class="first-level" title="Библиородина">
                    <a href="#" class="firs-level-link">
                        <i class="fa fa-book fa-fw"></i> <span class="menu-item-text">Библиородина</span>
                        <span class="fa arrow"></span>
                    </a>

                    <ul class="nav nav-second-level collapse">
                        <li><a href="/admin/library/libraries.html">Библиотеки</a></li>
                        <li><a href="/admin/book/books.html">Издания</a></li>
                        <li><a href="/admin/biblio/publishing-houses.html">Издательства</a></li>
                        <li><a href="/moderator/biblio-russian-post-books-info-from-file.html">Почта России [загрузить прайс-лист]</a></li>
                        <li><a href="/moderator/biblio-russian-post-price-lists.html">Почта России [загруженные прайс-листы]</a></li>
                        <li><a href="/admin/biblio/partners-biblio-list.html">Партнеры</a></li>
                        <li><a href="/admin/biblio/advertise-biblio-list.html">Реклама</a></li>
                        <li><a href="/admin/biblio/orders.html">Заказы</a></li>
                    </ul>
                </li>

                <li class="first-level" title="Благотворительность">
                    <a href="#" class="firs-level-link">
                        <i class="fa fa-money fa-fw"></i> <span class="menu-item-text">Благотворительность</span>
                        <span class="fa arrow"></span>
                    </a>

                    <ul class="nav nav-second-level collapse">
                        <li><a href="/moderator/charity-partners-promo-list.html">Партнеры</a></li>
                        <li><a href="/admin/charity/posts.html">Новости</a></li>
                        <li><a href="/admin/charity/school-library-themes.html">Библиотека файлов (темы / разделы)</a></li>
                        <li><a href="/admin/charity/school-library-files.html">Библиотека файлов</a></li>
                    </ul>
                </li>

                <li class="first-level" title="Настройки">
                    <a href="/admin/configuration-list.html">
                        <i class="fa fa-cog fa-fw"></i>
                        <span class="menu-item-text">Настройки</span>
                    </a>
                </li>
            </ul>
        </div>

        <div class="sidebar-bottom-actions">
            <div class="sidebar-bottom-actions_i toggle-menu js-toggle-menu" title="Свернуть">
                <i class="fa fa-angle-left"></i>
            </div>
        </div>
    </div>
</nav>
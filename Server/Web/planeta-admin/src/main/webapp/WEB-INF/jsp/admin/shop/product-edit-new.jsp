<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ include file="/WEB-INF/jsp/includes/generated/doctype-header.jsp" %>
<head>
    <%@ include file="/WEB-INF/jsp/admin/head.jsp" %>
    <link type="text/css" rel="stylesheet" href="//${hf:getStaticBaseUrl("")}/css-generated/shop-admin.css"/>
    <link type="text/css" rel="stylesheet" href="//${hf:getStaticBaseUrl("")}/css/jquery.fancybox.css"/>
    <link type="text/css" rel="stylesheet"
          href="//${hf:getStaticBaseUrl("")}/admin-new/bootstrap/css/jquery-ui-1.9.2.custom.css">
    <link type="text/css" rel="stylesheet" href="//${hf:getStaticBaseUrl("")}/css/jquery.Jcrop.css">
    <style>
        .modal-photo-crop {
            width: 700px !important;
        }

        .modal-photo-crop .modal-body {
            max-height: none !important;
        }

        .jcrop-holder img {
            max-width: none !important;
        }

        .photo-crop_wrap .jcrop-holder {
            margin: auto !important;
        }

        .photo-crop_img {
            max-height: 500px;
        }
    </style>
    <link type="text/css" rel="stylesheet" href="//${hf:getStaticBaseUrl("")}/css/font-awesome.min.css">
    <c:choose>
        <c:when test="${product.productId>0}">
            <title>Редактирование товара "${product.name}"</title>
        </c:when>
        <c:otherwise>
            <title>Создание товара</title>
        </c:otherwise>
    </c:choose>

    <script type="text/javascript">

        $(document).ready(function () {
            ShopUtils.setProductTags(${hf:toJson(productTags)});
            ShopUtils.setTagToAttributesTypeMap(${hf:toJson(predefinedAttributes)});
            workspace.appModel.productCategories = ${hf:toJson(productCategories)};

            var productModel = new ProductEdit.Models.Product(${hf:toJson(product)});
            productModel.set("resourceHost", "${resourceHost}");

            var productEditView = new ProductEdit.Views.ProductEditPage({
                el: '.js-editpage',
                model: productModel
            });
            productEditView.render();
        });
    </script>
</head>

<body>

<%@ include file="/WEB-INF/jsp/admin/navbar.jsp" %>
<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                <c:choose>
                    <c:when test="${product.productId > 0}">
                        Редактирование товара ${product.name}
                    </c:when>
                    <c:otherwise>
                        Создание товара
                    </c:otherwise>
                </c:choose>
            </h1>
        </div>
    </div>

    <div class="row">
        <div class="js-editpage col-lg-12"></div>
    </div>
</div>

</body>
</html>

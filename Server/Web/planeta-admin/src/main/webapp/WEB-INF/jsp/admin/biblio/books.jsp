<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ include file="/WEB-INF/jsp/includes/generated/doctype-header.jsp" %>
<%@taglib prefix="p" uri="http://planeta.ru/tags" %>
<head>
    <%@ include file="/WEB-INF/jsp/admin/head.jsp" %>

    <style type="text/css">
        img {
            max-width: 200px;
        }
    </style>

    <script>
        $(function() {
            $('#clear-filter').click(function(e) {
                e.preventDefault();
                $('form').find('input,selectCampaignById').val('');
                $('#allTimeButton').click();
            });
        })
    </script>

</head>
<body>

<%@ include file="/WEB-INF/jsp/admin/navbar.jsp" %>

<div id="page-wrapper">

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Издания</h1>
        </div>
    </div>

    <div class="main-page-actions">
        <form method="get">
            <a href="/admin/book/book.html" class="btn btn-success btn-circle btn-outline btn-lg" title="Добавить">
                <i class="fa fa-plus"></i>
            </a>
        </form>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-primary">
                <div class="panel-heading">Поиск изданий</div>
                <div class="panel-body">
                    <form id="search-frm" method="GET" action="/admin/book/books.html" class="clearfix">
                        <input type="hidden" name="offset" value="0"/>
                        <input type="hidden" name="limit" value="10"/>

                        <div class="row ma-b-20">
                            <div class="col-lg-8">
                                <label>Запрос</label>
                                <input class="form-control"
                                       type="text"
                                       placeholder="Введите название"
                                       id="searchStr"
                                       name="searchStr"
                                       value="${param.searchStr}">
                            </div>

                            <div class="col-lg-4">
                                <label>Статус</label>
                                <select class="form-control" id="status" name="status">
                                    <option value="" <c:if test="${empty param.status}">selected=''</c:if>>Все</option>
                                    <option value="ACTIVE" <c:if test="${param.status eq 'ACTIVE'}">selected=''</c:if>>
                                        Активные
                                    </option>
                                    <option value="PAUSED" <c:if test="${param.status eq 'PAUSED'}">selected=''</c:if>>На
                                        паузе
                                    </option>
                                </select>
                            </div>
                        </div>

                        <div class="row ma-b-20">
                            <div class="col-lg-12">
                                <ct:date-range-picker dateFrom="${dateFrom}" dateTo="${dateTo}" allButton="true"/>
                            </div>
                        </div>

                        <div class="span2 btn-group ma-b-20">
                            <button id="search" class="btn btn-primary">
                                <i class="fa fa-search"></i> Поиск
                            </button>
                            <button id="clear-filter" class="btn btn-default">
                                <i class="fa fa-trash"></i> Очистить
                            </button>
                        </div>

                        <div class="input-group-btn">
                            <button class="btn btn-success" formaction="books-report.html">
                                <i class="fa fa-download"></i> Скачать в формате Excel
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <c:if test="${empty books}">
        <div class="row">
            <div class="col-lg-12">
                <h2 class="text-center">
                    <small>По Вашему запросу ничего не найдено ...</small>
                </h2>
                <hr>
            </div>
        </div>
    </c:if>

    <c:if test="${not empty books}">
        <div class="row">
            <div class="col-lg-12 admin-table">
                <table class="table table-hover table-bordered">
                    <thead>
                    <tr>
                        <th>Название</th>
                        <th>Описание</th>
                        <th>Обложка</th>
                        <th>Статус</th>
                        <th>Комментарии</th>
                        <th title="Отображать на главной">Главная</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:forEach items="${books}" var="book">
                        <tr>
                            <td> ${book.title} </td>
                            <td> ${book.description} </td>
                            <td><img src="${book.imageUrl}"></td>
                            <td>
                                <c:choose>
                                    <c:when test="${book.status eq 'ACTIVE'}">
                                        <span class="label label-success">Активно</span>
                                    </c:when>
                                    <c:when test="${book.status eq 'PAUSED'}">
                                        <span class="label label-info">Приостановлено</span>
                                    </c:when>
                                    <c:when test="${book.status eq 'DELETED'}">
                                        <span class="label label-danger">Удалено</span>
                                    </c:when>
                                </c:choose>
                                <br>Добавлено <fmt:formatDate pattern="dd.MM.yyyy" value="${book.timeAdded}"/>
                            </td>
                            <td>${book.comment}</td>
                            <td><c:choose><c:when
                                    test="${book.index > 0}">Да</c:when><c:otherwise>Нет</c:otherwise></c:choose></td>
                            <td class="col-lg-1">
                                <div class="btn-group">
                                    <a class="btn btn-outline btn-primary"
                                       href="/admin/book/book.html?bookId=${book.bookId}"
                                       title="Редактировать">
                                        <i class="fa fa-pencil"></i>
                                    </a>
                                    <a class="btn btn-outline btn-danger"
                                       href="/admin/book/delete-book.html?bookId=${book.bookId}"
                                       title="Удалить">
                                        <i class="fa fa-trash"></i>
                                    </a>
                                </div>
                            </td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
                <%@ include file="../paginator.jsp" %>
            </div>
        </div>
    </c:if>
</div>
</body>
</html>


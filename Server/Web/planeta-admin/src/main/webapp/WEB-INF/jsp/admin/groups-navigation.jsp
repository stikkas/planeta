<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<c:set var="page" value="${(offset div limit) + 1}"/>

<div class="pagination">
    <ul>
        <c:if test="${offset>0}">
            <li>
                <c:if test="${offset>=limit}">
                <a href="?${requestGetParams}offset=${offset - limit}&limit=${limit}"></c:if>
                    <c:if test="${offset<limit}">
                    <a href="?${requestGetParams}offset=0&limit=${limit}"></c:if>
                        Prev</a>
            </li>
        </c:if>
        <c:if test="${page - 2 > 1}">
            <li><a href="?${requestGetParams}offset=0&limit=${limit}">1</a></li>
            <li><a>...</a></li>
        </c:if>
        <c:if test="${page - 2 >= 1}">
            <li>
                <a href="?${requestGetParams}offset=${offset - 2 * limit}&limit=${limit}"><fmt:formatNumber
                        maxFractionDigits="0" value="${page - 2}"/></a>
            </li>
        </c:if>
        <c:if test="${page - 1 >= 1}">
            <li>
                <a href="?${requestGetParams}offset=${offset - 1 * limit}&limit=${limit}"><fmt:formatNumber
                        maxFractionDigits="0" value="${page - 1}"/></a></li>
        </c:if>
        <c:if test="${fn:length(profiles) < limit}">
            <li class="active"><a
                    href="?${requestGetParams}offset=${offset}&limit=${limit}"><fmt:formatNumber
                    maxFractionDigits="0" value="${page}"/></a></li>
        </c:if>
        <c:if test="${fn:length(profiles) == limit}">
            <li class="active"><a
                    href="?${requestGetParams}offset=${offset}&limit=${limit}"><fmt:formatNumber
                    maxFractionDigits="0" value="${page}"/></a></li>
        </c:if>

        <c:if test="${fn:length(profiles) == limit}">
            <li><a href="?${requestGetParams}offset=${offset+limit}&limit=${limit}">
                <fmt:formatNumber maxFractionDigits="0" value="${page+1}"/></a></li>
            <li><a href="?${requestGetParams}offset=${offset+limit}&limit=${limit}">Next</a></li>
        </c:if>

    </ul>
</div>


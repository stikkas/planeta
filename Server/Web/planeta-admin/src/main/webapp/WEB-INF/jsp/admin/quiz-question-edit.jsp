<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ include file="/WEB-INF/jsp/includes/generated/doctype-header.jsp" %>
<link type="text/css" rel="stylesheet"
      href="//${hf:getStaticBaseUrl("")}/css/ui-lightness/jquery-ui-1.10.0-bootstrap.custom.css"/>
<head>
    <%@include file="head.jsp" %>
    <script type="text/javascript">
        $(document).ready(function () {
            var changeQuizQuestionOptionData = function (quizQuestionOption) {
                $.post('/admin/quiz-question-option-edit.json', quizQuestionOption).done(function (response) {
                    if (response) {
                        if (response.success) {
                            location.reload();
                        } else {
                        }
                    }
                });
            };

            var optionActionHandler = function () {
                var quizQuestionOption = $('#form-option').serializeObject();
                if (!quizQuestionOption.optionText) {
                    $('.error-message').html('Текст варианта ответа не может быть пустым!');
                } else {
                    $('.error-message').html('');
                    changeQuizQuestionOptionData(quizQuestionOption);
                }
            };

            var showOptionModal = function (option) {
                Modal.View.show({
                    template: '#quiz-question-option-modal-template',
                    model: new BaseModel(option)
                });
                $(".js-add-button").click(function () {
                    optionActionHandler();
                });
            };

            $(".js-add-question-option").click(function () {
                var option = {
                    quizQuestionId: ${quizQuestion.quizQuestionId},
                    quizQuestionOptionId: 0,
                    optionText: ''
                };
                showOptionModal(option);
            });

            $(".js-edit-question-option").click(function (e) {
                var $tr = $(e.currentTarget).closest('tr');
                var quizQuestionOptionId = $tr.attr('quiz-question-option-id');
                var quizQuestionOptionText = $tr.find('.js-quiz-question-option-text').html().trim();
                var option = {
                    quizQuestionId: ${quizQuestion.quizQuestionId},
                    quizQuestionOptionId: quizQuestionOptionId,
                    optionText: quizQuestionOptionText
                };
                showOptionModal(option);
            });

            $(".js-remove-question-option").click(function (e) {
                e.preventDefault();
                var $tr = $(e.currentTarget).closest('tr');
                var quizQuestionOptionId = $tr.attr('quiz-question-option-id');
                var option = {
                    quizQuestionOptionId: quizQuestionOptionId
                };
                Modal.showConfirm('Удалить вариант ответа?', "Подтвердите удаление", {
                    success: function (e) {
                        $.post('/admin/quiz-question-option-remove.json', option).done(function (response) {
                            if (response) {
                                if (response.success) {
                                    location.reload();
                                }
                            }
                        });
                    }
                });
            });

            $("#type").click(function () {
                var chosenOption = $(this).val();
                if (chosenOption == 'RADIO_NUMBER_RANGE') {
                    $('#radioNumberRange').css({display: 'block'});
                } else {
                    $('#radioNumberRange').css({display: 'none'});
                }

                if (chosenOption == 'RADIO_OPTIONS') {
                    $('.js-has-custom-radio').css({display: 'block'});
                    if ($("input[name='hasCustomRadio']").attr('checked')) {
                        $('#customRadioText').css({display: 'block'});
                    }
                } else {
                    $('.js-has-custom-radio').css({display: 'none'});
                    $('#customRadioText').css({display: 'none'});
                }
            });

            $("input[name='hasCustomRadio']").click(function (e) {
                if ($(e.currentTarget).attr('checked')) {
                    $('#customRadioText').css({display: 'block'});
                } else {
                    $('#customRadioText').css({display: 'none'});
                }
            });

            var startIndex;
            //JQUERY plugin that makes possible to sort collection and move collection elements
            var sortable = $('table tbody').sortable({
                revert: true,
                handle: '.btn.js-move',

                helper: function (e, ui) {
                    ui.children().each(function () {
                        $(this).width($(this).width());
                    });
                    return ui;
                },

                start: function (event, ui) {
                    ui.placeholder.html(ui.helper.html());
                    startIndex = ui.item.index();
                },

                update: function (event, ui) {
                    var stopIndex = ui.item.index();
                    if (stopIndex != startIndex) {
                        var quizQuestionOptionId = $(ui.item[0]).attr('quiz-question-option-id').trim();
                        $.ajax({
                            url: '/admin/quiz-questions-option-sort.html',
                            data: {
                                quizQuestionId: ${quizQuestion.quizQuestionId},
                                quizQuestionOptionId: quizQuestionOptionId,
                                oldOrderNum: startIndex,
                                newOrderNum: stopIndex
                            },
                            success: function (response) {
                                console.log('success');
                            }
                        });
                    }
                }
            });

            var tinyModel;
            moduleLoader.loadModule('attachments');
            moduleLoader.loadModule('tiny-mce-campaigns').done(function () {

                var modelClass = function (campaignModel) {
                    _.extend(this, new TinyMcePlaneta.EditorModel(campaignModel), {
                        config: TinyMcePlaneta.getQuizEditorConfiguration(),
                        width: 550,
                        height: 400,
                        saveToParentModel: function () {

                        },
                        destructPlugins: function () {
                            if (tinymce.activeEditor.plugins.planetafullscreencampaign.fullScreen) {
                                tinymce.activeEditor.execCommand("planetafullscreencampaign");
                            }
                        }
                    });
                };

                tinyModel = new modelClass();
                tinyModel.init($('.tinymce-body'));
            });
        });
    </script>
    <script id="quiz-question-option-modal-template" type="text/x-jquery-template">
        <div class="modal-header">
            <a type="button" class="close" data-dismiss="modal" aria-hidden="true">×</a>
            {{if quizQuestionOptionId > 0}}
                <h4>Изменение варианта ответа</h4>
            {{else}}
                <h4>Добавление варианта ответа</h4>
            {{/if}}
        </div>

        <div class="modal-body">
            <form method="post" id="form-option">
                <div class="row">
                    <div class="col-lg-12">
                        <label>Текст варанта ответа:</label>
                        <input name="optionText" type="text" class="form-control" value="{{= optionText}}">
                        <div class="error-message"></div>
                    </div>
                    <input name="quizQuestionId" type="hidden" value="{{= quizQuestionId}}">
                    <input name="quizQuestionOptionId" type="hidden" value="{{= quizQuestionOptionId}}">
                </div>
            </form>
        </div>

        <div class="modal-footer">
            {{if quizQuestionOptionId > 0}}
                <button class="btn btn-primary js-add-button">Изменить</button>
            {{else}}
                <button class="btn btn-primary js-add-button">Добавить</button>
            {{/if}}
        </div>
    </script>
    <style>
        .help-inline {
            color: #cc1010;
        }
    </style>
</head>

<body>
<%@ include file="navbar.jsp" %>

<div id="page-wrapper">

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Вопрос к опросу № ${quiz.quizId}</h1>
        </div>
    </div>

    <div class="main-page-actions">
        <c:if test="${quiz.alias != null}">
            <a href="${mainAppUrl}/quiz/${quiz.alias}" class="btn btn-primary btn-circle btn-outline btn-lg" title="Страница опроса" target="_blank">
                <i class="fa fa-file-o"></i>
            </a>
        </c:if>
        <c:if test="${quiz.alias == null}">
            <a href="${mainAppUrl}/quiz/${quiz.quizId}" class="btn btn-primary btn-circle btn-outline btn-lg" title="Страница опроса" target="_blank">
                <i class="fa fa-file-o"></i>
            </a>
        </c:if>
    </div>

    <c:if test="${quiz.enabled}">
        <div class="row">
            <div class="col-lg-12">
                <div class="alert alert-danger bs-alert-old-docs">
                    <strong>Внимание!</strong> Данный вопрос находится в действующем опросе и виден пользователям. Любые
                    изменения будут сразу же применены.
                </div>
            </div>
        </div>
    </c:if>

    <div class="row">
        <div class="col-lg-12">
            <%@include file="successMessage.jsp" %>
        </div>
    </div>

    <div class="row ma-b-20">
        <div class="col-lg-12">
            <form:form method="POST" commandName="quizQuestion" class="form-horizontal">
                <form:input type="hidden" path="quizQuestionId"/>
                <form:input type="hidden" path="quizQuestionOrderNum"/>

                <div class="row ma-b-20">
                    <div class="col-lg-12">
                        <label>Вопрос</label>
                        <form:input type="text" path="questionText" value="${quizQuestion.questionText}" cssClass="form-control"/>
                        <form:errors path="questionText"><span class="help-inline"><form:errors path="questionText"/></span></form:errors>
                    </div>
                </div>

                <div class="row ma-b-20">
                    <div class="col-lg-12">
                        <label>HTML описание</label>

                        <div id="questionDescription" class="tinymce-body">
                                ${quizQuestion.questionDescription}
                        </div>
                        <form:errors path="questionDescription"><span class="help-inline"><form:errors path="questionDescription"/></span></form:errors>
                    </div>
                </div>

                <div class="row ma-b-20">
                    <div class="col-lg-12">
                        <label>Тип вопроса</label>

                        <form:select path="type" cssClass="form-control" id="type">
                            <option value="TEXT"
                                    <c:if test="${quizQuestion.type == \"TEXT\"}">selected="selected"</c:if>>
                                Текстовый ответ
                            </option>

                            <option value="TEXT_BIG"
                                    <c:if test="${quizQuestion.type == \"TEXT_BIG\"}">selected="selected"</c:if>>
                                Текстовый ответ на много букв
                            </option>

                            <option value="NUMBER"
                                    <c:if test="${quizQuestion.type == \"NUMBER\"}">selected="selected"</c:if>>
                                Числовой ответ
                            </option>

                            <option value="CHECKBOX_SIMPLE"
                                    <c:if test="${quizQuestion.type == \"CHECKBOX_SIMPLE\"}">selected="selected"</c:if>>
                                Да/нет
                            </option>

                            <%--<option value="CHECKBOX_MULTI"
                                    <c:if test="${quizQuestion.type == \"CHECKBOX_MULTI\"}">selected="selected"</c:if>>
                                Множественный выбор
                            </option>--%>

                            <option value="RADIO_NUMBER_RANGE"
                                    <c:if test="${quizQuestion.type == \"RADIO_NUMBER_RANGE\"}">selected="selected"</c:if>>
                                Оценка в баллах от и до
                            </option>

                            <option value="RADIO_OPTIONS"
                                    <c:if test="${quizQuestion.type == \"RADIO_OPTIONS\"}">selected="selected"</c:if>>
                                Один из вариантов
                            </option>
                        </form:select>
                    </div>
                </div>

                <c:if test="${quizQuestion.type == \"RADIO_OPTIONS\"}">
                    <div class="row ma-b-20">
                        <div class="col-lg-12 js-has-custom-radio">
                            <form:checkbox path="hasCustomRadio" name="hasCustomRadio"/>
                            <label>Добавить вариант со своим ответом</label>
                            <form:errors path="hasCustomRadio"><span class="help-inline"><form:errors path="hasCustomRadio"/></span></form:errors>
                        </div>
                    </div>
                </c:if>

                <c:if test="${quizQuestion.type != \"RADIO_OPTIONS\"}">
                    <div class="row ma-b-20">
                        <div class="col-lg-12 js-has-custom-radio" style="display: none;">
                            <form:checkbox path="hasCustomRadio" name="hasCustomRadio"/>
                            <label>Добавить вариант со своим ответом</label>
                            <form:errors path="hasCustomRadio"><span class="help-inline"><form:errors path="hasCustomRadio"/></span></form:errors>
                        </div>
                    </div>
                </c:if>

                <c:if test="${quizQuestion.type == \"RADIO_OPTIONS\" && quizQuestion.hasCustomRadio}">
                    <div class="row ma-b-20">
                        <div id="customRadioText" class="col-lg-12">
                            <label>Заголовок своего варианта ответа</label>
                            <form:input type="text" path="customRadioText" value="${quizQuestion.customRadioText}" cssClass="form-control"/>
                            <form:errors path="customRadioText"><span class="help-inline"><form:errors path="customRadioText"/></span></form:errors>
                        </div>
                    </div>
                </c:if>
                
                <c:if test="${!quizQuestion.hasCustomRadio || quizQuestion.type != \"RADIO_OPTIONS\"}">
                    <div class="row ma-b-20">
                        <div id="customRadioText" class="col-lg-12" style="display: none;">
                            <label>Заголовок своего варианта ответа</label>
                            <form:input type="text" path="customRadioText" value="${quizQuestion.customRadioText}" cssClass="form-control"/>
                            <form:errors path="customRadioText"><span class="help-inline"><form:errors path="customRadioText"/></span></form:errors>
                        </div>
                    </div>
                </c:if>

                <c:if test="${quizQuestion.type == \"RADIO_NUMBER_RANGE\"}">
                    <div class="row ma-b-20" id="radioNumberRange">
                        <div class="col-lg-6">
                            <label>Стартовое значение</label>
                            <form:input type="number" min="0" path="startValue" cssClass="form-control"/>
                            <form:errors path="startValue">
                                    <span class="help-inline">
                                        <form:errors path="startValue"/>
                                    </span>
                            </form:errors>
                        </div>

                        <div class="col-lg-6">
                            <label>Конечное значение</label>

                            <form:input type="number" min="0" path="endValue" cssClass="form-control"/>
                            <form:errors path="endValue">
                                    <span class="help-inline">
                                        <form:errors path="endValue"/>
                                    </span>
                            </form:errors>
                        </div>
                    </div>
                </c:if>

                <c:if test="${quizQuestion.type != \"RADIO_NUMBER_RANGE\"}">
                    <div class="row ma-b-20" id="radioNumberRange" style="display: none;">
                        <div class="col-lg-6">
                            <label>Стартовое значение</label>
                            <form:input type="number" min="0" path="startValue" cssClass="input-small"/>
                            <form:errors path="startValue">
                                <span class="help-inline">
                                    <form:errors path="startValue"/>
                                </span>
                            </form:errors>
                        </div>

                        <div class="col-lg-6">
                            <label>Конечное значение</label>
                            <form:input type="number" min="0" path="endValue" cssClass="input-small"/>
                            <form:errors path="endValue">
                                <span class="help-inline">
                                    <form:errors path="endValue"/>
                                </span>
                            </form:errors>
                        </div>
                    </div>
                </c:if>

                <button type="submit" class="btn btn-primary">Сохранить</button>
            </form:form>
        </div>
    </div>

    <c:if test="${quizQuestion.quizQuestionId > 0}">
        <h2 <c:if test="${quizQuestion.type != \"RADIO_OPTIONS\"}"> style="background-color: white; opacity: .5;"</c:if>>
            Варианты ответа (${quizQuestion.quizQuestionOptionList.size()})
        </h2>

        <button class="btn btn-success btn-xs js-add-question-option ma-b-20">
            <i class="fa fa-plus"></i> Добавить
        </button>

        <div class="row" <c:if test="${quizQuestion.type != \"RADIO_OPTIONS\"}"> style="background-color: white; opacity: .5;"</c:if>>
            <div class="col-lg-12 admin-table">
                <table class="table table-bordered table-striped">
                    <thead>
                        <th>Вариант</th>
                        <th>Действие</th>
                    </thead>

                    <tbody>
                        <c:forEach items="${quizQuestion.quizQuestionOptionList}" var="quizQuestionOption" varStatus="st">
                            <tr quiz-question-option-id="${quizQuestionOption.quizQuestionOptionId}">
                                <td class="js-quiz-question-option-text">
                                        ${quizQuestionOption.optionText}
                                </td>
                                <td class="text-right">
                                    <div class="btn-group">
                                        <a class="btn btn-default btn-outline js-move" title="Переместить">
                                            <i class="fa fa-arrows"></i>
                                        </a>
                                        <button class="btn btn-primary btn-outline js-edit-question-option"
                                                quiz-question-option-id="${quizQuestionOption.quizQuestionOptionId}"
                                                title="Редактировать">
                                            <i class="fa fa-pencil"></i>
                                        </button>
                                        <button class="btn btn-danger btn-outline js-remove-question-option"
                                                quiz-question-option-id="${quizQuestionOption.quizQuestionOptionId}"
                                                title="Удалить">
                                            <i class="fa fa-trash"></i>
                                        </button>
                                    </div>
                                </td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
            </div>
        </div>
    </c:if>
</div>
</body>
</html>

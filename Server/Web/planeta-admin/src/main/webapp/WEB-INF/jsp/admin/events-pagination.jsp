<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<div class="row ">
    <div class="span12">
        <c:set var="page" value="${(offset div 10) + 1}"/>
        <c:set var="pageCount" value="${(count - 1) div 10 + 1}"/>
        <c:set var="pagePrev" value="${page - 2}"/>
        <c:set var="filter" value="&dateFrom=${dateFrom}&dateTo=${dateTo}"/>
        <div class="pagination">
            <ul>
                <c:if test="${page > 1}">
                    <li>
                        <a href="?searchString=${searchString}&offset=${offset - limit}&limit=${limit}${filter}">Prev</a>
                    </li>
                </c:if>
                <c:if test="${page < pageCount}">
                    <li class="active"><a
                            href="?searchString=${searchString}&offset=${offset}&limit=${limit}${filter}"><fmt:formatNumber
                            maxFractionDigits="0" value="${page}"/></a></li>
                </c:if>
                <c:if test="${pageCount - page > 1}">
                    <li>
                        <a href="?searchString=${searchString}&offset=${offset + 1 * limit}&limit=${limit}${filter}">Next</a>
                    </li>
                </c:if>
            </ul>
        </div>
    </div>
</div>

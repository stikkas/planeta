<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ include file="/WEB-INF/jsp/includes/generated/doctype-header.jsp" %>
<head>
    <%@ include file="/WEB-INF/jsp/admin/head.jsp" %>
</head>

<body>
<%@ include file="/WEB-INF/jsp/admin/navbar.jsp" %>

<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Категории товаров</h1>
        </div>
    </div>

    <div class="main-page-actions">
        <a class="btn btn-success btn-circle btn-outline btn-lg" href="/moderator/shop/product-tag.html" title="Добавить категорию">
            <i class="fa fa-plus"></i>
        </a>
    </div>

    <div class="row">
        <div class="col-lg-12 admin-table">
            <table class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th width="50">#</th>
                        <th width="50">Название категории</th>
                        <th>Алиас</th>
                        <th>Тип</th>
                        <th>Вес</th>
                        <th><div class="text-right">Действия</div></th>
                    </tr>
                </thead>

                <tbody>
                    <c:forEach var="category" items="${categories}">
                        <tr>
                            <td>${category.categoryId}</td>
                            <td>${category.value}</td>
                            <td>${category.mnemonicName}</td>
                            <td>${category.tagType}</td>
                            <td>${category.preferredOrder}</td>

                            <td class="text-right">
                                <a href="/moderator/shop/product-tag.html?categoryId=${category.categoryId}" class="btn btn-primary btn-outline">
                                    <i class="fa fa-pencil"></i>
                                </a>
                            </td>
                        </tr>
                    </c:forEach>
                </tbody>
            </table>
        </div>
    </div>

    <%@ include file="/WEB-INF/jsp/admin/paginator.jsp" %>
</div>
</body>
</html>


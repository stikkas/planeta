<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ include file="/WEB-INF/jsp/includes/generated/doctype-header.jsp" %>
<head>
    <%@include file="head.jsp" %>
</head>

<body>

<%@ include file="/WEB-INF/jsp/admin/navbar.jsp" %>

<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Категории проектов</h1>
        </div>
    </div>

    <div class="main-page-actions">
        <form method="get">
            <a href="/admin/edit-category.html" class="btn btn-success btn-circle btn-outline btn-lg" title="Добавить">
                <i class="fa fa-plus"></i>
            </a>
        </form>
    </div>

    <div class="row">
        <div class="col-lg-12 admin-table">
            <table class="table table-striped table-hover table-bordered admin-banner-image">
                <thead>
                    <tr>
                        <td>
                            <strong>ID</strong>
                        </td>
                        <td>
                            <strong>Название</strong>
                        </td>
                        <td>
                            <strong>Английсккое название</strong>
                        </td>
                        <td>
                            <strong>Алиас</strong>
                        </td>
                        <td>
                            <strong>Алиас спонсора</strong>
                        </td>
                        <td>
                            <strong>Порядковый номер</strong>
                        </td>
                        <td>
                            <strong>Видимость категории</strong>
                        </td>
                        <td>
                            <strong>Комментарии</strong>
                        </td>
                        <td>
                            <strong>Действия</strong>
                        </td>
                    </tr>
                </thead>

                <c:forEach var="campaign" items="${campaignTags}">
                    <tbody>
                        <tr>
                            <td>${campaign.id}</td>
                            <td>${campaign.name}</td>
                            <td>${campaign.engName}</td>
                            <td>${campaign.mnemonicName}</td>
                            <td>${campaign.sponsorAlias}</td>
                            <td>${campaign.preferredOrder}</td>
                            <td>
                                <c:if test="${campaign.editorVisibility}">
                                    <span class="label label-success">редактор</span><br/>
                                </c:if>
                                <c:if test="${campaign.visibleInSearch}">
                                    <span class="label label-warning">поиск</span><br/>
                                </c:if>
                                <c:if test="${campaign.charityVisibility}">
                                    <span class="label label-info">благо</span>
                                </c:if>
                                <c:if test="${campaign.specialProject}">
                                    <span class="label label-danger">спецпроект</span>
                                </c:if>
                            </td>
                            <td>${campaign.warnText}</td>
                            <td class="text-right">
                                <a href="/admin/edit-category.html?campaignTagId=${campaign.id}"
                                   class="btn btn-primary btn-outline"
                                   title="Редактировать">
                                    <i class="fa fa-pencil"></i>
                                </a>
                            </td>
                        </tr>
                    </tbody>
                </c:forEach>
            </table>
        </div>
    </div>
</div>

</body>



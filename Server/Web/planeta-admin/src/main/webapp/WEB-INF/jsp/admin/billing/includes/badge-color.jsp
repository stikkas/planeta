<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<jsp:useBean id="typeColor" class="java.util.HashMap" scope="request"/>
<c:set target="${typeColor}" property="CAMPAIGN_NOT_ACTIVE" value="label-warning"/>
<c:set target="${typeColor}" property="NOT_ENOUGH_SHARE_AMOUNT" value="label-primary"/>
<c:set target="${typeColor}" property="NOT_ENOUGH_PRODUCT_AMOUNT" value="label-primary"/>
<c:set target="${typeColor}" property="ORDER_NOT_ENOUGH_MONEY_FOR_PURCHASE" value="label-danger"/>
<c:set target="${typeColor}" property="OTHER_ERRORS" value="label-danger"/>
<c:set target="${typeColor}" property="TWO_FAIL_PAYMENTS" value="label-info"/>
<c:set target="${typeColor}" property="LARGE_SUM_FAIL_PAYMENT" value="label-danger"/>

<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ include file="/WEB-INF/jsp/includes/generated/doctype-header.jsp" %>
<%@taglib prefix="p" uri="http://planeta.ru/tags" %>

<head>
    <%@ include file="/WEB-INF/jsp/admin/head.jsp" %>

    <link type="text/css" rel="stylesheet" href="/css/ui-lightness/jquery-ui-1.10.2.custom.min.css"/>
    <link type="text/css" rel="stylesheet" href="/css/jquery.timepicker.css"/>

    <style>
        .control-label > span {
            display: none;
        }

        .control-label > span.required {
            display: inline;
            color: red;
        }

        .my-error {
            margin-left: 8px;
        }
    </style>

    <script type="text/javascript">
        $(document).ready(function () {
            $("#date-box").datetimepicker();
            <c:if test="${not empty book.changePriceDate}">
                $("#date-box").data("DateTimePicker").date(new Date(${book.changePriceDate.time}));
            </c:if>


            $("#printingForm").submit(function () {
                var changePriceDate = $("#date-box").data("DateTimePicker").date().format('x');
                if (changePriceDate) {
                    $("#changePriceDate").val(changePriceDate);
                }

                return true;
            });


            $(document).on('click', '.js-add-house', function (e) {
                e.preventDefault();
                var houseName = $('.js-new-house-name').val();
                $.post("/admin/biblio/new-publishing-house.json",
                    {name: houseName}).done(function (response) {
                    if (response.success) {
                        $('#publishingHouseId').append($("<option></option>")
                            .attr("value", response.result)
                            .text(houseName));
                        $('.js-new-house-name').val('');
                    }
                });
            });

            var tags = $('#tags');
            tags.select2({
                tags: true
            });

            if ($('#imageImg').attr('data-url')) {
                $('#imageImg').attr('src', $('#imageImg').attr('data-url'));
            } else {
                $('#imageImg').attr('src', workspace.staticNodesService.getResourceUrl("/images/defaults/upload.png"));
            }

        });
    </script>
</head>
<body>

<%@ include file="/WEB-INF/jsp/admin/navbar.jsp" %>

<div id="page-wrapper">

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                <c:if test="${book.bookId == 0}">
                    Новое издание
                </c:if>
                <c:if test="${book.bookId != 0}">
                    Редактировать издание
                </c:if>
            </h1>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <form:form id="printingForm" class="form-horizontal" commandName="book" method="post">
                <input id="id" name="id" type="hidden" value="${book.bookId}">

                <div class="row ma-b-20">
                    <div class="col-lg-4">
                        <label for="title">Название <span class="required">*</span></label>
                        <form:input type="text" name="title" class="form-control" path="title"/>
                        <form:errors path="title" cssClass="error-message my-error" element="span"/>
                    </div>

                    <div class="col-lg-4">
                        <label for="price">Стоимость подписки <span class="required">*</span></label>
                        <input id="price" class="form-control" type="number" name="price" value="${book.price}">
                        <form:errors path="price" cssClass="error-message my-error" element="span"/>
                    </div>

                    <div class="col-lg-4">
                        <label for="newPrice">Новая цена</label>
                        <input id="newPrice" class="form-control" name="newPrice" type="number" value="${book.newPrice}">
                    </div>
                </div>

                <div class="row ma-b-20">
                    <div class="col-lg-4">
                        <label for="date-box">Дата изменения цены</label>
                        <input id="changePriceDate" path="changePriceDate" name="changePriceDate" type="hidden"/>
                        <input id="date-box" type="text" class="form-control" value="<fmt:formatDate pattern="dd-MM-yyyy hh:mm" value="${book.changePriceDate}" />"/>
                    </div>

                    <div class="col-lg-4">
                        <label>Категория издания <span class="required">*</span></label>
                        <select class="form-control" id="tags" name="tagIds" multiple>
                            <c:forEach items="${bookTags}" var="tag">
                                <c:if test="${hf:containsObjectInList(book.tags, tag)}">
                                    <option value="${tag.tagId}" selected="selected">${tag.name}</option>
                                </c:if>
                                <c:if test="${!hf:containsObjectInList(book.tags, tag)}">
                                    <option value="${tag.tagId}">${tag.name}</option>
                                </c:if>
                            </c:forEach>
                        </select>
                        <form:errors path="tags" cssClass="error-message my-error" element="span"/>
                    </div>


                    <div class="col-lg-4">
                        <label for="status">Статус издания</label>
                        <select id="status" name="status" class="form-control">
                            <c:forEach items="${bookStatuses}" var="status">
                                <c:if test="${book.status == status}">
                                    <option selected="selected">${status}</option>
                                </c:if>
                                <c:if test="${book.status != status}">
                                    <option>${status}</option>
                                </c:if>
                            </c:forEach>
                        </select>
                    </div>
                </div>

                <div class="row ma-b-20">
                    <div class="col-lg-12">
                        <label for="description">Описание <span class="required">*</span></label>
                        <textarea class="form-control" id="description" name="description" rows="6">${book.description}</textarea>
                        <form:errors path="description" cssClass="error-message my-error" element="span"/>
                    </div>
                </div>

                <div class="row ma-b-20">
                    <div class="col-lg-12">
                        <ct:file url="${book.imageUrl}" path="imageUrl" type="IMAGE"/>
                    </div>
                </div>

                <div class="row ma-b-20">
                    <div class="col-lg-4">
                        <label for="publishingHouseId">Издательство</label>
                        <select id="publishingHouseId" name="publishingHouseId" class="form-control">
                            <c:forEach items="${houses}" var="house">
                                <c:if test="${book.publishingHouseId == house.publishingHouseId}">
                                    <option selected="selected"
                                            value="${house.publishingHouseId}">${house.name}</option>
                                </c:if>
                                <c:if test="${book.publishingHouseId != house.publishingHouseId}">
                                    <option value="${house.publishingHouseId}">${house.name}</option>
                                </c:if>
                            </c:forEach>
                        </select>
                    </div>

                    <div class="col-lg-4">
                        <label for="periodicity">Периодичность</label>
                        <input class="form-control" id="periodicity" name="periodicity" value="${book.periodicity}">
                    </div>

                    <div class="col-lg-4">
                        <label for="bonus">Персональный бонус</label>
                        <input id="bonus" name="bonus" type="text" class="form-control" value="${book.bonus}">
                    </div>
                </div>

                <div class="row ma-b-20">
                    <div class="col-lg-12">
                        <label for="comment">Комментарии</label>
                        <textarea class="form-control" id="comment" name="comment" rows="6">${book.comment}</textarea>
                    </div>
                </div>

                <div class="row ma-b-20">
                    <div class="col-lg-4">
                        <label for="email">Контактный email</label>
                        <input id="email" name="email" type="text" class="form-control" value="${book.email}">
                    </div>

                    <div class="col-lg-4">
                        <label for="contact">Контактное лицо</label>
                        <input id="contact" name="contact" type="text" class="form-control" value="${book.contact}">
                    </div>

                    <div class="col-lg-4">
                        <label for="site">Сайт издания</label>
                        <input id="site" name="site" type="text" class="form-control" value="${book.site}">
                    </div>
                </div>

                <hr/>

                <div class="row ma-b-20">
                    <div class="col-lg-3">
                        <label for="index">Показывать на главной</label>
                        <input id="index" name="index" type="number" class="form-control" value="${book.index}">
                        <p class="help-block">
                            Число больше 0 - издание показывается на главной. Чем больше число тем ближе к началу списка
                        </p>
                    </div>
                </div>


                <div class="btn-group">
                    <c:if test="${book.bookId == 0}">
                        <button type="submit"
                                formaction="/admin/save-book.html"
                                class="btn btn-primary">
                            Добавить
                        </button>
                    </c:if>
                    <c:if test="${book.bookId != 0}">
                        <button type="submit"
                                formaction="/admin/save-book.html?bookId=${book.bookId}"
                                class="btn btn-primary">
                            Сохранить
                        </button>
                    </c:if>
                    <a class="btn btn-default" href="/admin/book/books.html">Отмена</a>
                </div>
            </form:form>
        </div>
    </div>
</div>
</body>
</html>


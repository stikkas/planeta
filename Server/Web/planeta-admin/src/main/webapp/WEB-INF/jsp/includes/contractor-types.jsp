<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<c:choose>
    <c:when test="${type == 'INDIVIDUAL'}">
        Физическое лицо
    </c:when>
    <c:when test="${type == 'LEGAL_ENTITY'}">
        Юридическое лицо
    </c:when>
    <c:when test="${type == 'OOO'}">
        ООО
    </c:when>
    <c:when test="${type == 'CHARITABLE_FOUNDATION'}">
        Некоммерческая организация
    </c:when>
    <c:when test="${type == 'INDIVIDUAL_ENTREPRENEUR'}">
        ИП
    </c:when>
    <c:otherwise>
        Неизвестный, обратитесь к разработчикам
    </c:otherwise>
</c:choose>

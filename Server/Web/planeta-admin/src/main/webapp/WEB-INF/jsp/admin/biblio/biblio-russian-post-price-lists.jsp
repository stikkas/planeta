<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ include file="/WEB-INF/jsp/includes/generated/doctype-header.jsp" %>
<head>
    <title>Прайс листы изданий Почты России</title>
    <%@ include file="../head.jsp" %>
</head>

<body>

<%@ include file="../navbar.jsp" %>

<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Почта России, прайс-листы изданий</h1>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12 admin-table">
            <table class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th style="text-align: center; vertical-align: middle;">ID</th>
                        <th style="text-align: center; vertical-align: middle;">Название</th>
                        <th style="text-align: center; vertical-align: middle;">Дата добавления</th>
                        <th></th>
                    </tr>
                </thead>

                <tbody>
                    <c:forEach items="${magazinePriceLists}" var="magazinePriceList" varStatus="varStatus">
                        <tr>
                            <td>${magazinePriceList.priceListId}</td>
                            <td>${hf:escapeHtml4(magazinePriceList.name)}</td>
                            <td>${hf:dateFormatByTemplate(magazinePriceList.timeAdded, 'dd MMMM yyyy в HH:mm')}</td>
                            <td>
                                <a href="/moderator/biblio-russian-post-magazine-info-list.html?priceListId=${magazinePriceList.priceListId}"
                                   target="_blank">Открыть</a>
                            </td>
                        </tr>
                    </c:forEach>
                </tbody>
            </table>
        </div>
    </div>
</div>
</body>
</html>


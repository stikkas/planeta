<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ include file="/WEB-INF/jsp/includes/generated/doctype-header.jsp" %>
<head>
    <%@ include file="/WEB-INF/jsp/admin/head.jsp" %>
    <script type="text/javascript">
        $(document).ready(function(e){
            var $btnChangeStatus = $('.js-confirmation-campaign-change-status');
            var $changeAction = $('.js-change-action');

            $btnChangeStatus.click(function(e) {
                confirmationCampaignChangeStatus(e);
            });

            $changeAction.click(function(e) {
                var $actionButton = $('.js-change-action');
                $actionButton.removeClass('btn-success');
                $actionButton.removeClass('btn-danger');
                handleCampaignChangeStatus(e);
            });

            var confirmationCampaignChangeStatus = function(e) {
                var $inputCampaignIDVal = $('.js-campaign-change-input').val();
                e.preventDefault();
                if(!isNumber($inputCampaignIDVal)) {
                    return;
                }
                $.ajax({
                    url: "/admin/confirmation-campaign-change-status.json",
                    type: 'post',
                    data: {
                        campaignId : $inputCampaignIDVal
                    },
                    success: function (response) {
                        if (response && response.result) {
                            var campaign = response.result;

                            if (campaign.status === 'FINISHED' || campaign.status === 'ACTIVE') {
                                $('input[name=campaignId]').val(campaign.campaignId);
                                $('div[name=campaignName]').text(campaign.name);
                                $('img[name=campaignImage]').attr('src', campaign.imageUrl);
                                if (campaign.status === 'FINISHED') {
                                    var $actionButton = $('.js-change-action');
                                    $actionButton.addClass('btn-success');
                                    $actionButton.text('Запустить');

                                    $('input[name=campaignStatus]').val('ACTIVE');
                                }

                                if (campaign.status === 'ACTIVE') {
                                    var $actionButton = $('.js-change-action');
                                    $actionButton.addClass('btn-danger');
                                    $actionButton.text('Завершить');

                                    $('input[name=campaignStatus]').val('FINISHED');
                                }
                                $('#confirmation-campaign-change-status').modal('show');
                            } else {
                                $('#error-campaign-change-status').modal('show');
                            }
                        } else {
                            $('#not-found-campaign').modal('show');
                        }
                    }
                });
            };

            var handleCampaignChangeStatus = function(e) {
                var $inputCampaignIDVal = $('.js-campaign-change-input').val();
                e.preventDefault();
                if(!isNumber($inputCampaignIDVal)) {
                    return;
                }
                $.ajax({
                    url: "/admin/handle-campaign-change-status.json",
                    type: 'post',
                    data: {
                        campaignId : $inputCampaignIDVal
                    },
                    success: function (response) {
                        if (response) {
                            $('#success-campaign-change-status').modal('show');
                        } else {
                            $('#access-error-campaign-change-status').modal('show');
                        }
                    }
                });
            };
            var isNumber = function(n) {
                return !isNaN(parseFloat(n)) && isFinite(n);
            };
        });
    </script>

    <style>
        .custom-modal {
            position: absolute;
            margin: auto;
            left: 0;
            right: 0;
        }
    </style>
</head>

<body>
<%@ include file="/WEB-INF/jsp/admin/navbar.jsp" %>

<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Управление проектами</h1>
        </div>
    </div>

    <div class="panel panel-primary">
        <div class="panel-heading">Отправить запрос на изменение статуса проекта</div>
        <div class="panel-body">
            <form method="post">
                <div class="form-group input-group">
                    <input type="text" name="campaignId" placeholder="ID проекта" class="form-control js-campaign-change-input"/>

                    <span class="input-group-btn">
                        <button formaction="/admin/confirmation-campaign-change-status.json"
                                class="btn btn-primary js-confirmation-campaign-change-status"
                                title="Изменить статус">
                            <i class="fa fa-check"></i>
                        </button>
                    </span>
                </div>

                <button class="btn btn-success" style="display: none;"><i class="icon-ok icon-white" style=" margin-right: 5px;"></i></button>
                <button class="btn btn-danger" style="display: none; font-size: 14px;font-weight: 700;padding: 6px 20px;">Такого проекта не существует</button>
            </form>
        </div>
    </div>

    <div id="confirmation-campaign-change-status" class="modal-dialog custom-modal hide fade" tabindex="-1" role="dialog" aria-labelledby="confirm-title" aria-hidden="true">
        <div class="modal">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h3>Изменение статуса проекта</h3>
            </div>
            <div class="modal-body">
                <form class="span5">
                    <p><b>Название проекта</b>:
                        <div name="campaignName"></div>
                        <div>
                            <img name="campaignImage" src="" height="50px"/>
                        </div>
                    </p>
                    <input type="hidden" name="campaignId" />
                    <input type="hidden" name="campaignStatus" />
                    <p><b>Вы действительно хотите изменить статус проекта?</b></p>
                </form>
            </div>
            <div class="modal-footer">
                <button class="btn confirm js-change-action" data-dismiss="modal">Да</button>
                <button class="btn" data-dismiss="modal">Отмена</button>
            </div>
        </div>
    </div>

    <div id="error-campaign-change-status" class="modal-dialog custom-modal hide fade" tabindex="-1" role="dialog" aria-labelledby="confirm-title" aria-hidden="true">
        <div class="modal">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h3>Запрещено</h3>
            </div>
            <div class="modal-body">
                <form class="span5">
                    <p>С помощью данного метода доступны <b>только</b> следующие изменения статуса:</p>
                    <p>
                        <ul>
                            <li>Завершенный -> Активный</li>
                            <li>Активный -> Завершенный</li>
                        </ul>
                    </p>
                    <p>Данный проект не находится ни в одном из этих статусов.</p>
                </form>
            </div>
            <div class="modal-footer">
                <button class="btn" data-dismiss="modal">Понятно :-(</button>
            </div>
        </div>
    </div>

    <div id="success-campaign-change-status" class="modal-dialog custom-modal hide fade" tabindex="-1" role="dialog" aria-labelledby="confirm-title" aria-hidden="true">
        <div>
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h3>Успех</h3>
            </div>
            <div class="modal-body">
                <form class="span5">
                    <div class="alert alert-success">
                        Статус проекта успешно изменен
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button class="btn" data-dismiss="modal">ОК</button>
            </div>
        </div>
    </div>

    <div id="access-error-campaign-change-status" class="modal-dialog custom-modal hide fade" tabindex="-1" role="dialog" aria-labelledby="confirm-title" aria-hidden="true">
        <div class="modal">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h3>Ошибка</h3>
            </div>
            <div class="modal-body">
                <form class="span5">
                    <div class="alert alert-error">
                        У вас нет прав для совершения этого действия
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button class="btn" data-dismiss="modal">Понятно :-(</button>
            </div>
        </div>
    </div>

    <div id="not-found-campaign" class="modal-dialog hide fade custom-modal" tabindex="-1" role="dialog" aria-labelledby="confirm-title" aria-hidden="true">
        <div class="modal">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h3>Не найдено</h3>
            </div>
            <div class="modal-body">
                <form class="span5">
                    <div class="alert alert-error">
                        Проект с таким номером не найден. Проверьте правильность ввода.
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button class="btn" data-dismiss="modal">Понятно :-(</button>
            </div>
        </div>
    </div>
</div>
</div>
</body>
</html>


<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ include file="/WEB-INF/jsp/includes/generated/doctype-header.jsp" %>

<head>
    <%@ include file="head.jsp" %>

    <link type="text/css" rel="stylesheet" href="/css/ui-lightness/jquery-ui-1.10.2.custom.min.css"/>
    <link type="text/css" rel="stylesheet" href="/css/jquery.timepicker.css"/>

    <script type="text/javascript">
        $(document).ready(function () {
            $("#date-box-start").datetimepicker();
            <c:if test="${not empty broadcast.timeBegin}">
                $("#date-box-start").data("DateTimePicker").date(new Date(${broadcast.timeBegin.time}));
            </c:if>
            $("#date-box-finish").datetimepicker();
            <c:if test="${not empty broadcast.timeEnd}">
                $("#date-box-finish").data("DateTimePicker").date(new Date(${broadcast.timeEnd.time}));
            </c:if>

            $("#broadcast-form").submit(function () {
                var dateBegin = $("#date-box-start").data("DateTimePicker").date().format('x');
                if (dateBegin) {
                    $("#timeBegin").val(dateBegin);
                }
                var dateEnd = $("#date-box-finish").data("DateTimePicker").date().format('x');
                if (dateEnd) {
                    $("#timeEnd").val(dateEnd);
                }
                return true;
            });

            $(document).on('click', ".upload-image", function () {
                var container = $(this).parent();
                UploadController.showUploadBroadcastAvatar(-1, function (filesUploaded) {
                    if (!filesUploaded || !filesUploaded.length) {
                        return;
                    }
                    var uploadResult = filesUploaded.at(0).get('uploadResult');
                    if (!uploadResult) {
                        return;
                    }
                    container.find('.image-url').val(uploadResult.imageUrl);
                    container.find('.image-id').val(uploadResult.objectId);
                    container.find('.image-img').attr('src', uploadResult.imageUrl);
                });
            });

            $(".city-name-holder").each(function (index, el) {
                $.ajax({
                    url: '/api/util/city-by-id.json',
                    data: {
                        cityId: $(el).html()
                    },
                    success: function (response) {
                        $(el).html(response.nameRus);
                    }
                });
            });

            $(document).on('click', '.toggler', function () {
                var $this = $(this);
                var targetSelector = $this.data('targetSelector');
                var $target = $(targetSelector);
                if ($this.prop('checked')) {
                    $target.removeClass('hidden');
                } else {
                    $target.addClass('hidden');
                }
            });

        });
    </script>

</head>
<body>

<%@ include file="/WEB-INF/jsp/admin/navbar.jsp" %>

<div id="page-wrapper">

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Настройки трансляции</h1>
        </div>
    </div>

    <c:if test="${broadcast.broadcastId > 0}">
        <div class="main-page-actions">
            <a class="btn btn-primary btn-circle btn-outline btn-lg"
               href="//${properties['tv.application.host']}/broadcast/${broadcast.broadcastId}"
               target="_blank"
               title="Страница трансляции">
                <i class="fa fa-file-o"></i>
            </a>

            <c:if test="${broadcast.broadcastStatus != \"LIVE\"}">
                <a class="btn btn-success btn-circle btn-outline btn-lg"
                   href="/moderator/event-broadcast-start.html?broadcastId=${broadcast.broadcastId}"
                   title="Запустить">
                    <i class="fa fa-play"></i>
                </a>
            </c:if>

            <c:if test="${broadcast.broadcastStatus == \"LIVE\"}">
                <a class="btn btn-warning btn-circle btn-outline btn-lg"
                   href="/moderator/event-broadcast-stop.html?broadcastId=${broadcast.broadcastId}"
                    title="Остановить">
                    <i class="fa fa-stop"></i>
                </a>
            </c:if>

            <a class="btn btn-default btn-circle btn-outline btn-lg"
               href="/moderator/event-broadcast-pause.html?broadcastId=${broadcast.broadcastId}"
                title="Пауза">
                <i class="fa fa-pause"></i>
            </a>

            <a class="btn btn-info btn-circle btn-outline btn-lg"
               href="/moderator/event-broadcast-private-targeting.html?broadcastId=${broadcast.broadcastId}"
                title="Сгенерировать ссылку для просмотра трансляции">
                <i class="fa fa-link"></i>
            </a>

            <a class="btn btn-danger btn-circle btn-outline btn-lg"
               href="/moderator/event-broadcast-remove.html?broadcastId=${broadcast.broadcastId}"
               onclick="return confirm('Вы действительно хотите удалить трансляцию?');"
               title="Удалить">
                <i class="fa fa-trash"></i>
            </a>
        </div>
    </c:if>

    <div class="row ma-b-20">
        <div class="col-lg-12">
            <c:if test="${broadcast.broadcastId > 0}">
                <label>
                    Текущий статус:
                    <c:choose>
                        <c:when test="${broadcast.broadcastStatus == \"NOT_STARTED\"}">
                            <span class="label label-default">Не началась</span>
                        </c:when>
                        <c:when test="${broadcast.broadcastStatus == \"LIVE\"}">
                            <span class="label label-info">Проигрывается</span>
                        </c:when>
                        <c:when test="${broadcast.broadcastStatus == \"PAUSED\"}">
                            <span class="label label-warning">Пауза</span>
                        </c:when>
                        <c:when test="${broadcast.broadcastStatus == \"FINISHED\"}">
                            <span class="label label-success">Завершена</span>
                        </c:when>
                    </c:choose>
                </label>
            </c:if>
        </div>
    </div>

    <div class="row ma-b-30">
        <div class="col-lg-12">
            <form:form id="broadcast-form" method="post" commandName="broadcast"
                       class="form-horizontal">
                <form:input type="hidden" path="broadcastId"/>
                <form:input type="hidden" path="defaultStreamId"/>
                <form:input type="hidden" path="profileId"/>
                
                <div class="row ma-b-20">
                    <div class="col-lg-3">
                        <label>Название</label>
                        <form:input type="text" path="name" cssClass="form-control"/> <form:errors path="name"><span
                            class="help-inline"><form:errors path="name"/></span></form:errors>
                    </div>

                    <div class="col-lg-3">
                        <label>Время начала</label>
                        <form:input id="timeBegin" type="hidden" path="timeBegin" cssClass="form-control"/>
                        <input id="date-box-start" class="form-control"
                               value='' type="text"/>
                        <form:errors path="timeBegin"><span class="help-inline"><form:errors
                                path="timeBegin"/></span></form:errors>
                    </div>

                    <div class="col-lg-3">
                        <label>Время окончания</label>
                        <form:input id="timeEnd" type="hidden" path="timeEnd" cssClass="form-control"/>
                        <input id="date-box-finish" class="form-control" value='' type="text"/>
                        <form:errors path="timeEnd"><span class="help-inline"><form:errors
                                path="timeEnd"/></span></form:errors>
                    </div>

                    <div class="col-lg-3">
                        <label>Категория трансляции</label>
                        <form:select path="broadcastCategory" cssClass="form-control">
                            <option value="CONCERTS"
                                    <c:if test="${broadcast.broadcastCategory == \"CONCERTS\"}">selected="selected"</c:if>>
                                Концерты
                            </option>
                            <option value="NASHE_360"
                                    <c:if test="${broadcast.broadcastCategory == \"NASHE_360\"}">selected="selected"</c:if>>
                                Наше 360
                            </option>
                            <option value="ALIVE"
                                    <c:if test="${broadcast.broadcastCategory == \"ALIVE\"}">selected="selected"</c:if>>
                                Живые
                            </option>
                            <option value="SCHOOL"
                                    <c:if test="${broadcast.broadcastCategory == \"SCHOOL\"}">selected="selected"</c:if>>
                                Школа краудфандинга
                            </option>
                        </form:select>
                    </div>
                </div>

                <div class="row ma-b-20">
                    <div class="col-lg-6">
                        <label>Описание</label>
                        <form:textarea path="description" cssClass="form-control" rows="10"/> <form:errors
                            path="description"><span
                            class="help-inline"><form:errors path="description"/></span></form:errors>
                    </div>

                    <div class="col-lg-6">
                        <label>Баннер</label>
                        <form:textarea path="advertismentBannerHtml" cssClass="form-control" rows="10"/> <form:errors
                            path="advertismentBannerHtml"><span class="help-inline"><form:errors
                            path="advertismentBannerHtml"/></span></form:errors>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-3">
                        <div class="checkbox">
                            <label>
                                <form:checkbox path="autoStart" />
                                Запускать и останавливать трансляцию автоматически
                            </label>
                        </div>
                    </div>

                    <div class="col-lg-3">
                        <div class="checkbox">
                            <label>
                                <form:checkbox path="autoSubscribe" />
                                Автоподписка зрителей
                            </label>
                        </div>
                    </div>

                    <div class="col-lg-3">
                        <div class="checkbox">
                            <label>
                                <form:checkbox path="archived" />
                                Доступна видеозапись трансляции
                            </label>
                        </div>
                    </div>

                    <div class="col-lg-3">
                        <div class="checkbox">
                            <label>
                                <form:checkbox path="closed" />
                                Закрытая трансляция
                            </label>
                        </div>
                    </div>
                </div>

                <div class="row ma-b-20">
                    <div class="col-lg-3">
                        <div class="checkbox">
                            <label>
                                <form:checkbox path="visibleOnDashboard" />
                                Трансляция будет доступна на витрине сервиса ТВ
                            </label>
                        </div>
                    </div>

                    <div class="col-lg-3">
                        <div class="checkbox">
                            <label>
                                <form:checkbox path="chatEnabled" />
                                Доступен чат трансляции
                            </label>
                        </div>
                    </div>

                    <div class="col-lg-3">
                        <div class="checkbox">
                            <label>
                                <form:checkbox path="anonymousCanView" />
                                Доступна незарегистрированным пользователям
                            </label>
                        </div>
                    </div>
                </div>

                <div class="row ma-b-20">
                    <div class="col-lg-6">
                        <label>Кол-во фальшивых пользователей в чате</label>
                        <form:input path="chatFakeUsersCount" min="0" type="number" cssClass="form-control" />
                        <form:errors path="chatFakeUsersCount"><span class="help-inline"><form:errors
                                path="chatFakeUsersCount"/></span></form:errors>

                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-6">
                        <ct:file url="${broadcast.closedImageUrl}" path="closedImageUrl" type="IMAGE" label="Заглушка для ограничений"/>
                    </div>
                </div>

                <div class="row ma-b-20">
                    <div class="col-lg-6">
                        <div class="checkbox">
                            <label>
                                <form:checkbox path="closedCustomDescription" />Выводить текст на заглушке
                            </label>
                        </div>
                        <form:textarea path="closedDescription" cssClass="form-control" rows="5"
                                       placeholder="К сожалению, у Вас нет доступа к этой трансляции."/>
                        <form:errors
                                path="closedDescription"><span
                                class="help-inline"><form:errors path="closedDescription"/></span></form:errors>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-6">
                        <ct:file url="${broadcast.imageUrl}" path="imageUrl" type="IMAGE" label="Заглушка до начала"/>
                    </div>
                </div>

                <div class="row ma-b-20">
                    <div class="col-lg-6">
                        <div class="checkbox">
                            <label>
                                <form:checkbox path="imageCustomDescription" />Выводить текст на заглушке
                            </label>
                        </div>
                        <form:textarea path="imageDescription" cssClass="form-control" rows="5"
                                       placeholder="Трансляция еще не началась."/> <form:errors
                            path="imageDescription"><span
                            class="help-inline"><form:errors path="imageDescription"/></span></form:errors>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-6">
                        <ct:file url="${broadcast.pausedImageUrl}" path="pausedImageUrl" type="IMAGE" label="Заглушка на паузе"/>
                    </div>
                </div>

                <div class="row ma-b-20">
                    <div class="col-lg-6">
                        <div class="checkbox">
                            <label>
                                <form:checkbox path="pausedCustomDescription" />Выводить текст на заглушке
                            </label>
                        </div>
                        <form:textarea path="pausedDescription" cssClass="form-control" rows="5"
                                       placeholder="Оставайтесь с нами. Трансляция возобновится через несколько минут"/>
                        <form:errors
                                path="pausedDescription"><span
                                class="help-inline"><form:errors path="pausedDescription"/></span></form:errors>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-6">
                        <ct:file url="${broadcast.finishedImageUrl}" path="finishedImageUrl" type="IMAGE" label="Заглушка после"/>
                    </div>
                </div>

                <div class="row ma-b-20">
                    <div class="col-lg-6">
                        <div class="checkbox">
                            <label>
                                <form:checkbox path="finishedCustomDescription" />Выводить текст на заглушке
                            </label>
                        </div>
                        <form:textarea path="finishedDescription" cssClass="form-control" rows="5"
                                       placeholder="Трансляция завершена. Спасибо за внимание!"/> <form:errors
                            path="finishedDescription"><span
                            class="help-inline"><form:errors path="finishedDescription"/></span></form:errors>
                    </div>
                </div>

                <div class="btn-group">
                    <button type="submit" class="btn btn-primary">Сохранить</button>
                    <a class="btn btn-default" href="/admin/book/books.html">Отмена</a>
                </div>
            </form:form>
        </div>
    </div>

    <c:if test="${broadcast.broadcastId > 0}">
        <h3>Потоки</h3>

        <div class="row">
            <div class="col-lg-12">
                <a class="btn btn-success"
                   href="/moderator/event-broadcast-stream-info.html?broadcastId=${broadcast.broadcastId}&streamId=0">
                    <i class="fa fa-plus"></i>
                    Добавить поток
                </a>
                <br/>
                <br/>
                <table class="table table-striped table-hover table-bordered">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Название</th>
                            <th>Тип</th>
                            <th>Статус</th>
                            <th></th>
                        </tr>
                    </thead>

                    <tbody>
                    <c:forEach var="stream" items="${streams}">
                        <tr>
                            <td>
                                <img src="${hf:getGroupAvatarUrl(stream.imageUrl, "USER_SMALL_AVATAR")}"
                                     alt="${stream.name}"/>
                            </td>
                            <td>
                                <a href="/moderator/event-broadcast-stream-info.html?broadcastId=${broadcast.broadcastId}&streamId=${stream.streamId}">
                                        ${stream.name}
                                </a>
                            </td>
                            <td>
                                <c:choose>
                                    <c:when test="${stream.broadcastType == \"VIDEO\"}">
                                        Видео
                                    </c:when>
                                    <c:when test="${stream.broadcastType == \"AUDIO\"}">
                                        Аудио
                                    </c:when>
                                    <c:when test="${stream.broadcastType == \"EMBED\"}">
                                        Встраиваемое видео
                                    </c:when>
                                </c:choose>
                            </td>
                            <td>
                                <c:choose>
                                    <c:when test="${stream.broadcastStatus == \"NOT_STARTED\"}">
                                        <span class="label label-default">Не началось</span>
                                    </c:when>
                                    <c:when test="${stream.broadcastStatus == \"LIVE\"}">
                                        <span class="label label-success">Проигрывается</span>
                                    </c:when>
                                    <c:when test="${stream.broadcastStatus == \"PAUSED\"}">
                                        <span class="label label-warning">Пауза</span>
                                    </c:when>
                                    <c:when test="${stream.broadcastStatus == \"FINISHED\"}">
                                        <span class="label label-info">Окончилось</span>
                                    </c:when>
                                </c:choose>

                                <c:if test="${broadcast.defaultStreamId == stream.streamId}">
                                    <span class="label label-info">Поток по умолчанию</span>
                                </c:if>
                            </td>
                            <td class="text-right">
                                <c:if test="${broadcast.defaultStreamId != stream.streamId}">
                                    <a href="/moderator/event-broadcast-set-default-stream.html?broadcastId=${broadcast.broadcastId}&streamId=${stream.streamId}"
                                       class="btn btn-primary btn-outline"
                                       title="Установить главным">
                                        <i class="fa fa-home"></i>
                                    </a>
                                </c:if>

                                <a href="/moderator/event-broadcast-stream-remove.html?broadcastId=${broadcast.broadcastId}&streamId=${stream.streamId}"
                                   onclick="return confirm('Вы действительно хотите удалить этот поток?');"
                                   class="btn btn-danger btn-outline"
                                   title="Удалить">
                                    <i class="fa fa-trash"></i>
                                </a>

                                <c:if test="${stream.streamId > 0 && broadcast.broadcastStatus == 'LIVE'}">
                                    <c:if test="${stream.broadcastStatus != 'PAUSED'}">
                                        <a class="btn btn-primary"
                                            href="/moderator/event-broadcast-toggle-stream-state.html?&broadcastId=${broadcast.broadcastId}&streamId=${stream.streamId}"
                                            title="Остановить">
                                            <i class="fa fa-stop"></i>
                                        </a>
                                    </c:if>

                                    <c:if test="${stream.broadcastStatus == 'PAUSED'}">
                                        <a class="btn btn-primary"
                                           href="/moderator/event-broadcast-toggle-stream-state.html?&broadcastId=${broadcast.broadcastId}&streamId=${stream.streamId}"
                                           title="Запустить">
                                            <i class="fa fa-play"></i>
                                        </a>
                                    </c:if>
                                </c:if>
                            </td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
            </div>
        </div>
    </c:if>

    <c:if test="${broadcast.broadcastId > 0}">
        <h3>Реклама</h3>
        <%--todo add copy configuration functional--%>
        <div class="row">
            <div class="col-lg-12">
                <div class="btn-group ma-b-20">
                    <c:if test="${vastDTO.adId > 0}">
                        <a class="btn btn-danger"
                           href="/moderator/event-broadcast-delete-advertising.html?broadcastId=${broadcast.broadcastId}">
                            <i class="fa fa-trash"></i>
                            Удалить рекламу
                        </a>
                        <c:forEach var="stream" items="${streams}">
                            <a class="btn btn-info" target="_blank"
                               href="https://${properties['tv.application.host']}/broadcast-stream-content.html?broadcastId=${broadcast.broadcastId}&streamId=${stream.streamId}">
                                <i class="fas fa-stethoscope"></i>
                                Тест потока "${stream.name}"
                            </a>
                        </c:forEach>
                    </c:if>

                    <c:if test="${vastDTO.adId <= 0}">
                        <a class="btn btn-success"
                           href="/moderator/event-broadcast-add-advertising.html?broadcastId=${broadcast.broadcastId}">
                            <i class="fa fa-plus"></i>
                            Добавить рекламу
                        </a>
                    </c:if>
                </div>

                <table class="table table-striped table-hover table-bordered">
                    <thead>
                    <tr>
                        <th>Название</th>
                        <th>Статус</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:if test="${vastDTO.adId > 0}">
                        <tr>
                            <td>
                                <a href="/moderator/event-broadcast-add-advertising.html?broadcastId=${broadcast.broadcastId}">
                                        ${vastDTO.adName}
                                </a>
                            </td>
                            <td>
                                    ${advertisingDTO.state}
                            </td>
                        </tr>
                    </c:if>
                    </tbody>
                </table>
            </div>
        </div>
    </c:if>

    <c:if test="${broadcast.broadcastId > 0}">
        <h3>Гео ограничения</h3>

        <div class="row">
            <div class="col-lg-12">
                <div class="btn-group ma-b-20">
                    <a class="btn btn-success"
                       href="/moderator/event-broadcast-geo-targeting-add.html?profileId=${broadcast.profileId}&broadcastId=${broadcast.broadcastId}">
                        <i class="fa fa-plus"></i>
                        Добавить
                    </a>
                    <a class="btn btn-danger"
                       href="/moderator/event-broadcast-geo-targeting-remove.html?broadcastId=${broadcast.broadcastId}"
                       onclick="return confirm('Вы действительно хотите удалить все ограничения?');">
                        <i class="fa fa-trash"></i>
                        Удалить все
                    </a>
                </div>

                <table class="table table-striped table-hover table-bordered">
                    <thead>
                        <tr>
                            <th>Страна</th>
                            <th>Город</th>
                            <th>Статус</th>
                            <th></th>
                        </tr>
                    </thead>

                    <tbody>
                    <c:forEach var="geoTargetingItem" items="${geoTargeting}">
                        <tr>
                            <td>
                                <select name="countryId" id="country-id" disabled>
                                    <c:forEach items="${countriesList}" var="country">
                                        <option
                                                <c:if test="${country.countryId == geoTargetingItem.countryId}">selected</c:if>
                                                value="${country.countryId}">${country.countryNameRus}
                                        </option>
                                    </c:forEach>
                                </select>
                            </td>

                            <td>
                                <c:if test="${geoTargetingItem.cityId > 0}">
                                    <span class="city-name-holder">${geoTargetingItem.cityId}</span>
                                </c:if>
                            </td>

                            <td>
                                <c:if test="${geoTargetingItem.allowed}">
                                    <span class="label label-success">Разрешено</span>
                                </c:if>
                                <c:if test="${!geoTargetingItem.allowed}">
                                    <span class="label label-danger">Запрещено</span>
                                </c:if>
                            </td>

                            <td class="text-right">
                                <a href="/moderator/event-broadcast-geo-targeting-remove-by-params.html?broadcastId=${broadcast.broadcastId}&countryId=${geoTargetingItem.countryId}&cityId=${geoTargetingItem.cityId}&allowed=${geoTargetingItem.allowed}"
                                   onclick="return confirm('Вы действительно хотите удалить это ограничение?');"
                                    class="btn btn-danger btn-outline"
                                    title="Удалить">
                                    <i class="fa fa-trash"></i>
                                </a>
                            </td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
            </div>
        </div>
    </c:if>

    <c:if test="${broadcast.broadcastId > 0}">
        <h3>Ограничения по акционерам</h3>

        <div class="row">
            <div class="col-lg-12">
                <a class="btn btn-success ma-b-20"
                   href="/moderator/event-broadcast-backers-targeting-add.html?profileId=${broadcast.profileId}&broadcastId=${broadcast.broadcastId}">
                    <i class="fa fa-plus"></i>
                    Добавить
                </a>

                <table class="table table-striped table-hover table-bordered">
                    <thead>
                    <tr>
                        <th>ID Проекта</th>
                        <th>Сумма</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                    </tr>
                    <c:forEach var="backersTargetingItem" items="${backersTargeting}">
                        <tr>
                            <td>
                                    ${backersTargetingItem.campaignId}
                            </td>
                            <td>
                                    ${backersTargetingItem.limitSumm}
                            </td>
                            <td>
                                <a class="btn btn-danger btn-outline"
                                   href="/moderator/event-broadcast-backers-targeting-remove.html?broadcastId=${broadcast.broadcastId}&campaignId=${backersTargetingItem.campaignId}"
                                   onclick="return confirm('Вы действительно хотите удалить ограничение?');"
                                   title="Удалить">
                                    <i class="fa fa-trash"></i>
                                </a>
                            </td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
            </div>
        </div>
    </c:if>


    <c:if test="${broadcast.broadcastId > 0}">
        <h3>Ограничения по покупателям товаров</h3>

        <div class="row">
            <div class="col-lg-12">
                <a class="btn btn-success ma-b-20"
                   href="/moderator/event-broadcast-product-targeting-add.html?broadcastId=${broadcast.broadcastId}">
                    <i class="fa fa-plus"></i>
                    Добавить
                </a>

                <table class="table table-striped table-hover table-bordered">
                    <thead>
                    <tr>
                        <th>Товар</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                    </tr>
                    <c:forEach var="productTargetingItem" items="${productTargeting}">
                        <tr>
                            <td>
                                    ${productTargetingItem.productId}
                            </td>
                            <td>
                                <a class="btn btn-danger btn-outline"
                                   href="/moderator/event-broadcast-product-targeting-remove.html?broadcastId=${broadcast.broadcastId}&productId=${productTargetingItem.productId}"
                                   onclick="return confirm('Вы действительно хотите удалить ограничение?');"
                                   title="Удалить">
                                    <i class="fa fa-trash"></i>
                                </a>
                            </td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
            </div>
        </div>
    </c:if>

</div>
</body>
</html>


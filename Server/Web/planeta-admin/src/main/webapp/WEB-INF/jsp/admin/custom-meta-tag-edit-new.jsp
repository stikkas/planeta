<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ include file="/WEB-INF/jsp/includes/generated/doctype-header.jsp" %>
<head>
    <%@ include file="head.jsp" %>
    <script type="text/javascript">
        $(document).ready(function () {
            //global variables
            var url = $('.js-url-input');
            var btn = $('.js-btn-save');
            var $title = $('.js-title-textarea');
            var $desc = $('.js-desc-textarea');
            var $ogTitle = $('.js-ogtitle-textarea');
            var $ogDesc = $('.js-ogdesc-textarea');
            var setTitleCount = function () {
                $('.js-title-count').text('Title: ' + $title.val().length);
            };
            var setDescCount = function () {
                $('.js-desc-count').text('Description: ' + $desc.val().length);
            };
            var setOgTitleCount = function () {
                $('.js-ogtitle-count').text('Title: ' + $ogTitle.val().length);
            };
            var setOgDescCount = function () {
                $('.js-ogdesc-count').text('Description: ' + $ogDesc.val().length);
            };

            if (url.val() == "") {
                btn.attr('disabled', true);
            }
            url.popover({'trigger': 'focus', html: true});
            setTitleCount();
            setDescCount();
            setOgTitleCount();
            setOgDescCount();
            //events binds
            url.bind('input', function () {
                if (url.val() == "") {
                    btn.attr('disabled', true);
                } else {
                    btn.attr('disabled', false);
                }
            });
            $title.bind('keyup', function (e) {
                setTitleCount();
            });
            $desc.bind('keyup', function (e) {
                setDescCount();
            });
            $ogTitle.bind('keyup', function (e) {
                setOgTitleCount();
            });
            $ogDesc.bind('keyup', function (e) {
                setOgDescCount();
            });
        });
    </script>

    <script type="text/javascript">
        $(document).ready(function () {
            $('.js-deleteByProfileId-href').click(function (e) {
                e.preventDefault();
                var self = this;
                console.log($(self).parent().find('.js-deleteByProfileId-input').val());
                Modal.showConfirm("Уверены что хотите удалить данный тег?", 'Подтверждение действия', {
                    success: function () {
                        $.ajax({
                            url: $(self).parent().find('.js-deleteByProfileId-input').val(),
                            success: function (response) {
                                var type = '${customMetaTagTypeId}';
                                //isn't beautiful but easy
                                document.location.href = "/admin/custom-meta-tag-list.html?customMetaTagType=" + type;
                            }
                        });
                    }
                });
            });
        });
    </script>
</head>
<body>
<%@ include file="/WEB-INF/jsp/admin/navbar.jsp" %>
<div id="page-wrapper">

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Редактирование мета тегов для ${customMetaTagTypeRus} </h1>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <form method="POST" class="form-horizontal" action="/admin/custom-meta-tag-save.html">
                <input name="customMetaTagType" type="hidden" value="${customMetaTagTypeId}">

                <div class="row ma-b-20">
                    <div class="col-lg-6">
                        <label>Ссылка</label>
                        
                        <c:if test="${not empty tagPath}">
                            <input class="form-control js-url-input" type="text" value="${tagPath}" disabled>
                            <input value="${tagPath}" name="tagPath" class="hidden">
                        </c:if>
                        
                        <c:if test="${empty tagPath}">
                            <input name="tagPath" placeholder="Введите нужную ссылку" class="form-control js-url-input"
                                   type="text" value="${customMetaTag.tagPath}" data-toggle="popover" data-placement="right"
                                   data-content="Возможные варианты страниц:<br>
                            <c:choose>
                                <c:when test="${customMetaTagType == 'main'}">
                                     /{group}/about<br>
                                     /{group}/audio<br>
                                     /{group}/audio/12345<br>
                                     /{group}/video<br>
                                     /{group}/albums<br>
                                     /{group}/events<br>
                                </c:when>
                                <c:when test="${customMetaTagType == 'shop'}">
                                     /products/12345
                                </c:when>
                                <c:when test="${customMetaTagType == 'tv'}">
                                     /broadcast/12345<br>
                                     /video/12345
                                </c:when>
                                <c:when test="${customMetaTagType == 'mobile'}">
                                     /12345/profile.html<br>
                                     /campaigns/12345.html
                                </c:when>
                            </c:choose>">
                            <c:if test="${not empty errorCMT}"><span style="margin-left:8px;color:red;">КМТ уже существуют для такой ссылки</span></c:if>
                        </c:if>
                    </div>

                    <div class="col-lg-6">
                        <label>Keywords</label>
                        <input placeholder="Keywords" type="text" class="form-control" name="keywords" value="${customMetaTag.keywords}">
                    </div>
                </div>

                <div class="row ma-b-20">
                    <div class="col-lg-6">
                        <label>Title</label>
                        <textarea class="form-control js-title-textarea"
                                  name="title"
                                  rows="5"
                                  placeholder="Title">${customMetaTag.title}</textarea>

                        <div class="js-title-count">Title: 0</div>
                    </div>

                    <div class="col-lg-6">
                        <label>Description</label>
                        <textarea class="form-control js-desc-textarea"
                                  name="description"
                                  rows="5"
                                  placeholder="Description">${customMetaTag.description}</textarea>

                        <div class="js-desc-count">Description: 0</div>
                    </div>
                </div>

                <h3>Open Graph</h3>

                <div class="row ma-b-20">
                    <div class="col-lg-6">
                        <label>Title</label>
                        <textarea class="form-control js-ogtitle-textarea"
                                  name="ogTitle"
                                  rows="5"
                                  placeholder="Title">${customMetaTag.ogTitle}</textarea>

                        <div class="js-ogtitle-count">Title: 0</div>
                    </div>

                    <div class="col-lg-6">
                        <label>Description</label>
                        <textarea class="form-control js-ogdesc-textarea"
                                  name="ogDescription"
                                  rows="5"
                                  placeholder="Description">${customMetaTag.ogDescription}</textarea>

                        <div class="js-ogdesc-count">Description: 0</div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12">
                        <label>Image</label>
                        <input placeholder="Image"
                               type="text"
                               class="form-control js-image-input"
                               name="image"
                               value="${customMetaTag.image}">
                    </div>
                </div>

                <input type="text" class="hidden" name="customMetaTagTypeId" value="${customMetaTagTypeId}"><br>
                <input type="text" class="hidden" name="customMetaTagId" value="${customMetaTag.customMetaTagId}"><br>

                <input type="submit" formaction="/admin/custom-meta-tag-save-new.html" class="btn btn-primary js-btn-save"
                       value="Сохранить">
                <c:if test="${not empty customMetaTag.customMetaTagId}">
                    <a href="javascript:void(0)" class="js-delete-href btn btn-danger" data-tooltip="Удалить">
                        Удалить
                    </a>
                    <input class="js-delete-input"
                           type="hidden"
                           value="/admin/custom-meta-tag-delete-new.json?customMetaTagId=${customMetaTag.customMetaTagId}"/>
                </c:if>
            </form>
        </div>
    </div>
</div>

</body>
</html>

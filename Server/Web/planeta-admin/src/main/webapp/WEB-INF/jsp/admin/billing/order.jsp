<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ include file="/WEB-INF/jsp/includes/generated/doctype-header.jsp" %>
<head>
    <%@ include file="../head.jsp" %>
    <script type="text/javascript" src="/js/utils/json-prettify.js"></script>
    <link type="text/css" rel="stylesheet" href="/css/utils/json-prettify.css"/>
    <link type="text/css" rel="stylesheet" href="/css/utils/log-prettify.css"/>

    <c:set var="orderAsJson" value="${hf:toJson(order)}" />
    <c:set var="paymentAsJson" value="${hf:toJson(payment)}" />

    <script>
        var nls_ru = {
            datePattern: 'd.m.y H:i:s',
            image: 'изображение',
            link: 'ссылка',
            nullOrUndefined: 'пусто'
        };

        var orderNlsProperties_ru = {
            key: {
                orderId: '№ заказа',
                buyerName: 'Покупатель',
                buyerEmail: 'Email покупателя',
                orderType: 'Тип заказа',
                orderObjectsInfo: {
                    name: 'Объекты заказа',
                    key: {
                        objectId: '№ объекта',
                        objectName: 'Объект',
                        merchantName: 'Продавец',
                        ownerName: 'Владелец',
                        price: 'Цена',
                        comment: 'Комментарий'
                    },
                    value: {}
                },
                totalPrice: 'Общая стоимость',
                paymentType: 'Тип оплаты',
                paymentStatus: 'Статус оплаты',
                payment: {
                    name: 'Связанный платеж',
                    key: {
                        transactionId: '№ платежа',
                        amountNet: 'Сумма',
                        status: 'Статус',
                        projectType: 'Проект-источник платежа',
                        param1: 'Экстра-параметр 1',
                        param2: 'Экстра-параметр 2',
                        timeAdded: 'Время создания',
                        timeUpdated: 'Время обновления'
                    },
                    value: {
/*
                        paymentSystemType: {
                            ROBOKASSA: 'ROBOKASSA',
                            DENGIONLINE: 'Деньги Online',
                            ALFABANK: 'Альфа-Банк',
                            QIWI: 'Qiwi',
                            W1: 'W1',
                            YANDEX_MONEY: 'Яндекс.Деньги',
                            CHRONOPAY: 'ChronoPay'
                        },
*/
                        projectType: {
                            MAIN: 'planeta.ru',
                            CAMPAIGN: 'start.planeta.ru',
                            CONCERT: 'concert.planeta.ru',
                            SHOP: 'shop.planeta.ru',
                            MOBILE: 'm.planeta.ru'
                        },
                        status: {
                            NEW: 'НОВЫЙ',
                            DONE: 'ЗАВЕРШЁН',
                            DECLINE: 'ОТКЛОНЁН',
                            ERROR: 'ОШИБКА'
                        }
                    }
                },
                deliveryType: 'Тип доставки',
                deliveryStatus: 'Статус доставки',
                addressee: {
                    name: 'Адресат',
                    key: {
                        name: 'Имя',
                        surname: 'Фамилия',
                        patronymic: 'Отчество',
                        phone: 'Телефон',
                        comment: 'Комментарий',
                        address: {
                            name: 'Адрес',
                            key: {
                                city: 'Город',
                                street: 'Улица',
                                zipCode: 'Индекс',
                                phone: 'Телефон'
                            },
                            value: {}
                        }
                    },
                    value: {}
                },
                timeAdded: 'Время создания',
                timeUpdated: 'Время обновления'
            },
            value: {
                orderType: {
                    SHARE: 'ВОЗНАГРАЖДЕНИЕ',
                    PRODUCT: 'ТОВАР',
                    BONUS: 'БОНУС',
                    DELIVERY: 'ДОСТАВКА'
                },
                paymentStatus: {
                    PENDING: 'ОЖИДАЕТСЯ',
                    COMPLETED: 'ЗАВЕРШЕНА',
                    CANCELLED: 'АННУЛИРОВАНА'
                },
                paymentType: {
                    NOT_SET: 'НЕ ОПРЕДЕЛЁН',
                    CASH: 'НАЛИЧНЫЙ РАСЧЕТ',
                    PAYMENT_SYSTEM: 'ПЛАТЁЖНАЯ СИСТЕМА'
                },
                deliveryStatus: {
                    PENDING: 'ОЖИДАЕТСЯ',
                    IN_TRANSIT: 'В ПУТИ',
                    DELIVERED: 'ДОСТАВЛЕН',
                    REJECTED: 'ВОЗВРАТ',
                    CANCELLED: 'АННУЛИРОВАН'
                },
                deliveryType: {
                    NOT_SET: 'НЕ ОПРЕДЕЛЕН',
                    EXPRESS: 'ЭКСПРЕСС',
                    ELECTRONIC: '"ЭЛЕКТРОННАЯ',
                    POST: 'ПОЧТА',
                    COURIER: 'КУРЬЕР',
                    CUSTOMER_PICKUP: 'САМОВЫВОЗ',
                    PICK_POINT: 'PICK_POINT'
                }
            }
        };

        $(document).ready(function() {
            var formatter = new JsonFormatter();

            var order = ${orderAsJson};
            new NlsJsonFormatter(nls_ru).format('.order-detail-info', formatter, order);

            order.payment = ${paymentAsJson};
            new NlsJsonFormatter(_.extend(nls_ru, orderNlsProperties_ru)).format('.order-common-info', formatter, order);

            formatter.format('.json-data');
            formatter.collapseAll('.json-data');

            $('.collapseAll').click(function(e) {
                e.preventDefault();
                var element = $(e.currentTarget).parent().next();
                formatter.collapseAll(element);
            });
            $('.expandAll').click(function(e) {
                e.preventDefault();
                var element = $(e.currentTarget).parent().next();
                formatter.expandAll(element);
            });

            $('.${initialTab}').addClass('active');
        });

        function cancelOrder(orderid) {
            $.post("/admin/billing/cancel-order.json",{orderId:orderid}, function (response) {
                if (response.success) {
                    alert("Заказ аннулирован");
                } else {
                    alert(response.errorMessage);
                }
            })
        }
    </script>

</head>
<body>

<%@ include file="/WEB-INF/jsp/admin/navbar.jsp" %>

<div id="page-wrapper">

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Заказ № ${order.orderId}</h1>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <ul class="nav nav-tabs">
                <li class="common">
                    <a href="#tab-order-common-info" data-toggle="tab">Кратко</a>
                </li>
                <li class="details">
                    <a href="#tab-order-detail-info" data-toggle="tab">Детально</a>
                </li>
                <li class="log">
                    <a href="/admin/billing/log-records.html?orderId=${order.orderId}">Журнал</a>
                </li>
            </ul>
        </div>


        <div class="col-lg-12 tab-content">

            <div class="tab-pane common" id="tab-order-common-info">
                <h3>Общая информация о заказе</h3>

                <div class="resizer">
                    <a href="javascript:void(0);" class="collapseAll" data-toggle="tooltip" title="Свернуть всё">
                        <i class="fa fa-minus"></i>
                    </a>
                    <a href="javascript:void(0);" class="expandAll" data-toggle="tooltip" title="Развернуть всё">
                        <i class="fa fa-plus"></i>
                    </a>
                </div>
                <pre class="order-common-info"></pre>
            </div>

            <div class="tab-pane details" id="tab-order-detail-info">
                <h3>Детальная информация о заказе</h3>

                <div class="resizer">
                    <a href="javascript:void(0);" class="collapseAll" data-toggle="tooltip" title="Свернуть всё">
                        <i class="fa fa-minus"></i>
                    </a>
                    <a href="javascript:void(0);" class="expandAll" data-toggle="tooltip" title="Развернуть всё">
                        <i class="fa fa-plus"></i>
                    </a>
                </div>
                <pre class="order-detail-info"></pre>
            </div>

            <div class="tab-pane log" id="tab-log-info">
                <h3>
                    <c:choose>
                        <c:when test="${empty logRecords}">
                            Для данного заказа нет записей журнала обработки ...
                        </c:when>
                        <c:otherwise>
                            Список действий по обработке заказа и связанных с ним платежей
                        </c:otherwise>
                    </c:choose>
                </h3>

                <c:if test="${logEnabled}">
                    <%@ include file="/WEB-INF/jsp/includes/log-records.jsp" %>
                </c:if>
            </div>

            <c:if test="${order.paymentStatus != 'CANCELLED'}">
                <div class="row">
                    <div class="col-lg-12">
                            <h3>Действия</h3>
                            <button class="btn btn-warning js-cancel-order" onclick="cancelOrder(${order.orderId})">Аннулировать заказ</button>
                        </div>
                    </div>
                </div>
            </c:if>
        </div>
    </div>
</div>

</body>
</html>

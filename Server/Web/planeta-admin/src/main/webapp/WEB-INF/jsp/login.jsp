<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ include file="/WEB-INF/jsp/includes/generated/doctype-header.jsp" %>
<head>
    <%@include file="admin/head.jsp" %>
</head>
<body>


<div class="login-container">
    <div class="login-cover"></div>

    <div class="login">
        <div class="login-header">
            <div class="brand">
                <span class="logo">
                    <i class="fa fa-globe"></i>
                </span> Planeta.ru
                <small>Административная панель Planeta.ru</small>
            </div>
            <div class="icon">
                <i class="fa fa-lock"></i>
            </div>
        </div>

        <div class="login-content">
            <form id="request" onsubmit="login_me_please(); return false">
                <div class="form-group ma-b-20">
                    <input class="form-control login-input" placeholder="E-mail" name="email" value="" type="text" autofocus="autofocus">
                </div>

                <div class="form-group">
                    <input type="password" class="form-control login-input" placeholder="Пароль" name="password">
                </div>

                <div class="row ma-b-30">
                    <div class="col-lg-12 text-danger js-login-error"></div>
                </div>

                <div class="login-buttons">
                    <button type="submit" class="btn btn-primary btn-block btn-lg">Войти</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script>
    function login_me_please() {
        $('.btn-primary').attr('disabled', true);

        var data= {
            username: $('#request [name=email]').val(),
            password: $('#request [name=password]').val()
        };

        $.post('/api/public/login.json', data, function(response) {
            if (response && response.success === false) {
                $('.js-login-error').text('Неправильный логин или пароль. Повторите попытку.');
            } else {
                location.href = 'dashboard.html'
            }
        }).fail(function() {
            $('.js-login-error').text('Нет ответа от сервера. Повторите попытку.');
        }).always(function() {
            $('.btn-primary').attr('disabled', false);
        });
    }
</script>

</body>
</html>



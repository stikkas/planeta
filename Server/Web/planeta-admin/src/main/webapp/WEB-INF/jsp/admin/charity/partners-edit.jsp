<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags/form" %>
<%@ include file="/WEB-INF/jsp/includes/generated/doctype-header.jsp" %>
<head>
    <%@include file="../head.jsp" %>
    <script>
        $(document).ready(function() {
            $("#imageUploader .upload-image").click(function() {
                UploadController.showUploadWelcomePromoImage(workspace.appModel.get('myProfile').get('profileId'), function(filesUploaded) {
                    if (!filesUploaded || !filesUploaded.length) { return; }
                    var uploadResult = filesUploaded.at(0).get('uploadResult');
                    if (!uploadResult) { return; }
                    var src = uploadResult.imageUrl;
                    var image = new Image();
                    image.src = src;
                    $(image).load(function () {
                        if(image.width >= 5 && image.height >= 5) {
                            $("#imageUrl").val(src);
                            $("#imageUploader").find(".error-message").addClass("hide");
                        } else {
                            $("#imageUrl").val("");
                            $("#imageUploader").find(".error-message").removeClass("hide");
                        }
                    });
                }, "Размер не менее 5x5.", "Загрузка картинки партнера");
            });
        });

    </script>
</head>
<body>
<%@ include file="/WEB-INF/jsp/admin/navbar.jsp" %>
<div id="container" class="container">
    <div class="row">
        <div class="span12">
            <ul class="breadcrumb">
                <li><a href="/">Главная</a> <span class="divider">/</span></li>
                <li class="active">Библиородина<span class="divider">/</span></li>
                <li><a href="/admin/charity/partners-charity-list.html">Партнеры</a> <span class="divider">/</span></li>
                <li class="active">Редактировать партнера</li>
            </ul>
        </div>
    </div>

    <div class="row">
        <div class="span12">
            <h2>Партнеры</h2>
            <p></p>
        </div>
    </div>
    <div class="row">
        <div class="span12">
            <s:form class="form-horizontal" commandName="configuration" method="post">
                <fieldset>

                    <!-- File Button -->
                    <div class="control-group">
                        <label class="control-label">Изображение</label>
                        <div id="imageUploader" class="controls">
                            <s:input path="imageUrl" type="text" placeholder="Нажмите &quot;Загрузить картинку&quot;" class="input-xxlarge" required=""/>
                            <span class="clearfix">Загрузите картинку или укажите любой адрес.</span>
                            <span class="error-message hide">Неподходящая картинка.</span>
                            <s:errors path="imageUrl" cssClass="error-message" element="span" />
                            <a href="javascript:void(0)" class="upload-image clearfix">Загрузить картинку</a>
                        </div>
                    </div>

                    <!-- Text input-->
                    <div class="control-group">
                        <label class="control-label">Партнер</label>
                        <div class="controls">
                            <s:input path="name" type="text" placeholder="" class="input-xxlarge" required=""/>
                            <p class="help-block">Название</p>
                            <s:errors path="name" cssClass="error-message" element="span" />
                        </div>
                    </div>

                    <!-- Text input-->
                    <div class="control-group">
                        <label class="control-label">Ссылка</label>
                        <div class="controls">
                            <s:input path="originalUrl" type="text" placeholder="" class="input-xxlarge" required=""/>
                            <p class="help-block">Ссылка на партнера</p>
                            <s:errors path="originalUrl" cssClass="error-message" element="span" />
                        </div>
                    </div>

                    <s:input path="id" type="hidden" />

                    <!-- Button -->
                    <div class="form-actions">
                        <button formaction="/admin/charity/partners-charity-edit.html" class="btn btn-primary">Сохранить</button>
                        <a href="/admin/charity/partners-charity-list.html" class="btn">Отмена</a>
                    </div>

                </fieldset>
            </s:form>

        </div>

    </div>
</body>


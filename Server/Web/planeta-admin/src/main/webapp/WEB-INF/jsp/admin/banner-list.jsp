<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ include file="/WEB-INF/jsp/includes/generated/doctype-header.jsp" %>
<head>
    <%@include file="head.jsp" %>
</head>

<body>

<%@ include file="/WEB-INF/jsp/admin/navbar.jsp" %>

<div id="page-wrapper">

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Баннеры</h1>
        </div>
    </div>

    <div class="main-page-actions">
        <form method="get">
            <input type="hidden" name="bannerId" value="0"/>
            <button formaction="/admin/edit-banner.html" class="btn btn-success btn-circle btn-outline btn-lg" title="Добавить">
                <i class="fa fa-plus"></i>
            </button>

            <a class="btn btn-primary btn-circle btn-outline btn-lg" href="${mainAppUrl}/api/admin/banner-reload.html" title="Перечитать">
                <i class="fa fa-refresh"></i>
            </a>
        </form>
    </div>

    <div class="row ma-b-20">
        <div class="col-lg-12">
            <%--TODO новая админка: сделать красиво--%>
            <a href="/admin/banners-list.html">Активные баннеры</a> |
            <a href="/admin/all-banners-list.html">Все баннеры</a>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12 admin-table">
            <table class="table table-striped table-hover table-bordered admin-banner-image">
                <c:forEach var="banner" items="${bannerList}">
                    <tr>
                        <td>
                            <b>${banner.bannerId}</b>
                        </td>
                        <td>

                            <b>${banner.name}</b>
                        </td>
                        <td>

                            <b>${banner.type}</b>
                        </td>
                        <td>
                                ${banner.maskView}
                        </td>
                        <td>
                            <c:if test="${banner.status == 'ON'}"><b>Включен</b></c:if>
                            <c:if test="${banner.status == 'OFF'}">Выключен</c:if>
                        </td>
                        <td>
                            <form method="post" class="text-right">
                                <input type="hidden" name="bannerId" value="${banner.bannerId}"/>
                                <input type="hidden" name="status" value="${banner.status}"/>

                                <div class="asdmin-table-actions">
                                    <a href="/admin/edit-banner.html?bannerId=${banner.bannerId}" class="btn btn-primary btn-outline" title="Редактировать">
                                        <i class="fa fa-pencil"></i>
                                    </a>
                                    <a href="/admin/edit-banner-simple.html?bannerId=${banner.bannerId}" class="btn btn-success btn-outline" title="Упрощенное редактирование">
                                        <i class="fa fa-dashboard"></i>
                                    </a>


                                    <button type="submit" formaction="/admin/copy-banner.html" class="btn btn-outline btn-info" title="Скопировать">
                                        <i class="fa fa-files-o"></i>
                                    </button>

                                    <c:if test="${banner.status == 'OFF'}">
                                        <button type="submit" formaction="/admin/switch-on-off-banner.html" class="btn btn-success btn-outline" title="Включить">
                                            <i class="fa fa-play"></i>
                                        </button>
                                    </c:if>

                                    <c:if test="${banner.status == 'ON'}">
                                        <button type="submit" formaction="/admin/switch-on-off-banner.html" class="btn btn-warning btn-outline" title="Выключить">
                                            <i class="fa fa-pause"></i>
                                        </button>
                                    </c:if>

                                    <button type="submit" formaction="/admin/delete-banner.html" class="btn btn-danger btn-outline" title="Удалить">
                                        <i class="fa fa-trash"></i>
                                    </button>
                                </div>
                            </form>
                        </td>
                    </tr>
                </c:forEach>
            </table>
        </div>
    </div>
</div>




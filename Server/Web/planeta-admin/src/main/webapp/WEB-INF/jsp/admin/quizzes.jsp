<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ include file="/WEB-INF/jsp/includes/generated/doctype-header.jsp" %>
<head>
    <%@ include file="head.jsp" %>
    <script type="text/javascript">
        $(function () {
            $('[data-contr-id]').each(function () {
                $(this).bind('click', function () {
                    var href = '/ololo?contractorId=' + $(this).data().contrId + '&campaignId=' + campaignId;
                    Modal.showConfirm("Текущий контрагент проекта будет заменен", 'Подтверждение действия', {
                        success: function () {
                            document.location.href = href;
                        }
                    });
                })
            })
        });
    </script>
</head>
<body>

<%@ include file="/WEB-INF/jsp/admin/navbar.jsp" %>

<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Опросы</h1>
        </div>
    </div>

    <div class="main-page-actions">
        <a href="/admin/quiz-info.html?quizId=0" class="btn btn-success btn-circle btn-outline btn-lg" title="Добавить опрос">
            <i class="fa fa-plus"></i>
        </a>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <form method=get action="/admin/quizzes.html">
                <input type="hidden" name="offset" value="0"/>
                <input type="hidden" name="limit" value="10"/>

                <div class="form-group input-group">
                    <input class="form-control"
                           type="text"
                           placeholder="Введите текст из названия или описания опроса"
                           id="xlInput"
                           name="searchString"
                           value="${searchString}">

                    <span class="input-group-btn">
                        <button class="btn btn-primary" type="submit">
                            <i class="fa fa-search"></i>
                        </button>
                    </span>
                </div>
            </form>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12 admin-table">
            <table class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th>#</th>
                        <th width="150">alias</th>
                        <th width="250">Название опроса</th>
                        <th>Описание</th>
                        <th>Доступен</th>
                        <th>Добавлен</th>
                        <th>Действия</th>
                    </tr>
                </thead>

                <tbody>
                    <c:forEach var="quiz" items="${quizzes}">
                        <tr>
                            <td>${quiz.quizId}</td>
                            <td>
                                <c:if test="${quiz.alias != null}">
                                    <a href="//${appHost}/quiz/${quiz.alias}" target="_blank">${quiz.alias}</a>
                                </c:if>
                                <c:if test="${quiz.alias == null}">
                                    <a href="//${appHost}/quiz/${quiz.quizId}" target="_blank">${quiz.quizId}</a>
                                </c:if>
                            </td>
                            <td>${quiz.name}</td>
                            <td>${quiz.description}</td>
                            <td style="text-align: center;"><c:if test="${quiz.enabled}"><label class="label label-success">да</label></c:if><c:if
                                    test="${!quiz.enabled}"><label class="label label-danger">нет</label></c:if></td>
                            <td width="150"><fmt:formatDate value="${quiz.timeAdded}" pattern="dd.MM.yyyy HH:mm"/></td>
                            <td class="text-right">
                                <div class="btn-group">
                                    <a class="btn btn-primary btn-outline"
                                       href="/admin/quiz-info.html?quizId=${quiz.quizId}"
                                       title="Редактировать">
                                        <i class="fa fa-pencil"></i>
                                    </a>
                                </div>
                            </td>
                        </tr>
                    </c:forEach>
                </tbody>
            </table>
        </div>
    </div>

    <%@ include file="paginator.jsp" %>
</div>
</body>
</html>


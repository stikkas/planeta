<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ include file="/WEB-INF/jsp/includes/generated/doctype-header.jsp" %>
<head>
    <%@include file="../head.jsp" %>
</head>
<body>

<div class="container">
    <%@ include file="/WEB-INF/jsp/admin/navbar.jsp" %>
    <div class="row">

        <div class="span12">
            <h2>Проекты</h2>
        </div>
    </div>

    <div class="row ">
        <div class="span12">
            <ul class="breadcrumb">
                <li><a href="/">Главная</a> <span class="divider">/</span></li>
                <li><a href="/moderator/promo/projects.html">Проекты</a> <span class="divider">/</span></li>
                <li class="active">Редактирование проекта</li>
            </ul>
        </div>
    </div>

    <div class="row">
        <div class="span12">
            <form:form id="projectForm" commandName="project" class="form-horizontal" method="post">
                <form:input type="hidden" path="projectId" id="projectId" />

                <fieldset>
                    <label><strong>Название</strong></label>
                    <form:input type="text" id="title" path="title" cssClass="input-xxlarge"/>
                    <form:errors path="title"><span class="help-inline" style="color: red;"><form:errors path="title"/></span></form:errors>

                    <br />
                    <br />
                    <label><strong>Описание</strong></label>
                    <form:textarea id="description" path="description" cssClass="input-xxlarge" rows="20" cssStyle="width: 100%"/>
                    <form:errors path="description"><span class="help-inline" style="color: red;"><form:errors path="description"/></span></form:errors>

                    <br />
                    <br />
                    <label><strong>Картинка для витрины</strong></label>
                    <form:input type="text" id="smallImageUrl" path="smallImageUrl" cssClass="input-xxlarge"/>
                    <form:errors path="smallImageUrl"><span class="help-inline" style="color: red;"><form:errors path="smallImageUrl"/></span></form:errors>

                    <br />
                    <br />
                    <label><strong>Картинка для страницы проекта</strong></label>
                    <form:input type="text" id="imageUrl" path="imageUrl" cssClass="input-xxlarge"/>
                    <form:errors path="imageUrl"><span class="help-inline" style="color: red;"><form:errors path="imageUrl"/></span></form:errors>

                    <br />
                    <br />
                    <label><strong>Видео для страницы проекта</strong></label>
                    <form:input type="text" id="videoSrc" path="videoSrc" cssClass="input-xxlarge"/>
                    <form:errors path="videoSrc"><span class="help-inline" style="color: red;"><form:errors path="videoSrc"/></span></form:errors>

                    <br />
                    <br />
                    <label><strong>Целевая аудитория</strong></label>
                    <form:textarea id="consumers" path="consumers" cssClass="input-xxlarge" rows="5" />
                    <form:errors path="consumers"><span class="help-inline" style="color: red;"><form:errors path="consumers"/></span></form:errors>

                    <br />
                    <br />
                    <label><strong>Команда</strong></label>
                    <form:textarea id="team" path="team" cssClass="input-xxlarge" rows="5" />
                    <form:errors path="team"><span class="help-inline" style="color: red;"><form:errors path="team"/></span></form:errors>

                    <br />
                    <br />
                    <label><strong>Город</strong></label>
                    <form:input type="text" id="city" path="city" cssClass="input-xxlarge"/>
                    <form:errors path="city"><span class="help-inline" style="color: red;"><form:errors path="city"/></span></form:errors>

                    <br />
                    <br />
                    <label><strong>Профиль</strong></label>
                    <form:input type="text" id="profileId" path="profileId" cssClass="input-xxlarge"/>
                    <form:errors path="profileId"><span class="help-inline" style="color: red;"><form:errors path="profileId"/></span></form:errors>

                    <br />
                    <br />
                    <label><strong>ID проекта</strong></label>
                    <form:input type="text" id="campaignId" path="campaignId" cssClass="input-xxlarge"/>
                    <form:errors path="campaignId"><span class="help-inline" style="color: red;"><form:errors path="campaignId"/></span></form:errors>

                    <br />
                    <br />
                    <label><strong>Alias проекта</strong></label>
                    <form:input type="text" id="campaignAlias" path="campaignAlias" cssClass="input-xxlarge"/>
                    <form:errors path="campaignAlias"><span class="help-inline" style="color: red;"><form:errors path="campaignAlias"/></span></form:errors>

                    <br />
                    <br />
                    <label><strong>Заголовок для шаринга</strong></label>
                    <form:input type="text" id="sharingTitle" path="sharingTitle" cssClass="input-xxlarge"/>
                    <form:errors path="sharingTitle"><span class="help-inline" style="color: red;"><form:errors path="sharingTitle"/></span></form:errors>


                    <br />
                    <br />
                    <label><strong>Картинка для шаринга</strong></label>
                    <form:input type="text" id="sharingImageUrl" path="sharingImageUrl" cssClass="input-xxlarge"/>
                    <form:errors path="sharingImageUrl"><span class="help-inline" style="color: red;"><form:errors path="sharingImageUrl"/></span></form:errors>

                </fieldset>

                <br />
                <button type="submit" class="btn btn-primary">
                    Сохранить
                </button>
                <br />
                <br />
                <br />
            </form:form>
        </div>
    </div>
</body>
</html>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ include file="/WEB-INF/jsp/includes/generated/doctype-header.jsp" %>
<head>
    <%@include file="head.jsp" %>
    <link type="text/css" rel="stylesheet"
          href="//${hf:getStaticBaseUrl("")}/admin-new/bootstrap/css/jquery-ui-1.9.2.custom.css"/>
</head>

<body>

<%@ include file="/WEB-INF/jsp/admin/navbar.jsp" %>

<div id="page-wrapper">

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Редактирование баннера</h1>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <form:form commandName="banner" class="form-horizontal">
                <form:hidden id="bannerId" path="bannerId"/>
                <form:hidden id="bannerOff" path="status"/>

                <div class="row ma-b-20">
                    <div class="col-lg-3">
                        <label for="edit_title">Название</label>
                        <form:input type="text" path="name" id="edit_title" class="form-control"/>
                        <form:errors path="name" cssClass="error"/>
                        <p class="help-block">Название используется для альта в картиках(будет прочитано поисковиками).</p>
                    </div>

                    <div class="col-lg-3">
                        <label for="refererUrl">Адрес перехода</label>
                        <form:input path="refererUrl" id="refererUrl" class="form-control"/>
                        <form:errors path="refererUrl" cssClass="error"/>
                    </div>

                    <div class="col-lg-3">
                        <label for="type">Тип Баннера</label>
                        <form:select path="type" id="type" items="${bannerTypes}" cssClass="form-control"/>
                        <form:errors path="type" cssClass="error"/>
                    </div>

                    <div class="col-lg-3">
                        <label for="maskView">Путь показа</label>
                        <form:input path="maskView" id="maskView" class="form-control"/>
                        <form:errors path="maskView" cssClass="error"/>
                        <p class="help-block">Изменяющуюся часть пути обозначьте символом * (звездочка)</p>
                    </div>
                </div>

                <div class="row ma-b-20">
                    <div class="col-lg-12">
                        <ct:file url="${banner.imageUrl}" path="imageUrl" type="IMAGE"/>
                    </div>
                </div>

                <div class="row ma-b-20">
                    <div class="col-lg-12">
                        <ct:file url="${banner.mobileImageUrl}" path="mobileImageUrl" label="Изображение для мобилы" type="IMAGE"/>
                    </div>
                </div>

                <div class="row ma-b-20">
                    <div class="col-lg-12">
                        <label for="targetingType">Тип Таргетинга</label>
                        <form:select path="targetingType" id="targetingType" cssClass="form-control"
                                     items="${targetingTypes}"/>
                        <form:errors path="targetingType" cssClass="error"/>
                    </div>
                </div>

                <div class="row ma-b-20">
                    <div class="col-lg-12">
                        <form:checkbox path="target" id="target"/>
                        <label for="target">Открывать в новом окне</label>
                        <form:errors path="target" cssClass="error"/>
                    </div>
                </div>

                <div class="row ma-b-20">
                    <div class="col-lg-4">
                        <label for="timeStart">Начало действия</label>
                        <input id="timeStart" name="timeStart" value="${banner.timeStart.time}" type="hidden"/>
                        <input id="date-box-start"
                               value="<fmt:formatDate value="${banner.timeStart}" pattern="dd.MM.yyyy"/>"
                               type="text"
                               class="form-control"
                               placeholder="Начало действия"/>
                        <form:errors path="timeStart" cssClass="error"/>
                    </div>

                    <div class="col-lg-4">
                        <label for="timeFinish">Конец действия</label>
                        <input id="timeFinish" name="timeFinish" value="${banner.timeFinish.time}" type="hidden"/>
                        <input id="date-box-finish"
                               value="<fmt:formatDate value="${banner.timeFinish}" pattern="dd.MM.yyyy"/>"
                               type="text"
                               class="form-control"
                               placeholder="Конец действия"/>
                        <form:errors path="timeFinish" cssClass="error"/>
                    </div>

                    <div class="col-lg-4">
                        <label for="showCounter">Количество показов</label>
                        <form:input path="showCounter" id="showCounter" cssClass="form-control"/>
                        <form:errors path="showCounter" cssClass="error"/>
                    </div>
                </div>

                <div class="show-hidden-button btn-link ma-b-20">Поля для программистов. Не нужно заполнять</div>

                <div class="row ma-b-20 js-external-fields hidden">
                    <div class="col-lg-6">
                        <label for="html">Html</label>
                        <form:textarea path="html" id="html" rows="3" cssClass="form-control"/>
                        <form:errors path="html" cssClass="error"/>
                    </div>

                    <div class="col-lg-6">
                        <label for="altText">Текст при наведении</label>
                        <form:textarea path="altText" id="altText" rows="3" cssClass="form-control"/>
                        <form:errors path="altText" cssClass="error"/>
                    </div>
                </div>

                <div class="row ma-b-20 js-external-fields hidden">
                    <div class="col-lg-6">
                        <label for="weight">Вес</label>
                        <form:input path="weight" id="weight" cssClass="form-control"/>
                        <form:errors path="weight" cssClass="error"/>
                    </div>

                    <div class="col-lg-6">
                        <label for="utmMarks">Метки</label>
                        <form:input path="utmMarks" id="utmMarks" cssClass="form-control"/>
                        <form:errors path="utmMarks" cssClass="error"/>
                    </div>
                </div>

                <div class="ma-b-20">
                    <div class="btn-group">
                        <button type="submit" formaction="/admin/save-banner.html" class="btn btn-primary">
                            <c:if test="${banner.bannerId == 0}">Добавить</c:if>
                            <c:if test="${banner.bannerId != 0}">Cохранить</c:if>
                        </button>
                        <a href="/admin/banners-list.html" class="btn btn-default">Отмена</a>
                    </div>

                    <div class="pull-right btn-group">
                        <c:if test="${banner.status == 'OFF'}">
                            <button class="btn btn-success btn-outline on-off" title="Включить">
                                <i class="fa fa-play"></i>
                            </button>
                        </c:if>
                        <c:if test="${banner.status == 'ON'}">
                            <button class="btn btn-warning btn-outline on-off" title="Выключить">
                                <i class="fa fa-stop"></i>
                            </button>
                        </c:if>

                        <c:if test="${banner.bannerId != 0}">
                            <button class="btn btn-info btn-outline copy-banner" title="Скопировать">
                                <i class="fa fa-files-o"></i>
                            </button>
                        </c:if>
                        <button formaction="javascript:void(0);" onclick="preview()" class="btn btn-default btn-outline"
                                title="Превью">
                            <i class="fa fa-eye"></i>
                        </button>
                    </div>
                </div>
            </div>
        </form:form>
    </div>
</div>

<script type="text/javascript">
    //$(document).ready(preview);
    (function () {

        var uploadImage = function (profileId, urlSelector, buttonSelector) {
            buttonSelector.click(function (e) {
                if (e) {
                    e.preventDefault();
                }
                UploadController.showUploadImages(profileId, function (filesUploaded) {
                    if (_.isEmpty(filesUploaded)) return;

                    var images = _.map(filesUploaded.models, function (fileUploaded) {
                        return fileUploaded.get('uploadResult');
                    });
                    var imageUrl = ImageUtils.getThumbnailUrl(images[0].imageUrl, ImageUtils.ORIGINAL, ImageType.PHOTO);
                    console.log(imageUrl);
                    urlSelector.val(imageUrl);
                });
            });
        };

        $('.on-off').click(function (e) {
            e.preventDefault();
            var value = ($('#bannerOff').val() == 'ON');
            $('#bannerOff').val(value ? 'OFF' : 'ON');
            var name = !value ? "Включить" : "Выключить";
            $(e.target).html(name);
        });

        $('.copy-banner').click(function (e) {
            e.preventDefault();
            $('#bannerId').val(0);
            var text = $('#edit_title').val();
            text += "[копия]";
            $('#edit_title').val(text);
            $(e.target).hide();
        });

        //
        var options = {
            locale: "ru",
            dateFormat: 'dd.MM.yyyy',
            dateFormatMonth: 'MM.yyyy',
            date: new Date()
        };
        $("#date-box-start").datepicker();
        $("#date-box-start").bind('change', function () {
            var date = $(this).datepicker('getDate');
            $("#timeStart").val(date.getTime());
        });
        $("#date-box-finish").datepicker();
        $("#date-box-finish").bind('change', function () {
            var date = $(this).datepicker('getDate');
            $("#timeFinish").val(date.getTime());
        });
        if ($("#timeStart").val() == null) {
            $("#timeStart").val(new Date().getTime());
        }


        $(document).ready(function () {
            var profileId = workspace.appModel.get('myProfile').get('profileId');
            uploadImage(profileId, $('#imageUrl'), $('.upload-file'));
            uploadImage(profileId, $('#mobileImageUrl'), $('.upload-file-mobile'));

            $('.show-hidden-button').click(function (e) {
                $('.js-external-fields').removeClass('hidden');
            })
        });


    })();

    function preview() {
        $("div#banner-example").removeClass("hidden");
        var html = $("#html").val();
        if (html) {
            $("#banner_text").html(html);
        } else {
            $("#banner_text").load("${mainAppUrl}/api/util/preview-banner.html", $('form').serializeObject());
        }
    }
</script>

<div class="hidden" id="banner-example">
    <div id="banner_text"></div>
</div>

</body>
</html>


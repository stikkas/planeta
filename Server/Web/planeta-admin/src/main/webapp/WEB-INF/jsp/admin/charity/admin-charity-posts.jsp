<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ include file="/WEB-INF/jsp/includes/generated/doctype-header.jsp" %>

<head>
    <%@ include file="/WEB-INF/jsp/admin/head.jsp" %>
    <title>${pageTitle}</title>
    <style type="text/css">
        .row {
            width: 100%;
            margin: 0 !important;
        }

        .span12 {
            width: 100%;
            margin: 0 !important;
        }
    </style>
</head>

<body>

<%@ include file="/WEB-INF/jsp/admin/navbar.jsp" %>

<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Новости благотворительности</h1>
        </div>
    </div>


    <div class="main-page-actions">
        <form method="get">
            <a class="btn btn-success btn-circle btn-outline btn-lg" href="/admin/post.html">
                <i class="fa fa-plus"></i>
            </a>
        </form>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <form id="search-frm" method="GET" action="/admin/charity/posts.html" class="clearfix">
                <input type="hidden" name="offset" value="0"/>
                <input type="hidden" name="limit" value="10"/>

                <div class="form-group input-group">
                    <input type="text" class="form-control" placeholder="Введите id поста" id="postId" name="postId" value="${param.postId}">
                    <span class="input-group-btn">
                        <button class="btn btn-primary" id="search">
                            <i class="fa fa-search"></i>
                        </button>
                    </span>
                </div>
            </form>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12 admin-table">
            <table class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>Ид поста</th>
                    <th>Вес</th>
                    <th>Заголовок</th>
                    <th>Краткий текст</th>
                    <th>Раздел</th>
                    <th>Дата события</th>
                    <th>Место события</th>
                    <th>Изображение</th>
                    <th>Категория</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                <c:forEach var="post" items="${posts}">
                    <tr>
                        <td>${post.id}</td>
                        <td>${post.weight}</td>
                        <td>${post.title}</td>
                        <td>${post.headingText}</td>
                        <td>${post.eventType}</td>
                        <td><fmt:formatDate value="${post.eventDate}" pattern="dd MMMM yyyy в hh:mm"/></td>
                        <td>${post.eventLocation}</td>
                        <td><img src="${post.imageUrl}" style="width: 200px;"></td>
                        <td>
                            <c:choose>
                                <c:when test="${tagNameResolver[post.tagId] != null}">
                                    ${tagNameResolver[post.tagId]}
                                </c:when>
                                <c:otherwise>
                                    ${post.tagId}
                                </c:otherwise>
                            </c:choose>
                        </td>
                        <td class="col-lg-1">
                            <div class="btn-group">
                                <a href="/admin/post.html?postId=${post.id}" class="btn btn-outline btn-primary" title="Редактировать">
                                    <i class="fa fa-pencil"></i>
                                </a>
                                <a href="/admin/charity/delete-post.html?postId=${post.id}" class="btn btn-danger btn-outline" title="Удалить">
                                    <i class="fa fa-trash"></i>
                                </a>
                            </div>
                        </td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>
    </div>

    <%@ include file="/WEB-INF/jsp/admin/paginator.jsp" %>
</div>
</body>
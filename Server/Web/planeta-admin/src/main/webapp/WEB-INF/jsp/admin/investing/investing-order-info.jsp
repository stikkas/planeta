<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="x" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags/form" %>
<%@ include file="/WEB-INF/jsp/includes/generated/doctype-header.jsp" %>
<head>
    <%@ include file="../head.jsp" %>
    <link type="text/css" rel="stylesheet" href="/css/ui-lightness/jquery-ui-1.10.2.custom.min.css"/>
    <link type="text/css" rel="stylesheet" href="/css/jquery.timepicker.css"/>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#birthDateBox").datetimepicker();
            <c:if test="${not empty investingOrderInfo.birthDate}">
            $("#birthDateBox").data("DateTimePicker").date(new Date(${investingOrderInfo.birthDate.time}));
            </c:if>

            $("#passportIssueDateBox").datetimepicker();
            <c:if test="${not empty investingOrderInfo.passportIssueDate}">
            $("#passportIssueDateBox").data("DateTimePicker").date(new Date(${investingOrderInfo.passportIssueDate.time}));
            </c:if>

            $("#investing-order-info-form").submit(function () {
                var birthDate = $("#birthDateBox").data("DateTimePicker").date().format('x');
                if (birthDate) {
                    $("[name=birthDate]").val(birthDate);
                }

                var passportIssueDate = $("#passportIssueDateBox").data("DateTimePicker").date().format('x');
                if (passportIssueDate) {
                    $("[name=passportIssueDate]").val(passportIssueDate);
                }

                return true;
            });
        });
    </script>
</head>
<body>
<%@ include file="/WEB-INF/jsp/admin/navbar.jsp" %>
<div id="page-wrapper">

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Заказ № ${investingOrderInfo.investingOrderInfoId}</h1>
        </div>
    </div>

    <div class="main-page-actions">
        <a class="btn btn-primary btn-circle btn-outline btn-lg" href="${mainAppUrl}/welcome/invoice/pdf.html?transactionId=${transactionId}" title="Скачать счёт">
            <i class="fa fa-download"></i>
        </a>
    </div>

    <div class="row">
    <form:form
            id="investing-order-info-form"
            commandName="investingOrderInfo"
            class="form-horizontal"
            method="post"
            action="/admin/investing-order-info.html">

        <div class="col-lg-12">
            <div>
                <b>Пользователь:</b>
                <a href="${mainAppUrl}/${myProfile.profile.profileId}">${myProfile.profile.profileId}</a>
            </div>

            <div>
                <b>Проект:</b> <a href="${mainAppUrl}/campaigns/${campaignId}">${campaignName}</a>
            </div>

            <div>
                <b>Дата создания заказа:</b> <fmt:formatDate pattern="dd.MM.yyy HH:mm" value="${investingOrderInfo.timeAdded}"/>
            </div>

            <c:if test="${not empty timePayment}">
                <div>
                    <b>Дата оплаты заказа:</b> <fmt:formatDate pattern="dd.MM.yyy HH:mm" value="${timePayment}"/>
                </div>
            </c:if>

            <div>
                <b>Статус счёта:</b>
                <c:choose>
                    <c:when test="${investingOrderInfo.moderateStatus eq 'NEW'}">
                        <span class="label label-info">Новый</span>
                        <a class="btn btn-success"
                           href="/admin/investing-order-info-moderate.html?orderId=${investingOrderInfo.investingOrderInfoId}&status=APPROVED">
                            Одобрить счёт
                        </a>
                        <a class="btn btn-warning"
                           href="/admin/investing-order-info-moderate.html?orderId=${investingOrderInfo.investingOrderInfoId}&status=FAIL">Отклонить счёт
                        </a>
                    </c:when>
                    <c:when test="${investingOrderInfo.moderateStatus eq 'APPROVED'}">
                        <span class="label label-success">Одобрен</span>
                    </c:when>
                    <c:when test="${investingOrderInfo.moderateStatus eq 'FAIL'}">
                        <span class="label label-important">Отклонён</span>
                    </c:when>
                </c:choose>
            </div>
        </div>

        <%--//TODO новая админка: делать красиво--%>
        <div class="col-lg-12">
            <fieldset>
                <legend>Счёт</legend>
                <input type="hidden" name="investingOrderInfoId" value="${investingOrderInfo.investingOrderInfoId}"/>
                <input type="hidden" name="timeAdded" value="${investingOrderInfo.timeAdded.time}"/>
                <input type="hidden" name="moderateStatus" value="${investingOrderInfo.moderateStatus}"/>

                <div class="control-group">
                    <label class="control-label">Тип покупателя</label>

                    <div class="controls">
                        <form:select path="userType" id="userType" items="${userTypes}"/>
                    </div>
                    <label class="control-label">Телефон</label>

                    <div class="controls">
                        <form:input type="text" path="phoneNumber"/>
                    </div>
                    <label class="control-label">Фамилия</label>

                    <div class="controls">
                        <form:input type="text" path="lastName"/>
                    </div>
                    <label class="control-label">Имя</label>

                    <div class="controls">
                        <form:input type="text" path="firstName"/>
                    </div>
                    <label class="control-label">Отчество</label>

                    <div class="controls">
                        <form:input type="text" path="middleName"/>
                    </div>
                    <label class="control-label">Должность</label>

                    <div class="controls">
                        <form:input type="text" path="bossPosition"/>
                    </div>
                    <label class="control-label">Пол</label>

                    <div class="controls">
                        <form:select path="gender" id="gender" items="${genders}"/>
                    </div>
                    <label class="control-label">Название</label>

                    <div class="controls">
                        <form:input type="text" path="orgName"/>
                    </div>
                    <label class="control-label">Краткое название</label>

                    <div class="controls">
                        <form:input type="text" path="orgBrand"/>
                    </div>
                    <label class="control-label">Дата рождения</label>

                    <div class="controls">
                        <input type="hidden" name="birthDate"/>
                        <input type="text" id="birthDateBox"/>
                    </div>
                    <label class="control-label">Место рождения</label>

                    <div class="controls">
                        <form:input type="text" path="birthPlace"/>
                    </div>
                    <label class="control-label">Индекс регистрации</label>

                    <div class="controls">
                        <form:input type="text" path="regIndex"/>
                    </div>
                    <label class="control-label">Страна регистрации</label>

                    <div class="controls">
                        <form:input type="text" path="regCountry"/>
                    </div>
                    <label class="control-label hidden">regCountryId</label>

                    <div class="controls hidden">
                        <form:input type="text" path="regCountryId"/>
                    </div>
                    <label class="control-label">Регион регистрации</label>

                    <div class="controls">
                        <form:input type="text" path="regRegion"/>
                    </div>
                    <label class="control-label hidden">regRegionId</label>

                    <div class="controls hidden">
                        <form:input type="text" path="regRegionId"/>
                    </div>
                    <label class="control-label">Место регистрации</label>

                    <div class="controls">
                        <form:input type="text" path="regLocation"/>
                    </div>
                    <label class="control-label">Адресс регистрации</label>

                    <div class="controls">
                        <form:input type="text" path="regAddress"/>
                    </div>
                    <label class="control-label">Индекс проживания</label>

                    <div class="controls">
                        <form:input type="text" path="liveIndex"/>
                    </div>
                    <label class="control-label">Страна проживания</label>

                    <div class="controls">
                        <form:input type="text" path="liveCountry"/>
                    </div>
                    <label class="control-label hidden">liveCountryId</label>

                    <div class="controls hidden">
                        <form:input type="text" path="liveCountryId"/>
                    </div>
                    <label class="control-label">Регион проживания</label>

                    <div class="controls">
                        <form:input type="text" path="liveRegion"/>
                    </div>
                    <label class="control-label hidden">liveRegionId</label>

                    <div class="controls hidden">
                        <form:input type="text" path="liveRegionId"/>
                    </div>
                    <label class="control-label">Место проживания</label>

                    <div class="controls">
                        <form:input type="text" path="liveLocation"/>
                    </div>
                    <label class="control-label">Адрес проживания</label>

                    <div class="controls">
                        <form:input type="text" path="liveAddress"/>
                    </div>
                    <label class="control-label">Номер паспорта</label>

                    <div class="controls">
                        <form:input type="text" path="passportNumber"/>
                    </div>
                    <label class="control-label">Паспорт выдан: </label>

                    <div class="controls">
                        <form:input type="text" path="passportIssuer"/>
                    </div>
                    <label class="control-label">Дата выдачи паспорта</label>

                    <div class="controls">
                        <input type="hidden" name="passportIssueDate"/>
                        <input type="text" id="passportIssueDateBox"/>
                    </div>
                    <label class="control-label">Код подразделения</label>

                    <div class="controls">
                        <form:input type="text" path="passportIssuerCode"/>
                    </div>
                    <label class="control-label">ИНН</label>

                    <div class="controls">
                        <form:input type="text" path="inn"/>
                    </div>
                    <label class="control-label">Сайт</label>

                    <div class="controls">
                        <form:input type="text" path="webSite"/>
                    </div>
                    <div class="form-actions">
                        <button type="submit" class="btn btn-primary">Сохранить</button>
                    </div>
                </div>
            </fieldset>
        </div>
    </form:form>
    </div>
</div>
</body>
</html>

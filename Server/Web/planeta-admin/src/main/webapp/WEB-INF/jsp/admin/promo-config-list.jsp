<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ include file="/WEB-INF/jsp/includes/generated/doctype-header.jsp" %>
<head>
    <%@include file="head.jsp" %>
</head>

<body>

<%@ include file="/WEB-INF/jsp/admin/navbar.jsp" %>

<div id="page-wrapper">

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Конфигурации промо-писем</h1>
        </div>
    </div>

    <div class="main-page-actions">
        <form method="get">
            <input type="hidden" name="configId" value="0"/>
            <button formaction="/admin/promo-config.html" class="btn btn-success btn-circle btn-outline btn-lg" title="Добавить">
                <i class="fa fa-plus"></i>
            </button>
        </form>
    </div>

    <div class="row">
        <div class="col-lg-12 admin-table">
            <table class="table table-striped table-hover table-bordered admin-banner-image">
                <thead>
                    <tr>
                        <td><b>Название</b></td>
                        <td><b>Минимальная покупка</b></td>
                        <td><b>Есть промокоды</b></td>
                        <td><b>Срок действия</b></td>
                        <td><b>Статус</b></td>
                        <td><b>Действия</b></td>
                    </tr>
                </thead>
                <tbody>
                <c:forEach var="config" items="${configs}">
                    <tr>
                        <td>
                            <a href="/admin/promo-config.html?configId=${config.configId}">${config.name}</a>
                        </td>
                        <td>
                            ${config.priceCondition}
                        </td>
                        <td>
                            <c:if test="${not config.hasPromocode}">Нет</c:if>
                            <c:if test="${config.hasPromocode}">Да</c:if>
                        </td>
                        <td>
                            <fmt:formatDate value="${config.timeStart}" pattern="dd.MM.yyyy"/> &mdash;
                            <fmt:formatDate value="${config.timeFinish}" pattern="dd.MM.yyyy"/>
                        </td>
                        <td>
                            <c:if test="${config.status == 'PAUSED'}">На паузе</c:if>
                            <c:if test="${config.status == 'ACTIVE'}">Активный</c:if>
                            <c:if test="${config.status == 'FINISHED'}">Завершенный</c:if>
                        </td>
                        <td>
                            <form method="post" class="text-right">
                                <input type="hidden" name="configId" value="${config.configId}"/>

                                <div class="asdmin-table-actions">
                                    <c:if test="${config.status == 'PAUSED'}">
                                        <button type="submit" formaction="/admin/promo-config-switch-on-off.html" class="btn btn-success btn-outline" title="Включить">
                                            <i class="fa fa-play"></i>
                                        </button>
                                    </c:if>

                                    <c:if test="${config.status == 'ACTIVE'}">
                                        <button type="submit" formaction="/admin/promo-config-switch-on-off.html" class="btn btn-warning btn-outline" title="Выключить">
                                            <i class="fa fa-pause"></i>
                                        </button>
                                    </c:if>

                                    <a href="/admin/promo-config.html?configId=${config.configId}"
                                       class="btn btn-primary btn-outline" title="Редактировать">
                                        <i class="fa fa-pencil"></i>
                                    </a>
                                </div>
                            </form>
                        </td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>
    </div>
</div>




<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ include file="/WEB-INF/jsp/includes/generated/doctype-header.jsp" %>
<head>
    <%@include file="../head.jsp" %>

    <script type="text/javascript">
        $(document).ready(function() {
            $('.btn.promo-remove').on('click', function(e) {
                e.preventDefault();
                var href = e.currentTarget.href;
                Modal.showConfirm('Удалить партнера?', "Подтвердите удаление", {
                    success:function (e) {
                        document.location=href;
                    }
                });
            });

            //
            var startIndex;
            //JQUERY plugin that makes possible to sort collection and move collection elements
            var sortable = $('table tbody').sortable({
                revert: true,
                handle: '.btn.move',

                helper: function(e, ui) {
                    ui.children().each(function() {
                        $(this).width($(this).width());
                    });
                    return ui;
                },

                start: function(event, ui) {
                    ui.placeholder.html(ui.helper.html());
                    startIndex = ui.item.index();
                },

                update: function(event, ui) {
                    var stopIndex = ui.item.index();
                    if(stopIndex != startIndex) {
                        $('form#sort #startIndex').val(startIndex);
                        $('form#sort #stopIndex').val(stopIndex);
                        $('form#sort').submit();
                    }
                }
            });
            sortable.disableSelection();
        });
    </script>

</head>
<body>
<%@ include file="/WEB-INF/jsp/admin/navbar.jsp" %>

<div id="container" class="container">
    <div class="row">
        <div class="span12">
            <ul class="breadcrumb">
                <li><a href="/">Главная</a> <span class="divider">/</span></li>
                <li class="active">Библиородина<span class="divider">/</span>Партнеры<span class="divider">/</span></li>
            </ul>
        </div>
    </div>
    <div class="row">
        <div class="span12">
            <h2>Партнеры</h2>
            <p></p>
        </div>
    </div>
    <div class="row">
        <div class="span12">
            <a href="/admin/charity/partners-charity-fill.html" class="btn"><i class="icon-plus"></i> Добавить</a>
            <br/><br/>
            <table class="table table-bordered table-striped">
                <thead>
                <th>Фото</th>
                <th>Партнер</th>
                <th>Действие</th>
                </thead>
                <tbody>
                <c:forEach items="${configurationList}" var="promo" varStatus="st">
                    <tr>
                        <td width="1px">
                            <a class="ap-item" href="${promo.originalUrl}" target="_blank" title="${promo.name}">
                                <img src="${promo.imageUrl}" alt="${promo.name}" style="max-height: 70px; max-width: 116px;">
                            </a>
                        </td>
                        <td>
                                ${promo.name}
                        </td>
                        <td >
                            <a class="btn move"><i class="icon-move icon-black"></i></a>
                            <a href="/admin/charity/partners-charity-edit.html?id=${st.index}" class="btn"><i class="icon-pencil"></i></a>
                            <a href="/admin/charity/partners-charity-delete.html?id=${st.index}" class="btn promo-remove"><i class="icon-remove"></i></a>
                        </td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
            <a href="/admin/charity/partners-charity-fill.html" class="btn"><i class="icon-plus"></i> Добавить</a>
            <form id="sort" action="/admin/charity/partners-charity-sort.html" method="post">
                <input type="hidden" name="startIndex" id="startIndex" />
                <input type="hidden" name="stopIndex" id="stopIndex" />
            </form>
        </div>
    </div>
</body>




<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ include file="/WEB-INF/jsp/includes/generated/doctype-header.jsp" %>
<head>
<%@include file="../head.jsp" %>

</head>
<body>

<%@ include file="/WEB-INF/jsp/admin/navbar.jsp" %>

<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">ТВ</h1>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12 admin-table">
            <table class="table table-striped table-hover table-bordered admin-banner-image">
                <tbody>
                    <tr>
                        <td>
                            <b>На витрину будут выводиться трансляции №,№,№,...</b>
                            <br/>
                            <i><small>укажите id трансляций через запятую</small></i>
                        </td>
                        <td>
                            <form id="promo-start-form" style="display:inline" method="post" action="/moderator/broadcast-promo-list.html">
                                <div class="form-group input-group">
                                    <input type="text" class="form-control" id="stringValue" name="stringValue" value="${hf:getMyListAsString(configuration)}">
                                    <span class="input-group-btn">
                                        <button id="search" class="btn btn-primary" type="submit" title="Сохранить">
                                            <i class="fa fa-floppy-o"></i>
                                        </button>
                                    </span>
                                </div>
                            </form>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</body>




<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ include file="/WEB-INF/jsp/includes/generated/doctype-header.jsp" %>
<head>
    <%@include file="head.jsp" %>
    <style>
        .table td {
            word-break: break-word;
        }
    </style>
</head>
<body>

<%@ include file="/WEB-INF/jsp/admin/navbar.jsp" %>

<div id="page-wrapper">

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                <c:if test="${empty param.profileId}">
                    Редиректы
                </c:if>
                <c:if test="${not empty param.profileId}">
                    Мои ссылки
                </c:if>
            </h1>
        </div>
    </div>

    <div class="main-page-actions">
        <form method="get" style="display:inline">
            <input type="hidden" name="shortLinkId" value="0"/>
            <button formaction="/admin/edit-short-link.html"
                    class="btn btn-success btn-circle btn-outline btn-lg"
                    title="Добавить">
                <i class="fa fa-plus"></i>
            </button>
        </form>
        <c:if test="${myProfile != null && empty param.profileId}">
            <form method="get" style="display:inline">
                <input type="hidden" name="profileId" value="${myProfile.profile.profileId}"/>
                <button formaction="/admin/short-links-list.html"
                        class="btn btn-primary btn-circle btn-outline btn-lg"
                        title="Мои ссылки">
                    <i class="fa fa-link"></i>
                </button>
            </form>
        </c:if>
    </div>

    <div class="row">
        <div class="col-lg-12 admin-table">
            <table class="table table-striped table-hover table-bordered">
                <c:forEach var="shortLink" items="${shortLinkList}">
                    <tr>
                        <td>
                            <b>${shortLink.shortLinkId}</b>
                        </td>
                        <td>
                            <a href="${redirectUrlHost}/${shortLink.shortLink}">${shortLink.shortLink}</a>
                        </td>
                        <td>
                            <a href="${shortLink.redirectAddressUrl}">${shortLink.redirectAddressUrl}</a>
                        </td>
                        <td class="text-right">
                            <form method="post" style="display:inline">
                                <input type="hidden" name="shortLinkId" value="${shortLink.shortLinkId}"/>
                                <input type="hidden" name="shortLinkStatus" value="${shortLink.shortLinkStatus}"/>

                                <div class="admin-table-actions">
                                    <a href="/admin/edit-short-link.html?shortLinkId=${shortLink.shortLinkId}"
                                       class="btn btn-primary btn-outline"
                                       title="Редактировать">
                                        <i class="fa fa-pencil"></i>
                                    </a>

                                    <c:if test="${shortLink.shortLinkStatus == 'OFF'}">
                                        <button type="submit"
                                                formaction="/admin/switch-on-off-short-link.html"
                                                class="btn btn-success btn-outline"
                                                title="Включить">
                                            <i class="fa fa-play"></i>
                                        </button>
                                    </c:if>

                                    <c:if test="${shortLink.shortLinkStatus == 'ON'}">
                                        <button type="submit"
                                                formaction="/admin/switch-on-off-short-link.html"
                                                class="btn btn-warning btn-outline"
                                                title="Выключить">
                                            <i class="fa fa-stop"></i>
                                        </button>
                                    </c:if>

                                    <button type="submit"
                                            formaction="/admin/delete-short-link.html"
                                            class="btn btn-danger btn-outline"
                                            title="Удалить">
                                        <i class="fa fa-trash"></i>
                                    </button>
                                </div>
                            </form>
                        </td>
                    </tr>
                </c:forEach>
            </table>
        </div>
    </div>
</div>



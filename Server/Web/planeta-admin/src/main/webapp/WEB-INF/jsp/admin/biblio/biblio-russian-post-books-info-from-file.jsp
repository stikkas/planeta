<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ include file="/WEB-INF/jsp/includes/generated/doctype-header.jsp" %>
<%@taglib prefix="p" uri="http://planeta.ru/tags" %>
<head>
    <%@ include file="/WEB-INF/jsp/admin/head.jsp" %>
</head>
<body>
<%@ include file="/WEB-INF/jsp/admin/navbar.jsp" %>

<div id="page-wrapper">

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Загрузка информации по изданиям почты России</h1>
        </div>
    </div>

    <c:if test="${not empty errorMessage}">
        <div class="alert alert-danger" role="alert">
            <h4 class="alert-heading">ERROR</h4>
            <br>
            <p>${errorMessage}</p>
        </div>
    </c:if>

    <div class="panel panel-primary">
        <div class="panel-heading">Загрузка информации по изданиям из Excel</div>
        <div class="panel-body">
            <form class="form-horizontal" action="/moderator/biblio-russian-post-books-info-from-file.html" method="POST" enctype="multipart/form-data">
                <input name="file" type="file" class="ma-b-20">
                <button type="submit" class="btn btn-success">
                    <i class="fa fa-upload"></i> Загрузить из Excel
                </button>
            </form>
        </div>
    </div>

</div>
</body>
</html>


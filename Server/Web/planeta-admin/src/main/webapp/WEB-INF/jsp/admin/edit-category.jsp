<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ include file="/WEB-INF/jsp/includes/generated/doctype-header.jsp" %>
<head>
    <%@include file="head.jsp" %>
    <link type="text/css" rel="stylesheet"
          href="//${hf:getStaticBaseUrl("")}/admin-new/bootstrap/css/jquery-ui-1.9.2.custom.css"/>
</head>
<body>

<%@ include file="/WEB-INF/jsp/admin/navbar.jsp" %>

<div id="page-wrapper">

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                <c:if test="${category.id == 0}">
                    Новая категория
                </c:if>
                <c:if test="${category.id != 0}">
                    Редактирование
                </c:if>
            </h1>
        </div>
    </div>

    <%@ include file="successMessage.jsp" %>

    <div class="row">
        <div class="col-lg-12">
            <form:form commandName="category" class="form-horizontal">
                <form:hidden id="id" path="id"/>

                    <div class="row ma-b-20">
                        <div class="col-lg-6">
                            <label for="name">Название</label>
                            <form:input type="text" path="name" id="name" cssClass="form-control"/>
                            <form:errors path="name" cssClass="error"/>
                        </div>

                        <div class="col-lg-6">
                            <label for="engName">Английское название</label>
                            <form:input type="text" path="engName" id="engName" cssClass="form-control"/>
                            <form:errors path="engName" cssClass="error"/>
                        </div>
                    </div>

                    <div class="row ma-b-20">
                        <div class="col-lg-4">
                            <label for="mnemonicName">Алиас</label>

                            <form:input type="text" path="mnemonicName" id="mnemonicName"
                                        placeholder="только латинские буквы" pattern="^[A-Za-z0-9\-\_]+$"
                                        cssClass="form-control"/>
                            <form:errors path="mnemonicName" cssClass="error"/>
                        </div>

                        <div class="col-lg-4">
                            <label for="sponsorAlias">Алиас спонсора</label>
                            <form:select path="sponsorAlias" id="sponsorAlias" items="${sponsors}" cssClass="form-control"/>
                            <form:errors path="sponsorAlias" cssClass="error"/>
                        </div>

                        <div class="col-lg-4">
                            <label for="preferredOrder">Порядковый номер</label>
                            <form:input type="text" path="preferredOrder" id="preferredOrder" cssClass="form-control"/>
                            <form:errors path="preferredOrder" cssClass="error"/>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-12">
                                <c:choose>
                                    <c:when test="${category.editorVisibility}">
                                        <form:checkbox path="editorVisibility" id="editorVisibility" checked="checked"/>
                                    </c:when>
                                    <c:otherwise>
                                        <form:checkbox path="editorVisibility" id="editorVisibility"/>
                                    </c:otherwise>
                                </c:choose>
                                <form:errors path="editorVisibility" cssClass="error"/>

                                <label for="editorVisibility">Видимость в редакторе</label>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-12">
                            <c:choose>
                                <c:when test="${category.visibleInSearch}">
                                    <form:checkbox path="visibleInSearch" id="visibleInSearch" checked="checked"/>
                                </c:when>
                                <c:otherwise>
                                    <form:checkbox path="visibleInSearch" id="visibleInSearch"/>
                                </c:otherwise>
                            </c:choose>
                            <form:errors path="visibleInSearch" cssClass="error"/>

                            <label for="visibleInSearch">Видимость в поиске</label>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-12">
                            <c:choose>
                                <c:when test="${category.charityVisibility}">
                                    <form:checkbox path="charityVisibility" id="charityVisibility"
                                                   checked="checked"/>
                                </c:when>
                                <c:otherwise>
                                    <form:checkbox path="charityVisibility" id="charityVisibility"/>
                                </c:otherwise>
                            </c:choose>
                            <form:errors path="charityVisibility" cssClass="error"/>

                            <label for="charityVisibility">Видимость в благотворительности</label>
                        </div>
                    </div>

                    <div class="row ma-b-20">
                        <div class="col-lg-12">
                            <c:choose>
                                <c:when test="${category.specialProject}">
                                    <form:checkbox path="specialProject" id="specialProject" checked="checked"/>
                                </c:when>
                                <c:otherwise>
                                    <form:checkbox path="specialProject" id="specialProject"/>
                                </c:otherwise>
                            </c:choose>
                            <form:errors path="specialProject" cssClass="error"/>

                            <label for="specialProject">Спецпроект</label>
                        </div>
                    </div>

                    <div class="row ma-b-20">
                        <div class="col-lg-12">
                            <label for="warnText">Комментарии</label>
                            <form:textarea path="warnText" id="warnText" rows="6" class="form-control"/>
                            <form:errors path="warnText" cssClass="error"/>
                        </div>
                    </div>


                    <div class="btn-group">
                        <button type="submit" formaction="/admin/save-category.html" class="btn btn-primary">
                            <c:if test="${category.id == 0}">Добавить</c:if>
                            <c:if test="${category.id != 0}">Cохранить</c:if>
                        </button>
                        <a href="/moderator/campaign-tags.html" class="btn btn-default">
                            Назад
                        </a>
                    </div>
            </form:form>
        </div>
    </div>
</div>
</body>
</html>


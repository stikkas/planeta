<%@ page contentType="text/html; charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@ include file="/WEB-INF/jsp/includes/generated/doctype-header.jsp" %>
<%@taglib prefix="p" uri="http://planeta.ru/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<head>
    <link type="text/css" rel="stylesheet" href="//${hf:getStaticBaseUrl("")}/css-generated/vip-landing.css"/>
    <%@ include file="/WEB-INF/jsp/admin/head.jsp" %>
    <link type="text/css" rel="stylesheet" href="/css/ui-lightness/jquery-ui-1.10.2.custom.min.css"/>
    <link type="text/css" rel="stylesheet" href="//${hf:getStaticBaseUrl("")}/css/jquery.Jcrop.css">
    <link type="text/css" rel="stylesheet" href="//${hf:getStaticBaseUrl("")}/css/admin.Crop.css">

    <title>Программа лояльности - Бонусы</title>

    <%-- Style section --%>

    <style type="text/css">
        .upload-pic {
            display: block;
            width: 100%;
            padding: 10px 0;
        }

        .upload-pic:before, .upload-pic:after {
            display: table;
            content: "";
        }

        .upload-pic:after {
            clear: both;
        }

        .upload-pic .upload-pic-img {
            width: 176px;
            display: inline-block;
            float: left;
            max-height: 130px;
            overflow: hidden;
        }

        .upload-pic .upload-pic-img img {
            width: 100%;
        }

        .upload-pic .upload-path {
            padding: 0 10px 0 0;
        }

        .upload-pic .upload-path input {
            width: 100%;
            margin: 0;
        }

        .upload-pic .upload-pic-option {
            display: inline-block;
            float: left;
            padding-left: 15px;
            width: 335px;
        }

        .upload-pic .upload-pic-option h4 {
            line-height: 20px;
            font-size: 12px;
        }

        .upload-pic .upload-pic-option a {
            color: #26aedd;
            text-decoration: underline;
        }

        .upload-pic .upload-pic-option ul {
            margin: 0;
        }

        .upload-pic .upload-pic-option ul li {
            display: inline-block;
            padding: 8px 16px 8px 0;
            color: #adadaf;
            float: left;
        }

        .upload-pic .upload-pic-option ul li p {
            font-size: 11px;
            line-height: 12px;
            margin-bottom: 4px;
            text-align: center;
        }

        .upload-pic .upload-pic-option ul li span {
            background: #ddd;
            border: 1px solid #adadaf;
            display: block;
            margin: 0 auto;
            cursor: pointer;
        }

        .upload-pic .upload-pic-option ul li.active span {
            background: #aac9d2;
            border: 1px solid #8caab5;
        }

        .upload-pic .upload-pic-option ul li.small span {
            width: 20px;
            height: 20px;
        }

        .upload-pic .upload-pic-option ul li.medium span {
            width: 30px;
            height: 25px;
        }

        .upload-pic .upload-pic-option ul li.big span {
            width: 40px;
            height: 30px;
        }

        .upload-pic .upload-pic-option ul li.huge span {
            width: 50px;
            height: 35px;
        }

        .upload-pic .upload-pic-option ul li.original span {
            width: 60px;
            height: 40px;
        }

        .upload-pic .upload-pic-option ul li.custom div {
            background: #D7E6FB;
            border: 1px solid #B0C3E3;
            display: block;
            height: 70px;
            margin: 0 auto;
            padding: 5px;
            text-align: center;
            width: 100px;
        }

        .upload-pic .upload-pic-option ul li.custom div p {
            color: #adadaf;
            font-size: 11px;
            line-height: 12px;
        }

        .upload-pic .upload-pic-option ul li.custom label {
            font-size: 11px;
            line-height: 12px;
            width: 98px;
        }

        .upload-pic .upload-pic-option ul li.custom input {
            height: 20px;
            margin: 0 2px;
            min-height: 10px;
            width: 44px;
        }

        .modal-foto-load {
            display: block;
        }

        .modal-foto-load .tc {
            text-align: center;
        }

        .modal-foto-load .modal-foto-load-select {
            display: inline-block;
            width: 554px;
        }

        .modal-foto-load .modal-foto-load-select span {
            color: #717171;
            display: inline-block;
            float: left;
            font-weight: 700;
            margin: 5px 12px;
            padding: 45px 0 14px;
            text-align: center;
            width: 160px;
            cursor: pointer;
        }

        .modal-foto-load .modal-foto-load-select span.link {
            background: url(https://${properties['static.host']}/images/planeta/mp-link.png) no-repeat 50% 10px #f0f0f0;
        }

        .modal-foto-load .modal-foto-load-select span.upload {
            background: url(https://${properties['static.host']}/images/planeta/mp-upload.png) no-repeat 50% 10px #f0f0f0;
        }

        .modal-foto-load .modal-foto-load-select span.album {
            background: url(https://${properties['static.host']}/images/planeta/mp-album.png) no-repeat 50% 10px #f0f0f0;
        }

        .modal-foto-load .modal-foto-load-select span:hover {
            background-color: #ebfbff;
        }

        /*===== style restoring =====*/
        body {
            background-color: #fff;
            background-image: none;
            font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;
            line-height: 20px;
        }

        .btn {
            font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;
            text-transform: none;
        }

        /*=============== ==========================*/
        .w100 {
            width: 100%;
        }

        [class*="span"] {
            paddig-left: 8px;
        }

        .cur-p {
            cursor: pointer;
        }

        .hidden-fire:hover {
            color: #ffffff;
            background-color: #bd362f;
            text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.25);
            background-image: -moz-linear-gradient(top, #ee5f5b, #bd362f);
            background-image: -o-linear-gradient(top, #ee5f5b, #bd362f);
            background-image: linear-gradient(to bottom, #ee5f5b, #bd362f);
            background-repeat: repeat-x;
            border-color: rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.25);
        }

        .hidden-fire:hover .icon {
            background-position: -72px -120px !important;
            background: url(https://${properties['static.host']}/admin-new/bootstrap/img/glyphicons-halflings-white.png);
        }

        .draggable:active {
            background-color: #ccc62f;
        }

        .grid-img > img {
            max-width: inherit;
            height: inherit;
        }

        section {
            display: block;
        }

        .connected, .sortable, .exclude, .handles {
            margin: auto;
            padding: 0;
            -webkit-touch-callout: none;
            -webkit-user-select: none;
            -khtml-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }

        .sortable.grid {
            display: flex;
            -webkit-flex-flow: row wrap;
            justify-content: space-around;
        }

        .connected li, .sortable li, .exclude li, .handles li {
            list-style: none;
            border: 1px solid #CCC;
            background: #F6F6F6;
            /*font-family: "Tahoma";
            color: #1C94C4;*/
            margin: 10px;
            padding: 10px;
            height: 22px;
        }

        .ui-sortable-handle {
            margin: 15px;
            width: 360px;
        }

        .handles span {
            cursor: move;
        }

        .sortable.grid li {
            float: left;
            width: 380px;
            height: 350px;
            text-align: center;
        }

        li.highlight {
            background: #FEE25F;
        }

        li.sortable-placeholder {
            border: 1px dashed #CCC;
            background: none;
        }

        .reorder-panel {
            border-top: 1px solid #d8dade;
            background: rgba(255, 255, 255, 0.89);
            position: fixed;
            bottom: 0;
            left: 0;
            width: 100%;
            padding: 20px;
        }

        .bonus-actions {
            display: block;
        }

        .bonus-actions .btn {
            border-radius: 0;
            display: block;
        }

        .bonus-template {
            width: 360px !important;
            height: 334px !important;
            margin: 15px !important;
        }

        .bonus-template_img {
            border: 1px solid #ccc;
            border-bottom: none;
            width: 360px;
            height: 300px;
            text-align: center;
            font-size: 20px;
            color: white;
            display: flex;
            background: #1c94c4;
        }

        .project-create_upload_drag {
            display: none;
            position: absolute;
            top: 0;
            bottom: 0;
            right: 0;
            -webkit-box-sizing: border-box;
            box-sizing: border-box;
            width: 100%;
            padding: 15px;
            font-family: 'ProximaNova', Arial, sans-serif;
            font-weight: 400;
            font-size: 14px;
            line-height: 18px;
            text-align: center;
            color: #262626;
            border: 2px dashed #e1e1e1;
            border-radius: 20px;
            background: #fff url(//${hf:getStaticBaseUrl("")}/images/project/pc-upload-drag.png) 50% 90% no-repeat;
        }
        .can-drop .project-create_upload_drag {
            display: block;
        }

        @media screen and (max-width: 1366px) and (min-width: 767px) {
            .bonus-template {
                width: 300px !important;
                height: 290px !important;
                margin: 3px !important;
            }

            .bonus-template_img {
                width: 297px;
                height: 260px;
            }

            .bonus-template_img img {
                width: 297px !important;
                height: 260px !important;
            }
        }
    </style>

    <%-- Templates section --%>

    <script id="bonus-template" type="text/x-jquery-template">
        <div class="bonus-template_img">
            {{if type == "BONUS"}}
                <img src="{{= imageUrl}}" alt="360x300" data-src="holder.js/360x300">
            {{else type == "INFO"}}
                <div style="margin: auto">{{= name}}</div>
            {{/if}}
        </div>

        <div class="btn-group bonus-actions">
            {{if enabled}}
                <a class="btn btn-success col-lg-4" data-event-click="onToggleEnabling" title="Выключить">
                    <i class="fa fa-eye"></i>
                </a>
            {{else}}
                <a class="btn btn-warning col-lg-4" data-event-click="onToggleEnabling" title="Включить">
                    <i class="fa fa-eye-slash"></i>
                </a>
            {{/if}}

            <a class="btn btn-danger col-lg-4" data-event-click="onDelete" title="Удалить">
                <i class="fa fa-trash"></i>
            </a>
            <a class="btn btn-primary col-lg-4" data-event-click="onEdit" title="Редактировать">
                <i class="fa fa-pencil"></i>
            </a>
        </div>
    </script>

    <script id="bonus-preview-template" type="text/x-jquery-template">
        {{if type == "BONUS"}}
        <div class="grid-i wow flipInY animated" style="visibility: visible; animation-delay: 50ms;">
            <div class="grid-img">
                <img src="{{= imageUrl}}"/>
            </div>
            <div class="grid-img-text">
                <div class="grid-img-text_cont">{{= name}}</div>
            </div>
            {{if !available }}
            <div class="grid-sale">
                <div class="grid-sale_text">Эти подарки уже разобрали</div>
            </div>
            {{/if}}
        </div>
        {{else}}
        {{html descriptionHtml}}
        {{/if}}
    </script>

    <script id="bonus-modal-editor-template" type="text/x-jquery-template">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4>
                {{if bonusId}}Редактирование{{else}}Создание{{/if}} бонуса.
            </h4>
        </div>
        <div class="modal-body">
            <%--//TODO новая админка: доделать красиво--%>
            <div class="row">
                <div class="col-lg-12">
                    {{if previewMode}}
                        {{tmpl '#bonus-preview-template'}}
                    {{else}}
                        <form>
                            <div class="row ma-b-20">
                                <div class="col-lg-12">
                                    <label>Тип</label>
                                    <div>
                                        <input type="radio" name="type" value="INFO" {{if type== "INFO"}}checked{{/if}} />
                                        Информация

                                        <input type="radio" name="type" value="BONUS" {{if type== "BONUS"}}checked{{/if}} />
                                        Бонус
                                    </div>
                                </div>
                            </div>

                            <div class="row ma-b-20">
                                <div class="col-lg-12">
                                    <label>Название</label>
                                    <textarea name="name" placeholder="Введите название" class="form-control" rows="1">{{if name}}{{= name }}{{/if}}</textarea>
                                    <div class="error-message"></div>
                                </div>
                            </div>

                            {{if type == "BONUS"}}

                            <div class="row ma-b-20">
                                <div class="col-lg-12">
                                    <div class="js-image-field"></div>
                                </div>
                            </div>

                            <div class="row ma-b-20">
                                <div class="col-lg-12">
                                    <label>Текстовое описание</label>
                                    <textarea name="description" placeholder="Описание, контакты." class="form-control"
                                              rows="6">{{if description}}{{= description }}{{/if}}</textarea>
                                    <div class="error-message"></div>
                                </div>
                            </div>

                            <div class="row ma-b-20">
                                <div class="col-lg-12">
                                    <label>Количество(Остаток)</label>
                                    <input name="available" value="{{= available }}" type="text" placeholder="Введите количество"
                                           class="form-control"/>
                                    <div class="error-message"></div>
                                </div>
                            </div>

                            <div class="row ma-b-20">
                                <div class="col-lg-12">
                                    <label>ID или алиас кампании</label>
                                    <input name="campaignAlias" value="{{= campaignAlias }}" type="text"
                                           placeholder="Введите алиас кампании" class="form-control"/>
                                    <div class="error-message"></div>
                                </div>
                            </div>

                            <div class="row ma-b-20">
                                <div class="col-lg-12">
                                    <label >Ссылка:</label>
                                    <input name="link" value="{{= link }}" type="text" placeholder="Введите адрес страницы"
                                           class="form-control"/>
                                    <div class="error-message"></div>
                                </div>
                            </div>

                            <div class="row ma-b-20">
                                <div class="col-lg-12">
                                    <label>Текст кнопки</label>
                                    <input name="buttonText" value="{{= buttonText }}" type="text" placeholder="Введите текст"
                                           class="form-control"/>
                                    <div class="error-message"></div>
                                </div>
                            </div>

                            <div class="row ma-b-20">
                                <div class="col-lg-12">
                                    <label>Текст ссылки</label>
                                    <input name="linkText" value="{{= linkText }}" type="text" placeholder="Введите текст"
                                           class="form-control"/>
                                    <div class="error-message"></div>
                                </div>
                            </div>

                            {{else type == "INFO"}}
                                <div class="row">
                                    <div class="col-lg-12">
                                        <label>Текстовое описание(HTML)</label>
                                        <textarea name="descriptionHtml" placeholder="Описание, контакты." class="form-control" rows="6">
                                    {{= descriptionHtml }}
                                </textarea>
                                        <div class="error-message"></div>
                                    </div>
                                </div>
                            {{/if}}
                        </form>
                    {{/if}}
                </div>
            </div>
        </div>

        <div class="modal-footer">
            <div class="modal-footer-cont">
                <button type="submit" class="btn btn-primary">Сохранить</button>
                <button type="reset" class="btn">Отмена</button>
                <div class="btn btn-success span3" data-event-click="onTogglePreview">
                    {{if previewMode}}Редактирование{{else}}Предпросмотр{{/if}}
                </div>
            </div>
        </div>
    </script>

    <script id="reorder-panel-template" type="text/x-jquery-template">
        <div class="row">
            <div class="col-lg-12">
                <strong class="col-lg-8 offset1">Порядок бонусов изменен</strong>
                <div class="col-lg-4 text-right">
                    <div class="btn-group">
                        <button class="btn btn-default" data-event-click="onCancel">
                            <i class="icon icon-remove"></i>Отменить
                        </button>
                        <button class="btn btn-primary" data-event-click="onReorder">
                            <i class="icon icon-white icon-ok"></i>
                            Зафиксировать
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </script>

    <script id="main-container-template" type="text/x-jquery-template">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Управление бонусами проектов</h1>
            </div>
        </div>

        <div class="main-page-actions">
            <a class="btn btn-success btn-circle btn-outline btn-lg" data-event-click="onCreateBonus" title="Добавить новый бонус">
                <i class="fa fa-plus"></i>
            </a>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <ul class="nav nav-tabs" style="margin-bottom: 30px">
                    <li class="active"><a href="#bonuses" data-toggle="tab">Редактирование</a></li>
                    <li><a href="#bonuses-preview" data-toggle="tab" data-event-click="onPreview">Предпросмотр</a></li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active bonuses-container" id="bonuses">
                        <div class="alert alert-danger bs-alert-old-docs">
                            <strong>Внимание!</strong> Параметры картинок здесь ОТЛИЧАЮТСЯ от размеров, которые будет видно
                            на странице бонусов.
                            <br><strong>Размер картинки должен быть 360x300px, иначе картинка будет сжата либо растянута!</strong>
                        </div>
                    </div>
                    <div class="tab-pane" id="bonuses-preview"></div>
                </div>
            </div>
        </div>
    </script>

    <script id="loyalty-view-image-template" type="text/x-jquery-template">
        <div class="col-lg-12">
            <label>Изображение</label>
            <div class="js-val"></div>
        </div>
    </script>

    <script id="loyalty-view-image-load-view-template" type="text/x-jquery-template">
        <div class="js-progress"></div>
        <div class="js-drag-zone">
            <div class="btn btn-success" style=" position: relative; overflow: hidden;">
                Загрузить фото
                <input type="file" name="files[]" class="js-input-file"
                       style=" position: absolute; top: 0; right: 0; margin: 0; opacity: 0; font-size: 200px;">
            </div>
            <div class="btn btn-info js-link-to-image">Фото по ссылке</div>
            <div class="project-create_upload_drag">
                Перетащите картинку <br> сюда
            </div>
        </div>
    </script>

    <script id="loyalty-view-image-crop-view-template" type="text/x-jquery-template">
        <img src="{{if $data.thumbnail}}{{= ImageUtils.getThumbnailUrl(imageUrl, thumbnail.imageConfig, thumbnail.ImageType)}}{{else}}{{= imageUrl}}{{/if}}"
             {{if width}}style="width: {{= width}}px" {{/if}}>

        <div class="btn-group">
            <%--<span class="btn btn-success" data-event-click="onCropClick" title="Обрезать">
                <i class="fa fa-crop"></i>
            </span>--%>
            <span class="btn btn-danger" data-event-click="onRemoveClick" title="Удалить">
                <i class="fa fa-trash"></i>
            </span>
        </div>
    </script>

    <script id="loyalty-load-image-field-progress-template" type="text/x-jquery-template">
        <div>
            {{if status !== FileUploadStatuses.SAVED}}
            Загрузка {{= Math.floor(uploadedSize / fileSize * 100)}}%
            {{else}}
            Обработка файла, подождите.
            {{/if}}
        </div>
        {{if status !== FileUploadStatuses.SAVED}}
        <div class="btn js-cancel">Отмена</div>
        {{/if}}
    </script>

    <%-- JS section --%>

    <script type="application/javascript">
        var Loyalty = {
            Models: {
                sync: function (url, data) {
                    var $dfd = $.Deferred();
                    $.ajax(url, _.extend({
                        type: 'POST',
                        data: JSON.stringify(data),
                        dataType: 'json',
                        contentType: 'application/json; charset=utf-8',
                        success: function (response) {
                            if (response.success) {
                                $dfd.resolve(response.result);
                            } else {
                                $dfd.reject(response.errorMessage, response.fieldErrors);
                            }
                        }
                    }));

                    return $dfd.promise();
                }
            },
            Views: {}
        };

        Loyalty.Models.Bonus = BaseModel.extend({

            remove: function () {
                var self = this;
                self.set({deleted: true}, {silent: true});

                return this.save().done(function () {
                    self.collection.remove(self);
                });
            },

            toggleEnabling: function () {
                this.set('enabled', !this.get('enabled'));
                return this.save();
            },

            save: function (options) {
                var self = this;
                var isNew = this.isNew();

                return Loyalty.Models.sync('/admin/loyalty/save-bonus.json', this.toJSON())
                    .done(function (bonus) {
                        self.set(bonus);
                        if (isNew) {
                            self.collection.add(self);
                        }
                    }).fail(function () {
                        if (!isNew) {
                            self.set(self.previousAttributes());
                        }
                    });
            }
        });

        Loyalty.Models.Bonuses = BaseCollection.extend({
            model: Loyalty.Models.Bonus,

            createNew: function () {
                return new Loyalty.Models.Bonus({
                    type: 'BONUS',
                    name: '',
                    imageUrl: '',
                    available: 1,
                    priority: this.lowestPriority()
                }, {collection: this});
            },

            lowestPriority: function () {
                return this.max(function (bonus) {
                    return bonus.priority;
                }) + 1;
            },

            reorder: function (newSequence, options) {
                var self = this;
                var bonuses = [];
                _.each(newSequence, function (id, index) {
                    var bonus = self.findByAttr('id', id);
                    if (bonus) {
                        bonuses.push(bonus);
                    }
                });

                this.reset(bonuses, options);
                this.trigger('reorder')
            },

            asSequence: function () {
                return this.map(function (bonus, i) {
                    return {
                        bonusId: bonus.get('id'),
                        priority: i
                    }
                });
            }
        });

        Loyalty.Models.Main = BaseModel.extend({

            initialize: function () {
                this.saveOriginalBonusSequence();
                this.set('enabledBonuses', new BaseCollection([]));
            },

            saveOriginalBonusSequence: function () {
                this.originalBonusSequence = this.get('bonuses').pluck('id');
            },

            restoreBonuses: function () {
                this.get('bonuses').reorder(this.originalBonusSequence);
            },

            reorderBonuses: function () {
                var self = this;
                return Loyalty.Models.sync('/admin/loyalty/reorder-bonuses.json', this.get('bonuses').asSequence())
                    .done(function () {
                        self.saveOriginalBonusSequence();
                    });
            },

            updateEnabledBonuses: function () {
                var bonuses = this.get('bonuses').filter(function (bonus) {
                    return bonus.get('enabled') && !bonus.get('deleted');
                });
                this.get('enabledBonuses').reset(bonuses);
            }
        });

        Loyalty.Views.Bonus = BaseView.extend({
            template: "#bonus-template",
            tagName: 'div',
            className: 'bonus-template',
            events: {
                'dblclick': 'onEdit'
            },

            afterRender: function () {
                this.$el.data('id', this.model.get('id'));
            },

            onToggleEnabling: function () {
                this.model.toggleEnabling().done(function () {
                    workspace.appView.showSuccessMessage('Доступность бонуса изменена');
                }).fail(function (errorMessage) {
                    workspace.appView.showErrorMessage(errorMessage || '...ну не судьба..');
                });
            },

            onDelete: function () {
                var self = this;
                Modal.showConfirm('Вы действительно хотите удалить бонус?', 'Удаление бонуса', {
                    success: function () {
                        self.model.remove().done(function () {
                            workspace.appView.showSuccessMessage('Бонус удален');
                        }).fail(function (errorMessage) {
                            workspace.appView.showErrorMessage(errorMessage || '...ну не судьба..');
                        });
                    }
                });
            },

            onEdit: function () {
                new Loyalty.Views.ModalBonusEditor({
                    model: this.model
                }).render();
            }
        });

        Loyalty.Views.Bonuses = BaseListView.extend({
            itemViewType: Loyalty.Views.Bonus,
            tagName: 'div',
            className: 'sortable grid',

            afterRender: function () {
                var self = this;
                this.$el.sortable().on('sortupdate', function () {
                    var sequence = [];
                    self.$el.find('.bonus-template').each(function (i, el) {
                        sequence.push($(el).data("id"));
                    });

                    self.collection.reorder(sequence, {silent: true});
                });
            }
        });

        Loyalty.Views.BonusPreview = BaseView.extend({
            template: '#bonus-preview-template'
        });

        Loyalty.Views.BonusPreviews = BaseListView.extend({
            itemViewType: Loyalty.Views.BonusPreview,
            className: ''
        });

        Loyalty.Views.ImageField = CrowdFund.Views.ImageField.extend({
            template: '#loyalty-view-image-template',
            createLoadView: function () {
                return new Loyalty.Views.ImageField.LoadView({model: this.model});
            },
            createCropView: function () {
                return new Loyalty.Views.ImageField.CropView({model: this.model});
            }
        });

        Loyalty.Models.ImageField = CrowdFund.Models.ImageField.extend({
            defaults: _.defaults({
                albumTypeId: AlbumTypes.ALBUM_COMMENT,
                thumbnail: {
                    imageConfig: ImageUtils.MEDIUM,
                    imageType: ImageType.PHOTO
                }
            }, CrowdFund.Models.ImageField.prototype.defaults),
            initialize: function () {
                CrowdFund.Models.ImageField.prototype.initialize.apply(this, arguments);
                this.set('profileId', workspace.appModel.get('profileModel').get('profileId'));
            },
            onImageUrlChange: function (model) {
                model.set('imageUrl', this.get('imageUrl'));
            }
        });

        Loyalty.Views.ImageField.LoadView = CrowdFund.Views.ImageField.LoadView.extend({
            template: '#loyalty-view-image-load-view-template',
            createProgressView: function (fileUploadModel, fileUploader) {
                return new Loyalty.Views.ImageField.LoadView.Progress({
                    model: fileUploadModel,
                    fileUploader: fileUploader
                });
            }
        });

        Loyalty.Views.ImageField.LoadView.Progress = CrowdFund.Views.ImageField.LoadView.Progress.extend({
            template: '#loyalty-load-image-field-progress-template'
        });

        Loyalty.Views.ImageField.CropView = CrowdFund.Views.ImageField.CropView.extend({
            template: '#loyalty-view-image-crop-view-template'
        });

        Loyalty.Views.ModalBonusEditor = Modal.View.extend({
            template: '#bonus-modal-editor-template',
            className: 'modal fade in',

            events: _.extend({}, Modal.View.prototype.events, {
                'keyup input[name]': 'onControlChanged',
                'keyup textarea[name]': 'onControlChanged',
                'drop input[name]': 'onControlChanged',
                'drop textarea[name]': 'onControlChanged',
                'change input[name]': 'onControlChanged',
                'change input[name="type"]': 'render'
            }),

            construct: function () {
                var width = 360;
                var height = 300;
                this.cardImageModel = new Loyalty.Models.ImageField({
                    originalImage: {
                        imageUrl: this.model.get('imageUrl')
                    },
                    aspectRatio: width / height,
                    width: width
                });

                this.addChildAtElement('.js-image-field', new Loyalty.Views.ImageField({
                    model: this.cardImageModel,
                    controller: this.model
                }));
            },

            onControlChanged: function (e) {
                var el = this.$(e.currentTarget);
                var name = el.attr('name');
                var value = el.val();

                this.model.set(name, value, {silent: true});
                this.model.trigger('change:' + name, value);
            },

            onTogglePreview: function () {
                this.model.set('previewMode', !this.model.get('previewMode'));
            },

            onAttachImage: function () {
                var model = this.model;
                var photoSelect = new Attach.Models.PhotoSelect({
                    selected: function (photo) {
                        model.set({
                            imageId: photo.id,
                            imageUrl: photo.value
                        });
                    }
                });

                new Attach.Views.PhotoLink({
                    model: photoSelect,
                    underlyingModal: this
                }).render();
            },

            save: function () {
                var self = this;
                return this.model.save().done(function (result) {
                    self.cancel();
                }).fail(_.bind(Delivery.ViewUtils.highlightErrors, this));
            },

            afterRender: function () {
                Modal.View.prototype.afterRender.call(this);
                this.$('.elastic').elastic();
            }
        });

        Loyalty.Views.ReorderPanel = BaseView.extend({
            template: '#reorder-panel-template',
            className: 'reorder-panel hide',

            onReorder: function () {
                this.$el.hide();
                this.model.reorderBonuses();
            },

            onCancel: function () {
                this.model.restoreBonuses();
                this.$el.hide();
            }
        });

        Loyalty.Views.Main = BaseView.extend({
            el: '#page-wrapper',
            template: '#main-container-template',

            construct: function (options) {
                this.addChildAtElement('#bonuses', new Loyalty.Views.Bonuses({
                    collection: this.model.get('bonuses')
                }));
                this.addChildAtElement('#bonuses-preview', new Loyalty.Views.BonusPreviews({
                    collection: this.model.get('enabledBonuses')
                }));
                this.reorderPanel = this.addChild(new Loyalty.Views.ReorderPanel({
                    model: this.model
                }));

                this.model.get('bonuses').on('reorder', this.onReordered, this);
            },

            onCreateBonus: function () {
                new Loyalty.Views.ModalBonusEditor({
                    model: this.model.get('bonuses').createNew()
                }).render();
            },

            onReordered: function () {
                this.reorderPanel.$el.show();
            },

            onPreview: function () {
                this.model.updateEnabledBonuses();
            }
        });

        $(document).ready(function () {
            var bonuses = _.chain(${hf:toJson(bonuses)})
                .filter(function (bonus) {
                    return !bonus.deleted;
                }).map(function (bonus) {
                    return _.extend(bonus, {id: bonus.bonusId});
                }).value();

            var model = new Loyalty.Models.Main({
                bonuses: new Loyalty.Models.Bonuses(bonuses)
            });
            var view = new Loyalty.Views.Main({
                model: model
            });

            view.render();
        });

    </script>

</head>

<body>

<%@ include file="/WEB-INF/jsp/admin/navbar.jsp" %>

<div id="page-wrapper"></div>

</body>

</html>


<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ include file="/WEB-INF/jsp/includes/generated/doctype-header.jsp" %>
<head>
    <%@ include file="head.jsp" %>
</head>
<body>
<%@ include file="/WEB-INF/jsp/admin/navbar.jsp" %>
<div id="page-wrapper">

    <div class="row">
        <div class="col-lg-12">
            <c:if test="${configuration.key == null}">
                <h1 class="page-header">Добавить</h1>
            </c:if>
            <c:if test="${configuration.key != null}">
                <h1 class="page-header">Редактировать</h1>
            </c:if>
        </div>
    </div>

    <div class="row ">
        <div class="span12">
            <div class="mrg-b-30">

            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <form:form commandName="configuration">
                <div class="row ma-b-20">
                    <div class="col-lg-4">
                        <label>Ключ</label>

                        <c:choose>
                            <c:when test="${configuration.key == null}">
                                <form:input path="key" type="text" cssClass="form-control" placeholder="" id="key"/>
                                <form:errors path="key" cssClass="error"/>
                            </c:when>
                            <c:when test="${configuration.key != null}">
                                <div class="span5">
                                    <label class="label-value">${configuration.key}</label>
                                    <input type="hidden" name="key" value="${configuration.key}">
                                </div>
                            </c:when>
                        </c:choose>
                    </div>
                </div>

                <div class="row ma-b-20">
                    <div class="col-lg-4">
                        <label>Значение - число</label>

                        <form:input path="intValue" cssClass="form-control" placeholder="0"/>
                        <form:errors path="intValue" cssClass="error"/>
                    </div>
                </div>

                <div class="row ma-b-20">
                    <div class="col-lg-4">
                        <label>Значение - строка</label>

                        <form:textarea path="stringValue" cssClass="form-control" rows="5" placeholder=""/>
                        <form:errors path="stringValue" cssClass="error"/>
                    </div>
                </div>

                <div class="row ma-b-20">
                    <div class="col-lg-4">
                        <div class="checkbox">
                            <label>
                                <form:checkbox path="booleanValue"/>
                                <form:errors path="booleanValue" cssClass="error"/>
                                Значение булево
                            </label>
                        </div>
                    </div>
                </div>

                <button type="submit" formaction="/admin/configuration-edit.html" class="btn btn-primary">
                    Cохранить
                </button>
                </form:form>
        </div>
    </div>
</div>
</body>
</html>

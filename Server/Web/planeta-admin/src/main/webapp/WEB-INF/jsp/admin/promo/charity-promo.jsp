<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ include file="/WEB-INF/jsp/includes/generated/doctype-header.jsp" %>
<head>
    <title>Загрузить картинку</title>
    <%@ include file="../head.jsp" %>
</head>
<body>

<%@ include file="/WEB-INF/jsp/admin/navbar.jsp" %>

<div id="page-wrapper">

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Благотворительность</h1>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12 admin-table">
                <table class="table table-striped table-hover table-bordered admin-banner-image">
                    <tbody>
                        <tr>
                            <td class="col-lg-6">
                                <b>Благотворительность, раздел "Обучение" будут выводиться трансляции №,№,№</b>
                                <br/>
                                <i><small>укажите id трансляций через запятую</small></i>
                            </td>
                            <td class="col-lg-6">
                                <form id="charity-broadcast-form" style="display:inline" method="post" action="/moderator/charity-broadcast-list.html">
                                    <div class="form-group input-group">
                                        <input type="text" class="form-control" id="charityRecentWebinarsIds" name="charityRecentWebinarsIds" value="${hf:getMyListAsString(charityRecentWebinarsList)}">
                                        <span class="input-group-btn">
                                            <button id="search" class="btn btn-primary" type="submit" title="Сохранить">
                                                <i class="fa fa-floppy-o"></i>
                                            </button>
                                        </span>
                                    </div>
                                </form>
                            </td>
                        </tr>
                    </tbody>
                </table>

                <table class="table table-striped table-hover table-bordered admin-banner-image">
                    <tbody>
                        <tr>
                            <td class="col-lg-6">
                                <b>Проекты, которые будут выводиться в карусель №,№,№</b>
                                <br/>
                                <i><small>укажите id проектов через запятую</small></i>
                            </td>
                            <td class="col-lg-6">
                                <form id="start-charity-promo-form" style="display:inline" method="post" action="/moderator/start-charity-promo-list.html">
                                    <div class="form-group input-group">
                                        <input type="text" class="form-control" id="charityPromoCampaignsIds" name="charityPromoCampaignsIds" value="${hf:getMyListAsString(charityPromoCampaignsList)}">
                                        <span class="input-group-btn">
                                            <button id="search" class="btn btn-primary" type="submit" title="Сохранить">
                                                <i class="fa fa-floppy-o"></i>
                                            </button>
                                        </span>
                                    </div>
                                </form>
                            </td>
                        </tr>
                    </tbody>
                </table>

                <table class="table table-striped table-hover table-bordered admin-banner-imag">
                    <tbody>
                        <tr>
                            <td class="col-lg-6">
                                <b>Проекты, которые будут выводиться на главную страницу №,№,№</b>
                                <br/>
                                <i><small>укажите id проектов через запятую</small></i>
                            </td>
                            <td class="col-lg-6">
                                <form id="charity-top-campaigns-form" style="display:inline" method="post" action="/moderator/start-charity-top-list.html">
                                    <div class="form-group input-group">
                                        <input type="text" class="form-control" id="charityTopCampaignsIds" name="charityTopCampaignsIds" value="${hf:getMyListAsString(charityTopCampaignsList)}">
                                        <span class="input-group-btn">
                                            <button id="search" class="btn btn-primary" type="submit" title="Сохранить">
                                                <i class="fa fa-floppy-o"></i>
                                            </button>
                                        </span>
                                    </div>
                                </form>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
</body>

<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<form method="get" action="/moderator/contractors.html" class="clearfix">

    <input type="hidden" name="offset" value="0">
    <input type="hidden" name="limit" value="10">
    <input type="hidden" name="campaignId" value="${campaignId}">


    <div class="form-group input-group">
        <input type="text" class="form-control" placeholder="Введите ID контрагента, проект к которому он привязан или ИНН" id="xlInput" name="searchString" value="${searchString}">
        <span class="input-group-btn">
            <button class="btn btn-primary" type="submit">
                <i class="fa fa-search"></i>
            </button>
        </span>
    </div>
</FORM>

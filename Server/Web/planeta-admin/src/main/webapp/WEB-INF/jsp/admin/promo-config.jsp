<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ include file="/WEB-INF/jsp/includes/generated/doctype-header.jsp" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<head>
    <%@include file="head.jsp" %>
    <link type="text/css" rel="stylesheet"
          href="//${hf:getStaticBaseUrl("")}/admin-new/bootstrap/css/jquery-ui-1.9.2.custom.css"/>


    <script type="text/javascript">
        $(document).ready(function () {
            $("#date-box-start").datepicker();
            $("#date-box-start").bind('change', function () {
                var date = $(this).datepicker('getDate');
                $("#timeStart").val(date.getTime());
            });
            $("#date-box-finish").datepicker();
            $("#date-box-finish").bind('change', function () {
                var date = $(this).datepicker('getDate');
                $("#timeFinish").val(date.getTime());
            });
            if ($("#timeStart").val() == null) {
                $("#timeStart").val(new Date().getTime());
            }

            var $promoCodeBlock = $('#promoCodeBlock');

            $(document).on('change', '#hasPromocode', function () {
                if ($(this).is(":checked")) {
                    $promoCodeBlock.show();
                } else {
                    $promoCodeBlock.hide();
                }
            });

            if (${promoConfig.hasPromocode}) {
                $promoCodeBlock.show();
            } else {
                $promoCodeBlock.hide();
            }
        });
    </script>

</head>

<body>

<%@ include file="/WEB-INF/jsp/admin/navbar.jsp" %>


<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Конфигурация промо-письма ${promoConfig.name}</h1>
        </div>

        <div class="col-lg-12">
            <div class="row">
                <div class="col-lg-12">
                    <form:form commandName="promoConfig" class="form-horizontal">
                        <form:hidden id="configId" path="configId"/>
                        <form:hidden id="creatorId" path="creatorId"/>
                        <form:hidden id="updaterId" path="updaterId"/>
                        <input type="hidden" name="timeAdded" value="${promoConfig.timeAdded.time}"/>
                        <input type="hidden" name="timeUpdated" value="${promoConfig.timeUpdated.time}"/>

                        <div class="row ma-b-20">
                            <div class="col-lg-4">
                                <c:if test="${promoConfig.creatorId > 0}">
                                    <p>Создан <fmt:formatDate value="${promoConfig.timeAdded}" pattern="dd.MM.yyyy HH:mm"/> пользователем ${promoConfig.creatorId}</p>
                                </c:if>
                                <c:if test="${promoConfig.updaterId > 0}">
                                    <p>Обновлен <fmt:formatDate value="${promoConfig.timeUpdated}" pattern="dd.MM.yyyy HH:mm"/> пользователем ${promoConfig.updaterId}</p>
                                </c:if>
                            </div>
                        </div>

                        <div class="row ma-b-20">
                            <div class="col-lg-4">
                                <label for="name">Название</label>
                                <form:input type="text" path="name" id="name" class="form-control"/>
                                <form:errors path="name" cssClass="error-message" element="span"/>
                            </div>

                            <div class="col-lg-4">
                                <label for="status">Статус</label>
                                <form:select path="status" id="status" items="${statuses}" cssClass="form-control"/>
                                <form:errors path="status" cssClass="error-message" element="span"/>
                            </div>

                            <div class="col-lg-4">
                                <label for="usageCount">Кол-во использований 1 покупателем (0 = ∞)</label>
                                <form:input type="number" path="usageCount" id="usageCount" class="form-control"/>
                                <form:errors path="usageCount" cssClass="error-message" element="span"/>
                            </div>
                        </div>

                        <div class="row ma-b-20">
                            <div class="col-lg-3">
                                <label for="timeStart">Начало действия</label>
                                <input id="timeStart" name="timeStart" value="${promoConfig.timeStart.time}" type="hidden"/>
                                <input id="date-box-start"
                                       value="<fmt:formatDate value="${promoConfig.timeStart}" pattern="dd.MM.yyyy"/>"
                                       type="text"
                                       class="form-control"
                                       placeholder="Начало действия"/>
                                <form:errors path="timeStart" cssClass="error-message" element="span"/>
                            </div>

                            <div class="col-lg-3">
                                <label for="timeFinish">Конец действия</label>
                                <input id="timeFinish" name="timeFinish" value="${promoConfig.timeFinish.time}" type="hidden"/>
                                <input id="date-box-finish"
                                       value="<fmt:formatDate value="${promoConfig.timeFinish}" pattern="dd.MM.yyyy"/>"
                                       type="text"
                                       class="form-control"
                                       placeholder="Конец действия"/>
                                <form:errors path="timeFinish" cssClass="error-message" element="span"/>
                            </div>

                            <div class="col-lg-3">
                                <label for="priceCondition">Минимальная покупка</label>
                                <form:input type="number" path="priceCondition" id="priceCondition" class="form-control"/>
                                <form:errors path="priceCondition" cssClass="error-message" element="span"/>
                            </div>

                            <div class="col-lg-3">
                                <label for="mailTemplate">Шаблон письма</label>
                                <form:select path="mailTemplate" id="mailTemplate" cssClass="form-control"
                                             items="${mailTemplates}" />
                                <form:errors path="mailTemplate" cssClass="error-message" element="span"/>
                            </div>
                        </div>

                        <div class="row ma-b-20">
                            <div class="col-lg-12">
                                <label for="campaignTags">Категории проектов</label>
                                <form:select path="campaignTags" id="campaignTags" cssClass="form-control"
                                             items="${tags}" itemValue="id" itemLabel="name" style="height: 200px"/>
                                <form:errors path="campaignTags" cssClass="error-message" element="span"/>
                            </div>
                        </div>

                        <div class="row ma-b-20">
                            <div class="col-lg-12">
                                <label for="hasPromocode">Есть промокоды</label>
                                <form:checkbox path="hasPromocode" id="hasPromocode" />
                                <form:errors path="hasPromocode" cssClass="error-message" element="span"/>
                            </div>
                        </div>

                        <div class="row ma-b-20" id="promoCodeBlock">
                            <div class="col-lg-12">
                                <label for="promoCodesString">Добавить промокоды (в столбик, без запятых):</label>
                                <form:textarea path="promoCodesString" id="promoCodesString" cssClass="form-control" />
                                <form:errors path="promoCodesString" cssClass="error-message" element="span"/>
                            </div>
                        </div>

                        <div class="ma-b-20">
                            <div class="btn-group">
                                <button type="submit" formaction="/admin/promo-config.html" class="btn btn-primary">
                                    <c:if test="${promoConfig.configId == 0}">Добавить</c:if>
                                    <c:if test="${promoConfig.configId != 0}">Cохранить</c:if>
                                </button>
                                <a href="/admin/promo-config-list.html" class="btn btn-default">Отмена</a>
                            </div>
                        </div>

                        <c:if test="${promoConfig.hasPromocode && not empty promoConfig.promoCodesList}">
                            <div class="row ma-b-20">
                                <div class="col-lg-12">
                                    <label>Использовано: ${codesUsed} из ${fn:length(promoConfig.promoCodesList)} промокодов.</label>
                                    <br>
                                    <label>Существующие промо-коды:</label>
                                    <c:forEach items="${promoConfig.promoCodesList}" var="item">
                                        ${item},
                                    </c:forEach>
                                </div>
                            </div>
                        </c:if>
                    </form:form>
                </div>
            </div>
        </div>
    </div>
</div>


</body>
</html>


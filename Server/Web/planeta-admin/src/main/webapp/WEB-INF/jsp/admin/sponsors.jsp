<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ include file="/WEB-INF/jsp/includes/generated/doctype-header.jsp" %>
<head>
    <%@include file="head.jsp" %>
</head>
<body>

<%@ include file="navbar.jsp" %>

<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Спонсоры</h1>
        </div>
    </div>

    <div class="main-page-actions">
        <a class="btn btn-success btn-circle btn-outline btn-lg" href="/admin/edit-sponsor.html" title="Добавить">
            <i class="fa fa-plus"></i>
        </a>
    </div>

    <div class="row">
        <div class="col-lg-12 admin-table">
            <table class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th>Алиас</th>
                        <th>Множитель</th>
                        <th>Действие</th>
                    </tr>
                </thead>

                <tbody>
                    <c:forEach items="${sponsors}" var="s" varStatus="st">
                        <tr>
                            <td width="1px">
                                <c:out value="${s.alias}"/>
                            </td>
                            <td>
                                ${s.multiplier}
                            </td>
                            <td class="text-right">
                                <div class="btn-group">
                                    <a href="/admin/edit-sponsor.html?alias=${s.alias}" class="btn btn-primary btn-outline" title="Редактировать">
                                        <i class="fa fa-pencil"></i>
                                    </a>
                                    <a href="/admin/delete-sponsor.html?alias=${s.alias}" class="btn btn-outline btn-danger promo-remove" title="Удалить">
                                        <i class="fa fa-trash"></i>
                                    </a>
                                </div>
                            </td>
                        </tr>
                    </c:forEach>
                </tbody>
            </table>
        </div>
    </div>
</body>




<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ include file="/WEB-INF/jsp/includes/generated/doctype-header.jsp" %>

<head>
    <%@include file="head.jsp" %>
    <link type="text/css" rel="stylesheet" href="/css/ui-lightness/jquery-ui-1.10.2.custom.min.css"/>
    <link type="text/css" rel="stylesheet" href="/css/jquery.timepicker.css"/>

    <script>
        $(document).ready(function () {
            $("#date-box-time-start").datetimepicker();

            <c:if test="${not empty campaign.timeStart.time}">
                $("#date-box-time-start").data("DateTimePicker").date(new Date(${campaign.timeStart.time}));
            </c:if>

            $(".js-action-btn-approved").click(function (e) {
                e.preventDefault();
                if (Modal) {
                    var self = e.currentTarget;
                    Modal.showConfirm("Внимание! Для того, чтобы проект был запущен автоматически укажите дату запуска!", "Утвердить проект?", {
                        success: function () {
                            var data = {
                                campaignId: $('input[name="campaignId"]').val(),
                                campaignStatus: $('.js-action-btn-approved').attr('value')
                            };

                            post("/moderator/campaign-moderation.html", data);
                        }
                    });
                }
            });

            $("#time-start-form").submit(function() {
                var dateTimeStart= $("#date-box-time-start").data("DateTimePicker").date();
                $("#timeStart").val(dateTimeStart != null ? dateTimeStart.valueOf() : null);
                return true;
            });
        });
    </script>
</head>
<body>
    <%@ include file="navbar.jsp" %>
    <div id="page-wrapper">

        <div class="row ">
            <div class="col-lg-12">
                <%@include file="successMessage.jsp"%>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Проект № ${campaign.campaignId}</h1>
            </div>
        </div>

        <div class="main-page-actions">
            <a class="btn btn-primary btn-circle btn-outline btn-lg" href="/moderator/campaign-info.html?campaignId=${campaign.campaignId}" title="Редактирование проекта">
                <i class="fa fa-pencil"></i>
            </a>

            <a class="btn btn-info btn-circle btn-outline btn-lg"
               href="${mainAppUrl}/campaigns/${campaign.campaignId}"
               target="_blank"
               title="Страница проекта на Planeta.ru">
                <i class="fa fa-file-o"></i>
            </a>

            <a class="btn btn-info btn-circle btn-outline btn-lg"
               href="${mainAppUrl}/api/private/campaign/contract.html?campaignId=${campaign.campaignId}"
               title="Договор онлайн">
                <i class="fa fa-internet-explorer"></i>
            </a>

            <a class="btn btn-danger btn-circle btn-outline btn-lg"
               href="${mainAppUrl}/welcome/campaign/contract-pdf.html?campaignId=${campaign.campaignId}"
               title="Договор PDF">
                <i class="fa fa-file-pdf-o"></i>
            </a>

            <a class="btn btn-success btn-circle btn-outline btn-lg"
               href="/admin/renew-campaign-videos.html?campaignId=${campaign.campaignId}"
               title="Обновить картинки с youtube">
                <i class="fa fa-youtube"></i>
            </a>

        </div>

        <div class="row ma-b-30">
            <div class="col-lg-2">
                <img src="${hf:getGroupAvatarUrl(campaign.imageUrl, "USER_AVATAR" ) }" alt="${groupProfile.displayName}" class="img-polaroid">
            </div>

            <div class="col-lg-5">
                <div><b>Название: </b><c:out value="${campaign.name}"/></div>

                <div><b>Краткое описание: </b><c:out value="${campaign.shortDescription}"/></div>
                <br/>
                <div>
                    <b>Текущий статус: </b>
                    <c:set var="status" value="${campaign.status}"/>
                    <%@ include file="campaign-statuses.jsp" %><br>
                    <c:if test="${campaign.status == 'APPROVED' and campaign.timeStart != null}">
                        <div style="text-align: center; background-color: #5cb85c!important; color: white; margin-top: 10px">[автозапуск проекта будет произведен <fmt:formatDate value="${campaign.timeStart}" pattern="dd.MM.yyyy в HH:mm"/>]
                        </div>
                    </c:if>
                    <c:if test="${campaign.status == 'APPROVED' and campaign.timeStart == null}">
                        <div style="text-align: center; background-color: #d9534f!important; color: white; margin-top: 10px">[дата запуска проекта не установлена]</div>
                    </c:if>
                </div>

                <c:if test="${campaign.status == 'APPROVED'}">
                    <br>
                    <div>
                        <form:form method='POST' id="time-start-form" commandName="campaign" class="form-horizontal">
                            <form:input id="timeStart" type="hidden" path="timeStart" cssClass="input-medium"/>
                            <label>Дата запуска: </label>
                            <div class="input-group">
                                <input id="date-box-time-start" class="form-control" value='' type="text"/>
                                <span class="input-group-btn">
                                    <button type="submit" class="btn btn-primary">
                                        Сохранить
                                    </button>
                                </span>
                            </div>
                            <form:errors path="timeStart"><span class="help-inline"><form:errors path="timeStart"/></span></form:errors>
                        </form:form>
                    </div>
                </c:if>
            </div>

            <div class="col-lg-5">
                <div class="panel panel-primary">
                    <div class="panel-heading">Контрагент</div>
                    <div class="panel-body">
                        <form:form action="/moderator/campaign-contractor-select.html" class="form-horizontal">
                            <input type="hidden" name="campaignId" value="${campaign.campaignId}" />
                            <c:set var="c" value="${campaign.status.code}"/>
                            <div>
                                <c:if test="${currentContractor != null}">${hf:escapeHtml4(currentContractor.name)}</c:if>
                                <c:if test="${currentContractor == null}">Не указан</c:if>
                            </div>
                        </form:form>

                        <c:if test="${campaign != null}">
                            <div class="btn-group pull-right">
                                <c:if test="${currentContractor != null}">
                                    <a class="btn btn-outline btn-primary" href="/moderator/edit-contractor.html?campaignId=${campaign.campaignId}&contractorId=${currentContractor.contractorId}" title="Редактировать текущего контрагента">
                                        <i class="fa fa-pencil"></i>
                                    </a>
                                </c:if>
                                <c:if test="${campaign.statusCode != 3 && campaign.statusCode != 4 && campaign.statusCode != 5 && campaign.statusCode != 10}">
                                    <a class="btn btn-outline btn-info" href="/moderator/contractors.html?campaignId=${campaign.campaignId}&campaignStatus=${campaign.status}" title="Выбрать существующего контрагента из списка">
                                        <i class="fa fa-list"></i>
                                    </a>
                                    <a class="btn btn-outline btn-success" href="/moderator/edit-contractor.html?campaignId=${campaign.campaignId}&campaignStatus=${campaign.status}" title="Добавить нового контрагента и прикрепить к текущему проекту">
                                        <i class="fa fa-plus"></i>
                                    </a>
                                </c:if>
                            </div>
                        </c:if>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">Менеджер проекта</div>
                    <div class="panel-body">
                        <form:form action="/moderator/campaign-manager-select.html" class="form-horizontal">
                            <div class="row ma-b-20">
                                <div class="col-lg-12">
                                    <label for="manager-id">Менеджер</label>
                                    <input type="hidden" name="campaignId" value="${campaign.campaignId}" />
                                    <select id="manager-id" name="managerId" class="form-control">
                                        <c:forEach var="manager" items="${managersList}">
                                            <option value="${manager.managerId}" <c:if test="${currentManager.managerId == manager.managerId}"> selected</c:if>>
                                                <c:out value="${manager.fullName}" />
                                            </option>
                                        </c:forEach>
                                    </select>
                                </div>
                            </div>

                            <div class="row ma-b-20">
                                <div class="col-lg-12">
                                    <label for="comment">Комментарий</label>
                                    <textarea id="comment" rows="6" class="form-control" name="comment"></textarea>
                                </div>
                            </div>

                            <button type="submit" class="btn btn-primary">Сохранить менеджера</button>
                        </form:form>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">Администраторы проекта</div>
                    <div class="panel-body">
                        <fieldset>
                            <a href="${mainAppUrl}/${creator.profileId}">${creator.displayName}</a> (создатель)

                            <c:forEach var="admin" items="${campaignAdmins}">
                                <form method="post" action="/moderator/remove-campaign-admin.html">
                                    <button class="btn btn-danger btn-outline btn-xs" type="submit" title="Убрать">
                                        <i class="fa fa-trash"></i>
                                    </button>

                                    <input type="hidden" name="campaignId" value="${campaign.campaignId}">
                                    <input type="hidden" name="adminId" value="${admin.profileId}">
                                    <a href="${mainAppUrl}/${admin.profileId}">${admin.displayName}</a>
                                </form>
                            </c:forEach>

                            <br>

                            <form method="post" action="/moderator/add-campaign-admin.html">
                                <input type="hidden" name="campaignId" value="${campaign.campaignId}">

                                <div class="form-group input-group">
                                    <input type="number" name="adminId" placeholder="Введите id профиля" class="form-control">
                                    <span class="input-group-btn">
                                        <button class="btn btn-success" type="submit">Добавить</button>
                                    </span>
                                </div>
                            </form>
                        </fieldset>
                    </div>
                </div>
            </div>
        </div>

        <div class="row ma-b-20">
            <div class="col-lg-12">
                <form method="POST" action="/moderator/campaign-set-editable.html" class="form-horizontal">
                    <input type="hidden" name="campaignId" value="${campaign.campaignId}">

                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="editable"<c:if test="${isEditable}"> checked="checked"</c:if> onclick="this.form.submit();">
                            Можно ли автору менять проект после запуска
                        </label>
                    </div>
                </form>
            </div>
        </div>

        <div class="row ">
            <div class="col-lg-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">Модерация</div>

                    <div class="panel-body">
                        <form method="POST" action="/moderator/campaign-moderation.html" class="form-horizontal">
                            <fieldset>
                                <input type="hidden" name="campaignId" value="${campaign.campaignId}">

                                <label><b>Ответ модератора</b></label>

                                <div class="form-actions">
                                    <c:if test="${hf:isNextActionVisible(campaignStatus, 'APPROVED')}">
                                        <button type="submit" class="btn btn-success js-action-btn-approved" value="APPROVED" name="campaignStatus">Утвердить</button>
                                    </c:if>

                                    <c:if test="${hf:isNextActionVisible(campaignStatus, 'ACTIVE')}">
                                        <button type="submit" class="btn btn-info" value="ACTIVE" name="campaignStatus">Запустить</button>
                                    </c:if>

                                    <c:if test="${hf:isNextActionVisible(campaignStatus, 'PAUSED')}">
                                        <button type="submit" class="btn btn-primary js-action-btn" data-action="pause" value="PAUSED" name="campaignStatus">Пауза</button>
                                    </c:if>

                                    <c:if test="${hf:isNextActionVisible(campaignStatus, 'PATCH')}">
                                        <button class="btn btn-warning js-action-btn" data-action="patch">На доработку</button>
                                    </c:if>

                                    <c:if test="${hf:isNextActionVisible(campaignStatus, 'DECLINED')}">
                                        <button class="btn btn-danger js-declined-btn">Отклонить</button>
                                    </c:if>

                                    <c:if test="${hf:isNextActionVisible(campaignStatus, 'FINISHED')}">
                                        <button type="submit" class="btn btn-primary" value="FINISHED" name="campaignStatus">Остановить</button>
                                    </c:if>

                                    <c:if test="${isSuperAdmin}">
                                        <button type="submit" class="btn btn-danger pull-right" value="DELETED" name="campaignStatus">Удалить</button>
                                    </c:if>
                                </div>
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-4">
                <div class="panel panel-primary">
                    <div class="panel-heading">Аннулирование всех покупок</div>

                    <div class="panel-body">
                        <form:form action="/admin/cancel-all-purchases.html" class="form-horizontal" id="cancel-purchase-form">
                            <input type="hidden" name="campaignId" value="${campaign.campaignId}" />
                            <textarea class="form-control" name="reason" rows="5" placeholder="Введите причину аннулирования"></textarea>

                            <br>

                            <button type="submit" class="btn btn-danger">Аннулировать</button>
                        </form:form>
                    </div>
                </div>
            </div>

            <div class="col-lg-4">
                <div class="panel panel-primary">
                    <div class="panel-heading">Быстрое создание баннера для проекта</div>

                    <div class="panel-body">
                        <form:form action="/moderator/campaign-quick-banner.html" class="form-horizontal">
                            <input type="hidden" name="campaignId" value="${campaign.campaignId}" />
                            <textarea class="form-control" name="html" rows="5" placeholder="Напишите сюда html код баннера"></textarea>

                            <br>

                            <button type="submit" class="btn btn-primary">Сохранить баннер</button>
                        </form:form>
                    </div>
                </div>
            </div>

            <div class="col-lg-4">
                <div class="panel panel-primary">
                    <div class="panel-heading">Оставить модераторское сообщение</div>

                    <div class="panel-body">
                        <form action="/moderator/save-internal-message.html" method="post" class="form-horizontal">
                            <input type="hidden" name="campaignId" value="${campaign.campaignId}" />
                            <input type="hidden" name="campaignStatus" value="${campaign.status}" />
                            <textarea class="form-control" name="message" rows="5" placeholder="Введите текст сообщения"></textarea>

                            <br>

                            <button type="submit" class="btn btn-primary">Сохранить</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <script type="text/javascript">
            $("#cancel-purchase-form .btn").click(function (e) {
                e.preventDefault();
                if (Modal) {
                    Modal.showConfirm("Внимание! Все покупки будут аннулированы! Продолжить?", "Подтверждение действия", {
                        success: function () {
                            $('#cancel-purchase-form').submit();
                        }
                    });
                }
            });
        </script>

        <h2>Сообщения</h2>

        <div class="row">
            <div class="col-lg-12 admin-table">
                <table class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th class="span5">Сообщение</th>
                            <th class="span1">Дата</th>
                            <th class="span1">Отправитель</th>
                            <th class="span1">Тип собщения</th>
                            <th class="span1">Статус проекта</th>
                        </tr>
                    </thead>

                    <tbody>
                        <c:forEach var="moderationMessage" items="${moderationMessageList}">
                            <tr>
                                <td>${moderationMessage.message}</td>
                                <td>
                                    <fmt:formatDate value="${moderationMessage.timeAdded}" pattern="dd.MM.yyyy HH:mm"/>
                                </td>
                                <td>
                                    <a href="${mainAppUrl}/${moderationMessage.senderId}">${moderationMessage.displayName}</a>
                                </td>
                                <td>
                                    <c:choose>
                                        <c:when test="${moderationMessage.status == \"MODERATOR_MESSAGE\"}">
                                            Публичное
                                        </c:when>
                                        <c:when test="${moderationMessage.status == \"AUTHOR_MESSAGE\"}">
                                            Сообщение автора
                                        </c:when>
                                        <c:when test="${moderationMessage.status == \"INTERNAL_MESSAGE\"}">
                                            Внутреннее
                                        </c:when>
                                        <c:otherwise>
                                            Другое
                                        </c:otherwise>
                                    </c:choose>
                                </td>
                                <td>
                                    <c:set var="status" value="${moderationMessage.campaignStatus}"/>
                                    <%@ include file="campaign-statuses.jsp" %><br>
                                </td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
            </div>
        </div>
    </div>


<div id="decline-confirmation" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="confirm-title" aria-hidden="true">
    <div class="modal-dialog modal-lg" style="height: auto">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h3>Отклонить проект</h3>
            </div>

            <div class="modal-body">
                <form>
                    <p>Выберите причину отказа в запуске проекта</p>
                    <select name="message-select" multiple="multiple" class="form-control">
                        <option value="specified">Проект преследует личную цель и не имеет прямой связи с общественно-полезной деятельностью</option>
                        <option value="specified">Проект ориентирован исключительно на привлечение прибыли (коммерческая цель) и не имеет прямой связи с творческой или общественно-полезной деятельностью</option>
                        <option value="specified">Цель проекта не связана с творческой или общественно-полезной деятельностью</option>
                        <option value="specified">Проект противоречит морально-этическим нормам общества</option>
                        <option value="specified">Проект связан с политической деятельностью</option>
                        <option value="specified">Проект связан с религиозной деятельностью</option>
                        <option value="specified">Проект противоречит законодательству Российской Федерации</option>
                        <option value="specified">Проект не соответствует целям и задачам сервиса, определенным в Правилах</option>
                        <option value="specified">Проект размещен на аналогичном ресурсе</option>
                        <option value="custom">Свой вариант</option>
                    </select>
                    <br>
                    <textarea name="message" class="form-control" rows="10" placeholder="Введите причину отказа"></textarea>
                    <input type="hidden" name="campaignId" value="${campaign.campaignId}">
                    <input type="hidden" name="campaignStatus" value="DECLINED">
                </form>
            </div>
            <div class="modal-footer">
                <button class="btn" data-dismiss="modal">Отмена</button>
                <button class="btn btn-danger confirm">Отклонить</button>
            </div>
            <script>
                function post(path, params, method) {
                    method = method || "post"; // Set method to post by default if not specified.

                    // The rest of this code assumes you are not using a library.
                    // It can be made less wordy if you use one.
                    var form = document.createElement("form");
                    form.setAttribute("method", method);
                    form.setAttribute("action", path);

                    for(var key in params) {
                        if(params.hasOwnProperty(key)) {
                            var hiddenField = document.createElement("input");
                            hiddenField.setAttribute("type", "hidden");
                            hiddenField.setAttribute("name", key);
                            hiddenField.setAttribute("value", params[key]);

                            form.appendChild(hiddenField);
                        }
                    }

                    document.body.appendChild(form);
                    form.submit();
                }

                $('.js-declined-btn').on('click', function(e) {
                    e.preventDefault();
                    var dialog = $('#decline-confirmation').modal('show'),
                            $select = dialog.find('selectCampaignById[name="message-selectCampaignById"]'),
                            $textarea = dialog.find('textarea[name="message"]'),
                            $confirm = dialog.find('.confirm');
                    $confirm.off();
                    $select.on('change', function(e) {
                        var $options = $select.find('option:selected');
                        $textarea.text('');
                        $options.each(function(){
                            var $option = $(this);
                            if ($option.val() == 'custom'){
        //                        $textarea.text('');
                                $textarea.prop('disabled', false);
                            } else {
                                $textarea.prop('disabled', true);
                                $textarea.text($textarea.text() + $option.text() + ". \n");
                            }
                        });
                    });
                    $select.trigger('change');
                    $confirm.click(function(e) {
                        var $option = $select.find('option:selected');
                        if ($option.length == 1 && $option.val() != 'custom'){
                            $textarea.text($option.text());
                        }
                        var data = {
                            message: $textarea.val(),
                            campaignId: dialog.find('input[name="campaignId"]').val(),
                            campaignStatus: dialog.find('input[name="campaignStatus"]').val()
                        };
                        var confirm = $(e.currentTarget)
                                .off('click')
                                .addClass('disabled');

                        post("/moderator/campaign-moderation.html", data);
                    });
                });
            </script>
        </div>
    </div>
</div>

<script>
    $('.js-action-btn').on('click', function(e) {
        var action = $(this).data('action');
        e.preventDefault();
        var dialog = $('#' + action + '-confirmation').modal('show');
        var $textarea = dialog.find('textarea[name="message"]');
        var $confirm = dialog.find('.confirm');
        dialog.on('shown', function () {
            $textarea.focus();
        });
        $confirm.off();
        $confirm.click(function(e) {
            $confirm.off();
            var data = {
                message: $textarea.val(),
                campaignId: dialog.find('input[name="campaignId"]').val(),
                campaignStatus: dialog.find('input[name="campaignStatus"]').val()
            };
            var confirm = $(e.currentTarget)
                    .off('click')
                    .addClass('disabled');

            post("/moderator/campaign-moderation.html", data);

        });
    });
</script>

<div id="patch-confirmation" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="confirm-title" aria-hidden="true">
    <div class="modal-dialog modal-lg" style="height: auto">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h3>На доработку</h3>
            </div>
            <div class="modal-body">
                <form>
                    <textarea class="form-control" name="message" rows="3"
                                       placeholder="Напишите сюда сообщение для пользователя"></textarea>
                    <input type="hidden" name="campaignId" value="${campaign.campaignId}">
                    <input type="hidden" name="campaignStatus" value="PATCH">
                </form>
            </div>
            <div class="modal-footer">
                <button class="btn" data-dismiss="modal">Отмена</button>
                <button class="btn btn-warning confirm">Ок</button>
            </div>
        </div>
    </div>
</div>

<div id="pause-confirmation" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="confirm-title" aria-hidden="true">
    <div class="modal-dialog modal-lg" style="height: auto">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h3>На паузу</h3>
            </div>

            <div class="modal-body">
                <form>
                    <textarea class="form-control" name="message" rows="3"
                              placeholder="Напишите сюда причину остановки"></textarea>
                    <input type="hidden" name="campaignId" value="${campaign.campaignId}">
                    <input type="hidden" name="campaignStatus" value="PAUSED">
                </form>
            </div>

            <div class="modal-footer">
                <button class="btn" data-dismiss="modal">Отмена</button>
                <button class="btn btn-warning confirm">Ок</button>
            </div>
        </div>
    </div>
</div>

<div id="payment-done-confirmation" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="confirm-title"
     aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3>Произвести выплату</h3>
    </div>
    <div class="modal-body">
        <p>После произведения выплаты дальнейшая работа с проектом невозможна.
           Вы уверены, что хотите произвести выплату?</p>
        <form>
            <input type="number" class="span5" name="message"
                      placeholder="Введите сумму выплат">
            <input type="hidden" name="campaignId" value="${campaign.campaignId}">
            <input type="hidden" name="campaignStatus" value="PAYMENT_DONE">
            <div class="js-error-message"></div>
        </form>
    </div>
    <div class="modal-footer">
        <button class="btn" data-dismiss="modal">Отмена</button>
        <button class="btn btn-inverse mrg-l-10 pull-right js-payment-done">Ок</button>
    </div>
    <script>
        $('.js-payment-done').on('click', function(e) {
            e.preventDefault();
            var dialog = $('#payment-done-confirmation').modal('show'),
                    $message = dialog.find('input[name="message"]'),
                    $confirm = dialog.find('.js-payment-done');
            $confirm.off();
            $confirm.click(function(e) {
                e.preventDefault();
                if(!/^(\d+|\s)+$/.test($message.val())) {
                    if (dialog.find('.js-error-message').text().length == 0) {
                        dialog.find('.js-error-message').append('<p>Внимание! Вы не ввели сумму выплаты в поле для ответа модератора (только цифры).</p>');
                    }
                    return false;
                }
                var data = {
                    message: $message.val(),
                    campaignId: dialog.find('input[name="campaignId"]').val(),
                    campaignStatus: dialog.find('input[name="campaignStatus"]').val()
                };
                var confirm = $(e.currentTarget)
                        .off('click')
                        .addClass('disabled');

                post("/moderator/campaign-moderation.html", data);
            });
        });
    </script>
</div>
</body>
</html>

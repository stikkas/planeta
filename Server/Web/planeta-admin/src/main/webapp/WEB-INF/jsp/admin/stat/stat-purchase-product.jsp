<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ include file="/WEB-INF/jsp/includes/taglibs.jsp" %>
<div id="info">
    <table class="table table-bordered table-striped">

        <thead>
            <th>Сообщество</th><th>Товар, Цена</th><th>Количество</th><th>Сумма</th>
        </thead>

        <tbody>
        <c:forEach var="purchase" items="${purchaseReport}" varStatus="status">
            <tr>
                <td><a href="/moderator/group-info.html?profileId=${purchase.groupProfile.profileId}">${purchase.groupProfile.displayName}</a></td>
                <td><a href="https://${properties["shop.application.host"]}/products/${purchase.product.productId}">${purchase.product.name}</a> <br> ${purchase.price}</td>
                <td>
                    ${purchase.count}
                    <c:choose>
                        <c:when test="${purchase.countDiff < 0}">
                            <span class="negative tooltip"
                                  data-tooltip="Изменение значения по сравнению с предыдущим интервалом">
                                (${purchase.countDiff})</span>
                        </c:when>
                        <c:when test="${purchase.countDiff > 0}">
                            <span class="positive tooltip"
                                  data-tooltip="Изменение значения по сравнению с предыдущим интервалом">
                                (+${purchase.countDiff})</span>
                        </c:when>
                        <c:otherwise>
                            (-)
                        </c:otherwise>
                    </c:choose>
                </td>
                <td>
                    ${purchase.amount}
                    <c:choose>
                        <c:when test="${purchase.amountDiff < 0}">
                            <span class="negative tooltip"
                                  data-tooltip="Изменение значения по сравнению с предыдущим интервалом">
                                (${purchase.amountDiff})</span>
                        </c:when>
                        <c:when test="${purchase.amountDiff > 0}">
                            <span class="positive tooltip"
                                  data-tooltip="Изменение значения по сравнению с предыдущим интервалом">
                                (+${purchase.amountDiff})</span>
                        </c:when>
                        <c:otherwise>
                            (-)
                        </c:otherwise>
                    </c:choose>
                </td>

            </tr>
        </c:forEach>
        <tr>
            <th>Итого</th>
            <th></th>
            <th>${totalCount}</th>
            <th>${totalAmount}</th>
        </tr>
        </tbody>
    </table>
</div>

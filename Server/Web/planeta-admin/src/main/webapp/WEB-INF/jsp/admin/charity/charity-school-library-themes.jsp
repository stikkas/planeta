<%@ taglib prefix="s" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ include file="/WEB-INF/jsp/includes/generated/doctype-header.jsp" %>
<head>
    <%@include file="../head.jsp" %>
</head>
<body>

<%@ include file="/WEB-INF/jsp/admin/navbar.jsp" %>

<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Библиотека файлов (темы / заголовки)</h1>
        </div>
    </div>

    <div class="main-page-actions">
        <a class="btn btn-success btn-circle btn-outline btn-lg" href="/admin/charity/school-library-theme-edit.html?themeId=0" title="Добавить">
            <i class="fa fa-plus"></i>
        </a>
    </div>

    <div class="row">
        <div class="col-lg-12 admin-table">
            <table class="table table-bordered table-striped">
                <thead>
                <th>ID</th>
                <th>Заголовок</th>
                <th>Действие</th>
                </thead>
                <tbody>
                <c:forEach items="${libraryFileThemes}" var="libraryFileTheme" varStatus="st">
                    <tr>
                        <td width="1px">
                            ${libraryFileTheme.themeId}
                        </td>
                        <td>
                            ${libraryFileTheme.themeHeader}
                        </td>
                        <td >
                            <a href="/admin/charity/school-library-theme-edit.html?themeId=${libraryFileTheme.themeId}" class="btn"><i class="icon-pencil"></i></a>
                        </td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>
    </div>
</body>



<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ include file="/WEB-INF/jsp/includes/generated/doctype-header.jsp" %>
<head>
    <%@ include file="head.jsp" %>
</head>
<body>

<%@ include file="/WEB-INF/jsp/admin/navbar.jsp" %>

<div id="page-wrapper">

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Статистика по вознаграждениям</h1>
        </div>
    </div>

    <div class="panel panel-primary">
        <div class="panel-heading">Поиск</div>

        <div class="panel-body">
            <form id="stat-params" method="GET">
                <div class="row ma-b-20">
                    <div class="col-lg-12">
                        <ct:date-range-picker dateFrom="${dateFrom}" dateTo="${dateTo}" allButton="true"/>
                    </div>
                </div>

                <button class="btn btn-success" formaction="sales-shares-report.html">
                    <i class="fa fa-download"></i> Скачать в формате Excel
                </button>
            </form>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <span>Количество активных проектов за выбранный интервал ${activeCampaignsCount}
                <c:choose>
                    <c:when test="${activeCampaignsCountDiff < 0}">
                    <span data-toggle="tooltip" data-placement="top" class="negative"
                          data-tooltip="Изменение значения по сравнению с предыдущим интервалом">
                        (${activeCampaignsCountDiff})</span>
                    </c:when>
                    <c:when test="${purchase.amountDiff > 0}">
                    <span data-toggle="tooltip" data-placement="top" class="positive"
                          data-tooltip="Изменение значения по сравнению с предыдущим интервалом">
                        (+${activeCampaignsCountDiff})</span>
                    </c:when>
                    <c:otherwise>
                        (-)
                    </c:otherwise>
                </c:choose>
            </span>
        </div>
    </div>


    <div class="row">
        <div class="col-lg-12">
            <%@include file="/WEB-INF/jsp/admin/stat/stat-purchase-share.jsp" %>
        </div>
    </div>
</div>
</body>
</html>


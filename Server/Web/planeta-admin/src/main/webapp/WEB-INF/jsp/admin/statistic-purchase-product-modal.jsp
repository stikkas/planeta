<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ include file="/WEB-INF/jsp/includes/generated/doctype-header.jsp" %>
<%@include file="/WEB-INF/jsp/includes/generated/taglibs.jsp" %>

    <head>
        <%@ include file="statistic-head.jsp" %>
    </head>

    <body>
        <div id="center-container" class="span18">
            <h2>Статистика по товарам
                <c:if test="${not empty dateFrom && not empty dateTo && dateTo > dateFrom}">
                    за <fmt:formatDate value="${dateFrom}" pattern="dd.MM.yyyy"/> - <fmt:formatDate value="${dateTo}" pattern="dd.MM.yyyy"/>
                </c:if>
                <c:if test="${not empty dateFrom}">
                    за <fmt:formatDate value="${dateFrom}" pattern="dd.MM.yyyy"/>
                </c:if>
            </h2>
            <%@include file="/WEB-INF/jsp/admin/stat/stat-purchase-product.jsp"%>
            <form id="stat-params" method="GET">
                <div class="pull-left">
                    <input name="dateFrom" type="hidden" value="${dateFrom.time}"/>
                    <input name="dateTo" type="hidden" value="${dateTo.time}"/>
                    <button class="btn btn-primary" formaction="sales-products-report.html">В Excel</button>
                </div>
            </form>
        </div>
	</body>
</html>


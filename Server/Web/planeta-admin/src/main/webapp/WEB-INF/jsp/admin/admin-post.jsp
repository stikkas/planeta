<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ include file="/WEB-INF/jsp/includes/generated/doctype-header.jsp" %>

<head>
    <%@ include file="/WEB-INF/jsp/admin/head.jsp" %>
    <title>${pageTitle}</title>

    <link type="text/css" rel="stylesheet" href="/css/ui-lightness/jquery-ui-1.10.2.custom.min.css"/>
    <link type="text/css" rel="stylesheet" href="/css/jquery.timepicker.css"/>
    <link type="text/css" rel="stylesheet" href="//${hf:getStaticBaseUrl("")}/css/jquery.Jcrop.css">
    <link type="text/css" rel="stylesheet" href="//${hf:getStaticBaseUrl("")}/css/admin.Crop.css">

    <style>
        .control-label>span{
            display: none;
        }

        .control-label>span.required{
            display: inline;
            color: red;
        }

        .my-error{
            margin-left: 8px;
        }
    </style>

    <script type="text/javascript">
        $(document).ready(function () {
            $("#date-box").datetimepicker();
            <c:if test="${not empty post.eventDate.time}">
            $("#date-box").data("DateTimePicker").date(new Date(${post.eventDate.time}));
            </c:if>

            $("#time-added-box").datetimepicker();
            <c:if test="${not empty post.timeAdded.time}">
            $("#time-added-box").data("DateTimePicker").date(new Date(${post.timeAdded.time}));
            </c:if>

            $("#charity-post-form").submit(function() {
                var eventDate = $("#date-box").data("DateTimePicker").date().format('x');
                if (eventDate) {
                    $("#eventDate").val(eventDate);
                }

                var timeAdded = $("#time-added-box").data("DateTimePicker").date().format('x');
                if (timeAdded) {
                    $("#timeAdded").val(timeAdded);
                }

                return true;
            });

            $(document).on('click', ".js-btn-crop", function () {
                var photo = $("#imageImg");
                var photoUrl = photo.attr('data-url');
                var photoId = photo.attr('data-image-id');


                var viewType = CrowdFund.Views.ImageField.ModalCropView.extend({
                    onSuccess: function(model) {
                        $('#imageId').val(model.get('objectId'));
                        $('#imageUrl').val(model.get('imageUrl'));
                        $('#imageImg').attr('src', model.get('imageUrl'));
                        $('#imageImg').attr('data-url', model.get('imageUrl'));
                        $('#imageImg').attr('data-image-id', model.get('objectId'));
                    }
                });


                var model = new CrowdFund.Models.ImageField({
                    originalImage: {
                        imageUrl: photoUrl,
                        photoId: photoId
                    },
                    profileId: ${myProfile.profile.profileId},
                    albumTypeId: AlbumTypes.ALBUM_HIDDEN,
                    thumbnail: {
                        imageConfig: ImageUtils.ORIGINAL,
                        imageType: ImageType.PHOTO
                    },
                    aspectRatio: 580 / 276,
                    title: '',
                    description: ''
                });

                var view = new viewType({
                    model: model
                });
                view.render();
            });


            if($('#imageImg').attr('data-url')) {
                $('#imageImg').attr('src', $('#imageImg').attr('data-url'));
                $('.js-btn-crop').show();
            } else {
                $('#imageImg').attr('src', workspace.staticNodesService.getResourceUrl("/images/defaults/upload.png"));
                $('.js-btn-crop').hide();
            }

            $('#eventType').change(function(e) {
                var eventType = e.target.value;
                var $tag =  $('#tagId');

                if(eventType == 'NEWS' || eventType == 'PARTNERS_NEWS') {
                    $tag.attr('disabled', false);
                    $tag.val(0);
                } else {
                    $tag.attr('disabled', true);
                }
            });
        });
    </script>
</head>

<body>

<%@ include file="/WEB-INF/jsp/admin/navbar.jsp" %>

<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                <c:if test="${post.id > 0}">
                    Редактировать пост № ${post.id}
                </c:if>
                <c:if test="${post.id == 0}">
                    Новый пост
                </c:if>
            </h1>
        </div>
    </div>

    <c:if test="${post.id == 0}">
        <div class="row ma-b-20">
            <div class="col-lg-12">
                <div class="row">
                    <div class="col-lg-4">
                        <form method="get" class="form-horizontal">
                            <div class="input-group">
                                <input type="text" class="form-control" placeholder="Введите id ранее созданного поста"
                                       id="form-control" name="postId">
                                <span class="input-group-btn">
                                    <button class="btn btn-success"
                                            type="submit"
                                            formaction="/admin/post.html"
                                            title="Загрузить">
                                        <i class="fa fa-download"></i>
                                    </button>
                                </span>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </c:if>

    <div class="row">
        <div class="col-lg-12">
            <form:form
                    id="charity-post-form"
                    commandName="post"
                    class="form-horizontal"
                    method="post"
                    action="/admin/post.html">

                <c:if test="${post.id != 0}">
                    <input type="hidden" name="id" value="${post.id}"/>

                    <div class="row ma-b-20">
                        <div class="col-lg-6">
                            <label>Заголовок</label>
                            <form:textarea path="title" id="title" rows="5" class="form-control"/>
                            <form:errors path="title" cssClass="error"/>
                        </div>

                        <div class="col-lg-6">
                            <label>Краткий текст</label>
                            <form:textarea path="headingText" id="headingText" rows="5" class="form-control"/>
                            <form:errors path="headingText" cssClass="error"/>
                        </div>
                    </div>

                    <div class="row ma-b-20">
                        <div class="col-lg-12">
                            <label>Полный текст</label>
                            <form:textarea path="postText" id="postText" rows="10" class="form-control"/>
                            <form:errors path="postText" cssClass="error"/>
                        </div>
                    </div>

                    <div class="row ma-b-20">
                        <div class="col-lg-4">
                            <label>Раздел</label>
                            <form:select path="eventType" id="eventType" cssClass="form-control">
                                <c:if test="${post.eventType == null || post.eventType == ''}">
                                    <form:option value="" selected="true"
                                                 label="-- Не выбран --" />
                                </c:if>
                                <c:forEach items="${charityEventTypes}" var="eventType" varStatus="st">
                                    <c:if test="${post.eventType == eventType}">
                                        <form:option value="${eventType}" selected="true"
                                                     label="${eventType}" />
                                    </c:if>
                                    <c:if test="${post.eventType != eventType}">
                                        <form:option value="${eventType}"
                                                     label="${eventType}" />
                                    </c:if>
                                </c:forEach>
                            </form:select>
                        </div>

                        <div class="col-lg-4">
                            <label>Дата создания</label>
                            <input id="timeAdded" path="timeAdded" name="timeAdded" type="hidden"/>
                            <input id="time-added-box" class="form-control" readonly type="text" value="<fmt:formatDate pattern="dd-MM-yyyy hh:mm" value="${post.timeAdded}" />"/>
                        </div>

                        <div class="col-lg-4">
                            <label>Дата события</label>
                            <input id="eventDate" path="eventDate" name="eventDate" type="hidden"/>
                            <input id="date-box" class="form-control" type="text" value="<fmt:formatDate pattern="dd-MM-yyyy hh:mm" value="${post.eventDate}" />"/>
                        </div>
                    </div>

                    <div class="row ma-b-20">
                        <div class="col-lg-12">
                            <label>Место проведения</label>
                            <input type="text" name="eventLocation" class="form-control" value="${post.eventLocation}"/>
                        </div>
                    </div>

                    <div class="row ma-b-20">
                        <div class="col-lg-12">
                            <ct:file url="${post.imageUrl}" path="imageUrl" type="IMAGE"/>
                        </div>
                    </div>

                    <div class="row ma-b-20">
                        <div class="col-lg-6">
                            <label>Категория</label>
                            <select id="tagId" class="form-control" name="tagId" <c:if test="${post.eventType != 'NEWS' && post.eventType != 'PARTNERS_NEWS'}">disabled="disabled"</c:if>>
                                <c:if test="${post.tagId == 0}">
                                    <option selected="selected" value="${tag.tagId}">-- Не выбрана --</option>
                                </c:if>
                                <c:forEach items="${charityNewsTags}" var="tag">
                                    <c:if test="${post.tagId == tag.tagId}">
                                        <option selected="selected" value="${tag.tagId}">${tag.nameRus}</option>
                                    </c:if>
                                    <c:if test="${post.tagId != tag.tagId}">
                                        <option value="${tag.tagId}">${tag.nameRus}</option>
                                    </c:if>
                                </c:forEach>
                            </select>
                        </div>

                        <div class="col-lg-6">
                            <label>Вес</label>
                            <form:input path="weight" id="weight" class="form-control"/>
                            <form:errors path="weight" cssClass="error"/>
                            <p class="help-block">Чем больше число тем ближе к началу списка.</p>
                        </div>
                    </div>

                    <div class="btn-group">
                        <c:if test="${post.id != 0}">
                            <button type="submit" class="btn btn-primary">Сохранить</button>
                        </c:if>
                        <c:if test="${post.id == 0}">
                            <button type="submit" class="btn btn-primary">Добавить</button>
                        </c:if>
                        <a href="/admin/charity/posts.html" class="btn btn-default">
                            Назад
                        </a>
                    </div>
                </c:if>
            </form:form>
        </div>
    </div>
</div>
</body>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ include file="/WEB-INF/jsp/includes/generated/doctype-header.jsp" %>

<head>
    <%@ include file="/WEB-INF/jsp/admin/head.jsp" %>
    <title>${pageTitle}</title>

</head>

<body>

<%@ include file="/WEB-INF/jsp/admin/navbar.jsp" %>

<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Посты</h1>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <form id="search-frm" method="GET" action="/admin/posts.html" class="clearfix">
                <input type="hidden" name="offset" value="0"/>
                <input type="hidden" name="limit" value="10"/>

                <div class="form-group input-group">
                    <input type="text" class="form-control" placeholder="Введите ID поста" id="postId" name="postId" value="${param.postId}">
                    <span class="input-group-btn">
                        <button id="search" class="btn btn-primary" type="submit">
                            <i class="fa fa-search"></i>
                        </button>
                    </span>
                </div>
            </form>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12 admin-table">
            <table class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Вес</th>
                        <th>Заголовок</th>
                        <th>Краткий текст</th>
                        <th>Дата события</th>
                        <th>Место события</th>
                        <th>Изображение</th>
                        <th></th>
                    </tr>
                </thead>

                <tbody>
                    <c:forEach var="post" items="${posts}">
                        <tr>
                            <td>${post.id}</td>
                            <td>${post.weight}</td>
                            <td>${post.title}</td>
                            <td>${post.headingText}</td>
                            <td><fmt:formatDate value="${post.eventDate}" pattern="dd MMMM yyyy в hh:mm"/></td>
                            <td>${post.eventLocation}</td>
                            <td>
                                <img src="${post.imageUrl}" style="width: 200px;">
                            </td>
                            <td>
                                <a href="/admin/post.html?postId=${post.id}" class="btn btn-primary btn-outline" title="Редактировать">
                                    <i class="fa fa-pencil"></i>
                                </a>
                            </td>
                        </tr>
                    </c:forEach>
                </tbody>
            </table>
        </div>
    </div>

    <%@ include file="/WEB-INF/jsp/admin/paginator.jsp" %>
</div>
</body>
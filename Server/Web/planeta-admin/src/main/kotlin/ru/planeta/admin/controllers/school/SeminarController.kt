package ru.planeta.admin.controllers.school

import org.apache.commons.lang3.ArrayUtils
import org.apache.commons.lang3.StringUtils
import org.apache.commons.lang3.time.DateUtils
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Controller
import org.springframework.validation.BindingResult
import org.springframework.web.bind.annotation.*
import org.springframework.web.servlet.ModelAndView
import ru.planeta.admin.Actions
import ru.planeta.admin.School
import ru.planeta.admin.controllers.services.AdminBaseControllerService
import ru.planeta.admin.utils.DateUtils.Interval
import ru.planeta.admin.utils.DateUtils.setIntervalTodayAsDate
import ru.planeta.api.exceptions.PermissionException
import ru.planeta.api.model.ActionStatus
import ru.planeta.api.search.SearchService
import ru.planeta.api.service.admin.AdminService
import ru.planeta.api.service.campaign.CampaignService
import ru.planeta.api.service.common.PartnerService
import ru.planeta.api.service.common.SeminarService
import ru.planeta.api.service.geo.GeoService
import ru.planeta.api.service.profile.SeminarRegistrationService
import ru.planeta.api.web.authentication.myProfileId
import ru.planeta.dao.generated.commondb.BaseSpeakerService
import ru.planeta.model.common.PartnerForJson
import ru.planeta.model.common.SpeakerForJson
import ru.planeta.model.common.school.Seminar
import ru.planeta.reports.ReportType
import java.text.SimpleDateFormat
import java.util.*
import javax.servlet.http.HttpServletResponse
import javax.validation.Valid

/**
 * Created with IntelliJ IDEA.
 * Date: 03.10.12
 * Time: 13:32
 */
@Controller
class SeminarController(private val seminarService: SeminarService,
                        private val seminarRegistrationService: SeminarRegistrationService,
                        private val speakerService: BaseSpeakerService,
                        private val partnerService: PartnerService,
                        private val searchService: SearchService,
                        private val campaignService: CampaignService,
                        private val adminService: AdminService,
                        @Autowired(required = false)
                        private val geoService: GeoService,
                        private val adminBaseControllerService: AdminBaseControllerService) {

    @Value("https://\${static.host:}\${static.base.url:}")
    lateinit var staticHost: String

    @GetMapping(School.ADMIN_SEMINARS)
    fun getSeminars(@RequestParam(defaultValue = "0") offset: Int,
                    @RequestParam(defaultValue = "10") limit: Int,
                    @RequestParam(defaultValue = "") searchString: String,
                    @RequestParam(required = false) dateTo: Date?,
                    @RequestParam(required = false) dateFrom: Date?): ModelAndView {
        val modelAndView = adminBaseControllerService.createAdminDefaultModelAndView(Actions.ADMIN_SEMINARS)

        var dateFrom2 = dateFrom
        if (dateFrom2 == null) {
            val formatter = SimpleDateFormat("dd/MM/yyyy")
            dateFrom2 = formatter.parse("01/01/1900")
        }
        val datepickerInterval = setIntervalTodayAsDate(dateFrom2, dateTo, modelAndView)
        val interval = Interval(datepickerInterval.dateFrom, DateUtils.addDays(datepickerInterval.dateTo, 1))

        val seminarSearchResult = searchService.searchForSeminars(searchString, interval.dateFrom, interval.dateTo, offset, limit)

        modelAndView.addObject("seminarList", seminarSearchResult.searchResultRecords)
        modelAndView.addObject("count", seminarSearchResult.estimatedCount)
        modelAndView.addObject("offset", offset)
        modelAndView.addObject("limit", limit)
        modelAndView.addObject("searchString", searchString)

        val seminarsOnMainPage = seminarService.selectSeminarListIsShownOnMainPage(true, 0, 1)
        if (seminarsOnMainPage != null && seminarsOnMainPage.size > 0) {
            modelAndView.addObject("isThereSeminarOnMainPage", true)
        } else {
            modelAndView.addObject("isThereSeminarOnMainPage", false)
        }

        return modelAndView
    }

    @GetMapping(School.ADMIN_SEMINAR_INFO)
    fun getSeminarInfo(@ModelAttribute("seminar") seminar: Seminar,
                       errors: BindingResult?,
                       @RequestParam(defaultValue = "0") seminarId: Long): ModelAndView {
        var seminar = seminar

        if (errors == null || !errors.hasErrors()) {
            seminar = if (seminarId > 0) {
                seminarService.selectSeminar(seminarId)
            } else {
                Seminar()
            }
        }

        val modelAndView = adminBaseControllerService.createAdminDefaultModelAndView(Actions.ADMIN_SEMINAR_INFO)
        modelAndView.addObject("staticHost", staticHost)
        modelAndView.addObject("seminar", seminar)
        if (errors != null) {
            modelAndView.addObject("errors", errors.allErrors)
        }

        val campaignTags = campaignService.campaignsCategories
        modelAndView.addObject("campaignTags", campaignTags)

        if (seminar.cityId != null && seminar.cityId > 0) {
            val city = geoService.getCityById(seminar.cityId.toInt())
            modelAndView.addObject("countryId", city.countryId)
            modelAndView.addObject("cityNameRus", city.nameRus)
        } else {
            modelAndView.addObject("countryId", 1)
        }

        val speakers = if (seminar.speakersIds != null) {
            speakerService.selectSpeakerList(Arrays.asList(*ArrayUtils.toObject(seminar.speakersIds)))
        } else {
            emptyList()
        }
        modelAndView.addObject("speakers", speakers)

        val partners = if (seminar.partnersIds != null) {
            partnerService.selectPartnerList(Arrays.asList(*ArrayUtils.toObject(seminar.partnersIds)))
        } else {
            emptyList()
        }
        return modelAndView.addObject("partners", partners)
    }

    @PostMapping(School.ADMIN_SEMINAR_INFO)
    fun saveSeminarInfo(@Valid @ModelAttribute("seminar") seminar: Seminar,
                        result: BindingResult): ModelAndView {
        if (result.hasErrors()) {
            var seminarId = seminar.seminarId
            if (seminarId == null) {
                seminarId = 0L
            }
            return getSeminarInfo(seminar, result, seminarId)
        }

        val clientId = myProfileId()
        if (!adminBaseControllerService.permissionService.hasAdministrativeRole(clientId)) {
            throw PermissionException()
        }

        seminarService.insertOrUpdateSeminar(seminar)
        return adminBaseControllerService.baseControllerService
                .createRedirectModelAndView(School.ADMIN_SEMINAR_INFO + "?seminarId=" + seminar.seminarId)
    }

    @PostMapping(School.ADMIN_REMOVE_SEMINAR)
    fun removeSeminar(@RequestParam seminarId: Long,
                      @RequestParam(defaultValue = "") returnUrl: String): ModelAndView {
        val clientId = myProfileId()
        if (!adminBaseControllerService.permissionService.hasAdministrativeRole(clientId)) {
            throw PermissionException()
        }
        seminarService.removeSeminar(seminarId)
        return adminBaseControllerService.baseControllerService.createRedirectModelAndView(returnUrl)
    }

    @PostMapping(School.ADMIN_SPEAKER_SAVE)
    @ResponseBody
    fun saveSpeaker(@RequestBody speaker: SpeakerForJson?): ActionStatus<*> {

        val clientId = myProfileId()
        if (!adminBaseControllerService.permissionService.hasAdministrativeRole(clientId)) {
            throw PermissionException()
        }

        if (speaker != null) {
            speakerService.insertOrUpdateSpeaker(speaker)
        }
        return ActionStatus.createSuccessStatus<SpeakerForJson>(speaker)
    }

    @PostMapping(School.ADMIN_PARTNER_SAVE)
    @ResponseBody
    fun savePartner(@RequestBody partner: PartnerForJson?): ActionStatus<*> {

        val clientId = myProfileId()
        if (!adminBaseControllerService.permissionService.hasAdministrativeRole(clientId)) {
            throw PermissionException()
        }

        if (partner != null) {
            partnerService.insertOrUpdatePartner(partner)
        }
        return ActionStatus.createSuccessStatus<PartnerForJson>(partner)
    }

    @ResponseBody
    @RequestMapping(School.ADMIN_SEMINAR_REGISTRATION_REPORT)
    fun getSeminarReport(response: HttpServletResponse,
                         @RequestParam seminarId: Long,
                         @RequestParam(defaultValue = "EXCEL") reportType: ReportType) {
        val clientId = myProfileId()
        adminBaseControllerService.permissionService.checkAdministrativeRole(clientId)

        val seminarRegistrations = seminarRegistrationService.selectSeminarRegistrationListBySeminarId(seminarId, 0, 0)
        val report = reportType.createReport("seminar_report_" + clientId)
        report.addCaptionRow(*adminBaseControllerService.baseControllerService.messageSource.getMessage("admin.seminar.registrations.report", null,
                Locale.getDefault()).split(";".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray())

        for (registration in seminarRegistrations) {
            val profileId = registration.profileId
            val email = if (profileId != null) adminService.getUserEmail(clientId, profileId) else ""

            val profileId2 = registration.profileId2
            val email2 = if (profileId2 != null) adminService.getUserEmail(clientId, profileId2) else ""

            report.addRow(
                    registration.profileId,
                    email,
                    registration.fio,
                    registration.phone,
                    registration.profileId2,
                    email2,
                    registration.fio2,
                    registration.phone2,
                    registration.city,
                    registration.campaignShortDescription,
                    registration.campaignTargetSum,
                    registration.organizationName,
                    registration.organizationLineOfAtivity,
                    registration.position,
                    registration.workPhone,
                    registration.organizationRegistrationDate,
                    registration.organizationSite,
                    if (registration.seminarFormat) "on-line" else "off-line"
            )
        }
        report.addToResponse(response)
    }

    @PostMapping(School.ADMIN_SHOW_SEMINAR_ON_MAIN_PAGE)
    fun showSeminarOnMainPage(@RequestParam seminarId: Long,
                              @RequestParam(defaultValue = "") returnUrl: String): ModelAndView {
        val clientId = myProfileId()
        if (!adminBaseControllerService.permissionService.hasAdministrativeRole(clientId)) {
            throw PermissionException()
        }

        seminarService.shownSeminarOnMainPage(seminarId)

        return if (StringUtils.isEmpty(returnUrl)) {
            adminBaseControllerService.baseControllerService.createRedirectModelAndView(School.ADMIN_SEMINARS)
        } else adminBaseControllerService.baseControllerService.createRedirectModelAndView(returnUrl)
    }

    @PostMapping(School.ADMIN_HIDE_SEMINAR_FROM_MAIN_PAGE)
    fun hideSeminarFromMainPage(@RequestParam seminarId: Long,
                                @RequestParam(defaultValue = "") returnUrl: String): ModelAndView {
        val clientId = myProfileId()
        adminBaseControllerService.permissionService.checkAdministrativeRole(clientId)
        seminarService.hideSeminarFromMainPage(seminarId)

        return if (StringUtils.isEmpty(returnUrl)) {
            adminBaseControllerService.baseControllerService.createRedirectModelAndView(School.ADMIN_SEMINARS)
        } else adminBaseControllerService.baseControllerService.createRedirectModelAndView(returnUrl)

    }
}

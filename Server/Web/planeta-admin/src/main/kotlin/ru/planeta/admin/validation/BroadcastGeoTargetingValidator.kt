package ru.planeta.admin.validation

import org.springframework.stereotype.Component
import org.springframework.validation.Errors
import org.springframework.validation.ValidationUtils
import org.springframework.validation.Validator
import ru.planeta.model.profile.broadcast.BroadcastGeoTargeting

/**
 * Date: 14.04.13
 */
@Component
class BroadcastGeoTargetingValidator : Validator {
    override fun supports(aClass: Class<*>): Boolean {
        return aClass.isAssignableFrom(BroadcastGeoTargeting::class.java)
    }

    override fun validate(o: Any, errors: Errors) {
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "countryId", "field.required")
    }
}

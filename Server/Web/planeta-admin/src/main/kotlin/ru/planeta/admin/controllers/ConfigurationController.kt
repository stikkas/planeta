package ru.planeta.admin.controllers

import org.springframework.beans.factory.annotation.Value
import org.springframework.context.MessageSource
import org.springframework.stereotype.Controller
import org.springframework.validation.BindingResult
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.servlet.ModelAndView
import ru.planeta.admin.Actions
import ru.planeta.admin.Urls
import ru.planeta.admin.controllers.services.AdminBaseControllerService
import ru.planeta.api.helper.ProjectService
import ru.planeta.api.service.admin.AdminService
import ru.planeta.api.service.common.CustomMetaTagService
import ru.planeta.api.service.configurations.ConfigurationService
import ru.planeta.api.service.configurations.StaticNodesService
import ru.planeta.api.service.profile.AuthorizationService
import ru.planeta.api.service.profile.ProfileService
import ru.planeta.api.service.profile.UserService
import ru.planeta.api.service.security.PermissionService
import ru.planeta.api.web.authentication.AuthUtils
import ru.planeta.api.web.authentication.myProfileId
import ru.planeta.api.web.utils.ProperRedirectView
import ru.planeta.mail.MailService
import ru.planeta.model.common.Configuration
import javax.validation.Valid

/**
 * Created with IntelliJ IDEA.
 * Date: 03.10.12
 * Time: 13:28
 */
@Controller
class ConfigurationController(private val configurationService: ConfigurationService,
                              private val adminBaseControllerService: AdminBaseControllerService) {

    @GetMapping(Urls.ADMIN_CONFIGURATION_LIST)
    fun getConfigurationList(@RequestParam(required = false) query: String?,
                             @RequestParam(defaultValue = "0") offset: Int,
                             @RequestParam(defaultValue = "0") limit: Int): ModelAndView =
            adminBaseControllerService.createAdminDefaultModelAndView(Actions.ADMIN_CONFIGURATIONS)
                    .addObject("configurations", configurationService.getConfigurationList(query, offset, limit))
                    .addObject("query", query)

    @PostMapping(Urls.ADMIN_CONFIGURATION_DELETE)
    fun deleteConfiguration(@RequestParam(defaultValue = "0") configurationKey: String): ModelAndView {
        verifyPlanetaAdminPermissions()
        configurationService.deleteConfigurationByKey(configurationKey)
        return ModelAndView(ProperRedirectView(Urls.ADMIN_CONFIGURATION_LIST))
    }

    @GetMapping(Urls.ADMIN_CONFIGURATION_EDIT)
    fun getConfigurationToEdit(@RequestParam(defaultValue = "NEW") configurationKey: String): ModelAndView {
        var configuration = configurationService.getConfigurationByKey(configurationKey)
        if (configuration == null) {
            configuration = Configuration()
        }
        return adminBaseControllerService.createAdminDefaultModelAndView(Actions.ADMIN_CONFIGURATION_EDIT)
                .addObject("configuration", configuration)
    }

    @PostMapping(Urls.ADMIN_CONFIGURATION_EDIT)
    fun saveConfiguration(@Valid configuration: Configuration, result: BindingResult): ModelAndView =
            if (result.hasErrors()) {
                getConfigurationToEdit(configuration.key)
            } else {
                verifyPlanetaAdminPermissions()
                configurationService.saveConfiguration(configuration)
                ModelAndView(ProperRedirectView(Urls.ADMIN_CONFIGURATION_LIST))
            }

    private fun verifyPlanetaAdminPermissions() = adminBaseControllerService.permissionService.checkAdministrativeRole(myProfileId())

}

package ru.planeta.admin.controllers.services

import org.springframework.stereotype.Service
import org.springframework.web.servlet.ModelAndView
import ru.planeta.api.exceptions.MessageCode
import ru.planeta.api.exceptions.PermissionException
import ru.planeta.api.service.security.PermissionService
import ru.planeta.api.web.authentication.myProfileId
import ru.planeta.api.web.controllers.IAction
import ru.planeta.api.web.controllers.services.BaseControllerService
import ru.planeta.model.enums.ProjectType

/**
 * Controller for admin methods
 *
 * @author ds.kolyshev
 * Date: 17.11.11
 */
@Service
class AdminBaseControllerService(val permissionService: PermissionService,
                                 val baseControllerService: BaseControllerService) {

    fun createAdminDefaultModelAndView(action: IAction): ModelAndView =
            baseControllerService.createDefaultModelAndView(action, ProjectType.ADMIN)

    /**
     * Verifies administrator permission for current user.
     * @param messageCode exception code, if checking not passed.
     * @throws ru.planeta.api.exceptions.PermissionException
     */
    fun verifyAdminPermission(messageCode: MessageCode) {
        if (!permissionService.isPlanetaAdmin(myProfileId())) {
            throw PermissionException(messageCode)
        }
    }

}

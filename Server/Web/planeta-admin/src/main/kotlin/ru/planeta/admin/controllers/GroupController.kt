package ru.planeta.admin.controllers

import org.apache.log4j.Logger
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.servlet.ModelAndView
import ru.planeta.admin.Actions
import ru.planeta.admin.Urls
import ru.planeta.admin.controllers.services.AdminBaseControllerService
import ru.planeta.api.service.admin.AdminService
import ru.planeta.api.web.authentication.myProfileId
import ru.planeta.commons.web.IpUtils
import ru.planeta.dao.profiledb.GroupDAO
import ru.planeta.model.enums.ProfileStatus
import java.util.*
import javax.servlet.http.HttpServletRequest

/**
 * Created with IntelliJ IDEA.
 * Date: 03.10.12
 * Time: 13:21
 */
@Controller
class GroupController(private val groupDAO: GroupDAO,
                      private val adminService: AdminService,
                      private val adminBaseControllerService: AdminBaseControllerService) {

    private val logger = Logger.getLogger(GroupController::class.java)

    @GetMapping(Urls.ADMIN_GROUP_INFO, Urls.MODERATOR_GROUP_INFO)
    fun getGroupInfo(@RequestParam profileId: Long,
                     @RequestParam(required = false) searchString: String?,
                     @RequestParam(value = "any", defaultValue = "false") filterAny: Boolean,
                     @RequestParam(value = "pending", defaultValue = "false") filterPending: Boolean,
                     @RequestParam(value = "active", defaultValue = "false") filterActive: Boolean,
                     @RequestParam(value = "rejected", defaultValue = "false") filterRejected: Boolean,
                     @RequestParam(required = false) successMessage: String?,
                     @RequestParam(required = false) errorMessage: String?): ModelAndView {

        val profile = adminBaseControllerService
                .baseControllerService.profileService.getProfileSafe(profileId)
        return adminBaseControllerService.createAdminDefaultModelAndView(Actions.ADMIN_GROUP_INFO)
                .addObject("searchString", searchString)
                .addObject("group", groupDAO.selectByProfileId(profileId))
                .addObject("groupProfile", profile)
                .addObject("filterAny", filterAny)
                .addObject("filterPending", filterPending)
                .addObject("filterActive", filterActive)
                .addObject("filterRejected", filterRejected)
                .addObject("intervalChecked", false)
                .addObject("successMessage", successMessage)
                .addObject("errorMessage", errorMessage)
    }

    /**
     * Sets specified status for group
     */
    @RequestMapping(Urls.ADMIN_GROUP_SET_STATUS)
    fun setGroupStatus(@RequestParam profileId: Long,
                       @RequestParam(value = "status") profileStatus: ProfileStatus,
                       req: HttpServletRequest): ModelAndView {
        val myProfileId = myProfileId()
        adminService.setProfileStatus(myProfileId, profileId, profileStatus)
        val ip = IpUtils.getRemoteAddr(req)
        logger.info("Admin id=$myProfileId changed group id=$profileId satus ip=$ip")
        val additionalParams = HashMap<String, Any>()
        additionalParams.put("successMessage", adminBaseControllerService
                .baseControllerService.messageSource.getMessage("project.events", null, Locale.getDefault()))
        additionalParams.put("profileId", profileId)
        return adminBaseControllerService
                .baseControllerService.createRedirectModelAndView(Urls.ADMIN_GROUP_INFO, additionalParams)
    }

    @PostMapping(Urls.ADMIN_DELETE_GROUP)
    fun deleteGroup(@RequestParam profileId: Long): ModelAndView =
            try {
                val clientId = myProfileId()
                adminService.deleteProfile(clientId, profileId)
                logger.info("Admin #$clientId has deleted group #$profileId")
                adminBaseControllerService.baseControllerService.createRedirectModelAndView(Urls.ADMIN_USERS)
            } catch (ex: Exception) {
                logger.error(ex.message)

                val additionalParams = HashMap<String, Any>()
                additionalParams.put("errorMessage", adminBaseControllerService
                        .baseControllerService.messageSource.getMessage("failed.to.deleteByProfileId.the.group", null, Locale.getDefault()))
                adminBaseControllerService
                        .baseControllerService.createRedirectModelAndView("${Urls.ADMIN_GROUP_INFO}?profileId=$profileId", additionalParams)
            }
}

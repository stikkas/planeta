package ru.planeta.admin.controllers.promo

import org.springframework.validation.BindingResult
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.servlet.ModelAndView
import ru.planeta.admin.Actions
import ru.planeta.admin.Urls
import ru.planeta.admin.controllers.services.AdminBaseControllerService
import ru.planeta.api.exceptions.NotFoundException
import ru.planeta.api.service.campaign.CampaignService
import ru.planeta.api.service.promo.ExternalPromoCodeService
import ru.planeta.api.service.promo.PromoConfigService
import ru.planeta.api.web.authentication.myProfileId
import ru.planeta.dao.commondb.MailTemplateDAO
import ru.planeta.model.enums.PromoConfigStatus
import ru.planeta.model.trashcan.PromoConfig
import java.util.*
import javax.validation.Valid

@RestController
class PromoConfigController(private val adminBaseControllerService: AdminBaseControllerService,
                            private val promoConfigService: PromoConfigService,
                            private val externalPromoCodeService: ExternalPromoCodeService,
                            private val mailTemplateDAO: MailTemplateDAO,
                            private val campaignService: CampaignService) {

    @GetMapping(Urls.PROMO_CONFIG_LIST)
    fun promoConfigList(): ModelAndView {
        var configs = promoConfigService.selectAll()
        if (configs == null) {
            configs = listOf()
        }
        return adminBaseControllerService.createAdminDefaultModelAndView(Actions.ADMIN_PROMO_CONFIG_LIST)
                .addObject("configs", configs)
    }

    @GetMapping(Urls.PROMO_CONFIG)
    fun promoConfig(@RequestParam("configId") configId: Long?): ModelAndView {
        val config: PromoConfig
        if (configId == null || configId == 0L) {
            config = PromoConfig()
        } else {
            config = promoConfigService.selectById(configId) ?: throw NotFoundException()
        }

        if (config.isHasPromocode) {
            config.promoCodesList = externalPromoCodeService.getAllCodes(config.configId)
        }
        return getPromoCodeModelAndView(config)
    }

    @PostMapping(Urls.PROMO_CONFIG)
    fun savePromoConfig(@Valid config: PromoConfig, errors: BindingResult): ModelAndView {
        if (errors.hasErrors()) {
            return getPromoCodeModelAndView(config)
                    .addObject("errors", errors.allErrors)
        }

        if (config.timeFinish.before(Date())) {
            config.status = PromoConfigStatus.FINISHED;
        }

        if (config.configId == 0L) {
            promoConfigService.insert(myProfileId(), config)
        } else {
            promoConfigService.update(myProfileId(), config)
        }

        if (config.isHasPromocode) {
            val promoCodes = config.promoCodesList
            if (promoCodes!= null && !promoCodes.isEmpty()) {
                externalPromoCodeService.insertCodes(config.configId, promoCodes)
            }
        }

        return adminBaseControllerService.baseControllerService.createRedirectModelAndView(Urls.PROMO_CONFIG + "?configId=" + config.configId)
    }

    @PostMapping(Urls.PROMO_CONFIG_CHANGE_STATUS)
    fun changePromoConfigStatus(@RequestParam configId: Long?) : ModelAndView {
        if (configId != null && configId != 0L) {
            promoConfigService.switchStatus(myProfileId(), configId)
        }
        return adminBaseControllerService.baseControllerService.createRedirectModelAndView(Urls.PROMO_CONFIG_LIST)
    }


    private fun getPromoCodeModelAndView(config: PromoConfig): ModelAndView {
        val codesUsed = if (config.isHasPromocode) externalPromoCodeService.getUsedPromoCodesCount(config.configId) else 0L
        return adminBaseControllerService.createAdminDefaultModelAndView(Actions.ADMIN_PROMO_CONFIG)
                .addObject("promoConfig", config)
                .addObject("codesUsed", codesUsed)
                .addObject("statuses", EnumSet.allOf(PromoConfigStatus::class.java))
                .addObject("mailTemplates", mailTemplateDAO.selectMailTemplates().map { mailTemplate -> mailTemplate.name })
                .addObject("tags", campaignService.allTags)
    }
}
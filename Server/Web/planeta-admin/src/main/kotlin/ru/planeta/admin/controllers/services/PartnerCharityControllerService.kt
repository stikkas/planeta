package ru.planeta.admin.controllers.services

import org.springframework.stereotype.Service
import ru.planeta.admin.Actions
import ru.planeta.admin.Urls
import ru.planeta.api.model.ConfigurationType
import ru.planeta.api.service.configurations.ConfigurationService
import ru.planeta.api.web.controllers.services.BaseControllerService

@Service
class PartnerCharityControllerService(adminBaseControllerService: AdminBaseControllerService,
                                      baseControllerService: BaseControllerService,
                                      configurationService: ConfigurationService) :
        BasePromoControllerService(adminBaseControllerService, baseControllerService, configurationService) {

    override val configurationKey: String = ConfigurationType.CHARITY_PARTNERS_CONFIGURATION_LIST
    override val actionAdd: Actions = Actions.ADMIN_CHARITY_PARTNERS_PROMO_ADD
    override val actionEdit: Actions = Actions.ADMIN_CHARITY_PARTNERS_PROMO_EDIT
    override val actionList: Actions = Actions.ADMIN_CHARITY_PARTNERS_PROMO_LIST
    override val urlList: String = Urls.ADMIN_CHARITY_PARTNERS_PROMO_LIST
}
package ru.planeta.admin.controllers

import org.apache.commons.lang3.StringUtils
import org.springframework.stereotype.Controller
import org.springframework.validation.BindingResult
import org.springframework.web.bind.annotation.*
import org.springframework.web.servlet.ModelAndView
import ru.planeta.admin.Actions
import ru.planeta.admin.Urls
import ru.planeta.admin.controllers.services.AdminBaseControllerService
import ru.planeta.api.exceptions.PermissionException
import ru.planeta.api.helper.ProjectService
import ru.planeta.api.model.ActionStatus
import ru.planeta.api.news.LoggerService
import ru.planeta.api.service.quiz.QuizService
import ru.planeta.api.web.authentication.myProfileId
import ru.planeta.model.enums.ProjectType
import ru.planeta.model.news.ProfileNews
import ru.planeta.model.trashcan.Quiz
import ru.planeta.model.trashcan.QuizAnswerForReport
import ru.planeta.model.trashcan.QuizQuestion
import ru.planeta.model.trashcan.QuizQuestionOption
import ru.planeta.reports.ReportType
import java.text.SimpleDateFormat
import java.util.*
import javax.servlet.http.HttpServletResponse
import javax.validation.Valid


@Controller
class AdminQuizController(private val quizService: QuizService,
                          private val profileNewsService: LoggerService,
                          projectService: ProjectService,
                          private val adminBaseControllerService: AdminBaseControllerService) {

    private val mainHost: String = projectService.getHost(ProjectType.MAIN)

    @GetMapping(Urls.QUIZZES)
    fun getQuizzes(@RequestParam(defaultValue = "") searchString: String,
                   @RequestParam(defaultValue = "0") offset: Int,
                   @RequestParam(defaultValue = "10") limit: Int): ModelAndView {
        val quizzes = quizService.getQuizzesSearch(searchString, offset, limit)

        val modelAndView = adminBaseControllerService.createAdminDefaultModelAndView(Actions.ADMIN_QUIZZES)
        modelAndView.addObject("searchString", searchString)
        modelAndView.addObject("quizzes", quizzes)
        modelAndView.addObject("count", quizzes.totalCount)
        modelAndView.addObject("offset", offset)
        modelAndView.addObject("limit", limit)
        modelAndView.addObject("appHost", mainHost)
        return modelAndView
    }


    @GetMapping(Urls.QUIZ_INFO)
    fun getQuizInfo(@ModelAttribute("quiz") quiz: Quiz,
                    bindingResult: BindingResult?,
                    @RequestParam quizId: Long): ModelAndView {
        var quiz = quiz
        if (bindingResult == null || !bindingResult.hasErrors()) {
            if (quizId > 0) {
                quiz = quizService.getQuizSafe(quizId.toString(), null)
            } else {
                quiz = Quiz()
            }
        }

        val modelAndView = adminBaseControllerService.createAdminDefaultModelAndView(Actions.ADMIN_QUIZ_INFO)
        modelAndView.addObject("quiz", quiz)
        val quizQuestions = quizService.getQuizQuestionsByQuizId(quizId, null)
        modelAndView.addObject("quizQuestions", quizQuestions ?: ArrayList<QuizQuestion>())
        return modelAndView
    }

    @PostMapping(Urls.QUIZ_INFO)
    fun setQuizInfo(@ModelAttribute quiz: Quiz,
                    bindingResult: BindingResult): ModelAndView {
        if (!adminBaseControllerService.permissionService.hasAdministrativeRole(myProfileId())) {
            throw PermissionException()
        }
        if (StringUtils.isBlank(quiz.alias)) {
            quiz.alias = null
        } else {
            val regex = Regex("(^[0-9]+$)|(.*([\\s]+).*)|(.*([а-яА-я]+.*))")
            val match = regex.matchEntire(quiz.alias)
            if (match != null) {
                bindingResult.rejectValue("alias", "exist.quiz.alias.rules")
            }
        }

        if (bindingResult.hasErrors()) {
            return getQuizInfo(quiz, bindingResult, quiz.quizId)
        }

        var selectedByAlias: Quiz? = null
        if (quiz.alias != null) {
            selectedByAlias = quizService.getQuiz(quiz.alias, null)
        }
        if (selectedByAlias != null
                && selectedByAlias.quizId != quiz.quizId) {
            bindingResult.rejectValue("alias", "exist.quiz.same.name")
        } else {
            quizService.addOrUpdate(quiz)
            profileNewsService.addProfileNews(ProfileNews.Type.ADMIN_CHANGE_QUIZ_INFO, myProfileId(), quiz.quizId)
        }

        return if (bindingResult.hasErrors()) {
            getQuizInfo(quiz, bindingResult, quiz.quizId)
        } else adminBaseControllerService.baseControllerService.createRedirectModelAndView(Urls.QUIZ_INFO, "quizId", quiz.quizId)
    }

    @GetMapping(Urls.QUIZ_QUESTIONS_SORT)
    fun quizQuestionsSort(@RequestParam quizId: Int,
                          @RequestParam quizQuestionId: Int,
                          @RequestParam oldOrderNum: Int,
                          @RequestParam newOrderNum: Int): ActionStatus<*> {
        if (!adminBaseControllerService.permissionService.hasAdministrativeRole(myProfileId())) {
            throw PermissionException()
        }
        quizService.updateQuizQuestionsOrder(quizId.toLong(), quizQuestionId.toLong(), newOrderNum, oldOrderNum)
        return ActionStatus.createSuccessStatus<Any>()
    }

    @GetMapping(Urls.QUIZ_QUESTION_EDIT)
    fun getQuizQuestionEdit(@ModelAttribute("quizQuestion") quizQuestion: QuizQuestion,
                            bindingResult: BindingResult?,
                            @RequestParam quizQuestionId: Long,
                            @RequestParam quizId: Long): ModelAndView {
        var quizQuestion = quizQuestion
        var quiz: Quiz? = null
        if (quizId > 0) {
            quiz = quizService.getQuizSafe(quizId.toString(), null)
        } else {
            throw IllegalArgumentException("quizId=" + quizId)
        }

        if (bindingResult == null || !bindingResult.hasErrors()) {
            if (quizQuestionId > 0) {
                quizQuestion = quizService.getQuizQuestionSafe(quizQuestionId)
            } else {
                quizQuestion = QuizQuestion()
            }
        }

        val modelAndView = adminBaseControllerService.createAdminDefaultModelAndView(Actions.ADMIN_QUIZ_QUESTION_EDIT)
        modelAndView.addObject("quiz", quiz)
        modelAndView.addObject("quizQuestion", quizQuestion)
        return modelAndView
    }

    @PostMapping(Urls.QUIZ_QUESTION_EDIT)
    fun setQuizQuestionEdit(@Valid @ModelAttribute quizQuestion: QuizQuestion,
                            bindingResult: BindingResult,
                            @RequestParam quizId: Long): ModelAndView {
        if (!adminBaseControllerService.permissionService.hasAdministrativeRole(myProfileId())) {
            throw PermissionException()
        }

        if (bindingResult.hasErrors()) {
            return getQuizQuestionEdit(quizQuestion, bindingResult, quizQuestion.quizQuestionId, quizId)
        }

        val quiz = quizService.getQuizSafe(quizId.toString(), null)
        val selectedQuizQuestion = quizService.getQuizQuestionById(quizQuestion.quizQuestionId)
        if (selectedQuizQuestion != null) {
            quizService.updateQuizQuestion(quizQuestion)
        } else {
            quizService.addQuizQuestion(quizQuestion)
            quizService.addQuizQuestionRelation(quizId, quizQuestion.quizQuestionId)
        }

        return adminBaseControllerService
                .baseControllerService
                .createRedirectModelAndView(Urls.QUIZ_QUESTION_EDIT, "quizId", quiz.quizId, "quizQuestionId", quizQuestion.quizQuestionId)
    }

    @PostMapping(Urls.QUIZ_QUESTION_CHANGE_ENABLING)
    @ResponseBody
    fun setQuizQuestionEnabling(@RequestParam quizQuestionId: Long,
                                @RequestParam enabled: Boolean): ActionStatus<*> {
        if (!adminBaseControllerService.permissionService.hasAdministrativeRole(myProfileId())) {
            throw PermissionException()
        }
        quizService.setQuizQuestionEnabling(quizQuestionId, enabled)
        return ActionStatus.createSuccessStatus<Any>()
    }

    @PostMapping(Urls.QUIZ_QUESTION_OPTION)
    @ResponseBody
    fun setQuizQuestionOption(@ModelAttribute quizQuestionOption: QuizQuestionOption,
                              bindingResult: BindingResult): ActionStatus<*> {
        if (!adminBaseControllerService.permissionService.hasAdministrativeRole(myProfileId())) {
            throw PermissionException()
        }

        if (bindingResult.hasErrors()) {
            return ActionStatus.createErrorStatus<Any>("error")
        }

        val selectedQuizQuestion = quizService.getQuizQuestionSafe(quizQuestionOption.quizQuestionId)
        if (quizQuestionOption.quizQuestionOptionId == 0L) {
            quizService.addOrUpdateQuizQuestionOption(quizQuestionOption)
        } else {
            quizService.updateQuizQuestionOption(quizQuestionOption)
        }

        return ActionStatus.createSuccessStatus<Any>()
    }

    @PostMapping(Urls.QUIZ_QUESTION_OPTION_REMOVE)
    @ResponseBody
    fun setQuizQuestioOptionRemove(@RequestParam quizQuestionOptionId: Long): ActionStatus<*> {
        if (!adminBaseControllerService.permissionService.hasAdministrativeRole(myProfileId())) {
            throw PermissionException()
        }
        quizService.deleteQuizQuestionOption(quizQuestionOptionId)
        return ActionStatus.createSuccessStatus<Any>()
    }

    @GetMapping(Urls.QUIZ_QUESTIONS_OPTION_SORT)
    fun quizQuestionOptionsSort(@RequestParam quizQuestionOptionId: Int,
                                @RequestParam quizQuestionId: Int,
                                @RequestParam oldOrderNum: Int,
                                @RequestParam newOrderNum: Int): ActionStatus<*> {
        if (!adminBaseControllerService.permissionService.hasAdministrativeRole(myProfileId())) {
            throw PermissionException()
        }
        quizService.updateQuizQuestionOptionsOrder(quizQuestionOptionId.toLong(), quizQuestionId.toLong(), newOrderNum, oldOrderNum)
        return ActionStatus.createSuccessStatus<Any>()
    }

    @ResponseBody
    @RequestMapping(Urls.ADMIN_QUIZ_ANSWERS_REPORT)
    fun getQuizAnswersReport(response: HttpServletResponse,
                             @RequestParam quizId: Long,
                             @RequestParam(defaultValue = "CSV") reportType: ReportType) {

        val report = reportType.createReport("quiz_answers_report_quiz_" + quizId + "_" + SimpleDateFormat("yyyy-MM-dd").format(Date()))
        report.addCaptionRow(*adminBaseControllerService.baseControllerService
                .messageSource.getMessage("admin.quiz.report", null, Locale.getDefault()).split(";".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray())

        var offset: Long = 0
        val step = 500
        val limit = offset + step

        var quizAnswerForReportList: List<QuizAnswerForReport>
        do {
            quizAnswerForReportList = quizService.getQuizAnswersForReportByParams(quizId, null, offset, limit)
            for (quizAnswerForReport in quizAnswerForReportList) {
                report.addRow(
                        quizAnswerForReport.questionText, quizAnswerForReport.answerCustomText,
                        quizAnswerForReport.answerInteger, quizAnswerForReport.isAnswerBoolean,
                        quizAnswerForReport.userId, quizAnswerForReport.timeAdded
                )
            }
            offset += step.toLong()
        } while (quizAnswerForReportList.size >= step)

        report.addToResponse(response)
    }
}


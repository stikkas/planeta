package ru.planeta.admin

/**
 * JSP sections/directories
 *
 * @author ds.kolyshev
 * Date: 01.09.11
 */
enum class ActionPrefixes (private val text: String) {

    EMPTY(""),
    ADMIN("admin/"),
    BIBLIO("admin/biblio/"),
    BILLING("admin/billing/"),
    CONCERT("admin/concert/"),
    SCHOOL("admin/school/"),
    IFRAME_APP("admin/iframe-app/"),
    GEO("admin/geo/"),
    PROMO("admin/promo/"),
    LOYALTY("admin/loyalty/"),
    SHOP("admin/shop/"),
    STAT("admin/stat/"),
    CAMPAIGN("admin/campaign/"),
    CHARITY("admin/charity/");

    override fun toString() = text
}

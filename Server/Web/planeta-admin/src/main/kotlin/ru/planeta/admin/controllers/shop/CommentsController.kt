package ru.planeta.admin.controllers.shop

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.servlet.ModelAndView
import ru.planeta.admin.Actions
import ru.planeta.admin.Shop
import ru.planeta.admin.controllers.services.AdminBaseControllerService
import ru.planeta.admin.utils.DateUtils.setIntervalTodayAsDate
import ru.planeta.dao.profiledb.CommentDAO
import java.text.SimpleDateFormat
import java.util.*

/**
 * Created with IntelliJ IDEA.
 * Date: 15.09.2015
 * Time: 14:44
 * To change this template use File | Settings | File Templates.
 */
@Controller("AdminCommentsController")
class CommentsController(private val adminControllerService: AdminBaseControllerService) {

    @Autowired
    private val commentDAO: CommentDAO? = null

    @GetMapping(Shop.PRODUCT_COMMENTS_LIST)
    operator fun get(@RequestParam(required = false) query: String?,
                     @RequestParam(defaultValue = "20") limit: Int,
                     @RequestParam(defaultValue = "0") offset: Int,
                     @RequestParam(required = false) dateFrom: Date?,
                     @RequestParam(required = false) dateTo: Date?): ModelAndView {

        val modelAndView = adminControllerService.createAdminDefaultModelAndView(Actions.ADMIN_PRODUCT_COMMENTS)
        var dateFrom2 = dateFrom
        if (dateFrom2 == null) {
            val formatter = SimpleDateFormat("dd/MM/yyyy")
            dateFrom2 = formatter.parse("01/01/1900")
        }
        val interval = setIntervalTodayAsDate(dateFrom2, dateTo, modelAndView)
        return modelAndView
                .addObject("comments", commentDAO!!.selectCommentsForAdminByDate(query, interval.dateFrom, interval.dateTo, limit, offset))
                .addObject("limit", limit)
                .addObject("offset", offset)
                .addObject("query", query)
    }
}

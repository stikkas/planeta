package ru.planeta.admin.validation

import org.apache.commons.lang3.StringUtils
import org.springframework.stereotype.Component
import org.springframework.validation.Errors
import org.springframework.validation.Validator
import ru.planeta.api.model.promo.TvPromoConfiguration

/**
 * Date: 29.10.12
 * Time: 16:50
 */
@Component
class TvPromoConfigurationValidator : Validator {
    override fun supports(aClass: Class<*>): Boolean {
        return aClass.isAssignableFrom(TvPromoConfiguration::class.java)
    }

    override fun validate(o: Any, errors: Errors) {
        try {
            val concertPromoConfiguration = o as TvPromoConfiguration
            if (StringUtils.isEmpty(concertPromoConfiguration.title)) {
                errors.rejectValue("title", "field.required")
            }
            if (StringUtils.isEmpty(concertPromoConfiguration.imageUrl)) {
                errors.rejectValue("imageUrl", "field.required")
            }
            if (StringUtils.isEmpty(concertPromoConfiguration.text)) {
                errors.rejectValue("text", "field.required")
            }
            if (StringUtils.isEmpty(concertPromoConfiguration.html)) {
                errors.reject("field.required")
            }
        } catch (e: Exception) {
            errors.reject("field.required")
        }

    }
}

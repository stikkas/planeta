package ru.planeta.admin.controllers.services

import org.springframework.stereotype.Service
import org.springframework.web.servlet.ModelAndView
import ru.planeta.admin.Actions
import ru.planeta.admin.utils.DateUtils.setIntervalTodayAsDate
import ru.planeta.api.Utils
import ru.planeta.api.service.common.PlanetaManagersService
import ru.planeta.api.service.security.PermissionService
import ru.planeta.api.web.authentication.myProfileId
import ru.planeta.api.web.controllers.services.BaseControllerService
import ru.planeta.dao.ListWithCount
import ru.planeta.model.common.campaign.CampaignNews
import ru.planeta.model.common.campaign.param.CampaignEventsParam
import ru.planeta.model.enums.ProjectType

/**
 * Controller for admin campaigns tab (now read only)
 * Later expected to be extended to read|modify managers list and campaign types relations
 *
 * @author Andrew.Arefyev@gmail.com
 * Date: 20.02.13 11:03
 */
@Service
class BaseCampaignControllerService(val baseControllerService: BaseControllerService,
                                    val planetaManagersService: PlanetaManagersService,
                                    val permissionService: PermissionService) {

    fun createCampaignEventsModelAndView(action: Actions, campaignEventsParam: CampaignEventsParam): ModelAndView {
        val modelAndView = createAdminDefaultModelAndView(action)
        val interval = setIntervalTodayAsDate(campaignEventsParam.dateFrom, campaignEventsParam.dateTo, modelAndView)
        campaignEventsParam.dateFrom = interval.dateFrom
        campaignEventsParam.dateTo = interval.dateTo

        modelAndView
                .addObject("profileNewsType", campaignEventsParam.profileNewsType)
                .addObject("campaignId", campaignEventsParam.campaignId)
                .addObject("query", campaignEventsParam.query)
                .addObject("offset", campaignEventsParam.offset)
                .addObject("limit", campaignEventsParam.limit)
                .addObject("managersList", planetaManagersService.getAllManagers(true))
                .addObject("managerId", campaignEventsParam.managerId)
        return modelAndView

    }

    fun addCampaignNewsListToModel(campaignNewsList: ListWithCount<CampaignNews>, modelAndView: ModelAndView) {
        modelAndView
                .addObject("campaignManagerList",
                        planetaManagersService.getManagersForCurrentCampaigns(
                                Utils.getIdList(CampaignNews.getCampaignList(campaignNewsList.list))))
                .addObject("campaignNewsList", campaignNewsList.list)
                .addObject("count", campaignNewsList.totalCount)
    }

    fun createAdminDefaultModelAndView(action: Actions): ModelAndView {
        permissionService.checkAdministrativeRole(myProfileId())
        return baseControllerService.createDefaultModelAndView(action, ProjectType.ADMIN)
    }
}

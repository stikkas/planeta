package ru.planeta.admin.controllers

import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.GetMapping
import ru.planeta.admin.Actions
import ru.planeta.admin.Urls
import ru.planeta.api.model.authentication.Authority
import ru.planeta.api.web.authentication.hasAuthority
import ru.planeta.api.web.authentication.isAnonymous
import ru.planeta.api.web.controllers.services.BaseControllerService
import ru.planeta.eva.api.web.cas.filter.SsoFilter
import ru.planeta.model.enums.ProjectType
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

/**
 * User: dlola
 * Date: 29.08.12
 * Time: 14:52
 */
@Controller
class LoginController(private val baseControllerService: BaseControllerService,
                      private val ssoFilter: SsoFilter) {

    @GetMapping(Urls.LOGIN)
    fun getLoginPage(response: HttpServletResponse, request: HttpServletRequest): Any {
        return if (hasAuthority(Authority.SUPER_ADMIN) || hasAuthority(Authority.ADMIN) || hasAuthority(Authority.MANAGER)) {
            "redirect:${Urls.DASHBOARD}"
        } else {
            if (!isAnonymous()) {
                ssoFilter.logout(request, response)
            }
            baseControllerService.createDefaultModelAndView(Actions.LOGIN, ProjectType.ADMIN)
        }
    }

    /**
     * Creates admin index jsp
     *
     */
    @GetMapping(Urls.ADMIN_INDEX)
    fun index(): String {
        var returnStr = "redirect:" + Urls.DASHBOARD
        if (!(hasAuthority(Authority.SUPER_ADMIN) || hasAuthority(Authority.ADMIN) || hasAuthority(Authority.MANAGER))) {
            returnStr = "redirect:" + Urls.LOGIN
        }
        return returnStr
    }
}

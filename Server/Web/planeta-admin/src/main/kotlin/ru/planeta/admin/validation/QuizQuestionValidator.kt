package ru.planeta.admin.validation

import org.springframework.stereotype.Component
import org.springframework.validation.Errors
import org.springframework.validation.ValidationUtils
import org.springframework.validation.Validator
import ru.planeta.model.trashcan.QuizQuestion
import ru.planeta.model.trashcan.QuizQuestionType

@Component
class QuizQuestionValidator : Validator {
    override fun supports(aClass: Class<*>): Boolean {
        return aClass.isAssignableFrom(QuizQuestion::class.java)
    }

    override fun validate(o: Any, errors: Errors) {
        ValidationUtils.rejectIfEmpty(errors, "questionText", "field.required")
        val quizQuestion = o as QuizQuestion
        if (quizQuestion.type == QuizQuestionType.RADIO_NUMBER_RANGE) {
            if (quizQuestion.endValue <= quizQuestion.startValue) {
                errors.rejectValue("endValue", "quiz.endvalue.must.be.higher.than.start.value")
            }
        }
    }
}

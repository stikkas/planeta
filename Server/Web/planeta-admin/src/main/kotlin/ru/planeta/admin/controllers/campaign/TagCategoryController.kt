package ru.planeta.admin.controllers.campaign

import org.springframework.stereotype.Controller
import org.springframework.validation.BindingResult
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.servlet.ModelAndView
import ru.planeta.admin.Actions
import ru.planeta.admin.Urls
import ru.planeta.admin.controllers.services.BaseCampaignControllerService
import ru.planeta.api.service.campaign.CampaignService
import ru.planeta.commons.web.WebUtils
import ru.planeta.model.common.campaign.CampaignTag
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse
import javax.validation.Valid

/**
 * Created with IntelliJ IDEA.
 * Date: 16.09.2015
 * Time: 13:50
 * To change this template use File | Settings | File Templates.
 */
@Controller("CampaignCategoryController")
class TagCategoryController(private val campaignService: CampaignService,
                            private val baseCampaignControllerService: BaseCampaignControllerService) {

    @GetMapping(Urls.CAMPAIGN_TAGS)
    fun getCampaignTags(@RequestParam(defaultValue = "0") offset: Int,
                        @RequestParam(defaultValue = "0") limit: Int,
                        request: HttpServletRequest, response: HttpServletResponse): ModelAndView =
            baseCampaignControllerService.createAdminDefaultModelAndView(Actions.ADMIN_CATEGORIES)
                    .addObject("campaignTags", campaignService.campaignsCategories)

    @GetMapping(Urls.EDIT_CATEGORY)
    fun editCategory(@RequestParam(defaultValue = "0") campaignTagId: Long,
                     @RequestParam(required = false) errorMessage: String?): ModelAndView {
        val category = if (campaignTagId == 0L) {
            CampaignTag()
        } else {
            campaignService.getCampaignsCategorieById(campaignTagId)
        }

        val sponsors = ru.planeta.commons.lang.CollectionUtils.map(campaignService.sponsors) { sourceObject -> sourceObject.alias }
        sponsors.add(0, "")
        return baseCampaignControllerService.createAdminDefaultModelAndView(Actions.ADMIN_EDIT_CATEGORY)
                .addObject("category", category)
                .addObject("sponsors", sponsors)
                .addObject("errorMessage", errorMessage)
    }

    @PostMapping(Urls.SAVE_CAMPAIGN_CATEGORY)
    fun saveCampaignCategory(@Valid category: CampaignTag, result: BindingResult): ModelAndView =
            if (result.hasErrors()) {
                baseCampaignControllerService.baseControllerService
                        .createRedirectModelAndView("${Urls.EDIT_CATEGORY}?campaignTagId=${category.id}")
            } else {
                try {
                    campaignService.save(category)
                    baseCampaignControllerService.baseControllerService
                            .createRedirectModelAndView(Urls.CAMPAIGN_TAGS)
                } catch (e: Exception) {
                    val errorUrl = WebUtils.createUrl(Urls.EDIT_CATEGORY,
                            WebUtils.Parameters().add("campaignTagId", category.id).add("errorMessage", e.toString()).params)
                    baseCampaignControllerService
                            .baseControllerService.createRedirectModelAndView(errorUrl)
                }
            }

}

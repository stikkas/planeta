package ru.planeta.admin.validation

import org.springframework.stereotype.Component
import org.springframework.validation.Errors
import org.springframework.validation.Validator
import ru.planeta.model.trashcan.PromoConfig

@Component
class PromoConfigValidator : Validator {

    override fun supports(clazz: Class<*>): Boolean {
        return PromoConfig::class.java.isAssignableFrom(clazz)
    }

    override fun validate(target: Any, errors: Errors) {
        val config = target as PromoConfig

        if (config.name == null || config.name.isBlank()) {
            errors.rejectValue("name", "field.required")
        }

        if (config.campaignTags == null || config.campaignTags.isEmpty()) {
            errors.rejectValue("campaignTags", "field.required")
        }

        if (config.mailTemplate == null || config.mailTemplate.isBlank()) {
            errors.rejectValue("mailTemplate", "field.required")
        }

        if (config.priceCondition == null) {
            errors.rejectValue("priceCondition", "field.required")
        }

        if (config.timeStart == null) {
            errors.rejectValue("timeStart", "field.required")
        }

        if (config.timeFinish == null) {
            errors.rejectValue("timeFinish", "field.required")
        }
    }
}

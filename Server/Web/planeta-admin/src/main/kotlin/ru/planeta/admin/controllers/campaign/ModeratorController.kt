package ru.planeta.admin.controllers.campaign

import org.apache.commons.lang3.StringUtils
import org.springframework.context.MessageSource
import org.springframework.stereotype.Controller
import org.springframework.validation.BindingResult
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.ModelAttribute
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.servlet.ModelAndView
import ru.planeta.admin.Actions
import ru.planeta.admin.Urls
import ru.planeta.admin.controllers.services.AdminBaseControllerService
import ru.planeta.api.news.LoggerService
import ru.planeta.api.service.billing.order.BatchCancelerService
import ru.planeta.api.service.campaign.CampaignPermissionService
import ru.planeta.api.service.campaign.CampaignService
import ru.planeta.api.service.campaign.ContractorService
import ru.planeta.api.service.campaign.ModerationMessageService
import ru.planeta.api.service.common.PlanetaManagersService
import ru.planeta.api.service.common.PlanetaManagersServiceImpl
import ru.planeta.api.service.profile.ProfileSubscriptionService
import ru.planeta.api.service.security.PermissionService
import ru.planeta.api.web.authentication.myProfileId
import ru.planeta.api.web.controllers.services.BaseControllerService
import ru.planeta.dao.commondb.SponsorDAO
import ru.planeta.model.common.ContractorForCurrentObject
import ru.planeta.model.common.ModerationMessage
import ru.planeta.model.common.PlanetaCampaignManager
import ru.planeta.model.common.campaign.Campaign
import ru.planeta.model.common.campaign.ShareEditDetails
import ru.planeta.model.common.campaign.enums.CampaignStatus
import ru.planeta.model.enums.ModerationMessageStatus
import ru.planeta.model.news.ProfileNews
import java.util.*
import javax.annotation.PostConstruct

/**
 * Date: 16.09.2015
 * Time: 13:35
 */
@Controller
class ModeratorController(private val sponsorDAO: SponsorDAO,
                          private val messageSource: MessageSource,
                          private val batchCancelerService: BatchCancelerService,
                          private val profileSubscriptionService: ProfileSubscriptionService,
                          private val campaignService: CampaignService,
                          private val profileNewsService: LoggerService,
                          private val moderationMessageService: ModerationMessageService,
                          private val permissionService: PermissionService,
                          private val contractorService: ContractorService,
                          private val planetaManagersService: PlanetaManagersService,
                          private val baseControllerService: BaseControllerService,
                          private val campaignPermissionService: CampaignPermissionService,
                          private val adminBaseControllerService: AdminBaseControllerService) {

    @PostConstruct
    fun init() {
        (planetaManagersService as? PlanetaManagersServiceImpl)?.campaignService = campaignService
    }

    @PostMapping(Urls.MODERATOR_CAMPAIGN_MODERATION)
    fun addCampaignModerationMessage(moderationMessageInfo: ModerationMessage): ModelAndView {
        try {
            val myProfileId = myProfileId()
            permissionService.checkAdministrativeRole(myProfileId)
            moderationMessageInfo.senderId = myProfileId
            if (moderationMessageInfo.campaignStatus == CampaignStatus.PAUSED && StringUtils.isEmpty(moderationMessageInfo.message)) {
                val additionalParams = HashMap<String, Any>()
                moderationMessageInfo.status = ModerationMessageStatus.MODERATOR_MESSAGE
                additionalParams.put("campaignId", moderationMessageInfo.campaignId)
                additionalParams.put("errorMessage", messageSource.getMessage("moderation.message.empty", null, Locale.getDefault()))
                return baseControllerService.createRedirectModelAndView(Urls.MODERATOR_CAMPAIGN_MODERATION_INFO, additionalParams)
            }
            moderationMessageInfo.status = ModerationMessageStatus.MODERATOR_MESSAGE

            campaignService.changeCampaignStatus(moderationMessageInfo.senderId, moderationMessageInfo.campaignId, moderationMessageInfo.campaignStatus)
            moderationMessageService.addModerationMessage(moderationMessageInfo)
            if (CampaignStatus.DECLINED == moderationMessageInfo.campaignStatus) {
                campaignService.sendCampaignDeclinedEmail(myProfileId, moderationMessageInfo.campaignId, moderationMessageInfo.message
                        ?: "")
            }
            return baseControllerService.createRedirectModelAndView("${Urls.MODERATOR_CAMPAIGN_MODERATION_INFO}?campaignId=${moderationMessageInfo.campaignId}")
        } catch (ex: Exception) {
            val additionalParams = HashMap<String, Any>()
            additionalParams.put("errorMessage", baseControllerService.getErrorMessage(ex.message, ex.message
                    ?: "") as Any)
            additionalParams.put("campaignId", moderationMessageInfo.campaignId)
            return baseControllerService.createRedirectModelAndView(Urls.MODERATOR_CAMPAIGN_MODERATION_INFO, additionalParams)
        }
    }

    @PostMapping(Urls.MODERATOR_INTERNAL_MESSAGE)
    fun addCampaignModerationInternalMessage(moderationMessageInfo: ModerationMessage): ModelAndView =
            try {
                val myProfileId = myProfileId()
                permissionService.checkAdministrativeRole(myProfileId)
                moderationMessageInfo.senderId = myProfileId
                moderationMessageInfo.status = ModerationMessageStatus.INTERNAL_MESSAGE
                moderationMessageService.addModerationMessage(moderationMessageInfo)
                baseControllerService.createRedirectModelAndView("${Urls.MODERATOR_CAMPAIGN_MODERATION_INFO}?campaignId=${moderationMessageInfo.campaignId}")
            } catch (ex: Exception) {
                val additionalParams = HashMap<String, Any>()
                additionalParams.put("errorMessage", baseControllerService.getErrorMessage(ex.message, ex.message
                        ?: "") as Any)
                additionalParams.put("campaignId", moderationMessageInfo.campaignId)
                baseControllerService.createRedirectModelAndView(Urls.MODERATOR_CAMPAIGN_MODERATION_INFO, additionalParams)
            }

    @PostMapping(Urls.MODERATOR_REMOVE_CAMPAIGN_ADMIN)
    fun removeCampaignAdmin(adminId: Long, campaignId: Long): ModelAndView =
            try {
                permissionService.checkAdministrativeRole(myProfileId())
                val campaign = campaignService.getCampaignSafe(campaignId)
                profileSubscriptionService.toggleAdmin(adminId, campaign.creatorProfileId, false)
                baseControllerService.createRedirectModelAndView(Urls.MODERATOR_CAMPAIGN_MODERATION_INFO + "?campaignId=" + campaignId)
            } catch (ex: Exception) {
                val additionalParams = HashMap<String, Any>()
                additionalParams.put("errorMessage", baseControllerService.getErrorMessage(ex.message, ex.message
                        ?: "") as Any)
                baseControllerService.createRedirectModelAndView(Urls.MODERATOR_CAMPAIGN_MODERATION_INFO, additionalParams)
            }

    @PostMapping(Urls.MODERATOR_ADD_CAMPAIGN_ADMIN)
    fun addCampaignAdmin(adminId: Long, campaignId: Long): ModelAndView =
            try {
                permissionService.checkAdministrativeRole(myProfileId())
                val campaign = campaignService.getCampaignSafe(campaignId)
                profileSubscriptionService.subscribeSafe(adminId, campaign.creatorProfileId)
                profileSubscriptionService.toggleAdmin(adminId, campaign.creatorProfileId, true)
                baseControllerService.createRedirectModelAndView(Urls.MODERATOR_CAMPAIGN_MODERATION_INFO + "?campaignId=" + campaignId)
            } catch (ex: Exception) {
                val additionalParams = HashMap<String, Any>()
                additionalParams.put("errorMessage", baseControllerService.getErrorMessage(ex.message, ex.message
                        ?: "") as Any)
                baseControllerService.createRedirectModelAndView(Urls.MODERATOR_CAMPAIGN_MODERATION_INFO, additionalParams)
            }

    @PostMapping(Urls.MODERATOR_CAMPAIGN_MANAGER_SELECT)
    fun selectManagerForCampaign(planetaCampaignManager: PlanetaCampaignManager): ModelAndView {
        permissionService.checkAdministrativeRole(myProfileId())
        planetaManagersService.setManagerForCurrentObject(planetaCampaignManager, myProfileId())
        return baseControllerService.createRedirectModelAndView(Urls.MODERATOR_CAMPAIGN_MODERATION_INFO + "?campaignId=" + planetaCampaignManager.campaignId)
    }

    @PostMapping(Urls.MODERATOR_CAMPAIGN_CONTRACTOR_SELECT)
    fun selectContractorForCampaign(contractorForCurrentObject: ContractorForCurrentObject): ModelAndView {
        contractorService.insertRelation(contractorForCurrentObject.contractorId, contractorForCurrentObject.objectId)
        return baseControllerService.createRedirectModelAndView(Urls.MODERATOR_CAMPAIGN_MODERATION_INFO + "?campaignId=" + contractorForCurrentObject.objectId)
    }

    @GetMapping(Urls.MODERATOR_CAMPAIGN_MODERATION_INFO)
    fun getCampaignModerationInfo(@RequestParam campaignId: Long,
                                  @RequestParam(required = false) errorMessage: String?): ModelAndView {

        val modelAndView = adminBaseControllerService.createAdminDefaultModelAndView(Actions.ADMIN_CAMPAIGN_MODERATION_INFO)
        val campaign = campaignService.getCampaignSafe(campaignId)
        val moderationMessageList = moderationMessageService.getAllCampaignModerationMessages(campaign.campaignId, 0, 1000)
        val managers = planetaManagersService.getAllManagers(true)
        val currentManager = planetaManagersService.getCampaignManager(campaignId)

        val curators = campaignService.getCampaignCurators(campaignId)
        val currentContractor = contractorService.selectByCampaignId(campaignId)

        val creator = baseControllerService.profileService.getProfileSafe(campaign.creatorProfileId)
        val campaignAdmins = profileSubscriptionService.getAdminList(campaign.creatorProfileId)

        return modelAndView.addObject("campaign", campaign)
                .addObject("campaignStatus", campaign.status)
                .addObject("moderationMessageList", moderationMessageList)
                .addObject("managersList", managers)
                .addObject("currentManager", currentManager)
                .addObject("isSuperAdmin", permissionService.isSuperAdmin(myProfileId()))
                .addObject("currentContractor", currentContractor)
                .addObject("curatorsList", curators)
                .addObject("creator", creator)
                .addObject("campaignAdmins", campaignAdmins)
                .addObject("isEditable", campaignPermissionService.isEditableByAuthor(campaignId))
                .addObject("errorMessage", errorMessage)
    }

    @PostMapping(Urls.MODERATOR_CAMPAIGN_MODERATION_INFO)
    fun getCampaignModerationInfo(@ModelAttribute("campaign") campaign: Campaign,
                                  result: BindingResult): ModelAndView {
        permissionService.checkAdministrativeRole(myProfileId())
        if (result.hasErrors()) {
            return getCampaignModerationInfo(campaign.campaignId, result.fieldError.defaultMessage)
        }
        val currentCampaign = campaignService.getCampaignSafe(campaign.campaignId)
        currentCampaign.timeStart = campaign.timeStart
        campaignService.saveCampaign(myProfileId(), currentCampaign, true)
        return baseControllerService.createRedirectModelAndView(Urls.MODERATOR_CAMPAIGN_MODERATION_INFO
                + "?campaignId=" + campaign.campaignId)
    }


    @PostMapping(Urls.MODERATOR_CAMPAIGN_INFO)
    fun setCampaignDescription(@RequestParam campaignId: Long,
                               @RequestParam descriptionHtml: String,
                               @RequestParam metaData: String,
                               @RequestParam sponsorAlias: String): ModelAndView {
        val myProfileId = myProfileId()
        campaignService.updateDescriptionAndMetaData(myProfileId, campaignId, descriptionHtml, metaData,
                if ("- не выбрано -".equals(sponsorAlias, ignoreCase = true)) "" else sponsorAlias)
        profileNewsService.addProfileNews(ProfileNews.Type.ADMIN_CHANGE_CAMPAIGN_DESCRIPTION_AND_META_DATA, myProfileId, campaignId, campaignId)
        return getCampaignDescription(campaignId)
    }


    @PostMapping(Urls.MODERATOR_CAMPAIGN_SET_EDITABLE)
    fun setCampaignEditable(@RequestParam campaignId: Long,
                            @RequestParam(defaultValue = "false") editable: Boolean): ModelAndView {
        val campaign = campaignService.getCampaignSafe(campaignId)
        campaignPermissionService.tooglePermission(editable, campaignId, myProfileId())
        return baseControllerService.createRedirectModelAndView(Urls.MODERATOR_CAMPAIGN_MODERATION_INFO + "?campaignId=" + campaign.campaignId)
    }

    @GetMapping(Urls.MODERATOR_CAMPAIGN_INFO)
    fun getCampaignDescription(@RequestParam campaignId: Long): ModelAndView {

        val campaign = campaignService.getCampaignSafe(campaignId)
        for (sd in campaignService.getCampaignDetailedShares(campaignId)) {
            campaign.addShare(ShareEditDetails(sd))
        }
        val sponsors = sponsorDAO.selectAll()

        return adminBaseControllerService.createAdminDefaultModelAndView(Actions.ADMIN_CAMPAIGN_INFO)
                .addObject("campaign", campaign)
                .addObject("sponsors", sponsors)
    }

    @PostMapping(Urls.CANCEL_CAMPAIGN_PURCHASES)
    fun cancelAllCampaignPurchases(@RequestParam campaignId: Long,
                                   @RequestParam(required = false) reason: String?): ModelAndView {
        batchCancelerService.batchCancelCampaignOrders(myProfileId(), campaignId, reason ?: "")
        return baseControllerService.createRedirectModelAndView(Urls.MODERATOR_CAMPAIGN_MODERATION_INFO + "?campaignId=" + campaignId)
    }
}


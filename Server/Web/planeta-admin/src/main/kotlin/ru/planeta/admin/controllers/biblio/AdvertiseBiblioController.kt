package ru.planeta.admin.controllers.biblio

import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.ModelAttribute
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.servlet.ModelAndView
import ru.planeta.admin.Biblio
import ru.planeta.admin.controllers.services.AdvertiseBiblioControllerService
import ru.planeta.admin.controllers.services.BiblioConfigControllerService
import ru.planeta.api.model.biblio.Advertise

/**
 *
 * User: Serge Blagodatskih<stikkas17></stikkas17>@gmail.com><br></br>
 * Date: 13.05.16<br></br>
 * Time: 09:32
 */
@Controller
class AdvertiseBiblioController(private val advertiseBiblioControllerService: AdvertiseBiblioControllerService) {

    @GetMapping(Biblio.ADMIN_ADVERTISE_FILL)
    fun fill(): ModelAndView = advertiseBiblioControllerService.fill()

    @PostMapping(Biblio.ADMIN_ADVERTISE_FILL)
    fun add(@ModelAttribute(BiblioConfigControllerService.CONFIGURATION_ATTR_NAME) advertise: Advertise): ModelAndView =
            advertiseBiblioControllerService.add(advertise)

    @GetMapping(Biblio.ADMIN_ADVERTISE_DELETE)
    fun deleteGet(@RequestParam id: Int): ModelAndView = advertiseBiblioControllerService.deleteGet(id)

    @GetMapping(Biblio.ADMIN_ADVERTISE_EDIT)
    fun editGet(@ModelAttribute(BiblioConfigControllerService.CONFIGURATION_ATTR_NAME) advertise: Advertise): ModelAndView =
            advertiseBiblioControllerService.editGet(advertise)

    @PostMapping(Biblio.ADMIN_ADVERTISE_EDIT)
    fun editPost(@ModelAttribute(BiblioConfigControllerService.CONFIGURATION_ATTR_NAME) advertise: Advertise): ModelAndView =
            advertiseBiblioControllerService.editPost(advertise)

    @GetMapping(Biblio.ADMIN_ADVERTISE_LIST)
    fun list(): ModelAndView = advertiseBiblioControllerService.list()

    @PostMapping(Biblio.ADMIN_ADVERTISE_SORT)
    fun sort(@RequestParam startIndex: Int, @RequestParam stopIndex: Int): ModelAndView = advertiseBiblioControllerService.sort(startIndex, stopIndex)
}


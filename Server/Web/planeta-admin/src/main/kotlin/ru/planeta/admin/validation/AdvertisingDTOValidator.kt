package ru.planeta.admin.validation

import org.springframework.stereotype.Component
import org.springframework.validation.Errors
import org.springframework.validation.ValidationUtils
import org.springframework.validation.Validator
import ru.planeta.model.advertising.AdvertisingDTO

/**
 * Created with IntelliJ IDEA.
 * Date: 24.01.14
 * Time: 13:09
 */
@Component
class AdvertisingDTOValidator : Validator {

    override fun supports(clazz: Class<*>): Boolean {
        return clazz.isAssignableFrom(AdvertisingDTO::class.java)
    }

    override fun validate(target: Any, errors: Errors) {
        ValidationUtils.rejectIfEmpty(errors, "buttonText", "field.required")
    }
}

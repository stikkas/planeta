package ru.planeta.admin.controllers.investing

import org.apache.commons.lang3.time.DateUtils
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.ModelAttribute
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.servlet.ModelAndView
import ru.planeta.admin.Actions
import ru.planeta.admin.Investing
import ru.planeta.admin.controllers.services.AdminBaseControllerService
import ru.planeta.admin.utils.DateUtils.Interval
import ru.planeta.admin.utils.DateUtils.setAllTimeAsDate
import ru.planeta.api.search.SearchService
import ru.planeta.api.service.billing.order.OrderService
import ru.planeta.api.service.billing.payment.PaymentService
import ru.planeta.api.service.common.InvestingOrderInfoService
import ru.planeta.api.service.profile.ProfileService
import ru.planeta.api.web.authentication.myProfileId
import ru.planeta.commons.model.Gender
import ru.planeta.model.common.InvestingOrderInfo
import ru.planeta.model.enums.ContractorType
import ru.planeta.model.enums.ModerateStatus
import ru.planeta.model.enums.OrderObjectType
import java.util.*

@Controller
class InvestingController(private val investingOrderInfoService: InvestingOrderInfoService,
                          private val paymentService: PaymentService,
                          private val orderService: OrderService,
                          private val searchService: SearchService,
                          private val adminBaseControllerService: AdminBaseControllerService,
                          private val profileService: ProfileService) {

    @GetMapping(Investing.INVESTING_ORDERS_INFO)
    fun getInvestingOrdersInfo(@RequestParam(required = false) searchStr: String?,
                               @RequestParam(required = false) dateFrom: Date?,
                               @RequestParam(required = false) dateTo: Date?,
                               @RequestParam(required = false) status: ModerateStatus?,
                               @RequestParam(defaultValue = "20") limit: Int,
                               @RequestParam(defaultValue = "0") offset: Int): ModelAndView {
        val modelAndView = adminBaseControllerService.createAdminDefaultModelAndView(Actions.ADMIN_INVESTING_ORDERS_INFO)

        val datepickerInterval = setAllTimeAsDate(dateFrom, dateTo, modelAndView)
        val interval = Interval(datepickerInterval.dateFrom, DateUtils.addDays(datepickerInterval.dateTo, 1))
        val invList = searchService.searchForInvestingOrderInfo(searchStr, status, interval.dateFrom, interval.dateTo, offset, limit)
        val invAdminList = ArrayList<InvestingOrderAdminInfo>()
        for (investOrder in invList.searchResultRecords) {
            val adminInfo = InvestingOrderAdminInfo()
            adminInfo.userType = investOrder.userType
            adminInfo.fio = investOrder.lastName + " " + investOrder.firstName + " " + investOrder.middleName
            adminInfo.orgName = investOrder.orgName
            adminInfo.investingOrderInfoId = investOrder.investingOrderInfoId
            adminInfo.phone = investOrder.phoneNumber
            adminInfo.status = investOrder.moderateStatus

            val order = orderService.getOrderSafe(myProfileId(), investOrder.investingOrderInfoId)
            if (order.orderType != OrderObjectType.BIBLIO) {
                adminInfo.campaign = orderService.getCampaignByOrderId(order.orderId)
            }
            adminInfo.transaction = paymentService.getPayment(order.topayTransactionId)
            adminInfo.displayName = profileService.getProfileWithDeleted(order.buyerId).displayName
//            adminInfo.userInfo = authorizationService.getUserPrivateInfoById(order.buyerId)

            invAdminList.add(adminInfo)
        }

        return modelAndView
                .addObject("offset", offset)
                .addObject("limit", limit)
                .addObject("searchStr", searchStr)
                .addObject("investingOrdersInfo", invAdminList)
                .addObject("count", invList.estimatedCount)
                .addObject("statuses", ModerateStatus.values())
    }

    @GetMapping(Investing.INVESTING_ORDER_INFO)
    fun getInvestingOrderInfo(@RequestParam orderId: Long): ModelAndView {
        val modelAndView = adminBaseControllerService.createAdminDefaultModelAndView(Actions.ADMIN_INVESTING_ORDER_INFO)

        val order = orderService.getOrderSafe(myProfileId(), orderId)

        if (order.orderType != OrderObjectType.BIBLIO) {
            val campaign = orderService.getCampaignByOrderId(orderId)
            modelAndView
                    .addObject("campaignId", campaign.campaignId)
                    .addObject("campaignName", campaign.name)
        }

        return modelAndView
                .addObject("investingOrderInfo", investingOrderInfoService.select(orderId))
                .addObject("userTypes", ContractorType.values())
                .addObject("genders", Gender.values())
                .addObject("profileId", order.buyerId)
                .addObject("transactionId", order.topayTransactionId)
    }

    @PostMapping(Investing.INVESTING_ORDER_INFO)
    fun saveInvestingOrderInfo(@ModelAttribute investiongOrderInfo: InvestingOrderInfo): ModelAndView {
        investiongOrderInfo.investingOrderInfoId = investiongOrderInfo.investingOrderInfoId
        investingOrderInfoService.save(investiongOrderInfo)
        return adminBaseControllerService.baseControllerService.createRedirectModelAndView(Investing.INVESTING_ORDERS_INFO)
    }

    @GetMapping(Investing.INVESTING_ORDER_INFO_MODERATE)
    fun moderateInvestingOrderInfo(@RequestParam orderId: Long,
                                   @RequestParam status: ModerateStatus): ModelAndView {
        investingOrderInfoService.changeModerateStatus(orderId, status)
        return adminBaseControllerService.baseControllerService.createRedirectModelAndView(Investing.INVESTING_ORDERS_INFO)
    }
}

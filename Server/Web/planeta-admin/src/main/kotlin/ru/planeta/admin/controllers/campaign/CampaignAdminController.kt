package ru.planeta.admin.controllers.campaign

import org.apache.commons.collections4.CollectionUtils
import org.apache.commons.lang3.math.NumberUtils
import org.apache.poi.ss.usermodel.WorkbookFactory
import org.springframework.context.MessageSource
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.*
import org.springframework.web.multipart.MultipartFile
import org.springframework.web.multipart.commons.CommonsMultipartFile
import org.springframework.web.servlet.ModelAndView
import ru.planeta.admin.Actions
import ru.planeta.admin.Urls
import ru.planeta.admin.controllers.services.BaseCampaignControllerService
import ru.planeta.admin.utils.DateUtils
import ru.planeta.admin.utils.DateUtils.setIntervalTodayAsDate
import ru.planeta.api.Utils.map
import ru.planeta.api.search.Range
import ru.planeta.api.search.SearchService
import ru.planeta.api.search.SearchServiceImpl
import ru.planeta.api.search.SortOrder
import ru.planeta.api.search.filters.SearchOrderFilter
import ru.planeta.api.service.billing.order.OrderService
import ru.planeta.api.service.billing.payment.settings.PaymentSettingsService
import ru.planeta.api.service.campaign.CampaignService
import ru.planeta.api.service.geo.GeoService
import ru.planeta.api.utils.DocumentUtils
import ru.planeta.api.web.authentication.myProfileId
import ru.planeta.dao.commondb.TopayTransactionDAO
import ru.planeta.dao.commondb.CampaignDAO
import ru.planeta.model.common.DeliveryAddress
import ru.planeta.model.common.OrderInfo
import ru.planeta.model.common.campaign.Campaign
import ru.planeta.model.common.campaign.enums.CampaignStatus
import ru.planeta.model.enums.OrderObjectType
import ru.planeta.model.enums.ProjectType
import ru.planeta.model.profile.location.LocationType
import ru.planeta.model.shop.enums.DeliveryStatus
import ru.planeta.reports.ReportType
import java.text.SimpleDateFormat
import java.util.*
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

/**
 * Date: 16.09.2015
 * Time: 13:37
 */
@Controller
class CampaignAdminController(private val orderService: OrderService,
                              private val campaignDAO: CampaignDAO,
                              private val transactionDAO: TopayTransactionDAO,
                              private val paymentSettingsService: PaymentSettingsService,
                              private val campaignService: CampaignService,
                              private val geoService: GeoService,
                              private val searchService: SearchService,
                              private val baseCampaignControllerService: BaseCampaignControllerService,
                              private val messageSource: MessageSource) {

    @GetMapping(Urls.ADMIN_CAMPAIGNS)
    fun getCampaigns(@RequestParam(defaultValue = "0") offset: Int,
                     @RequestParam(defaultValue = "10") limit: Int,
                     @RequestParam(required = false) searchString: String?,
                     @RequestParam(required = false) campaignStatus: CampaignStatus?,
                     @RequestParam(required = false) managerId: Long?,
                     @RequestParam(required = false) contractorId: Long?,
                     @RequestParam(required = false) dateFrom: Date?,
                     @RequestParam(required = false) dateTo: Date?,
                     @RequestParam(required = false) sortOrderList: List<SortOrder>?,
                     @RequestParam(required = false) campaignTagId: Int?,
                     @RequestParam(defaultValue = "true") exactlySearch: Boolean): ModelAndView {

        val modelAndView = baseCampaignControllerService.createAdminDefaultModelAndView(Actions.ADMIN_CAMPAIGNS)

        val campaignId = NumberUtils.toLong(searchString, 0)
        val campaigns: List<Campaign>
        var count: Long = 1
        var campaign: Campaign? = null
        if (campaignId > 0) {
            campaign = campaignService.getCampaign(campaignId)
        } else if (searchString != null && !searchString.contains(" ")) {
            campaign = campaignService.getCampaign(searchString)
        }
        val sortOrderList = if (CollectionUtils.isEmpty(sortOrderList))
            listOf(SortOrder.SORT_BY_TIME_STARTED_DESC, SortOrder.SORT_BY_TIME_ADDED_DESC)
        else sortOrderList

        val status = campaignStatus?:CampaignStatus.NOT_STARTED;

        var dateFrom2 = dateFrom
        if (dateFrom2 == null) {
            val formatter = SimpleDateFormat("dd/MM/yyyy")
            dateFrom2 = formatter.parse("01/01/2010")

        }
        var dateTo2 = dateTo;
        if (dateTo2 == null) {
            val formatter = SimpleDateFormat("dd/MM/yyyy")
            dateTo2 = formatter.parse("01/01/2100")

        }
        modelAndView.addObject("dateFrom", dateFrom2)
        modelAndView.addObject("dateTo", dateTo2)

        if (campaign == null) {
            if (exactlySearch) {
                // database ilike %query% search
                val campaignStatuses: EnumSet<CampaignStatus>
                if (status == CampaignStatus.ALL) {
                    campaignStatuses = EnumSet.of(CampaignStatus.APPROVED, CampaignStatus.ACTIVE, CampaignStatus.FINISHED, CampaignStatus.DRAFT,
                            CampaignStatus.PAUSED, CampaignStatus.PATCH, CampaignStatus.NOT_STARTED, CampaignStatus.DECLINED)
                } else {
                    campaignStatuses = EnumSet.of(status)
                }

                val stringOrderBy: String = SearchServiceImpl.getStringSortBy(sortOrderList)
                campaigns = campaignService.getCampaignsForAdminSearch(myProfileId(), searchString,
                        offset, limit, managerId, dateFrom, dateTo, campaignStatuses,
                        campaignTagId, stringOrderBy)
                count = campaignService.getCampaignsForAdminSearchCount(myProfileId(), searchString,
                        managerId, dateFrom, dateTo, campaignStatuses, campaignTagId).toLong()
            } else {
                // sphinx search

                val interval = setIntervalTodayAsDate(dateFrom2, dateTo, modelAndView)
                val timeAddedRange = Range<Date>(interval.dateFrom, interval.dateTo)
                val result = searchService.searchForCampaignsAdmins(myProfileId(), searchString,
                        offset, limit, status, managerId, timeAddedRange, sortOrderList, campaignTagId)
                campaigns = result.searchResultRecords
                count = result.estimatedCount
            }


        } else {
            campaigns = listOf(campaign)
        }

        val availableSearchStatuses = EnumSet.allOf(CampaignStatus::class.java)
        availableSearchStatuses.remove(CampaignStatus.DELETED)
        return modelAndView.addObject("campaigns", campaigns)
                .addObject("offset", offset)
                .addObject("limit", limit)
                .addObject("count", count)
                .addObject("searchString", searchString)
                .addObject("campaignStatusCode", status.code)
                .addObject("campaignStatusList", availableSearchStatuses)
                .addObject("campaignTagId", campaignTagId)
                .addObject("campaignTagsList", campaignService.allTags)
                .addObject("managerId", managerId)
                .addObject("contractorId", contractorId)
                .addObject("managersList", baseCampaignControllerService.planetaManagersService.getAllManagers(null))
                .addObject("sortOrderList", sortOrderList)
    }

    @GetMapping(Urls.ADMIN_MANAGER_CAMPAIGNS)
    fun getManagerCampaigns(@RequestParam(defaultValue = "0") offset: Int,
                            @RequestParam(defaultValue = "10") limit: Int): ModelAndView {

        val modelAndView = baseCampaignControllerService.createAdminDefaultModelAndView(Actions.ADMIN_MANAGER_CAMPAIGNS)
        val campaigns = campaignDAO.selectCampaignsByStatusAndManagerId(EnumSet.of(CampaignStatus.PATCH, CampaignStatus.NOT_STARTED),
                baseCampaignControllerService.planetaManagersService.getManagerId(myProfileId()), offset, limit)

        return modelAndView.addObject("campaigns", campaigns)
                .addObject("offset", offset)
                .addObject("limit", limit)
                .addObject("count", if (campaigns.isEmpty()) offset else 100500)
    }

    @ResponseBody
    @RequestMapping(value = [(Urls.ADMIN_CAMPAIGNS_REPORT)])
    fun getCampaignsReport(response: HttpServletResponse,
                           @RequestParam(required = false) searchString: String?,
                           @RequestParam(required = false) campaignStatus: CampaignStatus?,
                           @RequestParam(required = false) managerId: Long?,
                           @RequestParam(required = false) contractorId: Long?,
                           @RequestParam(required = false) dateFrom: Date?,
                           @RequestParam(required = false) dateTo: Date?,
                           @RequestParam(required = false) sortOrderList: List<SortOrder>?,
                           @RequestParam(required = false) campaignTagId: Int?,
                           @RequestParam(defaultValue = "CSV") reportType: ReportType) {
        val modelAndView = getCampaigns(0, MAX_LIMIT, searchString, campaignStatus, managerId, contractorId, dateFrom, dateTo, sortOrderList, campaignTagId, true)
        val campaigns = modelAndView.model["campaigns"] as List<Campaign>
        val report = reportType.createReport("campaigns_report_${myProfileId()}")

        report.addCaptionRow(*messageSource.getMessage("admin.campaigns.report", null,
                Locale.getDefault()).split(";".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray())
        for (campaign in campaigns) {
            report.addRow(campaign.campaignId, campaign.campaignAlias, campaign.name, campaign.description,
                    campaign.status, campaign.timeAdded, campaign.timeStart, campaign.timeFinish)
        }
        report.addToResponse(response)
    }

    @PostMapping(Urls.ADMIN_GEO_ADD_CITY)
    fun addCity(@RequestParam nameRu: String, @RequestParam regionId: Int): ModelAndView {
        if (!nameRu.isEmpty()) {
            geoService.addNewCity(nameRu, regionId)
        }
        return getGeoManagerView(regionId, nameRu)
    }

    @GetMapping(Urls.ADMIN_GEO_ADD_CITY)
    fun manageCityes(@RequestParam(required = false) nameRu: String?,
                     @RequestParam(defaultValue = "0") regionId: Int): ModelAndView {
        return getGeoManagerView(regionId, nameRu)
    }

    private fun getGeoManagerView(regionId: Int, nameRu: String?): ModelAndView {
        val modelAndView = baseCampaignControllerService.createAdminDefaultModelAndView(Actions.ADMIN_GEO_MANAGE_CITIES)
                .addObject("regions", geoService.getNestedLocations(1, LocationType.COUNTRY))
                .addObject("regionId", regionId)
                .addObject("nameRu", nameRu)
        if (regionId > 0 && nameRu != null && !nameRu.isEmpty()) {
            modelAndView.addObject("cities", geoService.getRegionCitiesBySubstring(regionId.toLong(), nameRu, 0, 1000))
        }
        return modelAndView
    }

    @GetMapping(Urls.ADMIN_ORDERS)
    fun getOrders(searchOrderFilter: SearchOrderFilter): ModelAndView {

        val clientId = myProfileId()
        if (!baseCampaignControllerService.permissionService.hasAdministrativeRole(clientId)) {
            return baseCampaignControllerService.baseControllerService.createRedirectModelAndView(Urls.ADMIN_INDEX)
        }
        if (searchOrderFilter.limit == 0) {
            searchOrderFilter.limit = 20
        }

        val modelAndView = baseCampaignControllerService.createAdminDefaultModelAndView(Actions.ADMIN_SHOP_ORDERS)

        val interval = DateUtils.setAllTimeAsDate(searchOrderFilter.dateFrom, searchOrderFilter.dateTo, modelAndView)
        searchOrderFilter.dateFrom = interval.dateFrom
        searchOrderFilter.dateTo = interval.dateTo
        val orderInfoSearchResult = searchService.searchForOrders(searchOrderFilter)

        return modelAndView
                .addObject("ordersInfo", orderInfoSearchResult.searchResultRecords)
                .addObject("validTransitions", DeliveryStatus.getValidTransitions())
                .addObject("offset", searchOrderFilter.offset)
                .addObject("limit", searchOrderFilter.limit)
                .addObject("count", orderInfoSearchResult.estimatedCount)
                .addObject("onlyProducts", false)
    }


    @GetMapping(Urls.ADMIN_ORDER_ONE)
    fun getOrder(@PathVariable("orderId") orderId: Long): ModelAndView {
        val modelAndView = baseCampaignControllerService.createAdminDefaultModelAndView(Actions.ADMIN_ONE_ORDER)
        val orderInfo = orderService.getOrderInfo(myProfileId(), orderId)

        if (orderInfo.orderType == OrderObjectType.SHARE) {
            val campaign = orderService.getCampaignByOrderId(orderId)
            val shareId = orderInfo.orderObjectsInfo[0].objectId
            orderInfo.questionToBuyer = campaignService.getShareSafe(shareId).questionToBuyer
            modelAndView.addObject("campaign", campaign)
        }

        if (orderInfo.topayTransactionId > 0) {
            val transaction = transactionDAO.select(orderInfo.topayTransactionId)
            val methods = paymentSettingsService.allPaymentMethods
            val providers = paymentSettingsService.allProviders
            modelAndView.addObject("transaction", transaction)
                    .addObject("methods", map(methods))
                    .addObject("providers", map(providers))
        }

        return modelAndView
                .addObject("orderInfo", orderInfo)
                .addObject("validTransitions", DeliveryStatus.getValidTransitions())
    }

    @GetMapping(Urls.ADMIN_ADDRESSES_STICKERS)
    fun getCampaignAddressesStickers(searchOrderFilter: SearchOrderFilter): ModelAndView {
        val clientId = myProfileId()
        if (!baseCampaignControllerService.permissionService.hasAdministrativeRole(clientId)) {
            return baseCampaignControllerService.baseControllerService.createRedirectModelAndView(Urls.ADMIN_INDEX)
        }
        if (searchOrderFilter.limit == 0) {
            searchOrderFilter.limit = 1000
        }

        val modelAndView = baseCampaignControllerService.createAdminDefaultModelAndView(Actions.ADMIN_CAMPAIGN_ADDRESSES_STICKERS)

        val interval = DateUtils.setAllTimeAsDate(searchOrderFilter.dateFrom, searchOrderFilter.dateTo, modelAndView)
        searchOrderFilter.dateFrom = interval.dateFrom
        searchOrderFilter.dateTo = interval.dateTo
        val orderInfoSearchResult = searchService.searchForCampaignOrders(searchOrderFilter)
        val searchResultRecords = orderInfoSearchResult.searchResultRecords
        CollectionUtils.filter(searchResultRecords) { orderInfo -> orderInfo.deliveryAddress.id != 0L }

        return getAdminAddressStickers(modelAndView, searchResultRecords)
    }

    private fun getAdminAddressStickers(modelAndView: ModelAndView, searchResultRecords: List<OrderInfo>): ModelAndView =
            modelAndView
                    .addObject("ordersInfo", searchResultRecords)
                    .addObject("count", searchResultRecords.size)
                    .addObject("restCount", searchResultRecords.size % 10)
                    .addObject("limitPerPage", 10)

    @GetMapping(Urls.ADMIN_ADDRESSES_STICKERS_FROM_FILE)
    fun getCampaignAddressesStickersFromFile(@RequestParam(defaultValue = "") errorMessage: String): ModelAndView {
        val clientId = myProfileId()
        if (!baseCampaignControllerService.permissionService.hasAdministrativeRole(clientId)) {
            return baseCampaignControllerService.baseControllerService.createRedirectModelAndView(Urls.ADMIN_INDEX)
        }
        return baseCampaignControllerService.baseControllerService.createDefaultModelAndView(Actions.ADMIN_CAMPAIGN_ADDRESSES_STICKERS_FROM_FILE, ProjectType.ADMIN)
                .addObject("errorMessage", errorMessage)
    }

    @PostMapping(Urls.ADMIN_ADDRESSES_STICKERS_FROM_FILE)
    fun getCampaignAddressesStickersFromFile(@RequestParam file: MultipartFile,
                                             request: HttpServletRequest, response: HttpServletResponse): ModelAndView {
        val clientId = myProfileId()
        if (!baseCampaignControllerService.permissionService.hasAdministrativeRole(clientId)) {
            return baseCampaignControllerService.baseControllerService.createRedirectModelAndView(Urls.ADMIN_INDEX)
        }

        val modelAndView = baseCampaignControllerService.createAdminDefaultModelAndView(Actions.ADMIN_CAMPAIGN_ADDRESSES_STICKERS)

        val orderInfoList = ArrayList<OrderInfo>()

        file.inputStream.use { inp ->
            val wb = WorkbookFactory.create(inp) ?:
                    return getCampaignAddressesStickersFromFile("Cat'n create inputStream from chosen document [" +
                            (file as CommonsMultipartFile).fileItem.name + "]. Are you sure the Excel document is right?")
            val dataSheetNumber = 0
            val sheet = wb.getSheetAt(dataSheetNumber) ?:
                    return getCampaignAddressesStickersFromFile("Can't open the sheet number " + dataSheetNumber +
                            " from [File: " + (file as CommonsMultipartFile).fileItem.name + "]. Are you sure the sheet is exists?")

            try {
                val rowsCount = sheet.lastRowNum
                for (i in 0..rowsCount) {
                    val orderInfo = OrderInfo()
                    val deliveryAddress = DeliveryAddress()

                    val row = sheet.getRow(i) ?: continue

                    deliveryAddress.fio = DocumentUtils.getCellValueString(row.getCell(0))
                    deliveryAddress.country = DocumentUtils.getCellValueString(row.getCell(1))
                    deliveryAddress.city = DocumentUtils.getCellValueString(row.getCell(2))
                    deliveryAddress.address = DocumentUtils.getCellValueString(row.getCell(3))
                    deliveryAddress.zipCode = DocumentUtils.getCellValueString(row.getCell(4))

                    orderInfo.deliveryAddress = deliveryAddress
                    orderInfoList.add(orderInfo)
                }
            } catch (ex: Exception) {
                return getCampaignAddressesStickersFromFile("Something went wrong with reading data from ["
                        + (file as CommonsMultipartFile).fileItem.name + " : sheet number " + dataSheetNumber + "]. "
                        + "[FOR DEVELOPERS: \n\n" + ex.toString() + "]")
            }
        }

        return getAdminAddressStickers(modelAndView, orderInfoList)
    }

    companion object {
        private const val MAX_LIMIT = 1000
    }
}

package ru.planeta.admin.controllers

import org.apache.commons.lang3.time.DateUtils
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.ResponseBody
import org.springframework.web.servlet.ModelAndView
import ru.planeta.admin.Actions
import ru.planeta.admin.UserCallback
import ru.planeta.admin.controllers.services.AdminBaseControllerService
import ru.planeta.admin.utils.DateUtils.Interval
import ru.planeta.admin.utils.DateUtils.setAllTimeAsDate
import ru.planeta.api.model.ActionStatus
import ru.planeta.api.model.json.UserCallbackInfo
import ru.planeta.api.search.SearchService
import ru.planeta.api.service.common.UserCallbackService
import ru.planeta.api.web.authentication.myProfileId
import java.util.*

/**
 * Created by eshevchenko.
 */
@Controller
class CallbackController(private val callbackService: UserCallbackService,
                         private val searchService: SearchService,
                         private val adminBaseControllerService: AdminBaseControllerService) {

    @GetMapping(UserCallback.MODERATOR_USER_CALLBACKS)
    fun getUserCallbacks(@RequestParam(required = false) searchStr: String?,
                         @RequestParam(defaultValue = "0") processed: Long,
                         @RequestParam(required = false) dateFrom: Date?,
                         @RequestParam(required = false) dateTo: Date?,
                         @RequestParam(defaultValue = "10") limit: Int,
                         @RequestParam(defaultValue = "0") offset: Int): ModelAndView {
        val modelAndView = adminBaseControllerService.createAdminDefaultModelAndView(Actions.MODERATOR_USER_CALLBACKS)

        val datepickerInterval = setAllTimeAsDate(dateFrom, dateTo, modelAndView)
        val interval = Interval(datepickerInterval.dateFrom, DateUtils.addDays(datepickerInterval.dateTo, 1))

        val callbacks = searchService.searchForUserCallback(myProfileId(), searchStr,
                if (processed == -1L) null else processed > 0, interval.dateFrom, interval.dateTo, offset, limit)
        return modelAndView
                .addObject("callbacks", callbacks.searchResultRecords)
                .addObject("count", callbacks.estimatedCount)
                .addObject("offset", offset)
                .addObject("limit", limit)
                .addObject("searchStr", searchStr)
    }

    @GetMapping(UserCallback.MODERATOR_UNPROCESSED_CALLBACKS)
    @ResponseBody
    fun getUnprocessedUserCallbacks(@RequestParam(required = false) dateFrom: Date?,
                                    @RequestParam(required = false) dateTo: Date?,
                                    @RequestParam(defaultValue = "20") limit: Int,
                                    @RequestParam(defaultValue = "0") offset: Int): ActionStatus<List<UserCallbackInfo>> =
            ActionStatus.createSuccessStatus(callbackService.getUnprocessedCallbacks(myProfileId(), null, dateFrom, dateTo, offset, limit))

    @GetMapping(UserCallback.MODERATOR_PROCESSED_CALLBACKS)
    @ResponseBody
    fun getProcessedUserCallbacks(@RequestParam(required = false) dateFrom: Date?,
                                  @RequestParam(required = false) dateTo: Date?,
                                  @RequestParam(defaultValue = "20") limit: Int,
                                  @RequestParam(defaultValue = "0") offset: Int): ActionStatus<List<UserCallbackInfo>> =
            ActionStatus.createSuccessStatus(callbackService.getProcessedCallbacks(myProfileId(), null, dateFrom, dateTo, offset, limit))

    @GetMapping(UserCallback.MODERATOR_USER_CALLBACK_COMMENT)
    fun getCommentPage(@RequestParam callbackId: Long): ModelAndView {
        val myProfileId = myProfileId()
        return adminBaseControllerService.createAdminDefaultModelAndView(Actions.MODERATOR_USER_CALLBACK_COMMENTS)
                .addObject("callback", callbackService.wrapCallback(callbackService.getCallback(myProfileId, callbackId)))
                .addObject("comments", callbackService.getCallbackComments(myProfileId, callbackId))
    }

    @PostMapping(UserCallback.MODERATOR_USER_CALLBACK_COMMENT)
    fun postCommentPage(@RequestParam callbackId: Long, @RequestParam text: String): ModelAndView {
        callbackService.addCallbackComment(myProfileId(), callbackId, text)
        return getCommentPage(callbackId)
    }
}


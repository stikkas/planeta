package ru.planeta.admin.controllers.campaign

import org.springframework.context.MessageSource
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.ResponseBody
import org.springframework.web.servlet.ModelAndView
import ru.planeta.admin.Actions
import ru.planeta.admin.Urls
import ru.planeta.admin.controllers.services.BaseCampaignControllerService
import ru.planeta.api.service.campaign.CampaignService
import ru.planeta.api.service.security.PermissionService
import ru.planeta.api.web.authentication.myProfileId
import ru.planeta.model.common.PlanetaManager
import ru.planeta.model.common.campaign.CampaignNews
import ru.planeta.model.common.campaign.param.CampaignEventsParam
import ru.planeta.model.enums.ObjectType
import java.util.*
import javax.servlet.http.HttpServletResponse


/**
 * Created with IntelliJ IDEA.
 * Date: 16.09.2015
 * Time: 13:40
 * To change this template use File | Settings | File Templates.
 */
@Controller("CampaignSocialController")
class SocialController(private val campaignService: CampaignService,
                       private val messageSource: MessageSource,
                       private val permissionService: PermissionService,
                       private val baseCampaignControllerService: BaseCampaignControllerService) {

    @ResponseBody
    @RequestMapping(Urls.CAMPAIGN_NEWS_REPORT)
    fun getCampaignNewsReport(response: HttpServletResponse, campaignEventsParam: ReportCampaignEventsParam) {
        campaignEventsParam.offset = 0
        campaignEventsParam.limit = 0
        val modelAndView = getCampaignNews(campaignEventsParam)

        val campaignNewsList = modelAndView.model["campaignNewsList"] as List<CampaignNews>
        val managersForCurrentCampaigns = modelAndView.model["campaignManagerList"] as List<PlanetaManager>


        val report = campaignEventsParam.reportType.createReport("campaign_news_report_${myProfileId()}")
        report.addCaptionRow(*messageSource.getMessage("campaign.news.report", null, Locale.getDefault()).split(";".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray())

        for (i in campaignNewsList.indices) {
            val campaignNews = campaignNewsList[i]
            val planetaManager = managersForCurrentCampaigns[i]

            report.addRow(campaignNews.campaign.campaignId, campaignNews.campaign.name, campaignNews.profileNews.type, campaignNews.profileNews.timeAdded, if (planetaManager == null) "" else planetaManager.fullName
            )
        }

        report.addToResponse(response)
    }

    @GetMapping(Urls.CAMPAIGN_POSTS)
    fun getCampaignPosts(campaignEventsParam: CampaignEventsParam): ModelAndView {
        if (!permissionService.hasAdministrativeRole(myProfileId())) {
            return baseCampaignControllerService.baseControllerService.createRedirectModelAndView(Urls.ADMIN_INDEX)
        }

        val modelAndView = baseCampaignControllerService.createCampaignEventsModelAndView(Actions.ADMIN_CAMPAIGN_POSTS, campaignEventsParam)
        val campaignNewsList = campaignService.selectCampaignPosts(campaignEventsParam)
        baseCampaignControllerService.addCampaignNewsListToModel(campaignNewsList, modelAndView)

        return modelAndView
                .addObject("tab", "campaign-news")
                .addObject("pageTitle", messageSource.getMessage("campaign.news", null, Locale.getDefault()))
                .addObject("reportJson", Urls.CAMPAIGN_POSTS_REPORT)
    }

    @ResponseBody
    @RequestMapping(Urls.CAMPAIGN_POSTS_REPORT)
    fun getCampaignPostsReport(response: HttpServletResponse, campaignEventsParam: ReportCampaignEventsParam) {
        permissionService.checkAdministrativeRole(myProfileId())
        campaignEventsParam.limit = 0
        campaignEventsParam.offset = 0

        val modelAndView = getCampaignPosts(campaignEventsParam)
        val campaignNewsList = modelAndView.model["campaignNewsList"] as List<CampaignNews>
        val managersForCurrentCampaigns = modelAndView.model["campaignManagerList"] as List<PlanetaManager>

        val report = campaignEventsParam.reportType.createReport("campaign_posts_report_${myProfileId()}")
        report.addCaptionRow(*messageSource.getMessage("campaign.posts.report", null, Locale.getDefault()).split(";".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray())

        for (i in campaignNewsList.indices) {
            val campaignNews = campaignNewsList[i]

            val planetaManager = managersForCurrentCampaigns[i]
            report.addRow(campaignNews.campaign.campaignId, campaignNews.campaign.name, campaignNews.post.id, campaignNews.post.timeAdded, if (planetaManager == null) "" else planetaManager.fullName
            )
        }

        report.addToResponse(response)
    }


    @GetMapping(Urls.CAMPAIGN_COMMENTS)
    fun getCampaignComments(campaignEventsParam: CampaignEventsParam): ModelAndView {
        if (!permissionService.hasAdministrativeRole(myProfileId())) {
            return baseCampaignControllerService.baseControllerService.createRedirectModelAndView(Urls.ADMIN_INDEX)
        }

        val modelAndView = baseCampaignControllerService.createCampaignEventsModelAndView(Actions.ADMIN_CAMPAIGN_COMMENTS, campaignEventsParam)
        val campaignNewsList = campaignService.selectCampaignComments(campaignEventsParam)
        baseCampaignControllerService.addCampaignNewsListToModel(campaignNewsList, modelAndView)

        return modelAndView
                .addObject("tab", "campaign-comments")
                .addObject("pageTitle", messageSource.getMessage("campaign.comments", null, Locale.getDefault()))
                .addObject("commentsType", campaignEventsParam.commentsType)
                .addObject("reportJson", Urls.CAMPAIGN_COMMENTS_REPORT)
    }

    @ResponseBody
    @RequestMapping(Urls.CAMPAIGN_COMMENTS_REPORT)
    fun getCampaignCommentsReport(response: HttpServletResponse, campaignEventsParam: ReportCampaignEventsParam) {
        campaignEventsParam.limit = 0
        campaignEventsParam.offset = 0
        val modelAndView = getCampaignComments(campaignEventsParam)

        val campaignNewsList = modelAndView.model["campaignNewsList"] as List<CampaignNews>
        val managersForCurrentCampaigns = modelAndView.model["campaignManagerList"] as List<PlanetaManager>

        val report = campaignEventsParam.reportType.createReport("campaign_comments_report_${myProfileId()}")
        report.addCaptionRow(*messageSource.getMessage("campaign.comments.report", null, Locale.getDefault()).split(";".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray())

        for (i in campaignNewsList.indices) {
            val campaignNews = campaignNewsList[i]
            val planetaManager = managersForCurrentCampaigns[i]
            report.addRow(campaignNews.campaign.campaignId, campaignNews.campaign.name,
                    if (campaignNews.comment.objectType == ObjectType.POST) "К новости" else "К проекту",
                    campaignNews.comment.id, campaignNews.comment.textHtml, campaignNews.comment.timeAdded,
                    if (planetaManager == null) "" else planetaManager.fullName
            )
        }

        report.addToResponse(response)
    }

    @GetMapping(Urls.CAMPAIGN_NEWS)
    fun getCampaignNews(campaignEventsParam: CampaignEventsParam): ModelAndView {

        if (!permissionService.hasAdministrativeRole(myProfileId())) {
            return baseCampaignControllerService.baseControllerService.createRedirectModelAndView(Urls.ADMIN_INDEX)
        }

        val modelAndView = baseCampaignControllerService.createCampaignEventsModelAndView(Actions.ADMIN_CAMPAIGN_NEWS, campaignEventsParam)

        val campaignNewsList = campaignService.selectCampaignNews(campaignEventsParam)
        baseCampaignControllerService.addCampaignNewsListToModel(campaignNewsList, modelAndView)
        return modelAndView
                .addObject("tab", "campaign-events")
                .addObject("pageTitle", messageSource.getMessage("campaign.events", null, Locale.getDefault()))
                .addObject("reportJson", Urls.CAMPAIGN_NEWS_REPORT)
    }
}

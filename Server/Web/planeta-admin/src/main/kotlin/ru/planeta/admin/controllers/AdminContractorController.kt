package ru.planeta.admin.controllers

import org.springframework.stereotype.Controller
import org.springframework.validation.BindingResult
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.ResponseBody
import org.springframework.web.servlet.ModelAndView
import ru.planeta.admin.Actions
import ru.planeta.admin.Urls
import ru.planeta.admin.controllers.services.AdminBaseControllerService
import ru.planeta.api.aspect.transaction.Transactional
import ru.planeta.api.helper.ProjectService
import ru.planeta.api.model.ActionStatus
import ru.planeta.api.news.LoggerService
import ru.planeta.api.service.campaign.CampaignService
import ru.planeta.api.service.campaign.ContractorService
import ru.planeta.api.service.content.ProfileFileService
import ru.planeta.api.service.geo.GeoService
import ru.planeta.api.web.authentication.myProfileId
import ru.planeta.model.common.Contractor
import ru.planeta.model.common.campaign.enums.CampaignStatus
import ru.planeta.model.enums.ProjectType
import ru.planeta.model.news.ProfileNews
import ru.planeta.model.profile.ProfileFile
import java.util.*
import javax.validation.Valid

/**
 * Created with IntelliJ IDEA.
 * Date: 03.10.12
 * Time: 13:23
 */
@Controller
class AdminContractorController(private val contractorService: ContractorService,
                                private val profileFileService: ProfileFileService,
                                private val campaignService: CampaignService,
                                private val geoService: GeoService,
                                private val profileNewsService: LoggerService,
                                projectService: ProjectService,
                                private val adminBaseControllerService: AdminBaseControllerService) {

    private val mainHost: String = projectService.getHost(ProjectType.MAIN)

    @GetMapping(Urls.ADMIN_CONTRACTORS)
    fun getContractors(@RequestParam(defaultValue = "") searchString: String,
                       @RequestParam(required = false) campaignId: Long?,
                       @RequestParam(required = false) campaignStatus: CampaignStatus?,
                       @RequestParam(defaultValue = "0") offset: Int,
                       @RequestParam(defaultValue = "10") limit: Int): ModelAndView {
        val contractors = contractorService.getContractorsSearch(searchString, offset, limit)

        for (contractorWithCampaigns in contractors) {
            val campaigns = campaignService.getCampaignsByContractorId(contractorWithCampaigns.contractorId, 0, 1000)
            contractorWithCampaigns.campaigns = campaigns
        }
        return adminBaseControllerService.createAdminDefaultModelAndView(Actions.ADMIN_CONTRACTORS)
                .addObject("searchString", searchString)
                .addObject("contractors", contractors)
                .addObject("count", contractors.totalCount)
                .addObject("offset", offset)
                .addObject("limit", limit)
                .addObject("appHost", mainHost)
                .addObject("campaignId", campaignId)
                .addObject("campaignStatus", campaignStatus)
    }

    @GetMapping(Urls.ADMIN_CONTRACTOR_EDIT)
    fun getContractorForEdit(@RequestParam(required = false) contractorId: Long?,
                             @RequestParam(required = false) campaignId: Long?,
                             @RequestParam(required = false) campaignStatus: CampaignStatus?): ModelAndView {
        val modelAndView = adminBaseControllerService.createAdminDefaultModelAndView(Actions.ADMIN_CONTRACTOR_EDIT)
        if (contractorId != null) {
            modelAndView.addObject("contractor", contractorService.select(contractorId))
        } else {
            modelAndView.addObject("contractor", Contractor())
        }

        var backUrl = Urls.ADMIN_CONTRACTORS
        var filesUploadedBefore = emptyList<ProfileFile>()
        if (campaignId != null) {
            backUrl = Urls.MODERATOR_CAMPAIGN_MODERATION_INFO + "?campaignId=" + campaignId
            val campaign = campaignService.getCampaign(campaignId)
            if (campaign != null) {
                modelAndView.addObject("campaign", campaign)
                filesUploadedBefore = profileFileService.getProfileFiles(myProfileId(), campaign.profileId, 0, 20)
            }
        }
        return modelAndView.addObject("countries", geoService.countries)
                .addObject("campaignId", campaignId)
                .addObject("campaignStatus", campaignStatus)
                .addObject("backUrl", backUrl)
                .addObject("filesUploadedBefore", filesUploadedBefore)
    }

    @PostMapping(Urls.ADMIN_CONTRACTOR_SAVE)
    @ResponseBody
    @Transactional
    fun add(@Valid contractor: Contractor, bindingResult: BindingResult,
            @RequestParam(defaultValue = "0") campaignId: Long,
            @RequestParam(required = false) campaignStatus: CampaignStatus?): ModelAndView {
        if (bindingResult.hasErrors()) {
            return adminBaseControllerService.createAdminDefaultModelAndView(Actions.ADMIN_CONTRACTOR_EDIT)
                    .addObject("contractor", contractor)
                    .addObject("campaignId", campaignId)
                    .addObject("campaignStatus", campaignStatus)
                    .addObject("countries", geoService.countries)
                    .addObject("errors", bindingResult.allErrors)
        }

        contractorService.add(contractor, myProfileId(), campaignId)

        return if (campaignId > 0) {
            contractorService.insertRelation(contractor.contractorId, campaignId)
            campaignService.publishCampaign(myProfileId(), campaignId)
            adminBaseControllerService.baseControllerService
                    .createRedirectModelAndView(Urls.MODERATOR_CAMPAIGN_MODERATION_INFO, mapOf("campaignId" to campaignId))
        } else
            adminBaseControllerService.baseControllerService
                    .createRedirectModelAndView(Urls.ADMIN_CONTRACTORS)
    }

    // TODO change to POST
    @GetMapping(Urls.ADMIN_CONTRACTOR_BIND)
    @ResponseBody
    fun bindContractor(@RequestParam contractorId: Long,
                       @RequestParam campaignId: Long,
                       @RequestParam(defaultValue = "false") fromContractors: Boolean): ModelAndView {
        contractorService.insertRelation(contractorId, campaignId)
        campaignService.publishCampaign(myProfileId(), campaignId)
        return if (fromContractors) {
            adminBaseControllerService.baseControllerService
                    .createRedirectModelAndView(Urls.ADMIN_CONTRACTORS)
        } else
            adminBaseControllerService.baseControllerService
                    .createRedirectModelAndView(Urls.MODERATOR_CAMPAIGN_MODERATION_INFO, mapOf("campaignId" to campaignId))
    }

    @PostMapping(Urls.ADMIN_DELETE_PROFILE_FILE)
    @ResponseBody
    fun deleteProfileFile(@RequestParam(value = "profileId") profileId: Long,
                          @RequestParam(value = "fileId") fileId: Long): ActionStatus<*> {
        val myProfileId = myProfileId()
        val profileFile = profileFileService.getProfileFile(myProfileId, profileId, fileId)
        profileFileService.deleteProfileFile(myProfileId, profileId, fileId)

        val extraParamsMap = HashMap<String, String>()
        extraParamsMap.put("fileUrl", profileFile.fileUrl)
        profileNewsService.addFileDeletedNews(ProfileNews.Type.PROFILE_FILE_DELETED, myProfileId(), profileFile.profileId, extraParamsMap)
        return ActionStatus.createSuccessStatus<Any>()
    }

}

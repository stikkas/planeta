package ru.planeta.admin.controllers.campaign

import org.springframework.stereotype.Controller
import org.springframework.validation.BindingResult
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.ModelAttribute
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.servlet.ModelAndView
import ru.planeta.admin.Actions
import ru.planeta.admin.Urls
import ru.planeta.admin.controllers.services.BaseCampaignControllerService
import ru.planeta.api.service.admin.AdminService
import ru.planeta.api.service.common.PlanetaManagersService
import ru.planeta.model.common.PlanetaManager
import javax.validation.Valid

/**
 * Created with IntelliJ IDEA.
 * Date: 16.09.2015
 * Time: 13:33
 * To change this template use File | Settings | File Templates.
 */
@Controller("CampaignManagerController")
class ManagerController(private val baseCampaignControllerService: BaseCampaignControllerService,
                        private val adminService: AdminService) {

    @GetMapping(Urls.CAMPAIGN_MANAGERS_RELATIONS)
    fun campaignManagersRelations(): ModelAndView = baseCampaignControllerService.createAdminDefaultModelAndView(Actions.ADMIN_CAMPAIGN_MANAGERS_RELATIONS)
            .addObject("campaignManagers", baseCampaignControllerService.planetaManagersService.getAllManagers(null))
            .addObject("campaignManagersActive", baseCampaignControllerService.planetaManagersService.getAllManagers(true))
            .addObject("campaignManagersRelations", baseCampaignControllerService.planetaManagersService.campaignTagManagersRelations)
            .addObject("planetaWorkers", adminService.planetaWorkersWithoutSuperAdmins)

    @GetMapping(Urls.CAMPAIGN_MANAGEMENT)
    fun campaignManagement(): ModelAndView = baseCampaignControllerService.createAdminDefaultModelAndView(Actions.ADMIN_CAMPAIGN_MANAGEMENT)

    @PostMapping(Urls.UPDATE_CAMPAIGN_MANAGERS_RELATION)
    fun updateCampaignManagersRelation(@RequestParam managerId: Long, @RequestParam tagId: Long): ModelAndView {
        baseCampaignControllerService.planetaManagersService.changeCampaignTagManagerRelation(managerId, tagId)
        return baseCampaignControllerService.baseControllerService.createRedirectModelAndView(Urls.CAMPAIGN_MANAGERS_RELATIONS)
    }

    @PostMapping(Urls.ADD_MANAGER)
    fun addCampaignManager(@RequestParam(value = "profileId", required = true) profileId: Long,
                           @RequestParam(value = "name", defaultValue = "") shortName: String,
                           @RequestParam(defaultValue = "") fullName: String,
                           @RequestParam(defaultValue = "") email: String): ModelAndView {
        if (!shortName.isEmpty() && !fullName.isEmpty() && !email.isEmpty()) {
            baseCampaignControllerService.planetaManagersService.createManager(shortName, fullName, email, profileId)
        }
        return baseCampaignControllerService.baseControllerService.createRedirectModelAndView(Urls.CAMPAIGN_MANAGERS_RELATIONS)
    }

    @PostMapping(Urls.SAVE_MANAGER)
    fun saveCampaignManager(@Valid @ModelAttribute manager: PlanetaManager, result: BindingResult): ModelAndView {
        if (!result.hasErrors()) {
            baseCampaignControllerService.planetaManagersService.saveManager(manager)
        }
        return baseCampaignControllerService.baseControllerService.createRedirectModelAndView(Urls.CAMPAIGN_MANAGERS_RELATIONS)
    }
}

package ru.planeta.admin.validation

import org.apache.commons.lang3.StringUtils
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Component
import org.springframework.validation.Errors
import org.springframework.validation.ValidationUtils
import org.springframework.validation.Validator
import ru.planeta.admin.models.ProductEdit
import ru.planeta.api.service.profile.ProfileService
import ru.planeta.api.utils.ValidateUtils
import ru.planeta.commons.web.WebUtils
import ru.planeta.model.enums.ProfileType
import ru.planeta.model.shop.enums.ProductCategory
import ru.planeta.model.shop.enums.ProductState
import java.math.BigDecimal
import java.math.RoundingMode

/**
 * User: m.shulepov
 * Date: 23.07.12
 * Time: 17:56
 */
@Component
class ProductEditValidator(private val productAttributeValidator: ProductAttributeValidator,
                           private val profileService: ProfileService) : Validator {

    // url for http HEAD validation
    @Value("\${public.cdn.url:}")
    lateinit var resourceHost: String

    override fun supports(clazz: Class<*>): Boolean {
        return ProductEdit::class.java.isAssignableFrom(clazz)
    }

    override fun validate(target: Any, errors: Errors) {
        val product = target as ProductEdit

        if (SECTION_COMMON == product.openSection || SECTION_PREVIEW == product.openSection) {
            ValidationUtils.rejectIfEmptyOrWhitespace(errors, "name", "field.required")
            ValidateUtils.rejectIfContainsNotValidString(errors, "name", "wrong.chars")
            ValidateUtils.rejectIfContainsHtmlTags(errors, "name", "wrong.chars")
            ValidationUtils.rejectIfEmptyOrWhitespace(errors, "descriptionHtml", "field.required")

            if (StringUtils.isNotBlank(product.referrerWebAlias)) {
                val profile = profileService.getProfile(product.referrerWebAlias)
                if (profile == null || !(profile.profileType == ProfileType.USER || profile.profileType == ProfileType.GROUP)) {
                    errors.rejectValue("referrerId", "not.found.profile")
                }
            }
        }

        if (SECTION_DETAILS == product.openSection || SECTION_PREVIEW == product.openSection) {
            // not check price for group
            if (product.productState == ProductState.META) {
                val childrenProducts = product.childrenProducts
                if (childrenProducts == null || childrenProducts.size == 0)
                    product.productState = ProductState.ATTRIBUTE
                else {
                    for (child in childrenProducts) {
                        errors.pushNestedPath("childrenProducts[" + childrenProducts.indexOf(child) + "]")
                        errors.pushNestedPath("productAttribute")
                        ValidationUtils.invokeValidator(productAttributeValidator, child.productAttribute, errors)
                        errors.popNestedPath()
                        errors.popNestedPath()
                    }
                }
            } else if (product.productCategory == ProductCategory.DIGITAL
                    && product.contentUrls != null
                    && !product.contentUrls.isEmpty()) {
                validateResourceHost(errors, product)
            }

            validatePrice(errors, product)
            validateTags(errors, product)
        }

        if (SECTION_PREVIEW == product.openSection) {
            validateMerchantProfile(errors, product)
        }
    }

    private fun validateResourceHost(errors: Errors, product: ProductEdit) =
            product.contentUrls
                    .filter { it.isNotBlank() }
                    .map { WebUtils.headRequest(resourceHost + it, 500, 500) }
                    .filter { it == null }
                    .forEach { errors.rejectValue("contentUrlsString", "product.invalid.url") }

    private fun validatePrice(errors: Errors, product: ProductEdit) {
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "price", "field.required")
        if (product.price != null && product.price.setScale(2, RoundingMode.HALF_UP).compareTo(BigDecimal.ZERO) <= 0) {
            errors.rejectValue("price", "wrong.number")
        }
    }

    private fun validateMerchantProfile(errors: Errors, product: ProductEdit) {
        errors.getFieldValue("merchantProfileId")

        if (product.merchantProfileId == 0L) {
            errors.rejectValue("merchantProfileId", "field.required")
            return
        }

        val group = profileService.getProfile(product.merchantProfileId)
        if (group == null) {
            errors.rejectValue("merchantProfileId", "not.found.profile")
        }
    }

    companion object {

        private const val SECTION_COMMON = "common"
        private const val SECTION_DETAILS = "details"
        private const val SECTION_PREVIEW = "preview"


        private fun validateTags(errors: Errors, product: ProductEdit) {
            if (product.tags.isEmpty()) {
                errors.rejectValue("tags", "product.no.tags")
            }
        }
    }

}

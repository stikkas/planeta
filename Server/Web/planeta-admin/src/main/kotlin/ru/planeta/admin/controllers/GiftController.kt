package ru.planeta.admin.controllers

import org.springframework.context.MessageSource
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.ResponseBody
import org.springframework.web.servlet.ModelAndView
import ru.planeta.admin.Actions
import ru.planeta.admin.Urls
import ru.planeta.admin.controllers.services.AdminBaseControllerService
import ru.planeta.api.exceptions.MessageCode
import ru.planeta.api.model.ActionStatus
import ru.planeta.api.news.LoggerService
import ru.planeta.api.service.admin.AdminService
import ru.planeta.api.service.billing.GiftService
import ru.planeta.api.service.billing.order.OrderService
import ru.planeta.api.service.billing.payment.PaymentService
import ru.planeta.api.service.profile.ProfileBalanceService
import ru.planeta.api.web.authentication.myProfileId
import ru.planeta.model.Constants
import ru.planeta.model.news.ProfileNews
import java.math.BigDecimal
import java.util.*

/**
 * Created by IntelliJ IDEA.
 * User: eshevchenko
 * Date: 07.11.12
 */
@Controller
class GiftController(private val giftService: GiftService,
                     private val profileBalanceService: ProfileBalanceService,
                     private val orderService: OrderService,
                     private val paymentService: PaymentService,
                     private val profileNewsService: LoggerService,
                     private val adminService: AdminService,
                     private val messageSource: MessageSource,
                     private val adminBaseControlleService: AdminBaseControllerService) {


    @PostMapping(Urls.ADMIN_GIFTS)
    fun giftMoney(@RequestParam profileId: Long,
                  @RequestParam amount: BigDecimal,
                  @RequestParam(defaultValue = "") comment: String): ModelAndView {
        val myProfileId = myProfileId()
        try {
        giftService.giftMoney(myProfileId, profileId, amount, comment)
        } catch (e: Exception) {
            return giftModel(profileId)
                    .addObject("errorMessage", e.message)
        }
        profileNewsService.addProfileNews(ProfileNews.Type.ADMIN_GIFT_MONEY_FOR_USER, myProfileId, profileId)
        return giftModel(profileId)
                .addObject("successMessage", messageSource.getMessage("gift.success", null, "", Locale.getDefault()));
    }

    @GetMapping(Urls.ADMIN_GIFTS)
    fun getUserInfo(@RequestParam profileId: Long): ModelAndView {
        adminBaseControlleService.verifyAdminPermission(MessageCode.GLOBAL_ADMIN_GIFT_PRESENT)
        return giftModel(profileId)
    }

    private fun giftModel(profileId: Long): ModelAndView {
        val planetaBalance = profileBalanceService.getBalance(Constants.PLANETA_PROFILE_ID)
        return adminBaseControlleService.createAdminDefaultModelAndView(Actions.ADMIN_GIFTS)
                .addObject("profileId", profileId)
                .addObject("planetaBalance", planetaBalance)
    }

}


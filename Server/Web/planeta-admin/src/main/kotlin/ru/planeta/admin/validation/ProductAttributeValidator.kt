package ru.planeta.admin.validation

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.MessageSource
import org.springframework.stereotype.Component
import org.springframework.validation.Errors
import org.springframework.validation.ValidationUtils
import org.springframework.validation.Validator
import ru.planeta.api.utils.ValidateUtils
import ru.planeta.model.shop.ProductAttribute

/**
 * User: m.shulepov
 * Date: 24.07.12
 * Time: 16:29
 */
@Component
class ProductAttributeValidator : Validator {

    @Autowired
    private val messageSource: MessageSource? = null

    override fun supports(clazz: Class<*>): Boolean {
        return ProductAttribute::class.java.isAssignableFrom(clazz)
    }

    override fun validate(target: Any, errors: Errors) {
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "value", "field.required")
        ValidateUtils.rejectIfContainsNotValidString(errors, "value", "wrong.chars")
        ValidateUtils.rejectIfSizeIsTooLarge(errors, "value", 25, messageSource)

    }
}

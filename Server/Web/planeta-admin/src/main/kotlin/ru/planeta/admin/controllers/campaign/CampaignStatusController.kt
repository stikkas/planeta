package ru.planeta.admin.controllers.campaign

import org.springframework.context.MessageSource
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.ResponseBody
import ru.planeta.admin.Urls
import ru.planeta.api.exceptions.NotFoundException
import ru.planeta.api.model.ActionStatus
import ru.planeta.api.news.LoggerService
import ru.planeta.api.service.campaign.CampaignService
import ru.planeta.api.web.authentication.myProfileId
import ru.planeta.model.news.ProfileNews

/**
 * Date: 16.09.2015
 * Time: 14:20
 */
@Controller
class CampaignStatusController(private val campaignService: CampaignService,
                               private val profileNewsService: LoggerService,
                               private val messageSource: MessageSource) {

    @PostMapping(Urls.HANDLE_CAMPAIGN_CHANGE_STATUS)
    @ResponseBody
    fun handleCampaignChangeStatus(@RequestParam campaignId: Long): Boolean =
            try {
                val myProfileId = myProfileId()
                profileNewsService.addProfileNews(ProfileNews.Type.ADMIN_SIMPLE_CHANGE_CAMPAIGN_STATUS, myProfileId, campaignId, campaignId)
                campaignService.updateCampaignStatusSpecialForTheNeedsOfAdmins(myProfileId, campaignId)
            } catch (e: NotFoundException) {
                false
            }

    @PostMapping(Urls.CONFIRMATION_CAMPAIGN_CHANGE_STATUS)
    @ResponseBody
    fun confirmationCampaignChangeStatus(@RequestParam campaignId: Long): ActionStatus<*> =
            try {
                ActionStatus.createSuccessStatus(campaignService.getCampaignSafe(campaignId))
            } catch (e: NotFoundException) {
                ActionStatus.createErrorStatus<Any>("error.campaign.not.found.verbatim", messageSource)
            }

}

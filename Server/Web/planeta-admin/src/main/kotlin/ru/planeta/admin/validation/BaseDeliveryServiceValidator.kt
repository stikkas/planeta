package ru.planeta.admin.validation


import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import org.springframework.validation.Errors
import org.springframework.validation.ValidationUtils
import org.springframework.validation.Validator
import ru.planeta.api.web.validation.AddressValidator
import ru.planeta.model.common.delivery.BaseDelivery
import ru.planeta.model.common.delivery.LinkedDelivery
import ru.planeta.model.shop.enums.DeliveryType

/**
 * @author Andrew.Arefyev@gmail.com
 * 01.11.13 16:29
 */
@Component
class BaseDeliveryServiceValidator(private val addressValidator: AddressValidator) : Validator {

    override fun supports(clazz: Class<*>): Boolean {
        return clazz.isAssignableFrom(BaseDelivery::class.java) && !LinkedDelivery::class.java.isAssignableFrom(clazz)
    }

    override fun validate(target: Any, errors: Errors) {
        val delivery = target as BaseDelivery
        //        ValidationUtils.rejectIfEmpty(errors, "description", "field.required");
        ValidationUtils.rejectIfEmpty(errors, "name", "field.required")
        if (delivery.serviceType == DeliveryType.CUSTOMER_PICKUP && delivery.serviceId != BaseDelivery.DEFAULT_CUSTOMER_PICKUP_SERVICE_ID) {
            errors.pushNestedPath("address")
            addressValidator.validate(delivery.address, errors)
            errors.popNestedPath()
        }
    }
}


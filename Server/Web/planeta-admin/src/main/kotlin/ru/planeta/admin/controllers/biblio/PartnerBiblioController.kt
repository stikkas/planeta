package ru.planeta.admin.controllers.biblio

import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.ModelAttribute
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.servlet.ModelAndView
import ru.planeta.admin.Actions
import ru.planeta.admin.Biblio
import ru.planeta.admin.controllers.services.BiblioConfigControllerService
import ru.planeta.admin.controllers.services.PartnerBiblioControllerService
import ru.planeta.api.model.ConfigurationType
import ru.planeta.api.model.biblio.Partner
import ru.planeta.api.service.admin.AdminService

@Controller
class PartnerBiblioController(private val partnerBiblioControllerService: PartnerBiblioControllerService) {

    @GetMapping(Biblio.ADMIN_PARTNERS_FILL)
    fun fill(): ModelAndView = partnerBiblioControllerService.fill()

    @PostMapping(Biblio.ADMIN_PARTNERS_FILL)
    fun add(@ModelAttribute(BiblioConfigControllerService.CONFIGURATION_ATTR_NAME) partner: Partner): ModelAndView =
            partnerBiblioControllerService.add(partner)

    @GetMapping(Biblio.ADMIN_PARTNERS_DELETE)
    fun deleteGet(@RequestParam id: Int): ModelAndView = partnerBiblioControllerService.deleteGet(id)

    @GetMapping(Biblio.ADMIN_PARTNERS_EDIT)
    fun editGet(@ModelAttribute(BiblioConfigControllerService.CONFIGURATION_ATTR_NAME) partner: Partner): ModelAndView =
            partnerBiblioControllerService.editGet(partner)

    @PostMapping(Biblio.ADMIN_PARTNERS_EDIT)
    fun editPost(@ModelAttribute(BiblioConfigControllerService.CONFIGURATION_ATTR_NAME) partner: Partner): ModelAndView =
            partnerBiblioControllerService.editPost(partner)

    @GetMapping(Biblio.ADMIN_PARTNERS_LIST)
    fun list(): ModelAndView = partnerBiblioControllerService.list()

    @PostMapping(Biblio.ADMIN_PARTNERS_SORT)
    fun sort(@RequestParam startIndex: Int, @RequestParam stopIndex: Int): ModelAndView =
            partnerBiblioControllerService.sort(startIndex, stopIndex)
}

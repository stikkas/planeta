package ru.planeta.admin.controllers

import org.apache.log4j.Logger
import org.springframework.stereotype.Controller
import org.springframework.validation.BindingResult
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.ResponseBody
import ru.planeta.admin.Urls
import ru.planeta.admin.models.ChangeAlias
import ru.planeta.api.exceptions.AlreadyExistException
import ru.planeta.api.model.ActionStatus
import ru.planeta.api.service.admin.AdminService
import ru.planeta.api.web.authentication.myProfileId
import ru.planeta.api.web.controllers.services.BaseControllerService
import ru.planeta.commons.web.IpUtils
import java.util.*
import javax.servlet.http.HttpServletRequest
import javax.validation.Valid

/**
 * Date: 29.10.12
 * Time: 17:59
 */
@Controller
class CommonController(private val baseControllerService: BaseControllerService,
                       private val adminService: AdminService) {

    private val logger = Logger.getLogger(CommonController::class.java)

    @PostMapping(Urls.ADMIN_CHANGE_ALIAS)
    @ResponseBody
    fun setAlias(@Valid changeAlias: ChangeAlias,
                 bindingResult: BindingResult,
                 req: HttpServletRequest): ActionStatus<*> {

        if (bindingResult.hasErrors()) {
            return baseControllerService.createErrorStatus<Any>(bindingResult)
        }

        try {
            adminService.changeAlias(changeAlias.profileId, changeAlias.aliasName)
            logger.info("Admin id=${myProfileId()} changed profile alias id=${changeAlias.profileId} request ip=${IpUtils.getRemoteAddr(req)}")
        } catch (e: AlreadyExistException) {
            val actionStatus = ActionStatus<Boolean>()
            actionStatus.isSuccess = false
            actionStatus.errorMessage = baseControllerService.messageSource.getMessage("field.error.exists", null, Locale.getDefault())
            return actionStatus
        }

        return ActionStatus.createSuccessStatus<Any>()
    }
}

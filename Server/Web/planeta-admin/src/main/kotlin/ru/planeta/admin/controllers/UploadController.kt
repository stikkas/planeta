package ru.planeta.admin.controllers

import org.springframework.stereotype.Controller
import org.springframework.validation.BindingResult
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.ResponseBody
import org.springframework.web.servlet.ModelAndView
import ru.planeta.admin.Actions
import ru.planeta.admin.Urls
import ru.planeta.admin.controllers.services.AdminBaseControllerService
import ru.planeta.api.service.content.PhotoService
import ru.planeta.api.service.content.ProfileFileService
import ru.planeta.api.web.authentication.myProfileId
import ru.planeta.model.profile.ProfileFile
import ru.planeta.model.profile.media.Photo
import java.io.FileNotFoundException

/**
 * Created with IntelliJ IDEA.
 * Date: 03.10.12
 * Time: 13:38
 */
@Controller
class UploadController(private val profileFileService: ProfileFileService,
                       private val photoService: PhotoService,
                       private val adminBaseControllerService: AdminBaseControllerService) {


    @GetMapping(Urls.ADMIN_IMAGE_UPLOAD)
    fun getImageUpload(@RequestParam(defaultValue = "0") offset: Int,
                       @RequestParam(defaultValue = "20") limit: Int): ModelAndView {
        val myProfileId = myProfileId()
        return adminBaseControllerService.createAdminDefaultModelAndView(Actions.ADMIN_IMAGE_UPLOAD)
                    .addObject("photosUploadedBefore", getLastUploadedImages(offset, limit))
                    .addObject("offset", offset)
                    .addObject("limit", limit)
                    .addObject("count", photoService.getUserHiddenPhotosCount(myProfileId, myProfileId))
    }

    @GetMapping(Urls.ADMIN_LAST_UPLOADED_IMAGES)
    @ResponseBody
    fun getLastUploadedImages(@RequestParam(defaultValue = "0") offset: Int,
                              @RequestParam(defaultValue = "10") limit: Int): List<Photo> {
        val myProfileId = myProfileId()
        return photoService.getUserHiddenPhotos(myProfileId, myProfileId, offset, limit)
    }

    @GetMapping(Urls.ADMIN_FILE_UPLOAD)
    fun getFileUpload(@RequestParam(defaultValue = "0") offset: Int,
                      @RequestParam(defaultValue = "30") limit: Int): ModelAndView =
            adminBaseControllerService.createAdminDefaultModelAndView(Actions.ADMIN_FILE_UPLOAD)
                    .addObject("filesUploadedBefore", getLastUploadedFiles(offset, limit))
                    .addObject("offset", offset)
                    .addObject("limit", limit)

    @GetMapping(Urls.ADMIN_DOCUMENTS_UPLOAD)
    fun getDocumentUpload(@RequestParam(defaultValue = "0") offset: Int,
                          @RequestParam(defaultValue = "500") limit: Int): ModelAndView =
            adminBaseControllerService.createAdminDefaultModelAndView(Actions.ADMIN_DOCUMENT_UPLOAD)
                    .addObject("filesUploadedBefore", profileFileService.getProfileFiles(myProfileId(),
                            DEFAULT_DOCUMENT_OWNER_PROFILE_ID, offset, limit))
                    .addObject("offset", offset)
                    .addObject("limit", limit)

    @GetMapping(Urls.ADMIN_LAST_UPLOADED_FILES)
    @ResponseBody
    fun getLastUploadedFiles(@RequestParam(defaultValue = "0") offset: Int,
                             @RequestParam(defaultValue = "10") limit: Int): List<ProfileFile> {
        val myProfileId = myProfileId()
        return profileFileService.getProfileFiles(myProfileId, myProfileId, offset, limit)
    }

    @GetMapping(Urls.ADMIN_DOCUMENT)
    fun adminDocument(@RequestParam(defaultValue = "0") fileId: Long,
                      @RequestParam(defaultValue = "0") profileId: Long): ModelAndView {
        val profileFile: ProfileFile
        if (fileId == 0L || profileId == 0L) {
            throw FileNotFoundException()
        } else {
            profileFile = profileFileService.getProfileFile(myProfileId(), profileId, fileId)
        }

        return adminBaseControllerService.createAdminDefaultModelAndView(Actions.ADMIN_DOCUMENT)
                .addObject("profileFile", profileFile)
    }

    @PostMapping(Urls.ADMIN_DOCUMENT)
    fun saveDocument(profileFile: ProfileFile, result: BindingResult): ModelAndView {
        val selectedProfileFile: ProfileFile
        if (profileFile.fileId == 0L || profileFile.profileId == 0L) {
            throw FileNotFoundException()
        } else {
            selectedProfileFile = profileFileService.getProfileFile(myProfileId(), profileFile.profileId, profileFile.fileId)
        }
        selectedProfileFile.title = profileFile.title
        selectedProfileFile.description = profileFile.description
        profileFileService.updateProfileFileTitleAndDescription(myProfileId(), selectedProfileFile)

        return adminBaseControllerService.baseControllerService.createRedirectModelAndView(Urls.ADMIN_DOCUMENTS_UPLOAD)
    }

    companion object {
        const val DEFAULT_DOCUMENT_OWNER_PROFILE_ID: Long = 21765
    }

}

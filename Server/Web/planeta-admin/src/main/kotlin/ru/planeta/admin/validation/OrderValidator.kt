package ru.planeta.admin.validation

import org.springframework.stereotype.Component
import org.springframework.validation.Errors
import org.springframework.validation.Validator
import ru.planeta.model.common.Order
import ru.planeta.model.shop.enums.DeliveryStatus

/**
 * @author p.vyazankin
 * @since 4/26/13 6:41 PM
 */
@Component
class OrderValidator : Validator {
    override fun supports(aClass: Class<*>): Boolean {
        return aClass.isAssignableFrom(Order::class.java)
    }

    override fun validate(o: Any, errors: Errors) {
        val order = o as Order
        if (order.deliveryStatus == DeliveryStatus.PENDING) {
            errors.rejectValue("deliveryStatus", "order.cantReturnDeliveryStatusToPending")
        }
    }
}

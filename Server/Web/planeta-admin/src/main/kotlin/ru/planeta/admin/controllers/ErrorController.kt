package ru.planeta.admin.controllers

import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.servlet.ModelAndView
import ru.planeta.admin.Actions
import ru.planeta.api.web.controllers.services.BaseControllerService
import ru.planeta.model.enums.ProjectType

/**
 * @author ds.kolyshev
 * Date: 02.09.11
 */
@Controller
class ErrorController(private val baseControllerService: BaseControllerService) {

    @GetMapping(value = "/error")
    fun error(@RequestParam(defaultValue = "") message: String): ModelAndView =
            baseControllerService.createDefaultModelAndView(Actions.ERROR, ProjectType.ADMIN)
                    .addObject("errorMessage", message)
}

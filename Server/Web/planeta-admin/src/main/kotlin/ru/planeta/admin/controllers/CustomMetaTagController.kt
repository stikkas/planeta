package ru.planeta.admin.controllers

import org.springframework.beans.factory.annotation.Value
import org.springframework.context.MessageSource
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.*
import org.springframework.web.servlet.ModelAndView
import ru.planeta.admin.Actions
import ru.planeta.admin.Urls
import ru.planeta.admin.controllers.services.AdminBaseControllerService
import ru.planeta.api.helper.ProjectService
import ru.planeta.api.model.ActionStatus
import ru.planeta.api.service.admin.AdminService
import ru.planeta.api.service.common.CustomMetaTagService
import ru.planeta.api.service.configurations.StaticNodesService
import ru.planeta.api.service.profile.AuthorizationService
import ru.planeta.api.service.profile.ProfileService
import ru.planeta.api.service.profile.UserService
import ru.planeta.api.service.security.PermissionService
import ru.planeta.api.web.authentication.myProfileId
import ru.planeta.mail.MailService
import ru.planeta.model.enums.CustomMetaTagType
import ru.planeta.model.commondb.CustomMetaTag
import javax.validation.Valid

@Controller
class CustomMetaTagController(private val customMetaTagServiceMy: CustomMetaTagService,
                              private val adminBaseControllerService: AdminBaseControllerService) {

    @GetMapping(Urls.ADMIN_CUSTOM_META_TAG_LIST)
    @ResponseBody
    fun getAdminCustomMetaTagList(@RequestParam customMetaTagType: CustomMetaTagType): ModelAndView {
        adminBaseControllerService.permissionService.checkAdministrativeRole(myProfileId())
        return adminBaseControllerService.createAdminDefaultModelAndView(Actions.ADMIN_CUSTOM_META_TAG_LIST)
                .addObject("customMetaTags", customMetaTagServiceMy.selectCustomMetaTagList(customMetaTagType, 0, 1000))
                .addObject("customMetaTagTypeRus", customMetaTagType.name)
                .addObject("customMetaTagType", customMetaTagType.name)
                .addObject("customMetaTagTypeId", customMetaTagType.name.toUpperCase())
    }

    @RequestMapping(Urls.ADMIN_CUSTOM_META_TAG_DELETE_NEW)
    @ResponseBody
    fun adminCustomMetaTagDelete(@RequestParam(value = "customMetaTagId") id: Long): ActionStatus<*> {
        adminBaseControllerService.permissionService.checkAdministrativeRole(myProfileId())
        customMetaTagServiceMy.deleteCustomMetaTag(id)
        return ActionStatus.createSuccessStatus<Any>()
    }

    @GetMapping(Urls.ADMIN_CUSTOM_META_TAG_EDIT_NEW)
    fun adminCustomMetaTagEdit(@RequestParam(required = false) tagPath: String?,
                               @RequestParam(required = false) customMetaTagId: Long?,
                               @RequestParam customMetaTagType: CustomMetaTagType): ModelAndView {
        adminBaseControllerService.permissionService.checkAdministrativeRole(myProfileId())

        val modelAndView = adminBaseControllerService.createAdminDefaultModelAndView(Actions.ADMIN_CUSTOM_META_TAG_EDIT_NEW)
                .addObject("customMetaTagTypeId", customMetaTagType.name.toUpperCase())
                .addObject("customMetaTagType", customMetaTagType.name)
                .addObject("customMetaTagTypeRus", customMetaTagType.name)
        val customMetaTag = if (tagPath != null) customMetaTagServiceMy.selectCustomMetaTag(customMetaTagType, customMetaTagId) else CustomMetaTag()

        modelAndView.addObject("customMetaTag", customMetaTag)
        return modelAndView
    }

    @PostMapping(Urls.ADMIN_CUSTOM_META_TAG_SAVE_NEW)
    fun adminCustomMetaTagSave(@Valid customMetaTag: CustomMetaTag): ModelAndView {

        adminBaseControllerService.permissionService.checkAdministrativeRole(myProfileId())
        val customMetaTagType = customMetaTag.customMetaTagType
        if (customMetaTag.customMetaTagId == null) {
            val selectedCMT = customMetaTagServiceMy.selectCustomMetaTag(customMetaTagType, customMetaTag.tagPath)
            if (selectedCMT != null) {
                return adminBaseControllerService.createAdminDefaultModelAndView(Actions.ADMIN_CUSTOM_META_TAG_EDIT_NEW)
                        .addObject("customMetaTagTypeId", customMetaTagType?.name?.toUpperCase())
                        .addObject("customMetaTagType", customMetaTagType?.name)
                        .addObject("customMetaTagTypeRus", customMetaTagType?.name)
                        .addObject("customMetaTag", customMetaTag)
                        .addObject("errorCMT", true)
            }
        }
        customMetaTagServiceMy.insertOrUpdateCustomMetaTag(customMetaTag)
        return adminBaseControllerService.baseControllerService
                .createRedirectModelAndView("${Urls.ADMIN_CUSTOM_META_TAG_LIST}?customMetaTagType=$customMetaTagType")
    }
}

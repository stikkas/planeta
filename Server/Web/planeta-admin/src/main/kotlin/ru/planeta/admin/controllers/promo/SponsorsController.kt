package ru.planeta.admin.controllers.promo

import org.springframework.stereotype.Controller
import org.springframework.validation.BindingResult
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.servlet.ModelAndView
import ru.planeta.admin.Actions
import ru.planeta.admin.Urls
import ru.planeta.api.Utils.empty
import ru.planeta.api.web.controllers.IAction
import ru.planeta.api.web.controllers.services.BaseControllerService
import ru.planeta.dao.commondb.SponsorDAO
import ru.planeta.model.common.campaign.Sponsor
import ru.planeta.model.enums.ProjectType
import javax.validation.Valid

/**
 *
 * Created by alexa_000 on 14.04.2015.
 */
@Controller
class SponsorsController(private val dao: SponsorDAO,
                         private val baseControllerService: BaseControllerService) {


    @GetMapping(Urls.SPONSORS)
    fun showSponsors(): ModelAndView = createDefaultModelAndView(Actions.ADMIN_SPONSORS)
            .addObject("sponsors", dao.selectAll())

    @GetMapping(Urls.EDIT_SPONSOR)
    fun editSponsor(@RequestParam(required = false) alias: String?): ModelAndView {
        val sponsor = if (!empty(alias)) dao.selectSponsor(alias) else Sponsor()
        return createDefaultModelAndView(Actions.ADMIN_EDIT_SPONSOR)
                .addObject("sponsor", sponsor)
    }

    @PostMapping(Urls.SAVE_SPONSOR)
    fun saveSponsor(@Valid sp: Sponsor, bindingResult: BindingResult): ModelAndView {
        val sponsor = dao.selectSponsor(sp.alias)
        if (sponsor == null) {
            sp.alias = sp.alias.toUpperCase()
            dao.insert(sp)
        } else {
            dao.update(sp)
        }
        return baseControllerService.createRedirectModelAndView(Urls.SPONSORS)
    }

    @GetMapping(Urls.DELETE_SPONSOR)
    fun deleteSponsor(@RequestParam alias: String): ModelAndView {
        dao.delete(alias)
        return baseControllerService.createRedirectModelAndView(Urls.SPONSORS)
    }

    private fun createDefaultModelAndView(action: IAction): ModelAndView =
            baseControllerService.createDefaultModelAndView(action, ProjectType.ADMIN)
}

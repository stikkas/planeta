package ru.planeta.admin.validation

import org.apache.commons.lang3.StringUtils
import org.springframework.stereotype.Component
import org.springframework.validation.Errors
import org.springframework.validation.ValidationUtils
import org.springframework.validation.Validator
import ru.planeta.model.promo.TechnobattleProject

/**
 * Created with IntelliJ IDEA.
 * User: michail
 * Date: 20.01.2017
 * Time: 17:15
 */
@Component
class TechnobattleProjectValidator : Validator {
    override fun supports(clazz: Class<*>): Boolean {
        return TechnobattleProject::class.java.isAssignableFrom(clazz)
    }

    override fun validate(o: Any, errors: Errors) {
        ValidationUtils.rejectIfEmpty(errors, "title", "field.required")
        ValidationUtils.rejectIfEmpty(errors, "description", "field.required")
        ValidationUtils.rejectIfEmpty(errors, "team", "field.required")
        ValidationUtils.rejectIfEmpty(errors, "consumers", "field.required")
        ValidationUtils.rejectIfEmpty(errors, "smallImageUrl", "field.required")
        ValidationUtils.rejectIfEmpty(errors, "profileId", "field.required")
        val project = o as TechnobattleProject
        if (StringUtils.isEmpty(project.imageUrl) && StringUtils.isEmpty(project.videoSrc)) {
            errors.rejectValue("imageUrl", "field.required")
            errors.rejectValue("videoSrc", "field.required")
        }
    }
}

package ru.planeta.admin.controllers

import org.apache.log4j.Logger
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Controller
import org.springframework.validation.BindingResult
import org.springframework.web.bind.annotation.*
import org.springframework.web.servlet.ModelAndView
import ru.planeta.admin.Actions
import ru.planeta.admin.Urls
import ru.planeta.admin.controllers.services.AdminBaseControllerService
import ru.planeta.admin.utils.DateUtils.getIntervalToday
import ru.planeta.admin.utils.DateUtils.setAllTimeAsDate
import ru.planeta.api.Utils.map
import ru.planeta.api.exceptions.MessageCode
import ru.planeta.api.exceptions.PermissionException
import ru.planeta.api.log.service.DBLogService
import ru.planeta.api.model.ActionStatus
import ru.planeta.api.news.LoggerService
import ru.planeta.api.remote.PaymentGateService
import ru.planeta.api.service.billing.order.MoneyTransactionService
import ru.planeta.api.service.billing.order.OrderService
import ru.planeta.api.service.billing.payment.PaymentErrorCommentsService
import ru.planeta.api.service.billing.payment.PaymentErrorsService
import ru.planeta.api.service.billing.payment.PaymentService
import ru.planeta.api.service.billing.payment.settings.PaymentSettingsService
import ru.planeta.api.service.profile.AuthorizationService
import ru.planeta.api.web.authentication.myProfileId
import ru.planeta.commons.web.WebUtils
import ru.planeta.dao.commondb.ProjectPaymentToolDAO
import ru.planeta.dao.payment.PaymentMethodDAO
import ru.planeta.dao.payment.PaymentToolDAO
import ru.planeta.model.common.ProjectPaymentTool
import ru.planeta.model.common.TopayTransaction
import ru.planeta.model.enums.ProjectType
import ru.planeta.model.enums.TopayTransactionStatus
import ru.planeta.model.enums.generated.PaymentErrorStatus
import ru.planeta.model.filters.PaymentErrorsFilter
import ru.planeta.model.news.ProfileNews
import ru.planeta.model.stat.log.LoggerType
import java.util.*
import javax.validation.Valid

/**
 * Handles payment management requests.
 * User: eshevchenko
 */
@Controller
class BillingController(private val paymentService: PaymentService,
                        private val paymentSettingsService: PaymentSettingsService,
                        private val dbLogService: DBLogService,
                        private val paymentErrorsService: PaymentErrorsService,
                        private val paymentErrorCommentsService: PaymentErrorCommentsService,
                        private val paymentMethodDAO: PaymentMethodDAO,
                        private val projectPaymentToolDAO: ProjectPaymentToolDAO,
                        private val paymentToolDAO: PaymentToolDAO,
                        private val moneyTransactionService: MoneyTransactionService,
                        private val orderService: OrderService,
                        private val profileNewsService: LoggerService,
                        private val authorizationService: AuthorizationService,
                        private val adminBaseControllerService: AdminBaseControllerService) {

    @Autowired(required = false)
    lateinit var paymentGateService: PaymentGateService

    @GetMapping(Urls.PAYMENT_CONFIGURATOR)
    fun paymentsRelations(): ModelAndView = if (!adminBaseControllerService.permissionService.isSuperAdmin(myProfileId())) {
        throw PermissionException(MessageCode.USER_NOT_ADMIN)
    } else {
        adminBaseControllerService.createAdminDefaultModelAndView(Actions.ADMIN_PAYMENT_CONFIGURATOR)
                .addObject("projectPaymentTools", projectPaymentToolDAO.selectAll())
                .addObject("paymentTools", paymentToolDAO.selectAllPaymentTools())
                .addObject("paymentMethods", paymentSettingsService.allPaymentMethods)
    }

    @GetMapping(Urls.ADMIN_BILLING_PAYMENTS)
    fun getPaymentList(@RequestParam(required = false) searchStr: String?,
                       @RequestParam(defaultValue = "0") orderOrTransactionId: Long,
                       @RequestParam(required = false) transactionStatus: TopayTransactionStatus?,
                       @RequestParam(required = false) paymentMethodId: Long?,
                       @RequestParam(required = false) paymentProviderId: Long?,
                       @RequestParam(required = false) projectType: ProjectType?,
                       @RequestParam(required = false) dateFrom: Date?,
                       @RequestParam(required = false) dateTo: Date?,
                       @RequestParam(defaultValue = "0") offset: Int,
                       @RequestParam(defaultValue = "25") limit: Int,
                       @RequestParam(defaultValue = "0") toEmulateId: Int): ModelAndView {
        val myProfileId = myProfileId()
        verifyPlanetaAdminPermissions(myProfileId)

        val modelAndView = adminBaseControllerService.createAdminDefaultModelAndView(Actions.ADMIN_BILLING_PAYMENTS)
        val interval = setAllTimeAsDate(dateFrom, dateTo, modelAndView)
        val transactions = paymentService.getPayments(myProfileId, searchStr, transactionStatus,
                paymentMethodId, paymentProviderId, projectType, interval.dateFrom, interval.dateTo, orderOrTransactionId, offset, limit)


        val methods = paymentSettingsService.allPaymentMethods
        val providers = paymentSettingsService.allProviders

        return modelAndView
                .addObject("transactions", transactions)
                .addObject("methods", map(methods))
                .addObject("providers", map(providers))
                .addObject("methodsList", methods)
                .addObject("providersList", providers)
                .addObject("statuses", TopayTransactionStatus.values())
                .addObject("projectTypes", ProjectType.values())
                .addObject("profileEmailByTransactionId", collectProfileEmails(transactions))
                .addObject("count", offset + transactions.size + if (transactions.size == limit) 1 else 0)
                .addObject("offset", offset)
                .addObject("limit", limit)
                .addObject("emulatedPaymentId", toEmulateId)
                .addObject("emulationStatus", null)
    }

    @PostMapping(Urls.ADMIN_CANCEL_PAYMENT)
    @ResponseBody
    fun cancelPayment(@RequestParam transactionId: Long): ActionStatus<*> {
        var error: String? = null
        try {
            paymentGateService.cancelPayment(transactionId)
            profileNewsService.addProfileNews(ProfileNews.Type.ADMIN_CANCEL_PAYMENT, myProfileId(), transactionId)
        } catch (e: UnsupportedOperationException) {
            error = adminBaseControllerService.baseControllerService
                    .messageSource.getMessage("operation.for.this.payment.system.is.not.supported", null, Locale.getDefault())
        } catch (e: Exception) {
            error = adminBaseControllerService.baseControllerService
                    .messageSource.getMessage("during.the.operatiot.there.was.an.error", null, Locale.getDefault()) + " " + e.javaClass.name + " " + e.message
            log.warn("error while canceling transaction", e)
        }

        return if (error != null) ActionStatus.createErrorStatus<Any>(error,
                adminBaseControllerService.baseControllerService.messageSource) else ActionStatus.createSuccessStatus<Any>()
    }

    @PostMapping(Urls.ADMIN_PROCESS_PAYMENT)
    @ResponseBody
    fun processPayment(@RequestParam transactionId: Long): ActionStatus<*> =
            try {
                paymentGateService.validate(transactionId)
                profileNewsService.addProfileNews(ProfileNews.Type.ADMIN_PROCESS_PAYMENT, myProfileId(), transactionId)
                ActionStatus.createSuccessStatus<Any>()
            } catch (e: UnsupportedOperationException) {
                log.warn("error while processing transaction", e)
                ActionStatus.createErrorStatus<Any>("operation.for.this.payment.system.is.not.supported",
                        adminBaseControllerService.baseControllerService.messageSource)
            } catch (e: Exception) {
                log.warn("error while processing transaction", e)
                ActionStatus.createErrorStatus<Any>("during.the.operation.there.was.an.error",
                        adminBaseControllerService.baseControllerService.messageSource, e.javaClass)
            }


    @PostMapping(Urls.SAVE_PROJECT_PAYMENT_TOOL)
    fun saveProjectPaymentTool(@Valid projectPaymentTool: ProjectPaymentTool, result: BindingResult): ModelAndView {

        if (result.hasErrors()) {
            return adminBaseControllerService.baseControllerService.createRedirectModelAndView(Urls.PAYMENT_CONFIGURATOR)
        }

        try {
            projectPaymentToolDAO.update(projectPaymentTool)
            paymentMethodDAO.updateProjectPaymentMethod(projectPaymentTool.projectType, projectPaymentTool.paymentMethodId, projectPaymentTool.isEnabled)
        } catch (e: Exception) {
            val errorUrl = WebUtils.createUrl(Urls.PAYMENT_CONFIGURATOR, WebUtils.Parameters().add("errorMessage", e.toString()).params)
            return adminBaseControllerService.baseControllerService.createRedirectModelAndView(errorUrl)
        }

        return adminBaseControllerService.baseControllerService.createRedirectModelAndView(Urls.PAYMENT_CONFIGURATOR)
    }

    private fun collectProfileEmails(transactions: List<TopayTransaction>): Map<Long, String> {
        val result = HashMap<Long, String>()
        val emailByProfileId = HashMap<Long, String>()

        for (transaction in transactions) {
            val transactionId = transaction.transactionId
            val profileId = transaction.profileId

            var email: String? = emailByProfileId[profileId]
            if (email == null) {
                email = authorizationService.getUserPrivateEmailById(profileId)

                if (email != null) {
                    emailByProfileId.put(profileId, email)
                }
            }
            if (email != null) {
                result.put(transactionId, email)
            }
        }

        return result
    }


    @GetMapping(Urls.ADMIN_BILLING_ORDER)
    fun getOrderInfo(@RequestParam orderId: Long,
                     @RequestParam(value = "tab", defaultValue = "common") initialTab: String): ModelAndView {
        val myProfileId = myProfileId()

        val order = orderService.getOrderInfo(myProfileId, orderId)
        return adminBaseControllerService.createAdminDefaultModelAndView(Actions.ADMIN_BILLING_ORDER)
                .addObject("order", order)
                .addObject("payment", paymentService.getPayment(order.topayTransactionId))
                .addObject("initialTab", initialTab)
                .addObject("logEnabled", false)
    }

    @GetMapping(Urls.ADMIN_BILLING_ORDER_LOG)
    fun getDbLogRecordInfo(@RequestParam orderId: Long): ModelAndView {

        val myProfileId = myProfileId()
        verifyPlanetaAdminPermissions(myProfileId)

        val logRecords = dbLogService.getLogRecords(LoggerType.ORDER, orderId, 0L)
        for (payment in paymentService.getOrderPayments(myProfileId, orderId)) {
            logRecords.addAll(dbLogService.getLogRecords(LoggerType.PAYMENT, payment.transactionId, 0L))
        }

        Collections.sort(logRecords) { o1, o2 -> java.lang.Long.signum(o1.timeAdded - o2.timeAdded) }

        val order = orderService.getOrderInfo(myProfileId, orderId)
        return adminBaseControllerService.createAdminDefaultModelAndView(Actions.ADMIN_BILLING_ORDER)
                .addObject("logRecords", logRecords)
                .addObject("order", order)
                .addObject("initialTab", "log")
                .addObject("payment", paymentService.getPayment(order.topayTransactionId))
                .addObject("logEnabled", true)
    }

    @GetMapping(value = Urls.ADMIN_BILLING_PAYMENT)
    fun getPaymentInfo(@RequestParam paymentId: Long,
                       @RequestParam(value = "tab", defaultValue = "common") initialTab: String): ModelAndView {

        verifyPlanetaAdminPermissions(myProfileId())
        val payment = paymentService.getPayment(paymentId)

        return adminBaseControllerService.createAdminDefaultModelAndView(Actions.ADMIN_BILLING_PAYMENT)
                .addObject("paymentMethod", paymentService.getPaymentMethod(payment.paymentMethodId))
                .addObject("paymentProvider", paymentService.getPaymentProvider(payment.paymentProviderId))
                .addObject("payment", paymentService.getPayment(paymentId))
                .addObject("logRecords", dbLogService.getLogRecords(LoggerType.PAYMENT, paymentId, 0L))
                .addObject("initialTab", initialTab)
    }

    private fun verifyPlanetaAdminPermissions(clientId: Long) {
        if (!adminBaseControllerService.permissionService.isPlanetaAdmin(clientId)) {
            throw PermissionException(MessageCode.USER_NOT_ADMIN)
        }
    }

    @RequestMapping(value = Urls.ADMIN_BILLING_PAYMENT_ERRORS, method = [RequestMethod.GET, RequestMethod.POST])
    fun getPaymentErrorList(@ModelAttribute paymentErrorsFilter: PaymentErrorsFilter): ModelAndView {

        val myProfileId = myProfileId()
        verifyPlanetaAdminPermissions(myProfileId)

        val intervalToday = getIntervalToday(paymentErrorsFilter.dateFrom, paymentErrorsFilter.dateTo)
        paymentErrorsFilter.dateFrom = intervalToday.dateFrom
        paymentErrorsFilter.dateTo = intervalToday.dateTo

        val paymentErrorList = paymentErrorsService.selectPaymentErrorsExListByFilter(paymentErrorsFilter)
        val count = paymentErrorsService.selectPaymentErrorsCountByFilter(paymentErrorsFilter)

        val methods = paymentSettingsService.allPaymentMethods
        val providers = paymentSettingsService.allProviders

        return adminBaseControllerService.createAdminDefaultModelAndView(Actions.ADMIN_BILLING_PAYMENT_ERRORS)
                .addObject("paymentErrorsFilter", paymentErrorsFilter)
                .addObject("paymentErrorList", paymentErrorList)
                .addObject("count", count)
                .addObject("methods", map(methods))
                .addObject("providers", map(providers))
    }

    @GetMapping(Urls.ADMIN_BILLING_PAYMENT_ERROR_COMMENT_LIST)
    fun getPaymentErrorCommentsList(@RequestParam paymentErrorId: Long): ModelAndView {
        val myProfileId = myProfileId()
        verifyPlanetaAdminPermissions(myProfileId)
        val paymentError = paymentErrorsService.selectPaymentErrorsWithComments(paymentErrorId)
        return adminBaseControllerService.createAdminDefaultModelAndView(Actions.ADMIN_BILLING_PAYMENT_ERROR_COMMENT_LIST)
                .addObject("paymentError", paymentError)
    }

    @PostMapping(Urls.ADMIN_BILLING_PAYMENT_ERROR_ADD_COMMENT)
    fun addPaymentErrorComment(@RequestParam paymentErrorId: Long, @RequestParam text: String): ModelAndView {
        val myProfileId = myProfileId()
        verifyPlanetaAdminPermissions(myProfileId)
        paymentErrorCommentsService.insertOrUpdatePaymentErrorComments(myProfileId, paymentErrorId, text)
        return adminBaseControllerService.baseControllerService
                .createRedirectModelAndView(Urls.ADMIN_BILLING_PAYMENT_ERROR_COMMENT_LIST, "paymentErrorId", paymentErrorId)
    }

    @PostMapping(Urls.ADMIN_BILLING_PAYMENT_ERRORS_CHANGE_STATUS)
    @ResponseBody
    fun paymentErrorsChangeStatus(@RequestParam paymentErrorId: Long, @RequestParam paymentErrorStatus: PaymentErrorStatus): ActionStatus<*> {
        paymentErrorsService.changeStatus(myProfileId(), paymentErrorId, paymentErrorStatus)
        return ActionStatus.createSuccessStatus<Any>()
    }

    @PostMapping(Urls.ADMIN_CANCEL_ORDER)
    @ResponseBody
    fun cancelOrder(@RequestParam orderId: Long): ActionStatus<*> {
        val myProfileId = myProfileId()
        if (!adminBaseControllerService.permissionService.isSuperAdmin(myProfileId)) {
            throw PermissionException(MessageCode.USER_NOT_ADMIN)
        }
        orderService.cancelOrderQuietly(myProfileId, orderId)
        log.info("Оrder $orderId cancelled by user $myProfileId")
        return ActionStatus.createSuccessStatus<Any>()
    }

    @GetMapping(Urls.ADMIN_TRANSACTIONS_HISTORY)
    fun getTransactionsHistoryPage(@RequestParam(defaultValue = "0") offset: Int,
                                   @RequestParam(defaultValue = "20") limit: Int,
                                   @RequestParam profileId: Long): ModelAndView {
        val myProfileId = myProfileId()
        if (!adminBaseControllerService.permissionService.isSuperAdmin(myProfileId)) {
            throw PermissionException(MessageCode.USER_NOT_ADMIN)
        }

        val moneyTransactions = moneyTransactionService.getTransactions(myProfileId, profileId, null, null, offset, limit)
        return adminBaseControllerService.createAdminDefaultModelAndView(Actions.ADMIN_TRANSACTIONS_HISTORY)
                .addObject("offset", offset)
                .addObject("limit", limit)
                .addObject("count", offset + moneyTransactions.size + if (moneyTransactions.size == limit) 1 else 0)
                .addObject("profileId", profileId)
                .addObject("transactions", moneyTransactions)
    }

    companion object {
        private val log = Logger.getLogger(BillingController::class.java)
    }

}

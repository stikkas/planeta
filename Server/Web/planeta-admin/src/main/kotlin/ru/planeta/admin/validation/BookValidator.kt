package ru.planeta.admin.validation

/*
 * Created by Alexey on 06.06.2016.
 */

import org.springframework.stereotype.Component
import org.springframework.validation.Errors
import org.springframework.validation.Validator
import ru.planeta.model.bibliodb.Book

import java.math.BigDecimal

@Component
class BookValidator : Validator {

    override fun supports(clazz: Class<*>): Boolean {
        return Book::class.java.isAssignableFrom(clazz)
    }

    override fun validate(target: Any, errors: Errors) {
        val book = target as Book

        if (book.title.isBlank()) {
            errors.rejectValue("title", "field.required")
        }

        if (book.imageUrl.isBlank()) {
            errors.rejectValue("imageUrl", "must.download.cover.image")
        }

        if (book.description.isBlank()) {
            errors.rejectValue("description", "field.required")
        }

        if (book.price == BigDecimal.ZERO) {
            errors.rejectValue("price", "field.required")
        }

        if (book.tagIds.isEmpty()) {
            errors.rejectValue("tags", "atleast.one.tag.required")
        }
    }
}

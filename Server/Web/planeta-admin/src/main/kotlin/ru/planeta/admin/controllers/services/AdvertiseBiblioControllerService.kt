package ru.planeta.admin.controllers.services

import org.springframework.stereotype.Service
import ru.planeta.admin.Actions
import ru.planeta.admin.Biblio
import ru.planeta.api.model.ConfigurationType
import ru.planeta.api.model.biblio.Advertise
import ru.planeta.api.service.configurations.ConfigurationService

@Service
class AdvertiseBiblioControllerService(private val adminBaseControllerService: AdminBaseControllerService,
                                       private val configurationService: ConfigurationService) :
        BiblioConfigControllerService<Advertise>(Advertise::class.java, adminBaseControllerService, configurationService) {

    override val configName: String = ConfigurationType.BIBLIO_ADVERTISE_CONFIGURATION_LIST
    override val fillAction: Actions = Actions.ADMIN_BIBLIO_ADVERTISE_FILL
    override val editAction: Actions = Actions.ADMIN_BIBLIO_ADVERTISE_EDIT
    override val rootUrl: String = Biblio.ADMIN_ADVERTISE_LIST
    override val listAction: Actions = Actions.ADMIN_BIBLIO_ADVERTISE_LIST
}
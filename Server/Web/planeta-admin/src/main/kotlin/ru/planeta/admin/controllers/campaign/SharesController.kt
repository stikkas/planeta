package ru.planeta.admin.controllers.campaign

import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.ResponseBody
import org.springframework.web.servlet.ModelAndView
import ru.planeta.admin.Actions
import ru.planeta.admin.Urls
import ru.planeta.admin.controllers.services.BaseCampaignControllerService
import ru.planeta.api.service.campaign.CampaignService
import ru.planeta.api.web.authentication.myProfileId
import ru.planeta.model.common.PlanetaManager
import ru.planeta.model.common.campaign.CampaignNews
import ru.planeta.model.common.campaign.param.CampaignEventsParam
import ru.planeta.model.news.ProfileNews
import java.util.*
import javax.servlet.http.HttpServletResponse


/**
 * Date: 16.09.2015
 * Time: 13:44
 */
@Controller("CampaignSharesController")
class SharesController(private val campaignService: CampaignService,
                       private val baseCampaignControllerService: BaseCampaignControllerService) {

    @GetMapping(Urls.CAMPAIGN_SHARES)
    fun getCampaignShares(campaignEventsParam: CampaignEventsParam): ModelAndView {
        if (!baseCampaignControllerService.permissionService.hasAdministrativeRole(myProfileId())) {
            return baseCampaignControllerService.baseControllerService.createRedirectModelAndView(Urls.ADMIN_INDEX)
        }

        if (campaignEventsParam.profileNewsTypeSet == null) {
            campaignEventsParam.profileNewsTypeSet = EnumSet.of(ProfileNews.Type.SHARE_ADDED, ProfileNews.Type.SHARE_CHANGED,
                    ProfileNews.Type.SHARE_DELETED, ProfileNews.Type.SHARE_PURCHASED)
        }

        val modelAndView = baseCampaignControllerService.createCampaignEventsModelAndView(Actions.ADMIN_CAMPAIGN_SHARES, campaignEventsParam)
        val campaignNewsList = campaignService.selectCampaignNews(campaignEventsParam)
        baseCampaignControllerService.addCampaignNewsListToModel(campaignNewsList, modelAndView)

        return modelAndView
                .addObject("pageTitle", baseCampaignControllerService.baseControllerService
                        .getParametrizedMessage("campaign.shares.events"))
                .addObject("tab", "campaign-share-events")
                .addObject("commentsType", campaignEventsParam.commentsType)
                .addObject("reportJson", Urls.CAMPAIGN_SHARES_REPORT)

    }

    @ResponseBody
    @RequestMapping(Urls.CAMPAIGN_SHARES_REPORT)
    fun getCampaignSharesReport(response: HttpServletResponse, campaignEventsParam: ReportCampaignEventsParam) {
        val modelAndView = getCampaignShares(campaignEventsParam)

        val campaignNewsList = modelAndView.model["campaignNewsList"] as List<CampaignNews>
        val managersForCurrentCampaigns = modelAndView.model["campaignManagerList"] as List<PlanetaManager>

        val report = campaignEventsParam.reportType.createReport("campaign_shares_report_${myProfileId()}")
        report.addCaptionRow(*baseCampaignControllerService.baseControllerService
                .getParametrizedMessage("campaign.shares.report").split(";".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray())
        for (i in campaignNewsList.indices) {
            val campaignNews = campaignNewsList[i]
            val planetaManager = managersForCurrentCampaigns[i]
            report.addRow(campaignNews.campaign.campaignId, campaignNews.campaign.name, getCyrillicName(campaignNews.profileNews.type),
                    campaignNews.profileNews.extraParamsMap["shareName"], campaignNews.profileNews.timeAdded, if (planetaManager == null) "" else planetaManager.fullName
            )
        }

        report.addToResponse(response)
    }

    private fun getCyrillicName(type: ProfileNews.Type): String {
        val code: String = when (type) {
            ProfileNews.Type.SHARE_ADDED -> "added"
            ProfileNews.Type.SHARE_DELETED -> "deleted"
            ProfileNews.Type.SHARE_PURCHASED -> "purchased"
            else -> "unknown"
        }
        return baseCampaignControllerService.baseControllerService.getParametrizedMessage(code)
    }

}

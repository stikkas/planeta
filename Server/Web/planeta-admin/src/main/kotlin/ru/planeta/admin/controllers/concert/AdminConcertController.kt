package ru.planeta.admin.controllers.concert

import org.springframework.stereotype.Controller
import org.springframework.validation.BindingResult
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.ResponseBody
import org.springframework.web.servlet.ModelAndView
import ru.planeta.admin.Actions
import ru.planeta.admin.ConcertUrls
import ru.planeta.admin.controllers.services.AdminBaseControllerService
import ru.planeta.api.exceptions.PermissionException
import ru.planeta.api.model.ActionStatus
import ru.planeta.api.service.concert.ConcertImportService
import ru.planeta.api.service.concert.ConcertService
import ru.planeta.api.web.authentication.isAdmin
import ru.planeta.model.concert.Concert
import ru.planeta.model.concert.ConcertStatus
import java.util.*

/**
 * Created with IntelliJ IDEA.
 * User: michail
 * Date: 18.10.16
 * Time: 12:05
 */

@Controller
class AdminConcertController(private val concertService: ConcertService,
                             private val concertImportService: ConcertImportService,
                             private val adminBaseControllerService: AdminBaseControllerService) {
    @GetMapping(ConcertUrls.CONCERT_LIST)
    fun listConcerts(@RequestParam(defaultValue = "10") limit: Int,
                     @RequestParam(defaultValue = "0") offset: Int): ModelAndView {
        val count = concertService.concertsCount
        val concertStatuses = EnumSet.allOf(ConcertStatus::class.java)
        val concerts = concertService.selectList(offset, limit)
        return adminBaseControllerService.createAdminDefaultModelAndView(Actions.ADMIN_CONCERTS_LIST)
                .addObject("concerts", concerts)
                .addObject("offset", offset)
                .addObject("limit", limit)
                .addObject("count", count)
                .addObject("concertStatuses", concertStatuses)
    }

    @GetMapping(ConcertUrls.CONCERT)
    fun editConcert(@RequestParam("concertId") concertId: Long): ModelAndView {
        val concert = if (concertId > 0) concertService.getConcertSafe(concertId) else Concert()
        val concertStatuses = EnumSet.allOf(ConcertStatus::class.java)

        return adminBaseControllerService.createAdminDefaultModelAndView(Actions.ADMIN_CONCERT_EDIT)
                .addObject("concert", concert)
                .addObject("concertStatuses", concertStatuses)
    }

    @PostMapping(ConcertUrls.SAVE_CONCERT)
    fun editConcert(concert: Concert, result: BindingResult): ModelAndView {
        // TODO need validator, need to analyze result.hasErrors()
        concertService.insertOrUpdateConcert(concert)
        return adminBaseControllerService.baseControllerService.createRedirectModelAndView(ConcertUrls.CONCERT_LIST)
    }

    @GetMapping(ConcertUrls.IMPORT_CONCERTS)
    @ResponseBody
    fun importConcerts(): ActionStatus<*> {
        if (!isAdmin()) {
            throw PermissionException()
        }
        concertImportService.importMoscowShowConcerts()
        return ActionStatus.createSuccessStatus<Any>()
    }
}


package ru.planeta.admin.controllers.biblio

import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.servlet.ModelAndView
import ru.planeta.admin.Actions
import ru.planeta.admin.Biblio
import ru.planeta.admin.controllers.services.AdminBaseControllerService
import ru.planeta.api.exceptions.MessageCode
import ru.planeta.api.exceptions.PermissionException
import ru.planeta.api.search.SearchService
import ru.planeta.api.search.filters.SearchOrderFilter
import ru.planeta.api.web.authentication.myProfileId

/**
 * Created with IntelliJ IDEA.
 * User: michail
 * Date: 01.06.16
 * Time: 17:07
 */
@Controller
class BiblioOrderController(private val searchService: SearchService,
                            private val adminBaseControllerService: AdminBaseControllerService) {
    @GetMapping(Biblio.ADMIN_BIBLIO_ORDERS)
    fun getBiblioOrders(searchOrderFilter: SearchOrderFilter): ModelAndView {
        if (!adminBaseControllerService.permissionService.isPlanetaAdmin(myProfileId())) {
            throw PermissionException(MessageCode.USER_NOT_ADMIN)
        }

        if (searchOrderFilter.limit == 0) {
            searchOrderFilter.limit = 10
        }

        val orderInfoSearchResult = searchService.searchForBiblioOrders(searchOrderFilter)

        return adminBaseControllerService.createAdminDefaultModelAndView(Actions.ADMIN_BIBLIO_ORDERS)
                .addObject("ordersInfo", orderInfoSearchResult.searchResultRecords)
                .addObject("offset", searchOrderFilter.offset)
                .addObject("limit", searchOrderFilter.limit)
                .addObject("count", orderInfoSearchResult.estimatedCount)
    }
}

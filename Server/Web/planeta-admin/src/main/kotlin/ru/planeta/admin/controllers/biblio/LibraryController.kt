package ru.planeta.admin.controllers.biblio

import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.*
import org.springframework.web.servlet.ModelAndView
import ru.planeta.admin.Actions
import ru.planeta.admin.LibraryUrls
import ru.planeta.admin.controllers.services.AdminBaseControllerService
import ru.planeta.api.model.ActionStatus
import ru.planeta.api.search.SearchService
import ru.planeta.api.service.biblio.LibraryService
import ru.planeta.api.service.notifications.NotificationService
import ru.planeta.api.web.utils.ExcelReportUtils
import ru.planeta.commons.web.WebUtils
import ru.planeta.model.bibliodb.Library
import ru.planeta.model.bibliodb.LibraryFilter
import ru.planeta.model.bibliodb.enums.BiblioObjectStatus
import java.util.*
import javax.servlet.http.HttpServletResponse

@Controller
class LibraryController(private val libraryService: LibraryService,
                        private val searchService: SearchService,
                        private val notificationService: NotificationService,
                        private val adminBaseControllerService: AdminBaseControllerService) {

    @Value("\${admin.files.upload.path:}")
    lateinit var uploadFilesPath: String

    @GetMapping(LibraryUrls.LIBRARIES)
    fun getLibraries(filter: LibraryFilter): ModelAndView {

        if (filter.limit == 0) {
            filter.limit = 10
        }
        val librarySearchResult = searchService.searchForLibraries(filter)
        return adminBaseControllerService.createAdminDefaultModelAndView(Actions.ADMIN_BIBLIO_LIBRARIES)
                .addObject("offset", filter.offset)
                .addObject("limit", filter.limit)
                .addObject("searchStr", filter.searchStr)
                .addObject("status", filter.statuses)
                .addObject("libraryType", filter.libraryType)
                .addObject("dateFrom", filter.dateFrom)
                .addObject("dateTo", filter.dateTo)
                .addObject("libraries", librarySearchResult.searchResultRecords)
                .addObject("count", librarySearchResult.estimatedCount)
    }

    @GetMapping(LibraryUrls.LIBRARIES_REPORT)
    fun getLibsReport(filter: LibraryFilter, response: HttpServletResponse) {
        filter.limit = Integer.MAX_VALUE;
        filter.offset = 0;

        if (filter.statuses == null) {
            filter.statuses = EnumSet.of(BiblioObjectStatus.ACTIVE, BiblioObjectStatus.PAUSED)
        }
        val libraries = searchService.searchForLibraries(filter).searchResultRecords

        ExcelReportUtils.setReportHeaders(response)
        ExcelReportUtils.generateLibraryReport(libraries, response.writer)
    }


    private fun createLibraryMAV(): ModelAndView {
        return adminBaseControllerService.createAdminDefaultModelAndView(Actions.ADMIN_BIBLIO_LIBRARY)
                .addObject("biblioObjectStatuses", BiblioObjectStatus.values())
    }

    @GetMapping(LibraryUrls.LIBRARY)
    fun getLibrary(@RequestParam libraryId: Long): ModelAndView =
            createLibraryMAV().addObject("library", libraryService.getLibrary(libraryId))

    @PostMapping(LibraryUrls.DELETE_LIBRARY)
    @ResponseBody
    fun deleteLibrary(@RequestParam libraryId: Long): ActionStatus<*> {
        libraryService.delete(libraryService.getLibrary(libraryId))
        return ActionStatus.createSuccessStatus<Any>()
    }

    @GetMapping(LibraryUrls.ADD_LIBRARY)
    fun addLibrary(): ModelAndView = createLibraryMAV().addObject("library", Library())

    @PostMapping(LibraryUrls.LIBRARY)
    fun saveLibrary(@ModelAttribute library: Library): ModelAndView {
        libraryService.save(library, notificationService)
        val returnUrl = WebUtils.createUrl(LibraryUrls.LIBRARY, WebUtils.Parameters().add("libraryId", library.libraryId).params)
        return adminBaseControllerService.baseControllerService.createRedirectModelAndView(returnUrl)
    }

}

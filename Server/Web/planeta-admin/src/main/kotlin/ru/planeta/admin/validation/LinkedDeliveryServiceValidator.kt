package ru.planeta.admin.validation

import org.springframework.stereotype.Component
import org.springframework.validation.Errors
import org.springframework.validation.ValidationUtils
import org.springframework.validation.Validator
import ru.planeta.model.common.delivery.LinkedDelivery

import java.math.BigDecimal

/**
 * @author Andrew.Arefyev@gmail.com
 * 07.11.13 23:15
 */
@Component
class LinkedDeliveryServiceValidator : Validator {
    override fun supports(clazz: Class<*>): Boolean {
        return LinkedDelivery::class.java.isAssignableFrom(clazz)
    }

    override fun validate(target: Any, errors: Errors) {
        ValidationUtils.rejectIfEmpty(errors, "serviceId", "field.required")
        ValidationUtils.rejectIfEmpty(errors, "shareId", "field.required")
        //        ValidationUtils.rejectIfEmpty(errors, "publicNote", "field.required");
        ValidationUtils.rejectIfEmpty(errors, "price", "field.required")
        val delivery = target as LinkedDelivery
        if (delivery.price != null) {
            if (delivery.price < BigDecimal.ZERO) {
                errors.rejectValue("price", "positive.required")
            }
        }
    }
}

package ru.planeta.admin.controllers.charity

import org.springframework.stereotype.Controller
import org.springframework.validation.BindingResult
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.ModelAttribute
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.servlet.ModelAndView
import ru.planeta.admin.Actions
import ru.planeta.admin.Urls
import ru.planeta.admin.controllers.services.BasePromoControllerService
import ru.planeta.admin.controllers.services.PartnerCharityControllerService
import ru.planeta.api.model.promo.PartnersPromoConfiguration
import javax.validation.Valid

@Controller
class PartnerCharityController(private val partnerCharityControllerService: PartnerCharityControllerService) {

    @GetMapping(Urls.ADMIN_CHARITY_PARTNERS_PROMO_LIST)
    fun charityPartnersList(): ModelAndView = partnerCharityControllerService.promoList()

    @GetMapping(Urls.ADMIN_CHARITY_PARTNERS_PROMO_FILL)
    fun promoFill(): ModelAndView = partnerCharityControllerService
            .adminBaseControllerService.createAdminDefaultModelAndView(Actions.ADMIN_CHARITY_PARTNERS_PROMO_FILL)
            .addObject(BasePromoControllerService.CONFIGURATION_ATTR_NAME, PartnersPromoConfiguration())

    @PostMapping(Urls.ADMIN_CHARITY_PARTNERS_PROMO_ADD)
    fun promoAddPost(@Valid @ModelAttribute(value = BasePromoControllerService.CONFIGURATION_ATTR_NAME)
                     mainConfiguration: PartnersPromoConfiguration, bindingResult: BindingResult): ModelAndView =
            partnerCharityControllerService.promoAddPost(mainConfiguration, bindingResult)

    @GetMapping(Urls.ADMIN_CHARITY_PARTNERS_PROMO_DELETE)
    fun promoDeleteGet(@RequestParam id: Int): ModelAndView = partnerCharityControllerService.promoDeleteGet(id)

    @GetMapping(Urls.ADMIN_CHARITY_PARTNERS_PROMO_EDIT)
    fun promoEditGet(@ModelAttribute(value = BasePromoControllerService.CONFIGURATION_ATTR_NAME) mainConfiguration: PartnersPromoConfiguration): ModelAndView =
            partnerCharityControllerService.promoEditGet(mainConfiguration)

    @PostMapping(Urls.ADMIN_CHARITY_PARTNERS_PROMO_EDIT)
    fun promoEditPost(@Valid @ModelAttribute(value = BasePromoControllerService.CONFIGURATION_ATTR_NAME)
                      mainConfiguration: PartnersPromoConfiguration, bindingResult: BindingResult): ModelAndView =
            partnerCharityControllerService.promoEditPost(mainConfiguration, bindingResult)

    @PostMapping(Urls.ADMIN_CHARITY_PARTNERS_PROMO_SORT)
    fun promoSort(@RequestParam startIndex: Int,
                  @RequestParam stopIndex: Int): ModelAndView = partnerCharityControllerService.promoSort(startIndex, stopIndex)
}

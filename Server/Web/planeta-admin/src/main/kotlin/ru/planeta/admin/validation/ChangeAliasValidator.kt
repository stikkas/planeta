package ru.planeta.admin.validation

import org.apache.commons.lang3.StringUtils
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.MessageSource
import org.springframework.stereotype.Component
import org.springframework.validation.Errors
import org.springframework.validation.Validator
import ru.planeta.admin.models.ChangeAlias
import ru.planeta.api.utils.ValidateUtils

/**
 * Date: 29.10.12
 * Time: 16:50
 */
@Component
class ChangeAliasValidator : Validator {
    @Autowired
    private val messageSource: MessageSource? = null

    override fun supports(aClass: Class<*>): Boolean {
        return aClass.isAssignableFrom(ChangeAlias::class.java)
    }

    override fun validate(o: Any?, errors: Errors) {
        if (o == null || StringUtils.isEmpty((o as ChangeAlias).aliasName)) {
            errors.reject("field.required")
        }

        ValidateUtils.rejectIfNotProfileAlias(errors, "aliasName", "wrong.chars")
        ValidateUtils.rejectIfNotProfileAliasHasLetter(errors, "aliasName", "field.error.alias")
        ValidateUtils.rejectIfReservedWord(errors, "aliasName", "field.error.exists")
        ValidateUtils.rejectIfSizeIsTooLarge(errors, "aliasName", 64, messageSource)
        ValidateUtils.rejectIfSizeIsTooSmall(errors, "aliasName", "field.length.short.4", 4, true)
    }
}

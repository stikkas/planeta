package ru.planeta.admin.controllers.shop

import org.apache.commons.lang3.StringUtils
import org.apache.log4j.Logger
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Controller
import org.springframework.validation.BindingResult
import org.springframework.web.bind.annotation.*
import org.springframework.web.servlet.ModelAndView
import ru.planeta.admin.Actions
import ru.planeta.admin.Shop
import ru.planeta.admin.controllers.services.BaseShopControllerService
import ru.planeta.admin.models.ProductEdit
import ru.planeta.api.model.ActionStatus
import ru.planeta.api.news.LoggerService
import ru.planeta.api.service.profile.ProfileService
import ru.planeta.api.service.security.PermissionService
import ru.planeta.api.service.shop.ProductUsersService
import ru.planeta.api.web.authentication.myProfileId
import ru.planeta.api.web.controllers.services.BaseControllerService
import ru.planeta.model.news.ProfileNews
import ru.planeta.model.profile.Profile
import ru.planeta.model.shop.Category
import ru.planeta.model.shop.Product
import ru.planeta.model.shop.ProductInfo
import ru.planeta.model.shop.enums.ProductCategory
import java.util.*
import javax.servlet.http.HttpServletRequest
import javax.validation.Valid

/**
 * Controller for creating and editing products and group of products
 * User: a.savanovich
 * Date: 02.07.12
 * Time: 16:07
 */
@Controller
class ProductEditController(private val profileNewsService: LoggerService,
                            private val productUsersService: ProductUsersService,
                            private val profileService: ProfileService,
                            private val permissionService: PermissionService,
                            private val baseControllerService: BaseControllerService,
                            @Value("\${public.cdn.url:}")
                            private val resourceHost: String,
                            private val baseShopControllerService: BaseShopControllerService) {

    //@Value("https://${private.cdn.host}/")
    // url for shop admins
    private val logger = Logger.getLogger(ProductEditController::class.java)

    @GetMapping(Shop.PRODUCT_CREATE)
    fun productCreate(@PathVariable("groupAlias") groupAlias: String,
                      @RequestParam(defaultValue = "0") parentId: Long,
                      request: HttpServletRequest): ModelAndView {

        val clientId = myProfileId()

        val group = getGroupForAdmin(groupAlias)

        val product = ProductInfo()
        product.parentProductId = parentId
        product.merchantProfileId = group.profileId
        product.storeId = group.profileId
        product.referrerId = 0

        return baseShopControllerService.createDefaultAdminModelAndView(Actions.ADMIN_PRODUCT_EDIT)
                .addObject("product", product)
                .addObject("currentGroup", group)
                .addObject("productCategories", ProductCategory.values())
                .addObject("resourceHost", resourceHost)
                .addObject("productGroup", getProductGroup(clientId, product))
    }

    @GetMapping(Shop.PRODUCT_EDIT)
    fun productEditView(@PathVariable("groupAlias") groupAlias: String,
                        @PathVariable("productId") productId: Long,
                        @RequestParam(defaultValue = "START") stage: ProductEditStages,
                        request: HttpServletRequest): ModelAndView {

        val clientId = myProfileId()
        val product = productUsersService.getProductCurrent(clientId, productId)
        product.referrer = profileService.getProfile(product.referrerId)

        return baseShopControllerService.createDefaultAdminModelAndView(Actions.ADMIN_PRODUCT_EDIT)
                .addObject("stage", stage)
                .addObject("product", product)
                .addObject("currentGroup", getGroupForAdmin(groupAlias))
                .addObject("productCategories", ProductCategory.values())
                .addObject("resourceHost", resourceHost)
                .addObject("productGroup", getProductGroup(clientId, product))
    }


    @GetMapping(Shop.PRODUCT_EDIT_NEW)
    fun productEditNewView(@PathVariable("productId") productId: Long): ModelAndView {
        val clientId = myProfileId()
        val product: ProductInfo

        if (productId != 0L) {
            product = productUsersService.getProductCurrent(myProfileId(), productId)
        } else {
            product = ProductInfo()
            product.merchantProfileId = DEFAULT_PRODUCT_MERCHANT_PROFILE_ID
        }
        product.referrer = profileService.getProfile(product.referrerId)
        return baseShopControllerService.createDefaultAdminModelAndView(Actions.ADMIN_PRODUCT_EDIT_NEW)
                .addObject("product", product)
                .addObject("productCategories", ProductCategory.values())
                .addObject("resourceHost", resourceHost)
                .addObject("productGroup", getProductGroup(clientId, product))
    }


    private fun getGroupForAdmin(groupAlias: String): Profile {
        val group = profileService.getProfileSafe(groupAlias)
        permissionService.checkIsAdmin(myProfileId(), group.profileId)
        return group
    }

    @PostMapping(Shop.PRODUCT_SAVE)
    @ResponseBody
    fun productSave(@Valid @RequestBody product: ProductEdit, result: BindingResult): ActionStatus<ProductInfo> {
        if (result.hasErrors()) {
            return baseControllerService.createErrorStatus(result)
        }

        val myProfileId = myProfileId()
        product.referrerId = if (StringUtils.isNotBlank(product.referrerWebAlias)) profileService.getProfileSafe(product.referrerWebAlias).profileId else 0

        val savedProduct = productUsersService.saveProduct(myProfileId, product)
        profileNewsService.addProfileNews(ProfileNews.Type.ADMIN_SAVE_PRODUCT, myProfileId, product.productId)
        logger.info(String.format("Moderation request for product %d was approved by user №%d with reason %s.",
                product.productId, myProfileId, if (product.productId > 0) "edited" else "created"))

        return ActionStatus.createSuccessStatus(productUsersService.getProductCurrent(myProfileId, savedProduct.productId))
    }

    @PostMapping(Shop.PRODUCT_TAGS_SAVE)
    @ResponseBody
    fun productTagsSave(@RequestBody product: Product,
                        result: BindingResult): ActionStatus<List<Category>> {
        if (product.tags.isEmpty()) {
            result.rejectValue("tags", "product.no.tags")
        }

        if (result.hasErrors()) {
            return baseControllerService.createErrorStatus(result)
        }

        val myProfileId = myProfileId()
        val savedTags = productUsersService.updateProductTags(myProfileId, product.productId, product.tags);

        val tagsMap: Map<String, String> = product.tags.map { it.mnemonicName to it.categoryId.toString() }.toMap()
        profileNewsService.addProfileNews(ProfileNews.Type.CHANGED_PRODUCT_TAGS, myProfileId, product.productId, 0, tagsMap)
        logger.info(String.format("UserId = %d changed tags for productId = %d with tags: %s",
                myProfileId, product.productId, product.tags.map { it.mnemonicName}.toList()))
        return ActionStatus.createSuccessStatus(savedTags)
    }

    @PostMapping(Shop.PRODUCT_PAUSE)
    @ResponseBody
    fun productPause(@RequestParam(value = "productId") productId: Long): ActionStatus<*> {

        productUsersService.pauseProduct(myProfileId(), productId)
        return ActionStatus.createSuccessStatus<Any>()
    }

    @PostMapping(Shop.PRODUCT_START)
    @ResponseBody
    fun productStart(@RequestParam(value = "productId") productId: Long): ActionStatus<*> {

        productUsersService.startProduct(myProfileId(), productId)
        return ActionStatus.createSuccessStatus<Any>()
    }

    @PostMapping(Shop.PRODUCT_DELETE)
    @ResponseBody
    fun productDelete(@RequestParam productId: Long): ActionStatus<*> {
        val myProfileId = myProfileId()
        productUsersService.deleteProduct(myProfileId, productId)
        profileNewsService.addProfileNews(ProfileNews.Type.ADMIN_DELETE_PRODUCT, myProfileId, productId)
        return ActionStatus.createSuccessStatus<Any>()
    }

    @PostMapping(Shop.PRODUCT_UPDATE_QUANTITY)
    @ResponseBody
    fun productSave(@RequestParam productId: Long,
                    @RequestParam quantity: Int): ActionStatus<*> {

        productUsersService.updateProductTotalQuantity(myProfileId(), productId, quantity)
        return ActionStatus.createSuccessStatus<Any>()
    }

    private fun getProductGroup(clientId: Long, product: ProductInfo): Product? =
            if (product.parentProductId > 0)
                productUsersService.getProductCurrent(clientId, product.parentProductId)
            else
                null


    @PostMapping(Shop.PRODUCT_BIND_TO_PROFILE)
    @ResponseBody
    fun productBindToProfile(@RequestParam productId: Long,
                             @RequestParam referrerWebAlias: String,
                             @RequestParam(defaultValue = "true") showOnCampaign: Boolean,
                             @RequestParam(required = false) campaignIdsString: String?): ActionStatus<Profile> {

        permissionService.checkAdministrativeRole(myProfileId())
        val profile = profileService.getProfileSafe(referrerWebAlias)
        val campaignIds = ArrayList<Long>()
        if (campaignIdsString != null && !StringUtils.isEmpty(campaignIdsString))
            campaignIdsString.split(",".toRegex()).filter { !StringUtils.isEmpty(it) }.forEach { campaignIds.add(java.lang.Long.parseLong(it.trim())) }
        productUsersService.bindToProfile(productId, profile.profileId, showOnCampaign, campaignIds.toLongArray())

        return ActionStatus.createSuccessStatus(profile)
    }

    @GetMapping(Shop.PRODUCT_FIND_DONATE_BY_PROFILE)
    @ResponseBody
    fun productFindDonateByProfile(@RequestParam referrerWebAlias: String): ActionStatus<Long> {
        permissionService.checkAdministrativeRole(myProfileId())
        val profile = profileService.getProfileSafe(referrerWebAlias)

        return try {
            val donateId = productUsersService.getDonateProductByReferrer(profile.profileId)
            ActionStatus.createSuccessStatus(donateId)
        } catch (ex: Exception) {
            logger.error(ex)
            ActionStatus.createErrorStatus("shop.no.donate.for.referrer", baseControllerService.messageSource)
        }
    }

    @PostMapping(Shop.PRODUCT_CLONE)
    @ResponseBody
    fun productBindToProfile(@RequestParam productId: Long): ActionStatus<Long> {
        val myProfileId = myProfileId()
        permissionService.checkAdministrativeRole(myProfileId)
        val clone = productUsersService.cloneProduct(myProfileId, productId)
        return ActionStatus.createSuccessStatus(clone.productId)
    }

    companion object {
        private val DEFAULT_PRODUCT_MERCHANT_PROFILE_ID: Long = 21765
    }
}

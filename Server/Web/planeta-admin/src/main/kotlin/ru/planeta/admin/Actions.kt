package ru.planeta.admin

import ru.planeta.api.web.controllers.IAction

/**
 * Forming path to jsp
 *
 * @author ameshkov
 */
enum class Actions(prefix: ActionPrefixes = ActionPrefixes.EMPTY, private val _text: String) : IAction {

    // <editor-fold defaultstate="collapsed" desc="Common member zone actions">
    ERROR(_text = "error"),
    LOGIN(_text = "login"),
    DASHBOARD(ActionPrefixes.ADMIN, "dashboard"),
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Admin actions">
    ADMIN(ActionPrefixes.ADMIN, "index"),
    ADMIN_GROUP_INFO(ActionPrefixes.ADMIN, "group-info"),
    ADMIN_USERS(ActionPrefixes.ADMIN, "users"),
    ADMIN_CONTRACTORS(ActionPrefixes.ADMIN, "contractors"),
    ADMIN_USER_INFO(ActionPrefixes.ADMIN, "user-info"),
    ADMIN_EVENTS(ActionPrefixes.ADMIN, "events"),
    ADMIN_TICKETS(ActionPrefixes.ADMIN, "tickets"),
    ADMIN_BROADCASTS(ActionPrefixes.ADMIN, "broadcasts"),
    ADMIN_EVENT_INFO(ActionPrefixes.ADMIN, "event-info"),
    ADMIN_SEMINARS(ActionPrefixes.SCHOOL, "seminars"),
    ADMIN_SEMINAR_INFO(ActionPrefixes.SCHOOL, "seminar-info"),

    ADMIN_CONFIGURATIONS(ActionPrefixes.ADMIN, "configuration-list"),
    ADMIN_CONFIGURATION_EDIT(ActionPrefixes.ADMIN, "configuration-edit"),
    ADMIN_BANNERS(ActionPrefixes.ADMIN, "banner-list"),
    ADMIN_EDIT_BANNER(ActionPrefixes.ADMIN, "edit-banner"),
    ADMIN_LINKS(ActionPrefixes.ADMIN, "short-link-list"),
    ADMIN_EDIT_LINK(ActionPrefixes.ADMIN, "edit-short-link"),
    ADMIN_CATEGORIES(ActionPrefixes.ADMIN, "campaign-tags"),
    ADMIN_PAYMENT_CONFIGURATOR(ActionPrefixes.ADMIN, "payment-configurator"),
    ADMIN_EDIT_CATEGORY(ActionPrefixes.ADMIN, "edit-category"),
    ADMIN_EDIT_BANNER_SIMPLE(ActionPrefixes.ADMIN, "edit-banner-simple"),
    ADMIN_SPONSORS(ActionPrefixes.ADMIN, "sponsors"),
    ADMIN_EDIT_SPONSOR(ActionPrefixes.ADMIN, "edit-sponsor"),
    ADMIN_PROMO_CONFIG_LIST(ActionPrefixes.ADMIN, "promo-config-list"),
    ADMIN_PROMO_CONFIG(ActionPrefixes.ADMIN, "promo-config"),
    ADMIN_STATISTIC_FORECAST(ActionPrefixes.ADMIN, "statistic-forecast"),
    ADMIN_STATISTIC_COMPOSITE(ActionPrefixes.ADMIN, "statistic-composite"),
    ADMIN_STATISTIC_SHARE_PURCHASE(ActionPrefixes.ADMIN, "statistic-purchase-share"),
    ADMIN_STATISTIC_PRODUCT_PURCHASE(ActionPrefixes.ADMIN, "statistic-purchase-product"),
    ADMIN_STATISTIC_REGISTRATION_MODAL(ActionPrefixes.ADMIN, "statistic-registration-user-modal"),
    ADMIN_STATISTIC_SHARE_PURCHASE_MODAL(ActionPrefixes.ADMIN, "statistic-purchase-share-modal"),
    ADMIN_STATISTIC_TICKET_PURCHASE_MODAL(ActionPrefixes.ADMIN, "statistic-purchase-ticket-modal"),
    ADMIN_STATISTIC_PRODUCT_PURCHASE_MODAL(ActionPrefixes.ADMIN, "statistic-purchase-product-modal"),
    ADMIN_CAMPAIGN_ADDRESSES_STICKERS(ActionPrefixes.ADMIN, "campaign-addresses-stickers"),
    ADMIN_CAMPAIGN_ADDRESSES_STICKERS_FROM_FILE(ActionPrefixes.ADMIN, "campaign-addresses-stickers-from-file"),
    ADMIN_SHOP_ORDERS(ActionPrefixes.ADMIN, "shop-orders"),
    ADMIN_ONE_ORDER(ActionPrefixes.ADMIN, "one-order"),
    ADMIN_SHOP_DELIVERY_LIST(ActionPrefixes.ADMIN, "shop-delivery-list"),
    ADMIN_SHOP_PROMO_CODES(ActionPrefixes.ADMIN, "shop-promo-codes"),
    ADMIN_SHOP_PROMO_CODE(ActionPrefixes.ADMIN, "shop-promo-code-edit"),
    ADMIN_SHOP_PRODUCT_TAGS(ActionPrefixes.SHOP, "shop-product-tags"),
    ADMIN_SHOP_PRODUCT_TAG(ActionPrefixes.SHOP, "shop-product-tag"),

    //
    ADMIN_START_PROMO_LIST(ActionPrefixes.PROMO, "start-promo"),
    ADMIN_CHARITY_PROMO_LIST(ActionPrefixes.PROMO, "charity-promo"),
    //
    ADMIN_TV_PROMO_ADD(ActionPrefixes.PROMO, "tv-promo-add"),
    ADMIN_TV_PROMO_EDIT(ActionPrefixes.PROMO, "tv-promo-edit"),
    ADMIN_TV_PROMO_LIST(ActionPrefixes.PROMO, "tv-promo-list"),
    //
    ADMIN_STORE_INFO(ActionPrefixes.SHOP, "store-info"),
    ADMIN_PRODUCT_EDIT(ActionPrefixes.SHOP, "product-edit"),
    ADMIN_PRODUCT_EDIT_NEW(ActionPrefixes.SHOP, "product-edit-new"),
    ADMIN_PRODUCT_COMMENTS(ActionPrefixes.SHOP, "comments"),


    //
    ADMIN_BROADCAST_PROMO_FILL(ActionPrefixes.PROMO, "broadcast-promo-fill"),
    ADMIN_BROADCAST_PROMO_ADD(ActionPrefixes.PROMO, "broadcast-promo-add"),
    ADMIN_BROADCAST_PROMO_EDIT(ActionPrefixes.PROMO, "broadcast-promo-edit"),
    ADMIN_BROADCAST_PROMO_LIST(ActionPrefixes.PROMO, "broadcast-promo-list"),
    //
    ADMIN_ABOUT_US_NEWS_PROMO_ADD(ActionPrefixes.PROMO, "about-us-news-promo-add"),
    ADMIN_ABOUT_US_NEW_SETTINGS(ActionPrefixes.PROMO, "about-us-new-settings"),
    //
    ADMIN_PARTNERS_PROMO_FILL(ActionPrefixes.PROMO, "partners-promo-fill"),
    ADMIN_PARTNERS_PROMO_ADD(ActionPrefixes.PROMO, "partners-promo-add"),
    ADMIN_PARTNERS_PROMO_EDIT(ActionPrefixes.PROMO, "partners-promo-edit"),
    ADMIN_PARTNERS_PROMO_LIST(ActionPrefixes.PROMO, "partners-promo-list"),
    ADMIN_CAMPUSES(ActionPrefixes.PROMO, "campus-list"),
    ADMIN_EDIT_CAMPUS(ActionPrefixes.PROMO, "campus-edit"),
    //
    ADMIN_CHARITY_PARTNERS_PROMO_FILL(ActionPrefixes.PROMO, "charity-partners-promo-fill"),
    ADMIN_CHARITY_PARTNERS_PROMO_ADD(ActionPrefixes.PROMO, "charity-partners-promo-add"),
    ADMIN_CHARITY_PARTNERS_PROMO_EDIT(ActionPrefixes.PROMO, "charity-partners-promo-edit"),
    ADMIN_CHARITY_PARTNERS_PROMO_LIST(ActionPrefixes.PROMO, "charity-partners-promo-list"),
    //
    ADMIN_CHARITY_SCHOOL_LIBRARY_FILES(ActionPrefixes.CHARITY, "charity-school-library-files"),
    ADMIN_CHARITY_SCHOOL_LIBRARY_FILE_EDIT(ActionPrefixes.CHARITY, "charity-school-library-file-edit"),
    ADMIN_CHARITY_SCHOOL_LIBRARY_THEMES(ActionPrefixes.CHARITY, "charity-school-library-themes"),
    ADMIN_CHARITY_SCHOOL_LIBRARY_THEME_EDIT(ActionPrefixes.CHARITY, "charity-school-library-file-theme-edit"),
    //
    ADMIN_CHARITY_POSTS(ActionPrefixes.CHARITY, "admin-charity-posts"),
    ADMIN_POSTS(ActionPrefixes.ADMIN, "admin-posts"),
    ADMIN_POST(ActionPrefixes.ADMIN, "admin-post"),
    //
    ADMIN_REDIRECT_PROMO_PREVIEW(ActionPrefixes.PROMO, "redirect-promo-preview"),
    //
    ADMIN_IMAGE_UPLOAD(ActionPrefixes.ADMIN, "image-upload"),
    ADMIN_FILE_UPLOAD(ActionPrefixes.ADMIN, "file-upload"),
    ADMIN_DOCUMENT_UPLOAD(ActionPrefixes.ADMIN, "document-upload"),
    ADMIN_DOCUMENT(ActionPrefixes.ADMIN, "admin-document"),
    ADMIN_GIFTS(ActionPrefixes.ADMIN, "gifts"),
    //campaigns
    ADMIN_CAMPAIGN_MANAGERS_RELATIONS(ActionPrefixes.ADMIN, "campaign-managers-relations"),
    ADMIN_CAMPAIGNS(ActionPrefixes.ADMIN, "campaigns"),
    ADMIN_MANAGER_CAMPAIGNS(ActionPrefixes.ADMIN, "manager-campaigns"),
    ADMIN_CAMPAIGN_INFO(ActionPrefixes.ADMIN, "campaign-info"),
    ADMIN_CAMPAIGN_MODERATION_INFO(ActionPrefixes.ADMIN, "campaign-moderation-info"),
    ADMIN_DELIVERY_SERVICES(ActionPrefixes.ADMIN, "delivery-services"),

    // admin new actions
    ADMIN_EVENT_TICKET_INFO(ActionPrefixes.ADMIN, "event-ticket-info"),
    ADMIN_EVENT_TICKET_LIST(ActionPrefixes.ADMIN, "event-ticket-list"),
    ADMIN_BROADCAST_INFO(ActionPrefixes.ADMIN, "event-broadcast-info"),
    ADMIN_BROADCAST_STREAM_INFO(ActionPrefixes.ADMIN, "event-broadcast-stream-info"),
    ADMIN_BROADCAST_GEO_TARGETING(ActionPrefixes.ADMIN, "event-broadcast-geo-targeting"),
    ADMIN_BROADCAST_BACKERS_TARGETING(ActionPrefixes.ADMIN, "event-broadcast-backers-targeting"),
    ADMIN_BROADCAST_PRODUCT_TARGETING(ActionPrefixes.ADMIN, "event-broadcast-product-targeting"),
    ADMIN_BROADCAST_PRIVATE_TARGETING(ActionPrefixes.ADMIN, "event-broadcast-private-targeting"),
    ADMIN_BROADCAST_PRIVATE_TARGETING_LIST(ActionPrefixes.ADMIN, "event-broadcast-private-targeting-list"),

    ADMIN_BROADCAST_EDIT_ADVERTISING(ActionPrefixes.ADMIN, "event-broadcast-advertising-edit"),
    ADMIN_BROADCAST_GET_GLOBAL_ADVERTISING(ActionPrefixes.ADMIN, "event-broadcast-get-global-advertising"),

    ADMIN_CUSTOM_META_TAG_LIST(ActionPrefixes.ADMIN, "custom-meta-tag-list"),
    ADMIN_CUSTOM_META_TAG_EDIT_NEW(ActionPrefixes.ADMIN, "custom-meta-tag-edit-new"),

    ADMIN_LOYALTY_BONUS_ORDERS(ActionPrefixes.LOYALTY, "bonus-orders"),
    ADMIN_EXPORT_BONUS_ORDERS(ActionPrefixes.LOYALTY, "export-bonus-orders"),
    ADMIN_LOYALTY_BONUSES(ActionPrefixes.LOYALTY, "bonuses"),
    ADMIN_LOYALTY_VIP_USERS(ActionPrefixes.LOYALTY, "vip-users"),

    ADMIN_BILLING_PAYMENTS(ActionPrefixes.BILLING, "payments"),
    ADMIN_BILLING_PAYMENT(ActionPrefixes.BILLING, "payment"),
    ADMIN_BILLING_ORDER(ActionPrefixes.BILLING, "order"),
    ADMIN_TRANSACTIONS_HISTORY(ActionPrefixes.BILLING, "transactions-history"),

    ADMIN_BILLING_PAYMENT_ERRORS(ActionPrefixes.BILLING, "payment-errors"),
    ADMIN_BILLING_PAYMENT_ERROR_COMMENT_LIST(ActionPrefixes.BILLING, "payment-error-comment-list"),

    ADMIN_CONCERTS_LIST(ActionPrefixes.CONCERT, "concert-list"),
    ADMIN_CONCERT_EDIT(ActionPrefixes.CONCERT, "concert-edit"),

    ADMIN_GEO_MANAGE_CITIES(ActionPrefixes.GEO, "manage-cities"),
    ADMIN_CAMPAIGN_MANAGEMENT(ActionPrefixes.CAMPAIGN, "campaign-management"),
    ADMIN_CAMPAIGN_NEWS(ActionPrefixes.CAMPAIGN, "campaign-news"),
    ADMIN_CAMPAIGN_POSTS(ActionPrefixes.CAMPAIGN, "campaign-posts"),
    ADMIN_CAMPAIGN_COMMENTS(ActionPrefixes.CAMPAIGN, "campaign-comments"),
    ADMIN_CAMPAIGN_SHARES(ActionPrefixes.CAMPAIGN, "campaign-shares"),
    MODERATOR_USER_CALLBACKS(ActionPrefixes.ADMIN, "user-callbacks"),
    MODERATOR_USER_CALLBACK_COMMENTS(ActionPrefixes.ADMIN, "user-callback-comment"),

    ADMIN_CONTRACTOR_EDIT(ActionPrefixes.ADMIN, "contractor-edit"),

    ADMIN_INVESTING_ORDERS_INFO(ActionPrefixes.ADMIN, "investing/investing-orders-info"),
    ADMIN_INVESTING_ORDER_INFO(ActionPrefixes.ADMIN, "investing/investing-order-info"),

    ADMIN_BIBLIO_ORDERS(ActionPrefixes.BIBLIO, "biblio-orders"),
    ADMIN_BIBLIO_LIBRARIES(ActionPrefixes.BIBLIO, "libraries"),
    ADMIN_BIBLIO_LIBRARY(ActionPrefixes.BIBLIO, "library"),
    ADMIN_BIBLIO_BOOKS(ActionPrefixes.BIBLIO, "books"),
    ADMIN_BIBLIO_PUBLISHING_HOUSES(ActionPrefixes.BIBLIO, "publishing-houses"),
    ADMIN_BIBLIO_PUBLISHING_HOUSE(ActionPrefixes.BIBLIO, "publishing-house"),
    ADMIN_BIBLIO_ADD_OR_EDIT_BOOK(ActionPrefixes.BIBLIO, "book"),
    ADMIN_BIBLIO_BOOK(ActionPrefixes.BIBLIO, "book"),
    ADMIN_BIBLIO_PARTNERS_FILL(ActionPrefixes.BIBLIO, "partners-fill"),
    ADMIN_BIBLIO_PARTNERS_EDIT(ActionPrefixes.BIBLIO, "partners-edit"),
    ADMIN_BIBLIO_PARTNERS_LIST(ActionPrefixes.BIBLIO, "partners-list"),
    ADMIN_BIBLIO_ADVERTISE_FILL(ActionPrefixes.BIBLIO, "advertise-fill"),
    ADMIN_BIBLIO_ADVERTISE_EDIT(ActionPrefixes.BIBLIO, "advertise-edit"),
    ADMIN_BIBLIO_ADVERTISE_LIST(ActionPrefixes.BIBLIO, "advertise-list"),
    ADMIN_BIBLIO_RUSSIAN_POST_BOOKS_INFO_FROM_FILE(ActionPrefixes.BIBLIO, "biblio-russian-post-books-info-from-file"),
    ADMIN_BIBLIO_RUSSIAN_POST_MAGAZINE_INFO_LIST(ActionPrefixes.BIBLIO, "biblio-russian-post-magazine-info-list"),
    ADMIN_BIBLIO_RUSSIAN_POST_PRICE_LISTS(ActionPrefixes.BIBLIO, "biblio-russian-post-price-lists"),

    ADMIN_CHARITY_PARTNERS_FILL(ActionPrefixes.CHARITY, "partners-fill"),
    ADMIN_CHARITY_PARTNERS_EDIT(ActionPrefixes.CHARITY, "partners-edit"),
    ADMIN_CHARITY_PARTNERS_LIST(ActionPrefixes.CHARITY, "partners-list"),

    ADMIN_TECHNOBATTLE_PROJECT_LIST(ActionPrefixes.PROMO, "project-list"),
    ADMIN_TECHNOBATTLE_PROJECT(ActionPrefixes.PROMO, "project"),

    ADMIN_QUIZZES(ActionPrefixes.ADMIN, "quizzes"),
    ADMIN_QUIZ_INFO(ActionPrefixes.ADMIN, "quiz-info"),
    ADMIN_QUIZ_QUESTION_EDIT(ActionPrefixes.ADMIN, "quiz-question-edit");


    private val prefix: String = prefix.toString()

    val text: String
        get() = prefix + _text

    //    override fun getPath() = text
    override val path: String
        get() = text

    //    override fun getActionName() = _text
    override val actionName: String
        get() = _text

    override fun toString() = text
}

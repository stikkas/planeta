package ru.planeta.admin.controllers.promo

import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.servlet.ModelAndView
import ru.planeta.admin.Actions
import ru.planeta.admin.Urls
import ru.planeta.api.web.controllers.IAction
import ru.planeta.api.web.controllers.services.BaseControllerService
import ru.planeta.dao.commondb.CampaignTagDAO
import ru.planeta.dao.promo.CampusDAO
import ru.planeta.model.enums.ProjectType
import ru.planeta.model.promo.Campus

/**
 *
 * Created by alexa_000 on 14.04.2015.
 */
@Controller
class CampusController(private val campaignTagDAO: CampaignTagDAO,
                       private val dao: CampusDAO,
                       private val baseControllerService: BaseControllerService) {

    @GetMapping(Urls.CAMPUSES)
    fun list(): ModelAndView = createDefaultModelAndView(Actions.ADMIN_CAMPUSES)
            .addObject("campuses", dao.selectAll())

    @GetMapping(Urls.EDIT_CAMPUS)
    fun edit(@RequestParam(required = false) campusId: Long?): ModelAndView {
        val campus = if (campusId != null) dao.select(campusId) else Campus()
        return createDefaultModelAndView(Actions.ADMIN_EDIT_CAMPUS)
                .addObject("campus", campus)
                .addObject("tags", campaignTagDAO.selectAll())
    }

    @PostMapping(Urls.SAVE_CAMPUS)
    fun save(campus: Campus): ModelAndView {
        val campusId = campus.id
        if (campusId == 0L) {
            dao.insert(campus)
        } else {
            dao.update(campus)
        }
        return baseControllerService.createRedirectModelAndView(Urls.CAMPUSES)
    }

    private fun createDefaultModelAndView(action: IAction): ModelAndView = baseControllerService.createDefaultModelAndView(action, ProjectType.ADMIN)
}

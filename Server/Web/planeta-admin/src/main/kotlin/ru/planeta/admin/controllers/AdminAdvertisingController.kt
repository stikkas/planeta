package ru.planeta.admin.controllers

import org.apache.commons.lang3.StringUtils
import org.springframework.stereotype.Controller
import org.springframework.validation.BindingResult
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.servlet.ModelAndView
import ru.planeta.admin.Actions
import ru.planeta.admin.Urls
import ru.planeta.admin.controllers.services.AdminBaseControllerService
import ru.planeta.api.advertising.AdvertisingService
import ru.planeta.api.exceptions.NotFoundException
import ru.planeta.commons.lang.DateUtils
import ru.planeta.model.advertising.AdvertisingDTO
import ru.planeta.model.advertising.AdvertisingState
import ru.planeta.model.advertising.VastDTO
import java.util.*
import javax.validation.Valid

/**
 * Created with IntelliJ IDEA.
 * Date: 22.01.14
 * Time: 21:13
 */
@Controller
class AdminAdvertisingController(private val advertisingService: AdvertisingService,
                                 private val adminBaseControllerService: AdminBaseControllerService) {
    @GetMapping(Urls.ADMIN_BROADCAST_SAVE_ADVERTISING)
    fun saveAdvertisingPage(@RequestParam broadcastId: Long): ModelAndView = getAdvertisingMaV(null, null, broadcastId)

    private fun getAdvertisingMaV(vastDTO: VastDTO?,
                                  advertisingDTO: AdvertisingDTO?,
                                  broadcastId: Long): ModelAndView {
        val advertisingDTO = advertisingDTO ?: if (broadcastId == 0L) {
            AdvertisingDTO()
        } else {
            try {
                advertisingService.loadLocalAdvertising(broadcastId)
            } catch (e: NotFoundException) {
                AdvertisingDTO()
            }

        }

        val states = arrayOfNulls<AdvertisingState>(2)
        states[0] = if (broadcastId <= 0) AdvertisingState.GLOBAL else AdvertisingState.THIS
        states[1] = AdvertisingState.NONE

        val vastDTO = vastDTO ?: advertisingService.extractDTO(advertisingService.extractVast(advertisingDTO))

        if (advertisingDTO.dateTimeFrom != null || advertisingDTO.dateTimeTo != null) {
            advertisingDTO.isDateChecked = true
        }

        if (advertisingDTO.campaignId > 0) {
            advertisingDTO.isCampaignChecked = true
        }

        return adminBaseControllerService.createAdminDefaultModelAndView(Actions.ADMIN_BROADCAST_EDIT_ADVERTISING)
                .addObject("vastDTO", vastDTO)
                .addObject("advertisingDTO", advertisingDTO)
                .addObject("enumValues", states)
    }

    @PostMapping(Urls.ADMIN_BROADCAST_SAVE_ADVERTISING)
    fun saveAdvertising(@Valid vastDTO: VastDTO, vastBindingResult: BindingResult,
                        @Valid advertisingDTO: AdvertisingDTO, advertisingBindingResult: BindingResult,
                        @RequestParam("broadcastId") broadcastId: Long): ModelAndView {
        advertisingDTO.broadcastId = broadcastId
        if (vastBindingResult.hasErrors() || advertisingBindingResult.hasErrors()) {
            return getAdvertisingMaV(vastDTO, advertisingDTO, broadcastId)
        }
        if (vastDTO.adId < 0 && StringUtils.isNotEmpty(vastDTO.videoUrl)) {
            vastDTO.adId = 1
        }

        val vast = advertisingService.createNewVast(vastDTO)

        advertisingService.putVastString(vast, advertisingDTO)

        if (advertisingDTO.isDateChecked) {
            advertisingDTO.campaignId = -1
            if (DateUtils.isBeforeNow(advertisingDTO.dateTimeTo)) {
                advertisingDTO.dateTimeFrom = null
                advertisingDTO.dateTimeTo = null
            }

            if (DateUtils.isAfterNow(advertisingDTO.dateTimeFrom)) {
                advertisingDTO.state = AdvertisingState.NONE
            }
        } else {
            advertisingDTO.dateTimeFrom = null
            advertisingDTO.dateTimeTo = null
        }

        if (advertisingDTO.isCampaignChecked) {
            advertisingDTO.dateTimeFrom = null
            advertisingDTO.dateTimeTo = null
        } else {
            advertisingDTO.campaignId = -1
        }


        if (broadcastId > 0) {
            advertisingDTO.broadcastId = broadcastId
        }
        advertisingService.saveAdvertising(advertisingDTO)

        return adminBaseControllerService.baseControllerService.createRedirectModelAndView(Urls.ADMIN_BROADCAST_INFO + "?broadcastId=" + broadcastId)
    }

    @GetMapping(Urls.ADMIN_BROADCAST_DELETE_ADVERTISING)
    fun deleteAdvertising(broadcastId: Long): ModelAndView {
        advertisingService.deleteAdvertising(broadcastId)
        return adminBaseControllerService.baseControllerService.createRedirectModelAndView(Urls.ADMIN_BROADCAST_INFO + "?broadcastId=" + broadcastId)
    }

}




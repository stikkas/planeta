package ru.planeta.admin.controllers

import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.servlet.ModelAndView
import ru.planeta.admin.Actions
import ru.planeta.admin.Urls
import ru.planeta.api.service.common.BannerService
import ru.planeta.api.web.authentication.myProfileId
import ru.planeta.api.web.controllers.services.BaseControllerService
import ru.planeta.model.common.Banner
import ru.planeta.model.enums.BannerStatus
import ru.planeta.model.enums.BannerType
import ru.planeta.model.enums.ProjectType
import ru.planeta.model.enums.TargetingType
import java.util.*

/**
 * User: a.savanovich
 * Date: 18.05.12
 * Time: 19:37
 */
@Controller
class BannerController(private val bannerService: BannerService,
                       private val baseControllerService: BaseControllerService) {

    @RequestMapping(value = Urls.DELETE_BANNER)
    fun deleteBanner(@RequestParam bannerId: Long): ModelAndView {
        bannerService.delete(myProfileId(), bannerId)
        return baseControllerService.createRedirectModelAndView(Urls.BANNERS_LIST)
    }

    @PostMapping(Urls.SAVE_BANNER)
    fun saveBanner(banner: Banner): ModelAndView {
        bannerService.saveWithMaskFormat(myProfileId(), banner)
        return baseControllerService.createRedirectModelAndView(Urls.BANNERS_LIST)
    }


    @PostMapping(Urls.SAVE_BANNER_SIMPLE)
    fun saveBannerSimple(banner: Banner): ModelAndView {
        bannerService.createTopBanner(myProfileId(), banner)
        return baseControllerService.createRedirectModelAndView(Urls.BANNERS_LIST)
    }

    @GetMapping(Urls.BANNERS_LIST)
    fun getBannersList(@RequestParam(defaultValue = "0") offset: Int,
                       @RequestParam(defaultValue = "0") limit: Int): ModelAndView {
        val bannerList = bannerService.selectList(myProfileId(), EnumSet.of(BannerStatus.ON), offset, limit)
        return createAdminDefaultModelAndView(Actions.ADMIN_BANNERS)
                .addObject("bannerList", bannerList)
    }

    @GetMapping(Urls.ALL_BANNERS_LIST)
    fun getAllBannersList(@RequestParam(defaultValue = "0") offset: Int,
                          @RequestParam(defaultValue = "0") limit: Int): ModelAndView {
        val bannerList = bannerService.selectList(myProfileId(), EnumSet.allOf(BannerStatus::class.java), offset, limit)
        return createAdminDefaultModelAndView(Actions.ADMIN_BANNERS)
                .addObject("bannerList", bannerList)
    }


    @GetMapping(Urls.EDIT_BANNER)
    fun editBanner(@RequestParam(defaultValue = "0") bannerId: Long): ModelAndView {
        val banner = if (bannerId == 0L) bannerService.createSimpleBanner() else bannerService.select(myProfileId(), bannerId)
        return createAdminDefaultModelAndView(Actions.ADMIN_EDIT_BANNER)
                .addObject("banner", banner)
                .addObject("bannerTypes", EnumSet.allOf(BannerType::class.java))
                .addObject("targetingTypes", EnumSet.allOf(TargetingType::class.java))
    }

    @GetMapping(Urls.EDIT_BANNER_SIMPLE)
    fun editBannerSimple(@RequestParam(defaultValue = "0") bannerId: Long): ModelAndView {
        val banner = if (bannerId == 0L) bannerService.createSimpleBanner() else bannerService.select(myProfileId(), bannerId)
        return createAdminDefaultModelAndView(Actions.ADMIN_EDIT_BANNER_SIMPLE)
                .addObject("banner", banner)
                .addObject("bannerTypes", EnumSet.allOf(BannerType::class.java))
                .addObject("targetingTypes", EnumSet.allOf(TargetingType::class.java))
    }

    @PostMapping(Urls.SWITCH_ON_OFF_BANNER)
    fun switchOnOffBanner(@RequestParam bannerId: Long, @RequestParam(value = "status") previousStatus: BannerStatus): ModelAndView {
        val myProfileId = myProfileId()
        val banner = bannerService.select(myProfileId, bannerId)
        if (banner.status == previousStatus) {
            banner.switchStatus()
            bannerService.save(myProfileId, banner)
        }
        return baseControllerService.createRedirectModelAndView(Urls.BANNERS_LIST)
    }

    @PostMapping(Urls.COPY_BANNER)
    fun copyBanner(@RequestParam bannerId: Long): ModelAndView {
        val myProfileId = myProfileId()
        val banner = bannerService.select(myProfileId, bannerId)
        bannerService.copy(myProfileId, banner)
        return baseControllerService.createRedirectModelAndView(Urls.EDIT_BANNER + "?bannerId=" + banner.bannerId)
    }

    @PostMapping(Urls.CREATE_BANNER_TO_CAMPAIGN_QUICK)
    fun setCampaignEditable(@RequestParam campaignId: Long,
                            @RequestParam(defaultValue = "false") html: String): ModelAndView {
        val banner = bannerService.createFromCampaignId(myProfileId(), campaignId, html)
        return baseControllerService.createRedirectModelAndView("${Urls.EDIT_BANNER}?bannerId=${banner.bannerId}")
    }

    protected fun createAdminDefaultModelAndView(action: Actions): ModelAndView = baseControllerService.createDefaultModelAndView(action, ProjectType.ADMIN)
}

package ru.planeta.admin.controllers

import org.apache.commons.lang3.time.DateUtils
import org.apache.log4j.Logger
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.servlet.ModelAndView
import ru.planeta.admin.Actions
import ru.planeta.admin.Urls
import ru.planeta.admin.controllers.services.AdminBaseControllerService
import ru.planeta.api.service.biblio.BookService
import ru.planeta.api.service.biblio.LibraryService
import ru.planeta.api.service.campaign.CampaignService
import ru.planeta.api.service.shop.ProductUsersService
import ru.planeta.api.service.statistic.AdminStatisticService
import ru.planeta.api.web.authentication.myProfileId
import ru.planeta.dao.commondb.AdminStatPurchaseDAO
import ru.planeta.model.common.campaign.enums.CampaignStatus
import ru.planeta.model.enums.OrderObjectType
import java.math.BigDecimal
import java.math.RoundingMode
import java.text.SimpleDateFormat
import java.util.*
import com.google.gson.Gson



/**
 * @author p.vyazankin
 * @since 4/4/13 1:26 PM
 */
@Controller
class DashboardController(private val adminStatisticService: AdminStatisticService,
                          private val productUsersService: ProductUsersService,
                          private val purchaseDAO: AdminStatPurchaseDAO,
                          private val bookService: BookService,
                          private val libraryService: LibraryService,
                          private val campaignService: CampaignService,
                          private val adminBaseControllerService: AdminBaseControllerService) {

    private var cachedStatisticsMap: Map<String, String>? = null
    private val logger = Logger.getLogger(DashboardController::class.java)

    @RequestMapping(Urls.DASHBOARD)
    fun getStatistic(@RequestParam(defaultValue = "false") flushCache: Boolean): ModelAndView {
        val modelAndView = adminBaseControllerService.createAdminDefaultModelAndView(Actions.DASHBOARD)
        return cacheModelAndView(modelAndView, flushCache)
    }

    @Synchronized
    private fun cacheModelAndView(modelAndView: ModelAndView, flushCache: Boolean): ModelAndView {
        if (cachedStatisticsMap == null || flushCache) {
            val now = Date()
            val tenDaysAfter = DateUtils.addDays(now, 10)
            val localMap = HashMap<String, String>()
            addDayDataModelObjects(localMap, now)
            addMonthDataModelObjects(localMap, now)
            addCampaignsDataModelObjects(localMap, tenDaysAfter)
            addShopDataModelObjects(localMap)
            addBiblioDataModelObjects(localMap)

            cachedStatisticsMap = localMap
        }
        val map = cachedStatisticsMap

        map?.forEach { modelAndView.addObject(it.key, it.value) }

        return modelAndView
    }

    private fun addBiblioDataModelObjects(statisticsMap: MutableMap<String, String>) {
        statisticsMap.put("activeBooksCount", bookService.countActiveBooks.toString())
        statisticsMap.put("activeLibrariesCount", libraryService.countActiveLibraries.toString())
        statisticsMap.put("biblioTotalPurchaseAmount", purchaseDAO.selectStatPurchases(null, null, OrderObjectType.BIBLIO).toString())
        statisticsMap.put("biblioTotalPurchaseCount", purchaseDAO.selectTotalOrdersCount(null, null, OrderObjectType.BIBLIO).toString())
    }

    private fun addShopDataModelObjects(statisticsMap: MutableMap<String, String>) {
        val productCount = productUsersService.currentProductsCount
        statisticsMap.put("totalProductCount", productCount.toString())
        statisticsMap.put("totalPurchaseCount", purchaseDAO.selectTotalOrdersCount(null, null, OrderObjectType.PRODUCT).toString())
        statisticsMap.put("totalProductPurchaseAmount", purchaseDAO.selectStatPurchases(null, null, OrderObjectType.PRODUCT).toString())
    }

    /**
     * Adds campaigns report data objects to map
     *
     */
    private fun addCampaignsDataModelObjects(statisticsMap: MutableMap<String, String>, tenDaysAfter: Date) {
        val activeCampaigns = campaignService.getCampaignsCount(0, CampaignStatus.ACTIVE)
        val campaignsEndIn10Days = campaignService.getCampaignsCountBetween(Date(), tenDaysAfter, CampaignStatus.ACTIVE)!!
        val fullAmount4Active = campaignService.getCampaignsTargetAmount(CampaignStatus.ACTIVE)
        val purchased4Active = campaignService.getCampaignsPurchasedAmount(CampaignStatus.ACTIVE)
        val purchased4AllTime = campaignService.purchasedForAllTime

        statisticsMap.put("activeCampaigns", Integer.toString(activeCampaigns))
        statisticsMap.put("campaignsEndIn10Days", Integer.toString(campaignsEndIn10Days!!))
        var activeAmountPercent = BigDecimal(0)
        if (BigDecimal.ZERO.compareTo(fullAmount4Active) != 0) {
            activeAmountPercent = purchased4Active.divide(fullAmount4Active, 4, RoundingMode.CEILING).multiply(BigDecimal.valueOf(100)).setScale(2, RoundingMode.CEILING)
        }
        statisticsMap.put("activeAmountPercent", activeAmountPercent.toString())
        statisticsMap.put("purchased4AllTime", purchased4AllTime.toString())

    }

    /**
     * Adds sales day-report data objects to map
     *
     */
    private fun addMonthDataModelObjects(statisticsMap: MutableMap<String, String>, now: Date) {
        val prevMonthStart = DateUtils.truncate(DateUtils.addMonths(now, -1), Calendar.MONTH)
        val thisMonthEnd = DateUtils.truncate(DateUtils.addMonths(now, 1), Calendar.MONTH)

        val purchaseReportsMap = adminStatisticService.getStatPurchaseCompositeReport(
                myProfileId(),
                prevMonthStart,
                thisMonthEnd,
                "month")

        val firstEntryValue = if (purchaseReportsMap.firstEntry() == null) null else purchaseReportsMap.firstEntry().value
        val lastEntryValue = if (purchaseReportsMap.lastEntry() == null) null else purchaseReportsMap.lastEntry().value

        if (firstEntryValue != null && lastEntryValue != null) {
            statisticsMap.put("currentMonthShareAmount", firstEntryValue.sharesAmount.toString())
            statisticsMap.put("previousMonthShareAmount", lastEntryValue.sharesAmount.toString())
            statisticsMap.put("currentMonthProductAmount", firstEntryValue.productsAmount.toString())
            statisticsMap.put("previousMonthProductAmount", lastEntryValue.productsAmount.toString())
            statisticsMap.put("currentMonthBiblioAmount", firstEntryValue.biblioPurchasesAmount.toString())
            statisticsMap.put("previousMonthBiblioAmount", lastEntryValue.biblioPurchasesAmount.toString())
        } else {
            logger.error("Dashboard data error: firstEntry or lastEntry is null")
        }

    }

    /**
     * Adds sales month-report data objects to map
     *
     */
    private fun addDayDataModelObjects(statisticsMap: MutableMap<String, String>, now: Date) {
        val calendar = Calendar.getInstance()

        calendar.add(Calendar.YEAR, -1)

        val yesterdayStart = DateUtils.truncate(DateUtils.addDays(now, -1), Calendar.DAY_OF_MONTH)
        val severalDaysAgo = DateUtils.truncate(DateUtils.addDays(now, -60), Calendar.DAY_OF_MONTH)
        val yearAgo = DateUtils.truncate(calendar.time, Calendar.YEAR)
        val todayEnd = DateUtils.truncate(DateUtils.addDays(now, 1), Calendar.DAY_OF_MONTH)
        val todayStart = DateUtils.truncate(now, Calendar.DAY_OF_MONTH)

        val purchaseReportsMap = adminStatisticService.getStatPurchaseCompositeReport(
                myProfileId(),
                yesterdayStart,
                todayEnd,
                "day")

        val purchaseReport = adminStatisticService.getStatPurchaseCompositeReport(
                myProfileId(),
                severalDaysAgo,
                todayEnd,
                "day"
        )

        val yearPurchaseReport = adminStatisticService.getStatPurchaseCompositeReport(
                myProfileId(),
                yearAgo,
                todayEnd,
                "month"
        )

        statisticsMap.put("stats", Gson().toJson(purchaseReport))
        statisticsMap.put("yearStats", Gson().toJson(yearPurchaseReport))


        val todayEntryValue = purchaseReportsMap[todayStart]
        val yesterdayEntryValue = purchaseReportsMap[yesterdayStart]



        if (todayEntryValue != null) {
            statisticsMap.put("todayShareAmount", todayEntryValue.sharesAmount.toString())
            statisticsMap.put("todayShareCount", Integer.toString(todayEntryValue.sharesCount))
            statisticsMap.put("todayProductAmount", todayEntryValue.productsAmount.toString())
            statisticsMap.put("todayProductCount", Integer.toString(todayEntryValue.productsCount))
            statisticsMap.put("todayBiblioAmount", todayEntryValue.biblioPurchasesAmount.toString())
            statisticsMap.put("todayBiblioCount", Integer.toString(todayEntryValue.biblioPurchasesCount))
        } else {
            statisticsMap.put("todayShareAmount", "0")
            statisticsMap.put("todayShareCount", "0")
            statisticsMap.put("todayProductAmount", "0")
            statisticsMap.put("todayProductCount", "0")
            statisticsMap.put("todayBiblioAmount", "0")
            statisticsMap.put("todayBiblioCount", "0")
        }

        if (yesterdayEntryValue != null) {
            statisticsMap.put("yesterdayShareAmount", yesterdayEntryValue.sharesAmount.toString())
            statisticsMap.put("yesterdayShareCount", Integer.toString(yesterdayEntryValue.sharesCount))
            statisticsMap.put("yesterdayProductAmount", yesterdayEntryValue.productsAmount.toString())
            statisticsMap.put("yesterdayProductCount", Integer.toString(yesterdayEntryValue.productsCount))
            statisticsMap.put("yesterdayBiblioAmount", yesterdayEntryValue.biblioPurchasesAmount.toString())
            statisticsMap.put("yesterdayBiblioCount", Integer.toString(yesterdayEntryValue.biblioPurchasesCount))
        } else {
            statisticsMap.put("yesterdayShareAmount", "0")
            statisticsMap.put("yesterdayShareCount", "0")
            statisticsMap.put("yesterdayProductAmount", "0")
            statisticsMap.put("yesterdayProductCount", "0")
            statisticsMap.put("yesterdayBiblioAmount", "0")
            statisticsMap.put("yesterdayBiblioCount", "0")
        }
    }
}


package ru.planeta.admin.controllers

import org.apache.log4j.Logger
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.*
import org.springframework.web.servlet.ModelAndView
import ru.planeta.admin.Actions
import ru.planeta.admin.Shop
import ru.planeta.admin.Urls
import ru.planeta.admin.controllers.services.AdminBaseControllerService
import ru.planeta.admin.utils.DateUtils
import ru.planeta.api.exceptions.ChangeProductQuantityException
import ru.planeta.api.model.ActionStatus
import ru.planeta.api.news.LoggerService
import ru.planeta.api.search.SearchService
import ru.planeta.api.search.filters.SearchOrderFilter
import ru.planeta.api.service.billing.order.OrderService
import ru.planeta.api.service.campaign.DeliveryService
import ru.planeta.api.utils.OrderUtils
import ru.planeta.api.web.authentication.myProfileId
import ru.planeta.api.web.dto.ChangeShopOrderStatusDTO
import ru.planeta.model.common.Order
import ru.planeta.model.common.OrderInfo
import ru.planeta.model.common.delivery.SubjectType
import ru.planeta.model.enums.OrderObjectType
import ru.planeta.model.news.ProfileNews
import ru.planeta.model.shop.enums.DeliveryStatus
import ru.planeta.model.shop.enums.PaymentStatus
import ru.planeta.reports.ReportType
import ru.planeta.reports.SimpleReport
import java.util.*
import java.util.concurrent.TimeUnit
import javax.servlet.http.HttpServletResponse

/**
 * Date: 03.10.12
 * Time: 13:30
 */
@Controller
class ShopController(private val deliveryService: DeliveryService,
                     private val orderService: OrderService,
                     private val searchService: SearchService,
                     private val profileNewsService: LoggerService,
                     private val adminBaseControllerService: AdminBaseControllerService) {

    @GetMapping(Urls.ADMIN_SHOP_ORDERS)
    fun getShopOrders(searchOrderFilter: SearchOrderFilter): ModelAndView {
        val clientId = myProfileId()
        if (!adminBaseControllerService.permissionService.hasAdministrativeRole(clientId)) {
            return adminBaseControllerService.baseControllerService.createRedirectModelAndView(Urls.ADMIN_INDEX)
        }
        if (searchOrderFilter.limit == 0) {
            searchOrderFilter.limit = 20
        }

        val modelAndView = adminBaseControllerService.createAdminDefaultModelAndView(Actions.ADMIN_SHOP_ORDERS)

        val interval = DateUtils.setAllTimeAsDate(searchOrderFilter.dateFrom, searchOrderFilter.dateTo, modelAndView)
        searchOrderFilter.dateFrom = interval.dateFrom
        searchOrderFilter.dateTo = interval.dateTo
        searchOrderFilter.orderObjectType = OrderObjectType.PRODUCT
        val orderInfoSearchResult = searchService.searchForMerchantOrders(searchOrderFilter)

        return modelAndView
                .addObject("ordersInfo", orderInfoSearchResult.searchResultRecords)
                .addObject("validTransitions", DeliveryStatus.getValidTransitions())
                .addObject("offset", searchOrderFilter.offset)
                .addObject("limit", searchOrderFilter.limit)
                .addObject("count", orderInfoSearchResult.estimatedCount)
                .addObject("onlyProducts", true)
    }


    @PostMapping(Urls.ORDER_CHANGE_DELIVERY_STATUS)
    @ResponseBody
    fun changeOrderStatus(@RequestBody dto: ChangeShopOrderStatusDTO): ActionStatus<Order> {
        try {
            orderService.changeOrderStatus(
                    myProfileId(), dto.orderId, dto.deliveryStatus, dto.isSendMessageToUser,
                    dto.reason, dto.trackingCode, dto.cashOnDeliveryCost, dto.deliveryComment)
            profileNewsService.addProfileNews(ProfileNews.Type.ADMIN_CHANGE_ORDER_DELIVERY_STATUS, myProfileId(), dto.orderId)
        } catch (e: ChangeProductQuantityException) {
            return ActionStatus.createErrorStatus("unable.to.send.the.goods", adminBaseControllerService.baseControllerService.messageSource)
        }
        return ActionStatus.createSuccessStatus(orderService.getOrderSafe(dto.orderId))
    }


    @GetMapping(Urls.DELIVERY_LIST)
    fun deliveryList(): ModelAndView = adminBaseControllerService.createAdminDefaultModelAndView(Actions.ADMIN_SHOP_DELIVERY_LIST)
            .addObject("departments", deliveryService.getLinkedDeliveries(0, SubjectType.SHOP))

    @ResponseBody
    @RequestMapping(Shop.ADMIN_SHOP_ORDERS_REPORT)
    fun ordersShopReport(response: HttpServletResponse, searchOrderFilter: SearchOrderFilter,
                         @RequestParam(defaultValue = "CSV") reportType: ReportType) {
        val interval = DateUtils.getAllTimeInterval(searchOrderFilter.dateFrom, searchOrderFilter.dateTo)
        searchOrderFilter.dateFrom = interval.dateFrom
        searchOrderFilter.dateTo = interval.dateTo
        val report = makeReport(searchOrderFilter, reportType, myProfileId())
        report.addToResponse(response)
    }

    @ResponseBody
    @RequestMapping(Shop.ADMIN_SHOP_ORDERS_MAIL_REPORT)
    fun ordersReportMail(response: HttpServletResponse, searchOrderFilter: SearchOrderFilter,
                         @RequestParam(defaultValue = "CSV") reportType: ReportType): ActionStatus<*> {
        val clientId = myProfileId()
        adminBaseControllerService.baseControllerService.delay(Runnable {
            try {
                val report = makeReport(searchOrderFilter, reportType, clientId)
                adminBaseControllerService.baseControllerService.sendReport(clientId, report)
            } catch (e: Exception) {
                log.error("", e)
            }
        }, 0, TimeUnit.SECONDS)
        return ActionStatus.createSuccessStatus<Any>()
    }

    private fun makeReport(dateFrom: Date?, dateTo: Date?, merchantId: Long, paymentStatus: PaymentStatus?,
                           deliveryStatus: DeliveryStatus?, offset: Int, limit: Int, reportType: ReportType,
                           clientId: Long): SimpleReport {

        val splittedMerchantOrders = orderService.getMerchantOrdersInfo(clientId, merchantId, OrderObjectType.PRODUCT,
                paymentStatus, deliveryStatus, dateFrom, dateTo, offset, limit)
        val groupedOrders = groupObjectsInOrders(splittedMerchantOrders)
        val report = reportType.createReport("shop_orders_report_" + clientId)

        report.addCaptionRow(*adminBaseControllerService.baseControllerService.messageSource.getMessage("order.statistic.report.head", null,
                Locale.getDefault()).split(";".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray())
        preapareOrdersReport(groupedOrders, report)
        return report
    }

    private fun makeReport(searchOrderFilter: SearchOrderFilter,
                           reportType: ReportType,
                           clientId: Long): SimpleReport {
        var splittedMerchantOrders: MutableCollection<OrderInfo> = ArrayList()

        if (searchOrderFilter.limit == 0 || searchOrderFilter.limit > UPPER_LIMIT) {
            searchOrderFilter.limit = UPPER_LIMIT

            var lastFoundCount = 0
            var offset = 0
            do {
                searchOrderFilter.offset = offset

                val orderInfoSearchTempResult = searchService.searchForOrders(searchOrderFilter)
                splittedMerchantOrders.addAll(orderInfoSearchTempResult.searchResultRecords)

                lastFoundCount = orderInfoSearchTempResult.size
                offset += UPPER_LIMIT
            } while (lastFoundCount >= UPPER_LIMIT)
        } else {
            splittedMerchantOrders = searchService.searchForOrders(searchOrderFilter).searchResultRecords
        }
        val groupedOrders = groupObjectsInOrders(splittedMerchantOrders)
        val report = reportType.createReport("shop_orders_report_" + clientId)

        report.addCaptionRow(*adminBaseControllerService
                .baseControllerService.messageSource
                .getMessage("order.statistic.report.head", null, Locale.getDefault()).split(";".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray())
        preapareOrdersReport(groupedOrders, report)
        return report
    }

    private fun preapareOrdersReport(groupedOrders: Collection<OrderInfo>, report: SimpleReport) {
        val messageSource = adminBaseControllerService.baseControllerService.messageSource
        for (order in groupedOrders) {
            for (orderObject in order.orderObjectsInfo) {
                report.addRow(
                        order.orderId, order.timeAdded, order.timeUpdated, orderObject.objectName, orderObject.price,
                        orderObject.count, order.buyerName, order.buyerEmail, order.totalPrice,
                        messageSource.getMessage("payment.status.naming", arrayOf<Any>(order.paymentStatus.code), Locale.getDefault()),
                        messageSource.getMessage("delivery.status.naming", arrayOf<Any>(order.deliveryStatus.code), Locale.getDefault())
                )

                if (order.linkedDelivery != null) {
                    report.addCells(order.linkedDelivery.name, order.linkedDelivery.price)
                } else {
                    report.skipCell(2)
                }


                if (order.deliveryAddress != null) {
                    val deliveryAddress = order.deliveryAddress
                    report.addCells(
                            deliveryAddress.zipCode, deliveryAddress.country, deliveryAddress.city, deliveryAddress.address, deliveryAddress.phone
                    )
                } else {
                    report.skipCell(5)
                }

                if (order.deliveryAddress != null) {
                    val deliveryAddress = order.deliveryAddress
                    report.addCells(
                            deliveryAddress.fio, deliveryAddress.phone
                    )
                } else {
                    report.addCell(order.buyerName).skipCell()
                }

                if (order.trackingCode != null) {
                    report.addCell(
                            order.trackingCode
                    )
                } else {
                    report.skipCell()
                }
            }
        }
    }

    companion object {

        private val log = Logger.getLogger(ShopController::class.java)
        private val UPPER_LIMIT = 100

        private fun groupObjectsInOrders(merchantOrders: Collection<OrderInfo>): Collection<OrderInfo> {
            for (oneOrderItem in merchantOrders) {
                val orderObjectsMap = OrderUtils.groupOrderObjectsByObjectId(oneOrderItem.orderObjectsInfo, null)
                oneOrderItem.orderObjectsInfo = ArrayList()
                for ((_, value) in orderObjectsMap) {
                    val orderObjectItem = value[0]
                    orderObjectItem.count = value.size
                    oneOrderItem.orderObjectsInfo.add(orderObjectItem)
                }
            }
            return merchantOrders
        }
    }
}

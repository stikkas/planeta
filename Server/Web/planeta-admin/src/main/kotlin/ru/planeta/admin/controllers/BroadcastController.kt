package ru.planeta.admin.controllers

import org.apache.commons.lang3.time.DateUtils
import org.springframework.stereotype.Controller
import org.springframework.validation.BindingResult
import org.springframework.web.bind.annotation.*
import org.springframework.web.servlet.ModelAndView
import ru.planeta.admin.Actions
import ru.planeta.admin.Urls
import ru.planeta.admin.controllers.services.AdminBaseControllerService
import ru.planeta.admin.utils.DateUtils.Interval
import ru.planeta.admin.utils.DateUtils.setIntervalTodayAsDate
import ru.planeta.api.advertising.AdvertisingService
import ru.planeta.api.exceptions.NotFoundException
import ru.planeta.api.news.LoggerService
import ru.planeta.api.search.SearchService
import ru.planeta.api.service.content.BroadcastService
import ru.planeta.api.utils.ValidateUtils
import ru.planeta.api.web.authentication.myProfileId
import ru.planeta.model.advertising.AdvertisingDTO
import ru.planeta.model.advertising.VastDTO
import ru.planeta.model.news.ProfileNews
import ru.planeta.model.profile.broadcast.*
import java.text.SimpleDateFormat
import java.util.*
import javax.validation.Valid

/**
 * Created with IntelliJ IDEA.
 * Date: 03.10.12
 * Time: 13:32
 */
@Controller
class BroadcastController(private val broadcastService: BroadcastService,
                          private val advertisingService: AdvertisingService,
                          private val profileNewsService: LoggerService,
                          private val searchService: SearchService,
                          private val adminBaseControllerService: AdminBaseControllerService) {

    @GetMapping(Urls.ADMIN_BROADCAST_INFO)
    fun getEventBroadcastInfo(@ModelAttribute("broadcast") broadcast: Broadcast, errors: BindingResult,
                              @RequestParam(value = "broadcastId") broadcastId: Long): ModelAndView {
        var broadcast = broadcast

        val myProfileId = myProfileId()
        if (!errors.hasErrors()) {
            if (broadcastId > 0) {
                broadcast = broadcastService.getBroadcastSafe(myProfileId, broadcastId)
            } else {
                broadcast = Broadcast()
                broadcast.profileId = BroadcastService.PLANETA_PROFILE_ID
            }
        }

        var geoTargeting: List<BroadcastGeoTargeting> = ArrayList()
        var backersTargeting: List<BroadcastBackersTargeting> = ArrayList()
        var productTargeting: List<BroadcastProductTargeting> = ArrayList()
        var broadcastStreams: List<BroadcastStream> = ArrayList()
        var vastDTO: VastDTO? = null
        var advertisingDTO: AdvertisingDTO? = null
        if (broadcastId > 0) {
            val profileId = BroadcastService.PLANETA_PROFILE_ID
            broadcastStreams = broadcastService.getBroadcastStreams(myProfileId, broadcastId, 0, 1000)
            geoTargeting = broadcastService.getBroadcastGeoTargeting(myProfileId, broadcastId)
            backersTargeting = broadcastService.getBroadcastBackersTargeting(myProfileId, profileId, broadcastId)
            productTargeting = broadcastService.getBroadcastProductTargeting(broadcastId)
            try {
                advertisingDTO = advertisingService.loadLocalAdvertising(broadcast.broadcastId)
                vastDTO = advertisingService.extractDTO(advertisingService.extractVast(advertisingDTO))
            } catch (ignored: NotFoundException) {
                vastDTO = VastDTO()
            }

        }

        return adminBaseControllerService.createAdminDefaultModelAndView(Actions.ADMIN_BROADCAST_INFO)
                .addObject("broadcast", broadcast)
                .addObject("streams", broadcastStreams)
                .addObject("geoTargeting", geoTargeting)
                .addObject("backersTargeting", backersTargeting)
                .addObject("productTargeting", productTargeting)
                .addObject("vastDTO", vastDTO)
                .addObject("advertisingDTO", advertisingDTO)
    }

    @PostMapping(Urls.ADMIN_BROADCAST_INFO)
    fun saveEventBroadcast(@Valid @ModelAttribute("broadcast") broadcast: Broadcast,
                           result: BindingResult): ModelAndView {
        if (result.hasErrors()) {
            return getEventBroadcastInfo(broadcast, result, broadcast.broadcastId)
        }

        if (broadcast.profileId <= 0) {
            broadcast.profileId = BroadcastService.PLANETA_PROFILE_ID
        }
        val myProfileId = myProfileId()
        val saved = broadcastService.saveBroadcast(myProfileId, broadcast)
        profileNewsService.addProfileNews(ProfileNews.Type.ADMIN_SAVE_BROADCAST_INFO, myProfileId, broadcast.broadcastId)

        return adminBaseControllerService.baseControllerService.createRedirectModelAndView(Urls.ADMIN_BROADCAST_INFO
                + "?profileId=" + saved.profileId
                + "&broadcastId=" + saved.broadcastId)
    }

    @GetMapping(Urls.ADMIN_BROADCAST_REMOVE)
    fun deleteBroadcast(@RequestParam broadcastId: Long): ModelAndView {
        val myProfileId = myProfileId()
        broadcastService.deleteBroadcast(myProfileId, broadcastId)
        profileNewsService.addProfileNews(ProfileNews.Type.ADMIN_REMOVE_BROADCAST, myProfileId, broadcastId)
        return adminBaseControllerService.baseControllerService.createRedirectModelAndView(Urls.ADMIN_BROADCASTS)
    }

    @GetMapping(Urls.ADMIN_BROADCAST_STREAM_INFO)
    fun getEventBroadcastStreamInfo(@ModelAttribute broadcastStream: BroadcastStream?,
                                    errors: BindingResult,
                                    @RequestParam broadcastId: Long,
                                    @RequestParam streamId: Long): ModelAndView {
        var broadcastStream = broadcastStream

        val myProfileId = myProfileId()
        val broadcast = broadcastService.getBroadcastSafe(myProfileId, broadcastId)

        if (!errors.hasErrors()) {
            if (streamId == 0L) {
                broadcastStream = BroadcastStream()
                broadcastStream.profileId = broadcast.profileId
                broadcastStream.broadcastId = broadcast.broadcastId
            } else {
                broadcastStream = broadcastService.getBroadcastStream(myProfileId, streamId)
            }
        }

        val modelAndView = adminBaseControllerService.createAdminDefaultModelAndView(Actions.ADMIN_BROADCAST_STREAM_INFO)
        modelAndView.addObject("broadcastStream", broadcastStream)
        modelAndView.addObject("broadcastStatus", broadcast.broadcastStatus)
        return modelAndView
    }

    @GetMapping(Urls.ADMIN_BROADCAST_TOGGLE_STREAM_STATE)
    fun toggleBroadcastStreamState(@RequestParam streamId: Long,
                                   @RequestParam(defaultValue = "0") broadcastId: Long): ModelAndView {
        broadcastService.toggleBroadcastStreamState(myProfileId(), broadcastId, streamId)
        return if (broadcastId > 0)
            adminBaseControllerService.baseControllerService.createRedirectModelAndView(Urls.ADMIN_BROADCAST_INFO + "?broadcastId=" + broadcastId)
        else
            adminBaseControllerService.baseControllerService.createRedirectModelAndView(Urls.ADMIN_BROADCAST_STREAM_INFO + "streamId=" + streamId)
    }

    @PostMapping(Urls.ADMIN_BROADCAST_STREAM_INFO)
    fun saveEventBroadcastStream(@Valid @ModelAttribute("broadcastStream") broadcastStream: BroadcastStream,
                                 result: BindingResult): ModelAndView {
        if (result.hasErrors()) {
            return getEventBroadcastStreamInfo(broadcastStream, result, broadcastStream.profileId, broadcastStream.streamId)
        }

        val saved = broadcastService.saveBroadcastStream(myProfileId(), broadcastStream)
        return adminBaseControllerService.baseControllerService.createRedirectModelAndView(Urls.ADMIN_BROADCAST_INFO
                + "?broadcastId=" + saved.broadcastId)
    }

    @GetMapping(Urls.ADMIN_BROADCAST_STREAM_REMOVE)
    fun removeEventBroadcastStream(@RequestParam broadcastId: Long,
                                   @RequestParam streamId: Long): ModelAndView {
        val myProfileId = myProfileId()
        val broadcast = broadcastService.getBroadcastSafe(myProfileId, broadcastId)
        broadcastService.deleteBroadcastStream(myProfileId, streamId)

        if (broadcast.defaultStreamId == streamId) {
            val streams = broadcastService.getBroadcastStreams(myProfileId, broadcast.broadcastId, 0, 1)
            if (!streams.isEmpty()) {
                broadcast.defaultStreamId = streams[0].streamId
                broadcastService.saveBroadcast(myProfileId, broadcast)
            }
        }
        return adminBaseControllerService.baseControllerService.createRedirectModelAndView(Urls.ADMIN_BROADCAST_INFO + "?broadcastId=" + broadcast.broadcastId)
    }

    @GetMapping(Urls.ADMIN_BROADCAST_START)
    fun playBroadcast(@RequestParam broadcastId: Long): ModelAndView {
        val myProfileId = myProfileId()
        broadcastService.playBroadcast(myProfileId, broadcastId)
        profileNewsService.addProfileNews(ProfileNews.Type.ADMIN_START_BROADCAST, myProfileId, broadcastId)
        return adminBaseControllerService.baseControllerService.createRedirectModelAndView(Urls.ADMIN_BROADCAST_INFO + "?broadcastId=" + broadcastId)
    }

    @GetMapping(Urls.ADMIN_BROADCAST_STOP)
    fun stopBroadcast(@RequestParam broadcastId: Long): ModelAndView {
        val myProfileId = myProfileId()
        broadcastService.stopBroadcast(myProfileId, broadcastId)
        profileNewsService.addProfileNews(ProfileNews.Type.ADMIN_STOP_BROADCAST, myProfileId, broadcastId)
        return adminBaseControllerService.baseControllerService.createRedirectModelAndView(Urls.ADMIN_BROADCAST_INFO + "?broadcastId=" + broadcastId)
    }

    @GetMapping(Urls.ADMIN_BROADCAST_PAUSE)
    fun pauseBroadcast(@RequestParam broadcastId: Long): ModelAndView {
        val myProfileId = myProfileId()
        broadcastService.pauseBroadcast(myProfileId, broadcastId)
        profileNewsService.addProfileNews(ProfileNews.Type.ADMIN_PAUSE_BROADCAST, myProfileId, broadcastId)
        return adminBaseControllerService.baseControllerService.createRedirectModelAndView(Urls.ADMIN_BROADCAST_INFO + "?broadcastId=" + broadcastId)
    }

    @GetMapping(Urls.ADMIN_BROADCAST_SET_DEFAULT_STREAM)
    fun setDefaultStream(@RequestParam broadcastId: Long,
                         @RequestParam streamId: Long): ModelAndView {
        broadcastService.setDefaultStreamId(myProfileId(), broadcastId, streamId)
        return adminBaseControllerService.baseControllerService.createRedirectModelAndView(Urls.ADMIN_BROADCAST_INFO + "?broadcastId=" + broadcastId)
    }

    @GetMapping(Urls.ADMIN_BROADCAST_ADD_GEO_TARGETING)
    fun getEventBroadcastGeoTargeting(@ModelAttribute broadcastGeoTargeting: BroadcastGeoTargeting,
                                      errors: BindingResult,
                                      @RequestParam profileId: Long,
                                      @RequestParam broadcastId: Long): ModelAndView {
        var broadcastGeoTargeting = broadcastGeoTargeting

        if (!errors.hasErrors()) {
            broadcastGeoTargeting = BroadcastGeoTargeting()
            broadcastGeoTargeting.profileId = profileId
            broadcastGeoTargeting.broadcastId = broadcastId
        }
        return adminBaseControllerService.createAdminDefaultModelAndView(Actions.ADMIN_BROADCAST_GEO_TARGETING)
                .addObject("broadcastGeoTargeting", broadcastGeoTargeting)
    }

    @PostMapping(Urls.ADMIN_BROADCAST_ADD_GEO_TARGETING)
    fun saveEventBroadcastGeoTargeting(@Valid @ModelAttribute("broadcastGeoTargeting") broadcastGeoTargeting: BroadcastGeoTargeting,
                                       result: BindingResult): ModelAndView {
        if (result.hasErrors()) {
            return getEventBroadcastGeoTargeting(broadcastGeoTargeting, result, broadcastGeoTargeting.profileId, broadcastGeoTargeting.broadcastId)
        }

        broadcastService.addBroadcastGeoTargeting(myProfileId(), broadcastGeoTargeting)
        return adminBaseControllerService.baseControllerService.createRedirectModelAndView(Urls.ADMIN_BROADCAST_INFO + "?broadcastId=" + broadcastGeoTargeting.broadcastId)
    }

    @GetMapping(Urls.ADMIN_BROADCAST_REMOVE_GEO_TARGETING)
    fun deleteBroadcastGeoTargeting(@RequestParam broadcastId: Long): ModelAndView {
        broadcastService.removeBroadcastGeoTargeting(myProfileId(), broadcastId)
        return adminBaseControllerService.baseControllerService.createRedirectModelAndView(Urls.ADMIN_BROADCAST_INFO + "?broadcastId=" + broadcastId)
    }

    @GetMapping(Urls.ADMIN_BROADCAST_REMOVE_GEO_TARGETING_BY_PARAMETERS)
    fun deleteBroadcastGeoTargeting(@RequestParam broadcastId: Long,
                                    @RequestParam countryId: Int,
                                    @RequestParam cityId: Int,
                                    @RequestParam allowed: Boolean): ModelAndView {
        broadcastService.removeBroadcastGeoTargetingByParams(myProfileId(), broadcastId, countryId, cityId, allowed)
        return adminBaseControllerService.baseControllerService.createRedirectModelAndView(Urls.ADMIN_BROADCAST_INFO + "?broadcastId=" + broadcastId)
    }

    @RequestMapping(Urls.ADMIN_BROADCAST_ADD_BACKERS_TARGETING)
    fun getEventBroadcastBackersTargeting(@ModelAttribute broadcastBackersTargeting: BroadcastBackersTargeting,
                                          errors: BindingResult,
                                          @RequestParam profileId: Long,
                                          @RequestParam broadcastId: Long): ModelAndView {
        var broadcastBackersTargeting = broadcastBackersTargeting
        if (!errors.hasErrors()) {
            broadcastBackersTargeting = BroadcastBackersTargeting()
            broadcastBackersTargeting.profileId = profileId
            broadcastBackersTargeting.broadcastId = broadcastId
        }
        return adminBaseControllerService.createAdminDefaultModelAndView(Actions.ADMIN_BROADCAST_BACKERS_TARGETING)
                .addObject("broadcastBackersTargeting", broadcastBackersTargeting)
    }

    @PostMapping(Urls.ADMIN_BROADCAST_ADD_BACKERS_TARGETING)
    fun saveEventBroadcastBackersTargeting(@Valid @ModelAttribute("broadcastBackersTargeting") broadcastBackersTargeting: BroadcastBackersTargeting,
                                       result: BindingResult): ModelAndView {
        if (result.hasErrors()) {
            return getEventBroadcastBackersTargeting(broadcastBackersTargeting, result, broadcastBackersTargeting.profileId,
                    broadcastBackersTargeting.broadcastId)
        }

        broadcastService.addBroadcastBackersTargeting(myProfileId(), broadcastBackersTargeting)
        return adminBaseControllerService.baseControllerService.createRedirectModelAndView(Urls.ADMIN_BROADCAST_INFO + "?broadcastId=" + broadcastBackersTargeting.broadcastId)
    }

    @GetMapping(Urls.ADMIN_BROADCAST_REMOVE_BACKERS_TARGETING)
    fun deleteBroadcastBackersTargeting(@RequestParam broadcastId: Long,
                                        @RequestParam campaignId: Long): ModelAndView {
        broadcastService.removeBroadcastBackersTargeting(myProfileId(), broadcastId, campaignId)
        return adminBaseControllerService.baseControllerService.createRedirectModelAndView(Urls.ADMIN_BROADCAST_INFO + "?broadcastId=" + broadcastId)
    }

    @RequestMapping(Urls.ADMIN_BROADCAST_ADD_PRODUCT_TARGETING)
    fun getEventBroadcastProductTargeting(@ModelAttribute broadcastProductTargeting: BroadcastProductTargeting,
                                          errors: BindingResult,
                                          @RequestParam broadcastId: Long): ModelAndView {
        var broadcastProductTargeting = broadcastProductTargeting
        if (!errors.hasErrors()) {
            broadcastProductTargeting = BroadcastProductTargeting()
            broadcastProductTargeting.broadcastId = broadcastId
        }
        return adminBaseControllerService.createAdminDefaultModelAndView(Actions.ADMIN_BROADCAST_PRODUCT_TARGETING)
                .addObject("broadcastProductTargeting", broadcastProductTargeting)
    }

    @PostMapping(Urls.ADMIN_BROADCAST_ADD_PRODUCT_TARGETING)
    fun saveEventBroadcastProductTargeting(@Valid @ModelAttribute broadcastProductTargeting: BroadcastProductTargeting,
                                       result: BindingResult): ModelAndView {
        if (result.hasErrors()) {
            return getEventBroadcastProductTargeting(broadcastProductTargeting, result, broadcastProductTargeting.broadcastId)
        }

        broadcastService.addBroadcastProductTargeting(myProfileId(), broadcastProductTargeting)
        return adminBaseControllerService.baseControllerService.createRedirectModelAndView(Urls.ADMIN_BROADCAST_INFO + "?broadcastId=" + broadcastProductTargeting.broadcastId)
    }

    @GetMapping(Urls.ADMIN_BROADCAST_REMOVE_PRODUCT_TARGETING)
    fun deleteBroadcastProductTargeting(@RequestParam broadcastId: Long,
                                        @RequestParam productId: Long): ModelAndView {
        broadcastService.removeBroadcastProductTargeting(myProfileId(), BroadcastProductTargeting(broadcastId, productId))
        return adminBaseControllerService.baseControllerService.createRedirectModelAndView(Urls.ADMIN_BROADCAST_INFO + "?broadcastId=" + broadcastId)
    }

    @GetMapping(Urls.ADMIN_BROADCAST_PRIVATE_TARGETING)
    fun getEventBroadcastPrivateTargetingList(@RequestParam broadcastId: Long): ModelAndView {
        val myProfileId = myProfileId()
        return adminBaseControllerService.createAdminDefaultModelAndView(Actions.ADMIN_BROADCAST_PRIVATE_TARGETING_LIST)
                .addObject("broadcastPrivateTargetings", broadcastService.getBroadcastPrivateTargetings(myProfileId, broadcastId))
                .addObject("broadcast", broadcastService.getBroadcast(myProfileId, broadcastId))
    }

    @GetMapping(Urls.ADMIN_BROADCAST_ADD_PRIVATE_TARGETING)
    fun getEventBroadcastPrivateTargeting(@Valid @ModelAttribute broadcastPrivateTargeting: BroadcastPrivateTargeting,
                                          errors: BindingResult,
                                          @RequestParam broadcastId: Long): ModelAndView {
        var broadcastPrivateTargeting = broadcastPrivateTargeting

        if (!errors.hasErrors()) {
            broadcastPrivateTargeting = BroadcastPrivateTargeting()
            broadcastPrivateTargeting.profileId = BroadcastService.PLANETA_PROFILE_ID
            broadcastPrivateTargeting.broadcastId = broadcastId
        }

        return adminBaseControllerService.createAdminDefaultModelAndView(Actions.ADMIN_BROADCAST_PRIVATE_TARGETING)
                .addObject("broadcast", broadcastService.getBroadcast(myProfileId(), broadcastId))
                .addObject("broadcastPrivateTargeting", broadcastPrivateTargeting)
    }

    @PostMapping(Urls.ADMIN_BROADCAST_ADD_PRIVATE_TARGETING)
    fun saveEventBroadcastPrivateTargeting(@Valid @ModelAttribute("broadcastPrivateTargeting") broadcastPrivateTargeting: BroadcastPrivateTargeting,
                                           result: BindingResult): ModelAndView {
        if (result.hasErrors()) {
            return getEventBroadcastPrivateTargeting(broadcastPrivateTargeting, result, broadcastPrivateTargeting.broadcastId)
        }

        // construct list of emails, performing validation
        val emailsList = ArrayList<String>()
        val emails = broadcastPrivateTargeting.email.split(",".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
        for (email in emails) {
            val _email = email.trim(' ')
            if (!ValidateUtils.isValidEmail(_email)) {
                result.rejectValue("email", "wrong.chars")
                return getEventBroadcastPrivateTargeting(broadcastPrivateTargeting, result, broadcastPrivateTargeting.broadcastId)
            }
            emailsList.add(_email)
        }

        broadcastService.generatePrivateBroadcastLinks(myProfileId(), broadcastPrivateTargeting.profileId,
                broadcastPrivateTargeting.broadcastId, emailsList, broadcastPrivateTargeting.validInHours)
        return adminBaseControllerService.baseControllerService.createRedirectModelAndView(Urls.ADMIN_BROADCAST_PRIVATE_TARGETING + "?broadcastId=" + broadcastPrivateTargeting.broadcastId)
    }


    @GetMapping(Urls.ADMIN_BROADCASTS)
    fun getBroadcasts(@RequestParam(defaultValue = "0") offset: Long,
                      @RequestParam(defaultValue = "10") limit: Int,
                      @RequestParam(defaultValue = "") searchString: String,
                      @RequestParam(required = false) dateTo: Date?,
                      @RequestParam(required = false) dateFrom: Date?): ModelAndView {
        val modelAndView = adminBaseControllerService.createAdminDefaultModelAndView(Actions.ADMIN_BROADCASTS)

        var dateFrom2 = dateFrom
        if (dateFrom2 == null) {
            val formatter = SimpleDateFormat("dd/MM/yyyy")
            dateFrom2 = formatter.parse("01/01/1900")
        }
        val datepickerInterval = setIntervalTodayAsDate(dateFrom2, dateTo, modelAndView)
        val interval = Interval(datepickerInterval.dateFrom, DateUtils.addDays(datepickerInterval.dateTo, 1))

//        val broadcastSearchResult = searchService.searchForBroadcasts(searchString, interval.dateFrom, interval.dateTo, offset, limit)
        val broadcastSearchResult = broadcastService.selectForAdmin(searchString, interval.dateFrom, interval.dateTo, offset, limit)
        val broadcastSearchResultEstimatedCount = broadcastService.selectForAdminCount(searchString, interval.dateFrom, interval.dateTo)

        return modelAndView.addObject("broadcastList", broadcastSearchResult)
                .addObject("count", broadcastSearchResultEstimatedCount)
                .addObject("offset", offset)
                .addObject("limit", limit)
                .addObject("searchString", searchString)
    }
}


package ru.planeta.admin.controllers.services

import org.springframework.stereotype.Service
import ru.planeta.admin.Actions
import ru.planeta.admin.Biblio
import ru.planeta.api.model.ConfigurationType
import ru.planeta.api.model.biblio.Partner
import ru.planeta.api.service.configurations.ConfigurationService

@Service
class PartnerBiblioControllerService(private val adminBaseControllerService: AdminBaseControllerService,
                                     private val configurationService: ConfigurationService) :
        BiblioConfigControllerService<Partner>(Partner::class.java, adminBaseControllerService, configurationService) {

    override val configName: String = ConfigurationType.BIBLIO_PARTNERS_CONFIGURATION_LIST
    override val fillAction: Actions = Actions.ADMIN_BIBLIO_PARTNERS_FILL
    override val editAction: Actions = Actions.ADMIN_BIBLIO_PARTNERS_EDIT
    override val rootUrl: String = Biblio.ADMIN_PARTNERS_LIST
    override val listAction: Actions = Actions.ADMIN_BIBLIO_PARTNERS_LIST
}
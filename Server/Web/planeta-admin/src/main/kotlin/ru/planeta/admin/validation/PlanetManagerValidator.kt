package ru.planeta.admin.validation

import org.springframework.stereotype.Component
import org.springframework.validation.Errors
import org.springframework.validation.ValidationUtils
import org.springframework.validation.Validator
import ru.planeta.api.utils.ValidateUtils
import ru.planeta.model.common.PlanetaManager

import java.util.regex.Pattern

/**
 * @author Andrew.Arefyev@gmail.com
 * 22.02.14 2:37
 */
@Component
class PlanetManagerValidator : Validator {
    override fun supports(clazz: Class<*>): Boolean {
        return PlanetaManager::class.java.isAssignableFrom(clazz)
    }

    override fun validate(target: Any, errors: Errors) {
        ValidateUtils.rejectIfNotEmail(errors, "email", "wrong.email")
        ValidationUtils.rejectIfEmpty(errors, "email", "field.required")
        ValidationUtils.rejectIfEmpty(errors, "name", "field.required")
        ValidationUtils.rejectIfEmpty(errors, "fullName", "field.required")
        ValidationUtils.rejectIfEmpty(errors, "managerId", "field.required")
        ValidateUtils.rejectIfNotMatches(errors, "managerId", "field.required", Pattern.compile("[1-9]\\d*"))
    }
}

package ru.planeta.admin.validation

import org.apache.commons.lang3.StringUtils
import org.springframework.stereotype.Component
import org.springframework.validation.Errors
import org.springframework.validation.Validator
import ru.planeta.api.model.promo.ShopPromoConfiguration

/**
 * Date: 29.10.12
 * Time: 16:50
 */
@Component
class ShopPromoConfigurationValidator : Validator {
    override fun supports(aClass: Class<*>): Boolean {
        return aClass.isAssignableFrom(ShopPromoConfiguration::class.java)
    }

    override fun validate(o: Any, errors: Errors) {
        try {
            val concertPromoConfiguration = o as ShopPromoConfiguration
            if (StringUtils.isEmpty(concertPromoConfiguration.imageUrl)) {
                errors.rejectValue("imageUrl", "field.required")
            }
            if (StringUtils.isEmpty(concertPromoConfiguration.link)) {
                errors.rejectValue("link", "field.required")
            }
            if (StringUtils.isEmpty(concertPromoConfiguration.html)) {
                errors.reject("field.required")
            }
        } catch (e: Exception) {
            errors.reject("field.required")
        }

    }
}

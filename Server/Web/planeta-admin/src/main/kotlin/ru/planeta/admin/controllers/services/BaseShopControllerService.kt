package ru.planeta.admin.controllers.services

import org.springframework.stereotype.Service
import org.springframework.web.servlet.ModelAndView
import ru.planeta.api.service.shop.ProductAttributeService
import ru.planeta.api.service.shop.ProductTagService
import ru.planeta.api.web.controllers.IAction
import ru.planeta.api.web.controllers.services.BaseControllerService
import ru.planeta.model.enums.ProjectType


/**
 * Base abstract controller for admin zone
 * User: a.savanovich
 * Date: 02.07.12
 * Time: 12:36
 */

@Service
class BaseShopControllerService(private val baseControllerService: BaseControllerService,
                                private val productTagService: ProductTagService,
                                private val productAttributeService: ProductAttributeService) {

    fun createDefaultAdminModelAndView(action: IAction): ModelAndView =
            baseControllerService.createDefaultModelAndView(action, ProjectType.ADMIN)
                    .addObject("productTags", productTagService.getProductTags(0, 0))
                    .addObject("predefinedAttributes", productAttributeService.tagToAttributesTypeMap)
}


package ru.planeta.admin.controllers.charity

import org.springframework.stereotype.Controller
import org.springframework.validation.BindingResult
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.ModelAttribute
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.servlet.ModelAndView
import ru.planeta.admin.Actions
import ru.planeta.admin.Charity
import ru.planeta.admin.controllers.services.AdminBaseControllerService
import ru.planeta.api.service.charity.CharityLibraryFileService
import ru.planeta.api.charity.CharityLibraryFileThemeService
import ru.planeta.api.web.authentication.myProfileId
import ru.planeta.model.charity.LibraryFile
import ru.planeta.model.charitydb.LibraryFileTheme
import javax.validation.Valid

@Controller
class AdminCharitySchoolController(private val charityLibraryFileService: CharityLibraryFileService,
                                   private val charityLibraryFileThemeService: CharityLibraryFileThemeService,
                                   private val adminBaseControllerService: AdminBaseControllerService) {

    @GetMapping(Charity.ADMIN_CHARITY_SCHOOL_LIBRARY_FILES)
    fun adminCharitySchoolLibraryFiles(@RequestParam(defaultValue = "0") offset: Int,
                                       @RequestParam(defaultValue = "200") limit: Int): ModelAndView {
        val libraryFiles = charityLibraryFileService.selectLibraryFileByHeaderList("", offset, limit)
        return adminBaseControllerService.createAdminDefaultModelAndView(Actions.ADMIN_CHARITY_SCHOOL_LIBRARY_FILES)
                .addObject("libraryFiles", libraryFiles)
    }

    @GetMapping(Charity.ADMIN_CHARITY_SCHOOL_LIBRARY_FILE_EDIT)
    fun adminCharitySchoolLibraryFileEdit(@RequestParam(defaultValue = "0") libraryFileId: Long): ModelAndView {
        var libraryFile: LibraryFile? = charityLibraryFileService.selectLibraryFile(libraryFileId)
        if (libraryFile == null) {
            libraryFile = LibraryFile()
        }
        val libraryFileThemes = charityLibraryFileThemeService.selectLibraryFileThemeList(0, 100)
        return adminBaseControllerService.createAdminDefaultModelAndView(Actions.ADMIN_CHARITY_SCHOOL_LIBRARY_FILE_EDIT)
                .addObject("libraryFile", libraryFile)
                .addObject("libraryFileThemes", libraryFileThemes)
    }

    @GetMapping(Charity.ADMIN_CHARITY_SCHOOL_LIBRARY_FILE_DELETE)
    fun adminCharitySchoolLibraryFileDelete(@RequestParam(defaultValue = "0") libraryFileId: Long): ModelAndView {
        adminBaseControllerService.permissionService.checkAdministrativeRole(myProfileId())
        charityLibraryFileService.deleteLibraryFile(libraryFileId)
        return adminCharitySchoolLibraryFiles(0, 200)
    }

    @PostMapping(Charity.ADMIN_CHARITY_SCHOOL_LIBRARY_FILE_EDIT)
    fun charitySchoolLibraryFileEditPost(@Valid @ModelAttribute("libraryFile") libraryFile: LibraryFile, bindingResult: BindingResult): ModelAndView {

        if (bindingResult.hasErrors()) {
            val libraryFileThemes = charityLibraryFileThemeService.selectLibraryFileThemeList(0, 100)
            return adminBaseControllerService.createAdminDefaultModelAndView(Actions.ADMIN_CHARITY_SCHOOL_LIBRARY_FILE_EDIT)
                    .addObject("libraryFile", libraryFile)
                    .addObject("errors", bindingResult.allErrors)
                    .addObject("libraryFileThemes", libraryFileThemes)
        }
        charityLibraryFileService.insertOrUpdateLibraryFile(libraryFile)
        return adminBaseControllerService.baseControllerService.createRedirectModelAndView(Charity.ADMIN_CHARITY_SCHOOL_LIBRARY_FILES)
    }

    @GetMapping(Charity.ADMIN_CHARITY_SCHOOL_LIBRARY_THEMES)
    fun adminCharitySchoolLibraryThemes(@RequestParam(defaultValue = "200") limit: Int,
                                        @RequestParam(defaultValue = "0") offset: Int): ModelAndView {
        val libraryFileThemes = charityLibraryFileThemeService.selectLibraryFileThemeList(offset, limit)
        return adminBaseControllerService.createAdminDefaultModelAndView(Actions.ADMIN_CHARITY_SCHOOL_LIBRARY_THEMES)
                .addObject("libraryFileThemes", libraryFileThemes)
    }

    @GetMapping(Charity.ADMIN_CHARITY_SCHOOL_LIBRARY_THEME_EDIT)
    fun adminCharitySchoolLibraryThemeEdit(@RequestParam(value = "themeId", defaultValue = "0") libraryFileThemeId: Long): ModelAndView {

        var libraryFileTheme: LibraryFileTheme? = charityLibraryFileThemeService.selectLibraryFileTheme(libraryFileThemeId)
        if (libraryFileTheme == null) {
            libraryFileTheme = LibraryFileTheme()
        }
        return adminBaseControllerService.createAdminDefaultModelAndView(Actions.ADMIN_CHARITY_SCHOOL_LIBRARY_THEME_EDIT)
                .addObject("libraryFileTheme", libraryFileTheme)
    }

    @PostMapping(Charity.ADMIN_CHARITY_SCHOOL_LIBRARY_THEME_EDIT)
    fun adminCharitySchoolLibraryThemeEditPost(@Valid @ModelAttribute("libraryFileTheme") libraryFileTheme: LibraryFileTheme, bindingResult: BindingResult): ModelAndView {

        if (bindingResult.hasErrors()) {
            val libraryFileThemes = charityLibraryFileThemeService.selectLibraryFileThemeList(0, 100)
            return adminBaseControllerService.createAdminDefaultModelAndView(Actions.ADMIN_CHARITY_SCHOOL_LIBRARY_THEME_EDIT)
                    .addObject("libraryFileTheme", libraryFileTheme)
                    .addObject("errors", bindingResult.allErrors)
                    .addObject("libraryFileThemes", libraryFileThemes)
        }
        charityLibraryFileThemeService.insertOrUpdateLibraryFileTheme(libraryFileTheme)
        return adminBaseControllerService.baseControllerService.createRedirectModelAndView(Charity.ADMIN_CHARITY_SCHOOL_LIBRARY_THEMES)
    }
}

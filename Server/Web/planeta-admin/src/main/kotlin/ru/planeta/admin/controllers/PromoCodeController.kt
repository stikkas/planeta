package ru.planeta.admin.controllers

import org.springframework.stereotype.Controller
import org.springframework.validation.BindingResult
import org.springframework.web.bind.annotation.*
import org.springframework.web.servlet.ModelAndView
import ru.planeta.admin.Actions
import ru.planeta.admin.Urls
import ru.planeta.admin.controllers.services.AdminBaseControllerService
import ru.planeta.api.model.ActionStatus
import ru.planeta.api.service.security.PermissionService
import ru.planeta.api.service.shop.ProductTagService
import ru.planeta.api.service.shop.PromoCodeService
import ru.planeta.api.web.authentication.myProfileId
import ru.planeta.api.web.controllers.services.BaseControllerService
import ru.planeta.model.shop.ExtendedPromoCode
import ru.planeta.model.shop.PromoCode
import javax.servlet.http.HttpServletResponse
import javax.validation.Valid

/**
 * Created with IntelliJ IDEA.
 * User: michail
 * Date: 08.04.16
 * Time: 16:16
 */
@Controller
class PromoCodeController(private val promoCodeService: PromoCodeService,
                          private val productTagService: ProductTagService,
                          private val permissionService: PermissionService,
                          private val adminBaseControllerService: AdminBaseControllerService,
                          private val baseControllerService: BaseControllerService) {

    @GetMapping(Urls.ADMIN_SHOP_PROMO_CODES)
    fun getPromoCodes(
            @RequestParam(defaultValue = "") searchString: String,
            @RequestParam(defaultValue = "0") offset: Int,
            @RequestParam(defaultValue = "10") limit: Int): ModelAndView {

        val promoCodesList = promoCodeService.getExtendedPromoCodesList(searchString, offset, limit)
        val count = promoCodeService.getExtendedPromoCodesListCount(searchString)
        val modelAndView = adminBaseControllerService.createAdminDefaultModelAndView(Actions.ADMIN_SHOP_PROMO_CODES)
        return modelAndView
                .addObject("promoCodesList", promoCodesList)
                .addObject("searchString", searchString)
                .addObject("offset", offset)
                .addObject("limit", limit)
                .addObject("count", count)
    }


    @GetMapping(Urls.ADMIN_SHOP_PROMO_CODE)
    fun getPromoCode(@ModelAttribute("promoCode") promoCode: ExtendedPromoCode,
                     errors: BindingResult,
                     @RequestParam(defaultValue = "0") promoCodeId: Long,
                     @RequestParam(required = false) success: Boolean?): ModelAndView {
        var promoCode = promoCode

        val modelAndView = adminBaseControllerService.createAdminDefaultModelAndView(Actions.ADMIN_SHOP_PROMO_CODE)

        if (!errors.hasErrors()) {
            if (promoCodeId > 0) {
                promoCode = promoCodeService.getExtendedPromoCodeById(promoCodeId)
                if (success != null && success) {
                    modelAndView.addObject("success", true)
                }
            } else {
                promoCode = ExtendedPromoCode()
            }
        }

        val productTags = productTagService.getProductTags(0, 0)

        return modelAndView.addObject("promoCode", promoCode)
                .addObject("productTags", productTags)
    }

    @PostMapping(Urls.ADMIN_SHOP_PROMO_CODE)
    fun updatePromoCode(@Valid @ModelAttribute("promoCode") promoCode: ExtendedPromoCode,
                        result: BindingResult): ModelAndView {
        if (result.hasErrors()) {
            return getPromoCode(promoCode, result, promoCode.promoCodeId, false)
        }

        promoCodeService.insertOrUpdatePromoCode(promoCode)
        return getPromoCode(promoCode, result, promoCode.promoCodeId, true)
    }

    @RequestMapping(Urls.ADMIN_GENERATE_PROMO_CODE)
    @ResponseBody
    fun generatePromoCode(response: HttpServletResponse, @RequestParam(defaultValue = "1") count: Int): ActionStatus<*> {
        val clientId = myProfileId()
        permissionService.checkAdministrativeRole(clientId)
        val code = promoCodeService.generateCode(PromoCode.CODE_LENGTH)
        return ActionStatus.createSuccessStatus(code)
    }

    @RequestMapping(Urls.ADMIN_DELETE_PROMO_CODE)
    fun disablePromoCode(@RequestParam promoCodeId: Long,
                         @RequestParam(defaultValue = "false") disabled: Boolean): ModelAndView {
        if (disabled) {
            promoCodeService.markPromoCodeEnabled(promoCodeId)
        } else {
            promoCodeService.markPromoCodeDisabled(promoCodeId)
        }

        return baseControllerService.createRedirectModelAndView(Urls.ADMIN_SHOP_PROMO_CODES)
    }
}


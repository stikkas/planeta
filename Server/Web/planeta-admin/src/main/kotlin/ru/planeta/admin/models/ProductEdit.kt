package ru.planeta.admin.models

import ru.planeta.model.shop.ProductAttribute
import ru.planeta.model.shop.ProductInfo

/**
 * This class represents product, while it's being created/edited
 * User: m.shulepov
 * Date: 23.07.12
 * Time: 18:05
 */
class ProductEdit : ProductInfo() {

    var openSection: String? = null
    var productAttributes: List<ProductAttribute>? = null
    var referrerWebAlias: String? = null
}

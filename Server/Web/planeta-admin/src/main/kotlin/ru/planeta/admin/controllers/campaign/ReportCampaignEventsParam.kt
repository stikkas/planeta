package ru.planeta.admin.controllers.campaign

import ru.planeta.model.common.campaign.param.CampaignEventsParam
import ru.planeta.reports.ReportType

/**
 * Created by asavan on 07.12.2016.
 */
class ReportCampaignEventsParam : CampaignEventsParam() {
    var reportType = ReportType.CSV

}

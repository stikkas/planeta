@file:JvmName("Urls")

package ru.planeta.admin

/**
 * Class containing all urls used in webapp
 *
 * @author atropnikov
 * Class URLS
 */
object ConcertUrls {
    const val CONCERT_LIST = "/admin/concert-list.html"
    const val CONCERT = "/admin/concert.html"
    const val SAVE_CONCERT = "/admin/save-concert.json"
    const val IMPORT_CONCERTS = "/admin/import-concerts.json"
}

object Shop {

    const val ADMIN_SHOP_ORDERS_REPORT = "/admin/shop-orders-report.html"
    const val ADMIN_SHOP_ORDERS_MAIL_REPORT = "/admin/shop-orders-mail-report.json"

    const val PRODUCT_CREATE = "/admin/products/{groupAlias}/create.html"
    const val PRODUCT_EDIT = "/admin/products/{groupAlias}/{productId}/edit.html"
    const val PRODUCT_EDIT_NEW = "/admin/products/{productId}/edit.html"
    const val PRODUCT_SAVE = "/admin/product-save.json"
    const val PRODUCTS_LIST = "/admin/products-list.json"
    const val PRODUCT_COMMENTS_LIST = "/moderator/products-comments.html"
    const val PRODUCT_TAGS_SAVE = "/admin/product-tags-save.json"

    const val PRODUCT_START = "/admin/product-start.json"
    const val PRODUCT_PAUSE = "/admin/product-pause.json"
    const val PRODUCT_DELETE = "/admin/product-deleteByProfileId.json"

    const val STORE_INFO = "/admin/products/{groupAlias}.html"
    const val STORE_PRODUCTS = "/admin/load-store-products.json"
    const val PRODUCT_CLONE = "/admin/clone-product.json"

    // products quantity in warehouses
    const val PRODUCT_UPDATE_QUANTITY = "/admin/product-update-quantity.json"
    const val PRODUCT_BIND_TO_PROFILE = "/admin/product-bind-to-profile.json"
    const val PRODUCT_FIND_DONATE_BY_PROFILE = "/admin/product-find-donate-by-profile.json"
}

object School {
    const val ADMIN_SEMINARS = "/admin/seminars.html"
    const val ADMIN_SEMINAR_INFO = "/admin/seminar-info.html"
    const val ADMIN_SEMINAR_REGISTRATION_REPORT = "/admin/seminar-registrations-report.html"
    const val ADMIN_SPEAKER_SAVE = "/admin/speaker/save.json"
    const val ADMIN_PARTNER_SAVE = "/admin/partner/save.json"
    const val ADMIN_SHOW_SEMINAR_ON_MAIN_PAGE = "/admin/show-seminar-on-main-page.html"
    const val ADMIN_HIDE_SEMINAR_FROM_MAIN_PAGE = "/admin/hide-seminar-from-main-page.html"
    const val ADMIN_REMOVE_SEMINAR = "/admin/seminar-remove.html"
}

object Loyalty {
    const val ADMIN_LOYALTY_ORDERS = "/moderator/loyalty-orders.html"
    const val ADMIN_EXPORT_LOYALTY_ORDERS = "/moderator/export-loyalty-orders.csv"
    const val ADMIN_LOYALTY_BONUSES = "/moderator/loyalty-bonuses.html"
    const val ADMIN_LOYALTY_VIP_USERS = "/moderator/loyalty-vip-users.html"
    const val ADMIN_REORDER_BONUSES = "/admin/loyalty/reorder-bonuses.json"
    const val ADMIN_LOYALTY_CANCEL_ORDER = "/moderator/cancel-loyalty-order.json"
    const val ADMIN_LOYALTY_COMPLETE_ORDER = "/moderator/complete-loyalty-order.json"
    const val ADMIN_LOYALTY_SAVE_BONUS = "/admin/loyalty/save-bonus.json"
    const val ADMIN_LOYALTY_CHANGE_USER_VIP_STATUS = "/admin/loyalty/change-vip-status.json"
}

object UserCallback {
    const val MODERATOR_USER_CALLBACKS = "/moderator/user-callbacks.html"
    const val MODERATOR_USER_CALLBACK_COMMENT = "/moderator/user-callback-comment.html"
    const val MODERATOR_UNPROCESSED_CALLBACKS = "/moderator/callbacks/unprocessed.json"
    const val MODERATOR_PROCESSED_CALLBACKS = "/moderator/callbacks/processed.json"
}

object Investing {
    const val INVESTING_ORDERS_INFO = "/admin/investing-orders-info.html"
    const val INVESTING_ORDER_INFO = "/admin/investing-order-info.html"
    const val INVESTING_ORDER_INFO_MODERATE = "/admin/investing-order-info-moderate.html"
}

object Biblio {
    const val ADMIN_PARTNERS_FILL = "/admin/biblio/partners-biblio-fill.html"
    const val ADMIN_PARTNERS_DELETE = "/admin/biblio/partners-biblio-deleteByProfileId.html"
    const val ADMIN_PARTNERS_EDIT = "/admin/biblio/partners-biblio-edit.html"
    const val ADMIN_PARTNERS_LIST = "/admin/biblio/partners-biblio-list.html"
    const val ADMIN_PARTNERS_SORT = "/admin/biblio/partners-biblio-sort.html"

    const val ADMIN_ADVERTISE_FILL = "/admin/biblio/advertise-biblio-fill.html"
    const val ADMIN_ADVERTISE_DELETE = "/admin/biblio/advertise-biblio-deleteByProfileId.html"
    const val ADMIN_ADVERTISE_EDIT = "/admin/biblio/advertise-biblio-edit.html"
    const val ADMIN_ADVERTISE_LIST = "/admin/biblio/advertise-biblio-list.html"
    const val ADMIN_ADVERTISE_SORT = "/admin/biblio/advertise-biblio-sort.html"

    const val ADMIN_BIBLIO_ORDERS = "/admin/biblio/orders.html"
}

object Charity {
    const val ADMIN_PARTNERS_FILL = "/admin/charity/partners-charity-fill.html"
    const val ADMIN_PARTNERS_DELETE = "/admin/charity/partners-charity-deleteByProfileId.html"
    const val ADMIN_PARTNERS_EDIT = "/admin/charity/partners-charity-edit.html"
    const val ADMIN_PARTNERS_LIST = "/admin/charity/partners-charity-list.html"
    const val ADMIN_PARTNERS_SORT = "/admin/charity/partners-charity-sort.html"
    const val ADMIN_CHARITY_POSTS = "/admin/charity/posts.html"
    const val ADMIN_CHARITY_DELETE_POST = "/admin/charity/deleteByProfileId-post.html"
    const val ADMIN_POSTS = "/admin/posts.html"
    const val ADMIN_POST = "/admin/post.html"

    const val ADMIN_CHARITY_SCHOOL_LIBRARY_THEMES = "/admin/charity/school-library-themes.html"
    const val ADMIN_CHARITY_SCHOOL_LIBRARY_THEME_EDIT = "/admin/charity/school-library-theme-edit.html"
    const val ADMIN_CHARITY_SCHOOL_LIBRARY_FILES = "/admin/charity/school-library-files.html"
    const val ADMIN_CHARITY_SCHOOL_LIBRARY_FILE_EDIT = "/admin/charity/school-library-file-edit.html"
    const val ADMIN_CHARITY_SCHOOL_LIBRARY_FILE_DELETE = "/admin/charity/school-library-file-deleteByProfileId.html"
}

object LibraryUrls {
    const val LIBRARY_LOAD_LIBRARIES_FROM_CSV = "/admin/library/loadCSV.html"
    const val LIBRARIES = "/admin/library/libraries.html"
    const val LIBRARIES_REPORT = "/admin/library/libraries-report.html"
    const val LIBRARY = "/admin/library/library.html"
    const val DELETE_LIBRARY = "/admin/library/delete_library.json"
    const val ADD_LIBRARY = "/admin/library/add_library.html"
}

object BookUrls {
    const val BOOKS = "/admin/book/books.html"
    const val BOOKS_REPORT = "/admin/book/books-report.html"
    const val BOOK = "/admin/book/book.html"
    const val DELETE_BOOK = "/admin/book/deleteByProfileId-book.html"
    const val SAVE_BOOK = "/admin/save-book.html"
}

object PublishingHouseUrls {
    const val PUBLISHING_HOUSE = "/admin/biblio/publishing-house.html"
    const val PUBBLISHING_HOUSES = "/admin/biblio/publishing-houses.html"
    const val PUBLISHING_HOUSES_REPORT = "/admin/biblio/publishing-houses-report.html"
}

object Techobattle {
    const val PROJECT_LIST = "/moderator/promo/projects.html"
    const val PROJECT = "/moderator/promo/project.html"
}

object Urls {
    const val LOGIN = "/login.html"
    const val ADMIN_INDEX = "/"
    const val ADMIN_CAMPAIGNS = "/moderator/campaigns.html"
    const val ADMIN_MANAGER_CAMPAIGNS = "/moderator/manager-campaigns.html"
    const val ADMIN_CAMPAIGNS_REPORT = "/moderator/campaigns-report.html"
    const val ADMIN_QUIZ_ANSWERS_REPORT = "/moderator/quiz-answers-report.html"
    const val ADMIN_CAMPAIGNS_MAIL_REPORT = "/moderator/campaigns-mail-report.html"

    //need for backward compatibility with old notification
    const val ADMIN_GROUP_INFO = "/admin/group-info.html"
    const val MODERATOR_GROUP_INFO = "/moderator/group-info.html"
    const val MODERATOR_CAMPAIGN_INFO = "/moderator/campaign-info.html"
    const val MODERATOR_CAMPAIGN_SET_EDITABLE = "/moderator/campaign-set-editable.html"
    const val MODERATOR_CAMPAIGN_MODERATION = "/moderator/campaign-moderation.html"
    const val MODERATOR_INTERNAL_MESSAGE = "/moderator/save-internal-message.html"
    const val MODERATOR_REMOVE_CAMPAIGN_ADMIN = "/moderator/remove-campaign-admin.html"
    const val MODERATOR_ADD_CAMPAIGN_ADMIN = "/moderator/add-campaign-admin.html"
    const val MODERATOR_CAMPAIGN_MANAGER_SELECT = "/moderator/campaign-manager-selectCampaignById.html"
    const val MODERATOR_CAMPAIGN_CONTRACTOR_SELECT = "/moderator/campaign-contractor-selectCampaignById.html"
    const val MODERATOR_CAMPAIGN_MODERATION_INFO = "/moderator/campaign-moderation-info.html"
    const val ADMIN_DELETE_GROUP = "/admin/deleteByProfileId-group.html"

    const val ADMIN_GROUP_SET_STATUS = "/moderator/group-set-status.html"
    const val ADMIN_USER_SET_STATUS = "/moderator/user-set-status.html"
    const val ADMIN_USERS = "/moderator/users.html"
    const val ADMIN_CONTRACTORS = "/moderator/contractors.html"
    const val ADMIN_USER_INFO = "/moderator/user-info.html"
    const val ADMIN_SET_USER_ROLE = "/moderator/set-user-role.html"
    const val ADMIN_DECREASE_USER_BALANCE = "/admin/decrease-user-balance.html"
    const val ADMIN_FREEZE_AMOUNT = "/admin/freeze-amount.html"
    const val ADMIN_UNFREEZE_AMOUNT = "/admin/unfreeze-amount.html"
    const val ADMIN_DECREASE_FROZEN_AMOUNT = "/admin/decrease-frozen-amount.html"
    const val ADMIN_DELETE_USER_POSTS = "/admin/deleteByProfileId-user-posts.html"
    const val ADMIN_DELETE_USER_COMMENTS = "/admin/deleteByProfileId-user-comments.html"
    const val ADMIN_DELETE_USER = "/admin/deleteByProfileId-user.html"
    const val ADMIN_LOGIN_AS_USER = "/admin/login-as-user.html"
    const val ADMIN_CHANGE_USER_PASSWORD = "/moderator/change-user-password.html"
    const val ADMIN_CHANGE_USER_EMAIL = "/moderator/change-user-email.html"
    const val ADMIN_UNSUBSCRIBE_USER_FROM_ALL_EMAIL_NOTIFICATIONS = "/moderator/unsubscribe-user-from-all-email-notifications.html"
    const val ADMIN_CONFIRM_USER_REGISTRATION = "/moderator/confirm-user-registration.html"
    const val ADMIN_SEND_REGISTRATION_COMPLETE_EMAIL = "/moderator/send-registration-complete-email.html"

    const val ADMIN_BROADCASTS = "/moderator/broadcasts.html"
    const val ADMIN_CONFIGURATION_LIST = "/admin/configuration-list.html"
    const val ADMIN_CONFIGURATION_EDIT = "/admin/configuration-edit.html"
    const val ADMIN_CONFIGURATION_DELETE = "/admin/configuration-deleteByProfileId.html"

    const val ADMIN_STATISTIC_COMPOSITE_REPORT = "/admin/sales-report.html"
    const val ADMIN_STATISTIC_SHARE_PURCHASE_REPORT = "/admin/sales-shares-report.html"
    const val ADMIN_STATISTIC_PRODUCT_PURCHASE_REPORT = "/admin/sales-products-report.html"
    const val ADMIN_STATISTIC_REGISTRATION_REPORT = "/admin/registrations-report.html"

    const val ADMIN_STATISTIC_COMPOSITE = "/admin/statistic-composite.html"
    const val ADMIN_STATISTIC_SHARE_PURCHASE = "/admin/statistic-share.html"
    const val ADMIN_STATISTIC_PRODUCT_PURCHASE = "/admin/statistic-product.html"

    const val ADMIN_STATISTIC_REGISTRATION_MODAL = "/admin/statistic-user-modal.html"
    const val ADMIN_STATISTIC_SHARE_PURCHASE_MODAL = "/admin/statistic-share-modal.html"
    const val ADMIN_STATISTIC_TICKET_PURCHASE_MODAL = "/admin/statistic-ticket-modal.html"
    const val ADMIN_STATISTIC_PRODUCT_PURCHASE_MODAL = "/admin/statistic-product-modal.html"

    const val ADMIN_STATISTIC_FORECAST = "/admin/statistic-forecast.html"

    const val ADMIN_IMAGE_UPLOAD = "/moderator/image-upload.html"
    const val ADMIN_FILE_UPLOAD = "/moderator/file-upload.html"
    const val ADMIN_DOCUMENTS_UPLOAD = "/moderator/document-upload.html"
    const val ADMIN_DOCUMENT = "/moderator/document.html"
    const val ADMIN_LAST_UPLOADED_IMAGES = "/moderator/last-uploaded-images.json"
    const val ADMIN_LAST_UPLOADED_FILES = "/moderator/last-uploaded-files.json"
    const val ADMIN_DELETE_PROFILE_FILE = "/moderator/deleteByProfileId-profile-file.json"

    const val ADMIN_GIFTS = "/admin/gifts.html"
    const val ADMIN_NESTED_LOCATIONS = "/admin/get-nested-locations.json"

    //
    const val ADMIN_START_PROMO_LIST = "/moderator/start-promo-list.html"
    const val ADMIN_START_CHARITY_PROMO_LIST = "/moderator/start-charity-promo-list.html"
    const val ADMIN_CHARITY_BROADCAST_LIST = "/moderator/charity-broadcast-list.html"
    const val ADMIN_START_CHARITY_TOP_LIST = "/moderator/start-charity-top-list.html"
    //

    //
    const val ADMIN_BROADCAST_PROMO_LIST = "/moderator/broadcast-promo-list.html"
    const val ADMIN_ABOUT_US_NEW_SETTINGS = "/moderator/about-us-new-settings.html"

    const val ADMIN_PARTNERS_PROMO_FILL = "/moderator/partners-promo-fill.html"
    const val ADMIN_PARTNERS_PROMO_ADD = "/moderator/partners-promo-add.html"
    const val ADMIN_PARTNERS_PROMO_DELETE = "/moderator/partners-promo-deleteByProfileId.html"
    const val ADMIN_PARTNERS_PROMO_EDIT = "/moderator/partners-promo-edit.html"
    const val ADMIN_PARTNERS_PROMO_LIST = "/moderator/partners-promo-list.html"
    const val ADMIN_PARTNERS_PROMO_SORT = "/moderator/partners-promo-sort.html"

    const val ADMIN_CHARITY_PARTNERS_PROMO_LIST = "/moderator/charity-partners-promo-list.html"
    const val ADMIN_CHARITY_PARTNERS_PROMO_FILL = "/moderator/charity-partners-promo-fill.html"
    const val ADMIN_CHARITY_PARTNERS_PROMO_ADD = "/moderator/charity-partners-promo-add.html"
    const val ADMIN_CHARITY_PARTNERS_PROMO_DELETE = "/moderator/charity-partners-promo-deleteByProfileId.html"
    const val ADMIN_CHARITY_PARTNERS_PROMO_EDIT = "/moderator/charity-partners-promo-edit.html"
    const val ADMIN_CHARITY_PARTNERS_PROMO_SORT = "/moderator/charity-partners-promo-sort.html"
    //

    const val ADMIN_CHANGE_ALIAS_HTML = "/moderator/event-set-alias.html"
    const val ADMIN_CHANGE_ALIAS = "/moderator/change-alias.json"


    const val ADMIN_CONTRACTOR_EDIT = "/moderator/edit-contractor.html"
    const val ADMIN_CONTRACTOR_SAVE = "/moderator/save-contractor.json"
    const val ADMIN_CONTRACTOR_BIND = "/moderator/bind-contractor.json"


    //<editor-fold defaultstate="collapsed" desc="shop urls">
    const val ADMIN_ORDERS = "/moderator/orders.html"
    const val ADMIN_ORDER_ONE = "/moderator/order/{orderId}"
    const val ADMIN_ADDRESSES_STICKERS = "/moderator/campaign-addresses-stickers.html"
    const val ADMIN_ADDRESSES_STICKERS_FROM_FILE = "/moderator/campaign-addresses-stickers-from-file.html"
    const val ADMIN_BIBLIO_RUSSIAN_POST_BOOKS_INFO_FROM_FILE = "/moderator/biblio-russian-post-books-info-from-file.html"
    const val ADMIN_BIBLIO_RUSSIAN_PRICE_LISTS = "/moderator/biblio-russian-post-price-lists.html"
    const val ADMIN_BIBLIO_RUSSIAN_MAGAZINE_INFO_LIST = "/moderator/biblio-russian-post-magazine-info-list.html"
    const val ADMIN_SHOP_ORDERS = "/moderator/shop/orders.html"
    const val ADMIN_SHOP_PROMO_CODES = "/moderator/shop/promo-codes.html"
    const val ADMIN_SHOP_PROMO_CODE = "/moderator/shop/promo-code.html"
    const val ADMIN_SHOP_PRODUCT_TAGS = "/moderator/shop/product-tags.html"
    const val ADMIN_SHOP_PRODUCT_TAG = "/moderator/shop/product-tag.html"
    const val ADMIN_SHOP_SAVE_PRODUCT_TAG = "/moderator/shop/save-product-tag.html"

    const val ADMIN_GENERATE_PROMO_CODE = "/moderator/shop/generate-promo-code.json"
    const val ADMIN_DELETE_PROMO_CODE = "/moderator/shop/deleteByProfileId-promo-code.json"
    const val ORDER_CHANGE_DELIVERY_STATUS = "/moderator/order-change-delivery-status.json"

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="delivery urls">
    const val DELIVERY_LIST = "/moderator/shop/delivery-list.html"
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Banner urls">
    const val SAVE_BANNER = "/admin/save-banner.html"

    const val SAVE_BANNER_SIMPLE = "/admin/save-banner-simple.html"
    const val EDIT_BANNER = "/admin/edit-banner.html"
    const val EDIT_BANNER_SIMPLE = "/admin/edit-banner-simple.html"
    const val COPY_BANNER = "/admin/copy-banner.html"
    const val SWITCH_ON_OFF_BANNER = "/admin/switch-on-off-banner.html"
    const val DELETE_BANNER = "/admin/deleteByProfileId-banner.html"
    const val BANNERS_LIST = "/admin/banners-list.html"
    const val ALL_BANNERS_LIST = "/admin/all-banners-list.html"
    const val CREATE_BANNER_TO_CAMPAIGN_QUICK = "/moderator/campaign-quick-banner.html"

    const val SAVE_SHORT_LINK = "/admin/save-short-link.html"
    const val EDIT_SHORT_LINK = "/admin/edit-short-link.html"
    const val GENERATE_SOCIAL_SHORT_LINKS = "/admin/generate-social-short-links.html"
    const val SWITCH_ON_OFF_SHORT_LINK = "/admin/switch-on-off-short-link.html"
    const val DELETE_SHORT_LINK = "/admin/deleteByProfileId-short-link.html"
    const val SHORT_LINKS_LIST = "/admin/short-links-list.html"


    const val SPONSORS = "/admin/sponsors.html"
    const val EDIT_SPONSOR = "/admin/edit-sponsor.html"
    const val SAVE_SPONSOR = "/admin/save-sponsor.html"
    const val DELETE_SPONSOR = "/admin/deleteByProfileId-sponsor.html"

    const val CAMPUSES = "/admin/campuses.html"
    const val EDIT_CAMPUS = "/admin/edit-campus.html"
    const val SAVE_CAMPUS = "/admin/save-campus.html"
    const val DELETE_CAMPUS = "/admin/deleteByProfileId-campus.html"

    const val QUIZZES = "/admin/quizzes.html"
    const val QUIZ_INFO = "/admin/quiz-info.html"
    const val QUIZ_QUESTIONS_SORT = "/admin/quiz-questions-sort.html"
    const val QUIZ_QUESTION_EDIT = "/admin/quiz-question-edit.html"
    const val QUIZ_QUESTION_OPTION = "/admin/quiz-question-option-edit.json"
    const val QUIZ_QUESTION_OPTION_REMOVE = "/admin/quiz-question-option-remove.json"
    const val QUIZ_QUESTIONS_OPTION_SORT = "/admin/quiz-questions-option-sort.html"

    const val CANCEL_CAMPAIGN_PURCHASES = "/admin/cancel-all-purchases.html"
    //</editor-fold>

    const val CAMPAIGN_MANAGERS_RELATIONS = "/moderator/campaign-managers-relations.html"
    const val UPDATE_CAMPAIGN_MANAGERS_RELATION = "/moderator/change-manager-relation.html"
    const val ADD_MANAGER = "/moderator/add-manager.html"
    const val SAVE_MANAGER = "/moderator/save-manager.html"

    const val CAMPAIGN_MANAGEMENT = "/moderator/campaign-management.html"
    const val CAMPAIGN_NEWS = "/moderator/campaign-news.html"
    const val CAMPAIGN_NEWS_REPORT = "/moderator/campaign-news-report.json"
    const val CAMPAIGN_POSTS = "/moderator/campaign-posts.html"
    const val CAMPAIGN_POSTS_REPORT = "/moderator/campaign-posts-report.json"
    const val CAMPAIGN_COMMENTS = "/moderator/campaign-comments.html"
    const val CAMPAIGN_SHARES = "/moderator/campaign-shares.html"
    const val CAMPAIGN_COMMENTS_REPORT = "/moderator/campaign-comments-report.json"
    const val CAMPAIGN_SHARES_REPORT = "/moderator/campaign-shares-report.json"

    const val START_CAMPAIGNS_GET_SHARES = "/moderator/get-star-campaign-shares.json"
    const val CONFIRMATION_CAMPAIGN_CHANGE_STATUS = "/admin/confirmation-campaign-change-status.json"
    const val CAMPAIGN_TAGS = "/moderator/campaign-tags.html"
    const val EDIT_CATEGORY = "/admin/edit-category.html"
    const val SAVE_CAMPAIGN_CATEGORY = "/admin/save-category.html"
    const val HANDLE_CAMPAIGN_CHANGE_STATUS = "/admin/handle-campaign-change-status.json"

    const val RENEW_CAMPAIGN_VIDEOS = "/admin/renew-campaign-videos.html"

    // admin new urls
    const val DASHBOARD = "/dashboard.html"

    const val ADMIN_BROADCAST_INFO = "/moderator/event-broadcast-info.html"
    const val ADMIN_BROADCAST_REMOVE = "/moderator/event-broadcast-remove.html"
    const val ADMIN_BROADCAST_STREAM_INFO = "/moderator/event-broadcast-stream-info.html"
    const val ADMIN_BROADCAST_TOGGLE_STREAM_STATE = "/moderator/event-broadcast-toggle-stream-state.html"
    const val ADMIN_BROADCAST_STREAM_REMOVE = "/moderator/event-broadcast-stream-remove.html"
    const val ADMIN_BROADCAST_START = "/moderator/event-broadcast-start.html"
    const val ADMIN_BROADCAST_STOP = "/moderator/event-broadcast-stop.html"
    const val ADMIN_BROADCAST_PAUSE = "/moderator/event-broadcast-pause.html"
    const val ADMIN_BROADCAST_SET_DEFAULT_STREAM = "/moderator/event-broadcast-set-default-stream.html"

    const val ADMIN_BROADCAST_ADD_GEO_TARGETING = "/moderator/event-broadcast-geo-targeting-add.html"
    const val ADMIN_BROADCAST_REMOVE_GEO_TARGETING = "/moderator/event-broadcast-geo-targeting-remove.html"
    const val ADMIN_BROADCAST_REMOVE_GEO_TARGETING_BY_PARAMETERS = "/moderator/event-broadcast-geo-targeting-remove-by-params.html"

    const val ADMIN_BROADCAST_ADD_BACKERS_TARGETING = "/moderator/event-broadcast-backers-targeting-add.html"
    const val ADMIN_BROADCAST_REMOVE_BACKERS_TARGETING = "/moderator/event-broadcast-backers-targeting-remove.html"

    const val ADMIN_BROADCAST_ADD_PRODUCT_TARGETING = "/moderator/event-broadcast-product-targeting-add.html"
    const val ADMIN_BROADCAST_REMOVE_PRODUCT_TARGETING = "/moderator/event-broadcast-product-targeting-remove.html"

    const val ADMIN_BROADCAST_PRIVATE_TARGETING = "/moderator/event-broadcast-private-targeting.html"
    const val ADMIN_BROADCAST_ADD_PRIVATE_TARGETING = "/moderator/event-broadcast-private-targeting-add.html"

    const val ADMIN_BROADCAST_SAVE_ADVERTISING = "/moderator/event-broadcast-add-advertising.html"
    const val ADMIN_BROADCAST_DELETE_ADVERTISING = "/moderator/event-broadcast-deleteByProfileId-advertising.html"

    const val ADMIN_CANCEL_PAYMENT = "/admin/billing/cancel-payment.json"
    const val ADMIN_PROCESS_PAYMENT = "/admin/billing/process-payment.json"
    const val ADMIN_BILLING_PAYMENTS = "/admin/billing/payments.html"
    const val ADMIN_BILLING_ORDER = "/admin/billing/order.html"
    const val ADMIN_BILLING_ORDER_LOG = "/admin/billing/log-records.html"
    const val ADMIN_BILLING_PAYMENT = "/admin/billing/payment.html"
    const val ADMIN_CANCEL_ORDER = "/admin/billing/cancel-order.json"
    const val ADMIN_TRANSACTIONS_HISTORY = "/admin/billing/transactions-history.html"

    const val ADMIN_BILLING_PAYMENT_ERRORS = "/admin/billing/payment-errors.html"
    const val ADMIN_BILLING_PAYMENT_ERROR_COMMENT_LIST = "/admin/billing/payment-error-comment-list.html"
    const val ADMIN_BILLING_PAYMENT_ERROR_ADD_COMMENT = "/admin/billing/payment-error-add-comment.html"
    const val ADMIN_BILLING_PAYMENT_ERRORS_CHANGE_STATUS = "/admin/billing/payment-errors-change-status.json"

    const val PAYMENT_CONFIGURATOR = "/moderator/payment-configurator.html"
    const val SAVE_PROJECT_PAYMENT_TOOL = "/admin/save-project-payment-tool.html"

    const val ADMIN_CUSTOM_META_TAG_DELETE_NEW = "/admin/custom-meta-tag-deleteByProfileId-new.json"
    const val ADMIN_CUSTOM_META_TAG_LIST = "/admin/custom-meta-tag-list.html"
    const val ADMIN_CUSTOM_META_TAG_EDIT_NEW = "/admin/custom-meta-tag-edit-new.html"
    const val ADMIN_CUSTOM_META_TAG_SAVE_NEW = "/admin/custom-meta-tag-save-new.html"

    const val MODERATOR_BASE_DELIVERY_SERVICES = "/moderator/base-delivery-services.html"
    const val MODERATOR_GET_BASE_DELIVERY_SERVICES = "/moderator/base-delivery-services.json"
    const val ADMIN_DELIVERY_UPDATE_BASE_SERVICE = "/admin/delivery/update-base-service.json"
    const val ADMIN_DELIVERY_UPDATE_LINCED_SERVICE = "/admin/delivery/update-linked-service.json"
    const val ADMIN_DELIVERY_GET_LINKED_SERVICE = "/admin/delivery/get-linked-service.json"
    const val ADMIN_DELIVERY_REMOVE_BASE_SERVICE = "/admin/delivery/remove-base-service.json"
    const val ADMIN_DELIVERY_REMOVE_LINKED_SERVICE = "/admin/delivery/remove-linked-delivery.json"
    const val ADMIN_SET_SHARE_TO_INVESTING = "/admin/set-share-to-investing.json"
    const val ADMIN_GEO_ADD_CITY = "/admin/geo/add-new-city.html"
    const val QUIZ_QUESTION_CHANGE_ENABLING = "/admin/quiz-question-enabling.json"

    const val PROMO_CONFIG_LIST = "/admin/promo-config-list.html"
    const val PROMO_CONFIG = "/admin/promo-config.html"
    const val PROMO_CONFIG_CHANGE_STATUS = "/admin/promo-config-switch-on-off.html"
}

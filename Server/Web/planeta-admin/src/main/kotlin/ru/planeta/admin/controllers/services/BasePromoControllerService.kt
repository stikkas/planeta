package ru.planeta.admin.controllers.services

import com.fasterxml.jackson.databind.ObjectMapper
import org.springframework.validation.BindingResult
import org.springframework.web.servlet.ModelAndView
import ru.planeta.admin.Actions
import ru.planeta.api.exceptions.NotFoundException
import ru.planeta.api.model.promo.BasePromoConfiguration
import ru.planeta.api.model.promo.PartnersPromoConfiguration
import ru.planeta.api.service.configurations.ConfigurationService
import ru.planeta.api.web.controllers.services.BaseControllerService
import java.util.*

abstract class BasePromoControllerService(val adminBaseControllerService: AdminBaseControllerService,
                                          private val baseControllerService: BaseControllerService,
                                          private val configurationService: ConfigurationService) {

    private val configurationClass: BasePromoConfiguration = PartnersPromoConfiguration()

    protected abstract val configurationKey: String
    protected abstract val actionAdd: Actions
    protected abstract val actionEdit: Actions
    protected abstract val actionList: Actions
    protected abstract val urlList: String

    private fun <T : BasePromoConfiguration> getConfigurationList(): MutableList<T> =
            configurationService.getJsonArrayConfig(configurationClass.javaClass, configurationKey) as MutableList<T>

    fun promoAddPost(mainConfiguration: BasePromoConfiguration, bindingResult: BindingResult): ModelAndView {
        if (bindingResult.hasErrors()) {
            return adminBaseControllerService.createAdminDefaultModelAndView(actionAdd)
        }
        val mainConfigurationList = getConfigurationList<BasePromoConfiguration>()
        mainConfigurationList.add(0, mainConfiguration)
        configurationService.saveConfigAsJson(mainConfigurationList, configurationKey)
        return baseControllerService.createRedirectModelAndView(urlList)
    }

    fun promoAddPreviewGet(mainConfiguration: BasePromoConfiguration): ModelAndView {
        return baseControllerService.createRedirectModelAndView(urlList)
    }

    fun promoAddPreviewPost(mainConfiguration: BasePromoConfiguration): ModelAndView {
        val welcomePromoConfigurationList = getConfigurationList<BasePromoConfiguration>()
        Collections.reverse(welcomePromoConfigurationList)
        welcomePromoConfigurationList.add(mainConfiguration)
        Collections.reverse(welcomePromoConfigurationList)
        val modelAndView = adminBaseControllerService.createAdminDefaultModelAndView(actionAdd)
        setPreview(modelAndView, welcomePromoConfigurationList)
        return modelAndView
    }

    fun promoDeleteGet(id: Int): ModelAndView {
        val mainConfigurationList = getConfigurationList<BasePromoConfiguration>()
        try {
            mainConfigurationList.removeAt(id)
        } catch (e: Exception) {
            throw NotFoundException(BasePromoConfiguration::class.java, id.toLong())
        }

        configurationService.saveConfigAsJson(mainConfigurationList, configurationKey)
        return baseControllerService.createRedirectModelAndView(urlList)
    }

    fun promoEditGet(welcomePromoConfiguration: BasePromoConfiguration): ModelAndView {
        var welcomePromoConfiguration = welcomePromoConfiguration
        val modelAndView = adminBaseControllerService.createAdminDefaultModelAndView(actionEdit)
        val welcomePromoConfigurationList = getConfigurationList<BasePromoConfiguration>()
        try {
            val id = welcomePromoConfiguration.id
            welcomePromoConfiguration = welcomePromoConfigurationList[id]
            welcomePromoConfiguration.id = id
        } catch (e: Exception) {
            throw NotFoundException("PromoEditFail")
        }

        modelAndView.addObject(CONFIGURATION_ATTR_NAME, welcomePromoConfiguration)
        setPreview(modelAndView, welcomePromoConfigurationList)
        return modelAndView
    }

    fun promoEditPost(welcomePromoConfiguration: BasePromoConfiguration, bindingResult: BindingResult): ModelAndView {
        val welcomePromoConfigurationList = getConfigurationList<BasePromoConfiguration>()
        try {
            welcomePromoConfigurationList[welcomePromoConfiguration.id] = welcomePromoConfiguration
        } catch (e: Exception) {
            throw NotFoundException("promoEditPostFail")
        }

        if (bindingResult.hasErrors()) {
            val modelAndView = adminBaseControllerService.createAdminDefaultModelAndView(actionEdit)
            setPreview(modelAndView, welcomePromoConfigurationList)
            return modelAndView
        }
        configurationService.saveConfigAsJson(welcomePromoConfigurationList, configurationKey)
        return baseControllerService.createRedirectModelAndView(urlList)
    }

    fun promoEditPreviewGet(): ModelAndView = baseControllerService.createRedirectModelAndView(urlList)

    fun promoEditPreviewPost(mainConfiguration: BasePromoConfiguration): ModelAndView {
        val welcomePromoConfigurationList = getConfigurationList<BasePromoConfiguration>()
        try {
            welcomePromoConfigurationList[mainConfiguration.id] = mainConfiguration
        } catch (e: Exception) {
            throw NotFoundException("promoEditPreviewPostFail")
        }

        val modelAndView = adminBaseControllerService.createAdminDefaultModelAndView(actionEdit)
        setPreview(modelAndView, welcomePromoConfigurationList)
        return modelAndView
    }

    fun promoList(): ModelAndView {
        val modelAndView = adminBaseControllerService.createAdminDefaultModelAndView(actionList)
        val welcomePromoConfigurationList = getConfigurationList<BasePromoConfiguration>()
        modelAndView.addObject(CONFIGURATION_LIST_ATTR_NAME, welcomePromoConfigurationList)
        setPreview(modelAndView, welcomePromoConfigurationList)
        return modelAndView
    }

    private fun <T : BasePromoConfiguration> setPreview(modelAndView: ModelAndView, basePromoConfigurationList: List<T>) {
        try {
            val json = ObjectMapper().writeValueAsString(basePromoConfigurationList)
            modelAndView.addObject(CONFIGURATION_LIST_PREVIEW_ATTR_NAME, json)
            modelAndView.addObject(CONFIGURATION_LIST_ATTR_NAME, basePromoConfigurationList)
        } catch (e: Exception) {
            throw NotFoundException()
        }
    }

    fun promoSort(startIndex: Int, stopIndex: Int): ModelAndView {
        var welcomePromoConfigurationList: MutableList<BasePromoConfiguration> = getConfigurationList()
        val basePromoConfiguration: BasePromoConfiguration
        try {
            basePromoConfiguration = welcomePromoConfigurationList.removeAt(startIndex)
        } catch (e: Exception) {
            throw NotFoundException(e.message)
        }

        val newWelcomePromoConfigurationList = ArrayList<BasePromoConfiguration>()
        newWelcomePromoConfigurationList.addAll(welcomePromoConfigurationList.subList(
                0,
                Math.min(stopIndex, welcomePromoConfigurationList.size)))
        newWelcomePromoConfigurationList.add(basePromoConfiguration)
        newWelcomePromoConfigurationList.addAll(welcomePromoConfigurationList.subList(
                Math.min(stopIndex, welcomePromoConfigurationList.size),
                welcomePromoConfigurationList.size))
        welcomePromoConfigurationList = newWelcomePromoConfigurationList
        configurationService.saveConfigAsJson(welcomePromoConfigurationList, configurationKey)
        return baseControllerService.createRedirectModelAndView(urlList)
    }

    companion object {
        const val CONFIGURATION_ATTR_NAME = "configuration"
        const val CONFIGURATION_LIST_ATTR_NAME = "configurationList"
        const val CONFIGURATION_LIST_PREVIEW_ATTR_NAME = "configurationListPreview"
    }

}

package ru.planeta.admin.controllers.promo

import org.springframework.stereotype.Controller
import org.springframework.validation.BindingResult
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.ModelAttribute
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.servlet.ModelAndView
import ru.planeta.admin.Actions
import ru.planeta.admin.Techobattle
import ru.planeta.admin.controllers.services.AdminBaseControllerService
import ru.planeta.api.exceptions.NotFoundException
import ru.planeta.api.service.promo.TechnobattleProjectService
import ru.planeta.model.promo.TechnobattleProject
import javax.validation.Valid

/**
 * Created with IntelliJ IDEA.
 * User: michail
 * Date: 20.01.2017
 * Time: 14:56
 */
@Controller
class TechnobattleAdminController(private val technobattleProjectService: TechnobattleProjectService,
                                  private val adminBaseControllerService: AdminBaseControllerService) {

    @GetMapping(Techobattle.PROJECT_LIST)
    fun getProjectList(@RequestParam(defaultValue = "0") offset: Int,
                       @RequestParam(defaultValue = "20") limit: Int): ModelAndView {
        val projects = technobattleProjectService.getProjects(offset, limit)
        return adminBaseControllerService.createAdminDefaultModelAndView(Actions.ADMIN_TECHNOBATTLE_PROJECT_LIST)
                .addObject("projects", projects)
                .addObject("offset", offset)
                .addObject("limit", limit)
    }

    @GetMapping(Techobattle.PROJECT)
    fun getProject(@ModelAttribute("project") project: TechnobattleProject?, errors: BindingResult,
                   @RequestParam projectId: Long): ModelAndView {
        var project = project
        if (!errors.hasErrors()) {
            project = if (projectId > 0) {
                technobattleProjectService.getProject(projectId)
            } else {
                TechnobattleProject()
            }
        }

        if (project == null) {
            throw NotFoundException(TechnobattleProject::class.java)
        }

        return adminBaseControllerService.createAdminDefaultModelAndView(Actions.ADMIN_TECHNOBATTLE_PROJECT)
                .addObject("project", project)
    }

    @PostMapping(Techobattle.PROJECT)
    fun saveProject(@Valid @ModelAttribute("project") project: TechnobattleProject, result: BindingResult): ModelAndView {
        if (result.hasErrors()) {
            return getProject(project, result, project.projectId)
        }
        technobattleProjectService.insertOrUpdate(project)
        return adminBaseControllerService.baseControllerService.createRedirectModelAndView(Techobattle.PROJECT_LIST)
    }
}


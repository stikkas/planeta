package ru.planeta.admin.controllers.loayalty

import org.apache.commons.lang3.StringUtils
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.ResponseBody
import org.springframework.web.servlet.ModelAndView
import ru.planeta.admin.Actions
import ru.planeta.admin.Loyalty
import ru.planeta.admin.controllers.services.AdminBaseControllerService
import ru.planeta.api.model.ActionStatus
import ru.planeta.api.search.SearchResult
import ru.planeta.api.service.admin.AdminService
import ru.planeta.api.service.profile.VipClubService
import ru.planeta.api.web.authentication.myProfileId
import ru.planeta.model.enums.ProfileType
import ru.planeta.model.profile.ProfileForAdminsWithEmail

/**
 * Created by eshevchenko on 16.09.14.
 */
@Controller
class VipUserController(private val vipClubService: VipClubService,
                        private val adminService: AdminService,
                        private val adminBaseControllerService: AdminBaseControllerService) {

    @GetMapping(Loyalty.ADMIN_LOYALTY_VIP_USERS)
    fun getVipUsers(@RequestParam(defaultValue = "0") offset: Int,
                    @RequestParam(defaultValue = "10") limit: Int,
                    @RequestParam(required = false) searchString: String?): ModelAndView {

        val users: SearchResult<ProfileForAdminsWithEmail> = if (StringUtils.isBlank(searchString))
            adminService.getVipUsers(myProfileId(), offset, limit)
        else
            adminService.getProfiles(myProfileId(), searchString, ProfileType.USER, offset, limit)

        return adminBaseControllerService.createAdminDefaultModelAndView(Actions.ADMIN_LOYALTY_VIP_USERS)
                .addObject("users", users.searchResultRecords)
                .addObject("count", users.estimatedCount)
                .addObject("searchString", searchString)
                .addObject("offset", offset)
                .addObject("limit", limit)
    }

    @PostMapping(Loyalty.ADMIN_LOYALTY_CHANGE_USER_VIP_STATUS)
    @ResponseBody
    fun changeUserVipStatus(@RequestParam userId: Long,
                            @RequestParam isVip: Boolean): ActionStatus<*> {
        vipClubService.changeUserVipStatus(myProfileId(), userId, isVip)
        return ActionStatus.createSuccessStatus<Any>()
    }
}

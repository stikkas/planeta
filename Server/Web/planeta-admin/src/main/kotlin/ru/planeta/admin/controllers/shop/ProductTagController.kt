package ru.planeta.admin.controllers.shop

import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.servlet.ModelAndView
import ru.planeta.admin.Actions
import ru.planeta.admin.Urls
import ru.planeta.admin.controllers.services.AdminBaseControllerService
import ru.planeta.api.service.shop.ProductTagService
import ru.planeta.api.web.controllers.services.BaseControllerService
import ru.planeta.model.shop.Category
import ru.planeta.model.shop.enums.ProductTagType
import java.util.*

/*
 * Created by Alexey on 07.09.2016.
 */

@Controller
class ProductTagController(private val adminBaseControllerService: AdminBaseControllerService,
                           private val baseControllerService: BaseControllerService,
                           private val productTagService: ProductTagService) {

    @RequestMapping(Urls.ADMIN_SHOP_PRODUCT_TAGS)
    fun shopProductTags(@RequestParam(defaultValue = "10") categoryId: Long,
                        @RequestParam(defaultValue = "10") limit: Int,
                        @RequestParam(defaultValue = "0") offset: Int): ModelAndView {

        val modelAndView = adminBaseControllerService.createAdminDefaultModelAndView(Actions.ADMIN_SHOP_PRODUCT_TAGS)
        val categories = productTagService.getProductTags(limit, offset)

        val count = productTagService.productTagsCount

        return modelAndView.addObject("categories", categories)
                .addObject("offset", offset)
                .addObject("limit", limit)
                .addObject("count", count)
    }

    @RequestMapping(value = Urls.ADMIN_SHOP_PRODUCT_TAG)
    fun shopProductTag(@RequestParam(defaultValue = "0") categoryId: Long): ModelAndView {

        if (categoryId > 0) {
            productTagService.getProductTagById(categoryId)
        }

        val category = if (categoryId == 0L) Category() else productTagService.getProductTagById(categoryId)
        val tagStatuses = EnumSet.allOf(ProductTagType::class.java)

        return adminBaseControllerService.createAdminDefaultModelAndView(Actions.ADMIN_SHOP_PRODUCT_TAG)
                .addObject("category", category)
                .addObject("tagStatuses", tagStatuses)
    }

    @RequestMapping(value = Urls.ADMIN_SHOP_SAVE_PRODUCT_TAG)
    fun saveShopProductTag(category: Category): ModelAndView {
        productTagService.saveProductTag(category)
        return baseControllerService.createRedirectModelAndView(Urls.ADMIN_SHOP_PRODUCT_TAGS)
    }
}

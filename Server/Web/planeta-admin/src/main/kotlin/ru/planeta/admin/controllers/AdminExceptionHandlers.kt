package ru.planeta.admin.controllers

import org.apache.log4j.Logger
import org.springframework.web.bind.annotation.ControllerAdvice
import ru.planeta.admin.Actions
import ru.planeta.api.web.authentication.AuthUtils
import ru.planeta.api.web.controllers.ExceptionHandlers
import java.io.EOFException
import java.io.IOException
import javax.servlet.ServletException
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

@ControllerAdvice(basePackages = ["ru.planeta.admin.controllers"])
class AdminExceptionHandlers : ExceptionHandlers() {
    private val log = Logger.getLogger(AdminExceptionHandlers::class.java)

    override fun handleException(request: HttpServletRequest, response: HttpServletResponse, statusCode: Int, message: String?, ex: Exception) {
        try {
            if (AuthUtils.isAjaxRequest(request)) {
                ajaxHandler(response, message, ex)
            } else {
                request.setAttribute("errorMessage", message)
                request.getRequestDispatcher("/WEB-INF/jsp/" + Actions.ERROR.text + ".jsp").forward(request, response)
            }
        } catch (e: EOFException) {
            log.warn(e)
        } catch (e: IOException) {
            log.error(e)
        } catch (e: ServletException) {
            log.error(e)
        }

    }
}

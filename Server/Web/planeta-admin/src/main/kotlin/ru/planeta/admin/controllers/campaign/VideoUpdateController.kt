package ru.planeta.admin.controllers.campaign

import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.servlet.ModelAndView
import ru.planeta.admin.Urls
import ru.planeta.api.service.admin.VideoUpdaterService
import ru.planeta.api.web.controllers.services.BaseControllerService

/**
 *
 * Created by a.savanovich on 08.12.2015.
 */
@Controller
class VideoUpdateController(private val videoUpdaterService: VideoUpdaterService,
                            private val baseControllerService: BaseControllerService) {

    @RequestMapping(value = Urls.RENEW_CAMPAIGN_VIDEOS, method = [RequestMethod.POST, RequestMethod.GET])
    fun updateVideos(@RequestParam campaignId: Long): ModelAndView {
        videoUpdaterService.updateCampaign(campaignId)
        return baseControllerService.createRedirectModelAndView(Urls.MODERATOR_CAMPAIGN_MODERATION_INFO + "?campaignId=" + campaignId)
    }
}

package ru.planeta.admin.controllers.promo

import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.servlet.ModelAndView
import ru.planeta.admin.Actions
import ru.planeta.admin.Urls
import ru.planeta.admin.controllers.services.AdminBaseControllerService
import ru.planeta.api.service.configurations.ConfigurationService
import ru.planeta.api.web.controllers.services.BaseControllerService


@Controller
class BroadcastPromoController(private val configurationService: ConfigurationService,
                               private val adminBaseControllerService: AdminBaseControllerService,
                               private val baseControllerService: BaseControllerService) {

    @GetMapping(Urls.ADMIN_BROADCAST_PROMO_LIST)
    fun promoListGet(): ModelAndView = adminBaseControllerService.createAdminDefaultModelAndView(Actions.ADMIN_BROADCAST_PROMO_LIST)
            .addObject("configuration", configurationService.promoProfilesIdsOfBroadcasts)

    @PostMapping(Urls.ADMIN_BROADCAST_PROMO_LIST)
    fun promoListPost(@RequestParam(value = "stringValue") idList: String): ModelAndView {
        configurationService.setPromoProfilesIdsOfBroadcasts(idList)
        return baseControllerService.createRedirectModelAndView(Urls.ADMIN_BROADCAST_PROMO_LIST)
    }

    @GetMapping(Urls.ADMIN_ABOUT_US_NEW_SETTINGS)
    fun aboutUsNewSettingsGet(): ModelAndView = adminBaseControllerService.createAdminDefaultModelAndView(Actions.ADMIN_ABOUT_US_NEW_SETTINGS)
            .addObject("aboutUsCampaignsIds", configurationService.promoAboutUsCampaignIds)

    @PostMapping(Urls.ADMIN_ABOUT_US_NEW_SETTINGS)
    fun aboutUsNewSettingsPost(@RequestParam aboutUsCampaignsIds: String): ModelAndView {
        configurationService.setPromoAboutUsCampaignIds(aboutUsCampaignsIds)
        return baseControllerService.createRedirectModelAndView(Urls.ADMIN_ABOUT_US_NEW_SETTINGS)
    }
}

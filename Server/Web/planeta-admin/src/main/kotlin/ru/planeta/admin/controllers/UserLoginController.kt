package ru.planeta.admin.controllers

import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.servlet.ModelAndView
import ru.planeta.admin.Urls
import ru.planeta.api.exceptions.MessageCode
import ru.planeta.api.exceptions.NotFoundException
import ru.planeta.api.exceptions.PermissionException
import ru.planeta.api.news.LoggerService
import ru.planeta.api.service.security.PermissionService
import ru.planeta.api.web.authentication.AuthUtils
import ru.planeta.api.web.authentication.getAuthentication
import ru.planeta.api.web.authentication.myProfileId
import ru.planeta.api.web.controllers.services.BaseControllerService
import ru.planeta.api.web.controllers.services.ControllerAutoLoginWrapService
import ru.planeta.eva.api.web.cas.handlers.CustomAuthenticationSuccessHandler
import ru.planeta.model.common.UserPrivateInfo
import ru.planeta.model.enums.ProjectType
import ru.planeta.model.enums.UserStatus
import ru.planeta.model.news.ProfileNews
import java.util.*
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

/**
 *
 * Created by asavan on 06.03.2017.
 */
@Controller
class UserLoginController(private val permissionService: PermissionService,
                          private val profileNewsService: LoggerService,
                          private val baseControllerService: BaseControllerService,
                          private val autoLoginWrapService: ControllerAutoLoginWrapService,
                          private val authenticationSuccessHandler: CustomAuthenticationSuccessHandler) {

    @GetMapping(Urls.ADMIN_LOGIN_AS_USER)
    fun loginAsUser(@RequestParam profileId: Long, request: HttpServletRequest, response: HttpServletResponse): ModelAndView {
        try {
            val clientId = myProfileId()

            // only users with administrative role has right to login as another users
            permissionService.checkAdministrativeRole(clientId)

            val userPrivateInfo = getUserPrivateInfoSafe(profileId)

            val userStatus = userPrivateInfo.userStatus

            // only SUPER_ADMIN can login under another SUPER ADMIN or ADMIN
            if ((userStatus.contains(UserStatus.SUPER_ADMIN) || userStatus.contains(UserStatus.ADMIN)) && !permissionService.isSuperAdmin(clientId)) {
                throw PermissionException()
            }

            // only ADMINS and SUPER_ADMINS has right to login as MANAGER
            if (userStatus.contains(UserStatus.MANAGER) && !permissionService.isPlanetaAdmin(clientId)) {
                throw PermissionException()
            }
            autoLoginWrapService.autoLoginUser(userPrivateInfo.username)
            authenticationSuccessHandler.onAuthenticationSuccess(request, response, getAuthentication())
            profileNewsService.addProfileNews(ProfileNews.Type.ADMIN_LOGIN_AS_USER, clientId, profileId)
            return baseControllerService.createRedirectModelAndView(baseControllerService.projectService.getUrl(ProjectType.MAIN))
        } catch (e: PermissionException) {
            val additionalParams = HashMap<String, Any>()
            additionalParams.put("errorMessage", baseControllerService.getErrorMessage(
                    MessageCode.PERMISSION_ILLEGAL_OPERATION.errorPropertyName, "") as Any)
            return baseControllerService.createRedirectModelAndView("""${Urls.ADMIN_USER_INFO}?profileId=$profileId""", additionalParams)
        }

    }

    private fun getUserPrivateInfoSafe(profileId: Long): UserPrivateInfo =
            baseControllerService.authorizationService.getUserPrivateInfoById(profileId) ?: throw NotFoundException(UserPrivateInfo::class.java, profileId)
}


package ru.planeta.admin.controllers.charity

import org.springframework.stereotype.Controller
import org.springframework.validation.BindingResult
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.servlet.ModelAndView
import ru.planeta.admin.Actions
import ru.planeta.admin.Charity
import ru.planeta.admin.controllers.services.AdminBaseControllerService
import ru.planeta.dao.charitydb.CharityNewsTagDAO
import ru.planeta.dao.profiledb.PostDAO
import ru.planeta.model.charity.enums.CharityEventType
import ru.planeta.model.profile.Post
import javax.validation.Valid

/*
 * Created by Alexey on 07.07.2016.
 */

@Controller
class AdminPostController(private val postDAO: PostDAO,
                          private val charityNewsTagDAO: CharityNewsTagDAO,
                          private val adminBaseControllerService: AdminBaseControllerService) {


    @GetMapping(Charity.ADMIN_CHARITY_POSTS)
    fun adminCharityPosts(@RequestParam(defaultValue = "0") postId: Long,
                          @RequestParam(defaultValue = "10") limit: Int,
                          @RequestParam(defaultValue = "0") offset: Int): ModelAndView {
        val posts = postDAO.selectCharityPostByIdOrAll(postId, limit, offset)

        val count = if (postId != 0L) 0 else postDAO.countCharityPosts()

        val newsTags = charityNewsTagDAO.selectAllCharityNewsTags(0, 0, false, false)
        return adminBaseControllerService.createAdminDefaultModelAndView(Actions.ADMIN_CHARITY_POSTS)
                .addObject("posts", posts)
                .addObject("offset", offset)
                .addObject("limit", limit)
                .addObject("count", count)
                .addObject("tagNameResolver", newsTags.map { it.tagId to it.nameRus }.toMap())
    }

    @GetMapping(value = Charity.ADMIN_POSTS)
    fun adminPosts(@RequestParam(defaultValue = "0") postId: Long,
                   @RequestParam(defaultValue = "10") limit: Int,
                   @RequestParam(defaultValue = "0") offset: Int): ModelAndView {
        val posts = postDAO.selectPosts(postId, offset, limit)

        val count = if (postId != 0L) 0 else postDAO.selectPostsCount()

        return adminBaseControllerService.createAdminDefaultModelAndView(Actions.ADMIN_POSTS)
                .addObject("posts", posts)
                .addObject("count", count)
                .addObject("offset", offset)
                .addObject("limit", limit)
    }

    @GetMapping(Charity.ADMIN_POST)
    fun adminPost(@RequestParam(defaultValue = "0") postId: Long): ModelAndView {
        val post = if (postId == 0L) Post() else postDAO.select(postId)

        val newsTags = charityNewsTagDAO.selectAllCharityNewsTags(0, 0, false, false)
        return adminBaseControllerService.createAdminDefaultModelAndView(Actions.ADMIN_POST)
                .addObject("post", post)
                .addObject("charityEventTypes", CharityEventType.values())
                .addObject("charityNewsTags", newsTags)
    }

    @PostMapping(Charity.ADMIN_POST)
    fun savePost(@Valid post: Post, result: BindingResult): ModelAndView {
        postDAO.updateCharityPost(post)
        return adminBaseControllerService.baseControllerService.createRedirectModelAndView(Charity.ADMIN_CHARITY_POSTS)
    }

    @GetMapping(Charity.ADMIN_CHARITY_DELETE_POST)
    fun deleteCharityPost(@RequestParam postId: Long): ModelAndView {
        postDAO.deleteCharityPost(postId)
        return adminBaseControllerService.baseControllerService.createRedirectModelAndView(Charity.ADMIN_CHARITY_POSTS)
    }
}


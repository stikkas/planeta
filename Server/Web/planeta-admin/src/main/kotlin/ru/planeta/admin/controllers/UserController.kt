package ru.planeta.admin.controllers

import org.apache.log4j.Logger
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Controller
import org.springframework.validation.BindingResult
import org.springframework.validation.DataBinder
import org.springframework.web.bind.annotation.*
import org.springframework.web.servlet.ModelAndView
import ru.planeta.admin.Actions
import ru.planeta.admin.Urls
import ru.planeta.admin.controllers.services.AdminBaseControllerService
import ru.planeta.admin.models.ChangeAlias
import ru.planeta.admin.validation.ChangeAliasValidator
import ru.planeta.api.exceptions.AlreadyExistException
import ru.planeta.api.exceptions.NotFoundException
import ru.planeta.api.model.ActionStatus
import ru.planeta.api.news.LoggerService
import ru.planeta.api.service.admin.AdminService
import ru.planeta.api.service.profile.AuthorizationService
import ru.planeta.api.service.profile.ProfileSettingsService
import ru.planeta.api.utils.ValidateUtils
import ru.planeta.api.web.authentication.myProfileId
import ru.planeta.commons.web.IpUtils
import ru.planeta.commons.web.WebUtils
import ru.planeta.model.common.UserPrivateInfo
import ru.planeta.model.enums.ProfileStatus
import ru.planeta.model.enums.UserStatus
import ru.planeta.model.news.ProfileNews
import java.math.BigDecimal
import java.util.*
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse
import javax.validation.Valid

/**
 * Date: 03.10.12
 * Time: 13:23
 */
@Controller
class UserController(private val profileSettingsService: ProfileSettingsService,
                     private val authorizationServiceMy: AuthorizationService,
                     private val profileNewsService: LoggerService,
                     private val adminService: AdminService,
                     private val adminBaseControllerService: AdminBaseControllerService) {

    private val logger = Logger.getLogger(UserController::class.java)
    @GetMapping(Urls.ADMIN_USERS)
    fun getUsers(@RequestParam(required = false) searchString: String?,
                 @RequestParam(defaultValue = "0") offset: Int,
                 @RequestParam(defaultValue = "10") limit: Int): ModelAndView {

        val profiles = adminService.getProfiles(myProfileId(), searchString, null, offset, limit)

        return adminBaseControllerService.createAdminDefaultModelAndView(Actions.ADMIN_USERS)
                .addObject("profiles", profiles.searchResultRecords)
                .addObject("count", profiles.estimatedCount)
                .addObject("searchString", searchString)
                .addObject("offset", offset)
                .addObject("limit", limit)
    }

    @GetMapping(Urls.ADMIN_USER_INFO)
    fun getUserInfo(@RequestParam(defaultValue = "0") profileId: Long,
                    @RequestParam(required = false) successMessage: String?,
                    @RequestParam(required = false) errorMessage: String?): ModelAndView {

        adminBaseControllerService.permissionService.checkAdministrativeRole(myProfileId())
        val profile = adminBaseControllerService.baseControllerService.profileService.getProfile(profileId)

        val privateInfo = getUserPrivateInfoSafe(profileId)
        val modelAndView = adminBaseControllerService.createAdminDefaultModelAndView(Actions.ADMIN_USER_INFO)
        val personalCabinet = adminBaseControllerService.baseControllerService.userService.getPersonalCabinet(profileId)
        return modelAndView.addObject("profile", profile)
                .addObject("userPrivateInfo", privateInfo)
                .addObject("balance", personalCabinet.balance)
                .addObject("frozenAmount", personalCabinet.frozenAmount)
                .addObject("email", privateInfo.email)
                .addObject("regCode", privateInfo.regCode)
                .addObject("successMessage", successMessage)
                .addObject("errorMessage", errorMessage)
    }

    private fun getUserPrivateInfoSafe(profileId: Long): UserPrivateInfo = authorizationServiceMy.getUserPrivateInfoById(profileId)
            ?: throw NotFoundException(UserPrivateInfo::class.java, profileId)

    @PostMapping(Urls.ADMIN_SET_USER_ROLE)
    fun setUserRole(@RequestParam profileId: Long,
                    @RequestParam(value = "roleNames") rolesSet: EnumSet<UserStatus>,
                    req: HttpServletRequest): ModelAndView {
        try {
            val myProfileId = myProfileId()
            adminService.setUserStatus(myProfileId, profileId, rolesSet)
            logger.info("Admin id=" + myProfileId + " changed profile id=" + profileId + " satus ip=" + IpUtils.getRemoteAddr(req))
            profileNewsService.addProfileNews(ProfileNews.Type.ADMIN_CHANGE_USER_STATUS, myProfileId, profileId)

        } catch (e: Exception) {
            logger.error("Cannot set roles for profile: " + profileId, e)
        }

        val errorUrl = WebUtils.createUrl(Urls.ADMIN_USER_INFO,
                WebUtils.Parameters()
                        .add("profileId", profileId)
                        .add("successMessage", adminBaseControllerService
                                .baseControllerService.messageSource.getMessage("user.is.assigned.to.new.roles", null, Locale.getDefault())).params)

        return adminBaseControllerService.baseControllerService.createRedirectModelAndView(errorUrl)
    }

    @PostMapping(Urls.ADMIN_CHANGE_USER_PASSWORD)
    fun changeUserPassword(@RequestParam profileId: Long, @RequestParam password: String, req: HttpServletRequest): ModelAndView {
        val myProfileId = myProfileId()
        adminService.setUserPassword(myProfileId, profileId, password)
        logger.info("Admin id=" + myProfileId + " changed user id=" + profileId + " password ip=" + IpUtils.getRemoteAddr(req))
        profileNewsService.addProfileNews(ProfileNews.Type.ADMIN_CHANGE_USER_PASSWORD, myProfileId, profileId)
        val additionalParams = HashMap<String, Any>()
        additionalParams.put("successMessage", adminBaseControllerService.baseControllerService
                .messageSource.getMessage("user.password.successfully.updated", null, Locale.getDefault()))
        return adminBaseControllerService.baseControllerService.createRedirectModelAndView(Urls.ADMIN_USER_INFO + "?profileId=" + profileId, additionalParams)
    }

    @PostMapping(Urls.ADMIN_CONFIRM_USER_REGISTRATION)
    fun confirmUserRegistration(@RequestParam profileId: Long): ModelAndView {

        try {
            adminService.confirmRegistration(myProfileId(), profileId)
        } catch (e: Exception) {
            logger.debug("User not confirmed " + e.message)
        }

        val additionalParams = HashMap<String, Any>()
        additionalParams.put("successMessage", adminBaseControllerService.baseControllerService
                .messageSource.getMessage("registration.is.successfully.confirmed", null, Locale.getDefault()))
        return adminBaseControllerService.baseControllerService.createRedirectModelAndView("${Urls.ADMIN_USER_INFO}?profileId=$profileId", additionalParams)
    }

    /**
     * Sets specified status for group
     */
    @RequestMapping(Urls.ADMIN_USER_SET_STATUS)
    fun setStatus(@RequestParam profileId: Long,
                  @RequestParam("status") profileStatus: ProfileStatus,
                  req: HttpServletRequest): ModelAndView {
        val myProfileId = myProfileId()
        adminService.setProfileStatus(myProfileId, profileId, profileStatus)
        val ip = IpUtils.getRemoteAddr(req)
        val additionalParams = HashMap<String, Any>()
        log.info("Admin id=$myProfileId changed user id=$profileId satus ip=$ip")
        additionalParams.put("successMessage", adminBaseControllerService.baseControllerService
                .messageSource.getMessage("user.is.assigned.to.new.roles", null, Locale.getDefault()))
        additionalParams.put("profileId", profileId)
        return adminBaseControllerService.baseControllerService.createRedirectModelAndView(Urls.ADMIN_USER_INFO, additionalParams)
    }


    @PostMapping(Urls.ADMIN_CHANGE_USER_EMAIL)
    fun changeUserEmail(@RequestParam profileId: Long, @RequestParam email: String, req: HttpServletRequest): ModelAndView {
        val additionalParams = HashMap<String, Any>()
        val myProfileId = myProfileId()
        adminService.setUserEmail(myProfileId, profileId, email)
        profileNewsService.addProfileNews(ProfileNews.Type.ADMIN_CHANGE_USER_EMAIL, myProfileId, profileId)
        logger.info("Admin id=" + myProfileId + " changed user id=" + profileId + " email ip=" + IpUtils.getRemoteAddr(req))
        additionalParams["successMessage"] = adminBaseControllerService.baseControllerService
                .messageSource.getMessage("user.email.successfully.updated", null, Locale.getDefault())
        return adminBaseControllerService.baseControllerService.createRedirectModelAndView(Urls.ADMIN_USER_INFO + "?profileId=" + profileId, additionalParams)
    }

    @Autowired
    lateinit var changeAliasValidator: ChangeAliasValidator

    @PostMapping(Urls.ADMIN_CHANGE_ALIAS_HTML)
    fun setAlias(@RequestParam profileId: Long, @RequestParam(required = false) aliasName: String?, req: HttpServletRequest): ModelAndView {
        val dataBinder = DataBinder(ChangeAlias(profileId, aliasName))
        dataBinder.validator = changeAliasValidator
        dataBinder.validate()
        val errors = dataBinder.bindingResult
        val additionalParams = HashMap<String, Any>()
        if (errors.hasErrors()) {
            additionalParams["errorMessage"] = errors.allErrors.joinToString(", ") {
                adminBaseControllerService.baseControllerService.messageSource
                        .getMessage(it.code, null, Locale.getDefault())
            }
        } else {

            try {
                adminService.changeAlias(profileId, aliasName)
                logger.info("Admin id=${myProfileId()} changed profile alias id=$profileId request ip=${IpUtils.getRemoteAddr(req)}")
                additionalParams["successMessage"] = adminBaseControllerService.baseControllerService
                        .messageSource.getMessage("user.alias.successfully.updated", null, Locale.getDefault())
            } catch (e: AlreadyExistException) {
                additionalParams["errorMessage"] = adminBaseControllerService.baseControllerService.messageSource
                        .getMessage("field.error.exists", null, Locale.getDefault())
            }
        }
        return adminBaseControllerService.baseControllerService.createRedirectModelAndView("${Urls.ADMIN_USER_INFO}?profileId=$profileId",
                additionalParams)
    }

    @PostMapping(Urls.ADMIN_UNSUBSCRIBE_USER_FROM_ALL_EMAIL_NOTIFICATIONS)
    fun unSubscribeUserFromAllMailNotifications(@RequestParam profileId: Long, req: HttpServletRequest): ModelAndView {
        val additionalParams = HashMap<String, Any>()
        val myProfileId = myProfileId()
        logger.info("Admin id=" + myProfileId + " changed user id=" + profileId + " email ip=" + IpUtils.getRemoteAddr(req))
        profileSettingsService.unSubscribeUserFromAllMailNotifications(myProfileId, profileId)
        additionalParams.put("successMessage", "Пользователь успешно отписан от уведомлений на почтовый адрес")
        return adminBaseControllerService
                .baseControllerService.createRedirectModelAndView("${Urls.ADMIN_USER_INFO}?profileId=$profileId", additionalParams)
    }

    @PostMapping(Urls.ADMIN_SEND_REGISTRATION_COMPLETE_EMAIL)
    fun sendRegistrationCompleteEmail(@RequestParam profileId: Long, request: HttpServletRequest, response: HttpServletResponse): ModelAndView {

        adminService.sendRegistrationCompleteEmail(myProfileId(), profileId)
        val additionalParams = HashMap<String, Any>()
        additionalParams.put("successMessage", adminBaseControllerService
                .baseControllerService.messageSource.getMessage("letter.to.confirm.registration.sent", null, Locale.getDefault()))
        return adminBaseControllerService.baseControllerService.createRedirectModelAndView(Urls.ADMIN_USER_INFO + "?profileId=" + profileId, additionalParams)
    }

    @PostMapping(Urls.ADMIN_DECREASE_USER_BALANCE)
    fun decreaseBalance(@RequestParam profileId: Long, @RequestParam amount: Int): ModelAndView {
        val clientId = myProfileId()
        adminService.decreaseBalance(clientId, profileId, BigDecimal(amount))
        profileNewsService.addProfileNews(ProfileNews.Type.ADMIN_DECREASE_USER_BALANCE, clientId, profileId)
        logger.info("SuperAdmin id=$clientId reduced user id=$profileId balance by the amount=$amount")
        val additionalParams = HashMap<String, Any>()
        additionalParams.put("successMessage", adminBaseControllerService
                .baseControllerService.messageSource.getMessage("decrease.user.balance", null, Locale.getDefault()))
        return adminBaseControllerService.baseControllerService.createRedirectModelAndView(Urls.ADMIN_USER_INFO + "?profileId=" + profileId, additionalParams)
    }

    @PostMapping(Urls.ADMIN_FREEZE_AMOUNT)
    fun freezeAmount(@RequestParam profileId: Long, @RequestParam amount: Int): ModelAndView {
        val clientId = myProfileId()
        adminService.freezeAmount(clientId, profileId, BigDecimal(amount))
        profileNewsService.addProfileNews(ProfileNews.Type.ADMIN_FREEZE_USER_AMOUNT, clientId, profileId)
        logger.info("SuperAdmin id=$clientId froze amount $amount to user id=$profileId")
        val additionalParams = HashMap<String, Any>()
        additionalParams.put("successMessage", adminBaseControllerService.baseControllerService
                .messageSource.getMessage("freeze.amount", null, Locale.getDefault()) + " " + amount)
        return adminBaseControllerService.baseControllerService.createRedirectModelAndView(Urls.ADMIN_USER_INFO + "?profileId=" + profileId, additionalParams)
    }

    @PostMapping(Urls.ADMIN_UNFREEZE_AMOUNT)
    fun unfreezeAmount(@RequestParam profileId: Long,
                       @RequestParam amount: Int): ModelAndView {
        val clientId = myProfileId()
        adminService.unfreezeAmount(clientId, profileId, BigDecimal(amount))
        profileNewsService.addProfileNews(ProfileNews.Type.ADMIN_UNFREEZE_USER_AMOUNT, clientId, profileId)
        logger.info("SuperAdmin id=$clientId unfroze amount $amount to user id=$profileId")
        val additionalParams = HashMap<String, Any>()
        additionalParams.put("successMessage", adminBaseControllerService
                .baseControllerService.messageSource.getMessage("returned.to.the.balance", null, Locale.getDefault()) + " " + amount)
        return adminBaseControllerService.baseControllerService.createRedirectModelAndView(Urls.ADMIN_USER_INFO + "?profileId=" + profileId, additionalParams)
    }

    @PostMapping(Urls.ADMIN_DECREASE_FROZEN_AMOUNT)
    fun decreaseFrozenAmount(@RequestParam profileId: Long, @RequestParam amount: Int): ModelAndView {
        val clientId = myProfileId()
        adminService.decreaseFrozenAmount(clientId, profileId, BigDecimal(amount))
        profileNewsService.addProfileNews(ProfileNews.Type.ADMIN_DECREASE_USER_FROZEN_AMOUNT, clientId, profileId)
        logger.info("SuperAdmin id=$clientId decrease frozen amount by $amount to user id=$profileId")
        val additionalParams = HashMap<String, Any>()
        additionalParams.put("successMessage", adminBaseControllerService
                .baseControllerService.messageSource.getMessage("debit.from.freezen.amount", null, Locale.getDefault()) + " " + amount)
        return adminBaseControllerService.baseControllerService
                .createRedirectModelAndView(Urls.ADMIN_USER_INFO + "?profileId=" + profileId, additionalParams)
    }

    @PostMapping(Urls.ADMIN_DELETE_USER)
    fun deleteUser(@RequestParam profileId: Long): ModelAndView {

        val clientId = myProfileId()
        val additionalParams = HashMap<String, Any>()

        return try {
            adminService.deleteProfile(clientId, profileId)
            logger.info("Admin id=$clientId has deleted user id=$profileId")
            additionalParams.put("successMessage", adminBaseControllerService
                    .baseControllerService.messageSource.getMessage("user.is.deleted", null, Locale.getDefault()))
            adminBaseControllerService.baseControllerService.createRedirectModelAndView(Urls.ADMIN_USERS, additionalParams)
        } catch (ex: Exception) {
            logger.info(ex.message)
            additionalParams.put("errorMessage", adminBaseControllerService.baseControllerService
                    .messageSource.getMessage("failed.to.deleteByProfileId.user", null, Locale.getDefault()))
            adminBaseControllerService.baseControllerService.createRedirectModelAndView(Urls.ADMIN_USER_INFO + "?profileId=" + profileId, additionalParams)
        }

    }

    @PostMapping(Urls.ADMIN_DELETE_USER_POSTS)
    fun deleteUserPosts(@RequestParam profileId: Long, @RequestParam daysCount: Int): ModelAndView {
        val clientId = myProfileId()
        adminService.deleteUserPosts(clientId, profileId, daysCount)
        logger.info("SuperAdmin id=$clientId has deleted user's (id=$profileId) posts")
        val additionalParams = HashMap<String, Any>()
        additionalParams.put("successMessage", adminBaseControllerService
                .baseControllerService.messageSource.getMessage("successful.deleting.user.posts", null, Locale.getDefault()))
        return adminBaseControllerService.baseControllerService.createRedirectModelAndView(Urls.ADMIN_USER_INFO + "?profileId=" + profileId, additionalParams)
    }

    @PostMapping(Urls.ADMIN_DELETE_USER_COMMENTS)
    fun deleteUserComments(@RequestParam profileId: Long, @RequestParam daysCount: Int): ModelAndView {
        val clientId = myProfileId()
        adminService.deleteUserComments(clientId, profileId, daysCount)
        logger.info("SuperAdmin id=$clientId has deleted user's (id=$profileId) comments")
        val additionalParams = HashMap<String, Any>()
        additionalParams.put("successMessage", adminBaseControllerService
                .baseControllerService.messageSource.getMessage("successful.deleting.user.comments", null, Locale.getDefault()))
        return adminBaseControllerService
                .baseControllerService.createRedirectModelAndView(Urls.ADMIN_USER_INFO + "?profileId=" + profileId, additionalParams)
    }

    companion object {

        private val log = Logger.getLogger(UserController::class.java)
    }

}


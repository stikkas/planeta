package ru.planeta.admin.validation

import org.springframework.stereotype.Component
import org.springframework.validation.Errors
import org.springframework.validation.ValidationUtils
import org.springframework.validation.Validator
import ru.planeta.model.profile.broadcast.BroadcastPrivateTargeting

/**
 * Date: 14.04.13
 */
@Component
class BroadcastPrivateTargetingValidator : Validator {
    override fun supports(aClass: Class<*>): Boolean {
        return aClass.isAssignableFrom(BroadcastPrivateTargeting::class.java)
    }

    override fun validate(o: Any, errors: Errors) {
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "email", "field.required")
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "validInHours", "field.required")
    }
}

package ru.planeta.admin.controllers

import org.apache.commons.lang3.time.DateUtils
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.servlet.ModelAndView
import ru.planeta.admin.Actions
import ru.planeta.admin.Urls
import ru.planeta.admin.utils.DateUtils.getDateTo
import ru.planeta.admin.utils.DateUtils.getInterval
import ru.planeta.admin.utils.DateUtils.setIntervalTodayAsDate
import ru.planeta.api.model.stat.TotalPurchaseReport
import ru.planeta.api.service.security.PermissionService
import ru.planeta.api.service.statistic.AdminStatisticService
import ru.planeta.api.web.authentication.myProfileId
import ru.planeta.api.web.controllers.IAction
import ru.planeta.api.web.controllers.services.BaseControllerService
import ru.planeta.api.web.utils.ExcelReportUtils
import ru.planeta.model.enums.ProjectType
import ru.planeta.model.enums.RotationPeriod
import ru.planeta.model.stat.StatPurchaseCommon
import ru.planeta.model.stat.StatPurchaseProduct
import ru.planeta.model.stat.StatPurchaseShare
import ru.planeta.reports.ReportType
import java.math.BigDecimal
import java.util.*
import javax.servlet.http.HttpServletResponse

/**
 * @author is.kuzminov
 * Date: 06.08.12
 * Time: 14:52
 */
@Controller
class StatisticController(private val permissionService: PermissionService,
                          private val adminStatisticService: AdminStatisticService,
                          private val baseControllerService: BaseControllerService) {

    @RequestMapping(Urls.ADMIN_STATISTIC_FORECAST)
    fun statisticForecast(): ModelAndView {
        val modelAndView = createModelAndView(Actions.ADMIN_STATISTIC_FORECAST)
        modelAndView.addObject("forecast", adminStatisticService.getCampaignsForecast(myProfileId()))
        return modelAndView
    }

    @RequestMapping(Urls.ADMIN_STATISTIC_COMPOSITE)
    fun getStatistic(@RequestParam(required = false) dateTo: Date?,
                     @RequestParam(required = false) dateFrom: Date?,
                     @RequestParam(required = false) step: String?): ModelAndView {
        var step = step

        val modelAndView = createModelAndView(Actions.ADMIN_STATISTIC_COMPOSITE)
        //
        val interval = setIntervalTodayAsDate(dateFrom, dateTo, modelAndView)
        if (step == null) {
            step = "day" // pgsql trunc arg
        }

        val purchaseReports = adminStatisticService.getStatPurchaseCompositeReport(myProfileId(),
                interval.dateFrom, DateUtils.addDays(interval.dateTo, 1), step)
        val totalReport = TotalPurchaseReport(LinkedList(purchaseReports.values))
        return modelAndView.addObject("dateFrom", interval.dateFrom)
                .addObject("dateTo", interval.dateTo)
                .addObject("step", step)
                .addObject("purchaseReports", purchaseReports)
                .addObject("totalReport", totalReport)
    }

    @GetMapping(Urls.ADMIN_STATISTIC_COMPOSITE_REPORT)
    fun getStatisticCompositeReport(@RequestParam dateFrom: Date,
                                    @RequestParam dateTo: Date,
                                    @RequestParam step: String,
                                    response: HttpServletResponse) {
        val interval = getInterval(dateFrom, dateTo)
        val purchaseReport = adminStatisticService.getStatPurchaseCompositeReport(myProfileId(), interval.dateFrom,
                DateUtils.addDays(interval.dateTo, 1), step)
        ExcelReportUtils.setReportHeaders(response)
        var totalAmount = BigDecimal.ZERO
        for (pr in purchaseReport.values) {
            totalAmount = totalAmount.add(pr.sharesAmount)
            totalAmount = totalAmount.add(pr.ticketsAmount)
            totalAmount = totalAmount.add(pr.productsAmount)
            totalAmount = totalAmount.add(pr.biblioPurchasesAmount)
        }
        ExcelReportUtils.generateSalesReport(purchaseReport, totalAmount, response.writer)
    }

    @GetMapping(Urls.ADMIN_STATISTIC_SHARE_PURCHASE_REPORT)
    fun getStatisticSharePurchaseReport(@RequestParam dateFrom: Date,
                                        @RequestParam dateTo: Date,
                                        @RequestParam(defaultValue = "EXCEL") reportType: ReportType,
                                        response: HttpServletResponse) {

        val report = reportType.createReport("share_statistic_report_" + myProfileId())

        val interval = getInterval(dateFrom, dateTo)
        val purchaseReport = adminStatisticService.getStatPurchaseShareReport(myProfileId(), interval.dateFrom, DateUtils.addDays(interval.dateTo, 1))

        report.addCaptionRow(*baseControllerService.messageSource.getMessage("report.shares.statistic", null,
                Locale.getDefault()).split(";".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray())
        for (statPurchaseShare in purchaseReport) {
            report.addRow(
                    statPurchaseShare.campaignId,
                    statPurchaseShare.campaignName,
                    statPurchaseShare.groupProfile.displayName,
                    statPurchaseShare.name,
                    statPurchaseShare.price,
                    statPurchaseShare.count,
                    statPurchaseShare.amount
            )
        }
        report.addToResponse(response)
    }

    @GetMapping(Urls.ADMIN_STATISTIC_PRODUCT_PURCHASE_REPORT)
    fun getStatisticProductPurchaseReport(@RequestParam dateFrom: Date,
                                          @RequestParam dateTo: Date,
                                          response: HttpServletResponse) {
        val interval = getInterval(dateFrom, dateTo)
        val purchaseReport = adminStatisticService.getStatPurchaseProductReport(myProfileId(), interval.dateFrom,
                DateUtils.addDays(interval.dateTo, 1))
        respondWithReport(purchaseReport, response, ExcelReportUtils.ReportType.SALES_PRODUCTS)
    }

    @GetMapping(Urls.ADMIN_STATISTIC_REGISTRATION_REPORT)
    fun getStatisticRegistrationReport(@RequestParam dateFrom: Date,
                                       @RequestParam dateTo: Date,
                                       response: HttpServletResponse) {
        val interval = getInterval(dateFrom, dateTo)
        val purchaseReport = adminStatisticService.getRegistrationsReport(myProfileId(), interval.dateFrom,
                DateUtils.addDays(interval.dateTo, 1))
        ExcelReportUtils.setReportHeaders(response)
        ExcelReportUtils.generateStatisticReport(purchaseReport, null, null, response.writer, ExcelReportUtils.ReportType.REGISTRATIONS)
    }

    private fun respondWithReport(report: List<StatPurchaseProduct>, response: HttpServletResponse, reportType: ExcelReportUtils.ReportType) {
        ExcelReportUtils.setReportHeaders(response)
        ExcelReportUtils.generateStatisticReport(report, getTotalAmount<StatPurchaseCommon>(report), getTotalCount<StatPurchaseCommon>(report), response.writer, reportType)
    }

    private fun createModelAndView(action: IAction): ModelAndView = baseControllerService.createDefaultModelAndView(action, ProjectType.ADMIN)

    @RequestMapping(Urls.ADMIN_STATISTIC_REGISTRATION_MODAL)
    fun getStatisticRegistrationModal(@RequestParam dateFrom: Date, @RequestParam step: RotationPeriod): ModelAndView {
        val dateTo = adminStatisticService.getDateTo(dateFrom, step)
        val users = adminStatisticService.getRegistrationsReport(myProfileId(), dateFrom, dateTo)

        return createModelAndView(Actions.ADMIN_STATISTIC_REGISTRATION_MODAL).addObject("dateFrom", dateFrom)
                .addObject("dateTo", getDateTo(dateFrom, step))
                .addObject("users", users)
    }

    @RequestMapping(Urls.ADMIN_STATISTIC_SHARE_PURCHASE)
    fun getStatisticShare(@RequestParam(required = false) dateFrom: Date?,
                          @RequestParam(required = false) dateTo: Date?): ModelAndView {

        val modelAndView = createModelAndView(Actions.ADMIN_STATISTIC_SHARE_PURCHASE)
        //
        val datepickerInterval = setIntervalTodayAsDate(dateFrom, dateTo, modelAndView)
        val apiInterval = ru.planeta.admin.utils.DateUtils.Interval(datepickerInterval.dateFrom, DateUtils.addDays(datepickerInterval.dateTo, 1))
        //
        val purchaseReport = adminStatisticService.getStatPurchaseShareReportWithoutDiff(myProfileId(),
                apiInterval.dateFrom, apiInterval.dateTo)
        //
        val previousDateRange = adminStatisticService.getPreviousRange(apiInterval.dateFrom, apiInterval.dateTo)
        //
        val previousPurchaseReport = adminStatisticService.getStatPurchaseShareReportWithoutDiff(myProfileId(),
                previousDateRange.from, previousDateRange.to)
        adminStatisticService.setDiff(purchaseReport, previousPurchaseReport)
        //
        modelAndView.addObject("purchaseReport", purchaseReport)
        val activeCampaignsCount = getActiveCampaignsCount(purchaseReport, apiInterval.dateFrom, apiInterval.dateTo)
        val previousActiveCampaignsCount = getActiveCampaignsCount(previousPurchaseReport, previousDateRange.from, previousDateRange.to)
        modelAndView.addObject("activeCampaignsCount", activeCampaignsCount)
                .addObject("activeCampaignsCountDiff", activeCampaignsCount - previousActiveCampaignsCount)
        setTotalCount(modelAndView, purchaseReport)
        setTotalAmount(modelAndView, purchaseReport)
        //
        setTotalAmount(modelAndView, purchaseReport)
        return modelAndView
    }

    @RequestMapping(Urls.ADMIN_STATISTIC_SHARE_PURCHASE_MODAL)
    fun getStatShare(@RequestParam dateFrom: Date, @RequestParam step: RotationPeriod): ModelAndView {
        val modelAndView = createModelAndView(Actions.ADMIN_STATISTIC_SHARE_PURCHASE_MODAL)
        //
        val dateTo = adminStatisticService.getDateTo(dateFrom, step)
        val purchaseReport = adminStatisticService.getStatPurchaseShareReport(myProfileId(), dateFrom, dateTo)
        //
        modelAndView.addObject("dateFrom", dateFrom)
                .addObject("dateTo", getDateTo(dateFrom, step))
                .addObject("purchaseReport", purchaseReport)
        //
        setTotalAmount(modelAndView, purchaseReport)
        setTotalCount(modelAndView, purchaseReport)
        return modelAndView
    }

    @RequestMapping(Urls.ADMIN_STATISTIC_PRODUCT_PURCHASE)
    fun getStatisticProduct(@RequestParam(required = false) dateFrom: Date?,
                            @RequestParam(required = false) dateTo: Date?): ModelAndView {

        val clientId = myProfileId()
        if (!permissionService.hasAdministrativeRole(clientId)) {
            return baseControllerService.createRedirectModelAndView(Urls.ADMIN_INDEX)
        }

        val modelAndView = createModelAndView(Actions.ADMIN_STATISTIC_PRODUCT_PURCHASE)
        //
        val datepickerInterval = setIntervalTodayAsDate(dateFrom, dateTo, modelAndView)
        val apiInterval = ru.planeta.admin.utils.DateUtils.Interval(datepickerInterval.dateFrom, DateUtils.addDays(datepickerInterval.dateTo, 1))
        //
        val purchaseReport = adminStatisticService.getStatPurchaseProductReport(clientId,
                apiInterval.dateFrom, apiInterval.dateTo)
        modelAndView.addObject("purchaseReport", purchaseReport)

        //
        setTotalAmount(modelAndView, purchaseReport)
        setTotalCount(modelAndView, purchaseReport)
        return modelAndView
    }

    @RequestMapping(Urls.ADMIN_STATISTIC_PRODUCT_PURCHASE_MODAL)
    fun getStatProduct(@RequestParam dateFrom: Date,
                       @RequestParam step: RotationPeriod): ModelAndView {
        val modelAndView = createModelAndView(Actions.ADMIN_STATISTIC_PRODUCT_PURCHASE_MODAL)
        //
        val dateTo = adminStatisticService.getDateTo(dateFrom, step)
        val purchaseReport = adminStatisticService.getStatPurchaseProductReport(myProfileId(), dateFrom, dateTo)
        //
        modelAndView.addObject("dateFrom", dateFrom)
                .addObject("dateTo", getDateTo(dateFrom, step))
                .addObject("purchaseReport", purchaseReport)
        //
        setTotalAmount(modelAndView, purchaseReport)
        setTotalCount(modelAndView, purchaseReport)
        return modelAndView
    }

    private fun <T : StatPurchaseCommon> setTotalAmount(modelAndView: ModelAndView, purchaseReport: List<T>) =
            modelAndView.addObject("totalAmount", getTotalAmount(purchaseReport))

    private fun <T : StatPurchaseCommon> setTotalCount(modelAndView: ModelAndView, purchaseReport: List<T>) =
            modelAndView.addObject("totalCount", getTotalCount(purchaseReport))

    private fun <T : StatPurchaseCommon> getTotalAmount(purchaseReport: List<T>): BigDecimal {
        var totalAmount = BigDecimal.ZERO
        for (statPurchaseCommon in purchaseReport) {
            totalAmount = totalAmount.add(statPurchaseCommon.amount)
        }
        return totalAmount
    }

    private fun <T : StatPurchaseCommon> getTotalCount(purchaseReport: List<T>): Int = purchaseReport.sumBy { it.count }

    private fun getActiveCampaignsCount(purchaseReport: List<StatPurchaseShare>, dateFrom: Date, dateTo: Date): Int {
        val activeCampaignIdSet = purchaseReport
                .filter {
                    it != null && (it.campaignTimeStart != null &&
                            it.campaignTimeStart.time >= dateFrom.time ||
                            it.campaignTimeFinish != null && it.campaignTimeFinish.time >= dateTo.time)
                }
                .map { it.campaignId }
                .toSet()
        return activeCampaignIdSet.size
    }

}



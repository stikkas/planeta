package ru.planeta.admin.controllers.services

import com.fasterxml.jackson.databind.ObjectMapper
import org.springframework.web.servlet.ModelAndView
import ru.planeta.admin.Actions
import ru.planeta.api.exceptions.NotFoundException
import ru.planeta.api.service.configurations.ConfigurationService
import ru.planeta.model.common.Identified
import java.util.*

abstract class BiblioConfigControllerService<in T : Identified>(private val classItem: Class<T>,
                                                                private val adminBaseControllerService: AdminBaseControllerService,
                                                                private val configurationService: ConfigurationService) {
    protected abstract val listAction: Actions
    protected abstract val fillAction: Actions
    protected abstract val editAction: Actions
    protected abstract val configName: String
    protected abstract val rootUrl: String

    open fun fill(): ModelAndView = adminBaseControllerService.createAdminDefaultModelAndView(fillAction)
            .addObject(CONFIGURATION_ATTR_NAME, classItem.newInstance())

    open fun add(item: T): ModelAndView {
        val configName = configName
        val items = configurationService.getJsonArrayConfig(classItem, configName)
        items.add(0, item)
        configurationService.saveConfigAsJson(items, configName)
        return adminBaseControllerService.baseControllerService.createRedirectModelAndView(rootUrl)
    }

    open fun deleteGet(id: Int): ModelAndView {
        val configName = configName
        val items = configurationService.getJsonArrayConfig(classItem, configName)
        try {
            items.removeAt(id)
        } catch (e: Exception) {
            throw NotFoundException(classItem, id.toLong())
        }

        configurationService.saveConfigAsJson(items, configName)
        return adminBaseControllerService.baseControllerService.createRedirectModelAndView(rootUrl)
    }

    open fun editGet(item: T): ModelAndView {
        var item = item
        val modelAndView = adminBaseControllerService.createAdminDefaultModelAndView(editAction)
        val items = configurationService.getJsonArrayConfig(classItem, configName)
        var id = -1
        try {
            // TODO what going on here?
            id = item.id.toInt()
            item = items[id]
            item.id = id.toLong()
        } catch (e: Exception) {
            throw NotFoundException(classItem, id.toLong())
        }

        modelAndView.addObject(CONFIGURATION_ATTR_NAME, item)
        setPreview(modelAndView, items)
        return modelAndView
    }

    open fun editPost(item: T): ModelAndView {
        val configName = configName
        val items = configurationService.getJsonArrayConfig(classItem, configName)

        return try {
            items[item.id.toInt()] = item
            configurationService.saveConfigAsJson(items, configName)
            adminBaseControllerService.baseControllerService.createRedirectModelAndView(rootUrl)
        } catch (e: Exception) {
            throw NotFoundException(classItem, item.id)
        }
    }

    open fun list(): ModelAndView {
        val modelAndView = adminBaseControllerService.createAdminDefaultModelAndView(listAction)
        val items = configurationService.getJsonArrayConfig(classItem, configName)
        modelAndView.addObject(CONFIGURATION_LIST_ATTR_NAME, items)
        setPreview(modelAndView, items)
        return modelAndView
    }

    open fun sort(startIndex: Int, stopIndex: Int): ModelAndView {
        val configName = configName
        val items = configurationService.getJsonArrayConfig(classItem, configName)
        val item = try {
            items.removeAt(startIndex)
        } catch (e: Exception) {
            throw NotFoundException(e.message)
        }

        val newItems = ArrayList<T>()
        newItems.addAll(items.subList(0, Math.min(stopIndex, items.size)))
        newItems.add(item)
        newItems.addAll(items.subList(Math.min(stopIndex, items.size), items.size))
        configurationService.saveConfigAsJson(newItems, configName)
        return adminBaseControllerService.baseControllerService.createRedirectModelAndView(rootUrl)
    }

    private fun setPreview(modelAndView: ModelAndView, list: List<T>) =
            try {
                modelAndView.addObject(CONFIGURATION_LIST_PREVIEW_ATTR_NAME, ObjectMapper().writeValueAsString(list))
                        .addObject(CONFIGURATION_LIST_ATTR_NAME, list)
            } catch (e: Exception) {
                throw NotFoundException()
            }

    companion object {
        const val CONFIGURATION_ATTR_NAME = "configuration"
        const val CONFIGURATION_LIST_ATTR_NAME = "configurationList"
        const val CONFIGURATION_LIST_PREVIEW_ATTR_NAME = "configurationListPreview"
    }
}

package ru.planeta.admin.controllers.shop

import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.ResponseBody
import org.springframework.web.servlet.ModelAndView
import ru.planeta.admin.Actions
import ru.planeta.admin.Shop
import ru.planeta.admin.controllers.services.BaseShopControllerService
import ru.planeta.api.service.shop.ProductUsersService
import ru.planeta.api.web.authentication.myProfileId
import ru.planeta.model.shop.ProductInfo

/**
 * Products controller for admin zone
 * User: a.savanovich
 * Date: 02.07.12
 * Time: 14:12
 */
@Controller
class ProductsController(private val baseShopControllerService: BaseShopControllerService,
                         private val productUsersService: ProductUsersService) {

    @GetMapping(Shop.STORE_INFO)
    fun storeProductsView(@RequestParam(defaultValue = "0") tagId: Int): ModelAndView =
            baseShopControllerService.createDefaultAdminModelAndView(Actions.ADMIN_STORE_INFO)
                    .addObject("tagId", tagId)

    @GetMapping(Shop.STORE_PRODUCTS)
    @ResponseBody
    fun getStoreProducts(@RequestParam(defaultValue = "") query: String,
                         @RequestParam(defaultValue = "0") tagId: Int,
                         @RequestParam(defaultValue = "0") offset: Int,
                         @RequestParam(defaultValue = "20") limit: Int,
                         @RequestParam(defaultValue = "") status: String): List<ProductInfo> =
            productUsersService.getTopProducts(offset, limit, query, tagId, status)

    @GetMapping(Shop.PRODUCTS_LIST)
    @ResponseBody
    fun getChildrenProducts(@RequestParam("productId") productId: Long): List<ProductInfo> =
            productUsersService.getProductsByParent(myProfileId(), productId)
}

package ru.planeta.admin.validation

import org.springframework.stereotype.Component
import org.springframework.validation.Errors
import org.springframework.validation.ValidationUtils
import org.springframework.validation.Validator
import ru.planeta.model.advertising.VastDTO

/**
 * Created with IntelliJ IDEA.
 * Date: 24.01.14
 * Time: 13:09
 */
@Component
class VastDTOValidator : Validator {
    override fun supports(clazz: Class<*>): Boolean {
        return clazz.isAssignableFrom(VastDTO::class.java)
    }

    override fun validate(target: Any, errors: Errors) {
        ValidationUtils.rejectIfEmpty(errors, "adName", "field.required")
        ValidationUtils.rejectIfEmpty(errors, "videoUrl", "field.required")
        ValidationUtils.rejectIfEmpty(errors, "redirectUrl", "field.required")
    }
}

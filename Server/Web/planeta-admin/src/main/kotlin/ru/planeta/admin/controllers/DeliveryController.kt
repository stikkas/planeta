package ru.planeta.admin.controllers

import org.springframework.stereotype.Controller
import org.springframework.validation.BindingResult
import org.springframework.web.bind.annotation.*
import org.springframework.web.servlet.ModelAndView
import ru.planeta.admin.Actions
import ru.planeta.admin.Urls
import ru.planeta.api.model.ActionStatus
import ru.planeta.api.service.campaign.DeliveryService
import ru.planeta.api.service.geo.GeoService
import ru.planeta.api.web.authentication.myProfileId
import ru.planeta.api.web.controllers.services.BaseControllerService
import ru.planeta.model.common.delivery.BaseDelivery
import ru.planeta.model.common.delivery.LinkedDelivery
import ru.planeta.model.common.delivery.SubjectType
import ru.planeta.model.enums.ProjectType
import ru.planeta.model.profile.location.IGeoLocation
import ru.planeta.model.profile.location.LocationType
import java.util.*
import javax.validation.Valid

/**
 * @author Andrew.Arefyev@gmail.com
 * 26.06.2014 16:00
 */
@Controller
class DeliveryController(private val deliveryService: DeliveryService,
                         private val geoService: GeoService,
                         private val baseControllerService: BaseControllerService) {

    @PostMapping(Urls.ADMIN_DELIVERY_UPDATE_LINCED_SERVICE)
    @ResponseBody
    fun updateLinkedDeliveryService(@Valid @RequestBody service: LinkedDelivery, result: BindingResult): ActionStatus<LinkedDelivery> {
        if (result.hasErrors()) {
            return ActionStatus.createErrorStatus(result, baseControllerService.messageSource)
        }
        deliveryService.saveLinkedDelivery(myProfileId(), service)
        return ActionStatus.createSuccessStatus(deliveryService.getLinkedDelivery(service.shareId, service.serviceId, service.subjectType))
    }

    @GetMapping(Urls.ADMIN_DELIVERY_GET_LINKED_SERVICE)
    @ResponseBody
    fun getLinkedDeliveries(@RequestParam subjectId: Long,
                            @RequestParam serviceId: Long,
                            @RequestParam subjectType: SubjectType): ActionStatus<LinkedDelivery> =
            ActionStatus.createSuccessStatus(deliveryService.getLinkedDelivery(subjectId, serviceId, subjectType))

    @GetMapping(Urls.MODERATOR_GET_BASE_DELIVERY_SERVICES)
    @ResponseBody
    fun getDeliveryServices(@RequestParam(defaultValue = "0") shareId: Long): List<BaseDelivery> {
        val deliveries = deliveryService.getUnlinkedBaseDeliveries(shareId, SubjectType.SHARE)
        Collections.sort(deliveries) { delivery, delivery2 -> delivery.name.compareTo(delivery2.name) }
        return deliveries
    }

    @GetMapping(Urls.MODERATOR_BASE_DELIVERY_SERVICES)
    fun getDeliveryServices(@RequestParam(required = false) searchString: String?): ModelAndView {
        val modelAndView = baseControllerService.createDefaultModelAndView(Actions.ADMIN_DELIVERY_SERVICES, ProjectType.ADMIN)
        modelAndView.addObject("deliveryServices", deliveryService.getBaseDeliveries(searchString))
        return modelAndView
    }

    @PostMapping(Urls.ADMIN_DELIVERY_REMOVE_LINKED_SERVICE)
    @ResponseBody
    fun removeLinkedDeliveryService(@RequestParam serviceId: Long,
                                    @RequestParam subjectId: Long,
                                    @RequestParam subjectType: SubjectType): ActionStatus<BaseDelivery> {
        deliveryService.disableLinkDelivery(myProfileId(), subjectId, serviceId, subjectType)
        return ActionStatus.createSuccessStatus()
    }

    @RequestMapping(value = Urls.ADMIN_NESTED_LOCATIONS, method = [RequestMethod.GET, RequestMethod.POST])
    @ResponseBody
    fun getNestedLocations(@RequestParam locationId: Int,
                           @RequestParam locationType: LocationType): Collection<IGeoLocation> =
            if (locationType == null) {
                geoService.topLocations
            } else geoService.getNestedLocations(locationId, locationType)

    @PostMapping(Urls.ADMIN_DELIVERY_UPDATE_BASE_SERVICE)
    @ResponseBody
    fun updateBaseDeliveryService(@Valid @RequestBody service: BaseDelivery, result: BindingResult): ActionStatus<BaseDelivery> {
        if (result.hasErrors()) {
            return ActionStatus.createErrorStatus(result, baseControllerService.messageSource)
        }
        val myProfileId = myProfileId()
        deliveryService.saveBaseDelivery(myProfileId, service)
        return ActionStatus.createSuccessStatus(deliveryService.getBaseDelivery(myProfileId, service.serviceId))
    }

    @PostMapping(Urls.ADMIN_DELIVERY_REMOVE_BASE_SERVICE)
    @ResponseBody
    fun removeBaseDeliveryService(@RequestParam serviceId: Long): ActionStatus<BaseDelivery> {
        deliveryService.removeBaseDelivery(myProfileId(), serviceId)
        return ActionStatus.createSuccessStatus()
    }
}

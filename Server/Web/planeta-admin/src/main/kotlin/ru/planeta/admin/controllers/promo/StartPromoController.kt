package ru.planeta.admin.controllers.promo

import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.servlet.ModelAndView
import ru.planeta.admin.Actions
import ru.planeta.admin.Urls
import ru.planeta.admin.controllers.services.AdminBaseControllerService
import ru.planeta.api.service.configurations.ConfigurationService


@Controller
class StartPromoController(private val configurationService: ConfigurationService,
                           private val adminBaseControllerService: AdminBaseControllerService) {

    @GetMapping(Urls.ADMIN_START_PROMO_LIST)
    fun promoListGet(): ModelAndView {
        val modelAndView = adminBaseControllerService.createAdminDefaultModelAndView(Actions.ADMIN_START_PROMO_LIST)
        val idList = configurationService.promoCampaignIds
        modelAndView.addObject("configuration", idList)
        return modelAndView
    }

    @GetMapping(Urls.ADMIN_CHARITY_BROADCAST_LIST)
    fun charityPromoListGet(): ModelAndView {
        val modelAndView = adminBaseControllerService.createAdminDefaultModelAndView(Actions.ADMIN_CHARITY_PROMO_LIST)
        val charityRecentWebinarsIds = configurationService.charityRecentWebinarsIds
        modelAndView.addObject("charityRecentWebinarsList", charityRecentWebinarsIds)

        val charityPromoCampaignsIds = configurationService.charityPromoCampaignIds
        modelAndView.addObject("charityPromoCampaignsList", charityPromoCampaignsIds)

        val charityTopCampaignsIds = configurationService.charityTopCampaignIds
        modelAndView.addObject("charityTopCampaignsList", charityTopCampaignsIds)
        return modelAndView
    }

    @PostMapping(Urls.ADMIN_CHARITY_BROADCAST_LIST)
    fun charityPromoListPost(@RequestParam charityRecentWebinarsIds: String): ModelAndView {
        configurationService.setCharityRecentWebinarsIds(charityRecentWebinarsIds)
        return adminBaseControllerService.baseControllerService.createRedirectModelAndView(Urls.ADMIN_CHARITY_BROADCAST_LIST)
    }

    @PostMapping(Urls.ADMIN_START_PROMO_LIST)
    fun promoListPost(@RequestParam(value = "stringValue") idList: String): ModelAndView {
        configurationService.setPromoCampaignIds(idList)
        return adminBaseControllerService.baseControllerService.createRedirectModelAndView(Urls.ADMIN_START_PROMO_LIST)
    }

    @PostMapping(Urls.ADMIN_START_CHARITY_PROMO_LIST)
    fun startCharityPromoListPost(@RequestParam(value = "charityPromoCampaignsIds") idList: String): ModelAndView {
        configurationService.setCharityPromoCampaignIds(idList)
        return adminBaseControllerService.baseControllerService.createRedirectModelAndView(Urls.ADMIN_CHARITY_BROADCAST_LIST)
    }

    @PostMapping(Urls.ADMIN_START_CHARITY_TOP_LIST)
    fun charityTopListPost(@RequestParam(value = "charityTopCampaignsIds") idList: String): ModelAndView {
        configurationService.setCharityTopCampaignIds(idList)
        return adminBaseControllerService.baseControllerService.createRedirectModelAndView(Urls.ADMIN_CHARITY_BROADCAST_LIST)
    }
}

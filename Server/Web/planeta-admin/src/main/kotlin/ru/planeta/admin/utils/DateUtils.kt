package ru.planeta.admin.utils

import org.springframework.web.servlet.ModelAndView
import ru.planeta.api.exceptions.PermissionException
import ru.planeta.model.enums.RotationPeriod

import java.util.Calendar
import java.util.Date
import java.util.GregorianCalendar

import org.apache.commons.lang3.time.DateUtils.*
import ru.planeta.commons.lang.DateUtils.*

object DateUtils {
    fun truncDate(date: Date): Date {
        return org.apache.commons.lang3.time.DateUtils.truncate(date, Calendar.DATE)
    }

    fun getIntervalToday(dateFrom: Date?, dateTo: Date?): Interval {
        val dateFrom = if (dateFrom == null) {
            truncDate(Date())
        } else {
            truncDate(dateFrom)
        }
        val dateTo = if (dateTo == null) {
            getEndOfDate(Date())
        } else {
            getEndOfDate(dateTo)
        }
        return Interval(dateFrom, dateTo)
    }

    fun setIntervalTodayAsDate(dateFrom: Date?, dateTo: Date?, modelAndView: ModelAndView): Interval {
        val interval = getIntervalToday(dateFrom, dateTo)
        if (dateFrom != null) {
            modelAndView.addObject("dateFrom", interval.dateFrom)
        }
        if (dateTo != null) {
            modelAndView.addObject("dateTo", interval.dateTo)
        }
        return interval
    }

    fun getAllTimeInterval(dateFrom: Date?, dateTo: Date?): Interval {
        var dateFrom = dateFrom
        var dateTo = dateTo
        //$('#allTimeButton').click(buttonClick).data('dates', [new Date(1900, 0, 1), new Date(2100, 0, 1)]);
        if (dateTo == null) {
            dateTo = truncDate(GregorianCalendar(2100, Calendar.JANUARY, 1).time)
        } else {
            dateTo = truncDate(dateTo)
        }
        if (dateFrom == null) {
            dateFrom = truncDate(GregorianCalendar(1900, Calendar.JANUARY, 1).time)
        } else {
            dateFrom = truncDate(dateFrom)
        }

        return Interval(dateFrom, if (dateFrom.before(dateTo)) dateTo else dateFrom)
    }

    fun setAllTimeAsDate(dateFrom: Date?, dateTo: Date?, modelAndView: ModelAndView): Interval {
        val interval = getAllTimeInterval(dateFrom, dateTo)
        if (dateFrom != null) {
            modelAndView.addObject("dateFrom", interval.dateFrom)
        }
        if (dateTo != null) {
            modelAndView.addObject("dateTo", interval.dateTo)
        }
        return interval
    }

    fun getDateTo(dateFrom: Date, step: RotationPeriod): Date {
        var dateTo: Date
        when (step) {
            RotationPeriod.DAY -> {
                dateTo = addDays(dateFrom, 1)
                dateTo = addDays(dateTo, -1)
            }
            RotationPeriod.WEEK -> {
                dateTo = addWeeks(dateFrom, 1)
                dateTo = addDays(dateTo, -1)
            }
            RotationPeriod.MONTH -> {
                dateTo = addMonths(dateFrom, 1)
                dateTo = addDays(dateTo, -1)
            }
            else -> throw PermissionException()
        }
        return dateTo
    }

    fun getInterval(dateFrom: Date?, dateTo: Date?): Interval {
        var dateTo = if (dateTo == null) {
            truncDate(Date())
        } else {
            truncDate(dateTo)
        }
        var dateFrom = if (dateFrom == null) {
            addMonths(dateTo, -1)
        } else {
            truncDate(dateFrom)
        }
        if (dateFrom.after(dateTo)) {
            val date = dateFrom
            dateFrom = dateTo
            dateTo = date
        }
        return Interval(dateFrom, dateTo)
    }

    class Interval(val dateFrom: Date, val dateTo: Date) {

        fun between(date: Date): Boolean {
            return dateFrom.before(date) && dateTo.after(date)
        }
    }
}

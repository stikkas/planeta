package ru.planeta.admin.controllers.loayalty

import org.apache.commons.lang3.math.NumberUtils
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.*
import org.springframework.web.servlet.ModelAndView
import ru.planeta.admin.Actions
import ru.planeta.admin.Loyalty
import ru.planeta.admin.controllers.services.AdminBaseControllerService
import ru.planeta.admin.utils.DateUtils.setIntervalTodayAsDate
import ru.planeta.api.model.ActionStatus
import ru.planeta.api.news.LoggerService
import ru.planeta.api.service.billing.order.OrderService
import ru.planeta.api.service.loyalty.BonusService
import ru.planeta.api.web.authentication.myProfileId
import ru.planeta.model.common.OrderInfo
import ru.planeta.model.common.loyalty.Bonus
import ru.planeta.model.common.loyalty.BonusPriorityItem
import ru.planeta.model.enums.OrderObjectType
import ru.planeta.model.news.ProfileNews
import ru.planeta.model.shop.enums.PaymentStatus
import java.text.SimpleDateFormat
import java.util.*

/**
 * Bonus orders controller.<br></br>
 */
@Controller
class BonusOrderController(private val bonusService: BonusService,
                           private val orderService: OrderService,
                           private val profileNewsService: LoggerService,
                           private val adminBaseControllerService: AdminBaseControllerService) {


    @GetMapping(Loyalty.ADMIN_EXPORT_LOYALTY_ORDERS)
    fun exportBonusOrders(@RequestParam(required = false) orderId: Long?,
                          @RequestParam(required = false) status: PaymentStatus?,
                          @RequestParam(required = false) dateFrom: Date?,
                          @RequestParam(required = false) dateTo: Date?): ModelAndView {
        val orderIds: List<Long> = if (orderId != null) listOf(orderId) else emptyList()
        val orders = orderService.getOrders(orderIds, OrderObjectType.BONUS, status, dateFrom, dateTo, 0, 0)
        return adminBaseControllerService.createAdminDefaultModelAndView(Actions.ADMIN_EXPORT_BONUS_ORDERS)
                .addObject("orders", orders)
    }

    @GetMapping(Loyalty.ADMIN_LOYALTY_ORDERS)
    fun bonusOrders(@RequestParam(required = false) query: String?,
                    @RequestParam(required = false) status: PaymentStatus?,
                    @RequestParam(required = false) dateFrom: Date?,
                    @RequestParam(required = false) dateTo: Date?,
                    @RequestParam(defaultValue = "0") offset: Int,
                    @RequestParam(defaultValue = "10") limit: Int): ModelAndView {

        val orderId = NumberUtils.toLong(query, 0)
        val modelAndView = adminBaseControllerService.createAdminDefaultModelAndView(Actions.ADMIN_LOYALTY_BONUS_ORDERS)

        val dateFrom2 = dateFrom ?: SimpleDateFormat("dd/MM/yyyy").parse("01/01/1900")

        val interval = setIntervalTodayAsDate(dateFrom2, dateTo, modelAndView)

        val orders: Collection<OrderInfo> = if (orderId > 0)
            orderService.getOrders(listOf(orderId), OrderObjectType.BONUS, status, interval.dateFrom, interval.dateTo, offset, limit)
        else
            orderService.getOrders(query, OrderObjectType.BONUS, status, interval.dateFrom, interval.dateTo, offset, limit)

        return modelAndView
                .addObject("orders", orders)
                .addObject("state", status)
                .addObject("offset", offset)
                .addObject("limit", limit)
                .addObject("count", offset + limit + if (limit == orders.size) 1 else 0)
                .addObject("statuses", Arrays.asList(PaymentStatus.PENDING, PaymentStatus.CANCELLED, PaymentStatus.COMPLETED))
    }

    @PostMapping(Loyalty.ADMIN_LOYALTY_CANCEL_ORDER)
    @ResponseBody
    fun cancelBonusOrder(@RequestParam orderId: Long): ActionStatus<*> {
        orderService.cancelBonusOrder(myProfileId(), orderId)
        return ActionStatus.createSuccessStatus<Any>()
    }

    @PostMapping(Loyalty.ADMIN_LOYALTY_COMPLETE_ORDER)
    @ResponseBody
    fun completeBonusOrder(@RequestParam orderId: Long): ActionStatus<*> {
        orderService.completeBonusOrder(myProfileId(), orderId)
        return ActionStatus.createSuccessStatus<Any>()
    }

    @GetMapping(Loyalty.ADMIN_LOYALTY_BONUSES)
    fun bonuses(): ModelAndView = adminBaseControllerService.createAdminDefaultModelAndView(Actions.ADMIN_LOYALTY_BONUSES)
            .addObject("bonuses", bonusService.getAllBonuses(0, 0))

    @PostMapping(Loyalty.ADMIN_REORDER_BONUSES)
    @ResponseBody
    fun reorderBonuses(@RequestBody priorityList: List<BonusPriorityItem>): ActionStatus<*> {
        bonusService.reorderBonuses(priorityList)
        return ActionStatus.createSuccessStatus<Any>()
    }

    @PostMapping(Loyalty.ADMIN_LOYALTY_SAVE_BONUS)
    @ResponseBody
    fun saveBonus(@RequestBody bonus: Bonus): ActionStatus<Bonus> {
        val newsType: ProfileNews.Type = if (bonus.bonusId > 0) {
            if (bonus.isDeleted) {
                ProfileNews.Type.ADMIN_DELETE_BONUS
            } else {
                ProfileNews.Type.ADMIN_EDIT_BONUS
            }
        } else {
            ProfileNews.Type.ADMIN_CREATE_NEW_BONUS
        }
        bonusService.saveBonus(bonus)
        profileNewsService.addProfileNews(newsType, myProfileId(), bonus.bonusId)
        return ActionStatus.createSuccessStatus(bonus)
    }
}
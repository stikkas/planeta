package ru.planeta.admin.controllers.biblio

import org.apache.poi.ss.usermodel.WorkbookFactory
import org.springframework.stereotype.Controller
import org.springframework.validation.BindingResult
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.multipart.MultipartFile
import org.springframework.web.multipart.commons.CommonsMultipartFile
import org.springframework.web.servlet.ModelAndView
import ru.planeta.admin.Actions
import ru.planeta.admin.BookUrls
import ru.planeta.admin.Urls
import ru.planeta.admin.controllers.services.AdminBaseControllerService
import ru.planeta.api.exceptions.NotFoundException
import ru.planeta.api.service.biblio.BookService
import ru.planeta.api.service.biblio.PublishingHouseService
import ru.planeta.api.utils.DocumentUtils
import ru.planeta.api.web.authentication.myProfileId
import ru.planeta.api.web.utils.ExcelReportUtils
import ru.planeta.commons.web.WebUtils
import ru.planeta.dao.bibliodb.MagazineInfoDAO
import ru.planeta.dao.bibliodb.MagazinePriceListDAO
import ru.planeta.model.bibliodb.*
import ru.planeta.model.bibliodb.enums.BiblioObjectStatus
import ru.planeta.model.enums.ProjectType
import java.util.*
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse
import javax.validation.Valid

/*
 * Created by Alexey on 12.04.2016.
 */
@Controller
class BookController(private val bookService: BookService,
                     private val publishingHouseService: PublishingHouseService,
                     private val magazinePriceListDAO: MagazinePriceListDAO,
                     private val magazineInfoDAO: MagazineInfoDAO,
                     private val adminBaseControllerService: AdminBaseControllerService) {

    @GetMapping(BookUrls.BOOKS)
    fun getPrintings(filter: BookFilter): ModelAndView {
        if (filter.limit == 0) {
            filter.limit = 10
        }

        if (filter.statuses == null) {
            filter.statuses = EnumSet.of(BiblioObjectStatus.ACTIVE, BiblioObjectStatus.PAUSED)
        }
        val books = bookService.searchBooks(filter)
        val count = bookService.selectCountSearchBooks(filter)
        return adminBaseControllerService.createAdminDefaultModelAndView(Actions.ADMIN_BIBLIO_BOOKS)
                .addObject("offset", filter.offset)
                .addObject("limit", filter.limit)
                .addObject("searchStr", filter.searchStr)
                .addObject("statuses", filter.statuses)
                .addObject("dateFrom", filter.dateFrom)
                .addObject("dateTo", filter.dateTo)
                .addObject("books", books)
                .addObject("count", count)
    }

    @GetMapping(BookUrls.BOOKS_REPORT)
    fun getBooksReport(filter: BookFilter, response: HttpServletResponse) {
        filter.limit = 0;
        filter.offset = 0;

        if (filter.statuses == null) {
            filter.statuses = EnumSet.of(BiblioObjectStatus.ACTIVE, BiblioObjectStatus.PAUSED)
        }
        val books = bookService.searchBooks(filter)

        ExcelReportUtils.setReportHeaders(response)
        ExcelReportUtils.generateBookReport(books, response.writer)
    }

    @GetMapping(BookUrls.BOOK)
    fun addOrEditPrinting(
            @RequestParam(defaultValue = "0") limit: Int,
            @RequestParam(defaultValue = "0") offset: Int,
            @RequestParam(defaultValue = "0") bookId: Long): ModelAndView {
        val book: Book
        if (bookId == 0L) {
            book = Book()
        } else {
            book = bookService.getBookById(bookId)
            book.tags = bookService.getBookTagsByBookId(bookId)
        }

        val bookTags = bookService.getBookTags(limit.toLong(), offset.toLong())
        val houses = publishingHouseService.getPublishingHouses(null, offset, limit)

        val bookStatuses = EnumSet.allOf(BiblioObjectStatus::class.java)
        return adminBaseControllerService.createAdminDefaultModelAndView(Actions.ADMIN_BIBLIO_ADD_OR_EDIT_BOOK)
                .addObject("bookTags", bookTags)
                .addObject("bookStatuses", bookStatuses)
                .addObject("book", book)
                .addObject("houses", houses)
    }

    @PostMapping(BookUrls.SAVE_BOOK)
    fun savePrinting(@Valid book: Book, result: BindingResult): ModelAndView {
        val bookTags = bookService.getBookTags(0, 1000)
        val houses = publishingHouseService.getPublishingHouses(null, 0, 1000)
        val bookStatuses = EnumSet.allOf(BiblioObjectStatus::class.java)

        if (result.hasErrors()) {
            return adminBaseControllerService.createAdminDefaultModelAndView(Actions.ADMIN_BIBLIO_ADD_OR_EDIT_BOOK)
                    .addObject("errors", result.allErrors)
                    .addObject("book", book)
                    .addObject("bookTags", bookTags)
                    .addObject("bookStatuses", bookStatuses)
                    .addObject("houses", houses)
        }

        //TODO: repair ugly hack
        for (tagId in book.tagIds) {
            book.addTag(BookTag(tagId!!))
        }

        bookService.saveBook(book)
        val returnUrl = WebUtils.createUrl(BookUrls.BOOK, WebUtils.Parameters().add("bookId", book.bookId).params)
        return adminBaseControllerService.baseControllerService.createRedirectModelAndView(returnUrl)
    }

    @GetMapping(BookUrls.DELETE_BOOK)
    fun deletePrinting(@RequestParam(required = false) bookId: Long?): ModelAndView {
        bookService.deleteBook(bookId)
        return adminBaseControllerService.baseControllerService.createRedirectModelAndView(BookUrls.BOOKS)
    }

    @GetMapping(Urls.ADMIN_BIBLIO_RUSSIAN_POST_BOOKS_INFO_FROM_FILE)
    fun getBiblioRussianPostBooksInfoFromFile(@RequestParam(defaultValue = "") errorMessage: String): ModelAndView {
        val clientId = myProfileId()
        if (!adminBaseControllerService.permissionService.hasAdministrativeRole(clientId)) {
            return adminBaseControllerService.baseControllerService.createRedirectModelAndView(Urls.ADMIN_INDEX)
        }

        val modelAndView = adminBaseControllerService.baseControllerService.createDefaultModelAndView(Actions.ADMIN_BIBLIO_RUSSIAN_POST_BOOKS_INFO_FROM_FILE, ProjectType.ADMIN)
        modelAndView.addObject("errorMessage", errorMessage)
        return modelAndView
    }

    @PostMapping(Urls.ADMIN_BIBLIO_RUSSIAN_POST_BOOKS_INFO_FROM_FILE)
    fun getBiblioRussianPostBooksInfoFromFile(@RequestParam("file") file: MultipartFile,
                                              request: HttpServletRequest, response: HttpServletResponse): ModelAndView {
        val clientId = myProfileId()
        if (!adminBaseControllerService.permissionService.hasAdministrativeRole(clientId)) {
            return adminBaseControllerService.baseControllerService.createRedirectModelAndView(Urls.ADMIN_INDEX)
        }

        val magazinePriceList = MagazinePriceList()
        val magazineInfoList = ArrayList<MagazineInfo>()

        file.inputStream.use { inp ->
            val wb = WorkbookFactory.create(inp) ?: return getBiblioRussianPostBooksInfoFromFile("Cat'n create inputStream from chosen document [" + (file as CommonsMultipartFile).fileItem.name + "]. Are you sure the Excel document is right?")
            val dataSheetNumber = 0
            val sheet = wb.getSheetAt(dataSheetNumber) ?: return getBiblioRussianPostBooksInfoFromFile("Can't open the sheet number " + dataSheetNumber + " from [File: " + (file as CommonsMultipartFile).fileItem.name + "]. Are you sure the sheet is exists?")

            try {
                val priceListTitleRow = sheet.getRow(2)
                val priceListName = DocumentUtils.getCellValueString(priceListTitleRow.getCell(0))
                magazinePriceList.name = priceListName?.trim { it <= ' ' }
                magazinePriceListDAO.insert(magazinePriceList)

                val rowStart = 5
                val rowsCount = sheet.lastRowNum
                for (i in rowStart..rowsCount) {
                    val magazineInfo = MagazineInfo()

                    val row = sheet.getRow(i) ?: continue

                    magazineInfo.priceListId = magazinePriceList.priceListId
                    magazineInfo.magazineName = DocumentUtils.getCellValueString(row.getCell(0))
                    magazineInfo.setpIndex(DocumentUtils.getCellValueString(row.getCell(1)))
                    magazineInfo.msp = DocumentUtils.getCellValueInt(row.getCell(2))
                    magazineInfo.periodicity = DocumentUtils.getCellValueInt(row.getCell(3))
                    magazineInfo.partPriceWithoutTax = DocumentUtils.getCellValueBigDecimal(row.getCell(6), 2)
                    magazineInfo.taxPercentage = DocumentUtils.getCellValueBigDecimal(row.getCell(8), 4)
                    magazineInfo.partPriceWithTax = DocumentUtils.getCellValueBigDecimal(row.getCell(9), 2)
                    magazineInfo.commentNoMagazineMonth = DocumentUtils.getCellValueString(row.getCell(11))

                    magazineInfoList.add(magazineInfo)
                }
            } catch (ex: Exception) {
                return getBiblioRussianPostBooksInfoFromFile("Something went wrong with reading data from ["
                        + (file as CommonsMultipartFile).fileItem.name + " : sheet number " + dataSheetNumber + "]. "
                        + "[FOR DEVELOPERS: \n\n" + ex.toString() + "]")
            }


        }

        for (magazineInfo in magazineInfoList) {
            magazineInfoDAO.insert(magazineInfo)
        }

        val redirectUrl = WebUtils.Parameters()
                .add("priceListId", magazinePriceList.priceListId)
                .createUrl(Urls.ADMIN_BIBLIO_RUSSIAN_MAGAZINE_INFO_LIST, false)

        return adminBaseControllerService.baseControllerService.createRedirectModelAndView(redirectUrl)
    }

    @GetMapping(Urls.ADMIN_BIBLIO_RUSSIAN_MAGAZINE_INFO_LIST)
    fun getBiblioRussianPostPriceList(@RequestParam priceListId: Long,
                                      request: HttpServletRequest, response: HttpServletResponse): ModelAndView {
        val clientId = myProfileId()
        if (!adminBaseControllerService.permissionService.hasAdministrativeRole(clientId)) {
            return adminBaseControllerService.baseControllerService.createRedirectModelAndView(Urls.ADMIN_INDEX)
        }

        val modelAndView = adminBaseControllerService.createAdminDefaultModelAndView(Actions.ADMIN_BIBLIO_RUSSIAN_POST_MAGAZINE_INFO_LIST)

        val magazinePriceList = magazinePriceListDAO.select(priceListId)
        val magazineInfoList: List<MagazineInfo> =
                if (magazinePriceList != null) {
                    magazineInfoDAO.selectList(priceListId, 0, 500)
                } else {
                    throw NotFoundException()
                }

        return modelAndView
                .addObject("magazinePriceList", magazinePriceList)
                .addObject("magazineInfoList", magazineInfoList)
    }

    @GetMapping(Urls.ADMIN_BIBLIO_RUSSIAN_PRICE_LISTS)
    fun getBiblioRussianPostPriceLists(request: HttpServletRequest, response: HttpServletResponse): ModelAndView {
        val clientId = myProfileId()
        if (!adminBaseControllerService.permissionService.hasAdministrativeRole(clientId)) {
            return adminBaseControllerService.baseControllerService.createRedirectModelAndView(Urls.ADMIN_INDEX)
        }

        val modelAndView = adminBaseControllerService.createAdminDefaultModelAndView(Actions.ADMIN_BIBLIO_RUSSIAN_POST_PRICE_LISTS)

        val magazinePriceLists = magazinePriceListDAO.selectList(0, 100)
        return modelAndView.addObject("magazinePriceLists", magazinePriceLists)
    }
}


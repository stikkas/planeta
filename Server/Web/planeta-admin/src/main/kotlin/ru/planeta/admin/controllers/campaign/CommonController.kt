package ru.planeta.admin.controllers.campaign

import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import ru.planeta.admin.Urls
import ru.planeta.api.service.campaign.CampaignService
import ru.planeta.model.common.campaign.Share
import ru.planeta.model.common.campaign.enums.CampaignStatus

/**
 * Created with IntelliJ IDEA.
 * Date: 16.09.2015
 * Time: 14:22
 * To change this template use File | Settings | File Templates.
 */
@RestController("CampaignCommonController")
class CommonController(private val campaignService: CampaignService) {

    @GetMapping(Urls.START_CAMPAIGNS_GET_SHARES)
    fun starCampaignShareList(@RequestParam campaignId: Long): List<Share>? {
        val campaign = campaignService.getCampaign(campaignId)
        return if (campaign == null || campaign.status != CampaignStatus.ACTIVE) {
            null
        } else {
            campaignService.getCampaignSharesFiltered(campaignId)
        }
    }
}

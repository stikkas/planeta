package ru.planeta.admin.controllers.investing

import ru.planeta.model.common.TopayTransaction
import ru.planeta.model.common.UserPrivateInfo
import ru.planeta.model.common.campaign.Campaign
import ru.planeta.model.enums.ContractorType
import ru.planeta.model.enums.ModerateStatus

/**
 *
 * Created by asavan on 26.08.2016.
 */
class InvestingOrderAdminInfo {

    var transaction: TopayTransaction? = null
    var displayName: String? = null
    var campaign: Campaign? = null
    var userType: ContractorType? = null
    var fio: String? = null
    var orgName: String? = null
    var status: ModerateStatus? = null
    var investingOrderInfoId: Long = 0
    var phone: String? = null
    var userInfo: UserPrivateInfo? = null
}

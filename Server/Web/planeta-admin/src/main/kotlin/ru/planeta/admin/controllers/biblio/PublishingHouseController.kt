package ru.planeta.admin.controllers.biblio

import org.springframework.stereotype.Controller
import org.springframework.validation.BindingResult
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.ResponseBody
import org.springframework.web.servlet.ModelAndView
import ru.planeta.admin.Actions
import ru.planeta.admin.PublishingHouseUrls
import ru.planeta.admin.controllers.services.AdminBaseControllerService
import ru.planeta.api.service.biblio.PublishingHouseService
import ru.planeta.api.web.utils.ExcelReportUtils
import ru.planeta.commons.web.WebUtils
import ru.planeta.model.bibliodb.PublishingHouse
import javax.servlet.http.HttpServletResponse
import javax.validation.Valid

/*
 * Created by Alexey on 12.04.2016.
 */
@Controller
class PublishingHouseController(private val publishingHouseService: PublishingHouseService,
                                private val adminBaseControllerService: AdminBaseControllerService) {

    @PostMapping(PublishingHouseUrls.PUBLISHING_HOUSE)
    @ResponseBody
    fun newPublishingHouse(@Valid publishingHouse: PublishingHouse, result: BindingResult): ModelAndView {
        publishingHouseService.addPublishingHouse(publishingHouse)
        val returnUrl = WebUtils.createUrl(PublishingHouseUrls.PUBLISHING_HOUSE,
                WebUtils.Parameters().add("publishingHouseId", publishingHouse.publishingHouseId).params)
        return adminBaseControllerService.baseControllerService.createRedirectModelAndView(returnUrl)
    }

    @GetMapping(PublishingHouseUrls.PUBLISHING_HOUSE)
    fun newPublishingHouse(@RequestParam(defaultValue = "0") publishingHouseId: Long): ModelAndView {
        val publishingHouse = if (publishingHouseId == 0L) {
            PublishingHouse()
        } else {
            publishingHouseService.getPublishingHouseById(publishingHouseId)
        }

        return adminBaseControllerService.createAdminDefaultModelAndView(Actions.ADMIN_BIBLIO_PUBLISHING_HOUSE)
                .addObject("house", publishingHouse)
    }

    @GetMapping(PublishingHouseUrls.PUBLISHING_HOUSES_REPORT)
    fun getLibsReport(@RequestParam(required = false) searchRow: String?, response: HttpServletResponse) {
        val publishingHouses = publishingHouseService.getPublishingHouses(searchRow, 0, 0)

        ExcelReportUtils.setReportHeaders(response)
        ExcelReportUtils.generatePublishingHouseReport(publishingHouses, response.writer)
    }

    @GetMapping(PublishingHouseUrls.PUBBLISHING_HOUSES)
    fun publishingHouses(@RequestParam(required = false) searchRow: String?,
                         @RequestParam(defaultValue = "10") limit: Int,
                         @RequestParam(defaultValue = "0") offset: Int): ModelAndView {

        val publishingHouses = publishingHouseService.getPublishingHouses(searchRow, limit, offset)
        val count = publishingHouseService.selectCountPublishingHouses(searchRow)
        return adminBaseControllerService.createAdminDefaultModelAndView(Actions.ADMIN_BIBLIO_PUBLISHING_HOUSES)
                .addObject("offset", offset)
                .addObject("limit", limit)
                .addObject("searchRow", searchRow)
                .addObject("publishingHouses", publishingHouses)
                .addObject("count", count)
    }
}

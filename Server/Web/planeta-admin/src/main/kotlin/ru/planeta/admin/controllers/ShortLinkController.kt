package ru.planeta.admin.controllers

import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.servlet.ModelAndView
import ru.planeta.admin.Actions
import ru.planeta.admin.Urls
import ru.planeta.admin.controllers.services.AdminBaseControllerService
import ru.planeta.api.exceptions.PermissionException
import ru.planeta.api.model.ConfigurationType
import ru.planeta.api.web.authentication.myProfileId
import ru.planeta.api.web.controllers.services.BaseControllerService
import ru.planeta.dao.commondb.ConfigurationDAO
import ru.planeta.dao.commondb.ShortLinkDAO
import ru.planeta.eva.api.services.ShortLinkService
import ru.planeta.model.common.ShortLink
import ru.planeta.model.enums.ProjectType
import ru.planeta.model.enums.ShortLinkStatus
import java.util.*

/**
 * User: a.savanovich
 * Date: 28.09.16
 * Time: 19:37
 */
@Controller
class ShortLinkController(private val shortLinkDAO: ShortLinkDAO,
                          private val baseControllerService: BaseControllerService,
                          private val shortLinkService: ShortLinkService,
                          private val configurationDAO: ConfigurationDAO,
                          private val adminBaseControllerService: AdminBaseControllerService) {

    @RequestMapping(value = [(Urls.DELETE_SHORT_LINK)])
    fun delete(@RequestParam shortLinkId: Long): ModelAndView {
        shortLinkDAO.delete(shortLinkId)
        return baseControllerService.createRedirectModelAndView(Urls.SHORT_LINKS_LIST)
    }

    @PostMapping(Urls.SAVE_SHORT_LINK)
    fun save(link: ShortLink): ModelAndView {
        link.profileId = myProfileId()

        if (link.shortLinkId > 0) {
            shortLinkDAO.update(link)
        } else {
            link.shortLinkStatus = ShortLinkStatus.ON
            shortLinkDAO.insert(link)
        }
        return goToEdit(link)
    }

    @GetMapping(Urls.SHORT_LINKS_LIST)
    fun getList(@RequestParam(defaultValue = "0") offset: Int,
                @RequestParam(defaultValue = "0") limit: Int,
                @RequestParam(defaultValue = "0") profileId: Long): ModelAndView {

        val shortLinkList = if (profileId > 0) {
            shortLinkDAO.selectListByProfileId(profileId, offset, limit)
        } else {
            shortLinkDAO.selectList(EnumSet.allOf(ShortLinkStatus::class.java), offset, limit)
        }
        return createAdminDefaultModelAndView(Actions.ADMIN_LINKS)
                .addObject("shortLinkList", shortLinkList)
                .addObject("redirectUrlHost", baseControllerService.projectService.getUrl(ProjectType.LINK))
    }

    @GetMapping(Urls.EDIT_SHORT_LINK)
    fun edit(@RequestParam(value = "shortLinkId", defaultValue = "0") shortLinkId: Long): ModelAndView {
        val shortLink: ShortLink? = if (shortLinkId == 0L) ShortLink() else shortLinkDAO.select(shortLinkId)

        val isItExtraSocialShortLink = if (shortLink == null || shortLinkId == 0L) false else shortLinkService.isItExtraSocialShortLink(shortLink.shortLink)
        var existsSocialShortLinksByBasename: List<ShortLink>? = null
        if (!isItExtraSocialShortLink) {
            val socialShortLinkAdditionsList: List<String>? = shortLinkService.getSocialShortLinkSuffixList()
            if (shortLink != null && shortLink.shortLink != null && socialShortLinkAdditionsList != null) {
                existsSocialShortLinksByBasename = shortLinkService.getExistsSocialShortLinksByBasename(shortLink.shortLink,
                        socialShortLinkAdditionsList, 0, 100)
            }
        }
        return createAdminDefaultModelAndView(Actions.ADMIN_EDIT_LINK)
                .addObject("shortLink", shortLink)
                .addObject("existsSocialShortLinksByBasename", existsSocialShortLinksByBasename)
                .addObject("isItExtraSocialShortLink", isItExtraSocialShortLink)
                .addObject("redirectUrlHost", baseControllerService.projectService.getUrl(ProjectType.LINK))
    }

    @GetMapping(Urls.GENERATE_SOCIAL_SHORT_LINKS)
    fun generateSocialShortLinks(@RequestParam(value = "shortLinkId", defaultValue = "0") shortLinkId: Long): ModelAndView {
        if (!adminBaseControllerService.permissionService.hasAdministrativeRole(myProfileId())) {
            throw PermissionException()
        }

        shortLinkService.generateMissingSocialLinks(myProfileId(),
                shortLinkId,
                configurationDAO.select(ConfigurationType.SHORTLINK_EXTRA_SOCIAL_LINKS).stringValue)

        val additionalParams = HashMap<String, Any>()
        additionalParams["shortLinkId"] = shortLinkId

        return createRetAdminDefaultModelAndView(Urls.EDIT_SHORT_LINK, additionalParams)
    }

    @PostMapping(Urls.SWITCH_ON_OFF_SHORT_LINK)
    fun switchOnOff(@RequestParam("shortLinkId") shortLinkId: Long,
                    @RequestParam("shortLinkStatus") previousStatus: ShortLinkStatus): ModelAndView {
        val link = shortLinkDAO.select(shortLinkId)

        if (link?.shortLinkStatus == previousStatus) {
            link.switchStatus()
            shortLinkDAO.update(link)
        }
        return goToEdit(link)
    }

    private fun goToEdit(link: ShortLink?): ModelAndView = baseControllerService.createRedirectModelAndView(Urls.EDIT_SHORT_LINK + "?shortLinkId=" + link?.shortLinkId)

    private fun createAdminDefaultModelAndView(action: Actions): ModelAndView = baseControllerService.createDefaultModelAndView(action, ProjectType.ADMIN)

    private fun createRetAdminDefaultModelAndView(url: String, additionalParams: Map<String, *>): ModelAndView = baseControllerService.createRedirectModelAndView(url, additionalParams)
}

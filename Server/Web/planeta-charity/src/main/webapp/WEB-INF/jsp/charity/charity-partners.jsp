<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="p" uri="http://planeta.ru/tags" %>

<div class="charity-partners">
    <div class="wrap">
        <div class="col-12">
            <div class="charity-partners_head">
                Партнёры
            </div>

            <div class="charity-partners_list">
                <c:forEach items="${partners}" var="partner">
                    <a href="${partner.originalUrl}" target="_blank" title="${partner.name}" style="margin-bottom: 10px; margin-top: 10px">
                        <img style="max-height: 50px; height: auto; width: auto; max-width: 300px;" src="${hf:getThumbnailUrl(partner.imageUrl, 'MEDIUM', 'PHOTO')}" alt="${partner.name}">
                    </a>
                </c:forEach>
            </div>
        </div>
    </div>
</div>
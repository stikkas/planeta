<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ include file="/WEB-INF/jsp/includes/generated/doctype-header.jsp" %>
<%@ taglib prefix="p" uri="http://planeta.ru/tags" %>
<c:set var="imagesDir" value="//${hf:getStaticBaseUrl('')}/images"/>

<head>
    <%@include file="/WEB-INF/jsp/includes/generated/stat-counters-async.jsp"%>
    <%@ include file="/WEB-INF/jsp/includes/head.jsp" %>
    <%@ include file="/WEB-INF/jsp/includes/generated/main-page-meta-header.jsp" %>
    <c:if test="${empty post}">
        <%@ include file="/WEB-INF/jsp/includes/metas-welcome.jsp" %>
    </c:if>
    <c:if test="${not empty post}">
        <%@ include file="/WEB-INF/jsp/includes/metas-news-post.jsp" %>
    </c:if>
    <p:script src="planeta-charity-search.js" />
    <%@ include file="/WEB-INF/jsp/includes/news-js.jsp" %>

    <script type="text/javascript">
        $(document).ready(function() {
            var limit = ${limit};
            var newsTags = ${hf:toJson(newsTags)} || [];
            var partnerNewsTags = ${hf:toJson(partnerNewsTags)} || [];

            var newsCollection = new News.Models.Posts([], {
                url: '/api/util/charity-news-list.json',
                limit: limit,
                offset: ${offset}
            });

            newsCollection.fetch({
                success: function(response) {
                    newsCollection.offset = response.length;
                    var newsView = new Charity.Views.News({
                        el: '.charity-news__top',
                        collection: newsCollection,
                        newsTags: newsTags,
                        title: 'Новости',
                        banner: true
                    });
                    newsView.render();
                }
            });

            var partnerNewsCollection = new News.Models.Posts([], {
                url: '/api/util/charity-partners-news-list.json',
                offset: ${offset},
                limit: limit
            });

            partnerNewsCollection.fetch({
                success: function(response) {
                    partnerNewsCollection.offset = response.length;
                    var partnerNewsView = new Charity.Views.News({
                        el: '.charity-news__offset',
                        collection: partnerNewsCollection,
                        newsTags: partnerNewsTags,
                        title: 'Новости партнёров',
                        banner: false
                    });
                    partnerNewsView.render();
                }
            });

            Banner.init('${mainAppUrl}', workspace.appModel.get('myProfile').get('isAuthor'));

            <c:if test="${not empty post}">
            var view = new Charity.Views.ModalPopup({
                el: '#modal-charity-news-popup',
                model: new BaseModel(${hf:toJson(post)})
            });
            view.open();
            </c:if>
        });
    </script>
</head>

<body class="grid-1200 charity-page">

<div id="modal-charity-news-popup" class="modal-charity-dialog modal-dialog" style="display: none; z-index: 510;"></div>

<%@ include file="/WEB-INF/jsp/includes/generated/irma/header.jsp" %>
<%@ include file="/WEB-INF/jsp/includes/generated/irma/main-nav.jsp" %>

<div id="global-container" class="hidden">
    <div id="main-container" class="wrap-container">
        <div id="center-container">
            <%@ include file="/WEB-INF/jsp/charity/charity-carousel.jsp" %>
            <%@ include file="/WEB-INF/jsp/charity/charity-menu.jsp" %>

            <div class="charity-news charity-news__top"></div>
            <div class="charity-news charity-news__offset"></div>
        </div>
    </div>
</div>

<div class="modal-backdrop modal-backdrop-charity" style="display: none"></div>

<%@ include file="/WEB-INF/jsp/includes/footer.jsp" %>
<div id="registration-modal-form"></div>
</body>
</html>


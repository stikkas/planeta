<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<script id="item-charity-news-template" type="text/x-jquery-template">
    <div class="charity-news_img">
        <a href="#" data-event-click="onPostClick"><img src="{{= imageUrl}}"></a>
    </div>
    <div class="charity-news_cont">
        <div class="charity-news_date">{{= DateUtils.formatDate(eventDate, "D MMMM YYYY")}}</div>
        <div class="charity-news_name">
            <a href="#" data-event-click="onPostClick">{{= title}}</a>
        </div>
        <div class="charity-news_text">{{html headingText}}</div>
    </div>
</script>

<script id="charity-news-template" type="text/x-jquery-template">
    <div class="wrap">
        <div class="col-12">
            <div class="charity-news_head">{{= title}}</div>

            <div class="wrap-row">
                <div class="col-3 col-push-9">
                    <div class="charity-nav js-news-tags"></div>
                    {{if banner}}
                    <div class="sidebar-banner-js"></div>
                    {{/if}}
                </div>
                <div class="col-9 col-pull-3 js-news-list"></div>
            </div>
        </div>
    </div>

</script>

<script id="charity-news-tags-block-template" type="text/x-jquery-template">
    <div class="charity-nav_head">
        {{= headingText}}
    </div>
    <div class="js-charity-news-tag-list"></div>
</script>

<script id="charity-news-tag-item-template" type="text/x-jquery-template">
    <a href="#" data-tag-id="{{= tagId}}" class="charity-nav_link">{{= nameRus}}</a>
</script>

<script id="charity-news-pager-template" type="text/x-jquery-template">
    <div class="charity-show-more">
        <a class="charity-show-more_link" href="#">Показать еще новости</a>
    </div>
</script>

<script id="charity-news-modal-popup" type="text/x-jquery-template">
    <div class="modal modal-feed-post modal-lg js-modal-overlapped-view">
        <div class="modal-body">
            <a class="close">×</a>
            <div class="feed-post_author_wrap">
                <div class="feed-post_author_ava">
                    <img src="https://s1.planeta.ru/i/1326b1/1469195072367_renamed.jpg">
                </div>
                <div class="feed-post_author_name">Planeta.ru</div>
            </div>

            <div class="feed_cont feed_cont__post">
                <div class="feed_post">
                    <div class="feed_name">{{= title}}</div>
                    <div class="feed_meta">
                        <span class="feed_meta_i">{{= DateUtils.formatDate(timeAdded)}}</span>
                    </div>
                    <div class="feed_text">
                        {{html postText}}
                    </div>
                </div>
                <div class="feed_sharing"></div>
            </div>
        </div>
    </div>
</script>

<script data-skip-comment-template="1" id="charity-events-item-template" type="text/x-jquery-template">
    <div class="charity-events_link" style="cursor: pointer;">
        <span class="charity-events_cont">
            <span class="charity-events_meta">
                <span class="charity-events_date">
                    {{= DateUtils.formatDateSimple(eventDate,"DD MMMM YYYY")}}
                </span>
                <span class="charity-events_place" style="font-weight: 700; color: #254959;">
                    {{= eventLocation}}
                </span>
            </span>

            <span class="charity-events_name">
                {{= title}}
            </span>

            {{if first}}
                {{if eventType == "ABOUT"}}
                    <span class="charity-events_descr">
                        {{html headingText}}
                    </span>

                    <span class="charity-events_action">
                        <span class="charity-events_action-btn">Подробнее</span>
                    </span>
                {{/if}}
            {{/if}}
        </span>

        <span class="charity-events_img">
            <span class="charity-events_img-cover" style="background-image:url('{{= ImageUtils.getThumbnailUrl(imageUrl, ImageUtils.CHARITY_SCHOOL_EVENTS_ITEM, ImageType.PHOTO, 580, 276, true, false)}}')"></span>
            <img src="{{= ImageUtils.getThumbnailUrl(imageUrl, ImageUtils.CHARITY_SCHOOL_EVENTS_ITEM, ImageType.PHOTO, 580, 276, true, false)}}"> <!-- width="580" height="276" -->
        </span>
    </div>
</script>

<script data-skip-comment-template="1" id="charity-events-template" type="text/x-jquery-template">
    <div class="wrap">
        <div class="col-12">
            <div class="charity-events_head">
                {{= title}}
            </div>

            <div class="js-school-events-list"></div>
        </div>
    </div>
</script>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ include file="/WEB-INF/jsp/includes/generated/doctype-header.jsp" %>
<%@taglib prefix="p" uri="http://planeta.ru/tags" %>
<c:set var="imagesDir" value="//${hf:getStaticBaseUrl('')}/images"/>

<head>
    <%@include file="/WEB-INF/jsp/includes/generated/stat-counters-async.jsp"%>
    <%@ include file="/WEB-INF/jsp/includes/head.jsp" %>
    <%@ include file="/WEB-INF/jsp/includes/generated/main-page-meta-header.jsp" %>
    <%@ include file="/WEB-INF/jsp/includes/metas-welcome.jsp" %>
    <p:script src="planeta-charity-search.js" />
    <%@ include file="/WEB-INF/jsp/includes/news-js.jsp" %>

    <script type="text/javascript">
        $(document).ready(function() {
            var limit = 2;

            var eventsCollection = new News.Models.Posts([], {
                url: '/api/util/charity-about-posts.json',
                limit: limit
            });

            eventsCollection.fetch().done(function(response) {
                eventsCollection.offset = response.length;
                var eventView = new Charity.Views.Events({
                    el: '.js-events',
                    collection: eventsCollection,
                    title: 'События'
                });
                eventView.render();
            });
        });
    </script>
</head>

<body class="grid-1200 charity-page">

<div id="modal-charity-news-popup" class="modal-charity-dialog modal-dialog" style="display: none; z-index: 510;"></div>

<%@ include file="/WEB-INF/jsp/includes/generated/irma/header.jsp" %>
<%@ include file="/WEB-INF/jsp/includes/generated/irma/main-nav.jsp" %>

<div id="global-container" class="hidden">
    <div id="main-container" class="wrap-container">
        <div id="center-container">
            <%@ include file="/WEB-INF/jsp/charity/charity-carousel.jsp" %>
            <%@ include file="/WEB-INF/jsp/charity/charity-menu.jsp" %>
            <%@ include file="/WEB-INF/jsp/charity/charity-stats.jsp" %>
            <%@ include file="/WEB-INF/jsp/charity/charity-social-politics.jsp" %>
            <%@ include file="/WEB-INF/jsp/charity/charity-team.jsp" %>

            <div class="js-events charity-events charity-events__rich"></div>

            <!--TODO: finish, when we'll have reports-->
            <!--
            <div class="charity-reports">
                <div class="wrap">
                    <div class="col-12">

                        <div class="charity-reports_head">
                            Отчеты
                        </div>

                        <div class="charity-reports_list">
                            <div class="charity-reports_i">
                                <div class="charity-reports_ico"></div>
                                <div class="charity-reports_name">
                                    Отчет за 2015 год
                                </div>
                                <div class="charity-reports_link">
                                    <a href="#">Смотреть</a>
                                </div>
                                <div class="charity-reports_size">
                                    30.3 КБ
                                </div>
                            </div>

                            <div class="charity-reports_i">
                                <div class="charity-reports_ico"></div>
                                <div class="charity-reports_name">
                                    Отчет за 2014 год
                                </div>
                                <div class="charity-reports_link">
                                    <a href="#">Смотреть</a>
                                </div>
                                <div class="charity-reports_size">
                                    30.3 КБ
                                </div>
                            </div>

                            <div class="charity-reports_i">
                                <div class="charity-reports_ico"></div>
                                <div class="charity-reports_name">
                                    Отчет за 2013 год
                                </div>
                                <div class="charity-reports_link">
                                    <a href="#">Смотреть</a>
                                </div>
                                <div class="charity-reports_size">
                                    30.3 КБ
                                </div>
                            </div>

                            <div class="charity-reports_i">
                                <div class="charity-reports_ico"></div>
                                <div class="charity-reports_name">
                                    Отчет за 2012 год
                                </div>
                                <div class="charity-reports_link">
                                    <a href="#">Смотреть</a>
                                </div>
                                <div class="charity-reports_size">
                                    30.3 КБ
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            -->

            <%--<div class="charity-faq">--%>
                <%--<div class="wrap">--%>
                    <%--<div class="col-12">--%>

                        <%--<div class="charity-faq_head">--%>
                            <%--Вопросы и ответы--%>
                        <%--</div>--%>

                        <%--<div class="charity-faq_text">--%>
                            <%--Тут вы найдете все ответы на свои вопросы--%>
                        <%--</div>--%>


                        <%--<div class="charity-faq_action">--%>
                            <%--<a href="https://${properties["application.host"]}/faq" class="btn btn-primary">Пойти почитать FAQ</a>--%>
                        <%--</div>--%>

                    <%--</div>--%>
                <%--</div>--%>
            <%--</div>--%>

            <%@ include file="/WEB-INF/jsp/charity/charity-partners.jsp" %>
        </div>
    </div>
</div>

<div class="modal-backdrop modal-backdrop-charity" style="display: none"></div>

<%@ include file="/WEB-INF/jsp/includes/footer.jsp" %>
<div id="registration-modal-form"></div>
</body>
</html>


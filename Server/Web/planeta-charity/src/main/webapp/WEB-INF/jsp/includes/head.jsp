<%@ page contentType="text/html; charset=UTF-8" language="java" pageEncoding="UTF-8"%>

<%@ include file="/WEB-INF/jsp/includes/common-css.jsp" %>
<%@ include file="/WEB-INF/jsp/includes/generated/common-templates.jsp" %>
<%@ include file="/WEB-INF/jsp/includes/planeta-js.jsp" %>
<%@include file="/WEB-INF/jsp/includes/generated/common-favicon.jsp" %>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">

<style>
    .modal-backdrop-charity  {
        z-index: 500;
    }

    .modal-charity-dialog {
        display: block;
        position: fixed;
        left: 0;
        top: 0;
        width: 100%;
        height: 100%;
        text-align: center;
        z-index: 1050;
        white-space: nowrap;
        overflow: auto;
    }
</style>

<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<script id="charity-tags-template" type="text/x-jquery-template">
    <div class="charity-nav_head">
        {{= headingText}}
    </div>
    <div class="js-charity-tag-list">
    </div>
</script>

<script id="city-autocompleter-template" type="text/x-jquery-template">
    <span class="charity-nav_link js-other-city" data-cities="{{= cityId}}">{{= name}}</span>
    <div class="charity-nav_city hide">
        <input class="form-control control-lg" name="cityNameRus" id="cityNameRus" data-planeta-ui="city" type="text"
               placeholder="Введите название" value="{{= cityNameRus}}">
        <input class="hide" id="cityId" name="cityId" value="{{= cityId}}">
        <input class="hide" id="countryId" name="countryId" data-selected-country-id="1" value="1">
        <input class="hide" id="countryNameRus" name="countryNameRus" value="Россия">
    </div>
</script>

<script id="charity-city-item-template" type="text/x-jquery-template">
    {{if (extended)}}
        {{tmpl '#city-autocompleter-template'}}
    {{else}}
        <a class="charity-nav_link" data-cities="{{= cityId}}">{{= name}}</a>
    {{/if}}
</script>


<script id="charity-tag-item-template" type="text/x-jquery-template">
    <a class="charity-nav_link" data-categories="{{= mnemonicName}}">{{= name}}</a>
</script>


<script id="charity-city-template" type="text/x-jquery-template">
    <div class="charity-nav_block">
        <div class="charity-nav_head">
            {{= headingText}}
        </div>
        <div class="js-city-list">
        </div>
        <div class="js-city-autocompleter">
        </div>
    </div>
</script>

<script id="charity-query-template" type="text/x-jquery-template">
    <div class="charity-nav_block">
        <div class="charity-nav_head">
            Поиск по проектам
        </div>
        <div class="charity-nav_search">
            <span class="s-icon s-icon-search"></span>
            <input type="text" class="form-control" id="query" placeholder="Введите текст...">
        </div>
    </div>
</script>

<script id="charity-maincontainer-template" type="text/x-jquery-template">
    <div class="wrap">
        <div class="col-3 col-push-9">

            <div class="charity-main-sidebar">
                <div class="charity-main-sidebar_overlay"></div>
                <div class="charity-main-sidebar_wrap">

                    <div class="charity-main-sidebar_toggle">
                        <div class="charity-main-sidebar_toggle-ico"></div>
                    </div>

                    <div class="charity-main-sidebar_cont">

                        <div class="charity-main-sidebar_in">


                            <div class="charity-nav js-query-string">
                            </div>
                            <div class="charity-nav">
                                <div class="charity-nav_block js-special">
                                </div>
                                <div class="charity-nav_block js-tags">
                                </div>
                            </div>

                            <div class="charity-nav js-city">
                            </div>

                            <div class="sidebar-banner-js">
                            </div>


                        </div>

                    </div>
                </div>
            </div>

        </div>

        <div class="col-9 col-pull-3 charity-projects-maincontainer">
            <div class="js-results">
            </div>
        </div>
    </div>
</script>

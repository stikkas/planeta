<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="p" uri="http://planeta.ru/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<script type="text/javascript">
    $(function () {
        var projectItems = $('.project-wide-carousel_i');
        var nameItems = $('.project-wide-carousel-names_title');

        projectItems.each(function (i, k) {
            $(k).attr('data-id', i);
        });

        nameItems.each(function (i, k) {
            $(k).attr('data-id', i);
        });
        nameItems.eq(0).addClass('active');

        var carouselOptions = {
            animateOut: 'fadeOut',
            lazyLoad: true,
            autoplay: true,
            loop: true,
            items: 1,
            slideBy: 1,
            nav: true,
            dots: false,
            margin: 20,
            mouseDrag: false,
            smartSpeed: 600
        };

        if (window.isMobileDev) {
            carouselOptions = {
                animateOut: false,
                lazyLoad: true,
                autoplay: false,
                loop: true,
                items: 1,
                slideBy: 1,
                nav: true,
                dots: false,
                margin: 10,
                mouseDrag: false,
                smartSpeed: 600,
                stagePadding: 50,
                responsive: {
                    568: {
                        stagePadding: 85
                    },
                    667: {
                        stagePadding: 90
                    },
                    736: {
                        stagePadding: 92
                    }
                }
            }
        }

        var project = $('.project-wide-carousel')
                .addClass('owl-carousel')
                .owlCarousel(carouselOptions);

        if ( window.isMobileDev ) {
            window.mobileCssLoadedDfd.done(function () {
                project.trigger('refresh.owl.carousel');
            });
        }

        nameItems.on('click', function () {
            var item = $(this);
            var id = item.data('id');

            if ( item.hasClass('active') ) return;

            project.trigger('to.owl.carousel', [id, 400]);
        });

        project.on('changed.owl.carousel', function() {
            setTimeout(function () {
                var item = $('.owl-item.active:eq(0) .project-wide-carousel_i', project);
                var id = item.data('id');

                $('.project-wide-carousel-names_title.active').removeClass('active');
                $('.project-wide-carousel-names_title').eq(id).addClass('active');
            });
        });
    });
</script>

<div class="charity-carousel">
    <div class="wrap">
        <div class="col-12">
            <div class="project-wide-carousel-wrap">
                <div class="project-wide-carousel">
                    <c:forEach items="${promoCampaigns}" var="campaign">
                        <div class="project-wide-carousel_i">
                            <div class="project-wide-carousel_cover">
                                <a href="http://${properties['projects.application.host']}/campaigns/${campaign.webCampaignAlias}" class="project-wide-carousel_cover-link">
                                    <img src="${hf:getProxyThumbnailUrl(campaign.imageUrl, "ORIGINAL", "PHOTO", 600, 366, true, false)}">
                                </a>
                            </div>

                            <div class="project-wide-carousel_cont">
                                <div class="project-wide-carousel_descr">
                                    <div class="project-wide-carousel_name">
                                        <a href="#">${campaign.name}</a>
                                    </div>
                                    <div class="project-wide-carousel_text">
                                        ${campaign.shortDescriptionHtml}
                                    </div>
                                </div>
                                <div class="project-wide-carousel_foot">
                                    <div class="project-wide-carousel_bar">
                                        <div class="project-wide-carousel_progress" style="width: ${campaign.progress}%;"></div>
                                    </div>

                                    <div class="project-wide-carousel_info">
                                        <div class="project-wide-carousel_info-i">
                                            <div class="project-wide-carousel_info-lbl">
                                                Собрано средств
                                            </div>
                                            <div class="project-wide-carousel_info-val">
                                                <fmt:formatNumber value="${campaign.collectedAmount}"/> <span class="b-rub">Р</span>
                                            </div>
                                        </div>
                                        <div class="project-wide-carousel_info-i">
                                            <div class="project-wide-carousel_info-lbl">
                                                Цель проекта
                                            </div>
                                            <div class="project-wide-carousel_info-val">
                                                <fmt:formatNumber value="${campaign.targetAmount}"/> <span class="b-rub">Р</span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="project-wide-carousel_action">
                                        <c:if test="${not empty campaign.webCampaignAlias}">
                                            <a href="http://${properties['projects.application.host']}/campaigns/${campaign.webCampaignAlias}" class="btn btn-primary">Помочь проекту</a>
                                        </c:if>
                                        <c:if test="${empty campaign.webCampaignAlias}">
                                            <a href="http://${properties['projects.application.host']}/campaigns/${campaign.campaignId}" class="btn btn-primary">Помочь проекту</a>
                                        </c:if>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </c:forEach>
                </div>

                <div class="project-wide-carousel-names">
                    <c:forEach items="${promoCampaigns}" var="campaign">
                        <div class="project-wide-carousel-names_i">
                            <div class="project-wide-carousel-names_title">
                                ${campaign.name}
                            </div>
                        </div>
                    </c:forEach>
                </div>
            </div>
        </div>
    </div>
</div>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="p" uri="http://planeta.ru/tags" %>

<script type="text/javascript">
    $(function() {
        function charityMenuMobile() {
            if ( !window.isMobileDev ) return;

            $(document).on('click', '.charity-menu_i.active .charity-menu_link', function (e) {
                e.preventDefault();

                $('.charity-menu').toggleClass('open');
            });
        }

        charityMenuMobile();


        var menuItem = location.pathname.substring(1, location.pathname.length);
        if(menuItem == '') {
            $('.js-charity-campaigns').addClass('active');
        } else {
            $('.js-' + menuItem).addClass('active');
        }
    });
</script>

<div class="charity-menu">
    <div class="wrap">
        <div class="col-12">
            <div class="charity-menu_list">
                <div class="charity-menu_i js-charity-campaigns">
                    <a href="https://${properties['charity.application.host']}/" class="charity-menu_link">
                        <span class="charity-menu_ico">
                            <span class="s-charity-menu-project"></span>
                        </span>
                        <span class="charity-menu_name">
                            Благотворительные проекты
                        </span>
                    </a>
                </div>

                <div class="charity-menu_i js-about">
                    <a href="https://${properties['charity.application.host']}/about" class="charity-menu_link">
                        <span class="charity-menu_ico">
                            <span class="s-charity-menu-about"></span>
                        </span>
                        <span class="charity-menu_name">
                            О нас
                        </span>
                    </a>
                </div>

                <div class="charity-menu_i js-news">
                    <a href="https://${properties['charity.application.host']}/news" class="charity-menu_link">
                        <span class="charity-menu_ico">
                            <span class="s-charity-menu-news"></span>
                        </span>
                        <span class="charity-menu_name">
                            Новости
                        </span>
                    </a>
                </div>

                <div class="charity-menu_i js-school">
                    <a href="https://${properties['charity.application.host']}/school" class="charity-menu_link">
                        <span class="charity-menu_ico">
                            <span class="s-charity-menu-school"></span>
                        </span>
                        <span class="charity-menu_name">
                            Обучение
                        </span>
                    </a>
                </div>

                <div class="charity-menu_i">
                    <a href="https://${properties['shop.application.host']}/products/search.html?productTagsMnemonics=CHARITY" class="charity-menu_link">
                        <span class="charity-menu_ico">
                            <span class="s-charity-menu-shop"></span>
                        </span>
                        <span class="charity-menu_name">
                            Магазин
                        </span>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>

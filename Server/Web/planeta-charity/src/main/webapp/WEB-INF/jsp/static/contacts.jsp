<%@ include file="/WEB-INF/jsp/includes/generated/doctype-header.jsp" %>
<head>
    <%@include file="/WEB-INF/jsp/includes/generated/stat-counters-async.jsp"%>
    <%@ include file="/WEB-INF/jsp/includes/head.jsp" %>
    <%@ include file="/WEB-INF/jsp/includes/generated/main-page-meta-header.jsp" %>
    <%@ include file="/WEB-INF/jsp/includes/metas-welcome.jsp" %>
    <link type="text/css" rel="stylesheet" href="//${hf:getStaticBaseUrl("")}/css-generated/school.css"/>
    <link type="text/css" rel="stylesheet" href="//${hf:getStaticBaseUrl("")}/css-generated/common.css" />
</head>

<body class="school-page">
<%@ include file="/WEB-INF/jsp/includes/generated/header.jsp" %>

<div id="global-container" class="hidden">
    <div id="main-container" class="wrap-container">
        <div id="center-container">
            <%@ include file="/WEB-INF/jsp/includes/school-header.jsp" %>

            <div class="school-contacts">
                <div class="wrap">
                    <div class="col-12">

                        <div class="school-contacts_head school-page-head">
                            Контакты
                        </div>



                        <div class="school-contacts_box">
                            <div class="school-contacts_ico"></div>
                            <div class="school-contacts_cont">
                                <div class="school-contacts_text">

                                    <div class="school-footer_info">
                                        <div class="school-footer_info_i">
                                            <span class="school-footer_info_ico"><span class="s-school-mail"></span></span>
                                            <a class="school-footer_info_val" href="mailto:school@planeta.ru">school@planeta.ru</a>
                                        </div>

                                        <div class="school-footer_info_i">
                                            <span class="school-footer_info_ico"><span class="s-school-phone"></span></span>
                                            <span class="school-footer_info_val">+7 (495) 181-05-05</span>
                                        </div>
                                    </div>

                                </div>
                                <div class="school-contacts_btn">
                                    <a href="https://files.planeta.ru/school/cf_school_presentation.pdf" class="school-btn school-btn-primary school-btn-lg">Смотреть презентацию <span class="s-school-tv"></span></a>
                                </div>
                                <div class="school-contacts_add">
                                    pdf (3 Mb)
                                </div>
                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<%@ include file="/WEB-INF/jsp/includes/footer.jsp" %>
</body>
</html>
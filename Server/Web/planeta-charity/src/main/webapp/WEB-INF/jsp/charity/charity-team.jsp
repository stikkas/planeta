<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="p" uri="http://planeta.ru/tags" %>

<div class="charity-team">
    <div class="wrap">
        <div class="col-12">
            <div class="charity-team_head">
                Команда
            </div>

            <div class="charity-team_list">
                <c:forEach items="${members}" var="member">
                    <div class="charity-team_i">
                        <img src="${member.imageUrl}">
                    </div>
                </c:forEach>
            </div>

            <div class="charity-team-descr">
                <div class="charity-team-descr_list" tabindex="0">
                    <c:forEach items="${members}" var="member">
                        <div class="charity-team-descr_i" data-id="0">
                            <div class="charity-team-descr_wrap">
                                <div class="charity-team-descr_cont">
                                    <div class="charity-team-descr_name">
                                            ${member.memberName}
                                    </div>
                                    <div class="charity-team-descr_quote">
                                            ${member.description}.
                                    </div>
                                </div>

                                <div class="charity-team-descr_meta">
                                    <div class="charity-team-descr_meta-i">
                                        <span class="s-team-meta-phone"></span>
                                            ${member.phoneNumber}
                                    </div>

                                    <div class="charity-team-descr_meta-i">
                                        <span class="s-team-meta-email"></span>
                                            ${member.email}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </c:forEach>
                </div>
            </div>

            <script type="text/javascript">
                $(function () {
                    var teamItems = $('.charity-team_i');
                    var descrItems = $('.charity-team-descr_i');

                    teamItems.each(function (i, k) {
                        $(k).attr('data-id', i);
                    });

                    descrItems.each(function (i, k) {
                        $(k).attr('data-id', i);
                    });

                    var team = $('.charity-team_list').addClass('owl-carousel').owlCarousel({
                        lazyLoad: true,
                        autoplay: false,
                        autoplayTimeout: 5000,
                        autoplayHoverPause: true,
                        loop: true,
                        items: 4,
                        slideBy: 1,
                        nav: true,
                        dots: false,
                        margin: 20,
                        mouseDrag: false,
                        smartSpeed: 600,
                        center: true,
                        autoWidth: true,
                        responsive: {
                            768: {
                                center: false,
                                autoWidth: false
                            }
                        }
                    });

                    var descr = $('.charity-team-descr_list').addClass('owl-carousel').owlCarousel({
                        animateOut: false,
                        animateIn:'fadeIn',
                        lazyLoad: true,
                        autoplay: false,
                        autoplayTimeout: 5000,
                        autoplayHoverPause: true,
                        loop: false,
                        items: 1,
                        slideBy: 1,
                        nav: false,
                        dots: false,
                        margin: 0,
                        mouseDrag: false,
                        smartSpeed: 600,
                        autoHeight: true,
                        touchDrag: false,
                        responsive: {
                            768: {
                                autoHeight: false
                            }
                        }
                    });

                    if ( window.isMobileDev ) {
                        window.mobileCssLoadedDfd.done(function () {
                            team.trigger('refresh.owl.carousel');
                            descr.trigger('refresh.owl.carousel');
                        });
                    }

                    $(document).on( 'click', '.charity-team_i', function() {
                        var item = $(this);
                        var id = item.data('id');

                        team.trigger('to.owl.carousel', [id, 400]);
                    });

                    team.on('changed.owl.carousel', function() {
                        setTimeout(function () {
                            var item = $('.owl-item.active:eq(0) .charity-team_i', team);
                            var id = item.data('id');

                            descr.trigger('to.owl.carousel', [id, 400]);
                        });
                    });

                    Banner.init('${mainAppUrl}', workspace.appModel.get('myProfile').get('isAuthor'));
                });
            </script>
        </div>
    </div>
</div>

<div class="js-projects-welcome-banner-container-new"></div>

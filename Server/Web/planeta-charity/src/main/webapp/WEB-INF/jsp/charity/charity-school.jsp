<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ include file="/WEB-INF/jsp/includes/generated/doctype-header.jsp" %>
<%@ taglib prefix="p" uri="http://planeta.ru/tags" %>
<c:set var="imagesDir" value="//${hf:getStaticBaseUrl('')}/images"/>

<head>
    <script type="text/javascript">
        var workspace = {};
    </script>

    <%@include file="/WEB-INF/jsp/includes/generated/stat-counters-async.jsp"%>
    <%@ include file="/WEB-INF/jsp/includes/head.jsp" %>
    <%@ include file="/WEB-INF/jsp/includes/generated/main-page-meta-header.jsp" %>
    <%@ include file="/WEB-INF/jsp/includes/metas-welcome.jsp" %>
    <%@ include file="/WEB-INF/jsp/includes/news-js.jsp" %>
    <p:script src="planeta-charity-search.js" />
    <p:script src="charity.js"/>
    <%@ include file="/WEB-INF/jsp/includes/charity-order-event-js.jsp" %>

    <script type="text/javascript">
        var School = {
            Models: {},
            Views: {}
        };

        School.Views.ResultsNotFound = BaseView.extend({
            queryText: "",
            template: "#school-library-files-not-found-template",

            modelJSON: function () {
                var json = this.model ? this.model.toJSON() : {};
                json.queryText = this.queryText;
                json.length = this.collection.length;
                return json;
            },

            construct: function (options) {
                if (options.queryText) {
                    this.queryText = options.queryText
                }
                /*this.collection = options.collection;*/
            }
        });

        School.Views.Search = BaseView.extend({
            placeholder: "Поиск по библиотеке",
            template: "#school-library-filter-search-template",
            className:'school-library-files-filter-search',
            tagName:'div',

            events: {
                'input #searchString':'changeSearchString',
                'keyup #searchString':'changeSearchString',
                'paste #searchString':'changeSearchString',
                'change #searchString':'changeSearchString'
            },

            modelJSON: function() {
                var json = this.model ? this.model.toJSON() : {};
                json.placeholder = this.placeholder;
                return json;
            },

            construct: function (options) {
                if(options.placeholder){
                    this.placeholder = options.placeholder
                }
                this.collection = options.collection;
                this.notFoundBox = options.notFoundBox;
            },

            changeSearchString: _.debounce(function () {
                $('#school-library-theme-container').empty();

                var searchQuery = $(this.el).find('#searchString').val();
                this.collection.data.query = searchQuery;
                if(this.oldQuery==this.collection.data.query) return;
                this.oldQuery = this.collection.data.query;

                var self = this;
                this.collection.load().done(function() {
                    if (self.notFoundBox) {
                        self.notFoundBox.queryText = '«' + searchQuery + '»';
                        self.notFoundBox.render();
                    }
                    if ( !window.isMobileDev ) {
                        $('.charity-biblio_list').columnize({ columns: 3 });
                    }
                });
            }, 500)
        });

        School.Models.BaseSchoolMainCollection = BaseCollection.extend({
            url: '/charity/search-charity-library-files.json',
            'query': '',
            'sortBy': 1,
            limit: 100,
            data: {
            },

            parse: function (result) {
                if (!result) return {success: false};
                if (result.length === 0) return [];

                return result;
            }
        });

        School.Views.CharityLibraryFileItem = BaseView.extend({
            template: "#school-library-file-item-template",
            className: 'charity-biblio-list_i'
        });

        School.Views.CharityLibraryFileList = DefaultContentScrollListView.extend({
            itemViewType: School.Views.CharityLibraryFileItem,
            className: 'charity-biblio-list',

            onAdd: function (model) {
                return DefaultContentScrollListView.prototype.onAdd.apply(this, arguments);
            }
        });

        School.Views.CharityLibraryFileThemeList = BaseView.extend({
            template: "#school-library-theme-template",
            className: 'charity-biblio_i dontsplit',

            construct: function () {
                this.addChildAtElement(this.$el, new School.Views.CharityLibraryFileList({
                    collection: new BaseCollection(this.model.get('libraryFiles'))
                }));
            },

            onAdd: function (model) {
                return DefaultContentScrollListView.prototype.onAdd.apply(this, arguments);
            }
        });

        School.Views.FileList = DefaultContentScrollListView.extend({
            el: '.school-library-theme-container',
            className: 'charity-biblio_list',
            itemViewType: School.Views.CharityLibraryFileThemeList,
            onAdd: function (model) {
                return DefaultContentScrollListView.prototype.onAdd.apply(this, arguments);
            }

        });

        $(document).ready(function () {
            var limitSchoolEvents = 4;

            var schoolEventsCollection = new News.Models.Posts([], {
                url: '/api/util/charity-school-events-list.json',
                limit: limitSchoolEvents
            });

            schoolEventsCollection.fetch({
                success: function(response) {
                    if (response.length) {
                        schoolEventsCollection.offset = response.length;
                        var schoolEventView = new Charity.Views.Events({
                            el: '.js-school-events',
                            collection: schoolEventsCollection,
                            title: 'Анонсы мероприятий'
                        });
                        schoolEventView.render();
                    }
                }
            });

            //------------------------------------------------------
            var filesView = new School.Views.FileList({
                el: '#school-library-theme-container',
                collection: new School.Models.BaseSchoolMainCollection([])
            });
            filesView.render();
            filesView.collection.load();
            //------------------------------------------------- tabs logic -------------------------------------
            var notFoundBox = new School.Views.ResultsNotFound({
                el: '#school-library-files-not-found-container',
                collection: filesView.collection
            });

            var searchBox = new School.Views.Search({
                el: '.school-library-files-filter-search',
                collection: filesView.collection,
                notFoundBox: notFoundBox
            });
            searchBox.render();
            searchBox.collection = filesView.collection;
        });
    </script>
</head>

<body class="grid-1200 charity-page">

<%@ include file="/WEB-INF/jsp/includes/generated/irma/header.jsp" %>
<%@ include file="/WEB-INF/jsp/includes/generated/irma/main-nav.jsp" %>

<div id="global-container" class="hidden">
    <div id="main-container" class="wrap-container">
        <div id="center-container">
            <%@ include file="/WEB-INF/jsp/charity/charity-carousel.jsp" %>
            <%@ include file="/WEB-INF/jsp/charity/charity-menu.jsp" %>

            <div class="charity-events charity-events__top">
                <div class="js-school-events"></div>
            </div>


            <div class="charity-school">
                <div class="wrap">
                    <div class="col-12">
                        <div class="charity-school_head">
                            Мы предлагаем
                        </div>

                        <div class="wrap">
                            <div class="col-7">
                                <div class="charity-school_cont">
                                    <div class="charity-school_title">
                                        Школа краудфандинга для&nbsp;нко
                                    </div>

                                    <div class="charity-school_text">
                                        Образовательное направление Planeta.ru, краудфандинговой платформы № 1 в России.
                                        <br><br>
                                        Мы проводим мастер-классы, вебинары, практические интенсивные тренинги в разных городах России. Мы расскажем вам, что такое краудфандинг в теории, и, как он работает на практике, ведь для многих НКО краудфандинг является эффективным инструментом коммуникации и привлечения средств.
                                    </div>

                                    <div class="charity-school_text-2">
                                        Хотите, чтобы мы провели для Вас лекцию или вебинар?
                                        Условия и форматы мероприятий в нашей <a href="https://s3.planeta.ru/f/589/charity_presentation.pdf">презентации</a>.
                                    </div>
                                </div>
                            </div>


                            <div class="col-5">
                                <div class="charity-school_right">
                                    <div class="charity-school_img">
                                        <img src="//${hf:getStaticBaseUrl("")}/images/charity/charity-school-img.png">
                                    </div>

                                    <div class="charity-school_action">
                                        <span class="btn btn-primary js-order-event-button" data-target="#modal-charity-form" data-toggle="modal">Заказать свое мероприятие</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="school-book charity-school-book">
                <div class="school-book_wrap">
                    <div class="wrap">
                        <div class="school-book_block col-12">

                            <div class="school-book_img">
                                <img src="//${hf:getStaticBaseUrl("")}/images/charity/book.jpg">
                            </div>

                            <div class="school-book_text">
                                <div class="school-book_head">
                                    <a href="https://s2.planeta.ru/f/390/prakticheskoe_posobie_po_crowdfundingu_planeta.ru.pdf" target="_blank">Скачайте</a> наше пособие по&nbsp;краудфандингу
                                </div>

                                <div class="school-book_download">
                                    <a href="https://s2.planeta.ru/f/390/prakticheskoe_posobie_po_crowdfundingu_planeta.ru.pdf" class="school-book_download-link" target="_blank">
                                        <span class="school-book_download-link-text">Скачать</span>
                                        <br>
                                        pdf, 55 мб
                                    </a>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

            <div class="charity-webinars">
                <div class="wrap">
                    <div class="col-12">

                        <div class="charity-webinars_head">
                            Прошедшие вебинары
                        </div>


                        <div class="charity-webinars_list">
                            <c:forEach items="${recentWebinarsList}" var="webinar">
                                <div class="charity-webinars_i">
                                    <div class="charity-webinars_name">
                                        <a href="https://${properties['tv.application.host']}/broadcast/${webinar.broadcastId}" target="_blank">${webinar.name}</a>
                                    </div>

                                    <div class="charity-webinars_cover">
                                        <img src="${hf:getProxyThumbnailUrl(webinar.imageUrl, "ORIGINAL", "PHOTO", 380, 214, true, false)}">
                                        <a href="https://${properties['tv.application.host']}/broadcast/${webinar.broadcastId}" class="charity-webinars_play" target="_blank">
                                            <span class="charity-webinars_play-ico"></span>
                                        </a>
                                    </div>

                                    <div class="charity-webinars_text">
                                        ${webinar.descriptionHtml}
                                    </div>
                                </div>
                            </c:forEach>
                        </div>


                        <div class="charity-webinars_more">
                            <div class="charity-show-more">
                                <a class="charity-show-more_link" href="https://${properties['tv.application.host']}/school" target="_blank">Перейти в архив вебинаров</a>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

            <c:if test="${isAdmin}">
            <div class="charity-biblio">
                <div class="wrap">
                    <div class="col-12">
                        <div class="charity-biblio_head">
                            Библиотека
                        </div>


                        <div class="school-library-files-filter-search"></div>

                        <div id="school-library-theme-container" class="school-library-theme-container charity-biblio_list"></div>

                        <script src="//${hf:getStaticBaseUrl('')}/js/libs/jquery.columnizer.min.js"></script>

                        <div id="school-library-files-not-found-container"></div>
                    </div>
                </div>
            </div>
            </c:if>

            <div class="charity-rich-more">
                <a href="https://${properties["application.host"]}/funding-rules" class="btn btn-charity-create">Создать благотворительный проект</a>
            </div>


        </div>
    </div>
    <div id="modal-charity-news-popup" class="modal-charity-dialog modal-dialog in" style="display: none; z-index: 510;"></div>
</div>

<div class="modal-backdrop-charity" style="display: none"></div>

<%@ include file="/WEB-INF/jsp/includes/footer.jsp" %>
<div id="registration-modal-form"></div>
</body>
</html>
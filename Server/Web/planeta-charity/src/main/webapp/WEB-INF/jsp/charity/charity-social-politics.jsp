<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="p" uri="http://planeta.ru/tags" %>

<div class="charity-social-politics">
    <div class="wrap">
        <div class="col-12">
            <div class="charity-social-politics_head">
                Социальная политика Planeta.ru
            </div>

            <div class="charity-social-politics_block">
                <div class="charity-social-politics_ava">
                    <img src="${imagesDir}/charity/fedor.jpg">
                </div>

                <div class="charity-social-politics_cont">
                    <div class="charity-social-politics_name">
                        Федор Мурачковский
                    </div>

                    <div class="charity-social-politics_prof">
                        Сооснователь и генеральный директор Planeta.ru
                    </div>

                    <div class="charity-social-politics_quote">
                        Категорию &laquo;благотворительность&raquo; на&nbsp;&laquo;Планете&raquo; мы&nbsp;превратили
                        в&nbsp;большое профессиональное &laquo;крыло&raquo;, социальную программу &laquo;Планета добрых
                        людей&raquo;. В&nbsp;благотворительном краудфандинге есть определенная специфика, и&nbsp;теперь
                        мы&nbsp;сможем работать с&nbsp;НКО и&nbsp;социальными предпринимателям более эффективно,
                        а&nbsp;проекты получат больше возможностей для сбора средств, обучения и&nbsp;роста.
                    </div>
                </div>

                <div class="charity-social-politics_docs">
                    <div class="charity-social-docs">
                        <a class="charity-social-docs_i" href="https://s3.planeta.ru/f/53d/social_politics.pdf">
                            <span class="charity-social-docs_ico"></span>
                            <span class="charity-social-docs_name">
                                <span class="charity-social-docs_title">Социальная политика Planeta.ru</span>
                            </span>
                            <span class="charity-social-docs_size">
                                17.9 МБ
                            </span>
                        </a>

                        <a class="charity-social-docs_i" href="https://s2.planeta.ru/f/39f/planeta+dobrykh+liudei.pdf">
                            <span class="charity-social-docs_ico"></span>
                            <span class="charity-social-docs_name">
                                <span class="charity-social-docs_title">Описание программы Planeta.ru</span>
                            </span>
                            <span class="charity-social-docs_size">
                                1.21 МБ
                            </span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ include file="/WEB-INF/jsp/includes/generated/doctype-header.jsp" %>
<%@taglib prefix="p" uri="http://planeta.ru/tags" %>
<head>
    <%@include file="/WEB-INF/jsp/includes/generated/stat-counters-async.jsp"%>
    <%@ include file="/WEB-INF/jsp/includes/head.jsp" %>
    <%@ include file="/WEB-INF/jsp/includes/generated/main-page-meta-header.jsp" %>
    <%@ include file="/WEB-INF/jsp/includes/metas-welcome.jsp" %>

    <p:script src="planeta-charity-search.js" />
    <%@ include file="/WEB-INF/jsp/includes/welcome-js.jsp" %>
    <script type="text/javascript">
        window.noLazyLoadImage = true;

        $(document).ready(function() {
            var querystring = window.location.search.replace('?','');
            var resultsModel = new Search.Models.Search({subsection: 'charity',objectId:querystring});

            resultsModel.fetch({
                success: function() {
                    var campaignTags = ${hf:toJson(campaignTags)} || [];
                    var mainView = new Charity.Views.SearchPage({
                        el: '.js-main-cont',
                        campaignTags: campaignTags,
                        resultsModel: resultsModel
                    });
                    mainView.render();
                }
            });

            var topCampaigns = ${hf:toJson(topCampaigns)} || [];

            var topView = new Charity.Views.TopProjects({
                el: '.js-top-projects',
                collection: new BaseCollection(topCampaigns)
            });
            topView.render();

            Banner.init('${mainAppUrl}', workspace.appModel.get('myProfile').get('isAuthor'));
        });

        </script>

</head>

<body class="grid-1200 charity-page">
    <%@ include file="/WEB-INF/jsp/includes/generated/irma/header.jsp" %>
    <%@include file="/WEB-INF/jsp/includes/generated/irma/main-nav.jsp" %>

    <div id="global-container" class="hidden">
        <div id="main-container" class="wrap-container">
            <div id="center-container">
                <%@ include file="/WEB-INF/jsp/charity/charity-carousel.jsp" %>
                <%@ include file="/WEB-INF/jsp/charity/charity-menu.jsp" %>

                <div class="charity-top-projects">
                    <div class="charity-main-oleg-top js-banner-container-top"></div>
                    <div class="wrap">
                        <div class="col-12">
                            <div class="project-card-list js-top-projects">
                            </div>
                        </div>
                    </div>
                </div>

                <div class="charity-main-oleg-middle js-projects-welcome-banner-container-new"></div>

                <div class="charity-main-cont js-main-cont">
                </div>
            </div>
        </div>
    </div>

    <%@ include file="/WEB-INF/jsp/includes/generated/irma/footer.jsp" %>
</body>
</html>


<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="p" uri="http://planeta.ru/tags" %>
<c:set var="imagesDir" value="//${hf:getStaticBaseUrl('')}/images"/>

<script type="text/javascript">
    $(function () {
        var charityGraph = {
            init: function () {
                var monthNames = ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'];
                var startRGB = [163, 141, 200];
                var endRGB = [133, 195, 242];
                var diffRGB = [
                    startRGB[0] - endRGB[0],
                    startRGB[1] - endRGB[1],
                    startRGB[2] - endRGB[2]
                ];

                var data = [];
                var months = [];

                $.ajax({
                    url: '/api/charity-campaigns-stats.json',
                    dataType: 'json',
                    type: 'get',
                    contentType : 'application/json',
                    success:function (response) {
                        var prevYear;
                        response.forEach(function(object) {
                            var newYear = new Date(object.date).getFullYear();
                            if(!prevYear || prevYear != newYear) {
                                data.push({
                                    year: newYear,
                                    data: []
                                });

                                months.push({
                                    year: newYear,
                                    data: []
                                });
                            }

                            data[data.length - 1].data.push(object.count);
                            months[data.length - 1].data.push(new Date(object.date).getMonth());
                            prevYear = newYear;
                        });

                        var maxValue = 0,
                                maxYearLength = 0,
                                commonCount = 0;

                        var year,
                                valsCount,
                                values,
                                value,
                                monthsNumbers;

                        for (year in data) {
                            values = data[year]['data'];
                            if(values) {
                                maxYearLength = Math.max(maxYearLength, values.length);
                                for (valsCount in values) {
                                    if (values.hasOwnProperty(valsCount)) {
                                        value = values[valsCount];
                                        maxValue = Math.max(maxValue, value);
                                        commonCount++;
                                    }
                                }
                            }
                        }

                        var barPercent = 100 / commonCount;
                        var colorPercent = 0;

                        var graph = $('.charity-stats-graph');

                        var graphList = $('<div class="charity-stats-graph_list">');
                        var barTmpl = $('<div class="charity-stats-graph_i">');
                        var barContTmpl = $('<div class="charity-stats-graph_i-cont" data-tooltip="" data-tooltip-position="top center">');

                        var yearsList = $('<div class="charity-stats-graph_years">');
                        var yearItemTmpl = $('<div class="charity-stats-graph_years-i">');

                        for ( year in data ) {
                            values = data[year]['data'];
                            monthsNumbers = months[year]['data'];

                            // draw bars
                            for (valsCount in values) {
                                if ( values.hasOwnProperty(valsCount) ) {
                                    value = values[valsCount] * 100 / maxValue;
                                    var bar = barTmpl.clone().appendTo(graphList);

                                    var singleBarContTmpl = barContTmpl.clone();
                                    var tooltip = monthNames[monthsNumbers[valsCount]] + ' ' + data[year]['year'] + ' – ' + values[valsCount] + ' проект' + StringUtils.declOfNum(values[valsCount], ['', 'а', 'ов']);
                                    singleBarContTmpl.attr('data-tooltip', tooltip);
                                    var barCont = singleBarContTmpl.appendTo(bar);

                                    var r = Math.ceil(startRGB[0] - (colorPercent * diffRGB[0] / 100));
                                    var g = Math.ceil(startRGB[1] - (colorPercent * diffRGB[1] / 100));
                                    var b = Math.ceil(startRGB[2] - (colorPercent * diffRGB[2] / 100));

                                    colorPercent = colorPercent + barPercent;

                                    bar.css({
                                        width: barPercent + '%',
                                        height: value + '%'
                                    });

                                    barCont.css({
                                        background: 'rgb(' + r + ', ' + g + ', ' + b + ')'
                                    });
                                }
                            }

                            // draw years
                            if(values) {
                                var yearLength = values.length;
                                var yearWidth = yearLength * 100 / commonCount;
                                var yearItem = yearItemTmpl.clone().appendTo(yearsList);

                                yearItem.html(data[year]['year']);
                                yearItem.css({
                                    width: yearWidth + '%'
                                });
                            }
                        }

                        graphList.appendTo(graph);
                        yearsList.appendTo(graph);
                        Tooltip.init();
                    }
                });
            }
        };
        charityGraph.init();
    });
</script>

<div class="charity-stats">
    <div class="wrap">
        <div class="col-12">
            <div class="charity-stats_img">
                <div class="charity-stats_img-cont">
                    <img src="${imagesDir}/charity/charity-stats-img.png">
                </div>
            </div>

            <div class="charity-stats_cont">
                <div class="charity-stats-value">
                    <div class="charity-stats-value_i">
                        <div class="charity-stats-value_val">
                            ${organizationCount}
                        </div>
                        <div class="charity-stats-value_lbl">
                            Организаций запустили проекты
                        </div>
                    </div>

                    <div class="charity-stats-value_i">
                        <div class="charity-stats-value_val">
                            ${successfulCount}
                        </div>
                        <div class="charity-stats-value_lbl">
                            Успешных проектов
                        </div>
                    </div>
                </div>

                <div class="charity-stats-graph">
                    <div class="charity-stats-graph_legend">
                        Динамика запуска проектов категории «благотворительность»
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
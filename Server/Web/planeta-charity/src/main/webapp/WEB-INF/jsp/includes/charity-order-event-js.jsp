<script type="text/javascript">
    $(document).ready(function () {
        if (workspace) {
            workspace.initOrderEventDialogs = function () {
                new CharityEventOrder.Views.Main({
                    model: new CharityEventOrder.Models.Main({
                    }),
                    callback: {
                        send: function(model, view) {
                            CharityEventOrder.Models
                                    .sync("${schoolHostUrl}/charity/charity-event-order-send.json", model)
                                    .done(function() {
                                        view.dispose();
                                        new CharityEventOrder.Views.Success({}).render();
                                    })
                                    .fail(function(errorMessage, fieldErrors){
                                        Form.highlightErrors(fieldErrors);
                                    });
                        }
                    }
                }).render();
            }
        }
        $('.js-order-event-button').bind("click", _.bind(workspace.initOrderEventDialogs, null));
    });
</script>
package ru.planeta.charity.controllers

enum class ActionPrefixes(private val text: String) {
    WELCOME("welcome/"),
    CHARITY("charity/"),
    STATIC("static/");

    override fun toString(): String = text
}





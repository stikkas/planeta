package ru.planeta.charity.controllers

import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.ResponseBody
import org.springframework.web.servlet.ModelAndView
import ru.planeta.api.model.ConfigurationType
import ru.planeta.api.model.biblio.Partner
import ru.planeta.api.service.campaign.CampaignService
import ru.planeta.api.service.charity.CharityService
import ru.planeta.api.service.configurations.ConfigurationService
import ru.planeta.api.service.content.BroadcastService
import ru.planeta.api.web.controllers.IAction
import ru.planeta.api.web.controllers.services.BaseControllerService
import ru.planeta.api.web.dto.TopCampaignDTO
import ru.planeta.dao.charitydb.CharityNewsTagDAO
import ru.planeta.dao.commondb.CampaignTagDAO
import ru.planeta.dao.profiledb.PostDAO
import ru.planeta.model.charity.CharityCampaignsStats
import ru.planeta.model.charity.enums.CharityEventType
import ru.planeta.model.enums.ProjectType
import ru.planeta.model.profile.Post
import java.util.*

/**
 * School web app welcome controller
 */

@Controller
class CharityWelcomeController(private val charityService: CharityService,
                               private val postDAO: PostDAO,
                               private val campaignTagDAO: CampaignTagDAO,
                               private val broadcastService: BroadcastService,
                               private val charityNewsTagDAO: CharityNewsTagDAO,
                               private val campaignService: CampaignService,
                               private val configurationService: ConfigurationService,
                               private val baseControllerService: BaseControllerService) {

    private fun createCharityDefaultModelAndView(action: IAction): ModelAndView =
            baseControllerService.createDefaultModelAndView(action, ProjectType.CHARITY)
                    .addObject("promoCampaigns", TopCampaignDTO.convert(campaignService.charityPromoCampaigns))

    @GetMapping(Urls.ROOT)
    fun welcome(): ModelAndView =
            createCharityDefaultModelAndView(Actions.WELCOME)
                    .addObject("campaignTags", campaignTagDAO.selectTagsForCharity())
                    .addObject("topCampaigns", TopCampaignDTO.convert(campaignService.charityTopCampaigns))

    @GetMapping(Urls.CHARITY_ABOUT)
    fun charityAbout(@RequestParam(defaultValue = "0") offset: Int,
                     @RequestParam(defaultValue = "20") limit: Int): ModelAndView {
        val successfulCount = campaignService.successfulCharityCampaignsCount
        val organizationCount = campaignService.countOrganizationCharityCampaigns()
        val stats = campaignService.charityCampaignsStats(START_CHARITY_DATE)
        val members = charityService.getAllMembers(offset, limit)
        Collections.shuffle(members)
        val events = postDAO.selectCharityNewsByTag(CharityEventType.ABOUT, 0, 0, 3)
        val partners = configurationService.getJsonArrayConfig(Partner::class.java, ConfigurationType.CHARITY_PARTNERS_CONFIGURATION_LIST)

        return createCharityDefaultModelAndView(Actions.ABOUT)
                .addObject("successfulCount", successfulCount)
                .addObject("members", members)
                .addObject("stats", stats)
                .addObject("organizationCount", organizationCount)
                .addObject("events", events)
                .addObject("partners", partners)
    }

    @GetMapping(Urls.CHARITY_NEWS)
    fun charityNews(@RequestParam(defaultValue = "0") offset: Int,
                    @RequestParam(defaultValue = "3") limit: Int): ModelAndView {
        val newsTags = charityNewsTagDAO.selectAllCharityNewsTags(0, MAX_TAG_LIMIT, true, false)
        val partnerNewsTags = charityNewsTagDAO.selectAllCharityNewsTags(0, MAX_TAG_LIMIT, false, true)
        return createCharityDefaultModelAndView(Actions.NEWS)
                .addObject("newsTags", newsTags)
                .addObject("partnerNewsTags", partnerNewsTags)
                .addObject("offset", offset)
                .addObject("post", null)
                .addObject("limit", limit)
    }

    @GetMapping(Urls.CHARITY_NEWS_ID)
    fun charityNewsId(@PathVariable("postId") postId: Long): ModelAndView {
        val newsTags = charityNewsTagDAO.selectAllCharityNewsTags(0, MAX_TAG_LIMIT, true, false)
        val partnerNewsTags = charityNewsTagDAO.selectAllCharityNewsTags(0, MAX_TAG_LIMIT, false, true)
        val post = postDAO.select(postId)
        return createCharityDefaultModelAndView(Actions.NEWS)
                .addObject("newsTags", newsTags)
                .addObject("partnerNewsTags", partnerNewsTags)
                .addObject("post", post)
                .addObject("offset", 0)
                .addObject("limit", 3)
    }


    @GetMapping(Urls.CHARITY_SCHOOL)
    fun charitySchool(@RequestParam(defaultValue = "0") offset: Int,
                      @RequestParam(defaultValue = "20") limit: Int): ModelAndView {
        val recentWebinarsIds = configurationService.charityRecentWebinarsIds
        val recentWebinarsList = broadcastService.selectByIdList(recentWebinarsIds)
        val charitySchoolEventsList = postDAO.selectFutureCharityPosts(CharityEventType.SCHOOL, offset, limit)

        return createCharityDefaultModelAndView(Actions.SCHOOL)
                .addObject("recentWebinarsIds", recentWebinarsIds)
                .addObject("recentWebinarsList", recentWebinarsList)
                .addObject("charitySchoolEventsList", charitySchoolEventsList)
    }

    @GetMapping(Urls.CHARITY_CAMPAIGNS_STATS)
    @ResponseBody
    fun charityCampaignsStats(): List<CharityCampaignsStats> = campaignService.charityCampaignsStats(START_CHARITY_DATE)

    @GetMapping(Urls.CHARITY_NEWS_LIST)
    @ResponseBody
    fun charityNews(@RequestParam(defaultValue = "0") offset: Int,
                    @RequestParam(defaultValue = "3") limit: Int,
                    @RequestParam(defaultValue = "0") newsTagId: Long): List<Post> {
        return postDAO.selectCharityNewsByTag(CharityEventType.NEWS, newsTagId, offset, limit)
    }

    @GetMapping(Urls.CHARITY_PARTNERS_NEWS_LIST)
    @ResponseBody
    fun charityPartnersNews(@RequestParam(defaultValue = "0") offset: Int,
                            @RequestParam(defaultValue = "3") limit: Int,
                            @RequestParam(defaultValue = "0") newsTagId: Long): List<Post> =
            postDAO.selectCharityNewsByTag(CharityEventType.PARTNERS_NEWS, newsTagId, offset, limit)

    @GetMapping(Urls.CHARITY_ABOUT_POSTS)
    @ResponseBody
    fun charityAboutPosts(@RequestParam(defaultValue = "0") offset: Int,
                          @RequestParam(defaultValue = "3") limit: Int): List<Post> =
            postDAO.selectCharityNewsByTag(CharityEventType.ABOUT, 0, offset, limit)


    companion object {
        private const val MAX_TAG_LIMIT = 100
        private val START_CHARITY_DATE = GregorianCalendar(2012, Calendar.FEBRUARY, 1).time
    }
}

package ru.planeta.charity.controllers

import org.springframework.context.MessageSource
import org.springframework.validation.BindingResult
import org.springframework.web.bind.annotation.*
import ru.planeta.api.model.ActionStatus
import ru.planeta.api.service.charity.CharityLibraryFileService
import ru.planeta.api.service.notifications.NotificationService
import ru.planeta.api.service.registration.RegistrationService
import ru.planeta.api.web.controllers.services.BaseProjectSpecificControllerService
import ru.planeta.dao.profiledb.PostDAO
import ru.planeta.model.charity.LibraryFile
import ru.planeta.model.charity.LibraryFilesWithTheme
import ru.planeta.model.charity.enums.CharityEventType
import ru.planeta.model.common.charity.CharityEventOrder
import ru.planeta.model.profile.Post
import ru.planeta.model.stat.RefererStatType
import java.util.*
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse
import javax.validation.Valid

@RestController
class CharitySchoolController(private val charityLibraryFileService: CharityLibraryFileService,
                              private val notificationService: NotificationService,
                              private val registrationService: RegistrationService,
                              private val postDAO: PostDAO,
                              private val messageSource: MessageSource,
                              private val baseProjectSpecificControllerService: BaseProjectSpecificControllerService) {

    @GetMapping(Urls.CHARITY_LIBRARY_FILES_GET)
    fun charityNews(@RequestParam(defaultValue = "0") offset: Int,
                    @RequestParam(defaultValue = "200") limit: Int,
                    @RequestParam(value = "query", defaultValue = "") searchString: String): List<LibraryFilesWithTheme> {
        val libraryFiles = charityLibraryFileService.selectLibraryFileByHeaderList(searchString, offset, limit)

        val libraryFilesByThemesMap = HashMap<String, LibraryFilesWithTheme>()
        for (libraryFile in libraryFiles) {
            val theme = libraryFile.themeHeader

            if (libraryFilesByThemesMap[theme] == null) {
                val newLibraryFileList = ArrayList<LibraryFile>()
                newLibraryFileList.add(libraryFile)

                val libraryFilesWithTheme = LibraryFilesWithTheme(theme, newLibraryFileList)
                libraryFilesByThemesMap.put(theme, libraryFilesWithTheme)
            } else {
                val existsLibraryFilesWithTheme = libraryFilesByThemesMap[theme]
                existsLibraryFilesWithTheme?.libraryFiles?.add(libraryFile)
            }

        }

        return ArrayList(libraryFilesByThemesMap.values)
    }

    @PostMapping(Urls.CHARITY_EVENT_ORDER_SEND)
    fun sendCharityEventOrder(@RequestBody @Valid charityEventOrder: CharityEventOrder,
                              result: BindingResult,
                              request: HttpServletRequest, response: HttpServletResponse): ActionStatus<CharityEventOrder> {
        if (result.hasErrors()) {
            return ActionStatus.createErrorStatus(result, messageSource)
        }

        val profileId = registrationService.autoRegisterIfNotExists(charityEventOrder.email, charityEventOrder.fio,
                baseProjectSpecificControllerService.getRequestStat(request, response, RefererStatType.REGISTRATION))
        charityEventOrder.profileId = profileId

        notificationService.sendCharityEventOrder(charityEventOrder.email, charityEventOrder)

        return ActionStatus.createSuccessStatus()
    }

    @GetMapping(Urls.CHARITY_SCHOOL_EVENTS_LIST)
    fun charityNews(@RequestParam(defaultValue = "0") offset: Int,
                    @RequestParam(defaultValue = "4") limit: Int): List<Post> =
            postDAO.selectFutureCharityPosts(CharityEventType.SCHOOL, offset, limit)

}

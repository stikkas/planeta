package ru.planeta.charity.controllers

import ru.planeta.api.web.controllers.IAction

enum class Actions(prefix: ActionPrefixes, private val _text: String) : IAction {
    WELCOME(ActionPrefixes.WELCOME, "welcome"),
    ABOUT(ActionPrefixes.CHARITY, "charity-about"),
    NEWS(ActionPrefixes.CHARITY, "charity-news"),
    SCHOOL(ActionPrefixes.CHARITY, "charity-school");

    private val text: String
        get() = prefix + _text

    private var prefix: String = prefix.toString()

    override val path: String
        get() = text

    override val actionName: String
        get() = _text

    override fun toString(): String = text
}

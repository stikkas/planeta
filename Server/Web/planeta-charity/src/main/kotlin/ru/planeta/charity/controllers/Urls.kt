package ru.planeta.charity.controllers

object Urls {
    const val ROOT = "/"
    const val CHARITY_ABOUT = "/about"
    const val CHARITY_NEWS = "/news"
    const val CHARITY_NEWS_ID = "/news/{postId}"
    const val CHARITY_SCHOOL = "/school"
    const val CHARITY_CAMPAIGNS_STATS = "/api/charity-campaigns-stats.json"
    const val CHARITY_NEWS_LIST = "/api/util/charity-news-list.json"
    const val CHARITY_PARTNERS_NEWS_LIST = "/api/util/charity-partners-news-list.json"
    const val CHARITY_ABOUT_POSTS = "/api/util/charity-about-posts.json"

    const val CHARITY_EVENT_ORDER_SEND = "/charity/charity-event-order-send.json"
    const val CHARITY_SCHOOL_EVENTS_LIST = "/api/util/charity-school-events-list.json"
    const val CHARITY_LIBRARY_FILES_GET = "/charity/search-charity-library-files.json"
}

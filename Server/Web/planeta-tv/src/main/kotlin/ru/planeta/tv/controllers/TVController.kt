package ru.planeta.tv.controllers

import org.apache.commons.lang3.StringUtils
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.servlet.ModelAndView
import ru.planeta.api.service.content.VideoService
import ru.planeta.api.web.authentication.myProfileId
import ru.planeta.api.web.controllers.services.BaseControllerService
import ru.planeta.commons.external.oembed.provider.VideoType
import ru.planeta.commons.web.WebUtils
import ru.planeta.model.enums.ProjectType
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

/**
 * Controller for TV Web app
 *
 * @author m.shulepov
 */
@Controller
class TVController(private val videoService: VideoService,
                   private val baseControllerService: BaseControllerService) {

    @GetMapping(Urls.VIDEO_FRAME)
    fun showVideoFrame(@RequestParam(value = "profileId", required = false) alias: String?,
                       @RequestParam(defaultValue = "0") videoId: Long,
                       @RequestParam(required = false) externalUrl: String?,
                       @RequestParam(defaultValue = "false") autostart: Boolean,
                       @RequestParam(defaultValue = "false") redirect: Boolean,
                       @RequestParam(defaultValue = "false") fromCampaign: Boolean,
                       resp: HttpServletResponse,
                       request: HttpServletRequest): ModelAndView? {

        val video = if (StringUtils.isNotEmpty(externalUrl)) {
            videoService.getExternalVideoDetails(externalUrl)
        } else {
            val profile = baseControllerService.profileService.getProfileSafe(alias)
            videoService.getVideoToSee(myProfileId(), profile.profileId, videoId)
        }

        if (redirect && video.cachedVideo != null) {
            //ignore redirect for all ios devices to prevent error when youtube app not installed on device
            // PLANETA-12328
            if (video.cachedVideo.videoType != VideoType.YOUTUBE || !WebUtils.isIos(request)) {
                resp.sendRedirect(video.cachedVideo.url)
                return null
            }
        }
        return baseControllerService.createDefaultModelAndView(Actions.VIDEO_FRAME, ProjectType.TV)
                .addObject("video", video)
                .addObject("autostart", autostart)
                .addObject("fromCampaign", fromCampaign)
                .addObject("html5PlayerSkinUrl", baseControllerService
                        .projectService.getUrl(ProjectType.STATIC, "/js/lib/jwplayer/skins/bekle.xml"))
    }

}


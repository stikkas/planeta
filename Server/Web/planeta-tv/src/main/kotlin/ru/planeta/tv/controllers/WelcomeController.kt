package ru.planeta.tv.controllers

import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.servlet.ModelAndView
import ru.planeta.api.service.configurations.ConfigurationService
import ru.planeta.api.service.content.BroadcastService
import ru.planeta.api.web.authentication.myProfileId
import ru.planeta.api.web.controllers.services.BaseControllerService
import ru.planeta.model.enums.ProjectType
import java.util.*

/**
 * TV web app welcome controller
 *
 * @author ds.kolyshev
 * Date: 18.06.12
 */
@Controller
class WelcomeController(private val broadcastService: BroadcastService,
                        private val configurationService: ConfigurationService,
                        private val baseControllerService: BaseControllerService) {

    @GetMapping(Urls.ROOT)
    fun welcome(): ModelAndView = getRootModelAndView(null)

    private fun getRootModelAndView(tabIdFromUrl: Int?): ModelAndView {
        val broadcastProfileIdsList = configurationService.promoProfilesIdsOfBroadcasts
        return createWelcomeModelAndView(tabIdFromUrl).addObject("promoBroadcasts",
                broadcastService.selectByIdList(broadcastProfileIdsList))
    }

    @GetMapping(Urls.CATEGORY_NASHE_RADIO)
    fun welcomeCategoryNasheRadio(): ModelAndView {
        val tabId = 2
        return getRootModelAndView(tabId)
    }

    @GetMapping(Urls.CATEGORY_ALIVE)
    fun welcomeCategoryAlive(): ModelAndView {
        val tabId = 1
        return getRootModelAndView(tabId)
    }

    @GetMapping(Urls.CATEGORY_SCHOOL)
    fun welcomeCategorySchool(): ModelAndView {
        val tabId = 0
        return getRootModelAndView(tabId)
    }

    @GetMapping(Urls.CATEGORY_CONCERTS)
    fun welcomeCategoryConcerts(): ModelAndView {
        val tabId = 3
        return getRootModelAndView(tabId)
    }


    @GetMapping(Urls.BROADCAST_SEARCH)
    fun searchBroadcasts(@RequestParam(defaultValue = "") query: String): ModelAndView =
            baseControllerService.createDefaultModelAndView(Actions.SEARCH_BROADCASTS, ProjectType.TV)
                    .addObject("query", query)

    private fun createWelcomeModelAndView(tabIdFromUrl: Int?): ModelAndView {
        val upcomingBroadcasts = broadcastService.getUpcomingBroadcasts(myProfileId(), Date(), 0, 0)
        return baseControllerService.createDefaultModelAndView(Actions.WELCOME, ProjectType.TV)
                .addObject("tabIdFromUrl", tabIdFromUrl)
                .addObject("upcomingBroadcasts", upcomingBroadcasts)
                .addObject("upcomingBroadcastsCount", upcomingBroadcasts?.size ?: 0)
                .addObject("now", Date())
    }

}

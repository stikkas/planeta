package ru.planeta.tv.service

import org.apache.commons.lang3.time.DateUtils
import org.springframework.context.annotation.Lazy
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Service
import ru.planeta.api.exceptions.NotFoundException
import ru.planeta.api.exceptions.PermissionException
import java.util.Date
import java.util.HashMap
import java.util.concurrent.locks.ReadWriteLock
import java.util.concurrent.locks.ReentrantReadWriteLock

@Service
@Lazy(false)
class ChatBanServiceImpl : ChatBanService {
    private val lock = ReentrantReadWriteLock()
    private val chatMap = HashMap<Long, OneChatBannedUsers>()

    private class OneChatBannedUsers {
        private val bannedUsers = HashMap<Long, Date>()

        val isEmpty: Boolean
            get() = bannedUsers.isEmpty()

        fun add(userId: Long) {
            bannedUsers.put(userId, DateUtils.addHours(Date(), BAN_TIME))
        }

        fun delete(userId: Long) {
            bannedUsers.remove(userId)
        }

        operator fun get(userId: Long): Date? {
            return bannedUsers[userId]
        }

        fun cleanOld(now: Date) {
            val entries = bannedUsers.entries.iterator()
            val thisEntry = entries.next() as Map.Entry<*, *>
            val value = thisEntry.value as Date
            if (value.before(now)) {
                entries.remove()
            }
        }
    }

    // CleanerJob
    @Scheduled(fixedRate = CLEAN_TIME.toLong())
    private fun clean() {
        lock.writeLock().lock()
        try {
            val now = Date()
            val entries = chatMap.entries.iterator()
            while (entries.hasNext()) {
                val thisEntry = entries.next() as Map.Entry<*, *>
                val value = thisEntry.value as OneChatBannedUsers
                value.cleanOld(now)
                if (value.isEmpty) {
                    entries.remove()
                }
            }
        } finally {
            lock.writeLock().unlock()
        }
    }

    override fun banUserInChat(clientId: Long, chatId: Long, bannedUserId: Long) {
        lock.writeLock().lock()
        try {
            var oneChatBannedUsers: OneChatBannedUsers? = chatMap[chatId]
            if (oneChatBannedUsers == null) {
                oneChatBannedUsers = OneChatBannedUsers()
                chatMap.put(chatId, oneChatBannedUsers)
            }
            oneChatBannedUsers.add(bannedUserId)
        } finally {
            lock.writeLock().unlock()
        }
    }

    override fun removeUserFromBan(clientId: Long, chatId: Long, bannedUserId: Long): Boolean {
        lock.writeLock().lock()
        try {
            val oneChatBannedUsers = chatMap[chatId] ?: return false
            val timeWhenBanned = oneChatBannedUsers[bannedUserId] ?: return false

            oneChatBannedUsers.delete(bannedUserId)
            return true
        } finally {
            lock.writeLock().unlock()
        }
    }

    override fun getBanFinishTime(chatId: Long, userId: Long): Long? {
        lock.readLock().lock()
        try {
            val oneChatBannedUsers = chatMap[chatId] ?: return null

            val timeWhenBanned = oneChatBannedUsers[userId] ?: return null

            val now = Date()
            val result = timeWhenBanned.time - now.time
            return if (result <= 0) {
                null
            } else result

        } finally {
            lock.readLock().unlock()
        }
    }

    companion object {
        private const val CLEAN_TIME = 10 * 60 * 1000 // 10 minutes
        private const val BAN_TIME = 8   // 8 hours
    }
}

package ru.planeta.tv.validator

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import org.springframework.validation.Errors
import org.springframework.validation.ValidationUtils
import org.springframework.validation.Validator
import ru.planeta.api.model.BroadcastSubscriptionDTO
import ru.planeta.dao.commondb.UserPrivateInfoDAO
import ru.planeta.model.common.UserPrivateInfo

/**
 * Created with IntelliJ IDEA.
 * Date: 22.10.2015
 * Time: 17:32
 * To change this template use File | Settings | File Templates.
 */
@Component
class BroadcastSubscriptionDTOValidator(private val userPrivateInfoDAO: UserPrivateInfoDAO) : Validator {


    override fun supports(clazz: Class<*>): Boolean = clazz.isAssignableFrom(BroadcastSubscriptionDTO::class.java)

    override fun validate(target: Any, errors: Errors) {
        val dto = target as BroadcastSubscriptionDTO

        if (dto.profileId == -1L) {
            ValidationUtils.rejectIfEmpty(errors, "email", "field.required")
        }

        val userPrivateInfo = userPrivateInfoDAO.selectByEmail(dto.email)
        if (userPrivateInfo != null) {
            errors.rejectValue("email", "field.error.exists")
        }
    }
}

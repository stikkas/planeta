package ru.planeta.tv.service

import org.apache.commons.collections4.IterableUtils
import org.apache.commons.collections4.Predicate
import org.apache.commons.lang3.StringUtils
import org.apache.commons.lang3.time.DateUtils
import org.apache.log4j.Logger
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Lazy
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Service
import ru.planeta.api.BaseService
import ru.planeta.api.exceptions.MessageCode
import ru.planeta.api.exceptions.NotFoundException
import ru.planeta.api.exceptions.PermissionException
import ru.planeta.api.model.json.ChatInfo
import ru.planeta.api.service.content.BroadcastService
import ru.planeta.api.text.FormattingService
import ru.planeta.dao.profiledb.ChatDAO
import ru.planeta.dao.profiledb.ChatMessageDAO
import ru.planeta.model.enums.ObjectType
import ru.planeta.model.enums.ProfileRelationStatus
import ru.planeta.model.profile.Profile
import ru.planeta.model.profile.chat.Chat
import ru.planeta.model.profile.chat.ChatMessage
import ru.planeta.model.profile.chat.ChatUser
import java.util.*

/**
 * Implementation of ChatService interface, all implemented methods are synchronized
 *
 * @author : m.shulepov
 */
@Service
@Lazy(false)
class ChatServiceImpl(private val chatBanService: ChatBanService,
                      private val formattingService: FormattingService,
                      private val broadcastCacheService: BroadcastCacheService,
                      private val chatDAO: ChatDAO,
                      private val chatMessageDAO: ChatMessageDAO) : BaseService(), ChatService {
    private val chatInfoCache = ArrayList<ChatInfo>()

    override fun banUserInChat(clientId: Long, chatId: Long, bannedUserId: Long) {
        verifyChatAdminPermissions(clientId, chatId)
        chatBanService.banUserInChat(clientId, chatId, bannedUserId)
    }

    override fun removeUserFromBan(clientId: Long, chatId: Long, bannedUserId: Long): Boolean {
        verifyChatAdminPermissions(clientId, chatId)
        return chatBanService.removeUserFromBan(clientId, chatId, bannedUserId)
    }

    @Synchronized
    override fun joinChat(clientId: Long, profileId: Long, ownerObjectId: Long, ownerObjectType: ObjectType): ChatInfo? {
        var chatInfo: ChatInfo = loadChatInfoFromCache(profileId, ownerObjectId, ownerObjectType) ?: return null

        val profile = profileDAO.selectById(clientId)
        if (profile != null) {
            val chatUser = ChatUser(profile, chatInfo.chat.chatId)
            addUserToChat(chatUser, chatInfo)
        }

        chatInfo = prepareChatInfoResponse(chatInfo)
        return chatInfo
    }

    @Synchronized override fun leaveChat(clientId: Long, profileId: Long, chatId: Long) {
        val chatInfo = getChatInfoFromCache(chatId)
        if (chatInfo != null) {
            val chatUser = ChatUser(clientId, chatId)
            removeUserFromChat(chatUser, chatInfo)
        }
    }

    @Synchronized
    override fun postMessage(chatMessage: ChatMessage): ChatMessage {
        var chatMessage = chatMessage
        verifyChatPermission(chatMessage.authorProfileId, chatMessage.chatId, chatMessage.profileId)

        chatMessage.timeAdded = Date()

        val chatInfo = getChatInfoSafe(chatMessage.chatId)

        //chatMessage.setMessageText(ProfanityFilter.filter(chatMessage.getMessageText()));
        chatMessage.messageText = chatMessage.messageText
        chatMessage.messageTextHtml = formattingService.formatPlainText(
                chatMessage.messageText, chatMessage.authorProfileId, true)
        chatMessage.messageText = StringUtils.EMPTY

        chatMessageDAO.insert(chatMessage)

        // get full message from db
        chatMessage = chatMessageDAO.selectMessage(chatMessage.profileId, chatMessage.chatId, chatMessage.messageId)

        return chatMessage
    }

    @Synchronized
    override fun removeChatMessage(clientId: Long, profileId: Long, chatId: Long, messageId: Long) {
        verifyChatAdminPermissions(clientId, chatId)

        val chatMessage = chatMessageDAO.selectMessage(profileId, chatId, messageId)
        chatMessage.isDeleted = java.lang.Boolean.TRUE
        chatMessageDAO.update(chatMessage)
    }

    @Synchronized
    override fun getMessages(clientId: Long, profileId: Long, chatId: Long, offset: Int, limit: Int): List<ChatMessage> {
        val chatInfo = getChatInfoSafe(chatId)

        val chatMessages = chatMessageDAO.selectMessages(profileId, chatId, offset, limit)
        updateChatUserActivityTime(chatInfo, clientId, chatId)

        return chatMessages
    }

    @Synchronized
    override fun getMessages(clientId: Long, profileId: Long, chatId: Long, startMessageId: Long): List<ChatMessage> {
        val chatInfo = getChatInfoSafe(chatId)

        val chatMessages = chatMessageDAO.selectMessages(profileId, chatId, startMessageId)
        updateChatUserActivityTime(chatInfo, clientId, chatId)

        return chatMessages
    }

    //    Private helper methods

    /**
     * Verifies if user has admin permissions in this chat
     *
     * @param clientId Client id
     * @param chatId   Chat id
     */
    private fun verifyChatAdminPermissions(clientId: Long, chatId: Long) {
        val chatInfo = getChatInfoSafe(chatId)
        permissionService.checkIsAdmin(clientId, chatInfo.chat.profileId)
    }

    private fun getChatInfoSafe(chatId: Long): ChatInfo = getChatInfoFromCache(chatId) ?: throw NotFoundException(Chat::class.java, chatId)

    /**
     * Verifies if client can do something with chat (post message or deleteByProfileId it)
     *
     * @param clientId           Client id
     * @param chatId             Chat id
     * @param chatOwnerProfileId Chat owner id
     */
    private fun verifyChatPermission(clientId: Long, chatId: Long, chatOwnerProfileId: Long) {
        if (permissionService.isAnonymous(clientId)) {
            throw PermissionException()
        }
        val profileRelationStatuses = permissionService.getProfileRelationStatus(chatOwnerProfileId, clientId)
        if (profileRelationStatuses.contains(ProfileRelationStatus.BLOCKED_BY_ME)) {
            throw PermissionException(MessageCode.PERMISSION_RELATION_BANNED)
        }
        val banFinishTime = chatBanService.getBanFinishTime(chatId, clientId)
        if (banFinishTime != null) {
            // todo create message with time
            throw PermissionException(MessageCode.PERMISSION_CHAT_BANNED)
        }
    }

    private fun loadChatInfoFromCache(profileId: Long, ownerObjectId: Long, ownerObjectType: ObjectType): ChatInfo? {
        val broadcast = broadcastCacheService.getBroadcastCacheItem(ownerObjectId)
        if (!broadcast.broadcastInfo.broadcast.isChatEnabled) {
            return null
        }

        var chatInfo = getChatInfoFromCache(profileId, ownerObjectId)
        if (chatInfo == null) {
            chatInfo = ChatInfo(loadChatFromDb(profileId, ownerObjectId, ownerObjectType))
            chatInfoCache.add(chatInfo)
        }
        return chatInfo
    }

    /**
     * Gets chat info from cache by chat id.
     *
     * @param chatId chat identifier
     * @return chat info if chat with specified chat id exists or null otherwise
     */
    private fun getChatInfoFromCache(chatId: Long): ChatInfo? {
        return IterableUtils.find(chatInfoCache) { chatInfo -> chatInfo.chat.chatId == chatId }
    }

    /**
     * Gets chat info from cache by profile id and owner object id.
     *
     * @param profileId     profile identifier
     * @param ownerObjectId owner object identifier
     * @return chat info for specified profile id and owner object id or null otherwise
     */
    private fun getChatInfoFromCache(profileId: Long, ownerObjectId: Long): ChatInfo? {
        return IterableUtils.find(chatInfoCache) { chatInfo ->
            val chat = chatInfo.chat
            chat.profileId == profileId && chat.ownerObjectId == ownerObjectId
        }
    }

    /**
     * Loads chat from database. Creates new chat and stores it in db if necessary
     *
     * @param profileId       profile identifier
     * @param ownerObjectId   owner object identifier
     * @param ownerObjectType owner object type
     * @return chat instance from database
     */
    private fun loadChatFromDb(profileId: Long, ownerObjectId: Long, ownerObjectType: ObjectType): Chat {
        var chat: Chat? = chatDAO.selectByOwnerObject(profileId, ownerObjectId)
        if (chat == null) {
            chat = Chat()
            chat.profileId = profileId
            chat.ownerObjectId = ownerObjectId
            chat.setOwnerObjectTypeCode(ownerObjectType.code)
            chat.timeAdded = Date()
            chatDAO.insert(chat)
        }
        return chat
    }

    @Scheduled(fixedRate = (CLIENT_IDLE_TIME / 2).toLong())
    @Synchronized internal fun cleanUp() {
        if (chatInfoCache.size == 0) return
        log.info("--- START: CLEANING UP CHAT CACHE ---")
        log.info("Cache size before clean up: " + chatInfoCache.size)
        for (chatInfo in chatInfoCache) {
            cleanUp(chatInfo)
        }
        log.info("Cache size after clean up: " + chatInfoCache.size)
        log.info("--- END: CLEANING UP CHAT CACHE ---")
    }

    companion object {

        private const val RANDOM_CHAT_USERS_SIZE = 35
        private const val CLIENT_IDLE_TIME = 900000 // 15 minutes

        private val log = Logger.getLogger(ChatServiceImpl::class.java)

        private fun addUserToChat(chatUser: ChatUser, chatInfo: ChatInfo) {
            chatInfo.chatUsers.remove(chatUser)
            chatInfo.chatUsers.add(chatUser)
        }

        private fun getUserFromChat(chatInfo: ChatInfo, profileId: Long, chatId: Long): ChatUser? {
            val chatUsers = chatInfo.chatUsers
            return IterableUtils.find(chatUsers) { chatUser -> chatId == chatUser.chatId && profileId == chatUser.profileId }
        }

        private fun removeUserFromChat(chatUser: ChatUser, chatInfo: ChatInfo) {
            chatInfo.chatUsers.remove(chatUser)
        }

        private fun prepareChatInfoResponse(chatInfo: ChatInfo): ChatInfo {
            val chatUsersCopy = HashSet(chatInfo.chatUsers)
            val randomChatUsers = HashSet<ChatUser>()
            val iterator = chatUsersCopy.iterator()
            while (iterator.hasNext() && randomChatUsers.size < RANDOM_CHAT_USERS_SIZE) {
                randomChatUsers.add(iterator.next())
            }

            val chatInfoCopy = ChatInfo(chatInfo.chat)
            chatInfoCopy.chatUsers = randomChatUsers
            chatInfoCopy.numOfUsers = chatUsersCopy.size.toLong()
            chatInfoCopy.isEnabled = chatInfo.isEnabled

            return chatInfoCopy
        }

        private fun updateChatUserActivityTime(chatInfo: ChatInfo, clientId: Long, chatId: Long) {
            val chatUser = getUserFromChat(chatInfo, clientId, chatId)
            if (chatUser != null) {
                chatUser.lastActivityTime = Date()
            }
        }

        private fun cleanUp(chatInfo: ChatInfo) {
            val now = Date()
            val idleTimeLimit = DateUtils.addMilliseconds(now, -CLIENT_IDLE_TIME)
            val chatUsers = HashSet(chatInfo.chatUsers)
            val iterator = chatUsers.iterator()
            while (iterator.hasNext()) {
                if (iterator.next().lastActivityTime.before(idleTimeLimit)) {
                    iterator.remove()
                }
            }
            chatInfo.chatUsers = chatUsers
            chatInfo.numOfUsers = chatUsers.size.toLong()
        }
    }

}

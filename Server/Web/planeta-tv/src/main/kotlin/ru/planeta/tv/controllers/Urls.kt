package ru.planeta.tv.controllers

/**
 * Contains webapp urls
 *
 *
 * User: m.shulepov
 */
object Urls {

    const val ROOT = "/"
    const val VIDEO = "/video/{profileId}/{videoId}"
    const val VIDEO_FRAME = "/video-frame.html"

    const val CATEGORY_NASHE_RADIO = "/nasheradio"
    const val CATEGORY_CONCERTS = "/concerts"
    const val CATEGORY_ALIVE = "/alive"
    const val CATEGORY_SCHOOL = "/school"

    const val BROADCAST_SEARCH = "/search/broadcasts"

    const val BROADCAST_GET_STATUS = "/broadcast/{broadcastId}/get-status.json"
    const val BROADCAST_STREAM = "/broadcast/{profileId}/{streamId}.html"
    const val BROADCAST_STREAM_CONTENT = "/broadcast-stream-content.html"
    const val BROADCAST = "/broadcast/{broadcastId}"
    const val PRIVATE_BROADCAST = "/broadcast/{profileId}/{generatedLink}/private.html"
    const val VALIDATE_PRIVATE_BROADCAST = "/broadcast/validate-private-broadcast.json"
    const val BROADCAST_UPDATE_SUBSCRIPTION = "/broadcast/broadcast-update-subscription.json"
    const val BROADCAST_DELETE_SUBSCRIPTION = "/broadcast/broadcast-deleteByProfileId-subscription.json"
    const val BROADCAST_REDIRECT = "/broadcast/{profileId}#{streamId}"
    const val BROADCAST_STREAM_INFO = "/profile/broadcast-stream-info.json"
    const val BROADCAST_VIEWS_INCREASE = "/broadcast/broadcast-views-increase.json"
    const val LOAD_ARCHIVED_BROADCASTS = "/broadcast/load-archived-broadcasts.json"


    //chat
    const val JOIN_CHAT = "/api/profile/join-chat.json"
    const val LEAVE_CHAT = "/api/profile/leave-chat.json"
    const val CHAT_MESSAGES = "/api/profile/chat-messages.json"
    const val CHAT_LAST_MESSAGES = "/api/profile/chat-last-messages.json"
    const val CHAT_POST_MESSAGE = "/api/profile/post-chat-message.json"
    const val CHAT_DELETE_MESSAGE = "/api/profile/deleteByProfileId-chat-message.json"
    const val CHAT_BAN_USER = "/api/profile/ban-chat-user.json"
    const val CHAT_UNBAN_USER = "/api/profile/unban-chat-user.json"

    const val CHECK_GEO_IP = "/utils/check-ip.html"

    const val VIDEO_NOT_FOUND = "/utils/video-not-found.html"
}


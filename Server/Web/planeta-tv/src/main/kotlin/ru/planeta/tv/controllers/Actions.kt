package ru.planeta.tv.controllers

import ru.planeta.api.web.controllers.IAction

/**
 * User: atropnikov
 * Date: 21.03.12
 * Time: 16:04
 */
enum class Actions(val text: String) : IAction {

    WELCOME("welcome"),
    VIDEO_FRAME("video-frame"),
    BROADCAST("broadcast"),
    BROADCAST_STREAM_FRAME("broadcast-stream-frame"),
    SEARCH_BROADCASTS("search-broadcasts");

    override val path: String
        get() = text
    override val actionName: String
        get() = text

    override fun toString(): String = text

}





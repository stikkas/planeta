package ru.planeta.tv.controllers

import org.apache.commons.collections4.CollectionUtils
import org.apache.commons.lang3.StringUtils
import org.springframework.context.MessageSource
import org.springframework.stereotype.Controller
import org.springframework.validation.BindingResult
import org.springframework.web.bind.annotation.*
import org.springframework.web.servlet.ModelAndView
import ru.planeta.api.exceptions.NotFoundException
import ru.planeta.api.exceptions.PermissionException
import ru.planeta.api.model.ActionStatus
import ru.planeta.api.model.BroadcastSubscriptionDTO
import ru.planeta.api.model.enums.image.ImageType
import ru.planeta.api.model.json.BroadcastInfo
import ru.planeta.api.service.content.BroadcastService
import ru.planeta.api.service.content.BroadcastSubscriptionService
import ru.planeta.api.service.registration.RegistrationService
import ru.planeta.api.service.security.PermissionService
import ru.planeta.api.utils.ValidateUtils
import ru.planeta.api.web.authentication.isAdmin
import ru.planeta.api.web.authentication.isAnonymous
import ru.planeta.api.web.authentication.isAuthorized
import ru.planeta.api.web.authentication.myProfileId
import ru.planeta.api.web.controllers.services.BaseProjectSpecificControllerService
import ru.planeta.api.web.utils.SessionUtils
import ru.planeta.commons.web.IpUtils
import ru.planeta.dao.commondb.UserPrivateInfoDAO
import ru.planeta.model.enums.ProjectType
import ru.planeta.model.enums.ThumbnailType
import ru.planeta.model.profile.broadcast.Broadcast
import ru.planeta.model.profile.broadcast.enums.BroadcastCategory
import ru.planeta.model.profile.broadcast.enums.BroadcastStatus
import ru.planeta.model.stat.RefererStatType
import ru.planeta.tv.dto.BroadcastStatusDTO
import ru.planeta.tv.service.BroadcastCacheService
import java.util.*
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse
import javax.validation.Valid

/**
 * Broadcast's requests controller
 *
 * @author ds.kolyshev
 * Date: 19.06.12
 */
@Controller
class BroadcastController(private val broadcastService: BroadcastService,
                          private val broadcastSubscriptionService: BroadcastSubscriptionService,
                          private val broadcastCacheService: BroadcastCacheService,
                          private val userPrivateInfoDAO: UserPrivateInfoDAO,
                          private val registrationService: RegistrationService,
                          private val baseProjectSpecificController: BaseProjectSpecificControllerService,
                          private val messageSource: MessageSource,
                          private val permissionService: PermissionService) {

    private val syncRoot = Any()
    private val privateBroadcastsToUsersMap = object : LinkedHashMap<String, AccessToken>(MAX_ENTRIES, 1.1f) {
        override fun removeEldestEntry(eldest: MutableMap.MutableEntry<String, AccessToken>?): Boolean {
            return size >= MAX_ENTRIES
        }
    }

    private data class AccessToken(private val sessionId: String, private val profileId: Long)

    /**
     * nginx cache must be enabled for this url, to prevent ddos java servlet,
     * and flushed(GET request on /flush-cache/broadcast/{broadcastId}/get-status.json) when broadcast change status
     * PLANETA-11531
     */
    @GetMapping(Urls.BROADCAST_GET_STATUS)
    @ResponseBody
    fun getBroadcastStatus(@PathVariable("broadcastId") broadcastId: Long): ActionStatus<BroadcastStatusDTO> {
        val broadcastCacheItem = broadcastCacheService.getBroadcastCacheItem(broadcastId)
        val broadcastInfo = broadcastCacheItem.broadcastInfo
        val broadcast = broadcastInfo?.broadcast
        val streamStatuses = HashMap<String, BroadcastStatus>()
        broadcastCacheItem.broadcastInfo?.streams?.forEach {
            streamStatuses.put(it.streamId.toString(), it.broadcastStatus)
        }

        return ActionStatus.createSuccessStatus(BroadcastStatusDTO(broadcast?.broadcastStatus, streamStatuses))
    }

    @GetMapping(Urls.BROADCAST_STREAM)
    private fun broadcastStream(@PathVariable("profileId") profileId: Long,
                                @PathVariable("streamId") streamId: Long,
                                request: HttpServletRequest): ModelAndView {
        return baseProjectSpecificController.baseControllerService
                .createRedirectModelAndView(Urls.BROADCAST_REDIRECT
                        .replace("{profileId}", profileId.toString())
                        .replace("{streamId}", streamId.toString()))
    }

    @GetMapping(Urls.BROADCAST)
    private fun broadcast(@PathVariable("broadcastId") broadcastId: Long,
                          request: HttpServletRequest): ModelAndView {
        if (broadcastId == 0L) {
            return baseProjectSpecificController.baseControllerService.createRedirectModelAndView("/")
        }

        val broadcastCacheItem = broadcastCacheService.getBroadcastCacheItem(broadcastId)
        val broadcastInfo = broadcastCacheItem.broadcastInfo
        val broadcast = broadcastInfo.broadcast

        val subscriptionDTO = BroadcastSubscriptionDTO()
        subscriptionDTO.profileId = myProfileId()
        val broadcastId = broadcast.broadcastId
        subscriptionDTO.broadcastId = broadcastId
        val profileId = broadcast.profileId
        subscriptionDTO.broadcastProfileId = profileId
        val broadcastSubscription = broadcastSubscriptionService.getBroadcastSubscription(myProfileId(), profileId, broadcastId)
        if (broadcastSubscription != null && myProfileId() != -1L) {
            subscriptionDTO.email = userPrivateInfoDAO.selectByUserId(myProfileId())?.email
        }

        var isAvailableToViewing = broadcast?.isArchived
        val now = Date()
        if (broadcast.isArchived) {
            isAvailableToViewing = now.before(broadcast.timeEnd) && now.after(broadcast.timeBegin)
        }

        val modelAndView = baseProjectSpecificController.baseControllerService.createDefaultModelAndView(Actions.BROADCAST.text, ProjectType.TV)
                .addObject("isAvailableToViewing", isAvailableToViewing)
                .addObject("broadcastInfo", broadcastInfo)
                .addObject("isGeoAllowed", checkGeoRestriction(request, broadcastInfo))
                .addObject("isBackersAllowed", checkBackersRestriction(broadcastInfo))
                .addObject("subscriptionDTO", subscriptionDTO)
        configurePrivateAccess(broadcast, modelAndView)
        return modelAndView.addObject("upcomingBroadcasts", getUpcomingBroadcasts(broadcast.broadcastId))
                .addObject("defaultStreamId", getDefaultStreamId(broadcastInfo))
    }

    private fun getUpcomingBroadcasts(excludedBroadcastId: Long): List<Broadcast>? {
        val upcomingBroadcasts = broadcastService.getUpcomingBroadcasts(myProfileId(), Date(), 0, 3)
        CollectionUtils.filterInverse(upcomingBroadcasts) { it.broadcastId == excludedBroadcastId }
        return upcomingBroadcasts
    }

    private fun configurePrivateAccess(broadcast: Broadcast, modelAndView: ModelAndView) {
        if (!broadcast.isClosed) {
            return
        }
        val broadcastId = broadcast.broadcastId
        val hash = SessionUtils.privateBroadcastGeneratedLink
        val isPrivateAccessAllowed = isPrivateAccessAllowed(hash, broadcastId)
        modelAndView.addObject("isPrivateAccessAllowed", isPrivateAccessAllowed)
        if (isPrivateAccessAllowed) {
            modelAndView.addObject("broadcastPrivateTargeting", broadcastService.getBroadcastPrivateTargeting(myProfileId(), broadcastId, hash))
        }
        synchronized(syncRoot) {
            if (hash != null)
                privateBroadcastsToUsersMap.put(hash, createTokenForProfile(myProfileId()))
        }
    }

    @GetMapping(Urls.BROADCAST_STREAM_CONTENT)
    private fun broadcastStreamContent(@RequestParam broadcastId: Long,
                                       @RequestParam streamId: Long): ModelAndView {

        val broadcastCacheItem = broadcastCacheService.getBroadcastCacheItem(broadcastId)
        val broadcastInfo = broadcastCacheItem.broadcastInfo
        val broadcastStream = broadcastCacheService.getBroadcastStream(myProfileId(), broadcastId, streamId)

        val modelAndView = baseProjectSpecificController.baseControllerService.createDefaultModelAndView(Actions.BROADCAST_STREAM_FRAME.text, ProjectType.TV)

        modelAndView.addObject("broadcast", fillDefaultsIfEmpty(broadcastInfo.broadcast))
        var isPrivateAccessAllowed = true
        if (broadcastInfo?.broadcast?.isClosed == true) {
            isPrivateAccessAllowed = isPrivateAccessAllowed(SessionUtils.privateBroadcastGeneratedLink, broadcastInfo.broadcast.broadcastId)
        }

        modelAndView.addObject("isPrivateAccessAllowed", isPrivateAccessAllowed)
                .addObject("activeStream", broadcastStream)
                .addObject("advertisingConfig", broadcastInfo?.broadcast?.advertising)

        if (broadcastStream != null) {
            var embed = broadcastStream.embedVideoHtml
            if (StringUtils.isNotEmpty(embed)) {
                val broadcast = broadcastInfo?.broadcast

                if ((isAuthorized() || broadcast?.isAnonymousCanView == true) && isPrivateAccessAllowed &&
                        (broadcast?.broadcastStatus == BroadcastStatus.LIVE
                                || broadcast?.broadcastStatus == BroadcastStatus.FINISHED && broadcast.isArchived
                                || isAdmin())) {
                    embed = embed
                            //remove one line comments. (?<!:) except lines contains :// like http://
                            .replace("(?<!:)//.+\\r?\\n".toRegex(), "")
                            //remove line ends
                            .replace("\\r?\\n".toRegex(), "").trim { it <= ' ' }

                    modelAndView.addObject("embed", embed)
                }
            }
        }

        return modelAndView
    }


    private fun getThumbnailUrl(url: String?): String? = baseProjectSpecificController
            .baseControllerService.staticNodesService.getThumbnailUrl(url, ThumbnailType.ORIGINAL, ImageType.BROADCAST)

    private fun fillDefaultsIfEmpty(broadcast: Broadcast): Broadcast {

        broadcast.pausedImageUrl = getThumbnailUrl(broadcast.pausedImageUrl)
        broadcast.closedImageUrl = getThumbnailUrl(broadcast.closedImageUrl)
        broadcast.imageUrl = getThumbnailUrl(broadcast.imageUrl)
        broadcast.finishedImageUrl = getThumbnailUrl(broadcast.finishedImageUrl)

        if (!broadcast.isClosedCustomDescription) {
            broadcast.closedDescriptionHtml = messageSource.getMessage("have.no.access.to.translation", null, Locale.getDefault())
        }

        if (!broadcast.isPausedCustomDescription) {
            broadcast.pausedDescriptionHtml = messageSource.getMessage("broadcast.will.resume.soon", null, Locale.getDefault())
        }

        if (!broadcast.isImageCustomDescription) {
            broadcast.imageDescriptionHtml = messageSource.getMessage("broadcasting.has.not.started.yet", null, Locale.getDefault())
        }

        if (!broadcast.isFinishedCustomDescription) {
            broadcast.finishedDescriptionHtml = messageSource.getMessage("broadcasting.has.finished", null, Locale.getDefault())
        }

        return broadcast
    }


    @GetMapping(Urls.PRIVATE_BROADCAST)
    private fun privateBroadcast(@PathVariable("profileId") broadcastId: Long,
                                 @PathVariable("generatedLink") generatedLink: String): ModelAndView {

        val broadcastCacheItem = broadcastCacheService.getBroadcastCacheItem(broadcastId)
        val broadcastInfo = broadcastCacheItem.broadcastInfo
        val broadcast = broadcastInfo.broadcast ?: throw NotFoundException("broadcast is not found")
        val targeting = broadcastService.getBroadcastPrivateTargeting(myProfileId(), broadcast.broadcastId, generatedLink)

        if (isAnonymous() || isPrivateAccessAllowed(generatedLink, broadcast.broadcastId)) {
            SessionUtils.privateBroadcastGeneratedLink = generatedLink
        } else {
            SessionUtils.removePrivateBroadcastGeneratedLink()
        }
        return baseProjectSpecificController.baseControllerService
                .createRedirectModelAndView(Urls.BROADCAST.replace("{profileId}", broadcastId.toString()).replace(".html", ""))
    }

    @GetMapping(Urls.VALIDATE_PRIVATE_BROADCAST)
    @ResponseBody
    fun isPrivateBroadcastValid(
            @RequestParam("broadcastId") broadcastId: Long): ActionStatus<*> {
        val hash = SessionUtils.privateBroadcastGeneratedLink
        var isValid = isPrivateAccessAllowed(hash, broadcastId)
        if (isValid) {
            synchronized(syncRoot) {
                isValid = createTokenForProfile(myProfileId()) == privateBroadcastsToUsersMap[hash]
            }
        }
        if (!isValid) {
            SessionUtils.removePrivateBroadcastGeneratedLink()
        }
        return ActionStatus.createSuccessStatus(isValid)
    }

    @GetMapping(Urls.LOAD_ARCHIVED_BROADCASTS)
    @ResponseBody
    fun loadArchivedBroadcast(@RequestParam(defaultValue = "0") offset: Int,
                              @RequestParam(defaultValue = "0") limit: Int,
                              @RequestParam(required = false) broadcastCategory: BroadcastCategory?,
                              @RequestParam(defaultValue = "false") isPopularSort: Boolean): List<Broadcast> {
        return broadcastService.getArchivedBroadcasts(broadcastCategory, myProfileId(), Date(), isPopularSort, offset, limit)
    }

    @PostMapping(Urls.BROADCAST_DELETE_SUBSCRIPTION)
    @ResponseBody
    fun deleteSubscriptions(dto: BroadcastSubscriptionDTO): ActionStatus<BroadcastSubscriptionDTO> {

        broadcastSubscriptionService.deleteBroadcastSubscription(myProfileId(), dto.broadcastProfileId, dto.broadcastId)
        dto.email = null
        dto.profileId = myProfileId()
        return ActionStatus.createSuccessStatus(dto)
    }

    @PostMapping(Urls.BROADCAST_UPDATE_SUBSCRIPTION)
    @ResponseBody
    fun updateUpcomingBroadcastSubscription(@Valid dto: BroadcastSubscriptionDTO,
                                            result: BindingResult,
                                            request: HttpServletRequest, response: HttpServletResponse): ActionStatus<BroadcastSubscriptionDTO> {

        if (result.hasErrors()) {
            return ActionStatus.createErrorStatus(result, messageSource)
        }

        if (myProfileId() != dto.profileId) {
            throw PermissionException()
        }

        if (myProfileId() == -1L) {
            val userPrivateInfo = userPrivateInfoDAO.selectByEmail(dto.email)
            if (userPrivateInfo != null) {
                throw PermissionException()
            } else {
                if (!ValidateUtils.isValidEmail(dto.email)) {
                    return ActionStatus.createErrorStatus("wrong.email", messageSource)
                }
                val profileId = registrationService.autoRegisterIfNotExists(dto.email, null,
                        baseProjectSpecificController.getRequestStat(request, response, RefererStatType.REGISTRATION))
                dto.profileId = profileId
            }
        } else {
            dto.profileId = myProfileId()
            dto.email = userPrivateInfoDAO.selectByUserId(myProfileId())?.email
        }

        broadcastSubscriptionService.addBroadcastSubscription(dto)

        return ActionStatus.createSuccessStatus(dto)
    }

    @PostMapping(Urls.BROADCAST_VIEWS_INCREASE)
    @ResponseBody
    fun updateUpcomingBroadcastSubscription(@RequestParam(value = "defaultStreamId") defaultStreamId: Long,
                                            request: HttpServletRequest): ActionStatus<Boolean> {
        broadcastService.updateBroadcastViewsCount(defaultStreamId)
        return ActionStatus.createSuccessStatus(true)
    }

    /**
     * Determines whether access to private broadcast is allowed
     *
     * @param generatedLink generatedLink
     * @param broadcastId   broadcast id
     * @throws PermissionException
     * @throws NotFoundException
     */
    private fun isPrivateAccessAllowed(generatedLink: String?, broadcastId: Long): Boolean {
        return !isAnonymous() && generatedLink != null && broadcastService.validateBroadcastLink(myProfileId(), broadcastId, generatedLink)
    }

    /**
     * Checks if broadcast is allowed for  cities
     *
     * @param request       request
     * @param broadcastInfo broadcast info
     * @return true or false, depending on whether broadcast is available in specified city
     */
    private fun checkGeoRestriction(request: HttpServletRequest, broadcastInfo: BroadcastInfo?): Boolean {
        if (broadcastInfo == null || broadcastInfo.broadcast == null) {
            throw NotFoundException("Broadcast not found")
        }

        // allow admins to watch broadcasts regardless of his/her ip address
        if (permissionService.isAdmin(myProfileId(), broadcastInfo.broadcast.profileId)) {
            return true
        }

        val ip = IpUtils.getRemoteAddr(request)
        return if (StringUtils.isNotEmpty(ip)) {
            broadcastService.checkBroadcastGeoTargeting(myProfileId(), broadcastInfo.broadcast.profileId, broadcastInfo.broadcast.broadcastId, ip)
        } else true

    }

    private fun checkBackersRestriction(broadcastInfo: BroadcastInfo?): Boolean {
        if (broadcastInfo == null || broadcastInfo.broadcast == null) {
            throw NotFoundException("Broadcast not found")
        }

        // allow admins to watch broadcasts without backers check
        return if (permissionService.isAdmin(myProfileId(), broadcastInfo.broadcast.profileId)) {
            true
        } else broadcastService.checkBroadcastBackersTargeting(myProfileId(), broadcastInfo.broadcast.broadcastId) &&
                broadcastService.checkBroadcastProductTargeting(myProfileId(), broadcastInfo.broadcast.broadcastId)

    }

    companion object {

        private val MAX_ENTRIES = 5000

        /**
         * Gets broadcast's default stream identifier
         *
         * @param broadcastInfo broadcast info
         * @return default stream id for broadcast or 0 by default
         */
        private fun getDefaultStreamId(broadcastInfo: BroadcastInfo): Long {
            var defaultStreamId: Long = 0
            val broadcast = broadcastInfo.broadcast
            if (broadcast != null && broadcast.defaultStreamId > 0) {
                defaultStreamId = broadcast.defaultStreamId
            } else {
                val streams = broadcastInfo.streams
                if (streams != null && !streams.isEmpty()) {
                    defaultStreamId = streams[0].streamId
                }
            }

            return defaultStreamId
        }

        private fun createTokenForProfile(profileId: Long): AccessToken = AccessToken(SessionUtils.session.id, profileId)
    }

}

package ru.planeta.tv.dto

import ru.planeta.model.profile.broadcast.enums.BroadcastStatus

/**
 * Created with IntelliJ IDEA.
 * Date: 17.04.2014
 * Time: 19:00
 */
class BroadcastStatusDTO(var broadcastStatus: BroadcastStatus?, var streamStatuses: Map<String, BroadcastStatus>?)

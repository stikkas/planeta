package ru.planeta.tv.service

/**
 * Special service managing ban in chats
 */
interface ChatBanService {
    /**
     * Banns user in chat
     *
     * @param clientId     admin user id
     * @param chatId       Chat id
     * @param bannedUserId Banned user id
     */
    fun banUserInChat(clientId: Long, chatId: Long, bannedUserId: Long)

    /**
     * Removes ban from user in chat
     *
     * @param clientId     Admin user id
     * @param chatId       Chat id
     * @param bannedUserId Banned user id
     * @return true if user has been removed from ban
     * @throws ru.planeta.api.exceptions.PermissionException
     * @throws ru.planeta.api.exceptions.NotFoundException
     */
    fun removeUserFromBan(clientId: Long, chatId: Long, bannedUserId: Long): Boolean

    /**
     * non throws any exceptions
     *
     * @param chatId Chat id
     * @param userId User id
     * @return time to unban in milliseconds or null if user not banned
     */
    fun getBanFinishTime(chatId: Long, userId: Long): Long?
}

package ru.planeta.tv.controllers

import org.springframework.validation.BindingResult
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import ru.planeta.api.model.ActionStatus
import ru.planeta.api.model.json.ChatInfo
import ru.planeta.api.web.authentication.myProfileId
import ru.planeta.api.web.controllers.services.BaseControllerService
import ru.planeta.model.enums.ObjectType
import ru.planeta.model.profile.chat.ChatMessage
import ru.planeta.tv.service.ChatService
import javax.validation.Valid

/**
 * Controller for all chat functional.
 *
 * @author m.shulepov
 */
@RestController
class ChatController(private val chatService: ChatService,
                     private val baseControllerService: BaseControllerService) {

    @GetMapping(Urls.JOIN_CHAT)
    fun joinChat(@RequestParam profileId: Long,
                 @RequestParam ownerObjectId: Long,
                 @RequestParam ownerObjectType: ObjectType): ChatInfo? {
        val chatInfo = chatService.joinChat(myProfileId(), profileId, ownerObjectId, ownerObjectType)
        return chatInfo
    }

    @PostMapping(Urls.LEAVE_CHAT)
    fun leaveChat(@RequestParam profileId: Long,
                  @RequestParam chatId: Long): ActionStatus<*> {
        chatService.leaveChat(myProfileId(), profileId, chatId)
        return ActionStatus.createSuccessStatus<Any>()
    }

    @GetMapping(Urls.CHAT_MESSAGES)
    fun getMessages(@RequestParam profileId: Long,
                    @RequestParam chatId: Long,
                    @RequestParam(defaultValue = "0") offset: Int,
                    @RequestParam(defaultValue = "25") limit: Int): List<ChatMessage> =
            chatService.getMessages(myProfileId(), profileId, chatId, offset, limit)

    @GetMapping(Urls.CHAT_LAST_MESSAGES)
    fun getLastMessages(@RequestParam profileId: Long,
                        @RequestParam chatId: Long,
                        @RequestParam(defaultValue = "0") startMessageId: Long): List<ChatMessage> =
            chatService.getMessages(myProfileId(), profileId, chatId, startMessageId)

    @PostMapping(Urls.CHAT_POST_MESSAGE)
    fun postMessage(@Valid chatMessage: ChatMessage, result: BindingResult,
                    @RequestParam(defaultValue = "0") startMessageId: Long): ActionStatus<List<ChatMessage>> {
        var chatMessage = chatMessage
        if (result.hasErrors()) {
            return baseControllerService.createErrorStatus(result)
        }
        chatMessage.authorProfileId = myProfileId()
        chatMessage = chatService.postMessage(chatMessage)

        val unreadMessages = chatService.getMessages(myProfileId(), chatMessage.profileId, chatMessage.chatId, startMessageId)
        return ActionStatus.createSuccessStatus(unreadMessages)
    }

    @PostMapping(Urls.CHAT_DELETE_MESSAGE)
    fun removeMessage(@RequestParam profileId: Long,
                      @RequestParam chatId: Long,
                      @RequestParam messageId: Long): ActionStatus<*> {
        chatService.removeChatMessage(myProfileId(), profileId, chatId, messageId)
        return ActionStatus.createSuccessStatus<Any>()
    }

    @PostMapping(Urls.CHAT_BAN_USER)
    fun banUser(@RequestParam profileId: Long,
                @RequestParam userId: Long,
                @RequestParam chatId: Long): ActionStatus<*> {
        chatService.banUserInChat(myProfileId(), chatId, userId)
        return ActionStatus.createSuccessStatus<Any>()
    }

    @PostMapping(Urls.CHAT_UNBAN_USER)
    fun unBanUser(@RequestParam profileId: Long,
                  @RequestParam userId: Long,
                  @RequestParam chatId: Long): ActionStatus<*> {
        chatService.removeUserFromBan(myProfileId(), chatId, userId)
        return ActionStatus.createSuccessStatus<Any>()
    }
}
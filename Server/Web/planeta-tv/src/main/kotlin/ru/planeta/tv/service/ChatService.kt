package ru.planeta.tv.service

import ru.planeta.api.model.json.ChatInfo
import ru.planeta.model.enums.ObjectType
import ru.planeta.model.profile.chat.ChatMessage

/**
 * Service for working with chats
 *
 * @author m.shulepov
 */
interface ChatService {

    /**
     * Bans user in chat
     *
     * @param clientId     Admin user id
     * @param chatId       Chat id
     * @param bannedUserId Banned user id
     */
    fun banUserInChat(clientId: Long, chatId: Long, bannedUserId: Long)

    /**
     * Removes user from ban in chat
     *
     * @param clientId     Admin user id
     * @param chatId       Chat id
     * @param bannedUserId Banned user id
     * @throws ru.planeta.api.exceptions.PermissionException
     * @throws ru.planeta.api.exceptions.NotFoundException
     */
    fun removeUserFromBan(clientId: Long, chatId: Long, bannedUserId: Long): Boolean

    /**
     * Creates chat if necessary and adds user to it. Returns chat info.
     *
     * @param clientId        client id
     * @param profileId       profile id
     * @param ownerObjectId   owner object id
     * @param ownerObjectType owner object type
     */
    fun joinChat(clientId: Long, profileId: Long, ownerObjectId: Long, ownerObjectType: ObjectType): ChatInfo?

    /**
     * Removes user from chat
     *
     * @param clientId  client id
     * @param profileId profile id
     * @param chatId    chat id
     */
    fun leaveChat(clientId: Long, profileId: Long, chatId: Long)

    /**
     * Posts a message to chat and returns formatted chat message
     *
     * @param chatMessage chat message
     * @return formatted chat message
     * @throws IllegalArgumentException                    if message text is empty
     * @throws ru.planeta.api.exceptions.NotFoundException if chat not found
     */
    fun postMessage(chatMessage: ChatMessage): ChatMessage

    /**
     * Removes message from chat
     *
     * @param clientId  Client user id
     * @param profileId profile id
     * @param chatId    chat id
     * @param messageId message id
     * @throws ru.planeta.api.exceptions.NotFoundException if chat not found
     */
    fun removeChatMessage(clientId: Long, profileId: Long, chatId: Long, messageId: Long)

    /**
     * Gets chat messages (sorted by timeAdded DESC).
     *
     * @param clientId  client id
     * @param profileId profile id
     * @param chatId    chat id
     * @param offset    offset
     * @param limit     limit
     * @return list of chat messages
     */
    fun getMessages(clientId: Long, profileId: Long, chatId: Long, offset: Int, limit: Int): List<ChatMessage>

    /**
     * Gets list of chat messages starting from "messageId" (exclusive)
     *
     * @param clientId       client id
     * @param profileId      profile id
     * @param chatId         chat id
     * @param startMessageId message id, to start searching from
     * @return list of chat messages
     */
    fun getMessages(clientId: Long, profileId: Long, chatId: Long, startMessageId: Long): List<ChatMessage>

}
package ru.planeta.tv.service

import org.apache.commons.collections4.IterableUtils
import org.apache.commons.collections4.Predicate
import org.apache.commons.lang3.StringUtils
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service
import ru.planeta.api.advertising.AdvertisingService
import ru.planeta.api.exceptions.NotFoundException
import ru.planeta.api.exceptions.PermissionException
import ru.planeta.api.model.json.BroadcastInfo
import ru.planeta.api.model.json.BroadcastStreamInfo
import ru.planeta.api.service.content.BroadcastService
import ru.planeta.model.advertising.AdvertisingDTO
import ru.planeta.model.profile.broadcast.Broadcast
import ru.planeta.model.profile.broadcast.BroadcastStream

/**
 * Broadcast cache service
 *
 * @author m.shulepov
 * Date: 17.04.13
 */
@Service
class BroadcastCacheService(private val broadcastService: BroadcastService,
                            private val advertisingService: AdvertisingService) {

    @Value("\${rtmp.server.url:}")
    lateinit var rtmpServerUrl: String


    /**
     * Returns broadcast info from cache;
     * If item is not found in cache, item is fetched from db and put into cache
     *
     * @param broadcastId profile id
     * @return [ru.planeta.api.model.json.BroadcastInfo] instance or null if broadcast for specified profile not found
     * @throws PermissionException if client doesn't have permission to access broadcast
     * @throws NotFoundException   if broadcast for specified profile doesn't exist
     */
    fun getBroadcastCacheItem(broadcastId: Long): BroadcastCacheItem {
        val broadcastInfo = broadcastService.getPublicBroadcastInfo(broadcastId) ?: throw NotFoundException(Broadcast::class.java, broadcastId)
        var advertisingDTO = try {
            advertisingService.loadActiveLocalAdvertising(broadcastInfo.broadcast.broadcastId)
        } catch (e: NotFoundException) {
            try {
                advertisingService.loadActiveGlobalAdvertising()
            } catch (ingored: NotFoundException) {
               null
            }
        }
        broadcastInfo.broadcast.advertising = advertisingDTO
        return BroadcastCacheItem(broadcastInfo)
    }

    /**
     * Returns broadcast stream info
     *
     * @param clientId  client id
     * @param streamId  stream id
     * @return [BroadcastStreamInfo] instance or null
     * @throws PermissionException if client doesn't have permission to access broadcast
     * @throws NotFoundException   if broadcast stream with specified id doesn't exist
     */
    fun getBroadcastStreamInfo(clientId: Long, broadcastId: Long, streamId: Long): BroadcastStreamInfo? {
        val stream = getBroadcastStream(clientId, broadcastId, streamId) ?: return null

        val streamInfo = BroadcastStreamInfo()
        streamInfo.cover = stream.imageUrl

        if (StringUtils.isNotEmpty(stream.broadcastUrl)) {
            // For example http://s1.planeta.ru/live/livestream27?login=asdas&password=asdasd
            streamInfo.host = StringUtils.substringBeforeLast(stream.broadcastUrl, "/")
            streamInfo.stream = StringUtils.substringBeforeLast(StringUtils.substringAfterLast(stream.broadcastUrl, "/"), "?")
        } else {
            streamInfo.host = if (rtmpServerUrl.endsWith("/")) rtmpServerUrl.substring(0, rtmpServerUrl.lastIndexOf("/")) else rtmpServerUrl
            streamInfo.stream = stream.streamUrlId
        }
        return streamInfo
    }

    /**
     * Returns broadcast stream
     *
     * @param clientId  client id
     * @param streamId  stream id
     * @return [BroadcastStream] instance or null
     * @throws PermissionException if client doesn't have permission to access broadcast
     * @throws NotFoundException   if broadcast stream with specified id doesn't exist
     */
    fun getBroadcastStream(clientId: Long, broadcastId: Long, streamId: Long): BroadcastStream? {
        val broadcastCacheItem = getBroadcastCacheItem(broadcastId)
        return IterableUtils.find(broadcastCacheItem.broadcastInfo?.streams) { it.streamId == streamId }
    }
}

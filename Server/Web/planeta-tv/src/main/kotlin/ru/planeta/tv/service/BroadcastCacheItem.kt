package ru.planeta.tv.service

import ru.planeta.api.model.json.BroadcastInfo

/**
 * Broadcast cache item
 *
 * @author m.shulepov
 * Date: 17.04.13
 */
class BroadcastCacheItem(val broadcastInfo: BroadcastInfo)


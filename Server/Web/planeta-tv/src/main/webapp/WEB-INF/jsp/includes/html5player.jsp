<%@include file="/WEB-INF/jsp/includes/generated/taglibs.jsp" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<script id="html5player" type="text/x-jquery-template">
<div class="html5-video-player">
    {{if !isRadio}}
    <div class="html5-video-container">
        <video class="html5-main-video" style="background:#000000;"></video>
    </div>
    {{else}}
        {{if useEqualizer}}
            <div class="html5-video-container">
               <div> ДОБАВИТЬ ЭКВАЛАЙЗЕР<div>
            </div>
        {{else}}
            <div class="html5-video-cover" style="background-image: url({{= imageUrl}});background-size: cover;">
                <div class="html5-video-name-block central-block-wrap">
                    <div class="central-block">
                        <div class="central-block-cont html5-video-name {{= displayTextClass}}" style="{{= displayText}}">
                            Вы слушаете радио на Planeta.ru
                        </div>
                    </div>
                    <div class="vertical-align"></div>
                </div>
            </div>
        {{/if}}
    {{/if}}
    <div class="html5-video-controls">
        {{if !isLive}}
        <div class="html5-video-progress">
            <div class="html5-video-bar">
                <div class="hvb-load" style="width: 50%;"></div>
                <div class="hvb-play" style="width: 25%;">
                    <div class="hvb-track-btn-block">
                        <div class="hvb-track-btn"></div>
                    </div>
                </div>
            </div>
        </div>
        {{/if}}
        <div class="html5-video-ui">
            <div class="hvui-btn hvui-play {{= controls.playPauseButton}}">
                <span class="icon-play"></span>
            </div>
            <div class="hvui-volume-control">
                <div class="hvui-btn hvui-volume">
                    <span id="{{= volumeIcons.id}}" class="icon-vol-max icon-vol-loud icon-vol-quiet icon-vol-min"></span>
                </div>
                <div class="hvui-volume-panel">
                    <div class="hvui-volume-panel-progress">
                        <div id="{{= volumeBar.volumeBarId}}" class="hvui-volume-panel-bar" style="width: 75%;"></div>
                    </div>
                </div>
            </div>
            {{if !isLive}}
            <div class="hvui-time">
                <span class="hvui-time-current">11:23</span>
                <span class="hvui-time-sep">/</span>
                <span class="hvui-time-duration">16:40</span>
            </div>
            {{/if}}
            {{if isLive}}
            <div class="hvui-time">
                <span class="hvui-time-current">Прямая трансляция</span>
            </div>
            {{/if}}
            {{if allowFullScreen}}
            <div class="hvui-btn hvui-fullscreen">
                <span class="icon-fullscreen icon-downsize"></span>
            </div>
            {{/if}}
        </div>
    </div>
    <div class="html5-video-logo"></div>
    {{if is360}}
    <div class="html5-video-instructions">
        <div class="html5-video-instructions-close"></div>
    </div>
    {{/if}}
    <div class="html5-video-state state-play hide">
        <div class="html5-video-state-play"></div>
    </div>
    <div class="html5-video-state state-pause hide">
        <div class="html5-video-state-pause"></div>
    </div>
</div>
</script>
<%@ include file="/WEB-INF/jsp/includes/generated/doctype-header.jsp" %>
<span class="vertical-align"></span>
<c:set var="now" value="<%=new java.util.Date()%>"/>
<c:if test="${broadcast.timeBegin.time > now.time}">

  <script>
    (function() {
      var target_date = new Date(${(broadcast.timeBegin).time});
      var days, hours, minutes, seconds;
      var countdown = $('.broadcast-video-frame .cbo-countdown');

      if (!countdown.length) return;

      var labels = {
        days: {nom: 'день', gen: 'дня', plu: 'дней'},
        hours: {nom: 'час', gen: 'часа', plu: 'часов'},
        minutes: {nom: 'минуту', gen: 'минуты', plu: 'минут'},
        seconds: {nom: 'секунду', gen: 'секунды', plu: 'секунд'}
      };

      function getNumEnding(num, cases) {
        num = Math.abs(num);
        var word = '';
        if (num.toString().indexOf('.') > -1) {
          word = cases.gen;
        } else {
          word = (
                  num % 10 == 1 && num % 100 != 11
                          ? cases.nom
                          : num % 10 >= 2 && num % 10 <= 4 && (num % 100 < 10 || num % 100 >= 20)
                          ? cases.gen
                          : cases.plu
          );
        }
        return word;
      }


      setInterval(function () {
        var current_date = new Date().getTime();
        var seconds_left = (target_date - current_date) / 1000;

        days = parseInt(seconds_left / 86400);
        seconds_left = seconds_left % 86400;

        hours = parseInt(seconds_left / 3600);
        seconds_left = seconds_left % 3600;

        minutes = parseInt(seconds_left / 60);

        seconds = parseInt(seconds_left % 60);

        var stringDate = "";
        if (days && days > 0) {
          stringDate += days + " " + getNumEnding(days, labels.days) + " ";
        }
        if (hours && hours > 0) {
          stringDate += hours +  " " + getNumEnding(hours, labels.hours) + " ";
        }
        if (minutes && minutes > 0) {
          stringDate += minutes + " " + getNumEnding(minutes, labels.minutes) + " ";
        }
        if (seconds && seconds > 0) {
          stringDate += seconds + " " + getNumEnding(seconds, labels.seconds) + " ";
        }

        countdown.html(stringDate);
      }, 1000);

    })();
  </script>
</c:if>
<%@ page contentType="text/html; charset=UTF-8" language="java" pageEncoding="UTF-8"%>
<%@ include file="includes/taglibs.jsp" %>
<%@ include file="/WEB-INF/jsp/includes/generated/doctype-header.jsp" %>
<c:set var="now" value="<%=new java.util.Date()%>"/>
<c:set var="canView" value="${isAuthorized || broadcast.anonymousCanView}"/>
<head>
    <link rel="stylesheet" type="text/css"
          href="https://${properties['tv.application.host']}/css/broadcast-stream-frame.css?_rnd=${pageContext.session.id}"/>
    <link rel="stylesheet" type="text/css" href="https://${hf:getStaticBaseUrl("")}/css/html5-player/css/style.css"/>

    <script type="text/javascript" src="//${hf:getStaticBaseUrl("/res")}/js/broadcast-stream.js"></script>
    <c:if test="${canView && activeStream != null && isPrivateAccessAllowed}">
        <%@include file="includes/generated/stat-counters-head.jsp" %>

        <script type="text/javascript" src="//${hf:getStaticBaseUrl("/res")}/js/broadcast-stream-inner.js"></script>

        <%@include file="includes/html5player.jsp" %>
        <%@include file="includes/html5AdvertisingPlayer.jsp" %>

        <script type="text/javascript">
            $(document).ready(function () {
                $(window).resize(function () {
                    $('#frame-video-player').width($(window).width());
                    $('#frame-video-player').height($(window).height());
                    $("#videoPlayer").planetaPlayer({event: {name: "resize", params: {w: $(window).width(), h: $(window).height()}}});
                });
                $("#videoPlayer").planetaPlayer({
                    embedPlayer: '<c:out value="${embed}"></c:out>',
                    html5playerTmpl: "html5player",
                    html5AdvertisingPlayerTmpl: "html5AdvertisingPlayer",
                    broadcastId:"${broadcast.broadcastId}",
                    advertisingConfig:${hf:toJson(advertisingConfig)}
                });
                $("#videoPlayer").planetaPlayer({event: {name: "resize", params: {w: $(window).width(), h: $(window).height()}}});
            })
        </script>
    </c:if>
    <style>
        #videoPlayer {
            height: 100%;
            width: 100%;
            position: absolute;
            top: 0;
            left: 0
        }
        .stub-image {
            background-repeat:no-repeat;
            background-position: center;
            background-size: cover;
        }
    </style>
    <%--broadcast status check script placed bottom of page--%>
</head>

<body>
<a id="js-ugly-hack-to-open-new-tab-from-js" href="https://tv.planeta.ru" target="_blank" rel="nofollow noopener"></a>
<div class="broadcast-video-frame">
    <c:choose>
        <c:when test="${canView && activeStream != null && isPrivateAccessAllowed &&
                                (broadcast.broadcastStatus == \"LIVE\"
                                || (broadcast.broadcastStatus == \"FINISHED\" && broadcast.archived)
                                || (broadcast.broadcastStatus == \"PAUSED\" && isAdmin))}">

            <c:if test="${activeStream.broadcastStatus == 'PAUSED' && broadcast.broadcastStatus == \"LIVE\"}">

                <img class="central-block" src="${broadcast.pausedImageUrl}" alt="${broadcast.name}">

                <div class="central-block-overlay">
                    <div class="cbo-text broadcast-custom-description">
                        Текущая камера временно отключена.
                    </div>
                    <div class="cbo-text-after"></div>
                </div>
            </c:if>

            <div id="videoPlayer"></div>
        </c:when>
        <c:otherwise>
            <c:choose>
                <c:when test="${canView && not isPrivateAccessAllowed}">
                    <c:set value="${broadcast.closedImageUrl}" var="image"/>
                    <c:set value="${broadcast.closedDescriptionHtml}" var="description"/>
                </c:when>
                <c:when test="${(broadcast.broadcastStatus == \"NOT_STARTED\"
                            || (broadcast.broadcastStatus == \"FINISHED\" && broadcast.millisecondsToStart > 0))}">
                    <c:set value="${broadcast.imageUrl}" var="image"/>
                    <c:set value="${broadcast.imageDescriptionHtml}" var="description"/>
                </c:when>
                <c:when test="${broadcast.broadcastStatus == \"PAUSED\"}">
                    <c:set value="${broadcast.pausedImageUrl}" var="image"/>
                    <c:set value="${broadcast.pausedDescriptionHtml}" var="description"/>
                </c:when>
                <c:when test="${broadcast.broadcastStatus == \"FINISHED\"}">
                    <c:set value="${broadcast.finishedImageUrl}" var="image"/>
                    <c:set value="${broadcast.finishedDescriptionHtml}" var="description"/>
                </c:when>
                <c:when test="${broadcast.broadcastStatus == \"LIVE\" && !canView}">
                    <c:set value="${broadcast.imageUrl}" var="image"/>
                    <c:set value="" var="description"/>
                </c:when>
                <c:otherwise>
                    <c:set value="${broadcast.imageUrl}" var="image"/>
                    <c:set value="" var="description"/>
                </c:otherwise>
            </c:choose>

            <div id="videoPlayer" class="central-block stub-image" alt="${broadcast.name}" style="background-image: url(${image})">
            </div>

            <c:if test="${not empty description}">
                <div class="central-block-overlay">
                    <div class="cbo-text broadcast-custom-description">
                            ${description}
                    </div>
                    <div class="cbo-text-after"></div>
                </div>
            </c:if>
        </c:otherwise>
    </c:choose>
<c:if test="${broadcast.timeBegin.time > now.time}">
    <div class="cbo-countdown-block">
        Трансляция начнется через <b class="cbo-countdown"></b>
    </div>
</c:if>
</div>
<%@include file="includes/stream-page/countdown-script.jsp"%>
<%@include file="includes/stream-page/status-updater-script.jsp"%>
</body>
</html>

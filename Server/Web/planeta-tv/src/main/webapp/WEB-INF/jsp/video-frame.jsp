<%@ page contentType="text/html; charset=UTF-8" language="java" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/includes/generated/doctype-header.jsp" %>

<head>

    <title>${video.name} | Planeta.ru</title>

    <meta name="viewport" content="width=device-width, initial-scale=1">

	<style>
		html{
			height: 100%;
			overflow: hidden;
		}
		body{
			height: 100%;
			margin: 0;
			overflow: hidden;
		}
		video{
			display: block;
			max-width: 100%;
			max-height: 100%;
			width: 100%;
			height: 100%;
		}
		#frame-video-player{
			height: 100%;
		}
		#frame-video-player .video-url{
			width: auto !important;
			height: auto !important;
		}
	</style>

    <script type="text/javascript" src="//${hf:getStaticBaseUrl("/res")}/js/libs-jquery.js?compress=${properties['static.compress']}&flushCache=${properties['static.flushCache']}"></script>
    <script type="text/javascript" src="//${hf:getStaticBaseUrl("/res")}/js/libs.js?compress=${properties['static.compress']}&flushCache=${properties['static.flushCache']}"></script>
    <script type="text/javascript" src="//${hf:getStaticBaseUrl("/res")}/js/framework.js?compress=${properties['static.compress']}&flushCache=${properties['static.flushCache']}"></script>
	<script type="text/javascript" src="//${hf:getStaticBaseUrl("")}/js/utils/storage-utils.js"></script>
	<script type="text/javascript" src="//${hf:getStaticBaseUrl("")}/js/utils/statistic-service.js"></script>
	<script type="text/javascript" src="//${hf:getStaticBaseUrl("")}/js/lib/swfobject.js"></script>

	<script>
        window.VIDEOJS_NO_DYNAMIC_STYLE = true;
	</script>
	<p:script src="planeta-video-player.js"></p:script>


	<link rel="stylesheet" href="//${hf:getStaticBaseUrl("")}/css-generated/vjs-player.css">

</head>

<body>

<script id="statistics-template" type="text/javascript">
    $(function () {
        $('html').css({overflow: 'hidden'});
        var statsService = StatisticService.init({ statServiceUrl: "${properties["statistics.service.url"]}", clientId: "${myProfile.profile.profileId}"});
        var videoModel = new BaseModel(${hf:toJson(video)});
        statsService.trackVideoView(videoModel);
    });
</script>

	<c:choose>
		<c:when test="${video.videoType == 'YOUTUBE' || video.videoType == 'VIMEO' || video.videoType == 'EXTERNAL_HIDDEN'}">
			<script type="text/javascript">
				$(function () {
                    $('#embed-video-container iframe')
                            .css({position: 'absolute', left: 0, top: 0, width: '100%', height: '100%'})
                            .removeAttr('width')
                            .removeAttr('height');
				});
			</script>

			<div id="embed-video-container">
				${video.html}
			</div>
		</c:when>
		<c:otherwise>
			<script type="text/javascript">
				$(function () {

					if ( vjsPlayer.utils.videoSupport() ) {

						var videoElem = $('<video class="video-js" controls="false" preload="none"></video>');
						$('#frame-video-player').append(videoElem);

						var videoInfo = {
							storyboardUrl:'${video.storyboardUrl}',
							qualityModesUrls:${hf:toJson(video.qualityModesUrls)},
							videoUrl:'${video.videoUrl}',
							imageUrl:'${hf:getThumbnailUrl(video.imageUrl, "CAMPAIGN_VIDEO", "VIDEO")}',
							windowWidth:$(window).width(),
							windowHeight:$(window).height()
						};

						var sources = [];
						for (var video in videoInfo.qualityModesUrls) {
							var quality = video.replace('MODE_', '').replace('P', '');
							sources.push({
								res: parseInt(quality),
								label: quality,
								type: "video/mp4",
								src: videoInfo.qualityModesUrls[video]
							})
						}


						vjsPlayer.init(document.querySelector('.video-js'), {
							autoplay: ${autostart},
							poster: videoInfo.imageUrl
						});
						vjsPlayer.playerView();

						vjsPlayer.player.updateSrc(sources);

						vjsPlayer.player.setThumbnails({
							width: 64,
							height: 48,
							thumbWidth: 128,
							thumbHeight: 72,
							spriteUrl: videoInfo.storyboardUrl
						});

					} else {
						$(window).resize(function() {
							$('#frame-video-player').width($(window).width());
							$('#frame-video-player').height($(window).height());
						});

						var flashvars = {
							nologo: true,
							json:"//${properties['tv.application.host']}/api/public/video-extended.json" +
							"&videoId=${video.videoId}&profileId=${video.profileId}&autostart=${autostart}&from=0&resizable=true"
						};
						var params = {
							wmode:'opaque',
							bgcolor:'#000000',
							allowfullscreen:true,
							scale:'noscale',
							allowScriptAccess:'always'
						};
						var attributes = {};

						swfobject.embedSWF("/flash/player.swf?version=1.0.1", "frame-video-player",
								$(window).width(), $(window).height(),
								"9.0.0", "expressInstall.swf", flashvars, params, attributes
						);
					}
				});
			</script>

			<div id="frame-video-player"></div>
		</c:otherwise>
	</c:choose>

</body>
</html>

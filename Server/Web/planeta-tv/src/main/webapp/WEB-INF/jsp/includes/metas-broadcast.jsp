<%@ page contentType="text/html; charset=UTF-8" language="java" pageEncoding="UTF-8"%>
<!-- Sharing meta data: start -->
<meta property="og:site_name" content="Planeta.ru"/>
<c:set var="imageUrl" value="${broadcast.imageUrl}"/>
<c:if test="${empty imageUrl}">
    <c:set var="imageUrl" value='//${hf:getStaticBaseUrl("")}/images/content/defaultnotext.png'/>
</c:if>
<link rel="image_src" href="${hf:getThumbnailUrl(imageUrl, "ORIGINAL", "VIDEO")}"/>

<c:choose>
    <c:when test="${not empty customMetaTag.image}">
        <meta property="og:image" content="${hf:getThumbnailUrl(customMetaTag.image, "ORIGINAL", "VIDEO")}"/>
    </c:when>
    <c:otherwise>
        <meta property="og:image" content="${hf:getThumbnailUrl(imageUrl, "ORIGINAL", "VIDEO")}"/>
    </c:otherwise>
</c:choose>

<c:choose>
    <c:when test="${not empty customMetaTag.ogTitle}">
        <meta property="og:title" content="${hf:escapeHtml4(customMetaTag.ogTitle)}"/>
    </c:when>
    <c:otherwise>
        <meta property="og:title" content="Онлайн трансляция «${hf:escapeHtml4(broadcast.name)}» | Planeta"/>
    </c:otherwise>
</c:choose>

<c:choose>
    <c:when test="${not empty customMetaTag.ogDescription}">
        <meta property="og:description"
              content="${hf:escapeHtml4(customMetaTag.ogDescription)}"/>
    </c:when>
    <c:otherwise>
        <meta property="og:description"
              content="На портале Planeta.ru состоится прямая трансляция &laquo;${hf:escapeHtml4(broadcast.name)}&raquo; ${hf:dateFormatByTemplate(broadcast.timeBegin, "dd MMMM yyyy в HH:mm")}. Архивная запись будет доступна абсолютно бесплатно!"
                />
    </c:otherwise>
</c:choose>
<!-- Sharing meta data: end -->

<c:choose>
    <c:when test="${not empty customMetaTag.title}">
        <title>${hf:escapeHtml4(customMetaTag.title)}</title>
    </c:when>
    <c:otherwise>
        <title>"Онлайн трансляция «${hf:escapeHtml4(broadcast.name)}» | Planeta"</title>
    </c:otherwise>
</c:choose>
<c:choose>
    <c:when test="${not empty customMetaTag.description}">
        <meta name="description" content="${customMetaTag.description}"/>
    </c:when>
    <c:otherwise>
        <meta name="description"
              content="На портале Planeta.ru состоится прямая трансляция &laquo;${hf:escapeHtml4(broadcast.name)}&raquo; ${hf:dateFormatByTemplate(broadcast.timeBegin, "dd MMMM yyyy в HH:mm")}. Архивная запись будет доступна абсолютно бесплатно!"/>
        <%--content="Трансляция события &laquo;<c:out value="${event.profile.name}"/>&raquo; &laquo;${broadcast.name}&raquo;. <c:out value="${hf:stripBbcode(broadcast.description)}"/>"/>--%>
    </c:otherwise>
</c:choose>
<c:if test="${not empty customMetaTag.keywords}">
    <meta name="keywords" content="${customMetaTag.keywords}"/>
</c:if>
<meta name="viewport" content="width=device-width">

<%@ page contentType="text/html; charset=UTF-8" language="java" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/includes/generated/doctype-header.jsp" %>
<head>
    <%@include file="/WEB-INF/jsp/includes/generated/stat-counters-async.jsp"%>
    <%@ include file="/WEB-INF/jsp/includes/head.jsp" %>
    <%@ include file="/WEB-INF/jsp/includes/generated/main-page-meta-header.jsp" %>

    <%@include file="/WEB-INF/jsp/includes/metas-welcome.jsp" %>

    <script id="dashboard-video-item-template" type="text/x-jquery-template">
        <div class="video-card_i">
            <a class="video-card_link"
               {{if videoId}}
               href="{{= 'https://'+workspace.serviceUrls.tvAppUrl+'/video/'+ProfileUtils.getUserLink(profileId, ownerAlias)+'/'+videoId}}">
                {{else}}
                href="{{= 'https://'+workspace.serviceUrls.tvAppUrl+'/broadcast/'+profileId}}">
                {{/if}}

                <span class="video-card_cover">
                    <img src="{{= ImageUtils.getThumbnailUrl(imageUrl,ImageUtils.TV_COVER,ImageType.VIDEO)}}">
                    <span class="video-play"><span class="video-play_ico"></span></span>
                </span>
                <span class="video-card_cont">
                    <span class="video-card_name">
                        {{if name}}
                        {{= name}}
                        {{else}}
                        Видео без названия
                        {{/if}}
                        {{= index}}
                    </span>
                </span>
            </a>
        </div>
    </script>

    <script id="tv-video-not-found-template" type="text/x-jquery-template">
        {{if length == 0}}
        <div class="tv-video-not-found">
            <div class="tv-video-not-found-label">Ничего не найдено по запросу</div>
            <div class="tv-video-not-found-value">
                {{= queryText}}
            </div>
            <div class="tv-video-not-found-all">
                <span class="tv-video-not-found-all-link">Показать все</span>
            </div>
        </div>
        {{/if}}
    </script>

    <script id="tv-filter-search-template" type="text/x-jquery-template">
        <div class="col-12">
            <input type="text" id="searchString" class="tv-search_input" value="{{= queryText}}"
                   placeholder="Поиск по видео">
        </div>
    </script>

    <script>
        var TV = {
            Models: {},
            Views: {}
        };

        TV.Views.Search = BaseView.extend({
            placeholder: "Поиск",
            template: "#tv-filter-search-template",
            className: 'tv-filter-search',
            tagName: 'div',

            events: {
                'input #searchString': 'changeSearchString',
                'keyup #searchString': 'changeSearchString',
                'paste #searchString': 'changeSearchString',
                'change #searchString': 'changeSearchString'
            },

            modelJSON: function () {
                var json = this.model ? this.model.toJSON() : {};
                json.placeholder = this.placeholder;
                return json;
            },

            construct: function (options) {
                if (options.placeholder) {
                    this.placeholder = options.placeholder
                }
                this.collection = options.collection;
                this.notFoundBox = options.notFoundBox;
            },

            changeSearchString: _.debounce(function () {
                var searchQuery = $(this.el).find('#searchString').val();
                this.collection.data.query = searchQuery;
                if (this.oldQuery == this.collection.data.query) return;
                this.oldQuery = this.collection.data.query;
                this.reloadCollection();
            }, 500), //Search.inputDelay
            afterRender:function(){
                $(this.el).find("#searchString").val(this.collection.data.query);
            },
            reloadCollection: function () {
                var self = this;
                this.collection.load().done(function () {
                    if (self.notFoundBox) {
                        self.notFoundBox.queryText = '«' + self.collection.data.query + '»';
                        self.notFoundBox.render();
                    }
                });
            }
        });

        TV.Views.ResultsNotFound = BaseView.extend({
            queryText: "",
            template: "#tv-video-not-found-template",

            modelJSON: function () {
                var json = this.model ? this.model.toJSON() : {};
                json.queryText = this.queryText;
                json.length = this.collection.length;
                return json;
            },

            construct: function (options) {
                if (options.queryText) {
                    this.queryText = options.queryText
                }
                this.collection = options.collection;
            },

            afterRender: function () {
                var self = this;
                this.$('.tv-video-not-found-all-link').click(function (e) {
                    self.collection.data.query = '';

                    self.collection.load().done(function () {
                        self.render();
                    });
                    $('#searchString').val("");
                    self.queryText = '';
                });
            }
        });

        TV.Models.BaseTvMainCollection = BaseCollection.extend({
            url: '/search-broadcasts.json',
            limit: 20,
            query:"школа",

            parse: function (result) {
                if (!result) return {success: false};
                if (!result.searchResultRecords) return [];

                //broadcast do not have field named "id"
                //replace zero id with null
                _.each(result.searchResultRecords, function (el) {
                    if (el.id === 0) {
                        el.id = null;
                    }
                });

                return result.searchResultRecords;
            }
        });

        TV.Views.BroadcastListItem = BaseView.extend({
            template: "#dashboard-video-item-template",
            replaceTag: true
        });

        TV.Views.BroadcastList = DefaultContentScrollListView.extend({
            el: '.video-card',
            itemViewType: TV.Views.BroadcastListItem
        });

        $(document).ready(function () {

            var model = new TV.Models.BaseTvMainCollection([]);
            var view = new TV.Views.BroadcastList({
                el: '.js-video-container',
                collection: model
            });
            view.render();
            model.data.query = "${query}";
            model.load();
            var notFoundBox = new TV.Views.ResultsNotFound({
                el: '.js-video-not-found-container',
                collection: model
            });

            var searchBox = new TV.Views.Search({
                el: '#js-search-input',
                collection: model,
                notFoundBox: notFoundBox
            });
            searchBox.render();
            searchBox.collection = model;
        })
    </script>

    <title></title>
</head>
<body>
<%@ include file="/WEB-INF/jsp/includes/generated/header.jsp" %>

<div id="global-container">
    <div id="main-container" class="wrap-container">


        <div class="tv-search">
            <div class="wrap" id="js-search-input">

            </div>
        </div>
        <script>
            $('.tv-search_input').focus();
        </script>


        <div class="wrap">
            <div class="col-12">
                <div class="video-card js-video-container">

                </div>
                <div class="js-video-not-found-container">

                </div>
            </div>
        </div>


    </div>
</div>
</body>
</html>

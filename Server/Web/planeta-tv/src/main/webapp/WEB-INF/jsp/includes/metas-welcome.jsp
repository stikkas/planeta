<%@ page contentType="text/html; charset=UTF-8" language="java" pageEncoding="UTF-8"%>
<!-- Sharing meta data: start -->
<meta property="og:site_name" content="Planeta.ru"/>

<c:if test="${not empty customMetaTag.image}">
    <meta property="og:image" content="${hf:getThumbnailUrl(customMetaTag.image, "ORIGINAL", "VIDEO")}" />
</c:if>

<c:choose>
    <c:when test="${not empty customMetaTag.ogTitle}">
        <meta property="og:title" content="${customMetaTag.ogTitle}" />
    </c:when>
    <c:otherwise>
        <meta property="og:title" content="Планета: телевидение" />
    </c:otherwise>
</c:choose>

<c:choose>
    <c:when test="${not empty customMetaTag.ogDescription}">
        <meta property="og:description" content="${customMetaTag.ogDescription}" />
    </c:when>
    <c:otherwise>
        <meta property="og:description" content="Онлайн-трансляции концертов, архив видеозаписей, эксклюзивные видеоклипы в высоком качестве." />
    </c:otherwise>
</c:choose>
<!-- Sharing meta data: end -->
<c:choose>
    <c:when test="${not empty customMetaTag.title}">
        <title>${customMetaTag.title}</title>
    </c:when>
    <c:otherwise>
        <title>Планета: телевидение</title>
    </c:otherwise>
</c:choose>

<c:choose>
    <c:when test="${not empty customMetaTag.description}">
        <meta name="description" content="${customMetaTag.description}"/>
    </c:when>
    <c:otherwise>
        <meta name="description" content="<c:out value="${product.description}"/> цена: <fmt:formatNumber value="${activeChild.price}"/> руб."/>
    </c:otherwise>
</c:choose>

<c:if test="${not empty customMetaTag.keywords}">
    <meta name="keywords" content="${customMetaTag.keywords}"/>
</c:if>

<script type="application/ld+json">
    {
        "@context": "http://schema.org",
        "@type": "WebSite",
        "url": "https://tv.planeta.ru/",
        "potentialAction": {
        "@type": "SearchAction",
        "target": "https://www.google.com/search?q={search_term_string}%20site:tv.planeta.ru",
        "query-input": "required name=search_term_string"
        }
    }
</script>
<meta name="viewport" content="width=device-width">

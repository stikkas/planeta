<%@include file="/WEB-INF/jsp/includes/generated/taglibs.jsp" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<script id="html5AdvertisingPlayer" type="text/x-jquery-template">
<div class="html5-video-player">
    <div id="{{= adFrame}}" style="display: none"></div>
    <div class="html5-video-container" id="{{= clickOnMe}}">
        <video class="html5-main-video" id="{{= advertisingVideoPlayer}}" style="background:#000000;"></video>
    </div>
    <div class="ad-controls">
        <div class="ad-progress">
            <div class="ad-bar">
                <div class="ad-bar-load" style="width: 0%;"></div>
                <div class="ad-bar-play {{= durationBar}}" style="width: 0%;"></div>
            </div>
        </div>
        <div class="ad-ui">
            <div class="ad-ui-btn">
                <span class="btn btn-primary {{= clickOnMe}}">{{= buttonText}}</span>
            </div>
            <div class="ad-ui-text" style="display:none">
                <a href="javascript:void(0);" class="{{= skipAdButton}}"> Пропустить рекламу</a>
            </div>
            <div class="ad-ui-text" style="display:none">
                Рекламу можно будет пропустить через
                <span id="{{= timeToSkip}}">{{= timeToSkipValue}}</span>
                секунд.
            </div>
        </div>
    </div>
</div>
</script>
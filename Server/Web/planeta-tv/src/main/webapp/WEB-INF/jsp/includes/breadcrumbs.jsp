<%@ page language="java" pageEncoding="UTF-8" contentType="text/html; charset=UTF-8" %>
<%@ include file="/WEB-INF/jsp/includes/taglibs.jsp" %>

<c:set var="titleLengthLimit" value="48"></c:set>
<c:set var="titleGroup" value="${currentGroup != null ? currentGroup : group}"></c:set>

<ul class="pl-breadcrumb">
    <li class="breadcrumb-item">
        <a class="link-icon" href="/">
            <i class="icon-house"></i>
            <span class="text-icon">Проекты planeta.ru</span>
        </a>
    </li>
    <li class="breadcrumb-item separator"></li>

    <c:choose>
        <c:when test="${action == \"admin/campaigns\"}">
            <li class="breadcrumb-item active">
                <span class="link-icon">
                    <i class="icon-action"></i>
                    <span class="text-icon">Проекты сообщества</span>
                </span>
            </li>
        </c:when>
        <c:when test="${action == \"group-campaigns\"}">
            <li class="breadcrumb-item active">
                <span class="link-icon">
                    <i class="icon-action"></i>
                    <span class="text-icon">${hf:getShrinkedString(titleGroup.name, titleLengthLimit)}</span>
                </span>
            </li>
        </c:when>
        <c:when test="${action == \"campaign-view\"}">
            <li class="breadcrumb-item">
                <a class="link-icon" href="/campaigns/group/${hf:getProfileAlias(titleGroup.profileId, titleGroup.alias)}">
                    <i class="icon-group"></i>
                    <span class="text-icon">${hf:getShrinkedString(titleGroup.name, titleLengthLimit)}</span>
                </a>
            </li>
            <li class="breadcrumb-item separator"></li>
            <li class="breadcrumb-item active">
                <span class="link-icon">
                    <i class="icon-action"></i>
                    <span class="text-icon">${hf:getShrinkedString(campaign.name, titleLengthLimit)}</span>
                </span>
            </li>
        </c:when>
        <c:when test="${action == \"admin/campaign-orders\"}">
            <li class="breadcrumb-item">
                <a class="link-icon" href="/campaigns/group/${hf:getProfileAlias(titleGroup.profileId, titleGroup.alias)}">
                    <i class="icon-group"></i>
                    <span class="text-icon">${hf:getShrinkedString(titleGroup.name, titleLengthLimit)}</span>
                </a>
            </li>
            <li class="breadcrumb-item separator"></li>
            <li class="breadcrumb-item">
                <a class="link-icon" href="/campaigns/${campaign.campaignId}">
                    <i class="icon-action"></i>
                    <span class="text-icon">${hf:getShrinkedString(campaign.name, titleLengthLimit)}</span>
                </a>
            </li>
            <li class="breadcrumb-item separator"></li>
            <li class="breadcrumb-item active">
                <span class="link-icon">
                    <i class="icon-profile-settings"></i>
                    <span class="text-icon">Список покупок</span>
                </span>
            </li>
        </c:when>
        <c:when test="${action == \"admin/campaign-edit\"}">
            <li class="breadcrumb-item">
                <a class="link-icon" href="/campaigns/group/${hf:getProfileAlias(titleGroup.profileId, titleGroup.alias)}">
                    <i class="icon-group"></i>
                    <span class="text-icon">${hf:getShrinkedString(titleGroup.name, titleLengthLimit)}</span>
                </a>
            </li>
            <li class="breadcrumb-item separator"></li>
            <c:choose>
                <c:when test="${campaign.status == \"DRAFT\"}">
                    <li class="breadcrumb-item active">
                        <span class="link-icon">
                            <i class="icon-profile-settings"></i>
                            <span class="text-icon">Создание проекта</span>
                        </span>
                    </li>
                </c:when>
                <c:otherwise>
                    <li class="breadcrumb-item">
                        <a class="link-icon" href="/campaigns/${campaign.campaignId}">
                            <i class="icon-action"></i>
                            <span class="text-icon">${hf:getShrinkedString(campaign.name, titleLengthLimit)}</span>
                        </a>
                    </li>
                    <li class="breadcrumb-item separator"></li>
                    <li class="breadcrumb-item active">
                        <span class="link-icon">
                            <i class="icon-profile-settings"></i>
                            <span class="text-icon">Редактирование проекта</span>
                        </span>
                    </li>
                </c:otherwise>
            </c:choose>
        </c:when>
    </c:choose>

</ul>
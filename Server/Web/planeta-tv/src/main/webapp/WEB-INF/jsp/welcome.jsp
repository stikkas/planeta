<%@ page contentType="text/html; charset=UTF-8" language="java" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/includes/generated/doctype-header.jsp" %>
<head>
    <%@include file="/WEB-INF/jsp/includes/generated/stat-counters-async.jsp"%>
    <%@ include file="/WEB-INF/jsp/includes/head.jsp" %>
    <%@ include file="/WEB-INF/jsp/includes/generated/main-page-meta-header.jsp" %>

    <%@include file="/WEB-INF/jsp/includes/metas-welcome.jsp" %>

    <script id="dashboard-video-item-template" type="text/x-jquery-template">
        <div class="video-card_i <%--video-card_i__large--%>">
            <a class="video-card_link"
               {{if videoId}}
               href="{{= 'https://'+workspace.serviceUrls.tvAppUrl+'/video/'+ProfileUtils.getUserLink(profileId, ownerAlias)+'/'+videoId}}">
                {{else}}
                href="{{= 'https://'+workspace.serviceUrls.tvAppUrl+'/broadcast/'+broadcastId}}">
                {{/if}}

                <span class="video-card_cover">
                    {{if $data.isBig}}
                        <img src="{{= ImageUtils.getThumbnailUrl(imageUrl,ImageUtils.TV_BIG_COVER,ImageType.VIDEO)}}">
                    {{else}}
                        <img src="{{= ImageUtils.getThumbnailUrl(imageUrl,ImageUtils.TV_COVER,ImageType.VIDEO)}}">
                    {{/if}}
                    <span class="video-play"><span class="video-play_ico"></span></span>
                </span>
                <span class="video-card_cont">
                    <span class="video-card_name">
                        {{if name}}
                        {{= name}}
                        {{else}}
                        Видео без названия
                        {{/if}}
                        {{= index}}
                    </span>
                    <span class="video-card_views">
                    {{= StringUtils.humanNumber(viewsCount)}} {{= StringUtils.declOfNum(viewsCount, ["просмотр", "просмотра", "просмотров"])}}
                    </span>
                </span>
            </a>
        </div>
    </script>

    <script id="tv-filter-search-template" type="text/x-jquery-template">
        <div class="input-icon input-icon-left input-field">
            <input id="searchString" name="searchString" type="text" placeholder="{{= placeholder}}">
            <i class="icon-search icon-gray"></i>
        </div>
    </script>

    <script id="tv-video-not-found-template" type="text/x-jquery-template">
        {{if length == 0}}
        <div class="tv-video-not-found">
            <div class="tv-video-not-found-label">Ничего не найдено по запросу</div>
            <div class="tv-video-not-found-value">
                {{= queryText}}
            </div>
            <div class="tv-video-not-found-all">
                <span class="tv-video-not-found-all-link">Показать все</span>
            </div>
        </div>
        {{/if}}
    </script>

    <script type="text/javascript">
        var TV = {
            Models: {},
            Views: {}
        };

        TV.Views.ResultsNotFound = BaseView.extend({
            queryText: "",
            template: "#tv-video-not-found-template",

            modelJSON: function () {
                var json = this.model ? this.model.toJSON() : {};
                json.queryText = this.queryText;
                json.length = this.collection.length;
                return json;
            },

            construct: function (options) {
                if (options.queryText) {
                    this.queryText = options.queryText
                }
                this.collection = options.collection;
            },

            afterRender: function () {
                var self = this;
                this.$('.tv-video-not-found-all-link').click(function (e) {
                    self.collection.data.query = '';

                    self.collection.load().done(function () {
                        self.render();
                    });
                    $('#searchString').val("");
                    self.queryText = '';
                });
            }
        });

        TV.Models.BaseTvMainCollection = BaseCollection.extend({
            url: '/search-broadcasts.json',
            limit: 13,

            parse: function (result) {
                if (!result) return {success: false};
                if (!result.searchResultRecords) return [];

                //broadcast do not have field named "id"
                //replace zero id with null
                _.each(result.searchResultRecords, function (el) {
                    if (el.id === 0) {
                        el.id = null;
                    }
                });

                return result.searchResultRecords;
            }
        });

        TV.Models.BaseTvBroadcastCollection = TV.Models.BaseTvMainCollection.extend({
            'query': '',
            'sortBy': 1,
            broadcastModel: true
        });

        TV.Models.ArchivedBroadcastCollectionNewConcerts = TV.Models.BaseTvBroadcastCollection.extend({
            data: {
                'broadcastCategory': 'CONCERTS'
            }
        });

        TV.Models.ArchivedBroadcastCollectionNewNashe360 = TV.Models.BaseTvBroadcastCollection.extend({
            data: {
                'broadcastCategory': 'NASHE_360'
            }
        });

        TV.Models.ArchivedBroadcastCollectionNewAlive = TV.Models.BaseTvBroadcastCollection.extend({
            data: {
                'broadcastCategory': 'ALIVE'
            }
        });

        TV.Models.ArchivedBroadcastCollectionNewSchool = TV.Models.BaseTvBroadcastCollection.extend({
            data: {
                'broadcastCategory': 'SCHOOL'
            }
        });

        TV.Views.BroadcastListItem = BaseView.extend({
            template: "#dashboard-video-item-template",
            replaceTag: true
        });

        TV.Views.BroadcastList = DefaultContentScrollListView.extend({
            el: '.video-card',
            itemViewType: TV.Views.BroadcastListItem,
            onAdd: function (model) {
                if (this.collection.indexOf(model) === 0) {
                    model.set('isBig', true);
                }
                return DefaultContentScrollListView.prototype.onAdd.apply(this, arguments);
            }
        });

        function getBroadcastSectionId() {
            return parseInt(StorageUtils.getBroadcastSectionId() || 0, 10);
        }

        $(document).ready(function () {

            var collections = [];
            var i = 0;

            var broadcastArchiveNewSchoolView = new TV.Views.BroadcastList({
                el: '#school-broadcast-new-tab',
                collection: new TV.Models.ArchivedBroadcastCollectionNewSchool([])
            });
            broadcastArchiveNewSchoolView.render();
            broadcastArchiveNewSchoolView.collection.load();
            collections[i++] = broadcastArchiveNewSchoolView.collection;

            var broadcastArchiveNewAliveView = new TV.Views.BroadcastList({
                el: '#alive-broadcast-new-tab',
                collection: new TV.Models.ArchivedBroadcastCollectionNewAlive([])
            });
            broadcastArchiveNewAliveView.render();
            broadcastArchiveNewAliveView.collection.load();
            collections[i++] = broadcastArchiveNewAliveView.collection;

            var broadcastNewNashe360View = new TV.Views.BroadcastList({
                el: '#nashe360-broadcast-new-tab',
                collection: new TV.Models.ArchivedBroadcastCollectionNewNashe360([])
            });
            broadcastNewNashe360View.render();
            broadcastNewNashe360View.collection.load();
            collections[i++] = broadcastNewNashe360View.collection;

            var broadcastArchiveNewConcertsView = new TV.Views.BroadcastList({
                el: '#concerts-broadcast-new-tab',
                collection: new TV.Models.ArchivedBroadcastCollectionNewConcerts([])
            });
            broadcastArchiveNewConcertsView.render();
            broadcastArchiveNewConcertsView.collection.load();
            collections[i++] = broadcastArchiveNewConcertsView.collection;

            _.each(collections, function (el) {
                el.limit = 12;
            });

            //------------------------------------------------- tabs logic -------------------------------------
            var sectionId = getBroadcastSectionId();
            <c:if test = "${tabIdFromUrl != null}">
            sectionId = ${tabIdFromUrl};
            StorageUtils.setBroadcastSectionId(sectionId);
            $('html, body').animate({
                scrollTop: $(".js-category-tabs-list").offset().top
            }, 2000);
            </c:if>

            var notFoundBox = new TV.Views.ResultsNotFound({
                el: '.tv-video-not-found-container',
                collection: collections[sectionId]
            });

            $($('.js-category-tabs')[sectionId]).addClass('active');
            $('.js-tv-video-category-container[data-tab-id=' + sectionId + ']').removeClass('hide');

            var sectionNames = ["school", "alive", "nasheradio", "concerts"];
            var getSectionName = function (tabId) {
                return sectionNames[tabId];
            };

            $('.js-category-tabs').click(function (e) {
                e.preventDefault();

                $('.js-category-tabs').removeClass('active');
                var tabSwitch = $(e.currentTarget);
                tabSwitch.addClass('active');
                //
                $('.js-tv-video-category-container').addClass('hide');
                $('.js-tv-video-list-container').addClass('hide');
                $('.video-card').removeClass('active');

                sectionId = parseInt(tabSwitch.attr('data-tab-id'));
                window.history.pushState({}, "", getSectionName(sectionId));

                if (collections[sectionId].length < 1) {
                    collections[sectionId].load().done(function () {
                        collections[sectionId].render();
                    });
                }

                $('.js-tv-video-category-container[data-tab-id=' + sectionId + ']').removeClass('hide');

                StorageUtils.setBroadcastSectionId(sectionId);
                $('html, body').animate({
                    scrollTop: $(".js-category-tabs-list").offset().top
                }, 0);

                CustomMetaTagsUtils.getCustomTagForThisPage().done(function (response) {
                    if (response && response.success && response.result) {
                        var customTags = response.result;
                        if (customTags.ogTitle) {
                            $("meta[property='og:title']").attr('content', customTags.ogTitle);
                        }
                        if (customTags.ogDescription) {
                            $("meta[property='og:description']").attr('content', customTags.ogDescription);
                        }
                        if (customTags.image) {
                            $("meta[property='og:image']").attr('content', ImageUtils.getThumbnailUrl(customTags.image, ImageUtils.ORIGINAL, ImageType.PHOTO));
                        }

                        if (customTags.title) {
                            $("title").attr('name', customTags.title);
                        }
                        if (customTags.description) {
                            $("meta[name='description']").attr('content', customTags.ogTitle);
                        }
                    }
                });
            });
        });
    </script>

</head>

<body>
<%@ include file="/WEB-INF/jsp/includes/generated/irma/header.jsp" %>
<%@include file="/WEB-INF/jsp/includes/generated/irma/main-nav.jsp" %>

<div id="global-container">
    <div id="main-container" class="wrap-container">

        <div class="tv-main">
            <div class="wrap">
                <div class="col-12">

                    <div class="tv-main_cont">
                        <div class="tv-main_slider">
                            <div class="tv-main_slider_list owl-carousel">
                                <c:forEach items="${promoBroadcasts}" var="promoBroadcast" varStatus="st">
                                    <div class="tv-main_slider_i">
                                        <a href="/broadcast/${promoBroadcast.broadcastId}">
                                            <span class="tv-main_slider_name">
                                                ${promoBroadcast.name}
                                            </span>
                                            <img class="owl-lazy" data-src="${hf:getThumbnailUrl(promoBroadcast.imageUrl, "ORIGINAL", "BROADCAST")}">
                                        </a>
                                    </div>
                                </c:forEach>
                            </div>
                        </div>
                    </div>
                    <script>
                        $(function () {
                            var owl = $(".tv-main_slider_list").owlCarousel({
                                lazyLoad: true,
                                autoplay: false,
                                autoplayTimeout: 5000,
                                autoplayHoverPause: true,
                                loop: true,
                                items: 1,
                                slideBy: 'page',
                                nav: true,
                                dots: false,
                                margin: 0,
                                mouseDrag: false,
                                smartSpeed: 600,
                                onInitialized: owlInitialized
                            });

                            function owlInitialized(event) {
                                var $prev = $('.owl-prev', event.target);
                                var $next = $('.owl-next', event.target);

                                $prev.html('<span class="s-icon s-icon-arrow-thin-left">');
                                $next.html('<span class="s-icon s-icon-arrow-thin-right">');
                            }
                        });
                    </script>
                </div>
            </div>
        </div>

        <div class="tv-type">
            <div class="wrap">
                <div class="col-12">
                    <div class="tv-type_slider">
                        <div class="tv-type_list">
                            <c:forEach items="${upcomingBroadcasts}" var="broadcast" varStatus="st">
                                <c:set var="liveClass" value=""/>
                                <c:if test="${broadcast.broadcastStatus == 'LIVE'}">
                                    <c:set var="liveClass" value="tv-type_i__now"/>
                                </c:if>
                                <a href="/broadcast/${broadcast.broadcastId}" class="tv-type_i ${liveClass}">
                                       <span class="tv-type_meta">
                                            <c:if test="${broadcast.broadcastStatus == 'LIVE'}">
                                            <span class="tv-type_meta_ico">
                                               <span class="s-tv-type-live"></span>
                                            </span>
                                             <span class="tv-type_meta_name">
                                               сейчас в эфире!
                                             </span>
                                            </c:if>
                                            <c:if test="${broadcast.broadcastStatus != 'LIVE'}">
                                                <c:if test="${!broadcast.archived}">
                                                    <span class="tv-type_meta_name">
                                                        Начало ${hf:dateFormatByTemplate(broadcast.timeBegin, "d")} ${hf:dateFormatByTemplate(broadcast.timeBegin, "MMMM")} в ${hf:dateFormatByTemplate(broadcast.timeBegin, "H:mm")}
                                                    </span>
                                                </c:if>
                                            </c:if>

                                       </span>
                                       <span class="tv-type_img">
                                           <img class="owl-lazy" data-src="${broadcast.imageUrl}">
                                       </span>
                                       <span class="tv-type_name">
                                           ${broadcast.name}
                                       </span>
                                </a>
                            </c:forEach>
                        </div>
                    </div>
                    <script>
                        $(function () {
                            var owl = $(".tv-type_list").addClass('owl-carousel').owlCarousel({
                                lazyLoad: true,
                                autoplay: false,
                                autoplayTimeout: 5000,
                                autoplayHoverPause: true,
                                loop: false,
                                navRewind: false,
                                items: 4,
                                slideBy: 4,
                                nav: true,
                                dots: false,
                                margin: 20,
                                mouseDrag: false,
                                smartSpeed: 200,
                                onInitialized: owlInitialized
                            });

                            function owlInitialized(event) {
                                var $prev = $('.owl-prev', event.target);
                                var $next = $('.owl-next', event.target);

                                $prev.html('<span class="s-icon s-icon-arrow-thin-left">');
                                $next.html('<span class="s-icon s-icon-arrow-thin-right">');
                            }
                        });
                    </script>
                </div>
            </div>
        </div>
        <div class="tv-list">
            <div class="wrap">
                <div class="col-12">
                    <div class="tv-list-filter js-category-tabs-list">
                        <div class="tv-list-filter_list">
                            <div data-tab-id="0" class="tv-list-filter_i js-category-tabs">
                                <span class="tv-list-filter_link">Школа краудфандинга</span>
                            </div>
                            <div data-tab-id="1" class="tv-list-filter_i js-category-tabs">
                                <span class="tv-list-filter_link">Своё радио</span>
                            </div>
                            <div data-tab-id="2" class="tv-list-filter_i js-category-tabs">
                                <span class="tv-list-filter_link">Наше радио</span>
                            </div>
                            <div data-tab-id="3" class="tv-list-filter_i js-category-tabs">
                                <span class="tv-list-filter_link">Архив</span>
                            </div>
                        </div>
                    </div>

                    <div data-tab-id="0" class="js-tv-video-category-container hide">
                        <div id="school-broadcast-new-tab" class="video-card video-card__main">
                        </div>
                    </div>

                    <div data-tab-id="1" class="js-tv-video-category-container hide">
                        <div id="alive-broadcast-new-tab" class="video-card video-card__main">
                        </div>
                    </div>

                    <div data-tab-id="2" class="js-tv-video-category-container hide">
                        <div id="nashe360-broadcast-new-tab" class="video-card video-card__main">
                        </div>
                    </div>

                    <div data-tab-id="3" class="js-tv-video-category-container hide">
                        <div id="concerts-broadcast-new-tab" class="video-card video-card__main">
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

</div>
<%@ include file="/WEB-INF/jsp/includes/generated/irma/footer.jsp" %>

<script>
    $('img[data-src]').each(function () {
        var $this = $(this);
        $this.attr('src', ImageUtils.getThumbnailUrl($this.attr('data-src'), ImageUtils.TV_ITEM_MAIN_SLIDER));
    });
</script>
</body>
</html>

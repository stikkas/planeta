<%@ include file="/WEB-INF/jsp/includes/generated/doctype-header.jsp" %>
<script>
  (function(){
    var oldStatus = "${broadcast.broadcastStatus}";
    var oldStreamStatus = "${activeStream.broadcastStatus}";
    var checkStatus = function(timeout) {
      setTimeout(function(){
        $.ajax({
          url:"/broadcast/${broadcast.broadcastId}/get-status.json",
          cache:false,
          success:function(data){
            if (data.success){
              if (data.result.broadcastStatus != oldStatus
                      || data.result.streamStatuses["${activeStream.streamId}"] != oldStreamStatus){
                document.location.reload();
              } else {
                if (data.result.broadcastStatus == 'LIVE'){
                  checkStatus(5000);
                } else {
                  checkStatus();
                }
              }
            } else {
              checkStatus();
            }
          },
          error : function(data){
            checkStatus();
          }
        })
      },timeout || 5000);
    };
    checkStatus();
  })();
</script>

<%@ page contentType="text/html; charset=UTF-8" language="java" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/includes/generated/doctype-header.jsp" %>
<%@include file="includes/generated/taglibs.jsp" %>

<c:set var="broadcast" value="${broadcastInfo.broadcast}"/>
<c:set var="canView" value="${isAuthorized || broadcast.anonymousCanView}"/>
<head>
<%@include file="/WEB-INF/jsp/includes/generated/stat-counters-async.jsp"%>
<%@ include file="/WEB-INF/jsp/includes/head.jsp" %>

<%@include file="/WEB-INF/jsp/includes/metas-broadcast.jsp"%>

<script type="text/javascript">
    var VALIDATE_PERIOD = 60000;
    var AUTO_SUBSCRIBE_PERIOD = 60000;
    var RELOAD_BROADCAST_ON_CHANGE_TIMER_PERIOD = 60000;

    var calculateViewsCount = function (viewingCookieName, currentStreamId) {
        try {
            if (viewingCookieName
                    && !CookieProvider.get(viewingCookieName)
                    && ${canView
                        && isAvailableToViewing}
                    && currentStreamId) {
                $.ajax({
                    type: "POST",
                    url: '/broadcast/broadcast-views-increase.json',

                    data: {
                        defaultStreamId: currentStreamId
                    },
                    success: function (response) {
                        if (response && response.success) {
                            var date = new Date();
                            date.setDate(date.getDate() + 1);
                            CookieProvider.set(viewingCookieName, true, {expires: date.toUTCString()});
                        }
                    },
                    error: function (result) {
                        console.log("Error, while fetching count data");
                    }
                });
            }
        } catch (exception) {
            console.log("Calculation error count  of views: " + exception);
        }
    };

    $(function () {
        <c:if test="${isGeoAllowed}">
        <c:if test="${broadcast.chatEnabled}">

        var chat = new Chat.Models.Chat({
            profileId: '${broadcast.profileId}',
            ownerObjectId: '${broadcast.broadcastId}',
            ownerObjectType: 'BROADCAST'
        });

        chat.fetch({
            success: function () {
                var chatView = new Chat.Views.ChatContainer({
                    el: '#chat-container',
                    model: chat
                });
                chatView.render();
            }
        });

        window.onbeforeunload = function () {
            if (chat) {
                chat.leaveChat();
            }
        };
        </c:if>
        </c:if>

        <c:if test="${!broadcast.archived}">
            var broadcastSubscriptionModel = new BroadcastHelper.Models.BroadcastSubscription(${hf:toJson(subscriptionDTO)});
            var broadcastSubscriptionView = new BroadcastHelper.Views.BroadcastSubscription({
                el: '#broadcast-subscription-container',
                model: broadcastSubscriptionModel
            });
            broadcastSubscriptionView.render();
        </c:if>

        $('.broadcast-register').click(function (e) {
            LazyHeader.showAuthForm('registration');
            CookieProvider.set('gtmRegistration', 'broadcast_success_registration', {path: '/', expires: 'Session'});
            e.stopPropagation();
        });

        $('.broadcast-login').click(function (e) {
            LazyHeader.showAuthForm('signup');
            e.stopPropagation();
        });

        var container = $('.broadcast-stream-description, .broadcast-custom-description');
        RichMediaUtils.processEl(container, {
            audio: '#common-audio-template',
            photo: '#common-photo-template',
            video: '#common-video-template'
        });
        RichMediaUtils.bindHandlers(container);

        var uri = window.location.protocol + '//' + window.location.host + '${requestScope['javax.servlet.forward.request_uri']}';
        var sharesRedraw = function () {
            $('.js-share-container').empty();
            $('.js-share-container').share({className: 'horizontal donate-sharing', counterEnabled: false, hidden: false, parseMetaTags: true, url: uri});
        };

        $(window).bind('resize', sharesRedraw);
        sharesRedraw();


        <c:if test="${isGeoAllowed && isBackersAllowed}">

        var broadcastId = '${broadcast.broadcastId}';
        var currentStreamId = '${defaultStreamId}';

        var changeStream = function () {
            var id = +(!!window.location.hash ? window.location.hash.replace('#', '') : 0);
            id = id ? id : +currentStreamId;

            $('.stream-link').removeClass('active');
            var target = $('.stream-link[data-stream-id=' + id + ']');

            if (!target.length) {
                target = $('.stream-link:first');
            }

            if (target.length){
                target.addClass('active');
                id = +target.attr("data-stream-id");
            }

            if (id != 0) {
                $('#stream-content-placeholder').attr("src", '/broadcast-stream-content.html?broadcastId=' + broadcastId + "&streamId=" + id);
                var viewingCookieName = 'viewingCounted_' + broadcastId +'_' + id;
                calculateViewsCount(viewingCookieName, id);
            }
        };

        $(window).on('hashchange', changeStream);

        changeStream();

        <c:if test="${canView && broadcast.millisecondsToStart > 0 && broadcast.autoStart}">
        JobManager.scheduleOnce('broadcast-stream-live-timer', changeStream, ${broadcast.millisecondsToStart} +1000);
        </c:if>

        <c:if test="${canView && broadcast.closed && isPrivateAccessAllowed}">
        var validatePrivateBroadcast = function () {
            $.ajax({
                url: '/broadcast/validate-private-broadcast.json',

                data: {
                    broadcastId: ${broadcast.broadcastId}
                },
                success: function (response) {
                    if (!response.success || !response.result) {
                        JobManager.removeJob('validate-private-broadcast');
                        changeStream();
                    }
                },
                error: function (result) {
                    console.log("Error, while fetching data");
                }
            });
        };
        JobManager.schedule('validate-private-broadcast', validatePrivateBroadcast, 0, VALIDATE_PERIOD, {
            context: this
        });
        </c:if>
        </c:if>
    });
</script>

</head>
<c:if test="${fn:length(broadcastInfo.streams) > 1}">
    <c:set value="body-tv-channel" var="bodyClass"/>
</c:if>
<body class="${bodyClass}">
<%@ include file="/WEB-INF/jsp/includes/generated/irma/header.jsp" %>
<%@ include file="/WEB-INF/jsp/includes/generated/irma/main-nav.jsp" %>

<div id="global-container">

<div id="main-container" class="wrap-container">

    <div class="cont-header">
        <c:if test="${isAdmin}">
        <%--admin bar--%>
        <div class="cont-header_row">
            <div class="wrap">
                <div class="col-12">
                    <div class="top-breads">
                        <div class="tb-bottom">
                            <div class="pull-right">
                                <c:if test="${broadcast.broadcastStatus == \"PAUSED\" && isAdmin}">
                                    <span class="tb-bottom-msg">
                                        Трансляция работает в режиме видимости администраторам
                                    </span>
                                </c:if>
                            </div>
                            <a href="https://${properties['admin.application.host']}/moderator/event-broadcast-info.html?broadcastId=${broadcast.broadcastId}"
                               class="btn btn-small"><i class="icon-edit"></i> Редактировать</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </c:if>
    </div>


    <div class="tv-main">
        <div class="wrap">
            <div class="col-12">
                <div class="tv-stream-name">
                    <h1 class="tv-stream-name_title">
                        <c:out value="${broadcast.name}"/>
                    </h1>
                    <c:if test="${broadcast.closed && isPrivateAccessAllowed && not empty broadcastPrivateTargeting}">
                        <div class="tv-stream-name_tempolink">
                            Вы перешли на эту трансляцию по временной ссылке. Трансляция будет доступна для Вас еще
                            <b>
                                <c:if test="${hf:getDateDifferenceInDays(now, broadcastPrivateTargeting.expirationTime) > 0}">
                                    <fmt:formatDate value="${broadcastPrivateTargeting.expirationTime}" pattern="dd.MM"/>
                                </c:if>
                                <fmt:formatDate value="${broadcastPrivateTargeting.expirationTime}" pattern="HH:mm"></fmt:formatDate>
                            </b>.
                        </div>
                    </c:if>
                </div>

                <div class="tv-main_cont">
                    <c:if test="${not empty broadcast.advertismentBannerHtml}">
                        <div class="broadcast-stream-ads">
                                ${broadcast.advertismentBannerHtml}
                        </div>
                    </c:if>
                    <div class="tv-main_video">
                        <div class="embed-video">

                            <iframe id="stream-content-placeholder" frameborder="0" allowfullscreen allow="autoplay; fullscreen"></iframe>
                                <c:choose>
                                    <c:when test="${!isGeoAllowed || !isBackersAllowed || (broadcast.closed && !isPrivateAccessAllowed)}">
                                        <div class="broadcast-video-frame">
                                        <c:choose>
                                            <c:when test="${not empty broadcast.closedImageUrl}">
                                                <img class="central-block"
                                                     src="${hf:getThumbnailUrl(broadcast.closedImageUrl, "ORIGINAL", "BROADCAST")}"
                                                     alt="${broadcast.name}">
                                            </c:when>
                                            <c:otherwise>
                                                <img class="central-block"
                                                     src="//${hf:getStaticBaseUrl("")}/images/content/defaultnotext.png"
                                                     alt="${broadcast.name}">
                                            </c:otherwise>
                                        </c:choose>
                                        <span class="vertical-align"></span>
                                        <c:if test="${canView && (!broadcast.closedCustomDescription || not empty broadcast.closedDescriptionHtml)}">
                                            <div class="central-block-overlay">
                                                <div class="cbo-text broadcast-custom-description">
                                                    <c:choose>
                                                        <c:when test="${broadcast.closedCustomDescription}">
                                                            ${broadcast.closedDescriptionHtml}
                                                        </c:when>
                                                        <c:otherwise>
                                                            К сожалению, у Вас нет доступа к этой трансляции.
                                                        </c:otherwise>
                                                    </c:choose>
                                                </div>
                                                <div class="cbo-text-after"></div>
                                            </div>
                                        </c:if>
                                        </div>
                                    </c:when>
                                    <c:otherwise>
                                    </c:otherwise>
                                </c:choose>
                        </div>
                        <c:if test="${!canView}">
                            <div class="broadcast-signup">
                                Для просмотра трансляции необходимо
                                <a href="javascript:void(0);" class="broadcast-login">войти</a> или
                                <a href="javascript:void(0);" class="broadcast-register">зарегистрироваться</a>.
                            </div>
                        </c:if>
                    </div>

                </div>
            </div>

        </div>
    </div>
    <c:if test="${fn:length(broadcastInfo.streams) > 1}">
        <div class="stream-meta">
            <div class="wrap">
                <div class="col-8">

                    <div class="stream-meta_head">
                        Доступные камеры:
                    </div>


                    <div class="stream-channels">
                        <c:forEach items="${broadcastInfo.streams}" var="stream" varStatus="counter">
                            <c:if test="${not empty stream.tooltip}">
                                <c:set var="dataOriginalTitle">
                                    <c:out value="${stream.tooltip}"/>
                                </c:set>
                            </c:if>
                            <c:set var="classString">
                                bsp-item stream-link <c:if test="${not empty stream.tooltip}">tooltip</c:if>
                            </c:set>

                            <c:if test="${stream.broadcastStatus != 'PAUSED'}">
                                <a class="stream-channels_i active ${classString}"  data-stream-id="${stream.streamId}"
                                   data-tooltip="${dataOriginalTitle}" href="/broadcast/${broadcast.broadcastId}#${stream.streamId}">
                                    <div class="stream-channels_cover">
                                        <c:choose>
                                            <c:when test="${stream.imageUrl != null}">
                                                <img src="${hf:getThumbnailUrl(stream.imageUrl, "SMALL", "BROADCAST")}"
                                                     alt="${stream.name}">
                                            </c:when>
                                            <c:otherwise>
                                                <img src="//${hf:getStaticBaseUrl("")}/images/content/defaultnotext.png"
                                                     alt="defaultnotext.png">
                                            </c:otherwise>
                                        </c:choose>
                                    </div>
                                    <div class="stream-channels_name">${stream.name}</div>
                                </a>
                            </c:if>
                        </c:forEach>
                    </div>
                </div>
            </div>
        </div>
    </c:if>

    <div class="wrap">
        <div class="col-8">
            <div class="tv-stream-description">
                <p>
                    <c:if test="${not empty broadcast.descriptionHtml}">
                        ${broadcast.descriptionHtml}
                    </c:if>
                </p>
            </div>
            <c:if test="${!empty upcomingBroadcasts}">
                <div class="tv-upcoming">
                    <div class="tv-upcoming_head">
                        Предстоящие трансляции
                    </div>

                    <div class="upcoming-card">
                        <c:forEach items="${upcomingBroadcasts}" var="broadcast" varStatus="st">
                            <div class="upcoming-card_i">
                                <a href="/broadcast/${broadcast.broadcastId}" class="upcoming-card_link">
                                                    <span class="upcoming-card_meta">
                                                        <span class="s-tv-type-live"></span>
                                                        <span class="upcoming-card_date">
                                                                ${hf:dateFormatByTemplate(broadcast.timeBegin, "d MMMM HH:mm")}
                                                        </span>
                                                    </span>
                                                    <span class="upcoming-card_cover">
                                                        <img src="${hf:getThumbnailUrl(broadcast.imageUrl, "MEDIUM", "BROADCAST")}">
                                                    </span>
                                                    <span class="upcoming-card_cont">
                                                        <span class="upcoming-card_name">
                                                                ${broadcast.name}
                                                        </span>
                                                    </span>
                                </a>
                            </div>
                        </c:forEach>
                    </div>
                </div>
            </c:if>
        </div>
        <div class="col-4">

            <div class="tv-stream-sharing">
                <div class="tv-stream-sharing_head">
                    Делитесь:
                </div>
                <div class="js-share-container tv-stream-sharing_icons sharing-mini"></div>
            </div>
            <div id="broadcast-subscription-container">

            </div>
            <div id="chat-container" class="broadcast-live-block">

            </div>
        </div>
    </div>




</div>
    <%@ include file="/WEB-INF/jsp/includes/generated/irma/footer.jsp" %>


</body>
</html>

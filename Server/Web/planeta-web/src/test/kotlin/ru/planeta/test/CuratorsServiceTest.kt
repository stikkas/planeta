package ru.planeta.test

import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit4.SpringRunner
import ru.planeta.api.model.ConfigurationType
import ru.planeta.api.model.promo.PartnersPromoConfiguration
import ru.planeta.api.service.configurations.ConfigurationService
import ru.planeta.web.services.CuratorsService

/**
 * Date: 01.06.2015
 * Time: 15:47
 */
@RunWith(SpringRunner::class)
@SpringBootTest
@ContextConfiguration(locations = ["classpath:spring/applicationContext-*.xml",
    "classpath*:/spring/applicationContext-dao.xml",
    "classpath*:/spring/applicationContext-geo.xml"])
class CuratorsServiceTest {

    @Autowired
    lateinit var curatorsService: CuratorsService

    @Autowired
    lateinit var configurationService: ConfigurationService

    @Test
    fun testPartners() {
        var startTime = System.currentTimeMillis()
        var list = curatorsService.welcomeCurators
        println(System.currentTimeMillis() - startTime)
        println(list.size)
        startTime = System.currentTimeMillis()
        list = curatorsService.welcomeCurators
        println(System.currentTimeMillis() - startTime)
        println(list.size)
    }

    @Test
    fun measure() {
        val finalCurators = ArrayList<PartnersPromoConfiguration>()

        println(1)
        println(System.currentTimeMillis())
        val partners = configurationService.getJsonArrayConfig(PartnersPromoConfiguration::class.java,
                ConfigurationType.PLANETA_PARTNERS_PROMO_CONFIGURATION_LIST)
        println(2)
        println(System.currentTimeMillis())
        partners.filter { it.isCurator }
        println(3)
        println(System.currentTimeMillis())

        // from partners list we must choose 12 shuffled curators with 'fixed' attribure
        var curatorsFixed = partners.toMutableList()
        println(4)
        println(System.currentTimeMillis())
        curatorsFixed.filter { it.isFixed }
        println(5)
        println(System.currentTimeMillis())
        curatorsFixed.shuffle()
        if (curatorsFixed.size > 12) {
            curatorsFixed = curatorsFixed.subList(0, 12)
        }


        // from partners list we must choose shuffled curators without 'fixed' attribure; total curators size must be <= 12
        val rest = 12 - curatorsFixed.size
        println(6)
        println(System.currentTimeMillis())
        var curatorsUnfixed = partners.toMutableList()
        println(7)
        println(System.currentTimeMillis())
        curatorsUnfixed.filter { !it.isFixed }
        println(8)
        println(System.currentTimeMillis())
        curatorsUnfixed.shuffle()
        if (curatorsUnfixed.size > rest) {
            curatorsUnfixed = curatorsUnfixed.subList(0, rest)
        }
        println(9)
        println(System.currentTimeMillis())

        finalCurators.addAll(curatorsFixed)
        finalCurators.addAll(curatorsUnfixed)
        println(10)
        println(System.currentTimeMillis())

    }
}

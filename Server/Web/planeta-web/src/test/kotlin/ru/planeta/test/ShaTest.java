package ru.planeta.test;

import org.apache.commons.codec.digest.DigestUtils;
import org.junit.Test;

public class ShaTest {

    @Test
    public void test() {
        String pass = "3589720f37ffdb6b6614accd04217c89";
        String hash = DigestUtils.sha256Hex(String.valueOf(pass));
        System.out.println(hash);
    }
}

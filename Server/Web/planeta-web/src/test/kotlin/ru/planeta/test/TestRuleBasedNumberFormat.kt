package ru.planeta.test

import com.ibm.icu.text.RuleBasedNumberFormat
import org.junit.Test
import java.math.BigDecimal
import java.util.Locale

class TestRuleBasedNumberFormat  {

    @Test
    fun testBabulesyPrint() {
        val nf = RuleBasedNumberFormat(Locale.forLanguageTag("ru"),
                RuleBasedNumberFormat.SPELLOUT)
        val bigDecimal = BigDecimal(3333333)
        println(nf.format(bigDecimal))

        /*for (String ruleSet : new String[] {
                "%spellout-cardinal-masculine-accusative",
                "%spellout-cardinal-masculine",
                "%spellout-numbering"
                }) {
            System.out.println(nf.format(2222222, ruleSet));
            System.out.println(nf.format(3222222, ruleSet));
            System.out.println(nf.format(4222222, ruleSet));
            System.out.println();
        }*/

        /*RuleBasedNumberFormat format = new RuleBasedNumberFormat(new ULocale("ru"), RuleBasedNumberFormat.SPELLOUT);
        for (String ruleSet : new String[] { "%spellout-cardinal-feminine-ablative",
                "%spellout-cardinal-feminine-accusative", "%spellout-cardinal-feminine-dative",
                "%spellout-cardinal-feminine-genitive", "%spellout-cardinal-feminine-locative",
                "%spellout-cardinal-feminine", "%spellout-cardinal-masculine-ablative",
                "%spellout-cardinal-masculine-accusative", "%spellout-cardinal-masculine-dative",
                "%spellout-cardinal-masculine-genitive", "%spellout-cardinal-masculine-locative",
                "%spellout-cardinal-masculine", "%spellout-cardinal-neuter-ablative",
                "%spellout-cardinal-neuter-accusative", "%spellout-cardinal-neuter-dative",
                "%spellout-cardinal-neuter-genitive", "%spellout-cardinal-neuter-locative",
                "%spellout-cardinal-neuter", "%spellout-cardinal-plural-ablative",
                "%spellout-cardinal-plural-accusative", "%spellout-cardinal-plural-dative",
                "%spellout-cardinal-plural-genitive", "%spellout-cardinal-plural-locative",
                "%spellout-cardinal-plural", "%spellout-numbering-year",
                "%spellout-numbering", "%spellout-ordinal-feminine-ablative",
                "%spellout-ordinal-feminine-accusative", "%spellout-ordinal-feminine-dative",
                "%spellout-ordinal-feminine-genitive", "%spellout-ordinal-feminine-locative",
                "%spellout-ordinal-feminine", "%spellout-ordinal-masculine-ablative",
                "%spellout-ordinal-masculine-accusative", "%spellout-ordinal-masculine-dative",
                "%spellout-ordinal-masculine-genitive", "%spellout-ordinal-masculine-locative",
                "%spellout-ordinal-masculine", "%spellout-ordinal-neuter-ablative",
                "%spellout-ordinal-neuter-accusative", "%spellout-ordinal-neuter-dative",
                "%spellout-ordinal-neuter-genitive", "%spellout-ordinal-neuter-locative",
                "%spellout-ordinal-neuter", "%spellout-ordinal-plural-ablative",
                "%spellout-ordinal-plural-accusative", "%spellout-ordinal-plural-dative",
                "%spellout-ordinal-plural-genitive", "%spellout-ordinal-plural-locative",
                "%spellout-ordinal-plural", }) {
            System.out.println(format.format(2222222, ruleSet));
        }*/
    }
}

package ru.planeta.eva.web.services

import org.assertj.core.api.Assertions.assertThat
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner
import ru.planeta.TestWebConfig
import ru.planeta.dao.TransactionScope
import ru.planeta.dto.ProfileSitesDTO

@RunWith(SpringJUnit4ClassRunner::class)
@ContextConfiguration(classes = [TestWebConfig::class])
class ProfileSitesServiceTest {

    private lateinit var transactionScope: TransactionScope

    @Before
    fun init() {
        transactionScope = TransactionScope.createOrGetCurrent()
    }

    @After
    fun close() {
        transactionScope.close()
    }

    @Autowired
    lateinit var profileSitesService: ProfileSitesService

    @Test
    fun testProfileSitesService() {
        val googleUrl = "testGoogle"
        val vkUrl = "testVk"
        val fbUrl = "testFb"
        val siteUrl = "testSite"
        val twitterUrl = "testTwitter"
        val profileId = -228L

        val facebookBase = "https://facebook.com/"
        val vkBase = "https://vk.com/"
        val siteBase = "http://"
        val twitterBase = "https://twitter.com/"
        val googleBase = "https://plus.google.com/"

        val profileSites = ProfileSitesDTO()
        profileSites.profileId = profileId
        profileSites.googleUrl = googleUrl
        profileSites.vkUrl = vkUrl
        profileSites.facebookUrl = fbUrl
        profileSites.siteUrl = siteUrl
        profileSites.twitterUrl = twitterUrl

        // insert testing
        profileSitesService.saveProfileSites(profileSites)

        val selected = profileSitesService.selectProfileSites(profileId)

        assertThat(selected).isNotNull()

        assertThat(selected?.siteUrl).isEqualToIgnoringCase(siteBase + siteUrl)
        assertThat(selected?.facebookUrl).isEqualToIgnoringCase(facebookBase + fbUrl)
        assertThat(selected?.vkUrl).isEqualToIgnoringCase(vkBase + vkUrl)
        assertThat(selected?.twitterUrl).isEqualToIgnoringCase(twitterBase + twitterUrl)
        assertThat(selected?.googleUrl).isEqualToIgnoringCase(googleBase + googleUrl)

        // update testing
        val googleUrl2 = "testGoogle2"
        val vkUrl2 = "testVk2"
        val fbUrl2 = "testFb2"
        val siteUrl2 = "testSite2"
        val twitterUrl2 = "testTwitter2"

        selected?.googleUrl = googleUrl2
        selected?.vkUrl = vkUrl2
        selected?.facebookUrl = fbUrl2
        selected?.siteUrl = siteUrl2
        selected?.twitterUrl = twitterUrl2

        if (selected != null) {
            profileSitesService.saveProfileSites(selected)

            val selected2 = profileSitesService.selectProfileSites(profileId)

            assertThat(selected2).isNotNull()

            assertThat(selected2?.siteUrl).isEqualToIgnoringCase(siteBase + siteUrl2)
            assertThat(selected2?.facebookUrl).isEqualToIgnoringCase(facebookBase + fbUrl2)
            assertThat(selected2?.vkUrl).isEqualToIgnoringCase(vkBase + vkUrl2)
            assertThat(selected2?.twitterUrl).isEqualToIgnoringCase(twitterBase + twitterUrl2)
            assertThat(selected2?.googleUrl).isEqualToIgnoringCase(googleBase + googleUrl2)
        }
    }
}

package ru.planeta.eva.web.controllers

import org.assertj.core.api.Assertions.assertThat
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.runners.MockitoJUnitRunner
import org.springframework.validation.BindException
import org.springframework.validation.FieldError
import ru.planeta.api.service.campaign.CampaignService
import ru.planeta.api.service.profile.AuthorizationService
import ru.planeta.api.service.profile.ProfileService
import ru.planeta.eva.web.dto.AskManagerDTO

@RunWith(MockitoJUnitRunner::class)
class FeedBackControllerTest {

    @Mock
    lateinit var campaignService: CampaignService

    @Mock
    lateinit var authorizationService: AuthorizationService

    @Mock
    lateinit var profileService: ProfileService

    lateinit var feedBackController: FeedBackController


    @Before
    fun setUp() {
        feedBackController = FeedBackController(campaignService, authorizationService, profileService)
    }

    @Test
    fun askManager() {
        var askManagerDTO = AskManagerDTO("Hello", 1234141, "my.good@mail.ru")
        var errors = BindException(askManagerDTO, "askManagerDTO").bindingResult
        var result = feedBackController.askManager(askManagerDTO, errors)
        assertThat(result.success).isTrue()

        askManagerDTO = AskManagerDTO("", 234141)
        errors.addError(FieldError("askManagerDTO", "message", "errors.field_required"))
        result = feedBackController.askManager(askManagerDTO, errors)
        assertThat(result.success).isFalse()
        assertThat(result.result).isEqualTo(errors.allErrors)

        // Anonym without email
        askManagerDTO = AskManagerDTO("Hello", 1234141)
        errors = BindException(askManagerDTO, "askManagerDTO").bindingResult
        result = feedBackController.askManager(askManagerDTO, errors)
        assertThat(result.success).isFalse()
        val resErrors = result.result as List<FieldError>
        assertThat(resErrors).hasSize(1)
        assertThat(resErrors[0].field).isEqualTo("email")

        // Authorized user without email
        `when`(authorizationService.getUserPrivateEmailById(-1)).thenReturn("may-any@mail.ru")
        askManagerDTO = AskManagerDTO("Hello", 1234141)
        errors = BindException(askManagerDTO, "askManagerDTO").bindingResult
        result = feedBackController.askManager(askManagerDTO, errors)
        assertThat(result.success).isTrue()
    }
}

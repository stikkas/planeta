package ru.planeta

import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.ImportResource
import ru.planeta.eva.web.services.ProfileSitesService

@Configuration
@ImportResource(locations = ["classpath*:/spring/applicationContext-dao.xml",
    "classpath*:/ru/planeta/spring/propertyConfigurer.xml"])
//@EnableJpaRepositories(basePackageClasses = [ProfileSitesRepository::class])
@ComponentScan(basePackageClasses = [ProfileSitesService::class])
class TestWebConfig

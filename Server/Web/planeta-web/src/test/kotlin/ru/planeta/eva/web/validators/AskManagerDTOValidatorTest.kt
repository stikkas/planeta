package ru.planeta.eva.web.validators

import org.assertj.core.api.Assertions.assertThat
import org.junit.Before
import org.junit.Test
import org.springframework.validation.BindException
import org.springframework.validation.BindingResult
import ru.planeta.api.utils.ValidateUtils
import ru.planeta.eva.web.dto.AskManagerDTO

class AskManagerDTOValidatorTest {

    lateinit var validator: AskManagerDTOValidator
    private val validEmail = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$"

    @Before
    fun setUp() {
        validator = AskManagerDTOValidator()
        val validateUtils = ValidateUtils()
        validateUtils.setValidEmail(validEmail)
    }

    @Test
    fun validateTest() {
        var errors = validate("    ", null, null)
        assertThat(errors.getFieldError("message").code).isEqualTo("errors.field_required")
        assertThat(errors.getFieldError("campaignId").code).isEqualTo("errors.campaignId-required")
        assertThat(errors.getFieldError("email")).isNull()

        errors = validate("", null, null)
        assertThat(errors.getFieldError("message").code).isEqualTo("errors.field_required")
        assertThat(errors.getFieldError("campaignId").code).isEqualTo("errors.campaignId-required")
        assertThat(errors.hasFieldErrors("email")).isFalse()

        errors = validate("Wrong", null, null)
        assertThat(errors.hasFieldErrors("message")).isFalse()
        assertThat(errors.getFieldError("campaignId").code).isEqualTo("errors.campaignId-required")
        assertThat(errors.hasFieldErrors("email")).isFalse()

        errors = validate("Wrong", 23411, "")
        assertThat(errors.hasFieldErrors("message")).isFalse()
        assertThat(errors.hasFieldErrors("campaignId")).isFalse()
        assertThat(errors.getFieldError("email").code).isEqualTo("errors.wrong-email")

        errors = validate("Wrong", 23411, "fadfafaff")
        assertThat(errors.hasFieldErrors("message")).isFalse()
        assertThat(errors.hasFieldErrors("campaignId")).isFalse()
        assertThat(errors.getFieldError("email").code).isEqualTo("errors.wrong-email")

        errors = validate("Wrong", 23411, "fadfafaff@fadsfaa")
        assertThat(errors.hasFieldErrors("message")).isFalse()
        assertThat(errors.hasFieldErrors("campaignId")).isFalse()
        assertThat(errors.getFieldError("email").code).isEqualTo("errors.wrong-email")

        errors = validate("Wrong", null, "fadfafaff@fadsfaa")
        assertThat(errors.hasFieldErrors("message")).isFalse()
        assertThat(errors.getFieldError("campaignId").code).isEqualTo("errors.campaignId-required")
        assertThat(errors.getFieldError("email").code).isEqualTo("errors.wrong-email")

        errors = validate("Good", 15144, null)
        assertThat(errors.hasFieldErrors("message")).isFalse()
        assertThat(errors.hasFieldErrors("campaignId")).isFalse()
        assertThat(errors.hasFieldErrors("email")).isFalse()

        errors = validate("Good", 15144, "hello.tut@yandex.ru")
        assertThat(errors.hasFieldErrors("message")).isFalse()
        assertThat(errors.hasFieldErrors("campaignId")).isFalse()
        assertThat(errors.hasFieldErrors("email")).isFalse()
    }

    private fun validate(message: String, campaignId: Long?, email: String? = null): BindingResult {
        val target = AskManagerDTO(message, campaignId, email)
        val errors = BindException(target, "target").bindingResult
        validator.validate(target, errors)
        return errors
    }
}

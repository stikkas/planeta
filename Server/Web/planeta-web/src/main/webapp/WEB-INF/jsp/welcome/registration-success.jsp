<%@ include file="/WEB-INF/jsp/includes/generated/doctype-header.jsp" %>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="refresh" content="0;${redirectUrl}">
    <title>Planeta</title>

    <%@include file="/WEB-INF/jsp/includes/generated/common-favicon.jsp" %>
    <script type="text/javascript" src="//${hf:getStaticBaseUrl("")}/js/lib/jquery-1.8.3.min.js"></script>
    <%@include file="/WEB-INF/jsp/includes/generated/stat-counters-head.jsp" %>
</head>
<body>
<div style="margin: 20px">
    <a href="https://${properties['application.host']}"><img border="0" src="//${hf:getStaticBaseUrl("")}/images/print-logotip.png" alt="planeta.ru"></a>
</div>
<div style="margin-left: 80px; width: 450px; font: 20px/24px Tahoma, sans-serif;">
    <p>Ваш аккаунт на «Планете» был подтвержден.</p>
    <p>Поздравляем – теперь вы зарегистрированный планетянин!</p>
    <p>Общайтесь с другими обитателями «Планеты», присоединяйтесь к сообществам, слушайте музыку и поддерживайте интересные проекты. Удачи!</p>
    <p><a style="color: #26aedd" href="${redirectUrl}">Перейти на Планету &rarr;</a></p>
</div>
</body>
</html>






<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ include file="/WEB-INF/jsp/includes/generated/doctype-header.jsp" %>
<%@ taglib prefix="p" uri="http://planeta.ru/tags" %>

<head>
    <c:choose>
        <c:when test="${not empty customMetaTag.title}">
            <title>${customMetaTag.title}</title>
        </c:when>
        <c:otherwise>
            <title>О проекте Planeta.ru</title>
        </c:otherwise>
    </c:choose>
    <c:choose>
        <c:when test="${not empty customMetaTag.description}">
            <meta name="description" content="${customMetaTag.description}"/>
        </c:when>
        <c:otherwise>
            <meta name="description"
                  content="Что такое онлайн-площадка Planeta.ru?"/>
        </c:otherwise>
    </c:choose>
    <c:if test="${not empty customMetaTag.keywords}">
        <meta name="keywords" content="${customMetaTag.keywords}"/>
    </c:if>

    <meta name="viewport" content="width=device-width"/>
    <%@include file="/WEB-INF/jsp/includes/generated/stat-counters-async.jsp"%>

    <%@include file="/WEB-INF/jsp/includes/common-css.jsp" %>
    <link type="text/css" rel="stylesheet" href="//${hf:getStaticBaseUrl("")}/css-generated/about.css"/>
    <%@include file="/WEB-INF/jsp/includes/generated/common-templates.jsp" %>
    <%@include file="/WEB-INF/jsp/includes/planeta-js.jsp" %>

</head>

<body class="grid-1200 about-page">
<%@ include file="/WEB-INF/jsp/includes/generated/irma/header.jsp" %>
<%@ include file="/WEB-INF/jsp/includes/generated/irma/main-nav.jsp" %>

<div id="global-container">
    <div id="main-container" class="wrap-container">

        <div id="center-container">

            <%@ include file="/WEB-INF/jsp/includes/about-new-menu.jsp" %>


            <div class="wrap">
                <div class="col-12">
                    <div class="about-head">
                        <spring:message code="footer-new.jsp.propertie.9" text="default text"> </spring:message>
                    </div>

                </div>
            </div>



            <div class="wrap">
                <div class="col-12">
                    <div class="partners-text">
                        <p>Кураторы проектов – это друзья Planeta.ru, которые оказывают информационную, материальную или иную поддержку наиболее активным и интересным крауд-кампаниям. Происходит это следующим образом: когда проект собирает свыше 25% от финансовой цели, мы включаем его в список, который рассылается всем нашим партнерам. Организация самостоятельно выбирает кампанию и закрепляется за ней в качестве куратора, помогающего проекту продвинуться вперед. Эта поддержка может быть разовой или постоянной – в зависимости от конкретных договоренностей между Planeta.ru и партнерской организацией.</p>
                        <p>Поробнее о кураторах, читайте в наше <a href="https://planeta.ru/faq/article/29!paragraph203">FAQ</a></p>
                        <p>Если вы тоже хотите стать куратором или партнёром Planeta.ru, напишите на <a href="mailto:pr@planeta.ru">pr@planeta.ru</a>.</p>
                        <br>
                        <br>
                    </div>

                    <div class="partners-list">
                        <c:forEach items="${partnersPromoConfigurations}" var="partner">
                            <div class="partners-list_i">
                                <a href="${partner.originalUrl}" class="partners-list_link" target="_blank" rel="nofollow noopener">
                                    <img class="partners-list_img" src="${hf:getThumbnailUrl(partner.imageUrl, 'MEDIUM', 'PHOTO')}">
                                </a>
                            </div>
                        </c:forEach>
                    </div>
                </div>
            </div>



        </div>

    </div>

</div>
<%@ include file="/WEB-INF/jsp/includes/generated/irma/footer.jsp" %>
</body>
</html>

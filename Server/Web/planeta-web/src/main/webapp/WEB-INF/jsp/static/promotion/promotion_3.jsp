<%@ include file="/WEB-INF/jsp/includes/generated/doctype-header.jsp" %>
<head>
    <%@include file="/WEB-INF/jsp/includes/generated/stat-counters-async.jsp"%>
    <%@include file="head.jsp"%>
    <title>Руководство по продвижению на этапе 50-99%</title>
</head>
<body class="project-promotion-page">
<%@ include file="/WEB-INF/jsp/includes/generated/irma/header.jsp" %>
<%@ include file="/WEB-INF/jsp/includes/generated/irma/main-nav.jsp" %>


<div id="global-container">
    <div id="main-container" class="wrap-container">
        <div id="center-container">


            <div class="project-promotion_head">Руководство по продвижению на этапе 50-99%</div>


            <div class="project-promotion_top">
                <div class="wrap">
                    <div class="col-12">

                        <div class="project-promotion_top_cont">
                            <div class="project-promotion_top_ico s-promotion-top-pig"></div>
                            <div class="project-promotion_top_text">
                                Сейчас вам нужно вдохнуть новую жизнь в свой проект. Представьте себе на месте спонсора, который уже поучаствовал в проекте или только собирается это сделать. Чтобы вы хотели получить за участие в этой истории? Ваши главные козыри — новые вознаграждения, бонусы, конкурсы,  который человек получит за свой вклад.
                            </div>
                        </div>

                    </div>
                </div>
            </div>


            <div class="project-promotion_cont">
                <div class="wrap">
                    <div class="col-12">

                        <div class="project-promotion_list">
                            <div class="project-promotion_list-i">
                                <div class="project-promotion_list-ico s-promotion-list-vk"></div>
                                <div class="project-promotion_list-head">
                                    Сейчас вам нужно вдохнуть новую жизнь в свой проект. Представьте себе на месте
                                    спонсора, который уже поучаствовал в проекте или только собирается это сделать.
                                    Чтобы вы хотели получить за участие в этой истории? Ваши главные козыри — новые
                                    вознаграждения, бонусы, конкурсы, который человек получит за свой вклад.
                                </div>
                                <div class="project-promotion_list-text">
                                    <span class="step-hl">Шаг 1:</span> Начните выдачу вознаграждений тем, кто уже поучаствовал в
                                    проекте. Обязательно используйте это как дополнительный
                                    инфоповод, который нужно
                                    осветить на своих ресурсах.
                                    <br>
                                    <br>
                                    <span class="step-hl">Шаг 2:</span> Обновите страницу проекта: добавьте новые вознаграждения,
                                    бонусы от известных людей или лидеров мнений, создайте серию видеороликов о рабочем
                                    процессе или смените <a href="http://www.youtube.com/playlist?list=PLIaV0soRfmMR8W_mfXaUDf6yFOb68jw4p">видеообращение</a>
                                    (пример: <a href="http://tv.planeta.ru/video/louna/20906">было</a> и <a href="http://tv.planeta.ru/video/louna/23537">стало</a>).
                                    <br>
                                    <br>
                                    <span class="step-hl">Шаг 3:</span> Предложите вознаграждение за <a href="https://planeta.ru/lifer/blog/120714">репост</a>, <a href="https://planeta.ru/189950/blog/125069">бонус</a> за
                                    участие в проекте на финальной стадии. Старайтесь мыслить нестандартно и придумывать
                                    новые поощрения для ваших спонсоров.
                                </div>
                            </div>

                            <div class="project-promotion_list-i">
                                <div class="project-promotion_list-ico s-promotion-list-msg"></div>
                                <div class="project-promotion_list-head">
                                    Рассылки — это информация, которую вы распространяете по друзьям, коллегам,
                                    родственникам и партнерам, используя личные сообщения в соцсетях, e-mail письма,
                                    телефонные звонки и sms-сообщения.
                                </div>
                                <div class="project-promotion_list-text">
                                    <span class="step-hl">Шаг 1:</span> Придумайте креативную рассылку. Это может быть
                                    дайджест самых интересных вознаграждений в вашем проекте или <a href="https://planeta.ru/sunsay/blog/122238">спец-предложение</a> для ваших
                                    партнеров/коллег/друзей. Не ограничивайте свою фантазию.
                                </div>
                            </div>

                            <div class="project-promotion_list-i">
                                <div class="project-promotion_list-ico s-promotion-list-events"></div>
                                <div class="project-promotion_list-head">
                                    Мероприятия — это оффлайновые встречи (концерты, выставки, мастер-классы,
                                    выступления, дружеские вечеринки), на которых вы можете рассказать о своем проекте.
                                    Не ограничивайтесь интернетом, рассказывайте о своем проекте на разных площадках!
                                </div>
                                <div class="project-promotion_list-text">
                                    <span class="step-hl">Шаг 1:</span> Проведите мероприятие в поддержку вашего проекта
                                    и позовите туда спонсоров. У вас в проекте было вознаграждение с приглашением на
                                    концерт/презентацию/выставку или что-то еще? Обязательно расскажите о том, как оно
                                    прошло в <a href="https://planeta.ru/petrushka/blog/123051">новостях проекта</a>, в
                                    <a href="https://planeta.ru/animaljazz/blog/126563">сообществе</a> и на других
                                    площадках.
                                </div>
                            </div>

                            <div class="project-promotion_list-i">
                                <div class="project-promotion_list-ico s-promotion-list-media"></div>
                                <div class="project-promotion_list-head">
                                    Сообщества и СМИ — это тематические блоги, паблики, сообщества в Facebook,
                                    ВКонтакте, Живом Журнале, Твиттере, а также близкие вашему проекту по тематике
                                    средства массовой информации.
                                </div>
                                <div class="project-promotion_list-text">
                                    <span class="step-hl">Шаг 1:</span> Подумайте над отдельным спецпроектом со СМИ и
                                    сообществами.
                                    <br>
                                    <br>
                                    <span class="step-hl">Шаг 2:</span> Предложите СМИ бартер (логотип на странице
                                    проекта, на собственных ресурсах в соц. сетях, логотип на картинке проекта, запрос
                                    на размещение логотипа партнера в разделе «Наши партнеры»)
                                    <br>
                                    <br>
                                    <span class="step-hl">Шаг 3:</span> Договоритесь со СМИ об <a href="http://vozduh.afisha.ru/archive/crowd-28/">интервью</a> или <a href="http://24doc.ru/news/1176">комментарии</a>,
                                    в котором вы сможете рассказать о своем проекте
                                </div>
                            </div>

                            <div class="project-promotion_list-i">
                                <div class="project-promotion_list-ico s-promotion-list-person"></div>
                                <div class="project-promotion_list-head">
                                    Лидеры мнений — это известные и уважаемые личности со сложившейся большой аудиторией
                                    (в том числе, и сетевой): общественные деятели, блогеры, журналисты, акулы бизнеса.
                                </div>
                                <div class="project-promotion_list-text">
                                    <span class="step-hl">Шаг 1:</span> Предложите сотрудничество, предварительно
                                    определив его формат: это может быть <a href="http://dolboeb.livejournal.com/2583370.html">пост</a>, ретвит, <a href="https://www.facebook.com/permalink.php?story_fbid=621242517909477&amp;id=100000712037223">анонс
                                    в соц.сетях</a>,
                                    <a href="http://www.youtube.com/watch?v=Vw1SSJU4y5Y">видеокомментарий</a>, бонус в
                                    проекте, <a href="http://odnovremenno.com/">баннер на сайте</a>, <a href="https://planeta.ru/128318/blog/118065">совместная акция</a>, <a href="https://www.facebook.com/misha.kozyrev/posts/633592420013133">репост</a>.
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>


        </div>
    </div>
</div>

<%@ include file="/WEB-INF/jsp/includes/generated/irma/footer.jsp" %>
</body>
</html>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ include file="/WEB-INF/jsp/includes/generated/doctype-header.jsp" %>
<%@taglib prefix="p" uri="http://planeta.ru/tags" %>
<head>
    <title>Всероссийский «Душевный Bazar» на Planeta.ru</title>

    <meta property="og:title" content="Всероссийский «Душевный Bazar» на Planeta.ru">
    <meta name="description"
          content="В рамках онлайн-ярмарки «Душевный Bazar» на Planeta.ru у вас есть возможность приобрести сувениры, подарки родным и близким к Новому Году, а также познакомиться со множеством благотворительных проектов!">
    <meta property="og:description"
          content="В рамках онлайн-ярмарки «Душевный Bazar» на Planeta.ru у вас есть возможность приобрести сувениры, подарки родным и близким к Новому Году, а также познакомиться со множеством благотворительных проектов!">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta property="og:image" content="https://s1.planeta.ru/i/db68e/original_campaign.jpg">

    <%@include file="/WEB-INF/jsp/includes/generated/common-favicon.jsp" %>
    <link href='https://fonts.googleapis.com/css?family=Cuprum:400,400italic,700,700italic&subset=latin,cyrillic'
          rel='stylesheet' type='text/css'>
    <link type="text/css" rel="stylesheet" href="//${hf:getStaticBaseUrl("")}/css-generated/bazar.css"/>
    <%@include file="/WEB-INF/jsp/includes/generated/stat-counters-head.jsp" %>
    <%@include file="/WEB-INF/jsp/includes/planeta-js.jsp" %>
    <script type="text/javascript" src="//${hf:getStaticBaseUrl("")}/js/lib/jquery.lazyloadxt.extra.min.js"></script>
    <script type="text/javascript" src="//${hf:getStaticBaseUrl("")}/js/lib/owl.carousel.2.min.js"></script>

    <script>
        $(function () {
            var itemCount = $(".project-card-list > div").length;
            var slideStep = (itemCount < 8) ? (itemCount % 4) : 4;

            var owl = $(".project-card-list").addClass('owl-carousel').owlCarousel({
                lazyLoad: true,
                autoplay: false,
                autoplayTimeout: 5000,
                autoplayHoverPause: true,
                loop: true,
                items: 4,
                slideBy: slideStep,
                nav: true,
                dots: false,
                margin: 15,
                mouseDrag: false,
                smartSpeed: 200,
                navText: [
                    '<span class="s-icon s-icon-arrow-thin-left">',
                    '<span class="s-icon s-icon-arrow-thin-right">'
                ],
                responsive: {
                    0: {
                        margin: 15,
                        items: 1,
                        autoWidth: true
                    },
                    400: {
                        margin: 15,
                        items: 1,
                        autoWidth: true
                    },
                    500: {
                        margin: 26,
                        items: 2,
                        autoWidth: true
                    },
                    700: {
                        margin: 26,
                        items: 3,
                        autoWidth: true,
                        center: true
                    },
                    1160: {
                        margin: 26,
                        items: 4,
                        center: false
                    }
                }
            });
        });
    </script>
</head>
<body>

<div class="bg-block">
    <div class="bg-wrap">
        <div class="bg-tabs"></div>
        <div class="bg-box-big"></div>
    </div>
    <div class="bg-box"></div>
</div>

<div class="main">
    <div class="header">
        <div class="wrap">
            <div class="header_logos">
                <a class="header_bazar-logo" href="http://d-bazar.ru/"><span class="header_bazar-logo_logo"></span></a>

                <a class="header_supradyn-logo" href="http://www.supradyn.ru/"></a>
            </div>

            <div class="lead">
                <div class="lead_cont">
                    <div class="lead_pre-head">
                        Самая масштабная благотворительная ярмарка
                    </div>
                    <div class="lead_head">
                        «Душевный&nbsp;Bazar» на Planeta.ru
                    </div>
                    <div class="lead_text">
                        В&nbsp;2015 году &laquo;Душевный&nbsp;Bazar&raquo; распахнёт свои двери не&nbsp;только на&nbsp;самом
                        мероприятии в&nbsp;декабре, но&nbsp;и&nbsp;на&nbsp;Planeta.ru, где участники ярмарки представят
                        свои краудфандинговые проекты. Примите участие в&nbsp;самой народной ярмарке онлайн!
                    </div>
                    <div class="lead_btn">
                        <a href="/campaigns/${bazarCampaign.url}" class="land-btn land-btn-primary">Поддержи проект</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="header_sub"></div>

    <div class="content">
        <div class="wrap">

            <div class="bazar-project">

                <div class="bazar-project_head">
                    Витаминов много, Супрадин — один!
                </div>

                <div class="bazar-project_box">
                    <div class="bazar-project_name-big">
                        Сделаем «Душевный&nbsp;Bazar» вместе
                    </div>

                    <div class="bazar-project_video">
                        <div class="embed-video">
                            <div class="bazar-project_cover">
                                <div class="cover"
                                     style="background-image:url(https://s1.planeta.ru/i/c9914/original_campaign.jpg);"></div>
                                <span class="video-play"><span class="video-play_ico"></span></span>
                            </div>
                            <iframe class="video hide" width="560" height="315"
                                    data-src="https://www.youtube-nocookie.com/embed/YwSisJ-jMsc?rel=0&amp;controls=1&amp;showinfo=0&amp;autoplay=1"
                                    frameborder="0" allowfullscreen></iframe>
                        </div>
                    </div>
                    <script>
                        $(".video-play").on("click", function () {
                            $('.bazar-project_cover').hide();
                            var video = $(".video");
                            video.show();
                            video.attr("src", video.data("src"));
                        })
                    </script>

                    <div class="bazar-project_data">
                        <div class="bazar-project_name">
                            Сделаем «Душевный Bazar» вместе
                        </div>

                        <div class="bazar-project_text">
                            13 декабря 2015 года в 6-й раз откроет свои двери самая масштабная благотворительная ярмарка
                            в России. В ней примут участие 74 благотворительные организации, которые создадут для вас
                            атмосферу настоящего новогоднего праздника и расскажут о своей деятельности. Уже сейчас
                            каждый из вас может внести свой вклад в организацию «Душевного&nbsp;Bazar`a», поддержав проект!
                        </div>

                        <div class="bazar-project_bar">
                            <div class="bazar-project_progress" style="width: ${bazarCampaign.progressProc}%;"></div>
                        </div>

                        <div class="bazar-project_meta">
                            <div class="bazar-project_meta_i">
                                <div class="bazar-project_meta_lbl">
                                    Собрано средств
                                </div>
                                <div class="bazar-project_meta_val">
                                    <fmt:formatNumber value="${bazarCampaign.collected}"
                                                      type="number"/> <span class="b-rub">Р</span>
                                </div>
                            </div>
                            <div class="bazar-project_meta_i">
                                <div class="bazar-project_meta_lbl">
                                    Цель проекта
                                </div>
                                <div class="bazar-project_meta_val">
                                    <fmt:formatNumber value="${bazarCampaign.target}"
                                                      type="number"/> <span class="b-rub">Р</span>
                                </div>
                            </div>
                        </div>

                        <div class="bazar-project_btn">
                            <a href="/campaigns/${bazarCampaign.url}" class="land-btn land-btn-primary">Поддержи
                                проект</a>
                        </div>
                    </div>
                </div>
            </div>

            <c:if test="${fn:length(campaings) > 0}">
                <div class="project-members">

                    <div class="project-members_head">
                        Проекты участников ярмарки «Душевный&nbsp;Bazar»
                    </div>

                    <div class="project-members_projects">

                        <div class="project-card-list">
                            <c:forEach items="${campaings}" var="campaing">
                                <div class="project-card-item">
                                    <a class="project-card-link" href="/campaigns/${campaing.url}">

                                        <span class="project-card-cover-wrap">
                                            <img class="project-card-cover owl-lazy" data-src="${hf:getProxyThumbnailUrl(campaing.image, "ORIGINAL", "PHOTO", 270, 164, true, false)}"
                                                 width="270"
                                                 height="164">
                                        </span>

                                        <span class="project-card-text">
                                            <span class="project-card-name hyphenate">${campaing.name}</span>
                                            <span class="project-card-descr">${campaing.shortDesc}</span>
                                        </span>

                                        <span class="project-card-footer">

                                            <span class="project-card-progress">
                                                <span class="pcp-bar" style="width:${campaing.progressProc}%;"></span>
                                            </span>

                                            <span class="project-card-info">
                                            <span class="project-card-info-i">
                                                <span class="pci-label">Собрано ${campaing.progressProc}%</span>
                                                <span class="pci-value"><fmt:formatNumber value="${campaing.collected}"
                                                                                          type="number"/> <span
                                                        class="b-rub">Р</span></span>
                                            </span>

                                            <span class="project-card-info-i">
                                                <span class="pci-label">Цель</span>
                                                <span class="pci-value"><fmt:formatNumber value="${campaing.target}"
                                                                                          type="number"/> <span
                                                        class="b-rub">Р</span></span>
                                            </span>
                                            </span>

                                        </span>
                                    </a>
                                </div>

                            </c:forEach>
                        </div>
                    </div>

                    <div class="project-members_total">
                        Проекты «Душевного BAZAR’а» собрали <b><fmt:formatNumber value="${totalAmount}" type="number"/>
                            <span class="rub">Р<span class="rub_t">–</span></span>
                        </b>
                    </div>

                </div>
            </c:if>

            ${newsBlock}

            <div class="supr_cont">
                <div class="supr_pre-head">Проект заряжает энергией</div>
                <div class="supr_head">Супрадин</div>
            </div>

            <div class="supr">
                <div class="past-events">
                    <div class="past-events_head">
                        Прошедшие события:
                    </div>
                    <div class="past-events_list">
                        <a class="past-events_link" href="http://d-bazar.ru/event/2012-bazar/">2012</a>
                        <a class="past-events_link" href="http://d-bazar.ru/event/2013-bazar/">2013</a>
                        <a class="past-events_link" href="http://d-bazar.ru/event/2014-bazar/">2014</a>
                    </div>
                </div>
            </div>

            ${partnersBlock}
        </div>
    </div>
</div>


<div class="footer">
    <div class="warn">
        <div class="wrap">
            <img class="warn-img" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7"
                 data-src="//${hf:getStaticBaseUrl("")}/images/bazar/warn.png" width="1160" height="192">
            <div class="warn-text">Имеются противопоказания. Перед приенением препарата ознакомьтесь с инструкцией.</div>
        </div>
    </div>

    <div class="footer_cont">
        <div class="wrap">

            <div class="footer_sharing">
                <div class="footer_sharing_head">
                    Расскажите о ярмарке друзьям
                </div>
                <div class="footer_sharing_cont js-share-container">
                </div>
            </div>

            <div class="footer_copy">
                <span class="footer_copy_head">Разработано</span>
                <span class="footer_copy_cont">
                    <a href="https://planeta.ru/" class="logo"></a>
                </span>
            </div>
        </div>
        <script>
            $(document).ready(function () {
                var uri = window.location.protocol + '//' + window.location.host + '${requestScope['javax.servlet.forward.request_uri']}';
                var sharesRedraw = function () {
                    $('.js-share-container').empty();
                    $('.js-share-container').share({
                        className: 'sharing-popup-social donate-sharing sharing-mini',
                        counterEnabled: false,
                        hidden: false,
                        parseMetaTags: true,
                        url: uri
                    });
                };

                $(window).bind('resize', sharesRedraw);
                sharesRedraw();
            })
        </script>

    </div>
</div>

<div class="modal-dialog hide" id="modal-request">
    <div class="modal modal-request">
        <div class="modal-request_close" data-dismiss="modal"></div>
    </div>
</div>
<script>
    var bgParallax = {
        init: function () {
            var items = $('.js-prlx-item');
            $(window).off('scroll.prlx');
            items.each(function () {
                var item = $(this);
                item.css('top', '');

                var itemPos = item.position().top;
                var itemVelocity = item.data('velocity');

                var itemStart = item.data('start');


                $(window).on('scroll.prlx', function () {
                    var winTop = $(window).scrollTop();
                    var winBottom = $(window).height() + winTop;
                    if (winBottom >= itemPos) {
                        var scrollPos = itemStart == "top" ? winTop : winBottom - itemPos;
                        item.css({top: itemPos + scrollPos / itemVelocity});
                    } else {
                        item.css('top', '');
                    }
                }).trigger('scroll');
            });
        }
    };
    bgParallax.init();
</script>
</body>
</html>

<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ include file="/WEB-INF/jsp/includes/generated/doctype-header.jsp" %>
<%@ taglib prefix="p" uri="http://planeta.ru/tags" %>

<head>
    <c:choose>
        <c:when test="${not empty customMetaTag.title}">
            <title>${customMetaTag.title}</title>
        </c:when>
        <c:otherwise>
            <title>О проекте Planeta.ru</title>
        </c:otherwise>
    </c:choose>
    <c:choose>
        <c:when test="${not empty customMetaTag.description}">
            <meta name="description" content="${customMetaTag.description}"/>
        </c:when>
        <c:otherwise>
            <meta name="description"
                  content="Что такое онлайн-площадка Planeta.ru?"/>
        </c:otherwise>
    </c:choose>
    <c:if test="${not empty customMetaTag.keywords}">
        <meta name="keywords" content="${customMetaTag.keywords}"/>
    </c:if>

    <meta name="viewport" content="width=device-width"/>
    <%@include file="/WEB-INF/jsp/includes/generated/stat-counters-async.jsp"%>

    <%@include file="/WEB-INF/jsp/includes/common-css.jsp" %>
    <link type="text/css" rel="stylesheet" href="//${hf:getStaticBaseUrl("")}/css-generated/about.css"/>
    <%@include file="/WEB-INF/jsp/includes/generated/common-templates.jsp" %>
    <%@include file="/WEB-INF/jsp/includes/planeta-js.jsp" %>

</head>

<body class="grid-1200 about-page">
<%@ include file="/WEB-INF/jsp/includes/generated/irma/header.jsp" %>
<%@ include file="/WEB-INF/jsp/includes/generated/irma/main-nav.jsp" %>

<div id="global-container">
    <div id="main-container" class="wrap-container">

        <div id="center-container">

            <%@ include file="/WEB-INF/jsp/includes/about-new-menu.jsp" %>


            <div class="wrap">
                <div class="col-12">
                    <div class="about-head">
                        Контакты
                    </div>


                    <div class="about-head-text">
                        Если вы хотите предложить информационное партнерство, обратиться по поводу проведения онлайн-трансляции или задать вопрос службе поддержки, все необходимые для этого контакты можно найти на этой странице.
                    </div>
                </div>
            </div>

            <div class="wrap">
                <div class="col-12">
                    <div class="contacts-list wrap-row">
                        <div class="contacts-list_i col-6">
                            <div class="contacts-list_ico">
                                <span class="s-contacts-user"></span>
                            </div>
                            <div class="contacts-list_cont">
                                <div class="contacts-list_name">
                                    Работа с авторами
                                </div>
                                <div class="contacts-list_descr">
                                    <div class="contacts-list_descr-cont">
                                        <p>
                                            Если вы хотите завести проект и нуждаетесь в оценке идеи, напишите на авторскую почту:
                                            <span class="contacts-list_p-mail">
                                                <a href="mailto:authors@planeta.ru">authors@planeta.ru</a>
                                            </span>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="contacts-list_i col-6">
                            <div class="contacts-list_ico">
                                <span class="s-contacts-support"></span>
                            </div>
                            <div class="contacts-list_cont">
                                <div class="contacts-list_name">
                                    Служба поддержки пользователей
                                </div>
                                <div class="contacts-list_descr">
                                    <div class="contacts-list_descr-cont">
                                        <p>
                                            Если у вас возник вопрос по работе <nobr>сервиса/сайта</nobr>, вы можете написать на:
                                            <span class="contacts-list_p-mail">
                                                <a href="mailto:support@planeta.ru">support@planeta.ru</a>
                                            </span>
                                        </p>
                                        <p>
                                            По вопросам оплаты и возврата денежных средств обращайтесь на:
                                            <span class="contacts-list_p-mail">
                                                <a href="mailto:payment.support@planeta.ru">payment.support@planeta.ru</a>
                                            </span>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="contacts-list_i col-6">
                            <div class="contacts-list_ico">
                                <span class="s-contacts-pr"></span>
                            </div>
                            <div class="contacts-list_cont">
                                <div class="contacts-list_name">
                                    PR &ndash; отдел
                                </div>
                                <div class="contacts-list_descr">
                                    <div class="contacts-list_descr-cont">
                                        <p>
                                            PR-директор <b>Василина Дрогичинская</b>
                                            <span class="contacts-list_p-mail">
                                                <a href="mailto:vasilina@planeta.ru">vasilina@planeta.ru</a>
                                            </span>
                                        </p>
                                        <p>
                                            По вопросам сотрудничества со СМИ и информационной поддержки вы можете написать <b>Наталье Игнатенко</b>:
                                            <span class="contacts-list_p-mail">
                                                <a href="mailto:irina@planeta.ru">pr@planeta.ru</a>
                                            </span>
                                        </p>
                                        <p>
                                            По вопросам организации образовательных мероприятий и участия в «Школе краудфандинга» пишите <b>Егору Ельчину</b>:
                                            <span class="contacts-list_p-mail">
                                                <a href="mailto:egor@planeta.ru">egor@planeta.ru</a>
                                            </span>
                                        </p>
                                        <p>
                                            По вопросам PR благотворительных проектов обращайтесь к <b>Елизавете Гальцевой</b>:
                                            <span class="contacts-list_p-mail">
                                                <a href="mailto:charitypr@planeta.ru">charitypr@planeta.ru</a>
                                            </span>
                                        </p>

                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="contacts-list_i col-6">
                            <div class="contacts-list">
                                <div class="contacts-list_i">
                                    <div class="contacts-list_ico">
                                        <span class="s-contacts-tv"></span>
                                    </div>
                                    <div class="contacts-list_cont">
                                        <div class="contacts-list_name">
                                            Онлайн &ndash; трансляции
                                        </div>
                                        <div class="contacts-list_descr">
                                            <div class="contacts-list_descr-cont">
                                                <p>
                                                    Если вы хотите организовать онлайн-трансляцию концерта на нашем портале <a href="https://${properties['tv.application.host']}">tv.planeta.ru</a> вы можете написать на:
                                                    <span class="contacts-list_p-mail">
                                                        <a href="mailto:tv@planeta.ru">tv@planeta.ru</a>
                                                    </span>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <div class="contacts-list_i">
                                    <div class="contacts-list_ico">
                                        <span class="s-contacts-advertising"></span>
                                    </div>
                                    <div class="contacts-list_cont">
                                        <div class="contacts-list_name">
                                            Спецпроекты
                                        </div>
                                        <div class="contacts-list_descr">
                                            <div class="contacts-list_descr-cont">
                                                <p>
                                                    По поводу размещения рекламы на портале вы можете написать на:
                                                    <span class="contacts-list_p-mail">
                                                        <a href="mailto:sale@planeta.ru">sale@planeta.ru</a>
                                                    </span>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>



                    <hr class="contacts-hr">



                    <div class="contacts-list wrap-row">
                        <div class="contacts-list_i col-6">
                            <div class="contacts-list_ico">
                                <span class="s-contacts-requisites"></span>
                            </div>
                            <div class="contacts-list_cont">
                                <div class="contacts-list_name">
                                    Наши реквизиты
                                </div>
                                <div class="contacts-list_descr">
                                    <div class="contacts-list_descr-cont">
                                        <p>
                                            Общество с ограниченной ответственностью «Глобал Нетворкс»
                                            <br>
                                            ОГРН 1107746618385
                                            <br>
                                            ИНН 7722724252
                                        </p>
                                    </div>
                                    <p>
                                        <span class="contacts-list_p-hl">Адрес для отправки документов и корреспонденции:</span>
                                        129090, г. Москва, ООО "Глобал Нетворкс", а/я 25
                                    </p>
                                </div>
                            </div>
                        </div>


                        <div class="contacts-list_i col-6">
                            <div class="contacts-requisites">
                                <div class="contacts-requisites_i">
                                    <div class="contacts-requisites_lbl">
                                        Юридический/почтовый адрес:
                                    </div>
                                    <div class="contacts-requisites_val">
                                        129090, Россия, город Москва, проспект Мира, дом 19, строение 3
                                    </div>
                                </div>

                                <div class="contacts-requisites_i">
                                    <div class="contacts-requisites_lbl">
                                        Мы находимся по адресу:
                                    </div>
                                    <div class="contacts-requisites_val">
                                        129090, Россия, г. Москва, проспект Мира, дом 19, строение 3, этаж 3
                                    </div>
                                </div>

                                <div class="contacts-requisites_i">
                                    <div class="contacts-requisites_lbl">
                                        Наш телефон:
                                    </div>
                                    <div class="contacts-requisites_val">
                                        <div class="contacts-requisites_tel">
                                            +7 (495) 181-05-05
                                        </div>
                                        <div class="contacts-requisites_time">
                                            Мы принимаем звонки в будние дни с 11:00 до 20:00 и в субботу с 11:00 до 18:00.
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="contacts-map" id="map"></div>
                    <script type="text/javascript" src="https://maps.google.com/maps/api/js?key=AIzaSyAcNhAbtfdB6fkVVs7BoaIoW5HEPjAn4Mw&v=3.31"></script>
                    <p:script src="planeta-google-map.js" />

                    <script>
                        google.maps.event.addDomListener(window, 'load', CustomPlnGoogleMap.init);
                    </script>
                </div>
            </div>
        </div>
    </div>

</div>
<%@ include file="/WEB-INF/jsp/includes/generated/irma/footer.jsp" %>
</body>
</html>

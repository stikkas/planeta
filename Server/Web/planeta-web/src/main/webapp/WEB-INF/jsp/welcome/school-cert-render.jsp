<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@include file="/WEB-INF/jsp/includes/generated/doctype-header.jsp" %>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Сертификат о прохождении онлайн-курса &laquo;Школы краудфандинга Planeta.ru&raquo;</title>
    <meta name="title" content="Сертификат о прохождении онлайн-курса &laquo;Школы краудфандинга Planeta.ru&raquo;">
    <link type="text/css" rel="stylesheet" href="//${hf:getStaticBaseUrl("")}/css-generated/common.css"/>
    <link type="text/css" rel="stylesheet" href="//${hf:getStaticBaseUrl("")}/css-generated/interactive.css"/>
</head>
<body>
<div class="intr-cert-page">
    <div class="intr-cert">
        <div class="intr-cert_img">
            <img src="//${hf:getStaticBaseUrl("")}/images/interactive/press.jpg">
        </div>
        <div class="intr-cert_name">
            ${displayName}
        </div>
    </div>
</div>
</body>

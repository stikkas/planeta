<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<%@ include file="/WEB-INF/jsp/includes/taglibs.jsp" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width"/>
        <meta property="og:site_name" content="planeta.ru"/>
        <meta property="og:title" content="<spring:message code='payment-success.jsp.propertie.33' text='default text'></spring:message> &laquo;${hf:escapeHtml4(campaign.name)}&raquo;"/>
        <meta property="og:image" content="${hf:getThumbnailUrl(firstObject.objectImageUrl, "ORIGINAL", "PRODUCT")}"/>
        <meta property="og:description" content="<spring:message code='payment-success.jsp.propertie.34' text='default text'></spring:message>"/>
        <meta name="description" content="<spring:message code='payment-success.jsp.propertie.34' text='default text'></spring:message>"/>
        <title><spring:message code="payment-success.jsp.propertie.33" text="default text"></spring:message> &laquo;${hf:escapeHtml4(campaign.name)}&raquo;</title>
        <%@include file = "/WEB-INF/jsp/includes/common-css.jsp" %>
        <link type="text/css" rel="stylesheet" href="//${hf:getStaticBaseUrl("")}/css-generated/payment.css"/>
        <%@include file="/WEB-INF/jsp/includes/generated/stat-counters-async.jsp" %>
        <%@include file="/WEB-INF/jsp/includes/planeta-js.jsp" %>
        <%@include file="/WEB-INF/jsp/includes/generated/common-templates.jsp" %>
    </head>
    <body>
        <%@ include file="/WEB-INF/jsp/includes/generated/irma/header.jsp" %>
        <%@ include file="/WEB-INF/jsp/includes/generated/irma/main-nav.jsp" %>
        <%@ include file="/WEB-INF/jsp/includes/generated/mobile-header.jsp"%>

        <script type="text/javascript">
            $(document).ready(function () {
                var getDataForShare = {
                    url: "https://${properties['application.host']}/campaigns/${firstObject.ownerId}",
                    className: 'horizontal donate-sharing sharing-mini',
                    counterEnabled: false,
                    hidden: false
                };
                $('.share-cont-horizontal').share(getDataForShare);

                if (window.gtm) {
                    var transactionInfo = {
                        transactionId: '${order.orderId}',
                        amount: '${order.totalPrice}'
                    };
                    var shareInfo = {
                        shareId: '${firstObject.objectId}',
                        name: '${hf:escapeHtml4(firstObject.objectName)}',
                        price: '${firstObject.price}',
                        quantity: '${firstObject.count}'
                    };
                    window.gtm.trackPurchaseShares(transactionInfo, ${hf:toJson(gtmCampaignInfo)}, shareInfo);
                }

                var relatedCampaigns = new CrowdFund.Models.RelatedCampaignsList([], {
                    data: {
                        objectId: ${firstObject.ownerId}
                    }
                });

                var relatedCampaignsList = new CrowdFund.Views.RelatedCampaignsList({
                    el: '.js-project-card-list-container div',
                    collection: relatedCampaigns
                });

                relatedCampaigns.load();

                $('.reward-rich_descr-expand_link').click(function () {
                    $(this).parent().siblings('.reward-rich_descr').toggleClass('expand');
                });

                $('.js-send-feedback-form').on('click', function (e) {
                    e.preventDefault();
                    var form = $("#user-feedback-form").serialize();
                    if (form.indexOf('score=') == -1) {
                        return;
                    }
                    $.post('/api/profile/add-user-feedback.json', form).done(function (response) {
                        if (response && response.success) {
                            if (response.result) {
                                $('.js-feedback-message-container').html('<div class="quality-polling_head">Спасибо! Ваш отзыв отправлен!</div>');
                            } else {
                                $('.js-feedback-message-container').html('<div class="quality-polling_head">Вы уже оставили отзыв ранее!</div>');
                            }
                        } else {
                            $('.js-feedback-message-container').html('<div class="quality-polling_head">При отправке отзыва поизошла ошибка!</div>');
                        }
                    });
                });

                $('.js-subscribe, .js-unsubscribe').click(function (e) {
                    e.preventDefault();
                    var me = $(this).parent();
                    $.get('/api/profile/' + $(e.currentTarget).attr('x-href')).done(function (response) {
                        if (response && response.success) {
                            me.hide();
                            if (me.hasClass('js-subscribe-div')) {
                                $('.js-unsubscribe-div').show();
                            } else {
                                $('.js-subscribe-div').show();
                            }
                        }
                    });
                });

                $.post('/api/profile/check-user-feedback-exists.json', {orderId: ${order.orderId}}).done(function (response) {
                    if (response && !response.result) {
                        $('.js-feedback-form').css('display', 'block');
                    }
                });
            });
        </script>

        <div id="global-container">
            <div id="main-container" class="wrap-container">
                <div id="center-container" class="wrap">
                    <div class="col-12">
                        <div class="pln-payment-box">
                            <c:choose>
                                <c:when test="${transaction.status == 'NEW' && !isMobilePayment && !isDeferred}">
                                    <div class="pln-payment-success">
                                        <div class="pln-payment-success_head">
                                            <spring:message code="payment-success.jsp.propertie.7" text="default text"> </spring:message>
                                            </div>
                                            <div class="pln-payment-success_sub-head">
                                            </div>

                                            <div class="pln-payment-success_widget cf">

                                                <div class="pln-payment-success_widget_i">
                                                    <div class="pln-payment-success_widget_ico">
                                                        <span class="success-widget-icon success-widget-icon-time"></span>
                                                    </div>
                                                    <div class="pln-payment-success_widget_text">
                                                    <spring:message code="payment-success.jsp.propertie.8" text="default text"> </spring:message>
                                                    </div>
                                                </div>

                                                <div class="pln-payment-success_widget_i">
                                                    <div class="pln-payment-success_widget_ico">
                                                        <span class="success-widget-icon success-widget-icon-mail"></span>
                                                    </div>
                                                    <div class="pln-payment-success_widget_text">
                                                    <spring:message code="payment-success.jsp.propertie.9" text="default text"> </spring:message>
                                                    </div>
                                                </div>

                                                <div class="pln-payment-success_widget_i">
                                                    <div class="pln-payment-success_widget_ico">
                                                        <span class="success-widget-icon success-widget-icon-flag"></span>
                                                    </div>
                                                    <div class="pln-payment-success_widget_text">
                                                    <spring:message code="payment-success.jsp.propertie.10" text="default text"> </spring:message> <a href="/account"><spring:message code="payment-success.jsp.propertie.11" text="default text"> </spring:message></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                </c:when>
                                <c:otherwise>
                                    <div class="pln-payment-success">
                                        <c:choose>
                                            <c:when test="${isMobilePayment}">
                                                <div class="pln-payment-success_head">
                                                    <spring:message code="payment-success.jsp.propertie.12" text="default text"> </spring:message>
                                                    </div>
                                                    <div class="pln-payment-success_sub-head">
                                                    <spring:message code="payment-success.jsp.propertie.31" text="default text"> </spring:message>
                                                        <br>
                                                        <spring:message code="payment-success.jsp.propertie.32" text="default text"> </spring:message>
                                                    </div>
                                            </c:when>
                                            <c:when test="${isDeferred}">
                                                <div class="pln-payment-success_head">
                                                    <spring:message code="payment-success.jsp.propertie.14" text="default text"> </spring:message>
                                                    </div>
                                                    <div class="pln-payment-success_sub-head">
                                                    <spring:message code="payment-success.jsp.propertie.15" text="default text"> </spring:message>
                                                    </div>
                                            </c:when>
                                            <c:otherwise>
                                                <div class="pln-payment-success_head">
                                                    <spring:message code="payment-success.jsp.propertie.16" text="default text"> </spring:message> <span class="hl"><spring:message code="payment-success.jsp.propertie.17" text="default text"> </spring:message></span>!
                                                    </div>
                                                    <div class="pln-payment-success_sub-head">
                                                        <spring:message code="payment-success.jsp.propertie.18" text="default text"></spring:message> <a href="//${properties['application.host']}/campaigns/${campaign.webCampaignAlias}"><spring:message code="payment-success.jsp.propertie.18.1" text="default text"></spring:message></a><spring:message code="payment-success.jsp.propertie.18.2" text="default text"></spring:message><b>${order.buyerEmail}</b><spring:message code="payment-success.jsp.propertie.19" text="default text"></spring:message>
                                                    </div>
                                            </c:otherwise>
                                        </c:choose>

                                        <div class="pln-payment-success_widget cf">
                                            <div class="pln-payment-success_widget_i">
                                                <div class="pln-payment-success_widget_ico">
                                                    <span class="success-widget-icon success-widget-icon-flag"></span>
                                                </div>
                                                <div class="pln-payment-success_widget_text">
                                                    <spring:message code="payment-success.jsp.propertie.20" text="default text"> </spring:message> <a href="${redirectUrl}"><spring:message code="payment-success.jsp.propertie.11" text="default text"> </spring:message></a>
                                                </div>
                                            </div>

                                            <div class="pln-payment-success_widget_i">
                                                <div class="pln-payment-success_widget_ico">
                                                    <span class="success-widget-icon success-widget-icon-star"></span>
                                                </div>
                                                <div class="pln-payment-success_widget_text">
                                                    <div class="js-subscribe-div" <c:if test='${isSubscribeded}'>style="display:none"</c:if> >
                                                        <spring:message code="payment-success.jsp.propertie.37" text="default text"> </spring:message>
                                                        <a href="javascript: void(0);" class="js-subscribe" x-href="subscribe.json?profileId=${campaign.creatorProfileId}">
                                                            <spring:message code="payment-success.jsp.propertie.39" text="default text"> </spring:message>
                                                        </a>
                                                    </div>
                                                    <div class="js-unsubscribe-div" <c:if test='${not isSubscribeded}'>style="display:none"</c:if> >
                                                        <spring:message code="payment-success.jsp.propertie.38" text="default text"> </spring:message>
                                                        <a href="javascript: void(0);" class="js-unsubscribe" x-href="unsubscribe.json?profileId=${campaign.creatorProfileId}">
                                                            <spring:message code="payment-success.jsp.propertie.40" text="default text"> </spring:message>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="pln-payment-success_widget_i">

                                                <div class="pln-payment-success_widget_sharing">
                                                    <div class="pln-payment-success_widget_sharing-head">
                                                    <spring:message code="payment-success.jsp.propertie.23" text="default text"> </spring:message>
                                                    </div>
                                                    <div class="share-cont-horizontal"></div>
                                                </div>

                                                <div class="pln-payment-success_widget_text">
                                                <spring:message code="payment-success.jsp.propertie.24" text="default text"> </spring:message>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </c:otherwise>
                            </c:choose>
                        </div>
                        <div class="pln-payment-box js-feedback-form" style="display: none;">
                            <form id="user-feedback-form" class="quality-polling js-feedback-message-container">
                                <div class="quality-polling_head">
                                    Оцените качество сервиса
                                </div>
                                <div class="quality-polling_tip">
                                    1 — очень плохо, 5 — хорошо, 10 — отлично!
                                </div>
                                <div class="quality-polling_value">
                                    <div class="quality-polling-score">
                                        <c:forEach var = "i" begin = "1" end = "10">
                                            <div class="quality-polling-score_i">
                                                <div class="form-ui form-ui-default">
                                                    <input class="form-ui-control" id="score-${i}" type="radio" name="score" value="${i}">
                                                        <label class="form-ui-label" for="score-${i}">
                                                            <span class="form-ui-txt">
                                                                <span class="form-ui-val">${i}</span>
                                                            </span>
                                                        </label>
                                                </div>
                                            </div>
                                        </c:forEach>

                                    </div>
                                </div>

                                <input type="hidden" name="orderId" value="${order.orderId}"/>
                                <input type="hidden" name="userId" value="${order.buyerId}"/>
                                <input type="hidden" name="pageType" value="PAYMENT_SUCCESS"/>
                                <c:if test="${campaign != null}">
                                    <input type="hidden" name="campaignId" value="${campaign.campaignId}"/>
                                </c:if>

                                <div class="quality-polling_action">
                                    <div class="quality-polling_extra">
                                        <div class="form-ui form-ui-default">
                                            <input class="form-ui-control" id="extraTesting" type="checkbox" name="extraTesting" value="true">
                                                <label class="form-ui-label" for="extraTesting">
                                                    <span class="form-ui-txt">я хочу принять участие в детальном опросе</span>
                                                </label>
                                        </div>
                                    </div>
                                    <div class="quality-polling_btn">
                                        <button type="submit" class="btn btn-primary js-send-feedback-form">Отправить</button>
                                    </div>
                                </div>
                            </form>
                        </div>

                        <c:if test="${not empty similarShares}">
                            <div class="pln-payment-box mrg-b-35">
                                <div class="pln-payment-box_head">
                                    <spring:message code="payment-success.jsp.propertie.25" text="default text"> </spring:message>
                                    </div>

                                    <div class="reward-rich">
                                    <c:forEach items="${similarShares}" var="share">
                                        <div class="reward-rich_i cf">
                                            <c:set var="campaignUrl" value="https://${properties['application.host']}/campaigns/${share.campaign.webCampaignAlias}"/>
                                            <c:if test="${not empty share.imageUrl}">
                                                <div class="reward-rich_img">
                                                    <img src="${hf:getGroupAvatarUrl(share.imageUrl, "MEDIUM" ) }">
                                                </div>
                                            </c:if>
                                            <div class="reward-rich_cont">
                                                <div class="reward-rich_project-name">
                                                    <spring:message code="payment-success.jsp.propertie.26" text="default text"> </spring:message> <a href="${campaignUrl}"><c:out value="${share.campaign.nameHtml}" escapeXml="false"/></a>
                                                    </div>
                                                    <div class="reward-rich_name">
                                                    ${hf:escapeHtml4(share.nameHtml)}
                                                </div>
                                                <div class="reward-rich_descr">
                                                    ${share.descriptionHtml}
                                                </div>
                                                <c:if test="${not empty share.imageUrl and fn:length(share.description) > 170 or empty share.imageUrl and fn:length(share.description) > 315}">
                                                    <div class="reward-rich_descr-expand">
                                                        <span class="reward-rich_descr-expand_link">
                                                            <spring:message code="payment-success.jsp.propertie.27" text="default text"> </spring:message>
                                                            </span>
                                                        </div>
                                                </c:if>
                                            </div>

                                            <div class="reward-rich_action-block">
                                                <div class="reward-rich_price"><fmt:formatNumber value="${share.price}"/>
                                                    <span class="b-rub">
                                                        <spring:message code="payment-success.jsp.propertie.28" text="default text"> </spring:message>
                                                        </span>
                                                    </div>

                                                    <div class="reward-rich_action">
                                                        <div class="btn btn-primary btn-block" onclick="location.href = '${campaignUrl}/donate/${share.shareId}'">
                                                        <spring:message code="payment-success.jsp.propertie.29" text="default text"> </spring:message>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                    </c:forEach>
                                </div>

                            </div>
                        </c:if>

                        <div class = "js-project-list-container project-related">
                            <div class="project-related-head">
                                <spring:message code="payment-success.jsp.propertie.30" text="default text"> </spring:message>
                                </div>
                                <div class = "js-project-card-list-container">
                                    <div class="project-card-list"></div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        <%@ include file="/WEB-INF/jsp/includes/generated/irma/footer.jsp" %>
    </body>
</html>

<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%--<%@include file="/WEB-INF/jsp/includes/generated/doctype-header.jsp" %>--%>

<!DOCTYPE html>
<%@ include file="/WEB-INF/jsp/includes/generated/taglibs.jsp" %>
<!--[if lt IE 7]>      <html class="ie lt-ie7" lang="ru"> <![endif]-->
<!--[if IE 7]>         <html class="ie ie7" lang="ru"> <![endif]-->
<!--[if IE 8]>         <html class="ie ie8" lang="ru"> <![endif]-->
<!--[if IE 9]>         <html class="ie ie9" lang="ru"> <![endif]-->
<!--[if gt IE 9]><!--> <html lang="ru"
                             xmlns="http://www.w3.org/TR/REC-html40"> <!--<![endif]-->
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Договор: Физическое лицо</title>
    <meta name="title" content="Договор: Физическое лицо">
    <%--<link type="text/css" rel="stylesheet" href="//${hf:getStaticBaseUrl("")}/css-generated/common.css"/>--%>
    <style type="text/css">
        p{
            margin: 0 0 2px;
            text-align: justify;
            letter-spacing: 0.1px;
        }

        .page{
            font-family: "Times New Roman";
            font-size: 12.5px;
            position: relative;
            padding: 0mm;
            width: 21cm;
            height: 30.5cm;
            margin: 0;
            /*margin: 4mm 0mm 8mm;*/
            page-break-before: always;
            /*border: 1px  #000000;*/
        }

        div.cont {
            width: 21cm;
            height: 30.5cm;
        }

        div.foot {
            position: absolute;
            width: 21cm;
            height: 0.3cm;
            bottom: 0px;
        }

        .columnable {
            /* turn it on if you wanna see it in browser (and save in chrome as pdf) in columns */

            /*-webkit-column-width: auto;
            -moz-column-width: auto;
            column-width: auto;

            -webkit-column-count: 2;
            -moz-column-count: 2;
            column-count: 2;

            -webkit-column-gap: 30px;
            -moz-column-gap: 30px;
            column-gap: 30px;*/
        }

        .left {
            position: absolute;
        }

        .right {
            position: absolute;
            right: 0px;
        }

        h4 {
            margin: 5px 0px 0px;
            letter-spacing: 0.1px;
        }

        td {
            padding-right: 10px;
        }
    </style>
</head>
<body style="background-color: white;" contenteditable="true">

<div class="page">
    <div class="cont columnable">
        <h4 align="center">
            <strong>СОГЛАШЕНИЕ</strong>
        </h4>
        <h4 align="center">
            <strong>о порядке использования сервиса «Акционирование»</strong>
        </h4>
        <p align="center">
            <strong></strong>
        </p>
        <div>
            <p style="position: absolute;">г. Москва </p>
            <c:set var="now" value="<%=new java.util.Date()%>" />
            <p style="text-align: right" contenteditable="true">${hf:dateFormatByTemplate(now, 'dd MMMM yyyy')} г.</p>
        </div>
        <p>
            <strong>Общество с ограниченной ответственностью «ГЛОБАЛ НЕТВОРКС»</strong>,
            далее именуемое <strong>Организатор Сервиса</strong>, в лице
            Генерального директора <strong>Мурачковского Федора Вадимовича</strong>,
            действующего на основании Устава, с одной стороны, и
        </p>
        <p>
            <strong><c:out value="${contractor.name}"/></strong>, далее именуемый (ая) <strong>Организатор Проекта</strong>, паспорт
            <c:set var="passportSeria" value="${fn:substring(contractor.passportNumber, 0, 4)}" />
            <c:set var="passportCode" value="${fn:substring(contractor.passportNumber, 5, contractor.passportNumber.length())}" />
            серия <strong>${passportSeria}</strong> номер <strong>${passportCode}</strong> выдан <strong>${contractor.authority}</strong>
            дата выдачи <strong><fmt:formatDate type="date" pattern="dd.MM.yyyy" value="${contractor.issueDate}" /></strong>,
            код подразделения <strong><c:out value="${contractor.unit}"/></strong>, с другой стороны, вместе именуемые Стороны,
            заключили настоящее Соглашение о порядке использования сервиса
            «Акционирование» (далее – Соглашение) на следующих условиях:
        </p>
        <h4 align="center">
            <strong>1. </strong>
            <strong>Предмет Соглашения</strong>
        </h4>
        <p>
            1.1. По условиям настоящего Соглашения Организатор Сервиса обязуется
            предоставить Организатору Проекта для использования интернет-сервис
            «Акционирование» (далее – Сервис), размещенного на веб-страницах
            интернет-сайта planeta.ru (далее – Портал), а Организатор Проекта
            обязуется использовать Сервис для привлечения финансирования Проекта с
            рабочим названием
            <strong>«<c:out value="${campaign.name}"/>»</strong>,
            условия которого определены Организатором Проекта и доступны по
            адресу <strong>https://planeta.ru/campaigns/${campaign.campaignId}</strong>. Условия
            Проекта, размещенные по указанному адресу, являются частью настоящего
            Соглашения.
        </p>
        <p>
            1.2. Проект должен быть реализован (завершен) в сроки, установленные в
            условиях Проекта.
        </p>
        <h4 align="center">
            <strong>2.</strong>
            <strong>Организатор Проекта имеет право:</strong>
        </h4>
        <p>
            2.1. Получить собранные средства при выполнении всех условий настоящего
            Соглашения.
        </p>
        <p>
            2.2. Обращаться с запросами к уполномоченным лицам Организатора Сервиса
            для разъяснения спорных или неясных ситуаций. Контактная информация
            приводится в разделе «Помощь» на Портале.
        </p>
        <p>
            2.3. Получить доступ в режиме онлайн к статистическим данным
            размещенного Проекта.
        </p>
        <h4 align="center">
            <strong>3.</strong>
            <strong>Обязанности Организатора Проекта</strong>
        </h4>
        <p>
            3.1. Использовать предоставленный Сервис в соответствии с его целевым
            назначением, а именно сбор средств для финансирования и продвижения
            Проекта, определенного в п.1.1. настоящего Соглашения.
        </p>
        <p>
            3.2. Выполнить перед Акционерами все обязательства, предусмотренные
            условиями Проекта.
        </p>
        <p>
            3.3. Предоставить Организатору Сервиса копии документов,
            предусмотренные настоящим Соглашением.
        </p>
        <p>
            3.4. Предоставить Пользователям достоверную информацию об условиях
            участия в Проекте, в том числе необходимую информацию о результатах
            каждой Акции, а также сведения об Организаторе Проекта в соответствии с
            требованиями законодательства РФ о защите прав потребителей.
        </p>
        <p>
            3.5. В случае невозможности исполнить обязательства, принятые перед
            Пользователями, вернуть все денежные средства, полученные от
            Организатора Сервиса. Возврат проводится на расчетный счет Организатора
            Сервиса.
        </p>
        <p>
            3.6. Не размещать Контент, права на который Организатору Проекта не
            принадлежат.
        </p>
        <p>
            3.7. Разместить адресную ссылку на Проект на своем официальном сайте, а
            также на страницах своих сообществ в социальных сетях (при наличии
            указанных ресурсов). В информации о Проекте на сторонних
            интернет-ресурсах Организатор Проекта размещает адресную ссылку на
            страницу своего Проекта при наличии соответствующей возможности.
        </p>
        <p>
            3.8. Не размещать такой же Проект на интернет-ресурсах, использующих
            схему краудфандинга. В случае нарушения данного обязательства
            Организатор Сервиса имеет право отказать Организатору Проекта в
            перечислении собранных средств.
        </p>
        <p>
            3.9. Разместить на странице Проекта, во вкладке «Новости проекта» информацию
            об успешном завершении Проекта, сроках и порядке передачи вознаграждений (наград).
        </p>
        <p>
            3.10. Самостоятельно рассчитать и уплатить налоги или сборы, подлежащие уплате в соответствие
            с законодательством Российской Федерации или иного государства.
        </p>
        <h4 align="center">
            <strong>4.</strong>
            <strong>Права Организатора Сервиса</strong>
        </h4>
        <p>
            4.1. Проводить обработку персональных данных, предоставленных
            Организатором Проекта, а также предоставлять указанные данные третьим
            лицам, если это необходимо для проведения расчетов, по запросам
            уполномоченных органов государственной власти или требуется для
            выполнения иных обязательств<sup>1</sup>.
        </p>
        <p>
            4.2. Приостанавливать или прекратить действие Проекта в случае
            выявления нарушений Организатором Проекта Правил использования сервиса
            «Акционирование» (далее – Правила) или настоящего Соглашения.
        </p>
        <p>
            4.3. Удержать комиссию в размере, предусмотренном настоящим
            Соглашением.
        </p>
        <p>
            4.4. Разместить на любой странице Портала, включая страницы Проекта,
            рекламные материалы, как своей деятельности, товаров и услуг, так и
            деятельности, товаров и услуг третьих лиц. Согласования содержания
            рекламных материалов с Организатором Проекта не требуется. Размещение
            Организатором Сервиса рекламных материалов на странице Проекта не
            порождает у Организатора Сервиса и/или третьих лиц, рекламные материалы
            которых размещены, обязанности выплатить какое-либо вознаграждение
            Организатору Проекта. Рекламные материалы не могут размещаться
            непосредственно в тексте, фото- и видео-материалах описания Проекта, а
            также непосредственно в тексте новостей и/или комментариев к Проекту.
            Рекламные материалы должны соответствовать требованиям действующего
            законодательства РФ.
        </p>
        <h4 align="center">
            <strong>5.</strong>
            <strong>Обязанности Организатора Сервиса</strong>
        </h4>
        <p>
            5.1. Предоставить Организатору Проекта техническую возможность
            разместить условия Проекта на соответствующих страницах Портала.
        </p>
        <p>
            5.2. Перечислить поступившие денежные средства на счет Организатора
            Проекта в порядке и на условиях настоящего Соглашения.
        </p>
        <h4 align="center">
            <strong>6.</strong>
            <strong>Финансовые расчеты</strong>
        </h4>
        <p>
            6.1. Сумма, заявленная Организатором Проекта, составляет не менее
            <strong><fmt:formatNumber type="number" value="${campaign.targetAmount}" />,00 (${wordNumberTargetAmount}) <spring:message code="decl.rubles" arguments="${hf:plurals(campaign.targetAmount)}"></spring:message>.</strong>
        </p>
        <p>
            6.2. Денежные средства перечисляются на счет Организатора Проекта в течение 7 (семи) рабочих
            дней после подписания Сторонами Акта сдачи-приемки оказанных услуг. Перечисление Организатором Сервиса
            денежных средств на счета третьих лиц не допускается.
        </p>
        <p>
            6.3. До перечисления денежных средств Стороны подписывают Акт
            сдачи-приемки оказанных услуг.
        </p>
        <p>
            6.4. Из общей суммы денежных средств вычитается комиссия Организатора Сервиса,
            определяемая в соответствии с п.6.5. Соглашения. Комиссия удерживается без НДС,
            так как Организатор Сервиса использует упрощенную систему налогообложения (глава 26.2. НК РФ).
        </p>
        <p>
            6.5. Комиссия Организатора Сервиса определяется в следующем порядке:
        </p>
        <div>
            <br clear="all"/>
            <hr align="left" size="1" width="33%"/>
            <div id="ftn1">
                <p>
                    <sup>1</sup>
                    Данный пункт применяется, если Организатором Проекта выступает
                    физическое лицо или индивидуальный предприниматель.
                </p>
            </div>
            <div id="ftn2">
                <p>
                    <sup>2</sup>
                    Внесенные Пользователем денежные средства могут быть потрачены на
                    Акции других Проектов или по заявлению Пользователя перечисляются
                    на его банковский или иной счет.
                </p>
            </div>
        </div>
    </div>
    <div class="foot">
        <div class="left">___________________________/ <strong>Ф.В. Мурачковский</strong></div>
        <div class="right">___________________________/ <strong><c:out value="${contractor.initialsWithLastName}"/></strong></div>
    </div>
</div>
<div class="page">
    <div class="cont">
        <div class="columnable">
            <p>
                6.5.1. в случае поступления денежных средств в размере менее 50% от
                суммы, указанной в п.6.1. настоящего Соглашения, Проект считается
                несостоявшимся, денежные средства Организатору Проекта не
                перечисляются, а разблокируются в Личном кабинете Пользователя<sup>2</sup>;
            </p>
            <p>
                6.5.2. в случае поступления денежных средств в размере от 50% до 99,9%
                от суммы, указанной в п.6.1. настоящего Соглашения, комиссия
                Организатора Сервиса составляет 15% (пятнадцать процентов) от суммы
                сбора (включая расходы на комиссию финансового агрегатора);
            </p>
            <p>
                6.5.3. в случае поступления денежных средств в размере более 99,9% от
                суммы, указанной в п.6.1. настоящего Соглашения, комиссия Организатора
                Сервиса составляет 10% (десять процентов) от суммы сбора (включая
                расходы на комиссию финансового агрегатора).
            </p>
            <p>
                6.6. Организатор Сервиса перечисляет аккумулированные средства
                Пользователей в случае одновременного наступления следующих условий:
            </p>
            <p>
                6.6.1. для финансирования Проекта собраны денежные средства в размере,
                не менее 50% от суммы, указанной в п.6.1. настоящего Соглашения);
            </p>
            <p>
                6.6.2. Организатор Проекта предоставил:
            </p>
            <p>
                - <u>для юридического лица</u>: заверенные руководителем или
                уполномоченным лицом копии свидетельств ОГРН и ИНН;
            </p>
            <p>
                - <u>для индивидуального предпринимателя</u>: заверенные им копии
                свидетельств ОГРН ИП и ИНН;
            </p>
            <p>
                - <u>для физического лица</u>: заверенные нотариально или Организатором
                Сервиса копия паспорта (копии страниц, содержащих основные сведения с
                фотографией, а также страницы с действующей регистрацией по месту
                жительства); свидетельства ИНН (если имеется).
            </p>
            <p>
                6.6.3. между Сторонами подписан Акт сдачи-приемки оказанных услуг.
            </p>
            <h4 align="center">
                <strong>7.</strong>
                <strong>Условия об интеллектуальных правах</strong>
            </h4>
            <p>
                7.1. В целях обеспечения правовых условий для исполнения обязательств
                по настоящему Соглашению Организатор Проекта в порядке и пределах,
                определенных настоящим Соглашением, предоставляет Организатору Сервиса
                неисключительное право на доведение любого Контента, размещенного на
                веб-страницах Портала до всеобщего сведения таким образом, что любое
                лицо может получить доступ к нему из любого места и в любое время по
                собственному выбору (доведение до всеобщего сведения).
            </p>
            <p>
                7.2. Право, указанное в п.7.1. настоящего Соглашения предоставляется
                Организатору Сервиса бессрочно с момента подписания настоящего
                Соглашения для использования на территории всего мира.
            </p>
            <p>
                7.3. В связи с тем, что Организатор Проекта самостоятельно размещает
                Контент на страницах Портала, вознаграждение за предоставленное право
                использования Контента Организатором Сервиса не выплачивается, если
                иное не установлено соглашением Сторон.
            </p>
            <h4 align="center">
                <strong>8.</strong>
                <strong>Гарантийные обязательства</strong>
            </h4>
            <p>
                8.1. Настоящим Соглашением Организатор Проекта признает и подтверждает,
                что именно он, Организатор Проекта, является стороной в правоотношениях
                с Пользователями (Акционерами) по поводу предметов, услуг,
                интеллектуальных прав и других результатов Акций, предоставляемых в
                связи с реализацией Проекта. Всю полноту ответственности, как за
                прямые, так и за косвенные убытки, возникшие (могущие возникнуть) в
                связи с реализацией Проекта в целом или его отдельных Акций, несет
                Организатор Проекта. Организатор Сервиса не несет никакой
                ответственности перед Пользователями, за действия или бездействие
                Организатора Проекта.
            </p>
            <p>
                8.2. Организатор Проекта гарантирует использование денежных средств
                исключительно в соответствии с заявленными условиями Проекта. В случае
                нарушения данного обязательства Организатор Проекта самостоятельно
                несет ответственность перед Пользователями.
            </p>
            <p>
                8.3. В случае привлечения Организатором Проекта любых третьих лиц в
                связи с реализацией Проекта в целом или в части Стороны руководствуются
                следующими положениями: Организатор Сервиса не является Стороной,
                обязанной по отношению к третьим лицам, привлеченным Организатором
                Проекта для исполнения каких-либо обязательств Организатора Проекта или
                осуществления прав, и не несет перед указанными лицами никакой
                ответственности.
            </p>
            <h4 align="center">
                <strong>9.</strong>
                <strong>Применимое право и урегулирование споров</strong>
            </h4>
            <p>
                9.1. Настоящее Соглашение регламентируется действующим
                законодательством РФ.
            </p>
            <p>
                9.2. Стороны устанавливают обязательный претензионный порядок
                разрешения споров. Претензия Стороны направляется в письменной форме.
                Претензия должна быть рассмотрена в течение 15 (пятнадцати) дней с
                момента получения другой Стороной.
            </p>
            <p>
                9.3. Судебные споры между Сторонами рассматриваются по месту нахождения
                Организатора Сервиса.
            </p>
            <h4 align="center">
                <strong>10. </strong>
                <strong>Заключительные положения</strong>
            </h4>
            <p>
                10.1. Неотъемлемой частью настоящего Соглашения являются
                    Пользовательское соглашение сервиса «Акционирование», размещенные на Портале.
            </p>
            <p>
                10.2. Заключая настоящее Соглашение, Организатор Проекта подтверждает,
                что он в полном объеме ознакомился с Правилами, все положения которых
                ему понятны.
            </p>
            <p>
                10.3. Определяя права и обязанности в настоящем Соглашении, Стороны
                устанавливают следующее: изменение условий Правил не требует внесения
                письменных изменений в настоящее Соглашение, если вносимые изменения не
                влекут существенного изменения порядка финансовых расчетов между
                Сторонами. Организатор Проекта обязуется соблюдать Правила
                использования сервиса в новой редакции с момента вступления в силу
                новой редакции Правил.
            </p>
            <p>
                10.4. Настоящее Соглашение действует в течение 1 (одного) года. Если ни
                одна из Сторон не заявит в письменной форме о расторжении Соглашения,
                оно автоматически продлевается на следующий год.
            </p>
            <p>
                10.5. Настоящее Соглашение составлено в 2 (двух) экземплярах, имеющих
                одинаковую юридическую силу по одному для каждой из Сторон.
            </p>
        </div>
        <div>
            <br clear="all"/>
            <div align="center">
                <table border="0" cellspacing="0" cellpadding="0" width="709">
                    <tbody>
                    <tr>
                        <td width="359" valign="top">
                            <p>
                                <strong>Организатор Сервиса</strong>
                            </p>
                            <p>
                                <strong>ООО «ГЛОБАЛ НЕТВОРКС»</strong>
                            </p>
                            <p>
                                <strong>Юр. адрес:</strong> 129090, г. Москва, Проспект Мира,
                                д.19, строение 3, пом. VIII
                            </p>
                            <p>
                                <strong>Фактический адрес:</strong> 129090, г. Москва, Проспект Мира,
                                д.19, строение 3, пом. VIII
                            </p>
                            <p>
                                <strong>Адрес для отправки корреспонденции:</strong> а/я 25, г. Москва,
                                129090, (ООО «Глобал Нетворкс»)
                            </p>
                            <p>
                                <strong>ОГРН</strong> 1107746618385
                            </p>
                            <p>
                                <strong>ИНН</strong> 7722724252 <strong>КПП</strong> 770201001
                            </p>
                            <p>
                                <strong>ОКПО</strong> 67939318; <strong>ОКВЭД</strong> 72.2
                            </p>
                            <p>
                                <strong>Рас. счет</strong> 40702810500310000443
                            </p>
                            <p>
                                в АБ "ИНТЕРПРОГРЕССБАНК" (ЗАО)
                            </p>
                            <p>
                                <strong>Кор. счет</strong> 30101810100000000402
                            </p>
                            <p>
                                <strong>БИК</strong> 044525402
                            </p>
                        </td>
                        <td width="359" valign="top">
                            <p>
                                <strong>Организатор Проекта</strong>
                            </p>
                            <p>
                                <strong><c:out value="${contractor.name}"/></strong>
                            </p>
                            <p>
                                <strong>Дата рождения</strong> <fmt:formatDate type="date" pattern="dd.MM.yyyy" value="${contractor.birthDate}" />
                            </p>
                            <p>
                                <strong>Паспорт серия</strong> <c:out value="${passportSeria}"/> <strong>номер </strong><c:out value="${passportCode}"/>
                            </p>
                            <p>
                                <strong>Выдан</strong> <c:out value="${contractor.authority}"/>
                            </p>
                            <p>
                                <strong>Дата выдачи </strong><fmt:formatDate type="date" pattern="dd.MM.yyyy" value="${contractor.issueDate}" />
                            </p>
                            <p>
                                <strong>Код подр. </strong> <c:out value="${contractor.unit}"/>
                            </p>
                            <p>
                                <strong>Зарегистрирован </strong> <c:out value="${contractor.legalAddress}"/>
                            </p>
                            <c:if test="${not empty contractor.inn}">
                            <p>
                                <strong>ИНН</strong> <c:out value="${contractor.inn}"/>
                            </p>
                            </c:if>
                            <p>
                                <strong>Рас. счет</strong> <c:out value="${contractor.checkingAccount}"/>
                            </p>
                            <p>
                                <strong>Банк получателя</strong> <c:out value="${contractor.beneficiaryBank}"/>
                            </p>
                            <p>
                                <strong>Кор. счет</strong>  <c:out value="${contractor.correspondingAccount}"/>
                            </p>
                            <p>
                                <strong>БИК</strong> <c:out value="${contractor.bic}"/>
                            </p>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="foot">
        <div class="left">___________________________/ <strong>Ф.В. Мурачковский</strong></div>
        <div class="right">___________________________/ <strong><c:out value="${contractor.initialsWithLastName}"/></strong></div>
    </div>
</div>

</div>

</body>
</html>
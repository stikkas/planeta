<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@include file="/WEB-INF/jsp/includes/generated/doctype-header.jsp" %>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Библиородина: Сертификаты заказа</title>
    <meta name="title" content="Библиородина: Сертификаты заказа">
    <link type="text/css" rel="stylesheet" href="//${hf:getStaticBaseUrl("")}/css-generated/common.css"/>
    <link type="text/css" rel="stylesheet" href="//${hf:getStaticBaseUrl("")}/css-generated/bibliorodina.css"/>
</head>
<body class="biblio-cert-page">
    <c:forEach items="${libraries}" varStatus="loop">
        <div class="biblio-cert">
            <img class="biblio-cert_img" src="//${hf:getStaticBaseUrl("")}/images/biblio/cert.jpg"/>
            <div class="biblio-cert_cont">
            <c:if test="${isOrg == false}">            
                Настоящий сертификат подтверждает, что
                <br>
                <span class="biblio-cert_name">${customerName}</span>
                <br>
                оформил<c:if test="${gender == 'FEMALE'}">а</c:if> подписку на печатные издания
                <br>
                <c:forEach items="${books}" varStatus="bookloop">
                    <b>${bookloop.current}</b><c:if test="${bookloop.last == false}">,</c:if>
                </c:forEach>
                <br>
                в поддержку библиотеки
                <br>
                <b>${loop.current}</b>
            </c:if>
            <c:if test="${isOrg == true}">            
                Настоящий сертификат выдан
                <br>
                <span class="biblio-cert_name">${customerName}</span>
                <br>
                в подтверждение факта оформления подписки
                <br>
                на печатные издания
                <br>
                <c:forEach items="${books}" varStatus="bookloop">
                    <b>${bookloop.current}</b><c:if test="${bookloop.last == false}">,</c:if>
                </c:forEach>
                <br>
                в поддержку библиотеки
                <br>
                <b>${loop.current}</b>
            </c:if>
            </div>
        </div>
        <c:if test="${loop.last == false}">
        <div class="page-break"></div>
        </c:if>
    </c:forEach>
</body>

    <%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
    <c:set var="baseUrl" value="${hf:getStaticBaseUrl('')}"/>

    <script src="//${baseUrl}/js/lib/jquery.sticky-kit.min.js"></script>
    <script type="text/javascript">
        $(function() {
            if (!window.isMobileDev) {
                $(".r-menu-project-wrap").stick_in_parent({
                    parent: ".pln-content-box",
                    offset_top: 10
                });
            }
        });
    </script>

    <div id="right-sidebar">
        <div class="r-menu-project-wrap">
            <div class="r-menu-project-block">
                <div class="n-side-wrap">
                    <div class="n-side-block">
                        <div class="n-side-h">
                            Правовая информация
                        </div>
                        <div class="r-menu-project">
                            <ul class="rmp-block">
                                <li class="rmp-item">
                                    <a href="https://${properties["application.host"]}/welcome/agreement.html" <c:if test="${action  == \"agreement\"}">class="active"</c:if>>Пользовательское соглашение</a>
                                </li>
                                <li class="rmp-item">
                                    <a href="https://${properties["application.host"]}/welcome/projects-agreement.html" <c:if test="${action  == \"projects-agreement\"}">class="active"</c:if>>Пользовательское соглашение сервиса «Акционирование»</a>
                                </li>
                                <li class="rmp-item">
                                    <a href="https://${properties["application.host"]}/welcome/private-info.html" <c:if test="${action  == \"private-info\"}">class="active"</c:if>>Положение о личной информации</a>
                                </li>
                                <li class="rmp-item">
                                    <a href="https://${properties["application.host"]}/welcome/share-return.html" <c:if test="${action  == \"share-return\"}">class="active"</c:if>>Возврат результатов акции</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
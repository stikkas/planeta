<%@ include file="/WEB-INF/jsp/includes/generated/doctype-header.jsp" %>
<head>
    <title>Регистрация на Планете</title>
    <%@include file="/WEB-INF/jsp/includes/common-css.jsp" %>
    <%@include file="/WEB-INF/jsp/includes/generated/common-templates.jsp" %>
    <%@include file="/WEB-INF/jsp/includes/generated/stat-counters-head.jsp" %>
    <%@include file="/WEB-INF/jsp/includes/planeta-registration-js.jsp" %>
</head>

<body>


<div id="global-container">
    <div id="main-container" class="wrap-container">
        <div id="center-container">
            <div class="wrap">
                <div class="col-12">
                    <div class="pln-content-box">
                        <div class="pln-content-box_cont cf pln-content-box_cont__offset" id="account-merge-container">



                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(function(){
        var c = workspaceInitParameters.configuration;
        var staticNodesService = new StaticNodesService(c.staticNode, c.staticNodes, c.resourcesHost,
            c.jsBaseUrl, c.flushCache, c.compress);
        TemplateManager.init(workspaceInitParameters.currentLanguage, staticNodesService);

        var options = {};
        <c:if test="${not empty credentialsInfo}">
            options.credentialsInfo = ${hf:toJson(credentialsInfo)};
        </c:if>
        <c:if test="${not empty userAuthorizationInfo}">
            options.userAuthorizationInfo = ${hf:toJson(userAuthorizationInfo)};
        </c:if>
        <c:if test="${not empty email}">
            options.email = '${email}';
        </c:if>


        var model = new AccountMerge.Models.Content(options);

        var contentView = new AccountMerge.Views.Content({model: model, el: "#account-merge-container"});

        contentView.render();



    });
</script>
</body>
</html>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ include file="/WEB-INF/jsp/includes/generated/doctype-header.jsp" %>
<%@taglib prefix="p" uri="http://planeta.ru/tags" %>
<head>
    <title>Всероссийский Душевный Bazar на Planeta.ru</title>

    <meta property="og:title" content="Всероссийский Душевный Bazar на Planeta.ru">
    <meta name="description"
          content="В рамках онлайн-ярмарки Душевный Bazar на Planeta.ru у вас есть возможность приобрести сувениры, подарки родным и близким к Новому Году, а также познакомиться со множеством благотворительных проектов!">
    <meta property="og:description"
          content="В рамках онлайн-ярмарки Душевный Bazar на Planeta.ru у вас есть возможность приобрести сувениры, подарки родным и близким к Новому Году, а также познакомиться со множеством благотворительных проектов!">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta property="og:image" content="https://s3.planeta.ru/i/14078e/1473263878943_renamed.jpg">

    <%@include file="/WEB-INF/jsp/includes/generated/common-favicon.jsp" %>
    <link href='https://fonts.googleapis.com/css?family=Cuprum:400,400italic,700,700italic&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
    <link type="text/css" rel="stylesheet" href="//${hf:getStaticBaseUrl("")}/css-generated/bazar-2016.css" />
    <%@include file="/WEB-INF/jsp/includes/generated/stat-counters-head.jsp" %>
    <%@include file="/WEB-INF/jsp/includes/planeta-js.jsp" %>

    <p:script src="bazar.js" />

    <script>
        $(document).ready(function () {
            var projectsContainer = new Promo.Views.CampaignListContainer({
                el: '.js-project-container',
                template: '#bazar-project-container-template',
                model: new Promo.Models.Filter({
                    category: 'BAZAR2016',
                    limit: 50,
                    carousel: {
                        itemsInRow: 4
                    }
                })
            });
            projectsContainer.render();


            // rabbits
            var controllerParallax = new ScrollMagic.Controller();

            function scrollParallax(el, speed) {
                var duration = (speed + $(el).height()) + $(window).height();

                var tween = new TimelineMax()
                        .add([
                            TweenMax.to(el + "_el", 1, {top: speed, ease: Linear.easeNone})
                        ]);

                var scene = new ScrollMagic.Scene({
                    triggerHook: 1,
                    triggerElement: el,
                    duration: duration
                })
                        .setTween(tween)
                        .addTo(controllerParallax);
            }

            scrollParallax('.rabbit-1', 700);
            scrollParallax('.rabbit-2', 150);
            scrollParallax('.rabbit-3', 150);
            scrollParallax('.rabbit-4', 700);

        });
    </script>
</head>


<body>
<div class="bg-block">
    <div class="wrap">
        <div class="col-12">

            <div class="bg-block_cont">
                <div class="tree-1"></div>
                <div class="tree-2"></div>

                <div class="rabbit-1">
                    <div class="rabbit-1_el" style="top: 218px;"></div>
                </div>
                <div class="rabbit-2">
                    <div class="rabbit-2_el" style="top: 21px;"></div>
                </div>
                <div class="rabbit-3">
                    <div class="rabbit-3_el" style="top: 0px;"></div>
                </div>
                <div class="rabbit-4">
                    <div class="rabbit-4_el" style="top: 0px;"></div>
                </div>
            </div>

        </div>
    </div>
</div>


<div class="main">

<div class="header">
    <div class="wrap">
        <div class="col-12">

            <div class="bazar-logo-wrap">
                <span class="bazar-logo"></span>
            </div>

            <div class="bazar-date-wrap">
                <span class="bazar-date"></span>
            </div>


            <div class="header_name">
                Всероссийский
                <br>
                Душевный Bazar
            </div>
            <div class="header_name-pln">
                на Planeta.ru
            </div>

        </div>
    </div>
</div>


<div class="lead">
    <div class="wrap">
        <div class="col-12">

            <div class="lead_block">
                <div class="lead_wrap">
                    <div class="lead_i">
                        <div class="lead_text">
                            <p>
                                В&nbsp;2016 году крупнейшая в&nbsp;России благотворительная ярмарка «Душевный Bazar»
                                расширяет свою географию.
                            </p>

                            <p>
                                Традиционное новогоднее мероприятие состоится в&nbsp;Москве 11 декабря в&nbsp;Гостином
                                дворе, а&nbsp;
                            <nobr>онлайн-ярмарка</nobr>
                            на&nbsp;Planeta.ru стартовала в&nbsp;сентябре и&nbsp;уже сейчас в&nbsp;рамках этого проекта
                            у&nbsp;вас есть возможность приобрести сувениры и&nbsp;подарки родным и&nbsp;близким к&nbsp;Новому
                            Году, познакомиться с&nbsp;благотворительными проектами, которые реализуются по&nbsp;всей
                            стране&nbsp;— от&nbsp;Калининграда до&nbsp;Владивостока, и&nbsp;внести свой вклад в&nbsp;решение
                            той или иной социальной проблемы.
                            </p>
                        </div>
                    </div>


                    <div class="lead_i">
                        <div class="lead_video">
                            <div class="embed-video">
                                <iframe class="video-iframe" width="560" height="315" data-video-src="https://www.youtube.com/embed/dcXInubNjRs?autoplay=1&amp;showinfo=0"
                                        frameborder="0" allowfullscreen="" src="https://www.youtube.com/embed/dcXInubNjRs?autoplay=0&amp;showinfo=0"></iframe>
                                <div class="video-cover" style="background-image: url(//${hf:getStaticBaseUrl("")}/images/promo/bazar-2016/video-cover.jpg);"></div>
                                <div class="video-play"><span class="video-play_ico"></span>
                                </div>
                            </div>
                        </div>

                        <script>
                            $('.video-play').on('click', function () {
                                $(this).hide();
                                $('.video-cover').hide();
                                var iframe = $('.video-iframe');
                                iframe.attr('src', iframe.attr('data-video-src'));
                            });
                        </script>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>


<div class="to-end">
    <div class="wrap">
        <div class="col-12">

                <div class="page-head">
                    <div class="page-head_block">
                        <div class="page-head_title">
                            Ярмарка завершена
                        </div>
                    </div>
                </div>
        </div>
    </div>
</div>


<div class="bazar-projects">
    <div class="wrap">
        <div class="col-12">

            <div class="bazar-projects_head page-head">
                <div class="page-head_block">
                    <div class="page-head_title">
                        Проекты «Душевного BAZAR’а» <span class="bazar-projects_head-count">собрали <nobr>
                        <fmt:formatNumber type="number" value="${totalAmount}"/><span class="b-rub">Р</span>
                    </nobr></span>
                    </div>
                </div>
            </div>


            <div class="bazar-projects_projects">
                <div class="js-project-container"></div>
            </div>
        </div>
    </div>

</div>

</div>

${newsBlock}


${partnersBlock}


<div class="footer">
    <div class="wrap">
        <div class="col-12">

            <div class="footer_sharing">
                <div class="footer_sharing-head">
                    поделись с друзьями
                </div>
                <div class="footer_sharing-cont js-share-container">
                </div>
            </div>


            <div class="footer_develop">
                <div class="footer_develop-head">
                    Разработано
                </div>
                <div class="footer_develop-logo">
                    <a href="https://planeta.ru" class="logo"></a>
                </div>
            </div>

        </div>
    </div>
</div>

${modalsBlock}

<script>
    $(document).ready(function () {
        var uri = window.location.protocol + '//' + window.location.host + '${requestScope['javax.servlet.forward.request_uri']}';
        var sharesRedraw = function () {
            $('.js-share-container').empty();
            $('.js-share-container').share({
                className: 'sharing-popup-social donate-sharing sharing-mini',
                counterEnabled: false,
                hidden: false,
                parseMetaTags: true,
                url: uri
            });
        };

        $(window).bind('resize', sharesRedraw);
        sharesRedraw();
    })
</script>


</body>
</html>

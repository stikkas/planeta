<%@ include file="/WEB-INF/jsp/includes/generated/doctype-header.jsp" %>
<head>
    <%@include file="/WEB-INF/jsp/includes/generated/stat-counters-async.jsp"%>
    <%@include file="head.jsp"%>
    <title>Руководство по продвижению на этапе 0-25%</title>
</head>
<body class="project-promotion-page">
<%@ include file="/WEB-INF/jsp/includes/generated/irma/header.jsp" %>
<%@ include file="/WEB-INF/jsp/includes/generated/irma/main-nav.jsp" %>


<div id="global-container">
    <div id="main-container" class="wrap-container">
        <div id="center-container">


            <div class="project-promotion_head">Руководство по продвижению на этапе 0-25%</div>


            <div class="project-promotion_top">
                <div class="wrap">
                    <div class="col-12">

                        <div class="project-promotion_top_cont">
                            <div class="project-promotion_top_ico s-promotion-top-fly"></div>
                            <div class="project-promotion_top_text">
                                Самый показательный период. Собранные 25% от суммы проекта в первые две недели его жизни говорят, что вы на правильном пути и ваши шансы на успех максимальны. Если пока не удалось достигнуть первой цели или рост замедлился, пересмотрите тактику продвижения проекта или скорее начните действовать. Еще не поздно. Все в ваших руках!
                            </div>
                        </div>

                    </div>
                </div>
            </div>


            <div class="project-promotion_cont">
                <div class="wrap">
                    <div class="col-12">

                        <div class="project-promotion_list">
                            <div class="project-promotion_list-i">
                                <div class="project-promotion_list-ico s-promotion-list-vk"></div>
                                <div class="project-promotion_list-head">
                                    Личные интернет-ресурсы — это ваш персональный сайт, паблики и личные страницы в
                                    социальных сетях, а также собственные блоги. Вы также можете создавать личные
                                    страницы специально под проект, используя все перечисленные выше площадки.
                                </div>
                                <div class="project-promotion_list-text">
                                    <span class="step-hl">Шаг 1:</span> Расскажите о том, <a href="http://vk.com/slotband?w=wall-9812_68782%2Fall">как работает
                                    народное финансирование</a>, сошлитесь на примеры успешной практики
                                    краудфандинга. Не забывайте рассказывать об <a href="https://planeta.ru/stories/">успешных
                                    проектах на
                                    Планете</a>.
                                    <br>
                                    <br>
                                    <span class="step-hl">Шаг 2:</span> Напишите пост о проекте на всех доступных
                                    ресурсах, <a href="https://www.facebook.com/InnaZhelannaya.group/posts/554520844632130">описав цель проекта</a>. <a href="https://vk.com/12colours?w=wall-42531786_3275">Пригласите всех к участию
                                    в проекте</a>, не забыв разместить привлекающую внимание картинку и ссылку на
                                    проект.
                                    <br>
                                    <br>
                                    <span class="step-hl">Шаг 3:</span> Инициируйте обсуждение проекта, <a href="https://planeta.ru/campaigns/6751/comments">следите
                                    за комментариями</a> к своим постам, <a href="https://planeta.ru/campaigns/4128/comments">активно отвечайте на
                                    вопросы</a>, <a href="https://planeta.ru/lavkalavka/blog/118304">благодарите за положительные отзывы</a>, работайте с
                                    негативом, объясняя все нюансы своего проекта и <a href="https://vk.com/purpurmusic?w=wall-2408591_7831%2Fall">системы краудфандинга</a>.
                                    <br>
                                    <br>
                                    <span class="step-hl">Шаг 4:</span> Проведите подготовительную работу, создав
                                    <a href="http://prntscr.com/3c25bm">аватарки</a> специально под проект, разместив ссылку на проект на своих
                                    личных ресурсах: <a href="http://prntscr.com/3c25mx">личный сайт</a>, <a href="https://vk.com/atlantida_project?w=wall-2105251_4969%2Fall">ВКонтакте</a>,
                                    <a href="http://prntscr.com/3c25tx">баннер</a> или <a href="http://prntscr.com/3c2656">виджет</a> на личном сайте, а также создав
                                    специальные <a href="https://vk.com/event50723533">сообщества под проект</a>.
                                    <br>
                                    <br>
                                    <span class="step-hl">Шаг 5:</span> Попросите ваших читателей сделать репост и
                                    оставить ссылку на публикацию о проекте (либо на сам проект) на всех доступных
                                    ресурсах.
                                </div>
                            </div>

                            <div class="project-promotion_list-i">
                                <div class="project-promotion_list-ico s-promotion-list-msg"></div>
                                <div class="project-promotion_list-head">
                                    Рассылки — это информация, которую вы распространяете по друзьям, коллегам,
                                    родственникам и партнерам, используя личные сообщения в соцсетях, e-mail письма,
                                    телефонные звонки и sms-сообщения.
                                </div>
                                <div class="project-promotion_list-text">
                                    <span class="step-hl">Шаг 1:</span> <a href="https://s2.planeta.ru/f/f8/primer+rassilki+lavka.docx">Сделайте рассылку по своим
                                    друзьям/коллегам/партнерам</a>, описав идею проекта, дав информацию о его
                                    старте, о том,
                                    на что пойдет собранная сумма и, рассказав о первых результатах. Если у человека нет
                                    возможности поддержать вас материально, <a href="https://s2.planeta.ru/f/f6/primer+lichnogo+pisma.docx">попросите распространить
                                    информацию о
                                    проекте</a> и ссылку на него, а также разместить <a href="https://s2.planeta.ru/f/f9/primer+bannera+na+storonnem+resyrse2.png">виджет проекта</a>
                                    на всех доступных
                                    ресурсах.
                                    <div class="footnote">
                                        При рассылке писем обязательно делайте каждое из них максимально
                                        персонализированным, обращаясь к адресату по имени
                                    </div>
                                    <br>
                                    <span class="step-hl">Шаг 2:</span> Публикуйте новости в проекте не реже одного раза
                                    в неделю: <a href="https://planeta.ru/happypeople/blog/127017">рассказывайте о добавленных вознаграждениях</a>, или <a href="http://instagram.com/p/qtujDACm8X/?modal=true">самых
                                    популярных вознаграждениях</a>, просите
                                    расшарить информацию о проекте и его <a href="https://planeta.ru/torbanakruche/blog/119995">успехах</a>.
                                </div>
                            </div>

                            <div class="project-promotion_list-i">
                                <div class="project-promotion_list-ico s-promotion-list-events"></div>
                                <div class="project-promotion_list-head">
                                    Мероприятия — это оффлайновые встречи (концерты, выставки, мастер-классы,
                                    выступления, дружеские вечеринки), на которых вы можете рассказать о своем проекте.
                                    Не ограничивайтесь интернетом, рассказывайте о своем проекте на разных площадках!
                                </div>
                                <div class="project-promotion_list-text">
                                    <span class="step-hl">Шаг 1:</span> Составьте список мероприятий на весь срок
                                    действия проекта, на которых вы сможете рассказать о нем. Как относящихся к проекту,
                                    так и не имеющих к нему отношения
                                    <br>
                                    <br>
                                    <span class="step-hl">Шаг 2:</span> Расскажите о запуске проекта и опишите его,
                                    открыто отвечайте на все вопросы, расскажите о ходе работы и достигнутых
                                    результатах. Используйте проекторы и мониторы для трансляции видеообращения,
                                    повесьте в доступных местах плакаты, подготовьте специальные <a href="http://prntscr.com/3c26n0">листовки</a>/<a href="http://prntscr.com/3c26ww">флайеры</a>
                                    с
                                    описанием проекта и распространяйте их на любых встречах и мероприятиях.
                                </div>
                            </div>

                            <div class="project-promotion_list-i">
                                <div class="project-promotion_list-ico s-promotion-list-media"></div>
                                <div class="project-promotion_list-head">
                                    Сообщества и СМИ — это тематические блоги, паблики, сообщества в Facebook,
                                    ВКонтакте, Живом Журнале, Твиттере, а также близкие вашему проекту по тематике
                                    средства массовой информации.
                                </div>
                                <div class="project-promotion_list-text">
                                    <span class="step-hl">Шаг 1:</span> Составьте список сообществ и блогов: с большой
                                    (массовой) аудиторией (от 20 000 подписчиков); с качественной аудиторией
                                    (максимально близкой по интересам, месту проживания, возрасту и полу).
                                    <br>
                                    <br>
                                    <span class="step-hl">Шаг 2:</span> Разошлите запрос на размещение информации о
                                    старте проекта в соцсетях и в тематических блогах и форумах. Попросите сделать
                                    <a href="http://instagram.com/p/p_GbwyNqEy/?modal=true">репост публикаций о проекте в блогах и сообществах</a>.
                                    <br>
                                    <br>
                                    <span class="step-hl">Шаг 3:</span> Разошлите в ключевые СМИ <a href="https://s2.planeta.ru/f/fa/%d1%80%d0%b5%d0%bb%d0%b8%d0%b7_%d1%83%d0%ba%d1%83%d0%bb%d0%b5%d0%bb%d0%b5.docx">пресс-релиз</a> с основной
                                    информацией о проекте.
                                    <br>
                                    <br>
                                    <span class="step-hl">Шаг 4:</span> Рассказывайте об <a href="http://www.interviewrussia.ru/movie/leto-kak-vzyat-kino-v-svoi-ruki">опубликованных
                                    материалах</a>/<a href="https://planeta.ru/msegal/blog/122185">проведенных эфирах</a>/<a href="https://tv.planeta.ru/broadcast/225540">радио</a>- и
                                    <a href="https://planeta.ru/206635/blog/123445">телевизионных выпусках</a>, посвящённых вашему проекту в новостях
                                    проекта на Планете и на своих личных ресурсах, а также в
                                    последующих пресс-релизах.
                                    <br>
                                    <br>
                                    <span class="step-hl">Шаг 5:</span> Напишите менеджеру вашего проекта, что хотите попасть в социальные сети или рассылку Planeta.ru.
                                    Наши редакторы самостоятельно подбирают крауд-проекты для анонсирования в социальных сетях и рассылках, критерии отбора можно посмотреть
                                    <a href="https://planeta.ru/faq/article/12!paragraph115">здесь</a>.
                                </div>
                            </div>

                            <div class="project-promotion_list-i">
                                <div class="project-promotion_list-ico s-promotion-list-person"></div>
                                <div class="project-promotion_list-head">
                                    Лидеры мнений — это известные и уважаемые личности со сложившейся большой аудиторией
                                    (в том числе, и сетевой): общественные деятели, блогеры, журналисты, акулы бизнеса.
                                </div>
                                <div class="project-promotion_list-text">
                                    <span class="step-hl">Шаг 1:</span> Попросите о поддержке, предварительно определив
                                    ее формат: это может быть <a href="http://dolboeb.livejournal.com/2583370.html">пост</a>, <a href="https://www.facebook.com/permalink.php?story_fbid=621242517909477&amp;id=100000712037223">анонс в соц.сетях</a>,
                                    <a href="http://www.youtube.com/watch?v=Vw1SSJU4y5Y">видеокомментарий</a>, <a href="http://odnovremenno.com/">баннер на сайте</a>, <a href="https://www.facebook.com/misha.kozyrev/posts/633592420013133">репост</a>.
                                    <br>
                                    <br>
                                    <span class="step-hl">Шаг 2:</span> Предложите возможный бартер, связанный с
                                    проектом: бонусы из проекта, <a href="https://planeta.ru/colta/blog/125899">пригласительные на мероприятия проекта</a>,
                                    <a href="https://planeta.ru/128318/blog/118065">совместная акция</a> и благодарности с рассылкой по своей аудитории.
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>


        </div>
    </div>
</div>

<%@ include file="/WEB-INF/jsp/includes/generated/irma/footer.jsp" %>
</body>
</html>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@include file="/WEB-INF/jsp/includes/generated/doctype-header.jsp" %>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Данный тип договора не поддерживается</title>
    <meta name="title" content="Данный тип договора не поддерживается">
    <link type="text/css" rel="stylesheet" href="//${hf:getStaticBaseUrl("")}/css-generated/common.css"/>
</head>
<body>
Данный тип договора не поддерживается. Обратитесь к администратору.
</body>

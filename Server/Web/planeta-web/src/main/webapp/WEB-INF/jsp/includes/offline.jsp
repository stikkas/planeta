<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<c:choose>
    <c:when test="${properties['offline.enabled']}">
        <script>
            try {
                if('serviceWorker' in navigator) {
                    navigator.serviceWorker.register('/offline.js', { scope: '/' })
                            .then(function(registration) {
                                console.log('Service Worker Registered');
                            });
                    navigator.serviceWorker.ready.then(function(registration) {
                        console.log('Service Worker Ready');
                    });
                }
            } catch (e) {
                console.log(e);
            }
        </script>
    </c:when>
    <c:otherwise>
        <script>
            try {
                if('serviceWorker' in navigator) {
                    navigator.serviceWorker.getRegistrations().then(function (registrations) {
                        if (registrations) {
                            if (registrations.length) {
                                registrations.forEach(function (reg) {
                                    reg.unregister();
                                });
                            }
                        }
                    });
                }
            } catch (e) {
                console.log(e);
            }
        </script>
    </c:otherwise>
</c:choose>

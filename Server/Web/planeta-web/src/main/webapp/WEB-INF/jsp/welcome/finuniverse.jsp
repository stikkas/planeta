<%@ include file="/WEB-INF/jsp/includes/generated/doctype-header.jsp" %>
<%@taglib prefix="p" uri="http://planeta.ru/tags" %>
<c:set var="baseUrl" value="${hf:getStaticBaseUrl('')}"/>
<html>
<head>
    <title>Университетский краудфaндинг №1 в России</title>
    <meta property="og:title" content="Университетский краудфaндинг №1 в России"/>
    <meta name="description" content="Более 50 университетов США и Европы уже успешно используют краудфандинг для реализации своих проектов.
Финансовый университет стал первым в России."/>
    <meta property="og:description" content="Более 50 университетов США и Европы уже успешно используют краудфандинг для реализации своих проектов.
Финансовый университет стал первым в России."/>
    <meta property="og:image" content="//${baseUrl}/images/fu/ogimage.png">

    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">

    <%@include file="/WEB-INF/jsp/includes/generated/common-favicon.jsp" %>
    <link type="text/css" rel="stylesheet" href="//${baseUrl}/css-generated/fu.css"/>
    <script src="//${baseUrl}/js/lib/modernizr.js"></script>
    <script src="//${baseUrl}/js/lib/jquery-1.8.3.js"></script>
    <%@ include file="/WEB-INF/jsp/includes/generated/stat-counters-head.jsp" %>
    <script src="//${baseUrl}/js/lib/bootstrap/bootstrap-3.2.0.min.js"></script>
    <script src="//${baseUrl}/js/lib/owl.carousel.2.min.js"></script>
    <script src="//${baseUrl}/js/lib/jquery.singlePageNav.js"></script>
    <script src="//${baseUrl}/js/lib/jquery.sticky-kit.min.js"></script>

    <script src="//${baseUrl}/js/lib/jquery.isEventOut.js" type="text/javascript"></script>
    <script src="//${baseUrl}/js/lib/jquery.dropPopup.js" type="text/javascript"></script>
    <script src="//${baseUrl}/js/lib/underscore-1.4.4.min.js" type="text/javascript"></script>
    <script src="//${baseUrl}/js/utils/plugins/share-widget.js" type="text/javascript"></script>


    <script>
        function uiSelect() {
            $('selectCampaignById.pln-selectCampaignById').each(function() {
                var select = $('<div class="pln-selectCampaignById dropdown"/>');
                var selectBtn = $('<div class="selectCampaignById-btn"/>');
                var selectBtnIcon = $('<i class="icon-selectCampaignById"/>');
                var selectCont = $('<span class="selectCampaignById-cont"/>');
                var selectList = $('<ul class="dropdown-menu"/>');

                $(this).find('option').each(function() {
                    var activeClass = '';
                    if ($(this).is(':selected')) activeClass = ' active';
                    var item = $('<li class="item' + activeClass + '"><a href="#">' + $(this).html() + '</a></li>');
                    item.appendTo(selectList);

                    item.find('> a').click(function(e) {
                        e.preventDefault();
                        selectCont.html($(this).html());
                        select.removeClass('open');
                    });
                });

                selectBtnIcon.appendTo(selectBtn);
                selectCont.html($(this).find('option:eq(0)').html());

                selectBtn.appendTo(select);
                selectCont.appendTo(select);
                selectList.appendTo(select);

                select.addClass($(this).attr('class'));

                $(this).replaceWith(select);

                select.dropPopup({
                    trigger: '> .selectCampaignById-cont',
                    popup: '.dropdown-menu',
                    activeClass: 'open'
                });

                setTimeout(function () {
                    select.addClass('pln-select__inited');
                }, 500);
            });

        }

        $(function() {
            uiSelect();
        });
        // /selectCampaignById dropdown



        // default dropdown
        $(function() {
            $('.pln-dropdown').dropPopup({
                trigger: '.pln-d-switch',
                popup: '> .pln-d-popup',
                closeTrigger: '.pln-d-close',
                activeClass: 'open',
                bottomUp: true,
                open: function(el) {
                    el.elem.find('.modal-scroll-content').scrollbar('repaint');
                    el.elem.find('.scroll-content').scrollbar('repaint');
                }
            });
        });
        // /default dropdown



        // rating star change
        $.fn.ratingStarChange = function() {
            this.each(function() {
                var $ratingBlock = $(this);
                var $ratingWrap = $('.r-star-wrap', $ratingBlock);
                var $ratingItems = $('.r-star-state b', $ratingBlock);
                var rating;

                $ratingWrap.bind('mouseenter.starRating', function() {
                    rating = $ratingBlock.data('rating');
                    $ratingWrap.unbind('mouseenter.starRating');
                });
                $ratingWrap.mouseleave(function() {
                    setRatingClass(rating);
                });

                $ratingItems.mouseover(function() {
                    var index = $(this).index();
                    setRatingClass(index + 1);
                });

                $ratingWrap.click(function() {
                    rating = getRatingClass();
                });

                function getRatingClass() {
                    var ratingClass = $ratingWrap.attr('class');
                    ratingClass = ratingClass.match(/r-star-val-(\d)/);
                    return ratingClass[1];
                }

                function setRatingClass(num) {
                    var ratingClass = $ratingWrap.attr('class');
                    ratingClass = ratingClass.replace(/(r-star-val-)\d/, '$1'+num);
                    $ratingWrap.attr('class', ratingClass);
                }
            });
        };
        $(function() {
            $('.r-star-changed').ratingStarChange();
        });
        // /rating star change


        /* Fixed button for create wizard pages */
        var fixedBtn = {
            init: function(selector, options) {
                var blockWrap = $(selector);
                if (!blockWrap.length) return;
                var blockOuterWidth = blockWrap.outerWidth();
                var blockOuterHeight = blockWrap.outerHeight();
                blockWrap.height(blockOuterHeight);

                if ( !!options && !!options.inner ) {
                    $(options.inner).width(blockOuterWidth);
                }

                $(window).bind('scroll.fixedBtn resize.fixedBtn', function () {
                    fixedScroll();
                });
                fixedScroll();

                function fixedScroll() {
                    var screenHeight = $(window).height();
                    var winTop = $(window).scrollTop();
                    var winBottom = winTop + screenHeight;
                    var positionTop = blockWrap.offset().top;
                    var position = positionTop + blockOuterHeight;

                    if ( !!options && !!options.type && options.type == 'top' ) {
                        if (winTop > positionTop) {
                            blockWrap.addClass('fixed');
                        } else {
                            blockWrap.removeClass('fixed');
                        }
                    } else {
                        if (winBottom > position) {
                            blockWrap.removeClass('fixed');
                        } else {
                            blockWrap.addClass('fixed');
                        }
                    }
                }
            }
        };
        /* Fixed button for create wizard pages */

        function owlIndexNormailze(event) {
            var direction = event.property.value - event.item.index;

            var index;
            if ( direction == 1 ) {
                index = event.item.index + 1 - Math.ceil(event.item.count / 2);
            } else {
                index = event.item.index - 1 - Math.ceil(event.item.count / 2);
                if ( index < 0 ) index = event.item.count - 1;
            }

            return index;
        }
    </script>
</head>
<body>
<div class="header">
    <div class="wrap">
        <div class="wrap-row">
            <div class="col-12">
                <a href="http://www.fa.ru/Pages/home.aspx" target="_blank" rel="nofollow noopener" class="header_logo"></a>
                <a href="https://planeta.ru/" class="logo"></a>
            </div>
        </div>
    </div>
</div>


<div class="lead">

    <div class="back-video-wrap">
        <video class="back-video" id="back-video" autoplay="" loop="" onended="this.play()">
            <source src="//${baseUrl}/images/fu/back.mp4" type="video/mp4">
            <source src="//${baseUrl}/images/fu/back.webm" type="video/webm">
        </video>
    </div>

    <div class="lead_wrap">
        <div class="wrap">
            <div class="wrap-row">
                <div class="col-12">
                    <div class="lead_title">Университетский краудфaндинг №1 в России</div>
                    <div class="lead_text">Более 50 университетов США и Европы уже успешно
                        используют краудфандинг для реализации своих проектов.
                    </div>
                    <div class="lead_text-bold">Финансовый университет стал первым в России.</div>
                    <!--<div class="lead_btn">
                        <span class="fu-btn fu-btn-default">Поддержать проекты</span>
                    </div>-->
                    <div class="lead_create">Вы из Финансового университета?<br>
                        <div class="lead_btn">
                            <div class="fu-btn fu-btn-primary">
                                <a href="/funding-rules">Создайте проект!</a></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="section-line">
        <div class="section-line_cnt"></div>
    </div>

</div>


<div class="menu">
    <div class="menu_wrap">

        <div class="wrap">
            <div class="wrap-row">
                <div class="col-12">

                    <div class="menu_list">
                        <a href="#fu-projects" class="menu_i">
                            <span class="menu_title">Проекты университета</span>
                            <span class="menu_text">Поддерживайте идеи студентов&nbsp;и&nbsp;выпускников</span>
                        </a>

                        <a href="#fu-what" class="menu_i">
                            <span class="menu_title">О краудфандинге</span>
                            <span class="menu_text">Как работает народное финансирование</span>
                        </a>

                        <a href="#who-can" class="menu_i">
                            <span class="menu_title">Условия</span>
                            <span class="menu_text">Кто может<br>создавать проекты</span>
                        </a>

                        <a href="#about" class="menu_i">
                            <span class="menu_title">О нас</span>
                            <span class="menu_text">Подробности сотрудничества «Планеты» и университета</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(function () {
        $('.menu_list').singlePageNav({
            offset: 115,
            currentClass: 'active',
            updateHash: true
        });
    });

    $(function () {
        $(".menu_wrap").stick_in_parent({
            parent: 'body'
        });
    });
</script>


<div class="fu-projects" id="fu-projects">
    <div class="wrap">
        <div class="wrap-row">
            <div class="col-12">


                <div class="project-card-list">
                <c:forEach var="c" items="${campaigns}" varStatus="i">

                    <div class="project-card-item" id="projectCard-${i.current}">
                        <a class="project-card-link" href="/campaigns/${c.url}">

                            <span class="project-card-cover-wrap">
                                <img class="project-card-cover" src="${hf:getProxyThumbnailUrl(c.image, "ORIGINAL", "PHOTO", 270, 164, true, false)}" width="220" height="134">
                            </span>

                            <span class="project-card-text">
                                <span class="project-card-name hyphenate">
                                        ${c.name}
                                </span>
                                <span class="project-card-descr">
                                        ${c.shortDesc}
                                </span>
                            </span>

                            <span class="project-card-footer">
                                <span class="project-card-progress">
                                    <span class="pcp-bar" style="width:${c.progressProc}%;">

                                    </span>
                                 </span>


                                <span class="project-card-info">
                                    <span class="project-card-info-i">
                                        <span class="pci-label">Собрано ${c.progressProc}%</span>
                                        <span class="pci-value"><fmt:formatNumber value="${c.collected}"
                                                                                  type="number"/> <span class="b-rub">Р</span></span>
                                    </span>

                                    <span class="project-card-info-i">
                                        <span class="pci-label">Цель</span>
                                        <span class="pci-value">
                                            <fmt:formatNumber value="${c.target}"
                                                              type="number"/> <span class="b-rub">Р</span>
                                        </span>
                                    </span>

                                </span>

                                <span class="project-card-link-to-project">
                                    <span class="btn btn-primary btn-lg btn-block">Поддержать</span>
                                </span>
                            </span>
                        </a>
                    </div>

                </c:forEach>

                    <c:if test="${showMore}">

                        <div class="project-card-item new-card-item">
                            <a href="/search/projects?query=&categories=UNIVERSITY">
                            <span class="new-card_i">
                                <span class="all-project">Все проекты →</span>
                            </span>
                            </a>

                            <div class="new-card_idea">
                                <span class="new-card_ask">Есть идея проекта?</span>
                                <a href="/funding-rules" class="fu-btn fu-btn-default">Создать проект</a>
                            </div>
                        </div>
                    </c:if>

                </div>
            </div>
        </div>
    </div>
</div>

<div class="fu-what" id="fu-what">
    <div class="wrap">
        <div class="wrap-row">
            <div class="col-12">
                <div class="what_list wrap-row">

                    <div class="what_title">Что такое краудфандинг?</div>

                    <div class="what_i col-4">
                        <div class="what_i_icon-1"></div>
                        <div class="what_i_title">У вас есть идея?</div>
                        <div class="what_i_text">Хотите провести мероприятие,
                            установить контейнеры для раздельного сбора мусора
                            или купить форму для хоккейной команды? Создайте
                            проект по сбору средств!
                        </div>
                    </div>

                    <div class="what_i col-4">
                        <div class="what_i_icon-2"></div>
                        <div class="what_i_title">Расскажите о ней</div>
                        <div class="what_i_text">Обратитесь к друзьям и близким,
                            опишите свою идею и предложите интересные бонусы за поддержку.
                            Именно эти люди в первую очередь станут вашими единомышленниками.
                        </div>
                    </div>

                    <div class="what_i col-4">
                        <div class="what_i_icon-3"></div>
                        <div class="what_i_title">Получите средства</div>
                        <div class="what_i_text">После завершения сбора денег вы
                            сможете воплотить свою идею в жизнь. И не забудьте раздать
                            обещанные вознаграждения тем, кто поддержал ваш проект!
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="section-line">
        <div class="section-line_cnt"></div>
    </div>

</div>

<div class="who-can" id="who-can">
    <div class="wrap">
        <div class="wrap-row">
            <div class="col-12">
                <div class="who-can-wrap">
                    <div class="who-can_title">Кто может создавать проекты?</div>

                    <div class="who-can_list wrap-row">
                        <div class="who-can-border"></div>

                        <div class="who-can_i col-4">
                            <div class="who-can_i_icon">
                                <div class="who-can_i_circle">
                                    <span class="who-can_i_people"></span>
                                </div>
                            </div>
                            <div class="who-can_i_text">Студенты и преподаватели<br> Финансового университета</div>
                        </div>

                        <div class="who-can_i col-4">
                            <div class="who-can_i_icon">
                                <div class="who-can_i_circle">
                                    <span class="who-can_i_group"></span>
                                </div>
                            </div>
                            <div class="who-can_i_text">Студенческие объединения<br>и творческие коллективы</div>
                        </div>

                        <div class="who-can_i col-4">
                            <div class="who-can_i_icon">
                                <div class="who-can_i_circle">
                                    <span class="who-can_i_tie"></span>
                                </div>
                            </div>
                            <div class="who-can_i_text">Выпускники, небезучастные<br>к судьбе Alma Mater
                            </div>
                        </div>
                    </div>
                    <div  class="who-can-btn">
                        <a href="/funding-rules" class="fu-btn fu-btn-default">Создать проект</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="about" id="about">
    <div class="wrap">
        <div class="wrap-row">
            <div class="col-12">
                <div class="about_title">О нас</div>
                <div class="about-list wrap-row">
                    <div class="about_i col-4">
                        <div class="about_i_text">Planeta.ru, краудфандинговая платформа №1
                            в России, и Финансовый университет при Правительстве Российской
                            Федерации объединяют свои усилия для воплощения в жизнь студенческих
                            и преподавательских инициатив, связанных с жизнью вуза.
                        </div>
                    </div>
                    <div class="about_i col-4">
                        <div class="about_i_text">Университетский краудфандинг – это возможность для студентов сделать
                            учебный процесс удобней и интересней, а также прекрасный шанс
                            научиться реализовать собственные идеи при помощи единомышленников.
                            Выпускникам эта программа позволит принимать активное участие в
                            жизни
                        </div>
                    </div>
                    <div class="about_i col-4">
                        <div class="about_i_text">alma mater, а преподаватели смогут развивать научную деятельность без
                            многолетнего
                            ожидания сторонних грантов и инвестиций. Университетский краудфандинг –
                            это шанс сделать образование таким, каким мы хотим его видеть, уже сейчас.
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="cite">
    <div class="wrap">
        <div class="wrap-row">
            <div class="col-12">
                <div class="cite_title">Обращение организаторов</div>
                <div class="cite-list_wrap">

                    <div class="cite-list">

                        <div class="cite_i cf">
                            <div class="cite_photo">
                                <div class="cite_photo_frame cf">
                                    <img src="//${baseUrl}/images/fu/rector.jpg"/>
                                </div>
                            </div>
                            <div class="cite-cnt">
                                <div class="cite_text">«Во всем мире краудфандинг уже зарекомендовал себя
                                    как эффективный инструмент для воплощения в жизнь различных интересных идей и
                                    социально значимых проектов. И Финансовый университет, один из ведущих вузов
                                    России с почти вековой историей, не может оставаться в стороне от этого
                                    масштабного общественного движения и станет одним из первых, кто в нашей
                                    стране будет широко применять народное финансирование для реализации инициатив
                                    в сфере науки и образования, тем самым задавая вектор развития для других
                                    вузов. Я убежден: в рамках совместной программы с Planeta.ru мы на деле сумеем
                                    доказать, что вместе мы - сила, и сделаем отечественное образование лучше и
                                    эффективнее».
                                </div>
                                <div class="cite_name">Михаил Абдурахманович Эскиндаров</div>
                                <div class="cite_position">Ректор Финансового университета при Правительстве РФ</div>
                            </div>
                        </div>

                        <div class="cite_i cf">
                            <div class="cite_photo">
                                <div class="cite_photo_frame cf">
                                    <img src="//${baseUrl}/images/fu/fedya.jpg"/>
                                </div>
                            </div>
                            <div class="cite-cnt">
                                <div class="cite_text">«Когда мы только запускали краудфандинговую платформу, нашей
                                    идеей было изменение мира к лучшему – не просто воплощение в жизнь каких-то
                                    проектов, а создание настоящей мечты. Именно поэтому нас так привлекает
                                    сотрудничество в области образования: во-первых, от его качества зависит то,
                                    какой мы увидим нашу страну в дальнейшем. А во-вторых, именно в университетской
                                    среде можно найти такое огромное количество идей, ждущих реализации, какое
                                    не встречается во «взрослом» мире, где многие боятся препятствий на пути к своей
                                    мечте. Для нас университетский краудфандинг – это наш вклад в жизнь страны
                                    и общества».
                                </div>
                                <div class="cite_name">Федор Вадимович Мурачковский</div>
                                <div class="cite_position">Генеральный директор и сооснователь Planeta.ru

                                </div>
                            </div>
                        </div>

                    </div>


                </div>


                <script>
                    $(function () {
                        $('.cite-list').addClass('owl-carousel').owlCarousel({
                            autoplay: false,
                            loop: true,
                            items: 1,
                            slideBy: 1,
                            nav: true,
                            dots: false,
                            margin: 20,
                            mouseDrag: false,
                            smartSpeed: 600,
                            onInitialized: owlInitialized
                        });

                        function owlInitialized(event) {
                            var $prev = $('.owl-prev', event.target);
                            var $next = $('.owl-next', event.target);

                            $prev.html('<span class="s-icon s-icon-arrow-thin-left">');
                            $next.html('<span class="s-icon s-icon-arrow-thin-right">');
                        }
                    });
                </script>


            </div>
        </div>
    </div>
</div>

<div class="footer">
    <div class="wrap">
        <div class="wrap-row">
            <div class="col-12">
                <div class="footer_list cf">
                    <div class="footer_social">

                        <script>
                            $(document).ready(function () {
                                var uri = window.location.protocol + '//' + window.location.host + '${requestScope['javax.servlet.forward.request_uri']}';
                                var sharesRedraw = function () {
                                    $('.js-share-container').empty();
                                    $('.js-share-container').share({
                                        //className: 'sharing-popup-social donate-sharing sharing-mini',
                                        className: 'sharing-popup-social sharing-mini',
                                        counterEnabled: false,
                                        hidden: false,
                                        parseMetaTags: true,
                                        url: uri
                                    });
                                };

                                $(window).bind('resize', sharesRedraw);
                                sharesRedraw();
                            })
                        </script>
                        <span class="footer_share">Поделитесь с друзьями</span>

                        <!--  sharing template  -->
                        <div class="js-share-container">
                        </div>

                    </div>
                    <div class="footer_copyright ">Реализация спецпроекта: <b>planeta.ru</b></div>
                </div>
            </div>
        </div>
    </div>
</div>

</body>
</html>

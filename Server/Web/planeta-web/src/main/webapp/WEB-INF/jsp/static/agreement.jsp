<%@ include file="/WEB-INF/jsp/includes/generated/doctype-header.jsp" %>
<head>
    <c:choose>
        <c:when test="${not empty customMetaTag.title}">
            <title>${customMetaTag.title}</title>
        </c:when>
        <c:otherwise>
            <title>Пользовательское соглашение | Planeta</title>
        </c:otherwise>
    </c:choose>
    <c:if test="${not empty customMetaTag.description}">
        <meta name="description" content="${customMetaTag.description}"/>
    </c:if>
    <c:if test="${not empty customMetaTag.keywords}">
        <meta name="keywords" content="${customMetaTag.keywords}"/>
    </c:if>
    <meta name="viewport" content="width=device-width">
    <%@include file="/WEB-INF/jsp/includes/generated/stat-counters-async.jsp"%>

    <%@include file="/WEB-INF/jsp/includes/common-css.jsp" %>
    <%@include file="/WEB-INF/jsp/includes/generated/common-templates.jsp" %>
    <%@include file="/WEB-INF/jsp/includes/planeta-js.jsp" %>
</head>

<body>
<%@include file="/WEB-INF/jsp/includes/generated/irma/header.jsp" %>
<%@include file="/WEB-INF/jsp/includes/generated/irma/main-nav.jsp" %>
<div id="global-container">
    <div id="main-container" class="wrap-container">
        <div class="pln-content-box">
            <div class="wrap">
                <div class="col-9">
                    <div class="pln-content-box_cont wrap-indent-right-half">
                        <div class="aboute-content">
                            <%@ include file="../includes/generated/agreement-plain.jsp" %>
                        </div>
                    </div>
                </div>
                <div class="col-3">
                    <%@include file="/WEB-INF/jsp/includes/about-menu.jsp" %>
                </div>
            </div>
        </div>
    </div>
</div>
<%@ include file="/WEB-INF/jsp/includes/generated/irma/footer.jsp" %>
</body>
</html>
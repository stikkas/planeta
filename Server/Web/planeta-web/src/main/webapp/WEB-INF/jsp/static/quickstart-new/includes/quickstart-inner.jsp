<div class="faq">
    <div class="wrap">

        <c:choose>
            <c:when test="${pageType == 'movie'}">

                <div class="faq-text">


                    <div class="faq-text_i">
                        <div class="faq-text_i_head">
                            Кто может заводить проекты на Planeta.ru?
                        </div>
                        <div class="faq-text_i_cont">
                            Проект на Planeta.ru может завести:
                            <ul>
                                <li>гражданин любой страны старше 18 лет, имеющий действующий счет в банке;</li>
                                <li>ваша компания, если она соответствует требованиям, установленным в Правилах нашего сервиса.</li>
                            </ul>
                        </div>
                    </div>

                    <div class="faq-text_i">
                        <div class="faq-text_i_head">
                            Сколько стоит сотрудничество с Planeta.ru?
                        </div>
                        <div class="faq-text_i_cont">
                            Нисколько, если речь идет именно о&nbsp;создании проекта, а&nbsp;не&nbsp;о&nbsp;пользовании&nbsp;другими сервисами портала (<nobr>онлайн-трансляции</nobr>, дополнительное продвижение проекта <nobr>и т. д.</nobr>).
                            <br>
                            В&nbsp;случае успешного завершения проекта Planeta.ru взимает только сервисную комиссию.
                        </div>
                    </div>

                    <div class="faq-text_i">
                        <div class="faq-text_i_head">
                            Какой процент взимает Planeta.ru с реализованных проектов?
                        </div>
                        <div class="faq-text_i_cont">
                            Общая комиссия для проектов, собравших сто и&nbsp;более процентов от&nbsp;заявленной суммы, составляет 10%. В&nbsp;случае, если проект собрал от&nbsp;50% до&nbsp;99%, общая комиссия составит 15%. И, разумеется, мы&nbsp;не&nbsp;взимаем свою комиссию с&nbsp;благотворительных проектов.
                        </div>
                    </div>
                </div>

            </c:when>
            <c:when test="${pageType == 'book'}">
                <div class="faq-text">

                    <div class="faq-text_i">
                        <div class="faq-text_i_head">Кто может заводить проекты на&nbsp;«Планете»?</div>
                        <div class="faq-text_i_cont">
                            Проект на «Планете» может завести:
                            <br>
                            <ul>
                                <li>гражданин любой страны старше 18&nbsp;лет, имеющий действующий счет в&nbsp;банке;
                                </li>
                                <li>ваша компания, если она соответствует требованиям, установленным в&nbsp;Правилах
                                    нашего сервиса.
                                </li>
                            </ul>
                        </div>
                    </div>

                    <div class="faq-text_i">
                        <div class="faq-text_i_head">Сколько стоит сотрудничество с&nbsp;«Планетой»?</div>
                        <div class="faq-text_i_cont">
                            Нисколько, если речь идет именно о&nbsp;создании проекта, а&nbsp;не&nbsp;о&nbsp;пользовании
                            другими сервисами «Планеты» (онлайн-трансляции, интернет-магазин и&nbsp;т.д.).
                            <br>
                            <br>
                            В случае успешного завершения проекта «Планета» взимает только сервисную комиссию.
                        </div>
                    </div>

                    <div class="faq-text_i">
                        <div class="faq-text_i_head">Какой процент взимает «Планета» с&nbsp;реализованных проектов?
                        </div>
                        <div class="faq-text_i_cont">
                            Комиссия «Планеты» составляет всего 5%&nbsp;от&nbsp;средств, собранных успешным проектом.
                            Также&nbsp;5%&nbsp;от&nbsp;собранной суммы отходит в&nbsp;пользу платежных агрегаторов.
                            Таким образом, общая комиссия для проектов, собравших сто и&nbsp;более процентов от&nbsp;заявленной
                            суммы, составляет 10%. В&nbsp;случае, если проект собрал от&nbsp;50 до&nbsp;99%, общая
                            комиссия составит 15%. И, разумеется, мы не&nbsp;взимаем свою комиссию с&nbsp;благотворительных
                            проектов.
                        </div>
                    </div>

                </div>
            </c:when>
            <c:otherwise>

                <div class="faq-text">

                    <div class="faq-text_i">
                        <div class="faq-text_i_head">Кто может заводить проекты на «Планете»?</div>
                        <div class="faq-text_i_cont">
                            Проект на «Планете» может завести:
                            <br>
                            <ul>
                                <li>Гражданин любой страны старше 18 лет, имеющий действующий счет в банке;</li>
                                <li>Ваша компания, если она соответствует требованиям, установленным в Правилах нашего
                                    сервиса.
                                </li>
                            </ul>
                        </div>
                    </div>

                    <div class="faq-text_i">
                        <div class="faq-text_i_head">Сколько стоит сотрудничество с «Планетой»?</div>
                        <div class="faq-text_i_cont">
                            Нисколько, если речь идет именно о создан¸ии проекта, а не о пользовании другими сервисами
                            «Планеты» (онлайн-трансляции, интернет-магазин и т.д.).
                            <br>
                            <br>
                            В случае успешного завершения проекта «Планета» взимает только сервисную комиссию.
                        </div>
                    </div>

                    <div class="faq-text_i">
                        <div class="faq-text_i_head">Какой процент взимает «Планета» с реализованных проектов?</div>
                        <div class="faq-text_i_cont">
                            Комиссия «Планеты» составляет всего 5% от средств, собранных успешным проектом. Также 5% от
                            собранной суммы отходит в пользу платежных агрегаторов. Таким образом, общая комиссия для
                            проектов, собравших сто и более процентов от заявленной суммы, составляет 10%. В случае,
                            если проект собрал от 50 до 99%, общая комиссия составит 15%. И, разумеется, мы не взимаем
                            свою комиссию с благотворительных проектов.
                        </div>
                    </div>

                </div>
            </c:otherwise>
        </c:choose>



        <div class="faq-author faq-author__bottom">
            <div class="faq-author_head">На ваши вопросы ответит</div>
            <div class="faq-author_block cf">
                <c:choose>
                    <c:when test="${pageType == 'book'}">
                        <div class="faq-author_ava">
                            <img src="//${hf:getStaticBaseUrl("")}/images/quickstart/klecheva.jpg">
                        </div>
                        <div class="faq-author_cont">
                            <div class="faq-author_name">Ася Клещёва</div>
                            <div class="faq-author_prof">Руководитель проектов</div>

                            <div class="faq-author_project-lbl">Курировала проекты</div>
                            <div class="faq-author_project-val">Евгения Гришковца, Виктора Шендеровича, Линор Горалик, Владимира Яковлева, правозащитного общества «Мемориал»</div>
                        </div>
                    </c:when>
                    <c:when test="${pageType == 'movie'}">
                        <div class="faq-author_ava">
                            <img src="//${hf:getStaticBaseUrl("")}/images/quickstart/klecheva.jpg">
                        </div>
                        <div class="faq-author_cont">
                            <div class="faq-author_name">Ася Клещёва</div>
                            <div class="faq-author_prof">Руководитель проектов</div>

                            <div class="faq-author_project-lbl">Курировала проекты:</div>
                            <div class="faq-author_project-val">телеспектакль «Петрушка» Владимира Мирзоева и Виктора Шендеровича, фильм «Атлантида Русского Севера», международный фестиваль «ДОКер».</div>
                        </div>
                    </c:when>
                    <c:when test="${pageType == 'business'}">
                        <div class="faq-author_ava">
                            <img src="//${hf:getStaticBaseUrl("")}/images/quickstart/german.jpg">
                        </div>
                        <div class="faq-author_cont">
                            <div class="faq-author_name">Ольга Герман</div>
                            <div class="faq-author_prof">Руководитель проектов</div>

                            <div class="faq-author_project-lbl">Курировала проекты</div>
                            <div class="faq-author_project-val">Вячеслава Семенчука, Пекарни «Дашины пирожки», кооператива&nbsp;LavkaLavka</div>
                        </div>
                    </c:when>

                    <c:when test="${pageType == 'music'}">
                        <div class="faq-author_ava">
                            <img src="//${hf:getStaticBaseUrl("")}/images/quickstart/maksimova.jpg">
                        </div>
                        <div class="faq-author_cont">
                            <div class="faq-author_name">Полина Максимова</div>
                            <div class="faq-author_prof">Руководитель проектов</div>

                            <div class="faq-author_project-lbl">Курировала проекты</div>
                            <div class="faq-author_project-val">Групп «Несчастный случай», «Ундервуд». «Сансара», SunSay</div>
                        </div>
                    </c:when>
                </c:choose>

            </div>
        </div>


    </div>
</div>

<div class="question-form">
    <div class="wrap">
        <div class="question-form_head">Остались вопросы?</div>
        <div class="question-form_block page-control">

            <div class="wrap-row">
                <div class="col-8">
                    <div class="fieldset-group">
                        <div class="fieldset pln-dropdown js-question js-error">
                            <textarea class="form-control control-xlg" rows="7" placeholder="Задайте свой вопрос"></textarea>
                            <div class="page-control_popup pln-d-popup">
                                <div class="pln-d-popup-tail"><div></div></div>
                                <div class="d-popup-cont">
                                    Пустой вопрос? Как-это?
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-4">
                    <div class="fieldset-group">
                        <div class="fieldset pln-dropdown js-name js-error">
                            <input class="form-control control-xlg" type="text" placeholder="Ваше имя">
                            <div class="page-control_popup pln-d-popup">
                                <div class="pln-d-popup-tail"><div></div></div>
                                <div class="d-popup-cont">
                                    Введите, пожалуйста, имя
                                </div>
                            </div>
                        </div>
                        <div class="fieldset pln-dropdown js-mail js-error">
                            <input class="form-control control-xlg" type="text" placeholder="Ваш e-mail">
                            <div class="page-control_popup pln-d-popup">
                                <div class="pln-d-popup-tail"><div></div></div>
                                <div class="d-popup-cont">
                                    Вы ввели неверный адрес электронной почты
                                </div>
                            </div>
                        </div>
                        <div class="fieldset pln-dropdown js-send">
                            <input class="btn btn-primary btn-block btn-xlg js-ask-question" type="submit" value="Задать вопрос">
                            <div class="page-control_popup pln-d-popup">
                                <div class="pln-d-popup-tail"><div></div></div>
                                <div class="d-popup-cont">
                                    Ваш вопрос отправлен
                                </div>
                            </div>
                        </div>
                    </div>
                    <script type="text/javascript">
                        $(function() {
                            var pddOptions =  {
                                popup: '> .pln-d-popup',
                                activeClass: 'open'
                            }
                            $('.js-error.pln-dropdown').dropPopup(pddOptions);
                            $('.js-ask-question').click(function() {
                                var flag = true;
                                function validateEmail(email) {
                                    var re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                                    return re.test(email);
                                }
                                if (!validateEmail($('.js-mail input').val())) {
                                     flag = false;
                                    $('.js-mail.pln-dropdown').addClass('open').addClass('error');
                                    setTimeout(function() { $('.js-mail.pln-dropdown').removeClass('open')}, 3000);
                                }
                                if ($('.js-name input').val() == '') {
                                    flag = false;
                                    $('.js-name.pln-dropdown').addClass('open').addClass('error');
                                    setTimeout(function() { $('.js-name.pln-dropdown').removeClass('open')}, 3000);
                                }
                                if ($('.js-question textarea').val() == '') {
                                    flag = false;
                                    $('.js-question.pln-dropdown').addClass('open').addClass('error');
                                    setTimeout(function() { $('.js-question.pln-dropdown').removeClass('open')}, 3000);
                                }
                                if (flag) {
                                    $('.js-error').removeClass('error');
                                    $.ajax({
                                        type: 'post',
                                        url: "/api/public/feedback.json",
                                        data: {
                                            quickstart: true,
                                            email: $('.js-mail input').val(),
                                            message: $('.js-question textarea').val(),
                                            theme: 'Вопрос по созданию проекта',
                                            additionalInfo: $('.js-name input').val()
                                        },
                                        success: function(response){
                                                if(window.gtm) {
                                                    window.gtm.trackUniversalEvent('quick' + '${quickstartType}' + '_ask_question');
                                                }

                                                if (response && response.success) {
                                                    $('.js-send.pln-dropdown').addClass('open');
                                                    setTimeout(function() { $('.js-send.pln-dropdown').removeClass('open')}, 3000);
                                                    $('.js-error input').val('');
                                                    $('.js-error textarea').val('');
                                                }
                                            }
                                    });
                                }
                            });
                        })

                    </script>
                </div>
            </div>

        </div>
    </div>
</div>

<div class="landing-wrap">

<div class="stories">
    <div class="stories_list transform-animate">
    <div class="stories_col" >
        <div class="stories_i visible">
            <div class="stories_i_cont">
                <div class="stories_create">
                    <a class="stories_create_btn" href="/funding-rules">Создать проект</a>
                </div>
                <a class="stories_link" href="https://planeta.ru/stories/tarakany" target="_blank">
                    <div class="stories_cover-wrap">
                        <div class="stories_cover">
                            <img class="stories_cover_img" src="//${hf:getStaticBaseUrl("")}/images/quickstart/stories/tarakany.jpg">
                        </div>
                        <div class="stories_plus"><span>+</span></div>
                    </div>
                    <div class="stories_cont">
                        <div class="stories_name">Альбом «Тараканов!» «Maximum Happy»</div>
                    </div>
                </a>
            </div>
        </div>
        <div class="stories_i visible" >
            <div class="stories_i_cont">
                <div class="stories_create">
                    <a class="stories_create_btn" href="/funding-rules">Создать проект</a>
                </div>
                <a class="stories_link" href="https://planeta.ru/stories/muzej" target="_blank">
                    <div class="stories_cover-wrap">
                        <div class="stories_cover">
                            <img class="stories_cover_img" src="//${hf:getStaticBaseUrl("")}/images/quickstart/stories/nalichniki.jpg">
                        </div>
                        <div class="stories_plus"><span>+</span></div>
                    </div>
                    <div class="stories_cont">
                        <div class="stories_name">Создание виртуального музея резных наличников</div>
                    </div>
                </a>
            </div>
        </div>
        <div class="stories_i visible">
            <div class="stories_i_cont">
                <div class="stories_create">
                    <a class="stories_create_btn" href="/funding-rules">Создать проект</a>
                </div>
                <a class="stories_link" href="https://planeta.ru/stories/ilfipetrov" target="_blank">
                    <div class="stories_cover-wrap">
                        <div class="stories_cover">
                            <img class="stories_cover_img" src="//${hf:getStaticBaseUrl("")}/images/quickstart/stories/ilfpetrov.jpg">
                        </div>
                        <div class="stories_plus"><span>+</span></div>
                    </div>
                    <div class="stories_cont">
                        <div class="stories_name">Съемки документально-анимационного фильма «ИЛЬФИПЕТРОВ»</div>
                    </div>
                </a>
            </div>
        </div>
    </div>
    <div class="stories_col" >
        <div class="stories_i visible">
            <div class="stories_i_cont">
                <div class="stories_create">
                    <a class="stories_create_btn" href="/funding-rules">Создать проект</a>
                </div>
                <a class="stories_link" href="https://planeta.ru/stories/lavkalavka" target="_blank">
                    <div class="stories_cover-wrap">
                        <div class="stories_cover">
                            <img class="stories_cover_img" src="//${hf:getStaticBaseUrl("")}/images/quickstart/stories/lavka.jpg">
                        </div>
                        <div class="stories_plus"><span>+</span></div>
                    </div>
                    <div class="stories_cont">
                        <div class="stories_name">Открытие фермерского магазина LavkaLavka!</div>
                    </div>
                </a>
            </div>
        </div>
        <div class="stories_i visible">
            <div class="stories_i_cont">
                <div class="stories_create">
                    <a class="stories_create_btn" href="/funding-rules">Создать проект</a>
                </div>
                <a class="stories_link" href="https://planeta.ru/stories/bolshe-tepla" target="_blank">
                    <div class="stories_cover-wrap">
                        <div class="stories_cover">
                            <img class="stories_cover_img" src="//${hf:getStaticBaseUrl("")}/images/quickstart/stories/teplo.jpg">
                        </div>
                        <div class="stories_plus"><span>+</span></div>
                    </div>
                    <div class="stories_cont">
                        <div class="stories_name">Благотворительный проект «Больше тепла!»</div>
                    </div>
                </a>
            </div>
        </div>
        <div class="stories_i visible">
            <div class="stories_i_cont">
                <div class="stories_create">
                    <a class="stories_create_btn" href="/funding-rules">Создать проект</a>
                </div>
                <a class="stories_link" href="https://planeta.ru/stories/stim" target="_blank">
                    <div class="stories_cover-wrap">
                        <div class="stories_cover">
                            <img class="stories_cover_img" src="//${hf:getStaticBaseUrl("")}/images/quickstart/stories/stim.jpg">
                        </div>
                        <div class="stories_plus"><span>+</span></div>
                    </div>
                    <div class="stories_cont">
                        <div class="stories_name">Запись сингла St1m «Коридоры»</div>
                    </div>
                </a>
            </div>
        </div>
    </div>
    <div class="stories_col" >
        <div class="stories_i visible" >
            <div class="stories_i_cont">
                <div class="stories_create">
                    <a class="stories_create_btn" href="/funding-rules">Создать проект</a>
                </div>
                <a class="stories_link" href="https://planeta.ru/stories/bardin" target="_blank">
                    <div class="stories_cover-wrap">
                        <div class="stories_cover">
                            <img class="stories_cover_img" src="//${hf:getStaticBaseUrl("")}/images/quickstart/stories/bardin.jpg">
                        </div>
                        <div class="stories_plus"><span>+</span></div>
                    </div>
                    <div class="stories_cont">
                        <div class="stories_name">Новый мультфильм Гарри Бардина «Три мелодии»</div>
                    </div>
                </a>
            </div>
        </div>
        <div class="stories_i visible" >
            <div class="stories_i_cont">
                <div class="stories_create">
                    <a class="stories_create_btn" href="/funding-rules">Создать проект</a>
                </div>
                <a class="stories_link" href="https://planeta.ru/stories/delo1937" target="_blank">
                    <div class="stories_cover-wrap">
                        <div class="stories_cover">
                            <img class="stories_cover_img" src="//${hf:getStaticBaseUrl("")}/images/quickstart/stories/delo1937.jpg">
                        </div>
                        <div class="stories_plus"><span>+</span></div>
                    </div>
                    <div class="stories_cont">
                        <div class="stories_name">Выпуск аудиоспектакля «Дело №1937»</div>
                    </div>
                </a>
            </div>
        </div>
        <div class="stories_i visible">
            <div class="stories_i_cont">
                <div class="stories_create">
                    <a class="stories_create_btn" href="/funding-rules">Создать проект</a>
                </div>
                <a class="stories_link" href="https://planeta.ru/stories/grishkovets1" target="_blank">
                    <div class="stories_cover-wrap">
                        <div class="stories_cover">
                            <img class="stories_cover_img" src="//${hf:getStaticBaseUrl("")}/images/quickstart/stories/grishkovec.jpg">
                        </div>
                        <div class="stories_plus"><span>+</span></div>
                    </div>
                    <div class="stories_cont">
                        <div class="stories_name">Создание видеоверсии спектакля Евгения Гришковца «+1»</div>
                    </div>
                </a>
            </div>
        </div>
    </div>
    <div class="stories_col" >
        <div class="stories_i visible" >
            <div class="stories_i_cont">
                <div class="stories_create">
                    <a class="stories_create_btn" href="/funding-rules">Создать проект</a>
                </div>
                <a class="stories_link" href="https://planeta.ru/stories/spirit" target="_blank">
                    <div class="stories_cover-wrap">
                        <div class="stories_cover">
                            <img class="stories_cover_img" src="//${hf:getStaticBaseUrl("")}/images/quickstart/stories/spirit.jpg">
                        </div>
                        <div class="stories_plus"><span>+</span></div>
                    </div>
                    <div class="stories_cont">
                        <div class="stories_name">Новый альбом Би-2 «Spirit»</div>
                    </div>
                </a>
            </div>
        </div>
        <div class="stories_i visible">
            <div class="stories_i_cont">
                <div class="stories_create">
                    <a class="stories_create_btn" href="/funding-rules">Создать проект</a>
                </div>
                <a class="stories_link" href="https://planeta.ru/stories/jukeboxtrio" target="_blank">
                    <div class="stories_cover-wrap">
                        <div class="stories_cover">
                            <img class="stories_cover_img" src="//${hf:getStaticBaseUrl("")}/images/quickstart/stories/jukebox.jpg">
                        </div>
                        <div class="stories_plus"><span>+</span></div>
                    </div>
                    <div class="stories_cont">
                        <div class="stories_name">Jukebox Trio «RODINA. Часть&nbsp;2»</div>
                    </div>
                </a>
            </div>
        </div>
        <div class="stories_i visible" >
            <div class="stories_i_cont">
                <div class="stories_create">
                    <a class="stories_create_btn" href="/funding-rules">Создать проект</a>
                </div>
                <a class="stories_link" href="https://planeta.ru/stories/billysband" target="_blank">
                    <div class="stories_cover-wrap">
                        <div class="stories_cover">
                            <img class="stories_cover_img" src="//${hf:getStaticBaseUrl("")}/images/quickstart/stories/billy.jpg">
                        </div>
                        <div class="stories_plus"><span>+</span></div>
                    </div>
                    <div class="stories_cont">
                        <div class="stories_name">Запись альбома группы Billy's Band «Когда был один»</div>
                    </div>
                </a>
            </div>
        </div>
    </div>
    <div class="stories_col" >
        <div class="stories_i visible" >
            <div class="stories_i_cont">
                <div class="stories_create">
                    <a class="stories_create_btn" href="/funding-rules">Создать проект</a>
                </div>
                <a class="stories_link" href="https://planeta.ru/stories/animaljazz" target="_blank">
                    <div class="stories_cover-wrap">
                        <div class="stories_cover">
                            <img class="stories_cover_img" src="//${hf:getStaticBaseUrl("")}/images/quickstart/stories/animal.jpg">
                        </div>
                        <div class="stories_plus"><span>+</span></div>
                    </div>
                    <div class="stories_cont">
                        <div class="stories_name">Запись альбома группы «Animal ДжаZ» «Фаза быстрого сна»</div>
                    </div>
                </a>
            </div>
        </div>
        <div class="stories_i visible" >
            <div class="stories_i_cont">
                <div class="stories_create">
                    <a class="stories_create_btn" href="/funding-rules">Создать проект</a>
                </div>
                <a class="stories_link" href="https://planeta.ru/stories/pilot" target="_blank">
                    <div class="stories_cover-wrap">
                        <div class="stories_cover">
                            <img class="stories_cover_img" src="//${hf:getStaticBaseUrl("")}/images/quickstart/stories/pilot.jpg">
                        </div>
                        <div class="stories_plus"><span>+</span></div>
                    </div>
                    <div class="stories_cont">
                        <div class="stories_name">Запись нового альбома группы «Пилот»</div>
                    </div>
                </a>
            </div>
        </div>
        <div class="stories_i visible" >
            <div class="stories_i_cont">
                <div class="stories_create">
                    <a class="stories_create_btn" href="/funding-rules">Создать проект</a>
                </div>
                <a class="stories_link" href="https://planeta.ru/stories/undervud" target="_blank">
                    <div class="stories_cover-wrap">
                        <div class="stories_cover">
                            <img class="stories_cover_img" src="//${hf:getStaticBaseUrl("")}/images/quickstart/stories/undervud.jpg">
                        </div>
                        <div class="stories_plus"><span>+</span></div>
                    </div>
                    <div class="stories_cont">
                        <div class="stories_name">Запись альбома группы «Ундервуд» «Женщины и&nbsp;дети»</div>
                    </div>
                </a>
            </div>
        </div>
    </div>
    <div class="stories_col" >
        <div class="stories_i visible" >
            <div class="stories_i_cont">
                <div class="stories_create">
                    <a class="stories_create_btn" href="/funding-rules">Создать проект</a>
                </div>
                <a class="stories_link" href="https://planeta.ru/stories/5diez" target="_blank">
                    <div class="stories_cover-wrap">
                        <div class="stories_cover">
                            <img class="stories_cover_img" src="//${hf:getStaticBaseUrl("")}/images/quickstart/stories/5diez.jpg">
                        </div>
                        <div class="stories_plus"><span>+</span></div>
                    </div>
                    <div class="stories_cont">
                        <div class="stories_name">Запись альбома группы ##### (5diez) «MMXIII»</div>
                    </div>
                </a>
            </div>
        </div>
        <div class="stories_i visible">
            <div class="stories_i_cont">
                <div class="stories_create">
                    <a class="stories_create_btn" href="/funding-rules">Создать проект</a>
                </div>
                <a class="stories_link" href="https://planeta.ru/stories/colta1" target="_blank">
                    <div class="stories_cover-wrap">
                        <div class="stories_cover">
                            <img class="stories_cover_img" src="//${hf:getStaticBaseUrl("")}/images/quickstart/stories/colta.jpg">
                        </div>
                        <div class="stories_plus"><span>+</span></div>
                    </div>
                    <div class="stories_cont">
                        <div class="stories_name">Запуск сайта Colta.ru</div>
                    </div>
                </a>
            </div>
        </div>
        <div class="stories_i visible">
            <div class="stories_i_cont">
                <div class="stories_create">
                    <a class="stories_create_btn" href="/funding-rules">Создать проект</a>
                </div>
                <a class="stories_link" href="https://planeta.ru/stories/opus" target="_blank">
                    <div class="stories_cover-wrap">
                        <div class="stories_cover">
                            <img class="stories_cover_img" src="//${hf:getStaticBaseUrl("")}/images/quickstart/stories/opus.jpg">
                        </div>
                        <div class="stories_plus"><span>+</span></div>
                    </div>
                    <div class="stories_cont">
                        <div class="stories_name">Запись сольного альбома Дмитрия Ревякина «Grandi Canzoni, Opus 1»</div>
                    </div>
                </a>
            </div>
        </div>
    </div>
    <div class="stories_col" >
        <div class="stories_i">
            <div class="stories_i_cont">
                <div class="stories_create">
                    <a class="stories_create_btn" href="/funding-rules">Создать проект</a>
                </div>
                <a class="stories_link" href="https://planeta.ru/stories/tarakany" target="_blank">
                    <div class="stories_cover-wrap">
                        <div class="stories_cover">
                            <img class="stories_cover_img" src="//${hf:getStaticBaseUrl("")}/images/quickstart/stories/tarakany.jpg">
                        </div>
                        <div class="stories_plus"><span>+</span></div>
                    </div>
                    <div class="stories_cont">
                        <div class="stories_name">Запись альбома «Тараканов!» «Maximum Happy»</div>
                    </div>
                </a>
            </div>
        </div>
        <div class="stories_i">
            <div class="stories_i_cont">
                <div class="stories_create">
                    <a class="stories_create_btn" href="/funding-rules">Создать проект</a>
                </div>
                <a class="stories_link" href="https://planeta.ru/stories/muzej" target="_blank">
                    <div class="stories_cover-wrap">
                        <div class="stories_cover">
                            <img class="stories_cover_img" src="//${hf:getStaticBaseUrl("")}/images/quickstart/stories/nalichniki.jpg">
                        </div>
                        <div class="stories_plus"><span>+</span></div>
                    </div>
                    <div class="stories_cont">
                        <div class="stories_name">Создание виртуального музея резных наличников</div>
                    </div>
                </a>
            </div>
        </div>
        <div class="stories_i">
            <div class="stories_i_cont">
                <div class="stories_create">
                    <a class="stories_create_btn" href="/funding-rules">Создать проект</a>
                </div>
                <a class="stories_link" href="https://planeta.ru/stories/ilfipetrov" target="_blank">
                    <div class="stories_cover-wrap">
                        <div class="stories_cover">
                            <img class="stories_cover_img" src="//${hf:getStaticBaseUrl("")}/images/quickstart/stories/ilfpetrov.jpg">
                        </div>
                        <div class="stories_plus"><span>+</span></div>
                    </div>
                    <div class="stories_cont">
                        <div class="stories_name">Съемки документально-анимационного фильма «ИЛЬФИПЕТРОВ»</div>
                    </div>
                </a>
            </div>
        </div>
    </div>
    <div class="stories_col" >
        <div class="stories_i">
            <div class="stories_i_cont">
                <div class="stories_create">
                    <a class="stories_create_btn" href="/funding-rules">Создать проект</a>
                </div>
                <a class="stories_link" href="https://planeta.ru/stories/lavkalavka" target="_blank">
                    <div class="stories_cover-wrap">
                        <div class="stories_cover">
                            <img class="stories_cover_img" src="//${hf:getStaticBaseUrl("")}/images/quickstart/stories/lavka.jpg">
                        </div>
                        <div class="stories_plus"><span>+</span></div>
                    </div>
                    <div class="stories_cont">
                        <div class="stories_name">Открытие фермерского магазина LavkaLavka!</div>
                    </div>
                </a>
            </div>
        </div>
        <div class="stories_i">
            <div class="stories_i_cont">
                <div class="stories_create">
                    <a class="stories_create_btn" href="/funding-rules">Создать проект</a>
                </div>
                <a class="stories_link" href="https://planeta.ru/stories/bolshe-tepla" target="_blank">
                    <div class="stories_cover-wrap">
                        <div class="stories_cover">
                            <img class="stories_cover_img" src="//${hf:getStaticBaseUrl("")}/images/quickstart/stories/teplo.jpg">
                        </div>
                        <div class="stories_plus"><span>+</span></div>
                    </div>
                    <div class="stories_cont">
                        <div class="stories_name">Благотворительный проект «Больше тепла!»</div>
                    </div>
                </a>
            </div>
        </div>
        <div class="stories_i">
            <div class="stories_i_cont">
                <div class="stories_create">
                    <a class="stories_create_btn" href="/funding-rules">Создать проект</a>
                </div>
                <a class="stories_link" href="https://planeta.ru/stories/stim" target="_blank">
                    <div class="stories_cover-wrap">
                        <div class="stories_cover">
                            <img class="stories_cover_img" src="//${hf:getStaticBaseUrl("")}/images/quickstart/stories/stim.jpg">
                        </div>
                        <div class="stories_plus"><span>+</span></div>
                    </div>
                    <div class="stories_cont">
                        <div class="stories_name">Запись сингла St1m «Коридоры»</div>
                    </div>
                </a>
            </div>
        </div>
    </div>
    <div class="stories_col" >
        <div class="stories_i">
            <div class="stories_i_cont">
                <div class="stories_create">
                    <a class="stories_create_btn" href="/funding-rules">Создать проект</a>
                </div>
                <a class="stories_link" href="https://planeta.ru/stories/bardin" target="_blank">
                    <div class="stories_cover-wrap">
                        <div class="stories_cover">
                            <img class="stories_cover_img" src="//${hf:getStaticBaseUrl("")}/images/quickstart/stories/bardin.jpg">
                        </div>
                        <div class="stories_plus"><span>+</span></div>
                    </div>
                    <div class="stories_cont">
                        <div class="stories_name">Новый мультфильм Гарри Бардина «Три мелодии»</div>
                    </div>
                </a>
            </div>
        </div>
        <div class="stories_i">
            <div class="stories_i_cont">
                <div class="stories_create">
                    <a class="stories_create_btn" href="/funding-rules">Создать проект</a>
                </div>
                <a class="stories_link" href="https://planeta.ru/stories/delo1937" target="_blank">
                    <div class="stories_cover-wrap">
                        <div class="stories_cover">
                            <img class="stories_cover_img" src="//${hf:getStaticBaseUrl("")}/images/quickstart/stories/delo1937.jpg">
                        </div>
                        <div class="stories_plus"><span>+</span></div>
                    </div>
                    <div class="stories_cont">
                        <div class="stories_name">Выпуск аудиоспектакля «Дело №1937»</div>
                    </div>
                </a>
            </div>
        </div>
        <div class="stories_i">
            <div class="stories_i_cont">
                <div class="stories_create">
                    <a class="stories_create_btn" href="/funding-rules">Создать проект</a>
                </div>
                <a class="stories_link" href="https://planeta.ru/stories/grishkovets1" target="_blank">
                    <div class="stories_cover-wrap">
                        <div class="stories_cover">
                            <img class="stories_cover_img" src="//${hf:getStaticBaseUrl("")}/images/quickstart/stories/grishkovec.jpg">
                        </div>
                        <div class="stories_plus"><span>+</span></div>
                    </div>
                    <div class="stories_cont">
                        <div class="stories_name">Создание видеоверсии спектакля Евгения Гришковца «+1»</div>
                    </div>
                </a>
            </div>
        </div>
    </div>
    <div class="stories_col" >
        <div class="stories_i">
            <div class="stories_i_cont">
                <div class="stories_create">
                    <a class="stories_create_btn" href="/funding-rules">Создать проект</a>
                </div>
                <a class="stories_link" href="https://planeta.ru/stories/spirit" target="_blank">
                    <div class="stories_cover-wrap">
                        <div class="stories_cover">
                            <img class="stories_cover_img" src="//${hf:getStaticBaseUrl("")}/images/quickstart/stories/spirit.jpg">
                        </div>
                        <div class="stories_plus"><span>+</span></div>
                    </div>
                    <div class="stories_cont">
                        <div class="stories_name">Новый альбом Би-2 «Spirit»</div>
                    </div>
                </a>
            </div>
        </div>
        <div class="stories_i">
            <div class="stories_i_cont">
                <div class="stories_create">
                    <a class="stories_create_btn" href="/funding-rules">Создать проект</a>
                </div>
                <a class="stories_link" href="https://planeta.ru/stories/jukeboxtrio" target="_blank">
                    <div class="stories_cover-wrap">
                        <div class="stories_cover">
                            <img class="stories_cover_img" src="//${hf:getStaticBaseUrl("")}/images/quickstart/stories/jukebox.jpg">
                        </div>
                        <div class="stories_plus"><span>+</span></div>
                    </div>
                    <div class="stories_cont">
                        <div class="stories_name">Jukebox Trio «RODINA. Часть&nbsp;2»</div>
                    </div>
                </a>
            </div>
        </div>
        <div class="stories_i">
            <div class="stories_i_cont">
                <div class="stories_create">
                    <a class="stories_create_btn" href="/funding-rules">Создать проект</a>
                </div>
                <a class="stories_link" href="https://planeta.ru/stories/billysband" target="_blank">
                    <div class="stories_cover-wrap">
                        <div class="stories_cover">
                            <img class="stories_cover_img" src="//${hf:getStaticBaseUrl("")}/images/quickstart/stories/billy.jpg">
                        </div>
                        <div class="stories_plus"><span>+</span></div>
                    </div>
                    <div class="stories_cont">
                        <div class="stories_name">Запись альбома группы Billy's Band «Когда был один»</div>
                    </div>
                </a>
            </div>
        </div>
    </div>
    </div>

    <script>
        var colWidthResize = {
            init: function() {
                var landingWrap = $('.landing-wrap');
                var storiesCols = $('.stories_col');
                var colWidth = storiesCols.eq(0).width();

                function colWidthChange() {
                    storiesCols.find('> .stories_i')
                            .filter('.visible')
                            .removeClass('visible');
                    var winWidth = landingWrap.width();
                    var colLengthResult = Math.round(winWidth / colWidth);
                    var colWidthResult = winWidth / colLengthResult;
                    storiesCols.each(function(i) {
                        if ( i < colLengthResult ) {
                            $(this).find('> .stories_i').addClass('visible');
                        }
                        $(this).width(colWidthResult);
                    });
                }

                colWidthChange();

                $(window).resize(colWidthChange);
            }
        };
        colWidthResize.init();

        var storiesFlip = {
            init: function() {
                var items = $('.stories_i');
                var wrap = $('.stories_list');

                if ( !$.fn.styleSupport('transform') || !$.fn.styleSupport('backfaceVisibility') ) {
                    wrap.removeClass('transform-animate')
                            .addClass('simple-animate');
                }

                var flipTime = 3000;
                var flipInterval = 500;

                var flipTimer = setInterval(flip, flipInterval + flipTime);
                var itemTimer;

                function flip() {
                    var itemsVisible = items.filter('.visible');
                    var max = itemsVisible.length - 1;
                    var rand = Math.floor(Math.random() * max);
                    var item = itemsVisible.eq(rand);
                    item.addClass('flip');
                    item.css({zIndex: 2});

                    itemTimer = setTimeout(unflip, flipTime);
                }

                function unflip() {
                    var item = items.filter('.flip');
                    item.find('.stories_link').afterTransition(function() {
                        item.css({zIndex: 'auto'});
                    });
                    item.removeClass('flip');
                }

                wrap.hover(function() {
                    clearInterval(flipTimer);
                    clearTimeout(itemTimer);
                }, function() {
                    flipTimer = setInterval(flip, flipInterval + flipTime);
                    itemTimer = setTimeout(unflip, flipTime);
                });

            }
        };
        storiesFlip.init();
    </script>
    </div>
</div>
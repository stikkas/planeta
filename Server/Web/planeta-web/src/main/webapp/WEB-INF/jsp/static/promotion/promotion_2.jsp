<%@ include file="/WEB-INF/jsp/includes/generated/doctype-header.jsp" %>
<head>
    <%@include file="head.jsp"%>
    <%@include file="/WEB-INF/jsp/includes/generated/stat-counters-async.jsp"%>
    <title>Руководство по продвижению на этапе 25-50%</title>
</head>
<body class="project-promotion-page">
<%@ include file="/WEB-INF/jsp/includes/generated/irma/header.jsp" %>
<%@ include file="/WEB-INF/jsp/includes/generated/irma/main-nav.jsp" %>


<div id="global-container">
    <div id="main-container" class="wrap-container">
        <div id="center-container">


            <div class="project-promotion_head">Руководство по продвижению на этапе 25-50%</div>


            <div class="project-promotion_top">
                <div class="wrap">
                    <div class="col-12">

                        <div class="project-promotion_top_cont">
                            <div class="project-promotion_top_ico s-promotion-top-people"></div>
                            <div class="project-promotion_top_text">
                                Тот самый этап, на котором ваш энтузиазм должен возрасти в разы. Ваша задача — приблизиться к отметке в 50%, после чего собранная сумма станет «несгораемой». Ключ к успеху — люди. Используйте любые контакты, которыми вы располагаете, обращайтесь к лидерам мнений, администраторам сообществ, предлагайте для публикации новости о вашем проекте в СМИ.
                            </div>
                        </div>

                    </div>
                </div>
            </div>


            <div class="project-promotion_cont">
                <div class="wrap">
                    <div class="col-12">

                        <div class="project-promotion_list">
                            <div class="project-promotion_list-i">
                                <div class="project-promotion_list-ico s-promotion-list-vk"></div>
                                <div class="project-promotion_list-head">
                                    Личные интернет-ресурсы — это ваш персональный сайт, паблики и личные страницы в
                                    социальных сетях, а также собственные блоги. Вы также можете создавать личные
                                    страницы специально под проект, используя все перечисленные выше площадки.
                                </div>
                                <div class="project-promotion_list-text">
                                    <span class="step-hl">Шаг 1:</span> Расскажите о том, чего уже удалось достичь, будь
                                    то <a href="https://planeta.ru/planet7000/blog/117041">сбор «круглой» суммы</a>, <a href="https://vk.com/blacksmithspb?w=wall-9161_13263">количество поучаствовавших в проекте</a>, средний чек или
                                    <a href="https://planeta.ru/150972/blog/121638">количество вышедших публикаций</a>.
                                    <br>
                                    <br>
                                    <span class="step-hl">Шаг 2:</span> Сакцентируйте внимание на вознаграждениях, которые вы
                                    недавно добавили в проект, которые <a href="https://vk.com/romanrainofficial?w=wall-100239_11301">пользуются большой популярностью</a> (в блоге Планеты
                                    это можно делать <a href="https://planeta.ru/253363/blog/126381">так</a>) или которые специально для вашего проекта <a href="http://louna.ru/novy-e-aktsii-v-nashem-proekte-na-planeta-ru/">предоставили
                                    известные люди/лидеры мнений</a>. Не забывайте радовать своих спонсоров специально
                                    подготовленным для них <a href="https://planeta.ru/piknik/blog/127326">эксклюзивным материалом</a>.
                                </div>
                            </div>

                            <div class="project-promotion_list-i">
                                <div class="project-promotion_list-ico s-promotion-list-msg"></div>
                                <div class="project-promotion_list-head">
                                    Рассылки — это информация, которую вы распространяете по друзьям, коллегам,
                                    родственникам и партнерам, используя личные сообщения в соцсетях, e-mail письма,
                                    телефонные звонки и sms-сообщения.
                                </div>
                                <div class="project-promotion_list-text">
                                    <span class="step-hl">Шаг 1:</span> Продолжайте работать с рассылкой.
                                    Сконцентрируйте внимание на дополнительных активностях. Например, предложите
                                    совместный конкурсы в рамках проекта/информационное спонсорство/предоставление
                                    призов. <a href="https://vk.com/b2band?w=wall-225666_95851">Пример</a>.
                                </div>
                            </div>

                            <div class="project-promotion_list-i">
                                <div class="project-promotion_list-ico s-promotion-list-events"></div>
                                <div class="project-promotion_list-head">
                                    Мероприятия — это оффлайновые встречи (концерты, выставки, мастер-классы,
                                    выступления, дружеские вечеринки), на которых вы можете рассказать о своем проекте.
                                    Не ограничивайтесь интернетом, рассказывайте о своем проекте на разных площадках!
                                </div>
                                <div class="project-promotion_list-text">
                                    <span class="step-hl">Шаг 1:</span> Проведите закрытое мероприятие для спонсоров,
                                    пригласив их на концерт/презентацию/выставку или любое другое событие в рамках
                                    вашего проекта. Пример <a href="https://planeta.ru/happypeople/blog/124891">новости</a> и <a href="https://planeta.ru/happypeople/blog/125221">пост-релиза</a>.
                                </div>
                            </div>

                            <div class="project-promotion_list-i">
                                <div class="project-promotion_list-ico s-promotion-list-media"></div>
                                <div class="project-promotion_list-head">
                                    Сообщества и СМИ — это тематические блоги, паблики, сообщества в Facebook,
                                    ВКонтакте, Живом Журнале, Твиттере, а также близкие вашему проекту по тематике
                                    средства массовой информации.
                                </div>
                                <div class="project-promotion_list-text">
                                    <span class="step-hl">Шаг 1:</span> <a href="http://burodd.ru/podari-knigu-detyam">Сделайте акцент на бонусах</a>. Вы уже рассказали о
                                    запуске проекта, теперь нужно привлечь потенциальных спонсоров в проект. Вознаграждения –
                                    ваша визитная карточка.
                                    <br>
                                    <br>
                                    <span class="step-hl">Шаг 2:</span> Не забывайте рассказывать об общих новостях
                                    проекта, таких как конкурсы, <a href="https://vk.com/janeairband?w=wall-9828_49271">добавление новых вознаграждений</a>, тизеры и прочее.
                                    <br>
                                    <br>
                                    <span class="step-hl">Шаг 3:</span> Предложите уникальный контент для размещения в
                                    СМИ или сообщества. Это может быть анонс видеоклипа/<a href="http://lenta.ru/articles/2013/10/31/kmost/">песни</a>/концерта/глава из книги
                                    или что-то другое.
                                </div>
                            </div>

                            <div class="project-promotion_list-i">
                                <div class="project-promotion_list-ico s-promotion-list-person"></div>
                                <div class="project-promotion_list-head">
                                    Лидеры мнений — это известные и уважаемые личности со сложившейся большой аудиторией
                                    (в том числе, и сетевой): общественные деятели, блогеры, журналисты, акулы бизнеса.
                                </div>
                                <div class="project-promotion_list-text">
                                    <span class="step-hl">Шаг 1:</span> Привлекайте в ваш проект новых людей. Они – ваш
                                    источник силы. Не важно, в каком формате это будет: <a href="http://dolboeb.livejournal.com/2583370.html">пост</a>, ретвит, <a href="https://www.facebook.com/permalink.php?story_fbid=621242517909477&amp;id=100000712037223">анонс в соц.сетях</a>,
                                    <a href="https://www.youtube.com/watch?v=Vw1SSJU4y5Y">видеокомментарий</a>, бонус в проекте, <a href="http://odnovremenno.com/">баннер на сайте</a>, <a href="https://planeta.ru/128318/blog/118065">совместная акция</a>, <a href="https://www.facebook.com/misha.kozyrev/posts/633592420013133">репост</a>.
                                    <br>
                                    <br>
                                    <span class="step-hl">Шаг 2:</span> Подумайте к кому вы могли бы обратиться из
                                    авторов Планеты. Все-таки у этих людей уже есть опыт и понимание в том, как вести
                                    свой проект, к тому же они обладают своей аудиторией и смогут привлечь в ваш проект
                                    совершенно другую аудиторию. Например, написав об этом в своем <a href="https://planeta.ru/zhelannaja/blog/122264">блоге на Планете</a>/<a href="https://vk.com/nsmusicband?w=wall-256815_4246">в
                                    социальных сетях</a>/на личном сайте.
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>


        </div>
    </div>
</div>


<%@ include file="/WEB-INF/jsp/includes/generated/irma/footer.jsp" %>
</body>
</html>
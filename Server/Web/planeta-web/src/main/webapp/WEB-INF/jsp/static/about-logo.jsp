<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ include file="/WEB-INF/jsp/includes/generated/doctype-header.jsp" %>
<%@ taglib prefix="p" uri="http://planeta.ru/tags" %>

<head>
    <c:choose>
        <c:when test="${not empty customMetaTag.title}">
            <title>${customMetaTag.title}</title>
        </c:when>
        <c:otherwise>
            <title>О проекте Planeta.ru</title>
        </c:otherwise>
    </c:choose>
    <c:choose>
        <c:when test="${not empty customMetaTag.description}">
            <meta name="description" content="${customMetaTag.description}"/>
        </c:when>
        <c:otherwise>
            <meta name="description"
                  content="Что такое онлайн-площадка Planeta.ru?"/>
        </c:otherwise>
    </c:choose>
    <c:if test="${not empty customMetaTag.keywords}">
        <meta name="keywords" content="${customMetaTag.keywords}"/>
    </c:if>

    <meta name="viewport" content="width=device-width"/>
    <%@include file="/WEB-INF/jsp/includes/generated/stat-counters-async.jsp"%>

    <%@include file="/WEB-INF/jsp/includes/common-css.jsp" %>
    <link type="text/css" rel="stylesheet" href="//${hf:getStaticBaseUrl("")}/css-generated/about.css"/>
    <%@include file="/WEB-INF/jsp/includes/generated/common-templates.jsp" %>
    <%@include file="/WEB-INF/jsp/includes/planeta-js.jsp" %>

</head>

<body class="grid-1200 about-page">
<%@ include file="/WEB-INF/jsp/includes/generated/irma/header.jsp" %>
<%@ include file="/WEB-INF/jsp/includes/generated/irma/main-nav.jsp" %>

<div id="global-container">
    <div id="main-container" class="wrap-container">

        <div id="center-container">

            <%@ include file="/WEB-INF/jsp/includes/about-new-menu.jsp" %>


            <div class="wrap">
                <div class="col-12">
                    <div class="about-head">
                        Цвета и логотипы
                    </div>


                    <div class="about-head-text">
                        Использование правильного цвета логотипа очень важно при создании рекламных материалов. Пожалуйста, проверьте цвет логотипа в баннере или в макетах печатной продукции.
                    </div>
                </div>
            </div>


            <div class="wrap">
                <div class="col-12">

                    <div class="logos-pantone wrap-row">
                        <div class="logos-pantone_i col-4">
                            <span class="logos-pantone_lbl">
                                Pantone
                            </span>
                            <span class="logos-pantone_val">
                                306 C
                            </span>
                        </div>

                        <div class="logos-pantone_i col-4">
                            <span class="logos-pantone_lbl">
                                CMYK
                            </span>
                            <span class="logos-pantone_val">
                                81% 3% 5% 0%
                            </span>
                        </div>

                        <div class="logos-pantone_i col-4">
                            <span class="logos-pantone_lbl">
                                #
                            </span>
                            <span class="logos-pantone_val">
                                00b8e5
                            </span>
                        </div>
                    </div>


                    <div class="logos">
                        <div class="logos_i">
                            <div class="logos_logo">
                                <img src="//${hf:getStaticBaseUrl("")}/images/pantone/pln-logo.svg">
                            </div>
                            <div class="logos_links">
                                <a href="https://files.planeta.ru/media-kit/planeta-01.pdf">Скачать в PDF</a>
                                <a href="https://files.planeta.ru/media-kit/pantone-logo-white.png">Скачать в PNG</a>
                            </div>
                        </div>

                        <div class="logos_i">
                            <div class="logos_logo logos_logo__black">
                                <img src="//${hf:getStaticBaseUrl("")}/images/pantone/pln-logo-black.svg">
                            </div>
                            <div class="logos_links">
                                <a href="https://files.planeta.ru/media-kit/planeta-02.pdf">Скачать в PDF</a>
                                <a href="https://files.planeta.ru/media-kit/pantone-logo-black.png">Скачать в PNG</a>
                            </div>
                        </div>

                        <div class="logos_i">
                            <div class="logos_logo logos_logo__white">
                                <img src="//${hf:getStaticBaseUrl("")}/images/pantone/pln-logo-white.svg">
                            </div>
                            <div class="logos_links">
                                <a href="https://files.planeta.ru/media-kit/planeta-03.pdf">Скачать в PDF</a>
                                <a href="https://files.planeta.ru/media-kit/pantone-logo-blue.png">Скачать в PNG</a>
                            </div>
                        </div>
                    </div>


                </div>
            </div>

        </div>


    </div>

</div>
<%@ include file="/WEB-INF/jsp/includes/generated/irma/footer.jsp" %>
</body>
</html>

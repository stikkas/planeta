<%@ include file="/WEB-INF/jsp/includes/generated/doctype-header.jsp" %>
<%@taglib prefix="p" uri="http://planeta.ru/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="baseUrl" value="${hf:getStaticBaseUrl('')}"/>
<head>

    <%@include file="/WEB-INF/jsp/includes/generated/common-templates.jsp" %>
    <%@include file="/WEB-INF/jsp/includes/generated/stat-counters-head.jsp" %>
    <script type="text/javascript" src="//${hf:getStaticBaseUrl("/res")}/js/planeta-birthday-common.js?compress=false&flushCache=false"></script>
    <%@include file="/WEB-INF/jsp/includes/planeta-js.jsp" %>
    <p:script src="planeta-birthday.js"></p:script>
    <%@include file="/WEB-INF/jsp/includes/generated/common-favicon.jsp" %>

    <title>День рождения крауд-платформы «Планета» | История краудфандинга в России</title>
            <meta property="og:title" content="День рождения крауд-платформы «Планета» | История краудфандинга в России"/>

            <meta name="description" content="7 июня не просто День российского краудфандинга – это день рождения платформы Planeta.ru. Вспомнить «как это было», кликайте по ссылке. Итак, мы взлетаем!"/>
            <meta property="og:description" content="7 июня не просто День российского краудфандинга – это день рождения платформы Planeta.ru. Вспомнить «как это было», кликайте по ссылке. Итак, мы взлетаем!"/>
            <meta property="og:image" content="https://s1.planeta.ru/i/123c8e/original.jpg" />
            <meta name="keywords" content="день российского краудфандинга, краудфандинг в россии, краудфандинг, история краудфандинга, день рождения планеты, планета ру"/>


            <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">

            <meta http-equiv="Content-Type" content="text/html; charset=utf-8">

            <link href="//${hf:getStaticBaseUrl("")}/css-generated/planeta-4-years.css" type="text/css" rel="stylesheet">




        </head>
        <body>


        <div class="page-scroll">
            <div><div class="page-scroll_mouse"></div></div>
            <div><div class="page-scroll_point animBounce"></div></div>
            <div class="page-scroll_text">вниз</div>
        </div>


        <div class="planet-back">
            <div class="planet-back-wrap">
                <div class="planet-slide"></div>
            </div>
        </div>




        <div id="particles-js"></div>




        <div class="rocket-smoke-wrap">
            <div class="wrap">
                <div class="rocket-smoke-2"></div>
                <div class="rocket-smoke-1"></div>
            </div>
        </div>



        <div class="rocket-wrap">
            <div class="wrap">
                <div class="rocket"></div>
            </div>
        </div>




        <div class="year-nav">
            <a href="#page-1" class="year-nav_link" id="year-nav-1">&mdash; 2012</a>
            <a href="#page-2" class="year-nav_link" id="year-nav-2">&mdash; 2013</a>
            <a href="#page-3" class="year-nav_link" id="year-nav-3">&mdash; 2014</a>
            <a href="#page-4" class="year-nav_link" id="year-nav-4">&mdash; 2015</a>
            <a href="#page-5" class="year-nav_link" id="year-nav-5">&mdash; 2016</a>
        </div>



        <div id="wrap-1" class="year-wrap">
            <div class="page" id="page-1">
                <div class="page-cont">
                    <div class="page-wrap wrap">

                        <div class="header">
                            <div class="wrap">
                                <div class="header_cont">
                                    <a href="https://planeta.ru/" class="logo"></a>

                                    <div class="header_text">— нам 4 года!</div>
                                </div>
                            </div>
                        </div>



                        <div class="timeline timeline__sm">
                            <div class="timeline_i">
                                <div class="timeline_date">
                                    10 января 2012
                                </div>
                                <div class="timeline_event timeline_event__md">
                                    <div class="timeline_project-wrap">
                                        <div class="timeline_project-ico">
                                            <div class="project-satellite"></div>
                                        </div>
                                        <div class="timeline_project">
                                            <div class="timeline_project-box">
                                                <nobr>Бета-тест</nobr>: запущен первый проект&nbsp;&mdash; альбом Spirit группы<span class="ensp"> </span> <nobr><span class="laquo">&laquo;</span>Би-2&raquo;!</nobr>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="timeline_i">
                                <div class="timeline_date">
                                    4 мая 2012
                                </div>
                                <div class="timeline_event">
                                    <div class="timeline_title">
                                        Первый миллион есть! Альбом Spirit!
                                    </div>
                                </div>
                            </div>

                            <div class="timeline_i">
                                <div class="timeline_date">
                                    30 мая 2012
                                </div>
                                <div class="timeline_event">
                                    <div class="timeline_title">
                                        Запущено 10 проектов!
                                    </div>
                                </div>
                            </div>

                            <div class="timeline_i">
                                <div class="timeline_date">
                                    7 июня 2012
                                </div>
                                <div class="timeline_event">
                                    <div class="timeline_project-wrap">
                                        <div class="timeline_project-ico">
                                            <div class="project-cosmonaut"></div>
                                        </div>
                                        <div class="timeline_project">
                                            <div class="timeline_project-box">
                                                Официальный старт Planeta.ru! <span class="hl">Поехали!</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="timeline_i">
                                <div class="timeline_date">
                                    27 октября 2012
                                </div>
                                <div class="timeline_event timeline_event__md">
                                    <div class="timeline_project-wrap">
                                        <div class="timeline_project-ico">
                                            <div class="project-planet-3"></div>
                                        </div>
                                        <div class="timeline_project">
                                            <div class="timeline_project-box">
                                                Первая трансляция! Онлайн с&nbsp;концерта<span class="ensp"> </span> <span class="laquo">&laquo;</span><nobr>Би-2</nobr><span class="raquo">&raquo;</span><span class="ensp"> </span> в&nbsp;Crocus City Hall
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="timeline_i">
                                <div class="timeline_date">
                                    19 ноября 2012
                                </div>
                                <div class="timeline_event">
                                    <div class="timeline_title">
                                        5 000 000 рублей!
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>



        <div id="wrap-2" class="year-wrap">
            <div class="year">
                <div class="year-cont">
                    2013
                </div>
            </div>



            <div class="page" id="page-2">
                <div class="page-cont">
                    <div class="page-wrap wrap">

                        <div class="timeline">
                            <div class="timeline_i">
                                <div class="timeline_date">
                                    18 марта 2013
                                </div>
                                <div class="timeline_event">
                                    <div class="timeline_title">
                                        10 000 000 рублей!
                                    </div>
                                </div>
                            </div>

                            <div class="timeline_i">
                                <div class="timeline_date">
                                    31 августа 2013
                                </div>
                                <div class="timeline_event timeline_event__md">
                                    <div class="timeline_project-wrap">
                                        <div class="timeline_project-ico">
                                            <div class="project-planet-2"></div>
                                        </div>
                                        <div class="timeline_project">
                                            <div class="timeline_project-box">
                                                Мультфильм Гарри Бардина<span class="ensp"> </span> <span class="laquo">&laquo;</span>Три мелодии<span class="raquo">&raquo;</span><span class="ensp"> </span> собрал 2&nbsp;251&nbsp;681&nbsp;рублей!
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="timeline_i">
                                <div class="timeline_date">
                                    14 ноября 2013
                                </div>
                                <div class="timeline_event">
                                    <div class="timeline_title">
                                        Запущено 1 000 проектов!
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>



        <div id="wrap-3" class="year-wrap">
            <div class="year">
                <div class="year-cont">
                    2014
                </div>
            </div>



            <div class="page" id="page-3">
                <div class="page-cont">
                    <div class="page-wrap wrap">

                        <div class="timeline">
                            <div class="timeline_i">
                                <div class="timeline_date">
                                    3 апреля 2014
                                </div>
                                <div class="timeline_event timeline_event__md">
                                    <div class="timeline_project-wrap">
                                        <div class="timeline_project-ico">
                                            <div class="project-planet-1"></div>
                                        </div>
                                        <div class="timeline_project">
                                            <div class="timeline_project-box">
                                                Телеспектакль Виктора Шендеровича и&nbsp;Владимира Мирзоева<span class="ensp"> </span> <span class="laquo">&laquo;</span>Петрушка<span class="raquo">&raquo;</span><span class="ensp"> </span> собрал 5&nbsp;865&nbsp;800&nbsp;рублей!
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="timeline_i">
                                <div class="timeline_date">
                                    15 мая 2014
                                </div>
                                <div class="timeline_event">
                                    <div class="timeline_title">
                                        100 000 000 рублей!
                                    </div>
                                </div>
                            </div>

                            <div class="timeline_i">
                                <div class="timeline_date">
                                    21 мая 2014
                                </div>
                                <div class="timeline_event">
                                    <div class="timeline_title">
                                        Запущено 2 000 проектов!
                                    </div>
                                </div>
                            </div>

                            <div class="timeline_i">
                                <div class="timeline_date">
                                    2 августа 2014
                                </div>
                                <div class="timeline_event timeline_event__md">
                                    <div class="timeline_project-wrap">
                                        <div class="timeline_project-ico">
                                            <div class="project-planet-5"></div>
                                        </div>
                                        <div class="timeline_project">
                                            <div class="timeline_project-box">
                                                Фильм об&nbsp;Алтае<span class="ensp"> </span> <span class="laquo">&laquo;</span>Счастливые люди<span class="raquo">&raquo;</span><span class="ensp"> </span> собрал 4&nbsp;905&nbsp;250&nbsp;рублей!
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="timeline_i">
                                <div class="timeline_date">
                                    8 сентября 2014
                                </div>
                                <div class="timeline_event">
                                    <div class="timeline_title">
                                        Запуск раздела<span class="ensp"> </span> <span class="laquo">&laquo;</span>Магазин<span class="raquo">&raquo;</span><span class="ensp"> </span> на&nbsp;Planeta.ru
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>



        <div id="wrap-4" class="year-wrap">
            <div class="year">
                <div class="year-cont">
                    2015
                </div>
            </div>



            <div class="page" id="page-4">
                <div class="page-cont">
                    <div class="page-wrap wrap">

                        <div class="timeline">
                            <div class="timeline_i">
                                <div class="timeline_date">
                                    7 апреля 2015
                                </div>
                                <div class="timeline_event">
                                    <div class="timeline_title">
                                        Первый звонок в&nbsp;Школе краудфандинга Planeta.ru
                                    </div>
                                </div>
                            </div>

                            <div class="timeline_i">
                                <div class="timeline_date">
                                    7 апреля 2015
                                </div>
                                <div class="timeline_event timeline_event__md">
                                    <div class="timeline_project-wrap">
                                        <div class="timeline_project-ico">
                                            <div class="project-planet-4"></div>
                                        </div>
                                        <div class="timeline_project">
                                            <div class="timeline_project-box">
                                                Книга<span class="ensp"> </span> <span class="laquo">&laquo;</span>Резные наличники: центральный регион<span class="raquo">&raquo;</span><span class="ensp"> </span> собрала 2&nbsp;083&nbsp;384&nbsp;рублей!
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="timeline_i">
                                <div class="timeline_date">
                                    7 июня 2015
                                </div>
                                <div class="timeline_event">
                                    <div class="timeline_title">
                                        Днём российского краудфандинга назван день рождения<span class="ensp"> </span> <span class="laquo">&laquo;</span>Планеты&raquo;!
                                    </div>
                                </div>
                            </div>

                            <div class="timeline_i">
                                <div class="timeline_date">
                                    5 октября 2015
                                </div>
                                <div class="timeline_event">
                                    <div class="timeline_title">
                                        300 000 000 рублей!
                                    </div>
                                </div>
                            </div>

                            <div class="timeline_i">
                                <div class="timeline_date">
                                    26 ноября 2015
                                </div>
                                <div class="timeline_event">
                                    <div class="timeline_title">
                                        Запущено 5 000 проектов!
                                    </div>
                                </div>
                            </div>

                            <div class="timeline_i">
                                <div class="timeline_date">
                                    22 декабря 2015
                                </div>
                                <div class="timeline_event timeline_event__md">
                                    <div class="timeline_project-wrap">
                                        <div class="timeline_project-ico">
                                            <div class="project-planet-6"></div>
                                        </div>
                                        <div class="timeline_project">
                                            <div class="timeline_project-box">
                                                Новые песни группы<span class="ensp"> </span> <span class="laquo">&laquo;</span>Аквариум<span class="raquo">&raquo;</span><span class="ensp"> </span> и&nbsp;БГ&nbsp;собрали 7&nbsp;303&nbsp;803&nbsp;рублей!
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>



        <div id="wrap-5" class="year-wrap">
            <div class="year">
                <div class="year-cont">
                    2016
                </div>
            </div>



            <div class="page" id="page-5">
                <div class="page-cont">
                    <div class="page-wrap wrap">

                        <div class="timeline">
                            <div class="timeline_i">
                                <div class="timeline_date">
                                    5 февраля 2016
                                </div>
                                <div class="timeline_event">
                                    <div class="timeline_title">
                                        Запущено 6 000 проектов!
                                    </div>
                                </div>
                            </div>

                            <div class="timeline_i">
                                <div class="timeline_date">
                                    16 марта 2016
                                </div>
                                <div class="timeline_event timeline_event__md">
                                    <div class="timeline_project-wrap">
                                        <div class="timeline_project-ico">
                                            <div class="project-planet-1"></div>
                                        </div>
                                        <div class="timeline_project">
                                            <div class="timeline_project-box">
                                                Старт <nobr>офлайн-проекта</nobr><span class="ensp"> </span> <span class="laquo">&laquo;</span>Первая Всероссийская &bdquo;Школа краудфандинга&ldquo;&raquo;
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="timeline_i">
                                <div class="timeline_date">
                                    6 апреля 2016
                                </div>
                                <div class="timeline_event">
                                    <div class="timeline_title">
                                        400 000 000 рублей!
                                    </div>
                                </div>
                            </div>

                            <div class="timeline_i">
                                <div class="timeline_date">
                                    7 июня 2016
                                </div>
                                <div class="timeline_event timeline_event__md">
                                    <div class="timeline_project-wrap">
                                        <div class="timeline_project-ico">
                                            <div class="project-planet-2"></div>
                                        </div>
                                        <div class="timeline_project">
                                            <div class="timeline_project-box">
                                                Фильм<span class="ensp"> </span> <span class="laquo">&laquo;</span>Empire V<span class="raquo">&raquo;</span><span class="ensp"> </span> по&nbsp;роману Виктора Пелевина собрал 7&nbsp;755&nbsp;272&nbsp;рублей!
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>



        <div class="page" id="page-6">
            <div class="page-cont">
                <div class="page-wrap wrap">

                    <div class="timeline timeline__final">
                        <div class="timeline_i">
                            <div class="timeline_date"></div>
                            <div class="timeline_event">
                                <div class="timeline_final">
                                    То ли еще будет!
                                </div>
                            </div>
                        </div>

                        <div class="timeline_i">
                            <div class="timeline_date"></div>
                            <div class="timeline_event">
                                <div class="timeline_share">
                                    <div class="timeline_share-head">
                                        Поделиться в социальных сетях
                                    </div>

                                    <div class="share_cont">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="timeline_i">
                            <div class="timeline_date"></div>
                            <div class="timeline_event">
                                <div class="read-blog">
                                    Читайте об истории краудфандинга в России – в <a href="https://medium.com/@planetaru">блоге «Планеты»</a>
                                </div>
                            </div>
                        </div>
                    </div>




                    <div class="planeta-menu">
                        <div class="planeta-menu_list wrap">
                            <div class="planeta-menu_i">
                                <a href="https://planeta.ru/about">О Planeta.ru</a>
                            </div>
                            <div class="planeta-menu_i">
                                <a href="https://planeta.ru/search/projects">Проекты</a>
                            </div>
                            <div class="planeta-menu_i">
                                <a href="https://planeta.ru/welcome/bonuses.html">VIP Клуб</a>
                            </div>
                            <div class="planeta-menu_i">
                                <a href="https://school.planeta.ru/">Школа</a>
                            </div>
                            <div class="planeta-menu_i">
                                <a href="https://shop.planeta.ru/">Магазин</a>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>


        </body>
</html>
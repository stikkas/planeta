<%--
  Date: 18.06.14
  Time: 13:31
--%>
<%@ page contentType="text/html; charset=UTF-8" language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="hf" uri="http://planeta.ru/taglibs/HelperFunctions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<!doctype html>
<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title>Смотрите как сотни людей собирают миллионы прямо сейчас!</title>
    <meta name="description" content="Краудфандинг в России ежемесячно собирает миллионы рублей. Следите, как люди из вашего города меняют мир!"/>

    <meta property="og:title" content="Смотрите как сотни людей собирают миллионы прямо сейчас!"/>
    <meta property="og:description" content="Краудфандинг в России ежемесячно собирает миллионы рублей. Следите, как люди из вашего города меняют мир!"/>
    <meta property="og:image" content="https://s2.planeta.ru/i/f7194/1455623252758_renamed.jpg"/>

    <meta name="viewport" content="width=1024">

    <%@include file="/WEB-INF/jsp/includes/generated/common-favicon.jsp" %>

    <link rel="stylesheet" href="//${hf:getStaticBaseUrl("")}/css/million/odometer.css">
    <link rel="stylesheet" href="//${hf:getStaticBaseUrl("")}/css/million/style.css">

    <script type="text/javascript" src="//${hf:getStaticBaseUrl("")}/js/lib/jquery-1.8.3.min.js"></script>
    <script type="text/javascript" src="//${hf:getStaticBaseUrl("")}/js/lib/odometer/odometer.js"></script>
    <%@include file="/WEB-INF/jsp/includes/generated/stat-counters-head.jsp"%>

    <script type="text/javascript" src="https://maps.google.com/maps/api/js?key=AIzaSyAcNhAbtfdB6fkVVs7BoaIoW5HEPjAn4Mw&v=3.31"></script>
    <script type="text/javascript" src="//${hf:getStaticBaseUrl("")}/js/lib/richmarker.min.js"></script>

    <script type="text/javascript">
        $(document).ready(function() {
            var shareData = {
                counterEnabled: false,
                hidden: false,
                parseMetaTags: true,
                url: location.href
            };
            shareData.className = 'donate-sharing sharing-mini';
            $('.share-cont-horizontal').share(shareData);


            setInterval(function() {
                var sharingTitle = $('.header-sharing_title');
                sharingTitle.addClass('tada');
                setTimeout(function() {
                    sharingTitle.removeClass('tada');
                }, 800);
            }, 4000);
        });
    </script>

    <script type="text/javascript" src="//${hf:getStaticBaseUrl("")}/js/utils/plugins/share-widget.js"></script>


    <script type="text/javascript">
        (function ($) {
            var MAX_POINTS_COUNT = 10;
            var cities = [<c:forEach items="${cities}" var="c" varStatus="i">{name:'${c.name}',lat:${c.lat}, lng:${c.lng}}<c:if test="${!i.last}">,</c:if></c:forEach>];
            var orders = [<c:forEach items="${orders}" var="o" varStatus="i">{price: ${o.price},name:'<c:out value="${o.name}"/>',id:${o.id}}<c:if test="${!i.last}">,</c:if></c:forEach>];
            var indexes = [];
            for (var i = 0; i < cities.length; ++i) {
                var c = cities[i];
                switch (c.name) {
                    case 'Москва':
                        for (var j = 0; j < 20; ++j) {
                            indexes.push(i);
                        }
                        break;
                    case 'Санкт-Петербург':
                        for (j = 0; j < 15; ++j) {
                            indexes.push(i);
                        }
                        break;
                    default:
                        indexes.push(i);
                }
            }
            var map;
            var collected = ${collected};
            var od;
            var centers = [[56.0297247, 42.7506858], [53.5297247, 46.7506858]];
            function init() {
                var center = $(window).width()  < 1400 ? centers[0] : centers[1];
                var mapCenter = new google.maps.LatLng(center[0], center[1]);
                map = new google.maps.Map(document.getElementById('map'), {
                    scrollwheel: false,
                    scaleControl: false,
                    draggable: false,
                    disableDoubleClickZoom: true,
                    disableDefaultUI: true,
                    zoom: 5,
                    center: mapCenter,
                    mapTypeId: google.maps.MapTypeId.ROADMAP
                });
            }

            google.maps.event.addDomListener(window, 'load', init);

            var RewardPoints = {
                list: [],
                add: function(options) {
                    var pointBlock = '' +
                            '<div class="point-reward animate bounceInDown">' +
                            '<div class="reward-price">' +
                            '<span class="reward-price-in">' +
                            options.price +
                            ' <span class="b-rub">Р</span>' +
                            '</span>' +
                            '</div>' +
                            '<div class="reward-name">' +
                            '<a class="reward-name-link" href="/campaigns/' + options.objectId + '" target="_blank">' +
                            '<span class="reward-name-in">' +
                            options.name +
                            '</span>' +
                            '</a>' +
                            '</div>' +
                            '</div>';

                    var point = new RichMarker({
                        map: map,
                        position: options.coord,
                        draggable: false,
                        flat: true,
                        anchor: RichMarkerPosition.BOTTOM_LEFT,
                        content: pointBlock,
                        city: options.city
                    });
                    for (var i = 0; i < this.list.length; ++i) {
                        var p = this.list[i];
                        if (p.city == point.city) {
                            this.remove(p);
                        }
                    }
                    this.list.push(point);

                    if (this.list.length > MAX_POINTS_COUNT) {
                        this.remove();
                    }
                },
                remove: function(point) {
                    var removed;
                    if (point) {
                        var index = RewardPoints.list.indexOf(point);
                        if (index > -1) {
                            removed = point;
                            RewardPoints.list.splice(index, 1);
                        }
                    }
                    if (!removed){
                        removed = RewardPoints.list.shift();
                    }
                    if (removed) {
                        removed.onRemove();
                    }
                }
            };

            var addMarker = function () {
                var city = cities[indexes[Math.floor(Math.random() * indexes.length)]];
                var order = orders[Math.floor(Math.random() * orders.length)];
                var point = {
                    coord: new google.maps.LatLng(city.lat, city.lng),
                    price: Math.floor(order.price),
                    name: order.name,
                    objectId: order.id,
                    city: city.name
                };
                RewardPoints.add(point);

                setTimeout(addMarker, (Math.random() * 5 + 2) * 1000);
            };
            $(function () {
                od = new Odometer({
                    el: $('.million-watch_val')[0],
                    value: collected,
                    format: '( ddd)'
                });

                if (orders.length > 0 && cities.length > 0) {
                    setTimeout(addMarker, 300);
                }
            });
        })(jQuery);

    </script>
</head>

<body>
<div id="map"></div>

<div class="million-watch">
        <span class="million-watch_fill">Наблюдаем,<br>
        как краудфандинг<br>
        в России<br>
        перешагнул<br>
        <span class="million-watch_val">900 000 000</span><br>
        рублей</span>
</div>



<div class="header">
    <div class="header-cont">
        <a class="logo" href="/"></a>

        <div class="header-sharing">
            <div class="header-sharing_title animate">Поделиться!</div>

            <div class="share-cont-horizontal"></div>
        </div>
    </div>
</div>

<div class="footer">
    <div class="footer-cont">
        <div class="copy">&copy; 2018 <a href="/">Planeta.ru</a></div>
        <div class="success-link">
            Не забудьте почитать грандиозные <a href="/stories/">истории успеха</a> проектов Планеты
        </div>
    </div>
</div>
</body>
</html>
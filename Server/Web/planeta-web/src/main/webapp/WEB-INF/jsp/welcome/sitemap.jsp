<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ include file="/WEB-INF/jsp/includes/generated/doctype-header.jsp" %>
<head>
    <title>Планета: Карта сайта</title>
    <%@include file="/WEB-INF/jsp/includes/generated/stat-counters-async.jsp"%>
    <%@include file="/WEB-INF/jsp/includes/common-css.jsp" %>
    <%@include file="/WEB-INF/jsp/includes/generated/common-templates.jsp" %>
    <%@include file="/WEB-INF/jsp/includes/planeta-js.jsp" %>
</head>

<body>
<%@ include file="/WEB-INF/jsp/includes/generated/header.jsp" %>
<div id="global-container">
    <div id="main-container" class="wrap-container">

<div class="wrap">
            <div class="col-12">

                <div class="sitemap">

                    <div class="top-breads out-container">
                        <div class="tb-top">
                            <div class="tbt-link">
                                <div class="tbt-link-item"><a href="/">Planeta.ru</a></div>
                                <div class="tbt-link-item item-sep"></div>
                                <div class="tbt-link-item"><span>Карта сайта</span></div>
                            </div>
                        </div>
                    </div>


                    <div class="cf">
                        <div class="col-3">
                            <div class="section-title">Сервис</div>
                            <ul>
                                <li><a href="/contacts">Контактная информация </a></li>
                                <li><a href="/faq/article/10!paragraph80">Возврат оплаты</a></li>
                                <li><a href="/faq">Вопросы и ответы</a></li>
                                <li><a href="/faq/article/8!paragraph53">Способы оплаты</a></li>
                            </ul>
                            <div class="section-title">Моя учетная запись</div>
                            <ul>
                                <li><a href="/account">Личный кабинет</a></li>
                                <li><a href="/settings">Настройки профиля</a></li>
                            </ul>
                        </div>
                        <div class="col-3">
                            <div class="section-title">О нас</div>
                            <ul>
                                <li><a href="/about">О Planeta.ru</a></li>
                                <li><a href="/partners">Наши партнеры</a></li>
                                <li><a href="/welcome/sitemap.html">Карта сайта</a></li>
                            </ul>
                        </div>
                        <div class="col-3">
                            <div class="section-title">Дополнительно</div>
                            <ul>
                                <li><a href="/crowdfunding.html">Краудфандинг: что это?</a></li>
                                <li><a href="https://${properties["affiliates.application.host"]}/main.html">Партнерская программа</a></li>
                                <li><a href="https://${properties["mobile.application.host"]}">Мобильная версия</a></li>
                                <li><a href="https://${properties["tv.application.host"]}">Онлайн трансляции</a></li>
                                <li><a href="https://${properties["shop.application.host"]}">Магазин Планеты</a></li>
                                <li><a href="/welcome/agreement.html">Пользовательское соглашение</a></li>
                                <li><a href="/welcome/projects-agreement.html">Пользовательское соглашение сервиса "Краудфандинг"</a></li>
                                <li><a href="/welcome/private-info.html">Положение о личной информации</a></li>
                            </ul>
                        </div>
                        <div class="col-3">
                            <div class="section-title">Проекты</div>
                            <ul>
                                <c:forEach var= "tag" items="${campaignTags}">
                                    <li><a href="https://${properties['application.host']}/search/projects?query=&categories=${tag.mnemonicName}">${tag.name}</a></li>
                                </c:forEach>
                            </ul>
                        </div>
                    </div>


                </div>

            </div>
        </div>  </div>
    </div>
</div>

<%@ include file="/WEB-INF/jsp/includes/generated/footer.jsp" %>
</body>
</html>
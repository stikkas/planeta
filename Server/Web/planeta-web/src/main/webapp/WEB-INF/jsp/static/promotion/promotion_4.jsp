<%@ include file="/WEB-INF/jsp/includes/generated/doctype-header.jsp" %>
<head>
    <%@include file="/WEB-INF/jsp/includes/generated/stat-counters-async.jsp"%>
    <%@include file="head.jsp"%>
    <title>Руководство по продвижению на этапе 100%+</title>
</head>
<body class="project-promotion-page">
<%@ include file="/WEB-INF/jsp/includes/generated/irma/header.jsp" %>
<%@ include file="/WEB-INF/jsp/includes/generated/irma/main-nav.jsp" %>

<div id="global-container">
    <div id="main-container" class="wrap-container">
        <div id="center-container">


            <div class="project-promotion_head">Руководство по продвижению на этапе 100%+</div>


            <div class="project-promotion_top">
                <div class="wrap">
                    <div class="col-12">

                        <div class="project-promotion_top_cont">
                            <div class="project-promotion_top_ico s-promotion-top-flag"></div>
                            <div class="project-promotion_top_text">
                                Этот этап можно назвать выходом на финишную прямую. Хотите не просто собрать необходимую сумму, но и перебрать ее? Громко заявить о своем проекте и пойти на рекорд? Сейчас для этого самое время!
                            </div>
                        </div>

                    </div>
                </div>
            </div>


            <div class="project-promotion_cont">
                <div class="wrap">
                    <div class="col-12">

                        <div class="project-promotion_list">
                            <div class="project-promotion_list-i">
                                <div class="project-promotion_list-ico s-promotion-list-vk"></div>
                                <div class="project-promotion_list-head">
                                    Личные интернет-ресурсы - это ваш персональный сайт, паблики и личные страницы в
                                    социальных сетях, а также собственные блоги. Вы также можете создавать личные
                                    страницы специально под проект, используя все перечисленные выше площадки.
                                </div>
                                <div class="project-promotion_list-text">
                                    <span class="step-hl">Шаг 1:</span> Выпустите <a href="http://vk.com/anacondaz?w=wall-2736916_44938">новость</a> об успешном сборе и
                                    <a href="https://planeta.ru/lakmus/blog/120956">поблагодарите всех спонсоров проекта</a> за их поддержку. Не стесняйтесь говорить о
                                    своих успехах.
                                    <br>
                                    <br>
                                    <span class="step-hl">Шаг 2:</span> Определите <a href="https://planeta.ru/zhelannaja/blog/122264">сверхцель</a> (на что пойдут деньги,
                                    собранные сверх суммы) с акцентом на продолжении сбора до истечения срока проекта
                                </div>
                            </div>

                            <div class="project-promotion_list-i">
                                <div class="project-promotion_list-ico s-promotion-list-msg"></div>
                                <div class="project-promotion_list-head">
                                    Рассылки — это информация, которую вы распространяете по друзьям, коллегам,
                                    родственникам и партнерам, используя личные сообщения в соцсетях, e-mail письма,
                                    телефонные звонки и sms-сообщения.
                                </div>
                                <div class="project-promotion_list-text">
                                    <span class="step-hl">Шаг 1:</span> Сделайте рассылку с приятным бонусом для ваших
                                    спонсоров и партнеров. Это может быть первая песня с альбома, обложка вашей будущей
                                    книги, первая серия фильма или что-то еще. Если же ваш проект еще в стадии
                                    реализации, то вы можете просто порадовать спонсоров фотографиями с бэкстейджа или
                                    <a href="https://planeta.ru/torbanakruche/blog/124157">новостями о том, как идет работа над проектом</a>.
                                </div>
                            </div>

                            <div class="project-promotion_list-i">
                                <div class="project-promotion_list-ico s-promotion-list-media"></div>
                                <div class="project-promotion_list-head">
                                    Сообщества и СМИ – это тематические блоги, паблики, сообщества в Facebook,
                                    ВКонтакте, Живом Журнале, Твиттере, а также близкие вашему проекту по тематике
                                    средства массовой информации
                                </div>
                                <div class="project-promotion_list-text">
                                    <span class="step-hl">Шаг 1:</span> Подготовьте <a href="http://te-st.ru/2014/03/17/volunteers-playing-with-fire-project/">материал для СМИ</a> об опыте
                                    краудфандинга.
                                    <br>
                                    <br>
                                    <span class="step-hl">Шаг 2:</span> Поделитесь своими знаниями со спонсорами и
                                    пользователями «Планеты». Возможно, кто-то, вдохновившись вашей историей, тоже
                                    поверит в свой успех. Используйте для этого раздел <a href="https://planeta.ru/stories/">«Истории успеха»</a> или блог
                                    Планеты, а также социальные сети.
                                </div>
                            </div>

                            <div class="project-promotion_list-i">
                                <div class="project-promotion_list-ico s-promotion-list-person"></div>
                                <div class="project-promotion_list-head">
                                    Лидеры мнений — это известные и уважаемые личности со сложившейся большой аудиторией
                                    (в том числе, и сетевой): общественные деятели, блогеры, журналисты, акулы бизнеса.
                                </div>
                                <div class="project-promotion_list-text">
                                    <span class="step-hl">Шаг 1:</span> Расскажите, кто за время существования проекта
                                    успел вас поддержать. Попросите расшарить эту информацию через <a href="https://vk.com/wall-52900363?own=1&amp;z=photo-52900363_316422263%2Falbum-52900363_00%2Frev">социальные сети</a> и
                                    личные страницы. Гордитесь своими «знаменитыми» спонсорами.
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>


        </div>
    </div>
</div>


<%@ include file="/WEB-INF/jsp/includes/generated/irma/footer.jsp" %>
</body>
</html>
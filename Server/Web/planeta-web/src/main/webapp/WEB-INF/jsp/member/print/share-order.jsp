<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="java.io.PrintWriter" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@include file="/WEB-INF/jsp/includes/generated/doctype-header.jsp" %>
<c:set var="baseUrl" value="${hf:getStaticBaseUrl('')}"/>
<head>
    <title>Planeta: Информация о заказе</title>
    <meta name="title" content="Planeta: Информация о заказе">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta charset="utf-8">

    <link type="text/css" rel="stylesheet" href="//${baseUrl}/css-generated/common.css"/>
    <link type="text/css" rel="stylesheet" href="//${baseUrl}/css-generated/pln-print.css"/>

    <script type="text/javascript">
        function printDocument() {
            setTimeout(function() {window.print();}, 1000);
        }
    </script>
    <style>
        .page{
            width: 210mm;
            min-height: 297mm;
            margin: 0 auto;
        }

        html, body {
            display: block;
            min-width: 0;
            min-height: 0;
            height: auto;
        }

        /*@page {*/
            /*size: A4;*/
            /*margin: 0 auto;*/
        /*}*/


        /*@media print {*/
            /*html, body {*/
                /*width: 210mm;*/
                /*height: 297mm;*/
                /*margin: auto;*/
            /*}*/


            /*.page {*/
                /*margin: 0 auto;*/
                /*border: initial;*/
                /*border-radius: initial;*/
                /*width: initial;*/
                /*min-height: initial;*/
                /*box-shadow: initial;*/
                /*background: initial;*/
                /*page-break-after: always;*/
            /*}*/
        /*}*/
    </style>
</head>
<body class="coupon-page" onload="printDocument()">
<!-- Coupon-print -->
<c:forEach items="${orderObjects}" var="orderObject" varStatus="varStatus">
<div class="page">
    <div class="coupon-print">
        <div class="coupon-print-item clearfix">
            <div class="logo">
                <img src="//${baseUrl}/images/print-logotip.png" alt="print-logotip.png">
            </div>
        </div>
        <div class="coupon-print-item clearfix">
            <div class="info">
                <h1 style="text-align: left;">${share.name}</h1>
                <h2><c:out value="${campaign.name}"/></h2>

                <c:if test="${not empty share.estimatedDeliveryTime}">
                    <div class="info-delivery-date">
                        Ориентировочная дата доставки вознаграждения: <fmt:formatDate pattern="dd.MM.yyyy" value="${share.estimatedDeliveryTime}"/>
                    </div>
                </c:if>

                <h3><c:out value="${group.displayName}"/></h3>
                <div class="info-item clearfix">
                    <span class="price">
                        Стоимость:
                        <b><fmt:formatNumber maxFractionDigits="2" minFractionDigits="0" value="${order.totalPrice}"/> руб.</b>
                    </span>
                    <span>
                        Дата продажи:
                        <b><fmt:formatDate pattern="dd.MM.yyyy" value="${order.timeAdded}"/></b>
                    </span>
                    <span>
                        Покупатель:
                        <b><c:out value="${profile.displayName}"/></b>
                    </span>
                    <span class="number">
                        Номер заказа:
                        <b>${hf:generateOrderNumber(orderObject)}</b>
                    </span>
                </div>
            </div>
        </div>
        <div class="coupon-print-item clearfix">
            <div class="description">
                <div class="descr">
                    <c:out value="${description}" escapeXml="false"/>
                </div>
            </div>
        </div>

        <c:if test="${order.deliveryType != \"NOT_SET\"}">
        <div class="coupon-print-item clearfix">
            <div class="address">
                <table class="table table-bordered">
                   <tbody><tr>
                       <th>наименование</th>
                       <th>цена</th>
                       <th>количество</th>
                       <th>стоимость</th>
                   </tr>
                       <tr>
                           <td><c:out value="${share.name}"/></td>
                           <td>${share.price} руб.</td>
                           <td>${orderObject.count} шт.</td>
                           <td>${share.price * orderObject.count} руб.</td>
                       </tr>
                   <c:if test="${order.deliveryPrice > 0}">
                       <tr>
                           <td>
                           <c:choose>
                               <c:when test="${order.deliveryType == \"CUSTOMER_PICKUP\"}">
                                   Самовывоз
                               </c:when>
                               <c:when test="${order.deliveryType == \"DELIVERY\" || order.deliveryType == \"RUSSIAN_POST\"}">
                                   Доставки
                               </c:when>
                           </c:choose>
                           </td>
                           <c:set var="deliveryType" value="${order.deliveryType}" scope="request"/>
                           <td><fmt:formatNumber value="${order.deliveryPrice}" type="number"/> руб.</td>
                           <td></td>
                           <td><fmt:formatNumber value="${order.deliveryPrice}" type="number"/> руб.</td>
                       </tr>
                   </c:if>
                   </tbody>
                </table>
                <table class="table order-address-table">
                    <colgroup>
                        <col width="140">
                        <col width="100%">
                        <col width="140">
                    </colgroup>
                    <tbody>
                    <tr>
                        <c:choose>
                            <c:when test="${order.deliveryType == \"CUSTOMER_PICKUP\"}">
                                <td>
                                    Адрес самовывоза:
                                </td>
                                <td><b><c:out value="${deliveryAddress.address}"/></b>
                                </td>
                            </c:when>
                            <c:when test="${order.deliveryType == \"DELIVERY\" || order.deliveryType == \"RUSSIAN_POST\"}">
                                <td>
                                    Адрес доставки:
                                </td>
                                <td><b><c:if test="${not empty deliveryAddress.zipCode}"><c:out value="${deliveryAddress.zipCode}"/>, </c:if>
                                    <c:out value="${deliveryAddress.city}"/>, <c:out value="${deliveryAddress.address}"/>, <c:out value="${deliveryAddress.phone}"/></b>
                                </td>
                            </c:when>
                        </c:choose>
                    </tr>
                    <tr>
                        <c:if test="${not empty deliveryPublicNote}">
                        <td>
                            Примечание:
                        </td>
                        <td>
                            <c:out value="${deliveryPublicNote}"/>
                        </td>
                        </c:if>
                    </tr>
                    <c:if test="${order.deliveryType == \"DELIVERY\" || order.deliveryType == \"RUSSIAN_POST\"}">
                    <tr>
                        <td>
                            Получатель:
                        </td>
                        <td>
                            <c:out value="${deliveryAddress.fio}"/>
                        </td>
                    </tr>
                    </c:if>
                    <c:catch var="e">
                        <c:if test="${order.cashOnDeliveryCost != null && order.cashOnDeliveryCost > 0}">
                            <tr>
                                <td>
                                    Наложенный платёж:
                                </td>
                                <td>
                                    <fmt:formatNumber value="${order.cashOnDeliveryCost}" type="number"/> руб.
                                </td>
                            </tr>
                        </c:if>
                        <c:if test="${not empty order.trackingCode}">
                            <tr>
                                <td>
                                    Почтовый идентификатор:
                                </td>
                                <td>
                                    <c:out value="${order.trackingCode}"/>

                                </td>
                            </tr>
                        </c:if>
                        <c:if test="${not empty order.deliveryComment}">
                            <tr>
                                <td>
                                    Комментарий:
                                </td>
                                <td>
                                    <c:out value="${order.deliveryComment}"/>

                                </td>
                            </tr>
                        </c:if>
                    </c:catch>
                    <c:if test="${e != null}">
                        <!--
                        <%
                            Exception e = (Exception) pageContext.getAttribute("e");
                            e.printStackTrace(new PrintWriter(out));
                        %>
                        -->
                    </c:if>

                    </tbody>
                </table>

            </div>
        </div>
        </c:if>
        <c:if test="${not empty share.rewardInstruction || isCancelled}">
            <div class="coupon-print-item clearfix">
                <div class="description">
                    <div class="descr">
                        <c:choose>
                            <c:when test="${isCancelled}">
                                <h5>Вознаграждение аннулировано.</h5>
                            </c:when>
                            <c:otherwise>
                                <h5>Инструкция для получения вознаграждения:</h5>
                                <p>${share.rewardInstruction}</p>
                            </c:otherwise>
                        </c:choose>
                    </div>
                </div>
            </div>
        </c:if>
        <div class="coupon-print-item clearfix">
            <div class="pull-right">
                <img src="/api/profile/barcode.html?orderId=${orderObject.orderId}&orderObjectId=${orderObject.orderObjectId}" alt="barcode">
            </div>
        </div>
    </div>
</div>
</c:forEach>
</body>
</html>
<%@ include file="/WEB-INF/jsp/includes/generated/doctype-header.jsp" %>
<head>
    <c:set var="pageType" value="book"/>
    <c:set var="eventLabel" value="quickbook_get_recommendation"/>
    <%@ include file="/WEB-INF/jsp/static/quickstart-new/includes/quickstart-header.jsp" %>
</head>

<body class="land_var2">
    <div class="header">
        <a class="logo" href="https://planeta.ru/"></a>

        <a class="btn btn-primary header_create-btn" href="/funding-rules">Создать проект</a>
    </div>

    <div class="video-lead var2">
        <div class="wrap">
            <div class="video-lead_head">Хотите издать книгу?</div>
            <div class="video-lead_block cf">
                <div class="video-lead_video">
                    <div class="embed-video">
                        <iframe width="560" height="315" src="//www.youtube.com/embed/5XkyLMsoyNQ" frameborder="0" allowfullscreen></iframe>
                    </div>
                </div>
                <div class="video-lead_cont">
                    <div class="video-lead_text">
                       У вас есть сюжет или готовый материал для книги?
                        Для реализации задуманного не обязательно искать
                         инвесторов – Вам помогут будущие читатели! Интересно?
                         Тогда не стесняйтесь и поделитесь своими замыслами с другими!
                       </div>

                    <div class="video-lead_action-text">
                        Получите видеозапись вебинара и&nbsp;руководство &laquo;Как издать книгу с&nbsp;помощью
                        краудфандинга&raquo;
                    </div>

                    <div class="video-lead_action">
                        <div class="fieldset pln-dropdown js-open-error">
                            <input class="form-control js-get-book-email" type="text" placeholder="Введите ваш e-mail">
                            <div class="page-control_popup pln-d-popup">
                                <div class="pln-d-popup-tail"><div></div></div>
                                <div class="d-popup-cont">
                                    Вы ввели неверный адрес электронной почты
                                </div>
                            </div>
                        </div>

                        <div class="video-lead_action_btn pln-dropdown js-get-book-btn-cnt" >
                            <button class="btn btn-primary js-get-book-btn" onclick="getBook()">Получить!</button>
                            <div class="page-control_popup pln-d-popup">
                                <div class="pln-d-popup-tail"><div></div></div>
                                <nobr class="d-popup-cont">
                                    Спасибо за регистрацию!<br>
                                    Вам отправлено руководство и ссылка на видеозапись
                                </nobr>
                            </div>
                        </div>
                    </div>

                    <div class="video-lead_secondary-text">
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="promo-lead var2">
        <div class="wrap">
            <div class="promo-lead_head">Кому это может быть полезно</div>
            <div class="promo-lead_block cf">

                <div class="promo-lead_list">
                    <div class="promo-lead_i">
                        <div class="promo-lead_i_img">
                            <img src="//${hf:getStaticBaseUrl("")}/images/quickstart/promo-lead-img-1-2.png">
                        </div>
                        <div class="promo-lead_i_head">
                            Писателю
                        </div>
                        <div class="promo-lead_i_text">
                            На&nbsp;&laquo;Планете&raquo; вы&nbsp;не&nbsp;только сможете проверить свой художественный
                            замысел на&nbsp;прочность и&nbsp;завоевать новых читателей, но&nbsp;и&nbsp;найти новые идеи
                            в&nbsp;общении с&nbsp;аудиторией. Неожиданные сюжетные ходы, новые персонажи и&nbsp;целые
                            вселенные&nbsp;&mdash; возьмите поклонников вашего творчества в&nbsp;соавторы!
                        </div>
                    </div>
                    <div class="promo-lead_i">
                        <div class="promo-lead_i_img">
                            <img src="//${hf:getStaticBaseUrl("")}/images/quickstart/promo-lead-img-2-2.png">
                        </div>
                        <div class="promo-lead_i_head">
                            Издателю
                        </div>
                        <div class="promo-lead_i_text">
                            Продажа книг&nbsp;&mdash; сложный бизнес, и&nbsp;не&nbsp;всегда издание даже
                            высококачественной литературы окупает свои затраты. На&nbsp;&laquo;Планете&raquo;
                            вы&nbsp;сможете найти множество читателей, которые будут рады приобрести будущую книгу,
                            поддержав ее&nbsp;выпуск рублем&nbsp;&mdash; а&nbsp;значит, вы&nbsp;сможете сделать такие
                            издания на&nbsp;сто процентов окупаемыми!
                        </div>
                    </div>
                    <div class="promo-lead_i">
                        <div class="promo-lead_i_img">
                            <img src="//${hf:getStaticBaseUrl("")}/images/quickstart/promo-lead-img-3-2.png">
                        </div>
                        <div class="promo-lead_i_head">
                            Автору комиксов
                        </div>
                        <div class="promo-lead_i_text">
                            Выпускать комиксы гораздо дороже, чем обычные книги, ведь для этого требуется
                            высококачественная полноцветная печать. Взять на&nbsp;себя такие затраты по&nbsp;плечу
                            немногим&nbsp;&mdash; но&nbsp;с&nbsp;поддержкой будущих читателей это невероятно просто!
                            А&nbsp;главное, вы&nbsp;обретете постоянную аудиторию, с&nbsp;нетерпением ждущую новых выпусков!
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <%@ include file="/WEB-INF/jsp/static/quickstart-new/includes/quickstart-inner.jsp" %>
    <div class="author-slider">
        <div class="author-slider_head">Отзывы авторов проектов</div>

        <div class="author-slider_head-wrap">
            <div class="author-slider_head-list uninit">

                <div class="author-slider_head-i">
                    <div class="author-slider_head-name">
                        <div class="author-slider_head-name-text">
                            <span class="cur-p">Иван Хафизов</span>
                        </div>
                    </div>
                    <div class="author-slider_head-ava"><img src="//${hf:getStaticBaseUrl("")}/images/quickstart/author/hafizov.jpg" width="144" height="144"></div>
                </div>

                <div class="author-slider_head-i">
                    <div class="author-slider_head-name">
                        <div class="author-slider_head-name-text">
                            <span class="cur-p">Михаил Ратгауз</span>
                        </div>
                    </div>
                    <div class="author-slider_head-ava"><img src="//${hf:getStaticBaseUrl("")}/images/quickstart/author/ratgauz.jpg" width="144" height="144"></div>
                </div>

                <div class="author-slider_head-i">
                    <div class="author-slider_head-name">
                        <div class="author-slider_head-name-text">
                            <span class="cur-p">Александра Ливергант</span>
                        </div>
                    </div>
                    <div class="author-slider_head-ava"><img src="//${hf:getStaticBaseUrl("")}/images/quickstart/author/livargan.png" width="144" height="144"></div>
                </div>
                <div class="author-slider_head-i">
                    <div class="author-slider_head-name">
                        <div class="author-slider_head-name-text">
                            <span class="cur-p">Иван Хафизов</span>
                        </div>
                    </div>
                    <div class="author-slider_head-ava"><img src="//${hf:getStaticBaseUrl("")}/images/quickstart/author/hafizov.jpg" width="144" height="144"></div>
                </div>

                <div class="author-slider_head-i">
                    <div class="author-slider_head-name">
                        <div class="author-slider_head-name-text">
                            <span class="cur-p">Михаил Ратгауз</span>
                        </div>
                    </div>
                    <div class="author-slider_head-ava"><img src="//${hf:getStaticBaseUrl("")}/images/quickstart/author/ratgauz.jpg" width="144" height="144"></div>
                </div>

                <div class="author-slider_head-i">
                    <div class="author-slider_head-name">
                        <div class="author-slider_head-name-text">
                            <span class="cur-p">Александра Ливергант</span>
                        </div>
                    </div>
                    <div class="author-slider_head-ava"><img src="//${hf:getStaticBaseUrl("")}/images/quickstart/author/livargan.png" width="144" height="144"></div>
                </div>
            </div>
        </div>


        <div class="author-slider_text-wrap">
            <div class="author-slider_text">

                <div class="author-slider_text-i">
                    <div class="author-slider_text-project">
                        Иван Хафизов, автор книги-энциклопедии о резных наличниках
                    </div>
                    <div class="author-slider_text-text">
                        Успешное народное финансирование скажет о проекте лучше всякого руководства любой компании.
                        Потому что если десять тысяч человек поддержат проект, то на любой вопрос можно смело отвечать — да!
                    </div>
                </div>

                <div class="author-slider_text-i">
                    <div class="author-slider_text-project">
                        Михаил Ратгауз, заместитель главного редактора Colta.ru
                    </div>
                    <div class="author-slider_text-text">
                        Никогда еще читатель не был так близок, никогда еще его привязанность к сайту не носила такие
                        конкретные очертания. Да что говорить, «Кольте» скоро будет год — и ее просто бы не было без сайта Planeta.ru. Спасибо, ребята!
                    </div>
                </div>

                <div class="author-slider_text-i">
                    <div class="author-slider_text-project">
                        Александра Ливергант, продюсер серии книг «История глазами Крокодила. ХХ век»
                    </div>
                    <div class="author-slider_text-text">
                        Когда мы повесили свой проект «История глазами Крокодила. ХХ век» на Planeta.ru, то за первые
                        несколько дней собрали почти сто тысяч. Наблюдать за этим было поразительно: людям не все равно то,
                        что ты делаешь, – они голосуют рублем. Это дорогого стоит - и хотя мы не собрали весь миллион,
                        считаем проект на Planeta удачным и полезным.
                    </div>
                </div>
                <div class="author-slider_text-i">
                    <div class="author-slider_text-project">
                        Иван Хафизов, автор книги-энциклопедии о резных наличниках
                    </div>
                    <div class="author-slider_text-text">
                        Успешное народное финансирование скажет о проекте лучше всякого руководства любой компании.
                        Потому что если десять тысяч человек поддержат проект, то на любой вопрос можно смело отвечать — да!
                    </div>
                </div>

                <div class="author-slider_text-i">
                    <div class="author-slider_text-project">
                        Михаил Ратгауз, заместитель главного редактора Colta.ru
                    </div>
                    <div class="author-slider_text-text">
                        Никогда еще читатель не был так близок, никогда еще его привязанность к сайту не носила такие
                        конкретные очертания. Да что говорить, «Кольте» скоро будет год — и ее просто бы не было без сайта Planeta.ru. Спасибо, ребята!
                    </div>
                </div>

                <div class="author-slider_text-i">
                    <div class="author-slider_text-project">
                        Александра Ливергант, продюсер серии книг «История глазами Крокодила. ХХ век»
                    </div>
                    <div class="author-slider_text-text">
                        Когда мы повесили свой проект «История глазами Крокодила. ХХ век» на Planeta.ru, то за первые
                        несколько дней собрали почти сто тысяч. Наблюдать за этим было поразительно: людям не все равно то,
                        что ты делаешь, – они голосуют рублем. Это дорогого стоит - и хотя мы не собрали весь миллион,
                        считаем проект на Planeta удачным и полезным.
                    </div>
                </div>
            </div>
        </div>
    </div>
    <%@ include file="/WEB-INF/jsp/static/quickstart-new/includes/quickstart-request.jsp" %>
</body>
</html>
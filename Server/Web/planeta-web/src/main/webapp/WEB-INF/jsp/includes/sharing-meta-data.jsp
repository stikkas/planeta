<%@ page contentType="text/html; charset=UTF-8" language="java" pageEncoding="UTF-8" %>

<meta property="og:site_name" content="Planeta.ru"/>
<meta property="og:title" content="<c:out value='${not empty customMetaTag.ogTitle ? customMetaTag.ogTitle : not empty metaTitle ? metaTitle : \"\"}'/>"/>
<meta property="og:description" content="<c:out value='${not empty customMetaTag.ogDescription ? customMetaTag.ogDescription : not empty sharingDescription ? sharingDescription : \"\"}'/>"/>
<meta name="description" content="<c:out value='${not empty customMetaTag.description ? customMetaTag.description : not empty metaDescription ? metaDescription : \"\"}'/>"/>


<c:catch>
    <c:if test="${not empty customMetaTag.imageList}">
        <c:forEach items="${customMetaTag.imageList}" var="image">
            <c:set var="existsImage" value="true"></c:set>
            <meta property="og:image" content="<c:out value='${image}'/>"/>
            <link rel="image_src" href="<c:out value='${image}'/>" />
        </c:forEach>
    </c:if>
</c:catch>
<c:if test="${empty existsImage}">
    <meta property="og:image" content="<c:out value='${not empty customMetaTag.image ? customMetaTag.image : not empty metaImage ? metaImage : \"\"}'/>"/>
    <link rel="image_src" href="<c:out value='${not empty customMetaTag.image ? customMetaTag.image : not empty metaImage ? metaImage : \"\"}'/>" />
</c:if>


<meta property="og:type" content="website"/>
<meta name="robots" content="noyaca, noodp"/>
<link rel="yandex-tableau-widget" href="https://${hf:getStaticBaseUrl('')}/json/yandex-tableau.json">

<script type="application/ld+json">
    {
        "@context": "http://schema.org",
        "@type": "WebSite",
        "url": "https://planeta.ru/",
        "logo": "https://static.planeta.ru/images/pln-logo.svg",
        "potentialAction": {
        "@type": "SearchAction",
        "target": "https://www.google.com/search?q={search_term_string}%20site:planeta.ru",
        "query-input": "required name=search_term_string"
        }
    }
</script>


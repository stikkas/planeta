<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<div class="header">
    <div class="wrap">
        <div class="col-12">
            <a class="logo" href="/">
                <img class="logo-img" src="//${hf:getStaticBaseUrl("")}/images/new_svg_animating/pln-logo_new_css.svg" alt="planeta.ru" width="118" height="31">
            </a>
            <div  class="register-header-info">
                Поздравляем! Вы зарегистрировались на Планете!
            </div>
        </div>
    </div>
</div>

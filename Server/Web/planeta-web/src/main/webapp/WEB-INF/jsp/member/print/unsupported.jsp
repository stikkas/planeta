<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@include file="/WEB-INF/jsp/includes/generated/doctype-header.jsp" %>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Planeta: Информация о заказе</title>
    <meta name="title" content="Planeta: Информация о заказе">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">

    <link type="text/css" rel="stylesheet" href="//${hf:getStaticBaseUrl("")}/css-generated/common.css"/>

</head>
<body class="tickets-page">
    У Вас нет прав для печати выбранного заказа, или печать заказа выбранного типа не поддерживается.
</body>
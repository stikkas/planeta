<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ include file="/WEB-INF/jsp/includes/generated/doctype-header.jsp" %>
<%@ taglib prefix="p" uri="http://planeta.ru/tags" %>

<head>
    <c:choose>
        <c:when test="${not empty customMetaTag.title}">
            <title>${customMetaTag.title}</title>
        </c:when>
        <c:otherwise>
            <title>О проекте Planeta.ru</title>
        </c:otherwise>
    </c:choose>
    <c:choose>
        <c:when test="${not empty customMetaTag.description}">
            <meta name="description" content="${customMetaTag.description}"/>
        </c:when>
        <c:otherwise>
            <meta name="description"
                  content="Что такое онлайн-площадка Planeta.ru?"/>
        </c:otherwise>
    </c:choose>
    <c:if test="${not empty customMetaTag.keywords}">
        <meta name="keywords" content="${customMetaTag.keywords}"/>
    </c:if>

    <meta name="viewport" content="width=device-width"/>
    <%@include file="/WEB-INF/jsp/includes/generated/stat-counters-async.jsp"%>

    <%@include file="/WEB-INF/jsp/includes/common-css.jsp" %>
    <link type="text/css" rel="stylesheet" href="//${hf:getStaticBaseUrl("")}/css-generated/about.css"/>
    <%@include file="/WEB-INF/jsp/includes/generated/common-templates.jsp" %>
    <%@include file="/WEB-INF/jsp/includes/planeta-js.jsp" %>
    <p:script src="planeta-about.js"/>

    <script>
        $(function () {
            $('.js-goto-contact-us').on('click', function(e) {
                e.preventDefault();
                $("html, body").animate({ scrollTop: $('#_contact-us').offset().top }, 0);
            });

            $('.js-goto-campaign-list').on('click', function(e) {
                e.preventDefault();
                $("html, body").animate({ scrollTop: $('#campaign-list').offset().top }, 100);
            });

            $('.about-marketing-send-mail').click(function() {
                $('.js-error').removeClass('error');
                var flag = true;
                var mailFrom = $('.js-about-marketing-email').val();
                if (!mailFrom) {
                    flag = false;
                    $('.js-about-marketing-email').parent().addClass('error');
                }
                var message = $('.js-about-marketing-message').val();
                if (message == '') {
                    flag = false;
                    $('.js-about-marketing-message').parent().addClass('error');
                }
                if (flag) {
                    $('.js-error').removeClass('error');
                    $.ajax({
                         type: 'post',
                         url: "/api/advertising-send-mail.json",
                         data: {
                             mailFrom: mailFrom,
                             message: message
                         },
                         success: function(response){
                             if (response) {
                                 if (response.success) {
                                     $('.js-about-marketing-email').val('');
                                     $('.js-about-marketing-message').val('');

                                     $('.js-for-disable').prop( "disabled", true );
                                     $('.btn-marketing-feedback').text('Отправлено')
                                     setTimeout(function() {
                                         $('.js-for-disable').prop( "disabled", false );
                                         $('.btn-marketing-feedback').text('Отправить')
                                     }, 3000);
                                 } else {
                                     if (response.fieldErrors) {
                                         _(response.fieldErrors).each(function(message, fieldName) {
                                             $('[name=' + fieldName + ']').parent().addClass('error');
                                         });
                                     }
                                 }
                             }
                         }
                     });
                }
            });
        });
    </script>

</head>

<body class="grid-1200 about-page">
<%@ include file="/WEB-INF/jsp/includes/generated/irma/header.jsp" %>
<%@ include file="/WEB-INF/jsp/includes/generated/irma/main-nav.jsp" %>

<div id="global-container">
    <div id="main-container" class="wrap-container">

        <div id="center-container">

            <%@ include file="/WEB-INF/jsp/includes/about-new-menu.jsp" %>


            <div class="marketing-lead">
                <div class="marketing-lead_cont">
                    <div class="wrap">
                        <div class="col-12">

                            <div class="marketing-lead_head about-head">
                                Спецпроекты
                            </div>

                            <div class="marketing-lead_text">
                                Мы разрабатываем нестандартные бизнес-решения и совместные программы с учетом целей и задач вашего бренда. Сотрудничество в рамках спецпроекта задействует все информационные и рекламные каналы «Планеты» с привлечением авторов крауд-проектов и их аудитории.
                            </div>

                            <div class="marketing-lead_action">
                                <span class="marketing-lead_action-btn">
                                    <span class="btn-marketing-lead js-goto-campaign-list">наши спецпроекты</span>
                                </span>
                            </div>

                        </div>
                    </div>
                </div>
            </div>



            <div class="marketing-docs">
                <div class="marketing-docs_list wrap">
                    <div class="marketing-docs_i col-4">
                        <div class="marketing-docs_ico">
                            <span class="s-marketing-docs-kit"></span>
                        </div>

                        <div class="marketing-docs_name">
                            Медиа-кит
                        </div>

                        <div class="marketing-docs_btn">
                            <a href="https://files.planeta.ru/media-kit.pdf" class="btn-marketing-docs" target="_blank">Скачать</a>
                        </div>
                    </div>


                    <div class="marketing-docs_i col-4">
                        <div class="marketing-docs_ico">
                            <span class="s-marketing-docs-price"></span>
                        </div>

                        <div class="marketing-docs_name">
                            Прайс-лист
                        </div>

                        <div class="marketing-docs_btn">
                            <a href="https://docs.google.com/spreadsheets/d/1twnjRonnBjG_FxNkdQjI8y16tHJz812h8K6niWw7SuM" class="btn-marketing-docs" target="_blank" rel="nofollow noopener">Скачать</a>
                        </div>
                    </div>


                    <div class="marketing-docs_i col-4">
                        <div class="marketing-docs_ico">
                            <span class="s-marketing-docs-develop"></span>
                        </div>

                        <div class="marketing-docs_name marketing-docs_name__2">
                            Бриф на разработку спецпроекта
                        </div>

                        <div class="marketing-docs_btn">
                            <a href="https://files.planeta.ru/Brief_Planeta.ru.docx" class="btn-marketing-docs" target="_blank">Скачать</a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="marketing-spec">
                <div class="wrap">
                    <div class="col-12">

                        <div class="marketing-spec_list-wrap" id="campaign-list">
                            <div class="marketing-spec_list">
                                <div class="marketing-spec_i">
                                    <a href="https://promo.planeta.ru/techbattle" class="marketing-spec_link">
                                        <span class="marketing-spec_cover">
                                            <img src="//${hf:getStaticBaseUrl("")}/images/marketing/spec-tech.jpg">
                                        </span>
                                        <span class="marketing-spec_cont">
                                            <span class="marketing-spec_in">
                                                <span class="marketing-spec_name">
                                                    &laquo;Битва технологий&raquo;
                                                </span>
                                                <span class="marketing-spec_text">
                                                        Десять разработчиков новейших отечественных гаджетов обучались краудфандингу, чтобы создать идеальный проект и&nbsp;проверить свое «железо» на&nbsp;прочность в&nbsp;«Битве технологий»&nbsp;— спецпроекте Planeta.ru и&nbsp;EVA Invest при поддержке РВК.
                                                </span>
                                                <span class="marketing-spec_more">
                                                    <span class="s-icon s-icon-arrow-right"></span>
                                                </span>
                                            </span>
                                        </span>
                                    </a>
                                </div>
                                <div class="marketing-spec_i">
                                    <a href="http://goodstarter.liptontea.ru/" class="marketing-spec_link">
                                        <span class="marketing-spec_cover">
                                            <img src="//${hf:getStaticBaseUrl("")}/images/marketing/spec-goodstarter.jpg">
                                        </span>
                                        <span class="marketing-spec_cont">
                                            <span class="marketing-spec_in">
                                                <span class="marketing-spec_name">
                                                    GOODSTARTER
                                                </span>
                                                <span class="marketing-spec_text">
                                                    Компания Lipton и&nbsp;<nobr>Бизнес-школа</nobr> СКОЛКОВО помогают воплотить в&nbsp;жизнь наиболее интересные и&nbsp;популярные идеи социального предпринимательства, отобранные Planeta.ru.
                                                </span>
                                                <span class="marketing-spec_more">
                                                    <span class="s-icon s-icon-arrow-right"></span>
                                                </span>
                                            </span>
                                        </span>
                                    </a>
                                </div>

                                <div class="marketing-spec_i">
                                    <a href="https://planeta.ru/welcome/bazar2016.html" class="marketing-spec_link">
                                        <span class="marketing-spec_cover">
                                            <img src="//${hf:getStaticBaseUrl("")}/images/marketing/spec-dbazar.jpg">
                                        </span>
                                        <span class="marketing-spec_cont">
                                            <span class="marketing-spec_in">
                                                <span class="marketing-spec_name">
                                                    <span class="laquo">&laquo;</span>Душевный Bazar&raquo;
                                                </span>
                                                <span class="marketing-spec_text">
                                                    На&nbsp;средства, вложенные компанией Bayer и&nbsp;собранные на&nbsp;Planeta.ru, организуется самая масштабная благотворительная ярмарка России.
                                                </span>
                                                <span class="marketing-spec_more">
                                                    <span class="s-icon s-icon-arrow-right"></span>
                                                </span>
                                            </span>
                                        </span>
                                    </a>
                                </div>

                                <div class="marketing-spec_i">
                                    <a href="https://biblio.planeta.ru/" class="marketing-spec_link">
                                        <span class="marketing-spec_cover">
                                            <img src="//${hf:getStaticBaseUrl("")}/images/marketing/spec-biblio.jpg">
                                        </span>
                                        <span class="marketing-spec_cont">
                                            <span class="marketing-spec_in">
                                                <span class="marketing-spec_name">
                                                    <span class="laquo">&laquo;</span>БиблиоРодина&raquo;
                                                </span>
                                                <span class="marketing-spec_text">
                                                    Мощный вклад в&nbsp;российское образование и&nbsp;спасение периодики: при поддержке Государственной Думы РФ&nbsp;и&nbsp;пользователей Planeta.ruорганизуется подписка региональных библиотек на&nbsp;<nobr>научно-популярные</nobr> издания.
                                                </span>
                                                <span class="marketing-spec_more">
                                                    <span class="s-icon s-icon-arrow-right"></span>
                                                </span>
                                            </span>
                                        </span>
                                    </a>
                                </div>

                                <div class="marketing-spec_i">
                                    <a href="https://planeta.ru/welcome/university.html" class="marketing-spec_link">
                                        <span class="marketing-spec_cover">
                                            <img src="//${hf:getStaticBaseUrl("")}/images/marketing/spec-online-kampus.jpg">
                                        </span>
                                        <span class="marketing-spec_cont">
                                            <span class="marketing-spec_in">
                                                <span class="marketing-spec_name">
                                                    <span class="laquo">&laquo;</span><nobr>Онлайн-кампус</nobr>&raquo;
                                                </span>
                                                <span class="marketing-spec_text">
                                                    Не&nbsp;важно, ученый ты&nbsp;или студент! Есть идея важного социального или научного проекта в&nbsp;рамках образовательных учреждений? Есть поддержка от&nbsp;Planeta.ru и&nbsp;Финансового университета при Правительстве РФ!
                                                </span>
                                                <span class="marketing-spec_more">
                                                    <span class="s-icon s-icon-arrow-right"></span>
                                                </span>
                                            </span>
                                        </span>
                                    </a>
                                </div>

                                <div class="marketing-spec_i">
                                    <a href="https://planeta.ru/search/projects?query=&categories=TECHNO" class="marketing-spec_link">
                                        <span class="marketing-spec_cover">
                                            <img src="//${hf:getStaticBaseUrl("")}/images/marketing/spec-techno.jpg">
                                        </span>
                                        <span class="marketing-spec_cont">
                                            <span class="marketing-spec_in">
                                                <span class="marketing-spec_name">
                                                    <span class="laquo">&laquo;</span>Технологии возможностей&raquo;
                                                </span>
                                                <span class="marketing-spec_text">
                                                    Равные возможности&nbsp;&mdash; дело технологий! <nobr>Крауд-технологии</nobr> Planeta.ru и&nbsp;Центр поддержки социальных инноваций &laquo;Технологии возможностей&raquo; объединяются для поддержки проектов, помогающих пожилым людям и&nbsp;лицам с&nbsp;ограничениями здоровья.
                                                </span>
                                                <span class="marketing-spec_more">
                                                    <span class="s-icon s-icon-arrow-right"></span>
                                                </span>
                                            </span>
                                        </span>
                                    </a>
                                </div>

                                <div class="marketing-spec_i">
                                    <a href="https://planeta.ru/search/projects?query=&categories=MEGAFON" class="marketing-spec_link">
                                        <span class="marketing-spec_cover">
                                            <img src="//${hf:getStaticBaseUrl("")}/images/marketing/spec-megafon.jpg">
                                        </span>
                                        <span class="marketing-spec_cont">
                                            <span class="marketing-spec_in">
                                                <span class="marketing-spec_name">
                                                    <span class="laquo">&laquo;</span>МегаФон помогает&raquo;
                                                </span>
                                                <span class="marketing-spec_text">
                                                    Помогай с&nbsp;&laquo;МегаФоном&raquo;&nbsp;&mdash; и&nbsp;любой вклад в&nbsp;благотворительность в&nbsp;рамках этого специального проекта увеличится в&nbsp;четыре раза!
                                                </span>
                                                <span class="marketing-spec_more">
                                                    <span class="s-icon s-icon-arrow-right"></span>
                                                </span>
                                            </span>
                                        </span>
                                    </a>
                                </div>

                                <div class="marketing-spec_i">
                                    <a href="https://planeta.ru/search/projects?query=&categories=RUSAL" class="marketing-spec_link">
                                        <span class="marketing-spec_cover">
                                            <img src="//${hf:getStaticBaseUrl("")}/images/marketing/spec-rusal.jpg">
                                        </span>
                                        <span class="marketing-spec_cont">
                                            <span class="marketing-spec_in">
                                                <span class="marketing-spec_name">
                                                    <span class="laquo">&laquo;</span>Территория РУСАЛа<span class="raquo">&raquo;</span><span class="ensp"> </span> и&nbsp;&laquo;Помогать просто&raquo;
                                                </span>
                                                <span class="marketing-spec_text">
                                                    Создавая собственные проекты на&nbsp;Planeta.ru, участники этих конкурсных программ компании &laquo;РУСАЛ&raquo; находят единомышленников&nbsp;&mdash; и&nbsp;дополнительное финансирование!
                                                </span>
                                                <span class="marketing-spec_more">
                                                    <span class="s-icon s-icon-arrow-right"></span>
                                                </span>
                                            </span>
                                        </span>
                                    </a>
                                </div>

                                <div class="marketing-spec_i">
                                    <a href="https://planeta.ru/welcome/clever.html" class="marketing-spec_link">
                                        <span class="marketing-spec_cover">
                                            <img src="//${hf:getStaticBaseUrl("")}/images/marketing/spec-clever.jpg">
                                        </span>
                                        <span class="marketing-spec_cont">
                                            <span class="marketing-spec_in">
                                                <span class="marketing-spec_name">
                                                    <span class="laquo">&laquo;</span>Clever&nbsp;&mdash; детям!&raquo;
                                                </span>
                                                <span class="marketing-spec_text">
                                                    Издательство Clever предоставляет детские книги в&nbsp;качестве наград для благотворительных проектов и&nbsp;организует совместно с&nbsp;Planeta.ru детские благотворительные мероприятия.
                                                </span>
                                                <span class="marketing-spec_more">
                                                    <span class="s-icon s-icon-arrow-right"></span>
                                                </span>
                                            </span>
                                        </span>
                                    </a>
                                </div>


                            </div>

                            <div class="marketing-spec_my">
                                <div class="marketing-spec_my-head">
                                    Хотите свой спецпроект?
                                </div>
                                <div class="marketing-spec_my-action">
                                    <span class="btn-marketing-lead js-goto-contact-us">Дайте нам знать!</span>
                                </div>
                            </div>
                        </div>


                        <script>
                            $(function () {
                                var specMy = $('.marketing-spec_my');

                                var specItems = $('.marketing-spec_i');
                                var specItemsLen = specItems.length;

                                specMy.addClass('marketing-spec_my__' + (4 - (specItemsLen % 4)))
                            })
                        </script>

                    </div>
                </div>
            </div>


            <div id="_contact-us" class="marketing-feedback">
                <div class="wrap">
                    <div class="col-12">

                        <div class="marketing-feedback_head">
                            Свяжитесь с нами
                        </div>



                        <div class="wrap-row">
                            <div class="col-6">
                                <div class="marketing-feedback_info">
                                    <div class="marketing-feedback_mail">
                                        <a href="mailto:sale@planeta.ru">sale@planeta.ru</a>
                                    </div>
                                    <div class="marketing-feedback_tel">
                                        +7 (495) 256 24 95
                                    </div>
                                </div>
                            </div>



                            <div class="col-6">
                                <form class="marketing-feedback_from" onsubmit="return false;">
                                    <div class="marketing-feedback_fieldset js-error">
                                        <input type="text" name="email" class="form-control js-about-marketing-email js-for-disable" placeholder="Контактный e-mail">
                                    </div>
                                    <div class="marketing-feedback_fieldset js-error">
                                        <textarea rows="5" name="message" class="form-control js-about-marketing-message js-for-disable" placeholder="Напишите свой вопрос"></textarea>
                                    </div>
                                    <div class="marketing-feedback_fieldset marketing-feedback_action">
                                        <button type="submit" class="btn btn-marketing-feedback about-marketing-send-mail js-for-disable">Отправить</button>
                                    </div>
                                </form>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>

    </div>

</div>
<%@ include file="/WEB-INF/jsp/includes/generated/irma/footer.jsp" %>
</body>
</html>

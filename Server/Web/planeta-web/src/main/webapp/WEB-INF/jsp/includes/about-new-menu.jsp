<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<script>
    $(function() {
        var pathname = location.pathname;
        var menuItem = pathname.substring(pathname.lastIndexOf('/') + 1, location.pathname.length);
        if(menuItem == '') {
            $('.js-about').addClass('active');
        } else {
            $('.js-' + menuItem).addClass('active');
        }

        var aboutMenuMobile = function () {
            if (!window.isMobileDev) return;

            $(document).on('click', '.about-menu_i.active .about-menu_link', function (e) {
                e.preventDefault();
                $('.about-menu').toggleClass('open');
            });
        };

        aboutMenuMobile();
    });
</script>
<div class="about-menu-wrap">
    <div class="about-menu">
        <div class="wrap">
            <div class="col-12">

                <div class="about-menu_list">
                    <div class="about-menu_i js-about">
                        <a href="https://${properties['application.host']}/about" class="about-menu_link"><spring:message code="page.about.us.about" /></a>
                    </div>
                    <div class="about-menu_i js-contacts">
                        <a href="https://${properties['application.host']}/contacts" class="about-menu_link"><spring:message code="page.about.us.contacts" /></a>
                    </div>
                    <div class="about-menu_i js-advertising">
                        <a href="https://${properties['application.host']}/advertising" class="about-menu_link"><spring:message code="page.about.us.advertising" /></a>
                    </div>
                    <div class="about-menu_i js-logo">
                        <a href="https://${properties['application.host']}/logo" class="about-menu_link"><spring:message code="page.about.us.logo" /></a>
                    </div>
                    <div class="about-menu_i js-partners">
                        <a href="https://${properties['application.host']}/partners" class="about-menu_link"><spring:message code="page.about.us.partners" /></a>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%--<%@include file="/WEB-INF/jsp/includes/generated/doctype-header.jsp" %>--%>

<!DOCTYPE html>
<%@ include file="/WEB-INF/jsp/includes/generated/taglibs.jsp" %>
<!--[if lt IE 7]>      <html class="ie lt-ie7" lang="ru"> <![endif]-->
<!--[if IE 7]>         <html class="ie ie7" lang="ru"> <![endif]-->
<!--[if IE 8]>         <html class="ie ie8" lang="ru"> <![endif]-->
<!--[if IE 9]>         <html class="ie ie9" lang="ru"> <![endif]-->
<!--[if gt IE 9]><!--> <html lang="ru"
                             xmlns="http://www.w3.org/TR/REC-html40"> <!--<![endif]-->
<head>
    <title>УВЕДОМЛЕНИЕ о расторжении Соглашения о порядке использования сервиса «Акционирование»</title>
    <!-- meta tags -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <!-- end of meta tags -->

    <style type="text/css">
        html {
            font-size: 100%;
            -webkit-text-size-adjust: 100%;
            -ms-text-size-adjust: 100%;
        }
        body {
            margin: 0;
            font-family: "Helvetica Neue",Helvetica,Arial,sans-serif;
            font-size: 14px;
            line-height: 20px;
            color: #333;
            background-color: #fff;
        }
        table {
            max-width: 100%;
            background-color: transparent;
            border-collapse: collapse;
            border-spacing: 0;
        }
        td{
            padding: 0;
            border: 0;
        }

        h1, h2, h3, h4, h5, h6 {
            margin: 10px 0;
            font-family: inherit;
            font-weight: bold;
            line-height: 20px;
            color: inherit;
            text-rendering: optimizelegibility;
        }
        h1, h2, h3 {
            line-height: 40px;
        }
        h3 {
            font-size: 24.5px;
        }
        a{
            color: #0796de;
        }




        body {
            padding-top: 0;
            padding-bottom: 40px;
        }
        .container {
            margin: 0 auto;
            position: relative;
            padding: 0mm;
            width: 21cm;
            height: 29cm;
            page-break-before: always;
        }
        .container-narrow > hr {
            margin: 30px 0;
        }

        .signatures-body{
            padding: 26px 0 40px;
            text-align: right;
            font-weight: 700;
        }
        .signatures{
            position: relative;
        }
        .signet{
            position: absolute;
            top: -40px;
            left: 420px;
            width: 228px;
            height: 179px;
        }
        .signatures-text{
            display: inline-block;
            vertical-align: top;
            text-align: left;
        }


        .top{
            width: 100%;
        }
        .top-sign{
            width: 50%;
            color: #0796de;
            font-size: .8em;
            line-height: 1.2;
        }

        .top-logo{
            text-align: right;
        }

        .who{
            text-align: right;
            padding: 30px 0 50px 100px;
            line-height: 1.1;
        }

        .head{
            text-align: center;
            font-weight: 700;
            padding: 0 0 15px;
        }

        .head-meta{
            width: 100%;
        }
        .head-meta-date{
            text-align: right;
        }


        .dear{
            padding: 20px 0 0;
            text-align: center;
            font-weight: 700;
            font-style: italic;
            font-family: "Times New Roman", Times, serif;
        }


        .text p{
            text-indent: 50px;
        }

        .isp{

        }
    </style>
</head>
<body style="background-color: white;">

<div class="container">

    <table width="772">
        <tr>
            <td>
                <table class="top">
                    <tr>
                        <td class="top-sign">
                            ООО «Глобал Нетворкс»
                            <br>
                            129090 Россия, Москва, а/я 25
                            <br>
                            ОГРН 1107746618385
                            <br>
                            ИНН / КПП 7722724252 / 770201001
                            <br>
                            Проспект мира, д. 19, стр. 3, 3 этаж
                            <br>
                            +7 (495) 181-05-05
                        </td>
                        <td class="top-logo">
                            <img src="https://s4.planeta.ru/i/1d5fb9/1513330785058_renamed.jpg">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>

        <tr>
            <td class="who">
                Кому: ${contractor.name}
            </td>
        </tr>

        <tr>
            <td class="head">
                УВЕДОМЛЕНИЕ
                <br>
                о расторжении Соглашения о порядке использования сервиса «Акционирование»
            </td>
        </tr>

        <tr>
            <td>
                <table class="head-meta">
                    <tr>
                        <td class="head-meta-city">
                            город Москва
                        </td>
                        <td class="head-meta-date">
                            14 декабря 2017 года
                        </td>
                    </tr>
                </table>
            </td>
        </tr>


        <tr>
            <td class="dear">
                Уважаемый партнер!
            </td>
        </tr>


        <tr>
            <td class="text">
                <p>В&nbsp;связи с&nbsp;развитием платформы Planeta.ru и&nbsp;курсом на&nbsp;дальнейшее совершенствование документооборота в&nbsp;компании <nobr>ООО &laquo;Глобал Нетворкс&raquo;</nobr> нами принято решение перейти на&nbsp;новую форму соглашения при размещении проектов в&nbsp;разделе &laquo;Благотворительность&raquo; и&nbsp;в&nbsp;рамках программы &laquo;Планета добрых людей&raquo;. Новое соглашение отличается от&nbsp;предыдущего тем, что оно будет универсальным, иметь автоматическое заполнение, уменьшено в&nbsp;объеме, а&nbsp;главное, на&nbsp;каждый новый проект будет заключаться новое соглашение. Для реализации вышеупомянутой модели мы&nbsp;вынуждены прекратить действие предыдущего Соглашения, включая все приложения и&nbsp;дополнения. Поэтому настоящим письмом мы&nbsp;уведомляем о&nbsp;расторжении Соглашения о&nbsp;порядке использования сервиса &laquo;Акционирование&raquo;, заключенного между <nobr>ООО &laquo;ГЛОБАЛ НЕТВОРКС&raquo;</nobr> и&nbsp;вашей организацией на&nbsp;размещение проектов народного финансирования (краудфандинга) на&nbsp;платформе Planeta.ru.</p>
                <p>Данное уведомление мы&nbsp;направляем в&nbsp;соответствии с&nbsp;пунктом 11.2. раздела 11 нашего с&nbsp;вами Соглашения. По&nbsp;истечении установленного пунктом 11.2. срока в&nbsp;30 (тридцать) дней Соглашение считаем расторгнутым.</p>
                <p>Если ваша организация в&nbsp;настоящее время имеет незавершенный активный проект, НЕ&nbsp;БЕСПОКОЙТЕСЬ, так как срок расторжения считается продленным до&nbsp;даты расчетов между нашими сторонами. Если по&nbsp;<nobr>каким-либо</nobr> причинам расчеты между нашими сторонами не&nbsp;были проведены или не&nbsp;были завершены иные обязательства, просим вас направить информацию для завершения таких расчетов (обязательств).</p>
                <p>В&nbsp;случае направления вашей организацией новых проектов для размещения на&nbsp;платформе Planeta.ru, предполагается заключение нового соглашения. Проект нового соглашения, которое уже применяется в&nbsp;настоящее время приложен к&nbsp;данному уведомлению.</p>
                <p>Если у&nbsp;вас возникли вопросы, связанные с&nbsp;расторжением предыдущего соглашения или заключением нового, вы&nbsp;можете направить их&nbsp;по&nbsp;электронному адресу <a href="mailto:alex_planeta@planeta.ru">alex_planeta@planeta.ru</a> Чеснокову Алексею.</p>
                <p>Надеемся на&nbsp;дальнейшее сотрудничество на&nbsp;благо добрых проектов!</p>
            </td>
        </tr>

        <tr>
            <td class="signatures-body">
                <div class="signatures">
                    <img class="signet" src="https://s1.planeta.ru/i/129eff/1466683149385_renamed.jpg">


                    <div class="signatures-text">
                        Генеральный директор
                        <br>
                        ООО "Глобал Нетворкс"_____________________ (Мурачковский Ф.В.)
                    </div>
                </div>
            </td>
        </tr>

        <tr>
            <td class="isp">
                исп. А. Чесноков
                <br>
                <a href="mailto:alex_planeta@planeta.ru">alex_planeta@planeta.ru</a>
            </td>
        </tr>

    </table>

</div>

</body>
</html>
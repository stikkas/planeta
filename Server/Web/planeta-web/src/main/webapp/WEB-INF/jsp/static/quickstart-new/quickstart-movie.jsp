<%@ include file="/WEB-INF/jsp/includes/generated/doctype-header.jsp" %>
<head>
    <c:set var="pageType" value="movie"/>
    <c:set var="eventLabel" value="quickmovie_get_recommendation"/>
    <%@ include file="/WEB-INF/jsp/static/quickstart-new/includes/quickstart-header.jsp" %>
</head>

<body>
    <div class="header">
        <a class="logo" href="https://planeta.ru/"></a>

        <a class="btn btn-primary header_create-btn" href="/funding-rules">Создать проект</a>
    </div>

    <div class="video-lead var3">
        <div class="wrap">
            <div class="video-lead_head">Хотите снять кино?</div>
            <div class="video-lead_block cf">
                <div class="video-lead_video">
                    <div class="embed-video">
                        <iframe width="560" height="315" src="//www.youtube.com/embed/X-UMyZE6Ze4" frameborder="0" allowfullscreen></iframe>
                    </div>
                </div>
                <div class="video-lead_cont">
                    <div class="video-lead_text">
                        У вас есть идея короткометражного фильма или полного метра?
                         Для её реализации не обязательно искать инвесторов –
                         вам помогут будущие зрители! Интересно? Тогда не стесняйтесь
                          и поделитесь своими замыслами с другими!
                        </div>

                    <div class="video-lead_action-text">
                        Получите видеозапись вебинара
                        <br>
                        и бесплатное руководство
                        <br>
                        «Как снять кино с помощью краудфандинга»
                    </div>

                    <div class="video-lead_action">
                        <div class="fieldset pln-dropdown js-open-error">
                            <input class="form-control js-get-book-email" type="text" placeholder="Введите ваш e-mail">
                            <div class="page-control_popup pln-d-popup">
                                <div class="pln-d-popup-tail"><div></div></div>
                                <div class="d-popup-cont">
                                    Вы ввели неверный адрес электронной почты
                                </div>
                            </div>
                        </div>

                        <div class="video-lead_action_btn pln-dropdown js-get-book-btn-cnt" >
                            <button class="btn btn-primary js-get-book-btn" onclick="getBook()">Получить!</button>
                            <div class="page-control_popup pln-d-popup">
                                <div class="pln-d-popup-tail"><div></div></div>
                                <nobr class="d-popup-cont">
                                    Вам отправлена ссылка на вебинар<br>
                                    и руководство «Как снять кино<br>
                                    с помощью краудфандинга»
                                </nobr>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>




    <%@ include file="/WEB-INF/jsp/static/quickstart-new/includes/quickstart-inner.jsp" %>
    <div class="author-slider">
        <div class="author-slider_head">Отзывы авторов проектов</div>

        <div class="author-slider_head-wrap">
            <div class="author-slider_head-list uninit">

                <div class="author-slider_head-i">
                    <div class="author-slider_head-name">
                        <div class="author-slider_head-name-text">
                            <span class="cur-p">Олег Куваев</span>
                        </div>
                    </div>
                    <div class="author-slider_head-ava"><img src="//${hf:getStaticBaseUrl("")}/images/quickstart/author/author-7.jpg" width="144" height="144"></div>
                </div>

                <div class="author-slider_head-i">
                    <div class="author-slider_head-name">
                        <div class="author-slider_head-name-text">
                            <span class="cur-p">Гарри Бардин</span>
                        </div>
                    </div>
                    <div class="author-slider_head-ava"><img src="//${hf:getStaticBaseUrl("")}/images/quickstart/author/bardin.png" width="144" height="144"></div>
                </div>

                <div class="author-slider_head-i">
                    <div class="author-slider_head-name">
                        <div class="author-slider_head-name-text">
                            <span class="cur-p">Ирина Юткина</span>
                        </div>
                    </div>
                    <div class="author-slider_head-ava"><img src="//${hf:getStaticBaseUrl("")}/images/quickstart/author/utkina.jpg" width="144" height="144"></div>
                </div>
                <div class="author-slider_head-i">
                    <div class="author-slider_head-name">
                        <div class="author-slider_head-name-text">
                            <span class="cur-p">Олег Куваев</span>
                        </div>
                    </div>
                    <div class="author-slider_head-ava"><img src="//${hf:getStaticBaseUrl("")}/images/quickstart/author/author-7.jpg" width="144" height="144"></div>
                </div>

                <div class="author-slider_head-i">
                    <div class="author-slider_head-name">
                        <div class="author-slider_head-name-text">
                            <span class="cur-p">Гарри Бардин</span>
                        </div>
                    </div>
                    <div class="author-slider_head-ava"><img src="//${hf:getStaticBaseUrl("")}/images/quickstart/author/bardin.png" width="144" height="144"></div>
                </div>

                <div class="author-slider_head-i">
                    <div class="author-slider_head-name">
                        <div class="author-slider_head-name-text">
                            <span class="cur-p">Ирина Юткина</span>
                        </div>
                    </div>
                    <div class="author-slider_head-ava"><img src="//${hf:getStaticBaseUrl("")}/images/quickstart/author/utkina.jpg" width="144" height="144"></div>
                </div>
            </div>
        </div>


        <div class="author-slider_text-wrap">
            <div class="author-slider_text">

                <div class="author-slider_text-i">
                    <div class="author-slider_text-project">
                        Олег Куваев, мультипликатор
                    </div>
                    <div class="author-slider_text-text">
                        Я очень доволен результатами краудфандинга. Их у меня было два, и оба превзошли мои ожидания. Проекты были очень удачными. И команда «Планеты», и спонсоры сработали на ура. Без сучка, без задоринки. Что бы не значила эта «задоринка».
                    </div>
                </div>

                <div class="author-slider_text-i">
                    <div class="author-slider_text-project">
                        Гарри Бардин, режиссер-мультипликатор
                    </div>
                    <div class="author-slider_text-text">
                        Хорошо, что есть Planeta.ru, к которой я однажды имел отношение. Для меня это — «с миру по нитке», но ниток оказалось так много, что это дало мне возможность закончить картину.
                    </div>
                </div>

                <div class="author-slider_text-i">
                    <div class="author-slider_text-project">
                        Ирина Юткина, продюсер проектов Евгения Гришковца
                    </div>
                    <div class="author-slider_text-text">
                        Честно говоря, я каждый день заглядываю на страницу Planeta.ru, и для меня это такая азартная штука. Потому что это воспринимается как чудо. То есть, люди не только купили билеты на съемку видеоверсии — ведь она состоялась 8 декабря 2012 г. — каждый день они тратят свои деньги на то, чтобы поддержать создание видеоверсии и купить DVD с автографом или электронный вариант спектакля.
                    </div>
                </div>
                <div class="author-slider_text-i">
                    <div class="author-slider_text-project">
                        Олег Куваев, мультипликатор
                    </div>
                    <div class="author-slider_text-text">
                        Я очень доволен результатами краудфандинга. Их у меня было два, и оба превзошли мои ожидания. Проекты были очень удачными. И команда «Планеты», и спонсоры сработали на ура. Без сучка, без задоринки. Что бы не значила эта «задоринка».
                    </div>
                </div>

                <div class="author-slider_text-i">
                    <div class="author-slider_text-project">
                        Гарри Бардин, режиссер-мультипликатор
                    </div>
                    <div class="author-slider_text-text">
                        Хорошо, что есть Planeta.ru, к которой я однажды имел отношение. Для меня это — «с миру по нитке», но ниток оказалось так много, что это дало мне возможность закончить картину.
                    </div>
                </div>

                <div class="author-slider_text-i">
                    <div class="author-slider_text-project">
                        Ирина Юткина, продюсер проектов Евгения Гришковца
                    </div>
                    <div class="author-slider_text-text">
                        Честно говоря, я каждый день заглядываю на страницу Planeta.ru, и для меня это такая азартная штука. Потому что это воспринимается как чудо. То есть, люди не только купили билеты на съемку видеоверсии — ведь она состоялась 8 декабря 2012 г. — каждый день они тратят свои деньги на то, чтобы поддержать создание видеоверсии и купить DVD с автографом или электронный вариант спектакля.
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="promo-lead">
        <div class="wrap">
            <div class="promo-lead_head">Кому это интересно?</div>
            <div class="promo-lead_block cf">

                <div class="promo-lead_list">
                    <div class="promo-lead_i">
                        <div class="promo-lead_i_img">
                            <img src="//${hf:getStaticBaseUrl("")}/images/quickstart/promo-lead-img-4.png">
                        </div>
                        <div class="promo-lead_i_head">
                            Режиссеру
                        </div>
                        <div class="promo-lead_i_text">
                            С&nbsp;помощью &laquo;Планеты&raquo; вы&nbsp;сможете собрать средства на&nbsp;воплощение
                            своего художественного замысла, а&nbsp;общение с&nbsp;пользователями позволит найти новые
                            идеи&nbsp;&mdash; и&nbsp;даже будущих актеров! Но&nbsp;главное, что при этом никто
                            не&nbsp;будет указывать вам, что должно быть в&nbsp;фильме и&nbsp;как его
                            снимать&nbsp;&mdash; мы&nbsp;дарим полную свободу для творчества.
                        </div>
                    </div>
                    <div class="promo-lead_i">
                        <div class="promo-lead_i_img">
                            <img src="//${hf:getStaticBaseUrl("")}/images/quickstart/promo-lead-img-4-2.png">
                        </div>
                        <div class="promo-lead_i_head">
                            Сценаристу
                        </div>
                        <div class="promo-lead_i_text">
                            У&nbsp;вас есть хороший сценарий, который хотелось&nbsp;бы воплотить в&nbsp;жизнь,
                            но&nbsp;вы&nbsp;не&nbsp;знаете, как, с&nbsp;кем и&nbsp;на&nbsp;какие деньги?
                            На&nbsp;&laquo;Планете&raquo; вы&nbsp;найдете множество творческих
                            личностей&nbsp;&mdash; актеров, режиссеров, декораторов и т. д., а&nbsp;будущие зрители
                            помогут вам найти средства для съемки фильма.
                        </div>
                    </div>
                    <div class="promo-lead_i">
                        <div class="promo-lead_i_img">
                            <img src="//${hf:getStaticBaseUrl("")}/images/quickstart/promo-lead-img-4-3.png">
                        </div>
                        <div class="promo-lead_i_head">
                            Продюсеру
                        </div>
                        <div class="promo-lead_i_text">
                            Аудитория &laquo;Планеты&raquo;&nbsp;&mdash; это сотни тысяч потенциальных зрителей вашей
                            будущей картины, которые, к&nbsp;тому&nbsp;же, готовы поддержать ее&nbsp;выпуск рублем.
                            Даже если вы&nbsp;не&nbsp;собираетесь получать прокатное удостоверение, у&nbsp;вас всегда
                            будет способ донести фильм до&nbsp;публики, которая уже будет с&nbsp;нетерпением ждать
                            выхода ленты.
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>


    <%@ include file="/WEB-INF/jsp/static/quickstart-new/includes/quickstart-request.jsp" %>

    <div class="partners">
        <div class="partners_list">
            <a href="http://tvkinoradio.ru/" target="_blank" rel="nofollow noopener"><img class="partners_i" src="//${hf:getStaticBaseUrl("")}/images/quickstart/partner-tvkr.png"></a>
            <a href="http://www.cinemotionlab.com/" target="_blank" rel="nofollow noopener"><img class="partners_i" src="//${hf:getStaticBaseUrl("")}/images/quickstart/partner-cinemotion.png"></a>
            <a href="http://crowdhunters.ru/" target="_blank" rel="nofollow noopener"><img class="partners_i" src="//${hf:getStaticBaseUrl("")}/images/quickstart/partner-crowdhunters.png"></a>
            <a href="http://moscowfilmschool.ru/" target="_blank" rel="nofollow noopener"><img class="partners_i" src="//${hf:getStaticBaseUrl("")}/images/quickstart/partner-mfs.png"></a>
        </div>
    </div>



</body>
</html>
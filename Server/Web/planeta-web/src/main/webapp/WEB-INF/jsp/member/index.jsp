<%@ include file="/WEB-INF/jsp/includes/generated/doctype-header.jsp" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<head>
    <%@include file="/WEB-INF/jsp/includes/generated/stat-counters-async.jsp" %>

    <c:if test="${not empty customMetaTag.keywords}">
        <meta name="keywords" content="${customMetaTag.keywords}"/>
    </c:if>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">

    <%@include file="/WEB-INF/jsp/includes/sharing-meta-data.jsp" %>
    <c:if test="${not interactiveCampaignZone}">
        <meta name="viewport" content="width=device-width, initial-scale=1"/>
    </c:if>

    <c:if test="${interactiveCampaignZone}">
        <meta name="viewport" content="width=1024, initial-scale=1.0, maximum-scale=1.0">
    </c:if>

    <link rel="search" type="application/opensearchdescription+xml" title="Planeta.ru" href="/opensearch.xml">

    <title>${not empty customMetaTag.title ? customMetaTag.title : not empty metaTitle ? metaTitle : 'Планета'}</title>

    <script type="text/javascript">
        <%--нужно, чтобы знать надо ли нам подставлять title и прочее при первом заходе на страницу--%>
        <%--или оно было взято из бд и ничего менять не нужно--%>
        window.customMetaTagsFromDb = ${not empty customMetaTag};
    </script>

    <script id="default-layout" type="text/x-layout-template">
        <div id="global-container" class="container">
            <div id="main-container" class="row main-container">
                <div id="left-sidebar" class="span4"></div>
                <div id="center-container" class="span10 columns">
                    <div class="js-banner-container-top"></div>
                    <div id="header-container"></div>
                    <div id="menu-container"></div>
                    <div id="section-container"></div>
                    <div id="content-view"></div>
                </div>
                <div class="sidebar-banner sidebar-banner-js span5"></div>
                <div id="right-sidebar" class="span5 columns"></div>
            </div>
        </div>
    </script>

    <script id="simple-page-layout" type="text/x-layout-template">
        <div id="main-container" class="wrap-container">
            <div id="center-container">
                <div class="wrap">
                    <div class="col-12">
                        <div class="pln-content-box">
                            <div class="pln-content-box_cont cf pln-content-box_cont__offset">
                                <div id="header-container"></div>
                                <div id="menu-container"></div>
                                <div id="section-container">
                                    <div id="content-view"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </script>

    <script id="quiz-page-layout" type="text/x-layout-template">
        <div id="main-container" class="wrap-container">
            <div id="section-container">
                <div id="content-view"></div>
            </div>
        </div>
    </script>

    <script id="wide-layout" type="text/x-layout-template">
        <div id="global-container">
            <div id="main-container">
                <div id="center-container">
                    <div id="section-container-extra"></div>
                    <div id="content-view"></div>
                </div>
            </div>
        </div>
    </script>

    <c:if test="${not interactiveCampaignZone}">
        <%@ include file="/WEB-INF/jsp/includes/generated/back-to-top.jsp" %>
    </c:if>

    <c:if test="${not interactiveCampaignZone}">
    <%@ include file="/WEB-INF/jsp/includes/generated/screen-width.jsp" %>
    </c:if>
    <%@include file="/WEB-INF/jsp/includes/common-css.jsp" %>
    <%@include file="/WEB-INF/jsp/includes/generated/common-templates.jsp" %>
    <%@include file="/WEB-INF/jsp/includes/planeta-js-async.jsp" %>
</head>
<body style="background-image: none;">


<c:if test="${param['no-counters'] != true && header['User-Agent'] != 'planeta-snapshot-bot' && properties['google.tag.manager.id'] != 'false'}">
    <!-- Google Tag Manager (noscript) -->
    <noscript>
        <iframe src="//www.googletagmanager.com/ns.html?id=${properties['google.tag.manager.id']}"
                height="0"
                width="0"
                style="display:none; visibility:hidden"></iframe>
    </noscript>
    <!-- End Google Tag Manager (noscript) -->
</c:if>

<c:if test="${not interactiveCampaignZone}">
    <%@include file="/WEB-INF/jsp/includes/generated/irma/header.jsp" %>
    <%@include file="/WEB-INF/jsp/includes/generated/irma/main-nav.jsp" %>
</c:if>

<c:if test="${interactiveCampaignZone}">
    <%@ include file="/WEB-INF/jsp/includes/generated/header-menu.jsp" %>
</c:if>

<%@ include file="/WEB-INF/jsp/includes/offline.jsp" %>
<noscript>Use Javascript, Luke</noscript>

<div id="root-container">
</div>

<c:if test="${not interactiveCampaignZone}">
    <%@ include file="/WEB-INF/jsp/includes/generated/irma/footer.jsp" %>
</c:if>
</body>
</html>


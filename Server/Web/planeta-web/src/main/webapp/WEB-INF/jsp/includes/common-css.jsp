<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<c:if test="${not interactiveCampaignZone}">
<link id="common-css-id" type="text/css" rel="stylesheet" href="//${hf:getStaticBaseUrl("")}/css-generated/common.css"/>
</c:if>

<!--[if lte IE 9]>
<link type="text/css" rel="stylesheet" href="//${hf:getStaticBaseUrl("")}/css/placeholder_polyfill.css"/>
<![endif]-->

<%@include file="/WEB-INF/jsp/includes/generated/common-favicon.jsp" %>

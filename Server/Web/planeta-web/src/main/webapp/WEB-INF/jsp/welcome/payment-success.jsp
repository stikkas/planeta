<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ include file="/WEB-INF/jsp/includes/taglibs.jsp" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <%@include file="/WEB-INF/jsp/includes/generated/stat-counters-async.jsp" %>
    <meta name="viewport" content="width=device-width"/>
    <c:if test="${firstObject != null}">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta property="og:site_name" content="planeta.ru"/>
        <meta property="og:title" content="Я поддерживаю проект <c:out value="${firstObject.ownerName}"/>"/>
    </c:if>
    <title>Спасибо за покупку!</title>
    <%@include file = "/WEB-INF/jsp/includes/common-css.jsp" %>
    <link type="text/css" rel="stylesheet" href="//${hf:getStaticBaseUrl("")}/css-generated/payment.css"/>
    <%@include file="/WEB-INF/jsp/includes/planeta-js.jsp" %>
    <%@include file="/WEB-INF/jsp/includes/generated/common-templates.jsp" %>

</head>
<body>
<c:set var="profile" value="${myProfile.profile}"/>
<%@include file="/WEB-INF/jsp/includes/generated/irma/header.jsp" %>
<%@include file="/WEB-INF/jsp/includes/generated/irma/main-nav.jsp" %>





<script type="text/javascript">
    $(document).ready(function() {

        if (window.gtm) {
            var transactionInfo = {
                transactionId: '${transaction.transactionId}',
                amount: '${transaction.amountNet}'
            };
            window.gtm.trackPurchaseBalance(transactionInfo);
        }


        var relatedCampaigns = new CrowdFund.Models.RelatedCampaignsList([], {
            data: {
                objectId: 0
            }
        });

        var relatedCampaignsList = new CrowdFund.Views.RelatedCampaignsList({
            el: '.js-project-card-list-container div',
            collection: relatedCampaigns
        });

        relatedCampaigns.load();
    });
</script>
<div id="global-container">
    <div id="main-container" class="wrap-container">
        <div id="center-container" class="wrap">
            <div class="col-12">
                <div class="pln-payment-box">
                    <c:choose>
                        <c:when test="${transaction.status == 'NEW' && !isMobilePayment && !isDeferred}">
                            <div class="pln-payment-success">
                                <div class="pln-payment-success_head">
                                    Ваш платеж зарегистрирован.
                                </div>
                                <div class="pln-payment-success_sub-head">
                                </div>

                                <div class="pln-payment-success_widget cf">

                                    <div class="pln-payment-success_widget_i">
                                        <div class="pln-payment-success_widget_ico">
                                            <span class="success-widget-icon success-widget-icon-time"></span>
                                        </div>
                                        <div class="pln-payment-success_widget_text">
                                            Обработка Вашего платежа может занять до получаса.
                                        </div>
                                    </div>

                                    <div class="pln-payment-success_widget_i">
                                        <div class="pln-payment-success_widget_ico">
                                            <span class="success-widget-icon success-widget-icon-mail"></span>
                                        </div>
                                        <div class="pln-payment-success_widget_text">
                                            О результатах платежа мы сообщим Вам, как только получим ответ от платёжной системы.
                                        </div>
                                    </div>

                                    <div class="pln-payment-success_widget_i">
                                        <div class="pln-payment-success_widget_ico">
                                            <span class="success-widget-icon success-widget-icon-flag"></span>
                                        </div>
                                        <div class="pln-payment-success_widget_text">
                                            Когда платёж пройдёт, купленные вознаграждения вы сможете найти в вашем <a href="/account">личном&nbsp;кабинете</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </c:when>
                        <c:otherwise>
                            <div class="pln-payment-success">
                                <c:choose>
                                    <c:when test="${isMobilePayment}">
                                        <c:choose>
                                            <c:when test="${order != null && order.orderType == 'DELIVERY'}">
                                                <div class="pln-payment-success_head">
                                                    Спасибо за оплату доставки!
                                                </div>
                                                <div class="pln-payment-success_sub-head">
                                                    В ближайшие пять минут должна прийти SMS с инструкцией для оплаты.
                                                    <br>
                                                    Следуйте инструкции, и оплата будете завершена.
                                                </div>
                                            </c:when>
                                            <c:otherwise>
                                                <div class="pln-payment-success_head">
                                                    Вам выставлен счет для оплаты!
                                                </div>
                                                <div class="pln-payment-success_sub-head">
                                                    В ближайшие пять минут должна прийти SMS с инструкцией для оплаты.
                                                    <br>
                                                    Следуйте инструкции, и оплата будете завершена.
                                                </div>
                                            </c:otherwise>
                                        </c:choose>
                                    </c:when>
                                    <c:when test="${isDeferred}">
                                        <c:choose>
                                            <c:when test="${order != null && order.orderType == 'DELIVERY'}">
                                                <div class="pln-payment-success_head">
                                                    Спасибо за оплату доставки!
                                                </div>
                                                <div class="pln-payment-success_sub-head">
                                                    Для совершения платежа вам нужно будет подтвердить выставленный Вам счет.
                                                </div>
                                            </c:when>
                                            <c:otherwise>
                                                <div class="pln-payment-success_head">
                                                    Вам выставлен счет для оплаты!
                                                </div>
                                                <div class="pln-payment-success_sub-head">
                                                    Для совершения платежа вам нужно будет подтвердить выставленный Вам счет.
                                                </div>
                                            </c:otherwise>
                                        </c:choose>
                                    </c:when>
                                    <c:otherwise>
                                        <div class="pln-payment-success_head">
                                            <c:choose>
                                                <c:when test="${order != null && order.orderType == 'DELIVERY'}">
                                                    Спасибо за оплату доставки!
                                                </c:when>
                                                <c:otherwise>
                                                    Оплата проведена <span class="hl">успешно</span>!
                                                </c:otherwise>
                                            </c:choose>
                                        </div>
                                        <div class="pln-payment-success_sub-head">
                                            <c:choose>
                                                <c:when test="${order != null}">
                                                    <c:if test="${order.orderType == 'SHARE'}">
                                                        Спасибо, что поддержали проект! На ваш e-mail <b>${order.buyerEmail}</b>
                                                        отправлено письмо с информацией о покупке.
                                                    </c:if>
                                                </c:when>
                                                <c:otherwise>
                                                    Теперь Вы можете поддержать сотни наших проектов, или совершить покупку в интернет-магазине Планеты.
                                                </c:otherwise>
                                            </c:choose>
                                        </div>
                                    </c:otherwise>
                                </c:choose>

                                <c:if test="${order != null && order.orderType == 'SHARE'}">
                                    <div class="pln-payment-success_widget cf">
                                        <div class="pln-payment-success_widget_i">
                                            <div class="pln-payment-success_widget_ico">
                                                <span class="success-widget-icon success-widget-icon-flag"></span>
                                            </div>
                                            <div class="pln-payment-success_widget_text">
                                                Купленное вознаграждение всегда находится в вашем <a href="${redirectUrl}">личном&nbsp;кабинете</a>
                                            </div>
                                        </div>

                                        <div class="pln-payment-success_widget_i">
                                            <div class="pln-payment-success_widget_ico">
                                                <span class="success-widget-icon success-widget-icon-star"></span>
                                            </div>
                                            <div class="pln-payment-success_widget_text">
                                                <c:choose>
                                                    <c:when test="${isSubscribeded}">
                                                        <a class="btn btn-primary btn-lg" href="#">Отписаться от автора</a>
                                                    </c:when>
                                                    <c:otherwise>
                                                        <a class="btn btn-primary btn-lg" href="#">Подписаться на автора</a>
                                                    </c:otherwise>
                                                </c:choose>
                                            </div>
                                        </div>

                                        <div class="pln-payment-success_widget_i">

                                            <div class="pln-payment-success_widget_sharing">
                                                <div class="pln-payment-success_widget_sharing-head">Поделиться</div>
                                                <div class="share-cont-horizontal"></div>
                                            </div>

                                            <div class="pln-payment-success_widget_text">
                                                Поделитесь информацией о&nbsp;проекте.
                                                <br>
                                                Это очень помогает!
                                            </div>
                                        </div>
                                    </div>
                                </c:if>
                            </div>
                        </c:otherwise>
                    </c:choose>
                </div>
                <div class = "js-project-list-container project-related">
                    <div class="project-related-head">
                        Проекты, которые только что поддержали
                    </div>
                    <div class = "js-project-card-list-container">
                        <div class="project-card-list"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<%@ include file="/WEB-INF/jsp/includes/generated/irma/footer.jsp" %>
</body>
</html>

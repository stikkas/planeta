<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@include file="/WEB-INF/jsp/includes/generated/doctype-header.jsp" %>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Planeta: Информация о заказе</title>
    <meta name="title" content="Planeta: Информация о заказе">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">

    <link type="text/css" rel="stylesheet" href="//${hf:getStaticBaseUrl("")}/css-generated/common.css"/>

    <style type="text/css">
        html {
            font-size: 100%;
            -webkit-text-size-adjust: 100%;
            -ms-text-size-adjust: 100%;
        }
        body {
            margin: 0;
            font-family: "Helvetica Neue",Helvetica,Arial,sans-serif;
            font-size: 14px;
            line-height: 20px;
            color: #333;
            background-color: #fff;
        }
        table {
            max-width: 100%;
            background-color: transparent;
            border-collapse: collapse;
            border-spacing: 0;
        }

        h1, h2, h3, h4, h5, h6 {
            margin: 10px 0;
            font-family: inherit;
            font-weight: bold;
            line-height: 20px;
            color: inherit;
            text-rendering: optimizelegibility;
        }
        h1, h2, h3 {
            line-height: 40px;
        }
        h3 {
            font-size: 24.5px;
        }

        .offset1 {
            margin-left: 100px;
        }

        .text-center {
            text-align: center;
        }
        .text-right {
            text-align: right;
        }

        .text-field {
            word-wrap: break-word;
            word-break: break-all;
        }




        body {
            padding-top: 20px;
            padding-bottom: 40px;
        }
        /* Custom container */
        .container {
            margin: 0 auto;
            width:772px;
        }
        .container-narrow > hr {
            margin: 30px 0;
        }
        .border {
            border: 1px solid #000;
        }
        .border-top {
            border-top: 1px solid #000;
        }
        .pp {
            width:100%;
        }
        .td-hr{
            height: 0;
            padding: 0;
            font: 0/0 a;
            border-bottom: 3px solid #000;
        }

        .signatures{
            position: relative;
        }
        .signet{
            position: absolute;
            top: -60px;
            left: 285px;
            width: 228px;
            height: 179px;
        }
    </style>

</head>
<body class="tickets-page">
<div class="container">

    <table width="772">
        <tbody>
        <tr><td>&nbsp;</td></tr>

        <tr>
            <td>
                <table class="pp border">
                    <tbody><tr>
                        <td class="border">ИНН 7722724252</td>
                        <td class="border">КПП 770201001</td>
                        <td class="border text-center" style="width: 54px;" rowspan="3" valign="top">Сч. №</td>
                        <td class="border" style="width: 218px;" rowspan="3" valign="top">40702810500310000443</td>
                    </tr>
                    <tr>
                        <td colspan="2">Получатель</td>
                    </tr>
                    <tr>
                        <td colspan="2">ООО "ГЛОБАЛ НЕТВОРКС"</td>
                    </tr>
                    <tr class="border-top">
                        <td colspan="2">Банк получателя</td>
                        <td class="border text-center">БИК</td>
                        <td>044525402</td>
                    </tr>
                    <tr>
                        <td colspan="2">АБ "ИНТЕРПРОГРЕССБАНК" (ЗАО), г. Москва, Старокаширское ш., д. 2, корп. 1, стр.1</td>
                        <td class="border text-center" valign="top">Сч. №</td>
                        <td valign="top">30101810100000000402</td>
                    </tr>
                    </tbody></table>
            </td>
        </tr>

        <tr>
            <td>
                <h3>Счет на оплату № ${order.topayTransactionId} от ${transactionDate} г.</h3>
            </td>
        </tr>

        <tr>
            <td class="td-hr">&nbsp;</td>
        </tr>

        <tr><td>&nbsp;</td></tr>

        <tr>
            <td>
                <table>
                    <tbody>
                    <tr>
                        <td valign="top"><b>Агент:&nbsp;</b></td>
                        <td class="text-field">
                            ООО "ГЛОБАЛ НЕТВОРКС", ИНН 7722724252, КПП 770201001, 129090, г. Москва, Проспект Мира, д.19, строение 3, пом. VIII, тел.: (495) 181-05-05
                        </td>
                    </tr>
                    <tr>
                        <td valign="top"><b>Автор проекта:&nbsp;</b></td>
                        <td class="text-field">
                            ${hf:escapeHtml4(contractor.name)}
                            <c:choose>
                                <c:when test = "${contractor.type == 'INDIVIDUAL'}">
                                    <c:if test = "${contractor.inn != null}">,&nbsp;ИНН ${contractor.inn}</c:if>
                                </c:when>
                                <c:otherwise>
                                    ,&nbsp;ИНН ${contractor.inn},&nbsp;ОГРН ${contractor.ogrn}&nbsp;
                                </c:otherwise>
                            </c:choose>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top"><b>Проект:&nbsp;</b></td>
                        <td class="text-field">
                            ${campaign.name} (${campaign.campaignId})
                        </td>
                    </tr>
                    <tr>
                        <td valign="top"><b>Инвестор:&nbsp;</b></td>
                        <td class="text-field">
                            <c:choose>
                                <c:when test = "${investOrderInfo.userType == 'INDIVIDUAL'}">
                                    ${investOrderInfo.lastName}&nbsp;${investOrderInfo.firstName}&nbsp;${investOrderInfo.middleName},&nbsp;дата рождения:&nbsp;${investorBirthDate},&nbsp;адрес регистрации:&nbsp;${investOrderInfo.regIndex},&nbsp;${investOrderInfo.regCountry},&nbsp;${investOrderInfo.regLocation},&nbsp;${investOrderInfo.regAddress}
                                </c:when>
                                <c:otherwise>
                                    ${investOrderInfo.orgName},&nbsp;ИНН ${investOrderInfo.inn},&nbsp;${investOrderInfo.regIndex},&nbsp;${investOrderInfo.regCountry},&nbsp;${investOrderInfo.regLocation},&nbsp;${investOrderInfo.regAddress}
                                </c:otherwise>
                            </c:choose>
                        </td>
                    </tr>
                    </tbody></table>
            </td>
        </tr>

        <tr><td>&nbsp;</td></tr>

        <fmt:setLocale value="ru_RU"/>
        <tr>
            <td>
                <table class="pp" cellpadding="4">
                    <tbody><tr>
                        <td class="border text-center" width="20"><b>№</b></td>
                        <td class="border text-center" width="340"><b>Основание платежа</b></td>
                        <td class="border text-center" width="60"><b>Ед.</b></td>
                        <td class="border text-center" width="50"><b>Кол-во</b></td>
                        <td class="border text-center" width="120"><b>Цена</b></td>
                        <td class="border text-center" width="120"><b>Сумма</b></td>
                    </tr>
                    <tr>
                        <td class="border text-right" valign="top">1</td>
                        <td class="border">Оплата по договору инвестирования №${order.topayTransactionId} в пользу проекта "${campaign.name}" (${campaign.campaignId})</td>
                        <td class="border text-center" valign="bottom">шт.</td>
                        <td class="border text-right" valign="bottom">1</td>
                        <td class="border text-right" valign="bottom"><fmt:formatNumber currencySymbol="" value="${order.totalPrice}" type="currency"/></td>
                        <td class="border text-right" valign="bottom"><fmt:formatNumber currencySymbol="" value="${order.totalPrice}" type="currency"/></td>
                    </tr>

                    <tr>
                        <td class="text-right" colspan="5"><b>Итого:</b></td>
                        <td class="border text-right"><b><fmt:formatNumber currencySymbol="" value="${order.totalPrice}" type="currency"/></b></td>
                    </tr>
                    <tr>
                        <td class="text-right" colspan="5"><b>Без налога (НДС).</b></td>
                        <td class="border text-right"><b>
                            -</b></td>
                    </tr>
                    <tr>
                        <td class="text-right" colspan="5"><b>Всего к оплате:</b></td>
                        <td class="border text-right"><b><fmt:formatNumber currencySymbol="" value="${order.totalPrice}" type="currency"/></b></td>
                    </tr>
                    </tbody></table>
            </td>
        </tr>

        <tr><td>&nbsp;</td></tr>

        <tr>
            <td>
                Всего наименований 1, на сумму <fmt:formatNumber currencySymbol="" value="${order.totalPrice}" type="currency"/> руб.          <br>
                <b>${wordNumberSum}</b>
            </td>
        </tr>

        <tr>
            <td class="td-hr">&nbsp;</td>
        </tr>

        <tr><td>&nbsp;</td></tr>
        <tr><td>&nbsp;</td></tr>

        <tr>
            <td>
                <div class="signatures">
                    <img class="signet" src="https://s1.planeta.ru/i/129eff/1466683149385_renamed.jpg">
                    <table width="100%">
                        <tr>
                            <td>
                                Генеральный директор ООО "ГЛОБАЛ НЕТВОРКС" _____________________ (Мурачковский Ф.В.)
                            </td>
                        </tr>
                    </table>
                </div>
            </td>
        </tr>
        </tbody></table>

</div>
</body>
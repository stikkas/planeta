<%@ page import="java.io.PrintWriter" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@include file="/WEB-INF/jsp/includes/generated/doctype-header.jsp" %>
<head>
    <title>Planeta: Информация о заказе</title>
    <meta name="title" content="Planeta: Информация о заказе">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta charset="utf-8">

    <link type="text/css" rel="stylesheet" href="//${hf:getStaticBaseUrl("")}/css-generated/common.css"/>

    <script type="text/javascript">
        function printDocument() {
            setTimeout(function () {
                window.print();
            }, 1000);
        }
    </script>
    <link href="//${hf:getStaticBaseUrl("")}/css/bootstrap.min.css" rel="stylesheet">
    <link href="//${hf:getStaticBaseUrl("")}/css/print.css" rel="stylesheet">
</head>
<body class="coupon-page" onload="printDocument()">
<!-- Coupon-print -->
<div class="coupon-print">
    <div class="coupon-print-item clearfix">
        <div class="logo">
            <img src="//${hf:getStaticBaseUrl("")}/images/print-logotip.png" alt="print-logotip.png">
        </div>
        <div class="coupon-print-menu">
            <span>музыка</span>
            <span>эксклюзив</span>
            <span>софинасирование</span>
            <span>билеты</span>
            <span>магазин</span>
            <span>трансляции</span>
        </div>
    </div>
    <div class="coupon-print-item clearfix">
        <div class="info">
            <div class="info-item clearfix">
                    <span class="price">
                        Стоимость:
                        <b><fmt:formatNumber maxFractionDigits="2" minFractionDigits="0" value="${order.totalPrice}"/>
                            руб.</b>
                    </span>
                    <span>
                        Дата продажи:
                        <b><fmt:formatDate pattern="dd.MM.yyyy" value="${order.timeAdded}"/></b>
                    </span>
                    <span>
                        Покупатель:
                        <b><c:out value="${profile.displayName}"/></b>
                    </span>
                    <span class="number">
                        Номер заказа:
                        <b>${order.orderId}</b>
                    </span>
            </div>
        </div>
    </div>
    <div class="coupon-print-item clearfix">
        <div class="description">
            <p>

            </p>

            <div class="descr">
                <table class="table table-bordered">
                    <tbody>
                    <tr>
                        <th>наименование</th>
                        <th>цена</th>
                        <th>количество</th>
                        <th>стоимость</th>
                    </tr>
                    <c:forEach items="${groupedOrderObjects}" var="orderObjects" varStatus="groupStatus">
                        <c:set var="groupedObject" value="${orderObjects.value[0]}"/>
                        <c:set var="size" value="${groupedObject.count * fn:length(orderObjects.value)}"/>
                        <tr>
                            <td>${groupedObject.objectName}</td>
                            <td>${groupedObject.price} руб.</td>
                            <td>${size} шт.</td>
                            <td>${groupedObject.price * size} руб.</td>
                        </tr>
                    </c:forEach>
                    <c:if test="${not empty delivery}">
                        <tr>
                            <td colspan="3">
                                <c:out value="${delivery.name}"/>
                            </td>
                            <td>
                                <c:choose>
                                    <c:when test="${order.deliveryPrice > 0}">
                                        ${order.deliveryPrice} руб.
                                    </c:when>
                                    <c:otherwise>бесплатно</c:otherwise>
                                </c:choose>
                            </td>
                        </tr>
                    </c:if>
                    </tbody>
                </table>
            </div>
            <p></p>
        </div>
    </div>
    <c:if test="${isCancelled}">
        <div class="coupon-print-item clearfix">
            <div class="description">
                <div class="descr">
                    <h5>Заказ аннулирован.</h5>
                </div>
            </div>
        </div>
    </c:if>

    <div class="coupon-print-item clearfix">
        <div class="address">
            <table class="table order-address-table">
                <colgroup>
                    <col width="140">
                    <col width="100%">
                    <col width="140">
                </colgroup>
                <tbody>
                <tr>
                    <c:choose>
                        <c:when test="${order.deliveryType == \"CUSTOMER_PICKUP\"}">
                            <td>
                                Адрес самовывоза:
                            </td>
                            <td><b><c:out value="${deliveryAddress.address}"/></b>
                            </td>
                        </c:when>
                        <c:when test="${order.deliveryType == \"DELIVERY\" || order.deliveryType == \"RUSSIAN_POST\"}">
                            <td>
                                Адрес доставки:
                            </td>
                            <td><b><c:if test="${not empty deliveryAddress.zipCode}"><c:out value="${deliveryAddress.zipCode}"/>, </c:if>
                                <c:out value="${deliveryAddress.city}"/>, <c:out value="${deliveryAddress.address}"/>, <c:out value="${deliveryAddress.phone}"/></b>
                            </td>
                        </c:when>
                    </c:choose>
                </tr>
                <tr>
                    <c:if test="${not empty deliveryPublicNote}">
                        <td>
                            Примечание:
                        </td>
                        <td>
                            <c:out value="${deliveryPublicNote}"/>
                        </td>
                    </c:if>
                </tr>
                <c:if test="${order.deliveryType == \"DELIVERY\" || order.deliveryType == \"RUSSIAN_POST\"}">
                    <tr>
                        <td>
                            Получатель:
                        </td>
                        <td>
                            <c:out value="${deliveryAddress.fio}"/>
                        </td>
                    </tr>
                </c:if>
                <c:catch var="e">
                    <c:if test="${order.cashOnDeliveryCost != null && order.cashOnDeliveryCost > 0}">
                        <tr>
                            <td>
                                Наложенный платёж:
                            </td>
                            <td>
                                <fmt:formatNumber value="${order.cashOnDeliveryCost}" type="number"/> руб.
                            </td>
                        </tr>
                    </c:if>
                    <c:if test="${not empty order.trackingCode}">
                        <tr>
                            <td>
                                Почтовый идентификатор:
                            </td>
                            <td>
                                <c:out value="${order.trackingCode}"/>

                            </td>
                        </tr>
                    </c:if>
                    <c:if test="${not empty order.deliveryComment}">
                        <tr>
                            <td>
                                Комментарий:
                            </td>
                            <td>
                                <c:out value="${order.deliveryComment}"/>

                            </td>
                        </tr>
                    </c:if>
                </c:catch>
                <c:if test="${e != null}">
                    <!--
                    <%
                        Exception e = (Exception) pageContext.getAttribute("e");
                        e.printStackTrace(new PrintWriter(out));
                    %>
                    -->
                </c:if>
                </tbody>
            </table>
        </div>
    </div>
    <div class="print-bg">
        <c:if test="${not fn:contains(header['User-Agent'], 'Firefox') }">
            <img src="//${hf:getStaticBaseUrl("")}/images/print-bg.jpg" alt="print-bg.jpg">
        </c:if>
    </div>
</div>
</body>
</html>
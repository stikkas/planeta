<%@ include file="/WEB-INF/jsp/includes/generated/doctype-header.jsp" %>
<head>
    <meta charset="UTF-8">
    <title>Партнеры «Планеты»</title>
    <%@include file="/WEB-INF/jsp/includes/generated/stat-counters-async.jsp"%>
    <%@include file="/WEB-INF/jsp/includes/common-css.jsp" %>
    <%@include file="/WEB-INF/jsp/includes/generated/common-templates.jsp" %>
    <%@include file="/WEB-INF/jsp/includes/planeta-js.jsp" %>
</head>
<body>
<%@ include file="/WEB-INF/jsp/includes/generated/header.jsp" %>
<script id="partner-display-template" type="text/x-jquery-template">
    <a class="ap-item" href="{{= originalUrl}}" target="_blank" title="{{= name}}" rel="nofollow noopener">
        <img src="{{= ImageUtils.getThumbnailUrl(imageUrl, ImageUtils.MEDIUM, ImageType.PHOTO)}}" alt="{{= name}}">
    </a>
</script>

<script>
    var renderPartners = function(array) {
        _.forEach(array, function (info) {
            $('#partner-display-template').tmpl(info).appendTo('.aboute-partners-list');
        });
    };
    $(function() {
        renderPartners(${partnersAsJson});
    });
</script>
<div id="global-container">
    <div id="main-container" class="wrap-container">
        <div class="pln-content-box">
            <div class="wrap">
                <div class="col-9">
                    <div class="pln-content-box_cont wrap-indent-right-half">
                        <div class="aboute-content">
                            <h1>Партнеры «Планеты»</h1>
                            <div class="aboute-partners-list"></div>
                        </div>
                    </div>
                </div>
                <div class="col-3">
                    <%@include file="/WEB-INF/jsp/includes/about-menu.jsp" %>
                </div>
            </div>
        </div>
    </div>
</div>
<%@ include file="/WEB-INF/jsp/includes/generated/footer.jsp" %>
</body>
</html>
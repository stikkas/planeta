<?xml version="1.0" encoding="utf-8"?>
<%@ page language="java" contentType="application/xml; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="/WEB-INF/jsp/includes/taglibs.jsp" %>
<OpenSearchDescription xmlns="http://a9.com/-/spec/opensearch/1.1/">
    <ShortName>Planeta.ru</ShortName>
    <Image width="16" height="16">https://static.planeta.ru/images/favicons/favicon.ico</Image>
    <InputEncoding>utf-8</InputEncoding>
    <Url type="text/html" template="${mainAppUrl}/search/projects?query={searchTerms}" />
</OpenSearchDescription>

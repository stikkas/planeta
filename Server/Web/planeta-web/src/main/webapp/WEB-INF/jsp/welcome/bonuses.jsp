<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ include file="/WEB-INF/jsp/includes/generated/doctype-header.jsp" %>
<%@taglib prefix="p" uri="http://planeta.ru/tags" %>
<c:set var="baseUrl" value="${hf:getStaticBaseUrl('')}"/>
<head>
    <title>Программа лояльности для спонсоров - Planeta Club</title>
    <meta name="description" content="Поддерживайте проекты краудфандинга на Planeta.ru и получайте эксклюзивные подарки от наших авторов и партнеров.">
    <meta property="og:title" content="Программа лояльности для спонсоров - Planeta Club"/>
    <meta property="og:description" content="Поддерживайте проекты краудфандинга на Planeta.ru и получайте эксклюзивные подарки от наших авторов и партнеров.">
    <meta property="og:image" content="//${hf:getStaticBaseUrl("")}/images/vip-landing/ogimage.png" />

    <%@include file="/WEB-INF/jsp/includes/generated/common-favicon.jsp" %>
    <link type="text/css" rel="stylesheet" href="//${hf:getStaticBaseUrl("")}/css-generated/vip-landing.css"/>
    <%@include file="/WEB-INF/jsp/includes/generated/stat-counters-head.jsp" %>
    <p:script src="vip-landing.js"/>
</head>
<body class="winter-page">
    <div class="bg-block">
        <div class="bg-wrap">
            <div class="bg-gift"></div>
            <div class="bg-mug"></div>
        </div>
    </div>
    <div class="header cf">
        <div class="wrap">
            <div class="header-lead">
                Вы можете выбрать подарок
                <br>
                <span class="header-lead_b">прямо сейчас!</span>
            </div>

            <a class="pln-logo" href="https://planeta.ru/"></a>

            <div class="vip-logo"></div>
        </div>
    </div>
    <div class="wrap">
        <div class="header-title"><a href="https://planeta.ru/search/projects">Поддерживайте проекты</a></div>
        <div class="header-post-title">и получайте подарки!</div>

        <div class="grid-list clearfix">
            <c:forEach items="${bonuses}" var="b" varStatus="i">
                <c:choose>
                    <c:when test="${b.type == 'BONUS'}">
                        <div class="grid-i wow flipInY animated js-bonus <c:if test="${b.available == 0}">grid-i__sale</c:if>" 
                             data-id="${b.bonusId}"
                             data-wow-delay="${50 + hf:random(200)}ms"
                             style="visibility: visible;-webkit-animation-delay: 50ms; -moz-animation-delay: 50ms; animation-delay: 50ms;">
                            <div class="grid-img">
                                <img src="${hf:getThumbnailUrl(b.imageUrl, "ORIGINAL", "PHOTO")}">
                            </div>
                            <div class="grid-img-text">
                                <div class="grid-img-text_cont">
                                    <c:out value="${b.name}"/>
                                </div>
                                <c:choose>
                                    <c:when test="${not empty b.link}">
                                        <div class="grid-project-link">
                                            <a class="grid-project-link_btn btn" href="${b.link}">
                                                <c:choose>
                                                    <c:when test="${not empty b.buttonText}"><c:out value="${b.buttonText}"/></c:when>
                                                    <c:otherwise>Перейти в проект</c:otherwise>
                                                </c:choose>
                                            </a>
                                        </div>
                                    </c:when>
                                    <c:when test="${not empty b.campaignAlias}">
                                        <div class="grid-project-link">
                                            <a class="grid-project-link_btn btn" href="/campaigns/${b.campaignAlias}">
                                                <c:choose>
                                                    <c:when test="${not empty b.buttonText}"><c:out value="${b.buttonText}"/></c:when>
                                                    <c:otherwise>Перейти в проект</c:otherwise>
                                                </c:choose>
                                            </a>
                                        </div>
                                    </c:when>
                                </c:choose>
                            </div>
                            <c:if test="${b.available == 0}">
                                <div class="grid-sale">
                                    <div class="grid-sale_text">Эти подарки уже разобрали</div>
                                </div>
                            </c:if>
                        </div>
                    </c:when>
                    <c:otherwise>
                        ${b.descriptionHtml}
                    </c:otherwise>
                </c:choose>
            </c:forEach>
        </div>
    </div>

    <div class="projects clearfix">
        <div class="wrap">
            <div class="lead-text project-lead col-2">
                <span class="wow fadeInRight animated" data-wow-offset="200">Ещё не в клубе?</span>
                <br>
                <b class="wow fadeInRight animated" data-wow-delay="600ms" data-wow-offset="200"><a href="https://planeta.ru/search/projects">Поддерживайте проекты!</a></b>
            </div>
            <div class="lead-text project-lead col-2 hide">
                <span class="wow fadeInRight animated" data-wow-offset="200">Вы уже выбрали подарок!</span>
                <br>
                <b class="wow fadeInRight animated" data-wow-delay="600ms" data-wow-offset="200">Спасибо за участие</b>
            </div>
            <div class="project_list">
                <c:forEach items="${campaigns}" var="c" end="3" varStatus="i">
                    <a class="project_i wow rotateIn animated" data-wow-delay="${400 + i.index * 100}ms" data-wow-duration="500ms" href="/campaigns/${c.campaignId}"
                       <c:if test="${i.first}">data-wow-offset="400"</c:if>>
                           <span class="project_i-cont">
                               <span class="project_img"><img src="${hf:getProxyThumbnailUrl(c.imageUrl, 'BIG', 'GROUP', 344, 210, true, false)}"></span>
                           <span class="project_name"><c:out value="${c.name}"/></span>
                           <c:if test="${c.targetAmount > 0}">
                               <span class="project_progress">
                                   <span class="project_bar" style="width: ${c.collectedAmount > 0 ? c.collectedAmount / c.targetAmount * 100 : 0}%"></span>
                               </span>
                           </c:if>
                           <span class="project_target">
                               Собрано <fmt:formatNumber value="${c.collectedAmount}"/>
                               <c:if test="${c.targetAmount > 0}">из <fmt:formatNumber value="${c.targetAmount}"/></c:if>
                                   <span class="b-rub">Р</span>
                               </span>
                           </span>
                       </a>
                </c:forEach>
            </div>
        </div>
    </div>

    <div class="footer">&copy; 2018 Planeta.ru</div>

    <c:choose>
        <c:when test="${!isVip and hasBonuses}">
            <c:set var="conditionCount" scope="request" value="1"/>
            <c:set var="conditionAmount" scope="request" value="${vipCondition.second}"/>
        </c:when>
        <c:when test="${!isVip and !hasBonuses}">
            <c:set var="conditionCount" scope="request" value="${vipCondition.count}"/>
            <c:set var="conditionAmount" scope="request" value="${vipCondition.amount}"/>
        </c:when>
    </c:choose>

    <c:if test="${!isVip}">
        <div class="modal-dialog hide in" id="modal-attention" tabindex="-1" role="dialog" aria-hidden="false">
            <div class="modal modal-attention">
                <a class="close" data-dismiss="modal">×</a>
                <div class="attention">
                    <div class="attention_wrap cf">
                        <div class="attention_meta">
                            <c:if test="${purchasedCount < conditionCount || purchasedAmount < conditionAmount}">
                                <div class="attention_meta_lbl">Осталось сделать</div>
                                <c:if test="${purchasedCount < conditionCount && purchasedAmount < conditionAmount}">
                                    <div class="attention_meta_val">
                                        <c:set var="remainCount" scope="request" value="${conditionCount - purchasedCount}"/>
                                        <c:if test="${remainCount > 1}">
                                            ${remainCount} <spring:message code="decl.purchases.accusative" arguments="${hf:plurals(remainCount)}"/>
                                        </c:if>
                                    </div>
                                    <div class="attention_meta_lbl"><c:if test="${remainCount <= 1}"><spring:message code="decl.purchases.accusative" arguments="${hf:plurals(2)}"/></c:if> на сумму не менее</div>
                                        <div class="attention_meta_val">
                                        <fmt:formatNumber value="${conditionAmount - purchasedAmount}" pattern="#,##0"/>
                                        <span class="b-rub">Р</span>
                                    </div>
                                </c:if>
                                <c:if test="${purchasedAmount >= conditionAmount && purchasedCount < conditionCount}">
                                    <div class="attention_meta_val">
                                        ${conditionCount - purchasedCount} <spring:message code="decl.purchases.accusative" arguments="${hf:plurals(conditionCount - purchasedCount)}"></spring:message>
                                        </div>
                                </c:if>
                                <c:if test="${purchasedAmount < conditionAmount && purchasedCount >= conditionCount}">
                                    <div class="attention_meta_val"></div>
                                    <div class="attention_meta_lbl">покупки на сумму не менее</div>
                                    <div class="attention_meta_val">
                                        <fmt:formatNumber value="${conditionAmount - purchasedAmount}" pattern="#,##0"/>
                                        <span class="b-rub">Р</span>
                                    </div>
                                </c:if>
                                <div class="attention_meta_btn">
                                    <a class="content-btn content-btn-rich-white btn-block" href="/search/projects">
                                        <span class="content-btn-text">Выбрать и поддержать</span>
                                    </a>
                                </div>
                            </c:if>
                        </div>

                        <div class="attention_cont">
                            <div class="attention_cont_head">Вы в одном шаге...</div>
                            <div class="attention_cont_text">...от замечательных подарков, которые доступны только участникам
                                нашего клуба особых покупателей. Чтобы попасть в клуб и выбрать подарок вам необходимо совершить
                                <c:choose>
                                    <c:when test="${conditionCount > 1}">
                                        всего ${conditionCount} <spring:message code="decl.purchases.accusative" arguments="${hf:plurals(conditionCount)}"></spring:message> на общую
                                    </c:when>
                                    <c:otherwise>
                                        <spring:message code="decl.purchases.accusative" arguments="${hf:plurals(conditionCount + 1)}"></spring:message> на
                                    </c:otherwise>
                                </c:choose>
                                сумму ${conditionAmount} <spring:message code="decl.rubles" arguments="${hf:plurals(conditionAmount)}"></spring:message>.
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </c:if>

    <c:if test="${isVip}">
        <div class="modal-dialog hide" id="modal-attention" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal modal-attention-text">

                <a class="close" data-dismiss="modal">&times;</a>
                <div class="attention">
                    <div class="attention_wrap cf">
                        <div class="attention_cont">
                            <div class="attention_cont_head">Вы не можете выбрать еще один бонус</div>
                            <div class="attention_cont_text">
                                Для получения следующего подарка продолжайте активно участвовать в проектах на Планете и ждите приглашения в новый сезон клуба!
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </c:if>


    <div class="modal-dialog hide" id="modal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal modal-gift">

            <a class="close" data-dismiss="modal">&times;</a>

            <div class="modal-gift-block fade in">

                <div class="modal-gift-block_title js-modal-name"></div>

                <div class="modal-gift-block_cont clearfix">
                    <div class="modal-gift-block_img">
                        <img class="js-modal-img" src="">
                    </div>

                    <div class="modal-gift-block_descr js-modal-descr"></div>

                    <div class="modal-gift-block_action">
                        <a class="mgba-other-link" href="#" data-dismiss="modal">Выбрать другой подарок</a>
                        <a class="btn" href="#">Заказать!</a>
                    </div>
                </div>
                <div class="modal-gift-block_project js-project-footer">
                    <div class="modal-gift-block_project_name"><span class="js-button-text">Перейти в проект </span><a class="js-project-link" href=""></a></div>
                    <div class="modal-gift-block_project_bar js-project-progress">
                        <div class="modal-gift-block_project_progress js-progress-bar"></div>
                    </div>
                </div>

            </div>

            <form class="modal-form-block clearfix fade hide">
                <div class="pln-payment-box">
                    <div class="pln-payment-box_head">
                        СПОСОБ ДОСТАВКИ
                    </div>

                    <div class="pln-payment-box_field-group">
                        <div class="pln-payment-box_field-row">
                            <div class="pln-payment-box_delivery-type">
                                <div class="radio-row flat-ui-control js-delivery-toggle" onclick="order.deliveryType = 'DELIVERY'" data-target=".js-delivery-option-2">
                                    <span class="radiobox active"></span>
                                    <span class="radiobox-label">Доставка почтой РФ</span>
                                </div>
                                <div class="radio-row flat-ui-control js-delivery-toggle" onclick="order.deliveryType = 'CUSTOMER_PICKUP'" data-target=".js-delivery-option-1">
                                    <span class="radiobox"></span>
                                    <span class="radiobox-label">Самовывоз</span>
                                </div>
                            </div>
                        </div>

                        <div class="pln-payment-box_field-row js-delivery-option  js-delivery-option-1">
                            <div class="pln-payment-box_field-label">Ваше имя</div>
                            <div class="pln-payment-box_field-value">
                                <div class="input-field">
                                    <input class="pln-payment-box_field-input" type="text" name="recipientName" required="true" placeholder="Укажите Ваши ФИО">
                                </div>
                            </div>
                        </div>

                        <div class="pln-payment-box_field-row js-delivery-option  js-delivery-option-1">
                            <div class="pln-payment-box_field-label">Контактный телефон</div>
                            <div class="pln-payment-box_field-value">
                                <div class="input-field">
                                    <input class="pln-payment-box_field-input" type="text" name="recipientPhone" required="true" placeholder="Укажите контактный телефон">
                                </div>
                            </div>
                        </div>

                        <div class="pln-payment-box_field-row hide js-delivery-option js-delivery-option-1">
                            <div class="pln-payment-box_field-label">Адрес самовывоза</div>
                            <div class="pln-payment-box_field-value">
                                <div class="pln-payment-box_field-text">
                                    ${properties['shop.address']}
                                    <br>
                                    +7 (495) 181-05-05
                                </div>
                            </div>
                        </div>

                        <div class="pln-payment-box_field-row js-delivery-option  js-delivery-option-2">
                            <div class="pln-payment-box_field-label">Страна</div>
                            <div class="pln-payment-box_field-value">
                                <div class="input-field">
                                    <input class="pln-payment-box_field-input" name="country" required="true" type="text" placeholder="Укажите название страны">
                                </div>
                            </div>
                        </div>

                        <div class="pln-payment-box_field-row js-delivery-option  js-delivery-option-2">
                            <div class="pln-payment-box_field-label">Населенный пункт</div>
                            <div class="pln-payment-box_field-value">
                                <div class="input-field">
                                    <input class="pln-payment-box_field-input" type="text" name="city" required="true" placeholder="Укажите название населенного пункта">
                                </div>
                            </div>
                        </div>

                        <div class="pln-payment-box_field-row js-delivery-option  js-delivery-option-2">
                            <div class="pln-payment-box_field-label">Адрес</div>
                            <div class="pln-payment-box_field-value">
                                <div class="input-field">
                                    <input class="pln-payment-box_field-input" type="text" name="address" required="true" placeholder="Укажите улицу, дом, корпус, квартиру">
                                </div>
                            </div>
                        </div>

                        <div class="pln-payment-box_field-row js-delivery-option  js-delivery-option-2">
                            <div class="pln-payment-box_field-label">Индекс</div>
                            <div class="pln-payment-box_field-value">
                                <div class="input-field">
                                    <input class="pln-payment-box_field-input" type="text" name="postIndex" required="true" placeholder="Укажите почтовый индекс получателя">
                                </div>
                            </div>
                        </div>

                        <div class="pln-payment-box_field-row js-delivery-option  js-delivery-option-2 hide">
                            <div class="pln-payment-box_field-label">Получатель</div>
                            <div class="pln-payment-box_field-value">
                                <div class="input-field">
                                    <input class="pln-payment-box_field-input" type="text" name="recipientName" required="true" placeholder="Укажите ФИО получателя">
                                </div>
                            </div>
                        </div>

                        <div class="pln-payment-box_field-row js-delivery-option  js-delivery-option-2 hide">
                            <div class="pln-payment-box_field-label">Контактный телефон</div>
                            <div class="pln-payment-box_field-value">
                                <div class="input-field">
                                    <input class="pln-payment-box_field-input" type="text" name="recipientPhone" required="true" placeholder="Укажите телефон получателя">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <input type="submit" value="Оформить бонус" class="btn btn-primary pull-right">
            </form>

            <div class="modal-success clearfix fade hide js-success">
                <h2><b>Бонус ждёт Вас!</b></h2>
                <p>В течение одного рабочего дня с вами свяжется куратор программы и уточнит детали получения бонуса.</p>
            </div>
            <div class="modal-success clearfix fade hide js-error">
                <div class="content-alert-error">
                    <i class="three-types-alerts-error"></i>
                    <p class="big">К сожалению, этот бонус закончился</p>
                    <p>Извините, но бонус, который вы выбрали, только что закончился. Вы можете вернуться и выбрать другой бонус.</p>
                </div>
            </div>
        </div>
    </div>

    <script>
        var site = {
            staticHost: '${hf:getStaticBaseUrl("")}',
            profileId: ${myProfile.profile.profileId}
        };
        var order = {deliveryType: 'DELIVERY'};
        $(function () {
            new WOW().init();

            // parallax
            var bgParallax = {
                init: function () {
                    var body = $('body');
                    var gift = $('.bg-gift');
                    var giftPos = gift.position().top;
                    var mug = $('.bg-mug');
                    var mugPos = mug.position().top;

                    $(window).scroll(function () {
                        var winTop = $(window).scrollTop();
                        gift.css({top: giftPos + winTop / 2.4});
                        mug.css({top: mugPos + winTop / 1.6});
                    }).trigger('scroll');
                }
            };
            bgParallax.init();
            var wrapBlock = $('.modal-gift');
            var giftBlock = $('.modal-gift-block');
            var formBlock = $('.modal-form-block');
            var successBlock = $('.js-success');
            var errorBlock = $('.js-error');

            formBlock.find('input[name]').change(function () {
                var self = $(this);
                order[self.attr('name')] = self.val();
            });
            formBlock.submit(function () {
                order.valid = true;
                return false;
            });

            var bonuses = $('.js-bonus');
            function showAlert() {
                $('#modal-attention').modal('show');
            }
        <c:choose>
            <c:when test="${isVip}">
            var modal = $('#modal');
            var bonus;
            var delivery = $('.js-delivery-toggle');
            var options = $('.js-delivery-option');
            var inputs = options.find('input');
            delivery.click(function () {
                showTab($(this));
            });
            var showTab = function (el) {
                delivery.find('.radiobox.active').removeClass('active');
                el.find('.radiobox').addClass('active');
                options.addClass('hide');
                inputs.attr('disabled', true);
                var target = options.filter(el.attr('data-target'));
                target.removeClass('hide');
                target.find('input').removeAttr('disabled');
            };
            bonuses.click(function () {
                var that = $(this);
                order = $.extend(order, {bonusId: that.attr('data-id'), profileId: site.profileId});
                $.get('/api/get-bonus.json', {bonusId: that.attr('data-id')}, function (response) {
                    bonus = response;
                    if (bonus && bonus.available) {
                        modal.modal('show');
                        modal.find('.js-modal-name').text(bonus.name);
                        modal.find('.js-modal-img').attr('src', bonus.imageUrl);
                        modal.find('.js-modal-descr').text(bonus.description);
                        if (bonus.campaignAlias || bonus.link) {
                            modal.find('.js-project-footer').show();
                            if (bonus.linkText) {
                                modal.find('.js-button-text').hide();
                            } else {
                                modal.find('.js-button-text').show();
                            }
                            modal.find('.js-project-link').text(bonus.linkText || bonus.campaignName).attr('href', bonus.link || ('/campaigns/' + bonus.campaignAlias));
                            if (bonus.campaignProgress) {
                                modal.find('.js-project-progress').show();
                                modal.find('.js-progress-bar').css('width', bonus.campaignProgress + '%');
                            } else {
                                modal.find('.js-project-progress').hide();
                            }
                        } else {
                            modal.find('.js-project-footer').hide();
                        }
                    } else {
                        giftBlock.addClass('hide');
                        errorBlock.removeClass('hide');
                    }
                });
            });


            giftBlock.find('.btn').click(function (e) {
                e.preventDefault();
                if (bonus && bonus.available) {
                    showTab(delivery.first());
                    transitionSteps({
                        wrap: wrapBlock,
                        step1: giftBlock,
                        step2: formBlock
                    });
                } else {
                    transitionSteps({
                        wrap: wrapBlock,
                        step1: giftBlock,
                        step2: errorBlock
                    });
                }
            });

            var btnPrimary = formBlock.find('.btn');
            btnPrimary.click(function () {
                btnPrimary.addClass('disabled');
                setTimeout(function () {
                    if (order.valid) {
                        $.post('/api/bonuses/create-order.json', order, function (response) {
                            btnPrimary.removeClass('disabled');
                            if (response.success) {
                                $('.project-lead').toggleClass('hide');
                                bonuses.off('click').on('click', showAlert);
                                transitionSteps({
                                    wrap: wrapBlock,
                                    step1: formBlock,
                                    step2: successBlock
                                });
                            } else {
                                transitionSteps({
                                    wrap: wrapBlock,
                                    step1: formBlock,
                                    step2: errorBlock
                                });
                            }
                        });
                    }
                }, 0);
            });
            modal.on('hide.pln.modal', function () {
                $('.modal').removeClass('size-transition').css({width: '', height: ''});
                giftBlock.removeClass('hide').addClass('in');
                formBlock.addClass('hide').removeClass('in');
                errorBlock.addClass('hide').removeClass('in');
                successBlock.addClass('hide').removeClass('in');
            });
            </c:when>
            <c:otherwise>
            bonuses.click(showAlert);
            </c:otherwise>
        </c:choose>
        });
    </script>
</body>
</html>

<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ include file="/WEB-INF/jsp/includes/taglibs.jsp" %>
<%@ page pageEncoding="UTF-8"%>
<% response.getWriter().print('\ufeff'); %>

Название вознаграждения ; Куплено (шт.); Собрано (руб.)
<c:forEach var="share" items="${shares}">${hf:csvEscapeString(share.name)};${share.purchaseCount};<fmt:formatNumber value="${share.purchaseSum}"/>
</c:forEach>
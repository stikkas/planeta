<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ include file="/WEB-INF/jsp/includes/generated/doctype-header.jsp" %>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta property="og:site_name" content="planeta.ru"/>
    <meta property="og:title" content="<spring:message code="payment-success.jsp.propertie.1" text="default text"> </spring:message><c:out value="${firstObject.ownerName}"/>"/>
    <meta property="og:image" content="${hf:getThumbnailUrl(firstObject.objectImageUrl, "ORIGINAL", "PRODUCT")}"/>
    <meta property="og:description" content="${hf:escapeEcmaScript(sharingDescription)}"/>
    <title><spring:message code="payment-success.jsp.propertie.2" text="default text"> </spring:message></title>
    <%@include file = "/WEB-INF/jsp/includes/common-css.jsp" %>
    <link type="text/css" rel="stylesheet" href="//${hf:getStaticBaseUrl("")}/css-generated/payment.css"/>
    <%@include file="/WEB-INF/jsp/includes/generated/stat-counters-head.jsp" %>
    <%@include file="/WEB-INF/jsp/includes/planeta-js.jsp" %>
    <%@include file="/WEB-INF/jsp/includes/generated/common-templates.jsp" %>


</head>
<body>

<%@ include file="/WEB-INF/jsp/includes/generated/screen-width.jsp" %>
<%--Start header reg confirm --%>
<%@ include file="/WEB-INF/jsp/includes/generated/header-reg-confirm.jsp"%>
<%@ include file="/WEB-INF/jsp/includes/generated/special-project.jsp"%>
<%--End header reg confirm--%>

<div id="page-head" class="header">


    <div class="wrap"><div class="col-12">
        <!-- Start header services menu -->
        <%@ include file="/WEB-INF/jsp/includes/generated/header-menu.jsp" %>
        <!-- End header services menu -->

        <!-- Start header info -->
        <%@ include file="/WEB-INF/jsp/includes/generated/header-inner.jsp" %>
        <!-- End header info -->
    </div></div>
</div>
<%@ include file="/WEB-INF/jsp/includes/generated/mobile-header.jsp"%>


<script>
    function timerCounter(sec) {
        $("#timer-count")[0].innerHTML = StringUtils.declOfNumWithNum(sec, ["секунду","секунды","секунд"]);
        if(sec) {            
            setTimeout(function() {
                timerCounter(sec-1);
            }, 1000);
        } else {     
            $("#download-link")[0].click();   
        }
    }
    
</script>

<script type="text/javascript">
    $(function() {
        var getDataForShare = {
            url : "https://${properties['application.host']}/campaigns/${firstObject.ownerId}",
            title: '<spring:message code="payment-success.jsp.propertie.1" text="default text"> </spring:message> ${hf:escapeEcmaScript(firstObject.ownerName)}',
            className: 'horizontal donate-sharing sharing-mini', counterEnabled: false, hidden: false,
            description: '${hf:escapeEcmaScript(sharingDescription)}',
            imageUrl: '${hf:getThumbnailUrl(firstObject.objectImageUrl, "ORIGINAL", "PRODUCT")}'
        };
        $('.share-cont-horizontal').share(getDataForShare);
    });
    $(document).ready(function() {
        if (window.gtm) {
            /*
            TODO Переделать трекинг под инвестпроекты
            
            var transactionInfo = {
                transactionId: '${transaction.transactionId}',
                amount: '${order.totalPrice}'
            };
            var shareInfo = {
                shareId: '${firstObject.objectId}',
                name: '${firstObject.objectName}',
                price: '${firstObject.price}',
                quantity: '${firstObject.count}'
            };
            window.gtm.trackPurchaseShares(transactionInfo, ${hf:toJson(gtmCampaignInfo)}, shareInfo);
            */
        }

        timerCounter(${autoLoadDelay});

        <c:if test="${properties['investing.related.enabled']}">

        var relatedCampaigns = new (BaseModel.extend({
            url: "/search-projects.json"
        }))();

        relatedCampaigns.fetch({
            data: {
                categories: "INVESTING",
                offset: 0,
                limit: 3              
            }
        }).done(function(){
                moduleLoader.loadModule('welcome').done(function () {
                    var campaignsList = new Welcome.Views.TopCampaigns({
                        el: '.js-project-card-list-container div',
                        collection: new BaseCollection(relatedCampaigns.get("searchResultRecords"))
                    });
                    
                    campaignsList.render();
                });
        });

        </c:if>
    });
</script>

<div id="global-container">
    <div id="main-container" class="wrap-container">
        <div id="center-container" class="wrap">
            <div class="col-12">
                <div class="pln-payment-box">
                    <div class="pln-payment-success">
                        <div class="pln-payment-success_head">
                            Вам выставлен счёт
                        </div>
                        <div class="pln-payment-success_sub-head">
                            Через <span id="timer-count"></span> должно начаться скачивание счёта.
                            <br>
                            Если этого не произошло, скачайте <a id="download-link" href="/welcome/invoice/pdf.html?transactionId=${transaction.transactionId}">счёт по ссылке</a>.
                        </div>


                        <div class="pln-payment-success_widget cf mrg-t-50">
                            <div class="pln-payment-success_widget_i">
                                <div class="pln-payment-success_widget_ico">
                                    <span class="success-widget-icon success-widget-icon-mail-r"></span>
                                </div>
                                <div class="pln-payment-success_widget_text">
                                    Кроме того мы&nbsp;отправили счёт на&nbsp;вашу электронную почту.
                                </div>
                            </div>

                            <div class="pln-payment-success_widget_i">
                                <div class="pln-payment-success_widget_ico">
                                    <span class="success-widget-icon success-widget-icon-user-r"></span>
                                </div>
                                <div class="pln-payment-success_widget_text">
                                    Вы&nbsp;всегда можете найти этот счёт и&nbsp;узнать статус оплаты в&nbsp;<a href="/account">личном кабинете</a>.
                                </div>
                            </div>

                            <div class="pln-payment-success_widget_i">
                                <div class="pln-payment-success_widget_ico">
                                    <span class="success-widget-icon success-widget-icon-question-r"></span>
                                </div>
                                <div class="pln-payment-success_widget_text">
                                    Больше об&nbsp;инвестировании вы&nbsp;можете узнать в&nbsp;разделе <a href="/faq">Вопросы и&nbsp;ответы</a>.
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                
                <div class="pln-payment-box">
                    <div class="pln-payment-box_head">
                        Реквизиты для оплаты
                    </div>

                    <div class="pln-payment-box_field-group">

                        <div class="pln-payment-box_field-row">
                            <div class="pln-payment-box_field-label">
                                Реквизиты <nobr>ООО «Глобал Нетворкс»</nobr>
                            </div>
                            <div class="pln-payment-box_field-value pln-payment-box_field-pdg">
                                Банк получателя: АБ "ИНТЕРПРОГРЕССБАНК" (ЗАО), г. Москва, Старокаширское ш., д. 2, корп. 1, стр.1
                                <br>
                                ИНН: 7722724252
                                <br>
                                БИК: 044525402
                                <br>
                                Корреспондентский счет (к/с): 30101810100000000402
                                <br>
                                Рассчетный счет (р/с): 40702810500310000443
                                <br>
                                Наименование получателя: ООО "ГЛОБАЛ НЕТВОРКС"
                                <br>
                                Юр. адрес: 129090, г. Москва, Проспект Мира, д.19, строение 3,  пом. VIII
                            </div>
                        </div>

                        <div class="pln-payment-box_field-row">
                            <div class="pln-payment-box_field-label">
                                Основание платежа
                            </div>
                            <div class="pln-payment-box_field-value pln-payment-box_field-pdg">
                                Оплата по договору инвестирования №${transaction.transactionId} в пользу проекта «${campaign.name}» (№${campaign.campaignId})
                            </div>
                        </div>

                        <div class="pln-payment-box_field-row pln-payment-box_field-pdg">
                            Вы всегда можете узнать реквизиты для оплаты в вашем <a href="/account/billings">личном кабинете</a>, нажав на соответствующий счёт.
                        </div>

                    </div>
                </div>

                <c:if test="${properties['investing.related.enabled']}">
                <div class = "project-related">
                    <div class="project-related-head">
                        Другие инвестиционные проекты
                    </div>
                    <div class = "js-project-card-list-container">
                        <div class="project-card-list"></div>
                    </div>
                </div>
                </c:if>

            </div>
        </div>
    </div>
</div>
<%@include file="/WEB-INF/jsp/includes/generated/footer.jsp"%>
</body>
</html>

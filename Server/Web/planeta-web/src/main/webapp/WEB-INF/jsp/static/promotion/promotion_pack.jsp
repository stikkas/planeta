<%@ include file="/WEB-INF/jsp/includes/generated/doctype-header.jsp" %>
<head>
    <%@include file="/WEB-INF/jsp/includes/generated/stat-counters-async.jsp"%>
    <%@include file="/WEB-INF/jsp/includes/common-css.jsp" %>
    <link type="text/css" rel="stylesheet" href="//${hf:getStaticBaseUrl("")}/css-generated/faq.css" />
    <%@include file="/WEB-INF/jsp/includes/generated/common-templates.jsp" %>
    <%@include file="/WEB-INF/jsp/includes/planeta-js.jsp" %>
</head>
<body class="faq-page">
<%@include file="/WEB-INF/jsp/includes/generated/irma/header.jsp" %>
<%@include file="/WEB-INF/jsp/includes/generated/irma/main-nav.jsp" %>
<script type="text/javascript">
    function goBack() {
        window.history.back();
    }
</script>
<div id="global-container">
    <div id="main-container" class="wrap-container">

        <div class="cont-header">
            <div class="cont-header_row">
                <div class="wrap">
                    <div class="col-12">
                        <a class="cont-header_back" onclick="goBack()"></a>
                        <div class="cont-header_title">
                            Сравнение пакетов продвижения
                        </div>

                    </div>
                </div>
            </div>
        </div>

        <div id="center-container">


            <div class="faq-content">
                <div class="wrap">
                    <div class="col-12">

                        <div class="promo-pack">

                            <div class="promo-pack_text">
                                <p>Вы можете выбрать один из двух пакетов услуг по подготовке и продвижению проектов.</p>
                                <p><b>Пакет «Стартовый»</b> подойдет вам, если вы имеете необходимые знания, возможности и ресурсы для продвижения вашего проекта, и вам необходимы только услуги по грамотному оформлению проекта на платформе.</p>
                                <p><b>Пакет «Персональный менеджер»</b> подразумевает работу с вашим проектом на всех этапах - от проработки идеи и оформления на платформе до планирования и реализации продвижения. Необходимо учитывать, что основная аудитория вашего проекта находится на ваших персональных ресурсах, поэтому без вашего участия работа по продвижению проекта невозможна. Мы же сделаем все необходимое для того, чтобы эта работа была наиболее эффективной (разработаем план, подготовим публикации, информационные поводы и т.д.)</p>
                            </div>

                            <table class="promo-pack_table table table-bordered table-fixed">
                                <colgroup>
                                    <col width="25%">
                                    <col width="25%">
                                    <col width="25%">
                                    <col width="25%">
                                </colgroup>
                                <thead>
                                <tr>
                                    <td>Пакеты продвижения</td>
                                    <th>Стандартные условия</th>
                                    <th>«Стартовый»</th>
                                    <th>«Персональный<br>менеджер»</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <th>
                                        <div class="promo-pack_title">
                                            Менеджер проекта
                                        </div>
                                    </th>
                                    <td>
                                        <div class="promo-pack_table-text">Взаимодействие с&nbsp;менеджерами платформы по&nbsp;вопросам доработки проекта (минимальные правки для&nbsp;соответствия правилам размещения на&nbsp;портале) и&nbsp;подписания документов.</div>
                                    </td>
                                    <td>
                                        <div class="promo-pack_table-text">За&nbsp;проектом закрепляется персональный менеджер только на&nbsp;этапе оформления и&nbsp;подготовки к&nbsp;запуску проекта. По&nbsp;всем остальным вопросам автор может обращаться в&nbsp;службу поддержки.</div>
                                    </td>
                                    <td>
                                        <div class="promo-pack_table-text">За&nbsp;проектом <strong>закрепляется персональный менеджер</strong> (куратор), который будет консультировать по&nbsp;всем вопросам на&nbsp;каждом этапе работы над&nbsp;проектом с&nbsp;учетом его специфики. Автор получает возможность поставить e-mail куратора в&nbsp;поле «Контакты» (вопросы от&nbsp;пользователей будут приходить напрямую куратору).</div>
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        <div class="promo-pack_title">
                                            Служба поддержки
                                            <div class="promo-pack_help tooltip" data-tooltip="Консультации и ответы на вопросы, возникающие в процессе работы над проектом, в т.ч. стандартные советы по продвижению, добавление новых вознаграждений. Время работы – по будням с 11.00 до 20.00."></div>
                                        </div>
                                    </th>
                                    <td>
                                        <span class="s-promo-pack-yes"></span>
                                    </td>
                                    <td>
                                        <span class="s-promo-pack-yes"></span>
                                    </td>
                                    <td>
                                        <span class="s-promo-pack-yes"></span>
                                    </td>
                                </tr>
                                <tr class="promo-pack_block-head">
                                    <td colspan="4">Создание и оформление страницы проекта</td>
                                </tr>
                                <tr>
                                    <th>
                                        <div class="promo-pack_title">
                                            Название и описание проекта
                                            <div class="promo-pack_help tooltip" data-tooltip="Грамотное составление названия, полного и краткого описания проекта на основании информации, предоставленной автором; копирайтинг, креатив, дизайн, базовая обработка фотографий."></div>
                                        </div>
                                    </th>
                                    <td>
                                        <span class="s-promo-pack-no"></span>
                                    </td>
                                    <td>
                                        <span class="s-promo-pack-yes"></span>
                                    </td>
                                    <td>
                                        <span class="s-promo-pack-yes"></span>
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        <div class="promo-pack_title">
                                            Вознаграждения проекта
                                            <div class="promo-pack_help tooltip" data-tooltip="Рекомендации по составлению вознаграждений проекта с учетом вопросов доставки, копирайтинг, креатив."></div>
                                        </div>
                                    </th>
                                    <td>
                                        <span class="s-promo-pack-no"></span>
                                    </td>
                                    <td>
                                        <span class="s-promo-pack-yes"></span>
                                    </td>
                                    <td>
                                        <span class="s-promo-pack-yes"></span>
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        <div class="promo-pack_title">
                                            FAQ на странице проекта
                                            <div class="promo-pack_help tooltip" data-tooltip="Ответы на часто задаваемые вопросы с учетом специфики проекта. За основу берутся реальные вопросы пользователей."></div>
                                        </div>
                                    </th>
                                    <td>
                                        <span class="s-promo-pack-no"></span>
                                    </td>
                                    <td>
                                        <span class="s-promo-pack-yes"></span>
                                    </td>
                                    <td>
                                        <span class="s-promo-pack-yes"></span>
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        <div class="promo-pack_title">
                                            Видеообращение
                                            <div class="promo-pack_help tooltip" data-tooltip="Съемка и монтаж простого видеообращения (до 3 мин.). Примеры (ссылки)."></div>
                                        </div>
                                    </th>
                                    <td colspan="3">
                                        Стоимость по запросу
                                    </td>
                                </tr>


                                <tr class="promo-pack_block-head">
                                    <td colspan="4">Продвижение и сопровождение проекта</td>
                                </tr>
                                <tr>
                                    <th>
                                        <div class="promo-pack_title">
                                            Разработка стратегии продвижения
                                            <div class="promo-pack_help tooltip" data-tooltip="Оптимизация цели проекта и посыла аудитории, собираемой суммы, сроков."></div>
                                        </div>
                                    </th>
                                    <td>
                                        <span class="s-promo-pack-no"></span>
                                    </td>
                                    <td>
                                        <span class="s-promo-pack-no"></span>
                                    </td>
                                    <td>
                                        <span class="s-promo-pack-yes"></span>
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        <div class="promo-pack_title">
                                            Разработка плана продвижения
                                            <div class="promo-pack_help tooltip" data-tooltip="Подготовка списка ресурсов, на которых можно разместить информацию о проекте; разработка примерного плана продвижения на каждый день для всего срока проекта (время публикации новостей, список информационных поводов, добавление новых вознаграждений и т.д.)."></div>
                                        </div>
                                    </th>
                                    <td colspan="2">
                                        <div>Стандартные рекомендации</div>
                                        <div class="promo-pack_table-text-add">
                                            автоматическая рассылка на основе динамики сборов<br>+ <a href="https://planeta.ru/faq/article/12!paragraph105" target="_blank">общий план продвижения</a>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="promo-pack_table-text">
                                            Составление плана продвижения с учетом специфики проекта + корректирование плана по ходу реализации и в случае продления проекта
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        <div class="promo-pack_title">
                                            Обновление проекта
                                            <div class="promo-pack_help tooltip" data-tooltip="Подготовка контента для новостей и их автоматическая рассылка подписчикам проекта на Planeta.ru. Подготовка контента, касающегося проекта, для соцсетей."></div>
                                        </div>
                                    </th>
                                    <td>
                                        <span class="s-promo-pack-no"></span>
                                    </td>
                                    <td>
                                        <span class="s-promo-pack-no"></span>
                                    </td>
                                    <td>
                                        <div class="promo-pack_table-text">
                                            Раз в неделю на основании фактического материала, предоставленного автором и/или при наличии информационных поводов
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        <div class="promo-pack_title">
                                            Создание аватаров для&nbsp;соцсетей
                                            <div class="promo-pack_help tooltip" data-tooltip="Подготовка дизайнерами «Планеты» аватаров для ВКонтакте и Facebook, а также картинок для Instagram и постов в соцсетях с иллюстрацией проекта, слоганом («Поддержи проект») и логотипом."></div>
                                        </div>
                                    </th>
                                    <td>
                                        <span class="s-promo-pack-no"></span>
                                    </td>
                                    <td>
                                        <span class="s-promo-pack-no"></span>
                                    </td>
                                    <td>
                                        <span class="s-promo-pack-yes"></span>
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        <div class="promo-pack_title">
                                            Мониторинг, статистика
                                            <div class="promo-pack_help tooltip" data-tooltip="Оценка количества просмотров страницы проекта и источников, откуда совершены переходы на эту страницу."></div>
                                        </div>
                                    </th>
                                    <td colspan="2">
                                        Статистика в личном кабинете
                                    </td>
                                    <td>
                                        <div class="promo-pack_table-text">
                                            Мониторинг эффективности продвижения с учетом более подробной
                                            статистики платформы, выработка рекомендаций по дальнейшей работе с проектом.
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        <div class="promo-pack_title">
                                            Написание <nobr>пресс-релиза</nobr>
                                            <div class="promo-pack_help tooltip" data-tooltip="Написание пресс-релиза PR-менеджером «Планеты»."></div>
                                        </div>
                                    </th>
                                    <td colspan="2">
                                        <div>2 000 руб. за пресс-релиз</div>
                                        <div class="promo-pack_table-text-add">рассылка не предусмотрена</div>
                                    </td>
                                    <td>
                                        <div class="promo-pack_table-text">
                                            <div>2 пресс-релиза + рассылка по нашей базе СМИ</div>
                                            <div class="promo-pack_table-text-add">без гарантии публикации</div>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        <div class="promo-pack_title">
                                            Работа с профильными сообществами
                                            <div class="promo-pack_help tooltip" data-tooltip="Публикация информации о проекте в профильных сообществах социальных сетей, на сайтах по схожей тематике, профильных форумах, площадках блогеров и лидеров мнений."></div>
                                        </div>
                                    </th>
                                    <td>
                                        <span class="s-promo-pack-no"></span>
                                    </td>
                                    <td>
                                        <span class="s-promo-pack-no"></span>
                                    </td>
                                    <td>
                                        <div>
                                            <span class="s-promo-pack-yes"></span>
                                        </div>
                                        <div class="promo-pack_table-text-add">без гарантии публикации</div>
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        <div class="promo-pack_title">
                                            Продвижение в рамках платформы
                                            <div class="promo-pack_help tooltip" data-tooltip="Публикация информации о проекте в блоге «Планеты», в наших сообществах в соц. сетях, в еженедельном дайджесте. Размещение баннеров на платформе."></div>
                                        </div>
                                    </th>
                                    <td colspan="2">
                                        На выбор редакции
                                    </td>
                                    <td>
                                        <div class="promo-pack_table-text">
                                            Гарантированное размещение материалов в&nbsp;соответствии с&nbsp;планом продвижения
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        <div class="promo-pack_title">
                                            Таргетированная (целевая) рассылка пользователям Planeta.ru
                                            <div class="promo-pack_help tooltip" data-tooltip="Подготовка рассылки с информацией о проекте для пользователей «Планеты» (копирайтинг, дизайн, верстка). Целевая аудитория выбирается на основании различных критериев и согласовывается с автором."></div>
                                        </div>
                                    </th>
                                    <td colspan="2">
                                        <div>5 руб. / адрес</div>
                                        <div class="promo-pack_table-text-add">Минимальный заказ – от 10 000 руб.</div>
                                    </td>
                                    <td>
                                        <div>1 рассылка</div>
                                        <div class="promo-pack_table-text-add">Минимум 20 000 адресов</div>
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        <div class="promo-pack_title">
                                            Медийная реклама на&nbsp;портале Planeta.ru
                                            <div class="promo-pack_help tooltip" data-tooltip="Размещение рекламных баннеров. Возможности размещения описаны в нашем медиаките (ссылка)."></div>
                                        </div>
                                    </th>
                                    <td>
                                        Стандартные условия
                                    </td>
                                    <td>
                                        Скидка до 50% на&nbsp;размещение
                                    </td>
                                    <td>
                                        Скидка до 70% на&nbsp;размещение
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        <div class="promo-pack_title">
                                            Составление медиаплана и бюджета интернет-рекламы
                                            <div class="promo-pack_help tooltip" data-tooltip="Предоставление возможности размещения рекламы на различных ресурсах (размещение оплачивает автор проекта по расценкам площадок), копирайтинг и разработка дизайна."></div>
                                        </div>
                                    </th>
                                    <td>
                                        <span class="s-promo-pack-yes"></span>
                                    </td>
                                    <td>
                                        <span class="s-promo-pack-yes"></span>
                                    </td>
                                    <td>
                                        <span class="s-promo-pack-yes"></span>
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        <div class="promo-pack_title">
                                            Организация <nobr>промо-мероприятий</nobr>
                                            <div class="promo-pack_help tooltip" data-tooltip="Организация трансляции, предпрослушивания, творческой встречи, точки продаж и других мероприятий."></div>
                                        </div>
                                    </th>
                                    <td colspan="3">
                                        Стоимость по запросу
                                    </td>
                                </tr>


                                <tr class="promo-pack_block-head">
                                    <td colspan="4">Реализация успешного проекта</td>
                                </tr>
                                <tr>
                                    <th>
                                        <div class="promo-pack_title">
                                            Создание цифровой версии продукта
                                            <div class="promo-pack_help tooltip" data-tooltip="Разработка персональной страницы цифрового продукта (аудио или видео) на Planeta.ru. Контент будет доступен только спонсорам, купившим соответствующие вознаграждения, с возможностью скачивания или без - по выбору автора. Кроме этого, со страницы цифровой версии спонсоры смогут установить приложение на свой компьютер для просмотра цифрового продукта без захода на платформу. Пример: (ссылка)."></div>
                                        </div>
                                    </th>
                                    <td colspan="3">
                                        15 000 руб.
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        <div class="promo-pack_title">
                                            Изготовление бонусов
                                            <div class="promo-pack_help tooltip" data-tooltip="Изготовление футболок, кружек, флэшек и других сувениров с авторским дизайном, печать дисков, изготовление винила, 3D-печать и т.д."></div>
                                        </div>
                                    </th>
                                    <td colspan="3">
                                        Стоимость по запросу
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        <div class="promo-pack_title">
                                            Выдача и рассылка бонусов (вознаграждений)
                                            <div class="promo-pack_help tooltip" data-tooltip="Выдача и рассылка вознаграждений спонсорам проекта. Для этого автору необходимо передать бонусы в офис Planeta.ru."></div>
                                        </div>
                                    </th>
                                    <td colspan="2">
                                        Осуществляется по действующим расценкам на отправку вознаграждений менеджерами Planeta.ru (ссылка)
                                    </td>
                                    <td>
                                        <div class="promo-pack_table-text">
                                            Организация пункта самовывоза в Москве, рассылка в регионы по действующим расценкам на отправку вознаграждений менеджерами Planeta.ru (ссылка)
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        <div class="promo-pack_title">
                                            Реализация ваших товаров в нашем интернет-магазине
                                            <div class="promo-pack_help tooltip" data-tooltip="Реализация готового продукта в магазине Planeta.ru после завершения проекта. Мы берем на реализацию только эксклюзивные продукты, которые нельзя приобрести в другом месте."></div>
                                        </div>
                                    </th>
                                    <td colspan="2">
                                        Комиссия составляет 40% от стоимости продукта в магазине
                                    </td>
                                    <td>
                                        <div class="promo-pack_table-text">
                                            Комиссия составляет 35% от стоимости продукта в магазине
                                        </div>
                                    </td>
                                </tr>

                                <tr>
                                    <th>
                                        <div class="promo-pack_title">
                                            Стоимость пакета
                                        </div>
                                    </th>
                                    <td>
                                        <div class="promo-pack_red">Бесплатно</div>
                                    </td>
                                    <td>
                                        <div class="promo-pack_red">5000 руб.</div>
                                    </td>
                                    <td>
                                        <div class="promo-pack_red">% от сбора средств*</div>
                                    </td>
                                </tr>

                                </tbody>
                            </table>


                            <div class="promo-pack_text">
                                <p class="promo-pack_text-small">* Стоимость пакета услуг «Персональный менеджер» определяется в зависимости от фактической суммы собранных средств и взимается дополнительно к стандартной комиссии сервиса «Акционирование» (согласно таблице ниже). Перед началом работ подписывается дополнительное соглашение и вносится предоплата. Размер предоплаты определяется в зависимости от финансовой цели проекта. Предоплата не возвращается в случае отказа от услуг или в случае неуспешного сбора средств.</p>
                            </div>


                            <table class="promo-pack_table table table-bordered table-fixed">
                                <colgroup>
                                    <col width="50%">
                                    <col width="50%">
                                </colgroup>
                                <thead>
                                <tr>
                                    <th>Финансовая цель / сбор по окончанию проекта</th>
                                    <th>Стоимость пакета услуг «Персональный менеджер» в процентах от сбора</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>
                                        До 150 тыс. руб.
                                    </td>
                                    <td>
                                        10%, предоплата 5000 руб.
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        От 150 тыс. до 300 тыс. руб.
                                    </td>
                                    <td>
                                        10%, предоплата 10000 руб.
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        От 300 тыс. до 500 тыс. руб.
                                    </td>
                                    <td>
                                        10%, предоплата 15000 руб.
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        От 500 тыс. до 1 млн. руб.
                                    </td>
                                    <td>
                                        7%, предоплата 15000 руб.
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        От 1 до 5 млн. руб.
                                    </td>
                                    <td>
                                        5%, предоплата 20000 руб.
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        От 5 млн. руб.
                                    </td>
                                    <td>
                                        3%, предоплата 20000 руб.
                                    </td>
                                </tr>
                                </tbody>
                            </table>


                            <div class="promo-pack_text">
                                <p class="promo-pack_text-small">Заказ и выполнение отдельных работ по стоимости, указанной в пакете «Стартовый», может осуществляться на основании выставляемых счетов.</p>
                                <p class="promo-pack_text-small">Мы не даем гарантию 100% успешного сбора, так как каждый проект уникален, и мы не всегда можем заранее знать, насколько та или иная идея будет интересна широкой аудитории. Мы используем накопленные нами знания и опыт и сделаем все необходимое для того, чтобы проект был успешным. Тем не менее, помните, что краудфандинг – это возможность с минимальными затратами протестировать вашу идею, чтобы оценить, стоит ли в дальнейшем вкладывать в ее развитие время, силы и средства.</p>
                            </div>


                        </div>

                    </div>
                </div>
            </div>


        </div>

    </div>
</div>
<%@ include file="/WEB-INF/jsp/includes/generated/irma/footer.jsp" %>
</body>
</html>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@include file="/WEB-INF/jsp/includes/generated/doctype-header.jsp" %>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Сертификат о прохождении онлайн-курса &laquo;Школы краудфандинга Planeta.ru&raquo;</title>
    <meta name="title" content="Сертификат о прохождении онлайн-курса &laquo;Школы краудфандинга Planeta.ru&raquo;">
    <meta name="description" content="Сертификат о прохождении онлайн-курса &laquo;Школы краудфандинга Planeta.ru&raquo;">
    <meta property="og:title" content="Сертификат о прохождении онлайн-курса &laquo;Школы краудфандинга Planeta.ru&raquo;">
    <meta property="og:description" content="Сертификат о прохождении онлайн-курса &laquo;Школы краудфандинга Planeta.ru&raquo;">
    <meta property="og:image" content="https://${widgetsHost}/image?type=SCHOOL&profileId=${certProfileId}">

    <%@include file="/WEB-INF/jsp/includes/common-css.jsp" %>
    <link type="text/css" rel="stylesheet" href="//${hf:getStaticBaseUrl("")}/css-generated/school.css"/>
    <%@include file="/WEB-INF/jsp/includes/planeta-js.jsp" %>
    <%@include file="/WEB-INF/jsp/includes/generated/common-templates.jsp" %>
</head>
<body>
<%@ include file="/WEB-INF/jsp/includes/generated/screen-width.jsp" %>
<%@ include file="/WEB-INF/jsp/includes/generated/irma/header.jsp"%>
<%@ include file="/WEB-INF/jsp/includes/generated/irma/main-nav.jsp" %>

<div id="global-container">
    <div id="main-container">
        <div id="center-container">


            <div class="school-cert">
                <div class="school-cert_bg">
                    <svg width="1440" height="711" viewBox="0 0 1440 711" preserveAspectRatio="none"><defs><linearGradient id="a" gradientUnits="userSpaceOnUse" x1="1260.062" y1="300.388" x2="1260.062" y2="299.406" gradientTransform="matrix(1440 0 0 -711 -1813770 213576)"><stop offset="0" stop-color="#254959"/><stop offset="1" stop-color="#2C2F33"/></linearGradient></defs><polygon id="Rectangle-3" fill="url(#a)" points="0,0 1440,0 1440,508.9 0,711" vector-effect="non-scaling-stroke"/></svg>
                </div>
                <div class="wrap">
                    <div class="col-12">

                        <div class="school-cert_logo">
                            <span class="school-logo">
                                <svg width="222" height="51" viewBox="0 0 632 145"><path d="M132.3 32.3c3.4-5.6 2.7-12.9-2.1-17.7-4.8-4.8-12.1-5.5-17.7-2.1C84.4-6.4 46-3.4 21.2 21.4-7 49.6-7 95.4 21.2 123.6c28.2 28.2 74 28.2 102.2 0 24.8-24.8 27.7-63.2 8.9-91.3m-15.8 84.5c-18 18-44.1 22.7-66.4 14.3-5-2.1-20.2-10.8.9-31.8l-2.8-2.8-2.8-2.8c-20.3 20.3-29.1 6.8-31.6 1.4C5.2 72.7 9.9 46.3 28 28.2c21.3-21.3 54.1-24 78.4-8.3-1.8 5.1-.7 11 3.4 15.1s10 5.2 15.1 3.4c15.7 24.3 12.9 57.1-8.4 78.4M70.7 49.3c-4.8 3.1-8.3 8.1-11.5 13-7.8.5-15.6 3.9-20.2 10.3-.6.9-1.5 2.1-2.1 4.2 1.3.2 3.1.5 4.2.7 2.8.5 6.1.8 9.2 2.7-.6 2.1-1.2 5-1.5 6.9v.5c1.6.8 3.6 1.7 5.2 3.3 1.6 1.6 2.5 3.7 3.3 5.2h.5c1.9-.3 4.8-1 6.9-1.5 1.9 3.1 2.2 6.4 2.7 9.2.2 1.1.5 2.9.7 4.2 2.1-.6 3.3-1.5 4.2-2.1 6.4-4.6 9.8-12.4 10.3-20.2 4.9-3.2 9.8-6.7 13-11.5 5.9-9.1 6.4-20 6.4-31.2-11.4-.1-22.2.3-31.3 6.3m11.8 20.6c-4.2 0-7.5-3.4-7.5-7.5s3.4-7.5 7.5-7.5 7.5 3.4 7.5 7.5-3.4 7.5-7.5 7.5" fill="#1A8CFF"/><path d="M175 23.5h8v29.8h8V23.5h8v29.8h8V23.5h8v37.2h-40V31m77.3 29.8l-14-18.6 14-18.6H243l-12 16v-16h-8v37.2h8V44.9l12 15.9h9.3zm34.8-18.6c0-13.6-7.2-19.9-17-19.9s-17 6.3-17 19.9c0 13.6 7.2 19.9 17 19.9s17-6.3 17-19.9m-26 0c0-9.5 3.7-12.4 9-12.4 5.4 0 9 3 9 12.4 0 9.5-3.7 12.4-9 12.4-5.4 0-9-3-9-12.4m60.4 18.6V23.5h-22.9L295.9 49c-.4 3.8-1 5-2.8 5-.5 0-2.1-.1-3.3-.3l-1.1 6.9c2.2.6 4.7.9 6.1.9 5.3 0 8.1-4 9-12.4l1.9-18.1h7.8v29.8h8zm40.9 0l-15.8-37.2h-5.3l-15.8 37.2h8.5l2.7-6.4h14.6l2.7 6.4h8.4zM348.2 47h-8.4l2-4.8 2.2-7.9 2.2 7.9 2 4.8zm-143.8 69l-14-18.6 14-18.6h-9.3l-12 16v-16h-8V116h8v-16l12 15.9h9.3zm12.9 0v-12.8h7.2c6.8 0 12.2-3.7 12.2-12.2s-5.6-12.3-12.2-12.3h-15.2V116h8zm0-20.3v-9.6h7.2c2.3 0 4.3.6 4.3 4.8s-2.1 4.7-4.3 4.7h-7.2zm57.5 20.3L259 78.7h-5.3L237.9 116h8.5l2.7-6.4h14.6l2.7 6.4h8.4zm-14.2-13.9h-8.4l2-4.8 2.2-7.9 2.2 7.9 2 4.8zm29.1 2.9c-1.4 3.2-2 4.3-4.5 4.3-1.3 0-3.8-.3-5.7-.9l-1.1 6.9c2.6 1 4.5 1.4 6.9 1.4 7.6 0 9.7-4.4 13.6-13.5l10.7-24.5H301l-7.6 17.7-8.9-17.7h-8.9l14.1 26.3zm54.3 18.9v-15.4h-4V78.7h-24l-2.4 23.2c-.5 3.8-1.5 6.6-4 6.6h-1.2v15.4h6.9v-8h21.8v8h6.9zm-12-37.7v22.3h-12.6c1.1-2 1.9-4.8 2.3-8.5l1.4-13.8h8.9zm37.5 29.8v-6.4h1.9c7.2 0 13.3-4.1 13.3-13.8 0-9.6-6.2-13.9-13.3-13.9h-1.9v-3.2h-8v3.2h-1.9c-7.1 0-13.3 4.3-13.3 13.9 0 9.6 5.9 13.8 13.3 13.8h1.9v6.4h8zm-8-13.9h-1.9c-2.7 0-5.3-.7-5.3-6.3s2.5-6.4 5.3-6.4h1.9v12.7zm8 0V89.4h1.9c2.8 0 5.3.8 5.3 6.4 0 5.6-2.6 6.3-5.3 6.3h-1.9zm52 13.9l-15.8-37.2h-5.3L384.6 116h8.5l2.7-6.4h14.6l2.7 6.4h8.4zm-14.2-13.9h-8.4l2-4.8 2.2-7.9 2.2 7.9 2 4.8zm47.5 13.9V78.7h-8v14.9h-13.4V78.7h-8V116h8v-14.9h13.4V116h8zm39.9 7.9v-15.4h-4V78.7h-24l-2.4 23.2c-.5 3.8-1.5 6.6-4 6.6h-1.2v15.4h6.9v-8h21.8v8h6.9zm-12-37.7v22.3h-12.6c1.1-2 1.9-4.8 2.3-8.5l1.4-13.8h8.9zm45.5 29.8V78.7h-7.4l-.5 3.6-13.4 17.7V78.7h-8V116h7.4l.5-3.5 13.4-17.8V116h8zm37.3 0V78.7h-8v14.9h-13.4V78.7h-8V116h8v-14.9h13.4V116h8zm16 0V86.2h14.6v-7.4h-22.6V116h8zm50.3 0L616 78.7h-5.3L594.9 116h8.5l2.7-6.4h14.6l2.7 6.4h8.4zm-14.3-13.9h-8.4l2-4.8 2.2-7.9 2.2 7.9 2 4.8z" fill="#FFFFFF"/></svg>
                            </span>
                        </div>

                        <div class="school-cert_head">
                            <span class="text-hl">${displayName}</span> уже молодец! И&nbsp;вы тоже можете!
                            <br>
                            <br>
                            Чувствуете, что можете воплотить в жизнь свою идею? Тогда не&nbsp;ждите&nbsp;— жмите кнопку и&nbsp;начинайте учиться: смотрите видео курса, читайте доп.материалы и запускайте собственную крауд-кампанию!
                        </div>

                        <div class="school-cert_cont">
                            <div class="intr-cert">
                                <div class="intr-cert_img">
                                    <img src="//${hf:getStaticBaseUrl("")}/images/interactive/press.jpg">
                                </div>
                                <div class="intr-cert_name">
                                    ${displayName}
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>


            <div class="school-interactive school-interactive__cert">
                <div class="school-interactive_cont">
                    <div class="wrap">
                        <div class="col-12">

                            <div class="school-interactive_cont-in">
                                <div class="school-interactive_title">
                                    <div class="school-interactive_title-text">
                                        Онлайн-курс<br><span class="laquo">«</span>Краудфандинговый<br>проект за 60 минут»                                    </div>
                                </div>


                                <div class="school-interactive_post-title">
                                    на <span class="school-interactive_pln-logo"></span> с Егором Ельчиным
                                </div>


                                <div class="school-interactive_text">
                                    Некогда читать книги и&nbsp;смотреть вебинары о&nbsp;краудфандинге? Пора&nbsp;ускоряться! Вас ждут 12 интерактивных уроков &laquo;2 в&nbsp;1&raquo;&nbsp;&mdash; Егор&nbsp;Ельчин не&nbsp;только рассказывает о&nbsp;народном финансировании, но&nbsp;и&nbsp;одновременно помогает вам создавать проект на&nbsp;Planeta.ru.
                                    <br>
                                    <br>
                                    Всё это абсолютно бесплатно, экстремально быстро и&nbsp;практично: начинайте учиться прямо сейчас, и&nbsp;через час у&nbsp;вас будет краудфандинговая кампания, готовая к&nbsp;запуску!
                                </div>


                                <div class="school-interactive_next">
                                    <div class="school-interactive_next-btn">
                                        <a href="${mainAppUrl}/interactive" class="btn school-interactive-btn">Пройти курс</a>
                                    </div>
                                    <div class="school-interactive_next-link">
                                        <a href="${mainAppUrl}/faq/article/30!paragraph205">Подробнее о курсе</a>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>


        </div>
    </div>
</div>

<%@include file="/WEB-INF/jsp/includes/generated/irma/footer.jsp"%>
</body>

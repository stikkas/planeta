<script>
    var landingSlider = $('.author-slider_text');
    var landingSliderItems = $('.author-slider_text-i');
    landingSlider.append(landingSliderItems.clone())
            .prepend(landingSliderItems.clone());

    landingSlider.carouFredSel({
        responsive: true,
        width: '100%',
        items: {
            visible: 1
        },
        auto: {
            play: false,
            timeoutDuration: 5000
        },
        scroll: {
            items: 1,
            duration: 600,
            pauseOnHover: 'resume',
            fx: 'crossfade',
            onBefore: function(data) {
                landingSlider.triggerHandler('currentVisible').removeClass('active');
                var current = landingSlider.triggerHandler('currentPosition');
                if ( headSlider ) headSlider.trigger('slideTo', current);
            },
            onAfter: function(data) {
                landingSlider.triggerHandler('currentVisible').addClass('active');
            }
        },
        onCreate: function() {
            landingSlider.triggerHandler('currentVisible').addClass('active');
        }
    });

    $(function() {
        $('.author-slider_head-i:eq(0)').fontSpy({
            delay: 2000,
            done: function() {
                setTimeout(headSliderInit, 200);
            }
        });
    });


    // head slider
    var headSlider = $('.author-slider_head-list');
    var headItems = $('.author-slider_head-i');
    var headItemsLen = headItems.length;
    var headSliderWidth;

    function headSliderInit() {
        headSliderWidth = headSlider.width();
        headSlider
                .append(headItems.clone())
                .prepend(headItems.clone());
        var headItemsFull = $('.author-slider_head-i');

        headSlider.carouFredSel({
            align: false,
            responsive: true,
            width: '100%',
            auto: false,
            items: {
                width: 'variable',
                visible: 10
            },
            scroll: {
                items: 1,
                duration: 600,
                onBefore: function(data) {
                    removeActive(data.items.old);
                    setActive(data.items.visible);
                }
            },
            onCreate: function(data) {
                headItemsFull.each(function(i) {
                    $(this).data('id', i);
                    $(this).attr('data-id', i);
                });

                setActive(data.items.prevObject, true);

                var parent = headSlider.parent();
                parent.css({width: 'auto'});
                $(window).resize(function() {
                    var activeItem = headItemsFull.filter('.active');
                    var offset = activeItem.offset().left + activeItem.width() / 2;

                    parent.css({
                        marginLeft: $(window).width() / 2 - offset,
                        float: 'none'
                    });
                }).trigger('resize');

                landingSlider.trigger('slideTo', 0);

                headSlider.removeClass('uninit');
            }
        });

        function setActive(el, onCreate) {
            var elem = {
                elActive: $(el[headItemsLen]),
                sibling: $([el[headItemsLen - 1], el[headItemsLen + 1]]),
                sibling2: $([el[headItemsLen - 2], el[headItemsLen + 2]])
            };

            elem.sibling.addClass('sibling');
            elem.sibling2.addClass('sibling-2');

            if ( !$.fn.styleSupport('transform') || !$.fn.styleSupport('transition') ) return;

            elem.elActive.addClass('active');

            /*var elWidth = elem.elActive.width();
             elem.elActive.data('width', elWidth);
             elem.elActive.css({width: elWidth});
             if ( !onCreate ) elem.elActive[0].offsetWidth;
             elem.elActive.css({width: elWidth * 1.3});*/
        }

        function removeActive(el) {
            var elem = {
                elActive: $(el[headItemsLen]),
                sibling: $([el[headItemsLen - 1], el[headItemsLen + 1]]),
                sibling2: $([el[headItemsLen - 2], el[headItemsLen + 2]])
            };
            elem.sibling.removeClass('sibling');
            elem.sibling2.removeClass('sibling-2');
            if ( !$.fn.styleSupport('transform') || !$.fn.styleSupport('transition') ) return;
            elem.elActive.removeClass('active');
            /*elem.elActive.css({width: elem.elActive.data('width')});*/
        }

        headItemsFull.click(function() {
            if ( $(this).hasClass('active') ) return;
            var to = $(this).data('id') - headItemsLen;
            landingSlider.trigger('slideTo', to);
        });

    }

</script>

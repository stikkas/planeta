<%@ include file="/WEB-INF/jsp/includes/generated/doctype-header.jsp" %>
<%@taglib prefix="p" uri="http://planeta.ru/tags" %>
<c:set var="baseUrl" value="${hf:getStaticBaseUrl('')}"/>
<head>
    <title>Благотворительный спецпроект "CLEVER-ДЕТЯМ" на Planeta.ru</title>
    <meta name="description" content='Издательство Clever и Planeta.ru представляют совместный благотворительный проект "CLEVER - ДЕТЯМ!" Оставив пожертвование в благотворительном проекте, Вы получаете одну из чудесных детских книг издательства Clever.'>
    <meta property="og:title" content='Благотворительный спецпроект "CLEVER-ДЕТЯМ" на Planeta.ru'/>
    <meta property="og:description" content='Издательство Clever и Planeta.ru представляют совместный благотворительный проект "CLEVER - ДЕТЯМ!" Оставив пожертвование в благотворительном проекте, Вы получаете одну из чудесных детских книг издательства Clever.'>
    <meta property="og:image" content="https://${baseUrl}/images/clever/clever-books.png" />

    <%@include file="/WEB-INF/jsp/includes/generated/common-favicon.jsp" %>
    <link type="text/css" rel="stylesheet" href="//${baseUrl}/css-generated/clever.css" />
    <%@include file="/WEB-INF/jsp/includes/generated/stat-counters-head.jsp" %>
    <%@include file="/WEB-INF/jsp/includes/planeta-js.jsp" %>
    <ct:loadModule moduleName="shop-utils"/>
    <%@include file="/WEB-INF/jsp/includes/generated/common-templates.jsp" %>
</head>
<body>

<div class="header">
    <div class="wrap">
        <div class="col-12">

            <a href ="http://clever-media.ru/" target="_blank" rel="nofollow noopener">
                <div class="logo"></div>
            </a>

            <div class="pln-logo_wrap"><a class="pln-logo" href="https://${properties['application.host']}/"></a></div>

            <div class="header_name"></div>

        </div>
    </div>
</div>



<div class="wrap">
    <div class="col-12">

        <div class="clever-books wrap-row">
            <div class="clever-books_cont col-6">
                <div class="clever-books_head">
                    Книги издательства Clever в&nbsp;благотворительных проектах Planeta.ru
                </div>
                <div class="clever-books_text">
                    Издательство Clever и&nbsp;краудфандинговая&nbsp;платформа Planeta.ru представляют совместный благотворительный проект «CLEVER&nbsp;— ДЕТЯМ!» Оставив пожертвование в&nbsp;благотворительном проекте, вы&nbsp;получаете одну из&nbsp;чудесных детских книг издательства Clever.
                </div>
            </div>
            <div class="clever-books_img">
                <img src="//${baseUrl}/images/clever/clever-books.png">
            </div>
        </div>

    </div>
</div>


<c:if test="${not empty cleverCampaignIds}">
<div class="clever-projects">
    <div class="clever-projects_girl"></div>
    <div class="wrap">
        <div class="clever-projects_cont col-12">
            <div class="clever-projects_boy"></div>

            <div class="clever-projects_head main-head">
                Благотворительные проекты «CLEVER — &nbsp;ДЕТЯМ»
            </div>

            <div class="project-list-block">
                <div class="project-card-list js-clever-projects">
                </div>
            </div>
        </div>
    </div>
</div>
</c:if>

<div class="clever-director">
    <div class="wrap">
        <div class="col-12">

            <div class="clever-director_cont">
                <div class="clever-director_head">
                    Александр Альперович, генеральный директор издательства Clever:
                </div>
                <div class="clever-director_text">
                    Детскому издательству Clever уже пять лет, и&nbsp;все пять лет мы&nbsp;издаем книги, которые делают наших детей счастливыми. Для этого мы&nbsp;постоянно ищем новых замечательных авторов и&nbsp;переиздаем проверенные поколениями произведения, сотрудничаем с&nbsp;журналами и&nbsp;образовательными проектами, чтобы расти и&nbsp;развиваться вместе с&nbsp;нашими читателями.
                    <br>
                    <br>
                    Мы&nbsp;верим, что добрые, умные, пронзительные книги могут творить чудеса: на&nbsp;наших книгах уже выросло целое поколение замечательных маленьких людей, которые научились доброте и&nbsp;сопереживанию.
                    <br>
                    <br>
                    Также мы&nbsp;верим, что каждый человек должен всегда помнить о&nbsp;тех, кто очень нуждается в&nbsp;помощи, и&nbsp;всегда протягивать им&nbsp;руку в&nbsp;трудные минуты. Разве можно учить наших детей доброте, а&nbsp;самим забывать о&nbsp;том, что такое помощь и&nbsp;сочувствие?
                    <br>
                    <br>
                    Поэтому мы&nbsp;с&nbsp;удовольствием решили поддержать благотворительные детские проекты на&nbsp;Planeta.ru, ведь мы&nbsp;хотим, чтобы вокруг нас было больше <nobr>по-настоящему</nobr> счастливых детей, которые смогут почувствовать, что они нужны не&nbsp;только маме с&nbsp;папой, что в&nbsp;мире много хороших людей, которые всегда готовы прийти на&nbsp;помощь.
                </div>
            </div>

        </div>
    </div>
</div>



<div class="clever-month">
    <div class="wrap">
        <div class="col-12">

            <div class="clever-month_info">
                Раз в месяц мы выбираем книгу и проект, о которых рассказываем на специальном детском празднике. В наших мероприятиях принимают участие известные актеры, режиссеры и музыканты. Приходите! Будет интересно!
            </div>

            <div class="clever-month_list wrap-row">
                <c:if test="${not empty cleverBookOfMonth}">
                <div class="clever-month_i col-6">
                    <div class="clever-month_i-head">Книга месяца</div>
                    ${cleverBookOfMonth}
                </div>
                </c:if>
                <c:if test="${not empty cleverProjectOfMonth}">
                <div class="clever-month_i col-6">
                    <div class="clever-month_i-head">Проект месяца</div>
                    ${cleverProjectOfMonth}
                </div>
                </c:if>
            </div>

        </div>
    </div>
</div>

<c:if test="${not empty cleverEventOfMonth}">
    ${cleverEventOfMonth}
</c:if>

<c:if test="${not empty products}">
<div class="clever-products">
    <div class="wrap">
        <div class="col-12">

            <div class="clever-products_head main-head">
                Товары Clever в благотворительном разделе магазина Planeta.ru
            </div>


            <div class="clever-products_list wrap-row js-product-list product-card">
            </div>

        </div>
    </div>
</div>
</c:if>

<div class="clever-share">
    <div class="wrap">
        <div class="col-12">

            <div class="clever-share_head main-head">Поделитесь страницей!</div>

            <div class="clever-share_cont">
            </div>

        </div>
    </div>
</div>


<script type="application/javascript">
    $(function () {
        CrowdFund.Models.CleverCampaignsList = BaseCollection.extend({
            url: "/api/campaign/campaigns_dto.json"
        });

        var campaignsList = new CrowdFund.Models.CleverCampaignsList([], {
            data: {
                ids: ${cleverCampaignIds}
            }
        });
        var CampaignsList = BaseListView.extend({
            className: "project-card-list",
            itemViewType: CrowdFund.Views.CampaignsItem
        });
        var view = new CampaignsList({
            el: '.js-clever-projects',
            collection: campaignsList
        });
        campaignsList.load().done(function() {
            campaignsList.each(function(model) {console.log(model); model.set({showButton: true}, {silent: true});});
            view.render();
        });

        var shareData = {
            counterEnabled: false,
            hidden: false,
            parseMetaTags: true,
            className: 'donate-sharing'
        };
        $('.clever-share_cont').share(shareData);


        ShopUtils.initializeShopUtils();
        var cleverProducts = new Products.Models.ProductCollection(${hf:toJson(products)});
        cleverProducts.allLoaded = true;

        var cleverProductsView = new Products.Views.ProductListView({
            el: '.js-product-list',
            collection: cleverProducts,
            gtmProductListTitle: 'Clever'
        });

        cleverProductsView.render();

    });
</script>
</body>
</html>

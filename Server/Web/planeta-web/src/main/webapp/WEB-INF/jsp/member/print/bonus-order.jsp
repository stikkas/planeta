<%--
  Created by IntelliJ IDEA.
  User: alexa_000
  Date: 29.09.2014
  Time: 19:56
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/jsp/includes/generated/doctype-header.jsp" %>
<head>
    <title>Planeta: Информация о заказе</title>
    <meta name="title" content="Planeta: Информация о заказе">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta charset="utf-8">

    <link type="text/css" rel="stylesheet" href="//${hf:getStaticBaseUrl("")}/css-generated/common.css"/>

    <script type="text/javascript">
        function printDocument() {
            setTimeout(function() {window.print();}, 1000);
        }
    </script>
    <link href="//${hf:getStaticBaseUrl("")}/css/bootstrap.min.css" rel="stylesheet">
    <link href="//${hf:getStaticBaseUrl("")}/css/print.css" rel="stylesheet">
</head>
<body class="coupon-page" onload="printDocument()">
<!-- Coupon-print -->
<div class="coupon-print">
    <div class="coupon-print-item clearfix">
        <div class="logo">
            <img src="//${hf:getStaticBaseUrl("")}/images/print-logotip.png" alt="print-logotip.png">
        </div>
    </div>
    <div class="coupon-print-item clearfix">
        <div class="info">
            <h1 style="text-align: left;">${objectName}</h1>
            <div class="info-item clearfix">
                    <span>
                        Дата продажи:
                        <b><fmt:formatDate pattern="dd.MM.yyyy" value="${order.timeAdded}"/></b>
                    </span>
                    <span>
                        Получатель:
                        <b><c:out value="${profile.displayName}"/></b>
                    </span>
                    <span class="number">
                        Номер заказа:
                        <b>${order.orderId}</b>
                    </span>
            </div>
        </div>
    </div>
    <c:if test="${isCancelled}">
        <div class="coupon-print-item clearfix">
            <div class="description">
                <div class="descr">
                    <h5>Бонус аннулирован.</h5>
                </div>
            </div>
        </div>
    </c:if>

    <div class="coupon-print-item clearfix">
        <div class="description">
            <div class="descr">
                Получатель: ${deliveryAddress.fio}
                <c:if test="${order.deliveryType == \"DELIVERY\" || order.deliveryType == \"RUSSIAN_POST\"}">
                    <br />
                    Куда: <c:if test="${not empty deliveryAddress.zipCode}"><c:out value="${deliveryAddress.zipCode}"/>, </c:if>
                    <c:out value="${deliveryAddress.city}"/>, <c:out value="${deliveryAddress.address}"/>, <c:out value="${deliveryAddress.phone}"/>
                </c:if>

                <c:if test="${order.deliveryType == \"CUSTOMER_PICKUP\"}">
                    <br />
                    Адрес самовывоза: ${deliveryAddress.address}
                </c:if>
            </div>
        </div>
    </div>
</div></body></html>
<%@ include file="/WEB-INF/jsp/includes/generated/doctype-header.jsp" %>
<head>
    <c:set var="pageType" value="business"/>
    <c:set var="eventLabel" value="quickbook_get_recommendation"/>
    <%@ include file="/WEB-INF/jsp/static/quickstart-new/includes/quickstart-header.jsp" %>
</head>

<body>
    <div class="header">
        <a class="logo" href="https://planeta.ru/"></a>

        <a class="btn btn-primary header_create-btn" href="/funding-rules">Воплотить идею</a>
    </div>


    <div class="video-lead var4">
        <div class="wrap">
            <div class="video-lead_head">Хотите создать бизнес?</div>
            <div class="video-lead_block cf">
                <div class="video-lead_cont">
                    <div class="video-lead_text">
                        У вас есть идея для стартапа или открываете новое направления?
                         Поиск инвестора — дело сложное и накладное. С помощью «Планеты»
                         найдите будущих потребителей, и они финансово поддержат Вас!
                        </div>

                    <div class="video-lead_action-text">
                        Запишитесь на вебинар о краудфандинге
                        <br>
                        и получите бесплатное руководство
                        <br>
                        «Как создать бизнес с помощью краудфандинга»
                    </div>

                    <div class="video-lead_action">
                        <div class="fieldset pln-dropdown js-open-error">
                            <input class="form-control js-get-book-email" type="text" placeholder="Введите ваш e-mail">
                            <div class="page-control_popup pln-d-popup">
                                <div class="pln-d-popup-tail"><div></div></div>
                                <div class="d-popup-cont">
                                    Вы ввели неверный адрес электронной почты
                                </div>
                            </div>
                        </div>

                        <div class="video-lead_action_btn pln-dropdown js-get-book-btn-cnt" >
                            <button class="btn btn-primary js-get-book-btn" onclick="getBook()">Получить!</button>
                            <div class="page-control_popup pln-d-popup">
                                <div class="pln-d-popup-tail"><div></div></div>
                                <nobr class="d-popup-cont">
                                    Вам отправлено письмо с руководством
                                    <br>
                                    «Как создать бизнес с помощью краудфандинга»
                                </nobr>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="promo-lead">
        <div class="wrap">
            <div class="promo-lead_head">Кому это может быть полезно</div>
            <div class="promo-lead_block cf">

                <div class="promo-lead_list">
                    <div class="promo-lead_i">
                        <div class="promo-lead_i_img">
                            <img src="//${hf:getStaticBaseUrl("")}/images/quickstart/promo-lead-img-5-1.png">
                        </div>
                        <div class="promo-lead_i_head">
                            Стартапам
                        </div>
                        <div class="promo-lead_i_text">
                            Если ваша идея достаточно интересна, с&nbsp;помощью
                            пользователей &laquo;Планеты&raquo; вы&nbsp;сможете найти средства на&nbsp;открытие
                            собственного бизнеса. Именно люди, поддержавшие ваш проект, и&nbsp;станут вашими первыми
                            потребителями&nbsp;&mdash; таким образом, формируется начальная аудитория будущего стартапа.
                        </div>
                    </div>
                    <div class="promo-lead_i">
                        <div class="promo-lead_i_img">
                            <img src="//${hf:getStaticBaseUrl("")}/images/quickstart/promo-lead-img-5-2.png">
                        </div>
                        <div class="promo-lead_i_head">
                            Малому и среднему бизнесу
                        </div>
                        <div class="promo-lead_i_text">
                            Вы&nbsp;найдете здесь аудиторию из&nbsp;разных регионов России и&nbsp;других стран,
                            готовую поддержать рублем уже сложившийся бренд и&nbsp;помочь его расширению. Новые филиалы,
                            открытие магазинов и&nbsp;связь с&nbsp;ритейлерами&nbsp;&mdash; вот только часть возможностей,
                            которые дает вам сотрудничество с&nbsp;&laquo;Планетой&raquo;.
                        </div>
                    </div>
                    <div class="promo-lead_i">
                        <div class="promo-lead_i_img">
                            <img src="//${hf:getStaticBaseUrl("")}/images/quickstart/promo-lead-img-5-3.png">
                        </div>
                        <div class="promo-lead_i_head">
                            Крупным корпорациям
                        </div>
                        <div class="promo-lead_i_text">
                            &laquo;Планета&raquo;&nbsp;&mdash; это мощный маркетинговый инструмент, а&nbsp;заодно
                            возможность проверить на&nbsp;прочность новую идею, прежде чем вкладывать в&nbsp;нее миллионы.
                            Здесь вы&nbsp;можете изучить спрос на&nbsp;продукт среди финансово активной аудитории,
                            а&nbsp;также повысить узнаваемость бренда, участвуя в&nbsp;спецпроектах &laquo;Планеты&raquo;.
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <%@ include file="/WEB-INF/jsp/static/quickstart-new/includes/quickstart-inner.jsp" %>
    <div class="author-slider">
        <div class="author-slider_head">Отзывы авторов проектов</div>

        <div class="author-slider_head-wrap">
            <div class="author-slider_head-list uninit">
                <div class="author-slider_head-i">
                    <div class="author-slider_head-name">
                        <div class="author-slider_head-name-text">
                            <span class="cur-p">Борис Акимов</span>
                        </div>
                    </div>
                    <div class="author-slider_head-ava"><img src="//${hf:getStaticBaseUrl("")}/images/quickstart/author/akimov.jpg" width="144" height="144"></div>
                </div>

                <div class="author-slider_head-i">
                    <div class="author-slider_head-name">
                        <div class="author-slider_head-name-text">
                            <span class="cur-p">Дарья Сонькина</span>
                        </div>
                    </div>
                    <div class="author-slider_head-ava"><img src="//${hf:getStaticBaseUrl("")}/images/quickstart/author/sonkina.jpg" width="144" height="144"></div>
                </div>

                <div class="author-slider_head-i">
                    <div class="author-slider_head-name">
                        <div class="author-slider_head-name-text">
                            <span class="cur-p">Сергей Никитин</span>
                        </div>
                    </div>
                    <div class="author-slider_head-ava"><img src="//${hf:getStaticBaseUrl("")}/images/quickstart/author/nikitin.jpg" width="144" height="144"></div>
                </div>

                <div class="author-slider_head-i">
                    <div class="author-slider_head-name">
                        <div class="author-slider_head-name-text">
                            <span class="cur-p">Борис Акимов</span>
                        </div>
                    </div>
                    <div class="author-slider_head-ava"><img src="//${hf:getStaticBaseUrl("")}/images/quickstart/author/akimov.jpg" width="144" height="144"></div>
                </div>

                <div class="author-slider_head-i">
                    <div class="author-slider_head-name">
                        <div class="author-slider_head-name-text">
                            <span class="cur-p">Дарья Сонькина</span>
                        </div>
                    </div>
                    <div class="author-slider_head-ava"><img src="//${hf:getStaticBaseUrl("")}/images/quickstart/author/sonkina.jpg" width="144" height="144"></div>
                </div>

                <div class="author-slider_head-i">
                    <div class="author-slider_head-name">
                        <div class="author-slider_head-name-text">
                            <span class="cur-p">Сергей Никитин</span>
                        </div>
                    </div>
                    <div class="author-slider_head-ava"><img src="//${hf:getStaticBaseUrl("")}/images/quickstart/author/nikitin.jpg" width="144" height="144"></div>
                </div>

            </div>
        </div>


        <div class="author-slider_text-wrap">
            <div class="author-slider_text">
                <div class="author-slider_text-i">
                    <div class="author-slider_text-project">
                        Борис Акимов, создатель фермерского кооператива LavkaLavka
                    </div>
                    <div class="author-slider_text-text">
                        То, что у нас получилось собрать денег на «Планете», — это
                        настоящее революционное событие! Для нас это очень важно
                        — это символ того, что нас поддерживают и нас понимают.
                        А для нас это самое главное!
                    </div>
                </div>

                <div class="author-slider_text-i">
                    <div class="author-slider_text-project">
                        Дарья Сонькина, создатель проекта «Дашины пирожки»
                    </div>
                    <div class="author-slider_text-text">
                        Честно говоря, запуская краудфандинг на Планете,
                        я думала больше о пиаре проекта, нежели о деньгах.
                        Я очень сомневалась, что незнакомые люди ни с того
                        ни с сего будут жертвовать деньги на неизвестный проект.
                        Так что успешный сбор оказался очень приятным сюрпризом.
                    </div>
                </div>

                <div class="author-slider_text-i">
                    <div class="author-slider_text-project">
                        Сергей Никитин, основатель проекта «Велоночь»
                    </div>
                    <div class="author-slider_text-text">
                        Друзья! Вместе с Вами мы провели Южную Московскую Велоночь,
                        эту монументальный проект, в котором приняло участие около
                        десяти тысяч человек — не считая помогавшей нам полиции.
                        Планете только пара лет, а уже столько счастливых свершений
                        случилось благодаря Вам и Вашему заботливому отношению к делу.
                        Спасибо и успехов!
                    </div>
                </div>

                <div class="author-slider_text-i">
                    <div class="author-slider_text-project">
                        Борис Акимов, создатель фермерского кооператива LavkaLavka
                    </div>
                    <div class="author-slider_text-text">
                        То, что у нас получилось собрать денег на «Планете», — это
                        настоящее революционное событие! Для нас это очень важно
                        — это символ того, что нас поддерживают и нас понимают.
                        А для нас это самое главное!
                    </div>
                </div>

                <div class="author-slider_text-i">
                    <div class="author-slider_text-project">
                        Дарья Сонькина, создатель проекта «Дашины пирожки»
                    </div>
                    <div class="author-slider_text-text">
                        Честно говоря, запуская краудфандинг на Планете,
                        я думала больше о пиаре проекта, нежели о деньгах.
                        Я очень сомневалась, что незнакомые люди ни с того
                        ни с сего будут жертвовать деньги на неизвестный проект.
                        Так что успешный сбор оказался очень приятным сюрпризом.
                    </div>
                </div>

                <div class="author-slider_text-i">
                    <div class="author-slider_text-project">
                        Сергей Никитин, основатель проекта «Велоночь»
                    </div>
                    <div class="author-slider_text-text">
                        Друзья! Вместе с Вами мы провели Южную Московскую Велоночь,
                        эту монументальный проект, в котором приняло участие около
                        десяти тысяч человек — не считая помогавшей нам полиции.
                        Планете только пара лет, а уже столько счастливых свершений
                        случилось благодаря Вам и Вашему заботливому отношению к делу.
                        Спасибо и успехов!
                    </div>
                </div>

            </div>
        </div>
    </div>
    <%@ include file="/WEB-INF/jsp/static/quickstart-new/includes/quickstart-request.jsp" %>
</body>
</html>
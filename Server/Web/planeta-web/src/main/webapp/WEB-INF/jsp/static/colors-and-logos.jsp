<%@ include file="/WEB-INF/jsp/includes/generated/doctype-header.jsp" %>
<head>
    <c:choose>
        <c:when test="${not empty customMetaTag.title}">
            <title>${customMetaTag.title}</title>
        </c:when>
        <c:otherwise>
            <title>Цвета и логотипы | Planeta</title>
        </c:otherwise>
    </c:choose>
    <c:if test="${not empty customMetaTag.description}">
        <meta name="description" content="${customMetaTag.description}"/>
    </c:if>
    <c:if test="${not empty customMetaTag.keywords}">
        <meta name="keywords" content="${customMetaTag.keywords}"/>
    </c:if>
    <%@include file="/WEB-INF/jsp/includes/generated/stat-counters-async.jsp"%>
    <%@include file="/WEB-INF/jsp/includes/common-css.jsp" %>
    <%@include file="/WEB-INF/jsp/includes/generated/common-templates.jsp" %>
    <%@include file="/WEB-INF/jsp/includes/planeta-js.jsp" %>
</head>

<body>
<%@ include file="/WEB-INF/jsp/includes/generated/header.jsp" %>
<div id="global-container">
    <div id="main-container" class="wrap-container">
        <div class="pln-content-box">
            <div class="wrap">
                <div class="col-9">
                    <div class="pln-content-box_cont wrap-indent-right-half">
                        <div class="aboute-content">
                            <h1 id="logo">Цвета и логотипы</h1>

                            <img src="//${hf:getStaticBaseUrl("")}/images/planeta/pantone-306.png">

                            <br>
                            <br>

                            <p>Использование правильного цвета логотипа очень важно при создании рекламных материалов. Пожалуйста, проверьте цвет логотипа в баннере или в макетах печатной продукции.</p>

                            <table class="table table-fixed aboute-pantone-table">
                                <colgroup>
                                    <col width="230">
                                    <col width="33%">
                                    <col width="33%">
                                </colgroup>
                                <tbody>
                                <tr>
                                    <td>
                                        <img class="show" src="//${hf:getStaticBaseUrl("")}/images/planeta/pantone-logo-white.png">
                                    </td>
                                    <td>
                                        <a class="link-icon" href="https://files.planeta.ru/media-kit/planeta-01.pdf">
                                            <i class="s-file-icon-pdf"></i>
                                            <span class="text-icon">Скачать в PDF</span>
                                        </a>
                                    </td>
                                    <td>
                                        <a class="link-icon" href="https://files.planeta.ru/media-kit/pantone-logo-white.png">
                                            <i class="s-file-icon-png"></i>
                                            <span class="text-icon">Скачать в PNG</span>
                                        </a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <img class="show" src="//${hf:getStaticBaseUrl("")}/images/planeta/pantone-logo-black.png">
                                    </td>
                                    <td>
                                        <a class="link-icon" href="https://files.planeta.ru/media-kit/planeta-02.pdf">
                                            <i class="s-file-icon-pdf"></i>
                                            <span class="text-icon">Скачать в PDF</span>
                                        </a>
                                    </td>
                                    <td>
                                        <a class="link-icon" href="https://files.planeta.ru/media-kit/pantone-logo-black.png">
                                            <i class="s-file-icon-png"></i>
                                            <span class="text-icon">Скачать в PNG</span>
                                        </a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <img class="show" src="//${hf:getStaticBaseUrl("")}/images/planeta/pantone-logo-blue.png">
                                    </td>
                                    <td>
                                        <a class="link-icon" href="https://files.planeta.ru/media-kit/planeta-03.pdf">
                                            <i class="s-file-icon-pdf"></i>
                                            <span class="text-icon">Скачать в PDF</span>
                                        </a>
                                    </td>
                                    <td>
                                        <a class="link-icon" href="https://files.planeta.ru/media-kit/pantone-logo-blue.png">
                                            <i class="s-file-icon-png"></i>
                                            <span class="text-icon">Скачать в PNG</span>
                                        </a>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="col-3">
                    <%@include file="/WEB-INF/jsp/includes/about-menu.jsp" %>
                </div>

            </div>
        </div>

    </div>
</div>
<%@ include file="/WEB-INF/jsp/includes/generated/footer.jsp" %>
</body>
</html>
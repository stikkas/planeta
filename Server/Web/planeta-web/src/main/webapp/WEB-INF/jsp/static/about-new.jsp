<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ include file="/WEB-INF/jsp/includes/generated/doctype-header.jsp" %>
<%@ taglib prefix="p" uri="http://planeta.ru/tags" %>

<head>
    <c:choose>
        <c:when test="${not empty customMetaTag.title}">
            <title>${customMetaTag.title}</title>
        </c:when>
        <c:otherwise>
            <title><spring:message code="page.about.us.about.53" /></title>
        </c:otherwise>
    </c:choose>
    <c:choose>
        <c:when test="${not empty customMetaTag.description}">
            <meta name="description" content="${customMetaTag.description}"/>
        </c:when>
        <c:otherwise>
            <meta name="description"
                  content="Что такое онлайн-площадка Planeta.ru?"/>
        </c:otherwise>
    </c:choose>
    <c:if test="${not empty customMetaTag.keywords}">
        <meta name="keywords" content="${customMetaTag.keywords}"/>
    </c:if>

    <meta name="viewport" content="width=device-width"/>
    <%@include file="/WEB-INF/jsp/includes/generated/stat-counters-async.jsp"%>

    <%@include file="/WEB-INF/jsp/includes/common-css.jsp" %>
    <link type="text/css" rel="stylesheet" href="//${hf:getStaticBaseUrl("")}/css-generated/about.css"/>
    <%@include file="/WEB-INF/jsp/includes/generated/common-templates.jsp" %>
    <%@include file="/WEB-INF/jsp/includes/planeta-js.jsp" %>
    <p:script src="planeta-about.js"/>
    <script>
        var openQuestions = function () {
            $('.about-faq_i').removeClass('hide');
            $('.js-faq-get-more').addClass('hide');
        }
        $(function () {
            $('.js-faq-get-more').click(function () {
                openQuestions();
            });

            if (window.location.hash && window.location.hash.indexOf('faq-question-') !== -1) {
                var id = window.location.hash.replace(/#faq-question-/, '');
                if (id > 3) {
                    openQuestions();
                }
                $(window.location.hash).find('.about-faq_question').next().slideToggle(300);
            }

            $('[id^="faq-question-"]').click(function () {
                window.location.hash = $(this).attr('id');
            });
        });
    </script>
</head>

<body class="grid-1200 about-page">
<%@ include file="/WEB-INF/jsp/includes/offline.jsp" %>
<%@ include file="/WEB-INF/jsp/includes/generated/irma/header.jsp" %>
<%@ include file="/WEB-INF/jsp/includes/generated/irma/main-nav.jsp" %>

<div id="global-container" class="hidden">
    <div id="main-container" class="wrap-container">

        <div id="center-container">


            <%@ include file="/WEB-INF/jsp/includes/about-new-menu.jsp" %>

            <div class="about-nav">
                <div class="about-nav_list">
                    <div class="about-nav_i">
                        <a href="#about-lead" class="about-nav_link"><span class="about-nav_link-text"><spring:message code="page.about.us.about.13" /></span></a>
                    </div>
                    <div class="about-nav_i">
                        <a href="#about-meta" class="about-nav_link"><span class="about-nav_link-text"><spring:message code="page.about.us.about.14" /></span></a>
                    </div>
                    <div class="about-nav_i">
                        <a href="#about-contacts" class="about-nav_link"><span class="about-nav_link-text"><spring:message code="page.about.us.about.15" /></span></a>
                    </div>
                    <div class="about-nav_i">
                        <a href="#about-faq" class="about-nav_link"><span class="about-nav_link-text">FAQ</span></a>
                    </div>
                    <div class="about-nav_i">
                        <a href="#about-spec-projects" class="about-nav_link"><span class="about-nav_link-text"><spring:message code="page.about.us.about.16" /></span></a>
                    </div>
                </div>
            </div>

            <script>
                $('.about-nav_list').singlePageNav({
                    currentClass: 'active',
                    updateHash: true
                });

                $(function () {
                    $(window).on('resize', _.debounce(aboutNavPos, 1000));
                    $(window).on('scroll', aboutNavPos);
                    setTimeout(aboutNavPos, 500);

                    function aboutNavPos() {
                        var winHeight = $(window).height();
                        var winTop = $(window).scrollTop();

                        var nav = $('.about-nav');
                        var navHeight = nav.height();

                        var headerBlocks = ['.about-menu-wrap', '.header', '.h-special'];
                        var headerHeight = 0;

                        _.each(headerBlocks, function (e) {
                            headerHeight += $(e).outerHeight();
                        });

                        var navTop = winHeight / 2 - navHeight / 2;

                        if ( winTop < headerHeight ) {
                            if ( navTop < (headerHeight - winTop) ) {
                                navTop = headerHeight - winTop;
                            }
                        }

                        nav.css({
                            top: navTop
                        });
                    }
                });
            </script>



            <div class="about-lead" id="about-lead">

                <div class="about-lead_top">
                    <div class="wrap">
                        <div class="col-12">
                            <div class="about-lead_img">
                                <img src="//${hf:getStaticBaseUrl("")}/images/about/lead-img.jpg">
                            </div>

                            <div class="about-lead_quick">
                                <div class="wrap-row">
                                    <div class="about-lead_quick-text col-8">
                                        <spring:message code="page.about.us.about.01" />
                                    </div>
                                    <div class="about-lead_quick-more col-4">
                                        <button class="about-lead_quick-btn btn btn-primary"><spring:message code="page.about.us.about.02" /></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="about-lead_cont">
                    <div class="wrap">
                        <div class="col-12">

                            <div class="about-lead_text">
                                <p>Хотите издать книгу, записать музыкальный альбом, снять фильм, запустить мегакрутой продукт или, возможно, вы&nbsp;на&nbsp;пути к&nbsp;величайшему изобретению, которое осчастливит человечество? Тогда вы&nbsp;пришли по&nbsp;адресу: мы&nbsp;расскажем, как донести свои идеи до&nbsp;масс, поможем найти единомышленников, которые готовы вложить свои деньги в&nbsp;ваш проект, и&nbsp;вместе мы&nbsp;сделаем этот мир прекраснее во&nbsp;всех смыслах! Дерзайте&nbsp;&mdash; предлагайте идеи, обсуждайте, выбирайте, поддерживайте, короче, активно вливайтесь в&nbsp;творческий процесс!</p>

                                <h2>Когда и&nbsp;как завертелась &laquo;Планета&raquo;?</h2>
                                <p>У&nbsp;троих друзей&nbsp;&mdash; Федора Мурачковского, Макса Лакмуса и&nbsp;Василия Андрющенко&nbsp;&mdash; была давняя мечта сделать онлайн-ресурс, полезный для людей. Серьезным поводом для воплощения мечты в&nbsp;реальность стали две проблемы: кризис 2008 года и&nbsp;пиратство в&nbsp;музыкальной индустрии. Во&nbsp;время посиделок в&nbsp;кафе, за&nbsp;обсуждением насущных проблем, друзьям пришла в&nbsp;голову мысль запустить онлайн-сервис для музыкантов, на&nbsp;котором можно сделать предзаказ альбома. Таким образом, будущая Planeta.ru, &laquo;мать&raquo; российского краудфандинга, решала разом все задачи: у&nbsp;музыкантов есть деньги на&nbsp;запись новых альбомов; у&nbsp;поклонников-акционеров есть уникальная возможность стать первыми владельцами этих альбомов и&nbsp;получить эксклюзивные подарки от&nbsp;своих кумиров; и&nbsp;главное&nbsp;&mdash; проблема пиратства ликвидирована.</p>
                                <p>Но, как известно, идеи витают в&nbsp;воздухе, в&nbsp;итоге создатели Planeta.ru были не&nbsp;единственными, кому идея краудфандинга пришла на&nbsp;ум. Параллельно с&nbsp;зарождением Planeta.ru на&nbsp;Западе появились первые краудфандинговые платформы&nbsp;&mdash; IndieGoGo и&nbsp;Kickstarter. Создатели &laquo;Планеты&raquo; не&nbsp;хотели копировать опыт зарубежных коллег, поэтому Федор, Макс, Василий и&nbsp;команда крутых разработчиков, путем проб и&nbsp;ошибок, создали основные сервисы собственной, своей (!) крауд-платформы, т.&nbsp;е. запустили на&nbsp;российскую &laquo;орбиту&raquo; &laquo;Планету&raquo;.</p>

                                <h2>Первыми &laquo;клиентами&raquo; российского краудфандинга стали музыканты группы <nobr>&laquo;Би-2&raquo;</nobr></h2>
                                <p>С&nbsp;помощью Planeta.ru они собрали 1&nbsp;млн. 250&nbsp;тыс. рублей на&nbsp;выпуск альбома Spirit, которым поклонники Левы и&nbsp;Шуры наслаждались уже осенью 2011&nbsp;года. И&nbsp;понеслась! Постепенно Planeta.ru, оправдывая свое название, превратилась в&nbsp;глобальную сеть: вслед за&nbsp;музыкантами на&nbsp;платформу потянулись авторы проектов из&nbsp;других сфер (Евгений Гришковец, Colta и&nbsp;др.), заработали &laquo;планетные&raquo; сервисы&nbsp;&mdash; онлайн-трансляции концертов, музыкальных фестивалей; интернет-магазин эксклюзивных товаров авторов &laquo;Планеты&raquo;; появились спецпроекты и&nbsp;уникальный контент.</p>

                                <h2>&laquo;Планета&raquo; получила национальную &laquo;Премию Рунета&raquo;</h2>
                                <p>В&nbsp;2014 году в&nbsp;жизни Planeta.ru произошли сразу два важных события: во-первых, платформа получила национальную &laquo;Премию Рунета&raquo;, одну из&nbsp;самых статусных наград в&nbsp;области высоких технологий и&nbsp;интернета (номинация &laquo;Экономика, Бизнес и&nbsp;Инвестиции&raquo;); во-вторых, Planeta.ru стала единственным краудфандинговым сервисом в&nbsp;России, который получил зарубежное признание и&nbsp;вошел в&nbsp;международный список крауд-платформ.</p>

                                <h2>7&nbsp;апреля 2015 года Planeta.ru запустила собственную образовательную программу &laquo;Школа краудфандинга&raquo;</h2>
                                <p>«Школа краудфандинга» – это собственная образовательная программа Planeta.ru, в рамках которой студенты узнают, как с помощью краудфандинга можно реализовать свой проект, найти инвесторов и партнеров, провести маркетинговый анализ бизнес-идеи и разработать грамотную PR-стратегию. На завершающем этапе обучения студенты презентуют свои крауд-проекты ведущим экспертам в области бизнеса, кино, музыки, гражданских инициатив и краудфандинга.</p>
                                <p>Преподаватели школы&nbsp;&mdash; это ведущие эксперты российского краудфандинга, сотрудники Planeta.ru и&nbsp;авторы успешных проектов, реализованных благодаря народному финансированию. По&nbsp;итогам курса студенты, посетившие все занятия, получают дипломы выпускников &laquo;Школы краудфандинга&raquo; и&nbsp;готовую систему планирования и&nbsp;реализации краудфандинговой кампании.</p>
                                <p>Партнеры &laquo;Школы краудфандинга&raquo;&nbsp;&mdash; СКОЛКОВО Стартап Академия, ФРИИ, фонд &laquo;Наше будущее&raquo;, &laquo;ОПОРА РОССИИ&raquo;, фонд &laquo;Городские проекты Ильи Варламова и&nbsp;Максима Каца&raquo;, Московская школа кино, Гильдия неигрового кино и&nbsp;ТВ и&nbsp;др.</p>
                                <p>В&nbsp;марте 2016 года Planeta.ru запустила Первую Всероссийскую Школу краудфандинга, чтобы жители регионов могли получить все необходимые знания о&nbsp;народном финансировании у&nbsp;гуру российского краудфандинга бесплатно.</p>
                                <p>&laquo;Школа краудфандинга&raquo;&nbsp;&mdash; постоянный участник крупнейших мероприятий, касающихся социально-культурной жизни и&nbsp;сферы предпринимательства.</p>
                                <p>Сейчас Planeta.ru уже не&nbsp;просто краудфандинговая платформа, это &laquo;агентство полного цикла&raquo;: помимо народного финансирования мы&nbsp;занимаемся пиаром проектов, продакшеном, пост-продакшном, логистикой и&nbsp;обучением авторов проектов азам краудфандинга! Planeta.ru активно сотрудничает с&nbsp;научными центрами, бизнес-инкубаторами, общественными организациями.</p>
                                <p>Так что не&nbsp;переключайтесь, дальше будет только круче и&nbsp;круче!</p>
                            </div>

                        </div>
                    </div>

                </div>


                <script>
                    $('.about-lead_quick-btn').on('click', function () {
                        $('.about-lead_cont').slideToggle(300);
                    });
                </script>

            </div>

            <div class="about-chronicles">
                <div class="about-chronicles_head">
                    Хочешь узнать, как развивалась Planeta.ru?
                </div>
                <div class="about-chronicles_btn">
                    <a href="https://promo.planeta.ru/chronicles" class="btn btn-primary">Смотри наши хроники</a>
                </div>
            </div>

            <div class="about-meta" id="about-meta">
                <div class="wrap">
                    <div class="col-12">

                        <div class="about-meta_row">
                            <div class="about-meta_total">
                                <span class="about-meta_total-lbl">
                                    <spring:message code="page.about.us.about.03" />
                                </span>
                                <span class="about-meta_total-val">
                                    ${campaignsTotalStatsHuman}
                                </span>
                                <span class="about-meta_total-lbl about-meta_total-rub">
                                    <spring:message code="rubles.cases.count" arguments="${hf:plurals(campaignsTotalStats)}"></spring:message>
                                </span>
                            </div>
                        </div>

                        <div class="about-meta_row">

                            <div class="about-meta_i">
                                <div class="about-meta_projects">
                                    <span class="about-meta_projects-val">
                                        ${successfulCampaignsCountHuman}
                                    </span>
                                    <span class="about-meta_projects-lbl">
                                        <spring:message code="successful.campaigns.stats.message.short" arguments="${hf:plurals(successfulCampaignsCount)}"></spring:message>
                                    </span>
                                </div>
                            </div>

                            <div class="about-meta_i">
                                <div class="about-meta_wow">
                                    <spring:message code="page.about.us.about.04" />
                                </div>
                            </div>

                        </div>

                        <div class="about-meta_row">
                            <div class="about-meta_bill">
                                <span class="about-meta_bill-lbl">
                                    <spring:message code="page.about.us.about.05" />
                                </span>
                                <span class="about-meta_bill-val">
                                    1 500 <span class="b-rub">Р</span>
                                </span>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

            <c:if test="${promoAboutUsCampaignListOfMaps != null}">
            <div class="about-project-carousel">
                <c:forEach items="${promoAboutUsCampaignListOfMaps}" var="campaign">
                    <div class="about-project-carousel_i"
                         style="background-image:url('${campaign['imageUrl']}')">
                        <div class="about-project-carousel_cont">
                            <div class="about-project-carousel_category">
                                <spring:message code="page.about.us.about.12" />: «<span class="about-project-carousel_category-name">${campaign['mainTag']}</span>»
                            </div>

                            <div class="about-project-carousel_sum">
                                <div class="about-project-carousel_sum-val">
                                    ${campaign['collectedAmountHuman']}
                                </div>
                                <div class="about-project-carousel_sum-currency">
                                    <spring:message code="rubles.cases.count" arguments="${hf:plurals(campaign['collectedAmount'])}"></spring:message>
                                </div>
                            </div>

                            <div class="about-project-carousel_bottom">
                                <div class="about-project-carousel_author">
                                    ${campaign['creatorDisplayName']}:
                                </div>
                                <div class="about-project-carousel_name">
                                    ${campaign['name']}
                                </div>
                            </div>
                        </div>
                    </div>
                </c:forEach>

            </div>
            </c:if>
        </div>

        <script>
            $(function () {

                var owlInitTimer = isMobileDev ? 1000 : 0;

                setTimeout(function() {
                    var owl = $(".about-project-carousel").addClass('owl-carousel').owlCarousel({
                        lazyLoad: true,
                        autoplay: false,
                        autoplayTimeout: 5000,
                        autoplayHoverPause: true,
                        loop: true,
                        items: 1,
                        slideBy: 'page',
                        nav: true,
                        dots: false,
                        margin: 0,
                        mouseDrag: false,
                        smartSpeed: 600,
                        navText: [
                            '<span class="s-icon s-icon-arrow-thin-left">',
                            '<span class="s-icon s-icon-arrow-thin-right">'
                        ]
                    });
                }, owlInitTimer);
            });
        </script>





        <div class="about-categories-stats">
            <div class="about-categories-stats_list wrap">
                <c:forEach items="${campaignsTagsPercentages}" var="item">
                    <div class="about-categories-stats_i col-4">
                        <div class="about-categories-stats_i-cont">
                            <div class="about-categories-stats_bar" data-procent="${item.right}"></div>
                            <div class="about-categories-stats_cont">
                                <div class="about-categories-stats_val">
                                        ${item.right}<span class="about-categories-stats_val-percent">%</span>
                                </div>
                                <div class="about-categories-stats_name">
                                    <c:choose>
                                        <c:when test="${item.left == 'music'}"><spring:message code="page.about.us.about.06" /></c:when>
                                        <c:when test="${item.left == 'film'}"><spring:message code="page.about.us.about.07" /></c:when>
                                        <c:when test="${item.left == 'literature'}"><spring:message code="page.about.us.about.08" /></c:when>
                                        <c:when test="${item.left == 'social'}"><spring:message code="page.about.us.about.09" /></c:when>
                                        <c:when test="${item.left == 'business'}"><spring:message code="page.about.us.about.10" /></c:when>
                                        <c:when test="${item.left == 'charity'}"><spring:message code="page.about.us.about.11" /></c:when>
                                    </c:choose>
                                </div>
                            </div>
                        </div>
                    </div>
                </c:forEach>
            </div>
        </div>

        <script>
            var arc = $('.about-categories-stats_bar');
            arc.arcDraw({
                radius: 105,
                fill: '#fff',
                strokeWidth: 9,
                strokeBgColor: '#d8e0eb',
                strokeColor: '#3498db'
            });
        </script>





        <div class="about-map-stats">
            <img src="//${hf:getStaticBaseUrl("")}/images/about/map.png">
        </div>




        <div class="about-info">
            <div class="about-info_wrap">
                <div class="wrap">
                    <div class="col-12">

                        <div class="about-info_list">
                            <div class="about-info_i">
                                <div class="about-info_i-cont">
                                    <div class="about-info_ico">
                                        <span class="s-about-info-group"></span>
                                    </div>
                                    <div class="about-info_val">
                                        ${usersCountHuman}
                                    </div>
                                    <div class="about-info_lbl">
                                        <spring:message code="page.about.us.about.17" />
                                    </div>
                                </div>
                            </div>

                            <div class="about-info_i">
                                <div class="about-info_i-cont">
                                    <div class="about-info_ico">
                                        <span class="s-about-info-dance"></span>
                                    </div>
                                    <div class="about-info_val">
                                        700 000<span class="about-info_val-plus"> +</span>
                                    </div>
                                    <div class="about-info_lbl">
                                        <spring:message code="page.about.us.about.18" />
                                    </div>
                                </div>
                            </div>

                            <div class="about-info_i">
                                <div class="about-info_i-cont">
                                    <div class="about-info_ico">
                                        <span class="s-about-info-stars"></span>
                                    </div>
                                    <div class="about-info_val">
                                        500 000<span class="about-info_val-plus"> +</span>
                                    </div>
                                    <div class="about-info_lbl">
                                        <spring:message code="page.about.us.about.19" />
                                    </div>
                                </div>
                            </div>

                            <div class="about-info_i">
                                <div class="about-info_i-cont">
                                    <div class="about-info_ico">
                                        <span class="s-about-info-stats"></span>
                                    </div>
                                    <div class="about-info_val">
                                        <div class="about-info_sex">
                                            <div class="about-info_sex-i">
                                                <div class="about-info_sex-val">
                                                    40<span class="about-info_sex-percent">%</span>
                                                </div>
                                                <div class="about-info_sex-lbl">
                                                    <spring:message code="page.about.us.about.20" />
                                                </div>
                                            </div>

                                            <div class="about-info_sex-i">
                                                <div class="about-info_sex-val">
                                                    60<span class="about-info_sex-percent">%</span>
                                                </div>
                                                <div class="about-info_sex-lbl">
                                                    <spring:message code="page.about.us.about.21" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="about-info_lbl">
                                        <spring:message code="page.about.us.about.22" />
                                    </div>
                                </div>
                            </div>

                            <div class="about-info_i">
                                <div class="about-info_i-cont">
                                    <div class="about-info_ico">
                                        <span class="s-about-info-products"></span>
                                    </div>
                                    <div class="about-info_val">
                                        ${shopCurrentProductsCountHuman}
                                    </div>
                                    <div class="about-info_lbl">
                                        <spring:message code="decl.products" arguments="${hf:plurals(shopCurrentProductsCount)}"></spring:message> <spring:message code="page.about.us.about.23" />
                                    </div>
                                </div>
                            </div>

                            <div class="about-info_i">
                                <div class="about-info_i-cont">
                                    <div class="about-info_ico">
                                        <span class="s-about-info-school"></span>
                                    </div>
                                    <div class="about-info_val">
                                        12 000<span class="about-info_val-plus"> +</span>
                                    </div>
                                    <div class="about-info_lbl">
                                        <spring:message code="page.about.us.about.24" />
                                    </div>
                                </div>
                            </div>
                        </div>



                        <div class="about-info-age">
                            <div class="about-info-age_head">
                                <spring:message code="page.about.us.about.25" />
                            </div>


                            <div class="about-info-age_list-wrap">
                                <div class="about-info-age_list">
                                    <div class="about-info-age_i about-info-age_i-small" style="width: 5.8%;">
                                        <div class="about-info-age_i-cont" style="background-color:#3497db; height: 27px;">
                                            <div class="about-info-age_val">
                                                5,8%
                                            </div>
                                            <div class="about-info-age_lbl" style="color:#8ed1ff;">
                                                <spring:message code="page.about.us.about.26" />
                                            </div>
                                        </div>
                                    </div>

                                    <div class="about-info-age_i" style="width: 30.8%;">
                                        <div class="about-info-age_i-cont" style="background-color:#ff501a; height: 45px;">
                                            <div class="about-info-age_val">
                                                30,8%
                                            </div>
                                            <div class="about-info-age_lbl" style="color:#f7b8a5;">
                                                <spring:message code="page.about.us.about.27" />
                                            </div>
                                        </div>
                                    </div>

                                    <div class="about-info-age_i" style="width: 41.5%;">
                                        <div class="about-info-age_i-cont" style="background-color:#cc4391; height: 62px;">
                                            <div class="about-info-age_val">
                                                41,5%
                                            </div>
                                            <div class="about-info-age_lbl" style="color:#fbc1e2;">
                                                <spring:message code="page.about.us.about.28" />
                                            </div>
                                        </div>
                                    </div>

                                    <div class="about-info-age_i" style="width: 8.6%;">
                                        <div class="about-info-age_i-cont" style="background-color:#41c8d5; height: 32px;">
                                            <div class="about-info-age_val">
                                                8,6%
                                            </div>
                                            <div class="about-info-age_lbl" style="color:#8df3fd;">
                                                <spring:message code="page.about.us.about.29" />
                                            </div>
                                        </div>
                                    </div>

                                    <div class="about-info-age_i" style="width: 13.3%;">
                                        <div class="about-info-age_i-cont" style="background-color:#ffcf0d; height: 39px;">
                                            <div class="about-info-age_val">
                                                13,3%
                                            </div>
                                            <div class="about-info-age_lbl" style="color:#fce37d;">
                                                <spring:message code="page.about.us.about.30" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </div>




        <div class="about-contacts" id="about-contacts">
            <div class="about-contacts_block">


                <div class="about-contacts_head">
                    <div class="about-contacts_head-logo">
                        <img src="//${hf:getStaticBaseUrl("")}/images/about/logo.svg">
                    </div>
                    <div class="about-contacts_head-cont">
                        <div class="about-contacts_head-lbl">
                            <spring:message code="page.about.us.about.31" />
                        </div>
                        <div class="about-contacts_head-val">
                            +7 495 181 05 05
                        </div>
                    </div>
                </div>


                <div class="about-contacts_list">
                    <div class="about-contacts_i">
                        <div class="about-contacts_ava">
                            <div class="about-contacts_ava-img">
                                <img src="//${hf:getStaticBaseUrl("")}/images/about/vasilina.jpg">
                            </div>
                        </div>
                        <div class="about-contacts_cont">
                            <div class="about-contacts_info">
                                <div class="about-contacts_prof">
                                    <spring:message code="page.about.us.about.32" />
                                </div>
                                <div class="about-contacts_name">
                                    <spring:message code="page.about.us.about.33" />
                                </div>
                                <div class="about-contacts_mail">
                                    <a href="mailto:vasilina@planeta.ru">vasilina@planeta.ru</a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="about-contacts_i">
                        <div class="about-contacts_ava">
                            <div class="about-contacts_ava-img">
                                <img src="//${hf:getStaticBaseUrl("")}/images/about/ignatenko.jpg">
                            </div>
                        </div>
                        <div class="about-contacts_cont">
                            <div class="about-contacts_info about-contacts_info__long">
                                <div class="about-contacts_prof">
                                    <spring:message code="page.about.us.about.34" />
                                </div>
                                <div class="about-contacts_name">
                                    <spring:message code="page.about.us.about.41" />
                                </div>
                                <div class="about-contacts_mail">
                                    <a href="mailto:irina@planeta.ru">pr@planeta.ru</a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="about-contacts_i">
                        <div class="about-contacts_ava">
                            <div class="about-contacts_ava-img">
                                <img src="//${hf:getStaticBaseUrl("")}/images/about/egor.jpg">
                            </div>
                        </div>
                        <div class="about-contacts_cont">
                            <div class="about-contacts_info">
                                <div class="about-contacts_prof">
                                    <spring:message code="page.about.us.about.36" />
                                </div>
                                <div class="about-contacts_name">
                                    <spring:message code="page.about.us.about.37" />
                                </div>
                                <div class="about-contacts_mail">
                                    <a href="mailto:egor@planeta.ru">egor@planeta.ru</a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="about-contacts_i">
                        <div class="about-contacts_ava">
                            <div class="about-contacts_ava-img">
                                <img src="//${hf:getStaticBaseUrl("")}/images/about/stenkina.jpg">
                            </div>
                        </div>
                        <div class="about-contacts_cont">
                            <div class="about-contacts_info">
                                <div class="about-contacts_prof">
                                    <spring:message code="page.about.us.about.38" />
                                </div>
                                <div class="about-contacts_name">
                                    <spring:message code="page.about.us.about.39" />
                                </div>
                                <div class="about-contacts_mail">
                                    <a href="mailto:stenkina@planeta.ru">stenkina@planeta.ru</a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="about-contacts_i">
                        <div class="about-contacts_ava">
                            <div class="about-contacts_ava-img">
                                <img src="https://s1.planeta.ru/i/13390a/big.jpg">
                            </div>
                        </div>
                        <div class="about-contacts_cont">
                            <div class="about-contacts_info">
                                <div class="about-contacts_prof">
                                    <spring:message code="page.about.us.about.40" />
                                </div>
                                <div class="about-contacts_name">
                                    <spring:message code="page.about.us.about.54" />
                                </div>
                                <div class="about-contacts_mail">
                                    <a href="mailto:pr@planeta.ru">charitypr@planeta.ru</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>





        <div class="about-faq" id="about-faq">
            <div class="wrap">
                <div class="col-12">

                    <div class="about-faq_head">
                        FAQ
                    </div>

                    <div class="about-faq_list">
                        <div id="faq-question-1" class="about-faq_i">
                            <div class="about-faq_question">
                                <div class="about-faq_question-text">
                                    <spring:message code="page.about.us.about.42" />
                                </div>
                            </div>
                            <div class="about-faq_answer">
                                <div class="about-faq_answer-cont">
                                    <p>Planeta.ru&nbsp;&mdash; крупнейшая российская краудфандинговая платформа, один из&nbsp;первых в&nbsp;Рунете сервисов для коллективного финансирования проектов. Лауреат &laquo;Премии Рунета 2014&raquo; в&nbsp;номинации &laquo;Экономика, Бизнес и&nbsp;Инвестиции&raquo;.</p>
                                    <p>Planeta.ru это больше, чем просто сайт или удобные сервисы. Это новая философия, форма отношений между генераторами идей, авторами проектов и&nbsp;их&nbsp;аудиторией. Мы&nbsp;создали экосистему в&nbsp;Сети, которая влияет на&nbsp;оффлайновую жизнь и&nbsp;открывает людям новые возможности для самореализации.</p>
                                </div>
                            </div>
                        </div>
                        <div id="faq-question-2" class="about-faq_i">
                            <div class="about-faq_question">
                                <div class="about-faq_question-text">
                                    <spring:message code="page.about.us.about.43" />
                                </div>
                            </div>
                            <div class="about-faq_answer">
                                <div class="about-faq_answer-cont">
                                    <p>7&nbsp;июня 2012&nbsp;года.</p>
                                </div>
                            </div>
                        </div>
                        <div id="faq-question-3" class="about-faq_i">
                            <div class="about-faq_question">
                                <div class="about-faq_question-text">
                                    <spring:message code="page.about.us.about.44" />
                                </div>
                            </div>
                            <div class="about-faq_answer">
                                <div class="about-faq_answer-cont">
                                    <p>В&nbsp;2009 году у&nbsp;трех друзей&nbsp;&mdash; Федора Мурачковского, Макса Лакмуса и&nbsp;Василия Андрющенко&nbsp;&mdash; появилась идея сделать ресурс для музыкантов, который помог&nbsp;бы решить вопрос борьбы с&nbsp;пиратством. Подумали, что выходом из&nbsp;ситуации может быть создание онлайновой схемы предзаказа для музыкальных альбомов. В&nbsp;это&nbsp;же время появились первые краудфандинговые ресурсы на&nbsp;Западе: заработали IndieGoGo и&nbsp;Kickstarter. Основатели Planeta.ru не&nbsp;хотели, чтобы их&nbsp;проект был просто &laquo;калькой&raquo; какого-то из&nbsp;западных ресурсов, началась разработка собственной платформы и&nbsp;основных сервисов Planeta.ru.</p>
                                    <p>В&nbsp;2011 году платформа запустилась в&nbsp;тестовом режиме: первым краудфандинговым проектом стал сбор денег на&nbsp;выпуск альбома Spirit группы &laquo;БИ-2&raquo;. Краудфандинговая схема сработала. Поклонники поддержали любимых музыкантов, было собрано 1&nbsp;250&nbsp;000&nbsp;рублей, и&nbsp;осенью 2011 года альбом вышел в&nbsp;свет. К&nbsp;тому моменту стало понятно, что краудфандинг применим не&nbsp;только к&nbsp;музыкальной сфере. Сначала мы&nbsp;создали успешные музыкальные кейсы на&nbsp;Planeta.ru, показали, как работает народное финансирование, затем начали привлекать проекты из&nbsp;других категорий и&nbsp;включать планомерно остальные &laquo;планетные&raquo; сервисы: онлайн трансляции и&nbsp;интернет-магазин, начали создавать свой контент, делать спецпроекты.</p>
                                    <p>Planeta.ru постоянно развивается и&nbsp;расширяет сферы деятельности, сейчас ресурс работает, как своего рода, &laquo;агентство полного цикла&raquo;: помимо краудфандинга мы&nbsp;занимаемся логистикой, продвижением, пиаром проектов, продакшеном, пост-продакшном и&nbsp;обучением.</p>
                                </div>
                            </div>
                        </div>
                        <div id="faq-question-4" class="about-faq_i hide">
                            <div class="about-faq_question">
                                <div class="about-faq_question-text">
                                    На «Планете» авторы проекта могут получить деньги, если проект собрал более 50% от заявленной суммы, но не достиг 100% от финансовой цели. Почему решили ввести такое правило, и как, на практике, авторы проектов реализуют денежные средства, ведь, по замыслу, их недостаточно для полной реализации проекта?
                                </div>
                            </div>
                            <div class="about-faq_answer">
                                <div class="about-faq_answer-cont">
                                    <p>Правила и условия у краудфандинговых площадок во всем мире разные. «Все или ничего» — правило Кикстартера, это не значит, что все остальные работают также. На «Планете» более гибкая схема. Автор сам решает, забирать ли ему деньги, если он не собрал 100% от заявленной суммы. Если он уверен в том, что найдет остальные деньги, сможет реализовать свою идею и отдать своим акционерам все, что обещал, то почему нет? Согласитесь, 60, 80% от суммы – это лучше, чем ничего.</p>
                                    <p>Краудфандинг это показатель доверия, репутация автора проекта служит лучшей защитой и гарантией для акционеров проекта, особенно если идет речь о репутации в сети.</p>
                                    <p>Более того, воплотить проект, не достигнув 100% барьера в крауд-сборе, реально, потому что краудфандинг это не только финансовый, но и мощный PR-инструмент. Активное освещение информации о проекте в СМИ позволяет авторам находить массу единомышленников, которые активно включаются в проект и помогают автору реализовать задуманное. Таким образом, общий бюджет проекта сокращается, так как, например, нашлись люди, готовые оказать свои профессиональные услуги на безвозмездной основе, а не за деньги, как предполагалось при запуске проекта.</p>
                                </div>
                            </div>
                        </div>
                        <div id="faq-question-5" class="about-faq_i hide">
                            <div class="about-faq_question">
                                <div class="about-faq_question-text">
                                    Какие еще сервисы, кроме краудфандинга есть на «Планете»? Как они появились, с какой целью?
                                </div>
                            </div>
                            <div class="about-faq_answer">
                                <div class="about-faq_answer-cont">
                                    <p>Мы определили для себя, что «Планета» — это социально-сервисная экосистема, которая помогает различными способами монетизировать идеи, проекты, творчество и авторский контент. Мы не остановились на одном краудфандинговом сервисе, и сейчас сайт работает как, своего рода, «агентство полного цикла». Мы запустили онлайн трансляции, программу лояльности для активных акционеров  и наш уникальный магазин, где продаются «результаты» краудфандинговых кампаний либо уникальные лоты от авторов проектов.</p>
                                    <p>Дополнительные сервисы позволяют проектам эффективней реализовать свои идеи, если, например, группа собирает на сайте деньги на запись нового альбома, то  трансляция их концерта будет хорошим промо, но это не обязательная опция.</p>
                                    <p>Работа этих сервисов хорошо сказывается на аудитории ресурса, позволяет нам заинтересовать и удержать людей на сайте.</p>
                                </div>
                            </div>
                        </div>
                        <div id="faq-question-6" class="about-faq_i hide">
                            <div class="about-faq_question">
                                <div class="about-faq_question-text">
                                    Почему люди участвуют в краудфандинговых проектах? Какая у них мотивация?
                                </div>
                            </div>
                            <div class="about-faq_answer">
                                <div class="about-faq_answer-cont">
                                    <p>Потому что краудфандинг – это интересно, это эмоции, чувство причастности к чему-то важному для тебя, осознание того, что ты можешь что-то сделать, на что-то реально повлиять, кому-то помочь, пусть не своими руками, а финансовым участием. Да еще и получить какую-то уникальную вещь в качестве вознаграждения за участие в проекте. Очень важная составляющая краудфандинговых проектов — это фактор социальной вовлеченности. Люди начинают активно участвовать не только в развлекательных проектах, но и в процессе решения серьезных социальных и культурных задач.</p>
                                </div>
                            </div>
                        </div>
                        <div id="faq-question-7" class="about-faq_i hide">
                            <div class="about-faq_question">
                                <div class="about-faq_question-text">
                                    Кто из известных людей открывал на «Планете» свои проекты?
                                </div>
                            </div>
                            <div class="about-faq_answer">
                                <div class="about-faq_answer-cont">
                                    <p>Гарри Бардин, Евгений Гришковец, Виктор Шендерович, Владимир Мирзоев, Олег Куваев, Александр Войцеховский, Федор Павлов-Андреевич, Артемий Троицкий, Дмитрий Васюков, Любовь Аркус, Семен Чайка, Михаил Сегал, Елена Погребижская, Дмитрий Чернышев (mi3ch), Ольга Дыховичная, Игорь Шпиленок, Сергей Пархоменко; десятки российских музыкальных коллективов: “БИ-2”, “Несчаcтный случай”, Ляпис Трубецкой, Петр Налич, “Animal ДжаZZ”, “Ундервуд”, “Король и Шут” и другие.</p>
                                </div>
                            </div>
                        </div>
                        <div id="faq-question-8" class="about-faq_i hide">
                            <div class="about-faq_question">
                                <div class="about-faq_question-text">
                                    Какие категории проектов наиболее популярны у пользователей?
                                </div>
                            </div>
                            <div class="about-faq_answer">
                                <div class="about-faq_answer-cont">
                                    <p>По сборам лидируют творческие проекты, в основном, в категориях «кино» и «музыка». Впрочем, эти категории самые популярные не только в России, но и на западных площадках. Все благотворительные проекты у нас успешны по умолчанию, так как мы выплачиваем всю сумму сбора. В последнее время растет популярность категорий «общество», «литература» и «бизнес», появляются инновационные проекты.</p>
                                </div>
                            </div>
                        </div>
                        <div  id="faq-question-9" class="about-faq_i hide">
                            <div class="about-faq_question">
                                <div class="about-faq_question-text">
                                    Почему музыкальные проекты имеют большой успех в краудфандинге?
                                </div>
                            </div>
                            <div class="about-faq_answer">
                                <div class="about-faq_answer-cont">
                                    <p>Для успеха крауд-проекта очень важен сам продукт или идея (на что собирают деньги), его актуальность и уникальность, а также фактор доверия к автору проекта и лояльная аудитория. Музыканты – люди, обладающие большим кредитом доверия, поклонниками и уникальным продуктом – их творчеством. Именно поэтому многие российские музыканты быстро поняли, насколько хорошо работает краудфандинговая схема.</p>
                                </div>
                            </div>
                        </div>
                        <div id="faq-question-10" class="about-faq_i hide">
                            <div class="about-faq_question">
                                <div class="about-faq_question-text">
                                    Почему «Планета» работает с благотворительными проектами?
                                </div>
                            </div>
                            <div class="about-faq_answer">
                                <div class="about-faq_answer-cont">
                                    <p>Когда мы запустили «Планету», то выбрали для себя модель, близкую известному крауд-ресурсу IndieGoGo (они отличаются от Кикстартера гибкой схемой выплат и тем, что много работают с социальными и благотворительными проектами), и сразу решили для себя, что будем работать с благотворительностью. Некоторые классические крауд-сервисы отказываются от благотворительных проектов, что, на наш взгляд, недальновидно, потому что аудитория этих проектов невероятно лояльна, она уже знает, что можно и нужно поддерживать финансово тех, кому это необходимо. «Планете» интересно привлекать такую аудиторию. Благотворительные фонды в свою очередь получают удобный инструмент и дополнительную площадку для сбора средств.</p>
                                    <p>«Планета» работает только с реальными, проверенными благотворительными фондами, так мы можем гарантировать, что проект не «фейк», что деньги пойдут тем, для кого их собирали. К сожалению, мы не выставляем на сайте благотворительные проекты от физических лиц. И этому есть объяснение: у нас нет специальной службы, которая бы проверяла каждый конкретный случай, поэтому мы и работаем через фонды.</p>
                                    <p>В случае с благотворительностью, краудфандинговые технологии применяются в несколько усеченном виде. Существуют специальные условия для благотворительных фондов: им перечисляется любая сумма (за вычетом расходов платежных систем), какую бы они ни собрали за время краудфандинговой кампании. Также «Планета» не берет с таких проектов сервисный сбор. Это сделано для стимулирования развития благотворительной деятельности в России в общем, и на ресурсе –— в частности. Но, по статистике, 45-50% благотворительных проектов достигают финансовой цели или собирают большую сумму. Сейчас категория «благотворительность» очень успешна и популярна на «Планете».</p>
                                </div>
                            </div>
                        </div>
                        <div id="faq-question-11" class="about-faq_i hide">
                            <div class="about-faq_question">
                                <div class="about-faq_question-text">
                                    Как вы оцениваете работу своей площадки? Удается ли зарабатывать на проекте?
                                </div>
                            </div>
                            <div class="about-faq_answer">
                                <div class="about-faq_answer-cont">
                                    <p>Мы верим, что «Планета» уже стала виртуальным пространством, меняющим реальность и дающим новые возможности для бизнеса, творчества и социальной активности. Мы хотим научить как можно большее количество людей эффективно воплощать в жизнь свои идеи и проекты.</p>
                                    <p>Мы пионеры и евангелисты краудфандинга в России. За три года работы мы не только усовершенствовали свой ресурс, но и провели огромную просветительскую работу, сумели объяснить аудитории Рунета, что такое краудфандинг, как он работает и какую пользу может принести.</p>
                                    <p>7 апреля 2015 года «Планета» открыла Школу краудфандинга. Первый «поток» состоял из восьми лекций и практических занятий, которые проводили эксперты и авторы успешных крауд-проектов. В сентябре запустится новый курс, и эта практика станет постоянной.</p>
                                    <p>В этом году образовательная и имиджевая составляющая бренда «Планеты» существенно вырастет: мы запустим новые долгосрочные проекты в партнерстве с Центром развития инноваций «Технология возможностей и МГТУ им. Баумана, Инновационным центром и бизнес-школой «Сколково» и Финансовым Университетом.</p>
                                    <p>«Планета» активно работает с институтами развития, различными бизнес-инкубаторами, благотворительными фондами, музыкальными и городскими фестивалями, культурными и образовательными кластерами и десятками других организаций.</p>
                                    <p>В 2013 году Планета вошла в шорт-лист «Премии Рунета» (проекту был всего год), а в прошлом году победила в номинации «Экономика, Бизнес и Инвестиции». Это признание — как со стороны экспертов Рунета, так и обычных людей.</p>
                                    <p>«Планету» поддерживают частные инвесторы, поскольку это уникальный программный продукт, над ним работают много людей, но все идет по заранее установленному плану. Разнообразие сервисов дает нам возможность зарабатывать не только с процента за краудфандинг — сейчас мы активно развиваем спецпроекты и программы социальной ответственности с коммерческими структурами.</p>
                                </div>
                            </div>
                        </div>
                        <div id="faq-question-12" class="about-faq_i hide">
                            <div class="about-faq_question">
                                <div class="about-faq_question-text">
                                    За последний год «Планета» запустила ряд совместных спецпроектов с крупными корпорациями по благотворительности и социальному предпринимательству. Какие перспективы вы видите в такого рода сотрудничестве с бизнесом?
                                </div>
                            </div>
                            <div class="about-faq_answer">
                                <div class="about-faq_answer-cont">
                                    <p>Целью спецпроектов с «МегаФоном», «РУСАЛом» и «Липтоном» является создание, эффективное финансирование и реализация социально значимых некоммерческих проектов в различных областях, разработка и внедрение новых социальных технологий и развитие благотворительных институтов России.</p>
                                    <p>Например, совместно с «МегаФоном» мы запустили благотворительную программу «Мегафон помогает». В рамках этой программы активные проекты благотворительных фондов, набравшие с помощью краудфандинга более 25 процентов от своей финансовой цели, получают финансовую поддержку от компании: каждый последующий взнос акционеров проекта умножается в четыре раза. Таким образом, эффективно объединяются инициативы некоммерческой организации, отдельных жертвователей и крупного бизнеса. В кризисный для России период такие «союзы» помогут добиться значительного прорыва в сфере благотворительности.</p>
                                    <p>Проект «Goodstarter» с компанией Lipton ориентирован на социальных предпринимателей. Он позволяет людям эффективно оценить и реализовать свой бизнес, в том числе с помощью наших партнеров — экспертов из Сколково. Участники этого проекта учатся правильно структурировать свою работу, выстраивать грамотную маркетинговую кампанию, узнают, как выйти с проектом на широкую аудиторию и, наконец, как получить грант от Lipton. Этот проект стал очень популярным за несколько месяцев: сейчас в его рамках открылась специальная образовательная программа для социальных предпринимателей.</p>
                                    <p>Появление проектов такого рода означает, что к краудфандингу в России уже серьезно относятся. Это повлечет за собой совершенно другой уровень доверия и активности людей, а также позволит реализовать действительно масштабные проекты.</p>
                                    <p>В сентябре мы запускаем большой совместный проект с Финансовым Университетом: студенты смогут получить теоретические и практические знания о краудфандинге и найти финансирование на свои проекты.</p>
                                </div>
                            </div>
                        </div>
                        <div id="faq-question-13" class="about-faq_i hide">
                            <div class="about-faq_question">
                                <div class="about-faq_question-text">
                                    От чего зависит успех краудфандингового проекта?
                                </div>
                            </div>
                            <div class="about-faq_answer">
                                <div class="about-faq_answer-cont">
                                    <p>Нелегко убедить людей дать вам деньги — об этом нужно помнить. Задайте себе вопрос «почему бы я сам поддержал свой проект, чем и кому он может быть интересен?». Отвечайте честно и искренне. Поймите, насколько ваш проект актуален и уникален. Популярными становятся проекты, в которых видна индивидуальность авторов.</p>
                                    <p>Многие спрашивают, а как продвигать крауд-проект? Если коротко, то как любой другой. Только больше! Проект становится частью вашей жизни. Над краудфандинговой кампанией надо работать — до его запуска, вовремя и после. Нельзя просто сидеть сложа руки и ждать, пока деньги посыплются с неба. О своем проекте надо, как минимум, рассказать окружающим. Важно записать искреннее или креативное видео, это привлекает внимание, но это не значит, что не нужно подробно описать проект, рассказать о тех, кто его придумал.</p>
                                    <p>Надо изначально понимать, кто твоя аудитория, есть ли она вообще, где ее найти, какие инструменты работы с аудиторией задействовать, на каком языке говорить, в какие СМИ обратиться за поддержкой. Нельзя завышать финансовые цели, вы должны доходчиво объяснить людям, зачем и сколько денег вам понадобится. Поработать над вознаграждениями, которые будут в проекте, продумать, как доставить участникам готовый продукт.</p>
                                    <p>Все успешные проекты имеют высокую степень проработки. Редко бывает так, что проект запустился и сам по себе собрал нужную сумму.</p>
                                    <p>Не забывайте, что краудфандинговый проект может удачно сочетать в себе сбор необходимой для реализации идеи суммы денег, отличный пиар-повод и маркетинговое исследование. Во всем мире запуск краудфандинговой кампании часто используют как информационный повод или вообще строят на них промо-кампанию, например, нового гаджета, фильма или бизнеса...</p>
                                </div>
                            </div>
                        </div>
                        <div id="faq-question-14" class="about-faq_i hide">
                            <div class="about-faq_question">
                                <div class="about-faq_question-text">
                                    Отличается ли российский краудфандинг от западного?
                                </div>
                            </div>
                            <div class="about-faq_answer">
                                <div class="about-faq_answer-cont">
                                    <p>Особых различий нет. В России краудфандинг развивается немного медленнее, так как у нас другой менталитет, уровень достатка, вовлеченности людей в Интернет и другая степень доверия к финансовым проектам. Но тренд развития одинаковый. Практически все алгоритмы, которые работают в США или Европе, работают и здесь, особенно, при грамотном подходе. Да, у нас разные политические и экономические условия, но принципы жизни и взаимодействия людей, мотивации одни и те же.</p>
                                    <p>Пока в России в краудфандинг приходят, в основном, творческие, общественные, благотворительные проекты. Но уже сейчас люди понимают, что краудфандинг можно использовать для продвижения, для запуска своего бизнеса, он позволяет значительно сэкономить на маркетинге. Например, с помощью проекта вы можете протестировать спрос и лучше понять аудиторию. К сожалению, на российских крауд-платформах пока мало проектов, связанных с технологиями и наукой. Такие проекты стремятся сразу выходить на международный уровень, но уже сейчас ситуация меняется, и мы стараемся сделать для этого все возможное.</p>
                                </div>
                            </div>
                        </div>
                        <div id="faq-question-15" class="about-faq_i hide">
                            <div class="about-faq_question">
                                <div class="about-faq_question-text">
                                    Каковы перспективы развития рынка краудфандинга в России?
                                </div>
                            </div>
                            <div class="about-faq_answer">
                                <div class="about-faq_answer-cont">
                                    <p>Рынок краудфандинга в России только формируется. По нашим оценкам, общий сбор по России в 2014 году превысил 300 миллионов рублей, и объем рынка в ближайшие два года вырастет в два-три раза. Запас роста очень большой, потому что существует много факторов, способствующих развитию краудфандинговой модели. В первую очередь, она уникальна вовлечением людей в процесс создания продукта или в процесс решения социальных вопросов. Сейчас еще не так много людей вовлечено в краудфандинг, но, в конечном итоге, когда сработает эффект критической массы, рынок начнет расти быстро. Будут вовлекаться новые авторы проектов и те, кто их поддерживает, будут возвращаться те, кто уже когда-то поучаствовал в крауд-проектах, будут расти суммы сборов. Плюс, очевидно, что краудфандинг — это глобальный мировой тренд.</p>
                                </div>
                            </div>
                        </div>
                        <div id="faq-question-16" class="about-faq_i hide">
                            <div class="about-faq_question">
                                <div class="about-faq_question-text">
                                    Насколько серьезным барьером для развития краудфандинга является сложившееся в обществе недоверие ко всякого рода финансовым проектам?
                                </div>
                            </div>
                            <div class="about-faq_answer">
                                <div class="about-faq_answer-cont">
                                    <p>В России люди много лет, по понятным причинам, с недоверием относились к интернет-технологиям, так или иначе связанным с деньгами. Но сейчас ситуация в корне меняется, глобальный рост продаж в сфере интернет-торговли это подтверждает. Доверие к краудфандингу растет с каждым днем, потому что уже есть сформировавшиеся и успешные кейсы, законченные проекты, которые собрали деньги, выпустили то, что хотели, и, главное, есть довольные участники таких проектов, которые становятся двигателями самой идеи краудфандинга, за что мы им безмерно благодарны.</p>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="about-faq_more">
                        <span class="btn btn-primary js-faq-get-more"><spring:message code="page.about.us.about.45" /></span>
                    </div>

                </div>
            </div>
        </div>

        <script>
            $('.about-faq_question').on('click', function () {
                $(this).next().slideToggle(300);
            });
        </script>




        <div class="about-spec-projects" id="about-spec-projects">
            <div class="wrap">
                <div class="col-12">

                    <div class="about-spec-projects_head">
                        <spring:message code="page.about.us.about.46" />
                    </div>

                    <div class="about-spec-projects_list">

                        <a href="http://goodstarter.liptontea.ru/"class="about-spec-projects_i">
                            <span class="about-spec-projects_img">
                                <img src="//${hf:getStaticBaseUrl("")}/images/about/goodstarter.png" alt="goodstarter">
                            </span>
                            <span class="about-spec-projects_name">
                                Goodstarter
                            </span>
                        </a>


                        <a href='https://planeta.ru/search/projects?query=&categories=TECHNO' class="about-spec-projects_i">
                            <span class="about-spec-projects_img">
                                <img src="//${hf:getStaticBaseUrl("")}/images/about/techno.png">
                            </span>
                            <span class="about-spec-projects_name">
                                <spring:message code="page.about.us.about.47" />
                            </span>
                        </a>


                        <a href='https://planeta.ru/search/projects?query=&categories=MEGAFON' class="about-spec-projects_i">
                            <span class="about-spec-projects_img">
                                <img src="//${hf:getStaticBaseUrl("")}/images/about/megafon.png">
                            </span>
                            <span class="about-spec-projects_name">
                                <spring:message code="page.about.us.about.48" />
                            </span>
                        </a>


                        <a href='https://biblio.planeta.ru/' class="about-spec-projects_i">
                            <span class="about-spec-projects_img">
                                <img src="//${hf:getStaticBaseUrl("")}/images/about/bibliorodina.png">
                            </span>
                            <span class="about-spec-projects_name">
                                <spring:message code="page.about.us.about.49" />
                            </span>
                        </a>


                        <a href='https://planeta.ru/welcome/bazar.html' class="about-spec-projects_i">
                            <span class="about-spec-projects_img">
                                <img src="//${hf:getStaticBaseUrl("")}/images/about/dbazar.png">
                            </span>
                            <span class="about-spec-projects_name">
                                <spring:message code="page.about.us.about.50" />
                            </span>
                        </a>


                        <a href='https://planeta.ru/welcome/university.html' class="about-spec-projects_i">
                            <span class="about-spec-projects_img">
                                <img src="//${hf:getStaticBaseUrl("")}/images/about/online-kampus.png">
                            </span>
                            <span class="about-spec-projects_name">
                                <spring:message code="page.about.us.about.51" />
                            </span>
                        </a>


                        <a href='https://planeta.ru/welcome/clever.html' class="about-spec-projects_i">
                            <span class="about-spec-projects_img">
                                <img src="//${hf:getStaticBaseUrl("")}/images/about/clever.png">
                            </span>
                            <span class="about-spec-projects_name">
                                <spring:message code="page.about.us.about.52" />
                            </span>
                        </a>


                    </div>


                </div>
            </div>
        </div>


    </div>

</div>
</div>
<%@ include file="/WEB-INF/jsp/includes/generated/irma/footer.jsp" %>
</body>
</html>

<c:choose>
    <c:when test="${not empty customMetaTag.title}">
        <title>${customMetaTag.title}</title>
        <meta property="og:title" content="<c:out value="${customMetaTag.title}"/>"/>
    </c:when>
    <c:otherwise>
        <title>Создай проект на краудфандинговой платформе Planeta.ru</title>
        <meta property="og:title" content="Создай проект на краудфандинговой платформе Planeta.ru"/>
    </c:otherwise>
</c:choose>
<c:choose>
    <c:when test="${not empty customMetaTag.description}">
        <meta name="description" content="${customMetaTag.description}"/>
        <meta property="og:description" content="${customMetaTag.description}"/>
    </c:when>
    <c:otherwise>
        <meta name="description" content="С помощью краудфандинговой платформы Planeta.ru вы сможете воплотить в жизнь ваши идеи и найти единомышленников, готовых поддержать ваши начинания."/>
        <meta property="og:description" content="С помощью краудфандинговой платформы Planeta.ru вы сможете воплотить в жизнь ваши идеи и найти единомышленников, готовых поддержать ваши начинания."/>
    </c:otherwise>
</c:choose>

<c:if test="${pageType == 'movie'}">
    <meta name="og:image" content='//${hf:getStaticBaseUrl("")}/images/quickstart/quickstart-movie.jpg'/>
</c:if>

<c:if test="${not empty customMetaTag.keywords}">
    <meta name="keywords" content="${customMetaTag.keywords}"/>
</c:if>
<%@include file="/WEB-INF/jsp/includes/quickstart-google-experiment.jsp" %>
<link type="text/css" rel="stylesheet" href="//${hf:getStaticBaseUrl("")}/css-generated/quickstart.css" />
<%@include file="/WEB-INF/jsp/includes/generated/common-favicon.jsp" %>
<%@include file="/WEB-INF/jsp/includes/generated/stat-counters-head.jsp" %>
<script type="text/javascript" src="//${hf:getStaticBaseUrl("/res")}/js/templates.js?flushCache=${properties['static.flushCache']}"></script>
<script type="text/javascript" src="//${hf:getStaticBaseUrl("/res")}/js/quickstart.js?compress=${properties['static.compress']}&flushCache=${properties['static.flushCache']}"></script>
<script type="text/javascript">
    function getBook() {
        function validateEmail(email) {
            var re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return re.test(email);
        }
        if (validateEmail($('.js-get-book-email').val())) {
            $('.js-open-error.pln-dropdown').removeClass('error');
            Backbone.sync('read', null, {
                url: "/welcome/get-quickstart-book.json",
                data: {
                    email: $('.js-get-book-email').val(),
                    type: '${pageType}'
                }
            }).done(function(response){
                if(window.gtm) {
                    window.gtm.trackUniversalEvent('${eventLabel}');
                }

                if (response && response.success) {
                    $('.js-get-book-btn-cnt.pln-dropdown').addClass('open');
                    setTimeout(function() { $('.js-get-book-btn-cnt.pln-dropdown').removeClass('open')}, 3000);
                }
                if(response && response.success === false && workspace && response.errorMessage) {
                    workspace.appView.showErrorMessage(response.errorMessage);
                }
            });
        }
        else {
            $('.js-open-error.pln-dropdown').addClass('open').addClass('error');
            setTimeout(function() { $('.js-open-error.pln-dropdown').removeClass('open')}, 3000);
        }
    }
    $(function() {
        var pddOptions =  {
            popup: '> .pln-d-popup',
            activeClass: 'open'
        }
        $('.js-open-error.pln-dropdown').dropPopup(pddOptions);
        $('.js-get-book-btn-cnt.pln-dropdown').dropPopup(pddOptions);
    });
</script>
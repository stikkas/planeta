<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<c:if test="${properties['application.host'] == 'planeta.ru' && requestScope['javax.servlet.forward.servlet_path'] == '/'}">
    <!-- Google Analytics Content Experiment code -->
    <script>function utmx_section() {
    }
    function utmx() {
    }
    window.loadGoogleABTestScript= (function() {
        var key = '78930212-2', location = document.location, cookie = document.cookie;
        if (location.search.indexOf('utm_expid=' + key) > 0)return;
        function getCookieValue(name) {
            if (cookie) {
                var i = cookie.indexOf(name + '=');
                if (i > -1) {
                    var j = cookie.
                            indexOf(';', i);
                    return escape(cookie.substring(i + name.length + 1, j < 0 ? cookie.
                            length : j))
                }
            }
        }

        var x = getCookieValue('__utmx'), xx = getCookieValue('__utmxx'), h = location.hash;
        document.write('<script src="http://www.google-analytics.com/ga_exp.js?utmxkey=' + key +
                '&utmx=' + (x ? x : '') + '&utmxx=' + (xx ? xx : '') + '&utmxtime=' + new Date().
                valueOf() + (h ? '&utmxhash=' + escape(h.substr(1)) : '') +
                '" type="text/javascript" charset="utf-8"><\/script>')
    });
    window.loadGoogleABTestScript();
    </script>
    <script>
    utmx('url', 'A/B');
    </script>
    <!-- End of Google Analytics Content Experiment code -->
</c:if>
<%@ include file="/WEB-INF/jsp/includes/generated/doctype-header.jsp" %>
<head>
    <c:choose>
        <c:when test="${not empty customMetaTag.title}">
            <title>${customMetaTag.title}</title>
        </c:when>
        <c:otherwise>
            <title>О проекте Planeta.ru</title>
        </c:otherwise>
    </c:choose>
    <c:choose>
        <c:when test="${not empty customMetaTag.description}">
            <meta name="description" content="${customMetaTag.description}"/>
        </c:when>
        <c:otherwise>
            <meta name="description"
                  content="Что такое онлайн-площадка Planeta.ru?"/>
        </c:otherwise>
    </c:choose>
    <c:if test="${not empty customMetaTag.keywords}">
        <meta name="keywords" content="${customMetaTag.keywords}"/>
    </c:if>

    <%@include file="/WEB-INF/jsp/includes/generated/stat-counters-async.jsp"%>
    <%@include file="/WEB-INF/jsp/includes/common-css.jsp" %>
    <%@include file="/WEB-INF/jsp/includes/generated/common-templates.jsp" %>
    <%@include file="/WEB-INF/jsp/includes/planeta-js.jsp" %>
</head>
<body>
<%@ include file="/WEB-INF/jsp/includes/generated/header.jsp" %>
<div class="container">
    <div class="content-alert-success mrg-l-30 mrg-r-30 mrg-t-20">
        <i class="three-types-alerts-success"></i>
        <p class="big">Вы отписались от всех уведомлений на почту.</p>
        <p>Вы можете изменить настройки своих уведомлени в <a href="https://${properties['application.host']}/settings/notifications">Настройках профиля.</a></p>
    </div>
    <div class="btn-toolbar"></div>
</div>
</body>
</html>

<%@ include file="/WEB-INF/jsp/includes/generated/doctype-header.jsp" %>
<head>
    <c:set var="pageType" value="music"/>
    <c:set var="eventLabel" value="quickmusic_get_recommendation"/>
    <%@ include file="/WEB-INF/jsp/static/quickstart-new/includes/quickstart-header.jsp" %>
</head>

<body>
    <div class="header">
        <a class="logo" href="https://planeta.ru/"></a>

        <a class="btn btn-primary header_create-btn" href="/funding-rules">Создать проект</a>
    </div>

    <div class="video-lead">
        <div class="wrap">
            <div class="video-lead_head">Хотите записать альбом?</div>
            <div class="video-lead_block cf">
                <div class="video-lead_video">
                    <div class="embed-video">
                        <iframe width="560" height="315" src="//www.youtube.com/embed/Uvz6YkojqVU" frameborder="0" allowfullscreen></iframe>
                    </div>
                </div>
                <div class="video-lead_cont">
                    <div class="video-lead_text">
                        Чтобы записать песню или полноформатный альбом,
                         Вам необходимы финансовые вложения. Не обязательно искать
                          инвесторов – Вам помогут будущие слушатели! Создайте
                          возможность для поклонников поддержать ваше творчество
                           еще до релиза!
                    </div>

                    <div class="video-lead_action-text">
                        Запишитесь на вебинар о краудфандинге
                        <br>
                        и получите бесплатное руководство
                        <br>
                        «Как записать альбом с помощью краудфандинга»
                    </div>

                    <div class="video-lead_action">
                        <div class="fieldset pln-dropdown js-open-error">
                            <input class="form-control js-get-book-email" type="text" placeholder="Введите ваш e-mail">
                            <div class="page-control_popup pln-d-popup">
                                <div class="pln-d-popup-tail"><div></div></div>
                                <div class="d-popup-cont">
                                    Вы ввели неверный адрес электронной почты
                                </div>
                            </div>
                        </div>

                        <div class="video-lead_action_btn pln-dropdown js-get-book-btn-cnt" >
                            <button class="btn btn-primary js-get-book-btn" onclick="getBook()">Получить!</button>
                            <div class="page-control_popup pln-d-popup">
                                <div class="pln-d-popup-tail"><div></div></div>
                                <nobr class="d-popup-cont">
                                    Вам отправлено письмо с руководством
                                    <br>
                                    «Как записать альбом с помощью краудфандинга»
                                </nobr>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="promo-lead">
        <div class="wrap">
            <div class="promo-lead_head">Краудфандинг доступен для всех</div>
            <div class="promo-lead_block cf">

                <div class="promo-lead_list">
                    <div class="promo-lead_i">
                        <div class="promo-lead_i_img">
                            <img src="//${hf:getStaticBaseUrl("")}/images/quickstart/promo-lead-img-1.png">
                        </div>
                        <div class="promo-lead_i_head">
                            Начинающим исполнителям
                        </div>
                        <div class="promo-lead_i_text">
                            &laquo;Планета&raquo;&nbsp;&mdash; это возможность найти новую аудиторию
                            и&nbsp;заинтересовать ее&nbsp;своим творчеством, а&nbsp;самое
                            главное&nbsp;&mdash;представить его. Именно с&nbsp;поддержкой новых слушателей вы&nbsp;сможете записать
                            дебютный альбом, устроить концерт или найти средства на&nbsp;покупку необходимых инструментов.
                        </div>
                    </div>
                    <div class="promo-lead_i">
                        <div class="promo-lead_i_img">
                            <img src="//${hf:getStaticBaseUrl("")}/images/quickstart/promo-lead-img-2.png">
                        </div>
                        <div class="promo-lead_i_head">
                            Музыкальным группам
                        </div>
                        <div class="promo-lead_i_text">
                            Найти аудиторию&nbsp;&mdash; это одно, а&nbsp;найти с&nbsp;ней общий язык гораздо сложней.
                            Именно у&nbsp;нас вы&nbsp;сможете более плотно пообщаться со&nbsp;своими слушателями
                            и&nbsp;вовлечь их&nbsp;в&nbsp;процесс творчества. Когда вы&nbsp;понимаете,
                            что нужно вашим поклонникам, успех неминуем&nbsp;&mdash; ведь вы&nbsp;говорите
                            с&nbsp;аудиторией на&nbsp;одном языке.
                        </div>
                    </div>
                    <div class="promo-lead_i">
                        <div class="promo-lead_i_img">
                            <img src="//${hf:getStaticBaseUrl("")}/images/quickstart/promo-lead-img-3.png">
                        </div>
                        <div class="promo-lead_i_head">
                            Настоящим
                            <br>
                            звёздам
                        </div>
                        <div class="promo-lead_i_text">
                            &laquo;Планета&raquo; дарит вам полную свободу для творчества и&nbsp;выражения
                            собственных идей. Вам не&nbsp;нужны посредники, чтобы радовать поклонников,
                            и&nbsp;не&nbsp;нужны продюсеры, которые вас учат, как ее&nbsp;писать.
                            Самые интересные эксперименты и&nbsp;музыкальные открытия происходят лишь
                            при полной независимости&nbsp;&mdash; и&nbsp;мы&nbsp;подарим ее&nbsp;вам!
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <%@ include file="/WEB-INF/jsp/static/quickstart-new/includes/quickstart-inner.jsp" %>
    <div class="author-slider">
        <div class="author-slider_head">Отзывы авторов проектов</div>

        <div class="author-slider_head-wrap">
            <div class="author-slider_head-list uninit">

                <div class="author-slider_head-i">
                    <div class="author-slider_head-name">
                        <div class="author-slider_head-name-text">
                            <span class="cur-p">Алексей Кортнев</span>
                        </div>
                    </div>
                    <div class="author-slider_head-ava"><img src="//${hf:getStaticBaseUrl("")}/images/quickstart/author/kortnev.png" width="144" height="144"></div>
                </div>

                <div class="author-slider_head-i">
                    <div class="author-slider_head-name">
                        <div class="author-slider_head-name-text">
                            <span class="cur-p">Александр Красовицкий</span>
                        </div>
                    </div>
                    <div class="author-slider_head-ava"><img src="//${hf:getStaticBaseUrl("")}/images/quickstart/author/krasovic.png" width="144" height="144"></div>
                </div>

                <div class="author-slider_head-i">
                    <div class="author-slider_head-name">
                        <div class="author-slider_head-name-text">
                            <span class="cur-p">Инна Желанная</span>
                        </div>
                    </div>
                    <div class="author-slider_head-ava"><img src="//${hf:getStaticBaseUrl("")}/images/quickstart/author/inna.jpg" width="144" height="144"></div>
                </div>

                <div class="author-slider_head-i">
                    <div class="author-slider_head-name">
                        <div class="author-slider_head-name-text">
                            <span class="cur-p">Алексей Кортнев</span>
                        </div>
                    </div>
                    <div class="author-slider_head-ava"><img src="//${hf:getStaticBaseUrl("")}/images/quickstart/author/kortnev.png" width="144" height="144"></div>
                </div>

                <div class="author-slider_head-i">
                    <div class="author-slider_head-name">
                        <div class="author-slider_head-name-text">
                            <span class="cur-p">Александр Красовицкий</span>
                        </div>
                    </div>
                    <div class="author-slider_head-ava"><img src="//${hf:getStaticBaseUrl("")}/images/quickstart/author/krasovic.png" width="144" height="144"></div>
                </div>

                <div class="author-slider_head-i">
                    <div class="author-slider_head-name">
                        <div class="author-slider_head-name-text">
                            <span class="cur-p">Инна Желанная</span>
                        </div>
                    </div>
                    <div class="author-slider_head-ava"><img src="//${hf:getStaticBaseUrl("")}/images/quickstart/author/inna.jpg" width="144" height="144"></div>
                </div>
            </div>
        </div>


        <div class="author-slider_text-wrap">
            <div class="author-slider_text">

                <div class="author-slider_text-i">
                    <div class="author-slider_text-project">
                        Алексей Кортнев, музыкант, лидер группы «Несчастный случай»
                    </div>
                    <div class="author-slider_text-text">
                        Краудфандинг прекрасно работает.
                        Те люди, которые вложили деньги в проект, волей-неволей
                        начинают заинтересовывать им все свое окружение.
                        Все начинает расти, как дерево.
                    </div>
                </div>

                <div class="author-slider_text-i">
                    <div class="author-slider_text-project">
                        Александр Красовицкий, музыкант, лидер группы «Animal Jazz»
                    </div>
                    <div class="author-slider_text-text">
                        «Я теперь стал реально фанатом идеи краудфандинга,
                        я почувствовал, что это работает. Это революция,
                        которая, судя по всему, потихоньку дойдет и до всех».
                    </div>
                </div>

                <div class="author-slider_text-i">
                    <div class="author-slider_text-project">
                        Инна Желанная, музыкант, лидер одноименной группы
                    </div>
                    <div class="author-slider_text-text">
                        Теперь мы знаем — будущее за краудфандингом!
                        Мы непременно будем продолжать пользоваться системой
                        краудфандинга, это очень интересный и полезный опыт сотрудничества.
                        А главное — это взаимовыгодный способ создавать музыку
                        и делиться ею с вами!
                    </div>
                </div>

                <div class="author-slider_text-i">
                    <div class="author-slider_text-project">
                        Алексей Кортнев, музыкант, лидер группы «Несчастный случай»
                    </div>
                    <div class="author-slider_text-text">
                        Краудфандинг прекрасно работает.
                        Те люди, которые вложили деньги в проект, волей-неволей
                        начинают заинтересовывать им все свое окружение.
                        Все начинает расти, как дерево.
                    </div>
                </div>

                <div class="author-slider_text-i">
                    <div class="author-slider_text-project">
                        Александр Красовицкий, музыкант, лидер группы «Animal Jazz»
                    </div>
                    <div class="author-slider_text-text">
                        «Я теперь стал реально фанатом идеи краудфандинга,
                        я почувствовал, что это работает. Это революция,
                        которая, судя по всему, потихоньку дойдет и до всех».
                    </div>
                </div>

                <div class="author-slider_text-i">
                    <div class="author-slider_text-project">
                        Инна Желанная, музыкант, лидер одноименной группы
                    </div>
                    <div class="author-slider_text-text">
                        Теперь мы знаем — будущее за краудфандингом!
                        Мы непременно будем продолжать пользоваться системой
                        краудфандинга, это очень интересный и полезный опыт сотрудничества.
                        А главное — это взаимовыгодный способ создавать музыку
                        и делиться ею с вами!
                    </div>
                </div>
            </div>
        </div>
    </div>
    <%@ include file="/WEB-INF/jsp/static/quickstart-new/includes/quickstart-request.jsp" %>

</body>
</html>
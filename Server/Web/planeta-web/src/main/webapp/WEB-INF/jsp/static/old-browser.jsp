<%@ include file="/WEB-INF/jsp/includes/generated/doctype-header.jsp" %>
<html><head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="robots" content="noindex">
    <title>Браузер больше не поддерживается</title>

    <style type="text/css">
        body{
            margin: 20px;
            padding: 0;
            font: 13px Arial, sans-serif;
            color: #000;
        }
        a{
            color: #26aedd;
        }
        .main{
            margin: 0 0 0 92px;
        }
        .info{
            font-size: 20px;
            line-height: 24px;
        }
        .browser-list{
            border: 0;
            border-collapse: collapse;
            border-spacing: 0;
        }
        .browser-list td{
            padding: 5px;
            vertical-align: top;
        }
        .browser-list img{
            border: 0;
        }
        .browser-list a{
            display: block;
            text-align: center;
            text-decoration: none;
        }
        .browser-list a:hover{
            text-decoration: underline;
        }
    </style>

</head>
<body>

    <div class="logo">
        <img src="//${hf:getStaticBaseUrl("")}/images/browsers/tr-logo.jpg" alt="planeta.ru">
    </div>


    <div class="main">
        <div class="info">
            <p>С прискорбием сообщаем, что Ваш браузер более не поддерживается на нашей планете.</p>
            <p>Мы рекомендуем Вам установить нечто более современное и надежное:</p>
        </div>

        <table class="browser-list">
            <tbody>
            <tr>
                <td width="104">
                    <a href="http://www.google.ru/chrome">
                        <span class="cover"><img src="//${hf:getStaticBaseUrl("")}/images/browsers/chrome.jpg" alt="chrome.jpg"></span>
                        <span class="name">Google Chrome</span>
                    </a>
                </td>
                <td width="104">
                    <a href="http://www.mozilla.org/ru/firefox/new/">
                        <span class="cover"><img src="//${hf:getStaticBaseUrl("")}/images/browsers/firefox.jpg" alt="firefox.jpg"></span>
                        <span class="name">Mozilla Firefox</span>
                    </a>
                </td>
                <td width="104">
                    <a href="http://www.apple.com/ru/safari/download/">
                        <span class="cover"><img src="//${hf:getStaticBaseUrl("")}/images/browsers/safari.jpg" alt="safari.jpg"></span>
                        <span class="name">Safari</span>
                    </a>
                </td>
                <td width="104">
                    <a href="http://ru.opera.com/download/">
                        <span class="cover"><img src="//${hf:getStaticBaseUrl("")}/images/browsers/opera.jpg" alt="opera.jpg"></span>
                        <span class="name">Opera</span>
                    </a>
                </td>
                <td width="104">
                    <a href="http://windows.microsoft.com/ru-RU/internet-explorer/downloads/ie">
                        <span class="cover"><img src="//${hf:getStaticBaseUrl("")}/images/browsers/ie.jpg" alt="ie.jpg"></span>
                        <span class="name">Internet Explorer</span>
                    </a>
                </td>
            </tr>
            </tbody>
        </table>
        <p>Впрочем, Вы можете посетить наш сайт на свой страх и риск. Для этого перейдите по <a href="${refererUrl}">ссылке</a></p>
    </div>


</body></html>
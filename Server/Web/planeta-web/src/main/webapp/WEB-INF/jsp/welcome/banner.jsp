<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ include file="/WEB-INF/jsp/includes/taglibs.jsp" %>
<c:if test="${not empty banner}">
    <div class="js-banner-place">
        <c:choose>
            <c:when test="${empty banner.html}">
                <c:choose>
                    <c:when test="${isMobile}">
                        <c:if test="${not empty banner.mobileImageUrl}">
                            <a href="${banner.refererUrl}"<c:if test="${banner.target}"> target="_blank" </c:if>
                               rel="nofollow noopener">
                                <img src='${banner.mobileImageUrl}' alt='${banner.name}' style="display:block;margin-left:auto;margin-right:auto;">
                            </a>
                        </c:if>
                    </c:when>
                    <c:otherwise>
                        <c:if test="${not empty banner.imageUrl}">
                            <a href="${banner.refererUrl}"<c:if test="${banner.target}"> target="_blank" </c:if>
                               rel="nofollow noopener">
                                <img src='${banner.imageUrl}' alt='${banner.name}' style="display:block;margin-left:auto;margin-right:auto;">
                            </a>
                        </c:if>
                    </c:otherwise>
                </c:choose>
            </c:when>
            <c:otherwise>
                ${banner.html}
            </c:otherwise>
        </c:choose>
        <input type="hidden" class="js-hidden-banner-id hidden" value="${banner.bannerId}">
    </div>
</c:if>

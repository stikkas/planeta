<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ include file="/WEB-INF/jsp/includes/generated/doctype-header.jsp" %>
<head>
    <title>Школа проектов Национальной Предпринимательской Сети в рамках «Время действовать»</title>

    <meta property="og:title" content="Школа проектов Национальной Предпринимательской Сети в рамках «Время действовать»">
    <meta name="description" content="Краудфандинг — отличная возможность помочь реализовать нашу миссию. В 5 мастер-классах ты получишь все для реализации проекта с помощью массового сбора средств.">
    <meta property="og:description" content="Краудфандинг — отличная возможность помочь реализовать нашу миссию. В 5 мастер-классах ты получишь все для реализации проекта с помощью массового сбора средств.">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta property="og:image" content="https://${hf:getStaticBaseUrl("")}/images/promo/nps/sharing.jpg">

    <%@include file="/WEB-INF/jsp/includes/generated/common-favicon.jsp" %>
    <link type="text/css" rel="stylesheet" href="//${hf:getStaticBaseUrl("")}/css-generated/nps.css" />
    <%@include file="/WEB-INF/jsp/includes/generated/stat-counters-head.jsp" %>
    <%@include file="/WEB-INF/jsp/includes/planeta-js.jsp" %>


    <p:script src="nps.js" />

    <script>
        $(document).ready(function () {

            var projectsContainer = new Promo.Views.CampaignListContainer({
                el: '.js-project-container',
                template: '#nps-project-container-template',
                model: new Promo.Models.Filter({
                    activeFilter: 0,
                    category: 'TIMEFORACTION',
                    limit: 50,
                    filters: [{
                        mnemonic: 'Новые',
                        status: 'NEW'
                    }, {
                        mnemonic: 'Популярные',
                        status: 'ACTIVE'
                    }],
                    carousel: {
                        itemsInRow: 5
                    },
                    isEmpty: true
                })
            });
            projectsContainer.render();


            // accordeon
            $('.workshops_i').each(function() {
                var self = this;
                var head = $('.workshops_h', self);
                var cont = $('.workshops_cont', self);

                head.on('click', function () {
                    var isSelfActive = $(self).hasClass('active');
                    $('.workshops_i.active .workshops_cont').slideUp(300);
                    $('.workshops_i.active').removeClass('active');

                    if (!isSelfActive) {
                        $(self).addClass('active');
                        cont.slideDown(300);
                    }
                });
            });



            // arcs draw
            var dateToEnd = new Date(2017, 1 - 1, 26);
            var arcOptions = {
                procent: 0,
                radius: 45,
                fill: '#fff',
                strokeWidth: 5,
                strokeBgColor: '#d8e0eb',
                strokeColor: '#ffcf0d'
            };

            var arcDays = $('.to-end-countdown_days .to-end-countdown_bar');
            arcDays.arcDraw(arcOptions);

            $('.to-end-countdown_days .to-end-countdown_count').countdown({
                until: dateToEnd,
                format: 'dHMS',
                layout: '\
                                {d<}\
                                    <div class="to-end-countdown_val">{dn}</div>\
                                    <div class="to-end-countdown_lbl">{dl}</div>\
                                {d>}',
                onTick: function(periods) {
                    var val = 100 - (periods[4] * 100 / 24);
                    arcDays.arcDraw('set', val);
                }
            });


            var arcHours = $('.to-end-countdown_hours .to-end-countdown_bar');
            arcHours.arcDraw(arcOptions);

            $('.to-end-countdown_hours .to-end-countdown_count').countdown({
                until: dateToEnd,
                format: 'dHMS',
                layout: '\
                                {h<}\
                                    <div class="to-end-countdown_val">{hn}</div>\
                                    <div class="to-end-countdown_lbl">{hl}</div>\
                                {h>}',
                onTick: function(periods) {
                    var val = 100 - (periods[5] * 100 / 60);
                    arcHours.arcDraw('set', val);
                }
            });


            var arcMinutes = $('.to-end-countdown_minutes .to-end-countdown_bar');
            arcMinutes.arcDraw(arcOptions);

            $('.to-end-countdown_minutes .to-end-countdown_count').countdown({
                until: dateToEnd,
                format: 'dHMS',
                layout: '\
                                {m<}\
                                    <div class="to-end-countdown_val">{mn}</div>\
                                    <div class="to-end-countdown_lbl">{ml}</div>\
                                {m>}',
                onTick: function(periods) {
                    var val = 100 - (periods[6] * 100 / 60);
                    arcMinutes.arcDraw('set', val);
                }
            });


            // single page nav
            $('.menu_list').singlePageNav({
                currentClass: 'active',
                updateHash: true
            });



            // sharing
            var uri = window.location.href;
            var sharesRedraw = function () {
                $('.js-share-container').empty();
                $('.js-share-container').share({
                    className: 'sharing-popup-social donate-sharing sharing-mini',
                    counterEnabled: false,
                    hidden: false,
                    parseMetaTags: true,
                    url: uri
                });
            };

            $(window).bind('resize', sharesRedraw);
            sharesRedraw();
        });
    </script>

</head>

<body>
<div class="header">
    <div class="wrap">
        <div class="col-12">

            <a href="https://${properties['application.host']}/" class="logo"></a>

            <a href="https://${properties['application.host']}/welcome/nps.html" class="nps-school-logo"></a>

            <a href="http://npsglobal.ru/" target="_blank" class="nps-logo" rel="nofollow noopener"></a>

            <a href="http://timeforaction.pro/" target="_blank" class="vremya-logo" rel="nofollow noopener"></a>

        </div>
    </div>
</div>





<div class="menu">
    <div class="wrap">
        <div class="col-12">

            <div class="menu_list">
                <!--
                <div class="menu_i">
                    <a href="#projects" class="menu_link">Проекты</a>
                </div>
                -->
                <div class="menu_i">
                    <a href="#what-crowd" class="menu_link">Краудфандинг</a>
                </div>
                <div class="menu_i">
                    <a href="#contest" class="menu_link">Конкурс</a>
                </div>
                <div class="menu_i">
                    <a href="#prizes" class="menu_link">Победителям</a>
                </div>
                <div class="menu_i">
                    <a href="#workshops" class="menu_link">Мастер-классы</a>
                </div>
            </div>

        </div>
    </div>
</div>





<div class="lead">
    <div class="lead_cont wrap">
        <div class="col-12">

            <div class="lead_head">
                Школа проектов Национальной Предпринимательской Сети в&nbsp;рамках &laquo;Время действовать&raquo;
            </div>


            <div class="lead_text">
                <div class="wrap-row">
                    <div class="col-6">
                        <div class="lead_text-i">
                            Миссия НПС&nbsp;&mdash; раскрыть предпринимательский потенциал каждого.
                            <br>
                            <br>
                            Краудфандинг&nbsp;&mdash; отличная возможность помочь реализовать нашу миссию.
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="lead_text-i">
                            В&nbsp;<a href="#workshops">5&nbsp;<nobr>мастер-классах</nobr></a> ты&nbsp;получишь все для реализации проекта с&nbsp;помощью массового сбора средств.
                            <br>
                            <br>
                            А&nbsp;победителей <a href="#contest">конкурса</a> ждет <a href="#prizes">бережная поддержка</a> в&nbsp;реализации проекта.
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>



<div class="js-project-container">
</div>



<div class="what-crowd" id="what-crowd">
    <div class="wrap">
        <div class="col-12">

            <div class="what-crowd_head">
                Что такое краудфандинг?
            </div>


            <div class="what-crowd_list wrap-row">
                <div class="what-crowd_i col-3">
                    <div class="what-crowd_ico">
                        <span class="s-what-crowd-idea"></span>
                    </div>

                    <div class="what-crowd_name">
                        Есть идея
                    </div>

                    <div class="what-crowd_descr">
                        У&nbsp;каждого рождаются мысли о&nbsp;том, как сделать мир лучше. Но&nbsp;зачастую они остаются в&nbsp;голове или на&nbsp;бумаге&nbsp;&mdash; на&nbsp;реализацию не&nbsp;хватает средств
                    </div>
                </div>


                <div class="what-crowd_i col-3">
                    <div class="what-crowd_ico">
                        <span class="s-what-crowd-project"></span>
                    </div>

                    <div class="what-crowd_name">
                        Размести проект
                    </div>

                    <div class="what-crowd_descr">
                        Краудфандинг упрощает переход от&nbsp;идеи к&nbsp;действию. На&nbsp;платформе Planeta.ru сделано все, чтобы ты&nbsp;удобно разместил проект для массового привлечения средств
                    </div>
                </div>


                <div class="what-crowd_i col-3">
                    <div class="what-crowd_ico">
                        <span class="s-what-crowd-promo"></span>
                    </div>

                    <div class="what-crowd_name">
                        Продвигай его
                    </div>

                    <div class="what-crowd_descr">
                        Эффективное продвижение&nbsp;&mdash; залог успеха кампании по&nbsp;финансированию проекта. НПС поможет тебе сделать так, чтобы заинтересованные узнали о&nbsp;существовании твоего проекта
                    </div>
                </div>


                <div class="what-crowd_i col-3">
                    <div class="what-crowd_ico">
                        <span class="s-what-crowd-finance"></span>
                    </div>

                    <div class="what-crowd_name">
                        Получи финансирование
                    </div>

                    <div class="what-crowd_descr">
                        Получение инвестиций&nbsp;&mdash; не&nbsp;конец, а&nbsp;только начало твоего проекта, подтверждение, что он&nbsp;нужен.
                        <br>
                        <a href="#prizes">Лучшим НПС поможет в&nbsp;реализации</a>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>




<div class="contest" id="contest">
    <div class="wrap">
        <div class="col-12">

            <div class="contest_head">
                Конкурс
            </div>

            <div class="lead_text">
            25 января завершился конкурс Школа Проектов «Время действовать»! В рамках него у каждого прослушавшего курс
            по краудфандингу была возможность разместить свой проект на Planeta.ru и собрать «народное финансирование»
            для реализации проекта. Победителем стал проект <a href="https://planeta.ru/campaigns/chainchallengefest">«Chain Challenge Fest»</a>
            - фестиваль уличной культуры. Команда победителя получила
            персонального ментора, а также получит информационную поддержку от НПС и возможность выступить на съезде
            лидеров клубов НПС 16 февраля в рамках «Время действовать».
            </div>
        </div>
    </div>
</div>




<div class="prizes" id="prizes">
    <div class="wrap">
        <div class="col-12">

            <div class="prizes_head">
                Победитель  получает
            </div>


            <div class="prizes_list wrap-row">
                <div class="prizes_i col-3">
                    <div class="prizes_ico">
                        <span class="s-prizes-teacher"></span>
                    </div>
                    <div class="prizes_name">
                        Персонального ментора для твоего проекта
                    </div>
                </div>

                <div class="prizes_i col-3">
                    <div class="prizes_ico">
                        <span class="s-prizes-tv"></span>
                    </div>
                    <div class="prizes_name">
                        Участие в&nbsp; трансляции &laquo;Время Действовать&raquo;
                    </div>
                    <div class="prizes_add">
                        в&nbsp;качестве гостя, совместное выступление со&nbsp;спикером в феврале
                    </div>
                </div>

                <div class="prizes_i col-3">
                    <div class="prizes_ico">
                        <span class="s-prizes-speech"></span>
                    </div>
                    <div class="prizes_name">
                        Выступление на&nbsp;всероссийском съезде предпринимательских сообществ
                    </div>
                    <div class="prizes_add">
                        в Москве в феврале
                    </div>
                </div>

                <div class="prizes_i col-3">
                    <div class="prizes_ico">
                        <span class="s-prizes-media"></span>
                    </div>
                    <div class="prizes_name">
                        Освещение проекта в&nbsp;СМИ
                    </div>
                    <div class="prizes_add">
                        продвижение с&nbsp;помощью всех маркетинговых ресурсов&nbsp;НПС
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>


<div class="video-list" id="workshops">
    <div class="wrap">
        <div class="col-12">

            <div class="video-list_list wrap-row">
                <div class="video-list_i col-4">
                    <div class="embed-video">
                        <iframe width="320" height="200" data-src="https://www.youtube.com/embed/Iye4CVvtvFs" frameborder="0" allowfullscreen=""></iframe>
                    </div>
                </div>

                <div class="video-list_i col-4">
                    <div class="embed-video">
                        <iframe width="320" height="200" data-src="https://www.youtube.com/embed/7RBVTNay4Zo" frameborder="0" allowfullscreen=""></iframe>
                    </div>
                </div>

                <div class="video-list_i col-4">
                    <div class="embed-video">
                        <iframe width="320" height="200" data-src="https://www.youtube.com/embed/ege_JJnMvmM" frameborder="0" allowfullscreen=""></iframe>
                    </div>
                </div>

                <div class="video-list_i col-4">
                    <div class="embed-video">
                        <iframe width="320" height="200" data-src="https://www.youtube.com/embed/4yX1jQzUqGQ" frameborder="0" allowfullscreen=""></iframe>
                    </div>
                </div>

                <div class="video-list_i col-4">
                    <div class="embed-video">
                        <iframe width="320" height="200" data-src="https://www.youtube.com/embed/66awaD-etAg" frameborder="0" allowfullscreen=""></iframe>
                    </div>
                </div>

                <div class="video-list_i col-4">
                    <div class="embed-video">
                        <iframe width="320" height="200" data-src="https://www.youtube.com/embed/wurEqVWPhRw" frameborder="0" allowfullscreen=""></iframe>
                    </div>
                </div>

                <div class="video-list_i col-4">
                    <div class="embed-video">
                        <iframe width="320" height="200" data-src="https://www.youtube.com/embed/3RG_z47BvXY" frameborder="0" allowfullscreen=""></iframe>
                    </div>
                </div>

                <div class="video-list_i col-4">
                    <div class="embed-video">
                        <iframe width="320" height="200" data-src="https://www.youtube.com/embed/mF6DxoawMaM" frameborder="0" allowfullscreen=""></iframe>
                    </div>
                </div>
            </div>
        </div>


        </div>
    </div>
</div>


<div class="footer">
    <div class="wrap">
        <div class="col-12">

            <div class="footer_sharing">
                <div class="footer_sharing-head">
                    поделись с друзьями
                </div>
                <div class="footer_sharing-cont js-share-container">
                </div>
            </div>


            <div class="footer_social">
                <a href="https://vk.com/npsonline" class="footer_social-link">
                    <span class="s-social-vk"></span>
                </a>
                <a href="https://www.facebook.com/nps.rybakovfond/?fref=ts" class="footer_social-link">
                    <span class="s-social-fb"></span>
                </a>
                <a href="https://telegram.me/eventsnps" class="footer_social-link">
                    <span class="s-social-tg"></span>
                </a>
            </div>


            <div class="footer_develop">
                <div class="footer_develop-head">
                    Разработано
                </div>
                <div class="footer_develop-logo">
                    <a href="https://planeta.ru/" class="logo"></a>
                </div>
            </div>

        </div>
    </div>
</div>


</body>
</html>
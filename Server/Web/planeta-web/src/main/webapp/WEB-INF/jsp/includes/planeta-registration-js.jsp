<%@ include file="/WEB-INF/jsp/includes/taglibs.jsp" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<%@ include file="/WEB-INF/jsp/includes/generated/planeta-js-init.jsp" %>
<script type="text/javascript" src="//${hf:getStaticBaseUrl("/res")}/js/templates.js?flushCache=${properties['static.flushCache']}"></script>
<script type="text/javascript" src="//${hf:getStaticBaseUrl("/res")}/js/planeta-vue.js?compress=${properties['static.compress']}&flushCache=${properties['static.flushCache']}"></script>

<script type="text/javascript">
    var workspace;
    // TODO need this file?
    $(document).ready(function() {
        workspace = Planeta.init({
            myProfile: workspaceInitParameters.myProfile,
            appModel: {
                constructor: AppModel,
                profileConstructor: BaseProfileModel,
                profile: ${hf:toJson(myProfile)}
            },
            configuration: workspaceInitParameters.configuration,
            routing: {
                router: PlanetaRouter
            }
        });
    });
</script>
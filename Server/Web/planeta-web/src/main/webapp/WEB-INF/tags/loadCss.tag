<%@ include file="/WEB-INF/jsp/includes/taglibs.jsp" %>
<%@attribute name="cssName" required="true" %>
<link type="text/css" rel="stylesheet" href="//${hf:getStaticBaseUrl("")}/css-generated/${cssName}.css" />

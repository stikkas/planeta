<%@ include file="/WEB-INF/jsp/includes/taglibs.jsp" %>
<%@attribute name="moduleName" required="true" %>

<script type="text/javascript" src="//${hf:getStaticBaseUrl("/res")}/js/${moduleName}.js?compress=${properties['static.compress']}&flushCache=${properties['static.flushCache']}"></script>
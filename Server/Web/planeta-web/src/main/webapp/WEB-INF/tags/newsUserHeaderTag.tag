<%@ tag language="java" %>
<%@ include file="/WEB-INF/jsp/includes/taglibs.jsp" %>

<%@attribute name="profileId" %>
<%@attribute name="timeAdded" type="java.util.Date"%>
<%@attribute name="firstName" %>
<%@attribute name="nickname" %>
<%@attribute name="lastName" %>

<fmt:formatDate pattern="HH:mm dd.MM.yyyy" value="${timeAdded}"/> <a href="/id${profileId}"> ${firstName} ${nickname} ${lastName}</a>
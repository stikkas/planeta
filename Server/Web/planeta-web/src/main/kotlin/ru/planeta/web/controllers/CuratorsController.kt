package ru.planeta.web.controllers

import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController
import ru.planeta.api.model.promo.PartnersPromoConfiguration
import ru.planeta.api.web.controllers.Urls.WELCOME_GET_CURATORS
import ru.planeta.api.web.controllers.Urls.WELCOME_GET_PARTNERS
import ru.planeta.web.services.CuratorsService

@RestController
class CuratorsController(private val curatorsService: CuratorsService) {

    @GetMapping(WELCOME_GET_PARTNERS)
    fun getPartners(): List<PartnersPromoConfiguration> {
        return curatorsService.welcomePartners
    }

    @GetMapping(WELCOME_GET_CURATORS)
    fun getCurators(): List<PartnersPromoConfiguration> {
        return curatorsService.welcomeCurators
    }
}

package ru.planeta.web.controllers.welcome

import org.springframework.beans.factory.annotation.Value
import org.springframework.context.i18n.LocaleContextHolder
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.ResponseBody
import org.springframework.web.servlet.ModelAndView
import ru.planeta.api.model.ActionStatus
import ru.planeta.api.model.campaign.PromoCampaignDTO
import ru.planeta.api.model.promo.PartnersPromoConfiguration
import ru.planeta.api.search.SearchCampaigns
import ru.planeta.api.search.SearchService
import ru.planeta.api.search.SearchShares
import ru.planeta.api.search.SortOrder
import ru.planeta.api.service.campaign.CampaignService
import ru.planeta.api.service.common.StatsService
import ru.planeta.api.service.configurations.ConfigurationService
import ru.planeta.api.service.notifications.NotificationService
import ru.planeta.api.service.profile.ProfileService
import ru.planeta.api.service.profile.UserService
import ru.planeta.api.service.shop.ProductUsersService
import ru.planeta.api.utils.ValidateUtils
import ru.planeta.api.web.authentication.myProfileId
import ru.planeta.model.common.WelcomeStats
import ru.planeta.model.common.campaign.CampaignTag
import ru.planeta.model.common.campaign.ShareForSearch
import ru.planeta.model.common.campaign.enums.CampaignStatus
import ru.planeta.web.controllers.Actions
import ru.planeta.web.controllers.Urls
import ru.planeta.web.controllers.services.MainBaseControllerService
import java.util.*
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse
import kotlin.collections.ArrayList

@Controller
class WelcomeController(private val productUsersService: ProductUsersService,
                        private val notificationService: NotificationService,
                        private val statsService: StatsService,
                        private val campaignService: CampaignService,
                        private val configurationService: ConfigurationService,
                        private val searchService: SearchService,
                        private val userService: UserService,
                        private val profileService: ProfileService,
                        private val mainBaseControllerService: MainBaseControllerService) {

    @Value("\${widgets.application.host}")
    lateinit var widgetsHost: String

    @GetMapping(Urls.AGREEMENT)
    fun agreement(): ModelAndView = createDefaultStaticModelAndView(Actions.AGREEMENT)

    @GetMapping(Urls.SHARE_RETURN)
    fun shareReturn(): ModelAndView = createDefaultStaticModelAndView(Actions.SHARE_RETURN)

    @GetMapping(Urls.PROJECTS_AGREEMENT)
    fun projectsAgreement(): ModelAndView = createDefaultStaticModelAndView(Actions.PROJECTS_AGREEMENT)

    @GetMapping(Urls.PRIVATE_INFO)
    fun privateInfo(): ModelAndView = createDefaultStaticModelAndView(Actions.PRIVATE_INFO)

    @GetMapping(Urls.ABOUT_NEW_CONTACTS)
    fun aboutNewContacts(): ModelAndView = createDefaultStaticModelAndView(Actions.ABOUT_NEW_CONTACTS)

    @GetMapping(Urls.ABOUT_NEW_ADVERTISING)
    fun aboutNewAdvertising(): ModelAndView = createDefaultStaticModelAndView(Actions.ABOUT_NEW_ADVERTISING)

    @GetMapping(Urls.ABOUT_NEW_LOGO)
    fun aboutNewLogo(): ModelAndView = createDefaultStaticModelAndView(Actions.ABOUT_NEW_LOGO)

    @GetMapping(Urls.ABOUT_NEW_PARTNERS)
    fun aboutNewPartners(): ModelAndView = createDefaultStaticModelAndView(Actions.ABOUT_NEW_PARTNERS)
            .addObject("partnersPromoConfigurations",
                    configurationService.getJsonArrayConfig(PartnersPromoConfiguration::class.java,
                            "planeta.partners.promo.configuration.list"))

    @GetMapping(Urls.PROMOTION_1)
    fun promotion1(): ModelAndView = createDefaultStaticModelAndView(Actions.PROMOTION_1)

    @GetMapping(Urls.PROMOTION_2)
    fun promotion2(): ModelAndView = createDefaultStaticModelAndView(Actions.PROMOTION_2)

    @GetMapping(Urls.PROMOTION_3)
    fun promotion3(): ModelAndView = createDefaultStaticModelAndView(Actions.PROMOTION_3)

    @GetMapping(Urls.PROMOTION_4)
    fun promotion4(): ModelAndView = createDefaultStaticModelAndView(Actions.PROMOTION_4)

    @GetMapping(Urls.PROMOTION_PACK)
    fun promotionPack(): ModelAndView = createDefaultStaticModelAndView(Actions.PROMOTION_PACK)

    @GetMapping(Urls.UNIVERSITY)
    fun finUniverse(): ModelAndView {
        val tag = campaignService.getCampaignTagByMnemonic("UNIVERSITY_PLANETA")
        val searchCampaigns = SearchCampaigns()
        searchCampaigns.limit = 3
        searchCampaigns.campaignTags = mutableListOf(tag ?: CampaignTag())
        searchCampaigns.campaignStatus = EnumSet.of(CampaignStatus.ACTIVE, CampaignStatus.FINISHED)
        searchCampaigns.sortOrderList = listOf(SortOrder.SORT_BY_COLLECTION_RATE)
        val finuniverse = searchService.searchForCampaigns(searchCampaigns).searchResultRecords

        return createDefaultStaticModelAndView(Actions.FINUNIVERSE)
                .addAllObjects(mapOf("showMore" to true,
                        "campaigns" to finuniverse?.map { PromoCampaignDTO.createFromCampaign(it) }))
    }

    @GetMapping(Urls.BIRTHDAY)
    fun fourYears(): ModelAndView = createDefaultStaticModelAndView(Actions.BIRTHDAY)

    @GetMapping(Urls.ABOUT_NEW)
    fun getAboutNew(request: HttpServletRequest, response: HttpServletResponse): ModelAndView {
        val modelAndView = createDefaultStaticModelAndView(Actions.ABOUT_NEW)
        val totalCollectedAmount = campaignService.totalCollectedAmount
        val successfulCampaignsCount = campaignService.successfulCampaignsCount
        modelAndView.addAllObjects(mapOf("campaignsTotalStats" to totalCollectedAmount,
                "campaignsTotalStatsHuman" to ru.planeta.commons.lang.StringUtils.humanNumber(totalCollectedAmount.toLong().toString()),
                "successfulCampaignsCount" to successfulCampaignsCount,
                "successfulCampaignsCountHuman" to ru.planeta.commons.lang.StringUtils.humanNumber(successfulCampaignsCount.toString()),
                "shopCurrentProductsCount" to productUsersService.currentProductsCount,
                "shopCurrentProductsCountHuman" to ru.planeta.commons.lang.StringUtils.humanNumber(productUsersService.currentProductsCount.toString()),
                "usersCount" to userService.usersCount,
                "usersCountHuman" to ru.planeta.commons.lang.StringUtils.humanNumber(userService.usersCount.toString()),
                "campaignsTagsPercentages" to campaignService.campaignsCountPercentagesByCustomTags))

        val promoAboutUsCampaignIds = configurationService.promoAboutUsCampaignIds
        if (promoAboutUsCampaignIds != null && !promoAboutUsCampaignIds.isEmpty()) {
            val campaigns = campaignService.getCampaigns(promoAboutUsCampaignIds)
            val promoAboutUsCampaignListOfMaps = ArrayList<Map<String, String>>(campaigns.size)
            for (campaign in campaigns) {
                val collectedAmount = campaign.collectedAmount.toInt().toString()
                val profile = profileService.getProfile(campaign.creatorProfileId)

                val map = mutableMapOf<String, String>("imageUrl" to (campaign.imageUrl ?: ""),
                        "collectedAmount" to collectedAmount,
                        "collectedAmountHuman" to ru.planeta.commons.lang.StringUtils.humanNumber(collectedAmount),
                        "name" to (campaign.name ?: ""),
                        "creatorDisplayName" to if (profile != null) (profile.displayName ?: "") else "")
                if ("en" == LocaleContextHolder.getLocale().language) {
                    map["mainTag"] = campaign.mainTag?.engName ?: ""
                } else {
                    map["mainTag"] = campaign.mainTag?.name ?: ""
                }
                promoAboutUsCampaignListOfMaps.add(map)
            }
            modelAndView.addObject("promoAboutUsCampaignListOfMaps", promoAboutUsCampaignListOfMaps)
        }
        return modelAndView
    }

    @ResponseBody
    @PostMapping(Urls.ABOUT_NEW_ADVERTISING_SEND_MAIL)
    fun getAboutNewAdvertisingSendMail(@RequestParam mailFrom: String,
                                       @RequestParam message: String): ActionStatus<*> {
        val fieldErrors = HashMap<String, String>()
        if (!ValidateUtils.isValidEmail(mailFrom)) {
            fieldErrors.put("email", "Wrong email")
        }
        if (message.isEmpty()) {
            fieldErrors.put("message", "Wrong message")
        }
        if (fieldErrors.size > 0) {
            val actionStatus = ActionStatus<Any>()
            actionStatus.fieldErrors = fieldErrors
            actionStatus.isSuccess = false
            actionStatus.errorMessage = message
            return actionStatus
        }
        notificationService.sendMailFromAboutUsAdvertising(mailFrom, message)
        return ActionStatus.createSuccessStatus<Any>()
    }

    /**
     * Creates default model and view for static prefix actions
     */
    private fun createDefaultStaticModelAndView(action: Actions): ModelAndView = mainBaseControllerService.defaultModelAndView(action.text, action.actionName)

    @GetMapping(Urls.SCHOOL_CERT_RENDER)
    fun getSchoolCertRender(@RequestParam profileId: Long): ModelAndView = mainBaseControllerService.defaultModelAndView(Actions.SCHOOL_CERT_RENDER)
            .addObject("displayName", profileService.getProfileSafe(profileId).displayName)

    @GetMapping(Urls.SCHOOL_CERT)
    fun getSchoolCert(@RequestParam profileId: Long): ModelAndView = mainBaseControllerService.defaultModelAndView(Actions.SCHOOL_CERT)
            .addAllObjects(mapOf("displayName" to profileService.getProfileSafe(profileId).displayName,
                    "myProfileId" to myProfileId(),
                    "certProfileId" to profileId,
                    "widgetsHost" to widgetsHost))

    @ResponseBody
    @GetMapping(Urls.Welcome.SHARES_LIST)
    fun getSharesList(): ActionStatus<List<ShareForSearch>> {
        val result = searchService.searchSharesForWelcome(SearchShares(arrayListOf(SortOrder.SORT_BY_PURCHASE_DATE_DESC), 1000, 2000, true, 4))
        return ActionStatus.createSuccessStatus(result)
    }

    @ResponseBody
    @GetMapping(Urls.Welcome.MAIN_SLIDER_STATS)
    fun getMainSliderStats(): WelcomeStats = statsService.selectWelcomeStats()

}


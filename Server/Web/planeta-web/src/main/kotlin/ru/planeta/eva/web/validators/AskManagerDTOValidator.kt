package ru.planeta.eva.web.validators

import org.springframework.stereotype.Component
import org.springframework.validation.Errors
import org.springframework.validation.ValidationUtils
import org.springframework.validation.Validator
import ru.planeta.api.utils.ValidateUtils
import ru.planeta.eva.web.dto.AskManagerDTO

@Component
class AskManagerDTOValidator : Validator {

    override fun supports(clazz: Class<*>): Boolean {
        return AskManagerDTO::class.java.isAssignableFrom(clazz)
    }

    override fun validate(target: Any, errors: Errors) {
        val askManagerDTO = target as AskManagerDTO
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "message", "errors.field_required")
        if (askManagerDTO.campaignId == null) {
            errors.rejectValue("campaignId", "errors.campaignId-required")
        }
        askManagerDTO.email?.let {
            if (!ValidateUtils.isValidEmail(it)) {
                errors.rejectValue("email", "errors.wrong-email")
            }
        }
    }
}


package ru.planeta.web.controllers.campaigns.model

import javax.xml.bind.annotation.XmlRootElement

@XmlRootElement
class Projects {
    // named so for good xml
    var project: List<CampaignInfo>? = null
}

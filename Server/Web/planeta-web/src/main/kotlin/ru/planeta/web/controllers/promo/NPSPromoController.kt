package ru.planeta.web.controllers.promo

import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.servlet.ModelAndView
import ru.planeta.web.controllers.Actions
import ru.planeta.web.controllers.Urls
import ru.planeta.web.controllers.services.MainBaseControllerService

@Controller
class NPSPromoController(private val mainBaseControllerService: MainBaseControllerService) {

    @GetMapping(Urls.NPS)
    fun npsPromo(): ModelAndView = mainBaseControllerService.defaultModelAndView(Actions.NPS)
}

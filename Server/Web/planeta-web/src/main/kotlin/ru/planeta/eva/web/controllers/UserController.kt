package ru.planeta.eva.web.controllers

import org.springframework.validation.BindingResult
import org.springframework.web.bind.annotation.*
import ru.planeta.api.news.LoggerService
import ru.planeta.api.service.billing.order.MoneyTransactionService
import ru.planeta.api.service.notifications.NotificationService
import ru.planeta.api.web.authentication.getAuthentication
import ru.planeta.api.web.authentication.myProfileId
import ru.planeta.api.web.authentication.updateCurrentProfile
import ru.planeta.api.web.controllers.services.ControllerAutoLoginWrapService
import ru.planeta.eva.api.models.ChangePasswordDTO
import ru.planeta.eva.api.models.ProfileMainSettingsDTO
import ru.planeta.eva.api.services.AuthorizationService
import ru.planeta.eva.api.services.UserProfileService
import ru.planeta.eva.api.web.ActionStatus
import ru.planeta.eva.web.dto.SubscriptionsDTO
import ru.planeta.model.news.ProfileNews
import javax.validation.Valid

@RestController
class UserController(private val profileNewsService: LoggerService,
                     private val authorizationService: AuthorizationService,
                     private val controllerAutoLoginWrapService: ControllerAutoLoginWrapService,
                     private val userProfileService: UserProfileService,
                     private val moneyTransactionService: MoneyTransactionService,
                     private val notificationService: NotificationService) {

    companion object {
        const val DEFAULT_LIMIT = 20
        const val DEFAULT_OFFSET = 0
    }

    /**
     * Сохраняет новый пароль пользователя
     */
    @PostMapping(ProfileUrls.UPDATE_PASSWORD)
    fun updatePassword(@Valid @RequestBody change: ChangePasswordDTO, errors: BindingResult): ActionStatus {
        if (errors.hasErrors()) {
            return ActionStatus(errors)
        }

        val myProfileId = myProfileId()
        val info = authorizationService.saveUserPassword(myProfileId, change, myProfileId)

        //update authentication token
        val authentication = getAuthentication()
        if (authentication != null) {
            controllerAutoLoginWrapService.updateAuthenticationToken(
                    authentication.principal,
                    info.password ?: "",
                    authentication.authorities
            )
        }

        profileNewsService.addProfileNews(ProfileNews.Type.PROFILE_PASSWORD_CHANGED, myProfileId, info.userId)

        return ActionStatus()
    }

    /**
     * Сохраняет основные настройки профиля пользователя
     */
    @PostMapping(ProfileUrls.PROFILE_SAVE_MAIN_SETTINGS)
    fun profileSaveMainSettings(@Valid @RequestBody profileMainSettings: ProfileMainSettingsDTO, result: BindingResult): ActionStatus {
        if (result.hasErrors()) {
            return ActionStatus(result)
        }

        val profile = userProfileService.selectProfile(myProfileId())

        if (profile != null) {
            profile.displayName = profileMainSettings.displayName
            profile.alias = profileMainSettings.alias?.toLowerCase()
            profile.userBirthDate = profileMainSettings.userBirthDate
            profile.userGender = profileMainSettings.userGender
            profile.phoneNumber = profileMainSettings.phoneNumber
            profile.countryId = profileMainSettings.countryId
            profile.cityId = profileMainSettings.cityId
            profile.summary = profileMainSettings.summary

            userProfileService.saveProfile(profile)
            updateCurrentProfile(profile)
        }

        profileNewsService.addProfileNews(ProfileNews.Type.PROFILE_SETTINGS_CHANGED, myProfileId(), myProfileId())
        return ActionStatus(result = true)
    }

    /**
     * Обновляет настройки уведомлений пользователя
     */
    @PostMapping(ProfileUrls.CHANGE_SUBSCRIPTIONS)
    fun changeSubscriptions(@RequestBody subscriptionsDTO: SubscriptionsDTO): ActionStatus {
        val profile = userProfileService.selectProfile(myProfileId())

        if (profile != null) {
            profile.isReceiveNewsletters = subscriptionsDTO.receiveNewsletters
            profile.receiveMyCampaignNewsletters = subscriptionsDTO.receiveMyCampaignNewsletters

            userProfileService.updateSubscriptions(profile)
        }

        profileNewsService.addProfileNews(ProfileNews.Type.PROFILE_SUBSCRIPTIONS_CHANGED, myProfileId(), myProfileId())
        return ActionStatus(result = true)
    }

    /**
     * Получает коллекцию операций по балансу пользователя
     */
    @GetMapping(ProfileUrls.PROFILE_BALANCE_OPERATIONS)
    fun getProfileBalanceOperations(@RequestParam(defaultValue = DEFAULT_OFFSET.toString()) offset: Int,
                                    @RequestParam(defaultValue = DEFAULT_LIMIT.toString()) limit: Int): ActionStatus {
        var limitValue = limit
        if (limitValue > DEFAULT_LIMIT) {
            limitValue = DEFAULT_LIMIT
        }

        return ActionStatus(result = moneyTransactionService.getTransactions(myProfileId(), myProfileId(), null, null, offset, limitValue))
    }

    /**
     * Отправить запрос в саппорт на аннулирование заказа
     */
    @PostMapping(ProfileUrls.CANCEL_PURCHASE)
    fun cancelPurchase(@RequestParam orderId: Long) {
        notificationService.sendOrderCancelRequestEmails(orderId, myProfileId())
    }
}

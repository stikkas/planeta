package ru.planeta.web.controllers.campaigns.validation

import org.springframework.stereotype.Component
import org.springframework.validation.Errors
import org.springframework.validation.Validator
import ru.planeta.api.utils.ValidateUtils
import ru.planeta.model.commondb.CampaignFaq

@Component
class CampaignFaqValidator : Validator {

    override fun supports(clazz: Class<*>): Boolean {
        return CampaignFaq::class.java.isAssignableFrom(clazz)
    }

    override fun validate(target: Any, errors: Errors) {
        ValidateUtils.rejectIfContainsNotValidString(errors, "answer", "contains.wrong.strings")
        ValidateUtils.rejectIfContainsNotValidString(errors, "question", "contains.wrong.strings")
    }

}

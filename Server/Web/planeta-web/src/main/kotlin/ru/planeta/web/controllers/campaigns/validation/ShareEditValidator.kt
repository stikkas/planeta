package ru.planeta.web.controllers.campaigns.validation

import org.springframework.stereotype.Component
import org.springframework.validation.Errors
import org.springframework.validation.ValidationUtils
import org.springframework.validation.Validator
import ru.planeta.api.service.campaign.CampaignService
import ru.planeta.api.utils.ValidateUtils
import ru.planeta.model.common.campaign.DraftShareEditDetails
import ru.planeta.model.common.campaign.enums.CampaignStatus
import java.math.RoundingMode
import java.text.SimpleDateFormat
import java.util.*

@Component
class ShareEditValidator(private val campaignService: CampaignService) : Validator {

    override fun supports(clazz: Class<*>): Boolean {
        return DraftShareEditDetails::class.java.isAssignableFrom(clazz)
    }

    override fun validate(target: Any, errors: Errors) {
        ValidateUtils.rejectIfContainsNotValidString(errors, "name", "wrong.chars")
        ValidateUtils.rejectIfContainsNotValidString(errors, "description", "wrong.chars")
        ValidateUtils.rejectIfContainsNotValidString(errors, "rewardInstruction", "wrong.chars")
        ValidateUtils.rejectIfContainsNotValidString(errors, "questionToBuyer", "wrong.chars")
        ValidateUtils.rejectIfContainsNotValidString(errors, "questionToBuyerHtml", "wrong.chars")
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "description", "field.required")
        ValidationUtils.rejectIfEmpty(errors, "price", "field.required")
        ValidationUtils.rejectIfEmpty(errors, "name", "field.required")

        val share = target as DraftShareEditDetails

        if (share.price != null && (share.price?.setScale(0, RoundingMode.HALF_DOWN)?.intValueExact()
                        ?: 0) < PRICE_MIN_VALUE) {
            errors.rejectValue("price", "wrong.number.more.than.50")
        }

        if (share.amount < 0) {
            errors.rejectValue("amount", "wrong.number")
        }

        //validate share connection to campaign
        val campaign = campaignService.getCampaign(share.campaignId)
        if (campaign == null) {
            errors.rejectValue("campaignId", "field.required")
            return
        }

        if ((campaign.status == CampaignStatus.ACTIVE
                        || campaign.status == CampaignStatus.NOT_STARTED
                        || campaign.status == CampaignStatus.PATCH
                        || campaign.status == CampaignStatus.APPROVED
                        || campaign.status == CampaignStatus.PAUSED)
                && ((campaign.timeAdded ?: Date()) < NEW_VALID_RULES_DAY)) {
            if (share.description != null && (share.description?.length ?: -1) > OLD_DESCRIPTION_MAX_LENGTH) {
                errors.rejectValue("description", "campaign.share.description.length.exceed", arrayOf<Any>(OLD_DESCRIPTION_MAX_LENGTH), " limit {0} symbols exceed")
            }

            if (share.name != null && (share.name?.length ?: -1) > OLD_NAME_MAX_LENGTH) {
                errors.rejectValue("name", "campaign.share.name.length.exceed", arrayOf<Any>(OLD_NAME_MAX_LENGTH), " limit {0} symbols exceed")
            }
        } else {
            if (share.description != null && (share.description?.length ?: -1) > DESCRIPTION_MAX_LENGTH) {
                errors.rejectValue("description", "campaign.share.description.length.exceed", arrayOf<Any>(DESCRIPTION_MAX_LENGTH), " limit {0} symbols exceed")
            }

            if (share.name != null && (share.name?.length ?: -1) > NAME_MAX_LENGTH) {
                errors.rejectValue("name", "campaign.share.name.length.exceed", arrayOf<Any>(NAME_MAX_LENGTH), " limit {0} symbols exceed")
            }
        }

        if (share.rewardInstruction != null && (share.rewardInstruction?.length
                        ?: -1) > REWARD_DELIVERY_INSTRUCTION_MAX_LENGTH) {
            errors.rejectValue("rewardInstruction", "campaign.share.reward.instruction.length.exceed", arrayOf<Any>(REWARD_DELIVERY_INSTRUCTION_MAX_LENGTH), " limit {0} symbols exceed")
        }

        if (share.shareId > 0 && share.amount > 0) {
            //check decrease share amount
            val selected = campaignService.getShare(share.shareId)
            if (selected != null && selected.purchaseCount > share.amount) {
                errors.rejectValue("amount", "reject.share.change.amount")
            }
        }
        if (share.pickupByCustomer) {
            //            errors.pushNestedPath("storedAddress");
            //            ValidationUtils.rejectIfEmpty(errors, "zipCode", "field.required");
            ValidationUtils.rejectIfEmpty(errors, "city", "field.required")
            ValidationUtils.rejectIfEmpty(errors, "street", "field.required")
            //            ValidationUtils.rejectIfEmpty(errors, "country", "field.required");
            ValidationUtils.rejectIfEmpty(errors, "phone", "field.required")
            //            errors.popNestedPath();

        }

    }

    companion object {
        private val REWARD_DELIVERY_INSTRUCTION_MAX_LENGTH = 250
        private val DESCRIPTION_MAX_LENGTH = 600
        private val NAME_MAX_LENGTH = 130
        private val PRICE_MIN_VALUE = 50
        private val NEW_VALID_RULES_DAY = SimpleDateFormat("dd-MM-yyyy").parse("20-03-2018")
        private val OLD_DESCRIPTION_MAX_LENGTH = 1000
        private val OLD_NAME_MAX_LENGTH = 200
    }
}

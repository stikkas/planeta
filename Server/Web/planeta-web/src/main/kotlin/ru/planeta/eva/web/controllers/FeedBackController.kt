package ru.planeta.eva.web.controllers

import org.springframework.validation.BindingResult

import org.springframework.validation.FieldError

import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController
import ru.planeta.api.service.campaign.CampaignService

import ru.planeta.api.service.profile.AuthorizationService
import ru.planeta.api.service.profile.ProfileService

import ru.planeta.api.web.authentication.myProfileId
import ru.planeta.eva.api.web.ActionStatus
import ru.planeta.eva.web.dto.AskManagerDTO
import javax.validation.Valid

/**
 * Контоллер для разных форм запросов от пользователей и анонимов к персоналу планеты
 */
@RestController
class FeedBackController(private val campaignService: CampaignService,
                         private val authorizationService: AuthorizationService,
                         private val profileService: ProfileService) {


    @PostMapping(Urls.ASK_MANAGER)
    fun askManager(@Valid @RequestBody askManagerDTO: AskManagerDTO, bindingResult: BindingResult): ActionStatus {
        if (bindingResult.hasErrors()) {
            return ActionStatus(bindingResult)
        }
        val profileId = myProfileId()

        val email = askManagerDTO.email ?: authorizationService.getUserPrivateEmailById(profileId)
        ?: return ActionStatus(FieldError("askManager", "email", "errors.wrong-email"))


        campaignService.sendCampaignCuratorsFeedbackMessage(profileId, askManagerDTO.campaignId!!, askManagerDTO.message,
                email, if (profileId > 0) profileService.getProfileSafe(profileId).displayName ?: "" else "")
        return ActionStatus()

    }
}


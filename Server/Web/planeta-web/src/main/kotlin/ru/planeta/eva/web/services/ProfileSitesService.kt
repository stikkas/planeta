package ru.planeta.eva.web.services

import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import ru.planeta.dao.ProfileSitesDAO
import ru.planeta.dto.ProfileSitesDTO
import java.util.*


@Service
class ProfileSitesService(private var repository: ProfileSitesDAO) {

    private val FACEBOOK_URL = "https://facebook.com/"
    private val VK_URL = "https://vk.com/"
    private val SITE_URL = "http://"
    private val TWITTER_URL = "https://twitter.com/"
    private val GOOGLE_URL = "https://plus.google.com/"

    @Transactional(readOnly = true)
    fun selectProfileSites(profileId: Long): ProfileSitesDTO {
        return repository.selectByProfileId(profileId)
    }

    @Transactional
    fun saveProfileSites(profileSites: ProfileSitesDTO) {
        prepareProfileSite(profileSites)
        val now = Date()
        profileSites.timeUpdated = now
        if (profileSites.profileId == 0L) {
            profileSites.timeAdded = now
            repository.insert(profileSites)
        } else {
            repository.update(profileSites)
        }
    }

    private fun prepareProfileSite(profileSites: ProfileSitesDTO) {
        profileSites.siteUrl = SITE_URL + profileSites.siteUrl
        profileSites.facebookUrl = FACEBOOK_URL + profileSites.facebookUrl
        profileSites.vkUrl = VK_URL + profileSites.vkUrl
        profileSites.twitterUrl = TWITTER_URL + profileSites.twitterUrl
        profileSites.googleUrl = GOOGLE_URL + profileSites.googleUrl
    }
}


package ru.planeta.web.controllers.services

import org.springframework.stereotype.Service
import org.springframework.web.servlet.ModelAndView
import ru.planeta.api.web.controllers.IAction
import ru.planeta.api.web.controllers.services.BaseControllerService
import ru.planeta.model.enums.ProjectType

@Service
class MainBaseControllerService(private val baseControllerService: BaseControllerService) {
    fun defaultModelAndView(action: String, actionName: String, projectType: ProjectType = ProjectType.MAIN): ModelAndView =
            baseControllerService.createDefaultModelAndView(action, actionName, projectType)

    fun defaultModelAndView(action: IAction, projectType: ProjectType = ProjectType.MAIN): ModelAndView =
            baseControllerService.createDefaultModelAndView(action, projectType)
}

package ru.planeta.web.controllers.campaigns.validation

import org.apache.commons.lang3.StringUtils
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.MessageSource
import org.springframework.stereotype.Component
import org.springframework.validation.Errors
import org.springframework.validation.Validator
import ru.planeta.api.utils.ValidateUtils
import ru.planeta.model.common.campaign.CampaignTargeting


@Component
class CampaignTargetingValidator(private val messageSource: MessageSource) : Validator {


    override fun supports(clazz: Class<*>): Boolean {
        return CampaignTargeting::class.java.isAssignableFrom(clazz)
    }

    override fun validate(target: Any, errors: Errors) {
        val campaignTargeting = target as CampaignTargeting

        if (!ValidateUtils.isItOnlyDigitsString(campaignTargeting.fbTargetingId)) {
            errors.rejectValue("fbTargetingId", "allow.only.digits")
        }
        if (StringUtils.isNotEmpty(campaignTargeting.fbTargetingId)) {
            ValidateUtils.rejectIfSizeIsTooLarge(errors, "fbTargetingId", 70, messageSource)
        }

        if (!ValidateUtils.isItValidVkTargetingId(campaignTargeting.vkTargetingId)) {
            errors.rejectValue("vkTargetingId", "allow.only.vk.targeting.id")
        }
        if (StringUtils.isNotEmpty(campaignTargeting.vkTargetingId)) {
            ValidateUtils.rejectIfSizeIsTooLarge(errors, "vkTargetingId", 70, messageSource)
        }
    }
}

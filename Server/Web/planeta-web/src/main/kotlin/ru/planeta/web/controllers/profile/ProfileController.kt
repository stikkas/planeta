package ru.planeta.web.controllers.profile

import com.github.moneytostr.MoneyToStr
import com.ibm.icu.text.RuleBasedNumberFormat
import org.apache.commons.io.IOUtils
import org.apache.commons.lang3.StringUtils
import org.apache.log4j.Logger
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.servlet.ModelAndView
import ru.planeta.api.helper.ProjectService
import ru.planeta.api.service.billing.order.OrderService
import ru.planeta.api.service.campaign.CampaignService
import ru.planeta.api.service.campaign.ContractorService
import ru.planeta.api.service.campaign.DeliveryService
import ru.planeta.api.service.common.InvestingOrderInfoService
import ru.planeta.api.service.geo.DeliveryAddressService
import ru.planeta.api.service.profile.ProfileService
import ru.planeta.api.service.security.PermissionService
import ru.planeta.api.text.FormattingService
import ru.planeta.api.utils.OrderUtils
import ru.planeta.api.web.authentication.myProfileId
import ru.planeta.api.web.authentication.userAuthorizationInfo
import ru.planeta.dao.commondb.TopayTransactionDAO
import ru.planeta.model.common.campaign.CampaignTag
import ru.planeta.model.common.delivery.LinkedDelivery
import ru.planeta.model.common.delivery.SubjectType
import ru.planeta.model.enums.ContractorType
import ru.planeta.model.enums.OrderObjectType
import ru.planeta.model.enums.ProjectType
import ru.planeta.model.enums.ThumbnailType
import ru.planeta.model.shop.enums.DeliveryType
import ru.planeta.model.shop.enums.PaymentStatus
import ru.planeta.web.controllers.Actions
import ru.planeta.web.controllers.Urls
import ru.planeta.web.controllers.services.MainBaseControllerService
import java.math.BigDecimal
import java.net.URL
import java.text.SimpleDateFormat
import java.util.*
import javax.servlet.http.HttpServletResponse

@Controller
class ProfileController(private val formattingService: FormattingService,
                        private val orderService: OrderService,
                        private val deliveryService: DeliveryService,
                        private val deliveryAddressService: DeliveryAddressService,
                        private val topayTransactionDAO: TopayTransactionDAO,
                        private val investingOrderInfoService: InvestingOrderInfoService,
                        private val contractorService: ContractorService,
                        private val permissionService: PermissionService,
                        private val campaignService: CampaignService,
                        private val projectService: ProjectService,
                        private val profileService: ProfileService,
                        private val mainBaseControllerService: MainBaseControllerService) {

    @Value("\${prerender.application.host:}")
    lateinit var prerenderHost: String

    @GetMapping(Urls.INVOICE_HTML)
    fun getPdfReport(@RequestParam transactionId: Long): ModelAndView {
        val transaction = topayTransactionDAO.select(transactionId)
        val order = orderService.getOrderSafe(transaction.orderId)

        val investOrderInfo = investingOrderInfoService.select(transaction.orderId)
        val modelAndView: ModelAndView =
                when (order.orderType) {
                    OrderObjectType.INVESTING, OrderObjectType.INVESTING_WITH_ALL_ALLOWED_INVOICE, OrderObjectType.INVESTING_WITHOUT_MODERATION -> {
                        val campaign = orderService.getCampaignByOrderId(transaction.orderId)
                        val contractor = contractorService.selectByCampaignId(campaign.campaignId)
                        mainBaseControllerService.defaultModelAndView(Actions.PRINT_INVOICE)
                                .addAllObjects(mapOf("investOrderInfo" to investOrderInfo,
                                        "investorBirthDate" to SimpleDateFormat("dd.MM.yyyy").format(investOrderInfo.birthDate),
                                        "transactionDate" to SimpleDateFormat("dd.MM.yyyy").format(transaction.timeAdded),
                                        "wordNumberSum" to MoneyToStr(MoneyToStr.Currency.RUR, MoneyToStr.Language.RUS, MoneyToStr.Pennies.NUMBER).convert(order.totalPrice.toDouble()),
                                        "contractor" to contractor,
                                        "campaign" to campaign))
                    }
                    OrderObjectType.BIBLIO -> mainBaseControllerService.defaultModelAndView(Actions.PRINT_BIBLIO_INVOICE)
                            .addAllObjects(mapOf("investOrderInfo" to investOrderInfo,
                                    "investorBirthDate" to SimpleDateFormat("dd.MM.yyyy").format(investOrderInfo.birthDate),
                                    "transactionDate" to SimpleDateFormat("dd.MM.yyyy").format(transaction.timeAdded),
                                    "wordNumberSum" to MoneyToStr(MoneyToStr.Currency.RUR,
                                            MoneyToStr.Language.RUS,
                                            MoneyToStr.Pennies.NUMBER).convert(order.totalPrice.toDouble())))
                    else -> return mainBaseControllerService.defaultModelAndView(Actions.PRINT_UNSUPPORTED)
                }

        return modelAndView.addAllObjects(mapOf("order" to order, "profile" to null))
    }

    @GetMapping(Urls.INVOICE_PUBLIC)
    fun getPdfReportPublic(@RequestParam transactionId: Long, response: HttpServletResponse) {
        isMyTransaction(transactionId)
        getPdf(response, Urls.INVOICE_HTML, transactionId, false, "transactionId")
    }

    @GetMapping(Urls.BIBLIO_CERT_HTML)
    fun getBiblioCertReport(@RequestParam transactionId: Long): ModelAndView {
        val transaction = topayTransactionDAO.select(transactionId)

        val order = orderService.getOrderSafe(transaction.orderId)

        val modelAndView: ModelAndView
        if (order.orderType == OrderObjectType.BIBLIO) {
            val oi = orderService.getOrderInfo(order)
            val libraries = oi.orderObjectsInfo
                    .filter { it.objectType == OrderObjectType.LIBRARY }
                    .map { it.objectName }
            if (libraries.isEmpty()) {
                // TODO что-нить придумать
                return mainBaseControllerService.defaultModelAndView(Actions.PRINT_UNSUPPORTED)
            }

            val books = oi.orderObjectsInfo
                    .filter { it.objectType == OrderObjectType.BOOK }
                    .map { it.objectName }
                    .toSet()
            if (books.isEmpty()) {
                return mainBaseControllerService.defaultModelAndView(Actions.PRINT_UNSUPPORTED)
            }

            val investInfo = investingOrderInfoService.select(order.orderId)
            val customerName = if (investInfo == null) {
                oi.buyerName
            } else {
                investInfo.orgName
            }
            val librarySum = order.totalPrice.divide(BigDecimal(libraries.size))
            modelAndView = mainBaseControllerService.defaultModelAndView(Actions.PRINT_BIBLIO_CERT)
                    .addAllObjects(mapOf("customerName" to customerName,
                            "isOrg" to (investInfo != null),
                            "gender" to oi.buyerGender,
                            "books" to books,
                            "libraries" to libraries,
                            "librarySum" to librarySum))
        } else {
            return mainBaseControllerService.defaultModelAndView(Actions.PRINT_UNSUPPORTED)
        }
        return modelAndView
    }

    @GetMapping(Urls.BIBLIO_CERT_PUBLIC)
    fun getBiblioCertPdf(@RequestParam transactionId: Long, response: HttpServletResponse) =
            getPdf(response, Urls.BIBLIO_CERT_HTML, transactionId, true, "transactionId")

    @GetMapping(Urls.CAMPAIGN_CONTRACT_PDF_PUBLIC)
    fun getPdfCampaignContractReport(@RequestParam campaignId: Long, response: HttpServletResponse) {
        val campaign = campaignService.getCampaignSafe(campaignId)
        permissionService.checkIsAdmin(myProfileId(), campaign.profileId ?: -1)
        getPdf(response, Urls.CAMPAIGN_CONTRACT_HTML, campaignId, false, "campaignId")
    }

    @GetMapping(Urls.CAMPAIGN_CONTRACT_HTML)
    fun getCampaignContractReport(@RequestParam campaignId: Long, @RequestParam(defaultValue = "false") columnize: Boolean): ModelAndView {
        val campaign = campaignService.getCampaignSafe(campaignId)
        val hasCharityTag: Boolean = campaign.tags.any { it.mnemonicName == CampaignTag.CHARITY }

        val contractor = contractorService.selectByCampaignId(campaign.campaignId)
        val modelAndView = when (contractor.type) {
            ContractorType.INDIVIDUAL -> mainBaseControllerService.defaultModelAndView(Actions.PRINT_CAMPAIGN_CONTRACT_INDIVIDUAL)
            ContractorType.INDIVIDUAL_ENTREPRENEUR -> mainBaseControllerService.defaultModelAndView(Actions.PRINT_CAMPAIGN_CONTRACT_INDIVIDUAL_ENTREPRENEUR)
            ContractorType.CHARITABLE_FOUNDATION -> if (hasCharityTag) mainBaseControllerService.defaultModelAndView(Actions.PRINT_CAMPAIGN_CONTRACT_CHARITY)
            else mainBaseControllerService.defaultModelAndView(Actions.PRINT_CAMPAIGN_CONTRACT_LEGAL_ENTITY)
            ContractorType.LEGAL_ENTITY,
            ContractorType.OOO -> mainBaseControllerService.defaultModelAndView(Actions.PRINT_CAMPAIGN_CONTRACT_LEGAL_ENTITY)
            else -> mainBaseControllerService.defaultModelAndView(Actions.PRINT_CAMPAIGN_CONTRACT_NOT_SUPPORTED)
        }

        val nf = RuleBasedNumberFormat(Locale.forLanguageTag("ru"),
                RuleBasedNumberFormat.SPELLOUT)

        modelAndView.addAllObjects(mapOf("campaign" to campaign,
                "wordNumberTargetAmount" to nf.format(campaign.targetAmount),
                "contractor" to contractor,
                "columnize" to columnize))
        return modelAndView
    }

    @GetMapping(Urls.CAMPAIGN_NEW_NKO_CONTRACT_PDF_PUBLIC)
    fun getPdfCampaignNewNKOContractReport(@RequestParam contractorId: Long, response: HttpServletResponse) {
        permissionService.hasAdministrativeRole(myProfileId())
        getPdf(response, Urls.CAMPAIGN_NEW_NKO_CONTRACT_HTML, contractorId, false, "contractorId")
    }

    @GetMapping(Urls.CAMPAIGN_NEW_NKO_CONTRACT_HTML)
    fun getCampaignNewNKOContractReport(@RequestParam contractorId: Long, @RequestParam(defaultValue = "false") columnize: Boolean): ModelAndView {
        val contractor = contractorService.select(contractorId);
        val modelAndView = mainBaseControllerService.defaultModelAndView(Actions.PRINT_NEW_NKO_CONTRACT);
        modelAndView.addAllObjects(mapOf("contractor" to contractor,
                "columnize" to columnize))
        return modelAndView
    }

    private fun getPdf(resp: HttpServletResponse, url: String, transactionId: Long, landscape: Boolean, parameterName: String) {
        val fileName = transactionId.toString() + ".pdf"
        resp.setHeader("Content-Type", "application/pdf")
        resp.setHeader("Content-disposition", "attachment; filename=" + fileName)
        val requestUrl = String.format(getPrerenderHostTemplate(prerenderHost),
                projectService.getUrl(ProjectType.MAIN) + url + "?" + parameterName + "=" + transactionId + "&renderAsPdf=1&" + (if (landscape) "landScape=1&" else "") + "pdfName=" + fileName)
        try {
            val link = URL(requestUrl)
            val inputStream = link.openStream()
            try {
                IOUtils.copy(inputStream, resp.outputStream)
            } finally {
                IOUtils.closeQuietly(inputStream)
            }
        } catch (ex: Exception) {
            log.warn("Pdf file get error", ex)
        }

    }

    @GetMapping(Urls.PRINT_ORDER)
    fun getOrderPrint(@RequestParam orderId: Long): ModelAndView {
        val profileId = myProfileId()
        val order = orderService.getOrderSafe(profileId, orderId)

        val orderObjects = orderService.getOrderObjectsInfo(profileId, orderId)
        val orderObject = orderObjects?.iterator()?.next()
        val groupedOrderObjects = OrderUtils.groupOrderObjectsByObjectId(orderObjects) { o1, o2 -> (o1.orderObjectId - o2.orderObjectId).toInt() }
        val modelAndView: ModelAndView =
                when (order.orderType) {
                    OrderObjectType.INVESTING,
                    OrderObjectType.INVESTING_WITH_ALL_ALLOWED_INVOICE,
                    OrderObjectType.INVESTING_WITHOUT_MODERATION,
                    OrderObjectType.SHARE -> {
                        val share = campaignService.getShareSafe(orderObject?.objectId ?: 0)
                        share.rewardInstruction = formattingService.formatMessage(share.rewardInstruction, myProfileId(), true)
                        share.imageUrl = ThumbnailType.getUrlByThumbnailType(share.imageUrl
                                ?: "", ThumbnailType.PHOTOPREVIEW)
                        val campaign = campaignService.getCampaignSafe(share.campaignId)
                        val group = profileService.getProfile(campaign.creatorProfileId)
                        mainBaseControllerService.defaultModelAndView(Actions.PRINT_SHARE_ORDER).addAllObjects(mapOf("share" to share,
                                "group" to group,
                                "campaign" to campaign,
                                "description" to StringUtils.replace(share.description, "\n", "<br>"),
                                "deliveryPublicNote" to (if (order.deliveryDepartmentId != 0L)
                                    StringUtils.replace(
                                            deliveryService.getLinkedDelivery(share.shareId, order.deliveryDepartmentId, SubjectType.SHARE).publicNote,
                                            "\n", "<br>")
                                else null)))
                    }
                    OrderObjectType.PRODUCT -> {
                        for (objects in groupedOrderObjects.values) {
                            for (i in objects.indices) {
                                objects[i].count = i + 1
                            }
                        }
                        mainBaseControllerService.defaultModelAndView(Actions.PRINT_PRODUCT_ORDER)
                    }
                    OrderObjectType.BONUS -> mainBaseControllerService.defaultModelAndView(Actions.PRINT_BONUS_ORDER).addObject("objectName", orderObject?.objectName)
                    else -> return mainBaseControllerService.defaultModelAndView(Actions.PRINT_UNSUPPORTED)
                }
        modelAndView.addObject("isCancelled", order.paymentStatus == PaymentStatus.CANCELLED)

        if (order.deliveryType != DeliveryType.NOT_SET) {

            val deliveryAddress = deliveryAddressService.getDeliveryAddress(order.orderId)
            modelAndView.addObject("deliveryAddress", deliveryAddress)

            val delivery: LinkedDelivery =
                    when (order.orderType) {
                        OrderObjectType.PRODUCT -> deliveryService.getLinkedDelivery(0, order.deliveryDepartmentId, SubjectType.SHOP)
                        OrderObjectType.SHARE -> deliveryService.getLinkedDelivery(orderObjects?.iterator()?.next()?.objectId ?: 0, order.deliveryDepartmentId, SubjectType.SHARE)
                        OrderObjectType.BONUS -> LinkedDelivery()
                        else -> return mainBaseControllerService.defaultModelAndView(Actions.PRINT_UNSUPPORTED)
                    }
            modelAndView.addObject("delivery", delivery)
        }
        return modelAndView.addAllObjects(mapOf("order" to order,
                "orderObjects" to orderObjects,
                "groupedOrderObjects" to groupedOrderObjects,
                "profile" to userAuthorizationInfo()?.profile))
    }

    private fun isMyTransaction(transactionId: Long) {
        val transaction = topayTransactionDAO.select(transactionId)
        val order = orderService.getOrderSafe(transaction.orderId)
        permissionService.checkIsAdmin(myProfileId(), order.buyerId)
    }

    companion object {

        private val log = Logger.getLogger(ProfileController::class.java)

        private fun getPrerenderHostTemplate(host: String?): String {
            return "http://$host/%s"
        }
    }
}

package ru.planeta.web.controllers.campaigns.validation

import org.apache.commons.lang3.StringUtils
import org.apache.commons.lang3.time.DateUtils
import org.springframework.context.MessageSource
import org.springframework.stereotype.Component
import org.springframework.validation.Errors
import org.springframework.validation.ValidationUtils
import org.springframework.validation.Validator
import ru.planeta.api.service.campaign.CampaignService
import ru.planeta.api.service.security.PermissionService
import ru.planeta.api.utils.ValidateUtils
import ru.planeta.api.web.authentication.myProfileId
import ru.planeta.model.common.campaign.Campaign
import java.math.BigDecimal
import java.math.RoundingMode
import java.util.*

@Component
class CampaignValidator(
        private val messageSource: MessageSource,
        private val campaignService: CampaignService,
        private val permissionService: PermissionService) : Validator {

    override fun supports(clazz: Class<*>): Boolean {
        return Campaign::class.java.isAssignableFrom(clazz)
    }

    override fun validate(target: Any, errors: Errors) {

        val campaign = target as Campaign
        val myProfileId = myProfileId()

        ValidateUtils.rejectIfNotCampaignAlias(errors, "campaignAlias", "campaign.alias.wrong.chars")
        if (errors.getFieldErrorCount("campaignAlias") == 0) {
            ValidateUtils.rejectIfNotCampaignAliasHasLetter(errors, "campaignAlias", "campaign.alias.wrong.start")
        }
        if (errors.getFieldErrorCount("campaignAlias") == 0) {
            ValidateUtils.rejectIfSizeIsTooLarge(errors, "campaignAlias", 64, messageSource)
        }

        if (errors.getFieldErrorCount("campaignAlias") == 0) {
            ValidateUtils.rejectIfSizeIsTooSmall(errors, "campaignAlias", "field.length.short.4", 4, true)
        }

        if (errors.getFieldErrorCount("campaignAlias") == 0) {
            val foundedCampaign = campaignService.getCampaign(campaign.campaignAlias ?: "")
            if (foundedCampaign != null && foundedCampaign.campaignId != campaign.campaignId) {
                errors.rejectValue("campaignAlias", "field.error.exists")
            }
        }

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "name", "field.required")
        if (!StringUtils.isEmpty(campaign.name)) {
            ValidateUtils.rejectIfSizeIsTooLarge(errors, "name", 45, messageSource)
        }

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "shortDescription", "field.required")
        ValidateUtils.rejectIfSizeIsTooLarge(errors, "shortDescription", 180, messageSource)

        if (campaign.tags.size < 1) {
            errors.rejectValue("tags", "atleast.one.tag.required")
        } else if (campaign.tags.size > 3) {
            if (!permissionService.hasAdministrativeRole(myProfileId)) {
                errors.rejectValue("tags", "nomore.then.three.tags")
            }
        }

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "descriptionHtml", "field.required")

        ValidationUtils.rejectIfEmpty(errors, "imageUrl", "field.required")
        if (errors.getFieldValue("imageUrl") === "null") {
            errors.rejectValue("imageUrl", "field.required")
        }

        if (StringUtils.isEmpty(campaign.viewImageUrl)) {
            errors.rejectValue("videoContent", "field.required")
        }

        if (campaign.shares.size < MIN_SHARES_COUNT_IN_CAMPAIGN) {
            errors.rejectValue("shares", "campaign.shares.empty") //
        }

        if (!permissionService.hasAdministrativeRole(myProfileId)) {
            val targetAmount = campaign.targetAmount
            if (targetAmount != null) {
                ValidationUtils.rejectIfEmpty(errors, "targetAmount", "field.required")
                if (!isIntegerValue(targetAmount) || targetAmount.compareTo(BigDecimal.ZERO) == 0
                        || targetAmount < BigDecimal(10000)
                        || targetAmount.setScale(2, RoundingMode.HALF_UP).precision() > 10) {
                    errors.rejectValue("targetAmount", "wrong.number.more.than.10000")
                }
            }
            ValidationUtils.rejectIfEmpty(errors, "timeFinish", "date.expected")
            val finishedTime = campaign.timeFinish
            if (campaign.status?.isDraft == true && finishedTime != null) {
                val now = DateUtils.truncate(Date(), Calendar.DATE)
                val finish = DateUtils.truncate(finishedTime, Calendar.DATE)
                if (now.after(finish)) {
                    errors.rejectValue("timeFinish", "campaign.timeFinish.before.now")
                } else if (DateUtils.addDays(now, 99).before(finish)) {
                    errors.rejectValue("timeFinish", "campaign.timeFinish.exceed")
                }

            }
        }
    }

    companion object {

        private val MIN_SHARES_COUNT_IN_CAMPAIGN = 2

        private fun isIntegerValue(targetAmount: BigDecimal): Boolean {
            return try {
                targetAmount.toBigIntegerExact()
                true
            } catch (e: ArithmeticException) {
                false
            }

        }
    }

}


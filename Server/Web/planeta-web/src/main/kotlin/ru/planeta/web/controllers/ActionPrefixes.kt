package ru.planeta.web.controllers

enum class ActionPrefixes private constructor(private val text: String) {

    MEMBER("member/"),
    MEMBER_PROFILE("member/profile/"),
    MEMBER_PRINT("member/print/"),
    WELCOME("welcome/"),
    ADMIN("admin/"),
    STATIC("static/"),
    STAT("admin/stat/"),
    COMMON_INCLUDES("includes/generated/"),
    CAMPAIGNS_ADMIN("campaigns/admin/"),
    CAMPAIGNS("campaigns/"),
    INCLUDES("includes/"),
    VK("vkapp/"),
    INVESTING("investing/");

    override fun toString(): String {
        return text
    }
}

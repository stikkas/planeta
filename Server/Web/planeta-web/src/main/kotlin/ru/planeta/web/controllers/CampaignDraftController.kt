package ru.planeta.web.controllers

import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import ru.planeta.api.model.ActionStatus
import ru.planeta.api.web.authentication.myProfileId
import ru.planeta.dao.trashcan.CampaignDraftDAO
import ru.planeta.model.trashcan.CampaignDraft
import ru.planeta.web.controllers.Urls.SAVE_CAMPAIGN_DRAFT
import java.util.*

@RestController
class CampaignDraftController(private val campaignDraftDAO: CampaignDraftDAO) {

    @PostMapping(SAVE_CAMPAIGN_DRAFT)
    fun saveDraft(@RequestParam("campaignId") campaignId: Long,
                  @RequestParam("desc") description: String): ActionStatus<*> {
        campaignDraftDAO.insert(CampaignDraft(campaignId, myProfileId(), Date(), description))
        return ActionStatus.createSuccessStatus<Any>()
    }
}

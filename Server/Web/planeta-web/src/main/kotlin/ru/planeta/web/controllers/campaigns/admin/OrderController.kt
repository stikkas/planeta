package ru.planeta.web.controllers.campaigns.admin

import org.apache.log4j.Logger
import org.springframework.http.HttpHeaders
import org.springframework.stereotype.Controller
import org.springframework.validation.BindingResult
import org.springframework.web.bind.annotation.*
import org.springframework.web.servlet.ModelAndView
import ru.planeta.api.exceptions.MessageCode
import ru.planeta.api.exceptions.PermissionException
import ru.planeta.api.model.ActionStatus
import ru.planeta.api.news.LoggerService
import ru.planeta.api.service.billing.order.BatchCancelerService
import ru.planeta.api.service.billing.order.OrderService
import ru.planeta.api.service.campaign.CampaignService
import ru.planeta.api.service.campaign.CampaignStatsService
import ru.planeta.api.service.campaign.DeliveryService
import ru.planeta.api.service.security.PermissionService
import ru.planeta.api.service.statistic.StatisticsService
import ru.planeta.api.web.authentication.myProfileId
import ru.planeta.api.web.controllers.services.BaseControllerService
import ru.planeta.api.web.utils.ExcelReportUtils
import ru.planeta.model.common.DeliveryAddress
import ru.planeta.model.common.OrderInfo
import ru.planeta.model.common.campaign.AggregationType
import ru.planeta.model.common.campaign.CampaignSearchFilter
import ru.planeta.model.common.delivery.SubjectType
import ru.planeta.model.news.ProfileNews
import ru.planeta.model.shop.enums.DeliveryStatus
import ru.planeta.reports.ReportType
import ru.planeta.web.controllers.Actions
import ru.planeta.web.controllers.Urls
import ru.planeta.web.controllers.services.MainBaseControllerService
import java.math.BigDecimal
import java.util.*
import java.util.concurrent.TimeUnit
import javax.servlet.http.HttpServletResponse
import javax.validation.Valid

@Controller
class OrderController(
        private val deliveryService: DeliveryService,
        private val campaignStatsService: CampaignStatsService,
        private val statisticsService: StatisticsService,
        private val batchCancelerService: BatchCancelerService,
        private val campaignService: CampaignService,
        private val permissionService: PermissionService,
        private val baseControllerService: BaseControllerService,
        private val mainBaseControllerService: MainBaseControllerService,
        private val orderService: OrderService,
        private val profileNewsService: LoggerService) {

    @ResponseBody
    @RequestMapping(value = Urls.Admin.ORDERS_REPORT, method = [RequestMethod.POST, RequestMethod.GET])
    fun getCampaignOrdersReport(@RequestParam(defaultValue = "CSV") reportType: ReportType,
                                campaignSearchFilter: CampaignSearchFilter, response: HttpServletResponse) {
        val report = campaignStatsService.getReport(myProfileId(), reportType, campaignSearchFilter)
        report.addToResponse(response)
    }


    @RequestMapping(value = Urls.Admin.ORDERS_REPORT_TO_EMAIL, method = [RequestMethod.POST, RequestMethod.GET])
    @ResponseBody
    fun getCampaignOrdersReportToEmail(@RequestParam(defaultValue = "CSV") reportType: ReportType, campaignSearchFilter: CampaignSearchFilter): ActionStatus<*> {
        val clientId = myProfileId()
        baseControllerService.delay(Runnable {
            try {
                val report = campaignStatsService.getReport(clientId, reportType, campaignSearchFilter)
                baseControllerService.sendReport(clientId, report)
            } catch (e: Exception) {
                log.error("", e)
            }
        }, 0, TimeUnit.SECONDS)
        return ActionStatus.createSuccessStatus<Any>()
    }

    @ResponseBody
    @PostMapping(Urls.Admin.CAMPAIGN_STATS_REPORT)
    fun getCampaignAffiliateReport(
            @RequestParam campaignId: Long,
            @RequestParam(defaultValue = "CSV") reportType: ReportType,
            @RequestParam(required = false) dateFrom: Date?,
            @RequestParam(required = false) dateTo: Date?,
            @RequestParam(defaultValue = "0") offset: Int,
            @RequestParam(defaultValue = "DATE") aggregationType: AggregationType,
            @RequestParam(defaultValue = "0") limit: Int,
            response: HttpServletResponse, campaignSearchFilter: CampaignSearchFilter) {

        val campaign = campaignService.getCampaignSafe(campaignSearchFilter.campaignId)
        if (!permissionService.isAdmin(myProfileId(), campaign.profileId ?: -1)) {
            throw PermissionException(MessageCode.USER_NOT_ADMIN)
        }

        val report = reportType.createReport("campaign_statistic_report_" + campaignSearchFilter.campaignId)  //создаем файл

        when (aggregationType) {
            AggregationType.DATE,
            AggregationType.GENERAL -> {
                report.addCaptionRow(*baseControllerService.getParametrizedMessage("report.campaign.statistic.date")
                        .split(";".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray())
                statisticsService.getCampaignGeneralStats(campaignId, dateFrom, dateTo, offset, limit)
                        .forEach { report.addRow(it.date, it.visitors, it.views, it.purchases, it.buyers, it.comments, it.amount) }
            }
            AggregationType.CITY -> {
                report.addCaptionRow(*baseControllerService.getParametrizedMessage("report.campaign.statistic.city")
                        .split(";".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray())
                statisticsService.getCampaignCityStats(campaignId, dateFrom, dateTo, offset, limit)
                        .forEach {
                            report.addRow(if (it.city.isNullOrEmpty()) baseControllerService.getParametrizedMessage("not.specified.city")
                            else it.city, it.visitors, it.views, it.purchases)
                        }
            }
            AggregationType.COUNTRY -> {
                report.addCaptionRow(*baseControllerService.getParametrizedMessage("report.campaign.statistic.country")
                        .split(";".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray())
                statisticsService.getCampaignCountryStats(campaignId, dateFrom, dateTo, offset, limit)
                        .forEach {
                            report.addRow(if (it.country.isNullOrEmpty()) baseControllerService.getParametrizedMessage("not.specified.country")
                            else it.country, it.visitors, it.views, it.purchases)
                        }
            }
            AggregationType.REFERER -> {
                report.addCaptionRow(*baseControllerService.getParametrizedMessage("report.campaign.statistic.referer")
                        .split(";".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray())
                statisticsService.getCampaignRefererStats(campaignId, dateFrom, dateTo, offset, limit)
                        .forEach {
                            report.addRow(if (it.referer.isNullOrEmpty()) baseControllerService.getParametrizedMessage("not.specified.referer")
                            else it.referer, it.visitors, it.views, it.purchases)
                        }
            }
            else -> {
            }
        }
        report.addToResponse(response)
    }

    @PostMapping(Urls.Admin.SHARES_SALE_STATS_REPORT)
    fun getCampaignSharesSaleReport(
            @RequestParam campaignId: Long,
            @RequestParam(defaultValue = "CSV") reportType: ReportType,
            @RequestParam(required = false) dateFrom: Date?,
            @RequestParam(required = false) dateTo: Date?,
            @RequestParam(defaultValue = "0") offset: Int,
            @RequestParam(defaultValue = "0") limit: Int,
            response: HttpServletResponse): ModelAndView? {

        val format = OLD_FORMATS[reportType]

        val campaign = campaignService.getCampaignSafe(campaignId)
        if (!permissionService.isAdmin(myProfileId(), campaign.profileId ?: -1)) {
            throw PermissionException(MessageCode.USER_NOT_ADMIN)
        }

        if (!permissionService.isAdmin(myProfileId(), campaign.profileId ?: -1)) {
            throw PermissionException(MessageCode.PERMISSION_MESSAGE_DENIED)
        }
        val contentDisposition = String.format(CONTENT_DISPOSITION_TEMPLATE, campaignId, format)
        response.status = HttpServletResponse.SC_OK
        response.setHeader(HttpHeaders.CONTENT_DISPOSITION, contentDisposition)
        response.setHeader(HttpHeaders.CONTENT_TYPE, String.format(CONTENT_TYPE_TEMPLATE, MIME_TYPE_MAP[format]))

        val shares = campaignStatsService.getSharesSaleInfo(campaignId, dateFrom, dateTo, offset, limit)
        if ("xls" == format) {
            ExcelReportUtils.generateCampaignSharesSaleReport(shares, response.writer)
            return null
        } else {
            return mainBaseControllerService.defaultModelAndView(Actions.ADMIN_CAMPAIGN_SHARES_STATS_REPORT)
                    .addObject("shares", shares)
                    .addObject("format", format)
        }
    }

    @PostMapping(Urls.Admin.BATCH_ORDERS_CANCEL)
    @ResponseBody
    fun batchCancelOrders(@RequestParam(value = "orderIds[]") orderIds: List<Long>,
                          @RequestParam(defaultValue = "") reason: String): ActionStatus<Int> {
        val count = batchCancelerService.batchCancelOrders(myProfileId(), orderIds, reason)
        return ActionStatus.createSuccessStatus(count)
    }

    @PostMapping(Urls.Admin.BATCH_ORDERS_COMPLETE)
    @ResponseBody
    fun batchDeliveryOrders(@RequestParam(value = "orderIds[]") orderIds: List<Long>,
                            @RequestParam(value = "orderState") orderState: String,
                            @RequestParam(value = "isSendNotification", defaultValue = "false") isSendNotification: Boolean): ActionStatus<*> {
        for (orderId in orderIds) {
            updateOrderState(orderId, orderState, null, null, "0", null, isSendNotification);
        }
        return ActionStatus.createSuccessStatus<Any>()
    }

    @PostMapping(Urls.Admin.UPDATE_ORDER_STATE)
    @ResponseBody
    fun updateOrderState(@RequestParam orderId: Long,
                         @RequestParam orderState: String,
                         @RequestParam(required = false) reason: String?,
                         @RequestParam(required = false) trackingCode: String?,
                         @RequestParam(defaultValue = "0") cashOnDeliveryCost: String,
                         @RequestParam(required = false) deliveryComment: String?,
                         @RequestParam(defaultValue = "false") isSendNotification: Boolean): ActionStatus<*> {

        val cashOnDeliveryCostNewValue = BigDecimal(cashOnDeliveryCost.replace(',', '.'))

        when (orderState) {
            "DELIVERED" -> {
                orderService.delivery(myProfileId(), orderId, trackingCode, cashOnDeliveryCostNewValue, deliveryComment, isSendNotification, DeliveryStatus.DELIVERED)
                profileNewsService.addProfileNews(ProfileNews.Type.UPDATE_ORDER_STATUS_COMPLETE, myProfileId(), orderId)
            }
            "IN_TRANSIT" -> {
                orderService.delivery(myProfileId(), orderId, trackingCode, cashOnDeliveryCostNewValue, deliveryComment, isSendNotification, DeliveryStatus.IN_TRANSIT)
                profileNewsService.addProfileNews(ProfileNews.Type.UPDATE_ORDER_STATUS_IN_TRANSIT, myProfileId(), orderId)
            }
            "GIVEN" -> {
                orderService.delivery(myProfileId(), orderId, trackingCode, cashOnDeliveryCostNewValue, deliveryComment, isSendNotification, DeliveryStatus.GIVEN)
                profileNewsService.addProfileNews(ProfileNews.Type.UPDATE_ORDER_STATUS_GIVEN, myProfileId(), orderId)
            }
            "CANCEL" -> orderService.cancelSharePurchase(myProfileId(), orderId, reason, isSendNotification)
        }

        return ActionStatus.createSuccessStatus<Any>()
    }

    @PostMapping(Urls.Admin.UPDATE_BUYER_ANSWER)
    @ResponseBody
    fun updateBuyerAnswer(@RequestParam orderId: Long,
                          @RequestParam objectId: Long,
                          @RequestParam answer: String): ActionStatus<*> {
        var result: ActionStatus<String>
        try {
            orderService.correctBuyerAnswer(myProfileId(), orderId, objectId, answer)
            result = ActionStatus.createSuccessStatus(answer)
        } catch (e: PermissionException) {
            result = ActionStatus.createErrorStatus("cannot.update.buyer.answer", baseControllerService.messageSource, e.javaClass)
        }

        return result
    }

    @GetMapping(Urls.Admin.GET_SHARE_DELIVERY_SETTINGS)
    @ResponseBody
    fun getShareDeliverySettings(@RequestParam shareId: Long): ActionStatus<*> {
        var result: ActionStatus<*>
        try {

            val deliverySettings = deliveryService.getLinkedDeliveries(shareId, SubjectType.SHARE)
            result = ActionStatus.createSuccessStatus(deliverySettings)
        } catch (e: Exception) {
            result = ActionStatus.createErrorStatus<Any>("cannot.get.delivery.settings", baseControllerService.messageSource, e.javaClass)
        }

        return result
    }

    @PostMapping(Urls.Admin.UPDATE_ORDER_DELIVERY)
    @ResponseBody
    fun updateOrderAddressee(@ModelAttribute @Valid address: DeliveryAddress,
                             bindingResult: BindingResult): ActionStatus<*> {
        val result: ActionStatus<*>
        if (bindingResult.hasErrors()) {
            result = baseControllerService.createErrorStatus<Any>(bindingResult)
        } else {
            val myProfileId = myProfileId()
            orderService.addDeliveryAddress(myProfileId, address.orderId, address)
            result = ActionStatus.createSuccessStatus<OrderInfo>(orderService.getOrderInfo(myProfileId, address.orderId))
        }
        return result
    }

    companion object {

        private val log = Logger.getLogger(OrderController::class.java)

        private val CONTENT_TYPE_TEMPLATE = "%s; charset=UTF-8"
        private val CONTENT_DISPOSITION_TEMPLATE = "attachment;filename=Report_%d_.%s"

        private val MIME_TYPE_MAP = mapOf("xls" to "application/vnd.ms-excel", "csv" to "text/csv")
        private val OLD_FORMATS = mapOf(ReportType.CSV to "csv", ReportType.EXCEL to "xls")

    }
}

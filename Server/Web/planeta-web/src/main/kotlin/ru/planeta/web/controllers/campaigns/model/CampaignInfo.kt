package ru.planeta.web.controllers.campaigns.model

import ru.planeta.api.model.enums.image.ImageType
import ru.planeta.api.service.configurations.ImageHelperFunctions
import ru.planeta.model.common.campaign.Campaign
import ru.planeta.model.enums.ThumbnailType
import java.math.BigDecimal
import java.math.RoundingMode
import java.util.*

class CampaignInfo {

    private var platform: String? = null //  - платформа,
    var name: String? = null //  - имя проекта,
    var photo: String? = null //  - ссылка на среднее изображение проекта,
    var user_name: String? = null // - имя пользователя,
    private var short_description: String? = null //  - краткое описание,
    var city: String? = null //  - город,
    private var funded_percentage: Int = 0 //  - % собранных средств,
    private var goal: Int = 0 //  - цель в рублях,
    private var pledged: Int = 0 // - количество собранных денег в рублях,
    private var finish_date: Date? = null // - дата завершения с временной зоной,
    private var finish_unix_time: Long? = null // - дата завершения в секундах (UTC),
    private var remains_seconds: Long? = null // - сколько осталось времени в секундах,
    var category: String? = null // - субкатегория,
    var parent_category: String? = null // - родительская категория
    var url: String? = null // урл проекта
    var video: String? = null // video url
    var video_embed_code: String? = null // video_embed_code  <video_embed_code> <![CDATA[ ...]]></video_embed_code>
    var pledges: Pledges? = null

    constructor(campaign: Campaign) {
        platform = "Planeta.ru"
        name = campaign.name
        photo = ImageHelperFunctions.getThumbnailUrl(campaign.imageUrl, ThumbnailType.MEDIUM, ImageType.PHOTO)
        short_description = campaign.shortDescription
        campaign.targetAmount?.let {
            if (it > BigDecimal.ZERO) {
                funded_percentage = campaign.purchaseSum.divide(campaign.targetAmount, 2, RoundingMode.HALF_UP).multiply(BigDecimal(100)).toInt()
            }
        }

        goal = campaign.targetAmount?.toInt() ?: 0
        pledged = campaign.purchaseSum.toInt()
        category = ""
        finish_date = campaign.timeFinish
        finish_date?.let {
            finish_unix_time = finish_date?.time
            remains_seconds = finish_unix_time?.minus(Date().time)
        }
    }
}

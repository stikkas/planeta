package ru.planeta.web.controllers.welcome

import org.apache.commons.io.IOUtils
import org.apache.log4j.Logger
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.ResponseBody
import ru.planeta.api.helper.ProjectService
import ru.planeta.web.controllers.Urls
import java.net.URL
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse


@Controller
class OfflineController(private val projectService: ProjectService) {

    @Value("\${static.flushCache:false}")
    private val flushCache: Boolean = false

    @Value("\${static.compress:true}")
    private val compressJs: Boolean = false

    @GetMapping(Urls.OFFLINE)
    @ResponseBody
    fun getHtmlSiteMap(request: HttpServletRequest, response: HttpServletResponse) {
        try {
            response.setHeader("Content-Type", "text/javascript; charset=utf-8")
            var baseUrl = projectService.getStaticJsUrl("/js/offline.js")
            baseUrl += "?lang=" + request.locale.language
            if (flushCache) {
                baseUrl += "&flushCache=true"
            }
            if (!compressJs) {
                baseUrl += "&compress=false"
            }
            val link = URL(baseUrl)
            val inputStream = link.openStream()
            try {
                IOUtils.copy(inputStream, response.outputStream)
            } finally {
                IOUtils.closeQuietly(inputStream)
            }
        } catch (ex: Exception) {
            log.warn("Offline manifest error", ex)
        }

    }

    private val log = Logger.getLogger(OfflineController::class.java)
}

package ru.planeta.web.controllers.campaigns

import org.springframework.context.MessageSource
import org.springframework.validation.BindingResult
import org.springframework.web.bind.annotation.*
import ru.planeta.api.model.ActionStatus
import ru.planeta.api.service.campaign.CampaignFaqService
import ru.planeta.api.web.authentication.myProfileId
import ru.planeta.model.commondb.CampaignFaq
import ru.planeta.web.controllers.Urls
import javax.validation.Valid

@RestController
class CampaignFaqController(private val campaignFaqService: CampaignFaqService,
                            private val messageSource: MessageSource) {

    @RequestMapping(Urls.Faq.CAMPAIGN_FAQ_LIST)
    fun campaignFaqList(@RequestParam campaignId: Long,
                        @RequestParam offset: Int, @RequestParam limit: Int): List<CampaignFaq> {
        return campaignFaqService.selectCampaignFaqListByCampaign(campaignId, offset, limit)
    }

    @RequestMapping(Urls.Faq.CAMPAIGN_FAQ_EDIT)
    fun campaignFaqEdit(@Valid @RequestBody campaignFaq: CampaignFaq, result: BindingResult): ActionStatus<CampaignFaq> {
        return if (result.hasErrors()) {
            ActionStatus.createErrorStatus(result, messageSource)
        } else {
            campaignFaqService.updateOrCreateCampaignFaq(myProfileId(), campaignFaq)
            ActionStatus.createSuccessStatus(campaignFaq)
        }
    }

    @PostMapping(Urls.Faq.CAMPAIGN_FAQ_RESORT)
    fun campaignFaqResort(@RequestParam idFrom: Int,
                          @RequestParam idTo: Int,
                          @RequestParam campaignId: Long): ActionStatus<CampaignFaq> {
        campaignFaqService.resort(myProfileId(), idFrom.toLong(), idTo.toLong(), campaignId)
        return ActionStatus.createSuccessStatus()
    }

    @RequestMapping(Urls.Faq.CAMPAIGN_FAQ_DELETE)
    fun campaignFaqDelete(@RequestBody campaignFaq: CampaignFaq): ActionStatus<*> {
        campaignFaqService.deleteCampaignFaq(myProfileId(), campaignFaq.campaignId ?: -147, campaignFaq.campaignFaqId ?: -147)
        return ActionStatus.createSuccessStatus<Any>()
    }
}




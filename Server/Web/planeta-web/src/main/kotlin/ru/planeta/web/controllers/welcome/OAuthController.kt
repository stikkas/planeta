package ru.planeta.web.controllers.welcome

import org.apache.commons.lang3.StringUtils
import org.apache.log4j.Logger
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.servlet.ModelAndView
import ru.planeta.api.exceptions.NotFoundException
import ru.planeta.api.helper.ProjectService
import ru.planeta.api.model.ExternalUserInfo
import ru.planeta.api.model.UserAuthorizationInfo
import ru.planeta.api.model.UserCredentialsInfo
import ru.planeta.api.service.profile.AuthorizationService
import ru.planeta.api.service.profile.ProfileService
import ru.planeta.api.service.registration.RegistrationService
import ru.planeta.api.service.social.OAuthClientService
import ru.planeta.api.utils.ValidateUtils
import ru.planeta.api.web.authentication.userAuthorizationInfo
import ru.planeta.api.web.controllers.services.BaseControllerService
import ru.planeta.api.web.controllers.services.ControllerAutoLoginWrapService
import ru.planeta.api.web.utils.SessionUtils
import ru.planeta.commons.web.WebUtils
import ru.planeta.model.enums.ProjectType
import ru.planeta.model.stat.RefererStatType
import ru.planeta.web.controllers.Actions
import ru.planeta.web.controllers.Urls
import ru.planeta.web.controllers.services.AuthorizationControllerService
import java.net.URL
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

@Controller
class OAuthController(private val registrationService: RegistrationService,
                      private val services: List<OAuthClientService>,
                      private val projectService: ProjectService,
                      private val profileService: ProfileService,
                      private val authorizationService: AuthorizationService,
                      private val baseControllerService: BaseControllerService,
                      private val authorizationControllerService: AuthorizationControllerService,
                      private val controllerAutoLoginWrapService: ControllerAutoLoginWrapService) {

    private val logger = Logger.getLogger(OAuthController::class.java)

    private fun getService(controllerUrl: String): OAuthClientService? {
        return services.firstOrNull { it.isMyUrl(controllerUrl) }
    }

    private fun getRedirectUrl(controllerUrl: String): String? {
        val service = getService(controllerUrl) ?: return null
        return service.getRedirectUrl(projectService.getUrl(ProjectType.MAIN, controllerUrl))
    }

    private fun getRedirectUrlByCode(code: String): String? {
        return getRedirectUrl("/welcome/$code.html")
    }

    private fun redirectAfterAuthorization(userInfo: ExternalUserInfo, userAuthorizationInfo: UserAuthorizationInfo?, request: HttpServletRequest, response: HttpServletResponse): ModelAndView {
        if (userAuthorizationInfo == null) {
            SessionUtils.setUserCredentials(userInfo)
            if (userInfo.hasEmail()) {
                val selectedInfo = authorizationService.getUserInfoByUsername(userInfo.email ?: "")
                        ?: return accountMergeOauth(userInfo.email, request, response)
                val credentials = userInfo.createUserCredentials(selectedInfo.profile.profileId)
                authorizationService.addCredentials(credentials)
                return loginBySession(null, null, request, response)
            }
            return baseControllerService.createRedirectModelAndView(Urls.ACCOUNT_MERGE_OAUTH)

        } else {
            return autoLoginAndCasRedirectViewFromOAuth(userAuthorizationInfo.username, null, request, response)
        }
    }

    private fun tryOAuthorizeByCode(oAuthClientService: OAuthClientService, code: String, redirectUrl: String,
                                    request: HttpServletRequest, response: HttpServletResponse): ModelAndView {
        try {
            val credentialsInfo = oAuthClientService.authorizeExternal(code, redirectUrl)

            if (credentialsInfo == null) {
                logger.error("Cannot parse OAuth info with " + oAuthClientService.javaClass)
                return authorizationControllerService.createErrorModelAndView("registration.error.unexpected")
            }

            if (credentialsInfo.registrationData != null) {
                if (ValidateUtils.isContainNotValidString(credentialsInfo.firstName)) {
                    credentialsInfo.firstName = credentialsInfo.externalAuthentication.username
                }
                if (ValidateUtils.isContainNotValidString(credentialsInfo.lastName)) {
                    credentialsInfo.lastName = credentialsInfo.externalAuthentication.username
                }
            }

            if (StringUtils.isEmpty(credentialsInfo.photoUrl)) {
                credentialsInfo.photoUrl = profileService.generateRandomAvatarUrl();
            }


            val userAuthorizationInfo = authorizationService.getUserInfo(credentialsInfo.externalAuthentication)
            return redirectAfterAuthorization(credentialsInfo, userAuthorizationInfo, request, response)
        } catch (ex: Exception) {
            logger.error("Cannot authorize user", ex)
            return authorizationControllerService.createErrorModelAndView("registration.error.unexpected")
        }

    }

    @GetMapping(Urls.ACCOUNT_MERGE_OAUTH)
    fun accountMerge(): ModelAndView {
        val externalUserInfo = SessionUtils.externalUserInfo
                ?: return authorizationControllerService.createErrorModelAndView("registration.error.unexpected")
        val modelAndView = baseControllerService.createDefaultModelAndView(Actions.ACCOUNT_MERGE, ProjectType.MAIN)
        val credentialsInfo = UserCredentialsInfo()
        credentialsInfo.registrationData = externalUserInfo.registrationData
        credentialsInfo.email = externalUserInfo.email
        credentialsInfo.externalAuthentication = externalUserInfo.externalAuthentication
        modelAndView.addObject("serviceName", credentialsInfo.credentialType)
        modelAndView.addObject("credentialsInfo", credentialsInfo)
        return modelAndView

    }


    @PostMapping(Urls.ACCOUNT_MERGE_OAUTH)
    fun accountMergeOauth(@RequestParam(value = "email", required = true) email: String?,
                          request: HttpServletRequest,
                          response: HttpServletResponse): ModelAndView {

        val userCredentialsInfo = SessionUtils.externalUserInfo
                ?: throw NotFoundException(UserCredentialsInfo::class.java)

        val userAuthorizationInfo = registrationService.registerFromSocialNetworkWithEmail(email,
                userCredentialsInfo.externalAuthentication,
                userCredentialsInfo.registrationData,
                baseControllerService.getRequestStat(request, response, RefererStatType.REGISTRATION, ProjectType.MAIN))
        return autoLoginAndCasRedirectViewFromOAuth(userAuthorizationInfo.username, null, request, response)
    }

    @GetMapping(Urls.OAuth.YANDEX, Urls.OAuth.FACEBOOK, Urls.OAuth.ODNOKLASSNIKI, Urls.OAuth.VKONTAKTE, Urls.OAuth.MAILRU)
    fun oAuth(request: HttpServletRequest, response: HttpServletResponse): ModelAndView {

        val requestMappingValue = request.servletPath
        val code = request.getParameter("code")
        val redirectUrl = WebUtils.appendProtocolIfNecessary(request.requestURL.toString(), true)
        val service = getService(requestMappingValue) ?: throw NotFoundException("OAuth controller not found")
        return if (code == null || code.isBlank()) {
            authorizationControllerService.createErrorModelAndView("registration.error.unexpected")
        } else tryOAuthorizeByCode(service, code, redirectUrl, request, response)
    }

    @GetMapping(Urls.OAuth.SAVE_REDIRECT_URL)
    fun saveRedirect(@RequestParam referrerUrl: String,
                     @RequestParam(value = "provider") providerAlias: String): ModelAndView {
        val redirectUrl = getRedirectUrlByCode(providerAlias)
        try {
            if (redirectUrl != null && redirectUrl.isNotBlank()) {
                val url = URL(redirectUrl).toString()
                SessionUtils.redirectAfterOAuth = referrerUrl
                return baseControllerService.createRedirectModelAndView(url)
            }

            return authorizationControllerService.createErrorModelAndView("registration.error.unexpected")
        } catch (ex: Exception) {
            logger.error("Cannot register user", ex)
            return authorizationControllerService.createErrorModelAndView("registration.error.unexpected")
        }

    }

    @GetMapping(Urls.OAuth.LOGIN_BY_SESSION)
    // DANGER !!! repair account merge
    fun loginBySession(@RequestParam(required = false) redirectUrl: String?,
                       @RequestParam(required = false) email: String?,
                       request: HttpServletRequest,
                       response: HttpServletResponse): ModelAndView {
        val credentials = SessionUtils.externalUserInfo ?: return getErrorRedirect(redirectUrl)
        val url = redirectUrl ?: SessionUtils.redirectAfterOAuth
        val userAuthorizationInfo: UserAuthorizationInfo? =
                if (email != null && email.isNotBlank())
                    authorizationService.getUserInfoByUsername(email)
                else
                    authorizationService.getUserInfo(credentials.externalAuthentication)

        return if (userAuthorizationInfo == null) getErrorRedirect(url) else autoLoginAndCasRedirectViewFromOAuth(userAuthorizationInfo.username, url, request, response)
    }

    private fun getErrorRedirect(redirectUrl: String?): ModelAndView {
        return baseControllerService.createRedirectModelAndView(redirectUrl ?: "/")
    }

    @PostMapping(Urls.ACCOUNT_MERGE)
    fun accountMerge(@RequestParam(value = "email", required = true) email: String,
                     request: HttpServletRequest,
                     response: HttpServletResponse): ModelAndView {
        val userAuthorizationInfo = registrationService.addEmailToExternalUser(email, userAuthorizationInfo())
        return if (userAuthorizationInfo != null) {
            autoLoginAndCasRedirectViewFromOAuth(userAuthorizationInfo.username, null, request, response)
        } else {
            authorizationControllerService.createErrorModelAndView("registration.error.unexpected")
        }
    }

    private fun redirectUrlFromOAuth(redirectUrl: String?): String {
        return if (redirectUrl.isNullOrBlank()) SessionUtils.redirectAfterOAuth
                ?: redirectUrl as String else redirectUrl as String
    }

    private fun autoLoginAndCasRedirectViewFromOAuth(username: String,
                                                     redirectUrl: String?,
                                                     request: HttpServletRequest,
                                                     response: HttpServletResponse): ModelAndView =
            controllerAutoLoginWrapService.autoLoginAndCasRedirectView(username, redirectUrlFromOAuth(redirectUrl), request, response)
}

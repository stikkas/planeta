package ru.planeta.eva.web.controllers

import org.springframework.validation.BindingResult
import org.springframework.web.bind.annotation.*
import ru.planeta.api.exceptions.PermissionException
import ru.planeta.api.service.security.PermissionService
import ru.planeta.api.web.authentication.myProfileId
import ru.planeta.api.web.controllers.services.BaseControllerService
import ru.planeta.dto.ProfileSitesDTO
import ru.planeta.eva.api.web.ActionStatus
import ru.planeta.eva.api.web.ProfileUrls
import ru.planeta.eva.api.web.Urls
import ru.planeta.eva.web.services.ProfileSitesService
import ru.planeta.model.enums.ProfileType
import javax.validation.Valid

@RestController
class ProfileSitesController(private val profileSitesService: ProfileSitesService,
                             private val permissionService: PermissionService,
                             private val baseControllerService: BaseControllerService) {

    /**
     * Мои сайты (контакты)
     */
    @GetMapping(ProfileUrls.MY_SITES)
    fun getMySites(): ActionStatus {
        return ActionStatus(result = getSitesForProfile(myProfileId()))
    }

    /**
     * Сайты (контакты) любого пользователя
     */
    @GetMapping(Urls.PROFILE_SITES)
    fun getProfileSites(@RequestParam profileId: Long): ProfileSitesDTO? {
        return getSitesForProfile(profileId)
    }

    private fun getSitesForProfile(profileId: Long): ProfileSitesDTO? {
        val profile = baseControllerService.profileService.getProfileSafe(profileId)

        val id = if (profile.profileType == ProfileType.HIDDEN_GROUP)
            profile.creatorProfileId
        else
            profile.profileId

        return profileSitesService.selectProfileSites(id)
    }

    @PostMapping(ProfileUrls.SAVE_MY_SITES)
    fun saveProfileSites(@Valid profileSites: ProfileSitesDTO, result: BindingResult): ActionStatus {
        if (result.hasErrors()) {
            return ActionStatus(false, result.allErrors)
        }

        if (!permissionService.isAdmin(myProfileId(), profileSites.profileId)) {
            throw PermissionException()
        }

        profileSitesService.saveProfileSites(profileSites)
        return ActionStatus(result = true)
    }
}

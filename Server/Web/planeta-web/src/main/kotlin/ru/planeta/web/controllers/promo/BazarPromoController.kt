package ru.planeta.web.controllers.promo

import org.apache.commons.lang3.StringUtils
import org.apache.log4j.Logger
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.servlet.ModelAndView
import ru.planeta.api.Utils.nvl
import ru.planeta.api.exceptions.NotFoundException
import ru.planeta.api.model.campaign.PromoCampaignDTO
import ru.planeta.api.search.SearchCampaigns
import ru.planeta.api.search.SearchService
import ru.planeta.api.search.SortOrder
import ru.planeta.api.service.campaign.CampaignService
import ru.planeta.api.service.configurations.ConfigurationService
import ru.planeta.dao.commondb.CampaignStatsDAO
import ru.planeta.model.common.campaign.enums.CampaignStatus
import ru.planeta.web.controllers.Actions
import ru.planeta.web.controllers.Urls
import ru.planeta.web.controllers.services.MainBaseControllerService
import java.math.BigDecimal
import java.util.*

@Controller
class BazarPromoController(private val campaignStatsDAO: CampaignStatsDAO,
                           private val mainBaseControllerService: MainBaseControllerService,
                           private val configurationService: ConfigurationService,
                           private val campaignService: CampaignService,
                           private val searchService: SearchService) {

    private val logger = Logger.getLogger(BazarPromoController::class.java)

    @GetMapping(Urls.PROMO_BAZAR_2016)
    fun bazar2016Promo(): ModelAndView {
        val tagMnemonic = "BAZAR2016"

        val mav = mainBaseControllerService.defaultModelAndView(Actions.BAZAR2016)

        var partnersBlock: String? = StringUtils.EMPTY
        try {
            partnersBlock = nvl(configurationService.getStringConfig("promo.bazar.partners.2016"), "")
        } catch (ex: NotFoundException) {
            logger.error(ex)
        }

        mav.addObject("partnersBlock", partnersBlock)

        var newsBlock: String? = StringUtils.EMPTY
        var modalsBlock: String? = StringUtils.EMPTY
        try {
            newsBlock = nvl(configurationService.getStringConfig("promo.bazar.news.2016"), "")
            modalsBlock = nvl(configurationService.getStringConfig("promo.bazar.news.modals.2016"), "")
        } catch (ex: NotFoundException) {
            logger.error(ex)
        }

        mav.addObject("newsBlock", newsBlock)
        mav.addObject("modalsBlock", modalsBlock)


        val campaignsFilter = SearchCampaigns()
        val campaignTag = campaignService.getCampaignTagByMnemonic(tagMnemonic)
        if (campaignTag != null) {
            campaignsFilter.campaignTags = mutableListOf(campaignTag)
        }
        campaignsFilter.sortOrderList = listOf(SortOrder.SORT_BY_MEDIAN_PRICE)
        campaignsFilter.campaignStatus = EnumSet.of(CampaignStatus.ACTIVE, CampaignStatus.FINISHED)
        campaignsFilter.limit = 1000
        val bazarProjects = searchService.searchForCampaigns(campaignsFilter).searchResultRecords

        var totalAmount = BigDecimal.ZERO
        bazarProjects?.forEach {
            totalAmount = totalAmount.add(it.collectedAmount)
        }
        mav.addObject("totalAmount", totalAmount)

        return mav
    }

    @GetMapping(Urls.PROMO_BAZAR)
    fun bazarPromo(): ModelAndView {
        val c = campaignService.getCampaignSafe(configurationService.getConfigurationByKey("promo.bazar.id")?.intValue?.toLong()
                ?: -147)
        val sponsorAlias = "BAZAR"
        val bazarProjects = campaignService.getCampaignBySponsorAlias(sponsorAlias, 0, 0).toMutableList()
        org.apache.commons.collections4.CollectionUtils.selectRejected(bazarProjects,
                { bazarCampaign -> bazarCampaign.campaignId == c.campaignId })

        bazarProjects.shuffle()

        val list = bazarProjects.map { PromoCampaignDTO.createFromCampaign(it) }

        return mainBaseControllerService.defaultModelAndView(Actions.BAZAR).addAllObjects(
                mapOf("partnersBlock" to nvl(configurationService.getStringConfig("promo.bazar.partners"), ""),
                        "newsBlock" to nvl(configurationService.getStringConfig("promo.bazar.news"), ""),
                        "bazarCampaign" to PromoCampaignDTO.createFromCampaign(c),
                        "now" to Date(),
                        "totalAmount" to campaignStatsDAO.getAggregateCampaignStatBySponsorAlias(sponsorAlias).collectedAmount,
                        "campaings" to list))
    }
}

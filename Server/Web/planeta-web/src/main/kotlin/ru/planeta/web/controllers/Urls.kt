package ru.planeta.web.controllers

/**
 * Class containing all urls used in webapp
 *
 * @author atropnikov
 * Class URLS
 */
object Urls {

    const val GET_CUSTOM_HTML = "/api/public/custom-html.json"
    const val SAVE_CAMPAIGN_DRAFT = "/api/public/save-campaign-draft.json"

    const val INVOICE_HTML = "/api/private/invoice.html"
    const val INVOICE_PUBLIC = "/welcome/invoice/pdf.html"
    const val BIBLIO_CERT_HTML = "/api/private/biblio/cert.html"
    const val CAMPAIGN_CONTRACT_HTML = "/api/private/campaign/contract.html"
    const val CAMPAIGN_CONTRACT_PDF_PUBLIC = "/welcome/campaign/contract-pdf.html"
    const val CAMPAIGN_NEW_NKO_CONTRACT_HTML = "/api/private/campaign/new-nko-contract.html"
    const val CAMPAIGN_NEW_NKO_CONTRACT_PDF_PUBLIC = "/welcome/campaign/new-nko-contract-pdf.html"
    const val BIBLIO_CERT_PUBLIC = "/api/welcome/biblio/cert-pdf.html"
    // <editor-fold defaultstate="collapsed" desc="Welcome zone urls">
    const val ACCOUNT_MERGE_OAUTH = "/welcome/account-merge.html"
    const val ACCOUNT_MERGE = "/welcome/account-merge-email.html"

    const val CONFIRM = "/welcome/confirm.html"
    const val CONFIRM_RECOVER = "/welcome/confirm-recover.html"
    const val RECOVER_PASSWORD = "/welcome/recover-password.json"

    const val PROMO_BONUSES = "/welcome/bonuses.html"
    const val PROMO_CLEVER = "/welcome/clever.html"
    const val PROMO_BAZAR = "/welcome/bazar.html"
    const val PROMO_BAZAR_2016 = "/welcome/bazar2016.html"
    const val NPS = "/welcome/nps.html"
    const val GET_BONUS = "/api/get-bonus.json"
    const val CREATE_BONUS_ORDER = "/api/bonuses/create-order.json"
    const val ONE_HUNDRED_MILLIONS = "/welcome/one-hundred-millions.html"

    const val ROTATOR_RELOAD = "/api/admin/banner-reload.html"
    const val GET_RANDOM_BANNER = "/api/util/random-banner.html"
    const val PREVIEW_BANNER = "/api/util/preview-banner.html"
    const val GET_RANDOM_BANNER_JSON = "/api/util/random-banner-json.json"
    const val GET_PROMO_BANNERS = "/api/util/get-promo-banners"

    const val PRINT_ORDER = "/profile/print-order.html"

    // Common static pages
    const val AGREEMENT = "/welcome/agreement.html"
    const val SHARE_RETURN = "/welcome/share-return.html"
    const val ABOUT_NEW = "/about.html"
    const val ABOUT_NEW_CONTACTS = "/contacts.html"
    const val ABOUT_NEW_ADVERTISING = "/advertising.html"
    const val ABOUT_NEW_ADVERTISING_SEND_MAIL = "/api/advertising-send-mail.json"
    const val ABOUT_NEW_LOGO = "/logo.html"
    const val ABOUT_NEW_PARTNERS = "/partners.html"
    const val PROJECTS_AGREEMENT = "/welcome/projects-agreement.html"
    const val PRIVATE_INFO = "/welcome/private-info.html"
    const val SCHOOL_CERT = "/welcome/school-cert.html"
    const val SCHOOL_CERT_RENDER = "/welcome/school-cert-render.html"

    const val QUICKSTART_MUSIC = "/quickstart/music"
    const val QUICKSTART_MOVIE = "/quickstart/movie"
    const val QUICKSTART_BOOK = "/quickstart/book"
    const val QUICKSTART_BUSINESS = "/quickstart/business"
    const val QUICKSTART_GET_BOOK = "/welcome/get-quickstart-book.json"
    const val PROMOTION_1 = "/welcome/promotion_1.html"
    const val PROMOTION_2 = "/welcome/promotion_2.html"
    const val PROMOTION_3 = "/welcome/promotion_3.html"
    const val PROMOTION_4 = "/welcome/promotion_4.html"
    const val PROMOTION_PACK = "/welcome/promotion_pack.html"

    //crowd-school
    const val UNIVERSITY = "/welcome/university.html"

    const val BIRTHDAY = "/welcome/4-years.html"
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Member zone, common pages urls">
    const val ROOT = "/"
    const val INDEX = "/index.html"
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Users controller urls">
    const val USER_SAVE_PASSWORD = "/profile/user-save-password.json"

    const val PROFILE_SAVE_GENERAL_SETTINGS = "/api/profile/profile-save-general-settings"

    const val PROFILE_UNSUBSCRIBE = "/api/profile/unsubscribe-mailing.json"
    // </editor-fold>

    const val CAMPAIGN_STAT_GENERAL = "/campaigns/general-stats.json"

    //start
    const val CAMPAIGN_DONATE_WITH_SHARE = "/campaigns/{campaignId}/donate/{shareId}.html{params}"

    const val GET_SHARE_INFO = "/api/public/campaign/detailed-share.json"

    const val MY_RETAINED_CAMPAIGNS = "/campaigns/get-my-retained-campaigns.json"

    const val FOR_CROUDSOURSING_RU = "/for-crowdsourcing-ru.xml"

    /**
     * "/campaign/contacts-send-author-feedback.json"
     * "/campaign/contacts-send-curator-feedback.json"
     */
    // public static final String CAMPAIGN_CONTACTS_SEND_TARGET_FEEDBACK = "/campaign/contacts-send-{target}-feedback.json";
    const val CAMPAIGN_CONTACTS_SEND_CURATOR_FEEDBACK = "/campaign/contacts-send-curator-feedback.json"
    const val CAMPAIGN_CONTACTS_SEND_AUTHOR_FEEDBACK = "/campaign/contacts-send-author-feedback.json"

    // Campaign Editor Activites
    const val CAMPAING_EDIT_TRACK = "/api/profile/campaign-edit-track.json"

    const val OFFLINE = "/offline.js"

    const val OPEN_SEARCH = "/opensearch.xml"

    object OAuth {

        const val YANDEX = "/welcome/ya.html"
        const val MAILRU = "/welcome/mailru.html"
        const val ODNOKLASSNIKI = "/welcome/ok.html"
        const val FACEBOOK = "/welcome/fb.html"
        const val VKONTAKTE = "/welcome/vk.html"
        const val SAVE_REDIRECT_URL = "/welcome/save-redirect.html"
        const val LOGIN_BY_SESSION = "/welcome/session-login.html"
    }

    object Payment {

        const val INCREASE_BALANCE = "/profile/increase-balance.html"
    }

    object Admin {

        //campaign action urls
        const val CAMPAIGN_SAVE = "/admin/campaign-save.json"
        const val CAMPAIGN_CREATE_INFO_STEP = "/admin/campaign-create-info-step.json"
        const val CAMPAIGN_SAVE_EMAIL_STEP = "/admin/campaign-save-email-step.json"
        const val CAMPAIGN_SAVE_INFO_STEP = "/admin/campaign-save-info-step.json"
        const val CAMPAIGN_SAVE_PROCEED_STEP = "/admin/campaign-save-proceed-step.json"
        const val CAMPAIGN_SAVE_DURATION_STEP = "/admin/campaign-save-duration-step.json"
        const val CAMPAIGN_SAVE_PRICE_STEP = "/admin/campaign-save-price-step.json"
        const val CAMPAIGN_SAVE_VIDEO_STEP = "/admin/campaign-save-video-step.json"
        const val CAMPAIGN_SAVE_DESCRIPTION_STEP = "/admin/campaign-save-description-step.json"
        const val CAMPAIGN_SAVE_REWARD_STEP = "/admin/campaign-save-reward-step.json"
        const val CAMPAIGN_SAVE_COUNTERPARTY_STEP = "/admin/campaign-save-counterparty-step.json"
        const val CAMPAIGN_SAVE_FINAL_STEP = "/admin/campaign-save-final-step.json"
        const val DRAFT_CAMPAIGN = "/admin/draft-campaign.json"
        const val DRAFT_CAMPAIGN_UNDO_CHANGES = "/admin/draft-campaign-undo-changes.json"
        const val CREATE_CAMPAIGN_BY_ADMIN = "/admin/create-campaign-by-admin.json"
        const val CAMPAIGN_START = "/admin/campaign-start.json"
        const val CAMPAIGN_PAUSE = "/admin/campaign-pause.json"
        const val CAMPAIGN_REMOVE = "/admin/campaign-remove.json"
        //share/reward action urls
        const val CAMPAIGN_VALIDATE_SHARE = "/admin/campaign-validate-share.json"
        const val CAMPAIGN_VALIDATE_REMOVE_SHARE = "/admin/campaign-validate-remove-share.json"
        //order action urls
        const val BATCH_ORDERS_CANCEL = "/admin/batch-orders-cancel.json"
        const val BATCH_ORDERS_COMPLETE = "/admin/batch-orders-complete.json"
        const val UPDATE_ORDER_STATE = "/admin/update-order-state.json"
        const val CAMPAIGN_ORDERS_LIST = "/admin/campaign-orders-list.json"
        const val CAMPAIGN_STATS_LIST = "/admin/campaign-stats-list.json"
        const val CAMPAIGN_TOTAL_STATS = "/admin/campaign-total-stats.json"
        const val CAMPAIGN_SHARES_SALE_LIST = "/admin/campaign-shares-sale-list.json"
        const val CAMPAIGN_STATS_REPORT = "/admin/campaign-stats-report.html"
        const val SHARES_SALE_STATS_REPORT = "/admin/campaign-shares-stats-report.html"
        const val ORDERS_REPORT = "/admin/campaign-orders-report.html"
        const val ORDERS_REPORT_TO_EMAIL = "/admin/campaign-orders-report-to-email.json"

        const val CAMPAIGN_ADD_CONTACT = "/campaign/add-contact.json"
        const val CAMPAIGN_CONTACTS_DELETE = "/campaign/contacts-deleteByProfileId.json"
        const val CAMPAIGN_POSSIBLE_CONTACTS = "/api/profile/campaign/possible-contacts-list.json"
        const val UPDATE_BUYER_ANSWER = "/admin/update-buyer-answer.json"
        const val GET_SHARE_DELIVERY_SETTINGS = "/admin/get-share-delivery-settings.json"
        const val UPDATE_ORDER_DELIVERY = "/admin/update-order-delivery.json"

        const val CAMPAIGN_ADD_CURATOR = "/campaign/add-curator.json"
        const val CAMPAIGN_CURATORS_DELETE = "/campaign/curator-deleteByProfileId.json"

        const val CAMPAIGN_CHANGE_BACKGROUND = "/api/change-campaign-bg-image.json"
        const val TOGGLE_DRAFT_VISIBLE = "/api/campaign/campaign-draft-toggle.json"

        const val CAMPAIGN_TARGETING = "/admin/campaign/campaign-targeting.json"
        const val CAMPAIGN_TARGETING_PUBLIC = "/api/public/campaign-targeting"
    }

    object Faq {

        const val CAMPAIGN_FAQ_LIST = "/api/public/campaign-faq-list.json"
        const val CAMPAIGN_FAQ_EDIT = "/api/profile/campaign-faq-edit.json"
        const val CAMPAIGN_FAQ_RESORT = "/api/profile/campaign-faq-resort.json"
        const val CAMPAIGN_FAQ_DELETE = "/api/profile/campaign-faq-deleteByProfileId.json"
    }

    object UserFeedback {
        const val CHECK_USER_FEEDBACK_EXISTS = "/api/profile/check-user-feedback-exists.json"
        const val CHECK_CAMPAIGN_USER_FEEDBACK_EXISTS = "/api/profile/check-campaign-user-feedback-exists.json"
        const val ADD_USER_FEEDBACK = "/api/profile/add-user-feedback.json"
    }

    object Interactive {
        const val TRACK_CAMPAIGN_INTERACTIVE_STEP_START = "/api/campaign/track-campaign-interactive-step-start.json"
        const val TRACK_CAMPAIGN_INTERACTIVE_STEP_NAME = "/api/campaign/track-campaign-interactive-step-name.json"
        const val TRACK_CAMPAIGN_INTERACTIVE_STEP_EMAIL = "/api/campaign/track-campaign-interactive-step-email.json"
        const val TRACK_CAMPAIGN_INTERACTIVE_STEP_PROCEED = "/api/campaign/track-campaign-interactive-step-proceed.json"
        const val TRACK_CAMPAIGN_INTERACTIVE_STEP_INFO = "/api/campaign/track-campaign-interactive-step-info.json"
        const val TRACK_CAMPAIGN_INTERACTIVE_STEP_DURATION = "/api/campaign/track-campaign-interactive-step-duration.json"
        const val TRACK_CAMPAIGN_INTERACTIVE_STEP_PRICE = "/api/campaign/track-campaign-interactive-step-price.json"
        const val TRACK_CAMPAIGN_INTERACTIVE_STEP_VIDEO = "/api/campaign/track-campaign-interactive-step-video.json"
        const val TRACK_CAMPAIGN_INTERACTIVE_STEP_DESCRIPTION = "/api/campaign/track-campaign-interactive-step-description.json"
        const val TRACK_CAMPAIGN_INTERACTIVE_STEP_REWARD = "/api/campaign/track-campaign-interactive-step-reward.json"
        const val TRACK_CAMPAIGN_INTERACTIVE_STEP_COUNTERPARTY = "/api/campaign/track-campaign-interactive-step-counterparty.json"
        const val TRACK_CAMPAIGN_INTERACTIVE_STEP_CHECK = "/api/campaign/track-campaign-interactive-step-check.json"
        const val TRACK_CAMPAIGN_INTERACTIVE_STEP_FINAL = "/api/campaign/track-campaign-interactive-step-final.json"
        const val TRACK_CAMPAIGN_INTERACTIVE_STEP_FINAL_DRAFT = "/api/campaign/track-campaign-interactive-step-final-draft.json"

        const val GET_INTERACTIVE_EDITOR_DATA = "/api/campaign/get-interactive-editor-data.json"
        const val CLEAR_INTERACTIVE_EDITOR_DATA = "/api/campaign/clear-interactive-editor-data.json"
        const val SAVE_INTERACTIVE_EDITOR_DATA_FORMS_SETTINGS = "/api/campaign/save-interactive-editor-data-form-settings.json"
        const val SET_AGREE_RECEIVE_LESSONS_MATERIALS_ON_EMAIL = "/api/campaign/set-agree-receive-lessons-materials-on-email.json"
    }

    object Welcome {
        const val SHARES_LIST = "/api/welcome/shares-list"
        const val MAIN_SLIDER_STATS = "/api/welcome/main-slider-stats"
    }

    const val FOR_IVI_RU = "/api/public/ivi-stat.json"
}

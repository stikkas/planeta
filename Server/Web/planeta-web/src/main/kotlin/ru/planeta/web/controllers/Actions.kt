package ru.planeta.web.controllers

import ru.planeta.api.web.controllers.IAction

enum class Actions private constructor(prefix: ActionPrefixes, private val viewName: String) : IAction {

    INDEX(ActionPrefixes.MEMBER, "index"),

    PAYMENT_SUCCESS(ActionPrefixes.WELCOME, "payment-success"),
    PAYMENT_FAIL(ActionPrefixes.COMMON_INCLUDES, "payment-fail"),
    ACCOUNT_MERGE(ActionPrefixes.WELCOME, "merge-account"),
    SITE_MAP(ActionPrefixes.WELCOME, "sitemap"),
    OPEN_SEARCH(ActionPrefixes.WELCOME, "opensearch"),
    SCHOOL_CERT_RENDER(ActionPrefixes.WELCOME, "school-cert-render"),
    SCHOOL_CERT(ActionPrefixes.WELCOME, "school-cert"),

    BANNER(ActionPrefixes.WELCOME, "banner"),

    BONUSES(ActionPrefixes.WELCOME, "bonuses"),
    CLEVER(ActionPrefixes.WELCOME, "clever"),
    BAZAR(ActionPrefixes.WELCOME, "bazar"),
    BAZAR2016(ActionPrefixes.WELCOME, "bazar2016"),
    NPS(ActionPrefixes.WELCOME, "nps"),
    FINUNIVERSE(ActionPrefixes.WELCOME, "finuniverse"),
    AGREEMENT(ActionPrefixes.STATIC, "agreement"),
    SHARE_RETURN(ActionPrefixes.STATIC, "share-return"),
    ABOUT_NEW(ActionPrefixes.STATIC, "about-new"),
    ABOUT_NEW_CONTACTS(ActionPrefixes.STATIC, "about-contacts"),
    ABOUT_NEW_ADVERTISING(ActionPrefixes.STATIC, "about-advertising"),
    ABOUT_NEW_LOGO(ActionPrefixes.STATIC, "about-logo"),
    ABOUT_NEW_PARTNERS(ActionPrefixes.STATIC, "about-partners"),
    OLD_BROWSER(ActionPrefixes.STATIC, "old-browser"),
    PROJECTS_AGREEMENT(ActionPrefixes.STATIC, "projects-agreement"),
    PRIVATE_INFO(ActionPrefixes.STATIC, "private-info"),
    BIRTHDAY(ActionPrefixes.STATIC, "birthday"),
    QUICKSTART_MUSIC(ActionPrefixes.STATIC, "quickstart-new/quickstart-music"),
    QUICKSTART_MOVIE(ActionPrefixes.STATIC, "quickstart-new/quickstart-movie"),
    QUICKSTART_BOOK(ActionPrefixes.STATIC, "quickstart-new/quickstart-book"),
    QUICKSTART_BUSINESS(ActionPrefixes.STATIC, "quickstart-new/quickstart-business"),
    PROMOTION_1(ActionPrefixes.STATIC, "promotion/promotion_1"),
    PROMOTION_2(ActionPrefixes.STATIC, "promotion/promotion_2"),
    PROMOTION_3(ActionPrefixes.STATIC, "promotion/promotion_3"),
    PROMOTION_4(ActionPrefixes.STATIC, "promotion/promotion_4"),
    PROMOTION_PACK(ActionPrefixes.STATIC, "promotion/promotion_pack"),

    PRINT_SHARE_ORDER(ActionPrefixes.MEMBER_PRINT, "share-order"),
    PRINT_PRODUCT_ORDER(ActionPrefixes.MEMBER_PRINT, "product-order"),
    PRINT_BONUS_ORDER(ActionPrefixes.MEMBER_PRINT, "bonus-order"),
    PRINT_UNSUPPORTED(ActionPrefixes.MEMBER_PRINT, "unsupported"),
    PRINT_INVOICE(ActionPrefixes.MEMBER_PRINT, "invoice"),
    PRINT_BIBLIO_INVOICE(ActionPrefixes.MEMBER_PRINT, "biblio-invoice"),
    PRINT_BIBLIO_CERT(ActionPrefixes.MEMBER_PRINT, "biblio-cert"),
    PRINT_CAMPAIGN_CONTRACT_INDIVIDUAL(ActionPrefixes.MEMBER_PRINT, "campaign-contract-individual"),
    PRINT_CAMPAIGN_CONTRACT_INDIVIDUAL_ENTREPRENEUR(ActionPrefixes.MEMBER_PRINT, "campaign-contract-individual_entrepreneur"),
    PRINT_CAMPAIGN_CONTRACT_LEGAL_ENTITY(ActionPrefixes.MEMBER_PRINT, "campaign-contract-legal-entity"),
    PRINT_CAMPAIGN_CONTRACT_CHARITY(ActionPrefixes.MEMBER_PRINT, "campaign-contract-charity"),
    PRINT_CAMPAIGN_CONTRACT_NOT_SUPPORTED(ActionPrefixes.MEMBER_PRINT, "campaign-contract-not-supported"),
    PRINT_NEW_NKO_CONTRACT(ActionPrefixes.MEMBER_PRINT, "new-nko-contract"),

    ADMIN_CAMPAIGN_SHARES_STATS_REPORT(ActionPrefixes.CAMPAIGNS_ADMIN, "campaign-shares-report"),
    SHARE_PAYMENT_SUCCESS(ActionPrefixes.CAMPAIGNS, "payment-success"),
    INVESTING_PAYMENT_SUCCES(ActionPrefixes.INVESTING, "investing-success"),
    INVESTING_PAYMENT_SUCCES_WITHOUT_MODERATION(ActionPrefixes.INVESTING, "investing-success2");

    private val prefix = prefix.toString()

    val text: String
        get() = prefix + viewName


    override val path: String
        get() = text

    override val actionName: String
        get() = viewName

    override fun toString(): String = text
}

package ru.planeta.web.controllers.campaigns.admin

import org.springframework.validation.BindingResult
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import ru.planeta.api.model.ActionStatus
import ru.planeta.api.service.billing.order.OrderService
import ru.planeta.api.service.campaign.CampaignService
import ru.planeta.api.web.authentication.myProfileId
import ru.planeta.api.web.controllers.services.BaseControllerService
import ru.planeta.model.common.campaign.DraftShareEditDetails
import ru.planeta.web.controllers.Urls
import javax.annotation.PostConstruct
import javax.validation.Valid

@RestController
class ShareEditController(private val baseControllerService: BaseControllerService,
                          private val campaignService: CampaignService,
                          private val orderService: OrderService) {

    @PostConstruct
    fun init() {
        campaignService.setOrderService(orderService)
    }

    @PostMapping(Urls.Admin.CAMPAIGN_VALIDATE_SHARE)
    fun validateShare(@Valid shareDetails: DraftShareEditDetails, result: BindingResult): ActionStatus<DraftShareEditDetails> {
        if (result.hasErrors()) {
            return baseControllerService.createErrorStatus(result)
        }
        campaignService.saveShare(myProfileId(), shareDetails)
        return ActionStatus.createSuccessStatus(shareDetails)
    }

    @PostMapping(Urls.Admin.CAMPAIGN_VALIDATE_REMOVE_SHARE)
    fun removeShare(@RequestParam shareId: Long): ActionStatus<*> {
        campaignService.disableShare(myProfileId(), shareId)
        return ActionStatus.createSuccessStatus<Any>()
    }
}

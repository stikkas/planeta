package ru.planeta.eva.web.controllers

object Urls {
    const val ASK_MANAGER = "/api/public/ask-manager"
}

object ProfileUrls {
    const val PROFILE_SAVE_MAIN_SETTINGS = "/api/profile/profile-save-main-settings"
    const val CHANGE_SUBSCRIPTIONS = "/api/profile/save-subscriptions"
    const val PROFILE_BALANCE_OPERATIONS = "/api/profile/balance-operations"
    const val RECHARGE_BALANCE = "/api/profile/recharge-balance"
    const val UPDATE_PASSWORD = "/api/profile/update-password"
    const val CANCEL_PURCHASE = "/api/profile/cancel-purchase"
}

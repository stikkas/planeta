package ru.planeta.web.controllers.campaigns.model

import ru.planeta.model.common.campaign.Share

class Pledge {
    var image: String? = null
    var name: String? = null
    var description: String? = null
    var price: Int = 0


    constructor(share: Share) {
        image = share.imageUrl
        name = share.name
        description = share.description
        price = share.price?.toInt() ?: 0
    }
}

package ru.planeta.web.controllers.campaigns.admin

import org.apache.commons.lang3.StringUtils
import org.apache.log4j.Logger
import org.springframework.context.MessageSource
import org.springframework.validation.BindingResult
import org.springframework.web.bind.annotation.*
import ru.planeta.api.exceptions.MessageCode
import ru.planeta.api.exceptions.NotFoundException
import ru.planeta.api.exceptions.PermissionException
import ru.planeta.api.model.ActionStatus
import ru.planeta.api.search.SearchService
import ru.planeta.api.search.filters.SearchOrderFilter
import ru.planeta.api.service.billing.order.OrderService
import ru.planeta.api.service.campaign.CampaignService
import ru.planeta.api.service.campaign.CampaignStatsService
import ru.planeta.api.service.campaign.CampaignTargetingService
import ru.planeta.api.service.campaign.ModerationMessageService
import ru.planeta.api.service.configurations.ConfigurationService
import ru.planeta.api.service.profile.AuthorizationService
import ru.planeta.api.service.profile.ProfileService
import ru.planeta.api.service.security.PermissionService
import ru.planeta.api.service.statistic.StatisticsService
import ru.planeta.api.utils.ValidateUtils
import ru.planeta.api.web.authentication.hasEmail
import ru.planeta.api.web.authentication.myProfileId
import ru.planeta.api.web.controllers.services.BaseControllerService
import ru.planeta.commons.web.IpUtils
import ru.planeta.model.common.ModerationMessage
import ru.planeta.model.common.OrderInfo
import ru.planeta.model.common.campaign.*
import ru.planeta.model.common.campaign.enums.CampaignStatus
import ru.planeta.model.enums.ModerationMessageStatus
import ru.planeta.model.stat.CampaignStat
import ru.planeta.web.controllers.Urls
import java.util.*
import javax.servlet.http.HttpServletRequest
import javax.validation.Valid

@RestController
class AdminCampaignsController(
        private val statisticsService: StatisticsService,
        private val campaignStatsService: CampaignStatsService,
        private val campaignTargetingService: CampaignTargetingService,
        private val campaignService: CampaignService,
        private val permissionService: PermissionService,
        private val searchService: SearchService,
        private val orderService: OrderService,
        private val authorizationService: AuthorizationService,
        private val profileService: ProfileService,
        private val configurationService: ConfigurationService,
        private val moderationMessageService: ModerationMessageService,
        private val baseControllerService: BaseControllerService,
        private val messageSource: MessageSource) {

    private val logger = Logger.getLogger(AdminCampaignsController::class.java)

    @PostMapping(Urls.Admin.CAMPAIGN_START)
    fun campaignStart(@RequestParam campaignId: Long, req: HttpServletRequest): ActionStatus<Campaign> {
        val myProfileId = myProfileId()
        val c = campaignService.startCampaign(myProfileId, campaignId)
        logger.info("Admin id=" + myProfileId + " started campaign id=" + campaignId + " request ip=" + IpUtils.getRemoteAddr(req))
        return ActionStatus.createSuccessStatus(c)
    }

    @PostMapping(Urls.Admin.CAMPAIGN_PAUSE)
    fun campaignPause(@RequestParam campaignId: Long, @RequestParam reason: String, req: HttpServletRequest): ActionStatus<Campaign> {
        if (StringUtils.isEmpty(reason)) {
            return ActionStatus.createErrorStatus(messageSource.getMessage("moderation.message.empty", null, Locale.getDefault()))
        }
        val myProfileId = myProfileId()
        val c = campaignService.pauseCampaign(myProfileId, campaignId)

        val message = ModerationMessage()
        message.campaignId = campaignId
        message.message = reason
        message.status = ModerationMessageStatus.MODERATOR_MESSAGE
        message.campaignStatus = CampaignStatus.PAUSED
        message.senderId = myProfileId
        moderationMessageService.addModerationMessage(message)

        logger.info("Admin id=" + myProfileId + " paused campaign id=" + campaignId + " request ip=" + IpUtils.getRemoteAddr(req))
        return ActionStatus.createSuccessStatus(c)
    }

    @PostMapping(Urls.Admin.CAMPAIGN_REMOVE)
    fun campaignRemove(@RequestParam campaignId: Long): ActionStatus<*> {
        campaignService.removeCampaign(myProfileId(), campaignId)
        return ActionStatus.createSuccessStatus<Any>()
    }

    @GetMapping(Urls.Admin.CAMPAIGN_ORDERS_LIST)
    fun campaignOrdersList(searchOrderFilter: SearchOrderFilter): Collection<OrderInfo> {
        val campaign = campaignService.getCampaignSafe(searchOrderFilter.campaignId)
        val myProfileId = myProfileId()
        permissionService.checkIsAdmin(myProfileId, campaign.profileId ?: -1)
        searchOrderFilter.clientId = myProfileId
        return if (searchOrderFilter.dateFrom != null && searchOrderFilter.dateTo != null && searchOrderFilter.dateFrom?.compareTo(searchOrderFilter.dateTo) == 1) {
            emptyList()
        } else orderService.setQuestionToBuyer(searchService.searchForCampaignOrders(searchOrderFilter).searchResultRecords ?: emptyList())
    }

    @GetMapping(Urls.Admin.CAMPAIGN_STATS_LIST)
    fun campaignStatsList(@RequestParam campaignId: Long,
                          @RequestParam(required = false) dateFrom: Date?,
                          @RequestParam(required = false) dateTo: Date?,
                          @RequestParam(defaultValue = "DATE") aggregationType: AggregationType,
                          @RequestParam(defaultValue = "0") offset: Int,
                          @RequestParam(defaultValue = "0") limit: Int): List<CampaignStat> {
        val campaign = campaignService.getCampaignSafe(campaignId)
        permissionService.checkIsAdmin(myProfileId(), campaign.profileId ?: -1)
        return when (aggregationType) {
            AggregationType.DATE -> statisticsService.getCampaignGeneralStats(campaignId, dateFrom, dateTo, offset, limit)
            AggregationType.REFERER -> statisticsService.getCampaignRefererStats(campaignId, dateFrom, dateTo, offset, limit)
            AggregationType.COUNTRY -> statisticsService.getCampaignCountryStats(campaignId, dateFrom, dateTo, offset, limit)
            AggregationType.CITY -> statisticsService.getCampaignCityStats(campaignId, dateFrom, dateTo, offset, limit)
            else -> emptyList()
        }
    }

    @GetMapping(Urls.Admin.CAMPAIGN_TOTAL_STATS)
    fun campaignTotalStats(@RequestParam campaignId: Long,
                           @RequestParam(required = false) dateFrom: Date?,
                           @RequestParam(required = false) dateTo: Date?): CampaignStat? {
        return statisticsService.getCampaignGeneralTotalStats(campaignId, dateFrom, dateTo)
    }

    @GetMapping(Urls.Admin.CAMPAIGN_SHARES_SALE_LIST)
    fun getCampaignSharesSaleList(@RequestParam campaignId: Long,
                                  @RequestParam(required = false) dateFrom: Date?,
                                  @RequestParam(required = false) dateTo: Date?,
                                  @RequestParam(defaultValue = "0") offset: Int,
                                  @RequestParam(defaultValue = "0") limit: Int): List<Share> {
        val campaign = campaignService.getCampaignSafe(campaignId)
        if (!permissionService.isAdmin(myProfileId(), campaign.profileId ?: -1)) {
            throw PermissionException(MessageCode.PERMISSION_ADMIN)
        }
        return campaignStatsService.getSharesSaleInfo(campaignId, dateFrom, dateTo, offset, limit)
    }

    @PostMapping(Urls.Admin.CAMPAIGN_ADD_CONTACT)
    fun addCampaignContact(@Valid @RequestBody campaignContact: CampaignContact, bindingResult: BindingResult): ActionStatus<*> {
        if (bindingResult.hasErrors()) {
            return ActionStatus.createErrorStatus<Any>(bindingResult, baseControllerService.messageSource)
        }
        campaignService.saveCampaignContact(myProfileId(), campaignContact)
        return ActionStatus.createSuccessStatus(campaignContact)
    }

    @PostMapping(Urls.Admin.CAMPAIGN_ADD_CURATOR)
    fun addCampaignCurator(@Valid @RequestBody campaignContact: CampaignContact, bindingResult: BindingResult): ActionStatus<*> {
        if (bindingResult.hasErrors()) {
            return ActionStatus.createErrorStatus<Any>(bindingResult, baseControllerService.messageSource)
        }
        campaignService.addCampaignCurator(myProfileId(), campaignContact)
        return ActionStatus.createSuccessStatus(campaignContact)
    }

    @PostMapping(Urls.Admin.CAMPAIGN_CONTACTS_DELETE)
    fun deleteCampaignContact(@RequestParam campaignId: Long,
                              @RequestParam email: String): ActionStatus<*> {
        campaignService.deleteCampaignContact(myProfileId(), campaignId, email)
        return ActionStatus.createSuccessStatus<Any>()
    }

    @PostMapping(Urls.Admin.CAMPAIGN_CURATORS_DELETE)
    fun deleteCampaignCurator(@RequestParam campaignId: Long,
                              @RequestParam email: String): ActionStatus<*> {
        campaignService.deleteCampaignCurator(myProfileId(), campaignId, email)
        return ActionStatus.createSuccessStatus<Any>()
    }

    @GetMapping(Urls.Admin.CAMPAIGN_POSSIBLE_CONTACTS)
    fun getCampaignPossibleContacts(@RequestParam campaignId: Long): List<CampaignContact> {
        return try {
            val myProfileId = myProfileId()
            val campaign = campaignService.getCampaignSafe(campaignId)
            permissionService.checkIsAdmin(myProfileId, campaign.profileId ?: -1)
            campaignService.getPossibleCampaignContacts(myProfileId, campaignId)
        } catch (e: NotFoundException) {
            emptyList()
        }
    }

    /**
     * TODO a new angular apps should use
     * {@link ru.planeta.eva.web.controllers.FeedBackController#askManager(AskManagerDTO, BindingResult)}
     */
    @PostMapping(Urls.CAMPAIGN_CONTACTS_SEND_CURATOR_FEEDBACK)
    fun sendCuratorContactsFeedback(@RequestParam campaignId: Long,
                                    @RequestParam message: String,
                                    @RequestParam(required = false) email: String?): ActionStatus<*> {
        if (message.isBlank()) {
            val errorStatus = ActionStatus.createErrorStatus<Any>("message.text.empty", baseControllerService.messageSource)
            errorStatus.addFieldError("message", baseControllerService.getParametrizedMessage("message.text.empty"))
            return errorStatus
        }
        var userDisplayName = ""
        var totalEmail: String? = email
        val myProfileId = myProfileId()
        if (hasEmail()) {
            totalEmail = authorizationService.getUserPrivateEmailById(myProfileId)
            userDisplayName = profileService.getProfileSafe(myProfileId).displayName ?: ""
        }
        if (!ValidateUtils.isValidEmail(totalEmail)) {
            val errorStatus = ActionStatus.createErrorStatus<Any>("check.email", baseControllerService.messageSource)
            errorStatus.addFieldError("email", baseControllerService.getParametrizedMessage("check.email"))
            return errorStatus
        }
        campaignService.sendCampaignCuratorsFeedbackMessage(myProfileId, campaignId, message, totalEmail
                ?: "", userDisplayName)
        return ActionStatus.createSuccessStatus<Any>()
    }


    @PostMapping(Urls.CAMPAIGN_CONTACTS_SEND_AUTHOR_FEEDBACK)
    fun sendAuthorFeedback(@RequestParam campaignId: Long,
                           @RequestParam message: String): ActionStatus<*> {
        if (message.isBlank()) {
            val errorStatus = ActionStatus.createErrorStatus<Any>("message.text.empty", baseControllerService.messageSource)
            errorStatus.addFieldError("message", baseControllerService.getParametrizedMessage("message.text.empty"))
            return errorStatus
        }
        val clientId = myProfileId()
        permissionService.checkSendMessagePermission(clientId)
        val totalEmail = authorizationService.getUserPrivateEmailById(clientId)
        val userDisplayName = profileService.getProfileSafe(clientId).displayName
        val spamWordsList = configurationService.getStringConfig("spam.words")
        val spamWords = Arrays.asList(*StringUtils.split(spamWordsList, ','))
        if (checkSpam(message, spamWords) || checkSpam(totalEmail ?: "", spamWords) || checkSpam(userDisplayName
                        ?: "", spamWords)) {
            campaignService.sendCampaignSpamFeedbackMessage(clientId, campaignId, message, totalEmail
                    ?: "", userDisplayName ?: "")
        } else {
            campaignService.sendCampaignAuthorFeedbackMessage(clientId, campaignId, message, totalEmail
                    ?: "", userDisplayName ?: "")
        }
        return ActionStatus.createSuccessStatus<Any>()
    }

    @GetMapping(Urls.Admin.CAMPAIGN_TARGETING)
    fun getCampaignTargeting(@RequestParam campaignId: Long): CampaignTargeting {
        val campaign = campaignService.getCampaignSafe(campaignId)
        if (!permissionService.isAdmin(myProfileId(), campaign.profileId ?: -1)) {
            throw PermissionException(MessageCode.PERMISSION_ADMIN)
        }

        var campaignTargeting = campaignTargetingService.getCampaignTargetingByCampaignId(campaignId)
        if (campaignTargeting == null) {
            campaignTargeting = CampaignTargeting(campaignId)
        }

        return campaignTargeting
    }

    @CrossOrigin(origins = ["null"])
    @GetMapping(Urls.Admin.CAMPAIGN_TARGETING_PUBLIC)
    fun getCampaignTargetingPublic(@RequestParam campaignId: Long): ActionStatus<CampaignTargeting>? {
        return ActionStatus.createSuccessStatus(campaignTargetingService.getCampaignTargetingByCampaignId(campaignId))
    }

    @PostMapping(Urls.Admin.CAMPAIGN_TARGETING)
    fun getCampaignTargetingPost(@Valid campaignTargeting: CampaignTargeting, bindingResult: BindingResult): ActionStatus<*> {
        if (bindingResult.hasErrors()) {
            return ActionStatus.createErrorStatus<Any>(bindingResult, baseControllerService.messageSource)
        }
        campaignTargetingService.insertOrUpdate(myProfileId(), campaignTargeting)
        return ActionStatus.createSuccessStatus(campaignTargeting)
    }

    private fun checkSpam(str: String, spamWords: List<String>) = spamWords.any { str.contains(it, true) }
}


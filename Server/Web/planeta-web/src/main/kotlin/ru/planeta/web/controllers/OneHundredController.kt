package ru.planeta.web.controllers

import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.servlet.ModelAndView
import ru.planeta.api.service.billing.order.OrderInfoService
import ru.planeta.api.service.campaign.CampaignService
import ru.planeta.api.service.geo.GeoService

@Controller
class OneHundredController(private val orderInfoService: OrderInfoService,
                           private val geoService: GeoService,
                           private val campaignService: CampaignService) {

    @GetMapping(Urls.ONE_HUNDRED_MILLIONS)
    fun showPage(): ModelAndView {
        return ModelAndView(VIEW)
                .addAllObjects(mapOf("orders" to orderInfoService.getOrdersForOHMPage(0, ORDERS_COUNT),
                        "cities" to geoService.getCitiesWithGeo(0, CITIES_COUNT),
                        "collected" to campaignService.totalCollectedAmount))
    }

    companion object {

        private val VIEW = "welcome/oneHundredMillions"
        private val ORDERS_COUNT = 60
        private val CITIES_COUNT = 30
    }
}

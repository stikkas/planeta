package ru.planeta.web.controllers

import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.ResponseBody
import ru.planeta.dao.trashcan.IviDao
import ru.planeta.model.common.IviStat
import java.util.*

@Controller
class IviRuController(private val iviDao: IviDao) {

    @ResponseBody
    @RequestMapping(value = Urls.FOR_IVI_RU, method = [RequestMethod.GET])
    fun getIviStats(@RequestParam(value = "from", required = false) from : Date?,
                    @RequestParam(value = "to", required = false) to : Date?,
                    @RequestParam(value = "offset", required = false) offset : Long?,
                    @RequestParam(value = "limit", required = false) limit: Int?) : List<IviStat>
    {
        return iviDao.getIviStats(from, to, offset ?: 0, limit ?: 100)
    }
}

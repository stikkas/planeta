package ru.planeta.web.controllers

import org.springframework.context.MessageSource
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.ResponseBody
import org.springframework.web.servlet.ModelAndView
import ru.planeta.api.model.ActionStatus
import ru.planeta.api.model.json.DeliveryInfo
import ru.planeta.api.search.CampaignStatusFilter
import ru.planeta.api.search.SearchCampaigns
import ru.planeta.api.search.SearchService
import ru.planeta.api.search.SortOrder
import ru.planeta.api.service.billing.order.OrderService
import ru.planeta.api.service.configurations.ConfigurationService
import ru.planeta.api.service.loyalty.BonusService
import ru.planeta.api.service.profile.ProfileService
import ru.planeta.api.service.profile.VipClubService
import ru.planeta.api.web.authentication.myProfileId
import ru.planeta.model.common.DeliveryAddress
import ru.planeta.model.common.delivery.BaseDelivery
import ru.planeta.model.common.loyalty.Bonus
import ru.planeta.model.shop.enums.DeliveryType
import ru.planeta.web.controllers.services.MainBaseControllerService
import java.math.BigDecimal


@Controller
class BonusController(private val bonusService: BonusService,
                      private val orderService: OrderService,
                      private val vipClubService: VipClubService,
                      private val profileService: ProfileService,
                      private val searchService: SearchService,
                      private val mainBaseControllerService: MainBaseControllerService,
                      private val configurationService: ConfigurationService,
                      private val messageSource: MessageSource) {

    @GetMapping(Urls.PROMO_BONUSES)
    fun showBonuses(@RequestParam(defaultValue = "0") limit: Int): ModelAndView {
        val myProfileId = myProfileId()
        val profile = profileService.getProfile(myProfileId)
        val stat = orderService.getBuyerOrdersStatForVipCondition(myProfileId)

        val searchResult = searchService.getTopFilteredCampaignsSearchResult(
                SearchCampaigns(listOf(SortOrder.SORT_BY_MEDIAN_PRICE), "", null,
                        CampaignStatusFilter.ACTIVE, null,
                        null, null, null, null,
                        null, null, true, 0, 4))
        val campaigns = searchResult.searchResultRecords

        return mainBaseControllerService.defaultModelAndView(Actions.BONUSES)
                .addAllObjects(mapOf("campaigns" to campaigns,
                        "bonuses" to bonusService.getBonuses(0, limit),
                        "isVip" to (profile != null && profile.isVip),
                        "purchasedAmount" to stat["amount"],
                        "purchasedCount" to stat["count"],
                        "hasBonuses" to stat["hasBonuses"],
                        "vipCondition" to configurationService.vipClubJoinCondition,
                        "profileId" to myProfileId))
    }

    @ResponseBody
    @GetMapping(Urls.GET_BONUS)
    fun getBonus(@RequestParam bonusId: Long): Bonus? {
        return bonusService.getBonus(bonusId)
    }

    @ResponseBody
    @PostMapping(Urls.CREATE_BONUS_ORDER)
    fun createOrder(@RequestParam bonusId: Long,
                    @RequestParam deliveryType: DeliveryType,
                    @RequestParam(required = false) country: String?,
                    @RequestParam(required = false) city: String?,
                    @RequestParam(required = false) address: String?,
                    @RequestParam(required = false) postIndex: String?,
                    @RequestParam(required = false) recipientName: String?,
                    @RequestParam(required = false) recipientPhone: String?): ActionStatus<*> {
        val myProfileId = myProfileId()

        val profile = profileService.getProfile(myProfileId)

        if (profile == null || !profile.isVip) {
            return ActionStatus.createErrorStatus<Any>("profile.isnot.vip", messageSource)
        }
        val bonus = bonusService.getBonus(bonusId)
        if (bonus == null || bonus.available < 1) {
            return ActionStatus.createErrorStatus<Any>("bonus.not.found", messageSource)
        }

        val deliveryAddress = DeliveryAddress()
        deliveryAddress.address = address
        deliveryAddress.city = city
        deliveryAddress.country = country
        deliveryAddress.fio = recipientName
        deliveryAddress.phone = recipientPhone
        deliveryAddress.zipCode = postIndex

        val deliveryDepartamentId = if (deliveryType == DeliveryType.CUSTOMER_PICKUP)
            BaseDelivery.DEFAULT_CUSTOMER_PICKUP_SERVICE_ID
        else
            BaseDelivery.DEFAULT_DELIVERY_SERVICE_ID
        val deliveryInfo = DeliveryInfo(deliveryDepartamentId, BigDecimal.ZERO, deliveryType)

        vipClubService.changeUserVipStatus(myProfileId, false)
        try {
            orderService.createBonusOrder(myProfileId, bonusId, deliveryAddress, deliveryInfo)
        } catch (ex: Exception) {
            vipClubService.changeUserVipStatus(myProfileId, true)
            throw ex
        }

        return ActionStatus.createSuccessStatus<Any>()
    }

}

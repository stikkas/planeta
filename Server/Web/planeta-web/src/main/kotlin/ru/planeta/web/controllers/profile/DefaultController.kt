package ru.planeta.web.controllers.profile

import org.apache.commons.lang3.StringUtils
import org.apache.log4j.Logger
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.servlet.ModelAndView
import ru.planeta.api.exceptions.NotFoundException
import ru.planeta.api.model.UserAuthorizationInfo
import ru.planeta.api.model.json.ProfileInfo
import ru.planeta.api.service.campaign.CampaignService
import ru.planeta.api.service.profile.ProfileService
import ru.planeta.api.utils.ValidateUtils
import ru.planeta.api.web.authentication.isAnonymous
import ru.planeta.api.web.authentication.myProfileId
import ru.planeta.api.web.authentication.userAuthorizationInfo
import ru.planeta.api.web.controllers.services.BaseControllerService
import ru.planeta.commons.web.WebUtils
import ru.planeta.web.controllers.Actions
import ru.planeta.web.controllers.Urls
import ru.planeta.web.controllers.services.AuthorizationControllerService
import javax.servlet.http.HttpServletRequest

@Controller
class DefaultController(private val authorizationControllerService: AuthorizationControllerService,
                        private val baseControllerService: BaseControllerService,
                        private val profileService: ProfileService,
                        private val campaignService: CampaignService) {


    @Value("\${default.controller.excluded.path}")
    lateinit var excludedPath: Array<String>

    private fun welcome(): ModelAndView {
        val modelAndView = authorizationControllerService.createDefaultModelAndView(Actions.INDEX)
        modelAndView.addObject("needStartRouter", true)
        return modelAndView
    }

    /**
     * This method handles all urls that does not have specific mappings.
     * It checks url for profile alias and opens profile's page if alias has been found.
     */
    @GetMapping("/*", "/**")
    fun handle(@RequestParam(defaultValue = "false") error: Boolean,
               @RequestParam(required = false) errorCode: String?,
               @RequestParam(required = false) username: String?,
               request: HttpServletRequest): ModelAndView? {
        val urlQuery = request.queryString
        val mav = getDefaultModelAndView(error, errorCode, username, request)
        mav?.addObject("urlQuery", urlQuery)
        return mav
    }

    private fun getDefaultModelAndView(error: Boolean, errorCode: String?, username: String?, request: HttpServletRequest): ModelAndView? {
        val uri = prepareUri(request)
        if (errorCode != null && (StringUtils.isEmpty(uri) || Urls.ROOT == uri)) {
            return authorizationControllerService.generatedWelcomeModelAndView(error, errorCode, username, request)
        }
        checkIfExcludedPath(uri)
        //
        var modelAndView = checkInternetExplorerRedirect(uri, request)

        if (modelAndView == null) {
            // check for enroute parameter
            if (request.getParameter(ENROUTE) == null) {
                // if no parameter, thеn this is first access,
                // we try to show page normally,
                // if JS-router fails thеn it will send us back here, but with parameter
                modelAndView = createModelAndView(uri)
            } else {
                // this is redirect from JS-router, second fail = show root (root == index now)
                modelAndView = baseControllerService.createRedirectModelAndView(Urls.ROOT)
            }
        }
        return modelAndView
    }

    /**
     * IE hack. It doesn't work right with JS-router.
     * This address should NOT be accessed normally.
     */
    @GetMapping(Urls.INDEX)
    fun index(request: HttpServletRequest): ModelAndView {
        // check for ie
        return if (WebUtils.isInternetExplorer(request)) {
            // check for marker parameter
            if (request.getParameter(ENROUTE) == null) {
                // if no marker, than this is first access,
                // we try to show index page (even if unauthorized)
                // if JS-router fails than it will send us back here, but with marker
                welcome()
            } else {
                // this is redirect from JS-router, second fail = show root
                baseControllerService.createRedirectModelAndView(Urls.ROOT)
            }
        } else {
            //normal people should not be here
            baseControllerService.createRedirectModelAndView(Urls.ROOT)
        }
    }

    private fun handleIndex(profileModel: ProfileInfo?): ModelAndView {

        val modelAndView = welcome()
        if (profileModel != null) {
            modelAndView.addObject("profileModel", profileModel)
        }
        return modelAndView
    }

    /**
     * Creates model and view for the specified uri (it will be rendered by our backbone fw.
     */
    private fun createModelAndView(uri: String): ModelAndView {
        val alias = getAlias(uri)
        val userAuthorizationInfo = userAuthorizationInfo()
        //
        if (alias == null || alias.isEmpty()) {
            return welcome()
        }

        if (!ValidateUtils.isReserved(alias)) {
            val profileModel = profileService.getProfile<ProfileInfo>(myProfileId(), alias)
            val profile = profileModel.profile
            profile?.alias?.let {
                if (it.isNotEmpty() && it != alias) {
                    return baseControllerService.createPermanentRedirectModelAndView(uri.replace(alias, it))
                }
            }
            if (!isMyProfile(userAuthorizationInfo, alias)) {
                return handleIndex(profileModel)
            }
        } else if (CAMPAIGNS == alias) {
            try {
                val campaignAlias = getCampaignAlias(uri)
                val campaign = campaignService.getCampaign(campaignAlias)
                if (campaign != null) {
                    if (!StringUtils.isEmpty(campaign.campaignAlias)) {
                        val redirectUri = getRedirectCampaignUri(uri, campaign.campaignId, campaign.campaignAlias ?: "")
                        if (uri.toLowerCase() != redirectUri.toLowerCase()) {
                            return baseControllerService.createPermanentRedirectModelAndView(redirectUri)
                        }
                    }
                    return handleIndex(profileService.getProfileInfo(myProfileId(), campaign.creatorProfileId))
                }
            } catch (e: NotFoundException) {
                return welcome()
            }

        }
        return welcome()
    }

    /**
     * Check for excluded paths first.
     * If current uri is excluded - just throw not found exception.
     */
    private fun checkIfExcludedPath(uri: String) {
        // Check for excluded paths first. If found -- throw not found exception
        for (excluded in excludedPath) {
            if (uri.startsWith(excluded)) {
                // never happen
                log.error("User want excluded resourse " + uri)
                throw NotFoundException(uri + " is not found")
            }
        }
    }


    companion object {
        private const val CAMPAIGNS = "campaigns"
        private const val CAMPAIGNS_WITH_SLASH = "/campaigns/"
        private const val ENROUTE = "enroute"
        private val log = Logger.getLogger(DefaultController::class.java)

    }

    /**
     * Does some preparations (returns uri without jsessionid)
     */
    private fun prepareUri(request: HttpServletRequest): String {
        val uri = request.requestURI
        return WebUtils.removeJSessionFromUrl(uri)
    }

    /**
     * Redirect Internet Explorer users from /alias to the url /handleIndex.html#alias
     */
    private fun checkInternetExplorerRedirect(uri: String, request: HttpServletRequest): ModelAndView? {
        if (WebUtils.isInternetExplorer(request) && !uri.contains(Urls.INDEX) && "/" != uri) {
            // IE sometimes ignores hash in redirect url.
            // So we also passing it through user session (it will be removed in handleIndex.jsp just after the usage)
            val locationHash = uri.replace("^/|/$".toRegex(), "")
            request.session.setAttribute("locationHash", locationHash)
            val modelAndView = baseControllerService.createRedirectModelAndView(Urls.INDEX)
            modelAndView.addObject("urlQuery", request.queryString)
            return modelAndView
        }
        return null
    }

    /**
     * Extracts alias from uri
     */
    private fun getAlias(uri: String?): String? {
        if (uri == null) {
            return null
        }
        val components = uri.split("/".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
        if (components.size < 2) {
            return null
        }
        val parts = components[1].split("!".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
        return if (parts.size == 0) {
            null
        } else parts[0]
    }

    private fun getCampaignAlias(uri: String?): String {
        if (uri == null) {
            throw NotFoundException()
        }
        var st = uri.indexOf(CAMPAIGNS_WITH_SLASH)
        if (st < 0) {
            throw NotFoundException("Wrong campaign: " + uri)
        }
        st += CAMPAIGNS_WITH_SLASH.length
        var en = st
        val ln = uri.length
        while (en < ln && uri[en] != '/' && uri[en] != '?' && uri[en] != '!') {
            en++
        }

        return uri.substring(st, en)
    }

    private fun getRedirectCampaignUri(uri: String?, campaignId: Long, campaignAlias: String): String {
        if (uri == null) {
            throw NotFoundException()
        }
        var st = uri.indexOf(CAMPAIGNS_WITH_SLASH)
        if (st < 0) {
            throw NotFoundException("Wrong campaign: " + uri)
        }
        st += CAMPAIGNS_WITH_SLASH.length
        var en = st
        val ln = uri.length
        while (en < ln && uri[en] != '/' && uri[en] != '?' && uri[en] != '!') {
            en++
        }
        return if (uri.substring(en).startsWith("/edit")) {
            uri.substring(0, st) + campaignId + uri.substring(en)
        } else {
            uri.substring(0, st) + campaignAlias + uri.substring(en)
        }
    }

    /**
     * Checks if specified alias is of my own profile
     */
    private fun isMyProfile(userAuthorizationInfo: UserAuthorizationInfo?, alias: String?): Boolean {
        // never happen but
        return (alias != null
                && userAuthorizationInfo != null
                && userAuthorizationInfo.profile != null
                && !isAnonymous()
                && (alias == userAuthorizationInfo.profile.alias || alias == java.lang.Long.toString(userAuthorizationInfo.profile.profileId)))
    }
}

package ru.planeta.web.controllers.welcome

import org.apache.commons.lang3.StringUtils
import org.apache.log4j.Logger
import org.springframework.context.MessageSource
import org.springframework.stereotype.Controller
import org.springframework.validation.BindingResult
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.ResponseBody
import org.springframework.web.servlet.ModelAndView
import ru.planeta.api.exceptions.NotFoundException
import ru.planeta.api.exceptions.PermissionException
import ru.planeta.api.model.ActionStatus
import ru.planeta.api.model.json.PasswordRecoveryChange
import ru.planeta.api.service.profile.AuthorizationService
import ru.planeta.api.web.authentication.isAnonymous
import ru.planeta.api.web.authentication.userAuthorizationInfo
import ru.planeta.api.web.controllers.services.ControllerAutoLoginWrapService
import ru.planeta.api.web.utils.SessionUtils
import ru.planeta.model.enums.ProjectType
import ru.planeta.web.controllers.Actions
import ru.planeta.web.controllers.Urls
import ru.planeta.web.controllers.services.AuthorizationControllerService
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse
import javax.validation.Valid

@Controller
class RegistrationController(private val authorizationControllerService: AuthorizationControllerService,
                             private val autoLoginWrapService: ControllerAutoLoginWrapService,
                             private val authorizationService: AuthorizationService,
                             private val messageSource: MessageSource) {

    private val logger = Logger.getLogger(RegistrationController::class.java)

    @GetMapping(Urls.CONFIRM)
    fun confirmRegistration(@RequestParam email: String,
                            @RequestParam regCode: String,
                            @RequestParam(required = false) redirectUrl: String?,
                            request: HttpServletRequest,
                            response: HttpServletResponse): ModelAndView {
        return try {
            val userPrivateInfo = authorizationService.confirmRegistration(regCode, email)
            autoLoginWrapService.autoLoginAndCasRedirectView(userPrivateInfo.username ?: "", redirectUrl
                    ?: Urls.ROOT, request, response)
        } catch (ex: NotFoundException) {
            if (ex.messageCode != null && ex.messageCode?.errorPropertyName != null)
                authorizationControllerService.createErrorModelAndView(ex.messageCode?.errorPropertyName ?: "")
            else
                authorizationControllerService.createErrorModelAndView("registration.error.confirm.regcode")
        } catch (ex: PermissionException) {
            if (ex.messageCode != null && ex.messageCode?.errorPropertyName != null)
                authorizationControllerService.createErrorModelAndView(ex.messageCode?.errorPropertyName ?: "")
            else
                authorizationControllerService.createErrorModelAndView("registration.error.confirm.regcode.expired")
        }

    }

    @GetMapping(Urls.CONFIRM_RECOVER)
    fun passwordRecoveryRequest(@RequestParam(value = "regCode", required = true) regCode: String,
                                @RequestParam(value = "passwordRecoveryRedirectUrl", defaultValue = "") passwordRecoveryRedirectUrl: String,
                                request: HttpServletRequest): ModelAndView {
        try {
            val userPrivateInfo = authorizationService.passwordRecoveryRequest(regCode)
            if (userPrivateInfo != null) {
                return authorizationControllerService.generatedWelcomeModelAndView(false, null, "", request)
                        .addAllObjects(mapOf("passwordRecovery" to true,
                                "passwordRecoveryRedirectUrl" to passwordRecoveryRedirectUrl,
                                "regCode" to regCode,
                                "email" to userPrivateInfo.email))
            }
        } catch (ex: Exception) {
            logger.error("Unexpected error checking for password recovery: " + regCode, ex)
        }

        return authorizationControllerService.createErrorModelAndView("password.recovery.error")
    }

    @PostMapping(Urls.RECOVER_PASSWORD)
    @ResponseBody
    fun passwordRecoveryChangePassword(@Valid passwordRecoveryChange: PasswordRecoveryChange,
                                       result: BindingResult): ActionStatus<*> {

        try {
            if (result.hasErrors()) {
                return ActionStatus.createErrorStatus<Any>(result, messageSource)
            } else {
                val userPrivateInfo = authorizationService.passwordRecoveryChangePassword(
                        passwordRecoveryChange.regCode ?: "", passwordRecoveryChange.password ?: "")
                if (userPrivateInfo != null) {
                    autoLoginWrapService.autoLoginUser(userPrivateInfo.username ?: "")
                    return ActionStatus.createSuccessStatus<Any>()
                } else {
                    return ActionStatus.createErrorStatus<Any>("passwordRecovery.error", messageSource)
                }
            }
        } catch (ex: Exception) {
            logger.error("Cannot change password", ex)
            return ActionStatus.createErrorStatus<Any>("passwordRecovery.error", messageSource)
        }

    }

    @GetMapping(Urls.ACCOUNT_MERGE)
    fun accountMerge(@RequestParam(required = false) redirectUrl: String?,
                     @RequestParam(required = false) email: String?): ModelAndView {
        val url = if (redirectUrl.isNullOrBlank()) Urls.ROOT else redirectUrl as String

        SessionUtils.redirectAfterOAuth = url
        if (isAnonymous() || StringUtils.isNotBlank(userAuthorizationInfo()?.userPrivateInfo?.email)) {
            throw NotFoundException()
        }

        SessionUtils.redirectAfterOAuth = url

        val modelAndView = authorizationControllerService.createDefaultModelAndView(Actions.ACCOUNT_MERGE)
        if (email != null) {
            modelAndView.addObject("email", email)
        }
        modelAndView.addObject("userAuthorizationInfo", userAuthorizationInfo())
        return modelAndView
    }

}

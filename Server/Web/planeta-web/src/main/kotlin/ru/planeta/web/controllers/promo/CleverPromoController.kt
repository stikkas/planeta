package ru.planeta.web.controllers.promo

import org.apache.commons.lang3.StringUtils
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.servlet.ModelAndView
import ru.planeta.api.model.CleverConfigurationType
import ru.planeta.api.service.configurations.ConfigurationService
import ru.planeta.api.service.shop.ProductUsersService
import ru.planeta.web.controllers.Actions
import ru.planeta.web.controllers.Urls
import ru.planeta.web.controllers.services.MainBaseControllerService

@Controller
class CleverPromoController(private val productUsersService: ProductUsersService,
                            private val mainBaseControllerService: MainBaseControllerService,
                            private val configurationService: ConfigurationService) {

    @GetMapping(Urls.PROMO_CLEVER)
    fun showCleverPromo(): ModelAndView = mainBaseControllerService.defaultModelAndView(Actions.CLEVER).addAllObjects(
            mapOf("cleverCampaignIds" to configurationService.getIds(CleverConfigurationType.CLEVER_CAMPAIGNS),
                    "products" to productUsersService.getProducts(configurationService.getIds(CleverConfigurationType.CLEVER_PRODUCTS)),
                    "cleverProjectOfMonth" to getPromoHtml(CleverConfigurationType.CLEVER_PROJECT_OF_MONTH),
                    "cleverBookOfMonth" to getPromoHtml(CleverConfigurationType.CLEVER_BOOK_OF_MONTH),
                    "cleverEventOfMonth" to getPromoHtml(CleverConfigurationType.CLEVER_EVENT_OF_MONTH)))

    private fun getPromoHtml(key: String): String {
        val result = configurationService.getConfigurationByKey(key) ?: return StringUtils.EMPTY
        return result.stringValue ?: ""
    }

}

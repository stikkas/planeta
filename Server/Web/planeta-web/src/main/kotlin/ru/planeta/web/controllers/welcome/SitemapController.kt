package ru.planeta.web.controllers.welcome

import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.servlet.ModelAndView
import ru.planeta.api.service.campaign.CampaignService
import ru.planeta.api.web.controllers.Urls
import ru.planeta.web.controllers.Actions
import ru.planeta.web.controllers.services.MainBaseControllerService

//sitmap генериться при помощи шелскриптов
//которые расположены в папке /opt/sitemaps
//mobile_sitemap.sh
//shop_sitemap.sh
//tv_sitemap.sh
//web_sitemap.sh

//запускается из крона root'a
//        5 0 1 * * /opt/sitemaps/shop_sitemap.sh
//        10 0 1 * * /opt/sitemaps/tv_sitemap.sh
//        15 0 1 * * /opt/sitemaps/mobile_sitemap.sh
//        20 0 1 * * /opt/sitemaps/web_sitemap.sh


@Controller
class SitemapController(private val mainBaseControllerService: MainBaseControllerService,
                        private val campaignService: CampaignService) {

    @GetMapping(Urls.HTML_SITE_MAP)
    fun htmlSiteMap(): ModelAndView = mainBaseControllerService.defaultModelAndView(Actions.SITE_MAP)
            .addObject("campaignTags", campaignService.allTags)

}


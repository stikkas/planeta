package ru.planeta.web.controllers.profile

import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import ru.planeta.api.exceptions.PermissionException
import ru.planeta.api.model.ActionStatus
import ru.planeta.api.web.authentication.myProfileId
import ru.planeta.dao.trashcan.UserFeedbackDAO
import ru.planeta.model.trashcan.FeedbackPageType
import ru.planeta.model.trashcan.UserFeedback
import ru.planeta.web.controllers.Urls
import java.util.*

@RestController
class FeedbackController(private val userFeedbackDAO: UserFeedbackDAO) {

    @PostMapping(Urls.UserFeedback.ADD_USER_FEEDBACK)
    fun addUserFeedback(userFeedback: UserFeedback): ActionStatus<*> {
        if (myProfileId() != userFeedback.userId) {
            throw PermissionException()
        }
        val lastUserFeedback: UserFeedback? = if (userFeedback.pageType == FeedbackPageType.CAMPAIGN_CREATED_SUCCESSFUL)
            userFeedbackDAO.selectUserFeedbackByCampaignIdAndPageType(userFeedback.campaignId, FeedbackPageType.CAMPAIGN_CREATED_SUCCESSFUL)
        else
            userFeedbackDAO.selectUserFeedbackByOrderId(userFeedback.orderId)

        lastUserFeedback?.let {
            return ActionStatus.createSuccessStatus(true)
        }
        if (userFeedback.timeAdded == null) {
            userFeedback.timeAdded = Date()
        }
        userFeedbackDAO.insert(userFeedback)
        return ActionStatus.createSuccessStatus(true)
    }

    @PostMapping(Urls.UserFeedback.CHECK_USER_FEEDBACK_EXISTS)
    fun checkUserFeedbackExists(@RequestParam orderId: Long): ActionStatus<*> {
        return if (userFeedbackDAO.selectUserFeedbackByOrderId(orderId) == null)
            ActionStatus.createSuccessStatus(false)
        else
            ActionStatus.createSuccessStatus(true)
    }

    @PostMapping(Urls.UserFeedback.CHECK_CAMPAIGN_USER_FEEDBACK_EXISTS)
    fun checkCampaignCreatedUserFeedbackExists(@RequestParam campaignId: Long): ActionStatus<*> {
        return if (userFeedbackDAO.selectUserFeedbackByCampaignIdAndPageType(campaignId, FeedbackPageType.CAMPAIGN_CREATED_SUCCESSFUL) == null)
            ActionStatus.createSuccessStatus(false)
        else
            ActionStatus.createSuccessStatus(true)
    }
}

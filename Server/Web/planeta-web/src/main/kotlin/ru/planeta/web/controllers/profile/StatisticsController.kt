package ru.planeta.web.controllers.profile

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.ResponseBody
import ru.planeta.api.exceptions.NotFoundException
import ru.planeta.api.exceptions.PermissionException
import ru.planeta.api.service.statistic.StatisticsService
import ru.planeta.model.stat.CampaignStat
import ru.planeta.web.controllers.Urls
import java.util.*

@Controller
class StatisticsController @Autowired
constructor(private val statisticsService: StatisticsService) {

    @ResponseBody
    @GetMapping(Urls.CAMPAIGN_STAT_GENERAL)
    @Throws(NotFoundException::class, PermissionException::class)
    fun getStatisticByDate(@RequestParam campaignId: Long,
                           @RequestParam(required = false) dateFrom: Date?,
                           @RequestParam(required = false) dateTo: Date?): List<CampaignStat> {

        //we should reverse this list, cus we need another ORDER BY for Graph
        val result = statisticsService.getCampaignGeneralStats(campaignId, dateFrom, dateTo, 0, 0)
        Collections.reverse(result)
        return result
    }
}

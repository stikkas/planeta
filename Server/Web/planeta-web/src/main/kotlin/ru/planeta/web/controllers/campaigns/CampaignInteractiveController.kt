package ru.planeta.web.controllers.campaigns

import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import ru.planeta.api.model.ActionStatus
import ru.planeta.api.news.LoggerService
import ru.planeta.api.service.campaign.CampaignService
import ru.planeta.api.service.interactive.InteractiveEditorDataService
import ru.planeta.api.service.notifications.NotificationService
import ru.planeta.api.service.profile.ProfileService
import ru.planeta.api.web.authentication.isAnonymous
import ru.planeta.api.web.authentication.myProfileId
import ru.planeta.dao.commondb.UserPrivateInfoDAO
import ru.planeta.model.news.ProfileNews
import ru.planeta.model.trashcan.InteractiveEditorData
import ru.planeta.web.controllers.Urls

@RestController
class CampaignInteractiveController(private val userPrivateInfoDAO: UserPrivateInfoDAO,
                                    private val notificationService: NotificationService,
                                    private val interactiveEditorDataService: InteractiveEditorDataService,
                                    private val profileNewsService: LoggerService,
                                    private val campaignService: CampaignService,
                                    private val profileService: ProfileService) {

    @GetMapping(Urls.Interactive.TRACK_CAMPAIGN_INTERACTIVE_STEP_INFO)
    fun trackCampaignInteractiveStepInfo(@RequestParam campaignId: Long): ActionStatus<*> {
        profileNewsService.addProfileNews(ProfileNews.Type.CAMPAIGN_INTERACTIVE_STEP_INFO, myProfileId(), campaignId, campaignId)
        return ActionStatus.createSuccessStatus<Any>()
    }

    @GetMapping(Urls.Interactive.TRACK_CAMPAIGN_INTERACTIVE_STEP_DURATION)
    fun trackCampaignInteractiveStepDuration(@RequestParam campaignId: Long): ActionStatus<*> {
        profileNewsService.addProfileNews(ProfileNews.Type.CAMPAIGN_INTERACTIVE_STEP_DURATION, myProfileId(), campaignId, campaignId)
        return ActionStatus.createSuccessStatus<Any>()
    }

    @GetMapping(Urls.Interactive.TRACK_CAMPAIGN_INTERACTIVE_STEP_PRICE)
    fun trackCampaignInteractiveStepPrice(@RequestParam campaignId: Long): ActionStatus<*> {
        profileNewsService.addProfileNews(ProfileNews.Type.CAMPAIGN_INTERACTIVE_STEP_PRICE, myProfileId(), campaignId, campaignId)
        return ActionStatus.createSuccessStatus<Any>()
    }

    @GetMapping(Urls.Interactive.TRACK_CAMPAIGN_INTERACTIVE_STEP_VIDEO)
    fun trackCampaignInteractiveStepVideo(@RequestParam campaignId: Long): ActionStatus<*> {
        profileNewsService.addProfileNews(ProfileNews.Type.CAMPAIGN_INTERACTIVE_STEP_VIDEO, myProfileId(), campaignId, campaignId)
        return ActionStatus.createSuccessStatus<Any>()
    }

    @GetMapping(Urls.Interactive.TRACK_CAMPAIGN_INTERACTIVE_STEP_DESCRIPTION)
    fun trackCampaignInteractiveStepDescription(@RequestParam campaignId: Long): ActionStatus<*> {
        profileNewsService.addProfileNews(ProfileNews.Type.CAMPAIGN_INTERACTIVE_STEP_DESCRIPTION, myProfileId(), campaignId, campaignId)
        return ActionStatus.createSuccessStatus<Any>()
    }

    @GetMapping(Urls.Interactive.TRACK_CAMPAIGN_INTERACTIVE_STEP_REWARD)
    fun trackCampaignInteractiveStepReward(@RequestParam campaignId: Long): ActionStatus<*> {
        profileNewsService.addProfileNews(ProfileNews.Type.CAMPAIGN_INTERACTIVE_STEP_REWARD, myProfileId(), campaignId, campaignId)
        return ActionStatus.createSuccessStatus<Any>()
    }

    @GetMapping(Urls.Interactive.TRACK_CAMPAIGN_INTERACTIVE_STEP_COUNTERPARTY)
    fun trackCampaignInteractiveStepCounterparty(@RequestParam campaignId: Long): ActionStatus<*> {
        profileNewsService.addProfileNews(ProfileNews.Type.CAMPAIGN_INTERACTIVE_STEP_COUNTERPARTY, myProfileId(), campaignId, campaignId)
        return ActionStatus.createSuccessStatus<Any>()
    }

    @GetMapping(Urls.Interactive.TRACK_CAMPAIGN_INTERACTIVE_STEP_CHECK)
    fun trackCampaignInteractiveStepCheck(@RequestParam campaignId: Long): ActionStatus<*> {
        profileNewsService.addProfileNews(ProfileNews.Type.CAMPAIGN_INTERACTIVE_STEP_CHECK, myProfileId(), campaignId, campaignId)
        return ActionStatus.createSuccessStatus<Any>()
    }

    @GetMapping(Urls.Interactive.TRACK_CAMPAIGN_INTERACTIVE_STEP_FINAL)
    fun trackCampaignInteractiveStepFinal(@RequestParam campaignId: Long): ActionStatus<*> {
        profileNewsService.addProfileNews(ProfileNews.Type.CAMPAIGN_INTERACTIVE_STEP_FINAL, myProfileId(), campaignId, campaignId)
        sendEmail(campaignId)
        return ActionStatus.createSuccessStatus<Any>()
    }

    @GetMapping(Urls.Interactive.TRACK_CAMPAIGN_INTERACTIVE_STEP_FINAL_DRAFT)
    fun trackCampaignInteractiveStepFinalDraft(@RequestParam campaignId: Long): ActionStatus<*> {
        profileNewsService.addProfileNews(ProfileNews.Type.CAMPAIGN_INTERACTIVE_STEP_FINAL_DRAFT, myProfileId(), campaignId, campaignId)
        sendEmail(campaignId)
        return ActionStatus.createSuccessStatus<Any>()
    }

    private fun sendEmail(campaignId: Long) {
        val campaign = campaignService.getCampaign(campaignId)
        campaign?.let {
            val profile = profileService.getProfile(campaign.creatorProfileId)
            val info = userPrivateInfoDAO.selectByUserId(campaign.creatorProfileId)
            info?.let {
                profile?.let {
                    notificationService.sendInteractiveCampaignFinished(info.userId, info.email ?: "", profile.displayName ?: "")
                }
            }
        }
    }

    @GetMapping(Urls.Interactive.GET_INTERACTIVE_EDITOR_DATA)
    fun getInteractiveEditorData(@RequestParam(defaultValue = "0") activeIndex: Int,
                                 @RequestParam(defaultValue = "true") agreeReceiveLessonsMaterialsOnEmail: Boolean): ActionStatus<InteractiveEditorData> {
        if (isAnonymous()) {
            return ActionStatus.createSuccessStatus()
        }
        val myProfileId = myProfileId()
        var interactiveEditorData: InteractiveEditorData? = interactiveEditorDataService.getByUserId(myProfileId)
        if (interactiveEditorData == null) {
            interactiveEditorData = InteractiveEditorData()
            interactiveEditorData.userId = myProfileId
            if (agreeReceiveLessonsMaterialsOnEmail) {
                interactiveEditorData.isAgreeReceiveLessonsMaterialsOnEmail = agreeReceiveLessonsMaterialsOnEmail
            }
        }

        if (interactiveEditorData.dataId > 0) {
            if (activeIndex < STEP_PROCEED_ID && activeIndex == interactiveEditorData.reachedStepNum + 1) {
                interactiveEditorData.reachedStepNum = activeIndex
                interactiveEditorDataService.update(interactiveEditorData)
            }
        } else {
            if (activeIndex < STEP_PROCEED_ID) {
                interactiveEditorData.reachedStepNum = activeIndex
            } else {
                interactiveEditorData.reachedStepNum = STEP_NAME_ID
            }
            if (activeIndex == STEP_EMAIL_ID) {
                interactiveEditorData.setShowFormsSettingsBaseOnReachedStepNum(interactiveEditorData.reachedStepNum + 1)
            }
            interactiveEditorDataService.insert(interactiveEditorData)
        }

        return ActionStatus.createSuccessStatus(interactiveEditorData)
    }

    @PostMapping(Urls.Interactive.SAVE_INTERACTIVE_EDITOR_DATA_FORMS_SETTINGS)
    fun setInteractiveEditorDataFormsSettings(@RequestParam(defaultValue = "0") activeIndex: Int,
                                              @RequestParam formsSettings: String): ActionStatus<*> {
        if (isAnonymous()) {
            return ActionStatus.createSuccessStatus<Any>()
        }

        if (formsSettings.isEmpty()) {
            return ActionStatus.createSuccessStatus<Any>()
        }

        val myProfileId = myProfileId()
        var interactiveEditorData: InteractiveEditorData? = interactiveEditorDataService.getByUserId(myProfileId)
        if (interactiveEditorData == null) {
            interactiveEditorData = InteractiveEditorData()
            interactiveEditorData.userId = myProfileId
        }

        interactiveEditorData.formsSettings = formsSettings

        if (activeIndex > interactiveEditorData.reachedStepNum) {
            interactiveEditorData.reachedStepNum = activeIndex
        }

        if (interactiveEditorData.dataId > 0) {
            interactiveEditorDataService.update(interactiveEditorData)
        } else {
            interactiveEditorDataService.insert(interactiveEditorData)
        }

        return ActionStatus.createSuccessStatus<Any>()
    }

    @PostMapping(Urls.Interactive.SET_AGREE_RECEIVE_LESSONS_MATERIALS_ON_EMAIL)
    fun setAgreeReceiveLessonsMaterialsOnEmail(@RequestParam("isAgree") isAgree: Boolean): ActionStatus<*> {
        if (isAnonymous()) {
            return ActionStatus.createSuccessStatus<Any>()
        }
        val myProfileId = myProfileId()
        var interactiveEditorData: InteractiveEditorData? = interactiveEditorDataService.getByUserId(myProfileId)
        if (interactiveEditorData == null) {
            interactiveEditorData = InteractiveEditorData()
            interactiveEditorData.userId = myProfileId
        }

        interactiveEditorData.isAgreeReceiveLessonsMaterialsOnEmail = isAgree

        if (interactiveEditorData.dataId > 0) {
            interactiveEditorDataService.update(interactiveEditorData)
        } else {
            interactiveEditorDataService.insert(interactiveEditorData)
        }

        return ActionStatus.createSuccessStatus<Any>()
    }

    @PostMapping(Urls.Interactive.CLEAR_INTERACTIVE_EDITOR_DATA)
    fun clearInteractiveEditorData(): ActionStatus<*> {
        if (isAnonymous()) {
            return ActionStatus.createSuccessStatus<Any>()
        }
        val myProfileId = myProfileId()
        interactiveEditorDataService.deleteByUserId(myProfileId, myProfileId)
        return ActionStatus.createSuccessStatus<Any>()
    }

    companion object {

        val STEP_NAME_ID = 1
        val STEP_EMAIL_ID = 2
        val STEP_PROCEED_ID = 3
    }

}

package ru.planeta.web.controllers.profile

import org.apache.commons.lang3.StringUtils
import org.apache.log4j.Logger
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.*
import org.springframework.web.servlet.ModelAndView
import ru.planeta.api.aspect.logging.BillingLoggableRequest
import ru.planeta.api.exceptions.MessageCode
import ru.planeta.api.exceptions.PaymentException
import ru.planeta.api.exceptions.PermissionException
import ru.planeta.api.exceptions.RegistrationException
import ru.planeta.api.helper.ProjectService
import ru.planeta.api.model.ActionStatus
import ru.planeta.api.remote.PaymentGateService
import ru.planeta.api.service.billing.order.OrderService
import ru.planeta.api.service.billing.payment.PaymentService
import ru.planeta.api.service.billing.payment.settings.PaymentSettingsService
import ru.planeta.api.service.campaign.CampaignService
import ru.planeta.api.service.common.InvestingOrderInfoService
import ru.planeta.api.service.geo.DeliveryAddressService
import ru.planeta.api.service.statistic.OrderShortLinkStatService
import ru.planeta.api.web.authentication.myProfileId
import ru.planeta.api.web.controllers.services.BaseControllerService
import ru.planeta.api.web.controllers.services.BasePaymentControllerService
import ru.planeta.api.web.utils.ProperRedirectView
import ru.planeta.commons.model.OrderShortLinkStat
import ru.planeta.commons.web.CookieUtils
import ru.planeta.commons.web.IpUtils
import ru.planeta.commons.web.WebUtils
import ru.planeta.dao.payment.PaymentMethodDAO
import ru.planeta.dao.statdb.OrderGeoStatDAO
import ru.planeta.dao.trashcan.IviDao
import ru.planeta.eva.api.web.dto.RechargeBalanceDTO
import ru.planeta.eva.web.controllers.ProfileUrls
import ru.planeta.model.common.Address
import ru.planeta.model.common.InvestingOrderInfo
import ru.planeta.model.common.Order
import ru.planeta.model.common.PaymentMethod
import ru.planeta.model.common.campaign.enums.ShareStatus
import ru.planeta.model.enums.ModerateStatus
import ru.planeta.model.enums.OrderObjectType
import ru.planeta.model.enums.ProjectType
import ru.planeta.model.stat.OrderGeoStat
import ru.planeta.web.controllers.Urls
import java.math.BigDecimal
import java.net.URLEncoder
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

@Controller
class PaymentController(private val orderGeoStatDAO: OrderGeoStatDAO,
                        private val paymentMethodDAO: PaymentMethodDAO,
                        private val investOrderService: InvestingOrderInfoService,
                        private val paymentService: PaymentService,
                        private val campaignService: CampaignService,
                        private val orderService: OrderService,
                        private val paymentGateService: PaymentGateService,
                        private val deliveryAddressService: DeliveryAddressService,
                        private val paymentSettingsService: PaymentSettingsService,
                        private val projectService: ProjectService,
                        private val baseControllerService: BaseControllerService,
                        private val basePaymentControllerService: BasePaymentControllerService,
                        private val orderShortLinkStatService: OrderShortLinkStatService,
                        private val iviDao: IviDao) {

    private val logger = Logger.getLogger(PaymentController::class.java)

    @BillingLoggableRequest(initial = true)
    @RequestMapping(value = Urls.Payment.INCREASE_BALANCE, method = [RequestMethod.GET, RequestMethod.POST])
    fun increaseBalance(@RequestParam amount: BigDecimal,
                        @RequestParam paymentMethodId: Long,
                        @RequestParam(required = false) phone: String?): ModelAndView {

        val transaction = paymentService.createPayment(myProfileId(), amount, paymentMethodId, ProjectType.MAIN, phone ?: "")
        return basePaymentControllerService.createPaymentRedirectModelAndView(transaction)
    }

    @BillingLoggableRequest(initial = true)
    @RequestMapping(value = ru.planeta.api.web.controllers.Urls.Payment.PAYMENT_CREATE, method = [RequestMethod.GET, RequestMethod.POST])
    @Throws(RegistrationException::class, PermissionException::class)
    fun createCampaignPayment(@RequestParam amount: BigDecimal,
                              @RequestParam shareId: Long,
                              @RequestParam fromBalance: Boolean,
                              @RequestParam(defaultValue = "0") paymentMethodId: Long,
                              @RequestParam(required = false) phone: String?,
                              @RequestParam(defaultValue = "1") count: Int,
                              @RequestParam donateAmount: BigDecimal,
                              @RequestParam(required = false) email: String?,
                              @RequestParam(required = false) campaignId: Long?,
                              @RequestParam(required = false) comment: String?,
                              @RequestParam(required = false) zipCode: String?,
                              @RequestParam(required = false) country: String?,
                              @RequestParam(defaultValue = "0") countryId: Int,
                              @RequestParam(defaultValue = "0") cityId: Int,
                              @RequestParam(required = false) city: String?,
                              @RequestParam(required = false) street: String?,
                              @RequestParam(required = false) contactPhone: String?,
                              @RequestParam(required = false) customerName: String?,
                              @RequestParam(required = false) reply: String?,
                              @RequestParam(defaultValue = "0") serviceId: Long,
                              @RequestParam(required = false) investInfo: String?,
                              @RequestParam(defaultValue = "true") tlsSupported: Boolean,
                              @RequestParam(required = false) iviConfirm: Boolean?,
                              request: HttpServletRequest, response: HttpServletResponse): ModelAndView {
        val buyerId: Long
        try {
            buyerId = basePaymentControllerService.getRegisteredOrCreateProfileId(email, request, response)
        } catch (ex: PermissionException) {
            return ModelAndView(ProperRedirectView(formatCampaignWithShareUrl(campaignId, shareId, email)))
        } catch (ex: RegistrationException) {
            return ModelAndView(ProperRedirectView(formatCampaignWithShareUrl(campaignId, shareId, email)))
        }

        val deliveryInfo = basePaymentControllerService.getDeliveryInfo(shareId, serviceId)

        val address = Address()
        address.street = street
        address.city = city
        address.country = country
        address.zipCode = zipCode
        address.cityId = cityId
        address.countryId = countryId
        address.phone = contactPhone

        val deliveryAddress = deliveryAddressService.getDeliveryAddress(address, customerName)

        if (deliveryAddress != null) {
            deliveryAddress.phone = contactPhone
        }

        var projectType = ProjectType.MAIN
        val shareStatus = campaignService.getShareReadyForPurchase(shareId, count).shareStatus
        if (shareStatus == ShareStatus.INVESTING || shareStatus == ShareStatus.INVESTING_WITHOUT_MODERATION) {
            projectType = ProjectType.INVEST
        }

        if (shareStatus == ShareStatus.INVESTING_ALL_ALLOWED) {
            projectType = ProjectType.INVEST_ALL_ALLOWED
        }

        val order = orderService.createOrderWithShare(buyerId, buyerId, shareId,
                donateAmount, count, reply, deliveryAddress, deliveryInfo, projectType)
        try {
            val shortLinkId = CookieUtils.getCookieValue(request.getCookies(), OrderShortLinkStat.SHORTLINK_ID_COOKIE_NAME, "0").toLong()
            if (shortLinkId > 0) {
                val orderShortLinkStat = OrderShortLinkStat()
                orderShortLinkStat.orderId = order.orderId
                orderShortLinkStat.shortLinkId = CookieUtils.getCookieValue(request.getCookies(), OrderShortLinkStat.SHORTLINK_ID_COOKIE_NAME, "0").toLong()
                orderShortLinkStat.referrer = CookieUtils.getCookieValue(request.getCookies(), OrderShortLinkStat.SHORTLINK_REFERRER_COOKIE_NAME, "")
                orderShortLinkStatService.add(orderShortLinkStat)
            }
        } catch (ex: Exception) {
            logger.error("ERROR during trying to store shortlink cookie data to db, orderId = " + order.orderId)
            logger.error(ex)
        }

        if (projectType == ProjectType.INVEST_ALL_ALLOWED) {
            if (paymentMethodId == paymentMethodDAO.selectByAlias(PaymentMethod.Alias.INVOICE.toString()).id) {
                order.orderType = OrderObjectType.INVESTING_WITH_ALL_ALLOWED_INVOICE
            } else {
                order.orderType = OrderObjectType.SHARE
            }
        }
        val orderType = order.orderType
        val methodId = if (fromBalance && paymentMethodId == 0L) {
            logger.error("Should never happen")
            paymentSettingsService.internalPaymentMethod.id
        } else paymentMethodId

        if (iviConfirm == true) {
            try {
                iviDao.insert(order.orderId)
            } catch (ex: Exception) {
                logger.error(ex)
            }
        }

        if (methodId == paymentSettingsService.internalPaymentMethod.id) {
            try {
                paymentGateService.purchaseOrder(order.orderId)
            } catch (ex: Exception) {
                logger.error(ex)
                return baseControllerService.createRedirectModelAndView(projectService.getPaymentFailUrl(projectType))
            }

            addInfo(shareId, comment, investInfo, request, buyerId, shareStatus ?: ShareStatus.ACTIVE, order, orderType
                    ?: OrderObjectType.PRODUCT)
            return baseControllerService.createRedirectModelAndView(projectService.getPaymentSuccessUrlByOrder(projectType, order.orderId))

        } else {
            val transaction = paymentService.createOrderPayment(order, amount, methodId, projectType, phone, tlsSupported)
            addInfo(shareId, comment, investInfo, request, buyerId, shareStatus ?: ShareStatus.ACTIVE, order, orderType
                    ?: OrderObjectType.PRODUCT)
            return basePaymentControllerService.createPaymentRedirectModelAndView(transaction)
        }

    }

    private fun addInfo(shareId: Long, comment: String?, investInfo: String?, request: HttpServletRequest, buyerId: Long,
                        shareStatus: ShareStatus, order: Order, orderType: OrderObjectType) {
        if (investInfo != null) {
            val info = InvestingOrderInfo.create(investInfo)
            info.investingOrderInfoId = order.orderId
            info.moderateStatus = ModerateStatus.NEW
            if (shareStatus == ShareStatus.INVESTING_WITHOUT_MODERATION || orderType == OrderObjectType.INVESTING_WITH_ALL_ALLOWED_INVOICE) {
                info.moderateStatus = ModerateStatus.APPROVED
            }
            // TODO: Maybe another async?
            investOrderService.save(info)
        }

        // retreive buyer's geo info
        collectByerInfo(request, order)
        addDeferredDonateComment(shareId, comment, buyerId, order.orderId)
    }

    private fun collectByerInfo(request: HttpServletRequest, order: Order) {
        try {
            val host = IpUtils.getRemoteAddr(request)
            var referer = CookieUtils.getCookieValue(request.cookies, CookieUtils.EXTERNAL_REFERRER, null)
            if (StringUtils.isBlank(referer)) {
                referer = WebUtils.getReferer(request)
            } else if (referer.contains("planeta.ru") && StringUtils.isNotBlank(WebUtils.getReferer(request))) {
                referer = WebUtils.getReferer(request)
            }
            if (StringUtils.isNotBlank(host) || StringUtils.isNotBlank(referer)) {
                orderGeoStatDAO.insert(OrderGeoStat(order.orderId, host, referer))
            } else {
                logger.error("No referrer or host $host $referer")
            }
        } catch (ex: Exception) {
            logger.error(ex)
        }

    }

    @RequestMapping(value = ru.planeta.api.web.controllers.Urls.Payment.DELIVERY_PAYMENT_CREATE, method = [RequestMethod.GET, RequestMethod.POST])
    @ResponseBody
    @Throws(RegistrationException::class, PaymentException::class, PermissionException::class)
    fun createDeliveryPayment(@RequestParam amount: BigDecimal,
                              @RequestParam price: BigDecimal?,
                              @RequestParam fromBalance: Boolean,
                              @RequestParam(defaultValue = "0") paymentMethodId: Long,
                              @RequestParam(required = false) phone: String?,
                              @RequestParam(required = false) email: String?,
                              request: HttpServletRequest, response: HttpServletResponse): ActionStatus<*> {

        if (price == null || price.compareTo(BigDecimal.ZERO) == 0) {
            logger.error("No price")
            throw PaymentException(MessageCode.WRONG_PRODUCT_COST)
        }

        val buyerId = basePaymentControllerService.getRegisteredOrCreateProfileId(email, request, response)

        val order = orderService.createDeliveryOrder(myProfileId(), buyerId, price.max(amount), ProjectType.DELIVERY)
        var methodId = paymentMethodId
        var _amount = amount
        if (fromBalance && paymentMethodId == 0L) {
            methodId = paymentSettingsService.internalPaymentMethod.id
            _amount = BigDecimal.ZERO
        }

        if (_amount.compareTo(BigDecimal.ZERO) == 0) {
            methodId = paymentSettingsService.internalPaymentMethod.id
        }

        val transaction = paymentService.createOrderPayment(order, _amount, methodId, ProjectType.DELIVERY, phone, true)

        val url = projectService.getPaymentGateRedirectUrl(transaction)
        return ActionStatus.createSuccessStatus(url)
    }

    private fun addDeferredDonateComment(shareId: Long, commentText: String?, profileId: Long, orderId: Long) {
        if (StringUtils.isNotBlank(commentText)) {
            val comment = campaignService.createCampaignComment(profileId, shareId, commentText ?: "")
            basePaymentControllerService.saveDeferredComment(orderId, comment)
        }
    }

    private fun formatCampaignWithShareUrl(campaignId: Long?, shareId: Long, email: String?): String =
            Urls.CAMPAIGN_DONATE_WITH_SHARE.replace("{campaignId}", campaignId?.toString() ?: "")
                    .replace("{shareId}.html", shareId.toString())
                    .replace("{params}", "?emailInUse=" + URLEncoder.encode(email, "UTF-8"))

    /**
     * Пополнить баланс пользователя
     */
    @ResponseBody
    @PostMapping(ProfileUrls.RECHARGE_BALANCE)
    fun rechargeBalance(@RequestBody rechargeBalanceDTO: RechargeBalanceDTO): ru.planeta.eva.api.web.ActionStatus {
        val transaction = paymentService.createPayment(
                myProfileId(),
                rechargeBalanceDTO.amount,
                rechargeBalanceDTO.paymentData.paymentMethodId,
                ProjectType.MAIN,
                rechargeBalanceDTO.paymentData.paymentPhone ?: ""
        )

        return ru.planeta.eva.api.web.ActionStatus(result = projectService.getPaymentGateRedirectUrl(transaction))
    }
}

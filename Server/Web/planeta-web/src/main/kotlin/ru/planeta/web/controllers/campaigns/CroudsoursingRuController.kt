package ru.planeta.web.controllers.campaigns

import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.GetMapping
import ru.planeta.api.helper.ProjectService
import ru.planeta.api.service.campaign.CampaignService
import ru.planeta.api.service.profile.ProfileService
import ru.planeta.model.common.campaign.Campaign
import ru.planeta.model.enums.ProjectType
import ru.planeta.web.controllers.Urls
import ru.planeta.web.controllers.campaigns.model.CampaignInfo
import ru.planeta.web.controllers.campaigns.model.Pledge
import ru.planeta.web.controllers.campaigns.model.Pledges
import ru.planeta.web.controllers.campaigns.model.Projects
import java.util.*
import javax.servlet.http.HttpServletResponse
import javax.xml.bind.JAXBContext
import javax.xml.bind.Marshaller

@Controller
class CroudsoursingRuController(private val campaignService: CampaignService,
                                private val projectService: ProjectService,
                                private val profileService: ProfileService) {

    @GetMapping(Urls.FOR_CROUDSOURSING_RU)
    fun getActiveProjects(response: HttpServletResponse) {

        val stub = campaignService.getCampaignSafe(48481)

        val result = listOf(createCampaignInfoFromCampaign(stub))
        response.contentType = "application/xml"
        writeXml(response, result)
    }

    private fun writeXml(response: HttpServletResponse, result: List<CampaignInfo>) {
        val context = JAXBContext.newInstance(Projects::class.java)
        val marshaller = context.createMarshaller()
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true)
        val campaignInfoListHolder = Projects()
        campaignInfoListHolder.project = result
        marshaller.marshal(campaignInfoListHolder, response.outputStream)
    }

    private fun createCampaignInfoFromCampaign(campaign: Campaign): CampaignInfo {
        val campaignInfo = CampaignInfo(campaign)
        setShares(campaign.campaignId, campaignInfo)
        campaignInfo.user_name = profileService.getProfileSafe(campaign.profileId ?: -1).displayName
        campaign.mainTag?.let {
            campaignInfo.parent_category = campaign.mainTag?.name
            campaignInfo.category = campaignInfo.parent_category
        }
        campaignInfo.url = getCampaignUrl(campaign.webCampaignAlias ?: "")
        return campaignInfo
    }

    private fun getCampaignUrl(webCampaignAlias: String): String {
        return projectService.getUrl(ProjectType.MAIN, "/campaigns/$webCampaignAlias")
    }

    private fun setShares(campaignId: Long, campaignInfo: CampaignInfo) {
        val shares = campaignService.getCampaignSharesFiltered(campaignId)
        val pledges = ArrayList<Pledge>(shares.size)
        shares.mapTo(pledges) { Pledge(it) }
        val pledges1 = Pledges()
        pledges1.pledge = pledges
        campaignInfo.pledges = pledges1
    }
}

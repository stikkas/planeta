package ru.planeta.eva.web.converters

import com.fasterxml.jackson.databind.ObjectMapper
import org.springframework.http.HttpInputMessage
import org.springframework.http.HttpOutputMessage
import org.springframework.http.MediaType.APPLICATION_FORM_URLENCODED
import org.springframework.http.MediaType.APPLICATION_JSON_UTF8
import org.springframework.http.converter.AbstractHttpMessageConverter
import ru.planeta.eva.web.dto.AskManagerDTO

class AskManagerDTOMessageConverter : AbstractHttpMessageConverter<AskManagerDTO>() {

    override fun supports(p0: Class<*>): Boolean {
        return p0.isAssignableFrom(AskManagerDTO::class.java)
    }

    override fun writeInternal(dto: AskManagerDTO, message: HttpOutputMessage) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun readInternal(clazz: Class<out AskManagerDTO>?, message: HttpInputMessage): AskManagerDTO {
        val node = ObjectMapper().readTree(message.body);
        return AskManagerDTO(node.get("message")?.asText()
                ?: "", node.get("campaignId")?.asLong(), node.get("email")?.asText())
    }

    init {
        supportedMediaTypes = listOf(APPLICATION_JSON_UTF8, APPLICATION_FORM_URLENCODED)
    }
}

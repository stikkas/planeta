package ru.planeta.web.services

import org.apache.commons.collections4.CollectionUtils
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Service
import ru.planeta.api.model.ConfigurationType
import ru.planeta.api.model.promo.PartnersPromoConfiguration
import ru.planeta.api.service.configurations.ConfigurationService
import java.util.*
import java.util.concurrent.locks.ReentrantReadWriteLock

@Service
class CuratorsServiceImpl(private val configurationService: ConfigurationService) : CuratorsService {

    private val partnersLock = ReentrantReadWriteLock()


    override
    val welcomeCurators: List<PartnersPromoConfiguration>
        get() {
            var partners = readPartners()
            CollectionUtils.filter(partners) { `object` -> `object`.isCurator }
            Collections.shuffle(partners)
            if (partners.size >  MAX_CURATORS) {
                partners = partners.subList(0, MAX_CURATORS);
            }
            return partners
        }

    override
    val welcomePartners: List<PartnersPromoConfiguration>
        get() {
            var partners = readPartners()
            CollectionUtils.filter(partners) { `object` -> `object`.isWelcome }
            Collections.shuffle(partners)
            if (partners.size >  MAX_PARTNERS) {
                partners = partners.subList(0, MAX_PARTNERS)
            }
            return partners
        }

    @Scheduled(fixedDelay = PARTNERS_UPDATE_DELAY.toLong(), initialDelay = 0)
    fun fillPartnersCache() {
        partnersLock.writeLock().lock()
        try {
            partners.clear()
            partners.addAll(updatePartners())
        } finally {
            partnersLock.writeLock().unlock()
        }
    }

    private fun updatePartners(): List<PartnersPromoConfiguration> {
        return configurationService.getJsonArrayConfig(PartnersPromoConfiguration::class.java, ConfigurationType.PLANETA_PARTNERS_PROMO_CONFIGURATION_LIST)
    }

    private fun readPartners(): List<PartnersPromoConfiguration> {
        partnersLock.readLock().lock()
        try {
            if (partners.isEmpty()) {
                partnersLock.readLock().unlock()
                fillPartnersCache()
                partnersLock.readLock().lock()
            }
            return ArrayList(partners)
        } finally {
            partnersLock.readLock().unlock()
        }
    }

    companion object {
        private val partners = ArrayList<PartnersPromoConfiguration>()
        private const val PARTNERS_UPDATE_DELAY = 15 * 60 * 1000 // 15  minutes
        private val MAX_CURATORS = 12
        private val MAX_PARTNERS = 5
    }
}

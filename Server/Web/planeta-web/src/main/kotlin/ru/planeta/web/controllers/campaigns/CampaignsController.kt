package ru.planeta.web.controllers.campaigns

import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import ru.planeta.api.model.ActionStatus
import ru.planeta.api.service.campaign.CampaignService
import ru.planeta.api.service.configurations.ConfigurationService
import ru.planeta.api.service.profile.GroupService
import ru.planeta.api.web.authentication.myProfileId
import ru.planeta.model.common.campaign.ShareDetails
import ru.planeta.web.controllers.Urls

@RestController
class CampaignsController(private val groupService: GroupService,
                          private val configurationService: ConfigurationService,
                          private val campaignService: CampaignService) {

    @RequestMapping(Urls.MY_RETAINED_CAMPAIGNS)
    fun rules(): ActionStatus<*> {
        val userGroups = groupService.getGroupsByUser(myProfileId())
        return ActionStatus.createSuccessStatus(userGroups)
    }

    @RequestMapping(Urls.GET_CUSTOM_HTML)
    fun customHtml(): ActionStatus<String> = ActionStatus.createSuccessStatus(configurationService.fundingRulesCustomHtml)

    @GetMapping(Urls.GET_SHARE_INFO)
    fun getShareInfo(@RequestParam("shareId") shareId: Long): ActionStatus<ShareDetails> {
        return ActionStatus.createSuccessStatus(campaignService.getDetailedShareForClient(myProfileId(), shareId))
    }

}


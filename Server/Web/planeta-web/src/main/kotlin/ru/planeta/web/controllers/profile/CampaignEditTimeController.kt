package ru.planeta.web.controllers.profile

import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import ru.planeta.api.model.ActionStatus
import ru.planeta.api.service.campaign.CampaignEditTimeService
import ru.planeta.api.web.authentication.myProfileId
import ru.planeta.web.controllers.Urls

@RestController
class CampaignEditTimeController(private val campaignEditTimeService: CampaignEditTimeService) {

    @PostMapping(Urls.CAMPAING_EDIT_TRACK)
    fun updateAndCheck(@RequestParam campaignId: Long): ActionStatus<*> {
        campaignEditTimeService.updateAndCheck(campaignId, myProfileId())
        return ActionStatus.createSuccessStatus<Any>()
    }
}

package ru.planeta.web.controllers.campaigns.admin

import org.apache.commons.lang3.StringUtils
import org.apache.commons.lang3.time.DateUtils
import org.apache.log4j.Logger
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.validation.BindingResult
import org.springframework.validation.ValidationUtils
import org.springframework.web.bind.annotation.*
import ru.planeta.api.exceptions.NotFoundException
import ru.planeta.api.exceptions.PermissionException
import ru.planeta.api.model.ActionStatus
import ru.planeta.api.news.LoggerService
import ru.planeta.api.service.campaign.CampaignPermissionService
import ru.planeta.api.service.campaign.CampaignService
import ru.planeta.api.service.interactive.InteractiveEditorDataService
import ru.planeta.api.service.security.PermissionService
import ru.planeta.api.utils.ValidateUtils
import ru.planeta.api.web.authentication.isAnonymous
import ru.planeta.api.web.authentication.myProfileId
import ru.planeta.api.web.controllers.services.BaseControllerService
import ru.planeta.dao.trashcan.RefererDAO
import ru.planeta.model.common.campaign.Campaign
import ru.planeta.model.common.campaign.CampaignTag
import ru.planeta.model.enums.ProfileType
import ru.planeta.model.enums.ProjectType
import ru.planeta.model.news.ProfileNews
import ru.planeta.model.stat.RefererStatType
import ru.planeta.model.trashcan.InteractiveEditorData
import ru.planeta.web.controllers.Urls
import ru.planeta.web.controllers.campaigns.validation.DraftCampaignValidator
import java.math.BigDecimal
import java.math.RoundingMode
import java.util.*
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse
import javax.validation.Valid

@RestController
class CampaignEditController(private val campaignPermissionService: CampaignPermissionService,
                             private val draftValidator: DraftCampaignValidator,
                             private val refererDAO: RefererDAO,
                             private val interactiveEditorDataService: InteractiveEditorDataService,
                             private val baseControllerService: BaseControllerService,
                             private val campaignService: CampaignService,
                             private val permissionService: PermissionService) {
    @Autowired
    protected lateinit var profileNewsServiceMy: LoggerService

    @PostMapping(Urls.Admin.DRAFT_CAMPAIGN)
    fun saveCampaign(@RequestBody campaign: Campaign, result: BindingResult,
                     @RequestParam(defaultValue = "false") force: Boolean,
                     request: HttpServletRequest,
                     response: HttpServletResponse): ActionStatus<Campaign> {
        if (campaign.campaignId > 0) {
            val currentCampaign = campaignService.getCampaignSafe(campaign.campaignId)
            if (campaign.status != currentCampaign.status) {
                throw PermissionException()
            }
        }

        draftValidator.validate(campaign, result)
        if (result.hasErrors()) {
            return baseControllerService.createErrorStatus(result)
        }
        val campaignActionStatus = saveCampaign(campaign, force, request, response)
        profileNewsServiceMy.addProfileNews(ProfileNews.Type.SAVE_DRAFT_CAMPAIGN, myProfileId(), campaign.campaignId, campaign.campaignId)
        return campaignActionStatus
    }

    @PostMapping(Urls.Admin.CAMPAIGN_SAVE)
    fun publishCampaign(@Valid @RequestBody campaign: Campaign, result: BindingResult,
                        @RequestParam(defaultValue = "false") force: Boolean,
                        request: HttpServletRequest,
                        response: HttpServletResponse): ActionStatus<Campaign> {
        if (result.hasErrors()) {
            return baseControllerService.createErrorStatus(result)
        }
        if (StringUtils.isBlank(campaign.campaignAlias)) {
            campaign.campaignAlias = null
        } else {
            campaign.campaignAlias = campaign.campaignAlias?.toLowerCase()
        }

        saveCampaign(campaign, force, request, response)
        profileNewsServiceMy.addProfileNews(ProfileNews.Type.SAVE_CAMPAIGN, myProfileId(), campaign.campaignId, campaign.campaignId)
        campaignService.publishCampaign(myProfileId(), campaign.campaignId)

        return ActionStatus.createSuccessStatus(campaign)
    }

    @PostMapping(Urls.Admin.CAMPAIGN_SAVE_EMAIL_STEP)
    fun validateCampaignEmailStep(): ActionStatus<*> {
        if (isAnonymous()) {
            throw PermissionException()
        }

        increaseInteractiveReachedStepNum(STEP_EMAIL_ID)
        return ActionStatus.createSuccessStatus<Any>()
    }

    @PostMapping(Urls.Admin.CAMPAIGN_CREATE_INFO_STEP)
    fun createCampaignInfoStep(@RequestBody campaign: Campaign, errors: BindingResult,
                               @RequestParam(defaultValue = "false") force: Boolean,
                               request: HttpServletRequest,
                               response: HttpServletResponse): ActionStatus<Campaign> {

        draftValidator.validate(campaign, errors)
        if (errors.hasErrors()) {
            return baseControllerService.createErrorStatus(errors)
        }
        val campaignActionStatus = saveCampaign(campaign, force, request, response)
        val myProfileId = myProfileId()
        profileNewsServiceMy.addProfileNews(ProfileNews.Type.CREATE_DRAFT_CAMPAIGN_INTERACTIVE_STEP_INFO, myProfileId,
                campaignActionStatus.result.campaignId,
                campaignActionStatus.result.campaignId)

        var interactiveEditorData: InteractiveEditorData? = interactiveEditorDataService.getByUserId(myProfileId)
        if (interactiveEditorData == null) {
            interactiveEditorData = InteractiveEditorData()
            interactiveEditorData.userId = myProfileId
            interactiveEditorData.campaignId = campaignActionStatus.result.campaignId
            interactiveEditorDataService.insert(interactiveEditorData)
        } else {
            interactiveEditorData.campaignId = campaignActionStatus.result.campaignId
            interactiveEditorDataService.update(interactiveEditorData)
        }

        return campaignActionStatus
    }

    @PostMapping(Urls.Admin.CAMPAIGN_SAVE_PROCEED_STEP)
    fun validateCampaignProceedStep(): ActionStatus<*> {
        if (isAnonymous()) {
            throw PermissionException()
        }

        increaseInteractiveReachedStepNum(STEP_PROCEED_ID)
        return ActionStatus.createSuccessStatus<Any>()
    }

    @PostMapping(Urls.Admin.CAMPAIGN_SAVE_INFO_STEP)
    fun validateCampaignInfoStep(@RequestBody campaign: Campaign, result: BindingResult,
                                 @RequestParam(defaultValue = "false") force: Boolean,
                                 request: HttpServletRequest,
                                 response: HttpServletResponse): ActionStatus<Campaign> {
        ValidationUtils.rejectIfEmptyOrWhitespace(result, "name", "field.required")
        if (!StringUtils.isEmpty(campaign.name)) {
            ValidateUtils.rejectIfSizeIsTooLarge(result, "name", 45, baseControllerService.messageSource)
        }

        ValidationUtils.rejectIfEmptyOrWhitespace(result, "shortDescription", "field.required")
        ValidateUtils.rejectIfSizeIsTooLarge(result, "shortDescription", 180, baseControllerService.messageSource)
        ValidateUtils.rejectIfContainsNotValidString(result, "shortDescription", "wrong.chars")

        if (campaign.tags.size < 1) {
            result.rejectValue("tags", "atleast.one.tag.required")
        } else if (campaign.tags.size > 3) {
            if (!permissionService.hasAdministrativeRole(myProfileId())) {
                result.rejectValue("tags", "nomore.then.three.tags")
            }
        }

        ValidationUtils.rejectIfEmpty(result, "imageUrl", "field.required")
        if (result.getFieldValue("imageUrl") === "null") {
            result.rejectValue("imageUrl", "field.required")
        }

        if (result.hasErrors()) {
            return baseControllerService.createErrorStatus(result)
        }
        if (StringUtils.isBlank(campaign.campaignAlias)) {
            campaign.campaignAlias = null
        } else {
            campaign.campaignAlias = campaign.campaignAlias?.toLowerCase()
        }

        saveCampaign(campaign, force, request, response)
        profileNewsServiceMy.addProfileNews(ProfileNews.Type.SAVE_CAMPAIGN_INTERACTIVE_STEP_INFO, myProfileId(), campaign.campaignId, campaign.campaignId)

        increaseInteractiveReachedStepNum(STEP_INFO_ID)
        return ActionStatus.createSuccessStatus(campaign)
    }

    @PostMapping(Urls.Admin.CAMPAIGN_SAVE_DURATION_STEP)
    fun validateCampaignDurationStep(@RequestBody campaign: Campaign, errors: BindingResult,
                                     @RequestParam(defaultValue = "false") force: Boolean,
                                     request: HttpServletRequest,
                                     response: HttpServletResponse): ActionStatus<Campaign> {

        if (!permissionService.hasAdministrativeRole(myProfileId())) {
            val charity = campaignService.getCampaignTagByMnemonic(CampaignTag.CHARITY)
            if (!campaign.tags.contains(charity)) {
                ValidationUtils.rejectIfEmpty(errors, "timeFinish", "date.expected")
                val finishedTime = campaign.timeFinish
                if (campaign.status?.isDraft == true && finishedTime != null) {
                    val now = DateUtils.truncate(Date(), Calendar.DATE)
                    val finish = DateUtils.truncate(finishedTime, Calendar.DATE)
                    if (now.after(finish)) {
                        errors.rejectValue("timeFinish", "campaign.timeFinish.before.now")
                    } else if (DateUtils.addDays(now, 99).before(finish)) {
                        errors.rejectValue("timeFinish", "campaign.timeFinish.exceed")
                    }

                }
            }

        }

        if (errors.hasErrors()) {
            return baseControllerService.createErrorStatus(errors)
        }

        saveCampaign(campaign, force, request, response)
        profileNewsServiceMy.addProfileNews(ProfileNews.Type.SAVE_CAMPAIGN_INTERACTIVE_STEP_DURATION, myProfileId(), campaign.campaignId, campaign.campaignId)

        increaseInteractiveReachedStepNum(STEP_DURATION_ID)
        return ActionStatus.createSuccessStatus(campaign)
    }

    @PostMapping(Urls.Admin.CAMPAIGN_SAVE_PRICE_STEP)
    fun validateCampaignPriceStep(@RequestBody campaign: Campaign, errors: BindingResult,
                                  @RequestParam(defaultValue = "false") force: Boolean,
                                  request: HttpServletRequest,
                                  response: HttpServletResponse): ActionStatus<Campaign> {
        if (!permissionService.hasAdministrativeRole(myProfileId())) {
            val charity = campaignService.getCampaignTagByMnemonic(CampaignTag.CHARITY)
            if (!campaign.tags.contains(charity)) {
                val targetAmount = campaign.targetAmount
                ValidationUtils.rejectIfEmpty(errors, "targetAmount", "field.required")
                if (!isIntegerValue(targetAmount ?: BigDecimal.ZERO) || targetAmount?.compareTo(BigDecimal.ZERO) == 0 ||
                        (targetAmount
                                ?: BigDecimal.ZERO) < BigDecimal(10000) || (targetAmount?.setScale(2, RoundingMode.HALF_UP)?.precision()
                                ?: 0) > 10) {
                    errors.rejectValue("targetAmount", "wrong.number.more.than.10000")
                }
            }

        }

        if (errors.hasErrors()) {
            return baseControllerService.createErrorStatus(errors)
        }

        saveCampaign(campaign, force, request, response)
        profileNewsServiceMy.addProfileNews(ProfileNews.Type.SAVE_CAMPAIGN_INTERACTIVE_STEP_PRICE, myProfileId(), campaign.campaignId, campaign.campaignId)

        increaseInteractiveReachedStepNum(STEP_PRICE_ID)
        return ActionStatus.createSuccessStatus(campaign)
    }

    @PostMapping(Urls.Admin.CAMPAIGN_SAVE_VIDEO_STEP)
    fun validateCampaignVideoStep(@RequestBody campaign: Campaign, errors: BindingResult,
                                  @RequestParam(defaultValue = "false") force: Boolean,
                                  request: HttpServletRequest,
                                  response: HttpServletResponse): ActionStatus<Campaign> {

        if (StringUtils.isEmpty(campaign.viewImageUrl)) {
            errors.rejectValue("videoContent", "field.required")
        }

        if (errors.hasErrors()) {
            return baseControllerService.createErrorStatus(errors)
        }
        if (StringUtils.isBlank(campaign.campaignAlias)) {
            campaign.campaignAlias = null
        } else {
            campaign.campaignAlias = campaign.campaignAlias?.toLowerCase()
        }

        saveCampaign(campaign, force, request, response)
        profileNewsServiceMy.addProfileNews(ProfileNews.Type.SAVE_CAMPAIGN_INTERACTIVE_STEP_VIDEO, myProfileId(), campaign.campaignId, campaign.campaignId)

        increaseInteractiveReachedStepNum(STEP_VIDEO_ID)
        return ActionStatus.createSuccessStatus(campaign)
    }

    @PostMapping(Urls.Admin.CAMPAIGN_SAVE_DESCRIPTION_STEP)
    fun validateCampaignDescriptionStep(@RequestBody campaign: Campaign, errors: BindingResult,
                                        @RequestParam(defaultValue = "false") force: Boolean,
                                        request: HttpServletRequest,
                                        response: HttpServletResponse): ActionStatus<Campaign> {

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "descriptionHtml", "field.required")

        if (errors.hasErrors()) {
            return baseControllerService.createErrorStatus(errors)
        }
        if (StringUtils.isBlank(campaign.campaignAlias)) {
            campaign.campaignAlias = null
        } else {
            campaign.campaignAlias = campaign.campaignAlias?.toLowerCase()
        }

        saveCampaign(campaign, force, request, response)
        profileNewsServiceMy.addProfileNews(ProfileNews.Type.SAVE_CAMPAIGN_INTERACTIVE_STEP_DESCRIPTION, myProfileId(), campaign.campaignId, campaign.campaignId)

        increaseInteractiveReachedStepNum(STEP_DESCRIPTION_ID)
        return ActionStatus.createSuccessStatus(campaign)
    }

    @PostMapping(Urls.Admin.CAMPAIGN_SAVE_REWARD_STEP)
    fun validateCampaignRewardStep(@RequestBody campaign: Campaign, errors: BindingResult,
                                   @RequestParam(defaultValue = "false") force: Boolean,
                                   request: HttpServletRequest,
                                   response: HttpServletResponse): ActionStatus<Campaign> {

        if (campaign.shares.size < MIN_SHARES_COUNT_IN_CAMPAIGN) {
            errors.rejectValue("shares", "campaign.shares.empty") //
        }

        if (errors.hasErrors()) {
            return baseControllerService.createErrorStatus(errors)
        }
        if (StringUtils.isBlank(campaign.campaignAlias)) {
            campaign.campaignAlias = null
        } else {
            campaign.campaignAlias = campaign.campaignAlias?.toLowerCase()
        }

        saveCampaign(campaign, force, request, response)
        profileNewsServiceMy.addProfileNews(ProfileNews.Type.SAVE_CAMPAIGN_INTERACTIVE_STEP_REWARD, myProfileId(), campaign.campaignId, campaign.campaignId)

        increaseInteractiveReachedStepNum(STEP_REWARD_ID)
        return ActionStatus.createSuccessStatus(campaign)
    }

    @PostMapping(Urls.Admin.CAMPAIGN_SAVE_COUNTERPARTY_STEP)
    fun validateCampaignCounterpartyStep(): ActionStatus<*> {
        if (isAnonymous()) {
            throw PermissionException()
        }

        increaseInteractiveReachedStepNum(STEP_COUNTERPARTY_ID)
        return ActionStatus.createSuccessStatus<Any>()
    }

    @PostMapping(Urls.Admin.CAMPAIGN_SAVE_FINAL_STEP)
    fun validateCampaignFinalStep(): ActionStatus<*> {
        if (isAnonymous()) {
            throw PermissionException()
        }

        increaseInteractiveReachedStepNum(STEP_CHECK_ID)
        return ActionStatus.createSuccessStatus<Any>()
    }

    private fun increaseInteractiveReachedStepNum(stepId: Int): Boolean {
        val interactiveEditorData = interactiveEditorDataService.getByUserId(myProfileId()) ?: return true

        if (stepId == interactiveEditorData.reachedStepNum) {
            interactiveEditorData.reachedStepNum = stepId + 1
        }
        interactiveEditorDataService.update(interactiveEditorData)
        return false
    }

    @PostMapping(Urls.Admin.CREATE_CAMPAIGN_BY_ADMIN)
    fun createCampaignByAdmin(@RequestParam profileId: Long,
                              request: HttpServletRequest,
                              response: HttpServletResponse): ActionStatus<Long> {
        if (!permissionService.hasAdministrativeRole(myProfileId())) {
            throw PermissionException()
        }

        var profile = baseControllerService.profileService.getProfileSafe(profileId)
        val profileType = profile.profileType
        if (profileType == ProfileType.USER) {
            profile = baseControllerService.profileService.getOrCreateHiddenGroup(profileId)
        } else if (profileType != ProfileType.GROUP && profileType != ProfileType.HIDDEN_GROUP) {
            throw NotFoundException()
        }
        val campaign = campaignService.insertCampaign(myProfileId(), profileId, profile.profileId)

        campaignLifeCycleTrack(campaign, request, response)

        return ActionStatus.createSuccessStatus(campaign.campaignId)
    }

    @GetMapping(Urls.Admin.DRAFT_CAMPAIGN)
    fun getCampaignForEdit(@RequestParam campaignId: Long): ActionStatus<Campaign> {
        val campaign = campaignService.getDraftCampaign(myProfileId(), campaignId)
        return ActionStatus.createSuccessStatus(campaign)
    }

    @PostMapping(Urls.Admin.DRAFT_CAMPAIGN_UNDO_CHANGES)
    fun draftCampaignUndoChanges(@RequestParam campaignId: Long): ActionStatus<Campaign> {
        val campaign = campaignService.undoChanges(myProfileId(), campaignId)
        return ActionStatus.createSuccessStatus(campaign)
    }

    @PostMapping(Urls.Admin.CAMPAIGN_CHANGE_BACKGROUND)
    fun setCampaignBackgroundUrl(@RequestParam campaignId: Long,
                                 @RequestParam src: String): ActionStatus<Campaign> {
        val campaign = campaignService.updateCampaignBackground(myProfileId(), campaignId, src)
        return ActionStatus.createSuccessStatus(campaign)
    }

    @PostMapping(Urls.Admin.TOGGLE_DRAFT_VISIBLE)
    fun toggleDraftVisible(@RequestParam campaignId: Long): ActionStatus<*> {
        campaignService.toggleDraftVisible(myProfileId(), campaignId)
        return ActionStatus.createSuccessStatus<Any>()
    }

    private fun saveCampaign(campaign: Campaign, force: Boolean,
                             request: HttpServletRequest, response: HttpServletResponse): ActionStatus<Campaign> {
        var campaign1 = campaign
        val ps = baseControllerService.profileService
        val profile = ps.getProfileSafe(campaign1.profileId ?: -1)
        val creatorProfileId = profile.profileId
        var groupId = creatorProfileId
        if (profile.profileType == ProfileType.USER) {
            val hiddenGroup = ps.getOrCreateHiddenGroup(creatorProfileId) ?: throw NotFoundException()
            groupId = hiddenGroup.profileId
        }
        campaign1.profileId = groupId
        val clientId = myProfileId()
        if (campaign1.campaignId == 0L) {
            log.warn("Save campaign with zero id")
            campaign1 = campaignService.insertCampaign(clientId, creatorProfileId, groupId)

            campaignLifeCycleTrack(campaign1, request, response)
        } else {
            campaignPermissionService.checkIsEditable(clientId, campaign1)
            campaignService.saveCampaign(clientId, campaign1, force)
        }

        return ActionStatus.createSuccessStatus(campaign1)
    }

    private fun campaignLifeCycleTrack(campaign: Campaign, request: HttpServletRequest, response: HttpServletResponse) {
        val requestStat = baseControllerService.getRequestStat(request, response, RefererStatType.CAMPAIGN_CREATE, ProjectType.MAIN)
        requestStat.externalId = campaign.campaignId
        refererDAO.insert(requestStat)
    }

    companion object {
        private val MIN_SHARES_COUNT_IN_CAMPAIGN = 2
        val STEP_EMAIL_ID = 2
        val STEP_PROCEED_ID = 3
        val STEP_INFO_ID = 4
        val STEP_DURATION_ID = 5
        val STEP_PRICE_ID = 6
        val STEP_VIDEO_ID = 7
        val STEP_DESCRIPTION_ID = 8
        val STEP_REWARD_ID = 9
        val STEP_COUNTERPARTY_ID = 10
        val STEP_CHECK_ID = 11
        val STEP_FINAL_ID = 12

        private val log = Logger.getLogger(CampaignEditController::class.java)

        private fun isIntegerValue(targetAmount: BigDecimal): Boolean {
            return try {
                targetAmount.toBigIntegerExact()
                true
            } catch (e: ArithmeticException) {
                false
            }

        }
    }

}

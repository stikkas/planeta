package ru.planeta.web.controllers.welcome

import org.apache.log4j.Logger
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.*
import org.springframework.web.servlet.ModelAndView
import ru.planeta.api.model.ActionStatus
import ru.planeta.api.service.common.BannerService
import ru.planeta.api.web.authentication.myProfileId
import ru.planeta.api.web.controllers.services.BaseControllerService
import ru.planeta.model.common.Banner
import ru.planeta.model.enums.BannerType
import ru.planeta.model.enums.ProjectType
import ru.planeta.web.controllers.Actions
import ru.planeta.web.controllers.Urls
import javax.servlet.http.HttpServletResponse

@Controller
class RotatorController constructor(private val bannerService: BannerService,
                                    private val baseControllerService: BaseControllerService) {

    @GetMapping(Urls.ROTATOR_RELOAD)
    fun nextBanner(): ModelAndView {
        bannerService.reloadFromDb(myProfileId())
        return baseControllerService.createRedirectModelAndView(
                "${baseControllerService.projectService.getUrl(ProjectType.ADMIN)}/admin/banners-list.html")
    }

    @GetMapping(Urls.GET_RANDOM_BANNER)
    fun getRandomBanner(@RequestParam(value = "bannerType", required = false) bannerType: BannerType?,
                        @RequestParam(value = "previousIds[]", required = false) previousIds: List<Long>?,
                        @RequestParam(value = "requestUrl", required = false) requestUrl: String?,
                        @RequestParam(value = "isMobile", defaultValue = "false") isMobile: Boolean,
                        @RequestParam(value = "isAuthor", defaultValue = "false") isAuthor: Boolean,
                        response: HttpServletResponse): ModelAndView {

        val banner = getBanner(bannerType, previousIds, requestUrl, isAuthor)
        val modelAndView = ModelAndView(Actions.BANNER.text)
        modelAndView.addObject("banner", banner)
        modelAndView.addObject("isMobile", isMobile)
        response.setHeader("Access-Control-Allow-Origin", "*")
        return modelAndView
    }

    @RequestMapping(value = Urls.PREVIEW_BANNER, method = [RequestMethod.GET, RequestMethod.POST])
    fun getRandomBanner(banner: Banner, response: HttpServletResponse): ModelAndView {
        val modelAndView = ModelAndView(Actions.BANNER.text)
        modelAndView.addObject("banner", banner)
        modelAndView.addObject("isMobile", false)
        response.setHeader("Access-Control-Allow-Origin", "*")
        return modelAndView
    }

    @ResponseBody
    @GetMapping(Urls.GET_RANDOM_BANNER_JSON)
    fun getRandomBannerJson(@RequestParam(value = "bannerType", required = false) bannerType: BannerType?,
                            @RequestParam(value = "previousIds[]", required = false) previousIds: List<Long>?,
                            @RequestParam(value = "isAuthor", defaultValue = "false") isAuthor: Boolean,
                            @RequestParam(value = "requestUrl", required = false) requestUrl: String?): ActionStatus<Banner> =
            ActionStatus.createSuccessStatus(getBanner(bannerType, previousIds, requestUrl, isAuthor))

    @ResponseBody
    @GetMapping(Urls.GET_PROMO_BANNERS)
    fun getPromoBanners(@RequestParam(value = "bannerType", required = false) bannerType: BannerType?,
                        @RequestParam(value = "isAuthor", defaultValue = "false") isAuthor: Boolean): ActionStatus<List<Banner>> =
            ActionStatus.createSuccessStatus(getBanners(bannerType, null, null, isAuthor))

    @Scheduled(fixedDelay = UPDATE_BANNERS_DELAY)
    fun reload() {
        log.info("Banner reload")
        bannerService.reload()
    }

    private fun getBanner(bannerType: BannerType?, previousIds: List<Long>?, requestUrl: String?, isAuthor: Boolean): Banner? =
            bannerService.selectRandom(myProfileId(), isAuthor, bannerType, previousIds, requestUrl)

    private fun getBanners(bannerType: BannerType?, previousIds: List<Long>?, requestUrl: String?, isAuthor: Boolean): List<Banner>? =
            bannerService.selectBanners(myProfileId(), isAuthor, bannerType, previousIds, requestUrl)

    companion object {

        private const val UPDATE_BANNERS_DELAY = (10 * 60 * 1000).toLong() // ten minutes

        private val log = Logger.getLogger(RotatorController::class.java)
    }
}







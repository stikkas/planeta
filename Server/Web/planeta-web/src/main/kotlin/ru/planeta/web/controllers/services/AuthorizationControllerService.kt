package ru.planeta.web.controllers.services

import org.springframework.stereotype.Service
import org.springframework.web.servlet.ModelAndView
import ru.planeta.api.Utils
import ru.planeta.api.utils.CASErrorUtils
import ru.planeta.api.web.controllers.services.BaseControllerService
import ru.planeta.model.enums.ProjectType
import ru.planeta.web.controllers.Actions
import ru.planeta.web.controllers.Urls
import java.net.URLDecoder
import java.util.*
import javax.servlet.http.HttpServletRequest

@Service
class AuthorizationControllerService(private val baseControllerService: BaseControllerService) {

    fun createErrorModelAndView(errorCode: String): ModelAndView =
            baseControllerService.createRedirectModelAndView(Urls.ROOT, mapOf("errorCode" to errorCode))


    fun generatedWelcomeModelAndView(error: Boolean, errorCode: String?, username: String?, request: HttpServletRequest): ModelAndView {

        val modelAndView = baseControllerService.createDefaultModelAndView(Actions.INDEX.text, ProjectType.MAIN)
        modelAndView.addObject("needStartRouter", true)

        modelAndView.addObject("error", error)
        errorCode?.let {
            if (it.isNotBlank()) {
                val afterAuthorizationRedirectUrl = request.getParameter("afterAuthorizationRedirectUrl")
                afterAuthorizationRedirectUrl?.let {
                    if (it.isNotEmpty()) {
                        val decodedUrl = URLDecoder.decode(it, "UTF-8")
                        if (Utils.isPlanetaUrl(decodedUrl)) {
                            modelAndView.addObject("afterAuthorizationRedirectUrl", decodedUrl)
                        }
                    }
                }

                modelAndView.addObject("errorCode", it)
                val casError = CASErrorUtils.getCASErrorByErrorCode(it)
                if (casError != null) {
                    val errorMessage = baseControllerService
                            .messageSource.getMessage(casError.messageCode, null, "Bad credentials", Locale.getDefault())
                    modelAndView.addObject("errorMessage", errorMessage)
                    modelAndView.addObject("username", username)
                }
            }
        }
        return modelAndView
    }

    fun createDefaultModelAndView(action: Actions): ModelAndView = baseControllerService.createDefaultModelAndView(action, ProjectType.MAIN)
}

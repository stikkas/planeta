package ru.planeta.eva.web.dto

class SubscriptionsDTO {

    /**
     * Получать письма с информацией из моих проектов
     */
    var receiveMyCampaignNewsletters = false

    /**
     * Получать рассылки планеты
     */
    var receiveNewsletters = false
}

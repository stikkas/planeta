package ru.planeta.web.services

import ru.planeta.api.model.promo.PartnersPromoConfiguration

interface CuratorsService {
    val welcomeCurators: List<PartnersPromoConfiguration>
    val welcomePartners: List<PartnersPromoConfiguration>
}

package ru.planeta.web.controllers.util

import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.servlet.ModelAndView
import ru.planeta.web.controllers.Actions
import ru.planeta.web.controllers.Urls
import ru.planeta.web.controllers.services.MainBaseControllerService

@Controller
class OpenSearchController(private val mainBaseControllerService: MainBaseControllerService) {

    @GetMapping(Urls.OPEN_SEARCH)
    fun getManifest(): ModelAndView = mainBaseControllerService.defaultModelAndView(Actions.OPEN_SEARCH)
}

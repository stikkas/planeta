package ru.planeta.web.controllers.welcome

import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.ResponseBody
import org.springframework.web.servlet.ModelAndView
import ru.planeta.api.exceptions.MessageCode
import ru.planeta.api.exceptions.PermissionException
import ru.planeta.api.model.ActionStatus
import ru.planeta.api.service.notifications.NotificationService
import ru.planeta.api.service.profile.QuickstartGetBookService
import ru.planeta.api.service.security.PermissionService
import ru.planeta.api.web.authentication.myProfileId
import ru.planeta.model.commondb.QuickstartGetBook
import ru.planeta.web.controllers.Actions
import ru.planeta.web.controllers.Urls
import ru.planeta.web.controllers.services.MainBaseControllerService

@Controller
class AuthorPromoLandingController(private val notificationService: NotificationService,
                                   private val quickstartGetBookService: QuickstartGetBookService,
                                   private val mainBaseControllerService: MainBaseControllerService,
                                   private val permissionService: PermissionService) {

    @GetMapping(Urls.QUICKSTART_MUSIC)
    fun quickstartMusic(): ModelAndView = getModelAndViewWithState(
            createDefaultQuickstartModelAndView(Actions.QUICKSTART_MUSIC)
                    .addObject("quickstartType", "music"))

    @GetMapping(Urls.QUICKSTART_MOVIE)
    fun quickstartMovie(): ModelAndView = getModelAndViewWithState(
            createDefaultQuickstartModelAndView(Actions.QUICKSTART_MOVIE)
                    .addObject("quickstartType", "movie"))

    @GetMapping(Urls.QUICKSTART_BOOK)
    fun quickstartBook(): ModelAndView = getModelAndViewWithState(
            createDefaultQuickstartModelAndView(Actions.QUICKSTART_BOOK)
                    .addObject("quickstartType", "book"))

    @GetMapping(Urls.QUICKSTART_BUSINESS)
    fun quickstartBusiness(): ModelAndView = getModelAndViewWithState(
            createDefaultQuickstartModelAndView(Actions.QUICKSTART_BUSINESS)
                    .addObject("quickstartType", "business"))

    @GetMapping(Urls.QUICKSTART_GET_BOOK)
    @ResponseBody
    fun sendGetBookEmail(@RequestParam(value = "email") email: String,
                         @RequestParam(value = "type") type: String): ActionStatus<*> {
        if (quickstartGetBookService.alreadySendLastDay(email, type)) {
            throw PermissionException(MessageCode.TOO_MANY_REQUESTS)
        }
        notificationService.sendQuickstartGetBook(email, type)
        quickstartGetBookService.insertQuickstartGetBook(QuickstartGetBook.builder
                .email(email)
                .profileId(myProfileId())
                .bookType(type)
                .build())
        return ActionStatus.createSuccessStatus<Any>()
    }

    private fun getModelAndViewWithState(modelAndView: ModelAndView): ModelAndView {
        val clientId = myProfileId()
        if (permissionService.isAnonymous(clientId)) {
            modelAndView.addObject("isAuthorized", false)
        } else {
            modelAndView.addObject("isAuthorized", true)
        }
        return modelAndView
    }

    private fun createDefaultQuickstartModelAndView(action: Actions): ModelAndView = mainBaseControllerService.defaultModelAndView(action.text, action.actionName)
}

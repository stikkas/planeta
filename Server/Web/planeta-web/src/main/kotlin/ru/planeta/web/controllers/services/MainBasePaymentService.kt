package ru.planeta.web.controllers.services

import org.springframework.stereotype.Service
import ru.planeta.api.web.controllers.services.BasePaymentService
import ru.planeta.model.common.Order
import ru.planeta.model.common.TopayTransaction
import ru.planeta.model.enums.OrderObjectType
import ru.planeta.web.controllers.Actions

@Service
class MainBasePaymentService : BasePaymentService {

    override fun getPaymentSuccessActionName(order: Order?): String {
        order?.let {
            return when (order.orderType) {
                OrderObjectType.SHARE -> Actions.SHARE_PAYMENT_SUCCESS.text
                OrderObjectType.INVESTING -> Actions.INVESTING_PAYMENT_SUCCES.text
                OrderObjectType.INVESTING_WITH_ALL_ALLOWED_INVOICE, OrderObjectType.INVESTING_WITHOUT_MODERATION -> Actions.INVESTING_PAYMENT_SUCCES_WITHOUT_MODERATION.text
                else -> Actions.PAYMENT_SUCCESS.text
            }
        }
        return Actions.PAYMENT_SUCCESS.text
    }

    override fun getPaymentSourceUrl(transaction: TopayTransaction): String? = ru.planeta.api.web.controllers.Urls.Member.ACCOUNT_PAYMENTS
}

package ru.planeta.eva.web.dto

/**
 * Данные, передаваемые с клиента из формы "Связаться с менеджером"
 */
data class AskManagerDTO(val message: String, val campaignId: Long?, val email: String? = null)

package ru.planeta.web.controllers.profile

import org.springframework.validation.BindingResult
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import ru.planeta.api.model.ActionStatus
import ru.planeta.api.model.PasswordChange
import ru.planeta.api.model.json.ProfileGeneralSettings
import ru.planeta.api.news.LoggerService
import ru.planeta.api.service.profile.ProfileSettingsService
import ru.planeta.api.web.authentication.getAuthentication
import ru.planeta.api.web.authentication.myProfileId
import ru.planeta.api.web.controllers.services.ControllerAutoLoginWrapService
import ru.planeta.model.news.ProfileNews
import ru.planeta.model.profile.Profile
import ru.planeta.web.controllers.Urls
import javax.validation.Valid

@RestController
class UsersController(private val profileSettingsService: ProfileSettingsService,
                      private val profileNewsService: LoggerService,
                      private val controllerAutoLoginWrapService: ControllerAutoLoginWrapService) {

    /**
     * Saves user's general settings
     *
     * @return JSON object with this action status
     */

    @PostMapping(Urls.USER_SAVE_PASSWORD)
    fun savePrivateInfo(@Valid change: PasswordChange, result: BindingResult): ActionStatus<*> {
        val bcs = controllerAutoLoginWrapService.baseControllerService
        if (result.hasErrors()) {
            return bcs.createErrorStatus<Any>(result)
        }

        val myProfileId = myProfileId()
        val info = bcs.authorizationService.saveUserPassword(myProfileId, change)
        //update authentication token
        val authentication = getAuthentication()
        if (authentication != null) {
            controllerAutoLoginWrapService.updateAuthenticationToken(authentication.principal, info.password ?: "", authentication.authorities)
        }
        profileNewsService.addProfileNews(ProfileNews.Type.PROFILE_PASSWORD_CHANGED, myProfileId, info.userId)

        return ActionStatus.createSuccessStatus<Any>()
    }

    @PostMapping(Urls.PROFILE_SAVE_GENERAL_SETTINGS)
    fun profileSaveGeneralSettings(@Valid profileGeneralSettings: ProfileGeneralSettings, result: BindingResult): ActionStatus<Profile> {
        if (result.hasErrors()) {
            return controllerAutoLoginWrapService.baseControllerService.createErrorStatus(result)
        }

        val myProfileId = myProfileId()
        val userinfo = profileSettingsService.saveProfileGeneralSettings(myProfileId, profileGeneralSettings)
        profileNewsService.addProfileNews(ProfileNews.Type.PROFILE_SETTINGS_CHANGED, myProfileId, profileGeneralSettings.profileId)
        return ActionStatus.createSuccessStatus(userinfo)
    }

    @PostMapping(Urls.PROFILE_UNSUBSCRIBE)
    fun profileUnsubscribe(@RequestParam profileId: Long, @RequestParam unsubscribe: Boolean): ActionStatus<*> {
        val myProfileId = myProfileId()
        profileSettingsService.unsubscribeUserFromSpam(myProfileId, profileId, unsubscribe)
        profileNewsService.addProfileNews(ProfileNews.Type.PROFILE_SETTINGS_CHANGED, myProfileId, profileId)
        return ActionStatus.createSuccessStatus<Any>()
    }
}

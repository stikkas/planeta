package ru.planeta.concert.controllers

import org.junit.Assert.assertNotNull
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit4.SpringRunner
import ru.planeta.api.service.billing.order.AfterFundServiceFactory
import ru.planeta.model.enums.OrderObjectType

/**
 * Created by a.savanovich on 19.10.2016.
 */

@RunWith(SpringRunner::class)
@ContextConfiguration(locations = ["classpath:spring/applicationContext-*.xml",
    "classpath*:/spring/applicationContext-dao.xml",
    "classpath*:/spring/applicationContext-geo.xml"])
@WebMvcTest
class ConcertFactoryTest {

    @Autowired
    lateinit var afterFundServiceFactory: AfterFundServiceFactory

    @Test
    fun test() {
        val concertService = afterFundServiceFactory.get(OrderObjectType.TICKET)
        assertNotNull(concertService)
    }
}
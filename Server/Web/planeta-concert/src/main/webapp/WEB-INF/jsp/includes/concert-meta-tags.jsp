<!-- Sharing meta data: start -->
<meta property="og:site_name" content="Concert.planeta.ru"/>

<c:choose>
    <c:when test="${not empty customMetaTag.image}">
        <meta property="og:image" content="${hf:getThumbnailUrl(customMetaTag.image, "ORIGINAL", "VIDEO")}" />
    </c:when>
    <c:otherwise>
        <meta property="og:image" content="https://${hf:getStaticBaseUrl("")}/images/charity/charity-stats-img.png" />
    </c:otherwise>
</c:choose>

<c:choose>
    <c:when test="${not empty customMetaTag.ogTitle}">
        <meta property="og:title" content="${customMetaTag.ogTitle}" />
    </c:when>
    <c:otherwise>
        <meta property="og:title" content="Концерты на Planeta.ru" />
    </c:otherwise>
</c:choose>

<c:choose>
    <c:when test="${not empty customMetaTag.ogDescription}">
        <meta property="og:description" content="${customMetaTag.ogDescription}" />
    </c:when>
    <c:otherwise>
        <meta property="og:description" content="Концерты на Planeta.ru" />
    </c:otherwise>
</c:choose>
<!-- Sharing meta data: end -->
<c:choose>
    <c:when test="${not empty customMetaTag.title}">
        <title>${customMetaTag.title}</title>
    </c:when>
    <c:otherwise>
        <title>Концерты на Planeta.ru</title>
    </c:otherwise>
</c:choose>

<c:choose>
    <c:when test="${not empty customMetaTag.description}">
        <meta name="description" content="${customMetaTag.description}"/>
    </c:when>
    <c:otherwise>
        <meta name="description" content="Концерты на Planeta.ru"/>
    </c:otherwise>
</c:choose>

<c:if test="${not empty customMetaTag.keywords}">
    <meta name="keywords" content="${customMetaTag.keywords}"/>
</c:if>
<meta name="viewport" content="width=device-width">

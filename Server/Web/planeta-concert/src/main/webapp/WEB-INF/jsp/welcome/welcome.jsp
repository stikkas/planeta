<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ include file="/WEB-INF/jsp/includes/generated/doctype-header.jsp" %>
<%@taglib prefix="p" uri="http://planeta.ru/tags" %>
<head>
    <%@include file="/WEB-INF/jsp/includes/generated/stat-counters-async.jsp"%>
    <%@ include file="/WEB-INF/jsp/includes/head.jsp" %>
    <%@ include file="/WEB-INF/jsp/includes/generated/main-page-meta-header.jsp" %>
    <%@ include file="/WEB-INF/jsp/includes/concert-meta-tags.jsp" %>
</head>

<body class="grid-1200">
<%@ include file="/WEB-INF/jsp/includes/generated/irma/header.jsp" %>

<div id="global-container">
    <div id="main-container" class="wrap-container">
        <div id="center-container">
            <c:forEach items="${concerts}" var="concert">
                <a href="/concert/${concert.concertId}">${concert.title}</a>
            </c:forEach>
        </div>
    </div>
</div>

<%@ include file="/WEB-INF/jsp/includes/generated/irma/footer.jsp" %>
</body>
</html>


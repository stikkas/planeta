<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ include file="/WEB-INF/jsp/includes/generated/doctype-header.jsp" %>
<%@taglib prefix="p" uri="http://planeta.ru/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<head>
    <%@include file="/WEB-INF/jsp/includes/generated/stat-counters-async.jsp"%>
    <%@ include file="/WEB-INF/jsp/includes/head.jsp" %>
    <%@ include file="/WEB-INF/jsp/includes/generated/main-page-meta-header.jsp" %>
    <%@ include file="/WEB-INF/jsp/includes/concert-meta-tags.jsp" %>

    <script type="text/javascript">
        $(document).ready(function() {
            $(".js-make-order").click(function() {
                var ticketData = _.map($('input:checked'), function(item) {
                    return {
                        "actionID": $(item).data('actionId'),
                        "sectionID": $(item).data('sectionId'),
                        "rowID": $(item).data('rowId'),
                        "paid" : false,
                        "count" : 1,
                        "position" : $(item).data('position'),
                        "price" : $(item).data('price')
                    };
                });

                var data = {
                    amount: 1000000,
                    hasEmail: ${hasEmail},
                    email: $('#email').val(),
                    paymentMethodId: 4,
                    needPhone: false,
                    orderItemList : ticketData
                };

                console.log(data);

                $.ajax('/api/public/place-order.json', _.extend({
                    data: JSON.stringify(data),
                    dataType: 'json',
                    type: 'post',
                    contentType : 'application/json',
                    success:function (response) {
                        console.log(response);
                    }
                }));
            });
        });

        var actionSegments = [];
        var segmentLines = [];
        var segmentPlaces = [];

    </script>

</head>

<body class="grid-1200">
<%@ include file="/WEB-INF/jsp/includes/generated/header.jsp" %>

<p:script src="concert.js" />

<div id="global-container">
    <div id="main-container" class="wrap-container">
        <div id="center-container">
            <a href="/concert/${concert.concertId}">${concert.title}</a>
            <img src="${concert.imageUrl}" />
            <p>${concert.description}</p>
            <p>${concert.concertDate}</p>
            <img src="${concert.schemeUrl}" />
            <canvas id="big_canvas_map" style="cursor: pointer;"></canvas>

            <hr />
            <c:if test="${hasEmail == false}">
                <input type="text" id="email" />
            </c:if>

            <button type="button" class="btn btn-primary js-make-order">Купить</button>

            <p>Список мест</p>
            <c:forEach items="${places}" var="place">
                <input type="checkbox" name="${place.placeId}"
                          data-action-id="${place.externalConcertId}"
                          data-section-id="${place.sectionId}"
                          data-row-id="${place.rowId}"
                          data-position="${place.position}"
                          data-price="${place.price}">
                    <b> name </b>${place.name}
                    <b> sectionId </b>${place.sectionId}
                    <b> rowId </b>${place.rowId}
                    <b> position </b>${place.position}
                    <b> price </b>${place.price}
                    <b> x </b>${place.x}
                    <b> y </b>${place.y}
                    <b> state </b>${place.state}
                </input>
                <br/>
            </c:forEach>

        </div>
    </div>
</div>

<%@ include file="/WEB-INF/jsp/includes/footer.jsp" %>
</body>
</html>


package ru.planeta.concert

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.builder.SpringApplicationBuilder
import org.springframework.boot.web.support.SpringBootServletInitializer
import org.springframework.context.MessageSource
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableOAuth2Client
import ru.planeta.api.web.utils.JstlHelperFunctions
import java.util.*

@SpringBootApplication(scanBasePackages = ["ru.planeta"])
@EnableOAuth2Client
class WebConfiguration : SpringBootServletInitializer() {

    override fun configure(builder: SpringApplicationBuilder): SpringApplicationBuilder {
        return builder.sources(WebConfiguration::class.java)
    }

    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            val context = SpringApplication.run(ConcertApp::class.java, *args)
            // Нужно для tlds
            JstlHelperFunctions.setProperties(context.getBean("properties", Properties::class.java))
            JstlHelperFunctions.setMessageSource(context.getBean(MessageSource::class.java))
        }
    }
}
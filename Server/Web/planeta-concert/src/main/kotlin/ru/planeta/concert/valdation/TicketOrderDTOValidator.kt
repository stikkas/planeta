package ru.planeta.concert.valdation

import org.springframework.validation.ValidationUtils
import ru.planeta.concert.model.TicketOrderDTO
import org.springframework.stereotype.Component
import org.springframework.validation.Errors
import org.springframework.validation.Validator
import ru.planeta.api.utils.ValidateUtils
import ru.planeta.dao.Statements
import ru.planeta.model.common.PaymentMethod
import ru.planeta.model.shop.enums.PaymentType

/**
 * Created with IntelliJ IDEA.
 * User: michail
 * Date: 17.10.16
 * Time: 18:25
 */
@Component
class TicketOrderDTOValidator : Validator {
    override fun supports(clazz: Class<*>): Boolean = TicketOrderDTO::class.java.isAssignableFrom(clazz)

    override fun validate(target: Any, errors: Errors) {
        val ticketOrderDTO = target as TicketOrderDTO
        if (!ValidateUtils.isValidEmail(ticketOrderDTO.email)) {
            errors.rejectValue("email", "wrong.email")
        }

        if (ticketOrderDTO.isNeedPhone) {
            ValidationUtils.rejectIfEmptyOrWhitespace(errors, "phone", "phone.required")
            if (!ValidateUtils.isValidPhone(ticketOrderDTO.phone)) {
                errors.rejectValue("phone", "phone.required")
            }
        }

        if (ticketOrderDTO.paymentMethodId == 0L) {
            errors.rejectValue("paymentMethodId", "payment.required")
        }
    }
}

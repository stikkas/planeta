package ru.planeta.concert.controllers

import ru.planeta.api.web.controllers.IAction

enum class Actions(prefix: ActionPrefixes, text: String) : IAction {
    WELCOME(ActionPrefixes.WELCOME, "welcome"),
    CONCERT(ActionPrefixes.CONCERT, "concert"),
    PAYMENT_SUCCESS(ActionPrefixes.WELCOME, "payment-success"),
    PAYMENT_FAIL(ActionPrefixes.COMMON_INCLUDES, "payment-fail");

    val text: String = prefix.toString() + text
    override val path: String
        get() = text
    override val actionName: String = text
    override fun toString(): String = text
}

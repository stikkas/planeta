package ru.planeta.concert

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.autoconfigure.security.oauth2.resource.ResourceServerProperties
import org.springframework.boot.autoconfigure.security.oauth2.resource.UserInfoTokenServices
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.context.annotation.*
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter
import org.springframework.security.core.Authentication
import org.springframework.security.oauth2.client.OAuth2ClientContext
import org.springframework.security.oauth2.client.OAuth2RestTemplate
import org.springframework.security.oauth2.client.filter.OAuth2ClientAuthenticationProcessingFilter
import org.springframework.security.oauth2.client.token.AccessTokenRequest
import org.springframework.security.oauth2.client.token.grant.password.ResourceOwnerPasswordResourceDetails
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableOAuth2Client
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter
import org.springframework.web.context.request.RequestContextListener
import ru.planeta.api.service.authentication.AuthenticatedUserDetailsService
import javax.servlet.Filter
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse


@ImportResource(locations = ["classpath*:/ru/planeta/spring/propertyConfigurer.xml",
    "classpath*:/spring/applicationContext-global.xml",
    "classpath*:/spring/applicationContext-dao.xml",
    "classpath*:/spring/applicationContext-geo.xml",
    "classpath:/spring/applicationContext-common.xml"
])
@Configuration
//@EnableOAuth2Client
class ConcertApp : WebSecurityConfigurerAdapter() {

    @Bean("userService")
    fun userServcie() = AuthenticatedUserDetailsService()
//
//    @Autowired
//    lateinit var oauth2ClientContext: OAuth2ClientContext

    override fun configure(http: HttpSecurity) {
        http.authorizeRequests()
                .antMatchers("/")
                .permitAll()
                .anyRequest().permitAll()
                .and()
                .formLogin().loginPage("/api/public/login.json").permitAll()
//                .addFilterBefore(ssoFilter(), BasicAuthenticationFilter::class.java)
    }

//    private fun ssoFilter(): Filter {
//        val filter = OAuthAuthenticationProcessingFilter("/api/public/login.json")
//        val template = OAuth2RestTemplate(authServer(), oauth2ClientContext)
//        filter.setRestTemplate(template)
//
//        val tokenServices = UserInfoTokenServices(authResource().userInfoUri, authServer().clientId)
//        tokenServices.setRestTemplate(template)
//        filter.setTokenServices(tokenServices)
//        return filter
//    }
//
//    @Bean
//    @ConfigurationProperties("auth-server.client")
////    @Scope(value = "session", proxyMode = ScopedProxyMode.TARGET_CLASS)
//    fun authServer(): ResourceOwnerPasswordResourceDetails = ResourceOwnerPasswordResourceDetails()
//
//    @Bean
//    @ConfigurationProperties("auth-server.resource")
//    fun authResource(): ResourceServerProperties = ResourceServerProperties()
//
//    @Bean
//    fun requestContextListener(): RequestContextListener {
//        return RequestContextListener()
//    }
}

class OAuthAuthenticationProcessingFilter(defaultFilterProcessesUrl: String) : OAuth2ClientAuthenticationProcessingFilter(defaultFilterProcessesUrl) {

    override fun attemptAuthentication(request: HttpServletRequest, response: HttpServletResponse): Authentication {
        val details = restTemplate.resource as ResourceOwnerPasswordResourceDetails
        details.username = request.getParameter("username")
        details.password = request.getParameter("password")
        return super.attemptAuthentication(request, response)
    }
}

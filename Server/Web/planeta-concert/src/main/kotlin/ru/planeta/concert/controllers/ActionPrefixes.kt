package ru.planeta.concert.controllers

enum class ActionPrefixes(private val text: String) {
    WELCOME("welcome/"),
    CONCERT("concert/"),
    STATIC("static/"),
    COMMON_INCLUDES("includes/generated/");

    override fun toString(): String = text
}

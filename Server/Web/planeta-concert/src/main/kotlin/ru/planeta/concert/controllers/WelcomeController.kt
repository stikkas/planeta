package ru.planeta.concert.controllers

import org.apache.http.HttpResponse
import org.apache.http.client.HttpClient
import org.apache.http.client.entity.UrlEncodedFormEntity
import org.apache.http.client.methods.HttpPost
import org.apache.http.impl.client.HttpClientBuilder
import org.apache.http.message.BasicNameValuePair
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.servlet.ModelAndView
import ru.planeta.api.service.concert.ConcertService
import ru.planeta.api.web.controllers.services.BaseControllerService
import ru.planeta.model.enums.ProjectType
import java.io.BufferedReader
import java.io.InputStreamReader
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

/**
 * Created with IntelliJ IDEA.
 * User: michail
 * Date: 12.10.16
 * Time: 15:07
 */

@Controller
class WelcomeController(private val concertService: ConcertService,
                        private val baseControllerService: BaseControllerService) {

    @GetMapping(Urls.ROOT)
    fun welcome(): ModelAndView = baseControllerService.createDefaultModelAndView(Actions.WELCOME, ProjectType.CONCERT)
            .addObject("concerts", concertService.selectList(0, 100))

    @RequestMapping("/api/public/login1.json")
    fun login(@RequestParam username: String, @RequestParam password: String): ModelAndView {
        val uri = "http://localhost:9090/uaa/oauth/token"
        val client = HttpClientBuilder.create().build()
        val post = HttpPost(uri)
        post.setHeader("User-Agent",
                "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36")
        val urlParameters = ArrayList<BasicNameValuePair>()
        urlParameters.add(BasicNameValuePair("grant_type", "password"))
        urlParameters.add(BasicNameValuePair("client_id", "my-client-with-secret"))
        urlParameters.add(BasicNameValuePair("client_secret", "secret"))
        urlParameters.add(BasicNameValuePair("username", username))
        urlParameters.add(BasicNameValuePair("password", password))
        post.entity = UrlEncodedFormEntity(urlParameters)
        val response = client.execute(post)
        System.out.println("Response Code : " + response.statusLine.statusCode)
        val rd = BufferedReader(InputStreamReader(response.entity.content))
        val result = StringBuffer()
        var line1 = rd.readLine()
        while (line1 != null) {
            result.append(line1)
            line1 = rd.readLine()
        }
        System.out.println(result)
        return welcome()
    }
}

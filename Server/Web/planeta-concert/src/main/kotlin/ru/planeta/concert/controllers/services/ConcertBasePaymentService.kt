package ru.planeta.concert.controllers.services

import org.springframework.stereotype.Service
import ru.planeta.api.web.controllers.services.BasePaymentService
import ru.planeta.concert.controllers.Actions
import ru.planeta.model.common.Order
import ru.planeta.model.common.TopayTransaction

@Service
class ConcertBasePaymentService : BasePaymentService {
    override fun getPaymentSuccessActionName(order: Order?): String = Actions.PAYMENT_SUCCESS.text

    override fun getPaymentSourceUrl(transaction: TopayTransaction): String? = ru.planeta.api.web.controllers.Urls.Member.ACCOUNT_PAYMENTS
}

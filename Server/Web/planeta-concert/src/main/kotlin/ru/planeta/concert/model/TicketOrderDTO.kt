package ru.planeta.concert.model

import ru.planeta.moscowshow.model.OrderItem

import java.math.BigDecimal

/**
 * Created with IntelliJ IDEA.
 * User: michail
 * Date: 17.10.16
 * Time: 17:37
 */
class TicketOrderDTO {
    var email: String? = null
    var orderItemList: Array<OrderItem>? = null

    var amount: BigDecimal? = null
    var paymentMethodId: Long = 0
    var phone: String? = null
    var isNeedPhone: Boolean = false
}

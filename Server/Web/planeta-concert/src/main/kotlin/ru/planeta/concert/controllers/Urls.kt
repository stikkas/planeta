package ru.planeta.concert.controllers

object Urls {
    const val ROOT = "/"
    const val CONCERT = "/concert/{concertId}"

    const val PLACEORDER = "/api/public/place-order.json"
}

package ru.planeta.concert.controllers

import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.servlet.ModelAndView
import ru.planeta.api.exceptions.NotFoundException
import ru.planeta.api.service.concert.ConcertService
import ru.planeta.api.service.concert.PlaceService
import ru.planeta.api.service.concert.SectionService
import ru.planeta.api.web.controllers.services.BaseControllerService
import ru.planeta.model.enums.ProjectType

/**
 * Created with IntelliJ IDEA.
 * User: michail
 * Date: 13.10.16
 * Time: 14:01
 */

@Controller
class ConcertController(private val concertService: ConcertService,
                        private val sectionService: SectionService,
                        private val placeService: PlaceService,
                        private val baseControllerService: BaseControllerService) {

    @GetMapping(Urls.CONCERT)
    fun productView(@PathVariable("concertId") concertId: Long): ModelAndView {
        // TODO populate model with Concert and other necessary objects
        if (concertId > 0) {
            val concert = concertService.getConcertSafe(concertId)
            val sectors = sectionService.getSectorsByExternalConcertId(concert.externalConcertId)
            val rows = sectionService.getRowsByExternalConcertId(concert.externalConcertId, null)
            val places = placeService.getPlacesByExternalConcertId(concert.externalConcertId)

            return baseControllerService.createDefaultModelAndView(Actions.CONCERT, ProjectType.CONCERT)
                    .addObject("concert", concert)
                    .addObject("sectors", sectors)
                    .addObject("rows", rows)
                    .addObject("places", places)
        } else {
            throw NotFoundException()
        }
    }
}


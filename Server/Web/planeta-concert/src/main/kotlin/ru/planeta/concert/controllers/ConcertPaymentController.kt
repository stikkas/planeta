package ru.planeta.concert.controllers

import org.springframework.context.MessageSource
import org.springframework.stereotype.Controller
import org.springframework.validation.BindingResult
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.ResponseBody
import ru.planeta.api.exceptions.PermissionException
import ru.planeta.api.exceptions.RegistrationException
import ru.planeta.api.helper.ProjectService
import ru.planeta.api.model.ActionStatus
import ru.planeta.api.service.billing.order.ConcertAfterFundService
import ru.planeta.api.service.billing.payment.PaymentService
import ru.planeta.api.service.concert.PlaceService
import ru.planeta.api.web.controllers.services.BasePaymentControllerService
import ru.planeta.concert.model.TicketOrderDTO
import ru.planeta.model.enums.ProjectType
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse
import javax.validation.Valid

/**
 * Created with IntelliJ IDEA.
 * User: michail
 * Date: 17.10.16
 * Time: 13:05
 */
@Controller
class ConcertPaymentController(private val placeService: PlaceService,
                               private val concertAfterFundService: ConcertAfterFundService,
                               private val messageSource: MessageSource,
                               private val paymentService: PaymentService,
                               private val projectService: ProjectService,
                               private val basePaymentControllerService: BasePaymentControllerService) {

    @ResponseBody
    @PostMapping(Urls.PLACEORDER)
    @Throws(RegistrationException::class, PermissionException::class)
    fun placeOrders(@Valid @RequestBody ticketOrderDTO: TicketOrderDTO,
                    result: BindingResult, request: HttpServletRequest, response: HttpServletResponse): ActionStatus<String> {
        if (result.hasErrors()) {
            return ActionStatus.createErrorStatus(result, messageSource)
        }

        val buyerId = basePaymentControllerService.getRegisteredOrCreateProfileId(ticketOrderDTO.email, request, response)
        val places = placeService.prepareMoscowShowOrder(ticketOrderDTO.orderItemList?.toList())
        val order = concertAfterFundService.createOrder(buyerId, places)
        val transaction = paymentService.createOrderPayment(order, ticketOrderDTO.amount,
                ticketOrderDTO.paymentMethodId, ProjectType.CONCERT, ticketOrderDTO.phone, true)
        val url = projectService.getPaymentGateRedirectUrl(transaction)
        return ActionStatus.createSuccessStatus(url)
    }
}

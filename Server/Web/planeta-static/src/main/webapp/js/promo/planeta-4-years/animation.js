var backPlanet = {
    init: function () {

        $('body').addClass('animation-init');

        var controller = new ScrollMagic.Controller();



        // page animation
        $.each( $('.timeline'), function (i, el) {
            $.each(  $('.timeline_i', el), function (i, el) {
                $(el).addClass('timeline-row-' + (i+1));
            });
        });


        $.each( $('.page'), function (i, el) {
            (function (i, el) {
                var lastHit = 'start';

                var page = $(el).find( '.page-wrap' );
                var pageHeight = page.height();

                new ScrollMagic.Scene({
                    duration: pageHeight,
                    triggerHook: .5,
                    offset: 0,
                    triggerElement: page[0]
                })
                    .on("start end", function (e) {
                        lastHit = e.type;
                    })
                    .on("enter leave", function (e) {
                        if ( lastHit == "start" && e.type == "enter" ) {
                            $(el).addClass('active');
                        } else if ( lastHit == "start" && e.type == "leave" ) {
                            $(el).removeClass('active');
                        }
                    })
                    .addTo(controller);

                if ( i > 0 && i != 5 ) {
                    new ScrollMagic.Scene({
                        duration: "500",
                        triggerHook: 0,
                        triggerElement: el
                    })
                        .setPin(el)
                        .addTo(controller);
                }

            })(i, el);
        });





        // year pinned
        $.each( $('.year'), function (i, el) {
            new ScrollMagic.Scene({
                duration: "100%",
                triggerHook: 0,
                triggerElement: el
            })
                .setPin(el)
                .addTo(controller);
        });





        // year nav
        $.each( $('.year-wrap'), function (i, el) {
            var yearNav = new ScrollMagic.Scene({
                triggerElement: el
            })
                .setClassToggle("#year-nav-" + (i+1), "active")
                .addTo(controller);

            $(window).on("resize.yearNav", _.debounce(updateYearNavDuration, 1000));
            updateYearNavDuration();

            function updateYearNavDuration() {
                var duration = $(el).height();
                yearNav.duration(duration);
            }
        });






        // planet back translate
        $(window).on("resize.planetTranslate", _.debounce(updatePlanetTranslateDuration, 1000));

        function updatePlanetTranslateDuration() {
            var duration = $('body').height() - $(window).height();
            planetTranslate.duration(duration);
        }



        var planetTranslate = new ScrollMagic.Scene({
            duration: 0,
            triggerHook: 0,
            triggerElement: "body"
        })
            .on("progress", function (e) {
                if ( e.progress > .95 ) {
                    $('.page-scroll').addClass('hide');
                } else {
                    $('.page-scroll').removeClass('hide');
                }
            })
            .addTo(controller);

        updatePlanetTranslateDuration();
        // /planet back translate




        // add global tween
        $(window).on('resize.rocketRatio', _.debounce(changeRocketRatio, 1000));
        changeRocketRatio();

        function changeRocketRatio() {
            var planetTranslateTween = new TimelineMax();
            var globalTweens = [];


            var rocketRatio = $(window).height() / 1080;
            rocketRatio = Math.min(Math.max(rocketRatio, 0), 1);



            // rocket
            var rocket = $('.rocket');
            TweenMax.set(rocket, { clearProps:"all" } );
            TweenMax.set(rocket, { css:{
                scale: rocketRatio,
                marginBottom: -450 * rocketRatio
            } });
            globalTweens.push(
                TweenMax.to(rocket, 1, {y: -450 * rocketRatio, ease: Linear.easeNone})
            );




            // rocket smoke
            var rocketSmokeWrap = $('.rocket-smoke-wrap');

            var rocketSmoke1 = $('.rocket-smoke-1');
            TweenMax.set(rocketSmoke1, { clearProps:"all" } );
            TweenMax.set(rocketSmoke1, { css:{
                scale: rocketRatio,
                marginBottom: -155 * rocketRatio
            } });

            var rocketSmoke2 = $('.rocket-smoke-2');
            TweenMax.set(rocketSmoke2, { clearProps:"all" } );
            TweenMax.set(rocketSmoke2, { css:{
                scale: rocketRatio,
                marginBottom: -51 * rocketRatio
            } });

            globalTweens.push(
                TweenMax.to({}, 1, {
                    onUpdate: function (tl) {
                        var tlp = tl.progress();


                        TweenMax.set(rocketSmokeWrap, {y: 70 * tlp, ease: Linear.easeNone});

                        if ( tlp > .5 ) {
                            tlp = 1 - tlp;
                        }

                        TweenMax.set(rocketSmoke1, {y: -150 * rocketRatio * tlp, ease: Linear.easeNone});
                        TweenMax.set(rocketSmoke2, {y: -51 * rocketRatio * tlp, ease: Linear.easeNone});
                    },
                    onUpdateParams: ["{self}"]
                })
            );




            // planet back translate
            var planetBack = $('.planet-back-wrap');
            TweenMax.set(planetBack, { clearProps:"all" } );
            globalTweens.push(
                TweenMax.to(planetBack, 1, {y: 250, ease: Linear.easeNone})
            );



            // hue for planet back
            var planetSlide = $('.planet-slide');
            globalTweens.push(
                TweenMax.to({}, 1, {
                    onUpdate: function (tl) {
                        var firstEndY = -35;

                        var intersectionY = -32;
                        var intersectionX = intersectionY / firstEndY;

                        var secondB = intersectionY / ( 1 - intersectionX );


                        var tlp = tl.progress() * firstEndY;

                        if ( tlp < intersectionY ) {
                            tlp = tl.progress() * -secondB + secondB;
                        }

                        TweenMax.set(planetSlide, {
                            '-webkit-filter': 'hue-rotate(' + tlp + 'deg)',
                            'filter': 'hue-rotate(' + tlp + 'deg)'
                        });
                    },
                    onUpdateParams: ["{self}"]
                })
            );



            // star mask translate
            globalTweens.push(
                TweenMax.to({}, 1, {
                    onUpdate: function (tl) {
                        maskY = tl.progress() * 250;
                    },
                    onUpdateParams: ["{self}"]
                })
            );




            planetTranslateTween.add(globalTweens);

            planetTranslate.setTween(planetTranslateTween);
        }
        // /add global tween



    }
};



var stars = {
    init: function () {
        particlesJS("particles-js", {
            "particles": {
                "number": {
                    "value": 100,
                    "density": {
                        "enable": true,
                        "value_area": 800
                    }
                },
                "color": {
                    "value": "#ffffff"
                },
                "shape": {
                    "type": "circle",
                    "stroke": {
                        "width": 0
                    }
                },
                "opacity": {
                    "value": 1,
                    "random": true,
                    "anim": {
                        "enable": true,
                        "speed": 1,
                        "opacity_min": 0,
                        "sync": false
                    }
                },
                "size": {
                    "value": 1.8,
                    "random": true,
                    "anim": {
                        "enable": false
                    }
                },
                "line_linked": {
                    "enable": false
                },
                "move": {
                    "enable": true,
                    "speed": 0.6,
                    "direction": "none",
                    "random": true,
                    "straight": false,
                    "out_mode": "out",
                    "bounce": false,
                    "attract": {
                        "enable": false,
                        "rotateX": 600,
                        "rotateY": 600
                    }
                }
            },
            "interactivity": {
                "detect_on": "canvas",
                "events": {
                    "onhover": {
                        "enable": false,
                        "mode": "bubble"
                    },
                    "onclick": {
                        "enable": false,
                        "mode": "repulse"
                    },
                    "resize": true
                }
            },
            "retina_detect": true
        });




        var mask = new Image();
        mask.src = workspace.staticNodesService.getResourceUrl('/images/promo/planeta-4-years/planet-mask.png');

        var customPJS = pJSDom[0].pJS;
        customPJS.fn.particlesDraw = function(){

            var pJS = customPJS;

            pJS.canvas.ctx.globalCompositeOperation = 'source-over';

            pJS.canvas.ctx.clearRect(0, 0, pJS.canvas.w, pJS.canvas.h);

            pJS.fn.particlesUpdate();

            for(var i = 0; i < pJS.particles.array.length; i++){
                var p = pJS.particles.array[i];
                p.draw();
            }


            // add mask
            pJS.canvas.ctx.globalCompositeOperation = 'destination-out';
            drawImageProp(pJS.canvas.ctx, mask, 0, maskY, pJS.canvas.w, pJS.canvas.h, .5, 1);

        }
    }
};



var setHeightPage = {
    init: function () {
        var pageElements = [
            '.planet-back',
            '#particles-js',
            '.page',
            '.year'
        ];


        window.addEventListener('orientationchange', changePageHeigth);
        changePageHeigth();


        function changePageHeigth() {
            var winHeight = $(window).height();

            $.each(pageElements, function (i, page) {
                $.each($(page), function (i, el) {
                    $(el).css({
                        minHeight: winHeight + 70
                    });
                })
            });
        }
    }
};




var nextPage = {
    init: function () {
        var nextPageArrow = $('.page-scroll_point');

        var pfx = ["webkit", "moz", "MS", "o", ""];
        function PrefixedEvent(element, type, callback) {
            for (var p = 0; p < pfx.length; p++) {
                if (!pfx[p]) type = type.toLowerCase();
                element.addEventListener(pfx[p]+type, callback, false);
            }
        }
        PrefixedEvent(nextPageArrow[0], "AnimationEnd", function() {
            nextPageArrow.removeClass('animBounce');
            setTimeout(function() {
                nextPageArrow.addClass('animBounce')
            }, 10);
        });
    }
};




var yearNav = {
    init: function () {
        $('.year-nav_link').on('click', function (e) {
            e.preventDefault();
            var page = $($(this).attr('href'));
            var offset = page.offset().top;
            $('body, html').animate({scrollTop: offset}, 300);
        });
    }
};




var maskY = 0;

$(function () {

    function ieDetect(){
        var ua = window.navigator.userAgent;
        return ( ua.indexOf('MSIE') != -1 || ua.indexOf('Trident') != -1 || ua.indexOf('Edge') != -1 );
    }

    if ( !ieDetect() ) {
        if ( !Modernizr.touch ) {
            backPlanet.init();
            stars.init();
            nextPage.init();
            yearNav.init();
        } else {
            setHeightPage.init();
        }
    }


    var shareData = {
        counterEnabled: false,
        hidden: false,
        parseMetaTags: true,
        url: window.location.href,
        className: 'sharing-popup-social sharing-mini'
    };

    $('.share_cont').share(shareData);

});





// draw image in canvas
function drawImageProp(ctx, img, x, y, w, h, offsetX, offsetY) {

    if (arguments.length === 2) {
        x = y = 0;
        w = ctx.canvas.width;
        h = ctx.canvas.height;
    }

    // default offset is center
    offsetX = typeof offsetX === "number" ? offsetX : 0.5;
    offsetY = typeof offsetY === "number" ? offsetY : 0.5;

    // keep bounds [0.0, 1.0]
    if (offsetX < 0) offsetX = 0;
    if (offsetY < 0) offsetY = 0;
    if (offsetX > 1) offsetX = 1;
    if (offsetY > 1) offsetY = 1;

    var iw = img.width,
        ih = img.height,
        r = Math.min(w / iw, h / ih),
        nw = iw * r,   // new prop. width
        nh = ih * r,   // new prop. height
        cx, cy, cw, ch, ar = 1;

    // decide which gap to fill
    if (nw < w) ar = w / nw;
    if (Math.abs(ar - 1) < 1e-14 && nh < h) ar = h / nh;  // updated
    nw *= ar;
    nh *= ar;

    // calc source rectangle
    cw = iw / (nw / w);
    ch = ih / (nh / h);

    cx = (iw - cw) * offsetX;
    cy = (ih - ch) * offsetY;

    // make sure source rectangle is valid
    if (cx < 0) cx = 0;
    if (cy < 0) cy = 0;
    if (cw > iw) cw = iw;
    if (ch > ih) ch = ih;

    // fill image in dest. rectangle
    ctx.drawImage(img, cx, cy, cw, ch,  x, y, w, h);
}

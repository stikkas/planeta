var Technobattle = Technobattle || {};
Technobattle.Views = Technobattle.Views || {};

Technobattle.Views.VideoDialog = Modal.OverlappedView.extend({
    template: "#modal-technobattle-video-template",
    className: "modal modal-common modal-video",
    clickOff: true
});

var layoutNavIncluded = !!window.top.layoutNavIncluded;

(function () {
    if ( layoutNavIncluded ) {
        layoutNavIncluded = true;
        return;
    }
    layoutNavIncluded = true;

    function getStyle(urls){
        var url = urls.shift();
        var style = document.createElement('link');
        style.type = "text/css";
        style.rel = "stylesheet";
        style.href = url;
        var head = document.getElementsByTagName('head')[0];
        head.appendChild(style);
        if ( urls.length ) getStyle(urls);
    }

    function getScript(urls){
        var url = urls.shift();
        var script = document.createElement('script');
        script.src = url;
        var head = document.getElementsByTagName('head')[0], done=false;
        script.onload = script.onreadystatechange = function(){
            if (!done && (!this.readyState || this.readyState == 'loaded' || this.readyState == 'complete')) {
                done=true;
                if ( urls.length ) getScript(urls);
                script.onload = script.onreadystatechange = null;
            }
        };
        head.appendChild(script);
    }

    var styles = [
        '/css/jstree/proton/style.min.css',
        '/css/navigation.css'
    ];
    getStyle(styles);

    var scripts = [
        '/js/jquery-1.8.3.min.js',
        '/js/jstree.min.js',
        '/js/mobile-detect.min.js',
        '/js/impulse.min.js',
        '/js/jquery.cookies.2.2.0.min.js',
        '/js/navigation-links.js',
        '/js/navigation.js'
    ];

    if (!!window.jQuery) scripts.shift();

    getScript(scripts);
})();

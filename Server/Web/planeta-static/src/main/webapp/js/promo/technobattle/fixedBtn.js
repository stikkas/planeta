var fixedBtn = {
    init: function(selector, options) {
        var blockWrap = $(selector);
        if (!blockWrap.length) return;
        var blockOuterWidth = blockWrap.outerWidth();
        var blockOuterHeight = blockWrap.outerHeight();

        if ( !!options && !!options.inner ) {
            $(options.inner).width(blockOuterWidth);
        }


        var lastDocHeight = 0;
        $(window).bind('resize.fixedBtn', function () {
            var docHeight = $(document).height();
            if ( lastDocHeight != docHeight ) {
                lastDocHeight = docHeight;
                changeHeight();
            }
        });
        changeHeight();

        function changeHeight() {
            setTimeout(function () {
                blockWrap.removeClass('fixed');
                blockWrap.addClass('unfixed');
                blockWrap.css('height', '');
                setTimeout(function () {
                    blockOuterHeight = blockWrap.outerHeight();
                    blockWrap.height(blockOuterHeight);
                    fixedScroll();
                });
            }, 500);
        }


        $(window).bind('scroll.fixedBtn resize.fixedBtn', function () {
            fixedScroll();
        });
        fixedScroll();

        function fixedScroll() {
            var screenHeight = $(window).height();
            var winTop = $(window).scrollTop();
            if ( options.type == 'bottom' ) {
                winTop = (winTop === 0) ? 1 : winTop;
            }
            var winBottom = winTop + screenHeight;
            var positionTop = blockWrap.offset().top;
            var position = positionTop + blockOuterHeight;
            var offset = options.offset || 0;

            if ( !!options && !!options.type && options.type == 'top' ) {
                if (winTop > positionTop ) {
                    blockWrap.addClass('fixed');
                    blockWrap.removeClass('unfixed');
                } else {
                    blockWrap.removeClass('fixed');
                    blockWrap.addClass('unfixed');
                }
            } else {
                if ( winTop > offset ) {
                    if (winBottom > position) {
                        blockWrap.removeClass('fixed');
                        blockWrap.addClass('unfixed');
                    } else {
                        blockWrap.addClass('fixed');
                        blockWrap.removeClass('unfixed');
                    }
                } else {
                    blockWrap.removeClass('fixed');
                    blockWrap.addClass('unfixed');
                }
            }
        }
    }
};

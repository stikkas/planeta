var isScrollAnimate = true;
var highVideo = false;


var ctrlDown = false,
    ctrlKey = 17,
    cmdKey = 91,
    downKey = 40,
    upKey = 38,
    homeKey = 36,
    endKey = 35;

$(document).keydown(function(e) {
    if (e.keyCode == ctrlKey || e.keyCode == cmdKey || e.key == 'Meta') ctrlDown = true;
}).keyup(function(e) {
    if (e.keyCode == ctrlKey || e.keyCode == cmdKey || e.key == 'Meta') ctrlDown = false;
});

$(document).keydown(function(e) {
    if ( (ctrlDown && ( e.keyCode == downKey || e.keyCode == upKey ))
            || e.keyCode == homeKey || e.keyCode == endKey ) {
        isScrollAnimate = true;
        setTimeout(function () {
            isScrollAnimate = false;
        }, 500);
    }
});



var backPlanet = {
    controller: new ScrollMagic.Controller(),
    planetTranslate: {},
    currentStep: 0,
    lastStep: 0,
    init: function () {
        var self = this;

        function planetTranslateProgress(e) {
            if (e.progress > .95) {
                $('.page-scroll').addClass('hide');
            } else {
                $('.page-scroll').removeClass('hide');
            }
        }


        self.planetTranslate = new ScrollMagic.Scene({
            duration: 0,
            triggerHook: 0,
            triggerElement: "body"
        })
            .on("progress", _.throttle(planetTranslateProgress, 800))
            .addTo(self.controller);


        $(window).on("resize.planetTranslate", _.debounce(updatePlanetTranslateDuration, 1000));
        function updatePlanetTranslateDuration() {
            var duration = $('body').height() - $(window).height();
            self.planetTranslate.duration(duration);
        }


        updatePlanetTranslateDuration();

        setTimeout(function () {
            isScrollAnimate = false;
        }, 500);

    },

    video: function () {
        var self = this;
        var videos = $('.video-back_video');
        var videosLen = videos.length;
        var videoStep = 0;
        var timeStep = 0;
        var videoDuration = 0;
        var currentTime = {time: 0};

        var progressBlock = $('#load-progress');
        var isProgress = !!progressBlock.length;

        videos.each(function (i) {
            var div = $('<div/>');
            isProgress && progressBlock.append(div);
            loadVideo(i + 1);
        });

        function loadVideo(i) {
            if (isProgress) var progress = progressBlock.find('div').eq(i-1);
            var vsrc = videoSrc;
            if ( !highVideo ) {
                vsrc = vsrc.replace('.mp4', '-low.mp4')
            }
            vsrc = videosLen > 1 ? vsrc.replace('.mp4', '_' + i + '.mp4') : vsrc;

            isProgress && progress.show();
            var req = new XMLHttpRequest();
            req.open('GET', vsrc, true);
            req.responseType = 'blob';
            req.onload = function() {
                if (this.status === 200) {
                    var videoBlob = this.response;
                    videos.eq(i - 1)[0].src = URL.createObjectURL(videoBlob);
                }
                isProgress && progress.hide();

                $('body').addClass('video-init');
            };
            req.onprogress = function(e) {
                var total = (e.total / 1000 / 1000).toFixed(2).toString().replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ');
                var percent = (e.loaded * 100 / e.total).toFixed(1);
                isProgress && progress.html(i + ' видео из ' + videosLen + ' – ' + percent + '% из ' + total + ' Мб');
            };
            req.send();

            videos.eq(i - 1).on('loadedmetadata', function () {
                videoDuration = videos.eq(i - 1)[0].duration * videosLen;
                timeStep = videoDuration / videosLen;
                planetTranslateVideo({
                    progress: self.planetTranslate.progress()
                });
            });
        }



        function planetTranslateVideo(e) {
            if ( videoDuration == 0 ) return;

            if ( isBreak ) {
                if ( self.currentStep == self.lastStep ) return;
                self.lastStep = self.currentStep;
            }

            var targetTime = e.progress * videoDuration;
            if ( Math.abs(currentTime.time - targetTime) < .08 ) return;

            var duration = Math.abs(currentTime.time - targetTime);
            if ( duration - 2 < .799 ) {
                duration = .799;
            }
            if ( duration > 2 ) {
                duration = 2;
            }

            TweenMax.killTweensOf(currentTime);
            TweenMax.fromTo(currentTime, duration, {
                time: currentTime.time
            }, {
                time: targetTime,
                ease: Power0.easeNone,
                onUpdate: function () {
                    videoStep = Math.ceil(currentTime.time / timeStep);
                    videoStep = currentTime.time == 0 ? 1 : videoStep;

                    var lowProgressVal = (videoStep - 1) * timeStep;

                    var video = videos.eq(videoStep - 1);
                    if ( !video.hasClass('active') ) {
                        $('.video-back_video.active').removeClass('active');

                        var prevVideo = videos.eq(videoStep - 2);
                        if ( !!prevVideo.length && !isNaN(prevVideo.duration) ) {
                            prevVideo[0].currentTime = prevVideo[0].duration;
                        }

                        var nextVideo = videos.eq(videoStep + 1);
                        if ( !!nextVideo.length && !isNaN(nextVideo.duration) ) {
                            nextVideo[0].currentTime = 0;
                        }
                    }
                    video.addClass('active');

                    var currentProggress = (currentTime.time - lowProgressVal);
                    currentProggress = currentTime.time == 0 ? 0 : currentProggress;


                    video[0].currentTime = currentProggress;
                }
            });
        }


        self.planetTranslate.on("progress", _.throttle(planetTranslateVideo, 800));

    },

    page: function () {
        var self = this;

        $('body').addClass('animation-init');


        var pages = $('.page');
        $.each( pages, function (i, el) {
            (function (i, el) {
                var lastHit = 'start';

                var cont = $('.page-cont', el);
                var id = $(el).attr('id').replace('page-', '');
                var nextId = '#year-' + (parseInt(id) + 1);
                var nextPage = $(nextId);

                if ( i > 0 && i != pages.length - 1 ) {
                    var scene = new ScrollMagic.Scene({
                        duration: "2000",
                        triggerHook: 0,
                        offset: 0,
                        triggerElement: el
                    })
                        .setPin(el)
                        .on("start end", function (e) {
                            lastHit = e.type;
                        })
                        .on("enter leave", function (e) {
                            if ( lastHit == "start" && e.type == "enter" ) {
                                $(el).addClass('active');
                            } else if ( lastHit == "start" && e.type == "leave" ) {
                                $(el).removeClass('active');
                            } else if ( lastHit == "end" && e.type == "leave" ) {

                                if ( !isScrollAnimate && !!nextPage.length ) {
                                    var offset = nextPage.data('scene') ? nextPage.data('scene').scrollOffset() : nextPage.offset().top;
                                    isScrollAnimate = true;
                                    $('html, body').animate({scrollTop: offset}, 200, function () {
                                        isScrollAnimate = false;
                                    });
                                }

                                // $(el).addClass('remove');
                            } else if ( lastHit == "end" && e.type == "enter" ) {
                                // $(el).removeClass('remove');
                            }
                        })
                        .on("progress", function (e) {
                            TweenMax.to(cont, .2, { css:{
                                y: $(window).height() / 4 * -e.progress
                            } });
                        })
                        .addTo(self.controller);

                    $(el).data('scene', scene);

                }

            })(i, el);
        });



        // year pinned
        var years = $('.year');
        $.each( years, function (i, el) {
            (function (i, el) {
                var lastHit = 'start';

                var cont = $('.year-cont', el);
                var id = $(el).attr('id').replace('year-', '');
                var nextId = '#page-' + id;
                var nextPage = $(nextId);

                var scene = new ScrollMagic.Scene({
                    duration: "1500",
                    triggerHook: 0,
                    triggerElement: el
                })
                    .setPin(el)
                    .on("start end", function (e) {
                        lastHit = e.type;
                    })
                    .on("enter leave", function (e) {
                        if ( lastHit == "start" && e.type == "enter" ) {
                            $(el).addClass('active');
                        } else if ( lastHit == "start" && e.type == "leave" ) {
                            $(el).removeClass('active');
                        } else if ( lastHit == "end" && e.type == "leave" ) {

                            if ( !isScrollAnimate && !!nextPage.length ) {
                                var offset = nextPage.data('scene') ? nextPage.data('scene').scrollOffset() : nextPage.offset().top;
                                isScrollAnimate = true;
                                $('html, body').animate({scrollTop: offset}, 200, function () {
                                    isScrollAnimate = false;
                                });
                            }

                            // $(el).addClass('remove');
                        } else if ( lastHit == "end" && e.type == "enter" ) {
                            // $(el).removeClass('remove');
                        }
                    })
                    .on("progress", function (e) {
                        TweenMax.to(cont, .2, { css:{
                            y: $(window).height() / 4 * -e.progress
                        } });
                    })
                    .addTo(self.controller);


                $(el).data('scene', scene);

            })(i, el);
        });
    },
    yearNav: function () {
        var self = this;
        // year nav
        $.each( $('.year-wrap'), function (i, el) {
            var yearNav = new ScrollMagic.Scene({
                triggerElement: el
            })
                .setClassToggle("#year-nav-" + (i), "active")
                .on("enter", function (e) {
                    self.currentStep = $(el).index();
                })
                .addTo(self.controller);

            $(window).on("resize.yearNav", _.debounce(updateYearNavDuration, 1000));
            updateYearNavDuration();

            function updateYearNavDuration() {
                var duration = $(el).height();
                yearNav.duration(duration);
            }
        });
    }
};




var nextPage = {
    init: function () {
        var nextPageArrow = $('.page-scroll_point');

        var pfx = ["webkit", "moz", "MS", "o", ""];
        function PrefixedEvent(element, type, callback) {
            for (var p = 0; p < pfx.length; p++) {
                if (!pfx[p]) type = type.toLowerCase();
                element.addEventListener(pfx[p]+type, callback, false);
            }
        }
        PrefixedEvent(nextPageArrow[0], "AnimationEnd", function() {
            nextPageArrow.removeClass('animBounce');
            setTimeout(function() {
                nextPageArrow.addClass('animBounce')
            }, 10);
        });
    }
};




var yearNav = {
    init: function () {
        $('.year-nav_link').on('click', function (e) {
            e.preventDefault();
            var page = $($(this).attr('href'));
            var offset = page.data('scene') ? page.data('scene').scrollOffset() : page.offset().top;
            isScrollAnimate = true;
            var derectionOffset = $(window).scrollTop() > offset ? 100 : -100;
            var body = $('body, html');
            body.scrollTop(offset + derectionOffset);
            body.animate({scrollTop: offset}, 300, function () {
                isScrollAnimate = false;
            });
        });
    }
};




$(function () {
    var disableVideo = false;
    var disablePage = true;


    if (
        !!bowser.mobile ||
        !!bowser.tablet ||
        (
            !bowser.chrome &&
            !bowser.opera &&
            !bowser.safari &&
            !bowser.firefox &&
            !bowser.msedge
        )
    ) {
        disableVideo = true;
    }

    if ( (!!bowser.mac || (!!bowser.windows && parseInt(bowser.osversion) >= 10)) && !bowser.firefox ) {
        highVideo = true;
    }

    if (
        !!bowser.mobile ||
        !!bowser.tablet ||
        (
            !bowser.webkit &&
            !bowser.blink &&
            !bowser.gecko &&
            !bowser.msedge
        )
    ) {
        disablePage = true;
    }

    if ( !!bowser.mobile || !!bowser.tablet ) {
        $('html').addClass('mobile');
    }


    if ( !disablePage ) backPlanet.page();
    backPlanet.init();
    if ( !disableVideo ) backPlanet.video();
    backPlanet.yearNav();
    nextPage.init();
    yearNav.init();




    $('.header-btn').on('click', function () {
        var page = $('#year-1');
        var offset = page.data('scene') ? page.data('scene').scrollOffset() : page.offset().top;
        var body = $('body, html');
        body.animate({scrollTop: offset}, 300, function () {
            isScrollAnimate = false;
        });
    });
});





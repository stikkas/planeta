$(document).ready(function () {
    $(function () {
        $('.nav-toggle').on('click', function () {
            $('html').toggleClass('nav-open');
        });
    });

    // sharing
    var sharesRedraw = function () {
        $('.js-share-container').empty();
        $('.js-share-container').share({
            className: 'sharing-popup-social donate-sharing sharing-mini',
            counterEnabled: false,
            hidden: false
        });
    };
    $(window).bind('resize', sharesRedraw);
    sharesRedraw();


    // send email
    $('.js-send-letter').click(function (e) {
        e.preventDefault();
        $('.js-send-letter').prop('disabled', true);

        $.post("/techbattle/subscribe-email.json", {
            email: $(e.currentTarget).parent().find('#letterEmail').val()
        }).done(function (response) {
            if (response.success) {
                $('.js-send-letter').text('Отправлено');
                $('#formEmail').removeClass('error');
                $('#formEmail').addClass('success');
            } else {
                $('.js-send-letter').prop('disabled', false);
                $('#formEmail').removeClass('success');
                $('#formEmail').addClass('error');
            }
        });
    });

    // date counter
    var now = new Date();
    var endOfVote =  new Date(2017, 2, 1);
    var difference = Math.ceil((endOfVote.getTime() - now.getTime())/(1000*60*60*24));
    if (difference < 0) {
        difference = 0;
    }
    var days = StringUtils.declOfNumWithNum(difference, ['день', 'дня', 'дней']);
    $('.js-end-of-vote').text(days);

    // vote button click
    $('.js-vote-button').click(function(e) {
        e.preventDefault();
        $('#modal-vote').removeClass('hide');
    });
});

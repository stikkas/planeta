/*global ProfileUtils, Modal, BaseModel */
var Tech = {Views:  {}, Models : {}};

//------------- about ------------

Tech.Models.ProjectDescription = BaseModel.extend({
    projectId: 0,
    autoFetch: 'projectId',
    parse: BaseModel.prototype.parseActionStatus,
    url: "/api/profile/tech-project.json"
});

Tech.Views.ProjectDescription = BaseView.extend({
    template: "#tech-project-description-template"
});

//------------- news ------------

Tech.Views.ModalPost = News.Views.Modal.Post.extend({
    afterRender: function () {
        Modal.OverlappedView.prototype.afterRender.apply(this, arguments);
        Tech.Views.Post.prototype.addSharing.apply(this, arguments);
    }
});

Tech.Views.Post = News.Views.Post.extend({
    beforeRender: function () {
        BaseRichView.prototype.beforeRender.apply(this, arguments);
        var postUrl = window.location.href;
        if (this.model.get('campaignId')) {
            postUrl = ProfileUtils.getAbsoluteCampaignLink(this.model.get('webCampaignAlias')) + '/updates!post' + this.model.get('postId');
        } else {
            postUrl = window.location.href + '/post/' + this.model.get('objectId');
        }

        this.model.set({
            postUrl: postUrl
        }, {silent: true});
    },

    onPostClick: function (e) {
        if (e) {
            e.preventDefault();
        }
        Tech.Views.Posts.prototype.showPostModal.call(this, this.model.get('objectId'));
    },

    addSharing: function () {
        if (!this.sharingData) {
            var post = this.model;
            var data = {
                title: post.get('postName'),
                counterEnabled: false,
                hidden: false,
                className: 'donate-sharing'
            };

            data.url = window.location.href;
            // get description
            var description = BlogUtils.trimContentHtml(post.get('headingText'));
            if (description) {
                description = StringUtils.cutString(description, BlogUtils.maxContentLength);
            } else {
                description = data.title;
            }
            data.description = description;
            // get image
            var photo = this.$el.find('[data-role="photo"]');
            data.image = photo.length ? photo.data('json').image : post.get('authorImageUrl');

            this.sharingData = data;
        }

        this.$('.feed_sharing').share(this.sharingData);
    }
});

Tech.Views.Posts = News.Views.Posts.extend({
    itemViewType: Tech.Views.Post,
    construct: function () {
    },

    showPostModal: function (postId) {
        var model = this.model;
        var post = new News.Models.SinglePost({
            postId: postId,
            type: 'POST',
            authorProfileId: model.get('authorProfileId'),
            campaignId: model.get('campaignId'),
            profileId: model.get('profileId')
        }, {
            collection: model.collection
        });

        Modal.showDialogByViewClass(Tech.Views.ModalPost, post);
    }
});

Tech.Models.TechProjectTabs = BaseModel.extend({
    defaults: {
        commentsCount: 0,
        newsCount: 0,
        onlyMyNews: true,
        tabVisited: {}
    },
    autoFetch: 'profileId',
    parse: BaseModel.prototype.parseActionStatus,
    url: "/api/profile/tech-stats.json"
});

Tech.Views.TechProjectTabs = BaseView.extend({
    template: "#tech-projects-tab-view-template",
    construct: function () {
    },

    onTechTabClicked: function (e) {
        e.preventDefault();
        $('.js-project-tab-content').hide();

        var $el = $(e.currentTarget);
        var tab = $el.data('tab');
        this.model.get('tabVisited')[tab] = true;
        this.trigger('change');
        this.model.set('tab', tab);

        $('.js-' + tab).show();

        if (tab === "about") {
            if (!this.descriptionModel) {
                this.initDescription();
            }
        }

        if (tab === "news") {
            if (!this.postCollection) {
                this.initNews();
            }
        }

        if (tab === "comments") {
            if (!this.comments) {
                this.initComments();
            }
        }

        var urlTab = "/" + tab;
        if (tab === "about") {
            urlTab = "";
        }
        workspace.changeUrl('/techbattle/project/' + this.model.get('projectId') + urlTab);
    },

    initDescription: function () {
        this.descriptionModel = new Tech.Models.ProjectDescription({
            profileId: this.model.get('profileId'),
            projectId: this.model.get('projectId')
        });
        this.descriptionView = new Tech.Views.ProjectDescription({
            el: ".js-description-inner",
            model: this.descriptionModel
        });
    },

    initNews: function () {
        this.postCollection = new News.Models.Posts([], {
            data: {
                profileId: this.model.get('profileId'),
                campaignId: 0,
                onlyMyNews: true
            },
            limit: 10
        });
        this.postCollection.load();
        this.newsView = new Tech.Views.Posts({
            el: ".js-news-inner",
            collection: this.postCollection,
            model: this.model
        });
    },

    initComments: function () {
        var self = this;
        this.model.loadDfd.done(function () {
            self._initComments();
        });
    },

    _initComments: function () {
        this.comments = new Comments.Models.Comments({
            profileId: this.model.get('profileId'),
            objectId: this.model.get('profileId'),
            objectOwnerProfileId: this.model.get('profileId'),
            objectType: 'PROFILE',
            showCounters: false
        }, {
            commentableObject: this.model
        });
        this.commentsView = new Comments.Views.ModalComments({
            el: ".js-comments-inner",
            model: this.comments
        });
        this.commentsView.render();
    }
});
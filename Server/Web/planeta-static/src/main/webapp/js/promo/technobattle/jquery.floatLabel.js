(function( $ ){

    var defaults = {

    };

    var methods = {
        init: function( options ) {
            options = $.extend(true, {}, defaults, options);

            return this.each(function() {
                var data = $(this).data('floatLabel');
                if (data) return this;

                $(this).data('floatLabel', true);
                methods.init_el($(this), options);
            });
        },
        init_el: function($elem, options) {
            var $input = $('input, textarea', $elem);

            checkVal();

            $input
                .on("click keydown keyup change blur", checkVal)
                .on("focus", function () {
                    $elem.addClass('js-float-label__focus');
                })
                .on("blur", function () {
                    $elem.removeClass("js-float-label__focus");
                });

            function checkVal() {
                var inputVal = $input.val();
                if ( !!inputVal.length ) {
                    $elem.addClass("js-float-label__fill");
                } else if (0 === inputVal.length) {
                    $elem.removeClass("js-float-label__fill");
                }
            }

        }
    };


    $.fn.floatLabel = function( method ) {

        if ( methods[method] ) {
            return methods[ method ].apply( this, Array.prototype.slice.call( arguments, 1 ));
        } else if ( typeof method === 'object' || ! method ) {
            return methods.init.apply( this, arguments );
        } else {
            $.error( 'Method ' +  method + ' does not exist on jQuery.floatLabel' );
        }

    };

    $(function () {
        $('.js-float-label').floatLabel();
    });

})( jQuery );

(function () {
    var Gtm = function () {
    };

    function loadGtmCampaignInfo(campaignIds, startIndex) {
        if (!_.isArray(campaignIds)) {
            campaignIds = [campaignIds];
        }
        var dfd = $.Deferred();
        $.get("/api/public/gtm/get-campaign-list-info.json", {ids: campaignIds}).done(function (response) {
                if (response && response.success && response.result) {
                    _.each(response.result, function (item, idx) {
                        item.pos = (startIndex || 0) + idx;
                    });
                    dfd.resolveWith(null, [response.result]);
                }
            }
        );
        return dfd.promise();
    }

    function loadGtmCampaignInfoAndRunMethod(ctx, methodName, campaignId) {
        var campaignInfo = getCampaignInfoFromLocalStorage();

        var args = Array.prototype.slice.call(arguments, 3);
        if (!campaignInfo || campaignInfo.campaignId != campaignId) {
            loadGtmCampaignInfo(campaignId).done(function (result) {
                if (result && result[0]) {
                    args.unshift(result[0]);
                    ctx[methodName].apply(ctx, args);
                }
            });
        } else {
            args.unshift(campaignInfo);
            ctx[methodName].apply(ctx, args);
        }
    }

    function _loadCampaignInfo(ctx, methodName, campaign) {
        var gtmCampaignInfo = getCampaignInfoFromLocalStorage();

        if(!gtmCampaignInfo || gtmCampaignInfo.campaignId != campaign.get('campaignId')) {
            ctx[methodName].call(ctx, campaign);
        } else {
            ctx[methodName].call(ctx, gtmCampaignInfo);
        }
    }

    function getCampaignInfoFromLocalStorage() {
        var campaignInfo;

        if(window.localStorage) {
            campaignInfo = window.localStorage.getItem('gtmCampaignInfo');
            if(campaignInfo && campaignInfo !== 'undefined') {
                campaignInfo = JSON.parse(campaignInfo);
            }
        }

        return campaignInfo;
    }

    Gtm.prototype = {

        /*Planeta tracks*/
        trackViewCampaignList: function (campaigns, listName, startIndex) {
            try {
                var self = this;
                var campaignIds = _.pluck(_.pluck(campaigns, 'attributes'), 'campaignId');
                loadGtmCampaignInfo(campaignIds, startIndex).done(function (result) {
                    _.each(campaigns, function (campaign) {
                        _.each(result, function (c) {
                            c.listName = listName;
                            if (c.campaignId === campaign.get('campaignId')) {
                                campaign.set('gtmInfo', c, {silent: true});
                            }
                        });
                    });
                    self._trackViewCampaignList(result, listName);
                });
            } catch(e) {
                console.log(e);
            }
        },

        _trackViewCampaignList: function (campaigns, listName) {
            var idx = 0, params;

            params = {
                event: 'impressionCard',
                ecommerce: {
                    impressions: []
                }
            };

            campaigns.forEach(function(campaign) {
                params.ecommerce.impressions[idx++] = {
                    name: campaign.campaignName,
                    id: campaign.campaignAlias ? campaign.campaignAlias : campaign.campaignId,
                    brand: campaign.creatorName,
                    category: campaign.firstTagName,
                    position: campaign.pos + 1 || 1,
                    list: listName
                };
            });

            dataLayer.push(params);
        },

        trackClickCampaignCard: function (campaign) {
            var campaignInfo = campaign.get('gtmInfo');
            if (!campaignInfo) {
                return;
            }
            if (window.localStorage) {
                window.localStorage.setItem('gtmCampaignInfo', JSON.stringify(campaignInfo));
            }

            var params = _.extend({
                event: 'universalCustomTrigger',
                ecommerce: {
                    click: {
                        actionField: {
                            list: campaignInfo.listName
                        },

                        products: [{
                            name: campaignInfo.campaignName,
                            id: campaignInfo.campaignAlias ? campaignInfo.campaignAlias : campaignInfo.campaignId,
                            brand: campaignInfo.creatorName,
                            category: campaignInfo.firstTagName,
                            position: campaignInfo.pos + 1 || 1
                        }]
                    }
                }
            });
            dataLayer.push(params);
        },

        trackViewCampaignPage: function (campaign) {
            _loadCampaignInfo(this, "_trackViewCampaignPage", campaign);
        },

        _trackViewCampaignPage: function (info) {
            var listName;
            info = _.clone(info.attributes || info);

            if(info.listName) {
                listName = info.listName;
            } else {
                listName = "Страница проекта";
                this._setGtmInfo(info, listName);
            }

            var params = _.extend({
                event: 'impressionCard',
                ecommerce: {
                    detail: {
                        actionField: {
                            list: listName
                        },

                        products: [{
                            name: info.campaignName ? info.campaignName : info.name ,
                            id: info.campaignAlias ? info.campaignAlias : info.campaignId,
                            brand: info.creatorName ? info.creatorName : info.creatorProfile.get('displayName'),
                            category: info.firstTagName ? info.firstTagName : info.mainTag.name
                        }]
                    }
                }
            });
            dataLayer.push(params);
        },

        _setGtmInfo: function(campaign, listName) {
            var campaignId = campaign.campaignId;

            loadGtmCampaignInfo(campaignId, 0).done(function (result) {
                result.forEach(function (resultCampaign) {
                    resultCampaign.listName = listName;
                    if (resultCampaign.campaignId === campaignId) {
                        if (window.localStorage) {
                            window.localStorage.setItem('gtmCampaignInfo', JSON.stringify(resultCampaign));
                        }
                    }
                });
            });
        },

        trackSelectShare: function (share) {
            loadGtmCampaignInfoAndRunMethod(this, "_trackSelectShare", share.get('campaignId'), share);
        },

        _trackSelectShare: function (campaignInfo, share) {
            var quantity = share.get('quantity') || 1,
                price;

            /*if you come to the payment page by an external link or information about share was deleted from sessionStorage*/
            if(SessionStorageProvider.get('sharePurchase_' + share.get('shareId'))) {
                price = SessionStorageProvider.get('sharePurchase_' + share.get('shareId')).donateAmount / quantity;
            } else {
                price = shareInfo
                        .price;
            }

            var params = _.extend({
                event: 'universalCustomTrigger',
                ecommerce: {
                    currencyCode: 'RUB',
                    add: {
                        actionField: {
                            list: campaignInfo.listName
                        },
                        products: [{
                            name: campaignInfo.campaignName,
                            id: campaignInfo.campaignId,
                            price: price,
                            brand: campaignInfo.creatorName,
                            category: campaignInfo.firstTagName,
                            quantity: quantity
                        }]
                    }
                }
            });
            dataLayer.push(params);
        },

        trackChoicePaymentProvider: function (share, paymentMethod) {
            loadGtmCampaignInfoAndRunMethod(this, "_trackChoicePaymentProvider", share.get('campaignId'), share, paymentMethod);
        },

        _trackChoicePaymentProvider: function (campaignInfo, share, paymentMethod) {
            var quantity = share.get('quantity') || 1,
                price;

            /*if you come to the payment page by an external link or information about share was deleted from sessionStorage*/
            if(SessionStorageProvider.get('sharePurchase_' + share.get('shareId'))) {
                price = SessionStorageProvider.get('sharePurchase_' + share.get('shareId')).donateAmount / quantity;
            } else {
                price = shareInfo.price;
            }

            var params = _.extend({
                event: 'universalCustomTrigger',
                ecommerce: {
                    checkout: {
                        actionField: {
                            step: 1,
                            option: paymentMethod,
                            list: campaignInfo.listName
                        },

                        products: [{
                            name: campaignInfo.campaignName,
                            id: campaignInfo.campaignId,
                            price: price,
                            brand: campaignInfo.creatorName,
                            category: campaignInfo.firstTagName,
                            quantity: quantity
                        }]
                    }
                }
            });
            dataLayer.push(params);
        },

        trackPurchaseShares: function (transactionInfo, campaignInfo, shareInfo) {
            this._trackPurchaseShares( campaignInfo, transactionInfo, shareInfo);
            // loadGtmCampaignInfoAndRunMethod(this, "_trackPurchaseShares", campaignInfo.campaignId, transactionInfo, shareInfo);
        },

        _trackPurchaseShares: function (campaignInfo, transactionInfo, shareInfo) {
            try {
                var quantity = shareInfo.quantity || 1;
                var price = transactionInfo.amount / quantity;
                var params = _.extend({
                    event: 'universalCustomTrigger',
                    ecommerce: {
                        purchase: {
                            actionField: {
                                id: transactionInfo.transactionId,
                                affiliation: 'Planeta.ru',
                                revenue: transactionInfo.amount,
                                list: campaignInfo.listName
                            },
                            products: [{
                                name: campaignInfo.campaignName,
                                id: campaignInfo.campaignId,
                                price: price,
                                brand: campaignInfo.creatorName,
                                category: campaignInfo.firstTagName,
                                quantity: quantity
                            }]
                        }
                    }
                });
                dataLayer.push(params);
            } catch (ex) {
                console.error('Planeta exception: ' + ex);
            }
        },


        trackViewRewardsList: function (rewards, listName) {
            var params = {
                event: 'impressionCard',
                ecommerce: {
                    impressions: []
                }
            };

            if (rewards && rewards.length > 0) {
                rewards.forEach(function(reward, index) {
                    params.ecommerce.impressions.push({
                        name: reward.name,
                        id: reward.shareId,
                        brand: reward.campaignName,
                        category: reward.campaignTags[0].name,
                        position: index + 1,
                        list: listName
                    });
                });
            }

            dataLayer.push(params);
        },

        trackClickRewardCard: function (reward, page) {
            var listName = '';
            if (page === 'search') {
                listName = 'Страница поиска вознаграждений';
            } else if (page === 'welcome') {
                listName = 'Карусель вознаграждений на главной';
            }

            var params = {
                event: 'universalCustomTrigger',
                ecommerce: {
                    click: {
                        actionField: {
                            list: listName
                        },

                        products: [{
                            name: reward.name,
                            id: reward.shareId,
                            brand: reward.campaignName,
                            category: reward.campaignTags[0].name,
                            position: 0 //TODO: придумать что-то
                        }]
                    }
                }
            };

            dataLayer.push(params);
        },

        /*END: Planeta tracks*/

        trackPurchaseBalance: function (transactionInfo) {
            this._trackPurchaseBalance(transactionInfo);
        },

        _trackPurchaseBalance: function (transactionInfo) {
            try {
                var params = _.extend({
                    event: 'universalCustomTrigger',
                    ecommerce: {
                        purchase: {
                            actionField: {
                                id: transactionInfo.transactionId,
                                affiliation: 'Balance.planeta.ru',
                                revenue: transactionInfo.amount
                            }
                        }
                    }
                });
                dataLayer.push(params);
            } catch (ex) {
                console.error('Planeta exception: ' + ex);
            }
        },



        /*Planeta-Shop tracks*/
        trackViewProductList: function (products, listName, startIndex) {
            _.each(products, function (product, idx) {
                product.set('gtmInfo', {
                    authorProfileId: product.get('authorProfileId'),
                    productId: product.get('productId'),
                    parentProductId: product.get('parentProductId'),
                    productName: product.get('name'),
                    category: product.get('mainTag').value,
                    listName: listName,
                    pos: startIndex + idx + 1
                }, {silent: true});
            });
            this._trackViewProductList(products, listName, startIndex);
        },

        _trackViewProductList: function (products, listName, startIndex) {
            var idx = 0;

            var params = {
                event: 'impressionCard',
                ecommerce: {
                    impressions: []
                }
            };

            products.forEach(function(product) {
                params.ecommerce.impressions[idx++] = {
                    name: product.get('name'),
                    id: product.get('productId'),
                    category: product.get('mainTag').value,
                    position: idx + startIndex,
                    list: listName
                };
            });

            dataLayer.push(params);
        },

        trackClickProductCard: function (product) {
            var productInfo = product.get('gtmInfo');

            if (productInfo) {
                if (window.localStorage) {
                    window.localStorage.setItem('gtmCartItemInfo_' + productInfo.productId, JSON.stringify(productInfo));
                }
            } else {
                productInfo = JSON.parse(window.localStorage.getItem('gtmCartItemInfo_' + product.get('productId')));
            }

            var params = _.extend({
                event: 'universalCustomTrigger',
                ecommerce: {
                    click: {
                        actionField: {
                            list: productInfo.listName
                        },

                        products: [{
                            name: productInfo.productName,
                            id: productInfo.parentProductId || productInfo.productId,
                            brand: productInfo.authorProfileId,
                            category: productInfo.category,
                            position: productInfo.pos || 1
                        }]
                    }
                }
            });
            dataLayer.push(params);
        },

        extendProduct: function (product) {
            product = _.clone(product.attributes || product);
            var gtmInfo,
                productId = product.parentObjectId > 0 ? product.parentObjectId : (product.productId || product.objectId);
            if (window.localStorage) {
                gtmInfo = window.localStorage.getItem('gtmCartItemInfo_' + productId);
                if (gtmInfo && gtmInfo !== 'undefined') {
                    gtmInfo = JSON.parse(gtmInfo);
                    if(gtmInfo.productId == productId) {
                        product.gtmInfo = gtmInfo;
                    } else {
                        product = this.setProductGtmInfo(product, 'Страница проекта - ' + product.mainTag.value);
                    }
                } else {
                    product = this.setProductGtmInfo(product, 'Страница проекта - ' + product.mainTag.value);
                }
            }

            return product;
        },

        trackViewProductPage: function (product) {
            product = this.extendProduct(product);
            var productInfo = product.gtmInfo;

            var params = _.extend({
                event: 'impressionCard',
                ecommerce: {
                    detail: {
                        actionField: {
                            list: productInfo.listName
                        },

                        products: [{
                            name: productInfo.productName,
                            id: productInfo.productId,
                            brand: productInfo.authorProfileId,
                            category: productInfo.category
                        }]
                    }
                }
            });
            dataLayer.push(params);
        },

        setProductGtmInfo: function(product, listName) {
            product.gtmInfo = {
                authorProfileId: product.authorProfileId,
                productId: product.productId,
                parentProductId: product.parentProductId,
                productName: product.name,
                category: product.mainTag.value,
                listName: listName
            };

            if (window.localStorage) {
                window.localStorage.setItem('gtmCartItemInfo_' + product.productId, JSON.stringify(product.gtmInfo));
            }

            return product;
        },

        trackAddProductToCart: function (cartItem) {
            cartItem = this.extendProduct(cartItem);

            var params = _.extend({
                event: 'universalCustomTrigger',
                ecommerce: {
                    currencyCode: 'RUB',
                    add: {
                        actionField: {
                            list: cartItem.gtmInfo.listName
                        },
                        products: [{
                            name: cartItem.objectName,
                            id: cartItem.parentObjectId || cartItem.objectId,
                            category: cartItem.mainTag,
                            price: cartItem.price,
                            quantity: cartItem.count || 1
                        }]
                    }
                }
            });
            dataLayer.push(params);
        },

        trackRemoveProductFromCart: function (cartItem) {
            cartItem = this.extendProduct(cartItem);

            var params = _.extend({
                event: 'universalCustomTrigger',
                ecommerce: {
                    currencyCode: 'RUB',
                    remove: {
                        actionField: {
                            list: cartItem.gtmInfo.listName
                        },
                        products: [{
                            name: cartItem.objectName,
                            id: cartItem.parentObjectId || cartItem.objectId,
                            category: cartItem.mainTag,
                            price: cartItem.price,
                            quantity: cartItem.count || 1
                        }]
                    }
                }
            });
            dataLayer.push(params);
        },

        trackViewCart: function (products) {
            var idx = 0, self = this;

            var params = _.extend({
                event: 'universalCustomTrigger',
                ecommerce: {
                    checkout: {
                        actionField: {
                            step: 1
                        },
                        products: []
                    }
                }
            });

            products.forEach(function(product) {
                product = self.extendProduct(product);
                params.ecommerce.checkout.products[idx++] = {
                    name: product.objectName,
                    id: product.parentObjectId || product.objectId,
                    price: product.price,
                    category: product.mainTag,
                    quantity: product.count || 1,
                    list: product.gtmInfo.listName
                };
            });

            dataLayer.push(params);
        },

        trackPurchaseCart: function (products) {
            var idx = 0, self = this;

            var params = _.extend({
                event: 'universalCustomTrigger',
                ecommerce: {
                    checkout: {
                        actionField: {
                            step: 2
                        },
                        products: []
                    }
                }
            });

            products.forEach(function(product) {
                product = self.extendProduct(product);
                params.ecommerce.checkout.products[idx++] = {
                    name: product.objectName,
                    id: product.parentObjectId || product.objectId,
                    price: product.price,
                    category: product.mainTag,
                    quantity: product.count || 1,
                    list: product.gtmInfo.listName
                };
            });

            dataLayer.push(params);
        },

        trackChoicePaymentProviderForCart: function (products, paymentMethod) {
            var idx = 0, self = this;

            var params = _.extend({
                event: 'universalCustomTrigger',
                ecommerce: {
                    checkout: {
                        actionField: {
                            step: 1,
                            option: paymentMethod
                        },
                        products: []
                    }
                }
            });

            products.forEach(function(product) {
                product = self.extendProduct(product);
                params.ecommerce.checkout.products[idx++] = {
                    name: product.objectName,
                    id: product.parentObjectId || product.objectId,
                    price: product.price,
                    category: product.mainTag,
                    quantity: product.count || 1,
                    list: product.gtmInfo.listName
                };
            });

            dataLayer.push(params);
        },

        trackPurchaseProducts: function (transactionInfo, products) {
            try {
                var idx = 0, self = this;

                var params = _.extend({
                    event: 'universalCustomTrigger',
                    ecommerce: {
                        purchase: {
                            actionField: {
                                id: transactionInfo.transactionId,
                                affiliation: 'Shop.planeta.ru',
                                revenue: transactionInfo.amount,
                                shipping: transactionInfo.deliveryPrice,
                                coupon: transactionInfo.promoCodeId
                            },
                            products: []
                        }
                    }
                });

                products.forEach(function (product) {
                    product = self.extendProduct(product);
                    params.ecommerce.purchase.products[idx++] = {
                        name: product.objectName,
                        id: product.parentObjectId || product.objectId,
                        price: product.price,
                        category: product.mainTag,
                        quantity: product.count || 1,
                        list: product.gtmInfo.listName
                    };
                });

                dataLayer.push(params);
            } catch (ex) {
                console.log('Shop exception: ' + ex);
            }
        },
        /*END: Planeta-Shop tracks*/


        trackCampaignPageViewEvent: function(url) {
            dataLayer.push({
                event: 'pageViewCampaign',
                ecommerce: {},
                customPagePath: url || document.location.href
            });
        },

        trackUniversalEvent: function(event, url) {
            if (url) {
                dataLayer.push({
                    event: event,
                    ecommerce: {},
                    'customPagePath': url
                });
            } else {
                dataLayer.push({
                    event: event,
                    ecommerce: {}
                });
            }
        },

        trackPurchaseBiblioBooks: function (transactionInfo, objects) {
            try {
                var params = _.extend({
                    event: 'universalCustomTrigger',
                    ecommerce: {
                        purchase: {
                            actionField: {
                                id: transactionInfo.transactionId,
                                affiliation: 'Biblio.planeta.ru',
                                revenue: transactionInfo.amount
                            },
                            products: []
                        }
                    }
                });

                objects.forEach(function(object) {
                    if(object.objectType === 'BOOK') {
                        params.ecommerce.purchase.products.push({
                            name: object.objectName,
                            id: object.objectId,
                            price: object.price,
                            brand: object.ownerName,
                            quantity: object.count
                        });
                    }
                });

                dataLayer.push(params);
            } catch (ex) {
                console.log('Planeta exception: ' + ex);
            }
        },

        getCookie: function (name) {
            var matches = document.cookie.match(new RegExp(
                "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
            ));

            return matches ? decodeURIComponent(matches[1]) : undefined;
        }
    };

    window.gtm = new Gtm();
}());
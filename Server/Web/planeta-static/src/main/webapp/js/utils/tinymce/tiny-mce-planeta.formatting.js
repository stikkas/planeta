/*global RichMediaUtils,TinyMcePlaneta,tinymce*/
//noinspection JSLint
TinyMcePlaneta.Formatting = function () {

    return {

        /**
         * Removes styles from tinymce output
         * Also note that native tinymce parser/serializer filters are version-dependent and buggy:
         * better to filter, change and check html here
         * @param {String} html - blog post html from editor
         * @return {String} - html for server save without styles
         */
        clearStyles: function (html) {
            var tmpEl = $('<div></div>');
            $(tmpEl).html(html);
//        selector : {class : style-match}
            var styleClearSelectors = {
                'li': {'mce-ie-zoom': /list-style-type:\s*none/i},
                '': {
                    'mce-text-center': /text-align:\s*center/i,
                    'mce-text-right': /text-align:\s*right/i,
                    'mce-text-left': /text-align:\s*left/i,
                    "mce-text-justify": /text-align:\s*justify/i,
                    'mce-img-left': /float:\s*left/i,
                    'mce-img-right': /float:\s*right/i,
                    'mce-img-none': /float:\s*none/i,
                    'mce-strikethrough': /text-decoration:\s*line-through/i,
                    'mce-underline': /text-decoration:\s*underline/i

                }
            };
            $.each(styleClearSelectors, function (selector, value) {
                $.each(value, function (cls, regex) {
                    // elements with defined style attributes
                    $(tmpEl).find(selector + '[style]').filter(
                    function () {
                        // only elements with matching style piece will be changed
                        return $(this).attr('style').match(regex);
                    }).addClass(cls);
                    /*.filter(
                     function () {
                     // only elements with matching style piece will be changed
                     return !$(this).attr('style').match("width|height");
                     }).removeAttr('style');*/
                });
            });
            $(tmpEl).find('[style]').removeAttr('style');
            return $(tmpEl).html();
        },

        isEmpty: function (jContainer) {
            var result = true;
            if (jContainer.text().match(/\S+/)) {
                result = false;
            }
            _.each(RichMediaUtils.TAGS, function (tagName) {
                var selector = RichMediaUtils.JQUERY_SELECTOR_PREFIX + tagName;
                if (jContainer.is(selector) || jContainer.find(selector).length) {
                    result = false;
                }
            });
            return result;
        },

        removeBrs: function (jEl) {
            jEl.find('p > br').filter(function() {
                return $(this).parent().text().match(/^\s+$/);
            }).addClass('good-brs');
            jEl.find('li > br').addClass('good-brs');
            jEl.find('br:not(.good-brs)').remove();
            jEl.find('br.good-brs').each(function () {
                $(this).removeClass('good-brs');
                if ($(this).attr('class') === '') {
                    $(this).removeAttr('class');
                }
            });
        },

        trim: function (html) {
            var $tmpEl = $(document.createElement('div'));

            /**
             * Recursively removes all 'empty' firstChild/lastChild elements
             * @param {jQuery} jContainer
             * @param selector - :first or :last, just space economy
             * @return {jQuery} - element with non-empty first/last child
             */
            function removeWhiteSpace(jContainer, selector) {
                if(TinyMcePlaneta.Formatting.isEmpty(jContainer)){
                    return jContainer;
                }
                var $current = jContainer;
                // find first empty node
                while(!TinyMcePlaneta.Formatting.isEmpty($current)) {
                    $current = $current.children(selector);
                    // exit if no beginning whitespace
                    if(!$current.length) {
                        return jContainer;
                    }
                }
                // remove first empty node
                $current.remove();
                // and try repeat
                return removeWhiteSpace(jContainer, selector);
            }

            $tmpEl.html(html);
            $tmpEl = removeWhiteSpace($tmpEl, ':first');
            $tmpEl = removeWhiteSpace($tmpEl, ':last');

            return $tmpEl.html();
        },

        /**
         * Replaces rich-media tags with image stubs
         * @param jTmpEl
         */
        parseRichMediaTags: function (jTmpEl) {
            var replacementMap = {
                'photo': TinyMcePlaneta.Photo.parseRichMedia,
                'video': TinyMcePlaneta.Video.parseRichMedia,
                'audio': TinyMcePlaneta.Audio.parseRichMedia
            };
            RichMediaUtils.replaceTagsWithHtml(jTmpEl, replacementMap);
        },
        /**
         * Replaces rich-media-stub images with real rich-media tags
         * @param jTmpEl
         */
        createRichMediaTags: function (jTmpEl) {
            var replacementMap = {
                'photo': TinyMcePlaneta.Photo.createRichMedia,
                'video': TinyMcePlaneta.Video.createRichMedia,
                'audio': TinyMcePlaneta.Audio.createRichMedia
            };
            RichMediaUtils.replaceHtmlWithTags(jTmpEl, replacementMap);
        },

        beforeSendPhotosFix: function (html) {
            var tmpEl = $('<div></div>');
            $(tmpEl).html(html);
            $(tmpEl).find("img[data-mce-json]").each(function () {
                var $img = $(this);
                var data = JSON.parse($img.attr('data-mce-json'));
                $img.wrap('<a href="javascript:workspace.showPhoto(' + data.ownerId + ', ' + data.objectId + ')"></a>').removeAttr('data-mce-json');
            });
            return $(tmpEl).html();
        },

        /**
         * Resolves conflict between align-setting classes and simple style text-align
         * @param {String} dirtyHtml - may be with classes and styles
         * @return {String} cleanHtml - with only styles (tiny works with styles)
         */
        paragraphAlignFixFilter: function (dirtyHtml) {
            var tmpEl = $('<div></div>');
            $(tmpEl).html(dirtyHtml);
            $(tmpEl).find("p.mce-text-left").removeClass("mce-text-left").css("text-align", "left");
            $(tmpEl).find("p.mce-text-right").removeClass("mce-text-right").css("text-align", "right");
            $(tmpEl).find("p.mce-text-center").removeClass("mce-text-center").css("text-align", "center");
            $(tmpEl).find("p.mce-text-justify").removeClass("mce-text-justify").css("text-align", "justify");
            $(tmpEl).find(".mce-strikethrough").removeClass("mce-strikethrough").css("text-decoration", "line-through");
            return $(tmpEl).html();
        },

        /**
         *
         * @param {String} html - "dirty" html
         * @return {String} - "clean" html
         */
        preFormatEditorHtml: function (html) {
            html = html.replace(/<p>\s*<\/p>\s*<div/ig, '<div');
            html = html.replace(/<nobr>([\s\S]*?)<\/nobr>/ig, '$1');
            html = html.replace(/<\/div>\s*<p>\s*<\/p>/ig, '<\/div>');
            html = html.replace(/<div>([\s\S]*?)<\/div>/ig, '$1');
            var jTmpEl = $('<div></div>');
            jTmpEl.html(html);
            jTmpEl.find(".mce-context-panel").remove();

            TinyMcePlaneta.Formatting.parseRichMediaTags(jTmpEl);
            html = $(jTmpEl).html();
            return TinyMcePlaneta.Formatting.paragraphAlignFixFilter(html);
        },

        pastePostProcess: function(pl, o) {
            $(o.node).find('a').replaceWith(function () {
                var replacement = tinymce.DOM.create('a');
                $(replacement).attr('href', $(this).attr('href'));
                $(replacement).text($(this).text());
                return $(replacement);
            });
            // all p and div tags attrs will be cut
            // div tags transforms into p
            $(o.node).find('div,br').replaceWith(function () {
                var replacement = tinymce.DOM.create('p');
                $(replacement).html($(this).html());
                return $(replacement);
            });
            $(o.node).find('[align=left]').removeAttr('align').addClass('mce-text-left');
            $(o.node).find('[align=right]').removeAttr('align').addClass('mce-text-right');
            $(o.node).find('[align=center]').removeAttr('align').addClass('mce-text-center');
            $(o.node).find('[align=justify]').removeAttr('align').addClass('mce-text-justify');

            // remove all non-(p or a) tags
            TinyMcePlaneta.Formatting.removeBrs($(o.node));
            //noinspection JSLint
            var html = $(o.node).html().replace(/<(?!\s*\/?(a|p|ul|ol|li|br|pre)(>|\s+))[^>]*>/ig, '');
            html = TinyMcePlaneta.Formatting.paragraphAlignFixFilter(html);

            o.node.innerHTML = html;
        }
    };
}();

TinyMcePlaneta.defaultConfiguration.paste_postprocess = TinyMcePlaneta.Formatting.pastePostProcess;

TinyMcePlaneta.RichMedia = function (options) {
    var self = options;

    if(!self.clsName) {
        self.clsName = 'mceplanetarichmedia';
    }

    /**
     * Create
     * @param {Object} attributes
     * @return {jQuery} replacement element
     */
    function parseRichMedia (attributes) {
        var $imgReplacement = $(document.createElement('img'));
        $imgReplacement.attr({
            'data-mce-json': JSON.stringify(attributes),
            // ACHTUNG
            // to process before tiny initializing
            // future-known class name is hardcoded here
            'class': self.clsName,
            'src': ImageUtils.getThumbnailUrl(attributes.image, ImageUtils.ORIGINAL, ImageType.PHOTO)
        });
        if (attributes.width && parseFloat(attributes.width)) {
            $imgReplacement.attr({
                width: parseFloat(attributes.width)
            });
            if (attributes.height && parseFloat(attributes.height)) {
                $imgReplacement.attr('height', parseFloat(attributes.height));
            }
        }
        $imgReplacement.addClass(attributes.clazz);
        $imgReplacement.removeAttr('height');
        return $imgReplacement;
    }

    function createRichMedia(jEl) {
        if (jEl.is('img.' + self.clsName)) {
            var data = jEl.attr('data-mce-json');
            if(!data) {
                return {};
            }
            data = JSON.parse(data);
            var additionalClass = $.trim(jEl.attr('class').replace(self.clsName, ''));
            var width = jEl.attr('width') || jEl.width();
            // ignore height due to fixed aspect ratio
            data.clazz = additionalClass;
            if (width) {
                data.width = width;
            }
            if(!data.width) {
                delete data.width;
            }
            return data;
        }
    }

    return _.extend(self, {
        parseRichMedia: self.parseRichMedia || parseRichMedia,
        createRichMedia: self.createRichMedia || createRichMedia
    });
};


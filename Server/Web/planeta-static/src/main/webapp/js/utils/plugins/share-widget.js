/*global jQuery,console,_*/
(function ($) {

    jQuery.fn.extend({
        /**
         * Plugin for sharing content on different social networks
         * <p>
         * Usage:
         * <code>$(selector).share(options)</code>, where <code>options</code> are listed below. Defaults are provided for all <code>options</code>.
         * </p>
         * url: url of the resource to share, defaults to <code>window.location.href</code>
         * title: title of the resource, defaults to <code>document.title</code>
         * description: description of the resource
         * imageUrl: image of the resource
         * className: class name or list of class names(separated by space) to add to share container holder
         * itemClassName: class name or list of class names(separated by space) to add to share item
         * orientation: 'horizontal' or 'vertical', <code>'horizontal'</code> by default
         * counterEnabled: whether to display total counter, <code>true</code> by default
         * parseMetaTags: whether to parse meta tags of current page, <code>false</code> by default
         * hidden: whether share bar is hidden, if true, share bar would be displayed on hover on the <code>selector</code>; <code>true</code> by default
         * <p>
         *     Note: even if <code>parseMetaTags</code> is set to true, <code>url</code>, <code>title</code>, <code>description</code> and <code>imageUrl</code> would still be overwritten if specified in options
         * </p>
         * @param options
         * @return {*}
         */
        share: function (options) {
            var defaults = {
                orientation: 'horizontal',
                className: 'sharing-popup-social',
                counterEnabled: true,
                extendedCounter: false,
                parseMetaTags: false,
                hidden: true
            };
            options = $.extend({}, defaults, (options || {}));

            for (var name in options) {
                if (!(defaults.hasOwnProperty(name) || name === 'url'))
                    console.warn(name, ' is depricated');
            }

            var services = {
                'vkontakte': {title: 'В контакте', type: 'VKONTAKTE', className: 'bs-icons-vk'},
                'facebook': {title: 'Facebook', type: 'FACEBOOK', className: 'bs-icons-fb'},
                'twitter': {title: 'Twitter', type: 'TWITTER', className: 'bs-icons-tw'},
                'odnoklassniki': {title: 'Одноклассники', type: 'ODNOKLASSNIKI', className: 'bs-icons-ok'},
                'moimir': {title: 'Мой Мир', type: 'MOIMIR', className: 'bs-icons-mr'},
                'google_plus': {title: 'Google+', type: 'GOOGLE_PLUS', className: 'bs-icons-gp'},
                'surfingbird': {title: 'Surfingbird', type: 'SURFINGBIRD', className: 'bs-icons-sb'},
                'telegram': {title: 'Telegram', type: 'TELEGRAM', className: 'bs-icons-tg'}
            };

            //removes unused services
            try  {
                if (options.unusedServices && options.unusedServices.length > 0) {
                    _.each(options.unusedServices, function (num) {
                        delete services[num];
                    });

                }
            } catch (e) {
                console.error(e);
            }
            var MOUSE_HOVER_DELAY = 200;
            var isCounterInitialized = false;
            var prepareUrl = function (url) {
                if (window.shareHost) {
                    //for debug
                    url = url.replace(new RegExp('http://[^/]+'), 'https://' + window.shareHost);
                }
                var hashIndex = url.lastIndexOf('#');
                if (hashIndex > 0) {
                    url = url.substring(0, hashIndex); // cut off part of the url after hash
                }
                //https://planeta.atlassian.net/browse/PLANETA-14526
                return url.replace('!', '%21');
            };
            var createServiceUrl = function (service, options) {
                options = options || {};

                var metaTitle = '';
                if (options.parseMetaTags) {
                    metaTitle = $('meta[property$=title]').attr('content') || $('meta[name=title]').attr('content');
                }

                $.extend(options, options[service.type]);
                return '/api/share/share-url.html?' + $.param({
                    shareServiceType: service.type,
                    url: prepareUrl(options.url || window.location.href),
                    title: options.title || document.title || metaTitle || ''
                });
            };

            var share = function (service, options) {
                var serviceUrl = createServiceUrl(service, options);
                console.log(decodeURI(serviceUrl));
                // init popup
                var popupName = '_blank';
                var width = 554;
                var height = 349;
                var left = (screen.width - width) / 2;
                var top = (screen.height - height) / 2;
                var popupParams = 'scrollbars=0, resizable=1, menubar=0, left=' + left + ', top=' + top + ', width=' + width + ', height=' + height + ', toolbar=0, status=0';
                window.open(serviceUrl, popupName, popupParams);
            };
            return $(this).each(function () {
                // create share elements for each service
                var container = $('<div class="sps-button"></div>');

                var counterEl;

                var updateCounter = function (counter) {
                    var number = parseInt(counter, 10);
                    if (!counterEl || number === undefined) {
                        return;
                    }
                    counterEl.text(number);
                };

                var initCounter = function () {
                    if (!options.counterEnabled) {
                        return;
                    }
                    var url = options.url || window.location.href;
                    if (_.any(['localhost', '127.0.0.1'], function (it) {
                        return window.location.hostname.indexOf(it) !== -1;
                    }))
//                    if (['localhost', '127.0.0.1', 'localhost.local'].indexOf(window.location.hostname) != -1)
                        url = url.replace(window.location.hostname, 'planeta.ru').replace(':' + window.location.port, '');
                    url = prepareUrl(url);
                    $.ajax({
                        url: '/api/share/get-counter.json',
                        data: {url: url},
                        dataType: 'json',
                        success: function (response) {
                            if (response.success) {
                                updateCounter(response.result);
                                isCounterInitialized = true;
                            }
                        }
                    });
                };

                $.each(services, function (serviceName, service) {
                    var serviceItemEl = $('<div class="spsb-item"></div>');
                    serviceItemEl.attr({
                        alt: service.title,
                        title: service.title
                    });
                    var contentEl = $('<a href="javascript:void(0)"></a>').append($('<i></i>').addClass(service.className));
                    serviceItemEl.append(contentEl);

                    serviceItemEl.click(function (e) {
                        e.preventDefault();
                        e.stopPropagation();
                        var shareUrl = options.url || window.location.href;
                        if (service.handler) {
                            var title = options.title || document.title;
                            service.handler(shareUrl, title);
                        } else {
                            share(service, options);
                            // wait a little bit before updating counter
                            _.delay(function () {
                                initCounter();
                            }, 500);
                        }
                    });

                    if (options.itemClassName) {
                        serviceItemEl.addClass(options.itemClassName);
                    }
                    container.append(serviceItemEl);
                });

                // add share elements to the container

                var wrapperClass = 'sharing-popup-social' + (options.isMobile ? " mobile-sharing" : "");
                var wrapperEl = $('<div class="' + wrapperClass + '"></div>');
                wrapperEl.append(container);

                var appendWrapperTo = function (jElement) {
                    jElement.find('.' + wrapperClass).remove();
                    jElement.append(wrapperEl);
                };

                var parentEl = options.hidden ? $(this).parent('div') : $(this);
                var popupElement;

                //checks if sharing in modal block
                if (options.modal) {
                    var offset = $(this).offset();
                    var wrapperContainer = $('<div class="sharing-popup" style="width: 88px;"></div>');
                    $(wrapperContainer).css({
                        'left': offset.left,
                        'top': offset.top
                    });
                    appendWrapperTo(wrapperContainer);
                    $('body .photo-view-sharing').remove();
                    $('body').append(wrapperContainer);
                    popupElement = wrapperContainer;
                    popupElement.mouseenter(function () {
                        popupElement.addClass('mouseenter');
                    });
                    popupElement.mouseleave(function () {
                        popupElement.removeClass('mouseenter');

                        _.delay(function () {
                            popupElement.removeClass('show');
                        }, MOUSE_HOVER_DELAY);
                    });
                } else {
                    appendWrapperTo(parentEl);
                    popupElement = wrapperEl;
                }

                if (options.className) {
                    popupElement.addClass(options.className);
                }


                // hide plugin and show it on certain events

                var hidePopup = function () {
                    if (options.modal) {
                        if (!popupElement.hasClass('mouseenter')) {
                            popupElement.removeClass('show');
                        }
                    } else {
                        popupElement.hide();
                    }
                };

                var showPopup = function () {

                    if (options.modal) {
                        popupElement.addClass('show');
                    } else {
                        popupElement.show();
                    }
                    if (!isCounterInitialized && options.counterEnabled) {
                        initCounter();
                    }
                };

                if (options.hidden) {
                    hidePopup();
                    // show share popup on 'mouseenter' with delay
                    parentEl.mouseenter(function () {
                        parentEl.addClass('mouseenter');
                        if (!popupElement.is(':visible')) {
                            _.delay(function () {
                                if (parentEl.hasClass('mouseenter')) {
                                    showPopup();
                                }
                            }, MOUSE_HOVER_DELAY);
                        }
                    });

                    // hide share popup on 'mouseleave' after delay
                    parentEl.mouseleave(function () {
                        parentEl.removeClass('mouseenter');
                        _.delay(function () {
                            if (!parentEl.hasClass('mouseenter')) {
                                hidePopup();
                            }
                        }, MOUSE_HOVER_DELAY);
                    });


                    // show share popup on 'click'
                    $(this).click(function () {
                        if (!popupElement.is(':visible')) {
                            showPopup();
                        }
                    });
                }

                var word;
                if (window.workspace && window.workspace.currentLanguage !== 'ru') {
                    word = 'Shares';
                } else {
                    word = 'Поделились';
                }

                // init counter if enabled
                if (options.counterEnabled) {
                    var counterCont = $('<div class="sps-count"></div>');
                    counterCont.html(word + '<span></span>');
                    counterEl = counterCont.find('span');
                    wrapperEl.append(counterCont);


                    updateCounter(0);


                    if (!options.hidden) {
                        initCounter();
                    }
                }

            });

        }
    });
}(jQuery));

/**
 * http://habrahabr.ru/post/145988/
 * see ru.planeta@yandex.ru mail - this mail is login for all accounts
 * see OAuthController java class
 * @type {Object}
 */
var OAuthUtils = function () {

    var getSaveRedirectUrl = function () {
        return 'https://' + workspace.serviceUrls.mainHost + '/welcome/save-redirect.html';
    };

    var providerClick = function (providerName) {
        if(window.gtm) {
            window.gtm.trackUniversalEvent('socialnetworks_success_registration');
        }
        window.location.href = getSaveRedirectUrl() + "?provider=" + providerName + "&referrerUrl=" + encodeURIComponent(document.location.href);
    };
    /**
     *
     */
    return {
        facebookClick: function () {
            providerClick("fb");
        },
        /**
         * https://oauth.yandex.ru/client/012dcb4f0d71470cae275c02cbefd06c/edit
         */
        yandexClick: function () {
            providerClick("ya");
        },
        /**
         * http://www.odnoklassniki.ru/games/planeta-app
         */
        odnoklassnikiClick: function () {
            providerClick("ok");
        },
        /**
         * mail : ru.planeta@mail.ru
         * http://api.mail.ru/sites/my/687562
         */
        mailClick: function () {
            providerClick("mailru");
        },

        /**
         * http://vk.com/editapp?id=3123355&section=options
         */
        vkontakteClick: function () {
            providerClick("vk");
        }
    }


}();

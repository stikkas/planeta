TinyMcePlaneta.createModule('characterCount', function () {
    var editor = this.editor;
    var limit = editor.getParam('limit');
    var $count = $('<div class="rest-count">').text(limit);
    var selectionLength = 0;

    var ua = navigator.userAgent;
    var isSafari = ua.indexOf('Safari') != -1 && ua.indexOf('Chrome') == -1;
    var isIOS = ua.match(/(iPad|iPhone|iPod)/g) !== null;
    var isAndroid = /(android)/i.test(ua.toLocaleLowerCase());
    var isIE = tinymce.isIE;
    var isPasteDifficultly = isSafari || isIOS || isAndroid || isIE;

    editor.onInit.add(function () {

        $('#' + editor.editorId).after($count);
        var remSettings = $.extend({}, $.fn.limitInput.defaults, {
            warnIfZeroComes: false,
            limit: limit,
            remTextEl: $count
        });

        $(editor.getBody()).on('mouseup mousedown keyup keydown touchstart touchend', function () {
            selectionLength = editor.selection.getContent({format: 'text'}).length;
        });

        editor.onNodeChange.add(function () {
            var html = editor.getContent({format: 'raw'});
            if ( html === '<div><br></div>' ) {
                editor.setContent('<p><br></p>', {format: 'raw'});
            }

            update();
        });
        editor.onKeyDown.add(function (ed, e) {
            update(e);
        });
        editor.onKeyUp.add(function (ed, e) {
            update(e);
        });
        editor.onRedo.add(function () {
            update();
        });
        editor.onUndo.add(function () {
            $(editor.getBody()).find('#_mcePaste').remove();
            update();
        });
        editor.onPaste.add(function (ed, e) {
            var text = TinyMcePlaneta.textClean(editor).length;
            var limitRest = limit - text;
            var content = ((e.originalEvent || e).clipboardData || window.clipboardData).getData('Text');
            if ( limitRest <= 0 && selectionLength === 0 ) {
                e.preventDefault();
                return false;
            } else if ( content.length > limitRest + selectionLength ) {
                if ( isPasteDifficultly ) {
                    e.preventDefault();
                    return false;
                } else {
                    e.preventDefault();
                    content = content.substring(0, limitRest + selectionLength);
                    editor.execCommand('mceInsertContent', false, content);
                }
            }
        });
        editor.onSetContent.add(function () {
            update();
        });


        var update = function (e) {
            var text = TinyMcePlaneta.textClean(editor).length;
            var limitRest = limit - text;
            if (limitRest <= 0 && selectionLength === 0) {
                if (!!e) {
                    if ( !( e.keyCode == 8 ||   // backspace
                            e.keyCode == 9 ||   // tab
                            e.keyCode == 46 ||  // deleteByProfileId
                            e.keyCode == 33 ||  // pageup
                            e.keyCode == 34 ||  // pagedown
                            e.keyCode == 37 ||  // left
                            e.keyCode == 38 ||  // top
                            e.keyCode == 39 ||  // right
                            e.keyCode == 40 ||  // bottom
                            e.metaKey ||
                            (e.ctrlKey && e.keyCode !== 13) ||
                            (e.shiftKey && e.keyCode !== 13) ||
                            (e.altKey && e.keyCode >= 37 && e.keyCode <= 40) ) ) {
                        e.preventDefault();
                    }
                }
            }
            limitRest = limitRest < 0 ? 0 : limitRest;
            $count.text(remSettings.remFilter(remSettings, limitRest));
        };
    });
});

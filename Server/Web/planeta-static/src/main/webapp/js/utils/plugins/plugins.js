/*global jQuery*/
(function ($) {
    var methods = {
        init: function (options) {
            //check for absent elements
            if (!this.length) {
                return this;
            }

            var settings = $.extend({}, $.fn.limitInput.defaults, options);

            var updateRemText = function ($el) {
                var charsRem = settings.limit - $el.val().length;
                var text = settings.remFilter(settings, charsRem);
                var hide = $el.hideCounter || false;
                var counter = settings.remTextEl.text(text);
                var button = settings.buttonToDisable || $();
                if (settings.showFrom) {
                    if ($el.val().length < settings.showFrom) {
                        hide = true;
                    }
                }

                if (hide && !counter.is('.hidden')) {
                    counter.addClass('hidden');
                } else if (!hide && counter.is('.hidden')) {
                    counter.removeClass('hidden');
                }
                if (charsRem >= 0 && charsRem <= 20 && settings.warnIfZeroComes) {
                    counter.addClass('rest-count-less');
                }
                if (charsRem < 0 && settings.enableNegative) {
                    counter.addClass('rest-count-super-less');
                    button.addClass('disabled');
                }
                if (charsRem > 20 && counter.hasClass('rest-count-less')) {
                    counter.removeClass('rest-count-less');
                }
                if (charsRem >= 0 && counter.hasClass('rest-count-super-less')) {
                    counter.removeClass('rest-count-super-less');
                    button.removeClass('disabled');
                }
            };

            var onKeyUp = function onKeyUp() {
                var $this = $(this);
                var value = $this.val();
                if (value.length > settings.limit && !settings.enableNegative) {
                    //cut value
                    $this.val(value.substring(0, settings.limit));
                }
                updateRemText($this);
            };

            var onKeyPress = function onKeyPress(e) {
                if (!e.keyCode || (e.keyCode > 46 && e.keyCode < 90) || e.keyCode == 13) {
                    //check length
                    if (!settings.enableNegative) {
                        return $(this).val().length <= settings.limit;
                    }
                }
            };

            var onClick = function onClick(e) {
                var $this = $(this);
                var value = $this.val();
                if (value.length > settings.limit && !settings.enableNegative) {
                    //cut value
                    $this.val(value.substring(0, settings.limit));
                }
                updateRemText($this);
            };

            return this.each(function () {

                if ($.data(this, 'inputLimit')) {
                    return;
                }

                var $this = $(this);

                $.data(this, 'inputLimit', true);

                if (typeof(settings.remTextEl) == 'string') {
                    settings.remTextEl = $(this).parent().find(settings.remTextEl);
                } else if (!settings.remTextEl) {
                    var span = $('<span></span>');
                    $this.after(span);
                    settings.remTextEl = span;
                }

                $this.bind('keyup', onKeyUp);
                $this.bind('keypress', onKeyPress);
                $this.bind('click', onClick);

                if (settings.toggleCounter) {
                    if (settings.toggleCounter.show) {
                        $this.hideCounter = true;
                        $this.bind(settings.toggleCounter.show, function () {
                            $this.hideCounter = false;
                            updateRemText($this);
                        });
                    }
                    if (settings.toggleCounter.hide) {
                        $this.bind(settings.toggleCounter.hide, function () {
                            $this.hideCounter = true;
                            updateRemText($this);
                        });
                    }
                }

                updateRemText($this);
            });

        },
        update: function (text) {
            if (typeof text != 'string') {
                return;
            }
            return this.each(function () {
                if (!$.data(this, 'inputLimit')) {
                    return;
                }

                var $this = $(this);

                // insert new value and emulate key up event
                $this.val(text).trigger('keyup');
            });
        },
        hide: function () {
            return this.each(function () {
                if (!$.data(this, 'inputLimit')) {
                    return;
                }

                var $this = $(this);
                $this.hideCounter = true;
                $this.trigger('keyup');
            });
        },
        show: function () {
            return this.each(function () {
                if (!$.data(this, 'inputLimit')) {
                    return;
                }

                var $this = $(this);
                $this.hideCounter = false;
                $this.trigger('keyup');
            });
        }
    };

    $.fn.extend({

        limitInput: function (options) {
            if (methods[options]) {
                return methods[options].apply(this, Array.prototype.slice.call(arguments, 1));
            }
            if (typeof options == 'object' || !options) {
                return methods.init.apply(this, arguments);
            }
            return $.error('Method ' + options + ' does not exist on jQuery.limitInput');
        }
    });

    $.fn.limitInput.defaults = {
        limit: 255,
        //text for show remaining chars count
        remText: '%lp %n %s',
        //text for show excess chars count (if enableNegative is set)
        remTextNegative: '%ln %n %s',
        //el where show how chars is remaining
        remTextEl: null,
        warnIfZeroComes: true,
        //returns formatted text, by default with plurals
        remFilter: function (settings, charsRem) {
            var L10n = {
                _dictionary:{
                    "ru":{
                        "leftOne":"Oстался",
                        "leftMany":"Осталось",
                        "extraOne":"Лишний",
                        "extraMany":"Лишние",
                        "symbolOne":"символ",
                        "symbolOneA":"символа",
                        "symbolMany":"символов"
                    },
                    "en":{
                        "leftOne":"Left",
                        "leftMany":"Left",
                        "extraOne":"Extra",
                        "extraMany":"Extra",
                        "symbolOne":"symbol",
                        "symbolOneA":"symbol",
                        "symbolMany":"symbols"
                    }
                }
            };

            var translate = function (word, lang) {
                if (!lang)
                    throw "language is empty.";
                return L10n._dictionary[lang][word] || word;
            };

            var lang = window.workspace && (workspace.currentLanguage === "en") ? "en" : "ru";

            var getPlural = function (number, titles) {
                var cases = [2, 0, 1, 1, 1, 2];
                return titles[ (number % 100 > 4 && number % 100 < 20) ? 2 : cases[(number % 10 < 5) ? number % 10 : 5] ];
            };

            var number = 0;
            var remTextPattern = '';
            if ((charsRem >= 0)) {
                remTextPattern = settings.remText;
                number = charsRem;
                remTextPattern = remTextPattern.replace(/%lp/, getPlural(number, [translate('leftOne', lang), translate('leftMany', lang), translate('leftMany', lang)]));
            } else if (settings.enableNegative && (charsRem < 0)) {
                remTextPattern = settings.remTextNegative;
                number = -charsRem;
                remTextPattern = remTextPattern.replace(/%ln/, getPlural(number, [translate('extraOne', lang), translate('extraMany', lang), translate('extraMany', lang)]));
            } else {
                number = charsRem >= 0 ? charsRem : 0;
                remTextPattern = settings.remText;
            }

            var remText = remTextPattern.replace(/%n/, number);
            var symbols = getPlural(number, [translate('symbolOne', lang), translate('symbolOneA', lang), translate('symbolMany', lang)]);

            return remText.replace(/%s/, symbols);
        },
        //option to {show:"event",hide:"event"}
        toggleCounter: false,
        enableNegative: false
    };


}(jQuery));

(function($) {
    $.fn.numbersOnly = function () {
        return this.each(function () {
            var that = $(this);
            that.on('focus', function () {
                that.attr('data-prev', that.val());
            });
            that.on('keydown input', function(e) {
                if (e.keyCode == undefined) {
                    return true;
                } else if (e.shiftKey) {

                }
                var keyCodes = [8, 9, 37, 39, 46];
                for (var i = 0; i < 10; ++i) {
                    keyCodes.push(48 + i);
                    keyCodes.push(96 + i);
                }
                var that = $(this);
                var prev = that.attr('data-prev') || '';
                var result = !e.shiftKey && keyCodes.indexOf(e.keyCode) != -1;
                if (!result) {
                    setTimeout(function () {that.val(prev);}, 0);
                    e.preventDefault && e.preventDefault();
                    e.cancelBubble && e.cancelBubble();
                } else {
                    setTimeout(function () {that.attr('data-prev', that.val());}, 0);
                }
                return result;
            });
        });
    }
})(jQuery);

jQuery.fn.extend({

    checkEmail: function (options) {
        var isEmailValidLocal = true;
        var serverEmailStatus;
        var isEmailValidServer = true;
        var $email = options.$email;
        var $hintContainer = options.$hintContainer || $();
        var $hint = options.$hint;
        var disableButtons = options.disableButtons;
        var signupLink = options.signupCallback;
        var $submit = options.$submit;
        var disableButtonOnAlreadyExists = options.disableButtonOnAlreadyExists || false;
        var hideMessageOnMayBeNotValid = options.hideMessageOnMayBeNotValid || false;
        var checkCallback = options.checkCallback;

        var serverMessage;
        var localMessage;

        var setSubmitButtonStatus = function (isEnable) {
            if (isEnable) {
                $submit.removeClass('disabled');
                $submit.unbind('click.registerSuggest');
            } else {
                $submit.addClass('disabled');
                $submit.unbind('click.registerSuggest');
                $submit.bind('click.registerSuggest', function (e) {
                    e.preventDefault();
                    e.stopPropagation();
                });
            }
        };

        var showMessageOnSuccess = function (email) {

            if (isEmailValidServer === null || isEmailValidLocal === null) {
                return;
            }
            if (email != $email.val()) {
                return;
            }

            $hint.hide();
            $hint.removeClass('warning-message-error');
            $hintContainer.hide();
            if (disableButtons && (serverEmailStatus != 'NOT_VALID')) {
                setSubmitButtonStatus(!(disableButtonOnAlreadyExists && (serverEmailStatus == 'ALREADY_EXIST')));
            }

            if (isEmailValidServer) {
                return;
            }

            if (isEmailValidLocal || serverEmailStatus == "ALREADY_EXIST") {
                if (hideMessageOnMayBeNotValid && (serverEmailStatus == 'MAY_BE_NOT_VALID')) {
                    return;
                }

                $hint.html(serverMessage);
            } else {
                $hint.html(localMessage);
            }

            if (signupLink) {
                $hint.find('a.signup-link').bind('click', signupLink);
            }

            if (serverEmailStatus == 'ALREADY_EXIST') {
                $hint.addClass('warning-message-error');
                $hint.find('a.force-logout').click(function(e) {
                    workspace.navigate($(this).attr('href'));
                });
            }

            $hint.fadeIn(150);
            $hintContainer.show();
        };

        function setServerMessage(message) {
            var L10n = {
                _dictionary:{
                    "ru":{
                        "emailExists":"Пользователь с таким e-mail уже зарегистрирован.<br>",
                        "exitAndLogInAsThisUser":"Выйти и зайти под ним.",
                        "logInAsThisUser":"Войти под ним.",
                        "emailTroubleMsg":'<br><sub>Если Вы не регистрировались на этот email, Вы можете написать об этом на <a href="mailto:support@planeta.ru">support@planeta.ru</a></sub>',
                        "maybeEmailDoesntExists":"Возможно, e-mail не существует"
                    },
                    "en":{
                        "emailExists":"User with this e-mail is already registered.<br>",
                        "exitAndLogInAsThisUser":"Log out and log in as this user.",
                        "logInAsThisUser":"Log in as this user.",
                        "emailTroubleMsg":'<br><sub>If you did not register using this e-mail, You can write about it to <a href="mailto:support@planeta.ru">support@planeta.ru</a></sub>',
                        "maybeEmailDoesntExists":"Probably this e-mail doesn’t exist"
                    }
                }
            };

            var translate = function (word, lang) {
                if (!lang)
                    throw "language is empty.";
                return L10n._dictionary[lang][word] || word;
            };

            var lang = window.workspace && (workspace.currentLanguage === "en") ? "en" : "ru";

            if (serverEmailStatus == 'ALREADY_EXIST') {
                message = translate('emailExists', lang);
                if (workspace.isAuthorized) {
                    message += '<a class="force-logout" href="/welcome/account-merge-email.html?email=' + $email.val() + '&redirectUrl=' + location.href + '">' + translate("exitAndLogInAsThisUser", lang) + '</a>';
                } else {
                    message += '<a class="signup-link" href="javascript:void(0)">' + translate('logInAsThisUser', lang) + '</a>';
                }
                message += translate('emailTroubleMsg', lang);
            } else {
                message = translate('maybeEmailDoesntExists', lang);
            }
            serverMessage = message;
        }

        var check = function () {

            var email = $email.val();
            if (!email || $email.data('oldValue') && $email.data('oldValue') == email) {
                return;
            }
            $email.data('oldValue', email);

            if (disableButtons) {
                setSubmitButtonStatus(false);
            }


            isEmailValidServer = null;
            isEmailValidLocal = null;
            serverMessage = localMessage = null;
            $hint.hide();
            $hintContainer.hide();

            var email = $email.val();
            $email.mailcheck({
                suggested: function (element, suggestion) {
                    var L10n = {
                        _dictionary: {
                            "ru": {
                                "maybeYouMeant": "Возможно вы имели в виду"
                            },
                            "en": {
                                "maybeYouMeant": "Probably you meant"
                            }
                        }
                    };

                    var translate = function (word, lang) {
                        if (!lang)
                            throw "language is empty.";
                        return L10n._dictionary[lang][word] || word;
                    };

                    var lang = window.workspace && (workspace.currentLanguage === "en") ? "en" : "ru";

                    // if (!$hint.html()) {
                    // First error - fill in/show entire hint element
                    localMessage = translate('maybeYouMeant', lang) + " <span class='suggestion'>" +
                        "<span class='address'>" + suggestion.address + "</span>"
                        + "@<a href='javascript:void(0)' class='domain'>" + suggestion.domain +
                        "</a></span>?";

                    isEmailValidLocal = false;
                    $('.suggestion', $hint).live('click', function () {
                        // On click, fill in the field with the suggestion and remove the hint
                        $email.val($hint.find(".suggestion").text());
                        $email.change();
                        return false;
                    });

                    showMessageOnSuccess(email);
                },

                empty: function () {
                    isEmailValidLocal = true;
                    showMessageOnSuccess(email);
                }
            });

            $.ajax({
                url: '/api/public/email-check',
                type: 'GET',
                data: {
                    email: $email.val()
                },

                success: function (response) {
                    if (response && response.success) {
                        serverEmailStatus = response.result;
                        isEmailValidServer = (serverEmailStatus == 'NEW_VALID_EMAIL');
                    } else {
                        isEmailValidServer = false;
                    }
                    if (!isEmailValidServer) {
                        var message = response.errorMessage;
                        setServerMessage(message);
                    }
                    showMessageOnSuccess(email);

                    if (checkCallback) {
                        checkCallback(response);
                    }
                },

                error: function(response) {
                    if (checkCallback) {
                        checkCallback(response);
                    }
                }

            });

        };

        try {
            if (options.showEmail) {
                isEmailValidServer = false;
                serverEmailStatus = 'ALREADY_EXIST';
                $email.val(options.showEmail);
                setServerMessage();
                showMessageOnSuccess(options.showEmail);
            }
            if (options.enterWithEmail) {
                signupLink();
            }
            $email.bind('keyup change', _.debounce(check, 1000));
            $(this).data('check', check);
        } catch (e) {
            console.error(e);
        }
    }
});

(function ($) {
    $.fn.megafonTooltip = function () {
        var tooltipTimeout;
        var wrap = $('.megafon-badge_tooltip-wrap');
        if (wrap.length == 0) {
            var L10n = {
                _dictionary: {
                    "ru": {
                        "megafonBonus": "Если вы поддержите этот проект, компания «МегаФон» увеличит в&nbsp;4&nbsp;раза сумму вашей поддержки."
                    },
                    "en": {
                        "megafonBonus": 'If you back this campaign, "Megafon" company multiply amount of your backing by&nbsp;4.'
                    }
                }
            };

            var translate = function (word, lang) {
                if (!lang)
                    throw "language is empty.";
                return L10n._dictionary[lang][word] || word;
            };

            var lang = window.workspace && (workspace.currentLanguage === "en") ? "en" : "ru";

            wrap = $(
            '<div class="megafon-badge_tooltip-wrap">\
                <div class="megafon-badge_tooltip">\
                    <div class="megafon-badge_tooltip_tail"></div>\
                    <div class="megafon-badge_tooltip_logo">\
                        <span class="s-megafon-tooltip"></span>\
                    </div>\
                    <div class="megafon-badge_tooltip_text">' + translate('megafonBonus', lang) + '</div>\
                </div>\
            </div>');
            $('body').append(wrap);
        }
        this.find('.megafon-badge').hover(function () {
            clearTimeout(tooltipTimeout);
            var badgePosition = $(this).offset();
            wrap.css({top: badgePosition.top, left: badgePosition.left}).show();
        }, hideTooltip);

        wrap.hover(function () {
            clearTimeout(tooltipTimeout);
        }, hideTooltip);

        function hideTooltip() {
            tooltipTimeout = setTimeout(function () {
                wrap.hide();
            }, 50);
        }
    };
})(jQuery);
/**
 * Created with IntelliJ IDEA.
 * User: alexa_000
 * Date: 01.08.13
 * Time: 18:38
 */
var CommonUtils = {
    urlParam: function (name) {
        var query;
        if (workspace && workspace.urlQuery) {
            query = workspace.urlQuery;
        } else {
            query = window.location.search;
        }
        var results = new RegExp('(^|&)' + name + '=([^&#]*)').exec(query);
        return results ? results[2] || 0 : null;
    }
};
var Widgets = {
    Models: {},
    Views: {}
};

Widgets.Models.BaseWidget = BaseModel.extend({

    defaults: {
        // keep this property in sync with WidgetType enum
        widgetTypes: [
            {widgetType: 'STANDARD', width: 228, height: 289},
            {widgetType: 'FAT_SKYSCRAPER', width: 240, height: 400},
            {widgetType: 'SQUARE_250', width: 250, height: 250},
            {widgetType: 'SQUARE_336', width: 336, height: 280}
        ]
   },

    initialize: function(options) {
        this.changeWidget();
    },
    getUrl: function(widgetType) {
        return 'implement it!';
    },
    changeWidget: function(widgetType) {
        var selectedWidget = _.find(this.get('widgetTypes'), function(type) {
            return type.widgetType == widgetType;
        });
        if (!selectedWidget) {
            selectedWidget = this.get('widgetTypes')[0];
        }
        selectedWidget.widgetUrl = this.getUrl(selectedWidget.widgetType);
        this.set({selectedWidget: selectedWidget});
    }

});

Widgets.Models.CampaignWidget = Widgets.Models.BaseWidget.extend({
	getUrl: function(widgetType) {
		return 'https://' + workspace.serviceUrls.widgetsAppUrl + '/widgets/campaign-widget.html?campaignId=' + this.get('widgetId') + '&type=' + widgetType;
	}
});

Widgets.Views.CampaignWidget = Modal.View.extend({
	template: '#widget-code-dialog-template',
    className: 'modal project-widget-modal',

    afterRender: function() {
        Modal.View.prototype.afterRender.call(this);
        this.$el.find(':input[type=radio]').radioButton({wrapperClassName: "rr-item"});
        this.$el.find('.rr-item').css('width', 'auto');
        var html = this.$el.find('textarea').val();
        this.$el.find('textarea').val(html.replace(/_tmplitem=(['"])[^'"]*\1\s*/, ''));
    },

    events: {
        'click .radio-row input': 'onWidgetSizeChanged',
        'click .cancel': 'cancel',
        'click button[type=reset]': 'cancel',
        'click textarea': 'selectText'
    },

    onWidgetSizeChanged: function(e) {
        e.preventDefault();
        this.model.changeWidget($(e.currentTarget).val());
    },

    selectText: function() {
        this.$el.find('textarea').select();
    }

});

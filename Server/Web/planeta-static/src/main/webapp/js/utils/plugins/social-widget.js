/*global jQuery,console,_*/
(function ($) {

  jQuery.fn.extend({

      /**
       * Plugin for social widgets
       * <p>
       * Usage:
       * <code>$(selector).socialize(options)</code>, where <code>options</code> are listed below. Defaults are provided for all <code>options</code>.
       * </p>
       * title: title to display on top of the widget
       * template: template <code>'#social-widget-sidebar-template'</code> by default
       * wrap: whether to wrap widget<code>true</code> by default
       * width: width of the widget <code>258</code> by default
       * @param options
       * @return {*}
       */
      socialize: function (options) {
          var title;
          if(window.workspace && workspace.currentLanguage && workspace.currentLanguage === 'en') {
              title = '<b>Join</b> the Planet!';
          } else {
              title = '<b>Присоединяйтесь</b> к Планете!';
          }
          var defaults = {
              title: title,
              template: '#social-widget-sidebar-template',
              wrap: true,
              width: 258
          };
          options = $.extend({}, defaults, (options || {}));

          return $(this).each(function () {
                var self = this;
                $(self).empty();

                var tabRules = [];
                tabRules['vk'] = 'fb';
                tabRules['fb'] = 'vk';

                TemplateManager.tmpl(options.template, options).done(function (html) {
                    $(self).append(html);

                    var tabs = $(self).find('.tab-pane');
                    if (tabs.size() > 0) {
                        var tabToSelectId = 0;
                        $(tabs[tabToSelectId]).addClass('active');
                        $($(self).find('.js-tab-pane-li')[tabToSelectId]).addClass('active');
                    }

                    $(self).find('.sidebar-social-widget-tabs .nav  a').click(function (e) {
                        e.preventDefault();
                        $(this).tab('show');
                    });
                });
            });
      }
    });
}(jQuery));

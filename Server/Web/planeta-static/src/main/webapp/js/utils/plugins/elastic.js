/**
 *    @name                            Elastic
 *    @description                     Elastic is jQuery plugin that grow and shrink your textareas automatically
 *    @version                        1.6.11
 *    @requires                        jQuery 1.2.6+
 *
 *    @author                            Jan Jarfalk
 *    @author-email                    jan.jarfalk@unwrongest.com
 *    @author-website                    http://www.unwrongest.com
 *
 *    @preserve                  MIT License - http://www.opensource.org/licenses/mit-license.php
 */
(function ($) {
    jQuery.fn.extend({
        elastic: function (oneline) {
            var onResizeCallback = function () {
            };
            //	We will create a div clone of the textarea
            //	by copying these attributes from the textarea to the div.
            var mimics = [
                'paddingTop',
                'paddingRight',
                'paddingBottom',
                'paddingLeft',
                'fontSize',
                'lineHeight',
                'fontFamily',
                'width',
                'fontWeight',
                'border-top-width',
                'border-right-width',
                'border-bottom-width',
                'border-left-width',
                'borderTopStyle',
                'borderTopColor',
                'borderRightStyle',
                'borderRightColor',
                'borderBottomStyle',
                'borderBottomColor',
                'borderLeftStyle',
                'borderLeftColor'
            ];

            var editing = false;

            return this.each(function () {

                // Elastic only works on textareas
                if (this.type !== 'textarea') {
                    return false;
                }

                var $textarea = jQuery(this),
                    $twin = jQuery('<div />').css({
                        'position': 'absolute',
                        'display': 'none',
                        'word-wrap': 'break-word',
                        'white-space': 'pre-wrap'
                    }),
                    lineHeight = parseInt($textarea.css('line-height'), 10) || parseInt($textarea.css('font-size'), '10'),
                    minheight = oneline ? lineHeight : parseInt($textarea.css('height'), 10) || lineHeight * 3,
                    maxheight = parseInt($textarea.css('max-height'), 10) || Number.MAX_VALUE,
                // NOTE: Removed padding check (wall textarea and comment textarea height were wrong because of it)
                    padding = parseInt($textarea.css('padding-top'), 10) + parseInt($textarea.css('padding-bottom'), 10) || 0;
                goalheight = 0;

                // Opera returns max-height of -1 if not set
                if (maxheight < 0) {
                    maxheight = Number.MAX_VALUE;
                }

                // Append the twin to the DOM
                // We are going to meassure the height of this, not the textarea.
                $twin.appendTo($textarea.parent());

                // Copy the essential styles (mimics) from the textarea to the twin
                var i = mimics.length;
                while (i--) {
                    $twin.css(mimics[i].toString(), $textarea.css(mimics[i].toString()));
                }

                // Updates the width of the twin. (solution for textareas with widths in percent)
                function setTwinWidth() {
                    var curatedWidth = Math.floor(parseInt($textarea.width(), 10));
                    if ($twin.width() !== curatedWidth) {
                        $twin.css({'width': curatedWidth + 'px'});

                        // Update height of textarea
                        update(true);
                    }
                    onResizeCallback();
                }

                // Sets a given height and overflow state on the textarea
                function setHeightAndOverflow(height, overflow) {

                    if ($textarea.val() == '') {
                        $textarea.css('height', '');
                    } else {
                        var curratedHeight = Math.floor(parseInt(height, 10));
                        if (getHeight($textarea) !== curratedHeight) {
                            $textarea.css({'height': curratedHeight + 'px', 'overflow': overflow});
                        }
                    }
                }

                function blur(forced) {
                    if (forced) {
                        editing = false;
                    }
                    var twinHeight = getHeight($twin);
                    if ($textarea.val() == '') {
                        $textarea.css('height', '');
                    } else if (twinHeight < maxheight) {
                        if (twinHeight > minheight) {
                            $textarea.height(twinHeight + padding);
                        } else {
                            // "+ padding" is added for blog-comment PLANETA-1933
                            $textarea.height(minheight + padding);
                        }
                    }
                }

                function getHeight($el) {
                    var prefixes = ['box-sizing', '-moz-box-sizing', '-webkit-box-sizing'];
                    var method = 'height';
                    $.each(prefixes, function (key, value) {
                        if ($el.css(value) == 'border-box') {
                            method = 'outerHeight';
                        }
                    });
                    return $el[method]();

                }

                // This function will update the height of the textarea if necessary
                function update(forced) {

                    // Get curated content from the textarea.
                    var textareaContent = $textarea.val().replace(/&/g, '&amp;').replace(/ {2}/g, '&nbsp;').replace(/<|>/g, '&gt;').replace(/\n/g, '<br />');

                    // Compare curated content with curated twin.
                    var twinContent = $twin.html().replace(/<br>/ig, '<br />');

                    if (forced || textareaContent + '&nbsp;' !== twinContent) {

                        // Add an extra white space so new rows are added when you are at the end of a row.
                        $twin.html(textareaContent + '&nbsp;');

                        var twinHeight = getHeight($twin);
                        // Change textarea height if twin plus the height of one line differs more than 3 pixel from textarea height
                        if (Math.abs(twinHeight + lineHeight - getHeight($textarea)) > 3) {

                            var goalheight = twinHeight;//+lineHeight;
                            if (goalheight >= maxheight) {
                                setHeightAndOverflow(maxheight + padding, 'auto');
                            } else if (goalheight <= minheight) {
                                setHeightAndOverflow(minheight + padding, 'hidden');
                            } else {
                                setHeightAndOverflow(goalheight + padding, 'hidden');
                            }

                        }

                    }

                    if (!editing) {
                        blur();
                    }

                }

                function addCallback(callback) {
                    onResizeCallback = callback;
                }

                // Hide scrollbars
                $textarea.css({'overflow': 'hidden'});

                // Update textarea size on keyup, change, cut and paste
                $textarea.bind('keyup change cut paste', function () {
                    editing = true;
                    update();
                });

                // Update width of twin if browser or textarea is resized (solution for textareas with widths in percent)
                $(window).bind('resize', setTwinWidth);
                $textarea.bind('resize', setTwinWidth);
                $textarea.bind('update', update);

                // Compact textarea on blur
                $textarea.bind('blur', blur);

                // And this line is to catch the browser paste event
                $textarea.bind('input paste', function (e) {
                    setTimeout(update, 250);
                });

                // Run update once when elastic is initialized
                update();

                function twinInit() {
                    setTwinWidth();
                    $textarea.unbind("focus mousemove click", twinInit);
                }

                $textarea.bind("focus mousemove click", twinInit);
                setTimeout(setTwinWidth, 100);

            });

        }
    });
})(jQuery);
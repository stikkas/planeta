var Order = {

    Type: {
        SHARE: 'SHARE',
        PRODUCT: 'PRODUCT',
        TICKET: 'TICKET',
        TICKET_NEW: 'TICKET_NEW',
        BONUS: 'BONUS'
    },

    PaymentStatus: {
        PENDING: 'PENDING',
        COMPLETED: 'COMPLETED',
        CANCELLED: 'CANCELLED'
    },

    DeliveryStatus: {
        PENDING: 'PENDING',
        IN_TRANSIT: 'IN_TRANSIT',
        DELIVERED: 'DELIVERED',
        REJECTED: 'REJECTED',
        CANCELLED: 'CANCELLED',
        GIVEN: 'GIVEN'
    }
};

Order.Utils = {

    getStatuses: function(paymentStatus, deliveryStatus) {
        return {
            paymentStatus: paymentStatus,
            deliveryStatus: deliveryStatus
        };
    },

    isOrderInStatus: function(statusName, order, statuses) {
        var statusList = _.isArray(statuses) ? statuses : [statuses];
        return _.include(statusList, order.get(statusName));
    },

    isOrderInPaymentStatus: function(order, statuses) {
        return this.isOrderInStatus('paymentStatus', order, statuses);
    },

    isOrderInDeliveryStatus: function(order, statuses) {
        return this.isOrderInStatus('deliveryStatus', order, statuses);
    },

    isOrderInStatuses: function(order, statuses) {
        return this.isOrderInPaymentStatus(order, statuses.paymentStatus) && this.isOrderInDeliveryStatus(order, statuses.deliveryStatus);
    },

    isStatus: function(statuses, status) {
        return _.include(_.values(statuses), status);
    },

    isPaymentStatus: function(status) {
        return this.isStatus(Order.PaymentStatus, status);
    },

    isDeliveryStatus: function(status) {
        return this.isStatus(Order.DeliveryStatus, status);
    },

    joinOrderObjects: function(orderObjects, extendData) {
        var result = [];
        _.each(orderObjects, function(orderObject) {
            var founded = _.find(result, function(object) {
                return _.isEqual(orderObject.objectId, object.objectId);
            });

            if (founded) {
                founded.count++;
                founded.totalPrice += orderObject.price;
            } else {
                result.push(_.extend(orderObject, extendData, {
                    count: 1,
                    totalPrice: orderObject.price
                }));
            }
        });

        return result;
    }
};
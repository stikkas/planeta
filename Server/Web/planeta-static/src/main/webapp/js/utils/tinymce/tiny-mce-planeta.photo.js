TinyMcePlaneta.createButtonPlugin("PlanetaPhoto", function (self) {
    /**
     * @type {TinyMcePlaneta.ResizeFrame}
     */
    self.resizeFrame = null;
    /**
     * @type {TinyMcePlaneta.ImageContextMenu}
     */
    self.contextMenu = null;

    var resizeDirections = {
        none: 'se',
        right: 'sw',
        left: 'se'
    };
    var iconClasses = {
        none: 'icon-ww-iright',
        right: 'icon-ww-ileft',
        left: 'icon-ww-inone'
    };
    var imageFloatClasses = {
        none: 'mce-img-no-layout',
        right: 'mce-img-right',
        left: 'mce-img-left'
    };
    var originalTitles = {
        none: 'Отключить обтекание',
        right: 'Включить обтекание слева',
        left: 'Включить обтекание справа'
    };

    self.editor.onPreInit.add(function (ed) {

        self.contextMenu = new TinyMcePlaneta.ImageContextMenu(ed, self.clsName);
        if (TinyMcePlaneta.ResizeFrame) {
            self.resizeFrame = new TinyMcePlaneta.ResizeFrame(ed, "." + self.clsName);
            self.resizeFrame.onResize(function () {
                self.contextMenu.setOffset();
            });
        }
        self.contextMenu.onAppear(function () {
            if (TinyMcePlaneta.ResizeFrame) {
                self.resizeFrame.setImage(self.contextMenu.imageRef);
                self.resizeFrame.setResizeDirection("se");
                if (self.resizeFrame.$imageRef.css("text-align") == "right") {
                    self.resizeFrame.setResizeDirection("sw");
                }
            }

            var $imageRef = $(self.contextMenu.imageRef);

            $.each(imageFloatClasses, function (cssFloat, cls) {
                if ($imageRef.hasClass(cls)) {
                    self.contextMenu.floatButtonsObject.left.currentMode = (cssFloat == 'left');
                    self.contextMenu.floatButtonsObject.right.currentMode = (cssFloat == 'right');
                    self.contextMenu.floatButtonsObject.redraw();
                    if (TinyMcePlaneta.ResizeFrame) {
                        self.resizeFrame.setResizeDirection(resizeDirections[cssFloat]);
                    }
                    return false;
                }
            });
            if (TinyMcePlaneta.ResizeFrame) {
                self.resizeFrame.appear();
            }
        });
        self.contextMenu.onDisappear(function () {
            if (TinyMcePlaneta.ResizeFrame) {
                self.resizeFrame.disappear();
            }
        });

        self.contextMenu.addCloseButton(function () {
        });

        if (TinyMcePlaneta.currentConfiguration.campaign) {
            self.contextMenu.addFloatButton(function () {
                var cssFloat = this.cssFloat;
                $(self.contextMenu.imageRef).removeClass(_.values(imageFloatClasses).join(' ')).addClass(imageFloatClasses[cssFloat]);
                this.right.$el.attr('data-tooltip', originalTitles[cssFloat == 'right' ? 'none' : 'right'])
                        .removeClass('active').addClass(cssFloat == 'right' ? 'active' : '');
                this.left.$el.attr('data-tooltip', originalTitles[cssFloat == 'left' ? 'none' : 'left'])
                        .removeClass('active').addClass(cssFloat == 'left' ? 'active' : '');
//             self.contextMenu.setOffset();
            });

        }


    });

    self.editor.addCommand(self.name, function () {
        var rng = self.editor.selection.getRng();
        var profileId = workspace.appModel.get('profileModel').get('profileId');

        Attach.showPhotoSource(profileId, function (response) {
            var richMediaData = {
                id: response.objectId || response.id,
                owner: response.ownerId || response.owner,
                image: response.imageUrl || response.value
            };
            console.log(response);
            $(self.editor.getBody()).queue(function (next) {
                var $el = $(document.createElement('div'));
                $el.append(TinyMcePlaneta.Photo.parseRichMedia(richMediaData));
                TinyMcePlaneta.insertHtml(self.editor, $el.html());
                setTimeout(function () {
                    next();
                }, 300);
            });
        });


        //</editor-fold>
    });


    self.editor.onLoadContent.add(function (ed) {
        $(ed.getBody()).find(".dummy-class:not(.bb-video-player-cover)").addClass(self.clsName);
        $(ed.getBody()).find('img').removeAttr('style');
    });
});


TinyMcePlaneta.Photo = new TinyMcePlaneta.RichMedia({clsName: 'mceplanetaphoto'});
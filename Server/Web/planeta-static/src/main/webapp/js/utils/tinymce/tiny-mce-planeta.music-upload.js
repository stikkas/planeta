/*globals Attach, TinyMcePlaneta, StringUtils, UploadController*/
TinyMcePlaneta.createButtonPlugin("PlanetaMusic", function (self) {

    self.avatarUrl = 'images/planeta/track-play.png';

    self.editor.onPreInit.add(function (ed) {

        self.contextMenu = new TinyMcePlaneta.ImageContextMenu(self.editor, self.clsName, 'div');
        self.contextMenu.addCloseButton(function () {
        });

    });
    self.editor.addCommand(self.name, TinyMcePlaneta.Audio.insertAudio(self, function (callback) {
        // save cursor before creating temp html element
        var rng = self.editor.selection.getRng();
        var profileId = workspace ? workspace.appModel.get('profileModel').get('profileId') : '';
        UploadController.exec("showUploadAudioTrack", profileId, 0, function (filesUploaded) {
            if (!filesUploaded) {
                return;
            }
            var uploadResult = filesUploaded.at(0).get('uploadResult');
            if (uploadResult) {
                callback(uploadResult);
            }
        });
    }));
});







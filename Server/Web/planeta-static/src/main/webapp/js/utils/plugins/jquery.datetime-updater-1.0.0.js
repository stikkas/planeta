/*plugin for jquery auto update datetime
 */
(function ($) {
    $.datetimeUpdater = function (options) {
        var selector = !!options.selector ? options.selector : ".updatable-time";
        var targetAttr = !!options.targetAttr ? options.targetAttr : "data-updatable-time";
        var interval = !!options.intervalMin ? options.intervalMin * 60000 : 60000;
        var method = !!options.method ? options.method : null;

        if (method === null) {
            throw 'Field "method" is required';
        }

        var update = function() {
            $(selector).each(function(){
                var isLowerCase = $(this).attr('is-lower-case');
                var time =Number($(this).attr(targetAttr));
                if (!!time){
                   $(this).html(method(time, null, isLowerCase));
                }
            })
        };

        if (!!window['__dateTimeUpdater_' + selector]){
            clearInterval(window['__dateTimeUpdater_' + selector]);
        }

        window['__dateTimeUpdater_' + selector] = setInterval(update,interval);

        return {
            dispose: function() {
                clearInterval(window['__dateTimeUpdater_' + selector]);
            },

            update: update
        }
    }
})(jQuery);


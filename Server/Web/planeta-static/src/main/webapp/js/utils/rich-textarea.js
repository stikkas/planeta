/**
 * RichTextArea
 * Usage: $('textarea').richTextArea(options) - creates textarea
 * Another usage: $('textarea').richTextArea('insert', <text>, <replace>, <selectCampaignById>) - inserts <text> at cursor position.
 *      May <replace> selection and <selectCampaignById> inserted text.
 */
(function ($) {

    var RichTextArea = function (element, options) {
        var self = this;
        this.element = $(element);
        this.options = {};
        this.options = _.extend(this.options, this.defaults);
        this.options = _.extend(this.options, options);
        this.template = this.options.template || this.template;

        var tinymci = $(element).closest('.tinymci');
        if (tinymci.size() > 0) {
            this.rich = tinymci;
        } else {
            this.rich = $.tmpl(this.template, {
                title: this.options.title,
                icon: this.options.icon,
                send: this.options.send,
                showClose: this.options.close
            }, {});
            this.element.addClass('ui-planeta text-area').after(this.rich);
            this.rich.find('.textarea').append(this.element);
        }

        var textareas = $(this.rich).find('textarea');
        var textarea = textareas[0];
        this.textarea = textarea;
        var bindToTextArea = function (func) {
            return function (event) {
                event && event.preventDefault();
                event && event.stopImmediatePropagation();
                _.bind(func, element)(event);
            };
        };
        this.rich.find('.photo').bind('click', bindToTextArea(this.options.attachPhoto));
        this.rich.find('.video').bind('click', bindToTextArea(this.options.attachVideo));
        this.rich.find('.link').bind('click', bindToTextArea(this.options.attachLink));
        this.rich.find('.external').bind('click', bindToTextArea(this.options.attachYoutube));
        this.rich.find('.audio').bind('click', bindToTextArea(this.options.attachAudio));
        this.rich.find('.bold').bind('click', bindToTextArea(this.options.bold));
        this.rich.find('.italic').bind('click', bindToTextArea(this.options.italic));
        this.rich.find('.underline').bind('click', bindToTextArea(this.options.underline));
        this.rich.find('.strikethrough').bind('click', bindToTextArea(this.options.strikethrough));
        this.rich.find('.cut').bind('click', bindToTextArea(this.options.cut));
        $(textareas).keypress(function (event) {
            if (event.ctrlKey && ((event.keyCode == 0xA) || (event.keyCode == 0xD))) {
                event.preventDefault();
                event.stopImmediatePropagation();
                $(event.currentTarget).submit();
            }
        });
        $(textareas).each(function () {
            $(this).bind('paste click keyup mouseup', function (event) {
                self.textarea = this;
                self.msieGetSel();
            });
        });
        this.rich.find('.close').bind('click', function (event) {
            event.preventDefault();
            event.stopImmediatePropagation();
            $(textarea).trigger('reset');
            return false;
        });
    };

    RichTextArea.prototype = {
        template: $('#richtextarea-template').template(),
        defaults: {
            attachPhoto: function () {
            },
            attachVideo: function () {
            },
            attachLink: function () {
            },
            attachYoutube: function () {
            },
            attachAudio: function () {
            },
            cut: function () {
                $(this).richTextArea('insert', '[cut/]');
            },
            bold: function () {
                $(this).richTextArea('fold', 'b');
            },
            italic: function () {
                $(this).richTextArea('fold', 'i');
            },
            underline: function () {
                $(this).richTextArea('fold', 'u');
            },
            strikethrough: function () {
                $(this).richTextArea('fold', 's');
            },
            title: 'Написать комментарий',
            icon: '/images/defaults/avatar.png',
            send: true,
            close: true
        },
        msieGetSel: function () {
            var element = this.textarea;
            this.selectionStart = element.selectionStart;
            this.selectionEnd = element.selectionEnd;
        },
        msieSetSel: function () {
            var element = this.textarea;
            this.selectionStart = element.selectionStart;
            this.selectionEnd = element.selectionEnd;
        },
        insert: function (text, replace, select) {
            var area = this.textarea;
            var caretPos = $.browser.msie ? this.selectionStart : area.selectionStart;
            var endPos = $.browser.msie ? this.selectionEnd : area.selectionEnd;
            if (!replace) {
                endPos = caretPos;
            }
            area.value = area.value.substr(0, caretPos) + text + area.value.substr(endPos);
            if (select == true) {
                area.selectionStart = caretPos;
                area.selectionEnd = caretPos + text.length;
                $.browser.msie && this.msieSetSel(area);
            }
            _.delay(function () {
                area.focus();
            }, 100);
            $(area).change();
        },
        fold: function (tag) {
            var tagStart = '[' + tag + ']';
            var tagEnd = '[/' + tag + ']';
            this.wrap(tagStart, tagEnd);
        },
        wrap: function (left, right, replacementIfSelectionIsEmpty) {
            var area = this.textarea;
            var caretPos = $.browser.msie ? this.selectionStart : area.selectionStart;
            var endPos = $.browser.msie ? this.selectionEnd : area.selectionEnd;
            var text = area.value.substr(caretPos, endPos - caretPos);
            if (!text && replacementIfSelectionIsEmpty !== undefined) {
                text = replacementIfSelectionIsEmpty;
            }
            this.insert(left + text + right, true, false);
            area.selectionStart = caretPos + left.length;
            area.selectionEnd = caretPos + left.length + text.length;
            $.browser.msie && this.msieSetSel(area);
            area.focus();
        }
    }

    $.fn.richTextArea = function (options) {
        if (typeof (options) == 'string') {
            var args = arguments;
            return $(this).each(function () {
                var varargs = [];
                Array.prototype.push.apply(varargs, args);
                varargs.shift();
                var richtext = $(this).data('richtext');
                if (richtext !== undefined) {
                    richtext[options].apply(richtext, varargs);
                }
            });
        } else {
            return $(this).map(function () {
                if (!$(this).is('textarea') && !$(this).hasClass('multiarea')) {
                    return null;
                }
                if (!$(this).hasClass('ui-planeta')) {
                    $(this).data('richtext', new RichTextArea(this, options));
                }
                return this;
            });
        }
    }

})(jQuery);

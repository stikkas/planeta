(function() {
    /**
     * Context menu for editor images
     * @param activeEditor
     * @param imgClsName
     * @constructor
     */
    TinyMcePlaneta.ContextMenu = function () {
        /**@private*/ this.buttons = {};
        /**@private*/ this.onDisappearCallbacks = [];
        /**@private*/ this.onAppearCallbacks = [];
        /**@private*/ this.visible = false;
    };

    TinyMcePlaneta.ContextMenu.prototype = {
        addButton: function (buttonName, onClickCallback) {
            this.buttons[buttonName] = {callback: onClickCallback};
        },
        clickButton: function (buttonName) {
            if (this.buttons[buttonName]) {
                return this.buttons[buttonName].callback();
            }
        },
        appear: function () {
            var self = this;
            this.visible = true;
            $.each(this.onAppearCallbacks, function (i, callback) {
                callback.call(self);
            });
        },
        disappear: function () {
            var self = this;
            $.each(this.onDisappearCallbacks, function (i, callback) {
                callback.call(self);
            });
            this.visible = false;
        },
        onDisappear: function (callback) {
            this.onDisappearCallbacks.push(callback);
        },
        onAppear: function (callback) {
            this.onAppearCallbacks.push(callback);
        },
        _disableEvent: function (ed, commandName, b, c, d) {
            if(!commandName.match(/^Justify.*/)) {
                d.terminate = true;
            } else {
                this.disappear();
            }
        }
    };



    /**
     *
     * @param editor target mce editor
     * @param imgClass html-class of all images that will have context menu
     * @constructor
     * @extends ContextMenu
     */
    TinyMcePlaneta.ImageContextMenu = function (editor, imgClass) {
        this.$el = $('<div class="mce-context-panel mceItemNoResize"></div>');
        this.$el.attr("contenteditable", false);
        // call constructor to clear prototype callbacks
        TinyMcePlaneta.ContextMenu.call(this);
        /**@private*/this.imgClass = imgClass;
        /**@private*/this.imageRef = null;

        this.body = editor.getBody();
        // show or hide image menu
        editor.onClick.add(function onImgClick(ed, e) {
            var target = e.target;
            var targetClass = $(target).attr('class');
            // first remove previous context menus
            TinyMcePlaneta.eachMenu(function(){
                this.disappear();
            });
            // next attach menu if target is image of specified class
            if ($(target).is("img") && $(target).hasClass(this.imgClass)) {
                if (!this.visible) {
                    this.imageRef = target;
                    var cursorLocation = this.imageRef.nextSibling || this.imageRef.parentNode.nextSibling;
                    if(cursorLocation) try{
                        var rng = ed.selection.getRng(true);
                        var offset = rng.startOffset;
                        ed.selection.setCursorLocation(cursorLocation, offset);
                    } catch (e) {
                        // ignore opera-specific behaviour
                    }
                    this.appear();
                    // webkit blue selection cancel
                    return false;
                } else {
                    // webkit blue selection cancel
                    ed.selection.collapse();
                }
            }
        }, this);
        // capture deleteByProfileId and backspace with visible menu
        // ToTop is sufficient - keyDown handlers can conflict with noneditable plugin
        editor.onKeyDown.addToTop(function (ed, e) {
            if ((e.keyCode == tinymce.VK.DELETE || e.keyCode == tinymce.VK.BACKSPACE ) && this.visible) {
                this.onClose(e);
            }
            this.disappear();

        }, this);
        editor.onMouseDown.add(function (editor, e) {
            if ($(e.target).hasClass(this.imgClass)) {
                this.disappear();
            }
        }, this);
        editor.onExecCommand.add(function (editor, e) {
            if (this.visible) {
                this.disappear();
            }
        }, this);

        TinyMcePlaneta.addMenu(this);


    };
    TinyMcePlaneta.ImageContextMenu.prototype = new TinyMcePlaneta.ContextMenu();
    TinyMcePlaneta.ImageContextMenu.prototype.appear = function () {
        TinyMcePlaneta.ContextMenu.prototype.appear.apply(this, arguments);
        tinymce.activeEditor.onBeforeExecCommand.add(this._disableEvent, this);
        $(this.imageRef).attr('unselectable', 'on');
        this.$el.appendTo(this.body);
        this.setOffset();
    };

    TinyMcePlaneta.ImageContextMenu.prototype.setOffset = function () {
        this.$el.offset($(this.imageRef).offset());
    };
    /**
     * @override
     */
    TinyMcePlaneta.ImageContextMenu.prototype.disappear = function () {
        this.$el.detach();
        tinymce.activeEditor.onBeforeExecCommand.remove(this._disableEvent);
        TinyMcePlaneta.ContextMenu.prototype.disappear.apply(this, arguments);
    };
    TinyMcePlaneta.ImageContextMenu.prototype.isExists = function () {
        return this.visible;
    };


    /**
     * @override
     */
    TinyMcePlaneta.ImageContextMenu.prototype.addButton = function (name, className, onClickCallback, innerHtml, tooltipText) {
        /**
         * Call callback, adds undo history record and cancels propagation
         * @param e event
         */
        function trueOnClick(e) {
            onClickCallback.call(this, e);
            $('.twipsy').remove();
            tinymce.activeEditor.undoManager.add();
            tinymce.dom.Event.cancel(e);
        }

        TinyMcePlaneta.ContextMenu.prototype.addButton.call(this, name, onClickCallback);
        var node = tinymce.DOM.create('div', {'class': className + ' mce-context-button tooltip', 'data-tooltip': tooltipText}, innerHtml);
        this.buttons[name].$el = $(node);
        this.$el.append(node);
        tinymce.dom.Event.add(node, 'click', trueOnClick, this);
    };

    TinyMcePlaneta.ImageContextMenu.prototype.onClose = function (e) {
        tinymce.dom.Event.cancel(e);
        this.disappear();
        // TODO detach or remove?
        $(this.imageRef).remove();
        $('.twipsy').remove();
        tinymce.activeEditor.undoManager.add();

    };

    TinyMcePlaneta.ImageContextMenu.prototype.addCloseButton = function (onImageDeleteCallback) {
        function onClose(e) {
            onImageDeleteCallback(e);
            this.onClose(e);
        }

        var buttonClass = this.imgClass + "-context-close";
        this.addButton("close", buttonClass, onClose, '<i class="icon-close"></i>', "Удалить");
    };

    TinyMcePlaneta.ImageContextMenu.prototype.addFloatButton = function (onToggleModeCallback) {
        var self = this;
        var buttonClass = this.imgClass + "-context-float";
        this.addButton("float-left", buttonClass + "-left", toggleLeftFloat, '<i class="icon-ww-ileft"></i>', '');
        this.addButton("float-right", buttonClass + "-right", toggleRightFloat, '<i class="icon-ww-iright"></i>', '');

        /**
         * Class for 3-state behaviour (left, right and none), managed by 2 buttons (left and right):
         * one button offs another
         * @param right - one button
         * @param left - other button
         * @constructor
         */
        var Buttons = function(right, left){
            /**
             * Toggles buttons modes on click and call redraw callback
             * @param button - clicked button
             * @param other - other button
             */
            this.click = function(button, other) {
                if(button.currentMode == true) {
                    button.currentMode = false;
                } else {
                    button.currentMode = true;
                    other.currentMode = false
                }
                this.redraw();
            };

            this.redraw = function() {
                this.cssFloat = left.currentMode?'left':(right.currentMode?'right':'none');
                onToggleModeCallback.call(this);
            };

            this.leftClick = function() {
                this.click(left, right);
                self.disappear();
            };
            this.rightClick = function() {
                this.click(right, left);
                self.disappear();
            };

            this.cssFloat = 'none';
            left.currentMode = false;
            right.currentMode = false;
            this.left = left;
            this.right = right;
            this.redraw();
        };
        var buttons = new Buttons(this.buttons['float-right'], this.buttons['float-left']);
        this.floatButtonsObject = buttons;




        function toggleRightFloat(e) {
            buttons.rightClick();
        }
        function toggleLeftFloat(e) {
            buttons.leftClick();
        }

    };


    var menus = [];
    /**
     * @param {TinyMcePlaneta.ImageContextMenu} menu
     */
    TinyMcePlaneta.addMenu = function(menu) {
        menus.push(menu);
    };
    /**
     * @param {Function} callback
     */
    TinyMcePlaneta.eachMenu = function(callback) {
        $.each(menus, function(index, menu){
            callback.call(menu);
        });
    };

})();


//todo-s.kalmykov replace imgClass with abstract selector
TinyMcePlaneta.createModule('ImageContextMenu', function () {
    var editor = this.editor;

    /**
     * Add tooltips for context menu buttons
     * Layout modified by 'mce-twipsy-wrapper' and 'mce-twipsy-arrow' classes
     */
    editor.onInit.add(function (ed) {
        $(editor.getBody()).find('.tooltip').twipsy({
            live: true,
            placement: function iframeTop(tip, element) {
                // ugly hack to set right position to tooltip in iframe
                // based on twipsy.patched.js internals
                // native offset will not work
                this.options.offset = $(editor.getBody()).scrollTop();

                var iframeOffset = $('iframe#tinymce-body_ifr').offset();

                var iframeScroll = $('iframe#tinymce-body_ifr').scrollTop();
                $('.mce-twipsy-wrapper').css(iframeOffset);
                // tooltip elements are created in parent document, not in iframe, so cant move these styles to content.css
                $('.mce-twipsy-wrapper').css('position', 'relative');
                $('.mce-twipsy-arrow').css('bottom', -5);
                // this is required param to twipsy internals

                return "top";
            },
            template: '<div class="twipsy-wrapper mce-twipsy-wrapper"><div class="twipsy-arrow mce-twipsy-arrow"></div><div class="twipsy-inner"></div></div>'
        });
    });
});


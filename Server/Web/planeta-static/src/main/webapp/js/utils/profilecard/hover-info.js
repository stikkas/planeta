var HoverInfoCard = {};
HoverInfoCard.Models = {};
HoverInfoCard.Views = {};

/**
 * Base model for hover
 *
 * If you need you can redefine prefetch method
 * By default it trigger change event
 */
HoverInfoCard.Models.BaseModel = BaseModel.extend({

	hideView: function() {
		this.trigger('hideView');
	}
});

/**
 * Base view for hover card.
 * All views uses for hover info must extend this view
 */
HoverInfoCard.Views.BaseView = BaseView.extend({

	className:'users-card',

	initialize: function(options) {
		BaseView.prototype.initialize.call(this, options);
		this.bindEl = options.bindEl;
		this.model.bind('hideView', this.hideView, this);
	},

	afterRender: function() {
		if (this._isElementInDom(this.bindEl[0])) {
			$('body').append(this.el);
			this.positionView();
		} else {
			this.dispose();
		}
	},

	positionView: function() {
		var bindElPos = $.extend({}, this.bindEl.offset(), {
			width: this.bindEl[0].offsetWidth,
			height: this.bindEl[0].offsetHeight,
			hoverOffset: 10
		});

		var $el = this.$el;
		var hoverWidth = $el.outerWidth(true);
		var hoverHeight = $el.outerHeight(true);

		if (!this.$w) {
			this.$w = $(window);
		}
		var windowTop = this.$w.scrollTop();
		var windowLeft = this.$w.scrollLeft();
		var windowHeight = this.$w.height() + windowTop;
		var windowWidth = this.$w.width() + windowLeft;

		var cssPos = {};
		var left = false, top = false;
		//calculate left position
		if (bindElPos.left + hoverWidth > windowWidth) {
			//position at right side
			cssPos.left = (bindElPos.left + bindElPos.width - hoverWidth) + 'px';
		} else {
			//position at left side
			cssPos.left = bindElPos.left + 'px';
			left = true;
		}
		if (bindElPos.top - hoverHeight - bindElPos.hoverOffset < windowTop) {
			//position below
			cssPos.top = (bindElPos.top + bindElPos.height + bindElPos.hoverOffset) + 'px';
			top = true;
		} else {
			//position above
			cssPos.top = (bindElPos.top - hoverHeight - bindElPos.hoverOffset) + 'px';
		}

		var cssClasses = ['t-str', 'r-str', 'l-str', 'b-str'];
		var cssClass;
		for (var i = 0; i < cssClasses.length; i++) {
			$el.removeClass(cssClasses[i]);
		}
		if (top && left) {
			cssClass = cssClasses[0];
		} else if (top && !left) {
			cssClass = cssClasses[1];
		} else if (!top && left) {
			cssClass = cssClasses[2]
		} else if (!top && !left) {
			cssClass = cssClasses[3];
		}
		$el.addClass(cssClass);
		$el.css(cssPos);

		this.$el.css({
			visibility: 'visible'
		});
	},

	hideView: function() {
		this.$el.css({
			visibility: 'hidden'
		});
	}
});
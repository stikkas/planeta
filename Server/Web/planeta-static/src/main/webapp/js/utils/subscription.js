/*global jQuery, LazyHeader*/
var Subscription = {
    Models: [],
    Views: []
};

Subscription.Models.CommonSubscription = BaseModel.extend({
    url: '/api/profile/check-subscribe.json',

    fetch: function (options) {
        options = _.extend({
            data: {
                profileId: this.get('profileId')
            }
        }, options);
        return BaseModel.prototype.fetch.call(this, options);
    }
});

Subscription.Views.CampaignSubpscription = BaseView.extend({
    template: '#campaign-subscription-template',

    events: {
        'click': 'toggleEventsSubscription'
    },

    toggleEventsSubscription: function (e) {
        e.preventDefault();
        if (workspace.isAuthorized) {
            this.model.toggleSubscription(this.model.get('isSubscribed'));
        } else {
            LazyHeader.showAuthForm('signup');
        }
    }

});

Subscription.Models.CampaignSubscription = Subscription.Models.CommonSubscription.extend({

    defaults: {
        isSubscribed: false
    },

    parse: function (response) {
        if (response && response.success && response.result) {
            this.set('isSubscribed', response.result);
        }
    },

    toggleSubscription: function (isSubscribed, model) {
        var self = this;
        var options = {
            url: '/api/profile/toggle-campaign-subscription.json',
            data: {
                profileId: this.get('profileId'),
                isSubscribed: isSubscribed
            },
            success: function (response) {
                self.set('isSubscribed', response.result);
                if (model) {
                    model.change();
                }
            }
        };
        Backbone.sync('update', this, options);
    }
});
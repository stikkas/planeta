var Wizard = {};
Wizard.Models = {};
Wizard.Views = {};

Wizard.Models.Wizard = BaseModel.extend({
    defaults: {
        steps: [
            {
                /*number:1,
                 view: Wizard.Views.BaseTab, //view tab class implementation
                 model: new BaseModel({...}) //model object*/
            },
            {

            }
        ],
        actions: {
            nextStep: true,
            prevStep: false,
            submit: false
        },
        currentStep: 0
    },

    initialize: function () {
        this.setActions({silent: true});
    },

    setActions: function (options) {
        options = options || {};
        if (this.isFirstStep()) {
            this.set({actions: {
                nextStep: true,
                prevStep: false,
                submit: false
            }}, options);
        } else if (this.isLastStep()) {
            this.set({actions: {
                nextStep: false,
                prevStep: true,
                submit: true
            }}, options);
        } else {
            this.set({actions: {
                nextStep: true,
                prevStep: true,
                submit: false
            }}, options);
        }
    },

    getNextStep: function (step) {
        return _.isUndefined(step) ? this.get('currentStep') + 1 : step;
    },
    getKeyByIndex: function (index) {
        return this.get('steps')[index].key;
    },
    setStep: function (step) {
        var self = this;
        if (this.get('currentStep') == step) {
            this.set({
                currentStep: step,
                currentStepKey: this.get('steps')[step].key
            }, {silent: true});
            return;
        }

        var nextStepFunc = this.get('currentStep') < step ?
            function (s){return self.getNextStep(++s)} :
            function (s){return self.getPreviousStep(--s)};
        while(this.get("steps")[step].skip && this.get("steps")[step].skip()){
            step = nextStepFunc(step);
        }

        this.set({
            currentStep: step,
            currentStepKey: this.get('steps')[step].key
        }, {silent: true});

        this.setActions();
    },
    nextStep: function () {
        if (!this.isLastStep()) {
            this.setStep(this.getNextStep());
        }
        this.setActions();
    },

    /**
     *
     * @param step
     * @returns {number}
     */
    getPreviousStep: function (step) {
        if (_.isUndefined(step)) {
            var defaultPrevStep = this.get('currentStep') - 1;
            return defaultPrevStep > 0 ? defaultPrevStep : 0;
        } else {
            return step >= 0 ? step : 0;
        }
    },
    prevStep: function () {
        if (!this.isFirstStep()) {
            this.setStep(this.getPreviousStep());
        }
        this.setActions();
    },

    isFirstStep: function () {
        return (this.get('currentStep') == 0);
    },

    isLastStep: function (step) {
        step = step || this.get('currentStep');
        return step == this.get('steps').length - 1;
    }
});

/**
 * simple button navigational wizard
 * @type {*}
 */
Wizard.Views.Wizard = BaseView.extend({
    template: '#wizard-template',
    className: 'plate',

    events: {
        "click .next-step-button:not(.disabled)": 'nextStep',
        "click .prev-step-button:not(.disabled)": 'prevStep',
        "click .submit-button:not(.disabled)": 'submit'
    },

    beforeRender: function () {
        this.bindCurrentStepNode();
    },

    invalidated: function () {
    },

    bindCurrentStepNode: function () {
        var steps = this.model.get('steps');
        var currentStep = this.model.get('currentStep') || 0;
        if (this.currentView) {
            this.currentView.dispose();
            this.removeChild(this.currentView);
        }
        this.currentView = new steps[currentStep].view({model: steps[currentStep].model});
        //todo check for other .plate-cont
        this.addChildAtElement('.wizard-tab-content', this.currentView);
    },

    nextStep: function (e) {
        e && e.preventDefault();
        this.frizzNavigation();
        var self = this;
        this.forwardValidate()
            .done(function () {
                self.model.nextStep();
                window.scrollTo(0, 0);
            })
            .fail(_.bind(this.invalidated, this))
            .always(_.bind(this.unfrizzNavigation, this));
    },

    prevStep: function (e) {
        this.frizzNavigation();
        var self = this;
        e && e.preventDefault();
        this.backwardValidate()
            .done(function () {
                self.model.prevStep();
                window.scrollTo(0, 0);
            })
            .fail(_.bind(self.invalidated, self))
            .always(_.bind(this.unfrizzNavigation, this));
    },

    /**
     *
     * @returns $.Deferred() object
     */
    forwardValidate: function () {
        return this.currentView.validate();
    },

    /**
     *
     * @returns $.Deferred() object
     */
    backwardValidate: function () {
        return this.currentView.onRevert ? this.currentView.onRevert() : $.Deferred().resolve();
    },

    submit: function (e) {
        this.frizzNavigation();
        e && e.stopPropagation();

        return this.forwardValidate()
            .always(_.bind(this.unfrizzNavigation, this));
    },

    /*:not(.disabled)":'nextStep',
     ":not(.disabled)":'prevStep',
     "click .next-step-button,.prev-step-button,.submit-button*/
    frizzNavigation: function () {
        this.$('.next-step-button,.prev-step-button,.submit-button').addClass('disabled');
    },
    unfrizzNavigation: function () {
        this.$('.next-step-button,.prev-step-button,.submit-button').removeClass('disabled');
    },

    afterRender: function () {
        this.$('.next-step-button, .submit-button').focus();
    }
});

/**
 * + added support for multi validation for forward validation
 * + views to validate push to <b>this.viewsToValidate</b>
 * extends {@link Wizard.Views.Wizard}
 * @type {*}
 */
Wizard.Views.MultiValidator = Wizard.Views.Wizard.extend({

    bindCurrentStepNode: function () {
        this.viewsToValidate = [];
        Wizard.Views.Wizard.prototype.bindCurrentStepNode.apply(this, arguments);
        this.viewsToValidate.push(this.currentView);
    },

    //todo тоже но в другую сторону со сбросом метки
    /**
     * (!) no validation possible is step is not rendered.
     * @returns {*}
     */
    backwardValidate: function () {
        var validationDfd = [];
        _(this.viewsToValidate).each(function (view) {
            if (typeof view.revertValidate == 'function') {
                validationDfd.push(view.revertValidate());
            }
        });
        return $.when.apply(this, validationDfd);
    },
    /**
     * (!) no validation possible is step is not rendered.
     * @returns {*}
     */
    forwardValidate: function () {
        var validationDfd = [];
        _(this.viewsToValidate).each(function (view) {
            if (typeof view.validate == 'function') {
                validationDfd.push(view.validate());
            }
        });
        return $.when.apply(this, validationDfd);
    }
});


/**
 * + support for change tab on header(.tab-header-item) click
 * * data-key required on element.
 * extends {@link Wizard.Views.MultiValidator}
 * @abstract must be implemented
 * @type {*}
 */
Wizard.Views.HeaderClickWizard = Wizard.Views.MultiValidator.extend({
    events: _.extend({}, Wizard.Views.Wizard.prototype.events, {
        'click .tab-header-item:not(.active):not(.disabled)': 'changeTab'
    }),

    changeTab: function (e) {
        e.preventDefault();
        var self = this;
        this.frizzNavigation();

        var index = this.$('.tab-header-item').index($(e.currentTarget));
        if (index < this.model.get('currentStep')) {
            var $validationResult = this.backwardValidate(index).done(function () {
                self.model.setStep(index);
            });
        } else {
            $validationResult = this.forwardValidate(index).done(function () {
                self.model.setStep(index);
                window.scrollTo(0, 0);
            });
        }

        $validationResult
            .fail(_.bind(this.invalidated, this))
            .always(_.bind(this.unfrizzNavigation, this));

    }
});

/**
 * <ul><li><b>'simple'</b> - simple forward/backward navigation</li>
 * <li><b>'with active headers'</b> - can use headers for navigation</li>
 * <li><b>'with internal memory'</b> - could save tabs data and restore it</li><ul>
 */
Wizard.Types = 0;
Wizard._typeReference = {
    'simple': Wizard.Views.Wizard,
    'with active headers': Wizard.Views.HeaderClickWizard,
    'with internal memory': Wizard.Views.Memorized
};
/**
 * @param anchorPoint css-selector of {@link jQuery} object
 * @param type see {@link Wizard.Types}
 * @param startStepIndex by default 0
 */
Wizard.createAt = function (anchorPoint, type, steps, startStepIndex) {
    startStepIndex = startStepIndex || 0;

    var WizardClazz = null;
    if (typeof type == 'string') {
        WizardClazz = this._typeReference[type];
    } else if (typeof type == 'object') {
        WizardClazz = type;
    }
    if (WizardClazz == null) {
        throw 'No wizard of type "' + type + '".';
    }
    if (steps == null || steps.length == 0) {
        throw 'Wizard steps must be defined';
    }
    return new WizardClazz({
        el: anchorPoint,
        model: new Wizard.Models.Wizard({
            currentStep: startStepIndex,
            steps: steps
        })
    });
};

/**
 * @param type see {@link Wizard.Types}
 * @param startStepIndex by default 0
 * @param steps required [{number,title,model(instance),view(viewClass)}]
 */
Wizard.create = function (type, steps, startStepIndex) {
    startStepIndex = startStepIndex || 0;
    var WizardClazz = null;
    if (typeof type == 'string') {
        WizardClazz = this._typeReference[type];
    } else if (typeof type == 'object') {
        WizardClazz = type;
    }
    if (WizardClazz == null) {
        throw 'No wizard of type "' + type + '".';
    }
    if (steps == null || steps.length == 0) {
        throw 'Wizard steps must be defined';
    }
    return new WizardClazz({
        model: new Wizard.Models.Wizard({
            currentStep: startStepIndex,
            steps: steps
        })
    });
}

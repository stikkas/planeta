// todo-s.kalmykov: editor internals replaces <i> to <em>,
// todo-s.kalmykov: empty <a> becomes <br/>
// todo-s.kalmykov: empty <span> becomes NULL
// todo-s.kalmykov add global tinymce filter to encodeURIComponent input (implement {escape: true} option)
var TinyMceUtils = TinyMceUtils || {};
(function () {

    // hack to fix tinymce bug similar to http://www.tinymce.com/develop/bugtracker_view.php?id=4487
    var nativeSetOuterHTML = tinyMCE.DOM.DOMUtils.prototype.setOuterHTML;
    tinyMCE.DOM.DOMUtils.prototype.setOuterHTML = function (e, h, d) {
        if($(e).find("span#mce_marker").length != 0 && $(h).find("span#mce_marker").length == 0) {
            return nativeSetOuterHTML.call(this, e, h + '<span id="mce_marker"></span>', d);
        } else {
            return nativeSetOuterHTML.call(this, e, h, d);
        }
    };

    /**
     * designations used:
     * html - DOM node with nested structure or real HTML
     * object - dom-like structure node in editor (repeats object nesting)
     * img - dom-like structure node in editor
     * json - pure data of the object
     *
     * DomObject(HTML) transforms into Editor Node using serializer/parser
     * Defining a filter for serializer/parser transforms html into img Node
     * this filter works with imgToObject/objectToImg functions
     * each of these functions works throught JSON
     * user must define JSON <-> object mapping
     * JSON <-> img mapping use data-mce-json image attribute
     * native <IMG> attributes are reserved for editing img-object in editor,
     * don't use it for mapping
     *
     * VERY IMPORTANT to not check one node twice, it leads to
     * node.replace() crash
     *
     * @param editor
     * @param objectClassName
     * @param objectTag
     * @constructor
     */
    TinyMceUtils.Mapping = function(editor, objectClassName, objectTag) {
        this.clsName = objectClassName;
        this.objectTag = objectTag || "div";
        this.editor = editor;
        this.rootAttributes = tinymce.explode('id,name,class,src');
        this.styleAttributes = tinymce.explode('width,height,align,float,hspace,vspace,background,display');
        this.Node = tinymce.html.Node;

        this.attrs = null;
        var self = this;
        this.editor.onPreInit.add(function () {



            // Convert object elements to image placeholder
            self.editor.parser.addNodeFilter(self.objectTag, function (nodes, name) {
                var i = nodes.length;

                while (i--)
                    self.objectToImg(nodes[i]);
            });

            // Convert image placeholders to video elements
            self.editor.serializer.addNodeFilter('img', function (nodes, name, args) {
                var i = nodes.length, node;

                while (i--) {
                    node = nodes[i];
                    if ((node.attr('class') || '').indexOf(self.clsName) !== -1)
                        self.imgToObject(node);
                }
            });
        });
    };

    TinyMceUtils.Mapping.prototype = {

        parsed: false,
        /**
         * @abstract
         * @param json
         */
        jsonToObject: function (json) {
            var err = 'override self.jsonToNode method';
            throw(err);
        },
        /**
         * @abstract
         * @param node
         */
        objectToJSON: function (node) {
            var err = 'override self.nodeToJSON method';
            throw(err);
        },

        imgToJSON: function (img) {
            var data = tinymce.util.JSON.parse(img.attr('data-mce-json')) || {};
            data.src = data.src || img.attr('data-mce-src');

            var style = img.attr('style');
            if (style) {
                $(this.styleAttributes).each(function (i, styleAttr) {
                    var stringS = styleAttr + '\\s*:\\s*(\\S*?)(;|$)';
                    var re = new RegExp(stringS);
                    var match;
                    if (match = style.match(re)) {
                        data[styleAttr] = match[1];
                        style = style.replace(re, '');
                    }
                });
            }
            $(this.rootAttributes).each(function (i, el) {
                if (img.attr(el)) {
                    data[el] = img.attr(el);
                }
            });
            return data;
        },

        jsonToImg: function (json, trueDom) {
            var data = {};
            $.each(json, function (key, value) {
                data[key] = value;
            });
            var opts = {};
            $(this.rootAttributes).each(function (i, el) {
                if (data[el]) {
                    opts[el] = data[el];
                    delete(data[el]);
                }
            });
            var style = '';
            $(this.styleAttributes).each(function (i, sa) {
                if (data[sa]) {
                    style += sa + ':' + data[sa] + ';';
                    delete(data[sa]);
                }
            });
            opts['style'] = style;
            opts['class'] = this.clsName + ' mceItemNoResize ' + (json['class'] ? json['class'] : '');
            opts['unselectable'] = 'on';
            opts['data-mce-json'] = tinymce.util.JSON.serialize(data, "'");

            var img = this.Node.create('img', opts);
            if (trueDom)
                img = this.editor.dom.create('img', opts);

            return img;
        },

        imgToObject: function (img) {
            var data = this.imgToJSON(img);
            $.each(this.attrs, function (key, value) { // check required attrs
                // img node cannot have no required attrs (objectToImg error)
                if (value.required && data[key] == undefined)
                    throw('cant find required attr ' + key);
                if (data[key] == undefined)
                    data[key] = value.empty;
            });
            var node = this.jsonToObject(data);
            img.replace(node);
        },


        objectToImg: function (node) {
//                if(!node.parent)
//                    node.parent = self.editor.getBody();
            if (!this.attrs) {
                var err = 'override self.attrs property';
                alert(err);
                throw(err);
            }
            var data = this.objectToJSON(node);
            if (data == null) {
                return; // not object = no replacement
            }
            var img = this.editor.parser.parse(' ').firstChild; // empty element
            this.parsed = true;
            var self = this;

            $.each(this.attrs, function (key, value) {
                // img node cannot have no required attrs (objectToImg error)
                if (value.required && data[key] == undefined)
                    self.parsed = false;
                if (data[key] == undefined)
                    data[key] = value.empty;
            });
            if (this.parsed) {
                img = this.jsonToImg(data);
            }
//                node.parent.appendNode(img)
            node.replace(img);
        },

        imgToHtml: function (img, force_absolute) {
            return this.editor.serializer.serialize(img,
            {forced_root_block: '', force_absolute: force_absolute});
        },

        domToHtml: function (domNode) {
            return this.editor.serializer.serialize(domNode,
            {forced_root_block: '', force_absolute: true});
        },

        htmlToImg: function (html) {
            var fragment = this.editor.parser.parse(html);
            return fragment.getAll('img')[0];
        },

        jsonToHtml: function (data, force_absolute) {
            var img = this.jsonToImg(data, true);
            return this.imgToHtml(img, force_absolute);
        },

        htmlToJSON: function (html) {
            var img = self.htmlToImg(html);
            return self.imgToJSON(img);
        }
    };
})
();
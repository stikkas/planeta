var ProfileUtils = {
    getUserLink: function (profileId, alias) {
        return alias || profileId;
    },

    getAbsoluteUserLink: function (profileId, alias) {
        return ProfileUtils.getScheme() + workspace.serviceUrls.mainHost + '/' + this.getUserLink(profileId, alias);
    },

    isInvestingProject: function (alias) {
        return _.contains(["INVESTING", "INVESTING_WITHOUT_MODERATION", "INVESTING_ALL_ALLOWED"], alias);
    },

    getCampaignLink: function (webAlias) {
        return "/campaigns/" + webAlias;
    },

    getAbsoluteCampaignLink: function (alias) {
        return ProfileUtils.getScheme() + workspace.serviceUrls.mainHost + this.getCampaignLink(alias);
    },

    getScheme: function () {
        return workspace.serviceUrls.mainHost.indexOf('localhost') >= 0 ? 'http://' : 'https://';
    },
   // с бэка прилетают задизэбленные доставки
    getEnabledDeliveries: function(services) {
        return _.filter(services, function(srv) {
            return srv.enabled;
        });
    }
};
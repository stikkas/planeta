/* this cut-structure is fragile: check such cases after change:
 * 1. if cut don't "cut" all <p> (cut's parent is not body), html can become invalid
 * so server-side htmlValidator will change it - so text layout can change unpredictable
 * 2. try to insert something between "cut" html parts in editor - it can break cut at all.
 * 3. cut can't be affected by style:float or img-resizing
 *
 **/
TinyMcePlaneta.defaultConfiguration.cut_separator = function() {
    return '<div class="mceplanetacut">' +
    '<img class="mceplanetacut" src="' +
    workspace.staticNodesService.getResourceUrl("/images/cut.png") +
    '"></div>';
};
TinyMcePlaneta.defaultConfiguration.cut_separator_selector = 'div.mceplanetacut > img';
// inserts cut if blog body becomes higher than 600(?) px
TinyMcePlaneta.defaultConfiguration.auto_cut_insert = false;


TinyMcePlaneta.createButtonPlugin("PlanetaCut", function (self) {
    self.avatarUrl = workspace.staticNodesService.getResourceUrl("/images/cut.png");

    var externalSeparator = self.editor.getParam('cut_separator')();
    self.setAutoCut = !!self.editor.getParam('auto_cut_insert');

    self.editor.onPreInit.add(function (ed) {
        self.contextMenu = new TinyMcePlaneta.ImageContextMenu(ed, self.clsName);
        self.contextMenu.addCloseButton(function () {
            self.setAutoCut = false;
            $(self.editor.getBody()).find("div." + self.clsName).detach();

        });
        self.editor.dom.bind(self.editor.dom.getRoot(), 'dragend', function (e) {
            reWrapCut(self.editor);
        });

    });

    (function createMapping() {
        var externalClass = $(externalSeparator).attr("class");
        // its better to take tagName from externalSeparator, but now it must be 'div'
        self.mapping = new TinyMcePlaneta.Mapping(self.editor, self.clsName, 'div');
        self.mapping.attrs = {};
        self.mapping.objectToJSON = function (node) {

            if (!node.attr('class') || !node.attr('class').match("(\\s|^)" + externalClass + "(\\s|$)"))
                return null; // null means node under consideration is not object
            var data = {};
            data.src = self.avatarUrl;
            return data;
        };
        self.mapping.jsonToObject = function (data) {
            var clearNode = (new tinymce.html.DomParser).parse($(externalSeparator).html());
            return clearNode.firstChild;
        };
    })();

    // Register commands
    self.editor.addCommand(self.name, function () {
        self.editor.selection.collapse();
        var existingCuts = $(self.editor.getBody()).find("div." + self.clsName + "> img");
        existingCuts.detach();
        self.cutIsInserting = true; // to avoid deadloop Exec-onNodeChange-Exec-...
        self.editor.execCommand('mceInsertContent', 0, externalSeparator);
//        self.insertHtml(externalSeparator);
        var cutSeparatorElement = $(self.editor.getBody()).find("div." + self.clsName + "> img");
        var afterCutHtml = $(self.editor.getBody()).html().split(cutSeparatorElement.parent().clone().wrap('<div>').parent().html())[1];
        if ($(afterCutHtml).text().match(/^\s*$/)) {
            $(cutSeparatorElement).parent().after("<p>&nbsp;</p>");
        }
        self.cutIsInserting = false;
    });

    self.editor.onClick.add(function (ed, e) {
        var target = e.target;

        if ($(target).is("img") && $(target).hasClass(self.clsName)) {
            ed.selection.select(target);
        }
    });

    function reWrapCut(ed) {
        // suppose we have only one cut in doc
        var innerHtml = $(ed.getBody()).find("div." + self.clsName).html();
        if (innerHtml && $(ed.getBody()).find("div." + self.clsName).children().length > 1) {
            $(ed.getBody()).find("div." + self.clsName).replaceWith(innerHtml);
            $(ed.getBody()).find("img." + self.clsName).wrap('<div class="' + self.clsName + '"/>');
        }
    }


    self.editor.onExecCommand.add(function (ed) {
        reWrapCut(ed);
    });

    self.editor.onNodeChange.add(function (ed, cm, n) {
        reWrapCut(ed);

        // remove cut div if cut image was moved outside from cut div
        $(ed.getBody()).find("div." + self.clsName).filter(function () {
            return $(this).find('img').length == 0;
        }).remove();
        // wrap cut image moved outside from cut div with another cut div
        $(ed.getBody()).find("img." + self.clsName).filter(function () {
            return !$(this).parent().hasClass(self.clsName);
        }).wrap('<div class="' + self.clsName + '"></div>');
        // highlight button
        cm.setActive(self.name, n.nodeName === 'DIV' && ed.dom.hasClass(n, self.clsName));
        // autoCut insert
//        if (ed.getBody().scrollHeight > 600 && $(ed.getBody()).find("div." + self.clsName).length == 0 &&
//        !self.cutIsInserting && self.setAutoCut) {
//            var allPs = $(ed.getBody()).find("p");
//            var nonEmptyPs = allPs.filter(function () {
//                return ($(this).text().match(/\S+/) || $(this).html().match(/<div/));
//            });
//            self.setAutoCut = false;
//            if (nonEmptyPs.length > 1)
//                ed.selection.selectCampaignById(nonEmptyPs[nonEmptyPs.length - 2]);
//            ed.selection.collapse();
//            ed.execCommand(self.name);
//            self.cutIsInserting = false;
//        }
    });
});

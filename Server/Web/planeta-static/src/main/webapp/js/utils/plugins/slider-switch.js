// This plugin is for manipulation of custom sliders
(function ($) {

    $.fn.sliderSwitchIcons = function(callback) {
        var trigger = $(this);
        if (!trigger.length) return false;

        var triggerWidth = trigger.width(),
            control = $('.switch-control', trigger),
            items = $('.switch-item', trigger),
            itemsLength = items.length,
            speed = 500;

        var ctrlDiffArr = [
            parseInt(control.css('marginLeft'), 10),
            parseInt($('[class^="switch-icon-"]:eq(0)', control).css('left'), 10),
            parseInt($('.switch-block', trigger).css('left'), 10),
            parseInt($('.switch-block', trigger).css('marginLeft'), 10)
        ],
        controlDiff = -(ctrlDiffArr[0] + ctrlDiffArr[1] - ctrlDiffArr[2] - ctrlDiffArr[3]);

        items.click(function () {
            if ($(this).hasClass('active')) return false;
            $('.switch-item.active', trigger).removeClass('active');
            $(this).addClass('active');

            var index = $(this).index(),
                pos = $(this).position().left + controlDiff;
                pos = index == 0 ? 0 : index == itemsLength - 1 ? triggerWidth : pos;
                control.animate({left: pos}, speed);

            $('[class^="switch-icon-"].active', control).fadeOut(speed).removeClass('active');
            $('[class^="switch-icon-"]:eq(' + index + ')', control).fadeIn(speed).addClass('active');
            if (callback) {
                callback(index);
            }
        });
    }

})(jQuery);
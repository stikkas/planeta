/*global HoverInfoCard*/
var ProfileInfoHover = {Models: {}, Views: {}};
ProfileInfoHover.Models.Info = HoverInfoCard.Models.BaseModel.extend({

	defaults: {
		useActions: false
	},

	parse:function (response) {
		var json = $.extend({}, response.profile, response);
		json.relationStatus = response.profileRelationStatus;
		return json;
	},

	url:function () {
		return '/api/public/profile-info.json?profileId=' + this.get('profileId');
	},

	initialize:function (options) {
		if (!options && !options.profileId) {
			throw 'Profile id not set';
		}
	},

	prefetch:function () {
		var self = this;
		this.fetch({
			success:function () {
				if (self.get('profileType') === 'USER') {
					self.checkOnlineStatus();
				}
			}
		});
	},

	checkOnlineStatus:function () {
		if (this.get('profileType') !== 'USER') {
			return;
		}
		var profileId = this.get('profileId');
		var relationStatus = this.get('relationStatus');
		var self = this;

		if (_.include(relationStatus, 'SELF')) {
			this.set({
				onlineStatus:true
			});
			return;
		}

        workspace.appModel.get('onlineChecker').sendCheckOnlineFullStatusAjax({profileId: profileId}).done(function (data){
			self.set({
				onlineStatus:data.online,
				lastOnline:data.lastOnlineTime
			});
		});
	}
});



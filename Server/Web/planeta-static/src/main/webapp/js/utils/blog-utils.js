var BlogUtils = {
    maxContentLength: 256,

    trimContentHtml: function (html) {
        html = $($('<div></div>').html(html));
        html.find('img').replaceWith("<p>&nbsp;</p>");
        html.find('div[class]').removeAttr('class');
        html.find('div[onclick]').remove();
        return $.trim(html.text()).replace(/\n\s+/ig, '\n');
    }
};


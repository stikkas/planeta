(function (m) {
	m.sign = function (a) {
		return a > 0 ? 1 : a == 0 ? 0 : -1;
	}
})(Math);
/**
 * @param inlineContent - inline scroll to target parent, otherwise scroll become inner element
 * @param reserveHeight :{ value:X }
 * @param precision - auto scroll top/bottom with this precision.
 * @version 0.1.2 scrolled item position forced to be absolute.
 *
 */
(function ($) {
	/**
	 *  removes created wrap and cleans linked events.
	 * @return $(this)
	 */
	$.fn.unscrollpan = function () {

		var clearWrapAndUnbindScrolledItem = function () {
			if (!this.scrollPanelContainer)return;
			//alert('undo start');
			this.scrollPanelContainer.find('.scrollbar-handle-container').remove();
			this.scrollPanelContainer.unbind('.' + this.cid);
			$(this).unwrap();
			if (this.scrollInline)this.scrollPanelContainer.parent().removeClass('scroll');
			else $(this).unwrap();
			this.scrollPanelContainer = null;
			$(window).unbind('.' + this.cid);
			$(document).unbind('.' + this.cid);
			return this;
		};
		return this.each(clearWrapAndUnbindScrolledItem);
	};
	/**
	 * triggers all scrollpan objects to update
	 * @param cid - not required
	 */
	$.updateScrollItems = function (cid) {
		if (cid) $(document).trigger('updateScrollItems.' + cid);
		else $(document).trigger('updateScrollItems');
	};

	/**
	 * $(item).updateScrollItems() updates it's scrollpan
	 * @return $(this)
	 */
	$.fn.updateScrollItems = function () {
		return this.each(function () {
			$.updateScrollItems(this.cid);
			return this;
		});
	};

	$.fn.scrollpan = function (options) {
		var options = options || {};
		var precision = options.precision || 4;
		/**
		 *
		 * @param scrolledContent
		 * @param scrollContainer
		 */
		var setupContentScrollPosition = function (scrollContainer, scrolledContent) {
			var initialScroll = options.scroll || 0;
			var shift = scrolledContent.height() > scrollContainer.height() ? scrolledContent.height() - scrollContainer.height() : 0;
			scrolledContent.css({
				top:-(shift) * initialScroll / 100,
				position:'relative'
			});
		};

		/**
		 *  Changed scrolled content size. Update handler height.
		 */
		var updateHandlerHeight = function (scrollContainer, scrolledContent, handlerLiner, handler) {
			if (scrollContainer.height() == 0) return;

			if (scrollContainer.height() < scrolledContent.height() - precision) {
				handlerLiner.show();
				handler.height(scrollContainer.height() * 100 / scrolledContent.height() + '%');

			} else {
				handler.height('100%');
				handlerLiner.hide();
			}
		};
		/**
		 *  sync handler position with content offset
		 */
		var updateHandlerPosition = function (scrollContainer, scrolledContent, handlerLiner, handler) {
			if (scrollContainer.height() == 0) return;
			if (scrollContainer.height() > scrolledContent.height()) {
				handler.css('top', 0);
			} else {
				var scrollTop = -scrolledContent.position().top * 100 / scrolledContent.height();
				handler.css({top:scrollTop + '%'});
			}
		};
		/**
		 * @param scrollContainer scroll container
		 * @param scrolledContent scrolled content
		 */
		var syncContentPositionWithHandler = function (scrollContainer, scrolledContent, handlerContainer, handler, top) {
			var shift = 0;
			if (handlerContainer.height() == 0) return;
			if ((scrolledContent.height() > scrollContainer.height()) && (top > precision))
				if (top + handler.height() > handlerContainer.height() - precision) {
					shift = scrolledContent.height() - scrollContainer.height();
				}
				else {
					shift = scrollContainer.height() * top / handlerContainer.height();
				}

			scrolledContent.css({
				top:-shift
			});
		};
		/**
		 *
		 * @return jQuery(this)
		 */
		var wrapAndBindScrolledItem = function () {
			var scrolledContent = $(this),
				scrollContainer = scrolledContent.wrap("<div class='scroll-content' />").parent();


			this.cid = "scrl" + (Math.round(Math.random() * 0xFFFF)).toString(16);

			this.scrollPanelContainer = scrollContainer.append("<div class='scrollbar-handle-container'><div class='scrollbar-handle' /></div>")
			this.scrollInline = !!options.inlineContent;
			if (this.scrollInline)this.scrollPanelContainer.parent().addClass("scroll");
			else this.scrollPanelContainer.wrap("<div class='scroll'/>");

			var handleContainer = $('.scrollbar-handle-container', scrollContainer),
				handle = $('.scrollbar-handle', handleContainer);


			//setup base configuration
			updateHandlerHeight(scrollContainer, scrolledContent, handleContainer, handle);
			setupContentScrollPosition(scrollContainer, scrolledContent);
			updateHandlerPosition(scrollContainer, scrolledContent, handleContainer, handle);

			handle.draggable({
				axis:'y',
				containment:'parent',
				drag:function (event, ui) {
					if (ui.position.top + handle.height() > handleContainer.height()) {
						ui.position.top = handleContainer.height() - handle.height();
					}
					if (ui.position.top < 0) {
						ui.position.top = 0;
					}

					syncContentPositionWithHandler(scrollContainer, scrolledContent, handleContainer, handle, ui.position.top);
				},
				stop:function (event, ui) {
					syncContentPositionWithHandler(scrollContainer, scrolledContent, handleContainer, handle, ui.position.top);
				}
			});

			scrollContainer.bind('mousewheel.' + this.cid, function (event, delta) {
				if (event.shiftKey || event.ctrlKey)return;
				var newTop = scrolledContent.position().top + Math.sign(delta) * scrollContainer.height() / 3;

				if (delta < 0) {
					if (scrolledContent.height() > scrollContainer.height()) {
						newTop = Math.abs(newTop) > scrolledContent.height() - scrollContainer.height() ? -(scrolledContent.height() - scrollContainer.height()) : newTop;
						newTop = precision + newTop < 0 ? newTop : 0;
					} else {
						newTop = 0;
					}
				}
				else {
					newTop = newTop > 0 ? 0 : newTop;
				}

				scrolledContent.css({
					top:newTop
				});
				updateHandlerPosition(scrollContainer, scrolledContent, handleContainer, handle);
				return false;
			});
			var self = this;
			scrollContainer.imagesLoaded(function () {
				$(self).updateScrollItems();
			});

			scrollContainer.css({top:0});
			$(document).bind('updateScrollItems.' + this.cid, function () {

				if (options.elastic) {
					scrollContainer.height(Math.min(scrolledContent.outerHeight(), scrollContainer.parent().height()));
				}

				if (options.reserveHeight) {
					scrollContainer.height(Math.min(scrolledContent.outerHeight(), $('body').height() - options.reserveHeight.value));
				}
				updateHandlerHeight(scrollContainer, scrolledContent, handleContainer, handle);
				updateHandlerPosition(scrollContainer, scrolledContent, handleContainer, handle);
				syncContentPositionWithHandler(scrollContainer, scrolledContent, handleContainer, handle, handle.position().top);
			});
			$(window).bind('resize.' + this.cid, function () {
				$(self).updateScrollItems();
			});

			//$.updateScrollItems();
			return this;
		};
		return this.each(wrapAndBindScrolledItem);
	};

	$.extend($.fn.scrollpan, {
		version:"0.1.2"
	});

})(jQuery);
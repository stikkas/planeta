/*globals BaseRichView,console,ModalConfirmView,TemplateManager */
/**
 * Base view and model for different modal windows and forms
 *
 */
var Modal = {
    loaderTemplate: '#dialog-loader-template',
    serviceUnavailableTemplate: '#service-unavailable-template',
    // firefox hack
    currentScrollTop: 0,
    showDialogByViewClass: function (viewClass, model, errorCallBack, ignoreErrors, underlyingModal, successCallback, isAlreadyFetched) {
        var self = this;
        var body = $("html, body");
        // firefox hack
        var currentScrollTop = body.scrollTop();
        this.currentScrollTop = currentScrollTop;
        Modal.showLoader();
        function renderFetched(errorState, currentScrollTop) {
            var view = new viewClass({
                underlyingModal: underlyingModal,
                model: model
            });
            view.onRenderError = function (ex) {
                viewClass.prototype.onRenderError(ex);
                errorState(ex);
            };

            view.render();
            if (currentScrollTop) {
                body.scrollTop(currentScrollTop);
            }
        }
        if (isAlreadyFetched) {
            var errorOnRender = function () {
                workspace.appView.showErrorMessage('Ошибка отрисовки');
                if (errorCallBack && _.isFunction(errorCallBack)) {
                    try {
                        errorCallBack.call(self);
                    } catch (e) {
                        console.log(e);
                    }
                }
                Modal.closeModal();
            };
            renderFetched(errorOnRender, currentScrollTop);
        } else {
            try {
                model.prefetch({
                    success: function (modelCame, res) {
                        var errorState = function () {
                            if (res && res.errorMessage) {
                                workspace.appView.showErrorMessage(res.errorMessage);
                            } else {
                                workspace.appView.showErrorMessage('Объект не найден');
                            }
                            if (errorCallBack && _.isFunction(errorCallBack)) {
                                try {
                                    errorCallBack.call(self, res);
                                } catch (e) {
                                    console.log(e);
                                }
                            }
                            Modal.closeModal();
                        };

                        if ((!res || res.errorMessage) && !ignoreErrors) {
                            errorState();
                            return;
                        }
                        if (successCallback && _.isFunction(successCallback)) {
                            successCallback();
                        }
                        renderFetched(errorState, currentScrollTop);
                    },
                    error: function (res) {
                        if (_.isFunction(errorCallBack)) {
                            try {
                                errorCallBack.call(self, res);
                            } catch (ex) {
                                console.log(ex);
                            }
                        }
                        Modal.closeModal();
                    }
                });
            } catch (ex) {
                Modal.closeModal();
                workspace.appView.showErrorMessage("Нет метода prefetch");
                console.log(model);
            }
        }
    },
    /**
     * if you don't need to fetch model use view.render() ;
     * @deprecated use showDialogByViewClass
     * @param view
     * @param errorCallBack
     */
    showDialog: function (view, errorCallBack) {
        var model = view.model;
        var self = this;
        var dfd = $.Deferred();

        if (!model) {
            view.render();
            return;
        }

        var body = $("html, body");
        // firefox hack
        var currentScrollTop = body.scrollTop();
        this.currentScrollTop = currentScrollTop;
        Modal.showLoader();

        model.prefetch({
            success: function () {
                view.render().renderDfd.done(function () {
                    dfd.resolve();
                });
                if (currentScrollTop) {
                    body.scrollTop(currentScrollTop);
                }
            },
            error: function (res) {
                if (_.isFunction(errorCallBack)) {
                    try {
                        errorCallBack.call(self, res);
                    } catch (ex) {

                    }
                }
                Modal.closeModal();
            }
        });
        return dfd.promise();
    },
    showConfirmWithTextArea: function(message, titleText, placeholderText, callback, okButtonText, cancelButtonText) {
        this.showConfirmEx({
            text: message,
            title: titleText,
            callback: callback,
            okButtonText: okButtonText,
            cancelButtonText: cancelButtonText,
            placeholderText: placeholderText,
            hasTextArea: true
        });
    },
    showConfirm: function (message, titleText, callback, okButtonText, cancelButtonText) {
        return this.showConfirmEx({
            text: message,
            title: titleText,
            callback: callback,
            okButtonText: okButtonText,
            cancelButtonText: cancelButtonText
        });
    },
    showConfirmEx: function (options) {
        options = options || {};
        var body = $("html, body");
        // firefox hack
        var currentScrollTop = body.scrollTop();

        var callback = options.callback || {};
        if (options.success) {
            callback.success = options.success;
        }
        if (options.cancel) {
            callback.cancel = options.cancel;
        }

        this.currentScrollTop = currentScrollTop;

        var L10n = {
            _dictionary:{
                "ru":{
                    "confirmation":"Подтверждение действия"
                },
                "en":{
                    "confirmation":"Confirmation"
                }
            }
        };

        var translate = function (word, lang) {
            if (!lang)
                throw "language is empty.";
            return L10n._dictionary[lang][word] || word;
        };

        var lang = window.workspace && (workspace.currentLanguage === "en") ? "en" : "ru";

        options.title = options.title || translate('confirmation', lang);
        var view = new ModalConfirmView({
            model: new BaseModel(options),
            callback: callback
        });
        if (currentScrollTop) {
            body.scrollTop(currentScrollTop);
        }
        view.render();
        return view;
    },
    showAlert: function (message, titleText, callback) {
        var body = $("html, body");
        // firefox hack
        var currentScrollTop = body.scrollTop();
        this.currentScrollTop = currentScrollTop;

        var L10n = {
            _dictionary:{
                "ru":{
                    "attention":"Внимание"
                },
                "en":{
                    "attention":"Attention"
                }
            }
        };

        var translate = function (word, lang) {
            if (!lang)
                throw "language is empty.";
            return L10n._dictionary[lang][word] || word;
        };

        var lang = window.workspace && (workspace.currentLanguage === "en") ? "en" : "ru";

        var title = titleText || translate('attention', lang);
        var view = new ModalConfirmView({
            model: new BaseModel({
                text: message,
                title: title,
                hasCancelButton: false
            }),
            callback: callback || {
                success: function () {
                }
            }
        });
        if (currentScrollTop) {
            body.scrollTop(currentScrollTop);
        }
        view.render();
    },
    showModal: function (modalEl, faderId, withoutTheaterMode) {
        faderId = faderId || 'modal-backdrop fade in';
        if (this.modalContainer) {
            if (_(this.modalContainer.children).include(modalEl)) {
                // This element is already shown
                // So we need not append it again
                return;
            }

            this.closeModal();
        }

        var $body = $('body');

        if (!$body.data('modalMargin')) {
            $body.data('modalMargin', true);
            var H = $('html');
            var w1 = $(window).width();
            H.addClass('page-overflow-test');
            var w2 = $(window).width();
            H.removeClass('page-overflow-test');
            $("<style type='text/css'>.theaterMode body{padding-right:" + (w2 - w1) + "px;}</style>").appendTo("head");
        }

        if (!withoutTheaterMode) {
            //TODO: this class is scrolling top
            $('html').addClass('modal-open theaterMode');
        }
        if ($('body>.modal-backdrop').length === 0) {
            $('<div class="' + faderId + '"></div>').prependTo("body");
        }
        var modalContainer = $('<div id="modal-dialog" class="modal-dialog"></div>');
        if (modalEl) {
            modalContainer.append(modalEl);
        }

        modalContainer.prependTo("body");
        this.modalContainer = modalContainer.get(0);
    },



    showLoader: function () {
        $.when(TemplateManager.tmpl(Modal.loaderTemplate, {})).done(function (loader) {
            Modal.showModal(loader, 'modal-backdrop fade in', true);
        });
    },
    showServiceUnavailableStub: function (status) {
        $.when(TemplateManager.tmpl(Modal.serviceUnavailableTemplate, {status: status})).done(function (loader) {
            Modal.showModal(loader, 'modal-backdrop fade in', true);
        });
    },
    showTransparentOverlay: function () {
        Modal.showModal(null, 'modal-backdrop fade', true);
    },
    closeModal: function () {
        if ($('body.js-overlapped-modal-view').length === 0) {
            $('html').removeClass('modal-open theaterMode');

            //ugly hack
            if ($('.js-charity-popup-opened').length === 0) {
                $('.modal-backdrop').remove();
            }
            // firefox hack
            if (this.currentScrollTop) {
                $("html, body").scrollTop(this.currentScrollTop);
            }
        }

        $('#modal-dialog').remove();

    },
    closeAllModal: function () {
        _.each(Modal.overlappedViews, function (view) {
            if (!view.noAutomaticDispose) {
                view.dispose();
            }
        });
        this.closeModal();
    },
    startLongOperation: function (timeout) {
        // performance issues
        // Modal.showTransparentOverlay();
        if (this._longOperationTimeoutId) {
            clearTimeout(this._longOperationTimeoutId);
        }
        this._longOperationTimeoutId = setTimeout(function () {
            Modal.showLoader();
        }, timeout || 1500);
    },
    finishLongOperation: function () {
        clearTimeout(this._longOperationTimeoutId);
        this._longOperationTimeoutId = null;
        Modal.closeModal();
    }
};

Modal.View = BaseRichView.extend({
    tagName: 'div',
    className: 'modal',
    clickOff: false,
    events: {
        'click a.close': 'cancel',
        'click .cancel': 'cancel',
        'click button[type=reset]': 'cancel',
        'click button[type=submit]': 'save'
    },
    initialize: function () {
        BaseRichView.prototype.initialize.apply(this, arguments);
        if (this.urlAnchor) {
            workspace.addAnchor(_.isFunction(this.urlAnchor) ? this.urlAnchor() : this.urlAnchor);
        }
    },
    unbindKeyClose: function () {
        $('body').unbind('keydown.delegateEvents' + this.cid);
    },
    bindKeyClose: function () {
        this.unbindKeyClose();
        $('body').bind('keydown.delegateEvents' + this.cid, _.bind(this.checkCommandKeys, this));
    },
    beforeRender: function () {
        Modal.showModal(this.el);
        this.bindKeyClose();
    },
    addClickOff: function () {
        var self = this;
        var $modal = $('.modal-dialog');
        $modal.on('click', function (e) {
            if (e.target == $modal.get(0)) {
                self.dispose();
            }
        });
    },
    removeClickOff: function () {

    },
    afterRender: function () {
        this.makeFocused();
        this.makeScrollable();
        this.delegateEventsRecursive();
        this.renderMedia();
        if (this.clickOff) {
            this.addClickOff();
        }
        BaseRichView.prototype.afterRender.apply(this, arguments);
    },
    makeFocused: function () {
        var $el = this.$('.btn-primary');
        if ($el.length === 0) {
            $el = this.$("a.close");
        }
        $el.focus();
    },
    makeScrollable: function () {
        var self = this;

        function setScrollable() {
            if (self.$el.height() > $(window).height()) {
                self.$el.css({
                    width: self.$el.outerWidth()
                });
            }
        }

        $(window).bind('resize.delegateEvents' + this.cid, setScrollable);
        setScrollable();
    },
    dispose: function () {
        if (this.isRouteChangingModal()) {
            this.redirectToUnderlyingPage();
        }
        BaseRichView.prototype.dispose.apply(this, arguments);
        workspace.appView.removeTooltips();
        workspace.appModel.set('currentViewProfileModel', null);
        Modal.closeModal();
        var $body = $('body');
        $body.unbind('.delegateEvents' + this.cid);
        $(window).unbind('.delegateEvents' + this.cid);
        $body.unbind('keypress');
        this.unbindKeyClose();

        if (this.options.underlyingModal) {
            Modal.showDialog(this.options.underlyingModal);
        }
        if (this.clickOff) {
            this.removeClickOff();
        }
        if (this.urlAnchor) {
            workspace.removeAnchor();
        }
    },
    cancel: function (e) {
        if (e) {
            e.preventDefault();
        }
        this.dispose();
    },
    checkCommandKeys: function (event) {
        if (event.keyCode === 27) { // escape
            this.cancel(event);
        }
    },
    save: function (e) {
        //not_overridable
        e.preventDefault();
        alert('implement this');
    },
    createScroll: function (containerEl) {
        if (this.scrollContent) {
            return;
        }
        if (!this.scrollContent && $('.scrollbar-pane', this.el).length > 0) {
            this.scrollContent = $('.modal-scroll-content', this.el);
            return;
        }

        this.scrollContent = $('.modal-scroll-content', this.el);
        if (this.scrollContent.length > 0) {
            var height = $(containerEl || 'ul', this.scrollContent).height();
            if ($(this.scrollContent).height() === 0) {
                $(this.scrollContent).height(350);
            }
            if (height > this.scrollContent.height()) {
                this.scrollContent.scrollbar({
                    arrows: false
                });
            }
        } else {
            this.scrollContent = null;
        }
    },
    /**
    /**
     * to determine if modal view was called onload of page with only header
     * like planeta.ru/video/1122131/asdfasd-asdfasdf
     * @return {Boolean}
     */
    isRouteChangingModal: function () {
        var pathRegExp = /^\/(video|photo)/i;
        // todo-s.kalmykov: add changing-route-key to view(model?) and set it with navigation-change creations
        // todo-s.kalmykov: or adopt var navigationStateRoutePart = workspace.navigationState.get('id') + '/' + workspace.navigationState.get('section');
        return !!(window.location.pathname.match(pathRegExp));
    },
    /**
     * UGLY-UGLY hack to avoid self-reference pages after calling modal pages (video|photo)
     * and changes location after modal close (if modal window was changed browser url)
     * @see BaseRouter.onClickNavigate()
     * after modal close:
     * this method does history.back() if modal is called from other project page
     * or redirects to main page if modal is called from blank page using short-url
     */
    redirectToUnderlyingPage: function () {
        if (!workspace || !workspace.navigationState) {
            return;
        }
        if (workspace.navigationState && workspace.navigationState.isNew()) {
            var link = '/';
            var photo = this.model.get('photo');
            if (photo) {
                link = '/' + photo.ownerWebAlias;
            } else if (this.model.get('videoId')) { // video
                link = '/' + this.model.get('profileId');
            }
            if (workspace.isRouterEnabled()) {
                workspace.navigate(link, true);
            } else {
                document.location.pathname = link;
            }
        } else {
            window.history.back();
        }
    }
});

Modal.View.show = function (options) {
    var ModalViewType = Modal.View.extend({
        template: options.template,
        urlAnchor: options.urlAnchor
    });
    var view = new ModalViewType({
        model: options.model
    });
    view.render();
};


Modal.overlappedViews = [];
Modal.OverlappedView = Modal.View.extend({
    hideWhenOverlapped: false,
    initialize: function () {
        Modal.View.prototype.initialize.apply(this, arguments);
        var last = _.last(Modal.overlappedViews);
        if (last) {
            last.unbindKeyClose();
        }
        if (last && last.hideWhenOverlapped) {
            last.$el.hide();
        }
        Modal.overlappedViews.push(this);
        console.log("init " + this.cid);
        this.zIndex = 1040 + Modal.overlappedViews.length;
        $('body').addClass('js-overlapped-modal-view');
    },
    beforeRender: function () {
        if (!this.$modal) {
            Modal.showModal(this.el);
            this.$modal = $('#modal-dialog');
            console.log("showModal " + this.cid + " z-index " + this.zIndex);
            this.$modal.attr('id', "").addClass("js-modal-overlapped-view").css('z-index', this.zIndex);
        }

        this.bindKeyClose();
    },
    dispose: function () {
        var curr = _.last(Modal.overlappedViews);
        if (!curr || curr.cid !== this.cid) {
            console.error("overlappedViews stack corrupted");
        } else {
            Modal.overlappedViews.pop();
        }
        if (Modal.overlappedViews.length === 0) {
            $('body').removeClass('js-overlapped-modal-view');
        } else {
            var last = _.last(Modal.overlappedViews);
            if (last) {
                last.bindKeyClose();
            }

            if (last && last.hideWhenOverlapped) {
                last.$el.show();
            }
        }
        this.$modal.remove();
        Modal.View.prototype.dispose.apply(this, arguments);
    }
});

var ModalConfirmView = Modal.OverlappedView.extend({
    className: 'modal alert-modal',
    template: '#modal-confirmation',
    construct: function (options) {
        Modal.OverlappedView.prototype.construct.apply(this, arguments);
        options = options || {};
        Modal.View.prototype.construct.call(this, options);
        if (options.callback) {
            var self = this;
            if (options.callback.success) {
                this.save = function (e) {
                    if (e) {
                        e.preventDefault();
                    }
                    self.dispose();
                    if (this.$('#modalReply')) {
                        options.callback.success(this.$('#modalReply').val());
                    } else {
                        options.callback.success();
                    }
                };
                $(document).bind("keydown.modalForm", function (e) {
                    if (e.which === 13 || e.which === 10) {
                        e.preventDefault();
                        self.save();
                        $(document).unbind('keydown.modalForm');
                    }
                });
            }
            if (options.callback.cancel) {
                this.cancel = _.compose(function () {
                    self.dispose();
                }, options.callback.cancel);
            }
        }
    },
    beforeRender: function () {
        Modal.OverlappedView.prototype.beforeRender.apply(this, arguments);
        if (this.model.get('hasCancelButton') !== false) {
            this.model.set({hasCancelButton: true}, {silent: true});
        }
    },
    dispose: function () {
        Modal.OverlappedView.prototype.dispose.call(this);
        $(document).unbind('keydown.modalForm');
    }
});


/**
 * Base model and view for the form view
 *
 * TODO: isValid and showError are in model now.
 * TODO: We should move these functions to template
 */
var Form = {
    highlightErrors: function (fieldErrors, form, errorClass, hideErrorMessage) {
        var find = (form instanceof jQuery) ? _.bind(form.find, form) : $;
        find('.error').removeClass('error');

        find('button[type=submit]').removeAttr('disabled');
        find('button[type=reset]').removeAttr('disabled');

        _.each(fieldErrors, function (errorValue, errorKey) {
            var el = find('#' + errorKey + ',[name="' + errorKey + '"]');

            if (errorClass) {
                var $fieldSetContainer = el.closest('.' + errorClass);

            } else {
                $fieldSetContainer = el.parent().parent();
            }
            $fieldSetContainer.addClass('error');
            el.addClass('error');
            hideErrorMessage = hideErrorMessage || false;
            if (!hideErrorMessage) {
                if (el.length > 1) {
                    el = $(el[0]);
                }
                var errField = $fieldSetContainer.find('.error-message');
                if (errField.length) {
                    errField.html(errorValue)
                } else {
                    el.after(function () {
                        return '<span class="help-inline error-message">' + errorValue + '</span>';
                    });
                }
            }
        });
        return hideErrorMessage;
    }, /**
     *
     * @param response
     * @param form
     * @param hideErrorMessage
     * @param errorClass
     * @returns {boolean}
     */
    isValid: function (response, form, hideErrorMessage, errorClass) {
        $('.error').removeClass('error');
        $('.error-message').remove();

        if (response.success === false) {
            this.highlightErrors(response.fieldErrors || [], form, errorClass, hideErrorMessage);
            return false;
        }
        return true;
    },
    /**
     * Copy of server-side validation of email
     * @param {String} email
     * @returns {Boolean} email is valid
     */
    isValidEmail: function (email) {
        return email.match(
                "^([A-Za-z0-9_\\-\\.])+\\@([A-Za-z0-9_\\-\\.])+\\.([A-Za-z]{2,4})$"
                );
    }
};

Form.BaseView = Modal.View.extend({
    initialize: function (options) {
        options = options || {};
        if (options.onConfirm && _.isFunction(options.onConfirm)) {
            this.onConfirm = options.onConfirm;
        }
        if (options.template) {
            this.template = options.template;
        }
        Modal.View.prototype.initialize.call(this, options);
    },
    serializeData: function () {
        var form = this.$("form");
        var data = form.serializeObject();

        $('input.time-picker', form).each(function () {
            var date = $(this).datetimepicker('getDate');
            data[$(this).attr('name')] = date && date.getTime();

        });
        $('input.date-picker', form).each(function () {
            var date = $(this).datepicker('getDate');
            data[$(this).attr('name')] = date && date.getTime();
        });

        return data;
    },
    disableButtons: function () {
        this.$('button[type=submit]').attr('disabled', 'disabled');
        this.$('button[type=reset]').attr('disabled', 'disabled');
    },
    enableButtons: function () {
        this.$('button[type=submit]').removeAttr('disabled');
        this.$('button[type=reset]').removeAttr('disabled');
    },
    save: function (e) {
        e.preventDefault();
        this.disableButtons();
        try {
            var self = this;
            $.when(this.onConfirm(this.serializeData())).done(function () {
                // self.dispose();
                self.enableButtons();
            }).fail(function () {
                self.enableButtons();
            });
        } catch (ex) {
            if (console && console.log) {
                console.log(ex);
            }
            throw ex;
        }
    },
    onConfirm: function (data) {
        throw new Error("implement Form.BaseView.onConfirm!");
    },
    afterRender: function () {
        Modal.View.prototype.afterRender.call(this);
        this.$('select').dropDownSelect();
        this.$('input[type=checkbox]').checkbox();
        var $timePicker = this.$('input.time-picker');
        if ($timePicker.length) {
            $timePicker.datetimepicker();
            $timePicker.datepicker({
                changeYear: true,
                yearRange: '-112:',
                defaultDate: new Date()
            });
        }
        this.initFormDatePicker();
    },
    initFormDatePicker: function () {
        var model = this.model;
        this.$('input.time-picker').each(function () {
            $(this).datetimepicker('setDate', new Date(model.get($(this).attr('name'))));
        });

        this.$('input.date-picker').each(function () {
            $(this).datepicker('setDate', new Date(model.get($(this).attr('name'))));
        });
    }
});


Form.Model = BaseModel.extend({
    defaults: {
        title: 'Редактировать объект',
        url: 'Post url',
        itemModel: null,
        saveCallback: null
    },
    initialize: function (options) {
        console.log("132")
        this.set(options.itemModel.toJSON());
    },
    isValid: function (response) {
        return Form.isValid(response);
    },
    saveForm: function (data, successCallback) {
        var self = this;
        var itemModel = this.get('itemModel');
        var isNew = itemModel ? itemModel.isNew() : false;
        var options = {
            url: self.get('url'),
            data: data,
            success: function (response) {
                $('.alert').remove();
                if (self.isValid(response)) {
                    if (successCallback) {
                        successCallback(response.result);
                    }
                    setTimeout(function () {
                        self.endEdit(response.result);
                    }, 1000);
                } else {
                    if (response.errorMessage) {
                        workspace.appView.showErrorMessage(response.errorMessage);
                    }
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                workspace.appView.showErrorMessage(textStatus);
            }
        };

        return Backbone.sync(isNew ? 'create' : 'update', itemModel, options);
    },
    endEdit: function (result) {
        var itemModel = this.get('itemModel');
        if (!itemModel) {
            return;
        }
        var saveCallback = this.get('saveCallback');
        itemModel.set(result);
        if (saveCallback) {
            saveCallback(itemModel);
        }
        this.destroy();
    }
});

Form.View = Form.BaseView.extend({
    successMessage: 'Данные сохранены',
    saveCallbackExternal: null,
    construct: function (options) {
        Form.BaseView.prototype.construct.call(this, options);
        if (options.successMessage) {
            this.successMessage = options.successMessage;
        }
        if (options.saveCallback) {
            this.saveCallbackExternal = options.saveCallback;
        }
    },
    save: function (e) {
        //dont_check_override
        e.preventDefault();
        var self = this;
        try {
            this.disableButtons();
            this.model.saveForm(this.serializeData(), function () {
                workspace.appView.showSuccessMessage(self.successMessage);
                if (self.saveCallbackExternal) {
                    self.saveCallbackExternal();
                }
                self.dispose();
            });
        } catch (ex) {
            if (console && console.log) {
                console.log(ex);
            }
            throw ex;
        }
    }
});


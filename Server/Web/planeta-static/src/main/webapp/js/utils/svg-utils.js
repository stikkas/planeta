/**
 * User: a.savanovich
 * Date: 06.02.14
 * Time: 16:10
 */
function svgPlanetaInit(rootElement, urlToCurrentSector, svgUrl, sectors) {

    var Page = {
        'usesvg': false,
        'svnDoc': null,
        'active': null
    };

    function MoveIn(e) {
        e.preventDefault();
        var color = this.getAttribute('fill');
        this.setAttribute('fill_prev', color);
        this.setAttribute('fill', "#ffffff");
    }

    function MoveOut(e) {
        e.preventDefault();
        var color = this.getAttribute('fill_prev');
        this.setAttribute('fill', color);
    }

    function MoveTest(e) {
        e.preventDefault();
        console.log("Test " + this.id);
    }

    function SerctorClick(me) {
        var id = this.id.substr(4);
        var url = urlToCurrentSector + id;
        window.location.href = url;
    }

    function MinitD(doc) {
        var sectors = $(doc).find('path');
        for (i = 0; i < sectors.length; i++) {
            if (isClickableSector(sectors[i].id.substr(4))) {
                sectors[i].addEventListener("mouseenter", MoveIn, false);
                sectors[i].addEventListener("mouseleave", MoveOut, false);
                sectors[i].addEventListener("click", SerctorClick, false);
                sectors[i].style.cursor = 'pointer';
            } else {
                sectors[i].setAttribute('opacity', '0.1');
            }
        }
        Page.sectors = sectors;
        // ColorAll();
    }


    function Minit() {
        Page.svnDoc = rootElement.contentDocument;
        MinitD(Page.svnDoc);
    }



    function MinitNew() {
        $.get(svgUrl, function (resp) {
            console.log(resp);
            var svgTag = $(resp).find('svg');
            console.log(svgTag);
            rootElement.append(svgTag);
            Page.svnDoc = rootElement.find('svg')[0];
            MinitD(Page.svnDoc);
        });
    }

    function isClickableSector(verifiableSectorId) {
        var foundSector = _.find(sectors, function (sector) { return sector.sectorId == verifiableSectorId; });
        if (foundSector) {
            return true;
        }
        return false;
    }





    $(document).ready(function () {
        Page.usesvg = (function () {
            return ((document.implementation.hasFeature("http://www.w3.org/TR/SVG11/feature#BasicStructure", "1.1")) ||
                (document.implementation.hasFeature("http://www.w3.org/TR/SVG11/feature#Shape", "1.0")));
        })();

        MinitNew();
        console.log(Page.usesvg);

    });
}

function getSectorColor(index) {
    var colors = [
        'brown',
        'yellow',
        'green',
        'blue',
        'more-blue',
        'pink',
        'violet',
        'gray',
        'red'
    ];

    if (index > (colors.length - 1)) {
        return '';
    }
    return colors[index];
}
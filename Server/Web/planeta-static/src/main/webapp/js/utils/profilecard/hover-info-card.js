/**
 * Plugin for displaying information on hover over link.
 * Displays info card with small delay.
 */
(function ($) {

	var HoverInfo = function (element, options) {
		this.element = $(element);
		this.modelConstructor = options.modelConstructor;
		this.viewConstructor = options.viewConstructor;
		this.getElementData = options.getElementData;
        this.model = options.model;
	};

	HoverInfo.hideDelay = 250;
	HoverInfo.showDelay = 500;

	HoverInfo.prototype = {

		isElementInDom: function(element) {
			while (element) {
				if (element == document) {
					return true;
				}
				element = element.parentNode;
			}

			return false;
		},

		show: function () {
			try {//removeTryCatch
				if (!this.isElementInDom(this.element[0])) {
					return;
				}

				var data = this.getElementData(this.element);
				if (!data) {
					return;
				}

				//define model and view
				this.model = this.model || new this.modelConstructor(data);
				this.view = this.view || new this.viewConstructor({
					model: this.model,
					bindEl: this.element
				});

				if (!this.model.loaded) {
					this.model.loaded = true;
					this.model.prefetch();
				}
                if (HoverInfo.previous && this !== HoverInfo.previous) {
                    HoverInfo.previous.remove();
                }
                HoverInfo.previous = this;
				this.view.render();

				var self = this;
				this.view.$el.hover(function () {
					self.element.addClass('mouseenter');
				}, function () {
					self.element.removeClass('mouseenter');
					self.hide();
				});
			} catch (ex) {
				console.log(ex);
			}
		},

		hide: function () {
			var self = this;
			_.delay(function () {
				if (self.element.hasClass('mouseenter')) {
					return;
				}
				self.remove();
			}, HoverInfo.hideDelay);
		},

		remove: function () {
			try {
				if (this.view) {
					this.view.dispose();
					this.view = undefined;
				}
			} catch (e) {
				//exception
			}
		}
	};

	/**
	 * In options you pass:
	 * modelConstructor: constructor of your model
	 * viewConstructor:  constructor of your hover view
	 * getElementData: function that extract data from element
	 * @param options
	 */
	$.fn.HoverInfo = function (options) {
		$.fn.HoverInfo.init.call(this, HoverInfo, options, 'hover-info');
		return this;
	};

	$.fn.HoverInfo.init = function (constructor, options, name) {

		function get(el) {
			var info = $.data(el, name);

			if (!info) {
				info = new HoverInfo(el, options);
				$.data(el, name, info);
			}

			return info;
		}

		function enter() {
			var info = get(this);
			if (info == null) {
				return;
			}

			//show element
			info.show();
		}

		function leave(e) {
			e.preventDefault();
			var info = get(this);
			if (info != null) {
				info.hide();
			}
		}

		function remove() {
			var info = get(this);
			if (info != null) {
				info.remove();
			}
		}

        function manageClass(e) {
            var el = $(e.currentTarget);
            if (e.type == 'mouseenter') {
                el.addClass('mouseenter');
            } else if (e.type == 'mouseleave') {
                el.removeClass('mouseenter');
                el.removeClass('hoverdefined');
            }
        }

		function handle(e) {
			var el = $(e.currentTarget);
            /*
			if (!el.is('.mouseenter')) {
				return;
			}
			if (el.is('.hoverdefined')) {
				return;
			}
            */
			el.addClass('hoverdefined');

			enter.call(this);
		}

		var onMouseEnter = function(e) {
			var self = this;
			_.delay(function() {
				handle.call(self, e);
			}, HoverInfo.showDelay);
		};


		this.live('mouseenter', enter)
            .live('mouseleave', leave)
            .live('click', remove)
            .live('hover', manageClass);
	};

})(jQuery);

(function monkeyPatchAutocomplete() {

	// don't really need this, but in case I did, I could store it and chain
	var oldFn = $.ui.autocomplete.prototype._renderItem;

	$.ui.autocomplete.prototype._renderItem = function (ul, item) {
		var re = new RegExp("^" + this.term);
		var t = item.label.replace(re, "<span style='font-weight:bold;color:Blue;'>" +
			this.term +
			"</span>");
		return $("<li></li>")
			.data("item.autocomplete", item)
			.append("<a>" + t + "</a>")
			.appendTo(ul);
	};
});

var RegionAutocompleter = {
	checkArguments: function (options) {
		if (!options || !options.countrySelect || !options.countrySelect.length) {
			return false;
		}

		return true;
	},
	 autocomplete: function (options) {
		if (!options.cutArguments && !RegionAutocompleter.checkArguments(options)) {
			console.log("Autocompleter error arguments!");
			return;
		}

		var dfdReady = $.Deferred();

		function init(countries) {
			var lang = workspace && workspace.currentLanguage ? workspace.currentLanguage : 'ru';
			try {
				var shareId = options.shareId || 0,
					cityListUrl = options.url || "/api/util/cities-list-by-substring.json",
					regionListUrl = options.url || "/api/util/regions-list-by-substring.json",
					countrySelect = options.countrySelect,
					cityNameInput = options.cityNameInput,
					cityIdInput = options.cityIdInput,
					selectedCountryId = options.selectedCountryId,
					regionNameInput = options.regionNameInput,
					regionIdInput = options.regionIdInput;

				_(countries).each(function (country) {
					var item;
					if(lang == 'ru') {
						item = $('<option></option>').val(country.countryId).text(country.countryNameRus);
					} else {
						item = $('<option></option>').val(country.countryId).text(country.countryNameEn);
					}

					if (selectedCountryId && country.countryId == selectedCountryId) {
						item.attr('selected', true);
					}
					$(countrySelect).append(item);
				});

				var autocompleteEvents = function (elementNameInput, elementIdInput, url) {
					$(elementNameInput).autocompleteOld(url, {
						minChars: 0,
						max: 100,
						autoFill: true,
						mustMatch: true,
						extraParams: {
							shareId: shareId,
							countryId: function () {
								return $(countrySelect).val();
							},
							regionId: function () {
								return $(regionIdInput).val() || 0;
							}
						},
						formatItem: function (item) {
							if (lang == 'ru') {
								return item.name;
							} else {
								return item.nameEn;
							}
						},
						parse: function (data) {
							if (!_(data).isArray()) {
								return [];
							}
							return $.map(data, function (row) {
								var dataRow;
								if (lang == 'ru') {
									dataRow = {
										data: row,
										value: row.name,
										result: row.name
									};
								} else {
									dataRow = {
										data: row,
										value: row.nameEn,
										result: row.nameEn
									};
								}
								return dataRow;
							});
						}
					});

					$(elementNameInput).result(function (e, data, format) {
						if(data) {
							var id = data.objectId || data.regionId || 0;
						}
						$(elementIdInput).val(id);
						$(elementIdInput).change();
					});

					// deleteByProfileId elementId if element is not autocompleted
					$(elementNameInput).change(function (e) {
						$(elementIdInput).val(0);
					});

					$(elementNameInput).blur(function (e) {
						$(e.currentTarget).search();
					});

				};

				autocompleteEvents(cityNameInput, cityIdInput, cityListUrl);
				if(regionNameInput) {
					autocompleteEvents(regionNameInput, regionIdInput, regionListUrl);
				}

				$(countrySelect).change(function (e) {
					$(cityNameInput).val(null).change().flushCache();
					if(regionNameInput) {
						$(regionNameInput).val(null).change().flushCache();
					}
				});

				$(regionNameInput).change(function (e) {
					$(cityNameInput).val(null).change().flushCache();
				});
			} catch (ex) {
				console.log('Error in RegionAutocompleter.autocomplete: ' + ex);
			}
		}

		if (!_.isEmpty(options.customCountryList)) {
			init(options.customCountryList);
			dfdReady.resolve();
		} else {
			if (!this.dfdLoadCountryList) {
				this.dfdLoadCountryList = $.ajax("/api/util/country-list.json");
			}
			this.dfdLoadCountryList.done(function (response) {
				if (response && response.success) {
					var firstRegion = response.result[0];
					if (firstRegion.countryId !== 0) {
						var noneRegion = {};
						for (var o in firstRegion)
							noneRegion[o] = firstRegion[o];
						noneRegion.countryId = 0;
						noneRegion.countryNameEn = "- Not selected -";
						noneRegion.countryNameRus = "- Не выбрано -";
						noneRegion.locationId = 0;
						noneRegion.name = "- Не выбрано -";
						noneRegion.parentLocationType = "GLOBAL_REGION";
						response.result.unshift(noneRegion);
					}
					init(response.result);
					dfdReady.resolve();
				} else {
					if (window.debugMode) {
						var errorMsg = 'Ошибка при получении базы стран';
						workspace.appView.showErrorMessage(errorMsg);
						console.error(errorMsg, response);
					}
				}

			});
		}
		return dfdReady.promise();
	}
};
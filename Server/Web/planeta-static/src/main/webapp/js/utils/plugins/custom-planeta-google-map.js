var CustomPlnGoogleMap = {
    init: function(options) {
        var map;
        var phoneNumber = options.phoneNumber || '+7 (495) 181-05-05';
        var styles  = [
            {
                stylers: [
                    {saturation: -100},
                    {visibility: 'simplified'}
                ]
            }
        ];

        var styledMap = new google.maps.StyledMapType(styles, {name: 'Styled Map'});
        var mapCenter = new google.maps.LatLng(55.77874613350528, 37.63163800000007);
        var markerPos = new google.maps.LatLng(55.77799899999999, 37.63163800000007);

        map = new google.maps.Map(document.getElementById('map'), {
            scrollwheel: false,
            scaleControl: false,
            draggable: true,
            disableDoubleClickZoom: false,
            disableDefaultUI: false,
            zoom: 17,
            center: mapCenter
        });

        var infoBubble = new InfoBubble({
            hideCloseButton: true,
            arrowSize: 20,
            borderRadius: 0,
            shadowStyle: 0,
            padding: 0,
            borderWidth: 0,
            disableAutoPan: true
        });

        infoBubble.setContent(
            '<div class="pln-map-bubble">' +
            '<div class="pln-map-bubble_logo"></div>' +
            '<div class="pln-map-bubble_tel">' + phoneNumber + '</div>' +
            '<div class="pln-map-bubble_addr">Проспект Мира, дом 19, строение 3</div>' +
            '<div class="pln-map-bubble_time-lbl">Время работы:</div>' +
            '<div class="pln-map-bubble_time">' +
            '<span class="pln-map-bubble_time-weekday">пн-пт с 11:00 до 20:00</span><br>' +
            '<span class="pln-map-bubble_time-weekday">сб с 11:00 до 18:00</span><br>' +
            '<span class="pln-map-bubble_time-weekday">воскресенье - выходной</span>' +
            '</div>' +
            '</div>'
        );

        var marker = new google.maps.Marker({
            map: map,
            position: markerPos,
            icon: 'data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSI1MCIgaGVpZ2h0PSI2NyIgdmlld0JveD0iMCwgMCwgNTAsIDY3Ij4gICAgPHBhdGggICAgICAgIGQ9Ik0yNSA2N1MwIDM5IDAgMjUuMTNjMC0xMy44MDcgMTEuMTkzLTI1IDI1LTI1czI1IDExLjE5MyAyNSAyNUM1MCAzOSAyNSA2NyAyNSA2N3ptMC01OC42MmExNi43NSAxNi43NSAwIDEgMCAxNi42NjcgMTYuNzVDNDEuNjkgMTUuOTAyIDM0LjIyOCA4LjQwMyAyNSA4LjM4em0wIDI1LjEyYTguMzc0IDguMzc0IDAgMSAxIDguMzMzLTguMzdBOC4zNTMgOC4zNTMgMCAwIDEgMjUgMzMuNXoiICAgICAgICBmaWxsPSIjMzQ5OERCIi8+PC9zdmc+'
        });

        var bubbleOpenListener = map.addListener('tilesloaded', function () {
            if (!infoBubble.isOpen()) {
                infoBubble.open(map, marker);
            }
            google.maps.event.removeListener(bubbleOpenListener);
        });

        google.maps.event.addListener(marker, 'click', function () {
            if (!infoBubble.isOpen()) {
                infoBubble.open(map, marker);
            } else {
                infoBubble.close();
            }
        });

        map.addListener('click', function (e) {
            if (infoBubble.isOpen()) {
                infoBubble.close();
            }
        });

        map.mapTypes.set('map_style', styledMap);
        map.setMapTypeId('map_style');
    }
};

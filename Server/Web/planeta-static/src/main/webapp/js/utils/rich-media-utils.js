/*globals App,console,Audios,moduleLoader*/
/*jslint plusplus:true*/
var RichMediaUtils = {

    SHOW_PLAYER_MIN_WITH: 300,
    TAGS: {AUDIO: 'audio', VIDEO: 'video', PHOTO: 'photo'},
    JQUERY_SELECTOR_PREFIX: 'p\\:',
    trueExternalUrl: {
        vkGroup: 'vk.com/planetaru',
        fbGroup: 'facebook.com/planetaru',
        youtubePage: 'youtube.com/user/planetarutv',
        twitter: 'twitter.com/PlanetaPortal'
    },
    awayUrl: '/util/away.html?to=',
    /**
     * Replaces rich-media tags with content generated with callback functions
     * @param {Object} replacementCallbacksMap - {tagName: function([Object] json}
     * @param {jQuery} jEl
     */
    replaceTagsWithHtml: function (jEl, replacementCallbacksMap) {
        var escapedTagNamespace = RichMediaUtils.JQUERY_SELECTOR_PREFIX;
        _.each(replacementCallbacksMap, function (replacementCallback, tagName) {
            jEl.find(escapedTagNamespace + tagName).each(function () {
                var $richTag = $(this);
                var attributes = RichMediaUtils.parseRichMedia(this);
                var replacementEl = replacementCallback(attributes);
                $richTag.replaceWith(replacementEl);
            });
        });
    },

    /**
     * Replaces content generated on client with rich-media tags using callback functions
     * @param {Object} replacementCallbacksMap - {tagName: function([jQuery] tag, [Object] json)}
     * @param {jQuery} jEl
     */
    replaceHtmlWithTags: function (jEl, replacementCallbacksMap) {

        _.each(replacementCallbacksMap, function (replacementCallback, tagName) {
            jEl.find('*').each(function () {
                var $elementToReplace = $(this);
                var data = replacementCallback($elementToReplace);
                if (!data) {
                    return;
                }
                var replacementEl = RichMediaUtils.createRichMedia(data, tagName);
                $elementToReplace.replaceWith(replacementEl);
            });
        });
    },

    /**
     * Converts rich-media custom tag to json object
     * @param {HTMLElement} el
     * @return {Object}
     */
    parseRichMedia: function (el) {
        var tagName = el.tagName.toLowerCase();

        var hasNamespace = tagName.indexOf('p:') == 0;
        var hasTagName = _(RichMediaUtils.TAGS).find(function (value) {
            return tagName == value;
        });

        if (!hasNamespace && !hasTagName) {
            throw 'Cannot parse rich-media: ' + tagName;
        }
        var attributes = {};
        $.each(el.attributes, function (index, attr) {
            attributes[attr.name] = attr.value;
        });
        return attributes;
    },

    /**
     * replaces custom planeta tags in textHtml with content rendered by specified templates and return result html
     *
     * @param textHtml
     * @param templates
     * @return {*}
     */
    processHtml: function (textHtml, templates) {
        if (!textHtml || !templates) {
            console.log('textHtml or templates are empty');
            return textHtml;
        }
        var root = $('<p>').html(textHtml);
        this.processEl(root, templates);
        return root.html();
    },

    /**
     * replace all child custom tags with specified templates
     *
     * @param el parent element
     * @param templates
     */
    processEl: function (el, templates) {
        if (!el || !templates) {
            console.log('el or templates are empty');
            return;
        }
        var tags = this.extract(el);
        var self = this;
        _.each(tags, function ($tags, tagName) {
            if (!templates[tagName]) {
                console.error('template for tag ' + tagName + ' is not set');
                return;
            }
            var template = $(templates[tagName]);
            if (template.length == 0) {
                console.error('template ' + templates[tagName] + ' is not found');
                return;
            }
            $tags.each(function () {
                try {
                    var formatted = template.tmpl(this);
                    var media = formatted.find('[data-role=' + tagName + ']');
                    if (media.length == 0) {
                        media = formatted;
                    }
                    media.attr('data-json', self.stringifyObject(this)).attr('data-role', tagName);
                    el.find('p\\:' + tagName + '#' + this.id).each(function () {
                        $(this).replaceWith($(formatted).clone());
                    });
                } catch (e) {
                    console.error(e);
                    console.log(this);
                }
            });
        });
    },

    cleanEl: function ($el) {
        _.each(RichMediaUtils.TAGS, function (tagName) {
            $el.find(RichMediaUtils.JQUERY_SELECTOR_PREFIX + tagName).remove();
        });
    },

    cleanHtml: function (html) {
        var $el = $('<p></p>').html(html);
        this.cleanEl($el);
        return $.trim($el.html());
    },

    cleanText: function (html) {
        var $el = $('<p></p>').html(html);
        this.cleanEl($el);
        return $.trim($el.text());
    },

    /**
     * extracts media objects from html text
     *
     * @param html
     * @return object with jQuery media collections
     *         {audio: [], photo: [], video: []}
     */
    extractFromHtml: function (html) {
        return this.extract($('<p></p>').html(html));
    },

    extract: function ($el) {
        var objects = {};
        _.each(RichMediaUtils.TAGS, function (tagName) {
            var index = 0;
            objects[tagName] = $el.find(RichMediaUtils.JQUERY_SELECTOR_PREFIX + tagName).map(function () {
                var attributes = RichMediaUtils.parseRichMedia(this);
                //noinspection JSLint
                attributes.index = index++;
                return attributes;
            });
        });
        return objects;
    },

    bindHandlers: function (el, model, options) {
        options = options || {};
        var self = this;

        var bind = function (element, event, callback) {
            if ($(element).data('bound.' + event)) {
                // callback is already bound to this element, exiting
                return;
            }
            $(element).data('bound.' + event, true);
            $(element).bind(event, callback);
        };

        var bindPhoto = function (element, object, mediaCollection, index) {
            bind(element, 'click.showPhoto', function (e) {
                e.preventDefault();
                e.stopPropagation();
                App.Mixins.Media.showPhotoCollection(object.id, object.owner, mediaCollection, index, element.data('fullscreen'));
            });
        };

        var bindVideo = function (element, object) {
            bind(element, 'click.showVideo', function (e) {
                e.preventDefault();
                e.stopPropagation();
                if (element.width() >= RichMediaUtils.SHOW_PLAYER_MIN_WITH) {
                    App.Mixins.Media.showVideoPlayer(object.owner, object.id, element);
                } else {
                    App.Mixins.Media.showVideo(object.owner, object.id, element);
                }
            });
        };

        var getParentObjectId = function () {
            if (model) {
                if (model.get("feedObject")) {
                    return model.get("feedObject").objectId;
                }
                return model.get("commentId") || model.get("messageId") || model.get("objectId");
            }
            return 0;
        };
        var audioPlayer;


        var showHideSlider = function (options) {
            options = options || {};
            var object = options.object || this;
            var $el = options.$el || this.$el;
            var controller = options.controller || this.controller;
            var playInfo = options.playInfo || this.playInfo;
            var model = options.model || this.model;

            var $tracklistItem = $el.hasClass('track-list-item') ? $el : $el.find('.track-list-item');
            $tracklistItem.toggleClass("listen-now", !!model.get('selected'));

            if (model.get("isNotFound")) {
                $tracklistItem.addClass("not-found");
                return;
            }

            $el.find('.play-track').toggleClass("pause", !!model.get('selected') && model.get('playerState') === PlayerState.PLAY);

            if (model.get('selected')) {
                if (!object.slider) {
                    object.slider = new Audios.Views.TrackSlider({
                        controller: controller,
                        model: playInfo
                    });
                }
                if (!BaseView.prototype._isElementInDom(object.slider.$el)) {
                    $el.find('.js-slider').empty().append(object.slider.$el);
                    object.slider.renderAsync();
                }
            } else {
                if (object.slider) {
                    object.slider.dispose();
                    delete object.slider;
                }
            }
        };

        var showSlider = function (track, audio) {
            if (!BaseView.prototype._isElementInDom(audio.$el)) {
                track.collection.playlist.destroy();
                track.set('selected', false, {silent: true});
            } else {
                if (track.get('selected')) {
                    self.playingElement = audio.$el;
                }
            }
            if (track.get("isNotFound")) {
                trackNotFound(audio.$el);
            } else {
                showHideSlider({
                    object: audio,
                    $el: audio.$el,
                    controller: audioPlayer,
                    playInfo: audioPlayer.playInfo,
                    model: track
                });
            }
        };

        var trackNotFound = function ($el) {
            $el.empty().removeClass().addClass('track-list-item not-found').text("Аудиозапись не найдена");
        };

        var bindAndLoadTracks = function (mediaCollection) {
            var audios = [];
            _.each(mediaCollection, function (audio) {
                audios.push({
                    profileId: audio.owner,
                    trackId: audio.id
                });
            });
            var savedAudios = el.data('audios');
            var isEqual = false;
            var i;
            if (_.isArray(savedAudios) && savedAudios.length === audios.length) {
                isEqual = true;
                for (i = 0; i < audios.length; i++) {
                    if (audios[i].profileId !== savedAudios[i].profileId || audios[i].trackId !== savedAudios[i].trackId) {
                        isEqual = false;
                        break;
                    }
                }
            }

            var tracks = el.data('tracks');
            if (!isEqual || !tracks) {
                tracks = new Audios.Models.SeveralTracks(audios);
                tracks.parentObjectId = getParentObjectId();
                tracks.load().done(function () {
                    _.each(mediaCollection, function (audio, idx) {
                        var track = tracks.models[idx];
                        audio.$el.data("track", track);
                        audio.$el.data("tracks", tracks);
                        track.bind('change', function () {
                            showSlider(track, audio);
                        });
                    });
                });

                el.data('audios', audios);
                el.data('tracks', tracks);

            }
            return tracks;
        };

        var playPauseTrack = _.debounce(function (track, tracks) {
            audioPlayer.playPause(track, tracks);
        }, 50);

        var bindAudio = function (element, object, mediaCollection, index) {
            var activeTrack = audioPlayer.playInfo.get('track');
            if (activeTrack && activeTrack.get("trackId") == mediaCollection[index].id) {
                var playingPlaylist = audioPlayer.get('playingPlaylist');
                var playingTracks = playingPlaylist && playingPlaylist.get('tracks');
                if (playingTracks && playingTracks.parentObjectId == getParentObjectId()) {
                    self.playingElement = element;
                    var tracks = bindAndLoadTracks(mediaCollection, index);
                    audioPlayer.createPlaylist(tracks);
                    tracks.at(index).set({
                        selected: true,
                        playerState: activeTrack.get('playerState')
                    });
                    showSlider(tracks.at(index), mediaCollection[index]);
                }
            }
            bind(element.find('.play-track'), 'click.playTrack', function (e) {
                e.preventDefault();
                e.stopPropagation();
                if (element[0] && self.playingElement && self.playingElement[0] === element[0]) {
                    audioPlayer.playPause(element.data('track'), element.data("tracks"));
                    return;
                }
                self.playingElement = element;
                var tracks = bindAndLoadTracks(mediaCollection);
                tracks.loadDfd.done(function () {
                    var track = tracks.at(index);
                    if (track && track.get('trackId')) {
                        playPauseTrack(track, tracks);
                    } else {
                        trackNotFound($(element[0]));
                    }

                });
            });

        };

        var initAndBindAudio = function (element, object, mediaCollection, index) {
            moduleLoader.loadModule('audio').done(function () {
                audioPlayer = workspace.appModel.audioPlayer;
                bindAudio(element, object, mediaCollection, index);
            });
        };

        _.each(RichMediaUtils.TAGS, function (tagName) {
            var collection = el.find('[data-role=' + tagName + ']');
            var mediaCollection = [];
            collection.each(function () {
                mediaCollection.push(_.extend(self.parseObject($(this).attr('data-json')), {$el: $(this)}));
            });
            collection.each(function (index) {
                var self = $(this);
                var object = mediaCollection[index];
                if (!object) {
                    console.warn('no data for tag');
                    return;
                }
                switch (tagName) {
                    case 'photo':
                        if(!options.dontBindPhoto){
                            bindPhoto(self, object, mediaCollection, index);
                        }
                        break;
                    case 'video':
                        bindVideo(self, object);
                        break;
                    case 'audio':
                        initAndBindAudio(self, object, mediaCollection, index);
                        break;
                    default:
                        console.error('Unknown rich media tag: ' + tagName);
                }
            });
        });
    },


    /**
     * Parses rich-media element from json object
     * @param {Object} attributes
     * @param {String} tagName
     * @return {HTMLElement}
     */
    createRichMedia: function (attributes, tagName) {
        var tagNamespace = 'p:';
        var replacementEl = document.createElement(tagNamespace + tagName);
        _.each(attributes, function (value, key) {
            if (value) {
                replacementEl.setAttribute(key, value);
            }
        });
        return replacementEl;
    },

    stringifyObject: function (obj) {
        return JSON.stringify(obj);
    },

    parseObject: function (jsonString) {
        return JSON.parse(jsonString);
    },

    isUsMainDomain: function (domain) {
        return domain.toLowerCase().indexOf(workspace.serviceUrls.mainHost) != -1;
    },

    isTrueExternalDomain: function (linkFragment) {
        var domainCode;
        for (domainCode in RichMediaUtils.trueExternalUrl) {
            var trueDomain = RichMediaUtils.trueExternalUrl[domainCode];
            if (linkFragment.toLowerCase().indexOf(trueDomain.toLowerCase()) != -1) {
                return true;
            }
        }

        return false;
    },

    wrapExternalUrl: function (url) {
        var tmpAnchor = $('<a></a>');
        tmpAnchor.attr('href', url);
        var mainExternalDomain = tmpAnchor[0].host;

        if (RichMediaUtils.isUsMainDomain(mainExternalDomain)) {
            return url;
        }
        if (RichMediaUtils.isTrueExternalDomain(url)) {
            return url;
        }
        url = RichMediaUtils.awayUrl + encodeURIComponent(url);

        return url;
    }
};

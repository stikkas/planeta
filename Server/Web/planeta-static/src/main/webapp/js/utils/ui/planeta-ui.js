jQuery.fn.extend({

    radioSelect: function () {
        return $(this).each(function () {
            if ($(this).hasClass('ui-planeta')) {
                return;
            }

            var elements = [];
            var select = this;

            $(this).children().each(function () {
                var span = $('<span class="selectCampaignById-spn"></span>');
                $(span).html($(this).html());

                if ($(this).hasClass('tooltip')) {
                    var tooltip = $(this).attr('data-tooltip');
                    $(span).attr('data-tooltip', tooltip).addClass('tooltip');
                }

                if ($(this).attr('selected')) {
                    $(span).addClass('active');
                }

                elements.push({
                    option: this,
                    span: span
                });
            });

            _(elements).each(function (value) {
                $(select).after(value.span);
                $(value.span).click(function (e) {
                    e.preventDefault();
                    if ($(value.span).hasClass('active')) {
                        return;
                    }

                    // FF hack
                    select.selectedIndex = -1;

                    _(elements).each(function (el) {
                        $(el.span).removeClass('active');
                        $(el.option).removeAttr('selected');
                    });
                    $(value.span).addClass('active');
                    $(value.option).attr('selected', true);
                    $(select).change();
                });
            });

            $(this).addClass('ui-planeta').hide();
        });
    },

    dropDownSelect: function (funcName, funcOptions) {

        var functions = {
            reset: function (options) {
                var select = $(this);
                select.next('.pln-selectCampaignById.dropdown').remove();
                select.removeClass('ui-planeta').show();
                select.find('option').removeAttr('selected');
                select.find('option[value=' + options + ']').attr({selected: true});
                functions.defaultFunc.call(this);
            },

            defaultFunc: function (options) {
                if(options && options.redraw){
                    $(this).next('.pln-selectCampaignById.dropdown').remove();
                }else if ($(this).hasClass('ui-planeta')) {
                    return;
                }

                var elements = [];
                var select = this;
                var selectClasses = $(this).attr('class');
                var dropSelect = $('<div></div>').addClass('pln-select dropdown')[0];
                if (funcOptions && funcOptions.addSelectClassesToDiv) $(dropSelect).addClass(selectClasses);
                var dropSelectBtn = $('<div></div>').addClass('select-btn').append($('<i></i>').addClass('icon-select'));
                var dropSelectText = $('<span></span>').addClass('select-cont');
                var dropSelectListUl = $('<ul></ul>').addClass('dropdown-menu');

                $(dropSelect).append(dropSelectBtn);
                $(dropSelect).append(dropSelectText);
                $(dropSelect).append(dropSelectListUl);
                $(dropSelect).attr('tabindex', 0);

                setTimeout(function () {
                    $(dropSelect).addClass('pln-select__inited');
                }, 500);

                $(select).after(dropSelect);

                var makeOption = function (level, heading) {
                    var item = $('<li></li>').addClass('item');
                    var itemText = $('<a></a>').attr('href', 'javascript:void(0)');
                    $(item).append(itemText);
                    $(itemText).text($(this).attr('label') ? $(this).attr('label') : ((heading ? heading + ' > ' : '') + $(this).html()));

                    if (level) {
                        item.addClass('level');
                    }
                    if ($(this).attr('class')) {
                        item.addClass($(this).attr('class'));
                    }

                    if ($(this).attr('selected')) {
                        $(item).addClass('active');
                        $(dropSelectText).text($(item).text());
                    }

                    var option = $(this).is('optgroup') ? null : this;

                    elements.push({
                        option: option,
                        span: item
                    });
                };

                $(this).children().each(function () {
                    makeOption.call(this);
                    if ($(this).is('optgroup')) {
                        var group = this;
                        $(this).children('option').each(function () {
                            makeOption.call(this, true, $(group).attr('label'));
                        });
                    }
                });

                _(elements).each(function (value) {
                    $(dropSelectListUl).append(value.span);

                    if (!value.option)
                        return;


                    $(value.span).click(function (e) {
                        e.preventDefault();
                        if ($(value.span).hasClass('active')) return;

                        // FF hack
                        select.selectedIndex = -1;

                        _(elements).each(function (el) {
                            $(el.span).removeClass('active');
                            $(el.option).removeAttr('selected');
                        });
                        $(value.span).addClass('active');
                        $(value.option).attr('selected', true);
                        $(dropSelectText).text($(value.span).text());
                        $(select).change();
                        $(document).unbind('click', close);
                        $(dropSelect).removeClass('open');
                        $(dropSelect).unbind('keydown');
                        $(dropSelect).addClass('js-span-clicked');
                    });
                });

                var close = function (event, force) {
                    var parentmatch = $(event.target).parents().map(function () {
                        return this == dropSelect ? this : null
                    });
                    if ((event.target == dropSelect || parentmatch.length > 0) && !force)
                        return;
                    $(document).unbind('click', close);
                    $(dropSelect).unbind('keydown');
                    $(dropSelect).removeClass('open');
                }
                // dropdown menu
                if (($(this).attr('disabled') != 'disabled') && ($(this).attr('readonly') != 'readonly')) {
                    $(dropSelect).click(function (event) {
                        if ($(dropSelect).hasClass('open')) {
                        } else {
                            if ($(dropSelect).hasClass('js-span-clicked')) {
                                $(dropSelect).removeClass('js-span-clicked');
                            } else {
                                $(dropSelect).addClass('open');
                                $(document).click(close);
                                $(dropSelect).keydown(function (e) {
                                    e.preventDefault();
                                    var key = e.keyCode || e.which;
                                    var children = $(dropSelect).find('.dropdown-menu').children();
                                    if (key == 38 || key == 40) {
                                        var oneChildHeight;
                                        var activeChildPxPosition = 0;
                                        var scrollContainer = $(dropSelect).find('.dropdown-menu');
                                        var maxContainerHeight = parseInt($(dropSelect).find('.dropdown-menu').css('maxHeight'), 10);
                                        children.each(function (i) {
                                            var child = $(this);
                                            oneChildHeight = child.height();
                                            if (child.hasClass('active')) {
                                                if (key == 40) {
                                                    activeChildPxPosition = (child.height() * (i + 1));
                                                    if (i + 1 <= children.length - 1) {
                                                        child.removeClass('active').removeClass('js-active');
                                                        $(children[i + 1]).addClass('active').addClass('js-active');
                                                        return false;
                                                    }
                                                } else {
                                                    activeChildPxPosition = (child.height() * i - 1);
                                                    if (i - 1 >= 0) {
                                                        child.removeClass('active').removeClass('js-active');
                                                        $(children[i - 1]).addClass('active').addClass('js-active');
                                                        return false;
                                                    }
                                                }
                                            }
                                        });
                                        var lowerBound = scrollContainer.scrollTop() + maxContainerHeight;
                                        var hireBound = scrollContainer.scrollTop();
                                        if (lowerBound > activeChildPxPosition && hireBound >= activeChildPxPosition) {
                                            scrollContainer.scrollTop(activeChildPxPosition - oneChildHeight);
                                        } else if (lowerBound < activeChildPxPosition + oneChildHeight && hireBound < activeChildPxPosition) {
                                            scrollContainer.scrollTop(activeChildPxPosition + oneChildHeight - maxContainerHeight);
                                        }
                                    }
                                    if (key == 13) {
                                        children.each(function (el) {
                                            $(this).removeAttr('selected');
                                        });
                                        _(elements).each(function (value) {
                                            if ($(value.span).hasClass('active')) {
                                                // FF hack
                                                select.selectedIndex = -1;
                                                _(elements).each(function (el) {
                                                    $(el.span).removeClass('active');
                                                    $(el.option).removeAttr('selected');
                                                });
                                                $(value.span).addClass('active');
                                                $(value.option).attr('selected', true);
                                                $(dropSelectText).text($(value.span).text());
                                                $(select).change();
                                                $(document).unbind('click', close);
                                                $(dropSelect).removeClass('open');
                                                $(dropSelect).unbind('keydown');
                                            }
                                        })
                                    }
                                });
                            }
                        }
                    });
                    $(dropSelect).focus(function (event) {
                        if (!$(dropSelect).hasClass('open')) {
                            $(dropSelect).click();
                        }
                    });
                } else {
                    $(dropSelect).addClass('disable');
                }

                $(this).addClass('ui-planeta').hide();
            }
        };

        return $(this).each(function () {
            funcName = funcName || 'defaultFunc';
            if (funcName && functions[funcName]) {
                functions[funcName].call(this, funcOptions)
            }
        });
    },

    radioButton: function (options) {
        this.defaults = {
            wrapperClassName: 'clearfix'
        };

        var defaults = _.defaults(options || {}, this.defaults);

        return $(this).each(function () {
            if ($(this).hasClass('ui-planeta')) {
                return;
            }

            var input = this;

            var div = $('<div></div>').addClass(defaults.wrapperClassName)[0];
            var radiobox = $('<span></span>').addClass('radiobox').attr('group-name', $(this).attr('name'))[0];
            $(div).append(radiobox);
            $(div).append($('<span></span>').addClass('radiobox-label').text($(this).parents('label').text()));

            var label = $(input).parents('label');
            var el = label ? label : $(input);

            el.after(div);

            var onRadioButtonChanged = function (event) {
                if ($(input).is(':checked')) {
                    // Unchecking all radio buttons
                    $(input).closest('form').find('.radiobox').each(function () {
                        if ($(this).attr('group-name') == $(input).attr('name')) {
                            $(this).removeClass('active');
                        }
                    });
                    $(radiobox).addClass('active');
                } else {
                    $(radiobox).removeClass('active');
                }
            };
            $(input).change(onRadioButtonChanged);

            $(div).click(function (event) {
                event.preventDefault();
                $(input).click();
            });
            onRadioButtonChanged();
            $(div).css('width', el.css('width'));

            $(this).addClass('ui-planeta');
            el.hide();
        });
    },

    checkbox: function (options) {
        this.defaults = {
            wrapperClassName: 'clearfix',
            ignoreElementWidth: true,
            copyAttrs: []
        };

        var defaults = _.defaults(options || {}, this.defaults);


        return $(this).each(function () {
            if ($(this).hasClass('ui-planeta')) {
                return;
            }

            var input = this;
            var $input = $(input);
            var wrapperClassName = defaults.wrapperClassName;
            if (options && options.addSelectClassesToDiv) {
                wrapperClassName = $input.attr('class');
            }
            var val = $input.val();
            if (val === "true") {
                $input.attr('checked', val);
            } else if (val === "false") {
                $input.removeAttr('checked');
            }

            var div = $('<div></div>').addClass(wrapperClassName)[0];
            var checkbox = $('<span></span>').addClass('checkbox')[0];
            var $checkbox = $(checkbox);
            var $div = $(div);
            $div.append(checkbox);

            var label = $('<span></span>').addClass('checkbox-label').html($(this).parents('label').html())
            label.find('input').remove();
            $div.append(label);


            label = $input.parents('label');
            var el = label ? label : $input;

            el.after(div);

            var checkboxChange = function (event) {
                $input.val(!!$input.attr('checked'));
                if ($input.attr('checked')) {
                    $checkbox.addClass('active');
                } else {
                    $checkbox.removeClass('active');
                }
                if ($input.attr('disabled')) {
                    $checkbox.addClass('disabled');
                } else {
                    $checkbox.removeClass('disabled');
                }
            };
            $input.change(checkboxChange);

            $div.click(function (event) {
                var $target = $(event.target);
                if ($target.is('a') || $input.attr('disabled')) return;

                event.preventDefault();
                if ($input.attr('checked')) {
                    $input.attr('checked', false);
                } else {
                    $input.attr('checked', true);
                }
                $input.change();
            });
            checkboxChange();
            $.each(defaults.copyAttrs, function (index, value) {
                $div.attr('data-' + value, $input.attr(value));
            });

            if (!defaults.ignoreElementWidth) {
                $div.css('width', el.css('width'));
            }

            $(this).addClass('ui-planeta');
            el.hide();
        });
    },

    dateSelect: function (options) {
        return $(this).each(function () {
            var $this = $(this);
            if ($this.hasClass('ui-planeta')) {
                return;
            }

            var date = +$this.val();

            function getDefaultDate() {
                var customCurrentDate = $this.attr('data-ui-default-value');
                var defaultDate;
                if (customCurrentDate) {
                    defaultDate = new Date(customCurrentDate);
                } else {
                    defaultDate = new Date();
                    defaultDate.setFullYear(defaultDate.getFullYear() - 18);
                }
                return defaultDate;
            }
            var defaultDate = getDefaultDate();
            function onDateChange() {
                var date = $this.datepicker('getDate').getTime();
                $this.attr('data-field-value', date).data('fieldValue', date);
                $this.trigger("date-change");
            }

            var currentYear = new Date().getFullYear();
            var customCurrentDate = $this.attr('data-ui-default-value');
            var minYear = currentYear;
            var maxYear = currentYear;
            if (customCurrentDate) {
                minYear = currentYear - 1;
                maxYear = currentYear + 5;
            } else {
                minYear = currentYear - 100;
                maxYear = currentYear;
            }
            $this.datepicker({
                changeYear: true,
                yearRange: minYear + ':' + maxYear,
                defaultDate: defaultDate,
                onSelect: onDateChange,
                beforeShow: function() {
                    setTimeout(function(){
                        $('#ui-datepicker-div').css({'zIndex': ''})
                    }, 0);
                }
            });

            $this.on('blur', function () {
                var val = $this.val();
                if (!$.trim(val)) {
                    $this.attr('data-field-value', '').data('fieldValue', '');
                } else {
                    try {
                        var newDate = $.datepicker.parseDate($this.datepicker('option', 'dateFormat'), val);
                    } catch (e) {
                        return;
                    }

                    if (!isNaN(newDate.getTime()) && newDate.getFullYear() >= minYear && newDate.getFullYear() <= maxYear) {
                        onDateChange();
                    }
                }
            });

            if (date) {
                $this.datepicker('setDate', new Date(date));
                $this.attr('data-field-value', date).data('fieldValue', date);
            }
        });
    }
});
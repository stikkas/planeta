(function () {


    /**
     * Draggable frame for image to resize with handlers
     * There can be only one resize frame in a moment in editor
     * @param {tinymce.Editor} editor - active blog editor
     * @property {tinymce.Editor} editor - same as argument
     * @property {tinymce.Editor} body - same as argument
     * @constructor
     */
    TinyMcePlaneta.ResizeFrame = function (editor, s) {
        this.editor = editor;
        this.body = editor.getBody();
        /**
         * @public
         * @type {Boolean}
         */
        this.aspectRatio = true;
        this.handleSize = 7;
        this.resizeType = "se";
        this._resizeCallbacks = [];
        this._isVisible = false;
        var self = this;


        TemplateManager.tmpl("#tiny-mce-resize-frame-wrapper", {}).done(function ($tmpl) {
            self.$el = $("<div></div>").html($tmpl);
            self.$hint = self.$el.find("#mce-resize-frame-hint").hide();
            self.$handler = self.$el.find("#mce-resize-frame-handler");
            self.$frame = self.$el.find("#mce-resize-frame-wrapper");

            var mouseUpHandler = function (e) {
                tinymce.dom.Event.remove(self.body, 'mouseup');
                tinymce.dom.Event.remove(self.body, 'mousemove');
                self.$hint.hide();
                tinymce.dom.Event.cancel(e);
                $(document.body).unbind('mouseover.stopresize');
            };
            // resize starts only if frame is visible and user drags image by handler
            tinymce.dom.Event.add(self.$handler[0], 'mousedown', function (e) {
                if (!self._isVisible) {
                    return;
                }
                self.setWrapperSize();
                self.$hint.show();
                tinymce.dom.Event.add(self.body, 'mousemove', self.resize, self);
                tinymce.dom.Event.add(self.body, 'mouseup', mouseUpHandler);
                // stop resize if mouse is not over iframe
                $(document.body).bind('mouseover.stopresize', mouseUpHandler);
                tinymce.dom.Event.cancel(e);
            });
        });

        editor.onExecCommand.add(function () {
            if (self._isVisible) {
                self.disappear();
            }
        });
    };

    TinyMcePlaneta.ResizeFrame.prototype = {
        /**
         * @type {tinymce.Editor}
         * @public
         */
        editor: null,
        /**
         * @private
         * @type {Boolean}
         */
        _isVisible: false,
        /**
         * jquery wrapped all overlay elements
         * @private
         */
        $el: $(),
        /**
         * jquery wrapped size hint panel
         * @private
         */
        $hint: $(),
        /**
         * jquery wrapped resize frame
         * @private
         */
        $frame: $(),
        /**
         * jquery wrapped element to wrap
         * @private
         */
        $imageRef: $(),
        /**
         * callbacks to exec on resize
         * @private
         * @type {Array}
         */
        _resizeCallbacks: null,
        /**
         * Disable editor buttons if frame is visible, except aligns:
         * in this case just hide frame.
         * @param {tinymce.Editor} ed - active editor
         * @param {String} commandName - name of executeCommand command
         * @param b
         * @param c
         * @param d - ugly-hack parameter to prevent all execCommand actions
         * @private
         */
        _disableEvent: function (ed, commandName, b, c, d) {
            if (commandName.match(/^Justify.*/) || commandName.match(/PlanetaFullScreen/i)) {
                this.disappear();
            } else {
                d.terminate = true;
            }
        },
        /**
         * @method
         * Show resize frame for selected image
         */
        appear: function () {
            if (this._isVisible) {
                return;
            }
            this.$el.appendTo(this.body);
            this._isVisible = true;
            this.editor.contentEditable = false;
            this.body.contentEditable = false;
            this.$imageRef.addClass('is-under-resize');
            this.oldSize = {
                width: this.$imageRef.width(),
                height: this.$imageRef.height()
            };
            if (this.$imageRef.offset().top + this.$imageRef.height() > $(this.body).height()) {
                $(this.body).height(this.body.scrollHeight);
//                $("html,body").scrollTop($("#confirmation-contacts").offset().top);
            }

            this.setWrapperSize();
            this.$imageRef.contentEditable = false;
            this.editor.onBeforeExecCommand.add(this._disableEvent, this);
        },

        /**
         * Hide existing resize frame
         */
        disappear: function () {
            if(this.body){
                tinymce.dom.Event.remove(this.body, 'mouseup');
                tinymce.dom.Event.remove(this.body, 'mousemove');
            }
            if (!this._isVisible) {
                return;
            }
            this.$imageRef.removeClass('is-under-resize');
            this.$imageRef.contentEditable = true;
            this.$el.detach();
            var newSize = {
                width: this.$imageRef.width(),
                height: this.$imageRef.height()
            };
            this._isVisible = false;
            this.body.contentEditable = true;
            this.editor.onBeforeExecCommand.remove(this._disableEvent);
            if (newSize.width != this.oldSize.width || newSize.height != this.oldSize.height) {
                this.editor.undoManager.add();
            }
        },

        isVisible: function () {
            return this._isVisible;
        },

        /**
         * Bind some callback to frame resize event
         * @param {Function} callback
         */
        onResize: function (callback) {
            this._resizeCallbacks.push(callback);
        },

        setImage: function (imageEl) {
            this.$imageRef = $(imageEl);
        },

        setResizeDirection: function (cls) {
            this.$handler.removeClass("se sw ne nw").addClass(cls);
            this.resizeType = cls;
        },

        setWrapperSize: function () {
            this.$frame.offset(this.$imageRef.offset());
            this.$frame.width(this.$imageRef.width());
            this.$frame.height(this.$imageRef.height());
            this.$handler.offset(this.$frame.find("." + this.resizeType).offset());
        },

        getWrapperSize: function () {
            return {
                top: this.$frame.offset().top,
                left: this.$frame.offset().left,
                height: this.$frame.height(),
                width: this.$frame.width()
            };
        },

        resize: function (e) {
            var offset = this.getWrapperSize();
            if (e.pageX == null && e.clientX != null) {
                var html = this.editor.dom.doc.documentElement;
                var body = this.body;

                e.pageX = e.clientX + (html.scrollLeft || body && body.scrollLeft || 0);
                e.pageX -= html.clientLeft || 0;

                e.pageY = e.clientY + (html.scrollTop || body && body.scrollTop || 0);
                e.pageY -= html.clientTop || 0;
            }
            if (this.resizeType == 'se') {
                var newHeight = Math.max(e.pageY - offset.top, this.handleSize);
                var newWidth = Math.max(e.pageX - offset.left, this.handleSize);
            } else if (this.resizeType == 'ne') {
                newHeight = Math.max(offset.top - e.pageY + offset.height, this.handleSize);
                newWidth = Math.max(e.pageX - offset.left, this.handleSize);
            } else if (this.resizeType == 'sw') {
                newHeight = Math.max(e.pageY - offset.top, this.handleSize);
                newWidth = Math.max(offset.left - e.pageX + offset.width, this.handleSize);
            } else if (this.resizeType == 'nw') {
                newHeight = Math.max(offset.top - e.pageY + offset.height, this.handleSize);
                newWidth = Math.max(offset.left - e.pageX + offset.width, this.handleSize);
            }
            if (this.aspectRatio) {
                newWidth = Math.floor(newWidth);
                newHeight = Math.floor(newWidth * (this.oldSize.height / this.oldSize.width));
            }
            this.$imageRef.height(newHeight);
            this.$imageRef.attr('height', newHeight);
            this.$imageRef.css('height', newHeight);
            this.$imageRef.width(newWidth);
            this.$imageRef.attr('width', newWidth);
            this.$imageRef.css('width', newWidth);
            this.$hint.html(newWidth + ' &times; ' + newHeight + ' px');

            this.setWrapperSize();
            for (var i = 0; i < this._resizeCallbacks.length; i++) {
                var func = this._resizeCallbacks[i];
                func.call(this);
            }
            tinymce.dom.Event.cancel(e);
        }
    };
})();
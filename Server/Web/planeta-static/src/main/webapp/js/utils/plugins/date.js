/**
 * @author Connane Doile
 */

var L10n = {
    _dictionary: {
        "ru": {
            "propName": "русский"
        },
        "en": {
            "propName": "english"
        }
    }
};

var translate = function (word, lang) {
    if (!lang)
        throw "language is empty.";
    return L10n._dictionary[lang][word] || word;
};

var lang = window.workspace && (workspace.currentLanguage === "en") ? "en" : "ru";

Date.prototype.format = function (format) {
    var returnStr = '';
    var replace = Date.replaceChars;
    for (var i = 0; i < format.length; i++) {
        var curChar = format.charAt(i);
        if (i - 1 >= 0 && format.charAt(i - 1) == "\\") {
            returnStr += curChar;
        } else if (replace[curChar]) {
            returnStr += replace[curChar].call(this);
        }
        else if (curChar != "\\") {
            returnStr += curChar;
        }
    }
    return returnStr;
};

Date.timeEndings = function () {
    return window.workspace && (workspace.currentLanguage === "en") ? {
        days: {nom: 'day', gen: 'day', plu: 'days'},
        hours: {nom: 'hour', gen: 'hour', plu: 'hours'},
        minutes: {nom: 'minute', gen: 'minute', plu: 'minutes'},
        seconds: {nom: 'second', gen: 'second', plu: 'seconds'}
    } : {
        days: {nom: 'день', gen: 'дня', plu: 'дней'},
        hours: {nom: 'час', gen: 'часа', plu: 'часов'},
        minutes: {nom: 'минуту', gen: 'минуты', plu: 'минут'},
        seconds: {nom: 'секунду', gen: 'секунды', plu: 'секунд'}
    };
};

Date.replaceChars = {
    shortMonths: function () {
        return window.workspace && (workspace.currentLanguage === "en")
            ? ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'June', 'July', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec']
            : ['Янв', 'Фев', 'Мар', 'Апр', 'Май', 'Июн', 'Июл', 'Авг', 'Сен', 'Окт', 'Ноя', 'Дек']
    },
    longMonths: function () {
        return window.workspace && (workspace.currentLanguage === "en")
            ? ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']
            : ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь']
    },
    longMonthsPlural: function () {
        return window.workspace && (workspace.currentLanguage === "en")
            ? ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']
            : ['Января', 'Февраля', 'Марта', 'Апреля', 'Мая', 'Июня', 'Июля', 'Августа', 'Сентября', 'Октября', 'Ноября', 'Декабря']
    },
    shortDays: function () {
        return window.workspace && (workspace.currentLanguage === "en")
            ? ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat']
            : ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб']
    },
    longDays: function () {
        return window.workspace && (workspace.currentLanguage === "en")
            ? ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday']
            : ['Воскресенье', 'Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота']
    },

    // Day
    d:function () {
        return (this.getDate() < 10 ? '0' : '') + this.getDate();
    },
    D:function () {
        return Date.replaceChars.shortDays()[this.getDay()];
    },
    j:function () {
        return this.getDate();
    },
    l:function () {
        return Date.replaceChars.longDays()[this.getDay()];
    },
    N:function () {
        return this.getDay() + 1;
    },
    S:function () {
        return (this.getDate() % 10 == 1 && this.getDate() != 11 ? 'st' : (this.getDate() % 10 == 2 && this.getDate() != 12 ? 'nd' : (this.getDate() % 10 == 3 && this.getDate() != 13 ? 'rd' : 'th')));
    },
    w:function () {
        return this.getDay();
    },
    z:function () {
        var d = new Date(this.getFullYear(), 0, 1);
        return Math.ceil((this - d) / 86400000);
    }, // Fixed now
    // Week
    W:function () {
        var d = new Date(this.getFullYear(), 0, 1);
        return Math.ceil((((this - d) / 86400000) + d.getDay() + 1) / 7);
    }, // Fixed now
    // Month
    F:function () {
        return Date.replaceChars.longMonths()[this.getMonth()];
    },
    f:function () {
        return Date.replaceChars.longMonthsPlural()[this.getMonth()];
    },
    m:function () {
        return (this.getMonth() < 9 ? '0' : '') + (this.getMonth() + 1);
    },
    M:function () {
        return Date.replaceChars.shortMonths()[this.getMonth()];
    },
    n:function () {
        return this.getMonth() + 1;
    },
    t:function () {
        var d = new Date();
        return new Date(d.getFullYear(), d.getMonth(), 0).getDate()
    }, // Fixed now, gets #days of date
    // Year
    L:function () {
        var year = this.getFullYear();
        return (year % 400 == 0 || (year % 100 != 0 && year % 4 == 0));
    }, // Fixed now
    o:function () {
        var d = new Date(this.valueOf());
        d.setDate(d.getDate() - ((this.getDay() + 6) % 7) + 3);
        return d.getFullYear();
    }, //Fixed now
    Y:function () {
        return this.getFullYear();
    },
    y:function () {
        return ('' + this.getFullYear()).substr(2);
    },
    // Time
    a:function () {
        return this.getHours() < 12 ? 'am' : 'pm';
    },
    A:function () {
        return this.getHours() < 12 ? 'AM' : 'PM';
    },
    B:function () {
        return Math.floor((((this.getUTCHours() + 1) % 24) + this.getUTCMinutes() / 60 + this.getUTCSeconds() / 3600) * 1000 / 24);
    }, // Fixed now
    g:function () {
        return this.getHours() % 12 || 12;
    },
    G:function () {
        return this.getHours();
    },
    h:function () {
        return ((this.getHours() % 12 || 12) < 10 ? '0' : '') + (this.getHours() % 12 || 12);
    },
    H:function () {
        return (this.getHours() < 10 ? '0' : '') + this.getHours();
    },
    i:function () {
        return (this.getMinutes() < 10 ? '0' : '') + this.getMinutes();
    },
    s:function () {
        return (this.getSeconds() < 10 ? '0' : '') + this.getSeconds();
    },
    u:function () {
        var m = this.getMilliseconds();
        return (m < 10 ? '00' : (m < 100 ? '0' : '')) + m;
    },
    // Timezone
    e:function () {
        return "Not Yet Supported";
    },
    I:function () {
        return "Not Yet Supported";
    },
    O:function () {
        return (-this.getTimezoneOffset() < 0 ? '-' : '+') + (Math.abs(this.getTimezoneOffset() / 60) < 10 ? '0' : '') + (Math.abs(this.getTimezoneOffset() / 60)) + '00';
    },
    P:function () {
        return (-this.getTimezoneOffset() < 0 ? '-' : '+') + (Math.abs(this.getTimezoneOffset() / 60) < 10 ? '0' : '') + (Math.abs(this.getTimezoneOffset() / 60)) + ':00';
    }, // Fixed now
    T:function () {
        var m = this.getMonth();
        this.setMonth(0);
        var result = this.toTimeString().replace(/^.+ \(?([^\)]+)\)?$/, '$1');
        this.setMonth(m);
        return result;
    },
    Z:function () {
        return -this.getTimezoneOffset() * 60;
    },
    // Full Date/Time
    c:function () {
        return this.format("Y-m-d\\TH:i:sP");
    }, // Fixed now
    r:function () {
        return this.toString();
    },
    U:function () {
        return this.getTime() / 1000;
    }
};

Date.prototype.parseInPageDate = function (elementId) {
    elementId = elementId || '.js-time';
    $(elementId).each(function () {
        var currentEl = $(this);
        currentEl.html(this.parseData(currentEl.attr('time')));
    });
};

/**
 * Fix for IE8 and earlier version.
 */
Date.now = Date.now || function () {
	return new Date().valueOf();
};

var DateUtils = {
    millsDIff: 0,
    timeDiff: 0,
    /**
     * Transforming date to GMT+4
     * TODO: This is temporary, we should use local timezone everywhere
     * The problem is that in blog comments we use server time (date formatting happens on server-side)
     * @param time
     */
    getDate: function (time) {
        return new Date(parseInt(time));
    },

    parseDate: function (str) {
        var arr = str.split('.');
        return new Date(arr[2], arr[1] - 1, arr[0]);
    },

    formatDateSimple: function (time, pattern, isLowerCase) {
        if (time === null) {
            return "";
        }
        pattern = pattern || 'D MMMM YYYY в HH:mm';

        var result;

        var date = moment(time);

        result = date.format(pattern);

        if (isLowerCase) {
            return result.toLowerCase();
        } else {
            return result;
        }
    },

    formatDate: function (time, pattern, isLowerCase, isWithoutDaysCount) {
        if (time === null) {
            return "";
        }
        pattern = pattern || 'D MMMM YYYY в HH:mm';

        var result;

        // hack for show date always in past, because timeDiff doesn't work every time
        var uglyHack = 10000;

        var diff = moment(time).hours(0).minutes(0).seconds(0).milliseconds(1).diff(moment().hours(0).minutes(0).seconds(0).milliseconds(1), "days");
        var localTime = time - DateUtils.millsDIff - uglyHack;

        // yesterday problem fix (campaign time finish - first second of a day)
        // so with usual hacks start of today transforms into end of yesterday
        var serverTime = new Date(time);
        var timeHacked = new Date(localTime);
        if (serverTime.getDate() != timeHacked.getDate()) {
            localTime = time;
        }

        if (isWithoutDaysCount) {
            if (diff < 2 && diff > -2) {
                result = moment(localTime).calendar();
            } else {
                result = moment(localTime).format(pattern);
            }
        } else {
            if (diff <= 3 && diff >= -3) {
                if (diff === 1 || diff === -1) {
                    result = moment(localTime).calendar();
                } else {
                    result = moment(localTime).from(new Date().getTime());
                }
            } else {
                result = moment(localTime).format(pattern);
            }
        }

        if (isLowerCase) {
            return result.toLowerCase();
        } else {
            return result;
        }

    },

    campaignFormatDate: function (time) {
        if (time === null) {
            return "";
        }
        var dateItem = DateUtils.getDate(time);
        var month = dateItem.format('m');
        var date = dateItem.getDate();

        var lang = window.workspace && (workspace.currentLanguage === "en") ? "en" : "ru";

        return (lang === "en")
            ? Date.replaceChars.longMonthsPlural()[month - 1] + "&nbsp;"  + date
            : date + "&nbsp;" + Date.replaceChars.longMonthsPlural()[month - 1].toLowerCase();
    },

    campaignFormatDate2: function (time) {
        if (time === null) {
            return "";
        }
        var dateItem = DateUtils.getDate(time);
        var month = dateItem.format('m');
        var date = dateItem.getDate();
        var year = dateItem.getFullYear();

        var lang = window.workspace && (workspace.currentLanguage === "en") ? "en" : "ru";
        var result = (lang === "en")
            ? Date.replaceChars.longMonthsPlural()[month - 1] + "&nbsp;"  + date
            : date + "&nbsp;" + Date.replaceChars.longMonthsPlural()[month - 1].toLowerCase();

        if (year != new Date().getFullYear()) {
            if (lang === "en") {
                result += ",&nbsp;" + year;
            } else {
                result += "&nbsp;" + year + "&nbsp;г.";
            }
        }
        return result;
    },

    campaignFormatDateWithYear: function (time) {
        var nowDate = new Date().getTime();
        if (nowDate - time < 1000 * 60 * 3) {
            return "только что";
        }

        return this.formatDate(time, "D MMMM YYYY")
    },

    estimatedDate: function (time) {
        return this.formatDate(time, "D MMMM YYYY")
    },

    formatDialogDate: function (time) {
        var date = DateUtils.getDate(time);
        var dif = Math.floor(new Date() / (1000 * 60 * 60 * 24)) - Math.floor(date / (1000 * 60 * 60 * 24));
        if (dif >= 0 && dif <= 1) {
            return (dif == 1 ? "Вчера" : "Сегодня") + date.format(", j f");
        } else {
            return date.format("j f Y");
        }
    },

    formatEventListDate: function (time) {
        var date = DateUtils.getDate(time);
        var dif = Math.floor(new Date().getTime() / (1000 * 60 * 60 * 24)) - Math.floor(date.getTime() / (1000 * 60 * 60 * 24));
        if (Math.abs(dif) < 2) {
            var dayDiff = new Date().getDay() - date.getDay();
            if (dayDiff == 1) {
                return "Вчера";
            } else if (dayDiff == 0) {
                return "Сегодня";
            } else {
                return "Завтра";
            }
        }
        return date.format("j f Y");
    },

    formatDuration: function (timeDifference) {
        return moment.duration(timeDifference).humanize();
    },

    formatUserLoginInfo: function (time) {
        var date = DateUtils.getDate(time);
        var now = DateUtils.getDate(new Date().getTime());
        var dif = Math.floor(now.getTime() / (1000 * 60 * 60 * 24)) - Math.floor(date.getTime() / (1000 * 60 * 60 * 24));
        if (Math.abs(dif) < 2) {
            var dayDiff = new Date().getDay() - date.getDay();
            if (dayDiff == 1) {
                return "вчера в " + date.format("H:i");
            } else if (dayDiff == 0) {
                return "сегодня в " + date.format("H:i");
            }
        }
        return moment(date).from(now) + " в " + date.format("H:i");
    },

    formatDialogTime: function (time) {
        var dateItem = DateUtils.getDate(time);
        var currentDate = new Date();

        if (currentDate.getDate() == dateItem.getDate()) {
            return dateItem.format('H:i');
        } else {
            return dateItem.format('d.m H:i');
        }
    },

    formatBlogTime: function (time) {
        return this.formatDate(time);//this.formatDateTime(time, 'j f Y' + ' года в ' + 'H:i').toLowerCase();
    },

    /**
     * formats count down time
     * @param time
     */
    formatBroadcastTime: function (time) {
        var millisecondsInDay = 3600 * 24 * 1000;
        var days = Math.floor(time / millisecondsInDay);
        var remDays = time - days * millisecondsInDay;
        var hours = Math.floor(remDays / (3600 * 1000));
        var remHours = remDays - hours * 3600 * 1000;
        var minutes = Math.floor(remHours / (60 * 1000));
        var remMinutes = remHours - minutes * 60 * 1000;
        var seconds = Math.floor(remMinutes / 1000);

        return (days > 0 ? days + 'д. ' : '')
            + (hours > 9 ? hours : +'0' + hours) + ':'
            + (minutes > 9 ? minutes : '0' + minutes) + ':'
            + (seconds > 9 ? seconds : '0' + seconds);
    },

    formatDateTime: function (time, format) {
        if (time == null) {
            return "";
        }
        try {
            format = format ? format : 'd.m.y H:i';
            var dateItem = DateUtils.getDate(time);
            return dateItem.format(format);
        } catch (e) {
            return "";
        }
    },

    years: function (n) {
        var d = new Date();
        var currentYear = d.getFullYear();

        var years = [];
        for (var i = currentYear; i >= currentYear - n; i--) {
            years.push(i);
        }
        return years;
    },

    getCurrentTime: function () {
        return new Date().getTime();
    },
    /**
     *
     * @param {Number} unixTimestamp
     * @returns {Boolean} moment is in future
     */
    afterNow: function (unixTimestamp) {
        return this.getCurrentTime() < unixTimestamp;
    },

    getAge: function (time) {
        var today = new Date();
        if (time == null) {
            return "";
        }
        var dateItem = DateUtils.getDate(time);
        var age = today.getFullYear() - dateItem.getFullYear() -
            (today.getMonth() * 32 + today.getDate() >= dateItem.getMonth() * 32 + dateItem.getDate() ? 0 : 1);
        return age;
    },

    getTimeFrom: function (time) {
        return this.formatDateTime(this.getCurrentTime() - time, 'H:i:s');
    },

    getDaysTo: function (time) {
        return this.getDaysBetween(time, new Date().getTime());
    },

    getDaysBetween: function (timeFrom, timeTo) {
        return Math.ceil((timeFrom - timeTo) / 1000 / 60 / 60 / 24);
    },

    getHoursTo: function (time) {
        return Math.round((time - new Date().getTime()) / 1000 / 60 / 60);
    },

    getMinutesTo: function (time) {
        return Math.round((time - new Date().getTime()) / 1000 / 60);
    },

    addDays: function (time, days) {
        return time + (days * (1000 * 60 * 60 * 24));
    },

    addHours: function (time, hours) {
        return time + (hours * (1000 * 60 * 60));
    },

    getHoursDiff: function () {
        return Math.round(DateUtils.timeDiff / 1000 / 60 / 60);
    },

    startOfDate: function (time) {
        var result = DateUtils.getDate(time);
        return result.setHours(0, 0, 0, 0);
    },

    endOfDate: function (time) {
        return DateUtils.startOfDate(time) + (1000 * 60 * 60 * 24) - 1;
    },

    middleOfDate: function (time) {
        return DateUtils.startOfDate(time) + (1000 * 60 * 60 * 12);
    },

    getNumEnding: function (num, cases) {
        num = Math.abs(num);
        var word = '';
        if (num.toString().indexOf('.') > -1) {
            word = cases.gen;
        } else {
            word = (
                num % 10 == 1 && num % 100 != 11
                    ? cases.nom
                    : num % 10 >= 2 && num % 10 <= 4 && (num % 100 < 10 || num % 100 >= 20)
                        ? cases.gen
                        : cases.plu
            );
        }
        return word;
    }
};

var Period = {

    Name: {
        ALL: 'all',
        TODAY: 'today',
        YESTERDAY: 'yesterday',
        WEEK: 'week',
        MONTH: 'month'
    },

    cases: {

        today: function() {
            var date = DateUtils.getCurrentTime()
            return Period.createPeriod(Period.Name.TODAY, date, date);
        },

        yesterday: function() {
            var now = DateUtils.getCurrentTime();
            var date = DateUtils.addDays(now, -1);
            return Period.createPeriod(Period.Name.YESTERDAY, date, date);
        },

        week: function() {
            var dateTo = DateUtils.getCurrentTime();
            var dateFrom = DateUtils.addDays(dateTo, -7);
            return Period.createPeriod(Period.Name.WEEK, dateFrom, dateTo);
        },

        month: function() {
            var dateTo = DateUtils.getCurrentTime();
            var dateFrom = DateUtils.addDays(dateTo, -30);
            return Period.createPeriod(Period.Name.MONTH, dateFrom, dateTo);
        }
    },

    createPeriod: function(period, dateFrom, dateTo) {
        return {
            period: period,
            dateFrom: _.isNull(dateFrom) ? null : DateUtils.startOfDate(dateFrom),
            dateTo: _.isNull(dateTo) ? null : DateUtils.endOfDate(dateTo)
        };
    },

    getPeriod: function(period) {
        var functionName = _.find(_.functions(this.cases), function(functionName) {
            return _.isEqual(functionName.toUpperCase(), period.toUpperCase());
        });

        return _.isUndefined(functionName)
            ? this.createPeriod(period, null, null)
            : this.cases[functionName]();
    }
};
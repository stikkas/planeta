jQuery.fn.extend({
    /**
     * Options configures clickOff handler - it fires on click not on this element(selector)
     *
     * Usage:
     * $('selector').clickOff(function(){
     *     doSomething();
     * }, {
     *     once: false,
     *     selector: 'a'
     * });
     *
     * @param {Function} callback - to execute on click
     * @param {Object} options - {once: false} means that clickOff callback will not be unbind after one usage
     * {selector: 'a'} means callback be called also if selector is specified and e.target is selector within this element
     * @example
     * "
     * $(".div-primary").clickOff(func, {once: false, selector: "a.some-class"})
     * " - execute func every time when click fired not on .div-primary or on any a.some-class link within .div-primary
     */
    clickOff: function (callback, options) {

        var clickOffFired = true;
        var self = this;
        options = _.extend({
            once: true
        }, options);

        var eventName = (options.eventName || "click") + ".offClick";

        var handler = function (e) {
            //if clickOff is not bound to element within modal form
            if (!(self.closest('.modal').length || self.closest('.modal-alert').length)) {
                //prevent to fire clickOff while open modal form
                if ($(e.target).closest('.modal').length || $(e.target).closest('.modal-alert').length || $(e.target).closest('.modal-backdrop').length) {
                    return;
                }
            }
            if (clickOffFired) {
                callbackOnce(e);
            } else {
                clickOffFired = true;
            }
        };

        var callbackOnce = function (e) {
            try {
                callback(e);
            } catch (ignore) {
            }
            if (options.once) {
                $(document).unbind(eventName, handler);
                self.unbind(eventName);
                self.find(options.selector).unbind(eventName);
            }
        };

        var checkClickOff = function () {
            clickOffFired = false;
        };

        if (callback == 'remove') {
            if (self.data('clickOffHandler')) {
                $(document).unbind(eventName, self.data('clickOffHandler'));
                self.unbind(eventName);
            }
            return;
        }

        self.data('clickOffHandler', handler);

        setTimeout(function () {
            self.bind(eventName, checkClickOff);
            $(document).bind(eventName, handler);
            if (options.selector) {
                self.find(options.selector).unbind(eventName).bind(eventName, callbackOnce);
            }
        }, 10);
    }
});

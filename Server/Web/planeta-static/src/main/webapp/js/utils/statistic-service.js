/*global counters, CommonUtils, console, CookieProvider, workspace*/


var StatisticService = (function () {

    var statisticsRequest = function (url, clientId) {
        var statImg = new Image();
        if (clientId <= 0 && window.CookieProvider) {
            clientId = CookieProvider.get('JSESSIONID');
        }
        statImg.src = url + '&clientId=' + clientId + '&_timestamp=' + String(new Date().getTime());
    };

    var getTrackPhotoUrl = function (statServiceUrl, photo) {
        return statServiceUrl + "/photo-view?profileId=" + photo.get("profileId") + "&objectId=" + photo.get("photoId") + "&childObjectId=" + photo.get("albumId");
    };
    var getTrackAudioListenUrl = function (statServiceUrl, track) {
        return statServiceUrl + '/audio-listen?profileId=' + track.get('uploaderProfileId') +
            "&objectId=" + track.get('uploaderTrackId') + "&childObjectId=" + track.get('uploaderAlbumId');
    };
    var getTrackVideoViewUrl = function (statServiceUrl, video) {
        return statServiceUrl + '/video-view?profileId=' +
            video.get('profileId') + "&objectId=" + video.get('videoId');
    };

    var getTrackStatEventUrl = function (statServiceUrl, type, objectId, sourceId, referer, utmSource) {
        var url = statServiceUrl + '/stat-event?type=' + type + '&object=' + objectId + "&source=" + sourceId;
        var location = document.location.href;
        var index = location.indexOf('?');
        var uri = index > 0 ? location.substring(0, index) : location;
        url += '&uri=' + uri;

        if (referer) {
            url += '&ref=' + encodeURIComponent(decodeURIComponent(referer));
        }
        if (utmSource) {
            url += '&utm_source=' + encodeURIComponent(decodeURIComponent(utmSource));
        }
        return url;
    };


    var trackPageView = function (url) {
        if (window.gtm) {
            window.gtm.trackUniversalEvent('pageView', url);
        }
    };

    var trackPageViewCampaign = function (url) {
        if (window.gtm) {
            window.gtm.trackCampaignPageViewEvent(url);
        }
    };

    var statisticService = {};

    var trackVideoView = function (video) {
        statisticsRequest(getTrackVideoViewUrl(statisticService.statServiceUrl, video), statisticService.clientId);
    };

    var trackAudioListen = function (audio) {
        statisticsRequest(getTrackAudioListenUrl(statisticService.statServiceUrl, audio), statisticService.clientId);
    };
    var trackPhotoView = function (photo) {
        statisticsRequest(getTrackPhotoUrl(statisticService.statServiceUrl, photo), statisticService.clientId);
    };
    var trackStatEvent = function (type, objectId) {
        if (!objectId) {
            console.warn('offer id is not provided');
            return;
        }
        var sourceId = CommonUtils.urlParam('source') || 0;
        var referer = CommonUtils.urlParam('ref') || document.referrer;
        var utmSource = CommonUtils.urlParam('utm_source') || null;
        statisticsRequest(getTrackStatEventUrl(statisticService.statServiceUrl, type, objectId, sourceId, referer, utmSource), statisticService.clientId);
    };

    var trackBannerClick = function (bannerId) {
        statisticsRequest(statisticService.statServiceUrl + '/api/public/our-reclame-clicked?reclameId=' + bannerId +
            "&referer=" + window.location.href + "&profileId=" + workspace.appModel.myProfileId(), statisticService.clientId);
    };
    // Банер отобразился на странице
    var trackBannerAppear = function (bannerId) {
        statisticsRequest(statisticService.statServiceUrl + '/api/public/our-reclame-appear?reclameId=' + bannerId +
            "&referer=" + window.location.href + "&profileId=" + workspace.appModel.myProfileId(), statisticService.clientId);
    };
    var trackHasReclameTrimmer = function () {
        statisticsRequest(statisticService.statServiceUrl + "/api/public/has-reclame-trimmer?profileId=" + workspace.appModel.myProfileId(), statisticService.clientId);
    };

    var tackNoProfileId = function (modelName) {
        statisticsRequest(statisticService.statServiceUrl + "/api/public/noprofileId?from_url=" + window.location.pathname
            + "&profileId=" + workspace.appModel.myProfileId() + "&model=" + modelName, statisticService.clientId);
    };

    var init = function (options) {
        statisticService.statServiceUrl = options.statServiceUrl;
        statisticService.clientId = options.clientId || -1;
        return {
            trackPageView: trackPageView,
            trackPageViewCampaign: trackPageViewCampaign,
            trackVideoView: trackVideoView,
            trackAudioListen: trackAudioListen,
            trackPhotoView: trackPhotoView,
            trackStatEvent: trackStatEvent,
            trackBannerClick: trackBannerClick,
            trackBannerAppear: trackBannerAppear,
            trackHasReclameTrimmer: trackHasReclameTrimmer,
            tackNoProfileId: tackNoProfileId
        }
    };

    return {
        init: init
    }

})();

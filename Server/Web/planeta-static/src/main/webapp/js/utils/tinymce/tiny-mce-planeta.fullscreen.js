/*global TinyMcePlaneta, Html5Fullscreen, jQuery */
(function ($) {
    /**
     * Set height of element to pageHeight - standardContentHeight
     * @return {Number} cssHeight of selected element
     */
    TinyMcePlaneta.jqueryFunctions.fullScreen = function () {
        var contentHeight = 0;
        var nonPreviewContent = [$('#page-head'), $('.edit-blog-header'), $('.edit-blog-info'), $('.top-breads')];
        $.each(nonPreviewContent, function (index, el) {
            contentHeight += $(el).is(":visible") ? $(el).outerHeight(true) : 0;
        });
        contentHeight += parseInt($('#global-container').css('paddingTop'), 10);
        var $editBlog = $('.edit-blog');
        contentHeight += parseInt(this.css('paddingTop'), 10);
        contentHeight += parseInt($editBlog.css('marginTop'), 10);
        contentHeight += parseInt($editBlog.css('marginBottom'), 10);
        contentHeight += parseInt(this.css('paddingBottom'), 10);
        contentHeight += parseInt(this.css('borderBottomWidth'), 10);
        contentHeight += parseInt(this.css('borderTopWidth'), 10);
        var additional = $('#tinymce-body_toolbargroup').outerHeight();

        var elementHeight = $(window).height() - contentHeight - (additional ? 22 : 0);
        this.height(elementHeight - 5);
        return elementHeight - 5;
    };

})(jQuery);

TinyMcePlaneta.createButtonPlugin("PlanetaFullScreen", function (self) {
    self.fullScreen = false;
    self.enableHtml5FullScreen = true;

    var fullScreenButton = null;
    self.toHide = [$('#page-head'), $('.edit-blog-header'), $('.edit-blog-info'), $('.top-breads')];

    function tinyResize() {
        try {
            var tinyHeight = $("#tinymce-body_tbl").tinyMcePlaneta('fullScreen');
            // let's use street magic instead of boring layout calculations
            var tinyHead = 37;
            $("#tinymce-body_ifr").height(tinyHeight - tinyHead);

            var sum = 0;
            // prevent errors if init not ended
            if (self.editor.getDoc() == null) {
                return;
            }
            var $body = $(self.editor.getBody());
            $body.children().each(function () {
                sum += $(this).height();
                $(this).removeAttr("height");
            });
            $body.height(Math.max(sum, tinyHeight - tinyHead - 20));
            $body.removeAttr("height");
        } catch (ignored) {
//            console.log("editor resize failed: " + e);
        }
    }

    self.editor.addCommand(self.name, function () {
        // static init once
        fullScreenButton = fullScreenButton || $(".mceToolbar  span.mce_planetafullscreen");
        if (self.fullScreen) {
            $.each(self.toHide, function (index, el) {
                if (self.enableHtml5FullScreen) {
                    Html5Fullscreen.cancel();
                }
                $(el).show();
            });
            fullScreenButton.removeClass('mce_planetanormalscreen').addClass('mce_planetafullscreen');
            self.fullScreen = false;
        } else {
            $.each(self.toHide, function (index, el) {
                if (self.enableHtml5FullScreen) {
                    Html5Fullscreen.request();
                }
                $(el).hide();
            });
            fullScreenButton.removeClass('mce_planetafullscreen').addClass('mce_planetanormalscreen');
            self.fullScreen = true;
        }
        tinyResize();
    });

    self.editor.onNodeChange.add(function (ed) {
        tinyResize();
    });

    self.editor.onPostRender.add(function (ed) {
        $("[id^=tinymce-body][id$=_tbl]").width('100%');
        tinyResize();
    });

    $(window).resize(function () {
        tinyResize();
    });

});


TinyMcePlaneta.createButtonPlugin("PlanetaFullScreenCampaign", function (self) {
    self.fullScreen = false;

    var fullScreenButton = null;

    var editorBody = $('.edit-blog-body');
    var editorOverlay = editorBody.next();

    function tinyResize() {
        if ( !(tinymce.activeEditor && tinymce.activeEditor.contentAreaContainer) ) return;
        var editorFrame = $(tinymce.activeEditor.contentAreaContainer).find('> iframe');
        if (self.fullScreen) {
            var winHeight = $(window).height();
            editorFrame.css('minHeight', winHeight - 36);
        } else {
            editorFrame.css('minHeight', '');
        }
    }

    self.editor.addCommand(self.name, function () {
        fullScreenButton = fullScreenButton || $(".mceToolbar  span.mce_planetafullscreencampaign");
        if (self.fullScreen) {
            $('html').removeClass('campaign-editor-fullscreen');
            editorOverlay.hide();
            fullScreenButton.removeClass('mce_planetanormalscreen').addClass('mce_planetafullscreen');
            self.fullScreen = false;
        } else {
            $('html').addClass('campaign-editor-fullscreen');
            editorOverlay.show();
            fullScreenButton.removeClass('mce_planetafullscreen').addClass('mce_planetanormalscreen');
            self.fullScreen = true;
        }
        tinyResize();
    });

    self.editor.onPostRender.add(function (ed) {
        $('#tinymce-body_planetafullscreencampaign .mceIcon').addClass('mce_planetafullscreen');
        $("[id^=tinymce-body][id$=_tbl]").width('100%');
    });

    $(window).resize(function () {
        tinyResize();
    });

});

TinyMcePlaneta.createButtonPlugin("PlanetaFullScreenFaqEdit", function (self) {
    self.fullScreen = false;
    var fullScreenButton,width,height;

    function tinyResize() {
        var $el = $(self.editor.getElement());
        self.editor.theme.resizeTo(width, $el.parent().height() - 32);
    }

    self.editor.addCommand(self.name, function () {

        fullScreenButton = fullScreenButton || $(".mceToolbar  span.mce_planetafullscreenfaqedit");
        self.fullScreen = !self.fullScreen;
        fullScreenButton.toggleClass('mce_planetanormalscreen', self.fullScreen).toggleClass('mce_planetafullscreen', !self.fullScreen);
        $('html').toggleClass('faq-edit-fullscreen', self.fullScreen);


        if (!width) {
            var $el = $(self.editor.getElement());
            width = $el.width();
            height = $el.height();
        }
        if (self.fullScreen) {
            $(window).on('resize', tinyResize);
            tinyResize();
        } else {
            self.editor.theme.resizeTo(width, height);
            $(window).off('resize', tinyResize);
        }
    });

});

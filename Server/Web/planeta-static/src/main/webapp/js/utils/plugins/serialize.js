jQuery.fn.extend({
    serializeObject:function () {
        var o = {};
        var a = this.serializeArray();
        $.each(a, function () {
            if (o[this.name] !== undefined) {
                if (!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(this.value || '');
            } else {
                o[this.name] = this.value || '';
            }
        });
        return o;
    },

	serializeParams: function() {
		var params = "";
		var a = this.serializeArray();
		$.each(a, function() {
		 	params += '&' + this.name + '=' + (encodeURIComponent(this.value) || '');
		});
		return params;
	}
});
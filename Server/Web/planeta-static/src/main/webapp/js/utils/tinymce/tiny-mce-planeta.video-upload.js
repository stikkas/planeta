/*globals Attach, TinyMcePlaneta, StringUtils, UploadController, tinymce, ProfileUtils*/
TinyMcePlaneta.createButtonPlugin("PlanetaVideo", function (self) {
    self.JSON = tinymce.util.JSON;
    self.html = '';
    self.contextMenu = null;

    self.editor.onPreInit.add(function (ed) {
        self.contextMenu = new TinyMcePlaneta.ImageContextMenu(ed, self.clsName);
        self.contextMenu.addCloseButton(function () {
        });
    });

    self.editor.addCommand(self.name, function () {
        var rng = self.editor.selection.getRng();
        var profileId = workspace ? workspace.appModel.get('profileModel').get('profileId') : '';
        UploadController.exec("showUploadOneVideo", profileId, function (filesUploaded) {
            if (!filesUploaded) {
                return;
            }
            var uploadResult = filesUploaded.at(0).get('uploadResult');
            if (uploadResult) {
                var $el = TinyMcePlaneta.Video.getElementToInsert(uploadResult);
                self.editor.selection.setRng(rng);
                TinyMcePlaneta.insertHtml(self.editor, $el.html());
            }
        });
    });
});




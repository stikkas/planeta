/*globals _,$,console  */
/**
 * Created with IntelliJ IDEA.
 * User: a.volkov
 * Date: 13.08.12
 * Time: 16:50
 */

(function () {
    if (window.debugMode) {
        return;
    }
    var global = this;
    window.console = {
        enabled: /^(((\w+[.])?(dev|test)[.](\w+[.])+ru)|(localhost.*?)|(192[.]168[.]10[.]\d+))$/.test(location.hostname),
        impl: global.console || {},

        init: function () {

        },
        log: function () {
            this.call('log', arguments);
        },
        debug: function () {
            this.call('debug', arguments);
        },
        info: function () {
            this.call('info', arguments);
        },
        warn: function () {
            this.call('warn', arguments);
        },
        error: function () {
            this.call('error', arguments);
        },
        trace: function () {
            this.call('trace', arguments);
        },
        assert: function () {
            this.call('assert', arguments);
        },
        call: function (method, args) {
            try {
                if (this.enabled && this.impl[method]) {
                    if ($.browser.msie && !(args instanceof String)) {
                        args = [JSON.stringify(args)];
                    }
                    Function.prototype.apply.call(console.impl[method], console.impl, args);
                }
            } catch (ignored) { // i.e. IE curcular references in args = [JSON.stringify(args)]
                //
            }
        }
    };
}());

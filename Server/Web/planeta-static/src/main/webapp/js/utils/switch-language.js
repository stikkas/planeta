

var toggleLanguage = function (current) {
    var switchLanguage = function (language) {
        if (window.workspace && workspace.stats && workspace.stats.trackStatEvent) {
            var langId;
            if (language === 'en') {
                langId = 1;
            } else {
                langId = 2;
            }
            workspace.stats.trackStatEvent('LANG_CHANGE', langId);
        }
        document.cookie = ("org.springframework.web.servlet.i18n.CookieLocaleResolver.LOCALE=" + language + "; path=/; domain=." + workspace.serviceUrls.mainHost);
        document.location.reload();
    };

    var PlanetaLanguages = ["ru", "en"];
    var currentIndex = PlanetaLanguages.indexOf(current);
    currentIndex = Math.max(currentIndex, 0);
    switchLanguage(PlanetaLanguages[1 - currentIndex]);
};

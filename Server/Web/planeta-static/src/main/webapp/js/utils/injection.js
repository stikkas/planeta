/*globals _,$  */

var injection = {
    helpers: {
        checkNested: function (obj /*, 'level1', 'level2', ... 'levelN'*/) {
            var args = Array.prototype.slice.call(arguments, 1);
            for (var i = 0; i < args.length; i++) {
                if (!obj || !Object.prototype.hasOwnProperty.call(obj, args[i])) {
                    return false;
                }
                obj = obj[args[i]];
            }
            return true;
        },
        objectAssign: function (obj, keyPath, value) {
            var lastKeyIndex = keyPath.length - 1;
            for (var i = 0; i < lastKeyIndex; ++i) {
                var key = keyPath[i];
                if (!(key in obj))
                    obj[key] = {};
                obj = obj[key];
            }
            obj[keyPath[lastKeyIndex]] = value;
            return obj;
        }
    },
    init: function (path, callback) {
        if (!injection.helpers.checkNested.apply(null, [injection].concat(path))) {
            injection.helpers.objectAssign(injection, path, callback);
        }
    },

    sponsors: {},
    loadDfd: {},

    load: function (alias) {
        if (!alias) {
            return;
        }
        var self = this;

        function fail() {
            if (window.debugMode) {
                var msg = 'ошибка при загрузке injection';
                workspace.appView.showErrorMessage(msg);
                console.error(msg);
            }
        }

        if (!this.loadDfd[alias]) {

            this.loadDfd[alias] = $.Deferred();
            this.aliasQueue = this.aliasQueue || [];
            this.aliasQueue.push(alias);
            if (!this.deferedLoad) {
                this.deferedLoad = setTimeout(function () {
                    Backbone.sync('read', null, {
                        url: '/api/welcome/get-sponsors.json',
                        traditional: true,
                        data: {
                            alias: self.aliasQueue
                        }
                    }).done(function (respons) {
                        if (respons && respons.result) {
                            var sponsors = respons.result;
                            _.each(sponsors, function (sponsor) {
                                self.sponsors[sponsor.alias] = sponsor;
                                self.loadDfd[sponsor.alias].resolve();
                            });
                        } else {
                            fail();
                        }
                    }).fail(fail);
                    self.deferedLoad = null;
                    self.aliasQueue = [];
                }, 10);
            }
        }

        return this.loadDfd[alias].promise();
    },

    injectTmpl: function (alias, view) {
        var dfd = this.load(alias);
        var self = this;
        var id = 'injection-' + alias + "-" + view.injectionHtml;
        if (!document.getElementById(id) || view.injectionAnchor) {
            dfd.done(function () {
                var sponsor = self.sponsors[alias];
                if (sponsor && sponsor[view.injectionHtml]) {
                    if (view.injectionAnchor) {
                        var $el = view.$(view.injectionAnchor);
                        if ($el.children().size() === 0) {
                            $el.append(sponsor[view.injectionHtml]);
                        }
                    } else if (!document.getElementById(id)) {
                        $('body').append($('<div id="' + id + '"></div>').append(sponsor[view.injectionHtml]));
                    }
                }
            });
        }
        return dfd;
    },

    afterRender: function (view) {
        try {//removeTryCatch
            var self = this;
            var alias = view.getSponsorAlias ? view.getSponsorAlias() : view.model.get('sponsorAlias');
            if (alias) {
                injection.injectTmpl(alias, view).done(function () {
                    if (view.injectionFunc && self[view.injectionFunc] && self[view.injectionFunc][alias]) {
                        self[view.injectionFunc][alias](view.$el, view.model);
                    }
                });
            } else {
                if(view.model) {
                    var productTags = view.model.get('tags');
                }
                if(productTags && !view.model.get('campaignId')) {
                    productTags.forEach(function (tag) {
                        var name = tag.mnemonicName;
                        injection.injectTmpl(name, view).done(function () {
                            if (view.injectionFunc && self[view.injectionFunc] && self[view.injectionFunc][name]) {
                                self[view.injectionFunc][name](view.$el, view.model);
                            }
                        });
                    });
                }
            }
        } catch (e) {
            if (window.debugMode) {
                var msg = 'ошибка при рендере injection';
                workspace.appView.showErrorMessage(msg);
                console.error(msg, view);
            }
        }

    }

};
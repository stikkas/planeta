/*globals _,$,workspace  */
var AlbumTypes = window.AlbumTypes = {
    ALBUM_DEFAULT: 1,
    ALBUM_AVATAR: 2,
    ALBUM_HIDDEN: 4,
    ALBUM_PRODUCT: 6,
    ALBUM_COMMENT: 7,
    ALBUM_BROADCAST: 9,
    ALBUM_BROADCAST_STREAM: 10,
    ALBUM_CAMPAIGN: 11
};

var ImageType = window.ImageType = {
    USER: 1,
    USER_FEMALE: 2,
    EVENT: 3,
    GROUP: 4,
    AUDIO: 10,
    VIDEO: 11,
    PHOTO: 12,
    PRODUCT: 20,
    DONATE: 21
};

var ImageUtils = window.ImageUtils = {

    SMALL_AVATAR: {
        fileName: 'small',
        width: 150,
        height: 150,
        crop: true,
        disableAnimatedGif: true
    },
    AVATAR: {
        fileName: 'user_avatar',
        width: 183,
        height: 0
    },
    PHOTO_PREVIEW: {
        fileName: 'photo_preview',
        width: 150,
        height: 112
    },
    ORIGINAL: {
        fileName: 'original',
        width: 1000,
        height: 1000
    },
    REAL: {
        fileName: 'real', //really uncropped file (copy of uploaded file)
        width: 1600,
        height: 0
    },
    ALBUM_COVER: {
        fileName: 'album_cover',
        width: 130,
        height: 130,
        crop: true,
        disableAnimatedGif: true
    },
    PRODUCT_COVER: {
        fileName: 'product_cover',
        width: 280,
        height: 280,
        crop: true,
        disableAnimatedGif: true
    },
    PRODUCT_COVER_NEW: {
        fileName: 'product_cover_new',
        width: 280,
        height: 280,
        crop: true,
        disableAnimatedGif: true
    },
    TV_COVER: {
        fileName: 'tv_cover',
        width: 220,
        height: 124,
        crop: true,
        pad: false,
        disableAnimatedGif: true
    },
    TV_BIG_COVER: {
        fileName: 'tv_big_cover',
        width: 460,
        height: 259,
        crop: true,
        pad: false,
        disableAnimatedGif: true
    },
    SMALL: {
        fileName: 'small',
        width: 150,
        height: 0
    },
    MEDIUM: {
        fileName: 'medium',
        width: 300,
        height: 0
    },
    SHARE_COVER: {
        fileName: 'small',
        width: 220,
        height: 150,
        useProxy: true,
        crop: true,
        pad: false
    },
    BIG: {
        fileName: 'big',
        width: 450,
        height: 0
    },
    HUGE: {
        fileName: 'huge',
        width: 670,
        height: 0
    },
    HUGE_FIXED: {
        useProxy: true,
        fileName: 'huge',
        width: 670,
        height: 502,
        pad: true
    },
    CAMPAIGN_VIDEO: {
        fileName: 'original_campaign',
        width: 600,
        height: 338,
        crop: true,
        pad: false
    },
    VIDEO_MIDDLE: {
        fileName: 'video_middle',
        width: 480,
        height: 270
    },

    CAMPAIGN_BACKGROUND: {
        fileName: 'background_campaign',
        width: 1920,
        height: 1080
    },

    PROJECT_ITEM_MAIN_SLIDER: {
        useProxy: true,
        fileName: 'original',
        width: 640,
        height: 390,
        crop: true
    },

    TV_ITEM_MAIN_SLIDER: {
        useProxy: true,
        fileName: 'original',
        width: 880,
        height: 495,
        crop: true
    },

    PROJECT_ITEM: {
        useProxy: true,
        fileName: 'original',
        width: 370,
        height: 225,
        pad: true
    },

    PROJECT_ITEM_BLUR: {
        useProxy: true,
        fileName: 'original',
        width: 23,
        height: 14,
        pad: true,
        disableAnimatedGif: true
    },

    CHARITY_PROJECT_ITEM: {
        useProxy: true,
        fileName: 'original',
        width: 283,
        height: 172,
        pad: true
    },

    CHARITY_PROJECT_ITEM_MAIN_SLIDER: {
        useProxy: true,
        fileName: 'original',
        width: 283,
        height: 172,
        pad: true
    },

    CHARITY_SCHOOL_EVENTS_ITEM: {
        useProxy: true,
        fileName: 'original',
        width: 580,
        height: 276,
        crop: true,
        pad: false
    },

    PROMO_NEWS: {
        useProxy: true,
        fileName: 'original',
        width: 380,
        height: 250,
        crop: true,
        pad: false
    },

    getThumbnailUrl: function (imageUrl, imageConfig, imageType) {
        function getFileName(url, imageConfig) {
            var imageUrl;

            switch (imageConfig) {
            case ImageUtils.SMALL_AVATAR:
                imageUrl = "/images/defaults/" + url + "-small-avatar.jpg";
                break;
            case ImageUtils.MEDIUM:
                imageUrl = "/images/defaults/" + url + "-medium.jpg";
                break;
            case ImageUtils.AVATAR:
                imageUrl = "/images/defaults/" + url + "-avatar.jpg";
                break;
            case ImageUtils.PHOTO_PREVIEW:
                imageUrl = "/images/defaults/" + url + "-photo-preview.jpg";
                break;
            case ImageUtils.ALBUM_COVER:
                imageUrl = "/images/defaults/" + url + "-album-cover.jpg";
                break;
            case ImageUtils.PRODUCT_COVER:
                imageUrl = "/images/defaults/" + url + "-product-cover.jpg";
                break;
            case ImageUtils.PRODUCT_COVER_NEW:
                imageUrl = "/images/defaults/" + url + "-product-cover-new.jpg";
                break;
            case ImageUtils.PROMO_NEWS:
                imageUrl = "/images/defaults/" + url + "-promo-news.jpg";
                break;
            case ImageUtils.SHARE_COVER:
                imageUrl = "/images/defaults/" + url + "-small.jpg";
                break;
            default:
                imageUrl = "/images/defaults/" + url + "-avatar.jpg";
            }
            return workspace.staticNodesService.getResourceUrl(imageUrl);
        }

        var staticNodesService = workspace.staticNodesService;
        var proxyParams = staticNodesService.getProxyParamsServerPath(imageUrl);
        var proxyOptions = {
            width: imageConfig.width,
            height: imageConfig.height,
            crop: !!imageConfig.crop,
            pad: !!imageConfig.pad,
            disableAnimatedGif: !!imageConfig.disableAnimatedGif
        };
        if (proxyParams) {
            // add image of other size to the same static node
            return staticNodesService.createProxyUrl(proxyParams.host, proxyParams.url, proxyOptions);
        }

        if (staticNodesService.isStaticServerPath(imageUrl) && !imageConfig.useProxy) {
            var originalFileName = imageUrl.substr(imageUrl.lastIndexOf('/') + 1).toLowerCase();
            var filePath = imageUrl.substr(0, imageUrl.length - originalFileName.length);
            filePath = StringUtils.replaceHttpToHttps(filePath);
            var extension = originalFileName.substr(originalFileName.length - 4) === '.gif' ? '.gif' : '.jpg';
            return filePath + imageConfig.fileName + extension;
        }

        if (imageUrl) {
            if (imageUrl.lastIndexOf('data:image', 0) === 0) {
                return imageUrl;

            }
            return staticNodesService.createProxyUrl(staticNodesService.findStaticNode(imageUrl), imageUrl, proxyOptions);
        }


        switch (imageType) {
        case ImageType.USER:
            return getFileName('user-male', imageConfig);
        case ImageType.USER_FEMALE:
            return getFileName('user-female', imageConfig);
        case ImageType.GROUP:
            return getFileName('group', imageConfig);
        case ImageType.EVENT:
            return getFileName('event', imageConfig);
        case ImageType.AUDIO:
            return getFileName('audio', imageConfig);
        case ImageType.VIDEO:
            return getFileName('video', imageConfig);
        case ImageType.PHOTO:
            return getFileName('photo', imageConfig);
        case ImageType.PRODUCT:
            return getFileName('product', imageConfig);
        case ImageType.DONATE:
            return getFileName('donate', imageConfig);
        default:
            return getFileName('generic', imageConfig);
        }
    },

    getUserAvatarUrl: function (imageUrl, fileName, userGender) {
        if (userGender && userGender === "FEMALE") {
            return this.getThumbnailUrl(imageUrl, fileName, ImageType.USER_FEMALE);
        }

        return this.getThumbnailUrl(imageUrl, fileName, ImageType.USER);
    }
};

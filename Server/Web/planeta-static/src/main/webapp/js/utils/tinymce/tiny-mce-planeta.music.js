/*globals Attach, TinyMcePlaneta, StringUtils, UploadController*/
TinyMcePlaneta.Audio = new TinyMcePlaneta.RichMedia({
    clsName: 'mceplanetamusic',
    parseRichMedia: function (attributes) {
        var src = document.location.protocol + "//" + workspace.staticNodesService.getStaticNode() +
            '/audio-stub.jpg?' + TinyMcePlaneta.serializeData({
                name:       attributes.name,
                artist:     attributes.artist,
                duration:   StringUtils.humanDuration(attributes.duration),
                width:      TinyMcePlaneta.currentConfiguration.audio_width
            });
        var $imgReplacement = $(document.createElement('img'));
        $imgReplacement.attr({
            'data-mce-json': JSON.stringify(attributes),
            'class': TinyMcePlaneta.Audio.clsName,
            'src': src,
            width: attributes.width,
            height: attributes.height
        });
        $imgReplacement.addClass(attributes['class']);
        return $imgReplacement;
    },

    insertAudio: function (self, callback) {
        return function() {
            var rng = self.editor.selection.getRng();
            callback(function(audio) {
                var duration = audio.duration || audio.trackDuration;
                var tmpElement = $('<div></div>');
                var richMediaData = {
                    duration: duration,
                    name: tmpElement.text(audio.name).html(),
                    owner: audio.ownerId || audio.owner || audio.profileId,
                    artist: tmpElement.text(audio.artist || audio.artistName).html(),
                    id: audio.objectId || audio.id
                };
                tmpElement.remove();
                var $el = $(document.createElement('div'));
                $el.append(TinyMcePlaneta.Audio.parseRichMedia(richMediaData));
                // restore cursor
                self.editor.selection.setRng(rng);
                TinyMcePlaneta.insertHtml(self.editor, $el.html());
            });
        };
    }
});



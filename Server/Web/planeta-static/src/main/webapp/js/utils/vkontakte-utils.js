/**
 * Lazy-initializing VK OpenAPI namespace alias
 * For development comfort all it just copies VK, but all functions
 * may cause initializing before executing
 */
var VkontakteUtils = function () {

    /**
     * @type {Boolean}
     */
    var vkInitialized = false;

    var transportId = 'vk_api_transport';
    var apiId = 3108980;

    var transportDiv = $('<div id="' + transportId + '"></div>');

    /**
     * Lazy vkInit with callback
     */
    function vkInit(callback) {
        if (vkInitialized) {
            callback();
            return;
        }

        window.vkAsyncInit = function () {
            VK.init({
                apiId: apiId,
                nameTransportPath: null
            });
            vkInitialized = true;
            callback();
        };

        $(document.body).append(transportDiv);
        var el = document.createElement("script");
        el.type = "text/javascript";
        el.src = "http://vkontakte.ru/js/api/openapi.js";
        el.async = true;
        document.getElementById(transportId).appendChild(el);

    }

    function loginViaVK() {
        vkInit(function () {
            VK.Widgets.Auth("vk_auth", {
                width: "200px",
                authUrl: '/start.html'
            });
        });
    }

    function drawLoginViaVKButton(id, options) {
        vkInit(function () {
            var $socialContainer = $('#' + id);
            $socialContainer.css('padding-top', 10);
            $socialContainer.children().not('#vk-social').hide();
            if ($socialContainer.find('#vk-social').length == 0) {
                var widgtsOpts = _.extend({
                    width: $socialContainer.parent().width(),
                    authUrl: 'https://'+workspace.serviceUrls.mainHost+'/welcome/vk.html?vk_auth=true'
                }, options);
                $socialContainer.append('<div id="vk-social"></div>');
                VK.Widgets.Auth('vk-social', widgtsOpts);
            } else {
                $socialContainer.find('#vk-social').toggle();
            }
        });
    }

    return {
        afterInit: vkInit,
        loginViaVK: loginViaVK,
        drawLoginViaVKButton: drawLoginViaVKButton
    };
}();

var FacebookUtils = function () {

    /**
     * @type {Boolean}
     */
    var fbInitialized = false;

    var transportId = 'fb-root';
    var apiId = 475204385834513;

    var transportDiv = $('<div id="' + transportId + '"></div>');


    function fbInit(callback) {
        if (fbInitialized) {
            callback();
            return;
        }

        window.fbAsyncInit = function () {
            FB.init({
                appId: apiId,
                channelUrl: '/start.planeta.ru/virtual', // Channel File
                status: false, // check login status
                cookie: true, // enable cookies to allow the server to access the session
                xfbml: true  // parse XFBML
            });
            fbInitialized = true;
            callback();
        };
        (function (d) {
            $(document.body).append(transportDiv);
            var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
            if (d.getElementById(id)) {
                return;
            }
            js = d.createElement('script');
            js.id = id;
            js.async = true;
            js.src = "//connect.facebook.net/ru_RU/all.js";
            ref.parentNode.insertBefore(js, ref);
        })(document);
    }
    function drawLoginViaFBButton(id, options) {
        OAuthUtils.facebookClick();
        return;
        fbInit(function () {
            var $socialContainer = $('#' + id);
            $socialContainer.children().not('#fb-social').hide();
            if ($socialContainer.find('#fb-social').length == 0) {
                var $regDiv = $('<div class="fb-registration"></div>');
                $regDiv.attr('width', $socialContainer.parent().width());
                $regDiv.attr('data-fields', JSON.stringify([
                    {name: 'name'},
                    {name: 'email'}
                ]));
                $regDiv.attr('data-redirect-uri', 'https://'+workspace.serviceUrls.mainHost+'/welcome/fb.html');
                $socialContainer.append('<div id="fb-social"></div>');
                $socialContainer.find('#fb-social').append($regDiv);
            } else {
                $socialContainer.find('#fb-social').toggle();

            }
        });
    }

    function drawProjectsShareButton(selector) {
        fbInit(function () {
            $(selector).append('<fb:like href="' + document.URL + '" send="false" layout="box_count" width="52" show_faces="false"></fb:like>');
        });
    }

    return {
        drawLoginViaFBButton: drawLoginViaFBButton,
        drawProjectsShareButton: drawProjectsShareButton
    }


}();



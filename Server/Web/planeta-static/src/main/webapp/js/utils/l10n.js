/**
 * Created with IntelliJ IDEA.
 * User: Andrew Arefyev
 * Date: 18.09.12
 * Time: 23:21
 *
 */
var L10n = function (word, lang) {
	return L10n.translate(arguments);
};
_.extend(L10n, {
	RU:"RU",
	_dictionary:{
		"RU":{
			"city":"Город",
			"country":"Страна",
			"description":"Описание",
			"descriptionHTML":"Описание",
			"phone":"Номер телефона",
			"street":"Локальный адрес",
			"title":"Название",
			"404":"Сервис не найден."
		},
		version:"1.1"
	},
	/**
	 *  returns "lang" synonym for "word"
	 * @param word
	 * @param lang
	 * @return translated or base word
	 */
	translate:function (word, lang) {
		if (!lang)
			throw "lahguage is empty.";
		return this._dictionary[lang][word] || word;
	}

});

/*global TinyMcePlaneta*/
TinyMcePlaneta.EditorModel = function (parentModel) {
    _.extend(this, {
        parentModel: parentModel,
        config: TinyMcePlaneta.getDefaultConfiguration(),
        rendered: false,
        rendering: false,
        autoHeight: function () {
            return 400;
        },
        calcMargins: function ($el) {
            return $el.outerHeight(true) - $el.height();
        },
        width: 700,
        init: _.debounce(function (selector) {
            this.element = $(selector);
            this.element.width(this.width);
            var height = this.height || this.autoHeight();
            if (height) {
                this.element.height(height);
            }
            if ($("iframe#tinymce-body_ifr", this.element).length) {
                return;
            }
            if (this.rendering) {
                return;
            }

            var self = this;
            var failTimer = setTimeout(function () {
                self.rendering = false;
            }, 2500);
            this.config.setup = function () {
//                console.log('init finished');
                setTimeout(function () {
                    $('.mceEditor').filter(function () {
                        return $(this).css('display') == 'none';
                    }).remove();
                    self.rendering = false;
                    self.rendered = true;
                    $('#description-editor-stub').remove();
                }, 100);

            };
            this.rendering = true;

//            console.log('init started');
            $(this.element).tinyMcePlaneta(this.config);
        }, 1000, true),

        save: function () {
            if (this.rendered) {
                this.saveToParentModel();
            }
        },

        saveToParentModel: function () {
            this.parentModel.set('descriptionHtml', this.html(), {silent: true});
            this.parentModel.set('description', this.text(), {silent: true});
        },


        removeEditor: function () {
            if ( !$(this.element).length ) return;
            var id = $(this.element).attr('id');
            var instance = tinyMCE.getInstanceById(id);
            if ( !!instance ) {
                try{
                    tinymce.EditorManager.remove(instance)
                } catch (e) {
                    console.log(e);
                }
            }
        },


        destruct: function () {
            this.removeEditor();
            if (this.rendered) {
                this.destructPlugins();
            }
            this.rendered = false;
            this.rendering = false;

        },

        destructPlugins: function () {
        },

        html: function () {
            return $(this.element).tinyMcePlaneta('mappedHtml');
        },

        text: function () {
            return $(this.element).tinyMcePlaneta('blogText');
        },

        textClean: function () {
            return $(this.element).tinyMcePlaneta('textClean');
        },

        serverHtmlClean: function (dirtyHtml, callback) {
            var url = "/api/util/html-clean.json";
            Backbone.sync('update', null, {
                url: url,
                data: {
                    html: dirtyHtml
                },
                success: function (response) {
                    callback(response.result);
                }
            });
        }

    });
};

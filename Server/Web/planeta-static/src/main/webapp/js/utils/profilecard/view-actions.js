/*globals ProfileInfoHover, Tags, moduleLoader, HoverInfoCard, User, PrivacyUtils*/

ProfileInfoHover.Views.InfoWithTags = BaseView.extend({
    template: '#profile-info-hover-template',

    onUnsubscribeClicked: function () {
        var self = this;
        PrivacyUtils.unsubscribe(self.model).always(function (result) {
            if (result && result.success) {
                self.render();
            } else {
                workspace.appView.showMessageUnexpectedError();
            }
        });
    },

    onSubscribeClicked: function () {
        if (workspace.isAuthorized) {
            var self = this;
            PrivacyUtils.subscribe(self.model).always(function (result) {
                if (result && result.success) {
                    self.render();
                } else {
                    workspace.appView.showMessageUnexpectedError();
                }
            });
        }
    },

    onSendMessageClicked: function () {
        this.model.hideView();
        if (PrivacyUtils.mayISendYouMessage(this.model)) {
            workspace.appModel.get('dialogsController').showSendMessageDialog(this.model.get("profileId"));
        }
    }
});

ProfileInfoHover.Views.ForAdmin = BaseView.extend({
    className: "floatLeft",
    template: '#admin-online-profile-info-template'
});

ProfileInfoHover.Views.InfoWithActions = HoverInfoCard.Views.BaseView.extend({

    construct: function () {
        this.addChild(new ProfileInfoHover.Views.InfoWithTags({
            model: this.model
        }));
    }
});
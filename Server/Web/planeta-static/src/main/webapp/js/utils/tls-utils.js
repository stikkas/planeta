var TlsUtils = {
    tlsCheckUrl: 'https://widget.cloudpayments.ru/bundles/cloudpayments',

    // https://planeta.atlassian.net/browse/PLANETA-16956
    checkClientTlsSupport: function(model) {
        window.jQuery2.ajax({
            url: TlsUtils.tlsCheckUrl,
            type: 'GET',
            dataType: 'jsonp',
            error: function(xhr, status, error) {
                if (status === 'error') {
                    console.log(status);
                    model.set({tlsSupported: false}, {silent: true});
                }
            }
        });
    }
};
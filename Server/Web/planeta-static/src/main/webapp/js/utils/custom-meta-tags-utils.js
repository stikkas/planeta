/*globals _,$,workspace  */
/**
 * Created with IntelliJ IDEA.
 * User: sshendyapin
 * Date: 01.07.13
 * Time: 21:01
 * To change this template use File | Settings | File Templates.
 */
//если заходим на сайт первый раз, кастомные мета теги уже подтянулись в CustomMetaTagInterseptorЕ, поэтому не будем тянуть json лишний раз

var CustomMetaTagsUtils = {
    getCustomTagForThisPage: function () {
        if (CustomMetaTagsUtils.notFirstTime) {
            var tagPath = document.location.pathname;
            if (tagPath.indexOf('!comment') > 0) {
                tagPath = tagPath.replace(/!\S*/, '');
            }

            return Backbone.sync('read', null, {
                url: '/api/util/custom-meta-tag-for-page.json',
                data: {
                    customMetaTagType: workspace.appModel.attributes.projectType,
                    tagPath: tagPath
                }
            });
        }
        CustomMetaTagsUtils.notFirstTime = true;
        return new $.Deferred().resolve();
    }
};
var initDatePicker = function () {
    $.datepicker.regional['ru'] = {
        closeText:'Закрыть',
        prevText:'&larr;',
        nextText:'&rarr;',
        currentText:'Сегодня',
        monthNames:['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь',
            'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
        monthNamesShort:['Янв', 'Фев', 'Мар', 'Апр', 'Май', 'Июн',
            'Июл', 'Авг', 'Сен', 'Окт', 'Ноя', 'Дек'],
        dayNames:['воскресенье', 'понедельник', 'вторник', 'среда', 'четверг', 'пятница', 'суббота'],
        dayNamesShort:['вск', 'пнд', 'втр', 'срд', 'чтв', 'птн', 'сбт'],
        dayNamesMin:['ВС', 'ПН', 'ВТ', 'СР', 'ЧТ', 'ПТ', 'СБ'],
        weekHeader:'Не',
        dateFormat:'dd.mm.yy',
        firstDay:1,
        isRTL:false,
        showMonthAfterYear:false,
        yearSuffix:''
    };
    $.datepicker.setDefaults($.datepicker.regional['ru']);

    if ($.timepicker) {
        $.timepicker.regional['ru'] = {
            timeOnlyTitle: 'Выберите время',
            timeText: 'Время',
            hourText: 'Часы',
            minuteText: 'Минуты',
            secondText: 'Секунды',
            millisecText: 'миллисекунды',
            currentText: 'Сейчас',
            closeText: 'Готово',
            ampm: false
        };
        $.timepicker.setDefaults($.timepicker.regional['ru']);
    }
};

initDatePicker();
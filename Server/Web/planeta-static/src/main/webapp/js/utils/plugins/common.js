/**
 * Ru plural
 * ['1 предмет', '2 предмета', '5 предметов'];
 */
Number.prototype.pluralIndex = function() {
    var n = this.valueOf();
    return (n % 10 == 1 && n % 100 != 11 ? 0 : n % 10 >= 2 && n % 10 <= 4 && (n % 100 < 10 || n % 100 >= 20) ? 1 : 2);
};

/**
 * proxy for underscore methods for usage in jQuery.tmpl-templates
 */
var ArrayUtils = {
    length: function(arr) {
        return _.size(arr);
    },
    isEmpty: function(obj) {
        return _.isEmpty(obj);
    }
}

NewsLongTextShower = function(string, count) {
    if (string.length < count) {
        return string;
    }
    return string.substring(0, count) + '...';
}

Array.prototype.contains = function(element) {
    for (var i = 0; i < this.length; i++) {
        if (this[i] == element) {
            return true;
        }
    }
    return false;
}
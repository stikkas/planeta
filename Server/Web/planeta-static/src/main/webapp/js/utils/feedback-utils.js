/*global swfobject, Form, TemplateManager*/
var FeedbackHelper = {
    showFeedBackDialog: function (options) {
        var url = "/api/public/feedback.json";
        var data = {
            isAuthorized: workspace.isAuthorized
        };
        if (options && options.theme) {
            data.theme = options.theme;
        }
        FeedbackHelper.showDialog('feedback-form-template-login', url, data);
    },

    showDialog: function (templateName, url, data){
        var self = this;
        TemplateManager.fetchTemplate('#' + templateName).done(function () {
            self._showDialog(templateName, url, data);
        }).fail(function (e) {
            console.log("Error to load template");
            console.log(e);
        });
    },

    _showDialog: function (templateName, url, data) {
        var successMessage = 'Сообщение отправлено';
        $('#' + templateName).template(templateName);
        var fader = $("<div class='modal-backdrop fade in'></div>");
        var modalDialog = $('<div id="modal-dialog" class="modal-dialog"></div>');
        var modalDiv = $("<div class='modal modal-project-feedback'></div>");
        $.tmpl(templateName, data).appendTo(modalDiv);

        fader.appendTo("body");
        modalDialog.appendTo("body");
        modalDiv.appendTo("#modal-dialog");

        $(document).bind('keydown.feedback', function (e) {
            if (e.keyCode === $.ui.keyCode.ESCAPE) {
                e.preventDefault();
                FeedbackHelper.closeModal();
                $(document).unbind('keydown.feedback');
            }
        });

        $(".close, a[type=reset], button[type=reset]", modalDialog).click(function (e) {
            e.preventDefault();
            $(document).unbind('keydown.feedback');
            FeedbackHelper.closeModal();
        });
        var selectEl = $('selectCampaignById[name="feedback-theme-selectCampaignById"]', modalDialog);
        if (selectEl.length) {
            selectEl.dropDownSelect('defaultFunc', {addSelectClassesToDiv: true});
        }
        $("a[type=submit],button[type=submit]", modalDialog).click(function (e) {
            e.preventDefault();
            $(document).unbind('keydown.feedback');
            data.email = modalDialog.find("#email").val() || workspace.appModel.get('myProfile').get('email');
            data.message = modalDialog.find("#message").val();
            data.theme = modalDialog.find('select').attr('selected', 'selected').val();

            if (!data.hideAdditionalInfo) {
                data.additionalInfo = "\n\nТехническая информация";
                var flashInfo = null;
                var dataInfo = null;
                try {
                    flashInfo = swfobject.getFlashPlayerVersion();
                    var profile = workspace.appModel.get('myProfile');
                    dataInfo = {
                        profileId: profile.get("profileId"),
                        displayName: profile.get('displayName')
                    };
                } catch (exception) {

                }
                var additionalInfo = {
                    ua: navigator.userAgent,
                    href: document.location.href,
                    flash: flashInfo
                };
                $.each(additionalInfo, function (key, value) {
                    data.additionalInfo += "\n" + key + "  =  " + JSON.stringify(value);
                });
                if (dataInfo !== null) {
                    $.each(dataInfo, function (key, value) {
                        data.additionalInfo += "\n" + key + "  =  " + JSON.stringify(value);
                    });
                }
            }

            var phoneNumber = modalDialog.find("#phoneNumber");
            if (phoneNumber) {
                data.phoneNumber = phoneNumber.val();
            }
            var contacts = modalDialog.find("#contacts");
            if (contacts) {
                data.contacts = contacts.val();
            }
            $.ajax({
                type: 'post',
                url: url,
                data: data,
                success: function (response) {
                    $('.error').removeClass('error');
                    $('.error-message').remove();
                    if (response.success) {
                        FeedbackHelper.swapContentWithSuccess(modalDialog);
                    } else if (response.fieldErrors) {
                        $.map(response.fieldErrors, function (value, key) {
                            var el = modalDialog.find('#' + key);
                            if (el) {
                                el.parent().parent().addClass('error');
                                el.addClass('error').after(function () {
                                    return '<span class="help-inline error-message">' + value + '</span>';
                                });
                            }
                        });
                    }
                }
            });
        });
    },

    swapContentWithSuccess: function(modalDialog) {

        var find = modalDialog ? _.bind(modalDialog.find, modalDialog) : $;
        var wrap = find('.modal-project-feedback');
        var step1 = find('.modal-project-feedback-form');
        var step2 = find('.modal-feedback-success');

        if (step2.length) {
            wrap.css({
                height: wrap.height(),
                width: wrap.width()
            });

            if ($.support['transition']) {
                wrap.bind('transitionend', function() {
                    step2.removeClass('hide');
                    setTimeout(function() {
                        step2.addClass('in');
                    });
                });

                step1.bind('transitionend', function(e) {
                    e.stopPropagation();
                    step1.addClass('hide');
                    wrap.addClass('size-transition');
                    wrap.css({
                        height: step2.outerHeight(),
                        width: step2.outerWidth()
                    });
                });

                step1.removeClass('in');
            } else {
                step1.removeClass('in');
                step1.addClass('hide');

                wrap.css({
                    height: step2.outerHeight(),
                    width: step2.outerWidth()
                });

                step2.removeClass('hide');
                step2.addClass('in');
            }
        } else {
            FeedbackHelper.closeModal();
        }
    },

    closeModal: function () {
        $('#modal-dialog').remove();
        $(".modal-backdrop.fade.in").remove();
    },

    showAlert: function (message, template, timeout) {
        timeout = _.isUndefined(timeout) ? 5000 : timeout;

        var alertEl = $.tmpl(template, {
            messageHtml: message
        });
        $(alertEl).find(".close").click(function (e) {
            e.preventDefault();
            $(alertEl).remove();
        });
        if (timeout) {
            _.delay(function () {
                $(alertEl).remove();
            }, timeout);
        }
        $('body').prepend(alertEl);
    },

    showSuccessMessage: function (message, timeout) {
        FeedbackHelper.showAlert(message, $('#success-message-template').template(), timeout);
    }
};


(function ($) {

    var url = "/api/util/add-subscriber.json";
    var defaultMessage = "new.projects";

    /**
     * Sends feedback message from form with email-type input
     * @param {Object} options
     */
    $.fn.subscribeByEmail = function (options) {
        options = options || {};
        var defaultSelector = $(this).find('input');
        var selector = options.selector || defaultSelector;
        $(this).find('.btn-primary').click(function () {
            var email = $(selector).val();
            if (!Form.isValidEmail(email)) {
                workspace.appView.showErrorMessage('Вы указали неправильный email');
                return;
            }

            var message = options.message || defaultMessage;
            var data = {
                email: email,
                subscriptionCode: message
            };
            if (options.form) {
                data = _.extend($(options.form).serialize(), data);
            }

            $.ajax({
                type: 'post',
                url: url,
                data: data,
                success: function (response) {
                    if (response.success === false) {
                        workspace.appView.showErrorMessage('Вы указали неправильный email');
                    } else {
                        workspace.appView.showSuccessMessage('Спасибо за подписку!');
                    }
                }
            });
        });
    };
}(jQuery));


/**
 * Created by asavan on 02.04.2017.
 */

(function () {
    function makeOffline(element, index, array) {
        var oldHref = element.href;
        if (oldHref.indexOf('offline') > 0) {
            return;
        }
        var newHref = oldHref.replace("favicons/favicon", "favicons/favicon-offline");
        element.href = newHref;
    }

    function makeOnline(element, index, array) {
        var oldHref = element.href;
        var newHref = oldHref.replace("favicons/favicon-offline", "favicons/favicon");
        element.href = newHref;
    }


    function updateStatus(event) {
        try {
            var fav = document.querySelectorAll('link[rel~="icon"]');
            if (navigator.onLine) {
                fav.forEach(makeOnline);
            } else {
                fav.forEach(makeOffline);
            }
        } catch (ignored) {

        }

    }


    try {
        window.addEventListener('load', function () {
            window.addEventListener('online', updateStatus);
            window.addEventListener('offline', updateStatus);
        });
    } catch (ignored) {

    }

})();

var StringUtils = {

    /**
     * Extracts domain name from the string
     * @param line
     */
    extractHost: function (url) {
        if (!url || !_.isString(url)) return null;

        var startIndex = url.indexOf('//');
        if (startIndex < 0) {
            return null;
        }
        var endIndex = url.indexOf('/', startIndex + 2);
        if (endIndex < 0) {
            return url.substring(startIndex + 2);
        }
        var host = url.substring(startIndex + 2, endIndex);
        return host;
    },

    /**
     * Extracts parameter value from query string or empty string if not found.<br/>
     * Example: http://domain.com/index.html?var=value ==> value<br/>
     */
    extractQueryStringParamValue: function (queryString, parameter) {
        parameter = parameter.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
        var regexS = "[\\?&]" + parameter + "=([^&#]*)";
        var regex = new RegExp(regexS);
        var results = regex.exec(queryString);
        if (results == null) {
            return "";
        } else {
            return decodeURIComponent(results[1].replace(/\+/g, " "));
        }
    },

    addParamToUrl: function (url, param, value, remove) {
        var hashPos = url.indexOf("#");
        if (hashPos < 0) {
            hashPos = url.length;
        }

        var questPos = url.indexOf('?');
        if (questPos < 0) {
            questPos = hashPos;

        }
        var params = url.substring(questPos + 1, hashPos);
        if (params) {
            params = ("&" + params).replace(new RegExp("&" + param + "=[^&]*"), "");
        }
        if (remove) {
            if (params && params[0] == '&') {
                params = params.substr(1);
            }
        } else {
            params = param + "=" + value + params;
        }
        return url.substring(0, questPos) + (params ? "?" + params : "") + url.substring(hashPos);
    },

    removeParamFromUrl: function (url, param) {
        return this.addParamToUrl(url, param, null, true);
    },

    declOfNum: function (number, titles) {
        if (!titles || !titles.length || titles.length < 3) return '';
        var cases = [2, 0, 1, 1, 1, 2];
        return titles[(number % 100 > 4 && number % 100 < 20) ? 2 : cases[(number % 10 < 5) ? number % 10 : 5]];
    },

    declOfNumWithNum: function (number, titles) {
        if (number === 0) {// do nothing
        } else if (!number) {
            return '';
        }
        var title = this.declOfNum(number, titles);
        return number + (title ? ' ' + title : '');
    },

    declOfNumWithNumAndBracket: function (number, titles) {
        if (!number) return '';
        return '(' + this.declOfNumWithNum(number) + ')';
    },

    numWithSignAndBracket: function (number) {
        if (!number) return '';
        var result = '(';
        if (number > 0) result += '+';
        return result + number + ')';
    },

    pluralOfNum: function (number, titles) {
        return number <= 1 ? titles[0] : titles[1];
    },
    
    pluralOfNumWithNum: function (number, titles) {
        if (number === 0) {// do nothing
        } else if (!number) {
            return '';
        }
        var title = this.pluralOfNum(number, titles);
        return number + (title ? ' ' + title : '');
    },

    humanDuration: function (totalSec) {
        if (typeof totalSec == 'undefined') {
            return '';
        }
        //if duration already formatted
        if (typeof totalSec == 'string' && totalSec.indexOf(':') > 0) {
            return totalSec;
        }
        var duration = '';
        var totalSecInt = parseInt(totalSec, 10);
        if (totalSecInt < 0) {
            totalSecInt = -totalSecInt;
            duration = '-';
        }
        var hours = parseInt(totalSecInt / 3600, 10) % 24;
        var minutes = parseInt(totalSecInt / 60, 10) % 60;
        var seconds = parseInt(totalSecInt % 60, 10);


        if (hours) {
            duration += hours + ':';
        }
        duration += ((hours && minutes < 10) ? '0' : '') + minutes;
        duration += ':' + (seconds < 10 ? '0' : '') + seconds;
        return duration;
    },

    humanFileSize: function (totalBytes) {
        var units = ['Б', 'Кб', 'Мб', 'Гб', 'Тб', 'Пб'];
        var i = 0;
        var size = totalBytes;
        while (size >= 1024) {
            size /= 1024;
            ++i;
        }
        var isInt = size % 1 == 0;
        return size.toFixed(i > 1 && !isInt ? 2 : 0) + ' ' + units[i];
    },

    cutString: function (str, length, escape) {
        if (str == null) {
            return '';
        }
        if (!str) {
            return str;
        }
        if (escape) {
            str = str.replace(/<(?!(\s*br\s*\/?\s*>))[^>]*>/g, '');
        }
        if (str.length < length) {
            return str;
        }
        var lastSpace = Math.max(str.lastIndexOf(' ', length), str.lastIndexOf('&nbsp;', length));
        return str.substring(0, lastSpace > 0 ? lastSpace : length - 1) + '\u2026';
    },


    cutHtml: function (str, length) {
        if (!str || str.length < length) {
            return str;
        }
        if (str.indexOf('<') < 0 || str.indexOf('<') > length) {
            return StringUtils.cutString(str, length);
        }

        _cut = function (el, len) {
            var curLen = 0, restLen = 0;
            el.contents().each(function () {
                var $this = $(this);
                if (curLen > len) {
                    $this.remove();
                } else {
                    restLen = len - curLen;
                    curLen += $this.text().length;
                }
            });
            var cont = el.contents();
            if (cont.length === 0) {
                if (el[0].nodeType === 3) {
                    el[0].nodeValue = el.text().substring(0, len) + '\u2026';
                }
            } else if (curLen >= len) {
                _cut($(cont[cont.length - 1]), restLen);
            }
        };

        var el = $("<div>" + str + "</div>");
        _cut(el, length);
        return el[0].innerHTML;

    },

    tidyHtml: function (html) {
        return $('<div></div>').html(html || '').html();
    },

    getShortDialogMessage: function (html, length) {

        if (!html) {
            return '';
        }

        if (html.indexOf('.showVideo') >= 0 || html.indexOf('workspace.showExternal') >= 0 || html.indexOf('<p:video') >= 0) {
            return '<div class="video">Видео</div>';
        }

        if (html.indexOf('playTrack') >= 0 || html.indexOf('<p:audio') >= 0) {
            return '<div class="audio">Аудио</div>';
        }

        if (html.indexOf('<img') >= 0 || html.indexOf('<p:photo') >= 0) {
            return '<div class="photo">Фотография</div>';
        }

        return StringUtils.cutString(html, length);
    },

    humanNumber: function (amount) {
        return ('' + amount).replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ');
    },

    hashEscape: function (param) {
        return param.replace(/\./g, '%2E').replace(/\//g, '%2F').replace(/%/g, '.');
    },

    hashUnescape: function (value) {
        try {
            return decodeURIComponent(value.replace(/\+/g, ' ').replace(/\./g, '%'));
        } catch (e) {
            return value;
        }
    },

    replace: function (str, pattern, replacement) {
        if (typeof(str) == 'string') {
            return str.replace(new RegExp(pattern, 'gi'), replacement);
        }
        return '';
    },

    shrinkToLength: function (str, length) {
        if (!str) {
            return "";
        }
        if (!length)
            length = 64;

        if (str.length > length) {
            str = str.substring(0, length - 1);
            str += '…';
        }

        return str;
    },

    isTrimmed: function (text) {
        return text && text.indexOf('…') == text.length - 1;
    },

    endsWith: function (str, suffix) {
        if (!str || !suffix || !_.isString(str) || !_.isString(suffix)) return false;
        return str.indexOf(suffix) + suffix.length === str.length;
    },

    startsWith: function (str, prefix) {
        if (!str || !prefix || !_.isString(str) || !_.isString(prefix)) return false;
        return str.indexOf(prefix) === 0;
    },

    plurals: function (n, strings) {
        var i;
        if (n == 0) {
            i = 0;
        } else {
            n = Math.abs(n) % 100;
            var n1 = n % 10;
            if (n > 10 && n < 20) {
                i = 0;
            } else if (n1 > 1 && n1 < 5) {
                i = 2;
            } else if (n1 == 1) {
                i = 1;
            } else {
                i = 0;
            }
        }
        return strings[i];
    },

    htmlDecode: function (input) {
        return $('<div/>').html(input).text();
    },

    trimEmptyLine: function (text) {
        var result = text, pos;
        while (true) {
            pos = result.indexOf('\n');
            if (pos < 0 || $.trim(StringUtils.htmlDecode(result.substring(0, pos)))) {
                break;
            }
            if (result.charAt(pos + 1) === '\r') {
                pos++;
            }
            result = result.substring(pos + 1);
        }

        while (true) {
            pos = result.lastIndexOf('\n');
            if (pos < 0 || $.trim(StringUtils.htmlDecode(result.substring(pos)))) {
                break;
            }
            result = result.substring(0, pos);
        }
        return result;
    },

    makeBrs: function (text) {
        text = text || '';
        var result = '';
        for (var i = 0; i < text.length; ++i) {
            if (text[i] == '\n') {
                result += '<br>';
            } else if (text[i] == '\r') {
                //do nothing
            } else {
                result += text[i];
            }
        }
        return result;
    },
    escapeHtml: function (text) {
        return $('<div></div>').text(text).html();
    },
    escape: function (text) {
        if (!text) {
            return text;
        }
        var out = '';
        for (var i = 0; i < text.length; ++i) {
            var c = text.charAt(i);
            switch (c) {
                case '=':
                    out += '%3D';
                    break;
                case '?':
                    out += '%3F';
                    break;
                case '&':
                    out += '%26';
                    break;
                default:
                    out += c;
            }
        }
        return out;
    },

    unescape: function (text) {
        if (!text) {
            return text;
        }
        return $('<textarea>' + text + '</textarea>').val();
    },

    capitalize: function (str) {
        if (!str) {
            return str;
        }
        var capitalizeWord = function (word) {
            return word.charAt(0).toUpperCase() + word.substr(1).toLowerCase();
        };
        if (str.indexOf(' ') == -1) {
            return capitalizeWord(str);
        }
        return str.replace(/\w\S*/g, capitalizeWord);
    },

    getSupportString: function (userGender) {
        if(workspace && workspace.currentLanguage && workspace.currentLanguage == 'en') {
            return "Sponsored";
        } else {
            if (userGender && userGender == "FEMALE") {
                return "Поддержала";
            }
            return "Поддержал";
        }
    },

    hyphenate: function (view) {
        moduleLoader.loadModule('hyphenate').done(function () {
            Hyphenator.hyphenateView(view);
        });
    },

    searchSnippet: function (rawTextHtml, maxGapBetweenHighlights, totalMaxLength) {
        var textHtml = rawTextHtml.replace(/(?:\r\n|\r|\n)/ig, ' ').replace(/\s+/, ' ');
        if (!/</.test(textHtml)) {
            if (textHtml.length <= totalMaxLength) {
                return textHtml;
            }
            return textHtml.substr(0, totalMaxLength) + '&#8230';
        }
        var headRegexp = new RegExp('^[^>]*\\s(\\S*?[^>]{' + maxGapBetweenHighlights + '})<', 'ig');
        var tailRegexp = new RegExp('(>[^<]{' + maxGapBetweenHighlights + '}\\S*?)\\s[^<]*$', 'ig');
        var middleRegexp = new RegExp('(>[^<]{' + maxGapBetweenHighlights + '}\\S*?)\\s[^<]*\\s(\\S*?[^<]{' + maxGapBetweenHighlights + '}<)', 'ig');

        textHtml = textHtml.replace(headRegexp, '&#8230 $1<');
        textHtml = textHtml.replace(middleRegexp, '$1 &#8230 $2');
        textHtml = textHtml.replace(tailRegexp, '$1 &#8230');
        if (textHtml.length <= totalMaxLength) {
            return textHtml;
        }
        return textHtml.substr(0, totalMaxLength) + '&#8230';
    },

    stripHtmlTags: function (html) {
        if (!html || html == "null") return "";
        var tmp = document.createElement("div");
        tmp.innerHTML = html;
        return tmp.textContent || tmp.innerText || "";
    },

    notMore: function (num, maxNum) {
        return num > maxNum ? maxNum + "+" : num;
    },
    isAbsoluteUrl: function (path) {
        return !!(path.indexOf('http://') == 0 || path.indexOf('https://') == 0 || path.indexOf('//') == 0);
    },
    replaceHttpToHttps: function (url) {
        if (url && (url.indexOf('http://') >= 0)) {
            url = url.replace("http://", "https://");
        }
        return url;
    },
    getAbsoluteUrl: function (path) {
        if (path.indexOf('http://') == 0) {
            path = path.replace('http://', '')
        } else if (path.indexOf('https://') == 0) {
            path = path.replace('https://', '')
        } else if (path.indexOf('//') == 0) {
            path = path.replace('//', '')
        }
        if (window.location.href.indexOf('localhost') >= 0) {
            return 'http://' + path;
        }
        return 'https://' + path;
    },
    getOrdinalSuffix: function(i) {
        var j = i % 10,
            k = i % 100;
        if (j == 1 && k != 11) {
            return i + "st";
        }
        if (j == 2 && k != 12) {
            return i + "nd";
        }
        if (j == 3 && k != 13) {
            return i + "rd";
        }
        return i + "th";
    },
    randomColor: function () {
        var r = Math.floor(Math.random() * (256));
        var g = Math.floor(Math.random() * (256));
        var b = Math.floor(Math.random() * (256));
        return 'rgb(' + r + ', ' + g + ', ' + b + ')';
    }
};
var inputCurrencyMask = {
    selectUtils: function () {
        if ( !$.fn.getCursorPosition ) {
            $.fn.getCursorPosition = function () {
                var input = this.get(0);
                if (!input) return; // No (input) element found
                if ('selectionStart' in input) {
                    // Standard-compliant browsers
                    return input.selectionStart;
                } else if (document.selection) {
                    // IE
                    input.focus();
                    var sel = document.selection.createRange();
                    var selLen = document.selection.createRange().text.length;
                    sel.moveStart('character', -input.value.length);
                    return sel.text.length - selLen;
                }
            };
        }

        if ( !$.fn.selectRange ) {
            $.fn.selectRange = function (start, end) {
                if (end === undefined) {
                    end = start;
                }
                return this.each(function () {
                    if ('selectionStart' in this) {
                        this.selectionStart = start;
                        this.selectionEnd = end;
                    } else if (this.setSelectionRange) {
                        this.setSelectionRange(start, end);
                    } else if (this.createTextRange) {
                        var range = this.createTextRange();
                        range.collapse(true);
                        range.moveEnd('character', end);
                        range.moveStart('character', start);
                        range.select();
                    }
                });
            };
        }
    },
    init: function (el) {
        this.selectUtils();

        var prefix = el.data('prefix') || '';
        var suffix = el.data('suffix') || ' ₽';
        el.inputmask({
            showMaskOnHover: false,
            rightAlign: false,
            placeholder: '',
            alias: 'numeric',
            digits: 0,
            groupSeparator: ' ',
            autoGroup: true,
            prefix: prefix,
            suffix: suffix,
            autoUnmask: true,
            allowPlus: false,
            allowMinus: false,
            removeMaskOnSubmit: true
        });


        el.on('click keyup touchend focus', function () {
            (function (el) {
                setTimeout(function () {
                    var value = $(el).val();
                    var valueLen = value.length;
                    valueLen = valueLen + Math.floor(Math.max(0, valueLen - 1) / 3) + prefix.length + suffix.length;
                    var suffixPos = valueLen - suffix.length;
                    suffixPos = valueLen == 0 ? 0 : suffixPos;
                    if ($(el).getCursorPosition() >= suffixPos) {
                        $(el).selectRange(suffixPos);
                    } else if ($(el).getCursorPosition() < prefix.length) {
                        $(el).selectRange(prefix.length);
                    }
                });
            })(this);
        });
    }
};
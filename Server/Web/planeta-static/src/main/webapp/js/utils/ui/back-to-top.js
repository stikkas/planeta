/**
 * Created with IntelliJ IDEA.
 * User: s.fionov
 * Date: 15.08.12
 */
$.backToTop = function (options) {
    if (options === undefined) {
        options = {};
    }

    var Top = function () {
        var self = this;
        var lastPosition = 0;
        var contentTop;
        var opacity = 1.0; // current button opacity (calculated from scroll position)
        var lastOpacity = -1;
        var down = false; // down state
        var fixed = false; // button fixed on top
        var changed = false; // down state changed
        var animatedSegment = 50; // size of the animated segment

        var expanded = null; // side button mode
        var minScrollTop = options.minScrollTop || 500;

        var wrap = $(options.wrap || '<div></div>');
        var phrase;
        if(workspace && workspace.currentLanguage && workspace.currentLanguage === 'en') {
            phrase = 'Back to top';
        } else {
            phrase = 'Вернуться наверх';
        }
        var content = $(options.content
        || '<div class="top-button-inner"><div class="latest-news hide"></div><div>'
        + '    <div class="top-button-cont"><b class="top-button-arrow"></b><span class="top-button-text">' + phrase + ' </span></div>'
        + '</div></div>');
        var contentClass = options.contentClass || 'top-button tb-in';
        var contentClassExpanded = options.contentClassExpanded || 'top-button tb-out';

        var parent = options.parent || '#left-sidebar.span4';
        var parentExpanded = options.parentExpanded || 'body';

        var wrapClass = options.wrapClass || 'top-button-wrap';
        var wrapExpandedClass = options.wrapExpandedClass || 'tbw-out';

        var expandedWidth = options.expandedWidth || 1640;

        var animationIsRunning = false;

        var fade = function () {
            $(content).stop();
            if (changed && !expanded) {
                if ($(content).is(':animated')) return;
                $(content).fadeOut('fast', function () {
                    changed = false;
                    fade();
                });
                return;
            }
            if (isDown()) {
                $(content).addClass('down');
            } else {
                $(content).removeClass('down');
            }
            if (fixed) {
                $(wrap).addClass('fixed');
            } else {
                $(wrap).removeClass('fixed');
            }

            // calculate target opacity
            var targetOpacity = expanded ? (isDown() ? 1.0 - opacity : opacity) : (isDown() ? 0 : opacity);
            if (targetOpacity !== lastOpacity) {
                $(content).css('opacity', targetOpacity).show().end();
            }
            lastOpacity = targetOpacity;

            if (targetOpacity < 0.05) {
                $(content).hide();
            }
        };

        var isDown = function () {
            return down;
        };

        var setDown = function (value) {
            if (down !== value) {
                changed = true;
            }
            down = value;
        };

        var updateState = function () {
            animationIsRunning = false;
            var currentPosition = $(document).scrollTop();
            var totalHeight = $(document).height();
            var visibleHeight = $(window).height();
            contentTop = (!expanded) ? $(wrap).offset().top : contentTop;
            fixed = (currentPosition >= contentTop);
            opacity = Math.min(1.0, currentPosition / minScrollTop);
            setDown(down && currentPosition < lastPosition && currentPosition < minScrollTop);
        };

        var redraw = function () {
            updateState();
            fade();
        };

        var scroll = function () {
            redraw();
        };

        var setExpanded = function (value) {
            if (expanded !== value) {
                expanded = value;
                var newParent;
                if (expanded) {
                    $(wrap).addClass(wrapExpandedClass);
                    $(content).children().removeClass(contentClass).addClass(contentClassExpanded);
                    newParent = parentExpanded;
                } else {
                    $(wrap).removeClass(wrapExpandedClass);
                    $(content).children().removeClass(contentClassExpanded).addClass(contentClass);
                    newParent = parent;
                }

                if ($(newParent).size() > 0) {
                    $(newParent).append(wrap);
                } else {
                    $('<div></div>').append(wrap);
                }
            }
        };

        var resize = function () {
            if ($(window).width() > expandedWidth) {
                setExpanded(true);
            } else {
                setExpanded(false);
            }
            redraw();
        };

        var click = function (event, forceExpanded) {
            if (animationIsRunning) {
                return;
            }
            setTimeout(function () {
                animationIsRunning = false;
            }, 1000);
            animationIsRunning = true;
            if (isDown() || forceExpanded) {
                if (lastPosition > animatedSegment) $('body,html').scrollTop(lastPosition - animatedSegment);
                $('body,html').animate({scrollTop: lastPosition}, 'fast', null, function () {
                    setDown(false);
                    redraw();
                });
            } else {
                lastPosition = $(document).scrollTop();
                if (lastPosition > animatedSegment) $('body,html').scrollTop(animatedSegment);
                $('body,html').animate({scrollTop: 0}, 'fast', null, function () {
                    setDown(expanded);
                    redraw()
                });
            }
        };

        var start = function () {
            $(content).addClass(contentClass);
            $(content).css('opacity', 0);
            $(wrap).addClass(wrapClass);
            $(wrap).append(content);

            $(content).click(click);
            $(window).resize(resize);
            $(window).scroll(scroll);
            resize();

            self.topClick = click;

            $(window).data('top', self);
        };

        var stop = function () {
            content.remove();
            wrap.remove();
            $(window).unbind('resize', resize);
            $(window).unbind('scroll', scroll);
            $(window).data('top', null);
        };

        this.wrap = $(wrap);
        this.content = $(content);
        this.start = start;
        this.stop = stop;

        start();
    };

    if ($(window).data('top')) {
        $(window).data('top').stop();
    }

    var $top = new Top();
    return $top.wrap;
};

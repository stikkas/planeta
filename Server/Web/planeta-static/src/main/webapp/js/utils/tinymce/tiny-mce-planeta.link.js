TinyMcePlaneta.createButtonPlugin("PlanetaLink", function(self) {

    this.maxAnchorLength = 50;

    self.editor.addCommand(self.name, function() {
        var sel = self.editor.selection.getContent();
        var enteredUrl = null;
        var node = self.editor.selection.getNode();
        if($(node).is('a')){
            enteredUrl = $(node).text();
        }
        var rng = self.editor.selection.getRng();
        Attach.showEditorLink(function(response) {
            var linkUrl = response.replace(/\[url="?(.*?)"?\].*/, '$1');
//            // pass url through uri object to fix any rubbish
//            var tmpAnchor = document.createElement('a');
//            tmpAnchor.href = linkUrl;
//            linkUrl = tmpAnchor.href;
            if(!sel && $(node).is(':not(a)')){
                if(linkUrl.length > self.maxAnchorLength)
                    sel = linkUrl.slice(0,self.maxAnchorLength) + '...';
                else
                    sel = linkUrl;
            }
            var anchorHtml = '<a href="'+ linkUrl +'" >' + sel + '</a>';

            if($(node).is('a')){
                $(node).attr('href', linkUrl);
                $(node).attr('data-mce-href', linkUrl);
            } else {
                self.editor.selection.setRng(rng);
                self.editor.execCommand('mceInsertContent', 0, anchorHtml);
            }
        }, enteredUrl);
    });
}, 'a');

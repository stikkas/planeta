/**
 * Head file for tiny-mce-planeta plugins,
 * must be after tiny-mce core but before other tiny-mce-planeta files in javascript.properties
 */
var TinyMcePlaneta = function () {

    /**@private*/ var editorSkinDirectory = "/js/utils/tinymce/planeta";
    /**@private*/ var baseUrl = "/js/lib/tiny_mce";
    /**@private*/ var contentCss = "/css-generated/common.css";

    tinymce.onAddEditor.add(function (tiny, ed) {
        TemplateManager.tmpl("#tiny-mce-resize-frame-wrapper").fail(function () {
            throw "tiny-mce templates load error";
        });
        /**
         * change editor layout with defined styles
         */
        ed.onInit.add(function (ed) {

            // TODO move content.css to less ?
            ed.dom.loadCSS(workspace.staticNodesService.getResourceUrl(editorSkinDirectory + "/content.css"));
            ed.dom.loadCSS(workspace.staticNodesService.getResourceUrl(contentCss));
            // TODO how to remove this quirk ?
            $('iframe, #blog-post-preview').css({
                'overflow-x': 'hidden',
                'overflow-y': 'scroll'
            });
            tinymce.dom.Event.add(ed.getWin(), 'focus', function (e) {
                $(ed.getContainer()).addClass('input-focus');
            });
            tinymce.dom.Event.add(ed.getWin(), 'blur', function (e) {
                $(ed.getContainer()).removeClass('input-focus');
            });
        });

        ed.onExecCommand.add(function () {
            var $toSelect = $(ed.getBody()).find('p.planeta-selection');
            if ($toSelect.length > 0) {
                setTimeout(function () {
                    ed.selection.select($toSelect[0].childNodes[0]);
                    ed.selection.collapse();
                    $toSelect.removeAttr('class').empty();

                }, 10);

            }
            $(ed.getBody()).find('#tinymce-body_formatselect_text')
        });

        ed.onNodeChange.add(
            function (ed, evt) {
                if ( ed.selection.getNode().nodeName === 'LI' && ed.selection.getNode().innerHTML.match(/<br[\s\/]*>\s*$/) === null ) {
                    var node = ed.selection.getNode();
                    ed.selection.setContent('<span id="__caret__" />', {
                        format: 'raw'
                    });
                    node.innerHTML = node.innerHTML + '<br>';
                    var caret = ed.dom.get('__caret__');
                    ed.selection.select(caret);
                    ed.selection.collapse();
                    ed.dom.remove(caret);
                }
            }
        );

        ed.onKeyPress.add(
                function (ed, evt) {
                    if ((evt.shiftKey && evt.keyCode == 13) || (evt.keyCode == 13)) {
                        if ( evt.shiftKey && ed.selection.getNode().nodeName == 'LI' ) {
                            ed.selection.setContent('<br id="__br-temp__" />', {
                                format: 'raw'
                            });
                            var n = ed.dom.get('__br-temp__');
                            n.removeAttribute('id');
                            ed.selection.select(n);
                            ed.selection.collapse();
                        }
                        var br = $("br", ed.getContainer());
                        for (i = 0; i < br.length; i++) {
                            p = document.createElement("p");
                            br[i].parentNode.replaceChild(p, br[i]);
                        }
                    }
                }
        );

//        ed.onPostRender.add(function (ed) {
//            $(ed.getBody()).find("div").
//        });
    });


    /**
     * Activates/deactivates button (toggles active class on button icon)
     * @param {tinymce.Editor} editor - current editor
     * @param {String} buttonName
     * @param {Boolean} isActive - activate or deactivate button
     */
    var setButtonActive = function (editor, buttonName, isActive) {
        editor.controlManager.setActive(buttonName, isActive);
    };

    var _getDefaultConfigurationByName = function (name) {
        var conf = _.clone(name);
        conf.language = window.workspace && (workspace.currentLanguage === "en") ? "en" : "ru";
        return conf;
    };

    return {
        /**
         * Creates tinymce some-html-inserting plugin with button on control panel.
         * @param {String} pluginName - lowercase name for button and plugin
         * @param {Function} initFunc - function to execute once (on plugin initialization)
         * @param {String} objectSelector - jquery selector to choose this plugin object in tinymce textarea
         */
        createButtonPlugin: function (pluginName, initFunc, objectSelector) {
            var lcName = pluginName.toLowerCase();
            tinymce.PluginManager.requireLangPack(lcName);

            var namespaceName = pluginName + "Plugin";
            tinymce.create("tinymce.plugins." + namespaceName, {
                /**
                 * Initialization function
                 * @param {tinymce.Editor} ed - current editor
                 * @param {String} url - plugin url, currently unused
                 */
                init: function (ed, url) {
                    var clsName = 'mce' + lcName;
                    this.name = lcName;
                    this.clsName = clsName;
                    this.editor = ed;
                    this.selector = objectSelector;
                    var self = this;
                    initFunc.apply(this, [self]);

                    /**
                     * @param {jQuery} $el
                     * @return {Boolean}
                     */
                    var isActive = function ($el) {
                        return self.selector && $el.is(self.selector)
                                || !self.selector && $el.is('img.' + self.clsName);
                    };

                    // create button on control panel, don't forget to add description in ru.js file
                    ed.addButton(lcName, {
                        title: lcName + ".desc",
                        cmd: lcName
                    });
                    // set button active on object selectCampaignById
                    ed.onNodeChange.add(function (ed, cm, node) {
                        /**
                         * @type {jQuery}
                         */
                        var $node = $(node);
                        setButtonActive(ed, self.name, isActive($node));
                    });
                    ed.onClick.addToTop(function collapseObjectSelection(ed, e) {
                        if (isActive($(e.target))) {
                            ed.selection.collapse();
                        }
                    });
                }
            });
            tinymce.PluginManager.add(lcName, tinymce.plugins[namespaceName]);
        },
        /**
         * Creates module-like plugin to tinymce. It can be accessed with tinymce.plugins.moduleName after initialization.
         * Doesn't require language pack for execution.
         * @param {String} moduleName
         * @param {Function} initFunc - is called once on module initialization
         */
        createModule: function (moduleName, initFunc) {
            var lcName = moduleName.toLowerCase();
            var namespaceName = moduleName + "Plugin";
            tinymce.create("tinymce.plugins." + namespaceName, {
                /**
                 * Initialization function
                 * @param {tinymce.Editor} ed - current editor
                 */
                init: function (ed) {
                    var clsName = 'mce' + lcName;
                    this.name = lcName;
                    this.clsName = clsName;
                    this.editor = ed;
                    var self = this;
                    initFunc.apply(this, [self]);
                }
            });
            tinymce.PluginManager.add(lcName, tinymce.plugins[namespaceName]);
        },
        /**
         * (re)initializes tinymce editor using specified configuration
         * @param {Object} editorConfiguration - native tinymce options mixed with additional options
         */
        createBlogEditor: function (editorConfiguration) {
            if (tinymce.activeEditor && editorConfiguration.removeOnCreateNew !== false) {
                tinymce.EditorManager.remove(tinymce.activeEditor);
//                tinymce.EditorManager.execCommand('mceRemoveControl',true, tinymce.activeEditor.editorId);
//                tinymce.EditorManager.execCommand('mceAddControl',true, editorId);
            }

            // redefine additional parameters
            editorSkinDirectory = editorConfiguration.editorSkinDirectory || editorSkinDirectory;
            contentCss = editorConfiguration.contentCss || contentCss;
            baseUrl = editorConfiguration.baseUrl || baseUrl;

            editorConfiguration.editor_css = workspace.staticNodesService.getResourceUrl(editorSkinDirectory + "/ui.css");

            tinyMCE.baseURL = workspace.staticNodesService.getResourceUrl(baseUrl);
            try {
                tinyMCE.init(editorConfiguration);
            } catch (e) {
                console.log("tinymce init failed: " + e);
                document.location.reload();
            }
        },
        /**
         * Get mapped html of specified editor
         * @param {tinymce.Editor} editor
         * @return {String} - mapped html
         */
        mappedHtml: function (editor) {
            if (!editor || editor.getBody() == null) {
                return "";
            }
            $(editor.getBody()).click();  // to remove top-layer menus from editor body
            var html = (editor.serializer.serialize(editor.getBody()));//.replace(/\n/ig, '');
            // quirk to remove growing space before divs-images
            html = html.replace(/<p>\s*<div/ig, "<div").replace(/<\/div>\s*<\/p>/, "</div>");
            html = $.trim(TinyMcePlaneta.Formatting.clearStyles(html));
            // quirk to create rich-media objects
            var jTmpEl = $(document.createElement('div'));
            jTmpEl.html(html);
            TinyMcePlaneta.Formatting.createRichMediaTags(jTmpEl);
            jTmpEl.find('p').filter(function () {
                return TinyMcePlaneta.Formatting.isEmpty($(this));
            }).replaceWith('<p>&nbsp;</p>');
            jTmpEl.find(':not(p,ul,ol,li):has(br)').replaceWith('<p>&nbsp;</p>');
            TinyMcePlaneta.Formatting.removeBrs(jTmpEl);
            html = jTmpEl.html();
            html = TinyMcePlaneta.Formatting.trim(html);
            return html.replace(/((?:\s*<p\s*>(?:\s|&nbsp;)*<\/p\s*>\s*)|(?:\s*<br\/\s*>\s*)){0,10}$/ig, "");

        },
        /**
         * Get text-only content of editable area
         * @param {tinymce.Editor} editor
         * @return {String} text with spaces instead of newlines (<br/>'s)
         */
        nonMappedText: function (editor) {
            $(editor.getBody()).click();  // to remove top-layer menus from editor body
            var bodyClone = $(editor.getBody()).clone();
            bodyClone.find('p').append('<span> </span>');
            return bodyClone.text();
        },
        textClean: function (editor) {
            $(editor.getBody()).click();  // to remove top-layer menus from editor body
            var bodyClone = $(editor.getBody()).clone();
            bodyClone.find('.mcePaste').remove();
            bodyClone.find('p:not(:empty),li:not(:empty)').append('<span class="row-detect-span"> </span>');
            bodyClone.find('li').each(function () {
                $(this).find('> br:not(:first)').before('<span class="row-detect-span"> </span>');
            });
            bodyClone.find('.row-detect-span:last').remove();
            return bodyClone.text();
        },
        getDefaultConfiguration: function () {
            return _getDefaultConfigurationByName(TinyMcePlaneta.defaultConfiguration);
        },
        getCampaignEditorConfiguration: function () {
            return _getDefaultConfigurationByName(TinyMcePlaneta.campaignEditorConfiguration);
        },
        getCampaignEditorShareDescriptionConfiguration: function () {
            return _getDefaultConfigurationByName(TinyMcePlaneta.campaignEditorShareDescriptionConfiguration);
        },
        getQuizEditorConfiguration: function () {
            return _getDefaultConfigurationByName(TinyMcePlaneta.quizEditorConfiguration);
        },
        getFaqEditorConfiguration: function () {
            return _getDefaultConfigurationByName(TinyMcePlaneta.faqEditorConfiguration);
        },
        getNewsPostEditorConfiguration: function () {
            return _getDefaultConfigurationByName(TinyMcePlaneta.newsPostEditorConfiguration);
        },
        getNewsPostEditorConfiguration710: function () {
            return _getDefaultConfigurationByName(TinyMcePlaneta.newsPostEditorConfiguration710);
        },
        getNewsPostEditorConfiguration480: function () {
            return _getDefaultConfigurationByName(TinyMcePlaneta.newsPostEditorConfiguration480);
        },
        getUserSettingsSummaryEditorConfiguration: function () {
            return _getDefaultConfigurationByName(TinyMcePlaneta.userSettingsSummaryEditorConfiguration);
        },
        serializeData: function (data) {
            var ret = [];
            $.each(data, function (key, value) {
                if (value) {
                    ret.push(encodeURIComponent(key) + "=" + encodeURIComponent(value));
                }
            });
            return ret.join("&");
        },
        insertHtml: function (ed, html) {
            ed.execCommand('mceInsertContent', 0, html + '<p class="planeta-selection">&nbsp;</p>');
        }
    }
}();
/**
 * Default configuration to substitute to createBlogEditor
 * @type {Object}
 */
TinyMcePlaneta.defaultConfiguration = {
    // translations are preloaded with utils/tinymce/ru.js
    // instead of loading separate file for each plugin
    language: "ru",
    language_load: false,
    // exact mode may be important: if you use textarea instead of div, some html entities can change after server-side clean
    mode: "exact",
    theme: "advanced",
    // see editorSkinDirectory
    skin: "planeta",
    // to prevent default content.css load from themes/advanced/skins...
    content_css: false,
    // replaces default themes/advanced/skins/planeta/ui.css
    editor_css: null,
    // don't try to convert urls to relatives or any other url transformation
    convert_urls: false,
    relative_urls: false,
            // may be unused
//            remove_trailing_brs: true,
            custom_undo_redo: true,
    audio_width: 890,
    // place "minus" to prevent after-initializing load of plugins from default (plugins) dir
    // add js files (plugins too) to javascript.properties instead
    plugins: "paste,"
//        + "spellchecker,"
            + "-planetamusic,-planetaphoto,-planetalink,-planetavideo,-planetaexternal,-imagecontextmenu",
//                "",
    theme_advanced_buttons1: "planetaphoto,planetamusic,planetavideo,planetaexternal,planetalink",
    theme_advanced_buttons2: "",
    theme_advanced_buttons3: "",
    theme_advanced_toolbar_location: "top",
    theme_advanced_toolbar_align: "left",
    theme_advanced_statusbar_location: "none",
//    spellchecker_languages : "+Russian=ru,Ukrainian=uk,English=en",
//    spellchecker_rpc_url : "http://speller.yandex.net/services/tinyspell",
//    spellchecker_word_separator_chars : '\\s!"#$%&()*+,./:;<=>?@[\]^_{|}\xa7\xa9\xab\xae\xb1\xb6\xb7\xb8\xbb\xbc\xbd\xbe\u00bf\xd7\xf7\xa4\u201d\u201c',
    // for gecko, can change with tinymce versions
    object_resizing: false,
    // patched tiny_mce_jquery_src.js near lines 13400 and 13310
    // to force soft-new-line work as new line (with empty p)
    force_br_newlines: false,
    force_p_newlines: true,
    paste_auto_cleanup_on_paste: true,
    paste_retain_style_properties: ""
};

TinyMcePlaneta.faqEditorConfiguration = _.defaults({
    audio_width: 540,
    plugins: "paste,"
            + "-planetamusic,-planetaphoto,-planetalink,-planetavideo,-planetaexternal,-planetafullscreenfaqedit,-imagecontextmenu",
    theme_advanced_buttons1: "bold,italic,underline, |,"
            + "planetaphoto,planetamusic,planetavideo,planetaexternal,planetalink,|,planetafullscreenfaqedit"
}, TinyMcePlaneta.defaultConfiguration);

TinyMcePlaneta.campaignEditorConfiguration = _.defaults({
    campaign: true,
    audio_width: 540,
    plugins: "paste,"
            + "-planetamusic,-planetaphoto,-planetalink,-planetavideo,-planetaexternal,-planetafullscreencampaign,-imagecontextmenu",
    theme_advanced_buttons1: "bold,italic,underline, |,"
            + "planetaphoto,planetamusic,planetavideo,planetaexternal,planetalink,|,justifyleft,justifycenter,justifyright,justifyfull,|,planetafullscreencampaign,",
    valid_elements: 'p,b/strong,i/em,u,span[*],img[*],a[*],p[style]'
}, TinyMcePlaneta.faqEditorConfiguration);

TinyMcePlaneta.campaignEditorShareDescriptionConfiguration = _.defaults({
    campaign: true,
    removeOnCreateNew: false,
    plugins: "paste,"
            + "-planetalink,-lists,-advlist,-charactercount",
    theme_advanced_buttons1: "planetalink,bullist,numlist",
    valid_elements: 'p,a[href],p[style],ul,ol,li,li,br',
    forced_root_block : 'p'
}, TinyMcePlaneta.faqEditorConfiguration);

TinyMcePlaneta.quizEditorConfiguration = _.defaults({
    campaign: true,
    audio_width: 540,
    plugins: "paste,"
    + "-planetaphoto,-planetalink,-planetafullscreencampaign,-imagecontextmenu",
    theme_advanced_buttons1: "bold,italic,underline, |,"
    + "planetaphoto,planetalink,|,justifyleft,justifycenter,justifyright,justifyfull,",
    valid_elements: 'p,b/strong,i/em,u,span[*],img[*],a[*],p[style]'
}, TinyMcePlaneta.faqEditorConfiguration);

TinyMcePlaneta.newsPostEditorConfiguration = _.defaults({
    plugins: "paste,"
            + "-planetamusic,-planetaphoto,-planetalink,-planetavideo,-planetaexternal,-planetafullscreencampaign,-imagecontextmenu",
    theme_advanced_buttons1: "formatselect,bold,italic,underline,blockquote,|,"
            + "planetaphoto,planetamusic,planetavideo,planetaexternal,planetalink,|,justifyleft,justifycenter,justifyright,justifyfull,|,bullist,numlist,",
    theme_advanced_blockformats: "p,h2,h3,h4,h5,h6",
    valid_elements: 'p,b/strong,i/em,u,span[*],img[*],a[*],p[style],ul,li,ol,blockquote[*],cite,h2,h3,h4,h5,h6',
    paste_preprocess: function(pl, value) {
        value.content = value.content.replace(/(href=")http:\/\/(.*?)\//, '$1/');
    }
}, TinyMcePlaneta.defaultConfiguration);

TinyMcePlaneta.newsPostEditorConfiguration710 = _.defaults({
    plugins: "paste,"
            + "-planetamusic,-planetaphoto,-planetalink,-planetavideo,-planetaexternal,-planetafullscreencampaign,-imagecontextmenu",
    theme_advanced_buttons1: "formatselect,bold,italic,underline,blockquote,|,"
            + "planetaphoto,planetamusic,planetavideo,planetaexternal,planetalink",
    theme_advanced_buttons2: "justifyleft,justifycenter,justifyright,justifyfull,|,bullist,numlist",
    theme_advanced_blockformats: "p,h2,h3,h4,h5,h6",
    valid_elements: 'p,b/strong,i/em,u,span[*],img[*],a[*],p[style],ul,li,ol,blockquote[*],cite,h2,h3,h4,h5,h6',
    paste_preprocess: function(pl, value) {
        value.content = value.content.replace(/(href=")http:\/\/(.*?)\//, '$1/');
    }
}, TinyMcePlaneta.defaultConfiguration);

TinyMcePlaneta.newsPostEditorConfiguration480 = _.defaults({
    plugins: "paste,"
            + "-planetamusic,-planetaphoto,-planetalink,-planetavideo,-planetaexternal,-planetafullscreencampaign,-imagecontextmenu",
    theme_advanced_buttons1: "formatselect,bold,italic,underline,blockquote",
    theme_advanced_buttons2: "planetaphoto,planetamusic,planetavideo,planetaexternal,planetalink",
    theme_advanced_buttons3: "justifyleft,justifycenter,justifyright,justifyfull,|,bullist,numlist",
    theme_advanced_blockformats: "p,h2,h3,h4,h5,h6",
    valid_elements: 'p,b/strong,i/em,u,span[*],img[*],a[*],p[style],ul,li,ol,blockquote[*],cite,h2,h3,h4,h5,h6',
    paste_preprocess: function(pl, value) {
        value.content = value.content.replace(/(href=")http:\/\/(.*?)\//, '$1/');
    }
}, TinyMcePlaneta.defaultConfiguration);

TinyMcePlaneta.userSettingsSummaryEditorConfiguration = _.defaults({
    plugins: "paste,"
//        + "spellchecker,"
            + "-planetamusic,-planetaphoto,-planetalink,-planetavideo,-planetaexternal,-imagecontextmenu",
//                "",
    theme_advanced_buttons1: "planetaphoto,planetamusic,planetavideo,planetaexternal,planetalink",
    valid_elements: 'p,img[*],a[*]'
}, TinyMcePlaneta.defaultConfiguration);


// jquery plugin for simple editor initialization
(function ($) {

    TinyMcePlaneta.jqueryFunctions = {
        create: function (editorConfiguration) {
            TinyMcePlaneta.currentConfiguration = editorConfiguration;
            var editorHtml = this.html();
            if (!editorHtml) {
                return;
            }
            editorHtml = TinyMcePlaneta.Formatting.preFormatEditorHtml(editorHtml);
            this.html(editorHtml);
            editorConfiguration.elements = this[0].id;
            this.tinymceConfig = editorConfiguration;

            TinyMcePlaneta.createBlogEditor(editorConfiguration);
        },
        mappedHtml: function (el) {
            return TinyMcePlaneta.mappedHtml(tinyMCE.getInstanceById(el[0].id));
        },
        textClean: function (el) {
            return TinyMcePlaneta.textClean(tinyMCE.getInstanceById(el[0].id));
        },
        blogText: function (el) {
            return TinyMcePlaneta.nonMappedText(tinyMCE.getInstanceById(el[0].id));
        }

    };
    // assume we have only one editor
    $.fn.tinyMcePlaneta = function (options) {
        if (typeof (options) == 'string') {
            var args = [];
            Array.prototype.push.apply(args, arguments);
            args.shift();
            args.push(this);
            return TinyMcePlaneta.jqueryFunctions[options].apply(this, args);
        } else if (options === undefined) {
            options = TinyMcePlaneta.defaultConfiguration();
            return TinyMcePlaneta.jqueryFunctions.create.call(this, options);
        } else {
            return TinyMcePlaneta.jqueryFunctions.create.call(this, options);
        }
    }

})(jQuery);

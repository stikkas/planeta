ProfileInfoHover.Models.InfoWithActions = ProfileInfoHover.Models.Info.extend({

	defaults: {
		useActions: true
	},

	initialize:function (options) {
		ProfileInfoHover.Models.Info.prototype.initialize.call(this, options);
        // remove action buttons for anonymous user
        if (!workspace.isAuthorized) {
            this.set({
                useActions: false
            });
        }
	},

	sendMessage: function() {
		var profileId = this.get('profileId');
		this.hideView();
		workspace.appModel.get('dialogsController').showSendMessageDialog(profileId);
	}
});
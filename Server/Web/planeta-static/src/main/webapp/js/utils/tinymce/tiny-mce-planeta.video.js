/*global TinyMcePlaneta, tinymce, Attach, StringUtils, ImageUtils, ImageType*/
TinyMcePlaneta.createButtonPlugin("PlanetaExternal", function (self) {
    /**
     * @type {TinyMcePlaneta.ImageContextMenu}
     */
    self.contextMenu = null;

    self.editor.onPreInit.add(function (ed) {
        self.contextMenu = new TinyMcePlaneta.ImageContextMenu(ed, self.clsName);
        self.contextMenu.addCloseButton(function () {
        });
    });


    self.editor.addCommand(self.name, function () {
        var rng = self.editor.selection.getRng();
        Attach.showExternalVideo({
            selected: function (response) {
                var $el = TinyMcePlaneta.Video.getElementToInsert(response);
                self.editor.selection.setRng(rng);
                TinyMcePlaneta.insertHtml(self.editor, $el.html());
            },
            playerSize: 'big',
            addVideoToProfileId: workspace.appModel.myProfileId()
        });
    });


});

TinyMcePlaneta.Video = new TinyMcePlaneta.RichMedia({
    clsName: 'mceplanetavideo',
    parseRichMedia: function (attributes) {
        var data = {
            name: attributes.name,
            duration: StringUtils.humanDuration(attributes.duration)
        };
        if (TinyMcePlaneta.currentConfiguration.campaign) {
            data.config = 'campaign';
        }
        if (attributes.image) {
            data.url = ImageUtils.getThumbnailUrl(attributes.image, ImageUtils.CAMPAIGN_VIDEO, ImageType.VIDEO);
        } else {
            data.profileId = attributes.owner;
            data.videoId = attributes.id;
        }
        var src = document.location.protocol + "//" + workspace.staticNodesService.getStaticNode() +
            '/video-stub.jpg?' + TinyMcePlaneta.serializeData(data);
        var $imgReplacement = $(document.createElement('img'));
        $imgReplacement.attr({
            'data-mce-json': JSON.stringify(attributes),
            // ACHTUNG
            // to process before tiny initializing
            // future-known class name is hardcoded here
            'class': TinyMcePlaneta.Video.clsName,
            'src': src
        });
        $imgReplacement.addClass(attributes['class']);
        // if uploader can't get screenshot, wait for video converting finish
        if (!attributes.image) {
            var richTagUpdate = setInterval(function () {
                $.get('/api/public/video.json?' + $.param({profileId: attributes.owner, videoId: attributes.id}), {}, function (response) {
                    if (response && response.status == 'ERROR') {
                        clearInterval(richTagUpdate);
                        if (workspace) {
                            workspace.appView.showErrorMessage("Не удалось загрузить видео. Неподдерживаемый кодек");
                        }
                    }
                    if (response.imageUrl) {
                        var json = JSON.parse($imgReplacement.attr('data-mce-json'));
                        json.image = response.imageUrl;
                        var $tinyImg = $(tinymce.activeEditor.getBody()).find('img[src="' + src + '"]');
                        $tinyImg.attr('data-mce-json', JSON.stringify(json));
                        $tinyImg.attr("src", src + "&r=" + (new Date()).getTime());
                        clearInterval(richTagUpdate);
                    }
                });
            }, 3000);
        }
        return $imgReplacement;
    },

    getElementToInsert: function (response) {
        var richMediaData = {
            duration: response.duration,
            id: response.objectId || response.id,
            name: response.name,
            owner: response.ownerId || response.owner || response.profileId,
            image: response.imageUrl || response.value,
            type: response.type || response.videoType
        };
        var $el = $(document.createElement('div'));
        $el.append(TinyMcePlaneta.Video.parseRichMedia(richMediaData));
        return $el;
    }

});


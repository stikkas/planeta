/**
 * User: User a.volkov
 * Date: 07.08.12
 * Time: 11:59
 */


var CookieProvider = {
    isSupported: function () {
        return true;
    },
    set: function (name, value, options) {
        if (!name) {
            return;
        }

        var cookieString = name + '=' + value;
        if (options) {
            var date = new Date();
            date.setDate(date.getDate() + 1000);
            options.expires = options.expires || date.toUTCString();

            $.each(options, function (key, value) {
                cookieString += ';' + key + '=' + value;
            });
        }
        document.cookie = cookieString;
    },
    get: function (name) {
        if (!name) {
            return null;
        }
        return this.parse()[name];
    },
    parse: function () {
        var cookies = {}, state = 0, c, name = '', value = '', i;
        var cookieString = document.cookie + ';';
        for (i = 0; i < cookieString.length; ++i) {
            c = cookieString.charAt(i);
            switch (state) {
                case 0:
                    if (c === '=') {
                        state = 1;
                    } else {
                        name += c;
                    }
                    break;
                case 1:
                    if (c === '"') {
                        state = 2;
                    } else if (c === ';') {
                        if (cookies[name]) {
                            console.warn('overriding cookie ' + name + '=' + cookies[name] + ' by ' + name + '=' + value);
                        }
                        cookies[name.trim()] = value.trim();
                        name = value = '';
                        state = 0;
                    } else {
                        value += c;
                    }
                    break;
                case 2:
                    if (c === '"') {
                        state = 1;
                    } else {
                        value += c;
                    }
                    break;
            }
        }
        return cookies;
    },
    remove: function (name, path) {
        if (!name) {
            return;
        }

        path = path || "";
        document.cookie = name + '=; path=' + path + '; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
    }
};

var LocalStorageProvider = {
    isSupported: function () {
        return window.localStorage !== undefined;
    },
    set: function (name, value) {
        if (!name) {
            return;
        }
        try {
            localStorage.setItem(name, value);
        } catch (e) {
        }
    },
    get: function (name) {
        if (!name) {
            return null;
        }
        return localStorage.getItem(name);

    },
    remove: function (name) {
        if (!name) {
            return;
        }
        localStorage.removeItem(name);
    }
};

var SessionStorageProvider = {
    PlanetaStorage : {},

    isSupported: function () {
        if (window.sessionStorageSupported !== undefined) {
            // flag defined
            return window.sessionStorageSupported;
        }

        // flag is undefined, need to define
        if (window.sessionStorage === undefined) {
            // storage unsupported
            window.sessionStorageSupported = false;
            return false;
        }

        // for Safari in Private browsing mode
        try {
            window.sessionStorage.setItem("supported", "testvalue");
            window.sessionStorageSupported = window.sessionStorage.getItem("supported") === "testvalue";
            // storage set and get works corretly
            return window.sessionStorageSupported;
        } catch (error) {
            // storage operations cause an exception
            window.sessionStorageSupported = false;
            return false;
        }
    },
    set: function (name, value) {
        if (!name) {
            return;
        }

        if (SessionStorageProvider.isSupported()) {
            try {
                sessionStorage.setItem(name, JSON.stringify(value));
            } catch (e) {
            }
        } else {
            SessionStorageProvider.PlanetaStorage[name] = value;
        }
    },
    get: function (name) {
        if (!name) {
            return null;
        }
        if (SessionStorageProvider.isSupported()) {
            return JSON.parse(sessionStorage.getItem(name));
        } else {
            return SessionStorageProvider.PlanetaStorage[name];
        }
    },
    remove: function (name) {
        if (!name) {
            return;
        }
        if (SessionStorageProvider.isSupported()) {
            sessionStorage.removeItem(name);
        } else {
            delete SessionStorageProvider.PlanetaStorage[name];
        }
    },
    extendByKey: function (key, extencionData) {
        var _temp = SessionStorageProvider.get(key) || {};
        _.extend(_temp, extencionData);
        SessionStorageProvider.set(key, _temp);
    }
};

var StorageUtils = {
    provider: LocalStorageProvider.isSupported() ? LocalStorageProvider : CookieProvider,
    setGroupCampaignBlockState: function (groupId, collapsed) {
        if (!groupId) {
            return;
        }
        var key = 'group.campaign.' + groupId;
        if (collapsed) {
            StorageUtils.provider.set(key, collapsed);
        } else {
            StorageUtils.provider.remove(key);
        }

    },
    getGroupCampaignBlockState: function (groupId) {
        return groupId ? StorageUtils.provider.get('group.campaign.' + groupId) : null;
    },

    /**
     *@param {Array} hiddenTypes
     */
    setFeedFilterHiddenTypes: function (hiddenTypes) {
        if (!hiddenTypes) {
            return;
        }
        StorageUtils.provider.set('feed.filter.hiddenTypes', JSON.stringify(hiddenTypes));
    },
    getFeedFilterHiddenTypes: function () {
        return $.parseJSON(StorageUtils.provider.get('feed.filter.hiddenTypes'));
    },

    /**
     *@param {Boolean} orderTop
     */
    setFeedOrderTop: function (orderTop) {
        StorageUtils.provider.set('feed.filter.orderTop', JSON.stringify(orderTop));
    },
    isFeedOrderTop: function () {
        return $.parseJSON(StorageUtils.provider.get('feed.filter.orderTop'));
    },

    setCommunicateFilterId: function (filter) {
        StorageUtils.provider.set('communicate.filter', JSON.stringify(filter));
    },
    getCommunicateFilterId: function () {
        return $.parseJSON(StorageUtils.provider.get('communicate.filter'));
    },

    setAudioPlayerState: function (state) {
        StorageUtils.set('audio.player.state', state);
    },

    getAudioPlayerState: function () {
        return StorageUtils.get('audio.player.state');
    },

    setVideoPlayerState: function (state) {
        StorageUtils.provider.set('video.player.state', JSON.stringify(state));
    },

    getVideoPlayerState: function () {
        return $.parseJSON(StorageUtils.provider.get('video.player.state'));
    },

    setInteractiveFormsSettings: function (state) {
        StorageUtils.provider.set('interactive.forms.state', JSON.stringify(state));
    },

    getInteractiveFormsSettings: function () {
        return $.parseJSON(StorageUtils.provider.get('interactive.forms.state'));
    },

    increaseIfItIsMoreThanMaxInteractiveReachedStepNum: function (stepNum) {
        var currentVal = this.getInteractiveReachedStepNum();
        if (stepNum > currentVal) {
            StorageUtils.provider.set('interactive.reached.step.num', stepNum);
        }
    },

    setInteractiveReachedStepNum: function (setNum) {
        return $.parseJSON(StorageUtils.provider.set('interactive.reached.step.num', setNum));
    },

    getInteractiveReachedStepNum: function () {
        return $.parseJSON(StorageUtils.provider.get('interactive.reached.step.num')) || 0;
    },


    /* ------  START ------*/
    setInteractiveDraftCampaignDesiredCollectedAmountPercent: function (campaign) {
        StorageUtils.provider.set('interactive.draft.campaign.desired.collected.amount.percent', JSON.stringify(campaign));
    },

    getInteractiveDraftCampaignDesiredCollectedAmountPercent: function () {
        return $.parseJSON(StorageUtils.provider.get('interactive.draft.campaign.desired.collected.amount.percent'));
    },
    /* ------  END ------*/


    /* ------  START ------*/
    setInteractiveDraftCampaignContractorTypePercent: function (campaign) {
        //INDIVIDUAL 13%
        //LEGAL_ENTITY 6%
        StorageUtils.provider.set('interactive.draft.campaign.contractor.type.percent', JSON.stringify(campaign));
    },

    getInteractiveDraftCampaignContractorTypePercent: function () {
        //INDIVIDUAL 13%
        //LEGAL_ENTITY 6%
        return $.parseJSON(StorageUtils.provider.get('interactive.draft.campaign.contractor.type.percent'));
    },
    /* ------  END ------*/


    /* ------  START ------*/
    setInteractiveDraftCampaignTargetAmount: function (campaign) {
        StorageUtils.provider.set('interactive.draft.campaign.target.amount', JSON.stringify(campaign));
    },

    getInteractiveDraftCampaignTargetAmount: function () {
        return $.parseJSON(StorageUtils.provider.get('interactive.draft.campaign.target.amount'));
    },
    /* ------  END ------*/


    setInteractiveDraftCampaignUserName: function (name) {
        StorageUtils.provider.set('interactive.draft.campaign.user.name', JSON.stringify(name));
    },

    getInteractiveDraftCampaignUserName: function () {
        return $.parseJSON(StorageUtils.provider.get('interactive.draft.campaign.user.name'));
    },

    setInteractiveStepNameStartSecondVariant: function (isSecondVariant) {
        StorageUtils.provider.set('interactive.step.name.start.second.variant', JSON.stringify(isSecondVariant));
    },

    isInteractiveStepNameStartSecondVariant: function () {
        return $.parseJSON(StorageUtils.provider.get('interactive.step.name.start.second.variant'));
    },

    setInteractiveDraftCampaignAgreeStorePersonalData: function (agree) {
        StorageUtils.provider.set('interactive.draft.campaign.agree.store.personal.data', JSON.stringify(agree));
    },

    getInteractiveDraftCampaignAgreeStorePersonalData: function () {
        return $.parseJSON(StorageUtils.provider.get('interactive.draft.campaign.agree.store.personal.data'));
    },

    setInteractiveDraftCampaignUserEmail: function (name) {
        StorageUtils.provider.set('interactive.draft.campaign.user.email', JSON.stringify(name));
    },

    getInteractiveDraftCampaignUserEmail: function () {
        return $.parseJSON(StorageUtils.provider.get('interactive.draft.campaign.user.email'));
    },

    setInteractiveDraftCampaignId: function (campaignId) {
        StorageUtils.provider.set('interactive.draft.campaign.id', JSON.stringify(campaignId));
    },

    getInteractiveDraftCampaignId: function () {
        return $.parseJSON(StorageUtils.provider.get('interactive.draft.campaign.id'));
    },

    setInteractiveDraftCampaignAgreeReceiveLessonsMaterialsOnEmail: function (isAgree) {
        StorageUtils.provider.set('interactive.draft.campaign.agree.receive.lessons.materials.on.email', JSON.stringify(isAgree));
    },

    toggleInteractiveDraftCampaignAgreeReceiveLessonsMaterialsOnEmail: function () {
        var isAgree = this.isInteractiveDraftCampaignAgreeReceiveLessonsMaterialsOnEmail();
        StorageUtils.provider.set('interactive.draft.campaign.agree.receive.lessons.materials.on.email', JSON.stringify(!isAgree));
        $.ajax({
            type: 'post',
            url: '/api/campaign/set-agree-receive-lessons-materials-on-email.json',
            data: {isAgree: JSON.stringify(!isAgree)}
        });
    },

    isInteractiveDraftCampaignAgreeReceiveLessonsMaterialsOnEmail: function () {
        return !!$.parseJSON(StorageUtils.provider.get('interactive.draft.campaign.agree.receive.lessons.materials.on.email'));
    },


    clearInteractiveData: function () {
        this.setInteractiveDraftCampaignId(null);
        this.setInteractiveDraftCampaignUserEmail(null);
        this.setInteractiveStepNameStartSecondVariant(false);
        this.setInteractiveDraftCampaignUserName(null);
        this.setInteractiveDraftCampaignContractorTypePercent(null);
        this.setInteractiveDraftCampaignDesiredCollectedAmountPercent(null);
        this.setInteractiveDraftCampaignTargetAmount(null);
        this.setInteractiveReachedStepNum(null);
        this.setInteractiveFormsSettings(null);
        this.setInteractiveDraftCampaignAgreeStorePersonalData(false);
        this.setInteractiveDraftCampaignAgreeReceiveLessonsMaterialsOnEmail(false);

        $.ajax({
            type: 'post',
            url: '/api/campaign/clear-interactive-editor-data.json'
        });
    },

    setVideosCount: function (videosCount) {
        StorageUtils.provider.set('videos.count', JSON.stringify(videosCount));
    },

    getVideosCount: function () {
        return $.parseJSON(StorageUtils.provider.get('videos.count'));
    },

    setCrowdfundInfoPopupAlreadySeen: function (isSeen) {
        StorageUtils.provider.set('crowdfund.popup.seen', JSON.stringify(isSeen));
    },

    getCrowdfundInfoPopupAlreadySeen: function () {
        return $.parseJSON(StorageUtils.provider.get('crowdfund.popup.seen'));
    },

    setBroadcastSectionId: function (sectionId) {
        StorageUtils.provider.set('broadcast.section.v2', JSON.stringify(sectionId));
    },

    getBroadcastSectionId: function () {
        return $.parseJSON(StorageUtils.provider.get('broadcast.section.v2'));
    },

    //deprecated
    setBroadcastSubSectionId: function (subSectionId) {
        StorageUtils.provider.set('broadcast.subsection', JSON.stringify(subSectionId));
    },
    //deprecated
    getBroadcastSubSectionId: function () {
        return $.parseJSON(StorageUtils.provider.get('broadcast.subsection'));
    },

    set: function (name, obj) {
        var value = JSON.stringify(obj);
        if ($ && $.browser && $.browser.msie) {
            if (value != StorageUtils.provider.get(name)) {
                StorageUtils.provider.set(name, value);
            }
        } else {
            StorageUtils.provider.set(name, value);
        }
    },
    get: function (name) {
        return $.parseJSON(StorageUtils.provider.get(name));
    },
    remove: function (name){
        StorageUtils.provider.remove(name);
    }
};



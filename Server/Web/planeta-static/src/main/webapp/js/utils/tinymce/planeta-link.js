TinyMceUtils.CreateButtonPlugin("PlanetaLink", function(self) {

    this.maxAnchorLength = 50;

    self.editor.addCommand(self.name, function() {
        var sel = self.editor.selection.getContent();
        var rng = self.editor.selection.getRng();
        Attach.showEditorLink(function(response) {
            var linkUrl = response.replace(/\[url=(.*?)\].*/, '$1');
            if(!sel){
                if(linkUrl.length > self.maxAnchorLength)
                    sel = linkUrl.slice(0,self.maxAnchorLength) + '...';
                else
                    sel = linkUrl;
            }
            var anchorHtml = '<a href="'+ linkUrl +'" >' + sel + '</a>';
            self.editor.selection.setRng(rng);
            self.editor.execCommand('mceInsertContent', 0, anchorHtml);
        });
    });

    self.editor.onNodeChange.add(function(ed, cm, n) {
        cm.setActive(self.name, n.nodeName === 'A');
    });
});

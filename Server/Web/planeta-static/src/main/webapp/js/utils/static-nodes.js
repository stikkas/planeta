/**
 * Static nodes service
 *
 * @param staticNode -- node for uploading static content like images, videos, music, etc
 * @param resourcesHost -- host where we store all css, less, images and javascript.
 * @param staticNodes -- host where we store all css, less, images and javascript.
 * @param jsBaseUrl -- base url for javascript files
 */
var StaticNodesService = function StaticNodesService(staticNode, staticNodes, resourcesHost, jsBaseUrl, flushCache, compress, staticServiceLang) {
    this.staticNodes = staticNodes;
    this.staticNode = staticNode;
    this.resourcesHost = resourcesHost;
    this.jsBaseUrl = jsBaseUrl;
    this.flushCache = flushCache;
    this.compress = compress;
    this.staticServiceLang = staticServiceLang;
};

StaticNodesService.prototype.getResourceUrl = function (url) {
    if (!url || StringUtils.isAbsoluteUrl(url)) {
        return StringUtils.replaceHttpToHttps(url);
    }

    if (this.resourcesHost) {
        if (url.indexOf('/') !== 0) {
            url = '/' + url;
        }
        return this.getUrlWithScheme(this.resourcesHost) + url;
    }

    return url;
};

StaticNodesService.prototype.getScheme = function (url) {
    var hrf = window.location.href, 
            proto = hrf.substr(0, hrf.indexOf('/') + 2);
    if (proto.indexOf('http') === 0)
        return proto;
    return url.indexOf('localhost') === 0 ? 'http://' : 'https://';
};

StaticNodesService.prototype.getUrlWithScheme = function (url) {
    return this.getScheme(url) + url;
};

StaticNodesService.prototype.getStaticNode = function () {
    //TODO: change it when we will have more than one static node
    return this.staticNode;
};

StaticNodesService.prototype.getJsUrl = function (url, params) {
    if (!url || StringUtils.isAbsoluteUrl(url)) {
        return url;
    }

    if (this.resourcesHost) {
        if (url.indexOf('/') !== 0) {
            url = '/' + url;
        }

        var data = {};
        if (!this.compress) {
            data.compress = false;
        }
        if (this.flushCache) {
            data.flushCache = true;
        }
        if (this.staticServiceLang) {
            data.lang = this.staticServiceLang;
        }

        var parametrs = _.extend(data, params);
        var paramString = '';
        if (parametrs) {
            paramString = '?' + $.param(parametrs);
        }
        // return only HTTPS
        return this.getUrlWithScheme(this.jsBaseUrl) + url + paramString;
    }
    return url;
};

StaticNodesService.prototype.getProxyParamsServerPath = function (url, forceProxy) {

    if (!url) {
        return null;
    }

    var params = $.parseUrl(url);

    var proxyParams = null;
    $.each(this.staticNodes, function (index, value) {
        if (params.host === value && params.path === "/p") {
            proxyParams = params;
            var queryParams = params.query.split('&');
            $.each(queryParams, function (i, keyValue) {
                var pair = keyValue.split('=');
                proxyParams[pair[0]] = decodeURIComponent(pair[1]);
            });
            return false;
        }
    });
    if (proxyParams == null && forceProxy) {
        proxyParams = {
            url: url,
            host: params.host
        };
    }
    return proxyParams;
};

StaticNodesService.prototype.createProxyUrl = function (node, url, options) {
    return 'https://' + node + '/p?' + $.param(_.extend({url: url}, options));
};


StaticNodesService.prototype.isStaticServerPath = function (url) {

    if (!url) {
        return false;
    }
    var i = 0;
    for (i = 0; i < this.staticNodes.length; i += 1) {
        if (url.indexOf(this.staticNodes[i]) >= 0) {
            return true;
        }
    }
    return false;
};

StaticNodesService.prototype.findStaticNode = function (url) {
    if (url) {
        var i = 0;
        for (i = 0; i < this.staticNodes.length; i += 1) {
            if (url.indexOf(this.staticNodes[i]) >= 0) {
                return this.staticNodes[i];
            }
        }
    }
    return this.getStaticNode();
};

StaticNodesService.prototype.isNodeProxyServerPath = function (url, staticNode) {
    if (!url) {
        return false;
    }
    return url.indexOf(staticNode + '/p?url=') >= 0;
};

StaticNodesService.prototype.isProxyServerPath = function (url) {
    var self = this;
    return !!_.any(this.staticNodes, function (staticNode) {
        return self.isNodeProxyServerPath(url, staticNode);
    });
};

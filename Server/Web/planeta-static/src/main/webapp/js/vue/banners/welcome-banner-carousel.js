Vue.component('welcome-banner-carousel', {

    template: '#welcome-banner-carousel',

    data: function () {
        return {
            banners: [],
            carouselReady: false,
            stats: {
                shares: 0,
                activeCampaigns: 0,
                users: 0
            },
            autoplayReq: {},
            carousel: {}
        }
    },

    methods: {
        load: function () {
            var self = this;

            if(!window.HTMLPictureElement || !('sizes' in document.createElement('img'))){
                moduleLoader.loadModule('respimgPolyfill');
            }

            this.$http.get('/api/util/get-promo-banners', {params: {bannerType: 'PROMO_BANNER',
                    isAuthor: workspace.appModel.get('myProfile').get('isAuthor')}}).then(
                function (response) {
                    response.body.result.forEach(function (i) {
                        self.banners.push(i);
                    });
                },

                function (response) {
                    workspace.appView.showErrorMessage(response.errorMessage);
                }
            );

            this.$http.get('/api/welcome/main-slider-stats').then(
                function (response) {
                    if (response.ok) {
                        this.stats = response.body;
                    }
                },

                function (response) {
                    workspace.appView.showErrorMessage(response.errorMessage);
                }
            );
        },

        humanNumber: function (value) {
            if (value === 0) {
                return "";
            }
            return StringUtils.humanNumber(value);
        },

        isAutorized: function () {
            return workspace.isAuthorized;
        },

        initCarousel: function () {
            if ( this.carouselReady === true ) return;
            this.carouselReady = true;

            var self = this;

            var transformProperty = (function () {
                var style = document.documentElement.style;
                if (typeof style.transform === 'string') {
                    return 'transform';
                }
                return 'WebkitTransform';
            })();

            var carouselOptions = {
                wrapAround: true,
                arrowShape: 'M69.1 12.4l-37 37.5 37.5 38c2.6 2.8 2.6 7.2-.1 10-2.7 2.7-7.1 2.8-9.9.1L17.2 55c-1.4-1.4-2.2-3.2-2.2-5.2 0-1.9.8-3.8 2.2-5.2l42.4-43c2.9-2.5 7.3-2.3 9.9.6 2.6 2.9 2.4 7.5-.4 10.2z',
                draggable: isTouchDevice(),
                prevNextButtons: window.screen.width >= 768
            };

            if (window.screen.width <= 1366 && isTouchDevice()) {
                carouselOptions.selectedAttraction = 0.028;
                carouselOptions.friction = 0.34;
            } else {
                carouselOptions.selectedAttraction = 0.018;
                carouselOptions.friction = 0.25;
            }


            var carouselEl = $('.project-hero-carousel_list');
            self.carousel = new Flickity(carouselEl[0], carouselOptions);

            if ( window.navigator.platform.indexOf('Win') != -1 ) {
                carouselEl.addClass('carousel-fade');
            } else {
                var carouselStepProgress = 100 / (self.carousel.slides.length - 1);

                var carouselItems = [];
                $('.project-hero-carousel_i').each(function(i, el) {
                    carouselItems.push({
                        'cover': $('.hero-cover', el)[0] || false,
                        'img': $('.hero-image', el)[0] || false,
                        'head': $('.hero-head', el)[0] || false,
                        'text': $('.hero-text', el)[0] || false,
                        'action': $('.hero-action', el)[0] || false
                    });
                });

                self.carousel.on('scroll', function (i) {

                    var slideWidth = self.carousel.size.width;
                    var commonProgress = Math.floor(i * 1000000) / 10000;
                    var currentSlideIndex = Math.floor((commonProgress + carouselStepProgress / 2 ) / carouselStepProgress % carouselStepProgress);

                    if (currentSlideIndex >= self.carousel.slides.length) currentSlideIndex = 0;
                    if (currentSlideIndex < 0) currentSlideIndex = self.carousel.slides.length - 1;

                    var stepProgress = Math.floor((commonProgress - Math.floor(commonProgress / carouselStepProgress % carouselStepProgress) * carouselStepProgress) * (self.carousel.slides.length - 1) * 100) / 100;


                    // var left;
                    // var right;
                    var leftIndex;
                    var rightIndex;

                    if ( stepProgress < 50 ) {
                        leftIndex = currentSlideIndex;
                        // left = carousel.slides[currentSlideIndex].cells[0].element;

                        rightIndex = currentSlideIndex + 1;
                        if ( rightIndex >= self.carousel.slides.length ) rightIndex = 0;
                        if ( rightIndex < 0 ) rightIndex = self.carousel.slides.length - 1;
                        // right = carousel.slides[rightIndex].cells[0].element;
                    } else if ( stepProgress >= 50 ) {
                        leftIndex = currentSlideIndex - 1;
                        if ( leftIndex >= self.carousel.slides.length ) leftIndex = 0;
                        if ( leftIndex < 0 ) leftIndex = self.carousel.slides.length - 1;
                        // left = carousel.slides[leftIndex].cells[0].element;

                        rightIndex = currentSlideIndex;
                        // right = carousel.slides[currentSlideIndex].cells[0].element;
                    }


                    var leftCover = carouselItems[leftIndex].cover;
                    var leftImg = carouselItems[leftIndex].img;
                    var leftHead = carouselItems[leftIndex].head;
                    var leftText = carouselItems[leftIndex].text;
                    var leftAction = carouselItems[leftIndex].action;

                    var leftCoverX = slideWidth * stepProgress / 110;
                    var leftHeadX = slideWidth * stepProgress / 130;
                    var leftTextX = slideWidth * stepProgress / 140;
                    var leftActionX = slideWidth * stepProgress / 150;

                    var leftOpacity = .8 * (100 - stepProgress) / 100 + .2;

                    if ( leftCover ) leftCover.style[ transformProperty ] = 'translate3d(' + leftCoverX + 'px, 0, 0)';
                    if ( leftImg ) leftImg.style[ transformProperty ] = 'translate3d(' + leftCoverX + 'px, 0, 0)';
                    if ( leftHead ) leftHead.style[ transformProperty ] = 'translate3d(' + leftHeadX + 'px, 0, 0)';
                    if ( leftText ) leftText.style[ transformProperty ] = 'translate3d(' + leftTextX + 'px, 0, 0)';
                    if ( leftAction ) leftAction.style[ transformProperty ] = 'translate3d(' + leftActionX + 'px, 0, 0)';

                    if ( leftHead ) leftHead.style['opacity'] = leftOpacity;
                    if ( leftText ) leftText.style['opacity'] = leftOpacity;
                    if ( leftAction ) leftAction.style['opacity'] = leftOpacity;




                    var rightCover = carouselItems[rightIndex].cover;
                    var rightImg = carouselItems[rightIndex].img;
                    var rightHead = carouselItems[rightIndex].head;
                    var rightText = carouselItems[rightIndex].text;
                    var rightAction = carouselItems[rightIndex].action;

                    var rightCoverOffset = 1 * slideWidth / 100;
                    var rightCoverX = rightCoverOffset - rightCoverOffset * stepProgress / 100;
                    var rightHeadOffset = 50 * slideWidth / 100;
                    var rightHeadX = rightHeadOffset - rightHeadOffset * stepProgress / 100;
                    var rightTextOffset = 70 * slideWidth / 100;
                    var rightTextX = rightTextOffset - rightTextOffset * stepProgress / 100;
                    var rightActionOffset = 90 * slideWidth / 100;
                    var rightActionX = rightActionOffset - rightActionOffset * stepProgress / 100;

                    var rightOpacity = .5 * (stepProgress) / 100 + .5;

                    if ( rightCover ) rightCover.style[ transformProperty ] = 'translate3d(' + rightCoverX + 'px, 0, 0)';
                    if ( rightImg ) rightImg.style[ transformProperty ] = 'translate3d(' + rightCoverX + 'px, 0, 0)';
                    if ( rightHead ) rightHead.style[ transformProperty ] = 'translate3d(' + rightHeadX + 'px, 0, 0)';
                    if ( rightText ) rightText.style[ transformProperty ] = 'translate3d(' + rightTextX + 'px, 0, 0)';
                    if ( rightAction ) rightAction.style[ transformProperty ] = 'translate3d(' + rightActionX + 'px, 0, 0)';

                    if ( rightHead ) rightHead.style['opacity'] = rightOpacity;
                    if ( rightText ) rightText.style['opacity'] = rightOpacity;
                    if ( rightAction ) rightAction.style['opacity'] = rightOpacity;
                });
            }




            function isTouchDevice() {
                return 'ontouchstart' in window || navigator.maxTouchPoints;
            }

            // get rAF, prefixed, if present
            var requestAnimationFrame = window.requestAnimationFrame ||
                window.webkitRequestAnimationFrame;

            var cancelAnimationFrame = window.cancelAnimationFrame ||
                window.webkitCancelAnimationFrame;

            // fallback to setTimeout
            var lastTime = 0;
            if (!requestAnimationFrame) {
                requestAnimationFrame = function (callback) {
                    var currTime = new Date().getTime();
                    var timeToCall = Math.max(0, 16 - ( currTime - lastTime ));
                    var id = setTimeout(callback, timeToCall);
                    lastTime = currTime + timeToCall;
                    return id;
                };
            }


            var state = 'playing';
            var startPlayTime = new Date().getTime();
            var pauseTime = 0;
            var startPauseTime = 0;
            var isEnter = false;
            var currentSlideId = 0;
            var autoPlayTimeout = 12000;
            var isSelected = false;
            var carouselBar = $('.project-hero-carousel_bar');


            self.carousel.on('settle', function () {
                if (currentSlideId !== self.carousel.selectedIndex) {
                    loadCurrentSlideImg();

                    currentSlideId = self.carousel.selectedIndex;
                    if (state === 'stopped') {
                        start();
                    } else {
                        unpause();
                    }
                } else if (isSelected) {
                    start();
                } else {
                    if (!isEnter) {
                        unpause();
                    } else if (state !== 'playing') {
                        start();
                    }
                }
                isSelected = false;
            });

            self.carousel.on('select', function () {
                if (currentSlideId !== self.carousel.selectedIndex) {
                    isSelected = true;
                    stop();
                }
            });

            $(window).on('resize', _.debounce(loadCurrentSlideImg, 1000));
            loadCurrentSlideImg();
            function loadCurrentSlideImg() {
                lazyLoadImg(self.carousel.cells[self.carousel.selectedIndex].element);
                var nextIndex = self.carousel.selectedIndex == self.carousel.cells.length - 1 ? 0 : self.carousel.selectedIndex + 1;
                lazyLoadImg(self.carousel.cells[nextIndex].element);
            }
            function lazyLoadImg(slide) {
                $('.hero-image-cover, .hero-image img', slide).each(function () {
                    var img = this;
                    if ( !$(img).data('lazyloadinit') ) {
                        if ( $(img).is(':visible') ) {
                            $(img).data('lazyloadinit', true);
                            if ( !!window.lazySizes ) lazySizes.loader.unveil(this);
                        }
                    }
                });
            }


            carouselEl.on('mouseenter', function () {
                enter();
            });
            self.carousel.on('pointerDown', function () {
                enter();
            });

            carouselEl.on('mouseleave', function () {
                leave();
            });
            self.carousel.on('pointerUp', function () {
                leave();
            });


            function enter() {
                isEnter = true;
                if (state === 'playing') {
                    pause();
                }
            }

            function leave() {
                isEnter = false;
                if (state === 'paused' && self.carousel.isAnimating !== true) {
                    unpause();
                }
            }


            function start() {
                state = 'playing';
                startPlayTime = new Date().getTime();
                pauseTime = 0;
                startPauseTime = 0;
                changeProgress(0);
                showProgress();
                if (isEnter) {
                    pause();
                }
            }

            function stop() {
                state = 'stopped';
                hideProgress();
            }

            function pause() {
                state = 'paused';
                startPauseTime = new Date().getTime();
            }

            function unpause() {
                state = 'playing';
                var currentPauseTime = new Date().getTime();
                pauseTime += currentPauseTime - startPauseTime;
            }

            function showProgress() {
                carouselBar.removeClass('hide-bar');
            }

            function hideProgress() {
                carouselBar.addClass('hide-bar');
            }

            function changeProgress(progress) {
                carouselBar[0].style[transformProperty] = 'scaleX(' + (progress / 100 + (0.01 * progress / 100)) + ')';
            }


            if (window.screen.width >= 768) {
                setTimeout(function () {
                    start();
                    carouselAutoplay();
                }, 2000);
            }

            function carouselAutoplay() {
                if (state === 'playing') {
                    var currentPlayTime = new Date().getTime();
                    var progress = (currentPlayTime - startPlayTime - pauseTime) * 100 / autoPlayTimeout;
                    progress = Math.min(Math.max(progress, 0), 100);

                    changeProgress(progress);

                    if (currentPlayTime - startPlayTime - pauseTime >= autoPlayTimeout) {
                        autoPlayTimeout = 7000;
                        stop();
                        if (!carouselEl.length) {
                            cancelAnimationFrame(self.autoplayReq);
                        } else {
                            self.carousel.next();
                        }
                    }
                }

                self.autoplayReq = requestAnimationFrame(function () {
                    carouselAutoplay();
                });
            }

            var checkSelfInterval = setInterval(function () {
                if (!!$('.project-hero-carousel').length) return;
                clearInterval(checkSelfInterval);
                cancelAnimationFrame(self.autoplayReq);
                self.carousel.destroy();
            }, 3000);
        }
    },

    created: function () {
        this.load();
    },

    updated: function () {
        if ( this.banners.length > 0 ) this.initCarousel();
    }
});
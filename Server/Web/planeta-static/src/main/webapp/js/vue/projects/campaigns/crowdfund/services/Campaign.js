/**
 * Сервис для работы с данными по проекту на карточке проекта
 */
(function () {
    var campaign = {},
        adminRole,
        isAdmin;

    VueServices.Campaign = {
        beforeCreate: function () {
            var self = this;
            adminRole = PrivacyUtils.hasAdministrativeRole();
            isAdmin = PrivacyUtils.isAdmin();

            self.campaign = campaign;

            if (_.isEmpty(campaign)) {
                self.$http.get('/api/campaign/campaign.json?profileId='
                    + workspace.appModel.myProfileId() + '&objectId='
                    + workspace.appModel.get('campaignModel').get('campaignId'))
                    .then(function (resp) {
                        if (resp.body.success) {
                            _.extend(self.campaign, resp.body.result);
                            loadModule('news').done(function () {
                                self.postCollection = new News.Models.Posts([], {
                                    data: {
                                        profileId: 0,
                                        campaignId: self.campaign.campaignId
                                    },
                                    limit: 10
                                });
                                self.postCollection.load();
                            });
                        }
                    });
            }
        },
        methods:
            {
                canSendNews: function () {
                    return adminRole || (isAdmin && _.contains(['ACTIVE', 'PAUSED', 'FINISHED'], campaign.status));
                }
                ,
                canChangeCampaign: function () {
                    return campaign.canChangeCampaign || adminRole;
                }
                ,
                isModerationButtonsVisible: function () {
                    return adminRole;
                }
                ,
                notDeleted: function () {
                    return adminRole || isAdmin;
                }
            }
    }
})
();


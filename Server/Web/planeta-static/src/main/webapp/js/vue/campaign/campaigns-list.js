Vue.component('campaigns-list', {

    data: function () {
        return {
            items: this.campaigns
        }
    },

    props: ['campaigns'],
    template: '#campaigns-list'
});
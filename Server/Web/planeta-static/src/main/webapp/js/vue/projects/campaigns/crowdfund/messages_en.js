$(document).ready(function () {
    workspace.i18messages = workspace.i18messages || {};
    workspace.i18messages.en = {
        addnews: 'Add a news entry',
        change: 'change',
        edit: 'Edit',
        reklama: 'Advertising tools',
        "change-background": 'Change a background',
        "draft-link": 'Draft link',
        statistics: 'Statistics',
        'phone-number': 'Phone number',
        project: {
            seo: "SEO",
            contractor: 'Campaign contractor:',
            manager: "Campaign moderator:"
        },
        campaign: {
            code: 'Campaign code'
        },

        //Страница поиска акций
        share_search: {
            sort_filter: {
                rate: "By rating",
                price_asc: "Price in increasing order",
                price_desc: "Price in decreasing order",
                new: "New",
                few: "Few",
                name: "By name",
                rare: "Rare"
            },
            query: {
                placeholder: "Search for rewards ..."
            },
            share_card: {
                campaign: "Campaign",
                bought: "Purchased",
                left: "Left"
            },
            filters: {
                price: 'Price',
                from: 'from ',
                to: 'to ',
                confirm: 'Confirm',
                reset: 'Reset',
                ready: 'Ready',
                clear: 'Clear',
                campaigns: 'Campaigns'
            },
            not_found: 'Nothing was found',
            change_criteria: 'Try to change search criteria'
        },

        welcome: {
            main_slider: {
                public_finance: 'people financing',
                crowdfunding_in_russia: 'Crowdfunding in Russia',
                description: "Realize creative ideas, socially significant projects and develop business together with thousands of like-minded people!",
                already_supported: 'people already with us',
                rare_rewards: 'rewards',
                active_campaigns: 'campaigns are waiting for backing',
                choose_your: 'Choose your campaign'
            }
        }
    };
});
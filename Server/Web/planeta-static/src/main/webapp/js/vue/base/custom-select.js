Vue.component('custom-select', {

    template: '#custom-selectCampaignById',

    props: ['options', 'current'],

    data: function () {
        return {
            isOpen: false
        }
    },

    methods: {
        select: function (code) {
            this.$emit('input', code);
            this.closeDropdown();
        },

        isActive: function (code) {
            return code === this.current;
        },

        getActiveTitle: function () {
            var name = '',
                self = this;

            this.options.forEach(function (i) {
                if (i.code === self.current) {
                    name = i.name;
                }
            });

            return name;
        },

        toggle: function () {
            $('.pln-selectCampaignById').toggleClass('open');
        },

        closeDropdown: function () {
            $('.pln-selectCampaignById').removeClass('open');
        }
    },

    mounted: function () {
        var $win = $(window),
            $box = $(".pln-selectCampaignById"),
            self = this;

        $win.on("click.Bst", function(event){
            if ($box.has(event.target).length === 0 && !$box.is(event.target)) {
                self.closeDropdown();
            } else {
                if(!$(event.target).is('a')) {
                    self.toggle();
                }
            }
        });
    }
});
Vue.component('rewards-lazy-load-list', {

    template: '#rewards-lazy-load-list',

    props: ['model', 'limit'],

    watch: {
        '$props.model':{
            handler: function () {
                this.changeUrl(this.model);
                this.load();

            },
            deep: true
        }
    },

    data: function () {
        return {
            rewards: [],
            showPreloader: false,
            offset: 0,
            loading: false,
            last: false,
            notFound: false
        }
    },

    methods: {
        changeUrl: function (model) {
            var url = '/search/shares';
            var status = false;

            for(var i in model) {
                if(!Array.isArray(model[i])) {
                    if (model[i] !== 0 && model[i] !== '') {
                        if (status) {
                            url += '&' + i + '=' + model[i];
                        } else {
                            url += '?' + i + '=' + model[i];
                        }

                        if (!status) {
                            status = true;
                        }
                    }
                } else {
                    if (model[i].length > 0) {
                        model[i].forEach(function (item) {
                            if (status) {
                                url += '&' + i + '=' + item;
                            } else {
                                url += '?' + i + '=' + item;
                            }

                            if (!status) {
                                status = true;
                            }
                        });
                    }
                }
            }

            window.history.replaceState({}, document.title, url);
        },

        onScroll:function(){
            var scrollY = window.pageYOffset;
            var windowHeight = window.innerHeight;
            var listHeight = $('.reward-card').height();

            if (windowHeight + scrollY >= listHeight) {
                this.load(true);
            }
        },

        generateParams: function () {
            var requestParams = {};

            for (var key in this.model) {
                if(this.model[key] !== '' && this.model[key] && this.model[key] !== 0) {
                    requestParams[key] = this.model[key];
                }
            }

            requestParams.offset = this.offset;
            requestParams.limit = this.limit || 12;

            return requestParams;
        },

        load: function (isLoadMore) {
            var self = this;

            if (!isLoadMore) {
                self.offset = 0;
                self.last = false;
                this.rewards.splice(0);
            }

            if (!self.last && !self.loading) {
                self.showPreloader = true;
                self.loading = true;

                if (isLoadMore) {
                    self.offset += self.limit;
                }

                var params = self.generateParams();
                $.ajax({
                    type: 'get',
                    url: '/api/search/shares',
                    data: params,
                    traditional: true,
                    success: function (response) {
                        if (response.success) {
                            var result = response.result.searchResultRecords;

                            if (result.length < self.limit) {
                                self.last = true;
                            }

                            self.notFound = result.length === 0 && self.offset === 0;

                            result.forEach(function (item) {
                                self.rewards.push(item);
                            });
                            if (workspace && workspace.stats) {
                                workspace.stats.trackPageView();
                            }
                            if (window.gtm) {
                                window.gtm.trackViewRewardsList(result, 'Страница поиска вознаграждений');
                            }
                        } else {
                            workspace.appView.showErrorMessage(response.errorMessage);
                        }

                        self.loading = false;
                        self.showPreloader = false;
                    },
                    error: function(data) {
                        workspace.appView.showErrorMessage(data);
                        self.loading = false;
                        self.showPreloader = false;
                    }
                });
            }
        }
    },

    created: function () {
        this.load();
        window.addEventListener('scroll', this.onScroll);
    },

    destroyed: function () {
        window.removeEventListener('scroll', this.onScroll);
    }
});

Vue.component('reward-card', {

    template: '#reward-card',
    props: ['reward', 'page'],

    data: function () {
        return {
            item: this.reward
        }
    },

    methods: {
        getImageUrl: function (url) {
            return ImageUtils.getThumbnailUrl(url, ImageUtils.SHARE_COVER);
        },

        getHumanPrice: function (price) {
            return StringUtils.humanNumber(price);
        },

        clickCard: function (event) {
            if(window.gtm) {
                window.gtm.trackClickRewardCard(this.reward, this.page);
            }
            setTimeout(function () {
                window.location.href = event.target.attributes.href.nodeValue;
            }, 500);
        },

        clickCampaignName: function (event) {
            if(window.gtm) {
                window.gtm.trackClickRewardCard(this.reward, this.page);
            }

            setTimeout(function () {
                window.location.href = event.target.attributes.href.nodeValue;
            }, 500);
        }
    }
});
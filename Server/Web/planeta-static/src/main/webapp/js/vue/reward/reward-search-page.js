Vue.component('reward-search-page', {

    template: '#reward-search-page',

    data: function () {
        var params = this.getAllUrlParams();
        console.log(params);
        return {
            model: {
                query: params.query ? params.query : '',
                sortOrderList: params.sortOrderList ? params.sortOrderList : 'SORT_BY_PURCHASE_COUNT_DESC',
                priceFrom: params.priceFrom ? params.priceFrom : '',
                priceTo: params.priceTo ? params.priceTo : '',
                campaignTagsIds: params.campaignTagsIds ? params.campaignTagsIds : []
            },
            projectCategories: [],
            sortOrderOptions: [
                {
                    name: this.$t('share_search.sort_filter.rate'),
                    code: 'SORT_BY_PURCHASE_COUNT_DESC'
                },
                {
                    name: this.$t('share_search.sort_filter.price_asc'),
                    code: 'SORT_BY_PRICE_ASC'
                },
                {
                    name: this.$t('share_search.sort_filter.price_desc'),
                    code: 'SORT_BY_PRICE_DESC'
                },
                {
                    name: this.$t('share_search.sort_filter.new'),
                    code: 'SORT_BY_TIME_ADDED_DESC'
                },
                {
                    name: this.$t('share_search.sort_filter.few'),
                    code: 'SORT_FEW'
                },
                {
                    name: this.$t('share_search.sort_filter.rare'),
                    code: 'SORT_RARE'
                }
            ]
        }
    },

    methods: {
        queryChanged: function (query) {
            this.model.query = query;
        },

        loadProjectsCategories: function () {
            var self = this;

            this.$http.get('/api/welcome/get-campaign-tags.json').then(
                function (response) {
                    response.body.forEach(function (item) {
                        if(!item.specialProject) {
                            self.projectCategories.push(item);
                        }
                    });
                },

                function (data) {
                    workspace.appView.showErrorMessage(data);
                }
            );
        },

        getCurrentLanguageCatName: function (cat) {
            var name;

            if (workspace.currentLanguage === 'ru') {
                name = cat.name;
            } else if (workspace.currentLanguage === 'en') {
                name = cat.engName;
            }
            return name;
        },

        deselectAll: function () {
            this.model.campaignTagsIds.splice(0);
        },

        reset: function () {
            this.model.query = '';
            this.model.sortOrderList = 'SORT_BY_PURCHASE_COUNT_DESC';
            this.model.priceFrom = 0;
            this.model.priceTo = 0;
            this.model.campaignTagsIds.splice(0);
        },

        toggleFilter: function () {
            if ($('body').hasClass('reward-filter-open')) {
                $('body').removeClass('reward-filter-open');
            } else {
                $('body').addClass('reward-filter-open');
            }
        },

        getAllUrlParams: function (url) {
            var queryString = url ? url.split('?')[1] : window.location.search.slice(1);
            var obj = {};
            if (queryString) {
                queryString = queryString.split('#')[0];
                var arr = queryString.split('&');
                for (var i = 0; i < arr.length; i++) {
                    var a = arr[i].split('=');
                    var paramNum = undefined;
                    var paramName = a[0].replace(/\[\d*\]/, function (v) {
                        paramNum = v.slice(1, -1);
                        return '';
                    });
                    var paramValue = typeof(a[1]) === 'undefined' ? true : a[1];

                    if (obj[paramName]) {
                        if (typeof obj[paramName] === 'string') {
                            obj[paramName] = [obj[paramName]];
                        }
                        if (typeof paramNum === 'undefined') {
                            obj[paramName].push(paramValue);
                        }
                        else {
                            obj[paramName][paramNum] = paramValue;
                        }
                    }
                    else {
                        obj[paramName] = paramValue;
                    }
                }
            }

            // Хак
            if (obj.campaignTagsIds && typeof obj.campaignTagsIds === 'string') {
                var id = obj.campaignTagsIds;
                obj.campaignTagsIds = [];
                obj.campaignTagsIds.push(id);
            }

            return obj;
        }
    },

    mounted: function () {
        var self = this;
        this.loadProjectsCategories();

        $(document).on('urlChangedFromHamburger', function (context, url) {
            var params = self.getAllUrlParams(url);

            self.model.query = params.query ? params.query : '';
            self.model.sortOrderList = params.sortOrderList ? params.sortOrderList : 'SORT_BY_PURCHASE_COUNT_DESC';
            self.model.priceFrom = params.priceFrom ? params.priceFrom : '';
            self.model.priceTo = params.priceTo ? params.priceTo : '';
            self.model.campaignTagsIds = params.campaignTagsIds ? params.campaignTagsIds : [];
        });
    }
});
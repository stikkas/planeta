$(document).ready(function () {
    workspace.mainVueApp = new Vue({
        i18n: new VueI18n({
            locale: workspace.currentLanguage,
            messages: workspace.i18messages
        })
    });
});


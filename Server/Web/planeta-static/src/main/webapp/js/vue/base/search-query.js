Vue.component('search-query', {

    props: ['placeholder', 'query'],

    data: function () {
        return {
            plh: this.placeholder,
            query_model: decodeURI(this.query)
        }
    },

    methods: {
        changeQuery: _.debounce(function () {
            this.$emit('query_changed', this.query_model);
        }, 400)
    },

    mounted: function () {
        $('#query').focus();
    },

    template: '#search-query'
});
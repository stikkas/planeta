Vue.component('top-campaigns', {

    data: function () {
        this.load();

        return {
            campaigns: []
        }
    },

    methods: {
        load: function () {
            this.$http.get('/api/welcome/dashboard-filtered-campaigns.json', {params: {status: this.status}}).then(
                function (response) {
                    var self = this;
                    response.body.forEach(function (i) {
                        self.campaigns.push(i);
                    });
                },

                function (data, status, request) {
                    //TODO handle error
                }
            );
        }
    },

    props: ['title', 'status'],
    template: '#top-campaigns'
});
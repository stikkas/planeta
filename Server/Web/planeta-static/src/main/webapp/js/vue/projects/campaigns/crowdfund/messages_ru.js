$(document).ready(function () {
    workspace.i18messages = workspace.i18messages || {};
    workspace.i18messages.ru = {
        addnews: 'Добавить новость',
        change: 'изменить',
        edit: 'Редактировать',
        reklama: 'Рекламные инструменты',
        "change-background": 'Изменить фон',
        "draft-link": 'Ссылка на черновик',
        statistics: 'Статистика',
        'phone-number': 'тел.',
        project: {
            seo: "Продвижение проекта",
            contractor: 'Контрагент проекта:',
            manager: "Менеджер проекта:"
        },
        campaign: {
            code: 'Код проекта для вставки на сайт'
        },

        //Страница поиска акций
        share_search: {
            sort_filter: {
                rate: "По популярности",
                price_asc: "Стоимость по возрастанию",
                price_desc: "Стоимость по убыванию",
                new: "Новые",
                few: "Осталось мало",
                name: "По названию",
                rare: "Редкие"
            },
            query: {
                placeholder: "Поиск по вознаграждениям..."
            },
            share_card: {
                campaign: "Проект",
                bought: "Куплено",
                left: "Осталось"
            },
            filters: {
                price: 'Стоимость',
                from: 'от ',
                to: 'до ',
                confirm: 'Применить',
                reset: 'Сбросить',
                ready: 'Готово',
                clear: 'Очистить',
                campaigns: 'Проекты'
            },
            not_found: 'Ничего не найдено',
            change_criteria: 'Попробуйте изменить критерии поиска'
        },

        welcome: {
            main_slider: {
                public_finance: 'Народное финансирование',
                crowdfunding_in_russia: 'Краудфандинг в России',
                description: 'Воплощайте в жизнь творческие идеи, социально значимые проекты и развивайте бизнес вместе с тысячами единомышленников!',
                already_supported: 'человек уже с нами',
                rare_rewards: 'вознаграждений',
                active_campaigns: 'проектов ждут поддержки',
                choose_your: 'Выбери свой проект'
            }
        }
    };
});

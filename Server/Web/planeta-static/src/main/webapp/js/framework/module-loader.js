
var ModuleLoader = Backbone.Model.extend({
    modules: {},
    _loadModules: {},
    loadModules: function (moduleList) {
        if (_.isEmpty(moduleList)) {
            return;
        }
        if (moduleList.length === 1) {
            return this.loadModule(moduleList[0]);
        }
        var self = this;

        function load(ml, dfd) {
            var firstModule = ml.shift();
            if (!dfd) {
                dfd = $.Deferred();
            }
            self.loadModule(firstModule).done(function () {
                if (ml.length) {
                    load(ml, dfd);
                } else {
                    dfd.resolve();
                }
            }).fail(function () {
                dfd.reject();
            });
            return dfd;
        }

        return load(moduleList);
    },
    loadModule: function (moduleName, ms) {
        if (!this.modules[moduleName]) {
            var dfd = $.Deferred();
            var self = this;
            this.modules[moduleName] = dfd;
            $(document).ready(function () {
                if (ms) {
                    self.modules[moduleName].isTimeout = true;
                    setTimeout(function () {
                        self._loadModule(moduleName);
                    }, ms);
                } else {
                    self._loadModule(moduleName);
                }
            });
        } else if (this.modules[moduleName].isTimeout && !ms) {
            this._loadModule(moduleName);
        }
        return this.modules[moduleName].promise();
    },
    _loadModule: function (moduleName) {
        if (!this._loadModules[moduleName]) {
            this._loadModules[moduleName] = this._getScript(moduleName);
        }
        this._loadModules[moduleName].then(this.modules[moduleName].resolve, this.modules[moduleName].reject);
    },
    _getScript: function (moduleName, staticNodesService) {
        var staticNodesService1 = staticNodesService || workspace.staticNodesService;
        return window.jQuery2.ajax({
            url: staticNodesService1.getJsUrl('js/' + moduleName + '.js'),
            dataType: 'script',
            cache: true
        });
    },
    findClass: function (className) {
        return _.reduce(className.split("."), function (obj, val) {
            if (obj) {
                return obj[val];
            }
        }, window);
    },
    hasModuleLoaded: function (moduleName) {
        return this.modules[moduleName] && this.modules[moduleName].isResolved();
    },
    hasClassLoaded: function (className) {
        return !!this.findClass(className);
    }
});
var moduleLoader = new ModuleLoader();
var loadModule = function () {
    return moduleLoader.loadModule.apply(moduleLoader, arguments);
};
var loadModules = function () {
    return moduleLoader.loadModules.apply(moduleLoader, arguments);
};


(function () {
    var arr = [];
    var push = arr.push;
    var support = {};
    var document = window.document,
            jQuery = function (selector, context) {
                return new jQuery.fn.init(selector, context);
            };

    jQuery.fn = jQuery.prototype = {
        constructor: jQuery,
        selector: "",
        length: 0,
        each: function (callback, args) {
            return jQuery.each(this, callback, args);
        },
        push: push,
        sort: arr.sort,
        splice: arr.splice
    };

    jQuery.extend = jQuery.fn.extend = $.extend;
    jQuery.extend({
        isReady: true,
        error: $.error,
        noop: $.noop,
        isFunction: $.isFunction,
        isPlainObject: $.isPlainObject,
        isEmptyObject: $.isEmptyObject,
        type: $.type,
        globalEval: $.globalEval,
        each: $.each,
        trim: $.trim,
        makeArray: $.makeArray,
        merge: $.merge,
        grep: $.grep,
        map: $.map,
        guid: 1,
        proxy: $.proxy,
        now: Date.now,
        support: support
    });

    var rsingleTag = (/^<(\w+)\s*\/?>(?:<\/\1>|)$/);
    var rootjQuery,
            rquickExpr = /^(?:\s*(<[\w\W]+>)[^>]*|#([\w-]*))$/,
            init = jQuery.fn.init = function (selector, context) {
                if (typeof selector === "string") {
                    if (selector[0] === "<" && selector[ selector.length - 1 ] === ">" && selector.length >= 3) {
                        match = [null, selector, null];
                    } else {
                        match = rquickExpr.exec(selector);
                    }
                    if (match && (match[1] || !context)) {
                        if (match[1]) {
                            context = context instanceof jQuery ? context[0] : context;
                            jQuery.merge(this, jQuery.parseHTML(
                                    match[1],
                                    context && context.nodeType ? context.ownerDocument || context : document,
                                    true
                                    ));
                            if (rsingleTag.test(match[1]) && jQuery.isPlainObject(context)) {
                                for (match in context) {
                                    if (jQuery.isFunction(this[ match ])) {
                                        this[ match ](context[ match ]);
                                    } else {
                                        this.attr(match, context[ match ]);
                                    }
                                }
                            }
                            return this;
                        } else {
                            elem = document.getElementById(match[2]);
                            if (elem && elem.parentNode) {
                                // Inject the element directly into the jQuery object
                                this.length = 1;
                                this[0] = elem;
                            }

                            this.context = document;
                            this.selector = selector;
                            return this;
                        }
                    } else if (!context || context.jquery) {
                        return (context || rootjQuery).find(selector);
                    } else {
                        return this.constructor(context).find(selector);
                    }
                } else if (selector.nodeType) {
                    this.context = this[0] = selector;
                    this.length = 1;
                    return this;
                } else if (jQuery.isFunction(selector)) {
                    return typeof rootjQuery.ready !== "undefined" ?
                            rootjQuery.ready(selector) :
                            selector(jQuery);
                }

                if (selector.selector !== undefined) {
                    this.selector = selector.selector;
                    this.context = selector.context;
                }

                return jQuery.makeArray(selector, this);
            };
    init.prototype = jQuery.fn;
    rootjQuery = jQuery(document);
    var rnotwhite = (/\S+/g);
    var readyList;
    jQuery.fn.ready = function (fn) {
        jQuery.ready.promise().done(fn);
        return this;
    };
    jQuery.extend({
        Deferred: $.Deferred,
        when: $.when,
        isReady: $.isReady,
        readyWait: 1,
        holdReady: $.holdReady,
        ready: $.ready
    });
    function completed() {
        document.removeEventListener("DOMContentLoaded", completed, false);
        window.removeEventListener("load", completed, false);
        jQuery.ready();
    }
    jQuery.ready.promise = function (obj) {
        if (!readyList) {
            readyList = jQuery.Deferred();
            if (document.readyState === "complete") {
                setTimeout(jQuery.ready);
            } else {
                document.addEventListener("DOMContentLoaded", completed, false);
                window.addEventListener("load", completed, false);
            }
        }
        return readyList.promise(obj);
    };
    jQuery.ready.promise();
    jQuery.event = $.event;
    jQuery.removeEvent = $.removeEvent;
    jQuery.Event = $.Event;
    jQuery.extend({
        cleanData: $.cleanData,
        propFix: $.propFix,
        prop: $.prop,
        propHooks: $.propHooks
    });
    jQuery.fn.extend({
        on: $.fn.on,
        one: $.fn.one,
        off: $.fn.off,
        trigger: $.fn.trigger,
        triggerHandler: $.fn.triggerHandler,
        remove: $.fn.remove,
        prop: $.fn.prop,
        removeProp: $.fn.removeProp,
        data: $.fn.data,
        removeData: $.fn.removeData
    });

    var nonce = jQuery.now();
    var rquery = (/\?/);
    var rts = /([?&])_=[^&]*/,
            rheaders = /^(.*?):[ \t]*([^\r\n]*)$/mg,
            rlocalProtocol = /^(?:about|app|app-storage|.+-extension|file|res|widget):$/,
            rnoContent = /^(?:GET|HEAD)$/,
            rprotocol = /^\/\//,
            rurl = /^([\w.+-]+:)(?:\/\/(?:[^\/?#]*@|)([^\/?#:]*)(?::(\d+)|)|)/,
            prefilters = {},
            transports = {},
            allTypes = "*/".concat("*"),
            ajaxLocation = window.location.href,
            ajaxLocParts = rurl.exec(ajaxLocation.toLowerCase()) || [];
    function addToPrefiltersOrTransports(structure) {
        return function (dataTypeExpression, func) {

            if (typeof dataTypeExpression !== "string") {
                func = dataTypeExpression;
                dataTypeExpression = "*";
            }

            var dataType,
                    i = 0,
                    dataTypes = dataTypeExpression.toLowerCase().match(rnotwhite) || [];

            if (jQuery.isFunction(func)) {
                while ((dataType = dataTypes[i++])) {
                    if (dataType[0] === "+") {
                        dataType = dataType.slice(1) || "*";
                        (structure[ dataType ] = structure[ dataType ] || []).unshift(func);
                    } else {
                        (structure[ dataType ] = structure[ dataType ] || []).push(func);
                    }
                }
            }
        };
    }
    function inspectPrefiltersOrTransports(structure, options, originalOptions, jqXHR) {
        var inspected = {},
                seekingTransport = (structure === transports);

        function inspect(dataType) {
            var selected;
            inspected[ dataType ] = true;
            jQuery.each(structure[ dataType ] || [], function (_, prefilterOrFactory) {
                var dataTypeOrTransport = prefilterOrFactory(options, originalOptions, jqXHR);
                if (typeof dataTypeOrTransport === "string" && !seekingTransport && !inspected[ dataTypeOrTransport ]) {
                    options.dataTypes.unshift(dataTypeOrTransport);
                    inspect(dataTypeOrTransport);
                    return false;
                } else if (seekingTransport) {
                    return !(selected = dataTypeOrTransport);
                }
            });
            return selected;
        }

        return inspect(options.dataTypes[ 0 ]) || !inspected[ "*" ] && inspect("*");
    }
    function ajaxExtend(target, src) {
        var key, deep,
                flatOptions = jQuery.ajaxSettings.flatOptions || {};
        for (key in src) {
            if (src[ key ] !== undefined) {
                (flatOptions[ key ] ? target : (deep || (deep = {})))[ key ] = src[ key ];
            }
        }
        if (deep) {
            jQuery.extend(true, target, deep);
        }
        return target;
    }
    function ajaxHandleResponses(s, jqXHR, responses) {
        var ct, type, finalDataType, firstDataType,
                contents = s.contents,
                dataTypes = s.dataTypes;
        while (dataTypes[ 0 ] === "*") {
            dataTypes.shift();
            if (ct === undefined) {
                ct = s.mimeType || jqXHR.getResponseHeader("Content-Type");
            }
        }
        if (ct) {
            for (type in contents) {
                if (contents[ type ] && contents[ type ].test(ct)) {
                    dataTypes.unshift(type);
                    break;
                }
            }
        }
        if (dataTypes[ 0 ] in responses) {
            finalDataType = dataTypes[ 0 ];
        } else {
            for (type in responses) {
                if (!dataTypes[ 0 ] || s.converters[ type + " " + dataTypes[0] ]) {
                    finalDataType = type;
                    break;
                }
                if (!firstDataType) {
                    firstDataType = type;
                }
            }
            finalDataType = finalDataType || firstDataType;
        }
        if (finalDataType) {
            if (finalDataType !== dataTypes[ 0 ]) {
                dataTypes.unshift(finalDataType);
            }
            return responses[ finalDataType ];
        }
    }
    function ajaxConvert(s, response, jqXHR, isSuccess) {
        var conv2, current, conv, tmp, prev,
                converters = {},
                dataTypes = s.dataTypes.slice();
        if (dataTypes[ 1 ]) {
            for (conv in s.converters) {
                converters[ conv.toLowerCase() ] = s.converters[ conv ];
            }
        }
        current = dataTypes.shift();
        while (current) {
            if (s.responseFields[ current ]) {
                jqXHR[ s.responseFields[ current ] ] = response;
            }
            if (!prev && isSuccess && s.dataFilter) {
                response = s.dataFilter(response, s.dataType);
            }
            prev = current;
            current = dataTypes.shift();
            if (current) {
                if (current === "*") {
                    current = prev;
                } else if (prev !== "*" && prev !== current) {
                    conv = converters[ prev + " " + current ] || converters[ "* " + current ];
                    if (!conv) {
                        for (conv2 in converters) {
                            tmp = conv2.split(" ");
                            if (tmp[ 1 ] === current) {
                                conv = converters[ prev + " " + tmp[ 0 ] ] ||
                                        converters[ "* " + tmp[ 0 ] ];
                                if (conv) {
                                    if (conv === true) {
                                        conv = converters[ conv2 ];
                                    } else if (converters[ conv2 ] !== true) {
                                        current = tmp[ 0 ];
                                        dataTypes.unshift(tmp[ 1 ]);
                                    }
                                    break;
                                }
                            }
                        }
                    }
                    if (conv !== true) {
                        if (conv && s[ "throws" ]) {
                            response = conv(response);
                        } else {
                            try {
                                response = conv(response);
                            } catch (e) {
                                return {state: "parsererror", error: conv ? e : "No conversion from " + prev + " to " + current};
                            }
                        }
                    }
                }
            }
        }
        return {state: "success", data: response};
    }
    jQuery.extend({
        active: 0,
        lastModified: {},
        etag: {},
        ajaxSettings: {
            url: ajaxLocation,
            type: "GET",
            isLocal: rlocalProtocol.test(ajaxLocParts[ 1 ]),
            global: true,
            processData: true,
            async: true,
            contentType: "application/x-www-form-urlencoded; charset=UTF-8",
            accepts: {
                "*": allTypes,
                text: "text/plain",
                html: "text/html",
                xml: "application/xml, text/xml",
                json: "application/json, text/javascript"
            },
            contents: {
                xml: /xml/,
                html: /html/,
                json: /json/
            },
            responseFields: {
                xml: "responseXML",
                text: "responseText",
                json: "responseJSON"
            },
            converters: {
                "* text": String,
                "text html": true,
                "text json": $.parseJSON,
                "text xml": $.parseXML
            },
            flatOptions: {
                url: true,
                context: true
            }
        },
        ajaxSetup: function (target, settings) {
            return settings ?
                    ajaxExtend(ajaxExtend(target, jQuery.ajaxSettings), settings) :
                    ajaxExtend(jQuery.ajaxSettings, target);
        },
        ajaxPrefilter: addToPrefiltersOrTransports(prefilters),
        ajaxTransport: addToPrefiltersOrTransports(transports),
        ajax: function (url, options) {
            if (typeof url === "object") {
                options = url;
                url = undefined;
            }
            options = options || {};
            var transport,
                    cacheURL,
                    responseHeadersString,
                    responseHeaders,
                    timeoutTimer,
                    parts,
                    fireGlobals,
                    i,
                    s = jQuery.ajaxSetup({}, options),
                    callbackContext = s.context || s,
                    globalEventContext = s.context && (callbackContext.nodeType || callbackContext.jquery) ?
                    jQuery(callbackContext) :
                    jQuery.event,
                    deferred = jQuery.Deferred(),
                    completeDeferred = $.Callbacks("once memory"),
                    statusCode = s.statusCode || {},
                    requestHeaders = {},
                    requestHeadersNames = {},
                    state = 0,
                    strAbort = "canceled",
                    jqXHR = {
                        readyState: 0,
                        getResponseHeader: function (key) {
                            var match;
                            if (state === 2) {
                                if (!responseHeaders) {
                                    responseHeaders = {};
                                    while ((match = rheaders.exec(responseHeadersString))) {
                                        responseHeaders[ match[1].toLowerCase() ] = match[ 2 ];
                                    }
                                }
                                match = responseHeaders[ key.toLowerCase() ];
                            }
                            return match == null ? null : match;
                        },
                        getAllResponseHeaders: function () {
                            return state === 2 ? responseHeadersString : null;
                        },
                        setRequestHeader: function (name, value) {
                            var lname = name.toLowerCase();
                            if (!state) {
                                name = requestHeadersNames[ lname ] = requestHeadersNames[ lname ] || name;
                                requestHeaders[ name ] = value;
                            }
                            return this;
                        },
                        overrideMimeType: function (type) {
                            if (!state) {
                                s.mimeType = type;
                            }
                            return this;
                        },
                        statusCode: function (map) {
                            var code;
                            if (map) {
                                if (state < 2) {
                                    for (code in map) {
                                        statusCode[ code ] = [statusCode[ code ], map[ code ]];
                                    }
                                } else {
                                    jqXHR.always(map[ jqXHR.status ]);
                                }
                            }
                            return this;
                        },
                        abort: function (statusText) {
                            var finalText = statusText || strAbort;
                            if (transport) {
                                transport.abort(finalText);
                            }
                            done(0, finalText);
                            return this;
                        }
                    };
            deferred.promise(jqXHR).complete = completeDeferred.add;
            jqXHR.success = jqXHR.done;
            jqXHR.error = jqXHR.fail;
            s.url = ((url || s.url || ajaxLocation) + "").replace(/#.*$/, "")
                    .replace(rprotocol, ajaxLocParts[ 1 ] + "//");
            s.type = options.method || options.type || s.method || s.type;
            s.dataTypes = jQuery.trim(s.dataType || "*").toLowerCase().match(rnotwhite) || [""];
            if (s.crossDomain == null) {
                parts = rurl.exec(s.url.toLowerCase());
                s.crossDomain = !!(parts &&
                        (parts[ 1 ] !== ajaxLocParts[ 1 ] || parts[ 2 ] !== ajaxLocParts[ 2 ] ||
                                (parts[ 3 ] || (parts[ 1 ] === "http:" ? "80" : "443")) !==
                                (ajaxLocParts[ 3 ] || (ajaxLocParts[ 1 ] === "http:" ? "80" : "443")))
                        );
            }
            if (s.data && s.processData && typeof s.data !== "string") {
                s.data = jQuery.param(s.data, s.traditional);
            }
            inspectPrefiltersOrTransports(prefilters, s, options, jqXHR);
            if (state === 2) {
                return jqXHR;
            }
            fireGlobals = jQuery.event && s.global;
            if (fireGlobals && jQuery.active++ === 0) {
                jQuery.event.trigger("ajaxStart");
            }
            s.type = s.type.toUpperCase();
            s.hasContent = !rnoContent.test(s.type);
            cacheURL = s.url;
            if (!s.hasContent) {
                if (s.data) {
                    cacheURL = (s.url += (rquery.test(cacheURL) ? "&" : "?") + s.data);
                    delete s.data;
                }
                if (s.cache === false) {
                    s.url = rts.test(cacheURL) ?
                            cacheURL.replace(rts, "$1_=" + nonce++) :
                            cacheURL + (rquery.test(cacheURL) ? "&" : "?") + "_=" + nonce++;
                }
            }
            if (s.ifModified) {
                if (jQuery.lastModified[ cacheURL ]) {
                    jqXHR.setRequestHeader("If-Modified-Since", jQuery.lastModified[ cacheURL ]);
                }
                if (jQuery.etag[ cacheURL ]) {
                    jqXHR.setRequestHeader("If-None-Match", jQuery.etag[ cacheURL ]);
                }
            }
            if (s.data && s.hasContent && s.contentType !== false || options.contentType) {
                jqXHR.setRequestHeader("Content-Type", s.contentType);
            }
            jqXHR.setRequestHeader(
                    "Accept",
                    s.dataTypes[ 0 ] && s.accepts[ s.dataTypes[0] ] ?
                    s.accepts[ s.dataTypes[0] ] + (s.dataTypes[ 0 ] !== "*" ? ", " + allTypes + "; q=0.01" : "") :
                    s.accepts[ "*" ]
                    );
            for (i in s.headers) {
                jqXHR.setRequestHeader(i, s.headers[ i ]);
            }
            if (s.beforeSend && (s.beforeSend.call(callbackContext, jqXHR, s) === false || state === 2)) {
                return jqXHR.abort();
            }
            strAbort = "abort";
            for (i in {success: 1, error: 1, complete: 1}) {
                jqXHR[ i ](s[ i ]);
            }
            transport = inspectPrefiltersOrTransports(transports, s, options, jqXHR);
            if (!transport) {
                done(-1, "No Transport");
            } else {
                jqXHR.readyState = 1;
                if (fireGlobals) {
                    globalEventContext.trigger("ajaxSend", [jqXHR, s]);
                }
                // Timeout
                if (s.async && s.timeout > 0) {
                    timeoutTimer = setTimeout(function () {
                        jqXHR.abort("timeout");
                    }, s.timeout);
                }

                try {
                    state = 1;
                    transport.send(requestHeaders, done);
                } catch (e) {
                    if (state < 2) {
                        done(-1, e);
                    } else {
                        throw e;
                    }
                }
            }
            function done(status, nativeStatusText, responses, headers) {
                var isSuccess, success, error, response, modified,
                        statusText = nativeStatusText;
                if (state === 2) {
                    return;
                }
                state = 2;
                if (timeoutTimer) {
                    clearTimeout(timeoutTimer);
                }
                transport = undefined;
                responseHeadersString = headers || "";
                jqXHR.readyState = status > 0 ? 4 : 0;
                isSuccess = status >= 200 && status < 300 || status === 304;
                if (responses) {
                    response = ajaxHandleResponses(s, jqXHR, responses);
                }
                response = ajaxConvert(s, response, jqXHR, isSuccess);
                if (isSuccess) {
                    if (s.ifModified) {
                        modified = jqXHR.getResponseHeader("Last-Modified");
                        if (modified) {
                            jQuery.lastModified[ cacheURL ] = modified;
                        }
                        modified = jqXHR.getResponseHeader("etag");
                        if (modified) {
                            jQuery.etag[ cacheURL ] = modified;
                        }
                    }
                    if (status === 204 || s.type === "HEAD") {
                        statusText = "nocontent";
                    } else if (status === 304) {
                        statusText = "notmodified";
                    } else {
                        statusText = response.state;
                        success = response.data;
                        error = response.error;
                        isSuccess = !error;
                    }
                } else {
                    error = statusText;
                    if (status || !statusText) {
                        statusText = "error";
                        if (status < 0) {
                            status = 0;
                        }
                    }
                }
                jqXHR.status = status;
                jqXHR.statusText = (nativeStatusText || statusText) + "";
                if (isSuccess) {
                    deferred.resolveWith(callbackContext, [success, statusText, jqXHR]);
                } else {
                    deferred.rejectWith(callbackContext, [jqXHR, statusText, error]);
                }
                jqXHR.statusCode(statusCode);
                statusCode = undefined;
                if (fireGlobals) {
                    globalEventContext.trigger(isSuccess ? "ajaxSuccess" : "ajaxError",
                            [jqXHR, s, isSuccess ? success : error]);
                }
                completeDeferred.fireWith(callbackContext, [jqXHR, statusText]);
                if (fireGlobals) {
                    globalEventContext.trigger("ajaxComplete", [jqXHR, s]);
                    if (!(--jQuery.active)) {
                        jQuery.event.trigger("ajaxStop");
                    }
                }
            }
            return jqXHR;
        }
    });
    jQuery.each(["get", "post"], function (i, method) {
        jQuery[ method ] = function (url, data, callback, type) {
            // Shift arguments if data argument was omitted
            if (jQuery.isFunction(data)) {
                type = type || callback;
                callback = data;
                data = undefined;
            }

            return jQuery.ajax({
                url: url,
                type: method,
                dataType: type,
                data: data,
                success: callback
            });
        };
    });
    jQuery._evalUrl = function (url) {
        return jQuery.ajax({
            url: url,
            type: "GET",
            dataType: "script",
            async: false,
            global: false,
            "throws": true
        });
    };
    var r20 = /%20/g,
            rbracket = /\[\]$/;

    function buildParams(prefix, obj, traditional, add) {
        var name;
        if (jQuery.isArray(obj)) {
            jQuery.each(obj, function (i, v) {
                if (traditional || rbracket.test(prefix)) {
                    add(prefix, v);

                } else {
                    buildParams(prefix + "[" + (typeof v === "object" ? i : "") + "]", v, traditional, add);
                }
            });
        } else if (!traditional && jQuery.type(obj) === "object") {
            for (name in obj) {
                buildParams(prefix + "[" + name + "]", obj[ name ], traditional, add);
            }
        } else {
            add(prefix, obj);
        }
    }
    jQuery.param = function (a, traditional) {
        var prefix,
                s = [],
                add = function (key, value) {
                    // If value is a function, invoke it and return its value
                    value = jQuery.isFunction(value) ? value() : (value == null ? "" : value);
                    s[ s.length ] = encodeURIComponent(key) + "=" + encodeURIComponent(value);
                };
        if (traditional === undefined) {
            traditional = jQuery.ajaxSettings && jQuery.ajaxSettings.traditional;
        }
        if (jQuery.isArray(a) || (a.jquery && !jQuery.isPlainObject(a))) {
            jQuery.each(a, function () {
                add(this.name, this.value);
            });
        } else {
            for (prefix in a) {
                buildParams(prefix, a[ prefix ], traditional, add);
            }
        }
        return s.join("&").replace(r20, "+");
    };
    jQuery.ajaxSettings.xhr = function () {
        try {
            return new XMLHttpRequest();
        } catch (e) {
        }
    };
    var xhrId = 0,
            xhrCallbacks = {},
            xhrSuccessStatus = {
                0: 200,
                1223: 204
            },
    xhrSupported = jQuery.ajaxSettings.xhr();
    if (window.attachEvent) {
        window.attachEvent("onunload", function () {
            for (var key in xhrCallbacks) {
                xhrCallbacks[ key ]();
            }
        });
    }
    support.cors = !!xhrSupported && ("withCredentials" in xhrSupported);
    support.ajax = xhrSupported = !!xhrSupported;
    jQuery.ajaxTransport(function (options) {
        var callback;
        if (support.cors || xhrSupported && !options.crossDomain) {
            return {
                send: function (headers, complete) {
                    var i,
                            xhr = options.xhr(),
                            id = ++xhrId;
                    xhr.open(options.type, options.url, options.async, options.username, options.password);
                    if (options.xhrFields) {
                        for (i in options.xhrFields) {
                            xhr[ i ] = options.xhrFields[ i ];
                        }
                    }
                    if (options.mimeType && xhr.overrideMimeType) {
                        xhr.overrideMimeType(options.mimeType);
                    }

                    if (!options.crossDomain && !headers["X-Requested-With"]) {
                        headers["X-Requested-With"] = "XMLHttpRequest";
                    }
                    for (i in headers) {
                        xhr.setRequestHeader(i, headers[ i ]);
                    }
                    callback = function (type) {
                        return function () {
                            if (callback) {
                                delete xhrCallbacks[ id ];
                                callback = xhr.onload = xhr.onerror = null;
                                if (type === "abort") {
                                    xhr.abort();
                                } else if (type === "error") {
                                    complete(
                                            xhr.status,
                                            xhr.statusText
                                            );
                                } else {
                                    complete(xhrSuccessStatus[ xhr.status ] || xhr.status,
                                            xhr.statusText,
                                            typeof xhr.responseText === "string" ? {
                                                text: xhr.responseText
                                            } : undefined,
                                            xhr.getAllResponseHeaders()
                                            );
                                }
                            }
                        };
                    };
                    xhr.onload = callback();
                    xhr.onerror = callback("error");
                    callback = xhrCallbacks[ id ] = callback("abort");

                    try {
                        xhr.send(options.hasContent && options.data || null);
                    } catch (e) {
                        if (callback) {
                            throw e;
                        }
                    }
                },
                abort: function () {
                    if (callback) {
                        callback();
                    }
                }
            };
        }
    });
    jQuery.ajaxSetup({
        accepts: {
            script: "text/javascript, application/javascript, application/ecmascript, application/x-ecmascript"
        },
        contents: {
            script: /(?:java|ecma)script/
        },
        converters: {
            "text script": function (text) {
                jQuery.globalEval(text);
                return text;
            }
        }
    });
    jQuery.ajaxPrefilter("script", function (s) {
        if (s.cache === undefined) {
            s.cache = false;
        }
        if (s.crossDomain) {
            s.type = "GET";
        }
    });
    jQuery.ajaxTransport("script", function (s) {
        if (s.crossDomain) {
            var script, callback;
            return {
                send: function (_, complete) {
                    script = jQuery("<script>").prop({
                        async: true,
                        charset: s.scriptCharset,
                        src: s.url
                    }).on("load error",
                            callback = function (evt) {
                                script.remove();
                                callback = null;
                                if (evt) {
                                    complete(evt.type === "error" ? 404 : 200, evt.type);
                                }
                            });
                    document.head.appendChild(script[ 0 ]);
                },
                abort: function () {
                    if (callback) {
                        callback();
                    }
                }
            };
        }
    });
    var oldCallbacks = [],
            rjsonp = /(=)\?(?=&|$)|\?\?/;
    jQuery.ajaxSetup({
        jsonp: "callback",
        jsonpCallback: function () {
            var callback = oldCallbacks.pop() || (jQuery.expando + "_" + (nonce++));
            this[ callback ] = true;
            return callback;
        }
    });
    jQuery.ajaxPrefilter("json jsonp", function (s, originalSettings, jqXHR) {
        var callbackName, overwritten, responseContainer,
                jsonProp = s.jsonp !== false && (rjsonp.test(s.url) ?
                        "url" :
                        typeof s.data === "string" && !(s.contentType || "").indexOf("application/x-www-form-urlencoded") && rjsonp.test(s.data) && "data"
                        );
        if (jsonProp || s.dataTypes[ 0 ] === "jsonp") {
            callbackName = s.jsonpCallback = jQuery.isFunction(s.jsonpCallback) ?
                    s.jsonpCallback() :
                    s.jsonpCallback;
            if (jsonProp) {
                s[ jsonProp ] = s[ jsonProp ].replace(rjsonp, "$1" + callbackName);
            } else if (s.jsonp !== false) {
                s.url += (rquery.test(s.url) ? "&" : "?") + s.jsonp + "=" + callbackName;
            }
            s.converters["script json"] = function () {
                if (!responseContainer) {
                    jQuery.error(callbackName + " was not called");
                }
                return responseContainer[ 0 ];
            };
            s.dataTypes[ 0 ] = "json";
            overwritten = window[ callbackName ];
            window[ callbackName ] = function () {
                responseContainer = arguments;
            };
            jqXHR.always(function () {
                window[ callbackName ] = overwritten;
                if (s[ callbackName ]) {
                    s.jsonpCallback = originalSettings.jsonpCallback;
                    oldCallbacks.push(callbackName);
                }
                if (responseContainer && jQuery.isFunction(overwritten)) {
                    overwritten(responseContainer[ 0 ]);
                }

                responseContainer = overwritten = undefined;
            });
            return "script";
        }
    });

    jQuery.parseHTML = function (data, context, keepScripts) {
        if (!data || typeof data !== "string") {
            return null;
        }
        if (typeof context === "boolean") {
            keepScripts = context;
            context = false;
        }
        context = context || document;
        var parsed = rsingleTag.exec(data),
                scripts = !keepScripts && [];
        if (parsed) {
            return [context.createElement(parsed[1])];
        }
        parsed = jQuery.buildFragment([data], context, scripts);

        if (scripts && scripts.length) {
            jQuery(scripts).remove();
        }
        return jQuery.merge([], parsed.childNodes);
    };

    jQuery.each(["ajaxStart", "ajaxStop", "ajaxComplete", "ajaxError", "ajaxSuccess", "ajaxSend"], function (i, type) {
        jQuery.fn[ type ] = function (fn) {
            return this.on(type, fn);
        };
    });
    window.jQuery2 = jQuery;
}());



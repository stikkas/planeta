/*global console,counters*/
/**
 * Backbone.sync implementation for planeta.ru project.
 *
 * TODO: Use ajax manager or cache response here
 */
var Methods = {
    'create': 'POST',
    'update': 'POST',
    'delete': 'POST',
    'read': 'GET'
};

(function () {
    Backbone._serviceUnavailable = false;
    Backbone.baseUrl = null;
    Backbone.jsonp = 'callback';
    Backbone.timeout = 60000;
    Backbone.retryMax = 5;

    // register AJAX prefilter : options, original options
    $.ajaxPrefilter(function (options, originalOptions, jqXHR) {

        // retry not set or less than 2 : retry not requested
        if (!originalOptions.retryMax || originalOptions.retryMax < 2) {
            return;
        }
        // no timeout was setup
        if (originalOptions.timeout <= 0) {
            return;
        }

        if (originalOptions.retryCount) {
            // increment retry count each time
            originalOptions.retryCount++;
        } else {
            // init the retry count if not set
            originalOptions.retryCount = 1;
            // copy original error callback on first time
            originalOptions._error = originalOptions.error;
        }

        // overwrite error handler for current request
        options.error = function (_jqXHR, _textStatus, _errorThrown) {

            // retry max was exhausted or it is not a timeout error
            if (originalOptions.retryCount >= originalOptions.retryMax || _textStatus !== 'timeout') {
                // call original error handler if any
                if (originalOptions._error) {
                    originalOptions._error(_jqXHR, _textStatus, _errorThrown);
                }
                return;
            }
            originalOptions.timeout = originalOptions.timeout + Backbone.timeout;
            // Call AJAX again with original options
            $.ajax(originalOptions);
        };
    });

    var getUrl = function (object) {
        if (!(object && object.url)) {
            return null;
        }
        return _.isFunction(object.url) ? object.url() : object.url;
    };

    var urlError = function () {
        throw new Error('A "url" property or function must be specified');
    };

    /**
     * Checks response status and may:
     * 1. Reload location if we have lost authorization somehow
     * 2. Call serviceUnavailable callback for 502/503/504 statuses (if request params do not have ignoreErrors field)
     *
     * @param options
     * @param xhr
     */
    var checkResponseStatus = function (options, xhr) {
        if (!xhr || !xhr.status) {
            // request just before page change (cancelled) - we don't want response
            return;
        }

        //noinspection JSLint
        switch (xhr.status) {
            case 401:
                //noinspection JSLint
                location.reload();
                //noinspection JSLint
                break;
            case 502:
            case 503:
            case 504:
                if (options && options.ignoreErrors) {
                    // Ignore server errors and do not call service unavailable callback
                    return;
                }

                // Service is currently unavailable (maybe deploy or smth)
                if (!Backbone._serviceUnavailable) {
                    Backbone._serviceUnavailable = true;
                    if (Backbone.onServiceUnavailable) {
                        Backbone.onServiceUnavailable(xhr.status);
                    }
                }
                break;
        }
    };

    var prepareParams = function (method, model, options) {
        var type = Methods[method];
        var params = _.extend({
            type: type,
            dataType: 'json',
            timeout: Backbone.timeout,
            retryMax: type === Methods.read ? Backbone.retryMax : 0 // we can retry ajax queries in case read queries only
        }, options);

        // if (window.debugMode)
        params.cache = false;

        if (!params.url) {
            params.url = getUrl(model) || urlError();
        }

        // default format is form-urlencoded

        if (!params.data && model && model.toJSON && (method === 'create' || method === 'update' || method === 'delete')) {
            params.data = model.toJSON();
        }
        params.data = params.data || {};

        if (params.contentType === 'application/json') {
            params.data = JSON.stringify(params.data);
        }


        if (StringUtils.isAbsoluteUrl(params.url)) {
            params.dataType = 'jsonp';
            params.crossDomain = true;
            if (!params.timeout) {
                params.timeout = Backbone.timeout;
            }
        }
        return params;
    };

    var processFilters = function (options) {
        //noinspection JSLint
        for (var i = 0; i < filters.length; i++) {
            var filter = filters[i];
            var dfd = filter(options);
            if (dfd) {
                return dfd;
            }
        }
    };

    var filters = [];
    Backbone.addSyncFilter = function (filter) {
        Backbone.removeSyncFilter(filter);
        filters.push(filter);
    };

    Backbone.removeSyncFilter = function (filter) {
        _.each(filters, function (curFilter) {
            if (curFilter === filter) {
                curFilter = null;
            }
        });
        filters = _.compact(filters);
    };

    Backbone.sync = function (method, model, options) {
        var params = prepareParams(method, model, options);

        var dfd = processFilters(params);
        if (dfd) {
            return dfd;
        }

        _.extend(params, {
            success: function (result, textStatus, jqXHR) {
                if (result && result.success === false && result.errorClass) {
                    if (options.confirm)
                        options.confirm(result);
                    else { 
                        if (window.debugMode) {
                            console.error("Url: " + params.url, result.errorClass, result.errorMessage);
                            console.log("params:", params, "result:", result);
                            workspace.appView.showErrorMessage("Url: " + params.url + "<br/>" + result.errorMessage + "<br/>" + result.errorClass);
                        } else {
                            workspace.appView.showErrorMessage(result.errorMessage);
                        }
                    }
                }

                if (options.success) {
                    options.success.apply(this, arguments);
                }
            }
        });

        _.extend(params, {
            complete: function (xhr, textStatus) {
                onRequestCompleted.call(this, options, xhr, textStatus);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                if (window.debugMode) {
                    console.error("Url: " + params.url, textStatus, errorThrown);
                    console.log("params:", params);
                    workspace.appView.showErrorMessage("Url: " + params.url + "<br/>" + textStatus + "<br/>" + errorThrown);
                }
                onRequestError.call(this, options, jqXHR, textStatus, errorThrown);
            }
        });

        var ajax = $.ajax(params);
        if (params.successMessage || params.errorMessage) {
            ajax.done(function (result) {
                if (result && result.success === false) {
                    if (params.errorMessage) {
                        workspace.appView.showErrorMessage(params.errorMessage === true ? "Непредвиденная ошибка" : params.errorMessage);
                    }
                } else {
                    if (params.successMessage) {
                        workspace.appView.showSuccessMessage(params.successMessage === true ? "Изменения сохранены" : params.successMessage);
                    }
                }
            });
            if (params.errorMessage) {
                ajax.fail(function () {
                    workspace.appView.showErrorMessage(params.errorMessage === true ? "Непредвиденная ошибка" : params.errorMessage);
                })
            }
        }
        return ajax;
    };

    /**
     * Called when request has been completed.
     *
     * @param options
     * @param xhr
     * @param textStatus
     */
    var onRequestCompleted = function (options, xhr, textStatus) {
        try {
            if (options && options.complete && _.isFunction(options.complete)) {
                options.complete(xhr, textStatus);
            }

            checkResponseStatus(options, xhr);
        } catch (ex) {
            if (console && console.log) {
                console.log(ex);
            }
        }
    };

    /**
     * Called on every error.
     * Sends error report to google analytics.
     * @param options
     * @param jqXHR
     * @param textStatus
     * @param errorThrown
     */
    var onRequestError = function (options, jqXHR, textStatus, errorThrown) {
        try {
            if (textStatus === 'timeout') {
                Backbone.onServiceUnavailable(0);
            }

            Backbone.GlobalEvents.trigger("onRequestError", options, jqXHR, textStatus, errorThrown);

            if (options && options.error && _.isFunction(options.error)) {
                options.error.call(this, jqXHR, textStatus, errorThrown);
            }
        } catch (ex) {
            if (console && console.log) {
                console.log(ex);
            }
        }
    };
})();

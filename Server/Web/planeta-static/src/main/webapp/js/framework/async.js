/**
 * Helper-classes for asynchronous tasks
 */

/**
 * Task queue class
 */
var Queue = function () {
    this.promise = jQuery(this).promise(); // Stub promise
};

// Magic with arguments is needed to pass arguments to the called function
Queue.prototype.append = function () {
    var args = arguments;

    var fn = args[0];
    if (!fn || !jQuery.isFunction(fn)) {
        throw new TypeError('1st parameter should be a function');
    }

    var self = this;
    args = Array.prototype.slice.call(args, 1);
    return this.promise = this.promise.pipe(function () {
        return jQuery.Deferred(
        function () {
            try {
                return fn.apply(this, args);
            } catch (ex) {
                // log exception
                this.reject(ex);
                return self.promise = jQuery(self).promise();
            }
        }).promise();
    });
};

/**
 * Manager for asynchronous tasks
 */
var Async = {

    _queues: {},

    append: function(fn, queueName) {

        queueName = queueName || '__default';
        if (!this._queues[queueName]) {
            this._queues[queueName] = new Queue();
        }
        return this._queues[queueName].append(fn);
    },

	clearQueue: function(queueName) {
		queueName = queueName || '__default';
		this._queues[queueName] = new Queue();
	}
};
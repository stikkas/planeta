/**
 * Scheduling Job Manager
 */
var JobManager = (function () {

    var _jobs= [];

    var _constantPeriodFunc =  function(period) {
        return period;
    };

    var _job =  function(options) {
        if (!options.jobName) {
            throw 'Job name is required'
        }
        if (!options.callback) {
            throw 'Job callback is required'
        }
        return _.extend({
            callCounts: 0,
            errorsCount: 0
        }, options);
    };

    var _execute = function(job) {
        try {
            job.callCounts++;
            var args = job.args || []; //ie8 exception: "Function.prototype.apply: argument is null or undefined"
            job.callback.apply(job.context, args);
        } catch (ex) {
            //TODO: how notify about error
            job.errorsCount++;
            console.error('Error execute job ' + ex);
        }
        if (job.period) {
            _startSchedule(job, job.period);
            job.period = job.periodFunc(job.period);
        }
    };


    var _startSchedule = function (job, period) {
        if (!_.any(_jobs, function (anyjob, index) {
                return anyjob.jobName === job.jobName
            }) && !job.eternal) {
            return;
        }
        job.timeoutId = setTimeout(function () {
            try {
                _execute(job);
            } catch (ex) {
                job.errorsCount++;
            }
        }, period || job.delay);
    };

    var _removeJob = function(job, index) {
        if (job.timeoutId) {
            clearTimeout(job.timeoutId);
            _jobs[index] = null;
        }
    };

    /**
     * Removing job from queue by name
     * @param jobName
     */
    var removeJob = function(jobName) {
        _.each(_jobs, function(job, index) {
            if (job.jobName === jobName) {
                _removeJob(job, index);
            }
        });
        _jobs = _.compact(_jobs);
    };



    /**
     * Execute job periodically with period and start delay
     * @param jobName
     * @param callback
     * @param delay
     * @param period
     * @param options
     */
    var schedule = function(jobName, callback, delay, period, options) {
        // job is unique by its name
        // If job has been scheduled second time - remove prev job and schedule it again
        removeJob(jobName);

        options = _.extend({
            jobName: jobName,
            callback: callback,
            delay: delay,
            period: period,
            periodFunc: options && options.periodFunc ? options.periodFunc : _constantPeriodFunc
        }, options);

        var job = _job(options);
        _jobs.push(job);
        _startSchedule(job);
    };

    /**
     * Execute job once with delay
     * @param jobName
     * @param callback
     * @param delay
     * @param options Could contain following<br/>
     * context: Function context
     * eternal: Could be true or false. If true -- job is eternal and does not destroyed
     * after route has been changed. Default value is false.
     */
    var scheduleOnce = function(jobName, callback, delay, options) {
        return schedule(jobName, callback, delay, 0, options);
    };

    /**
     * Clearing queue from non eternal jobs
     */
    var clearQueue = function() {
        for (var i = _jobs.length - 1; i >= 0; i--) {
            var job = _jobs[i];
            if (!job.eternal) {
                _removeJob(job, i);
            }
        }
        _jobs = _.compact(_jobs);
    };

    return {
        clearQueue: clearQueue,
        scheduleOnce: scheduleOnce,
        schedule: schedule,
        removeJob: removeJob
    };
})();



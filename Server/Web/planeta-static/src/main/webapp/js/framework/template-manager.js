/*globals StringUtils,console*/

/**
 * Manages jquery.tmpl templates (loads files with templates asynchronously).
 * Main methods are: <br/>
 * <b>init</b> -- initializes TemplateManager with fileTemplates map.
 * <b>tmpl</b> -- the same as $.tmpl, but does its work asynchronously. You can
 * access its results in <b>done</b> callback.<br/>
 * <b>usage:</b> TemplateManager.tmpl('#template', {}, {}).done(function(rendered) { alert(rendered); })<br/>
 * <b>fetchTemplate</b> -- asynchronously loads file with templates (mapping is in this.fileTemplates object).<br/>
 * <b>usage:</b> TemplateManager.fetchTemplate('template').done(function() { alert('#template is now exist!'); )})
 */

var TemplateManager = function () {

    /**
     * Map with mapping between templates and file urls
     */
    var fileTemplates= {};

    var staticNodesService = {};

    var initDfd = {};

    /**
     * Collection containing loaded files name (used to check if file is loaded or not)
     */
    var loadedFiles= [];

    /**
     * Compiled templates cache
     */
    var compiledTemplates = {};

    var loadedCss = {};


    /**
     * Initializes template manager with
     * FileTemplates map
     *
     * @param options
     */
    var init = function (options, staticNodesServiceIniter) {
        // TODO remove later
        if (options.fileTemplates) {
            fileTemplates = options.fileTemplates;
            initDfd = $.Deferred().resolve();
            return initDfd;

        }
        var lang = options;
        staticNodesService = staticNodesServiceIniter;
        var url = staticNodesService.getUrlWithScheme(staticNodesService.jsBaseUrl) + '/templates.json';
        if (lang === 'en') {
            url += '?lang=en';
        }
        initDfd = $.get(url);
        initDfd.done(function(resp) {
            fileTemplates = resp;
        });
        return initDfd;
    };

    var getFileName = function (templateName, fileTemplates) {
        var getLoadingFile = null;

        _.find(fileTemplates, function (templateNames, key, d) {
            if (_.include(templateNames, templateName)) {
                getLoadingFile = key;
                return true;
            }
            return false;
        });
        return getLoadingFile;
    };


    var _fetchTemplate = function (template) {

        var templateName = template.substr(1, template.length);
        var fileUrl = getFileName(templateName, fileTemplates);
        // already on page
        if (!fileUrl) {
            return $.Deferred().resolve();
        }

        var dfd = loadedFiles[fileUrl];
        if (!dfd) {
            dfd = $.Deferred();
            loadedFiles[fileUrl] = dfd;
            var div = $('<div></div>');
            div.load(fileUrl, function (response, status, xhr) {
                $("head").append(response);
                $.when(fetchAllCss(findAllCss(response)), fetchIncludedTemplates(response, dfd)).done(function () {
                    dfd.resolve(response);
                }).fail(function () {
                    dfd.reject();
                });
            });
        }
        return dfd;
    };

    var fetchTemplate = function (template) {
        var dfd = $.Deferred();
        initDfd.done(function() {
            _fetchTemplate(template).done(function (response) {
                dfd.resolve(response);
            }).fail(function () {
                dfd.reject();
            });
        });
        return dfd;
    };

    var _includeCssRegexp = new RegExp('<!-- *include +css +(.+?)-->', 'g');
    var findAllCss = function (tx) {
        var arr, cssArray = [];
        while ((arr = _includeCssRegexp.exec(tx))) {
            cssArray.push(arr[1]);
        }
        return cssArray;
    };


    var supportsToken = function(token, relList) {
        if (relList && relList.supports && token) {
            return relList.supports(token);
        }
        return false;
    };

    var fetchCss = function (css, toBody) {


        var dfd = loadedCss[css];
        if (!dfd) {
            loadedCss[css] = dfd = $.Deferred();
            var link = document.createElement("link");

            if (supportsToken("preload", link.relList)) {
                link.rel = "preload";
                link.as = "style";
                link.onload = function () {
                    link.rel = "stylesheet";
                }
            } else {
                link.rel = "stylesheet";
                link.type = "text/css";
            }
            var staticNodesServiceLocal = staticNodesService || workspace.staticNodesService;
            link.href = "//" + staticNodesServiceLocal.resourcesHost + css;

            if(toBody) {
                // $('body').prepend('<link rel="stylesheet" type="text/css" href="//' + workspace.staticNodesService.resourcesHost + css + '">');
                link.async = true;
                document.body.insertBefore(link, document.body.childNodes[0]);

            } else {
                // $('head').append('<link rel="stylesheet" type="text/css" href="//' + workspace.staticNodesService.resourcesHost + css + '">');
                document.head.appendChild(link);
            }

            var $el = $('<div id="_' + css.substring(css.lastIndexOf('/') + 1, css.indexOf('.')) + '_css_">').appendTo($('body'));
            var needCancel = false;
            var errTm = setTimeout(function () {
                console.error("cannot upload css file " + css);
                needCancel = true;
            }, 5000);
            var fi = setInterval(function () {
                if ($el.css('display') === 'none') {
                    clearInterval(fi);
                    clearTimeout(errTm);
                    $el.remove();
                    dfd.resolve();
                    return;
                }
                if (needCancel) {
                    clearInterval(fi);
                    dfd.reject();
                }
            }, 300);
        }
        return dfd;
    };

    var fetchAllCss = function (cssArray) {
        if (!cssArray.length) {
            return $.Deferred().resolve();
        }
        var dfdArray = [];
        _.each(cssArray, function (css) {
            dfdArray.push(fetchCss(css));
        });
        return $.when.apply($, dfdArray).promise();
    };

    var fetchIncludedTemplates = function (tx, templateDfd) {
        var regexp = /\{\{(tmpl|wrap) [^\}]*['"](#[^\}'"]+)['\"][^\}]*\}\}/g;
        var arr;
        var dfdArray = [];
        while ((arr = regexp.exec(tx))) {
            var tmplName = arr[2];
            if (tmplName && tmplName.charAt(tmplName.length - 1) !== '_') {
                var dfd = _fetchTemplate(tmplName);
                if (dfd !== templateDfd) {
                    //included template is in other file
                    dfdArray.push(dfd);
                }
            }
        }
        if (dfdArray.length > 0) {
            return $.when.apply($, dfdArray).promise();
        }

        return $.Deferred().resolve();
    };

    var applyTemplate = function (template, data, options, dfd, templateName) {
        try {//removeTryCatch
            var rendered = $.tmpl(template, data, options);
            dfd.resolve(rendered);
        } catch (ex) {
            var exObj = {template: templateName, data: data, error: ex};
            dfd.reject(exObj);
        }
    };

    var fetchAndCompileTemplate = function (template, data, options, dfd) {
        $.when(fetchTemplate(template)).done(function (responce) {
            try {//removeTryCatch
                // File with templates has been loaded, compiling template
                var compiled = $(template).template();
                if (!compiled) {
                    console.log('can not compile template: "' + template + '"');
                    dfd.reject({template: template});
                    return;
                }
                compiledTemplates[template] = compiled;
                applyTemplate(compiled, data, options, dfd, template);
            } catch (ex) {
                console.log('Compile template exception:', ex);
                console.log('compiled template: "' + template + '"');
                console.log('template data', data);
                var templateObj = template && _.isFunction(template) ? template() : template;
                var exObj = {template: templateObj, data: data, error: ex};
                dfd.reject(exObj);
            }
        }).fail(function (ex) {
            console.log('Fetch template exception', ex);
            console.log(template);
            dfd.reject(ex);
        });
    };

    var tmpl = function (template, data, options) {
        var dfd = $.Deferred();

        if (!template) {
            // Template is not set, resolving deferred immediately
            dfd.resolve();
        } else if (typeof (template) !== 'string') {

            // This is compiled template.
            // Resolving or rejecting depending on render result
            applyTemplate(template, data, options, dfd);
        } else if (compiledTemplates[template]) {

            // Template is already compiled, applying it
            applyTemplate(compiledTemplates[template], data, options, dfd, template);
        } else {

            // Fetching and compiling template. Applying it after this.
            fetchAndCompileTemplate(template, data, options, dfd);
        }

        return dfd.promise();
    };

    return {
        init: init,
        tmpl: tmpl,
        fetchTemplate: fetchTemplate,
        fetchCss: fetchCss
    }
}();

ProductEdit.Views.Preview = BaseRichView.extend({
    anchor: '.plate-cont',
    template: '#product-edit-preview-template',

    afterRender: function() {
        this.renderMedia();
        var self = this;
        moduleLoader.loadModule('fancybox').done(function () {
            self.$('.photo-zoom-link').fancybox();
        });
    }
});
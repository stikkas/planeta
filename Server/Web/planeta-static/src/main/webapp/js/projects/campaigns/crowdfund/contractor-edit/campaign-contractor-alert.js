CrowdFund.Views.ContractorAlert = BaseView.extend({
    template: '#project-contractor-alert-template',
    initialize: function (options) {
        BaseView.prototype.initialize.apply(this, arguments);
        this.model.campaign = options.campaign;
    },

    afterRender: function () {
        var isVisible = CrowdFund.Views.ContractorAlert.isVisible(this.model.campaign);
        this.$el.toggle(isVisible);
        if (isVisible) {
            if (!this.model.get('contractorId') && !this.showYet && (this.model.campaign.get("status") === 'NOT_STARTED')) {
                this.showYet = true;
            }
        }
    }
});

CrowdFund.Views.ContractorAlert.isVisible = function (model) {
    return PrivacyUtils.hasAdministrativeRole();
};

CrowdFund.Models.ContractorAlert = BaseModel.extend({
    url: '/admin/contractor-get-campaign.json',
    defaults: {
        cityId: 0,
        countryId: 1
    },

    initialize: function (options) {
        BaseModel.prototype.initialize.call(this, options);
        this.set({
            currentPlanetaManager: new CrowdFund.Models.CurrentPlanetaManager({}),
            changeModerationUrl: 'https://' + workspace.serviceUrls.adminHost + '/moderator/campaign-moderation-info.html?campaignId=' + this.get('campaignId')
        });
    },

    fetch: function (options) {
        var self = this;
        var managerOptions = {
            success: function () {
                options = _.extend({
                    data: {
                        campaignId: self.get('campaignId')
                    }
                }, options);
                BaseModel.prototype.fetch.call(self, options);
            },
            data: {
                campaignId: self.get('campaignId')
            }
        };
        if (PrivacyUtils.isAdmin()) {
            this.get('currentPlanetaManager').fetch(managerOptions);
        }
    },

    parse: function (response) {
        if (!response) {
            return {
                name: 'не указан',
                contractorId: 0
            };
        }
        return response;
    }

});


/*PrivacyUtils*/
var ShopUtils = {

    // TODO: rename productTypes to category, and find better name for what's now productCategories
    _productTags: {
/*        ALL: {value: 'Все товары', id: 0},
        CLOTHES: {value: 'Одежда', id: 1},
        CD: {value: 'Диски', id: 2},
        BOOKS: {value: 'Книги', id: 3},
        ANOTHER: {value: 'Другое', id: 4}*/
    },

    productCategories: {
        DIGITAL: 'Электронный',
        PHYSICAL: 'Физический',
        DONATE: 'Поддержка автора'
    },

    getCreateProductUrl: function(groupAlias, parentId) {
        var url = "/admin/products/" + groupAlias + "/create";
        parentId && (url += "?parentId=" + parentId);
        return url;
    },

    getEditProductUrl: function(groupAlias, productId) {
        return "/admin/products/" + groupAlias + '/' + productId + "/edit";
    },

    getProductCategoryName: function(category) {
        return ShopUtils.productCategories[category];
    },


    setProductTags: function(productTags) {
        ShopUtils._productTags = productTags || [];
    },

    getProductTags: function() {
        return ShopUtils._productTags;
    },

    getDefaultProductType: function() {
        return ShopUtils._productTags ? ShopUtils._productTags[0] : {};
    },

    getProductTag: function(tagId) {
        return _.find(ShopUtils._productTags || [], function(productType) {
            return productType.categoryId == tagId;
        });
    },
    getProductTagByName: function(name) {
        return _.find(ShopUtils._productTags || [], function(productType) {
            return productType.mnemonicName == name;
        });
    },

    setTagToAttributesTypeMap:function(map){
        ShopUtils._tagToAttributesTypeMap = map;
    },

    getTagToAttributesTypeMap:function(){
        return ShopUtils._tagToAttributesTypeMap;
    },

    getTagToAttributesTypeMapNames:function(){
        var names = [];
        for (var name in ShopUtils._tagToAttributesTypeMap){
            names.push(name);
        }

        return names;
    },

    getTagAttributesTypeByName:function(name){
        return ShopUtils._tagToAttributesTypeMap[name];
    },

    initializeShopUtils: function() {
        var options = {
            url: '/api/public/initialize-shop-utils.json',
            success: function(response) {
                if (response.success === true) {
                    ShopUtils.setProductTags(response.result.productTags);
                    if (window.workspace) {
                        workspace.appModel.productCategories = response.result.productCategories;
                    }
                } else {
                    if (window.workspace) {
                        workspace.appView.showErrorMessage(response.errorMessage);
                    }
                }
            }
        };
        return Backbone.sync('read', this, options);
    },

    getProductUrl: function(productId) {
        return StringUtils.getAbsoluteUrl(workspace.serviceUrls.shopAppUrl + '/products/' + productId);
    }

};
/*globals Comments*/

/**
 * Model for product's details page
 */
var ProductPreview = {};
ProductPreview.Models = {};
ProductPreview.Views = {};

ProductPreview.Models.Main = BaseModel.extend({

    initialize: function(options) {
        BaseModel.prototype.initialize.call(this, options);

        var product = this.get('product');
        product = this.extendProduct(product);
        var childrenProducts = this.createChildrenProductsModel();
        var firstEnabled = _.find(childrenProducts.get('products'), function(product) {
            return product.isEnabled;
        });
        firstEnabled = firstEnabled || _.first(childrenProducts.get('products'));
        var selectedProduct = this.createSelectedProductModel(firstEnabled);
        workspace.selectedProduct = selectedProduct;
        var likesDataForModel = {
            profileId: product.profileId,
            objectId: product.voteObjectId,
            objectAuthorId: product.authorProfileId || product.profileId,
            objectType: product.objectType,
            votableModel: new BaseModel(product)
        };

        this.set({
            childrenProducts: childrenProducts,
            selectedProduct: selectedProduct,
            otherProductsModel: this.createOtherProductsModel(),
            comments: this.createCommentsModel(),
            likesModel: likesDataForModel
        }, {silent: true});

        this.get('comments').fetch();
    },

    createChildrenProductsModel: function() {
        var self = this;
        var parentProduct = this.get('product');
        var childrenProducts = this.get('product').childrenProducts;
        if (!_.isArray(childrenProducts)) {
            childrenProducts = [];
        }

        childrenProducts = _.map(childrenProducts, function(childProduct) {
            return self.extendProductWithDonate(childProduct, parentProduct);
        });

        return new BaseModel({
            products: _.map(childrenProducts, function(childProduct) {
                return self.extendProduct(childProduct);
            }),
            productType: this.get('productType'),
            product: this.get('product')
        });
    },

    createOtherProductsModel: function() {
        var self = this;
        var otherProducts = self.get('otherProducts');
        var visibleOtherProductsCount = this.get('maxVisibleOtherProductsCount') || 6;
        var otherProductsCount = _.size(otherProducts);

        return new BaseModel({
            hasOtherProducts: (otherProductsCount > 0),
            hasMoreOtherProducts: (_.size(otherProducts) > visibleOtherProductsCount),
            otherProductsCount: otherProductsCount,
            products: (otherProductsCount > visibleOtherProductsCount) ? _.first(otherProducts, visibleOtherProductsCount) : otherProducts
        });
    },

    createSelectedProductModel: function(product) {
        var selectedProduct;
        if (_.isEmpty(product)) {
            selectedProduct = this.get('product');
            selectedProduct.isSelected = true;
        } else {
            selectedProduct = product;
            selectedProduct.isSelected = selectedProduct.isEnabled;
        }

        return new BaseModel({
            product: selectedProduct,
            canPurchase: this.canPurchase(selectedProduct)
        });
    },

    createCommentsModel: function() {
        var product = this.get('product');

        return new Comments.Models.Comments({
            profileId: product.merchantProfileId,
            objectId: product.productId,
            objectType: 'PRODUCT'
        }, {
            commentableObject: new BaseModel(product)
        });
    },

    canPurchase: function(product) {
        return this.get('product').productStatus === 'ACTIVE' && product.available;
    },

    extendProduct: function(product) {
        return _.extend(product, {
            isMeta: (product.productState === 'META'),
            isGroup: (product.productState === 'GROUP'),
            isEnabled: (product.readyForPurchaseQuantity > 0),
            isSelected: false
        });
    },

    extendProductWithDonate: function(product, parentProduct) {
        if (!parentProduct) {
            return product;
        }

        return _.extend(product, {
            donateId: parentProduct.donateId,
            donate: parentProduct.donate
        });
    },

    setSelectedProduct: function(product) {
        this.get('selectedProduct').set({
            product: product,
            digital: product.productCategory === 'DIGITAL',
            canPurchase: this.canPurchase(product)
        });
    },

    switchProduct: function(productId, parentProduct) {
        var self = this;
        var selectedProduct = self.get('selectedProduct');
        var childrenProducts = self.get('childrenProducts');

        var newSelectionProduct = _.find(childrenProducts.get('products'), function(product) {
            return _.isEqual(product.productId, productId);
        });

        if (_.isEmpty(newSelectionProduct) || !newSelectionProduct.isEnabled || _.isEqual(selectedProduct.get('product').productId, productId)) {
            return;
        }

        return Backbone.sync('read', null, {
            url: '/product.json',
            data: {
                productId: productId
            },
            success: function(response) {
                childrenProducts.set('products', _.map(childrenProducts.get('products'), function(childProduct) {
                    childProduct.isSelected = _.isEqual(childProduct.productId, productId);
                    return childProduct;
                }));

                var selectedProduct = self.extendProduct(response);
                if (parentProduct) {
                    selectedProduct = self.extendProductWithDonate(selectedProduct, parentProduct);
                }
                self.setSelectedProduct(selectedProduct);
                childrenProducts.change();
            }
        });
    }
});
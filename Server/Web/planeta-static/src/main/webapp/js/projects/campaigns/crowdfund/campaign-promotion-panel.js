/*globals StorageUtils, CrowdFund*/
CrowdFund.Views.PromotionPanel = BaseView.extend({
    template: '#campaign-promotion-panel-template'
});

CrowdFund.Models.PromotionPanel = BaseModel.extend({
    initialize: function (model) {
        BaseModel.prototype.initialize.apply(this, arguments);
        if (_.indexOf(["DRAFT", "DECLINED", "FINISHED"], this.get('status')) < 0) {
            var campaignId = this.get('campaignId');
            if ((this.get('targetAmount') && campaignId)) {
                var stage = 0;
                var completionPercentage = Math.floor(this.get('collectedAmount') / this.get('targetAmount') * 100);

                if (this.get('collectedAmount') > 0) {
                    if (completionPercentage < 25) {
                        stage = 1; //0-25%
                    } else if (completionPercentage < 50) {
                        stage = 2;//25-50%
                    } else if (completionPercentage < 100) {
                        stage = 3;//50-100%
                    } else {
                        stage = 4;//100%+
                    }
                }

                var campaignPromotionPanelHide = StorageUtils.get('campaignPromotionPanelHide') || {};
                var savedStage = campaignPromotionPanelHide[campaignId];
                if (!_.isNumber(savedStage) || savedStage < stage) {
                    this.set({
                        stage: stage,
                        completionPercentage: completionPercentage
                    });
                    return;
                }
            }
        }
        this.dontShow = true;
    }

});
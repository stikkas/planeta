/**
 * Отрисовывает статистику
 */
Biblio.Views.Stats = BaseView.extend({
    template: "#biblio-stats_cont",
    el: '.js-stat-block'
});

/**
 * Специальная кнопка для перехода на 1 шаг
 */
Biblio.Views.BookSpecial = BaseView.extend({
    template: '#book-card_special',
    className: 'book-card_i'
});

/**
 * Отрисовывает обложки с ценами и специальную кнопку 
 */
Biblio.Views.IndexBooksList = BaseView.extend({
    template: "#biblio-main-list_preview__books",
    el: ".biblio-main-list_preview__books",
    construct: function (options) {
        var self = this;
        _.each(options.first, function (model) {
            self.addChildAtElement('.book-card', new Biblio.Views.Book({
                model: new Biblio.Models.Book(model)
            }));
        });
        self.addChildAtElement('.book-card', new Biblio.Views.BookSpecial({
            model: options.spec
        }));
        _.each(options.second, function (model) {
            self.addChildAtElement('.book-card', new Biblio.Views.Book({
                model: new Biblio.Models.Book(model)
            }));
        });
    },
    afterRender: function () {
        $('.js-to-books').click(function () {
            Biblio.Utils.changeUrl('/books');
        });
    }
});

/**
 * рисует значок партнера
 */
Biblio.Views.Partner = BaseView.extend({
    template: '#partner-template',
    tagName: 'span'
});

/**
 * рисует блок с партнерами
 */
Biblio.Views.Partners = BaseView.extend({
    el: '.biblio-partners',
    construct: function () {
        var self = this;
        self.collection.each(function (partner) {
            self.createChildView(Biblio.Views.Partner, {
                model: partner
            });
        });
    }
});

/**
 * Рисует объявление (рекламу) 
 */
Biblio.Views.Advertise = BaseView.extend({
    template: '#biblio-main-list_i',
    className: 'biblio-main-list_i',
    events: {
        'click .biblio-main-play_link': 'play'
    },
    play: function (e) {
        this.$(e.currentTarget).hide();
        this.$('.embed-video').show();

        var iframe = this.$('.embed-video iframe'),
                url = iframe.data('src');
        if (/youtu\.?be/.test(url)) {
            url = url.substr(url.lastIndexOf('/') + 1);
            if (/^watch\?/.test(url))
                url = url.substr(url.search('=') + 1);
            url = "https://www.youtube.com/embed/" + url + "?autoplay=1&enablejsapi=1&origin=http://example.com";
        }
        iframe.attr('src', url);
    }
});

/**
 * Рисует рекламный блок 
 */
Biblio.Views.Advertises = BaseView.extend({
    construct: function () {
        var self = this;
        self.collection.forEach(function (advert) {
            self.createChildView(Biblio.Views.Advertise, {
                model: advert
            });
        });
    }
});

/**
 * Рисует форму выбора типа регистрируемой организации
 */

Biblio.Views.OrganizationType = BaseView.extend({
    template: '#biblio-request-form-template',

    events : {
        'click .js-open-request-form': 'openRequestForm'
    },

    openRequestForm: function(e) {
        var type = $(e.currentTarget).attr('data-org-type');
        var dialog;

        if(type == 'BOOK') {
            dialog = new Biblio.Views.ModalWindow({
                view: Biblio.Views.BookRequest,
                clazz: 'modal-biblio-request',
                el: '#modal-biblio-request',
                bodyModel: new Biblio.Models.BookRequest()
            });
        } else if(type == 'LIBRARY') {
            dialog = new Biblio.Views.ModalWindow({
                view: Biblio.Views.LibraryRequest,
                clazz: 'modal-biblio-request',
                el: '#modal-biblio-request',
                bodyModel: new Biblio.Models.LibraryRequest()
            });
        }

        dialog.open();
    }
});

/**
 * Base view for biblio request
 */
Biblio.Views.Request = BaseView.extend({
    events: {
        'click .btn-primary': 'send',
        'change input, textarea, select': 'updateModel'
    },
    send: function (e) {
        e.preventDefault();
        var self = this;
        self.model.save().done(function (res) {
            $('.js-error').text('').hide();
            if (res && res.success) {
                self.parent.close();
                if (!self.success) {
                    self.success = new Biblio.Views.ModalWindow({
                        view: Biblio.Views.SuccessRequest,
                        el: "#success-request-modal",
                        bodyModel: new BaseModel()
                    });
                }
                self.success.open();
            } else {
                for (var field in res.fieldErrors) {
                    $('#' + field + 'Error').text(res.fieldErrors[field]).show();
                }
            }
        });
    },

    updateModel: function (e) {
        var name = this.$(e.target).prop('name'),
            value = this.$(e.target).val();

        this.model.set(name, value, {silent: true});
    }
});

/**
 * Рисует форму заявки на учаситие для библиотеки
 */
Biblio.Views.LibraryRequest = Biblio.Views.Request.extend({
    isExistsPlanetaUiElement: true,
    template: '#biblio-library-request-template',
    afterRender: function() {
        workspace.changeUrl("/libraryrequest");
    },
    dispose: function () {
        workspace.changeUrl("/");
        Biblio.Views.LibraryRequest.prototype.dispose.call(this);
    },
    close: function() {
        workspace.changeUrl("/");
    }
});

/**
 * Рисует форму заявки на учаситие для издания
 */
Biblio.Views.BookRequest = Biblio.Views.Request.extend({
    template: '#biblio-book-request-template',
    afterRender: function() {
        workspace.changeUrl("/bookrequest");
    },
    dispose: function () {
        workspace.changeUrl("/");
        Biblio.Views.Request.prototype.dispose.call(this);
    },
    close: function() {
        workspace.changeUrl("/");
    }

});
/**
 * Рисует окошко с успешной отправкой заявки
 */
Biblio.Views.SuccessRequest = BaseView.extend({
    template: '#request-success-template',
    events: {
        'click .btn-primary': 'close'
    },
    close: function () {
        this.parent.close();
    }
});

/*global ProfileUtils,google,BaseModel,Audios,workspace,$,BaseView,_,BaseListView,unescape,escape,moduleLoader*/
/**
 * User: a.savanovich
 * Date: 10.04.12
 * Time: 12:55
 */

var GroupStatistics = {
    Models: {},
    Views: {},

    tableWidth: 760,
    tableHeight: 500,

    numWithSignAndBracketHtml: function (number) {
        var result = "<span ";
        if (!number) {
            result += '>(-)';
        } else {
            result += ' class=';
            if (number > 0) {
                result += '"positive"';
            } else {
                result += '"negative"';
            }
            result += '>';
            result += '(';
            if (number > 0) {
                result += '+';
            }
            result += number + ')';
        }
        return result + "</span>";
    },
    loadVisualization: function () {
        google.load("visualization", "1", {packages: ["corechart"], "callback": window.baseChartGlobal.visualizationLoaded});
    },

    categoryName: function (number) {
        if (number == 2) {
            return 'Участники';
        }
        return 'Все';
    }
};


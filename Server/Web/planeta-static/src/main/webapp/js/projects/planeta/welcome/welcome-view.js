/*global Welcome, CampaignsList, ScrollableListView, animateHide, jQuery,moduleLoader, Modal, CampaignUtils, Banners, StringUtils*/
Welcome.Views.WelcomeView = BaseView.extend({
    template: '#planeta-welcome-template',

    construct: function () {
        this.addChildAtElement('.js-projects-welcome-box-container', new Welcome.Views.ProjectBoxView({
            model: this.model.projectBox
        }));

        this.addChildAtElement('.js-projects-welcome-partners-container', new Welcome.Views.Partners({}));
    },

    afterRender: function () {
        new Vue({
            el: '.welcome-rewards',

            i18n: new VueI18n({
                locale: workspace.currentLanguage,
                messages: workspace.i18messages
            })
        });

        new Vue({
            el: '#welcome-carousel',

            i18n: new VueI18n({
                locale: workspace.currentLanguage,
                messages: workspace.i18messages
            })
        });
        Banner.init('https://' + workspace.serviceUrls.mainHost, workspace.appModel.get('myProfile').get('isAuthor'));
    }
});

Welcome.Views.ProjectBoxView = BaseView.extend({
    template: '#welcome-content-box-project-template',

    afterRender: function () {
        var self = this;
        var initAnimation = function () {
            setTimeout(function () {
                self.initTopCampaignsSlider();
                // TODO: remove old initPromoSlider. gtmTrack on slider?
                //self.initPromoSlider();
            }, 500);
        };

        workspace.appView.dfdCalcLayout.done(initAnimation);
    },

    initPromoSlider: function () {
        var self = this;

        var $slider = this.$('.project-welcome-main-slider');

        this.gtmTrackViewCampaignList($slider);
    },

    gtmTrackViewCampaignList: function(el) {
        var campaigns = [],
            children = $(el).children();

        _.each(children, function(child) {
            if($(child).data('campaignModel')) {
                campaigns.push($(child).data('campaignModel'));
            }
        });

        if (campaigns.length) {
            if (window.gtm) {
                window.gtm.trackViewCampaignList(campaigns, 'Главная карусель - главная', 0);
            }
        }
    },

    initTopCampaignsSlider: function () {
        var self = this;
        self.topCampaigns = self.addChildAtElement('.js-popular-projects .welcome-projects_carousel-in', new Welcome.Views.TopCampaigns({
            collection: self.model.topCampaigns,
            type: 'POPULAR',
            welcomeCarousel: true
        }));

        self.topCampaignsNew = self.addChildAtElement('.js-new-projects .welcome-projects_carousel-in', new Welcome.Views.TopCampaigns({
            collection: self.model.newCampaigns,
            type: 'NEW',
            welcomeCarousel: true
        }));
    }
});

Welcome.Views.PartnerOrCuratorItem = BaseView.extend({
    template: '#welcome-project-partners-item-template',
    className: 'partners-list_i'
});

Welcome.Views.PartnersOrCuratorsList = BaseListView.extend({
    itemViewType: Welcome.Views.PartnerOrCuratorItem,
    className: 'partners-list'
});

/*
Welcome.Views.Curators = BaseView.extend({
    template: '#welcome-project-curators-template',
    className: 'project-partners',
    afterRender: function() {
        var curators = new Welcome.Models.Curators();
        var self = this;
        curators.fetch().success(function() {
            self.addChildAtElement('.js-curators-list', new Welcome.Views.PartnersOrCuratorsList({
                collection: curators
            }), true);
        });
    }
});
*/

Welcome.Views.Partners = BaseView.extend({
    template: '#welcome-project-partners-template',
    className: 'project-partners',
    afterRender: function() {
        var partners = new Welcome.Models.Partners();
        var self = this;
        partners.fetch().success(function() {
            self.addChildAtElement('.js-partners-list', new Welcome.Views.PartnersOrCuratorsList({
                collection: partners
            }), true);
        });
    }
});


Welcome.Views.WelcomeAbout = BaseView.extend({
    template: '#welcome-project-about-template-in',
    className: 'project-about',
    modelEvents: {
        'destroy': 'dispose'
    },

    events: {
        'click .js-create-button': 'chooseDraftCampaign'
    },

    onHowCreateClicked: function (e) {
        e.preventDefault();
        var view = new Welcome.Views.WelcomeHowCreate({model: this.model});
        view.render();
    },

    chooseDraftCampaign: function (e) {
        var $specialProject = this.$el.find('.js-header-special-project');
        $specialProject.find('.js-create-button-block');
        if (workspace.isAuthorized) {
            e.preventDefault();
        }
        Welcome.Views.SelectDraftCampaignList.onCreateProjectClicked(e);
    }
});

Welcome.Views.WelcomeHowCreate = Modal.View.extend({
    template: '#welcome-project-how-create',
    afterRender: function () {
        workspace.showVideoPlayer('21765', '26709', this.$('.js-video'), {
            width: 480,
            height: 270,
            autostart: true,
            ignoreStorageEvents: true,
            ignoreVideoManager: true,
            fromCampaign: true,
            imageUrl: 'https://s1.planeta.ru/i/4b369/video_middle.jpg'
        });

    }
});

Welcome.Views.DraftCampaign = BaseView.extend({
    template: '#draft-campaign-item-template',
    className: 'ctba-modal-draft-item',
    events: {
        'click': 'onClick'
    },

    onClick: function () {
        workspace.navigate('/campaigns/' + this.model.get('campaignId') + '/edit');
    }
});

Welcome.Views.CategoryTypesList = BaseView.extend({
    template: "#campaign-categories-list-template",
    className: 'project-welcome-type',
    construct: function () {
        var self = this;
        CampaignUtils.getCampaignTags().done(function (tags) {
            self.tags = _.filter(tags, function (obj) {
                return obj.visibleInSearch;
            });
            self.render();
        });

    },

    modelJSON: function () {
        return {tags: this.tags, useEng : workspace && workspace.currentLanguage && workspace.currentLanguage === "en"};
    }
});

Welcome.Views.PromoCampaignsItem = BaseView.extend({
    template: '#promo-campaign-item-template',
    className: 'pwms-item',
    events: {
        'click': 'onClick'
    },

    onClick: function () {
        if (window.gtm) {
            window.gtm.trackClickCampaignCard(this.model);
        }
    },

    beforeRender: function () {
        var button = window.location.search;
        this.model.set({
            button: button
        }, {silent: true});
    },

    afterRender: function () {
        StringUtils.hyphenate(this);
        this.$el.data('campaignModel', this.model);
    }
});

Welcome.Views.PromoBanner = BaseView.extend({
    template: '#welcome-promo-banner-template',
    className: 'pwms-item'
});



Welcome.Views.TopCampaign = BaseView.extend({
    template: '#top-campaigns-item-template',
    className: 'project-card-item',
    injectionHtml: 'projectCardHtml',
    injectionFunc: 'projectCard',
    events: {
        'click': 'onClick'
    },

    afterRender: function () {
        var self = this;
        StringUtils.hyphenate(self);
        self.model.get('tags').forEach(function(it){
            if (it === 'INVESTING') {
                self.$el.addClass('project-card-item-investing');
            }
        });
    },
    onClick: function () {
        if (window.gtm) {
            window.gtm.trackClickCampaignCard(this.model);
        }
    }
});

Welcome.Views.TopCampaigns = BaseListView.extend({
    itemViewType: Welcome.Views.TopCampaign,
    className: 'project-card-list',

    construct: function (options) {
        BaseListView.prototype.construct.apply(this, arguments);
        if ( options.welcomeCarousel !== undefined ) {
            this.welcomeCarousel = options.welcomeCarousel;
        }
    },

    afterRender: function () {
        var self = this;
        setTimeout(function(){
            if (window.isMobileDev) {
                self.$el.scroll(function() {
                    $(window).trigger('scroll');
                });
            }

            self.trackViewCampaignList(self.options.type);
        }, 3000);

        if ( !!this.welcomeCarousel ) {
            var renderDone = setInterval(function () {
                if ( !!$('.project-card-item', self.$el).length ) {
                    clearInterval(renderDone);
                    self.initSlider();
                }
            }, 300);
        }
    },

    initSlider: function() {
        var self = this;

        Flickity.prototype.setPrevNextCells = function () {
            changeSlideClasses(this.currentSlide, 'remove', 'is-current');
            changeSlideClasses(this.previousSlide, 'remove', 'is-prev');
            changeSlideClasses(this.nextSlide, 'remove', 'is-next');

            var currentIndex = this.selectedIndex;
            this.currentSlide = this.slides[currentIndex];

            var prevIndex = this.selectedIndex - 1;
            prevIndex = prevIndex < 0 ? this.slides.length - 1 : prevIndex;
            this.previousSlide = this.slides[prevIndex];

            var nextIndex = this.selectedIndex + 1;
            nextIndex = nextIndex > this.slides.length - 1 ? 0 : nextIndex;
            this.nextSlide = this.slides[nextIndex];

            changeSlideClasses(this.currentSlide, 'add', 'is-current');
            changeSlideClasses(this.previousSlide, 'add', 'is-prev');
            changeSlideClasses(this.nextSlide, 'add', 'is-next');
        };

        function changeSlideClasses(slide, method, className) {
            if (!slide) {
                return;
            }
            slide.getCellElements().forEach(function (cellElem) {
                cellElem.classList[method](className);
            });
        }

        function isTouchDevice() {
            return 'ontouchstart' in window || navigator.maxTouchPoints;
        }

        var carouselOptions = {
            wrapAround: true,
            draggable: isTouchDevice(),
            pageDots: false,
            autoPlay: false,
            groupCells: true,
            cellAlign: 'center',
            contain: true,
            prevNextButtons: false,
            accessibility: false
        };

        var parentEl = self.$el.closest('.welcome-projects');
        parentEl.attr('tabindex', 0)
            .on('keyup', function (evt) {
                if (evt.keyCode === 37) {
                    if (isAnimating) return;
                    changeSlide('prev');
                }
                if (evt.keyCode === 39) {
                    if (isAnimating) return;
                    changeSlide('next');
                }
            });

        self.welcomeCarousel = new Flickity(self.$el[0], carouselOptions);
        var isAnimating = false;

        $('.welcome-projects_arrow.prev', parentEl).on('click', function () {
            if (isAnimating) return;
            changeSlide('prev');
        });
        $('.welcome-projects_arrow.next', parentEl).on('click', function () {
            if (isAnimating) return;
            changeSlide('next');
        });

        function changeSlide(direction) {
            if (window.screen.width > 1024) {
                isAnimating = true;
                if (direction === 'next') {
                    self.welcomeCarousel.next(true, true);
                } else {
                    self.welcomeCarousel.previous(true, true);
                }
                $(self.welcomeCarousel.slider).addClass('move to-' + direction);
                setTimeout(function () {
                    $(self.welcomeCarousel.slider).removeClass('move');
                    setTimeout(function () {
                        $(self.welcomeCarousel.slider).removeClass('to-' + direction);
                        self.welcomeCarousel.setPrevNextCells();
                        setTimeout(function () {
                            isAnimating = false;
                        });
                    });
                }, 1160);
            } else {
                if (direction === 'next') {
                    self.welcomeCarousel.next(true);
                } else {
                    self.welcomeCarousel.previous(true);
                }
            }
        }


        $(window).on('resize.changeComparePreview', _.debounce(changeComparePreview, 500));
        $(window).trigger('resize.changeComparePreview');
        self.welcomeCarousel.setPrevNextCells();
        self.welcomeCarousel.on('select', function () {
            self.welcomeCarousel.selectedElements.forEach(function (cell) {
                $('img', cell).trigger('appear');
            });
        });
        if ( window.screen.width < 768 ) {
            self.welcomeCarousel.on('settle', function () {
                $('img', $(self.welcomeCarousel.selectedElement).next()).trigger('appear');
                $('img', $(self.welcomeCarousel.selectedElement).prev()).trigger('appear');
            });
        }


        function changeComparePreview() {
            var listWidth = self.welcomeCarousel.size.width;
            var itemWidth = self.welcomeCarousel.cells[0].size.width;
            var groupCells = Math.floor(listWidth / itemWidth);
            groupCells = groupCells > 3 ? 3 : groupCells;
            if (listWidth < 768) groupCells = 1;

            if (self.welcomeCarousel.slides.length != groupCells) {
                self.welcomeCarousel.options.groupCells = groupCells;
                self.welcomeCarousel.reloadCells();
                self.welcomeCarousel.resize();
                self.welcomeCarousel.reposition();
            }
            self.welcomeCarousel.setPrevNextCells();

            self.welcomeCarousel.slides.forEach(function (slide) {
                slide.cells.forEach(function (cell, cellIndex) {
                    $(cell.element).attr('data-id', cellIndex);
                });
            });
        }
    },

    trackViewCampaignList: function (type) {
        var listName = '';

        switch (type) {
            case 'POPULAR' :
                listName = "Популярные проекты – карусель";
                break;
            case 'NEW':
                listName = "Рекомендуемые проекты – карусель";
                break;
        }

        if (window.gtm) {
            window.gtm.trackViewCampaignList(this.collection.toArray(), listName, 1);
        }
    },

    onAdd: function () {
        BaseListView.prototype.onAdd.apply(this, arguments);
        var itemsRendered = ++this.collection.itemsRendered;
        this.$('.project-card-item').last().addClass("js-item-number-" + this.collection.itemsRendered);
        if (itemsRendered > this.collection.limit && !this.isCharityTab) {
            var insertTo = this.$('.js-item-number-' + (itemsRendered - 1)).next();
            var newItem = this.$('.project-card-item:gt(' + (itemsRendered - 2) + ')');
            this.$el.trigger('insertItem', [newItem, insertTo]);
        }
    },

    dispose: function () {
        $(window).off('resize.changeComparePreview');
        if (this.welcomeCarousel && this.welcomeCarousel.destroy) {
            this.welcomeCarousel.destroy();
        }
    }
});


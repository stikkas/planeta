(function ($) {

    $.fn.productPreviewSlider = function(options) {
        var previewImages = $(this).find('img');
        var isStopped = true;
        var thumbBlock = $('#product-preview-thumb'),
            thumbHeight = $('#product-preview-thumb').outerHeight(true),
            thumbWrapHeight = $('#product-preview-thumb').parent().height();

        $('#product-preview-thumb .product-preview-item-link').click(function() {
            if (!isStopped) return false;

            /* прелоадер и переключалка слайдера */
            var self = $(this);
            var selfWrap = self.closest('.product-preview-item');

            var index = selfWrap.index();
            var previewImg = $(previewImages[index]);
            var srcImg = previewImg.data('src');

            if (srcImg != '') {
                self.after($('<div class="img-loading"></div>'));

                previewImg.data('src', '');
                previewImg.attr('src', srcImg);
                var dfd = previewImg.imagesLoaded();
                dfd.done( function() {
                    if (selfWrap.hasClass('active')) {
                        $('#product-preview').trigger( 'slideTo', index );
                    }
                    self.next('.img-loading').remove();
                } );
            } else {
                $('#product-preview').trigger( 'slideTo', index );
            }

            $('#product-preview-thumb .active').removeClass('active');
            selfWrap.addClass('active');
            /* /прелоадер и переключалка слайдера */


            /* прокрутка thumb блока */
            if (previewImages.length > 4) {
                var itemHeight = selfWrap.height(),
                        itemPos = selfWrap.position().top;
                if ( itemPos + itemHeight/2 > thumbWrapHeight/2 ) {
                    var newTop = thumbWrapHeight/2 - (itemPos + itemHeight/2);
                    newTop = Math.abs(newTop) > thumbHeight - thumbWrapHeight ? -(thumbHeight - thumbWrapHeight) : newTop;
                    thumbBlock.animate({
                        top: newTop
                    });
                } else if (thumbBlock.position().top < 0) {
                    thumbBlock.animate({
                        top: 0
                    });
                }
            }
            /* /прокрутка thumb блока */

        });

        if (previewImages.length > 4) {
            $('.block-shadow-top, .block-shadow-bottom').show();

            thumbBlock.bind('mousewheel', function(event, delta, deltaX, deltaY) {
                var DELTA_SCALE = 2;// 76 changed due to deltaY(-1) -> delta(-20)
                if (delta < 0) {
                    var newTop = thumbBlock.position().top + delta * DELTA_SCALE;
                    newTop = Math.abs(newTop) > thumbHeight - thumbWrapHeight ? -(thumbHeight - thumbWrapHeight) : newTop;
                    thumbBlock.stop(1,1).animate({
                        top: newTop
                    }, 150);
                } else {
                    var newTop = thumbBlock.position().top + delta * DELTA_SCALE;
                    newTop = newTop > 0 ? 0 : newTop;
                    thumbBlock.stop(1,1).animate({
                        top: newTop
                    }, 150);
                }
                return false;
            });
        }

        $(this).carouFredSel({
            items: 1,
            responsive: true,
            auto: false,
            scroll: {
                pauseOnHover: "resume",
                fx: 'fade',
                duration: 100,
                onBefore: function() {
                    isStopped = false
                },
                onAfter: function() {
                    isStopped = true
                }
            },
            onCreate: function() {
                $('#product-preview-thumb .active .product-preview-item-link').trigger('click');
            }
        });
    }
})(jQuery);
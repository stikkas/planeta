/*globals Products*/

var ProductSearch = {};
ProductSearch.Models = {};

ProductSearch.Models.Filter = BaseModel.extend({
    defaults: {
        query: '',
        productTags: [],
        mainCategoryMnemonic: '',
        referrerIds: [],
        sortOrder: 'SORT_DEFAULT',
        priceFrom: 0,
        priceTo: 0,
        limit: 3,
        offset: 0
    },

    initialize: function (options) {
        BaseModel.prototype.initialize.call(this, options);
    },

    getURI: function () {
        return '/products/search-products.json?';
    },

    getParams: function () {
        var result = 'query=' + this.get('query');
        var productTags = this.get('productTags'),
            referrerIds = this.get('referrerIds');

        if(productTags && productTags.length > 0) {
            productTags.forEach(function (productTag) {
                result += '&productTagsMnemonics=' + productTag;
            });
        }

        if(referrerIds) {
            referrerIds.forEach(function (id) {
                result += '&referrerIds=' + id;
            });
        }

        if (this.get('priceFrom')) {
            result += '&priceFrom=' + this.get('priceFrom');
        }

        if (this.get('priceTo')) {
            result += '&priceTo=' + this.get('priceTo');
        }

        result += '&sortOrder=' + this.get('sortOrder');

        return result;
    },

    getURL: function () {
        return this.getURI() + this.getParams();
    },

    silentSet: function (attrName, attrValue) {
        if (this.has(attrName) && _.isEqual(this.get(attrName), attrValue)) {
            return;
        }

        var attr = {};
        attr[attrName] = attrValue;
        this.set(attr, {silent: true});
        this.trigger('filterChanged');
    }

});

ProductSearch.Models.Main = BaseModel.extend({

    initialize: function (options) {
        BaseModel.prototype.initialize.call(this, options);

        this.set({
            filter: new ProductSearch.Models.Filter({
                query: this.get('query'),
                priceFrom: this.get('priceFrom'),
                priceTo: this.get('priceTo'),
                productTags: this.get('productTags'),
                referrerIds: this.get('referrerIds'),
                mainCategoryMnemonic: this.get('mainCategoryMnemonic')
            }),
            products: new Products.Models.ProductCollection(this.get('products'), options),
            referrers: new Products.Models.Referrers(this.get('referrers'), options)
        });

        this.getFilter().bind('filterChanged', this.filterChanged, this);
        this.getCollection().controller = this;
    },

    filterChanged: function () {
        this.getCollection().load();
        workspace.changeUrl('/products/search.html?' + this.get('filter').getParams());
        Banner.init('https://' + workspace.serviceUrls.mainHost, workspace.appModel.get('myProfile').get('isAuthor'));
    },

    getFilter: function () {
        return this.get('filter');
    },

    getCollection: function () {
        return this.get('products');
    },

    getSearchUrl: function () {
        return (window.location.host === workspace.serviceUrls.shopAppUrl ? '' : 'https://' + workspace.serviceUrls.shopAppUrl) + '/products/search.html?query=' + StringUtils.hashUnescape(this.getFilter().get('searchString'));
    }

});

ProductSearch.Models.MainNew = BaseModel.extend({
    initialize: function (options) {
        BaseModel.prototype.initialize.call(this, options);

        options.limit = options.limit || this.limit;

        this.set({
            filter: new ProductSearch.Models.Filter({
                query: this.query,
                productTags: options.productTags,
                referrerIds: this.get('referrerIds')
            }),
            products: new Products.Models.ProductCollection(this.get('products'), options)
        });

        this.getCollection().controller = this;
    },

    filterChanged: function () {
        this.getCollection().load();
    },

    getFilter: function () {
        return this.get('filter');
    },

    getCollection: function () {
        return this.get('products');
    }
});


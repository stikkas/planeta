/*globals ProductEdit*/
ProductEdit.Views = {};

ProductEdit.Views.ProductEditPage = BaseView.extend({
    template: '#product-edit-onepage-template',

    events: {
        "click .submit-button:not(.disabled)": 'submit'
    },

    modelEvents: {
        'destroy': 'dispose'
    },


    construct: function() {
        this.addChildAtElement('.js-product-description', new ProductEdit.Views.ProductDescription({
            model: this.model
        }));
        this.addChildAtElement('.js-bind-profile', new ProductEdit.Views.BindProfile({
            model: this.model
        }));
        this.addChildAtElement('.js-bind-donate', new ProductEdit.Views.BindDonate({
            model: this.model
        }));
        this.addChildAtElement('.js-preorder', new ProductEdit.Views.Preorder({
            model: this.model
        }));
        this.addChildAtElement('.js-photo', new ProductEdit.Views.ProductPhotoContainer({
            model: this.model
        }));
        this.addChildAtElement('.js-product-tags', new ProductEdit.Views.Tags({
            model: this.model
        }));
        this.addChildAtElement('.js-product-category', new ProductEdit.Views.Category({
            model: this.model
        }));
        this.addChildAtElement('.js-product-price', new ProductEdit.Views.Price({
            model: this.model
        }));
        this.addChildAtElement('.js-product-params', new ProductEdit.Views.Params({
            model: this.model
        }));

        this.addChildAtElement('.js-product-attributes', new ProductEdit.Views.Attributes({
            model: _.extend(this.model, ProductStorages.Model.prototype)
        }));
        this.addChildAtElement('.js-product-infotable', new ProductEdit.Views.InfoTable({
            model: this.model
        }));
        this.addChildAtElement('.js-product-quantity', new ProductEdit.Views.SingleProductQuantity({
            model: _.extend(this.model, ProductStorages.Model.prototype)
        }));
    },

    getData: function() {
        var areaSelector = this.$('#descriptionHtmlArea');
        if (areaSelector) {
            var descrSelector = this.$('#descriptionHtml');
            if (descrSelector) {
                descrSelector.val(areaSelector.tinyMcePlaneta('mappedHtml'));
            }
        }

        var dateInput = this.$("#js-human-date").val();
        if (dateInput) {
            var date = this.$("#js-human-date").data("DateTimePicker").date();
            this.model.set({'startSaleDate': date});
        } else {
            this.model.set({'startSaleDate': null});
        }

        var form = this.$('form');
        return form ? form.serializeObject() : {};
    },

    submit: function() {
        var self = this;
        this.model.saveProduct(this.getData()).done(function() {
            workspace.appView.showSuccessMessage('Товар успешно сохранён', 1000, $('.js-message'));

            var newLocation;
            if (self.model.get('productId')) {
                newLocation = "/admin/products/" + this.get('productId') + "/edit";
            } else {
                newLocation = "/admin/products/planeta.html";
            }
            _.delay(function () {
                document.location.href = newLocation;
            }, 1000);
        }).fail(function(errorMessage) {
            workspace.appView.showErrorMessage(errorMessage, 5000, $('.js-message'));
        });
    }
});

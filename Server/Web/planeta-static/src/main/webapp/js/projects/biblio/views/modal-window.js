/**
 * Модальное окно 
 */
Biblio.Views.ModalWindow = BaseView.extend({
    template: '#biblio-modal-template',
    events: {
        'click .modal-biblio-close': 'close'
    },
    /**
     * @param {Object} options:
     *  - {Biblio.Models.*} bodyModel - model for body (required)
     *  - {Biblio.Views.*} view - view for body (required)
     *  - {String} el - selector for this (required)
     *  - {String} title - title for window (optional)
     *  - {String} clazz - additional css class for this (optional)
     */
    construct: function (options) {
        var self = this;
        self.model = new BaseModel({
            title: options.title || '',
            clazz: options.clazz
        });
        self.body = self.addChildAtElement('.modal-biblio-body', new options.view({
            model: options.bodyModel,
            options: options.viewOpts
        }));
        self.$bg = $('.modal-backdrop-biblio');

    },
    close: function () {
        this.$el.hide();
        this.$bg.hide();
        this.hidden = true;
        this.body.close && this.body.close();
        $('html').removeClass('modal-open');
        $('html').removeClass('theaterMode');
    },
    open: function () {
        this.render();
        this.$el.show();
        this.$bg.show();
        $('html').addClass('modal-open theaterMode');
        this.hidden = false;
    },
    setBodyModel: function (model) {
        this.body.model = model;
    }
});

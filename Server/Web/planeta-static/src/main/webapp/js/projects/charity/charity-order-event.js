var CharityEventOrder = {
    Models: {
        sync: function(url, data) {
            var $dfd = $.Deferred();
            $.ajax(url, _.extend({
                type: 'POST',
                data: JSON.stringify(data),
                dataType: 'json',
                contentType: 'application/json; charset=utf-8',
                success: function(response) {
                    if (response.success) {
                        $dfd.resolve(response.result);
                    } else {
                        $dfd.reject(response.errorMessage, response.fieldErrors);
                    }
                }
            }));

            return $dfd.promise();
        }
    },
    Views: {}
};

CharityEventOrder.Models.Main = BaseModel.extend({
    defaults: {
        fio: "",
        email: "",
        eventOrderId: 0,
        profileId: "",
        phone: "",
        organizationName: "",
        eventGoal: "",
        eventResult: "",
        eventExpectedTime: "",
        eventAudience: "",
        participantsCount: "",
        eventFormat: ""
    }
});

CharityEventOrder.Views.Main = Modal.OverlappedView.extend({
    viewType: "",
    template: '#charity-event-order-view-template',
    viewEvents: {
    },

    construct: function (options) {
        Modal.OverlappedView.prototype.construct.apply(this, arguments);
        this.callbacks = (options && options.callback) || {};
    },

    callback: function (callbackName) {
        var callback = this.callbacks[callbackName];
        if (_.isFunction(callback)) {
            callback(this.model, this);
        }
    },

    makeFocused: function () {
        var $el = this.$('input')[0];
        if ($el.length === 0) {
            $el = this.$("a.close");
        }
        $el.focus();
    },

    send: function () {
        var serializedForm = this.$('form').serializeObject();
        /*serializedForm.organizationRegistrationDate = new Date(serializedForm.year, serializedForm.month - 1, serializedForm.day).getTime();*/
        this.model.set(serializedForm, {silent: true});

        this.$('.error').removeClass('error');
        this.$('.error-message').remove();
        this.callback('send');
        /*if(window.gtm) {
            window.gtm.trackUniversalEvent('successful_crowdschool_request');
        }*/
    }
});

CharityEventOrder.Views.Success = Modal.OverlappedView.extend({
    template: '#charity-event-order-view-success-template'
});
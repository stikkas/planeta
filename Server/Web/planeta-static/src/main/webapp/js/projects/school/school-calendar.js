var Seminars = {
    Models: {
        sync: function(url, data) {
            var $dfd = $.Deferred();
            $.ajax(url, _.extend({
                type: 'POST',
                data: JSON.stringify(data),
                dataType: 'json',
                contentType: 'application/json; charset=utf-8',
                success: function(response) {
                    if (response.success) {
                        $dfd.resolve(response.result);
                    } else {
                        $dfd.reject(response.errorMessage, response.fieldErrors);
                    }
                }
            }));

            return $dfd.promise();
        }
    },
    Views: {}
};

Seminars.Views.TagSelector = BaseView.extend({
    template: '#calendar-seminar-tag-selector-template',
    el: '.seminar-tag-selector',

    events: {
        'change .js-change-tag': 'changeTag'
    },

    construct: function() {
    	var collection = new BaseCollection([], {
            url: "/school/get-tags-exists-in-seminars.json"
        });
    	var self = this;
    	collection.load().done(function(){
    		self.tagListView = self.addChild(new Seminars.Views.TagList({
	            collection: collection,
	            model: new BaseModel()
	        }));
    	});
    },

    afterRender: function() {
        var tagListView = this.tagListView;
        if(tagListView) {
            tagListView.tagId = this.model.get('tagId');
        }
    },

    changeTag: function(el) {
        this.model.set("tagId", el.target.value);
    }
});

Seminars.Views.SeminarItem = BaseView.extend({
    tagName: 'tr',
    template: '#seminar-item-template',

    events: {
        'click .js-register-button': 'openRegisterDialog'
    },

    openRegisterDialog: function() {
        workspace.initRegisterDialogs(this.model.get('seminarId'),
            this.model.get('formatSelection'),
            this.model.get('seminarType'));
    }
});

Seminars.Views.SeminarList = BaseListView.extend({
    tagName: 'tbody',
    itemViewType: Seminars.Views.SeminarItem,
    el: '.seminar-tag-selector',

    seminarChanged: function (seminarModel) {
        var self = this;
        this.collection.data.tagId = seminarModel.get('tagId');
        this.collection.load().done(function(){
            self.render();
        });
    }
});

Seminars.Views.TagItem = BaseView.extend({
    tagName: 'option',
    template: '#tag-item-template',

    beforeRender: function() {
        this.$el.attr('value', this.model.id);
        if (this.model.get('selected')) {
            this.$el.attr('selected', true);
        }
    }
});

Seminars.Views.TagList = BaseView.extend({	
	template: '#tag-selector-template',
    isExistsPlanetaUiElement: true,
    construct: function(options) {
    	var self = this;
    	this.collection.each(function(el){
    		self.addChildAtElement('.pln-selectCampaignById', new Seminars.Views.TagItem({
    			model: el
    		}));
    	});    	
    }
});

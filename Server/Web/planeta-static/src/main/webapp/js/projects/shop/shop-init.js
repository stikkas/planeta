/**
 * Created by asavan on 01.05.2017.
 */

(function() {

    $(document).ready(function() {
    var shoppingCartModel = new Shop.Models.ShoppingCart();
    workspace.shoppingCart = shoppingCartModel;
    workspace.shoppingCart.load().done(function(response) {
        new Shop.Views.ShoppingCartWidget({
            model: shoppingCartModel
        }).render();
    });
    });

})();

/*global Modal,DefaultScrollableListView,PrivacyUtils*/
if (!window.User) {
    var User = {
        Models: {},
        Views: {}
    };
}

User.Models.Subscription = BaseModel.extend({
    url: "/api/profile/get-subscription-info.json",
    defaults: {
        isSubscription: true
    },
    initialize: function (attributes) {
        BaseModel.prototype.initialize.apply(this, arguments);
        this.fetchDfd = this.fetchX({
            data: {
                profileId: attributes.profileId
            }
        });
    }
});
User.Models.Subscriber = User.Models.Subscription.extend({
    url: "/api/profile/get-subscriber-info.json",
    defaults: {
        isSubscription: false
    }
});

User.Views.Subscription = Modal.OverlappedView.extend({
    template: "#user-views-subscription-template",
    urlAnchor: 'subscriptions',
    className: 'modal modal-followers',
    initialize: function () {
        Modal.OverlappedView.prototype.initialize.apply(this, arguments);
        var self = this;
        this.model.fetchDfd.done(function () {
            //wait for load model, that known lastViewSubscribersDate
            self.toggleListView(!!self.model.get('isAdminTab'), self.query);
        });

        this.setQuery($.trim(CommonUtils.urlParam('s')));
        if (CommonUtils.urlParam('admins') === 'true' && PrivacyUtils.isAdmin()) {
            workspace.addParam('admins', true);
            this.model.set('isAdminTab', true);
        }
    },

    onSubscriptionTabClicked: function () {
        if (this.model.get('isAdminTab')) {
            this.toggleListView();
        }
    },

    toggleListView: function (isAdminTab, query) {
        if (_.isUndefined(isAdminTab)) {
            isAdminTab = !this.model.get('isAdminTab');
        }
        if (isAdminTab) {
            workspace.addParam('admins', isAdminTab);
        } else {
            workspace.removeParam('admins', isAdminTab);
        }

        this.model.set('isAdminTab', isAdminTab, {silent: true});
        if (isAdminTab) {
            if (!this.adminListView) {
                this.createAdminListView(query);
            }
        } else {
            if (!this.userListView) {
                this.createUserListView(query);
            }
        }
        this.setQuery(this.getCollection().data.query);
        this.render();
    },

    createUserListView: function (query) {
        var Collection = this.model.get("isSubscription") ? User.Models.Subscription.Collection : User.Models.Subscriber.Collection;
        this.userCollection = new Collection([], {
            data: {
                profileId: this.model.get('profileId'),
                lastViewSubscribersDate: this.model.get("lastViewSubscribersDate"),
                query: query
            }
        });

        this.userListView = new User.Views.Subscription.List({
            controller: this,
            collection: this.userCollection
        });
        this.addChildAtElement(".js-user-list-view", this.userListView);
        this.userCollection.load();

    },

    onAdminTabClicked: function () {
        if (!this.model.get('isAdminTab')) {
            this.toggleListView();
        }
    },

    createAdminListView: function (query) {
        var Collection = this.model.get("isSubscription") ? User.Models.Subscription.Collection : User.Models.Subscriber.Collection;
        this.adminCollection = new Collection([], {
            data: {
                profileId: this.model.get('profileId'),
                isAdmin: true,
                query: query
            }
        });
        this.adminCollection.load();

        this.adminListView = new User.Views.Subscription.List({
            controller: this,
            collection: this.adminCollection
        });
        this.addChildAtElement(".js-admin-list-view", this.adminListView);

    },

    getCollection: function () {
        return this.model.get('isAdminTab') ? this.adminCollection : this.userCollection;
    },

    onSearchQueryChanged: function () {
        var query = $.trim(this.$('input').val());
        if (this.query == query) {
            return;
        }
        this.setQuery(query);
        var collection = this.getCollection();
        collection.reset();
        collection.data.query = query;
        collection.load();
    },

    changeAdminsCount: function (model) {
        this.model.set('adminsCount', this.model.get('adminsCount') + (model.get('isAdmin') ? +1 : -1));
        if (model.collection === this.adminCollection) {
            model.destroy();
            var theSameProfile = this.userCollection.find(function (item) {
                return item.get('profileId') === model.get('profileId');
            });
            if (theSameProfile) {
                theSameProfile.set('isAdmin', model.get('isAdmin'));
            }
        } else if (this.adminListView) {
            this.adminListView.dispose();
            this.adminListView = null;
        }
    },

    setQuery: function (query) {
        if (query) {
            workspace.addParam('s', query);
        } else {
            workspace.removeParam('s');
        }
        this.model.set('query', query, {silent: true});

        this.query = query;
    }
});

User.Views.Subscriber = User.Views.Subscription.extend({
    urlAnchor: 'subscribers'
});


User.Models.Subscription.Collection = BaseCollection.extend({
    url: '/api/profile/get-subscription-list.json',
    limit: 9,
    data: {},
    isSubscription: true,
    parse: function () {
        var self = this;
        var result = BaseCollection.prototype.parse.apply(this, arguments);
        _.each(result, function (item) {
            item.isSubscription = self.isSubscription;
        });
        return result;
    }
});
User.Models.Subscriber.Collection = BaseCollection.extend({
    url: '/api/profile/get-subscriber-list.json',
    isSubscription: false,
    limit: 24
});

User.Views.Subscription.Item = BaseView.extend({
    template: '#user-views-subscription-item-template',
    onSetAdminClicked: function () {
        var self = this;
        var isAdmin = !self.model.get("isAdmin");
        PrivacyUtils.setAdmin(isAdmin, this.model.get("profileId")).done(function (result) {
            if (result && result.success) {
                self.model.set("isAdmin", isAdmin);
                self.controller.changeAdminsCount(self.model);
            }
        });
    }
});

User.Views.Subscription.List = DefaultScrollableListView.extend({
    className: 'n-followers_list cf',
    pagerTemplate: '#user-views-subscription-pager-template',
    itemViewType: User.Views.Subscription.Item,
    emptyListTemplate: '#user-views-subscription-list-empty-template'
});


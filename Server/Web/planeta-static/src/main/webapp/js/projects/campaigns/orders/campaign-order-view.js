/*globals CampaignOrders, Orders, ModalConfirmView, Form, Campaign, GroupListView*/

CampaignOrders.Views = {};
CampaignOrders.Views.BaseReportDownloader = BaseView.extend({
    className: 'btn-group clearfix',
    template: '#order-report-downloader-template',
    events: {
        'click .download-icon-item': 'onChoice',
        'click .format-choice': 'onShowChoiceFormat',
        'click .download': 'onShowChoiceFormat'
    },

    construct: function(options) {
        Orders.initProperty(options, this, 'additionalClass', '');
    },

    afterRender: function() {
        this.$el.addClass(this.additionalClass);
        this.getForm().hide();
    },

    togglePopup: function() {
        Orders.Views.togglePopup(this.el, 'open');
    },

    onShowChoiceFormat: function() {
        this.togglePopup();
    },

    onChoice: function(e) {
        this.togglePopup();
        var format = $(e.currentTarget).data('format');
        this.model.setFormat(format);
        if (this.model.isSetFormat()) {
            this.getForm()
                .attr('action', this.model.getDownloadUrl())
                .submit();
        }
    },

    getForm: function() {
        return this.$el.find('form');
    }

});

CampaignOrders.Views.EmailReportDownloader = CampaignOrders.Views.BaseReportDownloader.extend({
    template: '#order-report-downloader-email-template',
    onChoice: function(e) {
        this.togglePopup();
        var format = $(e.currentTarget).data('format');
        this.model.setFormat(format);
        var url = this.model.getDownloadUrl();
        $.get(url, function(res) {
            workspace.appView.showSuccessMessage("Скоро отчет придет на email");
        });
    }
});

CampaignOrders.Views.Filter = Orders.Views.BaseFilter.extend({
    template: '#campaign-orders-filter-template',
    events: _.extend({}, Orders.Views.BaseFilter.prototype.events, {
        'change #shares': 'onShareChanged'
    }),

    modelEvents: {
        'destroy': 'dispose'
    },

    afterRender: function () {
        Orders.Views.BaseFilter.prototype.afterRender.apply(this, arguments);
        this.$el.find('select.share').dropDownSelect();
    },

    onShareChanged: function (e) {
        e.preventDefault();
        this.selectedShareId = $(e.target).find('option:selected').val();
        this.model.silentSet("shareId", this.selectedShareId);
        this.model.silentSet("selectedShareId", this.selectedShareId);
    }
});

CampaignOrders.Views.CommonChangeDialog = ModalConfirmView.extend({
    template: '#common-change-dialog-template',
    events: {
        'click .cancel': 'onCloseClicked',
        'click .confirm:not(.disabled)': 'onConfirmClicked'
    },

    initialize: function(options) {
        options = (options || {});
        _.extend(this, {
            url: options.url,
            model: new BaseModel(_.extend(options.extraAttr || {}, this.model.attributes)),
            successHandler: options.successHandler
        });

        ModalConfirmView.prototype.initialize.call(this, options);
    },

    onCloseClicked: function(e) {
        e.preventDefault();
        this.dispose();
    },

    onConfirmClicked: function(e) {
        var self = this;
        var el = $(e.target);

        el.toggleClass('disabled');
        self.change().done(function() {
            self.onCloseClicked(e);
        }).fail(function() {
            el.toggleClass('disabled');
        });
    },

    change: function() {
        var self = this;
        var dfd = $.Deferred();

        var options = {
            url: this.getChangeUrl(),
            data: this.$el.find('form *:enabled:not([readonly])').serializeObject(),
            success: function(response) {
                if (Form.isValid(response)) {
                    dfd.resolve();
                    self.onSuccessChanged(response.result);
                } else {
                    dfd.reject();
                    if (response.errorMessage) {
                        workspace.appView.showErrorMessage(response.errorMessage);
                    }
                }
            }
        };
        Backbone.sync('update', this, options);

        return dfd.promise();
    },

    getChangeUrl: function() {
        return this.url;
    },

    onSuccessChanged: function(data) {
        if (_.isFunction(this.successHandler)) {
            this.successHandler(data);
        }
    }
});

CampaignOrders.Views.AddresseeChangeDialog = CampaignOrders.Views.CommonChangeDialog.extend({

    urls: {
        DELIVERY: '/admin/update-order-delivery.json'
    },

    construct: function() {
        var model = this.model;
        var deliverySettings = model.get('deliverySettings');

        this.model.set({
            switchedAddressee: {},
            title: 'Изменение адреса доставки',
            formTemplate: '#delivery-address-change-form-template'
        });
    },

    getChangeUrl: function() {
        return this.urls[this.model.get('deliveryType')];
    }
});

CampaignOrders.Views.ChangeStatusConfirmDialog = ModalConfirmView.extend({
    template: '#change-statuses-dialog-template',

    construct: function(options) {
        if (options.callback && _.isFunction(options.callback.success)) {
            this.save = options.callback.success;
        }
    },

    onCloseClicked: function(e) {
        e.preventDefault();
        this.dispose();
    },

    onConfirmClicked: function(e) {
        this.save(this.$('#reason').val());
        this.onCloseClicked(e);
    }
});

CampaignOrders.Views.OrderStatusChanger = Orders.Views.OrderStatusChanger.extend({
    template: '#campaign-order-status-changer-template',
    data: {},

    events: _.extend({}, Orders.Views.OrderStatusChanger.prototype.events, {
        'change .js-status-dropdown': 'dropdownChange'
    }),

    afterRender: function () {
        Orders.Views.OrderStatusChanger.prototype.afterRender.apply(this, arguments);
        var self = this;
        this.$('.js-input').on('change',function () {
            var that = $(this);
            var name = that.attr('data-name');
            if (self.model.get(name) != that.val()) {
                if (name === "trackingCode" || name === "cashOnDeliveryCost") {
                    self.model.set("isDeliveryInfoChanged", true, {silent: true});
                }
                if (name) {
                    self.model.set(name, that.val(), {silent: true});
                }
            }
        }).trigger('change');

        self.model.set('isSendNotification', this.$('.js-input-checkbox').hasClass('active'), {silent: true});
        this.$('.js-input-checkbox').on('click',function () {
            var that = $(this);
            that.toggleClass('active');
            self.model.set('isSendNotification', that.hasClass('active'), {silent: true});
        }).trigger('change');
    },

    actions: {
        IN_TRANSIT: function(actionName, view, controller) {
            controller.updateOrderState(view.getData(), view.model);
        },

        DELIVERED: function(actionName, view, controller) {
            controller.updateOrderState(view.getData(), view.model);
        },

        CANCEL: function(actionName, view, controller) {
            var options = {
                modelAttrs: {
                    title: 'Аннулирование заказа',
                    text: 'Вы действительно хотите аннулировать заказ и вернуть деньги покупателю?',
                    actionName: actionName
                },
                callback: {
                    success: function(reason) {
                        var data = view.getData();
                        data['reason'] = reason;
                        controller.updateOrderState(data, view.model);
                    }
                }
            };
            view.showConfirm(options);
        },

        GIVEN: function(actionName, view, controller) {
            controller.updateOrderState(view.getData(), view.model);
        }
    },

    dropdownChange: function (e) {
        if(e.target.value && e.target.value == 'GIVEN') {
            this.$('[data-name=trackingCode]').attr('disabled', true);
            this.$('[data-name=trackingCode]').attr('value', '');
            this.$('[data-name=cashOnDeliveryCost]').attr('disabled', true);
            this.$('[data-name=cashOnDeliveryCost]').attr('value', 0);
        } else {
            this.$('[data-name=trackingCode]').attr('disabled', false);
            this.$('[data-name=cashOnDeliveryCost]').attr('disabled', false);
        }
    },

    execute: function(actionName, controller) {
        if (Orders.hasProperty(this.actions, actionName)) {
            this.actions[actionName](actionName, this, controller);
        }
    },

    batchExecute: function(actionName, controller) {
        var self = this;
        var newStatus = this.$el.find('option:selected').text();
        var options = {
            modelAttrs: {
                title: 'Смена статуса',
                text: 'Вы действительно хотите сменить статус у выбранных покупок на "' + newStatus + '"?',
                actionName: actionName
            },
            callback: {
                success: function(reason) {
                    controller.batchExecuteAction(actionName, _.extend({}, self.getData(), {reason: reason}));
                }
            }
        };
        this.showConfirm(options);
    },

    change: function(controller) {
        var actionName = this.getStatuses().actionName;
        if (this.isBatchChanger()) {
            this.batchExecute(actionName, controller);
        } else {
            this.execute(actionName, controller);
        }
    },

    showConfirm: function(options) {
        var view = new CampaignOrders.Views.ChangeStatusConfirmDialog({
            model: new BaseModel(options.modelAttrs),
            callback: options.callback
        });
        view.render();
    },
    getData: function () {
        var fields = ['orderId', 'orderState', 'trackingCode', 'cashOnDeliveryCost', 'deliveryComment', 'isSendNotification'];
        var data = {};
        for (var i in fields) {
            var val = this.model.get(fields[i]);
            if (val != null && val != undefined) {
                data[fields[i]] = val;
            }
        }
        return data;
    }
});

CampaignOrders.Views.BatchActionsPanel = Orders.Views.BatchActionsPanel.extend({
    statusChangerType: CampaignOrders.Views.OrderStatusChanger
});

CampaignOrders.Views.Order = Orders.Views.Order.extend({
    template: '#campaign-order-template',
    statusChangerType: CampaignOrders.Views.OrderStatusChanger
});

CampaignOrders.Views.OrderDetails = BaseView.extend({
    tagName: 'tr',
    className: 'table-collapse-row',
    template: '#share-order-details-template',
    construct: function() {
        var self = this;
        self.model.set({isAdmin: PrivacyUtils.hasAdministrativeRole()}, {silent: true});
    },
    viewEvents: {
        'click .change-addressee': 'onChangeAddresseeClicked',
        'click .change-answer': 'onChangeAnswerClicked'
    },

    togglePopup: function() {
        $('.table-collapse-block', this.el).slideToggle(300);
        Orders.Views.togglePopup(this.el, 'active');
    }

});

CampaignOrders.Views.ListView = GroupListView.extend({
    tagName: 'tbody',
    itemViewTypes: [
        CampaignOrders.Views.Order, CampaignOrders.Views.OrderDetails
    ],
    pagerTemplate: '#pager-template',
    pagerLoadingTemplate: '#pager-loading-template',
    addPagerElement: function (pager) {
        if (this.pagerElement) {
            this.pagerElement.remove();
        }
        this.pagerElement = pager;

        if (!this.pagerElement) {
            return;
        }

        var table = this.$el.parent();
        if (this.sortDirection !== 'asc') {
            if (this.pagerPosition === 'outside') {
                table.before(this.pagerElement);
            } else {
                table.prepend(this.pagerElement);
            }
        } else {
            if (this.pagerPosition === 'outside') {
                table.after(this.pagerElement);
            } else {
                table.append(this.pagerElement);
            }
        }
    }
});

CampaignOrders.Views.Controller = BaseView.extend({
    template: '#campaign-orders-template',

    onInput: function () {
        console.log(arguments);
    },

    construct: function () {
        var filter = this.model.filter || this.model.getFilter();
        this.addChildAtElement('.search-block', new CampaignOrders.Views.Filter({
            model: filter
        }));
        this.addChildAtElement('.project-stats-table', new CampaignOrders.Views.ListViewHeader({
            model: this.model.orders.allSelector
        }));
        this.addChildAtElement('.project-stats-table', new CampaignOrders.Views.ListView({
            collection: this.model.orders
        }));

        this.addChildAtElement('.batch-action-status-changer-placeholder', new CampaignOrders.Views.BatchActionsPanel({
            model: this.model,
            orders: this.model.orders
        }), true);

        this.addChildAtElement('.top-downloader-placeholder', new CampaignOrders.Views.BaseReportDownloader({
            model: this.model.downloader
        }), true);

        this.addChildAtElement('.bottom-downloader-placeholder', new CampaignOrders.Views.BaseReportDownloader({
            model: this.model.downloader,
            additionalClass: 'pull-right'
        }), true);

        this.addChildAtElement('.bottom-downloader-email-placeholder', new CampaignOrders.Views.EmailReportDownloader({
            model: this.model.emaildownloader,
            additionalClass: 'pull-right'
        }), true);

        this.bind('onAllCheckedClicked', this.onToggleAllOrdersSelection, this);
        this.bind('onOrderItemClicked', this.onToggleOrderSelection, this);
        this.bind('onChangeStatus', this.onChangeStatus, this);
        this.bind('onChangeAddresseeClicked', this.onChangeAddresseeClicked, this);
        this.bind('onChangeAnswerClicked', this.onChangeAnswerClicked, this);
    },

    onToggleAllOrdersSelection: function() {
        this.model.orders.toggleAllSelection();
    },

    onToggleOrderSelection: function(view) {
        this.model.orders.toggleSelection(view.model);
    },

    onChangeStatus: function(view) {
        view.change(this.model);
    },

    onChangeAddresseeClicked: function(view) {
        var self = this;
        var model = view.model;
        var shareId = _.first(model.get('orderObjectsInfo')).objectId;

        var deliverySettings = self.model.getShareDeliverySettings(shareId);
        if (deliverySettings) {
            self.showAddresseeChangeDialog(model, deliverySettings);
        } else {
            this.model.loadShareDeliverySettings(shareId).then(function(shareId, loadedDeliverySettings) {
                self.showAddresseeChangeDialog(model, loadedDeliverySettings);
            });
        }
    },

    showAddresseeChangeDialog: function(orderModel, deliverySettings) {
        if (orderModel.get('deliveryType') === 'DELIVERY' || orderModel.get('deliveryType') === 'RUSSIAN_POST') {
            new CampaignOrders.Views.AddresseeChangeDialog({
                model: orderModel,
                extraAttr: {
                    deliverySettings: deliverySettings
                },
                successHandler: function(order) {
                    orderModel.set({
                        deliveryAddress: order.deliveryAddress,
                        deliveryType: order.deliveryType
                    });
                }
            }).render();
        } else {
            workspace.appView.showErrorMessage('Для вознаграждения не настроены параметры доставки.');
        }
    },

    onChangeAnswerClicked: function(view) {
        var model = view.model;

        new CampaignOrders.Views.CommonChangeDialog({
            url: '/admin/update-buyer-answer.json',
            model: model,
            extraAttr: {
                title: 'Изменение ответа покупателя',
                formTemplate: '#buyer-answer-change-form-template'
            },
            successHandler: function(answer) {
                _.each(model.get('orderObjectsInfo'), function(orderObject) {
                    orderObject.comment = answer;
                });
                model.change();
            }
        }).render();
    }

});

CampaignOrders.Views.ListViewHeader = BaseView.extend({
    tagName: 'thead',
    template: '#order-list-header-template',

    viewEvents: {
        'change input[type="checkbox"]': 'onAllCheckedClicked'
    },

    afterRender: function() {
        this.$('input:checkbox').checkbox();
    }
});
CampaignOrders.Views.Page = Campaign.Views.BasePage.extend({
    contentViewClass: CampaignOrders.Views.Controller
});
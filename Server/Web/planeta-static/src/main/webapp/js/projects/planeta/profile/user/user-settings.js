/*globals RegionAutocompleter,News,TinyMcePlaneta,moduleLoader*/
if (!window.User) {
    var User = {
        Models: {},
        Views: {}
    };
}
User.Views.Settings = {};
User.Models.Settings = {};

User.Views.Settings.General = BaseView.extend({
    template: '#user-views-settings-general-template',
    isExistsPlanetaUiElement: true,
    modelEvents: {
        'destroy': 'dispose'
    },
    initialize: function (options) {
        this.model = options.contentModel.get("profileModel");
        var hint;

        if (workspace && workspace.currentLanguage && workspace.currentLanguage == 'en') {
            hint = 'Change the URL of your page. The URL can contain only letters and numbers. The URL change is possible only once.';
        } else {
            hint = 'Поменяйте адрес вашей страницы. Адрес может содержать только латинские буквы и цифры. Смена адреса возможна только один раз.';
        }

        this.model.set({
            hint: hint
        });
        BaseView.prototype.initialize.apply(this, arguments);
    },
    construct: function () {
        this.tinyView = this.addChildAtElement('[data-anchor=tiny-mce]', new User.Views.Settings.Summary({
            model: this.model
        }));
    },
    afterRender: function () {
        News.Views.disableTinyView();
        this.tinyView.initTiny();

    },

    onUnsubscribe: function () {
        this._unsubscribe(true).done(function() {workspace.appView.showSuccessMessage("Вы успешно отписаны от рассылки")});
    },

    onSubscribe: function () {
        this._unsubscribe(false).done(function() {workspace.appView.showSuccessMessage("Вы успешно подписаны на рассылку")});
    },

    _unsubscribe: function(isUnsubscribe) {
        var data = {profileId : this.model.get('profileId'), unsubscribe: isUnsubscribe };
        return Backbone.sync('update', null, {
            url: '/api/profile/unsubscribe-mailing.json',
            data: data,
            successMessage: false,
            errorMessage: true
        });
    },

    onSaveSettingsGeneralClicked: function () {
        this.tinyView.editorModel.saveToParentModel();
        var self = this;
        var data = this.collectData(this.$el);
        data.gender = data.userGender;
        data.summary = this.model.get('summary');
        data.profileId = this.model.get('profileId');
        Backbone.sync('update', null, {
            url: '/api/profile/profile-save-general-settings',
            data: data,
            successMessage: true,
            errorMessage: true
        }).done(function (response) {
            self.showErrors(self.$el, response);
            if (response.success) {
                self.model.set(response.result);
            }
        });
    }
});

User.Models.Settings.Contacts = BaseModel.extend({
    url: '/api/public/profile-sites',
    prefetch: function (options) {
        var profileId = this.contentModel.get("profileModel").get("profileId");
        
        if (!profileId) {
            workspace.stats.tackNoProfileId('UserModelsSettingsContacts');
        }

        options = _.extend({}, options, {
            data: {
                profileId: profileId || -1
            }
        });
        BaseModel.prototype.prefetch.call(this, options);
    },
    parse: function (response) {
        if (!response || !response.status) {
            return {
                profileId: workspace.appModel.get("profileModel").get('profileId')
            };
        }
        return response.result;
    }

});

User.Views.Settings.Contacts = BaseView.extend({
    template: '#user-views-settings-contacts-template',
    modelEvents: {
        'change:fieldErrors': 'render',
        'destroy': 'dispose'
    },
    onFieldValueChange: function (e) {
        var $el = $(e.currentTarget);
        var fieldName = $el.attr('name');
        var url_prefix = $el.attr('url-prefix');
        var fullUrl = url_prefix + $el.val();
        this.model.set(fieldName, fullUrl);
    },
    onSaveContactsClicked: function () {
        var self = this;
        var fail = function (response) {
            if (response && response.fieldErrors) {
                self.model.set({
                    fieldErrors: response.fieldErrors
                });
                self.showErrors(self.$el, response);
            } else {
                workspace.appView.showErrorMessage('При сохранении произошла ошибка');
            }
        };


        if (self.model.get('profileId')) {
            Backbone.sync('update', this.model, {
                url: '/api/profile/save-my-sites'
            }).done(function (response) {
                if (response && response.success) {
                    if (response.result) {
                        self.model.set(response.result);
                    }
                    self.model.set('fieldErrors', null);
                    self.$('.js-success-save').fadeIn(300).delay(800).fadeOut(400);
                } else {
                    fail(response);
                }
            }).fail(function () {
                fail();
            });
        }
    }

});

User.Views.Settings.Password = BaseView.extend({
    template: '#user-settings-password-template',
    onSavePasswordClicked: function (e) {
        e.preventDefault();
        var self = this;
        var data = $(this.el).find('form').serializeObject();
        data.profileId = this.model.get('profileModel').get('profileId');

        var options = {
            url: '/profile/user-save-password.json',
            data: data,
            context: this,
            successMessage: "Пароль успешно изменен",
            success: function (result) {
                if (result && result.success === false) {
                    self.showErrors(result.fieldErrors);
                } else {
                    self.clean();
                }
            }
        };
        Backbone.sync('update', null, options);
    },
    showErrors: function (fieldErrors) {
        var self = this;
        this.$('.js-error').hide().attr("data-tooltip", "");
        _.each(fieldErrors, function (val, key) {
            self.$("[field=" + key + "]").find(".js-error").show().attr("data-tooltip", val);
        });
    },
    clean: function () {
        this.$('.js-error').hide().attr("data-tooltip", "");
        this.$('input').val("");
    }
});

User.Models.Settings.TinyEditor = function (parentModel) {
    _.extend(this, new TinyMcePlaneta.EditorModel(parentModel), {
        config: TinyMcePlaneta.getUserSettingsSummaryEditorConfiguration(),
        height: 400,
        width: '100%',
        saveToParentModel: function () {
            this.parentModel.set({
                summary: this.html()
            }, {silent: true});
        }
    });
};

User.Views.Settings.Summary = News.Views.TinyEditor.extend({
    className: 'ww-flat edit-blog2',
    template: '#user-views-settings-summary-tiny-mce-template',
    createEditorModel: function (model) {
        return new User.Models.Settings.TinyEditor(model);
    },
    initTiny: function () {
        var model = this.model;
        var self = this;
        var $dfd = $.Deferred();
        if (News.Views._tinyViewInited) {
            $dfd.resolve();
            return $dfd;
        }

        this.generatedId = this.generatedId || this.generateId();
        moduleLoader.loadModule('tiny-mce-campaigns').done(function () {
            var tinyModel = self.editorModel || self.createEditorModel(model);
            var $element = self.$('.tinymce-body').attr({id: self.generatedId});
            $element.height(400);
            var onDisable = function () {
                self.render();
            };
            setTimeout(function () {
                tinyModel.init($element);
                self.editorModel = tinyModel;
                $dfd.resolve();
                News.Views.initTinyView(onDisable);
                $('.edit-blog-body').fadeIn();
                $('.edit-blog-body-stub').hide();
            }, 100);
            self.$('.tb-bottom').hide();
        });
        return $dfd;
    },
    dispose: function () {
        if (this.editorModel) {
            News.Views.disableTinyView();
            this.editorModel.destruct();
        }
    }
});

User.Views.Settings.Tabs = BaseView.extend({
    template: '#user-views-settings-tabs-template',
    className: 'tabs_wrap',
    initialize: function (options) {
        options.controller.pageData = this.pageData();
    },

    pageData: function () {
        var creatorProfile = workspace.appModel.get('profileModel');
        var displayName = creatorProfile.get('displayName');

        var title = $('title').html();
        var description = $('meta[name=description]').html();

        switch (workspace.navigationState.get('subsection')) {
            case 'general':
                title = 'Мои настройки';
                description = _.template('Основные настройки профиля <%=displayName%> на платформе краудфандинга Planeta.ru', {
                    displayName: displayName
                });
                break;
            case 'contacts':
                title = 'Мои контакты';
                description = _.template('Контакты <%=displayName%> на платформе краудфандинга Planeta.ru', {
                    displayName: displayName
                });

                break;
            case 'password':
                title = 'Изменение пароля';

                break;
        }

        var result = {
            title: title,
            description: description
        };

        return result;
    }
});

User.Views.Settings.GroupTabs = BaseView.extend({
    template: '#group-views-settings-tabs-template'
});






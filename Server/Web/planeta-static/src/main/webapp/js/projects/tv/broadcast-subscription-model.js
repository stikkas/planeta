var BroadcastHelper = {};
BroadcastHelper.Models = {};
BroadcastHelper.Views = {};

/*
 * Model for broadcast subscription controls
 */
BroadcastHelper.Models.BroadcastSubscription = BaseModel.extend({

    cancelSubscription: function (e) {
        var self = this;
        $.ajax({
            url: '/broadcast/broadcast-deleteByProfileId-subscription.json',
            type: 'POST',
            data: this.toJSON(),
            dataType: 'json',
            success: function (response) {
                if (response && response.success) {
                    self.set(response.result);
                }
            }
        });
    },

    updateSubscription: function (email) {
        var self = this;
        this.set({"email": email}, {silent: true});
        $(".js-accept-subscribe").parent().removeClass("error");
        $.ajax({
            url: '/broadcast/broadcast-update-subscription.json',
            type: 'POST',
            data: this.toJSON(),
            dataType: 'json',
            success: function (response) {
                if (response && response.success) {
                    self.set(response.result);
                } else {
                    if (response.fieldErrors) {
                        var emailError = response.fieldErrors.email;
                    } else {
                        emailError = response.errorMessage;
                    }
                    if (emailError && emailError.toLocaleLowerCase().indexOf("адрес занят") > -1) {
                        var email = self.get("email");
                        LazyHeader.showAuthForm('signup', {
                            email: email,
                            focusPassword: true
                        });
                        $("[name=username]").val(email);
                    } else if (emailError && emailError.toLocaleLowerCase().indexOf("неправильный e-mail") > -1) {
                        $(".js-accept-subscribe").parent().addClass("error");
                    }
                }
            }
        });
    }
});
Biblio.Views.Library = BaseView.extend({
	template: '#library-item-template',
	className: 'biblio-library_i',
	events: {
		'click .biblio-library_link': 'onChoose'
	},
	onChoose: function() {
		this.model.changeSelected();
	}	
});

Biblio.Views.Marker = BaseView.extend({
	template: '#library-marker-template',
	onChoose: function() {
		if(!this.model.get('selected')) {
			this.marker.setIcon(this.parent.markerCheckImage);
		} else {
			this.marker.setIcon(this.parent.markerImage);
		}        
        this.parent.markerClusterer.removeMarker(this.marker); 
		this.model.changeSelected();   	
	},
	beforeRender: function() {
		var latLng = new google.maps.LatLng(this.model.get('latitude'), this.model.get('longitude'));

		var icon = this.parent.markerImage;
		var text = 'Выбрать библиотеку';
		
		if(this.model.get('count') > 1) {
			text = '';
			icon = this.parent.markerClusterImage;
		} else if(this.model.get('selected')) {
			icon = this.parent.markerCheckImage;
			text = 'Отменить выбор';
		}
        var marker = new Biblio.Marker({
            position: latLng,
            draggable: false,
            icon: icon,
            name: this.model.get('name'),
            address: this.model.get('address') || ''
        });
        
        if(this.model.get('count') > 1){
        	marker.count = this.model.get('count');
        	marker.setLabel({
        			text: ''+this.model.get('count'),
        			style: 'font-size:16px;'
        		});
		}	
        
        this.marker = marker;
        
        var self = this;

        (function (marker) {
            google.maps.event.addListener(marker, 'click', function () {
            	if(!marker.label) {
            		if (self.parent.activeMarker == marker && self.parent.infoBubble.isOpen()) {
	                	self.parent.infoBubble.close(self.parent.map, self.parent.activeMarker);
	                	self.parent.activeMarker = null;
	                } else {
                        if(self.parent.options.noLibraryChooseButton) {
                            self.parent.infoBubble.setContent(
                                '<div class="biblio-map-tooltip">\
                                    <div class="biblio-map-tooltip_cont">\
                                        <div class="biblio-map-tooltip_name">' + marker.name + '</div>\
                                        <div class="biblio-map-tooltip_text">' + marker.address + '</div>\
				                        <div class="biblio-map-tooltip_action">\
				                        </div>\
				                    </div>\
				                </div>'
                            );
                        } else {
                            self.parent.infoBubble.setContent(
	                            '<div class="biblio-map-tooltip">\
				                    <div class="biblio-map-tooltip_cont">\
				                        <div class="biblio-map-tooltip_name">' + marker.name + '</div>\
                                        <div class="biblio-map-tooltip_text">' + marker.address + '</div>\
				                        <div class="biblio-map-tooltip_action">\
				                            <span class="btn btn-primary js-check-library">' + text + '</span>\
				                        </div>\
				                    </div>\
				                </div>'
	                            );

							// TODO attention clearInterval
							var waitButton = setInterval(function () {
								var btn = $('.js-check-library');
								if (!btn.length)
									return;
								clearInterval(waitButton);

								btn.on('click', function () {
									self.onChoose();
									self.parent.infoBubble.close(self.parent.map, self.parent.activeMarker);
									if(self.model.get('selected')) {
										self.parent.trigger('choose');
									}
								});
							}, 100);
                        }

	                    self.parent.infoBubble.close(self.parent.map, self.parent.activeMarker);
	                    self.parent.infoBubble.open(self.parent.map, marker);
	                    self.parent.activeMarker = marker;
	                }
            	} else {
            		self.parent.map.setZoom(self.parent.minZoom+1);
            		self.parent.map.setCenter(marker.getPosition());
            	}
            });
        })(marker);

        this.parent.markers.push(marker);
        this.parent.markerClusterer.addMarker(marker);
	}
});

Biblio.Views.LibraryList = DefaultListView.extend({
    itemViewType: Biblio.Views.Library
});

Biblio.Views.LibraryMap = BaseListView.extend({
    itemViewType: Biblio.Views.Marker,
	construct: function () {
    	this.mapCenter = new google.maps.LatLng(56.732834, 63.128374);
    	this.map = new google.maps.Map(document.getElementById('map'), {
            disableDefaultUI: true,
            zoomControl: true,
            zoom: 4,
            minZoom: 2,
            center: this.mapCenter,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            scrollwheel: false
        });
    	this.infoBubble = new InfoBubble({
            maxWidth: 300,
            hideCloseButton: true,
            arrowSize: 10,
            borderRadius: 0,
            shadowStyle: 0,
            padding: 0,
            borderWidth: 0,
            disableAutoPan: true
        });
    	this.styles = [{
            url: workspace.staticNodesService.getResourceUrl('/images/biblio/map-pin.png'),
            width: 38,
            height: 39,
            anchor: [0, 0],
            textColor: '#132129',
            textSize: 16,
            iconAnchor: [19, 39]
        }];
    	this.markers = [];
    	this.markerClusterImage = {
    			url: workspace.staticNodesService.getResourceUrl('/images/biblio/map-pin.png'),
                size: new google.maps.Size(38, 39),
                origin: new google.maps.Point(0, 0),
                anchor: new google.maps.Point(19, 19)
		};
    	this.markerImage = new google.maps.MarkerImage(
    			workspace.staticNodesService.getResourceUrl('/images/biblio/library-pin.png'),
                new google.maps.Size(38, 39),
                new google.maps.Point(0, 0),
                new google.maps.Point(19, 19)
                );
    	this.markerCheckImage = new google.maps.MarkerImage(
    			workspace.staticNodesService.getResourceUrl('/images/biblio/library-map-check.png'),
                new google.maps.Size(38, 39),
                new google.maps.Point(0, 0),
                new google.maps.Point(19, 19)
                );
		this.markerClusterer = new MarkerClusterer(this.map, this.markers, {
            maxZoom: 16,
            gridSize: null,
            styles: this.styles
        });
		
		this.markerClusterer.setCalculator(function(markers, numStyles) {
			var index = 0;
			var count = _.reduce(markers, function(count, marker){
					return count + (marker.count||1);
				}, 0);
			var dv = count;
			while (dv !== 0) {
				dv = parseInt(dv / 10, 10);
				index++;
			}
			
			index = Math.min(index, numStyles);
			return {
				text: count,
				index: index
			};
		});

    	this.activeMarker = null;
		this.collection.on("reset", this.onLibraryDrop, this);
		this.minZoom = this.options.minZoom;
		var self = this;
		google.maps.event.addListener(this.map, 'bounds_changed', _.debounce(function(event){
				if(!self.frozen && !self.silentMove)
					self.mapChange(event);
				self.silentMove = false;
			}, 300));
	},
	mapChange: function(event) {
		this.infoBubble.close(this.map, this.activeMarker);
		this.activeMarker = null;
		var bnd = this.map.getBounds().toJSON();
		var self = this;
		this.collection.data = {
			zoomLevel: this.map.getZoom(),
			north: bnd.north,
            south: bnd.south,
            east: bnd.east,
            west: bnd.west,
            query: this.collection.data.query,
            libraryType: this.collection.data.libraryType
		};
		if (!this.collection.data.libraryType) {
            delete this.collection.data.libraryType;
		}
		this.collection.loading = false;
		return this.collection.load();
	},
	onLibraryDrop: function() {
		this.markerClusterer.removeMarkers(this.markers);
		this.markers = [];
	},
	setMapCenter: function(center, zoom, frozen, silent) {
		this.frozen = frozen;
		this.silentMove = silent;
		this.map.setCenter(center);
		this.map.setZoom(zoom);
	}
});

Biblio.categories = {
    ALL: {name: "Все"} ,
    PUBLIC: {name: "Публичные"} ,
    SCIENTIFIC: {name: "Научные"},
    SCHOLASTIC: {name: "Школьные"},
	DOMESTIC: {name: "Детские и семейные"},
    SPECIAL: {name: "Специализированные"}
};

/**
 * Абстрактрый класс для отображения категорий
 */
Biblio.Views.LibraryCategoryTag = BaseView.extend({
    events: {
        'click span': 'choose'
    },
    choose: function (e) {
        _.each(['.js-library-type-choose', '.biblio-wizard-tabs_drop-list'], function (it) {
            $(it).children().each(function () {
                var it = $(this);
                if (it.hasClass('active')) {
                    it.removeClass('active');
                    return false;
                }
            });
        });
        this.$el.addClass('active');
        this.controller.set('libraryType', this.model.get('libraryType'));
    }
});


Biblio.Views.LibraryCategory = Biblio.Views.LibraryCategoryTag.extend({
    template: '#biblio-wizard-main_tag',
    className: 'biblio-wizard-tabs_i'
});


Biblio.Views.LibraryChoose = BaseView.extend({
	template: '#library-choose-template',
	events: {
		'keyup input': 'changeQuery',
		'click .biblio-show-checked': 'showSelected',
		'click .biblio-switch-list': 'changeView'
	},
	changeQuery: _.debounce(function (e) {
        this.model.set('query', $(e.currentTarget).val());
    }, 1000),
	construct: function () {
		var self = this;
		this.states = {'-1': 'library', '1': 'map'};
		this.libraryCollection = this.model.get('libraryCollection');
		this.mapCollection = this.model.get('mapCollection');
		var minZoom = this.model.get('minZoom');
		this.model = this.model.get('filter');
		this.model.set('showActive', false);
		this.libraryView = new Biblio.Views.LibraryList({collection: this.libraryCollection, el: '.js-biblio-libraries'});
		this.libraryView.parent = this;
		this.mapView = new Biblio.Views.LibraryMap({collection: this.mapCollection, minZoom: minZoom, el: '#map'});
		this.mapView.parent = this;
		var currState = this.states[this.model.get('state')];
		this.collection = this[currState+'Collection'];
		$('.biblio-'+currState).toggleClass('hidden');
		this.model.on("change", this.searchLibrary, this);
		this.model.set('query', this.options.query || '');

        _.each(Biblio.categories, function (data, key ) {
            var view;
			view = self.addChildAtElement('.js-library-type-choose', new Biblio.Views.LibraryCategory({
				model: new BaseModel(_.extend(data, {libraryType: key})),
				controller: self.model
			}));
            if (key === 'ALL') {
                view.$el.addClass('active');
            }
        });
	},
	searchLibrary: function() {
		var self = this;
		var query = this.model.get('query');
		this.model.set({'showActive': false}, {silent: true});
		workspace.changeUrl('/library?query=' + query);
		this.collection.data.query = query;
		this.collection.loading = false;
		if (this.model.get('libraryType') !== 'ALL') {
            this.collection.data.libraryType = this.model.get('libraryType');
		} else {
            delete this.collection.data.libraryType;
		}
		this.collection.load({
			success: function() {
	            var searchInput = $('.js-library-filter', self.el).focus();
	            searchInput.val(query);
			}});
	},
	addRandomLibrary: function() {
		var self = this;
		return Backbone.sync('read', null, {
			url: '/library/library-random.json'
		}).done(function(response){
			if (Biblio.data.bin) {
                Biblio.data.bin.addLibrary(new Biblio.Models.Library(response));
                Biblio.data.bin.save();
			}
			var libZoom = 15;
			self.libraryCollection.add([response]);
			self.mapCollection.add([response]);
			self.mapView.setMapCenter({lat: response.latitude, lng: response.longitude}, libZoom, self.model.get('showActive'), true);
		});
	},
	showSelected: function() {
		this.model.set('showActive', !this.model.get('showActive'), {silent: true});
		if(this.model.get('showActive')) {
			this.collection.reset(Biblio.data.bin.data().libraries);
			this.collection.data.query = '';
			this.collection.loading = true;
			this.mapView.frozen = true;
			this.render();
			$('.js-library-filter', this.el).val('');
		} else {
			this.mapView.frozen = false;
			this.model.set('query', '');
		}
	},
	changeView: function() {
		var currState = this.states[this.model.get('state')];
		$('.biblio-'+currState).toggleClass('hidden');
		var newState = -1*this.model.get('state');
		currState = this.states[newState];
		this.collection = this[currState+'Collection'];
		$('.biblio-'+currState).toggleClass('hidden');
		this.model.set({'state': newState, 'showActive': false});
	}
});

/**
 * Надпись под корзиной "столько-то выбранных изданий пойдут во столько-то библиотеки" 
 */
Biblio.Views.AddInfoLib = BaseView.extend({
    template: '#wizard_add_info-library-template',
    el: '.biblio-wizard_add-info'
});


/*global User, moduleLoader, TinyMcePlaneta, Modal, BaseRichView, News, PrivacyUtils*/

User.Views.News = BaseView.extend({
    className: 'feed',
    template: '#user-news-template',
    construct: function () {
        if (PrivacyUtils.isAdmin()) {
            this.addChildAtElement('.post-write', new User.Views.News.CreatePostView({
                model: this.getNews().createPost()
            }));
        }
        var profileId = workspace.appModel.get('profileModel').get('profileId');
        var authorProfileId = workspace.appModel.get('profileModel').get('creatorProfileId') || profileId;
        var campaignId = workspace.appModel.get('contentModel').get('campaignId') || 0;
        if (workspace.appModel.isCurrentProfileMine()) {
            var filterPostModel =  new BaseModel({
                profileId: profileId,
                onlyMyNews: false
            });
            filterPostModel.bind("change:onlyMyNews", function () {
                this.model.loadItems(filterPostModel.get('onlyMyNews'));
            }, this);
            this.addChildAtElement('.js-feed_filter', new User.Views.News.FilterPostView({
                model: filterPostModel
            }));
        }
        this.addChildAtElement('.feed_list', new News.Views.Posts({
            collection: this.getNews(),
            model: new BaseModel({
                profileId: profileId,
                authorProfileId: authorProfileId,
                campaignId: campaignId
            })
        }));
    },
    getNews: function () {
        return this.model.items;
    }
});

User.Models.News = BaseModel.extend({
    initialize: function () {
        this.items = new News.Models.Posts([], {
            limit: 10
        });
    },

    fetch: function () {
        this.loadItems(false);
        BaseModel.prototype.fetch.apply(this, arguments);
    },

    pageData: function () {
        var profileModel = this.get('profileModel');
        var cityName = profileModel.get('cityName');
        var userBirthDate = profileModel.get('userBirthDate');

        return {
            image: profileModel.get('imageUrl'),
            title: "Новости " + profileModel.get('displayName') + ' | Planeta',
            description: _.template("Новости <%=displayName%><%=age%><%=city%>. Подробная информация доступна для авторизованных пользователей. Зарегистрируйтесь, чтобы найти новых друзей на Planeta.ru", {
                displayName: profileModel.get('displayName'),
                age: !cityName ? "" : ", " + cityName,
                city: !userBirthDate ? "" : ", " + StringUtils.declOfNumWithNum(DateUtils.getAge(userBirthDate), ["год", "года", "лет"])
            })
        };
    },

    loadItems: function (onlyMyNews) {
        this.items.data = {
            profileId: workspace.appModel.get('profileModel').get('profileId'),
            onlyMyNews: onlyMyNews
        };
        this.items.load();
    }
});
User.Views.News.CreatePostView = BaseView.extend({
    className: 'post-write',
    template: '#user-create-post-template',
    modelEvents: {
        'destroy': 'dispose'
    },
    construct: function () {
        this.tinyView = this.addChildAtElement('[data-anchor=tiny-mce]', new News.Views.TinyEditor({
            model: this.model
        }));
    },

    afterRender: function () {
        News.Views.disableTinyView();
        this.tinyView.initTiny();
    },
    onSave: function () {
        var self = this;
        var title = this.$('input.post-write_head_input').val();
        var postTest = this.tinyView.editorModel.html();
        this.model.save(title, postTest).done(function () {
            self.model.clear({silent: true});
            self.model.set(self.model.collection.createPost());
            self.render();
            self.onClose();
        }).fail(function (errorMessage) {
            workspace.appView.showErrorMessage(errorMessage);
        });
    },
    onFocus: function () {
        this.toggle(true);
    },
    onClose: function () {
        this.toggle(false);
    },
    toggle: function (on) {
        var self = this;
        self.tinyView.initTiny().done(function () {
            self.$el.toggleClass('post-write__open', !!on);
        });
    }
});
User.Views.News.FilterPostView = BaseView.extend({
    className: 'feed_filter',
    template: '#user-filter-post-template',
    showMyNews: function () {
        this.model.set({onlyMyNews: true});
    },
    showAllNews: function () {
        this.model.set({onlyMyNews: false});
    }
});





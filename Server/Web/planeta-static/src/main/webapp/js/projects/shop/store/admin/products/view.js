/*globals Shop, ShopUtils, Modal, Form, GroupListView, ProductStorages*/

Shop.Views.Products = BaseView.extend({

    template: '#store-admin-products-template',
    events: {
        'deleteProduct': 'deleteProduct',
        'cloneProduct': 'cloneProduct',
        'change .js-mark-filter-category-item': 'onCategoryChange',
        'change #status': 'onStatusChange',
        'keyup .js-search-query': 'queryChanged'
    },
    cloneProduct: function(e, view) {
        var self = this;
        Modal.showConfirm("Клонировать этот товар?", "Подтверждение клонирования", {
            success: function() {
                $.when(self.model.cloneProduct(view.model)).done(function(e) {
                    window.location.href = '/admin/products/' + e + '/edit';
                }).fail(function() {
                    workspace.appView.showErrorMessage("Ошибка при попытке клонировании товара");
                });
            }
        });
    },


    deleteProduct: function(e, view) {
        var self = this;

        var title = null,
            messageHTMLText = null;
        if (view.model.get("productState") == 'GROUP') {
            title = "Подтверждение расформирования группы товаров";
            messageHTMLText = "<p>Вы точно хотите расформировать \"" + view.model.get("name") + "\"?</p>" +
                "<p>После удаления группы, товары, ранее входившие в состав группы, станут самостоятельными.</p>";
        } else {
            title = "Подтверждение удаления товара.";
            messageHTMLText = "<p>Вы точно хотите удалить \"" + view.model.get("name") + "\"?</p>";
        }
        Modal.showConfirm(messageHTMLText, title, {
            success: function() {
                $
                    .when(self.model.deleteProduct(view.model))
                    .done(function() {
                        if (view.model.collection.controller) {
                            view.model.collection.controller.trigger('change:quantity', view.model, '-' + view.model.get('totalQuantity') + ":delta");
                        }
                        view.parent.collection.remove(view.model);
                        workspace.appView.showSuccessMessage("Товар помещен в архив.");
                        self.model.fetch();
                    })
                    .fail(function(e) {
                        workspace.appView.showErrorMessage("Ошибка при попытке удаления товара: " + e);
                    });
            }
        });
    },
    _prepareData: function(data) {
        var products = this.model.get('products');
        _.extend(products.data, data);
        return products;
    },
    onFilterChange: _.debounce(function(data) {
        var products = this._prepareData(data);
        products.offset = 0;
        products.limit = 20;
        return products.load();
    }, 500),
    queryChanged: function(e) {
        var query = $(e.currentTarget).val();
        this._prepareData({query: query});
        this.onFilterChange();
    },
    onCategoryChange: function(e) {
        var $el = this.$(e.currentTarget);
        $el.addClass('active');
        this._prepareData({tagId: $el.val() || 0});
        this.onFilterChange();
    },
    onStatusChange: function (e) {
        var val = $(e.currentTarget).val();
        this._prepareData({status: val});
        this.onFilterChange();
    },
    construct: function() {

        this.addChildAtElement('#products-table', new Shop.Views.ProductsList({
            controller: this.model,
            collection: this.model.get('products')
        }));
    }
});

Shop.Views.ProductsListItemGroupMembers = BaseView.extend({
    template: '#store-admin-products-item-group-members-template',
    className: 'table-collapse-block level-group-table-collapse-block',

    construct: function() {
        this.addChildAtElement('.group-product-manage-table', new Shop.Views.ProductsList({
            controller: this.controller,
            collection: this.collection
        }));
    }
});

Shop.Views.templateCollapseRow = BaseView.extend({
    tagName: 'tr',
    className: 'table-collapse-row', //'.content-anchor'
    template: "#empty-row-template",

    construct: function() {
        switch (this.model.get('productState')) {
        case 'GROUP':
            this.model.fetch();
        case 'META':
            this.contentView = this.addChildAtElement('td.content-anchor',
                new Shop.Views.ProductsListItemGroupMembers({
                    controller: this.model,
                    collection: this.model.get('childrenCollection')
                }));
            break;
        case 'ATTRIBUTE':
            break;
        case 'SOLID_PRODUCT':
            break;
        default:
            console.error('productState is not defined.');
        }
    },
    //-------
    isOpen: function() {
        return this.$el.hasClass('active');
    },
    hide: function() {
        this.$el.removeClass('active');
    },
    show: function() {

        if (this.model.get('productState') != 'GROUP') {
            this.$el.addClass('active');
            var self = this;
            this.$el.clickOff(function() {
                self.$el.removeClass('active');
            });
        } else if (this.model.get('childrenCount') > 0) {
            this.$el.addClass('active');
        }

    },

    afterRender: function() {
        if (this.model.get('productState') == 'GROUP') {
            this.show();
        }
    }
});

Shop.Views.ProductCount = BaseView.extend({
    tagName: 'span',
    className: 'quantity',
    template: '#products-quantity-template',
    modelEvents: _.extend({}, BaseView.prototype.modelEvents, {
        'model change:quantity': 'softRefresh'
    }),
    softRefresh: function(model, newValue) {
        var value = this.model.get('totalQuantity');
        var parsed = parseInt(newValue, 10);
        if (/\d.*?:delta/i.test(newValue)) {
            value += parsed;
        } else {
            value = parsed;
        }

        this.model.set({
            totalQuantity: value
        }, {silent: true});

        if (this.model.collection.controller) {
            this.model.collection.controller.trigger('change:quantity', model, newValue);
        }

        this.render();
    }
});

Shop.Views.ProductsListItem = BaseView.extend({
    tagName: 'tr',
    template: '#store-admin-products-item-template',

    viewEvents: {
        'click .js-delete-product': 'deleteProduct',
        'click .js-clone': 'cloneProduct'
    },
    events: {
        'click .js-pause-sale': 'pauseSale',
        'click .js-start-sale': 'startSale',
        'click .js-show-children': 'toggleDropDownRow'
    },

    construct: function() {
        if (this.model.get('productState') == 'GROUP') {
            this.$el.addClass('tr-collapse-active');
        }
        this.addChildAtElement('span.quantity', new Shop.Views.ProductCount({model: this.model}), true);
        this.addChildAtElement('.js-product-tags', new ProductEdit.Views.TagsInProductList({
            model: this.model
        }));

        if (this.model.get('productState') == 'ATTRIBUTE' || this.model.get("productState") == "SOLID_PRODUCT") {
            this.controller && this.model.set({
                parent: this.controller.toJSON()
            }, {silent: true});
        }
    },

    killZombieTooltips: function() {
        $('.planet-input-tooltip').remove();
    },
    restoreActionsBlock: function() {
        this.$('.manage-action').removeClass('hidden');
        this.$('.action-processing').addClass('hidden');
    },
    showActionProcessing: function() {
        this.$('.manage-action').addClass('hidden');
        this.killZombieTooltips();
        this.$('.action-processing').removeClass('hidden');
    },
    _showErrorResponseMessage: function(errorMessage) {
        workspace.appView.showErrorMessage(errorMessage);
    },
    startSale: function(e) {
        this.showActionProcessing();
        var self = this;
        this.model.startSale().fail(_.bind(self.restoreActionsBlock, self))
            .fail(_.bind(self._showErrorResponseMessage, self))
            .always(_.bind(self.killZombieTooltips, self));
    },

    pauseSale: function(e) {
        this.showActionProcessing(e);
        var self = this;
        this.model.pauseSale().fail(_.bind(self.restoreActionsBlock, self))
            .fail(_.bind(self._showErrorResponseMessage, self))
            .always(_.bind(self.killZombieTooltips, self));
    },

    afterRender: function() {
        this.killZombieTooltips();
    },

    toggleDropDownRow: function(e) {
        var dropDownSubView = this.nextGroupView;
        if (dropDownSubView.isOpen()) {
            dropDownSubView.hide();
        } else {
            var loader = $(e.currentTarget).parent().find('.loader-icon');
            loader.show();
            this.model.fetch();
            dropDownSubView.show();
        }
    }

});

Shop.Views.ProductsList = GroupListView.extend({
    pagerTemplate: '#store-admin-table-load-more-template',
    pagerLoadingTemplate: '#store-admin-table-loader-template',
    emptyListTemplate:'#store-admin-table-empty-template',
    pagerPosition:'inside',
    tagName: 'tbody',
    itemViewTypes: [Shop.Views.ProductsListItem, Shop.Views.templateCollapseRow]
});

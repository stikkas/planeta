/*global Blog, Search, StringUtils, ProfileInfoHover*/
/**
 * Namespaces and common functions
 */
var Search = {
    Models: {},
    Views: {},
    INPUT_DELAY: 500,
    defaults: {
        blogs: {
            title: 'Поиск блогов',
            limit: 6,
            subsectionName: 'blogs'
        },
        projects: {
            title: 'Поиск проектов',
            limit: 12,
            subsectionName: 'projects'
        },
        charity: {
            title: 'Благотворительность',
            limit: 9,
            subsectionName: 'charity'
        }
    }
};

/**
 * Page model
 */
Search.Models.Search = BaseModel.extend({
    defaults: {
        profileModel: null,
        subsection: '',
        objectId: null,
        fetching: false,
        countryId: 0,
        regionId: 0,
        cityId: 0
    },
    initialize: function (options) {
        BaseModel.prototype.initialize.call(this, options);
        var subsection = this.get('subsection');
        var objectId = this.get('objectId');
        var url = this.getSearchUrl(subsection);
        this.query = new Search.Models.Query({subsection: subsection});

        this.query.setQuery(objectId, subsection);

        this.searchResults = new Search.Models.Results([], {
            url: url,
            query: this.query,
            model: BaseModel,
            limit: this.getResultsLimit(subsection)
        });
        this.set({
            fetching: false,
            searchResults: this.searchResults,
            title: this.getTitle()
        });
    },
    getResultsLimit: function (searchType) {
        if (Search.defaults[searchType]) {
            return Search.defaults[searchType].limit;
        }
        return 24;
    },
    /**
     * Get Controller url for specified search type
     * @param {String} searchType - 'users' or smth like this
     * @return {String}
     */
    getSearchUrl: function (searchType) {
        var url;

        if (searchType === 'blogs') {
            searchType = 'posts';
        }

        if (searchType === 'shares') {
            url = '/api/search/shares';
        } else {
            url = '/search-' + searchType + '.json';
        }


        return url;
    },
    /**
     * For async call after model was fetched
     * @see AppView.changePageData()
     * @return {Object}
     */
    pageData: function () {
        var data = {};
        data.title = this.getTitle() + ' | Planeta';
        data.description = data.title;
        return data;
    },

    getTitle: function () {
        var title;
        var searchType = this.get('subsection');
        if (Search.defaults[searchType]) {
            title = Search.defaults[searchType].title;
        }
        title = title || "Поиск";
        return title;
    },

    fetch: function (options) {
        this.searchResults.estimatedCount = undefined;
        var self = this;
        this.set('fetching', true);
        this.searchResults.load({
            success: function () {
                if (options && options.success) {
                    options.success();
                }
                self.trigger('queryResultsReset');
                self.fetchPurchasedCampaignId(self);
            },
            complete: function () {
                self.set('fetching', false);
                if (options && options.complete) {
                    options.complete();
                }
            }
        });
    },

    fetchPurchasedCampaignId: function (model) {
        if (!workspace.isAuthorized) {
            return;
        }
        Backbone.sync('read', null, {
            url: "/api/campaign/campaign-purchased-campaign-ids.json",
            data: {}
        }).done(function (response) {
            if (response && response.success && response.result) {
                _.each(response.result, function (id) {
                    var m = model.searchResults.findByAttr('campaignId', id);
                    if (m) {
                        m.set('isPurchased', true);
                    }
                });
            }
        });

    },

    changeQuery: function (query) {
        this.set({
            objectId: query
        }, {
            silent: true
        });
        this.query.setQuery(query, this.get('subsection'));
        this.fetch();
    },
    changeCategory: function (category) {
        this.query.set({categories: category});
        this.query.navigate(false);
        this.searchResults.reset();
        this.fetch();
    },

    changeLocation: function (countryId, regionId, cityId) {
        this.query.set({
            countryId: countryId,
            regionId: regionId,
            cityId: cityId
        });
        this.query.navigate(false);
        this.searchResults.reset();
        this.fetch();
    },

    changeQueryString: function (queryString) {
        this.query.set({query: queryString});
        this.query.navigate(false);
        this.searchResults.reset();
        this.fetch();
    },

    toJSON: function () {
        var json = BaseModel.prototype.toJSON.call(this);
        json.queryModel = this.query.toJSON();
        return json;
    }
});

/**
 * Search query model
 */
Search.Models.Query = BaseModel.extend({
    getJSON: function () {
        var a = this.toJSON();
        delete a.countries;
        delete a.subsection;
        delete a.searchType;
        delete a.getval;
        if (!a.cityId) {
            delete a.cityName;
        }
        var b = {};
        _.each(a, function (value, key) {
            if (value != null && value != undefined && value != '') {
                b[key] = value;
            }
        });
        if (b.query === undefined) {
            b.query = '';
        }
        return b;
    },
    getParam: function () {
        // since Backbone.Router does decodeURIcomponent on hashtag before we may parse it, replace '%'s by '.'s
        var json = this.getJSON();
        var query = json.query;
        delete json.query;
        var paramQuery = $.param({query: query});
        var paramOpts = $.param(json);
        var param = paramOpts.length > 0 ? paramQuery + '&' + paramOpts : paramQuery;
        return StringUtils.hashEscape(param);
    },
    parseQuery: function (query) {
        var paramList = {};
        var params = [];

        if (query) {
            var pos = query.indexOf('?');
            if (pos >= 0) {
                query = query.substr(0, pos);
            }

            params = query.split('&');
        }

        params = _.map(params, function (param) {
            return param.split('=');
        });
        _.map(params, function (param) {
            var value = param[1];
            if (param[0] == 'sortby') {
                param[0] = 'sortBy';
            }
            if (param.length > 1) {
                if (param[0] != 'query') {
                    value = param[1].toUpperCase();
                }
                value = StringUtils.hashUnescape(value);
            }
            if (/[A-Za-z0-9_\-]+/.test(param[0])) {
                paramList[param[0]] = value;
            }
        });
        return paramList;
    },
    setQuery: function (query, subsection) {
        var self = this;
        this.clear();
        this.set({
            subsection: subsection,
            getval: function (option) {
                return self.get(option) || false;
            }
        }, {
            silent: true
        });
        this.set(this.parseQuery(query));
    },
    getSearchUrl: function () {
        var subsection = this.get('subsection');
        if (subsection === 'products') {
            return (window.location.host === workspace.serviceUrls.shopAppUrl ? '' : 'https://' + workspace.serviceUrls.shopAppUrl) + '/' + subsection + '/search.html?' + StringUtils.hashUnescape(this.getParam());
        } else if (subsection === 'charity') {
            return (window.location.host === workspace.serviceUrls.charityUrl ? '' : 'https://' + workspace.serviceUrls.charityUrl) + '/?' + this.getParam();
        }
        return (window.location.host === workspace.serviceUrls.mainHost ? '' : 'https://' + workspace.serviceUrls.mainHost) + '/search/' + subsection + '?' + this.getParam();
    },
    navigate: function (reload) {
        if (this.get('subsection') === 'charity') {
            reload = false;
        }

        if (reload === false) {
            workspace.changeUrl(this.getSearchUrl());
        } else {
            workspace.navigate(this.getSearchUrl());
        }

    }
});

Search.Models.FakeModel = BaseModel.extend({
    initialize: function (options) {
        BaseModel.prototype.initialize.call(this, options);
    }
});

/**
 * Search results model
 */
Search.Models.Results = BaseCollection.extend({
    offset: 0,
    limit: 20, // to fulfill video 6 columns
    searchTime: 0,
    estimatedCount: undefined,
    initialize: function (models, options) {
        BaseCollection.prototype.initialize.call(this, models, options);
        if (options) {
            this.query = options.query;
            this.model = options.model || this.model;
        }
    },
    parse: function (response) {
        if (response.error === undefined) {
            this.searchTime = response.searchTime;
            this.estimatedCount = response.estimatedCount;
            this.size = response.size;
            if (response.searchResultRecords == null || response.searchResultRecords.length == 0) {
                this.allLoaded = true;
                return [];
            }
            this.allLoaded = false;
            return response.searchResultRecords;
        }
        this.estimatedCount = 0;
        return [];
    },
    getData: function () {
        return this.query.getJSON();
    },
    load: function (options) {
        this.data = this.getData();
        return BaseCollection.prototype.load.call(this, options);
    },
    loadNext: function (options) {
        this.data = this.getData();
        return BaseCollection.prototype.loadNext.call(this, options);
    }
});

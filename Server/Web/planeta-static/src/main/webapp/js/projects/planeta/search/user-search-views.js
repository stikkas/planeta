var UserSearch = {
    Models: {},
    Views: {}
};


UserSearch.Views.ContentView = BaseView.extend({

    modelEvents: {
        'destroy': 'dispose'
    },

    construct: function () {

        this.addChild(new UserSearch.Views.Query({
            model: this.model
        }));

        this.addChild(new UserSearch.Views.Results({
            model: this.model
        }));
    }
});


UserSearch.Views.Query = Search.Views.Query.extend({
    className: 'n-search',
    template: '#search-user-block-template'
});

UserSearch.Views.Results = BaseView.extend({
    id: 'resultlist',
    tagName: 'div',
    className: 'wrap',
    template: '#search-user-results-container-template',
    listEl: null,
    construct: function () {
        this.listEl = this.addChildAtElement('.js-result-list', new UserSearch.Views.Users({
            id: 'search-results',
            className: 'n-search_list',
            collection: this.model.searchResults,
            itemViewType: UserSearch.Views.ResultItem,
            model: this.model
        }));
    }
});


UserSearch.Views.Users = DefaultScrollableListView.extend({
    emptyListTemplate: '#search-results-empty',
    pagerLoadingTemplate: '#search-scrollable-list-loader-template'
});

UserSearch.Views.ResultItem = Search.Views.ResultItem.extend({
    className: 'n-search_i',
    tagName: 'div',
    template: '#search-user-list-items',
    events: {
        'click .js-message': 'showMessageDialog',
        'click .js-subscribe': 'subscribe'
    },
    showMessageDialog: function () {
        if (Header.checkAuthorization()) {
            workspace.appModel.get('dialogsController').showSendMessageDialog(this.model.get('profile').profileId);
        }
    },

    subscribe: function () {
        if (!this.model.get('subscribed') && Header.checkAuthorization()) {
            var model = this.model;
            PrivacyUtils.subscribe(this.model.get('profile').profileId).done(function (result) {
                if (result.success) {
                    model.set('subscribed', true);
                }
            });
        }
    }
});
/*globals Modal, Products, ScrollableListView, moduleLoader*/
Products.Views = {};

Products.Views.ProductListItem = BaseView.extend({
    template: '#product-list-item-template',
    className: 'product-card_i',
    injectionHtml: 'projectCardHtml',
    injectionFunc: 'projectCard',

    events: {
        'click': 'onClick'
    },

    onAddToShoppingCart: function (view) {
        if (this.model.get('isMeta')) {
            this.onShowInfoClicked();
        } else {
            workspace.shoppingCart.addProduct(this.model.get('productId'), 1);
        }
    },

    onClick: function () {
        if (window.gtm) {
            window.gtm.trackClickProductCard(this.model);
        }
    }
});

Products.Views.ProductListView = DefaultScrollableListView.extend({
    tagName: 'div',
    className: 'product-card product-card__3',
    itemViewType: Products.Views.ProductListItem,
    pagerLoadingTemplate: '#search-scrollable-list-loader-template',
    emptyListTemplate: '#nothing-found-template',
    pagerAllLoadedTemplate: null,
    isFilter: null,
    isFilterFixedInit: false,

    construct: function (options) {
        this.$el.addClass(options.customClasses);
        this.gtmProductListTitle = options.gtmProductListTitle;
    },


    renderPager: function () {
        BaseListView.prototype.renderPager.call(this);
        this.stickyStuffs();
    },

    stickyStuffs: function () {
        var shopFilter = $('.shop-filter');
        this.isFilter = this.isFilter || !!shopFilter.length;
        if ( this.isFilter && !window.isMobileDev) {
            if (!this.isFilterFixedInit) {
                shopFilter.stick_in_parent({
                    parent: $('.shop-list-wrap')
                });
                this.isFilterFixedInit = true;
            } else {
                requestAnimationFrame(function () {
                    shopFilter.trigger('sticky_kit:recalc');
                });
            }
        }
    },

    prepareScrollDown: function () {
        var self = this;

        $(window).bind("scroll.eventView" + this.cid, function () {
            var scrollY = window.pageYOffset;
            var windowHeight = window.innerHeight;
            var listHeight = self.$el.height();

            if (windowHeight + scrollY >= listHeight && !self.collection.allLoaded) {
                self.onScrollDown();
            }
        });
    },
    onAdd: function (product) {
        var self = this;

        if (window.gtm) {
            if (!this.productList) {
                this.productList = [];
                this.startIndex = this._itemViews.length;
                setTimeout(function () {
                    window.gtm.trackViewProductList(self.productList, self.gtmProductListTitle, self.startIndex);
                    delete self.productList;
                }, 10);
            }
            this.productList.push(product);
        }
        return ScrollableListView.prototype.onAdd.apply(this, arguments);
    },

    onFilterChanged: function (filter) {
        this.gtmProductListTitle = 'Категория - ' + (ShopUtils.getProductTag(filter.get('mainCategoryMnemonic')) ? ShopUtils.getProductTagByName(filter.get('mainCategoryMnemonic')).value : 'Все товары');
    }
});

Products.Views.HorizontalProductListView = BaseListView.extend({
    itemViewType: Products.Views.ProductListItem,

    afterRender: function () {
        BaseListView.prototype.afterRender.call(this);
        if (this.moreThenOne) {
            return;
        }
        this.moreThenOne = true;
        $(window).trigger("scroll");
        this.trackViewProductList();
    },
    trackViewProductList: function (currentPage) {
        currentPage = currentPage || 0;
        if (window.gtm) {
            window.gtm.trackViewProductList(this.collection.toArray().slice(currentPage * 4, currentPage * 4 + 4), 'Только что поддержали', currentPage * 4);
        }
    }
});

Products.Views.ReferrerItem = BaseView.extend({
    template: '#product-search-referrer-item-template'
});

Products.Views.ReferrerList = BaseListView.extend({
    itemViewType: Products.Views.ReferrerItem
});

Products.Views.ReferrersView = BaseView.extend({
    template: '#product-search-referrers-view-template',

    events: {
        'click .js-shop-filter_multi-i': 'onReferrerItemClick',
        'change #referrerNameInput': 'autocomplete',
        'keyup #referrerNameInput': 'debounceAutocomplete'

    },

    construct: function() {
        var referrerIds = this.model.get('referrerIds'),
            referrers = this.model.get('referrers').models;

        if(referrerIds && referrerIds.length > 0) {
            referrerIds.forEach(function( id) {
                referrers.forEach(function(referrer) {
                    if(id == referrer.get('profileId')) {
                        referrer.set('isSelected', true);
                    }
                })
            });
        }

        this.addChildAtElement('.js-referrers-list', new Products.Views.ReferrerList({
            collection: this.model.get('referrers'),
            mainCategoryMnemonic: this.model.get('mainCategoryMnemonic')
        }));
    },

    onReferrerItemClick: function(e) {
        var referrerId = e.target.id;
        var referrerIds = this.model.get('filter').get('referrerIds');

        if(referrerIds && referrerIds.length > 0) {
            var counter = 0,
                index = -1;
            referrerIds.forEach(function(id) {
                if(id == referrerId) {
                    index = counter;
                }
                counter++;
            });

            if(index != -1) {
                referrerIds.splice(index, 1);
            } else {
                referrerIds.push(referrerId);
            }
        } else {
            referrerIds = [];
            referrerIds.push(referrerId);
        }

        this.model.get('filter').set('referrerIds', referrerIds);
        this.model.filterChanged();
    },

    autocomplete: function(e) {
        this.referrerAutocomplete(e.target.value);
    },

    debounceAutocomplete: _.debounce(function(e) {
        this.referrerAutocomplete(e.target.value);
    }, 700),

    referrerAutocomplete: function(query) {
        var referrers = this.model.get('referrers').models,
            regexp = new RegExp(query, "i"),
            fields = $('.shop-filter_multi-i');

        var validReferrers = _.filter(referrers, function(referrer) {
            return regexp.test(referrer.get('displayName'));
        });

        $.each(fields, function(i, field) {
            var referrer = null;
            referrer = _.find(validReferrers, function(e) {
                return e.get('profileId') == $(field).attr('data-referrer-id');
            });

            if(referrer) {
                $(field).removeClass('hidden')
            } else {
                $(field).addClass('hidden')
            }
        });
    }
});

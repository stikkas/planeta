/*global Campaign, StringUtils, StorageUtils, FileUploader*/
var CampaignEdit = {
    Models: {},
    Views: {}
};
CampaignEdit.Models = {
    createCampaign: function (ownerProfileId) {
        var model = new CampaignEdit.Models.Tab({
            ownerProfileId: ownerProfileId,
            objectId: 0
        });
        model.saveCampaign().done(function (response) {
            if(response.result) {
                workspace.onClickNavigate(null, '/campaigns/' + response.result.campaignId + '/edit');
            } else {
                workspace.appView.showDebugMessage(response.errorMessage);
            }
        });
    }
};

CampaignEdit.Models.DraftCampaign = Campaign.Models.BaseCampaign.extend({
    url: '/admin/draft-campaign.json',
    defaults: _.extend({}, Campaign.Models.BaseCampaign.prototype.defaults, {
        errors: {},
        faqCount: 0,
        section: 'ALL',
        shares: []
    }),
    initialize: function () {
        Campaign.Models.BaseCampaign.prototype.initialize.apply(this, arguments);
        if (this.get('profileModel')) {
            this.set('group', this.get('profileModel').attributes);
        }
        this.initShares();
        this.editorModel = new CampaignEdit.Models.EditorModel(this);
        if (this.get('campaignId')) {
            workspace.navigationState.bind("change", this.onNavigationStateChanged, this);
        }
        if (CampaignEdit.uploaderController) {
            CampaignEdit.uploaderController.setDraftModel(this);
        } else {
            CampaignEdit.uploaderController = new CampaignEdit.Models.UploaderController({draftModel: this});
        }
    },

    onNavigationStateChanged: function (navigationState) {
        workspace.navigationState.unbind("change", this.onNavigationStateChanged, this);
        var subsection = navigationState.get('subsection');
        if (navigationState.get('section') === 'campaigns' && subsection && subsection.indexOf('edit') === 0) {
            CampaignEdit.SavedDraftCampaign = this.getDataToSave(["webCampaignAlias", "collectedAmount", "purchaseCount", "commentsCount", "errors", "draftTimeUpdated", "draftSaveError", "edited"]);
        } else {
            delete CampaignEdit.SavedDraftCampaign;
            CampaignEdit.uploaderController.clearUploaderList();
        }
    },

    getDataToSave: function (fields) {
        if (this.editorModel) {
            this.editorModel.save();
        }
        //todo уточнить нужные поля
        var data = _.extend(_.pick(this.attributes, "tags", "profileId", "campaignId", "draftCampaignId", "videoContent", "name", "description", "descriptionHtml", "shortDescription", "status", "statusCode", "authorProfileId", "videoProfileId", "objectType", "imageId", "timeAdded", "timeFinish", "nameHtml", "videoId", "albumId", "timeStart", "videoUrl", "campaignAlias", "creatorProfileId", "viewImageUrl", "ignoreTargetAmount", "targetAmount", "finishOnTargetReach", "descriptionHtml", "targetStatus", "shortDescriptionHtml", "viewImageId", "targetStatusCode", "notificationMethodCode", "imageUrl", "campaignOffer", "planetaDelivery", "countryId", "cityId", "regionId", "cityNameRus", "cityNameEng", "regionNameRus", "regionNameEng"), {
            shares: this.shares.models
        });
        if (fields) {
            _.extend(data, _.pick(this.attributes, fields));
        }
        if (!$.trim(data.campaignAlias)) {
            data.campaignAlias = null;
        }
        data.timeUpdated = this.get('timeUpdated');
        return data;
    },

    saveCampaign: function (saveOnly, footer, url) {
        return this._save(url ? url : this.url, false, saveOnly, footer);
    },

    saveAndValidateCampaign: function (saveOnly, footer) {
        return this._save('/admin/campaign-save.json', false, saveOnly, footer);
    },
    _save: function(url, force, saveOnly, footer) {
        var self = this,
            lUrl = force ? url + '?force=true' : url;

        var data = _.extend(self.getDataToSave(), $('.js-main-campaign-form').serializeObject());

        return Backbone.sync("update", null, {
            url: lUrl,
            contentType: 'application/json',
            data: data,
            confirm: function(result) {
                Modal.showConfirm(result.errorMessage, "Внимание!", {
                    success: function () {
                        self._save(url, true, saveOnly, footer);
                    },
                    cancel: function() {
                        if (footer)
                            footer._fail();
                    }
                }, "Сохранить мои правки", "Отменить мои правки");
            },
            success: function(response) {
                if (response.result) {
                    self.set('shares', response.result.shares, {silent: true});
                    self.initShares();
                    self.set('errors', {});
                    if(response.result.timeUpdated)
                        self.set('timeUpdated', response.result.timeUpdated, {silent: true}); 
                }
                if (response.success) {
                    if (self.contractor && !saveOnly) {
                        document.location.href = document.location.href.replace('/edit-contractor', '');
                    } else if (footer && footer._saved)
                        footer._saved(response.result.edited);
                }
            }
        });

    },
    initModel: function () {
        this.initShares();
        this.set({
            isDraft: this.get('status') === 'DRAFT' || this.get('status') === 'NOT_STARTED' || this.get('purchaseCount') === 0,
            notFirstChangeTimeFinish: false,
            notFirstChangeTargetAmount: false
        }, {silent: true});
    },
    /**
     * reloads other dependent components on success
     * @param options
     * @returns {*}
     */
    fetch: function (options) {
        options = options || {};
        var self = this, $dfd = $.Deferred();
        var success = options.success || function () { };
        var campaignId = self.get('campaignId');
        if (CampaignEdit.SavedDraftCampaign && CampaignEdit.SavedDraftCampaign.campaignId === campaignId) {
            self.set(CampaignEdit.SavedDraftCampaign);
            delete CampaignEdit.SavedDraftCampaign;
            self.initModel();
            success();
            $dfd.resolveWith(self, [self]);
        } else {
            Campaign.Models.BaseCampaign.prototype.fetch.call(self, {
                data: {
                    profileId: self.get('profileId'),
                    campaignId: self.get('campaignId') || 0
                },
                success: function (model, resp) {
                    resp = resp || model;
                    if (resp.success === false) {
                        $dfd.fail(success).rejectWith(self, [
                            {success: false}
                        ]);
                        return;
                    }
                    self.initModel();
                    success();
                    $dfd.resolveWith(self, [model]);
//                success(model);
                },
                error: function () {
                    $dfd.reject();
                }
            });
        }
        return $dfd.promise();
    },

    toJSON: function () {
        var json = Campaign.Models.BaseCampaign.prototype.toJSON.call(this);
        json.shares = this.shares.toJSON();
        return json;
    },

    initShares: function () {
        if (this.shares instanceof CampaignEdit.Models.ShareCollection) {
            this.shares.reset(this.get('shares') || [], {silent: true});
        } else {
            this.shares = new CampaignEdit.Models.ShareCollection(this.get('shares') || []);
        }
        var campaign = this;
        this.shares.each(function (shareModel) {
            shareModel.set({
                isCharity: !!_.where(campaign.get('tags'), {mnemonicName: 'CHARITY'}, true)
            });
        });
    },

    /**
     * @override
     */
    parse: function (response) {
        return (response.success && response.result) || {};
    }
});

CampaignEdit.Models.UploaderController = BaseModel.extend({
    uploaderList: {},
    setDraftModel: function (draftModel) {
        this.set('draftModel', draftModel);
    },

    getFileUploader: function (imageFieldModel, uploaderOptions, modelToUpdate) {
        var self = this;
        var uploader = this.uploaderList[imageFieldModel.uploaderName];
        if (uploader && uploader.isUploading) {
            uploader.fileUploader.removeAllListeners();
            uploader.imageFieldModel.off('change:imageUrl', uploader.onImageUrlChange, this);

            if (uploaderOptions.listener) {
                _.defer(function () {
                    uploader.fileUploader.addListener(uploaderOptions.listener);
                });
            }
        } else {
            uploader = {
                fileUploader: new FileUploader(uploaderOptions)
            };
            uploader.fileUploader.addListener({
                onAdd: function (file) {
                    uploader.isUploading = true;
                    uploader.file = file;
                }
            });
            this.uploaderList[imageFieldModel.uploaderName] = uploader;
            uploader.modelToUpdateList = [];
        }

        if (modelToUpdate) {
            uploader.modelToUpdateList.push(modelToUpdate);
        }
        uploader.imageFieldModel = imageFieldModel;
        uploader.onImageUrlChange = function () {
            imageFieldModel.off('change:imageUrl', uploader.onImageUrlChange, self);
            delete self.uploaderList[imageFieldModel.uploaderName];
            uploader.file = null;
            uploader.isUploading = false;
            if (_.isEmpty(uploader.modelToUpdateList)) {
                imageFieldModel.onImageUrlChange(self.get('draftModel'));
            } else {
                _.forEach(uploader.modelToUpdateList, function (updmodel) {
                    imageFieldModel.onImageUrlChange(updmodel);
                });
            }

        };
        imageFieldModel.on('change:imageUrl', uploader.onImageUrlChange, this);

        uploader.url = window.location.pathname;

        return uploader.fileUploader;
    },

    addModelToUpdate: function (uploaderName, modelToUpdate) {
        if (uploaderName) {
            modelToUpdate.uploaderName = uploaderName;
            var uploader = this.uploaderList[uploaderName];
            if (uploader) {
                uploader.modelToUpdateList.push(modelToUpdate);
            }
        }
    },

    removeModelToUpdate: function (uploaderName, modelToUpdate) {
        if (uploaderName) {
            var self = this;
            //первый обработчик CrowdFund.Views.ImageField.onImageUrlChange который вызывает removeModelToUpdate
            //затем вызывается uploader.onImageUrlChange который, не найдя modelToUpdate, изменяет draftModel
            //поэтому задерживаем удаление modelToUpdate чтобы правильно отработал uploader.onImageUrlChange
            _.defer(function () {
                var uploader = self.uploaderList[uploaderName];
                if (uploader) {
                    uploader.modelToUpdateList = _.without(uploader.modelToUpdateList, modelToUpdate);
                    if (_.isEmpty(uploader.modelToUpdateList)) {
                        self.removeFileUploader(uploaderName);
                    }
                }
            });
        }
    },


    removeFileUploaderIfNotUploading: function (uploaderName) {
        this.removeFileUploader(uploaderName, true);
    },

    removeFileUploader: function (uploaderName, checkUploading) {
        var uploader = this.uploaderList[uploaderName];
        if (uploader && !(checkUploading && uploader.isUploading)) {
            uploader.fileUploader.removeAllListeners();
            uploader.imageFieldModel.off('change:imageUrl', uploader.onImageUrlChange, this);
            delete this.uploaderList[uploaderName];
        }
    },

    clearUploaderList: function () {
        _.each(this.uploaderList, function (uploader) {
            var fileUploader = uploader.fileUploader;
            if (fileUploader) {
                fileUploader.removeAllListeners();
                fileUploader.cancel(uploader.file);
            }
        });
        this.uploaderList = {};
    },

    ifExistsUploadingFiles: function () {
        return !_.chain(this.uploaderList).pluck('isUploading').compact().isEmpty().value();
    },

    alertIfExistsUploadingFiles: function (options) {
        if (this.ifExistsUploadingFiles()) {
            Modal.showConfirmEx(_.extend({
                text: 'В данный момент происходит загрузка аудио-, фото- или видеоматериалов. Дождитесь ее окончания и сохраните проект. Вы также можете сохранить его прямо сейчас - в этом случае загружаемые файлы будут потеряны.',
                title: 'Подтверждение действия',
                okButtonText: 'Сохранить без файлов',
                cancelButtonText: 'Дождаться загрузки',
                cancel: function () {
                    CampaignEdit.uploaderController.gotoAnyPageWithUploadingFile();
                }
            }, options));
        } else {
            if (options && options.success) {
                options.success();
            }
        }
    },

    gotoAnyPageWithUploadingFile: function () {
        var uploaderName = _.first(_.keys(this.uploaderList));
        var uploader = this.uploaderList[uploaderName];
        if (uploader) {
            workspace.navigate(uploader.url);
        }
    }

});


CampaignEdit.Models.Tab = CampaignEdit.Models.DraftCampaign.extend({
    defaults: _.extend({}, CampaignEdit.Models.DraftCampaign.prototype.defaults, {
        tabs: {
            edit: {
                name: 'Основные данные',
                fields: ['name', 'campaignAlias', 'shortDescription', 'imageUrl', 'tags', 'targetAmount', 'timeFinish']
            },
            'edit-details': {
                name: 'Детали',
                fields: ['videoContent', 'descriptionHtml']
            },
            'edit-shares': {
                name: 'Вознаграждения',
                fields: ['shares']
            },
            'edit-contractor': {
                name: 'Контрагент',
                fields: []
            }
        }
    }),

    initialize: function (options) {
        this.set({
            profileId: options.ownerProfileId || this.get('profileModel').get('profileId'),
            campaignId: this.get('objectId'),
            creatorProfile: this.get('profileModel')
        }, {silent: true});
        CampaignEdit.Models.DraftCampaign.prototype.initialize.call(this, options);
        this.set({
            activeTab: this._getTab(0),
            nextTab: this._getTab(+1),
            prevTab: this._getTab(-1)
        });
    },

    /**
     * get tab name for next (+1), previous (-1) (etc.) tab
     * @param {int} shift
     * @private
     */
    _getTab: function (shift) {
        var subsection = workspace.navigationState.get('subsection');
        var keys = _.keys(this.get('tabs'));
        var index = _.indexOf(keys, subsection);
        if (index === -1) {
            return null;
        }
        var shiftedIndex = index + shift;
        if (shiftedIndex >= 0 && shiftedIndex < keys.length) {
            return keys[shiftedIndex];
        }
        return null;
    },

    /**
     * @override
     * @returns {*}
     */
    saveAndValidateCampaign: function (saveOnly, footer) {
        var self = this;
        var $dfd = CampaignEdit.Models.DraftCampaign.prototype.saveAndValidateCampaign.apply(this, [saveOnly, footer]);
        $dfd.done(function (response) {
            if (response.success && self.contractor) {
                if (saveOnly) {
                    if (footer && footer._saved) {
                        footer._saved(response.result.edited);
                    }
                } else {
                    if ('DRAFT' === self.get('status')) {
                        document.location.href = document.location.href.replace('/edit-contractor', '/created-successful');
                    }
                }
            } else {
                self.navigateToValidation(response.errorMessage, response.fieldErrors, response.errorClass);
            }
        });
        return $dfd;
    },

    navigateToValidation: function (globalError, errors, errorClass) {
        if (errors == null) {
            if (globalError && !errorClass) {
                workspace.appView.showErrorMessage(globalError);
            }
            return;
        }
        var self = this;
        // get first tab with errors
        $.each(this.get('tabs'), function (name, tab) {
            var hasError = _.any(tab.fields, function (field) {
                return errors[field];
            });
            if (hasError) {
                // save validation errors and redirect to tab with errors
                self.set('errors', errors);
                workspace.onClickNavigate(null, '/campaigns/' + self.get('campaignId') + '/' + name);
                // break $.each
                return false;
            } 
        });
    },

    onLocationChange: function () {
        return;// do nothing
    }
});
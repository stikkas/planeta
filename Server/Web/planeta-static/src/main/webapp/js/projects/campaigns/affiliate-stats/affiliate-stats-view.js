/*globals CampaignAffiliateStats, Orders, ModalConfirmView, Form, Campaign, google, DefaultListView, TemplateManager*/

CampaignAffiliateStats.Views = {};

CampaignAffiliateStats.Views.Graph = BaseView.extend({
    tagName: 'div',
    template: '#affiliate-stats-graph-template',
    events: {
        'change .js-first-graph-select' : 'onFirstGraphSelectChange',
        'change .js-second-graph-select' : 'onSecondGraphSelectChange'
    },

    afterRender: function() {
        var selects = $('select', this.el);
        selects.dropDownSelect();
        if (this.model.getFilter().get('aggregationType') == "DATE") {
            if (!this.model.get('graphInited')) {
                this.initGraph();
            } else {
                this.drawChart();
            }
        }
        //double check for aggregation type
        if (this.model.getFilter().get('aggregationType') != "DATE") {
            $(this.el).find('.raw').hide();
            $(this.el).find('.affiliate-statistic-chart').hide();
        }
    },

    initGraph: function () {
        window.baseChartGraphGlobal = this;
        var script = document.createElement("script");
        script.src = "https://www.google.com/jsapi?callback=window.baseChartGraphGlobal.loadVisualization";
        script.type = "text/javascript";
        document.getElementsByTagName("head")[0].appendChild(script);
    },

    loadVisualization: function () {
        var self = this;
        google.load("visualization", "1", {packages: ["corechart"], "callback": function () {
            window.baseChartGraphGlobal.isInit = true;
            self.drawChart();
        }});
    },

    onFirstGraphSelectChange: function(e) {
        var self = this;
        var $select = $(e.currentTarget);
        var target = $select.val();
        var name = $select.find('option[selected]').text();
        this.model.set({
            firstGraph: {
                name : name,
                target: target,
                values: this.model.get('initParams')[target]
            }
        });
        setTimeout(function () {
            var chart = new google.visualization.AreaChart(document.getElementById('affiliate-statistic-chart'));
            chart.draw(self.buildData(), self.model.get('graphOptions'));
        }, 100);
    },

    onSecondGraphSelectChange: function(e) {
        var self = this;
        var $select = $(e.currentTarget);
        var target = $select.val();
        var name = $select.find('option[selected]').text();
        this.model.set({
            secondGraph: {
                name : name,
                target: target,
                values: this.model.get('initParams')[target]
            }
        });
        setTimeout(function () {
            var chart = new google.visualization.AreaChart(document.getElementById('affiliate-statistic-chart'));
            chart.draw(self.buildData(), self.model.get('graphOptions'));
        }, 100);
    },


    drawChart: function () {
        var chart = new google.visualization.AreaChart(document.getElementById('affiliate-statistic-chart'));
        chart.draw(this.buildData(), this.model.get('graphOptions'));
    },

    buildData: function () {
        var rows = [];
        var date = this.model.get('initParams').date;
        var first = this.model.get('firstGraph');
        var second = this.model.get('secondGraph');
        if (date.length < 2) {
            return;
        }
        for (var i = 0; i < date.length; ++i) {
            rows.push([date[i], first.values[i], second.values[i]]);
        }
        var data = new google.visualization.DataTable();
        data.addColumn('date', 'Дата');
        data.addColumn('number', first.name);
        data.addColumn('number', second.name);
        data.addRows(rows);
        return data;
    }
});

CampaignAffiliateStats.Views.Filter = Orders.Views.BaseFilter.extend({
    template: '#campaign-affiliate-stats-filter-template',
    events: _.extend({}, Orders.Views.BaseFilter.prototype.events, {
        'click .js-filter-link-item': 'onAggregationTypeClicked'
    }),

    onAggregationTypeClicked: function (e) {
        var $e = $(e.currentTarget);
        if ($e.hasClass('active')) {
            return;
        }
        this.model.set({aggregationType: $e.attr('data-type'), loading: true});
        this.model.trigger('filterChanged');
    },

    afterRender: function() {
        Orders.Views.BaseFilter.prototype.afterRender.call(this);
        var timeFilter = this.model.get('timeFilter');
        var $activeEl = $('#periods .active');
        if (timeFilter != $activeEl.children().attr('id')) {
            $activeEl.removeClass('active');
            $(this.el).find('#' + timeFilter).parent().addClass('active');
        }
    }
});


CampaignAffiliateStats.Views.TotalStats = BaseView.extend({
    tagName: 'tr',
    template: '#campaign-stats-total',
    className: 'js-total-stats-row',

    construct: function(options) {
        var self = this;

        options.collection.on('reset', function (e) {
            var aggregationType = e.currentFilter.get('aggregationType');
            self.model.set('aggregationType', aggregationType);
            self.model.fetch().done(function(response) {
                if(response) {
                    $('.js-total-stats-row').show();
                } else {
                    $('.js-total-stats-row').hide();
                }
            });
        });

        options.collection.on('add', function (e) {
            self.model.fetch();
        });
    }
});

CampaignAffiliateStats.Views.Order = BaseView.extend({
    tagName: 'tr',
    template: '#campaign-stats-general'
});

CampaignAffiliateStats.Views.ListView = DefaultListView.extend({
    itemViewType: CampaignAffiliateStats.Views.Order,
    tagName: 'tbody',
    className: 'js-table-tbody',
    addPagerElement: function (pager) {
        if (this.pagerElement) {
            this.pagerElement.remove();
            $('.list-loader').remove();
        }
        this.pagerElement = pager;

        if (!this.pagerElement) {
            return;
        }
        if (this.sortDirection != 'asc') {
            if (this.pagerPosition == 'outside') {
                $('table').before(this.pagerElement);
            } else {
                $('table').prepend(this.pagerElement);
            }
        } else {
            if (this.pagerPosition == 'outside') {
                $('table').after(this.pagerElement);
            } else {
                $('table').append(this.pagerElement);
            }
        }
    },

    renderPagerElement: function () {
        if (this.pagerElement) {
            $('.list-loader').remove();
            this.pagerElement.remove();
        }
        if (this.pagerTemplate && !this.collection.allLoaded) {
            var self = this;
            $.when(TemplateManager.tmpl(this.pagerTemplate, this.modelJSON(), this)).done(function (pager) {
                pager.click(function (e) {
                    self.onPagerClicked(e);
                });
                self.addPagerElement(pager);
            });
        }
    }
});

CampaignAffiliateStats.Views.OrdersLoader = BaseView.extend({
    tagName: 'tbody',
    template: '#campaign-stats-order-list-template'
});
CampaignAffiliateStats.Views.Controller = BaseView.extend({
    template: '#campaign-affiliate-stats-template',

    construct: function() {
        this.addChildAtElement('.search-block', new CampaignAffiliateStats.Views.Filter({
            model: this.model.filter
        }));

        var header = this.addChildAtElement('.project-stats-table', new CampaignAffiliateStats.Views.ListViewHeaderAffiliate({
            model: this.model.orders.listHeader
        }));
        this.addChildAtElement('.js-graph-block', new CampaignAffiliateStats.Views.Graph({
            model: this.model.graph
        }));

        this.addChildAtElement('.project-stats-table', new CampaignAffiliateStats.Views.OrdersLoader({
            model: this.model.ordersLoader
        }));

        this.addChildAtElement('.project-stats-table', new CampaignAffiliateStats.Views.ListView({
            collection: this.model.orders
        }));

        this.addChildAtElement('.js-table-tbody' , new CampaignAffiliateStats.Views.TotalStats({
            collection: this.model.orders,
            model: this.model.totalStats
        }), false, 'prepend');

        //TODO: check if we really need 'true' flag
        this.addChildAtElement('.top-downloader-placeholder', new CampaignOrders.Views.BaseReportDownloader({
            model: this.model.downloader
        }), true);

        //TODO: check if we really need 'true' flag
        this.addChildAtElement('.bottom-downloader-placeholder', new CampaignOrders.Views.BaseReportDownloader({
            model: this.model.downloader,
            additionalClass: 'pull-right'
        }), true);
        this.model.filter.bind('change', header.render, this);
    }
});

CampaignAffiliateStats.Views.ListViewHeaderAffiliate = BaseView.extend({
	tagName: 'thead',
	template: '#affiliate-stats-list-header-template'
});

CampaignAffiliateStats.Views.Page = Campaign.Views.BasePage.extend({
    contentViewClass: CampaignAffiliateStats.Views.Controller
});

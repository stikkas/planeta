/**
 * Popup with detail description of the printing
 */
Biblio.Views.BookDetails = BaseView.extend({
    template: "#book-view-template",
    className: "book-view",
    events: {
        'click .btn-primary': 'goNextStep',
        'click .btn-biblio': 'choose',
        'click .book-view_count_plus': 'increase',
        'click .book-view_count_minus': 'decrease'
    },
    chooseUrl: '/books',
    construct: function(options) {
        if (options.options) {
            var opts = options.options;
            if (opts.nextUrl) {
                this.chooseUrl = opts.nextUrl;
            }
            if (opts.nextText) {
                this.nextText = opts.nextText;
            }
        }
    },
    afterRender: function () {
        var self = this;
        self.$sum = self.$('.book-view_cost > span:first');
        self.$cnt = self.$('.book-view_count_val > input');
        self.$cnt.on('change', function (e, val) {
            val = val || self.getValue();
            self.$cnt.val(val);
            self.changeSum();
        });
        self.$cnt.trigger('change', Math.max(1, Biblio.data.bin.countBook(self.model)));
        if (self.nextText)  {
            self.$('.btn-primary').text(self.nextText);
        }
    },
    getValue: function() {
        var value = parseInt(this.$cnt.val());
        if (isNaN(value) || value < 1)
            value = 1;
        return value;
    },
    changeSum: function () {
        this.$sum.text(StringUtils.humanNumber(
                this.model.get('price') * parseInt(this.$cnt.val())));
    },
    increase: function () {
        this.$cnt.trigger('change', parseInt(this.$cnt.val()) + 1);
    },
    decrease: function () {
        this.$cnt.trigger('change', Math.max(1, parseInt(this.$cnt.val()) - 1));
    },
    close: function () {
        workspace.changeUrl(Biblio.data.savedUrl || window.location.pathname);
        !this.parent.hidden && this.parent.close();
    },

    goFirstStep: function () {
        if (Biblio.data.stepOne) {
            this.close();
        } else {
            Biblio.Utils.changeUrl('/books');
        }
    },
    goNextStep: function () {
        Biblio.data.bin.changeBooks(this.model, this.getValue());
        Biblio.Utils.changeUrl('/library');
    },
    choose: function () {
        Biblio.data.bin.changeBooks(this.model, this.getValue());
        this.goFirstStep();
    }
});

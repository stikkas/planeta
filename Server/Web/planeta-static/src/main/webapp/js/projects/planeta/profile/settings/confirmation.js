var Confirmation = {
    /**
     * Creates new confirmation panel on js
     * @param {Object} errors
     * @return view with model to append
     */
    createFromNotificationSettingsError: function (errors) {
        var UUID = null;
        var textHtml = "На Ваш телефон был выслан код подтверждения";
        var placeholder = "Введите код из SMS";
        if (errors.notificationPhone) {
            UUID = errors.notificationPhone;
            textHtml = "На Ваш телефон был выслан код подтверждения";
            placeholder = "Введите код из SMS";

        } else if (errors.notificationEmail) {
            UUID = errors.notificationEmail;
            textHtml = "На Ваш e-mail был выслан код подтверждения";
            placeholder = "Введите код из письма";
        }

        var confirmationModel = new Confirmation.Model({
            confirmationId: UUID,
            confirmationTextHtml: textHtml,
            placeholder: placeholder
        });

        return new Confirmation.View({
            model: confirmationModel
        });
    }
};

Confirmation.Model = BaseModel.extend({
    confirmUrl: "/api/confirm.json",
    confirmResendUrl: "/api/confirm-resend.json",

    defaults: {
        placeholder: 'Введите код из письма'
    },

    initialize: function (options) {
        this.set(options);
    },

    resendSecureCode: function (successCallback) {
        var self = this;
        Backbone.sync('read', this, {
            url: this.confirmResendUrl + "?confirmationId=" + this.get('confirmationId'),
            success: function (response) {
                self.set('confirmationId', response.result);
                successCallback();
            }
        });
    },

    confirmSecureCode: function (secureCode, successCallback) {
        var self = this;
        Backbone.sync("update", this, {
            url: this.confirmUrl,
            data: {
                confirmationId: this.get('confirmationId'),
                code: secureCode
            },
            success: function (status) {
                if (status === "CONFIRMED") {
                    self.onConfirmSuccess();
                }
                successCallback(status);
            }
        });
    },
    /**
     * for override
     */
    onConfirmSuccess: function () {
        // no action
    }

});

Confirmation.View = BaseView.extend({
    template: "#confirmation-input-panel",
    events: {
        "click #confirmation-link": "submit",
        "click #confirmation-resend": "reSendCode"
    },
    submit: function (e) {
        e.preventDefault();
        var self = this;
        this.model.confirmSecureCode($("#confirmationSecureCode").val(), function (status) {
            if (status === "CONFIRMED") {
                workspace.appView.showSuccessMessage("Подтверждение прошло успешно");
                self.dispose();
            } else if (status === "WRONG_CODE") {
                workspace.appView.showErrorMessage("Неверный код подтверждения");
            } else if (status === "NOT_FOUND") {
                workspace.appView.showErrorMessage("Время подтверждения истекло");
            } else if (status === "ERROR") {
                workspace.appView.showErrorMessage("Внутренняя ошибка");
            }
        });

    },

    reSendCode: function (e) {
        e.preventDefault();
        this.model.resendSecureCode(function () {
            workspace.appView.showSuccessMessage("Код подтверждения выслан повторно");
        });
    }

});

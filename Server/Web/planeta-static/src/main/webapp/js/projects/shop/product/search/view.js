/*globals Products, ProductSearch*/

ProductSearch.Views = {};
ProductSearch.Views.SearchFilter = BaseView.extend({
	template:'#product-search-filter-template',
	events:{
		'keyup #searchString':'onSearchStringChanged',
		'input #searchString':'onSearchStringChanged',
		'paste #searchString':'onSearchStringChanged',
		'drop #searchString':'onSearchStringChanged',
		'click .js-category-tab-link':'onCategoryChanged',
		'click .pln-dropdown': 'toggleDropDown'
	},

    construct: function(options) {
    },

	toggleDropDown:function (e) {
		e.preventDefault();
		var ddContainer = this.$(e.target).closest('.pln-dropdown');
		ddContainer.toggleClass('open');
		ddContainer.find('.sbf-btn').toggleClass('active');
		ddContainer.clickOff(function(e){
			ddContainer.toggleClass('open');
			ddContainer.find('.sbf-btn').toggleClass('active');
		});
	},

	onSearchStringChanged:_.debounce(function (e) {
		var el = $(e.currentTarget);
		this.model.silentSet(el.attr('id'), el.val());
		this.changeUrl();
	}, 300),

    /*
    _switchCategory: function(categoryTypeId) {
        this.$('.js-category-tab-link.active').removeClass('active');
        var categoryName = this.$('.js-category-tab-link[data-id="' + categoryTypeId + '"]').addClass('active').text();
		if (!categoryName || categoryName.length <= 3){
			categoryName = "Категории"
		}
        this.$('.js-content-current-category').text(categoryName);
    },
    */
    changeCategory: function(mainCategoryMnemonic) {
        //this._switchCategory(categoryTypeId);
        this.model.silentSet("mainCategoryMnemonic", mainCategoryMnemonic);
		this.changeUrl();
    },
    onCategoryChanged:function (e) {
		e.preventDefault();
        this.changeCategory($(e.target).data('id'));
    },
    afterRender: function() {
        //this._switchCategory(this.model.get('categoryTypeId'));
    },

	changeUrl: function () {
		workspace.changeUrl('/products/search.html?query=' + this.model.get('searchString') + '&productTagsMnemonics=' + this.model.get('mainCategoryMnemonic'));
	}
});

ProductSearch.Views.Controller = BaseView.extend({

	construct:function (options) {
        options = options || {};
		var collection = this.model.getCollection();
		var filter = this.model.getFilter();

        if (!options.filterDisabled) {
            this.searchFilter = this.addChildAtElement('.js-anchr-filter-container', new ProductSearch.Views.SearchFilter({
                model: filter
            }), true);
        }

		this.listView = this.addChildAtElement(options.productsAnchor || '#js-product-container', new Products.Views.ProductListView({
			collection: collection,
            customClasses: options.customClasses
		}), options.replaceProductsAnchor);

		this.listView.onFilterChanged(this.model.get('filter'));

		filter.bind('filterChanged', this.onFilterChanged, this);
        this.bind('onShowInfoClicked', this.onShowInfoClicked, this);
        this.bind('onAddToShoppingCart', this.onAddToShoppingCart, this);
	},

    onFilterChanged:function () {
		this.listView.onFilterChanged(this.model.get('filter'));
		this.listView.clear();
	},

	onLoadNextClicked:function (e) {
		this.model.getCollection().loadNext();
	}

});

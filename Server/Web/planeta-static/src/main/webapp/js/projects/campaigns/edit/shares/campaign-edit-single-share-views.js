/*global CampaignEdit, CrowdFund*/
CampaignEdit.Views.SharesContentInner = BaseView.extend({
    className: 'project-create_list',
    modelEvents: {
        destroy: 'dispose',
        'change:errors': 'render'
    },
    construct: function () {
        var attrs = {
            model: this.model,
            // model with shares collection
            controller: this.controller
        };
        this.cardImageView = new CampaignEdit.Views.ShareImage({
            model: this.model.cardImageModel,
            // controller is needed to refresh image in shares collection
            controller: this.model,
            uploaderName: this.model.uploaderName
        });
        this.addChild(new CampaignEdit.Views.ShareName(attrs));
        if (PrivacyUtils.hasAdministrativeRole()) {
            this.addChild(new CampaignEdit.Views.ShareType(attrs));
        }
        this.addChild(new CampaignEdit.Views.ShareDescription(attrs));
        this.addChild(new CampaignEdit.Views.ShareRewardInstruction(attrs));
        this.addChild(new CampaignEdit.Views.SharePrice(attrs));
        this.addChild(new CampaignEdit.Views.ShareAmount(attrs));
        this.addChild(new CampaignEdit.Views.EstimatedDeliveryTime(attrs));
        this.addChild(this.cardImageView);
        this.addChild(new CampaignEdit.Views.ShareDelivery(attrs));
        this.addChild(new CampaignEdit.Views.SharePickup(attrs));
        this.addChild(new CampaignEdit.Views.ShareQuestionToBuyer(attrs));
        this.addChild(new CampaignEdit.Views.ShareFooter(attrs));
        this.addChild(new CampaignEdit.Views.ShareAdded(attrs));
    }
});

CampaignEdit.Views.ShareName = CampaignEdit.Views.BaseInput.extend({
    template: '#campaign-edit-share-name-template',
    modelAttr: 'name',
    afterRender: function () {
        // Temporary ugly hack
        // --- begin ---
        if ((this.controller.get('status') == 'ACTIVE'
            || this.controller.get('status') == 'NOT_STARTED'
            || this.controller.get('status') == 'PATCH'
            || this.controller.get('status') == 'APPROVED'
            || this.controller.get('status') == 'PAUSED')
            && (this.controller.get('timeAdded') < new Date('2018-03-20').getTime())) {
            this.$el.find('.js-share-name-limit').data('limit', 200);
        }
        // --- end ---
        CampaignEdit.Views.BaseInput.prototype.afterRender.call(this);
    }
});

CampaignEdit.Views.ShareType = CampaignEdit.Views.CampaignValidatedRow.extend({
    template: '#campaign-edit-share-type-template',
    modelAttr: 'shareStatus',
    events: {
        'change select': 'typeChanged'
    },
    afterRender: function() {
        var statusName = this.model.get("shareStatus");
        if (ProfileUtils.isInvestingProject(statusName)) {
            this.$('select').val(statusName);
        }
    },
    typeChanged: function(e) {
        this.model.set(this.modelAttr, $(e.currentTarget).val());
    }
});

CampaignEdit.Views.ShareDescription = CampaignEdit.Views.BaseInput.extend({
    template: '#campaign-edit-share-description-template',
    modelAttr: 'descriptionHtml',

    construct: function () {
        // Temporary ugly hack
        // --- begin ---
        var limit = 600;
        if ((this.controller.get('status') == 'ACTIVE'
                || this.controller.get('status') == 'NOT_STARTED'
                || this.controller.get('status') == 'PATCH'
                || this.controller.get('status') == 'APPROVED'
                || this.controller.get('status') == 'PAUSED')
            && (this.controller.get('timeAdded') < new Date('2018-03-20').getTime())) {
            limit = 1000;
        }
        // --- end ---
        this.model.editorShareDescriptionModel.config.limit = limit;

        this.addChildAtElement('[data-anchor=tiny-mce]', new CampaignEdit.Views.ShareDescriptionEdit({
            model: this.model
        }));
    }
});

CampaignEdit.Views.SharePrice = CampaignEdit.Views.BaseInput.extend({
    template: '#campaign-edit-share-price-template',
    modelAttr: 'price'
});

CampaignEdit.Views.ShareAmount = CampaignEdit.Views.BaseInputOrCheckbox.extend({
    template: '#campaign-edit-share-amount-template',
    modelAttr: 'amount'
});

CampaignEdit.Views.EstimatedDeliveryTime = CampaignEdit.Views.Date.extend({
    template: '#campaign-edit-share-estimated-delivery-time-template',
    modelAttr: 'estimatedDeliveryTime',
    isExistsPlanetaUiElement: true
});

CampaignEdit.Views.ShareImage = CrowdFund.Views.ImageField.extend({
    createLoadView: function () {
        var LoadView = CrowdFund.Views.ImageField.LoadView.extend({
            getFileUploader: function () {
                if (!this.model.hasOwnProperty('uploaderName')) {
                    this.model.uploaderName += '.' + this.model.cid;
                    this.controller.uploaderName = this.model.uploaderName;
                }

                return CampaignEdit.uploaderController.getFileUploader(this.model, this.getFileUploaderOptions(), this.controller);
            },
            removeFileUploader: function () {
                if (this.options.needsRemoveModelToUpdate) {
                    CampaignEdit.uploaderController.removeModelToUpdate(this.model.uploaderName, this.controller);
                }
            }
        });
        if (this.options.uploaderName) {
            CampaignEdit.uploaderController.addModelToUpdate(this.options.uploaderName, this.controller);
            this.model.uploaderName = this.options.uploaderName;
        }
        return new LoadView({
            model: this.model,
            controller: this.controller,
            needsRemoveModelToUpdate: !!this.options.uploaderName
        });

    }
});

CampaignEdit.Models.ShareImage = CrowdFund.Models.ImageField.extend({
    uploaderName: 'CampaignEdit.Models',
    onImageUrlChange: function (campaignModel) {
        campaignModel.set('imageId', this.get('imageId'));
        campaignModel.set('imageUrl', this.get('imageUrl'));
    }
});

CampaignEdit.Views.ShareDelivery = CampaignEdit.Views.BaseCheckboxAccordion.extend({
    template: '#campaign-edit-share-delivery-address-template',
    modelAttr: 'deliverToCustomer',
    construct: function() {
        this.model.on('change:deliverToCustomer', function(model, status) {
            _.each(model.get('linkedDeliveries'), function(it){
                if (it.serviceType === 'DELIVERY' || it.serviceType === 'RUSSIAN_POST')
                    it.enabled = status;
            });
        });
    }
});

CampaignEdit.Views.SharePickup = CampaignEdit.Views.BaseCheckboxAccordion.extend({
    template: '#campaign-edit-share-pickup-template',
    selectorToToggle: '[data-anchor=customerPickup]',
    modelAttr: 'pickupByCustomer',
    construct: function () {
        this.addChildAtElement(this.selectorToToggle, new CampaignEdit.Views.ShareDeliveryCustomerPickupCity({
            model: this.model
        }));
        this.addChildAtElement(this.selectorToToggle, new CampaignEdit.Views.ShareDeliveryCustomerPickupAddress({
            model: this.model
        }));
        this.addChildAtElement(this.selectorToToggle, new CampaignEdit.Views.ShareDeliveryCustomerPickupTel({
            model: this.model
        }));
        this.model.on('change:pickupByCustomer', function(model, status) {
            _.each(model.get('linkedDeliveries'), function(it){
                if (it.serviceType === 'CUSTOMER_PICKUP')
                    it.enabled = status;
            });
        });
    }
});

CampaignEdit.Views.ShareDeliveryCustomerPickupCity = CampaignEdit.Views.BaseInput.extend({
    template: '#campaign-edit-share-pickup-city-template',
    modelAttr: 'city'
});

CampaignEdit.Views.ShareDeliveryCustomerPickupAddress = CampaignEdit.Views.TextArea.extend({
    template: '#campaign-edit-share-pickup-address-template',
    modelAttr: 'street'
});

CampaignEdit.Views.ShareDeliveryCustomerPickupTel = CampaignEdit.Views.BaseInput.extend({
    template: '#campaign-edit-share-pickup-tel-template',
    modelAttr: 'phone'
});

CampaignEdit.Views.ShareRewardInstruction = CampaignEdit.Views.TextArea.extend({
    template: '#campaign-edit-share-reward-instruction-template',
    modelAttr: 'rewardInstruction'
});

CampaignEdit.Views.ShareRewardInstructionText = CampaignEdit.Views.TextArea.extend({
    modelAttr: 'rewardInstruction',
    template: '#campaign-edit-share-reward-instruction-row-template',

    modelEvents: {
        'change': '_render',
        'destroy': 'dispose'
    },

    _render: function() {
        var modelValue = this.model.get(this.modelAttr),
            input = this.getInput();

        if (modelValue !== input.val())
            this.render();
    }
});

CampaignEdit.Views.ShareRewardInstructionTextVariants = BaseView.extend({
    modelAttr: 'rewardInstructionVariant',
    template: '#campaign-edit-share-reward-instruction-row-variants-template',
    className: 'project-create_row',
    isExistsPlanetaUiElement: true,

    events: {
        'change': 'changeElement',
        'click .js-selector': 'openVariants'
    },

    afterRender: function() {
        this.$el.find('.dropdown').addClass('prj-crt-selectCampaignById');
        this.$el.find('.dropdown').prepend('<div class="prj-crt-select_over-lbl">Выберите один из типичных вопросов</div>');
    },

    changeElement: function(e) {
        this.model.set({
            rewardInstruction: $(e.currentTarget).find('.js-selector').val()
        });
    },

    openVariants: function() {
        $('[data-anchor=rewardInstruction]').attr('style', 'overflow: inherit;');
    }
});

CampaignEdit.Views.ShareQuestionToBuyer = CampaignEdit.Views.BaseCheckboxAccordion.extend({
    template: '#campaign-edit-share-author-question-template',
    modelAttr: 'questionToBuyer',
    selectorToToggle: '[data-anchor=questionToBuyer]',

    construct: function () {
        this.textArea = this.addChildAtElement(this.selectorToToggle, new CampaignEdit.Views.ShareQuestionToBuyerText({
            model: this.model
        }));

        this.addChildAtElement(this.selectorToToggle, new CampaignEdit.Views.ShareQuestionToBuyerTextVariants({
            model: this.model
        }));
    }
});

CampaignEdit.Views.ShareQuestionToBuyerText = CampaignEdit.Views.TextArea.extend({
    modelAttr: 'questionToBuyer',
    template: '#campaign-edit-share-author-question-row-template',
    modelEvents: {
        'change': '_render',
        'destroy': 'dispose'
    },
    _render: function() {
        var modelValue = this.model.get(this.modelAttr),
                input = this.getInput();

        if (modelValue !== input.val())
            this.render();
    }
});

CampaignEdit.Views.ShareQuestionToBuyerTextVariants = BaseView.extend({
    modelAttr: 'questionToBuyerVariant',
    template: '#campaign-edit-share-author-question-row-variants-template',
    className: 'project-create_row',
    isExistsPlanetaUiElement: true,

    events: {
        'change': 'changeElement'
    },

    afterRender: function() {
        this.$el.find('.dropdown').addClass('prj-crt-selectCampaignById');
        this.$el.find('.dropdown').prepend('<div class="prj-crt-select_over-lbl">Выберите один из типичных вопросов</div>');
    },

    changeElement: function(e) {
        this.model.set({
            questionToBuyer: $(e.currentTarget).find('.js-selector').val()
        });
    }
});

CampaignEdit.Views.ShareAdded = BaseView.extend({
    className: 'project-create_add-reward-success hidden',
    template: '#campaign-edit-share-added-template',
    id: 'share-added'
});

CampaignEdit.Views.ShareFooter = BaseView.extend({
    className: 'project-create_row',
    template: '#campaign-edit-share-add-template',

    onShareAdd: function () {
        var self = this;
        if (this.model.editorShareDescriptionModel) {
            this.model.editorShareDescriptionModel.save();
        }
        this.controller.addShare(this.model).done(function () {
            var linkedDeliveries = [];

            if(self.model.get('deliverToCustomer')) {
                linkedDeliveries.push({name: 'Доставка почтой'});
            }

            if(self.model.get('pickupByCustomer')) {
                linkedDeliveries.push({name: 'Самовывоз'});
            }

            if(linkedDeliveries.length > 0) {
                self.model.set('linkedDeliveries', linkedDeliveries);
            }
            //Я этого делать не хотел

            var errors = self.controller.get('errors');
            if (errors) {
                delete errors.shares;
            }
            self.controller.switchActiveShare();
            self.scrollTo(300);
            try{
                $('.js-modal-overlapped-view').animate({scrollTop: 0}, 300);
            } catch(ex) {}
            $('#share-added').show().removeClass('hidden').delay(1000).fadeOut(1000);
            $('#errorRow').show().removeClass('hidden').delay(1000).fadeOut(1000);
            try {
                setTimeout(function () {
                    self.onCancel();
                    var elOffset = $('.js-add-share-button').offset();
                    if (elOffset) {
                        $('html, body').animate({scrollTop: elOffset.top - 10}, 'slow');
                    }
                }, 1000);
            } catch(ex) {}
        }).fail(function (globalError, errors) {
            workspace.appView.showErrorMessage(globalError);
            self.scrollTo($('.error:first').scrollTop());
            console.log(errors);
        });

    },

    scrollTo: function (height) {
        $(window).scrollTop(height + 100);
        $('body, html').animate({scrollTop: height}, 300);
    },

    onShareSave: function () {
        var self = this;
        if (this.model.editorShareDescriptionModel) {
            this.model.editorShareDescriptionModel.save();
        }
        self.$('.btn-primary').attr('disabled', 'disabled');
        self.controller.saveShare(this.model).done(function () {
            self.onCancel();
        }).fail(function (globalError, errors) {
            self.$('.btn-primary').removeAttr('disabled');
            workspace.appView.showErrorMessage(globalError);
            console.log(errors);
        });
    },

    onCancel: function () {
        this.model.trigger('closeModal');
    }
});

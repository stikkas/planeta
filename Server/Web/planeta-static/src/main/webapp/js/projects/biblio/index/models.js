

Biblio.Models.Stats = BaseModel.extend({
    url: '/api/public/biblio-stats.json'
});

Biblio.Models.Partner = BaseModel.extend({});
Biblio.Models.Advertise = BaseModel.extend({});

Biblio.Models.LibraryRequest = BaseModel.extend({
    defaults: {
        organization: '',
        email: '',
        address: '',
        name: '',
        libraryType: 'PUBLIC'
    },

    save: function () {
        return Backbone.sync('create', this, {
            contentType: 'application/json',
            url: '/api/profile/request-create-library.json'
        });
    }
});

Biblio.Models.BookRequest = BaseModel.extend({
    defaults: {
        title: '',
        site: '',
        contact: '',
        email: ''
    },

    save: function () {
        return Backbone.sync('create', this, {
            contentType: 'application/json',
            url: '/api/profile/request-create-book.json'
        });
    }
});

Biblio.Models.IndexBooksList = BaseModel.extend({
    initialize: function () {
        this.collection = new Biblio.Collections.Book({
            url: '/api/public/books-index.json'
        });
    }
});

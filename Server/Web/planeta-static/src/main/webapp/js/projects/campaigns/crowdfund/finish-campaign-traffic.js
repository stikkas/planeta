var FinishCampaignTraffic = {
    Views: {}, Models: {}
};

FinishCampaignTraffic.init = function (campaignId, parentView, creatorProfileId, mainTag) {
    return Backbone.sync("read", null, {
        url: "/api/campaigns/finish-campaign-traffic-info.json",
        data: {
            campaignId: campaignId
        }
    }).done(function (response) {
        if (response && response.success && response.result) {
            var result = response.result;
            var viewType = 0;
            if (result.campaign) {
                viewType = 1;
                parentView.addChildAtElement('.js-author-active-campaign', new FinishCampaignTraffic.Views.ActiveCampaign({
                    model: new BaseModel(result.campaign)
                }));
            } /*else if (!result.campaign && result.productList && result.productList.length === 1) {
                viewType = 2;
                parentView.addChildAtElement('.js-finish-campaign-products-list', new FinishCampaignTraffic.Views.ProductCardOne({
                    model: new BaseModel(result.productList[0])
                }));
            }*/ if (result.productList) {
                viewType = 3;
                parentView.addChildAtElement('.js-finish-campaign-products-list', new FinishCampaignTraffic.Views.ProductCardList({
                    collection: new BaseCollection(result.productList.slice(0, 6)),
                    model: new BaseModel({
                        productsCount: result.productsCount,
                        creatorProfileId: creatorProfileId
                    })
                }));
            } /*else if (result.campaign && result.productList) {
                viewType = 4;
                parentView.addChildAtElement('.js-finish-campaign-traffic-active-campaign', new FinishCampaignTraffic.Views.ProductCardListWithCampaign({
                    collection: new BaseCollection(result.productList.slice(0, 2)),
                    model: new BaseModel(_.extend(result.campaign, {productsCount: result.productsCount}))
                }));
            } else if (result.campaignList) {
                viewType = 5;
                parentView.addChildAtElement('.js-finish-campaign-products-list', new FinishCampaignTraffic.Views.CampaignCardList({
                    collection: new BaseCollection(result.campaignList.slice(0, 4)),
                    campaignMainTag: mainTag
                }));
            }*/

            if (viewType) {
                parentView.addChildAtElement('.js-finish-campaign-traffic-alert-panel', new FinishCampaignTraffic.Views.AlertPanel({
                        model: new BaseModel({viewType: viewType})
                    }
                ));
            }

        }
    });
};

FinishCampaignTraffic.Views.AlertPanel = BaseView.extend({
    template: '#finish-campaign-traffic-alert-panel-template'
});
FinishCampaignTraffic.Views.ProductCardOne = BaseView.extend({
    template: '#finish-campaign-traffic-product-card-one-template'
});
FinishCampaignTraffic.Views.ProductCard = BaseView.extend({
    template: '#finish-campaign-traffic-product-card-template',
    tagName: 'span'
});

FinishCampaignTraffic.Views.ProductCardList = BaseListView.extend({
    template: '#finish-campaign-traffic-product-card-list-template',
    itemViewType: FinishCampaignTraffic.Views.ProductCard,
    childrenAnchor: '.js-list'
});

FinishCampaignTraffic.Views.ProductCardListWithCampaign = FinishCampaignTraffic.Views.ProductCardList.extend({
    template: '#finish-campaign-traffic-product-card-list-with-campaign-template',
    events: {
        'click .js-project-card': 'onClick'
    },

    construct: function () {
        this.collection.each(function (item) {
            item.set('hasActiveCampaign', true, {silent: true});
        });
        if (window.gtm) {
            window.gtm.trackViewCampaignList([this.model], "Виджеты на закрытом проекте");
        }
    },

    onClick: function () {
        if (window.gtm) {
            window.gtm.trackClickCampaignCard(this.model);
        }
    }
});


FinishCampaignTraffic.Views.CampaignCard = BaseView.extend({
    template: '#finish-campaign-traffic-campaign-card-template',
    events: {
        'click': 'onClick'
    },
    onClick: function () {
        if (window.gtm) {
            window.gtm.trackClickCampaignCard(this.model);
        }
    }
});

FinishCampaignTraffic.Views.CampaignCardList = BaseListView.extend({
    template: '#finish-campaign-traffic-campaign-card-list-template',
    itemViewType: FinishCampaignTraffic.Views.CampaignCard,
    childrenAnchor: '.js-list',
    construct: function (options) {

        this.collection.each(function (item) {
            var isTheSameTag = options.campaignMainTag && item.get('mainTag') && options.campaignMainTag.id === item.get('mainTag').id;
            item.set({
                gaEventLabel: isTheSameTag ? 'project_completed-projects_same_category' : 'project_completed-projects_same_category_clickanotherproject'
            }, {silent: true});
        });
    },
    onReset: function () {
        if (window.gtm) {
            if (this.collection.length) {
                window.gtm.trackViewCampaignList(this.collection.toArray(), "Виджеты на закрытом проекте");
            }
        }
        return BaseListView.prototype.onReset.apply(this, arguments);
    }
});

FinishCampaignTraffic.Views.initOdometer = function (view, model) {
    if (model.isOdometerInit) {
        return;
    }
    model.isOdometerInit = true;

    loadModule('odometer').done(function () {
        var deltaToAnimate = 0;
        var collectedAmount = model.get('collectedAmount') || 0;
        if (model.get('status') == "ACTIVE") {
            deltaToAnimate = Math.floor(Math.random() * 1500);
        }
        var od = new Odometer({
            el: view.$el.find('.js-campaign-odometer')[0],
            value: Math.max(collectedAmount - deltaToAnimate, 0),
            format: '( ddd)'
        });
        setTimeout(function () {
            od.update(collectedAmount);
        }, 500);
    });
};


FinishCampaignTraffic.Views.ActiveCampaign = BaseView.extend({
    template: '#finish-campaign-traffic-active-campaign-template',

    construct: function () {
        if (window.gtm) {
            window.gtm.trackViewCampaignList([this.model], "Виджеты на закрытом проекте");
        }
    },

    modelJSON: function () {
        return _.extend(BaseView.prototype.modelJSON.apply(this, arguments), {
            progress: this.model.get('collectedAmount') / this.model.get('targetAmount') * 100
        });

    }/*,

    afterRender: function () {
        FinishCampaignTraffic.Views.initOdometer(this, this.model);
    }
    */
});
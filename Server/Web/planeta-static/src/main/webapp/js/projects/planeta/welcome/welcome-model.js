/*global BaseModel,_,$,Backbone,BaseCollection*/
var Welcome = {
    Models: {},
    Views: {}
};

Welcome.Models.WelcomeModel = BaseModel.extend({
    initialize: function () {
        BaseModel.prototype.initialize.call(this);
        this.welcomeAbout = new Welcome.Models.WelcomeAbout();
        this.projectBox = new Welcome.Models.ProjectBox();
    },

    fetch: function (options) {
        if (!workspace.isAuthorized) {
            this.projectBox.fetch();
            return this.welcomeAbout.fetch(options);
        }
        // this.parallel([this.welcomeAbout, this.projectBox], options);
        return this.projectBox.fetch(options);
    }
});

Welcome.Models.ProjectBox = BaseModel.extend({

    defaults: {
        unreadNotificationsCount: 0,
        hasNotifications: false,
        projectFilter: 'ACTIVE'
    },

    initialize: function () {
        BaseModel.prototype.initialize.call(this);
        this.topCampaigns = new Welcome.Models.TopCampaigns([], {});
        this.newCampaigns = new Welcome.Models.NewCampaigns([], {});
        this.promoBanner = new Welcome.Models.PromoBanner();
    },

    fetch: function (options) {
        this.topCampaigns.load();
        this.newCampaigns.load();
        this.parallel([this.promoCampaigns, this.promoBanner], options);
    }
});

Welcome.Models.WelcomeAbout = BaseModel.extend({
    url: '/api/util/campaign/total-purchase-sum.json',
    parse: function (response) {
        return {
            campaignsTotalStats: response
        };
    }
});

Welcome.Models.Partners = BaseCollection.extend({
    url: '/api/welcome/get-partners.json'
});

Welcome.Models.PromoBanner = BaseModel.extend({
    url: '/api/util/random-banner-json.json?bannerType=PROMO_BANNER',
    parse: BaseModel.prototype.parseActionStatusQuietly
});


Welcome.Models.TopCampaigns = BaseCollection.extend({
    url: '/api/welcome/dashboard-filtered-campaigns.json',

    defaults: {
        offset: 0,
        limit: 12
    }
});

Welcome.Models.NewCampaigns = BaseCollection.extend({
    url: '/api/welcome/dashboard-filtered-campaigns.json',

    defaults: {
        offset: 0,
        limit: 12
    },
    
    data: {
        status: 'RECOMMENDED'
    },

    loadNext: function (options) {
        var self = this;
        var totalLimit = 32;

        if (this.offset + this.limit > totalLimit) {
            this.limit = totalLimit - this.offset;
        }
        var dfd = BaseCollection.prototype.loadNext.apply(this, arguments);

        dfd.done(function () {
            if (self.offset >= totalLimit) {
                self.allLoaded = true;
            }
        });
        return dfd;
    }
});

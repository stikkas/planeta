/*global Modal, AlbumTypes, ImageUtils, loadModules, UploadController, webcam*/
/**
 * Model and views for avatar manipulations.
 */

var Avatar = {
    changePhoto: function (options) {
        options = options || {};
        loadModules(['jcrop', 'webcam']).done(function () {
            var model = new Avatar.Models.Crop(options);
            var view = new Avatar.Views.Crop({
                imageType: options.imageType,
                model: model
            });
            Modal.showDialog(view);
        });
    },
    webcamAvatar: function (success, error) {
        var model = new Avatar.Models.Webcam({
            success: success,
            error: error
        });
        var view = new Avatar.Views.Webcam({
            model: model
        });
        Modal.showDialog(view);
    },
    showPhoto: function (photo) {
        workspace.showPhoto(photo.get('profileId'), photo.get('imageId'));
    }
};

Avatar.Views = {};
Avatar.Models = {};

/*****************************************************************
 * Views for avatar
 *****************************************************************/
Avatar.Models.Crop = BaseModel.extend({
    coords: {},
    initialize: function (options) {
        var profileModel = workspace.appModel.get('profileModel');
        var data = {
            profileId: profileModel.get('profileId'),
            userGender: profileModel.get('userGender'),
            success: options.success
        };
        if (options.imageUrl) {
            data.imageId = options.imageId;
            data.imageUrl = options.imageUrl;
            data.smallImageId = options.imageId;
            data.smallImageUrl = options.imageUrl;
        } else {
            data.imageId = profileModel.get('imageId');
            data.imageUrl = profileModel.get('imageUrl');
            data.smallImageId = profileModel.get('smallImageId');
            data.smallImageUrl = profileModel.get('smallImageUrl');
        }
        this.set(data);
    },
    saveAvatar: function (successCallback) {
        var _self = this;
        var options = {
            url: '/api/profile/avatar-save.json',
            data: {
                avatarImageId: _self.get('imageId'),
                profileId: _self.get('profileId')
            },
            success: function (response) {
                if (response && response.success) {
                    if (successCallback) {
                        successCallback();
                    } else if (_self.get('success')) {
                        var modelCallback = _self.get('success');
                        modelCallback(response.result);
                    }
                }
            }
        };
        Backbone.sync('update', this, options);
    },
    saveSmallAvatar: function (successCallback) {
        var _self = this;
        var coef = this.coef || 1;
        var options = {
            url: '/api/profile/crop-image.html',
            data: {
                clientId: _self.get('profileId'),
                ownerId: _self.get('profileId'),
                albumTypeId: AlbumTypes.ALBUM_AVATAR,
                url: ImageUtils.getUserAvatarUrl(_self.get('imageUrl'), ImageUtils.ORIGINAL, _self.get('userGender')),
                cropX: Math.round(_self.coords.x * coef),
                cropY: Math.round(_self.coords.y * coef),
                cropWidth: Math.round(_self.coords.w * coef),
                cropHeight: Math.round(_self.coords.h * coef)
            },
            success: function (response) {
                if (response && response.success) {
                    Backbone.sync('update', _self, {
                        url: '/api/profile/avatar-small-save.json',
                        data: {
                            avatarImageId: response.result.photoId,
                            profileId: _self.get('profileId')
                        },
                        success: function (response) {
                            if (response && response.success) {
                                _self.set({
                                    smallImageId: response.result.smallImageId,
                                    smallImageUrl: response.result.smallImageUrl
                                });
                                if (successCallback) {
                                    successCallback();
                                }
                            } else {
                                workspace.appView.showErrorMessage(response.errorMessage);
                            }
                        }
                    });
                } else {
                    workspace.appView.showErrorMessage(response.errorMessage);
                }
            }
        };
        Backbone.sync('read', this, options);
        return false;
    }
});

Avatar.Views.Crop = Modal.View.extend({
    className: 'modal narrow',
    template: '#avatar-crop-dialog-template',
    renderTemplate: true,
    events: {
        'click a.upload': 'upload',
        'click a.webcam': 'webcam',
        'click a.close': 'cancel',
        'click button[type=reset]': 'cancel',
        'click button[type=submit]': 'save'
    },

    initialize: function (options) {
        this.imageType = options.imageType || ImageUtils.SMALL_AVATAR;
        Modal.View.prototype.initialize.call(this, options);
    },
    afterRender: function () {
        Modal.View.prototype.afterRender.call(this);
        // var rendered = Modal.View.prototype.render.call(this);
        var _self = this, _el = $(this.el);
        var original = _el.find('#original-avatar');
        var showPreview = function (coords) {
            _self.model.coords = coords;
            var rx = _self.imageType.width / coords.w;
            var ry = _self.imageType.height / coords.h;
            $('.avatar-upload-preview-pic').css({
                width: _self.imageType.width + 'px',
                height: _self.imageType.height + 'px'
            });
            $('#preview-avatar').css({
                width: Math.round(rx * original.width()) + 'px',
                height: Math.round(ry * original.height()) + 'px',
                marginLeft: '-' + Math.round(rx * coords.x) + 'px',
                marginTop: '-' + Math.round(ry * coords.y) + 'px'
            });
        };
        if (this.model.get('imageUrl')) {
            var img = new Image();
            img.onload = function () {
                _self.model.coef = img.width / original.width();
                var m = Math.min(Math.max(130 / _self.model.coef, 20), original.width(), original.height());
                original.Jcrop({
                    boxWidth: 300,
                    boxHeight: 300,
                    setSelect: [0, 0, 50, 50],
                    onChange: showPreview,
                    onSelect: showPreview,
                    aspectRatio: _self.imageType.width / _self.imageType.height,
                    minSize: [m, m]
                });
            };
            img.src = ImageUtils.getUserAvatarUrl(_self.model.get('imageUrl'), ImageUtils.ORIGINAL, _self.model.get('userGender'));
        }
    },
    upload: function (e) {
        e.preventDefault();
        var success = this.model.get('success');
        var profileId = this.model.get('profileId');
        this.dispose();
        var self = this;
        UploadController.exec("showUploadProfileAvatar", profileId, function (uploadFiles) {
            if (uploadFiles) {
                var uploadFile = uploadFiles.models.shift();
                var uploadResult = uploadFile.get('uploadResult');
                if (uploadResult) {
                    Avatar.changePhoto({
                        imageType: self.imageType,
                        title: self.model.get('title'),
                        webcam: self.model.get('webcam'),
                        imageId: uploadResult.photoId,
                        imageUrl: uploadResult.imageUrl,
                        success: success
                    });
                }
            }
        });
    },
    webcam: function (e) {
        e.preventDefault();
        var successHandler = this.model.get('success');
        this.dispose();
        Avatar.webcamAvatar(function (response) {
            if (response) {
                response = JSON.parse(response);
            }
            if (response.success) {
                var photo = response.result;
                Avatar.changePhoto({imageId: photo.photoId, imageUrl: photo.imageUrl, success: successHandler});
            } else {
                workspace.appView.showErrorMessage(response.errorMessage);
            }
        }, function () {
            workspace.appView.showErrorMessage('Не удалось загрузить фотографию');
        });
    },
    save: function () {
        var _self = this;
        var successCallback = _self.model.get('success');
        var button = $('[type="submit"]', this.el);
        button.attr('disabled', 'disabled');
        button.html('<i class="icon-load"></i> Изменения сохраняются');
        _self.model.saveAvatar(
            _self.model.saveSmallAvatar(function () {
//	            button.removeAttr('disabled');
                if (successCallback) {
                    successCallback({
                        imageId: _self.model.get('imageId'),
                        imageUrl: _self.model.get('imageUrl'),
                        smallImageId: _self.model.get('smallImageId'),
                        smallImageUrl: _self.model.get('smallImageUrl')
                    });
                } else {
                    workspace.appView.showErrorMessage("Ошибка при сохранении данных");
                }
                workspace.appView.showSuccessMessage("Данные сохранены");
                _self.dispose();
            })
        );
        return false;
    }
});

/*****************************************************************
 * View and Model for webcam captured avatar
 *****************************************************************/
Avatar.Models.Webcam = BaseModel.extend({});

Avatar.Views.Webcam = Modal.View.extend({
    template: '#avatar-webcam-dialog-template',
    renderTemplate: true,
    coords: {},
    events: {
        'click a.close': 'cancel',
        'click button[type=reset]': 'cancel',
        'click button[type=submit]': 'save'
    },
    afterRender: function () {
        Modal.View.prototype.afterRender.call(this);
        //var rendered = Modal.View.prototype.render.call(this);
        var _el = $(this.el);
        webcam.set_swf_url('/flash/webcam.swf');
        webcam.set_api_url('/api/profile/upload-avatar.html');
        webcam.set_quality(90); // JPEG quality (1 - 100)
        webcam.set_shutter_sound(false); // no shutter click sound
        webcam.set_stealth(true); // enable stealth mode
        webcam.set_hook('onComplete', this.model.get('success'));
        webcam.set_hook('onError', this.model.get('error'));
        _el.find('.modal-body').html(webcam.get_html(320, 240));
    },
    save: function (event) {
        if (event) {
            event.preventDefault();
            event.stopPropagation();
        }
        setTimeout(function () {
            webcam.snap();
        }, 10);
    }
});

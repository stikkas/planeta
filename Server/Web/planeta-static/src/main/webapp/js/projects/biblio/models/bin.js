/**
 * Корзина содержит издания и библиотеки
 */
Biblio.Models.Bin = BaseModel.extend({
    url: function () {
        return '/api/public/bin.json';
    },
    data: function () {
        var self = this;
        function add(prev, cur) {
            prev.push(cur.toJSON());
            return prev;
        }
        return {
            id: self.get('id'),
            profileId: self.get('profileId'),
            books: _.reduce(self.get('books'), add, []),
            libraries: _.reduce(self.get('libraries'), add, [])
        };
    },
    changeBooks: function (book, value) {
        var bookId = book.get('bookId'),
                books = this.get('books'),
                found = books[bookId];
        if (value) {
            if (!found) {
                books[bookId] = found = new BaseModel({book: book, count: 0});
                this.booksChanged();
            }
            if (found.get('count') != value) {
                found.set('count', value);
                this.updateTotalSum();
            }
        } else if (found) {
            delete books[bookId];
            this.updateTotalSum();
            this.booksChanged();
        }
    },
    /**
     * Добавляет книги в корзину, если их еще нет
     * @param {Array(Biblio.Models.Book)} books
     */
    addBooks: function (books) {
        var myBooks = this.get('books'),
                changed = false;
        _.each(books, function (book) {
            var bookId = book.get('bookId');
            if (!myBooks[bookId]) {
                myBooks[bookId] = new BaseModel({book: book, count: 1});
                changed = true;
            }
        });
        if (changed) {
            this.updateTotalSum();
            this.booksChanged();
        }
    },
    /**
     * Выбрасывает событие когда изменился размер (кол-во свойств) объекта books
     * в корзине 
     */
    booksChanged: function () {
        this.trigger('books', _.keys(this.get('books')).length);
    },
    /**
     * Выбрасывает событие когда изменился размер (кол-во свойств) объекта libraries
     * в корзине 
     */
    libsChanged: function () {
        this.trigger('libs', _.keys(this.get('libraries')).length);
    },
    updateTotalSum: function () {
        var totalSum = 0;
        _.each(this.get('books'), function (it) {
            totalSum += it.get('book').get('price') * it.get('count');
        });
        this.set('totalSum', totalSum * (Biblio.data.stepOne ? 1 : Math.max(1, _.keys(this.get('libraries')).length)));
    },
    /**
     * Return count of a book  
     * @param {Biblio.Models.Book} book
     */
    countBook: function (book) {
        var books = this.get('books'),
                found = books[book.get('bookId')];
        if (found)
            return found.get('count');
        return 0;
    },
    addLibrary: function (library) {
        var libraryId = library.get('libraryId'),
                libraries = this.get('libraries');
        if (!libraries[libraryId]) {
            this.libsChanged();
            libraries[libraryId] = library;
            this.updateTotalSum();
            this.libsChanged();
        }
        var libcount = this.get('librariesCount') || 0;
        libcount++;
        this.set({'librariesCount': libcount});
    },
    delLibrary: function (library) {
        var libraries = this.get('libraries'),
                id = library.get('libraryId');
        if (libraries[id]) {
            delete libraries[id];
            this.updateTotalSum();
            this.libsChanged();
        }
        var libcount = this.get('librariesCount');
        if (libcount > 0) {
            libcount--;
        }
        this.set({'librariesCount': libcount});
    },
    save: function () {
        var self = this;
        if (!self._areq) { // Второй запрос не выполнится, пока не выполнится первый
            self._areq = Backbone.sync('create', null, {
                contentType: 'application/json',
                url: self.url(),
                data: self.data()
            }).always(function () {
                self._areq = undefined;
            });
        }
        return self._areq;
    },
    fetch: function () {
        var self = this;
        return Backbone.sync('read', self, {
            success: function (data) {
                self.set({id: data.id, profileId: data.profileId, books: {}, libraries: {}}, {silent: true});
                var books = self.get('books');

                _.each(data.books, function (book) {
                    books[book.book.bookId] = new BaseModel({book: new Biblio.Models.Book(book.book), count: book.count});
                });
                _.each(data.libraries, function (library) {
                    self.addLibrary(new Biblio.Models.Library(library));
                });
                self.updateTotalSum();
            }
        });
    }
});

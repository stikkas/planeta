/*globals CampaignEdit, Attach, ValidateUtils, CrowdFund, AlbumTypes, ImageUtils, ImageType, Modal */

/**
 * Used as controller in subviews
 */
CampaignEdit.Models.Shares = CampaignEdit.Models.Tab.extend({
    newShare: function (shareModel) {
        shareModel = shareModel || {};
        var options = _.extend({}, shareModel.attributes, {
            campaignId: this.get('campaignId'),
            // necessary for share image upload
            profileId: this.get('profileId'),
            purchaseCount: 0,
            purchaseSum: 0,
            order: null
        });
        options.shareId = 0;
        return new CampaignEdit.Models.Share(options);
    },
    getSharesCollection: function () {
        return this.shares;
    },
    shareChanged: function (shareModel) {
        if (shareModel.editorShareDescriptionModel) {
            shareModel.editorShareDescriptionModel.save();
        }
        var originalShare = this.getSharesCollection().findByAttr('shareId', shareModel.get('shareId'));
        if (!originalShare) {
            return false;
        }
        return !_.isEqual(originalShare.getData(), shareModel.getData());
    },
    /**
     *
     * @param {CampaignEdit.Models.Share} shareModel
     */
    saveShare: function (shareModel) {
        var self = this;
        return shareModel.validateShare().done(function (savedShare) {
            if (savedShare.shareStatus !== 'DISABLED') {
                var share = self.getSharesCollection().findByAttr('shareId', shareModel.get('shareId'));
                var needsResort = share.get('price') != savedShare.price && !share.get('order');
                share.set(savedShare);
                if (needsResort) {
                    self.getSharesCollection().remove(share);
                    self._addShareToCollection(share);
                }
                CampaignEdit.uploaderController.addModelToUpdate(shareModel.uploaderName, share);
            } else {
                self.getSharesCollection().remove(savedShare);
            }
        }).fail(function (globalError, errors) {
            shareModel.set({errors: errors});
        });
    },
    removeShare: function (shareModel) {
        var self = this;
        return shareModel.deleteShare().done(function () {
            self.getSharesCollection().remove(shareModel);
            shareModel.set('shareStatus', 'DISABLED');
        }).fail(function (globalError, errors) {
            shareModel.set({errors: errors});
        });
    },
    /**
     * Add new share to shares collection.
     * @param {CampaignEdit.Models.Share} shareModel
     */
    addShare: function (shareModel) {
        var collection = this.getSharesCollection();
        var self = this;
        return shareModel.validateShare().done(function (model) {
            shareModel.set(model);
            shareModel.set({errors: {}});
            shareModel.set('isDonate', shareModel.get('price') === 0);
            self._addShareToCollection(shareModel);
        }).fail(function (globalError, errors) {
            shareModel.set({errors: errors});
        });
    },
    _addShareToCollection: function (shareModel) {
        var collection = this.getSharesCollection();
        var insertIndex = collection.length;
        collection.find(function (item, idx) {
            if (!item.get('order') && item.get('price') > shareModel.get('price')) {
                insertIndex = idx;
                return true;
            }
            return false;
        });
        collection.add(shareModel, {at: insertIndex});
    },
    deleteShare: function (shareModel) {
        this.removeShare(shareModel);
    },
    switchActiveShare: function (shareModel) {
        this.set('activeShare', this.newShare(shareModel));
        try {
            workspace.appView.views.contentView.innerView.render();
        } catch (ex) {
        }
    },
    onLocationChange: function (e, callback) {
        if (!_.isEqual(this.get('activeShare').getData(), this.newShare().getData())) {
            e.preventDefault();
            e.stopPropagation();
            Modal.showConfirm("Текущее вознаграждение не сохранено. Продолжить?", "Подтверждение перехода", callback);
        }
    }
});

CampaignEdit.Models.Share = BaseModel.extend({
    idAttribute: 'shareId',
    observableFields: {
        campaignId: 0,
        profileId: 0,
        shareId: 0,
        name: '',
        shareStatus: 'ACTIVE',
        description: '',
        descriptionHtml: '',
        imageId: 0,
        imageUrl: '',
        price: 0,
        amount: 0,
        rewardInstruction: '',
        questionToBuyer: '',
        purchaseCount: 0,
        pickupByCustomer: false,
        deliverToCustomer: false,
        city: '',
        street: '',
        phone: '',
        estimatedDeliveryTime: '',
        currentDate: new Date(),
        isDonate: false,
        linkedDeliveries: undefined
    },
    defaults: function () {
        return _.extend({}, this.observableFields, {
            notCharity: true,
            showShareHover: false,
            campaignStatus: 'ACTIVE',
            errors: {}
        });
    },
    initialize: function () {
        this.cardImageModel = new CampaignEdit.Models.ShareImage({
            originalImage: {
                imageUrl: this.get('imageUrl'),
                photoId: this.get('imageId')
            },
            profileId: this.get('profileId'),
            albumTypeId: AlbumTypes.ALBUM_COMMENT,
            thumbnail: {
                imageConfig: ImageUtils.MEDIUM,
                imageType: ImageType.PHOTO
            },
            aspectRatio: 220 / 134,
            width: 220,
            title: 'Картинка на вознаграждение'
//            description: 'Эта картинка отображается на главной странице Планеты и в результатах поиска.'
        });
        this.editorShareDescriptionModel = new CampaignEdit.Models.EditorShareDescriptionModel(this);
        this.set({isDonate: this.get('price') === 0});
    },
    uploadPhoto: function (underlyingModal) {
        var self = this;

        Attach.showUploadSinglePhotoDialog(function (photo) {
            self.set({
                imageId: photo.id,
                imageUrl: photo.value
            });
        }, underlyingModal);
    },
    getData: function () {
        var self = this;
        var data = {};
        _.each(this.observableFields, function (value, key) {
            // cast to string to make comparison possible
            //TODO: Костыль. Подумать, как сделать нормально
            if(key !== 'linkedDeliveries') {
                data[key] = self.get(key) ? '' + self.get(key) : value;
            } else {
                data[key] = undefined;
            }
        });
        return data;
    },
    reset: function () {
        this.set(_.extend({}, this.defaults, {
            campaignId: this.get('campaignId'),
            profileId: this.get('profileId')
        }));
        this.id = null;
    },
    restore: function () {
        this.id = this.get('shareId') || null;
        this.validator.reset('*', {silent: true});
        this.set(this.get('oldData'));
    },
    validateShare: function () {
        var $dfd = $.Deferred();
        var self = this,
            order = this.get('order'),
            data = this.getData();
        if (order !== null)
            _.extend(data, {order: order});
        var _options = {
            url: '/admin/campaign-validate-share.json',
            data: data,
            success: function (response) {
                if (response.success) {
                    var savedShare = self.parse(response.result);
                    $dfd.resolve(savedShare);
                } else {
                    $dfd.reject(response.errorMessage, response.fieldErrors);
                }
            },
            error: function () {
                $dfd.reject({global: 'Ошибка соединения'});
            }
        };
        Backbone.sync('update', this, _options);
        return $dfd.promise();
    },
    parse: function (result) {
        var self = this;
        var data = {};
        _.each(this.getData(), function (value, key) {
            data[key] = result[key] || self.get(key);
        });
        return data;
    },
    deleteShare: function () {
        var shares = this.collection;
        var $dfd = $.Deferred();
        if (shares.length === 1) {
            $dfd.rejectWith(this, ['У проекта должно быть хотя бы одно вознаграждение']);
        } else {
            if (this.isNew()) {
                $dfd.rejectWith(this, ['Вознаграждение не сохранено']);
            } else {
                var self = this;
                var options = {
                    url: '/admin/campaign-validate-remove-share.json',
                    data: {
                        shareId: this.get('shareId')
                    },
                    context: this,
                    success: function (response) {
                        if (response.success) {
                            // shares.remove(this);
                            $dfd.resolve();
                        } else {
                            $dfd.rejectWith(this, ['Невозможно удалить вознаграждение']);
                        }
                    },
                    complete: function () {
                        $dfd.rejectWith(this, ['Ошибка соединения']);
                    }
                };

                Backbone.sync('delete', self, options);
            }
        }

        return $dfd.promise();
    }
});

CampaignEdit.Models.ShareCollection = BaseCollection.extend({
    model: CampaignEdit.Models.Share
});
Promo.Views.CampaignList = Welcome.Views.TopCampaigns.extend({
    construct: function (options) {
        if (options && options.carousel) {
            this.carousel = _.clone(options.carousel);
        }
    },
    afterRender: function () {
        $("img", this.$el).each(function () {
            $(this).attr("src", $(this).attr("data-original"));
        });

        if (this.carousel) {
            var itemCount = this.collection.length;
            var itemsInRow = this.carousel.itemsInRow;
            var navText = this.carousel.navText;
            var itemsInRow1024 = itemsInRow - 1;
            var slideStep = (itemCount < itemsInRow * 2) ? (itemCount % itemsInRow) : itemsInRow;
            var slideStep1024 = (itemCount < itemsInRow1024 * 2) ? (itemCount % itemsInRow1024) : itemsInRow1024;


            var owl = this.$el.addClass('owl-carousel').owlCarousel({
                lazyLoad: true,
                autoplay: false,
                autoplayTimeout: 5000,
                autoplayHoverPause: true,
                loop: true,
                items: itemsInRow,
                slideBy: slideStep,
                nav: true,
                dots: false,
                margin: 20,
                mouseDrag: false,
                smartSpeed: 200,
                navText: [
                    '<span class="owl-arrow-left"></span>',
                    '<span class="owl-arrow-right"></span>'
                ],
                responsive: {
                    0: {
                        items: 1,
                        margin: 10,
                        autoWidth: true,
                        center: true
                    },
                    768: {
                        items: 1,
                        margin: 10,
                        autoWidth: true,
                        center: true
                    },
                    1024: {
                        loop: itemCount > itemsInRow1024,
                        margin: 20,
                        items: slideStep1024,
                        autoWidth: false,
                        center: false,
                        slideBy: slideStep1024
                    },
                    1300: {
                        loop: itemCount > itemsInRow,
                        items: itemsInRow,
                        slideBy: slideStep
                    }
                }
            });
        }
    }
});


Promo.Views.CampaignListContainer = BaseView.extend({
    alreadyRendered: false,
    events: {
        'click .js-filter': 'onFilterClick',
        'click .js-all-projects': 'onAllProjectsClick'
    },
    onFilterClick: function (e) {
        e.preventDefault();
        var $el = $(e.currentTarget);
        this.model.set({activeFilter: $el.data('filterId')});
    },
    onAllProjectsClick: function (el) {
        var url = "https://" + workspace.serviceUrls.mainHost + "/search/projects";
        if (this.model.get("category")) {
            url = url + "?categories=" + this.model.get("category");
        }
        document.location.href = url;
    },
    construct: function (options) {
        if (options) {
            if (options.template) {
                this.template = options.template;
            }
            if (options.listClass) {
                this.listClass = options.listClass;
            }
        }
    },
    afterRender: function () {
        var status; // must be undefined, if no filters set
        if (this.model.get("filters") && this.model.get("filters").length > 0) {
            status = this.model.get("filters")[this.model.get("activeFilter")].status;
        }

        var topCampaigns = new Promo.Models.CampaignList({
            data: {
                categories: this.model.get("category"),
                status: status
            },
            limit: this.model.get("limit")
        });


        var self = this;

        topCampaigns.load().done(function () {
            var viewTopCampaigns = new Promo.Views.CampaignList({
                collection: topCampaigns,
                el: self.listClass || '.js-project-list',
                carousel: self.model.get("carousel")
            });
            viewTopCampaigns.render();
        }).always(function () {
            if (topCampaigns && topCampaigns.length > 0 && !self.alreadyRendered) {
                self.model.set({isEmpty: false});
            }
            self.alreadyRendered = true;
        });
    }
});

Promo.Views.NewsItem = BaseRichView.extend({
        template: '#promo-news-item-template',
        className: 'news-list_i',

        events: {
            'click': 'onClick'
        },

        onClick: function (e) {
            if (e) {
                e.preventDefault();
            }
            Promo.Views.News.prototype.showPostModal.call(this, this.model.get('postId'));
        }
});

Promo.Views.News = DefaultListView.extend({
    itemViewType: Promo.Views.NewsItem,
    className: 'news-list',
    pagerTemplate: '#promo-news-pager-template',

    showPostModal: function (postId) {
        var model = this.model;
        var post = new News.Models.SinglePost({
            postId: postId,
            type: 'POST',
            authorProfileId: model.get('authorProfileId'),
            campaignId: model.get('campaignId'),
            profileId: model.get('profileId')
        }, {
            collection: model.collection
        });

        Modal.showDialogByViewClass(Tech.Views.ModalPost, post);
    }
});
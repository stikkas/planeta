CrowdFund.Views.ProfileSites = BaseView.extend({
    template: '#profile-sites-template',
    construct: function (options) {

        var needFetch = true;
        if (!options || !options.profileId) {
            workspace.stats.tackNoProfileId('CrowdFundModelsProfileSites: ' + JSON.stringify(options));
            needFetch = false;
        }

        if (!this.model) {
            this.model = new CrowdFund.Models.ProfileSites();
            if (needFetch) {
                this.model.fetch({
                    data: {profileId: options.profileId}
                }).done(function () {
                    if (options && options.controller) {
                        options.controller.change();
                    }
                });
            }
        }
    }
});

CrowdFund.Models.ProfileSites = BaseModel.extend({
    url: '/api/public/profile-sites'
});
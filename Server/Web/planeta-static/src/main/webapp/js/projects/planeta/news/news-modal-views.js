/*globals News, Modal, Comments, Campaign, BaseRichView*/
News.Views.Modal = {};
News.Models.Modal = {
    CAMPAIGN_SPAM_DELAY: 7 * 24 * 3600 * 1000
};

News.Views.Modal.Post = Modal.OverlappedView.extend({
    className: 'modal modal-feed-post',
    clickOff: true,
    template: '#user-news-post-template',
    urlAnchor: function () {
        return 'post' + this.model.get('postId');
    },
    construct: function () {
        if (this.model.get('campaignId')) {
            var campaignCard = new News.Views.Modal.Post.CampaignCard({
                model: this.getCampaignModel()
            });
            this.addChildAtElement('.feed-post_project', campaignCard);
        }

        this.addChildAtElement('.feed_post', new News.Views.Modal.Post.Content({
            model: this.model
        }));

        this.addChildAtElement('.feed_action', new News.Views.Modal.Post.AdminPanel({
            model: this.model
        }));
        this.addChildAtElement('.comments-list', new Comments.Views.ModalComments({
            model: this.getCommentsModel()
        }));
    },

    getCommentsModel: function () {
        if (!this.commentsModel) {
            this.commentsModel = this.model.createComments();
            this.commentsModel.fetch();
        }
        return this.commentsModel;
    },

    getCampaignModel: function () {
        var campaignModel = new News.Models.Modal.Campaign({
            campaignId: this.model.get('campaignId')
        });
        campaignModel.fetch();
        return campaignModel;
    },

    afterRender: function () {
        Modal.OverlappedView.prototype.afterRender.apply(this, arguments);
        News.Views.Post.prototype.addSharing.apply(this, arguments);
        this.addPageData();
    },

    getMetaTags: function () {
        return this.sharingData;
    },

    addPageData: function () {
        var title = this.model.get('postName');
        var tagTitle = '<title>' + title + '</title>';
        if ($('head>title').length > 0) {
            $('head>title').html(title);
        } else {
            $('head').append(tagTitle);
        }

        var description = BlogUtils.trimContentHtml(this.model.get('headingText'));
        if (description) {
            description = StringUtils.cutString(description, BlogUtils.maxContentLength);
        } else {
            description = data.title;
        }

        var metaTagDescription = '<meta name="description" content="' + description + '">';
        if ($('meta[name="description"]').length > 0) {
            $('meta[name="description"]').attr('content', title);
        } else {
            $('head').append(metaTagDescription);
        }
    },

    dispose: function () {
        Modal.OverlappedView.prototype.dispose.apply(this, arguments);
        if (this.model.collection) {
            var original = this.model.collection.findByAttr('id', this.model.get('id'));
            original.comments.copyFrom(this.getCommentsModel(), 3);
        }
    }
});

News.Views.PostPage = BaseRichView.extend({
    template: '#user-news-post-page-template',

    construct: function () {
        if (this.model.get('campaignId')) {
            var campaignCard = new News.Views.Modal.Post.CampaignCard({
                model: this.getCampaignModel()
            });
            this.addChildAtElement('.feed-post_project', campaignCard);
        }

        this.addChildAtElement('.feed_post', new News.Views.Modal.Post.Content({
            model: this.model
        }));

        this.addChildAtElement('.feed_action', new News.Views.Modal.Post.AdminPanel({
            model: this.model
        }));
        this.addChildAtElement('.comments-list', new Comments.Views.ModalComments({
            model: this.getCommentsModel()
        }));
    },

    cancel: function () {
    },

    getCampaignModel: function () {
        var campaignModel = new News.Models.Modal.Campaign({
            campaignId: this.model.get('campaignId')
        });
        campaignModel.fetch();
        return campaignModel;
    },

    getCommentsModel: function () {
        if (!this.commentsModel) {
            this.commentsModel = this.model.createComments();
            this.commentsModel.fetch();
        }
        return this.commentsModel;
    },

    getMetaTags: function () {
        return this.sharingData;
    },

    addSharing: function () {
        if (!this.sharingData) {
            var post = this.model;
            var data = {
                title: post.get('postName'),
                counterEnabled: false,
                hidden: false,
                className: 'donate-sharing'
            };

            var isCampaignPost = post.get('campaignId') > 0;
            data.url = isCampaignPost
                ? ProfileUtils.getAbsoluteUserLink(this.model.get('profileId'), this.model.get("authorAlias")) + '/news/' + post.get('postId')
                : ProfileUtils.getAbsoluteUserLink(this.model.get('profileId'), this.model.get("authorAlias")) + '/news/' + post.get('postId');
            // get description
            var description = BlogUtils.trimContentHtml(post.get('headingText'));
            if (description) {
                description = StringUtils.cutString(description, BlogUtils.maxContentLength);
            } else {
                description = data.title;
            }
            data.description = description;
            // get image
            var photo = this.$el.find('[data-role="photo"]');
            data.image = photo.length ? photo.data('json').image : post.get(isCampaignPost ? 'campaignImageUrl' : 'authorImageUrl');

            this.sharingData = data;
        }
        this.$('.feed_sharing').share(this.sharingData);
    },

    afterRender: function () {
        this.addSharing();
    }
});

News.Views.Modal.CampaignPost = News.Views.Modal.Post.extend({
    getCampaignModel: function () {
        var contentModel = workspace.appModel.get('contentModel');
        return contentModel.has('campaignModel')
            ? contentModel.get('campaignModel')
            : News.Views.Modal.Post.prototype.getCampaignModel.apply(this, arguments);
    }
});

News.Models.Modal.Campaign = BaseModel.extend({
    defaults: {
        status: 'DRAFT',
        collectedAmount: 0,
        targetAmount: 0,
        nameHtml: '',
        shortDescription: '',
        timeFinish: new Date().getTime(),
        imageUrl: ''
    },
    url: function () {
        return '/api/campaign/get-campaign.json?' + $.param({campaignAlias: this.get('campaignId')});
    },
    parse: BaseModel.prototype.parseActionStatus
});

News.Views.Modal.Post.CampaignCard = BaseView.extend({
    className: 'n-own-project_list n-own-project_list__slim',
    template: '#user-news-post-campaign-card-template',
    beforeRender: function () {
        Campaign.Views.Widget.prototype.beforeRender.apply(this, arguments);
    },
    onDonate: function () {
        this.parent.cancel();
        workspace.onClickNavigate(null, 'https://' + workspace.serviceUrls.mainHost + '/campaigns/' + this.model.get('campaignId') + '/donate');
    }
});

News.Views.Modal.Post.Content = BaseRichView.extend({
    template: '#user-news-post-content-template'
});

News.Views.Modal.Post.AdminPanel = BaseView.extend({
    template: '#user-edit-post-admin-panel-template',
    onDelete: function () {
        var L10n = {
            _dictionary:{
                "ru":{
                    "areYouSureDoYouWannaDelete":"Действительно удалить запись?",
                    "deleteConfirmation":"Подтвердите удаление."
                },
                "en":{
                    "areYouSureDoYouWannaDelete":"Are you sure you want to deleteByProfileId the post?",
                    "deleteConfirmation":"Delete confirmation"
                }
            }
        };

        var translate = function (word, lang) {
            if (!lang)
                throw "language is empty.";
            return L10n._dictionary[lang][word] || word;
        };

        var lang = window.workspace && (workspace.currentLanguage === "en") ? "en" : "ru";

        var self = this;
        Modal.showConfirm(translate('areYouSureDoYouWannaDelete', lang), translate('deleteConfirmation', lang), {
            success: function () {
                self.closeModal();
                self.model.remove();
                Backbone.GlobalEvents.trigger('news-deleted');
            }
        });
    },

    closeModal: function () {
        this.parent.cancel();
    },

    onEdit: function () {
        new News.Views.Modal.PostEdit({model: this.model}).render();
    },

    onSpam: function () {
        var L10n = {
            _dictionary:{
                "ru":{
                    "areYouSureDoYouWannaSentNews":"Вы действительно хотите отправить новость подписчикам?",
                    "newsletterSent":"Рассылка отправлена"
                },
                "en":{
                    "areYouSureDoYouWannaSentNews":"Send this message to all subscribers?",
                    "newsletterSent":"Newsletters sent"
                }
            }
        };

        var translate = function (word, lang) {
            if (!lang)
                throw "language is empty.";
            return L10n._dictionary[lang][word] || word;
        };

        var lang = window.workspace && (workspace.currentLanguage === "en") ? "en" : "ru";

        var self = this;
        Modal.showConfirm(translate('areYouSureDoYouWannaSentNews', lang), null, {
            success: function () {
                self.model.startSpam().done(function () {
                    workspace.appView.showSuccessMessage(translate('newsletterSent', lang));
                });
            }
        });

    }
});

News.Models.Modal.TinyEditor = function (parentModel) {
    _.extend(this, new News.Models.TinyEditor(parentModel), {
        width: 660,
        height: 0,
        autoHeight: function () {
            var iframeBordersHeight = 22;
            var toolbarHeight = 50;
            var self = this;
            var height = $(window).height();
            var parentSelectors = ['.modal', '.modal-body'];
            _.each(parentSelectors, function (selector) {
                height -= self.calcMargins($(selector));
            });
            var siblingsSelectors = ['.post-write_action', '.post-write_head'];
            _.each(siblingsSelectors, function (selector) {
                height -= $(selector).outerHeight(true);
            });
            return height - iframeBordersHeight - toolbarHeight + 20;
        }
    });
};

News.Views.Modal.TinyEditor = News.Views.TinyEditor.extend({
    createEditorModel: function (model) {
        return new News.Models.Modal.TinyEditor(model);
    }
});


News.Views.Modal.PostEdit = Modal.OverlappedView.extend({
    className: 'modal modal-feed-post-edit',
    template: '#user-edit-post-modal-template',
    construct: function () {
        News.Views.disableTinyView();
        this.tinyView = this.addChildAtElement('[data-anchor=tiny-mce]', new News.Views.Modal.TinyEditor({
            model: this.model
        }));
    },

    renderMedia: function () {
        // no action to prevent rich-media-tags convert before tiny initialization
    },

    render: function () {
        return Modal.OverlappedView.prototype.render.apply(this, arguments);
    },

    afterRender: function () {
        Modal.OverlappedView.prototype.afterRender.apply(this, arguments);
        News.Views.disableTinyView();
        this.tinyView.initTiny();
    },

    onSave: function (e) {
        var $btn = $(e.currentTarget);
        if ($btn.is('.disabled')) {
            return;
        }

        var self = this;
        var title = this.$('input.post-write_head_input').val();
        var postText = this.tinyView.editorModel.html();

        $btn.toggleClass('disabled');
        this.model.save(title, postText).done(function (result) {
            self.dispose();
            if($('.updates > div > div').children().length == 1) {
                document.location.reload();
            }
        }).fail(function (globalError, errors) {
            var message = _.isEmpty(errors) ? globalError : _.first(_.values(errors));
            workspace.appView.showErrorMessage(message);
        }).always(function () {
            $btn.toggleClass('disabled');
        });
    },

    dispose: function () {
        // new News.Views.Modal.Post({model: this.model}).render();
        Modal.OverlappedView.prototype.dispose.apply(this, arguments);
    }
});

News.Views.Modal.PostEditor = News.Views.Modal.PostEdit.extend({
    modelEvents: {
        'destroy': 'dispose'
    },

    dispose: function () {
        Modal.OverlappedView.prototype.dispose.apply(this, arguments);
    }
});

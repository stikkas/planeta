/*global DefaultScrollableListView*/
if (!window.User) {
    var User = {
        Models: {},
        Views: {}
    };
}

User.Models.Projects = BaseModel.extend({
    pageData: function () {
        var profileModel = this.get('profileModel');
        var cityName = profileModel.get('cityName');
        var userBirthDate = profileModel.get('userBirthDate');

        return {
            image: profileModel.get('imageUrl'),
            title: profileModel.get('displayName') + ' | Planeta',
            description: _.template("<%=displayName%><%=age%><%=city%>. Подробная информация доступна для авторизованных пользователей. Зарегистрируйтесь, чтобы найти новых друзей на Planeta.ru", {
                displayName: profileModel.get('displayName'),
                age: !cityName ? "" : ", " + cityName,
                city: !userBirthDate ? "" : ", " + StringUtils.declOfNumWithNum(DateUtils.getAge(userBirthDate), ["год", "года", "лет"])
            })
        };
    }
});

User.Views.Projects = BaseView.extend({
    template: '#user-projects-template',

    initialize: function (options) {
        var self = this;
        this.profileModel = options.contentModel.get('profileModel');
        this.model = this.profileModel.clone();

        BaseView.prototype.initialize.apply(this, arguments);
        this.model.set({
            showBackedProjects: false,
            showCreatedProjects: false,
            showRandomProjects: false,
            showEmpty: false,
            showPreload: true
        });


        var createdProjectCollection, backedProjectCollection;
        this.firstCollection = createdProjectCollection = new User.Models.Projects.CreatedProjectColection([], {
            data: {
                profileId: this.model.get("profileId")
            }
        });
        createdProjectCollection.load().done(function () {
            self.model.set("showCreatedProjects", createdProjectCollection.length !== 0);
            self.showEmpty();
        });
        this.createdProjectList = new User.Views.Projects.CreatedProjectList({
            collection: createdProjectCollection
        }).insertInto(this);

        this.backedProjectCollectionNeeds = this.model.get("profileType") === "USER" && (workspace.appModel.isCurrentProfileMine() || PrivacyUtils.hasAdministrativeRole() || this.model.get('showBackedCampaigns'));

        if (this.backedProjectCollectionNeeds) {
            this.secondCollection = backedProjectCollection = new User.Models.Projects.BackedProjectColection([], {
                data: {
                    profileId: this.model.get("profileId")
                }
            });
            backedProjectCollection.load().done(function () {
                if (backedProjectCollection.length === 0 && workspace.appModel.isCurrentProfileMine()) {
                    self.addRandomProjects();
                } else {
                    self.model.set("showBackedProjects", backedProjectCollection.length !== 0);
                    self.showEmpty();
                }
            });
            this.backedProjectList = new User.Views.Projects.BackedProjectList({
                collection: backedProjectCollection
            }).insertInto(this);
        }

    },

    addRandomProjects: function () {
        var self = this;
        var randomCollection = new User.Models.Projects.RandomProjectColection();
        this.secondCollection = randomCollection;
        randomCollection.load().done(function () {
            self.model.set("showRandomProjects", randomCollection.length !== 0);
            self.showEmpty();
        });
        this.addChildAtElement(".js-random-projects", new User.Views.Projects.RandonProjectList({
            collection: randomCollection
        }));
    },

    onShowBackedCampaignsClicked: function () {
        if (this.model.get('isSendShowBackedCampaigns')) {
            return;
        }
        this.isSendShowBackedCampaigns = true;
        var self = this;
        var isShowBackedCampaigns = !this.model.get('showBackedCampaigns');
        this.model.set({
            showBackedCampaigns: isShowBackedCampaigns,
            isSendShowBackedCampaigns: true
        });


        var fail = function () {
            self.model.set('showBackedCampaigns', !isShowBackedCampaigns);
            workspace.appView.showMessageUnexpectedError();
        };
        Backbone.sync('update', null, {
            url: "/api/profile/set-is-show-backed-campaigns.json",
            data: {
                isShowBackedCampaigns: isShowBackedCampaigns
            }
        }).done(function (result) {
            if (!result || result.success === false) {
                fail();
            } else {
                self.profileModel.set('showBackedCampaigns', isShowBackedCampaigns, {silent: true});
            }
        }).always(function () {
            self.model.set('isSendShowBackedCampaigns', false);
        }).fail(fail);
    },

    showEmpty: function () {
        var someCollectionVisible = this.model.get('showCreatedProjects') || this.model.get('showBackedProjects') || this.model.get('showRandomProjects');
        this.model.set('showPreload', !someCollectionVisible);

        if (!this.backedProjectCollectionNeeds) {
            this.model.set({
                showEmpty: !someCollectionVisible,
                showPreload: false
            });
        } else {
            var loadedAllYet = this.firstCollection.loading === false && this.secondCollection.loading === false;
            if (loadedAllYet) {
                this.model.set({
                    showEmpty: !someCollectionVisible,
                    showPreload: false
                });
            }
        }
    }
});

User.Models.Projects.CreatedProjectItem = BaseModel.extend({
    /**
     * Remove specified campaign
     * Returns deferred object
     * @param campaignId
     */
    removeCampaign: function (campaignId) {
        var collection = this.collection;
        var self = this;
        var data = {
            campaignId: campaignId || this.get('campaignId')
        };
        var $dfd = $.Deferred();
        var options = {
            url: '/admin/campaign-remove.json',
            data: data,
            success: function (response) {
                if (response.success === true) {
                    collection.remove(self);
                    $dfd.resolve(response.result);
                } else {
                    $dfd.reject(response.errorMessage, response.fieldErrors);
                }
            },
            error: function () {
                $dfd.reject('Ошибка удаления проекта');
            }
        };
        Backbone.sync('update', null, options);
        return $dfd.promise();
    }
});

User.Models.Projects.Statistic = BaseModel.extend({
    initialize: function (options) {
        this.campaignId = options.campaignId
    },

    url: function () {
        return '/api/campaign/get-short-campaign-stat.json?campaignId=' + this.campaignId
    }
});

User.Views.Projects.Statistic = BaseView.extend({
    template: '#user-created-project-short-stat-template'
});

User.Models.Projects.CreatedProjectColection = BaseCollection.extend({
    url: '/api/campaign/get-campaign-list-with-count.json',
    model: User.Models.Projects.CreatedProjectItem,
    limit: 3
});

User.Views.Projects.CreatedProjectItem = BaseView.extend({
    template: '#user-created-project-item-template',
    onDelete: function () {
        var self = this;
        Modal.showConfirm("Вы действительно хотите удалить проект?", "Подтверждение действия", {
            success: function () {
                self.model.removeCampaign();
            }
        });
    },

    construct: function(){
        var self = this;
        if(self.model.get('status') === 'ACTIVE') {
        // if (false) {
            var statistic = new User.Models.Projects.Statistic({
                campaignId: self.model.get('campaignId'),
                webCampaignAlias: self.model.get('webCampaignAlias')
            });
            statistic.fetch().done(function () {
                var view = new User.Views.Projects.Statistic({model: statistic});
                self.addChildAtElement('.js-statistics', view);
            });
        }
    }
});

User.Views.Projects.CreatedProjectList = BaseListView.extend({
    className: 'n-own-project_list',
    pagerTemplate: '#user-backed-project-item-pager-template',
    itemViewType: User.Views.Projects.CreatedProjectItem,
    onPagerClicked: function () {
        this.collection.limit = 0;
        BaseListView.prototype.onPagerClicked.apply(this, arguments);
    }
});


User.Models.Projects.BackedProjectColection = BaseCollection.extend({
    url: '/api/campaign/get-backed-campaign-list.json',
    limit: 9
});


User.Views.Projects.BackedProjectItem = BaseView.extend({
    template: '#user-backed-project-item-template',

    construct: function() {
        if (workspace) {
            if(workspace.currentLanguage) {
                this.model.set('lang', workspace.currentLanguage);
            }
        }
    }
});

User.Views.Projects.BackedProjectList = DefaultScrollableListView.extend({
    className: 'n-support-project_list cf',
    itemViewType: User.Views.Projects.BackedProjectItem
});


User.Models.Projects.RandomProjectColection = BaseCollection.extend({
    //Берем три рандомных проекта из карусели
    url: '/api/welcome/dashboard-filtered-campaigns.json?status=ACTIVE',
    limit: 12,
    parse: function (result) {
        if (result && result.length > 3) {
            var newResult = [];
            var i;
            for (i = 0; i < 3; i++) {
                var idx = Math.floor(Math.random() * result.length);
                newResult.push(result[idx]);
                result.splice(idx, 1);
            }
            return newResult;
        }
        return result;
    }
});

User.Views.Projects.RandonProjectList = DefaultScrollableListView.extend({
    className: "n-support-project_list cf",
    itemViewType: User.Views.Projects.BackedProjectItem
});


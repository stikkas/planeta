CampaignEdit.Models.CreatedSuccessful = CrowdFund.Models.CampaignContactsEdit.Email.extend({
    initialize: function () {
        var self = this;
        CrowdFund.Models.CampaignContactsEdit.Email.prototype.initialize.apply(this, arguments);
        var data = {data: { objectId: this.get("campaignId") ? this.get("campaignId") : this.get("objectId") }};
        Backbone.sync('get', {url: '/api/campaign/campaign.json'}, data).done(function (result) {
            self.set('campaign', result);
        });
    },

    pageData: function () {
        var self = this;
        return {
            image: ImageUtils.getThumbnailUrl(self.get('campaign').imageUrl, {
                        useProxy: true,
                        fileName: 'original',
                        width: 270,
                        height: 164,
                        crop: true,
                        pad: false,
                        disableAnimatedGif: true
                    }, ImageType.PHOTO),
            title: 'Проект «' + self.get('campaign').name + '» отправлен на модерацию Planeta.ru',
            description: 'В течение 3 рабочих дней менеджер Planeta рассмотрит заявку, подготовит комментарии и предложения по доработке проекта.'
        };
    }
});
/*global Shop, CampaignDonate*/
Shop = Shop || {};
Shop.Views = Shop.Views || {};

Shop.Views.DeliveryCitySection = BaseView.extend({
    template: "#product-delivery-city-template"
});

Shop.Views.DeliveryAddressSection = BaseView.extend({
    template: "#product-delivery-address-template",
    afterRender: function () {
    }
});

Shop.Views.DeliveryServicesItem = BaseView.extend({
    template: '#product-delivery-item-template',
    className: 'shop-order-delivery-type_i',
    viewEvents: {
        'click': 'productDeliverySelected'
    }
});

Shop.Views.DeliveryServicesList = BaseListView.extend({
    itemViewType: Shop.Views.DeliveryServicesItem
});


Shop.Views.DeliveryContactSection = CampaignDonate.Views.DeliveryAddressSection.extend({
    template: "#product-selectCampaignById-delivery-contacts-template",
    className: "",
    construct: function () {
        this.addChildAtElement(".js-delivery-address-container", new Shop.Views.DeliveryAddressContainer({
            controller: this,
            model: this.model
        }), true);

        this.addChildAtElement(".js-anchor-delivery-city", new Shop.Views.DeliveryCitySection({
            controller: this,
            model: this.model
        }), true);


        this.addChildAtElement(".js-anchor-delivery-address", new Shop.Views.DeliveryAddressSection({
            controller: this,
            model: this.model
        }), true);
    }
});


Shop.Views.DeliveryAddressContainer = CampaignDonate.Views.SharePaymentAndDeliveryPage.extend({
    template: "#product-delivery-page-template",
    className: "shop-order-form_fieldset fieldset",
    isExistsPlanetaUiElement: true,
    viewEvents: {
        'result input[name="customerContacts.city"]': 'onFieldChange',
        'change input[name="customerContacts.city"]': 'onLocationChange',
        'change select[name="customerContacts.countryId"]': 'onLocationChange',
        'change input.js-anchr-address-cityId': 'onLocationChange',
        'change input[name]': 'onFieldChange'
    },
    events: {
        'productDeliverySelected': 'productDeliveryMethodChanged'
    },
    selectNewDeliveryService: function (newService) {
        if (this.currentDeliveryService) {
            this.currentDeliveryService.set({
                isSelected: false
            });
        }
        this.currentDeliveryService = newService;

        //this.$(".js-error-not-exists-delivery").toggle(!newService);

        if (!newService) {
            this.model.set({
                serviceId: null,
                deliveryType: null,
                deliveryPrice: 0
            }, {silent: true});
            return;
        }

        this.currentDeliveryService.set({
            isSelected: true
        });
    },
    productDeliveryMethodChanged: function (e, modelOrView) {
        var model;
        if (modelOrView instanceof BaseView) {
            model = modelOrView.model;
        } else if (modelOrView instanceof BaseModel) {
            model = modelOrView;
        }
        this.selectNewDeliveryService(model);

        var deliveryPrice = this.currentDeliveryService.get('price');
        var serviceType = this.currentDeliveryService.get('serviceType');
        var location = this.currentDeliveryService.get('location');

        var customerContacts = this.model.get("customerContacts");
        if (location && location.countryId && location.countryId > 0) {
            customerContacts.countryId = location.countryId;
            customerContacts.disableCountry = true;
        } else {
            customerContacts.disableCountry = false;
        }

        if (location && location.locationType && location.locationType == "CITY") {
            customerContacts.city = location.name || "";
            customerContacts.disableCity = true;
        } else {
            customerContacts.disableCity = false;
        }

        var donateAmount = this.model.get('donateAmount');
        this.model.set({
            serviceId: this.currentDeliveryService.get('serviceId'),
            delServiceId: this.currentDeliveryService.get('serviceId'),
            deliveryType: serviceType,
            deliveryPrice: deliveryPrice,
            description: this.currentDeliveryService.get('description'),
            address: this.currentDeliveryService.get('address') || {}
        });

        this.model.get('payment').setPrice(deliveryPrice + donateAmount);
    },
    construct: function () {
        var self = this;

        self.deliveryServices = self.addChildAtElement('.js-anchor-delivery-list', new Shop.Views.DeliveryServicesList({
            collection: self.model.get('linkedDeliveriesCollection')
        }));


        var delServiceId = self.model.get('delServiceId');
        if (delServiceId != undefined) {
            var currentDelivery = self.deliveryServices.collection.find(function (model) {
                return model.get('serviceId') === self.model.get('delServiceId');
            });
            if (currentDelivery) {
                this.deliveryMethodChanged(null, currentDelivery);
            }
        }
    }
});


Shop.Views.PromoCodeSection = BaseView.extend({
    template: "#product-promo-code-template",
    events: {
        "click .js-promo-button": "onCheckPromoCodeClick",
        "keyup .js-promo-input": "onPromoCodeChanged",
        "change .js-promo-input": "onPromoCodeChanged"
    },
    onPromoCodeChanged: _.debounce(function (e) {
        var promoCode = this.$(".js-promo-input").val().trim();
        this.model.set({promoCode: promoCode}, {silent: true});
    }, 500),
    onCheckPromoCodeClick: function (e) {
        var promoCode = this.$(".js-promo-input").val().trim();

        if (!promoCode) {
            workspace.appView.showErrorMessage("Вы не ввели промокод");
            return;
        }

        this.model.applyPromoCode(promoCode);
    }
});


Shop.Views.Payment = CampaignDonate.Views.Payment.extend({
    template: '#product-payment-block-template',
    className: '',
    isCashEnabled: function () {
        // now cash is disabled for products
        /*
        var parent = this.model.controller;
        var hasPermitedCashProducts = parent.hasProductWithDisabledCash();
        return CampaignDonate.Views.Payment.prototype.isCashEnabled.call(this) && !hasPermitedCashProducts;
        */
        return false;
    }
});

Shop.Views.OrderBlock = BaseView.extend({
    template: '#product-purchase-payment-order-template',
    className: 'shop-confirm_i'
});

Shop.Views.ProductSelectDeliveryPage = CampaignDonate.Views.SharePaymentAndDeliveryPage.extend({
    template: '#product-purchase-contacts-page-template',
    events: {
        'onLocationChange': 'onLocationChange',
        'onFieldChange': 'silentUpdateField',
        'change [name]': 'silentUpdateField',
        'click .js-submit': 'submitContacts',
        'click .js-return-to-share-selection': 'returnToShareSelection',
        'click .js-return-to-shopping-cart': 'returnToSoppingCart',
        'deliverySelected': 'deliveryMethodChanged',
        'afterPaymentGroupChange': 'afterPaymentGroupChange'
    },
    submitContacts: function (e) {
        e.preventDefault();

        var data = {
            'customerName': this.model.get("customerName") || '',
            'customerId': this.model.get("profileId") || '-1',
            'email': this.model.get("email") || '',
            'comment': this.model.get('comment'),
            'phoneNumber': this.model.get("phoneNumber") || '',
            'serviceId': this.model.get("serviceId") || 0,
            'deliveryType': this.model.get("deliveryType") || 'NOT_SET',
            'deliveryPrice': this.model.get("deliveryPrice") || 0,
            'onlyDigitalProducts': this.model.get("onlyDigitalProducts") || false,
            'customerContacts.countryId': this.model.get("customerContacts").countryId || 0,
            'customerContacts.city': this.model.get("customerContacts").city || '',
            'customerContacts.street': this.model.get("customerContacts").street || '',
            'customerContacts.zipCode': this.model.get("customerContacts").zipCode || ''
        };
        var self = this;
        var options = {
            url: '/payment/shopping-cart-contacts.json',
            data: $.param(data),
            success: function (response) {
                if (response) {
                    if (response.success) {
                        window.location.href = '/payment/shopping-cart-payment';
                    } else {
                        Form.isValid(response, 'js-contacts-block', false, 'fieldset');
                        var hasErrorEmail = false;
                        _(response.fieldErrors).each(function (errorText, fieldName) {
                            if (fieldName == "email") {
                                if (errorText === "Пользователь с таким e-mail уже зарегистрирован") {
                                    hasErrorEmail = true;
                                    var field = self.$('[name="' + fieldName + '"]');
                                    self.model.backupAll();
                                    LazyHeader.showAuthForm('signup', {
                                        email: field.val(),
                                        focusPassword: true
                                    });
                                } else if (errorText == "session expired") {
                                    hasErrorEmail = true;
                                    self.model.backupAll();
                                    LazyHeader.showAuthForm('signup', {
                                        email: workspace.appModel.get("myProfile").get("email") || "",
                                        focusPassword: true
                                    });
                                }
                                return false;
                            }
                        });
                        if (!hasErrorEmail && response.errorMessage) {
                            workspace.appView.showErrorMessage(response.errorMessage);
                        }
                    }
                }
            }
        };
        return Backbone.sync('update', this, options);
    },
    construct: function () {
        var serviceId = this.model.get("serviceId");
        if (serviceId) {
            this.model.set('delServiceId', serviceId);
        }
        this.addChildAtElement('.js-contacts-block', new Shop.Views.DeliveryContactSection({
            controller: this,
            model: this.model
        }));

        var cartItems = this.model.get("shoppingCartModel").get("cartItems").toJSON();
        var physicalProduct = _.find(cartItems, function (item) {
            return _.isEqual(item.productCategory, 'PHYSICAL');
        });
        this.model.set({onlyDigitalProducts: _.isUndefined(physicalProduct)});
    }
});

Shop.Views.ProductSelectPaymentPage = CampaignDonate.Views.SharePaymentAndDeliveryPage.extend({
    template: '#product-purchase-payment-page-template',
    events: {
        'click .js-submit': 'submitPayment'
    },
    submitPayment: function (e) {
        e.preventDefault();
        var action = '/payment/shopping-cart-payment.json';
        var data = {
            promoCodeId: this.model.get("promoCodeModel").get("promoCodeId") || 0,
            promoCode: this.model.get("promoCodeModel").get("promoCode") || '',
            paymentMethodId: this.model.get("payment").get("paymentMethodId") || 0,
            paymentType: this.model.get("payment").get("paymentType") || 'PAYMENT_SYSTEM',
            planetaPurchase: this.model.get("payment").get("planetaPurchase") || false,
            paymentPhone: this.model.get("payment").get("phone"),
            needPhone: this.model.get("payment").get("needPhone"),
            amount: this.model.get("payment").get("amount"),
            donateAmount: this.model.get("donateAmount"), // need on last page
            discountAmount: this.model.get("discountAmount"), // need on last page
            deliveryPrice: this.model.get("deliveryPrice")      // promocode may change delivery price
        };
        var self = this;
        var options = {
            url: action,
            data: $.param(data),
            success: function (response) {
                if (response) {
                    if (response.success) {
                        window.location.href = '/payment/shopping-cart-confirm';
                    } else {
                        Form.isValid(response, 'js-contacts-block', false, 'fieldset');
                        workspace.appView.showErrorMessage('Проверьте введённые данные');
                    }
                }
            }
        };
        return Backbone.sync('update', this, options);
    },
    construct: function () {
        this.addChildAtElement(".js-promo-code-block", new Shop.Views.PromoCodeSection({
            model: this.model.get('promoCodeModel')
        }));

        this.addChildAtElement('.js-project-payment-details-container', new Shop.Views.Payment({
            model: this.model.get('payment'),
            controller: this
        }), true);
    }
});
/*globals Comments, TinyMcePlaneta*/
var News = {
    AVAILABLE_POST_LENGTH_WITHOUT_CUT: 400
};
News.Models = {};

News.Views = {
    _tinyViewInited: false,
    initTinyView: function (callback) {
        News.Views._tinyViewInited = callback;
    },
    disableTinyView: function () {
        if (_.isFunction(News.Views._tinyViewInited)) {
            News.Views._tinyViewInited();
        }
        News.Views._tinyViewInited = false;
    }
};

News.Models.Post = BaseModel.extend({
    url: null,
    defaults: {
        timeAdded: new Date().getTime(),
        authorAlias: '',
        authorImageUrl: '',
        headingText: '',
        postName: '',
        text: ''
    },

    newsToPostMapping: {
        id: 'objectId',
        profileId: 'profileId',
        campaignId: 'campaignId',
        title: 'postName',
        postText: 'text',
        timeAdded: 'timeAdded',
        timeUpdated: 'timeUpdated',
        headingText: 'headingText',
        notificationTime: 'notificationTime',
        eventDate: 'eventDate',
        eventLocation: 'eventDate',
        tagId: 'eventDate',
        imageUrl: 'eventDate'
    },

    initialize: function (options) {
        BaseModel.prototype.initialize.call(this, options);
        if (this.get('type') === 'POST') {
            this.comments = this.createComments(3);
            this.set({
                commentsCount: this.get('commentsCount')
            });
            if (!this.isNew()) {
                this.comments.fetch();
            }
        }
    },

    createComments: function (limit) {
        return new Comments.Models.Comments({
            profileId: this.get('authorProfileId'),
            objectId: this.get('postId'),
            objectType: 'POST',
            limit: limit || 0,
            showCounters: false
        }, {
            commentableObject: this
        });
    },

    newsAsPost: function () {
        var self = this;
        var data = {};

        _.each(this.newsToPostMapping, function (newsAttrName, postAttrName) {
            data[postAttrName] = self.get(newsAttrName) || self.defaults[newsAttrName];
        });
        console.log(data);
        return data;
    },

    save: function (title, postText) {
        var isNew = this.isNew();
        this.set({
            postName: title,
            text: postText
        }, {silent: true});

        var self = this;
        var $dfd = $.Deferred();
        var options = {
            url: isNew ? '/api/profile/news/add-post.json' : '/api/profile/news/change-post.json',
            data: this.newsAsPost(),
            success: function (response) {
                if (response.success) {
                    var result = response.result;
                    self.set(result);
                    if (isNew) {
                        self.collection.add(self.clone(), {at: 0});
                        Backbone.GlobalEvents.trigger('news-added');
                    }
                    $dfd.resolve(result);
                } else {
                    self.set({errors: response.fieldErrors});
                    $dfd.reject(response.errorMessage, response.fieldErrors);
                }
            }
        };
        Backbone.sync('create', this, options);
        return $dfd.promise();
    },

    remove: function () {
        var self = this;
        var $dfd = $.Deferred();
        var options = {
            url: '/api/profile/news/deleteByProfileId-post.json',
            success: function (response) {
                if (response.success) {
                    var result = response.result;
                    self.collection.removeById(self.get('id'));
                    $dfd.resolve(result);
                } else {
                    self.set({errors: response.fieldErrors});
                    $dfd.reject(response.errorMessage, response.fieldErrors);
                }
            }
        };
        Backbone.sync('delete', this, options);
        return $dfd.promise();
    },

    startSpam: function () {
        var self = this;
        var $dfd = $.Deferred();
        var options = {
            url: '/api/profile/send-campaign-news-notification.json',
            data: {
                profileId: this.get('profileId'),
                postId: this.get('objectId'),
                campaignId: this.get('campaignId')
            },
            success: function (response) {
                if (response.success) {
                    self.set({
                        canSendSpam: false,
                        notificationTime: new Date().getTime()
                    });
                    $dfd.resolve(response.result);
                } else {
                    $dfd.reject(response.errorMessage, response.fieldErrors);
                }
            }
        };

        Backbone.sync('update', this, options);
        return $dfd.promise();
    }
});

News.Models.SinglePost = News.Models.Post.extend({
    url: function () {
        return '/api/campaign/profile-post.json?' + $.param({postId: this.get('postId')});
    },

    parse: function (response) {
        if (response && response.success) {
            var result = response.result;
            return _.extend(result.newsPost, {
                canEdit: result.canEdit,
                canSendSpam: result.canSendSpam,
                notificationDaysLimit: result.notificationDaysLimit
            });
        }
        workspace.appView.showErrorMessage(response.errorMessage);
    }
});

News.Models.SinglePostPage = News.Models.SinglePost.extend({
    initialize: function (options) {
        News.Models.Post.prototype.initialize.call(this, options);
        var profileId = workspace.appModel.get('profileModel').get('profileId');
        var authorProfileId = workspace.appModel.get('profileModel').get('creatorProfileId') || profileId;
        //var campaignId = workspace.appModel.get('contentModel').get('campaignId') || 0;

        this.set({
            postId: this.get('objectId'),
            type: 'POST',
            authorProfileId: authorProfileId,
            //  campaignId: campaignId,
            profileId: profileId
        });
    },

    parse: function (response) {
        var parseResult = News.Models.SinglePost.prototype.parse.call(this, response);
        if (parseResult) {
            return parseResult;
        }
        return BaseModel.prototype.parse.call(this, response);
    }
});

News.Models.Posts = BaseCollection.extend({
    url: '/api/public/news.json',
    model: News.Models.Post,
    limit: 0,
    offset: 0,
    data: [],
    newsType: '',

    createPost: function (campaignId) {
        var profile = workspace.appModel.get('profileModel');
        return new News.Models.Post({
            type: 'POST',
            timeAdded: new Date().getTime(),
            profileId: profile.get('profileId'),
            authorImageUrl: profile.get('imageUrl'),
            campaignId: campaignId || 0
        }, {
            collection: this
        });
    }
});

News.Models.TinyEditor = function (parentModel, $element) {
    var config = TinyMcePlaneta.getNewsPostEditorConfiguration();
    if ( $element ) {
        var parent = $element.closest('.post-write');
        if ( parent.length ) {
            if ( parent.width() <= 710 ) {
                config = TinyMcePlaneta.getNewsPostEditorConfiguration710();
            }
            if ( parent.width() <= 480 ) {
                config = TinyMcePlaneta.getNewsPostEditorConfiguration480();
            }
        }
    }

    _.extend(this, new TinyMcePlaneta.EditorModel(parentModel), {
        config: config,
        height: 400,
        width: 590,
        saveToParentModel: function () {
            this.parentModel.set({
                postText: this.html()
            }, {silent: true});
        }
    });
};
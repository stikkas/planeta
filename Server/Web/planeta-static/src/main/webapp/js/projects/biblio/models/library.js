Biblio.Models.Library = BaseModel.extend({
	changeSelected: function() {
		if(this.get('selected')){
			if (Biblio.data.bin){
                Biblio.data.bin.delLibrary(this);
                Biblio.data.bin.save();
            }
		} else {
			if (Biblio.data.bin){
                Biblio.data.bin.addLibrary(this);
	            Biblio.data.bin.save();
			}
		}
		this.set('selected', !this.get('selected'));
	}, construct: function() {
		this.data = {query: ''};
	}
});


Biblio.Models.ViewLibrary = Biblio.Models.Library.extend({
	
});

Biblio.Models.MapLibrary = Biblio.Models.Library.extend({
	
});

Biblio.Models.BaseLibraries = BaseCollection.extend({
    add: function(models) {
    	if(Biblio.data.bin) {
    		_.each(models, function(value) {
				value.selected = false;
				value.libraryId = value.libraryId||-1;
    			if(_.find(Biblio.data.bin.get('libraries'), function(v) {return v.get('libraryId') == value.libraryId;})){
    				value.selected = true;
    			}
    		});
    	}
    	BaseCollection.prototype.add.apply(this, arguments);
    },
    comparator: function(libraryOne, libraryTwo) {
    	return libraryOne.get('selected') >= libraryTwo.get('selected') ? -1 : 1;
    }
});

Biblio.Models.ViewLibraries = Biblio.Models.BaseLibraries.extend({
    model: Biblio.Models.ViewLibrary,
    url: '/library/library-search.json'
});

Biblio.Models.MapLibraries = Biblio.Models.BaseLibraries.extend({
    model: Biblio.Models.MapLibrary,
    url: '/library/map/library-search.json'
});

Biblio.Models.LibraryFilter = BaseModel.extend({
	
});
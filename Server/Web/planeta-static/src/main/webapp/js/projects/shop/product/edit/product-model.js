/*globals ProductStorages, Shop, ShopUtils, UploadController, Form*/

var ProductEdit = {};
ProductEdit.Models = {};
ProductEdit.Models.ChildProducts = BaseCollection.extend({
    comparator: function (model1, model2) {
        return (model1.id == model2.id) ? 0 : (model1.id > model2.id) ? 1 : -1;
    },
    model: ProductStorages.Model
});

ProductEdit.Models.Product = BaseModel.extend({
    idAttribute: 'productId',
    predefinedAttributes: {},

    initialize: function () {
        this._initGoodsCategory();
        this._initPhotos();
        this._initChilds();
    },

    _initGoodsCategory: function () {
        if (!this.has("category")) {
            this.set('category', ShopUtils.getDefaultProductType());
        }
        if (!this.has("productAttribute")) {
            this.set('productAttribute', {});
        }
    },

    /**********************************
     * Photos manipulation
     ***********************************/

    _initPhotos: function () {
        this.photos = new BaseCollection([]);
        if (this.get('productState') == 'GROUP') {
            var imageUrls = this.get('coverImageUrl') && [this.get('coverImageUrl')] || [];
        } else {
            imageUrls = this.get('imageUrls') || [];
        }
        var self = this;
        _.each(imageUrls, function (imageUrl) {
            self.photos.add(new BaseModel({imageUrl: imageUrl}));
        });

        // set cover image url
        if (!this.get('coverImageUrl') && this.photos.length > 0) {
            this.selectPhoto(this.photos.at(0));
        }
        var selectedPhoto = this.photos.findByAttr('imageUrl', this.get('coverImageUrl'));

        if (selectedPhoto) {
            selectedPhoto.set({
                selected: true
            });
        }
        // append upload photo item
        var uploadPhoto = new BaseModel({upload: true});
        this.photos.add(uploadPhoto);
    },

    selectPhoto: function (model) {
        if (model.get('selected')) {
            return;
        }
        this.photos.each(function (photo) {
            var selected = model.get('imageUrl') == photo.get('imageUrl');
            photo.set({
                selected: selected
            });
            if (selected) {
                this.set({
                    coverImageUrl: photo.get('imageUrl')
                }, {silent: true});
            }
        }, this);
    },

    swapPhotos: function(startIndex, stopIndex) {
        var swap = this.photos.models[startIndex];
        if (startIndex < stopIndex) {
            for (var i=startIndex; i<stopIndex; i++) {
                this.photos.models[i] = this.photos.models[i+1];
            }
            this.photos.models[stopIndex] = swap;
        } else {
            for (var i=startIndex; i>stopIndex; i--) {
                this.photos.models[i] = this.photos.models[i-1];
            }
        }
        this.photos.models[stopIndex] = swap;
    },

    deletePhoto: function (photo) {
        this.photos.remove(photo);
        if (this.get('coverImageUrl') == photo.get('imageUrl')) {
            this.unset('coverImageUrl', {silent: true});
        }

        if (!this.get('coverImageUrl') && this.photos.length > 1) {
            this.selectPhoto(this.photos.at(0));
        }
    },

    savePhotos: function (result) {
        var photos = _.compact(result.pluck('uploadResult'));
        if (photos && photos.length > 0) {
            var position = this.photos.length - 1;
            this.photos.add(photos, {
                at: position
            });
        }
        if (!this.get('coverImageUrl')) {
            this.selectPhoto(this.photos.at(0));
        }
    },
    /**
     * for product-group only
     */
    uploadPhoto: function () {
        var self = this;
        var onUploadComplete = function (result) {
            if (result.status && result.status == 'ERROR') {
                workspace.appView.showErrorMessage('Произошла ошибка при загрузке фото.');
                return;
            }
            var photos = _.compact(result.pluck('uploadResult'));
            if (photos && photos.length > 0) {

                photos[0].selected = false;
                if (self.photos.length == 1) {
                    self.photos.add(photos[0], {at: 0});
                } else {
                    self.photos.at(0).set(photos[0]);
                }
            }

            self.selectPhoto(self.photos.at(0));

        };

        UploadController.showUploadGroupProductImage(this.get('merchantProfileId'), this.get('albumId') || 0, onUploadComplete);
    },

    uploadTableImage: function () {
        var self = this;
        var onUploadComplete = function (result) {
            if (result.status && result.status == 'ERROR') {
                workspace.appView.showErrorMessage('Произошла ошибка при загрузке фото.');
                return;
            }
            var photos = _.compact(result.pluck('uploadResult'));
            if (photos && photos.length > 0) {
                self.set({
                    tableImageUrl: photos[0].imageUrl
                });
            }
        };

        UploadController.showUploadProductTableImage(this.get('merchantProfileId'), this.get('albumId') || 0, onUploadComplete);
    },

    /**********************************
     * Product attributes |child products
     ***********************************/

    _initChilds: function () {
        if (!this.has('childrenProducts')) {
            this.set({childrenProducts: []}, {silent: true});
        }
    },

    addAttribute: function (attr, contentUrlsString) {
        if (_.isUndefined(attr)) return $.Deferred().reject();

        var attrValue;
        var attrIndex;

        if (_.isObject(attr)) {
            attrValue = attr.value;
            attrIndex = attr.index;
            contentUrlsString = attr.contentUrlsString;
        } else {
            attrValue = attr;
            attrIndex = 0;
            _(this.get('childrenProducts')).each(function (model) {
                if (model.index > attrIndex) {
                    attrIndex = model.index;
                }
            });
            _(this.predefinedAttributes[this.get('category').categoryId]).each(function (item) {
                if (item.index > attrIndex) {
                    attrIndex = item.index;
                }
            });

            attrIndex++;
        }
        var sameAttrValue = _(this.get('childrenProducts')).find(function (child) {
            return child.productAttribute.value.toLowerCase() == attrValue.toLowerCase();
        }, this);
        if (sameAttrValue) {
            return $.Deferred().reject();
        }

        var childParams = {
            productAttribute: {
                value: attrValue,
                index: attrIndex
            },
            productId: 0,
            parentProductId: this.get('productId'),
            contentUrlsString: contentUrlsString,
            totalQuantity: 0
        };

        this.get('childrenProducts').push(childParams);
        this.trigger('change');
        return $.Deferred().resolve();
    },

    isClothes: function () {
        var tags = this.get("tags");
        var result = null;
        _.each(tags, function (el) {
            if (el.categoryId == 1) {
                result = el;
            }
        });

        return result;
    },


    saveProduct: function (data) {
        return this._persistProduct(data);
    },

    deleteProduct: function (productId, options) {
        options = options || {};
        var success = options.success,
            error = options.error;
        var products = this.get("childrenProducts") || [];

        var product = _.find(products, function (productItem) {
            return productItem.productId == productId;
        });

        var self = this;

        var updateOptions = {
            url: '/admin/product-deleteByProfileId.json',
            data: {
                productId: productId
            },
            success: function (response) {
                if (response.success) {
                    if (_.isFunction(success)) {
                        success();
                    }
                    products.splice(_.indexOf(products, product), 1);
                    self.set({childrenProducts: products});
                    self.change();
                } else if (_.isFunction(error)) {
                    error();
                }
            },
            error: error
        };
        return Backbone.sync('update', this, updateOptions);
    },

    getData: function (options, section) {
        delete options.category;

        var data = _.extend({}, this.toJSON(), options || {});
        var childrenProducts = data.childrenProducts || [];
        if (childrenProducts.length > 0) {
            data.productState = 'META';
        }
        _(childrenProducts).each(function (product) {
            delete product.descriptionHtml;
        });
        data.description = data.descriptionHtml.trim();

        data.imageUrls = _.filter(_.pluck(this.photos.toJSON(), 'imageUrl'), function (imageUrl) {
            return !_.isUndefined(imageUrl);
        });

        data.openSection = section || data.openSection;
        return data;
    },

    _persistProduct: function (data) {
        var dfd = $.Deferred();
        var options = {
            url: '/admin/product-save.json',
            contentType: 'application/json',
            data: this.getData(data || {}, 'preview'),
            context: this,
            success: function (response) {
                if (Form.isValid(response)) {
                    this.set(response.result, {silent: true});
                    dfd.resolveWith(this, [response.result]);
                } else {
                    workspace.appView.showErrorMessage(response.errorMessage);
                    dfd.rejectWith(this, [response.errorMessage]);
                }
            },
            error: function () {
                dfd.rejectWith(this, ["Товар не удалось сохранить"]);
            }
        };

        Backbone.sync('update', this, options);

        return dfd.promise();
    },

    addProductTagIfNotExists: function (tagId) {
        if (tagId) {
            var newTags = this.get('tags');
            var result = $.grep(newTags, function (e) {
                return e.categoryId == tagId;
            });
            if (result.length == 0) {
                newTags.push(ShopUtils.getProductTag(tagId));
                this.set({tags: newTags}, {silent: true});
            }
        }
    },

    removeProductTag: function (tagId) {
        if (tagId) {
            var newTags = this.get('tags');
            newTags = $.grep(newTags, function (e) {
                return e.categoryId != tagId;
            });
            this.set({tags: newTags}, {silent: true});
        }
    },

    changeProductCategory: function (newCategory, options) {
        this.set({'productCategory': newCategory}, options);
        // to avoid type inconsistency or overriding attribute on server
        this.unset('productCategoryTypeCode', options);
    },

    removeChildProduct: function (value) {
        var children = this.get('childrenProducts') || [];
        var childForRemove = _.find(children, function (child) {
            return child.productAttribute.value == value;
        });
        children.splice(children.indexOf(childForRemove), 1);
        this.trigger('change');
    }
});
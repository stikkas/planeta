var Quiz = {
    Collections: {},
    Models: {},
    Views: {}
};

Quiz.Collections.QuizQuestion = BaseCollection.extend({
    url: '/api/quiz/quiz-questions.json'
});

Quiz.Models.QuizQuestion = BaseModel.extend({
});

Quiz.Models.Base = BaseModel.extend({
    url: '/api/quiz/get-quiz.json',
    parse: BaseModel.prototype.parseActionStatus,
    defaults: _.extend({}, BaseModel.prototype.defaults, {
    }),
    initialize: function () {
        BaseModel.prototype.initialize.apply(this, arguments);
        this.set({quizAlias: this.get('objectId')});
        this.quizQuestions = new Quiz.Collections.QuizQuestion({
            data: {
                quizAlias: this.get('objectId')
            },
            limit: 50
        });

    },

    fetch: function (options) {
        options = _.extend({
            data: {
                quizAlias: this.get('objectId')
            }
        }, options);
        return BaseModel.prototype.fetch.call(this, options);
    }
});
/*global Search,CampaignSearch,Account,Breadcrumbs,CrowdFund*/
/**
 * Application navigation configuration
 */
var NavigationMap = {
    User: {
        common: {
            defaults: {
                rightSidebarViewType: null,
                sidebarViewType: null,
                headerViewType: null,
                menuViewType: null,
                rightSidebarBannerType: null,
                contentExtraViewType: null
            },
            'welcome': {
                layout: 'wide-layout',
                module: 'welcome',
                modelType: 'Welcome.Models.WelcomeModel',
                contentViewType: 'Welcome.Views.WelcomeView',
                bodyCssClass: 'project-page grid-1170'
            },
            'search:users': {
                layout: 'users-search-layout',
                module: 'search-users',
                modelType: 'Search.Models.Search',
                contentViewType: 'UserSearch.Views.ContentView'
            },
            'search:users:object': {
                layout: 'users-search-layout',
                module: 'search-users',
                modelType: 'Search.Models.Search',
                contentViewType: 'UserSearch.Views.ContentView'
            },
            'search:projects': {
                layout: 'wide-layout',
                modelType: Search.Models.Search,
                contentViewType: CampaignSearch.Views.ContentView,
                contentExtraViewType: CampaignSearch.Views.BreadCrumbs,
                bodyCssClass: 'grid-1170 project-page'

            },
            'search:projects:object': {
                layout: 'wide-layout',
                modelType: Search.Models.Search,
                contentViewType: CampaignSearch.Views.ContentView,
                contentExtraViewType: CampaignSearch.Views.BreadCrumbs,
                bodyCssClass: 'grid-1170 project-page'
            },
            'search:shares': {
                layout: 'wide-layout',
                modelType: Search.Models.FakeModel,
                contentViewType: ShareSearch.Views.ContentView,
                bodyCssClass: 'project-page grid-1170'
            },
            'search:shares:object': {
                layout: 'wide-layout',
                contentViewType: ShareSearch.Views.ContentView,
                bodyCssClass: 'project-page grid-1170'
            },

            'search:blogs': {
                layout: 'wide-layout',
                module: 'news',
                modelType: 'News.Models.Search.Main',
                contentViewType: 'News.Views.Search.Page'

            },
            'search:blogs:object': {
                layout: 'wide-layout',
                module: 'news',
                modelType: 'News.Models.Search.Main',
                contentViewType: 'News.Views.Search.Page'
            }
        },
        main: {
            defaults: {

                'module': 'user-pages',
                'cardView': 'User.Views.Card',
                'infoView': 'User.Views.Info',
                'tabsView': 'User.Views.Tabs',
                'socialView': 'User.Views.Social',
                'backedProjectsStatView': 'User.Views.BackedProjectsStat',
                'adminActionsView': 'User.Views.AdminActions',
                'moderatorAlertView': 'User.Views.ModeratorAlert',
                'urlAnchorView': {
                    'info': 'User.Views.ModalInfo',
                    'subscriptions': 'user-subscribers:infoView showSubscriptionList',
                    'subscribers': 'user-subscribers:infoView showSubscriberList',
                    'increase_balance': 'infoView onIncreaseBalanceClicked'
                }
            },
            'info': {
                'contentView': 'User.Views.Projects',
                'modelType': 'User.Models.Projects',
                'bodyCssClass': 'grid-1170'
            },
            'news': {
                'module': 'user-pages,news',
                'contentView': 'User.Views.News',
                'modelType': 'User.Models.News',
                'bodyCssClass': 'grid-1170'
            },
            'news:object': {
                'cardView': null,
                'infoView': null,
                'tabsView': null,
                'socialView': null,
                'backedProjectsStatView': null,
                'adminActionsView': null,
                'moderatorAlertView': null,
                'urlAnchorView': null,
                layout: 'wide-layout',
                module: 'news',
                contentViewType: 'News.Views.PostPage',
                modelType: 'News.Models.SinglePostPage',
                'bodyCssClass': 'grid-1170'
            }
        },
        faq: {
            defaults: {
                module: 'faq',
                layout: 'faq-layout',
                bodyCssClass: 'faq-page',
                headerViewType: 'faq:FAQ.Views.SearchHeaderWithBreadCrumbs'
            },
            'faq': {
                modelType: 'FAQ.Models.Main',
                contentViewType: 'FAQ.Views.Main'
            },
            'faq:edit': {
                modelType: 'FAQ.Models.Main',
                contentViewType: 'FAQ.Views.Main',
                headerViewType: null
            },
            'faq:search': {
                modelType: 'FAQ.Models.Search',
                contentViewType: 'FAQ.Views.SearchResults'
            },
            'faq:search:object': {
                modelType: 'FAQ.Models.Search',
                contentViewType: 'FAQ.Views.SearchResults'
            },
            'faq:article:object': {
                headerViewType: 'FAQ.Views.BreadCrumbs',
                modelType: 'FAQ.Models.Article',
                contentViewType: 'FAQ.Views.Article'

            }
        },
        // account: {
        //     defaults: {
        //         layout: 'wide-layout',
        //         modelType: 'Account.Models.PersonalCabinet',
        //         contentViewType: 'Account.Views.PersonalCabinet'
        //     },
        //     'account:purchases': {},
        //     'account:billings': {}
        // },
        settings: {
            defaults: {
                'module': 'news,user-settings',
                'layout': 'user-settings-layout',
                'tabsView': 'User.Views.Settings.Tabs'
            },
            'settings:general': {
                contentView: 'User.Views.Settings.General'

            },
            'settings:contacts': {
                contentView: 'User.Views.Settings.Contacts',
                contentModel: 'User.Models.Settings.Contacts'
            },
            'settings:password': {
                contentView: 'User.Views.Settings.Password'
            }
        },
        campaigns: {
            'funding-rules': {
                layout: 'simple-page-layout',
                module: 'funding-rules',
                modelType: 'FundingRules.Model',
                contentViewType: 'FundingRules.View'
            },
            'delivery-payment': {
                module: 'campaign-donate',
                modelType: 'CommonDonate.Models.DeliveryPaymentPage',
                contentViewType: 'CampaignDonate.Views.DeliveryPaymentPage',
                bodyCssClass: 'project-page'
            }
        },
        interactive: {
            'interactive:start': {
                layout: 'interactive-wide-layout',
                module: 'interactive',
                headerViewType: 'CampaignEdit.Views.Interactive.Header',
                menuViewType: 'CampaignEdit.Views.Interactive.LessonMaterial',
                modelType: 'CampaignEdit.Models.Interactive.Start',
                contentViewType: 'CampaignEdit.Views.Interactive.Start'
            },
            'interactive:name': {
                layout: 'interactive-wide-layout',
                module: 'interactive',
                headerViewType: 'CampaignEdit.Views.Interactive.Header',
                menuViewType: 'CampaignEdit.Views.Interactive.LessonMaterial',
                modelType: 'CampaignEdit.Models.Interactive.Name',
                contentViewType: 'CampaignEdit.Views.Interactive.Name'
            },
            'interactive:email': {
                layout: 'interactive-wide-layout',
                module: 'interactive',
                headerViewType: 'CampaignEdit.Views.Interactive.Header',
                menuViewType: 'CampaignEdit.Views.Interactive.LessonMaterial',
                modelType: 'CampaignEdit.Models.Interactive.Email',
                contentViewType: 'CampaignEdit.Views.Interactive.Email'
            },
            'interactive:proceed': {
                layout: 'interactive-wide-layout',
                module: 'interactive',
                headerViewType: 'CampaignEdit.Views.Interactive.Header',
                menuViewType: 'CampaignEdit.Views.Interactive.LessonMaterial',
                modelType: 'CampaignEdit.Models.Interactive.Proceed',
                contentViewType: 'CampaignEdit.Views.Interactive.Proceed'
            },
            'interactive:info': {
                layout: 'interactive-wide-layout',
                module: 'interactive',
                headerViewType: 'CampaignEdit.Views.Interactive.Header',
                menuViewType: 'CampaignEdit.Views.Interactive.LessonMaterial',
                modelType: 'CampaignEdit.Models.Interactive.Info',
                contentViewType: 'CampaignEdit.Views.Interactive.Info'
            },
            'interactive:duration': {
                layout: 'interactive-wide-layout',
                module: 'interactive',
                headerViewType: 'CampaignEdit.Views.Interactive.Header',
                menuViewType: 'CampaignEdit.Views.Interactive.LessonMaterial',
                modelType: 'CampaignEdit.Models.Interactive.Duration',
                contentViewType: 'CampaignEdit.Views.Interactive.Duration'
            },
            'interactive:price': {
                layout: 'interactive-wide-layout',
                module: 'interactive',
                headerViewType: 'CampaignEdit.Views.Interactive.Header',
                menuViewType: 'CampaignEdit.Views.Interactive.LessonMaterial',
                modelType: 'CampaignEdit.Models.Interactive.Price',
                contentViewType: 'CampaignEdit.Views.Interactive.Price'
            },
            'interactive:video': {
                layout: 'interactive-video-wide-layout',
                module: 'interactive',
                headerViewType: 'CampaignEdit.Views.Interactive.Header',
                menuViewType: 'CampaignEdit.Views.Interactive.LessonMaterial',
                modelType: 'CampaignEdit.Models.Interactive.Video',
                contentViewType: 'CampaignEdit.Views.Interactive.Video',
                contentExtraViewType: 'CampaignEdit.Views.Interactive.WriteDownButton'
            },
            'interactive:description': {
                layout: 'interactive-video-wide-layout',
                module: 'interactive',
                headerViewType: 'CampaignEdit.Views.Interactive.Header',
                menuViewType: 'CampaignEdit.Views.Interactive.LessonMaterial',
                modelType: 'CampaignEdit.Models.Interactive.Description',
                contentViewType: 'CampaignEdit.Views.Interactive.Description',
                contentExtraViewType: 'CampaignEdit.Views.Interactive.WriteDownButton'
            },
            'interactive:reward': {
                layout: 'interactive-video-wide-layout',
                module: 'interactive',
                headerViewType: 'CampaignEdit.Views.Interactive.Header',
                menuViewType: 'CampaignEdit.Views.Interactive.LessonMaterial',
                modelType: 'CampaignEdit.Models.Interactive.Reward',
                contentViewType: 'CampaignEdit.Views.Interactive.Reward',
                contentExtraViewType: 'CampaignEdit.Views.Interactive.WriteDownButton'
            },
            'interactive:counterparty': {
                layout: 'interactive-video-wide-layout',
                module: 'interactive',
                headerViewType: 'CampaignEdit.Views.Interactive.Header',
                menuViewType: 'CampaignEdit.Views.Interactive.LessonMaterial',
                modelType: 'CampaignEdit.Models.Interactive.Counterparty',
                contentViewType: 'CampaignEdit.Views.Interactive.Counterparty',
                contentExtraViewType: 'CampaignEdit.Views.Interactive.WriteDownButton'
            },
            'interactive:check': {
                layout: 'interactive-wide-layout',
                module: 'interactive',
                headerViewType: 'CampaignEdit.Views.Interactive.Header',
                modelType: 'CampaignEdit.Models.Interactive.Check',
                contentViewType: 'CampaignEdit.Views.Interactive.Check'
            },
            'interactive:final': {
                layout: 'interactive-wide-layout',
                module: 'interactive',
                headerViewType: 'CampaignEdit.Views.Interactive.Header',
                modelType: 'CampaignEdit.Models.Interactive.FinalModeration',
                contentViewType: 'CampaignEdit.Views.Interactive.FinalModeration'
            },
            'interactive:final-draft': {
                layout: 'interactive-wide-layout',
                module: 'interactive',
                headerViewType: 'CampaignEdit.Views.Interactive.Header',
                modelType: 'CampaignEdit.Models.Interactive.FinalDraft',
                contentViewType: 'CampaignEdit.Views.Interactive.FinalDraft'
            }
        },
        quiz: {
            'quiz:quizId': {
                layout: 'quiz-page-layout',
                module: 'quiz',
                headerViewType: null,
                modelType: 'Quiz.Models.Base',
                contentViewType: 'Quiz.Views.Base'
            }
        }
    },
    Group: {
        main: {
            defaults: {
                'layout': 'user-profile-layout',
                'module': 'user-pages',
                'cardView': 'User.Views.Card',
                'infoView': 'User.Views.Info',
                'tabsView': 'User.Views.Tabs',
                'socialView': 'User.Views.Social',
                'backedProjectsStatView': 'User.Views.BackedProjectsStat',
                'adminActionsView': 'User.Views.AdminActions',
                'moderatorAlertView': 'User.Views.ModeratorAlert',
                'urlAnchorView': {
                    'info': 'User.Views.ModalInfo',
                    'subscriptions': 'user-subscribers:infoView showSubscriberList',
                    'subscribers': 'user-subscribers:infoView showSubscriberList'
                }
            },
            'info': {
                'contentView': 'User.Views.Projects',
                'modelType': 'User.Models.Projects'
            },
            'news': {
                'module': 'user-pages,news',
                'contentView': 'User.Views.News',
                'modelType': 'User.Models.News'
            },
            'news:object': {
                'cardView': null,
                'infoView': null,
                'tabsView': null,
                'socialView': null,
                'backedProjectsStatView': null,
                'adminActionsView': null,
                'moderatorAlertView': null,
                'urlAnchorView': null,
                layout: 'wide-layout',
                module: 'news',
                contentViewType: 'News.Views.PostPage',
                modelType: 'News.Models.SinglePostPage',
                'bodyCssClass': 'grid-1170'
            },
            'comments': {
                'contentView': null,
                'modelType': null
            }
        },
        campaigns: {
            defaults: {
                headerViewType: Breadcrumbs.Views.Start2,
                menuViewType: null,
                contentViewType: null,
                sidebarViewType: null,
                rightSidebarViewType: null
            },
            'funding-rules': {
                layout: 'simple-page-layout',
                module: 'funding-rules',
                modelType: 'FundingRules.Model',
                contentViewType: 'FundingRules.View',
                headerViewType: null
            },
            'campaigns:campaignId': {
                layout: 'campaigns-main-layout',
                headerViewType: null,
                module: 'planeta-contractor-alert',
                modelType: CrowdFund.Models.Page,
                contentExtraViewType: CampaignSearch.Views.BreadCrumbs,
                contentViewType: CrowdFund.Views.CampaignPage,
                bodyCssClass: 'grid-1170 project-page',
                urlAnchorView: {
                    'contacts': 'contentView onAddContactsClick'
                }
            },
            'campaigns:edit:campaignId': {
                layout: 'campaigns-layout',
                module: 'planeta-campaigns-edit',
                headerViewType: 'CampaignEdit.Views.Header',
                menuViewType: 'CampaignEdit.Views.Tabs',
                modelType: 'CampaignEdit.Models.Common',
                contentViewType: 'CampaignEdit.Views.Common',
                rightSidebarViewType: 'CampaignEdit.Views.TopCampaign',
                footerViewType: 'CampaignEdit.Views.Footer',
                bodyCssClass: 'grid-1170'
            },
            'campaigns:edit-details:campaignId': {
                layout: 'campaigns-layout',
                module: 'planeta-campaigns-edit',
                headerViewType: 'CampaignEdit.Views.Header',
                menuViewType: 'CampaignEdit.Views.Tabs',
                modelType: 'CampaignEdit.Models.Details',
                contentViewType: 'CampaignEdit.Views.Details',
                footerViewType: 'CampaignEdit.Views.Footer',
                bodyCssClass: 'grid-1170'
            },
            'campaigns:edit-shares:campaignId': {
                layout: 'campaigns-layout',
                module: 'planeta-campaigns-edit',
                headerViewType: 'CampaignEdit.Views.Header',
                menuViewType: 'CampaignEdit.Views.Tabs',
                modelType: 'CampaignEdit.Models.Shares',
                contentViewType: 'CampaignEdit.Views.SharesContent',
                rightSidebarViewType: 'CampaignEdit.Views.SharesEditListWrapper',
                footerViewType: 'CampaignEdit.Views.Footer',
                bodyCssClass: 'grid-1170'
            },

            'campaigns:edit-contractor:campaignId': {
                layout: 'campaigns-layout',
                module: 'planeta-campaigns-edit',
                headerViewType: 'CampaignEdit.Views.Header',
                menuViewType: 'CampaignEdit.Views.Tabs',
                modelType: 'CampaignEdit.Models.Check',
                contentViewType: 'CampaignEdit.Views.Check',
                footerViewType: 'CampaignEdit.Views.Footer',
                bodyCssClass: 'grid-1170'
            },
            'campaigns:created-successful:campaignId': {
                layout: 'campaigns-layout',
                module: 'planeta-campaigns-edit',
                headerViewType: null,
                modelType: 'CampaignEdit.Models.CreatedSuccessful',
                contentViewType: 'CampaignEdit.Views.CreatedSuccessful'
            },
            'campaigns:create': {
                modelType: 'CampaignEdit.Models.CreateCampaignPage'
            },
            'campaigns:orders:campaignId': {
                layout: 'simple-page-layout',
                module: 'campaign-stats',
                modelType: 'CampaignOrders.Models.Page',
                contentViewType: 'CampaignOrders.Views.Page'
            },
            'campaigns:affiliate-stats:campaignId': {
                layout: 'simple-page-layout',
                module: 'campaign-stats',
                modelType: 'CampaignAffiliateStats.Models.Page',
                contentViewType: 'CampaignAffiliateStats.Views.Page'
            },
            'campaigns:shares-stats:campaignId': {
                layout: 'simple-page-layout',
                module: 'campaign-stats',
                modelType: 'CampaignSharesStats.Models.Page',
                contentViewType: 'CampaignSharesStats.Views.Page'
            },
            'campaigns:advertising-tools:campaignId': {
                layout: 'simple-page-layout',
                module: 'campaign-stats',
                modelType: 'CampaignAdvertisingTools.Models.Page',
                contentViewType: 'CampaignAdvertisingTools.Views.Page'
            },
            'campaigns:donate:campaignId': {
                headerViewType: null,
                module: 'campaign-donate',
                modelType: 'CampaignDonate.Models.DonateSharePage',
                contentViewType: 'CampaignDonate.Views.DonateSharePage',
                bodyCssClass: 'grid-1170 project-page'
            },
            'campaigns:donate:campaignId:shareId': {
                headerViewType: null,
                module: 'campaign-donate',
                modelType: 'CampaignDonate.Models.DonateSharePage',
                contentViewType: 'CampaignDonate.Views.DonateSharePage',
                bodyCssClass: 'grid-1170 project-page'
            },
            'campaigns:donatesingle:campaignId:shareId': {
                headerViewType: null,
                module: 'campaign-donate',
                modelType: 'CampaignDonate.Models.DonateSharePage',
                contentViewType: 'CampaignDonate.Views.DonateSharePage',
                bodyCssClass: 'grid-1170 project-page'
            },
            'campaigns:donate:campaignId:shareId:payment': {
                headerViewType: null,
                module: 'campaign-donate',
                modelType: 'CommonDonate.Models.SharePaymentAndDeliveryPage',
                contentViewType: 'CampaignDonate.Views.SharePaymentAndDeliveryPage',
                bodyCssClass: ''
            },
            'campaigns:donate:campaignId:shareId:paymentinvestfiz': {
                headerViewType: null,
                module: 'campaign-donate',
                modelType: 'CommonDonate.Models.InvestingPaymentPage',
                contentViewType: 'CampaignDonate.Views.InvestingPaymentFizPage',
                bodyCssClass: ''
            },
            'campaigns:donate:campaignId:shareId:paymentinvestul': {
                headerViewType: null,
                module: 'campaign-donate',
                modelType: 'CommonDonate.Models.InvestingPaymentPage',
                contentViewType: 'CampaignDonate.Views.InvestingPaymentUlPage',
                bodyCssClass: ''
            }


        },
        settings: {
            defaults: {
                'module': 'news,user-settings',
                'layout': 'user-settings-layout',
                'tabsView': 'User.Views.Settings.GroupTabs'
            },
            'settings:general': {
                contentView: 'User.Views.Settings.General'
            },
            'settings:contacts': {
                contentView: 'User.Views.Settings.Contacts',
                contentModel: 'User.Models.Settings.Contacts'
            }
        }
    }
};

NavigationMap.User.main.defaults.layout = window.isMobileDev ? 'user-profile-layout-mobile' : 'user-profile-layout';

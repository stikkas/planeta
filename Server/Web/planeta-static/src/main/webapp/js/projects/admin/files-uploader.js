var AdminFileUploader = {
    Models: {},
    Views: {}
};

AdminFileUploader.Models.Main = BaseModel.extend({

    initialize: function(options) {
        this.set({
            profileId: workspace.appModel.get('myProfile').get('profileId'),
            justUploadedFiles: new BaseCollection([]),
            earlierUploadedFiles: new AdminFileUploader.Models.UploadedFilesCollection([])
        });

        this.get('earlierUploadedFiles').load();
    },

    uploadFiles: function() {
        var self = this;
        UploadController.showUploadFiles(self.get('profileId'), function(filesUploaded) {
            if (_.isEmpty(filesUploaded)) {
	            return;
            }

            var files = _.map(filesUploaded.models, function(fileUploaded) {
                return new BaseModel(fileUploaded.get('uploadResult'));
            });

            self.get('justUploadedFiles').add(files);
            self.get('earlierUploadedFiles').defaults.offset = _.size(self.get('justUploadedFiles').models);
        });
    }

});

AdminFileUploader.Views.FileUpload = BaseView.extend({
    el: '#center-container',
    template: '#admin-file-upload-template',

    events: {
        'click .upload-file': 'onUploadClicked'
    },

    construct: function(options) {
        this.addChildAtElement('.files-placeholder', new AdminFileUploader.Views.FileList({
            collection: this.model.get('justUploadedFiles')
        }));

        this.addChildAtElement('.last-uploaded-files-placeholder', new AdminFileUploader.Views.FileList({
            collection: this.model.get('earlierUploadedFiles')
        }));
    },

    onUploadClicked: function(e) {
        this.model.uploadFiles();
    }
});

AdminFileUploader.Views.File = BaseView.extend({
    template: '#admin-uploaded-file-template',
    tagName: 'li'
});

AdminFileUploader.Views.FileList = BaseListView.extend({
    itemViewType: AdminFileUploader.Views.File,
	tagName: 'ul'
});

AdminFileUploader.Models.UploadedFilesCollection = BaseCollection.extend({
    url: '/moderator/last-uploaded-files.json',

    remove: function(models, options) {
        BaseCollection.prototype.remove.call(this, models, options);
        this.load();
    }

});

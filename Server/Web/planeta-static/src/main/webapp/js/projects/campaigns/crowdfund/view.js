/*globals CrowdFund, UserPayment, BaseRichView, Modal, Blog, Comments, Likes, Widgets, Subscription, DonateUtils,
 CampaignUtils, ProfileUtils, PrivacyUtils, HoverInfoCard, Campaign, DefaultListView, StringUtils, Form,ImageUtils,
 ImageType, Banner, Breadcrumbs, News, StorageUtils, loadModule, CampaignDonate, Odometer, JobManager, FinishCampaignTraffic*/
CrowdFund = window.CrowdFund || {};
CrowdFund.Views = {};

CrowdFund.Views.Campaign = BaseRichView.extend({
    mediaTemplates: {
        audio: '#common-audio-template',
        photo: '#campaign-rich-photo-template',
        video: '#common-large-video-template'
    },
    template: "#campaign-common-template",
    injectionHtml: 'projectPageHtml',
    injectionFunc: 'projectPage',
    _needOdUpdate: true,
    events: {
        'click .js-click-to-talk': 'onTalkRequest',
        'click .js-click-to-subscribe': 'onSubscribeRequest',
        'click [data-click=add-contacts]': 'onAddContacts',
        'click .js-start-campaign': 'onStartCampaignClicked',
        'click .js-pause-campaign': 'onPauseCampaignClicked',
        'click .js-add-update': 'onCreateNewPost',
        'click .js-change-bg': 'onChangeBg',
        'click .js-promo-widgets': 'onPromoWidgetsClicked',
        'click .admin-action_drop': 'adminActionDrop',
        'click .js-open-link-dropdown': 'openDraftCampaignLink',
        'click .project-draft_form_btn': 'copyLink',
        'click .js-toggle-description': 'toggleDescription',
        'click .project-view-block_remind-text': 'subscribeOnEndProject',
        'click .project-view-block_remind-text-ok': 'unsubscribeOnEndProject',
        'click .project-tab_i': 'scrollToTop'
    },
    modelEvents: {
        'destroy': 'dispose',
        'change:selectedShare': 'selectShare'
    },
    isAdminActionDropOpen: false,
    isDraftCampaignLinkOpen: false,
    wasVisibilityChanged: false,
    construct: function () {
        this.model.set("isPreview", false);
        this.model.set('subscriptions', this.model.subscriptions);
        if ((this.model.get('targetAmount') && this.model.get('campaignId'))) {
            var stage = 1; //0-25%
            var completionPercentage = Math.floor(this.model.get('collectedAmount') / this.model.get('targetAmount') * 100);

            if (this.model.get('collectedAmount') > 0) {
                if (completionPercentage < 50) {
                    stage = 2;//25-50%
                } else if (completionPercentage < 100) {
                    stage = 3;//50-100%
                } else {
                    stage = 4;//100%+
                }
            }

            this.model.set({
                stage: stage
            }, {silent: true});
        }
        if (workspace) {
            if (workspace.currentLanguage) {
                this.model.set('lang', workspace.currentLanguage);
            }
        }
        var self = this;
        var campaignId = this.model.get('campaignId');

        this.campaignTabsModel = new CrowdFund.Models.CampaignTabs({
            campaignId: campaignId,
            isCharity: this.model.get('charity'),
            webCampaignAlias: this.model.get('webCampaignAlias')
        });
        this.campaignTabsModel.on('change:tab', this.handleSectionsView, this);
        this.addChildAtElement('.js-campaign-tabs', new CrowdFund.Views.CampaignTabs({
            model: this.campaignTabsModel
        }));


        if (!workspace.isBotRequest) {
            this.addChildAtElement('.js-project-list-container', new CrowdFund.Views.RelatedCampaigns({
                campaignId: campaignId
            }));
        }

        if (this.model.get('status') === 'FINISHED') {
            loadModule('finish-campaign-traffic').done(function () {
                FinishCampaignTraffic.init(self.model.get('campaignId'), self, self.model.get('creatorProfileId'), self.model.get('mainTag'));
            });
        }

        if (PrivacyUtils.isAdmin()) {
            loadModule('planeta-campaigns-edit').done(function () {
                if (CrowdFund.Views.ContractorAlert.isVisible(self.model)) {
                    self.addChildAtElement('.project-admin-controls_info', new CrowdFund.Views.ContractorAlert({
                        model: self.model.contractor,
                        campaign: self.model
                    }));
                }

                //if (workspace.navigationState.get('anchor') == 'contractor') {
                //    CrowdFund.Views.CampaignContractorEdit.open(self.model.contractor, self.model);
                //}

                var promotionPanelModel = new CrowdFund.Models.PromotionPanel(self.model.attributes);
                if (!promotionPanelModel.dontShow) {
                    self.addChildAtElement('.js-promotion-panel', new CrowdFund.Views.PromotionPanel({
                        model: promotionPanelModel
                    }));
                }
            });
        }

        var shares = this.setTitlesForShares(this.model.shares);
        this.sharesList = this.addChildAtElement('.js-action-card-list', new CrowdFund.Views.ShareList({
            collection: shares,
            controller: this.model
        }));

        if (!this.model.get('creatorProfileId')) {
            workspace.stats.tackNoProfileId('String 108: ' + JSON.stringify(this.model.toJSON()));
        }
        this.addChildAtElement('.js-project-author', new CrowdFund.Views.CampaignAuthor({
            model: this.model
        }));

        this.subscription = this.addChildAtElement('.js-subscription-container', new Subscription.Views.CampaignSubpscription({
            model: this.model.subscriptions
        }));

        $('.project-draft_form_btn .btn').twipsy({
            live: true,
            twipsyClass: 'twipsy twipsy-dark',
            template: '<div class="twipsy-wrapper"><div class="twipsy-arrow"></div><div class="twipsy-inner"></div></div>'
        });
    },

    dispose: function () {
        if (this.odometerUpdater)
            window.clearInterval(this.odometerUpdater);
        BaseRichView.prototype.dispose.call(this);
    },
    beforeRender: function () {
        this.model.set({
            backgroundImageUrlSmall: ImageUtils.getThumbnailUrl(this.model.get('backgroundImageUrl'), ImageUtils.HUGE, ImageType.PHOTO),
            progress: this.model.get('collectedAmount') / this.model.get('targetAmount') * 100
        }, {silent: true});
    },

    toggleDescription: function () {
        $('.project-short-description').addClass('hide');
        $('.project-description').addClass('show');
    },
    scrollToTop: function () {
        $('html, body').stop().animate({
            scrollTop: Math.ceil($(".project-view-block").offset().top) + 1
        }, 300);
    },
    setTitlesForShares: function (collection) {
        var shares = collection.models;
        for (var i = 0; i < shares.length; i++) {
            var isPrevInvesting = false,
                isNextInvesting = false,
                isCurrentInvesting = false;

            if (i > 0) {
                isPrevInvesting = ProfileUtils.isInvestingProject(shares[i - 1].get('shareStatus'));
            }

            if (i + 1 < shares.length) {
                isNextInvesting = ProfileUtils.isInvestingProject(shares[i + 1].get('shareStatus'));
            }


            isCurrentInvesting = ProfileUtils.isInvestingProject(shares[i].get('shareStatus'));
            if (isCurrentInvesting) {
                if (!isPrevInvesting && isNextInvesting) {
                    shares[i].set('multipleTitle', true);
                } else if (!isNextInvesting && !isPrevInvesting) {
                    shares[i].set('singleTitle', true);
                }
            }
        }

        collection.models = shares;
        return collection;
    },

    adminActionDrop: function () {
        var self = this;

        if (!this.isAdminActionDropOpen) {
            this.openDropdown('.admin-action_drop', '.admin-action_drop-switch');
            this.$el.find('.admin-action_drop').clickOff(function (e) {
                self.closeDropdown('.admin-action_drop', '.admin-action_drop-switch');
                self.isAdminActionDropOpen = false;
            });
        } else {
            this.closeDropdown('.admin-action_drop', '.admin-action_drop-switch');
        }

        this.isAdminActionDropOpen = !this.isAdminActionDropOpen;
    },

    openDraftCampaignLink: function () {
        var self = this;

        if (!this.$el.find('.js-open-link-dropdown').hasClass('open')) {
            this.openDropdown('.js-open-link-dropdown', '.js-open-link');
            $('.project-draft_popup').clickOff(function () {
                self.closeDropdown('.js-open-link-dropdown', '.js-open-link');
            });
        }
    },

    openDropdown: function (dropdownSelector, activeSelector) {
        this.$el.find(dropdownSelector).addClass('open');
        this.$el.find(activeSelector).addClass('active');
    },

    closeDropdown: function (dropdownSelector, activeSelector) {
        this.$el.find(dropdownSelector).removeClass('open');
        this.$el.find(activeSelector).removeClass('active');
    },

    switchDraftVisibility: function () {
        $('.project-draft_switch_val').toggleClass('on');
        $('.project-draft_text').toggle();
        $('.project-draft_form_wrap').toggle();

        var options = {
            url: '/api/campaign/campaign-draft-toggle.json',
            data: {
                campaignId: this.model.get('campaignId')
            }
        };

        this.wasVisibilityChanged = true;
        Backbone.sync('update', this.model, options);
    },

    copyLink: function df() {
        var succeeded,
            tooltipBlock = $('.js-copy-value'),
            input = $('#js-draft-link-copy');

        input.select(); // Select the input node's contents
        try {
            // Copy it to the clipboard
            succeeded = document.execCommand("copy");
        } catch (e) {
            succeeded = false;
        }

        if (succeeded) {
            tooltipBlock.data('tooltipOption').tooltip.remove();
            if (!df.copyTooltip) {
                df.copyTooltip = new Tooltip({
                    target: $('.copy-ico')[0],
                    position: 'top center',
                    content: "Скопировано",
                    openOn: null
                });
            }

            if (df.showTimeout) {
                clearTimeout(df.showTimeout)
            }

            df.copyTooltip.open();
            df.showTimeout = setTimeout(function () {
                df.copyTooltip.remove();
            }, 1000);
        }

    },

    onAddContacts: function () {
        $('.js-add-contacts').click();
    },
    renderSharingPopup: function () {
        this.sharesRedraw();
    },
    sharesRedraw: function () {
        var model = this.model;
        // Standard buttons are enabled
        $('.share-cont:not(.post-option-item)').empty();
        if ($('.standard-share-buttons').length > 0) {
            return;
        }

        if (!PrivacyUtils.hasAdministrativeRole()) {
            switch (model.get('status')) {
                case 'DELETED':
                case 'NOT_STARTED':
                case 'DRAFT':
                    return;
                case 'FINISHED':
                    if (model.get('targetStatus') === 'FAIL') {
                        return;
                    }
                    break;
            }
        }

        var shareData = {
            counterEnabled: true,
            hidden: false,
            parseMetaTags: true,
            url: 'https://' + workspace.serviceUrls.mainHost + '/campaigns/' + model.get('webCampaignAlias')
        };
        shareData.className = 'donate-sharing';
        $('.sharing-popup-social.sharing-mini').share(shareData);
    },
    afterRender: function () {
        var self = this;

        if (workspace.isAuthorized) {
            $.get('/api/profile/is-subscribed-campaign-end.json?campaignId=' + self.model.get('campaignId'))
                .done(function (response) {
                    if (response && response.success && response.result) {
                        $('.project-view-block_remind').toggleClass('ok');
                    }
                });
        }

        BaseRichView.prototype.afterRender.call(self);
        var isStuck = false;
        var nowIsStuck = false;
        var projectTab = self.$(".project-view-block_nav").stick_in_parent({
            parent: 'body'
        });
        projectTab
            .on('sticky_kit:stick', function () {
                isStuck = true;
                changeProjectTabClass();
            })
            .on('sticky_kit:unstick', function () {
                isStuck = false;
                changeProjectTabClass();
            });
        function changeProjectTabClass() {
            if ( isStuck !== nowIsStuck ) {
                nowIsStuck = isStuck;
                projectTab.toggleClass('nav-fixed', isStuck);
            }
        }

        self.showHideTabContent();

        if (self.model.get('videoId')) {
            workspace.showVideoPlayer(self.model.get('videoProfileId'), self.model.get('videoId'), self.$el.find('.video-content'), {
                width: 640,
                height: 360,
                autostart: true,
                ignoreStorageEvents: true,
                ignoreVideoManager: true,
                fromCampaign: true,
                imageUrl: self.model.get('viewImageUrl'),
                imageId: self.model.get('viewImageId')
            });
        }
        CrowdFund.Views.initOdometer(self, self.model);

        if (!self.model.get('isEditMode')) {
            self.socialWidget();
            self.renderSharingPopup();
        }
        self.model.get('tags').forEach(function (it) {
            if (it.mnemonicName === 'INVESTING') {
                var header = self.$el.find('.project-view-header');
                header.addClass('project-view-header__investing');
                header.find('.pvc-donate-btn-block > .flat-btn.btn-block').addClass('btn-invest');
            }
        });
        var status = self.model.get('status');
        if (status == 'DECLINED' || status == 'PATCH') {
            $.ajax({
                url: '/api/campaign/last-campaign-moderation-message.json',
                data: {
                    campaignId: self.model.get('campaignId')
                },
                success: function (response) {
                    var message = response.result;
                    if (message) {
                        message = message.replace(/\r\n/g, "<br />");
                    } else {
                        if (status == 'DECLINED') {
                            message = "Ваш проект не прошел модерацию.";
                        } else {
                            message = "Ваш проект требует доработки.";
                        }
                    }
                    self.$('.js-reason-declined').html(message);
                }
            });
        }
        this._initDropDraft();


        this.$('.project-view-card_share.pln-dropdown').dropPopup({
            trigger: '.pln-d-switch',
            popup: '> .pln-d-popup',
            activeClass: 'open'
        });

        this.initSticky(this.controller.get('tab'));

        window.toTopStart = $('.project-view-block').offset().top + 2;
        window.toTopOffset = 70;

        var toTop = $('.to-top');
        var toTopBlock = $('.to-top_block');
        var winHeight = window.innerHeight;

        toTopBlock.css({paddingTop: window.toTopOffset});

        $(window).on('resize', function () {
            winHeight = window.innerHeight;
        });

        $(window).on('scroll', function () {
            var winTop = $(window).scrollTop();

            if ( winTop > winHeight + window.toTopStart ) {
                toTop.addClass('to-top__show');
            } else {
                toTop.removeClass('to-top__show');
            }
        });

        toTop.on('click', function () {
            $(window).scrollTop(50);
            $('html, body').animate({scrollTop: 0}, 200, null);
        });
    },

    _initDropDraft: function mmx() {
        if (!mmx.called) {
            var self = this;
            mmx.called = true;
            var draftEl = $('.project-draft');
            if (draftEl.length) {
                var _Drop;
                _Drop = Drop.createContext({
                    classPrefix: 'drop'
                });

                $('.drop-down').each(function () {
                    var $drop, $target, content, drop, openOn, theme, position;
                    $drop = $(this);
                    theme = $drop.data('theme');
                    position = $drop.data('position') || 'bottom center';
                    openOn = $drop.data('open-on') || 'click';
                    $target = $drop.find('.drop-target');
                    $target.addClass(theme);
                    content = $drop.find('.drop-content').html();
                    drop = new _Drop({
                        target: $target[0],
                        classes: theme,
                        position: position,
                        constrainToWindow: true,
                        constrainToScrollParent: false,
                        openOn: openOn,
                        content: content
                    });
                    $drop.data('drop', drop);
                    return drop;
                });


                $(document).on('click', '.project-draft_switch_val', function () {
                    self.switchDraftVisibility();
                });
                $(document).on('click', '.project-draft .form-control', function () {
                    $(this).select();
                });
                var supported = document.queryCommandSupported("copy");
                if (supported) {
                    try {
                        document.execCommand("copy");
                    } catch (e) {
                        supported = false;
                    }
                }

                draftEl.data('drop').on('open', function () {
                    var tooltipBtn = $('.drop-element .project-draft_form_btn .btn');
                    if (supported) {
                        var clipboard = new Clipboard(tooltipBtn[0]);
                        clipboard.on('success', function () {
                            var tooltip = $('.drop-element .project-draft_popup .btn').data('tooltipOption');
                            var tooltipContent = $(tooltip.drop).find('.tooltip-content div');
                            tooltipContent.html('Скопировано');
                            tooltip.tooltip.drop.position();
                            tooltip.tooltip.drop.once('close', function () {
                                setTimeout(function () {
                                    tooltipContent.html('Копировать');
                                }, 200);
                            });
                        });
                    } else {
                        $('.drop-element .project-draft_form_btn').remove();
                    }
                });
            }
        }
    },
    socialWidget: function () {
        var self = this;

        if (!this.socialWidgetInit) {
            this.socialWidgetInit = true;
            Banner.init('', workspace.appModel.get('myProfile').get('isAuthor'));
            loadModule("social-widget", 3000).done(function () {
                self.$('.social-widget-sidebar').socialize();
            });
        }
    },
    subscribeOnEndProject: function () {
        if (LazyHeader.checkNotAnonymous()) {
            $.post('/api/profile/subscribe-campaign-end.json', {campaignId: this.model.get('campaignId')})
                .done(function (resp) {
                    if (resp && resp.success) {
                        $('.project-view-block_remind').toggleClass('ok');
                    }
                });
        }
    },
    unsubscribeOnEndProject: function () {
        if (LazyHeader.checkNotAnonymous()) {
            $.post('/api/profile/unsubscribe-campaign-end.json', {campaignId: this.model.get('campaignId')})
                .done(function (resp) {
                    if (resp && resp.success) {
                        $('.project-view-block_remind').toggleClass('ok');
                    }
                });
        }
    },
    onTalkRequest: function (e) {
        e.preventDefault();
        if (this.model.get('contactsCount')) {
            CampaignUtils.openCampaignFeedback(this.model.get('campaignId'), this.model.get('contactsCount'));
        } else {
            var self = this;
            this.model.loadContacts().done(function () {
                CampaignUtils.openCampaignFeedback(self.model.get('campaignId'), self.model.get('contactsCount'));
            })

        }
    },

    onSubscribeRequest: function (e) {
        e.preventDefault();
        if (workspace.isAuthorized) {
            this.model.subscriptions.toggleSubscription(this.model.subscriptions.get('isSubscribed'), this.model);
        } else {
            LazyHeader.showAuthForm('signup');
        }
    },

    onCreateNewPost: function () {
        var self = this;
        if (!this.postCollection) {
            this.initUpdates();
        }
        loadModule('news').done(function () {
            new News.Views.Modal.PostEditor({
                model: self.postCollection.createPost(self.model.get('campaignId'))
            }).render();
        });

    },
    onChangeBg: function () {
        var self = this;
        loadModule('campaign-crop-bg').done(function () {
            Modal.showDialogByViewClass(CrowdFund.Views.ImageField.ModalCropBgView, self.model, null, null, null, null, true);
        });
    },
    onStartCampaignClicked: function (e) {
        if (this.model.get('pendingStart')) {
            return;
        }
//        e.preventDefault();
        var self = this;
        var $el = $(e.currentTarget);
        Modal.showConfirm('Запустить проект?', undefined, {
            //todo move it to deferred execution chain?
            success: function () {
                $el.addClass('disabled');
                self.model.startCampaign().done(function (message) {
                    workspace.appView.showSuccessMessage(message);
                    document.location.reload();
                }).fail(function (message) {
                    workspace.appView.showErrorMessage(message);
                    self.model.set('pendingStart', false);
                });
            }
        });
    },
    onPauseCampaignClicked: function (e) {
        e.preventDefault();
        var self = this;
        Modal.showConfirmWithTextArea('Остановить проект?', undefined, "Введите причину остановки", {
            success: function (modalReply) {
                if (!modalReply) {
                    workspace.appView.showErrorMessage("Не введена причина остановки");
                }
                self.model.pauseCampaign(modalReply).done(function (message) {
                    workspace.appView.showSuccessMessage(message);
                    document.location.reload();
                }).fail(function (message) {
                    workspace.appView.showErrorMessage(message);
                });
            }
        });
    },
    pageData: function (section) {
        var model = this.model;
        var campaignName = model.get('name');
        var creatorProfile = workspace.appModel.get('profileModel');
        var displayName = creatorProfile.get('displayName');
        var viewImageUrl = model.get('viewImageUrl') || model.get('imageUrl');
        var imageUrl = ImageUtils.getThumbnailUrl(model.get('imageUrl'), ImageUtils.ORIGINAL, ImageType.PRODUCT);
        var title = _.template('<%=campaignName%> | Planeta', {
            campaignName: campaignName
        });

        var description = model.get('shortDescription');

        switch (section) {
            case 'comments':
                title = _.template('Комментарии проекта «<%=campaignName%>»', {
                    campaignName: campaignName
                });
                description = _.template('Комментарии спонсоров проекта «<%=campaignName%>» на платформе краудфандинга Planeta.ru', {
                    campaignName: campaignName
                });
                break;
            case 'backers':
                title = _.template('Спонсоры, поддержавшие проект «<%=campaignName%>»', {
                    campaignName: campaignName
                });
                description = _.template('Участники проекта «<%=campaignName%>» на платформе краудфандинга Planeta.ru', {
                    campaignName: campaignName
                });

                break;
            case 'updates':
                title = _.template('Новости проекта «<%=campaignName%>»', {
                    campaignName: campaignName
                });
                description = _.template('Последние новости проекта «<%=campaignName%>» на платформе краудфандинга Planeta.ru', {
                    campaignName: campaignName
                });

                break;
            case 'faq':
                title = _.template('Вопрос – ответ | Проект «<%=campaignName%>»', {
                    campaignName: campaignName
                });
                description = _.template('Ответы на популярные вопросы в проекте «<%=campaignName%>» на платформе краудфандинга Planeta.ru', {
                    campaignName: campaignName
                });

                break;

        }

        $('link[rel="canonical"]').remove();
        var result = {
            image: imageUrl,
            link: imageUrl,
            title: title,
            description: description

        };

        if (_.contains(["DRAFT", "PATCH", "NOT_STARTED", "APPROVED"], model.get("status"))) {
            result.robots = "noindex, nofollow";
        }

        if (model.get('campaignId')) {
            CampaignUtils.trackPixel(model.get('campaignId'));
        }

        return result;
    },
    handleSectionsView: function (m, section) {
        this.onTabChanged(section);

        if (this.controller) {
            this.controller.pageData = this.pageData(section);
            var prevTab = this.controller.get('tab');
            if (prevTab && prevTab !== section) {
                workspace.appView.changePageData(this.controller);
            }
            this.controller.set({
                tab: section
            }, {silent: true});
        }
        workspace.audioPlayerPause();

        this.showHideTabContent();
    },
    showHideTabContent: function () {
        if (this.campaignTabsModel) {
            this.$('.donate-action-view').hide();
            this.$('.donate-action-view.' + this.campaignTabsModel.get('tab')).show();
        }
    },
    onTabChanged: function (section) {
        this.initSticky(section);

        switch (section) {
            case 'comments':
                if (!this.commentsView) {
                    this.initComments();
                }
                break;
            case  'backers':
                if (!this.backersView) {
                    this.initBackers();
                }
                break;
            case  'updates':
                if (!this.updatesView) {
                    this.initUpdates();
                }
                break;
            case 'faq':
                if (!this.faqView) {
                    this.initFaq();
                }
                break;
        }
        if (this.backersView) {
            this.backersView.onTabChanged(section);
        }

    },
    initSticky: function (section) {
        if ( window.screen.width >= 768 ) {
            var content = $('.project-view-block_content');
            var sidebar = $('.project-view-block_sidebar');
            if ( section === 'about' ) {
                _.defer(function () {
                    if ( content.height() < sidebar.height() ) {
                        content.stick_in_parent({
                            parent: '.project-view-block_wrap',
                            spacer: false
                        });
                        $('img', content).one('load.about-sticky', function () {
                            if ( content.height() < sidebar.height() ) {
                                content.trigger('sticky_kit:recalc');
                            } else {
                                content.trigger('sticky_kit:detach');
                                $('img', content).off('load.about-sticky');
                            }
                        });
                    }
                });
            } else {
                content.trigger('sticky_kit:detach');
            }
        }
    },
    initComments: function () {
        var self = this;
        //комменты не загрузятся если commentsCount = 0
        this.campaignTabsModel.loadDfd.done(function () {
            self.model.set('commentsCount', self.campaignTabsModel.get('commentsCount'));
            self._initComments();
        });
    },
    _initComments: function () {
        var comments = new Comments.Models.Comments({
            profileId: this.model.get('profileId'),
            objectId: this.model.get('campaignId'),
            objectOwnerProfileId: this.model.get('creatorProfileId'),
            objectType: 'CAMPAIGN',
            showCounters: false
        }, {
            commentableObject: this.model
        });
        this.commentsView = this.addChildAtElement('.js-comments-list', new Comments.Views.ModalComments({
            model: comments
        }));
    },
    initBackers: function () {
        var backers = new CrowdFund.Models.Backers([], {
            data: {
                campaignId: this.model.get('campaignId')
            },
            limit: 10
        });
        backers.load();
        this.backersView = this.addChildAtElement('.backers-list-wrap', new CrowdFund.Views.Backers({
            collection: backers,
            model: this.model,
            itemViewType: CrowdFund.Views.BackerItem
        }));
    },
    initUpdates: function () {
        var self = this;
        loadModule('news').done(function () {
            self._initUpdates();
        });
    },
    _initUpdates: function () {
        this.postCollection = new News.Models.Posts([], {
            data: {
                profileId: 0,
                campaignId: this.model.get('campaignId')
            },
            limit: 10
        });
        this.postCollection.load();
        this.updatesView = this.addChildAtElement('.updates-list', new News.Views.Posts({
            collection: this.postCollection,
            model: this.model
        }));
    },

    initFaq: function () {
        var faqQuestions = new CrowdFund.Models.Faq([], {
            data: {
                campaignId: this.model.get('campaignId')
            }
        });

        faqQuestions.load();
        this.faqView = this.addChildAtElement('.js-faq-list', new CrowdFund.Views.FaqList({
            collection: faqQuestions
        }));
        var faqQuestionModel = new CrowdFund.Models.FaqQuestion({
            campaignId: this.model.get('campaignId')
        });
        faqQuestionModel.collection = faqQuestions;
        this.addChildAtElement('.js-add-faq', new CrowdFund.Views.FaqEdit({
            model: faqQuestionModel,
            collection: faqQuestions
        }));
    },

    onGetWidgetCodeClicked: function () {
        var model = new Widgets.Models.CampaignWidget({
            widgetId: this.model.get('campaignId')
        });

        var view = new Widgets.Views.CampaignWidget({
            model: model
        });

        Modal.showDialog(view);
    },
    onPromoWidgetsClicked: function (e) {
        var url = 'https://' + workspace.serviceUrls.widgetsAppUrl + '/widgets-external.html?campaignId=' + this.model.get('campaignId');
        var popupOptions = 'scrollbars=1, resizable=1, menubar=0, width=1024, height=680, toolbar=0, status=0';
        window.open(url, '_blank', popupOptions);
        return false;
    },
    selectShare: function (share) {
        this.$el.find('.action-card.focus').removeClass('focus');
    }
});

CrowdFund.Models.CampaignTabs = BaseModel.extend({
    defaults: {
        commentsCount: 0,
        backersCount: 0,
        updatesCount: 0,
        faqCount: 0,
        newNewsCount: 0,
        newCommentCount: 0,
        newPurchaseCount: 0,
        isCharity: false,
        tabVisited: {}
    },
    autoFetch: 'campaignId',
    parse: BaseModel.prototype.parseActionStatus,
    url: "/api/campaign/get-campaign-new-events-count.json"
});

CrowdFund.Views.CampaignTabs = BaseView.extend({
    template: "#campaigns-tab-view-template",
    construct: function () {
        var self = this;
        var tab = workspace.navigationState.get('subsection') || 'about';
        self.model.set('tab', tab);
        self.model.get('tabVisited')[tab] = true;
        this.addedNews = function () {
            self.model.set('updatesCount', self.model.get('updatesCount') + 1);
            if (workspace.navigationState.get('subsection') !== 'updates') {
                self.model.set('newNewsCount', self.model.get('newNewsCount') + 1);
            }
        };
        Backbone.GlobalEvents.on('news-added', this.addedNews);

        this.removedNews = function () {
            var currentCount = self.model.get('updatesCount');
            if (currentCount > 0) {
                currentCount -= 1;
                self.model.set('updatesCount', currentCount);
            }
            if (currentCount === 0) {
                var tab = 'about';
                self.model.get('tabVisited')[tab] = true;
                self.trigger('change');
                self.model.set('tab', tab);
                var url = "campaigns/" + self.model.get('webCampaignAlias') + "/" + tab;
                workspace.navigate(url, {trigger: false, replace: true});
            }
        };
        Backbone.GlobalEvents.on('news-deleted', this.removedNews);
    },
    onCampaignTabClicked: function (e) {
        e.preventDefault();
        var $el = $(e.currentTarget);
        var tab = $el.data('tab');
        this.model.get('tabVisited')[tab] = true;
        this.trigger('change');
        this.model.set('tab', tab);
        var url = "campaigns/" + this.model.get('webCampaignAlias') + "/" + tab;
        workspace.navigate(url, {trigger: false, replace: true});
    },
    dispose: function () {
        Backbone.GlobalEvents.off('news-added', this.addedNews);
        Backbone.GlobalEvents.off('news-deleted', this.removedNews);
    }
});

CrowdFund.Views.setOdometerUpdater = function (view, model, odometer) {
    if (odometer && view.model.get('status') === 'ACTIVE' && !view.model.get('tabs')) {
        view.odometerUpdater = setInterval(function xx() {
            if (xx.xhr && xx.xhr.readyState !== 4)
                xx.xhr.abort();
            xx.xhr = $.ajax({
                url: "/api/campaign/campaign-collected-amount.json",
                data: {
                    campaignId: model.get('campaignId')
                },
                success: function (response) {
                    odometer.update(response);
                }
            });
        }, 120000);
    }
};

CrowdFund.Views.initOdometer = function (view, model) {
    if (model.isOdometerInit) {
        return;
    }
    model.isOdometerInit = true;

    loadModule('odometer').done(function () {
        var deltaToAnimate = 0;
        var collectedAmount = model.get('collectedAmount') || 0;
        if (model.get('status') === "ACTIVE") {
            deltaToAnimate = Math.floor(Math.random() * 1500);
        }
        var od = new Odometer({
            el: view.$el.find('.js-campaign-odometer')[0],
            value: Math.max(collectedAmount - deltaToAnimate, 0),
            format: '( ddd)'
        });
        setTimeout(function () {
            od.update(collectedAmount);
        }, 500);
        if (view._needOdUpdate) {
            CrowdFund.Views.setOdometerUpdater(view, model, od);
        }
    });
};

CrowdFund.Views.CampaignPreview = CrowdFund.Views.Campaign.extend({
    mediaTemplates: {
        audio: '#common-audio-template',
        photo: '#campaign-rich-photo-template',
        video: '#common-large-video-template'
    },
    template: '#campaign-common-template',
    construct: function () {
        this.model.set("isPreview", true);

        if (workspace) {
            if (workspace.currentLanguage) {
                this.model.set('lang', workspace.currentLanguage);
            }
        }

        var shares = this.setTitlesForShares(this.model.shares);
        this.addChildAtElement('.js-action-card-list', new CrowdFund.Views.PreviewShareList({
            collection: shares
        }));

        var campaignId = this.model.get('campaignId');

        this.campaignTabsModel = new CrowdFund.Models.CampaignTabs({
            campaignId: campaignId,
            isCharity: this.model.get('charity'),
            webCampaignAlias: this.model.get('webCampaignAlias')
        });

        this.campaignTabsModel.set({
            tab: "about"
        });

        this.addChildAtElement('.js-campaign-tabs', new CrowdFund.Views.CampaignTabs({
            model: this.campaignTabsModel
        }));

        if (!this.model.get('creatorProfileId')) {
            workspace.stats.tackNoProfileId('String 662: ' + JSON.stringify(this.model.toJSON()));
        }
        this.addChildAtElement('.js-project-author', new CrowdFund.Views.CampaignAuthor({
            model: this.model
        }));
    },

    beforeRender: function () {
        this.model.set({
            progress: this.model.get('collectedAmount') / this.model.get('targetAmount') * 100
        }, {silent: true});
    },

    showHideTabContent: function () {
        if (this.campaignTabsModel) {
            this.$('.donate-action-view').hide();
            this.$('.donate-action-view.about').show();
        }
    },

    afterRender: function () {
        var self = this;

        BaseRichView.prototype.afterRender.call(self);

        this.showHideTabContent();

        if (self.model.get('videoId')) {
            workspace.showVideoPlayer(self.model.get('videoProfileId'), self.model.get('videoId'), self.$el.find('.video-content'), {
                width: 640,
                height: 360,
                autostart: true,
                ignoreStorageEvents: true,
                ignoreVideoManager: true,
                fromCampaign: true,
                imageUrl: self.model.get('viewImageUrl'),
                imageId: self.model.get('viewImageId')
            });
        }

        $('html, body').animate({scrollTop: 0}, 500);
    }
});

CrowdFund.Views.Backers = DefaultListView.extend({
    itemViewType: CrowdFund.Views.BackerItem,
    emptyListTemplate: '#empty-backers-template',
    className: "backers-list clearfix",
    construct: function () {
        DefaultListView.prototype.construct.apply(this, arguments);
        var UPDATE_ONLINE_STATUS_PERIOD = 5000;
        this.isActive = true;
        JobManager.schedule('update-online-status', this.updateBackersOnlineStatus, 0, UPDATE_ONLINE_STATUS_PERIOD, {
            context: this
        });
    },
    onTabChanged: function (tab) {
        this.isActive = tab === 'backers';
    },
    updateBackersOnlineStatus: function () {
        if (!this.isActive) {
            return;
        }
        var self = this;
        var users = this.collection.pluck('profileId');
        if (users.length > 0) {
            workspace.appModel.get('onlineChecker').sendCheckOnlineStatusAjax({users: users}).done(function (data) {
                self.collection.each(function (backer) {
                    $('#backer' + backer.profileId).attr('class', _(data.users).indexOf(backer.profileId) >= 0 ? 'icon-online' : 'icon-offline');
                });
            });
        }
    }
});

CrowdFund.Views.FaqEdit = BaseView.extend({
    template: '#campaign-faq-edit-template',
    events: {
        'click .js-add-faq': 'addOrUpdateQuestion',
        'keypress': 'onKeyPress'
    },
    updateQuestionModel: function (validationCallback) {
        var $question = this.$('[name=question]');
        var $answer = this.$('[name=answer]');
        var data = {
            question: $question.val(),
            answer: $answer.val()
        };
        var validation = this.model.validateModel(data);
        data = _.extend(data, {
            questionError: '',
            answerError: ''
        });
        if (!validation.success) {
            data = _.extend(data, validation.errors);
        }
        this.model.set(data);
        return validation;
    },
    refreshForm: function () {
        this.model.set({question: '', answer: ''});
    },
    addOrUpdateQuestion: function (e) {
        var validation = this.updateQuestionModel();
        if (!validation.success) {
            return;
        }

        var $submitButton = this.$('.js-add-faq');
        if ($submitButton.attr('disabled')) {
            return false;
        }
        $submitButton.attr('disabled', true);
        this.model.unset('questionError');
        this.model.unset('answerError');
        var self = this;
        this.model.updateFaqQuestion({
            error: function (response) {
                workspace.appView.showErrorMessage(response.errorMessage);
            },
            success: function () {
                self.refreshForm();
            },
            complete: function () {
                $submitButton.attr('disabled', false);
            }
        });
    },
    onKeyPress: function (event) {
        if (event.keyCode == 10 || (event.keyCode == $.ui.keyCode.ENTER && (event.ctrlKey || $(event.target).is('.js-add-faq')))) {
            return this.addOrUpdateQuestion();
        }
    }

});

CrowdFund.Views.ModalFaqEdit = Modal.View.extend({
    className: 'modal modal-faq-answer',
    template: "#campaign-faq-modal-edit-template",
    events: _.extend({}, CrowdFund.Views.FaqEdit.prototype.events, Modal.View.prototype.events),
    addOrUpdateQuestion: function (e) {
        CrowdFund.Views.FaqEdit.prototype.addOrUpdateQuestion.apply(this, arguments);
    },
    onKeyPress: function (e) {
        CrowdFund.Views.FaqEdit.prototype.onKeyPress.apply(this, arguments);
    },
    updateQuestionModel: function () {
        return CrowdFund.Views.FaqEdit.prototype.updateQuestionModel.apply(this, arguments);
    },
    refreshForm: function () {
        this.cancel();
    }
});


CrowdFund.Views.FaqItem = BaseView.extend({
    template: '#campaign-faq-template',
    className: 'js-campaign-faq-item',
    events: {
        'click .project-faq_i-question': 'onClick',
        'click .js-edit-faq': 'editFaq',
        'click .js-delete-faq': 'deleteFaq'
    },
    afterRender: function () {
        this.$el.data('model', this.model);
    },
    onClick: function (e) {
        e.preventDefault();
        var answer = this.$('.project-faq_i-answer');
        var container = this.$('.project-faq_i');

        if (container.hasClass('active')) {
            container.removeClass('active');
        } else {
            container.addClass('active');
        }

        answer.stop(1, 1).slideToggle(300);
    },
    editFaq: function (e) {
        e.stopPropagation();
        var model = this.model;
        var view = new CrowdFund.Views.ModalFaqEdit({
            model: model
        });
        view.render();
    },
    deleteFaq: function (e) {
        e.stopPropagation();
        var model = this.model;
        Modal.showConfirm('Удалить ответ на вопрос?', "Подтверждение удаления", {
            success: function () {
                model.deleteFaqQuestion();
            }
        });

    }
});

CrowdFund.Views.FaqList = DefaultListView.extend({
    emptyListTemplate: '#campaign-faq-empty-template',
    itemViewType: CrowdFund.Views.FaqItem,
    afterRender: function () {
        if (PrivacyUtils.isAdmin() || PrivacyUtils.hasAdministrativeRole()) {
            var self = this;
            var collection = this.collection;
            this.$el.sortable({
                items: ".js-campaign-faq-item",
                axis: 'y',
                update: function (event, ui) {
                    var draggedModel = ui.item.data('model');
                    self.resort({
                        idTo: collection.at(ui.item.index()).get('campaignFaqId'),
                        idFrom: draggedModel.get('campaignFaqId'),
                        campaignId: draggedModel.get('campaignId'),
                        draggedModel: draggedModel,
                        indexTo: ui.item.index()
                    });
                }
            });
        }

    },
    resort: function (options) {
        var self = this;

        function fail() {
            workspace.appView.showMessageUnexpectedError();
            self.onReset();
        }

        Backbone.sync('update', null, {
            url: '/api/profile/campaign-faq-resort.json',
            data: {
                idFrom: options.idFrom,
                idTo: options.idTo,
                campaignId: options.campaignId
            }
        }).done(function (response) {
            if (response && response.success) {
                self.collection.remove(options.draggedModel, {silent: true});
                self.collection.add(options.draggedModel, {at: options.indexTo, silent: true});
                self.onReset();
            } else {
                fail();
            }

        }).fail(fail);

    }
});


CrowdFund.Views.CampaignAuthor = BaseView.extend({
    template: '#campaign-author-template',
    anchorEnabled: true,
    construct: function () {
        this.profileSitesView = this.addChildAtElement('.js-profile-sites', new CrowdFund.Views.ProfileSites({
            profileId: this.model.get('creatorProfileId'),
            controller: this.model
        }));

        this.model.set('profileSites', this.profileSitesView.model);
        //this.anchor = workspace.navigationState.get('anchor');
    },
    onAddContactsClick: function () {
        if (PrivacyUtils.isAdmin()) {
            var self = this;
            loadModule('planeta-campaigns-edit').done(function () {
                var model = new BaseModel({
                    campaign: self.model,
                    profileSites: self.profileSitesView.model
                });
                var view = new CrowdFund.Views.CampaignContactsEdit({model: model});
                view.render();
            });
        }
    },
    afterRender: function () {
        if (this.anchorEnabled && workspace.navigationState.get('anchor') === 'sites') {
            this.anchorEnabled = false;
            this.onAddContactsClick();
        }
    }
});

CrowdFund.Views.BackerItem = BaseView.extend({
    template: '#backer-template-new',
    className: "backers-list-item",
    events: {
        'click': 'onClicked'
    },

    construct: function () {
        if (workspace) {
            if (workspace.currentLanguage) {
                this.model.set('lang', workspace.currentLanguage);
            }
        }
    },
    onClicked: function (e) {
        workspace.onClickNavigate(e, ProfileUtils.getAbsoluteUserLink(this.model.get('profileId'), this.model.get('alias')));
    },
    afterRender: function () {
        if (this.model.get('onlineStatus') == null) {
            this.model.checkOnlineStatus();
        }
    }
});


CrowdFund.Views.CampaignSelectionItem = BaseView.extend({
    template: "#group-item-template",
    tagName: 'li',
    className: 'project-create-item',
    construct: function () {
        var self = this;
        this.$el.on('click', function () {
            self.onItemClick();
        });
    },

    onItemClick: function () {
        Campaign.Models.createCampaign(this.model.get("profileId"));
    }
});

CrowdFund.Views.CampainsList = DefaultContentScrollListView.extend({
    itemViewType: CrowdFund.Views.CampaignSelectionItem,
    className: 'modal-project-create',
    tagName: 'ul',
    onReset: function () {
        var self = this;
        return BaseListView.prototype.onReset.call(this).done(function () {
            if (self.$el.height() > self.$el.parent().height()) {
                self.$el.parent().scrollbar({arrows: false});
            }
        });
    }
});

CrowdFund.Views.SelectCampaignDialog = Modal.View.extend({
    template: '#group-list-template',
    construct: function () {
        this.addChildAtElement('.modal-scroll-content', new CrowdFund.Views.CampainsList({
            collection: new BaseCollection(_.union(workspace.appModel.get('profileModel'), this.model.get("groups")))
        }));
    }
});


CrowdFund.Views.BaseShareItem = BaseView.extend({
    template: "#share-item-template",
    tagName: "li",
    className: "action-card-list_i",
    events: {
        'click img': 'showImage',
        'click .action-card_show': 'showModal',
        'click .btn-nd-primary': 'navigateToPurchaseDetails',
        'keyup .action-card_counter-val': 'donateAmountChanged',
        'click .js-increase:not(.disabled)': "tryIncreaseQuantity",
        'click .js-decrease:not(.disabled)': "tryDecreaseQuantity"
    },
    construct: function () {
        if ((this.model.get('amount') && this.model.get('amount') <= this.model.get('purchaseCount'))) {
            this.sellOut = true;
        }

        if (this.model.get('isDonate')) {
            if (window.workspace && (workspace.currentLanguage === "en")) {
                this.model.set({
                    name: 'No reward',
                    nameHtml: 'No reward',
                    description: 'Thank you I don’t need a reward I just want to back the campaign',
                    descriptionHtml: 'Thank you I don’t need a reward I just want to back the campaign'
                }, {silent: true});
            }

            if (window.workspace && (workspace.currentLanguage === "ru")) {
                this.model.set({
                    name: 'Поддержать на любую сумму',
                    nameHtml: 'Поддержать на любую сумму',
                    description: 'Спасибо, мне не нужно вознаграждение, я просто хочу поддержать проект.',
                    descriptionHtml: 'Спасибо, мне не нужно вознаграждение, я просто хочу поддержать проект.'
                }, {silent: true});
            }
        }
    },
    afterRender: function () {
        if (ProfileUtils.isInvestingProject(this.model.get('shareStatus'))) {
            this.$el.addClass('action-card-i__investing');
        }
        if (this.sellOut) {
            this.$el.addClass('disabled');
            this.$el.find('.action-card_show').remove();
        }

        inputCurrencyMask.init(this.$el.find('.js-currency-mask'));

        if (this.controller) {
            var shareData = {
                counterEnabled: false,
                hidden: false,
                parseMetaTags: true,
                url: 'https://' + workspace.serviceUrls.mainHost + '/campaigns/' + this.controller.get('webCampaignAlias') +
                '/donate/' + this.model.get('shareId')
            };
            shareData.className = 'donate-sharing';
            $('.js-share-share').share(shareData);
        }
    },

    showImage: function () {
        var options = {underlyingModal: this.parent};
        App.Mixins.Media.showPhoto(this.model.get('profileId'), this.model.get('imageId'), null, options);

    },

    showModal: function () {
        if (this.$el.hasClass('disabled')) {
            return;
        }

        var selectedShare = this.controller.get('selectedShare');
        if (!selectedShare || this.model.get('shareId') !== selectedShare.get('shareId')) {
            this.controller.switchShare(this.model, false);
        }
        var donateAmount = this.controller.get('donateAmount');
        if (!donateAmount) {
            this.controller.set('donateAmount', this.model.get('price'), {silent: true});
        }

        if (!this.model.get('isDonate')) {
            this.updateQuantityModifiers(this.model.get('quantity'));
        }

        var el = this.$el.find('.action-card');

        if (el.hasClass('active')) {
            return;
        }

        if (window.screen.width > 767) {
            this.actionCardDesktop(el);
        }
    },

    actionCardDesktop: function (el) {
        var text = $('.action-card_descr-text', el);
        var textHeight, textCollapse, textClass;

        var showMore = $('.action-card_descr-show', el);

        var action = $('.action-card_action', el);
        var actionCont = $('.action-card_action-cont', el);
        var actionHeight;

        var input = $('.action-card_counter-val', el);

        if (!!el.closest('.action-card-donate').length) {
            var openTimeout, closeTimeout;

            if (el.hasClass('active')) return;
            clearTimeout(openTimeout);
            clearTimeout(closeTimeout);

            $('body, html').animate({
                    scrollTop: input.closest('.action-card-list_i').offset().top - $('.project-tab_list').height() - parseInt(input.closest('.action-card-list_i').css('margin-top'), 10)
                }, 'slow', null
            ).promise().then(
                function () {
                    closeCard($('.action-card.active'));
                    openCard();
                });

            function openCard() {
                actionHeight = actionCont.height();
                action.height(actionHeight);

                el.addClass('focus active');

                openTimeout = setTimeout(function () {
                    input.focus();
                    action.css('height', 'auto');
                }, 550);
            }

            function closeCard(el) {
                var actionCont = $('.action-card_action-cont', el);
                var action = $('.action-card_action', el);
                var actionHeight = actionCont.height();
                action.height(actionHeight);
                closeTimeout = setTimeout(function () {
                    action.css('height', '');
                    el.removeClass('focus active');
                });
            }

        } else {
            $('body, html').animate({
                    scrollTop: input.closest('.action-card-list_i').offset().top - $('.project-tab_list').height() - parseInt(input.closest('.action-card-list_i').css('margin-top'), 10)
                }, 'slow', null
            ).promise().then(function () {
                actionHeight = actionCont.height();
                textHeight = text.height();
                textCollapse = textHeight > 108;
                textClass = textCollapse ? ' text-collapsed' : '';
                text.height(textHeight);


                $('.action-card').removeClass('focus');
                $('.action-card').removeClass('text-collapsed');
                $('.action-card').removeClass('active');
                $('.action-card').addClass('inactive');
                $('.action-card .action-card_action').css('height', 0);

                if (textCollapse) {
                    text.height(108);
                }

                action.height(actionHeight);
                el.addClass('focus active' + textClass);

                setTimeout(function () {
                    input.focus();
                    el.removeClass('inactive');
                    action.css('height', 'auto');
                }, 550);

                showMore.one('click', function (e) {
                    e.stopPropagation();
                    if (textCollapse) {
                        text.height(textHeight);
                    }

                    el.removeClass(textClass);
                });
            });

        }
    },

    donateAmountChanged: _.debounce(function (e) {
        //Попробовать убрать selectedShare
        var selectedShare = this.controller.get('selectedShare');
        if (!selectedShare || this.model.get('shareId') !== selectedShare.get('shareId')) {
            this.controller.switchShare(this.model, false);
        }
        var value = parseInt($(e.currentTarget).val());
        if ( value === parseInt(this.controller.get('donateAmount')) ) return;
        this.controller.set('donateAmount', value, {silent: true});
        if (this.model.get('isDonate')) {
            this.$el.find('.btn-nd-primary span').text(StringUtils.humanNumber(value));
        }
    }, 300),

    navigateToPurchaseDetails: function () {
        if (!this.isValid()) {
            return false;
        }
        var currentShare = this.model;
        var shareId = currentShare.get('shareId');
        SessionStorageProvider.extendByKey('commonInfo', {orderType: 'SHARE'});
        SessionStorageProvider.extendByKey('sharePurchase_' + shareId, {
            donateAmount: parseInt(this.controller.get('donateAmount')),
            quantity: this.model.get('quantity')
        });

        if (window.gtm) {
            window.gtm.trackSelectShare(this.model);
        }
        var campaignId = this.controller.get('campaignId')
        var donateThisShareUrl = '/campaigns/' + campaignId + '/donate/' + shareId + '/payment';
        if (ProfileUtils.isInvestingProject(currentShare.get('shareStatus'))) {
            donateThisShareUrl += 'investfiz';
            workspace.navigate(donateThisShareUrl);
        } else {
            $.ajax('/api/public/purchase-share', {
                contentType: 'application/json',
                type: 'POST',
                data: JSON.stringify({
                    shareId: currentShare.get('shareId'),
                    quantity: currentShare.get('quantity'),
                    donateAmount: parseInt(this.controller.get('donateAmount'))
                })
            }).done(function (response) {
                if (response.success) {
                    var prefix = '';
                    if ((/localhost:/).test(document.location.origin)) {
                        prefix = 'http://localhost:4200';
                    }
                    document.location = prefix + '/campaigns/' + campaignId + '/donate/' + response.result + '/payment';
                } else {
                    console.log(response.result)
                    workspace.navigate(donateThisShareUrl);
                }
            }).fail(function (err) {
                console.log(err);
                workspace.navigate(donateThisShareUrl);
            });
        }
    },

    isValid: function () {
        //Попробовать убрать selectedShare
        var selectedShare = this.controller.get('selectedShare');
        if (!selectedShare || this.model.get('shareId') !== selectedShare.get('shareId')) {
            this.controller.switchShare(this.model, false);
        }
        var errMessage;
        this.$('.action-card_counter').removeClass('error');
        if (!this.controller.isValidDonateAmount() ||
            this.controller.get('status') === "FINISHED") {
            if (this.model.get('isDonate')) {
                errMessage = 'Сумма не может быть меньше одного рубля.';
            } else {
                var quantity = this.model.get('quantity');
                errMessage = 'СТОИМОСТЬ ' + quantity + ' ВОЗНАГРАЖДЕНИ' + StringUtils.plurals(quantity, ['Й', 'Я', 'Й']) +
                    ' НЕ МОЖЕТ БЫТЬ МЕНЬШЕ ' + quantity * this.model.get('price') + ' ₽';
            }
            this.$('.action-card_counter').addClass('error');
            this.$('.js-donate-amount-error').text(errMessage);
            return false;
        }
        return true;
    }
    ,
    tryIncreaseQuantity: function (e) {
        e.stopPropagation();
        //Попробовать убрать selectedShare
        var selectedShare = this.controller.get('selectedShare');
        if (!selectedShare || this.model.get('shareId') !== selectedShare.get('shareId')) {
            this.controller.switchShare(this.model, false);
        }
        var quantity = this.model.get('quantity');
        this.changeQuantity(quantity + 1);
    }
    ,
    tryDecreaseQuantity: function (e) {
        e.stopPropagation();
        //Попробовать убрать selectedShare
        var selectedShare = this.controller.get('selectedShare');
        if (!selectedShare || this.model.get('shareId') !== selectedShare.get('shareId')) {
            this.controller.switchShare(this.model, false);
        }
        var quantity = this.model.get('quantity');
        this.changeQuantity(quantity - 1);
    }
    ,

    changeQuantity: function (quantity) {
        if (this.model.tryChangeQuantity(quantity)) {
            var textCollapsed = this.$el.find('.action-card').hasClass('text-collapsed');

            this.updateQuantityModifiers(quantity);
            this.model.set({
                quantity: quantity,
                donateAmount: quantity * this.model.get('price')
            });

            this.$el.find('.action-card').addClass('focus active').removeClass('inactive');
            this.$el.find('.action-card .action-card_action').css('height', 'auto');

            if (textCollapsed) {
                this.$el.find('.action-card').addClass('text-collapsed');
                this.$el.find('.action-card .action-card_descr-text').css('height', 108);
            }

            this.controller.set({
                donateAmount: this.model.get('donateAmount')
            });
        }
    }
    ,

    updateQuantityModifiers: function (quantity) {
        var decDisabled, incDisabled;
        decDisabled = !this.model.isEnoughQuantity(quantity - 1);
        incDisabled = !this.model.isEnoughQuantity(quantity + 1);
        this.$('.js-decrease').prop('disabled', decDisabled);
        this.$('.js-increase').prop('disabled', incDisabled);
    }
});

CrowdFund.Views.ShareItem = CrowdFund.Views.BaseShareItem.extend({});

CrowdFund.Views.BaseShareList = BaseListView.extend({
    itemViewType: CrowdFund.Views.BaseShareItem,
    tagName: "ul",
    className: "action-card-list"
});

CrowdFund.Views.ShareList = CrowdFund.Views.BaseShareList.extend({
    itemViewType: CrowdFund.Views.ShareItem,

    afterRender: function () {
        var sharesSaled = this.$('.js-sales');
        for (var i = 0; i < sharesSaled.length; i++) {
            if (i !== 0) {
                sharesSaled[i].remove();
            }
        }
    }
});

CrowdFund.Views.PreviewShareList = CrowdFund.Views.BaseShareList.extend({
    itemViewType: CrowdFund.Views.BaseShareItem
});

CrowdFund.Views.CampaignsItem = BaseView.extend({
    template: "#top-campaigns-item-template",
    className: "project-card-item",
    injectionHtml: 'projectCardHtml',
    injectionFunc: 'projectCard',
    afterRender: function () {
        var self = this;
        StringUtils.hyphenate(this);
        self.model.get('tags').forEach(function (it) {
            if (it.mnemonicName === 'INVESTING') {
                self.$el.addClass('project-card-item-investing');
            }
        });
    }
});

CrowdFund.Views.RelatedCampaign = CrowdFund.Views.CampaignsItem.extend({
    template: "#related-campaigns-item-template",
    events: {
        'click': 'onClick'
    },
    onClick: function () {
        if (window.gtm) {
            window.gtm.trackClickCampaignCard(this.model);
        }
    }
});

CrowdFund.Views.RelatedCampaignsList = BaseListView.extend({
    className: "project-card-list",
    itemViewType: CrowdFund.Views.RelatedCampaign,
    onReset: function () {
        if (window.gtm) {
            if (this.collection.length) {
                window.gtm.trackViewCampaignList(this.collection.toArray(), "Только что поддержали");
            }
        }
        return BaseListView.prototype.onReset.apply(this, arguments);

    }
});

CrowdFund.Views.CampaignPage = BaseView.extend({
    construct: function (options) {
        this.addChild(new CrowdFund.Views.Campaign({
            model: this.model.getCampaignModel(),
            controller: options.controller
        }));
    }
});

CrowdFund.Views.ProjectAlert = BaseView.extend({
    template: '#project-alert-template',
    message: null,
    afterRender: function () {
        var self = this;
        if (!self.message) {
            $.ajax({
                url: '/api/campaign/last-campaign-moderation-message.json',
                data: {
                    campaignId: this.model.get('campaignId')
                },
                success: function (response) {
                    self.message = response.result;
                    if (self.message) {
                        self.message = self.message.replace(/\r\n/g, "<br />");
                        self.$('.moderator-message').html(self.message);
                    } else {
                        self.message = "no message";
                    }
                }
            });
        }

        if (self.message && self.message !== "no message") {
            self.$('.moderator-message').html(self.message);
        }

    }
});

CrowdFund.Views.RelatedCampaigns = BaseView.extend({
    template: '#related-campaigns-template',
    construct: function (options) {
        var relatedCampaignsCollection = new CrowdFund.Models.RelatedCampaignsList([], {
            data: {
                objectId: options.campaignId
            }
        });
        this.addChildAtElement('.js-project-card-list-container', new CrowdFund.Views.RelatedCampaignsList({
            collection: relatedCampaignsCollection
        }));
        setTimeout(function () {
            relatedCampaignsCollection.load();
        }, 2000);
    }
});
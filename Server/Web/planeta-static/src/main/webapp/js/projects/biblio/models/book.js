/**
 * Издание 
 */
Biblio.Models.Book = BaseModel.extend({
    defaults: {
        bookId: 0,
        title: '',
        description: 'Научно-популярный журнал широкого профиля для семейного чтения и самообразования. ' +
                'Издается с 1890 года. Освещает важнейшие аспекты науки и техники, публикует научно-художественные ' +
                'и фантастические произведения, занимательные задачи и игры.',
        price: 0,
        newPrice: 0,
        imageUrl: '',
        periodicity: '12 номеров в год',
        bonus: 'Электронный архив за 2000-2015годы',
        comment: 'Цена одной подписки 4 500 руб. включает в себя стоимость 5 номеров издания за 2016 год и их доставки в библиотеку.',
        publishingHouseName: 'Наука и Жизнь'
    }
});

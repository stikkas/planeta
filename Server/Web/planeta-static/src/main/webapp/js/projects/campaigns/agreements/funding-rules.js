/*global Form, CrowdFund, ProfileUtils, Modal, Campaign*/
var FundingRules = {};
FundingRules.View = BaseView.extend({
    template: "#funding-rules-template",
    className: 'founding-rules',
    events: {
        'change input[type=checkbox][name=acceptRules]': 'toggleAgree',
        'click .create-campaign': 'onCreateCampaignClick'
    },
    toggleAgree: function (e) {
        if ($(e.currentTarget).attr('checked')) {
            this.$('.create-campaign').removeClass('disabled');
        } else {
            this.$('.create-campaign').addClass('disabled');
        }
    },
    onCreateCampaignClick: function (e) {
        e.preventDefault();
        if ($(e.currentTarget).is('.disabled')) {
            return;
        }

        if (!workspace.appModel.hasEmail()) {
            location.href = 'https://' + workspace.serviceUrls.mainHost + '/welcome/account-merge-email.html?' + $.param({redirectUrl: location.href});
        }

        Modal.startLongOperation();
        this.model.loadMyGroups().done(function () {
            if (this.get('myGroups').length === 0) {
                Campaign.Models.createCampaign(workspace.appModel.get("profileModel").get("profileId"));
            } else {
                var dialogView = new CrowdFund.Views.SelectCampaignDialog({
                    model: new BaseModel({
                        groups: this.get('myGroups')
                    })
                });
                Modal.finishLongOperation();
                dialogView.render();
            }
        }).fail(function () {
            Modal.finishLongOperation();
        });
    },

    afterRender: function () {
        var $agreeCheckbox = this.$('input[type=checkbox][name=acceptRules]');
        $agreeCheckbox.checkbox({
            wrapperClassName: 'checkbox-row'
        });
    }
});

FundingRules.Model = BaseModel.extend({
    defaults: {
        myGroups: [],
        ownerGroupIsSet: false
    },
    initialize: function (options) {
        var ownerModel = options.profileModel;

        if (ownerModel.get('profileType') === 'GROUP' || ownerModel.get('profileType') === 'HIDDEN_GROUP') {
            this.set({
                ownerGroupIsSet: true,
                groupAlias: ProfileUtils.getUserLink(ownerModel.get('profileId'), ownerModel.get('alias')),
                groupId: ownerModel.get('profileId')
            }, {silent: true});
        }
        this.set({
            isAuthorized: workspace.isAuthorized
        }, {silent: true});
    },
    prefetch: function (_options) {
        var success = _options.success;
        var options = {
            url: '/api/public/custom-html.json',
            data: {},
            context: this,
            success: function (responce) {
                if (responce.success) {
                    this.set({
                        customHtml: responce.result
                    });
                    success();
                }
            }
        };
        Backbone.sync('read', null, options);
    },
    loadMyGroups: function () {
        var $dfd = $.Deferred();
        var self = this;
        if (!this.get('isAuthorized')) {
            return $dfd.reject();
        }

        var myOptions = {
            url: '/campaigns/get-my-retained-campaigns.json',
            success: function (response) {
                if (response.success) {
                    self.set({
                        myGroups: response.result
                    });
                    $dfd.resolveWith(self, [response.result]);
                } else {
                    $dfd.reject();
                }
            },

            error: function () {
                $dfd.reject();
            }
        };
        Backbone.sync('read', null, myOptions);
        return $dfd.promise();
    },

    /**
     * For async call after model was fetched
     * @see AppView.changePageData()
     * @return {Object}
     */
    pageData: function () {
        return {
            title: 'Создайте проект краудфандинга на Planeta.ru!',
            description: 'Условия и правила для авторов проектов на платформе краудфандинга Planeta.ru. Расскажите о своей идее, найдите единомышленников и они поддержат вас!',
            keywords: 'коллективное финансирование, краудфандинговый сервис, спонсирование проектов,  сервис коллективного финансирования проектов'
        };
    }
});
/*globals Delivery, Modal*/
/**
 *
 * @author Andrew
 * Date: 31.10.13
 * Time: 21:27
 */


Delivery.Views.InModalAdditor = Delivery.Views.ModalEditor.extend({
    template: '#admin-delivery-add-base-in-liked-service-template',

    events: _.extend({}, Delivery.Views.ModalEditor.prototype.events, {
        'click .js-save-delivery': 'save'
    }),
    className: 'js-empty',
    constructor: function(options) {
        Delivery.Views.ModalEditor.prototype.constructor.call(this, options);
        this.container = options.container;
    },
    save: function(options) {
        var self = this;
        var $dfd = Delivery.Views.ModalEditor.prototype.save.call(self);
        $dfd.done(function(service) {
            self.container.model.set(_.extend({ isNew: false }, service));
        });

    },
    cancel: function(e) {
        this.$el.slideUp();
        $('form.js-link-delivery-layer > .control-group > .controls > [name]').removeAttr('disabled');
        $('#save-linked-service').removeClass('disabled');
    },
    beforeRender: function() {},
    afterRender: function() {
        this.$('.elastic').elastic();
    }
});
Delivery.Views.ModalAdditor = Modal.View.extend({
    template: '#admin-delivery-add-liked-service-template',

    events: _.extend({}, Modal.View.prototype.events, {
        'click .js-add-new-base-delivery': 'toggleRegisterDeliveryBlock',
        'keyup input[name]': 'onInputChanged',
        'keyup textarea[name]': 'onInputChanged',
        'drop input[name]': 'onInputChanged',
        'drop textarea[name]': 'onInputChanged',
        'change input[data-type=numerical]': 'onNumberInputChanged',
        'change input[name]': 'onInputChanged'

    }),
    construct: function(options) {
        this.controllerModel = options.controllerModel;
        if (this.model.get('isNew')) {

            this.addChildAtElement('.anchor-add-delivery', new Delivery.Views.InModalAdditor({
                model: new Delivery.Models.BaseService({enabled: true}),
                container: this
            }));
        }
    },
    toggleRegisterDeliveryBlock: function(e) {
        var $anchor = this.$('.anchor-add-delivery');
        if ($anchor.css('display') != 'none') {
            $anchor.slideUp();
            this.$('form.js-link-delivery-layer > .control-group > .controls > [name]').removeAttr('disabled');
            this.$('#save-linked-service').removeClass('disabled');
        } else {
            $anchor.slideDown();
            this.$('form.js-link-delivery-layer > .control-group > .controls > [name]').attr('disabled', 'disabled');
            this.$('#save-linked-service').addClass('disabled');
        }
    },
    onInputChanged: function(e) {
        var el = this.$(e.currentTarget);
        this.model.set(el.attr('name'), el.val(), {silent: true});
        this.model.trigger('change:' + el.attr('name'), el.val());
    },
    selectDeliveryService: function(data) {
        this.model.set(data);
    },

    save: function(options) {
        var self = this;
        this.model.update().done(function(result) {
            if (self.controllerModel) {
                var linkedDeliveries = self.controllerModel.get('linkedDeliveries');
                if (linkedDeliveries instanceof BaseCollection) {
                    var linkedServiceItem = _.find(linkedDeliveries.models, function(item) {
                        return item.get('serviceId') == result.serviceId;
                    });
                    if (linkedServiceItem) {
                        linkedServiceItem.set(result);
                    } else {
                        linkedDeliveries.add(result);
                    }
                } else {
                    linkedServiceItem = _(linkedDeliveries).find(function(item) {
                        return item.serviceId == result.serviceId;
                    });
                    if (linkedServiceItem) {
                        _.extend(linkedServiceItem, result);
                    } else {
                        linkedDeliveries.push(result);
                    }
                    self.controllerModel.change();
                }
            }
            self.cancel();
        }).fail(_.bind(Delivery.ViewUtils.highlightErrors, this));

    },
    refreshServicesList: function() {
        if (this.model.get('serviceId')) {
            return;
        }
        var self = this;
        if (this.model.get('isNew')) {
            this.$deliverySelector = self.$('input[name=deliveryService]');
            var baseServicesForSelection = self.model.baseServicesForSelection;
            baseServicesForSelection.load({
                success: function(servicesList) {
                    var items = [];
                    _.each(servicesList, function(model, index) {
                        var label = model.name + ' (' + model.location.name + ')';
                        items.push({
                            label: label,
                            value: label,
                            model: model
                        });
                    });
                    self.$deliverySelector.autocomplete({
                        minLength: 0,
                        source: items,
                        appendTo: self.$('.base-services-list'),
                        change: function(event, ui) {
                            if (ui.item) {
                                self.selectDeliveryService(ui.item.model);
                            }
                        }
                    }).focus(function() {
                        $(this).autocomplete("search", "");
                    });

                }
            });
        }
    },
    onNumberInputChanged: function(e) {
        var $input = this.$(e.currentTarget);
        var value = $input.val();
        if (value != (parseInt(value, 10) || 0)) {
            $input.val(parseInt(value, 10) || 0);
        }
    },
    afterRender: function() {
        Modal.View.prototype.afterRender.call(this);
        this.$('.elastic').elastic();

        this.refreshServicesList();
    }

});

Delivery.Views.ShareItem = BaseView.extend({
    tagName: 'tr',
    template: '#admin-campaign-info-share-list-item-template',
    events: {
        'click .remove-delivery-service': 'removeDeliveryService',
        'click .edit-delivery-service': 'editDeliveryService',
        'click .add-delivery-service': 'addDeliveryService',
        'click .js-setinvesting': 'setInvesting'
    },
    removeDeliveryService: function(e) {
        var data = $(e.currentTarget).data();
        var self = this;
        Modal.showConfirm("Вы точно хотите удалить эту запись?", "Подтверждение удаления службы доставки", {
            success: function() {
                var options = {
                    url: '/admin/delivery/remove-linked-delivery.json',
                    data: data,
                    success: function(responce) {
                        if (responce.success) {
                            var services = self.model.get('linkedDeliveries');

                            self.model.set({
                                linkedDeliveries: services.filter(function(item) {
                                    return item.serviceId != data.serviceId;
                                })
                            });

                        }
                    }
                };
                Backbone.sync('update', {}, options);
            }
        });
    },
    editDeliveryService: function(e) {
        var data = this.$(e.currentTarget).data();
        data.isNew = false;
        new Delivery.Views.ModalAdditor({
            model: new Delivery.Models.LinkedService(data),
            controllerModel: this.model
        }).render();
    },

    setInvesting: function(e) {
        var data = {
            shareId : this.model.get('shareId'),
            shareStatus: this.model.get('shareStatus')
        };
        var options = {
            url: '/admin/set-share-to-investing.json',
            data: data
        };
        Backbone.sync('update', {}, options);
    },
    addDeliveryService: function(e) {
        new Delivery.Views.ModalAdditor({
            model: new Delivery.Models.LinkedService({
                isNew: true,
                subjectId: this.model.get('shareId'),
                subjectType: 'SHARE'
            }),
            controllerModel: this.model
        }).render();
    },
    afterRender: function() {
        if (this.model.get('price') == 0) {
            this.$el.hide();
        }
    }
});
Delivery.Views.ShareList = BaseListView.extend({
    itemViewType: Delivery.Views.ShareItem
});



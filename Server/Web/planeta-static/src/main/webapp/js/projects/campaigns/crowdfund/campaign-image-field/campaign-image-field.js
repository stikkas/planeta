/*global CrowdFund*/
if (!window.CrowdFund) {
    var CrowdFund = {Views:  {}, Models : {}};
}
CrowdFund.Views.ImageField = BaseView.extend({
    template: "#campaign-image-field-template",
    className: "project-create_row",
    modelEvents: {
        'destroy': 'dispose',
        'change:imageUrl': 'onImageUrlChange',
        'change:errors': 'render'
    },

    initialize: function () {
        BaseView.prototype.initialize.apply(this, arguments);
        this.onImageUrlChange();
    },

    onImageUrlChange: function () {
        var view;
        if (this.loadView) {
            this.loadView.dispose();
            this.loadView = null;
        }
        if (this.cropView) {
            this.cropView.dispose();
            this.cropView = null;
        }
        if (this.videoView) {
            this.videoView.dispose();
            this.videoView = null;
        }

        if (this.model.get('videoUrl')) {
            this.videoView = view = this.createVideoView();
        } else if (this.model.get('imageUrl')) {
            this.cropView = view = this.createCropView();
        } else {
            this.loadView = view = this.createLoadView();
        }
        if (this.model.onImageUrlChange) {
            this.model.onImageUrlChange(this.controller);
        }
        this.addChildAtElement('.js-val', view);
    },

    createVideoView: function () {
        return new CrowdFund.Views.ImageField.VideoView({model: this.model, controller: this.controller});
    },

    createCropView: function () {
        return new CrowdFund.Views.ImageField.CropView({model: this.model, controller: this.controller});
    },

    createLoadView: function () {
        return new CrowdFund.Views.ImageField.LoadView({model: this.model, controller: this.controller});
    },

    cancel: function () {
        this._deletePhoto(this.model.get('croppedImage'));
        this._deletePhoto(this.model.get('loadedImage'));
    },

    save: function () {
        if (this.model.get("isImageDeleted")) {
            this._deletePhoto(this.model.get('originalImage'));
        }
        var croppedImage = this.model.get('croppedImage');
        var loadedImage = this.model.get('loadedImage');
        if (loadedImage && croppedImage && croppedImage.photoId != loadedImage.photoId && loadedImage.photoId) {
            this._deletePhoto(loadedImage);
        }
    },
    _deletePhoto: function (image) {
        if (image && image.photoId && image.profileId) {
            Backbone.sync("delete", null, {
                url: "/api/profile/deleteByProfileId-photo.json",
                data: {
                    profileId: image.profileId || this.model.get("profileId"),
                    photoId: image.photoId
                }
            });
        }
    },

    _deleteVideo: function (video) {
        if (video && video.videoId) {
            Backbone.sync("delete", null, {
                url: "/api/profile/deleteByProfileId-video.json",
                data: {
                    profileId: video.profileId || this.model.get("profileId"),
                    videoId: video.videoId
                }
            });

        }
    }
});

CrowdFund.Models.ImageField = BaseModel.extend({
    defaults: {
        albumId: 0,
        errors: {},
        sizeSuggestionText: '640x360'
    },

    initialize: function () {
        BaseModel.prototype.initialize.apply(this, arguments);
        if (!this.get('clientId')) {
            this.set('clientId', workspace.appModel.get('myProfile').get('profileId'));
        }
        if (this.get('video')) {
            this.set({
                videoUrl: this.get('video').originalVideo.videoUrl,
                videoProfileId: this.get('video').originalVideo.videoProfileId,
                videoId: this.get('video').originalVideo.videoId,
                videoType: this.get('video').originalVideo.videoType
            }, {silent: true});
        }
        if (this.get('originalImage')) {
            this.set({
                imageUrl: this.get('originalImage').imageUrl,
                imageId: this.get('originalImage').photoId
            });
        }
        this.defaults = _.clone(this.attributes);
    }
});

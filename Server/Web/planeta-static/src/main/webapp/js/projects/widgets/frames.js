/**
 * Created with IntelliJ IDEA.
 * User: a.volkov
 * Date: 30.10.13
 * Time: 16:26
 */
(function($) {
    var Frame = function (el, options) {
        this.el = el;
        this.fn = options || {};
        this.bindEvents(options.events);
        delete this.fn.events;
        this.trigger('init');
    };
    var eventSplitter = /^(\S+)\s*(.*)$/;
    Frame.prototype = {
        //some part from Backbone
        bindEvents: function(events) {
            events = events || {};
            for (var key in events) {
                var method = events[key];
                if (!_.isFunction(method)) method = this.fn[events[key]];
                if (!method) throw new Error('Event "' + events[key] + '" does not exist');
                var match = key.match(eventSplitter);
                var eventName = match[1], selector = match[2];
                method = $.proxy(method, this);
                if (selector === '') {
                    this.el.bind(eventName, method);
                } else {
                    this.el.delegate(selector, eventName, method);
                }
            }
        },

        next: function () {
            var frameEl = this.getNext();
            if (frameEl && this.validate()) {
                var frame = frameEl.data('planeta.frame');
                this.hide();
                frame.show();
            }
        },
        getNext: function () {
            var nextFrameName = this.el.attr('data-frame-next');
            var frameEl;
            if (nextFrameName) {
                frameEl = $('[data-frame=' + nextFrameName + ']');
            }
            return frameEl;
        },
        $: function(x) {
            return this.el.find(x);
        },
        prev: function () {
            var prevFrameName = this.el.attr('data-frame-prev');
            if (prevFrameName) {
                var frameEl = $('[data-frame=' + prevFrameName + ']');
                if (frameEl) {
                    var frame = frameEl.data('planeta.frame');
                    this.hide();
                    frame.show();
                }
            }
        },
        trigger: function (event) {
            if ($.isFunction(this.fn[event])) {
                this.fn[event].apply(this);
            }
        },
        show: function () {
            this.el.show();
            this.trigger('show');
            if (!this.el.data('planeta.scrollbar')) {
                var winHeight = $(window).height();
                var headerHeight = this.$('.project-reward-header').outerHeight();
                var footerHeight = this.$('.project-reward-footer').outerHeight();

                var scrollHeight = winHeight - headerHeight - footerHeight;

                this.$('.modal-scroll-content').scrollbar({
                    shadow: true,
                    shadowSize: 20,
                    shadowOnOff: false,
                    arrows: false,
                    scrollWheelStep: 70,
                    containerSize: scrollHeight
                });
                this.el.data('planeta.scrollbar', true);
            } else {
                this.layout();
            }
        },
        hide: function () {
            this.el.hide();
            this.trigger('hide');
        },
        layout: function () {
            this.$('.modal-scroll-content').scrollbar('repaint');
        },
        highliteErrors: function(fieldErrors) {
            var inputs = this.$('input:visible, textarea:visible, selectCampaignById:visible');
            inputs.closest('.input-field.error').removeClass('error').find('.error-message').remove();
            _(fieldErrors).each(function(message, fieldName) {
                var panel = inputs.filter('[name=' + fieldName + ']:first').closest('.input-field');
                panel.addClass('error');
                panel.append($('<div class="help-inline error-message"></div>').text(message))
            });
        },
        validate: function () {
            var valid = true;
            var inputs = this.$('input:visible, textarea:visible');
            var errInput;
            inputs.each(function () {
                var that = $(this);
                that.parent().find(".help-inline").addClass("hidden");
                var value = this.value;
                var err = $('<div class="help-inline error-message"></div>');
                var errText;
                var panel = that.closest('.input-field');
                panel.removeClass('error').find('.error-message').remove();
                if (that.attr('required') && value.length == 0) {
                    errText = 'Обязательное поле';
                } else if (that.attr('pattern') && !new RegExp(that.attr('pattern'), 'gi').test(value)) {
                    errText = 'Неверное значение поля';
                }
                if (errText) {
                    if (!errInput) {
                        errInput = that;
                    }
                    if (panel.length) {
                        panel.addClass('error');
                        err.text(errText);
                        panel.append(err);
                    }
                    valid = false;
                }
            });
            if (errInput) {
                this.layout();
                setTimeout(function () {
                    errInput.attr('data-scroll', true);
                    var element = $('[data-scroll = "true"]');
                    $('.modal-scroll-content').scrollbar('scrollto', element);
                    errInput.focus();
                    errInput.parent().find(".help-inline").removeClass("hidden");
                }, 0);
            }
            return valid;
        }
    };
    $.fn.frame = function (options) {
        var el = this.filter(':first');
        var frame = el.data('planeta.frame');
        if (!frame) {
            frame = new Frame(el, options);
            el.data('planeta.frame', frame);
        }
        return frame;
    };
})(jQuery);

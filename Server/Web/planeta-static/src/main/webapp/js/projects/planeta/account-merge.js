/*globals Confirmation,BaseModel,_,$,workspace,Form,Backbone,BaseView*/

var AccountMerge = {
    Models: {},
    Views: {}
};

AccountMerge.Models.Content = BaseModel.extend({
    defaults: {
        alreadyHaveEmail: false,
        open: false
    },

    initialize: function (options) {
        BaseModel.prototype.initialize.call(this, options);

        var namesMap = {
            VK: "ВКонтакте",
            FACEBOOK: "Facebook",
            YANDEX: "Яндекс",
            ODNOKLASSINKI: "Одноклассники",
            MAILRU: "@MAIL.RU"
        };

        var credentialsInfo = this.get('credentialsInfo');
        if (credentialsInfo) {
            this.set({
                'authModelPhoto': credentialsInfo.externalPhotoUrl || null,
                'serviceName': namesMap[credentialsInfo.credentialType],
                'email': credentialsInfo.email
            });
        }
        var userAuthorizationInfo = this.get('userAuthorizationInfo');
        if (userAuthorizationInfo) {
            var credentialType = userAuthorizationInfo.username.replace(/\d+/ig, '').toUpperCase();
            this.set({
                'authModelPhoto': userAuthorizationInfo.profile.imageUrl || null,
                'credentialsInfo': {
                    userName: userAuthorizationInfo.username,
                    credentialType: credentialType,
                    credentialStatus: 'ACTIVE',
                    password: ''
                },
                'serviceName': namesMap[credentialType],
                'email': userAuthorizationInfo.email || ''
            });
        }
        if (options.email) {
            this.set({
                'email': options.email || ''
            });
        }

    },

    sendEmail: function (data, options) {
        data = _.extend({}, this.get('credentialsInfo'), data, {
            hasEmail: this.get('alreadyHaveEmail')
        });
        if (data.emailPassword == null) {
            data.emailPassword = "";
        }

        var sendOptions = {
            data: data,
            url: '/api/profile/save-new-user-email.json',
            context: this,
            success: function (response) {
                if (response.success && response.fieldErrors) {
                    location.href = "/"; //workspace.navigate('/') ?
//                    options.confirm(response.fieldErrors);
                } else if (response.success) {
                    var successCallback = options.success || function () {
                        workspace.appView.showSuccessMessage("Данные сохранены");
                    };
                    successCallback(response);
                } else {
                    var errorCallback = options.error || function () {
                        workspace.appView.showErrorMessage("Проверьте введенные данные");
                    };
                    Form.isValid(response);
                    errorCallback(response);
                }
            },
            error: function (response) {
                workspace.appView.showErrorMessage("Ошибка обращения к серверу");
            }
        };

        Backbone.sync('update', this, sendOptions);
    }

});

AccountMerge.Views.Content = BaseView.extend({

    template: '#account-merge-content',
    events: {
        "click .already-have-account": "alreadyHaveEmail",
        "click .new-account": "newEmail",
        "submit .form-inline": "sendEmail",
        "click .alien-link": "toggleHaveEmail"
    },


    alreadyHaveEmail: function () {
        this.model.set({
            alreadyHaveEmail: true
        });
        if (!this.model.get('open')) {
            this.model.set('open', true, {silent: true});
            this.$el.find(".already-have-account-block").slideToggle(300);
        }
    },

    newEmail: function () {
        this.model.set('alreadyHaveEmail', false);
        if (!this.model.get('open')) {
            this.model.set('open', true, {silent: true});
            this.$el.find(".already-have-account-block").slideToggle(300);
        }
    },

    toggleHaveEmail: function () {
        this.model.set('alreadyHaveEmail', !this.model.get('alreadyHaveEmail'));
    },

    sendEmail: function (e) {
        if (this.emailIsChecked) {
            return;
        }
        e.preventDefault();
        var self = this;
        if (self.confirmationView) {
            self.confirmationView.dispose();
            self.confirmationView = null;
        }
        var data = this.$el.find(".form-inline").serializeObject();
        self.model.set('email', data.email);

        var onConfirmSuccess = function () {
            if (self.model.has('userAuthorizationInfo')) {
                document.location.href = '/welcome/session-login.html?' + $.param({email: self.model.get('email')});
            } else {
                document.location.href = '/welcome/session-login.html';
            }
        };

        this.model.sendEmail(data, {
            success: function (response) {
                if (self.model.get('alreadyHaveEmail')) {
                    onConfirmSuccess();
                } else {
                    self.emailIsChecked = true;
                    self.$el.find(".form-inline").submit();
                }
            },
            error: function (response) {
                workspace.appView.showErrorMessage("Проверьте введенные данные");
                Form.isValid(response);
                $(".help-inline.error-message").insertAfter(".submit-already-have-email");
            },
            fatal: function (response) {
                workspace.appView.showErrorMessage("Ошибка обращения к серверу");
            },
            confirm: function (errors) {
                workspace.appView.showErrorMessage("Пользователь с таким e-mail'ом уже зарегистрирован");
                var UUID = errors.email;
                var textHtml = "Введите <a href='javascript:void(0)' class='alien-link'>пароль</a> или введите код " +
                    "подтверждения, который был выслан на Ваш e-mail";
                var confirmationModel = new Confirmation.Model({
                    confirmationId: UUID,
                    confirmationTextHtml: textHtml
                });
                confirmationModel.onConfirmSuccess = onConfirmSuccess;
                var confirmationView = new Confirmation.View({
                    model: confirmationModel
                });
                self.confirmationView = confirmationView;
                self.addChildAtElement(".confirmation-panel", confirmationView);
                self.render();

                $(".register-email")[0].disabled = true;
                $(".submit-already-have-email").remove();

                self.$el.find(".already-have-account-block").show();
            }
        });
    }
});

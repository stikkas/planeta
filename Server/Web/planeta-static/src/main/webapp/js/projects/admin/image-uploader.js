var ImageUploader = {
    Models: {},
    Views: {}
};

ImageUploader.Models.Main = BaseModel.extend({

    initialize: function(options) {
        this.set({
            profileId: workspace.appModel.get('myProfile').get('profileId'),
            justUploadedImages: new BaseCollection([]),
            earlierUploadedImages: new ImageUploader.Models.UploadedImageCollection([])
        });

        this.get('earlierUploadedImages').load();
    },

    uploadImages: function() {
        var self = this;
        UploadController.showUploadImages(self.get('profileId'), function(filesUploaded) {
            if (_.isEmpty(filesUploaded)) return;

            var images = _.map(filesUploaded.models, function(fileUploaded) {
                return new BaseModel(fileUploaded.get('uploadResult'));
            });

            self.get('justUploadedImages').add(images);
            self.get('earlierUploadedImages').defaults.offset = _.size(self.get('justUploadedImages').models);
        });
    },

    deleteImage: function(model) {
        var self = this;

        var options = {
            url: '/api/profile/deleteByProfileId-photo.json',
            data: {
                profileId: this.get('profileId'),
                photoId: model.get('photoId')
            },
            context: this,
            success: function (response) {
                if (response.success) {
                    model.collection.remove(model);
                    workspace.appView.showSuccessMessage('Файл удален', 2500);
                } else {
                    workspace.appView.showErrorMessage(response.errorMessage, 2500);
                }
            }
        };

        Backbone.sync('delete', this, options);
    }

});

ImageUploader.Views.ImageUpload = BaseView.extend({
    el: '#center-container',
    template: '#image-upload-template',

    events: {
        'click .upload-image': 'onImagesUploadClicked'
    },

    construct: function(options) {
        this.addChildAtElement('.images-placeholder', new ImageUploader.Views.ImageList({
            collection: this.model.get('justUploadedImages')
        }));

        this.addChildAtElement('.last-uploaded-images-placeholder', new ImageUploader.Views.ImageList({
            collection: this.model.get('earlierUploadedImages')
        }));

        this.bind('onDeleteImage', this.onDeleteImage, this);
        this.bind('onShowLinks', this.onShowLinks, this);
    },

    onImagesUploadClicked: function(e) {
        this.model.uploadImages();
    },

    onDeleteImage: function(view) {
        this.model.deleteImage(view.model);
    },

    onShowLinks: function(view) {
        var view = new ImageUploader.Views.ImageSizeDialog({
            model: view.model
        });

        Modal.showDialog(view);
    }
});

ImageUploader.Views.ImageSizeDialog = Modal.View.extend({
    template: '#image-size-links-template',
    events: {
        'click a.close': 'cancel',
        'click button[type=reset]': 'cancel',
        'change #image-size': 'onImageSizeClicked'
    },

    afterRender: function() {
        $(':input[type=radio]', this.el).radioButton({wrapperClassName: "radio-row"});
        this.onImageSizeClicked();
    },

    onImageSizeClicked: function(e) {
        var imageSize = this.$el.find('#image-size').serializeObject().imageSize;
        var imageUrl = this.model.get('imageUrl');
        var imageLink = ImageUtils.hasOwnProperty(imageSize)
                ? ImageUtils.getThumbnailUrl(imageUrl, ImageUtils[imageSize], ImageType.PHOTO)
                : imageUrl;

        $('#image-preview').attr('href', imageLink);
        $('#image-link').val(imageLink);
    }

});

ImageUploader.Views.Image = BaseView.extend({
    template: '#image-template',
    className: 'photo-list-item',
    viewEvents: {
        'click .delete': 'onDeleteImage',
        'click .show-links': 'onShowLinks'
    }
});

ImageUploader.Views.ImageList = BaseListView.extend({
    itemViewType: ImageUploader.Views.Image,
    className: 'photo-list'
});

ImageUploader.Models.UploadedImageCollection = BaseCollection.extend({
    url: '/moderator/last-uploaded-images.json',

    remove: function(models, options) {
        BaseCollection.prototype.remove.call(this, models, options);
        this.load();
    }

});

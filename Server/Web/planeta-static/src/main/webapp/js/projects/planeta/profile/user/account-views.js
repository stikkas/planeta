/*global BaseView,$*/
Account.Views.PersonalCabinet = BaseView.extend({
    template: '#personal-cabinet-template',
    collections: {
    },
    views: {
    },
    events: {
        'click .js-pager': 'search',
        'click .js-tab': 'changeTab',
        'click .private-office-balance_btn': 'increaseBalance'
    },
    construct: function (options) {
        var self = this,
                tabs = {
                    'purchases': {col: Account.Collections.Purchases, view: Account.Views.Purchases},
//                'transactions': 'Transactions',
//                     'cards': {col: Account.Collections.Cards, view: Account.Views.Cards},
//                     'regulars': {col: Account.Collections.Regulars, view: Account.Views.Regulars},
                    'billings': {col: Account.Collections.Billings, view: Account.Views.Billings}
                };
        self.curTab = workspace.navigationState.get('subsection');
        if (!self.collections[self.curTab]) {
            // o_O > what is going on here?
            self.collections[self.curTab] = new tabs[self.curTab].col(); // if there is no "self.curTab" in tabs then an error will be here -> .col()
            self.views[self.curTab] = new tabs[self.curTab].view({
                collection: self.collections[self.curTab]});
            // < o_O
        }
        options.controller.pageData = this.pageData();
    },
    changeTab: function (e) {
        var tabName = $(e.currentTarget).attr("data-tab");
        if (tabName !== this.curTab) {
            this.views[this.curTab].offset = 0;
            workspace.navigate('/account/' + tabName);
        }
    },
    afterRender: function () {
        var self = this,
                searchInput = self.$('input[name="searchCriteria"]'),
                curView = self.views[self.curTab];
        $('.js-tab').each(function (i, it) {
            var item = $(it),
                    tabName = item.attr('data-tab');
            if (tabName === self.curTab) {
                item.addClass('active');
            } else {
                item.removeClass('active');
            }
        });
        Account.searchString = Account.searchString || searchInput.val();
        curView.loadEl = self.$('.js-loader');
        curView.loadBtn = self.$('.js-pager');
        curView.loadMore = self.$('.js-pager-loader');
        self.addChildAtElement('#list-items', curView);
        searchInput.bind('keyup', _.debounce(function (e) {
            Account.searchString = $(e.currentTarget).val();
            self.search(e);
        }, 300));
    },
    increaseBalance: function () {
        if (!LazyHeader.checkNotAnonymous()) {
            return;
        }
        moduleLoader.loadModule('payment').done(function () {
            var model = new UserPayment.Models.BasePayment({
                redirectUrl: '/profile/increase-balance.html',
                oldPayment: true
            });
            Modal.showDialogByViewClass(UserPayment.Views.BalancePaymentDialog, model);
        });
    },
    search: function () {
        var self = this;
        if (self.curTab === 'purchases' || self.curTab === 'billings') {
            self.views[self.curTab].render();
        }
    },
    pageData: function () {
        var creatorProfile = workspace.appModel.get('profileModel');
        var displayName = creatorProfile.get('displayName');
        var title = $('title').html();
        var description = $('meta[name=description]').html();

        switch (workspace.navigationState.get('subsection')) {
            case 'purchases':
                title = 'Мои покупки';
                description = _.template('Покупки <%=displayName%> на платформе краудфандинга Planeta.ru', {
                    displayName: displayName
                });
                break;
        }

        var result = {
            title: title,
            description: description
        };

        return result;
    }
});
Account.Views.CollectView = BaseView.extend({
    afterRender: function () {
        var self = this,
                loadEl = self.offset > 0 ? self.loadMore : self.loadEl;
        $('.js-empty').hide();
        $('.js-private-office-block').show();
        loadEl.show();
        self.collection.load({data: {searchString: Account.searchString,
                limit: self.limit + 1, offset: self.offset}, // получаем на один больше, но отображаем только limit 
            // этот костыль нужен для обработки пограничных условий, когда кол-во последней порции совпадает с limit
            // т.к. мы не передаем общее кол-во.
            success: function (items) {
                if (items.length === 0) {
                    $('.js-empty').show();
                    $('.js-private-office-block').hide();
                    self.clear();
                    self.offset = 0;
                } else {
                    if (self.offset == 0)
                        for (var o in self._childViews)
                            self.removeChild(self._childViews[o]);
                    self.collection.each(function (model, i) {
                        if (i < self.limit)
                            self.addChildAtElement(self.chSelector, new self.chClass({
                                model: model
                            }));
                    });
                    if (items.length < self.limit + 1) {
                        self.loadBtn.hide();
                        self.offset = 0;
                    } else {
                        self.loadBtn.show();
                        self.offset += self.limit;
                    }
                }
            },
            complete: function () {
                loadEl.hide();
            }
        });
    }
});
Account.Views.Purchase = BaseView.extend({
    className: "private-office-purchases_i",
    template: '#account-purchase-template',
    events: {
        'click .private-office-product_collapse-link': 'showHideSlide'
    },
    showHideSlide: function () {
        this.$('.private-office-product_collapse').slideToggle(300);
    },
    construct: function () {
        var model = this.model,
                status = 'new',
                canPrint = false,
                orderType = model.get('orderType'),
                payStatus = model.get('paymentStatus'),
                isProduct = orderType === 'PRODUCT',
                isCompleted = payStatus === 'COMPLETED',
                isBonus = orderType === 'BONUS',
                isBiblio = orderType === 'BIBLIO',
                isDonate = orderType === 'DONATE',
                isShare = orderType === 'SHARE',
                isInvesting = (ProfileUtils.isInvestingProject(orderType)),
                deliveryAddress = model.get('deliveryAddress'),
                delAddress = [],
                delStatus = model.get('deliveryStatus');
        ["zipCode", "country", "city", "address"].forEach(function (prop) {
            var value = deliveryAddress[prop];
            if (value)
                delAddress.push(value);
        });
        canPrint = isProduct || isShare || isBonus || isInvesting || isBiblio;
        if (payStatus == 'CANCELLED') {
            status = 'canceled';
        } else if (delStatus == 'DELIVERED') {
            status = 'delivered';
        } else if(delStatus == 'GIVEN') {
            status = 'given';
        } else if (delStatus == 'IN_TRANSIT') {
            status = 'sent';
        } else if (isCompleted) {
            status = 'paid';
        }

        var products = {}, prods = [];
        if (isBiblio) {
            var libs = {count: 0, items: []};
            model.set('libs', libs, {silent: true});
        }

        model.get('orderObjectsInfo').forEach(function (it) {
            if (it.objectType === "LIBRARY") {
                libs.count++;
                libs.items.push(it);
            } else {
                var product = products[it.objectId];
                if (product)
                    ++product.count;
                else
                    products[it.objectId] = it;
            }
        });
        for (var o in products) {
            prods.push(products[o]);
        }
        model.set('orderObjectsInfo', prods, {silent: true});
        model.set({status: status,
            canPrint: canPrint,
            isCompleted: isCompleted,
            isProduct: isProduct,
            isDonate: isDonate,
            isBonus: isBonus,
            isBiblio: isBiblio,
            isShare: isShare,
            delAddress: delAddress.join(", "),
            phone: deliveryAddress.phone,
            isInvesting: isInvesting}, {silent: true});
    },
    afterRender: function () {
        var status = this.model.get('status');
        if (status != 'new')
            this.$el.addClass(status);
    }
});
Account.Views.Purchases = Account.Views.CollectView.extend({
    limit: 10,
    offset: 0,
    className: 'pob-purchases-wrap',
    template: '#account-purchases-template',
    chSelector: '.private-office-purchases',
    chClass: Account.Views.Purchase
});
Account.Views.Billing = BaseView.extend({
    className: 'private-office-purchases_i',
    template: '#account-billing-template',
    afterRender: function () {
        if (this.model.get('status') == 'DONE')
            this.$el.addClass('paid');
        else
            this.$el.addClass('not-paid');
    }

});
Account.Views.Billings = Account.Views.Purchases.extend({
    chClass: Account.Views.Billing
});
/*
 Account.Views.Transactions = BaseView.extend({
 className: 'pob-purchases-wrap',
 template: '#account-transactions-template',
 afterRender: function () {
 this.addChildAtElement('.payments.table. > tbody', new Account.Views.Transaction({
 }));
 }
 });
 */

Account.Views.Cards = BaseView.extend({
    className: 'pob-purchases-wrap',
    afterRender: function () {
        this.addChild(new Account.Views.Card({
        }));
    }
});
Account.Views.Regulars = BaseView.extend({
    className: 'pob-purchases-wrap',
    afterRender: function () {
        this.addChild(new Account.Views.Regular({
        }));
    }
});
/*
 Account.Views.Transaction = BaseView.extend({
 tagName: 'tr',
 template: '#account-transaction-template'
 });
 */

Account.Views.Card = BaseView.extend({
    className: 'pob-purchases-block',
    template: '#account-card-template'
});
Account.Views.Regular = BaseView.extend({
    className: 'pob-purchases-block pob-purchases-block__regular',
    template: '#account-regular-template'
});

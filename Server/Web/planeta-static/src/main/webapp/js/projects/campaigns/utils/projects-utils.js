var CampaignUtils = (function () {

    var statusTypes = {
        ALL: {name: 'Все проекты', engName: 'All campaigns'},
        LAST_UPDATED: {name: 'Последние обновленные', engName: 'Last updated'},
        SUCCESSFUL: {name: 'Успешные', engName: 'Successful'},
        RECORD_HOLDERS: {name: 'Рекордсмены', engName: 'Top funded'},
        NEW: {name: 'Новые', engName: 'New campaigns'},
        CLOSE_TO_FINISH: {name: 'Близкие к завершению', engName: 'Close to completion'},
        DISCUSSED: {name: 'Обсуждаемые', engName: 'Recently commented'},
        CHARITY: {name: 'Благотворительные', engName: 'Charity'},
        ACTIVE: {name: 'Активные', engName: 'Recently funded'}
    };

    var statuses = {
        'DRAFT': 'черновик',
        'PAUSED': 'приостановлен',
        'ACTIVE': 'запущен',
        'FINISHED': 'завершен',
        'APPROVED': 'утверждён',
        'NOT_STARTED': 'на модерации',
        'DELETED': 'удален'
    };

    var getStatusName = function (status) {
        return statuses[status];
    };

    var getShareDisplayName = function (name, price) {
        if (!name) {
            return "Вознаграждение за " + price + " руб.";
        }
        return name;
    };

    var CampaignFeedbackForm = Form.View.extend({
        clickOff: true,
        className: 'modal modal-project-feedback size-transition',
        template: '#campaign-feedback-form-template',
        events: _.extend({}, Form.View.prototype.events, {
            'click .next-step': 'flowToStep2'
        }),
        urlByTarget: {
            'author': '/campaign/contacts-send-author-feedback.json',
            'curators': '/campaign/contacts-send-curator-feedback.json'
        },
        construct: function () {
            if (!this.model.get('authorsCount')) {
                var target = "curators";
                this.model.set({
                    step: 2,
                    target: target,
                    url: this.urlByTarget[target]
                });
            }
        },
        flowToStep2: function (e) {
            e.preventDefault();
            var target = $(e.currentTarget).data('target');

            if (target === 'author') {
                var self = this;
                if (!workspace.isAuthorized) {
                    LazyHeader.showAuthForm('signup');
                    workspace.appView.showErrorMessage("Анонимный пользователь не может начать диалог с автором");
                    return;
                }
                $.get('/campaign/is-it-possible-to-send-mail-to-author.json', {
                    campaignId: self.model.get('campaignId')
                }).done(function (response) {
                    if (response && response.success) {
                        if (!response.result) {
                            workspace.appView.showErrorMessage("Автор не оставил контактов для связи. Попробуйте связаться с куратором от «Планеты»");
                            self.$('a[data-target="author"]').attr("disabled", true);
                        } else {
                            self.keepFlowingToStep2(self, target);
                        }
                    } else {
                        workspace.appView.showErrorMessage(response.errorMessage || "Попробуйте связаться с куратором от «Планеты»");
                    }
                });
            } else {
                if (target === 'curators') {
                    this.$('.js-curators-feedback-info').removeClass('hide');
                }
                this.keepFlowingToStep2(this, target);
            }
        },

        keepFlowingToStep2: function (self, target) {
            self.$el.flowForm('.modal-project-feedback-step-1', '.modal-project-feedback-form');

            self.model.set({
                url: self.urlByTarget[target]
            }, {silent: true});
        },

        save: function (e) {
            e.preventDefault();
            var self = this;
            this.disableButtons();
            this.model.saveForm(this.serializeData(), function () {
                self.$el.flowForm('.modal-project-feedback-form', '.modal-feedback-success');
            });

        },
        beforeRender: function () {
            this.$el.removeAttr('style');
            Form.View.prototype.beforeRender.call(this);

        },
        afterRender: function () {
            Form.View.prototype.afterRender.call(this);
        }
    });

    var openCampaignFeedback = function (campaignId, authorsCount) {
        var model = new Form.Model({
            itemModel: new BaseModel({
                campaignId: campaignId
            }),
            campaignId: campaignId,
            authorsCount: authorsCount,
            askEmail: !workspace.appModel.hasEmail()
        });
        model.endEdit = function (result) {
            $(document).unbind('keydown.modalForm').bind("keydown.modalForm", function (e) {
                e.preventDefault();
                model.destroy();
                $(document).unbind('keydown.modalForm');
            });
        };
        var view = new CampaignFeedbackForm({
            model: model
        });
        Modal.showDialog(view);
    };

    var trackPixel = function (campaignId) {
        function addVkPixel(pixelId) {
            console.log('vkTargetingId: ' + pixelId);
            var img = new Image();
            img.src = 'https://vk.com/rtrg?p='
                + pixelId;
        }
        function addFbPixel(pixelId) {
            console.log('fbTargetingCode: ' + pixelId);
            var img = new Image();
            img.style = 'display: none;';
            img.src = 'https://www.facebook.com/tr?id=' + pixelId
                + '&ev=PageView&noscript=1';
        }

        $.get('/api/public/campaign-targeting', {campaignId: campaignId}, function (response) {
            if (response.success && response.result) {
                if (response.result.vkTargetingId) {
                    addVkPixel(response.vkTargetingId);
                }

                if (response.result.fbTargetingId) {
                    addFbPixel(response.fbTargetingId);
                }
            }
        });
    };

    return {
        statusTypes: statusTypes,
        getStatusName: getStatusName,
        getShareDisplayName: getShareDisplayName,
        openCampaignFeedback: openCampaignFeedback,
        trackPixel: trackPixel,
        getCampaignTags: function () {
            if (!this.tagsDfd) {
                this.tagsDfd = Backbone.sync('read', null, {
                    url: '/api/welcome/get-campaign-tags.json'
                });
            }
            return this.tagsDfd;
        }
    };
}());

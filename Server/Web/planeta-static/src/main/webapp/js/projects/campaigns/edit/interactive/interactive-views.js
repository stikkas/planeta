if (!CampaignEdit.Views.Interactive) {
    CampaignEdit.Views.Interactive = {};
}
CampaignEdit.Views.Interactive.Header = BaseView.extend({
    template: '#campaign-interactive-header-template',
    events: {
        'click .js-open-navigation-bar': 'openNavBar'
    },

    construct: function () {
        $('body').on('player.share', function () {
            Modal.showDialogByViewClass(CampaignEdit.Views.Interactive.SharingModal, null, null, true, null, null, true);
            var sharingData;
            if (!sharingData) {
                var data = {
                    title: 'Создай свой проект за 60 минут!',
                    counterEnabled: false,
                    hidden: false,
                    className: 'donate-sharing'
                };

                data.url = 'https://planeta.ru/interactive';
                data.description = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi';
                data.image = 'https://s3.planeta.ru/i/17451b/original.jpg';
                sharingData = data;
            }

            $('.js-interactive-sharing').share(sharingData);
        });
    },

    openNavBar: function (e) {
        var toggleClassName = 'header_steps-nav__open';
        var navBar = this.$('.header_steps-nav');
        if (navBar.hasClass(toggleClassName)) {
            navBar.removeClass(toggleClassName);
        } else {
            navBar.addClass(toggleClassName);
        }
    },

    afterRender: function () {
        var arcOptions = {
            procent: 0,
            radius: 5,
            fill: '#fff',
            strokeWidth: 10,
            strokeBgColor: '#e4f1ff',
            strokeColor: '#4da1ff'
        };
        var arcDays = this.$el.find('.steps-progress_bar');
        arcDays.arcDraw(arcOptions);
        arcDays.arcDraw('set', this.model.get('arcValue'));

        this.$el.find('.js-services-popup').dropPopup({
            trigger: '.pln-d-switch',
            popup: '> .pln-d-popup',
            closeTrigger: '.pln-d-close',
            activeClass: 'open'
        });

        this.$el.find('.steps-nav_name').click(function (el) {
            el.preventDefault();
            if (typeof player !== 'undefined') {
                //player.player.removeRemoteTextTrack(player.track);
                player.destroyAllControls();
                player.player.loop(false);
                player.player.off('ended');
                player.player.off('pause', this.loopPauseDisable);
            }
            $('body').off('player.repeat');

            workspace.navigate($(el.currentTarget).attr('href'));
        });

        /*this.$el.clickOff(function (e) {
         self.$('.pln-dropdown').removeClass('open');
         });*/
    }
});

CampaignEdit.Views.Interactive.Sharing = BaseView.extend({
    template: '#campaign-interactive-sharing-template'
});

CampaignEdit.Views.Interactive.Base = BaseView.extend({
    template: '#campaign-interactive-main-template',
    formTemplate: '',
    nextStepUrl: '',

    events: {
        'click .js-next-step': 'sendForm',
        'click .js-next-step-b': 'sendFormB'

    },

    dispose: function () {
        $('.js-header').removeClass('hidden');
        $('.header:not(.js-interactive-header)').removeClass('hidden');
        $('#js-header-special-project').removeClass('hidden');
        $('.js-footer').removeClass('hidden');
        BaseView.prototype.dispose.call(this);
    },

    playAgain: function () {
        player.player.off('pause', this.loopPauseDisable);
        this.setFullVideo();
        // if (!this.model.get('showFormAlways')) {
        this.model.setShowForm(false);
        // }
    },

    nextStep: function (url) {
        if (typeof player !== 'undefined') {
            player.destroyAllControls();
            player.player.loop(false);
            player.player.off('ended');
            player.player.off('pause', this.loopPauseDisable);
        }
        $('body').off('player.repeat');
        // $('body').off('player.share');
        if (url) {
            workspace.navigate(url);
        } else {
            workspace.navigate(this.nextStepUrl);
        }
    },

    sendForm: function (e) {/*override this*/
        e.preventDefault();
        this.nextStep();
    },
    sendFormB: function (e) {
        e.preventDefault();
        this.nextStep();
    },
    checkErrorsForm: function (formObject) {
        /*override this*/
    },

    afterRender: function () {
        $('.js-header').addClass('hidden');
        $('.header:not(.js-interactive-header)').addClass('hidden');
        $('#js-header-special-project').addClass('hidden');
        $('.js-footer').addClass('hidden');
        $('#common-css-id').remove();
    },

    loadVideoModule: function (callback, secondVariant) {
        var self = this;
        loadModule("interactive-video-player").done(function () {
            player.init();
            player.destroyAllControls();
            $('body').off('player.repeat');

            if (!self.model.get('showForm')) {
                player.normalView();
                if (secondVariant) {
                    player.player.src(playlist[self.model.get('activeIndex')].video2);
                    player.track = player.addRemoteTextTrack(
                            playlist[self.model.get('activeIndex')].track2
                            , true);
                } else {
                    player.player.src(playlist[self.model.get('activeIndex')].video);
                    player.track = player.addRemoteTextTrack(
                            playlist[self.model.get('activeIndex')].track
                            , true);
                }

                player.player.one('ended', function () {
                    if (callback) {
                        callback(self);
                    } else {
                        player.destroyAllControls();
                        self.model.setShowForm(true);
                        self.setLoopInstance();
                    }
                });
            } else {
                self.setLoopInstance();
            }

        });
    },

    setLoopInstance: function () {
        var self = this;
        self.model.set({showForm: true}, {silent: false});

        player.loopView();
        player.player.src(playlist[this.model.get('activeIndex')].loop);

        player.player.removeRemoteTextTrack(player.track);

        if (!!player.captions.toggleCaptions) {
            player.captions.toggleCaptions(false);
        }

        player.player.loop(true);
        player.player.on('pause', this.loopPauseDisable);


        $('body').one('player.repeat', function () {
            player.player.off('pause', self.loopPauseDisable);
            self.setFullVideo();
            // if (!self.model.get('showFormAlways')) {
            //self.model.setShowForm(false);
            // }
        });

        setTimeout(function () {
            $('body').trigger('initLoopInstance');
        }, 100);

        self.model.setShowForm(true);
    },

    loopPauseDisable: function () {
        player.player.play();
    },

    setFullVideo: function () {
        player.destroyAllControls();
        // if (!this.model.get('showFormAlways')) {
        this.model.set({showForm: false});
        // }

        player.normalView();
        player.player.src(playlist[this.model.get('activeIndex')].video);
        player.track = player.addRemoteTextTrack(
                playlist[this.model.get('activeIndex')].track
                , true);

        // player.player.play();
        player.player.loop(false);

        var self = this;
        player.player.one('ended', function () {
            player.destroyAllControls();
            self.setLoopInstance();
            self.model.setShowForm(true);
        });
    }
});

CampaignEdit.Views.Interactive.Start = CampaignEdit.Views.Interactive.Base.extend({
    template: '#campaign-interactive-start-template',
    nextStepUrl: '/interactive/name',

    construct: function () {
        this.loadVideoModule();
    },

    sendForm: function (e) {/*override this*/
        e.preventDefault();
        StorageUtils.setInteractiveStepNameStartSecondVariant(false);
        this.nextStep();
    },

    sendFormB: function (e) {
        e.preventDefault();
        StorageUtils.setInteractiveStepNameStartSecondVariant(true);
        this.nextStep();
    }
});

CampaignEdit.Views.Interactive.Name = CampaignEdit.Views.Interactive.Base.extend({
    template: '#campaign-interactive-name-template',
    nextStepUrl: '/interactive/email',

    construct: function () {
        var isSecondVariant = StorageUtils.isInteractiveStepNameStartSecondVariant() || false;
        this.loadVideoModule(null, isSecondVariant);
    },

    afterRender: function () {
        if (workspace.isAuthorized) {
            this.$el.find('input[name=name]').attr('readonly', true);
        }
    },

    sendForm: function (e) {
        e.preventDefault();
        _.each(this.$('#interactive-fio-form').find('.tooltip'), function (el) {
            $(el).attr('data-tooltip', '');
        });

        var personDetails = this.$('#interactive-fio-form').serializeObject();
        var validateResponse = this.checkErrorsForm(personDetails);
        if (personDetails && validateResponse.success) {
            console.log('save displayName from personDetails.name: ' + personDetails.name);
            StorageUtils.setInteractiveDraftCampaignUserName(personDetails.name);
            StorageUtils.setInteractiveDraftCampaignAgreeStorePersonalData(personDetails.agree);
            this.nextStep();
        } else {
            Form.highlightErrors(validateResponse.errors, this.$('#interactive-fio-form'), "video-form_field", true);
            _.each(validateResponse.errors, function (errorValue, errorKey) {
                var el = this.$('#interactive-fio-form').find('.js-tooltip-' + errorKey);
                el.attr('data-tooltip', errorValue);
            });
        }
    },

    checkErrorsForm: function (formObject) {
        var response = {
            success: true,
            errors: {}
        };
        if (formObject) {
            if (!formObject.name) {
                response.success = false;
                response.errors.name = 'Обязательное поле!';
            }
            if (!formObject.agree) {
                response.success = false;
                response.errors.agree = 'Обязательное поле!';
            }
        } else {
            response.success = false;
            response.errors.name = 'Обязательное поле!';
            response.errors.agree = 'Обязательное поле!';
        }

        return response;
    }
});

CampaignEdit.Views.Interactive.Email = CampaignEdit.Views.Interactive.Base.extend({
    template: '#campaign-interactive-email-template',
    nextStepUrl: '/interactive/proceed',

    construct: function () {
        this.loadVideoModule();
    },

    afterRender: function () {
        if (workspace.isAuthorized) {
            this.$el.find('input[name=email]').attr('readonly', true);
        }
    },

    sendForm: function (e) {
        e.preventDefault();
        if (workspace.isAuthorized) {
            var self = this;
            $.post('/admin/campaign-save-email-step.json').done(function (response) {
                if (response && response.success) {
                    self.nextStep();
                } else {
                    workspace.appView.showErrorMessage("Непредвиденная ошибка", 3000);
                }
            });
        } else {
            _.each(this.$('#interactive-email-form').find('.tooltip'), function (el) {
                $(el).attr('data-tooltip', '');
            });

            var personDetails = this.$('#interactive-email-form').serializeObject();
            var validateResponse = this.checkErrorsForm(personDetails);
            if (personDetails && validateResponse.success) {
                StorageUtils.setInteractiveDraftCampaignUserEmail(personDetails.email);

                var self = this;
                self.model.set({
                    email: personDetails.email
                }, {silent: true});

                if (self.model.get('email') && self.model.get('name')) {
                    $.post('/api/school-way/create-user.json', {email: self.model.get('email'),
                        firstName: self.model.get('name'),
                        password: 123123}).done(function (response) {
                        if (response && response.success) {
                            document.location.href = /*"/welcome/cas-redirect.html?successUrl=" + */self.nextStepUrl;
                        } else {
                            if (response && response.fieldErrors && response.fieldErrors.email) {
                                if (response.fieldErrors.email == 'Пользователь с таким e-mail уже зарегистрирован') {
                                    LazyHeader.showAuthForm('signup', {email: self.model.get('email'),
                                        focusPassword: true});
                                } else {
                                    Form.highlightErrors(response.fieldErrors, self.$('#interactive-email-form'), "video-form_field", true);
                                    _.each(response.fieldErrors, function (errorValue, errorKey) {
                                        var el = self.$('#interactive-email-form').find('.js-tooltip-' + errorKey);
                                        el.attr('data-tooltip', errorValue);
                                    });
                                }
                            } else if (!workspace.isAuthorized) {
                                LazyHeader.showAuthForm('signup', {email: self.model.get('email'),
                                    focusPassword: true});
                            }
                        }
                    });
                }
            } else {
                var self = this;
                Form.highlightErrors(validateResponse.errors, self.$('#interactive-email-form'), "video-form_field", true);
                _.each(validateResponse.errors, function (errorValue, errorKey) {
                    var el = self.$('#interactive-email-form').find('.js-tooltip-' + errorKey);
                    el.attr('data-tooltip', errorValue);
                });
            }
        }
    },

    checkErrorsForm: function (formObject) {
        var response = {
            success: true,
            errors: {}
        };
        if (formObject) {
            if (!formObject.email) {
                response.success = false;
                response.errors.email = 'Обязательное поле!';
            }
        } else {
            response.success = false;
            response.errors.email = 'Обязательное поле!';
        }

        return response;
    }
});

CampaignEdit.Views.Interactive.Proceed = CampaignEdit.Views.Interactive.Base.extend({
    template: '#campaign-interactive-proceed-template',
    nextStepUrl: '/interactive/info',

    construct: function () {
        this.loadVideoModule();
    },

    sendForm: function (e) {
        e.preventDefault();
        var self = this;
        $.post('/admin/campaign-save-proceed-step.json').done(function (response) {
            if (response && response.success) {
                self.nextStep();
            } else {
                workspace.appView.showErrorMessage("Непредвиденная ошибка", 3000);
            }
        });
    }
});

CampaignEdit.Views.Interactive.Info = CampaignEdit.Views.Interactive.Base.extend({
    template: '#campaign-interactive-info-template',
    nextStepUrl: '/interactive/duration',

    construct: function () {
        var self = this;
        if (!workspace.isAuthorized) {
            document.location.href = this.model.get('baseUrl');
        } else {
            this.loadVideoModule();
            var attrs = {model: self.model.campaign};
            this.addChildAtElement('.video-form-row_list', new CampaignEdit.Views.Interactive.InfoTags({
                model: self.model.campaign,
                campaignTags: self.model.campaignTags,
                dropdownCssClass: 'video-form-select2-drop'
            }));
            this.addChildAtElement('.video-form-row_list', new CampaignEdit.Views.Interactive.InfoName(attrs));
            this.addChildAtElement('.video-form-row_list', new CampaignEdit.Views.Interactive.InfoImageAndDescription({
                imageModel: self.model.cardImageModel,
                model: self.model.campaign
            }));
            this.addChildAtElement('.video-form-row_list', new CampaignEdit.Views.Interactive.NextButton(attrs));
            $.get("/api/campaign/track-campaign-interactive-step-info.json", {campaignId: self.model.campaign.get('campaignId')});
        }
    },

    sendForm: function (e) {
        e.preventDefault();
        var self = this;
        this.model.campaign.saveCampaign(false, null, '/admin/campaign-save-info-step.json').done(function (response) {
            if (response.result) {
                self.nextStep();
            } else if (!response.success) {
                console.log('Ошибка при сохранении проекта');
                if (response.fieldErrors) {
                    Form.highlightErrors(response.fieldErrors, self.$el, "video-form_field", true);
                    _.each(response.fieldErrors, function (errorValue, errorKey) {
                        var el = self.$el.find('.js-tooltip-' + errorKey);
                        el.attr('data-tooltip', errorValue);
                    });
                }
            }
        });
    }
});

CampaignEdit.Views.Interactive.NextButton = BaseView.extend({
    template: '#campaign-interactive-next-button-template',
    className: 'video-form_row'
});

CampaignEdit.Views.Interactive.InfoImageAndDescription = BaseView.extend({
    template: '#campaign-interactive-info-image-and-description-template',
    className: 'video-form_row',

    construct: function (options) {
        this.addChildAtElement('.js-campaign-image', new CampaignEdit.Views.Interactive.InfoImage({
            model: options.imageModel,
            controller: this.model
        }));
        this.addChildAtElement('.js-short-description', new CampaignEdit.Views.Interactive.InfoShortDescription({model: this.model}));
    }
});

CampaignEdit.Views.Interactive.InfoName = CampaignEdit.Views.BaseInput.extend({
    template: '#campaign-interactive-info-name-template',
    className: 'video-form_row',
    modelAttr: 'name',
    onInputChange: function () {
        CampaignEdit.Views.BaseInput.prototype.onInputChange.apply(this, arguments);
        /*var escapedName = $("<div></div>").text(this.model.get('name')).html();
         this.model.set('nameHtml', escapedName);*/
    }
});

CampaignEdit.Views.Interactive.InfoShortDescription = CampaignEdit.Views.TextArea.extend({
    template: '#campaign-interactive-info-short-description-template',
    className: 'video-form_field',
    modelAttr: 'shortDescription'
});

CampaignEdit.Views.Interactive.InfoTags = CampaignEdit.Views.Categories.extend({
    template: '#campaign-interactive-info-tags-template',
    className: 'video-form_row',
    modelAttr: 'tags'
});

CampaignEdit.Views.Interactive.InfoImage = CampaignEdit.Views.Image.extend({
    template: '#campaign-interactive-info-image-template',
    className: 'video-form_field',
    modelAttr: 'imageUrl'
});

CampaignEdit.Views.Interactive.Duration = CampaignEdit.Views.Interactive.Base.extend({
    template: '#campaign-interactive-duration-template',
    nextStepUrl: '/interactive/price',

    construct: function () {
        if (!workspace.isAuthorized) {
            document.location.href = this.model.get('baseUrl');
        } else {
            this.loadVideoModule();
            $.get("/api/campaign/track-campaign-interactive-step-duration.json", {campaignId: this.model.campaign.get('campaignId')});
        }
    },

    afterRender: function () {
        CampaignEdit.Views.Interactive.Base.prototype.afterRender.apply(this, arguments);

        var self = this;
        var slider = $('#slider');
        var handle = $('#slider-handle');
        var tooltip = $('.ui-slider-handle-tooltip');

        var durationValue = 60;
        if (self.model.campaign && self.model.campaign.get('timeFinish')) {
            durationValue = DateUtils.getDaysBetween(self.model.campaign.get('timeFinish'), DateUtils.endOfDate(new Date().getTime())) + 1;
        } else {
            self.model.campaign.set({
                timeStart: new Date(),
                timeFinish: new Date(DateUtils.addDays(new Date().getTime(), durationValue - 1))
            }, {silent: true});
        }
        slider.slider({
            min: 30,
            max: 100,
            value: durationValue,
            range: "min",
            animate: true,
            create: function () {
                tooltip.text($(this).slider('value') + ' ' + DateUtils.getNumEnding($(this).slider('value'), Date.timeEndings().days));
            },
            slide: function (event, ui) {
                tooltip.text(ui.value + ' ' + DateUtils.getNumEnding(ui.value, Date.timeEndings().days));
            },
            change: function (event, ui) {
                self.model.campaign.set({
                    timeStart: new Date(),
                    timeFinish: new Date(DateUtils.addDays(new Date().getTime(), ui.value - 1))
                }, {silent: true});
            }
        });
    },

    sendForm: function (e) {
        e.preventDefault();
        var self = this;
        this.model.campaign.saveCampaign(null, null, '/admin/campaign-save-duration-step.json').done(function (response) {
            if (response.result) {
                self.nextStep();
            } else if (!response.success) {
                console.log('Ошибка при сохранении проекта');
                if (response.fieldErrors) {
                    Form.highlightErrors(response.fieldErrors, this.$el, "video-form_field", true);
                    _.each(response.fieldErrors, function (errorValue, errorKey) {
                        var el = self.$el.find('.js-tooltip-' + errorKey);
                        el.attr('data-tooltip', errorValue);
                    });
                }
            }
        });
    }
});

CampaignEdit.Views.Interactive.Price = CampaignEdit.Views.Interactive.Base.extend({
    template: '#campaign-interactive-price-template',
    nextStepUrl: '/interactive/video',

    modelEvents: {
        'change:showForm': 'render',
        'destroy': 'dispose'
    },

    construct: function () {
        if (!workspace.isAuthorized) {
            document.location.href = this.model.get('baseUrl');
        } else {
            this.loadVideoModule();
            var self = this;

            self.addChildAtElement('.js-desired-target-amount', new CampaignEdit.Views.Interactive.PriceDesiredTargetAmount({
                model: self.model
            }));

            self.addChildAtElement('.js-desired-collected-amount-percent', new CampaignEdit.Views.Interactive.PriceDesirecCollectedAmountPercent({
                model: self.model
            }));

            self.addChildAtElement('.js-contractor-type-percent', new CampaignEdit.Views.Interactive.PriceContractorTypePercent({
                model: self.model
            }));

            self.addChildAtElement('.js-visible-target-amount', new CampaignEdit.Views.Interactive.PriceVisibleTargetAmount({
                model: self.model
            }));
            $.get("/api/campaign/track-campaign-interactive-step-price.json", {campaignId: self.model.campaign.get('campaignId')});
        }
    },

    sendForm: function (e) {
        e.preventDefault();
        var self = this;
        this.model.campaign.saveCampaign(null, null, '/admin/campaign-save-price-step.json').done(function (response) {
            if (response.result) {
                self.nextStep();
            } else if (!response.success) {
                console.log('Ошибка при сохранении проекта');
                if (response.fieldErrors) {
                    Form.highlightErrors(response.fieldErrors, self.$el, "video-form_field", true);
                    _.each(response.fieldErrors, function (errorValue, errorKey) {
                        var el = self.$el.find('.js-tooltip-' + errorKey);
                        el.attr('data-tooltip', errorValue);
                    });
                }
            }
        });
    }
});

CampaignEdit.Views.Interactive.PriceDesirecCollectedAmountPercent = CampaignEdit.Views.BaseInputOrCheckbox.extend({
    template: '#campaign-interactive-price-desired-collected-amount-percent-template',
    modelAttr: 'desiredCollectedAmountPercent',
    className: 'price-calc_cell price-calc_slide-1',
    uncheckedValue: 10,
    checkedValue: 15,

    onCheckboxChange: function () {
        var $checkbox = this.getCheckbox();
        var bValue = !!$checkbox.attr('checked');
        if (bValue) {
            this.model.set(this.modelAttr, this.checkedValue);
            StorageUtils.setInteractiveDraftCampaignDesiredCollectedAmountPercent(this.checkedValue);
        } else {
            this.model.set(this.modelAttr, this.uncheckedValue);
            StorageUtils.setInteractiveDraftCampaignDesiredCollectedAmountPercent(this.uncheckedValue);
        }
        this.render();
    }
});

CampaignEdit.Views.Interactive.PriceContractorTypePercent = CampaignEdit.Views.BaseInputOrCheckbox.extend({
    template: '#campaign-interactive-price-contractor-type-percent-template',
    modelAttr: 'contractorTypePercent',
    className: 'price-calc_cell price-calc_slide-2',
    checkedValue: 6,
    uncheckedValue: 13,

    onCheckboxChange: function () {
        var $checkbox = this.getCheckbox();
        var bValue = !!$checkbox.attr('checked');
        if (bValue) {
            this.model.set(this.modelAttr, this.checkedValue);
            StorageUtils.setInteractiveDraftCampaignContractorTypePercent(this.checkedValue);
        } else {
            this.model.set(this.modelAttr, this.uncheckedValue);
            StorageUtils.setInteractiveDraftCampaignContractorTypePercent(this.uncheckedValue);
        }
        this.render();
    }
});

CampaignEdit.Views.Interactive.PriceDesiredTargetAmount = CampaignEdit.Views.BaseInput.extend({
    template: '#campaign-interactive-price-desired-target-amount-template',
    className: 'price-calc_cell',
    modelAttr: 'desiredTargetAmount',
    needRestoreCaretAfterRender: true,
    modelEvents: {
        'change': 'render',
        'destroy': 'dispose'
    },

    onInputChange: function () {
        CampaignEdit.Views.BaseInput.prototype.onInputChange.apply(this, arguments);
        if (this.model.get('desiredTargetAmount')) {
            var targetAmount = InteractiveUtils.calculateNeededTargetAmount(this.model.get('contractorTypePercent'),
                    this.model.get('desiredCollectedAmountPercent'),
                    this.model.get('desiredTargetAmount'));

            StorageUtils.setInteractiveDraftCampaignTargetAmount(targetAmount);
            this.model.set({desiredTargetAmount: 0}, {silent: true});
            this.model.set({targetAmount: targetAmount});
            this.model.campaign.set({targetAmount: targetAmount});
        }
    },

    afterRender: function () {
        this.$el.find('input[name=desiredTargetAmount]').inputmask({
            showMaskOnHover: false,
            rightAlign: false,
            placeholder: '0',
            alias: 'numeric',
            groupSeparator: ' ',
            autoGroup: true,
            digits: 0,
            suffix: ' ₽',
            autoUnmask: true
        });
        sessionStorage.getItem('lastFocusedInputField')
        if (this.needRestoreCaretAfterRender && (this.getInput().attr('id') == sessionStorage.getItem('lastFocusedInputField'))) {
            this.restoreCaretPosition();
        }
    }
});

CampaignEdit.Views.Interactive.PriceVisibleTargetAmount = CampaignEdit.Views.BaseInput.extend({
    template: '#campaign-interactive-price-visible-target-amount-template',
    className: 'price-calc_cell',
    modelAttr: 'targetAmount',
    needRestoreCaretAfterRender: true,
    modelEvents: {
        'change': 'render',
        'destroy': 'dispose'
    },

    onInputChange: function () {
        CampaignEdit.Views.BaseInput.prototype.onInputChange.apply(this, arguments);
        this.model.campaign.set({targetAmount: this.model.get('targetAmount')});
        StorageUtils.setInteractiveDraftCampaignTargetAmount(this.model.get('targetAmount'));
    },

    afterRender: function () {
        this.$el.find('input[name=targetAmount]').inputmask({
            showMaskOnHover: false,
            rightAlign: false,
            placeholder: '0',
            alias: 'numeric',
            groupSeparator: ' ',
            autoGroup: true,
            digits: 0,
            suffix: ' ₽',
            autoUnmask: true
        });
        if (this.needRestoreCaretAfterRender && (this.getInput().attr('id') == sessionStorage.getItem('lastFocusedInputField'))) {
            this.restoreCaretPosition();
        }
    }
});

CampaignEdit.Views.Interactive.Video = CampaignEdit.Views.Interactive.Base.extend({
    template: '#campaign-interactive-video-template',
    className: 'project-edit-container',
    nextStepUrl: '/interactive/description',

    construct: function () {
        if (!workspace.isAuthorized) {
            document.location.href = this.model.get('baseUrl');
        } else {
            this.loadVideoModule();
            this.addChildAtElement('.js-campaign-name', new CampaignEdit.Views.Interactive.VideoCampaignImage({
                model: this.model.campaign
            }));

            this.imageOrVideoView = new CampaignEdit.Views.Interactive.VideoCampaignViewImage({
                model: this.model.imageOrVideoModel,
                controller: this.model.campaign
            });

            this.addChildAtElement('.js-campaign-image', this.imageOrVideoView);
            this.addChildAtElement('.js-campaign-card', new CampaignEdit.Views.Interactive.VideoCampaignCard({
                model: this.model.campaign
            }));

            this.addChildAtElement('[data-anchor=tiny-mce]', new CampaignEdit.Views.Interactive.VideoDescriptionPlug({
                model: this.model.campaign
            }));

            this.addChildAtElement('.js-project-author', new CampaignEdit.Views.Interactive.VideoCampaignCreator({
                model: workspace.appModel.get('profileModel')
            }));

            this.addChildAtElement('.js-interactive-shares', new CampaignEdit.Views.Interactive.SharesList({
                model: this.model.campaign
            }));
            $.get("/api/campaign/track-campaign-interactive-step-video.json", {campaignId: this.model.campaign.get('campaignId')});


            $('body').off('initLoopInstance');
            $('body').on('initLoopInstance', function () {
                $('.js-write-down-btn').on('click', function () {
                    $('html, body').animate({scrollTop: $('.project-view-container').offset().top - 20}, 'slow');
                });
            });
        }
    },

    afterRender: function () {
        CampaignEdit.Views.Interactive.Base.prototype.afterRender.apply(this, arguments);
        this.$el.find('.js-interactive-shares').addClass('hold-block-transparent');
        this.$el.find('.pv-card-block').addClass('hold-block-transparent');
        this.$el.find('.project-tab-wrap').addClass('hold-block-white');
        fixedBtn.init('.project-edit-container_action', {
            type: 'bottom',
            offset: 590
        });
    },

    sendForm: function (e) {
        e.preventDefault();
        var self = this;
        this.model.campaign.saveCampaign(null, null, '/admin/campaign-save-video-step.json').done(function (response) {
            if (response.result) {
                self.nextStep();
            } else if (!response.success) {
                if (response.fieldErrors) {
                    Form.highlightErrors(response.fieldErrors, self.$el, "video-form-upload-project", true);
                }
            }
        });
    }
});

CampaignEdit.Views.Interactive.VideoDescriptionPlug = BaseView.extend({
    template: '#campaign-interactive-video-description-plug-template',
    className: 'sub-content'
});

CampaignEdit.Views.Interactive.CampaignDescription = BaseRichView.extend({
    template: '#campaign-interactive-video-description-template',
    className: 'sub-content hold-block',

    mediaTemplates: {
        audio: '#common-audio-template',
        photo: '#campaign-rich-photo-template',
        video: '#common-large-video-template'
    }
});

CampaignEdit.Views.Interactive.VideoCampaignViewImage = CampaignEdit.Views.ViewImage.extend({
    template: '#campaign-interactive-campaign-view-image-template',
    className: 'pv-media-view wrap-indent-right',
    modelAttr: 'viewImageUrl'
});

CampaignEdit.Views.Interactive.VideoCampaignImage = BaseView.extend({
    template: '#campaign-interactive-video-campaign-image-template'
});

CampaignEdit.Views.Interactive.VideoCampaignCard = BaseView.extend({
    template: '#campaign-card-template',
    className: 'project-author_block',
    modelEvents: {
        'destroy': 'dispose'
    },
    afterRender: function () {
        CrowdFund.Views.initOdometer(this, this.model);
    }
});

CampaignEdit.Views.Interactive.VideoCampaignCreator = BaseView.extend({
    template: '#campaign-creator-template'
});

CampaignEdit.Views.Interactive.Description = CampaignEdit.Views.Interactive.Base.extend({
    template: '#campaign-interactive-video-template',
    className: 'project-edit-container',
    nextStepUrl: '/interactive/reward',

    construct: function () {
        if (!workspace.isAuthorized) {
            document.location.href = this.model.get('baseUrl');
        } else {
            this.loadVideoModule();
            this.addChildAtElement('.js-campaign-name', new CampaignEdit.Views.Interactive.VideoCampaignImage({
                model: this.model.campaign
            }));

            this.addChildAtElement('.js-campaign-image', new CampaignEdit.Views.Interactive.DescriptionViewImage({
                model: this.model.campaign
            }));

            this.addChildAtElement('[data-anchor=tiny-mce]', new CampaignEdit.Views.DescriptionEdit({
                model: this.model.campaign
            }));

            this.addChildAtElement('.js-campaign-card', new CampaignEdit.Views.Interactive.VideoCampaignCard({
                model: this.model.campaign
            }));

            this.addChildAtElement('.js-project-author', new CampaignEdit.Views.Interactive.VideoCampaignCreator({
                model: workspace.appModel.get('profileModel')
            }));

            this.addChildAtElement('.js-interactive-shares', new CampaignEdit.Views.Interactive.SharesList({
                model: this.model.campaign
            }));
            $.get("/api/campaign/track-campaign-interactive-step-description.json", {campaignId: this.model.campaign.get('campaignId')});


            $('body').off('initLoopInstance');
            $('body').on('initLoopInstance', function () {
                $('.js-write-down-btn').on('click', function () {
                    $('html, body').animate({scrollTop: $('.project-tab-wrap').offset().top - 10}, 'slow');
                });
            });
        }
    },

    afterRender: function () {
        CampaignEdit.Views.Interactive.Base.prototype.afterRender.apply(this, arguments);
        this.$el.find('.js-interactive-shares').addClass('hold-block-transparent');
        this.$el.find('.project-view-header').addClass('hold-block-transparent');
        this.$el.find('.project-tab-i:not(.active)').addClass('hold-block-transparent');
        fixedBtn.init('.project-edit-container_action', {
            type: 'bottom',
            offset: 590
        });
    },

    sendForm: function (e) {
        e.preventDefault();
        var self = this;
        this.model.campaign.saveCampaign(null, null, '/admin/campaign-save-description-step.json').done(function (response) {
            if (response.result) {
                self.nextStep();
            } else if (!response.success) {
                console.log('Ошибка при сохранении проекта');
                if (response.fieldErrors) {
                    Form.highlightErrors(response.fieldErrors, self.$el, "project-description", true);
                }
            }
        });
    }
});

CampaignEdit.Views.Interactive.DescriptionViewImage = BaseView.extend({
    template: '#campaign-interactive-description-view-image-template',
    className: 'pv-media-view wrap-indent-right',

    afterRender: function () {
        var self = this;
        if (self.model.get('videoId')) {
            workspace.showVideoPlayer(self.model.get('videoProfileId'), self.model.get('videoId'), self.$el.find('.video-content'), {
                width: 640,
                height: 360,
                autostart: true,
                ignoreStorageEvents: true,
                ignoreVideoManager: true,
                fromCampaign: true,
                imageUrl: self.model.get('viewImageUrl'),
                imageId: self.model.get('viewImageId')
            });
        }
    }
});

CampaignEdit.Views.Interactive.Reward = CampaignEdit.Views.Interactive.Base.extend({
    template: '#campaign-interactive-video-template',
    className: 'project-edit-container',
    nextStepUrl: '/interactive/counterparty',

    construct: function () {
        if (!workspace.isAuthorized) {
            document.location.href = this.model.get('baseUrl');
        } else {
            this.loadVideoModule();
            this.addChildAtElement('.js-campaign-name', new CampaignEdit.Views.Interactive.VideoCampaignImage({
                model: this.model.campaign
            }));

            this.addChildAtElement('.js-campaign-image', new CampaignEdit.Views.Interactive.DescriptionViewImage({
                model: this.model.campaign
            }));

            this.addChildAtElement('[data-anchor=tiny-mce]', new CampaignEdit.Views.Interactive.CampaignDescription({
                model: this.model.campaign
            }));

            this.addChildAtElement('.js-campaign-card', new CampaignEdit.Views.Interactive.VideoCampaignCard({
                model: this.model.campaign
            }));

            this.addChildAtElement('.js-project-author', new CampaignEdit.Views.Interactive.VideoCampaignCreator({
                model: workspace.appModel.get('profileModel')
            }));

            this.addChildAtElement('.js-interactive-shares', new CampaignEdit.Views.Interactive.SharesList({
                model: this.model.campaign
            }));

            this.addChildAtElement('.js-interactive-sidebar', new CampaignEdit.Views.Interactive.AddShareButton({
                model: this.model.campaign,
                controller: this.model.campaign
            }));
            $.get("/api/campaign/track-campaign-interactive-step-reward.json", {campaignId: this.model.campaign.get('campaignId')});


            $('body').off('initLoopInstance');
            $('body').on('initLoopInstance', function () {
                $('.js-write-down-btn').on('click', function () {
                    $('html, body').animate({scrollTop: $('.interactive-shares').offset().top - 10}, 'slow');
                });
            });
        }
    },

    afterRender: function () {
        CampaignEdit.Views.Interactive.Base.prototype.afterRender.apply(this, arguments);
        this.$el.find('.project-view-header').addClass('hold-block-transparent');
        this.$el.find('.js-hold-block-white').addClass('hold-block-white');
        this.$el.find('.project-tab-wrap').addClass('hold-block-white');
        fixedBtn.init('.project-edit-container_action', {
            type: 'bottom',
            offset: 590
        });
    },

    sendForm: function (e) {
        e.preventDefault();
        var self = this;
        this.model.campaign.saveCampaign(null, null, '/admin/campaign-save-reward-step.json').done(function (response) {
            if (response.result) {
                self.nextStep();
            } else if (!response.success) {
                console.log('Ошибка при сохранении проекта');
                if (response.fieldErrors) {
                    Form.highlightErrors(response.fieldErrors, self.$el, "video-form_field", true);
                    _.each(response.fieldErrors, function (errorValue, errorKey) {
                        var el = self.$el.find('.js-tooltip-' + errorKey);
                        el.attr('data-tooltip', errorValue);
                    });
                }
            }
        });
    }
});

CampaignEdit.Views.Interactive.Counterparty = CampaignEdit.Views.Interactive.Base.extend({
    template: '#campaign-interactive-counterparty-template',
    className: 'project-edit-container',
    nextStepUrl: '/interactive/check',

    modelEvents: _.extend({}, CampaignEdit.Views.Interactive.Base.prototype.modelEvents, {
        'contractorValidationSuccess': 'nextStep'
    }),

    construct: function () {
        if (!workspace.isAuthorized) {
            document.location.href = this.model.get('baseUrl');
        } else {
            this.loadVideoModule();
            var self = this,
                    contractorsSelector,
                    contractors;

            $.get("/api/campaign/track-campaign-interactive-step-counterparty.json", {campaignId: self.model.campaign.get('campaignId')});

            Backbone.sync('read', null, {
                url: "/admin/contractor-list-by-profile.json",
                data: {
                    profileId: workspace.appModel.get("profileModel").get("profileId"),
                    offset: 0,
                    limit: 25
                }
            }).done(function (response) {
                if (response && response.success) {
                    if (contractorsSelector)
                        contractorsSelector.createContractorChoose(response.result);
                    else
                        contractors = response.result;
                }
            });
            self.model.contractor.fetch({success: function () {
                    var isNewContractor = !(self.model.contractor && self.model.contractor.contractorId);
                    var isCharity = !!self.model.campaign.get('tags').find(function (obj) {
                        return obj.mnemonicName === 'CHARITY';
                    });
                    if (isCharity && isNewContractor) {
                        self.model.contractor.set({isCharity: isCharity, type: "CHARITABLE_FOUNDATION"});
                    }
                    contractorsSelector = self.addChildAtElement('.project-create_cont', new CrowdFund.Views.CampaignContractorEdit({
                        model: self.model.contractor,
                        campaign: self.model.campaign
                    }));
                    if (contractors)
                        contractorsSelector.createContractorChoose(contractors);
                }});

            $('body').off('initLoopInstance');
            $('body').on('initLoopInstance', function () {
                $('.js-write-down-btn').on('click', function () {
                    $('html, body').animate({scrollTop: $('.project-create_cont').offset().top - 10}, 'slow');
                });
            });
        }
    },

    sendForm: function (e) {
        e.preventDefault();
        this.model.contractor.trigger('contractorSaveEvent', e, true, this, '/admin/contractor-add-extra-validate-and-change-campaign-status.json', true);
    }
});

CampaignEdit.Views.Interactive.SharesList = CampaignEdit.Views.InteractiveSharesEditListWrapper.extend({
    className: '',

    afterRender: function () {
        this.$el.find('[data-event-click=onCloneClick]').addClass('hidden');
        if (this.parent.model.get('activeIndex') == 9) {
            if (this.model.shares && this.model.shares.size() > 1) {
                $('.js-next-step').attr('disabled', false);
            } else {
                $('.js-next-step').attr('disabled', true);
            }
        }
    }
});

CampaignEdit.Views.Interactive.LessonMaterial = BaseView.extend({
    template: '#campaign-interactive-lesson-material-template',
    className: 'lesson-materials',
    events: {
        'click .js-agree-receive-lessons-on-email:': 'toggleReceive'
    },

    toggleReceive: function (el) {
        StorageUtils.toggleInteractiveDraftCampaignAgreeReceiveLessonsMaterialsOnEmail();
        var editorData = this.model.get('editorData');
        editorData.agreeReceiveLessonsMaterialsOnEmail = StorageUtils.isInteractiveDraftCampaignAgreeReceiveLessonsMaterialsOnEmail();
        this.model.set({editorData: editorData}, {silent: true});
    }
});

CampaignEdit.Views.Interactive.WriteDownButton = BaseView.extend({
    template: '#campaign-interactive-write-down-button-template',
    className: 'video-form_cont'
});

CampaignEdit.Views.Interactive.AddShareButton = BaseView.extend({
    template: '#campaign-interactive-add-share-button-template',
    className: 'project-edit-container_add-reward',
    events: {
        'click .js-add-share-button': 'addShare'
    },

    addShare: function () {
        var shareModel = {};
        var options = _.extend({}, shareModel.attributes, {
            campaignId: this.model.get('campaignId'),
            // necessary for share image upload
            profileId: this.model.get('profileId'),
            purchaseCount: 0,
            purchaseSum: 0,
            order: null
        });
        options.shareId = 0;
        var model = new CampaignEdit.Models.Share(options);
        model.uploaderName = this.model.uploaderName || "CampaignEdit.Views.ShareItem." + model.cid;
        var dialogView = new CampaignEdit.Views.SharesContentModal({
            model: model,
            controller: this.controller
        });
        Modal.showDialog(dialogView).done(function () {
            // only for share-edit dialog
            $('.modal-dialog').scrollTop(0);
        });
    }
});

CampaignEdit.Views.Interactive.Check = CampaignEdit.Views.Interactive.Base.extend({
    template: '#campaign-interactive-check-template',
    events: {
        'click .js-send-for-moderation': 'sendForModeration',
        'click .js-show-campaign': 'showCampaign'
    },
    construct: function () {
        if (!workspace.isAuthorized) {
            document.location.href = this.model.get('baseUrl');
        } else {
            this.loadVideoModule();
            $.get("/api/campaign/track-campaign-interactive-step-check.json", {campaignId: this.model.campaign.get('campaignId')});
        }
    },

    showCampaign: function () {
        var self = this;
        $.post('/admin/campaign-save-final-step.json').done(function (response) {
            if (response && response.success) {
                self.nextStep('/interactive/final-draft');
            } else {
                workspace.appView.showErrorMessage("Непредвиденная ошибка", 3000);
            }
        });
    },

    sendForModeration: function () {
        var self = this;
        $.post('/admin/campaign-save-final-step.json').done(function (response) {
            if (response && response.success) {
                self.nextStep('/interactive/final');
            } else {
                workspace.appView.showErrorMessage("Непредвиденная ошибка", 3000);
            }
        });
    }
});

CampaignEdit.Views.Interactive.SharingModal = Modal.OverlappedView.extend({
    template: '#campaign-interactive-sharing-modal-dialog-template'
});

CampaignEdit.Views.Interactive.FinalModeration = CampaignEdit.Views.Interactive.Base.extend({
    template: '#campaign-interactive-final-template',

    construct: function () {
        if (!workspace.isAuthorized) {
            document.location.href = this.model.get('baseUrl');
        } else {
            if (this.model.campaign.get('campaignId') > 0) {
                $.get("/api/campaign/track-campaign-interactive-step-final.json", {campaignId: this.model.campaign.get('campaignId')});
                this.loadVideoModule(this.saveAndValidateCampaign);
            } else {
                this.loadVideoModule('/interactive/email');
            }
        }
    },

    saveAndValidateCampaign: function (context) {
        var saveOnly = false;
        var footer = null;

        var self = context || this;
        var $dfd = CampaignEdit.Models.DraftCampaign.prototype.saveAndValidateCampaign.apply(self.model.campaign, [saveOnly, footer]);
        $dfd.done(function (response) {
            if (response.success) {
                if (saveOnly) {
                    if (footer && footer._saved) {
                        footer._saved(response.result.edited);
                    }
                } else {
                    if ('DRAFT' === self.model.campaign.get('status')) {
                        StorageUtils.clearInteractiveData();
                        document.location.href = '/campaigns/' + self.model.campaign.get('campaignId') + '/created-successful';
                    }
                }
            } else {
                self.model.navigateToValidation(response.errorMessage, response.fieldErrors, response.errorClass);
            }
        });
        return $dfd;
    },

    _fail: function () {
    }
});

CampaignEdit.Views.Interactive.FinalDraft = CampaignEdit.Views.Interactive.Base.extend({
    template: '#campaign-interactive-final-template',

    construct: function () {
        if (!workspace.isAuthorized) {
            document.location.href = this.model.get('baseUrl');
        } else {
            if (this.model.campaign.get('campaignId') > 0) {
                $.get("/api/campaign/track-campaign-interactive-step-final-draft.json", {campaignId: this.model.campaign.get('campaignId')});
                this.loadVideoModule(this.showCampaign);
            } else {
                this.loadVideoModule('/interactive/email');
            }
        }
    },

    showCampaign: function (context) {
        StorageUtils.clearInteractiveData();
        document.location.href = '/campaigns/' + context.model.campaign.get('campaignId');
    }
});
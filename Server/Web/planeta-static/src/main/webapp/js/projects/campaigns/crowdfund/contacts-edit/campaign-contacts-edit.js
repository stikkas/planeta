/*globals CrowdFund,Modal, AlbumTypes,ImageUtils,ImageType*/
CrowdFund.Views.CampaignContactsEdit = Modal.OverlappedView.extend({
    className: 'modal modal-author-info',
    template: '#campaign-contacts-edit-template',
    urlAnchor: 'contacts',

    modelEvents: {
        'destroy': 'dispose'
    },

    cleanErrors: function () {
        $(this.el).find('.js-contacts-row').each(function () {
            $(this).removeClass('error');
        });
        $(this.el).find('.js-contacts-error-icon').each(function () {
            $(this).remove();
        });
    },

    initialize: function () {
        Modal.OverlappedView.prototype.initialize.apply(this, arguments);
        var campaign = this.model.get("campaign");
        var profileSites = this.model.get("profileSites");
        this.model.set({
            creatorProfileDisplayName: campaign.get('creatorProfile').get('displayName'),
            siteUrl: profileSites.get('siteUrl'),
            vkUrl: profileSites.get('vkUrl'),
            facebookUrl: profileSites.get('facebookUrl'),
            twitterUrl: profileSites.get('twitterUrl'),
            fieldErrors: null
        });
        new CrowdFund.Views.CampaignContactsEdit.Email({
            model: new CrowdFund.Models.CampaignContactsEdit.Email({campaignId: this.model.get('campaign').get("campaignId")})
        }).insertInto(this);

        var creatorProfile = this.model.get("campaign").get('creatorProfile');
        this.avatarModel = new CrowdFund.Models.ImageField({
            originalImage: {
                imageUrl: creatorProfile.get('imageUrl'),
                photoId: creatorProfile.get('imageId')
            },
            profileId: creatorProfile.get('profileId'),
            albumTypeId: AlbumTypes.ALBUM_AVATAR,
            thumbnail:{
                imageConfig: ImageUtils.ALBUM_COVER,
                imageType: ImageType.GROUP
            },
            aspectRatio: 1,
            title: 'Аватар',
            sizeSuggestionText: '130x130'
        });
        this.avatarView = new CrowdFund.Views.ImageField({
            model: this.avatarModel
        });
        this.addChildAtElement('.js-edit-avatar', this.avatarView);
    },

    storeDataToModel: function () {
        var data = {campaignId: this.model.get('campaign').get("campaignId")};
        this.$('[data-field-name]').each(function () {
            var $this = $(this);
            data[$this.data('fieldName')] = _.isUndefined($this.data("fieldValue")) ? $this.val() : $this.data("fieldValue");
        });

        this.$('.js-url-input').each(function () {
            var $this = $(this);
            var url_prefix = $this.attr('url-prefix');
            var fullUrl = url_prefix + $.trim($this.val());
            data[$this.data('field-name')] = fullUrl === url_prefix ? '' : fullUrl;
        });

        this.model.set(data, {silent: true});

        return data;
    },

    disableButtons: function () {
        this.$('button[type=submit]').attr('disabled', 'disabled');
        this.$('button[type=reset]').attr('disabled', 'disabled');
    },
    enableButtons: function () {
        this.$('button[type=submit]').removeAttr('disabled');
        this.$('button[type=reset]').removeAttr('disabled');
    },

    save: function (e) {
        if (e) {
            e.preventDefault();
        }
        var self = this;
        this.disableButtons();
        this.cleanErrors();
        var data = this.storeDataToModel();
        if (!$.trim(data.displayName)) {
            workspace.appView.showErrorMessage('поле "Имя / название коллектива" не может быть пустым');
            this.$("[data-field-name='displayName']").focus();
            self.enableButtons();
            return;
        }

        this.model.get("profileSites").set({
            vkUrl: data.vkUrl,
            facebookUrl: data.facebookUrl,
            twitterUrl: data.twitterUrl,
            siteUrl: data.siteUrl
        });
        data.imageId = this.avatarModel.get("imageId");

        _.extend(this.model.get("campaign").get("creatorProfile"), {
            imageUrl: this.avatarModel.get("imageUrl"),
            imageId: data.creatorProfileImageId,
            smallImageUrl: this.avatarModel.get("imageUrl"),
            smallImageId: data.creatorProfileImageId,
            displayName: data.creatorProfileDisplayName
        });

        this.model.get("campaign").trigger("change");

        var fail = function (response) {
            if (response.fieldErrors) {
                $.map(response.fieldErrors, function (value, key) {
                    var rowEl = $(self.el).find('.js-contacts-row[name=' + key + ']');
                    $(rowEl).addClass('error');
                    var titleEl = $(self.el).find('.js-contacts-title[name=' + key + ']');
                    $(titleEl).append('<div class="pc_row_warning tooltip js-contacts-error-icon" data-tooltip="' + value + '"><div class="s-pc-action-warning"></div></div>');
                });
            } else {
                workspace.appView.showErrorMessage('При сохранении произошла ошибка');
            }
            self.enableButtons();
        };

        Backbone.sync("update", null, {
            data: data,
            url: '/api/campaigns/campaign-save-contact-info.json'
        }).done(function (response) {
            if (response && response.success) {
                workspace.appView.showSuccessMessage('Изменения сохранены');
                if (self.avatarView.save) {
                    self.avatarView.save();
                }
                if (response.result) {
                    self.model.get("profileSites").set(response.result);
                }
                self.enableButtons();
                self.dispose();
            } else {
                fail(response);
            }
        }).fail(function (response) {
            fail(response);
        });
    },

    cancel: function () {
        if (this.avatarView.cancel) {
            this.avatarView.cancel();
        }
        Modal.View.prototype.cancel.apply(this, arguments);
    }

});



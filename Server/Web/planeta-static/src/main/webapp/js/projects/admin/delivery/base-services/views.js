/*globals Delivery, Modal*/
/**
 *
 * @author Andrew
 * Date: 31.10.13
 * Time: 21:27
 */
Delivery.ViewUtils = {
    highlightErrors: function(_1, errMsg, fieldErrors) {
        var self = this;
        self.$('.control-group').removeClass('error');
        self.$('.error-message').empty();
        if (!fieldErrors) {
            workspace.appView.showErrorMessage(errMsg, 10000, self.$el);
        } else {
            _(fieldErrors).each(function(message, fieldName) {

                var s = fieldName.split('.');
                var $container = self.$el;
                for (var i = 0; i + 1 < s.length; i++ ) {
                    $container = $container.find('[data-name=' + s[i] + ']');
                }

                var name = s[s.length-1];
                var targetControlGroup = $container.find('[name=' + name + ']').closest('.control-group');
                targetControlGroup.addClass('error');
                targetControlGroup.find('.error-message').html(message);
            });
        }
    }
};

Delivery.Views.NestedLocationItem = BaseView.extend({
    tagName: 'li',
    template: '#nested-location-list-item-template',
    viewEvents: {
        'click': 'onSelectNestedLocation'
    }
});
Delivery.Views.NestedLocationList = BaseListView.extend({
    tagName: 'ul',
    className: 'dropdown-menu pre-scrollable',
    itemViewType: Delivery.Views.NestedLocationItem
});
Delivery.Views.ActiveLocation = BaseView.extend({
    template: '#editable-location-chain-template',
    events: {
        'click .remove-location': 'removeActiveLocation',
        'onSelectNestedLocation': 'selectNestedLocation'
    },
    construct: function(options) {
        this.nestedLocationsList = new Delivery.Models.NestedLocations([]);
        this.addChildAtElement('.nested-locations-list', new Delivery.Views.NestedLocationList({
            collection: this.nestedLocationsList
        }), true);
        this.resetNestedLocationsList();
    },
    resetNestedLocationsList: function() {
        this.nestedLocationsList.data = {
            locationId: this.model.get('location').locationId || 0,
            locationType: this.model.get('location').locationType || ''
        };
        this.nestedLocationsList.parent = this.model.get('location');
        this.nestedLocationsList.load();

    },
    removeActiveLocation: function(e) {

        var data = this.$(e.currentTarget).data();
        var locationId = data.locationId;
        var locationType = data.locationType;

        var location = this.model.get('location');
        while (location.locationId != locationId || location.locationType != locationType) {
            location = location.parent;
        }
        var parent = location.parent || {
            locationId: 0,
            locationType: '',
            parent: null
        };
        this.model.set({
            location: parent
        });
        this.resetNestedLocationsList();

    },
    selectNestedLocation: function(e, view) {
        var location = view.model.toJSON();
        location.parent = this.model.get('location');
        this.model.set({
            location: location
        });
        this.resetNestedLocationsList();
        this.$el.removeClass('open');
    },
    afterRender: function() {
        if (!this.model.get('location').locationType) {
            _.bind(Delivery.ViewUtils.highlightErrors, this)(null, null, {'location': 'обязательное поле'})
        }
    }
});
Delivery.Views.AddressBlock = BaseView.extend({
    template: '#address-edit-block-template',
    attributes: {
        'data-name': 'address'
    },
    modelEvents: {
        'change': 'render',
        'destroy': 'dispose',
        'change:serviceType': 'render'
    },
    events: {
        'keyup input[name]': 'onInputChanged',
        'keyup textarea[name]': 'onInputChanged',
        'drop input[name]': 'onInputChanged',
        'drop textarea[name]': 'onInputChanged',
        'change input[name]': 'onInputChanged'
    },
    onInputChanged: function(e) {
        var el = this.$(e.currentTarget);
        this.model.get('address')[el.attr('name')] = el.val();
    },
    afterRender: function() {
        if (this.model.get('serviceType') != 'CUSTOMER_PICKUP') {
            this.$el.hide();
        } else {
            this.$el.show();
        }
    }
});
Delivery.Views.ModalEditor = Modal.View.extend({
    template: '#admin-delivery-edit-base-service-template',

    events: _.extend({}, Modal.View.prototype.events, {
        'keyup input[name]': 'onInputChanged',
        'keyup textarea[name]': 'onInputChanged',
        'drop input[name]': 'onInputChanged',
        'drop textarea[name]': 'onInputChanged',
        'change input[name]': 'onInputChanged'

    }),
    construct: function(options) {
        this.addChildAtElement('.location-chain', new Delivery.Views.ActiveLocation({
            model: this.model
        }));
        if (this.model.get('serviceId') >= 0) {
            this.addChildAtElement('.delivery-address-block', new Delivery.Views.AddressBlock({
                model: this.model
            }));
        }
    },
    onInputChanged: function(e) {
        var el = this.$(e.currentTarget);
        this.model.set(el.attr('name'), el.val(), {silent: true});
        this.model.trigger('change:' + el.attr('name'), el.val());
    },

    save: function(options) {
        var self = this;
        return this.model.update().done(function(result) {
            self.model.sourceServiceModel && self.model.sourceServiceModel.set(result);
            self.model.collection && self.model.collection.add(self.model);
            self.cancel();
        }).fail(_.bind(Delivery.ViewUtils.highlightErrors, this));

    },

    afterRender: function() {
        Modal.View.prototype.afterRender.call(this);
        this.$('.elastic').elastic();
    }

});

Delivery.Views.BaseServiceItem = BaseView.extend({
    tagName: 'tr',
    template: '#base-delivery-campaign-template',
    events: {
        'click .start-service': 'enableService',
        'click .stop-service': 'disableService',
        'click .edit-service': 'editService',
        'click .remove-service': 'removeService'
    },
    enableService: function(e) {
        this.model.enable().fail(function(message) {
            workspace.appView.showErrorMessage(message, 5000);
        });
    },
    disableService: function(e) {
        this.model.disable().fail(function(message) {
            workspace.appView.showErrorMessage(message, 5000);
        });
    },
    editService: function(e) {
        var editServiceModel = new Delivery.Models.BaseService(this.model.toJSON());
        editServiceModel.sourceServiceModel = this.model;
        new Delivery.Views.ModalEditor({
            model: editServiceModel
        }).render();

    },
    removeService: function(e) {
        var self = this;
        Modal.showConfirm("Вы точно хотите удалить эту запись?", "Подтверждение удаления службы доставки", {
            success: function() {
                self.model.remove().fail(function(message) {
                    workspace.appView.showErrorMessage(message, 5000);
                });
            }
        });
    }
});
Delivery.Views.BaseServiceList = BaseListView.extend({
    className: 'services-list',
    tagName: 'tbody',
    itemViewType: Delivery.Views.BaseServiceItem
});

Delivery.Views.ModerateBaseServicesContent = BaseView.extend({
    template: '#campaign-delivery-services-page-template',
    events: {
        'click .add-base-service': 'addNewService'
    },
    construct: function(options) {
        this.addChildAtElement('tbody.services-list', new Delivery.Views.BaseServiceList({
            collection: this.model.services
        }), true);
    },
    addNewService: function(e) {
        var newServiceModel = new Delivery.Models.BaseService();
        newServiceModel.collection = this.model.services;
        new Delivery.Views.ModalEditor({
            model: newServiceModel
        }).render();
    }
});

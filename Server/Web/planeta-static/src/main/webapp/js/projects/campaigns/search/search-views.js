/*globals Search, Campaign, DefaultScrollableListView, moduleLoader, Banners, StringUtils, CampaignUtils, Header, PrivacyUtils, DefaultListView */
var CampaignSearch = {
    Models: {},
    Views: {}
};

CampaignSearch.Views.ResultItem = Search.Views.ResultItem.extend({
    className: 'project-card-item',
    tagName: 'div',
    template: '#top-campaigns-item-template',
    injectionHtml: 'projectCardHtml',
    injectionFunc: 'projectCard',

    construct: function () {
        this.$el.attr('href', 'https://' + workspace.serviceUrls.mainHost + '/campaigns/' + this.model.get('campaignId'));
    },

    beforeRender: function () {
        Campaign.Views.Widget.prototype.beforeRender.call(this);
        if (this.model
            && this.model.collection
            && this.model.collection.data
            && this.model.collection.data.categories) {
            var targetTagMnemonic = this.model.collection.data.categories;
            if (targetTagMnemonic) {
                var targetTag = _.find(this.model.get('tagsFull'), function (tag) {
                    return tag.mnemonicName == targetTagMnemonic;
                });
                this.model.set({targetTag: targetTag}, {silent: true});
            }
        }
    },

    afterRender: function () {
        var self = this;
        StringUtils.hyphenate(self);
        self.model.get('tags').forEach(function (it) {
            if (it === 'INVESTING') {
                self.$el.addClass('project-card-item-investing');
            }
        });
        if (self.model.get('status') === "FINISHED") {
            self.$el.addClass('project-card-item-finish')
        }
    },

    onItemClick: function () {
        if (window.gtm) {
            window.gtm.trackClickCampaignCard(this.model);
        }
    }
});

CampaignSearch.Views.BreadCrumbs = BaseView.extend({
    template: '#campaign-breadcrumbs-template',
    className: 'project-breadcrumbs',
    events: {
        'click .js-campaigns-breadcrumb' : 'campaignsClick'
    },

    construct: function () {
        var self = this;
        CampaignUtils.getCampaignTags().done(function (tags) {
            self.tags = _.filter(tags, function (obj) {
                return obj.visibleInSearch;
            });
            self.render();
        });

    },

    beforeRender: function () {
        if (this.model
            && this.model.query
            && this.model.query.get('categories')) {
            var targetTagMnemonic = this.model.query.get('categories');
            if (targetTagMnemonic) {
                var queryTag = _.find(this.tags, function (tag) {
                    return tag.mnemonicName == targetTagMnemonic;
                });
                this.model.set({queryTag: queryTag}, {silent: true});
            }
        }
    },

    campaignsClick: function (e) {
        e.preventDefault();
        window.location.href = '/search/projects';
    }
});

CampaignSearch.Views.ContentView = BaseView.extend({
    className: 'project-list-container',
    modelEvents: {
        'destroy': 'dispose'
    },

    construct: function () {
        var self = this;

        CampaignUtils.getCampaignTags().done(function (campaignTags) {
            self.changePlaceholder(campaignTags);

            var allCategories = {
                id: 0,
                mnemonicName: 'ALL',
                name: 'Все категории',
                engName: 'All categories',
                visibleInSearch: 'true'
            };

            if(!_.findWhere(campaignTags, allCategories)) {
                campaignTags.unshift(allCategories);
            }

            if (window.isMobileDev) {
                var categories = (_.filter(campaignTags, function (tag) {
                    return tag.visibleInSearch;
                }));
                self.model.set({campaignTags: categories}, {silent: true});
            } else {
                self.model.set({campaignTags: campaignTags}, {silent: true});
            }

            self.addChild(new CampaignSearch.Views.Query({
                model: self.model
            }));

            self.addChild(new CampaignSearch.Views.Results({
                model: self.model
            }));
        });

        //Костыль: анбиндит эвент на клик по дропдауну на странице поиска вознаграждений, дабы не мешать дропдаунам на
        //этой странице.
        $(window).unbind('click.Bst');
    },

    changePlaceholder: function (campaignTags) {
        var self = this,
            placeholder;

        var cats = new BaseCollection(_.filter(campaignTags, function (tag) {
            return !tag.specialProject && tag.visibleInSearch && tag.mnemonicName === self.model.query.get('categories') && tag.mnemonicName !== 'ALL';
        }));

        if (workspace.currentLanguage && workspace.currentLanguage === 'ru') {
            if (cats.length > 0) {
                placeholder = 'Поиск проектов в категории «' + cats.models[0].get('name') + '»';
            } else {
                placeholder = 'Поиск проектов';
            }
        } else {
            if (cats.length > 0) {
                placeholder = 'Search for projects in category "' + cats.models[0].get('engName') + '"';
            } else {
                placeholder = 'Search campaigns';
            }
        }

        this.model.set('placeholder', placeholder);
        $('#query').attr('placeholder', placeholder);
    },

    afterRender: function () {
        Banner.init('', workspace.appModel.get('myProfile').get('isAuthor'));
    }
});

CampaignSearch.Views.Projects = DefaultScrollableListView.extend({
    emptyListTemplate: '#campaign-search-results-empty-container-template',
    pagerLoadingTemplate: '#search-scrollable-list-loader-template',

    modelEvents: _.extend({
        "change:fetching": "onFetchingChanged"
    }, DefaultScrollableListView.prototype.modelEvents),

    onAdd: function (campaign) {
        var self = this;

        if (window.gtm) {
            if (!this.campaignList) {
                this.campaignList = [];
                this.startIndex = this._itemViews.length;

                setTimeout(function () {
                    var campaignList = self.campaignList;
                    delete self.campaignList;
                    CampaignUtils.getCampaignTags().done(function (campaignTags) {
                        var category = self.model.query.get('categories');
                        var tag = _.find(campaignTags, function (t) {
                            return t.mnemonicName == category;
                        });

                        if (window.gtm) {
                            window.gtm.trackViewCampaignList(campaignList, "Витрина проектов - " + (tag ? tag.name : 'Все категории'), self.startIndex);
                        }
                    });

                }, 10);
            }
            this.campaignList.push(campaign);
        }
        return DefaultScrollableListView.prototype.onAdd.apply(this, arguments);
    },

    onFetchingChanged: function () {
        if (this.model.get('fetching')) {
            this.collection.reset();
            this.renderPagerLoadingElement();
        } else {
            this.addPagerElement(null);
        }
    }
});

CampaignSearch.Views.RightMenuItem = BaseView.extend({
    tagName: 'li',
    template: "#items-campaign-tag-template",
    className: 'project-category_i',
    construct: function () {
        if (workspace) {
            if (workspace.currentLanguage) {
                this.model.set('useEng', workspace.currentLanguage === "en", {silent: true});
            }
        }
    },
    afterRender: function () {
        this.$el.removeClass("active");
        if (this.controller && this.controller.query && this.controller.query.toJSON().categories === this.model.get("mnemonicName")) {
            this.$el.addClass("active");
        }
        if (this.controller && !this.controller.query.toJSON().categories && this.model.get("mnemonicName") == 'ALL') {
            this.$el.addClass("active");
        }
    }
});


CampaignSearch.Views.TopMenu = BaseView.extend({
    template: '#campaign-top-menu-template',
    events: {
        'click .js-type-select': 'toggleDropdown',
        'click .dropdown .item a': 'visualizeSwitchToActiveSelection',
        'click [data-status]': 'onStatusChanged',
        'click .js-country_select': 'openDropDown',
        'change .js-country_select': 'countrySelect',
        'change #regionId': 'regionSelect',
        'change #cityId': 'citySelect'
    },

    isExistsPlanetaUiElement: true,

    modelEvents: {
        'destroy': 'dispose'
    },

    construct: function () {
        var pathName = window.location.href,
            row = pathName.substring(pathName.lastIndexOf("/") + 1, pathName.length),
            params = this.parseUrlToParams(row),
            countryId = params.countryId ? params.countryId : 0,
            regionId = params.regionId ? params.regionId : 0,
            cityId = params.cityId ? params.cityId : 0;

        if (countryId != 0) {
            this.model.set('countryId', countryId);
        }

        if (regionId != 0 || cityId != 0) {
            this.searchLocationFilterNames.call(this.model, cityId, regionId);
        } else {
            this.model.set('onlyCountry', true);
        }
    },
    getSponsorAlias: function () {
        var sponsor = undefined;
        var query = this.model.query.get('categories');
        if (query) {
            var tag = undefined;
            CampaignUtils.getCampaignTags().done(function (campaignTags) {
                tag = _.find(campaignTags, function (item) {
                    return item.mnemonicName == query;
                });
            });
            if (tag) {
                sponsor = tag.sponsorAlias;
            }
        }
        return sponsor;
    },

    searchLocationFilterNames: function (cityId, regionId) {
        var self = this;
        $.ajax({
            url: '/api/search/search-location-filter-names',
            async: false,
            data: {
                regionId: regionId,
                cityId: cityId
            },
            success: function (response) {
                if (response.cityNameRus) {
                    self.set('cityNameRus', response.cityNameRus);
                    self.set('cityId', response.cityId);
                }
                if (response.cityNameEng) {
                    self.set('cityNameEng', response.cityNameEng);
                    self.set('cityId', response.cityId);
                }

                self.set({
                    countryId: response.countryId,
                    regionId: response.regionId,
                    regionNameRus: response.regionNameRus,
                    regionNameEng: response.regionNameEng
                });

                self.changeLocation(response.countryId, response.regionId, response.cityId);
            }
        });
    },

    changeCategories: function (e) {
        e.preventDefault();
        var $sel = $(e.currentTarget);
        $sel.closest('.project-category').find('.active').removeClass('active');
        $sel.parent().addClass('active');
        this.model.changeCategory($sel.data('categories'));
        Banner.init('', workspace.appModel.get('myProfile').get('isAuthor'));

    },

    parseUrlToParams: function (row) {
        return _.object(_.compact(_.map(row.split('&'), function (item) {
            if (item) {
                return item.split('=');
            }
        })));
    },

    openDropDown: function () {
        this.$('.project-filter-row').attr('style', 'display: block;');
    },

    countrySelect: function (event) {
        var countryId = event.target.value;
        this.model.set({
            'countryId': countryId,
            'regionNameRus': '',
            'regionNameEng': '',
            'regionId': 0,
            'cityNameRus': '',
            'cityNameEng': '',
            'cityId': 0
        });
        this.model.changeLocation(countryId, 0, 0);
    },

    regionSelect: _.debounce(function (event) {
        var el = event.target,
            $query = this.model.query,
            cityId = $query.get('cityId') ? $query.get('cityId') : 0;

        if (($query.get('regionId') && ($query.get('regionId') != el.value))
            || (!$query.get('regionId') && (el.value != 0))) {
            this.model.set({
                'regionNameRus': this.$('[name=regionNameRus]').val(),
                'regionNameEng': this.$('[name=regionNameEng]').val(),
                'regionId': el.value,
                'cityNameRus': '',
                'cityNameEng': '',
                'cityId': 0
            });
            this.model.changeLocation($query.get('countryId'), el.value, cityId);
        }
        document.body.focus();
    }, Search.INPUT_DELAY),

    citySelect:
        _.debounce(function (event) {
            var el = event.target,
                $query = this.model.query,
                regionId = $query.get('regionId') ? $query.get('regionId') : 0;
            if (($query.get('cityId') && ($query.get('cityId') != el.value))
                || (!$query.get('cityId') && (el.value != 0))) {
                this.model.set({
                    'cityNameRus': this.$('[name=cityNameRus]').val(),
                    'cityNameEng': this.$('[name=cityNameEng]').val(),
                    'cityId': el.value
                });
                this.model.changeLocation($query.get('countryId'), regionId, el.value);
            }
            document.body.focus();
        }, Search.INPUT_DELAY),

    visualizeSwitchToActiveSelection: function (e) {
        var $sel = $(e.currentTarget);
        $sel.closest('.dropdown').find('.selectCampaignById-cont').text($sel.text());
        $sel.closest('.dropdown-menu').find('.active').removeClass('active');
        $sel.parent().addClass('active');
    },

    onStatusChanged: function (e) {
        e.preventDefault();
        var $sel = $(e.currentTarget);
        this.model.query.set({status: $sel.data('status')});
        this.model.query.navigate();
    },

    toggleDropdown: function (e) {
        var dropSelect = $(e.currentTarget);
        dropSelect.clickOff(function() {
            dropSelect.removeClass('open');
        }, null);

        if (!dropSelect.hasClass('open')) {
            dropSelect.addClass('open');
        } else {
            dropSelect.removeClass('open');
        }
    },

    afterRender: function () {
        var countryId = this.model.get('countryId'),
            regionId = this.model.get('regionId'),
            cityId = this.model.get('cityId');

        if (countryId != 0 || regionId != 0 || cityId != 0) {
            this.$('.project-filter-row').css('display', 'block');
            $('.s-icon-angle-down').rotate(180);
            $('.s-icon-angle-down').attr('data-open', 'true');
        }

        var self = this;
        setTimeout(function () {
            self.$('.pln-selectCampaignById').addClass('pln-select__inited');
        }, 500);
        this.$('.dropdown .item.active a').each(function () {
            var $el = $(this);
            $el.closest('.dropdown').find('.selectCampaignById-cont').text($el.text());
        });
    }
});

CampaignSearch.Views.RightMenuListCategories = DefaultListView.extend({
    tagName: 'ul',
    className: 'project-category_list',
    itemViewType: CampaignSearch.Views.RightMenuItem
});

CampaignSearch.Views.RightMenuListSpecials = BaseListView.extend({
    tagName: 'ul',
    className: 'project-category_list project-category_list__spec',
    itemViewType: CampaignSearch.Views.RightMenuItem
});

CampaignSearch.Views.MobileCategoriesSelect = BaseView.extend({
    template: '#campaign-search-categories-mobile-template',
    events: {
        'click .js-category-select': 'toggleDropdown'
    },

    fillTag: function () {
        var query = this.model.query.get('categories');
        var campaignTags = this.model.get('campaignTags');

        if (query) {
            var tag = _.find(campaignTags, function (item) {
                return item.mnemonicName == query;
            });
            return tag;
        }
    },

    afterRender: function () {
        setTimeout(function () {
            this.$('.pln-selectCampaignById').addClass('pln-select__inited');
        }, 500);

        var campaignTags = this.model.get('campaignTags');
        var tag = this.fillTag();
        if (tag) {
            this.$('.js-selectCampaignById-categories-text').text(this.model.get('lang') == 'en' ? tag.engName : tag.name);
        } else {
            var allTag = _.find(campaignTags, function (item) {
                return item.mnemonicName == 'ALL';
            });
            this.$('.js-selectCampaignById-categories-text').text(this.model.get('lang') == 'en' ? allTag.engName : allTag.name);
        }
    },

    toggleDropdown: function (e) {
        var dropSelect = $(e.currentTarget);
        var close = function (event) {
            var parentmatch = $(event.target).parents().map(function () {
                return this == dropSelect[0] ? this : null;
            });
            if ((event.target == dropSelect[0] || parentmatch.length > 0)) {
                return;
            }
            $(document).unbind('click', close);
            dropSelect.removeClass('open');
        };

        if (!dropSelect.hasClass('open')) {
            dropSelect.addClass('open');
            $(document).click(close);
        } else {
            dropSelect.removeClass('open');
        }
    }
});

CampaignSearch.Views.DesktopCategoriesSelect = BaseView.extend({
    template: '#campaign-search-categories-desktop-template',
    construct: function () {
        var self = this;
        var campaignTags = self.model.get('campaignTags');
        self.addChildAtElement('.js-by-specials', new CampaignSearch.Views.RightMenuListSpecials({
            collection: new BaseCollection(_.filter(campaignTags, function (tag) {
                return !!tag.specialProject && tag.visibleInSearch;
            })),
            model: self.model,
            controller: self.model
        }));

        var categories = (_.filter(campaignTags, function (tag) {
            return !tag.specialProject && tag.visibleInSearch;
        }));
        self.addChildAtElement('.js-by-category', new CampaignSearch.Views.RightMenuListCategories({
            collection: new BaseCollection(categories),
            model: self.model,
            controller: self.model
        }));

    }
});

CampaignSearch.Views.Results = BaseView.extend({
    tagName: 'div',
    id: 'resultlist',
    className: 'wrap',
    template: '#campaign-search-results-container-template',
    injectionHtml: "searchPageHtml",
    injectionAnchor: ".js-campaign-search-injection",

    listEl: null,

    events: {
        'click [data-categories]': 'changeCategories'
    },
    construct: function () {
        var self = this;
        this.listEl = this.addChildAtElement('.project-list-block', new CampaignSearch.Views.Projects({
            id: 'search-results',
            className: 'project-card-list',
            collection: this.model.searchResults,
            itemViewType: CampaignSearch.Views.ResultItem,
            model: this.model
        }));

        if (window.isMobileDev) {
            self.addChildAtElement('.js-mobile-categories', new CampaignSearch.Views.MobileCategoriesSelect({
                model: self.model
            }));
        } else {
            self.addChildAtElement('.js-desktop-categories', new CampaignSearch.Views.DesktopCategoriesSelect({
                model: self.model
            }));
        }

        self.addChildAtElement('.js-project-list-filter', new CampaignSearch.Views.TopMenu({
            model: self.model,
            controller: self.model
        }));
    },

    getSponsorAlias: function () {
        return this.model.query.get('categories');
    },

    changeCategories: function (e) {
        e.preventDefault();
        var $sel = $(e.currentTarget);
        $sel.closest('.project-category').find('.active').removeClass('active');
        $sel.parent().addClass('active');
        this.model.changeCategory($sel.data('categories'));
        Banner.init();
        CampaignSearch.Views.ContentView.prototype.changePlaceholder.call(this, this.model.get('campaignTags'));
    }
});

CampaignSearch.Views.Query = Search.Views.Query.extend({
    template: '#campaign-search-block-template',
    events: _.extend({}, Search.Views.Query.prototype.events),

    modelEvents: {
        'destroy': 'dispose'
    },

    construct: function () {
        if (workspace) {
            if (workspace.currentLanguage) {
                this.model.set('lang', workspace.currentLanguage);
            }
        }
    },

    afterRender: function () {
        if (this.model.query) {
            $('#query').val(this.model.query.get('query'));
        }
        $('#query').focus();

        window.toTopStart = 0;
        window.toTopOffset = 0;

        var toTop = $('.to-top');
        var toTopBlock = $('.to-top_block');
        var winHeight = window.innerHeight;

        toTopBlock.css({paddingTop: window.toTopOffset});

        $(window).on('resize', function () {
            winHeight = window.innerHeight;
        });

        $(window).on('scroll', function () {
            var winTop = $(window).scrollTop();

            if ( winTop > winHeight + window.toTopStart ) {
                toTop.addClass('to-top__show');
            } else {
                toTop.removeClass('to-top__show');
            }
        });

        toTop.on('click', function () {
            $(window).scrollTop(50);
            $('html, body').animate({scrollTop: 0}, 200, null);
        });
    }
});
